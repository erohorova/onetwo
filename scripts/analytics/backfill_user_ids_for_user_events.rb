# frozen_string_literal: true

# =========================================
# We fixed user_ids for user_created and user_edited events in thusee following:
#
# - EP-19221  (https://github.com/edcast/edcast/pull/9298)
# - EP-19227  (https://github.com/edcast/edcast/pull/9300)
#
# This file is for fixing existing data with these changes.
# =========================================

module Analytics
  class BackfillUserIdsForUserEvents
    class << self
    
      # Given that user_created events have already been exported to files
      # (using .build_backup), goes through them to set created_user_id,
      # and attempt to set user_id and _user_id using Invitation and
      # InvitationFile records. Pushes them back to Influx afterwards.
      #
      # (NOTE: Should configure the server to push to an alternate database
      # before using this method)
      def run_for_user_created_events!(min_date)
        
        confirm_backup_made("user_created")

        updated_count = 0
        events_with_new_schema_skipped = 0
        user_ids_found_from_bulk_invitations = 0
        user_ids_found_from_direct_invitations = 0
        user_ids_left_blank = 0

        invitation_files_map = build_invitation_files_map(min_date)
        direct_invitations_map = build_direct_invitations_map(min_date)

        orig_records_paths = get_orig_records_paths("user_created")
        orig_records_paths.each do |path|
          puts "PROCESSING #{path}"
          records = JSON.parse(File.read(path)).flat_map do |record_group|
            record_group["values"]
          end
          insertion_records = records.map do |record|
            Analytics::MetricsRecorder.response_to_insertion(
              "users",
              record,
              tag_keys: user_tags
            )
          end
          ids_to_emails_map = build_ids_to_emails_map(insertion_records)
          insertion_records.each do |record|
            if record["values"]["created_user_id"]
              events_with_new_schema_skipped += 1
              next
            end
            user_id = record["values"]["user_id"].to_s
            record["values"]["created_user_id"] = user_id
            record["values"].delete("user_id")
            record["tags"].delete("_user_id")
            updated_count += 1
            org_id = record["tags"].fetch("org_id").to_s
            sender_id = if id = direct_invitations_map.dig(org_id, user_id)
              user_ids_found_from_direct_invitations += 1
              id
            else
              email = ids_to_emails_map.dig(org_id, user_id)
              if id = invitation_files_map.dig(org_id, email)
                user_ids_found_from_bulk_invitations += 1
                id
              else
                user_ids_left_blank += 1
                nil
              end
            end
            next unless sender_id
            # Yes this is N+1 but it's a hassle to refactor and it's a one-off script
            sender_record = User.find_by id: sender_id
            next unless sender_record
            sender = RecursiveOpenStruct.new(
              Analytics::MetricsRecorder.user_attributes(sender_record)
            )
            record["values"]["user_id"] = sender_id
            record["tags"]["_user_id"] = sender_id
            record["values"]["sign_in_ip"] = sender.sign_in_ip
            record["values"]["sign_in_at"] = sender.sign_in_at.to_i.to_s
            record["values"]["user_role"] = sender.organization_role
            record["values"]["user_org_uid"] = sender.user_org_uid.to_s
            record["values"]["onboarding_status"] = sender.onboarded? ? '1' : '0'
            record["values"]["full_name"] = sender.full_name
            record["values"]["user_handle"] = sender.handle

          end
          puts "INSERTING #{insertion_records.length} records"
          Analytics::MetricsRecorder.influxdb_bulk_record(
            "users",
            insertion_records
          )
        end
        puts "#{updated_count} updated"
        puts "#{user_ids_found_from_bulk_invitations} ids found from bulk invitations"
        puts "#{user_ids_found_from_direct_invitations} ids found from direct invitations"
        puts "#{user_ids_left_blank} ids left blank"
        puts "#{events_with_new_schema_skipped} events with new schema skipped"
        puts "DONE"
      end

      # Given that user_edited events have already been exported to files
      # (using .build_backup), goes through them to set edited_user_id,
      # and set user_id and _user_id to nil.
      # Pushes them back to Influx afterwards.
      #
      # (NOTE: Should configure the server to push to an alternate database
      # before using this method)
      def run_for_user_edited_events!

        confirm_backup_made("user_edited")

        updated_count = 0
        events_with_new_schema_skipped = 0
        non_admin_events = 0

        orig_records_paths = get_orig_records_paths("user_edited")
        orig_records_paths.each do |path|
          puts "PROCESSING #{path}"
          records = JSON.parse(File.read(path)).flat_map do |record_group|
            record_group["values"]
          end
          insertion_records = records.map do |record|
            Analytics::MetricsRecorder.response_to_insertion(
              "users",
              record,
              tag_keys: user_tags
            )
          end
          insertion_records.each do |record|
            # Don't update the record if it already has edited_user_id
            # (it was pushed using the new code which includes correct user_id)
            if record["values"]["edited_user_id"]
              events_with_new_schema_skipped += 1
              next
            end
            updated_count += 1
            record["values"]["edited_user_id"] = record["values"]["user_id"]
            unless record["values"]["is_admin_request"] == "1"
              non_admin_events += 1
              next
            end
            record["values"].delete("user_id")
            record["tags"].delete("_user_id")
            record["values"].delete("sign_in_ip")
            record["values"].delete("sign_in_at")
            record["values"].delete("user_role")
            record["values"].delete("user_org_uid")
            record["values"].delete("onboarding_status")
            record["values"].delete("full_name")
            record["values"].delete("user_handle")

          end
          puts "INSERTING #{insertion_records.length} records"
          Analytics::MetricsRecorder.influxdb_bulk_record(
            "users",
            insertion_records
          )
        end
        puts "#{updated_count} updated"
        puts "#{events_with_new_schema_skipped} events with new schema skipped"
        puts "#{non_admin_events} non-admin events"
        puts "DONE"
      end

      # NOTE: This should be manually run for each of the events,
      # prior to running the migrations.
      def build_backup(measurement, event, start_date: nil)
        start_date ||= get_start_date(measurement, event)
        query = InfluxQuery.new(measurement)
          .filter!(:event, "event", "=", event)
          .add_time_filters!(start_date: start_date)
        puts query.to_plain_query
        query.resolve_with_loop(interval: 15.days.to_i) do |records, idx|
          path = orig_record_path(event, "#{idx}.json")
          File.open(path, "w") do |file|
            file.write(records.to_json)
          end
        end
        nil
      end

      # Returns a glob selector for files containing records to modify.
      # Populate these files using .build_backup
      def get_orig_records_paths(event)
        Dir.glob(orig_record_path(event, "*"))
      end

      # Builds a path to a single file containing records to modify.
      # Used by .build_backup
      def orig_record_path(event, suffix)
        Rails.root.join(
          "scripts",
          "analytics",
          "backfill_user_ids_for_user_events",
          "#{event}_records#{suffix}",
        )
      end

      # Returns the earliest event date from Influx for the given event
      def get_start_date(measurement, event)
        query_result = InfluxQuery.new(measurement).
          filter!(:event, "event", "=", event).
          order!("ASC").
          limit!(1).
          select!("user_id").
          resolve

        time = query_result[0]["values"][0]["time"]
        Time.parse(time).utc.beginning_of_day.to_i
      end

      # Maps user email to InvitationFile record. Used to backfill user_created
      def build_invitation_files_map(min_date)
        results = {}

        query = InvitationFile.
          joins("INNER JOIN users ON invitation_files.sender_id = users.id").
          where("invitation_files.created_at >= ?", Time.at(min_date)).
          where(invitable_type: "Organization").
          select("sender_id, data, invitation_files.id, users.organization_id")

        query.find_in_batches(batch_size: 1000) do |records|
          records.compact.each do |record|

            # attempt to parse the csv
            begin
              data_csv = CSV.parse(record.data)
            rescue CSV::MalformedCSVError
              next
            end

            # take the second element of all rows (excluding the header row)
            data_csv.tap(&:shift).each do |csv_row|
              email = csv_row[2]
              next if email.blank?
              # Map the email to the sender in the results hash
              org_id = record.organization_id.to_s
              results[org_id] ||= {}
              results[org_id][email] = record.sender_id.to_s
            end

          end
        end

        results
      end

      # Maps user id to Invitation record. Used for user_created backfill
      # With these records we have sender_id and recipient_id.
      # The recipient_id is only set if the user accepts the invitation,
      # but those are the only cases we care about.
      def build_direct_invitations_map(min_date)
        results = {}

        query = Invitation.
          joins("INNER JOIN users ON invitations.recipient_id = users.id").
          where(invitable_type: "Organization").
          where("invitations.created_at >= ?", Time.at(min_date)).
          where("recipient_id IS NOT NULL").
          select("sender_id, recipient_id, invitations.id, users.organization_id")

        query.find_in_batches(batch_size: 1000) do |records|
          records_map = records.
            compact.
            group_by(&:organization_id).
            transform_keys(&:to_s).
            transform_values do |values_per_org|
              values_per_org.
                index_by(&:recipient_id).
                transform_keys(&:to_s).
                transform_values(&:sender_id).
                transform_values(&:to_s)
            end

          results.deep_merge! records_map
        end

        results
      end

      # Looks up emails for each of the given users.
      # Used for user_created backfill, in order to join with InvitationFiles
      def build_ids_to_emails_map(insertion_records)
        user_ids = insertion_records.map do |record|
          record["values"].fetch("user_id")
        end
        User.
          where(id: user_ids).
          select(:id, :email, :organization_id).
          group_by(&:organization_id).
          transform_keys(&:to_s).
          transform_values do |values_per_org|
            values_per_org.
              index_by(&:id).
              transform_keys(&:to_s).
              transform_values(&:email)
          end
      end

      def confirm_backup_made(event)
        puts <<-TXT.strip_heredoc

        =========================================
        PLEASE CONFIRM YOU HAVE MADE A BACKUP OF #{event}
        RECORDS BEFORE RUNNING THIS SCRIPT!

        IF YOU HAVE DONE SO, TYPE OK
        =========================================

        TXT
        unless gets.chomp.downcase == "ok"
          puts "CONFIRMATION NOT GIVEN, EXITING"
          exit
        end
      end

      def user_tags
        %w{
          _user_id
          analytics_version
          event
          is_system_generated
          org_id
        }
      end

    end
  end
end

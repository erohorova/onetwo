module Analytics

  class BackfillPathwayAndJourneyName

    class << self
    
      def run_for_pathway_events!
        # -----------------------------------------
        # NOTE: the reason we can use InfluxUpdate here,
        # is because we're only updating fields, not tags.
        # -----------------------------------------
        num_changed_total = 0
        Analytics::InfluxUpdater.process_records(
          measurement: "cards",
          in_batches_of: 5000,
          date_chunk_range: 100,
          start_date: Date.new(2017, 1, 3), # earliest record date in prod
          end_date: Date.tomorrow,
          from_version: Analytics::InfluxUpdater.analytics_version,
          write_client: INFLUXDB_CLIENT.class::CLIENT,
          event: "card_added_to_pathway"
        ) do |records|
          num_changed_in_block = 0
          puts "processing #{records.length} card_added_to_pathway events"
          records = records.select { |record| record['values']['pathway_id'] }
          pathway_ids = records.map do |record|
            record['values']['pathway_id']
          end.uniq # compact since there are some records lacking pathway_id
          pathway_names = Card
            .with_deleted
            .where(id: pathway_ids)
            .select(:id, :message, :title)
            .index_by(&:id)
            .transform_values { |val| [val.message, val.title] }
          records.each do |record|
            pathway_id = record["values"]["pathway_id"].to_i
            pathway_name_opts = pathway_names[pathway_id]
             # If card no longer exists, even in deleted table,
             # for whatever reason, skip it.
            next unless pathway_name_opts&.reject(&:blank?)&.any?
            next if record["values"]["pathway_name"]
            message, title = pathway_name_opts
            record["values"]["pathway_name"] = title || message
            num_changed_total += 1
            num_changed_in_block += 1
          end
          puts "#{num_changed_in_block} changed"
        end
        puts "#{num_changed_total} changed in total"

      end # def run_for_pathway_events!

      def run_for_journey_events!
        # -----------------------------------------
        # NOTE: the reason we can use InfluxUpdate here,
        # is because we're only updating fields, not tags.
        # -----------------------------------------

        num_changed_total = 0
        Analytics::InfluxUpdater.process_records(
          measurement: "cards",
          in_batches_of: 5000,
          date_chunk_range: 100,
          start_date: Date.new(2018, 4, 18), # earliest record date in prod
          end_date: Date.tomorrow,
          from_version: Analytics::InfluxUpdater.analytics_version,
          write_client: INFLUXDB_CLIENT.class::CLIENT,
          event: "card_added_to_journey"
        ) do |records|
          num_changed_in_block = 0
          puts "processing #{records.length} card_added_to_journey_events"
          records = records.select { |record| record['values']['journey_id'] }
          journey_ids = records.map do |record|
            record['values']['journey_id']
          end.uniq # compact since there are some records lacking journey_id
          journey_names = Card
            .with_deleted
            .where(id: journey_ids)
            .select(:id, :title)
            .index_by(&:id)
            .transform_values(&:title)
          records.each do |record|
            journey_id = record["values"]["journey_id"].to_i
            journey_name = journey_names[journey_id]
             # If card no longer exists, even in deleted table,
             # for whatever reason, skip it.
            next unless journey_name
            next if record["values"]["journey_name"]
            record["values"]["journey_name"] = journey_name
            num_changed_total += 1
            num_changed_in_block += 1
          end
          puts "#{num_changed_in_block} changed"
        end
        puts "#{num_changed_total} changed total"
      end # def run_for_journey_events!


    end # class << self
  end # class BackfillPathwayAndJourneyName
end # module Analytics
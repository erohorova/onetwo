# =========================================
#
# This script takes as input the paths of one or more log files,
# taken from logentries which contain logs from sidekiq.
#
# It parses them, determines the average time taken for each job,
# and prints the results to stdout.
#
# USAGE:
#
# Call from shell like so:
#
# ruby ./analyze_sidekiq_jobs /some/log/path /some/other/path
#
# =========================================

require 'oj'
require 'byebug'

class AnalyzeSidekiqLogs

  CHECKPOINT = 100_000

  def self.run!(log_paths)
    results = Hash.new { |h,k| h[k] = { count: 0, time: 0.0 } }
    start_time = Time.now.to_i
    log_paths.each do |log_path|
      analyze_log_path!(results, log_path)
    end
    print_results!(results)
    puts "took: #{Time.now.to_i - start_time} seconds"
  end

  def self.analyze_log_path!(results, log_path)
    ensure_log_file_exists!(log_path)
    total_num_lines = get_total_num_lines(log_path)
    puts "total num lines: #{total_num_lines}"
    IO.foreach(log_path).each.with_index do |line, idx|
      log_message_at_checkpoint!(idx, total_num_lines)
      analyze_line!(results, line, idx, total_num_lines)
    end
  end

  def self.ensure_log_file_exists!(log_path)
    unless File.exists?(log_path)
      puts "enter valid log file path as first argument"
      exit
    end
  end

  def self.get_total_num_lines(log_path)
    `wc -l #{log_path}`.split(" ")[0].to_i
  end

  def self.analyze_line!(results, line)
    regex =  /"line":"[^ ]+ [^ ]+ [^ ]+ ([^ ]+) [^ ]+ INFO: done: ([^ ]+)/
    matches = line.scan(regex).shift
    next unless matches&.length == 2
    job_name, time = matches
    results[job_name][:time] = (
      (
        results[job_name][:time] * results[job_name][:count]
      ) + time.to_f
    ) / (results[job_name][:count] + 1)
    results[job_name][:count] += 1
  end

  def self.log_message_at_checkpoint!(idx, total_num_lines)
    if idx % self::CHECKPOINT == 0
      puts "#{((idx.to_f / total_num_lines) * 100).to_i}\% done (i = #{idx})"
    end
  end

  def self.print_results!(results)
    longest_name = results.keys.max_by(&:length)
    results.each do |job_name, data|
      puts [
        job_name.ljust(longest_name.length, ' '),
        data[:time],
        "(#{data[:count]} found)"
      ].join(" - ")
    end
  end
  
end

if __FILE__ == $0
  AnalyzeSidekiqLogs.run!(ARGV.to_a)
end


require 'capybara/rails'
require 'capybara/cucumber'
require 'capybara/poltergeist'

Given(/^I visit forum wide mode page$/) do

  #generate test client and api key for development
  client_owner = User.create!(email: 'client_owner@client.com', first_name: 'Client', last_name: 'Owner', 'password' => 'password')
  client = Client.create!(user: client_owner, name: 'test_client')
  su1 = User.create!(email: 'firststudentuser@course-master.com', first_name: 'First', last_name: 'Studentuser', 'password' => 'password')
  su2 = User.create!(email: 'secondstudentuser@course-master.com', first_name: 'Second', last_name: 'Student', 'password' => 'password')
  fa1 = User.create!(email: 'facultyuser@course-master.com', first_name: 'Faculty', last_name: 'Teacher', 'password' => 'password')
  resource1 = Group.create(name: 'test group 1', description: 'first test group', client_resource_id: 'test_resource', client: client)
  resource2 = Group.create(name: 'test group 2', description: 'second test group', client_resource_id: 'test_resource_2', client: client)
  resource1.students << su1
  resource1.students << su2
  resource1.instructors << fa1
  question1 = Question.create!(user: su1, group: resource1, title: 'student question?', message: 'I is student #test #to #trial')
  question2 = Question.create!(user: su1, group: resource1, title: 'student votes?', message: 'question to check the votes')
  question3 = Question.create!(user: fa1, group: resource1, title: 'faculty question?', message: 'question created by faculty')
  answer2 = Answer.create!(message: 'I is an answer', commentable: question1, user: su1)
  answer3 = Answer.create!(message: 'I is not an answer', commentable: question1, user: client_owner)
  answer4 = Answer.create!(message: 'faculty answer', commentable: question1, user: fa1)
  Vote.create(votable: question2, voter: client_owner)
  Vote.create(votable: question2, voter: su2)
  Vote.create(votable: question2, voter: su1)

  visit '/ui/demo'

end

And(/^I have created a question$/) do
  sleep(0)
  within_frame 'cm-gui-0' do
    click_on '+ New Post'
    fill_in('Title (required)',  :with => 'Sample post')
    within_frame 0 do
      editor = page.find_by_id('tinymce')
      editor.native.send_keys("Is the new post working?")
    end
    click_on 'Post'
    end
sleep(2)
end

When(/^I click “New Post" button$/) do
  sleep(0)
  within_frame 'cm-gui-0' do
    click_on '+ New Post'
    end

end

And(/^I submit valid title and description$/) do
  within_frame 'cm-gui-0' do
  fill_in('Title (required)',  :with => 'Post new question')
  within_frame 0 do
    editor = page.find_by_id('tinymce')
    editor.native.send_keys("Is the new question working?")
  end
  click_on 'Post'
  end
sleep(2)
end

Then(/^I should see the question posted$/) do
  within_frame 'cm-gui-0' do
 assert page.has_content?('Post new question')
  end
end

And(/^I submit invalid title$/) do
  within_frame 'cm-gui-0' do
    fill_in('Title (required)',  :with => ' ')
    click_on 'Post'
  end
  sleep(2)
end

Then(/^I should get validation error$/) do
  within_frame 'cm-gui-0' do
  assert page.has_content?('What is your question?')
 end
end

And(/^I submit blank description $/) do
  within_frame 'cm-gui-0' do
   fill_in('#tinymce',  :with => ' ')
  click_on 'Post'
  sleep(5)
  end
end

Then(/^I should get validation a error for description$/) do
  assert page.has_content?('')
end

When(/^I visit a quesiton  $/) do
  within_frame 'cm-gui-0' do
    click_on 'faculty question?'
  end
end


And(/^I click Answer button$/) do
  within_frame 'cm-gui-0' do
    click_on '+ Answer'
  end
end

And(/^I fill valid answer to a question$/) do
  within_frame 'cm-gui-0' do
    within_frame 0 do
      editor = page.find_by_id('tinymce')
      editor.native.send_keys("Is the new answer working?")

    end
    click_on 'Post Response'
  end
  sleep(2)
end

Then(/^I should see answer to the corresponding question$/) do
  within_frame 'cm-gui-0' do
  assert page.has_content?('Is the new answer working?')
    end
end


And(/^I submit blank answer to a question$/) do
  within_frame 'cm-gui-0' do
    within_frame 0 do
      editor = page.find_by_id('tinymce')
      editor.native.send_keys('')
    end
  end
  sleep(2)
end

And(/^I click “Upvote” button over a question$/) do
  sleep(5)
  within_frame 'cm-gui-0' do
    find('div[data-el="question.detail.upvote.button"]').click
  end
  sleep(10)
end

And(/^I click “Upvote” button over an answer$/) do
  sleep(5)
  within_frame 'cm-gui-0' do
    find('div[data-el="answer.list.upvote.button"]').click
  end
end

Then(/^ upvote count for question should increase by one$/) do
  within_frame 'cm-gui-0' do
    assert page.has_content?('1')
  end

end


Given(/^I have an answered question$/) do
  sleep(0)
  within_frame 'cm-gui-0' do
    find('div[class="question-container unread"]',  :text => 'student question?').click
  end
end

Then(/^ upvote count for  answer should increase by one$/) do
  within_frame 'cm-gui-0' do
    assert page.has_content?('1')
  end

end

When(/^I enter existing keyword in searchbox$/) do
  within_frame 'cm-gui-0' do
  fill_in('search for keywords or users',  :with => 'student')
  find('input[class="search-input ng-valid ui-autocomplete-input ng-dirty"]').native.send_keys(:return)
 end
 end

When(/^I enter existing keyword in searchbox and click on search$/) do
  within_frame 'cm-gui-0' do
    fill_in('search for keywords or users',  :with => 'student')
    click_on 'Search'
  end
end

Then(/^I should see the matching queries$/) do
  within_frame 'cm-gui-0' do
  assert page.has_content?('student')
  end

end

When(/^I enter non-existing keyword in searchbox$/) do
  within_frame 'cm-gui-0' do
    fill_in('search for keywords or users',  :with => 'Physics')
    find('input[class="search-input ng-valid ui-autocomplete-input ng-dirty"]').native.send_keys(:return)
   end
end

When(/^I enter non-existing keyword in searchbox and click on search$/) do
  within_frame 'cm-gui-0' do
    fill_in('search for keywords or users',  :with => 'Physics')
    click_on 'Search'
  end
end

Then(/^I should see the message$/) do
  sleep(0)
  within_frame 'cm-gui-0' do
    assert page.has_content?('No results for "Physics"')
  end
end

#question-container unread
When(/^I visit a question$/) do
  sleep(1)
  within_frame 'cm-gui-0' do
    find('div[class="question-container unread"]',  :text => 'student question?').click
  end
end

And(/^I submit blank description$/) do
  within_frame 'cm-gui-0' do
    within_frame 0 do
      editor = page.find_by_id('tinymce')
      editor.native.send_keys('')
    end
  end
  sleep(2)
end
And(/^I submit blank title$/) do
  within_frame 'cm-gui-0' do
    fill_in('Title (required)',  :with => '')
    click_on 'Post'
  end
  sleep(2)
end
Then(/^I should not be able to post answer$/) do
  within_frame 'cm-gui-0' do
    assert page.has_no_button? ('Post Response')
  end
  sleep(2)
end
Then(/^I should not be able to post question/) do
  within_frame 'cm-gui-0' do
    assert page.find('button[ng-click="submitQuestion(); newQuestion=false"]').disabled?
  end
  sleep(2)
end
And(/^I submit question without description$/) do
  within_frame 'cm-gui-0' do
    fill_in('Title (required)',  :with => 'Post new question')
   click_on 'Post'
  end
  sleep(2)
end
When(/^I visit a question of other person$/) do
  sleep(0)
  within_frame 'cm-gui-0' do
    find('div[class="question-container unread"]',  :text => 'student question?').click
  end
end

Then(/^upvote count for question should increase by one$/) do
  sleep(0)
  within_frame 'cm-gui-0' do
    assert page.first('div[data-el="question.detail.upvote.button"] span').text == '1'
  end
end
Then(/^upvote count for answer should increase by one$/) do
  sleep(10)
  within_frame 'cm-gui-0' do
    assert page.first('div[data-el="answer.list.upvote.button"] span').text == '1'
  end
end
Given(/^I have answered a question$/) do
  within_frame 'cm-gui-0' do
    find('div[class="question-container unread"]',  :text => 'student question?').click
    click_on '+ Answer'
    within_frame 0 do
      editor = page.find_by_id('tinymce')
      editor.native.send_keys("Is the new answer working?")
    end
    click_on 'Post Response'
  end
  sleep(5)
end

When(/^I delete the answer$/) do
  within_frame 'cm-gui-0' do
    within find('div[class="answer-super-container ng-scope"]',  :text=> 'Is the new answer working?') do
      find('div[class="delete"]').click
    click_on 'Delete'
    end

  end
end
Then(/^I should not be able to see the answer$/) do
  within_frame 'cm-gui-0' do
    assert page.has_no_content? ('Is the new answer working?')
  end
end
When(/^I edit the answer$/) do
  within_frame 'cm-gui-0' do
    within find('div[class="answer-super-container ng-scope"]',  :text=> 'Is the new answer working?') do
      find('div[class="edits"]').click
      within find('div[class="form-container"]') do
      # within_frame 0 do
      within_frame 0 do
        editor = page.find_by_id('tinymce')
        sleep(10)
        editor.native.send_keys("edited answer")
        sleep(10)
      end
      end
      click_on 'Post Response'
    end

  end
end
Then(/^I should see the edited answer$/) do
  sleep(10)
end
And(/^I click on Spam on a question$/) do
  within_frame 'cm-gui-0' do
    within find('div[class="question-detail-container"]',  :text=> 'student question?') do

      find('a[id="report-tag"]').click
  sleep(5)
   find('a[data-ea="report.spam"]',  :text=> 'Spam').click
      sleep(5)
end
    end
end
Then(/^the color of flag should change$/) do
  sleep(0)

end
And(/^I click on Spam on a answer$/) do
  within_frame 'cm-gui-0' do
    within find('div[class="answer-super-container ng-scope"]',  :text=> 'I is an answer') do
      find('a[id="report-tag"]').click
      sleep(0)
      find('a[data-ea="report.spam"]',  :text=> 'Spam').click
      sleep(0)
    end
  end
end
And(/^I submit question with tag$/) do
  within_frame 'cm-gui-0' do
    click_on '+ New Post'
    fill_in('Title (required)',  :with => 'Sample tag post')
    within_frame 0 do
      editor = page.find_by_id('tinymce')
      editor.native.send_keys("Is the new post working?")
      editor.native.send_keys("#creative")
     end
    click_on 'Post'
    sleep(10)
  end
end
Then(/^I should see the question posted with tag$/) do
  within_frame 'cm-gui-0' do
    assert page.has_content?('Sample tag post')
  end
end
And(/^I submit question with existing tag$/) do
  within_frame 'cm-gui-0' do
    click_on '+ New Post'
    fill_in('Title (required)',  :with => 'Sample tag post')
    within_frame 0 do
      editor = page.find_by_id('tinymce')
      editor.native.send_keys("Is the new post working? #t")
    end
    sleep(0)
    find('li[data-name="trial"]').click
    click_on 'Post'
    sleep(10)
  end
end
And(/^answer should note come in the list$/) do
  within_frame 'cm-gui-0' do
    assert page.has_content?('Post reported as inappropriate')
  end
end
When(/^I enter existing tag in searchbox and click on search$/) do
  within_frame 'cm-gui-0' do
    fill_in('search for keywords or users',  :with => '#test')
    click_on 'Search'
  end
end
Then(/^I should see the matching tag queries$/) do
  within_frame 'cm-gui-0' do
    assert page.has_content?('student question?')
  end
end
Then(/^I should see the message for tag$/) do
  sleep(0)
  within_frame 'cm-gui-0' do
    assert page.has_content?('No results for "#test123"')
  end
end
When(/^I enter non-existing tag in searchbox and click on search$/) do
  within_frame 'cm-gui-0' do
    fill_in('search for keywords or users',  :with => '#test123')
    click_on 'Search'
  end
end

When(/^I click on most recent$/) do
  within_frame 'cm-gui-0' do
    click_on 'most recent'
  end
end
Then(/^I should get the most recent question$/) do
  sleep(0)
end

When(/^I click on most upvoted/) do
  within_frame 'cm-gui-0' do
    click_on 'most upvoted'
  end
end
Then(/^I should get the most upvoted question$/) do
  within_frame 'cm-gui-0' do
    sleep(0)
  end
end

When(/^I click on most answers/) do
  within_frame 'cm-gui-0' do
    click_on 'most answers'
  end
end
Then(/^I should get the most answers question/) do
  within_frame 'cm-gui-0' do
    assert page.find('div[data-el="question.list.summary.container"] div[class="count text-center right ng-binding student-answer"]').text == '2'
  end
end
Then(/^I should see the list of questions with titles on left pane$/) do
  within_frame 'cm-gui-0' do
    sleep(0)
    assert page.has_content? ('Sample post')
    assert page.has_content? ('student question?')
    assert page.has_content? ('student votes?')
  end
end
And(/^I fill valid title and description$/) do
  within_frame 'cm-gui-0' do
    fill_in('Title (required)',  :with => 'Post cancel new question')
    within_frame 0 do
      editor = page.find_by_id('tinymce')
      editor.native.send_keys("Is the new question working?")
    end

  end
  sleep(2)
end
And(/^I click cancel$/) do
  within_frame 'cm-gui-0' do
    click_link 'Cancel'
  end
end
Then(/^I should reach landing page and question should not be posted$/) do
  within_frame 'cm-gui-0' do
    assert page.has_no_content?('Post cancel new question')
  end
end
And(/^I click on "(.*?)" over the question form$/) do |arg1|
  within_frame 'cm-gui-0' do
    click_link 'Close question'
  end
end
And(/^I fill valid response$/) do
  within_frame 'cm-gui-0' do
    within_frame 0 do
      editor = page.find_by_id('tinymce')
      editor.native.send_keys('cancel the new answer?')
    end
  end
  sleep(2)
end
Then(/^I reach to question page and answer should not be posted$/) do
  within_frame 'cm-gui-0' do
    assert page.has_no_content?('cancel the new answer?')
  end
end
And(/^I click to cancel an answer$/) do
  within_frame 'cm-gui-0' do
    find('a[data-el="answer.form.new.cancel.link"]').click
  end
end
Then(/^"(.*?)" button should be disabled$/) do |arg1|
  sleep(5)
  within_frame 'cm-gui-0' do
    assert page.find('div[data-el="question.detail.upvote.button"]')
    assert page.first('div[data-el="question.detail.upvote.button"] span').text == '0'
  end
  sleep(10)
end
When(/^I visit my posted question$/) do
  sleep(0)
  within_frame 'cm-gui-0' do
    find('div[data-el="question.list.summary.container"]',  :text => 'Sample post').click
  end
end
When(/^I visit my posted answer$/) do
  within_frame 'cm-gui-0' do
    find('div[data-el="question.list.summary.container"]',  :text => 'Sample post').click
  end
end
Then(/^"(.*?)" button for answer should be disabled$/) do |arg1|
  sleep(5)
  within_frame 'cm-gui-0' do

    assert page.find('div[data-el="answer.list.upvote.button"]')
    assert page.first('div[data-el="answer.list.upvote.button"] span').text == '0'

  end
  sleep(10)
end
Given(/^I have posted an answer$/) do
  within_frame 'cm-gui-0' do
    find('div[class="question-container read"]',  :text => 'Sample post').click
    click_on '+ Answer'
    within_frame 0 do
      editor = page.find_by_id('tinymce')
      editor.native.send_keys("answer for upvote")

    end
    click_on 'Post Response'
  end
end
When(/^I visit a question posted by faculty$/) do
  within_frame 'cm-gui-0' do
    find('div[data-el="question.list.summary.container"]',  :text => 'faculty question?').click
  end
  sleep(0)
end
Then(/^I could not see the option to report it$/) do
  assert page.has_no_link? ('report-tag')
end
When(/^I visit an answer posted by faculty$/) do
  within_frame 'cm-gui-0' do
    find('div[data-el="question.list.summary.container"]',  :text => 'student question?').click

  end
  sleep(0)
end
Then(/^I could not see the option to report an answer$/) do
  within_frame 'cm-gui-0' do
    sleep(0)
    within find('div[class="answer-div-container ng-scope"]',  :text=> 'faculty answer') do
      assert page.has_no_link? ('report-tag')
    end
  end
end
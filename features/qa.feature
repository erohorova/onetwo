Feature: 
  I use edcast forum

  Background:
    Given I visit forum wide mode page
    And I have created a question

  @javascript
  Scenario: See list of questions
    Then I should see the list of questions with titles on left pane
   
  @javascript
  Scenario: Post a new question
    When I click “New Post" button
    And I submit valid title and description
    Then I should see the question posted
  
  @javascript
  Scenario: Do not post a blank title for question
    When I click “New Post" button
    And I submit blank title
    Then I should not be able to post question

  @javascript
  Scenario: Error:Post an invalid title for question
    When I click “New Post" button
    And I submit invalid title
    Then I should get validation error

  @javascript
  Scenario: Do not post a blank title & description for question
    When I click “New Post" button
    And I submit blank description
    Then I should not be able to post question

  @javascript
  Scenario: Post a question without description
    When I click “New Post" button
    And I submit question without description
    Then I should see the question posted
  
  @javascript
  Scenario: Cancel posting of a question using cancel button
    When I click “New Post" button
    And I fill valid title and description
    And I click cancel
    Then I should reach landing page and question should not be posted

  @javascript
  Scenario: Cancel posting of a question using "x"
    When I click “New Post" button
    And I fill valid title and description
    And I click on "x" over the question form
    Then I should reach landing page and question should not be posted

  @javascript
  Scenario: Post Answer to a question
    When I visit a question
    And I click Answer button
    And I fill valid answer to a question
    Then I should see answer to the corresponding question

  @javascript
  Scenario: Do not post blank Answer to a question
    When I visit a question
    And I click Answer button
    And I submit blank answer to a question
    Then I should not be able to post answer

  @javascript
  Scenario: Cancel posting of an answer using cancel button
    Given I visit a question
    When I click Answer button
    And I fill valid response
    And I click to cancel an answer
    Then I reach to question page and answer should not be posted

  @javascript
  Scenario: Upvote a question
    When I visit a question of other person
    And I click “Upvote” button over a question
    Then upvote count for question should increase by one

  @javascript
  Scenario: Do not upvote my posted question
    When I visit my posted question
    Then "Upvote" button should be disabled

  @javascript
  Scenario: Upvote an answer
    Given I have an answered question
    When I click “Upvote” button over an answer
    Then upvote count for answer should increase by one

  @javascript
  Scenario: Do not upvote my posted answer
    Given I have posted an answer
    When I visit my posted answer
    Then "Upvote" button for answer should be disabled

  @javascript
  Scenario: Search an existing keyword
    When I enter existing keyword in searchbox
    Then I should see the matching queries

  @javascript
  Scenario: Search a non-existing keyword
    When I enter non-existing keyword in searchbox
    Then I should see the message

  @javascript
  Scenario: Search an existing keyword using search button
    When I enter existing keyword in searchbox and click on search
    Then I should see the matching queries

  @javascript
  Scenario: Search a non-existing keyword using search button
    When I enter non-existing keyword in searchbox and click on search
    Then I should see the message

  @javascript
  Scenario: Search an existing tag using search button
    When I enter existing tag in searchbox and click on search
    Then I should see the matching queries

  @javascript
  Scenario: Search a non-existing tag using search button
    When I enter non-existing tag in searchbox and click on search
    Then I should see the message

  @javascript
  Scenario: Delete an answer
    Given I have answered a question
    When I delete the answer
    Then I should not be able to see the answer

  @javascript
  Scenario: Edit an answer
    Given I have answered a question
    When I edit the answer
    Then I should see the edited answer

  @javascript
  Scenario: Report a question
    When I visit a question
    And I click on Spam on a question
    Then the color of flag should change

  @javascript
  Scenario: Report an answer
    When I visit a question
    And I click on Spam on a answer
    Then the color of flag should change
    And answer should note come in the list
  
  @javascript
  Scenario: Do not report faculty posted question
    When I visit a question posted by faculty
    Then I could not see the option to report it

  @javascript
  Scenario: Do not report faculty posted answer
    When I visit an answer posted by faculty
    Then I could not see the option to report an answer

  @javascript
  Scenario: Add a new tag to a question
    When I click “New Post" button
    And I submit question with tag
    Then I should see the question posted with tag

  @javascript
  Scenario: Add an existing tag to a question
    When I click “New Post" button
    And I submit question with existing tag
    Then I should see the question posted with tag

  @javascript
  Scenario: Get the most recent question
    When I click on most recent
    Then I should get the most recent question

  @javascript
  Scenario: Get the most upvoted question
    When I click on most upvoted
    Then I should get the most upvoted question

  @javascript
  Scenario: Get the most answers question
    When I click on most answers
    Then I should get the most answers question

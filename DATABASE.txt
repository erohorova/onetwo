CREATE DATABASE `edcast_development` CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL ON `edcast_development`.* TO `rails`@localhost IDENTIFIED BY 'railspass';
FLUSH PRIVILEGES;

CREATE DATABASE `edcast_test` CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL ON `edcast_test`.* TO `rails`@localhost IDENTIFIED BY 'railspass';
FLUSH PRIVILEGES;
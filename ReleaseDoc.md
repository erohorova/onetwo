# Upcoming release v2.57.0

### Database Migrations

### Rake Tasks(Pre-release)
- EP-17782
  Description: Support livestreams with AWS SNS & Transcoder.
  NY region: Add ENV variable `TRANSCODER_REGION` with a value as `us-east-1` in beanstalk + ECS environments.
  EU region: Add ENV variable `TRANSCODER_REGION` with a value as `eu-west-1` in beanstalk + ECS environments.
  EU region: Add ENV variable `AWS_TRANSCODER_PIPELINE_ID` with a value as `1551776913893-2s1wrh` in beanstalk + ECS environments.
  EU region: Add ENV variable `AWS_TRANSCODER_HLS_PRESET_ID` with a value as `1551777846678-wzdoyc,1351620000001-200040,1351620000001-200050` in beanstalk + ECS environments.

### Rake Tasks(Post-release)

### After party tasks


-----------------------------------------------------------------------------------------

# Upcoming release v2.56.0

### Database Migrations
- EP-23053 <br>
  Description: Add table widgets<br>
  File: https://github.com/edcast/edcast/blob/3209b2628174b2058529a71971f505482f9ca4bb/db/migrate/20190221042354_create_widgets.rb

### Rake Tasks(Pre-release)

### Rake Tasks(Post-release)
- EP-23470 <br>
  Description: Reindex Users where role names have got changed from default names
  TO BE RUN ON EU / SANDBOX
  File: https://github.com/edcast/edcast/blob/aa85eec84fdd3ca184e719bbd84eb4323d650af0/lib/tasks/reindex_users.rake
  Command: ` rake 'reindex_users:whose_role_names_differ_from_default_name[100010 100005]' `

- EP-23326 <br>
  Description: Added new JSON design for edcast-team-invite email template<br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/update_default_templates.rake <br>
  `rake update_default_templates:design`

- EP-22763 <br>
  Description: Added new permission to `admin` group.   
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/update_permissions.rake
  `rake update_permissions:admin_role_permissions`

### After party tasks

- EP-20091 <br>
  Description: Reindex channels having teams_channels_follows record<br>
  File: https://github.com/edcast/edcast/blob/6c38d1fe511de192a5a16d99ce09d6a7315ddfc4/lib/tasks/deployment/20190220110005_reindex_channels_for_teams_channels_follows_record.rake


-----------------------------------------------------------------------------------------

# Upcoming release v2.55.0

### Database Migrations
- EP-20370 <br>
  Description: Added expiry_date field in SkillsUser<br>
  File: https://github.com/edcast/edcast/blob/master/db/migrate/20190130112931_add_expiry_date_to_skills_user.rb

- EP-23072 <br>
  Description: Added dashboard_info field in UserProfile<br>
  File: https://github.com/edcast/edcast/blob/master/db/migrate/20190208081624_add_dashboard_info_to_user_profile.rb

### Rake Tasks(Pre-release)

### Rake Tasks(Post-release)
- EP-22392 <br>
  Description: Remove added_channel_follower template from the list of customizable emails<br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/update_default_templates.rake <br>
  `rake update_default_templates:remove_custom_email_type_for_all['added_channel_follower']`

- EP-23710 <br>
  Update image url for badges(ClcBadge and CardBadge) <br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/update_image_url_for_badges.rake <br>
  `rake badges:update_image_url_for_custom_badges`

### Console Scripts

- EP-21551-c <br>
    Description: Switch on `workflow_service` config for a particular organizations, at least for deloitte, deloitestage, perf and edcastinternal<br>
    Script:
    ```
    org_ids = [<ids>]
    org_ids.each do |org_id|
        org = Organization.find_by(id: org_id)
        next unless org
        config = Config.where(name: 'workflow_service', configable: org).first_or_initialize do |s|
          s.value = 'true'
          s.category = 'settings'
          s.data_type = 'boolean'
        end
        config.save
    end
    ```
-----------------------------------------------------------------------------------------

# Upcoming release v2.54.0

### Rake Tasks(Pre-release)


### Rake Tasks(Post-release)
- EP-22698 <br>
  Description: Added new permission CAN_RATE.<br>
  File: https://github.com/edcast/edcast/blob/5a3477a89e4386415b9fd50a1a714ae96adffea8/lib/tasks/add_permission_can_rate.rake
  `rake add_permission_can_rate`

- EP-20455 <br>
  Description: Updated JSON design for content_shared custom template<br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/update_default_templates.rake <br>
  `rake update_default_templates:design`

# Upcoming release v2.53.0

### Rake Tasks(Pre-release)


### Rake Tasks(Post-release)
- EP-20788 & EP-20430 & EP-21682 <br>
  Description: Added new design files for custom templates.<br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/update_default_templates.rake <br>
  `rake update_default_templates:design`

- EP-22633 <br>
  Description: To remove irrelevant dynamic selection groups<br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/dynamic_selection_groups.rake <br>
  `rake dynamic_selection_groups:remove`

---------------------------------------------------------------------------------------------------------------------------------------------

# Upcoming release v2.52.0

### Rake Tasks(Pre-release)
- EP-20678 <br>
  Description: Add new fields to user mappings in ES with correct analyzer.<br>
  **This must be done before deploy.**<br>
  `rake update_mappings:put_searchable_skills`

### Rake Tasks(Post-release)
- EP-20678 <br>
  Description: Reindex users who have expert topics.<br>
  `rake reindex_users:reindex_users_with_expert_topics`

- EP-22021<br>
  Description: Add cleanup task for custom carousal
  `rake discover:cleanup_config_when_structures_get_deleted`

---------------------------------------------------------------------------------------------------------------------------------------------
# Upcoming release v2.51.0

### Post release
- EP-20433<br>
  Description: Dismiss mentioned assignments for team 'WIPRO - ALL' (Approximately 135355 assignments on production)<br>
  `rake "one_time_tasks:dismiss_assignment[1813,3802,2500635 2602132 2627436 2627453 2751749 2945894 2941512 2854746]"`

### Database Migrations
- EP-20905<br>
  Description: Created table `workflow_patch_revisions`
  File: https://github.com/edcast/edcast/blob/master/db/migrate/20181120084652_create_workflow_patch_revisions.rb

- EP-21414<br>
  Description Add job_title column to `user_profiles`
  File: https://github.com/edcast/edcast/blob/master/db/migrate/20181205113705_add_job_title_to_user_profiles.rb

---------------------------------------------------------------------------------------------------------------------------------------------

# Upcoming release v2.50.0

### Deployment Dependencies

### Database Migrations
1. EP-15934<br>
 Description: Add a column to Teams Table. Default value : false
 File: https://github.com/edcast/edcast/blob/master/db/migrate/20181022061152_add_only_admin_can_post_to_teams.rb

- EP-20118
  Description: changed `abbreviation` and `display_name` columns limits for `customs_fields` table
  File: https://github.com/edcast/edcast/blob/master/db/migrate/20181018144516_change_abbreviation_limit.rb

- EP-11004
  Description: add the column `language` to `cards` table
  File: https://github.com/edcast/edcast/blob/master/db/migrate/20181002105059_add_language_to_card.rb
  Description: add `index` to card language
  File: https://github.com/edcast/edcast/blob/master/db/migrate/20181011122947_add_index_to_card_language.rb

### After Party Tasks(Pre-release)

### Rake Tasks(Post-release)
- EP-18087<br>
  Description: Create a config for encryption_payload.<br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/create_encryption_config_for_organization.rake
  `rake create_encryption_config_for_organization:encryption_config[:organization_id];`
  For ICICI organization , pass ICICI org id

- EP-20106<br>
  Description: Change default label for My Shared Cards and default value true.<br>
  `rake update_configs_org_customization_config:update_my_shared_cards_for_mobile`

- EP-20531<br>
  Description: Configuration not working on enabling mobile-new-me-tab-ui on any org.<br>
  `rake update_configs_org_customization_config:remove_meTabV2_if_null`

### Console Scripts

### Environment Variables
* EP-18087<br>
  Description: Add public and private key to environment variables to encrypt and decrypt payload <br>

  **JS_PUBLIC_KEY_RAILS=value**

  **JS_PRIVATE_KEY=value**

### Sidekiq Queues to be added

### Pre Release


### Post release
- EP-19786<br>
  Description: Fix existing data for pubnub channels in ES.<br>
  `rake users:reindex_users_for_pubnub`


---------------------------------------------------------------------------------------------------------------------------------------------


# Upcoming release v2.49.0

### Deployment Dependencies

### Database Migrations
- EP-16917-https://jira.edcastcloud.com/browse/EP-16917
  Description: Added completed_percentage in UserContentCompletion.
  File: db/migrate/20180929064701_add_completed_percentage_to_user_content_completion.rb

- EP-19040-https://jira.edcastcloud.com/browse/EP-19040
  Description: Added dynamic_group_revisions table
  File: https://github.com/edcast/edcast/blob/master/db/migrate/20180913113704_create_dynamic_group_revisions.rb

### After Party Tasks(Pre-release)

- EP-17986<br>
  Description: Update mappings for users' expert topics.<br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/deployment/20180919082609_update_mappings_for_skills_and_reindex.rake

### Rake Tasks(Post-release)
- EP-19467<br>
  Description: Update default templates design fields with proper JSON content.<br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/update_default_templates.rake
  `rake update_default_templates:design`

- EP-19494<br>
  Description: Update Org customization config for mobile<br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/update_configs_org_customization_config.rake
  `rake update_configs_org_customization_config:update_me_tab_config_for_mobile`

- EP-19430<br>
  Description: Update channel topics from two level to one level for nasscom.<br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/migrate_taxonomy_topics.rake
  `rake migrate_taxonomy_topics:migrate_channel_topics ORG_ID=<<organization_id>> TAXO_URL=<<Taxonomy_url>>`

- EP-19429<br>
  Description: Update learing interest of users from two level to one level for nasscom.<br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/migrate_taxonomy_topics.rake
  `rake migrate_taxonomy_topics:migrate_user_learning_topics ORG_ID=<<organization_id>> TAXO_URL=<<Taxonomy_url>>`

- EP-17986<br>
  Description: Reindex visible users not from default org with expert topics.<br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/reindex_users.rake
  `rake reindex_users:reindex_users_with_expert_topics`

- EP-20544<br>
  Description: Added rake task remove duplicates for external profile.<br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/remove_duplicates_for_external_profile.rake
  `rake profile:remove_duplicates_for_external_profile`

### Console Scripts

### Environment Variables

### Ruby Gems(Added/Deleted/Updated)

### Sidekiq Queues to be added

### Pre Release

- EP-19409<br>
  Description: Consolidate ENV variables on LXP backend API beanstalk (We can add environment variables to environment_variables file and upload it to S3)<br>
  Upload the following file to the existing bucket - `edcast-production-paperclip-store`<br>
  Filename : `environment_variables`<br>

### Post release
- EP-19550
  Description: Removes the duplicates across teams_users, follows, user_profiles, user_onboardings, and user_roles. Only then, the rake will add unique index across these tables.
  `rake cleanup_records_and_add_indices:run`

---------------------------------------------------------------------------------------------------------------------------------------------

# Upcoming release v2.48.0

### Deployment Dependencies
- EP-16500, EP-16500<br>
  Description: following rakes have to be triggered manually before deployment and in sequence
  1. rake to copy default_name from name<br>
  https://github.com/edcast/edcast/blob/master/lib/tasks/update_default_role.rake
  2. rake to add 'MARK_AS_PRIVATE' permission to member role<br>
  https://github.com/edcast/edcast/blob/EP-16500/lib/tasks/add_permission.rake
  3. rake to mark cards private in private channel(only for hpe/schneider)<br>
  https://github.com/edcast/edcast/blob/master/lib/tasks/private_channel_cards.rake
  4. rake to mark 'private_content' true for ecl_source of private channel(only for hpe/schneider)<br>
  https://github.com/edcast/edcast/blob/EP-16550-admin/lib/tasks/private_channel_ecl_source.rake

### Database Migrations
- EP-17867<br>
  Description: Add `tac_accepted_at` column to `user_profiles` table.
  File: https://github.com/edcast/edcast/blob/a92595acba571476935bcc9696125a6c6c518ad7/db/migrate/20180906163032_add_tac_accepted_at_column_to_user_profiles.rb

- EP-18630<br>
  Description: Add `sign_out_at` to `users` table<br>
  File: https://github.com/edcast/edcast/blob/73afb422b16aae435a47c01b1b22b1c6e39e97d1/db/migrate/20180907083314_add_column_sign_out_at_to_users.rb<br>

- EP-18629<br>
  Description: Add `welcome_token` column to `users` table.
  File: https://github.com/edcast/edcast/blob/8b8da8a3d8d5f52b2631973d9f20faf7c1251ee3/db/migrate/20180905073033_add_welcome_token_to_user.rb

- EP-16550<br>
  Description: added private_content column in ecl_sources with default value false
  File: https://github.com/edcast/edcast/blob/EP-16550-admin/db/migrate/20180906091848_add_private_content_to_ecl_source.rb

- EP-17370<br>
  Description: Add `hide_from_leaderboard` column to `users` table.
  File: https://github.com/edcast/edcast/blob/2078a8c6806e74b4111ffe272f24c78aa3bfe94b/db/migrate/20180905064845_add_hide_from_leaderboard_column_to_users.rb

- EP-18086<br>
  Description: Add `is_default` and `image_original_url` columns to `badges` table
  Files: https://github.com/edcast/edcast/blob/0facd03c02e3e53a1a7507518801150a3a829084/db/migrate/20180815091218_add_image_original_url_to_badges.rb and
  https://github.com/edcast/edcast/blob/0facd03c02e3e53a1a7507518801150a3a829084/db/migrate/20180815091218_add_image_original_url_to_badges.rb

- EP-17337<br>
  Description: Remove `review_started` columns from `cards` table
  Files: https://github.com/edcast/edcast/blob/d8e3f6a6d7d463ccbb3986be7c031e5a25e648b5/db/migrate/20180911080927_remove_review_started_from_cards.rb

- EP-19670<br>
  Description: update duplicate handles and added unique Constraint on handle in users the table
  Files: https://github.com/edcast/edcast/blob/43af7ee3f58a7fdb91a9e1e74718c518fcb83f07/db/migrate/20181004104926_cleanup_duplicate_handles_users.rb and
  https://github.com/edcast/edcast/blob/43af7ee3f58a7fdb91a9e1e74718c518fcb83f07/db/migrate/20181004104947_add_uniqueness_constraint_on_handle_to_users.rb

### After Party Tasks(Pre-release)
- EP-17768<br>
  Description: update default label courses in channel configs, replacing the letter `C` which was not english.<br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/deployment/20180921085032_update_default_label_courses_in_channel_configs.rake

- EP-12345-Sample<br>
  Description: create external_profile for users who do not have it, setting uid and external_id if empty<br>
  NOTE: it will take a lot of time in production ~ 18-20 minutes
  File: https://github.com/edcast/edcast/blob/5ce815ee19381014218f3e2c4e8ab67b473b1658/lib/tasks/deployment/20180821105926_set_uid_and_external_id_for_old_external_profile.rake

- EP-18086<br>
  Description: set is_default true for old badges<br>
  File: https://github.com/edcast/edcast/blob/0ac9bf4a00fb5425f68b47f806dcf8b1f00a79e7/lib/tasks/deployment/20180815095347_set_for_old_badges_is_default_true.rake

- EP-19037<br>
  Description: replacing smartbite to smartcard in default email templates<br>
  File: https://github.com/edcast/edcast/blob/1253d44c5d0280a1e34dc32f0afa65f865d9a76c/lib/tasks/deployment/20180917084131_replacing_smartbite_to_smartcard_in_emailtemplates.rake

- EP-12840<br>
  Description: reindex users_custom_fields data<br>
  File: https://github.com/edcast/edcast/blob/f32c7d697d8e95ad46559e115d0829d15abf06ca/lib/tasks/deployment/20180904125531_reindex_users_custom_fields_data.rake

### Rake Tasks(Post-release)
- EP-18571<br>
  Description: Update transaction_data of existing records in transaction_audit table to maintain consistency of data<br>
  File: https://github.com/edcast/edcast/blob/d7b38170ca03d36607eb1c3962f81165cb4e28cd/lib/tasks/update_transaction_data_existing_transaction_audit_records.rake<br>
  `rake transaction_audit:update_transaction_data_existing_records`

- EP-18565<br>
Description: Resize existing user uploaded images
File: https://github.com/edcast/edcast/blob/d2c43257c77bc4436efe110bfd070b9d1dcd997e/lib/tasks/resize_user_uploaded_images.rake
`rake resize_user_upload_images:resize_filestack_images > <file_name_1>`
`rake resize_user_upload_images:resize_resource_whose_image_changed > <file_name_2>`
Note: The file contains ids of cards and resources which filestack api gets failed  & which will have to be updated post production. This requires manual intervention and updation.


### Console Scripts

### Environment Variables
- EP-18565<br>
  Add environment variables for filestack API and APP_secret
  FILESTACK_API_KEY (needed from now on)
  FILESTACK_API_SECRET (one time task; can be removed after the rake task runs)

### Ruby Gems(Added/Deleted/Updated)
- EP-18630<br>
  Description: Added gems for development and test groups only<br>
  Gem **m** and gem **pry**

- EP-18442<br>
  Description: To check type and size of image
  **fastimage**

### Sidekiq Queues to be added
- EP-18442<br>
  `filestack_image_processing`

- EP-18565<br>
  `existing_filestack_images`
  `existing_resource_images`

### Post release

- EP-18565:
  Manually modifying the card_ids and resource_ids obtained from rake task will be modified post release

---------------------------------------------------------------------------------------------------------------------------------------------

# Upcoming release v2.47.0

### Deployment Dependencies

### Database Migrations
- EP-15736
  Description: added auto_pin_cards column in channels with default value false
  File: https://github.com/edcast/edcast/blob/913a06f077e9b798c4d19f12ad10628874bd51fc/db/migrate/20180823103814_add_auto_pin_cards_to_channels.rb

- EP-18069
  Description: added is_mandatory column in teams with default value false
  File: https://github.com/edcast/edcast/blob/master/db/migrate/20180813082643_add_mandatory_to_team.rb

### After Party Tasks(Pre-release)
- EP-18281<br>
  Description: Add leaderboard config for backfilling<br>
  File: https://github.com/edcast/edcast/blob/79c6b247e2cafa405050f38771e402e354586c5c/lib/tasks/deployment/20180821083654_create_config_for_leaderboard_for_all_orgs.rake

- EP-15736
  Description: Enable auto_pin_cards of channels for orgs where the current automated_pinned_cards is enabled
  File: https://github.com/edcast/edcast/blob/913a06f077e9b798c4d19f12ad10628874bd51fc/lib/tasks/deployment/20180824110720_update_channels_automated_pinned_cards_config.rake

- EP-17604<br>
  Description: Reindex channels with superadmins followers
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/deployment/20180822124835_reindex_channels_with_superadmins_followers.rake

- EP-17806<br>
  Description: remove permissions_manage_channels and related permissions for sub_admin and group_sub_admin<br>
  File: https://github.com/edcast/edcast/blob/47173a8f6e245785043aad558d6f4a2d851a55a8/lib/tasks/deployment/20180816100807_remove_permissions_with_channels_for_sub_admin.rake

- EP-16201<br>
  Description: add new role trusted_collaborator<br>
  File: https://github.com/edcast/edcast/blob/7f88f9836fcc31ec327c6a7c7a641c1966cd695c/lib/tasks/deployment/20180817173855_add_new_role_trusted_collaborator.rake

### Rake Tasks(Post-release)

- EP-15769<br>
  Description: Update video card if video URL is nil<br>
  `rake cards:update_card_type ORG_ID=123`

- EP-17922<br>
  Description: cleanup team invitation.
  File:https://github.com/edcast/edcast/blob/EP-17922/lib/tasks/cleanup_team_invitation.rake
  `rake one_time_tasks:cleanup_team_invitation`

- EP-17603<br>
  Description: reindex teams
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/reindex_teams.rake
  `rake reindex_teams`

  need to run above rake for Organization `dellsandbox`
  NOTE: This rake task will be run one time to fix problem with ES data in Team's index.


### Environment Variables

### Sidekiq Queues to be added

---------------------------------------------------------------------------------------------------------------------------------------------

# Upcoming release v2.46.0

### Deployment Dependencies
- EP-17165<br>
  Description: Deploy ECL first for Search based on Paid/Free i.e is_paid status.

### Database Migrations
- EP-12345-Sample<br>
  Description: Add card metadata table<br>
  File: https://github.com/edcast/edcast/blob/release-v2.42.0/db/migrate/20180504054828_create_card_metadata.rb

- EP-15943<br>
  Description: Change default value of `content` in `email_templates` table<br>
  File: https://github.com/edcast/edcast/blob/master/db/migrate/20180719144312_email_templates_change_column_null.rb

- EP-17464<br>
  Description: Add `is_dynamic` field to `teams` table<br>
  File: https://github.com/edcast/edcast/blob/master/db/migrate/20180801104434_add_is_dynamic_to_teams.rb

### After Party Tasks(Pre-release)
- EP-12345-Sample<br>
  Description: Change card type of scorm cards<br>
  File: https://github.com/edcast/edcast/blob/release-v2.42.0/lib/tasks/deployment/20180427092152_convert_cardtype_scorm_cards_to_course.rake

- EP-13917<br>
  Description: Create a new elasticsearch index for Card Reporting
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/deployment/20180730054747_create_index_and_index_card_reporting.rake

- EP-17205<br>
  Description: Create ES index for saving card versions<br>
  File: https://github.com/edcast/edcast/blob/aec3d95a49c42bc533a04428a0dc36adce7029a6/lib/tasks/deployment/20180801151515_create_es_indexes_for_versioning.rake

- EP-17165<br>
  Description: Propogate the paid cards data, to update is_paid column in ECL
  File: https://github.com/edcast/edcast/blob/3f9461f8d8f6d4528a721923ba74540734621d92/lib/tasks/deployment/20180807091734_sync_to_ecl_cards_having_paid_plans.rake

- EP-17694<br>
  Description: Populate organizations with default email templates
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/deployment/20180802100404_seed_default_templates.rake

- EP-17303<br>
  Description: Remove accepted invitations for teams
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/deployment/20180726071802_remove_accepted_invitations_for_teams.rake

- EP-17284<br>
  Description: Destroy default role `external`
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/deployment/20180720082633_destroy_default_role_external.rake

- EP-17284<br>
  Description: Create profiles for users
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/deployment/20180720104732_create_profiles_for_users.rake

### Rake Tasks(Post-release)
- EP-12345-Sample<br>
  Description: Update content_item of pathways and hidden cards<br>
  File: https://github.com/edcast/edcast/blob/release-v2.42.0/lib/tasks/update_content_item.rake<br>
  `rake content_item:update_content_item_for_pathways_and_hidden_cards`

- EP-16630<br>
  Description: delete identity providers of a org users where Okata is enable.
  File:https://github.com/edcast/edcast/blob/EP-16630/lib/tasks/delete_identity_providers_for_org.rake
  `rake one_time_tasks:delete_identity_providers_for_org host_name=org_host_name` or
  `rake one_time_tasks:delete_identity_providers_for_org user_id=user_ids`

  need to run above rake for Organization `skillset`
  NOTE: This rake task will be run on demand, as and when need arises.

- EP-14285<br>
  Description: Change content type for brainshark contents<br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/change_card_type_for_brainshark_contents.rake<br>
  `rake one_time_tasks:change_card_type_for_brainshark_contents`

- EP-15795<br>
  Description: reindex teams
  File:https://github.com/edcast/edcast/blob/EP-15795/lib/tasks/reindex_teams.rake
  `rake reindex_teams`


- EP-13917<br>
  `card_reporting`

- EP-17205<br>
  `versioning`

- EP-17603<br>
  `reindex_urgent`

### Environment Variables
* EP-16763<br>
  Description: Add env variable for china region push notification(pushy)<br>
  **PUSHY_URL:https://api.pushy.me/push**
  **PUSHY_API_KEY:f2cfbcf68b6fcf4c3008e055b3cc70f19de09344153bcfaa6dd5b3d239cef657**

### Ruby Gems(Added/Deleted/Updated)
- EP-12345-Sample<br>
  Description: Why gem was added/deleted/updated<br>
  **GEM NAME**

### Third Party Dependencies(e.g. database/api)
- EP-12345-Sample<br>
  Description: any new connection to any database/any api<br>

### Post Release

- EP-16403<br>
  Description: Console commands to set source_name and order_type in transactions table<br>

  ```
  @transactions = Transaction.joins(:order).where("transactions.order_type is null")
  @transactions.each do |transaction|
    order = transaction&.order
    args = {}
    args[:order_type] = order.orderable_type
    args[:source_name] = order&.orderable&.ecl_metadata&.dig('source_type_name') if (order.orderable_type == "Card")
    transaction.update_columns(args)
  end
  ```

---------------------------------------------------------------------------------------------------------------------------------------------

# Upcoming release v2.45.0

### Deployment Dependencies

- EP-12345-sample<br>
  Deploy ECS first since we are adding Team Index to ES and indexing all teams from non default org(3558 teams)


### Database Migrations

- EP-16453<br>
  Description: Migration to create table comment_reportings
  File:https://github.com/edcast/edcast/blob/ff312cf035e4b8ad62a6d277beb4ac06f78ad18a/db/migrate/20180628051529_create_comment_reporting.rb

- EP-16454<br>
  Description: Migration to add `deleted_at` in `comments` <br>
  File: https://github.com/edcast/edcast/blob/a93f866d742a4571d1acb3dd8d8eadf447b707e2/db/migrate/20180702065848_add_deleted_at_to_comments.rb

- EP-16454<br>
  Description: Migration to add `deleted_at` in `activity_streams` <br>
  File: https://github.com/edcast/edcast/blob/a93f866d742a4571d1acb3dd8d8eadf447b707e2/db/migrate/20180702071857_add_deleted_at_to_activity_streams.rb

- EP-16454<br>
  Description: Migration to add `deleted_at` in `votes` <br>
  File: https://github.com/edcast/edcast/blob/a93f866d742a4571d1acb3dd8d8eadf447b707e2/db/migrate/20180702075014_add_deleted_at_to_votes.rb

- EP-16454<br>
  Description: Migration to add `deleted_at` in `mentions` <br>
  File: https://github.com/edcast/edcast/blob/a93f866d742a4571d1acb3dd8d8eadf447b707e2/db/migrate/20180702080122_add_deleted_at_to_mentions.rb

- EP-16454<br>
  Description: Migration to add index on `deleted_at` in `comments`, `activity_streams`, `votes`  and `mentions` <br>
  File: https://github.com/edcast/edcast/blob/a93f866d742a4571d1acb3dd8d8eadf447b707e2/db/migrate/20180705061552_add_index_on_deleted_at_for_comment_activity_stream_vote_mention.rb

- EP-15940<br>
  Description: Migration added new column `design` to email_templates table <br>
  File: https://github.com/edcast/edcast/blob/a93f866d742a4571d1acb3dd8d8eadf447b707e2/db/migrate/20180703140620_add_column_to_email_templates.rb

- EP-16690<br>
  Description: Migration added new column `is_private` to teams table <br>
  File: https://github.com/edcast/edcast/blob/a93f866d742a4571d1acb3dd8d8eadf447b707e2/db/migrate/20180703083536_add_is_private_to_teams.rb

### After Party Tasks(Pre-release)

- EP-16422<br>
  Description: Cleaning up card_pack_relations wrong relation mappings.<br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/deployment/20180629103018_update_card_pack_relation_mapping_of_deleted_cards.rake

- EP-16690<br>
  Description: Setting field `is_private` to `true` value for old teams.<br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/deployment/20180704081840_set_is_private_true_for_old_teams.rake

- EP-16664<br>
  Description: Removing not using (old) permissions for sub-admin.<br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/deployment/20180702132456_remove_old_sub_admin_permissions.rake

- EP-16161<br>
  Description: Reindex teams.<br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/deployment/20180711103054_teams_reindex.rake

- EP-16599<br>
  Description: Reindex channels.<br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/deployment/20180710070944_channels_reindex.rake

- EP-15647<br>
  Description: Add `custom_fields` and `joined_date` mapping to users.<br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/deployment/20180702075929_add_mapping_to_users.rake<br>
  Description: Reindex users with `custom_fields`.<br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/deployment/20180702080328_reindex_users_with_custom_fields.rake

- EP-12300<br>
  Description: Update `content_type` for_existed journeys on ecl.<br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/deployment/20180613083340_update_journeys_on_ecl.rake

### Rake Tasks(Pre-release)

* EP-12345-sample<br>
  Description: Update discovery page providers config for futureskills <br>
  File: https://github.com/edcast/edcast/blob/master/lib/tasks/sample.rake<br>

* EP-13828<br>
  Description: Rake task to set card metadata for pathway and journeys<br>
  File: https://github.com/edcast/edcast/blob/632bf70b940b3bb065beee9aa59c430708c7fcc9/lib/tasks/migrate_card_metadata_for_pack_journey.rake<br>
  `rake migrate_card_metadata_for_pack_journey:set_metadata`

* EP-15932<br>
  Description: Rake task to update permissions for curators and collaborators<br>
  File: https://github.com/edcast/edcast/blob/632bf70b940b3bb065beee9aa59c430708c7fcc9/lib/tasks/update_permissions.rake<br>
  `rake update_permissions:collaborator_role_permission`
  `rake update_permissions:curator_role_permissions`


### console Script:

  * script to reset the channels_cards rank for organization LD flag off for reordering.

  channels = Channel.where.not(organization_id: "organization for which LD flag is on for reodering of channels card")

  channels.find_each do |channel|

    channels_cards = ChannelsCard.where(channel_id: channel.id)
    channels_cards.update_all(rank: nil)
  end

### Environment Variables

* EP-12345-Sample<br>
  Description: Add env variable for database<br>
  **KEY:VALUE**

* EP-13808<br>
  Description: Add env variable for storing those org hostnames for which image clean up is required. Incase limit has reached, we may replace AUTOFOLLOW_HANDLES as it is not used now<br>
  **ORG_HOSTNAMES_FOR_URL_CLEANUP:genpact**

### Ruby Gems(Added/Deleted/Updated)

* EP-12345-Sample<br>
  Description: Why gem was added/deleted/updated<br>
  **GEM NAME**

* EP-14471<br>
  Description: Upgraded doorkeeper gem as previous version had security vulnerability<br>
  **doorkeeper**

### Third Party Dependencies(e.g. database/api)

* EP-12345-Sample<br>
  Description: any new connection to any database/any api


### Sidekiq Queues to be added

* EP-12345-sample<br>
  scorm_upload_status

* EP-13808<br>
  update_images

### Post release

- EP-16046
  add language column to the sample CSV file.
  URL is `https://s3.amazonaws.com/ed-general/bulk_import_sample.csv`
  S3 bucket is `ed-general`.

- EP-16676
  Description: Sync organization_role column of user with RBAC
  File: https://github.com/edcast/edcast/blob/ba9f617/lib/tasks/sync_user_roles.rake
  `rake roles:sync_user_role`
  NOTE: This rake task will be run on demand, as and when need arises.

//= require sdk/init
fixture.preload("fixture.html");
describe('CM.UI', function() {

    beforeEach(function() {
        spyOn(CM.UI.prototype, 'render');

        this.fixtures = fixture.load("fixture.html", true);
        CM.init(document);
    });

    it('should create iframe element', function(){
        // should have ui, returnUrl, numPosts, orderBy
        
        //wrong to use the host and port of the current window. needs to come from the configuration.
        //expect(CM.WIDGETS[0].ui.uiElement.src).toContain(window.top.location.hostname + ':' + window.top.location.port);
        
        expect(CM.WIDGETS[0].ui.uiElement.name).toEqual(CM.WIDGETS[0].data.guiId);
    });

    it('should render iframe', function(){
        expect(CM.UI.prototype.render).toHaveBeenCalled();
        expect(CM.UI.prototype.render.callCount).toEqual(2);
    });
});

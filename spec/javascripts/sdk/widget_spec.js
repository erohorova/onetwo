//= require sdk/init
fixture.preload("fixture.html");
describe("Using fixtures and parse", function() {

    beforeEach(function() {
        spyOn(CM.UI.prototype, 'render');

        this.fixtures = fixture.load("fixture.html", true);
        CM.init(document);
    });

    it('should parse data-* attributes', function(){
        expect(CM.WIDGETS[0].data['ui']).toEqual('qa');
        expect(CM.WIDGETS[0].data['returnUrl']).toEqual("http://www.example.com/my-page");
        expect(CM.WIDGETS[0].data['numPosts']).toEqual('100');
        expect(CM.WIDGETS[0].data['orderBy']).toEqual('abc');
        expect(CM.WIDGETS[0].data['width']).toEqual('500');
    });

    it('should create guiId', function() {
        expect(CM.WIDGETS[0].data['guiId']).toEqual('cm-gui-test1');
        expect(CM.WIDGETS[0].data['guiId']).toBeDefined();
    });

    it('should support multiple widget', function(){
        expect(CM.WIDGETS.length).toEqual(2);
    });

    it('should populate default values', function(){
        expect(CM.WIDGETS[0].data['guiId']).toMatch('cm-gui-');
        expect(CM.WIDGETS[1].data['ui']).toEqual('qa');
        expect(CM.WIDGETS[1].data['numPosts']).toEqual('5');
        expect(CM.WIDGETS[1].data['orderBy']).toEqual('social');
        expect(CM.WIDGETS[1].data['width']).toEqual('100%');
        expect(CM.WIDGETS[1].data['height']).toEqual('600px');
    });
});

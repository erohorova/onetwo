require 'test_helper'
class OrganizationMetricsGathererTest < ActiveSupport::TestCase

  test 'current period data' do
    org = create(:organization)
    completed_profile_users = create_list(:user, 5, organization: org, is_complete: true)
    suspended_users = create_list(:user, 3, organization: org, is_complete: true, is_suspended: true)
    edcast_admins = create_list(:user, 2, organization: org, is_complete: true, is_edcast_admin: true)

    period = :week
    offset = 10

    UserLevelMetric.expects(:get_organization_metrics).
      with(organization_id: org.id, period: period, offset: offset).
      returns({smartbites_consumed: 6, smartbites_created: 15, active_users: 3, average_session: 10}).
      once

    org.expects(:get_new_users_count).with(period: period, offset: offset).returns(1)

    org.expects(:real_users_count_before).returns(4).once

    assert_equal({smartbites_consumed: 6, smartbites_created: 15, active_users: 3, average_session: 10, new_users: 1, engagement_index: 75, total_users: 4}, OrganizationMetricsGatherer.current_period_data(organization: org, period: period, offset: offset))
  end

  test 'metrics over span' do
    org = create(:organization)
    real_users = create_list(:user, 5, organization: org, is_complete: true)

    # 2 days have gone by in week 2, We are just getting over the hump
      UserLevelMetric.expects(:get_organization_metrics_over_span).
        with(organization_id: org.id, period: :day, begin_offset: 0, end_offset: 1, team_id: nil).
        returns({smartbites_consumed: 21, smartbites_created: 210, active_users: 3, average_session: 17}).
        once

      org.expects(:get_new_users_count_over_span).
        with(begin_time: PeriodOffsetCalculator::BEGINNING_OF_TIME,
             end_time: PeriodOffsetCalculator::BEGINNING_OF_TIME + 2.days).
        returns(1).once

      org.expects(:real_users_count_before).with(PeriodOffsetCalculator::BEGINNING_OF_TIME + 2.days).returns(4).once

      assert_equal({smartbites_consumed: 21, smartbites_created: 210, active_users: 3, new_users: 1, engagement_index: 75, average_session: 17, total_users: 4}, OrganizationMetricsGatherer.metrics_over_span(organization: org, team_id: nil, period: :day, offsets: [0,1], timeranges: [[PeriodOffsetCalculator::BEGINNING_OF_TIME, PeriodOffsetCalculator::BEGINNING_OF_TIME + 1.day], [PeriodOffsetCalculator::BEGINNING_OF_TIME + 1.day, PeriodOffsetCalculator::BEGINNING_OF_TIME + 2.day]]))
  end

  test 'comparison data' do
    org = create(:organization)
    period = :week
    offset = 2
    drilldown_timeranges = PeriodOffsetCalculator.drill_down_timeranges(period: :week, offset: 2)
    previous_drilldown_timeranges = PeriodOffsetCalculator.drill_down_timeranges(period: :week, offset: 1)

    Timecop.freeze(PeriodOffsetCalculator::BEGINNING_OF_TIME + 17.days + 4.hours) do
      PeriodOffsetCalculator.expects(:drill_down_period_and_offsets).with(period: :week, offset: 2).returns([:day, (14..20).to_a]).once
      PeriodOffsetCalculator.expects(:drill_down_period_and_offsets).with(period: :week, offset: 1).returns([:day, (7..13).to_a]).once
      PeriodOffsetCalculator.expects(:drill_down_timeranges).with(period: :week, offset: 2).returns(drilldown_timeranges).once
      PeriodOffsetCalculator.expects(:drill_down_timeranges).with(period: :week, offset: 1).returns(previous_drilldown_timeranges).once

      # current period
      OrganizationMetricsGatherer.expects(:metrics_over_span).
        with(organization: org, team_id: nil, period: :day, offsets: [14,15,16,17], timeranges: drilldown_timeranges.take(4)).
        returns({smartbites_consumed: 2, smartbites_created: 0, active_users: 1, total_users: 3, new_users: 1, engagement_index: 10, average_session: 5}).
        once

      OrganizationMetricsGatherer.expects(:metrics_over_span).
        with(organization: org, team_id: nil, period: :day, offsets: [7,8,9,10], timeranges: previous_drilldown_timeranges.take(4)).
        returns({smartbites_consumed: 6, smartbites_created: 0, active_users: 0, average_session: 10, total_users: 4, new_users: 1, engagement_index: 5}).once

      assert_equal({smartbites_consumed: -67, smartbites_created: 0, active_users: "Inf", new_users: 0, average_session: -50, total_users: -25, engagement_index: 100}, OrganizationMetricsGatherer.comparison_data(organization: org, period: period, offset: offset))
    end
  end

  test 'comparison data for beginning of time period' do
    org = create(:organization)
    period = :week
    offset = 2
    drilldown_timeranges = PeriodOffsetCalculator.drill_down_timeranges(period: :week, offset: 2)
    previous_drilldown_timeranges = PeriodOffsetCalculator.drill_down_timeranges(period: :week, offset: 1)

    Timecop.freeze(PeriodOffsetCalculator::BEGINNING_OF_TIME + 14.days + 4.hours) do
      PeriodOffsetCalculator.expects(:drill_down_period_and_offsets).with(period: :week, offset: 2).returns([:day, (14..20).to_a]).once
      PeriodOffsetCalculator.expects(:drill_down_period_and_offsets).with(period: :week, offset: 1).returns([:day, (7..13).to_a]).once
      PeriodOffsetCalculator.expects(:drill_down_timeranges).with(period: :week, offset: 2).returns(drilldown_timeranges).once
      PeriodOffsetCalculator.expects(:drill_down_timeranges).with(period: :week, offset: 1).returns(previous_drilldown_timeranges).once

      assert_equal({smartbites_consumed: 0, smartbites_created: 0, active_users: 0, new_users: 0, average_session: 0, total_users: 0, engagement_index: 0}, OrganizationMetricsGatherer.comparison_data(organization: org, period: period, offset: offset))
    end
  end

  test 'drill down data' do

    org = create(:organization)
    period = :week
    offset = 10

    expected_data = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/metrics_drill_down_data.json")).deep_symbolize_keys

    UserLevelMetric.expects(:get_organization_drill_down_data).
      with(organization_id: org.id, period: period, offset: offset).
      returns({
        smartbites_consumed: expected_data[:smartbites_consumed][:drilldown][:current],
        smartbites_created: expected_data[:smartbites_created][:drilldown][:current],
        average_session: expected_data[:average_session][:drilldown][:current],
        total_users: expected_data[:total_users][:drilldown][:current],
        active_users: expected_data[:active_users][:drilldown][:current]}).
      once

    PeriodOffsetCalculator.expects(:drill_down_timeranges).with(period: period, offset: offset).
      returns([[Time.now.utc - 2.days,Time.now.utc - 1.day],
               [Time.now.utc - 1.day, Time.now.utc]]).
      once

    org.expects(:get_new_users_drill_down_data).
      with(period: period, offset: offset).
      returns(expected_data[:new_users][:drilldown][:current]).
      once

    # total 4 users. This is to ensure engagement index number matches the one in the fixture
    org.stubs(:real_users_count_before).returns(3).then.returns(4)

    expected_drill_down_data = Hash[[:smartbites_consumed, :smartbites_created, :average_session, :new_users, :active_users, :engagement_index, :total_users].map{|metric| [metric, expected_data[metric][:drilldown][:current]]}]
    assert_equal(expected_drill_down_data, OrganizationMetricsGatherer.drill_down_data(organization: org, period: period, offset: offset))
  end

  # drilldown data for custom period
  test 'drill down data for custom period and timerange' do
    org = create(:organization)
    period = :day
    from_date = Time.now - 10.days
    to_date   = Time.now
    timerange = [from_date, to_date]

    expected_data = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/metrics_drill_down_data.json")).deep_symbolize_keys

    UserLevelMetric.expects(:get_organization_drill_down_data_for_timerange).
      with(organization_id: org.id, team_id: nil, period: period, timerange: timerange).
      returns({
        smartbites_consumed: expected_data[:smartbites_consumed][:drilldown][:current],
        smartbites_created: expected_data[:smartbites_created][:drilldown][:current],
        average_session: expected_data[:average_session][:drilldown][:current],
        total_users: expected_data[:total_users][:drilldown][:current],
        active_users: expected_data[:active_users][:drilldown][:current]}).
      once

    timeranges = [
                    [Time.now.utc - 2.days,Time.now.utc - 1.day],
                    [Time.now.utc - 1.day, Time.now.utc]
                 ]

    PeriodOffsetCalculator.expects(:drill_down_timeranges_for_timerange).with(period: period, timerange: timerange).
      returns(timeranges).
      once

    org.expects(:get_new_users_drill_down_data_for_timeranges).
      with(timeranges: timeranges).
      returns(expected_data[:new_users][:drilldown][:current]).
      once

    # total 4 users. This is to ensure engagement index number matches the one in the fixture
    org.stubs(:real_users_count_before).returns(3).then.returns(4)

    expected_drill_down_data = Hash[[:smartbites_consumed, :smartbites_created, :average_session, :new_users, :active_users, :engagement_index, :total_users].map{|metric| [metric, expected_data[metric][:drilldown][:current]]}]
    assert_equal(expected_drill_down_data, OrganizationMetricsGatherer.drill_down_data_for_custom_period(organization: org, team_id: nil, timerange: timerange))
  end
end

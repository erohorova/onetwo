require 'test_helper'

class TrackingTest < ActiveSupport::TestCase
  test 'tracks acts' do
    skip
    Tracking.unstub :track_act
    Tracking.expects(:track).with(
        99,
        'act',
        card: 'cardurl',
        type: 'unlike',
        object: 'card',
        platform: 'test',
    )

    Tracking::track_act(
        user_id: 99,
        card: 'cardurl',
        object: 'card',
        action: 'unupvote',
        platform: 'test',
    )
  end

  class TrackingDataTest < ActiveSupport::TestCase
    include Tracking::Visit

    test 'sets tracking params' do
      skip
      user = create(:user)
      assert_equal ({ object: 'user', user: user.handle }), tracking_params(user)
      assert_empty tracking_params(nil)
    end
  end
end

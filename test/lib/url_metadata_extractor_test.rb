require 'test_helper'
require 'ostruct'
#require 'UrlMetadataExtractor'
class UrlMetadataExtractorTest < ActiveSupport::TestCase
  include UrlMetadataExtractor

  #TODO: Again, dependency on live url should be removed
  setup do
    stub_request(:get, "http://url.com").
          to_return(:status => 200,
                    :body => '<html>
                    <head>
                      <title>
                        title
                      </title>
                      <link rel="alternate" type="application/json+oembed" href="http://www.oembed-url.json" />
                      <meta property="og:title" content="og:title">
                      <meta property="og:description" content="og:description">
                      <meta property="og:site_name" content="og:site_name">
                      <meta property="og:image" content="http://imageurl.com">
                      <meta property="og:image" content="http://imgur.com">
                      <meta property="og:video" content="http://videourl.com">
                      <meta property="og:type" content="og:type">
                    </head>
                    </html>',
                    :headers => {'Content-Type' => 'text/html'},
                    )
    @url = 'http://url.com'
    @another_url = 'http://anotherurl.com'

    @embed_html = "<iframe height='480' width='500' src='http://someurl.com?a=b' allowfullscreen />"
    @embed_html_autoplay = "<iframe height='480' width='500' src='http://someurl.com?a=b&autoplay=1' allowfullscreen />"

    stub_request(:get, "http://www.oembed-url.json").
      to_return(:status => 200,
                :body => {type: 'video', html: @embed_html}.to_json,
                :headers => {})
  end

  test 'should handle link type oembed' do
    Rails.logger.expects(:error).never
    link_url = 'http://www.link-url.com'
    link_embed_url = "http://www.link-embed-url.json"
    stub_request(:get, link_url).
      to_return(:status => 200,
                :body => "<html><head><link rel='alternate' type='application/json+oembed' href='#{link_embed_url}' /></head></html>",
                :headers => {})
    stub_request(:get, link_embed_url).
      to_return(:status => 200,
                :body => {type: 'link', somekey: 'someval'}.to_json,
                :headers => {})

    metadata = grab_metadata link_url
    assert_nil metadata['embed_html']
    assert_equal 'link', metadata['oembed_type']
  end

  test 'should handle rich type oembed' do
    skip("Extending to 'rich' fails on wordpress pages that send am embed html that does not display properly. See EP-2675")

    Rails.logger.expects(:error).never
    rich_url = 'http://www.rich-url.com'
    rich_embed_url = "http://www.rich-embed-url.json"
    stub_request(:get, rich_url).
      to_return(:status => 200,
                :body => "<html><head><link rel='alternate' type='application/json+oembed' href='#{rich_embed_url}' /></head></html>",
                :headers => {})
    stub_request(:get, rich_embed_url).
      to_return(:status => 200,
                :body => {type: 'rich', html: '<iframe height="40" width="50" src="http://blah.com" />'}.to_json,
                :headers => {})

    metadata = grab_metadata rich_url
    assert_not_nil metadata['embed_html']
    assert_equal 'rich', metadata['oembed_type']

    # now no iframe
    stub_request(:get, rich_embed_url).
      to_return(:status => 200,
                :body => {type: 'rich', html: '<div />'}.to_json,
                :headers => {})

    metadata = grab_metadata rich_url
    assert_not_nil metadata['embed_html']
    assert_equal 'rich', metadata['oembed_type']
  end

  test 'should handle photo type oembed' do
    Rails.logger.expects(:error).never
    photo_url = 'http://www.photo-url.com'
    photo_embed_url = "http://www.photo-embed-url.json"
    stub_request(:get, photo_url).
      to_return(:status => 200,
                :body => "<html><head><link rel='alternate' type='application/json+oembed' href='#{photo_embed_url}' /></head></html>",
                :headers => {})
    stub_request(:get, photo_embed_url).
      to_return(:status => 200,
                :body => {type: 'photo', html: '<img src="http://someimage.png" />'}.to_json,
                :headers => {})

    metadata = grab_metadata photo_url
    assert_nil metadata['embed_html']
    assert_equal 'photo', metadata['oembed_type']
  end

  test "should not remove trailing slash" do
    text = 'www.url.com/example/text/'
    assert_equal 'http://www.url.com/example/text/', UrlMetadataExtractor.return_url(text)
  end

  test "should remove the unwanted characters comma from the url" do
    text = 'www.url.com/example/test"]'
    text = UrlMetadataExtractor.return_url text
    assert_equal "http://www.url.com/example/test", text
  end

  test "should not fail for URL enclosed in square brackets" do
    text = '[http://www.nupedia.com Nupedia]'
    text = UrlMetadataExtractor.return_url text
    assert_equal "http://www.nupedia.com", text
  end

  test "should not fail for URL including special chars" do
    url_with_text = '<http://www.fo_o.bar/?listings.html#section-2>'
    url = UrlMetadataExtractor.return_url url_with_text
    assert_equal "http://www.fo_o.bar/?listings.html#section-2", url
  end

  test "should not fail for image attachment" do
    text = '![a busy cat](https://cdn.sstatic.net/Sites/stackoverflow/img/error-lolcat-problemz.jpg)¬'
    text = UrlMetadataExtractor.return_url text
    assert_equal "https://cdn.sstatic.net/Sites/stackoverflow/img/error-lolcat-problemz.jpg", text

    text = "http://Spill-coffee-laptop-keyboard-600x412.jpg-3629]"
    text = UrlMetadataExtractor.return_url text
    assert_equal "http://Spill-coffee-laptop-keyboard-600x412.jpg-3629", text
  end

  test "prepend http/https if doesn't exist" do
    url = "www.url.com/example/test"
    url = UrlMetadataExtractor.return_url url
    assert_equal url, "http://www.url.com/example/test"
  end

  test "skip prepend http/https if exist already" do
    url = "https://u1001.www1.url.com/example/test"
    url = UrlMetadataExtractor.return_url url
    assert_equal url, url
  end

  test 'should handle short url types like (https|http)://abc.ab' do
    url = 'https://jwt.io'
    url = UrlMetadataExtractor.return_url url
    assert_equal url, url
  end

  test 'should not fail on unauthorized urls' do
    bad_url = 'http://badurl.com'
    stub_request(:get, bad_url).
          to_return(:status => 403,
                    :body => '<html><head></head><body>Unauthorized</body></html>',
                    :headers => {})
    metadata = grab_metadata bad_url
    assert_equal metadata, {}
  end

  def grab_metadata text
    url = UrlMetadataExtractor.return_url text
    UrlMetadataExtractor.get_metadata url
  end

  test "should grab title field" do
    metadata = grab_metadata @url
    assert metadata.key?('title')
    assert metadata['title'], 'title'
  end

  test "should grab og:title" do
    metadata = grab_metadata @url
    assert metadata.key?('og:title')
    assert_equal metadata['og:title'], 'og:title'
  end

  test "should grab og:description" do
    metadata = grab_metadata @url
    assert metadata.key?('og:description')
    assert_equal metadata['og:description'], 'og:description'
  end

  test "should grab og:image" do
    metadata = grab_metadata @url
    assert metadata.key?('og:image')
    expected_url = URI.parse('http://imageurl.com')
    parsed_url = URI.parse(metadata['og:image'])
    assert_equal expected_url, parsed_url
  end

  test "should grab only first og:image when multiple og:images present on page" do
    metadata = grab_metadata @url
    assert metadata.key?('og:image')

    expected_url = URI.parse('http://imageurl.com')
    parsed_url = URI.parse(metadata['og:image'])
    assert_equal expected_url, parsed_url
  end

  test "should grab og:type" do
    metadata = grab_metadata @url
    assert metadata.key?('og:type')
    assert_equal metadata['og:type'], 'og:type'
  end

  test "should grab og:site_name" do
    metadata = grab_metadata @url
    assert metadata.key?('og:site_name')
    assert_equal metadata['og:site_name'], 'og:site_name'
  end

  test "should grab og:video" do
    metadata = grab_metadata @url
    assert metadata.key?('og:video')
    expected_url = URI.parse 'http://videourl.com'
    parsed_url = URI.parse metadata['og:video']
    # params can be in different order
    assert_equal expected_url.host, parsed_url.host
    assert_equal expected_url.path, parsed_url.path
  end

  test 'should populate content type' do
    metadata = grab_metadata @url
    assert_equal metadata['content_type'], 'text/html'
  end

  test 'should populate embed html' do
    metadata = grab_metadata @url
    assert_equal @embed_html_autoplay, metadata['embed_html']
    assert_equal 'video', metadata['oembed_type']
  end

  test 'should fallback to splash server to get html' do
    bad_but_reachable_by_splash_url = 'http://example.com'
    stub_request(:get, bad_but_reachable_by_splash_url).
          to_return(:status => 403,
                    :body => '<html><head></head><body>Unauthorized</body></html>',
                    :headers => {})
    splash_test_endpoint = "http://sometest.splash.io:8050"

    stub_request(:post, splash_test_endpoint + "/render.html").with(body: {:url => bad_but_reachable_by_splash_url, :headers => {"User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"}}.to_json).
      to_return(status: 200,
                body: '<html>
                        <head>
                          <title>title</title>
                          <link rel="alternate" type="application/json+oembed" href="http://www.oembed-url.json" />
                          <meta property="og:title" content="og:title">
                        </head>
                      </html>',
                headers: {'Content-Type' => 'text/html'}
                )

    Settings.stubs(:splash_server).returns(OpenStruct.new(endpoint: splash_test_endpoint, enabled: true))
    metadata = grab_metadata bad_but_reachable_by_splash_url
    assert_equal 'text/html', metadata['content_type']
    assert metadata.key?('title')
    assert_equal 'title', metadata['title']
  end


  class MetaDataAttrsTest < ActiveSupport::TestCase

    setup do
      stub_request(:get, 'http://url.com').
          to_return(status: 200,
                    body:
                      '<html>
                        <head>
                          <link rel="alternate" type="application/json+oembed" href="www.lynda.com/player/oembed?url=https://www.lynda.com/Classroom-Management-tutorials/Welcome/430072/458667-4.html" />
                        </head>
                      </html>',
                    headers: {'Content-Type' => 'text/html'}
                    )
      @url = 'http://url.com'
      @embed_html = "<iframe height='480' width='500' src='http://someurl.com?a=b' allowfullscreen />"

      stub_request(:get, 'https://www.lynda.com/player/oembed?url=https://www.lynda.com/Classroom-Management-tutorials/Welcome/430072/458667-4.html').
          to_return(status: 200,
                    body: {type: 'video', html: @embed_html}.to_json,
                    headers: {})

    end

    test 'should add proper URI prefix if it exist in oembed_url' do
      metadata = UrlMetadataExtractor.get_metadata(@url)
      assert_equal 'video', metadata['oembed_type']
    end

    test 'should get src from oembed_html string' do
      metadata = UrlMetadataExtractor.get_metadata(@url)
      src = UrlMetadataExtractor.get_src_from_embed(metadata['embed_html'])
      valid_link = 'http://someurl.com?a=b&autoplay=1'

      assert_equal valid_link, src
    end
  end

  test 'should handle correct encoding' do
    url = "http://abcd.com"
    response_body = StringIO.new("<html><head><title>Encoding’s test</title><meta content=\"text/html;charset=utf-8\"></head></html>")
    #net_http adapter is setting this encoding(ascii-8bit)
    response_body.set_encoding('ascii-8bit')
    response = OpenStruct.new(body: response_body.read, headers: {}, status: 200)
    metadata = UrlMetadataExtractor.get_metadata_attributes(response, url)
    assert_equal "Encoding’s test", metadata['title']
  end

  test 'should not include list parameter in embedded HTML for youtube video urls' do
    url = "https://www.youtube.com/watch?v=3XTMQw-mOJk&index=3&list=PL0fCe9z_vQ5lXF3_jyuUMq5IetVAlCEIs"
    oembed_url = "https://www.youtube.com/watch?v=3XTMQw-mOJk&index=3&"
    embed_html = "<iframe height='480' width='500' src='https://www.youtube.com/embed/3XTMQw-mOJk?feature=oembed' allowfullscreen />"
    embed_html_autoplay = "<iframe height='480' width='500' src='https://www.youtube.com/embed/3XTMQw-mOJk?autoplay=1&feature=oembed' allowfullscreen />"

    stub_request(:get, url).
      to_return(:status => 200,
                :body => "<html><head><link rel='alternate' type='application/json+oembed' href='#{url}' /></head></html>",
                :headers => {})
    stub_request(:get, oembed_url).
      to_return(:status => 200,
                :body => {type: 'video', html: embed_html}.to_json,
                :headers => {})
    metadata = grab_metadata url
    assert_nil metadata['embed_html']['list']
  end
end

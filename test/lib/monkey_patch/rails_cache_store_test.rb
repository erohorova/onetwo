require 'test_helper'
class RailsCacheStoreTest < ActiveSupport::TestCase
  
  class CacheableClass1
    include Cacheable
    def value
      "cache1:#{arg}"
    end
    
  end

  class CacheableClass2
    include Cacheable
    def value
      "cache2:#{arg}"
    end

  end
  
  test 'fetch should the value method' do
    cache = CacheableClass1.new(arg: 'val')
    assert_equal 'cache1:val', cache.value
    assert_equal cache.value, Rails.cache.fetch(cache)
  end
  
  test 'fetch_multi should work' do
    cache1 = CacheableClass1.new(arg: 'val')
    cache2 = CacheableClass2.new(arg: 'val')
    assert_equal [cache1.value, cache2.value], Rails.cache.fetch_multi(cache1, cache2)

    results = Rails.cache.fetch_multi(cache1, cache2, "somekey") do |k|
      if k == 'somekey'
        k + '-result'
      else
        raise "Unknown key: #{k}"
      end
    end
    assert_equal [cache1.value, cache2.value, "somekey-result"], results
  end
end
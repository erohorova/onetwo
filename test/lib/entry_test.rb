require 'test_helper'
class EntryTest < ActiveSupport::TestCase
  class EntryClassTest
    include Entry
  end
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'

    @credential = create(:credential)
    @org = create(:organization, client: @credential.client)
    CacheFactory.stubs(:org_with_api_key).returns(@org)
    CacheFactory.stubs(:fetch_group).returns(nil)
    PartnerAuthentication.stubs(:is_api_client_request_valid?).returns(true)
    WebMock.disable!
    Searchkick.enable_callbacks
  end

  test 'save course term to resource group when triggered from JsAddon' do
    # when created
    cm_admin = create :user, email: 'admin@course-master.com'
    pars = { course_term: "Fall 2015",
      parent_url: "https://test-app.localhost/courses/188/learn",
      resource_id: 188,
      resource_name: "Invite Only 1",
      timestamp: "1452857724",
      user_email: "facultyuserone@edcast.com",
      user_first_name: "Facultyuser",
      user_id: "2",
      user_image: "https:/system/users/pictures/000/000/002/original/banner2.jpg?1452755944",
      user_last_name: "One",
      user_name: "Facultyuser One",
      user_role: "admin" }

    object = EntryClassTest.new
    object.prepare_data(pars)
    rg = ResourceGroup.first
    assert_match /#{rg.name}/, pars[:resource_name]
    assert_match /#{rg.website_url}/, pars[:parent_url]
    assert_equal rg.organization_id, @org.id
    assert_match /#{rg.client_resource_id}/, pars[:resource_id].to_s
    assert_match /#{rg.type}/, 'ResourceGroup'
    assert_match /#{rg.course_term}/, pars[:course_term]

    # when updated
    CacheFactory.stubs(:fetch_group).returns(rg)
    pars.merge!(course_term: 'Rise 2016')
    object = EntryClassTest.new
    object.prepare_data(pars)
    rg = ResourceGroup.first
    assert_match /#{rg.course_term}/, 'Rise 2016'
  end

  test 'save course term to resource group when triggered from LtiAddon' do
    # when created
    cm_admin = create :user, email: 'admin@course-master.com'
    pars = {
      custom_course_term: "Fall 2015",
      parent_url: "https://test-app.localhost/courses/188/learn",
      resource_id: 188,
      resource_name: "Invite Only 1",
      timestamp: "1452857724",
      user_email: "facultyuserone@edcast.com",
      user_first_name: "Facultyuser",
      user_id: "2",
      user_image: "https:/system/users/pictures/000/000/002/original/banner2.jpg?1452755944",
      user_last_name: "One",
      user_name: "Facultyuser One",
      user_role: "admin" }
    object = EntryClassTest.new
    object.prepare_data(pars)
    rg = ResourceGroup.first
    assert_match /#{rg.course_term}/, pars[:custom_course_term]

    # when updated
    CacheFactory.stubs(:fetch_group).returns(rg)
    pars.merge!(custom_course_term: 'Rise 2016')
    object = EntryClassTest.new
    object.prepare_data(pars)
    rg = ResourceGroup.first
    assert_match /#{rg.course_term}/, 'Rise 2016'
  end
end

require 'test_helper'
#require 'UrlMetadataExtractor'
class CamoFilterTest < ActiveSupport::TestCase
  include CamoFilter

  test 'should get http urls from camo' do
    url = 'http://i.something.png'
    expected = 'http%3A%2F%2Fi.something.png'
    camo_url = CamoFilter.filter(url)
    assert_equal "#{Settings.camo_host}/ec/?url=#{expected}", camo_url
  end

  test 'should send https urls as is' do
    url = 'https://i.something.png'
    assert_equal url, CamoFilter.filter(url)
  end

  test 'any other input as is' do
    assert_nil CamoFilter.filter(nil)
    input = '//someimage.png'
    assert_equal input, CamoFilter.filter(input)
  end
end

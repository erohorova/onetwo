require 'test_helper'

class JArrayTest < ActiveSupport::TestCase
  test 'should return sql in query format' do
    assert_equal "'a', 'b', 'c'", ["a", "b", "c"].to_sql_in
  end

  test 'should return empty string for empty array' do
    assert_equal "", [].to_sql_in
  end
end
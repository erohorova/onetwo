require 'test_helper'
class GdprTest < ActionDispatch::IntegrationTest
  setup do
  	@organization = create(:organization)
  	@user = create(:user, organization: @organization, organization_role: 'admin')
    @email = @user.email
  	@admin = create(:user, organization: @organization)
    @other_user = create(:user, organization: @organization)

    @card_by_other_user = create(:card, card_type: 'media', card_subtype: 'text', author_id: @admin.id, title: 'this is title', message: 'this is message')
    @channel = create(:channel, user: @user, organization: @organization)
    create(:channels_curator, channel_id: @channel.id, user_id: @user.id)
    create(:channels_user, channel_id: @channel.id, user_id: @user.id)

    @channels_curator = ChannelsCurator.where({user_id: @user.id}).pluck(:id)
    @channels_collaborator = ChannelsUser.where({user_id: @user.id}).pluck(:id)

  	#creating cards
    #media
  	@media = create(:card, card_type: 'media', card_subtype: 'text', author_id: @user.id, title: 'this is title', message: 'this is message')
    #video stream
    @video_stream = create(:wowza_video_stream, status: 'upcoming', creator: @user)
    @video_stream_card = @video_stream.card
    #poll
    @poll_card = create(:card, card_type: 'poll', author: @user, organization_id: @organization.id)
    option1 = create(:quiz_question_option, label: 'a', quiz_question: @poll_card, is_correct: true)
    option2 = create(:quiz_question_option, label: 'b', quiz_question: @poll_card)


    @access_log = create(:access_log, user: @user)
    @activity_stream = create(:activity_stream, streamable: @media)
    @assignment = create(:assignment, assignee: @admin, assignable: @card_by_other_user)
    @bookmark = create(:bookmark, user: @user, bookmarkable: @media)
    @card_user_access = create(:card_user_access, user: @user, card: @card_by_other_user)
    @comment = create(:comment, user: @user,  commentable: @media)
    @follow = create(:follow, user: @user, followable: @channel)

    #Team
    @team = create(:team, organization: @organization)
    create(:teams_user, user_id: @user.id, team_id: @team.id, as_type: 'admin')
    create(:teams_user, user_id: @admin.id, team_id: @team.id, as_type: 'member')

    #Projects
    @project_card = create(:card, organization: @organization, author: @admin,card_type: 'project',card_subtype: 'text')
    @project_submission = create(:project_submission, description: "something", project_id: @project_card.project.id, submitter_id: @user.id)

    @project_card1 = create(:card, organization: @organization, author: @user, card_type: 'project',card_subtype: 'text')
    @project_submission = create(:project_submission, description: "something", project_id: @project_card1.project.id, submitter_id: @other_user.id)
  end

  test 'should delete appropriate data when delete request is made' do
    user_id = @user.id
  	Gdpr.delete_user(@user.id, @admin.id)
    assert_empty AccessLog.where({user_id: user_id})
    assert_empty ActivityStream.where({user_id: user_id})
    assert_empty Assignment.where({user_id: user_id})
    assert_empty AssignmentNoticeStatus.where({user_id: user_id})
    assert_empty Bookmark.where({user_id: user_id})
    assert_empty CardUserAccess.where({user_id: user_id})
    assert_empty ClcOrganizationsRecord.where({user_id: user_id})
    assert_empty ClcProgress.where({user_id: user_id})
    assert_empty Comment.where({user_id: user_id})
    assert_empty Email.where({user_id: user_id})
    assert_empty Event.where({user_id: user_id})
    assert_empty EventsUser.where({user_id: user_id})
    assert_empty IdentityProvider.where({user_id: user_id})
    assert_empty Invitation.where({recipient_id: user_id})
    assert_empty LearningQueueItem.where({user_id: user_id})
    assert_empty Mention.where({user_id: user_id})
    assert_empty NotificationConfig.where({user_id: user_id})
    assert_empty NotificationEntry.where({user_id: user_id})
    assert_empty Notification.where({user_id: user_id})
    assert_empty Post.where({user_id: user_id})
    assert_empty PubnubChannel.where({item_type: 'User', item_id: user_id})
    assert_empty PwcRecord.where({user_id: user_id})
    assert_empty QuizQuestionAttempt.where({user_id: user_id})
    assert_empty Score.where({user_id: user_id})
    assert_empty SkillsUser.where({user_id: user_id})
    assert_empty SuperAdminAuditLog.where({user_id: user_id})
    assert_empty Tagging.where({taggable_type: 'User', taggable_id: user_id})
    assert_empty UserBadge.where({user_id: user_id})
    assert_empty UserConnection.where({user_id: user_id})
    assert_empty UserContentCompletion.where({user_id: user_id})
    assert_empty UserContentLevelMetric.where({user_id: user_id})
    assert_empty UserCustomField.where({user_id: user_id})
    assert_empty UserDailyLearning.where({user_id: user_id})
    assert_empty UserLevelMetric.where({user_id: user_id})
    assert_empty UserNotificationRead.where({user_id: user_id})
    assert_empty UserOnboarding.where({user_id: user_id})
    assert_empty UserPreference.where({user_id: user_id})
    assert_empty UserProfile.where({user_id: user_id})
    assert_empty UserRole.where({user_id: user_id})
    assert_empty UserSession.where({user_id: user_id})
    assert_empty UserTaxonomyTopicLevelMetric.where({user_id: user_id})
    assert_empty UserTopicLevelMetric.where({user_id: user_id})
    assert_empty UsersCardsManagement.where({user_id: user_id})
    assert_empty UsersExternalCourse.where({user_id: user_id})
    assert_empty UsersIntegration.where({user_id: user_id})
    assert_empty Follow.where({user_id: user_id})
    assert_empty ChannelsCurator.where({user_id: user_id})
    assert_empty ChannelsUser.where({user_id: user_id})
    assert_empty TeamsUser.where({user_id: @user_id})
    assert_empty GroupsUser.where({user_id: user_id})
    assert_empty ProjectSubmission.where({submitter_id: @user.id})
    assert_equal 1, Card.where({author_id: user_id, card_type: 'media', card_subtype: 'text'}).map(&:message).uniq.length
    assert_equal 'This card is deleted by author', Card.where({author_id: user_id, card_type: 'media', card_subtype: 'text'}).map(&:message).uniq[0]
    assert_equal 'media', @video_stream_card.reload.card_type
    assert_equal 1, Card.where({author_id: user_id, card_type: 'poll'}).count
    assert_equal @admin.id, ChannelsCurator.where(id: @channels_curator).pluck(:user_id).uniq.first
    assert_equal @admin.id, ChannelsUser.where(id: @channels_collaborator).pluck(:user_id).uniq.first
    @user.reload
    assert_equal "#{@user.id}suspended@edcast.com", @user.email
    assert_equal 'UnknownFname', @user.first_name
    assert_equal 'UnknownLname', @user.last_name
    assert_equal "@#{@user.id}suspended", @user.handle
    assert_equal 'member', @user.organization_role
    assert @user.is_suspended?

    #Testing if user is able to signup again with same email
    open_session do |session|
      session.post "#{@organization.home_page}/api/v2/users/email_signup", {user: {email: @email, first_name: 'SameUser', last_name: 'Again', password: 'Test@1234'}, profile_attributes: {tac_accepted: true, dob: '12/12/1987'}}
    end
    new_user = User.where(email: @email).first
    assert_not_equal @user.id, new_user.id
    assert_equal @organization.id, new_user.organization_id
  end
end
require 'test_helper'
class OrgSamlTest < ActiveSupport::TestCase
  setup do
    @org = create(:organization)
    @sso = create(:sso, name: 'saml')
    @org_sso = create(:org_sso, sso: @sso, organization: @org,
                      saml_issuer: 'edcast',
                      saml_idp_sso_target_url: 'http://example.com/login'
            )
  end
  test 'set organization configuration' do
    rack_app = mock()
    org_saml_middleware = OmniAuth::Strategies::OrgSAML.new rack_app

    org_saml_middleware.set_options_for_organization(@org)
    assert_nil org_saml_middleware.options.something_else
    assert_equal 'edcast', org_saml_middleware.options.issuer
    assert_equal 'http://example.com/login', org_saml_middleware.options.idp_sso_target_url
  end
end

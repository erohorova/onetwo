# frozen_string_literal: true
require 'test_helper'

class Analytics::DeleteUserSearchHistoryTest < ActiveSupport::TestCase

  test "deletes the user search history from influx" do
    user_id, org_id = 1, 2
    INFLUXDB_CLIENT.expects(:query).with(
      "delete  from \"searches\" where _user_id = %{user_id} AND org_id = %{org_id} ;",
      params: {
        user_id: user_id.to_s,
        org_id: org_id.to_s
      }
    )
    Analytics::DeleteUserSearchHistory.run!(
      user_id: user_id,
      org_id: org_id
    )
  end

end
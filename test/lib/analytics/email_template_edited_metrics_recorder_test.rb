# frozen_string_literal: true
require 'test_helper'
class EmailTemplateEditedMetricsRecorderTest < ActiveSupport::TestCase

  setup do
    @klass = Analytics::EmailTemplateEditedMetricsRecorder
    @timestamp = Time.now.to_i
    @org = create :organization
    @actor = create :user, organization: @org
    @default_template = EmailTemplate.create!(
      organization: @org,
      title:        "fake",
      creator:      @actor,
      language:     "fake",
      design:       "fake",
      state:        "draft",
      content:      "fake",
      is_active:    true
    )
    @email_template = EmailTemplate.create!(
      organization: @org,
      title:        "foo123",
      creator:      @actor,
      language:     "anglish",
      design:       "is this html?",
      state:        "draft",
      content:      "foo bar",
      default_id:   @default_template.id,
      is_active:    true
    )
    InfluxdbRecorder.unstub :record

    @expected_event_template = {
      timestamp: @timestamp,
      tags: {
        event: "org_email_template_edited",
        org_id: @org.id.to_s,
        is_system_generated: '1',
        _user_id: @actor.id.to_s,
      },
      values: {
        user_id: @actor.id.to_s,
        org_host_name: @org.host_name,
        org_name: @org.name,
        org_slug: @org.slug,
        org_is_open: @org.is_open ? '1' : '0',
        org_is_registrable: @org.is_registrable ? '1' : '0',
        org_status: @org.status,
        org_send_weekly_activity: @org.send_weekly_activity ? '1' : '0',
        org_show_sub_brand_on_login: @org.show_sub_brand_on_login ? '1' : '0',
        org_savannah_app_id: @org.savannah_app_id.to_s,
        org_social_enabled: @org.social_enabled ? '1' : '0',
        org_xapi_enabled: @org.xapi_enabled ? '1' : '0',
        org_show_onboarding: @org.show_onboarding ? '1' : '0',
        org_enable_role_based_authorization: @org.enable_role_based_authorization ? '1' : '0',

        email_template_content:    @email_template.content,
        email_template_creator_id: @email_template.creator_id.to_s,
        email_template_state:      @email_template.state,
        email_template_language:   @email_template.language,
        email_template_title:      @email_template.title,
        email_template_is_active:  @email_template.is_active ? '1' : '0',
        email_template_default_id: @email_template.default_id.to_s,
        email_template_design:     @email_template.design,
        email_template_id:         @email_template.id.to_s,
      }
    }
    @recorder_args = {
      org: Analytics::MetricsRecorder.org_attributes(@org),
      actor: Analytics::MetricsRecorder.user_attributes(@actor),
      email_template: Analytics::MetricsRecorder.email_template_attributes(@email_template),
      event: "org_email_template_edited",
      timestamp: @timestamp,
      additional_data: { user_id: @actor.id }
    }
  end

  test 'records edited event' do
    InfluxdbRecorder.expects(:record).with(
      'organizations',
      @expected_event_template.deep_merge(
        values: {
          changed_column: "content",
          old_val: "the old",
          new_val: "the new",
        }
      ),
      { write_client: nil, duplicate_check: true }
    )
    @klass.record(@recorder_args.merge(
      changed_column: "content",
      old_val: "the old",
      new_val: "the new"
    ))
  end

end
require 'test_helper'

class Analytics::GroupmetricsAggregationRecorderTest < ActiveSupport::TestCase

  using ObjectRefinements::GroupBySequence

  setup do
    @base_time = Time.now.utc
    Time.stubs(:now).returns @base_time
    @beginning_of_day = @base_time.beginning_of_day
    @beginning_of_yesterday = @beginning_of_day - 1.days
    @end_of_day = @beginning_of_day.end_of_day
    @end_of_yesterday = @beginning_of_yesterday.end_of_day
    @klass = Analytics::GroupMetricsAggregationRecorder

    # create a couple orgs
    @org1, @org2 = create_list(:organization, 2)

    # create a couple teams
    @org1_team1 = create :team, organization: @org1
    @org1_team2 = create :team, organization: @org1
    @org2_team1 = create :team, organization: @org2
    @org2_team2 = create :team, organization: @org2

    # create some users in each org.=
    @users = {
      @org1 => create_list(:user, 2, organization: @org1),
      @org2 => create_list(:user, 2, organization: @org2)
    }
    @org1_user1, @org1_user2 = @users[@org1]
    @org2_user1, @org2_user2 = @users[@org2]

    # Update users to have created_at either yesterday or the day before
    @users.values.each do |users|
      users.each_with_index do |user, idx|
        user.update created_at: @beginning_of_day - (idx + 1).days
      end
    end

    # add the users to teams. All each org's users are added to the first team
    # in that org. The second team per org has no users.
    @org1_team1.add_to_team [@org1_user1, @org1_user2]
    @org2_team1.add_to_team [@org2_user1, @org2_user2]

    # Manually set all the teams_users to have a created_at of yesterday.
    # This is so they will show up in the total_users count
    TeamsUser.update_all(created_at: @beginning_of_yesterday)

    # create UserMetricsAggregation records for all users, over 3 days.
    [@org1, @org2].each_with_index do |org, org_idx|
      org_idx = org_idx + 1 # start idx at 1
      users = @users[org]
      3.times do |day_idx|
        day_idx = day_idx + 1 # start idx at 1
        record_start_time = @beginning_of_day - day_idx.days
        record_end_time = record_start_time.end_of_day
        users.each_with_index do |user, user_idx|
          user_idx = user_idx + 1 # start idx at 1
          UserMetricsAggregation.create!(
            user:                       user,
            organization:               org,
            smartbites_commented_count: 0, # comments and likes have their own test case
            smartbites_liked_count:     0,
            smartbites_completed_count: 2 * org_idx * day_idx * user_idx,
            smartbites_consumed_count:  3 * org_idx * day_idx * user_idx,
            smartbites_created_count:   4 * org_idx * day_idx * user_idx,
            # time spent minutes is 0 for all users in org 1
            time_spent_minutes:         12 * (org_idx - 1) * day_idx * user_idx,
            total_smartbite_score:      7 * org_idx * day_idx * user_idx,
            total_user_score:           8 * org_idx * day_idx * user_idx,
            start_time:                 record_start_time.to_i,
            end_time:                   record_end_time.to_i
          )
        end
      end
    end

    # Lambda to fetch all GroupMetricsAggregation records' attributes,
    # grouped by org id, team id, and start_time.
    @fetch_records = -> do
      group_by_sequence(
        GroupMetricsAggregation.all,
        %i{organization_id team_id start_time}
      ) do |records|
        records.map do |record|
          record.attributes.except *%w{created_at updated_at id}
        end
      end
    end
  end

  test 'MEASUREMENT' do
    assert_equal "group_aggregation_daily", @klass::MEASUREMENT
  end

  test 'TIME_RANGE' do
    # A time range of zero days means that it will create an aggregation
    # for the previous day only, and no additional days.
    assert_equal 0.days, @klass::TIME_RANGE
  end

  test 'run calls run_for_org for all orgs by default' do
    [@org1, @org2].each do |org|
      @klass.expects(:run_for_org).with(org)
    end
    @klass.run
  end


  test 'run calls run_for_org for specified orgs only' do
    @klass.expects(:run_for_org).with(@org1)
    @klass.run(orgs: [@org1])
  end

  test 'run creates records for given org and previous day' do
    @klass.run
    actual = @fetch_records.call
    expected = {
      @org1.id => {
        @org1_team1.id => {
          @beginning_of_yesterday.to_i => [{
            'organization_id'     => @org1.id,
            'team_id'             => @org1_team1.id,
            'start_time'          => @beginning_of_yesterday.to_i,
            'end_time'            => @end_of_yesterday.to_i,
            'smartbites_created'  => 12,
            'smartbites_consumed' => 9,
            'time_spent_minutes'  => 0,
            'total_users'         => 2,
            'active_users'        => 0,
            'new_users'           => 1,
            'engagement_index'    => 0,
            'num_comments'        => 0,
            'num_likes'           => 0
          }]
        }
      },
      @org2.id => {
        @org2_team1.id => {
          @beginning_of_yesterday.to_i => [{
            'organization_id'     => @org2.id,
            'team_id'             => @org2_team1.id,
            'start_time'          => @beginning_of_yesterday.to_i,
            'end_time'            => @end_of_yesterday.to_i,
            'smartbites_created'  => 24,
            'smartbites_consumed' => 18,
            'time_spent_minutes'  => 36,
            'total_users'         => 2,
            'active_users'        => 2,
            'new_users'           => 1,
            'engagement_index'    => 100,
            'num_comments'        => 0,
            'num_likes'           => 0
          }]
        }
      }
    }
    assert_equal expected, actual
  end

  test 'run backfills records for given org and TIME_RANGE' do
    skip('Yet to be fixed')
    begin
      # Set the TIME_RANGE to encompass all records.
      # Now, for each org / team, we will get records for each of 3 days.
      # At the end of the test we ensure that gets set back to the original value.
      orig_time_range = @klass::TIME_RANGE
      @klass.const_set "TIME_RANGE", 2.days
      @klass.run
      actual = @fetch_records.call

      expected = {
        @org1.id => {
          @org1_team1.id => {
            (@beginning_of_yesterday - 2.days).to_i => [{
              'organization_id'     => @org1.id,
              'team_id'             => @org1_team1.id,
              'start_time'          => (@beginning_of_yesterday - 2.days).to_i,
              'end_time'            => (@end_of_yesterday - 2.days).to_i,
              'smartbites_created'  => 36,
              'smartbites_consumed' => 27,
              'time_spent_minutes'  => 0,
              'total_users'         => 0,
              'active_users'        => 0,
              'new_users'           => 0,
              'engagement_index'    => 0,
              'num_comments'        => 0,
              'num_likes'           => 0
            }],
            (@beginning_of_yesterday - 1.days).to_i => [{
              'organization_id'     => @org1.id,
              'team_id'             => @org1_team1.id,
              'start_time'          => (@beginning_of_yesterday - 1.days).to_i,
              'end_time'            => (@end_of_yesterday - 1.days).to_i,
              'smartbites_created'  => 24,
              'smartbites_consumed' => 18,
              'time_spent_minutes'  => 0,
              'total_users'         => 0,
              'active_users'        => 0,
              'new_users'           => 1,
              'engagement_index'    => 0,
              'num_comments'        => 0,
              'num_likes'           => 0
            }],
            @beginning_of_yesterday.to_i => [{
              'organization_id'     => @org1.id,
              'team_id'             => @org1_team1.id,
              'start_time'          => @beginning_of_yesterday.to_i,
              'end_time'            => @end_of_yesterday.to_i,
              'smartbites_created'  => 12,
              'smartbites_consumed' => 9,
              'time_spent_minutes'  => 0,
              'total_users'         => 2,
              'active_users'        => 0,
              'new_users'           => 1,
              'engagement_index'    => 0,
              'num_comments'        => 0,
              'num_likes'           => 0
            }]
          }
        },
        @org2.id => {
          @org2_team1.id => {
            (@beginning_of_yesterday - 2.days).to_i => [{
              'organization_id'     => @org2.id,
              'team_id'             => @org2_team1.id,
              'start_time'          => (@beginning_of_yesterday - 2.days).to_i,
              'end_time'            => (@end_of_yesterday - 2.days).to_i,
              'smartbites_created'  => 72,
              'smartbites_consumed' => 54,
              'time_spent_minutes'  => 108,
              'total_users'         => 0,
              'active_users'        => 2,
              'new_users'           => 0,
              'engagement_index'    => 0,
              'num_comments'        => 0,
              'num_likes'           => 0
            }],
            (@beginning_of_yesterday - 1.days).to_i => [{
              'organization_id'     => @org2.id,
              'team_id'             => @org2_team1.id,
              'start_time'          => (@beginning_of_yesterday - 1.days).to_i,
              'end_time'            => (@end_of_yesterday - 1.days).to_i,
              'smartbites_created'  => 48,
              'smartbites_consumed' => 36,
              'time_spent_minutes'  => 72,
              'total_users'         => 0,
              'active_users'        => 2,
              'new_users'           => 1,
              'engagement_index'    => 0,
              'num_comments'        => 0,
              'num_likes'           => 0
            }],
            @beginning_of_yesterday.to_i => [{
              'organization_id'     => @org2.id,
              'team_id'             => @org2_team1.id,
              'start_time'          => @beginning_of_yesterday.to_i,
              'end_time'            => @end_of_yesterday.to_i,
              'smartbites_created'  => 24,
              'smartbites_consumed' => 18,
              'time_spent_minutes'  => 36,
              'total_users'         => 2,
              'active_users'        => 2,
              'new_users'           => 1,
              'engagement_index'    => 100,
              'num_comments'        => 0,
              'num_likes'           => 0
            }]
          }
        }
      }
      assert_equal expected, actual
    ensure
      @klass.const_set "TIME_RANGE", orig_time_range
    end

  end

  test 'new_users and total_users are accurate even if users have no activity' do
    # Delete all user metrics aggregations, this metric shouldn't be dependent.
    UserMetricsAggregation.delete_all
    @klass.run
    actual = @fetch_records.call
    expected = {
      @org1.id => {
        @org1_team1.id => {
          @beginning_of_yesterday.to_i => [{
            'organization_id'     => @org1.id,
            'team_id'             => @org1_team1.id,
            'start_time'          => @beginning_of_yesterday.to_i,
            'end_time'            => @end_of_yesterday.to_i,
            'smartbites_created'  => 0,
            'smartbites_consumed' => 0,
            'time_spent_minutes'  => 0,
            'total_users'         => 2,
            'active_users'        => 0,
            'new_users'           => 1,
            'engagement_index'    => 0,
            'num_comments'        => 0,
            'num_likes'           => 0
          }]
        }
      },
      @org2.id => {
        @org2_team1.id => {
          @beginning_of_yesterday.to_i => [{
            'organization_id'     => @org2.id,
            'team_id'             => @org2_team1.id,
            'start_time'          => @beginning_of_yesterday.to_i,
            'end_time'            => @end_of_yesterday.to_i,
            'smartbites_created'  => 0,
            'smartbites_consumed' => 0,
            'time_spent_minutes'  => 0,
            'total_users'         => 2,
            'active_users'        => 0,
            'new_users'           => 1,
            'engagement_index'    => 0,
            'num_comments'        => 0,
            'num_likes'           => 0
          }]
        }
      }
    }
    assert_equal expected, actual
  end

  test 'num_comments and num_likes are accurate' do
    # Set comments and likes counts for some records
    # For yesterday, @org1_user1 has 1 comment and @org2_user1 has 1 like
    UserMetricsAggregation.find_by!(
      user: @org1_user1,
      start_time: @beginning_of_yesterday.to_i
    ).update(smartbites_commented_count: 1)
    UserMetricsAggregation.find_by!(
      user: @org2_user1,
      start_time: @beginning_of_yesterday.to_i
    ).update(smartbites_liked_count: 1)

    @klass.run
    actual = @fetch_records.call
    expected = {
      @org1.id => {
        @org1_team1.id => {
          @beginning_of_yesterday.to_i => [{
            'organization_id'     => @org1.id,
            'team_id'             => @org1_team1.id,
            'start_time'          => @beginning_of_yesterday.to_i,
            'end_time'            => @end_of_yesterday.to_i,
            'smartbites_created'  => 12,
            'smartbites_consumed' => 9,
            'time_spent_minutes'  => 0,
            'total_users'         => 2,
            'active_users'        => 0,
            'new_users'           => 1,
            'engagement_index'    => 0,
            'num_comments'        => 1,
            'num_likes'           => 0
          }]
        }
      },
      @org2.id => {
        @org2_team1.id => {
          @beginning_of_yesterday.to_i => [{
            'organization_id'     => @org2.id,
            'team_id'             => @org2_team1.id,
            'start_time'          => @beginning_of_yesterday.to_i,
            'end_time'            => @end_of_yesterday.to_i,
            'smartbites_created'  => 24,
            'smartbites_consumed' => 18,
            'time_spent_minutes'  => 36,
            'total_users'         => 2,
            'active_users'        => 2,
            'new_users'           => 1,
            'engagement_index'    => 100,
            'num_comments'        => 0,
            'num_likes'           => 1
          }]
        }
      }
    }
    assert_equal expected, actual
  end

end
require 'test_helper'

class InfluxQuery::EntityStrategiesTest < ActiveSupport::TestCase

  setup do
    @organization = create(:organization)
    @credential = create(:developer_api_credential, organization: @organization)
    @user = create :user, organization: @organization
    @dummy_params = {}
    @version = Settings.influxdb.event_tracking_version
  end

  ##### index ########
  # cards
  test 'should return list of events with default options' do
    now = Time.now.utc
    Time.stubs(:now).returns now

    start_date = (now - 7.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i

    query, params = InfluxQuery.get_query_for_entity(
      entity:  'cards',
      organization_id: @organization.id.to_s,
      filters: {
        start_date: start_date,
        end_date: end_date
      }
    )

    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from \"cards\"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      version: @version,
      organization_id: @organization.id.to_s,
      start_date: start_date,
      end_date: end_date,
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)
      .returns(sample_influx_event_data)

    results = InfluxQuery.fetch(query, params)

    assert_equal sample_influx_event_data, results
  end

  test 'should paginate' do
    now = Time.now.utc
    Time.stubs(:now).returns now

    start_date = (now - 10.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i
    query, params = InfluxQuery.get_query_for_entity(
      entity: 'cards',
      organization_id: 1,
      filters: {
        group_by: 'user_id',
        offset: 1,
        limit: 2,
        start_date: start_date,
        end_date: end_date
      }
    )
    expected = <<-TXT.chomp.strip_heredoc.tr("\n", " ")
      select * from \"cards\"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      GROUP BY #{params[:group_by]}
      ORDER BY ASC
      LIMIT %{limit}
      OFFSET %{offset} ;
    TXT
    assert_equal expected, query
    assert_equal params, {
      order: "ASC",
      version: @version,
      organization_id: '1',
      start_date: start_date,
      end_date: end_date,
      group_by: "user_id",
      limit: 2,
      offset: 1
    }
  end

  test 'should return list of events with group by and daterange' do
    now = Time.now.utc
    Time.stubs(:now).returns now

    start_date = (now - 10.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i
    group_by = 'event, user_id'

    query, params = InfluxQuery.get_query_for_entity(
      entity: 'cards',
      organization_id: @organization.id.to_s,
      filters: {
        group_by: group_by,
        start_date: start_date,
        end_date: end_date
      }
    )

    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from \"cards\"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      GROUP BY #{params[:group_by]}
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      version: @version,
      organization_id: @organization.id.to_s,
      start_date: start_date,
      end_date: end_date,
      group_by: group_by
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)
      .returns(sample_influx_event_data)

    results = InfluxQuery.fetch(query, params)
    assert_equal sample_influx_event_data, results
  end

  test 'should return list of cards events with matching card id' do
    now = Time.now.utc
    Time.stubs(:now).returns now

    start_date = (now - 7.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i

    query, params = InfluxQuery.get_query_for_entity(
      entity: 'cards',
      organization_id: @organization.id.to_s,
      filters: {
        card_id: 21,
        start_date: start_date,
        end_date: end_date
      }
    )

    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from \"cards\"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      AND _card_id = %{card_id}
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      version: @version,
      organization_id: @organization.id.to_s,
      card_id: 21,
      start_date: start_date,
      end_date: end_date
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)
      .returns(sample_influx_event_data)

    results = InfluxQuery.fetch(query, params)
    assert_equal sample_influx_event_data, results
  end

  test 'should return list of cards events with matching user id' do
    now = Time.now.utc
    Time.stubs(:now).returns now

    start_date = (now - 7.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i
    user_id = 21

    query, params = InfluxQuery.get_query_for_entity(
      entity: 'cards',
      organization_id: @organization.id.to_s,
      filters: {
        user_id: user_id,
        start_date: start_date,
        end_date: end_date
      }
    )

    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from \"cards\"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      AND _user_id = %{user_id}
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      version: @version,
      organization_id: @organization.id.to_s,
      start_date: start_date,
      end_date: end_date,
      user_id: user_id
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)
      .returns(sample_influx_event_data)
    results = InfluxQuery.fetch(query, params)
    assert_equal sample_influx_event_data, results
  end

  # users
  test 'should return list of users events with matching user id' do
    now = Time.now.utc
    Time.stubs(:now).returns now

    start_date = (now - 7.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i
    user_id = 21

    query, params = InfluxQuery.get_query_for_entity(
      entity: 'users',
      organization_id: @organization.id.to_s,
      filters: {
        user_id: user_id,
        start_date: start_date,
        end_date: end_date
      }
    )

    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from \"users\"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      AND _user_id = %{user_id}
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      version: @version,
      organization_id: @organization.id.to_s,
      start_date: start_date,
      end_date: end_date,
      user_id: user_id
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)
      .returns(sample_influx_event_data)

    results = InfluxQuery.fetch(query, params)
    assert_equal sample_influx_event_data, results
  end

  test 'should return list of users events' do
    now = Time.now.utc
    Time.stubs(:now).returns now

    start_date = (now - 7.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i

    query, params = InfluxQuery.get_query_for_entity(
      entity: 'users',
      organization_id: @organization.id.to_s,
      filters: {
        start_date: start_date,
        end_date: end_date
      }
    )

    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from \"users\"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      version: @version,
      organization_id: @organization.id.to_s,
      start_date: start_date,
      end_date: end_date,
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)
      .returns(sample_influx_event_data)

    results = InfluxQuery.fetch(query, params)

    assert_equal sample_influx_event_data, results
  end

  test 'should query for user_scores events' do
    now = Time.now.utc
    Time.stubs(:now).returns now
    start_date = (now - 7.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i
    query, params = InfluxQuery.get_query_for_entity(
      entity: 'user_scores',
      organization_id: @organization.id.to_s,
      filters: {
        role: "actor",
        user_id: "123",
        event: "card_viewed",
        start_date: start_date,
        end_date: end_date
      }
    )
    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from "user_scores"
      where org_id = %{organization_id}
      AND event = %{event}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      AND user_id = %{user_id}
      AND role = %{role}
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      version: @version,
      organization_id: @organization.id.to_s,
      start_date: start_date,
      end_date: end_date,
      role: "actor",
      user_id: "123",
      event: "card_viewed"
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)

    InfluxQuery.fetch(query, params)
  end


  test 'should query for user_scores_daily events' do
    now = Time.now.utc
    Time.stubs(:now).returns now
    start_date = (now - 7.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i
    query, params = InfluxQuery.get_query_for_entity(
      entity: 'user_scores_daily',
      organization_id: @organization.id.to_s,
      filters: {
        user_id: "123",
        start_date: start_date,
        end_date: end_date
      }
    )
    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from "user_scores_daily"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND user_id = %{user_id}
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      organization_id: @organization.id.to_s,
      start_date: start_date,
      end_date: end_date,
      user_id: "123",
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)

    InfluxQuery.fetch(query, params)
  end

  # groups
  test 'should return list of groups events with matching group id' do
    now = Time.now.utc
    Time.stubs(:now).returns now

    start_date = (now - 7.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i
    group_id = 21

    query, params = InfluxQuery.get_query_for_entity(
      entity: 'groups',
      organization_id: @organization.id.to_s,
      filters: {
        group_id: group_id,
        start_date: start_date,
        end_date: end_date
      }
    )

    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from \"groups\"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      AND _group_id = %{group_id}
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      version: @version,
      organization_id: @organization.id.to_s,
      start_date: start_date,
      end_date: end_date,
      group_id: group_id
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)
      .returns(sample_influx_event_data)

    results = InfluxQuery.fetch(query, params)
    assert_equal sample_influx_event_data, results
  end

  test 'should return list of groups events' do
    now = Time.now.utc
    Time.stubs(:now).returns now

    start_date = (now - 7.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i

    query, params = InfluxQuery.get_query_for_entity(
      entity: 'groups',
      organization_id: @organization.id.to_s,
      filters: {
        start_date: start_date,
        end_date: end_date
      }
    )

    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from \"groups\"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      version: @version,
      organization_id: @organization.id.to_s,
      start_date: start_date,
      end_date: end_date,
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)
      .returns(sample_influx_event_data)

    results = InfluxQuery.fetch(query, params)

    assert_equal sample_influx_event_data, results
  end

  # channels
  test 'should return list of channels events with matching channel id' do
    now = Time.now.utc
    Time.stubs(:now).returns now

    start_date = (now - 7.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i
    channel_id = 21

    query, params = InfluxQuery.get_query_for_entity(
      entity: 'channels',
      organization_id: @organization.id.to_s,
      filters: {
        channel_id: channel_id,
        start_date: start_date,
        end_date: end_date
      }
    )

    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from \"channels\"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      AND _channel_id = %{channel_id}
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      version: @version,
      organization_id: @organization.id.to_s,
      start_date: start_date,
      end_date: end_date,
      channel_id: channel_id
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)
      .returns(sample_influx_event_data)

    results = InfluxQuery.fetch(query, params)
    assert_equal sample_influx_event_data, results
  end

  test 'should return list of channels events' do
    now = Time.now.utc
    Time.stubs(:now).returns now

    start_date = (now - 7.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i

    query, params = InfluxQuery.get_query_for_entity(
      entity: 'channels',
      organization_id: @organization.id.to_s,
      filters: {
        start_date: start_date,
        end_date: end_date
      }
    )

    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from \"channels\"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      version: @version,
      organization_id: @organization.id.to_s,
      start_date: start_date,
      end_date: end_date,
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)
      .returns(sample_influx_event_data)

    results = InfluxQuery.fetch(query, params)

    assert_equal sample_influx_event_data, results
  end

  test 'should return list of searches requests' do
    now = Time.now.utc
    Time.stubs(:now).returns now

    start_date = (now - 7.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i
    user_id = 21

    query, params = InfluxQuery.get_query_for_entity(
      entity: 'searches',
      organization_id: @organization.id.to_s,
      filters: {
        user_id: user_id,
        start_date: start_date,
        end_date: end_date
      }
    )

    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from \"searches\"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      AND _user_id = %{user_id}
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      version: @version,
      organization_id: @organization.id.to_s,
      start_date: start_date,
      end_date: end_date,
      user_id: user_id
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)
      .returns(sample_influx_event_data)

    results = InfluxQuery.fetch(query, params)

    assert_equal sample_influx_event_data, results
  end

  test 'should return list of organizations events' do
    now = Time.now.utc
    Time.stubs(:now).returns now

    start_date = (now - 7.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i
    user_id = 21
    event = "foo"

    query, params = InfluxQuery.get_query_for_entity(
      entity: 'organizations',
      organization_id: @organization.id.to_s,
      filters: {
        user_id: user_id,
        event: event,
        start_date: start_date,
        end_date: end_date
      }
    )

    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from \"organizations\"
      where org_id = %{organization_id}
      AND event = %{event}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      AND _user_id = %{user_id}
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      version: @version,
      organization_id: @organization.id.to_s,
      start_date: start_date,
      event: event,
      end_date: end_date,
      user_id: user_id
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)
      .returns(sample_influx_event_data)

    results = InfluxQuery.fetch(query, params)

    assert_equal sample_influx_event_data, results
  end

  test 'should return list of org_scores_daily events' do
    now = Time.now.utc
    Time.stubs(:now).returns now

    start_date = (now - 7.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i

    query, params = InfluxQuery.get_query_for_entity(
      entity: 'org_scores_daily',
      organization_id: @organization.id.to_s,
      filters: {
        start_date: start_date,
        end_date: end_date
      }
    )

    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from \"org_scores_daily\"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      organization_id: @organization.id.to_s,
      start_date: start_date,
      end_date: end_date,
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)
      .returns(sample_influx_event_data)

    results = InfluxQuery.fetch(query, params)

    assert_equal sample_influx_event_data, results
  end

  test 'should return list of group_scores_daily events' do
    now = Time.now.utc
    Time.stubs(:now).returns now

    start_date = (now - 7.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i
    group_id = 22

    query, params = InfluxQuery.get_query_for_entity(
      entity: 'group_scores_daily',
      organization_id: @organization.id.to_s,
      filters: {
        group_id: group_id,
        start_date: start_date,
        end_date: end_date
      }
    )

    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from \"group_scores_daily\"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      AND group_id = %{group_id}
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      version: @version,
      organization_id: @organization.id.to_s,
      start_date: start_date,
      end_date: end_date,
      group_id: group_id
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)
      .returns(sample_influx_event_data)

    results = InfluxQuery.fetch(query, params)

    assert_equal sample_influx_event_data, results
  end

  test 'should return list of channel_aggregation_daily events' do
    now = Time.now.utc
    Time.stubs(:now).returns now

    start_date = (now - 7.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i
    channel_id = 22

    query, params = InfluxQuery.get_query_for_entity(
      entity: 'channel_aggregation_daily',
      organization_id: @organization.id.to_s,
      filters: {
        channel_id: channel_id,
        start_date: start_date,
        end_date: end_date
      }
    )

    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from \"channel_aggregation_daily\"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      AND channel_id = %{channel_id}
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      version: @version,
      organization_id: @organization.id.to_s,
      start_date: start_date,
      end_date: end_date,
      channel_id: channel_id
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)
      .returns(sample_influx_event_data)

    results = InfluxQuery.fetch(query, params)

    assert_equal sample_influx_event_data, results
  end

  test 'should return list of group_user_scores_daily events when given single group_id' do
    now = Time.now.utc
    Time.stubs(:now).returns now

    start_date = (now - 7.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i
    group_id = 22
    user_id = 23

    query, params = InfluxQuery.get_query_for_entity(
      entity: 'group_user_scores_daily',
      organization_id: @organization.id.to_s,
      filters: {
        user_id: user_id,
        group_ids: group_id,
        start_date: start_date,
        end_date: end_date
      }
    )

    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from \"group_user_scores_daily\"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      AND user_id = %{user_id}
      AND (group_id = %{group_id_0})
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      version: @version,
      organization_id: @organization.id.to_s,
      start_date: start_date,
      end_date: end_date,
      group_id_0: group_id,
      user_id: user_id
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)
      .returns(sample_influx_event_data)

    results = InfluxQuery.fetch(query, params)

    assert_equal sample_influx_event_data, results
  end

  test 'should return list of group_user_scores_daily events when given array of group_ids' do
    now = Time.now.utc
    Time.stubs(:now).returns now

    start_date = (now - 7.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i
    group_id = 22
    group_id_2 = 33
    user_id = 23

    query, params = InfluxQuery.get_query_for_entity(
      entity: 'group_user_scores_daily',
      organization_id: @organization.id.to_s,
      filters: {
        user_id: user_id,
        group_ids: [group_id, group_id_2],
        start_date: start_date,
        end_date: end_date
      }
    )

    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from \"group_user_scores_daily\"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      AND user_id = %{user_id}
      AND (group_id = %{group_id_0} OR group_id = %{group_id_1})
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      version: @version,
      organization_id: @organization.id.to_s,
      start_date: start_date,
      end_date: end_date,
      user_id: user_id,
      group_id_0: group_id,
      group_id_1: group_id_2
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)
      .returns(sample_influx_event_data)

    results = InfluxQuery.fetch(query, params)

    assert_equal sample_influx_event_data, results
  end

  test 'should return content_engagement_metrics events' do
    now = Time.now.utc
    Time.stubs(:now).returns now
    start_date = (now - 7.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i
    query, params = InfluxQuery.get_query_for_entity(
      entity: 'content_engagement_metrics',
      organization_id: @organization.id.to_s,
      filters: {
        start_date: start_date,
        end_date: end_date
      }
    )

    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from \"content_engagement_metrics\"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      version: @version,
      organization_id: @organization.id.to_s,
      start_date: start_date,
      end_date: end_date,
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)
      .returns(sample_influx_event_data)

    results = InfluxQuery.fetch(query, params)

    assert_equal sample_influx_event_data, results
  end

  test 'should return integrations events' do
    now = Time.now.utc
    Time.stubs(:now).returns now
    start_date = (now - 7.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i
    query, params = InfluxQuery.get_query_for_entity(
      entity: 'integrations',
      organization_id: @organization.id.to_s,
      filters: {
        start_date: start_date,
        end_date: end_date
      }
    )

    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from \"integrations\"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      version: @version,
      organization_id: @organization.id.to_s,
      start_date: start_date,
      end_date: end_date,
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)
      .returns(sample_influx_event_data)

    results = InfluxQuery.fetch(query, params)

    assert_equal sample_influx_event_data, results
  end

  test 'should return xapi_credentials events' do
    now = Time.now.utc
    Time.stubs(:now).returns now
    start_date = (now - 7.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i
    query, params = InfluxQuery.get_query_for_entity(
      entity: 'xapi_credentials',
      organization_id: @organization.id.to_s,
      filters: {
        start_date: start_date,
        end_date: end_date
      }
    )

    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from \"xapi_credentials\"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      version: @version,
      organization_id: @organization.id.to_s,
      start_date: start_date,
      end_date: end_date,
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)
      .returns(sample_influx_event_data)

    results = InfluxQuery.fetch(query, params)

    assert_equal sample_influx_event_data, results
  end

  test 'doesnt have risk of injection hack via parameterized variables' do
    skip # TEST FAILS UNLESS THE ORG HAS CARD RECORD IN INFLUX
    WebMock.disable!
    now = Time.now.utc
    Time.stubs(:now).returns now
    start_date = (Time.now - 7.days).beginning_of_day.to_i
    end_date = Time.now.end_of_day.to_i

    # Malicious user_id
    card_id = "1'; DROP MEASUREMENT cards; SELECT * FROM cards WHERE card_id='"

    query, params = InfluxQuery.get_query_for_entity(
      entity: 'cards',
      organization_id: @organization.id.to_s,
      filters: {
        card_id: card_id,
        start_date: start_date,
        end_date: end_date
      }
    )

    INFLUXDB_CLIENT.unstub :query

    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from \"cards\"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      AND _card_id = %{card_id}
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      version: @version,
      organization_id: @organization.id.to_s,
      start_date: start_date,
      end_date: end_date,
      card_id: card_id
    }

    InfluxQuery.fetch(query, params)

    # Query for the cards card to make sure they're still there:
    result = INFLUXDB_CLIENT.query("SELECT COUNT(card_id) FROM cards")[0]["values"][0]["count"]
    assert result > 0
    WebMock.enable!
  end

  test 'should not return results when event tracking version is different' do
    Settings.influxdb.stubs(:event_tracking_version).returns('1.0.0')

    now = Time.now.utc
    Time.stubs(:now).returns now

    start_date = (now - 7.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i

    query, params = InfluxQuery.get_query_for_entity(
      entity:  'cards',
      organization_id: @organization.id.to_s,
      filters: {
        start_date: start_date,
        end_date: end_date
      }
    )

    assert_equal query, <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
      select * from \"cards\"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      ORDER BY ASC ;
    HEREDOC

    assert_equal params, {
      order: "ASC",
      version: '1.0.0',
      organization_id: @organization.id.to_s,
      start_date: start_date,
      end_date: end_date,
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)
      .returns([])

    results = InfluxQuery.fetch(query, params)

    assert_empty results
  end

  # dummy influx event response
  def sample_influx_event_data
    [
      {
          "name": "cards",
          "tags": {
              "event": "card_liked"
          },
          "values": [
              {
                  "time": "2017-10-02T00:57:04Z",
                  "author_id": "12",
                  "author_name": "admin1",
                  "card_id": "21",
                  "message": "https://techcrunch.com/2017/09/21/zenefits-tries-a-rebrand-and-hands-off-insurance-brokerage/",
                  "organization_hostname": "org47",
                  "organization_id": "2",
                  "organization_name": "Organization 47",
                  "promoted": "0",
                  "resource_url": "https://techcrunch.com/2017/09/21/zenefits-tries-a-rebrand-and-hands-off-insurance-brokerage/",
                  "state": "published",
                  "subtype": "link",
                  "title": nil,
                  "type": "media",
                  "user_handle": nil,
                  "user_id": "12",
                  "user_name": "admin1"
              }
          ]
      }
    ]
  end

  def query
    query.gsub("\n", ' ').strip
  end
end

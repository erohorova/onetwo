require 'test_helper'
class CardBookmarkRecorderTest < ActiveSupport::TestCase

  setup do
    UserContentsQueue.stubs(:remove_from_queue).returns true

    author_first_name = Faker::Name.first_name
    actor_first_name = Faker::Name.first_name

    author_last_name = Faker::Name.last_name
    actor_last_name = Faker::Name.last_name

    @org = create(:organization, name: 'LXP', host_name: 'spark' )
    @author = create(
      :user,
      first_name: author_first_name,
      last_name: author_last_name,
      organization: @org
    )
    @actor = create(
      :user,
      first_name: actor_first_name,
      last_name: actor_last_name,
      organization: @org
    )
    @card = create(:card, author: @author, created_at: 2.days.ago)

    @bookmark = create(:bookmark, bookmarkable: @card, user: @actor)
  end

  test 'valid registered events' do
    registered_events = ['card_bookmarked', 'card_unbookmarked']
    assert_equal(
      registered_events,
      Analytics::CardBookmarkRecorder::CARD_BOOKMARK_EVENTS
    )
  end

  test 'valid record args' do
    invalid_event = 'invalid event'
    InfluxdbRecorder.stubs(:record).returns :true
    timestamp = Time.now.to_i
    assert_nothing_raised do
      Analytics::CardBookmarkRecorder.record(
        bookmark_id: @bookmark.id,
        org: Analytics::MetricsRecorder.org_attributes(@org),
        card: Analytics::MetricsRecorder.card_attributes(@bookmark.bookmarkable),
        actor: Analytics::MetricsRecorder.user_attributes(@bookmark.user),
        timestamp: timestamp,
        event: 'card_bookmarked'
      )
      Analytics::CardBookmarkRecorder.record(
        org: Analytics::MetricsRecorder.org_attributes(@org),
        bookmark_id: @bookmark.id,
        card: Analytics::MetricsRecorder.card_attributes(@bookmark.bookmarkable),
        actor: Analytics::MetricsRecorder.user_attributes(@bookmark.user),
        timestamp: timestamp,
        event: 'card_unbookmarked'
      )
    end

    assert_raises do
      Analytics::CardBookmarkRecorder.record(
        org: Analytics::MetricsRecorder.org_attributes(@org),
        bookmark_id: @bookmark.id,
        card: Analytics::MetricsRecorder.card_attributes(@bookmark.bookmarkable),
        actor: Analytics::MetricsRecorder.user_attributes(@bookmark.user),
        timestamp: timestamp,
        event: invalid_event
      )
    end
  end

  test 'should record card vote event' do
    stub_time
    InfluxdbRecorder.unstub(:record)
    card_bookmark = Analytics::CardBookmarkRecorder::EVENT_CARD_BOOKMARKED

    field_set = card_field_set(
      @org, @bookmark.bookmarkable, @bookmark.user
    )

    tag_set = card_tag_set(
      @bookmark.bookmarkable, @bookmark.user, card_bookmark
    )

    InfluxdbRecorder.expects(:record).with('cards',
      {
        values: field_set.symbolize_keys.merge(
          {
            card_bookmark_id: @bookmark.id.to_s
          }
        ).compact,
        tags: tag_set.compact,
        timestamp: Time.now.to_i
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true

    Analytics::CardBookmarkRecorder.record(
      bookmark_id: @bookmark.id,
      org: Analytics::MetricsRecorder.org_attributes(@org),
      card: Analytics::MetricsRecorder.card_attributes(@bookmark.bookmarkable),
      actor: Analytics::MetricsRecorder.user_attributes(@bookmark.user),
      timestamp: Time.now.to_i,
      event: card_bookmark,
      additional_data: {
        platform: 'web',
        user_agent: 'Edcast iPhone',
        is_admin_request: 0
      }
    )
  end
end
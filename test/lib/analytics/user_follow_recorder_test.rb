require 'test_helper'
class UserFollowRecorderTest < ActiveSupport::TestCase

  setup do
    author_first_name = Faker::Name.first_name
    actor_first_name = Faker::Name.first_name

    author_last_name = Faker::Name.last_name
    actor_last_name = Faker::Name.last_name

    @org = create(:organization, name: 'LXP', host_name: 'spark' )
    @user = create(:user, first_name: author_first_name,
      last_name: author_last_name, organization: @org,
      sign_in_count: 2, last_sign_in_at: 2.days.ago,
      last_sign_in_ip: Faker::Internet.ip_v4_address,
      current_sign_in_ip: Faker::Internet.ip_v4_address
    )

    @followed = create(:user, organization: @org)
    @follow = create(:follow, user: @user, followable: @followed)
  end

  test 'valid registered events' do
    registered_events = ['user_followed', 'user_unfollowed']
    assert_equal registered_events, Analytics::UserFollowRecorder::USER_FOLLOW_EVENTS
  end

  test 'valid record args' do
    invalid_event = 'invalid event'
    InfluxdbRecorder.stubs(:record).returns :true
    assert_nothing_raised do
      Analytics::UserFollowRecorder.record(
        org: Analytics::MetricsRecorder.org_attributes(@org),
        timestamp: Time.now.to_i,
        actor: Analytics::MetricsRecorder.user_attributes(@user),
        followed: Analytics::MetricsRecorder.org_attributes(@follow.followable),
        follower: Analytics::MetricsRecorder.org_attributes(@follow.user),
        event: Analytics::UserMetricsRecorder::EVENT_USER_FOLLOWED,
        additional_data: {}
      )
    end

    assert_raises do
      Analytics::UserFollowRecorder.record(
        org: Analytics::MetricsRecorder.org_attributes(@org),
        timestamp: Time.now.to_i,
        actor: Analytics::MetricsRecorder.user_attributes(@user),
        followed: Analytics::MetricsRecorder.org_attributes(@follow.followable),
        follower: Analytics::MetricsRecorder.org_attributes(@follow.user),
        event: invalid_event,
        additional_data: {}
      )
    end
  end

  test 'should record user follow event' do
    Role.create_master_roles(@org)
    InfluxdbRecorder.unstub(:record)

    # user roles
    admin_role = @org.roles.find_by_name('admin')
    @user.roles << admin_role
    @user.onboard!

    actor = create(:user, organization: @org)

    # identity providers
    fb_ip_provider = create(:identity_provider, type: 'FacebookProvider',
      uid: 'SPARK123', user: actor
    )

    user_follow = Analytics::UserMetricsRecorder::EVENT_USER_FOLLOWED

    field_set = {}
    field_set[:sign_in_ip] = actor.last_sign_in_ip
    field_set[:sign_in_at] = actor.last_sign_in_at.to_i.to_s
    field_set[:user_id] = actor.id.to_s
    field_set[:full_name] = actor.full_name
    field_set[:user_handle] = actor.handle
    field_set[:follower_id] = @user.id.to_s
    field_set[:user_role] = actor.organization_role
    field_set[:user_agent] = 'Edcast iPhone'
    field_set[:org_hostname] = actor.organization.host_name
    field_set[:user_org_uid] = fb_ip_provider.uid
    field_set[:onboarding_status] = '1'
    field_set[:followed_user_id] = @followed.id.to_s
    field_set[:followed_user_handle] = @followed.handle
    field_set[:is_admin_request] = '0'
    field_set[:followed_user_full_name] = @followed.full_name
    field_set[:platform] = 'web'

    tag_set = {}
    tag_set[:is_system_generated] = '0'
    tag_set[:_user_id] = actor.id.to_s
    tag_set[:event] = user_follow
    tag_set[:org_id] = actor.organization_id.to_s
    timestamp = Time.now.to_i

    InfluxdbRecorder.expects(:record).with('users',
      {
        values: field_set.compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true
    Analytics::UserFollowRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      followed: Analytics::MetricsRecorder.user_attributes(@follow.followable),
      follower: Analytics::MetricsRecorder.user_attributes(@follow.user),
      timestamp: timestamp,
      event: user_follow,
      additional_data: {
        platform: 'web',
        user_agent: 'Edcast iPhone',
        is_admin_request: 0
      }
    )
  end

  test 'should record user unfollow event' do
    INFLUXDB_CLIENT.stubs :write_point
    InfluxdbRecorder.unstub :record
    Follow.any_instance.stubs(:record_user_follow_action)
    Role.create_master_roles(@org)

    # user roles
    admin_role = @org.roles.find_by_name('admin')
    @user.roles << admin_role
    @user.onboard!

    actor = create(:user, organization: @org)

    # identity providers
    fb_ip_provider = create(:identity_provider, type: 'FacebookProvider',
      uid: 'SPARK123', user: actor
    )

    user_unfollow = Analytics::UserMetricsRecorder::EVENT_USER_UNFOLLOWED
    @follow.destroy #unfollow

    field_set = {}
    field_set[:sign_in_ip] = actor.last_sign_in_ip
    field_set[:sign_in_at] = actor.last_sign_in_at.to_i.to_s
    field_set[:user_id] = actor.id.to_s
    field_set[:full_name] = actor.full_name
    field_set[:user_handle] = actor.handle
    field_set[:follower_id] = @user.id.to_s
    field_set[:user_role] = actor.organization_role
    field_set[:user_agent] = 'Edcast iPhone'
    field_set[:org_hostname] = actor.organization.host_name
    field_set[:user_org_uid] = fb_ip_provider.uid
    field_set[:onboarding_status] = '1'
    field_set[:followed_user_id] = @followed.id.to_s
    field_set[:is_admin_request] = '0'
    field_set[:followed_user_full_name] = @followed.full_name
    field_set[:followed_user_handle] = @followed.handle
    field_set[:platform] = 'web'

    tag_set = {}
    tag_set[:is_system_generated] = '0'
    tag_set[:_user_id] = actor.id.to_s
    tag_set[:event] = user_unfollow
    tag_set[:org_id] = actor.organization_id.to_s
    timestamp = Time.now.to_i

    timestamp = Time.now.to_i

    InfluxdbRecorder.expects(:record).with('users',
      {
        values: field_set.compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true
    Analytics::UserFollowRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      followed: Analytics::MetricsRecorder.user_attributes(@follow.followable),
      follower: Analytics::MetricsRecorder.user_attributes(@follow.user),
      timestamp: timestamp,
      event: user_unfollow,
      additional_data: {
        platform: 'web',
        user_agent: 'Edcast iPhone',
        is_admin_request: 0
      }
    )
  end
end

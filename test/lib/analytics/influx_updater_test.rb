require 'test_helper'

class InfluxUpdaterTest < ActiveSupport::TestCase

  setup do
    @class = Analytics::InfluxUpdater
  end

  test 'analytics_version' do
    assert_equal(
      InfluxdbRecorder::ANALYTICS_VERSION,
      @class.analytics_version
    )
  end

  # TODO: more tests

end
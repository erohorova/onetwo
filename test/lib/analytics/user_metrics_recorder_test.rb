require 'test_helper'
class UserMetricsRecorderTest < ActiveSupport::TestCase

  setup do
    author_first_name = Faker::Name.first_name
    actor_first_name = Faker::Name.first_name

    author_last_name = Faker::Name.last_name
    actor_last_name = Faker::Name.last_name

    @org = create :organization,
      name: 'LXP',
      host_name: 'spark'

    @user = create :user,
      first_name: author_first_name,
      last_name: author_last_name,
      organization: @org,
      sign_in_count: 2,
      last_sign_in_at: 2.days.ago,
      last_sign_in_ip: Faker::Internet.ip_v4_address,
      current_sign_in_ip: Faker::Internet.ip_v4_address

    @klass = Analytics::UserMetricsRecorder
  end

  test 'valid registered events' do
    registered_events = {
      "user_created"                 => true,
      "user_followed"                => true,
      "user_unfollowed"              => true,
      "user_logged_in"               => true,
      "user_expertise_topic_added"   => true,
      "user_expertise_topic_removed" => true,
      "user_interest_topic_added"    => true,
      "user_interest_topic_removed"  => true,
      "user_edited"                  => true,
      "user_role_added"              => true,
      "user_role_removed"            => true
    }
    assert_equal registered_events, @klass::VALID_EVENTS
  end

  test 'valid record args' do
    invalid_event = 'invalid event'
    InfluxdbRecorder.stubs(:record).returns :true

    assert_nothing_raised do
      @klass.record(
        timestamp: Time.now.to_i,
        user: Analytics::MetricsRecorder.user_attributes(@user),
        actor: Analytics::MetricsRecorder.user_attributes(@user),
        org: Analytics::MetricsRecorder.org_attributes(@org),
        event: 'user_created'
      )
    end

    assert_raises do
      @klass.record(
        timestamp: Time.now.to_i,
        user: Analytics::MetricsRecorder.user_attributes(@user),
        actor: Analytics::MetricsRecorder.user_attributes(@user),
        org: Analytics::MetricsRecorder.org_attributes(@org),
        event: invalid_event
      )
    end
  end

  test 'should record user create event' do
    created_user = create :user, organization: @org
    InfluxdbRecorder.unstub :record
    INFLUXDB_CLIENT.stubs(:write_point)
    Role.create_master_roles(@org)

    # user roles
    admin_role = @org.roles.find_by_name('admin')
    @user.roles << admin_role

    # user profile
    create(:user_profile, user: @user)

    @user.onboard!

    user_profile = @user.profile
    user_profile.update(expert_topics: Taxonomies.domain_topics,
      learning_topics: Taxonomies.domain_topics
    )

    # identity providers
    fb_ip_provider = create(:identity_provider, type: 'FacebookProvider',
      uid: 'SPARK123', user: @user
    )

    google_ip_provider = create(:identity_provider, type: 'GoogleProvider',
      uid: 'Google123', user: @user
    )

    expert_topics = learning_topics = Taxonomies.domain_topics.collect {|t| t['topic_label']}.join(",")

    user_create = @klass::EVENT_USER_CREATED

    field_set = {}
    field_set[:sign_in_ip] = @user.last_sign_in_ip
    field_set[:sign_in_at] = @user.last_sign_in_at.to_i.to_s
    field_set[:user_id] = @user.id.to_s
    field_set[:user_role] = @user.organization_role
    field_set[:user_handle] = @user.handle
    field_set[:user_agent] = 'chrome'
    field_set[:org_hostname] = @user.organization.host_name
    field_set[:onboarding_status] = '1'
    field_set[:user_org_uid] = fb_ip_provider.uid
    field_set[:is_admin_request] = '0'
    field_set[:full_name] = @user.full_name
    field_set[:platform] = 'web'
    field_set[:created_user_id] = created_user.id.to_s
    field_set[:upload_mode] = "direct_sign_up"

    tag_set = { }
    tag_set[:is_system_generated] = '0'
    tag_set[:_user_id] = @user.id.to_s
    tag_set[:org_id] = @user.organization_id.to_s
    tag_set[:event] = user_create

    timestamp = Time.now.to_i

    InfluxdbRecorder.expects(:record).with('users',
      {
        values: field_set.compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true
    @klass.record(
      actor: Analytics::MetricsRecorder.user_attributes(@user),
      user: Analytics::MetricsRecorder.user_attributes(created_user),
      org: Analytics::MetricsRecorder.org_attributes(@org),
      event: user_create,
      timestamp: timestamp,
      additional_data: {
        platform: 'web',
        user_agent: 'chrome',
        is_admin_request: 0
      }
    )
  end

  test 'should record user create event with custom upload_mode' do
    created_user = create :user, organization: @org
    InfluxdbRecorder.unstub :record
    INFLUXDB_CLIENT.stubs(:write_point)
    Role.create_master_roles(@org)

    # user roles
    admin_role = @org.roles.find_by_name('admin')
    @user.roles << admin_role

    # user profile
    create(:user_profile, user: @user)

    @user.onboard!
    @user.upload_mode = "bulk_import"

    user_profile = @user.profile
    user_profile.update(expert_topics: Taxonomies.domain_topics,
      learning_topics: Taxonomies.domain_topics
    )

    # identity providers
    fb_ip_provider = create(:identity_provider, type: 'FacebookProvider',
      uid: 'SPARK123', user: @user
    )

    google_ip_provider = create(:identity_provider, type: 'GoogleProvider',
      uid: 'Google123', user: @user
    )

    expert_topics = learning_topics = Taxonomies.domain_topics.collect {|t| t['topic_label']}.join(",")

    user_create = @klass::EVENT_USER_CREATED

    field_set = {}
    field_set[:sign_in_ip] = @user.last_sign_in_ip
    field_set[:sign_in_at] = @user.last_sign_in_at.to_i.to_s
    field_set[:user_id] = @user.id.to_s
    field_set[:user_role] = @user.organization_role
    field_set[:user_handle] = @user.handle
    field_set[:user_agent] = 'chrome'
    field_set[:org_hostname] = @user.organization.host_name
    field_set[:onboarding_status] = '1'
    field_set[:user_org_uid] = fb_ip_provider.uid
    field_set[:is_admin_request] = '0'
    field_set[:full_name] = @user.full_name
    field_set[:platform] = 'web'
    field_set[:created_user_id] = created_user.id.to_s
    field_set[:upload_mode] = "direct_sign_up"

    tag_set = { }
    tag_set[:is_system_generated] = '0'
    tag_set[:_user_id] = @user.id.to_s
    tag_set[:org_id] = @user.organization_id.to_s
    tag_set[:event] = user_create

    timestamp = Time.now.to_i

    InfluxdbRecorder.expects(:record).with('users',
      {
        values: field_set.compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true
    @klass.record(
      actor: Analytics::MetricsRecorder.user_attributes(@user),
      user: Analytics::MetricsRecorder.user_attributes(created_user),
      org: Analytics::MetricsRecorder.org_attributes(@org),
      event: user_create,
      timestamp: timestamp,
      additional_data: {
        platform: 'web',
        user_agent: 'chrome',
        is_admin_request: 0
      }
    )
  end

  test "records user_role_added change event" do
    org = create :organization
    member = create :user, organization: org
    field_set = {}
    field_set[:sign_in_ip] = @user.last_sign_in_ip
    field_set[:sign_in_at] = @user.last_sign_in_at.to_i.to_s
    field_set[:user_id] = @user.id.to_s
    field_set[:user_handle] = @user.handle
    field_set[:user_role] = @user.organization_role
    field_set[:user_agent] = 'chrome'
    field_set[:org_hostname] = @user.organization.host_name
    field_set[:onboarding_status] = '1'
    field_set[:is_admin_request] = '0'
    field_set[:added_role] = "fake_added_role"
    field_set[:removed_role] = "fake_removed_role"
    field_set[:full_name] = @user.full_name
    field_set[:platform] = 'web'
    field_set[:member_user_id] = member.id.to_s

    tag_set = { }
    tag_set[:is_system_generated] = '0'
    tag_set[:_user_id] = @user.id.to_s
    tag_set[:org_id] = @user.organization_id.to_s
    tag_set[:event] = "user_role_added"

    timestamp = Time.now.to_i

    InfluxdbRecorder.expects(:record).with('users',
      {
        values: field_set.compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true
    @klass.record(
      actor: Analytics::MetricsRecorder.user_attributes(@user),
      user: Analytics::MetricsRecorder.user_attributes(member),
      org: Analytics::MetricsRecorder.org_attributes(@org),
      event: "user_role_added",
      timestamp: timestamp,
      added_role: "fake_added_role",
      removed_role: "fake_removed_role",
      additional_data: {
        platform: 'web',
        user_agent: 'chrome',
        is_admin_request: 0
      }
    )
  end

  test "records user_role_removed change event" do
    org = create :organization
    member = create :user, organization: org
    field_set = {}
    field_set[:sign_in_ip] = @user.last_sign_in_ip
    field_set[:sign_in_at] = @user.last_sign_in_at.to_i.to_s
    field_set[:user_id] = @user.id.to_s
    field_set[:user_handle] = @user.handle
    field_set[:user_role] = @user.organization_role
    field_set[:user_agent] = 'chrome'
    field_set[:org_hostname] = @user.organization.host_name
    field_set[:onboarding_status] = '1'
    field_set[:is_admin_request] = '0'
    field_set[:added_role] = "fake_added_role"
    field_set[:removed_role] = "fake_removed_role"
    field_set[:full_name] = @user.full_name
    field_set[:platform] = 'web'
    field_set[:member_user_id] = member.id.to_s

    tag_set = { }
    tag_set[:is_system_generated] = '0'
    tag_set[:_user_id] = @user.id.to_s
    tag_set[:org_id] = @user.organization_id.to_s
    tag_set[:event] = "user_role_removed"

    timestamp = Time.now.to_i

    InfluxdbRecorder.expects(:record).with('users',
      {
        values: field_set.compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true
    @klass.record(
      actor: Analytics::MetricsRecorder.user_attributes(@user),
      user: Analytics::MetricsRecorder.user_attributes(member),
      org: Analytics::MetricsRecorder.org_attributes(@org),
      event: "user_role_removed",
      timestamp: timestamp,
      added_role: "fake_added_role",
      removed_role: "fake_removed_role",
      additional_data: {
        platform: 'web',
        user_agent: 'chrome',
        is_admin_request: 0
      }
    )
  end

  test 'records user_edited event for whitelisted columns' do
    whitelist = %w{foo bar}
    @klass.stubs(:whitelisted_change_attrs)
                                  .returns(whitelist)
    do_record = -> (changed_col) {
      @klass.record(
        org: {},
        user: {},
        actor: {},
        event: "user_edited",
        changed_column: changed_col,
        additional_data: {},
        timestamp: 0
      )
    }
    whitelist.each do |changed_col|
      @klass.expects(:influxdb_record)
      do_record.call(changed_col)
    end
  end


  test 'doesnt record user_edited event for non-whitelisted columns' do
    whitelist = %w{foo bar}
    test_columns = %w{asd bsd}
    @klass.stubs(:whitelisted_change_attrs)
                                  .returns(whitelist)
    do_record = -> (changed_col) {
      @klass.record(
        org: {},
        user: {},
        actor: {},
        event: "user_edited",
        changed_column: changed_col,
        additional_data: {},
        timestamp: 0
      )
    }
    @klass.expects(:influxdb_record).never
    test_columns.each do |changed_col|
      do_record.call(changed_col)
    end
  end

  test 'whitelisted_change_attrs' do
    assert_equal(
      %w{
        email
        first_name
        last_name
        handle
        bio
        is_complete
        organization_role
        parent_user_id
        locked_at
        is_active
        hide_from_leaderboard
        exclude_from_leaderboard
        default_team_id
      },
      @klass.whitelisted_change_attrs
    )
  end

  test 'is_ignored_event?' do
    whitelist = %w{foo bar}
    @klass.stubs(:whitelisted_change_attrs)
                                    .returns(whitelist)
    assert @klass.is_ignored_event? "asd"
    refute @klass.is_ignored_event? "foo"
    refute @klass.is_ignored_event? nil
  end

  test "records user_edited event" do
    InfluxdbRecorder.unstub :record
    metadata = {
      platform: "platform",
      user_agent: "user_agent",
      is_admin_request: 0,
      platform_version_number: "platform_version_number"
    }
    timestamp = Time.now.to_i
    InfluxdbRecorder.expects(:record).with( 'users',
      {
        values: {
          org_hostname: @org.host_name,
          onboarding_status: '1',
          is_admin_request: '0',
          sign_in_ip: @user.last_sign_in_ip,
          sign_in_at: @user.last_sign_in_at.to_i.to_s,
          user_id: @user.id.to_s,
          edited_user_id: @user.id.to_s,
          user_handle: @user.handle,
          user_role: 'member',
          platform_version_number: 'platform_version_number',
          user_agent: 'user_agent',
          changed_column: 'handle',
          old_val: '@old_handle',
          new_val: '0',
          full_name: @user.full_name,
          platform: 'platform',
        },
        tags: {
          is_system_generated: '0',
          _user_id: @user.id.to_s,
          org_id: @org.id.to_s,
          event: 'user_edited',
        },
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    @klass.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      actor: Analytics::MetricsRecorder.user_attributes(@user),
      user: Analytics::MetricsRecorder.user_attributes(@user),
      event: "user_edited",
      changed_column: "handle",
      old_val: "@old_handle",
      new_val: 0, # will get converted to string
      additional_data: metadata,
      timestamp: timestamp
    )
  end

  test "distinguishes actor and user in user_edited event" do
    actor = create :user, organization: @org, last_sign_in_ip: "foo"
    InfluxdbRecorder.unstub :record
    metadata = {
      platform: "platform",
      user_agent: "user_agent",
      is_admin_request: 0,
      platform_version_number: "platform_version_number"
    }
    timestamp = Time.now.to_i
    InfluxdbRecorder.expects(:record).with( 'users',
      {
        values: {
          org_hostname: @org.host_name,
          onboarding_status: '1',
          is_admin_request: '0',
          sign_in_ip: actor.last_sign_in_ip,
          sign_in_at: actor.last_sign_in_at.to_i.to_s,
          user_id: actor.id.to_s,
          edited_user_id: @user.id.to_s,
          user_handle: actor.handle,
          user_role: 'member',
          platform_version_number: 'platform_version_number',
          user_agent: 'user_agent',
          changed_column: 'handle',
          old_val: '@old_handle',
          new_val: '0',
          full_name: actor.full_name,
          platform: 'platform',
        },
        tags: {
          is_system_generated: '0',
          _user_id: actor.id.to_s,
          org_id: @org.id.to_s,
          event: 'user_edited',
        },
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    @klass.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      user: Analytics::MetricsRecorder.user_attributes(@user),
      event: "user_edited",
      changed_column: "handle",
      old_val: "@old_handle",
      new_val: 0, # will get converted to string
      additional_data: metadata,
      timestamp: timestamp
    )
  end

end

require 'test_helper'
class OrgBadgingMetricsRecorderTest < ActiveSupport::TestCase
  setup do
    admin_first_name = Faker::Name.first_name
    admin_last_name = Faker::Name.last_name

    @org = create(:organization, name: 'LXP', host_name: 'spark' )
    @admin = create(:user, first_name: admin_first_name,
      last_name: admin_last_name, organization: @org,
      sign_in_count: 2, last_sign_in_at: 2.days.ago,
      last_sign_in_ip: Faker::Internet.ip_v4_address,
      current_sign_in_ip: Faker::Internet.ip_v4_address
    )
  end

  test 'valid registered events' do
    registered_events = 'org_badging_created'
    assert Analytics::OrgBadgingMetricsRecorder::ORG_BADGING_EVENTS.include? registered_events
  end

  test 'invalid events' do
    invalid_event = 'org_badging_created_invalid'
    badge = create(:badge, organization: @org, type: "CardBadge")
    pathway = create(:card, organization: @org, card_type: "pack", author: @admin)
    badging = Badging.create(badge_id: badge.id, badgeable_id: pathway.id, title: "titleee")

     metadata = {
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0,
      user_id: @admin.id
     }
    timestamp = Time.now.to_i
    exception = assert_raise do 
      Analytics::OrgBadgingMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      actor: Analytics::MetricsRecorder.user_attributes(@admin),
      event: invalid_event,
      timestamp: timestamp,
      badging: Analytics::MetricsRecorder.card_badging_attributes(badging),
      additional_data: metadata
    )
    end
    assert_equal exception.class, RuntimeError
    assert_equal "Invalid event: #{invalid_event}. Valid events: #{Analytics::OrgBadgingMetricsRecorder::ORG_BADGING_EVENTS.join(",")}", exception.message
  end

  test 'includes badging attributes' do
    badge = create(:badge, organization: @org, type: "CardBadge")
    pathway = create(:card, organization: @org, card_type: "pack", author: @admin)
    badging = Badging.create(
      badge_id: badge.id, 
      badgeable_id: pathway.id, 
      title: "titleee", 
      type: "CardBadging", 
      target_steps: 100,
      all_quizzes_answered: true
    )
    event = "org_badging_created"

    InfluxdbRecorder.unstub :record
    timestamp = Time.now.to_i


    metadata = {
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0,
      user_id: @admin.id
    }
    InfluxdbRecorder.expects(:record).with( 'organizations',
      {
        tags: {
          org_id: @org.id.to_s,
          event: event,
          _user_id: @admin.id.to_s,
          is_system_generated: '1'
        },
        values: {
          org_name: @org.name,
          org_slug: @org.slug,
          org_host_name: @org.host_name,
          org_is_open: @org.is_open ? '1' : '0',
          org_is_registrable: @org.is_registrable ? '1' : '0',
          org_status: @org.status,
          org_send_weekly_activity: @org.send_weekly_activity ? '1' : '0',
          org_show_sub_brand_on_login: @org.show_sub_brand_on_login ? '1' : '0',
          org_savannah_app_id: @org.savannah_app_id.to_s,
          org_social_enabled: @org.social_enabled ? '1' : '0',
          org_xapi_enabled: @org.xapi_enabled ? '1' : '0',
          org_show_onboarding: @org.show_onboarding ? '1' : '0',
          org_enable_role_based_authorization: @org.enable_role_based_authorization ? '1' : '0',
          platform_version_number: 'platform_version_number',
          user_agent: 'user_agent',
          user_id: @admin.id.to_s,
          is_admin_request: '0',
          badging_id:                      badging.id.to_s,
          badging_title:                   badging.title,
          badging_badge_id:                badging.badge_id.to_s,
          badging_badgeable_id:            badging.badgeable_id.to_s,
          badging_type:                    badging.type,
          badging_target_steps:            badging.target_steps,
          badging_all_quizzes_answered:    badging.all_quizzes_answered
        },
        timestamp: timestamp
      },
      {
        write_client:    nil,
        duplicate_check: true
      }
    )
    Analytics::OrgBadgingMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      actor: Analytics::MetricsRecorder.user_attributes(@admin),
      event: event,
      badging: Analytics::MetricsRecorder.card_badging_attributes(badging),
      timestamp: timestamp,
      additional_data: metadata
    )
  end

  test 'includes clc attributes' do
    clc_badging = Clc.create(
      entity: @org, 
      from_date: Time.now, 
      to_date: Time.now + 2.weeks, 
      target_score: 100,
      target_steps: 100,
      organization_id: @org.id,
      name: "CLC BADGE TEST"
    )
    event = "org_clc_created"

    InfluxdbRecorder.unstub :record
    timestamp = Time.now.to_i


    metadata = {
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0,
      user_id: @admin.id
    }
    InfluxdbRecorder.expects(:record).with( 'organizations',
      {
        tags: {
          org_id: @org.id.to_s,
          event: event,
          _user_id: @admin.id.to_s,
          is_system_generated: '1'
        },
        values: {
          org_name: @org.name,
          org_slug: @org.slug,
          org_host_name: @org.host_name,
          org_is_open: @org.is_open ? '1' : '0',
          org_is_registrable: @org.is_registrable ? '1' : '0',
          org_status: @org.status,
          org_send_weekly_activity: @org.send_weekly_activity ? '1' : '0',
          org_show_sub_brand_on_login: @org.show_sub_brand_on_login ? '1' : '0',
          org_savannah_app_id: @org.savannah_app_id.to_s,
          org_social_enabled: @org.social_enabled ? '1' : '0',
          org_xapi_enabled: @org.xapi_enabled ? '1' : '0',
          org_show_onboarding: @org.show_onboarding ? '1' : '0',
          org_enable_role_based_authorization: @org.enable_role_based_authorization ? '1' : '0',
          platform_version_number: 'platform_version_number',
          user_agent: 'user_agent',
          user_id: @admin.id.to_s,
          is_admin_request: '0',
          clc_id:              clc_badging.id.to_s,
          clc_entity_id:       clc_badging.entity_id.to_s,
          clc_entity_type:     clc_badging.entity_type,
          clc_from_date:       clc_badging.from_date.to_time.to_i,
          clc_to_date:         clc_badging.to_date.to_time.to_i,
          clc_target_score:    clc_badging.target_score.to_s,
          clc_target_steps:    clc_badging.target_steps.to_s,
          clc_name:            clc_badging.name
        },
        timestamp: timestamp
      },
      {
        write_client:    nil,
        duplicate_check: true
      }
    )
    Analytics::OrgBadgingMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      actor: Analytics::MetricsRecorder.user_attributes(@admin),
      event: event,
      clc: Analytics::MetricsRecorder.clc_attributes(clc_badging),
      timestamp: timestamp,
      additional_data: metadata
    )
  end

end
require 'test_helper'
class UserOnboardingRecorderTest < ActiveSupport::TestCase
  setup do
    author_first_name = Faker::Name.first_name
    actor_first_name = Faker::Name.first_name

    author_last_name = Faker::Name.last_name
    actor_last_name = Faker::Name.last_name

    @org = create(:organization, name: 'LXP', host_name: 'spark' )
    @user = create(:user, first_name: author_first_name,
      last_name: author_last_name, organization: @org,
      sign_in_count: 2, last_sign_in_at: 2.days.ago,
      last_sign_in_ip: Faker::Internet.ip_v4_address,
      current_sign_in_ip: Faker::Internet.ip_v4_address
    )
  end

  test 'valid registered events' do
    registered_events = 'user_onboarding_started'
    assert Analytics::UserOnboardingRecorder::USER_ONBOARDING_EVENTS.include? registered_events
  end

  test 'invalid events' do
    invalid_event = 'user_onboarding_started_invalid'
    onboarding_user = create(:user, organization: @org, organization_role: 'member')
    user_onboarding = UserOnboarding.create(id: 10000001 , user: onboarding_user)

     metadata = {
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0,
      user_id: @user.id
     }
    timestamp = Time.now.to_i
    exception = assert_raise do 
      Analytics::UserOnboardingRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      actor: Analytics::MetricsRecorder.user_attributes(@user),
      event: invalid_event,
      timestamp: timestamp,
      user_onboarding: Analytics::MetricsRecorder.user_onboarding_attributes(user_onboarding),
      additional_data: metadata
    )
    end
    assert_equal exception.class, RuntimeError
    assert_equal "Invalid event: #{invalid_event}. Valid Events: #{Analytics::UserOnboardingRecorder::USER_ONBOARDING_EVENTS.join(",")}", exception.message
  end

  test 'includes user onboarding attributes' do
    onboarding_user = create(:user, organization: @org, organization_role: 'member')
    user_onboarding = UserOnboarding.create(id: 10000001 , user: onboarding_user)
    event = "user_onboarding_created"

     InfluxdbRecorder.unstub :record
    timestamp = Time.now.to_i


     metadata = {
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0,
      user_id: @user.id
     }
     InfluxdbRecorder.expects(:record).with( 'users',
      {
        tags: {
          org_id: @org.id.to_s,
          event: event,
          _user_id: @user.id.to_s,
          is_system_generated: '1'
        },
        values: {
          sign_in_ip: @user.last_sign_in_ip,
          sign_in_at: @user.last_sign_in_at.to_i.to_s,
          user_id: @user.id.to_s,
          user_role: @user.organization_role,
          org_hostname: @org.host_name,
          onboarding_status: '1',
          full_name: @user.full_name,
          user_handle: @user.handle,
          platform_version_number: 'platform_version_number',
          user_agent: 'user_agent',
          is_admin_request: '0',
          user_onboarding_id: user_onboarding.id.to_s,
          user_onboarding_current_step: user_onboarding.current_step.to_s,
          user_onboarding_status: user_onboarding.status
        },
        timestamp: timestamp
      },
      {
        write_client:    nil,
        duplicate_check: true
      }
    )
    Analytics::UserOnboardingRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      actor: Analytics::MetricsRecorder.user_attributes(@user),
      event: event,
      user_onboarding: Analytics::MetricsRecorder.user_onboarding_attributes(user_onboarding),
      timestamp: timestamp,
      additional_data: metadata
    )
    end
end
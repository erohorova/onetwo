require 'test_helper'

class KnowledgeGraphRecorderTest < ActiveSupport::TestCase

  setup do
    time = Time.now.utc
    Time.stubs(:now).returns time

    @start_time = (time.beginning_of_day - 1.day).to_i
    @end_time   = (time.end_of_day - 1.day).to_i

    @org   = create(:organization)
    @user1 = create(:user, organization: @org)
    @user2 = create(:user, organization: @org)
    @user3 = create(:user, organization: @org)

    @card1 = create(:card, author: @user1, message: "foo", taxonomy_topics: ["a", "b"])
    @card2 = create(:card, author: @user1, message: "foo", taxonomy_topics: ["b"])
    @card3 = create(:card, author: @user1, message: "foo", taxonomy_topics: ["c"])
  end

  test 'run' do
    query = (
      "select COUNT(card_id) from \"cards\" where (org_id = %{org_id_0}) AND " +
      "(event = %{event_0} OR event = %{event_1} OR event = %{event_2} " +
      "OR event = %{event_3} OR event = %{event_4} OR event = %{event_5} " +
      "OR event = %{event_6}) AND " +
      "time >= %{start_date}s AND time <= %{end_date}s " +
      "GROUP BY org_id,_user_id,_card_id ;"
    )
    params = {
      org_id_0:   @org.id.to_s,
      event_0:    'card_viewed',
      event_1:    'card_liked',
      event_2:    'card_comment_added',
      event_3:    'card_shared',
      event_4:    'card_bookmarked',
      event_5:    'card_marked_as_complete',
      event_6:    'card_created',
      group_by:   'org_id,_user_id,_card_id',
      start_date: @start_time,
      end_date:   @end_time
    }

    # user1 has activity on card1 and card2 (topic 'a' = 2 actions, topic 'b' = 7 actions)
    # user2 has activity on card1           (topic 'a' = 9 actions, topic 'b' = 9 actions)
    # user3 and card3 have no activity
    stub_response = [
      {
        "name" => "cards",
        "tags" => {
          "org_id" => @org.id.to_s,
          "_user_id" => @user1.id.to_s,
          "_card_id" => @card1.id.to_s
        },
        "values" => [{
          "count" => 2
        }]
      },
      {
        "name" => "cards",
        "tags" => {
          "org_id" => @org.id.to_s,
          "_user_id" => @user1.id.to_s,
          "_card_id" => @card2.id.to_s
        },
        "values" => [{
          "count" => 5
        }]
      },
      {
        "name" => "cards",
        "tags" => {
          "org_id" => @org.id.to_s,
          "_user_id" => @user2.id.to_s,
          "_card_id" => @card1.id.to_s
        },
        "values" => [{
          "count" => 9
        }]
      },
    ]

    INFLUXDB_CLIENT.expects(:query).with(query, params: params).returns stub_response

    Analytics::KnowledgeGraphRecorder.run

    users_topics_activities = UsersTopicsActivity
      .all
      .map { |record| record.attributes.except("id") }
      .index_by { |record| record["user_id"] }

    assert_equal(
      {
        @user1.id => {
          "organization_id"        => @org.id,
          "user_id"                => @user1.id,
          "topic_counts"           => {"a" => 2, "b" => 7},
          "start_time"             => @start_time,
          "end_time"               => @end_time,
          "total_activities_count" => 9
        },
        @user2.id => {
          "organization_id"        => @org.id,
          "user_id"                => @user2.id,
          "topic_counts"           => {"a" => 9, "b" => 9},
          "start_time"             => @start_time,
          "end_time"               => @end_time,
          "total_activities_count" => 18
        }
      },
      users_topics_activities
    )

  end

end
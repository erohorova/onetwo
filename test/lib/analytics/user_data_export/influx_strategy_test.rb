require 'test_helper'

class Analytics::UserDataExport::InfluxStrategyTest < ActiveSupport::TestCase


  setup do
    @class = Analytics::UserDataExport::InfluxStrategy

    # Some sample, fake influx data
    # records aren't actually in this exact format,
    # but it shouldn't matter for the test.
    @stub_results = @class::InfluxUserPointers
      .each_with_object({}) do |(measurement, _), memo|
        memo[measurement] = 3.times.map do |i|
          { "type" => measurement, "id" => i }
        end
    end

    @org = create :organization
    @user = create :user, organization: @org

    @queries = @class.send(:queries, @user)
    @class.stubs(:queries).with(@user).returns @queries
    @queries.each do |measurement, query|
      query
        .expects(:resolve_with_loop)
        .with(interval: 30.days.to_i)
        .returns(@stub_results[measurement])
    end
  end

  test '.get_data fetches data' do
    assert_equal @stub_results, @class.get_data(@user)
  end

end
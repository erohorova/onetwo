require 'test_helper'

class Analytics::UserDataExport::SqlStrategyTest < ActiveSupport::TestCase

  setup do
  # =========================================
  # DISABLING SQL EXPORTER FOR NOW
  # UNSKIP TESTS IF WE RE-ENABLE
  # =========================================
    skip
    @class = Analytics::UserDataExport::SqlStrategy
    @with_timestamps = Analytics::MetricsRecorder.method(:with_timestamps)

    @org = create :organization
    @user = create :user, organization: @org
    @cards = create_list :card, 2, author: @user
  end

  test '.get_data fetches data' do
    expected = {
      users: [@user.attributes],
      cards: @cards.map(&:attributes)
    }.transform_values { |val| val.map(&@with_timestamps) }
    actual = @class.get_data(@user)
    assert_equal expected, actual
  end

  test '.get_data sanitizes data' do
    # Set some temporary value in the filtered attriubutes constant
    # Now we exclude :created_at from the users records
    orig_filter = @class::ExcludedDatabaseAttributes[:User]
    @class::ExcludedDatabaseAttributes[:User] = ["created_at", "id"]
    expected = {
      users: [@user.attributes.except("created_at", "id")],
      cards: @cards.map(&:attributes)
    }.transform_values { |val| val.map(&@with_timestamps) }
    actual = @class.get_data(@user)
    # Use a begin/raise/ensure block to revert the filter changes,
    # even if the expectation failed
    begin
      assert_equal expected, actual
    rescue => e
      raise
    ensure
      @class::ExcludedDatabaseAttributes[:User] = orig_filter
    end
  end

end
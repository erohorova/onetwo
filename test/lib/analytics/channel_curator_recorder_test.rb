require 'test_helper'
class ChannelCuratorRecorderTest < ActiveSupport::TestCase

  setup do
    @org = create :organization,
      name: 'LXP',
      host_name: 'spark'
    @author = create :user,
      first_name: Faker::Name.first_name,
      last_name: Faker::Name.last_name,
      organization: @org
    @curator = create :user,
      first_name: Faker::Name.first_name,
      last_name: Faker::Name.last_name,
      organization: @org
    @channel = create :channel,
      organization: @org,
      curate_only: false,
      is_promoted: false,
      is_private: true,
      ecl_enabled: false
    @user_agent = "chrome"
    @platform = "some platform"
    @platform_version_number = "0.1.1"
  end

  test 'valid registered events' do
    registered_events = %w{channel_curator_added channel_curator_removed}
    assert_equal registered_events,
      Analytics::ChannelCuratorRecorder::CHANNEL_CURATOR_EVENTS
  end

  test 'should record add_curator event' do
    timestamp = 0
    event = 'channel_curator_added'
    field_set = {
      channel_id:  @channel.id.to_s,
      user_id:  @author.id.to_s,
      user_full_name: @author.full_name,
      channel_name:  @channel.label,
      user_agent:  @user_agent,
      curator_id: @curator.id.to_s,
      platform_version_number: @platform_version_number,
      is_admin_request: '0',
      is_channel_featured: '0',
      is_channel_curated: '0',
      is_channel_public: '0',
      is_ecl_enabled: '0',
      curator_full_name: @curator.full_name,
      user_handle: @author.handle,
      platform: @platform,
    }

    tag_set = {
      _channel_id: @channel.id.to_s,
      is_system_generated: '0',
      _user_id: @author.id.to_s,
      event: event,
      org_id: @org.id.to_s,
    }

    InfluxdbRecorder.expects(:record).with(
      'channels', {
        values: field_set,
        tags: tag_set,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true

    Analytics::ChannelCuratorRecorder.record(
      channel: Analytics::MetricsRecorder.channel_attributes(@channel),
      curator: Analytics::MetricsRecorder.user_attributes(@curator),
      actor: Analytics::MetricsRecorder.user_attributes(@author),
      event: event,
      timestamp: timestamp,
      additional_data: {
        platform: @platform,
        user_agent: @user_agent,
        platform_version_number: @platform_version_number,
        is_admin_request: 0
      }
    )
  end

  test 'should record remove_curator event' do
    InfluxdbRecorder.unstub(:record)
    timestamp = 0
    event = 'channel_curator_removed'
    field_set = {
      channel_id:  @channel.id.to_s,
      user_id:  @author.id.to_s,
      user_full_name: @author.full_name,
      user_handle: @author.handle,
      channel_name:  @channel.label,
      user_agent:  @user_agent,
      curator_id: @curator.id.to_s,
      platform_version_number: @platform_version_number,
      is_admin_request: '0',
      is_channel_featured: '0',
      is_channel_curated: '0',
      is_channel_public: '0',
      is_ecl_enabled: '0',
      curator_full_name: @curator.full_name,
      platform: @platform,
    }

    tag_set = {
      _channel_id: @channel.id.to_s,
      is_system_generated: '0',
      _user_id: @author.id.to_s,
      event: event,
      org_id: @org.id.to_s,
    }

    InfluxdbRecorder.expects(:record).with(
      'channels', {
        values: field_set,
        tags: tag_set,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true

    Analytics::ChannelCuratorRecorder.record(
      channel: Analytics::MetricsRecorder.channel_attributes(@channel),
      curator: Analytics::MetricsRecorder.user_attributes(@curator),
      actor: Analytics::MetricsRecorder.user_attributes(@author),
      event: event,
      timestamp: timestamp,
      additional_data: {
        platform: @platform,
        user_agent: @user_agent,
        platform_version_number: @platform_version_number,
        is_admin_request: 0
      }
    )
  end

end

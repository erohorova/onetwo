# frozen_string_literal: true

require 'test_helper'
class UserCustomFieldRecorderTest < ActiveSupport::TestCase

  test 'records events to influx' do
      InfluxdbRecorder.unstub :record
      timestamp = Time.now.to_i
      org = create :organization
      member = create :user, organization: org
      actor = create :user, organization: org
      metadata = {
        user_id: actor.id
      }
      custom_field = CustomField.create(
        organization: org,
        display_name: "foo",
        abbreviation: "f"
      )
      user_custom_field = UserCustomField.create(
        custom_field: custom_field,
        user: member,
        value: "old_val"
      )

    field_set = {}
    field_set[:sign_in_at] = "0"
    field_set[:user_id] = actor.id.to_s
    field_set[:user_role] = actor.organization_role
    field_set[:user_handle] = actor.handle
    field_set[:org_hostname] = actor.organization.host_name
    field_set[:onboarding_status] = '1'
    field_set[:full_name] = actor.full_name
    field_set[:member_user_id] = member.id.to_s
    field_set[:custom_field_display_name] = "foo"
    field_set[:custom_field_id] = custom_field.id.to_s
    field_set[:value] = "old_val"
    field_set[:changed_column] = "value"
    field_set[:old_val] = "old_val"
    field_set[:new_val] = "new_val"

    tag_set = { }
    tag_set[:is_system_generated] = '1'
    tag_set[:_user_id] = actor.id.to_s
    tag_set[:org_id] = actor.organization_id.to_s
    tag_set[:event] = "user_custom_field_edited"

    timestamp = Time.now.to_i

    InfluxdbRecorder.unstub :record
    InfluxdbRecorder.expects(:record).with( 'users',
      {
        timestamp: timestamp,
        tags: tag_set,
        values: field_set
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    Analytics::UserCustomFieldRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      member: Analytics::MetricsRecorder.user_attributes(member),
      event: "user_custom_field_edited",
      timestamp: timestamp,
      additional_data: metadata,
      custom_field: Analytics::MetricsRecorder.custom_field_attributes(custom_field),
      user_custom_field: Analytics::MetricsRecorder.user_custom_field_attributes(user_custom_field),
      changed_column: "value",
      old_val: "old_val",
      new_val: "new_val"
    )
  end

end

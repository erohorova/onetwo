require 'test_helper'
class Analytics::GroupMetricsRecorderTest < ActiveSupport::TestCase

  recorder = Analytics::GroupMetricsRecorder

  test 'should have right measurement set' do
    assert 'groups', Analytics::GroupMetricsRecorder::MEASUREMENT
  end

  test 'should record get called with right args and push to influx' do
    org         = create(:organization)
    user        = create(:user, organization: org)
    group       = create :team, organization: org, name: "foo"
    group_user  = TeamsUser.create user: user, team: group, as_type: "admin"
    timestamp   = Time.now.to_i
    field_set   = group_field_set(group, user)
    event       = "group_created"
    tag_set     = group_tag_set(group, event, user)
    
    recorder.expects(:influxdb_record).with(
      "groups",
      {
        values:   field_set.compact,
        tags:     tag_set.compact,
        timestamp: timestamp
      }
    )
    recorder.record(
      org:        Analytics::MetricsRecorder.org_attributes(group.organization),
      group:      Analytics::MetricsRecorder.team_attributes(group),
      actor:      Analytics::MetricsRecorder.user_attributes(user),
      timestamp:  timestamp,
      event:      event,
      additional_data: {
        platform:                 'web',
        user_agent:               'chrome',
        platform_version_number:  "1.0",
        is_admin_request:         0
      }
    )

  end

  test 'should record group event #visit' do
    org         = create(:organization)
    actor       = create(:user, organization: org)
    group       = create :team, organization: org, name: "foo"
    group_user  = TeamsUser.create user: actor, team: group, as_type: "admin"
    group_visit = Analytics::GroupMetricsRecorder::EVENT_GROUP_VISITED
    timestamp   = Time.now.to_i
    field_set   = group_field_set(group, actor)
    tag_set     = group_tag_set(group, group_visit, actor)
    InfluxdbRecorder.unstub :record
    InfluxdbRecorder.expects(:record).with 'groups',
      {
        values:     field_set.compact,
        tags:       tag_set.compact,
        timestamp:  timestamp
      },
      {
        write_client:     nil,
        duplicate_check:  true
      }

    Analytics::GroupMetricsRecorder.record(
      org:        Analytics::MetricsRecorder.org_attributes(group.organization),
      group:      Analytics::MetricsRecorder.team_attributes(group),
      event:      'group_visited',
      actor:      Analytics::MetricsRecorder.user_attributes(actor),
      timestamp:  timestamp,
      additional_data: {
        platform:                 'web',
        user_agent:               'chrome',
        is_admin_request:         0,
        platform_version_number:  "1.0"
      }
    )
  end

  def group_field_set(group, actor)
    {
      group_id:                 group.id.to_s,
      group_name:               group.name,
      org_name:                 group.organization.name,
      org_hostname:             group.organization.host_name,
      user_id:                  actor.id.to_s,
      user_agent:               'chrome',
      user_full_name:           actor&.full_name,
      user_handle:              actor&.handle,
      is_admin_request:         '0',
      platform:                 'web',
      platform_version_number:  "1.0"
    }
  end

  def group_tag_set(group, event, actor)
    {
      _group_id:            group.id.to_s,
      _user_id:             actor.id.to_s,
      is_system_generated: '0',
      event:                event,
      org_id:               group.organization_id.to_s,
    }
  end
end
# frozen_string_literal: true

require 'test_helper'
class Analytics::CardAssignmentMetricsRecorderTest < ActiveSupport::TestCase

  test "calls recorder as expected" do
    InfluxdbRecorder.unstub :record
    INFLUXDB_CLIENT.stubs :write_point

    org = create(:organization)
    actor = create(:user, organization: org)
    card = create :card, organization: org, author: actor, title: "foo"
    assignee = create :user, organization: org
    timestamp = Time.now.to_i
    metadata = {
      platform: "foo",
      user_agent: "bar",
      is_admin_request: 0
    }
    field_set = card_field_set(org, card, actor)
    tag_set = card_tag_set(card, actor, 'card_assigned')

    InfluxdbRecorder.expects(:record).with('cards',
      {
        values: field_set.merge!(
          {
            user_agent: 'bar',
            platform: 'foo',
            assigned_to_user_id: assignee.id.to_s
          }
        ),
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true

    Analytics::CardAssignmentMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(card.organization),
      card: Analytics::MetricsRecorder.card_attributes(card),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      event: "card_assigned",
      timestamp: timestamp,
      additional_data: metadata,
      assignee: Analytics::MetricsRecorder.user_attributes(assignee)
    )

  end

end
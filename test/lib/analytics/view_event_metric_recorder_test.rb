# frozen_string_literal: true

require 'test_helper'
class ViewEventMetricRecorderTest < ActiveSupport::TestCase

  test 'should record card view event' do
    org = create(:organization)
    viewer = create(:user, organization: org)
    card = create(:card, author: create(:user, organization: org))

    Card.any_instance.expects(:push_view_event)
    .with(viewer).once

    Analytics::ViewEventMetricRecorder.record_view_event(
      entity: card,
      viewer: viewer
    )
  end

  test 'should record card click_through event' do
    org = create(:organization)
    viewer = create(:user, organization: org)
    card = create(:card, author: create(:user, organization: org))

    Card.any_instance.expects(:push_click_through_event)
    .with(viewer).once

    Analytics::ViewEventMetricRecorder.record_click_through_event(
      entity: card,
      viewer: viewer
    )
  end


  test 'should record channel view event' do
    org = create(:organization)
    viewer = create(:user, organization: org)
    channel = create(:channel, organization: org)

    Channel.any_instance.expects(:push_view_event)
    .with(viewer).once

    Analytics::ViewEventMetricRecorder.record_view_event(entity: channel, viewer: viewer)
  end


end

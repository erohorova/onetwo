# frozen_string_literal: true

require 'test_helper'
class Analytics::CardViewMetricsRecorderTest < ActiveSupport::TestCase

  setup do
    @class = Analytics::CardViewMetricsRecorder
    @org = create :organization
    @user = create :user, organization: @org
    @card = create :card, author: @user

    @time = Time.now + 5.minutes
    Time.stubs(:now).returns @time
  end

  test 'valid registered events' do
    registered_events = {
      "card_viewed" => true,
      "card_source_visited" => true,
    }
    assert_equal(registered_events, @class::VALID_EVENTS)
  end

  test 'records to influx and sets initial cache value when recording' do
    begin
      orig_cache = Rails.cache
      Rails.cache = ActiveSupport::Cache::MemoryStore.new
      INFLUXDB_CLIENT.unstub :query
      query = "select count(user_id) from \"cards\" where _card_id = %{card_id} " +
              "AND event = %{event} AND time >= %{start_date}s AND time <= %{end_date}s ;"
      params = {
        card_id: @card.id.to_s,
        event: 'card_viewed',
        start_date: @card.created_at.to_i,
        end_date: @time.to_i
      }
      response = [
        "values" => [{"count" => 5}]
      ]
      INFLUXDB_CLIENT.expects(:query).with(query, params: params).returns response
      InfluxdbRecorder.unstub(:record)
      RequestStore.stubs(:read).with(:request_metadata)
      Analytics::MetricsRecorder.expects(:push_auxiliary_metrics)

      field_set = card_field_set(
        @org, @card, @user
      )

      tag_set = card_tag_set(
        @card, @user, 'card_viewed'
      )

      InfluxdbRecorder.expects(:record).with('cards',
        {
          values: field_set.symbolize_keys.merge(
            {
              platform: 'ios'
            }
          ).compact,
          tags: tag_set.compact,
          timestamp: @time.to_i
        },
        {
          write_client: nil,
          duplicate_check: true
        }
      ).returns OpenStruct.new(code: "204")

      Analytics::CardViewMetricsRecorder.record(
        org: Analytics::MetricsRecorder.org_attributes(@org),
        card: Analytics::MetricsRecorder.card_attributes(@card),
        event: 'card_viewed',
        actor: Analytics::MetricsRecorder.user_attributes(@user),
        timestamp: @time.to_i,
        additional_data: {
          platform: 'ios',
          user_agent: 'Edcast iPhone',
          is_admin_request: 0
        }
      )
      cache_key = @class.card_view_count_cache_key @card
      # It is 6 not 5 because it increments as well.
      assert_equal 6, Rails.cache.fetch(cache_key)
    ensure
      Rails.cache = orig_cache
    end
  end

  test 'doesnt increment cache if recording failed' do
    InfluxdbRecorder.unstub(:record)
    InfluxdbRecorder.stubs(:is_duplicate?).returns true
    @class.expects(:increment_card_view_count).never

    Analytics::CardViewMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      card: Analytics::MetricsRecorder.card_attributes(@card),
      event: 'card_viewed',
      actor: Analytics::MetricsRecorder.user_attributes(@user),
      timestamp: @time.to_i,
      additional_data: {
        platform: 'ios',
        user_agent: 'Edcast iPhone',
        is_admin_request: 0
      }
    )
  end

  test 'increments the count if the cache key is found' do
    begin
      orig_cache = Rails.cache
      Rails.cache = ActiveSupport::Cache::MemoryStore.new

      # write an original count to the cache
      cache_key = @class.card_view_count_cache_key @card
      Rails.cache.write cache_key, 3, raw: true

      # Influx is never queried in this case
      INFLUXDB_CLIENT.expects(:query).never

      # Record a new event
      Analytics::CardViewMetricsRecorder.record(
        org: Analytics::MetricsRecorder.org_attributes(@org),
        card: Analytics::MetricsRecorder.card_attributes(@card),
        event: 'card_viewed',
        actor: Analytics::MetricsRecorder.user_attributes(@user),
        timestamp: @time.to_i
      )

      # The cache has incremented
      assert_equal 4, Rails.cache.fetch(cache_key)
    ensure
      Rails.cache = orig_cache
    end
  end

end
# frozen_string_literal: true
require 'test_helper'

class Analytics::UserDataDeletionTest < ActiveSupport::TestCase

  setup do
    @class = Analytics::UserDataDeletion

    @org = create(:organization)
    @user = create :user, organization: @org

    @influx_strategy = Analytics::UserDataDeletion::InfluxStrategy
    @strategies = {
      influxdb: @influx_strategy
    }
    @queries = @influx_strategy.queries(@user)
    @influx_strategy.stubs(:queries).returns @queries
  end

  test 'Strategies' do
    assert_equal @strategies, @class::Strategies
  end

  test '.run with default strategies' do
    @strategies.each do |name, klass|
      klass.expects(:run!).with(@user)
    end
    @class.run! @user
  end

  test '.run with custom strategies' do
    # begin/ensure to prevent changes to constant from leaking
    begin
      foo_strategy = Class.new
      @class::Strategies[:foo] = foo_strategy
      foo_strategy.expects(:run!).with(@user)
      @strategies[:influxdb].expects(:run!).with(@user).never
      @class.run! @user, strategies: [:foo]
    ensure
      @class::Strategies.delete :foo
    end
  end

  test '.run doesnt raise an error' do
    InfluxQuery.stubs(:fetch).returns []
    expected = {
      influxdb: {
        cards: [],
        channels: [],
        groups: [],
        users: [],
        searches: [],
        organizations: []
      }
    }
    assert_equal expected, @class.run!(@user)
  end

end
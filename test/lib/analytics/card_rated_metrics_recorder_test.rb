require 'test_helper'
class Analytics::CardRatedMetricsRecorderTest < ActiveSupport::TestCase

  test ".record calls InfluxdbRecorder with expected keys" do
    InfluxdbRecorder.unstub(:record)
    INFLUXDB_CLIENT.stubs(:write_point)
    org = create(:organization)
    actor = create(:user, organization: org)
    author = create(:user, organization: org)
    card = create(:card, organization: org, author: author)
    card_rating = create(:cards_rating, card: card, rating: 1, level: "beginner")
    event = "card_relevance_rated"
    timestamp = Time.now.to_i

    metadata = {
      platform: "platform",
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      user_id: 0,
      is_admin_request: 0
    }

    field_set = card_field_set(
      org, card, actor
    )

    tag_set = card_tag_set(
      card, actor, event
    )

    InfluxdbRecorder.expects(:record).with('cards',
      {
        values: field_set.symbolize_keys.merge(
          {
            platform: "platform",
            user_agent: "user_agent",
            platform_version_number: "platform_version_number",
            card_rating: card_rating.rating.to_s,
            card_level: card_rating.level,
          }
        ).compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true

    Analytics::CardRatedMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(card.organization),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      cards_rating: Analytics::MetricsRecorder.cards_rating_attributes(card_rating),
      event: event,
      card: Analytics::MetricsRecorder.card_attributes(card),
      timestamp: timestamp,
      additional_data: metadata
    )
  end

end

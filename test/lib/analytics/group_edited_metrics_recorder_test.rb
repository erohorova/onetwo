require 'test_helper'
class Analytics::GroupEditedMetricsRecorderTest < ActiveSupport::TestCase

  recorder = Analytics::GroupEditedMetricsRecorder

  test 'inherits from GroupMetricsRecorder' do
    assert recorder < Analytics::GroupMetricsRecorder
  end

  test "calls InfluxdbRecorder.record" do
    org = create :organization
    actor = create :user, organization: org
    Group.any_instance.stubs :send_group_created_metric
    team = create :team, organization: org, name: "foo"
    metadata = {
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0,
      user_id: actor.id
    }
    timestamp = Time.now.to_i
    InfluxdbRecorder.unstub :record
    InfluxdbRecorder.expects(:record).with( 'groups',
      {
        timestamp: timestamp,
        values: {
          group_name: 'foo',
          org_name: org.name,
          user_id: actor.id.to_s,
          user_full_name: actor.full_name,
          user_handle: actor.handle,
          group_id: team.id.to_s,
          platform_version_number: 'platform_version_number',
          user_agent: 'user_agent',
          old_val: "old_val",
          new_val: "1",
          org_hostname: org.host_name,
          is_admin_request: '0',
          changed_column: "changed_column",
        },
        tags: {
          is_system_generated: '1',
          _user_id: actor.id.to_s,
          _group_id: team.id.to_s,
          org_id: org.id.to_s,
          event: 'group_edited',
        }
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    Analytics::GroupEditedMetricsRecorder.record(
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      org: Analytics::MetricsRecorder.org_attributes(org),
      timestamp: timestamp,
      event: "group_edited",
      additional_data: metadata,
      group: Analytics::MetricsRecorder.team_attributes(team),
      changed_column: "changed_column",
      old_val: "old_val",
      new_val: 1 # will be changed to string
    )
  end

  test 'should have group edited event registered' do
    assert_includes recorder::GROUP_EDITED_EVENTS, 'group_edited'
    assert_equal(
      recorder,
      Analytics::MetricsRecorder.get_recorder_for_event('group_edited')
    )
  end

end

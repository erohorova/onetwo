require 'test_helper'
class Analytics::GroupChannelsMetricsRecorderTest < ActiveSupport::TestCase

  test 'should record group event' do
    stub_time
    org = create(:organization, name: 'LXP', host_name: 'spark' )
    actor = create :user, organization: org
    team = create(:team, organization: org)
    channel = create(:channel, organization: org)

    InfluxdbRecorder.unstub(:record)
    INFLUXDB_CLIENT.stubs(:write_point)

    add_to_group = Analytics::GroupChannelsMetricsRecorder::EVENT_GROUP_CHANNEL_FOLLOWED

    field_set = {group_id: team.id.to_s, group_name: team.name}
    field_set[:user_id] = actor.id.to_s

    field_set[:org_name] = team.organization.name
    field_set[:user_agent] = 'Edcast iPhone'
    field_set[:is_admin_request] = '0'
    field_set[:org_hostname] = team.organization.host_name
    field_set[:user_full_name] = actor.try(:full_name)
    field_set[:user_handle] = actor.handle
    field_set[:platform] = 'web'
    field_set[:channel_id] = channel.id.to_s
    field_set[:channel_name] = channel.label

    tag_set = {_user_id: actor.id.to_s, _group_id: team.id.to_s}
    tag_set[:is_system_generated] = '0'
    tag_set[:org_id] = team.organization_id.to_s
    tag_set[:event] = add_to_group

    timestamp = Time.now.to_i

    InfluxdbRecorder.expects(:record).with('groups',
      {
        values: field_set.compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true

    Analytics::GroupChannelsMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      group: Analytics::MetricsRecorder.team_attributes(team),
      channel: Analytics::MetricsRecorder.channel_attributes(channel),
      event: add_to_group,
      timestamp: timestamp,
      additional_data: {
        platform: 'web',
        user_agent: 'Edcast iPhone',
        is_admin_request: 0
      }
    )
  end

  test 'valid registered events' do
    registered_events = {"group_channel_followed" => true, "group_channel_unfollowed" => true}
    assert_same_elements registered_events, Analytics::GroupChannelsMetricsRecorder::VALID_EVENTS
  end
end

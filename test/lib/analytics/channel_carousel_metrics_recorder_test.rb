require 'test_helper'

class Analytics::ChannelCarouselMetricsRecorderTest < ActionController::TestCase

  test 'calls ChannelCarouselMetricsRecorder with expected args' do
    actor = build :user, organization_id: 123
    channel = build :channel
    event = "channel_carousel_created"
    additional_data = {}
    timestamp = Time.now.to_i
    structure = build :structure
    Analytics::ChannelMetricsRecorder.expects(:record).with(
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      event: event,
      channel: {organization_id: actor.organization_id},
      additional_data: additional_data,
      timestamp: timestamp
    )
    Analytics::ChannelCarouselMetricsRecorder.record(
      event: event,
      additional_data: additional_data,
      timestamp: timestamp,
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      structure: Analytics::MetricsRecorder.structure_attributes(structure)
    )
  end

  test 'calls InfluxdbRecorder.record with expected args (when change keys are ommitted)' do
    org = create :organization
    actor = create :user, organization: org
    creator = create :user, organization: org
    channel = create :channel, organization: org
    structure = create :structure, {
      current_context: 'discover',
      current_slug: 'channel',
      parent: channel,
      creator: creator,
      enabled: false
    }
    event = "channel_carousel_created"
    additional_data = {
      is_admin_request: 0,
      user_agent: "user_agent",
    }
    timestamp = Time.now.to_i
    InfluxdbRecorder.unstub :record
    InfluxdbRecorder.expects(:record).with('channels',
      {
        values: {
          is_admin_request: '0',
          is_channel_featured: '0',
          is_channel_curated: '0',
          is_channel_public: '1',
          channel_carousel_creator_id: creator.id.to_s,
          is_ecl_enabled: '0',
          is_channel_carousel_enabled: '0',
          channel_carousel_slug: structure.slug,
          channel_carousel_id: structure.id.to_s,
          user_id: actor.id.to_s,
          user_full_name: actor.full_name,
          user_handle: actor.handle,
          user_agent: "user_agent",
        },
        tags: {
          org_id: org.id.to_s,
          is_system_generated: '1',
          _user_id: actor.id.to_s,
          event: 'channel_carousel_created',
        },
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    Analytics::ChannelCarouselMetricsRecorder.record(
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      event: event,
      additional_data: additional_data,
      timestamp: timestamp,
      structure: Analytics::MetricsRecorder.structure_attributes(structure)
    )
  end

  test 'record channel carousel enabled when edited #enabled' do
    org = create :organization
    actor = create :user, organization: org
    creator = create :user, organization: org
    channel = create :channel, organization: org
    structure = create :structure, {
      current_context: 'discover',
      current_slug: 'channel',
      parent: channel,
      creator: creator,
      enabled: true
    }
    event = "channel_carousel_enabled"
    additional_data = {
      is_admin_request: 0,
      user_agent: "user_agent",
    }
    timestamp = Time.now.to_i
    InfluxdbRecorder.unstub :record
    InfluxdbRecorder.expects(:record).with('channels',
      {
        values: {
          is_admin_request: '0',
          is_channel_featured: '0',
          changed_column: "enabled",
          is_channel_curated: '0',
          is_channel_public: '1',
          channel_carousel_creator_id: creator.id.to_s,
          is_ecl_enabled: '0',
          is_channel_carousel_enabled: '1',
          channel_carousel_slug: structure.slug,
          channel_carousel_id: structure.id.to_s,
          user_id: actor.id.to_s,
          user_full_name: actor.full_name,
          user_handle: actor.handle,
          user_agent: "user_agent",
          old_val: '0',
          new_val: '1',
        },
        tags: {
          org_id: org.id.to_s,
          is_system_generated: '1',
          _user_id: actor.id.to_s,
          event: event,
        },
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    Analytics::ChannelCarouselMetricsRecorder.record(
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      event: event,
      changed_column: "enabled",
      old_val: 0,
      new_val: 1,
      additional_data: additional_data,
      timestamp: timestamp,
      structure: Analytics::MetricsRecorder.structure_attributes(structure)
    )
  end

  test 'record channel carousel enabled when edited #disabled' do
    org = create :organization
    actor = create :user, organization: org
    creator = create :user, organization: org
    channel = create :channel, organization: org
    structure = create :structure, {
      current_context: 'discover',
      current_slug: 'channel',
      parent: channel,
      creator: creator,
      enabled: false
    }
    event = "channel_carousel_disabled"
    additional_data = {
      is_admin_request: 0,
      user_agent: "user_agent",
    }
    timestamp = Time.now.to_i
    InfluxdbRecorder.unstub :record
    InfluxdbRecorder.expects(:record).with('channels',
      {
        values: {
          is_admin_request: '0',
          is_channel_featured: '0',
          changed_column: "enabled",
          is_channel_curated: '0',
          is_channel_public: '1',
          channel_carousel_creator_id: creator.id.to_s,
          is_ecl_enabled: '0',
          is_channel_carousel_enabled: '0',
          channel_carousel_slug: structure.slug,
          channel_carousel_id: structure.id.to_s,
          user_id: actor.id.to_s,
          user_full_name: actor.full_name,
          user_handle: actor.handle,
          user_agent: "user_agent",
          old_val: '1',
          new_val: '0',
        },
        tags: {
          org_id: org.id.to_s,
          is_system_generated: '1',
          _user_id: actor.id.to_s,
          event: event,
        },
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    Analytics::ChannelCarouselMetricsRecorder.record(
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      event: event,
      changed_column: "enabled",
      old_val: 1,
      new_val: 0,
      additional_data: additional_data,
      timestamp: timestamp,
      structure: Analytics::MetricsRecorder.structure_attributes(structure)
    )
  end

  test 'records card_edited event for whitelisted columns' do
    whitelist = %w{foo bar}
    Analytics::ChannelCarouselMetricsRecorder.stubs(:whitelisted_change_attrs)
                                             .returns(whitelist)
    do_record = -> (changed_col) {
      Analytics::ChannelCarouselMetricsRecorder.record(
        structure: {},
        event: 'channel_carousel_edited',
        actor: {},
        timestamp: 0,
        changed_column: changed_col,
        new_val: 0,
        old_val: 1,
        additional_data: {}
      )
    }
    whitelist.each do |changed_col|
      Analytics::ChannelCarouselMetricsRecorder.expects(:influxdb_record)
      do_record.call(changed_col)
    end
  end


  test 'doesnt record card_edited event for non-whitelisted columns' do
    whitelist = %w{foo bar}
    test_columns = %w{asd bsd}
    Analytics::ChannelCarouselMetricsRecorder.stubs(:whitelisted_change_attrs)
                                             .returns(whitelist)
    do_record = -> (changed_col) {
      Analytics::ChannelCarouselMetricsRecorder.record(
        structure: {},
        event: 'channel_carousel_edited',
        actor: {},
        timestamp: 0,
        changed_column: changed_col,
        new_val: 0,
        old_val: 1,
        additional_data: {}
      )
    }
    Analytics::ChannelCarouselMetricsRecorder.expects(:influxdb_record).never
    test_columns.each do |changed_col|
      do_record.call(changed_col)
    end
  end

  test 'whitelisted_change_attrs' do
    assert_equal(
      %w{
        enabled
        context
        display_name
      },
      Analytics::ChannelCarouselMetricsRecorder.whitelisted_change_attrs
    )
  end

  test 'is_ignored_event?' do
    whitelist = %w{foo bar}
    Analytics::ChannelCarouselMetricsRecorder.stubs(:whitelisted_change_attrs)
                                             .returns(whitelist)
    assert Analytics::ChannelCarouselMetricsRecorder.is_ignored_event? "asd"
    refute Analytics::ChannelCarouselMetricsRecorder.is_ignored_event? "foo"
    refute Analytics::ChannelCarouselMetricsRecorder.is_ignored_event? "foo"
    refute Analytics::ChannelCarouselMetricsRecorder.is_ignored_event? nil
  end

end

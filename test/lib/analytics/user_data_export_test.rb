require 'test_helper'

class Analytics::UserDataExportTest < ActiveSupport::TestCase

  setup do
    @class = Analytics::UserDataExport
    @with_timestamps = Analytics::MetricsRecorder.method(:with_timestamps)

    # create some db records for sql strategy
    @org = create :organization
    @user = create :user, organization: @org
    @cards = create_list :card, 2, author: @user
    @expected_sql_results = {
      users: [@user.attributes],
      cards: @cards.map(&:attributes)
    }.transform_values { |val| val.map(&@with_timestamps) }

    # stub influx responses
    @stub_influx_results = @class::InfluxStrategy::InfluxUserPointers
      .each_with_object({}) do |(measurement, _), memo|
        memo[measurement] = 3.times.map do |i|
          { "type" => measurement, "id" => i }
        end
    end
    @influx_queries = @class::InfluxStrategy.send(:queries, @user)
    @class::InfluxStrategy.stubs(:queries).with(@user).returns(@influx_queries)
  end

  test 'Bucket' do
    expected = Settings.user_data_export.s3_bucket
    actual = @class::Bucket.name
    assert_equal expected, actual
  end

  test 'Strategies' do
    expected = {
      # sql: @class::SqlStrategy,
      influxdb: @class::InfluxStrategy
    }
    actual = @class::Strategies
    assert_equal expected, actual
  end

  test '.load_data_from_strategies' do
    @influx_queries.each do |measurement, query|
      query
        .expects(:resolve_with_loop)
        .with(interval: 30.days.to_i)
        .returns(@stub_influx_results[measurement])
    end

    expected = {
      # sql: @expected_sql_results,
      influxdb: @stub_influx_results
    }
    actual = @class.load_data_from_strategies(@user)
    assert_equal expected, actual
  end

  test '.write_to_zipfile' do
    # Generate a zip file
    data = {
      # sql: @expected_sql_results,
      influxdb: @stub_influx_results
    }
    tmpfile = @class.write_to_zipfile(@user, data)
    # Unzip the zip file
    Zip::File.open(tmpfile.path) do |zipfile|
      zipfile.each do |entry|
        unless File.exist?(entry.name)
          FileUtils::mkdir_p(File.dirname(entry.name))
          zipfile.extract(entry, entry.name)
        end
      end
    end
    # ensure that the unzipped directory is always deleted after the test
    begin
      data.each do |source, source_data|
        source_data.each do |table, table_data|
          table_filename = @class.send(:build_table_filename, @user, source, table)
          actual = CSV.parse File.read table_filename
          expected = [
            table_data[0].keys.map(&:to_s),
            *table_data.map(&:values)
          ].map do |row|
            row.map { |val| val.nil? ? val : val.to_s }
          end
          assert_equal expected, actual
        end
      end
    rescue
      raise
    ensure
      tmpfile.delete
      FileUtils.rm_rf "org-#{@org.id}"
    end
  end

  test '.upload_data' do
    tmpfile = Tempfile.new
    tmp_pathname = Pathname.new(tmpfile.path)
    s3_key = @class.send(:build_s3_zipfile_key, @user)
    # Some convoluted stubbing to maintain exact handles on objects
    bucket_objs = @class::Bucket.objects
    @class::Bucket.expects(:objects).returns(bucket_objs)
    bucket_obj = bucket_objs[s3_key]
    bucket_objs.expects(:[]).with(s3_key).returns bucket_obj
    # The part where the file gets uploaded
    bucket_obj.expects(:write).with(tmp_pathname)
    @class.upload_data @user, tmpfile
  end

  test '.dump_table' do
    tmp = Tempfile.new
    Tempfile.stubs(:new).returns tmp
    sample_data = [{foo: "bar"}]
    @class.send(:dump_table, sample_data)
    expected = [sample_data[0].keys.map(&:to_s), *sample_data.map(&:values)]
    actual = CSV.parse File.read tmp.path
    assert_equal expected, actual
  end

  test '.build_table_filename' do
    org_id = @user.organization_id
    source = "foo"
    table = "bar"
    ext = "jpg"
    expected = "org-#{org_id}/user-#{@user.id}/#{source}/#{table}.#{ext}"
    actual = @class.send(:build_table_filename, @user, source, table, ext)
    assert_equal expected, actual
  end

  test '.build_s3_zipfile_key' do
    org_id = @user.organization_id
    expected = "org-#{org_id}/user-#{@user.id}/data_export.zip"
    actual = @class.send(:build_s3_zipfile_key, @user)
    assert_equal expected, actual
  end

  test '.delete_from_s3!' do
    s3_key = "foobar"
    # Some convoluted stubbing to maintain exact handles on objects
    bucket_objs = @class::Bucket.objects
    @class::Bucket.expects(:objects).returns(bucket_objs)
    bucket_obj = bucket_objs[s3_key]
    bucket_objs.expects(:[]).with(s3_key).returns bucket_obj
    # The part where the file gets deleted
    bucket_obj.expects(:delete)
    @class.delete_from_s3!(s3_key)
  end

  test 'generate_link_for' do
    s3_key = "foo"
    # Some convoluted stubbing to maintain exact handles on objects
    bucket_objs = @class::Bucket.objects
    @class::Bucket.expects(:objects).returns(bucket_objs)
    bucket_obj = bucket_objs[s3_key]
    bucket_objs.expects(:[]).with(s3_key).returns bucket_obj
    stub_url = bucket_obj.url_for(:read, expires: 600)
    bucket_obj.expects(:url_for).with(:read, expires: 600).returns stub_url
    assert_equal stub_url.to_s, @class.generate_link_for(s3_key)
  end

  test '.run' do
    stub_data = {
      # sql: [1,2,3],
      influxdb: [4,5,6]
    }
    @class.expects(:load_data_from_strategies).with(@user,
      strategies: @class::Strategies.keys
    ).returns stub_data
    zipfile = Tempfile.new
    zipfile_path = zipfile.path
    @class.expects(:write_to_zipfile).with(@user, stub_data).returns zipfile
    s3_key = "foobar"
    @class.expects(:upload_data).with(@user, zipfile)
    @class.run(@user)
    refute File.exists? zipfile_path
  end


end
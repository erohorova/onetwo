require 'test_helper'
class Analytics::ContentEngagementInfluxQueryTest < ActiveSupport::TestCase

  setup do
    @class = Analytics::ContentEngagementInfluxQuery
  end

  test 'AnalyticsVersion' do
    expected = Settings.influxdb.event_tracking_version
    assert_equal expected, @class::AnalyticsVersion
    refute expected.blank?
  end

  test 'Measurement' do
    expected = "org_level_user_engagement_metrics"
    assert_equal expected, @class::Measurement
  end

  test 'ActiveUserEvents' do
    expected = {}
    expected[:cards] = %w{
      card_added_to_channel
      card_approved_for_channel
      card_bookmarked
      card_comment_created
      card_comment_removed
      card_created
      card_deleted
      card_dismissed
      card_edited
      card_liked
      card_marked_as_complete
      card_marked_as_uncomplete
      card_pinned
      card_poll_response_created
      card_quiz_response_creaated
      card_rejected_for_channel
      card_viewed
      card_shared
      card_unbookmarked
      card_unpinned
      card_promoted
      card_unpromoted
      card_marked_as_complete
    }
    expected[:channels] = %w{
      channel_visited
      channel_visited
      channel_followed
      channel_unfollowed
      channel_promoted
      channel_created
    }
    expected[:users] = %w{
      user_expertise_topic_added
      user_expertise_topic_removed
      user_interest_topic_added
      user_interest_topic_removed
      user_followed
      user_unfollowed
      user_logged_in
      user_profile_visited
      user_expertise_added
    }
    expected.each do |key, events|
      assert_equal events, @class::ActiveUserEvents[key]
    end
    assert_equal expected.keys, @class::ActiveUserEvents.keys
  end

  test 'default_org_ids' do
    Organization.stubs(:pluck).with(:id).returns [1,2,3]
    assert_equal [1,2,3], @class.default_org_ids
  end

  test 'normalize_influx_response' do
    sample_data = [
      { "tags" => { "org_id" => "1" }, "values" => { "foo" => "bar" } },
      { "tags" => { "org_id" => "2" }, "values" => { "bar" => "foo" } },
    ]
    expected = {
      "1" => { "foo" => "bar" },
      "2" => { "bar" => "foo" },
    }
    assert_equal expected, @class.normalize_influx_response(sample_data)
  end

  test 'format_heredoc' do
    assert_equal "hello world", @class.format_query(<<-TXT)
      hello
      world
    TXT
  end

  test 'query_measurement_for_active_users' do
    expected_query = (
      "select DISTINCT(user_id) AS user_id from \"foo\" " +
      "where analytics_version = %{version} " +
      "AND time >= %{start_date}s " +
      "AND time <= %{end_date}s " +
      "AND (event = %{event_0} OR event = %{event_1}) " +
      "AND (org_id = %{org_id_0} OR org_id = %{org_id_1}) " +
      "AND is_system_generated = %{is_system_generated} " +
      "GROUP BY org_id ;"
    )
    expected_params = {
      version: '2.0.0',
      start_date: 1234,
      end_date: 5678,
      event_0: 'event1',
      event_1: 'event2',
      org_id_0: '1',
      org_id_1: '2',
      group_by: 'org_id',
      is_system_generated: '0'
    }
    actual = @class.query_measurement_for_active_users(
      measurement: "foo",
      org_ids: [1,2],
      start_date: 1234,
      end_date: 5678,
      event_names: ["event1", "event2"]
    )
    assert_equal expected_query, actual.finalize_query
    assert_equal expected_params, actual.params
  end

  test "filter_result" do
    org_ids = Set.new(['1','2'])
    input = {
      '1' => {},
      '2' => {},
      '3' => {}
    }
    assert_equal input.slice(*(org_ids.to_a)), @class.filter_result(
      org_ids_set: org_ids,
      result: input
    )
  end

  test 'get_active_user_ids_over_period' do
    org_ids = [1, 2, 5]
    @class::ActiveUserEvents.each.with_index do |(measurement, event_names), idx|
      stub_query = InfluxQuery.new(measurement)
      @class.expects(:query_measurement_for_active_users).with(
        measurement: measurement,
        org_ids: org_ids,
        start_date: 1234,
        end_date: 5678,
        event_names: event_names
      ).returns(stub_query)
      stub_query.expects(:resolve).returns([{
        "tags" => { "org_id" => org_ids[idx].to_s },
        "values" => [{ "user_id" => (org_ids[idx] * 10).to_s }]
      }])
    end
    expected = {
      "1" => Set.new(["10"]),
      "2" => Set.new(["20"]),
      "5" => Set.new(["50"])
    }
    actual = @class.get_active_user_ids_over_period(
      org_ids: org_ids,
      start_date: 1234,
      end_date: 5678
    )
    assert_equal expected, actual
  end

  test 'get_daily_active_user_ids' do
    time = Time.now
    Time.stubs(:now).returns time
    @class.expects(:get_active_user_ids_over_period).with(
      org_ids: [1,2,3],
      start_date: (time.utc - 1.days).to_i
    ).returns "stub result"
    assert_equal "stub result", @class.get_daily_active_user_ids(
      org_ids: [1,2,3]
    )
  end

  test 'get_monthly_active_user_ids' do
    time = Time.now
    Time.stubs(:now).returns time
    @class.expects(:get_active_user_ids_over_period).with(
      org_ids: [1,2,3],
      start_date: (time.utc - 30.days).to_i
    ).returns "stub result"
    assert_equal "stub result", @class.get_monthly_active_user_ids(
      org_ids: [1,2,3]
    )
  end

  test "get_total_logged_in_user_ids" do
    expected_query = "select DISTINCT(user_id) AS user_id from \"users\" where event = %{event} AND analytics_version = %{version} GROUP BY org_id ;"
    expected_params = {
      event: 'user_logged_in',
      version: '2.0.0',
      group_by: 'org_id'
    }
    stub_result = [
      {
        "tags" => { "org_id" => "1" },
        "values" => [
          { "user_id" => "2" },
          { "user_id" => "3" },
        ]
      },
      {
        "tags" => { "org_id" => "4" },
        "values" => [{ "user_id" => "5" }]
      },
      {
        "tags" => { "org_id" => "6" },
        "values" => [{ "user_id" => "7" }]
      }
    ]
    expected_result = {
      "1" => Set.new(["2", "3"]),
      "4" => Set.new(["5"]),
    }
    INFLUXDB_CLIENT.expects(:query).with(
      expected_query,
      params: expected_params
    ).returns stub_result
    assert_equal expected_result, @class.get_total_logged_in_user_ids(
      org_ids: [1,4]
    )
  end

  test 'get_total_user_ids' do
    org1 = create :organization
    org2 = create :organization
    org3 = create :organization
    user1 = create :user, organization: org1, is_suspended: false
    user2 = create :user, organization: org2, is_suspended: false
    user3 = create :user, organization: org2, is_suspended: true
    user4 = create :user, organization: org3, is_suspended: false
    expected_result = {
      org1.id.to_s => Set.new([user1.id.to_s]),
      org2.id.to_s => Set.new([user2.id.to_s])
    }
    assert_equal expected_result, @class.get_total_user_ids(
      org_ids: [org1.id, org2.id]
    )
  end

  test 'get_total_onboarded_user_ids' do
    org1 = create :organization
    org2 = create :organization
    org3 = create :organization
    user1 = create :user, organization: org1, is_suspended: false
    user2 = create :user, organization: org2, is_suspended: false
    user3 = create :user, organization: org2, is_suspended: false
    user4 = create :user, organization: org3, is_suspended: false
    create :user_onboarding, user: user1, status: "completed"
    create :user_onboarding, user: user2, status: "completed"
    expected_result = {
      org1.id.to_s => Set.new([user1.id.to_s]),
      org2.id.to_s => Set.new([user2.id.to_s])
    }
    assert_equal expected_result, @class.get_total_onboarded_user_ids(
      org_ids: [org1.id, org2.id]
    )
  end

  test 'get_monthly_engagement' do
    input = {
      daily_active_users: 10,
      total_onboarded_users: 20
    }
    assert_equal 50.0, @class.get_daily_engagement(input)
  end

  test 'get_daily_engagement' do
    input = {
      monthly_active_users: 10,
      total_onboarded_users: 20
    }
    assert_equal 50.0, @class.get_monthly_engagement(input)
  end

  test 'build_insertion_records' do
    time = Time.now
    Time.stubs(:now).returns time
    input = {
      "1" => {
        total_users: 200,
        total_onboarded_users: 100,
        total_logged_in_users: 3,
        monthly_active_users: 20,
        daily_active_users: 10,
        daily_engagement: 10.0,
        monthly_engagement: 20.0
      }
    }
    expected = [{
      timestamp: time.utc.beginning_of_day.to_i,
      tags: { org_id: "1" },
      values: {
        total_users: 200,
        total_onboarded_users: 100,
        total_logged_in_users: 3,
        monthly_active_users: 20,
        daily_active_users: 10,
        daily_engagement: 10.0,
        monthly_engagement: 20.0
      }
    }]
    assert_equal expected, @class.build_insertion_records(input)
  end

  test 'insert_records' do
    input = [:foo, :bar]
    InfluxdbRecorder.expects(:bulk_record).with(
      @class::Measurement,
      input
    ).returns "stub output"
    assert_equal "stub output", @class.insert_records(input)
  end

  test 'aggregate_data' do
    input = {
      total_users: {
        "1" => ["2", "3"],
        "4" => ["5", "6", "7"],
      },
      total_onboarded_users: {
        "1" => ["5", "6", "7"],
        "4" => ["2", "3"],
      }
    }
    expected = {
      "1" => {
        total_users: 2,
        total_onboarded_users: 3
      },
      "4" => {
        total_users: 3,
        total_onboarded_users: 2
      }
    }
    assert_equal expected, @class.aggregate_data(input)
  end

  test "filter_data" do
    input = {
      total_users: {
        "1" => Set.new(["2", "3"])
      },
      total_logged_in_user_ids: {
        "1" => Set.new(["2", "4"])
      },
      monthly_active_user_ids: {
        "1" => Set.new(["3", "4"])
      },
      daily_active_user_ids: {
        "1" => Set.new(["2", "4"])
      }
    }
    expected = {
      total_users: {
        "1" => Set.new(["2", "3"])
      },
      total_logged_in_user_ids: {
        "1" => ["2"]
      },
      monthly_active_user_ids: {
        "1" => ["3"]
      },
      daily_active_user_ids: {
        "1" => ["2"]
      }
    }
  end

  test 'fetch_data' do
    org_ids = [1,2]
    @class.expects(:get_total_user_ids).with(org_ids: org_ids).returns "total"
    @class.expects(:get_total_onboarded_user_ids).with(org_ids: org_ids).returns "onboarded"
    @class.expects(:get_total_logged_in_user_ids).with(org_ids: org_ids).returns "logged in"
    @class.expects(:get_daily_active_user_ids).with(org_ids: org_ids).returns "daily"
    @class.expects(:get_monthly_active_user_ids).with(org_ids: org_ids).returns "monthly"
    expected = {
      total_user_ids: "total",
      total_onboarded_user_ids: "onboarded",
      total_logged_in_user_ids: "logged in",
      monthly_active_user_ids: "monthly",
      daily_active_user_ids: "daily",
    }
    assert_equal expected, @class.fetch_data(org_ids: org_ids)
  end

  test 'run' do
    org_ids = [1,2]
    @class.expects(:fetch_data).with(org_ids: org_ids).returns "data"
    @class.expects(:filter_data).with("data").returns "filtered"
    @class.expects(:aggregate_data).with("filtered").returns "aggregated"
    @class.expects(:build_insertion_records).with("aggregated").returns("insertions")
    @class.expects(:insert_records).with("insertions")
    @class.run(org_ids: org_ids)
  end

end

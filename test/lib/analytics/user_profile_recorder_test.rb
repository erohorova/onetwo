require 'test_helper'
class UserProfileRecorderTest < ActiveSupport::TestCase

  user_metrics_recorder = Analytics::UserMetricsRecorder
  user_profile_recorder = Analytics::UserProfileRecorder

  setup do
    @org = Organization.first || create(:organization)
    @actor = create :user, organization: @org
    @sample_topic_1, @sample_topic_2 = get_sample_topics
    @fb_ip_provider = create(:identity_provider,
      type: 'FacebookProvider',
      uid: 'SPARK123',
      user: @actor
    )
  end

  test "event name constants" do
    assert_equal(
      user_profile_recorder::AddEvents["expert_topics"],
      "user_expertise_topic_added"
    )
    assert_equal(
      user_profile_recorder::RemoveEvents["expert_topics"],
      "user_expertise_topic_removed"
    )
    assert_equal(
      user_profile_recorder::AddEvents["learning_topics"],
      "user_interest_topic_added"
    )
    assert_equal(
      user_profile_recorder::RemoveEvents["learning_topics"],
      "user_interest_topic_removed"
    )
  end

  test "inherits from UserMetricsRecorder" do
    assert Analytics::UserProfileRecorder < Analytics::UserMetricsRecorder
  end


  test ".record calls .push_added_topic when an expert_topic is added" do
    profile = { id: 0, user_id: 1, org_id: 2 }

    changed_profile_keys = { "expert_topics" => [[@sample_topic_1], [@sample_topic_1, @sample_topic_2]] }
    timestamp = Time.now.to_i
    metadata = { foo: "bar" }
    user_profile_recorder.expects(:push_added_topic).with(
      @org, @actor, profile, "expert_topics", @sample_topic_2.stringify_keys, timestamp, metadata
    )
    user_profile_recorder.record(
      org: @org,
      actor: @actor,
      user_profile: profile,
      changed_profile_keys: changed_profile_keys,
      timestamp: timestamp,
      metadata: metadata
    )
  end

  test ".record stringifies topic/domain attribute values" do
    profile = { id: 0, user_id: 1, org_id: 2 }

    modified_topic_2 = @sample_topic_2.merge(domain_id: 1)

    changed_profile_keys = { "expert_topics" => [[@sample_topic_1], [@sample_topic_1, modified_topic_2]] }
    timestamp = Time.now.to_i
    metadata = { foo: "bar" }

    stringified_topic = modified_topic_2.
      transform_keys(&:to_s).
      transform_values(&:to_s)
        
    user_profile_recorder.expects(:push_added_topic).with(
      @org, @actor, profile, "expert_topics", stringified_topic, timestamp, metadata
    )
    user_profile_recorder.record(
      org: @org,
      actor: @actor,
      user_profile: profile,
      changed_profile_keys: changed_profile_keys,
      timestamp: timestamp,
      metadata: metadata
    )
  end

  test ".record calls .push_added_topic when a learning_topic is added" do
    profile = { id: 0, user_id: 1, org_id: 2 }

    changed_profile_keys = { "learning_topics" => [[@sample_topic_1], [@sample_topic_1, @sample_topic_2]] }
    timestamp = Time.now.to_i
    metadata = { foo: "bar" }
    user_profile_recorder.expects(:push_added_topic).with(
      @org, @actor, profile, "learning_topics", @sample_topic_2.stringify_keys, timestamp, metadata
    )
    user_profile_recorder.record(
      org: @org,
      actor: @actor,
      user_profile: profile,
      changed_profile_keys: changed_profile_keys,
      timestamp: timestamp,
      metadata: metadata
    )
  end

  test ".record calls .push_removed_topic when a expert_topic is removed" do
    profile = { id: 0, user_id: 1, org_id: 2 }

    changed_profile_keys = { "expert_topics" => [[@sample_topic_1, @sample_topic_2], [@sample_topic_1]] }
    timestamp = Time.now.to_i
    metadata = { foo: "bar" }
    user_profile_recorder.expects(:push_removed_topic).with(
      @org, @actor, profile, "expert_topics", @sample_topic_2.stringify_keys, timestamp, metadata
    )
    user_profile_recorder.record(
      org: @org,
      actor: @actor,
      user_profile: profile,
      changed_profile_keys: changed_profile_keys,
      timestamp: timestamp,
      metadata: metadata
    )
  end

  test ".record calls .push_removed_topic when a learning_topic is removed" do
    profile = { id: 0, user_id: 1, org_id: 9000 }

    changed_profile_keys = { "learning_topics" => [[@sample_topic_1, @sample_topic_2], [@sample_topic_1]] }
    timestamp = Time.now.to_i
    metadata = { foo: "bar" }
    user_profile_recorder.expects(:push_removed_topic).with(
      @org, @actor, profile, "learning_topics", @sample_topic_2.stringify_keys, timestamp, metadata
    )
    user_profile_recorder.record(
      org: @org,
      actor: @actor,
      user_profile: profile,
      changed_profile_keys: changed_profile_keys,
      timestamp: timestamp,
      metadata: metadata
    )
  end

  test ".push_added_topic" do
    timestamp = Time.now.to_i
    InfluxdbRecorder.unstub :record
    metadata = {
      platform: "fake platform",
      user_agent: "fake user_agent",
      platform_version_number: "1.2.3",
      is_admin_request: 0
    }
    InfluxdbRecorder.expects(:record).with(
      "users",
      {
        timestamp: timestamp,
        tags: {
          event: "user_expertise_topic_added",
          _user_id: @actor.id.to_s,
          org_id: @org.id.to_s,
          is_system_generated: '0',
        },
        values: {
          platform: "fake platform",
          user_org_uid: @fb_ip_provider.uid,
          is_admin_request: '0',
          org_hostname: @org.host_name,
          onboarding_status: '1',
          sign_in_at: @actor.last_sign_in_at.to_i.to_s,
          user_role: @actor.organization_role,
          user_id: @actor.id.to_s,
          full_name: @actor.full_name,
          user_handle: @actor.handle,
          member_user_id: "fake_profile_user_id",
          platform_version_number: "1.2.3",
          user_agent: "fake user_agent",
        }.merge(@sample_topic_1.symbolize_keys)
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    user_profile_recorder.push_added_topic(
      Analytics::MetricsRecorder.org_attributes(@org),
      Analytics::MetricsRecorder.user_attributes(@actor),
      {user_id: "fake_profile_user_id", org_id: 1 },
      "expert_topics",
      @sample_topic_1,
      timestamp,
      metadata
    )
  end

  test ".push_removed_topic" do
    timestamp = Time.now.to_i
    metadata = {
      platform: "fake platform",
      user_agent: "fake user_agent",
      platform_version_number: "1.2.3",
      is_admin_request: 0
    }
    InfluxdbRecorder.unstub(:record)
    InfluxdbRecorder.expects(:record).with(
      "users",
      {
        timestamp: timestamp,
        tags: {
          event: "user_expertise_topic_removed",
          _user_id: @actor.id.to_s,
          org_id: @org.id.to_s,
          is_system_generated: '0',
        },
        values: {
          platform: "fake platform",
          user_org_uid: @fb_ip_provider.uid,
          is_admin_request: '0',
          org_hostname: @org.host_name,
          onboarding_status: '1',
          sign_in_at: @actor.last_sign_in_at.to_i.to_s,
          user_role: @actor.organization_role,
          user_id: @actor.id.to_s,
          full_name: @actor.full_name,
          user_handle: @actor.handle,
          member_user_id: "fake_profile_user_id",
          platform_version_number: "1.2.3",
          user_agent: "fake user_agent",
        }.merge(@sample_topic_1.symbolize_keys)
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    user_profile_recorder.push_removed_topic(
      Analytics::MetricsRecorder.org_attributes(@org),
      Analytics::MetricsRecorder.user_attributes(@actor),
      {user_id: "fake_profile_user_id", org_id: 1 },
      "expert_topics",
      @sample_topic_1,
      timestamp,
      metadata
    )
  end

end
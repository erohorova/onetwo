require 'test_helper'
require 'action_controller'

class Analytics::UserLoginMetricsRecorderTest < ActionController::TestCase

  test 'inherits from UserMetricsRecorder' do
    assert(
      Analytics::UserMetricsRecorder,
      Analytics::UserLoginMetricsRecorder.superclass
    )
  end

  test 'calls UserMetricsRecorder.record' do
    org = create :organization
    user = create :user, organization: org
    timestamp = Time.now.to_i
    metadata = {
      user_id: user.id,
      foo: "bar",
      is_admin_request: 0
    }
    Analytics::UserMetricsRecorder.expects(:record).with(
      timestamp: timestamp,
      event: "user_logged_in",
      actor: user,
      org: org,
      additional_data: metadata
    )
    Analytics::UserLoginMetricsRecorder.record(
      metadata: metadata,
      actor: user,
      org: org,
      timestamp: timestamp
    )
  end

  test 'calls influx recorder' do
    InfluxdbRecorder.unstub(:record)
    org = create :organization
    user = create :user, organization: org
    timestamp = Time.now.to_i
    metadata = {
      user_id: user.id,
      foo: "bar",
      controller: "SomeController",
      action: "SomeAction",
      is_admin_request: 0
    }
    InfluxdbRecorder.expects(:record).with(
      'users',
      {
        values: {
          sign_in_at: user.last_sign_in_at.to_i.to_s,
          user_id: user.id.to_s,
          user_handle: user.handle,
          full_name: user.full_name,
          user_role: user.organization_role,
          is_admin_request: '0',
          org_hostname: org.host_name,
          onboarding_status: '1',
          is_admin_request: '0',
          controller_action: 'SomeController#SomeAction',
        },
        tags: {
          is_system_generated: '1',
          _user_id: user.id.to_s,
          org_id: user.organization.id.to_s,
          event: 'user_logged_in',
        },
        timestamp: timestamp,
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    Analytics::UserLoginMetricsRecorder.record(
      metadata: metadata,
      actor: Analytics::MetricsRecorder.user_attributes(user),
      org: Analytics::MetricsRecorder.org_attributes(org),
      timestamp: timestamp
    )
  end

end

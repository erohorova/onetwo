# frozen_string_literal: true

require 'test_helper'
class ChannelMetricsRecorderTest < ActiveSupport::TestCase
  setup do
    author_first_name = Faker::Name.first_name
    actor_first_name = Faker::Name.first_name

    author_last_name = Faker::Name.last_name
    actor_last_name = Faker::Name.last_name

    @org = create :organization,
      name: 'LXP',
      host_name: 'spark'
    @actor = create :user,
      first_name: actor_first_name,
      last_name: actor_last_name,
      organization: @org
    @channel = create :channel,
      user: @actor,
      organization: @org
  end

  test 'should extract tags' do
    channel_create = Analytics::ChannelMetricsRecorder::EVENT_CHANNEL_CREATED
    tag_set = Analytics::ChannelMetricsRecorder.extract_tags(
      @channel, channel_create, @actor
    ).with_indifferent_access

    valid_keys = %w{
      _channel_id _user_id org_id is_channel_featured is_channel_curated
      is_channel_public is_ecl_enabled platform event
    }
    tag_set.assert_valid_keys(valid_keys)
  end

  test 'should extract fields' do
    field_set = Analytics::ChannelMetricsRecorder
                .extract_fields(@channel, @actor)
                .with_indifferent_access
    valid_keys = [
      "channel_id", "user_id", "channel_name", "is_channel_featured",
      "is_channel_curated", "is_channel_public", "is_ecl_enabled",
      "user_full_name", "user_handle"
    ]
    field_set.assert_valid_keys(valid_keys)
  end

  test 'should record channel event #create' do
    InfluxdbRecorder.unstub :record
    channel_create = Analytics::ChannelMetricsRecorder::EVENT_CHANNEL_CREATED
    timestamp = Time.now.to_i
    field_set = channel_field_set(@channel, @actor)
    tag_set = channel_tag_set(@channel, channel_create, @actor)

    InfluxdbRecorder.expects(:record).with 'channels',
      {
        values: field_set.compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    

    Analytics::ChannelMetricsRecorder.record(
      event: 'channel_created',
      channel: Analytics::MetricsRecorder.channel_attributes(@channel),
      actor: Analytics::MetricsRecorder.user_attributes(@actor),
      timestamp: timestamp,
      additional_data: {
        platform: 'web',
        user_agent: 'chrome',
        is_admin_request: 0
      }
    )
  end

  test 'should record channel event #visit' do
    channel_visit = Analytics::ChannelMetricsRecorder::EVENT_CHANNEL_VISITED
    timestamp = Time.now.to_i
    field_set = channel_field_set(@channel, @actor)
    tag_set = channel_tag_set(@channel, channel_visit, @actor)
    InfluxdbRecorder.unstub :record
    InfluxdbRecorder.expects(:record).with 'channels',
      {
        values: field_set.compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }

    Analytics::ChannelMetricsRecorder.record(
      channel: Analytics::MetricsRecorder.channel_attributes(@channel),
      event: 'channel_visited',
      actor: Analytics::MetricsRecorder.user_attributes(@actor),
      timestamp: timestamp,
      additional_data: {
        platform: 'web',
        user_agent: 'chrome',
        is_admin_request: 0
      }
    )
  end

  test 'should include change-related data if given' do
    timestamp = Time.now.to_i
    field_set = channel_field_set(@channel, @actor)
    tag_set = channel_tag_set(@channel, "channel_edited", @actor)
    InfluxdbRecorder.unstub :record
    InfluxdbRecorder.expects(:record).with 'channels',
      {
        values: field_set.compact.merge(
          old_val: "2",
          new_val: "1",
          changed_column: "label",
        ),
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }

    Analytics::ChannelMetricsRecorder.record(
      channel: Analytics::MetricsRecorder.channel_attributes(@channel),
      event: 'channel_edited',
      actor: Analytics::MetricsRecorder.user_attributes(@actor),
      timestamp: timestamp,
      changed_column: "label",
      new_val: 1, # will be converted to string
      old_val: "2",
      additional_data: {
        platform: 'web',
        user_agent: 'chrome',
        is_admin_request: 0
      }
    )
  end

  test 'records card_edited event for whitelisted columns' do
    whitelist = %w{foo bar}
    Analytics::ChannelMetricsRecorder.stubs(:whitelisted_change_attrs)
                                     .returns(whitelist)
    do_record = -> (changed_col) {
      Analytics::ChannelMetricsRecorder.record(
        channel: {},
        event: 'channel_edited',
        actor: {},
        timestamp: 0,
        changed_column: changed_col,
        new_val: 0,
        old_val: 1,
        additional_data: {}
      )
    }
    whitelist.each do |changed_col|
      Analytics::ChannelMetricsRecorder.expects(:influxdb_record)
      do_record.call(changed_col)
    end
  end


  test 'doesnt record card_edited event for non-whitelisted columns' do
    whitelist = %w{foo bar}
    test_columns = %w{asd bsd}
    Analytics::ChannelMetricsRecorder.stubs(:whitelisted_change_attrs)
                                  .returns(whitelist)
    do_record = -> (changed_col) {
      Analytics::ChannelMetricsRecorder.record(
        channel: {},
        event: 'channel_edited',
        actor: {},
        timestamp: 0,
        changed_column: changed_col,
        new_val: 0,
        old_val: 1,
        additional_data: {}
      )
    }
    Analytics::ChannelMetricsRecorder.expects(:influxdb_record).never
    test_columns.each do |changed_col|
      do_record.call(changed_col)
    end
  end

  test 'whitelisted_change_attrs' do
    assert_equal(
      %w{
        label
        description
        is_private
        curate_only
        ecl_enabled
        curate_ugc
        allow_follow
        is_open
        only_authors_can_post
      },
      Analytics::ChannelMetricsRecorder.whitelisted_change_attrs
    )
  end

  test 'is_ignored_event?' do
    whitelist = %w{foo bar}
    Analytics::ChannelMetricsRecorder.stubs(:whitelisted_change_attrs)
                                    .returns(whitelist)
    assert Analytics::ChannelMetricsRecorder.is_ignored_event? "asd"
    refute Analytics::ChannelMetricsRecorder.is_ignored_event? "foo"
    refute Analytics::ChannelMetricsRecorder.is_ignored_event? nil
  end

  test 'valid record args' do
    invalid_event = 'invalid event'
    assert_raises do
      Analytics::ChannelMetricsRecorder.record(
        channel: Analytics::MetricsRecorder.channel_attributes(@channel),
        event: invalid_event,
        actor: Analytics::MetricsRecorder.user_attributes(@actor),
        timestamp: Time.now.to_i,
        additional_data: {
          platform: 'web',
          user_agent: 'chrome',
          is_admin_request: 0
        }
      )
    end
  end

  test 'valid registered events' do
    registered_events = {
      "channel_created"                       => true,
      "channel_deleted"                       => true,
      "channel_visited"                       => true,
      "channel_followed"                      => true,
      "channel_unfollowed"                    => true,
      "channel_edited"                        => true,
      "channel_promoted"                      => true,
      "channel_unpromoted"                    => true,
      "channel_course_added"                  => true,
      "channel_course_removed"                => true,
    }
    assert_equal(
      registered_events,
      Analytics::ChannelMetricsRecorder::VALID_EVENTS
    )
  end

  test 'card measurement name' do
    assert_equal 'channels', Analytics::ChannelMetricsRecorder::MEASUREMENT
  end

  def channel_field_set(channel, actor)
    {
      channel_id:  channel.id.to_s,
      user_id:  actor.id.to_s,
      channel_name:  channel.label,
      user_agent:  'chrome',
      user_full_name: actor&.full_name,
      user_handle: actor&.handle,
      is_admin_request: '0',
      is_channel_featured: (channel.is_promoted && '1') || '0',
      is_channel_curated: (channel.curate_only && '1') || '0',
      is_channel_public: (!channel.is_private && '1') || '0',
      is_ecl_enabled: (channel.ecl_enabled && '1') || '0',
      platform: 'web',
    }
  end

  def channel_tag_set(channel, event, actor)
    {
      _channel_id: channel.id.to_s,
      _user_id: actor.id.to_s,
      is_system_generated: '0',
      event: event,
      org_id: channel.organization_id.to_s,
    }
  end
end
require 'test_helper'
class ChannelCollaboratorRecorderTest < ActiveSupport::TestCase

  test ".record calls ChannelMetricsRecorder.record" do
    actor = User.new
    channel = Channel.new
    timestamp = Time.now.to_i
    metadata = {
      user_id: actor.id,
      foo: "bar",
      is_admin_request: 0
    }
    Analytics::ChannelMetricsRecorder.expects(:record).with(
      channel: channel,
      actor: actor,
      event: "channel_collaborator_added",
      timestamp: timestamp,
      additional_data: metadata
    )
    Analytics::ChannelCollaboratorRecorder.record(
      user_id: nil,
      channel: channel,
      actor: actor,
      additional_data: metadata,
      timestamp: timestamp,
      event: "channel_collaborator_added"
    )
  end

  test ".record ends up calling influxdb_record with correct args" do
    org = create :organization
    actor = create :user,
      handle: "fake-handle",
      first_name: "first",
      last_name: "last"
    collaborator = create :user
    channel = create :channel,
      organization: org,
      label: "fake-label",
      is_private: true,
      is_promoted: false,
      ecl_enabled: false,
      curate_only: false

    timestamp = Time.now.to_i
    metadata = {
      user_id: actor.id,
      platform: "fake platform",
      is_admin_request: 0
    }
    InfluxdbRecorder.unstub :record
    InfluxdbRecorder.expects(:record).with(
      "channels",
      {
        values: {
          is_channel_featured: '0',
          is_channel_curated: '0',
          is_channel_public: '0',
          is_admin_request: '0',
          is_ecl_enabled: '0',
          user_id: actor.id.to_s,
          user_full_name: actor.full_name,
          channel_name: channel.label,
          collaborator_id: collaborator.id.to_s,
          channel_id: channel.id.to_s,
          user_handle: actor.handle,
          platform: "fake platform",
        },
        tags: {
          _channel_id: channel.id.to_s,
          _user_id: actor.id.to_s,
          is_system_generated: '0',
          org_id: channel.organization.id.to_s,
          event: "channel_collaborator_added",
        },
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    Analytics::ChannelCollaboratorRecorder.record(
      user_id: collaborator.id,
      channel: Analytics::MetricsRecorder.channel_attributes(channel),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      additional_data: metadata,
      timestamp: timestamp,
      event: "channel_collaborator_added"
    )
  end

end
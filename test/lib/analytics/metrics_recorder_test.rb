require 'test_helper'
class MetricsRecorderTest < ActiveSupport::TestCase

  setup do
    @recorder = Analytics::MetricsRecorder
    @user_score_recorder = Analytics::UserScoreRecorder
    @measurement = 'foos'
    @timestamp = Time.now.to_i
    @event = "stub_event"
    @actor_id = 4
    @owner_id = 5
    @org_id = 6
    @data = {
      tags: {
        craftsman_id: @owner_id,
        event: @event,
        org_id: @org_id
      },
      values: {
        buyer_id: @actor_id
      },
      timestamp: @timestamp
    }
    @stub_scores_table = {
      @event => {
        "scores" => {
          "actor" => 10,
          "owner" => 20,
          "actor_and_owner" => 30
        },
        "refs" => {
          "owner_id" => ["tags", "craftsman_id"],
          "actor_id" => ["values", "buyer_id"]
        }
      }
    }
    @stub_recording_targets = [
      {
        actor_id: @actor_id,
        owner_id: @owner_id,
        user_id: @actor_id,
        role: "actor",
        score_value: 10
      },
      {
        actor_id: @actor_id,
        owner_id: @owner_id,
        user_id: @owner_id,
        role: "owner",
        score_value: 20
      }
    ]
  end

  test 'should invoke influxdb record with measurement and data' do
    measurement = 'test_measurement'
    data = {values: {card_id: 1}, tags: {user_id: 2, first_name: nil}}

    InfluxdbRecorder.expects(:record).with(
      measurement,
      {
        values: {card_id: 1},
        tags: {user_id: 2}
      },
      { 
        write_client:    nil,
        duplicate_check: true
      }
    )
    @recorder.influxdb_record(measurement, data)
  end

  test 'should add additional data to dataset' do
    class RecorderAdditionalDataTest < @recorder
      def self.merge_request_info(data, additional_data)
        merge_additional_data(data, additional_data)
      end
    end

    data = {values: {card_id: 1}, tags: {user_id: 2, first_name: nil}}

    additional_data = {
      platform_version_number: 2,
      platform: 'web',
      user_agent: 'chrome',
      is_admin_request: 0,
    }
    RecorderAdditionalDataTest.merge_request_info(data, additional_data)

    assert_equal 'web', data[:values][:platform]
    assert_equal 2, data[:values][:platform_version_number]
    assert_equal 'chrome', data[:values][:user_agent]
    assert_equal '0', data[:values][:is_admin_request]
    assert_equal "0", data[:tags][:is_system_generated]
  end

  test '.influxdb_record calls .push_auxiliary_metrics' do
    measurement = 'foos'
    data = {tags: {}, values: {}}
    stub_response = OpenStruct.new(code: "204")
    InfluxdbRecorder.unstub :record
    InfluxdbRecorder.expects(:record).with(
      measurement, data, { 
        write_client:    nil,
        duplicate_check: true
      }
    ).returns(stub_response)
    @recorder.expects(:push_auxiliary_metrics).with(
      measurement, data
    )
    @recorder.influxdb_record(measurement, data)
  end

  test '.influxdb_record returns a response object' do
    stub_response = OpenStruct.new(code: "123456")
    InfluxdbRecorder.unstub :record
    InfluxdbRecorder.expects(:record).returns(stub_response)
    measurement = 'food'
    data = {tags: {}, values: {}}
    assert_equal @recorder.influxdb_record(
      measurement, data
    ), stub_response
  end

  test '.influxdb_record accepts a custom write client' do
    measurement = 'foos'
    data = {tags: {}, values: {}}
    stub_response = OpenStruct.new(code: "204")
    write_client = InfluxDB::Client.new
    InfluxdbRecorder.unstub :record
    InfluxdbRecorder.expects(:record).with(
      measurement,
      data,
      write_client:    write_client,
      duplicate_check: true
    ).returns(stub_response)
    @recorder.expects(:push_auxiliary_metrics).with(
      measurement,
      data,
    )
    @recorder.influxdb_record(
      measurement,
      data,
      write_client: write_client
    )
  end

  test '.push_auxiliary_metrics calls .push_user_score_metric' do
    measurement = 'foos'
    data = {tags: {}, values: {}}
    @recorder.expects(:push_user_score_metric).with(
      measurement, RecursiveOpenStruct.new(data)
    )
    @recorder.push_auxiliary_metrics(measurement, data)
  end

  test '.push_auxiliary_metric doesnt push user score for admin requests' do
    measurement = 'foos'
    data = { values: { is_admin_request: 1 } }
    @recorder.expects(:push_user_score_metric).never
    @recorder.push_auxiliary_metrics(measurement, data)
  end

  test '.push_user_score_metric raises error with missing timestamp' do
    @user_score_recorder.stubs(:scores_table).returns @stub_scores_table
    error = assert_raises KeyError do
      @recorder.push_user_score_metric(
        @measurement,
        RecursiveOpenStruct.new(@data.except(:timestamp))
      )
    end
    assert_equal error.message, "key not found: :timestamp"
  end

  test '.push_user_score_metric returns early with nil/unknown event' do
    @user_score_recorder.stubs(:scores_table).returns @stub_scores_table
    @user_score_recorder.expects(:recording_targets).never
    @user_score_recorder.expects(:record).never
    @recorder.push_user_score_metric(
      @measurement,
      RecursiveOpenStruct.new(@data.tap { |data| data[:tags].delete :event })
    )
    @recorder.push_user_score_metric(
      @measurement,
      @data.tap { |data| data[:tags][:event] = "unknown_event" }
    )
  end

  test '.push_user_score_metric returns if there are no actor/owner ids' do
    @user_score_recorder.stubs(:scores_table).returns @stub_scores_table
    stub_data_struct = RecursiveOpenStruct.new(@data)
    @user_score_recorder.expects(:find_actor_and_owner_ids).with(
      @event, stub_data_struct
    ).returns [nil, nil]
    @user_score_recorder.expects(:recording_targets).never
    @user_score_recorder.expects(:record).never
    @recorder.push_user_score_metric(
      @measurement,
      RecursiveOpenStruct.new(@data)
    )
  end

  test '.push_user_score_metric calls record for each recording target' do
    @user_score_recorder.stubs(:scores_table).returns @stub_scores_table
    @user_score_recorder.expects(:recording_targets).with(
      event: @event,
      actor_id: @actor_id,
      owner_id: @owner_id
    ).returns(@stub_recording_targets)
    @stub_recording_targets.each do |target|
      @user_score_recorder.expects(:record).with(target.merge(
        event: @event,
        timestamp: @timestamp,
        org_id: @org_id
      ))
    end
    @recorder.push_user_score_metric(
      @measurement,
      RecursiveOpenStruct.new(@data)
    )
  end

  test '.send_query formats query string and sends to influx' do
    query = "SELECT foo FROM bar"
    stub_result = :okie_dokie
    @recorder.expects(:format_query).with(query).returns(query)
    INFLUXDB_CLIENT.expects(:query).with(query, {}).returns stub_result
    assert_equal stub_result, @recorder.send_query(query)
  end

  test '.format_query removes extra spaces and newlines' do
    query = "\n 123  456\n 789\n "
    assert_equal "123 456 789", @recorder.format_query(query)
  end

  test '.influxdb_bulk_record' do
    measurement = "foo"
    data_points = [
      { a: nil, tags: { b: 1, c: nil }, values: { d: 2, e: nil } }
    ]
    InfluxdbRecorder.expects(:bulk_record).with(
      measurement,
      [{tags: {b: 1}, values: {d: 2}}]
    )
    @recorder.influxdb_bulk_record(measurement, data_points)
  end

  test '.influxdb_bulk_record with custom write client' do
    measurement = "foo"
    data_points = [
      { a: nil, tags: { b: 1, c: nil }, values: { d: 2, e: nil } }
    ]
    write_client = InfluxDB::Client.new
    InfluxdbRecorder.expects(:bulk_record).with(
      measurement,
      [{tags: {b: 1}, values: {d: 2}}],
      write_client: write_client
    )
    @recorder.influxdb_bulk_record(
      measurement,
      data_points,
      write_client: write_client
    )
  end

  test 'logs error if non-success code returned' do
    InfluxdbRecorder.unstub(:record)
    INFLUXDB_CLIENT.unstub(:write_point)

    error_code = OpenStruct.new(code: "500")
    INFLUXDB_CLIENT.class::CLIENT.expects(:write_point).returns error_code
    measurement = "cards"
    data = {
      tags: { analytics_version: '2.0.0' },
      values: { foo: "bar", is_admin_request: '0' },
      timestamp: Time.now.to_i
    }
    
    log.expects(:error).with(
      "Received non-successful status code writing to Influx. " +
      "code = 500, measurement = cards, " +
      "data = #{data}"
    )

    Analytics::MetricsRecorder.influxdb_record(measurement, data)
  end

  test 'doesnt log error if success code returned' do
    InfluxdbRecorder.unstub(:record)
    INFLUXDB_CLIENT.unstub(:write_point)

    success_code = OpenStruct.new(code: "204")
    INFLUXDB_CLIENT.class::CLIENT.expects(:write_point).returns success_code
    
    time = rand(1e9).to_s.rjust 19, '0'
    measurement = "cards"
    data = {
      tags: { analytics_version: '2.0.0' },
      values: { foo: "bar", is_admin_request: '0' },
      timestamp: time
    }
    
    log.expects(:error).never

    Analytics::MetricsRecorder.influxdb_record(measurement, data)
  end

  test 'doesnt log error if writing to redis queue' do
    begin
      InfluxdbRecorder.unstub(:record)

      org_id = '123'
      InfluxdbRecorder::ORGS_USING_INFLUX_SERVICE.add org_id

      time = rand(1e9).to_s.rjust 19, '0'
      measurement = "cards"
      data = {
        tags: { analytics_version: '2.0.0', org_id: org_id },
        values: { foo: "bar", is_admin_request: '0' },
        timestamp: time
      }
      
      log.expects(:error).never

      Analytics::MetricsRecorder.influxdb_record(measurement, data)
    ensure
      InfluxdbRecorder::ORGS_USING_INFLUX_SERVICE.clear
    end      
  end

  test 'doesnt log error if the event is a duplicate' do
    InfluxdbRecorder.unstub(:record)

    measurement = "cards"
    data = {
      tags: { analytics_version: '2.0.0' },
      values: { foo: "bar", is_admin_request: '0' },
      timestamp: Time.now.to_i
    }

    InfluxdbRecorder.stubs(:is_duplicate?).returns true
    log.expects(:error).never

    Analytics::MetricsRecorder.influxdb_record(measurement, data)    
  end


end

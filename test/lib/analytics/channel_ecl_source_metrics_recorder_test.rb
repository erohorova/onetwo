# frozen_string_literal: true

require 'test_helper'

class ChannelEclSourceMetricsRecorderTest < ActiveSupport::TestCase

  test ".record calls ChannelMetricsRecorder.record" do
    actor = User.new
    channel = Channel.new
    timestamp = Time.now.to_i
    metadata = {
      user_id: actor.id,
      foo: "bar",
      is_admin_request: 0
    }
    Analytics::ChannelMetricsRecorder.expects(:record).with(
      channel: channel,
      actor: actor,
      event: "channel_ecl_source_added",
      timestamp: timestamp,
      additional_data: metadata
    )
    Analytics::ChannelEclSourceMetricsRecorder.record(
      ecl_source_id: nil,
      channel: channel,
      actor: actor,
      additional_data: metadata,
      timestamp: timestamp,
      event: "channel_ecl_source_added"
    )
  end

  test ".record ends up calling influxdb_record with correct args" do
    org = create :organization
    actor = create :user,
      handle: "fake-handle",
      first_name: "first",
      last_name: "last"
    channel = create :channel,
      organization: org,
      label: "fake-label",
      is_private: true,
      is_promoted: false,
      ecl_enabled: false,
      curate_only: false
    timestamp = Time.now.to_i
    metadata = {
      user_id: actor.id,
      platform: "fake platform",
      is_admin_request: 0
    }
    ecl_source = build :ecl_source, channel: channel, ecl_source_id: 9000
    ecl_source.stubs(:setup_fetch)
    ecl_source.save!
    InfluxdbRecorder.unstub :record
    InfluxdbRecorder.expects(:record).with(
      "channels",
      {
        values: {
          channel_id: channel.id.to_s,
          user_id: actor.id.to_s,
          user_full_name: actor.full_name,
          user_handle: actor.handle,
          channel_name: channel.label,
          ecl_source_id: ecl_source.ecl_source_id.to_s,
          is_channel_featured: '0',
          is_channel_curated: '0',
          is_channel_public: '0',
          is_admin_request: '0',
          is_ecl_enabled: '0',
          platform: "fake platform",
        },
        tags: {
          _channel_id: channel.id.to_s,
          is_system_generated: '0',
          _user_id: actor.id.to_s,
          org_id: channel.organization.id.to_s,
          event: "channel_ecl_source_added",
        },
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    Analytics::ChannelEclSourceMetricsRecorder.record(
      ecl_source_id: ecl_source.ecl_source_id,
      channel: Analytics::MetricsRecorder.channel_attributes(channel),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      additional_data: metadata,
      timestamp: timestamp,
      event: "channel_ecl_source_added"
    )
  end

  test 'valid registered events' do
    registered_events = %w{
      channel_ecl_source_added
      channel_ecl_source_removed
    }
    assert_equal(
      registered_events,
      Analytics::ChannelEclSourceMetricsRecorder::CHANNEL_ECL_SOURCE_EVENTS
    )
  end
end
require 'test_helper'

class Analytics::UserProfileVisitMetricsRecorderTest < ActionController::TestCase

  test "calls UserMetricsRecorder with expected args" do
    org = build :organization
    actor = build :user
    visited_profile_user = build :user
    event = "user_profile_visited"
    timestamp = Time.now.to_i
    metadata = {
      platform: "platform",
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0
    }
    Analytics::UserMetricsRecorder.expects(:record).with(
      org: org,
      timestamp: timestamp,
      actor: actor,
      event: event,
      additional_data: metadata
    )
    Analytics::UserProfileVisitMetricsRecorder.record(
      org: org,
      timestamp: timestamp,
      actor: actor,
      visited_profile_user: visited_profile_user.attributes,
      additional_data: metadata
    )
  end

  test "calls InfluxdbRecorder with expected args" do
    org = create :organization
    actor = create :user, organization: org
    fb_ip_provider = create(:identity_provider,
      type: 'FacebookProvider',
      uid: 'SPARK123',
      user: actor
    )
    visited_profile_user = create :user
    event = "user_profile_visited"
    timestamp = Time.now.to_i
    metadata = {
      platform: "platform",
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0
    }
    InfluxdbRecorder.unstub :record
    InfluxdbRecorder.expects(:record).with(
      'users',
      {
        values: {
          org_hostname: org.host_name,
          user_org_uid: fb_ip_provider.uid,
          is_admin_request: '0',
          onboarding_status: '1',
          sign_in_at: '0',
          user_role: 'member',
          user_id: actor.id.to_s,
          user_handle: actor.handle,
          full_name: actor.full_name,
          platform_version_number: 'platform_version_number',
          user_agent: 'user_agent',
          visited_profile_user_id: visited_profile_user.id.to_s,
          platform: 'platform',
        },
        tags: {
          _user_id: actor.id.to_s,
          is_system_generated: '0',
          org_id: org.id.to_s,
          event: 'user_profile_visited',
        },
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    Analytics::UserProfileVisitMetricsRecorder.record(
      timestamp: timestamp,
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      visited_profile_user: Analytics::MetricsRecorder.user_attributes(visited_profile_user),
      additional_data: metadata
    )
  end

end

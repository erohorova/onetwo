require 'test_helper'
class UserBadgeRecorderTest < ActiveSupport::TestCase

  setup do
    author_first_name = Faker::Name.first_name
    actor_first_name = Faker::Name.first_name

    author_last_name = Faker::Name.last_name
    actor_last_name = Faker::Name.last_name

    @org = create(:organization, name: 'LXP', host_name: 'spark' )
    @user = create(:user, first_name: author_first_name,
      last_name: author_last_name, organization: @org,
      sign_in_count: 2, last_sign_in_at: 2.days.ago,
      last_sign_in_ip: Faker::Internet.ip_v4_address,
      current_sign_in_ip: Faker::Internet.ip_v4_address
    )
  end

  test 'valid registered events' do
    registered_events = ['user_badge_completed']
    assert_equal registered_events, Analytics::UserBadgeRecorder::USER_BADGE_EVENTS
  end

  test 'invalid events' do
    invalid_event = 'user_badge_completed_testing_invalid'
    badge = create(:badge, organization: @org, type: "CardBadge")
    pathway = create(:card, organization: @org, card_type: "pack", author: @user)
    badging = Badging.create(badge_id: badge.id, badgeable_id: pathway.id, title: "titleee", type: "CardBadging")
    user_badge  = UserBadge.create!(user: @user, badging_id: badging.id)

    metadata = {
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0,
      user_id: @user.id
     }
    timestamp = Time.now.to_i
    exception = assert_raise do 
      Analytics::UserBadgeRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      actor: Analytics::MetricsRecorder.user_attributes(@user),
      event: invalid_event,
      timestamp: timestamp,
      user_badge: Analytics::MetricsRecorder.user_badge_attributes(user_badge),
      additional_data: metadata
    )
    end
    assert_equal exception.class, RuntimeError
    assert_equal "Invalid event: #{invalid_event}. Valid Events: #{Analytics::UserBadgeRecorder::USER_BADGE_EVENTS.join(",")}", exception.message
  end

  test 'includes user badge attributes' do
    badge = create(:badge, organization: @org, type: "CardBadge")
    pathway = create(:card, organization: @org, card_type: "pack", author: @user)
    badging = Badging.create(badge_id: badge.id, badgeable_id: pathway.id, title: "titleee", type: "CardBadging")
    user_badge  = UserBadge.create!(user: @user, badging_id: badging.id)
    event = "user_badge_completed"

    InfluxdbRecorder.unstub :record
    timestamp = Time.now.to_i
    
    
    metadata = {
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0,
      user_id: @user.id
     }
     InfluxdbRecorder.expects(:record).with( 'users',
      {
        tags: {
          org_id: @org.id.to_s,
          event: event,
          _user_id: @user.id.to_s,
          is_system_generated: '1'
        },
        values: {
          sign_in_ip: @user.last_sign_in_ip,
          sign_in_at: @user.last_sign_in_at.to_i.to_s,
          user_id: @user.id.to_s,
          user_role: @user.organization_role,
          org_hostname: @org.host_name,
          onboarding_status: '1',
          full_name: @user.full_name,
          user_handle: @user.handle,
          platform_version_number: 'platform_version_number',
          user_agent: 'user_agent',
          is_admin_request: '0',
          badge_id: user_badge.id.to_s,
          badge_title: badging.title,
          badge_type: badging.type
        },
        timestamp: timestamp
      },
      {
        write_client:    nil,
        duplicate_check: true
      }
    )
    Analytics::UserBadgeRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      actor: Analytics::MetricsRecorder.user_attributes(@user),
      event: event,
      user_badge: Analytics::MetricsRecorder.user_badge_attributes(user_badge),
      timestamp: timestamp,
      additional_data: metadata
    )
    end
end
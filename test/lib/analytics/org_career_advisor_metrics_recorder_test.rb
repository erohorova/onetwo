require 'test_helper'
class OrgCareerAdvisorMetricsRecorderTest < ActiveSupport::TestCase
  setup do
    admin_first_name = Faker::Name.first_name
    admin_last_name = Faker::Name.last_name

    @org = create(:organization, name: 'LXP', host_name: 'spark' )
    @admin = create(:user, first_name: admin_first_name,
      last_name: admin_last_name, organization: @org,
      sign_in_count: 2, last_sign_in_at: 2.days.ago,
      last_sign_in_ip: Faker::Internet.ip_v4_address,
      current_sign_in_ip: Faker::Internet.ip_v4_address
    )
  end

  test 'valid registered events' do
    registered_events = 'org_career_advisor_created'
    assert Analytics::OrgCareerAdvisorMetricsRecorder::ORG_CAREER_ADVISOR_EVENTS.include? registered_events
  end

  test 'invalid events' do
    invalid_event = 'org_career_advisor_created_invalid'
    career_advisor = CareerAdvisor.create(id: 10000001 , organization: @org, skill: 'data_scientist', links: ["https://qa.cmnetwork.co"])

     metadata = {
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0,
      user_id: @admin.id
     }
    timestamp = Time.now.to_i
    exception = assert_raise do 
      Analytics::OrgCareerAdvisorMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      actor: Analytics::MetricsRecorder.user_attributes(@admin),
      event: invalid_event,
      timestamp: timestamp,
      career_advisor: Analytics::MetricsRecorder.career_advisor_attributes(career_advisor),
      additional_data: metadata
    )
    end
    assert_equal exception.class, RuntimeError
    assert_equal "Invalid event: #{invalid_event}. Valid events: #{Analytics::OrgCareerAdvisorMetricsRecorder::ORG_CAREER_ADVISOR_EVENTS.join(",")}", exception.message
  end

  test 'includes career advisor attributes' do
    career_advisor = CareerAdvisor.create(id: 10000001 , organization: @org, skill: 'data_scientist', links: ["https://qa.cmnetwork.co"])
    event = "org_career_advisor_created"

    InfluxdbRecorder.unstub :record
    timestamp = Time.now.to_i


    metadata = {
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0,
      user_id: @admin.id
    }
    InfluxdbRecorder.expects(:record).with( 'organizations',
      {
        tags: {
          org_id: @org.id.to_s,
          event: event,
          _user_id: @admin.id.to_s,
          is_system_generated: '1'
        },
        values: {
          org_name: @org.name,
          org_slug: @org.slug,
          org_host_name: @org.host_name,
          org_is_open: @org.is_open ? '1' : '0',
          org_is_registrable: @org.is_registrable ? '1' : '0',
          org_status: @org.status,
          org_send_weekly_activity: @org.send_weekly_activity ? '1' : '0',
          org_show_sub_brand_on_login: @org.show_sub_brand_on_login ? '1' : '0',
          org_savannah_app_id: @org.savannah_app_id.to_s,
          org_social_enabled: @org.social_enabled ? '1' : '0',
          org_xapi_enabled: @org.xapi_enabled ? '1' : '0',
          org_show_onboarding: @org.show_onboarding ? '1' : '0',
          org_enable_role_based_authorization: @org.enable_role_based_authorization ? '1' : '0',
          platform_version_number: 'platform_version_number',
          user_agent: 'user_agent',
          user_id: @admin.id.to_s,
          is_admin_request: '0',
          career_advisor_id:    career_advisor.id.to_s,
          career_advisor_skill: career_advisor.skill,
          career_advisor_links: career_advisor.links
        },
        timestamp: timestamp
      },
      {
        write_client:    nil,
        duplicate_check: true
      }
    )
    Analytics::OrgCareerAdvisorMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      actor: Analytics::MetricsRecorder.user_attributes(@admin),
      event: event,
      career_advisor: Analytics::MetricsRecorder.career_advisor_attributes(career_advisor),
      timestamp: timestamp,
      additional_data: metadata
    )
  end
end
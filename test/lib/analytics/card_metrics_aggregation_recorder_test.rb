require 'test_helper'

class Analytics::CardMetricsAggregationRecorderTest < ActiveSupport::TestCase

  setup do
    @klass = Analytics::CardMetricsAggregationRecorder
    @org = create :organization
    @author = create :user, organization: @org
    @cards = create_list(:card, 2, {
      organization: @org,
      author: @author
    })
    @time = Time.now.utc
    Time.stubs(:now).returns @time
    @start_time = @time.beginning_of_day - 1.day
    @end_time = @time.end_of_day - 1.day

    query = (
      "select * from " +
      "(" +
        "select MEAN(num_likes) AS num_likes,"  +
        "MEAN(num_views) AS num_views," +
        "MEAN(num_completions) AS num_completions," +
        "MEAN(num_bookmarks) AS num_bookmarks," +
        "MEAN(num_comments) AS num_comments," +
        "MEAN(num_assignments) AS num_assignments " +
        "from (" +
          "select COUNT(card_id) AS num_likes from \"cards\" " +
          "where org_id = '#{@org.id}' AND event = 'card_liked' " +
          "AND time >= #{@start_time.to_i}s AND time <= #{@end_time.to_i}s " +
          "GROUP BY _card_id,time(1d),org_id" +
        "),(" +
          "select COUNT(card_id) AS num_views from \"cards\" " +
          "where org_id = '#{@org.id}' AND event = 'card_viewed' " +
          "AND time >= #{@start_time.to_i}s AND time <= #{@end_time.to_i}s " +
          "GROUP BY _card_id,time(1d),org_id" +
        "),(" +
          "select COUNT(card_id) AS num_completions from \"cards\" " +
          "where org_id = '#{@org.id}' AND event = 'card_marked_as_complete' " +
          "AND time >= #{@start_time.to_i}s AND time <= #{@end_time.to_i}s " +
          "GROUP BY _card_id,time(1d),org_id" +
        "),(" +
          "select COUNT(card_id) AS num_bookmarks from \"cards\" " +
          "where org_id = '#{@org.id}' AND event = 'card_bookmarked' " +
          "AND time >= #{@start_time.to_i}s AND time <= #{@end_time.to_i}s " +
          "GROUP BY _card_id,time(1d),org_id" +
        "),(" +
          "select COUNT(card_id) AS num_comments from \"cards\" " +
          "where org_id = '#{@org.id}' AND event = 'card_comment_created' " +
          "AND time >= #{@start_time.to_i}s AND time <= #{@end_time.to_i}s " +
          "GROUP BY _card_id,time(1d),org_id" +
        "),(" +
        "select COUNT(card_id) AS num_assignments from \"cards\" " +
          "where org_id = '#{@org.id}' AND event = 'card_assigned' " +
          "AND time >= #{@start_time.to_i}s AND time <= #{@end_time.to_i}s " +
          "GROUP BY _card_id,time(1d),org_id" +
        ")" +
        " GROUP BY _card_id,time(1d),org_id" +
      ")" +
      " where (" +
        "num_likes > 0 OR num_views > 0 OR num_completions > 0 " +
        "OR num_bookmarks > 0 OR num_comments > 0 OR num_assignments > 0" +
      ") ;"
    )
    params = {}
    query_response = [
      {
        "tags" => {
          "org_id"          => @org.id.to_s,
          "_card_id"        => @cards[0].id.to_s,
        },
        "values" => [{
          "time"            => (@start_time + 20.minutes).to_s,
          "num_likes"       => 1,
          "num_views"       => 2,
          "num_completions" => 3,
          "num_bookmarks"   => 4,
          "num_comments"    => 5,
          "num_assignments" => 6,
        }],
      },
      {
        "tags" => {
          "org_id"    => @org.id.to_s,
          "_card_id"  => @cards[1].id.to_s,
        },
        "values" => [{
          # This one is intentionally missing cols, so the default values are set.
          "time"      => (@start_time + 20.minutes).to_s,
          "num_likes" => 1,
        }]
      }
    ]
    INFLUXDB_CLIENT
      .stubs(:query)
      .with(query, params: params)
      .returns(query_response)
  end

  test "uses default opts for initialize" do
    inst = @klass.new
    assert_equal [@org.id],   inst.org_ids
    assert_equal @start_time, inst.start_time
    assert_equal @end_time,   inst.end_time
  end

  test "accepts custom opts for initialize" do
    inst = @klass.new(
      org_ids:    [:fake_org_id],
      start_time: :fake_time_1,
      end_time:   :fake_time_2
    )
    assert_equal [:fake_org_id], inst.org_ids
    assert_equal :fake_time_1,   inst.start_time
    assert_equal :fake_time_2,   inst.end_time
  end

  test "queries for all orgs when #run is called" do
    @klass.new.run
    records = CardMetricsAggregation.all.group_by(&:card_id)

    # One record was created for each card, since all results concerned a single day.
    assert_equal [1], records.values.map(&:length).uniq

    record1 = records[@cards[0].id][0]
    assert_equal(
      {
        "organization_id" => @org.id,
        "card_id"         => @cards[0].id,
        "num_likes"       => 1,
        "num_views"       => 2,
        "num_completions" => 3,
        "num_bookmarks"   => 4,
        "num_comments"    => 5,
        "num_assignments" => 6,
        "start_time"      => @start_time.to_i,
        "end_time"        => @end_time.to_i,
        "created_at"      => record1.created_at,
        "updated_at"      => record1.updated_at
      },
      record1.attributes.except("id")
    )

    record2 = records[@cards[1].id][0]
    assert_equal(
      {
        "organization_id" => @org.id,
        "card_id"         => @cards[1].id,
        "num_likes"       => 1,
        "num_views"       => 0,
        "num_completions" => 0,
        "num_bookmarks"   => 0,
        "num_comments"    => 0,
        "num_assignments" => 0,
        "start_time"      => @start_time.to_i,
        "end_time"        => @end_time.to_i,
        "created_at"      => record2.created_at,
        "updated_at"      => record2.updated_at
      },
      record2.attributes.except("id")
    )
  end

  test 'doesnt create record with no positive count' do
    record = {
      "organization_id" => @org.id,
      "card_id"         => Card.last.id + 1,
      "num_likes"       => 0,
      "num_views"       => 0,
      "num_completions" => 0,
      "num_bookmarks"   => 0,
      "num_comments"    => 0,
      "num_assignments" => 0,
      "time"            => @start_time.to_s
    }
    assert_no_difference ->{ CardMetricsAggregation.count } do
      @klass.new.send(:save_results, [record])
    end
  end

end
require 'test_helper'

class Analytics::UserMetricsAggregationRecorderTest < ActiveSupport::TestCase

  setup do
    @klass = Analytics::UserMetricsAggregationRecorder

    base_time = Time.now.utc
    Time.stubs(:now).returns base_time
    @start_time = base_time.beginning_of_day - 1.days
    @end_time = base_time.end_of_day - 1.days

    @stub_results = [{
      "tags" => {},
      "values" => [
        {
          "time"                       => @start_time.to_s,
          "user_id"                    => "1",
          "org_id"                     => "0",
          "smartbites_commented_count" => 1,
          "smartbites_completed_count" => 2,
          "smartbites_consumed_count"  => 3,
          "smartbites_created_count"   => 4,
          "smartbites_liked_count"     => 5,
          "time_spent_minutes"         => 6,
          "total_smartbite_score"      => 7,
          "total_user_score"           => 8
        },
        # The nil values in this record will get defaulted to zero
        # when saved to SQL.
        {
          "time"                       => @start_time.to_s,
          "user_id"                    => "2",
          "org_id"                     => "0",
          "smartbites_commented_count" => 1,
          "smartbites_completed_count" => nil,
          "smartbites_consumed_count"  => nil,
          "smartbites_created_count"   => nil,
          "smartbites_liked_count"     => nil,
          "time_spent_minutes"         => nil,
          "total_smartbite_score"      => nil,
          "total_user_score"           => nil
        }
      ]
    }]

    @expected_query = (
      "select * from \"user_scores_daily\" " +
      "where time >= %{start_date}s ;"
    )
    @expected_params = {
      start_date: @start_time.to_i
    }

    INFLUXDB_CLIENT.stubs(:query).with(
      @expected_query,
      params: @expected_params
    ).returns @stub_results
  end

  test '#initialize with default args' do
    inst = @klass.new
    assert_equal nil, inst.org_ids
    assert_equal @start_time.to_i, inst.start_time
    assert_equal @end_time.to_i, inst.end_time
  end

  test '#initialize with custom args' do
    opts = {
      org_ids: [0],
      start_time: (@start_time + 5.minutes).to_i,
      end_time: (@end_time + 5.minutes).to_i
    }
    inst = @klass.new(opts)
    opts.each do |key, val|
      assert_equal val, inst.send(key)
    end
  end

  test '#run' do
    inst = @klass.new
    inst.run
    records = UserMetricsAggregation.all.map(&:attributes).sort_by do |record|
      record["user_id"]
    end
    assert_equal 2, records.length
    expected = [
      {
        "smartbites_commented_count"   => 1,
        "smartbites_completed_count"   => 2,
        "smartbites_consumed_count"    => 3,
        "smartbites_created_count"     => 4,
        "smartbites_liked_count"       => 5,
        "time_spent_minutes"           => 6,
        "total_smartbite_score"        => 7,
        "total_user_score"             => 8,
        "user_id"                      => 1,
        "organization_id"              => 0,
        "start_time"                   => @start_time.to_i,
        "end_time"                     => @start_time.end_of_day.to_i,
      },
      {
        "smartbites_commented_count"   => 1,
        "smartbites_completed_count"   => 0,
        "smartbites_consumed_count"    => 0,
        "smartbites_created_count"     => 0,
        "smartbites_liked_count"       => 0,
        "time_spent_minutes"           => 0,
        "total_smartbite_score"        => 0,
        "total_user_score"             => 0,
        "user_id"                      => 2,
        "organization_id"              => 0,
        "start_time"                   => @start_time.to_i,
        "end_time"                     => @start_time.end_of_day.to_i,
      }
    ]
    actual = records.map do |record|
      record.except "created_at", "updated_at", "id"
    end
    assert_equal expected, actual
  end

  test 'doesnt save records with no positive counts' do
    record = {
      smartbites_commented_count: 0,
      smartbites_completed_count: 0,
      smartbites_consumed_count:  0,
      smartbites_created_count:   0,
      smartbites_liked_count:     0,
      time_spent_minutes:         0,
      total_smartbite_score:      0,
      total_user_score:           0,
      user_id:                    (User.last&.id || 0) + 1,
      organization_id:            0,
      start_time:                 @start_time.to_i,
      end_time:                   @end_time.to_i
    }
    assert_no_difference -> { UserMetricsAggregation.count } do
      @klass.new.push_results [record]
    end
  end

  test 'doesnt push duplicate records' do
    other_start_time = @start_time + 1.days

    # create a UserMetricsAggregation for a different start time
    UserMetricsAggregation.create!(
      "smartbites_commented_count"   => 1,
      "smartbites_completed_count"   => 0,
      "smartbites_consumed_count"    => 0,
      "smartbites_created_count"     => 0,
      "smartbites_liked_count"       => 0,
      "time_spent_minutes"           => 0,
      "total_smartbite_score"        => 0,
      "total_user_score"             => 0,
      "user_id"                      => 2,
      "organization_id"              => 0,
      "start_time"                   => other_start_time.to_i,
      "end_time"                     => other_start_time.end_of_day.to_i,
    )

    # Make Influx return a record with the same attributes
    @stub_results.tap(&:clear).push(
      "tags" => {},
      "values" => [{
        "time"                       => other_start_time.to_s,
        "user_id"                    => "2",
        "org_id"                     => "0",
        "smartbites_commented_count" => 1,
        "smartbites_completed_count" => 0,
        "smartbites_consumed_count"  => 0,
        "smartbites_created_count"   => 0,
        "smartbites_liked_count"     => 0,
        "time_spent_minutes"         => 0,
        "total_smartbite_score"      => 0,
        "total_user_score"           => 0
      }]
    )

    assert_no_difference -> { UserMetricsAggregation.count } do
      @klass.new.run
    end

  end

end
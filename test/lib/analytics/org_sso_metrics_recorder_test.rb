# frozen_string_literal: true

require 'test_helper'
class OrgSsoMetricsRecorderTest < ActiveSupport::TestCase
  setup do
    create_default_ssos
  end

  test 'records to influx' do
    Organization.any_instance.unstub :default_sso
    InfluxdbRecorder.unstub :record

    timestamp = Time.now.to_i
    org = create :organization
    org_sso = org.org_ssos.first

    actor = create :user, organization: org
    metadata = {
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0,
      user_id: actor.id
    }
    InfluxdbRecorder.expects(:record).with( 'organizations',
      {
        timestamp: timestamp,
        tags: {
          event: "org_sso_edited",
          org_id: org.id.to_s,
          is_system_generated: '1',
          _user_id: actor.id.to_s
        },
        values: {
          is_admin_request: '0',
          changed_column: "is_enabled",
          org_host_name: org.host_name,
          old_val: "1",
          new_val: "0",
          org_name: org.name,
          org_slug: org.slug,
          org_is_open: org.is_open ? '1' : '0',
          org_is_registrable: org.is_registrable ? '1' : '0',
          org_status: org.status,
          org_send_weekly_activity: org.send_weekly_activity ? '1' : '0',
          org_show_sub_brand_on_login: org.show_sub_brand_on_login ? '1' : '0',
          org_savannah_app_id: org.savannah_app_id.to_s,
          org_social_enabled: org.social_enabled ? '1' : '0',
          org_xapi_enabled: org.xapi_enabled ? '1' : '0',
          org_show_onboarding: org.show_onboarding ? '1' : '0',
          org_enable_role_based_authorization: org.enable_role_based_authorization ? '1' : '0',
          platform_version_number: 'platform_version_number',
          user_agent: 'user_agent',
          org_sso_name: org_sso.sso.name,
          user_id: actor.id.to_s
        }
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    Analytics::OrgSsoMetricsRecorder.record(
      sso: Analytics::MetricsRecorder.sso_attributes(org_sso.sso),
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      event: "org_sso_edited",
      timestamp: timestamp,
      additional_data: metadata,
      changed_column: "is_enabled",
      old_val: 1,
      new_val: 0 # Will get turned into string
    )
  end

  test 'should not record when non whitelisted attributes updated' do
    Organization.any_instance.unstub :default_sso
    InfluxdbRecorder.unstub :record

    timestamp = Time.now.to_i
    org = create :organization
    org_sso = org.org_ssos.first

    actor = create :user, organization: org

    InfluxdbRecorder.expects(:record).never
    Analytics::OrgSsoMetricsRecorder.record(
      sso: Analytics::MetricsRecorder.sso_attributes(org_sso.sso),
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      event: "org_sso_edited",
      timestamp: timestamp,
      additional_data: {},
      changed_column: "position",
      old_val: "5",
      new_val: "3"
    )
  end

  test 'should only consider whitelisted attributes for changed columns' do
    whitelisted_attributes = %w{
      is_enabled
      label_name
      saml_idp_sso_target_url
      saml_issuer
      saml_idp_cert
      saml_assertion_consumer_service_url
    }
    actual_attributes = Analytics::OrgSsoMetricsRecorder.whitelisted_change_attrs
    assert_same_elements whitelisted_attributes, actual_attributes
  end
end

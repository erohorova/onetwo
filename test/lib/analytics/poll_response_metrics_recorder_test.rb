require 'test_helper'
class Analytics::PollResponseMetricsRecorderTest < ActiveSupport::TestCase

  def shared_test_logic(opt_is_correct:, type:)
    InfluxdbRecorder.unstub(:record)
    org = create(:organization)
    actor = create :user, organization: org
    author = create :user, organization: org
    poll = create :card,
      card_type: "poll",
      is_public: true,
      author_id: author.id,
      organization: org,
      title: "foo"
    poll_opt = create :quiz_question_option,
      label: 'opt 1',
      is_correct: opt_is_correct,
      quiz_question: poll
    metadata = {
      platform: "platform",
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0
    }
    timestamp = Time.now.to_i
    InfluxdbRecorder.expects(:record).with('cards',
      {
        values: {
          card_subtype: 'text',
          org_name: org.name,
          user_id: actor.id.to_s,
          user_full_name: actor.full_name,
          user_handle: actor.handle,
          card_author_full_name: author.full_name,
          card_id: poll.id.to_s,
          platform_version_number: 'platform_version_number',
          user_agent: 'user_agent',
          card_title: poll.snippet,
          card_state: 'published',
          card_type: 'poll',
          card_author_id: author.id.to_s,
          is_admin_request: '0',
          is_live_stream: '0',
          org_hostname: org.host_name,
          is_card_promoted: '0',
          is_user_generated: '1',
          is_public: '1',
          platform: 'platform',
        }.merge(
          case type
          when "poll"
            {
              poll_option_label: 'opt 1',
              poll_option_id: poll_opt.id.to_s,
            }
          when "quiz"
            {
              quiz_option_label: 'opt 1',
              quiz_option_id: poll_opt.id.to_s,
            }
          end
        ).merge(
          opt_is_correct.nil? ? {} : { is_correct: opt_is_correct }
        ),
        tags: {
          is_system_generated: '0',
          org_id: org.id.to_s,
          _card_id: poll.id.to_s,
          _user_id: actor.id.to_s,
        }.merge(
          case type
          when "poll"
            { event: "card_poll_response_created" }
          when "quiz"
            { event: "card_quiz_response_created" }
          end
        ),
        :timestamp => timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    Analytics::PollResponseMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(org),
      timestamp: timestamp,
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      card: Analytics::MetricsRecorder.card_attributes(poll),
      quiz_question_options: [poll_opt].map(
        &Analytics::MetricsRecorder.method(:quiz_question_option_attributes)
      ),
      additional_data: metadata
    )
  end


  test "records the correct event for correct quiz responses" do
    shared_test_logic(
      opt_is_correct: 'true',
      type: 'quiz'
    )
  end

  test "records the correct event for wrong quiz responses" do
    shared_test_logic(
      opt_is_correct: 'false',
      type: 'quiz'
    )
  end

  test "records the correct event for polls" do
    shared_test_logic(
      opt_is_correct: nil,
      type: 'poll'
    )
  end

end

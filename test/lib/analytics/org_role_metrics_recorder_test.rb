# frozen_string_literal: true

require 'test_helper'
class OrgRoleMetricsRecorderTest < ActiveSupport::TestCase

  test 'should have role permission event registered' do
    klass = Analytics::OrgRoleMetricsRecorder
    assert_equal klass, Analytics::MetricsRecorder.get_recorder_for_event(
      'org_role_permission_enabled'
    )

    assert_equal klass, Analytics::MetricsRecorder.get_recorder_for_event(
      'org_role_permission_disabled'
    )
  end

  test 'records to influx with additional information' do
    InfluxdbRecorder.unstub :record
    timestamp = Time.now.to_i
    org = create :organization
    client = create(:client, organization: org)
    actor = create :user, organization: org
    metadata = {
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0,
      user_id: actor.id,
      role_id: 1,
      role_permission: 'MANAGE_CARD',
      role_name: 'admin',
    }
    InfluxdbRecorder.expects(:record).with( 'organizations',
      {
        timestamp: timestamp,
        tags: {
          org_id: org.id.to_s,
          event: "org_role_permission_enabled",
          is_system_generated: '1',
          _user_id: actor.id.to_s
        },
        values: {
          is_admin_request: '0',
          org_host_name: org.host_name,
          org_name: org.name,
          org_slug: org.slug,
          org_client_id: org.client_id.to_s,
          org_is_open: org.is_open ? '1' : '0',
          org_is_registrable: org.is_registrable ? '1' : '0',
          org_status: org.status,
          org_send_weekly_activity: org.send_weekly_activity ? '1' : '0',
          org_show_sub_brand_on_login: org.show_sub_brand_on_login ? '1' : '0',
          org_savannah_app_id: org.savannah_app_id.to_s,
          org_social_enabled: org.social_enabled ? '1' : '0',
          org_xapi_enabled: org.xapi_enabled ? '1' : '0',
          org_show_onboarding: org.show_onboarding ? '1' : '0',
          org_enable_role_based_authorization: org.enable_role_based_authorization ? '1' : '0',
          platform_version_number: 'platform_version_number',
          user_agent: 'user_agent',
          user_id: actor.id.to_s,
          role_id: '1',
          role_name: 'admin',
          role_permission: 'MANAGE_CARD'
        }
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    Analytics::OrgRoleMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      event: "org_role_permission_enabled",
      timestamp: timestamp,
      additional_data: metadata
    )
  end
end
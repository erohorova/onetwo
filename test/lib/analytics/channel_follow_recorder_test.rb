require 'test_helper'
class ChannelFollowRecorderTest < ActiveSupport::TestCase

  setup do
    author_first_name = Faker::Name.first_name
    author_last_name = Faker::Name.last_name

    @org = create(:organization, name: 'LXP', host_name: 'spark' )
    @actor = create :user, organization: @org
    @user = create(:user, first_name: author_first_name,
      last_name: author_last_name, organization: @org
    )

    @followed = create(:channel, label: 'Science', organization: @org)
    @follow = create(:follow, user: @user, followable: @followed)
    @actor = User.first
  end

  test 'valid registered events' do
    registered_events = ['channel_followed', 'channel_unfollowed']
    assert_equal registered_events, Analytics::ChannelFollowRecorder::CHANNEL_FOLLOW_EVENTS
  end

  test 'valid record args' do
    invalid_event = 'invalid event'
    InfluxdbRecorder.stubs(:record).returns :true

    assert_nothing_raised do
      Analytics::ChannelFollowRecorder.record(
        actor: Analytics::MetricsRecorder.user_attributes(@actor),
        follower: Analytics::MetricsRecorder.user_attributes(@follow.user),
        followed: Analytics::MetricsRecorder.channel_attributes(@follow.followable),
        timestamp: Time.now.to_i,
        event: Analytics::ChannelMetricsRecorder::EVENT_CHANNEL_FOLLOWED,
        additional_data: {}
      )
    end

    assert_raises do
      Analytics::ChannelFollowRecorder.record(
        actor: Analytics::MetricsRecorder.user_attributes(@actor),
        follower: Analytics::MetricsRecorder.user_attributes(@follow.user),
        followed: Analytics::MetricsRecorder.channel_attributes(@follow.followable),
        timestamp: Time.now.to_i,
        event: invalid_event,
        additional_data: {}
      )
    end
  end

  test 'should record channel follow event' do
    InfluxdbRecorder.unstub(:record)
    channel_follow = Analytics::ChannelMetricsRecorder::EVENT_CHANNEL_FOLLOWED

    field_set = {
      channel_id:  @followed.id.to_s,
      follower_id: @user.id.to_s,
      user_id:  @actor.id.to_s,
      user_full_name: @actor.full_name,
      user_handle: @actor.handle,
      channel_name:  @followed.label,
      user_agent:  'chrome',
      is_channel_featured: '0',
      is_channel_curated: '0',
      is_admin_request: '0',
      is_channel_public: '1',
      is_ecl_enabled: '0',
      platform: 'web',
    }

    tag_set = {
      is_system_generated: '0',
      _channel_id: @followed.id.to_s,
      _user_id: @actor.id.to_s,
      event: channel_follow,
      org_id: @followed.organization_id.to_s,
    }

    timestamp = Time.now.to_i

    InfluxdbRecorder.expects(:record).with('channels',
      {
        values: field_set.compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true

    Analytics::ChannelFollowRecorder.record(
      timestamp: timestamp,
      actor: Analytics::MetricsRecorder.user_attributes(@actor),
      follower: Analytics::MetricsRecorder.user_attributes(@user),
      followed: Analytics::MetricsRecorder.channel_attributes(@followed),
      event: channel_follow,
      additional_data: {
        platform: 'web',
        user_agent: 'chrome',
        is_admin_request: 0
      }
    )
  end

  test 'should record user unfollow event' do
    Follow.any_instance.stubs(:record_user_follow_action)
    Role.create_master_roles(@org)
    InfluxdbRecorder.unstub(:record)
    INFLUXDB_CLIENT.stubs :write_point

    channel_unfollow = Analytics::ChannelMetricsRecorder::EVENT_CHANNEL_UNFOLLOWED
    @follow.destroy #unfollow

    field_set = {
      channel_id:  @followed.id.to_s,
      follower_id: @user.id.to_s,
      user_id:  @actor.id.to_s,
      user_full_name: @actor.full_name,
      user_handle: @actor.handle,
      channel_name:  @followed.label,
      user_agent:  'chrome',
      is_channel_featured: '0',
      is_channel_curated: '0',
      is_admin_request: '0',
      is_channel_public: '1',
      is_ecl_enabled: '0',
      platform: 'web',
    }

    tag_set = {
      _channel_id: @followed.id.to_s,
      is_system_generated: '0',
      _user_id: @actor.id.to_s,
      event: channel_unfollow,
      org_id: @followed.organization_id.to_s,
    }

    timestamp = Time.now.to_i
    InfluxdbRecorder.expects(:record).with('channels',
      {
        values: field_set.compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true

    Analytics::ChannelFollowRecorder.record(
      actor: Analytics::MetricsRecorder.user_attributes(@actor),
      follower: Analytics::MetricsRecorder.user_attributes(@user),
      followed: Analytics::MetricsRecorder.channel_attributes(@followed),
      event: channel_unfollow,
      timestamp: timestamp,
      additional_data: {
        platform: 'web',
        user_agent: 'chrome',
        is_admin_request: 0
      }
    )
  end
end

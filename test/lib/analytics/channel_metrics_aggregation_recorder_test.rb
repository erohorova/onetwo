# frozen_string_literal: true

require 'test_helper'

class Analytics::ChannelMetricsAggregationRecorderTest < ActiveSupport::TestCase

  setup do
    # Alias for the klass being tested
    @klass = Analytics::ChannelMetricsAggregationRecorder

    # don't log stuff
    @klass.stubs :debug_log

    # Stub out current time
    @time = Time.now
    Time.stubs(:now).returns @time
    @end_of_yesterday = (@time.utc.end_of_day - 1.day).to_i
    @thirty_days_ago = (@time.utc.end_of_day - 1.day - 30.days).to_i
    @start_of_yesterday = (@time.utc.beginning_of_day - 1.day).to_i

    # First, create a channel and follower
    @org = create :organization
    @channel = create :channel, organization: @org
    @user = create :user, organization: @org
    Follow.create user: @user, followable: @channel

    # Next, create a card in the channel
    @card = create :card, author: @user, organization: @org
    ChannelsCard.create card: @card, channel: @channel

    # The expected result of the channel_followers_by_org function
    @channel_followers_by_org = {
      @org.id => {
        @channel.id => Set.new([@user.id])
      }
    }

    # The expected result of the cards_by_channel_and_org function
    @cards_by_channel_and_org = {
      @org.id => {
        @channel.id => Set.new([@card.id])
      }
    }

    # These are the expected Influx queries to determine monthly active user
    content_engagement = Analytics::ContentEngagementInfluxQuery
    events = content_engagement::ActiveUserEvents

    @monthly_active_users_queries = events.map do |measurement, names|
      query = (
        "select user_id,platform from \"#{measurement}\" " +
        "where analytics_version = %{version} AND time >= %{start_date}s " +
        "AND time <= %{end_date}s " +
        "AND (" + (
          names.each.with_index.reduce([]) do |memo, (name, idx)|
            memo + ["event = %{event_#{idx}}"]
          end.join(" OR ")
        ) + ") " +
        "AND (org_id = %{org_id_0}) " +
        "AND is_system_generated = %{is_system_generated} GROUP BY org_id ;"
      )
      params = {
        version: '2.0.0',
        start_date: @thirty_days_ago,
        end_date: @end_of_yesterday,
        group_by: 'org_id',
        is_system_generated: '0',
        org_id_0: @org.id.to_s
      }.merge(names.each.with_index.reduce({}) do |memo, (name, idx)|
        memo[:"event_#{idx}"] = name
        memo
      end)
      [query, params]
    end.sort_by(&:first)
    @monthly_active_users_queries.each do |(query, params)|
      result = [
        {
          "tags" => { "org_id" => @org.id.to_s },
          # The second platform here will end up as "null_platform"
          "values" => ["web", "?! , "].map do |platform|
            { "platform" => platform, "user_id" => @user.id.to_s }
          end
        }
      ]
      INFLUXDB_CLIENT.stubs(:query).with(query, params: params).returns result
    end
    # The expected results from the get_monthly_active_users method
    # It comes the data from the different measurements, without duplicates
    @num_monthly_active_users = {
      @org.id.to_s => {
        @channel.id.to_s => {
          @start_of_yesterday => {
            "num_monthly_active_users_in_web_platform" => 1,
            "num_monthly_active_users_in_null_platform" => 1,
            "num_monthly_active_users_total" => 1, # DOES NOT DUPLICATE
          }
        }
      }
    }

    # Stub the influx response for get_num_channel_visits
    INFLUXDB_CLIENT.stubs(:query).with(
      (
        "select COUNT(channel_id) from \"channels\" " +
        "where event = %{event} " +
        "AND (org_id = %{org_id_0}) " +
        "AND time >= %{start_date}s " +
        "AND time <= %{end_date}s " +
        "GROUP BY org_id,_channel_id,time(1d) ;"
      ),
      params: {
        event: 'channel_visited',
        group_by: 'org_id,_channel_id,time(1d)',
        start_date: @start_of_yesterday,
        org_id_0: @org.id.to_s,
        end_date: @end_of_yesterday
      }
    ).returns([
      {
        "tags" => {
          "_channel_id" => @channel.id.to_s,
          "org_id" => @org.id.to_s
        },
        "values" => [{
          "count" => 1,
          "time" => Time.at(@start_of_yesterday).to_s
        }]
      }
    ])
    # The expected result from the get_num_channel_visits method
    @num_channel_visits = {
      @org.id.to_s => {
        @channel.id.to_s => {
          @start_of_yesterday => {
            "num_channel_visits" => 1
          }
        }
      }
    }

    # Stub the influx response for get_num_channel_follows
    INFLUXDB_CLIENT.stubs(:query).with(
      (
        "select COUNT(channel_id) from \"channels\" " +
        "where event = %{event} " +
        "AND (org_id = %{org_id_0}) " +
        "AND time >= %{start_date}s " +
        "AND time <= %{end_date}s " +
        "GROUP BY org_id,_channel_id,time(1d) ;"
      ),
      params: {
        event: 'channel_followed',
        group_by: 'org_id,_channel_id,time(1d)',
        start_date: @start_of_yesterday,
        end_date: @end_of_yesterday,
        org_id_0: @org.id.to_s
      }
    ).returns([
      {
        "tags" => {
          "_channel_id" => @channel.id.to_s,
          "org_id" => @org.id.to_s
        },
        "values" => [{
          "count" => 1,
          "time" => Time.at(@start_of_yesterday).to_s
        }]
      }
    ])
    # The expected result from the get_num_channel_follows method
    @num_channel_follows = {
      @org.id.to_s => {
        @channel.id.to_s => {
          @start_of_yesterday => {
            "num_channel_follows" => 1
          }
        }
      }
    }

    # Stub the influx response for get_num_cards_added_to_channel
    INFLUXDB_CLIENT.stubs(:query).with(
      (
        "select card_id,channel_id,org_id from \"cards\" " +
        "where event = %{event} " +
        "AND (org_id = %{org_id_0}) " +
        "AND time >= %{start_date}s " +
        "AND time <= %{end_date}s ;"
      ),
      params: {
        event: 'card_added_to_channel',
        start_date: @start_of_yesterday,
        end_date: @end_of_yesterday,
        org_id_0: @org.id.to_s
      }
    ).returns([
      {
        "values" => [
          {
            "card_id" => @card.id.to_s,
            "channel_id" => @channel.id.to_s,
            "org_id" => @org.id.to_s,
            "time" => (Time.at(@start_of_yesterday) + 3.hours).to_s
          }
        ]
      }
    ])
    # The expected result from the get_num_cards_added_to_channel
    @num_cards_added_to_channel = {
      @org.id.to_s => {
        @channel.id.to_s => {
          @start_of_yesterday => {
            "num_cards_added_to_channel" => 1
          }
        }
      }
    }

    # Stub the influx response for get_num_cards_removed_from_channel
    INFLUXDB_CLIENT.stubs(:query).with(
      (
        "select card_id,channel_id,org_id from \"cards\" " +
        "where event = %{event} " +
        "AND (org_id = %{org_id_0}) " +
        "AND time >= %{start_date}s " +
        "AND time <= %{end_date}s ;"
      ),
      params: {
        event: 'card_removed_from_channel',
        start_date: @start_of_yesterday,
        end_date: @end_of_yesterday,
        org_id_0: @org.id.to_s
      }
    ).returns([
      {
        "values" => [
          {
            "card_id" => @card.id.to_s,
            "channel_id" => @channel.id.to_s,
            "org_id" => @org.id.to_s,
            "time" => (Time.at(@start_of_yesterday) + 3.hours).to_s
          }
        ]
      }
    ])
    # The expected result from the get_num_cards_removed_from_channel
    @num_cards_removed_from_channel = {
      @org.id.to_s => {
        @channel.id.to_s => {
          @start_of_yesterday => {
            "num_cards_removed_from_channel" => 1
          }
        }
      }
    }

    # Stub the influx response for get_num_cards_liked_in_channel
    INFLUXDB_CLIENT.stubs(:query).with(
      (
        "select COUNT(card_id) from \"cards\" where event = %{event} " +
        "AND (org_id = %{org_id_0}) " +
        "AND time >= %{start_date}s " +
        "AND time <= %{end_date}s " +
        "GROUP BY org_id,_card_id,time(1d) ;"
      ),
      params: {
        event: 'card_liked',
        start_date: @start_of_yesterday,
        end_date: @end_of_yesterday,
        group_by: 'org_id,_card_id,time(1d)',
        org_id_0: @org.id.to_s
      }
    ).returns([
      {
        "tags" => {
          "_card_id" => @card.id.to_s,
          "org_id" => @org.id.to_s
        },
        "values" => [{
          "count" => 1,
          "time" => Time.at(@start_of_yesterday).to_s
        }]
      }
    ])
    # The expected result from the get_num_cards_liked_in_channel
    @num_cards_liked_in_channel = {
      @org.id.to_s => {
        @channel.id.to_s => {
          @start_of_yesterday => {
            "num_cards_liked_in_channel" => 1
          }
        }
      }
    }

    # Stub the influx response for get_num_cards_commented_in_channel
    INFLUXDB_CLIENT.stubs(:query).with(
      (
        "select COUNT(card_id) from \"cards\" where event = %{event} " +
        "AND (org_id = %{org_id_0}) " +
        "AND time >= %{start_date}s " +
        "AND time <= %{end_date}s " +
        "GROUP BY org_id,_card_id,time(1d) ;"
      ),
      params: {
        event: 'card_comment_created',
        start_date: @start_of_yesterday,
        end_date: @end_of_yesterday,
        group_by: 'org_id,_card_id,time(1d)',
        org_id_0: @org.id.to_s
      }
    ).returns([
      {
        "tags" => {
          "_card_id" => @card.id.to_s,
          "org_id" => @org.id.to_s
        },
        "values" => [
          {
            "count" => 1,
            "time" => Time.at(@start_of_yesterday).to_s
          }
        ]
      }
    ])
    # The expected result from the get_num_cards_commented_in_channel
    @num_cards_commented_in_channel = {
      @org.id.to_s => {
        @channel.id.to_s => {
          @start_of_yesterday => {
            "num_cards_commented_in_channel" => 1
          }
        }
      }
    }

    # Stub the influx response for get_num_cards_completed_in_channel
    INFLUXDB_CLIENT.stubs(:query).with(
      (
        "select COUNT(card_id) from \"cards\" where event = %{event} " +
        "AND (org_id = %{org_id_0}) " +
        "AND time >= %{start_date}s " +
        "AND time <= %{end_date}s " +
        "GROUP BY org_id,_card_id,time(1d) ;"
      ),
      params: {
        event: 'card_marked_as_complete',
        start_date: @start_of_yesterday,
        end_date: @end_of_yesterday,
        group_by: 'org_id,_card_id,time(1d)',
        org_id_0: @org.id.to_s
      }
    ).returns([
      {
        "tags" => {
          "_card_id" => @card.id.to_s,
          "org_id" => @org.id.to_s
        },
        "values" => [{
          "count" => 1,
          "time" => Time.at(@start_of_yesterday).to_s
        }]
      }
    ])
    # The expected result from the get_num_cards_completed_in_channel
    @num_cards_completed_in_channel = {
      @org.id.to_s => {
        @channel.id.to_s => {
          @start_of_yesterday => {
            "num_cards_completed_in_channel" => 1
          }
        }
      }
    }

    # The result of fetch_data method
    @unmerged_data = {
      num_monthly_active_users:       @num_monthly_active_users,
      num_channel_visits:             @num_channel_visits,
      num_channel_follows:            @num_channel_follows,
      num_cards_added_to_channel:     @num_cards_added_to_channel,
      num_cards_removed_from_channel: @num_cards_removed_from_channel,
      num_cards_liked_in_channel:     @num_cards_liked_in_channel,
      num_cards_commented_in_channel: @num_cards_commented_in_channel,
      num_cards_completed_in_channel: @num_cards_completed_in_channel,
    }

    # result of passing this through merge_data
    @merged_data = {
      @org.id.to_s => {
        @channel.id.to_s => {
          @start_of_yesterday => {
            "num_monthly_active_users_total"            => 1,
            "num_monthly_active_users_in_web_platform"  => 1,
            "num_monthly_active_users_in_null_platform" => 1,
            "num_channel_visits"                        => 1,
            "num_channel_follows"                       => 1,
            "num_cards_added_to_channel"                => 1,
            "num_cards_removed_from_channel"            => 1,
            "num_cards_liked_in_channel"                => 1,
            "num_cards_commented_in_channel"            => 1,
            "num_cards_completed_in_channel"            => 1,
          }
        }
      }
    }

    # result of passing this through build_insertion_records
    @insertion_records = [
      {
        timestamp: @start_of_yesterday,
        tags: {
          org_id: @org.id.to_s,
          channel_id: @channel.id.to_s
        },
        values: {
          "num_monthly_active_users_total"            => 1,
          "num_monthly_active_users_in_web_platform"  => 1,
          "num_monthly_active_users_in_null_platform" => 1,
          "num_channel_visits"                        => 1,
          "num_channel_follows"                       => 1,
          "num_cards_added_to_channel"                => 1,
          "num_cards_removed_from_channel"            => 1,
          "num_cards_liked_in_channel"                => 1,
          "num_cards_commented_in_channel"            => 1,
          "num_cards_completed_in_channel"            => 1,
        }
      }
    ]
  end

  test 'merge_data' do
    actual = @klass.merge_data @unmerged_data
    expected = @merged_data
    assert_equal expected, actual
  end

  test 'build_insertion_records' do
    actual = @klass.build_insertion_records(@merged_data)
    expected = @insertion_records
    assert_equal expected, actual
  end

  test 'MEASUREMENT' do
    assert_equal "channel_aggregation_daily", @klass::MEASUREMENT
  end

  test 'push_records' do
    final_insertion_records = @insertion_records.map do |record|
      record.deep_dup.deep_merge(
        series: @klass::MEASUREMENT,
        timestamp: record[:timestamp].to_s.ljust(19, "0"),
        tags: {
          analytics_version: '2.0.0'
        }
      ).merge(values: record[:values].symbolize_keys)
    end
    InfluxdbRecorder.unstub(:record)
    INFLUXDB_CLIENT.unstub(:write_points)
    INFLUXDB_CLIENT.expects(:write_points).with final_insertion_records
    @klass.push_records @insertion_records
  end

  test 'channel_followers_by_org' do
    actual = @klass.send(:channel_followers_by_org, [@org])
    assert_equal @channel_followers_by_org, actual
  end

  test 'cards_by_channel_and_org' do
    actual = @klass.send(:cards_by_channel_and_org, [@org])
    assert_equal @cards_by_channel_and_org, actual
  end

  test 'get_monthly_active_users' do
    actual = @klass.get_num_monthly_active_users(
      users_channels_map: @channel_followers_by_org,
       orgs: [@org]
    )
    assert_equal @num_monthly_active_users, actual
  end

  test 'get_num_channel_visits' do
    actual = @klass.get_num_channel_visits orgs: [@org]
    assert_equal @num_channel_visits, actual
  end

  test 'get_num_channel_follows' do
    actual = @klass.get_num_channel_follows orgs: [@org]
    assert_equal @num_channel_follows, actual
  end

  test 'get_num_cards_added_to_channel' do
    actual = @klass.get_num_cards_added_to_channel orgs: [@org]
    assert_equal @num_cards_added_to_channel, actual
  end

  test 'get_num_cards_removed_from_channel' do
    actual = @klass.get_num_cards_removed_from_channel orgs: [@org]
    assert_equal @num_cards_removed_from_channel, actual
  end

  test 'get_num_cards_liked_in_channel' do
    actual = @klass.get_num_cards_liked_in_channel(
      cards_channels_map: @cards_by_channel_and_org,
      orgs: [@org]
    )
    assert_equal @num_cards_liked_in_channel, actual
  end

  test 'get_num_cards_commented_in_channel' do
    actual = @klass.get_num_cards_commented_in_channel(
      cards_channels_map: @cards_by_channel_and_org,
      orgs: [@org]
    )
    assert_equal @num_cards_commented_in_channel, actual
  end

  test 'get_num_cards_completed_in_channel' do
    actual = @klass.get_num_cards_completed_in_channel(
      cards_channels_map: @cards_by_channel_and_org,
      orgs: [@org]
    )
    assert_equal @num_cards_completed_in_channel, actual
  end

  test 'remove_empties' do
    input = {
      a: {},
      b: { c: {} },
      d: { e: 1, f: {} }
    }
    expected = {
      d: { e: 1 }
    }
    actual = @klass.send :remove_empties, input
    assert_equal expected, actual
  end

  test 'fetch_data' do
    actual = @klass.fetch_data [@org]
    expected = @unmerged_data
    assert_equal expected, actual
  end

  test 'run' do
    @klass.expects(:push_records).with @insertion_records
    @klass.run
  end

end

# frozen_string_literal: true

require 'test_helper'

class UserScoreContinuousQueriesTest < ActiveSupport::TestCase

  setup do

    skip # SKIP THIS ENTIRE FILE UNLESS TESTING LOCALLY!

    WebMock.disable!

    @class = Analytics::UserScoreContinuousQueries

    @owner_id = "123"
    @actor_id = "456"

    InfluxdbRecorder.unstub(:record)
    INFLUXDB_CLIENT.unstub :write_point

    @timestamps = 1.upto(3).reduce({}) do |memo, i|
      memo[i] = i.days.ago.utc.change(
        sec: 0, usec: 0, min: 0, hour: 0
      ).to_i
      memo
    end

    INFLUXDB_CLIENT.query "DROP MEASUREMENT user_scores"
    INFLUXDB_CLIENT.query "DROP MEASUREMENT user_scores_daily"
    INFLUXDB_CLIENT.query "DROP MEASUREMENT user_scores_by_second"

    # push some sample data (when proc is called)
    @seed_data = -> {
      scores = {
        owner: [@owner_id, 5],
        actor: [@actor_id, 10]
      }
      events = %w{
        card_comment_created
        card_marked_as_complete
        card_viewed
        card_created
        card_liked
      }
      #                                               # 30 total events
      #                                               # ------------------------
      @timestamps.values.each do |timestamp|          # 3 iterations (days)
        events.each do |event|                        # 5 iterations (events)
          scores.each do |role, (user_id, score)|     # 2 iterations (users)
            InfluxdbRecorder.record('user_scores', {
              tags: {
                role: role.to_s,
                owner_id: @owner_id,
                user_id: user_id,
                actor_id: @actor_id,
                event: event.to_s
              },
              values: {
                score_value: score
              },
              timestamp: timestamp
            })
          end
        end
      end
    }

  end

  test 'populates user_scores_daily measurement' do

    measurement = "user_scores_daily"

    # NOTE - these are the default values, but since a different test
    # sets them to custom values, they must be manually set to originals.
    {
      MEASUREMENT: measurement,
      TIME_LENGTH: "1d",
      RESAMPLE_EVERY: "1d",
      RESAMPLE_FOR: "2d"
    }.each &@class.method(:const_set)

    @seed_data.call
    query = @class.build_backfill_query
    INFLUXDB_CLIENT.query query
    record_groups = INFLUXDB_CLIENT
    .query("SELECT * FROM #{measurement}")
    .shift["values"]
    .group_by { |val| val["user_id"] }
    .transform_values { |vals| vals.sort_by { |val| val["time"] }.reverse }

    # This is the score per day
    scores = { @owner_id => 25, @actor_id => 50 }

    # Ensure there are records for each user.
    assert_equal record_groups.keys.sort, [@owner_id, @actor_id].sort

    record_groups.each do |user_id, records|
      @timestamps.each do |i, timestamp|
        assert_equal(
          Time.parse(records[i-1]["time"]).to_i,
          timestamp
        )
      end
      records.each do |record|
        total_smartbite_score = scores[record["user_id"]]
        # TODO: add another event type to distinguish user & smartbite scores
        assert_equal record["total_user_score"], total_smartbite_score
        if record["user_id"] == @actor_id
          assert_equal record["time_spent_minutes"], 1
        else
          assert_equal record["time_spent_minutes"], nil
        end
        # The rest of the keys are not set for the owner role.
        # They will be nil in the results.
        next unless record["user_id"] == @actor_id
        assert_equal record["total_smartbite_score"], total_smartbite_score
        other_keys = @class
          .evaluate_subqueries
          .except("total_user_score", "total_smartbite_score")
          .keys
        other_keys.each { |key| assert_equal record[key], 1 }
      end
    end

    WebMock.enable!
  end

  test 'continuous query works' do

    # =========================================
    # This is a slow integration test.
    # Skip it unless working on this feature.
    #
    # Also, make sure to manually delete the continuous query
    # if this test is force-exited.
    # =========================================

    measurement = "user_scores_by_second"

    # =========================================
    # Configure the continuous query
    # =========================================

    {
      MEASUREMENT: measurement,
      TIME_LENGTH: "1s",
      RESAMPLE_EVERY: "1s",
      RESAMPLE_FOR: "2s"
    }.each &@class.method(:const_set)

    # =========================================
    # Drop existing data
    # =========================================

    puts "dropping existing measurement / CQ"
    @class.drop_measurement
    @class.drop_continuous_query

    # =========================================
    # Insert initial data which will be ignored by the CQ
    # =========================================
    @seed_data.call

    # =========================================
    # Run CQ
    # =========================================

    puts "running CQ"
    @class.run_continuous_query

    # =========================================
    # Insert data into user_scores each second, keeping note of start time
    # =========================================

    start_time = Time.now.to_i
    10.times do |i|
      sleep 1
      puts "inserting data #{i+1}/10"
      @timestamps = {0 => Time.now.to_i}
      @seed_data.call
    end
    puts "done inserting data"

    # =========================================
    # Wait for CQ to create records for each of the inserted user_scores
    # =========================================

    puts "waiting for CQ to catch up"
    records = Timeout.timeout(20) do
      loop do
        records = INFLUXDB_CLIENT.query("SELECT * FROM #{measurement}").shift["values"]
        puts "aggregation records found: #{records.length}"
        break records if records.length >= 20
        sleep 0.25
      end
    end
    end_time = Time.now.to_i
    puts "Done. Took #{end_time - start_time} seconds for CQ to process 10s worth of data"

    # =========================================
    # Validate the aggregation data
    # =========================================

    # This is the score per second
    scores = { @owner_id => 25, @actor_id => 50 }
    record_groups = records.group_by { |record| record["user_id"] }
    assert_equal record_groups.keys.sort, [@owner_id, @actor_id].sort
    record_groups.values.each(&:shift)
    record_groups.each do |user_id, records|
      records.each do |record|
        total_smartbite_score = scores[record["user_id"]]
        # TODO: add another event type to distinguish user & smartbite scores
        assert_equal record["total_user_score"], total_smartbite_score
        # The rest of the keys are not set for the owner role.
        # They will be nil in the results.
        next unless record["user_id"] == @actor_id
        assert_equal record["total_smartbite_score"], total_smartbite_score
        other_keys = @class
          .evaluate_subqueries
          .except("total_user_score", "time_spent_minutes", "total_smartbite_score")
          .keys
        other_keys.each { |key| assert_equal record[key], 1 }
      end
    end

    # =========================================
    # Cleanup
    # =========================================

    @class.drop_continuous_query

  end

  test 'calculates time_spent_minutes when user has more than 1 activity slot per day' do
    measurement = "user_scores_daily"
    # NOTE - these are the default values, but since a different test
    # sets them to custom values, they must be manually set to originals.
    {
      MEASUREMENT: measurement,
      TIME_LENGTH: "1d",
      RESAMPLE_EVERY: "1d",
      RESAMPLE_FOR: "2d"
    }.each &@class.method(:const_set)
    @seed_data.call
    # Seed another sample event, at a different time
    InfluxdbRecorder.record('user_scores', {
      tags: {
        role: "actor",
        owner_id: @owner_id,
        user_id: @actor_id,
        actor_id: @actor_id,
        event: "fake_event"
      },
      values: {
        score_value: 1
      },
      timestamp: (Time.at(@timestamps.values.first) + 5.minutes).to_i
    })
    INFLUXDB_CLIENT.query @class.build_backfill_query

    record_groups = INFLUXDB_CLIENT
      .query("SELECT * FROM #{measurement}")
      .shift["values"]
      .group_by { |val| val["user_id"] }
      .transform_values { |vals| vals.sort_by { |val| val["time"] }.reverse }

    record_groups.each do |user_id, records|
      # Note that only actor and actor_and_owner roles get time_spent credit for an event.
      time_spent_minutes_list = records.map { |record| record["time_spent_minutes"] }
      if user_id == @actor_id
        assert_equal [2,1,1], time_spent_minutes_list
      else
        assert_equal [nil,nil,nil], time_spent_minutes_list
      end
    end
  end

end

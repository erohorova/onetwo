require 'test_helper'

class Analytics::SearchRecorderTest < ActionController::TestCase

  setup do
    @search_recorder = Analytics::SearchRecorder
    @measurement = @search_recorder::MEASUREMENT
    @stub_time = Time.now.to_i
    @org = create :organization
    @actor = create :user, organization: @org
    @stub_version_number = "fake version number"
    @stub_user_agent = "fake user agent"
    @stub_search_query = "foo"
    @stub_platform = "fake platform"
  end

  test 'has the correct measurement name' do
    assert_equal @measurement, "searches"
  end

  test 'should record search information' do
    expected_data = {
      tags: {
        _user_id: @actor.id.to_s,
        org_id: @org.id.to_s,
        event: "search",
        is_system_generated: '0',
      },
      values: {
        platform: @stub_platform,
        org_hostname: @org.host_name,
        search_query: @stub_search_query,
        user_id: @actor.id.to_s,
        platform_version_number: @stub_version_number,
        results_count: 3,
        is_admin_request: '0',
        ###NOTE: PII
        user_full_name: @actor.try(:full_name),
        ###
        user_agent: @stub_user_agent,
      },
      timestamp: @stub_time
    }
    @search_recorder.expects(:influxdb_record).with(@measurement, expected_data)
    @search_recorder.record(
      actor: Analytics::MetricsRecorder.user_attributes(@actor),
      search_query: @stub_search_query,
      results_count: 3,
      timestamp: @stub_time,
      org: Analytics::MetricsRecorder.org_attributes(@org),
      event: "search",
      additional_data: {
        platform: @stub_platform,
        platform_version_number: @stub_version_number,
        user_agent: @stub_user_agent,
        is_admin_request: 0
      }
    )
  end

  test 'should record results_count as int always' do
    expected_data = {
      tags: {
        _user_id: @actor.id.to_s,
        org_id: @org.id.to_s,
        event: "search",
        is_system_generated: '0',
      },
      values: {
        platform: @stub_platform,
        org_hostname: @org.host_name,
        search_query: @stub_search_query,
        user_id: @actor.id.to_s,
        platform_version_number: @stub_version_number,
        results_count: 3,
        is_admin_request: '0',
        ###NOTE: PII
        user_full_name: @actor.try(:full_name),
        ###
        user_agent: @stub_user_agent,
      },
      timestamp: @stub_time
    }
    @search_recorder.expects(:influxdb_record).with(@measurement, expected_data)
    @search_recorder.record(
      actor: Analytics::MetricsRecorder.user_attributes(@actor),
      search_query: @stub_search_query,
      results_count: 3.0,
      timestamp: @stub_time,
      org: Analytics::MetricsRecorder.org_attributes(@org),
      event: "search",
      additional_data: {
        platform: @stub_platform,
        platform_version_number: @stub_version_number,
        user_agent: @stub_user_agent,
        is_admin_request: 0
      }
    )
  end

  test "sanitizes search query" do
    skip # SKIP UNLESS TESTING LOCALLY
    WebMock.disable!
    complicated_search_queries = [
      "Wow\t\n' .,`~1<>!@#$%^&*()[]{}?/\\|",
      "asdasd\\",
      "\r", "\t", "\n", "\\r", "\\n", "\\t",
      "\\ \\n \\t \\r \n \t \r asdasd",
      "∆ƒˆ´®˚¬´˚ç´˚ƒ´˙˙´®¬∂˚¬∂"
    ]
    complicated_search_queries.each do |query|
      # INFLUXDB_CLIENT handles sanitization.
      # To test this, first unstub the recorder and push the data:
      InfluxdbRecorder.unstub(:record)
      INFLUXDB_CLIENT.unstub(:write_point)
      @search_recorder.record(
        actor: Analytics::MetricsRecorder.user_attributes(@actor),
        org: Analytics::MetricsRecorder.org_attributes(@org),
        search_query: query,
        timestamp: @stub_time,
        results_count: 3,
        event: "search",
        additional_data: {
          platform: @stub_platform,
          platform_version_number: @stub_version_number,
          user_agent: @stub_user_agent,
          is_admin_request: 0
        }
      )
      # Then, query for the data and make sure it's preserved:
      result = InfluxQuery.fetch(
        *InfluxQuery.prepare_query(@org.id, {}, @measurement)
      )
      assert_equal(
        result.last["values"].last["search_query"],
        query
      )
    end
    WebMock.enable!
  end

end

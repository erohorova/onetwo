require 'test_helper'

class InfluxdbClientTest < ActiveSupport::TestCase

  setup do
    @klass = INFLUXDB_CLIENT
    @invalid_points = [
      { timestamp: 12345 },
      { timestamp: "12345" },
      { "timestamp" => 12345 },
      { }
    ]
    @valid_points = [
      {
        timestamp: InfluxdbRecorder.to_ns("foo", Time.now.to_i),
        values: { v1: "v1" },
        tags: { t1: "t1" },
        series: "things"
      },
      {
        "timestamp" => InfluxdbRecorder.to_ns("foo", Time.now.to_i + 5),
        "values" => { "v1" => "v1" },
        "tags" => { "t1" => "t1" },
        "series" => "things"
      }
    ]
    %i{write_point write_points}.each do |fn|
      @klass.unstub fn
      @klass.class::CLIENT.stubs fn
    end
    @queue_name = Settings.redis["influx_recording_input_queue"]
  end

  test 'stores the real client as a private const' do
    assert_equal InfluxDB::Client, @klass.class::CLIENT.class
  end

  test 'write_points_to_queue enqueues to redis' do
    point = {
      timestamp: "1539031586391170000",
      tags: { t1: "t1" },
      values: { v1: "v1" },
      series: "things"
    }
    enqueued_point = point.merge(
      timestamp: "2018-10-08T20:46:26.391170000Z",
      name: "things"
    ).except(:series)
    REDIS_CLIENT.expects(:rpush).with(@queue_name, [enqueued_point].to_json)
    @klass.write_points_to_queue [point]
  end

  test 'write_points pushes directly to influx' do
    point = {
      timestamp: "1539031586391170000",
      tags: { t1: "t1" },
      values: { v1: "v1" },
      series: "things"
    }
    @klass.class::CLIENT.expects(:write_points).with([point])
    @klass.write_points [point]
  end

  test 'write_point_to_queue enqueues to redis' do
    point = {
      timestamp: "1539031586391170000",
      tags: { t1: "t1" },
      values: { v1: "v1" }
    }
    enqueued_point = point.merge(
      timestamp: "2018-10-08T20:46:26.391170000Z",
      name: "things"
    )
    REDIS_CLIENT.expects(:rpush).with(@queue_name, [enqueued_point].to_json)
    @klass.write_point_to_queue "things", point
  end

  test 'write_point pushes directly to influx' do
    point = {
      timestamp: "1539031586391170000",
      tags: { t1: "t1" },
      values: { v1: "v1" }
    }
    @klass.class::CLIENT.expects("write_point").with("things", point)
    @klass.write_point "things", point
  end

  test "write_points raises an error if invalid timestamp is passed" do
    err_msg = "must provide timestamps with nanosecond precision"
    @invalid_points.each do |point|
      error = assert_raises(ArgumentError) do
        @klass.write_points [point]
      end
      assert_equal err_msg, error.message
    end
    # Valid inputs raise no error
    @valid_points.each do |point|
      @klass.write_points [point]
    end
  end

  test "write_points_to_queue raises an error if invalid timestamp is passed" do
    err_msg = "must provide timestamps with nanosecond precision"
    @invalid_points.each do |point|
      error = assert_raises(ArgumentError) do
        @klass.write_points [point]
      end
      assert_equal err_msg, error.message
    end
    # Valid inputs raise no error
    @valid_points.each do |point|
      @klass.write_points_to_queue [point]
    end
  end

  test "write_point raises an error if invalid timestamp is passed" do
    err_msg = "must provide timestamps with nanosecond precision"
    @invalid_points.each do |point|
      error = assert_raises(ArgumentError) do
        @klass.write_point "things", point
      end
      assert_equal err_msg, error.message
    end
    # Valid inputs raise no error
    @valid_points.each do |point|
      @klass.write_point "things", point.except("series", :series)
    end
  end

  test "write_point_to_queue raises an error if invalid timestamp is passed" do
    err_msg = "must provide timestamps with nanosecond precision"
    @invalid_points.each do |point|
      error = assert_raises(ArgumentError) do
        @klass.write_point "things", point
      end
      assert_equal err_msg, error.message
    end
    # Valid inputs raise no error
    @valid_points.each do |point|
      @klass.write_point_to_queue "things", point.except("series", :series)
    end
  end

  test "#query combines records with the same measurement and tag set" do
    query = "show tag keys from /.+/"
    # The first 2 top-level objects have the same measurement & tag set
    stub_result = [
      {
        "name" => "cards",
        "tags" => nil,
        "values" => [{ "tagKey" => "_card_id" }]
      },
      {
        "name" => "cards",
        "tags" => nil,
        "values" => [{ "tagKey" => "_user_id" }]
      },
      {
        "name" => "cards",
        "tags" => {foo: "bar"},
        "values" => [{ "tagKey" => "_foo_id" }]
      }
    ]
    @klass.class::CLIENT
      .expects(:query)
      .with(query, params: {})
      .returns(stub_result)
    # They are combined into a single object
    expected = [
      {
        "name"=>"cards",
        "tags"=>nil,
        "values"=>[{"tagKey"=>"_card_id"},{"tagKey"=>"_user_id"}]
      },
      {
        "name" => "cards",
        "tags" => {foo: "bar"},
        "values" => [{ "tagKey" => "_foo_id" }]
      }
    ]
    actual = @klass.query query
    assert_equal expected, actual
  end

end

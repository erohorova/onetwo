require 'test_helper'
class Analytics::GroupCardMetricsRecorderTest < ActiveSupport::TestCase
  recorder = Analytics::GroupCardMetricsRecorder

  test 'inherits from GroupMetricsRecorder' do
    assert recorder < Analytics::GroupMetricsRecorder
  end

  test 'should record group event' do
    org = create(:organization, name: 'LXP', host_name: 'spark' )
    actor = create :user, organization: org
    team = create(:team, organization: org)
    card = create(:card, author: nil, organization: org)

    InfluxdbRecorder.unstub(:record)
    INFLUXDB_CLIENT.stubs(:write_point)

    timestamp = Time.now.to_i

    field_set = {
      group_id: team.id.to_s,
      group_name: team.name,
      card_id: card.id.to_s
    }

    field_set[:user_id] = actor.id.to_s

    field_set[:org_name] = card.organization.name
    field_set[:user_agent] = 'Edcast iPhone'
    field_set[:is_admin_request] = '0'
    field_set[:org_hostname] = card.organization.host_name
    field_set[:user_full_name] = actor.try(:full_name)
    field_set[:user_handle] = actor.handle
    field_set[:platform] = 'web'

    tag_set = {_user_id: actor.id.to_s, _group_id: team.id.to_s}
    tag_set[:is_system_generated] = '0'
    tag_set[:org_id] = card.organization_id.to_s
    tag_set[:event] = 'group_card_assigned'

    InfluxdbRecorder.expects(:record).with('groups',
      {
        values: field_set.compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true

    Analytics::GroupCardMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      group: Analytics::MetricsRecorder.team_attributes(team),
      card: Analytics::MetricsRecorder.team_attributes(card),
      event: 'group_card_assigned',
      timestamp: timestamp,
      additional_data: {
        platform: 'web',
        user_agent: 'Edcast iPhone',
        is_admin_request: 0
      }
    )
  end

  test 'should have group edited event registered' do
    assert_includes recorder::GROUP_CARD_EVENTS, 'group_card_assigned'
    assert_equal(
      recorder,
      Analytics::MetricsRecorder.get_recorder_for_event('group_card_assigned')
    )
  end

end

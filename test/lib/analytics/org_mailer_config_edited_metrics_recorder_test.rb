# frozen_string_literal: true
require 'test_helper'
class OrgMailerConfigEditedMetricsRecorderTest < ActiveSupport::TestCase

  setup do
    @klass = Analytics::OrgMailerConfigEditedMetricsRecorder
    @timestamp = Time.now.to_i
    @org = create :organization
    @actor = create :user, organization: @org
    @mailer_config = create :mailer_config, {
      from_address: "foo@bar.com",
      from_name: "foo bar",
      is_active: false,
      verification: {}
    }
    InfluxdbRecorder.unstub :record

    @expected_event_template = {
      timestamp: @timestamp,
      tags: {
        event: "org_email_sender_edited",
        org_id: @org.id.to_s,
        is_system_generated: '1',
        _user_id: @actor.id.to_s,
      },
      values: {
        user_id: @actor.id.to_s,
        org_host_name: @org.host_name,
        org_name: @org.name,
        org_slug: @org.slug,
        org_is_open: @org.is_open ? '1' : '0',
        org_is_registrable: @org.is_registrable ? '1' : '0',
        org_status: @org.status,
        org_send_weekly_activity: @org.send_weekly_activity ? '1' : '0',
        org_show_sub_brand_on_login: @org.show_sub_brand_on_login ? '1' : '0',
        org_savannah_app_id: @org.savannah_app_id.to_s,
        org_social_enabled: @org.social_enabled ? '1' : '0',
        org_xapi_enabled: @org.xapi_enabled ? '1' : '0',
        org_show_onboarding: @org.show_onboarding ? '1' : '0',
        org_enable_role_based_authorization: @org.enable_role_based_authorization ? '1' : '0',

        email_sender_from_address: @mailer_config.from_address,
        email_sender_from_name: @mailer_config.from_name,
        mailer_config_id: @mailer_config.id.to_s,
        is_sender_verified: '0',
        is_current_sender: '0'
      }
    }
    @recorder_args = {
      org: Analytics::MetricsRecorder.org_attributes(@org),
      actor: Analytics::MetricsRecorder.user_attributes(@actor),
      mailer_config: Analytics::MetricsRecorder.mailer_config_attributes(@mailer_config),
      event: "org_email_sender_edited",
      timestamp: @timestamp,
      additional_data: { user_id: @actor.id }
    }
  end

  test 'records event when from_address changes' do
    InfluxdbRecorder.expects(:record).with(
      'organizations',
      @expected_event_template.deep_merge(
        values: {
          changed_column: "email_sender_from_address",
          old_val: "foo@bar.com",
          new_val: "foo2@bar.com",
        }
      ),
      { write_client: nil, duplicate_check: true }
    )
    @klass.record(@recorder_args.merge(
      changed_column: "from_address",
      old_val: "foo@bar.com",
      new_val: "foo2@bar.com"
    ))
  end

  test 'records event when from_name changes' do
    InfluxdbRecorder.expects(:record).with(
      'organizations',
      @expected_event_template.deep_merge(
        values: {
          changed_column: "email_sender_from_name",
          old_val: "foo bar",
          new_val: "foo bar 2",
        }
      ),
      { write_client: nil, duplicate_check: true }
    )
    @klass.record(@recorder_args.merge(
      changed_column: "from_name",
      old_val: "foo bar",
      new_val: "foo bar 2"
    ))
  end

  test 'records event when is_active changes' do
    InfluxdbRecorder.expects(:record).with(
      'organizations',
      @expected_event_template.deep_merge(
        values: {
          changed_column: "is_current_sender",
          old_val: "0",
          new_val: "1",
        }
      ),
      { write_client: nil, duplicate_check: true }
    )
    @klass.record(@recorder_args.merge(
      changed_column: "is_active",
      old_val: "false",
      new_val: "true"
    ))
  end

  test 'records event when verification changes' do
    InfluxdbRecorder.expects(:record).with(
      'organizations',
      @expected_event_template.deep_merge(
        values: {
          changed_column: "is_sender_verified",
          old_val: "0",
          new_val: "1",
        }
      ),
      { write_client: nil, duplicate_check: true }
    )
    @klass.record(@recorder_args.merge(
      changed_column: "verification",
      old_val: { },
      new_val: { domain: true }
    ))
  end


end

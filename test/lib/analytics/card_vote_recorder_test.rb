require 'test_helper'
class CardVoteRecorderTest < ActiveSupport::TestCase

  setup do
    author_first_name = Faker::Name.first_name
    actor_first_name = Faker::Name.first_name

    author_last_name = Faker::Name.last_name
    actor_last_name = Faker::Name.last_name

    @org = create :organization,
      name: 'LXP',
      host_name: 'spark'

    @author = create :user,
      first_name: author_first_name,
      last_name: author_last_name,
      organization: @org

    @actor = create :user,
      first_name: actor_first_name,
      last_name: actor_last_name,
      organization: @org

    @card = create :card,
      author: @author,
      created_at: 2.days.ago

    @vote = create :vote,
      votable: @card,
      voter: @actor
  end

  test 'valid registered events' do
    registered_events = ['card_liked', 'card_unliked']
    assert_equal(
      registered_events,
      Analytics::CardVoteRecorder::CARD_LIKE_EVENTS
    )
  end

  test 'valid record args' do
    invalid_event = 'invalid event'
    InfluxdbRecorder.stubs(:record).returns :true

    assert_nothing_raised do
      Analytics::CardVoteRecorder.record(
        org: Analytics::MetricsRecorder.org_attributes(@vote.votable.organization),
        card: Analytics::MetricsRecorder.card_attributes(@vote.votable),
        actor: Analytics::MetricsRecorder.user_attributes(@vote.voter),
        timestamp: Time.now.to_i,
        event: 'card_liked',
        additional_data: {}
      )
      Analytics::CardVoteRecorder.record(
        org: Analytics::MetricsRecorder.org_attributes(@vote.votable.organization),
        card: Analytics::MetricsRecorder.card_attributes(@vote.votable),
        actor: Analytics::MetricsRecorder.user_attributes(@vote.voter),
        timestamp: Time.now.to_i,
        event: 'card_unliked',
        additional_data: {}
      )
    end

    assert_raises do
      Analytics::CardVoteRecorder.record(
        org: Analytics::MetricsRecorder.org_attributes(@vote.votable.organization),
        card: Analytics::MetricsRecorder.card_attributes(@vote.votable),
        actor: Analytics::MetricsRecorder.user_attributes(@vote.voter),
        timestamp: Time.now.to_i,
        event: invalid_event,
        additional_data: {}
      )
    end
  end

  test 'should record card vote event' do
    InfluxdbRecorder.unstub :record
    timestamp = Time.now.to_i
    card_like = Analytics::CardVoteRecorder::EVENT_CARD_LIKED

    field_set = card_field_set(
      @org, @card, @actor
    )

    tag_set = card_tag_set(
      @card, @actor, card_like
    )

    InfluxdbRecorder.expects(:record).with('cards',
      {
        values: field_set.symbolize_keys.merge(
          {
            platform: 'android'
          }
        ).compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true

    Analytics::CardVoteRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(@vote.votable.organization),
      card: Analytics::MetricsRecorder.card_attributes(@vote.votable),
      actor: Analytics::MetricsRecorder.user_attributes(@vote.voter),
      timestamp: timestamp,
      event: card_like,
      additional_data: {
        platform: 'android',
        user_agent: 'Edcast iPhone',
        is_admin_request: 0
      }
    )
  end
end
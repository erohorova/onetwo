# frozen_string_literal: true
require 'test_helper'
class OrgEditedMetricsRecorderTest < ActiveSupport::TestCase

  test 'calls OrgMetricsRecorder' do
    timestamp = Time.now.to_i
    org = create :organization
    actor = create :user, organization: org
    metadata = {
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0,
      user_id: actor.id
    }
    Analytics::OrgMetricsRecorder.expects(:record).with(
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      event: "org_edited",
      taxonomy: nil,
      timestamp: timestamp,
      announcement: nil,
      sources_credential: nil,
      email_template: nil,
      embargo: nil,
      partner_center: nil,
      mailer_config: nil,
      additional_data: metadata,
    )
    Analytics::OrgEditedMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      event: "org_edited",
      timestamp: timestamp,
      additional_data: metadata,
      changed_column: "name",
      old_val: "old_val",
      new_val: 1 # Will get turned into string
    )
  end

  test 'records to influx' do
    InfluxdbRecorder.unstub :record
    timestamp = Time.now.to_i
    org = create :organization
    config = create :config,
      configable_type: "Organization",
      configable_id: org.id,
      name: "config_name"
    client = create(:client, organization: org)
    actor = create :user, organization: org
    metadata = {
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0,
      user_id: actor.id
    }
    InfluxdbRecorder.expects(:record).with( 'organizations',
      {
        timestamp: timestamp,
        tags: {
          event: "org_edited",
          org_id: org.id.to_s,
          is_system_generated: '1',
          _user_id: actor.id.to_s,
        },
        values: {
          user_id: actor.id.to_s,
          org_host_name: org.host_name,
          is_admin_request: '0',
          changed_column: "name",
          old_val: "old_val",
          new_val: "1",
          org_name: org.name,
          org_slug: org.slug,
          org_client_id: org.client_id.to_s,
          org_is_open: org.is_open ? '1' : '0',
          org_is_registrable: org.is_registrable ? '1' : '0',
          org_status: org.status,
          org_send_weekly_activity: org.send_weekly_activity ? '1' : '0',
          org_show_sub_brand_on_login: org.show_sub_brand_on_login ? '1' : '0',
          org_savannah_app_id: org.savannah_app_id.to_s,
          org_social_enabled: org.social_enabled ? '1' : '0',
          org_xapi_enabled: org.xapi_enabled ? '1' : '0',
          org_show_onboarding: org.show_onboarding ? '1' : '0',
          org_enable_role_based_authorization: org.enable_role_based_authorization ? '1' : '0',
          platform_version_number: 'platform_version_number',
          user_agent: 'user_agent',
          config_name: 'config_name',
          config_id: config.id.to_s,
        }
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    Analytics::OrgEditedMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      config: Analytics::MetricsRecorder.config_attributes(config),
      event: "org_edited",
      timestamp: timestamp,
      additional_data: metadata,
      changed_column: "name",
      old_val: "old_val",
      new_val: 1 # Will get turned into string
    )
  end

  test 'includes taxonomy edited attributes' do
    InfluxdbRecorder.unstub :record
    timestamp = Time.now.to_i
    org = create :organization
    client = create(:client, organization: org)
    actor = create :user, organization: org
    topic_request = create(
      :topic_request, organization_id: org.id, label: "test", 
      requestor_id: client.id, approver_id: client.id, publisher_id: client.id
    )

    %w{org_taxonomy_approved org_taxonomy_rejected org_taxonomy_published}.each do |event|
    metadata = {
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0,
      user_id: actor.id
    }
    InfluxdbRecorder.expects(:record).with( 'organizations',
      {
        tags: {
          org_id: org.id.to_s,
          event: event,
          _user_id: actor.id.to_s,
          is_system_generated: '1'
        },
        values: {
          org_host_name: org.host_name,
          org_name: org.name,
          org_slug: org.slug,
          org_client_id: org.client_id.to_s,
          org_is_open: org.is_open ? '1' : '0',
          org_is_registrable: org.is_registrable ? '1' : '0',
          org_status: org.status,
          org_send_weekly_activity: org.send_weekly_activity ? '1' : '0',
          org_show_sub_brand_on_login: org.show_sub_brand_on_login ? '1' : '0',
          org_savannah_app_id: org.savannah_app_id.to_s,
          org_social_enabled: org.social_enabled ? '1' : '0',
          org_xapi_enabled: org.xapi_enabled ? '1' : '0',
          org_show_onboarding: org.show_onboarding ? '1' : '0',
          org_enable_role_based_authorization: org.enable_role_based_authorization ? '1' : '0',
          user_id: actor.id.to_s,
          changed_column: "state",
          old_val: "approved",
          new_val: "published",
          platform_version_number: 'platform_version_number',
          user_agent: 'user_agent',
          is_admin_request: '0'
        },
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    Analytics::OrgEditedMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      event: event,
      timestamp: timestamp,
      changed_column: "state",
      old_val: "approved",
      new_val: "published",
      additional_data: metadata
    )
    end
   end

  test 'includes embargo edited attributes' do
    InfluxdbRecorder.unstub :record
    timestamp = Time.now.to_i
    org = create :organization
    client = create(:client, organization: org)
    actor = create :user, organization: org
    embargo = Embargo.create!(
        organization_id: org.id, category: "approved_sites", 
        value: "https://approved.sites", user: actor
      )

    %w{org_embargo_edited}.each do |event|
    metadata = {
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0,
      user_id: actor.id
     }
     InfluxdbRecorder.expects(:record).with( 'organizations',
     {
        tags: {
          org_id: org.id.to_s,
          event: event,
          _user_id: actor.id.to_s,
          is_system_generated: '1'
        },
        values: {
          org_host_name: org.host_name,
          org_name: org.name,
          org_slug: org.slug,
          org_client_id: org.client_id.to_s,
          org_is_open: org.is_open ? '1' : '0',
          org_is_registrable: org.is_registrable ? '1' : '0',
          org_status: org.status,
          org_send_weekly_activity: org.send_weekly_activity ? '1' : '0',
          org_show_sub_brand_on_login: org.show_sub_brand_on_login ? '1' : '0',
          org_savannah_app_id: org.savannah_app_id.to_s,
          org_social_enabled: org.social_enabled ? '1' : '0',
          org_xapi_enabled: org.xapi_enabled ? '1' : '0',
          org_show_onboarding: org.show_onboarding ? '1' : '0',
          org_enable_role_based_authorization: org.enable_role_based_authorization ? '1' : '0',
          user_id: actor.id.to_s,
          changed_column: "value",
          old_val: "old_val",
          new_val: "edited_value",
          platform_version_number: 'platform_version_number',
          user_agent: 'user_agent',
          is_admin_request: '0'
        },
        timestamp: timestamp,
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    Analytics::OrgEditedMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      event: event,
      timestamp: timestamp,
      additional_data: metadata,
      changed_column: "value",
      old_val: "old_val",
      new_val: "edited_value"
    )
    end
   end

  test 'records sources credential edited event' do
    InfluxdbRecorder.unstub :record
    timestamp   = Time.now.to_i
    org         = create :organization
    client      = create(:client, organization: org)
    actor       = create :user, organization: org
    event       = "org_sources_credential_edited"
    
    metadata    = {
      user_agent:               "user_agent",
      platform_version_number:  "platform_version_number",
      is_admin_request:         0,
      user_id:                  actor.id
    }
    auth_api_info = {"auth_required":true, "url":"asdsa", "method":"POST", "host":"asdsa das"}

    sources_credential = SourcesCredential.create(
      id:             10000001 ,
      organization:   org, 
      source_id:      "8b5ab2f1", 
      shared_secret:  "11a8a57",
      auth_api_info:  auth_api_info,
      source_name:    "TEST"
    )

    InfluxdbRecorder.expects(:record).with( 'organizations',
      {
        tags: {
          org_id: org.id.to_s,
          event: event,
          _user_id: actor.id.to_s,
          is_system_generated: '1'
        },
        values: {
          org_host_name: org.host_name,
          org_name: org.name,
          org_slug: org.slug,
          org_client_id: org.client_id.to_s,
          org_is_open: org.is_open ? '1' : '0',
          org_is_registrable: org.is_registrable ? '1' : '0',
          org_status: org.status,
          org_send_weekly_activity: org.send_weekly_activity ? '1' : '0',
          org_show_sub_brand_on_login: org.show_sub_brand_on_login ? '1' : '0',
          org_savannah_app_id: org.savannah_app_id.to_s,
          org_social_enabled: org.social_enabled ? '1' : '0',
          org_xapi_enabled: org.xapi_enabled ? '1' : '0',
          org_show_onboarding: org.show_onboarding ? '1' : '0',
          org_enable_role_based_authorization: org.enable_role_based_authorization ? '1' : '0',
          user_id: actor.id.to_s,
          sources_credential_id: sources_credential.id.to_s,
          sources_credential_source_id: sources_credential.source_id,
          sources_credential_shared_secret: sources_credential.shared_secret,
          sources_credential_auth_api_info: sources_credential.auth_api_info.to_json,
          sources_credential_source_name: sources_credential.source_name,
          platform_version_number: 'platform_version_number',
          user_agent: 'user_agent',
          is_admin_request: '0',
          changed_column: "value",
          old_val: "old_val",
          new_val: "edited_value",
        },
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    Analytics::OrgEditedMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      event: event,
      timestamp: timestamp,
      sources_credential: Analytics::MetricsRecorder.sources_credential_attributes(sources_credential),
      additional_data: metadata,
      changed_column: "value",
      old_val: "old_val",
      new_val: "edited_value"
    )
    end

end

require 'test_helper'
class Analytics::CardModerationMetricsRecorderTest < ActiveSupport::TestCase

  test ".record calls InfluxdbRecorder with expected keys" do
    InfluxdbRecorder.unstub(:record)
    INFLUXDB_CLIENT.stubs(:write_point)
    timestamp = Time.now.to_i
    org = create :organization
    actor = create(:user, organization: org)
    author = create :user, organization: org
    card = create(:card,
      ecl_id: "ecl_id",
      author: author,
      organization: org,
      title: "title",
    )
    channel = create(:channel, organization: org)
    event = 'card_approved_for_channel'
    additional_data = {
      platform: "platform",
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0
    }

    field_set = card_field_set(
      org, card, actor
    )

    tag_set = card_tag_set(
      card, actor, 'card_approved_for_channel'
    )

    InfluxdbRecorder.expects(:record).with('cards',
      {
        values: field_set.symbolize_keys.merge(
          {
            platform: "platform",
            user_agent: "user_agent",
            platform_version_number: "platform_version_number",
            channel_id: channel.id.to_s
          }
        ).compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true

    Analytics::CardModerationMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      card: Analytics::MetricsRecorder.card_attributes(card),
      channel: Analytics::MetricsRecorder.channel_attributes(channel),
      timestamp: timestamp,
      additional_data: additional_data,
      event: "card_approved_for_channel",
    )
  end

end

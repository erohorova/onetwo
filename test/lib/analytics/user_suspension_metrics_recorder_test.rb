require 'test_helper'
class Analytics::UserSuspensionMetricsRecorderTest < ActiveSupport::TestCase

  test "calls InfluxdbRecorder with expected args" do
    org = create(:organization)
    user = create(:user, organization: org)
    fb_ip_provider = create(:identity_provider,
      type: 'FacebookProvider',
      uid: 'SPARK123',
      user: user
    )
    suspended_user = create(:user, organization: org)
    metadata = {
      platform: "platform",
      user_agent: "user_agent",
      platform_version_number: "1.0.0",
      is_admin_request: 0
    }
    InfluxdbRecorder.unstub(:record)
    timestamp = Time.now.to_i
    # both events look the exact same, except for the 'event' string
    %w{user_suspended user_unsuspended}.each do |event|
      InfluxdbRecorder.expects(:record).with(
        'users',
        {
          timestamp: timestamp,
          tags: {
            _user_id: user.id.to_s,
            org_id: org.id.to_s,
            event: 'user_suspended',
            is_system_generated: '0',
          },
          values: {
            platform: 'platform',
            user_org_uid: fb_ip_provider.uid,
            org_hostname: org.host_name,
            is_admin_request: '0',
            onboarding_status: '1',
            sign_in_at: '0',
            user_id: user.id.to_s,
            user_handle: user.handle,
            full_name: user.full_name,
            user_role: 'member',
            platform_version_number: '1.0.0',
            user_agent: 'user_agent',
            suspended_user_id: suspended_user.id.to_s,
          }
        },
        {
          write_client: nil,
          duplicate_check: true
        }
      )
      Analytics::UserSuspensionMetricsRecorder.record(
        org: Analytics::MetricsRecorder.org_attributes(org),
        timestamp: timestamp,
        actor: Analytics::MetricsRecorder.user_attributes(user),
        suspended_user: Analytics::MetricsRecorder.user_attributes(suspended_user),
        event: "user_suspended",
        additional_data: metadata
      )
    end
  end

end

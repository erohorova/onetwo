# frozen_string_literal: true

require 'test_helper'
class OrgCustomFieldRecorderTest < ActiveSupport::TestCase

  test 'calls OrgMetricsRecorder' do
    timestamp = Time.now.to_i
    org = create :organization
    custom_field = CustomField.create(
      organization: org,
      display_name: "foo",
      abbreviation: "f"
    )
    actor = create :user, organization: org
    metadata = {
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0,
      user_id: actor.id
    }
    Analytics::OrgMetricsRecorder.expects(:record).with(
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      event: "org_custom_field_added",
      timestamp: timestamp,
      additional_data: metadata,
    )
    Analytics::OrgCustomFieldRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      event: "org_custom_field_added",
      timestamp: timestamp,
      additional_data: metadata,
      custom_field: Analytics::MetricsRecorder.custom_field_attributes(custom_field)
    )
  end

  test 'records org_custom_field_edited events to influx' do
    %w{display_name enable_people_search}.each do |col|
      InfluxdbRecorder.unstub :record
      timestamp = Time.now.to_i
      org = create :organization
      custom_field = CustomField.create(
        organization: org,
        display_name: "foo",
        abbreviation: "f"
      )
      config = custom_field.configs.create(
        name: "enable_in_filters",
        value: 'true'
      )
      client = create(:client, organization: org)
      actor = create :user, organization: org
      metadata = {
        user_agent: "user_agent",
        platform_version_number: "platform_version_number",
        is_admin_request: 0,
        user_id: actor.id
      }
      InfluxdbRecorder.expects(:record).with( 'organizations',
        {
          timestamp: timestamp,
          tags: {
            event: "org_custom_field_edited",
            org_id: org.id.to_s,
            is_system_generated: '1',
            _user_id: actor.id.to_s
          },
          values: {
            is_admin_request: '0',
            changed_column: col,
            org_host_name: org.host_name,
            old_val: "01",
            new_val: "10",
            org_name: org.name,
            org_slug: org.slug,
            org_client_id: org.client_id.to_s,
            org_is_open: org.is_open ? '1' : '0',
            org_is_registrable: org.is_registrable ? '1' : '0',
            org_status: org.status,
            org_send_weekly_activity: org.send_weekly_activity ? '1' : '0',
            org_show_sub_brand_on_login: org.show_sub_brand_on_login ? '1' : '0',
            org_savannah_app_id: org.savannah_app_id.to_s,
            org_social_enabled: org.social_enabled ? '1' : '0',
            org_xapi_enabled: org.xapi_enabled ? '1' : '0',
            org_show_onboarding: org.show_onboarding ? '1' : '0',
            org_enable_role_based_authorization: org.enable_role_based_authorization ? '1' : '0',
            platform_version_number: 'platform_version_number',
            user_agent: 'user_agent',
            custom_field_display_name: custom_field.display_name,
            custom_field_id: custom_field.id,
            user_id: actor.id.to_s,
            config_id: config.id.to_s,
            config_name: config.name.to_s
          },
        },
        {
          write_client: nil,
          duplicate_check: true
        }
      )
      Analytics::OrgCustomFieldRecorder.record(
        org: Analytics::MetricsRecorder.org_attributes(org),
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        event: "org_custom_field_edited",
        timestamp: timestamp,
        additional_data: metadata,
        custom_field: Analytics::MetricsRecorder.custom_field_attributes(custom_field),
        config: Analytics::MetricsRecorder.config_attributes(config),
        changed_column: col,
        old_val: "01",
        new_val: 10 # Will get turned into string
      )
    end
  end

end

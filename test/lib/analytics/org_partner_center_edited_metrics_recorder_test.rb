# frozen_string_literal: true
require 'test_helper'
class OrgPartnerCenterEditedMetricsRecorderTest < ActiveSupport::TestCase

  setup do
    Analytics::MetricsRecorder.stubs(:record)
    @klass = Analytics::OrgPartnerCenterEditedMetricsRecorder
    @timestamp      = Time.now.to_i
    @org            = create :organization
    @user           = create :user, organization: @org
    @partner_center = create :partner_center, {
      organization: @org,
      user:         @user,
      enabled:      true,
      description:  "OK",
      embed_link:   "link123",
      integration:  "foobar"
    }
    Analytics::MetricsRecorder.unstub(:record)
    InfluxdbRecorder.unstub(:record)
  end

  test 'records to influx' do
    InfluxdbRecorder.expects(:record).with( 'organizations',
      {
        timestamp: @timestamp,
        tags: {
          event:                "org_partner_center_edited",
          org_id:               @org.id.to_s,
          _user_id:             @user.id.to_s,
          is_system_generated: '1'
        },
        values: {
          user_id: @user.id.to_s,
          org_host_name: @org.host_name,
          changed_column: "enabled",
          old_val: "true",
          new_val: "false",
          org_name: @org.name,
          org_slug: @org.slug,
          org_is_open: @org.is_open ? '1' : '0',
          org_is_registrable: @org.is_registrable ? '1' : '0',
          org_status: @org.status,
          org_send_weekly_activity: @org.send_weekly_activity ? '1' : '0',
          org_show_sub_brand_on_login: @org.show_sub_brand_on_login ? '1' : '0',
          org_savannah_app_id: @org.savannah_app_id.to_s,
          org_social_enabled: @org.social_enabled ? '1' : '0',
          org_xapi_enabled: @org.xapi_enabled ? '1' : '0',
          org_show_onboarding: @org.show_onboarding ? '1' : '0',
          org_enable_role_based_authorization: @org.enable_role_based_authorization ? '1' : '0',
          partner_center_id: @partner_center.id.to_s,
          partner_center_enabled: @partner_center.enabled ? '1' : '0',
          partner_center_description: @partner_center.description,
          partner_center_embed_link: @partner_center.embed_link,
          partner_center_integration: @partner_center.integration,
        }
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    @klass.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      actor: Analytics::MetricsRecorder.user_attributes(@user),
      partner_center: Analytics::MetricsRecorder.partner_center_attributes(@partner_center),
      event: "org_partner_center_edited",
      timestamp: @timestamp,
      additional_data: {},
      changed_column: "enabled",
      old_val: "true",
      new_val: false # Will get turned into string
    )
  end

end

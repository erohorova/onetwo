# frozen_string_literal: true

require 'test_helper'
class OrgMetricsRecorderTest < ActiveSupport::TestCase
  test 'raises error if non-valid event passed' do
    error = assert_raises StandardError do
      Analytics::OrgMetricsRecorder.record(
        org: {},
        event: "fake",
        actor: {},
        timestamp: nil
      )
    end
    assert_equal error.message, "Event is not registered"
  end

  test 'records to influx' do
    %w{org_edited org_created}.each do |event|
      InfluxdbRecorder.unstub :record
      timestamp = Time.now.to_i
      org = create :organization
      client = create(:client, organization: org)
      actor = create :user, organization: org
      metadata = {
        user_agent: "user_agent",
        platform_version_number: "platform_version_number",
        is_admin_request: 0,
        user_id: actor.id
      }
      InfluxdbRecorder.expects(:record).with( 'organizations',
        {
          timestamp: timestamp,
          tags: {
            org_id: org.id.to_s,
            event: event,
            is_system_generated: '1',
            _user_id: actor.id.to_s
          },
          values: {
            is_admin_request: '0',
            org_host_name: org.host_name,
            org_name: org.name,
            org_slug: org.slug,
            org_client_id: org.client_id.to_s,
            org_is_open: org.is_open ? '1' : '0',
            org_is_registrable: org.is_registrable ? '1' : '0',
            org_status: org.status,
            org_send_weekly_activity: org.send_weekly_activity ? '1' : '0',
            org_show_sub_brand_on_login: org.show_sub_brand_on_login ? '1' : '0',
            org_savannah_app_id: org.savannah_app_id.to_s,
            org_social_enabled: org.social_enabled ? '1' : '0',
            org_xapi_enabled: org.xapi_enabled ? '1' : '0',
            org_show_onboarding: org.show_onboarding ? '1' : '0',
            org_enable_role_based_authorization: org.enable_role_based_authorization ? '1' : '0',
            platform_version_number: 'platform_version_number',
            user_agent: 'user_agent',
            user_id: actor.id.to_s
          }
        },
        {
          write_client: nil,
          duplicate_check: true
        }
      )
      Analytics::OrgMetricsRecorder.record(
        org: Analytics::MetricsRecorder.org_attributes(org),
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        event: event,
        timestamp: timestamp,
        additional_data: metadata
      )
    end
  end

  test 'records notification event to influx' do
    notification = {
      "json_payload" => { foo: "bar" }.to_json,
      "message" => "some message"
    }
    %w{org_custom_notification_sent}.each do |event|
      InfluxdbRecorder.unstub :record
      timestamp = Time.now.to_i
      org = create :organization
      client = create(:client, organization: org)
      actor = create :user, organization: org
      metadata = {
        user_agent: "user_agent",
        platform_version_number: "platform_version_number",
        is_admin_request: 0,
        user_id: actor.id
      }
      InfluxdbRecorder.expects(:record).with('organizations',
        {
          timestamp: timestamp,
          tags: {
            org_id: org.id.to_s,
            event: event,
            is_system_generated: '1',
            _user_id: actor.id.to_s
          },
          values: {
            is_admin_request: '0',
            org_host_name: org.host_name,
            org_name: org.name,
            org_slug: org.slug,
            org_client_id: org.client_id.to_s,
            notification_message: notification["message"],
            notification_json_payload: notification["json_payload"],
            org_is_open: org.is_open ? '1' : '0',
            org_is_registrable: org.is_registrable ? '1' : '0',
            org_status: org.status,
            org_send_weekly_activity: org.send_weekly_activity ? '1' : '0',
            org_show_sub_brand_on_login: org.show_sub_brand_on_login ? '1' : '0',
            org_savannah_app_id: org.savannah_app_id.to_s,
            org_social_enabled: org.social_enabled ? '1' : '0',
            org_xapi_enabled: org.xapi_enabled ? '1' : '0',
            org_show_onboarding: org.show_onboarding ? '1' : '0',
            org_enable_role_based_authorization: org.enable_role_based_authorization ? '1' : '0',
            platform_version_number: 'platform_version_number',
            user_agent: 'user_agent',
            user_id: actor.id.to_s
          }
        },
        {
          write_client: nil,
          duplicate_check: true
        }
      )
      Analytics::OrgMetricsRecorder.record(
        org: Analytics::MetricsRecorder.org_attributes(org),
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        event: event,
        timestamp: timestamp,
        notification: notification,
        additional_data: metadata
      )
    end
  end

  test 'should record whitelisted key as value' do
    InfluxdbRecorder.unstub :record
    timestamp = Time.now.to_i
    org = create :organization
    client = create(:client, organization: org)
    actor = create :user, organization: org
    dev_credential = create(:developer_api_credential, organization: org, creator: actor)
    metadata = {
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0,
      user_id: actor.id
    }
    InfluxdbRecorder.expects(:record).with( 'organizations',
      {
        timestamp: timestamp,
        tags: {
          org_id: org.id.to_s,
          event: "org_developer_api_credentials_created",
          is_system_generated: '1',
          _user_id: actor.id.to_s
        },
        values: {
          is_admin_request: '0',
          org_host_name: org.host_name,
          org_name: org.name,
          org_slug: org.slug,
          org_client_id: org.client_id.to_s,
          org_is_open: org.is_open ? '1' : '0',
          org_is_registrable: org.is_registrable ? '1' : '0',
          org_status: org.status,
          org_send_weekly_activity: org.send_weekly_activity ? '1' : '0',
          org_show_sub_brand_on_login: org.show_sub_brand_on_login ? '1' : '0',
          org_savannah_app_id: org.savannah_app_id.to_s,
          org_social_enabled: org.social_enabled ? '1' : '0',
          org_xapi_enabled: org.xapi_enabled ? '1' : '0',
          org_show_onboarding: org.show_onboarding ? '1' : '0',
          org_enable_role_based_authorization: org.enable_role_based_authorization ? '1' : '0',
          platform_version_number: 'platform_version_number',
          user_agent: 'user_agent',
          user_id: actor.id.to_s,
          developer_api_key: dev_credential.api_key
        }
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    Analytics::OrgMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      event: "org_developer_api_credentials_created",
      timestamp: timestamp,
      additional_data: metadata.merge(developer_api_key: dev_credential.api_key)
    )
  end

  test 'includes announcement attributes' do
    InfluxdbRecorder.unstub :record
    timestamp = Time.now.to_i
    org = create :organization
    client = create(:client, organization: org)
    actor = create :user, organization: org

    announcement_start = Time.now - 5.days
    announcement_end = Time.now - 4.days
    announcement_label = "announcement_label"
    announcement = Pronouncement.create(
      start_date: announcement_start,
      end_date: announcement_end,
      label: announcement_label,
      scope: org
    )

    %w{org_announcement_created org_announcement_deleted}.each do |event|
    metadata = {
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0,
      user_id: actor.id
    }
    InfluxdbRecorder.expects(:record).with( 'organizations',
      {
        timestamp: timestamp,
        tags: {
          org_id: org.id.to_s,
          event: event,
          is_system_generated: '1',
          _user_id: actor.id.to_s
         },
        values: {
          is_admin_request: '0',
          org_host_name: org.host_name,
          org_name: org.name,
          org_slug: org.slug,
          org_client_id: org.client_id.to_s,
          org_is_open: org.is_open ? '1' : '0',
          org_is_registrable: org.is_registrable ? '1' : '0',
          org_status: org.status,
          org_send_weekly_activity: org.send_weekly_activity ? '1' : '0',
          org_show_sub_brand_on_login: org.show_sub_brand_on_login ? '1' : '0',
          org_savannah_app_id: org.savannah_app_id.to_s,
          org_social_enabled: org.social_enabled ? '1' : '0',
          org_xapi_enabled: org.xapi_enabled ? '1' : '0',
          org_show_onboarding: org.show_onboarding ? '1' : '0',
          org_enable_role_based_authorization: org.enable_role_based_authorization ? '1' : '0',
          platform_version_number: 'platform_version_number',
          user_agent: 'user_agent',
          announcement_name: announcement.label.to_s,
          announcement_start_date: announcement.start_date.to_i,
          announcement_end_date: announcement.end_date.to_i,
          announcement_id: announcement.id.to_s,
          user_id: actor.id.to_s
        }
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    Analytics::OrgMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      event: event,
      timestamp: timestamp,
      announcement: Analytics::MetricsRecorder.pronouncement_attributes(announcement),
      additional_data: metadata
    )
    end
  end
  
  test 'includes taxonomy attributes' do
    InfluxdbRecorder.unstub :record
    timestamp = Time.now.to_i
    org = create :organization
    client = create(:client, organization: org)
    actor = create :user, organization: org
    topic_request = create(
      :topic_request, organization_id: org.id, label: "test", 
      requestor_id: client.id, approver_id: client.id, publisher_id: client.id
    )

    %w{org_taxonomy_created org_taxonomy_deleted}.each do |event|
    metadata = {
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0,
      user_id: actor.id
     }
    InfluxdbRecorder.expects(:record).with( 'organizations',
      {
        tags: {
          org_id: org.id.to_s,
          event: event,
          _user_id: actor.id.to_s,
          is_system_generated: '1'
        },
        values: {
          org_host_name: org.host_name,
          org_name: org.name,
          org_slug: org.slug,
          org_client_id: org.client_id.to_s,
          org_is_open: org.is_open ? '1' : '0',
          org_is_registrable: org.is_registrable ? '1' : '0',
          org_status: org.status,
          org_send_weekly_activity: org.send_weekly_activity ? '1' : '0',
          org_show_sub_brand_on_login: org.show_sub_brand_on_login ? '1' : '0',
          org_savannah_app_id: org.savannah_app_id.to_s,
          org_social_enabled: org.social_enabled ? '1' : '0',
          org_xapi_enabled: org.xapi_enabled ? '1' : '0',
          org_show_onboarding: org.show_onboarding ? '1' : '0',
          org_enable_role_based_authorization: org.enable_role_based_authorization ? '1' : '0',
          user_id: actor.id.to_s,
          taxonomy_id: topic_request.id.to_s,
          taxonomy_label: topic_request.label,
          taxonomy_requestor_id: topic_request.requestor_id.to_s,
          taxonomy_approver_id:  topic_request.approver_id.to_s,   
          taxonomy_publisher_id: topic_request.publisher_id.to_s,
          taxonomy_state: topic_request.state,
          platform_version_number: 'platform_version_number',
          user_agent: 'user_agent',
          is_admin_request: '0'
        },
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    Analytics::OrgMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      event: event,
      timestamp: timestamp,
      taxonomy: Analytics::MetricsRecorder.topic_request_attributes(topic_request),
      additional_data: metadata
    )
    end
   end

  test 'includes embargo attributes' do
    InfluxdbRecorder.unstub :record
    timestamp = Time.now.to_i
    org = create :organization
    client = create(:client, organization: org)
    actor = create :user, organization: org
    embargo = Embargo.create!(
        organization_id: org.id, category: "approved_sites", 
        value: "https://approved.sites", user: actor
      )

    %w{org_embargo_added org_embargo_removed}.each do |event|
    metadata = {
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      is_admin_request: 0,
      user_id: actor.id
    }
    InfluxdbRecorder.expects(:record).with( 'organizations',
      {
        tags: {
          org_id: org.id.to_s,
          event: event,
          _user_id: actor.id.to_s,
          is_system_generated: '1'
        },
        values: {
          org_host_name: org.host_name,
          org_name: org.name,
          org_slug: org.slug,
          org_client_id: org.client_id.to_s,
          org_is_open: org.is_open ? '1' : '0',
          org_is_registrable: org.is_registrable ? '1' : '0',
          org_status: org.status,
          org_send_weekly_activity: org.send_weekly_activity ? '1' : '0',
          org_show_sub_brand_on_login: org.show_sub_brand_on_login ? '1' : '0',
          org_savannah_app_id: org.savannah_app_id.to_s,
          org_social_enabled: org.social_enabled ? '1' : '0',
          org_xapi_enabled: org.xapi_enabled ? '1' : '0',
          org_show_onboarding: org.show_onboarding ? '1' : '0',
          org_enable_role_based_authorization: org.enable_role_based_authorization ? '1' : '0',
          user_id: actor.id.to_s,
          embargo_id: embargo.id.to_s,
          embargo_type: embargo.category,
          embargo_content: embargo.value,
          platform_version_number: 'platform_version_number',
          user_agent: 'user_agent',
          is_admin_request: '0'
        },
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    Analytics::OrgMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      event: event,
      timestamp: timestamp,
      embargo: Analytics::MetricsRecorder.embargo_attributes(embargo),
      additional_data: metadata
    )
    end
   end

  test 'records sources credential event' do
    InfluxdbRecorder.unstub :record
    timestamp = Time.now.to_i
    org       = create :organization
    client    = create(:client, organization: org)
    actor     = create :user, organization: org
    event     = "org_sources_credential_created"
    
    metadata  = {
      user_agent:               "user_agent",
      platform_version_number:  "platform_version_number",
      is_admin_request:         0,
      user_id:                  actor.id
    }
    auth_api_info = {"auth_required":true, "url":"asdsa", "method":"POST", "host":"asdsa das"}

    sources_credential = SourcesCredential.create(
      id:             10000001,
      organization:   org, 
      source_id:      "8b5ab2f1", 
      shared_secret:  "11a8a57",
      auth_api_info:  auth_api_info,
      source_name:    "TEST"
    )

    InfluxdbRecorder.expects(:record).with( 'organizations',
      {
        tags: {
          org_id: org.id.to_s,
          event: event,
          _user_id: actor.id.to_s,
          is_system_generated: '1'
        },
        values: {
          org_host_name: org.host_name,
          org_name: org.name,
          org_slug: org.slug,
          org_client_id: org.client_id.to_s,
          org_is_open: org.is_open ? '1' : '0',
          org_is_registrable: org.is_registrable ? '1' : '0',
          org_status: org.status,
          org_send_weekly_activity: org.send_weekly_activity ? '1' : '0',
          org_show_sub_brand_on_login: org.show_sub_brand_on_login ? '1' : '0',
          org_savannah_app_id: org.savannah_app_id.to_s,
          org_social_enabled: org.social_enabled ? '1' : '0',
          org_xapi_enabled: org.xapi_enabled ? '1' : '0',
          org_show_onboarding: org.show_onboarding ? '1' : '0',
          org_enable_role_based_authorization: org.enable_role_based_authorization ? '1' : '0',
          user_id: actor.id.to_s,
          sources_credential_id: sources_credential.id.to_s,
          sources_credential_source_id: sources_credential.source_id,
          sources_credential_shared_secret: sources_credential.shared_secret,
          sources_credential_auth_api_info: sources_credential.auth_api_info.to_json,
          sources_credential_source_name: sources_credential.source_name,
          platform_version_number: 'platform_version_number',
          user_agent: 'user_agent',
          is_admin_request: '0'
        },
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )
    Analytics::OrgMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      event: event,
      timestamp: timestamp,
      sources_credential: Analytics::MetricsRecorder.sources_credential_attributes(sources_credential),
      additional_data: metadata
    )
    end
 end
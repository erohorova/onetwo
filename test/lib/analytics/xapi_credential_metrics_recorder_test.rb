# frozen_string_literal: true

require 'test_helper'
class XapiCredentialMetricsRecorderTest < ActiveSupport::TestCase
  setup do
    @org = create :organization
    @xapi_credential = create :xapi_credential, organization: @org
    @user = create :user, organization: @org
    @class = Analytics::XapiCredentialMetricsRecorder
    @timestamp = Time.now.to_i
    @metrics_recorder = Analytics::MetricsRecorder
    InfluxdbRecorder.unstub(:record)
  end

  test 'records to influx' do
    events = %w{
      xapi_credential_created
      xapi_credential_deleted
    }
    events.each do |event|
      InfluxdbRecorder.expects(:record).with('xapi_credentials', {
        :values => {
          :xapi_credential_id => @xapi_credential.id.to_s,
          :user_full_name     => @user.full_name,
          :user_handle        => @user.handle,
          :user_id            => @user.id.to_s,
          :org_name           => @org.name,
          :org_hostname       => @org.host_name,
          :endpoint           => 'http://xapi.example.com/lrs/statements',
          :lrs_login_key      => 'example',
          :lrs_api_version    => '1.0.0'
        },
        :tags => {
          :_user_id            => @user.id.to_s,
          :org_id              => @org.id.to_s,
          :event               => event,
          :is_system_generated => '1'
        },
        :timestamp => @timestamp
      }, {
        write_client: nil,
        duplicate_check: true
      })
      @class.record(
        org: @metrics_recorder.organization_attributes(@org),
        xapi_credential: @metrics_recorder.xapi_credential_attributes(@xapi_credential),
        event: event,
        actor: @metrics_recorder.user_attributes(@user),
        timestamp: @timestamp,
        additional_data: {},
      )
    end
  end


end

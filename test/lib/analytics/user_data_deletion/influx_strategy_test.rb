# frozen_string_literal: true
require 'test_helper'

class Analytics::UserDataDeletion::InfluxStrategyTest < ActiveSupport::TestCase

  setup do
    @org = create :organization
    @user = create :user, organization: @org

    @class = Analytics::UserDataDeletion::InfluxStrategy

    # get handles on the export class, since we re-use its queries.
    @data_export_class = Analytics::UserDataExport::InfluxStrategy
    @data_export_queries = @data_export_class.queries(@user)

    # The user deletion queries are modified from the export ones.
    @expected_queries = @data_export_queries.transform_values(&:delete_all!)

    # The real query objects
    @queries = @class.queries(@user)
    @class.stubs(:queries).with(@user).returns @queries
  end

  test '.run' do
    @queries.each do |name, query|
      query.expects(:resolve).returns([])
    end
    expected = @queries.transform_values { [] }
    assert_equal expected, @class.run!(@user)
  end

  test '.queries' do
    assert_equal(
      @expected_queries.transform_values(&:finalize_query),
      @queries.transform_values(&:finalize_query)
    )
    finalized = @queries.transform_values(&:finalize_query)

    # apologize about the formatting here, but this is mostly just copy-pasted
    # and it's too much work to change it!
    expected = {
      :cards=>"delete  from \"cards\" where analytics_version = %{version} AND time >= %{start_date}s AND (_user_id = '#{@user.id}' OR _assigned_to_user_id = '#{@user.id}' OR card_author_id = '#{@user.id}') ;",

      :channels=>"delete  from \"channels\" where analytics_version = %{version} AND time >= %{start_date}s AND (_user_id = '#{@user.id}' OR _collaborator_id = '#{@user.id}' OR _curator_id = '#{@user.id}' OR _follower_id = '#{@user.id}') ;",

      :groups=>"delete  from \"groups\" where analytics_version = %{version} AND time >= %{start_date}s AND (_group_user_id = '#{@user.id}') ;",

      :users=>"delete  from \"users\" where analytics_version = %{version} AND time >= %{start_date}s AND (_user_id = '#{@user.id}' OR _visited_profile_user_id = '#{@user.id}' OR followed_user_id = '#{@user.id}' OR _follower_id = '#{@user.id}' OR _member_user_id = '#{@user.id}' OR _suspended_user_id = '#{@user.id}') ;",

      :searches=>"delete  from \"searches\" where analytics_version = %{version} AND time >= %{start_date}s AND (_user_id = '#{@user.id}') ;",

      :organizations=>"delete  from \"organizations\" where analytics_version = %{version} AND time >= %{start_date}s AND (_user_id = '#{@user.id}') ;"
    }
    assert_equal expected, finalized
  end

end
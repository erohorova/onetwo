require 'test_helper'
class Analytics::UserSkillMetricsRecorderTest < ActiveSupport::TestCase

  test "calls InfluxdbRecorder with expected args" do
    org = create(:organization)
    user = create(:user, organization: org)
    fb_ip_provider = create(:identity_provider,
      type: 'FacebookProvider',
      uid: 'SPARK123',
      user: user
    )
    skill = create(:skill, name: 'ruby')
    user_skill = create(:skills_user,
      skill_level: 'beginner', user: user,
      skill: skill, credential: [{username: 'username'}],
      credential_url: 'https://www.edcast.com',
      credential_name: 'TEST',
      experience: '2 years',
      description: 'SKILL DESCRIPTION'
    )

    metadata = {
      platform: "platform",
      user_agent: "user_agent",
      platform_version_number: "1.0.0",
      is_admin_request: 0
    }
    InfluxdbRecorder.unstub(:record)
    timestamp = Time.now.to_i
    # both events look the exact same, except for the 'event' string
    %w{user_skill_created user_skill_deleted user_skill_edited}.each do |event|
      fields = {
        platform:                'platform',
        user_org_uid:            fb_ip_provider.uid,
        org_hostname:            org.host_name,
        is_admin_request:        '0',
        onboarding_status:       '1',
        sign_in_at:              '0',
        user_id:                 user.id.to_s,
        user_handle:             user.handle,
        full_name:               user.full_name,
        user_role:               'member',
        platform_version_number: '1.0.0',
        user_agent:              'user_agent',
        skill_name:              skill.name,
        skill_id:                skill.id.to_s,
        skill_description:       user_skill.description,
        skill_experience:        user_skill.experience,
        skill_level:             user_skill.skill_level,
        skill_credential_name:   user_skill.credential_name,
        skill_credential_url:    user_skill.credential_url,
        skill_credential:        user_skill.credential.to_json
      }

      if event == 'user_skill_edited'
        fields.merge!({
          changed_column:   'experience',
          old_val:          '2 years',
          new_val:          '5 years',
          skill_experience: '5 years'
        })
      end

      InfluxdbRecorder.expects(:record).with(
        'users', {
          timestamp: timestamp,
          tags: {
            _user_id:            user.id.to_s,
            org_id:              org.id.to_s,
            event:               event,
            is_system_generated: '0',
          },
          values: fields
        },
        {
          write_client: nil,
          duplicate_check: true
        }
      )
    end

    Analytics::UserSkillMetricsRecorder.record(
      org:             Analytics::MetricsRecorder.org_attributes(org),
      timestamp:       timestamp,
      actor:           Analytics::MetricsRecorder.user_attributes(user),
      event:           "user_skill_created",
      skill:           Analytics::MetricsRecorder.skill_attributes(skill),
      user_skill:      Analytics::MetricsRecorder.skills_user_attributes(user_skill),
      additional_data: metadata
    )

    Analytics::UserSkillMetricsRecorder.record(
      org:             Analytics::MetricsRecorder.org_attributes(org),
      timestamp:       timestamp,
      actor:           Analytics::MetricsRecorder.user_attributes(user),
      event:           "user_skill_deleted",
      skill:           Analytics::MetricsRecorder.skill_attributes(skill),
      user_skill:      Analytics::MetricsRecorder.skills_user_attributes(user_skill),
      additional_data: metadata
    )

    user_skill.update experience: '5 years'
    Analytics::UserSkillMetricsRecorder.record(
      org:             Analytics::MetricsRecorder.org_attributes(org),
      timestamp:       timestamp,
      actor:           Analytics::MetricsRecorder.user_attributes(user),
      event:           "user_skill_edited",
      skill:           Analytics::MetricsRecorder.skill_attributes(skill),
      user_skill:      Analytics::MetricsRecorder.skills_user_attributes(user_skill),
      changed_column:  'experience',
      old_val:         '2 years',
      new_val:         '5 years',
      additional_data: metadata
    )
  end

end

require 'test_helper'

class Analytics::GenericEventRecorderTest < ActionController::TestCase
  setup do
    @klass         = Analytics::GenericEventRecorder

    # Create a sample user and organization
    @org           = create :organization
    @user          = create :user, organization: @org

    # Stub time (the class uses Time.now for the timestamp)
    @stub_time = Time.now
    Time.stubs(:now).returns @stub_time
    @timestamp = InfluxdbRecorder.to_ns("groups", @stub_time.to_i)

    # Metadata that is written to RequestStore
    @metadata = {
      platform:                "test platform",
      user_agent:              "test user agent",
      platform_version_number: "test platform version number",
      is_admin_request:        "1"
    }

    RequestStore.store[:request_metadata] = @metadata

    # Attributes of the event
    @event       = "group_notification_sent"
    @measurement = "groups"
    @attributes  = { "foo" => "bar" }
    @group_id     = "123"

    # Stub the valid events, so that @event is valid
    @klass::MEASUREMENTS_BY_EVENT[@event] = "groups"

    # The final event object which is recorded to influx
    @event_obj = {
      timestamp: @timestamp,
      tags: {
        org_id:    @org.id.to_s,
        _user_id:  @user.id.to_s,
        event:     @event,
        _group_id: @group_id,
        is_system_generated: "0",
      },
      values: {
        user_id:    @user.id.to_s,
        group_id:   @group_id,
        attributes: @attributes.to_json
      }.merge(@metadata)
    }

    $event_obj = @event_obj

    # Parameters that are passed to GenericEventRecorder.new in the tests
    @klass_constructor_args = {
      user_id: @user.id.to_s,
      org_id:  @org.id.to_s,
      params:  {
        "event"       => @event,
        "attributes"  => @attributes,
        "group_id"    => @group_id
      }
    }

    # Expected parameters to flow through MetricsRecorderJob
    @metrics_recorder_job_args = {
      recorder:    @klass.name,
      measurement: @measurement,
      event_obj:   @event_obj
    }

    # Standard test for failure cases
    @failure_test = -> (err_msg, &blk) do
      Analytics::MetricsRecorderJob.expects(:perform_later).never
      blk.call
      result = @klass.new(@klass_constructor_args).enqueue_recording_job
      assert_equal [nil, err_msg], result
    end
  end

  test 'none of the regular events are whitelisted as generic events' do
    regular_events = Set.new([
      *Analytics::MetricsRecorder.load_event_recorder_mapping.keys,
      "user_score"
    ])
    generic_events = @klass::MEASUREMENTS_BY_EVENT.keys
    refute generic_events.any? { |event| regular_events.member? event }
  end

  test 'calls MetricsRecorderJob with valid args' do
    Analytics::MetricsRecorderJob.
      expects(:perform_later).
      with(@metrics_recorder_job_args)

    result = @klass.new(@klass_constructor_args).enqueue_recording_job
    assert_equal [@event_obj, nil], result
  end

  test 'works when there are no special required args' do
    @event_obj[:tags].delete "_group_id"
    @event_obj[:values].delete "group_id"

    Analytics::MetricsRecorderJob.
      expects(:perform_later).
      with(@metrics_recorder_job_args)

    result = @klass.new(@klass_constructor_args).enqueue_recording_job
    assert_equal [@event_obj, nil], result
  end

  test 'returns error with missing event' do
    err = "Param 'event' is missing or not a string."
    @failure_test.call(err) do
      @klass_constructor_args[:params].delete "event"
    end
  end

  test 'returns error with invalid type for event' do
    err = "Param 'event' is missing or not a string."
    @failure_test.call(err) do
      @klass_constructor_args[:params]["event"] = 1
    end
  end

  test 'returns error with non-registered event' do
    err = (
      "Param 'event' is not registered. " +
      "Valid events: #{@klass::MEASUREMENTS_BY_EVENT.keys.join(",")}"
    )
    @failure_test.call(err) do
      @klass_constructor_args[:params]["event"] = "fake"
    end
  end  

  test 'returns error with missing required arg' do
    err = (
      "Param 'group_id' is required " +
      "for measurement 'groups'. " +
      "It is either missing or not a string."
    )
    @failure_test.call(err) do
      @klass_constructor_args[:params].delete "group_id"
    end
  end

  test 'returns error with required arg that is not a string' do
    err = (
      "Param 'group_id' is required " +
      "for measurement 'groups'. " +
      "It is either missing or not a string."
    )
    @failure_test.call(err) do
      @klass_constructor_args[:params]["group_id"] = 123
    end
  end

  test 'gets .record called by MetricsRecorderJob' do
    @klass.expects(:record).with(
      measurement: @measurement,
      event_obj:   @event_obj
    )
    Analytics::MetricsRecorderJob.perform_now(
      recorder:    @klass.name,
      measurement: @measurement,
      event_obj:   @event_obj
    )
  end

  test 'calls influxdb_record from .record' do
    Analytics::MetricsRecorder.expects(:influxdb_record).with(
      @measurement,
      @event_obj,
    )
    @klass.record(
      measurement: @measurement,
      event_obj:   @event_obj
    )
  end

end

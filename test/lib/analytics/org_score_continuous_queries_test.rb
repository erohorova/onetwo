# frozen_string_literal: true

require 'test_helper'

class OrgScoreContinuousQueriesTest < ActiveSupport::TestCase

  setup do

    skip # SKIP THIS WHOLE FILE UNLESS TESTING LOCALLY
    @class = Analytics::OrgScoreContinuousQueries

    WebMock.disable!

    @user_scores_cq = Analytics::UserScoreContinuousQueries

    @owner_id = "123"
    @actor_id = "456"
    @org_id = "789"

    InfluxdbRecorder.unstub(:record)
    INFLUXDB_CLIENT.unstub :write_point

    @timestamps = 1.upto(3).reduce({}) do |memo, i|
      memo[i] = i.days.ago.utc.change(
        sec: 0, usec: 0, min: 0, hour: 0
      ).to_i
      memo
    end

    INFLUXDB_CLIENT.query "DROP MEASUREMENT user_scores"
    INFLUXDB_CLIENT.query "DROP MEASUREMENT user_scores_daily"
    INFLUXDB_CLIENT.query "DROP MEASUREMENT org_scores_daily"

    # push some sample data (when proc is called)
    @seed_data = -> {
      scores = {
        owner: [@owner_id, 5],
        actor: [@actor_id, 10]
      }
      events = %w{
        card_comment_created
        card_marked_as_complete
        card_viewed
        card_created
        card_liked
      }
      #                                               # 30 total events
      #                                               # ------------------------
      @timestamps.values.each do |timestamp|          # 3 iterations (days)
        events.each do |event|                        # 5 iterations (events)
          scores.each do |role, (user_id, score)|     # 2 iterations (users)
            InfluxdbRecorder.record('user_scores', {
              tags: {
                role: role.to_s,
                owner_id: @owner_id,
                user_id: user_id,
                actor_id: @actor_id,
                org_id: @org_id,
                event: event.to_s
              },
              values: {
                score_value: score
              },
              timestamp: timestamp
            })
          end
        end
      end
    }
  end

  test "builds the correct query" do
    query = @class.build_backfill_query
    expected_query = <<-TXT.chomp.strip_heredoc
      SELECT SUM(total_user_score) AS total_user_score,SUM(smartbites_commented_count) AS smartbites_commented_count,SUM(smartbites_completed_count) AS smartbites_completed_count,SUM(smartbites_created_count) AS smartbites_created_count,SUM(smartbites_consumed_count) AS smartbites_consumed_count,SUM(smartbites_liked_count) AS smartbites_liked_count,SUM(total_smartbite_score) AS total_smartbite_score,SUM(time_spent_minutes) AS time_spent_minutes INTO org_scores_daily FROM user_scores_daily GROUP BY org_id,time(1d)
    TXT
    assert_equal expected_query, query
  end

  test 'pushes the correct data to influx when run as backfill' do
    @seed_data.call
    INFLUXDB_CLIENT.query @user_scores_cq.build_backfill_query
    INFLUXDB_CLIENT.query @class.build_backfill_query
    user_scores_daily = INFLUXDB_CLIENT
      .query("SELECT * FROM user_scores_daily")
      .shift["values"]
      .group_by { |val| val["time"] }
    org_scores_daily = INFLUXDB_CLIENT
      .query("SELECT * FROM org_scores_daily")
      .shift["values"]
      .group_by { |val| val["time"] }
    user_scores_daily.each do |time, values|
      %w{
        total_user_score
        smartbites_commented_count
        smartbites_completed_count
        smartbites_created_count
        smartbites_consumed_count
        smartbites_liked_count
        total_smartbite_score
        time_spent_minutes
      }.each do |attr|
        sum = values.map { |val| val[attr] || 0 }.sum
        assert_equal org_scores_daily[time][0][attr], sum
      end
    end
  end

  test 'launches continuous query' do
    INFLUXDB_CLIENT.expects(:create_continuous_query).with(
      "fetch_org_scores_daily",
      Settings.influxdb.database,
      @class.build_backfill_query,
      resample_every: "1d",
      resample_for: "2d"
    )
    @class.run_continuous_query
  end

end
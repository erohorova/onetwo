require 'test_helper'
class CardCommentRecorderTest < ActiveSupport::TestCase

  setup do
    author_first_name = Faker::Name.first_name
    actor_first_name = Faker::Name.first_name

    author_last_name = Faker::Name.last_name
    actor_last_name = Faker::Name.last_name

    @org = create :organization,
        name: 'LXP',
        host_name: 'spark'
    @author = create :user,
        first_name: author_first_name,
        last_name: author_last_name,
        organization: @org
    @actor = create :user,
        first_name: actor_first_name,
        last_name: actor_last_name,
        organization: @org
    @card = create :card,
        author: @author,
        created_at: 2.days.ago
    @comment = create :comment,
        commentable: @card,
        user: @actor,
        message: 'comment message'
  end

  test 'valid registered events' do
    registered_events = ['card_comment_created', 'card_comment_deleted']
    assert_equal(
        registered_events.sort,
        Analytics::CardCommentRecorder::CARD_COMMENT_EVENTS.sort
    )
  end

  test 'valid record args' do
    invalid_event = 'invalid event'
    InfluxdbRecorder.stubs(:record).returns :true
    assert_nothing_raised do
      Analytics::CardCommentRecorder.record(
        org: Analytics::MetricsRecorder.org_attributes(@org),
        comment: Analytics::MetricsRecorder.comment_attributes(@comment),
        card: Analytics::MetricsRecorder.card_attributes(@card),
        actor: Analytics::MetricsRecorder.user_attributes(@actor),
        timestamp: @comment.created_at,
        event: 'card_comment_created'
      )

      Analytics::CardCommentRecorder.record(
        org: Analytics::MetricsRecorder.org_attributes(@org),
        comment: Analytics::MetricsRecorder.comment_attributes(@comment),
        actor: Analytics::MetricsRecorder.user_attributes(@actor),
        card: Analytics::MetricsRecorder.card_attributes(@card),
        timestamp: Time.now,
        event: 'card_comment_deleted'
      )
    end

    assert_raises do
      Analytics::CardCommentRecorder.record(
        org: Analytics::MetricsRecorder.org_attributes(@org),
        comment: Analytics::MetricsRecorder.comment_attributes(@comment),
        actor: Analytics::MetricsRecorder.user_attributes(@actor),
        card: Analytics::MetricsRecorder.card_attributes(@card),
        timestamp: Time.now,
        event: invalid_event,
        additional_data: {}
      )
    end
  end

  test 'should record card vote event' do
    InfluxdbRecorder.unstub :record
    card_comment = Analytics::CardCommentRecorder::EVENT_CARD_COMMENT_CREATED
    timestamp = Time.now.to_i

    field_set = card_field_set(
      @org, @card, @actor
    )

    tag_set = card_tag_set(
      @card, @actor, card_comment
    )

    InfluxdbRecorder.expects(:record).with('cards',
      {
        values: field_set.symbolize_keys.merge(
          {
            comment_type: "StandardComment",
            comment_message: @comment.message,
            comment_id: @comment.id.to_s,
            platform: 'ios'
          }
        ).compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true

    Analytics::CardCommentRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      comment: Analytics::MetricsRecorder.comment_attributes(@comment),
      card: Analytics::MetricsRecorder.card_attributes(@card),
      actor: Analytics::MetricsRecorder.user_attributes(@actor),
      timestamp: timestamp,
      event: card_comment,
      additional_data: {
        platform: 'ios',
        user_agent: 'Edcast iPhone',
        is_admin_request: 0
      }
    )
  end
end
require 'test_helper'

class Analytics::DuplicateRecordRemoverTest < ActiveSupport::TestCase

  setup do
    @org = create :organization
    @class = Analytics::DuplicateRecordRemover
    @org_id = @org.id.to_s
    @opts = {
      grouping_columns: ["foo", "bar"],
      conditions: {event: "card_uploaded"},
      time_frame: "10m",
      field: "name",
      measurement: "things",
      min_time: (Time.now - 100.days).to_i,
      max_time: (Time.now - 5.days).to_i
    }

    @time = Time.now.utc
    @time_string = @time.strftime("%Y-%m-%dT%H:%M:%S.%9NZ")

    @sample_record = {
      "foo" => "foo1",
      "bar" => "bar1",
      "time" => @time_string,
      "org_id" => @org_id,
      "event" => "card_uploaded"
    }
    @duplicate_records_query_result = [{
      "values" => [@sample_record.merge("_duplicate_count" => 2)]
    }]

    @filter_attrs = {
      "foo" => "foo1",
      "bar" => "bar1",
      "org_id" => @org_id,
      "event" => "card_uploaded"
    }

    @duplicate_records_details_query_result = [{
      # Note: the 'idx' key is not actually present in real responses,
      # it's just used here to distinguish the two otherwise identical
      # objects
      "values" => 2.times.map { @sample_record }
    }]

    @class.any_instance.stubs :log_records
  end

  test '#initialize with default opts' do
    inst = @class.new
    assert_equal([], inst.grouping_columns)
    assert_equal({}, inst.conditions)
    assert_equal("1s", inst.time_frame)
    assert_equal("user_id", inst.field)
  end

  test 'attr_readers raise errors if they are not set' do
    inst = @class.new
    msg = assert_raises(RuntimeError) { inst.measurement }.message
    assert_equal msg, "measurement has not been set"

    msg = assert_raises(RuntimeError) { inst.min_time }.message
    assert_equal msg, "min_time has not been set"
  end

  test '#initialize with custom opts' do
    min_time =(Time.now - 100.days).to_i
    inst = @class.new(**@opts)
    assert_equal(@opts[:grouping_columns], inst.grouping_columns)
    assert_equal(@opts[:conditions],       inst.conditions)
    assert_equal(@opts[:time_frame],       inst.time_frame)
    assert_equal(@opts[:field],            inst.field)
    assert_equal(@opts[:measurement],      inst.measurement)
    assert_equal(@opts[:min_time],         inst.min_time)
    assert_equal(@opts[:max_time],         inst.max_time)
  end

  test '#run' do
    query = (
      "select foo,bar,event,org_id,_duplicate_count from " +
      "(" +
        "select COUNT(name) AS _duplicate_count from \"things\" " +
        "where time >= #{@opts[:min_time]}s " +
        "AND time <= #{@opts[:max_time]}s " +
        "AND org_id = '#{@org_id}' " +
        "AND event = 'card_uploaded' " +
        "GROUP BY foo,bar,event,org_id,time(10m)" +
      ") " +
      "where _duplicate_count > %{_duplicate_count} ;"
    )
    params = {
      _duplicate_count: 1,
    }
    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)
      .returns(@duplicate_records_query_result)
    inst = @class.new(**@opts)
    inst.expects(:remove_duplicates).with(
      @org_id,
      @duplicate_records_query_result[0]["values"][0]
    )
    inst.run
  end

  test '#remove_duplicates' do
    inst = @class.new(**@opts)
    attrs = @duplicate_records_query_result[0]["values"][0]
    query = (
      "select * from \"things\" where foo = %{foo} AND bar = %{bar} " +
      "AND org_id = %{org_id} AND event = %{event} AND time >= %{start_date}s " +
      "AND time <= %{end_date}s ;"
    )
    params = {
      :foo => 'foo1',
      :bar => 'bar1',
      :org_id => @org_id,
      :event => 'card_uploaded',
      :start_date => @time.to_i - 1,
      :end_date => @time.to_i + 1,
    }

    INFLUXDB_CLIENT.expects(:query)
      .with(query, params: params)
      .returns(@duplicate_records_details_query_result)

    inst.expects(:delete_record_with_attrs).with(
      @duplicate_records_details_query_result[0]["values"][1],
      @filter_attrs
    )
    inst.remove_duplicates(@org_id, attrs)
  end

  test '#delete_record_with_attrs' do
    inst = @class.new(**@opts)
    inst.stubs(:log_records) # test this method elsewhere
    query = (
      "delete  from \"things\" where foo = %{foo} AND bar = %{bar} " +
      "AND org_id = %{org_id} AND event = %{event} AND time = %{time} ;"
    )
    params = {
      foo: 'foo1',
      bar: 'bar1',
      org_id: @org_id,
      event: 'card_uploaded',
      time: (@time.to_i.to_s + @time.nsec.to_s).to_i
    }
    INFLUXDB_CLIENT.expects(:query).with(query, params: params)
    inst.delete_record_with_attrs(@sample_record, @filter_attrs)
  end

  test '#log_records' do
    begin
      @class.any_instance.unstub :log_records
      inst = @class.new(**@opts)
      query = InfluxQuery.new("things")
      values = [{ "a": "1" }, { "b": "2" }]
      query.expects(:resolve).returns [{
        "values" => values
      }]
      inst.log_records(query)
      expected = values.to_json
      assert_equal expected, File.read("./log/duplicate_cleanup_log.txt").chomp
    ensure
      File.delete "./log/duplicate_cleanup_log.txt"
    end
  end

  test 'integration test' do
    # This test doesn't predictably pass for some reason,
    # probably because of the asynchronous nature of influx commands.
    skip

    begin
      measurement = "_duplicate_record_remover_test"
      orig_measurement = Analytics::CardMetricsRecorder::MEASUREMENT
      Analytics::CardMetricsRecorder.const_set :MEASUREMENT, measurement

      WebMock.disable!
      InfluxdbRecorder.unstub(:record)
      INFLUXDB_CLIENT.unstub :write_point

      org = create :organization
      user = create :user, organization: org
      card = create :card, author: user

      # Will create records at 5 times, each 5 seconds apart.
      now = Time.now.utc.to_i
      times = 5.times.map { |i| now - (5*i) }

      recorder_attrs = {
        org: Analytics::MetricsRecorder.org_attributes(org),
        actor: Analytics::MetricsRecorder.user_attributes(user),
        card: Analytics::MetricsRecorder.card_attributes(card),
        event: "card_viewed"
      }

      # create 5 duplicate records at each time
      times.each do |time|
        5.times do
          # Although we give the same timestamp to each, they should still
          # be distinct records because of the automatic nanosecond variation
          # we added to MetricsRecorder
          Analytics::CardViewMetricsRecorder.record(recorder_attrs.merge(
            timestamp: time
          ))
        end
      end

      # Verify the records that were created

      records_query = InfluxQuery.new(measurement)
        .select!("COUNT(user_id)")
        .filter!(:event, "event", "=", "card_viewed")
        .group_by!("time(1s)")
        .add_time_filters!(start_time: (Time.now - 60.seconds).to_i)

      orig_records_result = records_query
        .resolve[0]["values"]
        .reject { |val| val["count"] == 0 }
        .group_by { |val| val["time"] }

      orig_records_timestamps = orig_records_result.keys
        .map(&Time.method(:parse))
        .map(&:to_i)
        .sort

      assert_equal times.sort, orig_records_timestamps

      orig_records_counts = orig_records_result.values
        .flatten
        .map {|val| val["count"] }

      assert_equal 5.times.map { 5 }, orig_records_counts

      # Run the duplicate deleter
      duplicate_deleter = @class.new(
        grouping_columns: ["_user_id", "_card_id"],
        conditions: { event: "card_viewed" },
        time_frame: "1s",
        field: "user_id",
        min_time: (Time.now - 60.seconds).to_i,
        measurement: measurement
      )

      sleep 5
      duplicate_deleter.run
      sleep 5

      # Now we re-run the query from earlier to make sure records were deleted
      final_records_result = records_query
        .resolve[0]["values"]
        .reject { |val| val["count"] == 0 }
        .group_by { |val| val["time"] }

      final_records_timestamps = final_records_result.keys
        .map(&Time.method(:parse))
        .map(&:to_i)
        .sort

      assert_equal times.sort, final_records_timestamps

      final_records_counts = final_records_result.values
        .flatten
        .map {|val| val["count"] }

      assert_equal 5.times.map { 1 }, final_records_counts
    ensure
      INFLUXDB_CLIENT.query "DROP MEASUREMENT #{measurement}"
      Analytics::CardMetricsRecorder.const_set :MEASUREMENT, orig_measurement
      WebMock.enable!
    end
  end

end
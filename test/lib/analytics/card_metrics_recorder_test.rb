 # frozen_string_literal: true

require 'test_helper'
class CardMetricsRecorderTest < ActiveSupport::TestCase
  setup do

    @class = Analytics::CardMetricsRecorder

    author_first_name = Faker::Name.first_name
    actor_first_name = Faker::Name.first_name

    author_last_name = Faker::Name.last_name
    actor_last_name = Faker::Name.last_name

    @org = create :organization,
      name: 'LXP',
      host_name: 'spark'

    @author = create :user,
      first_name: author_first_name,
      last_name: author_last_name,
      organization: @org

    @actor = create :user,
      first_name: actor_first_name,
      last_name: actor_last_name,
      organization: @org

    @card = create :card,
      author: @author,
      duration: 123,
      readable_card_type: Card.readable_card_type("Course")

    CardMetadatum.create!(
      card_id: @card.id,
      average_rating: 1.5
    )

  end

  test 'should extract tags' do
    card_create = @class::EVENT_CARD_CREATED
    tag_set = @class
              .extract_tags(@org, @card, card_create, @actor)
              .with_indifferent_access
    valid_keys = %w{
      org_id _card_id event _user_id
    }
    tag_set.assert_valid_keys(valid_keys)
  end

  test 'should extract fields' do
    field_set = @class
                .extract_fields(
                  @org,
                  RecursiveOpenStruct.new(
                    Analytics::MetricsRecorder.card_attributes(@card)
                  ),
                  @actor
                ).with_indifferent_access
    valid_keys = [
      "ecl_id", "card_id", "card_title", "card_subtype", "card_resource_url",
      "card_author_id", "user_id", "org_name", "card_state", "card_type",
      "org_hostname", "is_card_promoted", "ecl_source_name", "is_user_generated",
      "is_live_stream", "card_author_full_name", "user_full_name", "user_handle",
      "is_public", "card_duration", "readable_card_type", "average_rating"
    ]
    field_set.assert_valid_keys(valid_keys)
  end

   test "stores is_user_generated=1 if card has no ecl data" do
    InfluxdbRecorder.unstub(:record)
    card_create = @class::EVENT_CARD_CREATED
    field_set = card_field_set(@org, @card, @author)
    tag_set = card_tag_set(@card, @author, card_create)
    assert_equal(field_set[:is_user_generated], '1')
    timestamp = Time.now.to_i

    InfluxdbRecorder.expects(:record).with('cards',
      {
        values: field_set.compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true
    @class.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      card: Analytics::MetricsRecorder.card_attributes(@card),
      actor: Analytics::MetricsRecorder.user_attributes(@author),
      timestamp: timestamp,
      event: card_create,
      additional_data: {
        platform: 'web',
        user_agent: 'Edcast iPhone',
        is_admin_request: 0
      }
    )
   end

   test "stores is_user_generated=0 if card has ecl data" do
    InfluxdbRecorder.unstub(:record)
    card_create = @class::EVENT_CARD_CREATED
    attrs = @card.attributes
    # NOTE: Since author_id is nil, the event pushed here is 'card_created_virtual'
    @card.stubs(:attributes).returns(attrs.except("author_id", "author"))
    @card.stubs(:author_id)
    @card.stubs(:author)
    field_set = card_field_set(@org, @card, @author)
      .except(:card_author_id)
      .except(:card_author_full_name)
      .compact
    tag_set = card_tag_set(@card, @author, card_create)
      .merge(event: "card_created_virtual")
      .except(:card_author_id)
      .compact
    assert_equal(field_set[:is_user_generated], '0')
    timestamp = Time.now.to_i
    InfluxdbRecorder.expects(:record).with('cards',
      {
        values: field_set,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true
    @class.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      card: Analytics::MetricsRecorder.card_attributes(@card),
      actor: Analytics::MetricsRecorder.user_attributes(@author),
      timestamp: timestamp,
      event: 'card_created',
      additional_data: {
        platform: 'web',
        user_agent: 'Edcast iPhone',
        is_admin_request: 0
      }
    )
   end

   test "logs an error and doesnt record if card and user dont share an org" do
    other_org = create :organization
    other_user = create :user, organization: other_org

    InfluxdbRecorder.unstub(:record)
    InfluxdbRecorder.expects(:record).never

    error_msg = (
      "ORG ID MISMATCH: " +
      "card id = #{@card.id}, " +
      "card org id = #{@card.organization_id}, " +
      "actor org id = #{other_user.organization_id}"+
      "event = card_created"
    )

    log.expects(:error).with(error_msg)

    @class.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      card: Analytics::MetricsRecorder.card_attributes(@card),
      actor: Analytics::MetricsRecorder.user_attributes(other_user),
      timestamp: Time.now.to_i,
      event: 'card_created',
      additional_data: {
        platform: 'web',
        user_agent: 'Edcast iPhone',
        is_admin_request: 0
      }
    )
   end


  test 'should record card event' do
    InfluxdbRecorder.unstub(:record)
    card_create = @class::EVENT_CARD_CREATED

    field_set = card_field_set(@org, @card, @author)
    tag_set = card_tag_set(@card, @author, card_create)
    timestamp = Time.now.to_i

    InfluxdbRecorder.expects(:record).with('cards',
      {
        values: field_set.compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true
    @class.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      card: Analytics::MetricsRecorder.card_attributes(@card),
      actor: Analytics::MetricsRecorder.user_attributes(@author),
      timestamp: timestamp,
      event: card_create,
      additional_data: {
        platform: 'web',
        user_agent: 'Edcast iPhone',
        is_admin_request: 0
      }
    )
  end

  test 'valid registered events' do
    registered_events = {
      "card_created"                      => true,
      "card_created_virtual"              => true,
      "card_deleted"                      => true,
      "card_promoted"                     => true,
      "card_unpromoted"                   => true,
      "card_marked_as_complete"           => true,
      "card_marked_as_uncomplete"         => true,
      "card_video_stream_stopped_viewing" => true,
      "card_video_stream_started_viewing" => true,
      "card_video_stream_downloaded"      => true,
      "card_video_stream_previewed"       => true,
      "card_added_to_pathway"             => true,
      "card_added_to_journey"             => true,
      "card_removed_from_pathway"         => true,
      "card_removed_from_journey"         => true,
      "card_published"                    => true,
      "card_dismissed"                    => true,
      "card_poll_response_created"        => true,
      "card_quiz_response_created"        => true,
      "card_added_to_career_advisor"      => true,
      "card_removed_from_career_advisor"  => true
    }
    assert_equal(
      registered_events,
      @class::VALID_EVENTS
    )
  end

  test 'card_events' do
    expected = %w{
      card_created
      card_created_virtual
      card_deleted
      card_promoted
      card_unpromoted
      card_marked_as_complete
      card_marked_as_uncomplete
      card_viewed
      card_source_visited
      card_video_stream_stopped_viewing
      card_video_stream_started_viewing
      card_video_stream_downloaded
      card_video_stream_previewed
      card_added_to_pathway
      card_removed_from_pathway
      card_published
      card_dismissed
      card_poll_response_created
      card_quiz_response_created
      card_added_to_journey
      card_removed_from_journey
      card_comment_created
      card_comment_deleted
      card_approved_for_channel
      card_rejected_for_channel
      card_added_to_channel
      card_removed_from_channel
      card_edited
      card_assigned
      card_assignment_dismissed
      card_liked
      card_unliked
      card_bookmarked
      card_unbookmarked
      card_relevance_rated
      card_shared
      card_pinned
      card_unpinned
      card_added_to_career_advisor
      card_edited_from_career_advisor
      card_removed_from_career_advisor
    }
    actual = @class.card_events
    assert_equal expected.sort, actual.sort
  end

  test 'card measurement name' do
    assert_equal 'cards', @class::MEASUREMENT
  end

  test 'should record pathway event' do
    InfluxdbRecorder.unstub(:record)
    INFLUXDB_CLIENT.stubs(:write_point)
    card_create = @class::EVENT_CARD_CREATED
    pathway = create(:card,
      card_type: 'pack',
      card_subtype: 'simple',
      title: "pathway title"
    )
    field_set = card_field_set(@org, @card, @author)
    field_set[:pathway_id] = pathway.id.to_s
    field_set[:pathway_name] = pathway.title
    tag_set = card_tag_set(@card, @author, card_create)
    timestamp = Time.now.to_i

    InfluxdbRecorder.expects(:record).with('cards',
      {
        values: field_set.compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true

    # TODO fix this so it doesn't require anything special in additional_data
    @class.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      card: Analytics::MetricsRecorder.card_attributes(@card),
      actor: Analytics::MetricsRecorder.user_attributes(@author),
      timestamp: timestamp,
      event: card_create,
      additional_data: {
        platform: 'web',
        user_agent: 'Edcast iPhone',
        pathway_id: pathway.id,
        pathway_name: pathway.title,
        is_admin_request: 0
      }
    )
  end


  test 'should record journey event' do
    InfluxdbRecorder.unstub(:record)
    INFLUXDB_CLIENT.stubs(:write_point)
    card_added_to_journey = @class::EVENT_CARD_ADDED_TO_JOURNEY

    # prepare section
    pathway_card = create(:card, card_type: 'pack', card_subtype: 'simple', author: @author)
    CardPackRelation.add cover_id: pathway_card.id, add_id: @card.id

    # Add section to pathway
    journey_card = create(:card,
      card_type: 'journey',
      card_subtype: 'self_paced',
      author: @author,
      title: "journey title"
    )

    JourneyPackRelation.add cover_id: journey_card.id, add_id: pathway_card.id

    field_set = card_field_set(@org, pathway_card, @author)
    field_set[:journey_id] = journey_card.id.to_s
    field_set[:journey_name] = journey_card.title
    tag_set = card_tag_set(pathway_card, @author, card_added_to_journey)
    timestamp = Time.now.to_i

    InfluxdbRecorder.expects(:record).with('cards',
      {
        values: field_set.compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true

    @class.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      card: Analytics::MetricsRecorder.card_attributes(pathway_card),
      actor: Analytics::MetricsRecorder.user_attributes(@author),
      timestamp: timestamp,
      event: card_added_to_journey,
      additional_data: {
        platform: 'web',
        user_agent: 'Edcast iPhone',
        journey_id: journey_card.id,
        journey_name: journey_card.title,
        is_admin_request: 0
      }
    )
  end

  test 'should record event for ecl-cards' do
    InfluxdbRecorder.unstub :record
    card_like = 'card_liked'
    viewer = create(:user, organization: @card.organization)
    card = create :card, {
      message: 'ecl_card',
      ecl_id: 'ECL-123kjl',
      author: nil,
      organization_id: @card.organization.id,
      ecl_source_name: "fake source"
    }
    timestamp = Time.now.to_i
    # Card author id is not included if it's nil (for ECL cards)
    field_set = card_field_set(@org, card, viewer).except(:card_author_id)
    tag_set = card_tag_set(card, viewer, card_like)
    assert_equal(field_set[:ecl_source_name], "fake source")
    InfluxdbRecorder.expects(:record).with('cards',
      {
        values: field_set.compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true
    @class.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      card: Analytics::MetricsRecorder.card_attributes(card),
      event: card_like,
      actor: Analytics::MetricsRecorder.user_attributes(viewer),
      timestamp: timestamp,
      additional_data: {
        platform: 'web',
        user_agent: 'Edcast iPhone',
        is_admin_request: 0
      }
    )
  end

  test 'should record card_created_virtual event for cards with no author_id' do
    InfluxdbRecorder.unstub :record
    viewer = create(:user, organization: @card.organization)
    card = create :card, {
      message: 'ecl_card',
      author: nil,
      organization_id: @card.organization.id,
    }
    timestamp = Time.now.to_i
    # Card author id is not included if it's nil (for ECL cards)
    field_set = card_field_set(@org, card, viewer).except(:card_author_id)
    tag_set = card_tag_set(card, viewer, 'card_created_virtual')
    InfluxdbRecorder.expects(:record).with('cards',
      {
        values: field_set.compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true
    @class.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      card: Analytics::MetricsRecorder.card_attributes(card),
      event: 'card_created',
      actor: Analytics::MetricsRecorder.user_attributes(viewer),
      timestamp: timestamp,
      additional_data: {
        platform: 'web',
        user_agent: 'Edcast iPhone',
        is_admin_request: 0
      }
    )
  end
  
  test "should push to influx card_added_to_career_advisor  event when career advisor created" do
    InfluxdbRecorder.unstub :record
    author          = create(:user, organization: @org, organization_role: 'admin')
    viewer          = create(:user, organization: @card.organization)
    career_advisor  = create(:career_advisor, organization: @org)
    course_card     = create(:card, organization: @org, card_type: 'course', author_id: author.id)

    auth_api_info   = {"auth_required":true, "url":" asdsa ", "method":"POST", "host":"asdsa das"}

    career_advisor_card = CareerAdvisorCard.create(
      id:             10000001,
      card:           course_card, 
      career_advisor: career_advisor, 
      level:          "beginner"
    )
    timestamp       = Time.now.to_i
    metadata        = { user_id: viewer.id }   
    field_set       = card_field_set(@org, course_card, viewer)
    tag_set         = card_tag_set(course_card, viewer, 'card_added_to_career_advisor') 
    values          = field_set
    values.merge!({
      career_advisor_card_id:    career_advisor_card.id.to_s, 
      career_advisor_id:         career_advisor_card.career_advisor_id.to_s, 
      career_advisor_card_level: career_advisor_card.level
    })

    InfluxdbRecorder.expects(:record).with('cards',
      {
        values:     values.compact,
        tags:       tag_set.compact,
        timestamp:  timestamp
      },
      {
        write_client:     nil,
        duplicate_check:  true
      }
    ).returns true
    @class.record(
      org:                  Analytics::MetricsRecorder.org_attributes(@org),
      card:                 Analytics::MetricsRecorder.card_attributes(course_card),
      event:                'card_added_to_career_advisor',
      actor:                Analytics::MetricsRecorder.user_attributes(viewer),
      career_advisor_card:  Analytics::MetricsRecorder.career_advisor_card_attributes(career_advisor_card),
      timestamp:            timestamp,
      additional_data:  {
        platform: 'web',
        user_agent: 'Edcast iPhone',
        is_admin_request: 0
      }
    )   
  end

end

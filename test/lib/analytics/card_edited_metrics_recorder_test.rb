require 'test_helper'

class Analytics::CardEditedMetricsRecorderTest < ActiveSupport::TestCase

  test 'it calls InfluxdbRecorder with the correct args' do
    InfluxdbRecorder.unstub :record
    org = create :organization
    actor = create :user, organization: org
    author = create :user, organization: org
    card = create(:card,
      organization: org,
      author: author,
      title: "foo"
    )
    timestamp = Time.now.to_i
    event = "card_edited"
    metadata = {
      user_agent: "user_agent",
      platform: "platform",
      is_admin_request: 0
    }

    field_set = card_field_set(
      org, card, actor
    )

    tag_set = card_tag_set(
      card, actor, event
    )

    InfluxdbRecorder.expects(:record).with('cards',
      {
        values: field_set.symbolize_keys.merge(
          {
            user_agent: "user_agent",
            changed_column: "title",
            old_val: "foo",
            new_val: "2",
            platform: 'platform'
          }
        ).compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true

    Analytics::CardEditedMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(card.organization),
      card: Analytics::MetricsRecorder.card_attributes(card),
      timestamp: timestamp,
      event: event,
      changed_column: "title",
      old_val: "foo",
      new_val: 2, # will be converted to string
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      additional_data: metadata
    )
  end

  test 'records card_edited event for whitelisted columns' do
    whitelist = %w{foo bar}
    Analytics::CardEditedMetricsRecorder.stubs(:whitelisted_change_attrs)
                                  .returns(whitelist)
    do_record = -> (changed_col) {
      Analytics::CardEditedMetricsRecorder.record(
        org: {},
        card: {},
        actor: {},
        event: "card_edited",
        changed_column: changed_col,
        old_val: 0,
        new_val: 1,
        additional_data: {},
        timestamp: 0
      )
    }
    whitelist.each do |changed_col|
      Analytics::CardMetricsRecorder.expects(:record)
      do_record.call(changed_col)
    end
  end


  test 'doesnt record card_edited event for non-whitelisted columns' do
    whitelist = %w{foo bar}
    test_columns = %w{asd bsd}
    Analytics::CardEditedMetricsRecorder.stubs(:whitelisted_change_attrs)
                                  .returns(whitelist)
    do_record = -> (changed_col) {
      Analytics::CardEditedMetricsRecorder.record(
        org: {},
        card: {},
        actor: {},
        event: "card_edited",
        changed_column: changed_col,
        old_val: 0,
        new_val: 1,
        additional_data: {},
        timestamp: 0
      )
    }
    Analytics::CardMetricsRecorder.expects(:record).never
    test_columns.each do |changed_col|
      do_record.call(changed_col)
    end
  end

  test 'whitelisted_change_attrs' do
    assert_equal(
      %w{
        author_id
        hidden
        published_at
        resource_id
        title
        state
        message
        is_official
        card_type
        card_subtype
        ecl_id
        provider
        duration
        is_paid
        filestack
        readable_card_type
        is_public
        language
        level
      },
      Analytics::CardEditedMetricsRecorder.whitelisted_change_attrs
    )
  end

  test 'is_ignored_event?' do
    whitelist = %w{foo bar}
    Analytics::CardEditedMetricsRecorder.stubs(:whitelisted_change_attrs)
                                    .returns(whitelist)
    assert Analytics::CardEditedMetricsRecorder.is_ignored_event? "asd"
    refute Analytics::CardEditedMetricsRecorder.is_ignored_event? "foo"
    refute Analytics::CardEditedMetricsRecorder.is_ignored_event? nil
  end

  test "should push to influx card_edited_from_career_advisor  event when career advisor edited" do
    InfluxdbRecorder.unstub :record
    org             = create :organization
    author          = create(:user, organization: org, organization_role: 'admin')
    viewer          = create(:user, organization: org)
    career_advisor  = create(:career_advisor, organization: org)
    course_card     = create(:card, organization: org, card_type: 'course', author_id: author.id)

    auth_api_info   = {"auth_required":true, "url":" asdsa ", "method":"POST", "host":"asdsa das"}

    career_advisor_card = CareerAdvisorCard.create(
      id:             10000001,
      card:           course_card, 
      career_advisor: career_advisor, 
      level:          "beginner"
    )
    career_advisor_card.update( level: "intermeditate") 
    timestamp     = Time.now.to_i
    metadata      = { user_id: viewer.id }   
    field_set     = card_field_set(org, course_card, viewer)
    tag_set       = card_tag_set(course_card, viewer, 'card_edited_from_career_advisor') 
    values        = field_set
    values.merge!({
      career_advisor_card_id:    career_advisor_card.id.to_s, 
      career_advisor_id:         career_advisor_card.career_advisor_id.to_s, 
      career_advisor_card_level: career_advisor_card.level,
      changed_column:             'level',
      old_val:                    "beginner",
      new_val:                    "intermediate"
    })

    InfluxdbRecorder.expects(:record).with('cards',
      {
        values:     values.compact,
        tags:       tag_set.compact,
        timestamp:  timestamp
      },
      {
        write_client:     nil,
        duplicate_check:  true
      }
    ).returns true

    Analytics::CardEditedMetricsRecorder.record(
      org:                  Analytics::MetricsRecorder.org_attributes(org),
      card:                 Analytics::MetricsRecorder.card_attributes(course_card),
      event:                'card_edited_from_career_advisor',
      actor:                Analytics::MetricsRecorder.user_attributes(viewer),
      career_advisor_card:  Analytics::MetricsRecorder.career_advisor_card_attributes(career_advisor_card),
      changed_column:       'level',
      old_val:              "beginner",
      new_val:              "intermediate",
      timestamp:            timestamp,
      additional_data:      {
        platform:         'web',
        user_agent:       'Edcast iPhone',
        is_admin_request: 0
      }
    )   
  end

end
require 'test_helper'

class Analytics::UserScoreRecorderTest < ActionController::TestCase

  setup do
    @recorder = Analytics::UserScoreRecorder
    @event = "stub_event"
    @actor_id = 5
    @owner_id = 6

    # This is an except of the payload for another event.
    # In reality the whole payload is passed to UserScoreRecorder,
    # but only some of this data is required for the tests.
    @stub_data = {
      tags: {
        "craftsman_id" => @owner_id,
      },
      values: {
        "buyer_id" => @actor_id
      }
    }

    @stub_scores_table = {
      "scores" => {
        "owner" => 1,
        "actor" => 2,
        "actor_and_owner" => 3
      },
      "refs" => {
        "owner_id" => ["tags", "craftsman_id"],
        "actor_id" => ["values", "buyer_id"]
      }
    }
  end

  test 'SCORES_TABLE' do
    path = File.join(Rails.root, "config", "analytics", "event_scores.yml")
    table = YAML.load File.read path

    assert_equal table.class, Hash
    assert_equal Analytics::UserScoreRecorder::SCORES_TABLE, table
    assert table.keys.length > 0

    # Typecheck the actual file contents.
    table.each do |name, val|
      assert name.is_a? String
      assert val.is_a? Hash
      assert val.has_key? "scores"
      val["scores"].each do |role, int|
        assert role.is_a? String
        assert int.is_a? Integer
      end
    end
  end

  test '.determine_score' do
    @recorder.stubs(:scores_table).returns(@event => @stub_scores_table)

    @stub_scores_table["scores"].each do |role, score|
      assert_equal score, @recorder.determine_score(
        event: @event,
        role: role
      )
    end

    error = assert_raises KeyError do
      @recorder.determine_score(
        event: "unknown_event",
        role: "actor"
      )
    end
    assert_equal error.message, "key not found: \"unknown_event\""

    error = assert_raises KeyError do
      @recorder.determine_score(
        event: @event,
        role: "unknown_role"
      )
    end
    assert_equal error.message, "key not found: \"unknown_role\""

  end

  test '.recording_targets with actor and owner returns 1 target' do
    @recorder.stubs(:scores_table).returns(@event => @stub_scores_table)
    targets = @recorder.recording_targets(
      event: @event,
      actor_id: 1,
      owner_id: 1
    )
    assert_equal [
      {
        actor_id: 1,
        owner_id: 1,
        user_id: 1,
        role: "actor_and_owner",
        score_value: 3
      }
    ], targets
  end

  test '.recording_targets with owner and nil actor returns 1 target' do
    @recorder.stubs(:scores_table).returns(@event => @stub_scores_table)
    targets = @recorder.recording_targets(
      event: @event,
      actor_id: nil,
      owner_id: 1
    )
    assert_equal [
      {
        actor_id: nil,
        owner_id: 1,
        user_id: 1,
        role: "owner",
        score_value: 1
      }
    ], targets
  end

  test '.recording_targets with actor and nil owner returns 1 target' do
    @recorder.stubs(:scores_table).returns(@event => @stub_scores_table)
    targets = @recorder.recording_targets(
      event: @event,
      actor_id: 1,
      owner_id: nil
    )
    assert_equal [
      {
        actor_id: 1,
        owner_id: nil,
        user_id: 1,
        role: "actor",
        score_value: 2
      }
    ], targets
  end

  test '.recording_targets with actor and owner returns 2 targets' do
    @recorder.stubs(:scores_table).returns(@event => @stub_scores_table)
    targets = @recorder.recording_targets(
      event: @event,
      actor_id: 1,
      owner_id: 2
    )
    assert_equal [
      {
        actor_id: 1,
        owner_id: 2,
        user_id: 1,
        role: "actor",
        score_value: 2
      },
      {
        actor_id: 1,
        owner_id: 2,
        user_id: 2,
        role: "owner",
        score_value: 1
      }
    ], targets
  end

  test '.record records user score measurement' do
    @recorder.stubs(:scores_table).returns(@event => @stub_scores_table)
    timestamp = Time.now.to_i
    InfluxdbRecorder.unstub :record
    InfluxdbRecorder.expects(:record).with( "user_scores",
      {
        timestamp: timestamp,
        tags: {
          event: @event,
          role: "actor_and_owner",
          user_id: '1',
          org_id: '2'
        },
        values: {
          actor_id: '1',
          owner_id: '1',
          score_value: 3
        }
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    )

    # This is the expected public usage.
    # .recording_targets returns a single target in this case.
    targets = @recorder.recording_targets(
      event: @event,
      actor_id: 1,
      owner_id: 1
    )
    targets.each do |target|
      @recorder.record target.merge(
        org_id: 2,
        event: @event,
        timestamp: timestamp
      )
    end
  end

  test '.find_actor_and_owner_ids depends on state of scores table' do
    @recorder.stubs(:scores_table).returns(@event => @stub_scores_table)

    # The passed :event must be present as a top-level key
    error = assert_raises KeyError do
      @recorder.find_actor_and_owner_ids(
        "unknown_event",
        RecursiveOpenStruct.new
      )
    end
    assert_equal error.message, "key not found: \"unknown_event\""

    # The value at that :event key must have a "refs" key
    refs = @stub_scores_table.delete "refs"
    error = assert_raises KeyError do
      @recorder.find_actor_and_owner_ids(@event, RecursiveOpenStruct.new)
    end
    assert_equal error.message, "key not found: \"refs\""

    # bring the refs back
    @stub_scores_table["refs"] = refs

    # test the actual return value
    assert_equal(
      @recorder.find_actor_and_owner_ids(
        @event,
        RecursiveOpenStruct.new(@stub_data)
      ),
      [@actor_id, @owner_id]
    )

  end

end

require 'test_helper'
class CardPinRecorderTest < ActiveSupport::TestCase

  setup do
    author_first_name = Faker::Name.first_name
    actor_first_name = Faker::Name.first_name

    author_last_name = Faker::Name.last_name
    actor_last_name = Faker::Name.last_name

    @org = create(:organization, name: 'LXP', host_name: 'spark' )
    @channel = create(:channel, organization: @org)
    @author = create(:user, first_name: author_first_name, last_name: author_last_name, organization: @org)
    @actor = create(:user, first_name: actor_first_name, last_name: actor_last_name, organization: @org)
    @card = create(:card, author: @author, created_at: 2.days.ago)

    @pin = create(:pin, pinnable: @channel, user: @actor, object: @card)
  end

  test 'valid registered events' do
    registered_events = ['card_pinned', 'card_unpinned']
    assert_equal registered_events, Analytics::CardPinRecorder::CARD_PIN_EVENTS
  end

  test 'valid record args' do
    invalid_event = 'invalid event'
    InfluxdbRecorder.stubs(:record).returns :true

    assert_nothing_raised do
      Analytics::CardPinRecorder.record(
        org: Analytics::MetricsRecorder.org_attributes(@org),
        pin_id: @pin.id,
        card: Analytics::MetricsRecorder.card_attributes(@pin.object),
        channel: Analytics::MetricsRecorder.channel_attributes(@pin.pinnable),
        actor: Analytics::MetricsRecorder.user_attributes(@pin.user),
        timestamp: Time.now.to_i,
        event: 'card_pinned',
        additional_data: {}
    )
      Analytics::CardPinRecorder.record(
        org: Analytics::MetricsRecorder.org_attributes(@org),
        pin_id: @pin.id,
        card: Analytics::MetricsRecorder.card_attributes(@pin.object),
        channel: Analytics::MetricsRecorder.channel_attributes(@pin.pinnable),
        actor: Analytics::MetricsRecorder.user_attributes(@pin.user),
        timestamp: Time.now.to_i,
        event: 'card_unpinned',
        additional_data: {}
    )
    end

    assert_raises do
      Analytics::CardPinRecorder.record(
        org: Analytics::MetricsRecorder.org_attributes(@org),
        pin_id: @pin.id,
        card: Analytics::MetricsRecorder.card_attributes(@pin.object),
        channel: Analytics::MetricsRecorder.channel_attributes(@pin.pinnable),
        actor: Analytics::MetricsRecorder.user_attributes(@pin.user),
        timestamp: Time.now.to_i,
        event: invalid_event,
        additional_data: {}
    )
    end
  end

  test 'should record card vote event' do
    InfluxdbRecorder.unstub(:record)
    card_pin = Analytics::CardPinRecorder::EVENT_CARD_PINNED
    timestamp = Time.now.to_i

    field_set = card_field_set(
      @org, @card, @actor
    )

    tag_set = card_tag_set(
      @card, @actor, card_pin
    )

    InfluxdbRecorder.expects(:record).with('cards',
      {
        values: field_set.symbolize_keys.merge(
          {
            platform: "ios",
            pin_id: @pin.id.to_s,
            channel_id: @channel.id.to_s,
            channel_name: @channel.label,
            is_channel_featured: '0',
            is_channel_curated: '0',
            is_channel_public: '1'
          }
        ).compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true

    Analytics::CardPinRecorder.record(
        org: Analytics::MetricsRecorder.org_attributes(@org),
        pin_id: @pin.id,
        card: Analytics::MetricsRecorder.card_attributes(@pin.object),
        channel: Analytics::MetricsRecorder.channel_attributes(@pin.pinnable),
        actor: Analytics::MetricsRecorder.user_attributes(@pin.user),
        timestamp: timestamp,
        event: card_pin,
        additional_data: {
            platform: 'ios',
            user_agent: 'Edcast iPhone',
            is_admin_request: 0
        }
    )
  end
end
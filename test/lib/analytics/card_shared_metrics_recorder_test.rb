require 'test_helper'


class Analytics::CardSharedMetricsRecorderTest < ActiveSupport::TestCase

  setup do
    @actor = create(:user)
    @org = @actor.organization
    @card = create(:card,
      organization: @org,
      author: create(:user, organization: @org),
      title: "foo"
    )
    @timestamp = Time.now.to_i
    @event = "card_shared"
    @metadata = {
      user_agent: "user_agent",
      platform: "platform",
      is_admin_request: 0
    }
    InfluxdbRecorder.unstub :record
    @field_set = card_field_set(@org, @card, @actor)
    @tag_set = card_tag_set(@card, @actor, @event)
  end

  test 'with team_id given, calls InfluxdbRecorder with the correct args' do
    team_id = 1
    InfluxdbRecorder.expects(:record).with('cards', 
      {
        values: @field_set.symbolize_keys.merge(
          {
            user_agent: "user_agent",
            platform: "platform",
            team_id: team_id.to_s
          }
        ).compact,
        tags: @tag_set.compact,
        timestamp: @timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true

    Analytics::CardSharedMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(@card.organization),
      card: Analytics::MetricsRecorder.card_attributes(@card),
      timestamp: @timestamp,
      event: @event,
      team_id: team_id,
      actor: Analytics::MetricsRecorder.user_attributes(@actor),
      additional_data: @metadata
    )
  end

  test 'with team_id absent, calls InfluxdbRecorder with the correct args' do
    InfluxdbRecorder.expects(:record).with('cards',
      {
        values: @field_set.symbolize_keys.merge(
          {
            user_agent: "user_agent",
            platform: "platform",
          }
        ).compact,
        tags: @tag_set.compact,
        timestamp: @timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true

    Analytics::CardSharedMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(@card.organization),
      card: Analytics::MetricsRecorder.card_attributes(@card),
      timestamp: @timestamp,
      event: @event,
      actor: Analytics::MetricsRecorder.user_attributes(@actor),
      additional_data: @metadata
    )
  end

  test 'includes shared_to_user_id if shared_to_user given' do
    shared_to_user = create :user, organization: @org
    InfluxdbRecorder.expects(:record).with('cards',
      {
        values: @field_set.symbolize_keys.merge(
          {
            user_agent: "user_agent",
            platform: "platform",
            shared_to_user_id: shared_to_user.id.to_s
          }
        ).compact,
        tags: @tag_set.compact,
        timestamp: @timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true

    Analytics::CardSharedMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(@card.organization),
      card: Analytics::MetricsRecorder.card_attributes(@card),
      timestamp: @timestamp,
      event: @event,
      actor: Analytics::MetricsRecorder.user_attributes(@actor),
      shared_to_user: Analytics::MetricsRecorder.user_attributes(shared_to_user),
      additional_data: @metadata
    )
  end  

end
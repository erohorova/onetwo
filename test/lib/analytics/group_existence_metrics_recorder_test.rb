
require 'test_helper'
class Analytics::GroupExistenceMetricsRecorderTest < ActiveSupport::TestCase

  recorder = Analytics::GroupExistenceMetricsRecorder

  test "ValidEvents" do
    assert_equal recorder::VALID_EVENTS, %w{
      group_created group_deleted
    }

    assert_equal(
      recorder,
      Analytics::MetricsRecorder.get_recorder_for_event('group_created')
    )

    assert_equal(
      recorder,
      Analytics::MetricsRecorder.get_recorder_for_event('group_deleted')
    )

  end

  test ".record with a valid event records the metric" do
    org = create(:organization)
    group = create :team, organization: org, name: "foo"
    user = create(:user, organization: org)
    group_user = TeamsUser.create user: user, team: group, as_type: "admin"
    timestamp = Time.now.to_i
    stub_platform = "fake platform"
    stub_version_number = "fake version number"
    stub_user_agent = "fake user agent"
    event = "group_created"
    recorder.expects(:influxdb_record).with(
      "groups",
      {
        values: {
          group_name: group.name,
          org_name: org.name,
          user_id: user.id.to_s,
          user_handle: user.handle,
          group_id: group.id.to_s,
          user_agent: stub_user_agent,
          platform_version_number: stub_version_number,
          org_hostname: org.host_name,
          is_admin_request: '0',
          user_full_name: user&.full_name,
          platform: stub_platform,
        },
        tags: {
          is_system_generated: '0',
          _user_id: user.id.to_s,
          _group_id: group.id.to_s,
          org_id: org.id.to_s,
          event: event,
        },
        timestamp: timestamp
      }
    )
    recorder.record(
      org: Analytics::MetricsRecorder.org_attributes(group.organization),
      group: Analytics::MetricsRecorder.team_attributes(group),
      actor: Analytics::MetricsRecorder.user_attributes(user),
      timestamp: timestamp,
      event: event,
      additional_data: {
        platform: stub_platform,
        user_agent: stub_user_agent,
        platform_version_number: stub_version_number,
        is_admin_request: 0
      }
    )
  end

  test ".record with an invalid event raises an error" do
    err = assert_raises StandardError do
      recorder.record(
        org: {},
        group: {},
        actor: {},
        timestamp: Time.now.to_i,
        event: "fake",
        additional_data: {}
      )
    end
    assert_match /Event is not registered/, err.message
  end

end

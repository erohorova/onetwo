require 'test_helper'
class Analytics::GroupMembershipMetricsRecorderTest < ActiveSupport::TestCase
  setup do
    @org = create(:organization, name: 'LXP', host_name: 'spark' )
    @actor = create :user, organization: @org
    @team = create(:team, organization: @org)
    @member = create(:user, organization: @org)
  end

  test 'should record group event' do
    @team.add_member(@member)
    InfluxdbRecorder.unstub(:record)
    INFLUXDB_CLIENT.stubs(:write_point)
    team_member = @team.teams_members.where(user: @member).first

    add_to_group = Analytics::GroupMembershipMetricsRecorder::EVENT_GROUP_USER_ADDED

    field_set = {group_id: @team.id.to_s, group_name: @team.name}
    field_set[:user_id] = @actor.id.to_s

    field_set[:group_user_id] = @member.id.to_s
    field_set[:org_name] = @team.organization.name
    field_set[:group_user_role] = 'member'
    field_set[:user_agent] = 'Edcast iPhone'
    field_set[:is_admin_request] = '0'
    field_set[:org_hostname] = @team.organization.host_name
    field_set[:user_full_name] = @actor.try(:full_name)
    field_set[:user_handle] = @actor.handle
    field_set[:platform] = 'web'

    tag_set = {_user_id: @actor.id.to_s, _group_id: @team.id.to_s}
    tag_set[:is_system_generated] = '0'
    tag_set[:org_id] = @team.organization_id.to_s
    tag_set[:event] = add_to_group

    timestamp = team_member.created_at.to_i

    InfluxdbRecorder.expects(:record).with('groups',
      {
        values: field_set.compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true
    Analytics::GroupMembershipMetricsRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      actor: Analytics::MetricsRecorder.user_attributes(@actor),
      group: Analytics::MetricsRecorder.team_attributes(@team),
      member: Analytics::MetricsRecorder.user_attributes(@member),
      role: team_member.as_type,
      event: add_to_group,
      timestamp: timestamp,
      additional_data: {
        platform: 'web',
        user_agent: 'Edcast iPhone',
        is_admin_request: 0
      }
    )
  end

  test 'valid registered events' do
    registered_events = {"group_user_added" => true, "group_user_removed" => true, "group_user_edited" => true}
    assert_same_elements registered_events, Analytics::GroupMembershipMetricsRecorder::VALID_EVENTS
  end
end

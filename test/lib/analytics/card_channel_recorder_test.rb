require 'test_helper'
class CardChannelRecorderTest < ActiveSupport::TestCase

  setup do
    author_first_name = Faker::Name.first_name
    actor_first_name = Faker::Name.first_name

    author_last_name = Faker::Name.last_name
    actor_last_name = Faker::Name.last_name

    @org = create(:organization, name: 'LXP', host_name: 'spark' )
    @channel = create(:channel, organization: @org)
    @author = create :user,
      first_name: author_first_name,
      last_name: author_last_name,
      organization: @org
    @actor = create :user,
      first_name: actor_first_name,
      last_name: actor_last_name,
      organization: @org
    @card = create(:card, author: @author, created_at: 2.days.ago)
    @card.channels << @channel

    @channels_card = @card.channels_cards.first
  end

  test 'valid registered events' do
    registered_events = ['card_added_to_channel', 'card_removed_from_channel']
    assert_equal registered_events, Analytics::CardChannelRecorder::CARD_CHANNEL_EVENTS
  end

  test 'valid record args' do
    invalid_event = 'invalid event'
    InfluxdbRecorder.stubs(:record).returns :true

    assert_nothing_raised do
      Analytics::CardChannelRecorder.record(
        org: Analytics::MetricsRecorder.org_attributes(@org),
        channel: Analytics::MetricsRecorder.channel_attributes(@channel),
        card: Analytics::MetricsRecorder.card_attributes(@card),
        actor: Analytics::MetricsRecorder.user_attributes(@actor),
        timestamp: @channels_card.created_at,
        event: 'card_added_to_channel'
      )
      Analytics::CardChannelRecorder.record(
        org: Analytics::MetricsRecorder.org_attributes(@org),
        channel: Analytics::MetricsRecorder.channel_attributes(@channel),
        card: Analytics::MetricsRecorder.card_attributes(@card),
        actor: Analytics::MetricsRecorder.user_attributes(@actor),
        timestamp: @channels_card.created_at,
        event: 'card_removed_from_channel'
      )
    end

    assert_raises do
      Analytics::CardChannelRecorder.record(
        org: Analytics::MetricsRecorder.org_attributes(@org),
        channel: Analytics::MetricsRecorder.channel_attributes(@channel),
        card: Analytics::MetricsRecorder.card_attributes(@card),
        timestamp: @channels_card.created_at,
        event: invalid_event
      )
    end
  end

  test 'should record card channel event' do
    InfluxdbRecorder.unstub :record
    add_to_channel = Analytics::CardChannelRecorder::EVENT_CARD_ADDED_TO_CHANNEL
    timestamp = @channels_card.created_at.to_i

    field_set = card_field_set(
      @org, @card, @actor
    )

    tag_set = card_tag_set(
      @card, @actor, add_to_channel
    )

    InfluxdbRecorder.expects(:record).with('cards',
      {
        values: field_set.symbolize_keys.merge(
          {
            is_channel_curated: '0',
            is_channel_public: '1',
            channel_id: @channel.id.to_s,
            channel_name: @channel.label,
            is_channel_featured: '0',
            platform_version_number: 10.5,
            platform: 'ios'
          }
        ).compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true

    Analytics::CardChannelRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      channel: Analytics::MetricsRecorder.channel_attributes(@channel),
      card: Analytics::MetricsRecorder.card_attributes(@card),
      actor: Analytics::MetricsRecorder.user_attributes(@actor),
      timestamp: @channels_card.created_at,
      event: add_to_channel,
      additional_data: {
        platform: 'ios',
        platform_version_number: 10.5,
        user_agent: 'Edcast iPhone',
        is_admin_request: 0
      }
    )
  end

  test 'should record card channel event #some other user' do
    InfluxdbRecorder.unstub :record
    add_to_channel = Analytics::CardChannelRecorder::EVENT_CARD_ADDED_TO_CHANNEL
    timestamp = @channels_card.created_at.to_i

    field_set = card_field_set(
      @org, @card, @actor
    )

    tag_set = card_tag_set(
      @card, @actor, add_to_channel
    )

    InfluxdbRecorder.expects(:record).with('cards',
      {
        values: field_set.symbolize_keys.merge({
          channel_name: @channel.label,
          is_channel_curated: '0',
          channel_id: @channel.id.to_s,
          is_channel_public: '1',
          is_channel_featured: '0',
          platform: 'ios'
        }).compact,
        tags: tag_set.compact,
        timestamp: timestamp
      },
      {
        write_client: nil,
        duplicate_check: true
      }
    ).returns true

    Analytics::CardChannelRecorder.record(
      org: Analytics::MetricsRecorder.org_attributes(@org),
      channel: Analytics::MetricsRecorder.channel_attributes(@channel),
      card: Analytics::MetricsRecorder.card_attributes(@card),
      timestamp: @channels_card.created_at,
      event: add_to_channel,
      actor: Analytics::MetricsRecorder.user_attributes(@actor),
      additional_data: {
        platform: 'ios',
        user_agent: 'Edcast iPhone',
        is_admin_request: 0
      })
  end
end

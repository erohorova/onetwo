require 'test_helper'

class Analytics::GroupScoreAggregationJobTest < ActiveJob::TestCase

  setup do
    @class = Analytics::GroupScoreAggregationRecorder
    @user_cq = Analytics::UserScoreContinuousQueries
  end

  test 'SearchPeriod' do
    assert_equal "2d", @class::SearchPeriod
  end

  test 'Measurement' do
    assert_equal "group_scores_daily", @class::Measurement
  end

  test 'LeaderboardMeasurement' do
    assert_equal "group_user_scores_daily", @class::LeaderboardMeasurement
  end

  test 'LeaderboardMongoKey' do
    assert_equal "group_user_scores_daily", @class::LeaderboardMongoKey
  end

  test 'MinTime is set to the first of january, 2016' do
    assert_equal 1451606400, @class::MinTime
  end

  test '.ref' do
    skip # UNSKIP IF WE ENABLE MONGO
    assert_equal MONGODB_CLIENT[@class::LeaderboardMongoKey], @class.ref
  end

  test '.run! fires off subsequent methods' do
    group_user_scores_daily = "group_user_scores_daily"
    group_score_aggs = "group_score_aggs"
    stub_records = ["insertion_records"]
    @class.expects(:fetch_data).returns [group_user_scores_daily, group_score_aggs]
    @class.expects(:build_insertion_records).with(group_score_aggs).
                                             returns stub_records
    @class.expects(:save_leaderboard!).with(group_user_scores_daily)
    @class.expects(:influxdb_bulk_record).with(@class::Measurement, stub_records)
    assert_equal stub_records.length, @class.run!
  end

  test '.run! exits early if the records length is zero' do
    group_user_scores_daily = "group_user_scores_daily"
    group_score_aggs = "group_score_aggs"
    stub_records = []
    @class.expects(:fetch_data).returns [group_user_scores_daily, group_score_aggs]
    @class.expects(:build_insertion_records).with(group_score_aggs).
                                             returns stub_records
    num_days = @class::SearchPeriod
    log.expects(:warn).with("no user_scores_daily found for last #{num_days}")
    @class.expects(:save_leaderboard!).never
    @class.expects(:influxdb_bulk_record).never
    assert_equal stub_records.length, @class.run!
  end

  test '.fetch_data calls other methods' do
    stubs = {
      user_groups: "user groups",
      user_scores: "user scores",
      group_aggs: "group aggs",
      group_scores_innput: "group_user_scores_daily",
      insertion_records: "insertion records"
    }

    @class.expects(:get_user_group_mappings).
           returns stubs[:user_groups]

    @class.expects(:get_user_scores_aggregations).
           returns stubs[:user_scores]

    @class.expects(:build_group_user_scores_daily).
           with(stubs[:user_groups], stubs[:user_scores]).
           returns(stubs[:group_user_scores_daily])
    @class.expects(:build_scores_by_group).with(stubs[:group_scored_input]).
           returns(stubs[:group_aggs])

    expected = [stubs[:group_user_scores_daily], stubs[:group_aggs]]
    assert_equal expected, @class.fetch_data
  end

  test 'save_leaderboard!' do
    input = "foo"
    # @class.expects(:write_leaderboard_to_mongo_cache!).with(input)
    @class.expects(:write_leaderboard_to_influx!).with(input)
    @class.save_leaderboard!("foo")
  end

  test 'write_leaderboard_to_mongo_cache!' do
    skip # UNSKIP IF WE ENABLE MONGO
    input = "foo"
    record = { tags: {tag: "tag"}, values: { val: "val" }, timestamp: 1 }
    mongo_record = { tag: "tag", val: "val", "time" => 1 }
    @class.expects(:build_insertion_records).with(input).returns [record]
    @class.ref.expects(:insert_many).with([mongo_record])
    @class.write_leaderboard_to_mongo_cache!(input)
  end

  test 'write_leaderboard_to_influx!' do
    input = "input"
    record = { tags: {tag: "tag"}, values: { val: "val" }, timestamp: 1 }
    @class.expects(:build_insertion_records).
           with(input, user_id: true).
           returns [record]
    @class.expects(:influxdb_bulk_record).with(
      @class::LeaderboardMeasurement,
      [record]
    )
    @class.write_leaderboard_to_influx!(input)
  end

  test 'write_in_chunks returns early if given 0 records ' do
    records = []
    records.expects(:in_groups_of).never
    @class.write_in_chunks(records)
  end

  test "write_in_chunks calls the block with chunks of records" do
    records = 900.times.to_a
    record_groups = [
      records[0...300],
      records[300...600],
      records[600...900]
    ]
    test_fn = -> (record_group) { }
    record_groups.each do |record_group|
      test_fn.expects(:call).with(record_group)
    end
    @class.write_in_chunks(records) do |record_group|
      test_fn.call(record_group)
    end
  end

  test '.build_scores_by_group builds per-group aggregations' do
    time = Time.now.to_s
    group_id = 22
    org_id = 33
    record_keys = Analytics::UserScoreContinuousQueries.query_names
    record1 = record_keys.each.with_index.with_object({}) do |(key, idx), memo|
      memo[key] = idx
    end.merge("org_id" => org_id)
    record2 = record1.transform_values { |val| val + 1 }.
                      merge("org_id" => org_id)
    group_user_scores_daily = {
      time => { group_id => [record1, record2] }
    }
    agg = record_keys.each_with_object({}) do |key, memo|
      memo[key] = record1[key] + record2[key]
    end.merge("org_id" => org_id)
    result = { time => { group_id => agg } }
    assert_equal(
      result,
      @class.build_scores_by_group(group_user_scores_daily)
    )
  end

  test ".write_leaderboard_to_mongo_cache!" do
    skip # UNSKIP IF WE ENABLE MONGO
    Mongo::Collection.any_instance.unstub :insert_many

    time_str = Time.now.to_s
    group_id = 1
    user_scores = [{"foo" => 1}, {"foo" => 2}]

    leaderboard = {
      time_str => {
        group_id => user_scores
      }
    }
    records = user_scores.map do |record|
      record.merge(
        "time" => Time.parse(time_str).to_i,
        "group_id" => group_id,
      )
    end
    @class.ref.expects(:insert_many).with(records)
    @class.write_leaderboard_to_mongo_cache!(leaderboard)
  end

  test '.save_leaderboard! scalability test' do
    # =========================================
    # NOTE - this test really reads/writes data to Mongo and Influx
    # =========================================
    skip # UNSKIP IF WE ENABLE MONGO AND IF TESTING LOCALLY

    WebMock.disable!
    INFLUXDB_CLIENT.query("DROP MEASUREMENT group_user_scores_daily")
    INFLUXDB_CLIENT.unstub :write_points
    Mongo::Collection.any_instance.unstub :insert_many

    @class.ref.delete_many # WIPES THE COLLECTION
    assert_equal 0, @class.ref.count

    num_days = 2
    num_groups = 100
    num_users_per_group = 500

    value_keys = Analytics::UserScoreContinuousQueries.query_names
    timestamp = Time.now.to_i
    leaderboard = num_days.times.each_with_object({}) do |num_days, memo1|
      time = (Time.now - num_days.days).to_s
      memo1[time] = num_groups.times.each_with_object({}) do |group_id, memo2|
        memo2[group_id] = num_users_per_group.times.map do |i|
          value_keys.reduce({}) do |memo, key|
              memo.merge(key => i, "org_id" => 5, "user_id" => i)
          end
        end
      end
    end

    @class.save_leaderboard!(leaderboard)

    assert_equal(
      (num_days * num_groups * num_users_per_group),
      @class.ref.count
    )
    @class.ref.delete_many # WIPES THE COLLECTION
    WebMock.enable!
  end

  test ".write_leaderboard_to_mongo_cache!, with real data" do
    skip # UNSKIP IF WE ENABLE MONGO
    Mongo::Collection.any_instance.unstub :insert_many
    # This data is a sample of what was produced in staging.
    leaderboard = {
      "2018-03-02T00:00:00Z" => {
        "790" => [{
          "time"=>"2018-03-02T00:00:00Z",
          "org_id"=>"1",
          "smartbites_commented_count"=>nil,
          "smartbites_completed_count"=>1,
          "smartbites_consumed_count"=>6,
          "smartbites_created_count"=>1,
          "smartbites_liked_count"=>nil,
          "time_spent_minutes"=>4,
          "total_smartbite_score"=>100,
          "total_user_score"=>100,
          "user_id"=>"1028"
        }]
      }
    }
    @class.ref.expects(:insert_many).with([
      "time" => Time.parse("2018-03-02T00:00:00Z").to_i,
      "group_id" => "790",
      "org_id"=>"1",
      "smartbites_commented_count"=>nil,
      "smartbites_completed_count"=>1,
      "smartbites_consumed_count"=>6,
      "smartbites_created_count"=>1,
      "smartbites_liked_count"=>nil,
      "time_spent_minutes"=>4,
      "total_smartbite_score"=>100,
      "total_user_score"=>100,
      "user_id"=>"1028"
    ])
    @class.write_leaderboard_to_mongo_cache!(leaderboard)
  end

  test '.build_insertion_records build a list of influx input hashes' do
    time = Time.now

    time_str = time.to_s
    timestamp = time.to_i

    group_id_1 = "1"
    group_id_2 = "2"
    org_id = "3"
    user_id = "4"

    # Note - user_id won't be included by default
    data = { "org_id" => org_id, "user_id" => user_id, "foo" => "bar" }

    group_aggs = {
      time_str => {
        group_id_1 => data,
        group_id_2 => data
      }
    }

    expected = [
      {
        timestamp: timestamp,
        tags: { "group_id" => group_id_1, "org_id" => org_id },
        values: data.slice("foo")
      },
      {
        timestamp: timestamp,
        tags: { "group_id" => group_id_2, "org_id" => org_id },
        values: data.slice("foo")
      }
    ]

    result = @class.build_insertion_records(group_aggs)

    assert_equal(expected, result)
  end

  test '.build_group_user_scores_daily organizes records by group' do
    time = Time.now
    time_str = time.to_s

    user_id = "1"

    group_id_1 = "3"
    group_id_2 = "4"

    # create 2 records for the user
    record_1 = {"user_id" => user_id}
    record_2 = record_1.clone

    users_groups = {
      user_id => [group_id_1, group_id_2]
    }

    user_scores = {
      time_str => [record_1, record_2]
    }

    expected = {
      time_str => {
        group_id_1 => [record_1, record_2],
        group_id_2 => [record_1, record_2]
      }
    }

    actual = @class.build_group_user_scores_daily(users_groups, user_scores)
    assert_equal expected, actual
  end

  test 'get_user_score_aggregations fetches user scores and groups by time' do
    time_str_1 = Time.now.to_s
    time_str_2 = (Time.now - 1.days).to_s
    query = @class.user_scores_aggregations_query
    record_1 = { "time" => time_str_1 }
    record_2 = { "time" => time_str_2 }
    stub_response = [ { "values" => [record_1, record_2] } ]
    @class.expects(:send_query).with(query).returns stub_response
    expected = {
      time_str_1 => [record_1],
      time_str_2 => [record_2]
    }
    actual = @class.get_user_scores_aggregations
    assert_equal expected, actual
  end

  test '.user_scores_aggregations_query returns influx query string' do
    period = @class::SearchPeriod
    expected = "SELECT * FROM user_scores_daily WHERE time > now() - #{period}"
    actual = @class.user_scores_aggregations_query
    assert_equal expected, @class.format_query(actual)
  end

  test '.get_user_group_mappings gets a list of groups for each user' do
    query = @class.user_group_mappings_query
    user_id_1 = "1"
    user_id_2 = "2"
    group_id_1 = "3"
    group_id_2 = "4"
    # users 1 and 2 are in group 1. User 1 is also in group 2
    stub_response = [
      {
        "tags" => { "_user_id" => user_id_1 },
        "values" => [
          { "group_id" => group_id_1 },
          { "group_id" => group_id_2 }
        ]
      },
      {
        "tags" => { "_user_id" => user_id_2 },
        "values" => [
          { "group_id" => group_id_1 },
        ]
      }
    ]
    @class.expects(:send_query).with(query).returns stub_response
    expected = {
      user_id_1 => [group_id_1, group_id_2],
      user_id_2 => [group_id_1]
    }
    actual = @class.get_user_group_mappings
    assert_equal expected, actual
  end

  test '.user_group_mappings_query returns a string to get groups per user' do
    expected = "SELECT DISTINCT(group_id) AS group_id FROM \"groups\" " +
               "WHERE event='group_user_added' " +
               "AND time > #{@class::MinTime} " +
               "GROUP BY _user_id"
    actual = @class.user_group_mappings_query
    assert_equal expected, @class.format_query(actual)
  end

end

require 'test_helper'

class InfluxQueryTest < ActiveSupport::TestCase

  setup do
    @event_tracking_version = Settings.influxdb.event_tracking_version
    @class = InfluxQuery
    @source = "source"
    @inst = @class.new(@source)
    assert_empty @inst.conditions
    assert_empty @inst.params
    @version = Settings.influxdb.event_tracking_version
  end

  test '.fetch' do
    query = "SELECT FOO FROM BAR PLZ"
    params = { foo: "bar!" }
    result = "result"
    INFLUXDB_CLIENT.expects(:query).with(query, params: params).returns result
    assert_equal result, @class.fetch(query, params)
  end

  test '#query_method' do
    query = InfluxQuery.new("things")
    assert_equal "select", query.query_method
    query.query_method = "cowsay"
    expected = "cowsay * from \"things\" ;"
    assert_equal expected, query.finalize_query
  end

  test '#delete_all!' do
    query = InfluxQuery.new("things")
    assert_equal "select", query.query_method
    assert_equal ["*"], query.select_columns
    query.order!("ASC")
    assert_equal "ASC", query.params[:order]
    result = query.delete_all!
    assert_equal nil, query.params[:order]
    assert_equal query, result
    assert_equal "delete", query.query_method
    assert_equal [""], query.select_columns
    expected = "delete  from \"things\" ;"
    assert_equal expected, query.finalize_query
  end

  test '#has_subquery default values and attr_reader' do
    # Default values and attr_accessor
    query = InfluxQuery.new("things")
    refute query.has_subquery
  end

  test '#has_subquery effect on #finalize_subquery' do
    query = InfluxQuery.new("(SELECT foo FROM bar)", has_subquery: true)
    actual = query.finalize_subquery
    # It does not wrap the measurement in quotes.
    expected = "select * from (SELECT foo FROM bar)"
    assert_equal expected, actual
  end

  test 'error handling in #initialize' do
    msg = assert_raises(ArgumentError) do
      InfluxQuery.new("<thin></thin>gs", has_subquery: true)
    end.message
    assert_equal("Given subquery doesn't start with parenthesis", msg)

    msg = assert_raises(ArgumentError) do
      InfluxQuery.new("(things", has_subquery: false)
    end.message
    assert_equal("pass has_subquery: true if using subquery as measurement", msg)

    msg = assert_raises(ArgumentError) do
      InfluxQuery.new("\"things\"")
    end.message
    assert_equal("don't include quotes in measurement name", msg)
  end

  test '#to_plain_query' do
    query = InfluxQuery.new("things")
      .filter!(:foo, "foo", "=", "1")
      .filter!(:bar, "bar", "=", 2)
      .group_by!("time(1d)")
      .order!("ASC")
      .limit!("5")
      .offset!("4")
    actual = query.to_plain_query
    expected = "select * from \"things\" where foo = '1' AND bar = 2 " +
                "GROUP BY time(1d) ORDER BY ASC LIMIT 5 OFFSET 4"
    assert_equal expected, actual

    actual2 = query.to_plain_query as_subquery: true
    assert_equal "(#{expected})", actual2
  end

  test 'resolve finalizes and sends the query' do
    query = @class.new("things").filter!(:foo, "foo", "=", "bar")
    INFLUXDB_CLIENT.expects(:query).with(
      "select * from \"things\" where foo = %{foo} ;",
      params: { foo: "bar" }
    ).returns "stub_result"
    assert_equal "stub_result", query.resolve
  end

  test '.get_query_for_entity with unknown entity' do
    assert_raises InfluxQuery::InfluxQueryError do
      result_query, result_params = @class.get_query_for_entity(
        entity: 'foo',
        organization_id: '1',
        filters: { start_date: Time.now.to_i }
      )
    end
  end

  # FOR MORE TESTS OF get_query_for_entity,
  # see test/lib/analytics/influx_query/entity_strategies_test.rb

  test 'initiailize and attr readers' do
    @inst.params[:param_key] = "param_val"
    @inst.conditions << "condition"
    @inst.select_columns << "col"
    assert_equal @source, @inst.source
    assert_equal({param_key: "param_val"}, @inst.params)
    assert_equal ["condition"], @inst.conditions
    assert_equal ["*", "col"], @inst.select_columns
  end

  test 'finalize_query' do
    assert_equal "#{@inst.finalize_subquery} ;", @inst.finalize_query
  end

  test 'finalize_subquery' do
    @inst.select_columns.tap(&:shift).concat ["col1", "col2"]
    @inst.params[:group_by] = "col3"
    @inst.params[:limit] = 500
    @inst.params[:offset] = 600
    expected = "select col1,col2 from \"#{@source}\" GROUP BY col3 LIMIT %{limit} OFFSET %{offset}"
    result = @inst.finalize_subquery
    assert_equal expected, result
  end

  test "limit!" do
    # The limit param is initially null
    assert_nil @inst.params[:limit]
    # Passing a null limit has no effect
    @inst.limit!(nil)
    assert_nil @inst.params[:limit]
    # Passing an invalid limit raises an error
    [0, 'foo'].each do |invalid_limit|
      error = assert_raises InfluxQuery::InfluxQueryError do
        @inst.limit!(invalid_limit)
      end
      assert_equal "Cannot add a limit of 0", error.message
      assert_nil @inst.params[:limit]
    end
    # Passing a valid limit sets the value in params
    @inst.limit!(9000)
    assert_equal 9000, @inst.params[:limit]
    # Passing a string value also works
    result = @inst.limit!("1000")
    assert_equal 1000, @inst.params[:limit]
    # Ensure the method returns self (i.e. is chainable)
    assert_equal @inst, result
  end

  test "order" do
    # The order param is initially null
    assert_nil @inst.params[:order]
    # Passing a null order has no effect
    @inst.order!(nil)
    assert_nil @inst.params[:order]
    # Passing a valid order sets the value in params
    result = @inst.order!("ASC")
    assert_equal "ASC", @inst.params[:order]
    # Ensure the method returns self (i.e. is chainable)
    assert_equal @inst, result
    # Ensure that the order is found in the finalized query
    expected = "select * from \"#{@source}\" ORDER BY ASC ;"
    assert_equal expected, @inst.finalize_query
  end

  test "offset!" do
    # The offset param is initially null
    assert_nil @inst.params[:offset]
    # Passing a null offset has no effect
    @inst.offset!(nil)
    assert_nil @inst.params[:offset]
    # Passing a valid offset sets the value in params
    @inst.offset!(9000)
    assert_equal 9000, @inst.params[:offset]
    # Passing a string value also works
    result = @inst.offset!("1000")
    assert_equal 1000, @inst.params[:offset]
    # Ensure the method returns self (i.e. is chainable)
    assert_equal @inst, result
  end

  test 'group_by!' do
    # The group_by param is initially null
    assert_nil @inst.params[:group_by]
    # Passing a null group_by has no effect
    @inst.group_by!(nil)
    assert_nil @inst.params[:group_by]
    # Passing a valid value sets the param
    result = @inst.group_by!("potato")
    assert_equal "potato", @inst.params[:group_by]
    # Ensure the method returns self (i.e. is chainable)
    assert_equal @inst, result
  end

  test 'add_conditions!' do
    # Push a condition with default args
    properties = [
      { key: "a", operator: "<", value: "%{foo}" },
      { key: "b", operator: ">", value: "%{bar}" }
    ]
    @inst.add_conditions!(properties)
    assert_equal 1, @inst.conditions.length
    expected = "a < %{foo} AND b > %{bar}"
    assert_equal expected, @inst.conditions.last
    # Push a condition with some configured options
    result = @inst.add_conditions!(properties, operator: "OR", wrap_clause: true)
    assert_equal 2, @inst.conditions.length
    expected = "(a < %{foo} OR b > %{bar})"
    assert_equal expected, @inst.conditions.last
    # Ensure the method returns self and is chainable
    assert_equal @inst, result
    # This method does NOT add to the params.
    assert_empty @inst.params
  end

  test 'add_where_in_filter!' do
    # Pass it a list of ids and it will build an OR clause with them
    result = @inst.add_where_in_filter!(:foo, "bar", [1,2])
    assert_equal 1, @inst.conditions.length
    expected = "(bar = %{foo_0} OR bar = %{foo_1})"
    assert_equal expected, @inst.conditions.last
    # Ensure the method returns self / is chainable
    assert_equal @inst, result
    # check that params are added
    expected = { foo_0: 1, foo_1: 2 }
    assert_equal expected, @inst.params
  end

  test 'add_default_filters!' do
    # Pass it nothing as args, so it uses defaults
    @inst.add_default_filters!
    assert_equal ["analytics_version = %{version}"], @inst.conditions
    assert_equal({:version=>"2.0.0"}, @inst.params)
    # clear the conditions and params
    @inst.conditions.length.times { @inst.conditions.shift }
    @inst.params.keys.each &@inst.params.method(:delete)
    # Pass it some custom args
    result = @inst.add_default_filters!(
      organization_id: '1',
      start_date: 2,
      end_date: 3,
      version: @version,
      foo: "bar" # this arg is ignored
    )
    expected = [
      "org_id = %{organization_id}",
      "time >= %{start_date}s",
      "time <= %{end_date}s",
      "analytics_version = %{version}"
    ]
    assert_equal expected, @inst.conditions
    # Ensure the method returns self / is chainable
    assert_equal @inst, result
    # Check that params are added
    expected = {
      organization_id: '1',
      start_date: 2,
      end_date: 3,
      version: @version
    }
    assert_equal expected, @inst.params
  end

  test 'filter!' do
    # Doesn't do anything if given a null value
    @inst.filter!(:foo, "bar", "=", nil)
    assert_empty @inst.conditions
    assert_empty @inst.params
    # Adds to conditions and params otherwise
    result = @inst.filter!(:foo, "bar", "=", "val")
    assert_equal 1, @inst.conditions.length
    expected = "bar = %{foo}"
    assert_equal expected, @inst.conditions[0]
    expected = {
      foo: "val"
    }
    assert_equal expected, @inst.params
    # Ensure method returns self
    assert_equal @inst, result
  end

  test 'add_version_filter!' do
    # It does nothing for some entities
    %w{user_scores_daily org_scores_daily}.each do |skipped_source|
      @inst.instance_variable_set "@source", skipped_source
      @inst.add_version_filter!
      assert_empty @inst.conditions
      assert_empty @inst.params
    end
    # It adds a filter otherwise
    @inst.instance_variable_set "@source", "foo"
    result = @inst.add_version_filter!
    assert_equal 1, @inst.conditions.length
    expected = "analytics_version = %{version}"
    assert_equal expected, @inst.conditions[0]
    expected = {version: @version}
    assert_equal expected, @inst.params
    # Ensure method returns self
    assert_equal @inst, result
  end

  test 'add_time_filters!' do
    time = Time.now
    Time.stubs(:now).returns time
    # It DOES NOT set default value for the start_date and end_date args
    @inst.add_time_filters!
    assert_equal 0, @inst.conditions.length
    expected = {}
    assert_equal expected, @inst.params
    # It accepts custom arguments as well
    result = @inst.add_time_filters!(start_date: 4, end_date: 5)
    expected = ["time >= %{start_date}s", "time <= %{end_date}s"]
    assert_equal 2, @inst.conditions.length
    assert_equal expected, @inst.conditions
    expected = {start_date: 4, end_date: 5}
    assert_equal expected, @inst.params
    # Ensure method returns self
    assert_equal @inst, result
  end

  test 'build_query_constraints directly interpolates ' do
    result = @inst.build_query_constraints(
      properties: [
        { key: "a", operator: ">", value: "%{foo}" },
        { key: "b", operator: "<", value: "%{bar}" },
      ]
    )
    assert_equal  "a > %{foo} AND b < %{bar}", result
  end

end
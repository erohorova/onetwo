require 'test_helper'
class LastActivityRecorderTest < ActiveSupport::TestCase
  setup do
    user_first_name = Faker::Name.first_name
    user_last_name = Faker::Name.last_name

    @org = create :organization,
      name: 'LXP',
      host_name: 'spark'

    @user = create :user,
      first_name: user_first_name,
      last_name: user_last_name,
      organization: @org,
      sign_in_count: 2,
      last_sign_in_at: 2.days.ago,
      last_sign_in_ip: Faker::Internet.ip_v4_address,
      current_sign_in_ip: Faker::Internet.ip_v4_address

    @klass = Analytics::LastActivityRecorder
  end

  test 'valid registered events' do
    registered_events = {
      "channel_visited"      => true,
      "user_profile_visited" => true,
      "card_source_visited"  => true,
      "card_viewed"          => true
    }
    assert_equal registered_events, @klass::VALID_EVENTS
  end

  test 'should not store in cache if event is invalid' do
    Rails.cache       = ActiveSupport::Cache::MemoryStore.new
    cache = Rails.cache

    cache.expects(:write).never
    @klass.new(user_id: @user.id)
      .add_data_to_cache(
        event: 'invalid_event',
        value: {}
      )
  end
  
  test 'add_data_to_cache method should store data to cache' do
    begin
      orig_cache = Rails.cache
      Rails.cache       = ActiveSupport::Cache::MemoryStore.new
      last_activity     = @klass.new(user_id: @user.id)
      last_activity.add_data_to_cache(event: "channel_visited",value: {"any_key":"any_value"})
      expected_response = {
        :event          =>  "channel_visited", 
        :entity         =>  "channels", 
        :last_viewed_at =>  nil, 
        :any_key        =>  "any_value"
      }
      assert Rails.cache.read("user_last_activity_#{@user.id}"), expected_response
    ensure
      Rails.cache = orig_cache
    end
  end

  test 'get_data_from_cache method should read data from cache' do
    begin
      orig_cache = Rails.cache
      Rails.cache       = ActiveSupport::Cache::MemoryStore.new
      last_activity     = @klass.new(user_id: @user.id)
      last_activity.add_data_to_cache(event: "channel_visited",value: {"any_key":"any_value"})
      expected_response = {
        :event          =>  "channel_visited", 
        :entity         =>  "channels", 
        :last_viewed_at =>  nil, 
        :any_key        =>  "any_value"
      }.to_json
      assert last_activity.get_data_from_cache, expected_response
    ensure
      Rails.cache = orig_cache
    end
  end

  test 'get_data_from_cache method should read empty is no cache data is found' do
    begin
      orig_cache = Rails.cache
      Rails.cache       = ActiveSupport::Cache::MemoryStore.new
      last_activity     = @klass.new(user_id: @user.id)
      assert_empty last_activity.get_data_from_cache
    ensure
      Rails.cache = orig_cache
    end
  end
end
require 'test_helper'

class AwsCloudFrontSslHeaderTest < ActionController::TestCase
  class CloudFrontTestController < ActionController::Base
    def index
      request.host
      render text: "scheme=#{request.scheme} host=#{request.host} host_with_port=#{request.host_with_port}"
    end
  end
  include Devise::Test::ControllerHelpers
  setup do
    @controller = CloudFrontTestController.new
  end

  test 'controller should think it is secured' do

    with_routing do |set|
      set.draw do
        get '/cf', to: 'aws_cloud_front_ssl_header_test/cloud_front_test#index'
      end
      @request.headers["HTTP_CLOUDFRONT_FORWARDED_PROTO"] = 'https'
      get :index
      assert_includes response.body, "scheme=https"
    end

  end

  test 'controller should think it is secured with the standard header' do

    with_routing do |set|
      set.draw do
        get '/cf', to: 'aws_cloud_front_ssl_header_test/cloud_front_test#index'
      end
      @request.headers["HTTP_X_FORWARDED_PROTO"] = 'https'
      get :index
      assert_includes response.body, "scheme=https"
    end

  end

  test 'controller should not think it is secured' do
    with_routing do |set|
      set.draw do
        get '/cf', to: 'aws_cloud_front_ssl_header_test/cloud_front_test#index'
      end
      @request.headers["HTTP_CLOUDFRONT_FORWARDED_PROTO"] = 'http'
      get :index
      assert_includes response.body, "scheme=http"
    end
  end

  test 'controller should not think it is secured when header is not provided' do
    with_routing do |set|
      set.draw do
        get '/cf', to: 'aws_cloud_front_ssl_header_test/cloud_front_test#index'
      end
      get :index
      assert_includes response.body, "scheme=http"
    end
  end

end

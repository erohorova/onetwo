require 'test_helper'

class RecommendTest < ActiveSupport::TestCase

  setup do
    skip #deprecated
    WebMock.disable!
    Recommend.unstub(:get_recommendations)
    Card.reindex
    UserContentsQueue.create_index! force: true
    @user = create(:user)
  end

  teardown do
    WebMock.enable!
  end

  def following_channel_setup
    @ch1, @ch2, @ch3 = create_list(:channel, 3)
    @cards = create_list(:card, 4, card_type: 'media', card_subtype: 'link')
    @channels = [@ch1, @ch1, @ch2, @ch3]
    items = []
    @cards.each_with_index do |card, indx|
      items << UserContentsQueue.new(
        user_id: @user.id,
        content_id: card.id,
        content_type: 'Card',
        metadata: [{'rationale' => {'type' => 'following_channel', 'channel_id' => @channels[indx].id}}]
      )
    end
    items
  end

  def following_user_setup
    @f1, @f2, @f3 = create_list(:user, 3)
    @cards = create_list(:card, 4, card_type: 'media', card_subtype: 'link')
    @authors = [@f1, @f1, @f2, @f3]
    items = []
    @cards.each_with_index do |card, indx|
      items << UserContentsQueue.new(
        user_id: @user.id,
        content_id: card.id,
        content_type: 'Card',
        metadata: [{'rationale' => {'type' => 'following_user', 'user_id' => @authors[indx].id}}]
      )
    end
    items
  end

  def stream_categorization_setup
    items = []
    @f1 = create(:user)
    @ch = create(:channel)
    vlive = create_list(:wowza_video_stream, 2, status: 'live')
    vs1, vs2 = vscheduled = create_list(:wowza_video_stream, 2, status: 'upcoming')
    vs1.update(start_time: 2.day.since)
    vs2.update(start_time: 1.day.since)
    vpast = create_list(:wowza_video_stream, 7, status: 'past')
    VideoStream.any_instance.stubs(:past_stream_available?).returns(true)

    vlive.each do |vl|
      items << UserContentsQueue.new(
          user_id: @user.id,
          content_id: vl.id,
          content_type: 'VideoStream',
          metadata: [{'rationale' => {'type' => 'influencer_stream'}}]
      )
    end
    vscheduled.each do |vl|
      items << UserContentsQueue.new(
          user_id: @user.id,
          content_id: vl.id,
          content_type: 'VideoStream',
          metadata: [{'rationale' => {'type' => 'following_user'}}]
      )
    end

    vpast[0..1].each do |vl|
      items << UserContentsQueue.new(
          user_id: @user.id,
          content_id: vl.id,
          content_type: 'VideoStream',
          metadata: [{'rationale' => {'type' => 'following_user', 'user_id' => @f1.id}}]
      )
    end
    vpast[2..3].each do |vl|
      items << UserContentsQueue.new(
          user_id: @user.id,
          content_id: vl.id,
          content_type: 'VideoStream',
          metadata: [{'rationale' => {'type' => 'following_channel', 'channel_id' => @ch.id}}]
      )
    end
    vpast[4..5].each do |vl|
      items << UserContentsQueue.new(
          user_id: @user.id,
          content_id: vl.id,
          content_type: 'VideoStream',
          metadata: [{'rationale' => {'type' => 'influencer_stream'}}]
      )
    end
    # no rationale
    items << UserContentsQueue.new(
          user_id: @user.id,
          content_id: vpast.last.id,
          content_type: 'VideoStream',
          metadata: [{}]
    )

    items
  end

  test 'should categorize video streams' do
    items = stream_categorization_setup
    should_be_excluded = []
    # Not a valid id
    should_be_excluded << UserContentsQueue.new(
          user_id: @user.id,
          content_id: 99999,
          content_type: 'VideoStream',
          metadata: [{}]
    )

    categorized, exclusion_list = Recommend.categorize_stream_recommendations(items + should_be_excluded)
    assert_equal 6, categorized.count

    assert_same_elements should_be_excluded, exclusion_list
    assert_equal({'title' => "Live right now!", 'description' => '', 'items' => items[0..1]}, categorized[0])
    assert_equal({'title' => "Scheduled streams you may be interested in", 'description' => '', 'items' => items.values_at(3,2)}, categorized[1])
    assert_equal({'title' => "Streams from influencers on Edcast", 'description' => '', 'items' => items[8..9]}, categorized[2])
    assert_equal({'title' => "Streams from people you are following", 'description' => '', 'items' => items[4..5]}, categorized[3])
    assert_equal({'title' => "Streams from channels you are following", 'description' => '', 'items' => items[6..7]}, categorized[4])
    assert_equal({'title' => "Streams you may be interested in", 'description' => '', 'items' => items[10..10]}, categorized[5])

    VideoStream.any_instance.stubs(:past_stream_available?).returns(false)
    # All past streams should now be excluded
    categorized, exclusion_list = Recommend.categorize_stream_recommendations(items)
    assert_equal 2, categorized.count
    assert_same_elements items[4..10], exclusion_list

    VideoStream.any_instance.unstub(:past_stream_available?)
  end

  test 'should not categorize upcoming video streams' do
    items = stream_categorization_setup
    should_be_excluded, upcoming_stream_exclusion = [], []
    # Not a valid id
    should_be_excluded << UserContentsQueue.new(
          user_id: @user.id,
          content_id: 99999,
          content_type: 'VideoStream',
          metadata: [{}]
    )

    vscheduled = VideoStream.upcoming
    vscheduled.each do |vl|
      upcoming_stream_exclusion << UserContentsQueue.new(
          user_id: @user.id,
          content_id: vl.id,
          content_type: 'VideoStream',
          metadata: [{'rationale' => {'type' => 'following_user'}}]
      )
    end

    categorized, exclusion_list = Recommend.categorize_stream_recommendations(items + should_be_excluded, {show_scheduled_streams: false})
    assert_equal 5, categorized.count

    assert_equal (upcoming_stream_exclusion + should_be_excluded).map(&:content_id), exclusion_list.map(&:content_id)
    assert_equal({'title' => "Live right now!", 'description' => '', 'items' => items[0..1]}, categorized[0])
    assert_equal({'title' => "Streams from influencers on Edcast", 'description' => '', 'items' => items[8..9]}, categorized[1])
    assert_equal({'title' => "Streams from people you are following", 'description' => '', 'items' => items[4..5]}, categorized[2])
    assert_equal({'title' => "Streams from channels you are following", 'description' => '', 'items' => items[6..7]}, categorized[3])
    assert_equal({'title' => "Streams you may be interested in", 'description' => '', 'items' => items[10..10]}, categorized[4])

    VideoStream.any_instance.stubs(:past_stream_available?).returns(false)
    # All past streams and upcoming should now be excluded
    categorized, exclusion_list = Recommend.categorize_stream_recommendations(items)
    assert_equal 2, categorized.count
    assert_same_elements items[4..10], exclusion_list

    VideoStream.any_instance.unstub(:past_stream_available?)
  end

  test 'should categorize cards from following channels' do
    items = following_channel_setup
    categorized, exclusion_list = Recommend.categorize_card_recommendations(items)
    assert_equal 2, categorized.count
    assert_equal({'title' => "Because you are following #{@ch1.label}", 'description' => '', 'items' => items.first(2)}, categorized.first)
    assert_equal({'title' => "Content from channels you are following", 'description' => '', 'items' => items[2..3]}, categorized.last)
  end

  test 'should categorize cards from following users' do
    items = following_user_setup
    categorized, exclusion_list = Recommend.categorize_card_recommendations(items)
    assert_equal 2, categorized.count
    assert_equal({'title' => "Because you are following #{@f1.name}", 'description' => '', 'items' => items.first(2)}, categorized.first)
    assert_equal({'title' => "Content from people you are following", 'description' => '', 'items' => items[2..3]}, categorized.last)
  end

  test 'should not categorize cards with no author #with options' do
    org = create(:organization)
    user = create(:user, organization: org)

    ch1, ch2 = create_list(:channel, 2, organization_id: org.id, is_promoted: true)

    c1,c2 = create_list(:card, 2, author_id: nil, organization: org)
    c3, c4, c5 = create_list(:card, 3, author_id: user.id, organization: org)

    items = []
    items << UserContentsQueue.new(
        user_id: user.id,
        content_id: c1.id,
        content_type: 'Card',
        metadata: [{'rationale' => {'type' => 'following_channel', 'channel_id' => ch1.id}}]
    )
    items << UserContentsQueue.new(
        user_id: user.id,
        content_id: c2.id,
        content_type: 'Card',
        metadata: [{'rationale' => {'type' => 'following_channel', 'channel_id' => ch2.id}}]
    )
    items << UserContentsQueue.new(
        user_id: user.id,
        content_id: c3.id,
        content_type: 'Card',
        metadata: [{'rationale' => {'type' => 'following_channel', 'channel_id' => ch1.id}}]
    )
    items << UserContentsQueue.new(
        user_id: user.id,
        content_id: c4.id,
        content_type: 'Card',
        metadata: [{'rationale' => {'type' => 'following_channel', 'channel_id' => ch2.id}}]
    )

    items << UserContentsQueue.new(
        user_id: user.id,
        content_id: c5.id,
        content_type: 'Card',
        metadata: [{'rationale' => {'type' => 'following_channel', 'channel_id' => ch2.id}}]
    )
    categorized, exclusion_list = Recommend.categorize_card_recommendations(items, {author_must: true})
    assert_equal 2, exclusion_list.count
    assert_equal items.first(2), exclusion_list

    assert_not_includes categorized.first["items"].map {|i| i[:content_id]}, c1
    assert_not_includes categorized.first["items"].map {|i| i[:content_id]}, c2

    assert_not_includes categorized.second["items"].map {|i| i[:content_id]}, c1
    assert_not_includes categorized.second["items"].map {|i| i[:content_id]}, c2
  end

  test 'should categorize cards with no author #without options' do
    org = create(:organization)
    user = create(:user, organization: org)

    ch1, ch2 = create_list(:channel, 2, organization_id: org.id)

    c1,c2 = create_list(:card, 2, author_id: nil, organization: org)
    c3, c4, c5 = create_list(:card, 3, author_id: user.id, organization: org)

    items = []
    items << UserContentsQueue.new(
        user_id: user.id,
        content_id: c1.id,
        content_type: 'Card',
        metadata: [{'rationale' => {'type' => 'following_channel', 'channel_id' => ch1.id}}]
    )
    items << UserContentsQueue.new(
        user_id: user.id,
        content_id: c3.id,
        content_type: 'Card',
        metadata: [{'rationale' => {'type' => 'following_channel', 'channel_id' => ch1.id}}]
    )
    items << UserContentsQueue.new(
        user_id: user.id,
        content_id: c2.id,
        content_type: 'Card',
        metadata: [{'rationale' => {'type' => 'following_channel', 'channel_id' => ch2.id}}]
    )
    items << UserContentsQueue.new(
        user_id: user.id,
        content_id: c4.id,
        content_type: 'Card',
        metadata: [{'rationale' => {'type' => 'following_channel', 'channel_id' => ch2.id}}]
    )

    items << UserContentsQueue.new(
        user_id: user.id,
        content_id: c5.id,
        content_type: 'Card',
        metadata: [{'rationale' => {'type' => 'following_channel', 'channel_id' => ch2.id}}]
    )
    categorized, exclusion_list = Recommend.categorize_card_recommendations(items)
    assert_empty exclusion_list

    assert_same_elements categorized.first["items"].map {|i| i[:content_id]}, [c1, c3].map(&:id)
    assert_same_elements categorized.second["items"].map {|i| i[:content_id]}, [c2, c4, c5].map(&:id)
  end

  test 'combined card recommender' do
    items = following_user_setup
    items += following_channel_setup

    # Add some oddballs, no metadata. random metadata
    random_cards = create_list(:card, 2, card_type: 'media', card_subtype: 'link')
    items << UserContentsQueue.new(
        user_id: @user.id,
        content_id: random_cards.first.id,
        content_type: 'Card',
        metadata: [{}]
    )
    items << UserContentsQueue.new(
        user_id: @user.id,
        content_id: random_cards.last.id,
        content_type: 'Card',
        metadata: [{'k' => 'v'}]
    )
    categorized, exclusion_list = Recommend.categorize_card_recommendations(items)
    assert_equal 5, categorized.count
    assert_equal({'title' => "Because you are following #{@f1.name}", 'description' => '', 'items' => items.first(2)}, categorized.first)
    assert_equal({'title' => "Content from people you are following", 'description' => '', 'items' => items[2..3]}, categorized[1])
    assert_equal({'title' => "Because you are following #{@ch1.label}", 'description' => '', 'items' => items[4..5]}, categorized[2])
    assert_equal({'title' => "Content from channels you are following", 'description' => '', 'items' => items[6..7]}, categorized[3])
    assert_equal({'title' => "Content you might be interested in", 'description' => '', 'items' => items[8..9]}, categorized.last)
  end

end

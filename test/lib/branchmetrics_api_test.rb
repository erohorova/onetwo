require 'test_helper'
class BranchmetricsApiTest < ActiveSupport::TestCase
  include BranchmetricsApi

  test 'should generate and return link' do
    payload = {'first_name' => 'a', 'last_name' => 'b',
               'email' => 'a@a.com', 'token' => '123'}

    stub_request(:post, "https://api.branch.io/v1/url").
      with(:body => {branch_key: Settings.branchmetrics.api_key, data: payload}.to_json,
           :headers => {
             'Accept'=>'*/*',
             'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
             'Content-Type'=>'application/json',
             'User-Agent'=>/.*/}).
             to_return(:status => 200, :body => "{\"url\":\"http://bitly.com\"}", :headers => {})
    url = BranchmetricsApi.generate_link(payload)
    assert_not_nil url
    assert_equal 'http://bitly.com', url
  end

  test 'should log error and fail on bad response' do
    payload = {'first_name' => 'a', 'last_name' => 'b',
               'email' => 'a@a.com', 'token' => '123'}

    stub_request(:post, "https://api.branch.io/v1/url").
      with(:body => {branch_key: Settings.branchmetrics.api_key, data: payload}.to_json,
           :headers => {
             'Accept'=>'*/*',
             'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
             'Content-Type'=>'application/json',
             'User-Agent'=>/.*/}).
             to_return(:status => 400, :body => "{\"url\":\"http://bitly.com\"}", :headers => {})

    Rails.logger.expects(:error).once
    url = BranchmetricsApi.generate_link(payload)
    assert_nil url
  end

  test 'should log error and fail on malformed json response' do
    payload = {'first_name' => 'a', 'last_name' => 'b',
               'email' => 'a@a.com', 'token' => '123'}

    stub_request(:post, "https://api.branch.io/v1/url").
      with(:body => {branch_key: Settings.branchmetrics.api_key, data: payload}.to_json,
           :headers => {
             'Accept'=>'*/*',
             'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
             'Content-Type'=>'application/json',
             'User-Agent'=>/.*/}).
             to_return(:status => 200, :body => "{\"url\":\"http://bitly.co}", :headers => {})

    Rails.logger.expects(:error).once
    url = BranchmetricsApi.generate_link(payload)
    assert_nil url
  end

  test 'should generate link with custom keys for whitelabel build' do
    BranchmetricsApi.unstub(:generate_link)
    api_key = "edcast-branchmetrics-key"
    # BranchmetricsApi.expects(:fetch_branchmetrics_api_key).once
    Organization
      .any_instance
      .expects(:get_settings_for)
      .with('whitelabel_branchmetrics_api_key')
      .once.returns(api_key)

    org = create(:organization, whitelabel_build_enabled: true)

    payload = {'first_name' => 'a', 'last_name' => 'b',
               'email' => 'a@a.com', 'token' => '123'}

    stub_request(:post, "https://api.branch.io/v1/url").
      with(:body => {branch_key: api_key, data: payload}.to_json,
           :headers => {
             'Accept'=>'*/*',
             'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
             'Content-Type'=>'application/json',
             'User-Agent'=>/.*/}).
             to_return(:status => 200, :body => "{\"url\":\"http://bitly.com\"}", :headers => {})
    url = BranchmetricsApi.generate_link(payload, organization: org)
    assert_not_nil url
    assert_equal 'http://bitly.com', url
  end
end

require 'test_helper'
class PeriodOffsetCalculatorTest < ActiveSupport::TestCase

  test 'timerange' do
    week0_begin = PeriodOffsetCalculator::BEGINNING_OF_TIME
    week0_end = PeriodOffsetCalculator::BEGINNING_OF_TIME + 1.week
    assert_equal [week0_begin, week0_end], PeriodOffsetCalculator.timerange(period: 'week', offset: 0)
  end

  test 'drill down to days from week' do
    assert_equal([:day, [0,1,2,3,4,5,6]], PeriodOffsetCalculator.drill_down_period_and_offsets(period: :week, offset: 0))
  end

  test 'drill down to weeks from month' do
    month_1_begin_offset = ((Time.parse("2015-02-01T00:00:00+00:00") - PeriodOffsetCalculator::BEGINNING_OF_TIME) / 1.day).floor
    month_1_end_offset = (((Time.parse("2015-03-01T00:00:00+00:00") - PeriodOffsetCalculator::BEGINNING_OF_TIME) / 1.day) - 1).floor
    assert_equal([:day, (month_1_begin_offset..month_1_end_offset).to_a], PeriodOffsetCalculator.drill_down_period_and_offsets(period: :month, offset: 1))
  end

  test 'drill down to months from year' do
    assert_equal([:month, [0,1,2,3,4,5,6,7,8,9,10,11]], PeriodOffsetCalculator.drill_down_period_and_offsets(period: :year, offset: 0))
  end

  test 'drill down to day is just day' do
    assert_equal([:day, [11]], PeriodOffsetCalculator.drill_down_period_and_offsets(period: :day, offset: 11))
  end

  test 'drill down time ranges' do
    begin_week, end_week = PeriodOffsetCalculator.timerange(period: 'week', offset: 3)
    assert_equal([[begin_week, begin_week+1.day],
                 [begin_week+1.day, begin_week+2.days],
                 [begin_week+2.days, begin_week+3.days],
                 [begin_week+3.days, begin_week+4.days],
                 [begin_week+4.days, begin_week+5.days],
                 [begin_week+5.days, begin_week+6.days],
                 [begin_week+6.days, begin_week+7.days]], PeriodOffsetCalculator.drill_down_timeranges(period: 'week', offset: 3))

  end

  test 'drill down time ranges for time range' do
    start_date = Time.now.utc - 5.days
    end_date = Time.now.utc
    assert_equal([[start_date, start_date + 1.day].map(&:to_i),
                  [start_date + 1.days, start_date + 2.days].map(&:to_i),
                  [start_date + 2.days, start_date + 3.days].map(&:to_i),
                  [start_date + 3.days, start_date + 4.days].map(&:to_i),
                  [start_date + 4.days, start_date + 5.days].map(&:to_i)], PeriodOffsetCalculator.drill_down_timeranges_for_timerange(period: 'day', timerange: [start_date, end_date]).map {|tr| tr.map(&:to_i)})
  end

  test 'drill down time ranges for same day' do
    now = Time.now.utc.beginning_of_day
    start_date = now
    end_date = now
    assert_equal([[start_date, end_date + 1.day].map(&:to_i)], PeriodOffsetCalculator.drill_down_timeranges_for_timerange(period: 'day', timerange: [start_date, end_date]).map {|tr| tr.map(&:to_i)})
  end
end

require 'test_helper'
class AwsSesMailerTest < ActiveSupport::TestCase

  setup do
    #unstub
    AWS.config(:stub_requests => false)
  end

  teardown do
    #restub
    AWS.stub!
  end

  class SesTestSyncMailer < ActionMailer::Base
    cattr_accessor :http_handler
    curb_http_handler = AWS::Core::Http::CurbHandler.new
    self.http_handler = AWS::Core::Http::Handler.new(curb_http_handler) { |req, resp|
      super(req, resp)
    }
    self.delivery_method = :amazon_ses
    self.amazon_ses_settings = {config: AWS::Core::Configuration.new(http_handler: self.http_handler)}

    def send_test_email
      mail(to: 'trung@edcast.com', from: 'author@example.com', delivery_method_options: {},
           subject: 'Welcome to My Awesome Site') do |format|
        format.html { render html: 'html text' }
        format.text { render text: 'plain text' }
      end
    end
  end


  class SesTestAsyncMailer < ActionMailer::Base
    cattr_accessor :http_handler
    curb_http_handler = AWS::Core::Http::CurbHandler.new
    self.http_handler = AWS::Core::Http::Handler.new(curb_http_handler) { |req, resp|
      super(req, resp)
    }
    self.delivery_method = :amazon_ses
    self.amazon_ses_settings = {config: AWS::Core::Configuration.new( http_handler: self.http_handler), async: true}
    def send_test_email
      mail(to: 'trung@edcast.com', from: 'author@example.com', delivery_method_options: {},
           subject: 'Welcome to My Awesome Site') do |format|
        format.html { render html: 'html text' }
        format.text { render text: 'plain text' }
      end
    end
  end



  test 'should send out the test email async' do
    SesTestAsyncMailer.http_handler.expects(:handle_async)
    assert_nothing_raised do
      SesTestAsyncMailer.send_test_email.deliver_later
    end
  end

  test 'should send out the test email sync' do
    SesTestSyncMailer.http_handler.expects(:handle)
    SesTestSyncMailer.http_handler.expects(:handle_async).never
    assert_nothing_raised do
      SesTestSyncMailer.send_test_email.deliver_later
    end
  end

end
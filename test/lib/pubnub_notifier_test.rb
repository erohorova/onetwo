require 'test_helper'
class PubnubNotifierTest < ActiveSupport::TestCase

  setup do
    NotificationGeneratorJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    PubnubNotifier.unstub(:send_payload)
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(false)
  end

  teardown do
    Organization.any_instance.unstub(:notifications_and_triggers_enabled?)
  end

  test 'send notification for scheduled stream reminder' do
    org = create(:organization)
    creator = create(:user, organization: org)
    v = create(:wowza_video_stream, status: 'upcoming', creator: creator)

    content = "Time to start your stream: #{v.snippet}"
    opts, payload = opts_and_payload_for_video_stream(v, content, 'scheduled_stream_reminder')
    pubnub = mock()
    pubnub.expects(:publish).with(has_entries(channel: creator.private_pubnub_channel_name, message: payload)).returns(nil).once
    Pubnub.expects(:new).returns(pubnub).once
    PubnubNotifier.notify(opts)
  end

  test 'send notifications if triggers feature is disabled' do
    org = create(:organization)
    creator = create(:user, organization: org)
    v = create(:wowza_video_stream, status: 'upcoming', creator: creator)

    content = "Time to start your stream: #{v.snippet}"
    opts, payload = opts_and_payload_for_video_stream(v, content, 'scheduled_stream_reminder')
    pubnub = mock()
    pubnub.expects(:publish).with(has_entries(channel: creator.private_pubnub_channel_name, message: payload)).returns(nil).once
    Pubnub.expects(:new).returns(pubnub).once
    PubnubNotifier.notify(opts)
  end

  test 'should not send notification if notifications and triggers feature is enabled' do
    org = create(:organization)
    creator = create(:user, organization: org)
    v = create(:wowza_video_stream, status: 'upcoming', creator: creator)

    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(true)

    content = "Time to start your stream: #{v.snippet}"
    opts, payload = opts_and_payload_for_video_stream(v, content, 'scheduled_stream_reminder')
    pubnub = mock()
    pubnub.expects(:publish).with(has_entries(channel: creator.private_pubnub_channel_name, message: payload)).returns(nil).never
    Pubnub.expects(:new).returns(pubnub).never
    PubnubNotifier.notify(opts)
  end

  test 'send notification for scheduled stream' do
    org = create(:organization)
    creator = create(:user, organization: org)
    v = create(:wowza_video_stream, status: 'upcoming', creator: creator)

    content = "#{v.creator.name} scheduled a live stream: '#{v.snippet}'"
    opts, payload = opts_and_payload_for_video_stream(v, content, 'stream_scheduled')

    pubnub = mock()
    pubnub.expects(:publish).with(has_entries(channel: creator.public_pubnub_channel_name, message: payload)).returns(nil).once
    Pubnub.expects(:new).returns(pubnub).once
    PubnubNotifier.notify(opts)
  end

  test 'send notification for live stream' do
    org = create(:organization)
    creator = create(:user, organization: org)
    v = create(:wowza_video_stream, status: 'live', creator: creator)

    content = "#{v.creator.name} is now live: '#{v.snippet}'"
    opts, payload = opts_and_payload_for_video_stream(v, content, 'stream_started')

    pubnub = mock()
    pubnub.expects(:publish).with(has_entries(channel: creator.public_pubnub_channel_name, message: payload)).returns(nil).once
    Pubnub.expects(:new).returns(pubnub).once
    PubnubNotifier.notify(opts)
  end

  test 'video stream push to public channel and followers' do
    org = create(:organization)
    creator = create(:user, organization: org)
    public_channel = create(:channel, :robotics, organization: org)
    v = create(:wowza_video_stream, status: 'live', creator: creator)
    v.channels << public_channel

    user_follows_creator = create(:user, organization: org)
    user_follows_channel = create(:user, organization: org)
    user_follows_creator_and_channel = create(:user, organization: org)

    Follow.create!(user: user_follows_creator, followable: creator, skip_notification: true)
    Follow.create!(user: user_follows_channel, followable: public_channel, skip_notification: true)
    Follow.create!(user: user_follows_creator_and_channel, followable: creator, skip_notification: true)
    Follow.create!(user: user_follows_creator_and_channel, followable: public_channel, skip_notification: true)


    content = "#{v.creator.name} is now live: '#{v.snippet}'"
    opts, payload = opts_and_payload_for_video_stream(v, content, 'stream_started')

    pubnub = mock()

    #notify user_follows_creator, and user_follows_creator_and_channel
    pubnub.expects(:publish).with(has_entries(channel: creator.public_pubnub_channel_name, message: payload)).returns(nil).once

    #notify user_follows_channel
    pubnub.expects(:publish).with(has_entries(channel: user_follows_channel.private_pubnub_channel_name, message: payload)).returns(nil).once

    Pubnub.expects(:new).returns(pubnub).once
    PubnubNotifier.notify(opts)
  end

  test 'video stream push for private channel videostream' do
    org = create(:organization)
    creator = create(:user, organization: org)
    channel1 = create(:channel, is_private: true, organization: org)
    channel2 = create(:channel, is_private: true, organization: org)
    u1, u2, u3 = create_list(:user, 3, organization: org)

    create(:follow, followable: creator, user: u1)
    create(:follow, followable: channel1, user: u2)

    # u3 is following both channels
    create(:follow, followable: channel1, user: u3)
    create(:follow, followable: channel2, user: u3)

    v = create(:wowza_video_stream, status: 'live', creator: creator)
    v.channels << channel1
    v.channels << channel2

    content = "#{v.creator.name} is now live: '#{v.snippet}'"
    opts, payload = opts_and_payload_for_video_stream(v, content, 'stream_started')

    pubnub = mock()
    pubnub.expects(:publish).with(has_entries(channel: u2.private_pubnub_channel_name, message: payload)).returns(nil).once
    pubnub.expects(:publish).with(has_entries(channel: u3.private_pubnub_channel_name, message: payload)).returns(nil).once
    pubnub.expects(:publish).with(has_entries(channel: creator.public_pubnub_channel_name, message: payload)).returns(nil).never
    Pubnub.expects(:new).returns(pubnub).once
    PubnubNotifier.notify(opts)
  end

  test 'announcement notification' do
    g = create(:group)
    users = create_list(:user, 4)
    admin = users.last
    members = users[0..2]
    g.add_admin(admin)
    g.add_members members
    a = create(:announcement, user: admin, group: g)

    content = "#{admin.name} made a new announcement: #{a.snippet}"

    opts = {
      content: content,
      item: a,
      notification_type: 'new_announcement',
      deep_link_id: a.id,
      deep_link_type: 'post',
      excluded_user_ids: [a.user_id],
      host_name: a.user.organization.host

    }

    payload = {
      pn_apns: {
        aps: {
          alert: content,
          sound: 'default',
          badge: 1
        },
        deep_link_id: a.id,
        deep_link_type: 'post',
        notification_type: 'new_announcement',
        host_name: opts[:host_name]
      },
      pn_gcm: {
        data: {
          summary: content,
          deep_link_id: a.id,
          deep_link_type: 'post',
          notification_type: 'new_announcement',
          host_name: opts[:host_name]
        }
      }
    }

    pubnub = mock()
    members.each do |member|
      pubnub.expects(:publish).with(has_entries(channel: member.private_pubnub_channel_name, message: payload)).returns(nil).once
    end
    Pubnub.expects(:new).returns(pubnub).once
    PubnubNotifier.notify(opts)
  end

  test 'comment notification on card' do
    org = create(:organization)
    u, u1, u2 = create_list(:user, 3, organization: org)
    c = create(:card, card_type: 'media', card_subtype: 'text',
      author_id: u.id, is_public: true)
    c1 = create(:comment, user: u1, commentable: c)
    c2 = create(:comment, user: u2, commentable: c)

    # Another one by u1, should notify u and u3
    c3 = create(:comment, user: u1, commentable: c)

    content = "some message"

    opts = {
      content: content,
      item: c3,
      notification_type: 'new_comment',
      deep_link_id: c.id,
      deep_link_type: 'card',
      excluded_user_ids: [u1.id],
      host_name: u1.organization.host
    }

    payload = {
      pn_apns: {
        aps: {
          alert: content,
          sound: 'default',
          badge: 1
        },
        deep_link_id: c.id,
        deep_link_type: 'card',
        notification_type: 'new_comment',
        host_name: opts[:host_name]
      },
      pn_gcm: {
        data: {
          summary: content,
          deep_link_id: c.id,
          deep_link_type: 'card',
          notification_type: 'new_comment',
          host_name: opts[:host_name]
        }
      }
    }


    pubnub = mock()
    [u,u2].each do |subscriber|
      pubnub.expects(:publish).with(has_entries(channel: subscriber.private_pubnub_channel_name, message: payload)).returns(nil).once
    end
    Pubnub.expects(:new).returns(pubnub).once
    PubnubNotifier.notify(opts)

    # should return as is for other card types. no notificationsui_layout_type
    poll_card = create(:card, card_type: 'poll', card_subtype: 'text', author_id: u.id, is_public: true)
    c = create(:comment, user: u1, commentable: poll_card)
    opts = {
      content: content,
      item: c,
      notification_type: 'new_comment',
      deep_link_id: poll_card.id,
      deep_link_type: 'card',
      excluded_user_ids: [u1.id],
      host_name: u1.organization.host
    }

    PubnubNotifier.expects(:send_payload).never
    PubnubNotifier.notify(opts)
  end

  test 'comment notification on post' do
    u, u1, u2 = create_list(:user, 3)
    g = create(:group)
    [u, u1, u2].each {|i| g.add_member i}
    p = create(:post, user: u, group: g)
    c1 = create(:comment, user: u1, commentable: p)
    c2 = create(:comment, user: u2, commentable: p)

    # Another one by u1, should notify u and u3
    c3 = create(:comment, user: u1, commentable: p)

    content = "some message"

    opts = {
      content: content,
      item: c3,
      notification_type: 'new_comment',
      deep_link_id: p.id,
      deep_link_type: 'post',
      excluded_user_ids: [u1.id],
      host_name: p.user.organization.host
    }

    payload = {
      pn_apns: {
        aps: {
          alert: content,
          sound: 'default',
          badge: 1
        },
        deep_link_id: p.id,
        deep_link_type: 'post',
        notification_type: 'new_comment',
        host_name: opts[:host_name]
      },
      pn_gcm: {
        data: {
          summary: content,
          deep_link_id: p.id,
          deep_link_type: 'post',
          notification_type: 'new_comment',
          host_name: opts[:host_name]
        }
      }
    }


    pubnub = mock()
    [u,u2].each do |subscriber|
      pubnub.expects(:publish).with(has_entries(channel: subscriber.private_pubnub_channel_name, message: payload)).returns(nil).once
    end
    Pubnub.expects(:new).returns(pubnub).once
    PubnubNotifier.notify(opts)
  end

  test 'channel invite notifications' do
    user    = create(:user)
    channel  = create(:channel, :robotics)

    content = "added you as a author"

    opts = {
      content: content,
      item: channel,
      notification_type: 'added_author',
      deep_link_id: channel.id,
      deep_link_type: 'channel',
      user_id: user.id,
      excluded_user_ids: [],
      host_name: user.organization.host
    }

    payload = {
      pn_apns: {
        aps: {
          alert: content,
          sound: 'default',
          badge: 1
        },
        deep_link_id: channel.id,
        deep_link_type: 'channel',
        notification_type: 'added_author',
        host_name: opts[:host_name]
      },
      pn_gcm: {
        data: {
          summary: content,
          deep_link_id: channel.id,
          deep_link_type: 'channel',
          notification_type: 'added_author',
          host_name: opts[:host_name]
        }
      }
    }


    pubnub = mock()
    pubnub.expects(:publish).with(has_entries(channel: user.private_pubnub_channel_name, message: payload)).returns(nil).once
    Pubnub.expects(:new).returns(pubnub).once
    PubnubNotifier.notify(opts)
  end

  test 'admin generated notifications' do
    user    = create(:user)

    content = "added you as a author"

    opts = {
      content: content,
      notification_type: 'AdminGenerated',
      deep_link_id: 1,
      deep_link_type: 'card',
      user_id: user.id,
      host_name: user.organization.host
    }

    payload = {
      pn_apns: {
        aps: {
          alert: content,
          sound: 'default',
          badge: 1
        },
        deep_link_id: 1,
        deep_link_type: 'card',
        notification_type: 'AdminGenerated',
        host_name: opts[:host_name]
      },
      pn_gcm: {
        data: {
          summary: content,
          deep_link_id: 1,
          deep_link_type: 'card',
          notification_type: 'AdminGenerated',
          host_name: opts[:host_name]
        }
      }
    }


    pubnub = mock()
    pubnub.expects(:publish).with(has_entries(channel: user.private_pubnub_channel_name, message: payload)).returns(nil).once
    Pubnub.expects(:new).returns(pubnub).once
    PubnubNotifier.notify(opts)
  end

  test 'assigned notifications' do
    user    = create(:user)

    content = "Admin assigned you something"
    assignment = create(:assignment, assignee: user)
    opts = {
      content: content,
      notification_type: 'assigned',
      deep_link_id: 1,
      deep_link_type: 'card',
      user_id: user.id,
      item: assignment,
      host_name: user.organization.host
    }

    payload = {
      pn_apns: {
        aps: {
          alert: content,
          sound: 'default',
          badge: 1
        },
        deep_link_id: 1,
        deep_link_type: 'card',
        notification_type: 'assigned',
        host_name: opts[:host_name]
      },
      pn_gcm: {
        data: {
          summary: content,
          deep_link_id: 1,
          deep_link_type: 'card',
          notification_type: 'assigned',
          host_name: opts[:host_name]
        }
      }
    }


    pubnub = mock()
    pubnub.expects(:publish).with(has_entries(channel: user.private_pubnub_channel_name, message: payload)).returns(nil).once
    Pubnub.expects(:new).returns(pubnub).once
    PubnubNotifier.notify(opts)
  end

  test 'mention in card notifications' do
    org = create(:organization)
    user = create(:user, organization: org)
    card  = create(:card, author: create(:user, organization: org))
    content = "Mentioned in Card"
    Notification.any_instance.stubs(:message).returns(content)
    payload = {
      pn_apns: {
        aps: {
          alert: content,
          sound: 'default',
          badge: 1
        },
        deep_link_id: card.id,
        deep_link_type: 'card',
        notification_type: Notification::TYPE_MENTION_IN_CARD,
        host_name: org.host
      },
      pn_gcm: {
        data: {
          summary: content,
          deep_link_id: card.id,
          deep_link_type: 'card',
          notification_type: Notification::TYPE_MENTION_IN_CARD,
          host_name: org.host
        }
      }
    }

    pubnub = mock()
    pubnub.expects(:publish).with(has_entries(channel: user.private_pubnub_channel_name, message: payload)).returns(nil).once
    Pubnub.expects(:new).returns(pubnub).once
    PubnubNotifier.notify({deep_link_id: card.id, deep_link_type: 'card', item: card, notification_type: Notification::TYPE_MENTION_IN_CARD, content: content, host_name: org.host, user_id: user.id})
  end

  test 'mention in comment notifications' do
    org = create(:organization)
    user = create(:user, organization: org)
    video_stream  = create(:iris_video_stream, creator: create(:user, organization: org))
    comment = create(:comment, commentable: video_stream)
    content = "Mentioned in comment"
    Notification.any_instance.stubs(:message).returns(content)
    payload = {
      pn_apns: {
        aps: {
          alert: content,
          sound: 'default',
          badge: 1
        },
        deep_link_id: video_stream.id,
        deep_link_type: 'video_stream',
        notification_type: Notification::TYPE_MENTION_IN_COMMENT,
        host_name: org.host
      },
      pn_gcm: {
        data: {
          summary: content,
          deep_link_id: video_stream.id,
          deep_link_type: 'video_stream',
          notification_type: Notification::TYPE_MENTION_IN_COMMENT,
          host_name: org.host
        }
      }
    }

    pubnub = mock()
    pubnub.expects(:publish).with(has_entries(channel: user.private_pubnub_channel_name, message: payload)).returns(nil).once
    Pubnub.expects(:new).returns(pubnub).once
    PubnubNotifier.notify({deep_link_id: video_stream.id, deep_link_type: 'video_stream', item: video_stream, notification_type: Notification::TYPE_MENTION_IN_COMMENT, content: content, host_name: org.host, user_id: user.id})
  end

  test 'send notification using notify_v2' do
    org = create(:organization)
    card = create(:card, organization: org, author: nil)
    opts = {
      content: "Test message",
      notification_type: 'notification',
      deep_link_id: card.id,
      deep_link_type: 'card',
      host_name: org.host,
      channel_names: [org.public_pubnub_channel_name]
    }

    payload = PubnubNotifier.form_payload(opts)
    pubnub = mock()
    pubnub.expects(:publish).with(has_entries(channel: org.public_pubnub_channel_name, message: payload)).returns(nil).once
    Pubnub.expects(:new).returns(pubnub).once
    PubnubNotifier.notify_v2(opts)
  end

  test 'form payload' do
    org = create(:organization)
    card = create(:card, organization: org, author: nil)
    opts = {
      content: "Test message",
      notification_type: 'notification',
      deep_link_id: card.id,
      deep_link_type: 'card',
      host_name: org.host,
      channel_names: [org.public_pubnub_channel_name]
    }

    payload = {
      pn_gcm: {
        data: {
          summary: 'Test message',
          deep_link_id: card.id,
          deep_link_type: 'card',
          notification_type: 'notification',
          host_name: org.host
        }
      },
      pn_apns: {
        aps: {
          alert: 'Test message',
          sound: 'default',
          badge: 1
        },
        deep_link_id: card.id,
        deep_link_type: 'card',
        notification_type: 'notification',
        host_name: org.host
      }
    }

    assert_equal payload, PubnubNotifier.form_payload(opts)
  end

  test 'should invoke notification with custom pubnub keys' do
    custom_pubnub_keys = {subscribe_key: 'asb', publish_key: '123'}
    Organization.any_instance
      .expects(:get_settings_for)
      .with('whitelabel_pubnub_keys')
      .returns(custom_pubnub_keys)

    org = create(:organization, whitelabel_build_enabled: true)
    creator = create(:user, organization: org)
    v = create(:wowza_video_stream, status: 'live', creator: creator)

    content = "#{v.creator.name} is now live: '#{v.snippet}'"
    opts, payload = opts_and_payload_for_video_stream(v, content, 'stream_started')

    pubnub = mock()
    pubnub.expects(:publish)
      .with(has_entries(channel: creator.public_pubnub_channel_name, message: payload))
      .returns(nil).once
    Pubnub.expects(:new)
      .with(has_entries(custom_pubnub_keys))
      .returns(pubnub).once
    PubnubNotifier.notify(opts)
  end

  def opts_and_payload_for_video_stream(v, content, notification_type)
    opts = {
      content: content,
      item: v.card,
      notification_type: notification_type,
      deep_link_id: v.card.id,
      deep_link_type: 'card',
      excluded_user_ids: [v.creator_id],
      host_name: v.creator.organization.host
    }

    payload = {
      pn_apns: {
        aps: {
          alert: content,
          sound: 'default',
          badge: 1
        },
        deep_link_id: v.card.id,
        deep_link_type: 'card',
        notification_type: notification_type,
        host_name: opts[:host_name]
      },
      pn_gcm: {
        data: {
          summary: content,
          deep_link_id: v.card.id,
          deep_link_type: 'card',
          notification_type: notification_type,
          host_name: opts[:host_name]
        }
      }
    }
    [opts, payload]
  end
end

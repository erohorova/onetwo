require 'test_helper'
class InfluxdbRecorder::DuplicateCheckerTest < ActiveSupport::TestCase

  setup do
    # The code tested here is written in a module that is extended to a class.
    # The methods are invoked through the class, but the constants are looked
    # up through the module.
    @klass = InfluxdbRecorder
    @module = @klass::DuplicateChecker
    # A static list of attributes for each event.
    @unique_event_attrs = @module::UNIQUE_EVENT_ATTRIBUTES
    # The lifetime of the cache data
    @expires_in = @module::DUPLICATE_CACHE_EXPIRES_IN
    # A sample measurement, doesn't matter what it is
    @measurement = "foobar"
    # A sample event that is included in @unique_event_attrs
    @event = "card_deleted"
    # One point in time
    base_time = Time.now
    @time1 = base_time.to_i
    # Another point in time, 2 seconds after the first
    @time2 = (base_time + 2.second).to_i
    # Some fake values, don't have to really exist in the database
    @org_id = "0"
    @card_id = "1"
    # The record that will be run through the system
    @record = {
      timestamp: @time1,
      tags: {
        org_id: @org_id,
        event: @event
      },
      values: {
        card_id: @card_id
      }
    }
    @record2 = @record.merge(timestamp: @time2)
    # The expected digest of the record's attributes
    @digest = [@org_id, @card_id].hash
    # The expected key that the record will be cached to
    @cache_key = @klass.build_cache_key(@event, @digest)
    # Stuff required for testing InfluxdbRecorder
    # (which calls duplicate remover methods)
    @measurement = "foobar"
    @write_client = INFLUXDB_CLIENT
    @write_client.unstub(:write_point)
    @klass.unstub(:record)
  end

  test 'has the correct events in the config file' do
    all_event_keys = [
      *Analytics::MetricsRecorder.load_event_recorder_mapping.keys,
      "user_score"
    ]
    assert all_event_keys.all? { |event| @unique_event_attrs.key? event }
  end

  test 'Raise an exception of not registered in duplicate checker file' do
    @record[:tags].merge! event: 'iam-an-unregistered-event'
    assert_raises do
      @klass.is_duplicate?(@measurement, @record)
    end
  end

  test 'config file is correctly formatted' do
    @unique_event_attrs.values.each do |val|
      assert_equal Array, val.class
      assert val.length > 0
      val.each do |attr_path|
        assert_equal Array, attr_path.class
        assert_equal 2, attr_path.length
        assert ['tags', 'values'].include?(attr_path[0])
        assert_equal String, attr_path[1].class
      end
    end
  end

  test '.build_cache_key' do
    # The cache key is all these components joined together
    expected = [
      "duplicate_checker",
      "test",
      @event,
      @digest
    ].join("/")
    actual = @klass.build_cache_key(@event, @digest)
    assert_equal expected, actual
  end

  test '.is_duplicate? returns false if the element is not found' do
    Rails.cache.expects(:write).once.with(
      @cache_key,
      @time1,
      expires_in: @expires_in
    )
    is_dup = @klass.is_duplicate?(@measurement, @record)
    assert_equal false, is_dup
  end

  test '.is_duplicate? returns true if the element is found and has recent time' do
    @klass.expects(:duplicate_cache_get).with(@cache_key).returns @time1
    Rails.cache.expects(:write).never
    is_dup = @klass.is_duplicate?(@measurement, @record)
    assert_equal true, is_dup
  end

  test '.is_duplicate? returns false if the element is found and has old time' do
    @klass.expects(:duplicate_cache_get).with(@cache_key).returns @time1
    Rails.cache.expects(:write).once.with(
      @cache_key,
      @time2,
      expires_in: @expires_in
    )
    is_dup = @klass.is_duplicate?(@measurement, @record2)
    assert_equal false, is_dup
  end

  test '.is_duplicate? returns false if redis-lock raises an error' do
    error = Redis::CommandError.new
    REDIS_LOCK_MANAGER.expects(:lock).raises(error)
    Bugsnag.expects(:notify).with error, severity: "error"

    is_dup = @klass.is_duplicate?(@measurement, @record2)
    refute is_dup
  end

  test '.is_duplicate? returns false if cache write raises an error' do
    error = Redis::CommandError.new
    Rails.cache.expects(:write).raises error
    Bugsnag.expects(:notify).with error, severity: "error"

    is_dup = @klass.is_duplicate?(@measurement, @record2)
    refute is_dup
  end

  # =========================================
  # NOTE: the following tests are basically the same as the above ones,
  # except that .is_duplicate? gets run through InfluxdbRecorder.record,
  # not directly
  # =========================================

  test '.record completes if the element is not found in cache' do
    Rails.cache.expects(:write).once.with(
      @cache_key,
      @time1,
      expires_in: @expires_in
    )
    @write_client.expects(:write_point).with(@measurement, @record)
    @klass.record(@measurement, @record)
  end

  test '.record returns early if element is found in cache and has recent time' do
    @klass.expects(:duplicate_cache_get).with(@cache_key).returns @time1
    Rails.cache.expects(:write).never
    @write_client.expects(:write_point).never
    @klass.record(@measurement, @record)
  end

  test '.record completes if element is found in cache and has old time' do
    @klass.expects(:duplicate_cache_get).with(@cache_key).returns @time1
    Rails.cache.expects(:write).once.with(
      @cache_key,
      @time2,
      expires_in: @expires_in
    )
    @write_client.expects(:write_point).with(
      @measurement,
      @record2
    )
    @klass.record(@measurement, @record2)
  end

  test 'special-cases records in the user_scores table' do
    role = "some_role"
    event = "some_event"
    org_id = "1"
    user_id = "2"
    owner_id = "3"
    actor_id = "4"
    timestamp = 123456

    attrs_digest = [
      role, event, org_id, user_id, owner_id, actor_id
    ].hash.to_s
    cache_key =       [
      "duplicate_checker",
      Rails.env,
      "user_score",
      attrs_digest
    ].join("/")

    record = {
      timestamp: timestamp,
      tags: {
        event: event,
        role: role,
        user_id: user_id,
        org_id: org_id
      },
      values: {
        actor_id: actor_id,
        owner_id: owner_id
      }
    }

    @klass.expects(:duplicate_cache_get).with(cache_key).returns timestamp
    @write_client.expects(:write_point).never
    @klass.record("user_scores", record)

  end

end

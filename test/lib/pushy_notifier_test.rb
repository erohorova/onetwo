require 'test_helper'
class PushyNotifierTest < ActiveSupport::TestCase
  test 'pushy api called if user is in china region' do
    org = create(:organization)
    user = create(:user, organization: org)
    user.profile = create(:user_profile, time_zone: 'Asia/Shanghai')
    payload = {
               user_id: user.id, 
               deep_link_id: 311360, 
               deep_link_type: "card", 
               notification_type: "assigned_content", 
               host_name: "#{org.host_name}.#{Settings.platform_domain}", 
               content: "Mansi Panchmia has assigned you: New Journey 15th June...", 
               channel_names: ["User-72832-private-a81f3a3b46f897ff7312d3a00d1b0e6f"]
             }

    send_payload = {
             summary: payload[:content],
             deep_link_id: payload[:deep_link_id].to_s,
             deep_link_type: payload[:deep_link_type],
             notification_type: payload[:notification_type],
             host_name: payload[:host_name]
            }

  api_key = 'f2cfbcf68b6fcf4c3008e055b3cc70f19de09344153bcfaa6dd5b3d239cef657'                        

  PushyNotifier.expects(:send_push).with(api_key, payload[:channel_names][0], send_payload).once
  PushyNotifier.notify(payload)
  end

  test 'pushy api not called if user is not in china region' do
    org = create(:organization)
    user = create(:user, organization: org)
    user.profile = create(:user_profile, time_zone: 'America/Los_Angeles')
    payload = {
               user_id: user.id, 
               deep_link_id: 311360, 
               deep_link_type: "card", 
               notification_type: "assigned_content", 
               host_name: "qa.lvh.me", 
               content: "Mansi Panchmia has assigned you: New Journey 15th June...", 
               channel_names: ["User-72832-private-a81f3a3b46f897ff7312d3a00d1b0e6f"]
             }

  PushyNotifier.expects(:send_push).never
  PushyNotifier.notify(payload)
  end
end
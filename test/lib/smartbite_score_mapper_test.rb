require 'test_helper'
class SmartbiteScoreMapperTest < ActiveSupport::TestCase
  include SmartbiteScoreMapper

  test 'smartbite score increments' do
    actor_id = 1
    owner_id = 2
    ts = Time.now
    opts = {owner_id: owner_id, action: "like", actor_id: actor_id, object_id: 1, object_type: 'card', created_at: ts.to_i}
    expected_increments = [[actor_id, 4], [owner_id, 10]]
    assert_equal expected_increments, SmartbiteScoreMapper.smartbite_score_increments(opts)
  end

  test 'award only actor points if actor equals to owner' do
    actor_id = 1
    owner_id = 1
    ts = Time.now
    opts = {owner_id: owner_id, action: "like", actor_id: actor_id, object_id: 1, object_type: 'card', created_at: ts.to_i}
    expected_increments = [[actor_id, 4]]
    assert_equal expected_increments, SmartbiteScoreMapper.smartbite_score_increments(opts)
  end

  test 'smartbite score increments to join team' do
    actor_id = 1
    owner_id = 2
    ts = Time.now
    opts = {owner_id: owner_id, action: "join", actor_id: actor_id, object_id: 1, object_type: 'team', created_at: ts.to_i}
    expected_increments = [[actor_id, 4], [owner_id, 0]]
    assert_equal expected_increments, SmartbiteScoreMapper.smartbite_score_increments(opts)
  end

  test 'smartbite score increments on clicking playback' do
    actor_id = 1
    owner_id = 2
    ts = Time.now
    opts = {owner_id: owner_id, action: "playback", actor_id: actor_id, object_id: 1, object_type: 'card', created_at: ts.to_i}
    expected_increments = [[actor_id, 10], [owner_id, 20]]
    assert_equal expected_increments, SmartbiteScoreMapper.smartbite_score_increments(opts)
  end

  test 'smartbite score increments for creating normal and poll card' do
    actor_id = 1
    owner_id = 2
    ts = Time.now
    opts = {owner_id: owner_id, action: "create", actor_id: actor_id, object_id: 1, object_type: 'card', created_at: ts.to_i}
    expected_increments = [[actor_id, 40], [owner_id, 0]]
    assert_equal expected_increments, SmartbiteScoreMapper.smartbite_score_increments(opts)

    opts = {owner_id: owner_id, action: "poll_create", actor_id: actor_id, object_id: 1, object_type: 'card', created_at: ts.to_i}
    expected_increments = [[actor_id, 20], [owner_id, 0]]
    assert_equal expected_increments, SmartbiteScoreMapper.smartbite_score_increments(opts)
  end

  test 'smartbite score increments for creating videostream card' do
    actor_id = 1
    owner_id = 2
    ts = Time.now
    opts = {owner_id: owner_id, action: "create", actor_id: actor_id, object_id: 1, object_type: 'card', created_at: ts.to_i}
    expected_increments = [[actor_id, 40], [owner_id, 0]]
    assert_equal expected_increments, SmartbiteScoreMapper.smartbite_score_increments(opts)
    
    opts = {owner_id: owner_id, action: "videostream_create", actor_id: actor_id, object_id: 1, object_type: 'card', created_at: ts.to_i}
    expected_increments = [[actor_id, 80], [owner_id, 0]]
    assert_equal expected_increments, SmartbiteScoreMapper.smartbite_score_increments(opts)
  end
end

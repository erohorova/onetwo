require 'test_helper'
class SimpleMessageExtractorTest < ActiveSupport::TestCase

  test 'should get inner text' do
    stub_request(:get, 'https://www.youtube.com/').to_return(:status => 200, :body => "", :headers => {})
    message = '<div><p>this is a <b>bold</b> text.</p><p> This is a <i>italic text with a link </i><a href="https://www.youtube.com">youtube</a></p></div>'
    simple_text = SimpleMessageExtractor.simple_text(message)
    assert_equal 'this is a bold text. This is a italic text with a link youtube', simple_text

    # non html messages should work as fine
    message = 'just plain text'
    simple_text = SimpleMessageExtractor.simple_text(message)
    assert_equal 'just plain text', simple_text

    # blank messaegs should work fine too
    message = ''
    simple_text = SimpleMessageExtractor.simple_text(message)
    assert_equal '', simple_text
  end
end

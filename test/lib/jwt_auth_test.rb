require 'test_helper'

class JwtAuthTest < ActiveSupport::TestCase
  test 'should return token with new expire time' do
    create_default_org
    org = create(:organization)
    user = create(:user, organization: org)

    web_session_timeout = Faker::Number.between(5, 100)
    create(:config, name: 'web_session_timeout', value: web_session_timeout, data_type: 'integer', configable: org)

    new_token = JwtAuth.renew(user)
    decode_new_token = JwtAuth.decode(new_token)

    assert decode_new_token['exp']
    assert decode_new_token['exp'] > (Time.now + (web_session_timeout - 1).minutes).to_i
    assert decode_new_token['exp'] < (Time.now + (web_session_timeout + 1).minutes).to_i
  end
end
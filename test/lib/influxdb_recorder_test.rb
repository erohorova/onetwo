require 'test_helper'
class InfluxdbRecorderTest < ActiveSupport::TestCase

  setup do
    @analytics_version = Settings.influxdb.event_tracking_version
    @class = InfluxdbRecorder
    # seed randonmess, for consistency with random nanosecond variation
    @current_ns = Time.now.nsec
    now = Time.now
    Time.stubs(:now).returns now
    now.stubs(:nsec).returns @current_ns
  end

  test "analytics version" do
    assert_equal InfluxdbRecorder::ANALYTICS_VERSION, @analytics_version
  end

  test ".record" do
    @class.unstub(:record)
    measurement = "fake measurement"
    values = { fake_field: "fake field val" }
    tags = { fake_tag: "fake tag val" }
    timestamp = Time.now.to_i
    data = { values: values, tags: tags, timestamp: timestamp }
    data_with_tags =  data.clone.tap do |data|
      data[:tags].merge! analytics_version: @analytics_version
      data[:timestamp] = @class.to_ns(measurement, data[:timestamp])
    end
    INFLUXDB_CLIENT.unstub :write_point
    INFLUXDB_CLIENT.expects(:write_point).with(
      measurement,
      data_with_tags
    )
    @class.record measurement, data
  end

  test ".record pushes to input queue if org is configured to do so" do
    begin
      orig_const = @class::ORGS_USING_INFLUX_SERVICE
      @class::ORGS_USING_INFLUX_SERVICE = Set.new(['123'])
      @class.unstub(:record)
      measurement = "fake measurement"
      values = { fake_field: "fake field val" }
      tags = { fake_tag: "fake tag val", org_id: '123' }
      timestamp = Time.now.to_i
      data = { values: values, tags: tags, timestamp: timestamp }
      data_with_tags =  data.clone.tap do |data|
        data[:tags].merge! analytics_version: @analytics_version
        data[:timestamp] = @class.to_ns(measurement, data[:timestamp])
      end
      INFLUXDB_CLIENT.unstub :write_point
      INFLUXDB_CLIENT.expects(:write_point).never
      INFLUXDB_CLIENT.expects(:write_point_to_queue).with(
        measurement,
        data_with_tags
      )
      @class.record measurement, data
    ensure
      @class::ORGS_USING_INFLUX_SERVICE = orig_const
    end
  end  

  test ".record with custom write client" do
    @class.unstub(:record)
    measurement = "fake measurement"
    values = { fake_field: "fake field val" }
    tags = { fake_tag: "fake tag val" }
    timestamp = Time.now.to_i
    write_client = @class.new
    data = { values: values, tags: tags, timestamp: timestamp }
    data_with_tags =  data.clone.tap do |data|
      data[:tags].merge! analytics_version: @analytics_version
      data[:timestamp] = @class.to_ns(measurement, data[:timestamp])
    end
    write_client.expects(:write_point).with(
      measurement,
      data_with_tags
    )
    @class.record measurement, data, write_client: write_client
  end

  test '.bulk_record' do
    measurement = "foo"
    record = {
      tags: { foo: "bar" },
      values: { bar: "foo" },
      timestamp: Time.now.to_i
    }
    record_with_analytics_version = record.clone.tap do |data|
      data[:tags][:analytics_version] = @analytics_VERSION
      data[:timestamp] = @class.to_ns(measurement, data[:timestamp])
    end
    data_points_with_series = [
      record_with_analytics_version.merge(series: measurement)
    ]
    INFLUXDB_CLIENT.unstub(:write_points)
    INFLUXDB_CLIENT.expects(:write_points).with(data_points_with_series)
    @class.bulk_record(measurement, [record])
  end

  test '.bulk_record writes to input queue if org is configured to do so' do
    begin
      orig_const = @class::ORGS_USING_INFLUX_SERVICE
      @class::ORGS_USING_INFLUX_SERVICE = Set.new(['123'])

      measurement = "foo"
      # This record will get pushed to queue
      record1 = {
        tags: { foo: "bar", org_id: '123' },
        values: { bar: "foo" },
        timestamp: Time.now.to_i
      }
      # This record will get pushed directly
      record2 = {
        tags: { foo: "bar", org_id: '456' },
        values: { bar: "foo" },
        timestamp: Time.now.to_i
      }
      record1_with_analytics_version = record1.clone.tap do |data|
        data[:tags][:analytics_version] = @analytics_VERSION
        data[:timestamp] = @class.to_ns(measurement, data[:timestamp])
      end
      record2_with_analytics_version = record2.clone.tap do |data|
        data[:tags][:analytics_version] = @analytics_VERSION
        data[:timestamp] = @class.to_ns(measurement, data[:timestamp])
      end
      record1_data_points_with_series = [
        record1_with_analytics_version.merge(series: measurement)
      ]
      record2_data_points_with_series = [
        record2_with_analytics_version.merge(series: measurement)
      ]
      INFLUXDB_CLIENT.
        expects(:write_points_to_queue).
        with(record1_data_points_with_series)
      INFLUXDB_CLIENT.unstub(:write_points)
      INFLUXDB_CLIENT.
        expects(:write_points).
        with(record2_data_points_with_series)
      @class.bulk_record(measurement, [record1, record2])
    ensure
      @class::ORGS_USING_INFLUX_SERVICE = orig_const
    end
  end

  test '.bulk_record with custom write client' do
    measurement = "foo"
    write_client = InfluxDB::Client.new
    record = {
      tags: { foo: "bar" },
      values: { bar: "foo" },
      timestamp: Time.now.to_i
    }
    record_with_analytics_version = record.clone.tap do |data|
      data[:tags][:analytics_version] = @analytics_version
      data[:timestamp] = @class.to_ns(measurement, data[:timestamp])
    end
    data_points_with_series = [
      record_with_analytics_version.merge(series: measurement)
    ]
    write_client.expects(:write_points).with(data_points_with_series)
    @class.bulk_record(
      measurement,
      [record],
      write_client: write_client
    )
  end

  test '.merge_common_tags!' do
    data_1 = {
      tags: { foo: "bar" },
      values: { bar: "foo" },
   }
    assert_equal(
      {
        tags: {
          foo: "bar",
          analytics_version: @analytics_version,
        },
        values:{
          is_admin_request: "0",
          bar: "foo"
        }
      },
      @class.merge_common_tags!(data_1)
    )
  end

  test '.merge_common_tags! raises an error if there are no values' do
    data_1 = { tags: { foo: "bar" } }
    error = assert_raises ArgumentError do
      @class.merge_common_tags!(data_1)
    end
    assert_equal(
      "must include values key with influx metric",
      error.message
    )
  end

  test '.merge_series_for_bulk_record!' do
    measurement = "things"
    data_points = [{foo: "bar"}]
    assert_equal(
      [{foo: "bar", series: measurement}],
      @class.merge_series_for_bulk_record!(
        measurement,
        data_points
      )
    )
  end

  test '.current_ns returns 0 for aggregation measurements and current nsec otherwise' do
    @class.unstub(:current_ns)
    aggregations = %w{
      user_scores_daily
      org_scores_daily
      group_scores_daily
      group_user_scores_daily
      content_engagement_metrics
    }
    aggregations.each do |measurement|
      assert_equal "0", @class.current_ns(measurement)
    end
    assert_equal @current_ns.to_s, @class.current_ns("foobar")
  end

end

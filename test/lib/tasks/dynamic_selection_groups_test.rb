require 'test_helper'

class DynamicSelectionGroupsTest < ActiveSupport::TestCase
  setup do
    TestAfterCommit.enabled = true
    Edcast::Application.load_tasks
    @org = create(:organization)
    @user = create(:user, organization: @org)
    @team1, @team2, @team3, @team4 = create_list(:team, 4, organization: @org)
  end

  test 'should remove dynamic selection group if it not exists on workflow side' do
    existed_ds = create(
      :dynamic_group_revision,
      group_id: @team1.id,
      organization_id: @org.id,
      external_id: 200
    )
    non_existed_ds = create(
      :dynamic_group_revision,
      group_id: @team2.id,
      organization_id: @org.id,
      external_id: 404
    )
    dynamic_group = create(
      :dynamic_group_revision,
      group_id: @team3.id,
      organization_id: @org.id,
      external_id: nil
    )

    ds_rev_exception = create(
      :dynamic_group_revision,
      group_id: @team4.id,
      organization_id: @org.id,
      external_id: 500
    )

    stub_request(:get, 'http://workflow.test/api/dynamic-selections/500').
      to_raise(Faraday::TimeoutError)
    stub_request(:get, 'http://workflow.test/api/dynamic-selections/200').
      to_return(status: 201)
    stub_request(:get, 'http://workflow.test/api/dynamic-selections/404').
      to_return(status: 404)

    TestAfterCommit.with_commits do
      Rake::Task['dynamic_selection_groups:remove'].invoke
    end

    assert Team.exists?(id: @team3.id)
    assert DynamicGroupRevision.exists?(id: existed_ds.id)
    assert Team.exists?(id: @team1.id)
    assert DynamicGroupRevision.exists?(id: dynamic_group.id)
    assert Team.exists?(id: @team4.id)
    assert DynamicGroupRevision.exists?(id: ds_rev_exception.id)

    refute DynamicGroupRevision.exists?(id: non_existed_ds.id)
    refute Team.exists?(id: @team2.id)
  end
end

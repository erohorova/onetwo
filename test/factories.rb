FactoryGirl.define do

  factory :notification_entry do
    event_name Notify::EVENT_NEW_COMMENT
    notification_name Notify::NOTE_NEW_SMARTBITE_COMMENT
    user
    date_in_timezone Time.zone.today
    sending_time_in_timezone Time.zone.now

    # Mediums and intervals
    # Web
    config_web_single true
    status_web_single Notify::STATUS_UNSENT

    # Email
    config_email_single false
    status_email_single Notify::STATUS_UNSENT

    config_email_digest_daily false
    status_email_digest_daily Notify::STATUS_UNSENT

    config_email_digest_weekly false
    status_email_digest_weekly Notify::STATUS_UNSENT

    # Push
    config_push_single false
    status_push_single Notify::STATUS_UNSENT

    config_push_digest_daily false
    status_push_digest_daily Notify::STATUS_UNSENT

    config_push_digest_weekly false
    status_push_digest_weekly Notify::STATUS_UNSENT
  end

  factory :notification_config do
  end

  factory :topic_request do
    sequence :label do |n|
      "label#{n}"
    end
    association :approver, factory: :user
    association :requestor, factory: :user
  end

  factory :role_permission do
    role ""
  end

  factory :quarter do
    name 'quarter'
  end

  factory :user_role do
    role nil
    user nil
  end

  factory :skill do
    sequence(:name) {|n| "skill#{n}"}
  end

  factory :skills_user do
    skill
    user
    skill_level 'beginner'
  end

  factory :pwc_record do
    user
    skill { Taxonomies.domain_topics }
    level 1
    abbr "sb"
  end

  factory :invitation_file do
    association :sender
    roles "member"
    data "test@test.com"
  end

  factory :role do
    name 'member'
    master_role false
    organization
  end

  factory :clc do
    target_score { 50 }
    from_date { Time.now }
    to_date { (Time.now + 1.year) }
    organization
  end

  factory :price do
    card
    amount BigDecimal.new("100")
    currency {"USD"}
  end

  factory :organization_price do
    currency {"USD"}
  end

  factory :organization_profile do
    organization
  end

  factory :clc_organizations_record do
    score 1
    created_at { Time.now }
    organization
    user
    card
  end

  factory :clc_channels_record do
    score 1
    created_at { Time.now }
    channel
    user
    card
  end

  factory :clc_teams_record do
    score 1
    created_at { Time.now }
    team
    user
    card
  end

  factory :clc_progress do
    clc_score 5
    user
    clc
  end

  factory :users_integration do
    user_id 1
    integration_id 1
  end

  factory :users_course do
    user_id 1
    course_id 1
    status "MyString"
  end

  factory :external_course do
    integration
    name "MyString"
    description "MyString"
    image_url "MyString"
    course_url "MyString"
    course_key "MyString"
  end

  factory :users_external_course do
    external_course
    status "enrolled"
  end

  factory :integration do
    name "test"
    description "test description"
    enabled true
  end

  factory :teams_channels_follow do
    team
    channel
  end

  factory :teams_card do
    team
    card
  end

  factory :card_user_access do
    card
    user
  end

  factory :card_user_share do
    card
    user
  end

  factory :card_user_permission do
    card
    user
    show true
  end

  factory :card_team_permission do
    card
    team
    show true
  end

  factory :shared_card, :parent => :teams_card, :class => 'SharedCard' do
    team
    card
  end

  factory :channels_card do
    channel
    card
  end

  factory :channels_curator do
    channel
    user
  end

  factory :channels_user do
    channel
    user
    as_type 'author'
  end

  factory :school do
    name "test"
    specialization "test"
    from_year "2015"
    to_year "2016"
    user_id 1
    image Rack::Test::UploadedFile.new('test/fixtures/images/architecture.jpg', 'image/jpg')
  end

  factory :webex_config do
    client
    name "test"
    site_name "test"
    site_id "123"
    partner_id "123"
  end

  sequence :email_address do |n|
    "person#{n}@example.com"
  end

  sequence :name do |n|
    "client#{n}"
  end

  sequence :handle do |n|
    "@handle#{n}"
  end

  factory :user do
    is_complete true
    password User.random_password
    association :organization
    organization_role 'member'
    sequence :first_name do |n|
      "fname#{n}"
    end
    sequence :last_name do |n|
      "lname#{n}"
    end
    email {generate(:email_address)}
    handle {generate(:handle)}
    bio 'my bio'
    skip_generate_images true
    confirmed_at Time.now

    trait :admin do
      first_name "admin"
      last_name "admin"
      email "a@course-master.com"
      password User.random_password
      after(:create) do |user|
        SuperAdminUser.create!(user_id: user.id)
      end
    end

    trait :no_email do
      email nil
    end

    trait :with_onboarding_incomplete do
      after(:create) do |user|
        user.generate_user_onboarding
      end
    end

    trait :with_onboarding_complete do
      after(:create) do |user|
        if user.is_complete && user.generate_user_onboarding
          user.user_onboarding.start! && user.user_onboarding.complete!
        end
      end
    end
  end

  factory :client do
    name
    user
    redirect_uri 'https://www.example.com/'
  end

  factory :announcement do
    transient do
      group nil
      channel nil
    end
    user
    postable_id {group.nil? ? (channel.nil? ? create(:group).id : channel.id) : group.id}
    postable_type {group.nil? ? (channel.nil? ? 'Group' : 'Channel') : 'Group'}
    title "Announcement"
    message "This is an announcement"
  end

  factory :resource_group, aliases: [:group] do
    # organization
    association :creator, factory: :user
    name "group"
    description "just another group"
    course_code "CC1234"
    # client

    sequence :client_resource_id do |n|
      "client_resource_id_#{n}"
    end
    website_url "http://localhost:3000/ui/demo?mode=wide&group=first"
  end

  factory :private_group do
    # client
    # organization
    name "my private group"
    access 'open'
    association :creator, factory: :user
  end

  factory :report do
    association :reportable, factory: :post
    association :reporter, factory: :user
    type 'SpamReport'
    validity true
    trait :spam_report do
      type 'SpamReport'
    end

    trait :inappropriate_report do
      type 'InappropriateReport'
    end

    trait :compromised_report do
      type 'CompromisedReport'
    end
  end

  factory :groups_user do
    user
    association :group, factory: :resource_group
    as_type "member"
  end

  factory :mention do
    association :user
  end

  factory :resource do
    url 'http://www.google.com'
    site_name 'google'
    video_url 'http://www.google.com'
    image_url 'http://www.google.com'
    type 'Article'
    description 'description'
    title 'title'
    user_id nil
  end

  factory :sources_credential do
    organization
    sequence(:source_id) {|n| "source_#{n}"}
    sequence(:shared_secret) { |n| "xyz" + n.to_s }
  end

  factory :post do
    transient do
      group nil
      channel nil
    end
    user
    postable_id {group.nil? ? (channel.nil? ? create(:group).id : channel.id) : group.id}
    postable_type {group.nil? ? (channel.nil? ? 'Group' : 'Channel') : 'Group'}
    title "Post"
    message "This is a post"
    is_mobile false
  end

  factory :question do
    transient do
      group nil
      channel nil
    end
    user
    postable_id {group.nil? ? (channel.nil? ? create(:group).id : channel.id) : group.id}
    postable_type {group.nil? ? (channel.nil? ? 'Group' : 'Channel') : 'Group'}
    title "Question"
    message "This is a question"
    pinned false
  end

  factory :answer, :parent => :comment, :class => 'Answer' do
    user
    message "This is an answer"
    association :commentable, factory: :question
  end

  factory :comment do
    user
    association :commentable, factory: :post
    message "This is a comment"
    is_mobile false
  end

  factory :pronouncement do
    association :scope, factory: :organization
    label "Label"
    start_date Time.now
  end

  factory :tag do
    sequence(:name) {|n| "tag#{n}"}
    mapped true
  end

  factory :interest do
    sequence(:name) {|n| "interest#{n}"}
  end

  factory :vote do
    association :voter, factory: :user
    association :votable, factory: :answer
    trait :downvote do
      weight 0
    end
    weight 1
  end

  factory :pin do
    association :user
  end

  factory :score do
    user
    association :group, factory: :resource_group
    score 0
    stars 0
  end

  factory :approval do
    association :approver, factory: :user
    association :approvable, factory: :post
  end

  factory :follow do
    user
    association :followable, factory: :post
  end

  factory :organization_email_domain do
    organization
    sequence :url do |n|
      "url#{n}.com"
    end
  end

  factory :transaction do
    order
    user
    amount BigDecimal.new("100")
    gateway "braintree"
    payment_state "initiated"
  end

  factory :order do
    organization
    user
    orderable_id 1
    orderable_type 'Recharge'
    state "unprocessed"
  end

  factory :org_subscription do
    organization
    name "Annual Subscription Fees"
    description "Your Org subscription for one year."
    start_date Date.today
    end_date (Date.today + 1.year)
    sku "EdCast Instance name: Spark - Annual Subscription Fees"
  end

  factory :wallet_balance do
    amount BigDecimal.new("100")
    user
  end

  factory :wallet_transaction do
    amount BigDecimal.new("100")
    order
    user
  end

  factory :recharge do
    skillcoin "10.0"
    active true
    description "this is description"
    sku "sku"
  end

  factory :recharge_price do
    amount "1.0"
    currency {"USD"}
  end

  factory :user_purchase do
  end

  factory :transaction_audit do
  end

  factory :card_subscription do
    card
    user
    organization
    start_date Date.today
    end_date nil
    transaction_id 1
  end

  factory :scorm_detail do
    card
    token ''
    status ''
    message ''
  end

  factory :card_reporting do
    card
    user
    reason 'some reason'
  end

  factory :comment_reporting do
    association :comment, factory: :comment
    association :user, factory: :user
    reason 'some reason'
  end

  factory :email_template do
    association :organization, factory: :organization
    sequence :title do |n|
      "original_title#{n}"
    end
    is_active true
    default_id nil
    sequence :content do |n|
      "original_content#{n}"
    end
    sequence :design do |n|
      "original_design#{n}"
    end
    language 'en'
    state 'published'
  end

  factory :workflow do
    user
    organization
    status 'new'
    sequence :name do |n|
      "workflow_name#{n}"
    end
    is_enabled false
    wf_type 'User'
    wf_input_source [{type: 'DB', source: 'User'}]
    wf_actionable_columns [
      { column_name: 'country',    type: 'string' },
      { column_name: 'first_name', type: 'string' },
      { column_name: 'last_name',  type: 'string' },
      { column_name: 'status',     type: 'string' },
      { column_name: 'email',      type: 'string' },
      { column_name: 'location',   type: 'string' }
    ]
    wf_rules {
      {
        and: [
          { equals: { country: 'India' } },
          { not_equals: { location: 'Mumbai' } }
        ]
      }
    }
    wf_action {
      {
        entity: 'Team',
        type: 'create_dynamic',
        value: {
          column_name: 'name',
          wf_column_names: ['location'],
          prefix: '',
          suffix: ''
        },
        metadata: {
          is_private: true,
          description: 'Group created from workflow',
          is_mandatory: true
        }
      }
    }
    wf_output_result nil
  end
end

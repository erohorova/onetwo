require 'test_helper'

class ScheduleStreamNotificationJobTest < ActiveJob::TestCase
  setup do
    ScheduleStreamNotificationJob.unstub :perform_now
    EdcastActiveJob.unstub :perform_now
    @org = create(:organization)
    @user = create(:user, organization: @org)
  end

  test 'event should be not be triggered if card does not exist' do
    EDCAST_NOTIFY.expects(:trigger_event).never
    ScheduleStreamNotificationJob.perform_now(card_id: Faker::Number.number(10).to_i)
  end

  test 'event should be triggered if card exist' do
    video_stream = create(:wowza_video_stream, :upcoming, start_time: Time.now.utc + 20.minutes, 
      organization_id: @org.id, creator: @user)
    EDCAST_NOTIFY.expects(:trigger_event).times(1)
    ScheduleStreamNotificationJob.perform_now(card_id: video_stream.card.id)
  end
end
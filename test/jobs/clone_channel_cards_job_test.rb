require 'test_helper'

class CloneChannelCardsJobTest < ActiveSupport::TestCase
  setup do
    TestAfterCommit.enabled = true
    @org1, @org2 = create_list(:organization, 2)
    @user = create(:user, organization: @org1)
    @org1_channel = create(:channel, organization: @org1, shareable: true)
    @org2_channel = create(:channel, organization: @org2, parent_id: @org1_channel.id)
  end

  test "should trigger clone channel cards job on cloning channel" do
    org1, org2 = create_list(:organization, 2)
    org1_channel = create(:channel, organization: org1, shareable: true)

    CloneChannelCardsJob.expects(:perform_later)
    org2_clone_channel = create(:channel, organization: org2, parent_id: org1_channel.id)
  end

  test "should not trigger clone channel cards job on cloning channel if parent channel not shareable" do
    org1, org2 = create_list(:organization, 2)
    org1_channel = create(:channel, organization: org1, shareable: false)

    CloneChannelCardsJob.expects(:perform_later).never
    org2_clone_channel = create(:channel, organization: org2, parent_id: org1_channel.id)
  end

  test "should clone smartcard on performing clone channel job" do
    card = create(:card, card_type: 'media', card_subtype: 'text', author_id: @user.id, title: 'This is card', 
      message: 'This is message', organization: @org1, ecl_source_name: 'UGC')
    card.current_user = @user
    card.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
    assert_equal 1, card.cloned_cards.count
    assert_equal @org2_channel.id, card.cloned_cards.first.channels.first.id
  end

  test "should clone non-ugc course smartcard on performing clone channel job" do
    card = create(:card, card_type: 'course', card_subtype: 'link', author_id: nil, title: 'This is card', 
      message: 'This is message', organization: @org1)
    card.current_user = @user
    card.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
    assert_equal 1, card.cloned_cards.count
    assert_equal @org2_channel.id, card.cloned_cards.first.channels.first.id
  end

  test "should clone course smartcard on performing clone channel job" do
    card = create(:card, card_type: 'course', card_subtype: 'link', author_id: @user.id, title: 'This is card', 
      message: 'This is message', organization: @org1, ecl_source_name: 'UGC')
    card.current_user = @user
    card.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
    assert_equal 1, card.cloned_cards.count
    assert_equal @org2_channel.id, card.cloned_cards.first.channels.first.id
  end

  test "should not clone private smartcard on performing clone channel job" do
    card = create(:card, card_type: 'media', card_subtype: 'text', author_id: @user.id, title: 'This is card', 
      message: 'This is message', organization: @org1, ecl_source_name: 'UGC', is_public: false)
    card.current_user = @user
    card.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
    assert_equal 0, card.cloned_cards.count
  end

  test "should not clone project smartcard on performing clone channel job" do
    card = create(:card, card_type: 'project', card_subtype: 'text', author_id: @user.id, title: 'This is card', 
      message: 'This is message', organization: @org1, ecl_source_name: 'UGC')
    card.current_user = @user
    card.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
    assert_equal 0, card.cloned_cards.count
  end

  test "should clone pathway on performing clone channel job" do
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover', 
      organization: @org1, ecl_source_name: 'UGC')
    cards = create_list(:card, 2, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end
    cover.current_user = @user
    cover.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
    assert_equal 1, cover.cloned_cards.count
    assert_equal 2, cards.sum {|card| card.cloned_cards.count}
  end

  test "should clone journey on performing clone channel job" do
    cover = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: @user, 
      organization: @org1, ecl_source_name: 'UGC')
    section = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user, organization: @org)
    cards = create_list(:card, 2, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: section.id, add_id: card.id, add_type: 'Card')
    end
    JourneyPackRelation.add(cover_id: cover.id, add_id: section.id)
    cover.current_user = @user
    cover.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
    assert_equal 1, cover.cloned_cards.count
    assert_equal 1, section.cloned_cards.count
    assert_equal 2, cards.sum {|card| card.cloned_cards.count}
  end
end
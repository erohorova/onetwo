require 'test_helper'

class SocialAnalyticsReportJobTest < ActiveJob::TestCase
  test "should invoke social content analytics service" do
    organization = create(:organization)
    Organization.any_instance.expects(:send_user_content_analytics).with({report_type: 'social'}).once

    SocialAnalyticsReportJob.perform_now(organization.id)
  end
end

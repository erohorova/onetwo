require 'test_helper'

class AssignmentPerformanceMetricJobTest < ActiveJob::TestCase
  test 'should update assignment metric for `assigned`, `completed`, `started` states' do
    org = create(:organization)
    card = create(:card, author_id: nil, organization: org)
    assignees = create_list(:user, 4, organization: org)
    assignor1, assignor2 = create_list(:user, 2, organization: org)
    team1, team2 = create_list(:team, 2, organization: org)

    assignment1 = create(:assignment, assignable: card, assignee: assignees[0])
    assignment2 = create(:assignment, assignable: card, assignee: assignees[1])
    assignment3 = create(:assignment, assignable: card, assignee: assignees[2])
    assignment4 = create(:assignment, assignable: card, assignee: assignees[3])

    # State #completed: Mark an assignment as completed.
    assignment2.start!
    assignment2.complete!
    # State #started: Mark an assignment as started.
    assignment3.start!
    # State #dismissed: Mark an assignment as dismissed.
    assignment4.start!
    assignment4.dismiss!

    assert_difference -> {TeamAssignmentMetric.count}, 2 do
      create(:team_assignment, team: team1, assignor: assignor1, assignment: assignment1)
      create(:team_assignment, team: team1, assignor: assignor1, assignment: assignment2)
      create(:team_assignment, team: team2, assignor: assignor2, assignment: assignment3)
      create(:team_assignment, team: team2, assignor: assignor2, assignment: assignment4)
      AssignmentPerformanceMetricJob.perform_now([card.id])
    end

    team_assignor1 = TeamAssignmentMetric
      .where(team_id: team1.id, assignor_id: assignor1.id, card_id: card.id).first

    assert_not_nil team_assignor1.assigned_at
    assert_equal team_assignor1.assigned_at, team1.team_assignments.first.created_at
    assert_equal 1, team_assignor1.assigned_count
    assert_equal 0, team_assignor1.started_count
    assert_equal 1, team_assignor1.completed_count

    team_assignor2 = TeamAssignmentMetric
      .where(team_id: team2.id, assignor_id: assignor2.id, card_id: card.id).first

    assert_equal 0, team_assignor2.assigned_count
    assert_equal 1, team_assignor2.started_count
    assert_equal 0, team_assignor2.completed_count

    # should decrement started_count and increment completed_count
    assignment3.complete!
    AssignmentPerformanceMetricJob.perform_now([card.id])
    team_assignor2.reload

    assert_not_nil team_assignor2.assigned_at
    assert_equal team_assignor2.assigned_at, team2.team_assignments.first.created_at
    assert_equal 0, team_assignor2.assigned_count
    assert_equal 0, team_assignor2.started_count
    assert_equal 1, team_assignor2.completed_count
  end

  test 'should create team assignment metric with right assigned_at date' do
    org = create(:organization)
    card = create(:card, author_id: nil, organization: org)
    assignee = create(:user, organization: org)
    assignor = create(:user, organization: org)
    team = create(:team, organization: org)
    assignment = create(:assignment, assignable: card, assignee: assignee)
    team_assignment = create(:team_assignment, team: team, assignor: assignor, assignment: assignment)
    AssignmentPerformanceMetricJob.perform_now([card.id])

    assert_equal 1, TeamAssignmentMetric.count
    assert_equal team_assignment.reload.created_at, TeamAssignmentMetric.first.assigned_at
  end
end

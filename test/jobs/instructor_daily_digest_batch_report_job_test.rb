require 'test_helper'

class InstructorDailyDigestBatchReportJobTest < ActiveSupport::TestCase
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    ActionMailer::Base.deliveries.clear
  end

  test 'add job to BatchJob table' do
    assert_difference -> { BatchJob.count } do
      InstructorDailyDigestBatchReportJob.new.perform_now
    end
    assert_equal 'InstructorDailyDigestBatchReportJob', BatchJob.last.name
  end

  test 'calls instructor digest report service method with expected params' do
    Timecop.freeze do
      InstructorDigestReportService.any_instance.expects(:send_report_to_instructors)
        .with(from: (Time.now.utc - 1.day).to_i, to: (Time.now.utc).to_i).returns(nil)
      InstructorDailyDigestBatchReportJob.new.perform_now
    end

    Timecop.return
  end
end

require 'test_helper'

class UpdateClcScoreForUserJobTest < ActiveJob::TestCase

  setup do
    @user = create(:user)
    @org = @user.organization
    @card = create(:card, organization: @user.organization, author: @user)
  end

  test "should call #update_clc_score_of_user" do
    clc = create(:clc, name: 'test clc', entity: @org, organization: @org, from_date: Date.today, to_date: Date.today + 1.days)

    ClcService.any_instance.expects(:update_clc_score_of_user).with(@card.id, @user.id, @org.id, "Organization").once
    UpdateClcScoreForUserJob.perform_now(@card.id, @user.id, @org.id, "Organization")
  end
end

require 'test_helper'

class TranscoderCompleteJobTest < ActiveSupport::TestCase
  setup do
    @s3_client = AWS::S3::Client.new(region: 'us-east-1', stub_requests: false)
    AWS::S3::Client.stubs(:new).returns(@s3_client)

    stub_request(:get, "https://bucket.s3.amazonaws.com/?prefix=video-streams-output/401/thumbnails/").
        to_return(:status => 200, :body => %q{<?xml version="1.0" encoding="UTF-8"?>
<ListBucketResult xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
  <Name>quotes</Name>
  <Prefix>video-streams-output/401/thumbnails/</Prefix>
  <Marker>Ned</Marker>
  <MaxKeys>40</MaxKeys>
  <IsTruncated>false</IsTruncated>
  <Contents>
    <Key>video-streams-output/401/thumbnails/0.png</Key>
    <LastModified>2006-01-01T12:00:00.000Z</LastModified>
    <ETag>&quot;828ef3fdfa96f00ad9f27c383fc9ac7f&quot;</ETag>
    <Size>5</Size>
    <StorageClass>STANDARD</StorageClass>
    <Owner>
      <ID>bcaf161ca5fb16fd081034f</ID>
      <DisplayName>webfile</DisplayName>
     </Owner>
  </Contents>
  <Contents>
    <Key>video-streams-output/401/thumbnails/1.png</Key>
    <LastModified>2006-01-01T12:00:00.000Z</LastModified>
    <ETag>&quot;828ef3fdfa96f00ad9f27c383fc9ac7f&quot;</ETag>
    <Size>4</Size>
    <StorageClass>STANDARD</StorageClass>
     <Owner>
      <ID>bcaf1ffd86a5fb16fd081034f</ID>
      <DisplayName>webfile</DisplayName>
    </Owner>
 </Contents>
</ListBucketResult>}, :headers => {})

    stub_request(:get, "https://bucket.s3.amazonaws.com/?prefix=video-streams-output-v2/server/3830/b720039b-6c4c-4ba9-ad0f-f07d95570be8/796/0/thumbnails/").
        to_return(:status => 200, :body => %q{<?xml version="1.0" encoding="UTF-8"?>
<ListBucketResult xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
  <Name>quotes</Name>
  <Prefix>video-streams-output/401/thumbnails/</Prefix>
  <Marker>Ned</Marker>
  <MaxKeys>40</MaxKeys>
  <IsTruncated>false</IsTruncated>
  <Contents>
    <Key>video-streams-output/401/thumbnails/0.png</Key>
    <LastModified>2006-01-01T12:00:00.000Z</LastModified>
    <ETag>&quot;828ef3fdfa96f00ad9f27c383fc9ac7f&quot;</ETag>
    <Size>5</Size>
    <StorageClass>STANDARD</StorageClass>
    <Owner>
      <ID>bcaf161ca5fb16fd081034f</ID>
      <DisplayName>webfile</DisplayName>
     </Owner>
  </Contents>
  <Contents>
    <Key>video-streams-output/401/thumbnails/1.png</Key>
    <LastModified>2006-01-01T12:00:00.000Z</LastModified>
    <ETag>&quot;828ef3fdfa96f00ad9f27c383fc9ac7f&quot;</ETag>
    <Size>4</Size>
    <StorageClass>STANDARD</StorageClass>
     <Owner>
      <ID>bcaf1ffd86a5fb16fd081034f</ID>
      <DisplayName>webfile</DisplayName>
    </Owner>
 </Contents>
</ListBucketResult>}, :headers => {})
  end
  test 'should fetch and store the thumnail clips' do
    v = create(:wowza_video_stream, status: 'past')
    recording = create(:recording, video_stream: v, default: false)
    transcoder_success_payload = "{\n  \"state\" : \"COMPLETED\",\n  \"version\" : \"2012-09-25\",\n  \"jobId\" : \"1444415920383-kngase\",\n  \"pipelineId\" : \"1436401151865-lxwln6\",\n  \"input\" : {\n    \"key\" : \"video_streams/893/server/b379602b-5956-453b-8d57-7af73e8cd01e.mp4\"\n  },\n  \"outputKeyPrefix\" : \"video-streams-output/401/\",\n  \"outputs\" : [ {\n    \"id\" : \"1\",\n    \"presetId\" : \"1351620000001-200030\",\n    \"key\" : \"server/b379602b-5956-453b-8d57-7af73e8cd01e/0\",\n    \"thumbnailPattern\" : \"thumbnails/{count}.jpg\",\n    \"segmentDuration\" : 1.0,\n    \"status\" : \"Complete\",\n    \"statusDetail\" : \"Some individual segment files for this output have a higher bit rate than the average bit rate of the transcoded media. Playlists including this output will record a higher bit rate than the rate specified by the preset.\",\n    \"duration\" : 119,\n    \"width\" : 242,\n    \"height\" : 432\n  }, {\n    \"id\" : \"2\",\n    \"presetId\" : \"1351620000001-100020\",\n    \"key\" : \"server/b379602b-5956-453b-8d57-7af73e8cd01e/0.mp4\",\n    \"status\" : \"Complete\",\n    \"duration\" : 119,\n    \"width\" : 414,\n    \"height\" : 736\n  } ],\n  \"userMetadata\" : {\n    \"recording_id\" : \"401\"\n  }\n}"

    v.expects(:reindex).returns(nil).once
    assert_difference ->{Thumbnail.count},2 do
      TranscoderCompleteJob.perform_now(recording, transcoder_success_payload)
    end
    last_thumbnail = Thumbnail.last
    assert_equal recording, last_thumbnail.recording
    assert_equal "https://#{Settings.wowza.s3_bucket}.s3.amazonaws.com/video-streams-output/401/thumbnails/1.png", last_thumbnail.uri
    assert_equal 1, last_thumbnail.sequence_number

    recording.reload
    assert_equal "video-streams-output/401/server/b379602b-5956-453b-8d57-7af73e8cd01e/0.m3u8", recording.hls_location, "without the new adaptive playlist"
    assert_equal recording.id, v.recordings.default.first.id
  end

  test 're-transcoding should not add more thumbnails' do
    v = create(:wowza_video_stream, status: 'past')
    recording = create(:recording, video_stream: v, default: false)
    transcoder_success_payload = "{\n  \"state\" : \"COMPLETED\",\n  \"version\" : \"2012-09-25\",\n  \"jobId\" : \"1444415920383-kngase\",\n  \"pipelineId\" : \"1436401151865-lxwln6\",\n  \"input\" : {\n    \"key\" : \"video_streams/893/server/b379602b-5956-453b-8d57-7af73e8cd01e.mp4\"\n  },\n  \"outputKeyPrefix\" : \"video-streams-output/401/\",\n  \"outputs\" : [ {\n    \"id\" : \"1\",\n    \"presetId\" : \"1351620000001-200030\",\n    \"key\" : \"server/b379602b-5956-453b-8d57-7af73e8cd01e/0\",\n    \"thumbnailPattern\" : \"thumbnails/{count}.jpg\",\n    \"segmentDuration\" : 1.0,\n    \"status\" : \"Complete\",\n    \"statusDetail\" : \"Some individual segment files for this output have a higher bit rate than the average bit rate of the transcoded media. Playlists including this output will record a higher bit rate than the rate specified by the preset.\",\n    \"duration\" : 119,\n    \"width\" : 242,\n    \"height\" : 432\n  }, {\n    \"id\" : \"2\",\n    \"presetId\" : \"1351620000001-100020\",\n    \"key\" : \"server/b379602b-5956-453b-8d57-7af73e8cd01e/0.mp4\",\n    \"status\" : \"Complete\",\n    \"duration\" : 119,\n    \"width\" : 414,\n    \"height\" : 736\n  } ],\n  \"userMetadata\" : {\n    \"recording_id\" : \"401\"\n  }\n}"

    v.expects(:reindex).returns(nil).twice
    assert_difference ->{Thumbnail.count},2 do
      TranscoderCompleteJob.perform_now(recording, transcoder_success_payload)
    end

    assert_difference ->{Thumbnail.count},0 do
      TranscoderCompleteJob.perform_now(recording, transcoder_success_payload)
    end

  end

  test 'should work with the new master playlist format' do
    v = create(:wowza_video_stream, status: 'past')
    recording = create(:recording, video_stream: v, default: false)
    transcoder_success_payload = "{\n  \"state\" : \"COMPLETED\",\n  \"version\" : \"2012-09-25\",\n  \"jobId\" : \"1450406375677-uq9jl4\",\n  \"pipelineId\" : \"1436291265781-7tpdna\",\n  \"input\" : {\n    \"key\" : \"video_streams_test/3830/server/b720039b-6c4c-4ba9-ad0f-f07d95570be8.mp4\"\n  },\n  \"outputKeyPrefix\" : \"video-streams-output-v2/server/3830/b720039b-6c4c-4ba9-ad0f-f07d95570be8/796/0/\",\n  \"outputs\" : [ {\n    \"id\" : \"1\",\n    \"presetId\" : \"1445551672039-3hplll\",\n    \"key\" : \"high_hls_output\",\n    \"thumbnailPattern\" : \"thumbnails/{count}\",\n    \"segmentDuration\" : 5.0,\n    \"status\" : \"Complete\",\n    \"statusDetail\" : \"Some individual segment files for this output have a higher bit rate than the average bit rate of the transcoded media. Playlists including this output will record a higher bit rate than the rate specified by the preset.\",\n    \"duration\" : 26,\n    \"width\" : 720,\n    \"height\" : 1280\n  }, {\n    \"id\" : \"2\",\n    \"presetId\" : \"1351620000001-100020\",\n    \"key\" : \"mp4_output.mp4\",\n    \"status\" : \"Complete\",\n    \"duration\" : 26,\n    \"width\" : 606,\n    \"height\" : 1080\n  }, {\n    \"id\" : \"3\",\n    \"presetId\" : \"1351620000001-200040\",\n    \"key\" : \"medium_hls_output\",\n    \"segmentDuration\" : 5.0,\n    \"status\" : \"Complete\",\n    \"statusDetail\" : \"Some individual segment files for this output have a higher bit rate than the average bit rate of the transcoded media. Playlists including this output will record a higher bit rate than the rate specified by the preset.\",\n    \"duration\" : 26,\n    \"width\" : 180,\n    \"height\" : 320\n  }, {\n    \"id\" : \"4\",\n    \"presetId\" : \"1351620000001-200050\",\n    \"key\" : \"low_hls_output\",\n    \"segmentDuration\" : 5.0,\n    \"status\" : \"Complete\",\n    \"statusDetail\" : \"Some individual segment files for this output have a higher bit rate than the average bit rate of the transcoded media. Playlists including this output will record a higher bit rate than the rate specified by the preset.\",\n    \"duration\" : 26,\n    \"width\" : 162,\n    \"height\" : 288\n  } ],\n  \"playlists\" : [ {\n    \"name\" : \"hls_playlist\",\n    \"format\" : \"HLSv3\",\n    \"outputKeys\" : [ \"high_hls_output\", \"medium_hls_output\", \"low_hls_output\" ],\n    \"status\" : \"Complete\"\n  } ],\n  \"userMetadata\" : {\n    \"recording_id\" : \"796\"\n  }\n}"

    v.expects(:reindex).returns(nil).once
    TranscoderCompleteJob.perform_now(recording, transcoder_success_payload)
    recording.reload
    assert_equal "video-streams-output-v2/server/3830/b720039b-6c4c-4ba9-ad0f-f07d95570be8/796/0/hls_playlist.m3u8", recording.hls_location
    assert_equal recording.id, v.recordings.default.first.id
  end

end

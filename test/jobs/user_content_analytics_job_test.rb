require 'test_helper'

class UserContentAnalyticsJobTest < ActiveJob::TestCase
  test "should invoke user content org analytics service" do
    organization = create(:organization)
    Organization.any_instance.expects(:send_user_content_analytics).once

    UserContentAnalyticsJob.perform_now(organization.id)
  end
end

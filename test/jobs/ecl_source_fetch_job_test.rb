require 'test_helper'

class EclSourceFetchJobTest < ActiveJob::TestCase
  include RequestHelperMethods

  test 'should fetch card from ECL' do
    organization = create(:organization)
    channel = create(:channel, organization: organization)
    ecl_source = create(:ecl_source, channel: channel, ecl_source_id: "7e9d12b4-b4f0-4139-9f09-0aa659a8561c")

    connection = EclApi::EclSearch.new(organization.id)
    ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items"]

    # return virtual cards
    virtual_cards = connection.send(:_format_data_as_cards, ecl_response['data'])
    EclApi::EclSearch.any_instance.stubs(:search).returns(OpenStruct.new(results: virtual_cards, aggregations: []))

    # stub content_items/:ecl_id card
    ecl_id1 = ecl_response['data'][0]['id']
    ecl_id2 = ecl_response['data'][1]['id']
    ecl_id3 = ecl_response['data'][2]['id']

    ecl_response1 = get_ecl_card_response(ecl_id1)
    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id1}").
      to_return(:status => 200, :body => ecl_response1.to_json)

    ecl_response1 = get_ecl_card_response(ecl_id2)
    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id2}").
      to_return(:status => 200, :body => ecl_response1.to_json)

    ecl_response1 = get_ecl_card_response(ecl_id3)
    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id3}").
      to_return(:status => 200, :body => ecl_response1.to_json)

    # stub external url
    stub_request(:get, "http://www.businessinsider.com/psychologist-explains-how-listening-music-while-working-studying-low-productivity-less-efficient-2016-11").
      with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.9.2'}).
      to_return(:status => 200, :body => "", :headers => {})

    stub_request(:get, "http://localhost:9200/cards_test/_search").to_return(:status => 200, :body => "", :headers => {})

    assert_difference ->{Card.count}, 3 do
      EclSourceFetchJob.perform_now(ecl_source.id)
    end

    #assert card channel association
    card1, card2 = Card.last(2)
    assert_includes card1.channels, channel
    assert_includes card2.channels, channel
  end


  test 'should create private cards if ecl_source for channel is private' do
    organization = create(:organization)
    channel = create(:channel, organization: organization)
    ecl_source = create(:ecl_source, channel: channel, ecl_source_id: "7e9d12b4-b4f0-4139-9f09-0aa659a8561c", private_content: true)

    connection = EclApi::EclSearch.new(organization.id)
    ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items"]

    # return virtual cards
    virtual_cards = connection.send(:_format_data_as_cards, ecl_response['data'])
    EclApi::EclSearch.any_instance.stubs(:search).returns(OpenStruct.new(results: virtual_cards, aggregations: []))

    # stub content_items/:ecl_id card
    ecl_id1 = ecl_response['data'][0]['id']
    ecl_id2 = ecl_response['data'][1]['id']
    ecl_id3 = ecl_response['data'][2]['id']

    ecl_response1 = get_ecl_card_response(ecl_id1)
    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id1}").
      to_return(:status => 200, :body => ecl_response1.to_json)

    ecl_response1 = get_ecl_card_response(ecl_id2)
    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id2}").
      to_return(:status => 200, :body => ecl_response1.to_json)

    ecl_response1 = get_ecl_card_response(ecl_id3)
    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id3}").
      to_return(:status => 200, :body => ecl_response1.to_json)

    # stub external url
    stub_request(:get, "http://www.businessinsider.com/psychologist-explains-how-listening-music-while-working-studying-low-productivity-less-efficient-2016-11").
      with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.9.2'}).
      to_return(:status => 200, :body => "", :headers => {})

    stub_request(:get, "http://localhost:9200/cards_test/_search").to_return(:status => 200, :body => "", :headers => {})

    assert_difference ->{Card.count}, 3 do
      EclSourceFetchJob.perform_now(ecl_source.id)
    end

    #assert card channel association
    card1, card2 = Card.last(2)
    assert_includes card1.channels, channel
    assert_includes card2.channels, channel
    assert !card1.is_public
    assert !card2.is_public
  end
end

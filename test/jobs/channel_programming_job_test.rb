require 'test_helper'
class ChannelProgrammingJobTest < ActiveJob::TestCase
  setup do
    ChannelProgrammingJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
  end

  test 'call content adapter for channel programming' do
    org = create(:organization)
    channel = create(:channel, organization: org, language: ['en'], content_type: ['article', 'link'])
    ecl_source1 = create(:ecl_source, channel: channel, ecl_source_id: '123-abc')
    ecl_source2 = create(:ecl_source, channel: channel, ecl_source_id: '123-xyz')

    adapter = mock
    adapter.expects(:channel_programming).with(channel.id, {node_paths: [],
                                                source_ids: [ecl_source1.ecl_source_id, ecl_source2.ecl_source_id],
                                                types: channel.content_type,
                                                topic_ids: [],
                                                organization_id: channel.organization_id,
                                                size: 500,
                                                scope: {channel_ids: [channel.id]},
                                                languages: channel.language})

    Adapter::Content.expects(:new).with({organization_id: channel.organization_id}).returns(adapter)
    ChannelProgrammingJob.perform_now(channel.id)
  end

  test 'call content adapter for channel programming with card_fetch_limit' do
    org = create(:organization)
    channel = create(:channel, organization: org, language: ['en'], content_type: ['article', 'link'], card_fetch_limit: 20)
    ecl_source1 = create(:ecl_source, channel: channel, ecl_source_id: '123-abc')
    ecl_source2 = create(:ecl_source, channel: channel, ecl_source_id: '123-xyz')

    adapter = mock
    adapter.expects(:channel_programming).with(channel.id, {node_paths: [],
                                                source_ids: [ecl_source1.ecl_source_id, ecl_source2.ecl_source_id],
                                                types: channel.content_type,
                                                topic_ids: [],
                                                organization_id: channel.organization_id,
                                                size: 20,
                                                scope: {channel_ids: [channel.id]},
                                                languages: channel.language})

    Adapter::Content.expects(:new).with({organization_id: channel.organization_id}).returns(adapter)
    ChannelProgrammingJob.perform_now(channel.id)
  end
end

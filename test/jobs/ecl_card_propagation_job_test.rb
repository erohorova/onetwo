require 'test_helper'

class EclCardPropagationJobTest < ActiveSupport::TestCase

  test 'should call ecl connector propagate method' do
    c = create(:card)
    EclApi::CardConnector.any_instance.expects(:propagate_card_to_ecl).with(c.id).returns(nil).once
    EclCardPropagationJob.perform_now(c.id)
  end

end

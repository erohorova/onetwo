require 'test_helper'

class UserTeamAssignmentNotificationJobTest < ActiveSupport::TestCase

  test 'should create user team assignment notification' do
    org = create(:organization)
    assignee = create(:user, organization: org)
    assignor = create(:user, organization: org)
    card = create(:card, author: assignor)

    assignment = create(:assignment, assignable: card, assignee: assignee)
    team_assignment = create(:team_assignment, assignment: assignment, assignor: assignor)

    assert_differences [[-> {Notification.count}, 1], [->{NotificationsCausalUser.count}, 1]] do
      UserTeamAssignmentNotificationJob.new.perform({team_assignment_id: team_assignment.id, notification_type: 'assigned'})
    end

    notification = Notification.last
    assert_equal assignment, notification.notifiable
    assert_equal assignment.assignee.id, notification.user_id
    assert_equal 'assigned', notification.notification_type
  end

  test 'should not create duplicate notification for assignment assigned through different groups' do
    org = create(:organization)
    assignee, assignor1, assignor2 = create_list(:user, 3, organization: org)
    card = create(:card, author: assignor1)

    assignment = create(:assignment, assignable: card, assignee: assignee)
    team_assignment1 = create(:team_assignment, assignment: assignment, assignor: assignor1)
    team_assignment2 = create(:team_assignment, assignment: assignment, assignor: assignor2)

    assert_differences [[-> {Notification.count}, 1], [->{NotificationsCausalUser.count}, 2]] do
      UserTeamAssignmentNotificationJob.new.perform({team_assignment_id: team_assignment1.id, notification_type: 'assigned'})
      UserTeamAssignmentNotificationJob.new.perform({team_assignment_id: team_assignment2.id, notification_type: 'assigned'})
    end

    notification = Notification.last
    assert_equal assignment, notification.notifiable
    assert_equal assignment.assignee.id, notification.user_id
    assert_equal 'assigned', notification.notification_type
  end

  test 'should destroy existing notification if notification user is not active' do
    org = create(:organization)
    team = create(:team, organization: org)
    causal_user, assignee = create_list(:user, 2, organization: org)
    card = create(:card, author: causal_user)
    assignment = create(:assignment, assignable: card, assignee: assignee)
    team_assignment = create(:team_assignment, team_id: team, assignment: assignment, assignor: causal_user)
    old_notification = Notification.create(notifiable: assignment, user_id: assignee.id, notification_type: "assignment_notice")
    assignee.update(is_suspended: true)

    assert_difference ->{Notification.count}, -1 do
      UserTeamAssignmentNotificationJob.new.perform({team_assignment_id: team_assignment.id, notification_type: 'assignment_notice'})
    end
    assert_equal 0, Notification.count
  end

  test 'assignee should get notication when assignor and assignee are same user' do
    org = create(:organization)
    assignor = create(:user, organization: org)
    card = create(:card, author: assignor)

    assignment = create(:assignment, assignable: card, assignee: assignor)
    team_assignment = create(:team_assignment, assignment: assignment, assignor: assignor)

    assert_differences [[-> {Notification.count}, 1], [->{NotificationsCausalUser.count}, 1]] do
      UserTeamAssignmentNotificationJob.new.perform({team_assignment_id: team_assignment.id, notification_type: 'assigned'})
    end

    notification = Notification.last
    team_assignment = TeamAssignment.last

    assert_equal assignment, notification.notifiable
    assert_equal team_assignment.assignor.id, notification.user_id
    assert_equal 'assigned', notification.notification_type
  end
end
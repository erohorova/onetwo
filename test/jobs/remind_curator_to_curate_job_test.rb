require 'test_helper'

class RemindCuratorToCurateJobTest < ActiveJob::TestCase
  setup do
    RemindCuratorToCurateJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    @org1 = create(:organization)
    create(:config, name: 'feature/notifications_and_triggers_enabled', value: 'true', configable: @org1)
    curator = create(:user, organization: @org1)

    user = create(:user, organization: @org1)
    channel = create(:channel, curate_ugc: true, curate_only: true, organization: @org1, created_at: 5.days.ago)
    create(:channels_user, user_id: user.id, channel_id: channel.id)
    create(:channels_curator, user_id: curator.id, channel_id: channel.id)

    card = create(:card, organization: @org1, author_id: user.id, created_at: 5.days.ago)
    create(:channels_card, card_id: card.id, channel_id: channel.id, created_at: 5.days.ago)

    @org2 = create(:organization)
    config = build :notification_config, organization: @org1
    config.set_enabled(Notify::NOTE_REMINDED_CURATOR, :email, :single)
    config.save
  end

  test 'event should be triggered only for orgs that have the remind curator config enabled' do
    EDCAST_NOTIFY.expects(:trigger_event).times(1)
    Organization.get_config_enabled_orgs('feature/notifications_and_triggers_enabled', 'true').each do |org|
      if NotificationConfig.find_by(organization_id: org.id).try(:enabled?, "reminded_curator", "email", "single")
        RemindCuratorToCurateJob.perform_now(channel_ids: org.channels.pluck(:id))
      end
    end
  end

end
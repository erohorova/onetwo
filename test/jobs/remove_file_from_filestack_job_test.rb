require 'test_helper'

class RemoveFileFromFilestackJobTest < ActiveSupport::TestCase
  test 'should call filestack to remove file' do
    filestack = {
      "filename": "Strix_nebulosaRB.jpg",
      "handle": "yT4BBpg7T5efeiriQv37"
    }.with_indifferent_access

    fr = create(:file_resource, filestack: filestack, filestack_handle: filestack["handle"])
    fr.destroy
    stub_request(:delete, "http://www.filestackapi.com/api/file/#{fr.filestack_handle}?key=#{Settings.filestack.api_key}").
      to_return(:status => 200, :body => "", :headers => {})

    log.expects(:warn).with("Successfully deleted file from filestack with handle: #{fr.filestack_handle} for card_id: #{fr.attachable_id}")
    RemoveFileFromFilestackJob.new.perform(handle: fr.filestack_handle, card_id: fr.attachable_id)
  end

  test 'should raise warning log if failed to remove file from filestack' do
    filestack = {
      "filename": "Strix_nebulosaRB.jpg",
      "handle": "yT4BBpg7T5efeiriQv37"
    }.with_indifferent_access
    fr = create(:file_resource, filestack: filestack, filestack_handle: filestack["handle"])
    fr.destroy
    stub_request(:delete, "http://www.filestackapi.com/api/file/#{fr.filestack_handle}?key=#{Settings.filestack.api_key}").
      to_return(:status => 422, :body => "", :headers => {})

    log.expects(:warn).with("Failed to delete file from filestack with handle: #{fr.filestack_handle} for card_id: #{fr.attachable_id}, Status: 422, Response: ")
    RemoveFileFromFilestackJob.new.perform(handle: fr.filestack_handle, card_id: fr.attachable_id)
  end

  test 'should raise warning log if fileresource exist with handle' do
    skip('DEPRECATED')

    filestack = {
      "filename": "Strix_nebulosaRB.jpg",
      "handle": "yT4BBpg7T5efeiriQv37"
    }.with_indifferent_access

    fr = create(:file_resource, filestack: filestack, filestack_handle: filestack["handle"])
    fr1 = create(:file_resource, filestack: filestack, filestack_handle: filestack["handle"])

    stub_request(:delete, "http://app:test_app_secret@www.filestackapi.com/api/file/#{fr.filestack_handle}?key=#{Settings.filestack.api_key}").
      to_return(:status => 422, :body => "", :headers => {})

    fr.destroy
    log.expects(:warn).with("Cannot delete file from filestack with handle: #{fr.filestack_handle} for card_id: #{fr.attachable_id}")
    RemoveFileFromFilestackJob.new.perform(handle: fr.filestack_handle, card_id: fr.attachable_id)
  end
end

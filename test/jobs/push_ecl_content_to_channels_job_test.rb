require 'test_helper'

class PushECLContentToChannelsJobTest < ActiveSupport::TestCase
  def setup
    @org = create :organization
    @channels = create_list :channel, 2, organization: @org
    @ecl_card = create(:card, ecl_id: "test-ecl-card", organization: @org,  author: nil)

    # stubbing ecl connector
    EclApi::CardConnector.any_instance.stubs(:card_from_ecl_id).returns(@ecl_card)
    @ecl_card.stubs(:reindex_async)
  end

  test 'should push ecl content to channels' do
    PushECLContentToChannelsJob.perform_now(
      organization_id: @org.id,
      channel_ids: @channels.map{|c| c.id},
      ecl_ids: [@ecl_card.id]
    )
    assert_same_elements @channels, @ecl_card.channels
  end

  test 'should only push ecl content to non-private channels' do
    @channels.first.update(is_private: true)

    PushECLContentToChannelsJob.perform_now(
      organization_id: @org.id,
      channel_ids: @channels.map{|c| c.id},
      ecl_ids: [@ecl_card.id]
    )

    assert_same_elements [@channels.last], @ecl_card.channels
  end

  test 'should not push ecl content if channel organization and current org mismatch' do
    another_org = create :organization

    PushECLContentToChannelsJob.perform_now(
      organization_id: another_org.id,
      channel_ids: @channels.map{|c| c.id},
      ecl_ids: [@ecl_card.id]
    )

    assert_empty @ecl_card.channels
  end
end

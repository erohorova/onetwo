require 'test_helper'

class InitializeScoreInVideoStreamsJobTest < ActiveJob::TestCase
  self.use_transactional_fixtures = false
  setup do
    WebMock.disable!
    VideoStream.searchkick_index.delete if VideoStream.searchkick_index.exists?
  end

  teardown do
    WebMock.enable!
  end

  test 'initialize existing video_streams with score' do
    skip("INTERMITTENT FAILURE ON TRAVIS")
    WowzaVideoStream.any_instance.stubs(:empty_update_data).returns({})
    WowzaVideoStream.any_instance.stubs(:creator_hash).returns({})
    video_stream = create(:wowza_video_stream)
    WowzaVideoStream.any_instance.unstub(:empty_update_data)
    WowzaVideoStream.any_instance.unstub(:creator_hash)
    VideoStream.searchkick_index.refresh
    elasticsearch_object = Search::VideoStreamSearch.new.search_by_id(video_stream.id)
    assert elasticsearch_object[:weekly_score].nil?
    VideoStream.searchkick_index.refresh
    InitializeScoreInVideoStreamsJob.perform_now
    VideoStream.searchkick_index.refresh
    updated_elasticsearch_object = Search::VideoStreamSearch.new.search_by_id(video_stream.id)
    assert !updated_elasticsearch_object[:weekly_score].nil?
    assert !updated_elasticsearch_object[:creator][:handle].nil?
  end
end

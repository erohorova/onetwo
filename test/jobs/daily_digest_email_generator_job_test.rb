require 'test_helper'

class DailyDigestEmailGeneratorJobTest < ActiveSupport::TestCase
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    ActionMailer::Base.deliveries.clear
    EdcastActiveJob.unstub :perform_later
    DailyDigestEmailGeneratorJob.unstub :perform_later

    Post.any_instance.stubs :p_read_by_creator

    @org = create(:organization)
    @client = create(:client, organization: @org)

    @u1  = create :user, organization: @org
    @u2 = create :user, organization: @org
    @u3 = create :user, organization: @org
    @u4 = create :user, organization: @org

    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(false)
    @rg = create :resource_group, end_date: nil,
      organization: @org, daily_digest_enabled: true, client: @client # open group

    @u5 = create :user, organization: @org
    @rg2 = create :resource_group, end_date: nil, close_access: 1,
      daily_digest_enabled: true, organization: @org, client: @client# closed group

    # open group
    @rg.add_admin(@u1)
    @rg.add_member(@u2)
    @rg.add_member(@u3)
    @rg.add_member(@u4)

    # closed group
    @rg2.add_member(@u5)
    @rg2.add_admin(@u1)

    # ------ For Open Group ------ #
    # Pinned post
    @pinned_post    = create :post, user: @u1, group: @rg, type: 'Question', postable: @rg, pinned: true, pinned_at: Time.now

    # Commented posts
    @commented_post = create :post, user: @u1, group: @rg, type: 'Question', postable: @rg
    # old comments
    @comment1 = create :answer, commentable: @commented_post, created_at: Time.now - 3.days, user_id: @u1.id
    # new comments
    @comment2 = create :answer, commentable: @commented_post, created_at: Time.now - 5.hours, user_id: @u2.id
    @comment3 = create :answer, commentable: @commented_post, user_id: @u1.id

    # Posts with approved comments
    @approved_commented_post = create :post, user: @u1, group: @rg, type: 'Question', postable: @rg
    @comment4 = create :answer, commentable: @approved_commented_post, user_id: @u2.id
    # old approvals
    @approved_comment1 = create :approval, approver: @u1, approvable: @comment1, created_at: Time.now - 3.days
    # new approvals
    @approved_comment2 = create :approval, approver: @u1, approvable: @comment3, created_at: Time.now - 5.hours
    @approved_comment4 = create :approval, approver: @u1, approvable: @comment4

    # Posts that are upvoted
    @upvoted_post = create :post, user: @u1, group: @rg, type: 'Question', postable: @rg
    @old_upvoted_post = create :post, user: @u1, group: @rg, type: 'Question', postable: @rg, created_at: 1.month.ago
    # new votes
    @vote1 = create :vote, voter: @u1, votable: @upvoted_post, weight: 1
    @vote2 = create :vote, voter: @u2, votable: @upvoted_post, weight: 1
    # old votes
    @vote3 = create :vote, voter: @u1, votable: @old_upvoted_post, weight: 1, created_at: Time.now - 5.days
    @vote4 = create :vote, voter: @u2, votable: @old_upvoted_post, weight: 1, created_at: Time.now - 6.days

    # ------ For Closed Group ------ #
    create_list :post, 5, user: @u1, group: @rg2, type: 'Question', postable: @rg2, pinned: true, pinned_at: Time.now
    stub_request(:post, "https://mandrillapp.com/api/1.0/messages/send-template.json").
    to_return(:status => 200, :body => "", :headers => {})
  end

  teardown do
    Organization.any_instance.unstub(:notifications_and_triggers_enabled?)
  end

  # iterates over open groups with daily digest option enabled
  test 'add job to BatchJob table' do
    assert_difference -> { BatchJob.count } do
      DailyDigestEmailGeneratorJob.perform_now
    end
  end

  # iterates over open groups with daily digest option enabled
  test 'picks config from mailer settings if config added' do
    config = create :mailer_config, organization: @org

    from_email = ''
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      from_email = message[:mailer].from_address
    }.at_least(3)

    DailyDigestEmailGeneratorJob.perform_now
    assert_includes from_email, 'test@gmail.com'
  end

  # iterates over open groups with daily digest option enabled
  test 'send emails successfully' do
    MandrillMailer.any_instance.expects(:send_api_email).at_least(3)
    DailyDigestEmailGeneratorJob.perform_now
  end

  # iterates over open groups with daily digest option enabled
  test 'doesnot send email if user opts out from daily digest' do
    # @u2 opts out, @u3 and @u4 should continue to receive emails
    MandrillMailer.any_instance.expects(:send_api_email).twice

    UserPreference.create!(user: @u2, key: 'notification.daily_digest.opt_out', value: 'true', preferenceable: @rg)
    DailyDigestEmailGeneratorJob.perform_now
  end

  test 'closed groups should be excluded even if daily digest option is enabled' do
    MandrillMailer.any_instance.expects(:send_api_email).never
    @rg.update_columns(close_access: true)
      DailyDigestEmailGeneratorJob.perform_now
  end

  test 'skips open groups with no client' do
    # users from @rg should not receive emails
    MandrillMailer.any_instance.expects(:send_api_email).never
    @org.client.destroy
    @rg.reload
    refute @rg.client
    DailyDigestEmailGeneratorJob.perform_now
  end

  test 'doesnot considers open groups with disabled daily digest option' do
    MandrillMailer.any_instance.expects(:send_api_email).never
    [@rg, @rg2].each { |g| g.update(daily_digest_enabled: false) }
    DailyDigestEmailGeneratorJob.perform_now
  end
end

require 'test_helper'

class ResourceJobTest < ActiveSupport::TestCase

  test 'should work with relative urls' do
    stub_request(:get, "http://image.com/url").
      to_return(:status => 200, :body => Rails.root.join('test/fixtures/images/logo.png').open, :headers => {'Content-Type' => 'image/png'})
    resource = create :resource, image_url: '//image.com/url'

    assert_difference -> { FileResource.count } do
      ResourceJob.perform_now(resource)
    end
  end

  test "should download asset and save as file resource" do
    stub_request(:get, "http://image.com/url").
      to_return(:status => 200, :body => Rails.root.join('test/fixtures/images/logo.png').open, :headers => {'Content-Type' => 'image/png'})
    resource = create :resource, image_url: 'http://image.com/url'

    assert_difference -> { FileResource.count } do
      ResourceJob.perform_now(resource)
    end

    # reuse existing file resource
    assert_no_difference -> { FileResource.count } do
      ResourceJob.perform_now(resource)
    end
  end

  test "log error if asset download fails" do
    stub_request(:get, "http://image.com/url").
      to_return(:status => 500, :body => "", :headers => {})
    resource = create :resource, image_url: 'http://image.com/url'

    log.expects(:error).once
    assert_no_difference -> { FileResource.count } do
      ResourceJob.perform_now(resource)
    end
  end

  test 'should blank out image url and reindex card if asset download fails' do
    stub_request(:get, "http://image.com/url").
      to_return(:status => 500, :body => "", :headers => {})
    resource = create :resource, image_url: 'http://image.com/url'
    c1, c2 = create_list(:card, 2, resource: resource)
    Card.any_instance.expects(:reindex_card).twice

    assert_no_difference -> { FileResource.count } do
      ResourceJob.perform_now(resource)
    end

    resource.reload
    assert_nil resource.image_url
  end

  test 'should reindex cards after resource job finished' do
    stub_request(:get, "http://image.com/url").
      to_return(:status => 200, :body => Rails.root.join('test/fixtures/images/logo.png').open, :headers => {'Content-Type' => 'image/png'})
    resource = create :resource, image_url: '//image.com/url'
    c1, c2 = create_list(:card, 2, resource: resource)

    IndexJob.stubs(:perform_later)
    Card.any_instance.expects(:reindex_card).twice

    assert_difference -> { FileResource.count } do
      ResourceJob.perform_now(resource)
    end
  end

end

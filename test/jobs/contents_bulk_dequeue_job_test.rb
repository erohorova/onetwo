require 'test_helper'

class ContentsBulkDequeueJobTest < ActiveSupport::TestCase

  setup do
    ContentsBulkDequeueJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    WebMock.disable!
    UserContentsQueue.create_index! force: true
    UserContentsQueue.refresh_index!

    uids = [1,2,3]
    @cids = [1,2,3]

    uids.each do |uid|
      @cids.each do |cid|
        UserContentsQueue.create(user_id: uid, content_id: cid, content_type: 'Card')
      end
    end

    UserContentsQueue.gateway.refresh_index!
  end

  teardown do
    WebMock.enable!
  end

  test 'should remove cards in bulk from user queue' do
    ContentsBulkDequeueJob.new.perform(user_ids: [1, 2], content_type: 'Card', content_ids: [1,2])

    UserContentsQueue.gateway.refresh_index!

    u1_new_queue = UserContentsQueue.get_cards_queue_for_user(user_id: 1)
    assert_equal [3], u1_new_queue.map(&:content_id)

    u2_new_queue = UserContentsQueue.get_cards_queue_for_user(user_id: 2)
    assert_equal [3], u2_new_queue.map(&:content_id)

    u3_new_queue = UserContentsQueue.get_cards_queue_for_user(user_id: 3)
    assert_same_elements @cids, u3_new_queue.map(&:content_id)
  end

  test 'should reenqueue cards in bulk from user queue' do
    cards = create_list(:card, 3)
    Card.any_instance.expects(:enqueue_card).times(3)
    ContentsBulkDequeueJob.new.perform(user_ids: [1, 2], content_type: 'Card', content_ids: cards.collect(&:id), reenqueue: true)
  end

  test 'should remove cards in bulk from all user queues' do
    ContentsBulkDequeueJob.new.perform(content_ids: [1,2], content_type: 'Card')

    UserContentsQueue.gateway.refresh_index!

    u1_new_queue = UserContentsQueue.get_cards_queue_for_user(user_id: 1)
    assert_equal [3], u1_new_queue.map(&:content_id)

    u2_new_queue = UserContentsQueue.get_cards_queue_for_user(user_id: 2)
    assert_equal [3], u2_new_queue.map(&:content_id)
  end

  test 'error logs' do
    Rails.logger.expects(:error).once

    ContentsBulkDequeueJob.new.perform(user_ids: [1], content_type: 'Card')
    ContentsBulkDequeueJob.new.perform(content_ids: [1], content_type: 'Card')

  end
end

require 'test_helper'

class QuizQuestionStatsRecorderJobTest < ActiveSupport::TestCase

  test 'should record stats' do
    Analytics::MetricsRecorderJob.stubs(:perform_later)
    poll = create(:card, card_type: 'poll', card_subtype: 'text')
    user = create(:user)
    attempt = create(:quiz_question_attempt, quiz_question: poll, user: user, selections: ['1', '2'])

    assert_difference ->{QuizQuestionStats.count} do
      QuizQuestionStatsRecorderJob.new.perform({quiz_question_attempt_id: attempt.id})
    end

    stats = QuizQuestionStats.last
    assert_equal({'attempt_count' => 1, 'options_stats' => {'1' => 1, '2' => 1}}, stats.as_json)

    # some other user attempts
    u2 = create(:user)
    attempt2 = create(:quiz_question_attempt, quiz_question: poll, user: u2, selections: ['1'])

    assert_no_difference ->{QuizQuestionStats.count} do
      QuizQuestionStatsRecorderJob.new.perform({quiz_question_attempt_id: attempt2.id})
    end

    stats.reload
    assert_equal({'attempt_count' => 2, 'options_stats' => {'1' => 2, '2' => 1}}, stats.as_json)
  end

  test 'should update data for record stats for quiz card' do
    Analytics::MetricsRecorderJob.stubs(:perform_later)
    org = create(:organization)
    autor, user, user1 = create_list(:user, 3, organization: org)
    poll = create(:card, card_type: 'poll', card_subtype: 'text', organization: org, author: autor)
    attempt = create(:quiz_question_attempt, quiz_question: poll, user: user, selections: ['1', '2'])
    create(:quiz_question_attempt, quiz_question: poll, user: user1, selections: ['2'])

    QuizQuestionStatsRecorderJob.new.perform({quiz_question_attempt_id: attempt.id})

    stats = QuizQuestionStats.last
    stats.reload
    assert_equal({'attempt_count' => 2, 'options_stats' => {'1' => 1, '2' => 2}}, stats.as_json)
  end
end

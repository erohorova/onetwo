require 'test_helper'

class EnqueueStreamsBatchJobTest < ActiveSupport::TestCase
  test 'should enqueue videam stream for batch users' do
    EnqueueStreamsBatchJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    org = create(:organization)
    users = create_list(:user, 5, organization: org)
    creator = create(:user, organization: org)
    v = create(:wowza_video_stream, status: 'live', start_time: Time.now.utc, organization: org, creator: nil)
    rationale = {'type' => 'following_user', 'user_id' => creator.id}
    users.each do |f|
      create(:follow, followable: creator, user_id: f.id)
      UserContentsQueue.expects(:enqueue_bulk_for_user).with(has_entries({user_id: f.id, flush: false, content_type: 'VideoStream', items: [{content_id: v.id, channel_ids: [], tags: [], metadata: {'rationale' => rationale}}]})).returns(nil)
    end
    UserContentsQueue.expects(:buffer_flush).returns(nil).once
    EnqueueStreamsBatchJob.new.perform({stream_id: v.id, user_ids: users.map(&:id), rationale: rationale, timestamp: Time.now.to_i, buffer_flush: false})
  end
end

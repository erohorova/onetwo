require 'test_helper'

class SuspendUserJobTest < ActiveJob::TestCase
  test 'suspend user job' do
    organization = create(:organization)
    user = create(:user, organization: organization)

    SuspendUserJob.perform_now(user.id, organization.id)
    user.reload
    assert_equal true, user.is_suspended?
  end
end

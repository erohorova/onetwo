require 'test_helper'

class OnFollowEnqueueJobTest < ActiveSupport::TestCase
  self.use_transactional_fixtures = false

  setup do
    OnFollowEnqueueJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    ChannelsCard.any_instance.stubs :create_users_management_card

    WebMock.disable!
    UserContentsQueue.create_index! force: true
    VideoStream.searchkick_index.delete if VideoStream.searchkick_index.exists?

    @organization = create(:organization)
    @author = create(:user, organization: @organization)
    @channel = create(:channel, :robotics, organization: @organization)
    @cards = (1..10).map do |i|
      card = create(:card, author_id: @author.id, organization_id: @organization.id, created_at: i.minutes.ago, published_at: i.minutes.ago)
      card.channels << @channel
      card
    end

    # Add an author less card
    @non_author_card = create(:card, author_id: nil, organization_id: @organization.id, created_at: 11.minutes.ago, published_at: 11.minutes.ago)
    @non_author_card.channels << @channel
    @cards << @non_author_card

    @channel1 = create(:channel, organization: @organization)
    @cards1 = create_list(:card, 5, author_id: @author.id, organization_id: @organization.id)

    @cards1[0].channels << @channel1
    @cards1[1].channels << @channel1
    @cards1[2].channels << @channel1
    @cards1[3].channels << @channel1
    @cards1[4].channels << @channel1

    @user = create(:user, organization: @organization)

    # user has a card already on the queue
    @oldcard = create(:card, organization_id: @organization.id, author_id: @author.id)
    UserContentsQueue.enqueue_for_user(
        user_id: @user.id,
        content_id: @oldcard.id,
        content_type: 'Card',
        queue_timestamp: 1.hour.ago
    )
    Card.reindex
    UserContentsQueue.gateway.refresh_index!
  end

  teardown do
    WebMock.enable!
  end

  test 'enqueue streams on channel follow' do
    ch = create(:channel, autopull_content: true, organization: @organization)
    tagged_stream = create(:wowza_video_stream, creator: create(:user, organization: @organization), name: "#tag1", status: 'live')
    posted_to_channel_stream = create(:wowza_video_stream, organization: @organization, creator: nil)
    posted_to_channel_stream.channels << ch
    create(:follow, user: @user, followable: ch)
    UserContentsQueue.gateway.refresh_index!
    queue = UserContentsQueue.get_queue_for_user(user_id: @user.id, content_type: 'VideoStream')
    assert_equal 1, queue.count
    assert_equal posted_to_channel_stream.id, queue.first.content_id
  end

  test 'enqueues cards on channel follow' do
    # user follows channel1
    create(:follow, user: @user, followable: @channel1)
    UserContentsQueue.gateway.refresh_index!

    # cards from channel appear at front of queue
    queue = UserContentsQueue.get_cards_queue_for_user(user_id: @user.id, limit: 20)
    assert_equal 6, queue.count

    assert_equal(
      (@cards1.map(&:id).reverse + [@oldcard.id]).sort,
      queue.first(10).map{|ucq| ucq.content_id}.sort
    )
    assert_equal @oldcard.id, queue.last.content_id
  end

  test 'dont enqueue non curated cards on channel follow' do
    # user follows channel
    curated_channel = create(:channel, curate_only: true, organization: @organization)
    @cards.each { |card| card.channels << curated_channel }
    first_two_cards = @cards.first(2)

    first_two_cards.each do |card|
      channels_card  = ChannelsCard.where(card:card, channel_id: curated_channel.id).first
      channels_card.curate
      channels_card.save
    end
    create(:follow, user: @user, followable: curated_channel)
    UserContentsQueue.gateway.refresh_index!

    # cards from channel appear at front of queue
    queue = UserContentsQueue.get_cards_queue_for_user(user_id: @user.id, limit: 20)
    assert_equal 3, queue.count

    # equal 'last_enqueue_event_time' for first two items
    #  result: fails frequently on Travis
    assert_same_elements first_two_cards.map(&:id), queue.first(2).map{|ucq| ucq.content_id}

    assert_equal @oldcard.id, queue.last.content_id
  end

  test 'enqueues streams on user follow' do
    followee = create(:user, organization: @organization)
    stream = create(:wowza_video_stream, status: 'live', creator: followee)
    create(:follow, user: @user, followable: followee)
    UserContentsQueue.gateway.refresh_index!

    queue = UserContentsQueue.get_queue_for_user(user_id: @user.id, content_type: 'VideoStream')
    assert_equal 1, queue.count
    assert_equal stream.id, queue.first.content_id
  end

  test 'enqueue streams respects privacy on channels' do
    creator, follower = create_list(:user, 2, organization: @organization)
    channel = create(:channel, is_private: true, organization: @organization)
    public_stream = create(:wowza_video_stream, status: 'live', creator: creator)
    private_stream = create(:wowza_video_stream, status: 'upcoming', creator: creator)
    private_stream.channels << channel
    create(:follow, followable: creator, user: follower)
    UserContentsQueue.gateway.refresh_index!

    queue = UserContentsQueue.get_queue_for_user(user_id: follower.id, content_type: 'VideoStream')
    assert_equal 1, queue.count
    assert_equal public_stream.id, queue.first.content_id
  end

  test 'enqueues cards on user follow' do
    author = create(:user, organization: @organization)

    Search::CardSearch.
        any_instance.
        expects(:search).
        with(author, responds_with(:id, author.organization_id), viewer: @user, limit: 10, sort: :updated,
             filter_params: {card_type: Card::EXPOSED_TYPES, state: 'published'}, load: true).
        returns(Search::CardSearchResponse.new(results: @cards.reverse))

    # user follows author
    create(:follow, user: @user, followable: author)
    UserContentsQueue.gateway.refresh_index!

    # cards from author appear at front of queue
    queue = UserContentsQueue.get_cards_queue_for_user(user_id: @user.id, limit: 20)
    assert_equal 12, queue.count
    # 0..-2 because last card in @cards array is an author less card
    # and hence it wont appear in the beginning of queue
    assert_equal @cards.map(&:id)[0..-2], queue.first(10).map{|ucq| ucq.content_id}
    refute_includes queue.first(10).map{|ucq| ucq.content_id}, @non_author_card.id
    assert_equal @oldcard.id, queue.last.content_id
  end

  test 'removes recommended user from queue on follow' do
    Recommend.unstub(:get_recommendations)

    author1, author2 = create_list(:user, 2, organization: @organization)
    [author1, author2].each do |author|
      UserContentsQueue.enqueue_for_user(
          user_id: @user.id,
          content_id: author.id,
          content_type: 'User',
          metadata: {rationales: [:global_influencer]},
      )
    end
    UserContentsQueue.gateway.refresh_index!

    # user has authors recommended
    assert_same_ids [author1, author2],
                    Recommend.get_recommendations(@user, content_type: 'User')[:items],
                    ordered: false

    Search::CardSearch.
        any_instance.
        expects(:search).
        returns(Search::CardSearchResponse.new(results: []))

    # user follows author2
    create(:follow, user: @user, followable: author2)
    UserContentsQueue.gateway.refresh_index!

    # author2 is no longer recommended
    assert_same_ids [author1],
                    Recommend.get_recommendations(@user, content_type: 'User')[:items],
                    ordered: false
  end

  test 'card should not get enqueued for user if user does not has access to it' do
    followee = create(:user, organization: @organization)
    followee1 = create(:user, organization: @organization)

    card = create(:card, author: followee, is_public: false)
    channel = create(:channel, organization: @user.organization, curate_only: false, only_authors_can_post: false)

    create(:channels_card, channel_id: channel.id, card_id: card.id)
    create(:card_user_permission, card_id: card.id, user_id: followee1.id)
    create(:follow, followable: channel, user_id: @user.id)

    UserContentsQueue.gateway.refresh_index!

    queue = UserContentsQueue.get_cards_queue_for_user(user_id: @user.id)
    assert_equal 1, queue.count
    assert_not_includes queue.map(&:content_id), card.id
  end

  test 'card should get enqueued for user only if user has access to it' do
    followee = create(:user, organization: @organization)

    card = create(:card, author: followee, is_public: false)
    channel = create(:channel, organization: @user.organization, curate_only: false, only_authors_can_post: false)

    create(:channels_card, channel_id: channel.id, card_id: card.id)
    create(:card_user_permission, card_id: card.id, user_id: @user.id)

    create(:follow, followable: channel, user_id: @user.id)

    UserContentsQueue.gateway.refresh_index!

    queue = UserContentsQueue.get_cards_queue_for_user(user_id: @user.id)

    assert_equal 2, queue.count
    assert_includes queue.map(&:content_id), card.id
  end
end

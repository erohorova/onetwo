require 'test_helper'

class UsersCardsManagementJobTest < ActiveJob::TestCase

  setup do
    @org = create(:organization)
    @user = create(:user, organization: @org)
    @card = create(:card, author: @user)
    @channel = create(:channel, organization: @org)
  end

  test 'should create a users_cards_management' do
    action = 'posted_to_channel'
    assert_difference -> { UsersCardsManagement.count } do
      UsersCardsManagementJob.perform_now(@user.id, @card.id, action, @channel.id)
    end
  end

  test 'should destroy the users_cards_management' do
    action = 'posted_to_channel'
    UsersCardsManagement.create(user: @user, card: @card, action: action, target_id: @channel.id)
    assert_difference -> { UsersCardsManagement.count }, -1 do
      UsersCardsManagementJob.perform_now(@user.id, @card.id, action, @channel.id, opts: 'destroy')
    end
  end

  test 'should return false if nil values exists' do
    job = UsersCardsManagementJob.new.perform(1, nil, 'action', 2, opts: 'create')
    assert_equal false, job
  end

  test 'should return UCM obj for create' do
    action = 'posted_to_channel'
    ucm = UsersCardsManagementJob.perform_now(@user.id, @card.id, action, @channel.id, opts: 'create')
    assert ucm.is_a?(UsersCardsManagement)
  end

  test 'should return deleted obj for destroy' do
    action = 'posted_to_channel'
    deleted_obj = UsersCardsManagement.create(user: @user, card: @card, action: action, target_id: @channel.id)
    ucm = UsersCardsManagementJob.perform_now(@user.id, @card.id, action, @channel.id, opts: 'destroy')
    assert ucm.is_a?(UsersCardsManagement)
    assert ucm.id, deleted_obj.id
  end
end
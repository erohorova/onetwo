require 'test_helper'
class EclCardArchiveJobTest < ActiveSupport::TestCase
  test 'should call to destroy card from ecl' do
    EclApi::CardConnector.any_instance.expects(:archive_card_from_ecl).with('test-ecl-id', 15).once
    EclCardArchiveJob.perform_now('test-ecl-id', 15)
  end
end

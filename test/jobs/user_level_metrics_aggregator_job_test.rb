require 'test_helper'
class UserLevelMetricsAggregatorJobTest < ActiveSupport::TestCase

  setup do
    WebMock.disable!
  end

  teardown do
    WebMock.enable!
  end

  test 'should call increment with right arguments' do
    UserLevelMetricsAggregatorJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    org = create(:organization)
    user = create(:user, organization: org)
    video_stream = create(:wowza_video_stream, creator: create(:user, organization: org))

    Timecop.freeze do
      UserLevelMetric.expects(:update_from_increments).with(opts: {user_id: user.id}, increments: [[:smartbites_score, 10], [:smartbites_comments_count, 1]], timestamp: Time.at(Time.now.to_i)).returns(nil).once
      UserLevelMetric.expects(:update_from_increments).with(opts: {user_id: video_stream.creator_id}, increments: [[:smartbites_score, 20]], timestamp: Time.at(Time.now.to_i)).returns(nil).once

      # consumed for actionable event 'comment'
      UserLevelMetric.expects(:update_from_increments).with(opts: {user_id: user.id}, increments: [[:smartbites_score, 0], [:smartbites_consumed, 1]], timestamp: Time.at(Time.now.to_i)).returns(nil).once
      UserLevelMetric.expects(:update_from_increments).with(opts: {user_id: video_stream.creator_id}, increments: [[:smartbites_score, 0]], timestamp: Time.at(Time.now.to_i)).returns(nil).once
      mr = MetricRecord.new(action: "comment", object_id: video_stream.id, object_type: "video_stream", actor_id: user.id, owner_id: video_stream.creator_id).save
    end
  end

  # Unwanted test

  # test 'all metrics increment' do
  #   user = create(:user)
  #   video_stream = create(:wowza_video_stream)
  #   MetricRecord.stubs(:is_creation_event?).returns(true)
  #   MetricRecord.stubs(:is_consumption_event?).returns(true)
  #   MetricRecord.stubs(:is_comment_event?).returns(true)
  #   MetricRecord.stubs(:is_like_event?).returns(true)

  #   Timecop.freeze do
  #     UserLevelMetric.expects(:update_from_increments).with(opts: {user_id: user.id}, increments: [[:smartbites_score, 10], [:smartbites_created, 1], [:smartbites_consumed, 1], [:smartbites_comments_count, 1], [:smartbites_liked, 1]], timestamp: Time.at(Time.now.to_i)).returns(nil).once
  #     UserLevelMetric.expects(:update_from_increments).with(opts: {user_id: video_stream.creator_id}, increments: [[:smartbites_score, 20]], timestamp: Time.at(Time.now.to_i)).returns(nil).once
  #     mr = MetricRecord.new(action: "comment", object_id: video_stream.id, object_type: "video_stream", actor_id: user.id, owner_id: video_stream.creator_id).save
  #   end
  # end

end

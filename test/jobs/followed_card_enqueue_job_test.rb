require 'test_helper'

class FollowedCardEnqueueJobTest < ActiveSupport::TestCase

  self.use_transactional_fixtures = false

  setup do
    WebMock.disable!
    UserContentsQueue.create_index! force: true
    Card.searchkick_index.delete if Card.searchkick_index.exists?
    TeamsChannelsAutofollowJob.unstub :perform_later
    FollowedCardBatchEnqueueJobV1.unstub :perform_later
    FollowedCardEnqueueJob.unstub :perform_later
    CardsJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    ChannelsCard.any_instance.stubs :create_users_management_card
    # Card.any_instance.unstub(:after_tagged)

    @organization = create(:organization)
    @author = create(:user, organization: @organization, handle: '@robertsmith', is_suspended: false)
    @another_author = create(:user, organization: @organization)

    # initial card on all users' queues
    @users = create_list(:user, 4, organization: @organization, is_suspended: false)
    UserContentsQueue.gateway.refresh_index!

    @full_channel = create(:channel, :good_music, organization: @organization) # #goodmusic @robertsmith
    @tag_channel = create(
        :channel,
        label: 'good music tag',
        organization: @organization
    )
  end

  teardown do
    WebMock.enable!
  end

  def queue_ids(user)
    queue = UserContentsQueue.get_cards_queue_for_user(user_id: user.id)
    return queue.map{|c| c['content_id']}
  end

  test 'enqueues new cards for followers' do
    channel_follower, user_follower, tag_follower, other_follower = @users
    create(:follow, user: channel_follower, followable: @full_channel)
    create(:follow, user: user_follower, followable: @author)
    create(:follow, user: tag_follower, followable: @tag_channel)
    robotics_channel = create(:channel, :robotics, organization: @organization)
    create(:follow, user: other_follower, followable: robotics_channel)

    # card in a different org with query #goodmusic. shouldn't show up
    diff_org = create(:organization)
    diff_org_author = create(:user, organization: diff_org)
    diff_org_card_id = Card.create(
          card_type: 'media',
          card_subtype: 'link',
          is_public: true,
          author_id: nil,
          state: 'published',
          published_at: Time.now,
          message: 'this is a #goodmusic card'
      ).id

    # card1 not followed by anyone; has no author
    card1_id = nil
    Timecop.freeze(3.hours.ago) do
      card1_id = Card.create(
          card_type: 'media',
          card_subtype: 'link',
          is_public: true,
          author_id: nil,
          organization_id: @organization,
          state: 'published',
          published_at: Time.now,
          message: 'some message'
      ).id
    end
    Card.reindex
    Card.searchkick_index.refresh
    UserContentsQueue.gateway.refresh_index!
    # received by no one
    @users.each do |u|
      assert_equal [], queue_ids(u)
    end

    # card2 created by author
    card2_id = nil
    Timecop.freeze(2.hours.ago) do
      card2_id = Card.create(
          card_type: 'media',
          card_subtype: 'link',
          is_public: true,
          author_id: @author.id,
          state: 'published',
          published_at: Time.now,
          message: 'some message'
      ).id
    end
    Card.searchkick_index.refresh
    UserContentsQueue.gateway.refresh_index!
    # received by user follower and followers of cq including author's handle
    # channel followers don't get it since there is no explicit association
    assert_equal [card2_id], queue_ids(user_follower)
    assert_equal [], queue_ids(channel_follower)
    assert_equal [], queue_ids(tag_follower)
    assert_equal [], queue_ids(other_follower)

    # card3 created by author
    card3_id = nil
    Timecop.freeze(1.hours.ago) do
      card3_id = Card.create(
          card_type: 'media',
          card_subtype: 'link',
          is_public: true,
          author_id: @author.id,
          state: 'published',
          published_at: Time.now,
          message: 'some message'
      ).id
    end
    Card.searchkick_index.refresh
    UserContentsQueue.gateway.refresh_index!
    # received by user follower and followers of cq including author's handle
    assert_equal [card3_id, card2_id], queue_ids(user_follower)

    # channel follower still doesn't see anything
    assert_equal [], queue_ids(channel_follower)
    assert_equal [], queue_ids(tag_follower)
    assert_equal [], queue_ids(other_follower)

    # card2 gets updated with tag
    Card.find(card2_id).update(title: '#robotics')
    Card.searchkick_index.refresh
    UserContentsQueue.gateway.refresh_index!
    # now received by all followers for tag and author; not moved in queue if already present
    # no change for channels
    assert_equal [card3_id, card2_id], queue_ids(user_follower)
    assert_equal [], queue_ids(channel_follower)
    assert_equal [], queue_ids(tag_follower)
    assert_equal [], queue_ids(other_follower)

    # card2 gets the robotics channel too
    card2 = Card.find_by(id: card2_id)
    ChannelsCard.any_instance.expects(:enqueue_for_followers).once
    card2.channels << robotics_channel

    ChannelCardEnqueueJob.new.perform(robotics_channel.id, card2.id)
    Card.searchkick_index.refresh
    UserContentsQueue.gateway.refresh_index!

    assert_equal [card2_id], queue_ids(other_follower)

    @full_channel.authors << @another_author
    ChannelsCard.any_instance.expects(:enqueue_for_followers).once
    # card gets created with channel
    card4_id = Card.create(
        card_type: 'media',
        card_subtype: 'link',
        is_public: true,
        author_id: @another_author.id,
        channel_ids: [@full_channel.id],
        state: 'published',
        published_at: Time.now,
        message: 'some message'
    ).id
    ChannelCardEnqueueJob.new.perform(@full_channel.id, card4_id)
    Card.searchkick_index.refresh
    UserContentsQueue.gateway.refresh_index!
    assert_equal [card3_id, card2_id], queue_ids(user_follower)
    assert_equal [card4_id], queue_ids(channel_follower)
    assert_equal [], queue_ids(tag_follower)
    assert_equal [card2_id], queue_ids(other_follower)
  end

  test 'enqueues private cards for followers with access' do
    *_, priv_channel_follower, user_follower = @users

    priv_channel = create(:channel, :private, organization_id: @organization.id)
    priv_channel.authors << @author

    create(:follow, user: priv_channel_follower, followable: priv_channel)

    create(:follow, user: user_follower, followable: @author)
    ChannelsCard.any_instance.expects(:enqueue_for_followers).once
    card1_id = Card.create(
        card_type: 'media',
        card_subtype: 'link',
        is_public: false,
        author_id: @author.id,
        channel_ids: [priv_channel.id],
        state: 'published',
        published_at: Time.now.utc,
        message: 'some message'
    ).id
    ChannelCardEnqueueJob.new.perform(priv_channel.id, card1_id)
    Card.reindex
    Card.searchkick_index.refresh
    UserContentsQueue.gateway.refresh_index!

    assert_equal [], queue_ids(user_follower)
    assert_equal [card1_id], queue_ids(priv_channel_follower)
  end

  test 'doesnt match handles that are superstrings' do
    follower = create(:user, organization: @organization)

    # follow superstring user's channel
    create(:user, handle: '@robertsmithington', organization: @organization)
    channel = create(
        :channel,
        label: "channel for @robertsmithington",
        organization: @organization
    )
    create(:follow, user: follower, followable: channel)

    # substring user creates card
    Card.create(
        card_type: 'media',
        card_subtype: 'link',
        is_public: true,
        author_id: @author.id,
        state: 'published',
        published_at: Time.now,
        message: 'some message'
    )
    Card.reindex
    Card.searchkick_index.refresh
    UserContentsQueue.gateway.refresh_index!

    # card not queued
    assert_empty queue_ids(follower)
  end

  test 'skips hidden cards' do
    Search::CardSearch.expects(:percolate).never

    Card.create(
        card_type: 'media',
        card_subtype: 'link',
        author_id: @author.id,
        hidden: true,
        state: 'published',
        published_at: Time.now,
        message: 'some message'
    )
  end

  test 'shouldnt enqueue their own card' do
    channel_follower, user_follower, tag_follower, other_follower = @users
    channel = create(:channel, :cars, organization: @organization)
    channel.add_authors([channel_follower])
    create(:follow, user: user_follower, followable: channel_follower)
    card1_id = nil
    Timecop.freeze(3.hours.ago) do
      card1_id = Card.create(
          card_type: 'media',
          card_subtype: 'link',
          is_public: true,
          author_id: channel_follower.id,
          state: 'published',
          published_at: Time.now,
          channel_ids: [channel.id],
          message: 'some message'
      ).id
    end
    Card.reindex
    UserContentsQueue.unstub :get_cards_queue_for_user
    Card.searchkick_index.refresh
    UserContentsQueue.gateway.refresh_index!

    assert_equal [card1_id], queue_ids(user_follower)
    assert_equal [], queue_ids(channel_follower)
  end

  # Remove card tagging/keyword recommendations
  # Commit id: b7b68a4562052845fc84297ace1fbcbb92f01c1f
  #
  test 'should not enqueue card based on user interest' do
    skip
    org = create(:organization)
    user = create(:user, organization: org)

    interest = create(:interest)
    user.interests << interest

    card_topics = [interest.name]

    card_id = nil
    Timecop.freeze(1.hours.ago) do
      card_id = Card.create(
          card_type: 'media',
          card_subtype: 'link',
          is_public: true,
          author_id: nil,
          state: 'published',
          organization_id: org.id,
          topics: card_topics.join(','),
          published_at: Time.now,
          message: 'some message'
      ).id
    end

    Card.searchkick_index.refresh
    UserContentsQueue.gateway.refresh_index!

    assert_empty queue_ids(user)
  end

  test 'shouldnt enqueue card which is not yet curated in a channel' do
    org = create(:organization)
    user1, user2, user3 = create_list(:user, 3, organization: org)

    curated_channel = create(:channel, organization: org, curate_only: true)
    curated_channel.add_authors([user3])
    create(:follow, user: user1, followable: curated_channel)
    uncurated_channel = create(:channel, organization: org, curate_only: false)
    uncurated_channel.add_authors([user3])
    create(:follow, user: user2, followable: uncurated_channel)


    card_id = nil
    another_card_id = nil
    ChannelsCard.any_instance.expects(:enqueue_for_followers).twice
    Timecop.freeze(1.hours.ago) do
      card_id = Card.create(
          card_type: 'media',
          card_subtype: 'link',
          is_public: true,
          author_id: nil,
          state: 'published',
          organization_id: org.id,
          channel_ids: [curated_channel.id, uncurated_channel.id],
          published_at: Time.now,
          message: 'some message'
      ).id
    end
    ChannelCardEnqueueJob.new.perform(uncurated_channel.id, card_id)
    Card.reindex
    Card.searchkick_index.refresh
    UserContentsQueue.gateway.refresh_index!

    assert_equal [], queue_ids(user1)
    assert_equal [card_id], queue_ids(user2)
  end

  test 'enqueue cards for teams that follow those channels in which the card is posted' do
    @team = create(:team, organization: @organization) # Create a team
    @team.add_to_team(@users + [@author], as_type: 'member') # Add users and author to the team

    # Scenario 1 : Existing cards which are posted to channel will not be queued for users in team
    card_1_id = Card.create(
      card_type: 'media',
      card_subtype: 'link',
      is_public: true,
      author_id: @author.id,
      channel_ids: [@full_channel.id],
      state: 'published',
      published_at: Time.now,
      message: 'some message'
    ).id
    Card.reindex
    Card.searchkick_index.refresh
    UserContentsQueue.gateway.refresh_index!

    card_2_id = Card.create(
      card_type: 'media',
      card_subtype: 'link',
      is_public: true,
      author_id: @author.id,
      channel_ids: [@full_channel.id],
      state: 'published',
      published_at: Time.now,
      message: 'some message'
    ).id
    Card.searchkick_index.refresh
    UserContentsQueue.gateway.refresh_index!

    # Card is just posted to the channel, but not followed by anyone.
    @users.each do |user|
      assert_equal [], queue_ids(user)
    end
    assert_equal [], queue_ids(@author)

    # Scenario 2: Team is following channel in which card is posted,
    # new card will be queued for team's users except author
    create(:teams_channels_follow, team: @team, channel: @full_channel)
    TeamsChannelsAutofollowJob.new.perform(team_ids: [@team.id], channel_id: [@full_channel.id])
    Card.searchkick_index.refresh
    UserContentsQueue.gateway.refresh_index!

    # Post new card to channel
    card_3_id = Card.create!(
      card_type: 'media',
      card_subtype: 'link',
      is_public: true,
      author_id: @author.id,
      channel_ids: [@full_channel.id],
      state: 'published',
      published_at: Time.now,
      message: 'some message'
    ).id
    FollowedCardEnqueueJob.new.perform({ card_id: card_3_id })
    Card.searchkick_index.refresh
    UserContentsQueue.gateway.refresh_index!

    @users.each do |user|
      assert_equal [card_2_id, card_1_id, card_3_id].sort, queue_ids(user).sort
    end
    assert_equal [], queue_ids(@author)

    # Scenario 3 : If a user from the team is following the channel,
    # new card should not be queued up twice for the user
    create(:follow, user: @users.first, followable: @full_channel)
    card_4_id = Card.create!(
      card_type: 'media',
      card_subtype: 'link',
      is_public: true,
      author_id: @author.id,
      channel_ids: [@full_channel.id],
      state: 'published',
      published_at: Time.now,
      message: 'some message'
    ).id
    FollowedCardEnqueueJob.new.perform({ card_id: card_4_id })
    Card.searchkick_index.refresh
    UserContentsQueue.gateway.refresh_index!

    assert_equal 1, queue_ids(@users.first).count(card_4_id)
  end
end

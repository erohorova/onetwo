require 'test_helper'

class EdcastActiveJobTest < ActiveSupport::TestCase

  class DummyJob < EdcastActiveJob
    queue_as :dummy_queue

    def perform(entity_id, entity_type)
    end
  end

  class UniqueDummyJob < EdcastActiveJob
    extend SetUniqueJob

    queue_as :dummy_queue

    def self.unique_args(args)
      [args[0], args[1]]
    end

    def perform(entity_id, entity_type)
    end
  end

  setup do
    # constant is modified to test other use cases
    EdcastActiveJob::WHITELISTED_JOB_CLASS << "EdcastActiveJobTest::DummyJob"
    EdcastActiveJob::WHITELISTED_JOB_CLASS << "EdcastActiveJobTest::UniqueDummyJob"

    UniqueDummyJob.unstub :perform_later
    DummyJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
  end

  test 'enqueues a job (set to identify unique) with no error' do
    assert_difference -> { BackgroundJob.count } do
      UniqueDummyJob.perform_later(4, 'Card')
    end
  end

  test 'enqueues a job with no error' do
    assert_difference -> { BackgroundJob.count } do
      UniqueDummyJob.perform_later({card_id: 4, type: 'Card'})
    end
  end

  test 'does not enqueues a job (not set to identify unique) with no error' do
    Sidekiq::JobStatus.any_instance.stubs(:enqueuable?).returns(false)

    assert_difference -> { BackgroundJob.count } do
      DummyJob.perform_later(4, 'Card')
    end
  end

  test 'ensures a job is enqueued only once' do
    ActiveJob::Base.queue_adapter = :sidekiq

    Sidekiq::Testing.fake! do
      assert_difference -> { BackgroundJob.count } do
        UniqueDummyJob.perform_later(4, 'Card')
      end

      Sidekiq::JobStatus.any_instance.stubs(:enqueuable?).returns(false)
      assert_no_difference -> { BackgroundJob.count } do
        UniqueDummyJob.perform_later(4, 'Card')
      end
    end
  end

  test 'ensures unique key is removed from redis once a job is processed' do
    Sidekiq::JobStatus.any_instance.stubs(:delete).once

    assert_difference -> { BackgroundJob.count } do
      UniqueDummyJob.perform_later(4, 'Card')
    end

    assert Sidekiq::JobStatus.new.enqueuable?([4, 'Card'])
  end

  test 'ensures unique key is removed from redis if a job has errored out' do
    Sidekiq::JobStatus.any_instance.stubs(:delete).once

    UniqueDummyJob.any_instance.stubs(:perform).raises(StandardError)

    Sidekiq::Testing.inline! do
      assert_difference -> { BackgroundJob.count } do
        UniqueDummyJob.perform_later(4, 'Card')
      end
    end

    assert Sidekiq::JobStatus.new.enqueuable?([4, 'Card'])
  end
end

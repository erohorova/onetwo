require 'test_helper'

class EnqueueStreamsJobTest < ActiveSupport::TestCase

  setup do
    EnqueueStreamsBatchJob.unstub :perform_later
    EnqueueStreamsJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
  end

  test 'should invoke batche enqueue job' do
    org = create(:organization)
    us = create_list(:user, 3, organization: org)
    chs = create_list(:channel, 2, organization: org)
    chs.first.add_followers([us.first])
    chs.second.add_followers([us.second])

    now = Time.now.utc
    Time.stubs(:now).returns(now)

    v = create(:wowza_video_stream, status: 'live', start_time: Time.now.utc, organization: org, creator: nil)
    v.channels << chs

    EnqueueStreamsBatchJob.expects(:perform_later).with(has_entries({stream_id: v.id, user_ids: [us.first.id], rationale: {'type' => 'following_channel', 'channel_id' => chs.first.id}, timestamp: now.to_i, buffer_flush: false})).returns(nil)
    EnqueueStreamsBatchJob.expects(:perform_later).with(has_entries({stream_id: v.id, user_ids: [us.second.id], rationale: {'type' => 'following_channel', 'channel_id' => chs.last.id}, timestamp: now.to_i, buffer_flush: false})).returns(nil)

    EnqueueStreamsJob.new.perform({id: v.id})
  end

  test 'should enqueue streams without authors to channel followers' do
    org = create(:organization)
    us = create_list(:user, 3, organization: org)
    chs = create_list(:channel, 2, organization: org)
    chs.first.add_followers([us.first])
    chs.second.add_followers([us.second])

    v = create(:wowza_video_stream, status: 'live', start_time: Time.now.utc, organization: org, creator: nil)
    v.channels << chs

    UserContentsQueue.expects(:buffer_flush).returns(nil).twice
    UserContentsQueue.expects(:enqueue_bulk_for_user).with(has_entries({user_id: us.first.id, content_type: 'VideoStream', flush: false, items: [{content_id: v.id, tags: [], channel_ids: chs.map(&:id), metadata: {'rationale' => {'type' => 'following_channel', 'channel_id' => chs.first.id}}}]})).returns(nil)
    UserContentsQueue.expects(:enqueue_bulk_for_user).with(has_entries({user_id: us.second.id, content_type: 'VideoStream', flush: false, items: [{content_id: v.id, tags: [], channel_ids: chs.map(&:id), metadata: {'rationale' => {'type' => 'following_channel', 'channel_id' => chs.last.id}}}]})).returns(nil)
    EnqueueStreamsJob.new.perform({id: v.id})
  end

  test 'should enqueue to followers and channel followers' do
    org = create(:organization)
    f1, f2, nf1, chf, direct_chf, ru, creator = create_list(:user, 7, organization: org)
    ch = create(:channel, autopull_content: true, label: 'some label', organization: org)
    direct_channel = create(:channel, autopull_content: false, organization: org)

    v = create(:wowza_video_stream, status: 'live', start_time: Time.now.utc, creator: creator, name: 'stream with tag1')
    v.tags << create(:tag, name: 'tag1')
    v.channels << direct_channel

    UserContentsQueue.expects(:buffer_flush).returns(nil).twice

    [f1, f2].each do |f|
      create(:follow, followable: creator, user: f)
      UserContentsQueue.expects(:enqueue_bulk_for_user).with(has_entries({user_id: f.id, flush: false, content_type: 'VideoStream', items: [{content_id: v.id, channel_ids: [direct_channel.id], tags: ['tag1'], metadata: {'rationale' => {'type' => 'following_user', 'user_id' => creator.id}}}]})).returns(nil)
    end

    # only direct channel follower sees the stream
    create(:follow, followable: ch, user: chf)
    create(:follow, followable: direct_channel, user: direct_chf)
    UserContentsQueue.expects(:enqueue_bulk_for_user).with(has_entries({user_id: direct_chf.id, flush: false, content_type: 'VideoStream', items: [{content_id: v.id, channel_ids: [direct_channel.id], tags: ['tag1'],metadata: {'rationale' => {'type' => 'following_channel', 'channel_id' => direct_channel.id}}}]})).returns(nil)

    EnqueueStreamsJob.new.perform({id: v.id})
  end

  test 'should respect channel privacy while enqueueing' do
    org = create(:organization)
    stream_creator, user_follower, channel_and_user_follower = create_list(:user, 3, organization: org)
    channel = create(:channel, is_private: true, organization: org)
    create(:follow, followable: stream_creator, user: user_follower)
    create(:follow, followable: stream_creator, user: channel_and_user_follower)
    create(:follow, followable: channel, user: channel_and_user_follower)

    public_stream = create(:wowza_video_stream, status: 'live', creator: stream_creator)
    channel_stream = create(:wowza_video_stream, status: 'live', creator: stream_creator)
    channel_stream.channels << channel

    UserContentsQueue.expects(:buffer_flush).returns(nil).twice

    UserContentsQueue.expects(:enqueue_bulk_for_user).with(has_entries({user_id: user_follower.id, flush: false, content_type: 'VideoStream', items: [{content_id: public_stream.id, channel_ids: [], tags: [], metadata: {'rationale' => {'type' => 'following_user', 'user_id' => stream_creator.id}}}]})).returns(nil)
    UserContentsQueue.expects(:enqueue_bulk_for_user).with(has_entries({user_id: channel_and_user_follower.id, flush: false, content_type: 'VideoStream', items: [{content_id: public_stream.id, channel_ids: [], tags: [], metadata: {'rationale' => {'type' => 'following_user', 'user_id' => stream_creator.id}}}]})).returns(nil)
    EnqueueStreamsJob.new.perform({id: public_stream.id})

    UserContentsQueue.expects(:enqueue_bulk_for_user).with(has_entries({user_id: channel_and_user_follower.id, flush: false, content_type: 'VideoStream', items: [{content_id: channel_stream.id, channel_ids: [channel.id], tags: [], metadata: {'rationale' => {'type' => 'following_channel', 'channel_id' => channel.id}}}]})).returns(nil)
    EnqueueStreamsJob.new.perform({id: channel_stream.id})
  end

end

require 'test_helper'

class InstructorDailyDigestReportJobTest < ActiveSupport::TestCase
  test 'calls instructor digest report service method with expected params' do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    to   = Time.parse("2016-05-16 00:00:40")
    from = to - 1.day

    InstructorDigestReportService.any_instance.expects(:generate_report).never
    InstructorDailyDigestReportJob.new.perform(instructor_data: {}, from: from.to_i, to: to.to_i)

    user = create(:user)
    instructor_data = { 'id' => user.id, 'language' => 'en', 'group_ids' => [] }

    InstructorDigestReportService.any_instance.expects(:generate_report).once
    InstructorDailyDigestReportJob.new.perform(
      instructor_data: instructor_data, from: from.to_i, to: to.to_i)
  end
end

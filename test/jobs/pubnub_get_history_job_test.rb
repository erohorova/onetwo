require 'test_helper'

class PubnubGetHistoryJobTest < ActiveSupport::TestCase

  setup do
    PubnubGetHistoryJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    @org = create(:organization)
    @users = create_list(:user, 3, organization: @org)

    # page 1, no comments here
    pagenum = 1
    @page1 = []
    @page1 << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'join', "uid" => @users.first.id, "type" => "videoStream"})
    @page1 << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'leave', "uid" => @users.second.id, "type" => "videoStream"})
    @page1 << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'join', "uid" => @users.last.id, "type" => "videoStream"})

    # some bad format
    @page1 << OpenStruct.new(status: 200, history_end: pagenum, message: {"type" => "badType"})

    # page 2, two comments here
    pagenum = 2
    @page2 = []
    @page2 << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'comment', "uid" => @users.first.id, "m" => "comment: 1" ,"type" => "videoStream"})
    @page2 << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'comment', "uid" => @users.second.id, "m" => "comment: 2" ,"type" => "videoStream"})
    @page2 << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'leave', "uid" => @users.last.id, "type" => "videoStream"})

    # some bad format
    @page2 << OpenStruct.new(status: 200, history_end: pagenum, message: {"type" => "videoStream", "a" => "bad action"})
    # bad uid
    @page2 << OpenStruct.new(status: 200, history_end: pagenum, message: {"type" => "videoStream", "a" => "comment"})
  end

  test 'record analytics' do
    v = create(:wowza_video_stream, status: 'past', creator: @users.first)
    channel = v.uuid
    pubnub = mock()

    data = []
    pagenum = 1
    data << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'like', "uid" => @users.first.id, "type" => "videoStream"})
    data << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'comment', "uid" => @users.second.id, "type" => "videoStream", "m" => "message"})
    data << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'join', "uid" => @users.last.id, "type" => "videoStream"})

    pubnub.expects(:history).with(channel: channel, reverse: true, http_sync: true, count: 100, start: nil).returns(data).once
    pubnub.expects(:history).with(channel: channel, reverse: true, http_sync: true, count: 100, start: 1).returns([]).once
    Pubnub.expects(:new).returns(pubnub).once

    MetricRecorderJob.unstub :perform_later
    MetricRecorderJob.expects(:perform_later).with(@users.first.id, "like", has_entries(object: 'video_stream', object_id: v.id)).returns(nil).once
    MetricRecorderJob.expects(:perform_later).with(@users.second.id, "comment", has_entries(object: 'video_stream', object_id: v.id)).returns(nil).once
    PubnubGetHistoryJob.new.perform(v.id)
  end

  test 'get history' do
    v = create(:wowza_video_stream, status: 'past', creator: @users.first)
    channel = v.uuid
    pubnub = mock()
    pubnub.expects(:history).with(channel: channel, reverse: true, http_sync: true, count: 100, start: nil).returns(@page1).once
    pubnub.expects(:history).with(channel: channel, reverse: true, http_sync: true, count: 100, start: 1).returns(@page2).once
    pubnub.expects(:history).with(channel: channel, reverse: true, http_sync: true, count: 100, start: 2).returns([]).once
    Pubnub.expects(:new).returns(pubnub).once

    create(:comment, commentable: v.card, user: @users.last)
    assert_equal 1, v.card.comments_count

    assert_difference ->{LiveComment.count}, 2 do
      PubnubGetHistoryJob.new.perform(v.id)
    end
    v.reload
    assert_equal 3, v.card.comments_count
    live_comments = v.card.comments.where(type: 'LiveComment')
    assert_equal ["comment: 1", "comment: 2"], live_comments.map(&:message)
    assert_equal [@users.first, @users.second], live_comments.map(&:user)
  end

  test 'join message' do
    v = create(:wowza_video_stream, status: 'past', creator: @users.last)
    us = create_list(:user, 3)
    v.watchers << us.first
    page1 = []
    pagenum = 1
    page1 << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'join', "uid" => us.first.id, "type" => "videoStream"})
    page1 << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'leave', "uid" => us.first.id, "type" => "videoStream"})
    page1 << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'join', "uid" => us.first.id, "type" => "videoStream"})
    page1 << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'anonymous_join', "uid" => -1, "type" => "videoStream"})
    page1 << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'anonymous_join', "uid" => -1, "deviceId" => nil, "type" => "videoStream"})
    page1 << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'anonymous_join', "uid" => -1, "deviceId" => "xyz", "type" => "videoStream"})

    page2 = []
    pagenum = 2
    page2 << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'join', "uid" => us.last.id, "type" => "videoStream"})
    page2 << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'leave', "uid" => us.last.id, "type" => "videoStream"})
    page2 << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'join', "uid" => us.last.id, "type" => "videoStream"})
    page2 << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'anonymous_join', "uid" => -1, "deviceId" => "abc", "type" => "videoStream"}) #should not count
    page2 << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'anonymous_join', "uid" => -1, "deviceId" => "xyz", "type" => "videoStream"})

    channel = v.uuid
    pubnub = mock()
    pubnub.expects(:history).with(channel: channel, reverse: true, http_sync: true, count: 100, start: nil).returns(page1).once
    pubnub.expects(:history).with(channel: channel, reverse: true, http_sync: true, count: 100, start: 1).returns(page2).once
    pubnub.expects(:history).with(channel: channel, reverse: true, http_sync: true, count: 100, start: 2).returns([]).once
    Pubnub.expects(:new).returns(pubnub).once
    assert_difference ->{VideoStreamsUser.count}, 1 do
      PubnubGetHistoryJob.new.perform(v.id)
    end
    v.reload
    assert_same_elements [us.first, us.last], v.watchers
    assert_equal 4, v.anon_watchers_count
  end

  test 'will not store deleted comments when stream was live from pubnub history' do
    pagenum = 1
    data = []
    @msgId1 = @users.first.id.to_s+"_"+DateTime.now.strftime('%Q').to_s
    @msgId2 = @users.second.id.to_s+"_"+DateTime.now.strftime('%Q').to_s
    @msgId3 = @users.last.id.to_s+"_"+DateTime.now.strftime('%Q').to_s

    data << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'join', "uid" => @users.first.id, "type" => "videoStream"})
    data << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'comment', "uid" => @users.first.id, "m" => "comment: 1" ,"type" => "videoStream", "msgId" => @msgId1})
    data << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'comment', "uid" => @users.second.id, "m" => "comment: 2" ,"type" => "videoStream", "msgId" => @msgId2})
    data << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'comment', "uid" => @users.last.id, "m" => "comment: 3" ,"type" => "videoStream", "msgId" => @msgId3})
    data << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'comment', "uid" => @users.last.id, "m" => "comment: 4" ,"type" => "videoStream"})
    data << OpenStruct.new(status: 200, history_end: pagenum, message: {"a" => 'leave', "uid" => @users.first.id, "type" => "videoStream"})
    data << OpenStruct.new(status: 200, history_end: pagenum, message: {"dc" => [@msgId1], "a" => "deletedComment"})

    video_stream = create(:wowza_video_stream, status: 'past', creator: @users.first)
    channel = video_stream.uuid
    pubnub = mock()

    pubnub.expects(:history).with(channel: channel, reverse: true, http_sync: true, count: 100, start: nil).returns(data).once
    pubnub.expects(:history).with(channel: channel, reverse: true, http_sync: true, count: 100, start: 1).returns([]).once
    Pubnub.expects(:new).returns(pubnub).once
    PubnubGetHistoryJob.new.perform(video_stream.id)
    video_stream.reload
    assert_equal 3, video_stream.card.comments_count
    live_comments = video_stream.card.comments.where(type: 'LiveComment')
    assert_same_elements ["comment: 2", "comment: 3", "comment: 4"], live_comments.map(&:message)
  end
end

require 'test_helper'

class WorkflowsPatcherJobTest < ActiveJob::TestCase
  include ActiveJob::TestHelper

  setup do
    WorkflowsPatcherJob.unstub(:perform_later)
    EdcastActiveJob.unstub(:perform_later)

    @org = create(:organization)
    @user = create(:user, organization: @org)

    data = {
      profile_id: '80',
      profile_external_id: 'ext_id-85',
      profile_uid: 'uid-85',
      id: '85',
      user_email: 'person1@example.com',
      user_first_name: 'fname1',
      user_last_name: 'lname1',
      user_status: 'active',
      user_created_at: 1538352000000
    }

    @patch = WorkflowPatchRevision.create(
      organization_id: @org.id,
      patch_type: 'delete',
      patch_id: 1538352000000,
      org_name: @org.host_name,
      data: data,
      data_id: data.dig(:data, :email, :user_email),
      in_progress: false,
      completed: false,
      version: 2
    )
  end

  test 'should properly enqueue workflow patcher job' do
    WorkflowsPatcherJob.any_instance.expects(:data_sender).with([@patch.id])
    WorkflowsPatcherJob.perform_now
  end
end

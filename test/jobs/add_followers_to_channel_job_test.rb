require 'test_helper'
class AddFollowersToChannelJobTest < ActiveSupport::TestCase

  setup do
    AddFollowersToChannelJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    @org = create(:organization)
    @user_1, @user_2 = create_list(:user, 2, organization: @org)
    @user_3 = create :user, organization: @org
    @other_user = create(:user, organization: @org)
  end

  test 'new user should autofollow channels' do
    channel = create(:channel, organization: @org)
    channel.followers << @user_3
    channel.reload

    assert Follow.where(followable: channel, user: @user_3).present?

    AddFollowersToChannelJob.perform_later(channel)
    [@user_1, @user_2].each do |u|
      assert Follow.where(followable: channel, user: u).present?
    end

    refute Follow.where(followable: @other_channel, user: @other_user).present?
  end
end

require 'test_helper'

class IndividualAssignmentJobTest < ActiveJob::TestCase
  test "should create individual assignments" do
    org = create(:organization)
    assignor = create(:user, organization: org)
    assignees = create_list(:user, 2, organization: org)
    card = create(:card, organization: org, author_id: nil)

    assert_difference -> {Assignment.count}, 2 do
      assert_difference -> {TeamAssignment.count}, 2 do

        IndividualAssignmentJob.perform_now({
          assignor_id: assignor.id,
          assignee_ids: assignees.map(&:id),
          assignable_id: card.id,
          opts: {}
        })

      end
    end

  end
end

require 'test_helper'
class AutoFollowChannelsJobTest < ActiveSupport::TestCase

  setup do
    AutoFollowChannelsJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    @org = create(:organization)
    @autofollow_channels = create_list(:channel, 2, organization: @org, auto_follow: true)
    @other_channel = create(:channel, organization: @org)
  end

  test 'new user should autofollow channels' do
    u = create(:user, organization: @org)
    AutoFollowChannelsJob.perform_later(user_id: u.id)
    @autofollow_channels.each do |ch|
      assert Follow.where(followable: ch, user: u).present?
    end
    refute Follow.where(followable: @other_channel, user: u).present?
  end

end

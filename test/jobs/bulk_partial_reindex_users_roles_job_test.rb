require 'test_helper'

class BulkPartialReindexUsersRolesJobTest < ActiveSupport::TestCase
  test 'should call BulkPartialIndexJob' do
    BulkPartialReindexJob.unstub :perform_later
    BulkPartialReindexUserRolesJob.unstub :perform_later
    org = create(:organization)
    user = create(:user,organization: org)
    role = create(:role, organization_id: org.id, default_name: 'sme', name:'sme')
    user.add_role(role)
    BulkPartialReindexJob.expects(:perform_later).with(ids: [user.id], klass_name: 'User', method_name: :roles_data)
    BulkPartialReindexUserRolesJob.perform_later(org_id:org.id, id: role.id)
  end
end

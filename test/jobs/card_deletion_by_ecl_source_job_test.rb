require 'test_helper'

class CardDeletionByEclSourceJobTest < ActiveJob::TestCase
  
  test 'when a source at ecl is deleted corresponding cards should be deleted' do
    CardDeletionByEclSourceJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    file_url = 'http://ecl-test.s3.amazonaws.com/deleted-source-ecl-test-source-123.csv'

    stub_request(:get, file_url).
          to_return(:status => 200, :body => 'ecl-test-123, ecl-test-456', headers: {})

    stub_request(:delete,'http://test.localhost/api/v1/sources/ecl-test-source-123/delete_s3_object_source').
          to_return(:status => 200)

    card1 = create(:card, ecl_id: 'ecl-test-123')
    card2 = create(:card, ecl_id: 'ecl-test-456')
    CardDeletionByEclSourceJob.perform_now('ecl-test-source-123', card1.organization_id, file_url)
    assert Card.where(ecl_id: ['ecl-test-123', 'ecl-test-123']).count.zero?
  end
end

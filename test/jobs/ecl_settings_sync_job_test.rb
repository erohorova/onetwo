require 'test_helper'

class EclSettingsSyncJobTest < ActiveJob::TestCase
  test 'should call settings from ecl' do
    organization = create(:organization)
    EclApi::EclSettings.any_instance.expects(:create_ecl_settings).with(default_source_enabled: false, sociative_integration: true).once
    EclSettingsSyncJob.perform_now(organization.id)
  end
end
require 'test_helper'

class ChannelEclSourcesFetchBatchJobTest < ActiveJob::TestCase
  setup do
    ChannelEclSourcesFetchBatchJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
  end

  test 'perform fetch job only for ecl enabled channels' do
    channel = create(:channel, ecl_enabled: false)
    assert_no_enqueued_jobs do
      ChannelEclSourcesFetchBatchJob.perform_now
    end

    channel.update(ecl_enabled: true)
    assert_enqueued_with(job: ChannelEclSourcesFetchJob, args: [channel.id, {only_default_sources: true}]) do
      ChannelEclSourcesFetchBatchJob.perform_now
    end
  end

  test 'perform fetch job only for channels where config channel_programming_v2 is OFF' do
    org = create(:organization)
    config = create(:config, category: 'settings',
                             name: 'channel_programming_v2',
                             data_type: 'boolean',
                             value: 'true',
                             configable_id: org.id,
                             configable_type: 'Organization')

    channel = create(:channel, ecl_enabled: true, organization: org)
    assert_no_enqueued_jobs do
      ChannelEclSourcesFetchBatchJob.perform_now
    end

    config.update(value: 'false')

    assert_enqueued_with(job: ChannelEclSourcesFetchJob, args: [channel.id, {only_default_sources: true}]) do
      ChannelEclSourcesFetchBatchJob.perform_now
    end
  end
end

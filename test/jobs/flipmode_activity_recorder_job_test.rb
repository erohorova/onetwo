require 'test_helper'

class FlipmodeActivityRecorderJobTest < ActiveSupport::TestCase

  test 'should send activity to savannah' do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    email = 'a@a.com'
    resource_id = 1

    stub_request(:post, "#{Settings.savannah_base_location}/courses/#{resource_id}/register_mobile_activity").
      with(:body => "{\"edcast_mobile_user_email\":\"#{email}\",\"EDCAST_TOKEN\":\"#{Settings.savannah_token}\"}").
      to_return(:status => 200, :body => "", :headers => {})

    FlipmodeActivityRecorderJob.new.perform(email, resource_id)

    # should log error if request fails
    stub_request(:post, "#{Settings.savannah_base_location}/courses/#{resource_id}/register_mobile_activity").
      with(:body => "{\"edcast_mobile_user_email\":\"#{email}\",\"EDCAST_TOKEN\":\"#{Settings.savannah_token}\"}").
      to_return(:status => 500, :body => "", :headers => {})

    Rails.logger.expects(:error).once
    FlipmodeActivityRecorderJob.new.perform(email, resource_id)
  end

end


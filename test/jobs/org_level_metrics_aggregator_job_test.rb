require 'test_helper'
class OrgLevelMetricsAggregatorJobTest < ActiveSupport::TestCase
  setup do
    unstub_background_jobs
  end

  test 'should call increment with right arguments' do
    user = create(:user)
    WebMock.disable!

    Timecop.freeze do
      OrganizationLevelMetric.expects(:increment_events_count).with({user_id: user.id, organization_id: user.organization_id, content_type: 'video_stream', action: "comment"}, 1, Time.at(Time.now.to_i)).returns(nil).once
      # consumed for actionable event 'comment'
      OrganizationLevelMetric.expects(:increment_events_count).with({user_id: user.id, organization_id: user.organization_id, content_type: 'video_stream', action: "impression"}, 1, Time.at(Time.now.to_i)).returns(nil).once

      mr = MetricRecord.new(action: "comment", object_id: 1, object_type: "video_stream", actor_id: user.id, owner_id: 2).save
    end
    WebMock.enable!
  end
end

require 'test_helper'

class WorkflowAfterProcessJobTest < ActiveJob::TestCase
  test 'should execute Team related actions after workflow is processed' do
    EdcastActiveJob.unstub(:perform_later)
    IndexJob.unstub(:perform_later)
    org = create :organization
    team = create :team, organization: org
    channel = create :channel, organization: org
    create :teams_channels_follow, team: team, channel: channel
    Channel.expects(:invalidate_followers_cache).with(channel.id).once
    IndexJob.expects(:perform_later).with('Team', team.id).once
    WorkflowAfterProcessJob.new.perform(model_name: 'Team', ids: team.id)
  end
end
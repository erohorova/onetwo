require 'test_helper'

class CardReportingIndexingJobTest < ActiveJob::TestCase

  setup do
    @user = create(:user)
    @org = @user.organization
    @card = create(:card, organization: @user.organization, author: @user, card_type: 'media')
    @card_reporting = create(:card_reporting, user_id: @user.id , card_id: @card.id, reason: "I am new")
    EdcastActiveJob.unstub :perform_later
    CardReportingIndexingJob.unstub :perform_later
  end

  test "should call service method to index record" do
    FlagContent::CardReportingSearchService.any_instance.expects(:save).once
    CardReportingIndexingJob.perform_now(card_id: @card.id)
  end
end

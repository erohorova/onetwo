require 'test_helper'

class AddUserToWhitelistedTeamsJobTest < ActiveJob::TestCase
  test "should add user to whitelisted domain group" do
    org = create(:organization)

    team = create(:team, organization: org, is_everyone_team: true, domains: "edcast.com")
    user = create(:user, organization: org, confirmed_at: Time.now, email: "test@edcast.com")

    WebMock.disable!
    Team.reindex
    assert_difference -> { TeamsUser.count }, 1 do
      AddUserToWhitelistedTeamsJob.perform_now(user_id: user.id)
    end
    WebMock.enable!
  end
end

require 'test_helper'

class LmsXapiSyncJobTest < ActiveSupport::TestCase
  test 'should call LMS to sync xapi credentials' do
    organization = create :organization
    xapi_credential = create :xapi_credential, organization: organization

    LmsIntegration::XapiCredential.any_instance.expects(:sync_xapi_credentials).once
    LmsXapiSyncJob.perform_now(xapi_credential.id)
  end
end

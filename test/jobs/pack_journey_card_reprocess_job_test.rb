require 'test_helper'
class PackJourneyCardReprocessJobTest < ActiveSupport::TestCase
  setup do
    @org = create(:organization)
    @user = create(:user, organization: @org)
    @smartcard = create :card, organization: @org, author: @user
    @smartcard2 = create :card, organization: @org, author: @user
    @pack_card = create(:card, card_type: 'pack', card_subtype: 'simple', organization: @org, author: @user)
    @journey = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: @user, organization: @org)
  end

  test 'should update completed_percentage of pathway if card is added to pathway' do
    @pack_card.add_card_to_pathway(@smartcard.id, 'Card')
    smartcard_completion = create(:user_content_completion,         
                                  completable: @smartcard,
                                  user: @user, 
                                  state: "completed",
                                  completed_percentage: 100)
    pack_completion = create(:user_content_completion,         
                              completable: @pack_card,
                              user: @user, 
                              state: "completed",
                              completed_percentage: 98)
    @pack_card.add_card_to_pathway(@smartcard2.id, 'Card')
    CompletionPercentageService.reset_completed_percentage(@pack_card.id)
    pack_completion.reload
    assert_equal 50, pack_completion.completed_percentage 
  end

  test 'should update completed_percentage of pathway if card is removed from pathway' do
    @pack_card.add_card_to_pathway(@smartcard.id, 'Card')
    @pack_card.add_card_to_pathway(@smartcard2.id, 'Card')

    smartcard_completion = create(:user_content_completion,         
                                  completable: @smartcard,
                                  user: @user, 
                                  state: "completed",
                                  completed_percentage: 100)
    
    pack_completion = create(:user_content_completion,         
                              completable: @pack_card,
                              user: @user, 
                              state: "completed",
                              completed_percentage: 50)
    @pack_card.remove_card_from_pathway(@smartcard2.id, 'Card', {delete_dependent: true})
    CompletionPercentageService.reset_completed_percentage(@pack_card.id)
    pack_completion.reload
    assert_equal 98, pack_completion.completed_percentage 
  end

  test 'should update completed_percentage of journey if card is added to journey' do
    @journey.add_card_to_journey(@pack_card.id)
    @pack_card.add_card_to_pathway(@smartcard.id, "Card")

    smartcard_completion = create(:user_content_completion,         
                                  completable: @smartcard,
                                  user: @user, 
                                  state: "completed",
                                  completed_percentage: 100)
    
    journey_completion = create(:user_content_completion,         
                              completable: @journey,
                              user: @user, 
                              state: "completed",
                              completed_percentage: 98)

    @pack_card.add_card_to_pathway(@smartcard2.id, "Card")
    CompletionPercentageService.reset_completed_percentage(@journey.id)
    journey_completion.reload
    assert_equal 50, journey_completion.completed_percentage 
  end

  test 'should update completed_percentage of journey if card is removed from journey' do
    @pack_card2 = create(:card, card_type: 'pack', card_subtype: 'simple', organization: @org, author: @user)
    @journey.add_card_to_journey(@pack_card.id)
    @pack_card.add_card_to_pathway(@smartcard.id, "Card")
    @pack_card2.add_card_to_pathway(@smartcard2.id, "Card")

    smartcard_completion = create(:user_content_completion,         
                                  completable: @smartcard,
                                  user: @user, 
                                  state: "completed",
                                  completed_percentage: 100)
    
    journey_completion = create(:user_content_completion,         
                              completable: @journey,
                              user: @user, 
                              state: "completed",
                              completed_percentage: 50)
    @journey.remove_card_from_journey(@pack_card2.id, true)
    CompletionPercentageService.reset_completed_percentage(@journey.id)
    journey_completion.reload
    assert_equal 98, journey_completion.completed_percentage 
  end
end

require 'test_helper'

class AssessmentReportGenerationJobTest < ActiveSupport::TestCase
  test 'should call the expected analytics report type for assessments' do
    user = create(:user)
    card = create(:card)
    AnalyticsReport::AssessmentReport.any_instance.expects(:initialize).with({card_id: card.id, user_id: user.id}).once
    AnalyticsReport::AssessmentReport.any_instance.expects(:generate_report).once
    User.any_instance.expects(:send_assessment_report_email_for).once.returns(true)

    AssessmentReportGenerationJob.perform_now(card_id: card.id, user_id: user.id)
  end
end
require 'test_helper'

class ChannelProgrammingBatchJobTest < ActiveJob::TestCase
  setup do
    ChannelProgrammingBatchJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
  end

  test 'perform fetch job only for channels where config channel_programming_v2 is ON' do
    org = create(:organization)
    config = create(:config, category: 'settings', 
                             name: 'channel_programming_v2', 
                             data_type: 'boolean', 
                             value: 'true', 
                             configable_id: org.id, 
                             configable_type: 'Organization')

    channel = create(:channel, ecl_enabled: true, organization: org)
    ChannelProgrammingJob.expects(:perform_later).with(channel.id).once
    ChannelProgrammingBatchJob.perform_now
    
    config.update(value: 'false')
    ChannelProgrammingJob.expects(:perform_later).with(channel.id).never
    ChannelProgrammingBatchJob.perform_now
  end
end

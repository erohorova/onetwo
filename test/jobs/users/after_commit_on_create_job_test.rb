require 'test_helper'

class Users::AfterCommitOnCreateJobTest < ActiveJob::TestCase
  test "should perform user create offload tasks " do
    org = create(:organization)
    user = create(:user, organization: org)

    User.any_instance.expects(:add_to_org_general_channel)
    User.any_instance.expects(:cleanup_invitation)

    Users::AfterCommitOnCreateJob
    .perform_now(
      user_id: user.id
    )
  end
end

require 'test_helper'

class Users::AddUserToEveryoneGroupJobTest < ActiveJob::TestCase
  test "should add user to everyone group" do
    org = create(:organization)
    team = create(:team, organization: org, is_everyone_team: true)
    user = create(:user, organization: org, confirmed_at: Time.now)

    assert_difference -> { TeamsUser.count }, 1 do
      Users::AddUserToEveryoneGroupJob
        .perform_now(
          user_id: user.id
        )
    end
  end
end

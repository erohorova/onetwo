require 'test_helper'

class RecalculateAssignmentPerformanceMetricJobTest < ActiveJob::TestCase
  test 'should update team assignment metric for `assigned`, `completed`, `started` states' do
    org = create(:organization)
    card = create(:card, author_id: nil, organization: org)
    assignees = create_list(:user, 4, organization: org)
    assignor1, assignor2 = create_list(:user, 2, organization: org)
    team = create(:team, organization: org)

    assignment1 = create(:assignment, assignable: card, assignee: assignees[0])
    assignment2 = create(:assignment, assignable: card, assignee: assignees[1])
    assignment3 = create(:assignment, assignable: card, assignee: assignees[2])
    assignment4 = create(:assignment, assignable: card, assignee: assignees[3])

    # State #completed: Mark an assignment as completed.
    assignment2.start!
    assignment2.complete!
    # State #started: Mark an assignment as started.
    assignment3.start!
    # State #dismissed: Mark an assignment as dismissed.
    assignment4.start!
    assignment4.dismiss!

    assert_difference -> {TeamAssignmentMetric.count}, 1 do
      create(:team_assignment, team: team, assignor: assignor1, assignment: assignment1)
      create(:team_assignment, team: team, assignor: assignor1, assignment: assignment2)
      AssignmentPerformanceMetricJob.perform_now([card.id])
    end
    
    team1_metric = TeamAssignmentMetric.find_by(team_id: team.id)
    assert 1, team1_metric.assigned_count
    assert 1, team1_metric.completed_count
    assert 0, team1_metric.started_count

    # destroying team assignments for a card
    TeamAssignment.where(assignment_id: card.assignments.pluck(:id), team_id: team.id).destroy_all
    assert_empty team.assignments

    RecalculateAssignmentPerformanceMetricJob.perform_now(team.id, [assignor1.id], card.id)
    
    team1_metric_updated = TeamAssignmentMetric.find_by(team_id: team.id, card_id: card.id)
    assert 0, team1_metric_updated.assigned_count
    assert 0, team1_metric_updated.completed_count
    assert 0, team1_metric_updated.started_count
  end
end
require 'test_helper'

class TeamsChannelsAutofollowJobJobTest < ActiveSupport::TestCase
  setup do
    WebMock.disable!
    @org = create(:organization)
    @users = create_list(:user, 10, organization: @org)
    @channel = create(:channel, organization: @org)
    @teams = create_list(:team, 2, organization: @org)
  end

  teardown do
    WebMock.enable!
  end

  test 'should return false if channel_id is not passed' do
    refute TeamsChannelsAutofollowJob.perform_now()
  end

  test "add followers to channel if passed as followers_ids" do
    TeamsChannelsAutofollowJob.perform_now(channel_id: @channel.id, followers_ids: [@users[0]])
    assert_same_elements @channel.all_followers.map(&:id),  [@users[0].id]
  end

  test "should not add duplicate followers" do
    @channel.add_followers [@users[0]]
    assert_no_difference -> {@channel.followers.count} do
      TeamsChannelsAutofollowJob.perform_now(channel_id: @channel.id, followers_ids: [@users[0]])
    end
  end

  test "should follow all team members for the channel" do
    @channel.followed_teams << @teams[0]
    @users.each {|user| @teams[0].add_member user}
    assert_equal @channel.all_followers.count, @teams[0].users.count
    assert_no_difference -> {@channel.all_followers.count}, 0 do
      TeamsChannelsAutofollowJob.perform_now(team_ids: [@teams[0].id], channel_id: @channel.id, followers_ids:[])
    end
  end

  test "should notify admin from old notification framework" do
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(false)
    admin = create :user, organization: @org, organization_role: 'admin'
    UserMailer.expects(:channel_follows_notification_to_admin).times(1)
    EDCAST_NOTIFY.expects(:trigger_event).never

    TeamsChannelsAutofollowJob.perform_now(channel_id: @channel.id, followers_ids: [@users[0]], sender_id: admin.id)
    Organization.any_instance.unstub(:notifications_and_triggers_enabled?)
  end

  test "should notify admin from new notification framework" do
    config = build :notification_config, organization: @org
    config.set_enabled(Notify::NOTE_ADDED_CHANNEL_FOLLOWER_ADMIN, :email, :single)
    config.save
    admin = create :user, organization: @org, organization_role: 'admin'
    EDCAST_NOTIFY.expects(:trigger_event).times(1)
    UserMailer.expects(:channel_follows_notification_to_admin).never

    TeamsChannelsAutofollowJob.perform_now(channel_id: @channel.id, followers_ids: [@users[0]], sender_id: admin.id)
  end

  test 'it should run channel autofollow job inline for after followed' do
    @teams[0].add_user(@users[0])
    TeamsChannelsAutofollowJob.any_instance.expects(:after_followed).once
    TeamsChannelsAutofollowJob.perform_now(channel_id: @channel.id, team_ids: [@teams[0].id])
  end
end
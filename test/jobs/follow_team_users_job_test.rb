require 'test_helper'
class FollowTeamUsersJobTest < ActiveSupport::TestCase

   setup do
    FollowTeamUsersJob.unstub :perform_later
    MetricRecorderJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    @org = create(:organization)
    @team = create(:team, organization: @org)
    @user = create(:user, organization: @org)
    @team.add_admin @user
    @member1, @member2 = create_list(:user, 2, organization: @org)
    @team.add_member @member1
    @team.add_member @member2
  end

  test 'All team users should be followed' do
    assert_difference -> {Follow.count}, 2 do
      FollowTeamUsersJob.perform_later(user_id: @member1.id, team_id: @team.id)
    end
  end

  test 'follow only users which are not followed' do
    members = create_list(:user, 2, organization: @org)
    create(:follow, user: @member1, followable: @member2)
    create(:follow, user: @member1, followable: @user)
    members.map {|m| @team.add_member(m)}
    #members will only followed
    assert_difference -> {Follow.count},2 do
      FollowTeamUsersJob.perform_later(user_id: @member1.id, team_id: @team.id)
    end
  end

  test 'track follow action' do
    Tracking.unstub :track_act
    new_member = create(:user, organization: @org)
    create(:follow, user: @member1, followable: @member2)
    create(:follow, user: @member1, followable: @user)
    @team.add_member(new_member)

    MetricRecord.unstub(:create)
    MetricRecord.expects(:create).with(
      actor_type: 'user',
      actor_id: @member1.id,
      object_type: 'user',
      object_id: new_member.id,
      owner_type: 'user',
      owner_id: new_member.id,
      action: 'follow',
      platform: 'web',
      device_id: 'device123',
      properties: nil,
      organization_id: @member1.organization_id,
      referer: nil,
      ecl_id: nil
    )

    assert_difference -> {Follow.count}, 1 do
      FollowTeamUsersJob.perform_later(user_id: @member1.id, team_id: @team.id, platform: 'web', device_id: 'device123')
    end
  end
end

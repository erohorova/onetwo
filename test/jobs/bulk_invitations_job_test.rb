require 'test_helper'

class BulkInvitationsJobTest < ActiveSupport::TestCase
  setup do
    BulkInvitationsJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    @email_mock = mock('email')
    @email_mock.stubs(:deliver_now)
  end

  test 'should send confirmation mail with failure message of processed CSV' do
    failure_msg = "We were unable to process your import file. Please contact support@edcast.com."
    team = create :team
    inv_file = create(:invitation_file, invitable: team, sender: create(:user, organization: team.organization))
    CSV.stubs(:parse).returns([])

    InvitationMandrillMailer.expects(:import_confirmation).with(inv_file.sender, failure_msg, team)
      .returns(@email_mock).once
    BulkInvitationsJob.new.perform(invitation_file_id: inv_file.id)
  end

  test 'should send confirmation mail with success message of processed CSV' do
    team = create :team, name: "my group"
    success_msg = "We have processed your import file successfully and sent out 1 of 1 invitations for my group Group "
    inv_file = create(:invitation_file, invitable: team, sender: create(:user, organization: team.organization))

    InvitationMandrillMailer.expects(:import_confirmation).with(inv_file.sender, success_msg, team)
      .returns(@email_mock).once
    BulkInvitationsJob.new.perform(invitation_file_id: inv_file.id)
  end
end
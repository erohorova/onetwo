require 'test_helper'

class TeamBatchAssignmentJobTest < ActiveSupport::TestCase
  test 'should mass assign assignment to team users' do
    org = create(:organization)
    team = create(:team, organization: org)
    users = create_list(:user, 5, organization: org)
    admin_user = create(:user, organization: org)

    users.each {|user| team.add_member(user)}
    team.add_admin admin_user

    card = create(:card, author_id: users.last.id)

    assert_differences [
                          [-> {Assignment.count}, 5],
                          [->{TeamAssignment.count}, 5]] do
      TeamBatchAssignmentJob.new.perform(team_id: team.id, assignor_id: admin_user.id, assignee_ids: users.map(&:id), assignable_id: card.id, assignable_type: 'Card', opts: {message: "this is test assignment message"})
    end

    assert_equal [card.id], users.first.assignments.map(&:assignable).map(&:id)
    assert_equal [card.id], team.assignments.map(&:assignable).map(&:id).uniq
    assert_equal ["this is test assignment message"], team.team_assignments.map(&:message).uniq
  end

  test 'should send assignments even without message' do
    org = create(:organization)
    team = create(:team, organization: org)
    users = create_list(:user, 2, organization: org)
    admin_user = create(:user, organization: org)

    users.each {|user| team.add_member(user)}
    team.add_admin admin_user

    card = create(:card, author_id: users.last.id)

    assert_differences [
                          [-> {Assignment.count}, 2],
                          [->{TeamAssignment.count}, 2]] do
      TeamBatchAssignmentJob.new.perform(team_id: team.id, assignor_id: admin_user.id, assignee_ids: users.map(&:id), assignable_id: card.id, assignable_type: 'Card')
    end

    assert_nil team.team_assignments.map(&:message).uniq.first
  end

  test 'should mass assign assignment to team users with due date' do
    org = create(:organization)
    team = create(:team, organization: org)
    users = create_list(:user, 2, organization: org)
    admin_user = create(:user, organization: org)
    future_date = (Date.today + 10.days).end_of_day

    users.each {|user| team.add_member(user)}
    team.add_admin admin_user

    card = create(:card, author_id: users.last.id)

    assert_differences [
                          [-> {Assignment.count}, 2],
                          [->{TeamAssignment.count}, 2]
                        ] do
      TeamBatchAssignmentJob.new.perform(team_id: team.id, assignor_id: admin_user.id, assignee_ids: users.map(&:id), assignable_id: card.id, assignable_type: 'Card', opts: {due_at: future_date.strftime("%m/%d/%Y")})
    end

    assert_equal future_date.to_i, users.last.assignments.last.due_at.to_i
  end

  test 'should assign to selected users in a team' do
    org = create(:organization)
    teams = create_list(:team, 2, organization: org)
    users = create_list(:user, 5, organization: org)
    admin_user = create(:user, organization: org)

    users1 = users[0..2]
    users2 = users[3..4]

    users.each {|user| teams.first.add_member(user)}
    users.each {|user| teams.last.add_member(user)}

    teams.first.add_admin admin_user
    teams.last.add_admin admin_user

    card = create(:card, author_id: users.last.id)

    assert_differences [
                          [-> {Assignment.count}, 5],
                          [->{TeamAssignment.count}, 5]
                        ] do

      TeamBatchAssignmentJob.new.perform(team_id: teams.first.id, assignor_id: admin_user.id, assignee_ids: users1.map(&:id), assignable_id: card.id, assignable_type: 'Card')
      TeamBatchAssignmentJob.new.perform(team_id: teams.last.id, assignor_id: admin_user.id, assignee_ids: users2.map(&:id), assignable_id: card.id, assignable_type: 'Card')
    end
  end

  test 'should be able to assign to admin himself' do
    org = create(:organization)
    team = create(:team, organization: org)
    users = create_list(:user, 2, organization: org)
    admin_user = create(:user, organization: org)

    users.each {|user| team.add_member(user)}
    team.add_admin admin_user

    card = create(:card, author_id: users.last.id)

    assert_differences [
                          [-> {Assignment.count}, 3],
                          [->{TeamAssignment.count}, 3]] do
      TeamBatchAssignmentJob.new.perform(team_id: team.id, assignor_id: admin_user.id, assignee_ids: (users + [admin_user]).map(&:id), assignable_id: card.id, assignable_type: 'Card', opts: {self_assign: true})
    end

    assert_equal [card.id], users.first.assignments.map(&:assignable).map(&:id)
    assert_equal [card.id], team.assignments.map(&:assignable).map(&:id).uniq
  end

  test 'should handle common members from multiple groups while assigning card' do
    org = create(:organization)
    teams = create_list(:team, 2, organization: org)
    users1 = create_list(:user, 2, organization: org)
    admin_user = create(:user, organization: org)

    users1.each do |user|
      teams.first.add_member user
    end

    teams.last.add_member users1.first

    teams.first.add_admin admin_user
    teams.last.add_admin admin_user

    card = create(:card, author_id: users1.last.id)

    assert_differences [
                          [-> {Assignment.count}, 2],
                          [->{TeamAssignment.count}, 3]] do
      TeamBatchAssignmentJob.new.perform(
        team_id: teams.first.id,
        assignor_id: admin_user.id,
        assignee_ids: users1.map(&:id),
        assignable_id: card.id,
        assignable_type: 'Card',
        opts: {self_assign: false}
      )
      TeamBatchAssignmentJob.new.perform(
        team_id: teams.last.id,
        assignor_id: admin_user.id,
        assignee_ids: users1.map(&:id),
        assignable_id: card.id,
        assignable_type: 'Card',
        opts: {self_assign: false}
      )
    end

    assert_equal 2, teams.first.team_assignments.count
    assert_equal 1, teams.last.team_assignments.count
    assert_equal 2, (teams.first.assignments + teams.last.assignments).uniq.count
  end

end
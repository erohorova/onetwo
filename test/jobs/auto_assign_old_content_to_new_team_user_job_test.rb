require 'test_helper'
class AutoAssignOldContentToNewTeamUserJobTest < ActiveSupport::TestCase

  test 'new user should auto assigned old group assignments' do
    AutoAssignOldContentToNewTeamUserJob.unstub :perform_later
    NotificationGeneratorJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    org = create(:organization)
    user, user1, user2, assignor, author = create_list(:user, 5, organization: org)
    team = create(:team, organization: org, auto_assign_content: true)
    
    team_user = team.teams_users.create(user: user, as_type: 'member')
    team_user1 = team.teams_users.create(user: user2, as_type: 'member')
    
    card = create(:card, organization: org, author: author)
    
    assignment = create(:assignment, assignable: card, assignee: user)
    assignment1 = create(:assignment, assignable: card, assignee: user2)
    
    team_assignment = create(:team_assignment, team_id: team.id, assignor: assignor, assignment: assignment)
    team_assignment1 = create(:team_assignment, team_id: team.id, assignor: assignor, assignment: assignment1)
    team_user = team.teams_users.create(user: user1, as_type: 'member')
    
    assert_difference ->{Assignment.count}, 1 do
      EDCAST_NOTIFY.expects(:trigger_event).never
      AutoAssignOldContentToNewTeamUserJob.new.perform(user_id: user1.id, team_id: team.id)
    end
  end
end

require 'test_helper'

class EventReminderJobTest < ActiveSupport::TestCase
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    EventReminderJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    @org = create(:organization)
    @client = create(:client, organization: @org)
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(false)
    @user1 = create(:user, organization: @org)
    @user2 = create(:user, organization: @org)
    @user3 = create(:user, organization: @org)
    @group, @user = create_gu(user_opts: {organization_id: @org.id})
    @group, @user = create_gu(user_opts: {}, group_opts: {organization_id: @org.id, client_resource_id: 1}, role: 'admin')
    @event = create(:event, event_start: Time.now + 1.day, user: @user, group: @group)
    create(:events_user, :rsvp_yes, event: @event, user: @user1)
    create(:events_user, :rsvp_yes, event: @event, user: @user2)
    create(:events_user, :rsvp_yes, event: @event, user: @user3)
  end


  test 'should reschedule the reminder email if the start time is changed' do
    ActiveJob::Base.queue_adapter = :sidekiq
    @event.run_callbacks(:commit) #runs the create callback
    @event = Event.find(@event.id)
    @event.event_start = 2.days.from_now
    job = mock()
    EventReminderJob.expects(:set).with({wait_until: @event.event_start - 1.hour}).returns(job)
    job.expects(:perform_later).with(event_id: @event.id, event_start: @event.event_start.to_i)
    @event.save!
    @event.run_callbacks(:commit) #runs the update callback
    ActiveJob::Base.queue_adapter = :inline
  end

  test 'should schedule an email to mandrill' do
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:rsvp] = message[:message][:merge_vars].first[:vars][3][:name]
      mail_args[:e_name] = message[:message][:global_merge_vars].select { |hash| hash['name'] == 'event_name' }.first['content']
    }.once

    EventReminderJob.perform_now(event_id: @event.id, event_start: @event.event_start.to_i)

    assert_match 'rsvp_status', mail_args[:rsvp]
    assert_match @event.name, mail_args[:e_name]
  end
end

require 'test_helper'

class TransferOwnershipJobTest < ActiveJob::TestCase
  setup do
    @org = create :organization
    @user = create :user, organization: @org
    @user1 = create :user, organization: @org
  end

  test 'should update project reviewer when author is changed' do
    cover = create(:card, organization: @org, author: @user, card_type: 'pack')
    card1 = create(:card,  organization: @org, author: @user, card_type: 'project', card_subtype: "text", message: 'card1', hidden: true)
    card2 = create(:card,  organization_id: @org.id, author: @user, card_type: 'media', message: 'card2', hidden: false)
    project = card1.project
    CardPackRelation.add(cover_id: cover.id, add_id: card1.id, add_type: 'Card')
    CardPackRelation.add(cover_id: cover.id, add_id: card2.id, add_type: 'Card')

    assert_equal project.reviewer_id, @user.id
    cover.update(author_id: @user1.id)
    TransferOwnershipJob.perform_now(cover.id, {old_author_id: @user.id})

    [card1, card1.project, card2].each {|c| c.reload}
 
    assert_equal  @user1.id, card1.author_id
    assert_equal  @user1.id, card1.project.reviewer_id
    assert_equal @user.id, card2.author_id
  end

  test "change_author should update author of hidden cards inside journey" do
    journey = create(:card, card_type: 'journey', card_subtype: 'self_paced', organization_id: @org.id, author: @user)
    hidden_pathway = create(:card, card_type: 'pack', card_subtype: 'simple', organization_id: @org.id, author: @user, hidden: true)
    card1 = create(:card,  organization_id: @org.id, author: @user, card_type: 'media', message: 'card1', hidden: true)
    CardPackRelation.add(cover_id: hidden_pathway.id, add_id: card1.id, add_type: 'Card')
    JourneyPackRelation.add(cover_id: journey.id, add_id: hidden_pathway.id)
    
    pathway = create(:card, card_type: 'pack', card_subtype: 'simple', organization_id: @org.id, author: @user) 
    card2 = create(:card,  organization_id: @org.id, author: @user, card_type: 'media', message: 'card2', hidden: false)
    CardPackRelation.add(cover_id: pathway.id, add_id: card2.id, add_type: 'Card')
    JourneyPackRelation.add(cover_id: journey.id, add_id: pathway.id)
    
    journey.update(author_id: @user1.id)
    TransferOwnershipJob.perform_now(journey.id, {old_author_id: @user.id})
    [hidden_pathway, pathway, card1, card2].each{|c| c.reload}
    
    assert_equal hidden_pathway.author_id, @user1.id
    assert_equal card1.author_id, @user1.id
    assert_equal card2.author_id, @user.id
    assert_equal pathway.author_id, @user.id
  end

  test "should not change author of paid hidden card" do
    pathway = create(:card, card_type: 'pack', card_subtype: 'simple', organization_id: @org.id, author: @user) 
    card2 = create(:card,  organization_id: @org.id, author: @user, card_type: 'media', message: 'card2', hidden: true, is_paid: true)
    CardPackRelation.add(cover_id: pathway.id, add_id: card2.id, add_type: 'Card')

    pathway.update(author_id: @user1.id)
    pathway.current_user = @user
    TransferOwnershipJob.perform_now(pathway.id, {old_author_id: @user.id})
    [pathway, card2].each {|c| c.reload}

    assert_equal pathway.author_id, @user1.id
    assert_equal card2.author_id, @user.id
  end

  test "should trigger notification to old and new author of the card" do
    card = create :card, organization: @org, author: @user
    card.update(author_id: @user1.id)
    EDCAST_NOTIFY.expects(:trigger_event).with(Notify::EVENT_CHANGE_AUTHOR, card).once
    EDCAST_NOTIFY.expects(:trigger_event).with(Notify::EVENT_NOTIFY_OLD_AUTHOR, card, {old_author_id: @user.id}).once

    TransferOwnershipJob.perform_now(card.id, {old_author_id: @user.id})
  end

  test "should trigger log history job for versioning" do
    pathway = create(:card, card_type: 'pack', card_subtype: 'simple', organization_id: @org.id, author: @user)
    card1 = create(:card,  organization_id: @org.id, author: @user, card_type: 'media', message: 'card1', hidden: true)
    CardPackRelation.add(cover_id: pathway.id, add_id: card1.id, add_type: 'Card')

    LogHistoryJob.expects(:perform_later).with("entity_type" => 'card', "entity_id" => card1.id,
      "changes" => {"author_id" => [@user.id, @user1.id]}, "user_id" => @user.id).once

    pathway.update(author_id: @user1.id)
    TransferOwnershipJob.perform_now(pathway.id, {old_author_id: @user.id, actor_id: @user.id})
  end

  test "Should not notify when actor is old author" do
    card = create :card, organization: @org, author: @user
    card.update(author_id: @user1.id)

    EDCAST_NOTIFY.expects(:trigger_event).with(Notify::EVENT_CHANGE_AUTHOR, card).once
    EDCAST_NOTIFY.expects(:trigger_event).with(Notify::EVENT_NOTIFY_OLD_AUTHOR, card, {old_author_id: @user.id}).never

    TransferOwnershipJob.perform_now(card.id, {old_author_id: @user.id, actor_id: @user.id})
  end

  test "Should not notify when actor is new author" do
    card = create :card, organization: @org, author: @user
    card.update(author_id: @user1.id)

    EDCAST_NOTIFY.expects(:trigger_event).with(Notify::EVENT_CHANGE_AUTHOR, card).never
    EDCAST_NOTIFY.expects(:trigger_event).with(Notify::EVENT_NOTIFY_OLD_AUTHOR, card, {old_author_id: @user.id}).once

    TransferOwnershipJob.perform_now(card.id, {old_author_id: @user.id, actor_id: @user1.id})
  end
end

require 'test_helper'

class SetUserTimeZoneJobTest < ActiveSupport::TestCase
  setup do
    response_body = {
      "country_code" => "JP",
      "country_name" => "Japan",
      "time_zone"    => "Asia/Tokyo"
    }

    stub_ip_lookup(ip_address: '127.0.0.1')
    stub_ip_lookup(ip_address: '127.0.0.2', response_body: response_body)
  end

  test 'should fetch user time zone' do
    user = create(:user)

    # no time zone response
    assert_no_difference -> { UserProfile.count } do
      SetUserTimeZoneJob.perform_now(user_id: user.id, ip_address: "127.0.0.1")
    end

    # successful time zone response
    assert_difference -> { UserProfile.count } do
      SetUserTimeZoneJob.perform_now(user_id: user.id, ip_address: "127.0.0.2")
    end
    assert_equal "Asia/Tokyo", user.profile.time_zone
  end

  test 'should not create a new profile' do
    user = create(:user)
    user_profile = create(:user_profile, user: user)

    SetUserTimeZoneJob.perform_now(user_id: user.id, ip_address: "127.0.0.2")

    assert_equal user_profile.id, user.profile.id
  end
end

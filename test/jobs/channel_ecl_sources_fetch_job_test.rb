require 'test_helper'

class ChannelEclSourcesFetchJobTest < ActiveSupport::TestCase
  include RequestHelperMethods

  test 'perform fetch job for channel topics' do
    IndexJob.stubs(:perform_later)
    channel = create(:channel, ecl_enabled: true)
    filter_params = { only_default_sources: true }

    # with no channel topics
    EclApi::EclSearch.any_instance.expects(:search).never
    ChannelEclSourcesFetchJob.perform_now(channel.id, filter_params)

    # with channel topics and topic_ids
    channel.topics = [{'id' => '11-11-11', 'name' => 'edcast.test', 'label' => 'test'}, {'id' => '11-11-11', 'name' => 'edcast.test', 'label' => 'test'}]
    channel.save

    topics = channel.topics.map {|topic| topic['name']}
    topic_ids = channel.topics.map {|topic| topic['id']}
    filter_params['topic'] = topics
    filter_params['topic_ids'] = topic_ids
    connection = EclApi::EclSearch.new(channel.organization_id)
    ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items"]

    # return virtual cards
    virtual_cards = connection.send(:_format_data_as_cards, ecl_response['data'])
    EclApi::EclSearch.any_instance.expects(:search).
      with(query: '', limit: EclSource::DEFAULT_NUMBER_OF_ITEMS, offset:0, filter_params: filter_params) do |actual_parameters|
        assert_same_elements filter_params['topic'], topics #with only checks first level params
      end.returns(OpenStruct.new(results: virtual_cards, aggregations: []))

    # stub content_items/:ecl_id card
    ecl_id1 = ecl_response['data'][0]['id']
    ecl_response1 = get_ecl_card_response(ecl_id1)
    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id1}").
      to_return(:status => 200, :body => ecl_response1.to_json)

    ecl_id2 = ecl_response['data'][1]['id']
    ecl_response2 = get_ecl_card_response(ecl_id2)
    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id2}").
      to_return(:status => 200, :body => ecl_response2.to_json)

    ecl_id3 = ecl_response['data'][2]['id']
    ecl_response3 = get_ecl_card_response(ecl_id3)
    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id3}").
      to_return(:status => 200, :body => ecl_response3.to_json)

  stub_request(:get, "http://localhost:9200/cards_test/_search").to_return(:status => 200, :body => "", :headers => {})

    assert_difference ->{Card.count}, 3 do
      ChannelEclSourcesFetchJob.perform_now(channel.id, filter_params)
    end

    #assert card channel association
    card1, card2 = Card.last(2)
    assert_includes card1.channels, channel
    assert_includes card2.channels, channel
  end
end

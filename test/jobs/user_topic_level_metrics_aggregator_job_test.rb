require 'test_helper'
class UserTopicLevelMetricsAggregatorJobTest < ActiveSupport::TestCase

  setup do
    skip

    WebMock.disable!
  end

  teardown do
    WebMock.enable!
  end

  test 'should call increment with right arguments' do
    UserTopicLevelMetricsAggregatorJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    org = create(:organization)
    user = create(:user, organization: org)
    video_stream = create(:wowza_video_stream, creator: create(:user, organization: org))

    tags = create_list(:tag, 3)
    tags.first(2).each {|t| t.update_attributes(type: "Interest")}
    video_stream.tags << tags
    video_stream.reload

    Timecop.freeze do
      tags.first(2).each do |topic|
        UserTopicLevelMetric.expects(:update_from_increments).with(opts: {user_id: user.id, tag_id: topic.id}, increments: [[:smartbites_score, 10]], timestamp: Time.at(Time.now.to_i)).returns(nil).once
        UserTopicLevelMetric.expects(:update_from_increments).with(opts: {user_id: video_stream.creator_id, tag_id: topic.id}, increments: [[:smartbites_score, 20]], timestamp: Time.at(Time.now.to_i)).returns(nil).once

        # consumed for actionable event 'comment'
        UserTopicLevelMetric.expects(:update_from_increments).with(opts: {user_id: user.id, tag_id: topic.id}, increments: [[:smartbites_score, 0], [:smartbites_consumed, 1]], timestamp: Time.at(Time.now.to_i)).returns(nil).once
        UserTopicLevelMetric.expects(:update_from_increments).with(opts: {user_id: video_stream.creator_id, tag_id: topic.id}, increments: [[:smartbites_score, 0]], timestamp: Time.at(Time.now.to_i)).returns(nil).once
      end
      mr = MetricRecord.new(action: "comment", object_id: video_stream.id, object_type: "video_stream", actor_id: user.id, owner_id: video_stream.creator_id).save
    end
  end
end

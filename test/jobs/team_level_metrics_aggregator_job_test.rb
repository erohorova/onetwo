require 'test_helper'
class TeamLevelMetricsAggregatorJobTest < ActiveSupport::TestCase

  setup do
    WebMock.disable!
  end

  teardown do
    WebMock.enable!
  end

  test 'all metrics increment' do
    TeamLevelMetricsAggregatorJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    Organization.any_instance.unstub :create_org_level_team
    org = create(:organization)
    org.run_callbacks(:commit)
    user = create(:user, organization: org)
    org.everyone_team.members << user

    teams = create_list(:team, 3, organization: org)
    teams.first(2).each {|team| team.add_user(user)}

    video_stream = create(:wowza_video_stream, creator: create(:user, organization: org))
    user.reload

    Timecop.freeze do
      teams.first(2).map(&:id).each do |team_id|
        TeamLevelMetric.expects(:update_from_increments).with(opts: {team_id: team_id}, increments: [[:smartbites_comments_count, 1]], timestamp: Time.at(Time.now.to_i)).returns(nil).once

        # consumed for actionable event 'comment'
        TeamLevelMetric.expects(:update_from_increments).with(opts: {team_id: team_id}, increments: [[:smartbites_consumed, 1]], timestamp: Time.at(Time.now.to_i)).returns(nil).once
      end
      mr = MetricRecord.new(action: "comment", object_id: video_stream.id, object_type: "video_stream", actor_id: user.id, owner_id: video_stream.creator_id).save
    end
  end

end

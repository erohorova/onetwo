require 'test_helper'
class UserContentLevelMetricsAggregatorJobTest < ActiveSupport::TestCase

  setup do
    WebMock.disable!
  end

  teardown do
    WebMock.enable!
  end

  test 'should call increment with right arguments' do
    UserContentLevelMetricsAggregatorJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    org = create(:organization)
    user = create(:user, organization: org)
    video_stream = create(:wowza_video_stream, creator: create(:user, organization: org))

    Timecop.freeze do
      UserContentLevelMetric.expects(:increment_smartbites_score).with({user_id: user.id, content_id: video_stream.id, content_type: 'video_stream'}, 10, Time.at(Time.now.to_i)).returns(nil).once
      UserContentLevelMetric.expects(:increment_smartbites_score).with({user_id: video_stream.creator_id, content_id: video_stream.id, content_type: 'video_stream'}, 20, Time.at(Time.now.to_i)).returns(nil).once

      # consumed for actionable event 'comment'
      UserContentLevelMetric.expects(:increment_smartbites_score).with({user_id: user.id, content_id: video_stream.id, content_type: 'video_stream'}, 0, Time.at(Time.now.to_i)).returns(nil).once
      UserContentLevelMetric.expects(:increment_smartbites_score).with({user_id: video_stream.creator_id, content_id: video_stream.id, content_type: 'video_stream'}, 0, Time.at(Time.now.to_i)).returns(nil).once
      mr = MetricRecord.new(action: "comment", object_id: video_stream.id, object_type: "video_stream", actor_id: user.id, owner_id: video_stream.creator_id).save
    end
  end
end

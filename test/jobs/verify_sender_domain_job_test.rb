require 'test_helper'

class VerifySenderDomainJobTest < ActiveJob::TestCase
  setup do
    @org = create(:organization)
    @mailer_config = create(:mailer_config, from_name: 'test', from_address: 'test@edcast.com', 
      domain: 'edcast.com', from_admin: true, organization: @org)
  end

  test 'should request mandrill API for domain authentication and save verification response' do
    stub_response = {
      "domain"=>"edcast.com", "created_at"=>"2018-04-18 07:13:00", "last_tested_at"=>"2018-10-03 06:18:40",
      "spf"=>{"valid"=>true, "valid_after"=>nil, "error"=>nil}, "dkim"=>{"valid"=>true, "valid_after"=>nil, "error"=>nil}, 
      "verified_at"=>"2016-03-25 09:47:58", "valid_signing"=>true
    }

    service = mock()
    Mandrill::API.any_instance.stubs(:senders).returns(service)
    service.expects(:check_domain).returns(stub_response).once

    VerifySenderDomainJob.perform_now({config_id: @mailer_config.id})
    assert @mailer_config.reload.verification[:domain]
  end

  test 'should not save domain verified in database if mandrill valid signing for domain is false' do
    stub_response = {
      "domain"=>"edcast.com", "created_at"=>"2018-04-18 07:13:00", "last_tested_at"=>"2018-10-03 06:18:40",
      "spf"=>{"valid"=>false, "valid_after"=>nil, "error"=>nil}, "dkim"=>{"valid"=>false, "valid_after"=>nil, "error"=>nil}, 
      "verified_at"=>nil, "valid_signing"=>false
    }

    service = mock()
    Mandrill::API.any_instance.stubs(:senders).returns(service)
    service.expects(:check_domain).returns(stub_response).once

    VerifySenderDomainJob.perform_now({config_id: @mailer_config.id})
    refute @mailer_config.reload.verification[:domain]
  end
end
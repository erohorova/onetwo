require 'test_helper'

class WorkflowsV2JobTest < ActiveJob::TestCase
  setup do
    User.any_instance.stubs(:detect_upsert)
    Profile.any_instance.stubs(:detect_upsert)
    @org = create(:organization)
    @user1, @user2, @user3 = create_list(:user, 3, organization: @org)
    [@user1, @user2, @user3].each do |user|
      Profile.create(
        user_id: user.id,
        organization_id: @org.id,
        external_id: "#{user.email + user.id.to_s}",
        uid: "#{user.email + user.id.to_s}"
      )
    end
    @prepared_group_patches = [
      {
        "patchId"=>"d30ad521-ee35-11e8-8bb5-9564013b0b8d",
        "patchType"=>"insert",
        "data"=>{
          "id"=>"England",
          "group_type"=>1,
          "mandatory_group"=>1,
          "group_description"=>"description"
        },
        "meta"=>{
          "historyRevisionVersion"=>1,
          "historyRevisionDatetime"=>1542877557000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>1,
          "outputSourceId"=>1
        }
      },
      {
        "patchId"=>"d30afc38-ee35-11e8-8bb5-9564013b0b8d",
        "patchType"=>"insert",
        "data"=>{
          "id"=>"England",
          "group_type"=>1,
          "mandatory_group"=>1,
          "group_description"=>"description"
        },
        "meta"=>{
          "historyRevisionVersion"=>1,
          "historyRevisionDatetime"=>1542877557000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"groups",
        "orgName"=>@org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>1,
          "outputSourceId"=>1
        }
      },
      {
        "patchId"=>"d40afc3b-ee35-11e8-8bb5-9564013b0b8d",
        "patchType"=>"insert",
        "data"=>{
          "id"=>"Russia",
          "group_type"=>1,
          "mandatory_group"=>1,
          "group_description"=>"description"
        },
        "meta"=>{
          "historyRevisionVersion"=>1,
          "historyRevisionDatetime"=>1542877557000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>1,
          "outputSourceId"=>1
        }
      },
      {
        "patchId"=>"d30ad521-ee35-3214-8bb5-9564013b0b8d",
        "patchType"=>"update",
        "data"=>{
          "id"=>"England",
          "group_type"=>0,
          "mandatory_group"=>0,
          "group_description"=>"description"
        },
        "meta"=>{
          "historyRevisionVersion"=>2,
          "historyRevisionDatetime"=>1542877557000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>1,
          "outputSourceId"=>1
        }
      },
      {
        "patchId"=>"1ef96440-ee38-11e8-8bb5-9564013b0b8d",
        "patchType"=>"delete",
        "data"=>{
          "id"=>"England",
          "group_type"=>1,
          "mandatory_group"=>1,
          "group_description"=>"description"
        },
        "meta"=>{
          "historyRevisionVersion"=>3,
          "historyRevisionDatetime"=>1542878544000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>1,
          "outputSourceId"=>1
        }
      },
      {
        "patchId"=>"1ef98b57-ee38-11e8-8bb5-9564013b0b8d",
        "patchType"=>"delete",
        "data"=>{
          "id"=>"England",
          "group_type"=>1,
          "mandatory_group"=>1,
          "group_description"=>"description"
        },
        "meta"=>{
          "historyRevisionVersion"=>2,
          "historyRevisionDatetime"=>1542878544000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>1,
          "outputSourceId"=>1
        }
      },
      {
        "patchId"=>"1ef98b5a-ee38-11e8-8bb6-9564013b0b8d",
        "patchType"=>"delete",
        "data"=>{
          "id"=>"Russia",
          "group_type"=>1,
          "mandatory_group"=>1,
          "group_description"=>"description"
        },
        "meta"=>{
          "historyRevisionVersion"=>3,
          "historyRevisionDatetime"=>1542878544000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>1,
          "outputSourceId"=>1
        }
      }
    ]
    @prepared_users_groups_patches = [
      {
        "patchId"=>"5fb61d70-ee38-11e8-8bb5-9564013b0b8d",
        "patchType"=>"insert",
        "data"=>{
          "id"=>"dynamic_group_by_city",
          "group_type"=>1,
          "mandatory_group"=>1,
          "group_description"=>"description"
        },
        "meta"=>{
          "historyRevisionVersion"=>1,
          "historyRevisionDatetime"=>1542878652000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>593,
          "outputSourceId"=>692
        }
      },
      {
        "patchId"=>"61018930-ee38-11e8-8bb5-9564013b0b8d",
        "patchType"=>"insert",
        "data"=>{
          "id_type"=>"email",
          "user_id"=>@user1.email,
          "group_id"=>"dynamic_group_by_city",
          "member_type"=>"some_constant_xyz"
        },
        "meta"=>{
          "historyRevisionVersion"=>1,
          "historyRevisionDatetime"=>1542878654000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"users_groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>681,
          "outputSourceId"=>660
        }
      },
      {
        "patchId"=>"61018931-ee51-11e8-8bb5-9564013b0b8d",
        "patchType"=>"insert",
        "data"=>{
          "id_type"=>"uid",
          "user_id"=>@user2.external_profile.uid,
          "group_id"=>"dynamic_group_by_city",
          "member_type"=>"leader"
        },
        "meta"=>{
          "historyRevisionVersion"=>1,
          "historyRevisionDatetime"=>1542878654000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"users_groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>681,
          "outputSourceId"=>660
        }
      },
      {
        "patchId"=>"61018931-ee38-1348-8bb5-9564013b0b8d",
        "patchType"=>"insert",
        "data"=>{
          "user_id"=>@user3.id.to_s,
          "group_id"=>"dynamic_group_by_city",
          "member_type"=>"admin"
        },
        "meta"=>{
          "historyRevisionVersion"=>1,
          "historyRevisionDatetime"=>1542878654000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"users_groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>681,
          "outputSourceId"=>660
        }
      },
      {
        "patchId"=>"61018930-1238-11e8-8b45-9564013b0b8d",
        "patchType"=>"update",
        "data"=>{
          "id_type"=>"email",
          "user_id"=>@user1.email,
          "group_id"=>"dynamic_group_by_city",
          "member_type"=>"leader"
        },
        "meta"=>{
          "historyRevisionVersion"=>2,
          "historyRevisionDatetime"=>1542878654000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"users_groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>681,
          "outputSourceId"=>660
        }
      },
      {
        "patchId"=>"c4eee550-ee38-11e8-8bb5-9564013b0b8d",
        "patchType"=>"delete",
        "data"=>{
          "id_type"=>"email",
          "user_id"=>@user1.email,
          "group_id"=>"dynamic_group_by_city",
          "member_type"=>"some_constant_xyz"
        },
        "meta"=>{
          "historyRevisionVersion"=>3,
          "historyRevisionDatetime"=>1542878822000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"users_groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>681,
          "outputSourceId"=>660
        }
      },
      {
        "patchId"=>"c4eee551-ee38-11e8-8bb5-9564013b0b8d",
        "patchType"=>"delete",
        "data"=>{
          "user_id"=>@user2.id.to_s,
          "group_id"=>"dynamic_group_by_city",
          "member_type"=>"leader"
        },
        "meta"=>{
          "historyRevisionVersion"=>2,
          "historyRevisionDatetime"=>1542878822000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"users_groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>681,
          "outputSourceId"=>660
        }
      },
      {
        "patchId"=>"c4eee551-e124-11e8-8bb5-9564013b0b8d",
        "patchType"=>"delete",
        "data"=>{
          "user_id"=>@user3.id.to_s,
          "group_id"=>"dynamic_group_by_city",
          "member_type"=>"admin"
        },
        "meta"=>{
          "historyRevisionVersion"=>2,
          "historyRevisionDatetime"=>1542878822000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"users_groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>681,
          "outputSourceId"=>660
        }
      },
      {
        "patchId"=>"c69d58a0-ee38-11e8-8bb5-9564013b0b8d",
        "patchType"=>"delete",
        "data"=>{
          "id"=>"dynamic_group_by_city",
          "group_type"=>1,
          "mandatory_type_type"=>1,
          "group_description"=>"Group created using Workflow"
        },
        "meta"=>{
          "historyRevisionVersion"=>2,
          "historyRevisionDatetime"=>1542878825000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>593,
          "outputSourceId"=>692
        }
      }
    ]
  end

  class GroupsPatchesV2Test < WorkflowsV2JobTest
    setup do
      # prepare all needed WorkflowPatchRevisions
      WorkflowPatchRevisionsJob.perform_now(
        patches: @prepared_group_patches,
        version: 2
      )
    end

    # groups insert/delete/update
    test 'groups processing workflow v2' do
      TestAfterCommit.enabled = true
      insert_patches = WorkflowPatchRevision.where(patch_type: 'insert')
      insert_patches.each do |patch|
        WorkflowsV2Job.perform_now(patch_ids: [patch.id])
      end
      expected_team1 = Team.find_by(name: 'England')
      expected_team2 = Team.find_by(name: 'Russia')
      dynamic_group_ids = @org.dynamic_group_revisions.pluck(:group_id)

      #insert assertions
      assert expected_team1
      assert expected_team1.is_mandatory
      assert expected_team1.is_private
      assert expected_team1.is_dynamic
      assert expected_team2
      assert expected_team2.is_mandatory
      assert expected_team2.is_private
      assert expected_team2.is_dynamic
      assert_same_elements [expected_team1.id, expected_team2.id], dynamic_group_ids

      update_patch = WorkflowPatchRevision.find_by(patch_type: 'update')
      WorkflowsV2Job.perform_now(patch_ids: [update_patch.id])

      expected_team1.reload
      #update assertions
      refute expected_team1.is_mandatory
      refute expected_team1.is_private


      delete_patches = WorkflowPatchRevision.where(patch_type: 'delete')
      delete_patches.each do |patch|
        WorkflowsV2Job.perform_now(patch_ids: [patch.id])
      end

      expected_team1.reload
      expected_team2.reload

      #deleted assertions
      assert expected_team1
      assert expected_team2
      refute expected_team1.is_private
      assert expected_team2.is_private

      refute expected_team1.is_dynamic
      refute expected_team2.is_dynamic
    end

    test 'groups insert should create dynamic in case group name is exists for non dynamic group' do
      create(:team, organization_id: @org.id, name: 'England')
      create(:team, organization_id: @org.id, name: 'England1')
      insert_patch = WorkflowPatchRevision.where(patch_type: 'insert').first
      WorkflowsV2Job.perform_now(patch_ids: [insert_patch.id])

      assert @org.teams.exists?(name: 'England2', is_dynamic: true)
      assert @org.dynamic_group_revisions.exists?(uid: 'England')
    end

    test 'insert patch should handle group flags private/mandatory' do
      insert_patch = WorkflowPatchRevision.where(patch_type: 'insert').first
      new_data = {
        id: 'England',
        group_type: 0, # private option
        mandatory_group: 1, # mandatory option
      }
      insert_patch.update_attributes(data: new_data)
      WorkflowsV2Job.perform_now(patch_ids: [insert_patch.id])

      assert @org.teams.exists?(
        name: 'England',
        is_dynamic: true,
        is_private: false,
        is_mandatory: true
      )
    end

    test 'insert patch should handle group flags private/mandatory default values' do
      insert_patch = WorkflowPatchRevision.where(patch_type: 'insert').first
      new_data = {
        id: 'England'
      }
      insert_patch.update_attributes(data: new_data)
      WorkflowsV2Job.perform_now(patch_ids: [insert_patch.id])

      assert @org.teams.exists?(
        name: 'England',
        is_dynamic: true,
        is_private: true,
        is_mandatory: true
      )
    end
  end

  class UsersGroupsPatchesV2Test < WorkflowsV2JobTest

    setup do
      # prepare all needed WorkflowPatchRevisions
      WorkflowPatchRevisionsJob.perform_now(
        patches: @prepared_users_groups_patches,
        version: 2
      )
    end

    # users_groups insert/delete/update
    test 'should properly process users_groups patches' do
      TestAfterCommit.enabled = true
      insert_patches = WorkflowPatchRevision.where(patch_type: 'insert')
      insert_patches.each do |patch|
        WorkflowsV2Job.perform_now(patch_ids: [patch.id])
      end

      expected_team_users = [@user1.id, @user2.id, @user3.id]
      dynamic_group_revision = DynamicGroupRevision.find_by(
        title: insert_patches.first.data['id']
      )
      expected_team = dynamic_group_revision.dynamic_group

      #insert assertions
      assert expected_team
      assert dynamic_group_revision
      assert_same_elements expected_team_users, expected_team.teams_users.pluck(:user_id)
      assert_equal 'sub_admin', expected_team.teams_users.find_by(user_id: @user3.id).as_type
      assert_equal 'admin', expected_team.teams_users.find_by(user_id: @user2.id).as_type
      assert_equal 'member', expected_team.teams_users.find_by(user_id: @user1.id).as_type
      assert expected_team.is_dynamic
      assert expected_team.is_private
      assert expected_team.is_mandatory

      update_patch = WorkflowPatchRevision.where(patch_type: 'update').first
      WorkflowsV2Job.perform_now(patch_ids: [update_patch.id])

      #update assertions
      assert_equal 'admin', expected_team.teams_users.find_by(user_id: @user1.id).as_type

      delete_patches = WorkflowPatchRevision.where(patch_type: 'delete')
      delete_patches.each do |patch|
        WorkflowsV2Job.perform_now(patch_ids: [patch.id])
      end

      #delete assertions
      assert Team.exists?(name: 'dynamic_group_by_city', is_dynamic: false)
      refute DynamicGroupRevision.exists?(uid: 'dynamic_group_by_city')
    end
  end
end

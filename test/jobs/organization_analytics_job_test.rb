require 'test_helper'

class OrganizationAnalyticsJobTest < ActiveJob::TestCase
  test "should invoke daily org analytics service" do
    organization = create(:organization)
    Organization.any_instance.expects(:send_org_analytics).once

    OrganizationAnalyticsJob.perform_now(organization.id)
  end
end

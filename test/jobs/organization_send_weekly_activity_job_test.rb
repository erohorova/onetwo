require 'test_helper'

class OrganizationSendWeeklyActivityJobTest < ActiveJob::TestCase

  test 'calls send weekly activity method on the org' do
    org = create(:organization)
    Organization.any_instance.expects(:send_weekly_activity_email)

    OrganizationSendWeeklyActivityJob.perform_now(org.id)
  end
end

require 'test_helper'

class NewTeamUserAutofollowJobTest < ActiveSupport::TestCase
  setup do
    WebMock.disable!
    @org = create(:organization)
    @user = create(:user, organization: @org)
    @channel = create(:channel, organization: @org)
    # @teams = create_list(:team, 2, organization: @org)
  end

  teardown do
    WebMock.enable!
  end

  test 'it sshould run channel autofollow job inline for after followed methods' do
    TeamsChannelsAutofollowJob.any_instance.expects(:after_followed).once
    NewTeamUserAutoFollowJob.perform_now(@channel.id, @user.id)
  end
end
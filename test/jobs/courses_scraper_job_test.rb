require 'test_helper'

class CoursesScraperJobTest < ActiveJob::TestCase
  test 'should invoke scraper for course providers' do
    user = create(:user)
    external_uid = "abc#{user.id}abc"

    # courseera
    Integrations::CourseraService.any_instance.expects(:scrape).once.returns(true)
    CoursesScraperJob.new.perform(user.id, external_uid, "Coursera")

    # Skill share
    Integrations::SkillShareService.any_instance.expects(:scrape).once.returns(true)
    CoursesScraperJob.new.perform(user.id, external_uid, "Skill Share")

    # Future Learn
    Integrations::FutureLearnService.any_instance.expects(:scrape).once.returns(true)
    CoursesScraperJob.new.perform(user.id, external_uid, "Future Learn")

    # Plural sight
    Integrations::PluralSightService.any_instance.expects(:scrape).once.returns(true)
    CoursesScraperJob.new.perform(user.id, external_uid, "Plural Sight")

    # Tree House
    Integrations::TreeHouseService.any_instance.expects(:scrape).once.returns(true)
    CoursesScraperJob.new.perform(user.id, external_uid, "Tree House")

    # Code School
    Integrations::CodeSchoolService.any_instance.expects(:scrape).once.returns(true)
    CoursesScraperJob.new.perform(user.id, external_uid, "Code School")

    # Alison
    Integrations::AlisonService.any_instance.expects(:scrape).once.returns(true)
    CoursesScraperJob.new.perform(user.id, external_uid, "Alison")

    # Udemy
    Integrations::UdemyService.any_instance.expects(:scrape).once.returns(true)
    CoursesScraperJob.new.perform(user.id, external_uid, "Udemy")

    # Codecademy
    Integrations::CodecademyService.any_instance.expects(:scrape).once.returns(true)
    CoursesScraperJob.new.perform(user.id, external_uid, "Codecademy")
  end
end

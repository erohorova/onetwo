require 'test_helper'
class UpdatePercentileForOrgJobTest < ActiveSupport::TestCase

  test 'should update percentile' do
    org1, org2 = create_list(:organization, 2)
    users = create_list(:user, 5, organization: org1)

    users[0..2].each_with_index do |user, indx|
      # all time
      create(:user_level_metric, smartbites_score: indx+1, user_id: user.id, period: :all_time, offset: 0, time_spent: 20)
      # # day 0
      create(:user_level_metric, smartbites_score: 10 - indx, user_id: user.id, period: :day, offset: 0, time_spent: 20)
    end

    # Create one more user, with the same time spent
    top_score = UserLevelMetric.where(period: :all_time, offset: 0, organization_id: org1.id).map(&:smartbites_score).max
    new_user = create(:user, organization: org1)
    create(:user_level_metric, user_id: new_user.id, period: :all_time, offset: 0, smartbites_score: top_score, time_spent: 20)

    UpdatePercentileForOrgJob.perform_now(org1.id, {"all_time" => 0})

    # For all time, new user should be 100%, third user should be 100%, second user should be 4/6%, third user should be 3/6, fourth and fifth at 2/6 (both at 0)
    assert_equal 100, UserLevelMetric.get_percentile(user_id: new_user.id, period: :all_time, offset: 0)
    assert_equal 100, UserLevelMetric.get_percentile(user_id: users[2].id, period: :all_time, offset: 0)

    assert_equal 66, UserLevelMetric.get_percentile(user_id: users[1].id, period: :all_time, offset: 0)
    assert_equal 50, UserLevelMetric.get_percentile(user_id: users[0].id, period: :all_time, offset: 0)
    assert_equal 33, UserLevelMetric.get_percentile(user_id: users[3].id, period: :all_time, offset: 0)
    assert_equal 33, UserLevelMetric.get_percentile(user_id: users[4].id, period: :all_time, offset: 0)
  end
end

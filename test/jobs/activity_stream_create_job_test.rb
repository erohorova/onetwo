require 'test_helper'

class ActivityStreamCreateJobTest < ActiveJob::TestCase
  test 'should create a activity stream' do
    card = create(:card)
    assert_difference -> {ActivityStream.count} do
      args = {
        streamable_id: card.id,
        streamable_type: card.class.name,
        user_id: card.author.id,
        action: ActivityStream::ACTION_CREATED_CARD
      }
      ActivityStreamCreateJob.perform_now(args)
    end
  end
end

require 'test_helper'

class DailyLearningForUserJobTest < ActiveSupport::TestCase

  setup do
    skip
  end

  def random_date()
    @counter ||= 0
    (Time.now.utc + @counter.days).to_date.to_s
    @counter += 1
  end

  test 'should trigger process' do
    user = create(:user)
    udl = create(:user_daily_learning, user: user, date: random_date())

    UserDailyLearning.any_instance.expects(:process!).once

    DailyLearningForUserJob.new.perform(udl.id)
  end

  test 'should do nothing and log warning if executed when item is in any state other than initialized' do
    user = create(:user)

    (DailyLearningProcessingStates::ALL_STATES - [DailyLearningProcessingStates::INITIALIZED]).each do |processing_state|
      udl = create(:user_daily_learning, processing_state: processing_state, user: user, date: random_date())

      Rails.logger.expects(:warn).once
      udl.expects(:process!).never

      DailyLearningForUserJob.new.perform(udl.id)
    end
  end
end

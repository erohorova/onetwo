require 'test_helper'

class SyncCloneAssociationsJobTest < ActiveSupport::TestCase
  setup do
    TestAfterCommit.enabled = true
    @org1, @org2 = create_list(:organization, 2)
    @user = create(:user, organization: @org1)
    @org1_channel = create(:channel, organization: @org1, shareable: true)
    @org2_channel = create(:channel, organization: @org2, parent_id: @org1_channel.id)
  end

  test "should trigger sync clone associations job on updating parent card which has clone" do
    card = create(:card, card_type: 'media', card_subtype: 'text', author_id: @user.id, title: 'This is card', 
      message: 'This is message', organization: @org1, ecl_source_name: 'UGC')
    card.current_user = @user
    card.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)

    SyncCloneAssociationsJob.expects(:perform_later).with({parent_card_id: card.id, sync_association_type: 'Card'})
    card.update(title: 'Updated title of card')
  end

  test "should not trigger sync clone associations job on not updating cloned card" do
    card = create(:card, card_type: 'media', card_subtype: 'text', author_id: @user.id, title: 'This is card', 
      message: 'This is message', organization: @org1, ecl_source_name: 'UGC')

    SyncCloneAssociationsJob.expects(:perform_later).never
    card.update(title: 'Updated title of card')
  end

  test "should trigger sync clone associations job on updating parent card price which has clone" do
    card = create(:card, card_type: 'media', card_subtype: 'text', author_id: @user.id, title: 'This is card', 
      message: 'This is message', organization: @org1, ecl_source_name: 'UGC')
    price = create :price, card: card, currency: 'USD', amount: 100
    card.current_user = @user
    card.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)

    SyncCloneAssociationsJob.expects(:perform_later).with({parent_card_id: card.id, sync_association_type: 'Price'})
    price.update(amount: 1000)
  end

  test "should trigger sync clone associations job on updating parent card metadata which has clone" do
    card = create(:card, card_type: 'media', card_subtype: 'text', author_id: @user.id, title: 'This is card', 
      message: 'This is message', organization: @org1, ecl_source_name: 'UGC')
    card_metadata = create :card_metadatum, card: card, plan: 'free', level: 'beginner', average_rating: 5
    card.current_user = @user
    card.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)

    SyncCloneAssociationsJob.expects(:perform_later).with({parent_card_id: card.id, sync_association_type: 'CardMetadatum'})
    card_metadata.update(average_rating: 4)
  end

  test "should trigger sync clone associations job on updating parent card resource which has clone" do
    resource = create(:resource)
    card = create(:card, card_type: 'media', card_subtype: 'text', author_id: @user.id, title: 'This is card', 
      resource: resource, message: 'This is message', organization: @org1, ecl_source_name: 'UGC')
    card_metadata = create :card_metadatum, card: card, plan: 'free', level: 'beginner', average_rating: 5
    card.current_user = @user
    card.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)

    SyncCloneAssociationsJob.expects(:perform_later).with({parent_card_id: card.id, sync_association_type: 'Card'})
    new_resource = create(:resource)
    card.resource = new_resource
    card.save
  end

  test "should trigger sync clone associations job on updating parent pathway which has clone" do
    cover = create(:card, ecl_source_name: 'UGC', card_type: 'pack', author_id: @user.id, title: 'pack cover')
    cards = create_list(:card, 2, author_id: @user.id, card_type: 'media', ecl_source_name: 'UGC')
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end
    cover.current_user = @user
    cover.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)

    SyncCloneAssociationsJob.expects(:perform_later).with({parent_card_id: cover.id, sync_association_type: 'CardPackRelation'})
    new_card = create(:card, author_id: @user.id, card_type: 'media', ecl_source_name: 'UGC')
    CardPackRelation.add(cover_id: cover.id, add_id: new_card.id, add_type: 'Card')
  end

  test "should trigger sync clone associations job on updating parent journey which has clone" do
    cover_journey = create(:card, card_type: 'journey', ecl_source_name: 'UGC', card_subtype: 'self_paced', author: @user, organization: @org)
    section = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user, organization: @org)
    cards = create_list(:card, 2, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: section.id, add_id: card.id, add_type: 'Card')
    end
    JourneyPackRelation.add(cover_id: cover_journey.id, add_id: section.id)
    cover_journey.current_user = @user
    cover_journey.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)

    SyncCloneAssociationsJob.expects(:perform_later).with({parent_card_id: section.id, sync_association_type: 'CardPackRelation'})
    new_card = create(:card, author_id: @user.id, card_type: 'media', ecl_source_name: 'UGC')
    CardPackRelation.add(cover_id: section.id, add_id: new_card.id, add_type: 'Card')
  end

  test "should not sync clone card if parent card removed from parent channel" do
    card = create(:card, card_type: 'media', card_subtype: 'text', author_id: @user.id, title: 'This is card', 
      message: 'This is message', organization: @org1, ecl_source_name: 'UGC')
    card_metadata = create :card_metadatum, card: card, plan: 'free', level: 'beginner', average_rating: 5
    card.current_user = @user
    card.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)

    clone_card = card.cloned_cards.first
    ChannelsCard.where(card_id: card.id, channel_id: @org1_channel.id).first.destroy
    SyncCloneChannelCardsJob.perform_now({parent_card_id: card.id, parent_channel_id: @org1_channel.id, action: 'destroy'})
    card_metadata.update(average_rating: 4)

    SyncCloneAssociationsJob.perform_now({parent_card_id: card.id, sync_association_type: 'CardMetadatum'})

    assert_equal 4, card.card_metadatum.average_rating
    assert_equal 5, clone_card.card_metadatum.average_rating
  end
end
require 'test_helper'
class EclCardTrashingJobTest < ActiveSupport::TestCase
  test 'should call to trash card from ecl' do
    EclApi::CardConnector.any_instance.expects(:trash_card_from_ecl).once
    EclCardTrashingJob.perform_now('test-ecl-id', 15)
  end
end

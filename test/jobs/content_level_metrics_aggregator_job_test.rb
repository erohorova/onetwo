require 'test_helper'
class ContentLevelMetricsAggregatorJobTest < ActiveSupport::TestCase

  setup do
    WebMock.disable!
    @org = create(:organization)
    @user = create(:user, organization: @org)
    @card = create(:card, organization: @org, author: @user)
  end

  teardown do
    WebMock.enable!
  end

  test 'metrics increment' do
    ContentLevelMetricsAggregatorJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    video_stream = create(:wowza_video_stream, creator: create(:user, organization: @org))

    Timecop.freeze do
      ContentLevelMetric.expects(:update_from_increments)
          .with(opts: {content_id: video_stream.id, content_type: "video_stream"},
                increments: [[:comments_count, 1]],
                timestamp: Time.at(Time.now.to_i)).returns(nil).once
      # consumed for actionable event 'comment'
      ContentLevelMetric.expects(:update_from_increments)
          .with(opts: {content_id: video_stream.id, content_type: "video_stream"},
                increments: [[:views_count, 1]],
                timestamp: Time.at(Time.now.to_i)).returns(nil).once
      MetricRecord.new(action: "comment", object_id: video_stream.id,
                       object_type: "video_stream", actor_id: @user.id,
                       owner_id: video_stream.creator_id).save
    end
  end

  test 'should decrease comment count when comment was deleted' do
    ContentLevelMetricsAggregatorJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    Timecop.freeze do
      ContentLevelMetric.expects(:update_from_increments)
          .with(opts: {content_id: @card.id, content_type: "card"},
                increments: [[:comments_count, -1]],
                timestamp: Time.at(Time.now.to_i)).returns(nil).once
      MetricRecord.new(actor: @user, object_type: "card", object_id: @card.id, action: "uncomment").save
    end
  end

  test 'should decrease likes count when like was deleted' do
    ContentLevelMetricsAggregatorJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    Timecop.freeze do
      ContentLevelMetric.expects(:update_from_increments)
          .with(opts: {content_id: @card.id, content_type: "card"},
                increments: [[:likes_count, -1]],
                timestamp: Time.at(Time.now.to_i)).returns(nil).once
      MetricRecord.new(actor: @user, object_type: "card", object_id: @card.id, action: "unlike").save
    end
  end
end

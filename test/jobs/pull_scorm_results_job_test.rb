require 'test_helper'

class PullScormResultsJobTest < ActiveJob::TestCase
  test 'should run scorm_service_completion' do
    ScormCompletionService.any_instance.expects(:run).once
     RecurringJobs::PullScormResultsJob.perform_now
   end
end
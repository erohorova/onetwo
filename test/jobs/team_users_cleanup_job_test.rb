require 'test_helper'

class TeamUsersCleanupJobTest < ActiveJob::TestCase

  test "should remove team users in the background" do
    org = create(:organization)
    team = create(:team, organization: org)
    users = create_list(:user, 3, organization: org)

    users.each { |user| team.add_member(user)}
    assert_difference ->{TeamsUser.count}, -3 do
      team_poro = Analytics::MetricsRecorder.team_attributes(team)
      TeamUsersCleanupJob.new.perform(team: team_poro)
    end
  end
end

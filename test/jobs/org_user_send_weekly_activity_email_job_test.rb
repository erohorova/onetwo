require 'test_helper'

class OrgUserSendWeeklyActivityEmailJobTest < ActiveJob::TestCase

  test "fetches information from the service and sends it to the user" do
    skip #deprecated
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(false)
    org = create(:organization)
    user = create(:user, organization: org)
    service = mock('OrganizationWeeklyActivityService')
    to_email = ''
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
        to_email = message[:message][:to].first[:email]
    }
    OrganizationWeeklyActivityService.expects(:new).with(org).returns(service)
    service.expects(:activity).returns({insights:[], pathways:[], video_streams:[]})
    OrgUserSendWeeklyActivityEmailJob.perform_now(org.id, user.id)
    assert_equal user.mailable_email, to_email
  end
end

require 'test_helper'

class CardDeletionJobTest < ActiveJob::TestCase
  include CardHelperMethods

  # self.use_transactional_fixtures = false
  test 'when a card is deleted it should delete that card from all pathways' do
    CardDeletionJob.unstub :perform_laer
    EdcastActiveJob.unstub :perform_laer
    VideoStream.stubs :reindex

    setup_card_packs
    cards = @cards[@pack_cover]
    cards[2].destroy
    CardDeletionJob.perform_now(cards[2].id)
    assert_nil CardPackRelation.where(cover_id: @pack_cover.id, from_id: cards[2].id).first
  end
end

require 'test_helper'
class OrgAdminReminderNotificationJobTest < ActiveJob::TestCase
  test "send_reminder_email is called" do
    org = create(:organization, created_at: 2.days.ago)
    admin = create(:user, organization_id: org.id, organization_role: 'admin')
    OrganizationMailer.expects(:reminder_mail_to_add_users).with(org, admin).once
    OrgAdminReminderNotificationJob.perform_now(org, admin)
  end
end

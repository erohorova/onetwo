require 'test_helper'

class UploadToScormJobTest < ActiveJob::TestCase
  test 'should run scorm_upload_service' do
    organization = create(:organization)
    user = create(:user, organization: organization)

    stub_request(:get, "https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv").
      to_return(:status => 200, :body => "", :headers => {})

    scorm_card = create(:card, author: user, organization: organization,
                    filestack: [scorm_course: true,
                    url: 'https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv'])


    ScormUploadService.any_instance.expects(:run).once
     UploadToScormJob.new.perform(scorm_card.id, 'somecourseid')
   end
end
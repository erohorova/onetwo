require 'test_helper'

class WorkflowPatchRevisionsJobTest < ActiveJob::TestCase

  setup do
    @org = create(:organization)
    @user = create(:user, organization: @org)
  end

  test 'should perform job and create patch revision' do
    # version 0
    patches = [
      {
        patchId: '8c6a04f0-b821-11e8-87cd-e54aaae67708',
        patchType: 'insert',
        data: {
          id: @user.id.to_s, # should be eq to users id
          group_name: 'patch_test',
          group_type: 'group_type', # unsupported in version 0, strict rules -> mandatory, private, dynamic
          group_description: 'patch_test' # unsupported in version 0, strict rules ()
        },
        meta: {
          historyRevisionVersion: 1,
          historyRevisionDatetime: 1536931486000,
          historyRevisionDataJson: {}
        },
        workflowType: 'groups',
        orgName: @org.host_name, # in next version will be additionally validated in scope of requested host_name
        params: {},
        models: { # in scope with data['id'] = `wuid` (workflow uniq id)
          workflowId: 1,
          outputSourceId: 1
        }
      }.with_indifferent_access,
      {
        patchId: '8c6a04f0-b821-11e8-87cd-e54aaae67708',
        patchType: 'insert',
        data: {
         id: @user.id.to_s,
         group_id: '8c6a04f0-b821-11e8-87cd',
         member_type: 'some_constant_xyz' # not supported in version 0
        },
        meta: {
          historyRevisionVersion: 1,
          historyRevisionDatetime: 1536931486000,
          historyRevisionDataJson: {}
        },
        workflowType: 'users_groups',
        orgName: @org.host_name,
        params: {},
        models: {
          workflowId: 1,
          outputSourceId: 1
        }
      }.with_indifferent_access
    ]
    assert_difference -> { WorkflowPatchRevision.count }, 2 do
      WorkflowPatchRevisionsJob.perform_now(patches: patches)
    end
    wpr = WorkflowPatchRevision.first

    expected_processing_info = { 'info' => [], 'errors' => [] }
    assert_equal 0, wpr.version
    assert_equal expected_processing_info, wpr.processing_info
    assert_equal @org.id, wpr.organization_id
    assert_equal '8c6a04f0-b821-11e8-87cd-e54aaae67708', wpr.patch_id
    assert_equal 'groups', wpr.workflow_type
    assert_equal 1, wpr.workflow_id
    assert_equal 1, wpr.output_source_id
    refute wpr.completed
    refute wpr.failed
    refute wpr.in_progress
    refute wpr.job_id
  end

  test 'should perform job create patch and add validation description' do
    # version 0, workflow_type -> `groups`
    patches = [
      {
        patchId: '8c6a04f0-b821-11e8-87cd-e54aaae67708',
        patchType: 'insert',
        data: {
          id: @user.id.to_s,
          group_name: nil, # blank group name
          group_type: 'group_type',
          group_description: 'patch_test'
        },
        meta: {
          historyRevisionVersion: 1,
          historyRevisionDatetime: 1536931486000,
          historyRevisionDataJson: {}
        },
        workflowType: 'groups',
        orgName: 'not_valid_host_name', # not valid org
        params: {},
        models: {
          workflowId: 1,
          outputSourceId: 1
        }
      }.with_indifferent_access
    ]
    assert_difference -> { WorkflowPatchRevision.count } do
      WorkflowPatchRevisionsJob.perform_now(patches: patches)
    end
    wpr = WorkflowPatchRevision.last

    expected_processing_info = {
      'info' => [],
      'errors' => [
        { 'group_name' => "Can't be blank" },
        { 'organization' => "Can't identify provided host name: not_valid_host_name" }
      ]
    }
    assert_equal 0, wpr.version
    assert_same_elements expected_processing_info['errors'], wpr.processing_info['errors']
    assert wpr.failed
    refute wpr.organization_id
    refute wpr.in_progress
    refute wpr.job_id
    refute wpr.completed
  end

  test 'should perform job create patch and add validation description of patch schema' do
    # version 0, workflow_type -> `groups`
    patches = [
      {
        patchType: 'insert',
        data: {
          id: @user.id.to_s,
          group_name: 'group_name', # blank group name
          group_type: 'group_type',
          group_description: 'patch_test'
        },
        meta: {
          historyRevisionVersion: 1,
          historyRevisionDatetime: 1536931486000,
          historyRevisionDataJson: {}
        },
        workflowType: 'groups',
        orgName: @org.host_name,
        params: {},
        models: {
          workflowId: 1,
          outputSourceId: 1
        }
      }.with_indifferent_access
    ]
    assert_difference -> { WorkflowPatchRevision.count } do
      WorkflowPatchRevisionsJob.perform_now(patches: patches)
    end
    wpr = WorkflowPatchRevision.last
    schema_validation = <<-TXT.strip_heredoc.gsub(/^*\n/, ' ')
      The property '#/' did not contain
      a required property of 'patchId' in schema
    TXT
    actual_schema_errors = wpr.processing_info['errors'].first['schema_validation']
    schema_validated = actual_schema_errors.first.start_with?(schema_validation)

    assert_equal 0, wpr.version
    assert schema_validated
    assert wpr.failed
    refute wpr.in_progress
    refute wpr.job_id
    refute wpr.completed
  end

  test 'should perform job create patch and add validation description for users_groups patch' do
    # version 0, workflow_type -> `users_groups`
    patches = [
      {
        patchId: '8c6a04f0-b821-11e8-87cd-e54aaae67708',
        patchType: 'insert',
        data: {
         id: @user.id.to_s,
         group_id: nil, # blank group_name
         member_type: 'some_constant_xyz' # not supported in version 0
        },
        meta: {
          historyRevisionVersion: 1,
          historyRevisionDatetime: 1536931486000,
          historyRevisionDataJson: {}
        },
        workflowType: 'users_groups',
        orgName: @org.host_name,
        params: {},
        models: {
          workflowId: 1,
          outputSourceId: 1
        }
      }.with_indifferent_access
    ]
    assert_difference -> { WorkflowPatchRevision.count } do
      WorkflowPatchRevisionsJob.perform_now(patches: patches)
    end
    wpr = WorkflowPatchRevision.last

    expected_processing_info = {
      'info' => [],
      'errors' => [
        { 'group_id' => "Can't be blank" }
      ]
    }
    assert_equal 0, wpr.version
    assert_same_elements expected_processing_info['errors'], wpr.processing_info['errors']
    assert wpr.failed
    assert wpr.organization_id
    refute wpr.in_progress
    refute wpr.job_id
    refute wpr.completed
  end

  # groups v1.2 critical validation
  test 'should perform job and validate presence of group id' do
    # version 2, workflow_type -> `groups`
    patches = [
      {
        patchId: '8c6a04f0-b821-11e8-87cd-e54aaae67708',
        patchType: 'insert',
        data: {
          id: nil,
          group_type: 'group_type',
          mandatory_type: 'mandatory_type',
          group_description: 'patch_test'
        },
        meta: {
          historyRevisionVersion: 1,
          historyRevisionDatetime: 1536931486000,
          historyRevisionDataJson: {}
        },
        workflowType: 'groups',
        orgName: @org.host_name,
        params: {},
        models: {
          workflowId: 1,
          outputSourceId: 1
        }
      }.with_indifferent_access
    ]
    assert_difference -> { WorkflowPatchRevision.count } do
      WorkflowPatchRevisionsJob.perform_now(patches: patches, version: 2)
    end
    wpr = WorkflowPatchRevision.last
    schema_validation = <<-TXT.strip_heredoc.gsub(/^*\n/, ' ')
      The property '#/data/id' of type NilClass did not match the following type:
    TXT
    actual_schema_errors = wpr.processing_info['errors'].first['schema_validation']
    schema_validated = actual_schema_errors.first.start_with?(schema_validation)

    assert_equal 2, wpr.version
    assert schema_validated
    assert wpr.failed
    refute wpr.completed
  end

  test 'should perform job and ignore blank patches' do
    patches = [{}, {}]
    assert_no_difference -> { WorkflowPatchRevision.count } do
      WorkflowPatchRevisionsJob.perform_now(patches: patches)
    end
  end
end

require 'test_helper'

class NotificationGeneratorJobTest < ActiveSupport::TestCase
  self.use_transactional_fixtures = false

  setup do
    EdcastActiveJob.unstub :perform_later
    Post.any_instance.stubs(:p_read_by_creator).returns(true)
    NotificationGeneratorJob.unstub :perform_later
    VideoStream.any_instance.stubs :create_index
    VideoStream.any_instance.stubs :update_index
    ChannelsCard.any_instance.stubs :create_users_management_card
    VideoStreamBatchNotificationGeneratorJob.unstub :perform_later

    @organization = create(:organization)
    @user = create(:user, organization: @organization)
  end

  test 'card added, removed, updated from pathway' do
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')

    #card added test
    message = "There are new card(s) added to the pathway, #{cover.title} since you completed. View the updates now"
    assert_difference ->{Notification.count}, 1 do
      NotificationGeneratorJob.new.perform({user_id: @user.id, item_id: cover.id, notification_type: 'added_to', item_type: 'added_to', custom_message: message})
    end
    assert_equal 'added_to', Notification.last.notification_type
    assert_equal cover.id, Notification.last.notifiable_id
    assert_equal message, Notification.last.custom_message

    #card removed test
    message = "A SmartCard has been removed from the completed pathway, #{cover.title}"
    assert_difference ->{Notification.count}, 1 do
      NotificationGeneratorJob.new.perform({user_id: @user.id, item_id: cover.id, notification_type: 'removed_from', item_type: 'removed_from', custom_message: message})
    end
    assert_equal 'removed_from', Notification.last.notification_type
    assert_equal cover.id, Notification.last.notifiable_id
    assert_equal message, Notification.last.custom_message

    #card updated test
    message = "An existing card has been updated since you completed the pathway, #{cover.title}. View the updates now"
    assert_difference ->{Notification.count}, 1 do
      NotificationGeneratorJob.new.perform({user_id: @user.id, item_id: cover.id, notification_type: 'card_updated', item_type: 'card_updated', custom_message: message})
    end
    assert_equal 'card_updated', Notification.last.notification_type
    assert_equal cover.id, Notification.last.notifiable_id
    assert_equal message, Notification.last.custom_message
  end

  test 'access control for video streams in channels' do
    VideoStream.any_instance.stubs(:generate_notification).returns(nil)
    VideoStream.any_instance.stubs(:generate_going_live_notification).returns(nil)
    PubnubNotifier.stubs(:notify)
    creator, user_follower, channel_follower, ch_and_u_follower, ch_and_ch_follower = create_list(:user, 5, organization: @organization)
    private_channel = create(:channel, is_private: true, organization: @organization)
    another_private_channel = create(:channel, is_private: true, organization: @organization)
    v = create(:wowza_video_stream, creator: creator, status: 'live')
    create(:follow, followable: creator, user: user_follower)
    create(:follow, followable: creator, user: ch_and_u_follower)
    create(:follow, followable: private_channel, user: channel_follower)
    create(:follow, followable: private_channel, user: ch_and_u_follower)
    create(:follow, followable: private_channel, user: ch_and_ch_follower)
    create(:follow, followable: another_private_channel, user: ch_and_ch_follower)

    assert_difference ->{Notification.count}, 2 do
      NotificationGeneratorJob.perform_now(item_id: v.id, item_type: 'video_stream', notification_type: 'stream_started')
    end
    assert_same_elements [user_follower, ch_and_u_follower], Notification.last(2).map(&:user)

    # now it is private
    v.channels << private_channel
    v.channels << another_private_channel
    # Make sure we don't send repeat notifications
    assert_difference ->{Notification.count}, 2 do
      NotificationGeneratorJob.perform_now(item_id: v.id, item_type: 'video_stream', notification_type: 'stream_started')
    end
    assert_same_elements [channel_follower, ch_and_ch_follower], Notification.last(2).map(&:user)
  end

  test 'generate notification on stream schedule/live for all creator followers and channel followers' do
    PubnubNotifier.unstub(:notify)
    f1, f2, f3, f4, nf5, chf = create_list(:user, 6, organization: @organization)
    ch = create(:channel, label: 'something', autopull_content: true, organization: @organization)
    creator = create(:user, organization: @organization)
    [f1, f2, f3, f4].each {|f| create(:follow, user: f, followable: creator) }
    create(:follow, user: chf, followable: ch)

    another_org = create(:organization)
    another_org_user = create(:user, organization: another_org)

    VideoStream.any_instance.stubs(:generate_notification).returns(nil)
    VideoStream.any_instance.stubs(:generate_going_live_notification).returns(nil)

    v = create(:wowza_video_stream, creator: creator, name: '#tag1', status: 'live')
    PubnubNotifier.expects(:notify).with(has_entries(item: v.card, notification_type: 'stream_started', excluded_user_ids: [v.creator_id], deep_link_id: v.card.id, deep_link_type: 'card', content: "#{creator.name} is now live: '#{v.snippet}'", host_name: v.creator.organization.host )).returns(nil).once
    expected_number_of_notifications = [f1,f2,f3,f4].count
    assert_difference ->{Notification.count}, expected_number_of_notifications do
      NotificationGeneratorJob.perform_later(item_id: v.id, item_type: 'video_stream', notification_type: 'stream_started')
    end
    notifs = Notification.last(expected_number_of_notifications)
    assert_equal ['stream_started'], notifs.map(&:notification_type).uniq
    assert_same_elements [f1,f2,f3,f4], notifs.map{|n| n.user}
    assert_equal [[creator.id]], notifs.map{|n| n.causal_users.pluck(:id)}.uniq

    PubnubNotifier.expects(:notify).with(has_entries(item: v.card, notification_type: 'stream_scheduled', excluded_user_ids: [v.creator_id], deep_link_id: v.card.id, deep_link_type: 'card', content: "#{creator.name} scheduled a live stream: '#{v.snippet}'", host_name: v.creator.organization.host )).returns(nil).once
    assert_difference ->{Notification.count}, expected_number_of_notifications do
      NotificationGeneratorJob.perform_later(item_id: v.id, item_type: 'video_stream', notification_type: 'stream_scheduled')
    end
    notifs = Notification.last(expected_number_of_notifications)
    assert_equal ['stream_scheduled'], notifs.map(&:notification_type).uniq
    assert_same_elements [f1,f2,f3,f4], notifs.map{|n| n.user}
    assert_equal [[creator.id]], notifs.map{|n| n.causal_users.pluck(:id)}.uniq

    # people following user and the channel should both get notifications
    new_stream = create(:wowza_video_stream, creator: creator, name: '#tag1', status: 'live')
    new_stream.channels << ch
    PubnubNotifier.expects(:notify).with(has_entries(item: new_stream.card, notification_type: 'stream_started', excluded_user_ids: [new_stream.creator_id], deep_link_id: new_stream.card.id, deep_link_type: 'card', content: "#{creator.name} is now live: '#{new_stream.snippet}'", host_name: new_stream.creator.organization.host )).returns(nil).once
    assert_difference ->{Notification.count}, 5 do
      NotificationGeneratorJob.perform_later(item_id: new_stream.id, item_type: 'video_stream', notification_type: 'stream_started')
    end
    notifs = Notification.last(5)
    assert_equal ['stream_started'], notifs.map(&:notification_type).uniq
    assert_same_elements [f1, f2, f3, f4, chf], notifs.map{|n| n.user}
    assert_equal [[creator.id]], notifs.map{|n| n.causal_users.pluck(:id)}.uniq

    PubnubNotifier.expects(:notify).with(has_entries(item: new_stream.card, notification_type: 'stream_scheduled', excluded_user_ids: [new_stream.creator_id], deep_link_id: new_stream.card.id, deep_link_type: 'card', content: "#{creator.name} scheduled a live stream: '#{new_stream.snippet}'", host_name: new_stream.creator.organization.host )).returns(nil).once
    assert_difference ->{Notification.count}, 5 do
      NotificationGeneratorJob.perform_later(item_id: new_stream.id, item_type: 'video_stream', notification_type: 'stream_scheduled')
    end
    notifs = Notification.last(5)
    assert_equal ['stream_scheduled'], notifs.map(&:notification_type).uniq
    assert_same_elements [f1,f2,f3,f4,chf], notifs.map{|n| n.user}
    assert_equal [[creator.id]], notifs.map{|n| n.causal_users.pluck(:id)}.uniq


    # bad notification type
    assert_no_difference ->{Notification.count} do
      NotificationGeneratorJob.perform_later(item_id: v.id, item_type: 'video_stream', notification_type: 'stream_ended')
    end

    PubnubNotifier.expects(:notify).with(has_entries(item: v.card, notification_type: 'scheduled_stream_reminder', deep_link_id: v.card.id, deep_link_type: 'card', content: "Time to start your stream: '#{v.snippet}'", host_name: v.creator.organization.host )).returns(nil).once
    # reminder notification
    v.status = 'upcoming'
    v.save
    assert_difference ->{Notification.count} do
      NotificationGeneratorJob.perform_later(item_id: v.id, item_type: 'video_stream', notification_type: 'scheduled_stream_reminder', card_id: 1)
    end
    notif = Notification.last
    assert_equal v.creator, notif.user
    assert_equal 'scheduled_stream_reminder', notif.notification_type
  end

  test 'should create new notification when a new clc is started' do
    clc = create(:clc,name: 'test clc', entity: @organization, organization: @organization)
    previous_notification_count = Notification.count
    assert_difference ->{Notification.count} do
      NotificationGeneratorJob.perform_later(item_id: clc.id, item_type: 'clc', notification_type: 'clc_started')
    end

    current_notification_count = Notification.count
    assert_equal (current_notification_count - previous_notification_count), clc.entity.users.not_suspended.count
  end

  test 'should create new notification when curator skips card' do
    skip "moved to new notification framework"
    channel = create :channel, user: @user, organization: @organization, curate_only: true
    curator = create :user, organization: @organization
    channel.curators << curator
    card = create(:card, author: @user)
    channels_card = create(:channels_card, channel_id: channel.id, card_id: card.id, state: ChannelsCard::STATE_NEW)
    channels_card.skip
    NotificationGeneratorJob.perform_now({item_id: channels_card.id, item_type: 'skipped_card', notification_type: 'skipped_card'})
    message = "The curator of the channel #{channel.label}, has rejected your post #{card.snippet_html}"

    notif = Notification.last
    assert_equal message, notif.message
    assert_equal message, notif.custom_message
    assert_equal 'skipped_card', notif.notification_type
  end

  test 'should create notification on a group invite with app_deep_link_id & type' do
    BranchmetricsApi.stubs :generate_link
    inviter = create(:user, organization: @organization)
    invited_user = create(:user, organization: @organization)
    group = create(:team, organization: @organization)
    assert_difference -> {Notification.count} do
      create(:invitation, recipient_email: invited_user.email, invitable: group, sender_id: inviter.id)
    end
    notification = Notification.last

    message = "You've been invited by #{inviter.name} to join\nthe #{group.name} Team"
    assert_equal message, notification.custom_message
    assert_equal 'group_invite', notification.notification_type
    assert_equal group.id, notification.app_deep_link_id
    assert_equal 'team', notification.app_deep_link_type
  end

  test 'should not create notification on a group invite with wrong item_id' do
    opts = {item_id: 3, item_type: 'group_invite'}
    warn_msg = 'Trying to generate group invite notification for bad id: 3'
    Rails.logger.expects(:warn).returns(warn_msg).once
    assert_no_difference -> {Notification.count} do
      NotificationGeneratorJob.new.perform(opts)
    end
  end

  test 'should create new notification when any user wins a badge' do
    clc_badge = create(:clc_badge, organization: @organization)
    clc = create(:clc,name: 'test clc', entity: @organization, organization: @organization,
                 clc_badgings_attributes: [{badge_id: clc_badge.id,
                                            title: "clc-25",
                                            target_steps:25}])
    clc_badging = clc.clc_badgings.first
    user_badge = create(:user_badge, user_id: @user.id, badging_id: clc_badging.id)
    assert_difference ->{Notification.count} do
      NotificationGeneratorJob.perform_later(item_id: user_badge.id, item_type: 'clc', notification_type: 'won_badge', user_id: @user.id)
    end

    notif = Notification.last
    assert_equal @user.id, notif.user_id
    assert_equal 'won_badge', notif.notification_type
  end

  test 'stop scheduled video stream notification if the video stream status is live' do
    creator = create(:user)
    v = create(:wowza_video_stream, creator: creator, name: '#tag1', status: 'live')

    PubnubNotifier.expects(:notify).never

    assert_no_difference ->{Notification.count} do
      NotificationGeneratorJob.perform_later(item_id: v.id, item_type: 'video_stream', notification_type: 'scheduled_stream_reminder', card_id: 1)
    end
  end

  test 'stop scheduled video stream notification if the video stream status is past' do
    creator = create(:user, organization: @organization)
    v = create(:wowza_video_stream, creator: creator, name: '#tag1', status: 'past')

    PubnubNotifier.expects(:notify).never

    assert_no_difference ->{Notification.count} do
      NotificationGeneratorJob.perform_later(item_id: v.id, item_type: 'video_stream', notification_type: 'scheduled_stream_reminder', card_id: 1)
    end
  end

  test 'schedule video stream notification if the video stream status is upcoming' do
    creator = create(:user, organization: @organization)
    v = create(:wowza_video_stream, creator: creator, name: '#tag1', status: 'upcoming')

    PubnubNotifier.expects(:notify).with(has_entries(item: v.card, notification_type: 'scheduled_stream_reminder', deep_link_id: v.card.id, deep_link_type: 'card', content: "Time to start your stream: '#{v.snippet}'", host_name: v.creator.organization.host )).returns(nil).once

    assert_difference ->{Notification.count} do
      NotificationGeneratorJob.perform_later(item_id: v.id, item_type: 'video_stream', notification_type: 'scheduled_stream_reminder', card_id: 1)
    end
    notif = Notification.last
    assert_equal v.creator, notif.user
    assert_equal 'scheduled_stream_reminder', notif.notification_type
  end

  test 'schedule video stream notification if the video stream status is pending' do
    creator = create(:user, organization: @organization)
    v = create(:wowza_video_stream, creator: creator, name: '#tag1', status: 'pending')

    PubnubNotifier.expects(:notify).with(has_entries(item: v.card, notification_type: 'scheduled_stream_reminder', deep_link_id: v.card.id, deep_link_type: 'card', content: "Time to start your stream: '#{v.snippet}'", host_name: v.creator.organization.host )).returns(nil).once

    assert_difference ->{Notification.count} do
      NotificationGeneratorJob.perform_later(item_id: v.id, item_type: 'video_stream', notification_type: 'scheduled_stream_reminder', card_id: 1)
    end
    notif = Notification.last
    assert_equal v.creator, notif.user
    assert_equal 'scheduled_stream_reminder', notif.notification_type
  end

  test 'generate notification on announcement for all group users' do
    setup_user_and_group
    PubnubNotifier.unstub(:notify)
    title = "this be title"
    PubnubNotifier.expects(:notify).with(has_entries(deep_link_type: 'post', content: "#{@fac.name} made a new announcement: '#{title}'", excluded_user_ids: [@fac.id], notification_type: 'new_announcement', host_name: @fac.organization.host )).returns(nil).once
    assert_difference -> {Notification.count}, 4 do
      a = create(:announcement, user: @fac, group: @group, title: title)
    end

    Notification.find_each do |n|
      assert_equal [@fac], n.causal_users
    end
    assert_equal [@user.id, @u1.id, @u2.id, @u3.id].sort, Notification.all.pluck(:user_id).sort

    # log error on bad id
    Rails.logger.expects(:warn).once
    assert_no_difference -> {Notification.count} do
      NotificationGeneratorJob.new.perform(item_id: 'xxx', item_type: 'announcement')
    end
  end

  test 'generate notification on getting follows' do
    f1, f2 = create_list(:user, 2, organization: @organization)
    assert_difference ->{Notification.count} do
      create(:follow, followable: @user, user: f1)
    end
    n = Notification.last
    assert_equal 'new_follow', n.notification_type
    assert_equal @user, n.user
    assert_equal [f1], n.causal_users
    assert_equal 0, n.app_deep_link_id
    assert_equal 'follow', n.app_deep_link_type
    assert_nil n.widget_deep_link_id
    assert_nil n.widget_deep_link_type

    assert_no_difference ->{Notification.count} do
      create(:follow, followable: @user, user: f2)
    end
    n.reload
    assert_equal [f1, f2], n.causal_users

    # if notification more than a day old, don't pile on to it
    n.update_attributes(created_at: 2.days.ago)
    f3 = create(:user, organization: @organization)
    assert_difference ->{Notification.count} do
      create(:follow, followable: @user, user: f3)
    end
  end

  test 'should return nil id and type for non-existent notifiable' do
    user = create(:user, organization: @organization)
    notify = Notification.create(notification_type: 'some type', user: user, notifiable: nil)

    assert_equal [nil,nil], notify.app_deep_link_id_and_type
  end

  test 'generate notification for comment for thread followers' do
    setup_user_and_group
    PubnubNotifier.unstub(:notify)
    title = "some title"
    p = create(:post, user: @user, group: @group, title: title)

    # first comment, only follower is the thread creator, causal user is comment creator
    PubnubNotifier.expects(:notify).with(has_entries(deep_link_id: p.id, deep_link_type: 'post', notification_type: 'new_comment', excluded_user_ids: [@u1.id], content: "#{@u1.name} commented on post: '#{title}'", host_name: p.user.organization.host )).returns(nil).once
    assert_difference -> {Notification.count} do
      a = create(:comment, commentable: p, user: @u1)
    end
    n = Notification.last
    assert_equal @user, n.user
    assert_equal [@u1], n.causal_users
    assert_not n.causal_user_anonymous?(@u1)

    # second comment on the thread. Should add new comment creator as a
    # causal user to the existing notification for the thread creator.
    # First commenter should get a new notification with just one causal user
    PubnubNotifier.expects(:notify).returns(nil).times(3)
    assert_difference -> {Notification.count} do
      a = create(:comment, commentable: p, user: @u2)
    end

    n.reload
    assert_equal @user, n.user
    assert_equal [@u1, @u2], n.causal_users

    u1_notification = Notification.last
    assert_equal @u1, u1_notification.user
    assert_equal [@u2], u1_notification.causal_users

    # now both @user and @u1 have seen their notifications
    u1_notification.mark_as_seen
    n.mark_as_seen

    # another new comment, should now generate fresh notifications for all followers
    assert_difference -> {Notification.count}, 3 do
      create(:comment, commentable: p, user: @u3)
    end

    notifications = Notification.last(3)
    user_n = notifications[0]
    assert_equal @user, user_n.user
    assert_equal [@u3], user_n.causal_users

    u1_n = notifications[1]
    assert_equal @u1, u1_n.user
    assert_equal [@u3], u1_n.causal_users

    u2_n = notifications[2]
    assert_equal @u2, u2_n.user
    assert_equal [@u3], u2_n.causal_users

    # now u3 makes another comment. No notification should change, except for
    # user, who is addicted to edcast and checks notifications every second
    user_n.mark_as_seen
    assert_difference [->{Notification.count}, ->{NotificationsCausalUser.count}] do
      create(:comment, commentable: p, user: @u3)
    end
    user_n = Notification.last
    assert_equal @user, user_n.user
    assert_equal [@u3], user_n.causal_users

    # should log error for non existant id
    Rails.logger.expects(:warn).once
    assert_no_difference -> {Notification.count} do
      NotificationGeneratorJob.new.perform(item_id: 'xxx', item_type: 'comment')
    end
  end

  test 'generate notification for comment for card' do
    setup_user_and_group
    org = create(:organization)
    card = create(:card, author_id:  @user.id, title: 'title')
    card.reload
    assert_difference ->{Notification.count} do
      create(:comment, commentable: card, user: @u1)
    end

    #skip notifications for CMS cards
    card1 = create(:card, author_id: nil, title: 'title', organization_id: @u1.organization_id)
    assert_no_difference ->{Notification.count} do
      create(:comment, commentable: card1, user: @u1)
    end
  end

  def t_votable(votable, notification_type)
    # first upvote on post
    assert_difference -> {Notification.count} do
      v = create(:vote, votable: votable, voter: @u1)
    end

    n = Notification.last
    assert_equal @user, n.user
    assert_equal [@u1], n.causal_users
    assert_equal notification_type, n.notification_type
    assert_not n.causal_user_anonymous?(@u1)

    # second upvote on the post. should just add new causal user to
    # existing notification
    assert_no_difference -> {Notification.count} do
      v = create(:vote, votable: votable, voter: @u2)
    end

    n.reload
    assert_equal @user, n.user
    assert_equal [@u1, @u2], n.causal_users

    # now user has seen notification. should create a new one on another upvote
    n.mark_as_seen

    assert_difference -> {Notification.count} do
      create(:vote, votable: votable, voter: @u3)
    end

    n = Notification.last
    assert_equal @user, n.user
    assert_equal [@u3], n.causal_users
  end

  def setup_user_and_group
    @fac = create(:user, organization: @organization)
    @group = create(:group, organization: @organization)
    @group.add_member(@user)
    @group.add_admin(@fac)
    @u1 = create(:user, organization: @organization)
    @group.add_member(@u1)

    @u2 = create(:user, organization: @organization)
    @group.add_member(@u2)

    @u3 = create(:user, organization: @organization)
    @group.add_member(@u3)
  end

  test 'generate notification for card upvote' do
    card = create(:card, author_id: @user.id, title: 'title')
    card.reload
    voter1 = create(:user)
    assert_difference ->{Notification.count} do
      create(:vote, voter: voter1, votable: card)
    end

    n = Notification.last
    assert_equal 'new_card_upvote', n.notification_type
    assert_equal @user, n.user
    assert_equal [voter1], n.causal_users
    assert_equal "#{voter1.name} liked your post: 'title'", n.message

    #skip notification for card author
    assert_no_difference ->{Notification.count} do
      create(:vote, voter: @user, votable: card)
    end

    # second upvote should just add to existing notification
    voter2 = create(:user)
    assert_no_difference ->{Notification.count} do
      create(:vote, voter: voter2, votable: card)
    end

    n.reload
    assert_equal 'new_card_upvote', n.notification_type
    assert_equal @user, n.user
    assert_equal [voter1, voter2], n.causal_users
    assert_equal "#{voter1.name} and 1 other person liked your post: 'title'", n.message
  end

  test 'skip notification for card upvote if created by CMS(author_id is nil)' do
    org = create(:organization)
    card = create(:card, author_id: nil, title: 'title', organization_id: org.id)
    voter1 = create(:user)
    assert_no_difference ->{Notification.count} do
      create(:vote, voter: voter1, votable: card)
    end
  end

  test 'generate notification for approval on posts and comments' do
    setup_user_and_group
    p = create(:post, user: @user, group: @group)
    assert_difference -> {Notification.count} do
      create(:approval, approver: @fac, approvable: p)
    end

    n = Notification.last
    assert_equal 'new_approval', n.notification_type
    assert_equal @user, n.user
    assert_equal [@fac], n.causal_users
    assert_not n.causal_user_anonymous?(@fac)

    c = create(:comment, commentable: p, user: @user)
    assert_difference -> {Notification.count} do
      create(:approval, approver: @fac, approvable: c)
    end

    n = Notification.last
    assert_equal 'new_approval', n.notification_type
    assert_equal @user, n.user
    assert_equal [@fac], n.causal_users
    assert_not n.causal_user_anonymous?(@fac)
  end

  test 'generate notification for upvote for posts' do
    setup_user_and_group
    p = create(:post, user: @user, group: @group)
    t_votable(p, 'new_post_upvote')

    # should log error for non existant id
    Rails.logger.expects(:warn).once
    assert_no_difference -> {Notification.count} do
      NotificationGeneratorJob.new.perform(item_id: 'xxx', item_type: 'vote')
    end
  end

  test 'generate notification for upvote on comments' do
    setup_user_and_group
    p = create(:post, user: @user, group: @group)
    c = create(:comment, commentable: p, user: @user)
    t_votable(c, 'new_comment_upvote')
  end

  test 'casual users should be marked anonymous if so' do
    setup_user_and_group
    p = create(:post, user: @user, group: @group)

    # first comment, only follower is the thread creator, causal user is comment creator who is anonymous
    assert_difference -> {Notification.count} do
      a = create(:comment, commentable: p, user: @u1, anonymous: true)
    end

    n = Notification.last
    assert_equal @user, n.user
    assert_equal [@u1], n.causal_users
    assert n.causal_user_anonymous?(@u1)

  end

  test 'dont send stream notifications to creator' do
    PubnubNotifier.unstub(:notify)
    ch = create(:channel, autopull_content: true, label: 'some channel', organization: @organization)
    u = create(:user, organization: @organization)
    create(:follow, followable: ch, user: u)

    PubnubNotifier.expects(:notify).with(has_entries(excluded_user_ids: [u.id])).returns(nil).once
    assert_no_difference ->{Notification.count} do
      v = create(:wowza_video_stream, creator: u, status: 'live', start_time: Time.now.utc, name: 'some stream #tag1')
      NotificationGeneratorJob.perform_later(item_id: v.id, item_type: 'video_stream', notification_type: "stream_started")
    end
  end

  test 'generate notification for poll answer' do
    Analytics::MetricsRecorderJob.stubs(:perform_later)
    card = create(:card, author_id: @user.id, card_type: 'poll', card_subtype: 'text', title: 'title')
    responder1 = create(:user)
    assert_difference -> {Notification.count} do
      create(:quiz_question_attempt, user: responder1, quiz_question: card)
    end
    n = Notification.last
    assert_equal 'poll_answer', n.notification_type
    assert_equal @user, n.user
  end

  test 'generate notification for new poll card' do
    followers = create_list(:user, 3, organization: @organization)
    followers.each do |follower|
      create(:follow, followable: @user, user: follower)
    end

    card = nil
    assert_difference 'Notification.count', 3 do
      card = create(:card, author_id: @user.id, card_type: 'poll', card_subtype: 'text')
    end

    n = Notification.last(3)
    assert_equal ['new_poll'], n.map(&:notification_type).uniq
    assert_equal followers, n.map(&:user)
  end

  test 'dont generate notification for new poll card with private content on' do
    follower = create(:user, organization: @organization)
    create(:follow, followable: @user, user: follower)

    card = nil
    assert_difference 'Notification.count', 0 do
      card = create(:card, author_id: @user.id, card_type: 'poll', card_subtype: 'text', is_public: false)
    end
  end

  #test 'generate notification for new poll card only for users who can access the card' do
  #EP-16550 Now public cards are visible to user irrespective of which container they belong to
  #end

  test 'generate notification for mention in card/comment' do
    setup_user_and_group
    card = create(:card, author_id: @user.id, card_type: 'media', card_subtype: 'link')
    card.reload
    mention_user = create(:user, handle: "@test1")
    # u1 = create :user

    assert_difference -> {Notification.count} do
      mention = create(:mention, mentionable: card, user: mention_user)
    end
    n = Notification.last
    assert_equal Notification::TYPE_MENTION_IN_CARD, n.notification_type

    Comment.any_instance.stubs(:user_authorized).returns(true)
    comment = create(:comment, commentable: card, user: @u1)

    assert_difference -> {Notification.count} do
      mention = create(:mention, mentionable: comment, user: mention_user)
    end
    n = Notification.last
    assert_equal Notification::TYPE_MENTION_IN_COMMENT, n.notification_type
    Comment.any_instance.unstub(:user_authorized)

    # no notifications for post comment mentions
    g, u = create_gu
    p = create(:post, user: u, group: g)
    cp = create(:comment, commentable: p, user: u)
    assert_no_difference ->{Notification.count} do
      create(:mention, mentionable: cp, user: create(:user))
    end
  end

  test 'generate notification and push for mention in card' do
    card = create(:card, author: @user, organization: @user.organization)
    user = create(:user, handle:'@test1', organization: @user.organization)
    assert_difference -> {Notification.count} do
      Notification.any_instance.stubs(:message).returns("Mentioned in card")
      PubnubNotifier.expects(:notify).with({ deep_link_id: card.id, deep_link_type: 'card', item: card, content: "Mentioned in card", notification_type: Notification::TYPE_MENTION_IN_CARD, host_name: user.organization.host, user_id: user.id })
      mention = create(:mention, mentionable: card, user: user)
    end
  end

  test 'generate notification and push for mention in comment' do
    video_stream = create(:iris_video_stream, creator: @user, organization: @user.organization , status: 'past')
    video_stream.card.reload
    comment = create(:comment, commentable: video_stream.card, user: create(:user, organization: @user.organization))
    user = create(:user, handle:'@test1', organization: @user.organization)
    assert_difference -> {Notification.count} do
      Notification.any_instance.stubs(:message).returns("Mentioned in comment")
      PubnubNotifier.expects(:notify).with({ deep_link_id: video_stream.card_id, deep_link_type: 'card', item: video_stream.card, content: "Mentioned in comment", notification_type: Notification::TYPE_MENTION_IN_COMMENT, host_name: user.organization.host, user_id: user.id })
      mention = create(:mention, mentionable: comment, user: user)
    end
  end

  test 'should not generate notification for draft pathway, hidden_card and hidden_comment' do

    pack_card = create(:card, author_id: @user.id, card_type: 'pack', state: 'draft', card_subtype: 'text')
    pack_card.reload
    card1 = create(:card, author_id: @user.id, card_type: 'media', card_subtype: 'link', hidden: true)
    card1.reload
    comment = create(:comment, user_id: @user.id, commentable: card1, hidden: true)
    mention_user = create(:user)

    #drafted pathway
    assert_no_difference -> {Notification.count} do
      mention = create(:mention, mentionable: pack_card, user: mention_user)
    end

    #hidden caed
    assert_no_difference -> {Notification.count} do
      mention = create(:mention, mentionable: card1, user: mention_user)
    end

    #hidden comment
    assert_no_difference -> {Notification.count} do
      mention = create(:mention, mentionable: comment, user: mention_user)
    end

    #published comment should create notification
    comment1 = create(:comment, user_id: @user.id, commentable: card1)
    assert_difference -> {Notification.count} do
      mention = create(:mention, mentionable: comment1, user: mention_user)
    end
  end

  test 'channel invite notifications' do
    setup_user_and_group
    @fac = create(:user, organization: @organization)

    PubnubNotifier.unstub(:notify)
    ch = create(:channel, :robotics, organization: @organization)
    user1 = create(:user, organization: @organization)
    user2 = create(:user, organization: @organization)
    user3 = create(:user, organization: @organization)
    PubnubNotifier.expects(:notify).with(has_entries(deep_link_id: ch.id, deep_link_type: 'channel', user_id: user1.id, notification_type: 'added_author', host_name: user1.organization.host)).returns(nil).once
    PubnubNotifier.expects(:notify).with(has_entries(deep_link_id: ch.id, deep_link_type: 'channel', user_id: user3.id, notification_type: 'added_author', host_name: user3.organization.host)).returns(nil).once
    assert_difference -> {Notification.count}, 2 do
      NotificationGeneratorJob.perform_later(item_id: ch.id, item_type: 'add_authors', users_ids: [user1.id, user3.id], inviter: @fac.id)
    end
    ns = Notification.last(2)
    assert_equal ['added_author'], ns.map(&:notification_type).uniq
    assert_equal [user1, user3], ns.map(&:user)
    assert_equal [[@fac], [@fac]], ns.map(&:causal_users)

    PubnubNotifier.expects(:notify).with(has_entries(deep_link_id: ch.id, deep_link_type: 'channel', user_id: user2.id, notification_type: 'added_follower', host_name: user2.organization.host)).returns(nil).once
    PubnubNotifier.expects(:notify).with(has_entries(deep_link_id: ch.id, deep_link_type: 'channel', user_id: user3.id, notification_type: 'added_follower', host_name: user3.organization.host)).returns(nil).once
    assert_difference -> {Notification.count}, 2 do
      NotificationGeneratorJob.perform_later(item_id: ch.id, item_type: 'add_followers', users_ids: [user2.id, user3.id], inviter: @fac.id)
    end
    ns = Notification.last(2)
    assert_equal ['added_follower'], ns.map(&:notification_type).uniq
    assert_equal [user2, user3], ns.map(&:user)
    assert_equal [[@fac], [@fac]], ns.map(&:causal_users)

    PubnubNotifier.expects(:notify).never
    #duplicate notification
    assert_no_difference -> {Notification.count} do
      NotificationGeneratorJob.perform_later(item_id: ch.id, item_type: 'add_followers', users_ids: [user2.id, user3.id], inviter: @fac.id)
    end
  end

  test "should generation notification if card is shared with group" do
    @author = create :user, organization: @organization
    @card = create :card, author_id: @author.id
    team = create :team, organization: @organization, name: "Ruby Development"
    team_members = (0..2).map{|x| u = create(:user, organization: @organization);
                                      create :teams_user, user: u, team: team; u}
    teams_card = create :teams_card, card: @card, team: team, type: 'SharedCard', user: @author
    notifications = Notification.last(3)
    assert_equal ['team_card_share'], notifications.map(&:notification_type).uniq
    assert_equal ['Card'], notifications.map(&:notifiable_type).uniq
    assert_same_elements team_members.map(&:id), notifications.map(&:user_id)
  end

  test "should generation notification if card is shared with user" do
    @author = create :user, organization: @organization
    user1 = create(:user, organization: @organization)
    @card = create :card , author_id: @author.id
    card_user_share = create :card_user_share, user: user1, card: @card
    notifications = Notification.last
    assert_equal 'user_card_share', notifications.notification_type
    assert_equal 'Card', notifications.notifiable_type
    assert_equal user1.id, notifications.user_id
  end

  test 'should generate notification for card shared with team only for user who have access' do
    @card = create :card, author_id: @user.id, is_public: false
    user_list = create_list(:user, 2, organization: @user.organization)
    create(:card_user_permission, card_id: @card.id, user_id: user_list[0].id)
    team = create :team, organization: @organization, name: "Gina"
    team.add_member user_list[1]
    team.add_member user_list[0]
    teams_card = create :teams_card, card: @card, team: team, type: 'SharedCard', user: @user
    assert 1, Notification.all.count
    assert_equal 'team_card_share', Notification.last.notification_type
    assert_equal 'Card', Notification.last.notifiable_type
    assert_equal user_list[0].id, Notification.last.user_id
  end

  test "should generation notification with author name if card is shared with user throught creation model" do
    @author = create :user, organization: @organization,first_name: "ABC" ,last_name: "XYZ"
    user1 = create(:user, organization: @organization)
    @card = create :card , author_id: @author.id
    card_user_share = create :card_user_share, user: user1, card: @card
    notification = Notification.last
    notification.causal_users << @author
    assert_match /ABC XYZ shared This is a cool card with you./, notification.message
  end

  test "should delete notification if teams_card get destroyed" do
    @author = create :user, organization: @organization
    @card = create :card, author_id: @author.id
    team = create :team, organization: @organization, name: "Ruby Development"
    team_members = (0..2).map{|x| u = create(:user, organization: @organization);
                                      create :teams_user, user: u, team: team; u}
    teams_card = create :teams_card, card: @card, team: team, type: 'SharedCard', user: @author
    notification_before_destroy = Notification.count
    teams_card.destroy
    notification_after_destroy = Notification.count
    assert_equal  notification_before_destroy.to_i - 3 ,notification_after_destroy
  end

  test "should delete notification if card_user_share get destroyed" do
    @author = create :user, organization: @organization
    user1 = create(:user, organization: @organization)
    @card = create :card , author_id: @author.id
    card_user_share = create :card_user_share, user: user1, card: @card
    notification_before_destroy = Notification.count
    card_user_share.destroy
    notification_after_destroy = Notification.count
    assert_equal  notification_before_destroy.to_i - 1 ,notification_after_destroy
  end

  test 'should create notification for sub_admin if user join to the team' do
    sub_admin = create(:user, organization: @organization)
    user = create(:user, organization: @organization)
    team = create(:team, organization: @organization)
    team.add_sub_admin(sub_admin)

    assert_difference -> {Notification.count}, 1 do
      NotificationGeneratorJob.perform_later(
        { item_id: team.id, item_type: 'join_group', team_user_id: user.id }
      )
    end

    notification = Notification.last

    assert_equal "#{user.full_name} joined the team #{team.name}", notification.custom_message
    assert_equal [team.id, 'team'], [notification.app_deep_link_id, notification.app_deep_link_type]
    assert_equal sub_admin.id, notification.user_id
  end

  test 'should create notification for sub_admin if user left the team' do
    sub_admin = create(:user, organization: @organization)
    user = create(:user, organization: @organization)
    team = create(:team, organization: @organization)
    team.add_sub_admin(sub_admin)

    assert_difference -> {Notification.count}, 1 do
      NotificationGeneratorJob.perform_later(
        { item_id: team.id, item_type: 'leave_group', team_user_id: user.id }
      )
    end

    notification = Notification.last

    assert_equal "#{user.full_name} left the team #{team.name}", notification.custom_message
    assert_equal [team.id, 'team'], [notification.app_deep_link_id, notification.app_deep_link_type]
    assert_equal sub_admin.id, notification.user_id
  end

  test 'should create notification for sub_admin if user accepted the invitation to the team' do
    sub_admin = create(:user, organization: @organization)
    user = create(:user, organization: @organization)
    team = create(:team, organization: @organization)
    team.add_sub_admin(sub_admin)

    assert_difference -> {Notification.count}, 1 do
      NotificationGeneratorJob.perform_later(
        { item_id: team.id, item_type: 'accept_group_invite', team_user_id: user.id }
      )
    end

    notification = Notification.last

    assert_equal "#{user.full_name} accepted the invitation to the team #{team.name}", notification.custom_message
    assert_equal [team.id, 'team'], [notification.app_deep_link_id, notification.app_deep_link_type]
    assert_equal sub_admin.id, notification.user_id
  end

  test 'should create notification for sub_admin if user declined an invitation to the team' do
    sub_admin = create(:user, organization: @organization)
    user = create(:user, organization: @organization)
    team = create(:team, organization: @organization)
    team.add_sub_admin(sub_admin)

    assert_difference -> {Notification.count}, 1 do
      NotificationGeneratorJob.perform_later(
        { item_id: team.id, item_type: 'decline_group_invite', team_user_id: user.id }
      )
    end

    notification = Notification.last

    assert_equal "#{user.full_name} declined an invitation to the team #{team.name}", notification.custom_message
    assert_equal [team.id, 'team'], [notification.app_deep_link_id, notification.app_deep_link_type]
    assert_equal sub_admin.id, notification.user_id
  end

  test 'should create notification when topic request has been approved and published by admin' do
    user = create(:user, organization: @organization)
    topic_request = create(:topic_request, organization_id: @organization.id, requestor_id: user.id, label: "Drawings",state: "approved")

    assert_difference -> {Notification.count}, 1 do
      NotificationGeneratorJob.perform_later(
        {item_id: topic_request.id, item_type: 'topic_request_approval', item_state: "topic_request_published" }
      )
    end
    notification = Notification.last
    assert_equal "Your learning topic 'Drawings' has been approved and published.", notification.custom_message
    assert_equal 'topic_request_published', notification.notification_type
    assert_equal 'TopicRequest', notification.notifiable_type
    assert_equal user.id, notification.user_id
  end

  test 'should create notification when topic request has been rejected by admin' do
    user = create(:user, organization: @organization)
    topic_request = create(:topic_request, organization_id: @organization.id, label: "Drawings", state: "pending",requestor_id: user.id)

    assert_difference -> {Notification.count}, 1 do
      NotificationGeneratorJob.perform_later(
        {item_id: topic_request.id, item_type: 'topic_request_approval', item_state: "topic_request_rejected" }
      )
    end
    notification = Notification.last
    assert_equal "Your learning topic 'Drawings' has been rejected.", notification.custom_message
    assert_equal 'topic_request_rejected', notification.notification_type
    assert_equal 'TopicRequest', notification.notifiable_type
    assert_equal user.id, notification.user_id
  end
end

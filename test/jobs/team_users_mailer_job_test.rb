require 'test_helper'

class TeamUsersMailerJobTest < ActiveSupport::TestCase
  setup do
    @email_mock = mock('email')
    @email_mock.stubs(:deliver_now)
  end

  test 'should send group admin email to all users' do
    
    msg = "This is message to all group users sent from admin."
    org = create(:organization)
    admin = create(:user, organization: org, organization_role: 'admin')
    user = create(:user, organization: org)
    team = create(:team, organization: org)
    team.add_member(user)
    
    TeamUsersMailer.expects(:send_notification).with({ 
      message: msg,
      user_id: user.id,
      sender_id: admin.id,
      team_id: team.id
    }).returns(@email_mock).once

    TeamUsersMailerJob.new.perform({message: msg, team_id: team.id, sender_id: admin.id})
  end
end

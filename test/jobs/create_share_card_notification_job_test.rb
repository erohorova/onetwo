require 'test_helper'

class CreateShareCardNotificationJobTest < ActiveSupport::TestCase

  setup do
    WebMock.disable!
    @org = create(:organization)
    @author = create :user, organization: @org
    @card = create :card, author_id: @author.id
  end

  teardown do
    WebMock.enable!
  end

  test "should trigger only content share notification" do
    team = create :team, organization: @org
    team_members = (0..2).map{|x| u = create(:user, organization: @org); create :teams_user, user: u, team: team; u}
    create :teams_card, card: @card, team: team, type: 'SharedCard'

    EDCAST_NOTIFY.expects(:trigger_event).times(1)
    CreateShareCardNotificationJob.perform_now(card_id: @card.id, event: Card::EVENT_CARD_SHARE, team_ids: [team.id])
  end

  test "should not trigger notification when content shared/posted to channel" do
    channel = create :channel, organization: @org
    users_following_channel = (0..2).map{|x| u = create(:user, organization: @org); create :follow, user: u, followable: channel; u}
    create :channels_card, card: @card, channel: channel

    EDCAST_NOTIFY.expects(:trigger_event).never
    CreateShareCardNotificationJob.perform_now(card_id: @card.id, event: Card::EVENT_CARD_SHARE, channel_ids: [channel.id])
  end

  test "should not trigger job if org notification toggle off" do
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(false)
    channel = create :channel, organization: @org
    users_following_channel = (0..2).map{|x| u = create(:user, organization: @org); create :follow, user: u, followable: channel; u}
    create :channels_card, card: @card, channel: channel

    CreateShareCardNotificationJob.expects(:perform_later).never
    @card.send_card_create_or_share_notification(event: Card::EVENT_CARD_SHARE, user_ids: nil, team_ids: nil)
    Organization.any_instance.unstub(:notifications_and_triggers_enabled?)
  end

  test "should not trigger job if card is draft" do
    card = create(:card, card_type: Card::TYPE_PACK, state: Card::STATE_DRAFT, author_id: @author.id)
    channel = create :channel, organization: @org
    users_following_channel = (0..2).map{|x| u = create(:user, organization: @org); create :follow, user: u, followable: channel; u}
    create :channels_card, card: card, channel: channel

    CreateShareCardNotificationJob.expects(:perform_later).never
    card.send_card_create_or_share_notification(event: Card::EVENT_CARD_SHARE, user_ids: nil, team_ids: nil)
  end
end
require 'test_helper'

class ChannelCardEnqueueJobTest < ActiveJob::TestCase

  test 'should call FollowedCardBatchEnqueueJobV1' do
    FollowedCardBatchEnqueueJobV1.unstub :perform_later
    ChannelCardEnqueueJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    org = create(:organization)
    channel = create(:channel, organization: org)
    card = create(:card, author_id: nil, organization: org)
    card.channels << channel
    users = create_list(:user, 5, organization: org)
    channel.add_followers(users)

    FollowedCardBatchEnqueueJobV1.expects(:perform_later).once
    ChannelCardEnqueueJob.perform_now(channel.id, card.id)
  end
end

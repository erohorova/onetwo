require 'test_helper'

class WorkflowObserverJobTest < ActiveJob::TestCase
  include ActiveJob::TestHelper
  test 'should add user to new group on attr change' do
    EdcastActiveJob.unstub(:perform_later)
    WorkflowProcessorJob.unstub(:perform_later)

    @org = create :organization
    @creator = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
    @workflow = create(
      :workflow,
      user_id: @creator.id,
      organization: @org
    )

    custom_field_location = create :custom_field, organization: @org, abbreviation: 'location', display_name: 'location'
    custom_field_country  = create :custom_field, organization: @org, abbreviation: 'country', display_name: 'country'

    user2 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
    create :user_custom_field, user: user2, custom_field: custom_field_country, value: 'India'

    user3 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active',
                   is_anonymized: false, first_name: nil
    create :user_custom_field, user: user3, custom_field: custom_field_country, value: 'India'

    user5 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
    user5_location = create :user_custom_field, user: user5, custom_field: custom_field_location, value: 'Pune'
    create :user_custom_field, user: user5, custom_field: custom_field_country, value: 'India'

    assert_equal 0, Team.all.length
    WorkflowService::Process.new(workflow: @workflow).run
    assert_equal 1, Team.all.length
    team1 = Team.first
    assert_equal 'Pune', team1.name
    assert_same_elements [user5.id], team1.users.ids
    assert_equal 'Team', @workflow.reload.wf_output_result['type']
    assert_same_elements [team1.id], @workflow.reload.wf_output_result['ids']

    @workflow.update_attributes(is_enabled: true)
    assert @workflow.is_enabled

    user5_location.update_attributes!(value: 'Bangalore')
    previous_changes = {'location' => %w[Pune Bangalore]}

    assert_no_enqueued_jobs

    assert_enqueued_with(job: WorkflowProcessorJob) do
      WorkflowObserverJob.new.perform(
        organization: @org,
        changes: previous_changes,
        model_name: user5_location.class.to_s,
        input_data_ids: [user5.id]
      )
    end

    assert_enqueued_jobs 1

    perform_enqueued_jobs do
      WorkflowObserverJob.new.perform(
        organization: @org,
        changes: previous_changes,
        model_name: user5_location.class.to_s,
        input_data_ids: [user5.id]
      )
    end

    assert_performed_jobs 1
    assert_equal 2, Team.all.length
    team2 = Team.last

    # user removed from team1
    assert_empty team1.users.ids

    assert_equal 'Bangalore', team2.name

    # user added in team2 on attr change
    assert_same_elements [user5.id], team2.users.ids

    assert_equal 'Team', @workflow.reload.wf_output_result['type']
    assert_same_elements [team1.id, team2.id], @workflow.reload.wf_output_result['ids']
  end
end
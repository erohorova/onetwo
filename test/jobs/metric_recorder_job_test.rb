require 'test_helper'
class MetricRecorderJobTest < ActiveSupport::TestCase

  setup do
    EdcastActiveJob.unstub :perform_later
    MetricRecorderJob.unstub :perform_later
  end

  test 'should figure out the owner for card' do
    org = create(:organization)
    card = create(:card, author: create(:user, organization: org))
    user = create(:user, organization: org)

    MetricRecord.expects(:create).with(has_entries(owner_id: card.author_id, owner_type: 'user'))
    MetricRecorderJob.perform_later(user.id, 'act', {type: 'like', object: 'card', object_id: card.id,
                                           platform: 'web', device_id: 'deviceid123'
                                           })


  end

  test 'should figure out the owner for collection' do
    org = create(:organization)
    card = create(:card, author: create(:user, organization: org))
    user = create(:user, organization: org)

    MetricRecord.expects(:create).with(has_entries(owner_id: card.author_id, owner_type: 'user'))
    MetricRecorderJob.perform_later(user.id, 'act', {type: 'publish', object: 'collection', object_id: card.id,
                                                     platform: 'web', device_id: 'deviceid123'
    })
  end

  test 'should figure out the owner for comment' do
    org = create(:organization)
    card = create(:card, is_public: true, author: create(:user, organization: org))
    comment = create(:comment, commentable: card, user: create(:user, organization: org))
    org = create(:organization)
    user = create(:user, organization: org)

    MetricRecord.expects(:create).with(has_entries(owner_id: comment.user_id, owner_type: 'user'))
    MetricRecorderJob.perform_later(user.id, 'act', {type: 'like', object: 'comment', object_id: comment.id,
                                                     platform: 'web', device_id: 'deviceid123'
                                           })
  end

  test 'should save the organization_id, referer page from payload' do
    card = create(:card, is_public: true)
    org = create(:organization)
    user = create(:user, organization: org)

    MetricRecord.expects(:create).with(
      has_entries(actor_id: user.id,
                  organization_id: user.organization_id,
                  referer: 'http://edcast.com'))

    MetricRecorderJob.perform_later(user.id, 'act',
      { type: 'like',
        object: 'card', object_id: card.id,
        platform: 'web', device_id: 'deviceid123',
        organization_id: user.organization_id,
        referer: 'http://edcast.com'
      }
    )
  end

  test 'should save the ecl id of card' do
    org = create(:organization)

    card = create(:card, is_public: true, ecl_id: 'ecl-test-id', author_id: nil, organization: org)
    user = create(:user, organization: org)

    MetricRecord.expects(:create).with(
      has_entries(actor_id: user.id,
                  organization_id: user.organization_id,
                  referer: 'http://edcast.com',
                  ecl_id: 'ecl-test-id'))

    MetricRecorderJob.perform_later(user.id, 'act',
      { type: 'like',
        object: 'card', object_id: card.id,
        platform: 'web', device_id: 'deviceid123',
        organization_id: user.organization_id,
        referer: 'http://edcast.com'
      }
    )
  end
end

require 'test_helper'

class CreateCustomEmailTemplateJobTest < ActiveJob::TestCase
  test 'should create all default templates for an org' do
    org = create(:organization)

    assert_difference -> { EmailTemplate.count }, 21 do
      CreateCustomEmailTemplateJob.perform_now(org.id)
    end
    template_list = EmailTemplate::DEFAULT_TITLES[:notify] + EmailTemplate::DEFAULT_TITLES[:mailer]
    assert_same_elements EmailTemplate.where(organization_id: org.id).pluck(:title), template_list
  end
    
end

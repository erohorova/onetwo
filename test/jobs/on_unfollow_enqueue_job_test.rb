require 'test_helper'

class OnUnfollowEnqueueJobTest < ActiveSupport::TestCase

  self.use_transactional_fixtures = false
  setup do
    OnFollowEnqueueJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    OnUnfollowEnqueueJob.unstub :perform_later
    ChannelsCard.any_instance.stubs :create_users_management_card

    WebMock.disable!
    UserContentsQueue.create_index! force: true

    @organization = create(:organization)
    @author = create(:user, organization: @organization)
    @channel = create(:channel, :robotics, organization: @organization)
    @cards = (1..10).map do |i|
      card = create(:card, author_id: @author.id, organization_id: @organization.id, created_at: i.minutes.ago, published_at: i.minutes.ago)
      card.channels << @channel
      card
    end

    @user = create(:user, organization: @organization)
    # user has a card already on the queue
    @oldcard = create(:card, organization_id: @organization.id, author_id: @author.id)
    UserContentsQueue.enqueue_for_user(
        user_id: @user.id,
        content_id: @oldcard.id,
        content_type: 'Card',
        queue_timestamp: 1.hour.ago
    )
    Card.reindex
    UserContentsQueue.gateway.refresh_index!
  end

  teardown do
    WebMock.enable!
  end

  test 'remove cards on channel un-follow' do
    follow = create(:follow, user: @user, followable: @channel)
    UserContentsQueue.gateway.refresh_index!

    # cards from channel appear at front of queue
    queue = UserContentsQueue.get_cards_queue_for_user(user_id: @user.id, limit: 20)
    assert_equal 11, queue.count

    assert_equal @cards.map(&:id).sort, queue.first(10).map{|ucq| ucq.content_id}.sort

    # On unfollow channel all cards should be removed from UserContentQueue for @user
    follow.destroy
    UserContentsQueue.gateway.refresh_index!
    queue = UserContentsQueue.get_cards_queue_for_user(user_id: @user.id, limit: 20)
    assert_equal 1, queue.count
    assert_empty (@cards.map(&:id))&(queue.first(10).map{|ucq| ucq.content_id})
    assert_equal @oldcard.id, queue.last.content_id
  end

  test 'remove cards on user un-follow' do
    author = create(:user, organization: @organization)

    card = create(:card, author_id: author.id, organization_id: @organization.id)
    Search::CardSearch.
        any_instance.
        expects(:search).
        with(author, responds_with(:id, author.organization_id), viewer: @user, limit: 10, sort: :updated,
             filter_params: {card_type: Card::EXPOSED_TYPES, state: 'published'}, load: true).
        returns(Search::CardSearchResponse.new(results: [card]))

    # user follows author
    follow = create(:follow, user: @user, followable: author)
    UserContentsQueue.gateway.refresh_index!

    # cards from author appear at front of queue
    queue = UserContentsQueue.get_cards_queue_for_user(user_id: @user.id, limit: 20)
    assert_equal 2, queue.count
    #should not appear cards from author on unfollow
    follow.destroy
    UserContentsQueue.gateway.refresh_index!
    queue = UserContentsQueue.get_cards_queue_for_user(user_id: @user.id, limit: 20)
    assert_equal 1, queue.count
    assert_empty (@cards.map(&:id))&(queue.first(10).map{|ucq| ucq.content_id})
    assert_equal @oldcard.id, queue.last.content_id
  end
end

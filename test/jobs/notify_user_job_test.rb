require 'test_helper'

class NotifyUserJobTest < ActiveSupport::TestCase
  setup do
    @user = create(:user)
    @org = @user.organization
  end

  #added_to

  test 'should invoke notification generator job if user content completion present' do
    @user1 = create :user
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')
    cover.state = 'published'
    cover.save
    cover.reload
    cards = create_list(:card, 3, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end
    opts = {:action => 'added_to', :card_ids => cards.map(&:id), :cover_ids => [cover.id], actor_id: @user1.id}
    ucu = create(:user_content_completion, completable: cover, user: @user, state: 'completed')
    message = "View updates by #{@user1.first_name} on your completed pathway, '#{cover.title}'"

    NotificationGeneratorJob.expects(:perform_later)
                            .with({user_id: @user.id, item_id: cover.id, item_type: 'added_to', notification_type: 'added_to', custom_message: message})
                            .once

    NotifyUserJob.new.perform(opts)
  end

  test 'should not invoke notification generator job if user content completion not present' do
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')
    cover.state = 'published'
    cover.save
    cover.reload
    cards = create_list(:card, 3, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end
    opts = {:action => 'added_to', :card_ids => cards.map(&:id), :cover_ids => [cover.id]}

    NotificationGeneratorJob.expects(:perform_later)
                            .never
    NotifyUserJob.new.perform(opts)                          
  end

  test 'should not invoke notification generator job if card added is not accessible to the user' do
  	author = create(:user)
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: author.id, title: 'pack cover', is_public: false)
    cover.state = 'published'
    cover.save
    cover.reload
    cards = create_list(:card, 2, author_id: @user.id, card_type: 'media', is_public: false)
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end
    opts = {:action => 'added_to', :card_ids => cards.map(&:id), :cover_ids => [cover.id]}

    NotificationGeneratorJob.expects(:perform_later)
                            .never
    NotifyUserJob.new.perform(opts)                          
  end

  #removed_from
  test 'should invoke notification generator job if card is removed from pathway and user content completion is present' do
    @user1 = create :user
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover', is_public: false)
    cover.state = 'published'
    cover.save
    cover.reload
    cards = create_list(:card, 3, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end
    opts = {:action => 'removed_from', :card_ids => cards.map(&:id), :cover_ids => [cover.id], actor_id: @user1.id}
    ucu = create(:user_content_completion, completable: cover, user: @user, state: 'completed')
    message = "A SmartCard has been removed by #{@user1.first_name} from the completed pathway, '#{cover.title}'"

    NotificationGeneratorJob.expects(:perform_later)
                            .with({user_id: @user.id, item_id: cover.id, item_type: 'removed_from', notification_type: 'removed_from', custom_message: message})
                            .once
    NotifyUserJob.new.perform(opts)                          
  end

  #card_updated
  test 'should invoke notification generator job if pathway card is updated and user content completion is present' do
    @user1 = create :user
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover', is_public: false)
    cover.state = 'published'
    cover.save
    cover.reload
    cards = create_list(:card, 3, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end
    opts = {:action => 'card_updated', :card_ids => cards.map(&:id), :cover_ids => [cover.id], :actor_id => @user1.id}
    ucu = create(:user_content_completion, completable: cover, user: @user, state: 'completed')
    message = "View updates by #{@user1.first_name} on your completed pathway, '#{cover.title}'"
    NotificationGeneratorJob.expects(:perform_later)
                            .with({user_id: @user.id, item_id: cover.id, item_type: 'card_updated', notification_type: 'card_updated', custom_message: message})
                            .once
    NotifyUserJob.new.perform(opts)                          
  end
end
require 'test_helper'

class LmsAddOrganizationUsersJobTest < ActiveSupport::TestCase

  test 'should call LMS connector to add users to LMS' do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    organization = create :organization
    create_list :user, 2, organization: organization

    LmsIntegration::UserSettings.any_instance.expects(:setup_user).times(organization.users.count)

    LmsAddOrganizationUsersJob.perform_now(organization.id)
  end

end

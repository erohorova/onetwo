require 'test_helper'

class AssignmentDueNotificationJobTest < ActiveSupport::TestCase
  test 'should create batch of notifications for assignments' do
    AssignmentDueNotificationJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    org = create(:organization)
    user = create(:user, organization: org)
    card = create(:card, author_id: nil, organization: org)
    assignment = create(:assignment, assignee: user, assignable: card, due_at: (Time.current + 10.days))


    assert_difference -> {Notification.count}, 1 do
      AssignmentDueNotificationJob.perform_now(assignment.id)
    end

    assert_equal 'assignment_due_soon', user.notifications.last.notification_type
  end

  test 'should not create batch of notifications for assignments if user is suspended' do
    AssignmentDueNotificationJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    org = create(:organization)
    user = create(:user, organization: org)
    card = create(:card, author_id: nil, organization: org)
    assignment = create(:assignment, assignee: user, assignable: card, due_at: (Time.current + 10.days))
    user.update(is_suspended: true)

    assert_difference -> {Notification.count}, 0 do
      AssignmentDueNotificationJob.perform_now(assignment.id)
    end
  end
end
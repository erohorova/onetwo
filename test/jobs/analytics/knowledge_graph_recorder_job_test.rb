require 'test_helper'

class KnowledgeGraphRecorderJobTest < ActiveJob::TestCase
  test "should trigger KnowledgeGraphRecorder" do
    Analytics::KnowledgeGraphRecorder.expects(:run)
    Analytics::KnowledgeGraphRecorderJob.perform_now
  end
end

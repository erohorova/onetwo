require 'test_helper'

class Analytics::UserMetricsAggregationRecorderJobTest < ActiveJob::TestCase
  test "should trigger UserMetricsAggregationRecorder" do
    klass = Analytics::UserMetricsAggregationRecorder
    inst = klass.new
    klass.expects(:new).returns inst
    inst.expects(:run)
    Analytics::UserMetricsAggregationRecorderJob.perform_now
  end
end

require 'test_helper'

class Analytics::DeleteUserSearchHistoryJobTest < ActiveSupport::TestCase

  test 'perform' do
    user_id, org_id = 1, 2
    Analytics::DeleteUserSearchHistory.expects(:run!).with(
      user_id: user_id,
      org_id: org_id
    )
    Analytics::DeleteUserSearchHistoryJob.new.perform(
      user_id: user_id,
      org_id: org_id
    )
  end

end
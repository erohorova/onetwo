require 'test_helper'

class Analytics::ContentEngagementMetricsJobTest < ActiveSupport::TestCase

  test 'calls method to query and save to influx' do
    INFLUXDB_CLIENT.stubs :query
    Analytics::ContentEngagementInfluxQuery.expects(:run)
    Analytics::ContentEngagementMetricsJob.perform_now
  end

end
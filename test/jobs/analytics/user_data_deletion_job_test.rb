require 'test_helper'

class Analytics::UserDataDeletionJobTest < ActiveSupport::TestCase

  test 'perform' do
    user = User.new
    Analytics::UserDataDeletion.expects(:run!).with(user)
    Analytics::UserDataDeletionJob.new.perform(user: user)
  end

end
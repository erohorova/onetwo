require 'test_helper'

class GroupMetricsAggregationRecorderJobTest < ActiveJob::TestCase
  test "should trigger GroupScoreAggregationRecorder" do
    Analytics::GroupMetricsAggregationRecorder.expects(:run)
    Analytics::GroupMetricsAggregationRecorderJob.perform_now
  end
end

require 'test_helper'

class Analytics::UserDataExportJobTest < ActiveSupport::TestCase

  setup do
    @datetime = DateTime.now.utc
    DateTime.stubs(:now).returns @datetime
  end

  test "fires off user data export process and updates user in db - SUCCESS CASE" do
    org = create :organization
    user = create :user, organization: org, data_export_status: "started"
    Analytics::UserDataExport.expects(:data_export_exists?).with(user).returns true
    Analytics::UserDataExport.expects(:run).with(user)
    Analytics::UserDataExportJob.new.perform(user: user)
    user.reload
    assert_equal "done", user.data_export_status
    assert_equal @datetime.to_i, user.last_data_export_time.to_i
  end

  test "fires off user data export process and updates user in db - ERROR CASE" do
    org = create :organization
    user = create :user, organization: org, data_export_status: "started"
    Analytics::UserDataExport.expects(:data_export_exists?).with(user).returns false
    s3_key = "foo"
    Analytics::UserDataExport.expects(:run).with(user)
    Analytics::UserDataExportJob.new.perform(user: user)
    user.reload
    assert_equal "error", user.data_export_status
    assert_equal nil, user.last_data_export_time
  end

  test "logs an error message when an error is raised" do
    org = create :organization
    user = create :user, organization: org, data_export_status: "started"
    error = ArgumentError.new("foo")
    Analytics::UserDataExport
      .expects(:data_export_exists?)
      .with(user)
      .raises(error)
    Analytics::UserDataExport.expects(:run).with(user)
    Rails.logger.expects(:debug).with(error)
    error_raised = false
    begin
      Analytics::UserDataExportJob.new.perform user: user
    rescue => e
      assert_equal e, error
      error_raised = true
    end
    user.reload
    assert_equal "error", user.data_export_status
    assert_equal nil, user.last_data_export_time
    assert error_raised
  end


end
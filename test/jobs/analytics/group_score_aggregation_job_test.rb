require 'test_helper'

class GroupScoreAggregationJobTest < ActiveJob::TestCase
  test "should trigger GroupScoreAggregationRecorder" do
    Analytics::GroupScoreAggregationRecorder.expects(:run!)
    Analytics::GroupScoreAggregationJob.perform_now
  end
end

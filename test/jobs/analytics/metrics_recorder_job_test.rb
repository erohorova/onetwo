require 'test_helper'

class Analytics::MetricsRecorderJobTest < ActiveJob::TestCase

  setup do
    @class          = Analytics::MetricsRecorderJob
    @recorder_class = Analytics::CardMetricsRecorder
    @recorder_name  = "Analytics::CardMetricsRecorder"
    @args           = { foo: "bar" }

    InfluxdbRecorder.unstub :record
    EdcastActiveJob.unstub(:perform_later)
    Analytics::MetricsRecorderJob.unstub(:perform_later)
  end

  test "calls the record method on the recorder" do
    @recorder_class.expects(:record).with(@args).once
    @class.perform_now(recorder: @recorder_name, **@args)
  end

  test 'retries upon error, with a decremented retries_left counter' do
    @recorder_class.stubs(:record).raises InfluxDB::Error
    
    @class.expects(:enqueue_clone).with(
      wait_time: 30.seconds,
      recorder: @recorder_name,
      retries_left: 19,
      **@args
    )
    
    @class.perform_now(recorder: @recorder_name, **@args)
  end

  test 'stops retrying and raises the error if the counter reaches zero' do
    @recorder_class.stubs(:record).raises InfluxDB::Error
    
    @class.expects(:enqueue_clone).never
    log.expects(:error).with(
      "failed to write data to influx due to InfluxDB::Error. " +
      "recorder = #{@recorder_name}, args = #{@args}"
    )

    assert_raises InfluxDB::Error do
      @class.new.perform(recorder: @recorder_name, retries_left: 0, **@args)
    end
  end

end

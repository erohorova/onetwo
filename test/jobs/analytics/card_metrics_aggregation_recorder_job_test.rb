require 'test_helper'

class Analytics::CardMetricsAggregationRecorderJobTest < ActiveJob::TestCase
  test "should trigger CardMetricsAggregationRecorder" do
    klass = Analytics::CardMetricsAggregationRecorder
    inst = klass.new
    klass.expects(:new).returns inst
    inst.expects(:run)
    Analytics::CardMetricsAggregationRecorderJob.perform_now
  end
end

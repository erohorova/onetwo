require 'test_helper'

class BulkImportEmbargoesJobTest < ActiveJob::TestCase
  setup do
    BulkImportEmbargoesJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    @email_mock = mock('email')
    @email_mock.stubs(:deliver_now)

    @org = create(:organization)
    @admin_user = create(:user, organization_role: 'admin', organization: @org)
  end

  test 'should send email with message of processed CSV' do
    csv_rows = File.open("#{Rails.root}/test/fixtures/files/embargo.csv").read
    embargo_file = create(:embargo_file,
                          user_id: @admin_user.id,
                          organization_id: @org.id,
                          data: csv_rows)
    BulkImportEmbargoDetailsMailer.expects(:notify_admin).returns(@email_mock).once
    BulkImportEmbargoesJob.new.perform(file_id: embargo_file.id)
  end
end

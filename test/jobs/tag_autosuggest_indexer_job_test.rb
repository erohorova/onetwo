require 'test_helper'
class TagAutosuggestIndexerJobTest < ActiveSupport::TestCase

  test 'should call indexer' do
    tg = Tagging.new
    TagSuggest.expects(:index_for_autosuggest).with(tg).returns(nil).once
    TagAutosuggestIndexerJob.new.perform(tg)
  end

end

require 'test_helper'

class ChannelCollaboratorJobTest < ActiveJob::TestCase
  setup do
    @org = create(:organization)
    @role = @org.roles.where(name: Role::TYPE_CURATOR).first_or_create
    @channel = create(:channel, organization: @org)

    @users = create_list(:user, 2, organization: @org)
    @users.each { |user| user.add_role(Role::TYPE_CURATOR) }
    @author = create(:user, organization: @org)
    @channel.add_authors([@author])
  end

  test 'should add a collaborators to the channel' do
    Channel.any_instance.expects(:send_collaborator_invites).once
    User.any_instance.expects(:add_role).with('collaborator').twice
    assert_difference -> { @channel.authors.count }, 2 do
      ChannelCollaboratorsJob.perform_now(
        channel_id: @channel.id,
        role_ids: [@role.id],
        from_id: @author.id
      )
    end
  end

  test 'should add trusted collaborators to the channel' do
    Channel.any_instance.expects(:send_collaborator_invites).never
    User.any_instance.expects(:add_role).with('trusted_collaborator').twice
    assert_difference -> { @channel.trusted_authors.count }, 2 do
      ChannelCollaboratorsJob.perform_now(
        channel_id: @channel.id,
        role_ids: [@role.id],
        from_id: @author.id,
        as_trusted: true
      )
    end
  end
end

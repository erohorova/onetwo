require 'test_helper'

class RecurringJobs::RemoveOldFeedContentJobTest < ActiveJob::TestCase
  test "should call user contents queue func" do
    Timecop.freeze do
      UserContentsQueue.expects(:clear_docs_older_than).with(6.month.ago.to_i).returns(nil).once
      RecurringJobs::RemoveOldFeedContentJob.perform_now
    end
  end
end

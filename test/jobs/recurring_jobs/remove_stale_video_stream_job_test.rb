require 'test_helper'

class RecurringJobs::RemoveStaleVideoStreamJobTest < ActiveJob::TestCase
  test "it should update live stream to past" do
    WebMock.disable!
    video_streams = create_list(:iris_video_stream, 2, last_stream_at: 10.minutes.ago, status: 'live')
    RecurringJobs::RemoveStaleVideoStreamJob.perform_now

    video_streams.each {|vs| vs.reload}

    assert_equal 'past', video_streams.first.status
    assert_equal 'past', video_streams.last.status
    WebMock.enable!
  end
end

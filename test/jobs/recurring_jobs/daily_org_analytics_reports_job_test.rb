require 'test_helper'

class RecurringJobs::DailyOrgAnalyticsReportsJobTest < ActiveJob::TestCase
  setup do
    IndexJob.stubs(:perform_later)
    OrganizationAnalyticsJob.unstub :perform_later
    RecurringJobs::DailyOrgAnalyticsReportsJobTest.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    Organization.any_instance.unstub :create_analytics_reporting_configs
  end

  test 'should call daily org analytics job' do
    TestAfterCommit.with_commits do
      reporting_enabled_org = create(:organization, enable_analytics_reporting: true)
      reporting_disabled_org = create(:organization, enable_analytics_reporting: false)

      # default daily org analytics reporting is disabled
      config = reporting_enabled_org.configs.find_by_name('AnalyticsReportingConfig')
      config.update(value: (config.parsed_value.merge!(summary_feed: true)).to_json)

      OrganizationAnalyticsJob.expects(:perform_now).with(reporting_enabled_org.id)
      RecurringJobs::DailyOrgAnalyticsReportsJob.perform_now
    end
  end

  test 'should not call daily org analytics job' do
    TestAfterCommit.with_commits do
      reporting_enabled_org = create(:organization, enable_analytics_reporting: true)
      reporting_disabled_org = create(:organization, enable_analytics_reporting: false)

      # default daily org analytics reporting is disabled

      OrganizationAnalyticsJob.expects(:perform_now).with(reporting_enabled_org.id).never
      RecurringJobs::DailyOrgAnalyticsReportsJob.perform_now
    end
  end
end

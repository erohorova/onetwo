require 'test_helper'

class RecurringJobs::ArchiveExpiredCardsJobTest < ActiveJob::TestCase
  setup do
    organization = create :organization
    user      = create :user, organization: organization

    resource1 = create :resource, url: 'http://xyz.example.com/this-is-an-expired-url'
    resource2 = create :resource, url: 'http://xyz.example.com/this-is-a-redirected-working-url'
    resource3 = create :resource, url: 'http://xyz.example.com/this-is-a-working-url'

    @card1    = create :card, resource: resource1, author_id: nil, organization: organization
    @card2    = create :card, resource: resource2, author_id: nil, organization: organization
    @card3    = create :card, resource: resource3, author_id: nil, organization: organization
    @card4    = create :card, resource: resource1, author_id: user.id, organization: organization
    @card5    = create :card, resource: resource2, author_id: user.id, organization: organization
    @card6    = create :card, resource: resource3, author_id: user.id, organization: organization
    @card7    = create :card, resource: nil, author_id: user.id, organization: organization
    @card8    = create :card, resource: nil, author_id: user.id, organization: organization

    stub_request(:get, resource1.url).
      to_return(status: 404, :body => '<html><head></head>404<body></body></html>', headers: {})
    stub_request(:get, resource2.url).
      to_return(status: 301, :body => '<html><head></head>301<body></body></html>', headers: {})
    stub_request(:get, resource3.url).
      to_return(status: 200, :body => '<html><head></head>200<body></body></html>', headers: {})
    RecurringJobs::ArchiveExpiredCardsJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
  end

  test 'system-generated: archives card if resource URL throws 404' do
    assert @card1.published?

    RecurringJobs::ArchiveExpiredCardsJob.perform_now

    assert @card1.reload.archived?
  end

  test 'system-generated: does not archives card if resource URL throws 301 but has valid content i.e throws 301' do
    assert @card2.published?

    RecurringJobs::ArchiveExpiredCardsJob.perform_now

    refute @card2.reload.archived?
  end

  test 'system-generated: does not archives card if resource URL has valid content i.e throws 200' do
    assert @card3.published?

    RecurringJobs::ArchiveExpiredCardsJob.perform_now

    refute @card3.reload.archived?
  end

  test 'user-generated: does not archives user-generated cards' do
    assert @card4.published?
    assert @card5.published?
    assert @card6.published?

    RecurringJobs::ArchiveExpiredCardsJob.perform_now

    refute @card4.reload.archived?
    refute @card5.reload.archived?
    refute @card6.reload.archived?
  end

  test 'does not consider non-resource cards' do
    assert @card7.published?
    assert @card8.published?

    RecurringJobs::ArchiveExpiredCardsJob.perform_now

    refute @card7.reload.archived?
    refute @card8.reload.archived?
  end
end

require 'test_helper'

class RecurringJobs::CleanImportStatusJobTest < ActiveJob::TestCase
  setup do
    @org  = create :organization
    @user = create :user, organization: @org

    csv_rows = File.open("#{Rails.root}/test/fixtures/files/users_with_custom_fields.csv").read

    # Creating InvitationFile with UserImportStatus (more than 2.weeks)
    # Here, UserImportStatus  will be deleted.
    @invitation_file1 = create(:invitation_file, sender_id: @user.id,
      invitable: @org, data: csv_rows, roles: 'member', created_at: (Time.now - 3.weeks),
      parameter: { new_groups_count: 2, old_groups_count: 1 })

    user_import_status1 = create :user_import_status, invitation_file: @invitation_file1,
      first_name: 'FirstName1', last_name: 'LastName1', email: 'name1@example.com',
      status: 'pending', existing_user: false, created_at: (Time.now - 3.weeks)

    user_import_status2 = create :user_import_status, invitation_file: @invitation_file1,
      first_name: 'FirstName2', last_name: 'LastName2', email: 'name2@example.com',
      status: 'success', existing_user: false, created_at: (Time.now - 3.weeks)

    user_import_status3 = create :user_import_status, invitation_file: @invitation_file1,
      first_name: 'FirstName3', last_name: 'LastName3', email: 'name3@example.com',
      status: 'error', existing_user: false, created_at: (Time.now - 3.weeks)

    user_import_status4 = create :user_import_status, invitation_file: @invitation_file1,
      first_name: 'FirstName4', last_name: 'LastName4', email: 'name4@example.com',
      status: 'success', existing_user: true, created_at: (Time.now - 3.weeks)

    user_import_status5 = create :user_import_status, invitation_file: @invitation_file1,
      first_name: 'FirstName5', last_name: 'LastName5', email: 'name5@example.com',
      status: 'success', resent_email: true, created_at: (Time.now - 3.weeks)

    # Creating InvitationFile with UserImportStatus (less than 2.weeks)
    # Here, UserImportStatus  will not be deleted.
    @invitation_file2 = create(:invitation_file, sender_id: @user.id,
      invitable: @org, data: csv_rows, roles: 'member',
      parameter: { new_groups_count: 2, old_groups_count: 1 })

    user_import_status1 = create :user_import_status, invitation_file: @invitation_file2,
      first_name: 'FirstName1', last_name: 'LastName1', email: 'name1@example.com',
      status: 'pending', existing_user: false

    RecurringJobs::CleanImportStatusJob.perform_now

    @invitation_file1.reload
    @invitation_file2.reload
  end

  test 'captures #old_users_count' do
    assert_equal 1, @invitation_file1.parameter['old_users_count']
  end

  test 'captures #new_users_count' do
    assert_equal 4, @invitation_file1.parameter['new_users_count']
  end

  test 'captures #failed_users_count' do
    assert_equal 1, @invitation_file1.parameter['failed_users_count']
  end

  test 'captures #resent_emails_count' do
    assert_equal 1, @invitation_file1.parameter['resent_emails_count']
  end

  test 'captures #total_users_count' do
    assert_equal 5, @invitation_file1.parameter['total_users_count']
  end

  test 'do not clean up statuses that are less than 2 weeks' do
    refute_empty @invitation_file2.user_import_statuses
  end
end

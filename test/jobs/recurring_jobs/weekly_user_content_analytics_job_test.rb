require 'test_helper'

class RecurringJobs::SetupWeeklyUserContentAnalyticsJobTest < ActiveJob::TestCase
  setup do
    IndexJob.stubs(:perform_later)
    UserContentAnalyticsJob.unstub :perform_later
    RecurringJobs::WeeklyUserContentAnalyticsJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    Organization.any_instance.unstub :create_analytics_reporting_configs
  end

  test "Should invoke user content analytics job" do
    TestAfterCommit.with_commits do
      reporting_enabled_org = create(:organization, enable_analytics_reporting: true)
      reporting_disabled_org = create(:organization, enable_analytics_reporting: false)
      # content analytics config is enabled by default

      UserContentAnalyticsJob.expects(:perform_now).with(reporting_enabled_org.id)
      RecurringJobs::WeeklyUserContentAnalyticsJob.perform_now
    end
  end

  test "Should not invoke user content analytics job" do
    IndexJob.stubs(:perform_later)

    TestAfterCommit.with_commits do
      reporting_enabled_org = create(:organization, enable_analytics_reporting: true)
      reporting_disabled_org = create(:organization, enable_analytics_reporting: false)

      config = reporting_enabled_org.configs.find_by_name('AnalyticsReportingConfig')
      config.update(value: (config.parsed_value.merge!(content_activity_feed: false)).to_json)

      UserContentAnalyticsJob.expects(:perform_now).with(reporting_enabled_org.id).never
      RecurringJobs::WeeklyUserContentAnalyticsJob.perform_now
    end
  end
end

require 'test_helper'

class RecurringJobs::AssignmentPerformanceMetricsJobTest < ActiveJob::TestCase
  test "Should perform assignment metrics job" do
    org = create(:organization)
    user1, user2 = create_list(:user, 2, organization: org)
    card1, card2 = nil, nil
    Timecop.freeze(Time.now.utc - 1.days) do
      card1, card2 = create_list(:card, 2, author_id: nil, organization: org)

      create(:assignment, assignable: card1, assignee: user1)
      create(:assignment, assignable: card2, assignee: user2)
    end

    Timecop.freeze(Time.now.utc - 2.days) do
      card3, card4 = create_list(:card, 2, author_id: nil, organization: org)

      create(:assignment, assignable: card3, assignee: user1)
      create(:assignment, assignable: card4, assignee: user2)
    end

    # expect job to get called only twice
    AssignmentPerformanceMetricJob
      .expects(:perform_later)
      .with([card1.id, card2.id])
      .once

    RecurringJobs::AssignmentPerformanceMetricsJob.perform_now
  end
end

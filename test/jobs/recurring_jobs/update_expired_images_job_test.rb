require 'test_helper'

class RecurringJobs::UpdateExpiredImagesJobTest < ActiveJob::TestCase
  setup do
    WebMock.enable!
    @configured_organization = create :organization, host_name: Settings.org_hostnames_for_url_cleanup.first
    non_configured_organization = create :organization, host_name: 'nonconfiguredhost'
    @user      = create :user, organization: @configured_organization

    @expired_img_url = 'http://xyz.example.com/this-is-an-expired-url'
    @active_img_url = 'http://xyz.example.com/this-is-a-redirected-working-url'

    expired_img_resource_1 = create :resource, image_url: @expired_img_url
    expired_img_resource_2 = create :resource, image_url: @expired_img_url
    active_img_resource = create :resource, image_url: @active_img_url

    @card1    = create :card, resource: expired_img_resource_1, author_id: nil, organization: @configured_organization
    @card2    = create :card, resource: active_img_resource, author_id: @user.id, organization: @configured_organization
    @card3    = create :card, resource: expired_img_resource_2, author_id: @user.id, organization: @configured_organization
    @card4    = create :card, resource: expired_img_resource_2, author_id: nil, organization: non_configured_organization

    stub_request(:get, expired_img_resource_1.image_url).
      to_return(status: 404)
    stub_request(:get, expired_img_resource_2.image_url).
      to_return(status: 404)
    stub_request(:get, active_img_resource.image_url).
      to_return(status: 200)

    RecurringJobs::UpdateExpiredImagesJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
  end

  teardown do
    WebMock.disable!
  end

  test 'system-generated: updates image if resource image_url throws 404 for non-UGC cards only' do
    RecurringJobs::UpdateExpiredImagesJob.perform_now

    assert_not_equal @expired_img_url, @card1.resource.reload.image_url
  end

  test 'user-generated: should not update images even if expired if card is UGC' do
    RecurringJobs::UpdateExpiredImagesJob.perform_now

    assert_equal @active_img_url, @card2.resource.reload.image_url
    assert_equal @expired_img_url, @card3.resource.reload.image_url
  end

  test 'cleanup service is called only for ecl cards' do
    ecl_cards = create_list(:card, 2, author_id: nil, organization: @configured_organization)
    ugc_cards = create_list(:card, 4, author_id: @user.id, organization: @configured_organization)

    #should be called thrice including @card1 which also belongs to same organization and is an ecl card
    CardImageCleanupService.any_instance.expects(:check_and_update_card_images).times(3)
    RecurringJobs::UpdateExpiredImagesJob.perform_now
  end

  test 'should not update expired images of organizations for whom cleanup is not configured' do
    RecurringJobs::UpdateExpiredImagesJob.perform_now

    assert_equal @expired_img_url, @card4.resource.reload.image_url
  end

end

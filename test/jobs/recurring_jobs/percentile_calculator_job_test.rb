require 'test_helper'

class RecurringJobs::PercentileCalculatorJobTest < ActiveJob::TestCase
  test "call update percentile job" do
    Timecop.freeze do
      now = Time.now.utc
      org1, org2 = create_list(:organization, 2)
      periods_with_offsets = PeriodOffsetCalculator.applicable_offsets(now)

      Organization.exclude_default_org.each do |org|
        UpdatePercentileForOrgJob.expects(:perform_later).with(org.id, periods_with_offsets).returns(nil).once
      end
      RecurringJobs::PercentileCalculatorJob.perform_now
    end
  end
end

require 'test_helper'

class RecurringJobs::ChannelPinUpdateJobTest < ActiveJob::TestCase
  setup do
    RecurringJobs::ChannelPinUpdateJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    Organization.any_instance.unstub :create_general_channel

    @org1 = create(:organization)
    create_list(:channel, 2, organization_id: @org1.id, auto_pin_cards: true)
    @org2 = create(:organization)
    create_list(:channel, 2, organization_id: @org2.id)
    @org1.reload
  end

  test 'pin update method should be triggered only for channels that have the automated pin config enabled' do
    # for the 2 channels of org1
    Pin.expects(:update_pinned_for_channel).times(2)

    RecurringJobs::ChannelPinUpdateJob.perform_now
  end

end

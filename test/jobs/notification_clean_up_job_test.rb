require 'test_helper'

class NotificationCleanUpJobTest < ActiveJob::TestCase

  class AssignmentNotificationCleanUpTest < ActiveJob::TestCase
    setup do
      org = create(:organization)
      team = create(:team, organization: org)
      @causal_user, assignee = create_list(:user, 2, organization: org)
      card = create(:card, author: @causal_user)
      @assignment = create(:assignment, assignable: card, assignee: assignee)
      team_assignment = create(:team_assignment, team_id: team, assignor: @causal_user)
    end

    test 'should delete notification_entry if sourceable assignment destroyed' do
      EDCAST_NOTIFY.trigger_event(Notify::EVENT_NEW_ASSIGNMENT, @assignment)
      @assignment.destroy
      NotificationCleanUpJob.perform_now(id: @assignment.id, type: @assignment.class.name)
      notification_entry = NotificationEntry.where(sourceable: @assignment)

      assert_equal 0, notification_entry.count
    end


    test 'should delete notification if notifiable assignment destroyed' do
      @assignment.create_notification(causal_user_id: @causal_user.id, notification_type: 'assigned', message: "Assigned content")
      @assignment.destroy
      NotificationCleanUpJob.perform_now(id: @assignment.id, type: @assignment.class.name)
      notification = Notification.where(notifiable: @assignment)

      assert_equal 0, notification.count
    end
  end

  class CardNotificationCleanUpTest < ActiveJob::TestCase
    setup do
      @user = create(:user)
      @card = create(:card, author_id: @user.id, card_type: 'media')
    end

    test 'should delete notification_entry if sourceable card destroyed' do

      EDCAST_NOTIFY.trigger_event(Notify::EVENT_NEW_CARD, @card, {event: Card::EVENT_NEW_CARD_SHARE})
      @card.destroy
      NotificationCleanUpJob.perform_now(id: @card.id, type: @card.class.name)
      notification_entry = NotificationEntry.where(sourceable: @card)

      assert_equal 0, notification_entry.count
    end


    test 'should delete notification if notifiable card destroyed' do
      
      @card.notifications.create!(notifiable: @card, user: @user)
      @card.destroy
      NotificationCleanUpJob.perform_now(id: @card.id, type: @card.class.name)
      notification = Notification.where(notifiable: @card)

      assert_equal 0, notification.count
    end
  end

  class CommentNotificationCleanUpTest < ActiveJob::TestCase
    setup do
    @user = create(:user)
    @card = create(:card, author: @user)
    @comment = create(:answer, commentable: @card, user: @user)
    end

    test 'should delete notification_entry if sourceable comment destroyed' do
    
      EDCAST_NOTIFY.trigger_event(Notify::EVENT_NEW_COMMENT, @comment)
      @comment.destroy
      NotificationCleanUpJob.perform_now(id: @comment.id, type: @comment.class.name)
      notification_entry = NotificationEntry.where(sourceable: @comment)

      assert_equal 0, notification_entry.count
    end


    test 'should delete notification if notifiable comment destroyed' do
     
      @comment.notifications.create!(notifiable: @comment, user: @user)
      @comment.destroy
      NotificationCleanUpJob.perform_now(id: @comment.id, type: @comment.class.name)
      notification = Notification.where(notifiable: @comment)

      assert_equal 0, notification.count
    end
  end

end
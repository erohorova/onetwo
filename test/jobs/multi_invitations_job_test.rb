require 'test_helper'

class MultiInvitationsJobTest < ActiveSupport::TestCase
  test 'creates multiple invitation records' do
    user = create(:user)
    org  = create(:organization)

    args = {
      sender_id: user.id,
      invitable_id: org.id,
      invitable_type: org.class.name,
      emails: ["abc@example.com", "pqr@example.com", "xyz@example.com"],
      roles: "admin"
    }

    assert_difference -> {Invitation.count}, 3 do
      MultiInvitationsJob.perform_now(args)
    end

    # already sent invites
    Rails.logger.expects(:warn).times(3)
    assert_no_difference -> {Invitation.count} do
      MultiInvitationsJob.perform_now(args)
    end
  end
end
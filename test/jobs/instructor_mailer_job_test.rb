require 'test_helper'

class InstructorMailerJobTest < ActiveSupport::TestCase
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    # run mandrill-api in test mode
    # ref: https://bitbucket.org/mailchimp/mandrill-api-ruby/wiki/Home
    Excon.defaults[:mock] = true
    Excon.stub({}, {body: '{}', status: 200})
  end

  test 'add job to BatchJob table' do
    assert_difference -> { BatchJob.count } do
      InstructorMailerJob.perform_now
    end
  end
end

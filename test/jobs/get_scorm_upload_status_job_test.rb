require 'test_helper'

class GetScormUploadStatusJobTest < ActiveJob::TestCase

  setup do
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @scorm_card = create(:card, author: @user, organization: @organization, state: 'processing',
                filestack: [scorm_course: true,
                url: 'https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv'])
    @scorm_detail = create(:scorm_detail, status: 'running', token: 'randomtoken000', card: @scorm_card)
  end

  test 'should run scorm_upload_status_service' do
    ScormUploadStatusService.any_instance.expects(:run).with(@scorm_card.id).once
      GetScormUploadStatusJob.new.perform(@scorm_card.id)
  end
end
require 'test_helper'
class EdcastActiveJobTest < ActiveSupport::TestCase

  class DummyJob < EdcastActiveJob
    queue_as :dummy_queue
    def perform
    end
  end

  class JobCallingOtherJob < EdcastActiveJob
    queue_as :dummy_queue
    def perform
      DummyJob.perform_now key: "val"
    end
  end

  class JobCalledByOtherJob < EdcastActiveJob
    queue_as :dummy_queue
    def perform(*args)
      assert_equal({foo: "bar"}, RequestStore.read(:request_metadata))
      assert_equal([{key: "val"}], args)
    end
  end

  class DummyBadJob < EdcastActiveJob
    queue_as :dummy_queue
    def perform
      raise "Bad Job"
    end
  end

  class DummyJobWithOpts < EdcastActiveJob
    def perform opts
    end
  end

  setup do
    [
      DummyJob, DummyBadJob, DummyJobWithOpts, JobCallingOtherJob,
      JobCalledByOtherJob, EdcastActiveJob
    ].each do |klass|
      EdcastActiveJob::WHITELISTED_JOB_CLASS << klass.name
      klass.unstub :perform_later
    end
  end

  test 'preserves RequestStore state' do
    RequestStore.store[:request_metadata] = { foo: "bar" }
    JobCallingOtherJob.perform_now
    # See JobCalledByOtherJob#perform for expectations
  end

  test 'should log finished job state' do
    skip
    assert_difference ->{BackgroundJob.count} do
      DummyJob.perform_later
    end

    bk_job = BackgroundJob.last
    assert_equal "dummy_queue", bk_job.job_queue
    assert_equal DummyJob.name, bk_job.job_class
    assert_not_nil bk_job.job_id
    assert_equal "finished", bk_job.status
    assert_not_nil bk_job.enqueued_at
    assert_not_nil bk_job.started_at
    assert_not_nil bk_job.finished_at
  end

  test 'should log error on exception' do
    Rails.logger.expects(:error).once
    assert_difference ->{BackgroundJob.count} do
      DummyBadJob.perform_later
    end

    bk_job = BackgroundJob.last
    assert_equal "dummy_queue", bk_job.job_queue
    assert_equal DummyBadJob.name, bk_job.job_class
    assert_not_nil bk_job.job_id
    assert_equal "error", bk_job.status
    assert_not_nil bk_job.enqueued_at
    assert_not_nil bk_job.started_at
    assert_not_nil bk_job.finished_at
  end

  test 'ensure arguments are saved in the logs' do
    metadata = { foo: "bar" }
    RequestStore.expects(:read).with(:request_metadata).returns metadata
    assert_difference ->{BackgroundJob.count} do
      DummyJobWithOpts.perform_later({"id" => 1}, 2)
    end
    bk_job = BackgroundJob.last
    assert_equal(
      [{"id" => 1}, 2, { request_store_data: metadata }],
      ActiveJob::Arguments.deserialize(JSON.parse(bk_job.job_arguments))
    )
  end

  test 'ensure long arguments are truncated' do
    assert_difference ->{BackgroundJob.count} do
      DummyJobWithOpts.perform_later({"id" => "0"*100000}, 2)
    end

    bk_job = BackgroundJob.last
    assert bk_job.truncated
  end

  test 'ensure active record models are serialized' do
    u = create(:user)
    metadata = { foo: "bar" }
    RequestStore.expects(:read).with(:request_metadata).returns metadata
    assert_difference ->{BackgroundJob.count} do
      DummyJobWithOpts.perform_later(user: u)
    end

    bk_job = BackgroundJob.last
    assert_equal(
      [{user: u}, { request_store_data: metadata }],
      ActiveJob::Arguments.deserialize(JSON.parse(bk_job.job_arguments))
    )
  end

  test 'should be able to override handle exception' do
    class DummyClass
      def self.dummy_function
      end
    end

    class DummyJobWithErrorHandler < EdcastActiveJob
      queue_as :dummy_queue
      def perform
        raise "Bad Job"
      end

      def handle_error exception
        DummyClass.dummy_function
        super
      end
    end

    EdcastActiveJob::WHITELISTED_JOB_CLASS << "EdcastActiveJobTest::DummyJobWithErrorHandler"

    DummyClass.expects(:dummy_function).once
    assert_difference ->{BackgroundJob.count} do
      DummyJobWithErrorHandler.perform_later
    end

    bk_job = BackgroundJob.last
    assert_equal "dummy_queue", bk_job.job_queue
    assert_equal DummyJobWithErrorHandler.name, bk_job.job_class
    assert_not_nil bk_job.job_id
    assert_equal "error", bk_job.status
    assert_not_nil bk_job.enqueued_at
    assert_not_nil bk_job.started_at
    assert_not_nil bk_job.finished_at
  end

  test 'must include lrs job as whitelisted class' do
    assert_equal(
      true, EdcastActiveJob::WHITELISTED_JOB_CLASS.include?('RecurringJobs::LrsPullJob')
    )
  end
end

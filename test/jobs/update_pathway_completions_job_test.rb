require 'test_helper'

class UpdatePathwayCompletionsJobTest < ActiveJob::TestCase

  setup do
    @user = create(:user)
    @org = @user.organization
    @cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')
    @cards = create_list(:card, 3, author_id: @user.id, card_type: 'media')
  end

  test 'should recalculate pathway completions after adding new cards' do
    new_cards = create_list(:card, 3, author_id: @user.id, card_type: 'media')
    @cards.each do |card|
      @cover.add_card_to_pathway(card.id, 'Card')
      create(
        :user_content_completion, completable: card, user: @user,
        state: 'completed', completed_percentage: 100
      )
    end

    p_ucc = create(
      :user_content_completion, completable: @cover, user: @user,
      state: 'completed', completed_percentage: 100
    )

    new_cards.each do |card|
      @cover.add_card_to_pathway(card.id, 'Card')
    end

    UpdatePathwayCompletionsJob.perform_now(
      cover_id: @cover, org_id: @org.id
    )

    p_ucc.reload
    assert_equal 50, p_ucc.completed_percentage
  end
end

require 'test_helper'
require './clock.rb'

class ClockTest < ActiveSupport::TestCase

  test "it calls the expected jobs" do

    timezone = "America/Los_Angeles"

    Clockwork.expects(:schedule_job).with 10.minutes,
      RecurringJobs::RemoveStaleVideoStreamJob

    Clockwork.expects(:schedule_job).with 3.hour,
      RecurringJobs::GroupStatsBatchJob,
      at: "**:01"

    Clockwork.expects(:schedule_job).with 1.hour,
      RecurringJobs::UpdateVideoStreamScoreJob,
      tz: timezone

    Clockwork.expects(:schedule_job).with 1.week,
      RecurringJobs::WeeklyUserContentAnalyticsJob,
      at: 'Monday 9:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 1.week,
      RecurringJobs::WeeklySocialAnalyticsReportJob,
      at: 'Monday 9:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 1.day,
      RecurringJobs::DailyOrgAnalyticsReportsJob,
      at: '9:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 1.day,
      RecurringJobs::PullScormResultsJob,
      at: '05:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 1.day,
      RecurringJobs::FollowBatchEmailGeneratorJob,
      at: '02:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 1.day,
      DailyDigestEmailGeneratorJob,
      at: '21:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 1.day,
      InstructorMailerJob,
      at: '8:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 1.day,
      InstructorDailyDigestBatchReportJob,
      at: '8:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 1.day,
      RecurringJobs::PromoteStreamsJob,
      at: '05:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 1.day,
      RecurringJobs::SetupInviteReminderJob,
      at: '9:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 1.day,
      RecurringJobs::RemindCuratorDailyJob,
      at: '9:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 1.day,
      RecurringJobs::RemoveOldBackgroundJobsLogsJob,
      at: '13:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 1.week,
      RecurringJobs::RemoveOldFeedContentJob,
      at: 'Wednesday 13:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 1.day,
      RecurringJobs::PercentileCalculatorJob,
      at: '3:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 1.day,
      RecurringJobs::ArchiveExpiredCardsJob,
      at: '20:00',
      tz: timezone,
      if: Clockwork::IsFirstOfMonth

    Clockwork.expects(:schedule_job).with 1.week,
      RecurringJobs::UpdateExpiredImagesJob,
      at: '10:00',
      tz: timezone

   Clockwork.expects(:schedule_job).with 1.day,
      RecurringJobs::AssignmentPerformanceMetricsJob,
      at: '2:30',
      tz: timezone

    Clockwork.expects(:schedule_job).with 2.hour,
      SendDailyDigestJob

    Clockwork.expects(:schedule_job).with 1.day,
      RecurringJobs::SendNewClcNotification,
      at: '00:30',
      tz: timezone

    Clockwork.expects(:schedule_job).with 6.hours,
      EclSourceFetchRunnerJob

    Clockwork.expects(:schedule_job).with 6.hours,
      ChannelEclSourcesFetchBatchJob

    Clockwork.expects(:schedule_job).with 1.day,
      ChannelProgrammingBatchJob,
      at: '00:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 1.hour,
      RecurringJobs::ChannelPinUpdateJob

    Clockwork.expects(:schedule_job).with 15.minutes,
      RecurringJobs::LrsPullJob

    Clockwork.expects(:schedule_job).with 1.day,
      Analytics::GroupScoreAggregationJob,
      at: '2:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 1.day,
      Analytics::ContentEngagementMetricsJob,
      at: '3:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 1.day,
      Analytics::ChannelMetricsAggregationRecorderJob,
      at: '4:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 1.day,
      Analytics::CardMetricsAggregationRecorderJob,
      at: '5:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 1.day,
      Analytics::UserMetricsAggregationRecorderJob,
      at: '6:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 1.day,
      Analytics::GroupMetricsAggregationRecorderJob,
      at: '8:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 2.weeks,
      RecurringJobs::CleanImportStatusJob,
      at: '10:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 1.day,
      Analytics::KnowledgeGraphRecorderJob,
      at: '7:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 1.day,
      RecurringJobs::AddUntranslatedStringsJob,
      at: '1:00',
      tz: timezone

    Clockwork.expects(:schedule_job).with 5.minutes,
      WorkflowsPatcherJob

    Clockwork.start_jobs
  end

end

require 'test_helper'

class PubnubNotifyChangeJobTest < ActiveSupport::TestCase

  setup do
  end

  test 'should call pubnub' do
    u, f = create_list(:user, 2)
    action = 'blah'
    # f decides to follow u
    payload = {
      pn_apns: {
        aps: {
          "content-available" => 1
        },
        action: action,
        channel_name: u.public_pubnub_channel_name
      },
      pn_gcm: {
        data: {
          action: action,
          channel_name: u.public_pubnub_channel_name
        }
      }
    }
    channel_names = [f.private_pubnub_channel_name]
    PubnubNotifier
      .expects(:send_payload)
      .with(payload, channel_names, organization: f.organization)
      .once
    PubnubNotifyChangeJob.new.perform(action, f, u.public_pubnub_channel_name)
  end

end

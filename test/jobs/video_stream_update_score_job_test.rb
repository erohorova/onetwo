require 'test_helper'

class VideoStreamUpdateScoreJobTest < ActiveJob::TestCase
  test "job runs and calls update score" do
    WebMock.disable!
    video_stream = create(:wowza_video_stream)
    video_stream.expects(:update_score)
    VideoStreamUpdateScoreJob.perform_now(video_stream)
    WebMock.enable!
  end
end

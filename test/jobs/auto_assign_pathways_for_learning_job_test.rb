require 'test_helper'

class AutoAssignPathwaysForLearningJobTest < ActiveJob::TestCase
  test 'call method for auto assign recommended pathways to user' do
    user = create(:user)
    AutoRecommendCardService.any_instance
      .expects(:auto_assign_recommended_cards_to_user).once
    AutoAssignPathwaysForLearningJob.perform_now(user.id)
  end
end

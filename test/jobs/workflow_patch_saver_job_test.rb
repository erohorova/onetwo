require 'test_helper'

class WorkflowPatchSaverJobTest < ActiveJob::TestCase
  include ActiveJob::TestHelper

  setup do
    WorkflowPatchSaverJob.unstub(:perform_later)
    EdcastActiveJob.unstub(:perform_later)

    @org = create(:organization)
    @user = create(:user, organization: @org)
    @config = @org.configs.create(
      name: 'workflow_service', value: true, data_type: 'boolean'
    )
  end

  test 'should properly enqueue patch saver job' do
    assert_enqueued_with(job: WorkflowPatchSaverJob, queue: 'default') do
      Profile.create(
        user_id: @user.id, organization_id: @org.id
      ).committed!
    end
  end

  test 'created entry should contain proper data_id' do
    profile = Profile.create(
      user_id: @user.id, organization_id: @org.id
    )
    @user.committed!
    profile.committed!
    data = @user.send(:detect_upsert, {import: true})
    assert data.present?
    WorkflowPatchSaverJob.perform_now(org_id: @org.id, data: data)

    assert_equal @user.email, WorkflowPatchRevision.last.data_id
  end
end

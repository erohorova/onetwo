require 'test_helper'

class UnsyncCloneChannelJobTest < ActiveSupport::TestCase
  test "should remove clone channel, clone cards entries when parent channel becomes unshareable" do
    TestAfterCommit.enabled = true
    org1, org2 = create_list(:organization, 2)
    user = create(:user, organization: org1)
    org1_channel = create(:channel, organization: org1, shareable: true)
    org2_channel = create(:channel, organization: org2, parent_id: org1_channel.id)
    
    # created smartcard and added to org1_channel
    card = create(:card, card_type: 'media', card_subtype: 'text', author_id: user.id, title: 'This is card', 
      message: 'This is message', organization: org1, ecl_source_name: 'UGC')
    card.current_user = user
    card.channels << org1_channel

    # created pathway and added to org1_channel
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: user.id, title: 'pack cover', 
      organization: org1, ecl_source_name: 'UGC')
    cards = create_list(:card, 2, author_id: user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end
    cover.current_user = user
    cover.channels << org1_channel

    CloneChannelCardsJob.perform_now(org1_channel.id, org2_channel.id)
    org1_channel.update(shareable: false)
    UnsyncCloneChannelJob.perform_now({parent_channel_id: org1_channel.id, action: 'remove'})

    assert_equal 0, org1_channel.cloned_channels.count
    assert_equal 0, card.cloned_cards.count
    assert_equal 0, cover.cloned_cards.count
  end
end
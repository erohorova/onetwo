require 'test_helper'

class EclSourceFetchRunnerJobTest < ActiveJob::TestCase
  setup do
    EclSourceFetchRunnerJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
  end

  test 'source with null next_run_at is scheduled' do
    now = Time.zone.now

    ecl_source = create :ecl_source
    ecl_source.update(next_run_at: nil)

    Timecop.freeze(now) do
      assert_enqueued_with(job: EclSourceFetchJob, args: [ecl_source.id]) do
        EclSourceFetchRunnerJob.perform_now
      end
    end

    ecl_source.reload
    assert_equal (now + 1.hour).to_s, ecl_source.next_run_at.to_s
  end

  test 'source with next_run_at less than now is scheduled' do
    now = Time.zone.now

    ecl_source = create :ecl_source, next_run_at: now - 10.minutes

    Timecop.freeze(now) do
      assert_enqueued_with(job: EclSourceFetchJob, args: [ecl_source.id]) do
        EclSourceFetchRunnerJob.perform_now
      end
    end

    ecl_source.reload
    assert_equal (now + 1.hour).to_s, ecl_source.next_run_at.to_s
  end

  test 'source with next_run_at greater than now is NOT scheduled' do
    now = Time.zone.now

    ecl_source = create :ecl_source, next_run_at: now + 10.minutes

    assert_no_enqueued_jobs do
      EclSourceFetchRunnerJob.perform_now
    end

    ecl_source.reload
    assert_equal (now + 10.minute).to_s, ecl_source.next_run_at.to_s
  end

  test 'only source of organization with config channel_programming_v2 = OFF is scheculed' do
    org1 = create(:organization)
    org2 = create(:organization)

    channel1 = create(:channel, organization: org1)
    channel2 = create(:channel, organization: org2)

    ecl_source1 = create(:ecl_source, channel: channel1, next_run_at: nil)
    ecl_source2 = create(:ecl_source, channel: channel2, next_run_at: nil)

    ecl_source1.update(next_run_at: nil)
    ecl_source2.update(next_run_at: nil)

    create(:config, category: 'settings',
                    name: 'channel_programming_v2',
                    data_type: 'boolean',
                    value: 'true',
                    configable_id: org1.id,
                    configable_type: 'Organization')

    now = Time.zone.now

    Timecop.freeze(now) do
      assert_enqueued_with(job: EclSourceFetchJob, args: [ecl_source2.id]) do
        EclSourceFetchRunnerJob.perform_now
      end
    end

    ecl_source2.reload
    assert_equal (now + 1.hour).to_s, ecl_source2.next_run_at.to_s
    assert_nil ecl_source1.next_run_at
  end
end

require 'test_helper'

class UserJobTest < ActiveSupport::TestCase

  self.use_transactional_fixtures = false
  self.use_shared_db_connection = false

  setup do
    skip
    @org = create(:organization)
    User.any_instance.unstub :generate_images
    User.any_instance.stubs(:reindex_async).returns(true)
    AnonImageJob.unstub(:perform_later)
    AvatarImageJob.unstub(:perform_later)
    EdcastActiveJob.unstub :perform_later
  end

  test 'should create the anonimage in the background' do
    user = User.new(password: User.random_password, organization: @org, organization_role: 'member')
    user.save!
    user.reload
    assert user.anonimage.present?
    assert 'image/png', user.anonimage_content_type
  end

  test 'should create avatar image from picture url in the background' do

    old_image_stub = stub_request(:get, "http://www.example.com/user/picture.jpg").
    with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
    to_return(:status => 200, :body => File.read(Rails.root.join('test/fixtures/images/user2.jpg')), :headers => {content_type:'image/jpg'})


    new_image_stub = stub_request(:get, "http://www.example.com/user/new_picture.jpg").
    with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
    to_return(:status => 200, :body => File.read(Rails.root.join('test/fixtures/images/user2.jpg')), :headers => {content_type:'image/jpg'})

    user = User.new(password: User.random_password, picture_url: 'http://www.example.com/user/picture.jpg', organization: @org, organization_role: 'member')
    user.save!
    user.reload
    assert user.avatar.present?
    assert 'image/jpg', user.avatar_content_type



    user = User.find(user.id)
    user.picture_url = 'http://www.example.com/user/picture.jpg'
    user.save! #picture url has not change, should not resubmit the upload job

    user = User.find(user.id)
    user.picture_url = 'http://www.example.com/user/new_picture.jpg'
    user.save!

    assert_requested old_image_stub, times: 1
    assert_requested new_image_stub, times: 1

  end

end

require 'test_helper'

class WorkflowsJobTest < ActiveJob::TestCase
  setup do
    @org = create(:organization)
    @user1, @user2, @user3 = create_list(:user, 3, organization: @org)
    @prepared_group_patches = [
      {
        "patchId"=>"d30ad521-ee35-11e8-8bb5-9564013b0b8d",
        "patchType"=>"insert",
        "data"=>{
          "id"=> @user1.id.to_s,
          "group_id"=>"England",
          "group_name"=>"England",
          "group_type"=>"England",
          "group_description"=>"description"
        },
        "meta"=>{
          "historyRevisionVersion"=>1,
          "historyRevisionDatetime"=>1542877557000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>1,
          "outputSourceId"=>1
        }
      },
      {
        "patchId"=>"d30afc38-ee35-11e8-8bb5-9564013b0b8d",
        "patchType"=>"insert",
        "data"=>{
          "id"=> @user2.id.to_s,
          "group_id"=>"England",
          "group_name"=>"England",
          "group_type"=>"England",
          "group_description"=>"description"
        },
        "meta"=>{
          "historyRevisionVersion"=>1,
          "historyRevisionDatetime"=>1542877557000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"groups",
        "orgName"=>@org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>1,
          "outputSourceId"=>1
        }
      },
      {
        "patchId"=>"d40afc3b-ee35-11e8-8bb5-9564013b0b8d",
        "patchType"=>"insert",
        "data"=>{
          "id"=> @user3.id.to_s,
          "group_id"=>"England",
          "group_name"=>"England",
          "group_type"=>"England",
          "group_description"=>"description"
        },
        "meta"=>{
          "historyRevisionVersion"=>1,
          "historyRevisionDatetime"=>1542877557000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>1,
          "outputSourceId"=>1
        }
      },
      {
        "patchId"=>"07ae41c0-ee38-11e8-8bb5-9564013b0b8d",
        "patchType"=>"update",
        "data"=>{
          "id"=> @user3.id.to_s,
          "group_id"=>"Russia",
          "group_name"=>"Russia",
          "group_type"=>"Russia",
          "group_description"=>"description"
        },
        "meta"=>{
          "historyRevisionVersion"=>2,
          "historyRevisionDatetime"=>1542878504000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>1,
          "outputSourceId"=>1
        }
      },
      {
        "patchId"=>"1ef96440-ee38-11e8-8bb5-9564013b0b8d",
        "patchType"=>"delete",
        "data"=>{
          "id"=> @user1.id.to_s,
          "group_id"=>"England",
          "group_name"=>"England",
          "group_type"=>"England",
          "group_description"=>"description"
        },
        "meta"=>{
          "historyRevisionVersion"=>2,
          "historyRevisionDatetime"=>1542878544000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>1,
          "outputSourceId"=>1
        }
      },
      {
        "patchId"=>"1ef98b57-ee38-11e8-8bb5-9564013b0b8d",
        "patchType"=>"delete",
        "data"=>{
          "id"=> @user2.id.to_s,
          "group_id"=>"England",
          "group_name"=>"England",
          "group_type"=>"England",
          "group_description"=>"description"
        },
        "meta"=>{
          "historyRevisionVersion"=>2,
          "historyRevisionDatetime"=>1542878544000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>1,
          "outputSourceId"=>1
        }
      },
      {
        "patchId"=>"1ef98b5a-ee38-11e8-8bb6-9564013b0b8d",
        "patchType"=>"delete",
        "data"=>{
          "id"=> @user3.id.to_s,
          "group_id"=>"Russia",
          "group_name"=>"Russia",
          "group_type"=>"Russia",
          "group_description"=>"description"
        },
        "meta"=>{
          "historyRevisionVersion"=>3,
          "historyRevisionDatetime"=>1542878544000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>1,
          "outputSourceId"=>1
        }
      }
    ]
    @prepared_users_groups_patches = [
      {
        "patchId"=>"5fb61d70-ee38-11e8-8bb5-9564013b0b8d",
        "patchType"=>"insert",
        "data"=>{
          "id"=>"fc2554ad-7422-4121-9cc1-93d67fd10a93",
          "group_id"=>nil,
          "group_name"=>"dynamic_group_by_city",
          "group_type"=>nil,
          "group_description"=>"Group created using Workflow"
        },
        "meta"=>{
          "historyRevisionVersion"=>1,
          "historyRevisionDatetime"=>1542878652000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>593,
          "outputSourceId"=>692
        }
      },
      {
        "patchId"=>"61018930-ee38-11e8-8bb5-9564013b0b8d",
        "patchType"=>"insert",
        "data"=>{
          "id"=> @user1.id.to_s,
          "group_id"=>"fc2554ad-7422-4121-9cc1-93d67fd10a93",
          "member_type"=>"some_constant_xyz"
        },
        "meta"=>{
          "historyRevisionVersion"=>1,
          "historyRevisionDatetime"=>1542878654000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"users_groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>681,
          "outputSourceId"=>660
        }
      },
      {
        "patchId"=>"61018931-ee38-11e8-8bb5-9564013b0b8d",
        "patchType"=>"insert",
        "data"=>{
          "id"=> @user2.id.to_s,
          "group_id"=>"fc2554ad-7422-4121-9cc1-93d67fd10a93",
          "member_type"=>"some_constant_xyz"
        },
        "meta"=>{
          "historyRevisionVersion"=>1,
          "historyRevisionDatetime"=>1542878654000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"users_groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>681,
          "outputSourceId"=>660
        }
      },
      {
        "patchId"=>"c4eee550-ee38-11e8-8bb5-9564013b0b8d",
        "patchType"=>"delete",
        "data"=>{
          "id"=> @user1.id.to_s,
          "group_id"=>"fc2554ad-7422-4121-9cc1-93d67fd10a93",
          "member_type"=>"some_constant_xyz"
        },
        "meta"=>{
          "historyRevisionVersion"=>2,
          "historyRevisionDatetime"=>1542878822000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"users_groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>681,
          "outputSourceId"=>660
        }
      },
      {
        "patchId"=>"c4eee551-ee38-11e8-8bb5-9564013b0b8d",
        "patchType"=>"delete",
        "data"=>{
          "id"=> @user2.id.to_s,
          "group_id"=>"fc2554ad-7422-4121-9cc1-93d67fd10a93",
          "member_type"=>"some_constant_xyz"
        },
        "meta"=>{
          "historyRevisionVersion"=>2,
          "historyRevisionDatetime"=>1542878822000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"users_groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>681,
          "outputSourceId"=>660
        }
      },
      {
        "patchId"=>"c69d58a0-ee38-11e8-8bb5-9564013b0b8d",
        "patchType"=>"delete",
        "data"=>{
          "id"=>"fc2554ad-7422-4121-9cc1-93d67fd10a93",
          "group_id"=>nil,
          "group_name"=>"dynamic_group_by_city",
          "group_type"=>nil,
          "group_description"=>"Group created using Workflow"
        },
        "meta"=>{
          "historyRevisionVersion"=>2,
          "historyRevisionDatetime"=>1542878825000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>593,
          "outputSourceId"=>692
        }
      }
    ]
    @group_patches_diff_workflow = [
      {
        "patchId"=>"d30ad521-ee35-11e8-8bb5-9564013b0b8d",
        "patchType"=>"insert",
        "data"=>{
          "id"=> @user1.id.to_s,
          "group_id"=>"England",
          "group_name"=>"England",
          "group_type"=>"England",
          "group_description"=>"description"
        },
        "meta"=>{
          "historyRevisionVersion"=>1,
          "historyRevisionDatetime"=>1542877557000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>1,
          "outputSourceId"=>1
        }
      },
      {
        "patchId"=>"d31afc38-ee35-11e8-8bb5-9564013b0b9d",
        "patchType"=>"insert",
        "data"=>{
          "id"=> @user1.id.to_s,
          "group_id"=>"London",
          "group_name"=>"London",
          "group_type"=>"London",
          "group_description"=>"description"
        },
        "meta"=>{
          "historyRevisionVersion"=>1,
          "historyRevisionDatetime"=>1542877557000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"groups",
        "orgName"=>@org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>2,
          "outputSourceId"=>2
        }
      },
      {
        "patchId"=>"1ef98b57-ee38-11e8-8bb5-9564013b0b8d",
        "patchType"=>"delete",
        "data"=>{
          "id"=> @user1.id.to_s,
          "group_id"=>"England",
          "group_name"=>"England",
          "group_type"=>"England",
          "group_description"=>"description"
        },
        "meta"=>{
          "historyRevisionVersion"=>2,
          "historyRevisionDatetime"=>1542878544000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>1,
          "outputSourceId"=>1
        }
      },
      {
        "patchId"=>"2ef98b5a-ee38-11e8-8bb5-9564013b0b8d",
        "patchType"=>"delete",
        "data"=>{
          "id"=> @user1.id.to_s,
          "group_id"=>"London",
          "group_name"=>"London",
          "group_type"=>"London",
          "group_description"=>"description"
        },
        "meta"=>{
          "historyRevisionVersion"=>2,
          "historyRevisionDatetime"=>1542878544000,
          "historyRevisionDataJson"=>{

          }
        },
        "workflowType"=>"groups",
        "orgName"=> @org.host_name,
        "params"=>{

        },
        "models"=>{
          "workflowId"=>2,
          "outputSourceId"=>2
        }
      }
    ]
  end

  class GroupsPatchesTest < WorkflowsJobTest
    setup do
      # prepare all needed WorkflowPatchRevisions
      WorkflowPatchRevisionsJob.perform_now(patches: @prepared_group_patches)
    end

    # groups insert/update/delete
    test 'groups processing regular workflow' do
      insert_patches = WorkflowPatchRevision.where(patch_type: 'insert')
      insert_patches.each do |patch|
        WorkflowsJob.perform_now(patch_id: patch.id)
      end
      expected_team = Team.find_by(name: 'England')

      assert expected_team
      assert expected_team.is_mandatory
      assert expected_team.is_private
      assert expected_team.is_dynamic
      assert_same_elements [@user1.id, @user2.id, @user3.id], expected_team.teams_users.pluck(:user_id)

      update_patch = WorkflowPatchRevision.find_by(patch_type: 'update')
      WorkflowsJob.perform_now(patch_id: update_patch.id)

      expected_team.reload
      expected_new_team = Team.find_by(name: 'Russia')

      assert_same_elements [@user1.id, @user2.id], expected_team.teams_users.pluck(:user_id)
      assert expected_new_team
      assert_same_elements [@user3.id], expected_new_team.teams_users.pluck(:user_id)

      delete_patches = WorkflowPatchRevision.where(patch_type: 'delete')
      delete_patches.each do |patch|
        WorkflowsJob.perform_now(patch_id: patch.id)
      end

      expected_team.reload
      expected_new_team.reload

      assert_same_elements [@user1.id, @user2.id], expected_team.teams_users.pluck(:user_id)
      assert_same_elements [@user3.id], expected_new_team.teams_users.pluck(:user_id)

      assert expected_team
      assert expected_new_team
      assert expected_team.is_private
      assert expected_new_team.is_private

      refute expected_team.is_mandatory
      refute expected_team.is_dynamic
      refute expected_new_team.is_mandatory
      refute expected_new_team.is_dynamic
    end

    # same user in context of different workflows
    # groups insert/delete
    test 'groups processing same user in context of different workflows' do
      # EP-20905 details
      WorkflowPatchRevisionsJob.perform_now(patches: @group_patches_diff_workflow)
      insert_patches = WorkflowPatchRevision.where(patch_type: 'insert').last(2)
      insert_patches.each do |patch|
        WorkflowsJob.perform_now(patch_id: patch.id)
      end
      expected_team1 = Team.find_by(name: 'England')
      expected_team2 = Team.find_by(name: 'London')

      assert expected_team1
      assert expected_team2
      assert_same_elements [@user1.id], expected_team2.teams_users.pluck(:user_id)
      assert_same_elements [@user1.id], expected_team1.teams_users.pluck(:user_id)

      delete_patch = WorkflowPatchRevision.where(patch_type: 'delete').last(1).first
      group_name = delete_patch.data['group_name']
      WorkflowsJob.perform_now(patch_id: delete_patch.id)


      delete_patch_target_team = Team.find_by(name: group_name)
      second_team = Team.where.not(id: delete_patch_target_team.id).first

      assert delete_patch_target_team
      assert second_team
      assert_same_elements [@user1.id], second_team.teams_users.pluck(:user_id)
      assert_same_elements [@user1.id], delete_patch_target_team.teams_users.pluck(:user_id)

      refute delete_patch_target_team.is_dynamic
      refute delete_patch_target_team.is_mandatory

      assert second_team.is_dynamic
      assert second_team.is_mandatory
    end
  end

  class DynamicSelectionPatchesTest < WorkflowsJobTest

    setup do
      @admin = create(:user, organization: @org)
      @card = create(:card, author: @admin)
      # prepare all needed WorkflowPatchRevisions
      WorkflowPatchRevisionsJob.perform_now(patches: @prepared_users_groups_patches)
      @dynamic_group_revision = DynamicGroupRevision.create(
        title: 'dynamic_group_by_city',
        uid: 'fc2554ad-7422-4121-9cc1-93d67fd10a93',
        external_id: 1,
        user_id: @admin.id,
        organization_id: @admin.organization_id,
        item_id: @card.id,
        item_type: 'Card',
        action: 'share',
        message: nil
      )
    end

    # dynamic-selection patches (include groups & users_groups)
    # DynamicGroupRevision `api/v2/workflows/dynamic-selections`
    test 'should properly process dynamic-selection patches' do
      insert_patches = WorkflowPatchRevision.where(patch_type: 'insert')
      insert_patches.each do |patch|
        WorkflowsJob.perform_now(patch_id: patch.id)
      end

      expected_team_users = [@user1.id, @user2.id, @admin.id]
      expected_team = Team.find_by(name: 'dynamic_group_by_city')
      @dynamic_group_revision.reload

      assert expected_team
      assert_equal @dynamic_group_revision.group_id, expected_team.id
      assert_same_elements expected_team_users, expected_team.teams_users.pluck(:user_id)
      assert_equal 'admin', expected_team.teams_users.find_by(user_id: @admin.id).as_type
      assert expected_team.is_dynamic
      assert expected_team.is_private
      assert expected_team.is_mandatory

      delete_patches = WorkflowPatchRevision.where(patch_type: 'delete')
      delete_patches.each do |patch|
        WorkflowsJob.perform_now(patch_id: patch.id)
      end

      refute Team.exists?(name: 'dynamic_group_by_city')
      refute DynamicGroupRevision.exists?(id: @dynamic_group_revision.id)
    end
  end
end

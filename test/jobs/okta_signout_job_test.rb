require 'test_helper'

class OktaSignoutJobTest < ActiveJob::TestCase
  test 'should signout user from okta' do
    stub_okta_group_creation
    stub_okta_app_creation
    stub_okta_group_app_link
    org = create(:organization)
    user = create(:user, organization: org)
    org.configs.create(name: 'OktaApiToken', value: SecureRandom.hex)
    org.configs.create(name: 'app.config.okta.enabled', value: "true")
    user.update(federated_identifier: SecureRandom.hex)

    stub_user_logout
    response = OktaSignoutJob.new.perform(user_id: user.id, organization_id: user.organization_id)
    assert_equal true, response
  end
end

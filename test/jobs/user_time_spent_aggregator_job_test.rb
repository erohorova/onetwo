require 'test_helper'
class UserTimeSpentAggregatorJobTest < ActiveSupport::TestCase

  setup do
    UserTimeSpentAggregatorJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    WebMock.disable!
  end

  teardown do
    WebMock.enable!
  end

  test 'should call increment with right arguments' do
    user = create(:user)
    video_stream = create(:wowza_video_stream)
    tags = create_list(:tag, 3)
    tags.first(2).map{|t| t.update_attributes(type: "Interest")}
    video_stream.tags << tags

    Timecop.freeze do
      UserSession.expects(:update_session).with(user.id, Time.at(Time.now.to_i)).returns(20).once
      UserLevelMetric.expects(:increment_time_spent).with({user_id: user.id}, 20, Time.at(Time.now.to_i)).returns(nil).once
      tags.first(2).each do |tag|
        UserTopicLevelMetric.expects(:increment_time_spent).with({user_id: user.id, tag_id: tag.id}, 20, Time.at(Time.now.to_i)).returns(nil).once
      end
      mr = MetricRecord.new(action: "impression", object_id: video_stream.id, object_type: "video_stream", actor_id: user.id, owner_id: video_stream.creator_id, properties: {view: 'summary'}).save
    end
  end
end

require 'test_helper'

class SyncCloneChannelCardsJobTest < ActiveSupport::TestCase
  setup do
    @org1, @org2 = create_list(:organization, 2)
    @user = create(:user, organization: @org1)
    @org1_channel = create(:channel, organization: @org1, shareable: true)
    @org2_channel = create(:channel, organization: @org2, parent_id: @org1_channel.id)
  end

  test "should clone card and add to child channel on adding card in shared channel" do
    card = create(:card, card_type: 'media', card_subtype: 'text', author_id: @user.id, title: 'This is card', 
      message: 'This is message', organization: @org1, ecl_source_name: 'UGC')
    card.current_user = @user
    card.channels << @org1_channel

    SyncCloneChannelCardsJob.perform_now({parent_card_id: card.id, parent_channel_id: @org1_channel.id, action: 'add'})
    clone_card = Card.find_by(super_card_id: card.id)
    assert_equal 1, card.cloned_cards.count
    assert_equal 1, clone_card.channels.count
    assert_equal @org1_channel.id, clone_card.channels.first.parent_id
  end

  test "should delete clone card on removing parent card from shared channel which has clone" do
    card = create(:card, card_type: 'media', card_subtype: 'text', author_id: @user.id, title: 'This is card', 
      message: 'This is message', organization: @org1, ecl_source_name: 'UGC')
    card.current_user = @user
    card.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)

    ChannelsCard.where(card_id: card.id, channel_id: @org1_channel.id).first.destroy
    SyncCloneChannelCardsJob.perform_now({parent_card_id: card.id, parent_channel_id: @org1_channel.id, action: 'destroy'})

    assert_equal 0, card.cloned_cards.count
  end

  test "should delete clone pathway card on removing pathway from shared channel which has clone" do
    TestAfterCommit.enabled = true
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover', 
      organization: @org1, ecl_source_name: 'UGC')
    cards = create_list(:card, 2, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end
    cover.current_user = @user
    cover.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
    clone_card_ids = [cover.cloned_cards.first.id]
    cards.each {|card|  clone_card_ids << card.cloned_cards.first.id}
    ChannelsCard.where(card_id: cover.id, channel_id: @org1_channel.id).first.destroy
    SyncCloneChannelCardsJob.perform_now({parent_card_id: cover.id, parent_channel_id: @org1_channel.id, action: 'destroy'})
    
    assert_equal 0, Card.where(id: clone_card_ids).count
  end

  test "should delete clone journey card on removing journey from shared channel which has clone" do
    TestAfterCommit.enabled = true
    cover = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: @user, 
      organization: @org1, ecl_source_name: 'UGC')
    section = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user, organization: @org)
    cards = create_list(:card, 2, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: section.id, add_id: card.id, add_type: 'Card')
    end
    JourneyPackRelation.add(cover_id: cover.id, add_id: section.id)
    cover.current_user = @user
    cover.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
    clone_card_ids = [cover.cloned_cards.first.id, section.cloned_cards.first.id]
    cards.each {|card|  clone_card_ids << card.cloned_cards.first.id}
  
    ChannelsCard.where(card_id: cover.id, channel_id: @org1_channel.id).first.destroy
    SyncCloneChannelCardsJob.perform_now({parent_card_id: cover.id, parent_channel_id: @org1_channel.id, action: 'destroy'})

    assert_equal 0, Card.where(id: clone_card_ids).count
  end

  test "should create new cards on adding parent journey to parent channel which was removed earlier" do
    TestAfterCommit.enabled = true
    cover = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: @user, 
      organization: @org1, ecl_source_name: 'UGC')
    section = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user, organization: @org)
    cards = create_list(:card, 2, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: section.id, add_id: card.id, add_type: 'Card')
    end
    JourneyPackRelation.add(cover_id: cover.id, add_id: section.id)
    cover.current_user = @user
    cover.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
    clone_card_ids = [cover.cloned_cards.first.id, section.cloned_cards.first.id]
    cards.each {|card|  clone_card_ids << card.cloned_cards.first.id}
    ChannelsCard.where(card_id: cover.id, channel_id: @org1_channel.id).first.destroy
    SyncCloneChannelCardsJob.perform_now({parent_card_id: cover.id, parent_channel_id: @org1_channel.id, action: 'destroy'})

    cover.channels << @org1_channel
    SyncCloneChannelCardsJob.perform_now({parent_card_id: cover.id, parent_channel_id: @org1_channel.id, action: 'add'})
    
    assert_equal 1, cover.cloned_cards.count
    assert_equal 1, section.cloned_cards.count
    assert_equal 2, cards.sum{|c| c.cloned_cards.count }
  end
end
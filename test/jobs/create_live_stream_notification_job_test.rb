require 'test_helper'

class CreateLiveStreamNotificationJobTest < ActiveJob::TestCase
  setup do
    @follower_notifier = Notify::NewLivestreamFromFollowedUserNotifier.new
  end

  test 'should send notification to channel followers and followers of creator when videostream is posted to channel' do
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(true)
    @org = create(:organization)

    user = create :user, organization: @org
    follower = create :user, organization: @org
    follower2 = create :user, organization: @org

    channel = create :channel, user: user, organization: @org
    channel.followers << follower

    create :follow, followable: user, user: follower2

    card = create :card, organization: @org, author_id: user.id
    video_stream = create(:wowza_video_stream, organization_id: @org.id, creator: user, card: card, status: 'pending')
    channel_card = create :channels_card, card_id: card.id, channel_id: channel.id

    assert_difference -> { NotificationEntry.all.count }, 2 do
      CreateLiveStreamNotificationJob.perform_now(
        video_stream_id: video_stream.id
      )
    end

    notification_params_channel = @follower_notifier.notification_params(NotificationEntry.first)

    assert_equal notification_params_channel[:receiver_name], follower2.name
    assert_equal NotificationEntry.where(user_id: follower2.id, event_name: Notify::EVENT_NEW_LIVESTREAM).count, 1
  end

  test 'should send notification to direct follower, channel follower, team members' do
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(true)
    @org = create(:organization)

    user = create :user, organization: @org
    follower = create :user, organization: @org
    follower2 = create :user, organization: @org

    channel = create :channel, user: user, organization: @org
    channel.followers << follower

    create :follow, followable: user, user: follower2
    channel_team_user = create :user, organization: @org
    team = create(:team, organization: @org)
    team.add_user channel_team_user

    card = create :card, organization: @org, author_id: user.id
    team_card = create(:teams_card, team_id: team.id, card_id: card.id, type: 'SharedCard')
    channel_card = create :channels_card, card_id: card.id, channel_id: channel.id

    video_stream = create(:wowza_video_stream, organization_id: @org.id, creator: user, card: card, status: 'pending')

    assert_difference -> { NotificationEntry.all.count }, 3 do
      CreateLiveStreamNotificationJob.perform_now(
        video_stream_id: video_stream.id
      )
    end

    assert_equal NotificationEntry.where(user_id: follower2.id, event_name: Notify::EVENT_NEW_LIVESTREAM).count, 1
    assert_equal NotificationEntry.where(user_id: follower.id, event_name: Notify::EVENT_NEW_CONTENT_IN_CHANNEL).count, 1
    assert_equal NotificationEntry.where(user_id: channel_team_user.id, event_name: Notify::EVENT_LIVE_STREAM_IN_GROUP).count, 1
  end
end

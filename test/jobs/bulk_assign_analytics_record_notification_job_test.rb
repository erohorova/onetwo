require 'test_helper'

class BulkAssignAnalyticsRecordNotificationJobTest < ActiveJob::TestCase
  setup do
    @org = create(:organization)
    @team = create(:team, organization: @org)
    @users = create_list(:user, 5, organization: @org)
    @admin_user = create(:user, organization: @org)

    create(:config, name: 'feature/notifications_and_triggers_enabled', value: 'true', configable: @org)

    config = build :notification_config, organization: @org
    config.set_enabled(Notify::NOTE_ASSIGNED_CONTENT, :email, :single)
    config.save

    @users.each {|user| @team.add_member(user)}
    @team.add_admin @admin_user

    @card = create(:card, author_id: @users.last.id)
    BulkAssignService.new(team: @team, assignor_id: @admin_user.id, assignable_type: 'Card', 
      assignable_id: @card.id, opts: {message: "this is test assignment message"}).run(assignee_ids: @users.map(&:id), batch_size: 200)

    @notify_assignment_ids = @team.team_assignments.map(&:assignment_id).uniq
    @notify_team_assignment_ids = @team.team_assignments.map(&:id).uniq
  end

  test "should make new assignment notification entry for each assignment" do
    UserTeamAssignmentNotificationJob.expects(:perform_later).times(5)

    BulkAssignAnalyticsRecordNotificationJob.new.perform({assignment_ids: @notify_assignment_ids, team_id: @team.id, 
      organization_id: @org.id, team_assignment_ids: @notify_team_assignment_ids})

    notifications = NotificationEntry.where(notification_name: Notify::NOTE_ASSIGNED_CONTENT)
    assert_equal 5, notifications.length
    assert_equal [Notify::EVENT_NEW_ASSIGNMENT], notifications.map{|n| n.event_name }.uniq
    assert_same_elements @notify_assignment_ids, notifications.map{|n| n.sourceable_id }.uniq
  end

  test "should trigger analytics metrics recorder job" do
    Analytics::MetricsRecorderJob.expects(:perform_later).times(5)

    BulkAssignAnalyticsRecordNotificationJob.new.perform({assignment_ids: @notify_assignment_ids, team_id: @team.id, 
      organization_id: @org.id, team_assignment_ids: @notify_team_assignment_ids})
  end
end
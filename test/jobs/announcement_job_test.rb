require 'test_helper'

class AnnouncementJobTest < ActiveSupport::TestCase

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'

    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(false)
    @faculty1= create(:user)
    @user1 = create(:user)
    @user2 = create(:user)
    @user3 = create(:user)
    @user4 = create(:user, email: nil, first_name: nil, last_name: nil)
    @org = create(:organization)
    @group1 = create(:group, website_url: 'http://awesomecourse.com', organization: @org)
    @group1.add_admin(@faculty1)
    @group1.add_member(@user1)
    @group1.add_member(@user2)
    @group1.add_member(@user3)
    @group1.add_member(@user4)
    @post = create(:announcement, group: @group1, user: @faculty1)

    @pg = create(:private_group, parent_id: @group1.id, organization: @org)
    @pg.add_admin(@faculty1)
    @pg.add_member(@user1)

    @private_group_announcement = create(:announcement, group: @pg, user: @faculty1)
  end

  test 'should schedule an email to mandrill' do
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:post_title] =message[:message][:global_merge_vars].first['content']
      mail_args[:name] = message[:message][:to].map{|d| d[:name]}
      mail_args[:keys] =  message[:message][:merge_vars].first[:vars].map{|d| d[:name]}
    }.once
    AnnouncementJob.perform_now(post_id: @post.id)

    #header looks like this
    #{\"_rcpt\":\"person3@example.com\",\"first_name\":\"fname\",\"last_name\":\"lname\",\"name\":\"fname\r\n lname\",\"unsubscribe_url\":\"http://localhost:3000/preferences?token=ak41WXgrZ3hPKzlDVmtrL0kwRlVUWWxyM0s5UnppeGUvaVFjUzBQcnkwYUFaOEpQN2djTEt3RyswOENwNmFCdUw1bHBlZVczOTVMT285R1V2WFRpdWhqVmNpekd0NVQ1d0xMc3ZtTVhabVh0MFBSKzFzSVNFa09aWUhNcVlGK3VRemF1M2hwMEJoTTYweFVnU1VvbkJGakprSDB5b04xWEM0UXhLbnJYeXlMMGdlcGZZZEk1YVdEZjNxUnhnbkZQLS1wakVDYm1UNW8wWk9LZUxZNGZWY2h3PT0%253D--cd3df632795703cf84dce05ea3cda23b64ccaddd\"}\r\n

    assert_equal @post.title, mail_args[:post_title]
    assert_includes  mail_args[:name], @post.user.name
    assert_includes mail_args[:keys], "unsubscribe_url"
  end

  test 'should send user to parent group for private group announcements' do
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:post_url] = message[:message][:global_merge_vars][2]['content']
    }.once
    AnnouncementJob.perform_now(post_id: @private_group_announcement.id)


    url = UrlBuilder::build(@group1.website_url, {'edcast_ref' => @private_group_announcement.id})
    assert_match CGI::escape(url.to_s), mail_args[:post_url]
  end

  test 'should escape name with comma' do
      user = create(:user, first_name: 'I have a , in my name and "double quote"')
      @group1.add_member(user)

      mail_args = {}
      MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
        mail_args[:name_list] = message[:message][:to].map{|d| d[:name]}
    }.once
      AnnouncementJob.perform_now(post_id: @post.id)
      assert_includes mail_args[:name_list], "I have a , in my name and \"double quote\" #{user.last_name}"
  end

  test 'should gracefully handle exception and continue onto the next email batch' do
    email_mock = mock('email')
    email_mock.stubs(:deliver_now)

    AnnouncementMailer.stubs(:announcement_email).raises("SomeError").then.raises("SomeOtherError").then.returns(email_mock)
    AnnouncementJob.perform_now(post_id: @post.id, batch_size: 2)

  end
end
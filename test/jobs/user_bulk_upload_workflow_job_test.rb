require 'test_helper'

class UserBulkUploadWorkflowJobTest < ActiveJob::TestCase
  include ActiveJob::TestHelper

  setup do
    EdcastActiveJob.unstub(:perform_later)
    WorkflowProcessorJob.unstub(:perform_later)

    @org = create :organization
    @config = create :config, configable: @org, value: true, data_type: 'boolean', name: 'simplified_workflow'
    @creator = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
    @workflow = create :workflow, user_id: @creator.id, is_enabled: true, organization: @org

    # country is India, first_name is not null, location is not Mumbai => selected
    @user1 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
    custom_field_country = create :custom_field, organization: @org, abbreviation: 'country', display_name: 'country'
    create :user_custom_field, user: @user1, custom_field: custom_field_country, value: 'India'

    # country is India, first_name is null, location is not Mumbai => not selected
    @user2 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active',
                   is_anonymized: false, first_name: nil
    create :user_custom_field, user: @user2, custom_field: custom_field_country, value: 'India'

    # country is India, first_name is not null, location is not Mumbai => selected
    @user3 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
    custom_field_location = create :custom_field, organization: @org, abbreviation: 'location', display_name: 'location'
    create :user_custom_field, user: @user3, custom_field: custom_field_location, value: 'Pune'
    create :user_custom_field, user: @user3, custom_field: custom_field_country, value: 'India'
  end

  test 'User bulk upload v1' do
    input_user_ids = [@user1.id, @user2.id, @user3.id]
    assert_no_enqueued_jobs
    assert_enqueued_with(job: WorkflowProcessorJob) do
      UserBulkUploadWorkflowJob.new.perform(organization_id: @org.id, user_metadata: {ids: input_user_ids})
    end
    assert_enqueued_jobs 1

    assert_equal 0, Team.all.length
    perform_enqueued_jobs do
      UserBulkUploadWorkflowJob.new.perform(organization_id: @org.id, user_metadata: {ids: input_user_ids})
    end

    assert_performed_jobs 1

    assert_equal 1, Team.all.length
    team = Team.first
    assert_equal 'Pune', team.name
    assert_same_elements [@user3.id], team.users.ids
  end

  test 'User bulk upload v2' do
    input_user_emails = [@user1.email, @user2.email, @user3.email]
    assert_no_enqueued_jobs
    assert_enqueued_with(job: WorkflowProcessorJob) do
      UserBulkUploadWorkflowJob.new.perform(organization_id: @org.id, user_metadata: {emails: input_user_emails})
    end
    assert_enqueued_jobs 1

    assert_equal 0, Team.all.length
    perform_enqueued_jobs do
      UserBulkUploadWorkflowJob.new.perform(organization_id: @org.id, user_metadata: {emails: input_user_emails})
    end

    assert_performed_jobs 1

    assert_equal 1, Team.all.length
    team = Team.first
    assert_equal 'Pune', team.name
    assert_same_elements [@user3.id], team.users.ids
  end
end
require 'test_helper'

class TeamAssignmentJobTest < ActiveSupport::TestCase
  test 'should invoke batch assignment job' do
    org = create(:organization)
    team = create(:team, organization: org)
    admin_user = create(:user, organization: org, organization_role: 'admin')
    users = create_list(:user, 10, organization: org)

    team.add_admin admin_user
    users.each {|u| team.add_member(u)}

    card = create(:card, author_id: admin_user.id)
    #total 11 users
    TeamBatchAssignmentJob.expects(:perform_later)
      .with(
          team_id: team.id,
          assignor_id: admin_user.id,
          assignee_ids: team.users.map(&:id),
          assignable_id: card.id,
          assignable_type: 'Card',
          opts: {self_assign: false}
      ).once.returns(nil)


    TeamAssignmentJob.new.perform(
      team_id: team.id,
      assignor_id: admin_user.id,
      assignee_ids: [],
      assignable_id: card.id,
      assignable_type: 'Card',
      opts: {self_assign: false}
    )
  end

  test 'should not invoke batch assignment job for suspended users' do
    org = create(:organization)
    team = create(:team, organization: org)
    admin_user = create(:user, organization: org, organization_role: 'admin')
    users = create_list(:user, 3, organization: org)
    suspended_user = users.last
    suspended_user.update(is_suspended: true)

    team.add_admin admin_user
    users.each {|u| team.add_member(u)}

    card = create(:card, author_id: admin_user.id)
    not_suspended_team_users = team.users.not_suspended
    assert_not_includes not_suspended_team_users, suspended_user

    # without assignee_ids
    TeamBatchAssignmentJob.expects(:perform_later).with(team_id: team.id, assignor_id: admin_user.id, assignee_ids: not_suspended_team_users.map(&:id), assignable_id: card.id, assignable_type: 'Card', opts: {self_assign: false}).once.returns(nil)
    TeamAssignmentJob.new.perform(team_id: team.id, assignor_id: admin_user.id, assignee_ids: [], assignable_id: card.id, assignable_type: 'Card', opts: {self_assign: false})

    # with assignee_ids
    not_suspended_user = users.first
    assignee_ids = [suspended_user.id, not_suspended_user.id]
    TeamBatchAssignmentJob.expects(:perform_later).with(team_id: team.id, assignor_id: admin_user.id, assignee_ids: [not_suspended_user.id], assignable_id: card.id, assignable_type: 'Card', opts: {self_assign: false}).once.returns(nil)
    TeamAssignmentJob.new.perform(team_id: team.id, assignor_id: admin_user.id, assignee_ids: assignee_ids, assignable_id: card.id, assignable_type: 'Card', opts: {self_assign: false})
  end

  test 'should create assignment with due date' do
    org = create(:organization)
    team = create(:team, organization: org)
    admin_user = create(:user, organization: org, organization_role: 'admin')
    users = create_list(:user, 10, organization: org)

    team.add_admin admin_user
    users.each {|u| team.add_member(u)}

    card = create(:card, author_id: admin_user.id)
    #total 11 users
    TeamBatchAssignmentJob.expects(:perform_later).with(team_id: team.id, assignor_id: admin_user.id, assignee_ids: team.users.map(&:id), assignable_id: card.id, assignable_type: 'Card', opts: {self_assign: false, due_at: (Date.current + 10.days).strftime("%m-%d-%Y")}).once
    TeamAssignmentJob.new.perform(team_id: team.id, assignor_id: admin_user.id, assignee_ids: [], assignable_id: card.id, assignable_type: 'Card', opts: {self_assign: false, due_at: (Date.current + 10.days).strftime("%m-%d-%Y")})
  end

  test 'should record event for team and assignable' do
    stub_time

    org = create(:organization)
    team = create(:team, organization: org)
    users = create_list(:user, 2, organization: org)
    admin_user = create(:user, organization: org)

    users.each {|user| team.add_member(user)}
    team.add_admin admin_user

    card = create(:card, author_id: users.last.id)

    # =======expect group card assigned gets called=======
    additional_data = {
      platform: 'Edcast iPhone',
      user_agent: 'web',
      user_id: admin_user.id
    }

    additional_data = {
      user_id: admin_user.id,
      platform: 'web'
    }
    RequestStore.stubs(:read).returns(additional_data)

    Analytics::MetricsRecorderJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    Analytics::GroupCardMetricsRecorder.expects(:record).with(
      has_entries({
        event: 'group_card_assigned',
        org: Analytics::MetricsRecorder.org_attributes(org),
        actor: Analytics::MetricsRecorder.user_attributes(admin_user),
        group: Analytics::MetricsRecorder.team_attributes(team),
        card: Analytics::MetricsRecorder.card_attributes(card),
        timestamp: Time.now.to_i
      })
    ).once
    # ================

    TeamAssignmentJob.new.perform(
      team_id: team.id,
      assignor_id: admin_user.id,
      assignee_ids: users.map(&:id),
      assignable_id: card.id,
      assignable_type: 'Card'
    )
  end
end
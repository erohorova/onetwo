require 'test_helper'

class InviatationMandrillMailerTest < ActionMailer::TestCase
  setup do
    Event.any_instance.stubs :schedule_reminder_email
    BranchmetricsApi.stubs(:generate_link).returns(true)
    @organization = create(:organization)
    @invitation = create(:invitation,
      recipient_email: generate(:email_address),
      invitable:  @organization,
      roles: 'member',
      sender: create(:user)
    )
  end

  test 'should not send group invite email to user if `feature/send_no_mails` is true ' do
    Organization.any_instance.stubs(:send_no_mails?).returns(true)
    MandrillMailer.any_instance.expects(:send_api_email).never
    team  = create(:team, organization: @organization)
    recipient = create(:user, organization: @organization)
    sender = create(:user)
    invitation = create(:invitation, recipient_email: recipient.email, invitable:  team,
      roles: 'member', sender: sender, recipient: recipient)

    InvitationMandrillMailer.invite({'first_name' => invitation.first_name, 'last_name' => invitation.last_name,
                                 'email' => invitation.recipient_email,
                                 'invitable_id' => invitation.invitable_id,
                                 'invitable_type' => invitation.invitable_type,
                                 'token' => invitation.token,
                                 'roles' => invitation.roles,
                                 'invitable_photo' => team.photo,
                                 'invitable_name' => team.name,
                                 'sender_name' => sender.try(:name),
                                 'recipient_name' => recipient.try(:first_name),
                                 'id' => invitation.id
                                }).deliver_now


  end

  test 'should send group invite email to user if `feature/send_no_mails` is true ' do
    Organization.any_instance.stubs(:send_no_mails?).returns(false)
    MandrillMailer.any_instance.expects(:send_api_email).once
    team  = create(:team, organization: @organization)
    recipient = create(:user, organization: @organization)
    sender = create(:user)
    invitation = create(:invitation, recipient_email: recipient.email, invitable:  team,
      roles: 'member', sender: sender, recipient: recipient)

    InvitationMandrillMailer.invite({'first_name' => invitation.first_name, 'last_name' => invitation.last_name,
                                 'email' => invitation.recipient_email,
                                 'invitable_id' => invitation.invitable_id,
                                 'invitable_type' => invitation.invitable_type,
                                 'token' => invitation.token,
                                 'roles' => invitation.roles,
                                 'invitable_photo' => team.photo,
                                 'invitable_name' => team.name,
                                 'sender_name' => sender.try(:name),
                                 'recipient_name' => recipient.try(:first_name),
                                 'id' => invitation.id
                                }).deliver_now


  end

  test 'should send welcome email for invitation' do
    Invitation.any_instance.expects(:branchmetrics_url).returns('http://test.com')
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:from_name] = message[:message][:from_name]
      mail_args[:subject] = message[:message][:subject]
    }.once

    InvitationMandrillMailer.send_welcome_email(@invitation.id).deliver_now
    assert_equal 'EdCast', mail_args[:from_name]
    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal "Welcome to #{@organization.name} on EdCast!", mail_args[:subject]
  end

  test 'should send welcome email for custom sender address' do
    Invitation.any_instance.expects(:branchmetrics_url).returns('http://test.com')
    create(:mailer_config, organization: @organization, from_address: 'abc@xyz.com', from_name: 'abc')

    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:from_name] = message[:message][:from_name]
    }.once

    InvitationMandrillMailer.send_welcome_email(@invitation.id).deliver_now
    assert_equal 'abc', mail_args[:from_name]
    assert_equal 'abc@xyz.com', mail_args[:from_email]
  end

  test 'should invite to organization' do
    BranchmetricsApi.expects(:generate_link).returns('http://test.com')
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:from_name] = message[:message][:from_name]
      mail_args[:subject] = message[:message][:subject]
    }.once

    InvitationMandrillMailer.invite(
      {
        'email' => 'test@email.com',
        'invitable_id' => @organization.id,
        'invitable_type' => 'Organization',
        'invitable_name' => @organization.name,
        'token' => 'test',
        'id' => @invitation.id,
        'roles' => 'member'
      }
    ).deliver_now

    assert_equal 'EdCast Team', mail_args[:from_name]
    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal "Invitation to join #{@organization.name} on EdCast", mail_args[:subject]
  end

  test 'should invite to organization using custom sender address' do
    BranchmetricsApi.expects(:generate_link).returns('http://test.com')
    create(:mailer_config, organization: @organization, from_address: 'abc@xyz.com', from_name: 'abc')

    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:from_name] = message[:message][:from_name]
    }.once

    InvitationMandrillMailer.invite(
      {
        'email' => 'test@email.com',
        'invitable_id' => @organization.id,
        'invitable_type' => 'Organization',
        'invitable_name' => @organization.name,
        'token' => 'test',
        'id' => @invitation.id,
        'roles' => 'member'
      }
    ).deliver_now

    assert_equal 'abc', mail_args[:from_name]
    assert_equal 'abc@xyz.com', mail_args[:from_email]
  end

  test 'invitation mail should have recipient name' do
    BranchmetricsApi.expects(:generate_link).returns('http://test.com')
    recipient = create(:user, organization: @organization)
    invitation = create(:invitation, recipient_email: recipient.email, invitable:  @organization,
      roles: 'member', sender: create(:user), recipient: recipient)

    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:from_name] = message[:message][:from_name]
      mail_args[:first_name] = message[:message][:global_merge_vars][17]["content"]
    }.once

    InvitationMandrillMailer.invite(
      {
        'email' => 'test@email.com',
        'invitable_id' => @organization.id,
        'invitable_type' => 'Organization',
        'invitable_name' => @organization.name,
        'token' => 'test',
        'recipient_name' => recipient.first_name,
        'id' => invitation.id,
        'roles' => 'member'
      }
    ).deliver_now

    assert_equal recipient.first_name, mail_args[:first_name]
  end
end

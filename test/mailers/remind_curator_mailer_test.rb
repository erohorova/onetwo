require 'test_helper'

class RemindCuratorMailerTest < ActionMailer::TestCase
  setup do
    @org = create(:organization)
    @curator = create(:user, organization: @org)

    user = create(:user, organization: @org)
    channel = create(:channel, curate_ugc: true, curate_only: true, organization: @org, created_at: 5.days.ago)
    create(:channels_user, user_id: user.id, channel_id: channel.id)
    create(:channels_curator, user_id: @curator.id, channel_id: channel.id)

    card = create(:card, organization: @org, author_id: user.id, created_at: 5.days.ago)
    create(:channels_card, card_id: card.id, channel_id: channel.id, created_at: 5.days.ago)

    config = build :notification_config, organization: @org
    config.set_enabled(Notify::NOTE_REMINDED_CURATOR, :email, :single)
    config.save
  end

  test 'verify subject of email' do
    mail_args = {}

    EDCAST_NOTIFY.expects(:send_email_mandrill).with(){|organization, user_info, subject, body|
      mail_args[:subject] = subject
    }.at_least_once
    EDCAST_NOTIFY.trigger_event(Notify::EVENT_REMIND_CURATOR, User.find_by(id: @curator.id))
    EDCAST_NOTIFY.send_single_notification(NotificationEntry.last)

    assert_equal  "Items Pending for Curation.", mail_args[:subject]
  end

  test 'do not send email when user email is not present' do
    # need a user without email. We have super admins in the system without the email
    @curator.update(email: nil)
    mandrill_stub = mock()

    # mandrill calls send on messages
    mandrill_stub.expects(:messages).never
    Mandrill::API.expects(:new).never

    subject     = "Items Pending for Curation."
    from_email  = 'admin@edcast.com'

    _message = <<~HEREDOC
        Email with subject #{subject} from #{from_email}
        has no receiver. Email notification was not sent.
      HEREDOC

    log.expects(:info).with(_message).once

    # trigger notification
    EDCAST_NOTIFY.trigger_event(Notify::EVENT_REMIND_CURATOR, User.find_by(id: @curator.id))
    EDCAST_NOTIFY.send_single_notification(NotificationEntry.last)
  end
end

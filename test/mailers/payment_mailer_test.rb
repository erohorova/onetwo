require 'test_helper'

class PaymentMailerTest < ActionMailer::TestCase
  include Rails.application.routes.url_helpers

  test 'should send email to user after payment' do
    org  = create(:organization)
    user = create(:user, organization: org)
    user_subscription = create(:user_subscription, organization_id: org.id, user_id: user.id)

    mail_args = {}
    MandrillMessageGenerationService.any_instance.expects(:merge_custom_attrs).with(){|args|
      mail_args[:to_email] = args[:to].map{|d| d[:email]}.last
      mail_args[:from_email] = args[:from_email]
      mail_args[:first_name] =args[:global_merge_vars][14]['content']
      mail_args[:amount] = args[:global_merge_vars][15]['content']
      mail_args[:currency] =args[:global_merge_vars][16]['content']
      mail_args[:org_name] =args[:global_merge_vars][17]['content']
      mail_args[:gateway_id] =args[:global_merge_vars][19]['content']
    }.once

    PaymentMailer.user_subscription_notification(user_subscription.id).deliver_now
    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal user.mailable_email, mail_args[:to_email]
    assert_equal 3500.00, mail_args[:amount]
    assert_equal user.first_name.capitalize, mail_args[:first_name]
    assert_equal "A1B2C3DE", mail_args[:gateway_id]

  end

  test 'should send email to user after card payment' do
    Transaction.any_instance.stubs(:create_transaction_audit).returns(true)
    org  = create(:organization)
    user = create(:user, organization: org)
    card = create(:card, message: 'Paid card')
    order = create(:order, orderable: card)
    transaction = create(:transaction, order: order)
    user_card_subscription = create(:card_subscription, organization_id: org.id, user: user, card: card, transaction_id: transaction.id)

    mail_args = {}
    MandrillMessageGenerationService.any_instance.expects(:merge_custom_attrs).with(){|args|
      mail_args[:to_email] = args[:to].map{|d| d[:email]}.last
      mail_args[:from_email] = args[:from_email]
      mail_args[:first_name] = args[:global_merge_vars][14]['content']
      mail_args[:amount] = args[:global_merge_vars][15]['content']
      mail_args[:currency] = args[:global_merge_vars][16]['content']
      mail_args[:org_name] = args[:global_merge_vars][17]['content']
      mail_args[:course_name] = args[:global_merge_vars][21]['content']
    }.once

    PaymentMailer.user_card_subscription_notification(user_card_subscription.id).deliver_now
    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal user.mailable_email, mail_args[:to_email]
    assert_equal 100.00, mail_args[:amount]
    assert_equal user.first_name.capitalize, mail_args[:first_name]
    assert_equal 'Paid card', mail_args[:course_name]
  end

  test 'card payment email should show resource title as the course name if present' do
    Transaction.any_instance.stubs(:create_transaction_audit).returns(true)
    org  = create(:organization)
    user = create(:user, organization: org)
    resource = create(:resource, title: 'Paid card title')
    card = create(:card, message: 'Paid card', resource: resource)
    order = create(:order, orderable: card)
    transaction = create(:transaction, order: order)
    user_card_subscription = create(:card_subscription, organization_id: org.id, user: user, card: card, transaction_id: transaction.id)

    mail_args = {}
    MandrillMessageGenerationService.any_instance.expects(:merge_custom_attrs).with(){|args|
      mail_args[:course_name] = args[:global_merge_vars][21]['content']
    }.once

    PaymentMailer.user_card_subscription_notification(user_card_subscription.id).deliver_now
    assert_equal 'Paid card title', mail_args[:course_name]
  end

  test 'should send email to user after successful recharge of wallet' do
    Transaction.any_instance.stubs(:create_transaction_audit).returns(true)
    org  = create(:organization)
    user = create(:user, organization: org)
    recharge = create(:recharge)
    order = create(:order, orderable: recharge)
    transaction = create(:transaction, order: order, amount: BigDecimal.new("100"), currency: 'INR')
    wallet_transaction = create(:wallet_transaction, order: order, user: user, amount: BigDecimal.new("20"))
    wallet_balance = create(:wallet_balance, user: user, amount: BigDecimal.new("20"))

    mail_args = {}
    MandrillMessageGenerationService.any_instance.expects(:merge_custom_attrs).with(){|args|
      mail_args[:to_email] = args[:to].map{|d| d[:email]}.last
      mail_args[:from_email] = args[:from_email]
      mail_args[:first_name] = args[:global_merge_vars][14]['content']
      mail_args[:amount] = args[:global_merge_vars][16]['content']
      mail_args[:currency] = args[:global_merge_vars][17]['content']
      mail_args[:balance] = args[:global_merge_vars][18]['content']
      mail_args[:wallet_link] = args[:global_merge_vars][19]['content']
      mail_args[:org_name] = args[:global_merge_vars][20]['content']
      mail_args[:skillcoins] = args[:global_merge_vars][15]['content']
    }.once

    PaymentMailer.wallet_recharge_notification(wallet_transaction.id).deliver_now
    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal user.mailable_email, mail_args[:to_email]
    assert_equal 100.00, mail_args[:amount]
    assert_equal user.first_name.capitalize, mail_args[:first_name]
    assert_equal 20, mail_args[:skillcoins]
    assert_equal 20, mail_args[:balance]
    assert_not_nil mail_args[:wallet_link]
  end
end
require 'test_helper'

class EventReminderMailerTest < ActionMailer::TestCase
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'

    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(false)
  end
  test 'should not send mail to suspended users' do
    suspended_user = create(:user, is_suspended: true)
    org = create(:organization)
    user1 = create(:user)
    user2 = create(:user)
    group = create(:resource_group, website_url: 'http://localhost/ui/demo', organization: org)
    group.add_member user1
    group.add_member user2
    group.add_member suspended_user
    event = create(:event, name: 'test_event',user_id: user1.id, group: group)
    mail_to = ''
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
       mail_to = message[:message][:to].map{|d| d[:email]}
    }.once
    EventReminderMailer.remind_users(event:event, users:[user1,user2,suspended_user]).deliver_now
    assert_equal [user1.mailable_email, user2.mailable_email], mail_to
  end

  test 'sends mail to groups non_suspended_users' do
    non_suspended_user = create(:user)
    org = create(:organization)
    user1 = create(:user)
    user2 = create(:user)
    group = create(:resource_group, website_url: 'http://localhost/ui/demo', organization: org)
    group.add_member user1
    group.add_member user2
    group.add_member non_suspended_user
    event = create(:event, name: 'test_event',user_id: user1.id, group: group)
    mail_to = ''
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
        mail_to = message[:message][:to].map{|d| d[:email]}
    }.once
    EventReminderMailer.remind_users(event:event, users:[user1, user2, non_suspended_user]).deliver_now
    assert_equal [user1.mailable_email, user2.mailable_email, non_suspended_user.mailable_email], mail_to
  end


  test 'send reminder mail using custom sender address' do
    org = create(:organization)
    org_user = create(:user, organization: org)
    create(:mailer_config, organization: org, from_address: 'abc@xyz.com', from_name: 'abc')
    group = create(:resource_group, organization: org)
    group.add_member org_user

    event = create(:event, name: 'test_event',user_id: org_user.id, group: group)
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:from_name] = message[:message][:from_name]
    }.once

    EventReminderMailer.remind_users(event:event, users:[org_user]).deliver_now
    assert_equal 'abc', mail_args[:from_name]
    assert_equal 'abc@xyz.com', mail_args[:from_email]
  end
end

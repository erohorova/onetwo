require 'test_helper'

class NewTeamRequestMailerTest < ActionMailer::TestCase
  include Rails.application.routes.url_helpers

  test 'should send request to staff with user details' do
    user = create(:user)

    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:to_email] = message[:message][:to].first['email']
      mail_args[:first_name] = message[:message][:global_merge_vars][14]['content']
      mail_args[:last_name] = message[:message][:global_merge_vars][15]['content']
      mail_args[:customer_email] =message[:message][:global_merge_vars][17]['content']
    }.once
    NewTeamRequestMailer.notify_staff(user).deliver_now

    
    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal Settings.new_team_request_mailing_list, mail_args[:to_email]
    assert_equal user.first_name, mail_args[:first_name]
    assert_equal user.last_name, mail_args[:last_name]
    assert_equal user.email, mail_args[:customer_email]
  end

  test 'send notify staff email using custom sender address' do
    user = create(:user)
    create(:mailer_config, organization: user.organization, from_address: 'abc@xyz.com', from_name: 'abc')

    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:from_name] = message[:message][:from_name]
    }.once

    NewTeamRequestMailer.notify_staff(user).deliver_now
    assert_equal 'abc', mail_args[:from_name]
    assert_equal 'abc@xyz.com', mail_args[:from_email]
  end
end
require 'test_helper'

class ActivityNotificationMailerTest < ActionMailer::TestCase

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'

    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(false)
    @org = create(:organization)
    @client = create(:client, organization: @org)
    @user = create(:user, is_suspended: false, organization: @org)
    @user1 = create(:user, is_suspended: false, organization: @org)
    @user3 = create(:user, is_suspended: true, organization: @org)
    @group = create(:resource_group, organization: @org, client: @client)
    @group.add_member @user
    @group.add_member @user1
    @post = create(:question, user: @user, group: @group)
    @post.stubs(:can_comment?).returns(true)
  end

  test 'send activity notification mail on comment with attachments ' do
    ActivityNotificationJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:to_email] = message[:message][:to].first[:email]
    }.once
    comment = create(:answer, commentable: @post, user: @user1)
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)
    comment.file_resources << fr1
    comment.save!

    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal @user.email, mail_args[:to_email]
  end

  test 'should not send activity notification mail on comment with attachments to suspended users' do
    comment = create(:answer, commentable: @post, user: @user3)
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)
    comment.file_resources << fr1

    MandrillMailer.any_instance.expects(:send_api_email).never
      ActivityNotificationMailer.activity_notification_email(followable: @post, users: [@user3], content: "test mail", activity_id: comment.id).deliver_now
  end

  test "activity notification mail without attachments" do
    comment = create(:answer, commentable: @post, user: @user1)

    MandrillMailer.any_instance.expects(:send_api_email).once
      ActivityNotificationMailer.activity_notification_email(followable: @post, users: [@user1], content: "test mail", activity_id: comment.id).deliver_now
  end

  test 'should not send activity notification mail without attachments to suspended users' do
    comment = create(:answer, commentable: @post, user: @user3)

    MandrillMailer.any_instance.expects(:send_api_email).never
      ActivityNotificationMailer.activity_notification_email(followable: @post, users: [@user3], content: "test mail", activity_id: comment.id).deliver_now
  end

  test 'unsubscribe url should be correct' do
    comment = create(:answer, commentable: @post, user: @user1)

    unsubscribe_token = ''
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
     unsubscribe_token =  message[:message][:merge_vars].first[:vars][3][:content]
     }
    ActivityNotificationMailer.activity_notification_email(followable: @post, users: [@user], content: "test mail", activity_id: comment.id).deliver_now

    token = CGI::parse(URI.parse(unsubscribe_token).query)['token'].first
    follow_id = ActiveSupport::MessageEncryptor.new(Edcast::Application.config.secret_key_base).decrypt_and_verify(CGI::unescape(token))

    assert_equal follow_id, Follow.where(followable: @post, user: @user).first.id
  end

  test 'send activity notification mail using custom sender address with mailer config' do
    ActivityNotificationJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    create(:mailer_config, organization: @org, from_address: 'abc@xyz.com', from_name: 'abc')
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:from_name] = message[:message][:from_name]
    }.once
    comment = create(:answer, commentable: @post, user: @user1)
    assert_equal 'abc@xyz.com', mail_args[:from_email]
    assert_equal 'abc', mail_args[:from_name]
  end

  test 'activity notification email should not be sent if send_no_mails is set to true' do
    comment = create(:answer, commentable: @post, user: @user1)
    Organization.any_instance.stubs(:send_no_mails?).returns(true)

    MandrillMailer.any_instance.expects(:send_api_email).never
    ActivityNotificationMailer.activity_notification_email(followable: @post, users: [@user], content: "test mail", activity_id: comment.id).deliver_now
  end

  test 'activity notification email should be sent even if new framework is on' do
    comment = create(:answer, commentable: @post, user: @user1)
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(true)

    MandrillMailer.any_instance.expects(:send_api_email).times(1)
    ActivityNotificationMailer.activity_notification_email(followable: @post, users: [@user], content: "test mail", activity_id: comment.id).deliver_now
  end
end

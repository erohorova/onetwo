require 'test_helper'

class DeveloperAdminInvitationMailerTest < ActionMailer::TestCase

  setup do
    @user = create(:user)
    @client = create(:client)
    @invitation = DeveloperAdminInvitation.create(recipient_email: "recipient@edcast.com",
                                                  sender: @user,
                                                  client: @client,
                                                  first_name: @user.first_name,
                                                  last_name: @user.last_name)
  end

  test 'send invitation email' do
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:to_email] = message[:message][:to].first['email']
      mail_args[:to_name] = message[:message][:to].first['name']
      mail_args[:first_name] =  message[:message][:global_merge_vars][14]['content']
      mail_args[:invite_content] = message[:message][:global_merge_vars][15]['content']
      mail_args[:client_name] =  message[:message][:global_merge_vars][17]['content']
    }
    DeveloperAdminInvitationMailer.send(:invite, @invitation.id).deliver_now

    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal @invitation.recipient_email, mail_args[:to_email]
    assert_not_nil @invitation.token
    assert_match /\/developers\/join\/#{@invitation.token}/, mail_args[:invite_content]

    assert_equal mail_args[:to_name], @user.name
    assert_equal mail_args[:client_name], @client.name
    assert_equal mail_args[:first_name], @invitation.first_name

  end

  test 'resend email' do
    MandrillMailer.any_instance.expects(:send_api_email).once
    @invitation.resend_mail
  end
end


require 'test_helper'

class TeamUsersMailerTest < ActionMailer::TestCase

  setup do
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(false)
    @msg = "This is message to all group users sent from admin."
    @org = create(:organization)
    @admin = create(:user, organization: @org, organization_role: 'admin')
    @user = create(:user, organization: @org)
    @team = create(:team, organization: @org)
    @team.add_member(@user)
  end

  test 'send no mails if flag `feature/send_no_mails` is true for organization' do
    Organization.any_instance.stubs(:send_no_mails?).returns(true)
    MandrillMailer.any_instance.expects(:send_api_email).never
    TeamUsersMailer.send(:send_notification, {
      message: @msg,
      user_id: @user.id,
      sender_id: @admin.id,
      team_id: @team.id
    }).deliver_now
  end

  test 'send mails if flag `feature/send_no_mails` is false for organization' do
    Organization.any_instance.stubs(:send_no_mails?).returns(false)
    MandrillMailer.any_instance.expects(:send_api_email).once
    TeamUsersMailer.send(:send_notification, {
      message: @msg,
      user_id: @user.id,
      sender_id: @admin.id,
      team_id: @team.id
    }).deliver_now
  end

  test 'send group admin announcement' do
    mail_args = {}
    MandrillMessageGenerationService.any_instance.expects(:merge_custom_attrs).with(){|args|
      mail_args[:from_email] = args[:from_email]
      mail_args[:to_email] = args[:to].first[:email]
      mail_args[:mail_content] = args[:global_merge_vars][16]['content']
    }.once


    TeamUsersMailer.send(:send_notification, {
      message: @msg,
      user_id: @user.id,
      sender_id: @admin.id,
      team_id: @team.id
    }).deliver_now

    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal @user.email, mail_args[:to_email]
    assert_match @msg, mail_args[:mail_content]
  end

  test 'do not send group admin announcement to unsubscribed users' do
    MandrillMailer.any_instance.expects(:send_api_email).never

    create(:user_preference,
      user: @user,
      key: UserPreference::TEAM_NOTIFICATION_KEY,
      value: 'true',
      preferenceable_type: 'Team',
      preferenceable_id: @team.id)

    TeamUsersMailer.send(:send_notification, {
      message: @msg,
      user_id: @user.id,
      sender_id: @admin.id,
      team_id: @team.id
    }).deliver_now
  end

  test 'send group admin announcement using custom sender address' do
    create(:mailer_config, organization: @user.organization, from_address: 'abc@xyz.com', from_name: 'abc')

    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:from_name] = message[:message][:from_name]
    }.once

    TeamUsersMailer.send(:send_notification, {
      message: @msg,
      user_id: @user.id,
      sender_id: @admin.id,
      team_id: @team.id
    }).deliver_now

    assert_equal 'abc', mail_args[:from_name]
    assert_equal 'abc@xyz.com', mail_args[:from_email]
  end
end

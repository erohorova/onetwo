require 'test_helper'

class DeviseMandrillMailerTest < ActionMailer::TestCase
  self.use_transactional_fixtures = false
  setup do
    stub_okta_session_token
  end

  test 'should not send out forgot password email' do
    user = create(:user)
    user.send_reset_password_instructions

    reset_pass = ''
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      reset_pass = message[:message][:global_merge_vars][14]['content']
    }

    # url for org
    org = create(:organization, host_name: 'ge')
    org_user = create(:user, organization: org)
    org_user.send_reset_password_instructions
    assert_match "https://ge.test.host/forgot_password/confirm", reset_pass
  end

  test 'should send forgot password email using custom sender address' do
    org = create(:organization)
    org_user = create(:user, organization: org)
    create(:mailer_config, organization: org, from_address: 'abc@xyz.com', from_name: 'abc')

    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:from_name] = message[:message][:from_name]
    }.once

    org_user.send_reset_password_instructions
    assert_equal 'abc', mail_args[:from_name]
    assert_equal 'abc@xyz.com', mail_args[:from_email]
  end

  test 'should send out an email confirmation email' do
    skip("INTERMITTENT FAILURE ON TRAVIS")
    #  Since we are calling the api, this method will be called twice, but the mail will be rejected at mandrill's end if there is no email
    MandrillMailer.any_instance.expects(:send_api_email).twice
    user = create(:user, confirmed_at: nil)

    assert_equal false, user.confirmed?

    user.send_confirmation_instructions

    # no email, should not send out anything
    user_no_email = create(:user, confirmed_at: nil, email: nil)
    user_no_email.send_confirmation_instructions
  end

  test 'emails should have org urls' do
    org = create(:organization, host_name: 'ge')
    user = create(:user, organization: org)
    user.platform = 'web'
    raw_confirmation_token, db_confirmation_token = Devise.token_generator.generate(User, :confirmation_token)
    user.confirmation_token = db_confirmation_token
    user.instance_variable_set(:@raw_confirmation_token, raw_confirmation_token)
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:url] = message[:message][:global_merge_vars][14]['content']
    }
    user.send_confirmation_instructions

    confirmation_url = "https://#{user.organization.host}/auth/users/confirmation.html?confirmation_token=#{raw_confirmation_token}"
    assert_equal confirmation_url, mail_args[:url]
  end

  test 'confirmation email should have the right url for web' do
    user = create(:user)
    user.platform = 'web'
    raw_confirmation_token, db_confirmation_token = Devise.token_generator.generate(User, :confirmation_token)
    user.confirmation_token = db_confirmation_token
    user.instance_variable_set(:@raw_confirmation_token, raw_confirmation_token)
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:url] = message[:message][:global_merge_vars][14]['content']
    }
    user.send_confirmation_instructions

    confirmation_url = "https://#{user.organization.host}/auth/users/confirmation.html?confirmation_token=#{raw_confirmation_token}"
    assert_equal confirmation_url, mail_args[:url]
  end

  test 'confirmation email should have the right url for a reserved handle' do
    user = create(:user)
    raw_confirmation_token, db_confirmation_token = Devise.token_generator.generate(User, :confirmation_token)
    user.confirmation_token = db_confirmation_token
    user.instance_variable_set(:@raw_confirmation_token, raw_confirmation_token)

    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:url] = message[:message][:global_merge_vars][14]['content']
    }
    # now there's a reserved handle
    user.send_confirmation_instructions

    confirmation_url = "https://#{user.organization.host}/auth/users/confirmation.html?confirmation_token=#{raw_confirmation_token}"
    assert_equal confirmation_url, mail_args[:url]
  end

  test 'confirmation email should have the right url for ios' do
    user = create(:user)
    user.platform = 'ios'
    raw_confirmation_token, db_confirmation_token = Devise.token_generator.generate(User, :confirmation_token)
    user.confirmation_token = db_confirmation_token

    user.instance_variable_set(:@raw_confirmation_token, raw_confirmation_token)
     mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:url] = message[:message][:global_merge_vars][14]['content']
    }
    # now it is an ios request. should redirect to edcastap://

    user.send_confirmation_instructions

    confirmation_url = "https://#{user.organization.host}/auth/users/confirmation.html?app_redirect=true&confirmation_token=#{raw_confirmation_token}"
    assert_equal confirmation_url, mail_args[:url]
  end

  test 'confirmation email should have the right url for android' do
    user = create(:user)
    user.platform = 'android'

    raw_confirmation_token, db_confirmation_token = Devise.token_generator.generate(User, :confirmation_token)
    user.confirmation_token = db_confirmation_token

    user.instance_variable_set(:@raw_confirmation_token, raw_confirmation_token)
     mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:url] = message[:message][:global_merge_vars][14]['content']
    }
    user.send_confirmation_instructions

    confirmation_url = "https://#{user.organization.host}/auth/users/confirmation.html?app_redirect=true&confirmation_token=#{raw_confirmation_token}"
    assert_equal confirmation_url, mail_args[:url]
  end

  test "should send handle in mail if set" do
    user = build(:user)
    user.save

    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:url] = message[:message][:global_merge_vars][14]['content']
    }

    user.send_confirmation_instructions
  end

  test 'should send out successful signup email' do
    user = create(:user)
    MandrillMailer.any_instance.expects(:send_api_email).once
    DeviseMandrillMailer.send_after_signup_email(user.id, 'web', 'email').deliver_now

    user_without_email = create(:user, email: nil)
    DeviseMandrillMailer.send_after_signup_email(user_without_email.id, 'web', 'email').deliver_now
  end

  # custom template and preferred user language should depend on a org context
  # EP-21655
  test 'should send a proper user in org context to MandrillMessageGenerationService for reset password instructions' do
    org1, org2 = create_list(:organization, 2)
    user = create(:user, organization: org1, email: 'test@email.com')
    create(:user, organization: org2, email: 'test@email.com')
    receiver = nil
    MandrillMessageGenerationService.any_instance.expects(:merge_custom_attrs).with(){|args|
      receiver = args[:current_user]
    }
    user.send_reset_password_instructions

    assert_equal receiver.organization, org1
  end
end

require 'test_helper'

class WelcomeGroupMemberMailerTest < ActionMailer::TestCase

  setup do
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(false)
    @org = create(:organization)
    @private_group = create(:private_group,
                           resource_group: create(:group, organization: @org),
                           organization: @org)
    @user = create(:user)
    @another_user = create(:user)
  end

  test 'should use a welcome-group-member template if the user join the group himself rather than someone else invited him'  do
    template_name = ''
    MandrillMessageGenerationService.any_instance.expects(:merge_custom_attrs).with{|args|
      template_name = args[:html_file]
    }.once
    WelcomeGroupMemberMailer.welcome_group_member_email({from_user_id: @another_user.id, to_user_id: @user.id, group_id: @private_group.id}).deliver_later
    assert_equal 'welcome-group-member-notification.html.slim', template_name
  end

  test 'should use a join-group-member template if the user join the group himself rather than someone else invited him'  do
    template_name = ''
    MandrillMessageGenerationService.any_instance.expects(:merge_custom_attrs).with{|args|
      template_name = args[:html_file]
    }.once
    WelcomeGroupMemberMailer.welcome_group_member_email({from_user_id: @user.id, to_user_id: @user.id, group_id: @private_group.id}).deliver_later
    assert_equal 'join-group-member-notification.html.slim', template_name
  end
end
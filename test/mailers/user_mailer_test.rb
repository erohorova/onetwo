require 'test_helper'

class UserMailerTest < ActionMailer::TestCase
  include Rails.application.routes.url_helpers

  setup do
    @org = create(:organization)
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(false)
  end

  teardown do
    Organization.any_instance.unstub(:notifications_and_triggers_enabled?)
  end

  test 'should email followers on comment' do
    ActivityNotificationJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    client = create(:client, organization: @org)
    user1 = create(:user, organization: @org)
    user2 = create(:user, organization: @org)
    group = create(:resource_group, website_url: 'http://localhost/ui/demo', organization: @org, client: client)
    group.add_member user1
    group.add_member user2

    # user1 follows post automatically upon its creation
    question = create(:question, group: group, user: user1)
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:to_email] = message[:message][:to].first[:email]
      mail_args[:subject] = message[:message][:subject]
      mail_args[:invite_url] = message[:message][:global_merge_vars][19]['content']
    }.once

    comment = create(:answer, commentable: question, user: user2)
    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal user1.email, mail_args[:to_email]

    assert_match /#{question.title}/, mail_args[:subject]
    base_url = (Settings.platform_force_ssl == "true") ? 'https://' : 'http://' + Settings.platform_host + '/redirect'
    url = CGI.escape("http://localhost/ui/demo?edcast_ref=" + question.id.to_s)
    string = Regexp.escape("#{base_url}?url=#{url}")
    assert_match /#{string}/, mail_args[:invite_url]
  end

  test 'should not email suspended users who are followers on comment' do
    #user3 is suspended, should not recieve email
    user2 = create(:user)
    user3 = create(:user, is_suspended: true)
    group = create(:resource_group, website_url: 'http://localhost/ui/demo', organization: @org)
    group.add_member user2
    group.add_member user3

    question = create(:question, group: group, user: user3)

    MandrillMailer.any_instance.expects(:send_api_email).never
    comment = create(:answer, commentable: question, user: user2)
  end

  test 'should provide stripped snippet' do
    assert_equal 'This is a...',
    ActivityNotificationMailer.stripped_snippet('<p>This&nbsp;is a message of 36 characters.</p>')
  end

  test 'should not send mail if user email nil' do
    user1 = create(:user, email: nil)
    user2 = create(:user)
    group = create(:resource_group, website_url: 'http://localhost/ui/demo', organization: @org)
    group.add_member user1
    group.add_member user2

    # user1 follows post automatically upon its creation
    post = create(:post, group: group, user: user1)

    MandrillMailer.any_instance.expects(:send_api_email).never
    comment = create(:comment, commentable: post, user: user2)
  end

  test 'should not send announcement mail if user email nil' do
    user1 = create(:user, email: nil)
    user2 = create(:user)
    group = create(:resource_group, website_url: 'http://localhost/blue', organization: @org)
    group.add_member user1
    group.add_admin user2
    MandrillMailer.any_instance.expects(:send_api_email).never
    create(:announcement, group: group, user: user2)
  end

  test "should send mail for events" do
    ActivityNotificationJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    client = create(:client, organization: @org)

    user1 = create(:user, organization: @org)
    user2 = create(:user, organization: @org)

    group = create(:group, website_url: 'http://localhost/ui/demo', organization: @org, client: client)
    group.add_member user1
    group.add_member user2

    # user1 follows post automatically upon its creation
    event = create(:event, group: group, user: user1)
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:to_email] = message[:message][:to].first[:email]
      mail_args[:subject] = message[:message][:subject]
      mail_args[:link_url] = message[:message][:global_merge_vars][19]['content']
    }.once
    comment = create(:comment, commentable: event, user: user2)

    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal user1.email, mail_args[:to_email]
    assert_match /#{event.name.downcase}/, mail_args[:subject]
    base_url = (Settings.platform_force_ssl == "true") ? 'https://' : 'http://' + Settings.platform_host + '/redirect'
    url = CGI.escape("http://localhost/ui/demo?edcast_ref=" + event.id.to_s)
    string = Regexp.escape("#{base_url}?url=#{url}")
    assert_match /#{string}/, mail_args[:link_url]
  end

  test "should contain unsubscribe_token" do
    ActivityNotificationJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    client = create(:client, organization: @org)

    user1 = create(:user, organization: @org)
    user2 = create(:user, organization: @org)

    group = create(:group, organization: @org, client: client)
    group.add_member user1
    group.add_member user2

    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:unsubscribe] = message[:message][:merge_vars].first[:vars][3][:content]
    }.once

    event = create(:event, group: group, user: user1)
    comment = create(:comment, commentable: event, user: user2)
    assert_match /https:\/\/#{Settings.platform_host}\/unsubscribe?/, mail_args[:unsubscribe]
  end

  test 'should send out follower notification' do
    ActivityNotificationJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    client = create(:client, organization: @org)

    user = create(:user, organization: @org)
    student1 = create(:user, organization: @org)
    student2 = create(:user, organization: @org)
    student3 = create(:user, organization: @org)
    group = create(:resource_group, organization: @org)
    group.add_member user
    group.add_member student1
    group.add_member student2
    group.add_member student3
    post = create(:post, user: user, group: group)
    follow1 = create(:follow, user: student1, followable: post)
    follow2 = create(:follow, user: student2, followable: post)
    follow3 = create(:follow, user: student3, followable: post)
    MandrillMailer.any_instance.expects(:send_api_email).once
    comment = create(:comment, commentable: post, user: user)
  end

  test 'should send out follower notification for private groups' do
    ActivityNotificationJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    client = create(:client, organization: @org)

    user = create(:user, organization: @org)
    student1 = create(:user, organization: @org)
    group = create(:resource_group, website_url: 'https://website.com', organization: @org, client: client)
    group.add_member user
    group.add_member student1

    pg = create(:private_group, parent_id: group.id, organization: @org, client: client)
    pg.add_member user
    pg.add_member student1

    p = create(:post, user: user, group: pg)
    follow1 = create(:follow, user: student1, followable: p)

    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:invite_url] = message[:message][:global_merge_vars][19]['content']
    }.once
    comment = create(:comment, commentable: p, user: user)

    base_url = (Settings.platform_force_ssl == "true") ? 'https://' : 'http://' + Settings.platform_host + '/redirect'
    url = CGI.escape("https://website.com?edcast_ref=" + p.id.to_s)
    string = Regexp.escape("#{base_url}?url=#{url}")
    assert_match /#{string}/, mail_args[:invite_url]
  end

  test 'confirm email address' do
     email1 = create(:email, email: 'useremail1@example.com')
     mail_args = {}
     MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:to_email] = message[:message][:to].first['email']
      mail_args[:token] = message[:message][:global_merge_vars][15]['content']
      }.once

      email1.run_callbacks(:commit)

     assert_not_nil email1.confirmation_token
     assert_match email1.confirmation_token, mail_args[:token]
     assert_equal 'admin@edcast.com', mail_args[:from_email]
     assert_equal 'useremail1@example.com', mail_args[:to_email]


    # no token, should not send email
    email_no_token = create(:email, email: 's@s.com', confirmation_token: nil)
    email_no_token.run_callbacks(:commit)
  end

  test 'data dump' do
    client = create(:client, organization: @org)
    group = create(:group, name: 'Test Group', organization: @org, client: client)
    user = group.organization.client.user
    mail_args = {}
     MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:to_email] = message[:message][:to].first[:email]
      mail_args[:link_url] = message[:message][:global_merge_vars][15]['content']
      }.once
      UserMailer.data_dump(group.id, user.id, "https://www.google.com").deliver_now

    assert_match "https://www.google.com", mail_args[:link_url]
    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal user.mailable_email, mail_args[:to_email]
  end

  test "follow batch notification" do
    org = create(:organization)
    followers = create_list(:user, 2, organization: org)
    followee = create(:user, organization: org)

    mail_args = {}
     MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:to_email] = message[:message][:to].first[:email]
      mail_args[:link_url] = message[:message][:global_merge_vars][15]['content']
      mail_args[:followers] = message[:message][:global_merge_vars][17]['content']
      mail_args[:followers_count] = message[:message][:global_merge_vars][21]['content']
      mail_args[:unsubscribe_url] =  message[:message][:global_merge_vars][15]['content']
      }.once

    UserMailer.follower_batch_notification_email(followee, followers).deliver_now
    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal followee.mailable_email, mail_args[:to_email]
    assert_equal followers.count, mail_args[:followers_count]
    assert_not_nil mail_args[:unsubscribe_url]
    assert_equal mail_args[:followers].first['url'], "http://#{org.host}/#{followers.first.handle_no_at}"
  end

  test "should not send follow batch notification if triggers feature is enabled" do
    org = create(:organization)
    org.stubs(:notifications_and_triggers_enabled?).returns(true)
    followers = create_list(:user, 2, organization: org)
    followee = create(:user, organization: org)

    # create(:config, configable: org, name: 'feature/notifications_and_triggers_enabled', value: 'true', data_type: 'boolean')
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).never

    UserMailer.follower_batch_notification_email(followee, followers).deliver_now
  end

  test 'should not send follow batch notification to suspended user' do
    followers = create_list(:user, 2)
    followee = create(:user, is_suspended: true)
    MandrillMailer.any_instance.expects(:send_api_email).never
    UserMailer.follower_batch_notification_email(followee, followers).deliver_now
  end

  test 'should send channel invite emails with the right subdomain' do
    org = create(:organization, host_name: 'ge')
    channel = create(:channel, organization: org)
    user = create(:user, organization: org)
    user2 = create(:user, organization: org, is_suspended: true)
    user3 = create(:user, organization: org)
    inviter = create(:user, organization: org)

    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:invite_url] = message[:message][:global_merge_vars][15]['content']
    }.times(3)
    # collaborator email
      UserMailer.invite_collaborator(inviter,user,channel).deliver_now
    assert_match org.host, mail_args[:invite_url]

    #suspended collaborator should not recieve email
    UserMailer.invite_collaborator(inviter,user2,channel).deliver_now

    # follower email
    UserMailer.invite_follower(inviter,user,channel).deliver_now
    assert_match org.host, mail_args[:invite_url]

    #curator invite mail
    UserMailer.invite_curator(inviter,user3,channel).deliver_now
    assert_match org.host, mail_args[:invite_url]

    #suspended follower should not recieve email
    UserMailer.invite_follower(inviter,user2,channel).deliver_now
  end

  test 'should invite an user if added as a collaborator' do
    inviter = create(:user)
    user = create(:user)
    channel = create(:channel, :robotics)

    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:to_email] = message[:message][:to].first[:email]
      mail_args[:first_name] = message[:message][:global_merge_vars][14]['content']
      mail_args[:invite_url] = message[:message][:global_merge_vars][15]['content']
      mail_args[:channel_name] = message[:message][:global_merge_vars][16]['content']
      mail_args[:inviter_name] = message[:message][:global_merge_vars][17]['content']
    }.once
      UserMailer.invite_collaborator(inviter,user,channel).deliver_now

    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal user.mailable_email, mail_args[:to_email]
    assert_equal "#{inviter.first_name} #{inviter.last_name}", mail_args[:inviter_name]
    assert_equal "#{channel.label}", mail_args[:channel_name]
    assert_equal "#{user.first_name}", mail_args[:first_name]
    assert_not_nil mail_args[:invite_url]

  end

  test 'should send channel follows notification to admin' do
    ActivityNotificationJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    client = create(:client, organization: @org)

    follower = create(:user, organization_role: 'member', organization: @org)
    inviter = create(:user, organization_role: 'admin', organization: @org)
    admin_user = create(:user, organization_role: 'admin', organization: @org)
    channel = create(:channel, :robotics, organization: @org)

    channel_names = channel.label

    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:channel_name] = message[:message][:global_merge_vars][14]['content']
      mail_args[:inviter_name] = message[:message][:global_merge_vars][15]['content']
      mail_args[:message] = message[:message][:global_merge_vars][18]['content']
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:to_email] = message[:message][:to].map{|d| d[:email]}
      mail_args[:subject] = message[:message][:subject]
    }.once
    UserMailer.channel_follows_notification_to_admin(inviter, [channel_names],[follower.email], []  ).deliver_now

    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal [admin_user.mailable_email].to_set, mail_args[:to_email].to_set
    assert_equal "#{inviter.first_name} #{inviter.last_name}", mail_args[:inviter_name]
    assert_equal "#{channel.label}", mail_args[:channel_name]
    assert mail_args[:subject].include? "#{inviter.name}"
  end

  test 'should not send channel follows notification to admin' do
    org = create(:organization)
    create(:config, configable: org, name: 'feature/send_no_mails', value: 'true', data_type: 'boolean')
    follower = create(:user, organization_role: 'member', organization_id: org.id)
    inviter = create(:user, organization_role: 'admin', organization_id: org.id)
    admin_user = create(:user, organization_role: 'admin', organization_id: org.id)
    channel = create(:channel, :robotics)
    channel_names = channel.label

    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:channel_name] = message[:message][:global_merge_vars][14]['content']
      mail_args[:inviter_name] = message[:message][:global_merge_vars][15]['content']
      mail_args[:message] = message[:message][:global_merge_vars][18]['content']
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:to_email] = message[:message][:to].map{|d| d[:email]}
      mail_args[:subject] = message[:message][:subject]
    }.never
    UserMailer.channel_follows_notification_to_admin(inviter, [channel_names],[follower.email], []).deliver_now
  end

  test 'should invite an user if added as a follower' do
    inviter = create(:user)
    user = create(:user)
    channel = create(:channel, :robotics)

    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:to_email] = message[:message][:to].first[:email]
      mail_args[:first_name] = message[:message][:global_merge_vars][14]['content']
      mail_args[:invite_url] = message[:message][:global_merge_vars][15]['content']
      mail_args[:channel_name] = message[:message][:global_merge_vars][16]['content']
      mail_args[:inviter_name] = message[:message][:global_merge_vars][17]['content']
    }.once

    UserMailer.invite_follower(inviter,user,channel).deliver_now

    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal user.mailable_email, mail_args[:to_email]
    assert_equal "#{inviter.first_name} #{inviter.last_name}", mail_args[:inviter_name]
    assert_equal "#{channel.label}", mail_args[:channel_name]
    assert_equal "#{user.first_name}", mail_args[:first_name]
    assert_not_nil mail_args[:invite_url]
  end

  test 'should not send daily digest to suspended user' do
    suspended_user = create(:user, is_suspended: true)
    user1 = create(:user)
    group = create(:resource_group, website_url: 'http://localhost/ui/demo')
    group.add_member user1
    group.add_member suspended_user
    post = create(:post,user_id: user1.id, postable_id: group.id, postable_type: 'Group')
    MandrillMailer.any_instance.expects(:send_api_email).never
      UserMailer.send_daily_digest(group,suspended_user, post).deliver_now
  end

  test 'should send daily digest even if triggers feature is enabled' do
    user = create(:user)
    user1 = create(:user)
    user.organization.stubs(:notifications_and_triggers_enabled?).returns(true)
    group = create(:resource_group, website_url: 'http://localhost/ui/demo')
    group.add_member user1
    group.add_member user


    post = create(:post,user_id: user1.id, postable_id: group.id, postable_type: 'Group')
    MandrillMailer.any_instance.expects(:send_api_email).once
    UserMailer.send_daily_digest(group,user, post).deliver_now
  end

  test 'should not send daily digest if send_no_mails is true' do
    user = create(:user)
    user1 = create(:user)
    user.organization.stubs(:send_no_mails?).returns(true)
    group = create(:resource_group, website_url: 'http://localhost/ui/demo')
    group.add_member user1
    group.add_member user


    post = create(:post,user_id: user1.id, postable_id: group.id, postable_type: 'Group')
    MandrillMailer.any_instance.expects(:send_api_email).never
    UserMailer.send_daily_digest(group,user, post).deliver_now
  end

  test 'should not send instructor daily digest to suspended_user' do
    suspended_user = create(:user, is_suspended: true)
    url = 'www.dummy.url'
    language = 'English'
    MandrillMailer.any_instance.expects(:send_api_email).never
    UserMailer.send_instructor_daily_digest_report(suspended_user.id, url, language).deliver_now
  end

  test 'should not send instructor daily digest if send_no_mails is true' do
    user = create(:user)
    url = 'www.dummy.url'
    language = 'English'
    Organization.any_instance.stubs(:send_no_mails?).returns(true)
    MandrillMailer.any_instance.expects(:send_api_email).never
    UserMailer.send_instructor_daily_digest_report(user.id, url, language).deliver_now
  end

  test 'should send instructor daily digest if triggers and notifications is true' do
    user = create(:user)
    url = 'www.dummy.url'
    language = 'English'
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(true)
    MandrillMailer.any_instance.expects(:send_api_email).times(1)
    UserMailer.send_instructor_daily_digest_report(user.id, url, language).deliver_now
  end

  test 'should not send instructor daily mails if send no mails is true' do
    user = create(:user)
    data = {}
    Organization.any_instance.stubs(:send_no_mails?).returns(true)
    MandrillMailer.any_instance.expects(:send_api_email).never
    UserMailer.send_daily_instructor_mails(user, data).deliver_now
  end

  test 'should send instructor daily mails if triggers and notifications is true' do
    user = create(:user)
    data = {}
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(true)
    MandrillMailer.any_instance.expects(:send_api_email).times(1)
    UserMailer.send_daily_instructor_mails(user, data).deliver_now
  end

  test 'should send assignment email for individual assignments too' do
    org = create(:organization)
    user = create(:user, organization_id: org.id)
    assignor = create(:user, organization_id: org.id)
    card = create(:card,author_id: user.id, organization_id: org.id)

    assignment = create(:assignment, assignee: user, assignable: card)
    team_assignment = create(:team_assignment, assignment: assignment, assignor: assignor, team: nil, message: "this is a test message")
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:to_email] = message[:message][:to].first[:email]
      mail_args[:first_name] = message[:message][:global_merge_vars][14]['content']
      mail_args[:assignment_message] = message[:message][:global_merge_vars][15]['content']
      mail_args[:assignment_url] = message[:message][:global_merge_vars][19]['content']
    }.once

      team_assignment.run_callbacks(:commit)

     assert_equal 'admin@edcast.com', mail_args[:from_email]
     assert_equal user.mailable_email, mail_args[:to_email]
     assert_equal "#{user.name}", mail_args[:first_name]
     assert_equal "this is a test message", mail_args[:assignment_message]
     refute mail_args[:assignment_url].index('https://95jo.test-app.link/').nil?
  end

  test 'should show due date to assignee in the email notification' do
    org = create(:organization)
    user = create(:user, organization_id: org.id)
    assignor = create(:user, organization_id: org.id)
    card = create(:card,author_id: user.id, organization_id: org.id)

    assignment = create(:assignment, assignee: user, assignable: card, due_at: Date.tomorrow)
    team_assignment = create(:team_assignment, assignment: assignment, assignor: assignor, team: nil, message: "this is a test message")
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:assignment_due_at] = message[:message][:global_merge_vars][22]['content']
    }.once

    team_assignment.run_callbacks(:commit)
    assert_equal assignment.due_at, mail_args[:assignment_due_at]
  end

  test 'should show start date to assignee in the email notification' do
    org = create(:organization)
    user = create(:user, organization_id: org.id)
    assignor = create(:user, organization_id: org.id)
    card = create(:card,author_id: user.id, organization_id: org.id)

    assignment = create(:assignment, assignee: user, assignable: card, due_at: Date.tomorrow, start_date: Date.today)
    team_assignment = create(:team_assignment, assignment: assignment, assignor: assignor, team: nil, message: "this is a test message")
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:assignment_start_date] = message[:message][:global_merge_vars][23]['content']
    }.once

    team_assignment.run_callbacks(:commit)
    assert_equal assignment.start_date, mail_args[:assignment_start_date]
  end

  test "should send assignment email notification if triggers feature is disabled" do
    org = create(:organization)

    user = create(:user, organization_id: org.id)
    assignor = create(:user, organization_id: org.id)
    card = create(:card,author_id: user.id, organization_id: org.id)

    assignment = create(:assignment, assignee: user, assignable: card)
    team_assignment = create(:team_assignment, assignment: assignment, assignor: assignor, team: nil, message: "this is a test message")
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:to_email] = message[:message][:to].first[:email]
      mail_args[:first_name] = message[:message][:global_merge_vars][14]['content']
      mail_args[:assignment_message] = message[:message][:global_merge_vars][15]['content']
    }.once

    team_assignment.run_callbacks(:commit)
  end

  test "should not send assignment email if notification and triggers feature is enabled" do
    org = create(:organization)

    user = create(:user, organization_id: org.id)
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(true)
    assignor = create(:user, organization_id: org.id)
    card = create(:card,author_id: user.id, organization_id: org.id)

    assignment = create(:assignment, assignee: user, assignable: card)
    team_assignment = create(:team_assignment, assignment: assignment, assignor: assignor, team: nil, message: "this is a test message")
    MandrillMailer.any_instance.expects(:send_api_email).never

    team_assignment.run_callbacks(:commit)
    Organization.any_instance.unstub(:notifications_and_triggers_enabled?)
  end

  test "send weekly activity email" do
    org = create(:organization)
    user = create(:user, organization_id: org.id)
    user2 = create(:user, organization_id: org.id)
    cards = create_list(:card, 3 ,author_id: user.id, organization_id: org.id)
    video_streams = create_list(:wowza_video_stream, 3, creator:user)
    activity = {}
    activity[:insights] = cards
    activity[:video_streams] = video_streams
    activity[:pathways] = []
    stats = {
        points_earned: 20,
        team_standing: 1,
        avg_points_earned_by_team: 2133
      }

    mail_args = {}
    MandrillMessageGenerationService.any_instance.expects(:merge_custom_attrs).with(){|args|
      mail_args[:org_url] = args[:global_merge_vars][16]['content']
      mail_args[:to_email] = args[:to].map{|d| d[:email]}
      mail_args[:from_email] = args[:from_email]
      mail_args[:points_earned] = args[:global_merge_vars][20]['content']
      mail_args[:team_standing] =args[:global_merge_vars][21]['content']
      mail_args[:avg_points_earned_by_team] =args[:global_merge_vars][23]['content']
    }.once

    UserMailer.weekly_activity(org, org.users, activity, stats).deliver_now
    assert_equal org.home_page, mail_args[:org_url]
    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal [user.mailable_email, user2.mailable_email].to_set, mail_args[:to_email].to_set

    assert_equal "20", mail_args[:points_earned]
    assert_equal 1, mail_args[:team_standing]
    assert_equal "2,133", mail_args[:avg_points_earned_by_team]
  end

  test "should not send weekly activity email to suspended users" do
    #should not send suspended users weekly activity mail
    org = create(:organization)
    user = create(:user, organization_id: org.id, is_suspended: true)
    user1 = create(:user, organization_id: org.id, is_suspended: false)
    cards = create_list(:card, 3 ,author_id: user.id, organization_id: org.id)
    video_streams = create_list(:wowza_video_stream, 3, creator:user)
    stats = {
        points_earned: 0,
        team_standing: 0,
        avg_points_earned_by_team: 0
      }
    activity = {}
    activity[:insights] = cards
    activity[:video_streams] = video_streams
    activity[:pathways] = []

    MandrillMailer.any_instance.expects(:send_api_email).once
    UserMailer.weekly_activity(org, org.users, activity, stats).deliver_now
  end

  test "send weekly activity back email if no activity" do
    org = create(:organization)
    user = create(:user, organization_id: org.id)
    cards = create_list(:card, 1 ,author_id: user.id, organization_id: org.id)
    activity = {}
    activity[:insights] = cards
    activity[:video_streams] = []
    activity[:pathways] = []

    mail_args ={}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:to_email] = message[:message][:to].map{|d| d[:email]}
    }.once

    stats = {
      points_earned: 0,
      team_standing: 0,
      avg_points_earned_by_team: 0
    }
    UserMailer.weekly_activity(org, org.users, activity, stats).deliver_now


    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal [user.mailable_email], mail_args[:to_email]
  end

  test "send magic link email to user" do
    org = create(:organization)
    user = create(:user, organization_id: org.id)
    token = "crazy-token"
    desktop_url = magic_link_login_api_v2_users_url(token: token, host: org.host, protocol: 'https')

    mail_args ={}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:to_email] = message[:message][:to].map{|d| d[:email]}
      mail_args[:first_name] = message[:message][:global_merge_vars][14]['content']
      mail_args[:magic_link] = message[:message][:global_merge_vars][15]['content']
    }.once
    UserMailer.magic_link(user, token).deliver_now

    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal [user.mailable_email], mail_args[:to_email]
    assert_equal  user.first_name, mail_args[:first_name]
    assert_equal  desktop_url, mail_args[:magic_link]
  end

  test 'should send organization analytics report to user' do
    org = create(:organization)
    user = create(:user, organization_id: org.id)

    timestamp = Time.now.utc
    s3_url = "https://s3.amazon.com/ad"

    mail_args ={}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:to_email] = message[:message][:to].map{|d| d[:email]}
      mail_args[:first_name] = message[:message][:global_merge_vars][14]['content']
      mail_args[:s3_url] = message[:message][:global_merge_vars][17]['content']
    }.once
    UserMailer.send_organization_report(s3_url: s3_url, user_ids: [user.id], email_title_snippet: timestamp.strftime("%m/%d/%Y")).deliver_now

    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal [user.mailable_email], mail_args[:to_email]
    assert_equal  user.first_name, mail_args[:first_name]
    assert_equal  s3_url, mail_args[:s3_url]
  end

  test 'should send assessment report email' do
    org = create(:organization)
    author = create(:user, organization: org)
    card = create(:card, author: author)

    s3_url = mock()

    mail_args ={}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:to_email] = message[:message][:to].map{|d| d[:email]}
      mail_args[:first_name] = message[:message][:global_merge_vars][14]['content']
      mail_args[:s3_url] = message[:message][:global_merge_vars][17]['content']
      mail_args[:card_title] = message[:message][:global_merge_vars][18]['content']
    }.once
    UserMailer.send_assessment_report(s3_url: s3_url, user_id: author.id, card_title: card.snippet).deliver_now

    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal [author.mailable_email], mail_args[:to_email]
    assert_equal  author.first_name, mail_args[:first_name]
    assert_equal  s3_url, mail_args[:s3_url]
    assert_equal  card.snippet, mail_args[:card_title]
  end

  test 'sends courses-report email for valid user' do
    org  = create(:organization)
    user = create(:user, organization: org)

    s3_url = mock()

    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:to_email] = message[:message][:to].map{|d| d[:email]}
      mail_args[:first_name] = message[:message][:global_merge_vars][14]['content']
      mail_args[:s3_url] = message[:message][:global_merge_vars][17]['content']
    }.once

    UserMailer.send_courses_report(s3_url: s3_url, user: user).deliver_now

    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal [user.mailable_email], mail_args[:to_email]
    assert_equal  user.first_name, mail_args[:first_name]
    assert_equal  s3_url, mail_args[:s3_url]
  end

  test 'sends courses-report email using custom sender address' do
    org  = create(:organization)
    user = create(:user, organization: org)
    create(:mailer_config, organization: org, from_address: 'abc@xyz.com', from_name: 'abc')

    s3_url = mock()

    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:from_name] = message[:message][:from_name]
    }.once

    UserMailer.send_courses_report(s3_url: s3_url, user: user).deliver_now

    assert_equal 'abc', mail_args[:from_name]
    assert_equal 'abc@xyz.com', mail_args[:from_email]
  end

  test 'does not send courses-report email for suspended user' do
    org  = create(:organization)
    user = create(:user, organization: org, is_suspended: true)

    s3_url = mock()
    MandrillMailer.any_instance.expects(:send_api_email).never
    UserMailer.send_courses_report(s3_url: s3_url, user: user).deliver_now
  end

  test 'should not send assessment report email' do
    org = create(:organization)
    author = create(:user, organization: org, is_suspended: true)
    card = create(:card, author: author)

    s3_url = mock()
    MandrillMailer.any_instance.expects(:send_api_email).never
    assert_raise do
      UserMailer.send_assessment_report(s3_url: s3_url, user_id: [author.id], card_title: card.snippet).deliver_now
    end
  end

  test 'should not send the data request mail if the user does not exist' do
    MandrillMailer.any_instance.expects(:send_api_email).never
    UserMailer.send_user_data_export_request(13).deliver_now
  end

  test 'should send the data request mail to the support email' do
    user = create(:user)
    
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:from_name] = message[:message][:from_name]
      mail_args[:to] = message[:message][:to][0][:email]  
      mail_args[:subject] = message[:message][:subject]
    }.once

    UserMailer.send_user_data_export_request(user.id).deliver_now
    assert_equal "Admin User", mail_args[:from_name]
    assert_equal "admin@edcast.com", mail_args[:from_email]
    assert_equal Settings.support_email, mail_args[:to]
    assert_equal "GDPR: Data Export Request from #{user.organization.host}",mail_args[:subject]
  end

end

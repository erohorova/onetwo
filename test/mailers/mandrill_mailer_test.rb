require 'test_helper'

class MandrillMailerTest < ActionMailer::TestCase

  class MandrillDummyMailer < MandrillMailer
    def test_transaction_email(users, org, common_vars)
      recipient_vars = {}
      users.each do |user|
        recipient_vars[user.email] = {first_name: user.first_name}
      end
      global_merge_vars = []
      if org.present?
        global_merge_vars, subject = get_org_custom_config(org, global_merge_vars, 'dummy subject')
      end
      mail(to: users.map(&:email), from: 'admin@test.com',
           template: 'test-template',
           recipient_vars: recipient_vars,
           common_vars: common_vars.merge({unsubscribe_url: "http://someurl"}),
           global_merge_vars: global_merge_vars
      )
    end
  end


  test 'should use mandrill smtp settings' do
    assert_not_equal ActionMailer::Base.smtp_settings, MandrillMailer.smtp_settings
    assert_equal Settings.mandrill.address, MandrillMailer.smtp_settings[:address]
  end

  test 'should support adding recipient_vars' do

    user1 = create(:user, first_name: 'name1')
    user2 = create(:user, first_name:'name2')

    MandrillDummyMailer.test_transaction_email([user1, user2], nil, {foo: 'bar'}).deliver_now

    mail = ActionMailer::Base.deliveries.last
    assert_equal 'test-template', mail['X-MC-Template'].value
    assert_equal 3, mail['X-MC-MergeVars'].length
    raw_merge_vars_headers = mail.header.to_s
    assert_includes raw_merge_vars_headers, '{"foo":"bar","unsubscribe_url":"http://someurl"}'
    assert_includes raw_merge_vars_headers, %Q{{"_rcpt":"#{user1.email}","first_name":"name1"}}
    assert_includes raw_merge_vars_headers, %Q{{"_rcpt":"#{user2.email}","first_name":"name2"}}
  end

  class RealEmailTest < MandrillMailer
    #test real email
    self.delivery_method = :smtp
    self.smtp_settings = self.smtp_settings.merge({user_name: 'trung@edcast.com', password: '13lJBFEoRACaPjE4fkAVRA', domain: 'edcast.com'})
    def test_my_email
      vars = {}
      vars['trung@phamcom.com'] =  {name: 'TrungPhamCom', payload: 'a'*10000}
      vars['trung@edcast.com']=  {name: 'TrungEdcast', payload: 'b'*10000}
      address = Mail::Address.new
      address.address = 'trung@phamcom.com'
      address.display_name = 'Pham, Trung "me"'
      mail(to: [address.format, 'trung@edcast.com'], recipient_vars: vars, from: 'admin@edcast.com', template: 'test-template', attachments: {'logo.png' => File.read(Rails.root.join('test/fixtures/images/logo.png'))})
    end
  end
  test 'real email sending' do
    #comment this out for now. do not really want to send out a real email on every run.
    #RealEmailTest.test_my_email.deliver_now
  end

  test 'should escape name' do
    res = MandrillMailer.send(:new).send(
        :formatted_email_address_from_email_and_name,
        'e@mail.com',
        'I have a , in my name and "double quote"',
    )
    assert_equal res, %Q{"I have a , in my name and \\"double quote\\"" <e@mail.com>}
  end

  test 'should not send mail to suspended user' do
    org = create :organization
    user1 = create :user, organization: org, is_suspended: true
    activity = {:insights=>[], :video_streams=>[], :pathways=>[]}
    stats = {
        time_spent: 23000,
        points_earned: 20,
        team_standing: 1,
        avg_time_spent: 12333,
        avg_points_earned: 2133
      }
    MandrillMailer.any_instance.expects(:send_api_email).never
    UserMailer.weekly_activity( org, org.users, activity, stats).deliver_now
  end


  test 'should get default options if not provided' do
    org = create :organization, name: 'Org'
    user = create(:user, first_name: 'name1', organization: org)

    MandrillDummyMailer.test_transaction_email([user], org, {foo: 'bar'}).deliver_now

    mail = ActionMailer::Base.deliveries.last
    var_headers = mail.header['global-merge-vars'].as_json['unparsed_value']
    assert_equal 'EdCast', var_headers[6]['content']
    assert_equal Settings.ios_app_location, var_headers[7]['content']
    assert_equal Settings.android_app_location, var_headers[8]['content']
    assert_equal Settings.support_email, var_headers[9]['content']
  end

  test 'should get custom options if provided' do
    org = create :organization, name: 'CustomOrg'
    user = create(:user, first_name: 'name1', organization: org)
    config_hash = {
        "appStore" => 'customappstoreapplink',
        "playStore" => 'customplaystoreapplink',
        "supportEmail" => 'support@custom.com'
    }
    config = create :config, name: 'footer_options', configable_id: org.id, configable_type: 'Organization',
        value: config_hash.to_json, data_type: 'json'

    MandrillDummyMailer.test_transaction_email([user], org, {foo: 'bar'}).deliver_now

    mail = ActionMailer::Base.deliveries.last
    var_headers = mail.header['global-merge-vars'].as_json['unparsed_value']
    assert_equal 'CustomOrg', var_headers[6]['content']
    assert_equal 'customappstoreapplink', var_headers[7]['content']
    assert_equal 'customplaystoreapplink', var_headers[8]['content']
    assert_equal 'support@custom.com', var_headers[9]['content']
  end


  test 'should get default css if not provided' do
    org = create :organization, name: 'Org'
    user = create(:user, first_name: 'name1', organization: org)

    MandrillDummyMailer.test_transaction_email([user], org, {foo: 'bar'}).deliver_now

    mail = ActionMailer::Base.deliveries.last
    var_headers = mail.header['global-merge-vars'].as_json['unparsed_value']
    assert_equal "##{Settings.mail_css.header_background_color}", var_headers[0]['content']
    assert_equal "##{Settings.mail_css.text_color}", var_headers[1]['content']
    assert_equal "##{Settings.mail_css.section_text_color}", var_headers[2]['content']
    assert_equal "##{Settings.mail_css.highlight_text_color}", var_headers[3]['content']
    assert_equal "##{Settings.mail_css.button_blue_color}", var_headers[4]['content']
    assert_equal "##{Settings.mail_css.button_background_color}", var_headers[5]['content']
  end

  test 'should get default css if custom css enabled off' do
    org = create :organization, name: 'Org'
    user = create(:user, first_name: 'name1', organization: org)
    config_hash = { "enableCustomCss" => false, "headerBackgroundColor" => '#e3452d',
        "textColor" => '#000000', "buttonBackgroundColor" => '#45e324' }

    config = create :config, name: 'mail_custom_css', configable_id: org.id, configable_type: 'Organization',
        value: config_hash.to_json, data_type: 'json'

    MandrillDummyMailer.test_transaction_email([user], org, {foo: 'bar'}).deliver_now

    mail = ActionMailer::Base.deliveries.last
    var_headers = mail.header['global-merge-vars'].as_json['unparsed_value']
    assert_equal "##{Settings.mail_css.header_background_color}", var_headers[0]['content']
    assert_equal "##{Settings.mail_css.text_color}", var_headers[1]['content']
    assert_equal "##{Settings.mail_css.section_text_color}", var_headers[2]['content']
    assert_equal "##{Settings.mail_css.highlight_text_color}", var_headers[3]['content']
    assert_equal "##{Settings.mail_css.button_blue_color}", var_headers[4]['content']
    assert_equal "##{Settings.mail_css.button_background_color}", var_headers[5]['content']
  end

  test 'should get org specific css if provided' do
    org = create :organization, name: 'Org'
    user = create(:user, first_name: 'name1', organization: org)
    config_hash = { "enableCustomCss" => true, "headerBackgroundColor" => '#e3452d',
        "textColor" => '#000000', "buttonBackgroundColor" => '#45e324' }

    config = create :config, name: 'mail_custom_css', configable_id: org.id, configable_type: 'Organization',
        value: config_hash.to_json, data_type: 'json'

    MandrillDummyMailer.test_transaction_email([user], org, {foo: 'bar'}).deliver_now

    mail = ActionMailer::Base.deliveries.last
    var_headers = mail.header['global-merge-vars'].as_json['unparsed_value']
    assert_equal config_hash["headerBackgroundColor"], var_headers[0]['content']
    assert_equal config_hash["textColor"], var_headers[1]['content']
    assert_equal config_hash["textColor"], var_headers[2]['content']
    assert_equal config_hash["textColor"], var_headers[3]['content']
    assert_equal config_hash["buttonBackgroundColor"], var_headers[4]['content']
    assert_equal config_hash["buttonBackgroundColor"], var_headers[5]['content']
  end

  test 'should not show edcast logo if disabled by org' do
    org = create :organization, name: 'Org'
    user = create(:user, first_name: 'name1', organization: org)
    config = create :config, name: 'enable_edcast_logo', configable: org, value: false, data_type: 'boolean'

    MandrillDummyMailer.test_transaction_email([user], org, {foo: 'bar'}).deliver_now

    mail = ActionMailer::Base.deliveries.last
    var_headers = mail.header['global-merge-vars'].as_json['unparsed_value']
    assert_equal false, var_headers[11]['content']
  end

  test 'should show edcast logo org config not present' do
    org = create :organization, name: 'Org'
    user = create(:user, first_name: 'name1', organization: org)

    MandrillDummyMailer.test_transaction_email([user], org, {foo: 'bar'}).deliver_now

    mail = ActionMailer::Base.deliveries.last
    var_headers = mail.header['global-merge-vars'].as_json['unparsed_value']
    assert_equal true, var_headers[11]['content']
  end

  test 'send email to user when created from bulk-import' do
    create_default_org
    stub_request(:post, "https://api.branch.io/v1/url").
      to_return(:status => 200, :body => "", :headers => {})
    org = create(:organization)
    user = create(:user, organization: org)
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:to_email] = message[:message][:to].first[:email]
      mail_args[:imp_keys] = message[:message][:global_merge_vars].map{|d| d['name']}
    }.once

    DeviseMandrillMailer.send(:send_import_user_welcome_email, { user_id: user.id }).deliver_now

    email = ActionMailer::Base.deliveries.last

    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal user.email, mail_args[:to_email]
    assert_same_elements mail_args[:imp_keys], ["header_background_color", "text_color", "section_text_color", "highlight_text_color",
      "button_blue_color", "button_background_color", "org_display_name", "appstore_link", "playstore_link", "support_email",
      "show_edcast_name", "show_edcast_logo","promotions","home_page", "first_name", "subject", "org_login_link", 'team_name', 'org_logo']
  end
end

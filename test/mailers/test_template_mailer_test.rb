# frozen_string_literal: true

require 'test_helper'

class TestTemplateMailerTest < ActionMailer::TestCase
  setup do
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(false)
    org = create(:organization)
    @user = create(:user, organization: org, email: 'test_user@email.com')
    content = <<-TXT.strip_heredoc
      <!DOCTYPE html>
      <html>
        <body>
          <p> Some content and variable: {{custom_variable}}
        </body>
      </html>
    TXT
    @email_template = EmailTemplate.create(
      is_active: false,
      title: 'title', content: content,
      organization: org, design: 'design', language: 'en'
    )
  end

  test 'send email with custom template' do
    expected_vars = [{"name": 'custom_variable', "content": 'custom_variable'}]
    expected_subject = "Test email template #{@email_template.title}"
    expected_to = [
      {
        "email": @user.email,
        "name": @user.name,
        "type": 'to'
      }
    ]
    MandrillMessageGenerationService.any_instance.expects(
      :merge_custom_attrs
    ).with(
      test_email: true,
      html_file: @email_template.content,
      subject: expected_subject,
      from_email: 'admin@edcast.com',
      from_name: 'EdCast template preview',
      to: expected_to,
      mailer: @user.organization.mailer_config,
      global_merge_vars: expected_vars
    ).once

    TestTemplateMailer.send(
      :send_custom_template,
      {
        template_content: @email_template.content,
        title: @email_template.title,
        user_id: @user.id
      }
    ).deliver_now
  end

  test 'Do not send email when user email not present' do
    @user.update(email: nil)

    TestTemplateMailer.any_instance.expects(:send_api_email).never

    TestTemplateMailer.send(
      :send_custom_template,
      {
        template_content: @email_template.content,
        title: @email_template.title,
        user_id: @user.id
      }
    )    
  end  
end

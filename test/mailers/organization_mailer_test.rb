
require 'test_helper'

class OrganizationMailerTest < ActionMailer::TestCase
  include Rails.application.routes.url_helpers

  test "should send reminder mail to admin" do
    org = create(:organization, created_at: 2.days.ago)
    admin = create(:user, organization_id: org.id, organization_role: 'admin')

    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:to_email] = message[:message][:to].first[:email]
      mail_args[:user_name] = message[:message][:global_merge_vars][14]['content']
      mail_args[:team_url] = message[:message][:global_merge_vars][15]['content']
      mail_args[:forgot_password_url] = message[:message][:global_merge_vars][16]['content']
    }.once
    OrganizationMailer.reminder_mail_to_add_users(org, admin).deliver_now

    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal admin.mailable_email, mail_args[:to_email]
    assert_equal "#{admin.name}", mail_args[:user_name]
    assert_equal org.home_page, mail_args[:team_url]
    assert_equal new_user_password_url(protocol:'https', host: org.host ), mail_args[:forgot_password_url]
  end

  test 'should send reminder mail to admin using custom sender address with mailer config' do
    org = create(:organization)
    admin = create(:user, organization_id: org.id, organization_role: 'admin')
    mailer_config = create(:mailer_config, organization: org, from_address: 'abc@xyz.com', from_name: 'abc')

    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:from_name] = message[:message][:from_name]
    }.once
    OrganizationMailer.reminder_mail_to_add_users(org, admin).deliver_now

    assert_equal 'abc@xyz.com', mail_args[:from_email]
    assert_equal 'abc', mail_args[:from_name]
  end
end

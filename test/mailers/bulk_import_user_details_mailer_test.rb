require 'test_helper'

class BulkImportUserDetailsMailerTest < ActionMailer::TestCase

  setup do
    org = create(:organization)
    @admin = create(:user, organization: org, organization_role: 'admin')
  end

  test 'send email to admin with bulk-import users and group details' do
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:merge_language] = message[:message]['merge_language']
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:to_email] = message[:message][:to].first[:email]
      mail_args[:imp_keys] = message[:message][:global_merge_vars].map{|d| d['name']}

    }.once
    BulkImportUserDetailsMailer.send(:notify_admin, {
      count:      { 'new_users_count' => 5 },
      new_groups: { 'new_test' => 3 },
      old_groups: { 'old_test' => 2 },
      admin: @admin,
      upload_time: Time.now
    }).deliver_now

    email = ActionMailer::Base.deliveries.last

    assert_equal  "handlebars", mail_args[:merge_language]
    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal @admin.email, mail_args[:to_email]
    assert_same_elements mail_args[:imp_keys], ["header_background_color", "text_color", "section_text_color", "highlight_text_color",
      "button_blue_color", "button_background_color", "org_display_name", "appstore_link", "playstore_link", "support_email", "show_edcast_name",
      "show_edcast_logo","promotions","home_page", "total_users_count", "new_users_count", "old_users_count", "resent_emails_count", "invitees_count", "failed_users_count",
      "channels_names_list", "new_groups_count", "new_groups", "old_groups", "upload_time", "first_name", "show_app_download", "org_logo"]
  end

  test 'send email to admin for bulk import users using custom sender address' do
    create(:mailer_config, organization: @admin.organization, from_address: 'abc@xyz.com', from_name: 'abc')
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:from_name] = message[:message][:from_name]
    }.once

    BulkImportUserDetailsMailer.send(:notify_admin, {
      count:      { 'new_users_count' => 5 },
      new_groups: { 'new_test' => 3 },
      old_groups: { 'old_test' => 2 },
      admin: @admin,
      upload_time: Time.now
    }).deliver_now

    assert_equal 'abc', mail_args[:from_name]
    assert_equal 'abc@xyz.com', mail_args[:from_email]
  end

  test 'send email to admin for bulk import users using custom template if enable config emailTemplatesEnabled' do
    create(:config, name: 'emailTemplatesEnabled', value: true, configable: @admin.organization)
    default_template = create(
      :email_template, organization: @admin.organization,
      title: 'edcast-bulk-import-admin'
    )
    custom_template = create(
      :email_template, default_id: default_template.id, language: 'en',
      content: 'simple_content', design: 'simple_design', creator_id: @admin.id,
      is_active: true, state: 'published', title: 'edcast-bulk-import-admin_custom',
      organization: @admin.organization
    )
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:html] = message[:message][:html]
    }.once

    BulkImportUserDetailsMailer.send(:notify_admin, {
        count:      { 'new_users_count' => 5 },
        new_groups: { 'new_test' => 3 },
        old_groups: { 'old_test' => 2 },
        admin: @admin,
        upload_time: Time.now
    }).deliver_now

    assert_equal custom_template.content, mail_args[:html]
  end

  test 'send email to admin for bulk import users using default template if disable config emailTemplatesEnabled' do
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:html] = message[:message][:html]
    }.once

    BulkImportUserDetailsMailer.send(:notify_admin, {
        count:      { 'new_users_count' => 5 },
        new_groups: { 'new_test' => 3 },
        old_groups: { 'old_test' => 2 },
        admin: @admin,
        upload_time: Time.now
    }).deliver_now

    template = "edcast-bulk-import-admin.html.slim"
    html_template = MandrillMessageGenerationService.new.send(:read_from_file, template)
    assert_equal html_template, mail_args[:html]
  end
end

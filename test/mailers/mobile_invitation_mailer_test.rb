require 'test_helper'

class MobileInvitationMailerTest < ActionMailer::TestCase

  setup do
    @user = create(:user)
    @invitation = MobileInvitation.create(recipient_email: @user.email,
                                          first_name: @user.first_name,
                                          last_name: @user.last_name)
  end

  test 'send invitation email' do
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:to_email] = message[:message][:to].first['email']
      mail_args[:invite] = message[:message][:global_merge_vars][3]['content']
    }.at_least(1)

    MobileInvitationMailer.send(:invite, @invitation.id).deliver_now

    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal @invitation.recipient_email, mail_args[:to_email]
    assert_not_nil @invitation.token
    assert_match "", mail_args[:invite]
  end

  test 'resend email' do
    MandrillMailer.any_instance.expects(:send_api_email).once
      @invitation.resend_mail
  end

  test 'send invitation email using custom sender address' do
    create(:mailer_config, organization: @user.organization, from_address: 'abc@xyz.com', from_name: 'abc')

    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_email] = message[:message][:from_email]
      mail_args[:from_name] = message[:message][:from_name]
    }.once

    MobileInvitationMailer.send(:invite, @invitation.id).deliver_now
    assert_equal 'abc', mail_args[:from_name]
    assert_equal 'abc@xyz.com', mail_args[:from_email]
  end
end

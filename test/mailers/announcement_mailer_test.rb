require 'test_helper'
class AnnouncementMailerTest < ActionMailer::TestCase

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'

    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(false)
    @org = create(:organization)
    @user = create(:user)
    @user1 = create(:user)
    @user3 = create(:user, is_suspended: true)
    @group = create(:resource_group, organization: @org)
    @group.add_member @user
    @group.add_member @user1
    @post = create(:question, user: @user, group: @group)
  end

  test 'send announcemnet mail with attachments' do
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)
    @post.file_resources << fr1
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:attachments] = message[:message][:attachments]
      mail_args[:to_email] = message[:message][:to].first[:email]
      mail_args[:from_email] = message[:message][:from_email]
    }
    AnnouncementMailer.announcement_email(post: @post, users: [@user1]).deliver_now
    assert_equal 'admin@edcast.com', mail_args[:from_email]
    assert_equal @user1.email, mail_args[:to_email]
    assert_equal  1, mail_args[:attachments].length
  end

  test 'should not send announcemnet mail if org has set send_no_mails' do
    Organization.any_instance.stubs(:send_no_mails?).returns(true)

    MandrillMailer.any_instance.expects(:send_api_email).never
    AnnouncementMailer.announcement_email(post: @post, users: [@user1]).deliver_now
  end

  test 'should send announcemnet mail if org has set send_no_mails off' do
    Organization.any_instance.stubs(:send_no_mails?).returns(false)

    MandrillMailer.any_instance.expects(:send_api_email).times(1)
    AnnouncementMailer.announcement_email(post: @post, users: [@user1]).deliver_now
  end

  test 'should send announcemnet mail if org has set notifications_and_triggers_enabled on' do
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(true)

    MandrillMailer.any_instance.expects(:send_api_email).times(1)
    AnnouncementMailer.announcement_email(post: @post, users: [@user1]).deliver_now
  end

  test 'should not send announcemnet mail with attachments to suspended users' do
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)
    @post.file_resources << fr1

    MandrillMailer.any_instance.expects(:send_api_email).never
    AnnouncementMailer.announcement_email(post: @post, users: [@user3]).deliver_now
  end

  test "send announcemnet mail without attachments" do
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:attachments] = message[:message][:attachments]
      mail_args[:to_email] = message[:message][:to].first[:email]
      mail_args[:from_email] = message[:message][:from_email]
    }

    AnnouncementMailer.announcement_email(post: @post, users: [@user1]).deliver_now
    assert_nil mail_args[:attachments]
  end

  test "send announcemnet mail with custom sender address using mailer config" do
    create(:mailer_config, organization: @org, from_address: 'abc@xyz.com', from_name: 'abc')
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:from_name] = message[:message][:from_name]
      mail_args[:from_email] = message[:message][:from_email]
    }

    AnnouncementMailer.announcement_email(post: @post, users: [@user1]).deliver_now
    assert_equal 'abc@xyz.com', mail_args[:from_email]
    assert_equal 'abc', mail_args[:from_name]
  end

  test 'should not send announcemnet mail without attachments to suspended users' do
    MandrillMailer.any_instance.expects(:send_api_email).never
    AnnouncementMailer.announcement_email(post: @post, users: [@user3]).deliver_now
  end
end

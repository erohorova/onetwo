require 'test_helper'

class ExtendDateTest < ActiveSupport::TestCase
  test 'should convert correct string to date(mm/dd/yyyy)' do
    str_date  = '02/28/2017'
    str_date1 = '03/32/2017'
    str_date2 = '13/01/2017'
    assert_equal Date.new(2017,02,28), Date.strptime_with_format(str_date)
    assert_nil Date.strptime_with_format(str_date1)
    assert_nil Date.strptime_with_format(str_date2)
  end

  test 'should convert correct string to date(dd/mm/yyyy)' do
    str_date  = '28/2/2017'
    str_date1 = '32/3/2017'
    str_date2 = '31/22/2017'
    assert_equal Date.new(2017,02,28), Date.strptime_with_format(str_date,'%d/%m/%Y')
    assert_nil Date.strptime_with_format(str_date1,'%d/%m/%Y')
    assert_nil Date.strptime_with_format(str_date2,'%d/%m/%Y')
  end
end

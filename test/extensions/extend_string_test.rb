require 'test_helper'

class ExtendStringTest < ActiveSupport::TestCase
  test 'to_bool' do
    assert 'true'.to_bool
    assert 't'.to_bool
    assert 'yes'.to_bool
    assert 'y'.to_bool
    assert '1'.to_bool
    assert 'on'.to_bool
    refute 'false'.to_bool
    refute 'f'.to_bool
    refute 'no'.to_bool
    refute 'n'.to_bool
    refute 'nil'.to_bool
    refute '0'.to_bool
    refute 'off'.to_bool
    refute ''.to_bool
    assert_raises(ArgumentError) { 'wolverine'.to_bool }
  end

  test 'to_camelcase_sym' do
    assert 'card_type'.to_camelcase_sym
    assert 'title'.to_camelcase_sym
  end
end
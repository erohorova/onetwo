require 'test_helper'

class ExtendFalseClassTest < ActiveSupport::TestCase
  test 'to_bool' do
    refute false.to_bool
  end
end
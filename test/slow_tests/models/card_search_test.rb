require 'test_helper'
class CardSearchTest < ActiveSupport::TestCase

  self.use_transactional_fixtures = false

  setup do
    ChannelsCard.any_instance.stubs :create_users_management_card

    WebMock.disable!
    if Card.searchkick_index.exists?
      Card.searchkick_index.delete
    end

    @org = create(:organization)
  end

  teardown do
    WebMock.enable!
  end

  test 'deleting a card should remove it from index' do
    new_card = create(:card, card_type: 'media', card_subtype: 'text', organization: @org, author_id: nil)

    Card.reindex
    search_result = Search::CardSearch.new.search_by_id(new_card.id)
    assert_not_nil search_result

    new_card.destroy

    Card.reindex
    Card.searchkick_index.refresh
    search_result = Search::CardSearch.new.search_by_id(new_card.id)
    assert_nil search_result
  end

  test 'full test for poll cards' do
    skip("INTERMITTENT FAILURE ON TRAVIS")
    EdcastActiveJob.unstub :perform_later
    QuizQuestionStatsRecorderJob.unstub :perform_later

    poll_card = create(:card, card_type: 'poll', card_subtype: 'text', organization: @org, author_id: nil)

    Card.reindex
    Card.searchkick_index.refresh

    pcard_from_indx = Search::CardSearch.new.search_by_id(poll_card.id)
    assert_equal [], pcard_from_indx['quiz_question_options']
    assert_nil pcard_from_indx['quiz_question_stats']

    # create some options
    opt1 = create(:quiz_question_option, quiz_question: poll_card, label: 'opt 1', is_correct: false)
    opt2 = create(:quiz_question_option, quiz_question: poll_card, label: 'opt 2', is_correct: true)

    Card.reindex
    Card.searchkick_index.refresh

    pcard_from_indx = Search::CardSearch.new.search_by_id(poll_card.id)
    assert_equal 2, pcard_from_indx['quiz_question_options'].count
    assert_nil pcard_from_indx['quiz_question_stats']

    # first attempt, someone checks opt 1 only
    create(:quiz_question_attempt, user: create(:user, organization: @org), quiz_question: poll_card, selections: [opt1.id])

    Card.searchkick_index.refresh
    pcard_from_indx = Search::CardSearch.new.search_by_id(poll_card.id)
    assert_equal 2, pcard_from_indx['quiz_question_options'].count
    assert_equal({'attempt_count' => 1, 'options_stats' => {opt1.id.to_s => 1}}, pcard_from_indx['quiz_question_stats'])

    # some one else attempts and checks both
    assert_no_difference ->{QuizQuestionStats.count} do
      create(:quiz_question_attempt, user: create(:user), quiz_question: poll_card, selections: [opt1.id, opt2.id])
    end
    Card.searchkick_index.refresh
    pcard_from_indx = Search::CardSearch.new.search_by_id(poll_card.id)
    assert_equal 2, pcard_from_indx['quiz_question_options'].count
    assert_equal({'attempt_count' => 2, 'options_stats' => {opt1.id.to_s => 2, opt2.id.to_s => 1}}, pcard_from_indx['quiz_question_stats'])
  end

  test 'search card by taxonomy topic' do
    @card = create(:card, card_type: 'media', card_subtype: 'text', organization: @org, author_id: nil)
    @card_1 = create(:card, card_type: 'media', card_subtype: 'text', organization: @org, author_id: nil)

    user = create(:user, organization: @org)
    create(:user_profile, user: user, learning_topics: get_sample_topics)

    @card.update(taxonomy_topics: ['fake topic name 1'])
    @card_1.update(user_taxonomy_topics: [{'label' => 'fake topic name 1', 'path' => 'fake topic name 1'}])

    Card.reindex
    Card.searchkick_index.refresh

    data = Search::CardSearch.new.search(
       nil,
       @card.organization,
       filter_params: {taxonomy_topics: user.learning_topics_name},
       viewer: user,
       offset: 0,
       limit: 10,
       sort: :created,
       load: true
    )

    assert_equal 2, data.total
    assert_same_elements [@card.id, @card_1.id], data.results.map(&:id)
  end
end
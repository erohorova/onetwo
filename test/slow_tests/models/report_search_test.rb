require 'test_helper'

class ReportSearchTest < ActiveSupport::TestCase
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
  end

  test "should mark item as hidden if above threshold" do
    org = create(:organization)
    client = create(:client, organization: org)
    g = create(:resource_group, client: client, organization: org)
    u1 = create(:user, organization: org)
    u2 = create(:user, organization: org)
    g.add_member u1
    g.add_member u2
    q = create(:question, user: u2, group: g)
    assert_not q.hidden
    create(:report, reporter: u1, reportable: q)
    assert_not q.hidden

    # above threshold
    create(:report, reporter: u2, reportable: q)
    assert q.hidden
  end

  test "should mark item as hidden if reported by faculty" do
    org = create(:organization)
    client = create(:client, organization: org)
    faculty = create(:user, organization: org)
    user = create(:user, organization: org)
    group = create(:resource_group, client: client, organization: org)
    group.add_admin faculty
    group.add_member user
    q = create(:question, user: user, group: group)
    assert_not q.hidden

    create(:report, reportable: q, reporter: faculty)
    assert q.hidden
  end
end

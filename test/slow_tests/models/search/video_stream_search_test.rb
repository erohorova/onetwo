require 'test_helper'
# DEPRECATED
class Search::VideoStreamSearchTest < ActiveSupport::TestCase
  self.use_transactional_fixtures = false
  setup do
    skip if Settings.skip_tests.models.search.video_stream

    WebMock.disable!
    VideoStream.searchkick_index.delete if VideoStream.searchkick_index.exists?

    @org = create(:organization)
    @user = create(:user, organization: @org, organization_role: 'member')
    @name1, @name2, @name3 = 'tech', 'food', 'travel'

    NotificationGeneratorJob.stubs(:set).returns(mock({perform_later:nil})) #cant use queue_as for inline test adapter
    Recording.any_instance.stubs(:submit_aws_transcoder_job).returns(nil)

    @v1 = create(:wowza_video_stream, :past, name: @name1, creator: @user)
    @v2 = create(:wowza_video_stream, :upcoming, name: @name2, creator: @user)
    @v3 = create(:wowza_video_stream, :live, name: @name3, creator: @user)

    past_streams = [@v1]
    past_streams += create_list(:wowza_video_stream, 12, :past, creator: @user, start_time: Time.now.utc - 1.day)
    past_streams.each {|ps| create(:recording, video_stream: ps)}

    VideoStream.reindex
    VideoStream.searchkick_index.refresh
  end

  teardown do
    WebMock.enable!
  end


  test "should support sort param" do
    @promoted_stream = create(:wowza_video_stream, :past, name: @name1, creator: @user, is_official: true)
    create(:recording, video_stream: @promoted_stream)
    VideoStream.reindex
    VideoStream.searchkick_index.refresh

    stream_pars = {viewer: @user}
    response = Search::VideoStreamSearch.new.search(viewer: @user, sort_params: [{is_official: {order: "desc"}}, {_score: "desc"}])
    assert_equal @promoted_stream.id, response.results.first.id
  end

  test "should return video streams for a viewer" do
    stream_pars = {viewer: @user}
    response = Search::VideoStreamSearch.new.search(stream_pars)

    assert_not_empty response.results
    assert_equal 15, response.total
    assert_equal 10, response.results.count
  end

  test "should return video streams matching query" do
    stream_pars = {q: 'food', viewer: @user}
    response = Search::VideoStreamSearch.new.search(stream_pars)

    assert_not_empty response.results
    assert_equal 1, response.total
    assert_equal [@v2.id], response.results.map(&:id)
  end

  test "should support pagination" do
    stream_pars = {viewer: @user, limit: 10, offset: 7}
    response = Search::VideoStreamSearch.new.search(stream_pars)

    assert_equal "success", response.status
    assert_not_empty response.results
    assert_equal 15, response.total
    assert_equal 8, response.results.count
  end

  test 'should not return video that doesnt have recording' do
    user = create(:user, organization_role: 'member')
    stream_pars = {viewer: user}

    past_streams = create_list(:wowza_video_stream, 2, :past, creator: user, start_time: Time.now.utc - 1.day)
    response = Search::VideoStreamSearch.new.search(stream_pars)

    assert_empty response.results
  end

  test 'should exclude deleted streams from search' do
    @v3.delete_from_cms!

    VideoStream.searchkick_index.refresh
    response = Search::VideoStreamSearch.new.search(viewer: @user, limit: 20, offset: 0)
    assert_equal 14, response.total
    refute response.results.map{|r| r['id']}.include?(@v3.id)
  end

  test 'should support creator must filter param' do
    @v3.update_attributes(creator: nil)
    VideoStream.searchkick_index.refresh

    response = Search::VideoStreamSearch.new.search(viewer: @user, limit: 20, offset: 0, filter_params: {creator_must: true})
    assert_equal 14, response.total
    refute response.results.map{|r| r['id']}.include?(@v3.id)

    response = Search::VideoStreamSearch.new.search(viewer: @user, limit: 20, offset: 0, filter_params: {})
    assert_equal 15, response.total
    assert response.results.map{|r| r['id']}.include?(@v3.id)
  end

  test 'should not return video streams from private channels' do
    channel = create(:channel, is_private: false, only_authors_can_post: true, organization: @org)
    channel.add_authors([@user])
    video_stream = create(:wowza_video_stream, :live, name: "travel food", creator: @user)
    video_stream.channels = [channel]
    viewer = create(:user, organization: @org )

    VideoStream.reindex
    VideoStream.searchkick_index.refresh

    response = Search::VideoStreamSearch.new.search(q: "travel", viewer: viewer, limit: 20, offset: 0, filter_params: {creator_must: true})
    assert_equal false, response.results.map(&:id).index(video_stream.id).nil?
    # channel is now private
    channel.update_columns(is_private: true)
    response = Search::VideoStreamSearch.new.search(q: "travel", viewer: viewer, limit: 20, offset: 0, filter_params: {creator_must: true})
    assert_equal true, response.results.map(&:id).index(video_stream.id).nil?
  end

  test 'should support range filter in video stream search' do
    org = create(:organization)
    user = create(:user, organization: org)
    v1 = create(:wowza_video_stream, :live, state: 'published', name: "test video1", creator: user, created_at: Time.current - 2.hours)
    v2 = create(:wowza_video_stream, :live, state: 'published', name: "test video2", creator: user, created_at: Time.current)

    VideoStream.reindex

    results = Search::VideoStreamSearch.new.search(viewer: user, limit: 20, offset: 0, filter_params: {range: {created_at: {gt: Time.current - 1.hour}}}).results

    assert_equal 1, results.count
    assert_equal [v2.id], results.map{|r| r['id']}
  end

  class Search::CmsVideoStreamSearchTest < ActiveSupport::TestCase

    self.use_transactional_fixtures = false
    setup do
      skip if Settings.skip_tests.models.search.video_stream

      WebMock.disable!
      VideoStream.any_instance.stubs(:schedule_reminder_push_notification).returns(nil)
      Recording.any_instance.stubs(:submit_aws_transcoder_job).returns(nil)
    end

    teardown do
      WebMock.enable!
    end

    def streams_setup
      EclCardPropagationJob.stubs(:perform_later).returns(nil)

      @org1, @org2 = create_list(:organization, 2)
      @all_streams = {@org1.id => [], @org2.id => []}
      @org1_user = create(:user, organization: @org1)
      @org2_user = create(:user, organization: @org2)
      @all_streams[@org1.id] << @live_stream = create(:wowza_video_stream, status: 'live', creator: @org1_user, name: 'nikolai')
      @all_streams[@org1.id] << @upcoming_stream = create(:wowza_video_stream, status: 'upcoming', start_time: Time.now.utc + 1.day, creator: @org1_user)
      @all_streams[@org1.id] << @past_stream = create(:wowza_video_stream, status: 'past', creator: @org1_user)
      @recording = create(:recording, mp4_location: 'something', hls_location: 'something', transcoding_status: 'available', video_stream: @past_stream)

      @some_channels = create_list(:channel, 2, organization: @org1)
      @some_tags = create_list(:tag, 2)

      @live_stream.channels << @some_channels
      @live_stream.tags << @some_tags

      @past_stream.channels << @some_channels.first
      @past_stream.tags << @some_tags.first

      # stream in org2
      @all_streams[@org2.id] << create(:wowza_video_stream, status: 'live', creator: @org2_user)

      # draft stream, should be excluded
      @draft_stream = create(:wowza_video_stream, state: "draft", status: 'live', creator: @org1_user, name: 'nikolai')
      VideoStream.reindex
      VideoStream.searchkick_index.refresh
    end

    test 'cms search for video streams should work with filters' do
      streams_setup

      # status filters
      resp = Search::VideoStreamSearch.new.search_for_cms('', @org1.id, filter_params: {status: 'live'}, limit: 10, offset: 0)
      assert_same_elements [@live_stream].map(&:id), resp.results.map(&:id)

      # sort on status
      resp = Search::VideoStreamSearch.new.search_for_cms('', @org1.id, filter_params: {}, sort: { key: 'status', order: 'asc' })
      assert_equal [@live_stream, @past_stream, @upcoming_stream].map(&:status), resp.results.map(&:status)

      # topics filters
      resp = Search::VideoStreamSearch.new.search_for_cms('', @org1.id, filter_params: {topics: @some_tags.map(&:name)}, limit: 10, offset: 0)
      assert_same_elements [@past_stream, @live_stream].map(&:id), resp.results.map(&:id)

      # channel filters
      resp = Search::VideoStreamSearch.new.search_for_cms('', @org1.id, filter_params: {channel_ids: @some_channels.map(&:id)}, limit: 10, offset: 0)
      assert_same_elements [@past_stream, @live_stream].map(&:id), resp.results.map(&:id)

      # allow empty channels
      resp = Search::VideoStreamSearch.new.search_for_cms('', @org1.id, filter_params: {allow_empty_channel_ids: true, channel_ids: @some_channels.map(&:id)}, limit: 10, offset: 0)
      assert_same_elements [@upcoming_stream, @past_stream, @live_stream].map(&:id), resp.results.map(&:id)

      # combination filters
      resp = Search::VideoStreamSearch.new.search_for_cms('', @org1.id, filter_params: {status: 'past', channel_ids: @some_channels.map(&:id)}, limit: 10, offset: 0)
      assert_same_elements [@past_stream].map(&:id), resp.results.map(&:id)

      # State filter
      resp = Search::VideoStreamSearch.new.search_for_cms('', @org1.id, filter_params: {state: 'published'}, limit: 10, offset: 0)
      assert_same_elements [@past_stream, @live_stream, @upcoming_stream].map(&:id), resp.results.map(&:id)

      @past_stream.update_columns(state: 'deleted')
      @past_stream.reindex
      VideoStream.searchkick_index.refresh
      resp = Search::VideoStreamSearch.new.search_for_cms('', @org1.id, filter_params: {state: 'deleted'}, limit: 10, offset: 0)
      assert_same_elements [@past_stream].map(&:id), resp.results.map(&:id)

      # Official filter
      resp = Search::VideoStreamSearch.new.search_for_cms('', @org1.id, filter_params: {is_official: true}, limit: 10, offset: 0)
      assert_same_elements [], resp.results.map(&:id)

      @past_stream.update_columns(is_official: true)
      @past_stream.reindex
      VideoStream.searchkick_index.refresh
      resp = Search::VideoStreamSearch.new.search_for_cms('', @org1.id, filter_params: {is_official: true}, limit: 10, offset: 0)
      assert_same_elements [@past_stream].map(&:id), resp.results.map(&:id)
    end

    test 'cms should support range filter' do
      streams_setup

      resp = Search::VideoStreamSearch.new.search_for_cms('', @org1.id, filter_params: {range: {updated_at: {gt: 2.days.ago}}}, limit: 10, offset: 0)
      assert_same_elements [@past_stream, @live_stream, @upcoming_stream].map(&:id), resp.results.map(&:id)
    end

    test 'cms search for video streams should work with free text' do
      streams_setup

      resp = Search::VideoStreamSearch.new.search_for_cms('nikolai', @org1.id, filter_params: {}, limit: 10, offset: 0)
      assert_same_elements [@live_stream].map(&:id), resp.results.map(&:id)
    end
  end
end

require 'test_helper'
class Search::ChannelSearchTest < ActiveSupport::TestCase
  # self.use_transactional_fixtures = false
  setup do
    WebMock.disable!
    Organization.any_instance.unstub(:create_general_channel)
    Channel.searchkick_index.delete if Channel.searchkick_index.exists?

    @org1, @org2 = create_list(:organization, 2)
    @general_channel = Channel.first
    @user = create(:user, organization: @org1, organization_role: 'member')
    @user_without_email = create(:user, organization: @org1, email: nil)

    @ch1 = create(:channel, :robotics, organization: @org1, is_private: false)
    @ch2 = create(:channel, :architecture, organization: @org1, is_private: false)
    @ch3 = create(:channel, :health, organization: @org1, is_private: false)
    @ch4 = create(:channel, :good_music, organization: @org1, is_private: false)
    @ch5 = create(:channel, :private, organization: @org1, is_private: true)
    @ch6 = create(:channel, label: 'travel', organization: @org1, is_private: false, visible_during_onboarding: true)
    @ch7 = create(:channel, label: 'private travel', organization: @org1, is_private: true)
    @ch8 = create(:channel, label: 'Followed via team', organization: @org1, is_private: true)
    @ch7.add_followers [@user]
    @ch3.add_followers [@user_without_email]
    @ch4.add_followers [@user_without_email]
    @channel_in_another_org = create(:channel, :good_music, organization: @org2, is_private: false)

    # Follow channel via team
    team = create(:team, organization: @org1)
    team.add_member(@user)
    team.teams_channels_follows.create(channel: @ch8)

    @all_channels = [@general_channel, @ch1, @ch2, @ch3, @ch4, @ch5, @ch6, @ch7, @ch8]
    Channel.reindex
  end

  teardown do
    WebMock.enable!
  end

  test 'autosuggest for cms' do
    # all public channels
    response = Search::ChannelSearch.new.autosuggest_for_cms('',  @org1.id, filter: {})
    assert_equal 6, response.total
    assert_same_elements [@ch1, @ch2, @ch3, @ch4, @ch6, @general_channel].map(&:label), response.results.map(&:label)
    assert_same_elements [@ch1, @ch2, @ch3, @ch4, @ch6, @general_channel].map(&:id), response.results.map(&:id)

    # private + public channels
    response = Search::ChannelSearch.new.autosuggest_for_cms('',  @org1.id, filter: {include_private: true})
    assert_equal 9, response.total
    assert_same_elements @all_channels.map(&:label), response.results.map(&:label)
    assert_same_elements @all_channels.map(&:id), response.results.map(&:id)

    distraction = create(:channel, label: 'music good', organization: @org1, is_private: false)
    Channel.reindex
    # Show show "general" and "good music" channels
    response = Search::ChannelSearch.new.autosuggest_for_cms('g', @org1.id, limit: 10, offset: 0)
    assert_same_elements [@ch4, @general_channel].map(&:id), response.results.map{|r| r['id']}

    another_architecture = create(:channel, label: "Architecture 2", organization: @org1, is_private: false)
    private_architecture = create(:channel, label: "Architecure Private", organization: @org1, is_private: true)
    Channel.reindex
    response = Search::ChannelSearch.new.autosuggest_for_cms('A', @org1.id, limit: 10, offset: 0)
    assert_same_elements [@ch2, another_architecture].map(&:id), response.results.map{|r| r['id']}

  end

  # TEST GROUPING REMOVED TO AVOID REDUNDANT DATA CREATION
  test 'should return all channels in an org' do
    response = Search::ChannelSearch.new.search(viewer: @user)
    assert_equal 6, response.total
    assert_not_empty response.results
    assert_same_elements (@all_channels - [@ch5, @ch7, @ch8]).map(&:id), response.results.map(&:id)

    # return channels matching query
    response = Search::ChannelSearch.new.search(q: 'good music', viewer: @user)
    assert_equal 1, response.total
    assert_not_empty response.results
    assert_equal [@ch4.id], response.results.map(&:id)

    # return channels matching query with filter options
    response = Search::ChannelSearch.new.search(viewer: @user, filter: {include_private: true})
    assert_equal 8, response.total
    assert_not_empty response.results
    assert_not_empty response.aggs['followers']
    assert_not_empty response.aggs['collaborators']
    assert_not_empty response.aggs['curators']
    assert_same_elements @all_channels.map(&:id) - [@ch5.id], response.results.map(&:id)

    # return only channels a user is following
    response = Search::ChannelSearch.new.search(viewer: @user_without_email, filter: {only_my_channels: 'true'})
    assert_equal 2, response.total
    assert_not_empty response.results
    assert_same_elements [@ch3.id, @ch4.id], response.results.map(&:id)

    # return only private channels matching query
    response = Search::ChannelSearch.new.search(viewer: @user, filter: {include_private: true, is_private: 'true'})
    assert_equal 2, response.total
    assert_not_empty response.results
    assert_same_elements [@ch7.id, @ch8.id], response.results.map(&:id)

    # return only public channels matching query
    response = Search::ChannelSearch.new.search(viewer: @user, filter: {include_private: true, is_private: 'false'})
    assert_equal 6, response.total
    assert_not_empty response.results
    assert_same_elements (@all_channels - [@ch5, @ch7, @ch8]).map(&:id), response.results.map(&:id)

    response = Search::ChannelSearch.new.search(viewer: @user, filter: {onboarding: true})
    assert_equal 1, response.total
    assert_not_empty response.results
    assert_equal [@ch6.id], response.results.map(&:id)

    response = Search::ChannelSearch.new.search(viewer: @user, filter: {include_private: false})
    assert_equal 6, response.total
    assert_not_empty response.results
    assert_same_elements (@all_channels - [@ch5, @ch7, @ch8]).map(&:id), response.results.map(&:id)

    # channel label #desc
    response = Search::ChannelSearch.new.search(viewer: @user, sort: {key: 'label', order: 'desc'})
    assert_equal 6, response.total
    assert_not_empty response.results
    # travel, robotics, health, good music, general, architecture
    assert_equal [@ch6, @ch1, @ch3, @ch4, @general_channel, @ch2].map(&:id), response.results.map(&:id)

    # sort by channel label #asc
    response = Search::ChannelSearch.new.search(viewer: @user, sort: {key: 'label', order: 'asc'}, filter: {include_private: true})
    assert_equal 8, response.total
    assert_not_empty response.results
    # architecture, Followed via team, general, good music, health, private channel, robotics, travel, Followed via team
    assert_equal [@ch2, @ch8, @general_channel, @ch4, @ch3, @ch7, @ch1, @ch6].map(&:label), response.results.map(&:label)
    assert_equal [@ch2, @ch8, @general_channel, @ch4, @ch3, @ch7, @ch1, @ch6].map(&:id), response.results.map(&:id)

    # return aggregations for channels matching query if skip_aggs is false
    response = Search::ChannelSearch.new.search(q: 'good music', viewer: @user, filter: {skip_aggs: false})
    assert_equal 1, response.total
    assert_not_empty response.results
    assert_equal [@ch4.id], response.results.map(&:id)
    assert_not_empty response.aggs['followers']
    assert_not_empty response.aggs['collaborators']
    assert_not_empty response.aggs['curators']

    # do not return aggregations for channels matching query if skip_aggs is true
    response = Search::ChannelSearch.new.search(q: 'good music', viewer: @user, filter: {skip_aggs: true})
    assert_equal 1, response.total
    assert_not_empty response.results
    assert_equal [@ch4.id], response.results.map(&:id)
    refute response.aggs
  end
end

require 'test_helper'
class MultiSearchTest < ActiveSupport::TestCase
  self.use_transactional_fixtures = false

  setup do
    skip #deprecated
    create_default_org
    WebMock.disable!
    Card.searchkick_index.delete if Card.searchkick_index.exists?

    @link_author, @video_author = create_list(:user, 2)

    # create 12 cards
    link_cards = create_list(:card, 6, card_type: 'media', card_subtype: 'link', is_public: true, author_id: @link_author.id)
    @video_cards = create_list(:card, 6, card_type: 'media', card_subtype: 'video', is_public: true, author_id: @video_author.id)
    @cards = link_cards + @video_cards

    # different creation times
    @cards.reverse.each_with_index {|card,i| card.update(created_at: i.hours.ago) }

    @lastcard = @cards.last
    @lastcard.update_attributes(message: 'matchy me')

    # first 7 and 9th have 'thingy' in content
    @thingy_cards = @cards[0..6] + [@cards[8]]
    @cards[0..6].each {|card| card.update_attributes(message: 'this is the thingy')}
    @cards[8].update_attributes(title: 'thingy title')

    @middle4_tag = create(:tag, name: 'middle4')
    @first8_tag = create(:tag, name: 'first8')

    # 1st through 8th have #first8 tag
    @cards[0..7].each{|card| card.tags << @first8_tag}

    # 5th through 8th have #middle4 tag
    @cards[4..7].each{|card| card.tags << @middle4_tag}

    # 3rd card has handle @handle
    @cards[2].update(author_id: create(:user, handle: '@handle').id)

    Card.reindex
    Card.searchkick_index.refresh


    User.searchkick_index.delete if User.searchkick_index.exists?
    @users = create_list(:user, 20)
    @users[0].update(handle: "@afshin", first_name: 'afshin', last_name: 'shafie')
    @users[1].update(handle: "@robertsmith", first_name: 'robert', last_name: 'smith')
    @users[2].update(handle: "@johnoates", first_name: 'john', last_name: 'oates')
    @users[3].update(handle: "@drdre", first_name: 'dr', last_name: 'dre', is_complete: false)
    @users[2].update(handle: "@trungpham", first_name: 'trung', last_name: 'pham')
    User.reindex
    User.searchkick_index.refresh


    VideoStream.searchkick_index.delete if VideoStream.searchkick_index.exists?
    NotificationGeneratorJob.stubs(:set).returns(mock({perform_later:nil})) #cant use queue_as for inline test adapter
    Recording.any_instance.stubs(:submit_aws_transcoder_job).returns(nil)

    v1 = create(:wowza_video_stream, :past)
    create(:recording, video_stream: v1)
    create(:wowza_video_stream, :upcoming)
    create(:wowza_video_stream, :live)

    VideoStream.reindex
    VideoStream.searchkick_index.refresh
  end

  teardown do
    WebMock.enable!
  end

  test 'should fire multi search queries' do
    ms = Search::MultiSearch.new
    card_pars = {viewer: nil, offset: 2, limit: 3, sort: :created}
    default_org = Organization.default_org
    ms.add('cards', Search::CardSearch.new.search_body('thingy', default_org, card_pars))
    user_pars = {q: 'trung', viewer: nil}
    ms.add('users', Search::UserSearch.new.search_body(user_pars))
    stream_pars = {q: 'awesome', offset: 1, viewer: nil}
    ms.add('video_streams', Search::VideoStreamSearch.new.search_body(stream_pars))
    results = ms.execute

    assert_equal Search::CardSearch.new.search('thingy', default_org, card_pars).results.map(&:id), results['cards'].results.map(&:id)
    assert_equal 3, results['cards'].results.size
    assert_equal 8, results['cards'].total
    assert_equal Search::UserSearch.new.search(user_pars).results.map(&:id), results['users'].results.map(&:id)
    assert_equal 1, results['users'].results.size
    assert_equal 1, results['users'].total
    assert_equal Search::VideoStreamSearch.new.search(stream_pars).results.map(&:id), results['video_streams'].results.map(&:id)
    assert_equal 2, results['video_streams'].results.size
    assert_equal 3, results['video_streams'].total

    # create a new user in a different org. Should all be empty
    org = create(:organization)
    org_user = create(:user, organization: org)
    User.all.map(&:reindex)
    User.searchkick_index.refresh

    ms = Search::MultiSearch.new
    ms.add('cards', Search::CardSearch.new.search_body('matchy', org, viewer: org_user))
    ms.add('users', Search::UserSearch.new.search_body(q: 'trung', viewer: org_user))
    ms.add('video_streams', Search::VideoStreamSearch.new.search_body(q: 'awesome', viewer: org_user))
    results = ms.execute
    assert_equal [], results['cards'].results
    assert_equal [], results['video_streams'].results
    assert_equal [], results['users'].results
  end

end

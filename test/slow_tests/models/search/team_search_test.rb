require 'test_helper'
class Search::TeamSearchTest < ActiveSupport::TestCase
  # self.use_transactional_fixtures = false
  setup do
    WebMock.disable!
    Team.searchkick_index.delete if Team.searchkick_index.exists?

    @org1, @org2 = create_list(:organization, 2)
    @users = create_list(:user, 2, organization: @org1, organization_role: 'member')
    @channel_list = create_list(:channel, 3, organization_id: @org1.id)

    @t1 = create(:team, name: 'sales', description: 'All things sales', organization: @org1)
    create(:teams_channels_follow, team_id: @t1.id, channel_id: @channel_list[0].id)
    create(:teams_channels_follow, team_id: @t1.id, channel_id: @channel_list[1].id)

    @t2 = create(:team, name: 'architecture', description: 'Taught by howard Rourk', organization: @org1)
    create(:teams_channels_follow, team_id: @t2.id, channel_id: @channel_list[2].id)

    @t3 = create(:team, name: 'health', description: 'BKS Iyengar', organization: @org1)
    create(:teams_channels_follow, team_id: @t3.id, channel_id: @channel_list[0].id)

    @t4 = create(:team, name: 'women health', is_private: true, organization: @org1)
    @t5 = create(:team, name: 'men health', is_private: false, organization: @org1)

    @inv1 = create(:invitation, invitable: @t2, recipient_email: @users[0].email, recipient_id: @users[0].id, roles: 'member')
    @inv2 = create(:invitation, invitable: @t5, recipient_email: @users[0].email, recipient_id: @users[0].id, roles: 'member')

    @team3_user = @t3.teams_users.create(user: @users[0], as_type: 'member')
    @team4_user = @t4.teams_users.create(user: @users[0], as_type: 'member')

    @team_in_another_org = create(:team, name: 'Astrophysics', description: 'ISRO', organization: @org2)

    Team.reindex
  end

  teardown do
    WebMock.enable!
  end

  test 'should return all teams in an org when no filters are passed' do
    response = Search::TeamSearch.new.search(viewer: @users[0], filter: {skip_aggs: false})
    assert_same_elements @org1.teams.pluck(:id), response.results.map{|d| d['id'].to_i}
    assert_not_empty response.aggs
  end
  
  test 'should return all teams in an org  and no aggs when skip_aggs with value true filters are passed' do
    response = Search::TeamSearch.new.search(q: 'architecture', viewer: @users[0], filter: {skip_aggs: true})
    assert_same_elements [@t2.id], response.results.map{|d| d['id'].to_i}
    refute response.aggs
  end

  test 'should return teams that follow specific channel only' do
    response = Search::TeamSearch.new.search(viewer: @users[0], filter: {channel_ids: [@channel_list[0].id]})
    assert_same_elements @channel_list[0].followed_teams.pluck(:id), response.results.map{|d| d['id'].to_i}
    assert_not_includes response.results.map{|d| d['id'].to_i}, @t2.id
  end

  test 'should remove channels from teams when team unfollow channel' do
    teams_channels_follow = TeamsChannelsFollow.find_by(team_id: @t2.id, channel_id: @channel_list[2].id)
    teams_channels_follow.destroy
    Team.reindex
    response = Search::TeamSearch.new.search(viewer: @users[0], filter: {channel_ids: [@channel_list[1].id, @channel_list[2].id]})
    assert_same_elements [@t1.id], response.results.map{|d| d['id'].to_i}
    assert_not_includes response.results.map{|d| d['id'].to_i}, @t2.id
  end

  test 'should return teams that match all filters' do
    response = Search::TeamSearch.new.search(viewer: @users[0], q: "architecture", filter: {channel_ids: [@channel_list[0].id]})    
    assert_empty response.results

    response1 = Search::TeamSearch.new.search(viewer: @users[0], q: "architecture", filter: {channel_ids: [@channel_list[2].id, @channel_list[0].id]})    

    assert_equal [@t2.id], response1.results.map{|d| d['id'].to_i}
    assert_not_includes response1.results.map{|d| d['id'].to_i}, @t1.id
    assert_not_includes response1.results.map{|d| d['id'].to_i}, @t3.id
  end

  test 'should return only private teams' do
    response = Search::TeamSearch.new.search(viewer: @users[0], q: "health", filter: {is_private: 'true'})
    assert_equal [@t4.id], response.results.map{|d| d['id'].to_i}
  end
  
  test 'should return only private teams with aggs' do
    response = Search::TeamSearch.new.search(viewer: @users[0], q: "health", filter: {is_private: 'true', skip_aggs: false})
    assert_equal [@t4.id], response.results.map{|d| d['id'].to_i}
    assert_not_empty response.aggs['leaders']
    assert_not_empty response.aggs['admins']
  end

  test 'should return only open teams' do
    response = Search::TeamSearch.new.search(viewer: @users[0], q: "health", filter: {is_private: 'false'})
    assert_equal [@t3.id, @t5.id], response.results.map{|d| d['id'].to_i}
  end

  test 'should return only current user teams' do
    response = Search::TeamSearch.new.search(viewer: @users[0], q: "health", filter: {current_user_teams: true})
    assert_equal [@t3.id, @t4.id], response.results.map{|d| d['id'].to_i}
  end

  test 'should return only current user pending teams' do
    response = Search::TeamSearch.new.search(viewer: @users[0], q: "health", filter: {current_user_pending_teams: true})
    assert_equal [@t5.id], response.results.map{|d| d['id'].to_i}
  end

  test 'should sort team by name' do
    response = Search::TeamSearch.new.search(viewer: @users[0], sort: {key: 'name', order: 'asc'})
    assert_same_elements @org1.teams.order(name: 'asc').map(&:name), response.results.map{ |d| d['name'] }
  end

  test 'should not return team if invitation was declined' do
    @inv2.destroy

    response = Search::TeamSearch.new.search(
      viewer: @users[0], q: 'health',
      filter: {current_user_pending_teams: true}
    )
    assert_equal [@t5.id], response.results.map { |d| d['id'].to_i }
    assert_equal @t5.id, @inv2.invitable.id

    Team.reindex
    reindexed_res = Search::TeamSearch.new.search(
      viewer: @users[0], q: 'health',
      filter: {current_user_pending_teams: true}
    )
    assert_empty reindexed_res.results.map { |d| d['id'].to_i }
  end

end

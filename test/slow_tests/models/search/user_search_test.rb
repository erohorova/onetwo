require 'test_helper'
class Search::UserSearchTest < ActiveSupport::TestCase

  # self.use_transactional_fixtures = false

  setup do
    WebMock.disable!
    if User.searchkick_index.exists?
      User.searchkick_index.delete
      User.reindex
    end
  end

  teardown do
    WebMock.enable!
  end

  test 'partial prefix search' do
    create_default_org
    default_org = Organization.default_org
    users = create_list(:user, 20, organization: default_org)
    users.each_with_index do |u,i|
      i < 17 ? u.first_name = "abc#{i}" : u.first_name = "xyz#{i}"
      u.save!
    end

    #incomplete user
    user = create(:user, is_complete: false, organization: default_org)

    User.reindex
    User.searchkick_index.refresh

    resp = Search::UserSearch.new.suggest(q: 'abc', limit: 20)
    assert_equal(Search::UserSearchResponse, resp.class)
    assert_equal('success', resp.status)
    assert_equal(17, resp.total)
    assert_equal('abc', resp.query)

    resp = Search::UserSearch.new.suggest(q: 'abc1')
    assert_equal(Search::UserSearchResponse, resp.class)
    assert_equal('success', resp.status)
    assert_equal(8, resp.total)

    resp = Search::UserSearch.new.suggest(q: 'abc11')
    assert_equal(Search::UserSearchResponse, resp.class)
    assert_equal('success', resp.status)
    assert_equal(1, resp.total)

    resp = Search::UserSearch.new.suggest(q: 'xyz')
    assert_equal(Search::UserSearchResponse, resp.class)
    assert_equal('success', resp.status)
    assert_equal(3, resp.total)

    # search inside an organization
    org = create(:organization)
    org_abcs = create_list(:user, 2, organization: org)

    User.reindex
    User.searchkick_index.refresh

    resp = Search::UserSearch.new.suggest(q: 'fname', organization_id: org.id)
    assert_equal 2, resp.total
    assert_same_elements org_abcs.map(&:id), resp.results.map{|r| r['id']}
  end

  test 'suggest should exclude suspended users' do
    org = create(:organization)
    user1, user2, user3 = create_list(:user, 3, organization: org)

    User.reindex
    User.searchkick_index.refresh

    resp = Search::UserSearch.new.suggest(q: "fname")
    assert_same_elements [user1, user2, user3].map(&:id), resp.results.map{|r| r['id']}

    user3.suspend!
    user3.reindex

    User.searchkick_index.refresh
    resp = Search::UserSearch.new.suggest(q: "fname")
    assert_same_elements [user1, user2].map(&:id), resp.results.map{|r| r['id']}
  end

  test 'search should include suspended users at the end if requested' do
    org = create(:organization)
    users = create_list(:user, 3, first_name: "uniqterm", organization: org)

    User.reindex
    User.searchkick_index.refresh

    resp = Search::UserSearch.new.search(q: "uniqterm", viewer: users.first)
    assert_same_elements users.map(&:id), resp.results.map{|r| r['id']}

    users.first.suspend!
    User.reindex
    User.searchkick_index.refresh
    resp = Search::UserSearch.new.search(q: "uniqterm", viewer: users.first, filter: {include_suspended: true})
    assert_same_elements users.map(&:id), resp.results.map{|r| r['id']}
    # Last element should be the suspended user
    assert_equal users.first.id, resp.results.last['id']
  end

  test 'search should return only suspended users' do
    org = create(:organization)
    users = create_list(:user, 3, organization: org)

    users.first.suspend!
    users.second.suspend!
    User.reindex
    User.searchkick_index.refresh
    resp = Search::UserSearch.new.search(q: '', viewer: users.last, filter: {include_suspended: true, only_suspended: true}, allow_blank_q: true)
    assert_same_elements users.first(2).map(&:id), resp.results.map{|r| r['id']}
  end

  test 'search should exclude suspended users' do
    org = create(:organization)
    users = create_list(:user, 3, first_name: "uniqterm", organization: org)

    User.reindex
    User.searchkick_index.refresh

    resp = Search::UserSearch.new.search(q: "uniqterm", viewer: users.first)
    assert_same_elements users.map(&:id), resp.results.map{|r| r['id']}

    users.last.suspend!
    User.reindex
    User.searchkick_index.refresh
    resp = Search::UserSearch.new.search(q: "uniqterm", viewer: users.first)
    assert_same_elements users.first(2).map(&:id), resp.results.map{|r| r['id']}

    resp = Search::UserSearch.new.search(q: "uniqterm", viewer: users.first, filter: {include_suspended: false})
    assert_same_elements users.first(2).map(&:id), resp.results.map{|r| r['id']}
  end

  test 'search should exclude users mention in exclude_ids' do
    org = create(:organization)
    users = create_list(:user, 5, first_name: "uniqterm", organization: org)

    User.reindex
    User.searchkick_index.refresh

    resp = Search::UserSearch.new.search(q: "uniqterm", viewer: users.first)
    assert_same_elements users.map(&:id), resp.results.map{|r| r['id']}

    User.reindex
    User.searchkick_index.refresh
    resp = Search::UserSearch.new.search(q: "uniqterm", viewer: users.first, filter: {exclude_ids: [users.last.id]})
    assert_same_elements users.first(4).map(&:id), resp.results.map{|r| r['id']}
  end

  test 'search should exclude suspended users for team scope unless requested' do
    org = create(:organization)
    users = create_list(:user, 3, first_name: 'uniqterm', organization: org)
    team = create(:team, organization: org)
    users.each { |user| team.add_member(user) }

    User.reindex
    User.searchkick_index.refresh

    resp = Search::UserSearch.new.search(q: "uniqterm", viewer: users.first)
    assert_same_elements users.map(&:id), resp.results.map{|r| r['id']}

    users.last.suspend!
    User.reindex
    User.searchkick_index.refresh
    resp = Search::UserSearch.new.search(q: "uniqterm", viewer: users.first, filter: {scope: team})
    assert_same_elements users.first(2).map(&:id), resp.results.map{|r| r['id']}

    resp = Search::UserSearch.new.search(q: "uniqterm", viewer: users.first, filter: {scope: team, include_suspended: false})
    assert_same_elements users.first(2).map(&:id), resp.results.map{|r| r['id']}

    resp = Search::UserSearch.new.search(q: "uniqterm", viewer: users.first, filter: {scope: team, include_suspended: true})
    assert_same_elements users.first(3).map(&:id), resp.results.map{|r| r['id']}
  end

  test 'pagination' do
    org = create(:organization)
    users = create_list(:user, 22, organization: org)
    #users.each{|u| u.update(handle: u.handle.sub('@','@x22'))}
    User.reindex
    User.searchkick_index.refresh

    resp = Search::UserSearch.new.suggest(q: "fname", limit: 5)
    assert_equal('success', resp.status)
    assert_equal(22, resp.total)
    assert_equal(5, resp.results.count)

    resp = Search::UserSearch.new.suggest(q: "fname", limit: 20, offset: 5)
    assert_equal('success', resp.status)
    assert_equal(22, resp.total)
    assert_equal(17, resp.results.count)
  end

  test 'org scoped search' do
    org = create(:organization)
    org_users = create_list(:user, 5, organization: org, first_name: 'uniqterm')
    org_viewer = create(:user, organization: org)

    User.reindex
    User.searchkick_index.refresh

    resp = Search::UserSearch.new.search(q: 'uniqterm', viewer: org_viewer)
    assert_same_elements org_users.map(&:id), resp.results.map{|r| r['id']}

    # don't allow blank q
    resp = Search::UserSearch.new.search(viewer: org_viewer)
    assert_empty resp.results

    # allow blank q
    resp = Search::UserSearch.new.search(viewer: org_viewer, allow_blank_q: true)
    assert_same_ids org_users + [org_viewer], resp.results, ordered: false
  end

  test 'support team ids filter' do
    org = create(:organization)
    org_viewer = create(:user, organization: org)

    t1, t2, distractor_team = create_list(:team, 3, organization: org)

    users = create_list(:user, 4, organization: org)

    t1.add_member(users[0])
    t2.add_member(users[0])

    t1.add_member(users[1])

    t2.add_member(users[2])

    distractor_team.add_member(users[3])

    User.reindex
    User.searchkick_index.refresh

    resp = Search::UserSearch.new.search(viewer: org_viewer, allow_blank_q: true, filter: {team_ids: [t1, t2].map(&:id)})
    assert_same_elements users[0..2].map(&:id), resp.results.map{|r| r['id']}

    resp = Search::UserSearch.new.search(viewer: org_viewer, allow_blank_q: true, filter: {team_ids: [t1].map(&:id)})
    assert_same_elements [users[0], users[1]].map(&:id), resp.results.map{|r| r['id']}

    resp = Search::UserSearch.new.search(viewer: org_viewer, allow_blank_q: true, filter: {team_ids: [distractor_team].map(&:id)})
    assert_same_elements [users[3]].map(&:id), resp.results.map{|r| r['id']}
  end

  test 'support team_ids or channel_ids filter' do
    org = create(:organization)
    org_viewer = create(:user, organization: org)

    team_list = create_list(:team, 2, organization: org)
    channel = create(:channel, organization: org)
    users = create_list(:user, 3, organization: org)

    team_list[0].add_member(users[0])
    team_list[1].add_member(users[2])

    Follow.create(followable: channel, user_id: users[1].id)

    User.reindex
    User.searchkick_index.refresh

    resp = Search::UserSearch.new.search(viewer: org_viewer, allow_blank_q: true, filter: {team_ids: [team_list[0]].map(&:id),
     channel_ids: [channel].map(&:id)})

    assert_same_elements users[0..1].map(&:id), resp.results.map{|r| r['id']}

    assert_not_includes resp.results.map{|r| r['id']}, users[2].id
  end

  test 'team membership search and count' do
    org = create(:organization)
    org_viewer = create(:user, organization: org)

    t1, t2, distractor_team = create_list(:team, 3, organization: org)

    amember = create(:user, organization: org, first_name: 'citizen', last_name: 'alpha')
    zmember = create(:user, organization: org, first_name: 'citizen', last_name: 'zeta')
    zadmin = create(:user, organization: org, first_name: 'leader', last_name: 'zeta')
    aadmin = create(:user, organization: org, first_name: 'leader', last_name: 'alpha')
    t1.add_member amember
    t1.add_member zmember
    t1.add_admin zadmin
    t1.add_admin aadmin

    # distractors
    t2.add_admin create(:user, organization: org, first_name: 'admin', last_name: 'zeta')
    distractor_team.add_admin zadmin

    User.reindex
    User.searchkick_index.refresh

    # sorted by last name, first name
    resp = Search::UserSearch.new.search(q: 'alpha', viewer: org_viewer, allow_blank_q: true, filter: {scope: t1})
    assert_same_ids [amember, aadmin], resp.results
    assert_equal 2, Search::UserSearch.new.count_team(q: 'alpha', team: t1)

    resp = Search::UserSearch.new.search(q: 'zeta', viewer: org_viewer, allow_blank_q: true, filter: {scope: t1}, sort: :role)
    assert_same_ids [zadmin, zmember], resp.results

    resp = Search::UserSearch.new.search(q: 'leader', viewer: org_viewer, allow_blank_q: true, filter: {scope: t1}, sort: :name)
    assert_same_ids [aadmin, zadmin], resp.results

    resp = Search::UserSearch.new.search(viewer: org_viewer, allow_blank_q: true, filter: {scope: t1}, sort: :name)
    assert_same_ids [amember, aadmin, zmember, zadmin], resp.results
    assert_equal 4, Search::UserSearch.new.count_team(team: t1)

    resp = Search::UserSearch.new.search(viewer: org_viewer, allow_blank_q: true, filter: {scope: t1}, limit: 1)
    assert_same_ids [amember], resp.results

    # sorted by role
    resp = Search::UserSearch.new.search(q: 'alpha', viewer: org_viewer, allow_blank_q: true, filter: {scope: t1}, sort: :role)
    assert_same_ids [aadmin, amember], resp.results

    resp = Search::UserSearch.new.search(viewer: org_viewer, allow_blank_q: true, filter: {scope: t1}, sort: :role)
    assert_same_ids [aadmin, zadmin, amember, zmember], resp.results

    resp = Search::UserSearch.new.search(viewer: org_viewer, filter: {scope: t1}, limit: 2, offset: 1, sort: :role, allow_blank_q: true)
    assert_same_ids [zadmin, amember], resp.results
  end

  test 'user sort by first_name' do
    org = create(:organization)
    org_viewer = create(:user, organization: org, first_name: 'fmember', last_name: 'zmember')

    amember = create(:user, organization: org, first_name: 'citizen', last_name: 'alpha')
    zmember = create(:user, organization: org, first_name: 'zadmin', last_name: 'zeta')
    bmember = create(:user, organization: org, first_name: 'bmember', last_name: 'zeta')

    User.reindex
    User.searchkick_index.refresh

    # sorted by first_name
    resp = Search::UserSearch.new.search(
      q: nil,
      viewer: org_viewer,
      allow_blank_q: true,
      load: true,
      sort: :first_name,
      order: 'asc'
    )
    assert_same_ids [bmember, amember, org_viewer, zmember], resp.results
  end

  test 'team members search and filter by role' do
    org = create(:organization)
    org_viewer = create(:user, organization: org)

    t1 = create(:team, organization: org)

    aamember = create(:user, organization: org, first_name: 'acitizen', last_name: 'alpha')
    zzmember = create(:user, organization: org, first_name: 'zcitizen', last_name: 'zeta')
    zladmin = create(:user, organization: org, first_name: 'leader', last_name: 'zeta')
    azadmin = create(:user, organization: org, first_name: 'zleader', last_name: 'alpha')
    t1.add_member aamember
    t1.add_member zzmember
    t1.add_admin zladmin
    t1.add_admin azadmin

    User.reindex
    User.searchkick_index.refresh

    # sorted by last name, first name
    resp = Search::UserSearch.new.search(q: nil, viewer: org_viewer, allow_blank_q: true, filter: {scope: t1, role: 'member'})
    assert_same_ids [aamember, zzmember], resp.results
    assert_equal 2, resp.total

    resp = Search::UserSearch.new.search(q: 'alpha', viewer: org_viewer, allow_blank_q: true, filter: {scope: t1, role: 'member'})
    assert_same_ids [aamember], resp.results
    assert_equal 1, resp.total

    resp = Search::UserSearch.new.search(q: nil, viewer: org_viewer, allow_blank_q: true, filter: {scope: t1, role: 'admin'})
    assert_same_ids [zladmin, azadmin], resp.results
    assert_equal 2, resp.total

    resp = Search::UserSearch.new.search(q: 'zeta', viewer: org_viewer, allow_blank_q: true, filter: {scope: t1, role: 'admin'})
    assert_same_ids [zladmin], resp.results
    assert_equal 1, resp.total
  end

  test 'keyword search' do
    # create_default_org
    org = create(:organization)
    users = create_list(:user, 6, last_name: 'mrBrown', first_name: 'afshin', organization: org)
    users += create_list(:user, 8, last_name: 'Blueman', first_name: 'afshin', organization: org)
    users += create_list(:user, 20, first_name: 'afshin', organization: org)

    incomplete1 = create(:user, first_name: 'afshin', is_complete: false, organization: org)
    incomplete2 = create(:user, first_name: 'mrbrown', is_complete: false, organization: org)
    incomplete3 = create(:user, first_name: 'bluesman', is_complete: false, organization: org)

    User.reindex
    User.searchkick_index.refresh

    resp = Search::UserSearch.new.search(q: 'someone', viewer: nil)
    assert_equal('success', resp.status)
    assert_equal(0, resp.total)
    assert_equal([], resp.results)

    resp = Search::UserSearch.new.search(q: 'mrbrown', viewer: nil)
    assert_equal('success', resp.status)
    assert_equal(7, resp.total)
    assert_equal(7, resp.results.count)

    resp = Search::UserSearch.new.search(q: 'bluesman', viewer: nil)
    assert_equal('success', resp.status)
    assert_equal(9, resp.total)
    assert_equal(9, resp.results.count)

    resp = Search::UserSearch.new.search(q: 'afshin', viewer: nil)
    assert_equal('success', resp.status)
    assert_equal(35, resp.total)
    assert_equal(10, resp.results.count)

    resp = Search::UserSearch.new.search(q: 'af', viewer: nil)
    assert_equal('success', resp.status)
    assert_equal(35, resp.total)
    assert_equal(10, resp.results.count)

    # Testing fuzziness, ashi insted of afshi(n)
    resp = Search::UserSearch.new.search(q: 'ashi', viewer: nil)
    assert_equal('success', resp.status)
    assert_equal(35, resp.total)
    assert_equal(10, resp.results.count)
  end

  test 'should get list of users that user is following' do
    org = create(:organization)
    viewer = create(:user, organization: org)
    followings = create_list(:user, 5, organization: org)
    not_following = create_list(:user, 2, organization: org)

    followings.each do |following|
      create(:follow, followable: following, user: viewer)
    end

    User.reindex
    User.searchkick_index.refresh

    resp = Search::UserSearch.new.search(viewer: viewer, allow_blank_q: true, filter: {following: true})

    assert_equal 5, resp.results.count
    assert_same_ids followings.map(&:id), resp.results.map {|user| user['id']}
    assert_empty not_following.map(&:id) & resp.results.map {|user| user['id']}
  end

  test 'should filter by sme role' do
    org = create(:organization)
    Role.create_master_roles(org)
    viewer = create(:user, organization: org)

    smes = create_list(:user, 5, organization: org)
    users = create_list(:user, 3, organization: org)
    smes.each {|user| user.make_sme}

    User.reindex
    User.searchkick_index.refresh

    resp = Search::UserSearch.new.search(viewer: viewer, allow_blank_q: true, filter: {role: 'sme'})
    assert_equal 5, resp.results.count
    assert_same_ids smes.map(&:id), resp.results.map {|user| user['id']}
  end

  test 'should filter by sme rolename even if sme role name has changed' do
    org = create(:organization)
    Role.create_master_roles(org)
    viewer = create(:user, organization: org)
    smes = create_list(:user, 5, organization: org)
    users = create_list(:user, 3, organization: org)
    smes.each {|user| user.make_sme}

    sme_role_to_be_changed = org.roles.where(name: 'sme').first
    sme_role_to_be_changed.name = 'new_name'
    sme_role_to_be_changed.save
    sme_role_to_be_changed.reload

    User.reindex
    User.searchkick_index.refresh

    resp = Search::UserSearch.new.search(viewer: viewer, allow_blank_q: true, filter: {role: sme_role_to_be_changed.name})

    assert_equal 5, resp.results.count
    assert_same_ids smes.map(&:id), resp.results.map {|user| user['id']}
  end

  test 'should return experties matching with viewer learning topics' do
    UserProfile.any_instance.stubs(:record_user_profile_update_event)
    org = create(:organization)
    viewer = create(:user, organization: org)
    create(:user_profile, user: viewer, learning_topics: Taxonomies.domain_topics, expert_topics: nil)

    smes = create_list(:user, 5, organization: org)
    users = create_list(:user, 3, organization: org)
    smes.each {|user| user.make_sme}
    smes.each {|sme| create(:user_profile, user: sme, expert_topics: Taxonomies.domain_topics)}
    users.each {|user| create(:user_profile, user: user, expert_topics: nil, learning_topics: nil)}

    other_user = create(:user, organization: org)
    create(:user_profile, user: other_user, expert_topics: [Taxonomies.domain_topics.last(1)], learning_topics: nil)

    User.reindex
    User.searchkick_index.refresh

    resp = Search::UserSearch.new.search(viewer: viewer, allow_blank_q: true, filter: {search_expertise: true})

    assert_equal 6, resp.results.count
    assert_same_ids (smes+[other_user]).map(&:id), resp.results.map {|user| user['id']}

    resp1 = Search::UserSearch.new.search(viewer: users[0], allow_blank_q: true, filter: {search_expertise: true})
    assert_equal 0, resp1.results.count
    assert_equal [], resp1.results.map {|user| user['id']}
  end

  test 'should find users by expert topics label' do
    org = create(:organization)
    user, member = create_list(:user, 2, organization: org)
    create(:user_profile, user: user, expert_topics: [{'topic_label' => 'Java'}])
    create(:user_profile, user: member, expert_topics: [{'topic_label' => 'Ruby'}])

    User.reindex
    User.searchkick_index.refresh

    resp = Search::UserSearch.new.search(
      q: 'ruby', viewer: user,
      allow_blank_q: true,
      filter: {},
      load: true,
      use_custom_fields: true
    )
    assert_equal 1, resp.results.count
    assert_equal [member.id], resp.results.map {|user| user['id']}
  end

  test 'should find users only by expert topics label or other information case insensitive and not exact match' do
    org = create(:organization)
    user, member = create_list(:user, 2, organization: org)
    user2 = create(:user, organization: org, first_name: 'Ruby')
    create(:user_profile, user: member, expert_topics: [{'topic_label' => 'Ruby'}, {'topic_label' => 'Some super skill'}])

    User.reindex
    User.searchkick_index.refresh

    resp = Search::UserSearch.new.search(
      q: 'rUbY', viewer: user,
      allow_blank_q: true,
      filter: { search_by: ['expert_topics.searchable_topic_label'] },
      load: true,
      use_custom_fields: true
    )
    assert_equal 1, resp.results.count
    assert_equal [member.id], resp.results.map {|user| user['id']}

    resp2 = Search::UserSearch.new.search(
      q: 'RuBY', viewer: user,
      allow_blank_q: true,
      filter: { search_by: ['name.searchable', 'first_name.searchable', 'last_name.searchable'] },
      load: true,
      use_custom_fields: true
    )
    assert_equal 1, resp2.results.count
    assert_equal [user2.id], resp2.results.map {|user| user['id']}

    # search should not be exact match
    resp3 = Search::UserSearch.new.search(
      q: 'sUpER', viewer: user,
      allow_blank_q: true,
      filter: { search_by: ['expert_topics.searchable_topic_label'] },
      load: true,
      use_custom_fields: true
    )
    assert_equal 1, resp3.results.count
    assert_equal [member.id], resp3.results.map {|user| user['id']}
  end

  test 'should filter users by emails and names' do
    org = create(:organization)
    user = create(:user, organization: org)
    user2 = create(:user, organization: org, email: 'user2@email.com', first_name: 'Test', last_name: 'User')
    user3 = create(:user, organization: org, email: 'user3@email.com', first_name: 'Test', last_name: 'User')
    User.reindex
    User.searchkick_index.refresh
    resp = Search::UserSearch.new.search(
      q: '', viewer: user,
      allow_blank_q: true,
      filter: { emails_for_filtering: ['user3@email.com']},
      load: true,
      use_custom_fields: true
    )
    assert_equal 1, resp.results.count
    assert_equal [user3.id], resp.results.map {|user| user['id']}
    resp2 = Search::UserSearch.new.search(
      q: '', viewer: user,
      allow_blank_q: true,
      filter: { names_for_filtering: ['test user']},
      load: true,
      use_custom_fields: true
    )
    assert_equal 2, resp2.results.count
    assert_equal [user2.id, user3.id], resp2.results.map {|user| user['id']}
    resp3 = Search::UserSearch.new.search(
      q: '', viewer: user,
      allow_blank_q: true,
      filter: { names_for_filtering: ['test user'],
                emails_for_filtering: ['user2@email.com']},
      load: true,
      use_custom_fields: true
    )
    assert_equal 1, resp3.results.count
    assert_equal [user2.id], resp3.results.map {|user| user['id']}
  end

  test 'should filter users by custom fields' do
    org = create(:organization)
    user, user2, user3 = create_list(:user, 3, organization: org)
    cf = create(:custom_field, organization: org, display_name: 'country')
    cf2 = create(:custom_field, organization: org, display_name: 'city')
    user_field = create(:user_custom_field, custom_field: cf, user: user2, value: 'russia')
    user_field2 = create(:user_custom_field, custom_field: cf, user: user3, value: 'england')
    user_field3 = create(:user_custom_field, custom_field: cf2, user: user3, value: 'london')
    user_field4 = create(:user_custom_field, custom_field: cf2, user: user2, value: 'moscow')
    User.reindex
    User.searchkick_index.refresh
    resp = Search::UserSearch.new.search(
      q: '', viewer: user,
      allow_blank_q: true,
      filter: { custom_fields: [{'name' => 'country', 'values' => ['russia']},
                                {'name' => 'city', 'values' => ['london']}]},
      load: true,
      use_custom_fields: true
    )
    assert_equal 0, resp.results.count
    resp2 = Search::UserSearch.new.search(
      q: '', viewer: user,
      allow_blank_q: true,
      filter: { custom_fields: [{'name' => 'country', 'values' => ['russia']},
                                {'name' => 'city', 'values' => ['moscow']}]},
      load: true,
      use_custom_fields: true
    )
    assert_equal 1, resp2.results.count
    assert_equal [user2.id], resp2.results.map {|user| user['id']}
  end

  test 'should return user with exact name match' do
    org = create(:organization)
    user_1 = create(:user, organization: org, first_name: 'firstname_1', last_name: 'testlastname')
    user_2 = create(:user, organization: org, first_name: 'firstname_2', last_name: 'testlastname')
    user_3 = create(:user, organization: org, first_name: 'testlast', last_name: 'test')
    user_4 = create(:user, organization: org, first_name: 'test', last_name: 'test')

    User.reindex
    User.searchkick_index.refresh

    search_result = Search::UserSearch.new.suggest(q: "testlast test")
    assert_equal [user_3.id], search_result.results.map { |res| res['id'] }
  end

  class LeaderboardUserSearchTest < ActiveSupport::TestCase
    # ------------ Following search is used in Leaderboard Service ---------------
    setup do
      WebMock.disable!
      if User.searchkick_index.exists?
        User.searchkick_index.delete
        User.reindex
      end
    end

    teardown do
      WebMock.enable!
    end

    test 'should return users matching the search term with first_name' do
      org = create(:organization)
      user_1 = create(:user, organization: org, first_name: 'user_1', last_name: 'last_name')
      user_2 = create(:user, organization: org, first_name: 'number_2', last_name: 'last_name')

      User.reindex
      User.searchkick_index.refresh

      search_result = User.search(body: User.string_search_query(q: 'user_1', organization_id: org.id))
      assert_equal [user_1.id], search_result.results.map(&:id)
    end

    test 'should return users matching the search term with last_name' do
      org = create(:organization)
      user_1 = create(:user, organization: org, first_name: 'user_1', last_name: 'uniq_last_name')
      user_2 = create(:user, organization: org, first_name: 'number_2', last_name: 'last_name')

      User.reindex
      User.searchkick_index.refresh

      search_result = User.search(body: User.string_search_query(q: 'uniq', organization_id: org.id))
      assert_equal [user_1.id], search_result.results.map(&:id)
    end

    test 'should return users matching the search term with email' do
      org = create(:organization)
      user_1 = create(:user, organization: org, first_name: 'user_1', last_name: 'last_name', email: "dummyemail@abc.com")
      user_2 = create(:user, organization: org, first_name: 'user_2', last_name: 'last_name', email: "user2email@abc.com")

      User.reindex
      User.searchkick_index.refresh

      search_result = User.search(body: User.string_search_query(q: 'dummyemail', organization_id: org.id))
      assert_empty search_result.results.map(&:id)
    end

    test 'should return users matching exactly the search term with email' do
      org = create(:organization)
      user_1 = create(:user, organization: org, first_name: 'user_1', last_name: 'last_name', email: "dummyemail@abc.com")
      user_2 = create(:user, organization: org, first_name: 'user_2', last_name: 'last_name', email: "user2email@abc.com")

      User.reindex
      User.searchkick_index.refresh

      search_result = User.search(body: User.string_search_query(q: 'dummyemail@abc.com', organization_id: org.id))
      assert_equal [user_1.id], search_result.results.map(&:id)
    end

    test 'should return users matching expertise' do
      org = create(:organization)
      user_1, user_2 = create_list(:user, 2, organization: org)
      create(:user_profile, user: user_1, expert_topics: [Taxonomies.domain_topics[0]])
      create(:user_profile, user: user_2, expert_topics: [Taxonomies.domain_topics[1]])

      User.reindex
      User.searchkick_index.refresh

      search_result = User.search(body: User.expertise_search_query(expertise_names: [Taxonomies.domain_topics[0]['topic_name']], organization_id: org.id))
      assert_equal [user_1.id], search_result.results.map(&:id)
    end

    test 'should return users matching search term & expertise' do
      org = create(:organization)
      user_1 = create(:user, organization: org, first_name: 'user_1', last_name: 'last_name')
      create(:user_profile, user: user_1, expert_topics: [Taxonomies.domain_topics[0]])
      user_2 = create(:user, organization: org, first_name: 'user_2', last_name: 'last_name')
      create(:user_profile, user: user_2, expert_topics: [Taxonomies.domain_topics[1]])

      User.reindex
      User.searchkick_index.refresh

      search_result = User.search(body: User.string_search_query(q: 'user', organization_id: org.id).merge!(
        User.expertise_search_query(expertise_names: [Taxonomies.domain_topics[0]['topic_name']], organization_id: org.id)))
      assert_equal [user_1.id], search_result.results.map(&:id)
    end
  end
end

require 'test_helper'
class Search::CardSearchTest < ActiveSupport::TestCase

  self.use_transactional_fixtures = false

  setup do
    ChannelsCard.any_instance.stubs :create_users_management_card
    WebMock.disable!
    Card.searchkick_index.delete if Card.searchkick_index.exists?
    @default_org = create(:organization)

    @link_author, @video_author = create_list(:user, 2, organization: @default_org)

    # create 12 cards
    link_resource = create(:resource, type: 'Article')
    video_resource = create(:resource, type: 'Video')
    @link_cards = create_list(:card, 6, card_type: 'media', card_subtype: 'link', is_public: true, author_id: @link_author.id, resource: link_resource)
    @video_cards = create_list(:card, 6, card_type: 'media', card_subtype: 'video', is_public: true, author_id: @video_author.id, resource: video_resource)

    @cards = @link_cards + @video_cards

    # different creation times
    @cards.reverse.each_with_index {|card,i| card.update(created_at: i.hours.ago) }

    @lastcard = @cards.last
    @lastcard.update_attributes(message: 'matchy me')

    # first 7 and 9th have 'thingy' in content
    @thingy_cards = @cards[0..6] + [@cards[8]]
    @cards[0..6].each {|card| card.update_attributes(message: 'this is the thingy')}
    @cards[8].update_attributes(title: 'thingy title')

    @middle4_tag = create(:tag, name: 'middle4')
    @first8_tag = create(:tag, name: 'first8')

    # 1st through 8th have #first8 tag
    @cards[0..7].each{|card| card.tags << @first8_tag}

    # 5th through 8th have #middle4 tag
    @cards[4..7].each{|card| card.tags << @middle4_tag}

    # 3rd card has handle @handle
    @cards[2].update(author_id: create(:user, handle: '@handle').id)

    Card.reindex
    Card.searchkick_index.refresh
  end

  teardown do
    WebMock.enable!
  end

  test 'gets a search result' do
    resp = Search::CardSearch.new.search('matchy', @default_org, viewer: nil)
    assert_equal 1, resp.total
    assert_equal 0, resp.offset
    assert_equal 10, resp.limit
    assert_equal 1, resp.results.count
    assert_equal @lastcard.id, resp.results.first['id']
    assert_equal 'success', resp.status
    assert_equal 'matchy', resp.query
  end

  test 'offsets and limits search results' do
    resp = Search::CardSearch.new.search('thingy', @default_org, viewer: nil, limit: 5)
    assert_equal 8, resp.total
    assert_equal 5, resp.results.count
    first_ids = resp.results.map{|r| r['id']}

    resp = Search::CardSearch.new.search('thingy', @default_org, viewer: nil, offset: 5, limit: 5)
    assert_equal 8, resp.total
    assert_equal 5, resp.offset
    assert_equal 5, resp.limit
    assert_equal 3, resp.results.count
    second_ids = resp.results.map{|r| r['id']}

    # all results covered by two pages
    assert_same_elements @thingy_cards.map(&:id), first_ids | second_ids
    # no intersection between two pages
    assert_empty first_ids & second_ids
  end

  test 'skips hidden cards, paginated' do
    [0,8].each {|i| @cards[i].update hidden: true}
    Card.reindex
    Card.searchkick_index.refresh

    resp = Search::CardSearch.new.search('thingy', @default_org, viewer: nil, limit: 5)
    assert_equal 6, resp.total
    assert_equal 5, resp.results.count
    first_ids = resp.results.map{|r| r['id']}

    resp = Search::CardSearch.new.search('thingy', @default_org, viewer: nil, offset: 5, limit: 5)
    assert_equal 6, resp.total
    assert_equal 1, resp.results.count
    second_ids = resp.results.map{|r| r['id']}

    # all results covered by two pages
    assert_same_elements @cards[1..6].map(&:id), first_ids | second_ids
    # no intersection between two pages
    assert_empty first_ids & second_ids
  end

  def all_have_all_tags?(cards, tag_array)
    cards.all?{|c| (c.tags.map{|t| t['name']} & tag_array).sort == tag_array.sort}
  end

  test 'matches tags' do
    resp = Search::CardSearch.new.search('middle4', @default_org, viewer: nil)
    assert_equal 4, resp.total
    assert all_have_all_tags?(resp.results, ['middle4'])
  end

  test 'boosts tags' do
    resp = Search::CardSearch.new.search('middle4 thingy', @default_org, viewer: nil)
    assert_equal 9, resp.total
    assert all_have_all_tags?(resp.results.first(4), ['middle4'])
    assert resp.results.last(5).none?{|c| c.tags.map{|t| t['name']}.include? 'middle4'}
  end

  test 'prefers more tags' do
    resp = Search::CardSearch.new.search('middle4 first8 thingy', @default_org, viewer: nil)
    assert_equal 9, resp.total
    #assert_same_elements ['#first8', '#middle4'], resp.tags
    assert all_have_all_tags?(resp.results[0..3], ['middle4', 'first8'])
    assert all_have_all_tags?(resp.results[0..7], ['first8'])
    assert_empty resp.results.last.tags
  end

  test 'sorts by created_at' do
    card1 = create(:card, title: "uniq", card_type: 'media', card_subtype: 'link', created_at: 1.day.ago, author: @link_author)
    card2 = create(:card, title: "uniq", card_type: 'media', card_subtype: 'link', author: @link_author)

    Card.reindex
    Card.searchkick_index.refresh
    resp = Search::CardSearch.new.search("uniq",
                                         @default_org,
                                         viewer: nil,
                                         sort: :created)
    assert_equal [card2, card1].map(&:id), resp.results.map{|c| c['id']}
  end

  test 'nil target' do
    pcard = create(:card, card_type: 'poll', card_subtype: 'text', author_id: @link_author.id)
    Card.reindex
    Card.searchkick_index.refresh
    resp = Search::CardSearch.new.search(nil, @default_org, viewer: nil, filter_params: {card_type: ['poll']}, limit: 3, sort: :created)
    assert_equal 'success', resp.status
    assert_equal [pcard.id], resp.results.map{|r| r['id']}
  end

  test 'yield channel results in chrono order if autopull enabled' do
    org =  create(:organization)
    channel = create(:channel, label: 'first8', autopull_content: true, organization: org)

    channel_cards = create_list(:card, 4, card_type: 'media', card_subtype: 'video', organization: org, author_id: nil)
    channel_cards.each_with_index do |card, i|
      card.channels << channel
      card.update_attributes(created_at: i.hours.ago, published_at: i.hours.ago)
    end

    Card.reindex
    Card.searchkick_index.refresh

    # influencer thing does not matter anymore
    resp = Search::CardSearch.new.search(channel, org, viewer: nil)
    assert_equal channel_cards.map(&:id), resp.results.map{|r| r.id}
  end

  test 'should include explicitly associated cards for channels' do
    channel = create(:channel, label: 'first8', autopull_content: true, organization: @default_org)

    associated_card = create(:card, card_type: 'media', card_subtype: 'link', is_public: true, author: @link_author)
    associated_card.channels << channel

    Card.reindex
    Card.searchkick_index.refresh

    resp = Search::CardSearch.new.search(channel, @default_org, viewer: nil)
    assert_same_ids [associated_card], resp.results

    # should only get explicitly associated content if autopull not enabled
    # Update: autopull_content field is defunct.
    channel.update_columns(autopull_content: false)
    resp = Search::CardSearch.new.search(channel, @default_org, viewer: nil)
    assert_same_ids [associated_card], resp.results
  end

  test 'finds author name and handle' do
    # All cards have pretty much the same author names, eithe person1 or person2. Elasticsearch returns all
    [
        @video_author.handle,
        @video_author.name,
        @video_author.first_name,
        @video_author.last_name,
    ].each do |q|
      resp = Search::CardSearch.new.search(q, @default_org, viewer: nil, limit: 12)
      assert_same_elements (@video_cards + @link_cards).map(&:id), resp.results.map{|c| c['id']}
    end

    unique_name_user = create(:user, handle: "@uniq",
                              first_name: "uniq", last_name: "uniq", organization: @default_org)
    unique_card = create(:card, card_type: 'media', card_subtype: 'link', author: unique_name_user)
    Card.reindex
    Card.searchkick_index.refresh

    resp = Search::CardSearch.new.search('uniq', @default_org, viewer: nil)
    assert_equal [unique_card.id], resp.results.map{|c| c['id']}

    # No matches
    resp = Search::CardSearch.new.search('nobodysname', @default_org, viewer: nil)
    assert_empty resp.results
  end

  test 'finds mentioned user name and handle' do
    skip 'not yet implemented'
    mention_cards = @cards[5..7]
    mentioned_user = create(:user)
    mention_cards.each {|c| c.mentioned_users << mentioned_user}
    Card.reindex
    Card.searchkick_index.refresh

    [
        mentioned_user.handle,
        mentioned_user.name,
        mentioned_user.first_name,
        mentioned_user.last_name,
    ].each do |q|
      resp = Search::CardSearch.new.search(q, @default_org, viewer: nil)
      assert_same_elements mention_cards.map(&:id),
                           resp.results.map{|c| c['id']}
    end

    test 'should sort by promoted cards' do
      card1 = create(:card, title: "uniq", card_type: 'media', card_subtype: 'link', promoted_at: 1.day.ago, is_official: true)
      card2 = create(:card, title: "uniq", card_type: 'media', card_subtype: 'link', created_at: Time.current)

      Card.searchkick_index.refresh
      resp = Search::CardSearch.new.search("uniq",
                                           @default_org,
                                           viewer: nil,
                                           sort: :promoted_first)
      assert_equal [card1, card2].map(&:id), resp.results.map{|c| c['id']}
    end
  end

  class Search::FilterParamsSearchTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false
    setup do
      # Analytics::CardChannelRecorder.stubs(:record)
      WebMock.disable!
      @card_types = [['media', 'video'], ['media', 'link'], ['media', 'text'], ['media', 'image'], ['pack'], ['poll', 'image'], ['poll', 'link'], ['poll', 'video']]
    end

    teardown do
      WebMock.enable!
    end

    test 'should respect card type and template type filters' do
      default_org = create(:organization)

      u, f = create_list(:user, 2, organization: default_org)
      create(:follow, followable: u, user: f)
      @cards = []

      link_resource = create(:resource, type: 'Article')
      video_resource = create(:resource, type: 'Video')
      file_resource = create(:file_resource)

      @card_types.each do |card_type|
        if card_type[0] == 'pack'
          cover = create(:card, card_type: 'pack', state: 'draft', author_id: u.id)
          CardPackRelation.add(cover_id: cover.id, add_id: @cards.first.id)
          @cards << cover
          next
        end

        case card_type[1]
        when 'video'
          @cards << create(:card, card_type: card_type[0], card_subtype: card_type[1], author_id: u.id, resource: video_resource)
        when 'link'
          @cards << create(:card, card_type: card_type[0], card_subtype: card_type[1], author_id: u.id, resource: link_resource)
        when 'image'
          @cards << create(:card, card_type: card_type[0], card_subtype: card_type[1], author_id: u.id, file_resources: [file_resource])
        else
          @cards << create(:card, card_type: card_type[0], card_subtype: card_type[1], author_id: u.id)
        end
      end

      Card.reindex
      Card.searchkick_index.refresh

      res = Search::CardSearch.new.search(u, default_org, viewer: f, filter_params: {card_type: ['media', 'poll', 'pack'], state: ['published']}, limit: @cards.count)

      # everything except draft_pack
      assert_equal @cards.count - 1, res.results.count

      # packs only
      res = Search::CardSearch.new.search(u, default_org, viewer: f, filter_params: {card_type: ['pack'], state: 'draft'}, limit: @cards.count)
      assert_equal [@cards[4].id], res.results.map{|r| r['id']}

      # polls only
      res = Search::CardSearch.new.search(u, default_org, viewer: f, filter_params: {card_type: ['poll']}, limit: @cards.count)
      assert_equal @cards[5..-1].map(&:id), res.results.map{|r| r['id']}

      # videos only
      res = Search::CardSearch.new.search(u, default_org, viewer: f, filter_params: {card_template: 'video', card_type: ['poll', 'media', 'pack']}, limit: @cards.count)
      assert_same_elements [@cards.first.id, @cards.last.id], res.results.map{|r| r['id']}
    end


    test 'should return cards matching topic filter' do
      default_org = create(:organization)

      u, f = create_list(:user, 2, organization: default_org)
      create(:follow, followable: u, user: f)
      card1, card2, card3, card4 = create_list(:card, 4, card_type: 'media', card_subtype: 'link', author_id: u.id)

      science = create(:tag, name: 'science')
      fiction = create(:tag, name: 'fiction')
      galaxy  = create(:tag, name: 'galaxy')
      start_up  = create(:tag, name: 'start up')

      card1.tags << science
      card2.tags << science
      card3.tags << science

      card1.tags << galaxy
      card2.tags << fiction
      card3.tags << fiction
      card4.tags << start_up

      Card.reindex
      Card.searchkick_index.refresh

      res = Search::CardSearch.new.search(u, default_org, viewer: f, filter_params: {card_type: ['media'], topics: ['fiction']}, limit: 10)

      assert_equal 2, res.results.count
      assert_equal [card2.id, card3.id], res.results.map{|r| r['id']}

      res = Search::CardSearch.new.search(u, default_org, viewer: f, filter_params: {card_type: ['media'], topics: ['science']}, limit: 10)
      assert_equal 3, res.results.count
      assert_equal [card1.id, card2.id, card3.id], res.results.map{|r| r['id']}

      res = Search::CardSearch.new.search(u, default_org, viewer: f, filter_params: {card_type: ['media'], topics: ['start up', 'galaxy']}, limit: 10)
      assert_equal 2, res.results.count
      assert_equal [card1.id, card4.id], res.results.map{|r| r['id']}
    end

    test 'should support range filter' do
      org = create(:organization)
      user = create(:user, organization: org)
      card1 = create(:card, card_type: 'media', card_subtype: 'link', promoted_at: Time.now - 2.hours.ago, author_id: user.id)
      card2 = create(:card, card_type: 'media', card_subtype: 'link', promoted_at: Time.now, author_id: user.id)

      Card.reindex
      Card.searchkick_index.refresh

      results = Search::CardSearch.new.search(
        user,
        user.organization,
        viewer: nil,
        filter_params: { card_type: Card::EXPOSED_TYPES, state: 'published', ranges: [{promoted_at: {gt: Time.current - 1.hour}}] },
        offset: 0,
        limit: 10,
        sort: :promoted_first
      ).results

      assert_equal 1, results.count
      assert_equal [card2.id], results.map{|r| r['id']}
    end
  end

  class Search::OrgCardSearchTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false

    setup do
      create_default_org
      # Analytics::CardChannelRecorder.stubs(:record)
      WebMock.disable!
      Card.searchkick_index.delete if Card.searchkick_index.exists?

      Card.reindex
      Card.searchkick_index.refresh
    end

    teardown do
      WebMock.enable!
    end

    test 'filter for orgs' do
      default_org = Organization.default_org
      org = create(:organization)
      org_user = create(:user, organization: org)
      org_cards = create_list(:card, 2, card_type: 'media', card_subtype: 'link', author_id: org_user.id, message: 'searchterm')

      default_org_user = create(:user, organization: default_org)
      default_org_cards = create_list(:card, 2, card_type: 'media', card_subtype: 'link', author_id: default_org_user.id, message: 'searchterm')

      Card.reindex
      Card.searchkick_index.refresh

      searchterm_search_results = Search::CardSearch.new.search("searchterm", default_org, viewer: nil)
      assert_same_elements default_org_cards.map(&:id), searchterm_search_results.results.map{|r| r['id']}

      default_org_viewer = create(:user)
      searchterm_search_results = Search::CardSearch.new.search("searchterm", default_org, viewer: default_org_viewer)
      assert_same_elements default_org_cards.map(&:id), searchterm_search_results.results.map{|r| r['id']}

      org_viewer = create(:user, organization: org)
      searchterm_search_results = Search::CardSearch.new.search("searchterm", org, viewer: org_viewer)
      assert_same_elements org_cards.map(&:id), searchterm_search_results.results.map{|r| r['id']}
    end
  end

  class Search::PrivateCardSearchTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false

    setup do
      ChannelsCard.any_instance.stubs :create_users_management_card
      # Analytics::CardChannelRecorder.stubs(:record)
      WebMock.disable!
      Card.searchkick_index.delete if Card.searchkick_index.exists?

      Card.reindex
      Card.searchkick_index.refresh
    end

    teardown do
      WebMock.enable!
    end

    test 'private cards' do
      org = create(:organization, host_name: 'abc')
      link_author, video_author = create_list(:user, 2, organization: org)

      public_link_cards = create_list(:card, 2, card_type: 'media', card_subtype: 'link', is_public: true, author_id: link_author.id)
      tag1 = create(:tag, name: 'tag1')
      public_link_cards.first.tags << tag1
      public_link_cards.first.update_attributes(title: 'title is searchterm')

      public_video_cards = create_list(:card, 2, card_type: 'media', card_subtype: 'video', is_public: true, author_id: video_author.id)
      public_video_cards.first.tags << tag1
      public_video_cards.first.update_attributes(title: 'title is searchterm')

      client = create(:client, organization: org)
      user = create(:user, organization: org)
      private_channel = create(:channel, is_private: true, label: 'private channel', client_id: client.id, organization: org)
      create(:follow, followable: private_channel, user: user)

      private_link_cards = create_list(:card, 2, card_type: 'media', card_subtype: 'link', is_public: true, client_id: nil, author_id: link_author.id)
      private_link_cards.each {|plc| plc.channels << private_channel}
      private_link_cards.first.tags << tag1
      private_link_cards.first.update_attributes(title: 'title is searchterm')

      private_video_cards = create_list(:card, 2, card_type: 'media', card_subtype: 'video', is_public: true, client_id: nil, author_id: video_author.id)
      private_video_cards.each {|plc| plc.channels << private_channel}
      private_video_cards.first.tags << tag1
      private_video_cards.first.update_attributes(title: 'title is searchterm')

      public_channel = create(:channel, is_private: false, label: 'public channel', autopull_content: true, organization: org)

      # explicitly associate channels to cards that had hashtags
      public_link_cards.first.channels << public_channel
      public_video_cards.first.channels << public_channel

      Card.reindex
      Card.searchkick_index.refresh

      # cards created by link_author
      user_public_page_results = Search::CardSearch.new.search(link_author, org, viewer: nil)
      assert_same_elements public_link_cards.map(&:id), user_public_page_results.results.map{|r| r['id']}

      # cards explicitly associated with the public channel
      channel_public_page_results = Search::CardSearch.new.search(public_channel, org, viewer: nil)
      assert_same_elements [public_link_cards.first.id, public_video_cards.first.id], channel_public_page_results.results.map{|r| r['id']}
      assert_equal 2, Search::CardSearch.new.count(public_channel, org, viewer: nil)

      searchterm_search_results = Search::CardSearch.new.search("searchterm", org, viewer: nil)
      assert_same_elements [public_link_cards.first.id, public_video_cards.first.id], searchterm_search_results.results.map{|r| r['id']}

      # private
      user_public_page_results = Search::CardSearch.new.search(link_author, org, viewer: user)
      assert_same_elements (public_link_cards | private_link_cards).map(&:id), user_public_page_results.results.map{|r| r['id']}

      # cards explicitly associated with the public channel
      channel_page_results = Search::CardSearch.new.search(public_channel, org, viewer: user)
      assert_same_elements [public_link_cards.first.id, public_video_cards.first.id], channel_page_results.results.map{|r| r['id']}
      assert_equal 2, Search::CardSearch.new.count(public_channel, org, viewer: user)

      searchterm_search_results = Search::CardSearch.new.search("searchterm", org, viewer: user)
      assert_same_elements [public_link_cards.first.id, public_video_cards.first.id, private_link_cards.first.id, private_video_cards.first.id], searchterm_search_results.results.map{|r| r['id']}

      # testing out for a user who has no access to private content, should return public only
      newu = create(:user)
      user_public_page_results = Search::CardSearch.new.search(link_author, org, viewer: newu)
      assert_same_elements public_link_cards.map(&:id), user_public_page_results.results.map{|r| r['id']}

      # cards explicitly associated with the public channel
      channel_page_results = Search::CardSearch.new.search(public_channel, org, viewer: newu)
      assert_same_elements [public_link_cards.first.id, public_video_cards.first.id], channel_page_results.results.map{|r| r['id']}
      assert_equal 2, Search::CardSearch.new.count(public_channel, org, viewer: newu)

      searchterm_search_results = Search::CardSearch.new.search("searchterm", org, viewer: newu)
      assert_same_elements [public_link_cards.first.id, public_video_cards.first.id], searchterm_search_results.results.map{|r| r['id']}

      # cards explicitly associated with the private channel, user has access
      channel_page_results = Search::CardSearch.new.search(private_channel, org, viewer: user)
      assert_same_elements (private_link_cards | private_video_cards).map(&:id), channel_page_results.results.map{|r| r['id']}
    end
  end

  class Search::CmsCardSearchTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false

    setup do
      ChannelsCard.any_instance.stubs :create_users_management_card
      Analytics::CardChannelRecorder.stubs(:record)
      WebMock.disable!
    end

    teardown do
      WebMock.enable!
    end

    def cards_setup
      @org1, @org2 = create_list(:organization, 2)
      @org1_user = create(:user, organization: @org1)
      @org2_user = create(:user, organization: @org2)
      @all_orgs = [@org1, @org2]
      @link = create(:resource, type: 'Article', url: 'http://url-link.com', image_url: 'http://imageurl.com', title: 'link title', description: 'link description')
      @video = create(:resource, type: 'Video', url: 'http://url-video.com', image_url: 'http://imageurl.com', video_url: 'http://videourl.com', embed_html: '<iframe>blah</iframe>', title: 'video title', description: 'video description')
      @image = create(:resource, type: 'Image', url: 'http://url.com', image_url: 'http://someimage.png', title: 'image title', description: 'image description')
      @image_upload = create(:file_resource)

      pic = Rails.root.join('test/fixtures/images/logo.png')
      @fr1 = FileResource.create(attachment: pic.open)

      @some_channels = create_list(:channel, 2, organization_id: @org1.id)
      @some_tags = [create(:tag, name: 'topic one'), create(:tag, name: "topic two")]

      @all_cards = {@org1.id => [], @org2.id => []}

      @all_cards[@org1.id] << @media_link_card = create(:card, card_type: 'media', card_subtype: 'link', resource_id: @link.id, title: 'link card', message: 'link card message', state: 'new', organization_id: @org1.id, author_id: @org1_user.id)
      @media_link_card.tags << @some_tags
      @media_link_card.channels << @some_channels

      @all_cards[@org1.id] << @media_video_card = create(:card, card_type: 'media', card_subtype: 'video', resource_id: @video.id, title: 'video card', message: 'video card message', organization_id: @org1.id, author_id: @org1_user.id, created_at: Time.now.utc)
      @media_video_card.tags << @some_tags.first
      @media_video_card.channels << @some_channels.first

      @all_cards[@org1.id] << @media_image_card_from_image_url = create(:card, card_type: 'media', card_subtype: 'image', resource_id: @image.id, title: 'image card from image url', message: 'image card message url', organization_id: @org1.id, author_id: @org1_user.id, created_at: 1.day.ago)
      @all_cards[@org1.id] << @media_image_card_from_image_upload = create(:card, card_type: 'media', card_subtype: 'image', title: 'image card from file upload', message: 'image card message file', organization_id: @org1.id, author_id: @org1_user.id, created_at: 2.days.ago, file_resources: [@image_upload])
      @media_image_card_from_image_upload.file_resources << @fr1

      @all_cards[@org1.id] << @media_text_card = create(:card, card_type: 'media', card_subtype: 'text', resource_id: nil, title: 'text card', message: 'text card message', organization_id: @org1.id, author_id: @org1_user.id).destroy

      distraction_poll_card = create(:card, card_type: 'poll', card_subtype: 'link', resource_id: @link.id, state: 'new', organization_id: @org1.id, author_id: @org1_user.id)

      Card.reindex
      Card.searchkick_index.refresh
    end

    test 'cms search with filters should work' do
      cards_setup

      # is official filters
      resp = Search::CardSearch.new.search_for_cms('', @org1.id, filter_params: {is_official: true}, limit: 10, offset: 0)
      assert_same_elements [], resp.results.map(&:id)

      @all_cards[@org1.id].first.update_columns(is_official: true)
      @all_cards[@org1.id].first.reindex
      Card.searchkick_index.refresh

      resp = Search::CardSearch.new.search_for_cms('', @org1.id, filter_params: {is_official: true}, limit: 10, offset: 0)
      assert_same_elements [@all_cards[@org1.id].first.id], resp.results.map(&:id)

      # state filters
      resp = Search::CardSearch.new.search_for_cms('', @org1.id, filter_params: {card_type: ['media'], state: nil}, limit: 10, offset: 0)
      assert_same_elements @all_cards[@org1.id].map(&:id) - [@media_text_card.id], resp.results.map(&:id)


      resp = Search::CardSearch.new.search_for_cms('', @org1.id, filter_params: {card_type: ['media'], state: 'new'}, limit: 10, offset: 0)
      assert_same_elements [@media_link_card.id], resp.results.map(&:id)

      resp = Search::CardSearch.new.search_for_cms('', @org1.id, filter_params: {card_type: ['media'], state: 'published'}, limit: 10, offset: 0)
      assert_same_elements @all_cards[@org1.id].map(&:id) - [@media_link_card.id,@media_text_card.id], resp.results.map(&:id)

      ## card type filter
      # media
      resp = Search::CardSearch.new.search_for_cms('', @org1.id, filter_params: {card_type: ['media']}, limit: 10, offset: 0)
      assert_same_elements @org1.cards.where(card_type: 'media').map(&:id), resp.results.map(&:id)

      # poll
      resp = Search::CardSearch.new.search_for_cms('', @org1.id, filter_params: {card_type: ['poll']}, limit: 10, offset: 0)
      assert_same_elements @org1.cards.where(card_type: 'poll').map(&:id), resp.results.map(&:id)

      ## channel ids filters
      resp = Search::CardSearch.new.search_for_cms('', @org1.id, filter_params: {channel_ids: @some_channels.map(&:id)}, limit: 10, offset: 0)
      assert_same_elements [@media_link_card.id, @media_video_card.id], resp.results.map(&:id)

      ## topics filters
      resp = Search::CardSearch.new.search_for_cms('', @org1.id, filter_params: {topics: @some_tags.map(&:name)}, limit: 10, offset: 0)
      assert_same_elements [@media_link_card.id, @media_video_card.id], resp.results.map(&:id)

      # topic filter has to be complete match
      resp = Search::CardSearch.new.search_for_cms('', @org1.id, filter_params: {topics: ['topic']}, limit: 10, offset: 0)
      assert_same_elements [], resp.results

      # default sort created_at desc
      resp = Search::CardSearch.new.search_for_cms('', @org1.id, filter_params: {state: 'published'}, limit: 10, offset: 0)
      assert_equal [@media_video_card, @media_image_card_from_image_url, @media_image_card_from_image_upload].map(&:id), resp.results.map(&:id)

      # Sort title ascending
      asc_ids = (@all_cards[@org1.id] - [@media_text_card]).sort_by {|c| c.title}.map(&:id)
      resp = Search::CardSearch.new.search_for_cms('', @org1.id, filter_params: {state: nil, card_type: ['media']}, limit: 10, offset: 0, sort: {key: 'title', order: 'asc'})
      assert_equal asc_ids, resp.results.map(&:id)

      # Sort title descending
      desc_ids = (@all_cards[@org1.id] - [@media_text_card]).sort_by {|c| c.title}.map(&:id).reverse
      resp = Search::CardSearch.new.search_for_cms('', @org1.id, filter_params: {state: nil, card_type: ['media']}, limit: 10, offset: 0, sort: {key: 'title', order: 'desc'})
      assert_equal desc_ids, resp.results.map(&:id)
    end

    test 'cms search should work with query term' do
      cards_setup
      resp = Search::CardSearch.new.search_for_cms('link', @org1.id, filter_params: {state: nil}, limit: 10, offset: 0)
      assert_same_elements [@media_link_card.id], resp.results.map(&:id)
    end

    test 'cms search should support range filter' do
      user = create(:user)
      card1 = create(:card, card_type: 'media', card_subtype: 'link', created_at: Time.now - 2.hours, author_id: user.id, state: 'new')
      card2 = create(:card, card_type: 'media', card_subtype: 'link', created_at: Time.now, author_id: user.id, state: 'new')

      Card.reindex
      Card.searchkick_index.refresh

      results = Search::CardSearch.new.search_for_cms(
        '',
        user.organization.id,
        filter_params: { card_type: Card::EXPOSED_TYPES, state: ['new'], ranges: [{created_at: {gt: Time.current - 1.hour}}] },
        offset: 0,
        limit: 10,
      ).results

      assert_equal 1, results.count
      assert_equal [card2.id], results.map{|r| r['id']}
    end
  end

  class Search::EmptyAuthorCardSearchTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false

    setup do
      Analytics::CardChannelRecorder.stubs(:record)
      WebMock.disable!
      Card.searchkick_index.delete if Card.searchkick_index.exists?

      Card.reindex
      Card.searchkick_index.refresh
    end

    teardown do
      WebMock.enable!
    end

    test 'should not return cards with no authors' do
      org = create(:organization)
      user1 = create(:user, organization: org)
      org_viewer = create(:user, organization: org)

      cards1 = cards = create_list(:card, 3, organization_id: org.id, author_id: nil)
      cards2 = cards = create_list(:card, 3, author_id: user1.id)

      Card.reindex

      results = Search::CardSearch.new.search(nil, org, viewer: org_viewer, filter_params: {author_must: true}).results
      assert_same_elements cards2.map(&:id), results.map{|r| r['id']}
      assert_equal 3, results.count
    end

    test 'should return official cards' do
      org = create(:organization)
      org_user = create(:user, organization: org)
      org_viewer = create(:user, organization: org)
      cards = create_list(:card,2,  organization_id: org.id, is_official: true, author: org_user)
      non_official_cards = create_list(:card,2,  organization_id: org.id, is_official: false, author: org_user)

      Card.reindex
      Card.searchkick_index.refresh
      results = Search::CardSearch.new.search(nil, org, viewer: org_viewer, filter_params: {is_official: true}).results
      assert_same_elements cards.map(&:id), results.map{|r| r['id']}
      assert_equal 2, results.count
    end

    test 'should filter by channel_ids' do
      org = create(:organization)
      user = create(:user, organization: org)
      channel1, channel2 = create_list(:channel, 2, organization: org)

      channel1_cards = create_list(:card, 2, author: nil, organization: org)
      channel1_cards.each {|card| card.channels << channel1 }

      channel2_cards = create_list(:card, 2, author: nil, organization:org)
      channel2_cards.each {|card| card.channels << channel2 }

      Card.reindex
      Card.searchkick_index.refresh
      results = Search::CardSearch.new.search(nil, org, viewer: user, filter_params: {channel_ids: [channel1.id]}).results
      assert_same_elements channel1_cards.map(&:id), results.map{|r| r['id']}
    end

    test 'should sort by promoted first, then chronological' do
      skip("INTERMITTENT FAILURE ON TRAVIS")
      org = create(:organization)
      org_user = create(:user, organization: org)
      org_viewer = create(:user, organization: org)
      cards = create_list(:card,2,  organization_id: org.id, author: org_user)
      non_official_cards = create_list(:card, 2, organization_id: org.id, is_official: false, author: org_user)
      time = Time.now
      cards.each{|card| card.update_attributes(is_official: true, promoted_at: time)}

      (cards + non_official_cards).each_with_index do |card, indx|
        card.update_attributes(published_at: time - indx.days)
      end

      Card.reindex
      Card.searchkick_index.refresh
      results = Search::CardSearch.new.search(nil, org, viewer: org_viewer, filter_params: {}, sort: :promoted_first).results

      assert_equal 4, results.count
      assert_equal((cards + non_official_cards).map(&:id), results.map{|r| r['id']})
    end
  end

  class Search::CmsPathwaysTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false
    include CardHelperMethods

    setup do
      Analytics::CardChannelRecorder.stubs(:record)
      ChannelsCard.any_instance.stubs :create_users_management_card
    end

    def setup_pack_cards
      @org1 = create(:organization)
      @org2 = create(:organization)

      @user1 = create(:user, organization: @org1)
      @user2 = create(:user, organization: @org2)

      #covers
      @pack_covers1 = create_list(:card, 4, card_type: 'pack', card_subtype: 'link', author_id: @user1.id, title: 'pack cover', organization: @org1)
      @link_cover = create(:card, card_type: 'pack', card_subtype: 'link', author_id: @user1.id, title: 'pack cover', organization: @org1)
      @pack_covers1 << @link_cover
      @cover_with_no_channel = create(:card, card_type: 'pack', card_subtype: 'link', author_id: @user1.id, title: 'some different card', organization: @org1)

      @pack_covers2 = create_list(:card, 3, card_type: 'pack', card_subtype: 'link', author_id: @user2.id, title: 'pack cover', organization: @org2)

      (@pack_covers1 + [@cover_with_no_channel]).each do |cover|
        CardPackRelation.add(cover_id: cover.id, add_id: create(:card, author: @user1).id)
        cover.publish!
      end
      @link_cover.destroy

      @pack_covers2.each do |cover|
        CardPackRelation.add(cover_id: cover.id, add_id: create(:card, author: @user2).id)
        cover.publish!
      end

      @org1_covers = (@pack_covers1 + [@cover_with_no_channel]).sort_by(&:created_at)
      @org2_covers =  @pack_covers2.sort_by(&:created_at)

      # topics
      @topics = []
      @topics << create(:tag, name: 'sunny')
      @topics << create(:tag, name: 'pizza')
      @topics << create(:tag, name: 'san francisco')

      # channels
      @channels = []
      @channels << create(:channel, label: 'pogo', organization: @org1)
      @channels << create(:channel, label: 'disney', organization: @org1)
      @channels << create(:channel, label: 'cartoon network', organization: @org2)

      # org 1 packs
      @pack_covers1.each_with_index do |cover, idx|
        tags, channels = if idx.odd?
          [@topics.first(2), @channels.first(1)]
        else
          [ @topics.last(1), [@channels[1]] ]
        end
        add_tags_and_channels_to_cover cover, tags, channels
        add_cards_to_cover cover
      end

      # org 2 packs
      @pack_covers2.each_with_index do |cover, idx|
        tags, channels = if idx.odd?
          [@topics.first(2), []]
        else
          [@topics.last(1), @channels.last(1)]
        end
        add_tags_and_channels_to_cover cover, tags, channels
        add_cards_to_cover cover
      end

      add_cards_to_cover(@cover_with_no_channel)

      Card.reindex
      Card.searchkick_index.refresh
    end

    setup do
      WebMock.disable!
      setup_pack_cards
    end

    teardown do
      WebMock.enable!
    end
    # Each test was taking about 20s. Aggregated all test one with one test dataset
    test 'should be able to get pathways for cms' do
      # org1
      resp = Search::CardSearch.new.search_pathways_for_cms('', @org1.id, filter_params: {}, limit: 10, offset: 0)
      assert_equal 5, resp.results.count
      assert_same_elements @org1_covers.map(&:id) - [@link_cover.id], resp.results.map {|item| item['id']}
      assert_empty @org2_covers.map(&:id) & resp.results.map {|item| item['id']}

      # org 2
      resp = Search::CardSearch.new.search_pathways_for_cms('', @org2.id, filter_params: {}, limit: 10, offset: 0)

      assert_equal 3, resp.results.count
      assert_same_elements @org2_covers.map(&:id), resp.results.map {|item| item['id']}
      assert_empty @org1_covers.map(&:id) & resp.results.map {|item| item['id']}
      # ----- test 1 ends

      # search pathways
      resp = Search::CardSearch.new.search_pathways_for_cms('draft', @org1.id, filter_params: {}, limit: 10, offset: 0)
      assert_empty resp.results.map {|item| item['id']}

      resp = Search::CardSearch.new.search_pathways_for_cms('pack cover', @org2.id, filter_params: {}, limit: 10, offset: 0)

      assert_equal 3, resp.results.count
      assert_same_elements @org2_covers.map(&:id), resp.results.map {|item| item['id']}

      # test 2---ends

      # state filter
      # published
      resp = Search::CardSearch.new.search_pathways_for_cms('', @org1.id, filter_params: {state: 'published'}, limit: 10, offset: 0)

      assert_equal 5, resp.results.count
      assert_same_elements (@pack_covers1.first(4) + [@cover_with_no_channel]).map(&:id), resp.results.map {|item| item['id']}

      #org 2 published
      resp = Search::CardSearch.new.search_pathways_for_cms('pack', @org2.id, filter_params: {state: 'published'}, limit: 10, offset: 0)

      assert_equal 3, resp.results.count
      assert_same_elements @pack_covers2.first(3).map(&:id), resp.results.map {|item| item['id']}
      # -----test 3 ends


      # channel filter
      # only odd configs has channel1 attached
      resp = Search::CardSearch.new.search_pathways_for_cms('', @org1.id, filter_params: {channel_ids: @channels.first(1).map(&:id)}, limit: 10, offset: 0)

      assert_equal 2, resp.results.count
      assert_same_elements [@pack_covers1[1], @pack_covers1[3]].map(&:id), resp.results.map {|item| item['id']}

      #Non odd configs has channel2 attached in org2
      resp = Search::CardSearch.new.search_pathways_for_cms('', @org2.id, filter_params: {channel_ids: @channels.last(1).map(&:id)}, limit: 10, offset: 0)

      assert_equal 2, resp.results.count
      assert_same_elements [@pack_covers2[0], @pack_covers2[2]].map(&:id), resp.results.map {|item| item['id']}

      # test 4 ends ---------
      # test 'should support topics filter'
      # odd configs has first two topics attached
      resp = Search::CardSearch.new.search_pathways_for_cms('pack', @org1.id, filter_params: {topics: ['pizza']}, limit: 10, offset: 0)

      assert_equal 2, resp.results.count
      assert_same_elements [@pack_covers1[1], @pack_covers1[3]].map(&:id), resp.results.map {|item| item['id']}

      #non -odd stuff
      resp = Search::CardSearch.new.search_pathways_for_cms('', @org1.id, filter_params: {topics: ['san francisco']}, limit: 10, offset: 0)

      assert_equal 2, resp.results.count
      assert_same_elements [@pack_covers1[0], @pack_covers1[2]].map(&:id), resp.results.map {|item| item['id']}

      # test 5---ends
      # test 'should support all filters'
      resp = Search::CardSearch.new.search_pathways_for_cms('pack', @org1.id, filter_params: {topics: ['sunny'], channel_ids: @channels.first(1).map(&:id), state: 'published'}, limit: 10, offset: 0)
      assert_equal 2, resp.results.count
      assert_same_elements [@pack_covers1[3], @pack_covers1[1]].map(&:id), resp.results.map {|item| item['id']}

      # test 6 ends ----
      # test 'should return pathways with missing channel when channel filter is passed'
      resp = Search::CardSearch.new.search_pathways_for_cms('', @org1.id, filter_params: {card_type: ['pack'], allow_empty_channel_ids: true, channel_ids: @channels.first(1).map(&:id)}, limit: 10, offset: 0)

      assert_equal 3, resp.results.count
      assert_same_elements [@pack_covers1[1], @pack_covers1[3], @cover_with_no_channel].map(&:id), resp.results.map {|item| item['id']}
    end

    #Get published pathways for ECR
    # test 'should exclude draft pathways' do
    #   skip #feature deprecated
    #   resp = Search::CardSearch.new.search_pathways_for_cms('', @org1.id, filter_params: {card_type: ['pack']}, limit: 10, offset: 0)
    #   assert_equal 5, resp.results.count
    #   assert_same_elements (@pack_covers1 + [@cover_with_no_channel] - [@link_cover]).map(&:id), resp.results.map {|item| item['id']}
    # end
  end

  class Search::SearchInChannelTest < ActiveSupport::TestCase
    setup do
      @organization = create(:organization)
      @channel = create(:channel, label: 'test channel', organization: @organization)

      # Create cards to be added to channel
      @author = create(:user, organization: @organization)

      @channel_user = create(:user, organization: @organization)

      WebMock.disable!
    end

    teardown do
      WebMock.enable!
    end

    def create_channel_cards_for_entitlements
      @public_card_1 = create(:card, organization: @organization, author: @author, title: 'Public card 1')
      @public_card_2 = create(:card, organization: @organization, author: @author, title: 'Public card 2')
      @private_card_1 = create(:card, organization: @organization, author: @author, title: 'Private card 1', is_public: false)
      @private_card_2 = create(:card, organization: @organization, author: @author, title: 'Private card 2', is_public: false)
      @channel.cards << [@public_card_1, @public_card_2, @private_card_1, @private_card_2]
      Card.reindex
      Card.searchkick_index.refresh
    end

    test 'should be able to search for cards within channel' do
      create_channel_cards_for_entitlements
      @channel.add_followers([@channel_user])

      # When q = 'card'
      json_response = Search::CardSearch.new.search_in_channel(q: 'card', organization_id: @organization.id,
        channel_id: @channel.id, viewer: @channel_user, filter: {}, offset: 0, limit: 10, load: true)
      assert_equal 4, json_response[:total]
      assert_same_elements @channel.cards.map(&:id), json_response[:results].map{ |card| card['id'].to_i }

      # When q = ''
      json_response = Search::CardSearch.new.search_in_channel(q: '', organization_id: @organization.id,
        channel_id: @channel.id, viewer: @channel_user, filter: {}, offset: 0, limit: 10, load: true)
      assert_equal 4, json_response[:total]
      assert_same_elements @channel.cards.map(&:id), json_response[:results].map{ |card| card['id'].to_i }

      # When q = '*'
      json_response = Search::CardSearch.new.search_in_channel(q: '*', organization_id: @organization.id,
        channel_id: @channel.id, viewer: @channel_user, filter: {}, offset: 0, limit: 10, load: true)
      assert_equal 4, json_response[:total]
      assert_same_elements @channel.cards.map(&:id), json_response[:results].map{ |card| card['id'].to_i }
    end

    test 'should not be able to get private cards with * search for a user not following the channel' do
      create_channel_cards_for_entitlements

      json_response = Search::CardSearch.new.search_in_channel(q: '*', organization_id: @organization.id,
        channel_id: @channel.id, viewer: @channel_user, filter: {}, offset: 0, limit: 10, load: true)
      assert_equal 2, json_response[:total]
      assert_same_elements [@public_card_1.id, @public_card_2.id], json_response[:results].map{ |card| card['id'].to_i }
    end

    test 'private card restricted to a user should not be visible to other channel followers' do
      create_channel_cards_for_entitlements
      # Restrict a private card to logged-in user
      create(:card_user_permission, card_id: @private_card_1.id, user_id: @channel_user.id)
      Card.reindex
      Card.searchkick_index.refresh

      # Create another user as follower to channel
      user = create(:user, organization: @organization)
      @channel.add_followers([user])

      json_response = Search::CardSearch.new.search_in_channel(q: 'card', organization_id: @organization.id,
        channel_id: @channel.id, viewer: user, filter: {}, offset: 0, limit: 10, load: true)
      assert_equal 3, json_response[:total]
      assert_same_elements [@public_card_1.id, @public_card_2.id, @private_card_2.id], json_response[:results].map{ |card| card['id'].to_i }
    end

    test 'private card restricted to a team should not be visible to other channel followers' do
      create_channel_cards_for_entitlements
      # Create a team and add logged-in user to the team
      team = create(:team, organization: @organization)
      create(:teams_user, team_id: team.id, user_id: @channel_user.id)

      # Restrict a private card to team
      create(:card_team_permission, card_id: @private_card_1.id, team_id: team.id)
      Card.reindex
      Card.searchkick_index.refresh

      # Create another user as follower to channel
      user = create(:user, organization: @organization)
      @channel.add_followers([user])

      json_response = Search::CardSearch.new.search_in_channel(q: 'card', organization_id: @organization.id,
        channel_id: @channel.id, viewer: user, filter: {}, offset: 0, limit: 10, load: true)
      assert_equal 3, json_response[:total]
      assert_same_elements [@public_card_1.id, @public_card_2.id, @private_card_2.id], json_response[:results].map{ |card| card['id'].to_i }
    end

    test 'should return cards by card type' do
      media_card = create(:card, organization: @organization, author: @author, title: 'Media card', card_type: 'media', card_subtype: 'text')
      poll_card = create(:card, organization: @organization, author: @author, title: 'Poll card', card_type: 'poll')
      @channel.cards << [media_card, poll_card]

      @channel.add_followers([@channel_user])
      Card.reindex
      Card.searchkick_index.refresh

      expected_aggs = [
        { type: 'content_type', display_name: media_card.card_subtype, id: media_card.card_subtype, count: 1, card_type: 'media', card_subtype: 'text'},
        { type: 'content_type', display_name: 'poll', id: 'poll', count: 1, card_type: 'poll', card_subtype: ''},
        { type: 'author_id', display_name: @author.name, id: @author.id, count: 2 }
      ]

      json_response = Search::CardSearch.new.search_in_channel(q: 'card', organization_id: @organization.id,
        filter: { content_type: [{'card_type' => 'media', 'card_subtype' => 'text'}] },
        channel_id: @channel.id, viewer: @channel_user, offset: 0, limit: 10, load: true)
      assert_equal 1, json_response[:total]
      assert_equal [media_card.id], json_response[:results].map{ |card| card['id'].to_i }
      assert_same_elements expected_aggs, json_response[:aggs]
    end

    test 'should return cards by author' do
      author_1 = create(:user, organization: @organization)
      card = create(:card, organization: @organization, author: author_1, title: 'Card by Author 1')
      @channel.cards << [card]

      @channel.add_followers([@channel_user])
      Card.reindex
      Card.searchkick_index.refresh

      expected_aggs = [
        { type: 'content_type', display_name: card.card_subtype, id: card.card_subtype, count: 1, card_type: 'media', card_subtype: 'text'},
        { type: 'author_id', display_name: author_1.name, id: author_1.id, count: 1 }
      ]

      json_response = Search::CardSearch.new.search_in_channel(q: 'card', organization_id: @organization.id,
        filter: { author_id: [author_1.id] },
        channel_id: @channel.id, viewer: @channel_user, offset: 0, limit: 10, load: true)
      assert_equal 1, json_response[:total]
      assert_equal [card.id], json_response[:results].map{ |card| card['id'].to_i }
      assert_same_elements expected_aggs, json_response[:aggs]
    end

    test 'should return cards by source name' do
      card = create(:card, organization: @organization, author: @author, title: 'Card of TEST source',
        ecl_metadata: { source_display_name: 'test' })
      @channel.cards << [card]

      @channel.add_followers([@channel_user])
      Card.reindex
      Card.searchkick_index.refresh

      expected_aggs = [
        { type: 'content_type', display_name: card.card_subtype, id: card.card_subtype, count: 1, card_type: 'media', card_subtype: 'text'},
        { type: 'author_id', display_name: @author.name, id: @author.id, count: 1 },
        { type: 'source_display_name', display_name: card.ecl_metadata['source_display_name'], id: card.ecl_metadata['source_display_name'], count: 1}
      ]

      json_response = Search::CardSearch.new.search_in_channel(q: 'card', organization_id: @organization.id,
        filter: { source_display_name: ['test'] },
        channel_id: @channel.id, viewer: @channel_user, offset: 0, limit: 10, load: true)
      assert_equal 1, json_response[:total]
      assert_equal [card.id], json_response[:results].map{ |card| card['id'].to_i }
      assert_same_elements expected_aggs, json_response[:aggs]
    end

    test 'should return cards by date range' do
      card_1 = create(:card, organization: @organization, author: @author, title: 'Card 1', created_at: DateTime.parse('01/12/2018'))
      card_2 = create(:card, organization: @organization, author: @author, title: 'Card 2', created_at: DateTime.parse('02/12/2018'))
      card_3 = create(:card, organization: @organization, author: @author, title: 'Card 3', created_at: Date.today.to_time)
      @channel.cards << [card_1, card_2, card_3]

      @channel.add_followers([@channel_user])
      Card.reindex
      Card.searchkick_index.refresh

      json_response = Search::CardSearch.new.search_in_channel(q: 'card', organization_id: @organization.id,
        filter: { date_range: [{'from_date' => DateTime.parse('02/12/2018'), 'till_date' => Date.today.to_time }] },
        channel_id: @channel.id, viewer: @channel_user, offset: 0, limit: 10, load: true)
      assert_equal 2, json_response[:total]
      assert_equal [card_3.id, card_2.id], json_response[:results].map{ |card| card['id'].to_i }
    end

    test 'should return cards by different combination of filter options' do
      author_1 = create(:user, organization: @organization)
      card_1 = create(:card, organization: @organization, author: @author, title: 'Card 1',
        created_at: DateTime.parse('01/12/2018'), ecl_metadata: { source_display_name: 'test' }, card_type: 'media', card_subtype: 'text')
      card_2 = create(:card, organization: @organization, author: author_1, title: 'Card 2',
        created_at: DateTime.parse('02/12/2018'), ecl_metadata: { source_display_name: 'test' }, card_type: 'poll')
      @channel.cards << [card_1, card_2]

      @channel.add_followers([@channel_user])
      Card.reindex
      Card.searchkick_index.refresh

      # Card type and source name
      json_response = Search::CardSearch.new.search_in_channel(q: 'card', organization_id: @organization.id,
        filter: { content_type: [{'card_type' => 'media', 'card_subtype' => 'text'}], source_display_name: ['test'] },
        channel_id: @channel.id, viewer: @channel_user, offset: 0, limit: 10, load: true)
      assert_equal 1, json_response[:total]
      assert_equal [card_1.id], json_response[:results].map{ |card| card['id'].to_i }

      # Card type and author
      json_response = Search::CardSearch.new.search_in_channel(q: 'card', organization_id: @organization.id,
        filter: { content_type: [{'card_type' => 'media', 'card_subtype' => 'text'}], author_id: [@author.id] },
        channel_id: @channel.id, viewer: @channel_user, offset: 0, limit: 10, load: true)
      assert_equal 1, json_response[:total]
      assert_equal [card_1.id], json_response[:results].map{ |card| card['id'].to_i }

      # Card type and date range
      json_response = Search::CardSearch.new.search_in_channel(q: 'card', organization_id: @organization.id,
        filter: { content_type: [{'card_type' => 'media', 'card_subtype' => 'text'}],
                  date_range: [{'from_date' => DateTime.parse('01/12/2018'), 'till_date' => DateTime.parse('30/12/3018')}] },
        channel_id: @channel.id, viewer: @channel_user, offset: 0, limit: 10, load: true)
      assert_equal 1, json_response[:total]
      assert_equal [card_1.id], json_response[:results].map{ |card| card['id'].to_i }

      # Source name and author
      json_response = Search::CardSearch.new.search_in_channel(q: 'card', organization_id: @organization.id,
        filter: { source_display_name: ['test'], author_id: [author_1.id] },
        channel_id: @channel.id, viewer: @channel_user, offset: 0, limit: 10, load: true)
      assert_equal 1, json_response[:total]
      assert_equal [card_2.id], json_response[:results].map{ |card| card['id'].to_i }

      # Source name and date range
      json_response = Search::CardSearch.new.search_in_channel(q: 'card', organization_id: @organization.id,
        filter: { source_display_name: ['test'],
                  date_range: [{'from_date' => DateTime.parse('02/12/2018'), 'till_date' => DateTime.parse('30/12/3018')}]},
        channel_id: @channel.id, viewer: @channel_user, offset: 0, limit: 10, load: true)
      assert_equal 1, json_response[:total]
      assert_equal [card_2.id], json_response[:results].map{ |card| card['id'].to_i }

      # Author and date range
      json_response = Search::CardSearch.new.search_in_channel(q: 'card', organization_id: @organization.id,
        filter: { author_id: [author_1.id],
                  date_range: [{'from_date' => DateTime.parse('02/12/2018'), 'till_date' => DateTime.parse('30/12/3018')}]},
        channel_id: @channel.id, viewer: @channel_user, offset: 0, limit: 10, load: true)
      assert_equal 1, json_response[:total]
      assert_equal [card_2.id], json_response[:results].map{ |card| card['id'].to_i }
    end

    test 'should not return users dismissed cards in search for non admin user' do
      card_1 = create(:card, organization: @organization, author: @author, title: 'Card 1', created_at: DateTime.parse('01/12/2018'))
      card_2 = create(:card, organization: @organization, author: @author, title: 'Card 2', created_at: DateTime.parse('02/12/2018'))
      @channel.cards << [card_1, card_2]

      @channel.add_followers([@channel_user])
      Card.reindex
      Card.searchkick_index.refresh

      # Dismiss card for channel_user
      create(:dismissed_content, content_id: card_1.id, content_type: 'Card', user_id: @channel_user.id)

      json_response = Search::CardSearch.new.search_in_channel(q: 'card', organization_id: @organization.id,
        channel_id: @channel.id, viewer: @channel_user, filter: {}, offset: 0, limit: 10, load: true)
      assert_equal 1, json_response[:total]
      assert_same_elements [card_2.id], json_response[:results].map{ |card| card['id'].to_i }
    end

    test 'should return dismissed cards in search if the request is from cms' do
      card_1 = create(:card, organization: @organization, author: @author, title: 'Card 1', created_at: DateTime.parse('01/12/2018'))
      card_2 = create(:card, organization: @organization, author: @author, title: 'Card 2', created_at: DateTime.parse('02/12/2018'))
      @channel.cards << [card_1, card_2]

      @channel.add_followers([@channel_user])
      Card.reindex
      Card.searchkick_index.refresh

      # Dismiss card for admin user
      create(:dismissed_content, content_id: card_1.id, content_type: 'Card', user_id: @channel_user.id)

      json_response = Search::CardSearch.new.search_in_channel(q: 'card', organization_id: @organization.id,
        channel_id: @channel.id, viewer: @channel_user, filter: { is_cms: true }, offset: 0, limit: 10, load: true)
      assert_equal 2, json_response[:total]
      assert_same_elements [card_1.id, card_2.id], json_response[:results].map{ |card| card['id'].to_i }
    end

    test 'should only return curated cards in search' do
      curatable_channel = create(:channel, organization: @organization, curate_only: true, curate_ugc: true)
      curatable_channel.add_followers([@channel_user])

      card_1 = create(:card, organization: @organization, author: @author, title: 'Card 1', created_at: DateTime.parse('01/12/2018'))
      card_2 = create(:card, organization: @organization, author: @author, title: 'Card 2', created_at: DateTime.parse('02/12/2018'))
      curatable_channel.cards << [card_1, card_2]

      # Curate card_1
      ChannelsCard.where(card_id: card_1.id, channel_id: curatable_channel.id).first.curate!

      Card.reindex
      Card.searchkick_index.refresh

      json_response = Search::CardSearch.new.search_in_channel(q: 'card', organization_id: @organization.id,
        channel_id: curatable_channel.id, viewer: @channel_user, filter: {}, offset: 0, limit: 10, load: true)
      assert_equal 1, json_response[:total]
      assert_same_elements [card_1.id], json_response[:results].map{ |card| card['id'].to_i }
    end

    test 'should not return aggregations if skip_aggs is true' do
      card_1 = create(:card, organization: @organization, author: @author, title: 'Card 1', created_at: DateTime.parse('01/12/2018'))
      card_2 = create(:card, organization: @organization, author: @author, title: 'Card 2', created_at: DateTime.parse('02/12/2018'))
      @channel.cards << [card_1, card_2]

      @channel.add_followers([@channel_user])
      Card.reindex
      Card.searchkick_index.refresh

      json_response = Search::CardSearch.new.search_in_channel(q: 'card', organization_id: @organization.id,
        channel_id: @channel.id, viewer: @channel_user, filter: { skip_aggs: true}, offset: 0, limit: 10, load: true)
      assert_equal 2, json_response[:total]
      assert_same_elements [card_1.id, card_2.id], json_response[:results].map{ |card| card['id'].to_i }
      assert_nil json_response[:aggs]
    end
  end
end

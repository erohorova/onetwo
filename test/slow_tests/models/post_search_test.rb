require 'test_helper'
class PostSearchTest < ActiveSupport::TestCase

  self.use_transactional_fixtures = false

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    WebMock.disable!
    if Post.searchkick_index.exists?
      Post.searchkick_index.delete
    end

    UserContentsQueue.stubs(:enqueue_for_user)

    # User.any_instance.stubs(:reindex_async).returns(true)
    @org = create(:organization)
    @client = create(:client, organization: @org)
    @user = create(:user, organization: @org)
    @faculty = create(:user, organization: @org)
    @group = create(:resource_group, organization: @org, client: @client)
    @group.add_member @user
    @group.add_admin @faculty
    Post.reindex
  end

  teardown do
    # User.any_instance.unstub(:reindex_async)
    WebMock.enable!
  end

  test 'should support search' do
    question = create(:question, group: @group, user: @user, title: 'question 1', message: 'this is the question 1')
    answer = create(:answer, commentable: question, user: @user, message: 'answer 1')
    question = create(:question, group: @group, user: @user, title: 'question 2', message: 'this is the question 2')
    answer = create(:answer, commentable: question, user: @user)
    question = create(:question, group: @group, user: @user, title: 'question 3', message: 'this is the question 3')
    answer = create(:answer, commentable: question, user: @user)

    Post.reindex

    questions = Post.search '*', where: {postable_id: @group.id, postable_type: 'Group'}, load: false

    assert_instance_of Searchkick::Results, questions
    assert_instance_of Hashie::Mash, questions.first
    assert_equal 'question 1', questions.first.title
    assert_equal 3, questions.size
  end

  test 'async indexing' do
    question = Question.new user: @user, postable: @group, title: 'my question title', message: 'my question message'
    assert question.save
    Post.reindex
    questions = Post.search '*', where: {postable_id: @group.id, postable_type: 'Group'},load: false
    assert_equal 1, questions.size

    assert question.destroy
    assert question.destroyed?

    Post.reindex
    questions = Post.search '*', where: {postable_id: @group.id, postable_type: 'Group'},load: false
    assert_empty questions, 'should remove from the index'

  end

  test 'should search over answer' do
    question1 = create(:question, group: @group, user: @user, title: 'question 1', message: 'this is the question 1')
    answer1 = create(:answer, commentable: question1, user: @user, message: 'answer one')
    question2 = create(:question, group: @group, user: @user, title: 'question 2', message: 'this is the question 2')
    answer2 = create(:answer, commentable: question2, user: @user , message: 'answer two')
    question3 = create(:question, group: @group, user: @user, title: 'question 3', message: 'this is the question 3')
    answer3 = create(:answer, commentable: question3, user: @user, message: 'answer three')

    Post.reindex

    questions = Post.search 'answer two', where: {group_id: @group.id}, load: false

    assert_equal 1, questions.size
    assert_equal question2.id, questions.first.id.to_i
  end

  test 'retrieve should search announcements' do
    q1 = create(:question, group: @group, user: @user, title: 'q1', message: 'searchtext')
    q2 = create(:question, group: @group, user: @user, title: 'q2', message: 'random')
    a1 = create(:announcement, group: @group, user: @faculty, title: 'a1', message: 'searchtext')

    Post.reindex

    results = Post.retrieve(filter_params: {group_id: @group.id},
                            sort_params: {created_at: :desc},
                            search_query: 'searchtext')
    assert_equal 2, results.size
    assert [a1.id, q1.id].include?(results[0].id.to_i)
    assert [a1.id, q1.id].include?(results[1].id.to_i)
  end

  test 'retrieve should filter by announcements or questions' do
    q1 = create(:question, group: @group, user: @user, title: 'q1', message: 'searchtext')
    q2 = create(:question, group: @group, user: @user, title: 'q2', message: 'random')
    a1 = create(:announcement, group: @group, user: @faculty, title: 'a1', message: 'searchtext')

    Post.reindex

    results = Post.retrieve(filter_params: {group_id: @group.id,
                                            type: 'Announcement'},
                            sort_params: {created_at: :desc},
                            search_query: 'searchtext')
    assert_equal results.size, 1
    assert_equal results[0].id.to_i, a1.id

    results = Post.retrieve(filter_params: {group_id: @group.id,
                                            type: 'Question'},
                            sort_params: {created_at: :desc},
                            search_query: 'searchtext')
    assert_equal results.size, 1
    assert_equal results[0].id.to_i, q1.id
  end

  test 'filtering should fail gracefully when no results present' do

    # crearting some dummy elements to create mappings
    g = create(:group)
    u = create(:user)
    g.add_member u
    f = create(:user)
    g.add_admin f

    create(:announcement, user: f, group: g)
    create(:question, user: u, group: g)

    Post.reindex

    results = Post.retrieve(filter_params: {group_id: @group.id,
                                            type: 'Announcement'},
                            sort_params: {created_at: :desc},
                            search_query: 'searchtext')
    assert_equal results.size, 0

    results = Post.retrieve(filter_params: {group_id: @group.id,
                                            type: 'Question'},
                            sort_params: {created_at: :desc},
                            search_query: 'searchtext')
    assert_equal results.size, 0
  end

  test 'should sort by pinned and then date' do
    q1 = create(:question, group: @group, user: @user,
                pinned: true, created_at: Time.now)
    q2 = create(:question, group: @group, user: @user,
                pinned: true, created_at: 1.day.ago)
    q3 = create(:question, group: @group, user: @user,
                pinned: false, created_at: Time.now)
    q4 = create(:question, group: @group, user: @user,
                pinned: false, created_at: 1.day.ago)

    Post.reindex

    results = Post.retrieve(filter_params: {group_id: @group.id},
                            sort_params: {pinned: :desc, created_at: :desc},
                            search_query: '*')
    assert_equal results.count, 4
    assert_equal((results.map { |r| r.id.to_i }), [q1.id, q2.id, q3.id, q4.id])
  end

  test 'should search over tag' do
    question1 = create(:question, group: @group, user: @user, title: 'question 1', message: 'this is the question 1')
    answer1 = create(:answer, commentable: question1, user: @user, message: 'answer one')
    question2 = create(:question, group: @group, user: @user, title: 'question 2', message: 'this is the question 2')
    question2.tags.create(name: 'mytag')
    answer2 = create(:answer, commentable: question2, user: @user , message: 'answer two')
    question3 = create(:question, group: @group, user: @user, title: 'question 3', message: 'this is the question 3')
    answer3 = create(:answer, commentable: question3, user: @user, message: 'answer three')

    Post.reindex

    questions = Post.search 'mytag', where: {group_id: @group.id}, load: false

    assert_equal 1, questions.size
    assert_equal 'mytag', questions.first.tags.first.name
  end

  def retrieve_setup
    @g1 = create(:resource_group, name: 'group1')

    @u1 = create(:user, email: 'user1@me.com')
    @u2 = create(:user, email: 'user2@me.com')

    @g1.add_member @u1
    @g1.add_member @u2
    @g1.add_member @user

    @q1 = create(:question,
                 group: @g1, user: @u1, title: 'blah?', message: 'alah #q1')
    @q2 = create(:question,
                 group: @g1, user: @u2, title: 'clah?', message: 'alah #q2')
    @q3 = create(:question,
                 group: @group, user: @user, title: 'glah?', message: 'alah #q3')

    # only q1 has a response
    @a1 = create(:answer, user: @u1,
                 commentable: @q1, message: 'xlah')

    # only q2 has votes
    create(:vote, votable: @q2, voter: @user)

    Post.reindex
  end

  test "pagination with different creation time should work" do

    old = create(:question, group: @group, user: @user, title: 'edcast', created_at: 2.days.ago)
    new = create(:question, group: @group, user: @user, title: 'edcast', created_at: 1.days.ago)

    Post.reindex

    # test descending
    filter_params = {:group_id => @group.id}
    sort_params = {:created_at => :desc}
    results = Post.retrieve(filter_params: filter_params,
                              sort_params: sort_params,
                              search_query: 'edcast',
                              limit: 1)

    assert_equal results.size, 1
    assert_equal results[0].id.to_i, new.id

    # Now get the next page
    # filter_params[:created_at] = new.created_at
    results = Post.retrieve(filter_params: filter_params,
                              sort_params: sort_params,
                              search_query: 'edcast',
                              limit: 1,
                              offset: 1)

    assert_equal results.size, 1
    assert_equal results[0].id.to_i, old.id

    # test ascending
    filter_params = {:group_id => @group.id}
    sort_params = {:created_at => :asc}
    results = Post.retrieve(filter_params: filter_params,
                              sort_params: sort_params,
                              search_query: 'edcast',
                              limit: 1)

    assert_equal results.size, 1
    assert_equal results[0].id.to_i, old.id

    # Now get the next page
    # filter_params[:created_at] = old.created_at
    results = Post.retrieve(filter_params: filter_params,
                              sort_params: sort_params,
                              search_query: 'edcast',
                              limit: 1,
                              offset: 1)

    assert_equal results.size, 1
    assert_equal results[0].id.to_i, new.id
  end

  test "pagination with different answer count should work" do
    retrieve_setup

    # q1 has 2 answers, q2 has one
    create(:answer, user: @u1, commentable: @q1)
    create(:answer, user: @u1, commentable: @q2)
    @q2.reload
    @q1.reload
    Post.reindex

    # test descending
    filter_params = {:group_id => @g1.id}
    sort_params = {:comments_count => :desc}

    results = Post.retrieve(filter_params: filter_params,
                              sort_params: sort_params,
                              search_query: 'alah',
                              misspelling: 0,
                              limit: 1)

    assert_equal results.size, 1
    assert_equal results[0].id.to_i, @q1.id

    # Now get the next page
    # filter_params[:comments_count] = @q1.comments_count
    results = Post.retrieve(filter_params: filter_params,
                              sort_params: sort_params,
                              search_query: 'alah',
                              misspelling: 0,
                              limit: 1,
                              offset: 1)

    assert_equal results.size, 1
    assert_equal results[0].id.to_i, @q2.id

    # test ascending
    filter_params = {:group_id => @g1.id}
    sort_params = {:comments_count => :asc}
    results = Post.retrieve(filter_params: filter_params,
                              sort_params: sort_params,
                              search_query: 'alah',
                              limit: 1)

    assert_equal results.size, 1
    assert_equal results[0].id.to_i, @q2.id

    # Now get the next page
    #filter_params[:comments_count] = @q2.comments_count
    results = Post.retrieve(filter_params: filter_params,
                              sort_params: sort_params,
                              search_query: 'alah',
                              limit: 1,
                              offset: 1)
    assert_equal results.size, 1
    assert_equal results[0].id.to_i, @q1.id
  end

  test "pagination with different vote count should work" do
    retrieve_setup

    # q2 has 2 votes, q1 has one
    create(:vote, votable: @q2, voter: @u1)
    create(:vote, votable: @q1, voter: @u2)

    @q2.reload
    @q1.reload

    Post.reindex

    # test descending
    filter_params = {:group_id => @g1.id}
    sort_params = {:votes_count => :desc}
    results = Post.retrieve(filter_params: filter_params,
                              sort_params: sort_params,
                              search_query: 'alah',
                              misspelling: 0,
                              limit: 1)

    assert_equal results.size, 1
    assert_equal results[0].id.to_i, @q2.id

    # Now get the next page
    # filter_params[:votes_count] = @q2.votes_count
    results = Post.retrieve(filter_params: filter_params,
                              sort_params: sort_params,
                              search_query: 'alah',
                              misspelling: 0,
                              limit: 1,
                              offset: 1)

    assert_equal results.size, 1
    assert_equal results[0].id.to_i, @q1.id

    # test ascending
    filter_params = {:group_id => @g1.id}
    sort_params = {:votes_count => :asc}

    results = Post.retrieve(filter_params: filter_params,
                              sort_params: sort_params,
                              search_query: 'alah',
                              misspelling: 0,
                              limit: 1)

    assert_equal results.size, 1
    assert_equal results[0].id.to_i, @q1.id

    # Now get the next page
    filter_params[:votes_count] = @q1.votes_count
    results = Post.retrieve(filter_params: filter_params,
                              sort_params: sort_params,
                              search_query: 'alah',
                              limit: 1,
                              offset: 1)

    assert_equal results.size, 1
    assert_equal results[0].id.to_i, @q2.id
  end

  test "pagination with answer and secondary created at" do
    retrieve_setup
    @g1.add_member @user
    # Both q_old and q_new have one answer each, q_most_voted has two
    q_most_answered = create(:question, group: @g1, user: @user,
                          title: 'edcast', created_at: 2.days.ago)
    q_old = create(:question, group: @g1, user: @user, title: 'edcast', created_at: 2.days.ago)
    q_new = create(:question, group: @g1, user: @user, title: 'edcast', created_at: 1.days.ago)

    create(:answer, commentable: q_most_answered, user: @user)
    create(:answer, commentable: q_most_answered, user: @u1)
    create(:answer, commentable: q_old, user: @user)
    create(:answer, commentable: q_new, user: @user)

    q_most_answered.reload
    q_old.reload
    q_new.reload
    Post.reindex

    filter_params = {:group_id => @g1.id}
    sort_params = {:comments_count => :desc, :created_at => :desc}

    results = Post.retrieve(filter_params: filter_params,
                              sort_params: sort_params,
                              search_query: 'edcast',
                              limit: 2)

    assert_equal results.size, 2
    assert_equal results[0].id.to_i, q_most_answered.id
    assert_equal results[1].id.to_i, q_new.id

    # Get next page
    #filter_params[:comments_count] = q_new.comments_count
    #filter_params[:created_at] = q_new.created_at

    results = Post.retrieve(filter_params: filter_params,
                              sort_params: sort_params,
                              search_query: 'edcast',
                              limit: 2,
                              offset: 2)

    assert_equal results.size, 1
    assert_equal results[0].id.to_i, q_old.id
  end

  test "pagination with votes and secondary created at" do
    retrieve_setup

    # Both q_old and q_new have one vote each, q_most_voted has two
    q_most_voted = create(:question, group: @g1, user: @u2,
                          title: 'edcast', created_at: 2.days.ago)
    q_old = create(:question, group: @g1, user: @u2, title: 'edcast', created_at: 2.days.ago)
    q_new = create(:question, group: @g1, user: @u2, title: 'edcast', created_at: 1.days.ago)

    create(:vote, votable: q_most_voted, voter: @user)
    create(:vote, votable: q_most_voted, voter: @u1)
    create(:vote, votable: q_old, voter: @u1)
    create(:vote, votable: q_new, voter: @u1)

    q_most_voted.reload
    q_old.reload
    q_new.reload

    Post.reindex

    filter_params = {:group_id => @g1.id}
    sort_params = {:votes_count => :desc, :created_at => :desc}

    results = Post.retrieve(filter_params: filter_params,
                              sort_params: sort_params,
                              search_query: 'edcast',
                              limit: 2)

    assert_equal results.size, 2
    assert_equal results[0].id.to_i, q_most_voted.id
    assert_equal results[1].id.to_i, q_new.id

    # Get next page
    results = Post.retrieve(filter_params: filter_params,
                              sort_params: sort_params,
                              search_query: 'edcast',
                              limit: 2,
                              offset: 2)

    assert_equal results.size, 1
    assert_equal results[0].id.to_i, q_old.id
  end

  test "should filter by group, user" do
    retrieve_setup

    @group.add_member @u1
    @group.add_member @u2
    # filter by group
    filter_params = {:group_id => @g1.id}
    results = Post.retrieve(filter_params: filter_params)
    assert_equal results.count, 2
    assert [@q1.id, @q2.id].include? results[0].id.to_i
    assert [@q1.id, @q2.id].include? results[1].id.to_i

    # filter by user
    filter_params = {:user_id => @u1.id}
    results = Post.retrieve(filter_params: filter_params)
    assert_equal results.count, 1
    assert_equal results[0].id.to_i, @q1.id

    # filter by multiple
    Post.reindex

    filter_params = {:group_id => @g1.id, :user_id => @u1.id}
    results = Post.retrieve(filter_params: filter_params)
    assert_equal results.count, 1
    assert_equal results[0].id.to_i, @q1.id

    # empty result
    new_user = create(:user)
    filter_params = {:group_id => @g1.id, :user_id => new_user.id}
    results = Post.retrieve(filter_params: filter_params)
    assert_equal results.count, 0
  end

  test "should filter by tags" do
    retrieve_setup
    new_group = create(:resource_group)
    new_group.add_member @u1
    another_question = create(:question, group: new_group, user: @u1, message: '#q1',
                              created_at: Time.now)

    Post.reindex

    # simple tag search
    filter_params = {:tag_names => 'q1'}
    results = Post.retrieve(filter_params: filter_params)

    assert_equal results.count, 2
    assert [@q1.id, another_question.id].include?(results[0].id.to_i)
    assert [@q1.id, another_question.id].include?(results[1].id.to_i)

    # tag search with multiple filters
    filter_params = {:tag_names => 'q1', :group_id => @g1.id}
    results = Post.retrieve(filter_params: filter_params)
    assert_equal results.count, 1
    assert_equal results[0].id.to_i, @q1.id

    # tag search, empty result
    filter_params = {:tag_names => 'q1', :group_id => @group.id}
    results = Post.retrieve(filter_params: filter_params)
    assert_equal results.count, 0
  end

  test "should filter by reported boolean" do
    create_default_org
    retrieve_setup

    # two reports for q1, one for q2, none for q3
    reporter = create(:user)
    @g1.add_member reporter

    reporter2 = create(:user)
    @g1.add_member reporter2

    create(:report, reporter: reporter, reportable: @q2)
    @q1.reload
    create(:report, reporter: reporter, reportable: @q1)
    @q2.reload
    create(:report, reporter: reporter2, reportable: @q2)

    Post.reindex

    # get reported answers only
    filter_params = {:hidden => true}
    results = Post.retrieve(filter_params: filter_params)
    assert_equal results.count, 1
    assert_equal results[0].id.to_i, @q2.id

    # get reported with multiple filters
    filter_params = {:hidden => true, :user_id => @u2.id}
    results = Post.retrieve(filter_params: filter_params)
    assert_equal results.count, 1
    assert_equal results[0].id.to_i, @q2.id

    # get reported, empty result
    filter_params = {:hidden => true, :group_id => @group.id}
    results = Post.retrieve(filter_params: filter_params,
                            sort_params: {created_at: :desc})
    assert_equal results.count, 0

    # get question that has a reported answer
    create(:report, reporter: reporter, reportable: @a1)
    create(:report, reporter: reporter2, reportable: @a1)
    Post.reindex
    filter_params = {:reported => true}
    results = Post.retrieve(filter_params: filter_params)
    assert_equal results.count, 2
    assert [@q1.id, @q2.id].include?(results[0].id.to_i)
    assert [@q1.id, @q2.id].include?(results[1].id.to_i)
  end

  test "should search title, message and answers" do
    retrieve_setup

    # test sorting by comments and group filter
    filter_params = {:group_id => @g1.id}
    sort_params = {:comments_count => :desc}
    results = Post.retrieve(filter_params: filter_params,
                              sort_params: sort_params,
                              search_query: 'alah')
    assert_equal results.count, 2
    assert_equal results[0].id.to_i, @q1.id
    assert_equal results[1].id.to_i, @q2.id

    # test sorting by votes
    sort_params = {:votes_count => :desc}
    results = Post.retrieve(filter_params: filter_params,
                              sort_params: sort_params,
                              search_query: 'alah')
    assert_equal results.count, 2
    assert_equal results[0].id.to_i, @q2.id
    assert_equal results[1].id.to_i, @q1.id

    # test user filter and multiple filters
    filter_params = {:group_id => @g1.id, :user_id => @u1.id}
    results = Post.retrieve(filter_params: filter_params,
                              sort_params: sort_params,
                              search_query: 'alah')
    assert_equal results.count, 1
    assert_equal results[0].id.to_i, @q1.id

    # test empty result set
    results = Post.retrieve(filter_params: filter_params,
                                sort_params: sort_params,
                                search_query: 'claha')
    assert_equal results.count, 1

    # test misspelling distance
    results = Post.retrieve(filter_params: filter_params,
                              sort_params: sort_params,
                              search_query: 'clah')
    assert_equal results.count, 1

    # test title search
    filter_params = {:group_id => @g1.id}
    sort_params = {:comments_count => :desc}
    results = Post.retrieve(filter_params: filter_params,
                              sort_params: sort_params,
                              search_query: 'blah')
    assert_equal results.count, 2
    assert_equal results[0].id.to_i, @q1.id

    # test answer message search
    results = Post.retrieve(filter_params: filter_params,
                              sort_params: sort_params,
                              search_query: 'xlah')
    assert_equal results.count, 2
    assert_equal results[0].id.to_i, @q1.id

    # no params passed. Log error and return nil
    results = Post.retrieve
    assert_nil results
  end


  test "should contain answer tags" do
    create_default_org
    user = create(:user)
    group = create(:group)

    group.add_member user
    q = create(:question, group: group, user: user)
    create(:answer, commentable: q, user: user, message: "aa #a")
    create(:answer, commentable: q, user: user, message: "bc #b #c")
    create(:answer, commentable: q, user: user, message: "de #d #e")
    create(:answer, commentable: q, user: user, message: "fg #f #g")

    Post.reindex

    assert_equal q.search_data[:tag_names], ['a', 'b', 'c', 'd', 'e', 'f', 'g']
  end

  test "should search for username" do
    user = create(:user)
    group = create(:group)

    group.add_member user

    q = create(:post, group: group, user: user)

    Post.reindex
    results = Post.retrieve(filter_params: {group_id: group.id},
                            sort_params: {created_at: :desc},
                            search_query: user.name)

    assert_equal results.first.id.to_i, q.id

    results = Post.retrieve(filter_params: {group_id: group.id},
                            sort_params: {created_at: :desc},
                            search_query: user.first_name)
    assert_equal results.first.id.to_i, q.id

    otheruser = create(:user, first_name: "other", last_name: "user")
    group.add_member otheruser
    create(:comment, commentable: q, user: otheruser)
    Post.reindex
    results = Post.retrieve(filter_params: {group_id: group.id},
                            sort_params: {created_at: :desc},
                            search_query: "other")

    assert_equal results.first.id.to_i, q.id
  end

  test 'should show correct student and faculty answer counts' do
    user = create(:user)
    group = create(:group)
    faculty = create(:user)
    group.add_member(user)
    group.add_admin(faculty)

    p = create(:post, user: user, group: group)
    Post.reindex
    result = Post.search_by_id(p.id, group.id).first
    assert_equal result.student_answer_count, 0
    assert_equal result.faculty_answer_count, 0

    sc1 = create(:comment, commentable: p, user: user)
    sc2 = create(:comment, commentable: p, user: user)
    fc1 = create(:comment, commentable: p, user: faculty)

    Post.reindex

    result = Post.search_by_id(p.id, group.id).first
    assert_equal result.student_answer_count, 2
    assert_equal result.faculty_answer_count, 1
  end

  test 'should delete post in index' do
    create_default_org
    u = create(:user)
    g = create(:group)
    g.add_member u
    p = create(:post, user: u, group: g)
    id = p.id
    c = create(:comment, commentable: p, user: u)

    Post.reindex

    result = Post.search_by_id(id, g.id).first
    assert_equal id, result.id.to_i

    p.destroy

    Post.reindex
    result = Post.search_by_id(id, g.id).first
    assert_nil result
  end

  test 'should retrieve channel posts' do
    ch = create(:channel, organization: @org)
    u = create(:user, organization: @org)
    create(:follow, followable: ch, user: u)
    p = create(:post, channel: ch, user: u)

    p.reindex
    Post.reindex
    p_indx = Post.retrieve_by_ids([p.id])
    assert_equal p_indx.results.first.id, p.id
  end
end

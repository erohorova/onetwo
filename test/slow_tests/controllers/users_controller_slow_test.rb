require 'test_helper'

class UsersControllerSlowTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  self.use_transactional_fixtures = false

  setup do
    skip #similar tests are written in several other places to get stats
    @controller = UsersController.new
    WebMock.disable!
    if Post.searchkick_index.exists?
      Post.searchkick_index.delete
    end

    Post.reindex

    @user = create(:user)
    @another_user = create(:user)
    @group = create(:group)
    @another_group = create(:group)
    @group.add_member @user
    @group.add_member @another_user
    @another_group.add_member @user
    @another_group.add_member @another_user
    @question1 = create(:question, group: @group, user: @user)
    @question2 = create(:question, group: @group, user: @user)
    @question3 = create(:question, group: @group, user: @another_user)
    @question4 = create(:question, group: @group, user: @another_user)
    @question5 = create(:question, group: @group, user: @another_user)
    @question3.answers << create(:answer, commentable: @question3, user: @user)
    @question4.answers << create(:answer, commentable: @question4, user: @user)
    @question5.answers << create(:answer, commentable: @question5, user: @user)
    Post.searchkick_index.refresh
    sign_in @user
  end

  teardown do
    WebMock.enable!
  end

  test ':api get stats' do
    get :stats, group_id: @group.id, id: @user.id, format: :json
    response_object = get_json_response response
    assert_equal 2, response_object['posted_count']
    assert_equal 3, response_object['answered_count']
  end

  test 'should not zero count stats' do
    get :stats, group_id: @another_group.id, id: @user.id, format: :json
    response_object = get_json_response response
    assert_equal 0, response_object['posted_count']
    assert_equal 0, response_object['answered_count']
  end
end
require 'test_helper'

class PostsSearchControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  self.use_transactional_fixtures = false

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'

    WebMock.disable!
    if Post.searchkick_index.exists?
      Post.searchkick_index.delete
    end

    PostRead.create_index! force: true

    Post.reindex

    UserContentsQueue.create_index! force: true
    UserContentsQueue.gateway.refresh_index!

    @org = create(:organization)
    set_host @org
    User.any_instance.stubs(:reindex_async).returns(true)
    @controller = PostsController.new

    @user = create(:user, organization: @org)
    @faculty = create(:user, email: 'slowfaculty@stanford.edu', organization: @org)
    @client = create(:client, organization: @org)
    @group = create(:resource_group, organization: @org, client: @client)
    @group.add_admin @faculty
    @group.add_member @user
  end

  teardown do
    User.any_instance.unstub(:reindex_async)
    WebMock.enable!
  end

  def api_setup
    Post.any_instance.unstub :p_read_by_creator
    @tag = create(:tag)
    @resource = create(:resource)
    @question = create(:question, group: @group, user: @user)
    @question.tags << @tag
    @question.resource = @resource
    @question.save
    @question.reload
    @voter = create(:user, organization: @org)
    @group.add_member @voter
    @vote = create(:vote, votable: @question, voter: @voter)
    @question.answers << create(:answer, commentable: @question, user: @user)
    @question.answers << create(:answer, commentable: @question, user: @faculty)
    @question.reload
    @expected_question = {'id' => @question.id,
                          'type' => 'Question',
                          'title' => @question.title,
                          'message' => @question.message,
                          'group_id'=> @question.group_id,
                          'user' => @user.as_json(only:[:id,:first_name,:last_name]),
                          'resource' => @resource.as_json(only:[:id,:url,:title,:description,:image_url,:video_url,:type, :embed_html,
                                                                :site_name]).merge({"created_at" => @resource.created_at.strftime("%Y-%m-%dT%H:%M:%S.000Z")}),
                          'tags' => [@tag.as_json(only:[:id,:name])],
                          'votes_count' => @question.votes_count,
                          'faculty_answer_count' => 1,
                          'student_answer_count' => 1,
                          'reporting_disabled' => false,
                          'reporting_disabled_message' => '',
                          'is_staff' => false,
                          'creator_label' => @question.creator_label,
                          'is_faculty_approved' => false,
                          'created_at' => @question.created_at.strftime("%Y-%m-%dT%H:%M:%S.%LZ"),
                          'updated_at' => @question.updated_at.strftime("%Y-%m-%dT%H:%M:%S.%LZ"),
                          'comments_count' => @question.comments_count,
                          'hidden' => false,
                          'context_id' =>nil,
                          'context_label' =>nil,
                          'read' => true,
                          'up_voted' => false,
                          'anonymous' => false,
                          'pinned' => false,
                          'is_mobile' => false,
                          'following' => true,
                          'file_resources' => [],
                          'post_url'=> Post.url(@question.attributes)
                         }

    @expected_question['user']['picture'] = @user.photo
    @expected_question['user']['name'] = @user.name
    @expected_question['user']['handle'] = @user.handle
    @expected_question['user']['bio'] = @user.bio
    @expected_question['user']['roles'] = ['']
    @expected_question['user']['self'] = true
    @expected_question['user']['following'] = false
    @expected_question['user']['followers_count'] = nil
    @expected_question['user']['full_name'] = @user.full_name
    @expected_question['user']['is_suspended'] = false
    @expected_question['user']['status'] = nil
    score_entry = @user.score(@group)
    @expected_question['user']['score'] = score_entry[:score]
    @expected_question['user']['stars'] = score_entry[:stars]

    Post.reindex
    Post.searchkick_index.refresh
    PostRead.gateway.refresh_index!
  end

  test ":api get questions for an existing course" do
    api_setup
    sign_in @user
    get :index, {:group_id => @group.id, :format => :json}
    assert_response :success
    questions = assigns(:posts)
    assert_not_nil questions
    assert_equal questions.count, 1
    assert_equal questions[0]['id'].to_i, @question.id
    assert_equal questions[0]['id'].to_i, @question.id

    # test json response
    response_object = get_json_response response
    assert_equal response_object.count, 1
    assert_equal @expected_question, response_object[0]
    sign_out @user
  end

  class ChannelPostsTest < ActionController::TestCase
    include Devise::Test::ControllerHelpers

    self.use_transactional_fixtures = false

    setup do
      skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'

      WebMock.disable!
      @org = create(:organization)
      @controller = PostsController.new
      @user = create(:user, organization: @org)
      @channel = create(:channel, organization: @org)
      create(:follow, followable: @channel, user: @user)

      # some group posts
      g, u = create_gu(user_opts: {organization_id: @org.id})
      group_posts = create_list(:post, 3, group: g, user: u)

      if Post.searchkick_index.exists?
        Post.searchkick_index.delete
      end

      sign_in @user
    end

    teardown do
      WebMock.enable!
    end

    test ':api get posts in a channel forum' do
      posts = create_list(:post, 3, channel: @channel, user: @user)
      Post.reindex
      Post.searchkick_index.refresh
      get :index, channel_id: @channel.id, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_same_elements posts.map(&:id), resp.map{|r| r['id']}
    end

    test ':api show for a channel post' do
      posts = create_list(:post, 3, channel: @channel, user: @user)
      Post.reindex
      Post.searchkick_index.refresh
      get :show, id: posts.first.id, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal posts.first.id, resp['id']
    end
  end

  test 'api to get pinned posts only' do
    g = create(:group, organization: @org)
    u, au = create_list(:user, 2, organization: @org)
    g.add_member(u)
    g.add_admin(au)
    q = create(:post, user: u, group: g, pinned: false)
    p = create(:post, user: u, group: g, pinned: true)
    a = create(:announcement, user: au, group: g, pinned: false)
    ap = create(:announcement, user: au, group: g, pinned: true, created_at: Time.now.utc - 5.hours)

    Post.reindex
    Post.searchkick_index.refresh

    sign_in u
    get :index, group_id: g.id, pinned: true, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal [p.id, ap.id], resp.map{|r| r['id']}

    # all posts
    get :index, group_id: g.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal 4, resp.count
    assert_same_elements [q,p,a,ap].map(&:id), resp.map{|r| r['id']}
  end

  test 'index action further tests' do
    api_setup
    u = create(:user, organization: @org)
    @group.add_member u
    q = create(:question, user: u, group: @group, created_at: 1.day.ago)
    create(:approval, approvable: q, approver: @faculty)
    create(:vote, voter: @user, votable: q)

    Post.reindex
    Post.searchkick_index.refresh
    sign_in @user
    get :index, group_id: @group.id, format: :json

    assert_response :success
    resp = get_json_response response

    assert_equal resp.size, 2
    assert resp[1]['up_voted']
    assert resp[1]['is_faculty_approved']
    sign_out @user
  end

  def multiple_question_setup
    @group1 = create(:resource_group, organization: @org, client: @client)
    @group1.add_member @user
    @question1 = create(:question, group: @group1, user: @user,
                        created_at: 1.days.ago)
    @question2 = create(:question, group: @group1, user: @user,
                        created_at: 2.days.ago)
    @question3 = create(:question, group: @group1, user: @user,
                        created_at: 3.days.ago)
    voter = create(:user)
    @group1.add_member voter
    voter2 = create(:user)
    @group1.add_member voter2
    create(:vote, votable: @question1, voter: voter)
    create(:vote, votable: @question1, voter: voter2)
    create(:vote, votable: @question2, voter: voter)
    @question3.followers << create_list(:user, 3)
    @question2.followers << create_list(:user, 2)
    @question1.followers << create_list(:user, 1)
    @question1.reload
    @question2.reload
    @question3.reload
    Post.reindex
    Post.searchkick_index.refresh
  end

  test 'order questions by date, descending (defaults)' do
    sign_in @user
    multiple_question_setup
    get :index, {:group_id => @group1.id, :format => :json}
    questions = assigns(:questions)
    resp = get_json_response response
    assert_equal resp.first['id'].to_i, @question1.id
    assert_equal resp.size, 3
    sign_out @user
  end

  test 'sort by update should work' do
    sign_in @user
    q1 = create(:question, user: @user, group: @group,
                pinned: true, created_at: 2.days.ago, title: 'searchtext')
    q2 = create(:question, user: @user, group: @group,
                pinned: false, created_at: 3.days.ago, title: 'searchtext')
    answer_q2 = create(:answer, user: @user, commentable: q2,
                       created_at: Time.now)
    a1 = create(:announcement, user: @faculty, group: @group,
                pinned: false, created_at: 1.days.ago, title: 'searchtext')
    q4 = create(:question, user: @user, group: @group,
                pinned: true, created_at: Time.now, title: 'title')

    Post.reindex
    Post.searchkick_index.refresh

    get :index, group_id: @group.id, search: 'searchtext',
      sort: 'updates', order: 'desc', page: 0, format: :json

    assert_response :success

    resp = get_json_response(response)

    assert_equal((resp.map{|r| r['id'].to_i}), [q2.id, a1.id, q1.id])
  end
  test 'sort by date should automatically sort by pinned first' do
    sign_in @user
    q1 = create(:question, user: @user, group: @group,
                pinned: true, created_at: 2.days.ago, title: 'searchtext')
    q2 = create(:question, user: @user, group: @group,
                pinned: false, created_at: Time.now, title: 'searchtext')
    a1 = create(:announcement, user: @faculty, group: @group,
                pinned: false, created_at: 1.days.ago, title: 'searchtext')
    q4 = create(:question, user: @user, group: @group,
                pinned: true, created_at: Time.now, title: 'title')

    Post.reindex
    Post.searchkick_index.refresh

    get :index, group_id: @group.id, search: 'searchtext',
      sort: 'date', order: 'desc', page: 0, format: :json

    assert_response :success

    resp = get_json_response(response)

    assert_equal((resp.map{|r| r['id'].to_i}), [q1.id, q2.id, a1.id])
  end

  test ':api get questions ordered by votes (default descending)' do
    sign_in @user
    multiple_question_setup
    get :index, {group_id: @group1.id, format: :json, sort: 'votes'}
    resp = get_json_response response

    assert_equal resp.first['id'].to_i, @question1.id
    assert_equal resp[2]['id'].to_i, @question3.id
    sign_out @user
  end

  test ':api get questions ordered by votes ascending' do
    sign_in @user
    multiple_question_setup
    get :index, {group_id: @group1.id, format: :json, sort: 'votes', order: 'asc'}
    resp = get_json_response response
    assert_equal resp.first['id'].to_i, @question3.id
    assert_equal resp[2]['id'].to_i, @question1.id
    sign_out @user
  end


  test ':api get questions ordered by followers count descending' do
    sign_in @user
    multiple_question_setup
    get :index, {group_id: @group1.id, format: :json, sort: 'followed'}
    resp = get_json_response response
    assert_same_ids [@question3, @question2, @question1], resp, ordered: true
    sign_out @user
  end

  test 'get questions sorted(default is by date desc)' do
    sign_in @user
    multiple_question_setup
    @question1.created_at = 1.days.ago
    @question2.created_at = 2.days.ago
    @question3.created_at = 3.days.ago
    @question1.save
    @question2.save
    @question3.save
    get :index, {group_id: @group1.id, format: :json}
    resp = get_json_response response
    assert_equal resp.count, 3
    assert_equal resp[0]['id'].to_i, @question1.id
    assert_equal resp[1]['id'].to_i, @question2.id
    assert_equal resp[2]['id'].to_i, @question3.id
    sign_out @user
  end

  test 'vote and comment sort should not consider pinned' do
    sign_in @user
    multiple_question_setup
    @question3.pinned = true
    @question3.save

    Post.reindex
    Post.searchkick_index.refresh

    get :index, group_id: @group1.id, sort: 'votes', order: 'desc',
      format: :json

    resp = get_json_response response
    assert_equal((resp.map{|r| r['id'].to_i}), [@question1.id, @question2.id, @question3.id])

    create_list(:answer, 5, commentable: @question2, user: @user)
    create_list(:answer, 2, commentable: @question1, user: @user)

    Post.reindex
    Post.searchkick_index.refresh

    get :index, group_id: @group1.id, sort: 'responses', order: 'desc',
      format: :json
    resp = get_json_response(response)
    assert_equal((resp.map{|r| r['id'].to_i}), [@question2.id, @question1.id, @question3.id])
  end

  test 'get questions ordered by date' do
    sign_in @user
    multiple_question_setup
    @question1.created_at = Time.now - 1.hour
    @question1.save!
    @question2.created_at = Time.now - 1.day
    @question2.save!
    @question3.created_at = Time.now
    @question3.save!

    Post.reindex
    Post.searchkick_index.refresh

    get :index, {group_id: @group1.id, format: :json, sort: 'date'}
    resp = get_json_response response
    assert_equal resp.first['id'].to_i, @question3.id
    assert_equal resp[2]['id'].to_i, @question2.id

    get :index, {group_id: @group1.id, format: :json, sort: 'date', order: 'asc'}
    resp = get_json_response response
    assert_equal resp.first['id'].to_i, @question2.id
    assert_equal resp[2]['id'].to_i, @question3.id
    sign_out @user
  end

  def multiple_answer_setup
    multiple_question_setup
    create_list(:answer, 5, commentable: @question2, user: @user)
    create_list(:answer, 2, commentable: @question1, user: @user)
    @question1.reload
    @question2.reload
    @question3.reload
    Post.reindex
    Post.searchkick_index.refresh
  end

  test ':api pagination by created at page1' do
    multiple_question_setup

    @question1.created_at = 1.days.ago
    @question2.created_at = 2.days.ago
    @question3.created_at = 3.days.ago
    @question1.save!
    @question2.save!
    @question3.save!
    Post.reindex
    Post.searchkick_index.refresh

    sign_in @user

    get :index, {group_id: @group1.id, format: :json,
                 sort: 'date', order: 'desc', limit: 2}
    resp = get_json_response response
    assert_equal resp.count, 2
    assert_equal resp[0]['id'].to_i, @question1.id
    assert_equal resp[1]['id'].to_i, @question2.id
  end

  test ":api pagination by created at page2" do
    multiple_question_setup
    @question1.created_at = 1.days.ago
    @question2.created_at = 2.days.ago
    @question3.created_at = 3.days.ago
    @question1.save!
    @question2.save!
    @question3.save!
    Post.reindex
    Post.searchkick_index.refresh
    sign_in @user
    # Now paginate
    get :index, {group_id: @group1.id, format: :json,
                 sort: 'date', order: 'desc', page: 2, limit: 1}
    resp = get_json_response response

    assert_equal resp.count, 1
    assert_equal resp[0]['id'].to_i, @question3.id

    sign_out @user
  end

  test ':api pagination by vote count page1' do
    multiple_question_setup
    @question1.created_at = 1.days.ago
    @question2.created_at = 2.days.ago
    @question3.created_at = 3.days.ago
    # question 2 and 1 have 2 votes
    voter = create(:user)
    @group1.add_member voter
    @question1.save!
    @question2.save!
    @question3.save!
    @question1.reload
    @question2.reload
    @question3.reload
    create(:vote, votable: @question2, voter: voter)
    @question1.reload
    Post.reindex
    Post.searchkick_index.refresh
    sign_in @user

    get :index, {group_id: @group1.id, format: :json,
                 sort: 'votes', order: 'asc', limit: 2}
    resp = get_json_response response
    assert_equal resp.count, 2
    assert_equal resp[0]['id'].to_i, @question3.id
    assert_equal resp[1]['id'].to_i, @question1.id
  end

  test ":api pagination by vote count page2" do
    multiple_question_setup
    @question1.created_at = 1.days.ago
    @question2.created_at = 2.days.ago
    @question3.created_at = 3.days.ago
    voter = create(:user)
    @group1.add_member voter
    # question 2 and 1 have 2 votes
    @question1.save!
    @question2.save!
    @question3.save!
    @question1.reload
    @question2.reload
    @question3.reload
    create(:vote, votable: @question2, voter: voter)
    Post.reindex
    Post.searchkick_index.refresh
    sign_in @user

    # Now paginate
    get :index, {group_id: @group1.id, format: :json,
                 sort: 'votes', order: 'asc',
                 page: 2,
                 limit: 1}
    resp = get_json_response response

    assert_equal resp.count, 1
    assert_equal resp[0]['id'].to_i, @question2.id

    sign_out @user
  end

  test ':api pagination by response count page1' do
    multiple_answer_setup
    @question1.created_at = 3.days.ago
    @question2.created_at = 2.days.ago
    @question3.created_at = 1.days.ago

    @question1.save!
    @question2.save!
    @question3.save!

    @question1.reload
    @question2.reload
    @question3.reload

    # question 3 and 1 have 2 votes
    create_list(:answer, 2, commentable: @question3, user: @user)

    Post.reindex
    Post.searchkick_index.refresh
    sign_in @user

    get :index, {group_id: @group1.id, format: :json,
                 sort: 'responses', order: 'desc', limit: 2}
    resp = get_json_response response
    assert_equal resp.count, 2
    assert_equal resp[0]['id'].to_i, @question2.id
    assert_equal resp[1]['id'].to_i, @question3.id
  end

  test ":api pagination by response count page2" do
    multiple_answer_setup
    @question1.created_at = 3.days.ago
    @question2.created_at = 2.days.ago
    @question3.created_at = 1.days.ago

    @question1.save!
    @question2.save!
    @question3.save!
    # question 3 and 1 have 2 votes
    create_list(:answer, 2, commentable: @question3, user: @user)

    @question1.reload
    @question2.reload
    @question3.reload
    Post.reindex
    Post.searchkick_index.refresh
    sign_in @user

    # Now paginate
    get :index, {group_id: @group1.id, format: :json,
                 sort: 'responses', order: 'desc',
                 page: 2,
                 limit: 1}

    resp = get_json_response response

    assert_equal resp.count, 1
    assert_equal resp[0]['id'].to_i, @question1.id

    sign_out @user
  end

  test 'get questions ordered by responses desc' do
    sign_in @user
    multiple_answer_setup
    get :index, {group_id: @group1.id, format: :json, sort: 'responses', order: 'desc'}
    resp = get_json_response response
    assert_equal resp.first['id'].to_i, @question2.id
    assert_equal resp[2]['id'].to_i, @question3.id
    sign_out @user
  end

  test ':api get questions ordered by responses asc' do
    sign_in @user
    multiple_answer_setup
    get :index, {group_id: @group1.id, format: :json, sort: 'responses', order: 'asc'}
    resp = get_json_response response
    assert_equal resp.first['id'].to_i, @question3.id
    assert_equal resp[2]['id'].to_i, @question2.id
    sign_out @user
  end

  test ":api filter by tags" do
    sign_in @user
    q1 = create(:question, group: @group, user: @user, message: '#hw1', created_at: 2.days.ago)
    q2 = create(:question, group: @group, user: @user, message: '#hw1', created_at: 1.days.ago)

    q3 = create(:question, group: @group, user: @user, message: '#hw2', created_at: 3.days.ago)
    Post.reindex
    Post.searchkick_index.refresh
    get :index, {group_id: @group.id, tag: 'hw1', format: :json}
    assigned_questions = assigns(:posts)
    assert_equal assigned_questions.size, 2
    assert_equal assigned_questions[0].id.to_i, q2.id
    assert_equal assigned_questions[1].id.to_i, q1.id
    sign_out @user
  end

  test 'filter by author_id' do
    another_user = create(:user)
    @group.add_member another_user

    sign_in @user
    q1 = create(:question, group: @group, user: another_user, message: '#hw1', created_at: 2.days.ago)
    q2 = create(:question, group: @group, user: @user, message: '#hw1', created_at: 1.days.ago)

    q3 = create(:question, group: @group, user: another_user, message: '#hw2', created_at: 3.days.ago)
    Post.reindex
    Post.searchkick_index.refresh
    get :index, {group_id: @group.id, author_id: @user.id, format: :json}
    assigned_questions = assigns(:posts)
    assert_equal 1, assigned_questions.size
    assert_equal q2.id, assigned_questions[0].id.to_i
    sign_out @user
  end

  test 'filter by commenter_id' do
    another_user = create(:user)
    @group.add_member another_user

    sign_in @user
    q1 = create(:question, group: @group, user: another_user, message: '#hw1', created_at: 2.days.ago)
    q2 = create(:question, group: @group, user: @user, message: '#hw1', created_at: 1.days.ago)

    q3 = create(:question, group: @group, user: another_user, message: '#hw2', created_at: 3.days.ago)

    create(:answer, commentable: q3, user: another_user)
    Post.reindex
    Post.searchkick_index.refresh
    get :index, {group_id: @group.id, commenter_id: another_user.id, format: :json}
    assigned_questions = assigns(:posts)
    assert_equal 1, assigned_questions.size
    assert_equal q3.id, assigned_questions[0].id.to_i
    sign_out @user

  end

  test ':api filter by reported' do
    create_default_org
    sign_in @user
    user2 = create(:user)

    gr = create(:resource_group)
    gr.add_member user2
    gr.add_member @user

    q1 = create(:question, group: gr, user: user2,
                message: '#hw1', created_at: 2.days.ago)
    q2 = create(:question, group: gr, user: user2,
                message: '#hw1', created_at: 1.days.ago)

    q3 = create(:question, group: gr, user: user2,
                message: '#hw2', created_at: 3.days.ago)

    a2 = create(:answer, commentable: q2, user: @user)

    r1 = create(:user)
    r2 = create(:user)

    gr.add_member r1
    gr.add_member r2

    create(:report, reporter: r1, reportable: q1)
    create(:report, reporter: r2, reportable: q1)
    create(:report, reporter: r1, reportable: a2)
    create(:report, reporter: r2, reportable: a2)
    create(:report, reporter: r1, reportable: q3)

    Post.reindex
    Post.searchkick_index.refresh

    get :index, {group_id: gr.id,
                 reported: true,
                 format: :json}

    resp = get_json_response response

    assert_equal resp.count, 2
    assert_equal resp[0]['id'].to_i, q2.id
    assert_equal resp[1]['id'].to_i, q1.id
  end

  test ":api simple search over questions" do
    sign_in @user
    q1 = create(:question,
                user: @user,
                group: @group,
                title: 'Bohemian Rhapsody',
                message: 'Is this the real life?')
    q2 = create(:question,
                user: @user,
                group: @group,
                title: 'Bohemian Rhapsody',
                message: 'Is this just fantasy?')
    a1 = create(:answer,
                commentable: q1,
                user: @user,
                message: 'Sentient being alert!')
    Post.reindex
    Post.searchkick_index.refresh
    # search over title, ensure sort works too
    get :index, {group_id: @group.id, format: :json,
                 search: 'Bohemian Rhapsody', sort: 'responses'}
    assigned_questions = assigns(:posts)
    assert_equal assigned_questions.size, 2
    assert_equal assigned_questions[0].id.to_i, q1.id
    assert_equal assigned_questions[1].id.to_i, q2.id
    assert_response :success

    # search over message
    get :index, {group_id: @group.id, format: :json, search: 'fantasy'}
    assigned_questions = assigns(:posts)
    assert_equal assigned_questions.size, 1
    assert q2.id, assigned_questions[0].id
    assert_response :success
    sign_out @user
  end

  test 'get announcements or questions only' do
    sign_in @user
    a = create(:announcement, user: @faculty, group: @group)
    q = create(:question, user: @user, group: @group)

    Post.reindex
    Post.searchkick_index.refresh

    get :index, {group_id: @group.id, type: 'Announcement', format: :json, sort: 'date'}
    assert_response :success
    resp = get_json_response response
    assert_equal resp.size, 1
    assert_equal resp[0]['id'].to_i, a.id

    get :index, group_id: @group.id, type: 'Question', format: :json, sort: 'date'
    assert_response :success
    resp = get_json_response response
    assert_equal resp.size, 1
    assert_equal resp[0]['id'].to_i, q.id
    sign_out @user
  end

  test 'search over announcements too' do
    sign_in @user
    q = create(:question, user: @user, group: @group, message: 'searchtext')
    a = create(:announcement, user: @faculty, group: @group, message: 'searchtext')
    Post.reindex
    Post.searchkick_index.refresh

    get :index, group_id: @group.id, search: 'searchtext', format: :json, sort: 'date'
    assert_response :success
    resp = get_json_response response
    assert_equal resp.size, 2
    assert [q.id, a.id].include?(resp[0]['id'].to_i)
    assert [q.id, a.id].include?(resp[1]['id'].to_i)
    sign_out @user
  end

  test "search over answers too" do
    sign_in @user
    q1 = create(:question,
                user: @user,
                group: @group,
                title: 'What is quantum mechanics?',
                message: 'Can someone answer this')
    a1 = create(:answer,
                user: @user,
                commentable: q1,
                message: 'It is magic'
               )
    q2 = create(:question,
                user: @user,
                group: @group,
                title: 'title',
                message: 'message')
    Post.reindex
    Post.searchkick_index.refresh
    get :index, {group_id: @group.id, format: :json, search: 'magic'}
    assigned_questions = assigns(:posts)

    assert_equal assigned_questions.size, 1
    assert_equal q1.id, assigned_questions[0].id.to_i
    assert_response :success
    sign_out @user
  end

  test "should not show hidden questions" do
    create_default_org
    multiple_question_setup
    r1 = create(:user)
    r2 = create(:user)
    @group.add_member r1
    @group.add_member r2
    q1 = create(:question, user: @user, group: @group)
    q2 = create(:question, user: @user, group: @group)
    q3 = create(:question, user: @user, group: @group)
    create(:report, reportable: q1, reporter: r1)
    create(:report, reportable: q1, reporter: r2)
    create(:report, reportable: q2, reporter: r1)

    Post.reindex
    Post.searchkick_index.refresh

    sign_in @user

    get :index, {:group_id => @group.id, :format => :json}

    resp = get_json_response response

    assert_equal resp.count, 2
    assert [q2.id, q3.id].include?(resp[0]['id'].to_i)
    assert [q2.id, q3.id].include?(resp[1]['id'].to_i)

    teacher = create(:user, email: 'c@c.com')
    @group.add_admin teacher
    create(:report, reportable: q3, reporter: teacher)
    Post.reindex
    Post.searchkick_index.refresh
    get :index, {:group_id => @group.id, :format => :json}

    resp = get_json_response response

    assert_equal resp.count, 1
    assert_equal resp[0]['id'].to_i, q2.id

    sign_out @user

  end

  test 'should set hidden field to true if reported by user' do
    create_default_org
    q1 = create(:question, group: @group, user: @user)
    q2 = create(:question, group: @group, user: @user)
    viewer = create(:user)
    @group.add_member viewer

    create(:report, reporter: viewer, reportable: q1)
    Post.reindex
    Post.searchkick_index.refresh
    sign_in viewer
    get :index, {:group_id => @group.id, :format => :json}
    resp = get_json_response response
    assert resp.select { |r| r['id'].to_i == q1.id }.first['hidden']
    assert_not resp.select { |r| r['id'].to_i == q2.id }.first['hidden']
  end

  test 'should show reporting disabled with message' do
    faculty_question = create(:question, group: @group, user: @faculty)
    Post.reindex
    Post.searchkick_index.refresh

    sign_in @user
    get :show, {:group_id => @group.id, :id => faculty_question.id,
                :format => :json}
    resp = get_json_response response
    assert resp['reporting_disabled']
    assert_equal resp['reporting_disabled_message'],
      'You cannot report a faculty post'
    sign_out @user

    student_question = create(:question, group: @group, user: @user)
    create(:report, reporter: @faculty, reportable: student_question)
    Post.reindex
    Post.searchkick_index.refresh

    sign_in @faculty
    get :show, {:group_id => @group.id, :id => student_question.id,
                :format => :json}
    resp = get_json_response response
    assert resp['reporting_disabled']
    assert_equal resp['reporting_disabled_message'],
      'Reported'
    sign_out @faculty

  end

  test 'should restore question' do
    create_default_org
    q = create(:question, group: @group, user: @user)
    r1 = create(:user)
    r2 = create(:user)
    @group.add_member r1
    @group.add_member r2
    create(:report, reporter: r1, reportable: q)
    create(:report, reporter: r2, reportable: q)

    # normal user cannot restore
    sign_in @user
    post :restore, {:group_id => @group.id, :id => q.id,
                    :format => :json}
    assert_response :forbidden
    assert q.reload.hidden
    sign_out @user

    # faculty can restore posts and clear all reports
    sign_in @faculty
    post :restore, {:group_id => @group.id, :id => q.id,
                    :format => :json}
    assert_response :ok
    assert_not q.reload.hidden
    assert_equal q.valid_reports.count, 0
    sign_out @faculty

    # faculty restoring own reported post
    create(:report, reporter: @faculty, reportable: q)
    sign_in @faculty
    assert_difference -> {q.reports.count}, -1 do
      post :restore, {:group_id => @group.id, :id => q.id,
                      :format => :json}
    end
    assert_response :ok
    assert_not q.reload.hidden
    assert_equal q.valid_reports.count, 0
    sign_out @faculty
  end

  test ":api response get show json" do
    api_setup
    sign_in @user
    get :show, {:group_id => @group.id, :id => @question.id, :format => :json}
    assert_response :success
    assigned_question = assigns(:post)
    assert_not_nil assigned_question

    # test json response
    response_object = get_json_response response
    response_question = response_object
    assert_equal @expected_question, response_question
    sign_out @user
  end

  test "hide user info on anonymous posts" do
    sign_in @user
    question = create(:question, user: @user, group: @group, anonymous: true)
    Post.reindex
    Post.searchkick_index.refresh
    get :show, {:group_id => @group.id, :id => question.id, :format => :json}

    q = assigns(:post)
    assert q
    resp = get_json_response response
    assert_equal resp['user']['name'], "Anonymous"
    assert_equal ActionController::Base.helpers.asset_path('anonymous-user.png'), resp['user']['picture']
    assert_nil resp['user']['first_name']
    assert_nil resp['user']['score']
    assert_nil resp['user']['stars']
    sign_out @user
  end

  test 'should show user voted' do
    faculty_question = create(:question, group: @group, user: @faculty)
    student_question = create(:question, group: @group, user: @user)
    create(:vote, voter: @faculty, votable: student_question)
    student_question.reload
    sign_in @faculty
    Post.reindex
    Post.searchkick_index.refresh
    get :show, {:group_id => @group.id, :id => faculty_question.id,
                :format => :json}
    resp = get_json_response response
    assert_not resp['up_voted']

    get :show, {:group_id => @group.id, :id => student_question.id,
                :format => :json}
    resp = get_json_response response

    assert resp['up_voted']
    sign_out @faculty
  end


  test "staff question should be flagged correctly" do
    sign_in @user
    question = create(:question, user: @faculty, group: @group)
    Post.reindex
    Post.searchkick_index.refresh

    get :show, {:group_id => @group.id, :id => question.id, :format => :json}
    assert_response :success
    response_question = get_json_response response

    assert_equal response_question['is_staff'], true
    sign_out @user
  end

  test 'should filter by context_id' do
    sign_in @user
    q1 = create(:question, group: @group, user: @user, message: '#hw1', created_at: 2.days.ago)
    q2 = create(:question, context_id: 'abc123', group: @group, user: @user, message: '#hw1', created_at: 1.days.ago)

    q3 = create(:question, group: @group, user: @user, message: '#hw2', created_at: 3.days.ago)
    Post.reindex
    Post.searchkick_index.refresh
    get :index, {group_id: @group.id, context_id: 'abc123', format: :json}
    assigned_questions = assigns(:posts)
    assert_equal 1, assigned_questions.size
    assert_equal q2.id, assigned_questions[0].id.to_i
    sign_out @user
  end

  test 'should send is mobile in api response' do
    sign_in @user
    p = create(:post, group: @group, user: @user, is_mobile: true)
    Post.reindex
    Post.searchkick_index.refresh
    get :show, group_id: @group.id, id: p.id, format: :json
    resp = get_json_response(response)
    assert resp['is_mobile']
  end

  test 'should send out file info in api response' do
    api_setup
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)
    fr2 = FileResource.create(attachment: pic.open)
    @question.file_resources << [fr1, fr2]

    Post.reindex
    Post.searchkick_index.refresh

    sign_in @user
    get :show, group_id: @group.id, id: @question.id, format: :json
    resp = get_json_response(response)
    expected_with_file = @expected_question.clone
    expected_with_file['file_resources'] = @question.file_resources.as_json.map{|a| a.merge({'available' => true})}

    assert_equal expected_with_file, resp
  end

  test 'should send out creator label in api response' do
    api_setup
    assistant = create(:user)
    @group.add_user(user: assistant, role: 'admin', label: 'assistant')
    @question.update_attributes(user_id: assistant.id)

    Post.reindex
    Post.searchkick_index.refresh

    sign_in @user
    get :show, group_id: @group.id, id: @question.id, format: :json
    resp = get_json_response(response)
    assert_equal 'assistant', resp['creator_label']

    get :index, group_id: @group.id, format: :json
    resp = get_json_response(response)
    assert_equal 'assistant', resp[0]['creator_label']
  end

  test 'index support last fetch time parameter' do
    api_setup
    q1 = create(:question, user: @user, group: @group, created_at: 3.days.ago)
    q2 = create(:question, user: @user, group: @group, created_at: 1.day.ago)

    Post.reindex
    Post.searchkick_index.refresh

    sign_in @user
    get :index, group_id: @group.id, sort: 'date', last_fetch_time: 2.days.ago, format: :json
    resp = get_json_response(response)
    assert_equal 2, resp.count
    assert_equal [@question.id, q2.id].sort, [resp[0]['id'], resp[1]['id']].sort

    get :index, group_id: @group.id, sort: 'date', last_fetch_time: 5.hours.ago, format: :json
    resp = get_json_response(response)
    assert_equal 1, resp.count
    assert_equal @question.id, resp[0]['id']

    get :index, group_id: @group.id, sort: 'date', last_fetch_time: Time.now, format: :json
    resp = get_json_response(response)
    assert_equal 0, resp.count

    sign_out @user
  end

  test 'show on non existant post should return 404' do
    sign_in @user
    get :show, group_id: @group.id, id: 'xxx', format: :json
    assert_response :not_found
    sign_out @user
  end
end

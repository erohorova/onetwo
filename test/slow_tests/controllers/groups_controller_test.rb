require 'test_helper'

class GroupsDumpControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  self.use_transactional_fixtures = false

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    create_default_org
    User.any_instance.unstub :associate_forum_user_if_present
    AnnouncementJob.unstub :perform_later
    DataDumpJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(false)
    WebMock.disable!

    Post.any_instance.unstub :p_read_by_creator
    @controller = GroupsController.new

    @org = create(:organization)
    @user = create(:user, organization: @org)
    @admin = create(:user, organization: @org)
    @client = create(:client, user: @admin, organization: @org)
    @faculty = create(:user, organization: @org, email: 'slowfaculty@stanford.edu')
    @group = create(:resource_group, organization: @org, client: @client)

    ClientsUser.create(client: @client, user: @user, client_user_id: @user.id)
    ClientsUser.create(client: @client, user: @faculty, client_user_id: @faculty.id)

    @group.add_member @user
    @group.add_admin @faculty

    @p1 = create(:question, group: @group, user: @user)
    @p2 = create(:announcement, group: @group, user: @faculty)
    @a1 = create(:answer, commentable: @p1, user: @faculty)

    Post.reindex
    set_host @org
  end

  teardown do
    WebMock.enable!
  end

  test 'should get data dump' do
    sign_in @admin
    mail_args = {}

    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      mail_args[:to_email] =  message[:message][:to]
      mail_args[:from_email] = message[:message][:from_email]
    }.at_least(3)

    get :data_dump, {client_id: @org.client.id, id: @group.id, format: 'JSON'}
    assert_response :success
    get :data_dump, {client_id: @org.client.id, id: @group.id, format: 'CSV'}
    assert_response :success
    get :data_dump, {client_id: @org.client.id, id: @group.id, format: 'XML'}
    assert_response :success

    assert @admin.email, mail_args[:to_email]
    assert 'admin@edcast.com', mail_args[:from_email]
    sign_out @admin
  end
end

require 'test_helper'

class ReportsSearchControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers
  self.use_transactional_fixtures = false

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    @controller = ReportsController.new
    WebMock.disable!
    Post.reindex

    @org = create(:organization)
    @user = create(:user, organization: @org)
    @client = create(:client, organization: @org)
    @group = create(:resource_group, organization: @org, client: @client)
    @group.add_member @user
    @question = create(:question, group: @group, user: @user)
  end

  teardown do
    WebMock.enable!
  end

  test "report a question" do
    # create_default_org
    sign_in @user
    assert_difference [->{Report.count}, ->{@question.reports.count}] do
      post :create, {:post_id => @question.id, :type => 'spam'}
    end
    assert_response :created
    sign_out @user
  end
end

require 'test_helper'

class ActivityRecordsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers
  self.use_transactional_fixtures = false

  teardown do
    WebMock.enable!
  end

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    WebMock.disable!
    ActivityRecord.create_index! force: true
    @org = create(:organization)
    @client = create(:client, organization: @org)
    @user = create(:user, organization: @org)
    @group1 = create(:resource_group, client: @client, organization: @org)
    @group1.add_member @user
    @group2 = create(:resource_group, client: @client, organization: @org)
    @group2.add_member @user
  end

  test 'should record the user activity across groups' do
    sign_in @user
    ActivityRecord.expects(:create_with_id).with(user_id: @user.id, group_id: @group1.id.to_s,
                                     date: Time.now.utc.strftime("%Y-%m-%d"), type: 'forum')

    post :create, {:group_id => @group1.id, type: 'forum', :format => :json}
  end

  test 'should not record the user activity more than once in the same day' do
    sign_in @user

    today = Time.now
    ActivityRecord.expects(:get_current_time_utc).twice.returns(today).then.returns(today)
    assert_difference -> {ActivityRecord.count}, 1 do
      post :create, {:group_id => @group1.id, type: 'forum', :format => :json}
      post :create, {:group_id => @group1.id, type: 'forum', :format => :json}
      ActivityRecord.gateway.refresh_index!
    end

  end

  # test 'should record the user activity once per day' do
  #   sign_in @user
  #   today = Time.now
  #   tomorrow = today + 1.day

  #   ActivityRecord.expects(:get_current_time_utc).twice.returns(today).then.returns(tomorrow)
  #   assert_difference -> {ActivityRecord.count}, 2 do
  #     post :create, {:group_id => @group1.id, type: 'forum', :format => :json}
  #     post :create, {:group_id => @group1.id, type: 'forum', :format => :json}
  #     ActivityRecord.gateway.refresh_index!
  #   end
  # end
end

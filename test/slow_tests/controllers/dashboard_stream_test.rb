require 'test_helper'

class DashboardStreamTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers
  include HelperMethods

  self.use_transactional_fixtures = false

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    WebMock.disable!

    @controller = CardsController.new
    @user = create(:user)
    @org = @user.organization
    @faculty = create(:user, email: 'slowfaculty@stanford.edu')
    @client = create(:client, organization: @org)
    @org1 = create(:organization)
    @another_client = create(:client, organization: @org1)

    @group = create(:resource_group, organization: @org,
      website_url: 'https://www.edcast.com', name: "test group", client: @client)
    @group.add_admin @faculty
    @group.add_member @user
  end

  teardown do
    WebMock.enable!
  end

  test 'send most recent posts' do
    @group_in_client = create(:resource_group, organization: @org, website_url: 'http://group-in-client.com')
    @group_in_client.add_member(@user)

    if Post.searchkick_index.exists?
      Post.searchkick_index.delete
    end
    fac1 = create(:user, organization: @org)
    group1 = create(:resource_group, organization: @org)
    group1.add_admin fac1
    create(:announcement, user: fac1, resource: create(:resource, url: 'http://some.url/'), group: group1)
    Post.reindex

    create_default_org
    post1 = create(:post, user: @user, group: @group)
    post2 = create(:post, user: @user, group: @group_in_client)
    Post.reindex

    get :dashboard_stream, widget_auth_params(group: @group, user: @user).merge({format: :json})
    assert_response :ok
    resp = get_json_response(response)
    assert_same_elements [post1, post2].map(&:id), resp.map{|r| r['id']}
  end

  test 'handle empty' do
    # different group/client
    another_group = create(:resource_group, organization: @org1,
      website_url: 'https://www.edcast.com', name: "test group", client: @another_client)
    user = create(:user, organization: @org1)
    another_group.add_member user
    post1 = create(:post, user: user, group: another_group)
    Post.reindex

    get :dashboard_stream, widget_auth_params(group: @group, user: @user).merge({format: :json})
    assert_response :ok
    resp = get_json_response(response)
    assert_equal [], resp
  end
end

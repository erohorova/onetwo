require 'test_helper'

class PostReadsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers
  self.use_transactional_fixtures = false

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    WebMock.disable!
    PostRead.create_index! force: true
  end

  teardown do
    WebMock.enable!
  end

  test 'should create read' do
    @org = create(:organization)
    @user = create(:user, organization: @org)
    @student = create(:user, organization: @org)
    @group = create(:resource_group, organization: @org)
    @group.add_member(@student)
    @group.add_member(@user)
    @question = create(:question, user: @student, group: @group)
    @post = create(:post, user: @student, group: @group)
    PostRead.gateway.refresh_index!

    sign_in @user

    assert_difference -> {PostRead.count} do
      post :create, {:post_id => @question.id, :format => :json}
      PostRead.gateway.refresh_index!
    end
    assert_response :created
    assert @question.read_by? @user

    assert_difference -> {PostRead.count} do
      post :create, {:post_id => @post.id, :format => :json}
      PostRead.gateway.refresh_index!
    end
    assert_response :created
    assert @post.read_by? @user
    assert_equal @post.group_id, assigns[:read].group_id
    sign_out @user
  end


  test 'should prevent duplicate post reads' do
    @org = create(:organization)
    @user = create(:user, organization: @org)
    @student = create(:user, organization: @org)
    @group = create(:resource_group, organization: @org)
    @group.add_member(@student)
    @group.add_member(@user)
    @question = create(:question, user: @student, group: @group)
    @post = create(:post, user: @student, group: @group)
    PostRead.gateway.refresh_index!

    sign_in @user

    assert_difference -> {PostRead.count} do
      post :create, {:post_id => @question.id, :format => :json}
      PostRead.gateway.refresh_index!
    end
    first_pr = PostRead.find("post-id-#{@question.id}-user-id-#{@user.id}")
    sleep 2

    assert_no_difference -> {PostRead.count} do
      post :create, {:post_id => @question.id, :format => :json}
      PostRead.gateway.refresh_index!
    end
    second_pr = PostRead.find("post-id-#{@question.id}-user-id-#{@user.id}")

    assert first_pr
    assert second_pr
    assert_not_equal first_pr.updated_at, second_pr.updated_at, 'should have different last read time'
    assert_equal first_pr.created_at, second_pr.created_at, 'should have the same first read time'

    sign_out @user

    user2 = create(:user)
    @group.add_member(user2)
    sign_in user2

    #making sure other user can read the post too
    assert_difference -> {PostRead.count} do
      post :create, {:post_id => @question.id, :format => :json}
      PostRead.gateway.refresh_index!
    end

  end

  test 'should show status forbidden if user not signed in' do
    assert_no_difference -> {PostRead.count} do
      post :create, {:post_id => 123, :format => :json}
    end
    assert_response :unauthorized
  end

end

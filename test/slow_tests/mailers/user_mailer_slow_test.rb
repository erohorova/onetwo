require 'test_helper'

class UserMailerSlowTest < ActionMailer::TestCase
  self.use_transactional_fixtures = false

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(false)
    AnnouncementJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    @org = create(:organization)
    @client = create(:client, organization: @org)
  end

  test 'should include link to opt out of announcement email' do
    user1 = create(:user, organization: @org)
    user2 = create(:user, organization: @org)
    client = create(:client, organization: @org)
    group = create(:resource_group, website_url: 'http://localhost/blue', organization: @org, client: @client)
    group.add_member user1
    group.add_admin user2

    unsubscribe_url =''
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      unsubscribe_url = message[:message][:merge_vars].first[:vars][3][:content]
    }
    create(:announcement, group: group, user: user2)
    assert_match /https:\/\/#{Settings.platform_host}\/preferences/, unsubscribe_url
  end

  test 'do not notify students who have opted out of announcement' do

    faculty1 = create(:user, organization: @org)
    group1 = create(:group, organization: @org, client: @client)
    group1.add_admin  faculty1
    student1 = create(:user, organization: @org)
    student2 = create(:user, organization: @org)
    group1.add_member student1
    group1.add_member student2

    create(:user_preference, user: student2, key: "notification.announcement.opt_out", value: 'true', preferenceable: group1)

    MandrillMailer.any_instance.expects(:send_api_email).once

    #only instructor and the student who did not opt out should get the email
    create(:announcement, group: group1, user: faculty1)

    ActionMailer::Base.deliveries.each do |email|
      assert_no_match [student2.email], email.header.to_s
    end
  end
end

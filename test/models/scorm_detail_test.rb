require 'test_helper'

class ScormDetailTest < ActiveSupport::TestCase

  setup do
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @resource = create(:resource)
  end
  test '#status_running? should return true if still uploading' do
    scorm_card = create(:card, author: @user, organization: @organization, state:   'processing',
      resource_id: @resource.id, filestack: [scorm_course: true,
        url: 'https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv'])
    scorm_detail = create(:scorm_detail, status: 'running', token: 'randomtoken000', card: scorm_card)

    assert_equal true, scorm_detail.running?
  end

  test '#status_finished? should return true if uploading finished' do
    scorm_card = create(:card, author: @user, organization: @organization, state:   'published',
      resource_id: @resource.id, filestack: [scorm_course: true,
        url: 'https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv'])
    scorm_detail = create(:scorm_detail, status: 'finished', token: 'randomtoken000', card: scorm_card)

    assert_equal true, scorm_detail.finished?
  end

  test '#status_error? should return true if error in uploading' do
    scorm_card = create(:card, author: @user, organization: @organization, state:   'processing',
      resource_id: @resource.id, filestack: [scorm_course: true,
        url: 'https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv'])
    scorm_detail = create(:scorm_detail, status: 'error', token: 'randomtoken000', card: scorm_card)

    assert_equal true, scorm_detail.error?
  end
end

require 'test_helper'

class XapiCredentialTest < ActiveSupport::TestCase
  should belong_to(:organization)

  test 'should call to lms for xapi credentials sync' do
    LmsXapiSyncJob.expects(:perform_later).with(23).once

    organization    = create :organization
    xapi_credential = create :xapi_credential, id: 23, organization: organization
    xapi_credential.run_callbacks(:commit)
  end
end

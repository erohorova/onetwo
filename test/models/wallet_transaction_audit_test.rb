require 'test_helper'

class WalletTransactionAuditTest < ActiveSupport::TestCase
  should have_db_column(:wallet_transaction_id).of_type(:integer)
  should have_db_column(:order_data).of_type(:text)
  should have_db_column(:orderable_data).of_type(:text)
  should have_db_column(:user_data).of_type(:text)
  should have_db_column(:wallet_transaction_data).of_type(:text)

  should validate_presence_of(:wallet_transaction_id)
end

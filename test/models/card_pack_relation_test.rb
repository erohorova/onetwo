require 'test_helper'

class CardPackRelationTest < ActiveSupport::TestCase
  should belong_to :cover
  include CardHelperMethods
  self.use_transactional_fixtures = false

  setup do
    ChannelsCard.any_instance.stubs :create_users_management_card
    stub_request(:post, 'http://localhost:9200/cards_test/_refresh')
    setup_one_card_pack

    @hidden = @cards[2]
    @cards[1].update(hidden: false)
    @hidden.update hidden: true
    @hidden_id = @hidden.id
    @non_hidden_id = @cards[1].id

    @channels = %i{robotics health}.map do |name|
      create(:channel, name, organization: @org)
    end
    @cover.channels = @channels

    Card.any_instance.stubs(:remove_card_from_queue)
    Search::CardSearch.any_instance.stubs(:search_by_id).returns(@cards.map(&:search_data).map{|x| Hashie::Mash.new(x)})
  end

  test 'retrieves packs' do
    assert_same_ids @cards, CardPackRelation.pack(cover_id: @cover.id)
  end

  test 'retrieves large packs' do
    skip
    cards = @cards.dup
    30.times do
      c = create(:card, organization: @org, author: @author)
      cards << c
      # updating to: will create a new relation record for the following item
      CardPackRelation.where(cover: @cover, to_id: nil).first.update to: c
    end
    # Card.searchkick_index.refresh
    pack = CardPackRelation.pack(cover_id: @cover.id)
    assert_same_ids cards, pack

    # make sure all cards are retrieved
    assert pack.map(&:deleted).none?
  end

  test 'retrieves packs with placeholder for deleted card' do
    skip
    deleted_id = @cards[1].id
    deleted_title = @cards[1].message
    @cards[1].destroy!
    # Card.searchkick_index.refresh

    @cards[1] = OpenStruct.new id: deleted_id

    pack = CardPackRelation.pack(cover_id: @cover.id)
    assert_same_ids @cards, pack
    assert pack[1][:deleted]
    assert_equal deleted_title, pack[1][:title]

    # user deletes the placeholder entry
    assert CardPackRelation.remove_from_pack(
        cover_id: @cover.id,
        remove_id: deleted_id,
        remove_type: 'Card',
    )
    @cards.delete_at 1
    assert_same_ids @cards, CardPackRelation.pack(cover_id: @cover.id)
  end

   test 'assignment url for CardPackRelation' do
    pathway = create(:card_pack_relation)
    assert_match "/collections/#{pathway.cover.slug}", pathway.assignment_url
  end

  test 'to_id record should exist' do
    new_card1 = create(:card, author_id: @author.id)
    CardPackRelation.add(cover_id: @cover.id, add_id: new_card1.id, add_type: 'Card')
    last_relation = @cover.pack_relns.last
    last_relation.update(to_id: last_relation.id+1)

    assert_nil @cover.pack_relns.last.to_id
  end

  test 'adds to packs' do
    new_card = create(:card, author_id: @author.id)
    CardPackRelation.add(cover_id: @cover.id, add_id: new_card.id, add_type: 'Card')
    assert CardPackRelation.exists?(cover_id: @cover.id, from_id: new_card.id)
  end

  test 'removes packs' do
    cover_id = @cover.id
    assert CardPackRelation.remove(cover_id: cover_id)
    assert_not Card.exists?(cover_id)
    assert_nil CardPackRelation.pack(cover_id: cover_id)
  end

  test 'should call PackJourneyCardReprocessJob after commit' do
    new_card = create(:card, author_id: @author.id)
    TestAfterCommit.with_commits(true) do
      PackJourneyCardReprocessJob.expects(:perform_later).once
      CardPackRelation.add(cover_id: @cover.id, add_id: new_card.id, add_type: 'Card')
    end
  end

  test 'should call PackJourneyCardReprocessJob after destroy' do
    cover_id = @cover.id
    TestAfterCommit.with_commits(true) do
      PackJourneyCardReprocessJob.expects(:perform_later).once
      CardPackRelation.remove(cover_id: cover_id)
    end
  end

  test 'errors work' do
    Rails.logger.expects(:error).with(regexp_matches(/no relations.*-1/))
    assert_nil CardPackRelation.remove(cover_id: -1)
  end

  test 'should destroy hidden cards from destroyed pack' do
    assert CardPackRelation.remove(cover_id: @cover.id)
    assert_not Card.exists?(@hidden_id)
    assert Card.exists?(@non_hidden_id)
  end

  test 'should expose hidden cards from destroyed pack' do
    assert CardPackRelation.remove(cover_id: @cover.id, destroy_hidden: false)

    assert Card.exists?(@hidden_id)
    assert_not @hidden.reload.hidden
    assert_equal @channels, @hidden.channels
  end

  test "should remove from pack" do
    pack = CardPackRelation.pack(cover_id: @cover.id)
    old_pack_count = pack.count
    old_pack_ids = pack.map(&:id)

    remove_id = old_pack_ids[1]

    assert_difference -> { CardPackRelation.count }, -1 do
      assert CardPackRelation.remove_from_pack(
          cover_id: @cover.id,
          remove_id: remove_id,
          remove_type: 'Card',
      )
    end
    refute CardPackRelation.exists?(cover_id: @cover.id, from_id: remove_id)

    pack = CardPackRelation.pack(cover_id: @cover.id)
    assert_equal old_pack_count, pack.count + 1
    assert_equal pack.map(&:id), old_pack_ids - [remove_id]
  end

  test "remove_from_pack should return false on error" do
    pack = CardPackRelation.pack(cover_id: @cover.id)

    assert_not CardPackRelation.remove_from_pack(
        cover_id: @cover.id,
        remove_id: 9999,
        remove_type: 'Card',
    )
    assert_not CardPackRelation.remove_from_pack(
        cover_id: 9999,
        remove_id: pack[1].id,
        remove_type: 'Card',
    )

    assert_not CardPackRelation.remove_from_pack(
        cover_id: @cover.id,
        remove_id: pack[1].id,
        remove_type: 'snuffaluffagus',
    )

    # relation doesn't get destroyed for some reason
    CardPackRelation.any_instance.expects(:destroy)

    assert_not CardPackRelation.remove_from_pack(
        cover_id: @cover.id,
        remove_id: pack[1].id,
        remove_type: 'Card',
    )
  end

  test 'should expose removed hidden card' do
    assert CardPackRelation.remove_from_pack(
        cover_id: @cover.id,
        remove_id: @hidden_id,
        remove_type: 'Card',
    )

    pack = CardPackRelation.pack(cover_id: @cover.id)
    assert_equal @cards.map(&:id) - [@hidden_id], pack.map(&:id)
    assert_not @hidden.reload.hidden
    assert_equal @channels, @hidden.channels
  end

  test 'should delete removed hidden card' do
    assert CardPackRelation.remove_from_pack(
        cover_id: @cover.id,
        remove_id: @hidden_id,
        remove_type: 'Card',
        destroy_hidden: true,
    )

    pack = CardPackRelation.pack(cover_id: @cover.id)
    assert_equal @cards.map(&:id) - [@hidden_id], pack.map(&:id)
    assert_not Card.exists?(@hidden_id)
  end

  test 'should not alter removed independent card' do
    assert CardPackRelation.remove_from_pack(
        cover_id: @cover.id,
        remove_id: @non_hidden_id,
        remove_type: 'Card',
        destroy_hidden: true,
    )
    pack = CardPackRelation.pack(cover_id: @cover.id)
    assert_equal @cards.map(&:id) - [@non_hidden_id], pack.map(&:id)
    assert Card.exists?(@non_hidden_id)
  end

  test 'reorder_pack_relations' do
    cover_id = @cover.id

    #string position
    assert_nil CardPackRelation.reorder_pack_relations(cover_id: cover_id, from_pos: 2, to_pos: "ss")

    #reorder with same position
    assert_nil CardPackRelation.reorder_pack_relations(cover_id: cover_id, from_pos: 2, to_pos: 2)

    #out of bound
    assert_nil CardPackRelation.reorder_pack_relations(cover_id: cover_id, from_pos: 2, to_pos: 50)

    #Cannot change cover position
    assert_nil CardPackRelation.reorder_pack_relations(cover_id: cover_id, from_pos: 2, to_pos: 0)

    # bad calls had no effect
    assert_same_ids @cards, CardPackRelation.pack(cover_id: cover_id)

    #2->1
    assert CardPackRelation.reorder_pack_relations(cover_id: cover_id, from_pos: 2, to_pos: 1)
    assert_same_ids [@cover, @card2, @card1, @card3], CardPackRelation.pack(cover_id: cover_id)

    #1->3
    assert CardPackRelation.reorder_pack_relations(cover_id: cover_id, from_pos: 1, to_pos: 3)
    assert_same_ids [@cover, @card1, @card3, @card2], CardPackRelation.pack(cover_id: cover_id)

    #3->1
    assert CardPackRelation.reorder_pack_relations(cover_id: cover_id, from_pos: 3, to_pos: 1)
    assert_same_ids [@cover, @card2, @card1, @card3], CardPackRelation.pack(cover_id: cover_id)

    #3->2
    assert CardPackRelation.reorder_pack_relations(cover_id: cover_id, from_pos: 3, to_pos: 2)
    assert_same_ids [@cover, @card2, @card3, @card1], CardPackRelation.pack(cover_id: cover_id)
  end

  class StreamCardPackRelationTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false
    include CardHelperMethods
    setup do
      stub_request(:post, "http://localhost:9200/cards_test/_refresh")
      VideoStream.stubs :reindex

      setup_card_packs
    end

    test 'retrieves packs with streams' do
      Search::CardSearch.any_instance.expects(:search_by_id).returns(
        ([@mixed_cover] + @cards[@mixed_cover]).map(&:search_data).map{|x| Hashie::Mash.new(x)}
      )

      assert_same_ids [@mixed_cover] + @cards[@mixed_cover],
                      CardPackRelation.pack(cover_id: @mixed_cover.id)
    end

    test 'deleted stream placeholder' do
      skip #pls see note in test_helper#setup_card_packs
      items = [@mixed_cover] + @cards[@mixed_cover]

      Search::CardSearch.any_instance.expects(:search_by_id).returns(
        items.map(&:search_data).map{|x| Hashie::Mash.new(x)}
      )

      deleted_id = items[1].id
      deleted_title = items[1].message
      items[1].destroy!
      # Card.searchkick_index.refresh

      items[1] = OpenStruct.new id: deleted_id
      pack = CardPackRelation.pack(cover_id: @mixed_cover.id)

      assert_same_ids items, pack
      assert pack[1][:deleted]
      assert_equal deleted_title, pack[1][:title]

      # user deletes the placeholder entry
      assert CardPackRelation.remove_from_pack(
          cover_id: @mixed_cover.id,
          remove_id: deleted_id,
          remove_type: 'Card',
      )
      items.delete_at 1

      Search::CardSearch.any_instance.expects(:search_by_id).returns(
        items.map(&:search_data).map{|x| Hashie::Mash.new(x)}
      )
      assert_same_ids items, CardPackRelation.pack(cover_id: @mixed_cover.id)
    end

    test 'add stream to pack' do
      skip #we allow only cards to be added to pathway
      VideoStream.any_instance.stubs :create_index
      VideoStream.any_instance.stubs :update_index
      new_stream = create(:wowza_video_stream, creator: create(:user, organization: @org))
      CardPackRelation.add(cover_id: @pack_cover.id, add_id: new_stream.id, add_type: 'VideoStream')

      assert CardPackRelation.exists?(cover_id: @pack_cover.id, from_id: new_stream.id)
    end

    test 'remove packs with streams' do
      cover_id = @mixed_cover.id
      assert CardPackRelation.remove(cover_id: cover_id)
      assert_not Card.exists?(cover_id)
      refute CardPackRelation.exists?(cover_id: cover_id)
    end

    test 'removes streams from packs' do
      items = [@mixed_cover] + @cards[@mixed_cover]
      Search::CardSearch.any_instance.expects(:search_by_id).returns(
        items.map(&:search_data).map{|x| Hashie::Mash.new(x)}
      )
      # malformed
      assert_not CardPackRelation.remove_from_pack(
          cover_id: @mixed_cover.id,
          remove_id: items[1].id,
          remove_type: 'WowzaVideoStream',
      )
      assert_same_ids items, CardPackRelation.pack(cover_id: @mixed_cover.id)

      # proper
      assert CardPackRelation.remove_from_pack(
          cover_id: @mixed_cover.id,
          remove_id: items[1].id,
          remove_type: 'Card',
      )
      items.delete_at 1
      Search::CardSearch.any_instance.expects(:search_by_id).returns(
          items.map(&:search_data).map{|x| Hashie::Mash.new(x)}
      )
      assert_same_ids items, CardPackRelation.pack(cover_id: @mixed_cover.id)
    end

    test 'gets items' do
      video_cards = @cards[@mixed_cover].values_at(0,2,4)
      cards = @cards[@mixed_cover].values_at(1,3)
      Search::CardSearch.any_instance.expects(:search_by_id).with(video_cards.map(&:id), {:state => ['draft', 'published']} ).returns(
        video_cards.map(&:search_data).map{|x| Hashie::Mash.new(x)}
      )
      Search::CardSearch.any_instance.expects(:search_by_id).with(cards.map(&:id), {:state => ['draft', 'published']}).returns(
        cards.map(&:search_data).map{|x| Hashie::Mash.new(x)}
      )

      assert_same_elements video_cards.map(&:id),
                           CardPackRelation.get_items(type: 'Card', ids: video_cards.map(&:id)).map(&:id)
      assert_same_ids cards.map(&:id),
                      CardPackRelation.get_items(type: 'Card', ids: cards.map(&:id)).map(&:id),
                      ordered: false
    end

    test 'counts' do
      counts = CardPackRelation.pack_counts(cover_ids: [@pack_cover.id, @mixed_cover.id])
      assert_equal 2, counts.size
      assert_equal 3, counts[@pack_cover.id]
      assert_equal 5, counts[@mixed_cover.id]

      assert_equal 3, CardPackRelation.pack_count(cover_id: @draft_cover.id)

      # not pack covers; no results
      assert_empty CardPackRelation.pack_counts(cover_ids: @cards[@pack_cover].map(&:id))
      assert_nil CardPackRelation.pack_count(cover_id: @cards[@pack_cover].first.id)
    end

    test 'should reindex cover' do
      cpr = CardPackRelation.pack_relations(cover_id: @mixed_cover.id).first
      Card.any_instance.expects(:reindex_card).once
      cpr.touch
    end
  end

  class RecordPathwayEvent < ActiveSupport::TestCase
    self.use_transactional_fixtures = false
    setup do
      IndexJob.stubs(:perform_later)
      Card.any_instance.stubs(:reindex).returns true
    end

    test 'record add to pathway event' do
      stub_time
      Card.any_instance.stubs(:record_card_create_event).returns true

      org = create(:organization)
      author = create(:user, organization: org)

      cover = create(:card,
        card_type: 'pack',
        card_subtype: 'simple',
        author: author,
        title: "cover_title"
      )

      card1, card2 = create_list(:card, 2, author: author)
      cards = cover, card1, card2

      additional_data = {
        platform: 'web',
        user_agent: 'chrome'
      }
      RequestStore.stubs(:read).with(:request_metadata).returns additional_data

      [card1, card2].each do |card|
        # 2 expectations for two cards being added to pathway
        Analytics::MetricsRecorderJob.expects(:perform_later).with(
          recorder: "Analytics::CardMetricsRecorder",
          org: Analytics::MetricsRecorder.org_attributes(card.organization),
          card: Analytics::MetricsRecorder.card_attributes(card),
          event: 'card_added_to_pathway',
          actor: Analytics::MetricsRecorder.user_attributes(cover.author),
          timestamp: Time.now.to_i,
          additional_data: additional_data.merge(
            pathway_id:   cover.id,
            pathway_name: cover.title
          )
        )

        CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: card.class.name)
      end
    end

    test 'record remove from pathway event' do
      stub_time
      Organization.any_instance.stubs(:create_admin_user)
      Card.any_instance.stubs(:record_card_create_event).returns true
      CardPackRelation.any_instance.stubs(:record_add_to_pathway_event)

      org = create(:organization)
      author = create(:user, organization: org)

      cover = create :card,
        card_type: 'pack',
        card_subtype: 'simple',
        author: author,
        title: "cover_title"

      card1, card2 = create_list(:card, 2, author: author, organization: org)
      cards = cover, card1, card2

      [card1, card2].each do |card|
        CardPackRelation.add(
          cover_id: cover.id,
          add_id: card.id,
          add_type: card.class.name
        )
      end

      additional_data = {
        platform: 'web',
        user_agent: 'chrome',
        pathway_id: cover.id.to_s,
        pathway_name: cover.title
      }

      RequestStore.stubs(:read).with(:request_metadata).returns additional_data

      Analytics::MetricsRecorderJob.expects(:perform_later).with(
        recorder: "Analytics::CardMetricsRecorder",
        org: Analytics::MetricsRecorder.org_attributes(card1.organization),
        card: Analytics::MetricsRecorder.card_attributes(card1),
        event: 'card_removed_from_pathway',
        actor: Analytics::MetricsRecorder.user_attributes(cover.author),
        timestamp: Time.now.to_i,
        additional_data: additional_data
      )

      CardPackRelation.remove_from_pack(
        cover_id: cover.id,
        remove_id: card1.id,
        remove_type: 'Card',
        destroy_hidden: false
      )
    end
  end

  class CPRManageableTest < ActiveSupport::TestCase

    setup do
      @org = create(:organization)
      @user, @author = create_list(:user, 2, organization: @org)
      @public_card = create(:card, author: @author, organization: @org)
      @user_card = create(:card, author: @user, organization: @org)
      @cover_pathway = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user, organization: @org)
      @cover_journey = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: @user, organization: @org)
      @section = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user, organization: @org)
      JourneyPackRelation.add(cover_id: @cover_journey.id, add_id: @section.id)
    end

    test '#manageable should return true if adding public card to User pathway' do
      cpr = CardPackRelation.new(cover_id: @cover_pathway.id, from_id: @public_card.id, from_type: 'Card')
      assert cpr.is_manageable?
    end

    test '#manageable should return false if adding User card to User pathway' do
      cpr = CardPackRelation.new(cover_id: @cover_pathway.id, from_id: @user_card.id, from_type: 'Card')
      refute cpr.is_manageable?
    end

    test '#manageable should return true if adding public card to User journey(section)' do
      cpr = CardPackRelation.new(cover_id: @section.id, from_id: @public_card.id, from_type: 'Card')
      assert cpr.is_manageable?
    end

    test '#manageable should return false if adding User card to User journey(section)' do
      cpr = CardPackRelation.new(cover_id: @section.id, from_id: @user_card.id, from_type: 'Card')
      refute cpr.is_manageable?
    end

    test '#manageable should return true if adding card without author' do
      card_without_author = create(:card, organization: @org, author: nil)
      cpr = CardPackRelation.new(cover_id: @cover_pathway.id, from_id: card_without_author.id, from_type: 'Card')
      assert cpr.is_manageable?
    end

    test '#get_ucm_values should return correct values for cpr object' do
      expected = [@user.id, @public_card.id, 'added_to_pathway', @cover_pathway.id]
      cpr = CardPackRelation.new(cover_id: @cover_pathway.id, from_id: @public_card.id, from_type: 'Card')
      options = cpr.get_ucm_values
      assert_equal expected, options
    end

    test '#get_ucm_values should return correct values for cpr object related to journey' do
      expected = [@user.id, @public_card.id, 'added_to_journey', @cover_journey.id]
      cpr_journey = CardPackRelation.new(cover_id: @section.id, from_id: @public_card.id, from_type: 'Card')
      options = cpr_journey.get_journey_ucm_values
      assert_equal expected, options
    end

  end

end

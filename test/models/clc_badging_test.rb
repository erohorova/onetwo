require 'test_helper'

class ClcBadgingTest < ActiveSupport::TestCase
  should have_db_column(:title).of_type(:string)
  should have_db_column(:badge_id).of_type(:integer)
  should have_db_column(:badgeable_id).of_type(:integer)
  should have_db_column(:type).of_type(:string)

  should belong_to(:clc_badge)
  should belong_to(:clc)

  should have_many :user_badges

  should validate_presence_of(:badge_id)
  should validate_presence_of(:title)

  test 'should assign org before save' do
    org = create(:organization)
    user = create(:user, organization: org)
    clc_badge = create(:clc_badge, organization: org)
    clc = create(:clc, name: 'test clc', entity: org, organization: org,
                 clc_badgings_attributes: [{badge_id: clc_badge.id,
                                            title: "clc-25",
                                            target_steps:25}])
    clc_badging = clc.clc_badgings.first
    user_badges = create(:user_badge, user_id: user.id, badging_id: clc_badging.id)

    assert_equal org.id, clc_badge.organization_id
    assert_equal clc_badge.type, 'ClcBadge'
    assert_equal clc_badging.type, 'ClcBadging'
    assert_equal clc_badging.badge_id, clc_badge.id
    assert_equal clc_badging.badgeable_id, clc.id
  end
end

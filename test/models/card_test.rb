# coding: utf-8
require 'test_helper'

class CardTest < ActiveSupport::TestCase
  setup do
    @org = create(:organization)
    @u = create(:user, organization: @org)

    @card = create(:card, card_type: 'media', card_subtype: 'text', author_id: @u.id, title: 'this is title', message: 'this is message')
    WebMock.disable!
  end

  teardown do
    WebMock.enable!
  end

  should have_one :video_stream
  should have_one :project
  should accept_nested_attributes_for(:tags)
  should have_one :card_badging
  should accept_nested_attributes_for(:card_badging)
  should accept_nested_attributes_for(:project)
  should accept_nested_attributes_for(:resource)


  should have_many :card_reportings
  should have_many(:card_team_permissions).dependent(:destroy)
  should have_many(:card_user_permissions).dependent(:destroy)
  should have_many(:card_user_shares).dependent(:destroy)


  test 'should populate message when title is null' do
    @card = create(:card, title:nil, message: 'It is good')
    assert_equal 'It is good', @card.display_title(27)
  end

  test 'expect notify user job on destroy' do
    cover = create(:card, card_type: 'pack', card_subtype: 'simple', author_id: @u.id, title: 'This is project pathway', message: 'This is message for pathway', organization_id: @org.id)
    @ugc = create(:card, card_type: 'project', card_subtype: 'text', author_id: @u.id, title: 'This is project card', message: 'This is test message for project card', organization_id: @org.id)
    CardPackRelation.add(cover_id: cover.id, add_id: @ugc.id, add_type: @ugc.class.name)
    @ugc.current_user = @u
    NotifyUserJob.expects(:perform_later).with({action: 'removed_from', card_ids: [@ugc.id], cover_ids: [cover.id], actor_id: @u.id}).once
    @ugc.destroy
  end

  test 'should delete structure_item if card is deleted' do
    card = create(:card, card_type: 'media', card_subtype: 'text', author_id: @u.id, title: 'This is private card', message: 'This is message',  is_public: true, organization_id: @org.id)
    structure = create(:structure, parent: @org, current_context: 'discover', current_slug: 'cards', creator: @u, display_name: 'Test')
    structure_item = create(:structured_item, structure: structure, entity: card)
    structure_item_id = structure_item.id
    card.destroy
    assert_nil StructuredItem.find_by_id(structure_item_id)
  end

  test 'post to channel and reindex when posted' do
    TestAfterCommit.with_commits(true) do
      Card.any_instance.expects(:reindex_card).once
      author = create :user, organization: @org
      card = create :card, organization: @org, author: author
      channel = create :channel, organization: @org

      card.current_user = author
      card.post_to_channel([channel.id])
      assert_includes card.channels.ids, channel.id
    end
  end

  test 'remove from channel' do
    org = create :organization
    user = create :user, organization: org
    author = create :user, organization: org
    card = create :card, organization: org, author: author
    channel = create :channel, organization: org
    channels_card = create(:channels_card, card_id: card.id, channel_id: channel.id)

    assert_includes card.channels.ids, channel.id

    card.remove_from_channel([channel.id])
    assert_not_includes card.channels.ids, channel.id
  end

  test 'should fetch card reportings' do
    org = create :organization
    user = create :user, organization: org
    author = create :user, organization: org
    card = create :card, organization: org, author: author
    card_reporting = create :card_reporting, card_id: card.id, user_id: user.id

    results = card.fetch_card_reportings
    assert_equal 1, results.length
  end

  test 'should test functionality of check_on_update callback on card update for text cards' do
    media_text_card = create(:card, card_type: 'media', card_subtype: 'text', author: @u, message: 'Hi')
    media_text_card.update_attribute(:message , "hello")
    assert_equal  [], media_text_card.filestack
  end

  test 'Should able to update project card' do
    @user = create(:user, organization: @org)
    cover = create(:card, card_type: 'pack', card_subtype: 'simple', author_id: @user.id, title: 'This is project pathway', message: 'This is message for pathway', organization_id: @org.id)
    @ugc = create(:card, card_type: 'project', card_subtype: 'text', author_id: @user.id, title: 'This is project card', message: 'This is test message for project card', organization_id: @org.id)
    CardPackRelation.add(cover_id: cover.id, add_id: @ugc.id, add_type: @ugc.class.name)
    assert_equal @ugc.card_subtype, 'text'

    #updating the project card
    @ugc.message = 'This is message'
    @ugc.save

    assert_equal @ugc.card_subtype, 'text'
    assert_equal @ugc.message, 'This is message'
  end

  test 'should assign org before save' do
    org = create(:organization)
    u = create(:user, organization: org, organization_role: 'member')
    c = create(:card, author_id: u.id)
    assert_equal org, c.organization
  end

  test 'new poll notification to not be triggered for polls that are hidden(created inside a pathway)' do
    user = create(:user, organization: @org)
    create(:follow, followable: user, user_id: user.id)

    TestAfterCommit.with_commits do
      Card.any_instance.expects(:generate_poll_notification).never
      hidden_poll =  create(:card, organization: @org, hidden: true, author: @u, card_type: 'poll')
    end
  end

  test 'new poll notification to be triggered for polls that are not hidden(created inside a pathway)' do
    user = create(:user, organization: @org)
    create(:follow, followable: user, user_id: user.id)

    TestAfterCommit.with_commits do
      Card.any_instance.expects(:generate_poll_notification).once
      poll =  create(:card, organization: @org, author: @u, card_type: 'poll')
    end
  end

  test 'should trigger propagate_card_to_ecl when author is changed' do
    user = create(:user, organization: @org)
    user1 = create(:user, organization: @org)
    card = create :card, organization: @org, author: user

    TestAfterCommit.with_commits do
      EclCardPropagationJob.expects(:perform_later).once
      card.expects(:propagate_card_to_ecl).once
      card.update(author_id: user1.id)
    end
  end

  test 'should trigger change_author when author is changed' do
    TestAfterCommit.enabled = true
    user = create(:user, organization: @org)
    user1 = create(:user, organization: @org)
    card = create :card, organization: @org, author: user

    TestAfterCommit.with_commits do
      card.expects(:change_author).once
      card.update(author_id: user1.id)
    end
  end

  test 'should trigger transfer_ownership_job when change_author is called' do
    TestAfterCommit.enabled = true
    user = create(:user, organization: @org)
    user1 = create(:user, organization: @org)
    card = create :card, organization: @org, author: user

    TestAfterCommit.with_commits do
      card.current_user = user
      TransferOwnershipJob.expects(:perform_later).once
      card.update(author_id: user1.id)
      card.change_author
    end
  end

  test 'should return pathway as paid if any card is_paid within pathway' do
    cover = create(:card, organization_id: @org.id, author: @u, card_type: 'pack')
    card1 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card1', is_paid: true)
    card2 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card2', is_paid: false)
    create(:card_metadatum, plan: 'paid', card_id: card1.id)
    CardPackRelation.add(cover_id: cover.id, add_id: card1.id, add_type: 'Card')
    UpdateCardMetadataJob.perform_now(cover.id)
    assert_equal true, cover.is_paid_card
  end

  test 'should return pathway as paid if associated card has paid plan (paid/premium/subscription)' do
    cover = create(:card, organization_id: @org.id, author: @u, card_type: 'pack')
    card1 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card1', is_paid: false)
    card2 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card2', is_paid: false)
    create(:card_metadatum, plan: 'premium', card_id: card1.id)
    CardPackRelation.add(cover_id: cover.id, add_id: card1.id, add_type: 'Card')
    UpdateCardMetadataJob.perform_now(cover.id)
    assert_equal true, cover.is_paid_card
  end

  test 'should return pathway as free if all cards within pathway are free' do
    cover = create(:card, organization_id: @org.id, author: @u, card_type: 'pack')
    card1 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card1', is_paid: false)
    card2 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card2', is_paid: false)
    create(:card_metadatum, plan: 'free', card_id: card1.id)
    create(:card_metadatum, plan: 'free', card_id: card2.id)
    CardPackRelation.add(cover_id: cover.id, add_id: card1.id, add_type: 'Card')
    CardPackRelation.add(cover_id: cover.id, add_id: card2.id, add_type: 'Card')
    UpdateCardMetadataJob.perform_now(cover.id)
    assert_equal false, cover.is_paid_card
  end

  test 'should return pathway as paid if we add a paid card in a free pathway' do
    cover = create(:card, organization_id: @org.id, author: @u, card_type: 'pack')
    card1 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card1', is_paid: false)
    create(:card_metadatum, plan: 'free', card_id: card1.id)
    CardPackRelation.add(cover_id: cover.id, add_id: card1.id, add_type: 'Card')
    create(:card_metadatum, plan: 'free', card_id: cover.id)
    assert_equal false, cover.is_paid_card

    card2 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card2', is_paid: true)
    create(:card_metadatum, plan: 'paid', card_id: card2.id)
    CardPackRelation.add(cover_id: cover.id, add_id: card2.id, add_type: 'Card')
    UpdateCardMetadataJob.perform_now(cover.id)
    cover.reload
    assert_equal 'free/paid', cover.card_metadatum.plan
  end

  test 'should return pathway as free if we remove  paid card from a paid pathway' do
    cover = create(:card, organization_id: @org.id, author: @u, card_type: 'pack')
    card1 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card1', is_paid: true)
    card2 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card2', is_paid: false)

    create(:card_metadatum, plan: 'paid', card_id: card1.id)
    create(:card_metadatum, plan: 'free', card_id: card2.id)
    create(:card_metadatum, plan: 'paid', card_id: cover.id)

    CardPackRelation.add(cover_id: cover.id, add_id: card1.id, add_type: 'Card')
    CardPackRelation.add(cover_id: cover.id, add_id: card2.id, add_type: 'Card')

    assert_equal true, cover.is_paid_card

    CardPackRelation.remove_from_pack(cover_id: cover.id, remove_id: card1.id, remove_type: 'Card',destroy_hidden: false)
    UpdateCardMetadataJob.perform_now(cover.id)
    cover.reload
    assert_equal false, cover.is_paid_card
  end

  test 'should return pathway as free if paid card deleted' do
    cover = create(:card, organization_id: @org.id, author: @u, card_type: 'pack')
    card1 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card1', is_paid: true)
    card2 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card2', is_paid: false)

    create(:card_metadatum, plan: 'paid', card_id: card1.id)
    create(:card_metadatum, plan: 'free', card_id: card2.id)
    create(:card_metadatum, plan: 'paid', card_id: cover.id)

    CardPackRelation.add(cover_id: cover.id, add_id: card1.id, add_type: 'Card')
    CardPackRelation.add(cover_id: cover.id, add_id: card2.id, add_type: 'Card')

    assert_equal true, cover.is_paid_card

    card1.destroy
    UpdateCardMetadataJob.perform_now(cover.id)
    cover.reload
    assert_equal false, cover.is_paid_card
  end

  test 'should return journey as paid if any pathway within journey is paid' do
    journey = create(:card, card_type: 'journey', card_subtype: 'self_paced',  organization_id: @org.id, author: @u)
    pathway = create(:card, card_type: 'pack', card_subtype: 'simple',  organization_id: @org.id, author: @u)
    card1 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card1', is_paid: false)
    card2 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card2', is_paid: true)

    create(:card_metadatum, plan: 'free', card_id: card1.id)
    create(:card_metadatum, plan: 'paid', card_id: card2.id)
    create(:card_metadatum, plan: 'paid', card_id: pathway.id)

    CardPackRelation.add(cover_id: pathway.id, add_id: card1.id, add_type: 'Card')
    CardPackRelation.add(cover_id: pathway.id, add_id: card2.id, add_type: 'Card')
    JourneyPackRelation.add(cover_id: journey.id, add_id: pathway.id)

    UpdateCardMetadataJob.perform_now(journey.id)
    journey.reload
    assert_equal true, journey.is_paid_card
  end

  test 'should return journey as free if all pathways within journey are free' do
    journey = create(:card, card_type: 'journey', card_subtype: 'self_paced',  organization_id: @org.id, author: @u)
    pathway1 = create(:card, card_type: 'pack', card_subtype: 'simple',  organization_id: @org.id, author: @u)
    pathway2 = create(:card, card_type: 'pack', card_subtype: 'simple',  organization_id: @org.id, author: @u)

    card1 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card1', is_paid: false)
    card2 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card2', is_paid: false)

    create(:card_metadatum, plan: 'free', card_id: card1.id)
    create(:card_metadatum, plan: 'free', card_id: card2.id)
    create(:card_metadatum, plan: 'free', card_id: pathway1.id)
    create(:card_metadatum, plan: 'free', card_id: pathway2.id)

    CardPackRelation.add(cover_id: pathway1.id, add_id: card1.id, add_type: 'Card')
    CardPackRelation.add(cover_id: pathway2.id, add_id: card2.id, add_type: 'Card')
    JourneyPackRelation.add(cover_id: journey.id, add_id: pathway1.id)
    JourneyPackRelation.add(cover_id: journey.id, add_id: pathway2.id)

    UpdateCardMetadataJob.perform_now(journey.id)
    journey.reload
    assert_equal false, journey.is_paid_card
  end


  test 'should return journey as paid if we add a paid pathway in a free journey' do
    journey = create(:card, card_type: 'journey', card_subtype: 'self_paced',  organization_id: @org.id, author: @u)
    pathway1 = create(:card, card_type: 'pack', card_subtype: 'simple',  organization_id: @org.id, author: @u)
    pathway2 = create(:card, card_type: 'pack', card_subtype: 'simple',  organization_id: @org.id, author: @u)

    card1 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card1', is_paid: false)
    card2 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card2', is_paid: true)

    create(:card_metadatum, plan: 'free', card_id: card1.id)
    create(:card_metadatum, plan: 'paid', card_id: card2.id)
    create(:card_metadatum, plan: 'free', card_id: pathway1.id)
    create(:card_metadatum, plan: 'paid', card_id: pathway2.id)

    CardPackRelation.add(cover_id: pathway1.id, add_id: card1.id, add_type: 'Card')
    CardPackRelation.add(cover_id: pathway2.id, add_id: card2.id, add_type: 'Card')
    JourneyPackRelation.add(cover_id: journey.id, add_id: pathway1.id)
    assert_equal false, journey.is_paid_card

    JourneyPackRelation.add(cover_id: journey.id, add_id: pathway2.id)
    UpdateCardMetadataJob.perform_now(journey.id)
    journey.reload
    assert_equal true, journey.is_paid_card
  end


  test 'should return journey as free if paid pathway removed from a paid journey' do
    journey = create(:card, card_type: 'journey', card_subtype: 'self_paced',  organization_id: @org.id, author: @u)
    pathway1 = create(:card, card_type: 'pack', card_subtype: 'simple',  organization_id: @org.id, author: @u)
    pathway2 = create(:card, card_type: 'pack', card_subtype: 'simple',  organization_id: @org.id, author: @u)

    card1 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card1', is_paid: false)
    card2 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card2', is_paid: true)

    create(:card_metadatum, plan: 'free', card_id: card1.id)
    create(:card_metadatum, plan: 'paid', card_id: card2.id)
    create(:card_metadatum, plan: 'free', card_id: pathway1.id)
    create(:card_metadatum, plan: 'paid', card_id: pathway2.id)
    create(:card_metadatum, plan: 'paid', card_id: journey.id)

    CardPackRelation.add(cover_id: pathway1.id, add_id: card1.id, add_type: 'Card')
    CardPackRelation.add(cover_id: pathway2.id, add_id: card2.id, add_type: 'Card')
    JourneyPackRelation.add(cover_id: journey.id, add_id: pathway1.id)
    JourneyPackRelation.add(cover_id: journey.id, add_id: pathway2.id)

    assert_equal true, journey.is_paid_card

    JourneyPackRelation.remove_from_journey(cover_id: journey.id, remove_id: pathway2.id, destroy_hidden: false)

    UpdateCardMetadataJob.perform_now(journey.id)
    journey.reload
    assert_equal false, journey.is_paid_card
  end

  test 'should return journey as free if paid pathway deleted ' do
    journey = create(:card, card_type: 'journey', card_subtype: 'self_paced',  organization_id: @org.id, author: @u)
    pathway1 = create(:card, card_type: 'pack', card_subtype: 'simple',  organization_id: @org.id, author: @u)
    pathway2 = create(:card, card_type: 'pack', card_subtype: 'simple',  organization_id: @org.id, author: @u)

    card1 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card1', is_paid: false)
    card2 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card2', is_paid: true)

    create(:card_metadatum, plan: 'free', card_id: card1.id)
    create(:card_metadatum, plan: 'paid', card_id: card2.id)
    create(:card_metadatum, plan: 'free', card_id: pathway1.id)
    create(:card_metadatum, plan: 'paid', card_id: pathway2.id)
    create(:card_metadatum, plan: 'paid', card_id: journey.id)

    CardPackRelation.add(cover_id: pathway1.id, add_id: card1.id, add_type: 'Card')
    CardPackRelation.add(cover_id: pathway2.id, add_id: card2.id, add_type: 'Card')
    JourneyPackRelation.add(cover_id: journey.id, add_id: pathway1.id)
    JourneyPackRelation.add(cover_id: journey.id, add_id: pathway2.id)

    assert_equal true, journey.is_paid_card

    pathway2.destroy
    UpdateCardMetadataJob.perform_now(journey.id)
    journey.reload
    assert_equal false, journey.is_paid_card
  end

  test 'should return journey/pathway as free/paid if we update asspciated unpaid card as paid ' do
    journey = create(:card, card_type: 'journey', card_subtype: 'self_paced',  organization_id: @org.id, author: @u)
    pathway = create(:card, card_type: 'pack', card_subtype: 'simple',  organization_id: @org.id, author: @u)

    card1 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card1', is_paid: false)
    card2 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card2', is_paid: false)

    create(:card_metadatum, plan: 'free', card_id: card1.id)
    create(:card_metadatum, plan: 'free', card_id: card2.id)
    create(:card_metadatum, plan: 'free', card_id: pathway.id)
    create(:card_metadatum, plan: 'free', card_id: journey.id)

    CardPackRelation.add(cover_id: pathway.id, add_id: card1.id, add_type: 'Card')
    CardPackRelation.add(cover_id: pathway.id, add_id: card2.id, add_type: 'Card')
    JourneyPackRelation.add(cover_id: journey.id, add_id: pathway.id)

    assert_equal false, pathway.is_paid_card
    assert_equal false, journey.is_paid_card

    card1.update(is_paid: true)
    card1.card_metadatum.update(plan: 'paid')

    UpdateCardMetadataJob.perform_now(card1.id)
    journey.reload
    pathway.reload

    assert_equal 'free/paid', pathway.card_metadatum.plan
    assert_equal 'free/paid', journey.card_metadatum.plan
  end

  test 'should send notification when card is shared with individual' do
    org = create(:organization)
    author = create(:user, organization_id: org.id)
    card = create(:card, author: author)
    users = create_list(:user, 2, organization: org)
    CreateShareCardNotificationJob.expects(:perform_later).once
    card.share_card( user_ids:users.map(&:id) , user: author)
  end

  test 'should allow org admin to share to all teams' do
    team = create(:team, organization: @org)

    admin_user =  create(:user, organization: @org, organization_role: 'admin')
    member_user =  create(:user, organization: @org, organization_role: 'member')

    card = create(:card, is_public: true)

    assert_no_difference -> {SharedCard.count} do
      card.share_card(team_ids: [team.id], user: member_user)
    end

    assert_difference -> { SharedCard.count }, 1 do
      card.share_card(team_ids: [team.id], user: admin_user)
    end
  end

  # test 'should return paid/free for pathway based on combination of is_paid/card_metadatum' do
  #   #case1: card metadata not present
  #   cover = create(:card, organization_id: @org.id, author: @u, card_type: 'pack')
  #   card1 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card1', is_paid: false)
  #   card2 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card2', is_paid: true)
  #   CardPackRelation.add(cover_id: cover.id, add_id: card1.id, add_type: 'Card')
  #   CardPackRelation.add(cover_id: cover.id, add_id: card2.id, add_type: 'Card')
  #   assert_equal true, cover.is_paid_card

  #   #case2: card metadata present with free, premium, subscription
  #   cover = create(:card, organization_id: @org.id, author: @u, card_type: 'pack')
  #   card1 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card1', is_paid: false)
  #   card2 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card2', is_paid: false)
  #   card3 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card3', is_paid: false)
  #   create(:card_metadatum, plan: 'free', card_id: card1.id)
  #   create(:card_metadatum, plan: 'premium', card_id: card2.id)
  #   create(:card_metadatum, plan: 'subscription', card_id: card3.id)
  #   CardPackRelation.add(cover_id: cover.id, add_id: card1.id, add_type: 'Card')
  #   CardPackRelation.add(cover_id: cover.id, add_id: card2.id, add_type: 'Card')
  #   CardPackRelation.add(cover_id: cover.id, add_id: card3.id, add_type: 'Card')
  #   assert_equal true, cover.is_paid_card

  #   #case3: card metadata not present and is_paid is false
  #   cover = create(:card, organization_id: @org.id, author: @u, card_type: 'pack')
  #   card1 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card1', is_paid: false)
  #   card2 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card2', is_paid: false)
  #   CardPackRelation.add(cover_id: cover.id, add_id: card1.id, add_type: 'Card')
  #   CardPackRelation.add(cover_id: cover.id, add_id: card2.id, add_type: 'Card')
  #   assert_equal false, cover.is_paid_card

  #   #case4: card metadata present with all free
  #   cover = create(:card, organization_id: @org.id, author: @u, card_type: 'pack')
  #   card1 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card1', is_paid: false)
  #   card2 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card2', is_paid: false)
  #   create(:card_metadatum, plan: 'free', card_id: card1.id)
  #   create(:card_metadatum, plan: 'free', card_id: card2.id)
  #   CardPackRelation.add(cover_id: cover.id, add_id: card1.id, add_type: 'Card')
  #   CardPackRelation.add(cover_id: cover.id, add_id: card2.id, add_type: 'Card')
  #   assert_equal false, cover.is_paid_card
  # end

  # test 'should return paid/free for journey based on combination of is_paid/card_metadatum' do
  #   #case3: if journey,pathway,card metadata not present and any card's is_paid is true
  #   journey = create(:card, card_type: 'journey', card_subtype: 'self_paced',  organization_id: @org.id, author: @u)
  #   pathway = create(:card, card_type: 'pack', card_subtype: 'simple',  organization_id: @org.id, author: @u)

  #   card1 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card1', is_paid: false)
  #   card2 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card2', is_paid: true)
  #   CardPackRelation.add(cover_id: pathway.id, add_id: card1.id, add_type: 'Card')
  #   CardPackRelation.add(cover_id: pathway.id, add_id: card2.id, add_type: 'Card')
  #   JourneyPackRelation.add(cover_id: journey.id, add_id: pathway.id)
  #   assert_equal true, journey.is_paid_card

  #   #case4: if journey,pathway,card metadata not present and is_paid is false
  #   journey = create(:card, card_type: 'journey', card_subtype: 'self_paced',  organization_id: @org.id, author: @u, is_paid: false)
  #   pathway = create(:card, card_type: 'pack', card_subtype: 'simple',  organization_id: @org.id, author: @u, is_paid: false)
  #   card1 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card1', is_paid: false)
  #   card2 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card2', is_paid: false)
  #   CardPackRelation.add(cover_id: pathway.id, add_id: card1.id, add_type: 'Card')
  #   CardPackRelation.add(cover_id: pathway.id, add_id: card2.id, add_type: 'Card')
  #   JourneyPackRelation.add(cover_id: journey.id, add_id: pathway.id)
  #   assert_equal false, journey.is_paid_card

  #   #case5: card metadata present with free, premium, subscription and is_paid is false
  #   journey = create(:card, card_type: 'journey', card_subtype: 'self_paced',  organization_id: @org.id, author: @u, is_paid: false)
  #   pathway = create(:card, card_type: 'pack', card_subtype: 'simple',  organization_id: @org.id, author: @u, is_paid: false)
  #   card1 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card1', is_paid: false)
  #   card2 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card2', is_paid: false)
  #   card3 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card3', is_paid: false)
  #   create(:card_metadatum, plan: 'free', card_id: card1.id)
  #   create(:card_metadatum, plan: 'premium', card_id: card2.id)
  #   create(:card_metadatum, plan: 'subscription', card_id: card3.id)
  #   CardPackRelation.add(cover_id: pathway.id, add_id: card1.id, add_type: 'Card')
  #   CardPackRelation.add(cover_id: pathway.id, add_id: card2.id, add_type: 'Card')
  #   CardPackRelation.add(cover_id: pathway.id, add_id: card3.id, add_type: 'Card')
  #   JourneyPackRelation.add(cover_id: journey.id, add_id: pathway.id)
  #   assert_equal true, journey.is_paid_card

  #   #case6: card metadata present with all free
  #   journey = create(:card, card_type: 'journey', card_subtype: 'self_paced',  organization_id: @org.id, author: @u, is_paid: false)
  #   pathway = create(:card, card_type: 'pack', card_subtype: 'simple',  organization_id: @org.id, author: @u, is_paid: false)
  #   card1 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card1', is_paid: false)
  #   card2 = create(:card,  organization_id: @org.id, author: @u, card_type: 'media', message: 'card2', is_paid: false)
  #   create(:card_metadatum, plan: 'free', card_id: card1.id)
  #   create(:card_metadatum, plan: 'free', card_id: card2.id)
  #   CardPackRelation.add(cover_id: pathway.id, add_id: card1.id, add_type: 'Card')
  #   CardPackRelation.add(cover_id: pathway.id, add_id: card2.id, add_type: 'Card')
  #   JourneyPackRelation.add(cover_id: journey.id, add_id: pathway.id)
  #   assert_equal false, journey.is_paid_card
  # end

  test 'should not send notification for draft pathways' do
    org = create(:organization)
    user = create(:user, organization_id: org.id)
    cover = create(:card, author: user, organization_id: org.id, card_type: 'pack')
    notifier =  EDCAST_NOTIFY.notification_name_to_notifiers[Notify::EVENT_NEW_CARD]
    EDCAST_NOTIFY.expects(:trigger_event).with(Notify::EVENT_NEW_CARD, cover, {}).never
    cover.send_notification
  end

  test 'should not send notification for hidden cards' do
    card = create(:card, hidden: true)
    notifier =  EDCAST_NOTIFY.notification_name_to_notifiers[Notify::EVENT_NEW_CARD]
    EDCAST_NOTIFY.expects(:trigger_event).with(Notify::EVENT_NEW_CARD, card, {}).never
    card.send_notification
  end

  test 'should send notification for cards' do
    card1 = create(:card)
    EDCAST_NOTIFY.expects(:trigger_event).with(Notify::EVENT_NEW_CARD, card1, {event: Card::EVENT_NEW_CARD_SHARE}).once
    card1.send_notification
  end

  test 'should ensure author in org' do
    org1, org2 = create_list(:organization, 2)
    user = create(:user, organization: org1)
    card = build(:card, organization: org2, author_id: user.id)
    assert_not card.valid?
    assert card.errors.full_messages.include?("Organization User does not belong to organization")
  end

  test 'should sanitize resource title and description to not store any malicious html/scripts' do
    org = create(:organization)
    user = create(:user, organization: org)
    card =  create(:card, title: '<a style="background:#006;color:#fff;text-align:center;" href="http://someurl">this is amazing!</a>',
        message: '<span style="display:block;;background:red">Do check this out</span>')
    assert_not_equal '<span style="display:block;;background:red">Do check this out</span>', card.message
    assert_equal "<a href=\"http://someurl\">this is amazing!</a>",card.title
    assert_equal 'Do check this out',card.message
  end

  test 'should not sanitize card message only for a text card' do
    org = create(:organization)
    user = create(:user, organization: org)
    text_card =  create(:card, message: '<span style="display:block;;background:red">Do check this out</span>', card_subtype: 'text')

    assert_equal '<span style="display:block;;background:red">Do check this out</span>', text_card.message
  end

  test 'should serialize title and description' do
    str = "🖥️ FINISHING MY FIRST MACHINE LEARNING GAME! (3/4)"
    str_strip = "FINISHING MY FIRST MACHINE LEARNING GAME! (3/4)"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str_strip, Card.last.title
    assert_equal str_strip, Card.last.message
  end

  test 'should create Smartcards in supported languages (urdu)' do
    str = "یہ زبانی ترجمہ کے لئے ایک ٹیسٹ پیغام ہے"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (ukrainian)' do
    str = "це тестове повідомлення для мовного перекладу"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (turkish)' do
    str = "Bu, dil çevirisi için bir test mesajıdır"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (telugu)' do
    str = "ఇది భాష అనువాదం కోసం ఒక పరీక్ష సందేశం"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (tamil)' do
    str = "இது மொழி மொழிபெயர்ப்புக்கான ஒரு சோதனை செய்தியாகும்"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (swedish)' do
    str = "Detta är ett testmeddelande för språköversättning"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (spanish)' do
    str = "este es un mensaje de prueba para la traducción de idiomas"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (slovak)' do
    str = "toto je skúšobná správa pre jazykový preklad"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (serbian)' do
    str = "ово је тест порука за превод на језике"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (russian)' do
    str = "это тестовое сообщение для языкового перевода"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (romanian)' do
    str = "acesta este un mesaj de test pentru traducerea limbii"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (portuguese)' do
    str = "esta é uma mensagem de teste para tradução de idiomas"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (polish)' do
    str = "to jest wiadomość testowa do tłumaczenia językowego"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (marathi)' do
    str = "हा भाषेच्या भाषांतरासाठी एक चाचणी संदेश आहे"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (malay)' do
    str = "ini adalah mesej ujian untuk terjemahan bahasa"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (lithuanian)' do
    str = "tai bandymo kalba kalbos vertimui"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (Japanese)' do
    str = "これは言語翻訳のためのテストメッセージです"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (italian)' do
    str = "questo è un messaggio di prova per la traduzione della lingua"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (hungarian)' do
    str = "ez egy tesztüzenet a nyelvfordításhoz"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (hindi)' do
    str = "यह भाषा अनुवाद के लिए एक परीक्षण संदेश है"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (hebrew)' do
    str = "זוהי הודעת בדיקה עבור תרגום שפה"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (gujrati)' do
    str = "આ ભાષાનો અનુવાદ માટે એક પરીક્ષણ સંદેશ છે"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (greek)' do
    str = "αυτό είναι ένα μήνυμα δοκιμής για τη μετάφραση γλώσσας"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (german)' do
    str = "Dies ist eine Testnachricht für die Sprachübersetzung"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (french)' do
    str = "ceci est un message de test pour la traduction de la langue"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (dutch)' do
    str = "dit is een testbericht voor taalvertaling"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (czech)' do
    str = "toto je testovací zpráva pro překlad jazyka"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (chinese)' do
    str = "这是语言翻译的测试信息"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'should create Smartcards in supported languages (arabic)' do
    str = "هذه رسالة اختبار لترجمة اللغة"
    assert_nothing_raised do
      Card.create!(title: str, message: str, organization: Organization.first)
    end
    assert_equal str, Card.last.message
  end

  test 'assignment url for Cards where card_type is media' do
    card = create(:card)
    assert_match "/insights/#{card.slug}", card.assignment_url

    video_stream =  create(:wowza_video_stream)

    assert_match "/insights/#{video_stream.card.slug}", video_stream.card.assignment_url
  end

  test 'assignment url for Cards where card_type is pack' do
    card = create(:card, card_type: 'pack', )
    assert_match "/pathways/#{card.slug}", card.assignment_url
  end

  test 'assignment url for Cards where card_type is journey' do
    card = create(:card, card_type: 'journey', card_subtype: 'self_paced')
    assert_match "/journey/#{card.slug}", card.assignment_url
  end

  test 'to ensure we get a unique count of attempts and the expected percentage of correctness in assessments' do
    org = create(:organization)
    user = create(:user, organization_id: org.id)
    cover = create(:card, author: user, organization_id: org.id, card_type: 'pack')

    user1 = create(:user, organization_id: org.id)
    user2 = create(:user, organization_id: org.id)

    card = create(:card, card_type: 'poll', author: user, organization_id: org.id)
    option1 = create(:quiz_question_option, label: 'a', quiz_question: card, is_correct: true)
    option2 = create(:quiz_question_option, label: 'b', quiz_question: card)

    card1 = create(:card, card_type: 'poll', author: user, organization_id: org.id)
    option3 = create(:quiz_question_option, label: 'c', quiz_question: card1, is_correct: true)
    option4 = create(:quiz_question_option, label: 'd', quiz_question: card1)
    option5 = create(:quiz_question_option, label: 'e', quiz_question: card1)

    CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    CardPackRelation.add(cover_id: cover.id, add_id: card1.id, add_type: 'Card')

    attempt = create(:quiz_question_attempt, user: user1, quiz_question: card, selections: [option1.id])
    attempt2 = create(:quiz_question_attempt, user: user1, quiz_question: card1, selections: [option4.id])
    attempt1 = create(:quiz_question_attempt, user: user2, quiz_question: card, selections: [option2.id])
    attempt3 = create(:quiz_question_attempt, user: user2, quiz_question: card1, selections: [option4.id])

    attempt.run_callbacks(:commit)
    attempt1.run_callbacks(:commit)
    attempt2.run_callbacks(:commit)
    attempt3.run_callbacks(:commit)

    # to check if attempts are counted only once for every unique user
    assert_equal 2, cover.attempted_by.count
    assert_equal [user1.id, user2.id].to_set, cover.attempted_by.pluck(:id).to_set

    # since only 1 attempt selected the correct answer amongst all the quiz questions, correctness is 1/4
    assert_equal 25.0, cover.percentage_of_correctness
  end

  test 'should check if card is available publicly' do
    org = create(:organization)
    user = create(:user, organization: org)

    cards = create_list(:card, 2, organization: org, author: user)

    public_channel = create(:channel,  is_private: false, curate_only: false,  organization: org)
    private_channel = create(:channel,  is_private: true, curate_only: false,  organization: org)
    cards[0].channels << public_channel
    cards[1].channels << private_channel

    assert cards[0].publicly_visible?
    refute cards[1].publicly_visible?
  end

  test 'check if a card is shareable' do
    author = create :user, organization: @org, organization_role: 'member'
    member = create :user, organization: @org, organization_role: 'member'
    other_user = create :user, organization: @org, organization_role: 'member'
    admin = create :user, organization: @org, organization_role: 'admin'

    hidden_card = create(:card, hidden: true)
    public_card = create(:card, is_public: true, author: author)

    private_card = create(:card, is_public: false, author: author)
    private_card_with_restriction = create(:card, is_public: false, author: author)
    create(:card_user_permission, card_id: private_card_with_restriction.id, user_id: member.id)

    draft_card = create(:card, is_public: false, author: author, state: Card::STATE_DRAFT)

    #hidden card cannot be shared by member or admin
    assert_not hidden_card.shareable?(member)
    assert_not hidden_card.shareable?(admin)

    #public card can be shared by anyone
    assert public_card.shareable?(author)
    assert public_card.shareable?(member)
    assert public_card.shareable?(admin)

    #private card can be shared by author and admin only
    assert private_card.shareable?(author)
    assert private_card.shareable?(admin)
    assert_not private_card.shareable?(member)

    #private cards that are restricted can be shared by author/admin/and users its restricted to through team or directly
    assert private_card_with_restriction.shareable?(member)
    assert_not private_card_with_restriction.shareable?(other_user)

    #Card in draft state cannot be shared by anyone
    assert_not draft_card.shareable?(member)
    assert_not draft_card.shareable?(author)
    assert_not draft_card.shareable?(admin)
  end

  test 'should create tags for cards with tags' do
    @card.update_attributes(title: 'a title with a #cool tag')
    @card.update_tags
    assert_equal 1, @card.tags.count
    assert_equal 'cool', @card.tags[0].name
    @card.update_attributes('title': '')
    @card.reload
    @card.update_tags
    assert_equal 0, @card.tags.count
    @card.update_attributes(message: '#a #b #c')
    @card.reload
    @card.update_tags
    assert_equal 3, @card.tags.count
  end

  test 'non_curated_channel_ids should return channel ids where card is not curated' do
    channels_list = create_list(:channel, 5, curate_only: true, organization_id: @u.organization_id)
    card = create(:card, organization_id: @u.organization_id, author: nil)
    channels_list.each do |ch|
      create(:channels_card, channel_id: ch.id, card_id: card.id, state: ChannelsCard::STATE_NEW)
    end
    assert_equal channels_list.map(&:id).sort, card.non_curated_channel_ids.sort
    assert_equal 5, channels_list.count
  end

  test 'scorm course card should have mark_as_complete disabled if org_config found' do
    create(:config, name: 'auto_mark_as_complete_scorm_cards', value: 'true', configable: @u.organization)
    author = create(:user, organization_id: @u.organization_id)
    card = create(:card, organization_id: @u.organization_id, author: author, filestack: [{scorm_course: true}])
    assert card.mark_feature_disabled?
  end

  test 'scorm course card should not disbale mark_as_complete if no org_config found' do
    author = create(:user, organization_id: @u.organization_id)
    card = create(:card, organization_id: @u.organization_id, author: author, filestack: [{scorm_course: true}])
    refute card.mark_feature_disabled?
  end

  test 'should return true if scorm card' do
    author = create(:user, organization_id: @u.organization_id)
    card = create(:card, organization_id: @u.organization_id, author: author, filestack: [{scorm_course: true}])
    assert card.is_scorm_card?
  end

  test 'should return false if not a scorm card' do
    author = create(:user, organization_id: @u.organization_id)
    card = create(:card, organization_id: @u.organization_id, author: author)
    refute card.is_scorm_card?
  end

  test 'scorm card should show in activity stream when transitioned from processing to published state' do
    author = create(:user, organization_id: @u.organization_id)
    card = create(:card, organization_id: @u.organization_id, author: author, state: 'processing', filestack: [{scorm_course: true}])
    ActivityStreamCreateJob.expects(:perform_later).once
    card.publish!
  end

  test 'should return false if the scorm card resource is not a scorm url' do
    author = create(:user, organization_id: @u.organization_id)
    card = create(:card, organization_id: @u.organization_id, author: author,
                  resource: build(:resource, url: 'https://something.com/else'))
    refute card.is_scorm_card?
  end

  test 'should return true if the scorm card resource is a scorm url' do
    author = create(:user, organization_id: @u.organization_id)
    card = create(:card, organization_id: @u.organization_id, author: author,
                  resource: build(:resource, url: 'https://www.edcast.com/api/scorm/launch?course_id'))
    assert card.is_scorm_card?
  end

  test 'should return false if the scorm card resource url is null' do
    author = create(:user, organization_id: @u.organization_id)
    resource = create(:resource, url: 'https://www.edcast.com/api/scorm/launch?course_id')
    card = create(:card, organization_id: @u.organization_id, author: author, resource: resource)

    # Expect url to be null, since this only happens for ECL cards
    resource.expects(:url).returns(nil)

    refute card.is_scorm_card?
  end

  test '#paid_by_user should return true if user has paid for card subscription' do
    skip 'Test intermittent failure on travis, on local working fine'
    CardSubscription.any_instance.stubs(:send_email_notification)
    organization = create(:organization)
    user = create(:user, organization: organization)
    author = create(:user, organization: organization)
    card = create(:card, organization_id: organization.id, is_paid: true, author: author)
    create(:card_subscription, card: card, user: user, organization: organization)
    assert card.paid_by_user?(user)
  end

  test "#paid_by_user should return false if user's card subscription has expired" do
    CardSubscription.any_instance.stubs(:send_email_notification)
    organization = create(:organization)
    user = create(:user, organization: organization)
    author = create(:user, organization: organization)
    card = create(:card, organization_id: organization.id, is_paid: true, author: author)
    create(:card_subscription, card: card, user: user, organization: organization, end_date: Date.today - 2)

    refute card.paid_by_user?(user)
  end

  test 'should return a hash of count of correct and wrong answers to a poll question' do
    QuizQuestionStatsRecorderJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    org = create(:organization)
    user = create(:user, organization_id: org.id)
    user1 = create(:user, organization_id: org.id)
    user2 = create(:user, organization_id: org.id)

    cover = create(:card, organization_id: org.id, author: user, card_type: 'pack')

    card = create(:card, card_type: 'poll', author: user, organization_id: org.id)
    option1 = create(:quiz_question_option, label: 'a', quiz_question: card, is_correct: true)
    option2 = create(:quiz_question_option, label: 'b', quiz_question: card)

    card1 = create(:card, card_type: 'poll', author: user, organization_id: org.id)
    option3 = create(:quiz_question_option, label: 'c', quiz_question: card1, is_correct: true)
    option4 = create(:quiz_question_option, label: 'd', quiz_question: card1)
    option5 = create(:quiz_question_option, label: 'e', quiz_question: card1)

    # pathway = create(:card_pack_relation, cover: cover)
    CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    CardPackRelation.add(cover_id: cover.id, add_id: card1.id, add_type: 'Card')

    attempt = create(:quiz_question_attempt, user: user1, quiz_question: card, selections: [option1.id])
    attempt2 = create(:quiz_question_attempt, user: user1, quiz_question: card1, selections: [option3.id])
    attempt1 = create(:quiz_question_attempt, user: user2, quiz_question: card, selections: [option1.id])
    attempt3 = create(:quiz_question_attempt, user: user2, quiz_question: card1, selections: [option4.id])

    attempt.run_callbacks(:commit)
    attempt1.run_callbacks(:commit)
    attempt2.run_callbacks(:commit)
    attempt3.run_callbacks(:commit)

    assert_equal(
      { 'correct_answers' => 3, 'wrong_answers' => 1, 'total_answers_count' => 4 },
      cover.correct_attempts_count(cover.poll_attempts.pluck(:quiz_question_id))
    )
  end

  test 'indexing map should work' do
    Role.create_master_roles @org
    User.any_instance.unstub :update_role
    @card.author.send :update_role

    user = create(:user, organization: @org)
    create(:vote, votable: @card, voter: user)
    mu1, mu2 = create_list(:user, 2, organization: @org)
    @card.mentioned_users << mu1
    @card.mentioned_users << mu2
    ch = create(:channel, :robotics, organization: @org)
    @card.channels << ch
    indexing_map = @card.indexing_data
    expected_map = {
                    'content' => {
                      'title' => 'this is title',
                      'message' => 'this is message'
                    },
                    'card_template' => @card.card_subtype,
                    'comments' => [],
                    'handle' => @u.handle,
                    'tags' => [],
                    'like_count' => 1,
                    'mentioned_users' => [{'id' => mu1.id, 'first_name' => mu1.first_name, 'last_name' => mu1.last_name, 'name' => mu1.name, 'handle' => mu1.handle, 'photo' => mu1.photo, 'photo_medium' => mu1.photo(:medium), 'picture' => mu1.photo(:medium), 'bio' =>  mu1.bio, 'is_suspended' => mu1.is_suspended },
                                          {'id' => mu2.id, 'first_name' => mu2.first_name, 'last_name' => mu2.last_name, 'name' => mu2.name, 'handle' => mu2.handle, 'photo' => mu2.photo, 'photo_medium' => mu2.photo(:medium), 'picture' => mu2.photo(:medium), 'bio' => mu2.bio, 'is_suspended' => mu2.is_suspended }],
                    'author' => {'id' => @u.id, 'first_name' => @u.first_name, 'last_name' => @u.last_name, 'name' => @u.name, 'handle' => @u.handle, 'photo' => @u.photo, 'photo_medium' => @u.photo(:medium), 'picture' => @u.photo(:medium), 'influencer' => false, 'followers_count' => 0, 'bio' => @u.bio, 'is_suspended' => @u.is_suspended, 'roles' => ['member'], 'organization_role' => 'member'},
                    'comments_count' => 0,
                    'file_resources' => [],
                    'quiz_question_options' => [],
                    'quiz_question_stats' => nil,
                    'quiz_question_attempts' => [],

                    'slug' => @card.slug,
                    'in_private_channel' => false,
                    'packs' => [],
                    'channel_ids' => [ch.id],
                    'curated_channel_ids' => [ch.id],
                    'pack_cards' => [],
                    'author_name' => @card.author_name,
                    'video_stream_id' => nil,
                    'author_skill_level' => nil,
                    'liked_by' => @card.liked_by.map(&:mini_profile_data),
                    'pack_ids' => [],
                    'resource' => nil,
                    'channels' => @card.channels.as_json(only:[:id, :label]),
                    'average_rating' => 0,
                    'users_with_access_ids' => [],
                    'users_with_permission_ids' => [],
                    'teams_with_permission_ids' => [],
                    'shared_with_teams_ids' => []
    }
    assert_equal expected_map, indexing_map.except('actions'),
                 "diff #{HashDiff.diff(expected_map, indexing_map)}"
  end

  test 'indexing_data should have average_rating' do
    TestAfterCommit.with_commits(true) do
      card = create :card
      cards_rating1 = create :cards_rating, card: card, rating: 2
      cards_rating2 = create :cards_rating, card: card, rating: 4
      indexed_data = card.indexing_data
      assert_equal 3, indexed_data['average_rating']
    end
  end

  test 'should return all Journey_ids of which a smartcard is a part of' do
    smartcard1 = create :card
    pack_cards = create_list(:card, 2, card_type: 'pack', card_subtype: 'simple')
    journey_cards = create_list(:card, 2, card_type: 'journey', card_subtype: 'self_paced')
    create(:card_pack_relation, cover: pack_cards[0], from:smartcard1)
    create(:card_pack_relation, cover: pack_cards[1], from:smartcard1)
    JourneyPackRelation.add(cover_id: journey_cards[0].id, add_id: pack_cards[0].id)
    JourneyPackRelation.add(cover_id: journey_cards[1].id, add_id: pack_cards[1].id)
    assert_equal [journey_cards[0].id,journey_cards[1].id], smartcard1.journey_ids_for_card
  end

  test 'should return all Journey_ids of which a pathway is a part of.' do
    pack_cards = create_list(:card, 2, card_type: 'pack', card_subtype: 'simple')
    journey_cards = create_list(:card, 3, card_type: 'journey', card_subtype: 'self_paced')
    JourneyPackRelation.add(cover_id: journey_cards[0].id, add_id: pack_cards[0].id)
    JourneyPackRelation.add(cover_id: journey_cards[1].id, add_id: pack_cards[0].id)
    JourneyPackRelation.add(cover_id: journey_cards[2].id, add_id: pack_cards[1].id)

    assert_equal [journey_cards[0].id,journey_cards[1].id], pack_cards[0].journey_ids_for_pack
    assert_equal [journey_cards[2].id], pack_cards[1].journey_ids_for_pack
  end

  test 'access control for open and closed orgs' do
    org1, org2 = create_list(:organization, 2)
    org1.update_columns(is_open: true)

    org1_user = create(:user, organization: org1)
    org2_user = create(:user, organization: org2)

    org1_card = create(:card, card_type: 'media', card_subtype: 'text', author_id: org1_user.id).search_data
    org2_card = create(:card, card_type: 'media', card_subtype: 'text', author_id: org2_user.id).search_data

    # open to world

    [nil, org1_user, org2_user].each {|u| assert Card.has_reading_rights?(org1_card, u)}
    assert_not Card.has_reading_rights?(org2_card, nil)
    assert_not Card.has_reading_rights?(org2_card, org1_user)
    assert Card.has_reading_rights?(org2_card, org2_user)
  end

  test 'index images using resource' do
    resource = create(:resource)
    file_resource = create(:file_resource, attachable: resource)
    @card.update resource: resource
    indexing_data = @card.indexing_data
    assert_equal file_resource.attachment.url(:small), indexing_data['small_image_url']
    assert_equal file_resource.attachment.url(:medium), indexing_data['medium_image_url']
    assert_equal file_resource.attachment.url(:large), indexing_data['large_image_url']
  end

  test 'index images using attached file resources' do
    file_resource = create(:file_resource, attachable: @card)
    indexing_data = @card.indexing_data
    assert_equal file_resource.attachment.url(:small), indexing_data['small_image_url']
    assert_equal file_resource.attachment.url(:medium), indexing_data['medium_image_url']
    assert_equal file_resource.attachment.url(:large), indexing_data['large_image_url']
  end

  test 'search data should work' do
    search_data = @card.search_data
    expected_data = @card.indexing_data.merge({
                    'client_id' => nil,
                    'client_item_id' => nil,
                    'card_type' => @card.card_type,
                    'card_subtype' => @card.card_subtype,
                    'id' => @card.id,
                    'is_public' => @card.is_public,
                    'author_id' => @card.author_id,
                    'author_first_name' => @card.author_first_name,
                    'author_last_name' => @card.author_last_name,
                    'author_picture_url' => @card.author_picture_url,
                    'super_card_id' => @card.super_card_id,
                    'cards_config_id' => nil,
                    'comments_count' => @card.comments_count,
                    'is_manual' => false,
                    'order_index' => @card.order_index,
                    'answers_count' => 0,
                    'expansions_count' => 0,
                    'impressions_count' => 0,
                    'outbounds_count' => 0,
                    'plays_count' => 0,
                    'shares_count' => 0,
                    'hidden' => false,
                    'resource_id' => nil,
                    'liked_by' => [],
                    'features' => nil,
                    'group_id' => nil,
                    'organization_id' => @card.organization_id,
                    'state' => 'published',
                    'is_official' => false,
                    'votes_count' => @card.votes_count,
                    'title' => @card.title,
                    'message' => @card.message,
                    'ecl_id' => nil,
                    'promoted_at' => nil,
                    'ecl_metadata' => nil,
                    'taxonomy_topics' => [],
                    'bookmarks_count' => nil,
                    'ecl_source_name' => nil,
                    'provider' => nil,
                    'provider_image' => nil,
                    'readable_card_type' => nil,
                    'user_taxonomy_topics' => [],
                    'duration' => nil,
                    "language"=>nil,
                    'author_skill_level' => nil,
                    'authoritative_all_time_views_count' => nil
    })

    assert_equal expected_data, search_data.except('created_at', 'updated_at', 'published_at', 'deleted_at'), "diff #{HashDiff.diff(expected_data, search_data.except('created_at', 'updated_at', 'published_at'))}"
  end

  test 'extract mentions on a card' do
    org = create(:organization)
    card = create(:card, card_type: 'media', card_subtype: 'text', author: create(:user, organization: org))
    u1 = create(:user, handle: '@simple-handle', organization: org)
    u2 = create(:user, handle: '@not-so-simple-handle123', organization: org)
    u3 = create(:user, handle: '@title-mention', organization: org)
    card.update_attributes(message: "some text #{u1.handle} some more text #{u2.handle}", title: "some text #{u3.handle} non existant @handle-non-existant")

    card.add_mentions

    assert_same_elements [u1, u2, u3], card.mentioned_users
  end

  test 'generates appropriate slugs' do
    skip
    # default slug
    card = create(:card)
    assert_match /\Acard-([a-z0-9]+\-){4}[a-z0-9]+\z/, card.slug
    assert_equal card, Card.friendly.find(card.slug)

    # title-based slug on save (simulates CardsApi.cards_create process)
    card.title = 'this title'
    card.save!
    assert_equal 'this-title', card.slug
    assert_equal card, Card.friendly.find(card.slug)

    # title and tag
    card = create(:card)
    card.title = 'this title'
    card.tags << create(:tag, name: 'taggy')
    card.save!
    assert_equal 'this-title-taggy', card.slug

    # title and more tags
    card = create(:card)
    card.title = 'this title'
    %w[tagone tagtwo].each {|tag| card.tags << create(:tag, name: tag)}
    card.save!
    assert_match /this-title-(tagone-tagtwo|tagtwo-tagone)/, card.slug

    # title and same tags
    card = create(:card)
    card.title = 'this title'
    card.tags = Tag.last(2)
    card.save!
    assert_match /this-title-(tagone-tagtwo|tagtwo-tagone)/, card.slug

    # title and message
    card = create(:card)
    card.title = 'this title'
    card.message = 'mess age'
    card.tags = Tag.where(name: 'taggy')
    card.save!
    assert_equal 'this-title-mess', card.slug

    # title and more message
    card = create(:card)
    card.title = 'this title'
    card.message = 'mess age'
    card.save!
    assert_equal 'this-title-mess-age', card.slug
    old_slug = card.slug

    # title change
    card.title = 'new title'
    card.regenerate_slug
    assert_equal 'new-title', card.slug

    # previous slug still works
    assert_equal card, Card.friendly.find(old_slug)

    # last resort uuid
    card = create(:card)
    card.title = 'this title'
    card.save!
    assert_match /\Athis-title-card-([a-z0-9]+\-){4}[a-z0-9]+\z/, card.slug

    # truncates long title
    card = create(:card)
    title = 'this is a really long title that exceeds sixty characters and therefore will be truncated'
    card.title = title
    card.save!
    assert_equal 60, card.slug.length

    # truncates long slug
    slug = SecureRandom.urlsafe_base64(300)
    assert slug.length > 300
    Card.any_instance.stubs(:slug_candidates).returns([slug])
    card = create(:card)
    assert_equal 191, card.slug.length
  end

  test 'has reading rights' do
    Organization.any_instance.unstub :create_general_channel
    @org.send :create_general_channel

    c = create(:card, card_type: 'media', card_subtype: 'text', author: create(:user, organization: @org))
    c_indx = c.search_data
    refute Card.has_reading_rights?(c_indx, nil)

    u = create(:user, organization: @org)
    assert Card.has_reading_rights?(c_indx, u)

    # Still public
    pubc = create(:channel, is_private: false, organization: @org)
    c.channels << pubc
    c_indx = c.search_data
    refute Card.has_reading_rights?(c_indx, nil)
    assert Card.has_reading_rights?(c_indx, u)

    # now a private channel
    pric = create(:card, card_type: 'media', card_subtype: 'text', author: create(:user, organization: @org))
    prich = create(:channel, is_private: true, organization: @org)
    pric.channels << prich
    c_indx = pric.search_data
    assert_not Card.has_reading_rights?(c_indx, nil)
    assert_not Card.has_reading_rights?(c_indx, u)

    create(:follow, followable: prich, user: u)
    assert Card.has_reading_rights?(c_indx, u)
  end

  test "poll_card?" do
    c = build(:card, card_type: 'poll', card_subtype: 'text', is_public: true)
    assert c.poll_card?
  end

  test 'should remove from queue on archive' do
    c = create(:card)
    ContentsBulkDequeueJob.expects(:perform_later).with(
        content_type: 'Card',
        content_ids: [c.id]
    ).returns(nil).once
    c.archive!
  end
  test "question_attempted?" do
    c = create(:card, card_type: 'poll', card_subtype: 'text', is_public: true)
    assert_not c.question_attempted?
  end

  test 'remove_notifications' do
    card = create(:card, card_type: 'poll', card_subtype: 'text', is_public: true)
    notification = create(:notification, notifiable: card)
    card.stubs(:remove_card_from_queue).returns(true)
    assert_difference -> { Notification.count }, -1 do
      assert card.destroy
    end

    card2 = create(:card, card_type: 'media')
    card2.stubs(:remove_card_from_queue).returns(true)
    notification = create(:notification, notifiable: card2)
    assert_difference -> {Notification.count}, -1 do
      assert card2.destroy
    end
  end

  test 'liked_by' do
    c = create(:card, card_type: 'media', card_subtype: 'text')
    u1 = create(:user)
    u2 = create(:user)
    cu = create(:vote, voter: u1, votable: c)
    assert_equal c.liked_by, [u1]

    assert_equal c.liked_by.include?(u2), false
  end

  test "indexing map should include pack cards data" do
    Card.any_instance.stubs(:reindex).returns true
    cover = create(:card, card_type: 'pack', card_subtype: 'simple', author_id: @u.id)
    card3, card2, card1 = create_list(:card, 3)

    [card1, card2, card3].each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: card.class.name)
    end

    indexing_map = cover.indexing_data
    assert_equal 3, indexing_map["pack_cards"].count
    assert_equal [card1.id, card2.id, card3.id], indexing_map["pack_cards"].map {|c| c["card_id"]}
    assert_equal [card3.title, card2.title, card1.title], indexing_map["pack_cards"].map {|c| c["title"]}
  end

  test 'should remove clc records on card delete' do
    org = create(:organization)
    users = create_list(:user,2)
    card = create(:card, card_type: 'media', card_subtype: 'text')
    users.each do |user|
      clc_records = create(:clc_organizations_record, organization_id: org.id, card_id: card.id, user:user)
    end

    card.destroy
    assert ClcOrganizationsRecord.where(card_id: card.id).blank?#card.clc_organizations_records.blank?
  end

  test '#redirect_page_url must return url for standalone page' do
    org = create(:organization, host_name: 'test')
    user = create(:user, organization: org)
    card = create(:card, organization: org, author: user, slug: 'test-card')

    assert_equal card.redirect_page_url(org: org), 'http://test.test.host/insights/test-card'
  end

  test '#remove_from_filestack remove old filestack contents' do
    skip('PENDING')

    RemoveFileFromFilestackJob.expects(:perform_later).once

    filestack = {
      "filename": "Strix_nebulosaRB.jpg",
      "mimetype": "image/jpeg",
      "size": 106176,
      "source": "local_file_system",
      "url": "https://cdn.filestackcontent.com/yT4BBpg7T5efeiriQv37",
      "handle": "yT4BBpg7T5efeiriQv37",
      "status": "Stored",
      "key": "VYgpc6fnTOKJkFWIkzti_Strix_nebulosaRB.jpg",
      "container": "your_bucket"
    }

    card = create :card, card_type: 'media', card_subtype: 'image', filestack: [filestack]

    filestack.merge!('url' => "yT4BBpg7T5efeiriQv48", "url": "https://cdn.filestackcontent.com/yT4BBpg7T5efeiriQv48")
    card.update_attributes(filestack: [filestack])

    card.remove_from_filestack
  end

  test '#remove_from_filestack ignore stock images' do
    skip('PENDING')

    RemoveFileFromFilestackJob.expects(:perform_later).never

    # pick one image randomly from stock images.
    filestack = {
      "mimetype": "image/jpeg",
      "size": 95599,
      "source": "local_file_system",
      "url": "https://cdn.filestackcontent.com/2mxSABeTQQ2gYi3i2pCf",
      "handle": "2mxSABeTQQ2gYi3i2pCf",
      "status": "Stored"
    }

    card = create :card, card_type: 'media', card_subtype: 'image', filestack: [filestack]

    # pretend image is being updated.
    filestack.merge!('url' => "yT4BBpg7T5efeiriQv48", "url": "https://cdn.filestackcontent.com/yT4BBpg7T5efeiriQv48")
    card.update_attributes(filestack: [filestack])

    card.remove_from_filestack
  end

  test 'card scope published_pathways should return all published pathways as well as advanced pathways that is curriculum' do
      author = create(:user, organization: @org)
      user = create(:user, organization: @org)
      pathway1 = create(:card, card_type: 'pack', card_subtype: 'curriculum', author: author, state: "draft")
      pathway2 = create(:card, card_type: 'pack', card_subtype: 'simple', author: author,state: "draft")
      pathway3 = create(:card, card_type: 'pack', card_subtype: 'simple', author: author,state: "draft")

      journey = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: author)
      card1, card2 = create_list(:card, 2, author: author)

      # link journey to pathway (curriculum)
      pathway1.add_card_to_pathway(journey.id, "Card")
      # link card1 to pathway2
      pathway2.add_card_to_pathway(card1.id, 'Card')
      # link card2 to pathway3
      pathway3.add_card_to_pathway(card2.id, 'Card')
      pathway1.update(state: 'published')
      pathway2.update(state: 'published')
      assert_equal 2, Card.all.published_pathways.length
  end

  class AuthorizeCourseTest < ActiveSupport::TestCase
    setup do
      WebMock.enable!
      Transaction.any_instance.stubs(:create_transaction_audit).returns(true)
    end

    teardown do
      WebMock.disable!
    end
    test "#authorize_course should call content provider's authorization service with proper parameters" do
      payload = { user: { email: 'abc@xyz.com',
                          first_name: 'abc',
                          last_name: 'def' },
                  card: { url: 'https://fake-domain-name.com/courses/1234/redirect',
                          client_resource_id: 1234,
                          external_id: "123456",
                          app_id: 1 },
                  payment: { amount: BigDecimal.new("100"),
                             currency: 'INR' } }
      headers = { 'Content-Type': 'application/json',
                  'Accept': 'application/json',
                  'Accept-Encoding': 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                  'User-Agent'=>'Ruby' }

      params = {data: payload, auth_token: 'abcd-1234'}
      ContentProvider::AuthorizeCourse.any_instance.stubs(:auth_token).returns('abcd-1234')
      stub_request(:post, "https://test.edcast.me/courses/unlock").with(:body => params.to_json, :headers => headers)
                                                                  .to_return(:status => 200, :body => "", :headers => {})

      source_credential = create :sources_credential, source_id: 'non-ugc-source-123',
                                                      auth_api_info: {url: '/courses/unlock',
                                                                      path: "/api/v2/auth",method: "post",
                                                                      host: "https://test.edcast.me"}
      org = create(:organization, savannah_app_id: 1)
      author = create(:user, organization_id: org.id)
      user = create(:user, email: 'abc@xyz.com', first_name: 'abc', last_name: 'def')
      resource = create(:resource, url: "https://fake-domain-name.com/courses/1234/redirect")
      card = create(:card, resource: resource, ecl_metadata: {source_id: 'non-ugc-source-123', external_id: '123456'},
                           organization:org, author: author)
      order = create(:order, orderable: card)
      card_transaction = create(:transaction, user: user, amount: BigDecimal.new("100"), currency: 'INR', order: order)

      assert card.authorize_course(card_transaction: card_transaction, user: user)
    end
  end

  test '#lxp_payment_enabled should return false if there is no configuration in Source credentials' do
    card = create(:card, ecl_metadata: {source_id: 'test-source-id'})
    refute card.lxp_payment_enabled?
  end

  test '#lxp_payment_enabled should return true if auth_api_info is present in Source credentials' do
    org = create(:organization)
    user = create(:user, organization: org)
    card = create(:card, ecl_metadata: {source_id: 'test-source-id'}, organization_id: org.id, author: user)
    source_cred1 = create :sources_credential, source_id: 'test-source-id', auth_api_info: {auth_required: false},
                                               organization_id: org.id
    assert card.lxp_payment_enabled?
  end

  class CardCompletionPercentTest < ActiveSupport::TestCase
    setup do
      @org = create(:organization)
      Card.any_instance.stubs(:reindex)
    end

    teardown do
      Card.any_instance.unstub(:reindex)
    end

    test "should return completion percentage for card" do
      card = create(:card, state: 'published', author: create(:user, organization: @org))
      card1 = create(:card, state: 'published', author: create(:user, organization: @org))
      user = create(:user, organization: @org)
      create(:user_content_completion, :completed, completable: card, user: user)
      assert_equal 100, card.completed_percent(user)
      assert_equal 0, card1.completed_percent(user)
    end

    test "should return completed_percent for pathway" do
      cover = create(:card, card_type: 'pack', card_subtype: 'simple', author: create(:user, organization: @org))
      user = create(:user, organization: @org)
      card4, card3, card2, card1 = create_list(:card, 4, author: create(:user, organization: @org))

      [card4, card3, card2, card1].each do |card|
        cover.add_card_to_pathway(card.id, "Card")
      end

      create(:user_content_completion, :completed, completable: card1, user: user)
      assert_equal 25, cover.completed_percent(user)

      [card4, card3, card2].each do |card|
        create(:user_content_completion, :completed, completable: card, user: user)
      end

      assert_equal 98, cover.completed_percent(user)

      create(:user_content_completion, :completed, completable: cover, user: user)
      assert_equal 100, cover.completed_percent(user)
    end

    test "should return correct completed_percent for journey single pathway single card journey" do
      cover = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: create(:user, organization: @org))
      user = create(:user, organization: @org)
      section1 = create(:card, card_type: "pack", card_subtype: "simple", author: create(:user, organization: @org))
      card1 = create(:card, author: create(:user, organization: @org))

      cover.add_card_to_journey(section1.id)

      section1.add_card_to_pathway(card1.id, "Card")

      assert_equal 0, cover.completed_percent(user)

      create(:user_content_completion, :completed, completable: card1, user: user)
      assert_equal 98, cover.completed_percent(user)

      create(:user_content_completion, :completed, completable: cover, user: user)
      assert_equal 100, cover.completed_percent(user)
    end

    test "should return correct completed_percent for journey 2 pathway journey" do
      cover = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: create(:user, organization: @org))
      user = create(:user, organization: @org)
      section2, section1 = create_list(:card, 2, card_type: "pack", card_subtype: "simple", author: create(:user, organization: @org))
      card2, card1 = create_list(:card, 2, author: create(:user, organization: @org))

      [section2, section1].each do |section|
        cover.add_card_to_journey(section.id)
      end

      section2.add_card_to_pathway(card2.id, "Card")
      section1.add_card_to_pathway(card1.id, "Card")

      assert_equal 0, cover.completed_percent(user)

      create(:user_content_completion, :completed, completable: card1, user: user)
      assert_equal 50, cover.completed_percent(user)

      create(:user_content_completion, :completed, completable: card2, user: user)
      assert_equal 98, cover.completed_percent(user)

      create(:user_content_completion, :completed, completable: cover, user: user)
      assert_equal 100, cover.completed_percent(user)
    end

    test "should return correct completed_percent for multiple pathway multiple cards journey" do
      cover = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: create(:user, organization: @org))
      user = create(:user, organization: @org)
      section3, section2, section1 = create_list(:card, 3, card_type: "pack", card_subtype: "simple", author: create(:user, organization: @org))
      card6, card5, card4, card3, card2, card1 = create_list(:card, 6, author: create(:user, organization: @org))

      [section3, section2, section1].each do |section|
        cover.add_card_to_journey(section.id)
      end

      section3.add_card_to_pathway(card6.id, "Card")
      section3.add_card_to_pathway(card5.id, "Card")
      section3.add_card_to_pathway(card4.id, "Card")
      section3.add_card_to_pathway(card3.id, "Card")
      section2.add_card_to_pathway(card2.id, "Card")
      section1.add_card_to_pathway(card1.id, "Card")

      #completed percentage should 0 when no cards are completed
      assert_equal 0, cover.completed_percent(user)

      #completed percentage should 33 when 1st pathway is completed
      create(:user_content_completion, :completed, completable: card1, user: user)
      assert_equal 33, cover.completed_percent(user)

      #completed percentage should 66 when 2 pathways are completed
      create(:user_content_completion, :completed, completable: card2, user: user)
      assert_equal 66, cover.completed_percent(user)

      #completed percentage should 75 pathway completed and 3rd pathways 1st card completed
      create(:user_content_completion, :completed, completable: card3, user: user)
      assert_equal 75, cover.completed_percent(user)

      #completed percentage should 83 pathway completed and 3rd pathways 2 card completed
      create(:user_content_completion, :completed, completable: card4, user: user)
      assert_equal 83, cover.completed_percent(user)

      #completed percentage should 91 pathway completed and 3rd pathways 3 card completed
      create(:user_content_completion, :completed, completable: card5, user: user)
      assert_equal 91, cover.completed_percent(user)

      #completed percentage should 98 all the pathways are completed
      create(:user_content_completion, :completed, completable: card6, user: user)
      assert_equal 98, cover.completed_percent(user)

      #completed percentage should 100 when journey is completed
      create(:user_content_completion, :completed, completable: cover, user: user)
      assert_equal 100, cover.completed_percent(user)
    end

    test "returns correct completed_percent for curriculum (journeys linked to a pathway)" do
      author = create(:user, organization: @org)
      user = create(:user, organization: @org)
      pathway = create(:card, card_type: 'pack', card_subtype: 'journey', author: author)
      journey = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: author)
      journey_pack = create(:card, card_type: "pack", card_subtype: "simple", author: author)
      card3, card2, card1 = create_list(:card, 3, author: author)

      # link journey to pathway (curriculum)
      pathway.add_card_to_pathway(journey.id, "Card")
      # Curriculum pathway can also hold single card
      pathway.add_card_to_pathway(card3.id, 'Card')

      # link pathway to journey
      journey.add_card_to_journey(journey_pack.id)

      # link cards to pathway
      journey_pack.add_card_to_pathway(card1.id, "Card")
      journey_pack.add_card_to_pathway(card2.id, "Card")

      assert_equal 0, pathway.completed_percent(user)

      create(:user_content_completion, :completed, completable: card1, user: user)
      assert_equal 25, pathway.completed_percent(user)

      create(:user_content_completion, :completed, completable: card2, user: user)
      assert_equal 49, pathway.completed_percent(user)

      create(:user_content_completion, :completed, completable: card3, user: user)
      assert_equal 99, pathway.completed_percent(user)
    end
  end

  class TrendingCardTest < ActiveSupport::TestCase
    setup do
      @org = create(:organization)
      @channel =  create(:channel, organization: @org)
      @org = @channel.organization
      author = create(:user, organization: @org)
      @cards_list = create_list(:card, 5, author: author).each do |card|
        create(:channels_card, card: card, channel: @channel)
      end
    end

    test 'should not return any cards when there are no comments on any of the channels cards' do
      trending_cards = Card.trending_channel_cards(@channel.id)
      assert_empty trending_cards
    end

    test 'should return trending cards in descending order of comments' do
      create_list(:comment, 7, commentable: @cards_list[0], user: create(:user, organization: @org))
      create_list(:comment, 5, commentable: @cards_list[1], user: create(:user, organization: @org))
      create_list(:comment, 3, commentable: @cards_list[2], user: create(:user, organization: @org))
      create(:comment, commentable: @cards_list[3], user: create(:user, organization: @org))
      trending_cards = Card.trending_channel_cards(@channel.id)
      assert_equal 4, trending_cards.length
      assert_equal [7, 5, 3, 1], trending_cards.map(&:comments_count)
      assert_not_includes trending_cards.map(&:id), @cards_list[4]
    end
  end

  class CardPublishTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false
    setup do
      WebMock.disable!
    end

    teardown do
      WebMock.enable!
    end

    test 'should enqueue cards to followers on publish' do
      c = create(:card, state: 'new')
      FollowedCardEnqueueJob.expects(:perform_later).with(
        card_id: c.id
      ).returns(nil).once
      c.publish!
    end

    test 'publishing sends influx event' do
      org = Organization.first
      c = create(:card, state: 'new', organization: org)
      c.expects(:push_card_published_influx_event)
      c.publish
    end

    test 'publishing doesnt send influx event if publishing failed' do
      org = Organization.first
      c = create(:card,
        state: 'new',
        organization: org,
        card_type: 'pack'
      )
      c.expects(:push_card_published_influx_event).never
      c.publish
    end

    test '#push_card_published_influx_event with user credentials' do
      org = create :organization
      actor = create :user, organization: org
      metadata = { user_id: actor.id }
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      stub_time = Time.now
      c = create(:card,
        state: 'new',
        organization: org,
        author: actor
      )
      Analytics::MetricsRecorderJob.expects(:perform_later).with(
        recorder: 'Analytics::CardMetricsRecorder',
        timestamp: stub_time.to_i,
        event: "card_published",
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        additional_data: metadata,
        card: Analytics::MetricsRecorder.card_attributes(c),
        org: Analytics::MetricsRecorder.org_attributes(org)
      )
      Timecop.freeze stub_time do
        c.send(:push_card_published_influx_event)
      end
    end

    test '#push_card_published_influx_event without metadata uses author as actor' do
      RequestStore.stubs(:read).with(:request_metadata).returns nil
      org = create(:organization)
      author = create(:user, organization: org)
      stub_time = Time.now
      card = create(:card,
        state: 'new',
        organization: org,
        author: author
      )
      Analytics::MetricsRecorderJob.expects(:perform_later).with(
        recorder: 'Analytics::CardMetricsRecorder',
        timestamp: stub_time.to_i,
        event: "card_published",
        actor: Analytics::MetricsRecorder.user_attributes(card.author),
        additional_data: {},
        card: Analytics::MetricsRecorder.card_attributes(card),
        org: Analytics::MetricsRecorder.org_attributes(org)
      )
      Timecop.freeze stub_time do
        card.send(:push_card_published_influx_event)
      end
    end

    test '#push_card_published_influx_event without metadata or viable actor' do
      RequestStore.stubs(:read).with(:request_metadata).returns nil
      card = build :card, organization: create(:organization)
      card.stubs(:author)
      Analytics::MetricsRecorderJob.expects(:perform_later).never
      card.send :push_card_published_influx_event
    end

    test 'calls #push_video_stream_edited_influx_event on update' do
      Analytics::MetricsRecorderJob.unstub :perform_later

      org = create :organization
      author = create :user, organization: org
      card = build :card, author: author
      card.save!

      Analytics::MetricsRecorderJob
        .expects(:perform_later)
        .with(has_entry(event: 'card_edited'))
        .at_least(1)

      card.update(title: "foo")
      assert_equal card.title, "foo"
    end

    test 'calls job when whitelisted column is updated' do
      whitelist = Analytics::CardEditedMetricsRecorder.whitelisted_change_attrs
      assert whitelist.include? "message"
      org = create(:organization)
      user = create :user, organization: org
      card = create :card, author: user
      Analytics::MetricsRecorderJob.expects(:perform_later).once
      card.update message: "@foobar"
    end

    test 'doesnt call job for non-whitelisted column' do
      refute Analytics::CardEditedMetricsRecorder.whitelisted_change_attrs.include?(
        "impressions_count"
      )
      org = create(:organization)
      user = create :user, organization: org
      card = create :card, author: user
      Analytics::MetricsRecorderJob.expects(:perform_later).never
      card.update impressions_count: 123123
    end

     test "#push_card_edited_influx_event calls recorder" do
        Card.any_instance.unstub(:push_card_edited_influx_event)
        org = create :organization
        author = create :user, organization: org
        actor = create :user, organization: org
        card = create :card, author: author
        metadata = {
          user_id: actor.id,
          platform: "platform",
          user_agent: "user_agent",
          version_number: "version_number",
          is_admin_request: 0
        }
        RequestStore.stubs(:read).with(:request_metadata).returns metadata
        stub_time = Time.now
        Analytics::MetricsRecorderJob.expects(:perform_later).with(
          recorder: 'Analytics::CardEditedMetricsRecorder',
          org: Analytics::MetricsRecorder.org_attributes(org),
          card: Analytics::MetricsRecorder.card_attributes(card).merge(
            "title" => "foo",
            "updated_at" => stub_time.to_i,
            "snippet" => "foo"
          ),
          changed_column: "title",
          old_val: nil,
          new_val: "foo",
          event: "card_edited",
          actor: Analytics::MetricsRecorder.user_attributes(actor),
          timestamp: stub_time.to_i,
          additional_data: metadata
        )
        Timecop.freeze(stub_time) do
          card.update(title: "foo")
        end
      end

    test 'should enqueue cards to followers on publish #pathway' do
      cover = create(:card, card_type: 'pack', state: 'draft', card_subtype: 'simple')

      card3, card2, card1 = create_list(:card, 3)

      [card3, card2, card1].each do |card|
        cover.add_card_to_pathway(card.id, "Card")
      end

      FollowedCardEnqueueJob.expects(:perform_later).with(card_id: cover.id).returns(nil).once
      cover.publish!
    end

    test 'should enqueue cards to followers when card created with publish state' do
      # Card Id is not available at the time of expect.
      FollowedCardEnqueueJob.expects(:perform_later).returns(nil).once
      create(:card, state: 'published')
    end
  end

  test 'slug should be scoped to org' do
    org1, org2 = create_list(:organization, 2)
    card1 = create :card,
      card_type: 'media',
      card_subtype: 'link',
      organization: org1,
      title: 'title',
      author_id: create(:user, organization: org1).id

    card2 = create :card,
      card_type: 'media',
      card_subtype: 'link',
      organization: org2,
      title: 'title',
      author_id: create(:user, organization: org2).id

    assert_equal card1.slug, card2.slug
    assert_equal 'title', card1.slug
  end

  test "user can_comment on card" do
    org = create(:organization)
    u = create(:user, organization: org, organization_role: 'member')
    c = create :card,
      author_id: u.id,
      title: "some title",
      message: "&nbsp; <p> Science \n is awesome </p>"

    assert c.can_comment?(u)
  end

  test "user can_comment on card posted in accessible channels" do
    org = create(:organization)
    u = create(:user, organization: org, organization_role: 'member')
    card = create :card,
      organization: org,
      author_id: u.id,
      title: "some title",
      message: "&nbsp; <p> Science \n is awesome </p>"
    ch = create(:channel, :robotics, organization: org, is_private: true)
    card.channels << ch
    ch.followers << u
    assert card.can_comment?(u)
  end

  test 'member should be able to comment on public card' do
    org = create(:organization)
    user = create(:user, organization: org)
    member_user = create(:user, organization: org, organization_role: 'member')
    card = create :card,
      organization: org,
      author_id: user.id,
      title: "some title",
      message: "&nbsp; <p> Science \n is awesome </p>"
    assert card.can_comment?(member_user)
  end

  test 'member can comment on private card shared to him' do
    org = create(:organization)
    admin_user = create(:user, organization: org, organization_role: 'member')
    user = create(:user, organization: org, organization_role: 'member')
    card = create :card,
      organization: org,
      is_public: false,
      author_id: admin_user.id,
      title: "some title",
      message: "&nbsp; <p> Science \n is awesome </p>"
    shared_card = CardUserShare.create(user: user, card: card)
    assert card.can_comment?(user)
  end

  test 'member can comment on private card shared in the team' do
    org = create(:organization)
    admin_user = create(:user, organization: org, organization_role: 'admin')
    user = create(:user, organization: org, organization_role: 'member')
    card = create :card,
      organization: org,
      is_public: false,
      author_id: admin_user.id,
      title: "some title",
      message: "&nbsp; <p> Science \n is awesome </p>"

    team = create :team, organization: org, name: "Ruby Development"
    create(:teams_user , user: user, team: team)
    create(:teams_card, card: card, team: team, type: 'SharedCard', user: admin_user)

    assert card.can_comment?(user)
  end

  test 'member can comment with team permissions' do
    org = create(:organization)
    admin_user = create(:user, organization: org, organization_role: 'admin')
    user = create(:user, organization: org, organization_role: 'member')
    card = create :card,
      organization: org,
      is_public: false,
      author_id: admin_user.id,
      title: "some title",
      message: "&nbsp; <p> Science \n is awesome </p>"

    team = create :team, organization: org
    create :teams_user, team_id: team.id, user_id: user.id
    create :card_team_permission, card_id: card.id, team_id: team.id

    assert card.can_comment?(user)
  end

  test 'member can comment on cards with user permissions' do
    org = create(:organization)
    admin_user = create(:user, organization: org, organization_role: 'admin')
    user = create(:user, organization: org, organization_role: 'member')
    card = create :card,
      organization: org,
      is_public: false,
      author_id: admin_user.id,
      title: "some title",
      message: "&nbsp; <p> Science \n is awesome </p>"

    create(:card_user_permission, card_id: card.id, user_id: user.id)

    assert card.can_comment?(user)
  end

  test 'member can comment on channels cards' do
    org = create(:organization)
    admin_user = create(:user, organization: org, organization_role: 'admin')
    user = create(:user, organization: org, organization_role: 'member')
    card = create :card,
      organization: org,
      is_public: false,
      author_id: admin_user.id,
      title: "some title",
      message: "&nbsp; <p> Science \n is awesome </p>"

    prich = create(:channel, is_private: true, organization: org)
    channel_card = create(:channels_card, channel_id: prich.id, card_id: card.id)
    create(:follow, followable: prich, user: user)

    assert card.can_comment?(user)
  end

  test 'member cant comment on a card that is not accessible to him/her' do
    org = create(:organization)
    user_list = create_list(:user, 2, organization: org, organization_role: 'member')

    card = create(:card, organization: org, is_public: false, author_id: user_list[0].id)
    refute card.can_comment?(user_list[1])
  end

  test 'admin can comment on all private and public cards' do
    org = create(:organization)
    user = create(:user, organization: org, organization_role: 'member')
    card = create :card,
      organization: org,
      is_public: false,
      author_id: user.id,
      title: "some title",
      message: "&nbsp; <p> Science \n is awesome </p>"

    admin_user = create(:user, organization: org, organization_role: 'admin')
    assert card.can_comment?(admin_user)
  end

  test 'ugc?' do
    c = create(:card, author_id: create(:user).id)
    assert c.ugc?

    c = create(:card, author_id: nil, organization_id: create(:organization).id)
    refute c.ugc?
  end

  test "it should return active ecl state" do
    org = create(:organization)
    author = create(:user, organization: org)

    published_card = create(:card, state: "published", author: author)
    archived_card = create(:card, state: "archived", author: author)
    draft_card = create(:card, state: "draft", author: author)

    assert_equal "active", published_card.ecl_state
    assert_equal "archived", archived_card.ecl_state
    assert_equal "drafted", draft_card.ecl_state
  end

  test 'snippet' do
    c = create(:card, title: nil, message: 'some message')
    assert_equal 'some message', c.snippet
  end

  test 'snippet_html' do
    c = create(:card, title: nil, message: '++test++ ~~test~~ _test_ **test**')
    assert_equal "<p><u>test</u> <del>test</del> <em>test</em> <strong>test</strong></p>\n", c.snippet_html
  end

  test "assign channels to card" do
    u = create(:user)
    c1 = create(:card, author_id: u.id)
    ch1 = create(:channel, label: 'Robotics', organization: u.organization)
    ch2 = create(:channel, label: 'AI', organization: u.organization)
    ch3 = create(:channel, label: "Unauthorized for user", organization: u.organization)
    [ch1, ch2].each {|ch| ch.authors << u}

    c1.assign_channels_to_card([ch1.id, ch2.id, ch3.id], u)

    assert_same_elements c1.channels, [ch1, ch2]
    assert_not_includes c1.channels, ch3
  end

  test 'should return video stream start_time as published_at' do
    video_stream = create(:wowza_video_stream, status: 'upcoming')
    assert_equal video_stream.card.published_at.to_i, video_stream.start_time.to_i

    video_stream.update(status: 'past')
    video_stream.reload
    assert_equal video_stream.card.published_at.to_i, video_stream.start_time.to_i
  end

  test "card_ecl_id" do
    assert_equal "ECL-always_dash", Card.card_ecl_id("always_dash")
    assert_not_equal "ECL.always_dash", Card.card_ecl_id("always_dash")
  end

  test "is_ecl_id?" do
    assert Card.is_ecl_id?("ECL-always_dash")
    refute Card.is_ecl_id?("ECL.always_dash")
  end

  test "get_ecl_id" do
    assert_equal Card.get_ecl_id("ECL-always_dash"), "always_dash"
    assert_nil Card.get_ecl_id("invalid-always_dash")
  end

  test '#add_card_to_channel' do
    channel = create(:channel, organization: @card.organization)
    assert_not_includes @card.channels, channel

    EclApi::CardConnector.any_instance.expects(:card_from_ecl_id).returns(@card)

    @card.add_card_to_channel(channel)
    assert_includes @card.channels, channel
  end

  test 'add_card_to_channel: card should be private if channel source integration is marked private' do
    channel = create(:channel, organization: @org, is_private: true)
    card = create(:card, card_type: 'media', card_subtype: 'text', author_id: @u.id, title: 'this is title', message: 'this is message', is_public: true, ecl_metadata: {source_id: "123"})
    ecl_source = create(:ecl_source, channel: channel, ecl_source_id: "123", private_content: true)
    EclApi::CardConnector.any_instance.expects(:card_from_ecl_id).returns(card)
    card.add_card_to_channel(channel, ecl_source.private_content)
    assert_includes card.channels, channel
    assert !card.is_public
  end

  test 'add_card_to_channel: card should propogate to ecl when added to channel' do
    TestAfterCommit.with_commits(true) do
      EclCardPropagationJob.expects(:perform_later).once
      EclApi::CardConnector.any_instance.expects(:card_from_ecl_id).returns(@card)
      channel = create(:channel, organization: @u.organization)
      ecl_source = create(:ecl_source, channel: channel, ecl_source_id: "dfdfd-dfdf")
      @card.add_card_to_channel(channel, ecl_source.private_content)
    end
  end

  test 'should have message in #pack_cards_index_data' do
    Card.any_instance.stubs(:reindex).returns true

    org = create(:organization)
    user = create(:user, organization: org)

    cover = create(:card, card_type: 'pack', card_subtype: 'simple', author: user)
    card1, card2, card3 = create_list(:card, 3, author: user)

    [card1, card2, card3].each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: card.class.name)
    end

    pack_cards_data = cover.pack_cards_index_data
    pack_cards_data.each do |pcd|
      assert_not_nil pcd['message']
    end
  end

  test 'pathway card should be in draft state #create' do
    assert_difference -> {Card.count} do
      card = create(:card, card_type: 'pack')
      assert_equal Card::STATE_DRAFT, card.state
    end

    assert_difference -> {Card.count} do
      card = create(:card, card_type: 'media')
      assert_equal Card::STATE_PUBLISHED, card.state
    end
  end

  class EclPropagationTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false
    setup do
      WebMock.disable!
    end

    teardown do
      WebMock.enable!
    end

    test "should not propgate if pathway in draft state on creation" do
      # Propage only once while updating state to published
      EclCardPropagationJob.expects(:perform_later).returns(nil).once

      #should not propogate while creating pathway in draft state
      draft_cover = create(:card, card_type: 'pack', state: 'draft', card_subtype: 'simple')

      # should propogate while updating state to published
      draft_cover.update(state: 'published')
    end

    test 'should propagate ugc cards to ecl' do
      # Once while creating, and once while updating
      EclCardPropagationJob.expects(:perform_later).returns(nil).twice

      # Should propagate
      propagated_card = create(:card, title: 'Hello', author_id: create(:user).id)

      # Should propagate again
      propagated_card.update(title: propagated_card.title + '!')
    end

    test 'should not propagate cards without author id' do
      EclCardPropagationJob.expects(:perform_later).returns(nil).never

      # Should not propagate
      non_propagated_card = create(:card, author_id: nil, organization_id: create(:organization).id)
    end

    test 'should not propagate when a non important details is updated' do
      # Propage only once while creating
      EclCardPropagationJob.expects(:perform_later).returns(nil)

      # Should propagate
      propagated_card = create(:card, title: 'Hello', author_id: create(:user).id)

      # Should not propagate
      propagated_card.update(published_at: Time.now.utc)
    end

    test 'should delete card from ecl when ecl id and author id is present' do
      user = create(:user)
      # Propage only once while creating
      EclCardPropagationJob.expects(:perform_later).never

      # Not Able to destroy card which does not have ecl_id
      card = create(:card, title: 'Hello', ecl_id: nil, organization_id: user.organization_id, author: nil)
      card.destroy

      ugc_card = create :card,
        title: 'Hello',
        author_id: user.id,
        organization_id: user.organization_id,
        ecl_id: '337cf70f-2253-45de-8da8-1867aca7c55a'
      # Request to delete card from ecl
      EclCardArchiveJob.expects(:perform_later)
      ugc_card.destroy
    end
  end

  class DeepLinkIdAndTypeV2Test < ActiveSupport::TestCase

    setup do
      @org = create(:organization)
      @author = create(:user, organization: @org)
    end

    test 'normal card' do
      c = create(:card, card_type: 'media')
      id, type = c.get_app_deep_link_id_and_type_v2
      assert_equal c.id, id
      assert_equal 'card', type
    end

    test 'pathway cover card' do
      c = create(:card, card_type: 'pack')
      id, type = c.get_app_deep_link_id_and_type_v2
      assert_equal c.id, id
      assert_equal 'collection', type
    end

    test 'video stream card' do
      c = create(:card, card_type: 'video_stream')
      id, type = c.get_app_deep_link_id_and_type_v2
      assert_equal c.id, id
      assert_equal 'card', type
    end

    test 'hidden card in a pathway' do
      CardPackRelation.stubs(:reindex)
      hidden_card = create(:card, hidden: true, card_type: 'media')
      pack_cover = create(:card, card_type: 'pack')
      CardPackRelation.add(
        cover_id: pack_cover.id,
        add_id: hidden_card.id,
        add_type: hidden_card.class.name
      )

      id, type = hidden_card.get_app_deep_link_id_and_type_v2
      assert_equal pack_cover.id, id
      assert_equal 'collection', type
    end

    test 'hidden card in a journey section' do
      hidden_card = create(
        :card, hidden: true,
        card_type: 'media',
        author: @author
      )

      setup_journey_card(@author)
      CardPackRelation.add(
        cover_id: @section2.id,
        add_id: hidden_card.id,
        add_type: hidden_card.class.name
      )

      id, type = hidden_card.get_app_deep_link_id_and_type_v2
      assert_equal @journey.id, id
      assert_equal 'journey', type
    end
  end

  class CardVideoStreamStateTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false

    setup do
      WebMock.disable!
      VideoStream.searchkick_index.delete if VideoStream.searchkick_index.exists?
      EclCardPropagationJob.stubs(:perform_later).returns(nil)
      VideoStream.any_instance.stubs(:schedule_reminder_push_notification)
    end

    teardown do
      WebMock.enable!
    end

    test 'should update video stream state #publish' do
      video_stream = create(:iris_video_stream, state: 'draft')
      card = create(:card, video_stream: video_stream, state: 'draft', card_type: 'video_stream')

      card.publish!
      video_stream.reload

      assert_equal card.state, video_stream.state
    end
  end

  class CardFileStackFileresoureceTest < ActiveSupport::TestCase
    test 'should create file resource objects' do
      skip('DEPRECATED')

      FileResource.any_instance.stubs(:valid_mime_type?).returns(true)

      stub_request(:get, "https://cdn.filestackcontent.com/yT4BBpg7T5efeiriQv37").to_return(status: 200)

      filestack = [
                   {
                      "filename": "Strix_nebulosaRB.jpg",
                      "mimetype": "image/jpeg",
                      "size": 106176,
                      "source": "local_file_system",
                      "url": "https://cdn.filestackcontent.com/yT4BBpg7T5efeiriQv37",
                      "handle": "yT4BBpg7T5efeiriQv37",
                      "status": "Stored",
                      "key": "VYgpc6fnTOKJkFWIkzti_Strix_nebulosaRB.jpg",
                      "container": "your_bucket"
                   }
                ]
      card = nil
      assert_difference -> {FileResource.count} do
        card = create(:card, filestack: filestack)
      end

      file_resource = FileResource.last
      assert_equal filestack[0][:handle], file_resource.filestack_handle
      assert_not_nil file_resource.attachment.url
      assert card.author.id, file_resource.user.id
    end

    test 'creates card but file_resource with invalid content_type is not saved' do
      stub_request(:get, "https://cdn.filestackcontent.com/MrJTrywST2iyCfXVFMUN").to_return(status: 200, headers: {content_type:'application/x-shockwave-flash'})

      filestack = [{
                    'container': "edcast-file-stack-temp",
                    'filename': "flash.swf",
                    'handle': "MrJTrywST2iyCfXVFMUN",
                    'key': "wNSvsi9nQFEPfYvykP4g_flash.swf",
                    'mimetype': "application/x-shockwave-flash",
                    'size': 116887,
                    'status': "Stored",
                    'url': "https://cdn.filestackcontent.com/MrJTrywST2iyCfXVFMUN"
                }]
      card = create(:card, filestack: filestack)
      assert_no_difference -> {FileResource.count} do
        card.send(:update_file_resources_from_filestack)
      end

      assert_empty card.reload.file_resources
    end

    test 'should create new file_resource and delete the previous file_resource on card update' do
      skip('DEPRECATED')

      # creating new card
      FileResource.any_instance.stubs(:valid_mime_type?).returns(true)
      stub_request(:get, "https://cdn.filestackcontent.com/yT4BBpg7T5efeiriQv37").to_return(status: 200)

      filestack = [
                   {
                      "filename": "Strix_nebulosaRB.jpg",
                      "mimetype": "image/jpeg",
                      "size": 106176,
                      "source": "local_file_system",
                      "url": "https://cdn.filestackcontent.com/yT4BBpg7T5efeiriQv37",
                      "handle": "yT4BBpg7T5efeiriQv37",
                      "status": "Stored",
                      "key": "VYgpc6fnTOKJkFWIkzti_Strix_nebulosaRB.jpg",
                      "container": "your_bucket"
                   }
                ]
      #created new card with filestack data
      card = nil
      assert_difference -> {FileResource.count} do
        card = create(:card,card_type: 'media', card_subtype: 'text',filestack: filestack)
      end

      file_resource = FileResource.last
      assert_equal filestack[0][:handle], file_resource.filestack_handle
      assert_not_nil file_resource.attachment.url
      assert card.author.id, file_resource.user.id

      #updating the same card with new values
      FileResource.any_instance.stubs(:valid_mime_type?).returns(true)
      stub_request(:get, "https://cdn.filestackcontent.com/yT4BBpg7T5efeiriQ897").to_return(status: 200)
      new_filestack = [
                   {
                      "filename": "Strix_nebulosaRBnilesh.jpg",
                      "mimetype": "image/jpeg",
                      "size": 106188,
                      "source": "local_file_system",
                      "url": "https://cdn.filestackcontent.com/yT4BBpg7T5efeiriQ897",
                      "handle": "yT4BBpg7T5efeiriQ897",
                      "status": "Stored",
                      "key": "VYgpc6fnTOKJkFWIkzti_Strix_nebulosaRBnilesh.jpg",
                      "container": "your_bucket"
                   }
                ]

      #update the card with new filestack values
      card.update_attributes(filestack: new_filestack)
      new_file_resource = FileResource.last
      assert_equal new_filestack[0][:handle], card.filestack[0][:handle]
      assert_not_nil new_file_resource.attachment.url
      assert card.author.id, new_file_resource.user.id
      assert_equal 1, card.file_resources.count


    end

    test 'should create file resource object for non-registered MIME types for zip' do
      skip('DEPRECATED')

      stub_request(:get, "https://cdn.filestackcontent.com/yT4BBpg7T5efeiriQv37").to_return(status: 200, :headers => {content_type:'application/x-zip-compressed'})

      filestack = [{
                      "filename": "Strix_nebulosaRB.zip",
                      "mimetype": "application/x-zip-compressed",
                      "size": 106176,
                      "source": "local_file_system",
                      "url": "https://cdn.filestackcontent.com/yT4BBpg7T5efeiriQv37",
                      "handle": "yT4BBpg7T5efeiriQv37",
                      "status": "Stored",
                      "key": "VYgpc6fnTOKJkFWIkzti_Strix_nebulosaRB.zip",
                      "container": "your_bucket"
                   }]
      assert_difference -> {FileResource.count} do
        card = create(:card, filestack: filestack)
      end
    end

  end

  class CardActionMetricRecorderTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false

    setup do
      IndexJob.stubs(:perform_later)
      @org = create(:organization)
      @author, @actor = create_list(:user, 2, organization: @org)
      @user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0'

      RequestStore.store[:request_metadata] = {platform: 'web', user_agent: @user_agent, user_id: @actor.id}
    end

    test 'should invoke card metric recorder job #card_create' do
      author = create(:user)
      Analytics::MetricsRecorderJob.unstub(:perform_later)

      Analytics::MetricsRecorderJob.expects(:perform_later)
        .with(has_entry(event: 'card_created'))
        .once

      create(:card, author: author)
    end

    test 'should invoke card metric recorder job #card_delete' do
      Card.any_instance.stubs(:remove_card_from_queue).returns true
      Card.any_instance.stubs(:delete_ecl_card).returns true

      now = Time.now.utc
      Time.stubs(:now).returns now

      metadata = {
        user_agent: @user_agent,
        platform: 'web',
        user_id: @actor.id
      }

      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      assert_equal true, Card.respond_to?(:track_delete_event)

      card = create(:card, author: @author, organization: @org)
      now = Time.now.utc
      Time.stubs(:now).returns now
      Analytics::MetricsRecorderJob.unstub :perform_later
      Analytics::MetricsRecorderJob.expects(:perform_later).with({
        recorder: 'Analytics::CardMetricsRecorder',
        org: Analytics::MetricsRecorder.org_attributes(@org),
        card: Analytics::MetricsRecorder.card_attributes(card),
        event: 'card_deleted',
        actor: Analytics::MetricsRecorder.user_attributes(@actor),
        timestamp: now.to_i,
        additional_data: metadata
      }).returns true
      card.destroy
    end

    test 'should invoke card metric recorder job #card_promote' do
      Card.any_instance.stubs(:record_card_create_event).returns true
      card = create(:card, author: @author, organization: @org)
      now = Time.now.utc
      Time.stubs(:now).returns now
      metadata = {
        user_agent: @user_agent,
        platform: 'web',
        user_id: @actor.id
      }
      RequestStore.stubs(:read).with(:request_metadata).returns(metadata)

      # Job get invoked twice
      # 1. when card edit happens for `is_official`
      # 2. Another one when promoted_at is set for `card_promoted` event
      Analytics::MetricsRecorderJob.expects(:perform_later)
      .with(has_entry(event: 'card_edited')).once

      Analytics::MetricsRecorderJob.expects(:perform_later)
      .with(has_entry(event: 'card_promoted')).once

      card.update(is_official: true)
    end

    test 'should invoke card promote events' do
      Card.any_instance.stubs(:record_card_create_event).returns true
      card = create(:card, author: @author)

      Card.any_instance.expects(:record_card_promote_event)
      card.update(is_official: true)

      Card.any_instance.expects(:record_card_unpromote_event)
      card.update(is_official: false)
    end

    test 'should invoke card metric recorder job #card_unpromote' do
      Card.any_instance.stubs(:record_card_create_event).returns true
      card = create :card,
        author: @author,
        is_official: true,
        organization: @org
      now = Time.now.utc
      Time.stubs(:now).returns now
      metadata = {user_agent: @user_agent, platform: 'web', user_id: @actor.id}
      RequestStore.stubs(:read).with(:request_metadata).returns(metadata)
      Analytics::MetricsRecorderJob.expects(:perform_later).with({
        recorder: 'Analytics::CardMetricsRecorder',
        timestamp: now.to_i,
        event: "card_unpromoted",
        actor: Analytics::MetricsRecorder.user_attributes(@actor),
        additional_data: metadata,
        card: Analytics::MetricsRecorder.card_attributes(card).merge(
          "is_official" => false,
          "updated_at" => now.to_i
        ),
        org: Analytics::MetricsRecorder.org_attributes(@org)
      }).returns true
      card.update(is_official: false)
    end

  end

  class ValidateCardSubTypeTest < ActiveSupport::TestCase
    test 'media card subtypes validation' do
      assert_nothing_raised do
        link_resource = create(:resource, type: 'Article')
        video_resource = create(:resource, type: 'Article')
        image_resource = create(:resource, type: 'Image')
        audio_file_resource = create(:file_resource, attachment_content_type: 'audio/mp3')
        pdf_file_resource = create(:file_resource, attachment_content_type: 'application/pdf')

        create(:card, card_type: 'media', card_subtype: 'video', resource: video_resource)
        create(:card, card_type: 'media', card_subtype: 'image', resource: image_resource)
        create(:card, card_type: 'media', card_subtype: 'link', resource: link_resource)
        create(:card, card_type: 'media', card_subtype: 'text')
        create(:card, card_type: 'media', card_subtype: 'file', file_resource_ids: [pdf_file_resource.id])
        create(:card, card_type: 'media', card_subtype: 'audio', file_resource_ids: [audio_file_resource.id])
      end

      assert_raise ActiveRecord::RecordInvalid do
        card = build(:card, card_type: 'media')
        card.card_subtype = 'pdf'
        card.save!
      end
    end

    test 'pack card type validation' do
      assert_nothing_raised do
        create(:card, card_type: 'pack', card_subtype: 'simple')
      end

      assert_raise ActiveRecord::RecordInvalid do
        card = build(:card, card_type: 'pack')
        card.card_subtype = 'pdf'
        card.save!
      end
    end

    test 'journey card type validation' do
      assert_nothing_raised do
        create(:card, card_type: 'journey', card_subtype: 'self_paced')
        create(:card, card_type: 'journey', card_subtype: 'weekly')
      end

      assert_raise ActiveRecord::RecordInvalid do
        card = build(:card, card_type: 'journey')
        card.card_subtype = 'simple'
        card.save!
      end
    end

    test 'pack card type for journey section validation' do
      assert_nothing_raised do
        create(:card, card_type: 'pack', card_subtype: 'simple')
      end

      assert_raise ActiveRecord::RecordInvalid do
        card = build(:card, card_type: 'pack')
        card.card_subtype = 'pdf'
        card.save!
      end
    end

    test 'journey_sections should return the jounrey_ids where pathway is not present' do
      journey = create(:card, card_type: 'journey', card_subtype: 'self_paced', title: 'this is title', message: 'this is message')
      pack = create(:card, card_type: 'pack', card_subtype: 'simple', title: 'this is title', message: 'this is message')
      result = pack.journeys_sections(pack: true)
      assert_nil result[:jpr_ids]
      assert_includes result[:card_ids], journey.id
    end

    test 'should set auto_complete for pack card' do
      @pack = create(:card, card_type: 'pack', card_subtype: 'simple', title: 'this is title', message: 'this is message')
      assert @pack.auto_complete
    end

    test 'should set auto_complete for journey card' do
      @journey = create(:card, card_type: 'journey', card_subtype: 'self_paced', title: 'this is title', message: 'this is message')
      assert @journey.auto_complete
    end

    test 'video stream card type validation' do
      assert_nothing_raised do
        create(:card, card_type: 'video_stream', card_subtype: 'simple')
        create(:card, card_type: 'video_stream', card_subtype: 'role_play')
      end

      assert_raise ActiveRecord::RecordInvalid do
        card = build(:card, card_type: 'video_stream')
        card.card_subtype = 'pdf'
        card.save!
      end
    end

    test 'poll card subtypes validation' do
      assert_nothing_raised do
        link_resource = create(:resource, type: 'Article')
        video_resource = create(:resource, type: 'Article')
        image_resource = create(:resource, type: 'Image')

        create(:card, card_type: 'poll', card_subtype: 'video', resource: video_resource)
        create(:card, card_type: 'poll', card_subtype: 'image', resource: image_resource)
        create(:card, card_type: 'poll', card_subtype: 'link', resource: link_resource)
        create(:card, card_type: 'media', card_subtype: 'text')
      end

      assert_raise ActiveRecord::RecordInvalid do
        card = build(:card, card_type: 'poll')
        card.card_subtype = 'pdf'
        card.save!
      end

      assert_raise ActiveRecord::RecordInvalid do
        card = build(:card, card_type: 'poll')
        card.card_subtype = 'video_stream'
        card.save!
      end
    end

    class CardDeeplinkUrlTest < ActiveSupport::TestCase

      setup do
        WebMock.disable!
      end

      teardown do
        WebMock.enable!
      end
      test 'return valid deeplink url for the card' do
        card = create(:card)
        deeplink_url = card.card_deeplink_url
        assert_match(Settings.branch_url, deeplink_url)
        assert_match('card', deeplink_url)
        assert_match(card.id.to_s, deeplink_url)
      end
    end
  end

  # TODO NEED HELP IN FIXING THIS TEST
  # test 'should remap locks in pathways after card delete' do
  #   cover = create(:card, card_type: 'pack', card_subtype: 'simple', author_id: @u.id)
  #   card3, card2, card1 = create_list(:card, 3, author: @u)

  #   CardPackRelation.add(cover_id: cover.id, add_id: card1.id, add_type: card1.class.name)
  #   CardPackRelation.add(cover_id: cover.id, add_id: card2.id, add_type: card2.class.name)
  #   CardPackRelation.add(cover_id: cover.id, add_id: card3.id, add_type: card3.class.name)

  #   locked_card = CardPackRelation.find_by(cover_id: cover.id, from_id: card2.id)
  #   locked_card.update_attributes!(locked: true)

  #   next_card = CardPackRelation.find_by(cover_id: cover.id, from_id: card3.id)
  #   # Related CPR's are not deleted -> CardDeletionJob

  #   assert card2.remap_lock?
  #   card2.destroy

  #   assert_not locked_card.reload.locked
  #   assert next_card.reload.locked
  # end

  test 'should not invoke remap locks in pathways after card delete for packs which content less than 2 cards' do
    cover = create(:card, card_type: 'pack', card_subtype: 'simple', author_id: @u.id)
    card1 = create(:card, author: @u)
    CardPackRelation.add(cover_id: cover.id, add_id: card1.id, add_type: card1.class.name)
    locked_card = CardPackRelation.find_by(cover_id: cover.id, from_id: card1.id)
    locked_card.update_attributes!(locked: true)

    refute card1.remap_lock?
  end

  test 'should not remap locks for card with false lock status' do
    cover = create(:card, card_type: 'pack', card_subtype: 'simple', author_id: @u.id)
    card3, card2, card1 = create_list(:card, 3, author: @u)

    CardPackRelation.add(cover_id: cover.id, add_id: card1.id, add_type: card1.class.name)
    CardPackRelation.add(cover_id: cover.id, add_id: card2.id, add_type: card2.class.name)
    CardPackRelation.add(cover_id: cover.id, add_id: card3.id, add_type: card3.class.name)

    refute card2.remap_lock?
  end

  test 'should remap locks in pathways after pack relation removed' do
    cover = create(:card, card_type: 'pack', card_subtype: 'simple', author_id: @u.id)
    card3, card2, card1 = create_list(:card, 3, author: @u)

    CardPackRelation.add(cover_id: cover.id, add_id: card1.id, add_type: card1.class.name)
    CardPackRelation.add(cover_id: cover.id, add_id: card2.id, add_type: card2.class.name)
    CardPackRelation.add(cover_id: cover.id, add_id: card3.id, add_type: card3.class.name)
    locked_card = CardPackRelation.find_by(cover_id: cover.id, from_id: card2.id)
    locked_card.update_attributes!(locked: true)

    CardPackRelation.remove_from_pack(cover_id: cover.id, remove_id: card2.id)
    next_card = CardPackRelation.find_by(cover_id: cover.id, from_id: card3.id)

    assert_not CardPackRelation.find_by(id: locked_card.id)
    assert next_card.reload.locked
  end

  test 'get time to read' do
    card_with_no_duration = create(:card, author: @u)
    card_with_duration = create(:card, duration: 70, author: @u)
    card_with_ecl_duration = create(:card, ecl_metadata: {'duration_metadata' => {'calculated_duration' => 90 }})

    assert_nil card_with_no_duration.time_to_read
    assert_equal 70, card_with_duration.time_to_read
    assert_equal 90, card_with_ecl_duration.time_to_read
  end

  test 'should push card view event' do
    org = create(:organization)
    viewer = create(:user, organization: org)
    card = create(:card, author: create(:user, organization: org))

    req_data = {
      platform: 'ios',
      platform_version_number: 2,
      user_agent: 'chrome',
      is_admin_request: 0
    }

    RequestStore.store[:request_metadata] = req_data
    now = Time.now.utc
    Time.stubs(:now).returns now
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::CardViewMetricsRecorder",
      org: Analytics::MetricsRecorder.org_attributes(org),
      card: Analytics::MetricsRecorder.card_attributes(card),
      actor: Analytics::MetricsRecorder.user_attributes(viewer),
      event: 'card_viewed',
      timestamp: now.to_i,
      additional_data: req_data
    )
    card.push_view_event(viewer)
  end

  test 'should push card_source_visited event' do
    org = create(:organization)
    viewer = create(:user, organization: org)
    card = create(:card, author: create(:user, organization: org))

    req_data = {
      platform: 'ios',
      platform_version_number: 2,
      user_agent: 'chrome',
      is_admin_request: 0
    }

    RequestStore.store[:request_metadata] = req_data
    now = Time.now.utc
    Time.stubs(:now).returns now
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::CardViewMetricsRecorder",
      org: Analytics::MetricsRecorder.org_attributes(org),
      card: Analytics::MetricsRecorder.card_attributes(card),
      actor: Analytics::MetricsRecorder.user_attributes(viewer),
      event: 'card_source_visited',
      timestamp: now.to_i,
      additional_data: req_data
    )
    card.push_click_through_event(viewer)
  end

  class FromUrlPathTest < ActiveSupport::TestCase
    test '#from_url_path gets valid card' do
      org = create(:organization)
      user = create(:user, organization: org)
      card = create(:card, author: user, organization: org, slug: 'new-card-slug')

      assert_equal card.id, Card.from_url_path('/insights/new-card-slug', org).id
      assert_equal card.id, Card.from_url_path('/insights/' + card.id.to_s, org).id
    end

    test '#from_url_path gets valid ECL card' do
      org = create(:organization)
      ecl_id = '1234-1234-1234-1234'

      stub_request(:get, "#{Settings.ecl.api_endpoint}/api/#{Settings.ecl.api_version}/content_items/#{CGI.escape(ecl_id)}")
        .to_return(body: {data: sample_ecl_card}.to_json)

      card = Card.from_url_path("/insights/ECL-#{ecl_id}", org)

      assert card.id
      assert_equal sample_ecl_card[:id], card.ecl_id

      resource = card.resource
      assert resource.present?
      assert_equal sample_ecl_card[:url], resource.url
    end

    def sample_ecl_card
      {
        "id": "e1e16a37-bd24-4b2d-a3b3-5e8409f9b058",
        "name": nil,
        "description": "Dell is saying there is 3 key steps to IT Transformation ",
        "summary": nil,
        "url": "https://www.dellemc.com/en-us/data-center-transformation/index.htm?i=m&cmp=soc-sprinklr-146726-950030088&linkId=39582277#overlay=/collateral/brochure/dellemc-3-key-steps-to-it-transformation.pdf",
        "external_id": "1929228",
        "author": nil,
        "source_id": "d8e3a95b-cee0-4caa-a042-378c65b98b85",
        "source_type_id": "cf236020-c3f8-4ac7-8e85-3b62683a90da",
        "source_type_name": "ugc",
        "content_type": "article",
        "organization_id": 1454,
        "duration_metadata": {
          "calculated_duration": 60,
          "calculated_duration_display": "1 minute"
        },
        "resource_metadata": {
          "title": "IT Transformation - Accelerate IT Innovation | Dell EMC US",
          "description": "Dell EMC IT Transformation solutions reduce IT costs, accelerate IT innovation, improve customer enlightenment and speed up application deployments.",
          "embed_html": nil,
          "images": [
            {
              "url": nil
            }
          ]
        },
        "additional_metadata": nil,
        "created_at": "2017-07-18T13:52:20.106Z",
        "updated_at": "2017-07-18T13:52:20.293Z",
        "user_id": 1234316,
        "source_display_name": "UGC",
        "taxonomies_list": [],
        "taxonomies": nil,
        "nlp_metadata": nil,
        "tags": [
          {
            "tag": "dell",
            "source": "citadel",
            "strength": 0.5,
            "tag_type": "keyword"
          },
          {
            "tag": "3 key step",
            "source": "citadel",
            "strength": 0.5,
            "tag_type": "keyword"
          }
        ]
      }
    end
  end

  class VisibleCardToUserTest < ActiveSupport::TestCase
    setup do
      @organization = create :organization

      @user = create :user, organization: @organization
      @other_user = create :user, organization: @organization
      @admin_user = create :user, organization: @organization, organization_role: 'admin'

      @card_common_params = {organization: @organization, card_type: 'media', card_subtype: 'text',
                             title: 'this is title', message: 'this is message', author: @other_user}
    end

    test 'visible_to_user should return true for any user if card is_public' do
      card =  create :card, @card_common_params.merge(is_public: true)
      assert card.visible_to_user(@other_user)
      assert card.visible_to_user(@user)
      assert card.visible_to_user(@admin_user)
    end

    test 'should return false for private cards' do
      card =  create :card, @card_common_params.merge(is_public: false)

      assert_not card.visible_to_user(@user)
    end

    test 'should return true for private cards, if user is explicitly granted access' do
      card =  create :card, @card_common_params.merge(is_public: false)
      card.users_with_access << @user

      assert card.visible_to_user(@user)
    end

    test 'should return true for private cards, if user\'s channel is granted access' do
      channel = create :channel, organization: @organization
      channel.add_followers [@user]

      card =  create :card, @card_common_params.merge(is_public: false)
      card.channels << channel

      assert card.visible_to_user(@user)
    end

    test 'should return true for private cards, if user\'s group is granted access' do
      group = create :team, organization: @organization
      group.add_user(@user)

      card =  create :card, @card_common_params.merge(is_public: false)
      SharedCard.find_or_create_by(team_id: group.id, card_id: card.id, user_id: nil)

      assert card.visible_to_user(@user)
    end

    test 'should return true for private cards, if user is the author' do
      card =  create :card, @card_common_params.merge(is_public: false, author: @user)

      assert card.visible_to_user(@user)
    end

    test 'should return true for private cards, if user is an admin' do
      card =  create :card, @card_common_params.merge(is_public: false, author: @user)

      assert card.visible_to_user(@admin_user)
    end

    test 'should return true for team sources ecl card, if user part of team' do
      card = create :card, @card_common_params.merge(author: nil, ecl_metadata: {source_id: 'abc-123'})
      team_user = create :teams_user, team: create(:team, organization: @organization, source_ids: ['abc-123']), user: @user

      assert card.visible_to_user(@user)
    end

    test 'should return true for all users, if org does not have team sources'  do
      card = create :card, @card_common_params.merge(author: nil, ecl_metadata: {source_id: 'abc-123'})

      assert card.visible_to_user(@user)
    end

    test 'should return true for all users, if source is not link to any team'  do
      card = create :card, @card_common_params.merge(author: nil, ecl_metadata: {source_id: 'abc-123'})
      not_team_user = create :user, organization: @organization
      team = create :team, organization: @organization, source_ids: ['abc-124']

      assert card.visible_to_user(@user)
      assert card.visible_to_user(not_team_user)
    end

    test 'should return true if user has been provided explicit permission' do
      card = create :card, @card_common_params.merge(author: nil, ecl_metadata: {source_id: 'abc-123'}, is_public: false)
      user = create(:user, organization: @organization)
      card.users_with_permission << user
      assert card.visible_to_user(user)
      assert_not card.visible_to_user(@user)
    end

    test 'should return true if users is from a team that has been provided explicit permission' do
      card = create :card, @card_common_params.merge(author: nil, ecl_metadata: {source_id: 'abc-123'}, is_public: false)
      team = create(:team, organization: @organization)
      user = create(:user, organization: @organization)
      team.add_member(user)
      card.teams_with_permission << team

      assert card.visible_to_user(user)
      assert_not card.visible_to_user(@user)
    end

    test 'should return false if users sub_admin' do
      card = create :card, @card_common_params.merge(author: nil, ecl_metadata: {source_id: 'abc-123'}, is_public: false)
      team = create(:team, organization: @organization)
      user = create(:user, organization: @organization)
      role = @organization.roles.find_by(name: 'group_admin')
      create :role_permission, role: role, name: Permissions::MANAGE_CONTENT
      team.add_admin(user)

      refute card.visible_to_user(user)
    end

    # This is a specific use case wrt EP-24390 & EP-24286
    test 'should return true if user does not have direct access but follows channel(where card is posted) via team' do
      # Post card to channel
      card = create(:card, @card_common_params.merge(author: nil, is_public: false))
      channel = create(:channel, organization: @organization)
      channel.cards << [card]

      # other_user follows channel via team
      team = create(:team, organization: @organization)
      team.add_member(@other_user)
      create(:teams_channels_follow, team_id: team.id, channel_id: channel.id)

      # Direct access to member_user on card
      member_user = create(:user, organization: @organization)
      CardUserShare.create(card: card, user: member_user)

      # Check visibility for other user
      assert card.visible_to_user(@other_user)
    end
  end

  test 'should not call ecl connector propagate method if update request coming from ecl' do
    c = create(:card)
    EclCardPropagationJob.expects(:perform_later).with(c.id).never
    c.update(title: "modified title", update_request_from_ecl: true)
  end

  test 'should card with message and source id' do
    c = create(:card, message: "test something", ecl_metadata: {source_id: "test"})
    EclCardPropagationJob.expects(:perform_later).with(c.id).never
    assert_equal c, Card.find_in_source_with_message("test something", "test")
  end

  class ScormCardTest < ActiveSupport::TestCase
      scorm_filestack = [{
                      "filename": "Strix_nebulosaRB.zip",
                      "mimetype": "application/x-zip-compressed",
                      "size": 106176,
                      "source": "local_file_system",
                      "url": "https://cdn.filestackcontent.com/yT4BBpg7T5efeiriQv37",
                      "handle": "yT4BBpg7T5efeiriQv37",
                      "status": "Stored",
                      "key": "VYgpc6fnTOKJkFWIkzti_Strix_nebulosaRB.zip",
                      "container": "your_bucket",
                      "scorm_course": true
                   }]
    test 'shoud call upload_to_scorm job' do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      stub_request(:get, "https://cdn.filestackcontent.com/yT4BBpg7T5efeiriQv37").to_return(status: 200)
      @card = create(:card, author: @user, filestack: scorm_filestack)
      UploadToScormJob.expects(:perform_later)
      @card.upload_to_scorm
    end

    test 'should set state to published from processing' do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      stub_request(:get, "https://cdn.filestackcontent.com/yT4BBpg7T5efeiriQv37").to_return(status: 200)
      card = create(:card, state: 'processing', author: @user)
      card.publish!
      assert card.published?
    end

    test 'should set state to error from processing' do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      stub_request(:get, "https://cdn.filestackcontent.com/yT4BBpg7T5efeiriQv37").to_return(status: 200)
      card = create(:card, state: 'processing', author: @user)
      card.error!
      assert card.error?
    end

  end

  class EclCardDeletionTest < ActiveSupport::TestCase
    test 'should  call ecl card archive for non-ugc card' do
      org = create(:organization)
      card = create(:card, author_id: nil, organization: org, ecl_id: 'non-ugc-123')
      EclCardArchiveJob.expects(:perform_later).with('non-ugc-123', org.id).once
      card.destroy
    end

    test 'should call ecl card deletion for ugc card' do
      user = create(:user)
      card = create(:card, author_id: user.id, organization_id: user.organization_id, ecl_id: 'ugc-123')
      EclCardArchiveJob.expects(:perform_later).with('ugc-123', user.organization_id).once
      card.destroy
    end

    test 'should not call ecl card deletion if delete requested from ecl' do
      org = create(:organization)
      card = create(:card, author_id: nil, organization: org, ecl_id: 'non-ugc-123')
      EclCardArchiveJob.expects(:perform_later).never
      card.update_request_from_ecl = true
      card.destroy
    end
  end

  class EclCardTrashingTest < ActiveSupport::TestCase
    test 'should call ecl card trashing' do
      org = create(:organization)
      card = create(:card, author_id: nil, organization: org, ecl_id: 'test-123')
      EclCardTrashingJob.expects(:perform_later).with('test-123', org.id).returns(true)
      card.trash_from_ecl
    end
  end

  class SharedChannelCardDeleteTest < ActiveSupport::TestCase
    setup do
      TestAfterCommit.enabled = true
      @org1, @org2 = create_list(:organization, 2)
      @user = create(:user, organization: @org1)
      @org1_channel = create(:channel, organization: @org1, shareable: true)
      @org2_channel = create(:channel, organization: @org2, parent_id: @org1_channel.id)
    end

    test "should delete parent card reference of clone cards on deleting parent card" do
      card = create(:card, card_type: 'media', card_subtype: 'text', author_id: @user.id, title: 'This is card',
        message: 'This is message', organization: @org1, ecl_source_name: 'UGC')
      card.current_user = @user
      card.channels << @org1_channel
      CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
      assert_equal 1, card.cloned_cards.count

      card.destroy
      SyncCloneChannelCardsJob.perform_now({ parent_card_id: card.id, parent_channel_id: @org1_channel.id, action: 'destroy' })
      assert_equal 0, card.cloned_cards.count
    end

    test "should delete parent card reference of clone cards on deleting parent pathway" do
      cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover',
        organization: @org1, ecl_source_name: 'UGC')
      cards = create_list(:card, 2, author_id: @user.id, card_type: 'media')
      cards.each do |card|
        CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
      end
      cover.current_user = @user
      cover.channels << @org1_channel
      CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
      assert_equal 1, cover.cloned_cards.count
      assert_equal 2, cards.sum {|card| card.cloned_cards.count}

      cover.destroy
      SyncCloneChannelCardsJob.perform_now({ parent_card_id: cover.id, parent_channel_id: @org1_channel.id, action: 'destroy' })
      assert_equal 0, cover.cloned_cards.count
      assert_equal 0, cards.sum {|card| card.cloned_cards.count}
    end

    test "should delete parent card reference of clone cards on deleting parent journey" do
      cover = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: @user,
        organization: @org1, ecl_source_name: 'UGC')
      section = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user, organization: @org)
      cards = create_list(:card, 2, author_id: @user.id, card_type: 'media')
      cards.each do |card|
        CardPackRelation.add(cover_id: section.id, add_id: card.id, add_type: 'Card')
      end
      JourneyPackRelation.add(cover_id: cover.id, add_id: section.id)
      cover.current_user = @user
      cover.channels << @org1_channel
      CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
      assert_equal 1, cover.cloned_cards.count
      assert_equal 1, section.cloned_cards.count
      assert_equal 2, cards.sum {|card| card.cloned_cards.count}

      cover.destroy
      SyncCloneChannelCardsJob.perform_now({ parent_card_id: cover.id, parent_channel_id: @org1_channel.id, action: 'destroy' })
      assert_equal 0, cover.cloned_cards.count
      assert_equal 0, section.cloned_cards.count
      assert_equal 0, cards.sum {|card| card.cloned_cards.count}
    end
  end

  class CardAllTimeViewCountTest < ActiveSupport::TestCase

    test 'should return card view count and store in cache' do
      begin
        orig_cache = Rails.cache
        Rails.cache = ActiveSupport::Cache::MemoryStore.new

        now = Time.now
        Time.stubs(:now).returns now

        Card.any_instance.unstub(:all_time_views_count)
        card = create(:card)

        expected_query = "select count(user_id) from \"cards\" where _card_id = %{card_id} " +
          "AND event = %{event} AND time >= %{start_date}s AND time <= %{end_date}s ;"

        INFLUXDB_CLIENT.expects(:query).with(
          expected_query,
          params: { card_id: card.id.to_s,
            event: 'card_viewed', start_date: card.created_at.to_i,
            end_date: Time.now.utc.to_i
          }
        ).returns([{"name"=>"cards", "tags"=>nil, "values"=>[{"time"=>"1970-01-01T00:00:00Z", "count"=>28}]}])

        assert_equal 28, card.all_time_views_count

        recorder = Analytics::CardViewMetricsRecorder
        cache_key = recorder.card_view_count_cache_key(card)
        assert_equal 28, Rails.cache.fetch(cache_key)
      ensure
        Rails.cache = orig_cache
      end
    end

    test 'should cache the return value' do
      begin
        orig_cache = Rails.cache
        Rails.cache = ActiveSupport::Cache::MemoryStore.new

        card = create :card
        recorder = Analytics::CardViewMetricsRecorder
        cache_key = recorder.card_view_count_cache_key(card)
        Rails.cache.write cache_key, 5, raw: true

        # When we request the first time Influx is queried
        INFLUXDB_CLIENT.expects(:query).never
        Card.any_instance.unstub(:all_time_views_count)
        2.times { assert_equal 5, card.all_time_views_count }
      ensure
        Rails.cache = orig_cache
      end
    end

    test 'should return card view count when no event was recorded' do
      now = Time.now
      Time.stubs(:now).returns now

      Card.any_instance.unstub(:all_time_views_count)
      card = create :card

      expected_query = "select count(user_id) from \"cards\" where _card_id = %{card_id} " +
        "AND event = %{event} AND time >= %{start_date}s AND time <= %{end_date}s ;"

      INFLUXDB_CLIENT.expects(:query).with(
        expected_query,
        params: {
          card_id: card.id.to_s,
          event: 'card_viewed',
          start_date: card.created_at.to_i,
          end_date: Time.now.utc.to_i
        }
      ).returns([])

      assert_equal 0, card.all_time_views_count
    end

    test 'should return card view count 0 for ecl cards' do
      Card.any_instance.unstub(:all_time_views_count)
      card = build(:card, ecl_id: 'abcj12324sdjkf')

      INFLUXDB_CLIENT.expects(:query).never

      assert_equal 0, card.all_time_views_count
    end

    test 'should always return view count as integer' do
      # CONTEXT: Rails.cache always returns strings, even if the value is
      # stored internally as an integer.
      # Need to make sure it's always returned to the client as a string.

      Card.any_instance.unstub(:all_time_views_count)
      card = Card.new(id: 123123)
      # Stub the cache fetch to return a string
      Rails.cache.stubs(:fetch).returns "123"
      assert_equal 123, card.all_time_views_count
      # Stub the cache to return nil, then stub the Influx query to return a string
      Rails.cache.stubs(:fetch)
      Analytics::CardViewMetricsRecorder.stubs(
        :set_initial_card_view_count
      ).returns "456"
      assert_equal 456, card.all_time_views_count
    end
  end

  class VisibleCardToUserV2Test < ActiveSupport::TestCase
    setup do
      @organization = create :organization

      @user = create :user, organization: @organization
      @other_user = create :user, organization: @organization
      @to_be_restricted_user = create :user, organization: @organization
      @admin_user = create :user, organization: @organization, organization_role: 'admin'

      @card_common_params = {organization: @organization, card_type: 'media', card_subtype: 'text',
                             title: 'this is title', message: 'this is message', author: @user}
      #team1
      @team1 = create(:team, is_private: false, organization: @organization)
      #team1 users
      @team1_admin_user = create(:teams_user, team_id: @team1.id, user_id: @admin_user.id)
      @team1_to_be_restricted_user = create(:teams_user, team_id: @team1.id, user_id: @to_be_restricted_user.id)

      #team2
      @team2 = create(:team, is_private: false, organization:@organization)
      #team2 users
      @team2_other_user = create(:teams_user, team_id: @team2.id, user_id: @other_user.id)
      @team2_admin_user = create(:teams_user, team_id: @team2.id, user_id: @admin_user.id)

      #team1 follows Channel
      @channel = create(:channel, organization:@organization)

      #channel followers
      @teams_channels_follows1 = create(:teams_channels_follow, team_id: @team1.id, channel_id: @channel.id)
    end

    test 'visible_to_userV2 should not make card visible to shared team users if its restricted to team he is not part of'  do
      card =  create :card, @card_common_params.merge(is_public: false)
      channel_card = create(:channels_card, channel_id: @channel.id, card_id: card.id)
      assert_equal 1, Card.where(author_id:@user).visible_to_userV2(@to_be_restricted_user).length
      assert_equal 0, Card.where(author_id:@user).visible_to_userV2(@other_user).length

      team_card = create(:teams_card, team_id: @team2.id, card_id: card.id, type: 'SharedCard')
      assert_equal 1, Card.where(author_id:@user).visible_to_userV2(@other_user).length
      assert_equal 1, Card.where(author_id:@user).visible_to_userV2(@admin_user).length

      card_team_permissions = create(:card_team_permission, team_id: @team1.id, card_id: card.id)
      assert_equal 1, Card.where(author_id:@user).visible_to_userV2(@to_be_restricted_user).length
      assert_equal 0, Card.where(author_id:@user).visible_to_userV2(@other_user).length
    end

    test 'visible_to_userV2 should not make card visible to shared users if its restricted to team he is not part of' do
      card =  create :card, @card_common_params.merge(is_public: false)
      channel_card = create(:channels_card, channel_id: @channel.id, card_id: card.id)
      assert_equal 1, Card.where(author_id:@user).visible_to_userV2(@to_be_restricted_user).length
      assert_equal 0, Card.where(author_id:@user).visible_to_userV2(@other_user).length

      user_share = create(:card_user_share, user_id: @other_user.id, card_id: card.id)
      assert_equal 1, Card.where(author_id:@user).visible_to_userV2(@other_user).length
      assert_equal 1, Card.where(author_id:@user).visible_to_userV2(@admin_user).length

      card_team_permissions = create(:card_team_permission, team_id: @team1.id, card_id: card.id)
      assert_equal 1, Card.where(author_id:@user).visible_to_userV2(@to_be_restricted_user).length
      assert_equal 0, Card.where(author_id:@user).visible_to_userV2(@other_user).length
    end

    test 'visible_to_userV2 should make card visible to user if the user has permission and card is not posted/shared' do
      card =  create :card, @card_common_params.merge(is_public: false)
      create(:card_user_permission, card_id: card.id, user_id: @to_be_restricted_user.id)

      assert_equal 1, Card.where(author_id:@user).visible_to_userV2(@to_be_restricted_user).length
      assert_equal 0, Card.where(author_id:@user).visible_to_userV2(@other_user).length
    end

    test 'visible_to_userV2 should make card visible to user if the user has permission through team and card is not posted/shared' do
      card =  create :card, @card_common_params.merge(is_public: false)
      create(:card_team_permission, card_id: card.id, team_id: @team1.id)

      assert_equal 1, Card.where(author_id:@user).visible_to_userV2(@to_be_restricted_user).length
      assert_equal 0, Card.where(author_id:@user).visible_to_userV2(@other_user).length
    end
  end
end

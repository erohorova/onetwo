# -*- coding: utf-8 -*-
require 'test_helper'

class UserRoleTest < ActiveSupport::TestCase

  setup do
    @org = create :organization
    @actor = create :user, organization: @org
    @user = create :user, organization: @org
    @role = Role.create(
      name: "role_name",
      organization: @org,
    )
  end

  should belong_to :user
  should validate_presence_of :user
  should belong_to :role
  should validate_presence_of :role

  test 'pushes metric upon creation' do
    metadata = { foo: "bar", user_id: @actor.id }
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    stub_time
    user_role = UserRole.new(user: @user, role: @role)
    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::UserMetricsRecorder",
      timestamp: Time.now.to_i,
      event: "user_role_added",
      user: Analytics::MetricsRecorder.user_attributes(@user),
      actor: Analytics::MetricsRecorder.user_attributes(@actor),
      added_role: "role_name",
      org: Analytics::MetricsRecorder.org_attributes(@org),
      additional_data: metadata
    )
    user_role.run_callbacks(:commit) {user_role.save}
    assert user_role.persisted?
  end

  test 'pushes metric upon deletion' do
    metadata = { foo: "bar", user_id: @actor.id }
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    stub_time
    user_role = UserRole.new(user: @user, role: @role)
    user_role.stubs(:push_user_role_added_event)
    user_role.save
    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::UserMetricsRecorder",
      timestamp: Time.now.to_i,
      event: "user_role_removed",
      user: Analytics::MetricsRecorder.user_attributes(@user),
      actor: Analytics::MetricsRecorder.user_attributes(@actor),
      org: Analytics::MetricsRecorder.org_attributes(@org),
      removed_role: "role_name",
      additional_data: metadata
    )
    user_role.destroy
    refute user_role.persisted?
  end

end

require 'test_helper'

class FakeTestClass < PaymentGateway
end

class PaymentGatewayTest < ActiveSupport::TestCase
  test '.gateway_for_currency' do
    assert_equal 'wallet', PaymentGateway.gateway_for_currency(
                   Settings.payment_currencies['edu_currency'])
    assert_equal 'braintree', PaymentGateway.gateway_for_currency('USD')
  end

  test '.gateway_cls' do
    assert_equal WalletPaymentGateway, PaymentGateway.gateway_cls('wallet')
    assert_equal BraintreePaymentGateway, PaymentGateway.gateway_cls('braintree')
    assert_equal PaypalPaymentGateway, PaymentGateway.gateway_cls('paypal')
  end

  test 'should raise error if child class has not implemented authorize_payment' do
    exception = assert_raises(Exception) { FakeTestClass.new.authorize_payment(nil) }
    assert_equal NotImplementedError, exception.class
  end

  test 'should raise error if child class has not implemented capture_payment' do
    exception = assert_raises(Exception) { FakeTestClass.new.capture_payment(nil) }
    assert_equal NotImplementedError, exception.class
  end

  test 'should raise error if child class has not implemented void_payment' do
    exception = assert_raises(Exception) { FakeTestClass.new.void_payment(nil) }
    assert_equal NotImplementedError, exception.class
  end

end

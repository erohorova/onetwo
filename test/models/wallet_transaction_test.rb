require 'test_helper'

class WalletTransactionTest < ActiveSupport::TestCase
  should have_db_column(:order_id).of_type(:integer)
  should have_db_column(:user_id).of_type(:integer)
  should have_db_column(:gateway_data).of_type(:text)
  should have_db_column(:amount).of_type(:decimal)
  should have_db_column(:payment_state).of_type(:integer)

  should belong_to(:order)
  should belong_to(:user)

  should validate_presence_of(:order)
  should validate_presence_of(:user)
  should validate_presence_of(:amount)
  should validate_presence_of(:payment_state)


  test 'should trigger email notification job on create if payment type is credit' do
    user = create(:user)
    recharge = create(:recharge)
    order = create(:order, orderable: recharge)
    transaction = create(:transaction, order: order)
    wallet_transaction = create(:wallet_transaction, order: order, user: user, payment_type: 0)

    mailer_mock = mock()
    mailer_mock.expects(:deliver_later).returns(nil).times(1)
    PaymentMailer.expects(:wallet_recharge_notification).with(wallet_transaction.id)
                 .returns(mailer_mock).once

    wallet_transaction.run_callbacks(:commit)
  end

  test 'should not trigger email notification job on create if payment type is debit' do
    user = create(:user)
    recharge = create(:recharge)
    order = create(:order, orderable: recharge)
    transaction = create(:transaction, order: order)
    wallet_transaction = create(:wallet_transaction, order: order, user: user, payment_type: 1)

    mailer_mock = mock()
    mailer_mock.expects(:deliver_later).never
    PaymentMailer.expects(:wallet_recharge_notification).never

    wallet_transaction.run_callbacks(:commit)
  end

  # Since other payment gateways return authorization complete status in the success? method, we need to use the same
  test '#success? must return true if payment is in pending state' do
    wallet_transaction = create(:wallet_transaction, payment_type: 1)
    wallet_transaction.start_processing!
    wallet_transaction.complete_processing!
    assert wallet_transaction.success?
  end

  test '#capture must update the wallet balance of the user if state is pending' do
    user = create(:user)
    wallet_balance = create(:wallet_balance, user: user)
    create(:wallet_transaction, user: user, payment_type: 0, amount: BigDecimal.new("100"), payment_state: 1)
    wallet_transaction = create(:wallet_transaction, user: user, payment_type: 1, amount: BigDecimal.new("25"))
    wallet_transaction.start_processing!
    wallet_transaction.complete_processing!
    wallet_transaction.capture

    assert_equal BigDecimal.new("75"), wallet_balance.reload.amount
  end

end
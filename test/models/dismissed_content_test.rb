require 'test_helper'

class DismissedContentTest < ActiveSupport::TestCase

  test 'when a dismissed content is created its dequeued' do
    user = create(:user)
    content = create(:card)
    UserContentsQueue.expects(:remove_from_queue).with(user, content)
    dismissed_content = DismissedContent.create(user: user, content: content)
    dismissed_content.run_callbacks(:commit)
  end

  test "#send_dismissed_card_influx_metric calls recorder job" do
    org = create :organization
    actor = create(:user, organization: org)
    author = create :user, organization: org
    card = create :card, author: author

    metadata = { user_id: actor.id }
    stub_time = Time.now
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    Analytics::MetricsRecorderJob.unstub :perform_later

    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        recorder: "Analytics::CardMetricsRecorder",
        org: Analytics::MetricsRecorder.org_attributes(org),
        card: Analytics::MetricsRecorder.card_attributes(card),
        event: 'card_dismissed',
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        timestamp: stub_time.to_i,
        additional_data: metadata
      )
    )
    Timecop.freeze stub_time do
      dismissed = create :dismissed_content, content: card
      dismissed.run_callbacks :commit
    end
  end
end

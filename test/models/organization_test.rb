require 'test_helper'
class OrganizationTest < ActiveSupport::TestCase
  should have_many(:channels)
  should have_many(:org_ssos)
  should have_many(:ssos)
  should have_many :topic_requests
  should have_many(:widgets)

  test 'should auto create general channel' do
    Organization.any_instance.unstub :create_general_channel
    org = create(:organization, name: 'ge')
    gc = org.general_channel
    assert_not_nil gc
    assert_equal "general", gc.slug
  end

  test 'should auto add organization members to general channel' do
    skip #THIS TEST DONT BELONG HERE
    Organization.any_instance.unstub :create_general_channel
    org = create(:organization)
    a = create(:user, organization: org, organization_role: 'admin')
    assert org.general_channel.is_author?(a)

    m = create(:user, organization: org, organization_role: 'member')
    assert org.general_channel.is_follower?(m)
  end

  test 'should get web session timeout' do
    org = create(:organization)
    web_session_timeout = Faker::Number.number(10)
    create(:config, name: 'web_session_timeout', value: web_session_timeout, configable: org)

    assert_equal web_session_timeout, org.web_session_timeout
  end

  test 'should get inactive web session timeout' do
    org = create(:organization)
    inactive_web_session_timeout = Faker::Number.number(10)
    create(:config, name: 'inactive_web_session_timeout', value: inactive_web_session_timeout, configable: org)

    assert_equal inactive_web_session_timeout, org.inactive_web_session_timeout
  end

  test 'should get auto_mark_complete if config is set' do
    org = create(:organization)
    create(:config, name: 'auto_mark_as_complete_scorm_cards', value: true, configable: org)
    assert org.auto_mark_as_complete_enabled?
  end

  test 'should get false if auto_mark_complete is not set' do
    org = create(:organization)
    refute org.auto_mark_as_complete_enabled?
  end

  test 'reporting_content_enabled? should be true if reporting config is set' do
    org = create(:organization)
    create(:config, name: 'reporting_content', value: true, configable: org)
    assert org.reporting_content_enabled?
  end

  test 'reporting_content_enabled? should be false if reporting config is not set' do
    org = create(:organization)
    refute org.reporting_content_enabled?
  end

  test 'leaderboard_enabled? should be true if leaderboard config is set' do
    org = create(:organization)
    create(:config, name: 'leaderboard', value: true, configable: org)
    assert org.leaderboard_enabled?
  end

  test 'leaderboard_enabled? should be false if leaderboard config is not set' do
    org = create(:organization)
    refute org.leaderboard_enabled?
  end

  test 'trending_org_journey? should be true if config is set' do
    org = create(:organization)
    create(:config, name: 'enable_trending_org_journey', value: true, configable: org)
    assert org.trending_org_journey?
  end

  test 'trending_org_journey? should be false if config is not set' do
    org = create(:organization)
    refute org.trending_org_journey?
  end

  test '#org_subscription_enabled should return true if member_pay is set' do
    org = create(:organization)
    create(:organization_profile, organization: org, member_pay: true)
    assert org.org_subscription_enabled?
  end

  test '#org_subscription_enabled should return false if there is no organization profile record' do
    org = create(:organization)
    refute org.org_subscription_enabled?
  end

  test 'should validate format of hostname' do
    org = build(:organization, host_name: 'www.test')
    assert !org.save
    assert !org.full_errors.index('Host name').nil?
  end

  test 'should create default configs after org create' do
    Organization.any_instance.unstub :create_default_config
    org = create(:organization)

    configs = Config.where(configable: org)

    assert_not_empty configs
    assert_equal OrganizationDefaultConfigs.default_configs.count, configs.count
  end

  test 'should create clc and card badges after org create' do
    Organization.any_instance.unstub :create_org_card_badges
    org = create(:organization)
    org.run_callbacks(:commit)
    org.reload

    assert_equal Settings.badge_image_urls.count, org.card_badges.count
    assert_equal Settings.badge_image_urls.count, org.clc_badges.count
  end

  test 'should return sharing permissions for user' do
    skip #deprecated
    org = create(:organization)
    user = create(:user, organization: org)

    assert org.get_sharing_permissions

    org.configs.where(name: 'permissions/social_sharing/can_share_channel_on_social_nw').first.update(value: 'false')
    org.reload

    assert_not org.get_sharing_permissions
  end

  test 'should return channel create permissions for user' do
    skip #deprecated
    org = create(:organization)
    member = create(:user, organization: org)
    admin = create(:user, organization: org, organization_role: 'admin')

    assert org.get_channel_permissions_for(member)
    assert org.get_channel_permissions_for(admin)

    org.configs.where(name: 'permissions/channel/member_can_create_channel').first.update(value: 'false')
    org.reload

    assert_not org.get_channel_permissions_for(member)
    assert org.get_channel_permissions_for(admin)
  end

  test 'should check notification is enabled by default' do
    Organization.any_instance.unstub :create_default_config
    org = create(:organization)
    member = create(:user, organization: org)
    admin = create(:user, organization: org, organization_role: 'admin')

    assert org.configs.where(name: 'feature/notifications_and_triggers_enabled', value: 'true').any?

    org.configs.where(name: 'feature/notifications_and_triggers_enabled').first.update(value: 'false')
    org.reload

    refute org.configs.where(name: 'feature/notifications_and_triggers_enabled', value: 'true').any?
  end

  test 'should be able to setting display of sub-brand image on login page' do
    # test the default value is true
    org = create(:organization)

    assert_equal true, org.show_sub_brand_on_login

    # test the toggle
    org.show_sub_brand_on_login = false
    org.save
    org.reload

    assert_equal false, org.show_sub_brand_on_login
  end

  test 'should fetch the custom splash image' do
    org = create(:organization)
    config = create(:config, name: 'custom_splash_screen', value: 'http://www.someimage.com/temp_image.png', configable: org)
    assert_equal 'http://www.someimage.com/temp_image.png', org.fetch_splash_image_url
    config.destroy!
    assert_nil org.fetch_splash_image_url
  end

  test 'real users' do
    Organization.any_instance.unstub :create_admin_user
    org = create(:organization)
    client = create(:client, organization: org)
    user = create(:user, organization: org)
    suspended_user = create(:user, organization: org, is_suspended: true)
    forum_user = create(:user, organization: org, is_complete: false)
    assert_equal 4, org.users.count
    assert_equal 1, org.real_users.count
  end

  test 'new users count' do
    org = create(:organization)
    Timecop.freeze(26.hours.ago) do
      create(:user, organization: org)
    end

    Timecop.freeze(16.hours.ago) do
      create(:user, organization: org)
    end

    timerange = [1.day.ago, Time.now.utc]
    PeriodOffsetCalculator.expects(:timerange).with(period: :day, offset: 0).returns(timerange).once

    assert_equal 1, org.get_new_users_count(period: :day, offset: 0)
  end

  test 'new users count over span' do
    org = create(:organization)
    Timecop.freeze(26.hours.ago) do
      create(:user, organization: org)
    end

    Timecop.freeze(16.hours.ago) do
      create(:user, organization: org)
    end

    assert_equal 1, org.get_new_users_count_over_span(begin_time: 24.hours.ago, end_time: Time.now.utc)
    assert_equal 2, org.get_new_users_count_over_span(begin_time: 48.hours.ago, end_time: Time.now.utc)
  end

  test 'real users count' do
    org1, org2 = create_list(:organization, 2)
    Timecop.freeze(20.hours.ago) do
      create_list(:user, 3, organization: org1)
    end

    create_list(:user, 2, organization: org1)

    # distraction
    create(:user, organization: org2)

    assert_equal 5, org1.real_users_count_before(Time.now.utc)
    assert_equal 3, org1.real_users_count_before(15.hours.ago)
  end

  test 'new users drill down' do
    org = create(:organization)

    # 2 users created between 4 days and 3 days ago
    Timecop.freeze(80.hours.ago) do
      create(:user, organization: org)
      create(:user, organization: org)
    end

    # 1 user created between 3 days and 2 days ago
    Timecop.freeze(55.hours.ago) do
      create(:user, organization: org)
    end

    # 1 user created between 1 day ago and now
    Timecop.freeze(16.hours.ago) do
      create(:user, organization: org)
    end

    timeranges = [[4.days.ago.utc, 3.days.ago.utc], [3.days.ago.utc, 2.days.ago.utc], [2.days.ago.utc, 1.day.ago.utc], [1.day.ago.utc, Time.now.utc]]
    PeriodOffsetCalculator.expects(:drill_down_timeranges).with(period: :week, offset: 0).returns(timeranges).once

    assert_equal [2, 1, 0, 1], org.get_new_users_drill_down_data(period: :week, offset: 0)
    assert_equal [0, 1], org.get_new_users_drill_down_data_for_timeranges(timeranges: [[2.days.ago.utc, 1.day.ago.utc], [1.day.ago.utc, Time.current]])
  end

  test 'new user drill down for a day should just be that day' do
    skip 'intermittent failure that wont go away'
    org = create(:organization)
    now = Time.now.utc
    create(:user, organization: org, created_at: now - 26.hours)
    create(:user, organization: org, created_at: now - 16.hours)

    applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)
    assert_equal [1], org.get_new_users_drill_down_data(period: :day, offset: applicable_offsets[:day])
  end

  test 'create roles when we enable role based authorization' do
    User.any_instance.unstub :update_role
    Organization.any_instance.unstub :create_or_update_roles
    Organization.any_instance.unstub :create_default_roles
    Organization.any_instance.unstub :create_admin_user

    assert_difference -> {Role.count}, 8 do
      random_org = create(:organization, enable_role_based_authorization: true)
      random_user = create(:user, organization_role: 'member', organization: random_org)
      random_org.run_callbacks(:commit)
      user = create(:user, organization_role: 'admin', organization: random_org)
      assert user.roles.where(name: Role::TYPE_ADMIN).exists?
      assert random_org.admins.first.roles.where(name: Role::TYPE_ADMIN).exists?
      assert random_org.members.first.roles.where(name: Role::TYPE_MEMBER).exists?
    end

    org = create(:organization)
    org.enable_role_based_authorization = true
    assert_difference -> {Role.count}, 5 do
      org.save
      org.run_callbacks(:commit)
      assert org.admins.first.roles.where(name: Role::TYPE_ADMIN).exists?
    end
  end

  test 'should select custom active mailer config from mailer_config' do
    org = create(:organization, name: 'a', host_name: 'a')
    mailer_config = create(:mailer_config, organization: org, from_address: 'testactive@edcast.com', from_admin: true, is_active: true)

    assert_equal mailer_config.id, org.mailer_config.id
    assert_equal 'testactive@edcast.com', org.from_address
  end

  test 'should have CREATE_TEXT_CARD permission enabled for newly created org' do
    org = create(:organization, name: 'a', host_name: 'a')
    create_org_master_roles org
    role = org.roles.where(name: 'member').first
    create_text_card_permission = role.role_permissions.where(name: 'CREATE_TEXT_CARD').first

    assert_equal 'CREATE_TEXT_CARD', create_text_card_permission.name
  end

  test 'should send default from_address if mailer_config is not active' do
    org = create(:organization, name: 'a', host_name: 'a')
    mailer_config = create(:mailer_config, organization: org, from_address: 'testactive@edcast.com', from_admin: true, is_active: false)

    assert_nil org.mailer_config
    assert_equal 'admin@edcast.com', org.from_address
  end

  test 'should send from_address from mailer_config' do
    org = create(:organization, name: 'a', host_name: 'a')
    mailer_config = create(:mailer_config, organization: org, from_address: 'test@edcast.com')

    assert_equal 'test@edcast.com', org.from_address
  end


  test 'should send default from_address if mailer_config not present' do
    org = create(:organization, name: 'a', host_name: 'a')

    assert_equal 'admin@edcast.com', org.from_address
  end

  class OrganizationEmailsTest < ActiveSupport::TestCase
    include ActiveJob::TestHelper
    test "should enqueue invite user reminder job for org with no users created 10, 5, 2 days ago" do
      OrgAdminReminderNotificationJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later

      [10, 5, 2].each do |number|
        org = create(:organization, created_at: (number.days.ago-2.hours))
        admin = create(:user, organization: org, organization_role: 'admin')
        assert_enqueued_with(job: OrgAdminReminderNotificationJob) do
          Organization.remind_admin_to_invite_users
        end
      end
    end

    test "should not enqueue invite user reminder job for org with no users created 3 days ago" do
      org = create(:organization, created_at: (4.days.ago-2.hours))
      admin = create(:user, organization: org, organization_role: 'admin')
      assert_no_enqueued_jobs do
        Organization.remind_admin_to_invite_users
      end
    end

    test "should not enqueue invite user reminder job for org with no users for orgs with more than one user" do
      OrgAdminReminderNotificationJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later

      orgs = create_list(:organization, 5, created_at: (2.days.ago-2.hours))
      orgs.each { |org| create(:user, organization: org, organization_role: 'admin') }
      orgs[0..1].each { |org| create(:user, organization: org) }
      assert_enqueued_jobs 3 do
        Organization.remind_admin_to_invite_users
      end
    end

    test 'should enqueue jobs for all orgs to setup weekly activity' do
      OrganizationSendWeeklyActivityJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later

      orgs = create_list(:organization, 2, send_weekly_activity: true)
      assert_enqueued_jobs 2 do
        Organization.setup_weekly_activity_jobs
      end
    end

    test 'should send weekly activity to all users' do
      org = create(:organization)
      user = create(:user, organization: org)
      unsubscribed_user = create(:user, organization: org)
      unsubscribed_user.pref_notification_weekly_activity = false
      OrgUserSendWeeklyActivityEmailBatchJob.expects(:perform_later).with({ organization_id: org.id, user_ids: [user.id, unsubscribed_user.id] })
      org.send_weekly_activity_email
    end

    test 'should not send mail to users who havent completed onboarding' do
      org = create(:organization)
      user = create(:user, organization: org)
      not_onboarded_user = create(:user, organization: org, is_complete: false)
      OrgUserSendWeeklyActivityEmailBatchJob.expects(:perform_later).with({ organization_id: org.id, user_ids: [user.id] })
      org.send_weekly_activity_email
    end

    test 'should create an admin user' do
      Organization.any_instance.unstub :create_admin_user
      org = nil
      assert_difference -> {User.count} do
        org = create(:organization)
      end
      user = User.last
      assert_equal 'admin', user.organization_role
      assert_equal org.id, user.organization_id
      assert_equal '@adminuser', user.handle
    end

    test 'should create everyone group when org is created' do
      Organization.any_instance.unstub :create_org_level_team
      org = create(:organization)
      org.run_callbacks(:commit)
      org.reload

      assert_equal 1, org.teams.count
      assert_equal 'Everyone', org.teams.last.name
    end

    test 'featured_courses' do
      org = create :organization
      group = create :resource_group, organization: org, is_promoted: true
      assert_includes org.featured_courses, group
    end
  end

  test 'exclude user ids for analytics' do
    Organization.any_instance.unstub :create_admin_user
    User.any_instance.unstub :associate_forum_user_if_present
    # Creating an org creates an edcast admin user automatically
    org = create(:organization)

    suspended_user = create(:user, organization: org, is_suspended: true)
    normal_user = create(:user, organization: org)
    forum_user = create(:user, organization: org, is_complete: false)

    assert_same_elements [suspended_user, forum_user, org.users.where(is_edcast_admin: true).first].map(&:id), org.exclude_user_ids_for_analytics
  end

  test 'should check if email is registrable for org' do
    org = create(:organization)

    # Should allow if AllowedEmailDomains config is not present
    assert org.email_registrable?("test@example.com")

    # Disallow any email domain
    config = create(:config, name: 'AllowedEmailDomains', value: 'none', configable: org)
    refute org.email_registrable?('test@example.com')

    # Allow every email domain
    config.update(value: 'all')
    assert org.email_registrable?('test@example.com')

    # Whitelist certain email domains
    config.update(value: 'http://www.test.com,abc.com,AB.com')
    refute org.email_registrable?(nil)
    refute org.email_registrable?("")
    refute org.email_registrable?('check@example.com')
    assert org.email_registrable?('check@abc.com')
    assert org.email_registrable?('check@test.com')
    assert org.email_registrable?('check@ab.com')

    # Disallow a specific email domain
    config.update(value: 'not:abc.com, xyz.com')
    refute org.email_registrable?('new@abc.com')
    refute org.email_registrable?('new@xyz.com')
    assert org.email_registrable?('new@ab.com')
  end

  test 'should check if sign_up in org is allowed or not' do
    org = create(:organization)
    assert org.is_sign_up_allowed?

    config = create(:config, name: 'AllowedEmailDomains', value: 'all', configable: org)
    assert org.is_sign_up_allowed?

    config.update(value: 'none')
    refute org.is_sign_up_allowed?
  end

  class WeeklyActivityTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false

    setup do
      skip #deprecated
      # WebMock.disable!
    end

    teardown do
      # WebMock.enable!
    end

    test 'should get specfic attributes for weekly activity mail' do
      org = create(:organization)
      author =  create(:user, organization_id: org.id)
      resource = create(:resource)
      file_resource = create(:file_resource, attachable: resource)
      # insight
      create(:card, author: author, organization_id: org.id, resource: resource)

      # video stream
      create(:wowza_video_stream, creator: author, organization_id: org.id, state: 'published')

      # pathways
      cover = create(:card, card_type: 'pack', card_subtype: 'text', author: author, organization_id: org.id, resource: resource)
      CardPackRelation.add(cover_id: cover.id, add_id: create(:card, author: author).id)
      cover.publish!

      Card.searchkick_index.refresh
      VideoStream.searchkick_index.refresh

      activity = org.weekly_activity_mail_attributes

      insight = activity[:insights][0]
      assert_equal ["slug", "title", "message", "author", "small_image_url"] , insight.keys
      assert_equal ["handle", "name", "picture"], insight.author.keys

      video_stream = activity[:video_streams][0]
      assert_equal ["name", "slug", "creator"], video_stream.keys
      assert_equal ["name", "photo", "handle"], video_stream.creator.keys

      pathway = activity[:pathways][0]
      assert_equal ["slug", "title", "message", "author", "small_image_url"], pathway.keys
      assert_equal ["handle", "name", "picture"], pathway.author.keys
    end
  end

  test '#onboarding_options returns list of onboarding options at an org level' do
    default_steps = UserOnboarding::DEFAULT_ONBOARDING_STEPS
    optional_steps = UserOnboarding::OPTIONAL_ONBOARDING_STEPS

    org    = create :organization
    config = create :config, configable_id: org.id, configable_type: 'Organization',
      name: 'onboarding_options', value: default_steps.merge(optional_steps).to_json, data_type: 'json'

    # 1. Returns list of steps configured at an org level
    assert_equal default_steps.merge(optional_steps), org.reload.onboarding_options

    # 2. Enforces profile_setup as a mandatory step
    config.update(value: { add_interests: 2 }.to_json)
    assert_equal default_steps.merge({ 'add_interests' => 2 }), org.reload.onboarding_options

    # 3. Enforces profile_setup as a mandatory step
    config.delete
    assert_equal default_steps, org.reload.onboarding_options
  end

  test 'add default `onboarding_options` to newly created organization' do
    Organization.any_instance.unstub :add_onboarding_options
    organization = create :organization
    organization.run_callbacks(:commit)

    config = organization.configs.find_by(name: 'onboarding_options')
    expected_config = { 'profile_setup' => 1, 'add_interests' => 2, 'add_expertise' => 3 }
    assert_equal expected_config, config.parsed_value
  end

  test 'should validate custom domain #valid' do
    assert create(:organization, custom_domain: 'learn.something.com')
    assert create(:organization, custom_domain: 'something.com')
  end

  test 'should validate custom domain #invalid' do
    assert_raise ActiveRecord::RecordInvalid do
      create(:organization, custom_domain: 'invalid123.domain#')
    end
  end

  test 'should return custom domain home page' do
    custom_domain = 'learn.something.com'
    org = create(:organization, custom_domain: custom_domain)
    assert_equal org.home_page, "https://#{custom_domain}"
  end

  test 'should return custom domain host' do
    custom_domain = 'learn.something.com'
    org = create(:organization, custom_domain: custom_domain)
    assert_equal org.host, custom_domain
  end

  test 'should send user content analytics #content' do
    reporting_enabled_org = create(:organization, enable_analytics_reporting: true)
    valid_users = create_list(:user, 2, organization_role: 'admin', organization: reporting_enabled_org)

    Timecop.freeze do
      timestamp = Time.now.prev_week.utc
      snippet = PeriodOffsetCalculator::timestamp_to_readable_period(timestamp: timestamp, period: :week)

      reporting_mock = mock()
      user_mailer = mock()
      aws_url = mock()

      user_mailer.expects(:deliver_later).once
      reporting_mock.expects(:generate_report).once.returns(aws_url)

      AnalyticsReport::UserContentActivityReport.expects(:new).with(organization: reporting_enabled_org, timestamp: timestamp, period: :week, report_type: 'content').returns(reporting_mock)
      UserMailer.expects(:send_organization_report).with(s3_url: aws_url, user_ids: valid_users.map(&:id), email_title_snippet: snippet).returns(user_mailer)

      reporting_enabled_org.send_user_content_analytics
    end
  end

  test 'should send user content analytics #social' do
    reporting_enabled_org = create(:organization, enable_analytics_reporting: true)
    valid_users = create_list(:user, 2, organization_role: 'admin', organization: reporting_enabled_org)

    Timecop.freeze do
      timestamp = Time.now.prev_week.utc
      snippet = PeriodOffsetCalculator::timestamp_to_readable_period(timestamp: timestamp, period: :week)

      reporting_mock = mock()
      user_mailer = mock()
      aws_url = mock()

      user_mailer.expects(:deliver_later).once
      reporting_mock.expects(:generate_report).once.returns(aws_url)

      AnalyticsReport::UserContentActivityReport.expects(:new).with(organization: reporting_enabled_org, timestamp: timestamp, period: :week, report_type: 'social').returns(reporting_mock)
      UserMailer.expects(:send_organization_report).with(s3_url: aws_url, user_ids: valid_users.map(&:id), email_title_snippet: snippet).returns(user_mailer)

      reporting_enabled_org.send_user_content_analytics(report_type: 'social')
    end
  end

  test 'should find org by request host' do
    edcast_host = "test1"
    platform_host = "#{edcast_host}.#{Settings.platform_host}"
    custom_domain = "learn.something.com"

    platform_org = create(:organization, host_name: edcast_host)
    custom_org = create(:organization, host_name: 'awesome', custom_domain: custom_domain)

    assert_equal platform_org, Organization.find_by_request_host(platform_host)
    assert_equal custom_org, Organization.find_by_request_host(custom_domain)
  end

  test 'should send daily org analytics report' do
    reporting_enabled_org = create(:organization, enable_analytics_reporting: true)
    valid_users = create_list(:user, 2, organization_role: 'admin', organization: reporting_enabled_org)

    Timecop.freeze do
      timestamp = Time.now.utc - 1.day
      snippet = PeriodOffsetCalculator::timestamp_to_readable_period(timestamp: timestamp, period: :day)

      reporting_mock = mock()
      user_mailer = mock()
      aws_url = mock()

      user_mailer.expects(:deliver_later).once
      reporting_mock.expects(:generate_report).once.returns(aws_url)

      AnalyticsReport::OrganizationAnalyticsReport.expects(:new).with(organization: reporting_enabled_org, timestamp: timestamp, period: :day).returns(reporting_mock)
      UserMailer.expects(:send_organization_report).with(s3_url: aws_url, user_ids: valid_users.map(&:id), email_title_snippet: snippet).returns(user_mailer)

      reporting_enabled_org.send_org_analytics
    end
  end

  test 'should create reporting config #create' do
    IndexJob.stubs(:perform_later)
    Organization.any_instance.unstub :create_analytics_reporting_configs
    TestAfterCommit.with_commits do
      org = create(:organization, enable_analytics_reporting: true)
      config = org.configs.find_by_name("AnalyticsReportingConfig")
      assert_not_nil config
      assert_not_nil config.parsed_value
    end
  end

  test 'should create reporting config #update' do
    IndexJob.stubs(:perform_later)
    Organization.any_instance.unstub :create_analytics_reporting_configs

    TestAfterCommit.with_commits do
      org = create(:organization)
      assert_nil org.configs.find_by_name("AnalyticsReportingConfig")

      org.update(enable_analytics_reporting: true)
      config = org.configs.find_by_name("AnalyticsReportingConfig")
      assert_not_nil config
      assert_not_nil config.parsed_value
    end
  end

  test "#create_admin_user is called after create" do
    skip #redundant
    org = Organization.new(name: "111", description: "222", host_name: "333")
    org.expects(:create_admin_user)
    org.save!
  end

  test "#create_admin_user creates the correct admin user" do
    Organization.any_instance.unstub :create_admin_user
    org = Organization.new(name: "111", description: "222", host_name: "333")
    stub_password = "bar"
    User.stubs(:random_password).returns stub_password
    stub_user = User.new
    User.expects(:new).with(
      first_name: "Admin",
      last_name: "User",
      organization: org,
      organization_role: "admin",
      is_edcast_admin: true,
      password: stub_password
    ).returns stub_user
    stub_user.expects(:skip_indexing=).with(true)
    stub_user.expects(:skip_generate_images=).with(true)
    stub_user.expects(:skip_reconfirmation!)
    stub_user.expects(:skip_confirmation!)
    stub_user.expects(:save!)
    org.create_admin_user
  end

  test "should push influx event when edited" do
    org = create :organization, name: "old_name"
    actor = create :user, organization: org
    stub_time
    metadata = { user_id: actor.id }
    RequestStore.expects(:read).with(:request_metadata).returns metadata
    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        recorder: "Analytics::OrgEditedMetricsRecorder",
        org: Analytics::MetricsRecorder.org_attributes(org).merge(
          "updated_at" => Time.now.to_i,
          "name" => "new_name"
        ),
        changed_column: "name",
        old_val: "old_name",
        new_val: "new_name",
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        event: "org_edited",
        additional_data: metadata,
        timestamp: Time.now.to_i
      )
    )

    org.update name: "new_name"
    assert_equal org.name, "new_name"
  end

  test "should push influx event when created" do
    user_org = create :organization
    actor = create :user, organization: user_org
    metadata = { user_id: actor.id }
    RequestStore.expects(:read).with(:request_metadata).returns metadata

    new_org_id = Organization.last.id + 1
    host_name = SecureRandom.hex
    new_org = Organization.new(
      id:          new_org_id,
      host_name:   host_name,
      name:        host_name,
      description: host_name,
      slug:        host_name
    )

    stub_time
    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        recorder: "Analytics::OrgMetricsRecorder",
        org: Analytics::MetricsRecorder.org_attributes(new_org).merge(
          "created_at" => Time.now.to_i,
          "updated_at" => Time.now.to_i
        ),
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        event: "org_created",
        additional_data: metadata,
        timestamp: Time.now.to_i
      )
    )
    new_org.run_callbacks(:commit) do
      new_org.save!
    end
  end

  test 'should not pushed ignored events' do
    org = create :organization, name: "foo"
    actor = create :user, organization: org

    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(:perform_later).never
    time = Time.now
    org.update created_at: time
    assert_equal time, org.created_at
  end

  test 'should auto create default skill for career_advisor' do
    Organization.any_instance.unstub :create_default_skill_for_career_advisor
    org = create(:organization, name: 'foo')
    ca = org.career_advisors.pluck(:skill)
    assert_same_elements CareerAdvisor::SKILLS, ca
  end

  test 'should auto create default email templates for org' do
    TestAfterCommit.with_commits do
      CreateCustomEmailTemplateJob.expects(:perform_later).once
      org = create(:organization, name: 'foo')
    end
  end

  test 'should create ecl settings after new org creation' do
    Organization.any_instance.unstub(:enable_sociative)

    TestAfterCommit.with_commits do
      Organization.any_instance.expects(:enable_sociative).once
      organization = create :organization
    end
  end

  class TrashedCardsOrganizationTest < ActiveSupport::TestCase
    setup do
      @organization = create(:organization)
      @user = create(:user, organization: @organization)

      author = create(:user, organization: @organization)
      @trashable_card = create(:card, organization: @organization, author: author, ecl_id: 'test-trash-ecl_id')
      @trashable_card.update_attributes(deleted_at: Time.now)
      create(:card_reporting, card: @trashable_card, user: @user, deleted_at: Time.now)
    end

    test 'should return trashed ecl id for org' do
      assert_equal [@trashable_card.ecl_id], @organization.trashed_ecl_ids
    end
  end
end

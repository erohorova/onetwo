require 'test_helper'

class ContextTest < ActiveSupport::TestCase

  class TestExperimentContextProvisioner
    attr_accessor :contexts do {} end

    def styles
      User.find(@contexts[:user_id]).try(:first_name) == 'test user' ? [:style1] : [:style2]
    end

    def properties(styles)
      style_properties = {
          style1: {
              'name' => 'value1',
          },
          style2: {
              'name' => 'value2',
          },
      }
      return style_properties.slice(*styles).values.reduce(&:merge)
    end
  end

  test 'yield contextual configuration values' do

    # default values
    create(:configuration, name: 'name', value: 'default_value', type: 'Configuration')
    create(:configuration, name: 'name2', value: 'default_value2', type: 'Configuration')

    assert_equal 'default_value', Configuration.cfg('name')

    # experiment context
    test_user = create(:user, first_name: 'test user')

    # set context provisioner
    Context.provisioners << TestExperimentContextProvisioner.new

    # initialize context (user and time)
    assert_difference ->{ Context.count } do
      Context.init(user_id: test_user.id, time: Time.now)
    end

    # default value overridden in experiment context
    assert_equal 'value1', Configuration.cfg('name')

    # experiment context saved
    assert_equal test_user.id, Context.last.context_set[:user_id]

    # other default value not overridden
    assert_equal 'default_value2', Configuration.cfg('name2')
  end

end

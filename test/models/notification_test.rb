require 'test_helper'

class NotificationTest < ActiveSupport::TestCase
  setup do
    NotificationGeneratorJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    @org1 = create(:organization)
    @user = create(:user, organization: @org1)
    @card = create(:card, card_type: 'media', card_subtype: 'link', author_id: @user.id)
  end

  test 'should return notification card type' do
    assignment = create(:assignment, assignable: @card, assignee: @user)
    notification = create(:notification, notifiable: assignment, notification_type: 'assigned')

    assert_equal 'SmartCard', notification.notification_card_type
  end

  test 'should not return notification card type if notifiable blank' do
    notification = create(:notification, notifiable: nil, notification_type: 'assigned')

    refute notification.notification_card_type
  end

  test "should not create notification for suspended user" do
    active_user = create(:user, organization: @org1)
    suspended_user = create(:user, is_suspended: true)

    channel = create(:channel)
    suspended_noti = Notification.new(notifiable: channel, notification_type: 'following', user_id: suspended_user.id)
    active_noti = Notification.new(notifiable: channel, notification_type: 'following', user_id: active_user.id)

    refute suspended_noti.valid?
    assert_not_empty suspended_noti.errors

    assert active_noti.valid?
    assert_empty active_noti.errors
  end

  class Message < NotificationTest
    include CardHelperMethods
    test "mention notifications" do
      cu = create(:user, organization: @org1)
      n1 = create(:notification, notifiable: @card, notification_type: "mention_in_card")
      n1.causal_users << cu
      assert_match "#{cu.name} mentioned you in the insight", n1.message

      Comment.any_instance.stubs(:user_authorized).returns(true)
      comment = create(:comment, commentable: @card)
      n2 = create(:notification, notifiable: comment, notification_type: 'mention_in_comment')
      n2.causal_users << cu
      assert_match "#{cu.name} mentioned you in a comment on the insight: 'This is a comment'", n2.message
      Comment.any_instance.unstub(:user_authorized)

      #check if notification message is correct when mention in a published pathway.
      cu2 = create(:user, organization: @org1)
      Comment.any_instance.stubs(:user_authorized).returns(true)
      comment1 = create(:comment, commentable: @card)
      mention = create(:mention, mentionable_id: comment1.id, user_id: cu2.id)
      n3 = create(:notification, notifiable: comment1, notification_type: 'mention_in_comment')
      n3.app_deep_link_type = 'collection'
      n3.save
      n3.causal_users << cu
      assert_match "#{cu.name} mentioned you in a comment on the insight: 'This is a comment'",n3.message
      Comment.any_instance.unstub(:user_authorized)

      #check if notification message for comment is correct when mention in a post
      Comment.any_instance.stubs(:user_authorized).returns(true)
      comment2 = create(:comment, commentable: @card)
      mention = create(:mention, mentionable_id: comment2.id, user_id: cu2.id)
      n4 = create(:notification, notifiable: comment2, notification_type: 'mention_in_comment')
      n4.app_deep_link_type = 'post'
      n4.save
      n4.causal_users << cu
      assert_match "#{cu.name} mentioned you in a comment on the post: 'This is a comment'", n4.message
      Comment.any_instance.unstub(:user_authorized)

      #check if notification message for comment is correct when mention in a videostream
      Comment.any_instance.stubs(:user_authorized).returns(true)
      comment3 = create(:comment, commentable: @card)
      mention = create(:mention, mentionable_id: comment3.id, user_id: cu2.id)
      n5 = create(:notification, notifiable: comment3, notification_type: 'mention_in_comment')
      n5.app_deep_link_type = 'video_stream'
      n5.save
      n5.causal_users << cu
      assert_match "#{cu.name} mentioned you in a comment on the video stream: 'This is a comment'", n5.message
      Comment.any_instance.unstub(:user_authorized)

      #check if notification message for comment is handled when no app_deep_link_type for notification
      Comment.any_instance.stubs(:user_authorized).returns(true)
      comment4 = create(:comment, commentable: @card)
      mention = create(:mention, mentionable_id: comment4.id, user_id: cu2.id)
      n6 = create(:notification, notifiable: comment4, notification_type: 'mention_in_comment')
      n6.app_deep_link_type = ''
      n6.save
      n6.causal_users << cu
      assert_match "#{cu.name} mentioned you in a comment on this post: 'This is a comment'", n6.message
      Comment.any_instance.unstub(:user_authorized)
    end

    test 'implicit channel following notification' do
      channel  = create(:channel, :robotics)
      n1       = create(:notification, notifiable: channel, notification_type: "following")
      assert_equal n1.message, "You are following: '#{channel.label}'"
    end

    test 'channel invite notifications' do
      cu = create(:user, organization: @org1)
      ch = create(:channel, :robotics)
      n1 = create(:notification, notifiable: ch, notification_type: "added_author")
      n1.causal_users << cu
      assert n1.message.include?("added you as a collaborator of the channel: 'Robotics'")

      n2 = create(:notification, notifiable: ch, notification_type: "added_follower")
      n2.causal_users << cu
      assert n2.message.include?("added you as a follower of the channel: 'Robotics'")
    end

    test 'card assignment notification to assignee' do
      assignee = create(:user, organization: @org1)
      assignor = create(:user, organization: @org1)
      card = create(:card, title: 'test-card', author: create(:user, organization: @org1))
      assignment = create(:assignment, assignable: card, assignee: assignee)
      n1 = create(:notification, notifiable: assignment, notification_type: 'assigned')
      n1.causal_users << assignor
      assert n1.message.include?("assigned you the SmartCard: 'test-card'")
      assert_equal 'card', n1.app_deep_link_type
      assert_equal card.id, n1.app_deep_link_id
    end

    test 'card assignment due notification to assignee' do
      skip 'Test intermittent failure on travis, on local working fine'
      assignee = create(:user, organization: @org1)
      assignor = create(:user, organization: @org1)
      card = create(:card, title: 'test-card', author: create(:user, organization: @org1))
      assignment = create(:assignment, assignable: card, assignee: assignee, due_at: Date.current + 2.days)
      n1 = create(:notification, notifiable: assignment, notification_type: 'assignment_due_soon')
      n1.causal_users << assignor
      assert n1.message.include?("Assignment due in 1 days: 'test-card'")
      assert_equal 'card', n1.app_deep_link_type
      assert_equal card.id, n1.app_deep_link_id
    end

    test 'card assignment overdue notification to assignee' do
      assignee = create(:user, organization: @org1)
      assignor = create(:user, organization: @org1)
      card = create(:card, title: 'test-card', author: create(:user, organization: @org1))
      assignment = create(:assignment, assignable: card, assignee: assignee, due_at: Date.current - 2.days)
      n1 = create(:notification, notifiable: assignment, notification_type: 'assignment_due_soon')
      n1.causal_users << assignor

      assert n1.message.include?("Assignment overdue by 2 days: 'test-card'")
      assert_equal 'card', n1.app_deep_link_type
      assert_equal card.id, n1.app_deep_link_id
    end

    test 'card assignment due today to assignee' do
      assignee = create(:user, organization: @org1)
      assignor = create(:user, organization: @org1)
      card = create(:card, title: 'test-card', author: create(:user, organization: @org1))
      assignment = create(:assignment, assignable: card, assignee: assignee, due_at: Date.current)
      n1 = create(:notification, notifiable: assignment, notification_type: 'assignment_due_soon')
      n1.causal_users << assignor

      assert n1.message.include?("Assignment due today: 'test-card'")
      assert_equal 'card', n1.app_deep_link_type
      assert_equal card.id, n1.app_deep_link_id
    end

    test 'pathway assignment notification to assignee' do
      VideoStream.stubs :reindex
      setup_one_card_pack
      @cover.update title: 'science and fiction pathway'
      @cover.reload

      assignee = create(:user, organization: @org)
      assignor = create(:user, organization: @org)
      assignment = create(:assignment, assignable: @cover, assignee: assignee)

      n1 = create(:notification, notifiable: assignment, notification_type: 'assigned')
      n1.causal_users << assignor

      assert n1.message.include?("assigned you the Pathway: 'science and fiction pathway'")
      assert_equal 'collection', n1.app_deep_link_type
      assert_equal @cover.id, n1.app_deep_link_id
    end

    test 'course assignment notification to assignee' do
      group = create(:resource_group, name: 'science and technology')
      client = group.client
      organization = create(:organization, client: client)
      assignee = create(:user, organization: organization)
      assignor = create(:user, organization: organization)

      n1 = create(:notification, notifiable: group, notification_type: 'assigned', user_id: assignee.id)
      n1.causal_users << assignor

      assert n1.message.include?("assigned you a Course: 'science and technology'")
      assert_equal 'group', n1.app_deep_link_type
      assert_equal group.id, n1.app_deep_link_id
    end

    test 'should return custom message for assignment notice' do
      org = create(:organization)
      team = create(:team, organization: org)

      causal_user, assignee = create_list(:user, 2, organization: org)
      card = create(:card, author: causal_user)
      assignment = create(:assignment, assignable: card, assignee: assignee)
      team_assignment = create(:team_assignment, team_id: team, assignor: causal_user)

      notification = create(:notification, notification_type: 'assignment_notice', notifiable: assignment, custom_message: 'Congrats! for completing the assignment', user_id: assignee.id)

      assert_match /Congrats! for completing the assignment/, notification.custom_message
    end

    test 'should return custom message for card share with group notification' do
      organization = create(:organization)
      author = create :user, organization: organization ,first_name: "ABC" ,last_name: "XYZ"
      user = create :user, organization: organization
      card1 = create :card, author_id: author.id, message: "Ruby basics"
      team1 = create :team, organization: organization, name: "Ruby Development"
      notification1 = create(:notification, notification_type: 'team_card_share', notifiable: card1, user_id: user.id )
      notification1.causal_users << author
      assert_equal "ABC XYZ shared Ruby basics with your group(s).", notification1.message
    end

    test 'should return custom message for card share with user notification' do
      organization = create(:organization)
      author = create :user, organization: organization ,first_name: "ABC" ,last_name: "XYZ"
      user = create :user, organization: organization
      card1 = create :card, author_id: author.id, message: "Ruby basics"
      notification1 = create(:notification, notification_type: 'user_card_share', notifiable: card1, user_id: user.id )
      notification1.causal_users << author
      assert_match /ABC XYZ shared Ruby basics with you./, notification1.message
    end
  end

  class AppDeepLinkIdAndType < NotificationTest
    test "for channel notifications" do
      channel = create(:channel, :robotics)
      n = create(:notification, notifiable: channel, notification_type: "added_author")
      assert_equal [n.app_deep_link_id, n.app_deep_link_type], [channel.id, "channel"]
    end
  end

  class PathNotificationTest < NotificationTest
    include CardHelperMethods
    self.use_transactional_fixtures = false
    setup do
      Comment.any_instance.stubs(:record_comment_event)
    end

    def _test_app_deep_links(expected_id, expected_type)
      assert_difference ->{Notification.count} do
        yield
      end
      n = Notification.last
      assert_equal expected_id, n.app_deep_link_id
      assert_equal expected_type, n.app_deep_link_type
    end

    test 'pathway notifications' do
      VideoStream.stubs :reindex
      setup_card_packs
      @mixed_cover.reload
      u = create(:user, organization: @mixed_cover.organization)

      stream = @cards[@mixed_cover][0]
      hidden_card = @cards[@mixed_cover][1]
      hidden_card.reload
      hidden_card.update_columns(hidden: true)
      non_hidden_card = @cards[@mixed_cover][3]
      non_hidden_card.reload

      # testing upvotes on cover, hidden insight in a pathway and non-hidden insight in a pathway
      # COVER
      _test_app_deep_links(@mixed_cover.id, 'collection') do
        vote = create(:vote, votable: @mixed_cover, voter: u)
        vote.run_callbacks(:commit)
      end

      # HIDDEN CARD
      _test_app_deep_links(@mixed_cover.id, 'collection') do
        vote = create(:vote, votable: hidden_card, voter: u)
        vote.run_callbacks(:commit)
      end

      # NON HIDDEN CARD
      _test_app_deep_links(non_hidden_card.id, 'card') do
        vote = create(:vote, votable: non_hidden_card, voter: u)
        vote.run_callbacks(:commit)
      end

      # testing comments on cover, hidden insight in a pathway and non-hidden insight in a pathway
      # COVER
      cover_card_comment = hidden_card_comment = non_hidden_card_comment = nil
      _test_app_deep_links(@mixed_cover.id, 'collection') do
        cover_card_comment = create(:comment, commentable: @mixed_cover, user: u)
      end

      # HIDDEN CARD
      _test_app_deep_links(@mixed_cover.id, 'collection') do
        hidden_card_comment = create(:comment, commentable: hidden_card, user: u)
      end

      # NON HIDDEN CARD
      _test_app_deep_links(non_hidden_card.id, 'card') do
        non_hidden_card_comment = create(:comment, commentable: non_hidden_card, user: u)
      end

      # testing votes on cover comments, hidden card comments and non hidden card comments
      # COVER COMMENT
      voter = create(:user, organization: @org1)
      _test_app_deep_links(@mixed_cover.id, 'collection') do
        create(:vote, votable: cover_card_comment, voter: voter)
      end

      # HIDDEN CARD COMMENT
      _test_app_deep_links(@mixed_cover.id, 'collection') do
        create(:vote, votable: hidden_card_comment, voter: voter)
      end

      # NON HIDDEN CARD COMMENT
      _test_app_deep_links(non_hidden_card.id, 'card') do
        create(:vote, votable: non_hidden_card_comment, voter: voter)
      end
    end
  end
end

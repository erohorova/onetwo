require 'test_helper'

class CardResourceHistoryTest < ActiveSupport::TestCase
  class CreateNewVersionTest < ActiveSupport::TestCase
    test "creates elasticsearch index for changes" do
      stub_request(:put, "http://localhost:9200/card_resource_test/card_resource_history/1")
        .to_return(status: 200, body: "", headers: {})
      CardResourceHistory.create_new_version(1, 1, {title: ["", "test"]})
    end
  end

  class FetchRecordTest < ActiveSupport::TestCase
    test "returns nil if record not found in ES" do
      CardResourceHistory.stubs(:find).raises(Elasticsearch::Persistence::Repository::DocumentNotFound)
      refute CardResourceHistory.fetch_record(1)
    end

    test "returns ES if record if present in ES" do
      time = Time.now
      obj = CardResourceHistory.new(
        id: "1",
        created_at: time,
        updated_at: time,
        updated_fields: [{"image_url"=>["something", "somewhere"]}]
      )
      CardResourceHistory.stubs(:find).returns(obj)
      assert_equal obj, CardResourceHistory.fetch_record(1)
    end
  end

  class GetChangesTest < ActiveSupport::TestCase
    test 'return resource changes -> nil to new' do
      resource = create(:resource)
      result = CardResourceHistory.get_changes([nil, resource.id])
      expected = [
        {"url"=>nil, "image_url"=>nil, "video_url"=>nil, "embed_html"=>nil},
        {"url"=>resource.url, "image_url"=>resource.image_url, "video_url"=>resource.video_url, "embed_html"=>nil}
      ]
      assert_equal expected, result
    end

    test 'return [] if before and after changes are same' do
      resource1 = create(:resource)
      resource2 = create(:resource)
      result = CardResourceHistory.get_changes([resource1.id, resource2.id])
      assert_equal [], result
    end

    test 'return resource changes' do
      resource1 = create(:resource, embed_html: "<html><body><h2>Hello</h2></body></html>")
      resource2 = create(:resource)
      result = CardResourceHistory.get_changes([resource1.id, resource2.id])
      expected = [
        {"url"=>resource1.url, "image_url"=>resource1.image_url, "video_url"=>resource1.video_url, "embed_html"=>resource1.embed_html},
        {"url"=>resource2.url, "image_url"=>resource2.image_url, "video_url"=>resource2.video_url, "embed_html"=>nil}
      ]
      assert_equal expected, result
    end
  end
end

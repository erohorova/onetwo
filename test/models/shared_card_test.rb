require 'test_helper'

class SharedCardTest < ActiveSupport::TestCase

  test 'inherits from teams card' do
    # see test/models/teams_card_test.rb
    assert SharedCard < TeamsCard
  end

  test '#record_card_shared_event' do
    stub_time
    org = create :organization
    team = create(:team, organization: org)
    user = create :user, organization: org
    card = create :card, organization: org, author_id: nil
    metadata = { foo: "bar" }
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    # This recorder uses the SharedCard#user as the actor,
    # instead of looking up from the RequestStore metadata
    Analytics::MetricsRecorderJob.unstub(:perform_later)
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        recorder: "Analytics::CardSharedMetricsRecorder",
        card: Analytics::MetricsRecorder.card_attributes(card),
        timestamp: Time.now.to_i,
        event: "card_shared",
        team_id: team.id,
        org: Analytics::MetricsRecorder.org_attributes(org),
        actor: Analytics::MetricsRecorder.user_attributes(user),
        additional_data: metadata
      )
    )
    shared_card = SharedCard.create(team: team, user: user, card: card)
    shared_card.run_callbacks :commit
  end

  class TeamsCardManageableTest < ActiveSupport::TestCase
    setup do
      @org = create(:organization)
      @user, @author = create_list(:user, 2, organization: @org)
      @team = create(:team, organization: @org)
      @team.add_admin(@user)
      @public_card = create(:card, author: @author, organization: @org)
      @user_card = create(:card, author: @user, organization: @org)
    end

    test '#manageable should return true if adding public_card to the team' do
      teams_card = TeamsCard.new(team_id: @team.id, card_id: @public_card.id, type: 'SharedCard', user_id: @user.id)
      assert teams_card.is_manageable?
    end

    test '#manageable should return false if adding public_card to the team' do
      teams_card = TeamsCard.new(team_id: @team.id, card_id: @user_card.id, type: 'SharedCard', user_id: @user.id)
      refute teams_card.is_manageable?
    end

    test '#manageable should return true if adding card without author' do
      card_without_author = create(:card, organization: @org, author: nil)
      teams_card = TeamsCard.new(team_id: @team.id, card_id: card_without_author.id, type: 'SharedCard', user_id: @user.id)
      assert teams_card.is_manageable?
    end

    test '#get_ucm_values should return correct values for teams_card object' do
      expected = [@user.id, @public_card.id, 'shared_to_team', @team.id]
      teams_card = TeamsCard.new(team_id: @team.id, card_id: @public_card.id, type: 'SharedCard', user_id: @user.id)
      values = teams_card.get_ucm_values
      assert_equal expected, values
    end
  end

end

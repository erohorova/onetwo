require 'test_helper'

class IdentityProviderTest  < ActiveSupport::TestCase

  should validate_presence_of(:user_id)
  should validate_presence_of(:uid)

  test "should have multiple identities of the same kind" do
    user1 = create(:user)
    user2 = create(:user)

    identity1 = build(:identity_provider, :facebook, uid: "1", user: user1)
    identity2 = build(:identity_provider, :facebook, uid: "1", user: user2)

    assert identity1.save
    assert identity2.save

    identity1 = build(:identity_provider, :twitter, uid: "1", user: user1)
    identity2 = build(:identity_provider, :twitter, uid: "1", user: user2)

    assert identity1.save
    assert identity2.save
  end

  test 'default scopes are true' do
    assert create(:identity_provider).scoped?(:share)
  end
end

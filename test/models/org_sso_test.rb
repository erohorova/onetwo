require 'test_helper'
class OrgSsoTest < ActiveSupport::TestCase
  should belong_to(:sso)
  should belong_to(:organization)

  setup do
    create_default_ssos
    Organization.any_instance.unstub :default_sso
    stub_okta_group_creation
    stub_okta_app_creation
    stub_okta_group_app_link
    @org = create(:organization)
    @org.configs.create(name: 'OktaApiToken', value: SecureRandom.hex)
    @org.configs.create(name: 'app.config.okta.enabled', value: "true")

    @lxp_oauth_sso = Sso.find_by(name: 'lxp_oauth')
  end

  test 'update org show_onboarding if org has only email sso enabled' do
    @org.update_attribute(:show_onboarding, false)
    @org.org_ssos.where(sso: Sso.where(name: ['google', 'linkedin', 'facebook'])).each do |record|
     record.update(is_enabled: false)
    end

    assert @org.only_email_sso_enabled?
    assert @org.reload.show_onboarding # org only has email sso authentication
  end

  test "should create idp on okta if the org sso is for okta" do
    OrgSso.any_instance.expects(:create_okta_idp).once
    create(:org_sso, organization: @org, sso: @lxp_oauth_sso, is_enabled: true, label_name: 'Okta Facebook',
      parameters: { provider_name: 'Facebook' })
  end

  test "should create idp for non okta org_sso" do
    OrgSso.any_instance.expects(:create_okta_idp).never
    create(:org_sso, organization: @org, sso: Sso.first, is_enabled: true, label_name: ' Facebook',
      parameters: { provider_name: 'Facebook' })
  end

  test '.revised_lxp_oauth' do
    email_sso = Sso.find_by(name: 'email')
    create(:org_sso, organization: @org, sso: @lxp_oauth_sso, is_enabled: true, label_name: 'Okta Google',
      parameters: { provider_name: 'Google' })

    assert_same_elements [email_sso.id, @lxp_oauth_sso.id], @org.org_ssos.revised_lxp_oauth.pluck(:sso_id)
  end

  test '.legacy_ssos' do
    create(:org_sso, organization: @org, sso: @lxp_oauth_sso, is_enabled: true, label_name: 'Okta Google',
      parameters: { provider_name: 'Google' })

    legacy_ssos = Sso.where(name: Sso::DEFAULT_SSO_NAMES)
    legacy_org_ssos = OrgSso.where(sso_id: legacy_ssos.pluck(:id))

    assert_same_elements legacy_org_ssos.pluck(:sso_id), @org.org_ssos.legacy_ssos.pluck(:sso_id)
  end

class OrgSsoEventTest < ActiveSupport::TestCase
  self.use_transactional_fixtures = false
  setup do
    stub_time
    create_default_ssos

    @org = create(:organization)
    @actor = create(:user, organization: @org)

    @metadata =  { user_id: @actor.id, platform: 'web', user_agent: 'Chrome' }

    RequestStore.unstub(:read)
    RequestStore.stubs(:read).with(:request_metadata).returns(@metadata)
  end

  test 'should record when new sso is added to organization' do
    time = Time.now
    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub :perform_later
    sso = Sso.first
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        sso: Analytics::MetricsRecorder.sso_attributes(sso),
        recorder: 'Analytics::OrgSsoMetricsRecorder',
        org: Analytics::MetricsRecorder.org_attributes(@org),
        actor: Analytics::MetricsRecorder.user_attributes(@actor),
        timestamp: time.to_i,
        additional_data: @metadata,
        event: 'org_sso_added'
      )
    )

    @org.ssos << sso
  end

  test 'should record when org sso is edited' do
    time = Time.now
    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub :perform_later
    @org.ssos << Sso.first

    org_sso = @org.org_ssos.first

    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      sso: Analytics::MetricsRecorder.sso_attributes(org_sso.sso),
      recorder: 'Analytics::OrgSsoMetricsRecorder',
      org: Analytics::MetricsRecorder.org_attributes(@org),
      actor: Analytics::MetricsRecorder.user_attributes(@actor),
      timestamp: time.to_i,
      additional_data: @metadata,
      event: 'org_sso_edited',
      changed_column: 'is_enabled',
      old_val: 'true',
      new_val: 'false'
    )
    org_sso.update(is_enabled: false)
  end

  test 'should push empty values when saml certificate is updated' do
    time = Time.now
    @org.ssos << Sso.first

    org_sso = @org.org_ssos.first
    org_sso.update(saml_idp_cert: "---------BEGIN CERTIFICATE----- old value")

    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub :perform_later

    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      sso: Analytics::MetricsRecorder.sso_attributes(org_sso.sso),
      recorder: 'Analytics::OrgSsoMetricsRecorder',
      org: Analytics::MetricsRecorder.org_attributes(@org),
      actor: Analytics::MetricsRecorder.user_attributes(@actor),
      timestamp: time.to_i,
      additional_data: @metadata,
      event: 'org_sso_edited',
      changed_column: 'saml_idp_cert',
      old_val: "",
      new_val: ""
    )
    org_sso.update(saml_idp_cert: "---------BEGIN CERTIFICATE----- new value")
  end

  test 'should record when org sso is deleted' do
    time = Time.now
    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub :perform_later
    @org.ssos << Sso.first

    org_sso = @org.org_ssos.first

    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      sso: Analytics::MetricsRecorder.sso_attributes(org_sso.sso),
      recorder: 'Analytics::OrgSsoMetricsRecorder',
      org: Analytics::MetricsRecorder.org_attributes(@org),
      actor: Analytics::MetricsRecorder.user_attributes(@actor),
      timestamp: time.to_i,
      additional_data: @metadata,
      event: 'org_sso_removed'
    )
    org_sso.destroy
  end

  test 'should toggle is_visible when is_enabled is toggled' do
    # legacy
    legacy_google_sso = Sso.find_by(name: 'google')
    legacy_google_org_sso = create :org_sso, sso: legacy_google_sso, organization: @org

    assert legacy_google_org_sso.is_visible
    assert legacy_google_org_sso.is_enabled

    # org_sso is disabled
    legacy_google_org_sso.update_attributes(is_enabled: false)

    # visibility is turned OFF too.
    refute legacy_google_org_sso.is_visible
    refute legacy_google_org_sso.is_enabled

    legacy_google_org_sso.update_attributes(is_enabled: true)

    # visibility is turned ON too.
    assert legacy_google_org_sso.is_visible
    assert legacy_google_org_sso.is_enabled

    # Same with revised ssos
    # revised
    revised_google_sso = Sso.find_by(name: 'lxp_oauth')
    revised_google_org_sso = create :org_sso, sso: revised_google_sso, organization: @org

    assert revised_google_org_sso.is_visible
    assert revised_google_org_sso.is_enabled

    revised_google_org_sso.update_attributes(is_enabled: false)

    refute revised_google_org_sso.is_visible
    refute revised_google_org_sso.is_enabled

    revised_google_org_sso.update_attributes(is_enabled: true)

    assert revised_google_org_sso.is_visible
    assert revised_google_org_sso.is_enabled
  end


  test 'is_visibility should not control is_enabled' do
    # legacy
    legacy_google_sso = Sso.find_by(name: 'google')
    legacy_google_org_sso = create :org_sso, sso: legacy_google_sso, organization: @org

    assert legacy_google_org_sso.is_visible
    assert legacy_google_org_sso.is_enabled

    # visibility is turned OFF, for the org_sso
    legacy_google_org_sso.update_attributes(is_visible: false)

    refute legacy_google_org_sso.is_visible
    assert legacy_google_org_sso.is_enabled

    # visibility is turned ON, for the org_sso
    legacy_google_org_sso.update_attributes(is_visible: true)

    assert legacy_google_org_sso.is_visible
    assert legacy_google_org_sso.is_enabled

    # revised
    revised_google_sso = Sso.find_by(name: 'lxp_oauth')
    revised_google_org_sso = create :org_sso, sso: revised_google_sso, organization: @org

    assert revised_google_org_sso.is_visible
    assert revised_google_org_sso.is_enabled

    # visibility is turned OFF, for the org_sso
    revised_google_org_sso.update_attributes(is_visible: false)

    # `is_enabled` remains unchanged
    refute revised_google_org_sso.is_visible
    assert revised_google_org_sso.is_enabled

    # visibility is turned ON, for the org_ssos
    revised_google_org_sso.update_attributes(is_visible: true)

    assert revised_google_org_sso.is_visible
    assert revised_google_org_sso.is_enabled
  end

end

end



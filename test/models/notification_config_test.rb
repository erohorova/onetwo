require 'test_helper'

class NotificationConfigTest < ActiveSupport::TestCase
  setup do
    @org = create :organization
    @config = create :notification_config, organization: @org
  end

  test 'set and get config' do
    @config.set_config('abcd', 'def', true)
    @config.save

    @config.reload

    assert @config.get_config('abcd', 'def')
  end

  test 'enable and disable' do
    @config.set_enabled('name', 'medium', 'interval')
    @config.save

    @config.reload

    assert @config.enabled?('name', 'medium', 'interval')

    @config.set_disabled('name', 'medium', 'interval')
    @config.save

    @config.reload

    assert_not @config.enabled?('name', 'medium', 'interval')
  end

  test "records event to influx on edit" do
    actor = create :user, organization: @org
    @config.update! configuration: { k1: "v1", k3: "v3" }

    metadata = { user_id: actor.id, platform: "ios" }
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    time = Time.now
    Time.stubs(:now).returns time
    EdcastActiveJob.unstub(:perform_later)
    Analytics::MetricsRecorderJob.unstub(:perform_later)

    # expect MetricsRecorderJob to be called when we edit attributes
    $recorder_args = {
      recorder:        "Analytics::OrgEditedMetricsRecorder",
      event: "org_notification_settings_edited",
      additional_data: metadata,
      timestamp:       time.to_i,
      actor:           Analytics::MetricsRecorder.user_attributes(actor),
      org:             Analytics::MetricsRecorder.org_attributes(@org),
      changed_column:  "k1",
      old_val:         "v1",
      new_val:         "v2"
    }
    Analytics::MetricsRecorderJob.expects(:perform_later).with($recorder_args)
    # k3 didnt change, so it won't get recorded
    @config.update!(configuration: {k1: "v2", k3: "v3"})
  end

  test "doesnt record event on edit if config has a user" do
    actor = create :user, organization: @org
    @config.update! user_id: actor.id, configuration: { k1: "v1" }

    metadata = { user_id: actor.id, platform: "ios" }
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    time = Time.now
    Time.stubs(:now).returns time
    EdcastActiveJob.unstub(:perform_later)
    Analytics::MetricsRecorderJob.unstub(:perform_later)

    Analytics::MetricsRecorderJob.expects(:perform_later).never
    @config.update!(configuration: {k1: "v2"})
  end

  test "doesnt record event on edit if something other than config changes" do
    actor = create :user, organization: @org
    @config.update! configuration: { k1: "v1" }

    metadata = { user_id: actor.id, platform: "ios" }
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    time = Time.now
    Time.stubs(:now).returns time
    EdcastActiveJob.unstub(:perform_later)
    Analytics::MetricsRecorderJob.unstub(:perform_later)

    Analytics::MetricsRecorderJob.expects(:perform_later).never
    @config.update!(user_id: actor.id)
  end

  test "doesnt record event if there is no actor" do
    @config.update! configuration: { k1: "v1", k3: "v3" }
    
    metadata = { platform: "ios" }
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    time = Time.now
    Time.stubs(:now).returns time
    EdcastActiveJob.unstub(:perform_later)
    Analytics::MetricsRecorderJob.unstub(:perform_later)

    Analytics::MetricsRecorderJob.expects(:perform_later).never
    # k3 didnt change, so it won't get recorded
    @config.update!(configuration: {k1: "v2", k3: "v3"})
  end

end

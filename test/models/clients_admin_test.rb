require 'test_helper'

class ClientsAdminTest < ActiveSupport::TestCase
  should validate_presence_of(:admin)
  should validate_presence_of(:client)

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    @admin = create(:user)
    @client = create(:client)
    @admin2 = create(:user)
    @client2 = create(:client)
    ClientsAdmin.create(client: @client, admin: @admin)
    ClientsAdmin.create(client: @client, admin: @admin2)
    ClientsAdmin.create(client: @client2, admin: @admin)
  end

  test 'user should be able to list first clients' do
    assert_equal @admin.administered_clients.first, @client
  end

  test 'client should be able to list first admins' do
    assert_equal @client.admins.first, @admin
  end

  test 'user should be able to list clients' do
    assert_equal @admin.administered_clients, [@client, @client2]
  end

  test 'client should be able to list admins' do
    assert_same_elements @client.admins, [@admin, @admin2, @client.user]
  end
end

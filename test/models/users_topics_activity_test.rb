require 'test_helper'

class UsersTopicsActivityTest < ActiveSupport::TestCase

  %i{
    organization_id
    user_id
    topic_counts
    start_time
    end_time
  }.each { |col| should validate_presence_of col }

  test "serializes topics_counts" do

    record = UsersTopicsActivity.create(
      # Note: organization_id and user_id are intentionally allowed
      # to point to non existent records
      organization_id: 0,
      user_id:         0,
      topic_counts:    { foo: "bar" },
      start_time:      0,
      end_time:        0
    )

    assert record.persisted?

    assert_equal({"foo" => "bar"}, record.topic_counts)

  end

end

require 'test_helper'

class UsersExternalCourseTest < ActiveSupport::TestCase
  should have_db_column(:user_id).of_type(:integer)
  should have_db_column(:external_course_id).of_type(:integer)
  should have_db_column(:status).of_type(:string)

  should belong_to :external_course
  should belong_to :user

  should validate_presence_of(:user_id)
  should validate_presence_of(:external_course_id)
end

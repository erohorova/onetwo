require 'test_helper'

class PubnubChannelTest < ActiveSupport::TestCase

  test 'uniqueness validation' do
    u = create(:user)
    pb = PubnubChannel.create(item: u, as_type: 'private')

    assert_not PubnubChannel.new(item: u, as_type: 'private').save
    assert PubnubChannel.new(item: u, as_type: 'public').save
    assert PubnubChannel.new(item: create(:user), as_type: 'private').save
  end

end

require 'test_helper'

class EmbargoFileTest < ActiveSupport::TestCase
  should belong_to :organization
  should belong_to :user

  setup do
    @org = create(:organization)
    @user = create(:user, organization: @org)
    @file = create(:embargo_file,
      data: File.read('test/fixtures/files/embargo.csv'),
      organization_id: @org.id,
      user_id: @user.id
    )
  end

  test 'should properly extract specific rows from file with limit & offset' do
    rows, total = @file.extract_specific_rows(limit: 2, offset: 2)
    expected_total = 8
    expected_rows = [
      {value: 'facebook.com', category: 'approved_sites'},
      {value: 'yahoo.com', category: 'approved_sites'}
    ].map(&:with_indifferent_access)

    assert_equal expected_total, total
    assert_equal expected_rows, rows
  end

  test 'should support default values for limit and offset' do
    rows, total = @file.extract_specific_rows

    refute rows.empty?
    assert_equal 8, total
  end

  test 'should properly get rows for a limit values bigger than rows count in a file' do
    rows, total = @file.extract_specific_rows(limit: 1000, offset: 1)

    assert_equal 7, rows.count
    assert_equal 8, total
  end

  test 'should properly get rows for a limit value bigger than count of rows in a file' do
    rows, total = @file.extract_specific_rows(limit: 1000, offset: 1)

    assert_equal 7, rows.count
    assert_equal 8, total
  end

  test 'should return empty array for a offset value bigger than count of rows in a file' do
    rows, total = @file.extract_specific_rows(limit: 10, offset: 100)

    assert rows.empty?
    assert_equal 8, total
  end
end

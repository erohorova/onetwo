require 'test_helper'

class PromotionTest < ActiveSupport::TestCase

  should belong_to :promotable
  should have_many :taggings
  should have_many(:tags).through :taggings

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    @org = create(:organization)
    @client = create(:client, organization: @org)
    @group = create(:group, organization: @org, client: @client)
  end

  test 'should not create promotion for ineligible promotable' do
    assert_no_difference ->{ Promotion.count } do
      Promotion.promote(@group, nil)
      Promotion.promote(@group, {})
      Promotion.promote(@group, {something: 'something'})

      @group.expects(:promotion_eligible?).returns(false)
      Promotion.promote(@group, url: 'url')
    end
  end

  test 'should create and update promotion' do
    @group.stubs(:promotion_eligible?).returns(true)

    assert_difference ->{ Promotion.count } do
      Promotion.promote(@group, url: 'url', tags: 'tag1|tag2', also: 'also')
    end
    promo = Promotion.last
    assert_equal 'url', promo.url
    assert_same_elements %w[tag1 tag2], promo.tags.pluck(:name)
    assert_equal ({also: 'also'}), promo.data

    assert_no_difference ->{ Promotion.count } do
      Promotion.promote(@group, url: 'url2', tags: '', and: 'and', too: 'too')
    end
    promo = Promotion.last
    assert_equal 'url2', promo.url
    assert_empty promo.tags
    assert_equal ({and: 'and', too: 'too'}), promo.data
  end

  test 'should update deleted promotion' do
    @group.stubs(:promotion_eligible?).returns(true)

    Promotion.promote(@group, url: 'url', tags: 'tag1|tag2')
    promo = Promotion.last

    assert_difference ->{ Promotion.count }, -1 do
      promo.delete
    end

    assert_difference ->{ Promotion.count } do
      Promotion.promote(@group, url: 'url2', tags: 'tag3')
    end
    assert_not promo.reload.deleted?
    assert_equal 'url2', promo.url
    assert_equal %w[tag3], promo.tags.pluck(:name)
  end

  test 'should close' do
    @group.stubs(:promotion_eligible?).returns(true)
    Promotion.promote(@group, url: 'url', tags: 'tag1|tag2')

    assert_difference ->{ Promotion.count }, -1 do
      Promotion.close(promotable: @group)
    end
    assert Promotion.with_deleted.last.deleted?
  end

  test 'should unpromote' do
    @group.stubs(:promotion_eligible?).returns(true)
    Promotion.promote(@group, url: 'url', tags: 'tag1|tag2')

    assert_difference ->{ Promotion.count }, -1 do
      Promotion.unpromote(promotable: @group)
    end
    assert Promotion.with_deleted.last.deleted?
  end

  test 'promotions should exclude courses the user is enrolled in already' do
    create_default_org
    Group.any_instance.stubs(:promotion_eligible?).returns(true)
    Group.any_instance.stubs(:savannah?).returns(true)

    ## defualt org test
    default_org = Organization.default_org
    default_client = create(:client, organization: default_org)

    default_org_groups = create_list(:group, 2, organization: default_org, client: default_client)
    default_org_promos = default_org_groups.map do |g|
      Promotion.promote(g, url: 'url', tags: 'randomtag')
    end

    default_org_user = create(:user, organization: default_org)

    default_org_groups.first.add_member(default_org_user)
    promos = Promotion.promotions_for(user: default_org_user)
    assert_equal [default_org_groups.last.id], promos.map(&:promotable_id)


    ## Org test
    org = create(:organization)
    cli = org.client
    cli_groups = create_list(:group, 5, organization: org)
    org_promos = cli_groups.map do |g|
      Promotion.promote(g, url: 'url', tags: 'randomtag')
    end

    # Org user is a part of org
    default_org_user = create(:user, organization: org)
    org_user = default_org_user.clone_for_org(org, "member")

    cli_groups.first.add_member(default_org_user)
    org_user_promos = Promotion.promotions_for(user: org_user, limit: 10)
    assert_equal 4, org_user_promos.count
    assert_same_elements cli_groups.map(&:id).reject{|gid| gid == cli_groups.first.id}, org_user_promos.map(&:promotable_id)
    ## Org test end
  end

  test 'should get promotions for user' do
    create_default_org
    Group.any_instance.stubs(:promotion_eligible?).returns(true)
    Group.any_instance.stubs(:savannah?).returns(true)

    ## defualt org test
    default_org = Organization.default_org
    default_client = create(:client, organization: default_org)

    default_org_groups = create_list(:group, 2, organization: default_org, client: default_client)
    default_org_promos = default_org_groups.map do |g|
      Promotion.promote(g, url: 'url', tags: 'randomtag')
    end
    default_org_user = create(:user, organization: default_org)
    promos = Promotion.promotions_for(user: default_org_user)
    assert_same_ids default_org_groups.map(&:id), promos.map(&:id)


    ## Org test
    cli = create(:client)
    org = create(:organization, client: cli)
    cli_groups = create_list(:group, 5, organization: org)
    org_promos = cli_groups.map do |g|
      Promotion.promote(g, url: 'url', tags: 'randomtag')
    end

    # Org user is a part of org
    org_user = create(:user, organization: org)
    org_user_promos = Promotion.promotions_for(user: org_user)
    assert_equal 3, org_user_promos.count
    assert_empty org_user_promos - org_promos
    ## Org test end
  end

end

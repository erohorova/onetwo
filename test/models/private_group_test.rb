require 'test_helper'

class PrivateGroupTest < ActiveSupport::TestCase

  should validate_inclusion_of(:access).in_array(%w(open invite))
  should validate_presence_of :resource_group
  should validate_presence_of :creator

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    @org = create(:organization)
    client = create(:client, organization: @org)
    @user1 = create(:user, email: 'a@a.com', organization: @org)
    @user2 = create(:user, email: 'b@b.com', organization: @org)
    @group = create(:resource_group, name: 'group', organization: @org, client: client)
    @group.add_member @user1
    @group.add_member @user2
  end

  test 'should not require description' do
    private_group = PrivateGroup.new(name: 'my personal group', access: 'open', creator: create(:user, organization: @org), resource_group: @group)

    assert(private_group.save)
  end

  test 'creator should be an admin' do
    user = create(:user)
    group = create(:private_group, resource_group: @group, creator: user)
    assert group.is_admin? user
  end

  test 'should add a new member' do
    user = create(:user)
    private_group = create(:private_group, resource_group: @group, creator: create(:user, organization: @org))

    private_group.add_member user

    assert private_group.is_member?(user)
  end

  test 'should create a user joined group private stream event' do
    StreamCreateJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    private_group = create(:private_group, resource_group: @group, creator: create(:user, organization: @org))
    user = create(:user, organization: @org)
    assert_difference ->{Stream.count} do
      gu = GroupsUser.create!(group: private_group, user: user, as_type: 'member', role_label: 'member')
      gu.run_callbacks(:commit)
      stream = Stream.last
      assert_equal user, stream.user
      assert_equal private_group, stream.item
      assert_equal 'joined', stream.action
      assert_equal @group.id, stream.group_id
    end

    assert_difference ->{Stream.count}, -1 do
      assert private_group.destroy
    end
  end

  test 'is_active_user? should work' do
    instructor = create(:user)
    student = create(:user)
    member = create(:user)
    admin = create(:user)
    creator = create(:user, organization: @org)

    group = create(:private_group, resource_group: @group, creator: creator)
    group.members << student
    group.admins << instructor
    group.members << member
    group.admins << admin

    [instructor, student, member, admin].each do |u|
      assert group.is_active_user? u
    end
  end

  test 'is_active_user? should not work for banned user' do
    instructor = create(:user)
    student = create(:user)
    member = create(:user)
    admin = create(:user)
    creator = create(:user, organization: @org)

    group = create(:private_group, resource_group: @group, creator: creator)
    group.members << student
    group.admins << instructor
    group.members << member
    group.admins << admin

    group.ban_user(admin)
    group.ban_user(instructor)
    group.ban_user(member)
    group.ban_user(student)

    [instructor, student, member, admin].each do |u|
      assert_not group.is_active_user? u
    end
  end

  test 'is_active_super_user? should work' do
    instructor = create(:user)
    student = create(:user)
    member = create(:user)
    admin = create(:user)
    creator = create(:user, organization: @org)

    group = create(:private_group, resource_group: @group, creator: creator)
    group.members << student
    group.admins << instructor
    group.members << member
    group.admins << admin

    [instructor, admin].each do |u|
      assert group.is_active_super_user? u
    end

    [student, member].each do |u|
      assert_not group.is_active_super_user? u
    end
  end

  test 'is_active_super_user? should not work for banned user' do
    instructor = create(:user)
    student = create(:user)
    member = create(:user)
    admin = create(:user)
    creator = create(:user, organization: @org)

    group = create(:private_group, resource_group: @group, creator: creator)
    group.members << student
    group.admins << instructor
    group.members << member
    group.admins << admin

    group.ban_user(admin)
    group.ban_user(instructor)


    [instructor, admin].each do |u|
      assert_not group.is_active_super_user? u
    end

    [student, member].each do |u|
      assert_not group.is_active_super_user? u
    end
  end

  test 'groups wont included groups which user is banned' do
    creator = create(:user, organization: @org)
    user = create(:user)
    group = create(:resource_group)
    pgroup1 = create(:private_group, resource_group: group, creator: creator)
    pgroup2 = create(:private_group, resource_group: group, creator: creator)
    pgroup1.add_member(user)
    pgroup2.add_member(user)
    assert_equal 2, user.groups.count
    pgroup2.ban_user(user)
    pgroup2.reload
    assert_equal 1, user.groups.count
  end


  test 'should get snippet' do
    private_group = PrivateGroup.new(name: 'this is a really big name so i will do it three times. this is a really big name. this is a really big name' , access: 'open', creator: create(:user, organization: @org), resource_group: @group)
    assert_equal 'this is a really big name so i will do it three times....', private_group.snippet

    private_group.name = 'small name'
    assert_equal 'small name', private_group.snippet
  end

end

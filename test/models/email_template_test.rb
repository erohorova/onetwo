require 'test_helper'

class EmailTemplateTest < ActiveSupport::TestCase
  should belong_to :organization
  should belong_to(:creator).class_name('User')
  should belong_to(:default).class_name('EmailTemplate')

  setup do
    @org = create(:organization)
    @user = create(:user, organization: @org)
    @original_template = create(:email_template, organization: @org)
    @custom_template = create(
      :email_template, is_active: true, default_id: @original_template.id,
      title: "#{@original_template.title}_custom_1", creator: @user,
      organization: @org
    )
  end

  test 'should validate title in scope of organization' do
    template = EmailTemplate.new(
      is_active: false, default_id: @original_template.id,
      title: @custom_template.title.to_s, creator: @user, content: 'new',
      organization: @org, design: 'design', language: 'en'
    )
    refute template.valid?
    error = template.errors.full_messages.join('')
    assert_equal 'Title has already been taken', error
  end

  test 'should validate title ignoring case sensitivity in scope of organization' do
    title = @custom_template.title.to_s.upcase
    template = EmailTemplate.new(
      is_active: false, default_id: @original_template.id,
      title: title, creator: @user, content: 'new',
      organization: @org, design: 'design', language: 'en'
    )
    refute template.valid?
    error = template.errors.full_messages.join('')
    assert_equal 'Title has already been taken', error
  end

  test 'should validate presence of required fields' do
    template = EmailTemplate.new(
      default_id: @original_template.id, creator: @user
    )
    refute template.valid?
    error = template.errors.full_messages.join(', ')
    items = %w[Organization Title Language Design Content]
    expectation = []
    items.each { |i| expectation.push("#{i} can't be blank") }
    assert_equal expectation.join(', '), error
  end

  test 'should set state draft by default' do
    template = EmailTemplate.new(
      is_active: false, default_id: @original_template.id,
      title: 'title', creator: @user, content: 'new', language: 'en',
      organization: @org, design: 'design'
    )
    assert_equal 'draft', template.state
  end

  test 'should validate proper state value' do
    template = EmailTemplate.new(
      is_active: false, default_id: @original_template.id,
      title: 'title', creator: @user, content: 'new', language: 'en',
      organization: @org, design: 'design', state: 'custom',
    )
    refute template.valid?
    error = template.errors.full_messages.join('')
    assert_equal 'State is not included in the list', error
  end

  test 'should validate creator id presence for custom templates' do
    template = EmailTemplate.new(
      is_active: false, default_id: @original_template.id,
      title: 'title', content: 'new', language: 'en',
      organization: @org, design: 'design'
    )
    refute template.valid?
    error = template.errors.full_messages.join('')
    assert_equal "Creator can't be blank", error
  end

  test '#activate should set proper value' do
    template = create(
      :email_template, is_active: false, default_id: @original_template.id,
      title: "#{@original_template}_custom_2", creator: @user
    )
    refute template.is_active
    template.activate_template
    template.reload
    assert template.is_active
  end

  test '#activate should set proper value for existed old one with same language' do
    template = create(
      :email_template, is_active: false, default_id: @original_template.id,
      title: "#{@original_template}_custom_2", creator: @user, language: 'en',
      organization: @org
    )
    refute template.is_active
    assert @custom_template.is_active
    info = template.activate_template
    template.reload
    @custom_template.reload
    assert template.is_active
    refute @custom_template.is_active
    assert info[:is_activated]
    assert info.key?(:disabled)
    assert_equal @custom_template.id, info[:disabled].first[:id]
  end

  test '#disable_template should set proper value' do
    template = create(
      :email_template, is_active: true, default_id: @original_template.id,
      title: "#{@original_template}_custom_2", creator: @user, state: 'published'
    )
    assert template.is_active
    template.disable_template
    template.reload
    refute template.is_active
  end

  test '#default should return proper boolean value' do
    refute @custom_template.default?
    assert @original_template.default?
  end

  test '#active? should return proper boolean value' do
    template = create(
      :email_template, is_active: false, default_id: @original_template.id,
      title: "#{@original_template}_custom_2", creator: @user, language: 'en'
    )
    refute template.active?
    assert @original_template.active?
  end

  class EventRecordingTest < ActiveSupport::TestCase

    test "records event upon creation" do
      Analytics::MetricsRecorderJob.unstub(:perform_later)
      EdcastActiveJob.unstub(:perform_later)

      TestAfterCommit.enabled = true

      stub_time = Time.now
      Timecop.freeze(stub_time) do
        org = create :organization
        actor = create(:user, organization: org, organization_role: 'admin')
        stub_id = (EmailTemplate.last&.id || 0) + 2000
        email_template = EmailTemplate.new(
          id:           stub_id,
          organization: org,
          title:        "foo123",
          creator:      actor,
          language:     "anglish",
          design:       "is this html?",
          state:        "draft",
          content:      "foo bar",
          is_active:    true
        )

        metadata = { user_id: actor.id }
        RequestStore.stubs(:read).with(:request_metadata).returns metadata

        Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries({
          recorder:         "Analytics::OrgMetricsRecorder",
          event:            "org_email_template_created",
          actor:            Analytics::MetricsRecorder.user_attributes(actor),
          org:              Analytics::MetricsRecorder.org_attributes(org),
          email_template:   Analytics::MetricsRecorder.email_template_attributes(email_template),
          timestamp:        stub_time.to_i,
          additional_data:  metadata
        }))
        email_template.save!
      end
    end

    test "records event upon deletion" do
      Analytics::MetricsRecorderJob.unstub(:perform_later)
      EdcastActiveJob.unstub(:perform_later)

      TestAfterCommit.enabled = true

      stub_time = Time.now
      Timecop.freeze(stub_time) do
        org = create :organization
        actor = create(:user, organization: org, organization_role: 'admin')
        stub_id = (EmailTemplate.last&.id || 0) + 2000
        email_template = EmailTemplate.create!(
          id:           stub_id,
          organization: org,
          title:        "foo123",
          creator:      actor,
          language:     "anglish",
          design:       "is this html?",
          state:        "draft",
          content:      "foo bar",
          is_active:    true
        )

        metadata = { user_id: actor.id }
        RequestStore.stubs(:read).with(:request_metadata).returns metadata

        Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries({
          recorder:         "Analytics::OrgMetricsRecorder",
          event:            "org_email_template_deleted",
          actor:            Analytics::MetricsRecorder.user_attributes(actor),
          org:              Analytics::MetricsRecorder.org_attributes(org),
          email_template:   Analytics::MetricsRecorder.email_template_attributes(email_template),
          timestamp:        stub_time.to_i,
          additional_data:  metadata
        }))
        email_template.destroy
      end
    end


    test "records event upon edit" do
      Analytics::MetricsRecorderJob.unstub(:perform_later)
      EdcastActiveJob.unstub(:perform_later)

      TestAfterCommit.enabled = true

      stub_time = Time.now
      Timecop.freeze(stub_time) do
        org = create :organization
        actor = create(:user, organization: org, organization_role: 'admin')
        stub_id = (EmailTemplate.last&.id || 0) + 2000
        email_template = EmailTemplate.create!(
          id:           stub_id,
          organization: org,
          title:        "foo123",
          creator:      actor,
          language:     "anglish",
          design:       "is this html?",
          state:        "draft",
          content:      "foo bar",
          is_active:    true
        )

        metadata = { user_id: actor.id }
        RequestStore.stubs(:read).with(:request_metadata).returns metadata

        Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries({
          recorder:         "Analytics::EmailTemplateEditedMetricsRecorder",
          event:            "org_email_template_edited",
          actor:            Analytics::MetricsRecorder.user_attributes(actor),
          org:              Analytics::MetricsRecorder.org_attributes(org),
          email_template:   Analytics::MetricsRecorder.email_template_attributes(email_template).merge(
            "content" => "the new"
          ),
          timestamp:        stub_time.to_i,
          additional_data:  metadata,
          changed_column: "content",
          old_val: "foo bar",
          new_val: "the new"
        }))
        email_template.update content: "the new"
      end
    end

  end

  class GetTemplateVariablesTest < ActiveSupport::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      @original_template = create(:email_template, organization: @org)
      @global_mailer_vars = %w[
        org_logo email first_name org_display_name appstore_link playstore_link
        support_email show_edcast_name show_edcast_logo promotions
        home_page header_background_color text_color section_text_color
        highlight_text_color button_blue_color button_background_color
      ]

      @global_notify_vars = %w[
        user.first_name user.last_name user.email user.org_name
        org.logo org.home_page org.name org.promotions org.appstore_link
        org.playstore_link org.support_email
        custom_css.header_background_color custom_css.text_color
        custom_css.highlight_text_color custom_css.section_text_color
        custom_css.card_text_color custom_css.button_background_color
      ]
    end

    test '#get_dynamic_variables should return constant variables for notify template' do
      @original_template.update_attributes(title: 'new_smartbite_comment')
      expected_keys = %w[
        content commenter.first_name commenter.last_name
        card.title card.title_simplified card.assignment_url card.image_url
      ]

      vars = EmailTemplate.get_dynamic_variables(@original_template.id)
      assert_same_elements expected_keys, vars[:template]
      assert_same_elements @global_notify_vars, vars[:global]
    end

    test '#get_dynamic_variables should return variables for mailer template' do
      title = 'edcast-confirmation-instruction'
      @original_template.update_attributes(title: title)
      expected_keys = %w[link_url org_logo show_app_download first_name]

      vars = EmailTemplate.get_dynamic_variables(@original_template.id)
      assert_same_elements expected_keys, vars[:template]
      assert_same_elements @global_mailer_vars, vars[:global]
    end

    test '#get_dynamic_variables should return [] for non existing template title' do
      title = 'wood'
      @original_template.update_attributes(title: title)
      vars = EmailTemplate.get_dynamic_variables(@original_template.id)

      assert_empty vars[:template]
      assert_empty vars[:global]
    end

    test '#get_proper_title should return default template title for custom template' do
      creator = create(:user, organization: @org)
      custom = create(
        :email_template, default_id: @original_template.id,
        title: 'custom_title', organization: @org, creator: creator
      )
      title = EmailTemplate.get_proper_title(custom)
      exp_title = @original_template.title

      assert_equal exp_title, title
    end
  end

  class GetActivePairTest < ActiveSupport::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      @original_template = create(:email_template, organization: @org)
      @custom_template = create(
        :email_template, is_active: true, default_id: @original_template.id,
        title: "#{@original_template.title}_custom_1", creator: @user,
        organization: @org, state: 'published'
      )
    end

    test '#active_pair should return activated pair template' do
      not_active = create(
        :email_template, is_active: false, default_id: @original_template.id,
        title: "#{@original_template.title}_custom_2", creator: @user,
        organization: @org, state: 'published'
      )

      assert_equal @custom_template, not_active.active_pair
    end

    test '#active_pair should return nil if no match' do
      @custom_template.disable_template
      not_active = create(
        :email_template, is_active: false, default_id: @original_template.id,
        title: "#{@original_template.title}_custom_2", creator: @user,
        organization: @org, state: 'published'
      )

      refute not_active.active_pair
    end
  end

  class GetReadableTitleTest < ActiveSupport::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      @original_template = create(
        :email_template, organization: @org, title: 'start_scheduledstream'
      )
      @custom_template = create(
        :email_template, is_active: true, default_id: @original_template.id,
        title: "#{@original_template.title}_custom_1", creator: @user,
        organization: @org, state: 'published'
      )
    end

    test '#get_readable_title should return correct description for a template' do
      expected_readable_title = 'When a user has to start a scheduled stream'

      custom_readable_title = EmailTemplate.get_readable_title(@custom_template)
      origin_readable_title = EmailTemplate.get_readable_title(@original_template)
      assert_equal @custom_template.title, custom_readable_title
      assert_equal expected_readable_title, origin_readable_title
    end
  end

  class GetCustomSubjectTest < ActiveSupport::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      @original_template = create(
        :email_template, organization: @org, title: 'start_scheduledstream'
      )
      @custom_template = create(
        :email_template, is_active: true, default_id: @original_template.id,
        title: "#{@original_template.title}_custom_1", creator: @user,
        organization: @org, state: 'published'
      )
    end

    test '#get_email_subject should return default subject when custom subject is not present' do
      email_subject = EmailTemplate.get_email_subject(@custom_template)
      assert_equal '{{user_name}} time to start your scheduled stream', email_subject
    end

    test '#get_email_subject should return default subject when custom template is not present' do
      original_template = create(
        :email_template, organization: @org, title: 'assigned_content'
      )
      email_subject = EmailTemplate.get_email_subject(original_template)
      assert_equal '{{assignor}} has assigned you: {{assignment.title_simplified}}', email_subject
    end

    test '#get_email_subject should return customized subject when custom subject is present on template' do
      @custom_template.update(subject: 'Time to start your scheduled stream: {{title}}')
      email_subject = EmailTemplate.get_email_subject(@custom_template)
      assert_not_equal EmailTemplate::DEFAULT_SUBJECTS['start_scheduledstream'], email_subject
      assert_equal 'Time to start your scheduled stream: {{title}}', email_subject
    end
  end
end

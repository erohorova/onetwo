require 'test_helper'

class TeamsUserTest < ActiveSupport::TestCase
  self.use_transactional_fixtures = false

  should validate_presence_of(:team)
  should validate_presence_of(:user)
  should validate_inclusion_of(:as_type).in_array(%w[admin member sub_admin])

  test 'more validations' do
    # team and user have same org
    assert_not build(:teams_user, user: create(:user)).valid?

    # uniqueness of user scoped to team
    tu = create(:teams_user)
    assert_not build(:teams_user, user: tu.user, team: tu.team).valid?
  end

  test 'should remove user assignments when removed from group' do
    stub_request(:post, "http://localhost:9200/_bulk").
      with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>/.*/}).
        to_return(:status => 200, :headers => {})

    org = create(:organization)
    team = create(:team, organization: org)
    users = create_list(:user, 2, organization: org)

    users.each {|user| team.add_member(user)}
    card = create(:card, author: users.first)

    assignments = []
    assignments << create(:assignment, assignable: card, assignee: users.first)
    assignments << create(:assignment, assignable: card, assignee: users.last)

    create(:team_assignment, team: team, assignment: assignments.first)
    create(:team_assignment, team: team, assignment: assignments.last)

    assert_equal 2, team.teams_users.count
    assert_equal 2, team.team_assignments.count
    assert_equal 1, users.first.assignments.count
    assert_equal 1, users.last.assignments.count

    # now remove user from group
    team_user = team.teams_users.where(user: users.last).first
    team_user.destroy
    team_user.run_callbacks(:commit)

    assert_equal 1, team.teams_users.count
    assert_equal 1, team.team_assignments.count
    assert_equal 1, users.first.assignments.count
    assert_equal 1, users.last.assignments.count #Dont remove assignment and user object. de-link from group only
  end

  test 'should remove user default team value when removed from group' do
    org = create(:organization)
    team = create(:team, organization: org)
    user = create(:user, organization: org)
    team.add_member(user)
    user.update_columns(default_team_id: team.id)

    # remove user from team
    team_user = team.teams_users.find_by(user: user)
    team_user.run_callbacks(:destroy) { team_user.destroy }
    user.reload
    assert_equal nil, user.default_team_id
  end

  test "should follow channel followed by teams" do
    NewTeamUserAutoFollowJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    # stub_request(:post, "http://localhost:9200/_bulk").to_return(:status => 200, :headers => {})
    org = create(:organization)
    team = create(:team, organization: org)
    channel = create(:channel, organization: org)
    user = create(:user, organization: org)

    channel.followed_teams << team
    channel.reload

    TeamsUser.any_instance.stubs(:record_add_to_group_event).returns true
    NewTeamUserAutoFollowJob.expects(:perform_later).once#.with(followers_ids: [user.id],team_ids: [team.id], channel_id: channel.id).once
    TestAfterCommit.with_commits do
      team_user = team.add_member(user)
    end
  end

  test 'should remove role group_admin from user if they are no longer sub_admin of any teams' do
    org = create(:organization)
    Role.create_standard_roles org
    team = create(:team, organization: org)
    user = create(:user, organization: org)
    team.add_sub_admin(user)
    user.teams_users.destroy_all
    assert_not_includes user.user_roles_default_names, 'group_admin'

    team.add_sub_admin(user)
    team.add_member(user)
    assert_not_includes user.user_roles_default_names, 'group_admin'
  end

  test 'should remove role group_leader from user if they are no longer admin of any teams' do
    org = create(:organization)
    Role.create_standard_roles org
    team = create(:team, organization: org)
    user = create(:user, organization: org)
    team.add_admin(user)
    user.teams_users.destroy_all
    assert_not_includes user.user_roles_default_names, 'group_leader'

    team.add_admin(user)
    team.add_member(user)
    assert_not_includes user.user_roles_default_names, 'group_leader'
  end

  test 'should not remove role group_leader from user if they are still admin of a team' do
    org = create(:organization)
    Role.create_standard_roles org
    teams = create_list(:team, 2, organization: org)
    team, team1 = teams
    user = create(:user, organization: org)
    team.add_admin(user)
    team1.add_admin(user)

    team1.teams_users.destroy_all
    assert_includes user.user_roles_default_names, 'group_leader'
  end

  test 'should not remove role group_admin from user if they are still sub_admin of a team' do
    org = create(:organization)
    Role.create_standard_roles org
    teams = create_list(:team, 2, organization: org)
    team, team1 = teams
    user = create(:user, organization: org)
    team.add_sub_admin(user)
    team1.add_sub_admin(user)

    team1.teams_users.destroy_all
    assert_includes user.user_roles_default_names, 'group_admin'
  end

  test '#can_destroy? should validate possibility of removing user from dynamic team' do
    org = create(:organization)
    mandatory_team = create(
      :team, organization: org, is_private: true,
      is_dynamic: true, is_mandatory: true
    )
    user = create(:user, organization: org)
    regular_team = create(:team, organization: org)
    # user in mandatory team
    team_user_mandatory = create(:teams_user, user: user, team: mandatory_team)
    # user not in regular team(not mandatory)
    team_user_regular = create(:teams_user, user: user, team: regular_team)
    expected_error_msg = "Can't remove a user from dynamic group"

    refute team_user_mandatory.send(:can_destroy?)
    assert_equal expected_error_msg, team_user_mandatory.full_errors
    # passing workflow context
    team_user_mandatory.is_workflow = true
    assert team_user_mandatory.send(:can_destroy?)

    assert team_user_regular.send(:can_destroy?)
  end

  class GroupEventRecordTest < ActiveSupport::TestCase

    setup do
      @org = create(:organization, name: 'LXP', host_name: 'spark')
      @team = create(:team, organization: @org)
      @actor = create :user, organization: @org
      @member = create(:user, organization: @org)
      RequestStore.unstub(:read)
      @metadata = {
        platform: 'web',
        user_agent: 'Edcast iPhone',
        user_id: @actor.id
      }
      RequestStore.store[:request_metadata] = @metadata
    end

    test 'should record event when member is added to group' do
      stub_time
      team_user = TeamsUser.create(team: @team, user: @member, as_type: "member")

      Analytics::MetricsRecorderJob.expects(:perform_later).with(
        has_entries(
          recorder: "Analytics::GroupMembershipMetricsRecorder",
          actor: Analytics::MetricsRecorder.user_attributes(@actor),
          org: Analytics::MetricsRecorder.org_attributes(@org),
          group: Analytics::MetricsRecorder.team_attributes(@team),
          member: Analytics::MetricsRecorder.user_attributes(@member),
          role: team_user.as_type,
          event: 'group_user_added',
          timestamp: Time.now.to_i,
          additional_data: @metadata
        )
      )
      team_user.run_callbacks :commit
    end

    test 'should record event when member is removed from group' do
      stub_time
      Analytics::MetricsRecorderJob.unstub :perform_later
      @team.add_member(@member)
      team_user = @team.teams_members.where(user: @member).first
      Analytics::MetricsRecorderJob.expects(:perform_later).with(
        recorder: "Analytics::GroupMembershipMetricsRecorder",
        actor: Analytics::MetricsRecorder.user_attributes(@actor),
        org: Analytics::MetricsRecorder.org_attributes(@org),
        group: Analytics::MetricsRecorder.team_attributes(@team),
        member: Analytics::MetricsRecorder.user_attributes(@member),
        role: team_user.as_type,
        event: 'group_user_removed',
        timestamp: Time.now.to_i,
        additional_data: @metadata
      )
      team_user.destroy
    end

    test 'should record event when member is edited from group' do
      stub_time
      Analytics::MetricsRecorderJob.unstub :perform_later
      @team.add_member(@member)
      team_user         = @team.teams_members.where(user: @member).first
      team_user.as_type = "admin"
      Analytics::MetricsRecorderJob.expects(:perform_later).with(
        recorder:         "Analytics::GroupMembershipMetricsRecorder",
        actor:            Analytics::MetricsRecorder.user_attributes(@actor),
        org:              Analytics::MetricsRecorder.org_attributes(@org),
        group:            Analytics::MetricsRecorder.team_attributes(@team),
        member:           Analytics::MetricsRecorder.user_attributes(@member),
        role:             team_user.as_type,
        event:            'group_user_edited',
        timestamp:        Time.now.to_i,
        changed_column:   'as_type',
        old_val:          'member',
        new_val:          'admin',
        additional_data:  @metadata
      )
      team_user.save
    end

    test 'should not record event for everyone group' do
      Analytics::MetricsRecorderJob.expects(:perform_later).never

      user = create(:user, organization: @org)
      @team.update is_everyone_team: true
      @team.add_member user
    end

    test 'should not reindex team when skip_team_indexing is true' do
      org = create :organization
      team = create :team, organization: org
      user = create :user, organization: org
      Team.any_instance.expects(:reindex_async).never
      team.add_user(user, skip_team_indexing: true)
    end
  end
end

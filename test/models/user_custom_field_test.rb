require 'test_helper'

class UserCustomFieldTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  setup do
    stub_time
    @org = create :organization
    @actor = create :user, organization: @org
    @member = create :user, organization: @org
    @custom_field = create :custom_field, {
      organization: @org,
      display_name: "Flavor",
      abbreviation: "flav"
    }
    @metadata = { user_id: @actor.id }
    RequestStore.stubs(:read).with(:request_metadata).returns @metadata
  end

  test "records event on create" do
    id = (UserCustomField.last&.id || 0) + 1
    user_custom_field = UserCustomField.new(
      id: id,
      custom_field: @custom_field,
      user: @member,
      value: "foobar"
    )
    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        recorder: "Analytics::UserCustomFieldRecorder",
        org: Analytics::MetricsRecorder.org_attributes(@org),
        actor: Analytics::MetricsRecorder.user_attributes(@actor),
        member: Analytics::MetricsRecorder.user_attributes(@member),
        custom_field: Analytics::MetricsRecorder.custom_field_attributes(@custom_field),
        user_custom_field: Analytics::MetricsRecorder.user_custom_field_attributes(user_custom_field),
        event: "user_custom_field_added",
        additional_data: @metadata,
        timestamp: Time.now.to_i
      )
    )
    user_custom_field.run_callbacks(:commit) do
      user_custom_field.save!
    end
  end

  test "records event on edit" do
    user_custom_field = UserCustomField.create(
      custom_field: @custom_field,
      user: @member,
      value: "foobar"
    )
    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        recorder: "Analytics::UserCustomFieldRecorder",
        org: Analytics::MetricsRecorder.org_attributes(@org),
        actor: Analytics::MetricsRecorder.user_attributes(@actor),
        member: Analytics::MetricsRecorder.user_attributes(@member),
        custom_field: Analytics::MetricsRecorder.custom_field_attributes(@custom_field),
        user_custom_field: Analytics::MetricsRecorder.user_custom_field_attributes(user_custom_field).merge(
          "value" => "asd123123"
        ),
        event: "user_custom_field_edited",
        changed_column: "value",
        old_val: "foobar",
        new_val: "asd123123",
        additional_data: @metadata,
        timestamp: Time.now.to_i
      )
    )
    user_custom_field.update value: "asd123123"
  end

  test "records event on destroy" do
    user_custom_field = UserCustomField.create(
      custom_field: @custom_field,
      user: @member,
      value: "foobar"
    )
    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        recorder: "Analytics::UserCustomFieldRecorder",
        org: Analytics::MetricsRecorder.org_attributes(@org),
        actor: Analytics::MetricsRecorder.user_attributes(@actor),
        member: Analytics::MetricsRecorder.user_attributes(@member),
        custom_field: Analytics::MetricsRecorder.custom_field_attributes(@custom_field),
        user_custom_field: Analytics::MetricsRecorder.user_custom_field_attributes(user_custom_field),
        event: "user_custom_field_removed",
        additional_data: @metadata,
        timestamp: Time.now.to_i
      )
    )
    user_custom_field.destroy
  end
end

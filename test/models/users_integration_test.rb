require 'test_helper'

class UsersIntegrationTest < ActiveSupport::TestCase
  should have_db_column(:user_id).of_type(:integer)
  should have_db_column(:integration_id).of_type(:integer)

  should belong_to :integration
  should belong_to :user

  should validate_presence_of(:user_id)
  should validate_presence_of(:integration_id)

  test '.connected' do
    assert scrub(UsersIntegration.connected.to_sql).include?("WHERE users_integrations.connected = 1")
  end
end

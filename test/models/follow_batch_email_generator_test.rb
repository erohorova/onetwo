require 'test_helper'

class FollowBatchEmailGeneratorTest < ActiveSupport::TestCase

  self.use_transactional_fixtures = false

  setup do
    FollowBatchEmailGenerator.unstub :perform_later
    FollowEmailNotificationGeneratorJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    @org = create(:organization)
    create(:batch_job, :follow_email)
  end

  test 'should update last run time' do
    now = Time.now
    Time.stubs(:now).returns(now)
    FollowBatchEmailGenerator.perform
    batch_job_entry = BatchJob.where(name: 'FollowBatchEmailGenerator').first
    assert_equal now.strftime("%FT%TZ"), batch_job_entry.last_run_time.strftime("%FT%TZ")
  end

  test 'should not send email if notifications read' do
    skip("INTERMITTENT FAILURE ON TRAVIS")
    NotificationGeneratorJob.unstub :perform_later

    now = Time.now
    Time.stubs(:now).returns(now)
    u = create(:user, organization: @org)
    f1, f2 = create_list(:user, 2, organization: @org)

    assert_difference ->{Notification.count} do
      create(:follow, followable: u, user: f1, created_at: Time.now.utc - 1.hour)
    end
    n = Notification.last
    n.update_attributes(unseen: false)

    UserMailer.expects(:follower_batch_notification_email).never
    FollowBatchEmailGenerator.perform

    # Next user follows. Notification unread. Should send user mailer
    create(:follow, followable: u, user: f2)

    # It's the future. Code writes itself now
    now = Time.now + 1.hour
    Time.stubs(:now).returns(now)
    mailer = mock()
    mailer.expects(:deliver_now)
    UserMailer.expects(:follower_batch_notification_email).with(u, [f2]).returns(mailer).once
    FollowBatchEmailGenerator.perform
  end

  test 'should send notification about a follower only once' do
    skip("INTERMITTENT FAILURE ON TRAVIS")
    NotificationGeneratorJob.unstub :perform_later
    now = Time.now
    Time.stubs(:now).returns(now)
    mailer = mock()
    mailer.expects(:deliver_now).returns(nil).twice

    u = create(:user, organization: @org)
    f1, f2 = create_list(:user, 2, organization: @org)
    [f1, f2].each {|f| create(:follow, followable: u, user: f, created_at: Time.now.utc - 1.hour)}
    UserMailer.expects(:follower_batch_notification_email).with(u, [f1, f2]).returns(mailer).once
    FollowBatchEmailGenerator.perform

    f3, f4 = create_list(:user, 2, organization: @org)
    [f3, f4].each {|f| create(:follow, followable: u, user: f, created_at: Time.now.utc)}
    now = Time.now + 4.hours
    Time.stubs(:now).returns(now)
    UserMailer.expects(:follower_batch_notification_email).with(u, [f3, f4]).returns(mailer).once
    FollowBatchEmailGenerator.perform
  end

  test 'should skip follows with skip_notification set to true' do
    skip("INTERMITTENT FAILURE ON TRAVIS")
    NotificationGeneratorJob.unstub :perform_later

    now = Time.now
    Time.stubs(:now).returns(now)
    mailer = mock()
    mailer.expects(:deliver_now).returns(nil).once

    u = create(:user, organization: @org)
    f1, f2, f3 = create_list(:user, 3, organization: @org)
    [f1, f2, f3].each {|f| create(:follow, followable: u, user: f, created_at: Time.now.utc - 1.hour, skip_notification: (f.id==f3.id))}
    UserMailer.expects(:follower_batch_notification_email).with(u, [f1, f2]).returns(mailer).once
    FollowBatchEmailGenerator.perform
  end

  test 'should skip follows with followee.pref_notification_follow false' do
    skip("INTERMITTENT FAILURE ON TRAVIS")
    NotificationGeneratorJob.unstub :perform_later

    now = Time.now
    Time.stubs(:now).returns(now)
    mailer = mock()
    mailer.expects(:deliver_now)

    u1, u2, f1, f2, f3 = create_list(:user, 5, organization: @org)

    # pref off for u2; u1 pref is on by default
    u2.pref_notification_follow = false

    [f1, f2].each {|f| create(:follow, followable: u1, user: f, created_at: Time.now.utc - 1.hour)}
    create(:follow, followable: u2, user: f3, created_at: Time.now.utc - 1.hour)

    # mail for u1 but not u2
    UserMailer.expects(:follower_batch_notification_email).with(u1, [f1, f2]).returns(mailer)
    UserMailer.expects(:follower_batch_notification_email).with(u2, anything).never

    FollowBatchEmailGenerator.perform
  end
end

require 'test_helper'

class DailyCardStatsTest < ActiveSupport::TestCase
  test "should return the daily stats for a given card" do
    skip
    card = create(:card)
    today = DateTime.now.utc.to_date
    DailyCardStat.create(card_id: card.id, date: today, impressions: 711)
    DailyCardStat.create(card_id: card.id, date: today-1, impressions: 0)
    DailyCardStat.create(card_id: card.id, date: today-2, impressions: 12)
    DailyCardStat.create(card_id: card.id, date: today-3, impressions: 1)

    resp = DailyCardStat.get_card_daily_stats(card.id, today, 7)
    assert_equal 7, resp.count
    assert_equal 711, resp.last[:impressions]
  end
end

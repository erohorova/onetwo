require 'test_helper'

class TagTest < ActiveSupport::TestCase

  test "extract tag from single hashtag text" do
    text = 'this is a #hashtag'
    tags = Tag.extract_tags text
    assert_equal tags.count, 1
    assert_equal tags[0].name, 'hashtag'
  end

  test 'should downcase tag name' do
    skip 'disabled'
    tg = Tag.new(name: 'Tag1')
    tg.save
    assert_equal tg.reload.name, 'tag1'
  end

  test "extract multiple tags from text" do
    text = '#tag1 another #tag2'
    tags = Tag.extract_tags text
    assert_equal tags.count, 2
    assert_equal tags[0].name, 'tag1'
    assert_equal tags[1].name, 'tag2'
  end

  test "extract multiple tags from text, pipe delimited" do
    text = 'tag1 | tag2 ||| '
    tags = Tag.extract_tags text, :pipe_delimited
    assert_equal tags.count, 2
    assert_equal tags[0].name, 'tag1'
    assert_equal tags[1].name, 'tag2'
  end

  test "no tags in text" do
    text = 'no tags here, move on'
    tags = Tag.extract_tags text
    assert_equal tags.count, 0

    text = 'this is no hash#tag'
    tags = Tag.extract_tags text
    assert_equal tags.count, 0
  end

  test 'extract tags from text in html' do
    text = '<p>this is a #tag</p>'
    tags = Tag.extract_tags text
    assert_equal tags.count, 1
    assert_equal tags[0].name, 'tag'
  end

  test 'do not extract tags from non text html' do
    text = '<p> this is a #tag<span style="color: #333333"> this is #anothertag</span></p>'
    tags = Tag.extract_tags text
    assert_equal tags.count, 2
    assert_equal tags[0].name, 'tag'
    assert_equal tags[1].name, 'anothertag'
  end

  test "if tag exists, then don't duplicate, return existing" do
    existing_tag = create(:tag, name: 'tag1')
    text = '#tag1'
    assert_no_difference 'Tag.count' do
      Tag.extract_tags text
    end
  end
end

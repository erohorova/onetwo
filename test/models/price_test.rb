require 'test_helper'

class PriceTest < ActiveSupport::TestCase
  should belong_to(:card)
  should validate_presence_of(:amount)
  should validate_presence_of(:currency)


  test 'validates uniquess of card id scoped to currency' do
    org = create(:organization)

    card = create(:card, author_id: nil, organization: org)
    price  = create(:price, card: card, currency: "INR", amount: "12")
    price1 = build(:price, card: card, currency: "INR", amount: "12")

    refute price1.valid?
  end

  test 'should round off the amount to nearest integer before saving' do
    org = create(:organization)

    card = create(:card, author_id: nil, organization: org)
    price  = create(:price, card: card, currency: "INR", amount: "12.7")

    assert_equal BigDecimal.new(13), price.reload.amount
  end
end

require 'test_helper'

class RecordTest < ActiveSupport::TestCase

  test 'should submit a transcoding job to AWS' do
    TranscoderJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    aws_client = AWS::ElasticTranscoder::Client.new

    AWS::ElasticTranscoder::Client.stubs(:new).returns(aws_client)

    create_job_mock = mock()
    create_job_mock.stubs(:data).returns({job: {id:'job123'}})
    aws_client.stubs(:create_job).with(){|params|
      !params.has_key?(:playlists) }.returns(create_job_mock)

    video_stream = create(:wowza_video_stream)
    recording = create(:recording, video_stream: video_stream)
    recording.run_callbacks(:commit)
    recording.reload

    assert_equal 'job123', recording.last_aws_transcoding_job_id
    assert_equal 'submitted', recording.transcoding_status
  end

  test 'should submit a transcoding job to AWS using the new playlist request' do
    TranscoderJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    aws_client = AWS::ElasticTranscoder::Client.new

    AWS::ElasticTranscoder::Client.stubs(:new).returns(aws_client)
    Settings.transcoder.stubs(:hls_preset_id).returns('1,2,3')
    create_job_mock = mock()
    create_job_mock.stubs(:data).returns({job: {id:'job123'}})
    aws_client.stubs(:create_job).with(){|params|
                                          assert_equal true, params.has_key?(:playlists)
                                          playlist = params[:playlists].first
                                          assert_equal 'hls_playlist', playlist[:name]
                                          assert_equal 'HLSv3', playlist[:format]
                                          assert_equal ['high_hls_output', 'medium_hls_output', 'low_hls_output'], playlist[:output_keys]

    }.returns(create_job_mock)

    video_stream = create(:wowza_video_stream)
    recording = create(:recording, video_stream: video_stream)
    recording.run_callbacks(:commit)
    recording.reload

    assert_equal 'job123', recording.last_aws_transcoding_job_id
    assert_equal 'submitted', recording.transcoding_status
  end

  test 'should destroy all thumbnails when destroyed' do
    video_stream = create(:wowza_video_stream)
    recording = create(:recording, video_stream: video_stream)
    (0..2).each { |i| create(:thumbnail, recording: recording, sequence_number: i) }
    assert_difference -> {Thumbnail.count}, -3 do
      recording.destroy
    end
  end

  test 'mark default with existing client recording' do
    v = create(:iris_video_stream)
    recording = create(:recording, video_stream: v, source: 'client', sequence_number: 0, default: false)
    recording.mark_default!
    assert_equal recording.id, v.recordings.default.first.id

    new_recording = create(:recording, video_stream: v, source: 'client', sequence_number: 1, default: false)
    new_recording.mark_default!
    assert_equal new_recording.id, v.recordings.default.first.id

    server_recording = create(:recording, video_stream: v, source: 'server', sequence_number: 2, default: false)
    server_recording.mark_default!
    assert_equal new_recording.id, v.recordings.default.first.id
  end

  test 'mark default with existing server recording' do
    v = create(:iris_video_stream)
    recording = create(:recording, video_stream: v, source: 'server', sequence_number: 0, default: false)
    recording.mark_default!
    assert_equal recording.id, v.recordings.default.first.id

    server_recording = create(:recording, video_stream: v, source: 'server', sequence_number: 1, default: false)
    server_recording.mark_default!
    assert_equal server_recording.id, v.recordings.default.first.id

    new_recording = create(:recording, video_stream: v, source: 'client', sequence_number: 2, default: false)
    new_recording.mark_default!
    assert_equal new_recording.id, v.recordings.default.first.id
  end

  test 'mark default for an already default recording' do
    v = create(:iris_video_stream)
    recording = create(:recording, video_stream: v, source: 'server', sequence_number: 0, default: true)
    recording.mark_default!
    assert_equal recording.id, v.recordings.default.first.try(:id)
  end

end

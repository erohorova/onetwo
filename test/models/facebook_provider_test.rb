require 'test_helper'

class FacebookProviderTest < ActiveSupport::TestCase

  setup do
    @org = create(:organization)
    user = create(:user, organization: @org)
    group = create(:resource_group, organization: @org)
    group.add_member user
    @question = create(:question, user: user, group: group)
  end

  test 'returns `NIL` if message & ref_url is blank' do
    identity_provider = create(:identity_provider, :facebook)

    facebook_provider = FacebookProvider.find(identity_provider.id)
    @question.expects(:ref_url).returns(nil)

    assert_nil facebook_provider.share(message: '', object: @question)
  end

  test 'should post to facebook' do
    identity_provider = create(:identity_provider, :facebook)

    facebook_provider = FacebookProvider.find(identity_provider.id)

    stub_request(:post, "https://graph.facebook.com/me/feed").
        with(:body => {"access_token"=>facebook_provider.token, "caption"=>"group", "description"=>"This is a question", "link"=>/.*/, "message"=>"testing messing", "name"=>"Question"},
             :headers => {'Accept'=>'*/*', 'Content-Type'=>'application/x-www-form-urlencoded', 'Date'=> /.*/, 'User-Agent'=> /.*/ }).
        to_return(:status => 200, :body => '{"id":"3213984_10102622119059213"}', :headers => {})
    endpoint = facebook_provider.share(message: 'testing messing', object: @question)

    assert_equal 'http://www.facebook.com/3213984_10102622119059213', endpoint
  end

  test 'should share card with fb' do
    create_default_org
    c = create(:card, slug: 'test', author: create(:user, organization: Organization.default_org))
    identity_provider = create(:identity_provider, :facebook)

    facebook_provider = FacebookProvider.find(identity_provider.id)

    stub_request(:post, "https://graph.facebook.com/me/feed").
      with(:body => {"access_token"=>facebook_provider.token, "link"=>"https://#{Organization.default_org.host}/insights/#{c.slug}", "message"=>"testing messing"},
             :headers => {'Accept'=>'*/*', 'Content-Type'=>'application/x-www-form-urlencoded', 'Date'=> /.*/, 'User-Agent'=> /.*/ }).
        to_return(:status => 200, :body => '{"id":"3213984_10102622119059213"}', :headers => {})
    endpoint = facebook_provider.share(message: 'testing messing', object: c)

    assert_equal 'http://www.facebook.com/3213984_10102622119059213', endpoint
  end

  test 'should share card with fb from an org' do
    organization = create(:organization)
    user = create(:user, organization: @org)
    org_user = user.clone_for_org(organization, 'member')
    c = create(:card, slug: 'test', author_id: org_user.id)

    identity_provider = create(:identity_provider, :facebook)

    facebook_provider = FacebookProvider.find(identity_provider.id)

    stub_request(:post, "https://graph.facebook.com/me/feed").
      with(:body => {"access_token"=>facebook_provider.token, "link"=>"https://#{organization.host}/insights/#{c.slug}", "message"=>"testing messing"},
             :headers => {'Accept'=>'*/*', 'Content-Type'=>'application/x-www-form-urlencoded', 'Date'=> /.*/, 'User-Agent'=> /.*/ }).
        to_return(:status => 200, :body => '{"id":"3213984_10102622119059213"}', :headers => {})
    endpoint = facebook_provider.share(message: 'testing messing', object: c)

    assert_equal 'http://www.facebook.com/3213984_10102622119059213', endpoint
  end

  test 'should determine scopes' do
    fb = create(:facebook_provider, uid: '123', user: create(:user), token_scope: nil)
    fb.stubs(:resolve_token_scope)

    assert fb.scoped?(:network)

    fb.update(token_scope: 'publish_actions')

    assert fb.scoped?(:share)
    assert_not fb.scoped?(:network)
  end
end

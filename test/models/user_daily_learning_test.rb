require 'test_helper'

class UserDailyLearningTest < ActiveSupport::TestCase

  test 'should set initial state on creation' do
    skip
    udl = create(:user_daily_learning)
    assert_equal DailyLearningProcessingStates::INITIALIZED, udl.processing_state
  end

  test 'available' do
    skip
    user = create(:user)
    date = Time.now.utc.to_date.to_s
    udl = create(:user_daily_learning, :initialized, user: user, date: date)
    refute udl.available?

    udl.update_attributes(processing_state: DailyLearningProcessingStates::STARTED)
    refute udl.available?

    udl.update_attributes(processing_state: DailyLearningProcessingStates::ERROR)
    refute udl.available?

    udl.update_attributes(processing_state: DailyLearningProcessingStates::COMPLETED)
    assert udl.available?
  end

  test 'initialize and start' do
    user = create(:user)
    date = Time.now.utc.to_date.to_s
    DailyLearningForUserJob.expects(:perform_later).returns(nil).once
    assert_difference ->{UserDailyLearning.count} do
      UserDailyLearning.initialize_and_start(user_id: user.id, date: date)
    end

    udl = UserDailyLearning.where(user: user, date: date).first
    assert_equal DailyLearningProcessingStates::INITIALIZED, udl.processing_state
  end

  class ProcessMethodTests < ActiveSupport::TestCase
    setup do
      skip
      @date = Time.now.utc.to_date.to_s
      @user = create(:user)
      @user_profile = create(:user_profile, user: @user)

      @topic_ids = @user_profile.learning_topics.map{|t| t["topic_id"]}
      @topics = @user_profile.learning_topics

      @udl = create(:user_daily_learning, user: @user, date: @date)
    end

    test 'process should create new topic daily learning entries' do
      TopicDailyLearning.any_instance.stubs(:process!)
      assert_difference ->{TopicDailyLearning.count}, @topic_ids.length do
        @udl.process!
      end

      assert_equal [true] * @topic_ids.length, TopicDailyLearning.all.map(&:requested_by_user)
    end


    test 'process should not create new topic daily entries if they already exist' do
      UserDailyLearningService.any_instance.stubs(:pick_daily_learning_from_topics).returns([])
      @topics.each do |topic|
        create(:topic_daily_learning, :completed, topic_id: topic["topic_id"], topic_name: topic["topic_name"], date: @date, organization_id: @user.organization_id, ecl_ids: [1,2,3])
      end

      assert_no_difference ->{TopicDailyLearning.count} do
        @udl.process!
      end
    end

    test 'process should call process on topic daily learning entries' do
      TopicDailyLearning.any_instance.expects(:process!).times(@topic_ids.length)
      @udl.process!
    end

    test 'process should not reprocess if topics already have data' do
      UserDailyLearningService.any_instance.stubs(:pick_daily_learning_from_topics).returns([])
      @topics.each do |topic|
        create(:topic_daily_learning, :completed, topic_id: topic["topic_id"], topic_name: topic["topic_name"], date: @date, organization_id: @user.organization_id, ecl_ids: [1,2,3])
      end
      TopicDailyLearning.any_instance.expects(:process!).never

      @udl.process!
    end

    test 'process should mark entry as completed when done' do
      TopicDailyLearning.any_instance.stubs(:process!)

      @udl.process!
      @udl.reload
      assert_equal DailyLearningProcessingStates::COMPLETED, @udl.processing_state
    end

    test 'should mark entry as errored and raise if processing errors out' do
      TopicDailyLearning.any_instance.stubs(:process!)
      service = mock()
      ecl_ids = [1,2,3]
      service.expects(:pick_daily_learning_from_topics).with(@topic_ids).raises(StandardError)
      UserDailyLearningService.expects(:new).with(@user).returns(service)

      assert_raises do
        @udl.process!
      end
      @udl.reload

      assert_equal DailyLearningProcessingStates::ERROR, @udl.processing_state
    end

    test 'process should call service and populate card ids' do
      TopicDailyLearning.any_instance.stubs(:process!)
      service = mock()
      card_ids = [1,2,3]
      service.expects(:pick_daily_learning_from_topics).with(@topic_ids).returns(card_ids)
      UserDailyLearningService.expects(:new).with(@user).returns(service)

      @udl.process!
      @udl.reload

      assert_equal DailyLearningProcessingStates::COMPLETED, @udl.processing_state
      assert_equal card_ids, @udl.card_ids
    end
  end
end

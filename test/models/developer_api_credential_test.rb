require 'test_helper'

class DeveloperApiCredentialTest < ActiveSupport::TestCase
  self.use_transactional_fixtures = false

  test 'should record event when credentials generated' do
    skip("INTERMITTENT FAILURE ON TRAVIS")
    stub_time
    unstub_metrics_recorder

    api_key = "iam-randomly-generated-string"
    DeveloperApiCredential.any_instance
    .stubs(:api_key)
    .returns(api_key)

    org = create(:organization)
    admin = create(:user, organization: org)
    metadata = {
      platform: 'web',
      user_id: admin.id,
      user_agent: 'chrome'
    }

    RequestStore.stubs(:read).with(:request_metadata).returns(
      metadata
    )

    EdcastActiveJob.unstub(:perform_later)
    Analytics::MetricsRecorderJob.unstub(:perform_later)

    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: 'Analytics::OrgMetricsRecorder',
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(admin),
      timestamp: Time.now.to_i,
      additional_data: metadata.merge(developer_api_key: api_key),
      event: 'org_developer_api_credentials_created'
    ).once

    developer_api_credentials = create(:developer_api_credential,
      organization: org,
      creator: admin
    )
  end

  test 'should record event when credentials deleted' do
    stub_time

    api_key = "iam-randomly-generated-string"
    DeveloperApiCredential.any_instance
    .stubs(:api_key)
    .returns(api_key)

    org = create(:organization)
    admin = create(:user, organization: org)
    metadata = {
      platform: 'web',
      user_id: admin.id,
      user_agent: 'chrome'
    }

    developer_api_credentials = create(:developer_api_credential,
      organization: org,
      creator: admin
    )

    RequestStore.stubs(:read).with(:request_metadata).returns(
      metadata
    )
    unstub_metrics_recorder

    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: 'Analytics::OrgMetricsRecorder',
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(admin),
      timestamp: Time.now.to_i,
      additional_data: metadata.merge(developer_api_key: api_key),
      event: 'org_developer_api_credentials_deleted'
    ).once

    developer_api_credentials.destroy
  end
end
# frozen_string_literal: true

require 'test_helper'

class WorkflowTest < ActiveSupport::TestCase
  setup do
    @org = create :organization
    @workflow = create :workflow, organization: @org
  end
end
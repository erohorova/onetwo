require 'test_helper'

class RechargeTest < ActiveSupport::TestCase
  should have_db_column(:skillcoin).of_type(:decimal)
  should have_db_column(:active).of_type(:boolean)
  should have_db_column(:description).of_type(:text)
  should have_db_column(:sku).of_type(:string)

  should validate_presence_of(:skillcoin)
  should validate_presence_of(:sku)

  test '#redirect_page_url must return url for skillcoins page' do
    org = create(:organization, host_name: 'test')
    recharge = create(:recharge)

    assert_equal 'http://test.test.host/me/skill-coins', recharge.redirect_page_url(org: org)
  end
end

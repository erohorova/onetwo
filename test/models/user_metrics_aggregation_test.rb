require 'test_helper'

class UserMetricsAggregationTest < ActiveSupport::TestCase

  setup do
    @org = create :organization
    @klass = UserMetricsAggregation
  end

  test 'validations' do
    record = @klass.new
    required_attrs = %i{
      organization_id            user_id
      smartbites_commented_count smartbites_completed_count
      smartbites_consumed_count  smartbites_created_count
      smartbites_liked_count     time_spent_minutes
      total_smartbite_score      total_user_score
      end_time                   start_time
    }
    expected = required_attrs.each_with_object({}) do |attr, memo|
      memo[attr] = ["can't be blank"]
    end
    actual = record.tap(&:valid?).errors.messages
    assert_equal expected, actual
  end

  # NOTE: this is intentional behavior.
  # We bulk-create these records using data we get from influx.
  # Therefore the referenced ids may no longer exist in SQL
  test 'does not check validity of foreign keys' do
    record = @klass.new(
      organization_id:            0,
      user_id:                    0,
      smartbites_commented_count: 0,
      smartbites_completed_count: 0,
      smartbites_consumed_count:  0,
      smartbites_created_count:   0,
      smartbites_liked_count:     0,
      time_spent_minutes:         0,
      total_smartbite_score:      0,
      total_user_score:           0,
      end_time:                   0,
      start_time:                 0
    )
    assert record.valid?
    record.save
    assert record.persisted?
  end

    test 'month_wise_avg_time_spent_by_org_users method should return month wise avg time spent data of the org users' do
      now                 = Time.now.utc
      users               = create_list(:user, 6, organization: @org)
      dates               = [now, now-1.month, now-2.month, now-3.month, now-4.month, now-5.month]
  
      dates.each do |d|
        users.each_with_index do |user, indx|
          create(:user_metrics_aggregation, 
            organization:         @org, 
            user:                 user,
            time_spent_minutes:   indx,
            start_time:           d.beginning_of_day,
            end_time:             d.end_of_day,
            created_at:           d
          )
        end
      end
      expected_years    = []
      expected_months   =  []
      dates.sort.reverse.each do |d|
        expected_months << d.month
        expected_years  << d.year
      end

      org_users_data = UserMetricsAggregation.avg_time_spent_by_users(org_id: @org.id, period: "month")

      actual_avg_time = []
      actual_months = []
      actual_years = []
      org_users_data.each do |data|
        actual_avg_time << data["avg_time_spent"].to_i
        actual_months   << data["month"]
        actual_years    << data["year"]
      end

      assert_equal [2, 2, 2, 2, 2, 2], actual_avg_time 
      assert_equal expected_months, actual_months
      assert_equal expected_years, actual_years
    end

    test 'month_wise_time_spent method should return month wise sum of time spent data of requested user' do
      now                 = Time.now.utc
      users               = create_list(:user, 6, organization: @org)
      dates               = [now, now-1.month, now-2.month, now-3.month, now-4.month, now-5.month]
  
      dates.each do |d|
        users.each_with_index do |user, indx|
          create(:user_metrics_aggregation, 
            organization:         @org, 
            user:                 user,
            time_spent_minutes:   indx,
            start_time:           d.beginning_of_day,
            end_time:             d.end_of_day,
            created_at:           d
          )
        end
      end
      expected_years    = []
      expected_months   =  []
      dates.sort.reverse.each do |d|
        expected_months << d.month
        expected_years  << d.year
      end

      user_data = UserMetricsAggregation.time_spent_by_user(user_id: users.last.id, period: "month")

      actual_avg_time = []
      actual_months = []
      actual_years = []
      user_data.each do |data|
        actual_avg_time << data["time_spent"].to_i
        actual_months   << data["month"]
        actual_years    << data["year"]
      end

      assert_equal [5, 5, 5, 5, 5, 5], actual_avg_time 
      assert_equal expected_months, actual_months
      assert_equal expected_years, actual_years
    end
  
end

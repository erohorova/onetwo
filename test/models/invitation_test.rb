require 'test_helper'

class InvitationTest < ActiveSupport::TestCase
  should allow_value('test@example.com').for(:recipient_email)
  should_not allow_value('test-example.com').for(:recipient_email)

  should validate_uniqueness_of(:recipient_email)
  class OrgainizationEmailTest < ActiveSupport::TestCase
    setup do
      stub_request(:post, "https://api.branch.io/v1/url").
          to_return(:status => 200, :body => "{\"url\":\"http://branch.io\"}", :headers => {})
    end
    test 'send out organization email' do
      organization = create(:organization)
      sender = create(:user, organization: organization)
      invitation = create(:invitation, roles: 'admin', recipient_email: generate(:email_address), invitable: organization, sender: sender)
      host =''
      MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
        host = message[:message][:global_merge_vars].first['content']
      }
        invitation.run_callbacks(:commit)
        refute_nil host

    end

    test 'allow same email address for different organization' do
      organization1 = create(:organization)
      organization2 = create(:organization)
      email = generate(:email_address)
      result1 =  create(:invitation, roles: 'admin', recipient_email: email, invitable: organization1)
      result2 = create(:invitation, roles: 'admin', recipient_email: email, invitable: organization2)

      assert_equal true, result1.persisted?
      assert_equal true, result2.persisted?
      assert_raise ActiveRecord::RecordInvalid do
        create(:invitation, roles: 'admin', recipient_email: email, invitable: organization2)
      end
    end
  end

  test 'send out team email' do
    stub_request(:post, "https://api.branch.io/v1/url").
        to_return(:status => 200, :body => "{\"url\":\"http://branch.io\"}", :headers => {})

      MandrillMailer.any_instance.expects(:send_api_email).once
      org = create(:organization)
      team = create(:team, organization: org)
      sender = create(:user, organization: org)

      invitation = create(:invitation, roles: 'admin', recipient_email: generate(:email_address), invitable: team, sender: sender)
      invitation.run_callbacks(:commit)
  end

  test 'should run reindex_team after commit if Team is invitable' do
    org = create(:organization)
    team = create(:team, organization: org)
    sender = create(:user, organization: org)
    persisted_inv = create(
      :invitation, roles: 'admin', recipient_email: generate(:email_address),
      invitable: team, sender: sender
    )

    Invitation.any_instance.expects(:reindex_team).twice

    # create
    create(
      :invitation, roles: 'admin', recipient_email: generate(:email_address),
      invitable: team, sender: sender
    ).committed!
    #deatroy
    persisted_inv.destroy.committed!
    # `org` is invitable
    create(
      :invitation, roles: 'admin', recipient_email: generate(:email_address),
      invitable: org, sender: sender
    ).committed!
  end

  test 'should destroy invitation and not run reindex for removed team' do
    org = create(:organization)
    team = create(:team, organization: org)
    sender = create(:user, organization: org)
    persisted_inv = create(
      :invitation, roles: 'admin', recipient_email: generate(:email_address),
      invitable: team, sender: sender
    )

    Invitation.any_instance.expects(:reindex_team).once

    team.destroy
    persisted_inv.destroy.committed!
  end
end

require 'test_helper'

class ProfileTest < ActiveSupport::TestCase
  should belong_to :user
  should belong_to :organization

  test 'should return error if user_id not exists in organization profile' do
    org = create(:organization)
    user = create(:user, organization: org)
    profile = user.build_external_profile(
      organization_id: org.id,
      uid: '123',
      external_id: '123'
    )
    profile1 = org.profiles.new(
      user_id: user.id + 1,
      uid: '1234',
      external_id: '1234'
      )

    assert profile.valid?
    refute profile1.valid?
  end
end

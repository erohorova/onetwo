require 'test_helper'

class AnswerTest < ActiveSupport::TestCase

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    Post.any_instance.stubs(:p_read_by_creator)
    @org = create(:organization)
    @user = create(:user, organization: @org)
    @group = create(:resource_group, organization: @org)
    @group.add_member @user
    @question = create(:question, user_id: @user.id, postable: @group)
  end

  test 'save answer for a question' do
    answer = build(:answer, commentable: @question, user_id: @user.id)
    assert answer.save
  end

  test 'do not save answer without a commentable attached' do
    answer = build(:answer, commentable_id: nil, user_id: @user.id)
    assert_not answer.save
  end

  test 'do not save answer without a creator' do
    answer = build(:answer, commentable: @question, user_id: nil)
    assert_not answer.save
  end

  test 'deleting an answer should delete associations' do
    u = create(:user)
    g = create(:resource_group)
    g.add_member u

    q = create(:question, group: g, user: u)
    a = create(:answer, commentable: q, user: u)
    tag = create(:tag)
    resource = create(:resource)
    a.tags << tag
    a.resource = resource
    a.save

    voter = create(:user)
    g.add_member voter
    create(:vote, votable: a, voter: voter)

    approver = create(:user)
    g.add_admin approver
    create(:approval, approvable: a, approver: approver)

    score_before = u.score g
    assert_equal score_before[:score], 1
    assert_equal score_before[:stars], 1

    assert_difference [ ->{Vote.count}, ->{Approval.count}, ->{Tagging.count}], -1 do
      a.destroy
    end

    score_after = u.score g
    assert_equal score_after[:score], 0
    assert_equal score_after[:stars], 0
  end

  test 'answer creation should inrement count' do
    u = create(:user)
    g = create(:resource_group)
    g.add_member u

    t = create(:user)
    g.add_admin t
    q = create(:question, group: g, user: u)

    assert_difference ->{q.reload.student_answer_count} do
      a = create(:answer, commentable: q, user: u)
    end

    assert_difference ->{q.reload.faculty_answer_count} do
      a = create(:answer, commentable: q, user: t)
    end
  end

  test 'answer destroy should decrement count' do
    u = create(:user)
    g = create(:resource_group)
    g.add_member u

    t = create(:user)
    g.add_admin t
    q = create(:question, group: g, user: u)
    sa = create(:answer, commentable: q, user: u)
    ta = create(:answer, commentable: q, user: t)

    assert_difference ->{q.reload.student_answer_count}, -1 do
      sa.destroy
    end

    assert_difference ->{q.reload.faculty_answer_count}, -1 do
      ta.destroy
    end
  end

  test 'should create and delete activity stream' do
    stub_request(:post, %r{http://localhost:9200/_bulk})
    Post.any_instance.stubs :remove_post_reads

    EdcastActiveJob.unstub :perform_later
    StreamCreateJob.unstub :perform_later
    TestAfterCommit.with_commits do
      post = create(:question, user: @user, group: @group)
      comment = create(:answer, user: @user, commentable: post)
      #One stream from question in set up block

      stream = Stream.find_by(item: post)
      assert_equal stream.user, @user
      assert_equal stream.group, @group
      assert_equal stream.item, post

      stream = Stream.find_by(item: comment)
      assert_equal stream.user, @user
      assert_equal stream.group, @group
      assert_equal stream.item, comment

      # deleting the post should delete comment, thus deleting both stream items
      assert_difference -> {Stream.count}, -2 do
        post.destroy
      end
    end
  end

  test 'should not create a new activity stream if comment is anonymous' do
    stub_request(:post, %r{http://localhost:9200/_bulk})
    post = create(:question, user: @user, group: @group, anonymous: true)
    assert_no_difference -> {Stream.count} do
      comment = create(:answer, user: @user, commentable: post, anonymous: true)
      comment.run_callbacks(:commit)
    end
  end
end

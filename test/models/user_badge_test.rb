require 'test_helper'

class UserBadgeTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  setup do
    # mocha requires a a record in db to be successfully able to run validations
    # if not found it will try to create its own and bad things happen.
    # https://github.com/thoughtbot/shoulda-matchers/issues/336
    @org = create(:organization)
    @badge = create(:badge, organization: @org)
    @user = create(:user, organization: @org)
    stub_request(:post, "http://localhost:9200/_bulk")
  end

  should have_db_column(:badging_id).of_type(:integer)
  should have_db_column(:user_id).of_type(:integer)

  should belong_to(:card_badging)
  should belong_to(:user)

  should validate_presence_of(:badging_id)
  should validate_presence_of(:user_id)

  test 'should create new notification for new user badge for clc' do
    NotificationGeneratorJob.expects(:perform_later).returns(nil).once

    clc_badge = create(:clc_badge, organization: @org)
    clc = create(:clc, name: 'test clc', entity: @org, organization: @org,
                 clc_badgings_attributes: [{badge_id: clc_badge.id,
                                            title: "clc-25",
                                            target_steps:25}])
    clc_badging = clc.clc_badgings.first
    user_badges = create(:user_badge, user_id: @user.id, badging_id: clc_badging.id)

  end

  test 'should not create new notification for updation of user badge' do
    NotificationGeneratorJob.expects(:perform_later).returns(nil).once
    clc_badge = create(:clc_badge, organization: @org)
    clc = create(:clc, name: 'test clc', entity: @org, organization: @org,
                 clc_badgings_attributes: [{badge_id: clc_badge.id,
                                            title: "clc-25",
                                            target_steps:25}])
    clc_badging = clc.clc_badgings.first
    user_badge = create(:user_badge, user_id: @user.id, badging_id: clc_badging.id )
    user_badge.update(updated_at: Time.zone.now)
  end

  class UserBadgeRecorderTest < ActiveSupport::TestCase
    test "should push influx user_badge_completed event when user badge is created" do
      TestAfterCommit.enabled = true
      org = create :organization
      user = create :user, organization: org
      badge = create(:badge, organization: org, type: "CardBadge")
      pathway = create(:card, organization: org, card_type: "pack", author: user)
      badging = Badging.create(badge_id: badge.id, badgeable_id: pathway.id, title: "titleee")
      stub_time = Time.now
      UserBadge.any_instance.stubs(:generate_identifier).returns("3UbMHIBg")
      user_badge  = UserBadge.new(id: 1000000001, user: user, badging_id: badging.id, created_at: stub_time, updated_at: stub_time, identifier: "3UbMHIBg")
      
      metadata = { user_id: user.id }    
      
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries(
          recorder: "Analytics::UserBadgeRecorder",
          event: "user_badge_completed",
          actor: Analytics::MetricsRecorder.user_attributes(user),
          org: Analytics::MetricsRecorder.org_attributes(org),
          timestamp: stub_time.to_i,
          user_badge: Analytics::MetricsRecorder.user_badge_attributes(user_badge),
          additional_data: metadata
          )
        )
      Timecop.freeze(stub_time) do
        user_badge.save
      end    
    end
  end
end
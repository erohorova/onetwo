require 'test_helper'

class InterestTest < ActiveSupport::TestCase

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
  end

  test 'get suggested channels' do
    ruby, reactjs, nodejs, elixir = create_list(:interest, 4)
    org1, org2 = create_list(:organization, 2)

    # first one is private, second one does not have the ruby or elixir tags
    org1_channels_non_relevant = create_list(:channel, 2, organization: org1)
    org1_channels_non_relevant.first.update_columns(is_private: true)
    org1_channels_non_relevant.first.interests << [ruby, elixir]
    org1_channels_non_relevant.last.interests << [nodejs, reactjs]

    org1_channels_relevant = create_list(:channel, 2, organization: org1, is_private: false)
    org1_channels_relevant.first.interests << [ruby, nodejs]
    org1_channels_relevant.last.interests << [ruby, elixir]

    org2_channel_relevant = create(:channel, organization: org2, is_private: false)
    org2_channel_relevant.interests << [ruby, elixir]

    org1_user = create(:user, organization: org1)
    org2_user = create(:user, organization: org2)

    already_following = create(:channel, organization: org1, is_private: false)
    already_following.interests << [ruby]
    create(:follow, followable: already_following, user: org1_user)

    suggested_channels = Interest.suggested_channels_based_on_interests_for(user: org1_user, interests: [ruby, elixir])
    assert_same_ids org1_channels_relevant, suggested_channels

    assert_empty Interest.suggested_channels_based_on_interests_for(user: org2_user, interests: [reactjs])
  end

  test 'get suggested users' do
    org1 = create(:organization)
    ruby, reactjs, nodejs, elixir = create_list(:interest, 4)
    users_with_similar_interests = create_list(:user, 2, organization: org1)
    users_with_similar_interests.first.interests << [ruby, nodejs]
    users_with_similar_interests.last.interests << [elixir, ruby]

    relevant_channels = create_list(:channel, 2, organization: org1, is_private: false)
    relevant_channels.first.interests << [ruby, nodejs]
    relevant_channels.last.interests << [elixir, ruby]
    channel_authors = create_list(:user, 2, organization: org1)
    relevant_channels.first.authors << channel_authors.first
    relevant_channels.last.authors << channel_authors.last

    distraction = create(:user, organization: org1)
    distraction.interests << [nodejs, reactjs]

    org2 = create(:organization)
    user_with_similar_interests_in_another_org = create(:user, organization: org2)
    user_with_similar_interests_in_another_org.interests << [elixir, ruby]

    org1_user = create(:user, organization: org1)
    org1_user.interests << [ruby, elixir]

    already_following = create(:user, organization: org1)
    already_following.interests << [ruby]
    create(:follow, followable: already_following, user: org1_user)

    suggested_users = Interest.suggested_users_based_on_interests_for(user: org1_user, interests: [ruby, elixir])
    assert_same_ids((users_with_similar_interests | channel_authors), suggested_users, ordered: false)
  end
end

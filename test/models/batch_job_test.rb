require 'test_helper'

class BatchJobTest < ActiveSupport::TestCase

  should validate_presence_of(:name)
  should validate_uniqueness_of(:name)
  should validate_presence_of(:last_run_time)
end

require 'test_helper'

class IrisVideStreamTest < ActiveSupport::TestCase
  setup do
    stub_request(:put, %r{http://localhost:9200/video_streams_test.*})
    stub_request(:get, %r{https://api.bambuser.com/group/media.json.*}).
        to_return(:status => 200, :body => %q{{"result":{"results":[{"gid":"32820","created":"1463090266","order_id":"3104754","type":"broadcast","id":"f6f7a6c8-8c26-445b-a931-d473c2b510e1","payload":{"author":"","country":"","created":1463090266,"device_class":"phone","device_name":"LGLS770","height":540,"id":"f6f7a6c8-8c26-445b-a931-d473c2b510e1","length":97,"owner":{"name":"a361715c-4d80-d92a-a9e3-4c81e04882e3","avatar":{"small":{"filename":"https:\/\/static.bambuser.com\/r\/img\/avatars\/default_45.gif","size":45},"large":{"filename":"https:\/\/static.bambuser.com\/r\/img\/avatars\/default_140.gif","size":140}}},"preview":"https:\/\/cf-us-west-2-archive-bambuser.birdplaneapp.com\/archive\/us-west-2-m00\/a_0001\/f6f7a6c8-8c26-445b-a931-34c012b510e1.jpg?2","tags":[],"talkback_types":"any","title":"Checkout my live stream!","type":"archived","url":"https:\/\/cf-us-west-2-archive-bambuser.birdplaneapp.com\/archive\/us-west-2-m00\/a_0001\/f6f7a6c8-8c26-445b-a931-d473c2b510e1.flv","username":"a361715c-4d80-d92a-a9e3-4c81e04882e3","vid":"6256822","views_live":2,"views_total":2,"visibility":"private","width":960}},{"gid":"32820","created":"1463066334","order_id":"3103906","type":"broadcast","id":"7e477893-0260-4173-bce0-f1d50c4d690c","payload":{"author":"","country":"India","created":1463066334,"device_class":"phone","device_name":"Samsung Galaxy S III Neo","height":960,"id":"7e477893-0260-4173-bce0-f1d50c4d690c","lat":20.593683,"length":3,"lon":78.96287,"owner":{"name":"a361715c-4d80-d92a-a9e3-4c81e04882e3","avatar":{"small":{"filename":"https:\/\/static.bambuser.com\/r\/img\/avatars\/default_45.gif","size":45},"large":{"filename":"https:\/\/static.bambuser.com\/r\/img\/avatars\/default_140.gif","size":140}}},"position_accuracy":2054220,"position_type":"country","preview":"https:\/\/archive.bambuser.com\/m\/06\/a_0008\/7e477893-0260-4173-bce0-0a53554d690c.jpg?0","tags":[],"talkback_types":"any","title":"Checkout my live stream!","type":"archived","url":"https:\/\/archive.bambuser.com\/m\/06\/a_0008\/7e477893-0260-4173-bce0-f1d50c4d690c.flv","username":"a361715c-4d80-d92a-a9e3-4c81e04882e3","vid":"6256369","views_live":1,"views_total":2,"visibility":"private","width":540}}],"next":"eyJicm9hZGNhc3Rfb3JkZXJfaWRfYWZ0ZXIiOiIxNDYzMDY2MzM0XzMxMDM5MDYiLCJpbWFnZV9jcmVhdGVkX2FmdGVyIjoiMTQ2MzA2NjMzNCJ9"}}}, :headers => {})

    stub_request(:get, "https://cf-us-west-2-archive-bambuser.birdplaneapp.com/archive/us-west-2-m00/a_0001/f6f7a6c8-8c26-445b-a931-d473c2b510e1.flv").
        with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
        to_return(:status => 200, :body => "something", :headers => {})
    stub_request(:put, %r{https://edcast-staging-paperclip-store.s3.amazonaws.com/video_streams_test/.*/server/.*.flv}).
        with(:body => "something").
        to_return(:status => 200, :body => "", :headers => {})
  end
  test 'upload past recording to S3' do
    ActiveJob::Base.queue_adapter = :sidekiq
    video_stream = create(:iris_video_stream, :live)
    video_stream.update(status: 'past')
    configured_job = IrisVideoStreamServerUploadJob.set(wait: 15.seconds)
    IrisVideoStreamServerUploadJob.expects(:set).with(wait: 15.seconds).returns(configured_job)
    configured_job.unstub :perform_later
    configured_job.expects(:perform_later).with(video_stream_id: video_stream.id)
    video_stream.process_past_stream
    ActiveJob::Base.queue_adapter = :inline
  end
end

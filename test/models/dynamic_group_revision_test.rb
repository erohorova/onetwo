require 'test_helper'

class DynamicGroupRevisionTest < ActiveSupport::TestCase

  setup do
    TestAfterCommit.enabled = true
    @org = create(:organization)
    @team = create(:team, organization: @org, is_dynamic: true)
  end

  should belong_to(:organization)
  should have_one(:dynamic_group)

  test 'should properly handle related group after destroy dynamic-selection group' do
    dgr = DynamicGroupRevision.create(
      group_id: @team.id,
      organization_id: @org.id,
      uid: @team.name,
      external_id: 1
    )
    dgr.destroy

    refute DynamicGroupRevision.exists?(id: dgr.id)
    refute Team.exists?(id: @team.id)
  end

  test 'should properly handle related group after destroy workflow group' do
    dgr = DynamicGroupRevision.create(
      group_id: @team.id,
      organization_id: @org.id,
      uid: @team.name
    )
    dgr.destroy

    refute DynamicGroupRevision.exists?(id: dgr.id)
    @team.reload

    assert Team.exists?(id: @team.id)
    refute Team.find_by(id: @team.id).is_dynamic
  end
end

require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    @user = create(:user)
    @org = @user.organization
    @client = create(:client, organization: @org)
    @group = create(:resource_group, organization: @org, client: @client)
    @group.add_member @user
    @post = create(:post, user: @user, group: @group)
    stub_request(:get, "http://url.com").
          to_return(:status => 200,
                    :body => '<html>
                    <head>
                      <title>title</title>
                      <meta property="og:title" content="og:title">
                      <meta property="og:description" content="og:description">
                      <meta property="og:site_name" content="og:site_name">
                      <meta property="og:image" content="http://imageurl.com">
                      <meta property="og:type" content="article">
                    </head>
                    </html>',
                    :headers => {})
    stub_request(:get, 'http://comment.com')
  end

  test "should have author" do
    comment = create(:comment, user: @user, commentable: @post)
    assert comment.user
  end

  test "should have post" do
    comment = create(:comment, user: @user, commentable: @post)
    assert comment.commentable
  end

  test "should strip script tag and its contents before saving post object" do
     comment = create(:comment, user: @user, message: "<script>I text here should be gone</script><p>Hello</p>", commentable: @post)
     assert comment.message, "<p>Hello</p>"
   end

  test 'should increment question comment counts on creation' do
    q = create(:question, user: @user, group: @group)
    old = q.student_answer_count
    create(:answer, commentable: q, user: @user)
    new = q.reload.student_answer_count
    assert_equal 1, (new - old)

    faculty = create(:user, organization: @org)
    @group.add_admin faculty

    old = q.faculty_answer_count
    create(:answer, commentable: q, user: faculty)
    new = q.reload.faculty_answer_count
    assert_equal 1, (new - old)
  end

  test 'should increment announcement comment counts on creation' do
    faculty = create(:user, organization: @org)
    @group.add_admin faculty

    a = create(:announcement, user: faculty, group: @group)

    old = a.student_answer_count
    create(:comment, commentable: a, user: @user)
    new = a.reload.student_answer_count
    assert_equal 1, (new - old)


    old = a.faculty_answer_count
    create(:comment, commentable: a, user: faculty)
    new = a.reload.faculty_answer_count
    assert_equal 1, (new - old)
  end

  test "should not have non empty message" do
    comment = build(:comment, user: @user, commentable: @post)
    assert comment.save
    comment = build(:comment, user: @user, commentable: @post, message: '')
    assert_not comment.save
  end

  test "create resource on comment creation with link" do
    post = create(:post, group: @group, user: @user, message: 'message')
    assert_difference -> {Resource.count} do
      post = create(:comment, commentable: post, user: @user, message: 'http://comment.com')
    end
  end

  test "do not create resource on comment creation without link" do
    post = create(:post, group: @group, user: @user, message: 'message')
    assert_no_difference -> {Resource.count} do
      post = create(:comment, commentable: post, user: @user, message: 'message')
    end
  end

  test "populate resource fields correctly for comment with link" do
    post = create(:post, group: @group, user: @user, message: 'message')
    url = 'http://url.com'
    comment = create(:comment, commentable: post, user: @user, message: url)

    resource = comment.resource
    assert_not_nil resource

    assert_equal resource.url, url
    assert_equal resource.title, 'title'
    assert_equal resource.type, 'Article'

    image_url = 'http://imageurl.com'
    assert_equal image_url, resource.image_url

    assert_equal resource.site_name, 'og:site_name'
  end

  test "do not create duplicate resource for an existing url just link" do
    resource = create(:resource, url: 'http://url.com')
    assert_difference -> {Resource.count} do
      comment = create(:comment, commentable: @post, user: @user, message: resource.url)
      assert_not_nil comment.resource
    end
    r = Resource.last
    assert_equal resource.url, r.url
  end

  test "should create tags for comment" do
    message = '#tag1 some text #tag2'
    comment = create(:comment, commentable: @post, user: @user, message: message)
    assert_equal comment.tags.count, 2
    assert_equal comment.tags[0].name, 'tag1'
    assert_equal comment.tags[1].name, 'tag2'
  end

  test "should not create tags for comment without tags" do
    message = 'no tag here'
    comment = create(:comment, commentable: @post, user: @user, message: message)
    assert_equal comment.tags.count, 0
  end

  test "comment can have votes" do
    user2 = create(:user, organization: @org)
    @group.add_member user2
    comment = create(:comment, commentable: @post, user: user2)
    vote = create(:vote, votable: comment, voter: @user)
    assert_equal comment.upvoted_by.first.id, @user.id
    assert_equal comment.upvoted_by.count, 1
  end

  test "user can vote on comment only once" do
    user2 = create(:user, organization: @org)
    @group.add_member user2
    comment = create(:comment, user: user2, commentable: @post)
    assert build(:vote, votable: comment, voter: @user).save
    assert_not build(:vote, votable: comment, voter: @user).save
    assert_not build(:vote, votable: comment, voter: @user).save
  end

  test "user can vote on multiple comments" do
    user2 = create(:user, organization: @org)
    @group.add_member user2
    comment1 = create(:comment, commentable: @post, user: user2)
    comment2 = create(:comment, commentable: @post, user: user2)
    assert build(:vote, votable: comment1, voter: @user).save
    assert build(:vote, votable: comment2, voter: @user).save
    assert_not build(:vote, votable: comment2, voter: @user).save
  end

  test "multiple users can vote on same comments" do
    user2 = create(:user, organization: @org)
    user3 = create(:user, organization: @org)
    @group.add_member user2
    @group.add_member user3
    comment = create(:comment, user: user3, commentable: @post)
    assert build(:vote, votable: comment, voter: @user).save
    assert build(:vote, votable: comment, voter: user2).save
  end

  test "faculty approved comments" do
    faculty = create(:user, organization: @org)
    @group.add_admin faculty

    comment = create(:comment, commentable: @post, user: @user)
    create(:approval, approver: faculty, approvable: comment)
    assert comment.has_approval?

    # vote by a faculty
    comment_not_approved = create(:comment, commentable: @post,
                                  user: @user)
    create(:vote, voter: faculty, votable: comment_not_approved)
    assert_not comment_not_approved.has_approval?

    # no votes, no approvals
    comment_no_votes = create(:comment, commentable: @post, user: @user)
    assert_not comment_no_votes.has_approval?
  end

  test 'is staff comment' do
    faculty = create(:user, organization: @org)
    @group.add_admin faculty

    comment = create(:comment, commentable: @post, user: @user)
    assert_not comment.created_by_staff?

    comment = create(:comment, commentable: @post, user: faculty)
    assert comment.created_by_staff?
  end

  test 'report action on comment valid' do
    faculty = create(:user, organization: @org)
    @group.add_admin faculty
    student_comment = create(:comment, user: @user, commentable: @post)
    faculty_comment = create(:comment, user: faculty, commentable: @post)

    valid, error = student_comment.get_report_action_validity faculty
    assert valid
    assert_equal error, ''

    invalid, error = faculty_comment.get_report_action_validity @user
    assert_not invalid
    assert_equal error, 'You cannot report a faculty post'

    create(:report, :spam_report, reportable: student_comment, reporter: faculty)
    invalid, error = student_comment.get_report_action_validity faculty
    assert_not invalid
    assert_equal error, 'Reported'
  end

  test 'vote action on comment valid' do
    faculty = create(:user, organization: @org)
    @group.add_admin faculty

    student_comment = create(:comment, user: @user, commentable: @post)
    faculty_comment = create(:comment, user: faculty, commentable: @post)

    valid, error = student_comment.get_vote_action_validity faculty
    assert valid
    assert_equal error, ''

    invalid, error = faculty_comment.get_vote_action_validity faculty
    assert_not invalid
    assert_equal error, 'You cannot upvote your own post'

    create(:vote, votable: student_comment, voter: faculty)
    invalid, error = student_comment.get_vote_action_validity faculty
    assert_not invalid
    assert_equal error, 'You have already upvoted this post'
  end

  test 'should follow commentable on comment' do
    user2 = create(:user, organization: @org)
    @group.add_member user2
    assert_difference ->{ Follow.count } do
      create(:comment, commentable: @post, user: user2)
    end
    assert_includes @post.followers, @user
  end

  test 'is mobile should work' do
    post = create(:post, user: @user, group: @group)
    comment = create(:comment, commentable: post, user: @user)
    assert_not comment.is_mobile?

    mobile_comment = create(:comment, commentable: post, user: @user, is_mobile: true)
    assert mobile_comment.is_mobile?
  end

  test 'creator label should work' do
    post = create(:post, user: @user, group: @group)

    # not explicity set, should default to role name
    comment = create(:comment, commentable: post, user: @user)
    assert_equal 'member', comment.creator_label

    # explicitly set
    admin = create(:user, organization: @org)
    @group.add_user(user: admin, role: 'admin', label: 'assistant')
    admin_comment = create(:comment, user: admin, commentable: post)
    assert_equal 'assistant', admin_comment.creator_label
  end

  test 'commenting rights' do
    create_default_org
    assert build(:comment, user: @user, commentable: @post).save

    u = create(:user, organization: @org)
    assert_not build(:comment, user: u, commentable: @post).save

    event = create(:event, group: @group, user: @user)
    assert build(:comment, user: @user, commentable: event).save
    assert_not build(:comment, user: u, commentable: event).save

    # TODO: inlcude cards in this
  end

  test 'should associate_files on create' do
    ActivityNotificationJob.any_instance.stubs(:perform).returns(nil)
    faculty = create(:user, organization: @org)
    @group.add_admin faculty
    a = create(:announcement, user: faculty, group: @group)
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)
    fr2 = FileResource.create(attachment: pic.open)
    comment = create(:comment, commentable: a, user: @user, file_ids: [fr1.id, fr2.id])
    assert comment.file_resources.count, 2
  end

  test '#notify_followers if group.forum_notifications_enabled is default(true)' do
    ActivityNotificationJob.expects(:perform_later).once
    faculty = create(:user, organization: @org)
    @group.add_admin faculty
    a = create(:announcement, user: faculty, group: @group)
    #notify_followers will be called in after_create update_follows
    comment = create(:comment, commentable: a, user: @user)
  end

  test '#notify_followers skip email notifications if forum_notifications_disabled' do
    ActivityNotificationJob.expects(:perform_later).never
    faculty = create(:user, organization: @org)
    user = create(:user, organization: @org)
    group = create(:resource_group, forum_notifications_enabled: false)
    group.add_member user
    group.add_admin faculty
    a = create(:announcement, user: faculty, group: group)
    comment = create(:comment, commentable: a, user: user)
    assert_nil comment.commentable.notify_followers("test",comment.user.id, comment.id)
  end

  test 'should not associate_files on create if file_ids are blank' do
    faculty = create(:user, organization: @org)
    @group.add_admin faculty
    a = create(:announcement, user: faculty, group: @group)
    comment = create(:comment, commentable: a, user: @user)
    assert comment.file_resources.count, 0
  end

  test 'should associate_files on update' do
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)
    fr2 = FileResource.create(attachment: pic.open)
    faculty = create(:user, organization: @org)
    @group.add_admin faculty
    a = create(:announcement, user: faculty, group: @group)
    comment = create(:comment, commentable: a, user: @user)
    comment.update(file_ids: [fr1.id, fr2.id])
    assert comment.file_resources.count, 2

    comment.update(file_ids: [])
    assert comment.file_resources.count, 0
  end

  test 'video stream comment score update' do
    WebMock.disable!
    EdcastActiveJob.unstub :perform_later
    VideoStreamUpdateScoreJob.unstub :perform_later
    user = create(:user, organization: @org)
    video_stream = create(:wowza_video_stream)
    video_stream.card.reload
    assert_enqueued_with(job: VideoStreamUpdateScoreJob, args: [video_stream]) do
      comment = create(:comment, commentable: video_stream, user: user)
      comment.run_callbacks(:commit)
    end
    WebMock.enable!
  end

  test "#organization" do
    card = create(:card, organization: @org, author_id: nil)
    comment = create(:comment, commentable: card, user: create(:user, organization: @org))
    assert_equal comment.organization, card.organization
  end

  test "#add_mentions?" do
    card = create(:card, author_id: nil, organization: @org)
    comment = create(:comment, commentable: card, user: create(:user, organization: @org))
    assert comment.add_mentions?

    g, u = create_gu
    post = create(:post, group: g, user: u)
    comment1 = create(:comment, commentable: post, user: u)
    assert_equal comment1.add_mentions?, false
  end

  test "#add_mentions? for the organization" do
    org = @user.organization
    comment = create(:comment, commentable: org)
    assert comment.add_mentions?
  end

  test "#add_mentions? for the team" do
    team = create(:team)
    comment = create(:comment, commentable: team)
    assert comment.add_mentions?
  end

  test "#add_mentions? for the ActivityStream" do
    comment = build(:comment, commentable_type: 'ActivityStream', commentable_id: 1)
    assert comment.add_mentions?
  end

  class FullCommitTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false

    test 'should delete notifications on destroy' do
      g, u = create_gu
      post = create(:post, group: g, user: u)
      comment = create(:comment, commentable: post, user: u)
      n = Notification.create(notification_type: "mention_in_comment", user: create(:user, organization: u.organization), notifiable: comment)
      assert_difference ->{Notification.count}, -1 do
        comment.destroy
      end
      refute Notification.where(notifiable: comment).present?
    end

    test 'comment on a comment' do
      skip
      # EdcastActiveJob.unstub :perform_later
      # NotificationGeneratorJob.unstub :perform_later
      g, u = create_gu
      post = create(:post, group: g, user: u)
      comment = create(:comment, commentable: post, user: u)
      reply = build(:comment, commentable: comment, user: u)

      # Currently no consequence anywhere else in the system
      UserContentsQueue.expects(:enqueue_for_user).never
      assert_difference ->{Stream.count} do
        assert_no_difference [->{ActionMailer::Base.deliveries.count}, ->{Notification.count}] do
          assert reply.save
        end
      end

      assert_equal 1, comment.comments.count
      assert_equal reply, comment.comments.first
    end

    test 'integration test for mobile notification' do
      skip("INTERMITTENT FAILURE ON TRAVIS")
      NotificationGeneratorJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later

      @org = create(:organization)
      client = create(:client, organization: @org)
      u1 = create(:user, organization: @org)
      u2 = create(:user, organization: @org)
      g = create(:group, organization: @org, client: client)

      g.add_member(u1)
      g.add_member(u2)

      pcard = create(:card, author_id: u1.id, card_type: 'media', card_subtype: 'text')
      pcard.reload

      # Card.reindex
      # Card.searchkick_index.refresh
      Comment.any_instance.stubs(:record_card_comment_event).returns true

      PubnubNotifier.expects(:notify).returns(nil).once
      assert_difference ->{Notification.count} do
        c = create(:comment, user: u2, commentable: pcard)
      end
    end
  end

  class CardCommentMetricRecorderTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false
    setup do
      User.any_instance.stubs(:record_user_create_event).returns true
    end

    test 'should invoke comment metric recorder job #comment' do
      IndexJob.stubs(:perform_later).returns true
      Card.any_instance.stubs(:record_card_create_event).returns true

      org = create(:organization)
      author, commentor = create_list(:user, 2,  organization: org)
      RequestStore.stubs(:read).returns({user_id: commentor.id})
      card = create(:card, author: author)

      Analytics::MetricsRecorderJob.unstub :perform_later
      Analytics::MetricsRecorderJob.expects(:perform_later)
      .with(has_entry(event: 'card_comment_created')).once

      card.reload
      comment = create(:comment, commentable: card, user: commentor)
    end

    test 'should invoke comment metric recorder job #live comment' do
      IndexJob.stubs(:perform_later).returns true
      Card.any_instance.stubs(:record_card_create_event).returns true
      Card.any_instance.stubs(:update_video_stream_state).returns true

      org       = create(:organization)
      stub_time = Time.now
      author, commentor = create_list(:user, 2,  organization: org)
      metadata  = { user_id: commentor.id }    
      RequestStore.stubs(:read).returns(metadata)
      card      = create(:card, organization: org, card_type: 'video_stream', author: nil)
      comment   = Comment.new(
        id:           1000001, 
        commentable:  card, 
        user:         commentor,
        type:         'LiveComment', 
        message:      "Tesgin live comment",
        created_at:   stub_time,
        updated_at:   stub_time
      )
      Analytics::MetricsRecorderJob.unstub :perform_later
      Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries(
          recorder:             "Analytics::CardCommentRecorder",
          event:                "card_comment_created",
          actor:                Analytics::MetricsRecorder.user_attributes(commentor),
          org:                  Analytics::MetricsRecorder.org_attributes(org),
          comment:              Analytics::MetricsRecorder.live_comment_attributes(comment),
          timestamp:            stub_time.to_i, 
          additional_data:      metadata
        )
      )
      
      Timecop.freeze(stub_time) do
        comment.save!
      end
    end

    test 'should not invoke comment metric recorder job #answer comment' do
      IndexJob.stubs(:perform_later).returns true
      Card.any_instance.stubs(:record_card_create_event).returns true
      Card.any_instance.stubs(:update_video_stream_state).returns true

      org       = create(:organization)
      stub_time = Time.now
      creator, commentor = create_list(:user, 2,  organization: org)
      client    = create(:client, organization: org)
      group     = create(:resource_group, organization: org, client: client)
      group.add_member creator
      post      = create(:post, group: group, user: creator)
      metadata  = { user_id: commentor.id }    
      RequestStore.stubs(:read).returns(metadata)
      
      comment = Comment.new(
        id:           1000001, 
        commentable:  post, 
        user:         creator,
        type:         'Answer', 
        message:      "Tesgin live comment",
        created_at:   stub_time,
        updated_at:   stub_time
      )

      Analytics::MetricsRecorderJob.unstub :perform_later
      Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries(
          recorder:             "Analytics::CardCommentRecorder",
          event:                "card_comment_created"
          )
      ).never
      
      Timecop.freeze(stub_time) do
        comment.save!
      end
    end

    test 'should invoke card metric recorder job #uncomment' do
      RequestStore.unstub(:read)
      Organization.any_instance.stubs :create_admin_user
      IndexJob.stubs(:perform_later).returns true
      Card.any_instance.stubs(:record_card_create_event).returns true
      Comment.any_instance.stubs(:record_card_comment_event).returns true
      org = create(:organization)
      author, actor = create_list(:user, 2,  organization: org)
      card = create(:card, author: author)
      card.reload
      comment = create(:comment, commentable: card, user: actor)

      RequestStore.store[:request_metadata] = {
        platform: 'web', user_agent: 'Edcast iPhone', user_id: actor.id
      }

      Analytics::MetricsRecorderJob.unstub :perform_later

      Timecop.freeze(2.days.from_now) do
        Analytics::MetricsRecorderJob.expects(:perform_later).with({
          recorder: 'Analytics::CardCommentRecorder',
          comment: Analytics::MetricsRecorder.comment_attributes(comment),
          card: Analytics::MetricsRecorder.card_attributes(card),
          org: Analytics::MetricsRecorder.org_attributes(org),
          actor: Analytics::MetricsRecorder.user_attributes(actor),
          timestamp: Time.now.to_i,
          event: 'card_comment_deleted',
          additional_data: {
            platform: 'web',
            user_agent: 'Edcast iPhone',
            user_id: actor.id
          }
        }).returns true

        comment.destroy
      end
    end

    test 'should invoke card metric recorder job #uncomment#deleted_by_other_user' do
      IndexJob.stubs(:perform_later).returns true
      Organization.any_instance.stubs :create_admin_user
      Card.any_instance.stubs(:record_card_create_event).returns true
      Comment.any_instance.stubs(:record_card_comment_event).returns true
      RequestStore.unstub(:read)
      org = create(:organization)
      actor, commentor = create_list(:user, 2,  organization: org)
      card = create(:card, author: actor)
      card.reload
      comment = create(:comment, commentable: card, user: commentor)
      RequestStore.store[:request_metadata] = {
        platform: 'web',
        user_agent: 'Edcast iPhone',
        user_id: actor.id
      }

      Timecop.freeze(2.days.from_now) do
        Analytics::MetricsRecorderJob.expects(:perform_later).with({
          recorder: 'Analytics::CardCommentRecorder',
          comment: Analytics::MetricsRecorder.comment_attributes(comment),
          card: Analytics::MetricsRecorder.card_attributes(card),
          org: Analytics::MetricsRecorder.org_attributes(org),
          actor: Analytics::MetricsRecorder.user_attributes(actor),
          timestamp: Time.now.to_i,
          event: 'card_comment_deleted',
          additional_data: {
            platform: 'web',
            user_agent: 'Edcast iPhone',
            user_id: actor.id
          }
        }).returns true

        comment.destroy
      end
    end

  end
end

require 'test_helper'

class ClcProgressTest < ActiveSupport::TestCase
  should belong_to(:clc)
  should belong_to(:user)
  should validate_presence_of(:clc_score)
  # should validate_uniqueness_of(:user_id).scoped_to(:clc_id)

  test "clc score should be greater than zero" do
    org = create(:organization)
    clc = create(:clc, name: 'test clc', entity: org, organization: org)
    clc_progress = build(:clc_progress, clc_score: 1, clc: clc)
    assert clc_progress.valid?

    clc_progress1 = build(:clc_progress, clc_score: 0, clc: clc)
    refute clc_progress1.valid?
  end

  test "uniquemess validation user_id" do
    org = create(:organization)
    clc = create(:clc, name: 'test clc', entity: org, organization: org)
    user = create(:user, organization: org)
    clc_progress = create(:clc_progress, clc_score: 1, user_id: user.id, clc: clc)

    clc_progress1 = build(:clc_progress, clc_score: 1, user_id: user.id, clc: clc)
    refute clc_progress1.valid?

    assert_equal "User has already been taken", clc_progress1.full_errors
  end
end

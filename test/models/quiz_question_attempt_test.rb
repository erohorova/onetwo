require 'test_helper'

class QuizQuestionAttemptTest < ActiveSupport::TestCase

  setup do
    @org = create(:organization)
    @author = create(:user, organization: @org)
  end

  test 'should call stats recorder job' do
    Analytics::MetricsRecorderJob.stubs(:perform_later)
    poll = create(:card, card_type: 'poll', card_subtype: 'text', author: @author)
    user = create(:user, organization: @org)
    attempt = create(:quiz_question_attempt, user: user, quiz_question: poll, selections: [1])

    QuizQuestionStatsRecorderJob.expects(:perform_later).with({quiz_question_attempt_id: attempt.id}).returns(nil).once
    attempt.run_callbacks(:commit)
  end

  test "poll_answer?" do
    poll = create(:card, card_type: 'poll', card_subtype: 'text', author: @author)
    ans = build(:quiz_question_attempt, quiz_question: poll, selections: [1])
    assert ans.poll_answer?
  end

  test 'should generate notification' do
    Analytics::MetricsRecorderJob.stubs(:perform_later)
    NotificationGeneratorJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    u1 = create(:user, organization: @org)
    poll = create(:card, card_type: 'poll', card_subtype: 'text', author_id: u1.id)

    u2 = create(:user, organization: @org)
    QuizQuestionStatsRecorderJob.stubs(:perform_later).returns(nil)
    assert_difference ->{Notification.count} do
      qqa = create(:quiz_question_attempt, user: u2, quiz_question: poll, selections: [1])
      qqa.run_callbacks(:commit)
    end
    notif = Notification.last
    assert_equal "poll_answer", notif.notification_type
    assert_equal [u2], notif.causal_users
    assert_equal u1, notif.user
    assert_equal 'card', notif.app_deep_link_type
    assert_equal poll.id, notif.app_deep_link_id
  end

  test 'records response to influx' do
    org = @org
    poll = create(:card, card_type: 'poll', card_subtype: 'text', organization: org, author_id: nil)

    poll_opt = create(:quiz_question_option, quiz_question: poll)
    user = create(:user, organization: org)
    attempt = build(:quiz_question_attempt, user: user, quiz_question: poll, selections: [poll_opt.id])
    attempt.expects(:create_poll_or_quiz_response_influx_metric)
    attempt.run_callbacks(:commit) { attempt.save }
  end

  test '#create_poll_or_quiz_response_influx_metric' do
    stub_time = Time.now
    org = @org
    poll = create :card,
      card_type: 'poll',
      card_subtype: 'text',
      organization: org,
      author: create(:user, organization: org)
    poll_opt = create(:quiz_question_option, quiz_question: poll)
    user = create(:user, organization: @org)
    metadata = { foo: "bar" }
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    attempt = build(
      :quiz_question_attempt,
      user: user,
      quiz_question: poll,
      selections: [poll_opt.id]
    )
    # This recorder uses QuizQuestionAttempt#user as the actor
    # instead of looking them up through RequestStore
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::PollResponseMetricsRecorder",
      timestamp: stub_time.to_i,
      actor: Analytics::MetricsRecorder.user_attributes(user),
      org: Analytics::MetricsRecorder.org_attributes(org),
      card: Analytics::MetricsRecorder.card_attributes(poll),
      quiz_question_options: [
        Analytics::MetricsRecorder.quiz_question_option_attributes(poll_opt)
      ],
      additional_data: metadata
    )
    Timecop.freeze stub_time do
      attempt.create_poll_or_quiz_response_influx_metric
    end
  end

end

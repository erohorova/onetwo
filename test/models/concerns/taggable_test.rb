require 'test_helper'

class TaggableTest < ActiveSupport::TestCase

  test 'update_topics: should add tags for card' do
    card = create :card
    tag = create :tag, name: "tag_1"
    card.tags << tag

    new_tag = create :tag, name: "tag_2"
    card.topics = "tag_1,tag_2"

    assert_difference -> { card.tags.count }, 1 do
      card.update_topics
    end

    assert_same_elements card.tags.pluck(:name), [tag.name, new_tag.name]
  end
end

require 'test_helper'

class UserRollableTest < ActiveSupport::TestCase

  test 'should add user role' do
    User.any_instance.unstub(:update_role)
    org = create(:organization)
    Role.create_standard_roles org

    admin_role = org.roles.find_by_name('admin')
    member_role = org.roles.find_by_name('member')

    user = create(:user, organization: org)
    user.add_role("admin")
    assert_equal [admin_role, member_role], user.reload.roles
  end

  test 'should remove curator role' do
    org = create(:organization)
    Role.create_standard_roles org

    curator_role = org.roles.find_by_name('curator')

    user = create(:user, organization: org)
    user.add_role('curator')

    user.remove_curator_role('curator')
    assert_equal false, user.reload.user_roles.map(&:role_id).include?(curator_role.id)
  end

  test 'should remove collaborator role' do
    org = create(:organization)
    Role.create_standard_roles org

    collaborator_role = org.roles.find_by_name('collaborator')

    user = create(:user, organization: org)
    user.add_role('collaborator')

    user.remove_collaborator_role('collaborator')
    assert_equal false, user.reload.user_roles.map(&:role_id).include?(collaborator_role.id)
  end

  test "should check organization role exist or not" do
    role = create(:role, name: "customer")
    user = create(:user, organization: role.organization)
    assert user.organization_roles_exist?(role)
    assert user.organization_roles_exist?("customer")
    assert_equal false,user.organization_roles_exist?("employee")
  end

  test "should check user role exist for app" do
    role = create(:role, name: "customer")
    role1 = create(:role, name: "employee", organization: role.organization)
    user = create(:user, organization: role.organization)
    assert_equal false,user.has_any_user_roles?(role)
    user.add_role(role)

    assert user.has_any_user_roles?(role)
    assert_equal false,user.has_any_user_roles?(role1)
    assert_equal false,user.has_any_user_roles?("employee")
  end

  test "should get user permissions rule name" do
    Role.any_instance.unstub(:create_role_permissions)
    User.any_instance.unstub(:update_role)
    org = create(:organization)
    Role.create_standard_roles org
    admin_role = org.roles.find_by_name('admin')
    member_role = org.roles.find_by_name('member')

    create(:role_permission, role: admin_role, name: Permissions::SHOW_CHANNEL_MANAGER)
    create(:role_permission, role: admin_role, name: Permissions::HAS_CMS_ACCESS)
    create(:role_permission, role: member_role, name: Permissions::HAS_CMS_ACCESS)

    user = create(:user, organization: org, organization_role: 'admin')
    user1 = create(:user, organization: org)

    assert_includes user.get_user_permissions, Permissions::SHOW_CHANNEL_MANAGER
    assert_includes user.get_user_permissions, Permissions::HAS_CMS_ACCESS
    assert_includes user1.get_user_permissions, Permissions::HAS_CMS_ACCESS
  end

  test "should check user is auhorized for permission name" do
    Role.any_instance.unstub(:create_role_permissions)
    User.any_instance.unstub(:update_role)

    org = create(:organization)

    Role.create_standard_roles org

    admin_role = org.roles.find_by_name('admin')
    member_role = org.roles.find_by_name('member')

    create(:role_permission, role: admin_role, name: Permissions::SHOW_CHANNEL_MANAGER)
    create(:role_permission, role: admin_role, name: Permissions::HAS_CMS_ACCESS)
    create(:role_permission, role: member_role, name: Permissions::HAS_CMS_ACCESS)

    user = create(:user, organization: org)
    user1 = create(:user, organization: org)
    user.add_role("admin")

    assert user.authorize?(Permissions::SHOW_CHANNEL_MANAGER)
    assert_equal false, user1.authorize?(Permissions::SHOW_CHANNEL_MANAGER)
  end

  test "should get user permissions rule name `VIEW_CARD_ANALYTICS`" do
    org = create(:organization)
    create_org_master_roles org
    admin_role = org.roles.find_by_name('admin')
    user = create(:user, organization: admin_role.organization)
    user.add_role("admin")
    assert_includes user.get_user_permissions, Permissions::VIEW_CARD_ANALYTICS
  end

  test "should get user permissions rule name 'UPLOAD'" do
    Role.any_instance.unstub(:create_role_permissions)
    org = create(:organization, enable_role_based_authorization: true)
    Role.create_master_roles org

    admin_role = org.roles.find_by_name('admin')
    user = create(:user, organization: admin_role.organization)
    user.add_role("admin")
    assert_includes user.get_user_permissions, Permissions::UPLOAD
  end

  test 'rbac should return user permissions in an org with role authorization enabled' do
    Role.any_instance.unstub(:create_role_permissions)
    org = create(:organization, enable_role_based_authorization: true)
    Role.create_standard_roles org

    user = create(:user, organization: org)
    user.add_role('admin')

    assert_same_elements Permissions.get_permissions_for_role('admin').keys, user.get_all_user_permissions

    user.add_role('curator')

    expected_permissions = (
      Permissions.get_permissions_for_role('admin').keys +
      Permissions.get_permissions_for_role('curator').keys
    ).uniq

    assert_same_elements expected_permissions, user.get_all_user_permissions
  end

  test 'rbac should return user permissions in an org' do
    Role.any_instance.unstub(:create_role_permissions)
    org = create(:organization)
    Role.create_standard_roles org
    User.any_instance.unstub(:update_role)

    user = create(:user, organization: org, organization_role: 'admin')
    assert_same_elements Permissions.get_permissions_for_role('admin').keys, user.get_all_user_permissions
  end

  test 'rbac should return user permissions in an org with disabled role authorization who is a curator' do
    org = create(:organization)

    user = create(:user, organization: org)
    channel = create(:channel, organization: org)
    channels_curator = ChannelsCurator.create(user: user, channel: channel)

    assert_includes user.get_all_user_permissions, Permissions::CURATE_CONTENT
  end

  test "#exclude_group_permission? for sub_admin" do
    Role.any_instance.unstub(:create_role_permissions)
    User.any_instance.unstub(:update_role)

    org = create(:organization)
    team = create(:team, organization: org)
    Role.create_standard_roles org

    member_role = org.roles.find_by_name('member')

    create(:role_permission, role: member_role, name: Permissions::MANAGE_ANALYTICS)
    create(:role_permission, role: member_role, name: Permissions::MANAGE_CONTENT)

    user = create(:user, organization: org)
    user.add_role("member")
    team.add_sub_admin(user)

    assert user.is_sub_admin?
    refute user.exclude_group_permission?(Permissions::MANAGE_GROUP_CONTENT)
    refute user.exclude_group_permission?(Permissions::MANAGE_GROUP_ANALYTICS)
    refute user.exclude_group_permission?(Permissions::MANAGE_GROUP_USERS)
  end

  test "#exclude_group_permission? for group_sub_admin" do
    Role.any_instance.unstub(:create_role_permissions)
    User.any_instance.unstub(:update_role)

    org = create(:organization)
    team = create(:team, organization: org)
    Role.create_standard_roles org

    member_role = org.roles.find_by_name('member')

    create(:role_permission, role: member_role, name: Permissions::MANAGE_GROUP_CONTENT)

    user = create(:user, organization: org)
    user.add_role("member")
    team.add_sub_admin(user)

    assert user.is_group_sub_admin?
    refute user.exclude_group_permission?(Permissions::MANAGE_GROUP_CONTENT)
    assert user.exclude_group_permission?(Permissions::MANAGE_GROUP_USERS)
  end
end

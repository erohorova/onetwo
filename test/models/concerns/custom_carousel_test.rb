require 'test_helper'

class CustomCarouselTest < ActiveSupport::TestCase
  test 'expect `reorder_and_destroy_call` on destroy of a entity(card, user, channel)' do    
    @org = create(:organization)
    @u = create(:user, organization: @org)
    card = create(:card, card_type: 'media', card_subtype: 'text', author_id: @u.id, title: 'This is private card', message: 'This is message',  is_public: true, organization_id: @org.id)
    structure = create(:structure, parent: @org, current_context: 'discover', current_slug: 'cards', creator: @u, display_name: 'Test')
    structure_item = create(:structured_item, structure: structure, entity: card)
    StructuredItem.expects(:reorder_and_destroy).with([structure_item]).once    
    card.destroy
  end

  test 'expect `reorder_and_destroy_call` on destroy of a entity(card, user, channel, channel_cards)' do    
    organization = create(:organization)
    user = create(:user, organization: organization)
    card = create(:card, card_type: 'media', card_subtype: 'text', author_id: user.id, title: 'This is private card', message: 'This is message',  is_public: true, organization_id: organization.id)
    channel = create(:channel, organization: organization, is_open: true)
    channel_card = create(:channels_card, channel_id: channel.id, card_id: card.id)    
    structure = create(:structure, creator_id: user.id, organization_id: organization.id, parent_id: channel.id, parent_type: 'Channel', slug: 'channel-cards')
    structure_item = create(:structured_item, structure: structure, entity: card)

    channel_card.expects(:remove_channels_cards_from_custom_carousel)

    channel_card.destroy
  end
end
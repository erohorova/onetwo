require 'test_helper'

class ManageableTest < ActiveSupport::TestCase

  setup do
    @org = create(:organization)
    @user, @author = create_list(:user, 2, organization: @org)
    @team = create(:team, organization: @org)
    @team.add_admin(@user)
    @public_card = create(:card, author: @author, organization: @org)
    @user_card = create(:card, author: @user, organization: @org)
    @cover_pathway = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user, organization: @org)
  end

  class CreateManagementMethodTest < ManageableTest

    test '#create_management should invoke UsersCardsManagementJob for CPR' do
      values = [@user.id, @public_card.id, 'added_to_pathway', @cover_pathway.id]
      UsersCardsManagementJob.expects(:perform_later).with(*values).once
      CardPackRelation.new(cover_id: @cover_pathway.id, from_id: @public_card.id, from_type: 'Card').create_management
    end

    test '#create_management should invoke UsersCardsManagementJob for TeamsCard' do
      values = [@user.id, @public_card.id, 'shared_to_team', @team.id]
      UsersCardsManagementJob.expects(:perform_later).with(*values).once
      TeamsCard.new(team_id: @team.id, card_id: @public_card.id, type: 'SharedCard', user_id: @user.id).create_management
    end

    test '#create_management should not invoke UsersCardsManagementJob if values content any nil value' do
      values = [@user.id, @public_card.id, 'shared_to_team', nil]
      UsersCardsManagementJob.expects(:perform_later).with(*values).never
      TeamsCard.new(team_id: nil, card_id: @public_card.id, type: 'SharedCard', user_id: @user.id).create_management
    end
  end

  class DestroyManagementMethodTest < ManageableTest

    test '#destroy_management should invoke UsersCardsManagementJob for CPR' do
      values = [@user.id, @public_card.id, 'added_to_pathway', @cover_pathway.id]
      cpr = CardPackRelation.create(cover_id: @cover_pathway.id, from_id: @public_card.id, from_type: 'Card')
      UsersCardsManagement.create(user_id: @user.id, card_id: @public_card.id, action: 'added_to_pathway', target_id: @cover_pathway.id)
      UsersCardsManagementJob.expects(:perform_later).with(*values, opts: 'destroy').once
      cpr.destroy
      refute cpr.persisted?
    end
  end
end
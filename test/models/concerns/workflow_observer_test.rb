require 'test_helper'

class WorkflowObserverTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper
  setup do
    @org = create :organization
    @config =  @org.configs.create(name: 'simplified_workflow', value: false, data_type: 'boolean')
  end

  test 'old workflow should be triggered when config is false' do
    @org.configs.create(name: 'workflow_service', value: true, data_type: 'boolean')
    EdcastActiveJob.unstub(:perform_later)
    WorkflowPatchSaverJob.unstub(:perform_later)
    assert_no_enqueued_jobs
    refute @org.get_settings_for('simplified_workflow')
    assert_enqueued_with(job: WorkflowPatchSaverJob) do
      Profile.create(
        user_id: create(:user, organization: @org).id,
        organization_id: @org.id
      ).committed!
    end
  end

  test 'old workflow should not be triggered when config is true' do
    EdcastActiveJob.unstub(:perform_later)
    WorkflowPatchSaverJob.unstub(:perform_later)
    assert_no_enqueued_jobs
    @config.value = true
    @config.save!
    assert @org.get_settings_for('simplified_workflow')
    user = create :user, organization: @org
    user.create_external_profile
    user.send :detect_upsert
    assert_no_enqueued_jobs
  end


  test 'should ensure WorkflowObserverJob is trigger on user_custom_field creation and updation' do
    EdcastActiveJob.unstub(:perform_later)
    WorkflowObserverJob.unstub(:perform_later)
    @config.value = true
    @config.save!
    assert @org.simplified_workflow_enabled?
    custom_field = create :custom_field, organization: @org, abbreviation: 'foo', display_name: 'foo'
    assert_no_enqueued_jobs
    assert_enqueued_with(job: WorkflowObserverJob) do
      TestAfterCommit.with_commits(true) do
        user_custom_field = UserCustomField.create(
          custom_field: custom_field,
          user: create(:user, organization: @org),
          value: 'bar'
        )
        user_custom_field.update_attributes!(value: 'bar!')
      end
    end
  end

  test 'should ensure WorkflowObserverJob is trigger on user creation and updation' do
    EdcastActiveJob.unstub(:perform_later)
    WorkflowObserverJob.unstub(:perform_later)
    @config.value = true
    @config.save!
    assert @org.simplified_workflow_enabled?
    assert_no_enqueued_jobs
    assert_enqueued_with(job: WorkflowObserverJob) do
      TestAfterCommit.with_commits(true) do
        user = User.create!(first_name: 'A user', password: 'Abcd@1234', organization: @org)
        user.update_attributes!(first_name: 'The user')
      end
    end
  end
end

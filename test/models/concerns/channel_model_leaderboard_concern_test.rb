require 'test_helper'

# Note: ChannelModelLeaderboardConcern is included into channel
class ChannelModelLeaderboardConcernTest < ActiveSupport::TestCase

  setup do
    time = Time.now - 1.day
    @start_time_base = time.beginning_of_day - 1.day
    @end_time_base = time.end_of_day - 1.day

    @org = create :organization
    @user = create :user, organization: @org, bio: "bio1"
    @user2 = create :user, organization: @org, bio: "bio2"
    @user3 = create :user, organization: @org, bio: "bio3"

    @channel_1 = create :channel, organization: @org
    @channel_2 = create :channel, organization: @org

    # Create a bunch of cards by @user
    # Set message for some, title for others
    @channel_1_cards = 4.times.map do |idx|
      title_and_message_attrs = if idx % 2 == 0
        { title: "", message: "channel_1_card_#{idx}_message" }
      else
        { message: "", title: "channel_1_card_#{idx}_title" }
      end
      create :card, {
        organization_id: @org.id,
        author: @user,
        title: "channel_1_card_#{idx}"
      }.merge(title_and_message_attrs)
    end
    # Create more cards by the same user, in a different channel.
    # Set message for some, title for others
    @channel_2_cards = 4.times.map do |idx|
      title_and_message_attrs = if idx % 2 == 0
        { title: "", message: "channel_1_card_#{idx}_message" }
      else
        { message: "", title: "channel_1_card_#{idx}_title" }
      end
      create :card, {
        organization_id: @org.id,
        author: @user,
        title: "channel_2_card_#{idx}"
      }.merge(title_and_message_attrs)
    end
    # add the cards to channels
    @channel_1_cards.each.with_index do |card, num_days_ago|
      ChannelsCard.create(
        card: card,
        channel: @channel_1,
        created_at: @start_time_base - num_days_ago.days
      )
    end
    @channel_2_cards.each.with_index do |card, num_days_ago|
      ChannelsCard.create(
        card: card,
        channel: @channel_2,
        created_at: @start_time_base - num_days_ago.days
      )
    end

    # Create cards for other two users, and add them to channel
    [@user2, @user3].each.with_index do |user, idx|
      # @user2 gets 1 cards added, @user3 gets 2
      (idx + 1).times do
        card = create(:card,
          organization_id: @org.id,
          author: user,
          title: "user_#{user.id}_card"
        )
        ChannelsCard.create(
          card: card,
          channel: @channel_1,
          created_at: @start_time_base - 1.days
        )
      end
    end

    # create yet another user, and add their cards to a different channel
    # This user's data won't be returned by any of the tests.
    ChannelsCard.create(
      card: create(:card,
        organization: @org,
        author: create(:user, organization: @org),
        title: "other_card"
      ),
      channel: @channel_2,
      created_at: @start_time_base - 1.days
    )

    # Create CardMetricsAggregation records for all the cards by @user
    # 2 channels
    # 4 cards per channel
    # 3 records per card (1 for each of 3 days)
    # Records for each card all have the same scores.
    [@channel_1_cards, @channel_2_cards].each do |cards|
      cards.each.with_index do |card, card_idx|
        3.times do |num_days_ago|
          CardMetricsAggregation.create!(
            organization_id: @org.id,
            card_id:         card.id,
            start_time:      (@start_time_base - num_days_ago.days).to_i,
            end_time:        (@end_time_base - num_days_ago.days).to_i,
            num_likes:       card_idx + 1,
            num_views:       card_idx + 2,
            num_completions: card_idx + 3,
            num_bookmarks:   card_idx + 4,
            num_comments:    card_idx + 5,
            num_assignments: card_idx + 6,
          )
        end
      end
    end

    # Make UserMetricsAggregation records for each of the users.
    # Only the total_user_score is important to be set.
    [@user, @user2, @user3].each.with_index do |user, idx|
      3.times do |num_days_ago|
        UserMetricsAggregation.create(
          organization_id:            @org.id,
          user_id:                    user.id,
          start_time:                 (@start_time_base - num_days_ago.days).to_i,
          end_time:                   (@end_time_base - num_days_ago.days).to_i,
          smartbites_commented_count: 0,
          smartbites_completed_count: 0,
          smartbites_consumed_count:  0,
          smartbites_created_count:   0,
          smartbites_liked_count:     0,
          time_spent_minutes:         0,
          total_smartbite_score:      0,
          total_user_score:           idx + 1
        )
      end
    end
    # Add the first 2 users as followers of the first channel
    @channel_1.follows.create user: @user
    @channel_1.follows.create user: @user2

    @default_photo_url = "http://cdn-test-host.com/assets/new-anonymous-user-small.jpeg"
  end

  # =========================================
  # TOP CONTRIBUTORS
  # =========================================

  test 'get_top_contributors with start_date and end_date' do
    actual = @channel_1.get_top_contributors(
      limit: 50,
      offset: 0,
      start_date: @start_time_base - 2.days,
      end_date: @end_time_base
    )
    expected = [
      {
         "first_name" => @user.first_name,
         "last_name"  => @user.last_name,
         "user_id"    => @user.id,
         "bio"        => @user.bio,
         "photo"      => @default_photo_url,
         "num_cards"  => 3 # Only 3/4 of this user's cards
                           # fall within the time bounds
      },
      {
         "first_name" => @user3.first_name,
         "last_name"  => @user3.last_name,
         "user_id"    => @user3.id,
         "bio"        => @user3.bio,
         "photo"      => @default_photo_url,
         "num_cards"  => 2
      },
      {
         "first_name" => @user2.first_name,
         "last_name"  => @user2.last_name,
         "user_id"    => @user2.id,
         "bio"        => @user2.bio,
         "photo"      => @default_photo_url,
         "num_cards"  => 1
      }
    ]
    assert_equal expected, actual
  end

  test 'get_top_contributors doesnt count cards that are deleted' do
    # Delete all the cards by @user
    @user.cards.delete_all

    actual = @channel_1.get_top_contributors(
      limit: 50,
      offset: 0,
      start_date: @start_time_base - 2.days,
      end_date: @end_time_base
    )
    expected = [
      {
         "first_name" => @user3.first_name,
         "last_name"  => @user3.last_name,
         "user_id"    => @user3.id,
         "bio"        => @user3.bio,
         "photo"      => @default_photo_url,
         "num_cards"  => 2
      },
      {
         "first_name" => @user2.first_name,
         "last_name"  => @user2.last_name,
         "user_id"    => @user2.id,
         "bio"        => @user2.bio,
         "photo"      => @default_photo_url,
         "num_cards"  => 1
      }
    ]
    assert_equal expected, actual
  end

  test 'get_top_contributors with offset and limit' do
    # Same query as the previous test case, but with offset/limit set
    actual = @channel_1.get_top_contributors(
      limit:      1,
      offset:     1,
      start_date: @start_time_base - 2.days,
      end_date:   @end_time_base
    )
    expected = [{
      "first_name" => @user3.first_name,
      "last_name"  => @user3.last_name,
      "user_id"    => @user3.id,
      "bio"        => @user3.bio,
      "photo"      => @default_photo_url,
      "num_cards"  => 2 # Only 2/3 cards are included by this date range
    }]
    assert_equal expected, actual
  end

  # =========================================
  # TOP CARDS
  # =========================================

  test 'get_top_cards_data with start_date and end_date' do
    # We request 2 days worth of data for the channel,
    # passing a generous limit to include all available cards' data.
    actual = @channel_1.get_top_cards_data(
      limit:      50,
      offset:     0,
      start_date: (@start_time_base - 1.days).to_i,
      end_date:   @end_time_base.to_i
    )
    expected = [
      {
        "title"               => "channel1card3title",
        "organization_id"     => @org.id,
        "card_id"             => @channel_1_cards[3].id,
        "sum_num_likes"       => 8,
        "sum_num_views"       => 10,
        "sum_num_completions" => 12,
        "sum_num_bookmarks"   => 14,
        "sum_num_comments"    => 16,
        "sum_num_assignments" => 18,
        "sort_score"          => 78
      },
      {
        "title"               => "channel1card2message",
        "organization_id"     => @org.id,
        "card_id"             => @channel_1_cards[2].id,
        "sum_num_likes"       => 6,
        "sum_num_views"       => 8,
        "sum_num_completions" => 10,
        "sum_num_bookmarks"   => 12,
        "sum_num_comments"    => 14,
        "sum_num_assignments" => 16,
        "sort_score"          => 66
      },
      {
        "title"               => "channel1card1title",
        "organization_id"     => @org.id,
        "card_id"             => @channel_1_cards[1].id,
        "sum_num_likes"       => 4,
        "sum_num_views"       => 6,
        "sum_num_completions" => 8,
        "sum_num_bookmarks"   => 10,
        "sum_num_comments"    => 12,
        "sum_num_assignments" => 14,
        "sort_score"          => 54
      },
      {
        "title"               => "channel1card0message",
        "organization_id"     => @org.id,
        "card_id"             => @channel_1_cards[0].id,
        "sum_num_likes"       => 2,
        "sum_num_views"       => 4,
        "sum_num_completions" => 6,
        "sum_num_bookmarks"   => 8,
        "sum_num_comments"    => 10,
        "sum_num_assignments" => 12,
        "sort_score"          => 42
      },
    ]
    assert_equal expected, actual
  end

  test 'get_top_cards_data ignores deleted cards' do
    # Delete all the cards from @channel_1 except one
    @channel_1_cards[1..-1].each(&:delete)

    # We request 2 days worth of data for the channel,
    # passing a generous limit to include all available cards' data.
    actual = @channel_1.get_top_cards_data(
      limit:      50,
      offset:     0,
      start_date: (@start_time_base - 1.days).to_i,
      end_date:   @end_time_base.to_i
    )
    expected = [
      {
        "title"               => "channel1card0message",
        "organization_id"     => @org.id,
        "card_id"             => @channel_1_cards[0].id,
        "sum_num_likes"       => 2,
        "sum_num_views"       => 4,
        "sum_num_completions" => 6,
        "sum_num_bookmarks"   => 8,
        "sum_num_comments"    => 10,
        "sum_num_assignments" => 12,
        "sort_score"          => 42
      }
    ]
    assert_equal expected, actual
  end

  test 'top_cards_data with pagination' do
    # We do the same query as the previous test,
    # but give limit=1 and offset=1
    actual = @channel_1.get_top_cards_data(
      limit:      1,
      offset:     1,
      start_date: (@start_time_base - 1.days).to_i,
      end_date:   @end_time_base.to_i
    )
    expected = [
      {
        "title"               => "channel1card2message",
        "organization_id"     => @org.id,
        "card_id"             => @channel_1_cards[2].id,
        "sum_num_likes"       => 6,
        "sum_num_views"       => 8,
        "sum_num_completions" => 10,
        "sum_num_bookmarks"   => 12,
        "sum_num_comments"    => 14,
        "sum_num_assignments" => 16,
        "sort_score"          => 66
      }
    ]
    assert_equal expected, actual
  end

  # =========================================
  # TOP SCORING USERS
  # =========================================

  test 'get_top_scoring_users with start_date and end_date' do
    # We request 2 days worth of data for the channel,
    # passing a generous limit to include all available cards' data.
    actual = @channel_1.get_top_scoring_users(
      limit:      50,
      offset:     0,
      start_date: (@start_time_base - 1.days).to_i,
      end_date:   @end_time_base.to_i
    )
    expected = [
      {
        "total_user_score" => 4,
        "user_id"          => @user2.id,
        "first_name"       => @user2.first_name,
        "last_name"        => @user2.last_name,
        "bio"              => @user2.bio,
        "photo"            => @default_photo_url,
      },
      {
        "total_user_score" => 2,
        "user_id"          => @user.id,
        "first_name"       => @user.first_name,
        "last_name"        => @user.last_name,
        "bio"              => @user.bio,
        "photo"            => @default_photo_url,
      }
    ]
    assert_equal expected, actual
  end

  test 'get_top_scoring_users ignores deleted follows' do
    # Delete all the follows by @user
    @user.follows.delete_all

    # We request 2 days worth of data for the channel,
    # passing a generous limit to include all available users' data.
    actual = @channel_1.get_top_scoring_users(
      limit:      50,
      offset:     0,
      start_date: (@start_time_base - 1.days).to_i,
      end_date:   @end_time_base.to_i
    )
    expected = [
      {
        "total_user_score" => 4,
        "user_id"          => @user2.id,
        "first_name"       => @user2.first_name,
        "last_name"        => @user2.last_name,
        "bio"              => @user2.bio,
        "photo"            => @default_photo_url,
      }
    ]
    assert_equal expected, actual
  end

  test 'get_top_scoring_users with pagination' do
    # We do the same query as the previous test,
    # but give limit=1 and offset=1
    actual = @channel_1.get_top_scoring_users(
      limit:      1,
      offset:     1,
      start_date: (@start_time_base - 1.days).to_i,
      end_date:   @end_time_base.to_i
    )
    expected = [
      {
        "total_user_score" => 2,
        "user_id"          => @user.id,
        "first_name"       => @user.first_name,
        "last_name"        => @user.last_name,
        "bio"              => @user.bio,
        "photo"            => @default_photo_url
      }
    ]
    assert_equal expected, actual
  end

end

require 'test_helper'

class AssignableConcernTest < ActiveSupport::TestCase
  setup do
    @org = create(:organization)
    @assignor = create(:user, organization: @org)
  end

  test "#create_assignment should create individual assignment for given user" do
    assignee = create(:user, organization: @org)
    assignable = create(:card, organization: @org, author: @assignor)
    assert_difference ->{Assignment.count} do
      assignable.create_assignment(user: assignee, assignor: @assignor, assignable: assignable, opts: {})
    end
    assert_nil TeamAssignment.find_by(assignment_id: Assignment.last).team_id
  end

  test "#create_assignment should update existing assignments state" do
    assignee = create(:user, organization: @org)
    assignable = create(:card, organization: @org, author: @assignor)
    assignment = create(:assignment, assignable: assignable, user_id: assignee.id)
    assert_nil assignment.due_at
    
    assignable.create_assignment(user: assignee, assignor: @assignor, assignable: assignable, 
      opts: {due_at: (Time.now + 10.days).strftime("%m/%d/%Y")})
    
    assert_equal assignment.id, assignable.assignments.first.id
    
    assert_equal Date.strptime((Time.now + 10.days).strftime("%m/%d/%Y"), "%m/%d/%Y").end_of_day.to_i,
      assignable.assignments.first.due_at.to_i
  end

  test "#create_assignment should reassign dismissed assignment" do
    assignee = create(:user, organization: @org)
    assignable = create(:card, organization: @org, author: @assignor)
    
    assignment = create(:assignment, assignable: assignable, user_id: assignee.id, state: Assignment::DISMISSED)
    assert_equal Assignment::DISMISSED, assignment.state
    
    assignable.create_assignment(user: assignee, assignor: @assignor, assignable: assignable)
    assignment.reload
    assert_equal Assignment::ASSIGNED, assignment.state
  end

  test "#create_assignment should not trigger assignment notification" do
    assignee = create(:user, organization: @org)
    assignable = create(:card, organization: @org, author: @assignor)
    
    EDCAST_NOTIFY.expects(:trigger_event).never
    assignable.create_assignment(user: assignee, assignor: @assignor, assignable: assignable, opts: {skip_notifications: true})
  end

  test "#create_assignment should trigger UsersCardsManagementJob for user assignment" do
    assignee = create(:user, organization: @org)
    author = create(:user, organization: @org)
    assignable = create(:card, organization: @org, author: author)

    UsersCardsManagementJob.expects(:perform_later)
      .with(@assignor.id, assignable.id, UsersCardsManagement::ACTION_USER_ASSIGNMENT, assignee.id)
      .once
    assignable.create_assignment(user: assignee, assignor: @assignor, assignable: assignable)
  end

  test "#create_assignment should trigger UsersCardsManagementJob for self assignment" do
    assignee = create(:user, organization: @org)
    author = create(:user, organization: @org)
    assignable = create(:card, organization: @org, author: author)

    UsersCardsManagementJob.expects(:perform_later)
      .with(@assignor.id, assignable.id, UsersCardsManagement::ACTION_SELF_ASSIGNMENT, @assignor.id)
      .once
    assignable.create_assignment(user: @assignor, assignor: @assignor, assignable: assignable)
  end


end
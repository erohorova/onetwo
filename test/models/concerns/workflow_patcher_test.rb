require 'test_helper'

class WorkflowPatcherTest < ActiveSupport::TestCase
  setup do
    @org = create(:organization)
    @user = create(:user, organization: @org)
    Profile.create(
      user_id: @user.id, organization_id: @org.id,
      external_id: "ext_id-#{@user.id}", uid: "uid-#{@user.id}"
    )
    @data_mapper_class = WorkflowPatcher::PatchDataMapper
  end

  test '#columns_mapper should take proper column schema in org context' do
    columns = @data_mapper_class.took_columns_mapper(@org)
    profile_keys = %w[profile_id profile_external_id profile_uid]
    user_keys = %w[
      id user_email user_first_name user_last_name user_status user_created_at
    ]
    base_keys = columns.map { |el| el[:id] }
    assert columns.is_a?(Array)
    assert_equal %i[id type label], columns.map(&:keys).flatten.uniq
    assert_same_elements  base_keys, profile_keys + user_keys

    cf = create(:custom_field, organization: @org, display_name: 'new_field')
    @org.reload
    custom_fields = %w[custom_new_field]
    columns_with_custom_fields = @data_mapper_class.took_columns_mapper(@org)

    base_keys = columns_with_custom_fields.map { |el| el[:id] }
    assert_same_elements  base_keys, profile_keys + user_keys + custom_fields
    only_customs = base_keys - (profile_keys + user_keys)

    assert_equal 'custom_' + cf.display_name, only_customs.first
  end

  test '#row_mapper should take proper fields and values in user context' do
    row = @data_mapper_class.row_mapper(@user)
    profile = @user.external_profile

    expected_keys = %w[
      profile_id profile_external_id profile_uid id user_email
      user_first_name user_last_name user_status user_created_at
    ]

    actual_keys = row.keys.map(&:to_s)
    assert_same_elements expected_keys, actual_keys
    assert_equal profile.id.to_s, row[:profile_id]
    assert_equal profile.external_id, row[:profile_external_id]
    assert_equal profile.uid, row[:profile_uid]
    assert_equal @user.id.to_s, row[:id]
    assert_equal @user.status, row[:user_status]
    assert_equal @user.created_at.strftime('%s%3N').to_i, row[:user_created_at]
    assert_equal @user.first_name, row[:user_first_name]
    assert_equal @user.last_name, row[:user_last_name]

    cf = create(:custom_field, organization: @org, display_name: 'new_field')
    @org.reload
    user_field = create(:user_custom_field, custom_field: cf, user: @user, value: 'field content')
    @user.reload

    row_with_custom_field = @data_mapper_class.row_mapper(@user)
    expected_key = 'custom_' + cf.abbreviation
    assert row_with_custom_field.has_key?(expected_key.to_sym)
    assert_equal user_field.value, row_with_custom_field[:custom_new_field]
  end

  class DetectUpsertTest < ActiveSupport::TestCase

    setup do
      @org = create(:organization)
      @config = @org.configs.create(
        name: 'workflow_service', value: true, data_type: 'boolean'
      )
      @data_mapper_class = WorkflowPatcher::PatchDataMapper
      Timecop.freeze(DateTime.new(2018,10,1))

      @user_insert_expect = {
        edcastSourceId: 'profile',
        orgName: @org.host_name,
        patchType: 'upsert',
        patchTimestamp: 1538352000000
      }

      @alter_schema_expect = {
        edcastSourceId: 'profile',
        orgName: @org.host_name,
        patchType: 'alter-schema',
        patchTimestamp: 1538352000000
      }
    end

    test 'profile #create after commit should send only one upsert patch' do
      user = create(:user, organization: @org)
      profile = Profile.create(
        user_id: user.id, organization_id: @org.id,
        external_id: "ext_id-#{user.id}", uid: "uid-#{user.id}"
      )
      @user_insert_expect[:data] = @data_mapper_class.row_mapper(user)

      WorkflowPatchSaverJob.expects(:perform_later).with(
        data: @user_insert_expect, org_id: @org.id
      ).once

      profile.run_callbacks(:commit)
    end

    test 'user #update after commit should send only upsert patch' do
      user = create(:user, organization: @org)
      user.committed!
      user.reload
      Profile.create(
        user_id: user.id, organization_id: @org.id,
        external_id: "ext_id-#{user.id}", uid: "uid-#{user.id}"
      )
      user.update_attributes(first_name: 'newName')

      @user_insert_expect[:data] = @data_mapper_class.row_mapper(user)

      WorkflowPatchSaverJob.expects(:perform_later).with(
        data: @user_insert_expect, org_id: @org.id
      ).once

      user.run_callbacks(:commit)
      assert_equal 'newName', @user_insert_expect[:data][:user_first_name]
    end

    test 'user #update should not send patches if updated not white listed user fields' do
      user = create(:user, organization: @org)
      user.update_attributes(sign_in_count: 4)
      @data_mapper_class.expects(:patch_sender).never
      user.run_callbacks(:commit)
    end

    test 'user #suspend! after commit should send delete patch for user' do
      user = create(:user, organization: @org)
      Profile.create(
        user_id: user.id, organization_id: @org.id,
        external_id: "ext_id-#{user.id}", uid: "uid-#{user.id}"
      )
      user.committed!
      user.reload

      @user_insert_expect[:data] = @data_mapper_class.row_mapper(user)
      @user_insert_expect[:patchType] = 'delete'
      @user_insert_expect[:data][:user_status] = 'suspended'

      WorkflowPatchSaverJob.expects(:perform_later).with(
        data: @user_insert_expect, org_id: @org.id
      ).once

      user.suspend!
    end

    test 'user custom field #create after commit should send upsert patch' do
      cf = create(:custom_field, organization: @org, display_name: 'new_field')
      user = create(:user, organization: @org)
      Profile.create(
        user_id: user.id, organization_id: @org.id,
        external_id: "ext_id-#{user.id}", uid: "uid-#{user.id}"
      )
      user_field = create(
        :user_custom_field, custom_field: cf,
        user: user, value: 'field content'
      )
      @org.reload
      user.reload

      @user_insert_expect[:data] = @data_mapper_class.row_mapper(user)

      WorkflowPatchSaverJob.expects(:perform_later).with(
        data: @user_insert_expect, org_id: @org.id
      ).once

      user_field.run_callbacks(:commit)
    end

    test 'user custom field #update after commit should send upsert patch' do
      cf = create(:custom_field, organization: @org, display_name: 'new_field')
      user = create(:user, organization: @org)
      Profile.create(
        user_id: user.id, organization_id: @org.id,
        external_id: "ext_id-#{user.id}", uid: "uid-#{user.id}"
      )
      user_field = create(
        :user_custom_field, custom_field: cf,
        user: user, value: 'field content'
      )
      user_field.update_attributes(value: 'new value')

      user.reload
      @org.reload

      @user_insert_expect[:data] = @data_mapper_class.row_mapper(user)

      WorkflowPatchSaverJob.expects(:perform_later).with(
        data: @user_insert_expect, org_id: @org.id
      ).once

      user_field.run_callbacks(:commit)

      assert_equal 'new value', @user_insert_expect[:data][:custom_new_field]
    end

    test 'custom field #create should send alter-schema patch' do
      cf = create(:custom_field, organization: @org, display_name: 'new_field')
      @org.reload

      @alter_schema_expect[:data] = @data_mapper_class.took_columns_mapper(@org)

      WorkflowPatchSaverJob.expects(:perform_later).with(
        data: @alter_schema_expect, org_id: @org.id
      ).once

      cf.run_callbacks(:commit)
    end

    test 'custom field #update should send alter-schema patch' do
      cf = create(:custom_field, organization: @org, display_name: 'new_field')
      cf.update_attributes(display_name: 'updated', abbreviation: 'updated')
      @org.reload

      @alter_schema_expect[:data] = @data_mapper_class.took_columns_mapper(@org)

      WorkflowPatchSaverJob.expects(:perform_later).with(
        data: @alter_schema_expect, org_id: @org.id
      ).once

      cf.run_callbacks(:commit)

      update_field_object = @alter_schema_expect[:data].find do |item|
        item[:label] == 'updated'
      end

      assert update_field_object.present?
      assert update_field_object.is_a?(Hash)
      assert_equal 'custom_updated', update_field_object[:id]
    end

    test 'profile #create after commit should send upsert patch' do
      user = create(:user, organization: @org)
      profile = Profile.create(
        user_id: user.id, organization_id: @org.id,
        external_id: "ext_id-#{user.id}", uid: "uid-#{user.id}"
      )

      @user_insert_expect[:data] = @data_mapper_class.row_mapper(user)

      WorkflowPatchSaverJob.expects(:perform_later).with(
        data: @user_insert_expect, org_id: @org.id
      ).once

      profile.run_callbacks(:commit)
    end
  end

  class DetectDeleteTest < ActiveSupport::TestCase

    setup do
      @org = create(:organization)
      @config = @org.configs.create(
        name: 'workflow_service', value: true, data_type: 'boolean'
      )
      @data_mapper_class = WorkflowPatcher::PatchDataMapper
      Timecop.freeze(DateTime.new(2018,10,1))
      @user_insert_expect = {
        edcastSourceId: 'profile',
        orgName: @org.host_name,
        patchType: 'delete',
        patchTimestamp: 1538352000000
      }

      @alter_schema_expect = {
        edcastSourceId: 'profile',
        orgName: @org.host_name,
        patchType: 'alter-schema',
        patchTimestamp: 1538352000000
      }
    end

    test 'custom field #destroy after commit should send alter-schema patch' do
      cf = create(:custom_field, organization: @org, display_name: 'new_field')
      WorkflowPatchSaverJob.expects(:perform_later).once
      cf.committed!

      cf.destroy
      @org.reload
      @alter_schema_expect[:data] = @data_mapper_class.took_columns_mapper(@org)

      WorkflowPatchSaverJob.expects(:perform_later).with(
        data: @alter_schema_expect, org_id: @org.id
      ).once
      cf.run_callbacks(:commit)
    end

    test 'user #destroy after commit should send delete patch' do
      user = create(:user, organization: @org)
      Profile.create(
        user_id: user.id, organization_id: @org.id,
        external_id: "ext_id-#{user.id}", uid: "uid-#{user.id}"
      )
      user.committed!

      user.destroy

      @user_insert_expect[:data] = @data_mapper_class.row_mapper(user)

      WorkflowPatchSaverJob.expects(:perform_later).with(
        data: @user_insert_expect, org_id: @org.id
      ).once

      user.run_callbacks(:commit)
    end
  end

  class WorkflowServiceConfig < ActiveSupport::TestCase
    setup do
      @org = create(:organization)
      @config = @org.configs.create(
        name: 'workflow_service', value: false, data_type: 'boolean'
      )
      @data_mapper_class = WorkflowPatcher::PatchDataMapper
      Timecop.freeze(DateTime.new(2018,10,1))
      @user_delete_expect = {
        edcastSourceId: 'profile',
        orgName: @org.host_name,
        patchType: 'delete',
        patchTimestamp: 1538352000000
      }
      @user_insert_expect = {
        edcastSourceId: 'profile',
        orgName: @org.host_name,
        patchType: 'upsert',
        patchTimestamp: 1538352000000
      }
    end

    test 'user #destroy after commit should not send delete patch without enabled config' do
      user = create(:user, organization: @org)
      Profile.create(
        user_id: user.id, organization_id: @org.id,
        external_id: "ext_id-#{user.id}", uid: "uid-#{user.id}"
      )
      user.committed!

      user.destroy

      @user_delete_expect[:data] = @data_mapper_class.row_mapper(user)

      WorkflowPatchSaverJob.expects(:perform_later).with(
        data: @user_delete_expect, org_id: @org.id
      ).never

      user.run_callbacks(:commit)
    end

    test 'profile #create after commit should not send a patch without enabled config' do
      user = create(:user, organization: @org)
      profile = Profile.create(
        user_id: user.id, organization_id: @org.id,
        external_id: "ext_id-#{user.id}", uid: "uid-#{user.id}"
      )

      @user_insert_expect[:data] = @data_mapper_class.row_mapper(user)

      WorkflowPatchSaverJob.expects(:perform_later).with(
        data: @user_insert_expect, org_id: @org.id
      ).never

      profile.run_callbacks(:commit)
    end
  end
end

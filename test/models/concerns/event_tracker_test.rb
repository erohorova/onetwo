require 'test_helper'

class EventTrackerTest < ActiveSupport::TestCase
  self.use_transactional_fixtures = false

  setup do
    Analytics::MetricsRecorderJob.unstub :perform_later

    # For some of the tests, we add event tracking to an existing class
    # which doesn't already have it. We create a temporary subclass to avoid
    # changing the original application behavior.
    @test_class = Class.new(UserMetricsAggregation) do
      def self.name; "TestClass"; end
      include EventTracker
      configure_metric_recorders(
        default_actor: lambda { |record| },
        trackers: {
          on_edit: {
            event: "fake",
          },
          on_delete: {
            event: "fake",
          },
        },
      )
    end

    @org = create(:organization)
    @user = create(:user, organization: @org)
    @agg = @test_class.create!(
      build(:user_metrics_aggregation,
        user: @user,
        organization: @org
      ).attributes
    )
    EdcastActiveJob.unstub(:perform_later)
    Analytics::MetricsRecorderJob.unstub(:perform_later)
  end

  test 'should track create event' do
    skip # CANT GET THIS ONE TO PASS
    author = create(:user)
    now = Time.now
    Time.stubs(:now).returns now

    card = create(:card, author: author)
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        recorder: 'CardMetricsRecorder',
        event: 'card_created',
        actor: Analytics::MetricsRecorder.user_attributes(author),
        org: Analytics::MetricsRecorder.org_attributes(author.organization),
        timestamp: now.to_i,
        additional_data: {},
        card: Analytics::MetricsRecorder.card_attributes(card)
      )
    ).once
    card.stubs(:author_changed?)
    card.run_callbacks(:commit)
  end

  test 'should record custom event' do
    author = create(:user)
    now = Time.now
    Time.stubs(:now).returns now

    Card.stubs :track_create_event
    card = create(:card, author: author)

    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        recorder: 'CardMetricsRecorder',
        event: 'card_published',
        actor: Analytics::MetricsRecorder.user_attributes(author),
        org: Analytics::MetricsRecorder.org_attributes(author.organization),
        timestamp: now.to_i,
        additional_data: {obj_id: 123},
        card: Analytics::MetricsRecorder.org_attributes(card)
      )
    ).once

    Analytics::MetricsRecorderJob.unstub :perform_later
    card.record_custom_event({
      recorder: 'CardMetricsRecorder',
      default_actor: :author,
      event: 'card_published',
      opts: {obj_id: 123}
    })
  end

  test 'should record delete event' do
    author = create(:user)
    now = Time.now
    Time.stubs(:now).returns now

    Card.stubs(:track_create_event).returns true
    card = create(:card, author: author)

    Analytics::MetricsRecorderJob.expects(:perform_later).with({
        recorder: 'CardMetricsRecorder',
        event: 'card_deleted',
        actor: Analytics::MetricsRecorder.user_attributes(author),
        org: Analytics::MetricsRecorder.org_attributes(author.organization),
        timestamp: now.to_i,
        additional_data: {},
        card: Analytics::MetricsRecorder.org_attributes(card)}
    )

    Analytics::MetricsRecorderJob.unstub :perform_later
    card.destroy
  end

  test 'doesnt push event on edit with unknown recorder' do
    # NOTE: I also wanted to test this behavior on create/destroy
    # but I can't figure out how to make the tests work correctly.

    Analytics::MetricsRecorderJob.expects(:perform_later).never
    @agg.update start_time: 1
  end

end

# coding: utf-8
require 'test_helper'

# =========================================
# Note. This concern adds class methods to the Team model.
# It is used from the teams controller to return per-team stats
# (either for a single team, or to compare teams).
#
# Most of the data used here comes from the GroupMetricsAggregation
# records, although some of it 
# =========================================

class TeamModelLeaderboardV2ConcernTest < ActiveSupport::TestCase

  using ObjectRefinements::GroupBySequence

  setup do
    # This is the class that does the heavy lifting here.
    # In the actual code, it's only accessed through Team
    @service = TeamModelLeaderboardV2Concern::Service
    # store some times
    @base_time = Time.now
    Time.stubs(:now).returns @base_time
    @start_of_yesterday = Time.now.utc.beginning_of_day - 1.day
    @end_of_yesterday = @start_of_yesterday.end_of_day

    # create 2 orgs
    @org1, @org2 = create_list :organization, 2

    # create 2 teams in each org
    org1_teams = create_list :team, 2, organization: @org1
    org2_teams = create_list :team, 2, organization: @org2
    @org1_team1, @org1_team2 = org1_teams
    @org2_team1, @org2_team2 = org2_teams

    # Create 2 users in each team
    # This is required for the total period "total_users" and "active_users"
    org1_teams.each do |team|
      users = create_list :user, 2, organization: @org1
      team.add_to_team users, as_type: "member"
    end
    org2_teams.each do |team|
      users = create_list :user, 2, organization: @org2
      team.add_to_team users, as_type: "member"
    end

    # Set all the org1 teams_users to have a created_at of yesterday.
    # This is so they are not factored into "total_users" and "active_users"
    # for two days ago.
    TeamsUser.joins(:team).where(teams: { organization_id: @org1.id }).update_all(
      created_at: @start_of_yesterday
    )

    # Create some UserMetricsAggregations for each user
    # This is required for the total period "active_users"
    User.find_each do |user|
      2.times do |day_idx|
        UserMetricsAggregation.create!(
          organization_id: user.organization_id,
          user_id: user.id,
          smartbites_commented_count: 0,
          smartbites_completed_count: 0,
          smartbites_consumed_count: 0,
          smartbites_created_count: 0,
          smartbites_liked_count: 0,
          # Time spent is the only meaningful metric here.
          # It will be one for all records on the previous day,
          # and zero for all records two days ago.
          time_spent_minutes: 1 - day_idx,
          total_smartbite_score: 0,
          total_user_score: 0,
          start_time: (@start_of_yesterday - day_idx.days).to_i,
          end_time: (@end_of_yesterday - day_idx.days).to_i
        )
      end
    end

    # create some aggregations for each team, over 2 days
    [org1_teams, org2_teams].each.with_index do |org_teams, org_idx|
      org_idx += 1 # don't use 0-indexing here
      org_teams.each.with_index do |team, team_idx|
        team_idx += 1 # don't use 0-indexing here
        2.times do |day_idx|
          GroupMetricsAggregation.create(
            team_id:             team.id,
            organization_id:     team.organization_id,
            smartbites_created:  "1#{org_idx}#{team_idx}#{day_idx}".to_i,
            smartbites_consumed: "2#{org_idx}#{team_idx}#{day_idx}".to_i,
            time_spent_minutes:  "3#{org_idx}#{team_idx}#{day_idx}".to_i,
            total_users:         "4#{org_idx}#{team_idx}#{day_idx}".to_i,
            active_users:        "5#{org_idx}#{team_idx}#{day_idx}".to_i,
            new_users:           "6#{org_idx}#{team_idx}#{day_idx}".to_i,
            engagement_index:    "7#{org_idx}#{team_idx}#{day_idx}".to_i,
            num_comments:        "8#{org_idx}#{team_idx}#{day_idx}".to_i,
            num_likes:           "9#{org_idx}#{team_idx}#{day_idx}".to_i,
            start_time:          (@start_of_yesterday - day_idx.days).to_i,
            end_time:            (@end_of_yesterday - day_idx.days).to_i
          )
        end
      end
    end

  end

  test 'fetch_v2_analytics with date filters and default ordering' do
    actual = Team.fetch_v2_analytics(
      params: {
        from_date: @start_of_yesterday.strftime("%m/%d/%Y"),
        to_date: @end_of_yesterday.strftime("%m/%d/%Y"),
        limit: 10,
        offset: 0
      },
      current_org: @org1
    )

    # The default ordering is smartbites_consumed DESC
    expected = [
      {
        "smartbites_created"        => 1120,
        "smartbites_consumed"       => 2120,
        "smartbites_comments_count" => 8120,
        "smartbites_liked"          => 9120,
        "total_users"               => 2,
        "active_users"              => 2,
        "organization_id"           => @org1.id,
        "team_id"                   => @org1_team2.id,
        "from_date"                 => @start_of_yesterday.to_i,
        "to_date"                   => @end_of_yesterday.to_i,
        "average_session"           => 3120.0,
        "engagement_index"          => 1.0,
        "time_spent"                => 3120,
        "daily_records" => [
          {
            "smartbites_created"        => 1120,
            "smartbites_consumed"       => 2120,
            "time_spent"                => 3120,
            "total_users"               => 4120,
            "active_users"              => 5120,
            "new_users"                 => 6120,
            "engagement_index"          => 7120,
            "smartbites_comments_count" => 8120,
            "smartbites_liked"          => 9120,
            "start_time"                => @start_of_yesterday.to_i,
            "end_time"                  => @end_of_yesterday.to_i,
          }
        ],
      },
      {
        "smartbites_created"        => 1110,
        "smartbites_consumed"       => 2110,
        "smartbites_comments_count" => 8110,
        "smartbites_liked"          => 9110,
        "total_users"               => 2,
        "active_users"              => 2,
        "organization_id"           => @org1.id,
        "team_id"                   => @org1_team1.id,
        "from_date"                 => @start_of_yesterday.to_i,
        "to_date"                   => @end_of_yesterday.to_i,
        "average_session"           => 3110.0,
        "time_spent"                => 3110,
        "engagement_index"          => 1.0,
        "daily_records" => [
          {
            "smartbites_created"        => 1110,
            "smartbites_consumed"       => 2110,
            "time_spent"                => 3110,
            "total_users"               => 4110,
            "active_users"              => 5110,
            "new_users"                 => 6110,
            "engagement_index"          => 7110,
            "smartbites_liked"          => 9110,
            "start_time"                => @start_of_yesterday.to_i,
            "end_time"                  => @end_of_yesterday.to_i,
            "smartbites_comments_count" => 8110,
          }
        ],
      },
    ]
    assert_equal expected, actual
  end

  test 'order params' do
    # We update the @org1_team1 aggregations to have a small num_likes.
    GroupMetricsAggregation.where(team_id: @org1_team1.id).update_all(
      num_likes: 1
    )
    # We fetch the results, ordering by num_likes ASC
    actual = Team.fetch_v2_analytics(
      params: {
        from_date: @start_of_yesterday.strftime("%m/%d/%Y"),
        to_date: @end_of_yesterday.strftime("%m/%d/%Y"),
        limit: 10,
        offset: 0,
        order_attr: "smartbites_liked",  order: "ASC"
      },
      current_org: @org1
    )

    expected = [
      {
        "smartbites_comments_count" => 8110,
        "smartbites_created"        => 1110,
        "smartbites_consumed"       => 2110,
        "smartbites_liked"          => 1,
        "total_users"               => 2,
        "active_users"              => 2,
        "organization_id"           => @org1.id,
        "team_id"                   => @org1_team1.id,
        "from_date"                 => @start_of_yesterday.to_i,
        "to_date"                   => @end_of_yesterday.to_i,
        "average_session"           => 3110.0,
        "time_spent"                => 3110,
        "engagement_index"          => 1.0,
        "daily_records" => [
          {
            "smartbites_comments_count" => 8110,
            "smartbites_created"        => 1110,
            "smartbites_consumed"       => 2110,
            "time_spent"                => 3110,
            "total_users"               => 4110,
            "active_users"              => 5110,
            "new_users"                 => 6110,
            "engagement_index"          => 7110,
            "smartbites_liked"          => 1,
            "start_time"                => @start_of_yesterday.to_i,
            "end_time"                  => @end_of_yesterday.to_i,
          }
        ],
      },
      {
        "smartbites_comments_count" => 8120,
        "smartbites_created"        => 1120,
        "smartbites_consumed"       => 2120,
        "smartbites_liked"          => 9120,
        "total_users"               => 2,
        "active_users"              => 2,
        "organization_id"           => @org1.id,
        "team_id"                   => @org1_team2.id,
        "from_date"                 => @start_of_yesterday.to_i,
        "to_date"                   => @end_of_yesterday.to_i,
        "average_session"           => 3120.0,
        "time_spent"                => 3120,
        "engagement_index"          => 1.0,
        "daily_records" => [
          {
            "smartbites_comments_count" => 8120,
            "smartbites_created"        => 1120,
            "smartbites_consumed"       => 2120,
            "time_spent"                => 3120,
            "total_users"               => 4120,
            "active_users"              => 5120,
            "new_users"                 => 6120,
            "engagement_index"          => 7120,
            "smartbites_liked"          => 9120,
            "start_time"                => @start_of_yesterday.to_i,
            "end_time"                  => @end_of_yesterday.to_i,
          }
        ],
      },
    ]
    assert_equal expected, actual
  end

  test 'pagination' do
    actual = Team.fetch_v2_analytics(
      params: {
        from_date: @start_of_yesterday.strftime("%m/%d/%Y"),
        to_date: @end_of_yesterday.strftime("%m/%d/%Y"),
        limit: 1,
        offset: 1
      },
      current_org: @org1
    )

    # The default ordering is smartbites_consumed DESC
    expected = [

      {
        "smartbites_comments_count" => 8110,
        "smartbites_created"        => 1110,
        "smartbites_consumed"       => 2110,
        "smartbites_liked"          => 9110,
        "total_users"               => 2,
        "active_users"              => 2,
        "organization_id"           => @org1.id,
        "team_id"                   => @org1_team1.id,
        "from_date"                 => @start_of_yesterday.to_i,
        "to_date"                   => @end_of_yesterday.to_i,
        "average_session"           => 3110.0,
        "time_spent"                => 3110,
        "engagement_index"          => 1.0,
        "daily_records" => [
          {
            "smartbites_comments_count" => 8110,
            "smartbites_created"        => 1110,
            "smartbites_consumed"       => 2110,
            "time_spent"                => 3110,
            "total_users"               => 4110,
            "active_users"              => 5110,
            "new_users"                 => 6110,
            "engagement_index"          => 7110,
            "smartbites_liked"          => 9110,
            "start_time"                => @start_of_yesterday.to_i,
            "end_time"                  => @end_of_yesterday.to_i,
          }
        ],
      },
    ]
    assert_equal expected, actual
  end

  test 'provides records for all days in range' do
    # All days in the requested range should have a record returned for them,
    # even if that record has all zeroes.
    # We're going to request data for the past 3 days, although there
    # only exists data for the previous two.
    # We are checking that a result gets returned for the first day.
    actual = Team.fetch_v2_analytics(
      params: {
        from_date: (@start_of_yesterday - 2.days).strftime("%m/%d/%Y"),
        to_date: @end_of_yesterday.strftime("%m/%d/%Y"),
        limit: 10,
        offset: 0,
        # We apply team filter so the results are smaller.
        team_ids: [@org1_team1]
      },
      current_org: @org1
    )

    # The default ordering is smartbites_consumed DESC
    expected = [
      {
        "smartbites_comments_count" => 16221,
        "smartbites_created"        => 2221,
        "smartbites_consumed"       => 4221,
        "smartbites_liked"          => 18221,
        "total_users"               => 2,
        "active_users"              => 2,
        "organization_id"           => @org1.id,
        "team_id"                   => @org1_team1.id,
        "from_date"                 => (@start_of_yesterday - 2.days).to_i,
        "to_date"                   => @end_of_yesterday.to_i,
        "average_session"           => 2073.67,
        "time_spent"                => 6221,
        "engagement_index"          => 1.0,
        "daily_records" => [
          {
            "smartbites_comments_count" => 0,
            "smartbites_created"        => 0,
            "smartbites_consumed"       => 0,
            "smartbites_liked"          => 0,
            "time_spent"                => 0,
            "total_users"               => 0,
            "active_users"              => 0,
            "new_users"                 => 0,
            "engagement_index"          => 0,
            "start_time"                => (@start_of_yesterday - 2.days).to_i,
            "end_time"                  => (@end_of_yesterday - 2.days).to_i
          }, {
            "smartbites_created"        => 1111,
            "smartbites_consumed"       => 2111,
            "smartbites_comments_count" => 8111,
            "smartbites_liked"          => 9111,
            "time_spent"                => 3111,
            "total_users"               => 4111,
            "active_users"              => 5111,
            "new_users"                 => 6111,
            "engagement_index"          => 7111,
            "start_time"                => (@start_of_yesterday - 1.days).to_i,
            "end_time"                  => (@end_of_yesterday - 1.days).to_i
          }, {
            "smartbites_created"        => 1110,
            "smartbites_consumed"       => 2110,
            "smartbites_comments_count" => 8110,
            "smartbites_liked"          => 9110,
            "time_spent"                => 3110,
            "total_users"               => 4110,
            "active_users"              => 5110,
            "new_users"                 => 6110,
            "engagement_index"          => 7110,
            "start_time"                => (@start_of_yesterday).to_i,
            "end_time"                  => (@end_of_yesterday).to_i
          }
        ],

      },
    ]
    assert_equal expected, actual
  end

  test 'team_ids filter' do
    # Here we are filtering to onl include @org1_team1
    actual = Team.fetch_v2_analytics(
      params: {
        from_date: @start_of_yesterday.strftime("%m/%d/%Y"),
        to_date: @end_of_yesterday.strftime("%m/%d/%Y"),
        limit: 10,
        offset: 0,
        team_ids: [@org1_team1]
      },
      current_org: @org1
    )

    # The default ordering is smartbites_consumed DESC
    expected = [
      {
        "smartbites_comments_count" => 8110,
        "smartbites_created"        => 1110,
        "smartbites_consumed"       => 2110,
        "smartbites_liked"          => 9110,
        "total_users"               => 2,
        "active_users"              => 2,
        "organization_id"           => @org1.id,
        "team_id"                   => @org1_team1.id,
        "from_date"                 => @start_of_yesterday.to_i,
        "to_date"                   => @end_of_yesterday.to_i,
        "average_session"           => 3110.0,
        "time_spent"                => 3110,
        "engagement_index"          => 1.0,
        "daily_records" => [
          {
            "smartbites_comments_count" => 8110,
            "smartbites_created"        => 1110,
            "smartbites_consumed"       => 2110,
            "time_spent"                => 3110,
            "total_users"               => 4110,
            "active_users"              => 5110,
            "new_users"                 => 6110,
            "engagement_index"          => 7110,
            "smartbites_liked"          => 9110,
            "start_time"                => @start_of_yesterday.to_i,
            "end_time"                  => @end_of_yesterday.to_i,
          }
        ],
      },
    ]
    assert_equal expected, actual
  end

  test 'build_cache_key' do
    # We provide stub values for the params
    params = {
      from_date:  "1",
      to_date:    "2",
      order:      "3",
      order_attr: "4",
      team_ids:   "5",
      limit:      "6",
      offset:     "7"
    }
    # Make a cryptographic code for the param values
    params_code = params.hash
    expected = [
      "organizations",
      @org1.id,
      "team-leaderboard",
      params_code
    ].join("/")
    actual = @service.send(
      :build_cache_key,
      params: params.merge("this": "is ignored"),
      current_org: @org1
    )
    assert_equal expected, actual
  end

  test 'caches' do
    begin
      # Store a temporary reference to the original cache
      orig_cache = Rails.cache

      # Temporarily set the cache to a memory store (it's null store by default)
      Rails.cache = ActiveSupport::Cache::MemoryStore.new
      
      # We provide stub values for the params
      params = {
        from_date:  "1",
        to_date:    "2",
        order:      "3",
        order_attr: "4",
        team_ids:   "5",
        limit:      "6",
        offset:     "7"
      }

      # The method that fires the queries is only hit once.
      # The second time that .fetch_v2_analytics is called, the result
      # is read from the cache instead.
      @service.expects(:get_results).once.returns "stub results"

      result = @service.send(
        :fetch_v2_analytics,
        params: params,
        current_org: @org1
      )

      assert_equal result, "stub results"

      # We want to check that the .get_results method is not called again
      @service.expects(:get_results).never

      result2 = @service.send(
        :fetch_v2_analytics,
        params: params,
        current_org: @org1
      )

      assert_equal result2, "stub results"
    ensure
      # Ensure that we set the cache back to the null store after every run.
      Rails.cache = orig_cache
    end

  end

  test 'caches with an expiry time of 30 minutes' do
    params = {
      from_date:  "1",
      to_date:    "2",
      order:      "3",
      order_attr: "4",
      team_ids:   "5",
      limit:      "6",
      offset:     "7"
    }
    cache_key = @service.send(:build_cache_key, params: params, current_org: @org1)
    @service.expects(:get_results).returns "stub result"
    Rails.cache.expects(:write).with cache_key, "stub result", expires_in: 30.minutes
    @service.send(
      :fetch_v2_analytics,
      params: params,
      current_org: @org1
    )
  end

end


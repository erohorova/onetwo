require 'test_helper'

class QuizQuestionStatsTest < ActiveSupport::TestCase

  setup do
    QuizQuestionStatsRecorderJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
  end

  test 'as json' do
    poll = create(:card, card_type: 'poll', card_subtype: 'text')
    stats = create(:quiz_question_stats, quiz_question: poll, attempt_count: 5, options_stats: {1 => 5, 2 => 0})
    assert_equal ({'attempt_count' => 5, 'options_stats' => {1 => 5, 2 => 0}}), stats.as_json
  end

  test 'should reindex card' do
    poll = create(:card, card_type: 'poll', card_subtype: 'text')
    poll.expects(:reindex).returns(nil).twice
    stats = create(:quiz_question_stats, quiz_question: poll, attempt_count: 0, stats: {})
    stats.run_callbacks(:commit)

    stats.update_attributes(attempt_count: 5, options_stats: {'1' => 6, '2' => 10})
    stats.run_callbacks(:commit)
  end
end

require 'test_helper'

class PaypalPaymentGatewayTest < ActiveSupport::TestCase
  setup do
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @card = create :card, organization: @organization, author: @user
    @price = create(:price, card: @card)
    @order = create :order, orderable: @card, user_id: @user.id,
                   organization_id: @organization.id
    @transaction = create :transaction, order: @order, user: @user
  end

  test '#authorize_payment success' do
    payment_object = PayPal::SDK::REST::Payment.new(id: 'PAY-A1B2C3D4E5F7G8H9', payer_id: 'ABCDE12345')
    PayPal::SDK::REST::Payment.stubs(:find).returns(payment_object)
    PayPal::SDK::REST::Payment.any_instance.stubs(:execute).returns(payment_object)

    gateway = PaypalPaymentGateway.new
    result = gateway.authorize_payment(@transaction, payment_id: payment_object.id)

    assert_equal payment_object, result
  end

  test '#authorize_payment failure' do
    payment_object = PayPal::SDK::REST::Payment.new(id: 'PAY-A1B2C3D4E5F7G8H9', payer_id: 'ABCDE12345')

    PayPal::SDK::REST::Payment.stubs(:find).returns(payment_object)
    payment_object.error = 'Error in execution'
    PayPal::SDK::REST::Payment.any_instance.stubs(:execute).returns(payment_object)
    gateway = PaypalPaymentGateway.new
    result = gateway.authorize_payment(@transaction, payment_id: payment_object.id)

    refute result.success?
  end

  test '#void_payment should return true if payment was voided successfully' do
    PaypalPaymentGateway.any_instance.stubs(:get_authorization_object).returns(PayPal::SDK::REST::Authorization.new)
    PayPal::SDK::REST::Authorization.any_instance.stubs(:void).returns(true)

    result = PaypalPaymentGateway.new.void_payment(gateway: PayPal::SDK::REST::Payment.new)

    assert_equal true, result
  end

  test '#capture_payment should return true if payment was captured successfully' do
    PaypalPaymentGateway.any_instance.stubs(:get_authorization_object).returns(PayPal::SDK::REST::Authorization.new)
    PayPal::SDK::REST::Authorization.any_instance.stubs(:capture).returns(true)

    result = PaypalPaymentGateway.new.capture_payment(transaction: @transaction, gateway: PayPal::SDK::REST::Payment.new)

    assert_equal true, result
  end

  test '#capture_payment should return false if payment was not captured successfully' do
    PaypalPaymentGateway.any_instance.stubs(:get_authorization_object).returns(PayPal::SDK::REST::Authorization.new)
    PayPal::SDK::REST::Authorization.any_instance.stubs(:capture).returns(false)

    result = PaypalPaymentGateway.new.capture_payment(transaction: @transaction, gateway: PayPal::SDK::REST::Payment.new)

    assert_equal false, result
  end

  test '#create_payment_object must return approval url of paypal' do
    urls = {return_url: 'http://host.domain.com',
            cancel_url: 'http://host.domain.com'}
    PayPal::SDK::REST::Payment.any_instance.stubs(:create).returns(true)
    PayPal::SDK::REST::Payment.any_instance.stubs(:approval_url).returns("http://sandbox.paypal.com?token=AAVBBB")

    payment_object = PaypalPaymentGateway.new.create_payment_object(orderable: @card, price: @price, urls: urls, user_id: @user.id)
    assert_equal "http://sandbox.paypal.com?token=AAVBBB", payment_object.approval_url
  end

  test '#create_payment_object must raise error in case paypal create api fails' do
    urls = {return_url: 'http://host.domain.com',
            cancel_url: 'http://host.domain.com'}
    PayPal::SDK::REST::Payment.any_instance.stubs(:create).returns(false)

    exception = assert_raises(Exception) {
      PaypalPaymentGateway.new.create_payment_object(orderable: @card, price: @price, urls: urls, user_id: @user.id)
    }
    assert_equal exception.message, 'Paypal create api failed'
  end

  test '#merge_gateway_params should merge redirect_url to the resp hash' do
    payment_object = PayPal::SDK::REST::Payment.new(id: 'PAY-A1B2C3D4E5F7G8H9', payer_id: 'ABCDE12345')
    payment_object.stubs(:approval_url).returns('https://test-paypal.com')
    resp_params = { transaction: @transaction, order: @order, token: 'AA11BB22CC33DD44EE55' }
    PaypalPaymentGateway.any_instance.stubs(:create_payment_object).returns(payment_object)
    result = PaypalPaymentGateway.new.merge_gateway_params(
                                       params: {
                                         resp_params: resp_params,
                                         orderable: @card,
                                         price: @price
                                       }
                                     )
    assert_equal 'https://test-paypal.com', result[:redirect_url]
  end

end
require 'test_helper'

class BookmarkTest < ActiveSupport::TestCase
  should belong_to(:user)

  setup do
    @org = create(:organization)
  end

  test 'should add to learning queue on bookmark' do
    user = create(:user, organization: @org)
    card = create(:card, author_id: nil, organization: @org)
    UserContentsQueue.stubs(:remove_from_queue).returns true

    assert_difference ->{LearningQueueItem.count} do
      bookmark = create(:bookmark, bookmarkable: card, user: user)
    end
    lq = LearningQueueItem.last
    assert_equal card, lq.queueable
    assert_equal user, lq.user
    assert_equal "bookmarks", lq.source
  end

  test 'should remove from learning queue on unbookmark and not remove from user content queue' do
    user = create(:user, organization: @org)
    card = create(:card, author_id: nil, organization: @org)

    bookmark = nil
    # removes item from user content queue once bookmarked
    UserContentsQueue.expects(:remove_from_queue).with(user, card).never

    assert_difference ->{LearningQueueItem.where(user: user, queueable: card, source: "bookmarks").count} do
      bookmark = create(:bookmark, bookmarkable: card, user: user)
    end

    assert_difference ->{LearningQueueItem.where(user: user, queueable: card, source: "bookmarks").count}, -1 do
      bookmark.destroy
    end
  end

  test 'should update a completed bookmarks source to assignment when unbookmarked' do
    user = create(:user, organization: @org)
    card = create(:card, author_id: nil, organization: @org)
    bookmark = nil
    assert_difference ->{LearningQueueItem.where(user: user, queueable: card, source: "bookmarks").count} do
      bookmark = create(:bookmark, bookmarkable: card, user: user)
    end
    user.learning_queue_items.find_by(queueable: card).complete

    assert_equal 'bookmarks', user.learning_queue_items.first.source

    create(:user_content_completion, :completed, completable: card, user: user)
    assert_no_difference ->{user.learning_queue_items.count} do
      bookmark.destroy
    end
    assert_equal 'assignments', user.learning_queue_items.first.source
  end

  test 'to ensure unbookmarking doesnt delete learning queue items that are marked as completed' do
    org = create(:organization)
    author, user, another_user = create_list(:user, 3, organization: org)
    card1, card2, card3 = create_list(:card, 3, author: author)
    UserContentsQueue.stubs(:remove_from_queue).returns true
    Analytics::MetricsRecorderJob.stubs(:perform_later)

    # create bookmarks for another user
    create(:user_content_completion, :completed, completable: card1, user: another_user)
    create(:user_content_completion, :started, completable: card2, user: another_user)

    bookmark = create(:bookmark, bookmarkable: card1, user: another_user)
    bookmark = create(:bookmark, bookmarkable: card2, user: another_user)

    # marked as completed first and then bookmarked
    create(:user_content_completion, :completed, completable: card1, user: user)
    bookmark = create(:bookmark, bookmarkable: card1, user: user)
    assert_no_difference ->{LearningQueueItem.where(user_id: user.id, source: "bookmarks", state: "active", queueable: card1).count} do
      bookmark.destroy
    end

    # bookmarked first and then marked as complete
    bookmark1 = create(:bookmark, bookmarkable: card2, user: user)
    create(:user_content_completion, :completed, completable: card2, user: user)
    assert_no_difference ->{user.learning_queue_items.count} do
      bookmark1.destroy
    end

    # when unbookmarking a learning queue item that is just bookmarked
    bookmark2 = create(:bookmark, bookmarkable: card3, user: user)
    assert_difference ->{user.learning_queue_items.count}, -1 do
      bookmark2.destroy
    end

    assert_equal 2, another_user.user_content_completions.count
  end

  class CardBookmarkMetricRecorderTest < ActiveSupport::TestCase
    setup do
      UserContentsQueue.stubs(:remove_from_queue).returns true
    end

    test 'should invoke bookmark metric recorder job #bookmark' do
      Card.any_instance.stubs(:record_card_create_event).returns true
      Analytics::MetricsRecorderJob.expects(:perform_later)
      .with(has_entry(event: 'card_bookmarked'))
      .once

      org = create(:organization)
      author = create(:user, organization: org)
      card = create(:card, author: author)
      bookmark = create(:bookmark, bookmarkable: card, user: author)
      bookmark.run_callbacks :commit
    end

    test 'should invoke card metric recorder job #unbookmark' do
      RequestStore.unstub(:read)
      Organization.any_instance.stubs(:create_admin_user)

      RequestStore.store[:request_metadata] = {
        platform: 'web', user_agent: 'Edcast iPhone'
      }
      Card.any_instance.stubs(:delete_ecl_card).returns true
      Bookmark.any_instance.stubs(:record_card_bookmark_event).returns true
      Team.any_instance.stubs(:send_group_created_metric)
      org = create(:organization)
      author = create(:user, organization: org)
      card = create(:card, author: author)
      bookmark = create(:bookmark, bookmarkable: card, user: author)
      Analytics::MetricsRecorderJob.unstub :perform_later
      Timecop.freeze(2.days.from_now) do
        Analytics::MetricsRecorderJob.expects(:perform_later).with({
          recorder: "Analytics::CardBookmarkRecorder",
          bookmark_id: bookmark.id,
          org: Analytics::MetricsRecorder.org_attributes(org),
          card: Analytics::MetricsRecorder.card_attributes(bookmark.bookmarkable),
          actor: Analytics::MetricsRecorder.user_attributes(author),
          timestamp: Time.now.to_i,
          event: 'card_unbookmarked',
          additional_data: {platform: 'web', user_agent: 'Edcast iPhone'}
        }).returns true
        bookmark.destroy
      end
    end
  end
end

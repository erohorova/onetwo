require 'test_helper'

class ConfigurationTest < ActiveSupport::TestCase

  test 'should access cfg' do
    create(:configuration, name: 'n', value: 'v', type: 'User')

    assert_equal 'v', User.cfg('n')
  end

  test 'should control access to cfg' do
    create(:configuration, name: 'n', value: 'v', type: 'Configuration')

    assert_equal 'v', Configuration.cfg('n')
    assert_raise Configuration::ConfigurationAccessError do
      User.cfg('n')
    end
  end

end

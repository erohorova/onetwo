require 'test_helper'
class SsoTest < ActiveSupport::TestCase

  should have_many(:organizations)
  should have_many(:org_ssos)

  test "show all orgs using this option" do
    create_default_ssos
    Organization.any_instance.unstub :default_sso
    sso = Sso.where(name: "email").first

    org = create_list(:organization, 2)
    assert_equal 2, sso.organizations.count
  end
end

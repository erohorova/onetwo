require 'test_helper'

class ActivityStreamTest < ActiveSupport::TestCase
  should belong_to(:user)
  should belong_to(:streamable)

  class CardActivityStreamTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false
    include ActiveJob::TestHelper
    include CardHelperMethods

    def create_simple_card
      ActivityStreamCreateJob.unstub(:perform_later)
      card = nil
      org = create(:organization)
      author = create(:user, organization: org)
      perform_enqueued_jobs do
        card = create(:card, author: author)
      end
      card
    end

    def create_simple_pathway_draft
      ActivityStreamCreateJob.unstub(:perform_later)
      org = create(:organization)
      user = create(:user, organization: org)
      draft_pathway = nil
      perform_enqueued_jobs do
        draft_pathway = create(:card, card_type: 'pack', state: 'draft', card_subtype: 'simple', author_id: user.id, title: 'draft cover')
        cards = create_list(:card, 4, card_type: 'media')

        cards.each do |card|
          CardPackRelation.add(cover_id: draft_pathway.id, add_id: card.id, add_type: card.class.name)
        end
      end
      draft_pathway
    end

    test 'card create should create activitystream job when created' do
      card = create_simple_card
      assert_equal ActivityStream::ACTION_CREATED_CARD, card.activity_streams.first.action
      assert_equal card.id, card.activity_streams.first.linkable_item.id
    end

    test 'should not create xapi activity on delete' do
      card = create_simple_card

      TestAfterCommit.with_commits do
        ActivityStream.any_instance.expects(:create_xapi_activity).twice
        activity_stream = create(:activity_stream,
          streamable: card,
          action: 'created_card',
          user: card.author,
          organization: card.organization
        )
        activity_stream.update(comments_count: 2)

        # DO NOT INVOKE ON DELETE
        ActivityStream.any_instance.expects(:create_xapi_activity).never
        activity_stream.destroy
      end

    end


    test 'card without author should not create activity stream' do
      card = nil
      organization = create(:organization)
      perform_enqueued_jobs do
        card = create(:card, author_id: nil, organization: organization)
      end
      assert_equal true, card.activity_streams.where(action: ActivityStream::ACTION_CREATED_CARD).blank?
    end

    test 'card create in a private should not create activitystream job when created' do
      ActivityStreamCreateJob.unstub(:perform_later)
      ChannelsCard.any_instance.stubs :create_users_management_card

      org = create(:organization)
      response_array = []
      channel1 = create(:channel, :private, organization: org)
      channel2 = create(:channel, :another_private, organization: org)
      channels = [channel1, channel2]
      user = create(:user, organization: org)
      channels.each { |channel| channel.users << user}
      card = build(:card, author: user, organization: org )
      params = card.attributes.symbolize_keys
      params[:channel_ids] = channels.collect(&:id)
      params[:card_type] = "media"
      params[:card_subtype] = 'link'
      perform_enqueued_jobs do
        response_array = Card.create(params)
      end
      created_card = Card.find(response_array.id)
      assert_equal true, created_card.activity_streams.where(action: ActivityStream::ACTION_CREATED_CARD).blank?
      params[:channel_ids] = []
      perform_enqueued_jobs do
        response_array = Card.create(params)
      end
      created_card = Card.find(response_array.id)
      assert_equal false, created_card.activity_streams.where(action: ActivityStream::ACTION_CREATED_CARD).blank?
    end

    test 'unpublished cards should not create activity stream' do
      card = nil
      perform_enqueued_jobs do
        card = create(:card, state: :new)
      end
      assert_equal true, card.activity_streams.where(action: ActivityStream::ACTION_CREATED_CARD).blank?
    end

    test 'when a card is unpublished it should delete all is activity stream' do
      card = create_simple_card
      assert_equal false, card.activity_streams.where(action: ActivityStream::ACTION_CREATED_CARD).blank?
      card.destroy
      assert_equal true, card.activity_streams.where(action: ActivityStream::ACTION_CREATED_CARD).blank?
    end

    test 'pathway draft should not create activity stream job when created' do
      draft_pathway = create_simple_pathway_draft
      assert_equal true, draft_pathway.activity_streams.where(action: ActivityStream::ACTION_CREATED_CARD).blank?
      assert_equal true, draft_pathway.activity_streams.where(action: ActivityStream::ACTION_CREATED_PATHWAY).blank?
    end

    test 'create activity stream when pathway is published' do
      draft_pathway = create_simple_pathway_draft
      perform_enqueued_jobs do
        draft_pathway.publish
      end
      assert_equal true, draft_pathway.activity_streams.where(action: ActivityStream::ACTION_CREATED_CARD).blank?
      assert_equal false, draft_pathway.activity_streams.where(action: ActivityStream::ACTION_CREATED_PATHWAY).blank?
    end

    test 'cards inside a pathway should not create a activity stream' do
      ActivityStreamCreateJob.unstub(:perform_later)

      perform_enqueued_jobs do
        setup_one_card_pack
      end
      created_card = Card.find(@card1.id)
      assert_equal true, created_card.activity_streams.where(action: ActivityStream::ACTION_CREATED_CARD).blank?
    end
  end

  class VideoStreamActivityStreamTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false
    include ActiveJob::TestHelper

    setup do
      VideoStream.any_instance.stubs(:schedule_reminder_push_notification).returns true
      ActivityStreamCreateJob.unstub(:perform_later)
      IrisVideoStream.any_instance.stubs(:populate_meta_data).returns true
      IrisVideoStream.any_instance.stubs(:create_index).returns true
      VideoStream.any_instance.stubs(:reindex).returns true
      VideoStream.any_instance.stubs(:update_index).returns true
      ChannelsCard.any_instance.stubs :create_users_management_card
    end

    test 'published live stream and scheduled stream should create activity stream' do
      org = create(:organization)
      creator = create(:user, organization: org)
      live_video_stream = nil
      scheduled_video_stream = nil
      past_video_stream = nil
      pending_video_stream = nil
      perform_enqueued_jobs do
        live_video_stream = create(:iris_video_stream, status: 'live', state: 'published', creator: creator)
        scheduled_video_stream = create(:iris_video_stream, status: 'upcoming', state: 'published', creator: creator)
        past_video_stream = create(:iris_video_stream, status: 'past', state: 'published', creator: creator)
        pending_video_stream = create(:iris_video_stream, status: 'pending', state: 'published', creator: creator)
      end
      assert_equal false, live_video_stream.activity_streams.where(action: ActivityStream::ACTION_CREATED_LIVESTREAM).blank?
      assert_equal live_video_stream.id, live_video_stream.card.activity_streams.first.linkable_item.video_stream.id
      assert_equal false, scheduled_video_stream.activity_streams.where(action: ActivityStream::ACTION_CREATED_LIVESTREAM).blank?
      assert_equal false, past_video_stream.activity_streams.where(action: ActivityStream::ACTION_CREATED_LIVESTREAM).blank?
      assert_equal true, pending_video_stream.activity_streams.where(action: ActivityStream::ACTION_CREATED_LIVESTREAM).blank?
    end

    test 'video stream without a creator should not create a activity stream' do
      org = create(:organization)
      live_video_stream = nil
      perform_enqueued_jobs do
        live_video_stream = create(:iris_video_stream, status: 'live', state: 'published', creator: nil, organization: org)
      end
      assert_equal true, live_video_stream.activity_streams.where(action: ActivityStream::ACTION_CREATED_LIVESTREAM).blank?
    end

    test 'draft video streams should not create activity stream' do
      live_video_stream = nil
      scheduled_video_stream = nil
      past_video_stream = nil
      perform_enqueued_jobs do
        live_video_stream = create(:iris_video_stream, status: 'live', state: 'draft')
        scheduled_video_stream = create(:iris_video_stream, status: 'upcoming', state: 'draft')
        past_video_stream = create(:iris_video_stream, status: 'past', state: 'draft')
      end
      assert_equal true, live_video_stream.activity_streams.where(action: ActivityStream::ACTION_CREATED_LIVESTREAM).blank?
      assert_equal true, scheduled_video_stream.activity_streams.where(action: ActivityStream::ACTION_CREATED_LIVESTREAM).blank?
      assert_equal true, past_video_stream.activity_streams.where(action: ActivityStream::ACTION_CREATED_LIVESTREAM).blank?
    end

    test 'draft video stream should create activity stream when published' do
      org = create(:organization)
      creator = create(:user, organization: org)
      past_draft_video_stream = create(:iris_video_stream, status: 'past', state: 'draft')
      perform_enqueued_jobs do
        past_draft_video_stream.publish!
      end
      assert_equal false, past_draft_video_stream.activity_streams.where(action: ActivityStream::ACTION_CREATED_LIVESTREAM).blank?
    end

    test 'video streams in private channels should not create activity stream' do
      org = create(:organization)
      creator = create(:user, organization: org)
      channel1 = create(:channel, :private, organization: org)
      channel2 = create(:channel, :another_private, organization: org)
      channels = [channel1, channel2]
      video_stream = create(:iris_video_stream, status: 'live', state: 'published', creator: creator)
      video_stream.channels = channels
      perform_enqueued_jobs do
        video_stream.save
      end
      assert_equal true, video_stream.activity_streams.where(action: ActivityStream::ACTION_CREATED_LIVESTREAM).blank?
    end
  end

  class UpVoteActivityStreamTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false
    include ActiveJob::TestHelper
    include CardHelperMethods

    setup do
      ActivityStreamCreateJob.unstub(:perform_later)
      IrisVideoStream.any_instance.stubs(:populate_meta_data).returns true
      IrisVideoStream.any_instance.stubs(:create_index).returns true
      VideoStream.any_instance.stubs(:reindex).returns true
      VideoStream.any_instance.stubs(:update_index).returns true
      VideoStream.any_instance.stubs(:schedule_reminder_push_notification).returns true
      @org = create(:organization)
    end

    test 'upvote on a card should create a activity stream' do
      card = create(:card, author: create(:user, organization: @org))
      user = create(:user, organization: @org)
      vote = nil
      perform_enqueued_jobs do
        card.reload
        vote = Vote.create(votable: card, voter: user)
      end
      assert_equal false, vote.activity_streams.where(action: ActivityStream::ACTION_UPVOTE).blank?
      assert_equal "Card", vote.activity_streams.where(action: ActivityStream::ACTION_UPVOTE).first.linkable_item.class.to_s
      assert_equal card.id, vote.activity_streams.where(action: ActivityStream::ACTION_UPVOTE).first.linkable_item.id
    end

    test 'upvote on a card in a private channel should not create a activity stream' do
      ChannelsCard.any_instance.stubs :create_users_management_card
      user = create(:user, organization: @org)
      channel1 = create(:channel, :private, organization: @org)
      channel2 = create(:channel, :another_private, organization: @org)
      channels = [channel1, channel2]
      card = create(:card, author: create(:user, organization: @org))
      card.channels << channels
      vote = nil
      perform_enqueued_jobs do
        card.reload
        vote = Vote.create(votable: card, voter: user)
      end
      assert_equal true, vote.activity_streams.where(action: ActivityStream::ACTION_UPVOTE).blank?
    end

    test 'upvote on a card inside a pathway should bubble up' do
      setup_one_card_pack
      voter = create(:user, organization: @cover.organization)
      vote = nil
      perform_enqueued_jobs do
        @card1.reload
        vote = Vote.create(votable: @card1, voter: voter)
      end
      assert_equal false, vote.activity_streams.where(action: ActivityStream::ACTION_UPVOTE).blank?
      assert_equal @cover.id, vote.activity_streams.where(action: ActivityStream::ACTION_UPVOTE).first.linkable_item.id
    end

    test 'upvote on a livestream should create a activity stream' do
      video_stream = create(:iris_video_stream, creator: create(:user, organization: @org))
      vote = nil
      perform_enqueued_jobs do
        vote =  create(:vote, votable: video_stream, voter: create(:user, organization: @org))
      end
      assert_equal false, vote.activity_streams.where(action: ActivityStream::ACTION_UPVOTE).blank?
      assert_equal "IrisVideoStream", vote.activity_streams.where(action: ActivityStream::ACTION_UPVOTE).first.linkable_item.class.to_s
      assert_equal video_stream.id, vote.activity_streams.where(action: ActivityStream::ACTION_UPVOTE).first.linkable_item.id
    end

    test 'upvote on a comment should not create activity stream' do
      card = create(:card, author: create(:user, organization: @org))
      card.reload
      comment = create(:comment, commentable: card, user: create(:user, organization: @org))
      vote = nil
      perform_enqueued_jobs do
        vote = create(:vote, votable: comment, voter: create(:user, organization: @org))
      end
      assert_equal true, vote.activity_streams.where(action: ActivityStream::ACTION_UPVOTE).blank?
    end

    test 'upvote on private content should not create a activity stream' do
      ChannelsCard.any_instance.stubs :create_users_management_card
      channel1 = create(:channel, :private, organization: @org)
      channel2 = create(:channel, :another_private, organization: @org)
      channels = [channel1, channel2]
      video_stream = create(:iris_video_stream, status: 'live', state: 'published', creator: create(:user, organization: @org))
      video_stream.channels = channels
      video_stream.save
      vote = nil
      perform_enqueued_jobs do
        vote = create(:vote, votable: video_stream, voter: create(:user, organization: @org))
      end
      assert_equal true, vote.activity_streams.where(action: ActivityStream::ACTION_UPVOTE).blank?
    end

    test 'upvote on post should not create a activity stream' do
      Post.any_instance.stubs(:p_read_by_creator)
      User.any_instance.stubs(:forum_user).returns true
      group, user = create_gu
      post = create(:post, group: group, user: user)
      voter = create(:user, organization: group.organization)
      group.add_member voter
      vote = nil
      perform_enqueued_jobs do
        vote = create(:vote, votable: post, voter: voter)
      end
      assert_equal true, vote.activity_streams.where(action: ActivityStream::ACTION_UPVOTE).blank?
    end

  end

  class CommentActivityStream < ActiveSupport::TestCase
    self.use_transactional_fixtures = false
    include ActiveJob::TestHelper
    include CardHelperMethods

    setup do
      ActivityStreamCreateJob.unstub(:perform_later)
      IrisVideoStream.any_instance.stubs(:populate_meta_data).returns true
      IrisVideoStream.any_instance.stubs(:create_index).returns true
      VideoStream.any_instance.stubs(:reindex).returns true
      VideoStream.any_instance.stubs(:update_index).returns true
      VideoStream.any_instance.stubs(:schedule_reminder_push_notification).returns true
      ChannelsCard.any_instance.stubs :create_users_management_card

      @org = create(:organization)
    end

    test 'comment on card should create a activity stream' do
      card = create(:card, author: create(:user, organization: @org))
      comment = nil
      perform_enqueued_jobs do
        card.reload
        comment = create(:comment, commentable: card, user: create(:user, organization: @org))
      end
      assert_equal false, comment.activity_streams.where(action: ActivityStream::ACTION_COMMENT).blank?
      assert_equal "Card", comment.activity_streams.where(action: ActivityStream::ACTION_COMMENT).first.linkable_item.class.to_s
      assert_equal card.id, comment.activity_streams.where(action: ActivityStream::ACTION_COMMENT).first.linkable_item.id
    end

    test 'comment on card in pathway should not create a activity stream' do
      setup_one_card_pack
      comment = nil
      commentor = create(:user, organization: @cover.organization)
      perform_enqueued_jobs do
        @card1.reload
        comment = create(:comment, commentable: @card1, user: commentor)
      end
      assert_equal true, comment.activity_streams.where(action: ActivityStream::ACTION_COMMENT).blank?
    end

    test 'comment on livestream should not create a activity stream' do
      video_stream = create(:iris_video_stream, creator: create(:user, organization: @org))
      comment = nil
      perform_enqueued_jobs do
        comment = create(:comment, commentable: video_stream, user: create(:user, organization: @org))
      end
      assert_equal false, comment.activity_streams.where(action: ActivityStream::ACTION_COMMENT).blank?
      assert_equal "IrisVideoStream", comment.activity_streams.where(action: ActivityStream::ACTION_COMMENT).first.linkable_item.class.to_s
      assert_equal video_stream.id, comment.activity_streams.where(action: ActivityStream::ACTION_COMMENT).first.linkable_item.id
    end

    test 'comment on post should not create a activity stream' do
      Post.any_instance.stubs(:p_read_by_creator)

      group, user = create_gu
      post = create(:post, group: group, user: user)
      commentor = create(:user, organization: group.organization)
      post.stubs(:can_comment?).returns(true)
      comment = nil
      perform_enqueued_jobs do
        comment = create(:comment, commentable: post, user: commentor)
      end
      assert_equal true, comment.activity_streams.where(action: ActivityStream::ACTION_COMMENT).blank?
    end

    test 'comment on private commentable should not create activity stream' do
      user = create(:user, organization: @org)
      author = create(:user, organization: @org)
      card = create(:card, author: author, organization: @org)
      channel1 = create(:channel, :private, organization: @org)
      channel2 = create(:channel, :another_private, organization: @org)
      channels = [channel1, channel2]
      channels.each { |channel| channel.users << user }
      card.channels << channels
      comment = nil
      perform_enqueued_jobs do
        card.reload
        comment = create(:comment, commentable: card, user: user)
      end
      assert_equal true, comment.activity_streams.where(action: ActivityStream::ACTION_COMMENT).blank?
    end

    test '#remove_team_feed_entry should destroy activity_stream and streamable' do
      user = create(:user, organization: @org)
      team = create(:team, organization: @org)
      comment = create(:comment, commentable: team, user: user)
      as_stream = create(
        :activity_stream, streamable: comment,
        organization: @org, user_id: user.id,
        action: 'comment'
      )
      as_stream.remove_team_feed_entry
      refute ActivityStream.exists?(id: as_stream.id)
      refute Comment.exists?(id: comment.id)
      assert Team.exists?(id: team.id)
    end

    test '#remove_team_feed_entry should not destroy not team_feed activity_streams' do
      # might be deleted only when streamable is comment and
      # commentable Team or Organization
      user = create(:user, organization: @org)
      card = create(:card, author: user)
      as_stream = create(
        :activity_stream, streamable: card,
        organization: @org, user_id: user.id,
        action: 'created_card'
      )
      as_stream.remove_team_feed_entry
      errors = as_stream.errors.messages[:activity_stream]
      expected_error_message = 'Incorrect activity type'
      assert_equal 1, errors.size
      assert_equal expected_error_message, errors.first
      assert Card.exists?(id: card.id), 'Card should not be destroyed'
    end
  end
end

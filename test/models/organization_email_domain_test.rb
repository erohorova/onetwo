require 'test_helper'

class OrganizationEmailDomainTest < ActiveSupport::TestCase
  should belong_to(:organization)

  test "should validate url format" do
    email_domain = OrganizationEmailDomain.new(url: "example")
    assert_equal false, email_domain.valid?

    email_domain = OrganizationEmailDomain.new(url: "example.om")
    assert_equal true, email_domain.valid?
  end
end

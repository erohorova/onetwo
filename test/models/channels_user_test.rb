require 'test_helper'

class ChannelsUserTest < ActiveSupport::TestCase

  test 'remove notification' do
    author = create(:user)
    channel = create(:channel, :robotics, organization: author.organization)
    channel.authors << author
    create(:notification, notifiable: channel, user_id: author.id, notification_type: 'added_author')
    assert_difference -> { Notification.count }, -1 do
      channel.run_callbacks(:commit) { channel.remove_authors([author]) }
    end
  end

  test '#push_collaborator_removed_event should not get invoked if channel does not exists' do
    Organization.any_instance.stubs(:create_admin_user)
    org = create :organization
    user = create :user, organization: org
    # passing non existed channel_id
    channels_user = ChannelsUser.new channel_id: 12, user: user
    Analytics::MetricsRecorderJob.expects(:perform_later).never
    channels_user.destroy
    # test that it is actually deleted ...
    refute channels_user.persisted?
  end

  test '#push_collaborator_added_event calls Analytics::MetricsRecorderJob' do
    Organization.any_instance.stubs(:create_admin_user)
    Analytics::MetricsRecorderJob.unstub(:perform_later)
    org = create :organization
    channel = create :channel, organization: org
    user = create :user, organization: org
    actor = create :user
    stub_time = Time.now
    channels_user = ChannelsUser.new(channel: channel, user: user)
    metadata = { user_id: actor.id, foo: 'bar' }
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    event = Analytics::ChannelCollaboratorRecorder::EVENT_CHANNEL_COLLABORATOR_ADDED
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        recorder: 'Analytics::ChannelCollaboratorRecorder',
        user_id: user.id,
        channel: Analytics::MetricsRecorder.channel_attributes(channel),
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        additional_data: metadata,
        timestamp: stub_time.to_i,
        event: event
      )
    )
    Timecop.freeze(stub_time) do
      channels_user.run_callbacks(:commit) { channels_user.save }
    end
  end

  test '#push_collaborator_removed_event calls Analytics::MetricsRecorderJob' do
    Organization.any_instance.stubs(:create_admin_user)
    Analytics::MetricsRecorderJob.unstub(:perform_later)
    org = create :organization
    channel = create :channel, organization: org
    user = create :user, organization: org
    actor = create :user
    stub_time = Time.now
    channels_user = ChannelsUser.new channel: channel, user: user
    channels_user.stubs(:push_collaborator_added_event)
    channels_user.save
    metadata = { user_id: actor.id, foo: 'bar' }
    event = Analytics::ChannelCollaboratorRecorder::EVENT_CHANNEL_COLLABORATOR_REMOVED
    RequestStore.expects(:read).with(:request_metadata).returns metadata
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: 'Analytics::ChannelCollaboratorRecorder',
      user_id: user.id,
      channel: Analytics::MetricsRecorder.channel_attributes(channel),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      additional_data: metadata,
      timestamp: stub_time.to_i,
      event: event
    )
    Timecop.freeze(stub_time) do
      channels_user.destroy
    end
  end


end

require 'test_helper'

class ConfigTest < ActiveSupport::TestCase

  test '.profile_templates' do
    assert_equal Config.profile_templates, {}
    p1 = create(:config, category: "profile", name: 'course_taken', value: 'v')
    p2 = create(:config, category: "profile", name: 'added_new_competencies', value: 'v')
    p3 = create(:config, category: "profile", name: 'education/summary', value: 'v')
    m1 = create(:config, category: "mail", name: 'test', value: 'v')
    assert_equal Config.profile_templates, {"course_taken"=>"v", "added_new_competencies"=>"v", "education/summary"=>"v"}
    assert_not_includes Config.profile_templates.keys, m1.name
  end

  test 'pushes org_config_edited event on change' do
    org = create :organization
    create :config, {
      category: "profile",
      name: 'member_something_can_do',
      value: 'true',
      configable: org
    }
    actor = create :user, organization: org
    metadata = { user_id: actor.id }
    RequestStore.expects(:read).with(:request_metadata).returns metadata
    stub_time = Time.now
    config_name = "member_something_can_do"
    config = org.configs.find_by name: config_name
    assert_equal config.value, "true"
    Analytics::MetricsRecorderJob.unstub :perform_later

    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        recorder: "Analytics::OrgEditedMetricsRecorder",
        event: "org_config_edited",
        timestamp: stub_time.to_i,
        additional_data: metadata,
        org: Analytics::MetricsRecorder.org_attributes(org),
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        config: Analytics::MetricsRecorder.config_attributes(config),
        changed_column: 'value',
        old_val: "true",
        new_val: "false"
      )
    )
    config.update! value: "false"
    assert_equal config.value, "false"
  end

  test 'pushes group_edited event on change to team config' do
    stub_time = Time.now
    Time.stubs(:now).returns stub_time

    org      = create :organization
    actor    = create :user, organization: org
    team     = create :team, organization: org

    metadata = { user_id: actor.id }
    RequestStore.expects(:read).with(:request_metadata).returns metadata

    config = create :config, {
      name:       'team_carousels',
      value:      'orig val',
      configable: team
    }

    EdcastActiveJob.unstub(:perform_later)
    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries({
        recorder: "Analytics::GroupEditedMetricsRecorder",
        event: "group_edited",
        timestamp: stub_time.to_i,
        additional_data: metadata,
        org: Analytics::MetricsRecorder.org_attributes(org),
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        group: Analytics::MetricsRecorder.team_attributes(team),
        changed_column: 'team_carousels',
        old_val: "orig val",
        new_val: "new val"
      })
    )
    config.update! value: "new val"
  end

  test 'doesnt push event on change to config when event not configured' do
    org = create :organization
    channel = create :channel, organization: org
    config = create :config, {
      category: "profile",
      name: 'channnel_carouself',
      value: 'true',
      configable: channel
    }
    actor = create :user, organization: org
    metadata = { user_id: actor.id }
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    stub_time = Time.now
    assert_equal config.value, "true"
    Analytics::MetricsRecorderJob.unstub :perform_later

    Analytics::MetricsRecorderJob.expects(:perform_later).never
    config.update! value: "false"
    assert_equal config.value, "false"
  end

  test 'pushes org_custom_field_added event on create' do
    TestAfterCommit.enabled = true
    org = create :organization  
    custom_field = create :custom_field, organization: org, abbreviation: "foo", display_name: "bar"
    # if configable is custom field, we get org_custom_field_edited event
    config = custom_field.configs.new(
      id:     1000001,
      name:   "enable_in_filters",
      value:  "true"
    )
    actor = create :user, organization: org
    metadata = { user_id: actor.id }
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    stub_time = Time.now

    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        recorder: "Analytics::OrgCustomFieldRecorder",
        event: "org_custom_field_added",
        timestamp: stub_time.to_i,
        additional_data: metadata,
        org: Analytics::MetricsRecorder.org_attributes(org),
        custom_field: Analytics::MetricsRecorder.custom_field_attributes(custom_field),
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        config: Analytics::MetricsRecorder.config_attributes(config)
      )
    )
    Timecop.freeze(stub_time) do
      config.save
    end 
  end
  test 'pushes org_custom_field_edited event on change' do
    org = create :organization  
    custom_field = CustomField.create!(
      organization: org,
      abbreviation: "foo",
      display_name: "bar"
    )
    # if configable is custom field, we get org_custom_field_edited event
    config = custom_field.configs.create!(
      name: "enable_in_filters",
      value: "true"
    )
    actor = create :user, organization: org
    metadata = { user_id: actor.id }
    RequestStore.expects(:read).with(:request_metadata).returns metadata
    stub_time = Time.now

    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        recorder: "Analytics::OrgCustomFieldRecorder",
        event: "org_custom_field_edited",
        timestamp: stub_time.to_i,
        additional_data: metadata,
        org: Analytics::MetricsRecorder.org_attributes(org),
        custom_field: Analytics::MetricsRecorder.custom_field_attributes(custom_field),
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        config: Analytics::MetricsRecorder.config_attributes(config),
        changed_column: 'value',
        old_val: "true",
        new_val: "false"
      )
    )
    config.update! value: "false"
    assert_equal config.value, "false"
  end

  test " on creation of OktaApiToken config okta groups should be created" do
    stub_okta_group_creation

    org = create(:organization)
    Config.any_instance.expects(:create_okta_group_config).once
    oktaapitoken = create(:config, configable: org, name: "OktaApiToken", value: "abc", data_type: "String")
  end

  test "on creation of normal config no okta groups config should be created" do
    org = create(:organization)
    Config.any_instance.expects(:create_okta_group_config).never
    oktaapitoken = create(:config, configable: org, name: "abc", value: "abc", data_type: "String")
  end

  test " on creation of OktaApiToken config okta application should be created" do
    stub_okta_group_creation

    org = create(:organization)
    Config.any_instance.expects(:create_okta_app).once
    oktaapitoken = create(:config, configable: org, name: "OktaApiToken", value: "abc", data_type: "String")
  end

  test "on creation of normal config no okta groups application should be created" do
    org = create(:organization)
    Config.any_instance.expects(:create_okta_app).never
    oktaapitoken = create(:config, configable: org, name: "abc", value: "abc", data_type: "String")
  end

  test 'guide me config with encrypted org key' do
    org = create(:organization)
    config = create(:config, name: 'guide_me_config', value: {
      edition: "ent", guide_me_enabled: true, org_key: "a77a159aa", org_secret: "9IDPLE05"
    }.to_json, data_type: 'json', configable: org)

    guide_me_config = config.get_guide_me_value

    assert_equal 'ent', guide_me_config['edition']
    assert guide_me_config['encrypted_org_key']
    assert_equal "a77a159aa", guide_me_config['org_key']
    refute guide_me_config['org_secret']
  end

  test 'guide me config updated with empty secret key it should take previous secret value' do
    org = create(:organization)
    config = create(:config, name: 'guide_me_config', value: {
      edition: "ent", guide_me_enabled: true, org_key: "a77a159aa", org_secret: "9IDPLE05"
    }.to_json, data_type: 'json', configable: org)

    config.value = {edition: "ent", guide_me_enabled: true, org_key: "939UE83WE"}.to_json
    config.save

    value = JSON.parse(config.reload.value)
    assert_equal '9IDPLE05', value['org_secret']
  end

end

require 'test_helper'

class OrderTest < ActiveSupport::TestCase

  should have_db_column(:orderable_id).of_type(:integer)
  should have_db_column(:orderable_type).of_type(:string)
  should have_db_column(:user_id).of_type(:integer)
  should have_db_column(:organization_id).of_type(:integer)
  should have_db_column(:state).of_type(:integer)

  should belong_to(:orderable)
  should belong_to(:user)
  should belong_to(:organization)

  should have_many(:order_transactions).class_name('Transaction')
  should have_many(:user_purchases)

  should validate_presence_of(:orderable_id)
  should validate_presence_of(:orderable_type)
  should validate_presence_of(:user_id)
  should validate_presence_of(:organization_id)
  should validate_presence_of(:state)

  test 'should throw uniqueness validation error if user has active card subscription for orderable type Card' do
    user = create(:user)
    card = create(:card)
    order = create(:order, orderable: card, user: user, state: 1)
    transaction = create(:transaction, order: order)
    create(:card_subscription, card_id: card.id, user_id: user.id, transaction_id: transaction.id, 
                               start_date: Date.today - 2.days, end_date: Date.today + 2.days)
    assert_raise DuplicateOrderError do
      order2 = create(:order, orderable: card, user: user)
    end
  end
    
  test 'should throw uniqueness validation error if user has no card subscriptions yet' do
    user = create(:user)
    card = create(:card)
    order = create(:order, orderable: card, user: user)
    transaction = create(:transaction, order: order)
    assert_raise DuplicateOrderError do
      order2 = create(:order, orderable: card, user: user)
    end
  end

  test 'should allow order creation if card subscription has expired and there are no other unprocessed order records' do
    user = create(:user)
    card = create(:card)
    order = create(:order, orderable: card, user: user, state: 2)
    order1 = create(:order, orderable: card, user: user, state: 1)
    transaction = create(:transaction, order: order)
    create(:card_subscription, card_id: card.id, user_id: user.id, transaction_id: transaction.id, 
                               start_date: Date.today - 2.days, end_date: Date.today - 1.days)
    order2 = create(:order, orderable: card, user: user)
    assert order2.present?
    assert_raise DuplicateOrderError do
      order3 = create(:order, orderable: card, user: user)
    end
  end

  test 'should allow order creation if previous order was in error state' do
    user = create(:user)
    card = create(:card)
    order1 = create(:order, orderable: card, user: user, state: 2)  
    order2 = create(:order, orderable: card, user: user)
    assert order2.present?
    assert_raise DuplicateOrderError do
      order3 = create(:order, orderable: card, user: user)
    end
  end

  test 'validate uniqueness for orderable_type OrgSubscription' do
    user = create(:user)
    organization = create(:organization)
    org_subscription = create(:org_subscription, organization: organization)
    order = create(:order, orderable: org_subscription, user: user, state: 1)
    assert_raise DuplicateOrderError do
      order3 = create(:order, orderable: org_subscription, user: user)
    end
  end

  test 'skip uniqueness validation for orderable_type Recharge' do
    user = create(:user)
    recharge = create(:recharge)
    order1 = create(:order, orderable: recharge, user: user)
    order2 = create(:order, orderable: recharge, user: user)
    assert order2.present?
  end

  test '#initialize order must fetch the same order record if payment is unprocessed for card' do
    org = create(:organization)
    user = create(:user, organization: org)
    card = create(:card)
    order = create(:order, orderable: card, user: user, state: 0, organization: org)
    transaction = create(:transaction, order: order, payment_state: 0)
    args = {
      orderable: card,
      user_id: user.id,
      organization_id: org.id
    }
    order2 = Order.initialize_order!(args: args, orderable_type: 'Card')
    assert_equal order.id, order2.id
  end

  test '#initialize order must create a new order record if payment had failed for card' do
    org = create(:organization)
    user = create(:user, organization: org)
    card = create(:card)
    order = create(:order, orderable: card, user: user, state: 2, organization: org)
    transaction = create(:transaction, order: order, payment_state: 2)
    args = {
      orderable: card,
      user_id: user.id,
      organization_id: org.id
    }
    order2 = Order.initialize_order!(args: args, orderable_type: 'Card')
    assert_not_equal order.id, order2.id
  end

  test '#initialize order must create a new order record if payment was successful for card and subscription expired' do
    org = create(:organization)
    user = create(:user, organization: org)
    card = create(:card)
    order = create(:order, orderable: card, user: user, state: 1, organization: org)
    transaction = create(:transaction, order: order, payment_state: 1)
    create(:card_subscription, card_id: card.id, user_id: user.id, start_date: Date.today - 2.days, end_date: Date.today - 1.days)
    args = {
      orderable: card,
      user_id: user.id,
      organization_id: org.id
    }
    assert_not_equal order.id, Order.initialize_order!(args: args, orderable_type: 'Card').id
  end

  test 'create order' do
    organization = create(:organization)
    user = create(:user, organization: organization)
    org_subscription = create :org_subscription, organization: organization

    order1 = Order.initialize_order!(
      args: { orderable: org_subscription,
        user_id: user.id,
        organization_id: organization.id 
      },
      orderable_type: 'OrgSubscription')

    order2 = Order.initialize_order!(
      args: { orderable: org_subscription,
        user_id: user.id,
        organization_id: organization.id
      },
      orderable_type: 'OrgSubscription')

    assert_not_nil order1
    assert_not_nil order2
    assert_equal order1.id, order2.id
  end
end

require 'test_helper'

class UserOnboardingTest < ActiveSupport::TestCase

  should belong_to(:user)

  class TransitionTest < ActiveSupport::TestCase
    setup do
      @user = create :user, :with_onboarding_complete
      @onboarding = create :user_onboarding, user: @user

      @user.update(is_complete: false)
      @user.reload
    end

    test '#new is the default state' do
      assert_equal 'new', @onboarding.status
    end

    test 'smooth transition from #new -> #started' do
      assert_equal 'new', @onboarding.status
      @onboarding.start!

      assert_equal 'started', @onboarding.reload.status
    end

    test 'reset event' do
      assert_equal 'new', @onboarding.status
      refute @onboarding.may_reset?
      @onboarding.start!
      assert @onboarding.may_reset?
      @onboarding.reset!
      assert_equal 'new', @onboarding.status
      @onboarding.start!
      @onboarding.complete!
      assert @onboarding.user.is_complete
      assert @onboarding.may_reset?
      @onboarding.reset!
      refute @onboarding.user.is_complete
      assert_equal 'new', @onboarding.status
    end

    test 'smooth transition from #started -> #completed' do
      @onboarding.start!
      @onboarding.complete!

      assert_equal 'completed', @onboarding.reload.status
    end

    test 'no reverse transition to previous transition once completed!' do
      @onboarding.start!
      @onboarding.complete!

      assert_raises AASM::InvalidTransition do
        @onboarding.start!
      end
    end

    test 'sets `completed_at` when completed' do
      @onboarding.start!
      @onboarding.complete!

      assert_not_nil @onboarding.reload.completed_at
    end

    test 'sets `is_complete` if user completes onboarding' do
      refute @user.is_complete

      @onboarding.start!
      @onboarding.complete!

      assert @user.reload.is_complete
    end
  end

  class UserOnboardingRecorderTest < ActiveSupport::TestCase
    test "should push to influx user onboarding created event when user onboarding is created" do
      TestAfterCommit.enabled = true
      org = create :organization
      user = create(:user, organization: org, organization_role: 'member')
      actor = create(:user, organization: org, organization_role: 'admin')

      user_onboarding = UserOnboarding.new(id: 10000001 , user: user)
      stub_time = Time.now
      metadata = { user_id: actor.id }    
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries(
          recorder:         "Analytics::UserOnboardingRecorder",
          event:            "user_onboarding_created",
          actor:            Analytics::MetricsRecorder.user_attributes(actor),
          org:              Analytics::MetricsRecorder.org_attributes(org),
          user_onboarding:  Analytics::MetricsRecorder.user_onboarding_attributes(user_onboarding),
          timestamp:        stub_time.to_i, 
          additional_data:  metadata
          )
        )
      Timecop.freeze(stub_time) do
        user_onboarding.save
      end    
    end
    test "should push to influx user onboarding started event when user onboarding started" do
      TestAfterCommit.enabled = true
      org = create :organization
      user = create(:user, organization: org, organization_role: 'member')
      actor = create(:user, organization: org, organization_role: 'admin')

      user_onboarding = UserOnboarding.create(id: 10000001 , user: user)
      stub_time = Time.now
      metadata = { user_id: actor.id }    
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries(
          recorder:         "Analytics::UserOnboardingRecorder",
          event:            "user_onboarding_started",
          actor:            Analytics::MetricsRecorder.user_attributes(actor),
          org:              Analytics::MetricsRecorder.org_attributes(org),
          user_onboarding:  Analytics::MetricsRecorder.user_onboarding_attributes(user_onboarding).merge!('status' => 'started'),
          timestamp:        stub_time.to_i, 
          additional_data:  metadata
          )
        )
      Timecop.freeze(stub_time) do
        user_onboarding.start!
      end    
    end

    test "should push to influx user onboarding completed event when user onboarding completed" do
      TestAfterCommit.enabled = true
      org = create :organization
      user = create(:user, organization: org, organization_role: 'member')
      actor = create(:user, organization: org, organization_role: 'admin')

      user_onboarding = UserOnboarding.create(id: 10000001 , user: user, status: 'started')
      stub_time = Time.now
      metadata = { user_id: actor.id }    
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries(
          recorder:         "Analytics::UserOnboardingRecorder",
          event:            "user_onboarding_completed",
          actor:            Analytics::MetricsRecorder.user_attributes(actor),
          org:              Analytics::MetricsRecorder.org_attributes(org),
          user_onboarding:  Analytics::MetricsRecorder.user_onboarding_attributes(user_onboarding).merge!('current_step' => 3, 'status' => 'completed'),
          timestamp:        stub_time.to_i, 
          additional_data:  metadata
          )
        )
      Timecop.freeze(stub_time) do
        user_onboarding.complete!
      end    
    end
  end
end

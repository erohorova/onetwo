require 'test_helper'

# DEPRECATED
class SchoolTest < ActiveSupport::TestCase
  # should belong_to(:user)
  # should validate_presence_of(:name)
  # should validate_presence_of(:specialization)
  # should validate_presence_of(:from_year)
  # should validate_presence_of(:to_year)
  # should validate_presence_of(:user_id)

  # test 'should attach images for school' do
  #   school = create(:education, image: Rack::Test::UploadedFile.new('test/fixtures/images/architecture.jpg', 'image/jpg'))
  #   assert_includes school.image(:small), 'architecture'
  # end

  # test 'from and to year validation' do
  #   school = build :education, from_year: "2006", to_year: "2012"
  #   assert school.valid?
  #   school.from_year = "invalid"
  #   refute school.valid?
  #   assert_equal school.errors.full_messages, ["From year should be in YYYY format", "To year should be same or after the From Year"]
  # end

  # test 'to year should be greater than from year' do
  #   school = build :education, from_year: '2008', to_year: '2006'
  #   refute school.valid?
  #   assert_equal school.errors.full_messages, ['To year should be same or after the From Year']
  #   school.to_year = '2008'
  #   assert school.valid?
  #   school.to_year = '2010'
  #   assert school.valid?
  # end

  # test 'from_year and to_year prevents unwanted values' do
  #   school = build :education, from_year: "bug2006bug", to_year: "bug2012bug"
  #   refute school.valid?
  # end
end

require 'test_helper'

class CustomFieldTest < ActiveSupport::TestCase
  should belong_to(:organization)
  should validate_presence_of(:abbreviation)
  should validate_presence_of(:display_name)

  test 'validates presence of organization' do
    custom_field = CustomField.new(
      organization_id: 999999,
      display_name: 'foo',
      abbreviation: 'f'
    )
    expected_error = "Organization can't be blank"
    refute custom_field.valid?
    assert custom_field.errors.full_messages.include?(expected_error)
  end

  test 'assign abbreviation if blank by display_name' do
    organization = create :organization

    cf1 = create :custom_field, display_name: 'Mobile Phone 2', abbreviation: nil, organization: organization
    assert_equal 'mobile_phone_2', cf1.abbreviation

    cf1.update(display_name: 'Updated Mobile Phone 2')
    assert_equal 'updated_mobile_phone_2', cf1.reload.abbreviation

    cf1.update(display_name: 'UpdatedMobilePhone2')
    assert_equal 'updated_mobile_phone2', cf1.reload.abbreviation

    cf1.update(display_name: 'Updatedmobilephone2')
    assert_equal 'updatedmobilephone2', cf1.reload.abbreviation
  end

  test 'default_scope lists all custom fields created_at DESC' do
    organization = create :organization
    cf1 = create :custom_field, display_name: 'Mobile Phone 2', abbreviation: nil, organization: organization, created_at: Date.today - 3.days
    cf2 = create :custom_field, display_name: 'Secondary Email', abbreviation: nil, organization: organization, created_at: Date.today - 1.day
    cf3 = create :custom_field, display_name: 'Total Experience', abbreviation: nil, organization: organization, created_at: Date.today

    assert_equal organization.reload.custom_fields.to_set, [cf3, cf2, cf1].to_set
  end

  test 'pushes event on creation' do
    time = Time.now
    org = create :organization
    admin = create :user, organization: org, organization_role: "admin"
    # This next line is only necessary for when tests are run on travis.
    RequestStore.stubs(:read).with(:request_metadata).returns({
      user_id: admin.id
    })
    custom_field = create(:custom_field, organization: org, display_name: 'foo')
    # This is so we can expect what id the method is called with:
    custom_field.stubs(:attributes).returns(display_name: "foo", id: custom_field.id)
    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: 'Analytics::OrgCustomFieldRecorder',
      org: Analytics::MetricsRecorder.org_attributes(org),
      event: "org_custom_field_added",
      actor: Analytics::MetricsRecorder.user_attributes(admin),
      timestamp: time.to_i,
      additional_data: { user_id: admin.id },
      custom_field: Analytics::MetricsRecorder.custom_field_attributes(custom_field)
    )
    Timecop.freeze(time) do
      custom_field.run_callbacks(:commit)
    end
  end

  test 'pushes event on deletion' do
    time = Time.now
    org = create :organization
    admin = create :user, organization: org, organization_role: "admin"
    custom_field = CustomField.create!(organization: org, display_name: "foo")
    # This next line is only necessary for when tests are run on travis.
    RequestStore.unstub(:read)
    RequestStore.stubs(:read).with(:request_metadata).returns({
      user_id: admin.id
    })
    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub :perform_later
    expected_args = {
      recorder: "Analytics::OrgCustomFieldRecorder",
      org: Analytics::MetricsRecorder.org_attributes(org),
      event: "org_custom_field_removed",
      actor: Analytics::MetricsRecorder.user_attributes(admin),
      timestamp: time.to_i,
      additional_data: {user_id: admin.id},
      custom_field: Analytics::MetricsRecorder.custom_field_attributes(custom_field)
    }
    Analytics::MetricsRecorderJob.expects(:perform_later).with(expected_args)
    Timecop.freeze(time) do
      custom_field.destroy!
    end
  end

  test 'pushes event on update' do
    time = Time.now
    org = create :organization
    admin = create :user, organization: org, organization_role: "admin"
    custom_field = CustomField.create!(organization: org, display_name: "foo")
    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub :perform_later
    RequestStore.stubs(:read).with(:request_metadata).returns({
      user_id: admin.id
    })
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::OrgCustomFieldRecorder",
      org: Analytics::MetricsRecorder.org_attributes(org),
      event: "org_custom_field_removed",
      actor: Analytics::MetricsRecorder.user_attributes(admin),
      timestamp: time.to_i,
      additional_data: {user_id: admin.id},
      custom_field: Analytics::MetricsRecorder.custom_field_attributes(custom_field)
    )
    Timecop.freeze(time) do
      custom_field.destroy!
    end
  end

  test 'doesnt push event for update on non-whitelisted columns' do
    org = create :organization
    admin = create :user, organization: org, organization_role: "admin"
    custom_field = CustomField.create!(organization: org, display_name: "foo")
    unstub_background_jobs
    Analytics::MetricsRecorderJob.expects(:perform_later).never
    time = Time.now
    custom_field.update! created_at: time
    assert_equal time.to_i, custom_field.reload.created_at.to_i
  end

  test 'should validate maximum length of display name' do
    org = create(:organization)
    name = 'very long long long long long long long long long long long long name'
    custom_field = CustomField.new(
      organization: org,
      display_name: name
    )
    refute custom_field.valid?
    expected_errors = [
      'Display name is too long (maximum is 64 characters)',
      'Abbreviation is too long (maximum is 64 characters)'
    ]
    assert_same_elements custom_field.errors.messages.values.flatten, expected_errors
  end
end

require 'test_helper'

class LinkedinProviderTest < ActiveSupport::TestCase

  setup do
    user = create(:user)
    group = create(:resource_group)
    group.add_member user
    @question = create(:question, user: user, group: group)
  end

  test 'should post to linkedin' do
    identity_provider = create(:identity_provider, :linkedin)

    linkedin_provider = LinkedinProvider.find(identity_provider.id)
    mocked_response = '{
    "updateKey": "UPDATE-29795450-5866314242084331520",
        "updateUrl": "http://www.linkedin.com/updates?discuss=&scope=29795450&stype=M&topic=5866314242084331520&type=U&a=2QKv"
    }'

    stub_request(:post, "https://api.linkedin.com/v1/people/~/shares?oauth2_access_token=#{linkedin_provider.token}").
        with(:body => "{\"visibility\":{\"code\":\"anyone\"},\"comment\":\"testing messing\",\"content\":{\"title\":\"Question\",\"description\":\"This is a question\",\"submitted_url\":#{@question.ref_url.to_json}}}",
             :headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Content-Type'=>'application/json', 'User-Agent'=>/.*/, 'X-Li-Format'=>'json'}).
        to_return(:status => 200, :body => mocked_response, :headers => {})




    endpoint = linkedin_provider.share(message: 'testing messing', object: @question)

    assert_equal 'http://www.linkedin.com/updates?discuss=&scope=29795450&stype=M&topic=5866314242084331520&type=U&a=2QKv', endpoint
  end

  test 'should determine scopes' do
    li = create :linkedin_provider, uid: '123', user: create(:user), token_scope: nil

    assert li.scoped?(:network)

    li.update(token_scope: 'rw_nus')

    assert li.scoped?(:share)
    assert_not li.scoped?(:network)
  end
end

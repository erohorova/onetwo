require 'test_helper'
class UserNotificationReadTest < ActiveSupport::TestCase
  should validate_presence_of(:user)
  should validate_presence_of(:seen_at)
end

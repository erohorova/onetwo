require 'test_helper'

class ClientTest < ActiveSupport::TestCase
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
  end

  should have_many :credentials
  should validate_presence_of :name
  should belong_to :user
  should have_one :mailer_config
  should have_one :webex_config

  test 'should create a default credential on create' do
    client = build(:client)
    assert_empty(client.credentials)
    client.save!
    assert_not_empty(client.credentials)
    assert_equal 1, client.credentials.count
    assert_no_difference -> {client.credentials.count} do
      client.update! name: 'new_name'
    end
  end

  test 'first credential name should be sandbox' do
    client = build(:client, name: 'test')
    client.save!
    assert_equal 'sandbox', client.valid_credential.label
  end

  test 'should add user to admins list' do
    user = create(:user)
    client = create(:client, user: user)
    assert_contains client.admins, user
  end

end

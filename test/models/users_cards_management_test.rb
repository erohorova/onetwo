require 'test_helper'

class UsersCardsManagementTest < ActiveSupport::TestCase
  should belong_to :user
  should belong_to :card

  setup do
    @org = create(:organization)
    @user = create(:user, organization: @org)
    @author = create(:user, organization: @org)
    @card, @card2 = create_list(:card, 2, author: @author, organization: @org)
  end

  test 'should be able to create ucm with valid params' do
    umc = UsersCardsManagement.new(card: @card, action: 'added_to_pathway', user: @user, target_id: 1)
    assert umc.valid?
    assert_difference -> { UsersCardsManagement.count }, 1 do
      umc.save
    end
  end

  test 'should validate uniqueness of card in scope user and target_id' do
    umc = UsersCardsManagement.new(card: @card, action: 'added_to_pathway', user: @user, target_id: 1)
    assert umc.valid?
    umc.save

    umc2 = UsersCardsManagement.new(card: @card, action: 'added_to_pathway', user: @user, target_id: 1)
    refute umc2.valid?
    umc3 = UsersCardsManagement.new(card: @card, action: 'added_to_pathway', user: @user, target_id: 2)
    assert umc3.valid?
  end

  test 'should not be able to create ucm with nil card or user' do
    umc = UsersCardsManagement.new(card: @card, action: 'added_to_pathway', user: nil, target_id: 1)
    umc2 = UsersCardsManagement.new(card: nil, action: 'added_to_pathway', user: @user, target_id: 1)
    refute umc.valid?
    refute umc2.valid?
  end

  test 'should not be able to create ucm with not valid action' do
    umc = UsersCardsManagement.new(card: @card, action: 'sample_action', user: nil, target_id: 1)
    refute umc.valid?
  end
end

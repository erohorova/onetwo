require 'test_helper'

class PartnerCenterTest < ActiveSupport::TestCase

  setup do
    TestAfterCommit.enabled = true
    @org = create :organization
    @user = create :user, organization: @org
    @partner_center = PartnerCenter.new(
      organization: @org,
      user:         @user,
      enabled:      true,
      description:  "OK",
      embed_link:   "link123",
      integration:  "foobar"
    )

    @stub_time = Time.now
    Time.stubs(:now).returns @stub_time
    RequestStore.store[:request_metadata] = { user_id: @user.id }
  end

  test 'records create event' do
    EdcastActiveJob.any_instance.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub(:perform_later)

    Analytics::MetricsRecorderJob.expects(:perform_later).with(has_entries({
      event: "org_partner_center_created",
      recorder: "Analytics::OrgMetricsRecorder",
      org: Analytics::MetricsRecorder.org_attributes(@org),
      actor: Analytics::MetricsRecorder.user_attributes(@user),
      partner_center: has_entries(Analytics::MetricsRecorder.partner_center_attributes(@partner_center).compact),
      timestamp: @stub_time.to_i,
      additional_data: { user_id: @user.id }
    }))

    @partner_center.save!
  end

  test 'records delete event' do
    @partner_center.save!

    EdcastActiveJob.any_instance.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub(:perform_later)

    Analytics::MetricsRecorderJob.expects(:perform_later).with(has_entries({
      event: "org_partner_center_deleted",
      recorder: "Analytics::OrgMetricsRecorder",
      org: Analytics::MetricsRecorder.org_attributes(@org),
      actor: Analytics::MetricsRecorder.user_attributes(@user),
      partner_center: has_entries(Analytics::MetricsRecorder.partner_center_attributes(@partner_center).compact),
      timestamp: @stub_time.to_i,
      additional_data: { user_id: @user.id }
    }))

    @partner_center.destroy
  end

  test 'records edit event' do
    @partner_center.save!

    EdcastActiveJob.any_instance.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub(:perform_later)

    Analytics::MetricsRecorderJob.expects(:perform_later).with(has_entries({
      event: "org_partner_center_edited",
      recorder: "Analytics::OrgPartnerCenterEditedMetricsRecorder",
      org: Analytics::MetricsRecorder.org_attributes(@org),
      actor: Analytics::MetricsRecorder.user_attributes(@user),
      partner_center: Analytics::MetricsRecorder.partner_center_attributes(@partner_center).merge(
        "description" => "OK2" 
      ),
      timestamp: @stub_time.to_i,
      additional_data: { user_id: @user.id },
      changed_column: "description",
      old_val: "OK",
      new_val: "OK2"
    }))

    @partner_center.update description: "OK2"
  end

  test 'skips edit event for ignored attrs' do
    @partner_center.save!

    EdcastActiveJob.any_instance.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub(:perform_later)

    Analytics::MetricsRecorderJob.expects(:perform_later).never

    @partner_center.update updated_at: Time.now + 5.minutes
  end
end

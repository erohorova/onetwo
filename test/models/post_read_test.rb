require 'test_helper'

class PostReadTest < ActiveSupport::TestCase

  class SlowTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false

    teardown do
      WebMock.enable!
    end

    setup do
      skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
      WebMock.disable!
      Post.any_instance.unstub :p_read_by_creator
      @org = create(:organization)
      @user, @u1, @u2, @u3 = create_list(:user, 4, organization: @org)
      @client = create(:client, organization: @org)
      @group = create(:resource_group, client: @client, organization: @org)

      @group.add_member @user
      @group.add_member @u1
      @group.add_member @u2
      @group.add_member @u3
      @p1 = create(:post, user: @u1, group: @group)
      @p2 = create(:post, user: @u2, group: @group)
      @p3 = create(:post, user: @u3, group: @group)
    end

    test 'user not in group cannot read post' do
      u = create(:user)
      pr = PostRead.new(user_id: u.id, group_id: @group.id, post_id: @p1.id)
      assert_not pr.save
      assert_not_empty pr.errors
    end

    test 'get reads for user' do
      PostRead.create(user_id: @user.id, group_id: @group.id, post_id: @p1.id)
      PostRead.create(user_id: @user.id, group_id: @group.id, post_id: @p2.id)

      PostRead.create(user_id: @u1.id, group_id: @group.id, post_id: @p2.id)

      PostRead.gateway.refresh_index!

      reads = PostRead.get_reads([@p1.id, @p2.id, @p3.id], @user)
      assert_not reads.key?(@p3.id)
      assert reads.key?(@p1.id)
      assert reads.key?(@p2.id)

      reads_empty = PostRead.get_reads([], @user)
      assert_equal reads_empty, {}
    end

    test 'should work with more than 10 post reads' do
      post_ids = (1..15).to_a.map do |i|
        post = create(:post, user: @user, group: @group)
        PostRead.create(id: i, user_id: @u1.id, group_id: @group.id, post_id: post.id)
        post.id
      end
      PostRead.gateway.refresh_index!
      reads = PostRead.get_reads(post_ids, @u1)
      assert_equal post_ids.length, reads.keys.length
    end

    test 'should get the reads count' do

      PostRead.create(user_id: @user.id, group_id: @group.id, post_id: @p1.id)
      PostRead.create(user_id: @u1.id, group_id: @group.id, post_id: @p1.id)
      PostRead.create(user_id: @u2.id, group_id: @group.id, post_id: @p1.id)
      PostRead.create(user_id: @user.id, group_id: @group.id, post_id: @p2.id)
      PostRead.create(user_id: @user.id, group_id: @group.id, post_id: @p3.id)

      PostRead.gateway.refresh_index!

      assert_equal 4, @p1.reads_count #4 because of the inclusion of the author
    end

    test 'should test read_by? instance method' do

      PostRead.create(user_id: @u1.id, group_id: @group.id, post_id: @p1.id)
      PostRead.create(user_id: @u2.id, group_id: @group.id, post_id: @p1.id)


      PostRead.gateway.refresh_index!

      assert @p1.read_by?(@u1)
      assert @p1.read_by?(@u2)
      assert_not @p1.read_by?(@u3)

    end

    test 'should test read_by? class method' do

      PostRead.create(user_id: @u1.id, group_id: @group.id, post_id: @p1.id)
      PostRead.create(user_id: @u2.id, group_id: @group.id, post_id: @p1.id)


      PostRead.gateway.refresh_index!

      assert PostRead.read_by?(user: @u1, post: @p1)
      assert PostRead.read_by?(user: @u2, post: @p1)
      assert_not PostRead.read_by?(user: @u3, post: @p1)

    end

    test 'should get the reads count in by a user in a give group' do
      PostRead.create(user_id: @user.id, group_id: @group.id, post_id: @p1.id)
      PostRead.create(user_id: @u1.id, group_id: @group.id, post_id: @p1.id)
      PostRead.create(user_id: @u2.id, group_id: @group.id, post_id: @p1.id)
      PostRead.create(user_id: @user.id, group_id: @group.id, post_id: @p2.id)
      PostRead.create(user_id: @user.id, group_id: @group.id, post_id: @p3.id)

      group2 = create(:resource_group)
      group2.add_member @user
      group2.add_member @u1
      group2.add_member @u2
      group2.add_member @u3
      p1 = create(:post, user: @u1, group: group2)
      p2 = create(:post, user: @u2, group: group2)
      p3 = create(:post, user: @u3, group: group2)
      PostRead.create(user_id: @user.id, group_id: group2.id, post_id: p1.id)
      PostRead.create(user_id: @u1.id, group_id: group2.id, post_id: p1.id)
      PostRead.create(user_id: @u2.id, group_id: group2.id, post_id: p1.id)

      PostRead.gateway.refresh_index!
      assert_equal 3, PostRead.reads_count(user_id: @user.id, group_id: @group.id)
      assert_equal 1, PostRead.reads_count(user_id: @user.id, group_id: group2.id)
    end

    test 'ensure all the post reads are delete if the post is destroyed' do

      PostRead.create(user_id: @user.id, group_id: @group.id, post_id: @p1.id)
      PostRead.create(user_id: @u1.id, group_id: @group.id, post_id: @p1.id)
      PostRead.create(user_id: @u2.id, group_id: @group.id, post_id: @p1.id)
      PostRead.create(user_id: @user.id, group_id: @group.id, post_id: @p2.id)
      PostRead.create(user_id: @user.id, group_id: @group.id, post_id: @p3.id)


      PostRead.gateway.refresh_index!

      assert_difference -> {PostRead.count}, -4 do #4 because of the inclusion of the author
        @p1.destroy
        PostRead.gateway.refresh_index!
      end

    end
  end
end

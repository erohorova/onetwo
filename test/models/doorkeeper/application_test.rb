require 'test_helper'

class Doorkeeper::ApplicationTest < ActiveSupport::TestCase

  test 're-use clients table' do
    org = create(:organization)
    client = create(:client, organization: org)
    assert_equal 'clients', Doorkeeper::Application.table_name
    assert_equal client.name, Doorkeeper::Application.first.name
    assert_equal client.id, Doorkeeper::Application.first.id
  end

  test 'should reuse the credentials table' do
    org = create(:organization)
    client = create(:client, organization: org)

    credential = create(:credential, client: client)

    application = Doorkeeper::Application.by_uid_and_secret(credential.api_key, credential.shared_secret)

    assert_equal client.id, application.id

    application = Doorkeeper::Application.by_uid(credential.api_key)

    assert_equal client.id, application.id
  end
end
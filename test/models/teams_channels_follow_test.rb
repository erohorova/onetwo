require 'test_helper'

class TeamsChannelsFollowTest < ActiveSupport::TestCase
  should belong_to :team
  should belong_to :channel

  should validate_presence_of(:channel_id)
  should validate_presence_of(:team_id)

  test 'uniqness validation' do
    team = create(:team)
    channel = create(:channel)
    team_channels = create(:teams_channels_follow, team: team, channel: channel)

    dup_team_channels = build(:teams_channels_follow, team: team, channel: channel)
    refute dup_team_channels.valid?
    assert_equal dup_team_channels.full_errors, "Team has already been taken"
  end

  test 'should invoke indexing job after commit' do
    TestAfterCommit.with_commits do
      TeamsChannelsFollow.any_instance.expects(:reindex_team).once
      TeamsChannelsFollow.any_instance.expects(:reindex_channel).once
      org = create(:organization)
      team = create(:team, organization: org)
      channel = create(:channel, organization: org)
      create(:teams_channels_follow, team: team, channel: channel)
    end
  end
end

class TeamsChannelsFollowEventTest < ActiveSupport::TestCase
  self.use_transactional_fixtures = false

  test "should record event when team follows channel" do
    stub_time

    org = create(:organization)
    admin = create(:user, organization: org)
    team = create(:team, organization: org)
    channel = create(:channel, organization: org)
    team.add_admin admin

    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub :perform_later
    additional_data = { user_id: admin.id, platform: 'iOS', user_agent: 'Edcast iPhone' }
    RequestStore.stubs(:read).with(:request_metadata).returns(
      additional_data
    )

    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        event: 'group_channel_followed',
        recorder: 'Analytics::GroupChannelsMetricsRecorder',
        group:  Analytics::MetricsRecorder.team_attributes(team),
        channel:  Analytics::MetricsRecorder.channel_attributes(channel),
        org:  Analytics::MetricsRecorder.org_attributes(org),
        actor: Analytics::MetricsRecorder.user_attributes(admin),
        timestamp: Time.now.to_i,
        additional_data: additional_data
      )
    )

    create(:teams_channels_follow, team: team, channel: channel)
  end

  test "should record event when team unfollow channel" do
    stub_time

    org = create(:organization)
    admin = create(:user, organization: org)
    team = create(:team, organization: org)
    channel = create(:channel, organization: org)
    team.add_admin admin
    team_channel_follow = create(:teams_channels_follow, team: team, channel: channel)

    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub :perform_later
    additional_data = { user_id: admin.id, platform: 'iOS', user_agent: 'Edcast iPhone' }
    RequestStore.stubs(:read).with(:request_metadata).returns(
      additional_data
    )

    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      event: 'group_channel_unfollowed',
      recorder: 'Analytics::GroupChannelsMetricsRecorder',
      group:  Analytics::MetricsRecorder.team_attributes(team),
      channel:  Analytics::MetricsRecorder.channel_attributes(channel),
      org:  Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(admin),
      timestamp: Time.now.to_i,
      additional_data: additional_data
    )
    team_channel_follow.destroy
  end
end

require 'test_helper'

class SettingsTest < ActiveSupport::TestCase

  setup do
    settings_file = Rails.application.root.join('config/settings.yml')
    file_data = open(settings_file).read
    erb_data = ERB.new(file_data).result
    @yaml_data = YAML.load(erb_data).to_hash
    @envs = ['development', 'test', 'staging', 'production']
  end

  def check_keys(hshes, topkey="")
    all_keys = hshes.map{|hsh| hsh.keys}.flatten.uniq
    all_keys.each do |key|
      class_names = hshes.map{|hsh| hsh[key].class.name}.uniq.map

      bool_normalized_classnames = class_names.map{|class_name| (class_name == 'TrueClass' || class_name == 'FalseClass') ? 'BoolClass' : class_name}.uniq

      # all classnames should be the same
      assert_equal 1, bool_normalized_classnames.size, "#{topkey}:#{key} Key error in settings. Current Configuration: #{hshes.map{|hsh| hsh[key]}}. Please verify settings.yml file. Possible causes: (a)You are using ENV variables in prod env only. This is error prone as you may forget to set the env variable on deployment and QA has no way of testing this. Set all ENV configuration in defaults. (b) Or you might be overriding a hash partially"

      class_name = class_names.first

      if class_name == 'Hash'
        check_keys(hshes.map{|hsh| hsh[key]}, "#{topkey}:#{key}")
      end

    end
  end

  test 'settings keys should be consistent across all envs' do
    # Config keys are not consistent across all envs.
    # custom keys are added in test env
    skip
    check_keys(@envs.map{|env| @yaml_data[env]})
  end

end

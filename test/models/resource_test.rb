require 'test_helper'

class ResourceTest < ActiveSupport::TestCase

  test "validate description length" do
    resource = build(:resource)

    # valid
    resource.description = "a"*1000
    assert resource.valid?

    # invalid
    resource.description = "a"*1001
    refute resource.valid?
  end

  test "save a valid resource" do
    resource = build(:resource)
    assert resource.save
  end

  test "resouce should contain a valid url" do
    resource = build(:resource, url:'bad url')
    assert_not resource.save
  end

  test 'should truncated title if url mising metdata' do
    bad_url = 'http://someurl.com?desind=ITEM%5fDETAILS%26componentID%3d00040922%26componentTypeID%3dE%2dLEARNING%26revi'
    Resource.expects(:get_resource_params).returns({title: bad_url.truncate(35), description: "", image_url: ""})

    resource_param = Resource.get_resource_params(bad_url)
  end

  test "resource should contain a valid video url, if present" do
    resource = build(:resource, video_url: 'video url')
    assert_not resource.save
  end

  test 'should create a video resource' do
    UrlMetadataExtractor.expects(:get_metadata)
        .returns({'og:type' => 'video', 'embed_html' => '<iframe />', 'title' => 'somev',
                  'og:video' => 'http://someurl.com'}).once
    vid = Resource.extract_resource('http://someurl.com')
    assert_not_nil vid
    assert_equal vid.class.name, 'Video'
    assert_not_nil vid.embed_html
  end

  test 'sets title with no value if the metadata is empty' do
    UrlMetadataExtractor.expects(:get_metadata).returns({'content_type' => 'text/html'}).once
    article = Resource.extract_resource('http://someurl.com')

    assert_equal '', article.title
  end

  test 'should create video resource for og type video' do
    UrlMetadataExtractor.expects(:get_metadata)
        .returns({'og:type' => 'video', 'title' => 'somev', 'og:video' => 'http://someurl.com'}).once
    vid = Resource.extract_resource('http://someurl.com')
    assert_not_nil vid
    assert_equal vid.class.name, 'Video'
    assert_nil vid.embed_html
  end

  test 'should store embed html' do
    UrlMetadataExtractor.expects(:get_metadata).returns({'embed_html' => '<iframe />'}).once
    r = Resource.extract_resource('http://someurl.com')
    assert_equal '<iframe />', r.embed_html
  end

  test 'should create an image resource' do
    UrlMetadataExtractor.expects(:get_metadata).returns({'content_type' => 'image/jpeg'}).once
    img = Resource.extract_resource('http://someurl.com')
    assert_not_nil img
    assert_equal img.class.name, 'Image'
  end

  test 'should create an article resource by default' do
    UrlMetadataExtractor.expects(:get_metadata).returns({}).once
    article = Resource.extract_resource('http://someurl.com')
    assert_not_nil article
    assert_equal article.class.name, 'Article'
  end

  test 'should add autoplay to video urls' do
    vid = Resource.create(:video_url => 'http://youtube.com?v=342',
                       :url => 'http://youtube.com?v=342')
    video_uri = URI.parse(vid.video_url)
    qhash = Rack::Utils.parse_nested_query(video_uri.query)
    assert_equal({'autoplay' => '1', 'v' => '342'}, qhash)
  end

  test 'should skip s3 prefix' do
    s3meta = Resource.extract_resource("http://localhost:4000")
    assert_nil s3meta
  end

  test 'should blank out non http/https image urls' do
    r = Resource.new(:url => 'http://someurl.com', :image_url => '/someimage.png', type: 'Article')
    assert r.save
    assert_nil r.reload.image_url
  end

  test 'should sanitize resource title and description to not store any malicious html/scripts' do
    r = Resource.new(:url => 'http://someurl.com', :title => '<a style="background:#006;color:#fff;text-align:center;" href="http://someurl">this is amazing!</a>',
     :description => '<span style="display:block;;background:red">Do check this out</span>', type: 'Article')
    assert r.save
    assert_not_equal '<span style="display:block;;background:red">Do check this out</span>', r.description
    assert_not_equal '<a style="background:#006;color:#fff;text-align:center;" href="http://someurl">this is amazing!</a>', r.title
    assert 'Do check this out', r.description
    assert 'this is amazing!', r.title
  end

  test 'should serialize title and description' do
    str = "🖥️ FINISHING MY FIRST MACHINE LEARNING GAME! (3/4)"
    str_strip = "FINISHING MY FIRST MACHINE LEARNING GAME! (3/4)"
    assert_nothing_raised do
      Resource.create!(url: "https://www.youtube.com/watch?v=GDy45vT1xlA", title: str, description: str)
    end
    assert_equal str_strip, Resource.last.title
    assert_equal str_strip, Resource.last.description
  end

  test 'should validate image url' do
    bad_image_url = '/prelogin/resources/images/mettl_beta.png?v=28.2.2'
    refute Resource.valid_image_url(bad_image_url)

    empty_image_url = ''
    refute Resource.valid_image_url(empty_image_url)
    refute Resource.valid_image_url(nil)

    correct_image_url = 'https://cdn.filestackcontent.com/8XU9oneQT76rvTBXi4Rq'
    assert Resource.valid_image_url(correct_image_url)
  end

  test 'should blank out bad image urls' do
    bad_url = "https://git@gitlab.example.com:group/project.git"
    assert_raises Addressable::URI::InvalidURIError do
      Addressable::URI.parse(bad_url)
    end
    r = Resource.new(:url => 'http://someurl.com', :image_url => bad_url, type: 'Article')
    assert r.save
    assert_nil r.reload.image_url
  end

  test 'should save image url which contains sapces' do
    bad_url = 'http://google.com/g g.jpg'
    Addressable::URI.parse(bad_url)
    r = Resource.new(:url => 'http://someurl.com', :image_url => bad_url, type: 'Article')
    assert r.save
    assert_not_nil r.reload.image_url
    assert_equal bad_url, r.reload.image_url
  end

  test 'should save the url hash' do
    r = Resource.new(:url => 'http://someurl.com', type: 'Article')
    r.save

    assert_not_nil r.url_hash
    assert_equal 64, r.url_hash.length
  end

  test 'sets `video_url` as per youtube-video-url passed in' do
    stub_request(:get, 'http://www.youtube.com/watch?v=6o-nmK9WRGE').
      to_return(status: 200, body: '', headers: {})
    s3meta = Resource.extract_resource('http://www.youtube.com/watch?v=6o-nmK9WRGE')
    assert_match %r{www.youtube.com/v/6o-nmK9WRGE\?autohide=1&autoplay=1}, s3meta[:video_url]
  end

  test 'do not set `video_url` as per vimeo-video-url passed in' do
    stub_request(:get, 'http://vimeo.com/113112718').
      to_return(status: 200, body: '', headers: {})
    s3meta = Resource.extract_resource('http://vimeo.com/113112718')
    assert_nil s3meta[:video_url]
  end

  test 'do not set `video_url` for other url passed in' do
    stub_request(:get, 'http://www.svtplay.se/video/2457628/planetens-granser/avsnitt-1-arktis').
      to_return(status: 200, body: '', headers: {})
    s3meta = Resource.extract_resource('http://www.svtplay.se/video/2457628/planetens-granser/avsnitt-1-arktis')
    assert_nil s3meta[:video_url]
  end

  test 'set og:type to video' do
    stub_request(:get, 'https://player.hihaho.com/embed/6C862FB2-FED5-40D3-BB60-A1902DC2818D').
      with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.9.2'}).
      to_return(:status => 200,
                :body => '<meta property="og:type" content="video.other">
                          <meta property="twitter:player" content="https://player.hihaho.com/embed/6C862FB2-FED5-40D3-BB60-A1902DC2818D">',
                :headers => {})
    s3meta = Resource.extract_resource('https://player.hihaho.com/embed/6C862FB2-FED5-40D3-BB60-A1902DC2818D')
    assert_equal s3meta[:type], 'Video'
  end

  test 'do not set `video_url` for not valid video url' do
    stub_request(:get, 'https://amplify.hpe.com/post/behpe/8736b17d-7e4f-4d1a-8c9e-fd7c74d80875').
        to_return(status: 200,
                  body: '<meta property="og:type" content="video">
                         <meta property="og:video" content="https://$CloudfrontDomain$/299101/b25d5efa-6ad2-457a-b482-3bab12d4fe8f_360.m4v">',
                  headers: {})
    s3meta = Resource.extract_resource('https://amplify.hpe.com/post/behpe/8736b17d-7e4f-4d1a-8c9e-fd7c74d80875')
    assert_nil s3meta[:video_url]
    assert_equal s3meta[:type], 'Article'
  end

  class GenerateYoutubeVideoUrl < ActiveSupport::TestCase
    test 'Case 1: `www.youtube.com/user/any-user-name#p/a/u/0/8WVTOUh53QY`' do
      url = Resource.generate_youtube_video_url('www.youtube.com/user/any-user-name#p/a/u/0/8WVTOUh53QY')
      assert_match /www.youtube.com\/v\/8WVTOUh53QY/, url
    end

    test 'Case 2: `youtu.be/0eX8D19htho`' do
      url = Resource.generate_youtube_video_url('youtu.be/0eX8D19htho')
      assert_match /www.youtube.com\/v\/0eX8D19htho/, url
    end

    test 'Case 3: `www.youtube.com/embed/0eX8D19htho`' do
      url = Resource.generate_youtube_video_url('www.youtube.com/embed/0eX8D19htho')
      assert_match /www.youtube.com\/v\/0eX8D19htho/, url
    end

    test 'Case 4: `//www.youtube.com/watch?v=0eX8D19htho`' do
      url = Resource.generate_youtube_video_url('//www.youtube.com/watch?v=0eX8D19htho')
      assert_match /www.youtube.com\/v\/0eX8D19htho/, url
    end

    test 'Case 5: `http://www.youtube.com/watch?v=0eX8D19htho`' do
      url = Resource.generate_youtube_video_url('http://www.youtube.com/watch?v=0eX8D19htho')
      assert_match /www.youtube.com\/v\/0eX8D19htho/, url
    end

    test 'Case 6: `//www.youtube.com/watch?v=0eX8D19htho`' do
      url = Resource.generate_youtube_video_url('//www.youtube.com/watch?v=0eX8D19htho')
      assert_match /www.youtube.com\/v\/0eX8D19htho/, url
    end

    test 'Case 7: `www.youtube.com/watch?v=0eX8D19htho`' do
      url = Resource.generate_youtube_video_url('www.youtube.com/watch?v=0eX8D19htho')
      assert_match /www.youtube.com\/v\/0eX8D19htho/, url
    end

    test 'Case 8: `http://youtu.be/0zM3nApSvMg`' do
      url = Resource.generate_youtube_video_url('http://youtu.be/0zM3nApSvMg')
      assert_match /www.youtube.com\/v\/0zM3nApSvMg/, url
    end

    test 'Case 9: `http://www.youtube.com/watch?v=0zM3nApSvMg`' do
      url = Resource.generate_youtube_video_url('http://www.youtube.com/watch?v=0zM3nApSvMg')
      assert_match /www.youtube.com\/v\/0zM3nApSvMg/, url
    end

    test 'Case 10: `http://www.youtube.com/watch?v=0zM3nApSvMg#t=0m10s`' do
      url = Resource.generate_youtube_video_url('http://www.youtube.com/watch?v=0zM3nApSvMg#t=0m10s')
      assert_match /www.youtube.com\/v\/0zM3nApSvMg/, url
    end

    test 'Case 11: `http://www.youtube.com/embed/0zM3nApSvMg?rel=0`' do
      url = Resource.generate_youtube_video_url('http://www.youtube.com/embed/0zM3nApSvMg?rel=0')
      assert_match /www.youtube.com\/v\/0zM3nApSvMg/, url
    end

    test 'Case 12: `http://www.youtube.com/v/0zM3nApSvMg?fs=1&hl=en_US&rel=0`' do
      url = Resource.generate_youtube_video_url('http://www.youtube.com/v/0zM3nApSvMg?fs=1&hl=en_US&rel=0')
      assert_match /www.youtube.com\/v\/0zM3nApSvMg/, url
    end

    test 'Case 13: `http://www.youtube.com/user/IngridMichaelsonVEVO#p/a/u/1/QdK8U-VIH_o`' do
      url = Resource.generate_youtube_video_url('http://www.youtube.com/user/IngridMichaelsonVEVO#p/a/u/1/QdK8U-VIH_o')
      assert_match /www.youtube.com\/v\/QdK8U-VIH_o/, url
    end

    test 'Case 14: `http://www.youtube.com/watch?v=0zM3nApSvMg&feature=feedrec_grec_index`' do
      url = Resource.generate_youtube_video_url('http://www.youtube.com/watch?v=0zM3nApSvMg&feature=feedrec_grec_index')
      assert_match /www.youtube.com\/v\/0zM3nApSvMg/, url
    end

    test 'Case 15: `http://www.youtube.com/watch?v=6o-nmK9WRGE&feature=player_embedded`' do
      url = Resource.generate_youtube_video_url('http://www.youtube.com/watch?v=6o-nmK9WRGE&feature=player_embedded')
      assert_match /www.youtube.com\/v\/6o-nmK9WRGE/, url
    end
  end

  test 'should create file resource for resources with image url' do
    ResourceJob.unstub(:perform_later)
    EdcastActiveJob.unstub :perform_later

    stub_request(:get, 'https://image.com/someimage.png').
      to_return(:status => 200, :body => "", :headers => {})

    r = nil

    assert_difference -> { FileResource.count } do
      r = Resource.create(:url => 'http://someurl.com',
                          :image_url => 'https://image.com/someimage.png',
                          type: 'Image')
      FileResource.any_instance.expects(:save).returns(create(:file_resource, attachable: r)).twice
      r.run_callbacks(:commit)
    end

    fr = FileResource.last
    assert_equal fr.id, r.file_resources.last.id
    ResourceJob.stubs(:perform_later)
  end

  test 'should extract keywords from url' do
    UrlMetadataExtractor.expects(:get_metadata).returns({'og:type' => 'video', 'embed_html' => '<iframe />',
                                                     'title' => 'somev', 'news_keywords' => 'Technology, iphone, magic', 'tags' => 'iphone, app'}).once

    assert_equal ['Technology', 'iphone', 'magic', 'app'], Resource.read_tags_and_keywords_from('https://www.somevaliddomain.com')
  end

  test '#image_url_secure returns https urls as is' do
    resource = build :resource, image_url: 'https://path.com/to_image.jpg'

    assert_equal 'https://path.com/to_image.jpg', resource.image_url_secure
  end

  test '#image_url_secure returns url as is if prefix_url is blank' do
    Settings.secure_images.expects(:prefix_url).returns('')
    resource = build :resource, image_url: 'http://path.com/to_image.jpg'

    assert_equal 'http://path.com/to_image.jpg', resource.image_url_secure
  end

  test '#image_url_secure prefixes http urls after encoding' do
    Settings.secure_images.expects(:prefix_url).returns('https://prefix_url.com/').twice
    resource = build :resource, image_url: 'http://path.com/to_image.jpg'

    image_url = resource.image_url_secure
    last_segment = image_url.split('/').last
    decoded_last_segment = JWT.decode(last_segment, Settings.secure_images.secret)[0]['url']

    assert_equal 'http://path.com/to_image.jpg', decoded_last_segment
  end

  test '#image_url_secure returns image_url if not present' do
    resource = build :resource, image_url: ''

    image_url = resource.image_url_secure

    assert_equal false, image_url
  end

  test '#url_with_context should return url as-is if blank' do
    resource1 = build :resource, url: ''
    assert_equal '', resource1.url_with_context(1)

    resource2 = build :resource, url: nil
    assert_nil resource2.url_with_context(1)
  end

  test '#url_with_context returns url as-is if scorm context secret not set' do
    Settings.scorm['user_context_secret'] = ''

    resource = build :resource, url: 'https://www.edcast.com/api/scorm/launch?course_id=1234'
    assert_equal 'https://www.edcast.com/api/scorm/launch?course_id=1234', resource.url_with_context(1)
  end

  test '#url_with_context returns url as-is if user_id passed is blank' do
    Settings.scorm['user_context_secret'] = 'ABCD'

    resource = build :resource, url: 'https://www.edcast.com/api/scorm/launch?course_id=1234'
    assert_equal 'https://www.edcast.com/api/scorm/launch?course_id=1234', resource.url_with_context('')
  end

  test '#url_with_context returns url as-is if URL hostname does not match platform' do
    Settings.expects(:platform_domain).returns('edcast.com')
    Settings.scorm['user_context_secret'] = 'ABCD'

    resource = build :resource, url: 'https://www.not-edcast.com/api/scorm/launch?course_id=1234'
    assert_equal 'https://www.not-edcast.com/api/scorm/launch?course_id=1234', resource.url_with_context(1)
  end

  test '#url_with_context returns url as-is if path is not that of scorm' do
    Settings.expects(:platform_domain).returns('edcast.com')
    Settings.scorm['user_context_secret'] = 'ABCD'

    resource = build :resource, url: 'https://www.edcast.com/api/not-scorm/launch?course_id=1234'
    assert_equal 'https://www.edcast.com/api/not-scorm/launch?course_id=1234', resource.url_with_context(1)
  end

  test '#url_with_context adds user context to url if scorm URL is passed' do
    Settings.expects(:platform_domain).returns('edcast.com')
    Settings.scorm['user_context_secret'] = 'ABCD'

    resource = build :resource, url: 'https://www.edcast.com/api/scorm/launch?course_id=1234'
    url_with_context = resource.url_with_context(1)

    parsed_url = Addressable::URI.parse(url_with_context)
    encoded = parsed_url.query_values['user_context']
    assert_not_nil encoded
    assert url_with_context.start_with?('https://www.edcast.com/api/scorm/launch?course_id=1234')

    decoded, _ = JWT.decode(CGI.unescape(encoded), Settings.scorm.user_context_secret)
    assert_equal 1, decoded['user_id']
  end

  test '#clone_resource clones a resource' do
    resource = create :resource
    user = create :user

    cloned_resource = Resource.clone_resource(resource, user_id: user.id)

    assert_not cloned_resource.persisted?
    assert_equal resource.url, cloned_resource.url
    assert_equal resource.description, cloned_resource.description
    assert_equal resource.title, cloned_resource.title
    assert_equal resource.type, cloned_resource.type
    assert_equal resource.site_name, cloned_resource.site_name
    assert_equal resource.image_url, cloned_resource.image_url
    assert_equal resource.video_url, cloned_resource.video_url
    assert_equal resource.embed_html, cloned_resource.embed_html
    assert_equal resource.url_hash, cloned_resource.url_hash

    assert_equal user.id, cloned_resource.user_id

    assert_nil cloned_resource.created_at
    assert_nil cloned_resource.updated_at
  end

  test '#parsed_client_resource_id returns client_resource_id from url for savannah course' do
    group = create :group
    resource1 = create :resource, url: "https://fake-domain-name.com/courses/#{group.id}/link"
    dummy_client_resource_id = 123
    resource2 = create :resource, url: "https://fake-domain-name.com/courses/#{dummy_client_resource_id}/redirect"

    assert_equal group.client_resource_id, resource1.parsed_client_resource_id
    assert_equal dummy_client_resource_id, resource2.parsed_client_resource_id
  end

  class ExtractResourceOnEdCastContentTest < ActiveSupport::TestCase
    setup do
      @org = create :organization, custom_domain: 'www.google.com'
      @user = create :user, organization: @org
    end

    test '#extract_resource calls resource_from_existing_card for edcast domain content' do
      card = create :card, organization: @org, author: @user, slug: 'hello-world',
                    resource: (create :resource)
      text = "https://#{@org.host_name}.#{Settings.platform_domain}/insights/hello-world"

      Resource.expects(:resource_from_existing_card)

      Resource.extract_resource text, user: @user, organization: @org
    end

    test '#extract_resource calls resource_from_existing_card for custom domain content' do
      card = create :card, organization: @org, author: @user, slug: 'hello-world',
                    resource: (create :resource)
      text = "https://#{@org.custom_domain}/insights/hello-world"

      Resource.expects(:resource_from_existing_card)

      Resource.extract_resource text, user: @user, organization: @org
    end

    test '#extract_resource does not call resource_from_existing_card for other content' do
      card = create :card, organization: @org, author: @user, slug: 'hello-world',
                    resource: (create :resource)
      text = "https://otherdomain.com/insights/hello-world"

      Resource.expects(:resource_from_existing_card).never
      Resource.expects(:get_resource_params).returns({})

      Resource.extract_resource text, user: @user, organization: @org
    end

    test '#extract_resource does not call resource_from_existing_card for SCORM content' do
      card = create :card, organization: @org, author: @user, slug: 'hello-world',
                    resource: (create :resource)
      text = "https://#{@org.custom_domain}/api/scorm/launch?course_id=hello&user_content=world"

      Resource.expects(:resource_from_existing_card).never
      Resource.expects(:get_resource_params).returns({})

      Resource.extract_resource text, user: @user, organization: @org
    end
  end

  class ResourceFromExistingCardTest < ActiveSupport::TestCase
    setup do
      @org = create :organization, custom_domain: 'www.google.com'
      @author = create :user, organization: @org
      @user = create :user, organization: @org

      @sample_slug = 'hello-world'
      @sample_path = "/insights/#{@sample_slug}"
      @sample_url = URI.parse("https://#{@org.custom_domain}#{@sample_path}")
    end

    test '#resource_from_existing_card clones resource from an existing card' do
      card = create :card, organization: @org, author: @user, slug: @sample_slug,
                    resource: (create :resource)

      cloned_resource = Resource.clone_resource(card.resource)
      Resource.expects(:clone_resource).returns(cloned_resource)

      resource = Resource.resource_from_existing_card(@sample_url, @org, @user)
      assert resource.persisted?
      assert_equal @user.id, resource.user_id
    end

    test '#resource_from_existing_card creates resource for a card without resource' do
      card = create :card, organization: @org, author: @user, slug: @sample_slug, resource: nil

      Resource.expects(:clone_resource).never

      resource = Resource.resource_from_existing_card(@sample_url, @org, @user)

      assert resource.persisted?
      assert_equal card.title, resource.title
      assert_equal card.message, resource.description
      assert_equal @sample_url.to_s, resource.url
    end
  end

  class ExtractResourceFromSourceForDiscoverTest < ActiveSupport::TestCase
    setup do
      @org = create(:organization, host_name: 'whoop')

      @user = create(:user, organization_id: @org.id)

      @sample_response = {"data"=>[{"id"=>"5dd24894-c0a6-4bea-a522-2dcd73d3e0f9", "organization_id"=>@org.id,
        "display_name"=>"whoooo", "description"=>"this is a test source", "banner_url"=>"http://tempp_image/temp.png"}]}

      stub_request(:get, "http://test.localhost/api/v1/sources?display_name=whooop&limit=1")
        .to_return(:body => @sample_response.to_json)

      UrlMetadataExtractor.stubs(:return_url).returns "http://whoop.test.host/discover/whooop"
    end

    test '#resource_from_source should call ecl for the source details and create resource with it' do
      resource = Resource.resource_from_source(URI.parse("http://whoop.test.host/discover/whooop"), @org, @user)
      resource.reload

      assert_equal resource.description, @sample_response['data'][0]['description']
      assert_equal resource.title, @sample_response['data'][0]['display_name']
      assert_equal resource.image_url, @sample_response['data'][0]['banner_url']
      assert_equal resource.user_id, @user.id
      assert_equal resource.url, "http://whoop.test.host/discover/whooop"
    end

    test '#resource_from_existing_card should not be called when the url pattern has /discover/' do
      Resource.any_instance.expects(:resource_from_existing_card).never
      Resource.extract_resource("http://whoop.test.host/discover/whooop", organization: @org, user: @user)
    end

    test '#resource_from_source should not be called for other url patterns with discover' do
      Resource.any_instance.expects(:resource_from_source).never
      Resource.extract_resource("http://businessinsider/yourself/discover", organization: @org, user: @user)
    end
  end
end

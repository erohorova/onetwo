require 'test_helper'
class VideoStreamTest < ActiveSupport::TestCase
  self.use_transactional_fixtures = false
  should have_db_column(:card_id).of_type(:integer)
  should belong_to :card
  setup do
    VideoStream.any_instance.stubs(:create_index).returns true
    VideoStream.any_instance.stubs(:update_index).returns true
    VideoStream.any_instance.stubs(:delete_index).returns true
    ChannelsCard.any_instance.stubs :create_users_management_card

    @org = create(:organization)
    VideoStream.any_instance.stubs(:reindex_async)
  end

  test 'access control empty channels' do
    vs = create(:iris_video_stream, state: "draft", status: 'past')
    assert vs.can_be_accessed_by?(create(:user))
  end

  test 'access control when channels match' do
    ch1, ch2 = create_list(:channel, 2, organization: @org)
    user = create(:user, organization: @org)
    create(:follow, user: user, followable: ch1)
    vs = create(:iris_video_stream, status: 'past', creator: create(:user, organization: @org))
    vs.channels << [ch1, ch2]
    assert vs.can_be_accessed_by?(user)
  end

  test 'draft stream publish should enqueue content' do
    EnqueueStreamsJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    video_stream = create(:wowza_video_stream, state: "draft", creator: create(:user, organization: @org))
    EnqueueStreamsJob.expects(:perform_later).with({id: video_stream.id}).returns(nil).once
    video_stream.publish!
  end

  test 'draft stream create should not call feed enqueue job' do
    EnqueueStreamsJob.expects(:perform_later).never
    video_stream = create(:wowza_video_stream, state: "draft", creator: create(:user, organization: @org))
  end

  test 'invalidate score job' do
    skip
    WebMock.disable!
    video_stream = create(:wowza_video_stream, creator: create(:user, organization: @org))
    VideoStream.searchkick_index.refresh
    users = create_list(:user, 5, organization: @org)
    Timecop.freeze(Date.today - 3)
    users.each do |user|
      create(:comment, commentable: video_stream, user: user, created_at: Time.now - 5.days)
      VideoStream.searchkick_index.refresh
    end
    users.each do |user|
      create(:comment, commentable: video_stream, user: user)
      VideoStream.searchkick_index.refresh
    end
    Timecop.return
    VideoStream.searchkick_index.refresh
    video_streams = Search::VideoStreamSearch.new.find_objects_with_invalid_score.results
    assert_equal video_stream.id, video_streams[0].id
    VideoStream.searchkick_index.refresh
    video_stream.invalidate_score
    VideoStream.searchkick_index.refresh
    elasticsearch_object = Search::VideoStreamSearch.new.search_by_id(video_stream.id)
    assert_equal 25, elasticsearch_object[:weekly_score]
    assert (Time.zone.parse(elasticsearch_object[:weekly_score_valid_till]) - 4.days.from_now < 10 )
    WebMock.enable!
  end

  test 'deleting videostream should delete index' do
    skip
    WebMock.disable!
    video_stream = create(:wowza_video_stream)
    VideoStream.searchkick_index.refresh
    assert_equal Search::VideoStreamSearch.new.search_by_id(video_stream.id).name, video_stream.name
    video_stream.destroy
    VideoStream.searchkick_index.refresh
    assert_nil Search::VideoStreamSearch.new.search_by_id(video_stream.id)
    WebMock.enable!
  end

  test 'assignment url for video streams' do
    video_stream =  create(:wowza_video_stream)
    assert_match "/video_streams/#{video_stream.slug}", video_stream.assignment_url
  end

  test 'handle elasticsearch documentmissing error' do
    skip
    VideoStream.any_instance.stubs(:create_index).returns([])
    video_stream = create(:wowza_video_stream)
    VideoStream.any_instance.unstub(:create_index)
    video_stream.partial_update = true
    video_stream.update_hash = { awesome_column: true, weekly_score: 25 }
    video_stream.save
    VideoStream.searchkick_index.refresh
    video_stream_elasticsearch_object = Search::VideoStreamSearch.new.search_by_id(video_stream.id)
    assert_equal true, video_stream_elasticsearch_object[:awesome_column]
    assert_equal 25, video_stream_elasticsearch_object[:weekly_score]
  end

  test 'find new popular trending streams with only state published' do
    skip #moved to ECL
    WebMock.disable!
    channel = create(:channel, organization: @org)
    video_streams = create_list(:wowza_video_stream, 5, creator: create(:user, organization: @org))
    video_streams += create_list(:wowza_video_stream, 5, state: 'deleted', creator: create(:user, organization: @org))
    video_streams.each do |video_stream|
      video_stream.channels = [channel]
      video_stream.update_hash = {"channel_ids": video_stream.channel_ids}
      video_stream.partial_update = true
      video_stream.reindex
    end

    VideoStream.searchkick_index.refresh

    results = Search::VideoStreamSearch.new.find_popular_videos([channel.id], :all, 30,0).results
    trending_results = Search::VideoStreamSearch.new.find_trending_videos([channel.id], :all, 30,0).results
    new_stream_results = Search::VideoStreamSearch.new.newest_streams([channel.id], :all, 30,0).results
    assert_equal 5, results.size
    assert_equal 5, trending_results.size
    assert_equal 5, new_stream_results.size
    WebMock.enable!
  end

  test 'video stream can only added to channels of the same org' do
    org = create(:organization)
    user = create(:user, organization: org)
    video_stream = create(:wowza_video_stream, organization: org, creator: user)
    channel = create(:channel)
    assert_raises(ActiveRecord::RecordInvalid) { video_stream.channels << channel }
    assert_equal true, video_stream.channels.collect(&:id).index(channel.id).nil?
  end

  test 'should send out push notification when changing from pending to live' do
    video_stream = create(:wowza_video_stream, :pending)
    NotificationGeneratorJob.expects(:perform_later).returns(nil).once
    EnqueueStreamsJob.expects(:perform_later).returns(nil).once
    video_stream.update!(status: 'live')
  end

  test 'should send out push notification through new notification framework when adding an upcoming stream' do
    TestAfterCommit.enabled = true
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(true)
    VideoStream.any_instance.unstub(:schedule_reminder_push_notification)
    job = mock()
    ScheduleStreamNotificationJob.expects(:set).returns(job)
    job.expects(:perform_later)
    video_stream = create(:wowza_video_stream, :upcoming, start_time: Time.now.utc + 20.minutes, 
      organization_id: @org.id, creator: create(:user, organization: @org))
  end

  test 'should not send out notification if videostream is not upcoming' do
    TestAfterCommit.enabled = true
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(true)
    VideoStream.any_instance.unstub(:schedule_reminder_push_notification)
    job = mock()
    ScheduleStreamNotificationJob.expects(:set).returns(job).never
    job.expects(:perform_later).never
    video_stream = create(:wowza_video_stream, organization_id: @org.id, creator: create(:user, organization: @org))
  end

  test 'should send notification to channel followers when videostream is posted to channel' do
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(true)
  
    user = create :user, organization: @org
    follower = create :user, organization: @org
    channel = create :channel, user: user, organization: @org
    channel.followers << follower
    card = create :card, organization: @org, author_id: user.id
    video_stream = create(:wowza_video_stream, organization_id: @org.id, creator: user, card: card)
    channel_card = create :channels_card, card_id: card.id, channel_id: channel.id

    CreateLiveStreamNotificationJob.expects(:perform_later).once

    video_stream.start!
  end

  test 'should send notification to followers of creator when videostream is started' do
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(true)
  
    user = create :user, organization: @org
    follower = create :user, organization: @org
    user.user_followers << follower
    card = create :card, organization: @org, author_id: user.id
    video_stream = create(:wowza_video_stream, organization_id: @org.id, creator: user, card: card)
    CreateLiveStreamNotificationJob.expects(:perform_later).once

    video_stream.start!
  end

  test 'should create a card on video_stream creation' do
    assert_difference ->{Card.count} do
      video_stream = create(:wowza_video_stream, :pending)
    end
  end

  test 'deletes associated card on destroy' do
    UserContentsQueue.stubs(:dequeue_content)
    video_stream = create(:wowza_video_stream, :pending)
    card = video_stream.card
    video_stream.destroy
    refute card.persisted?
  end

  test 'should update  card on video_stream name or state changed' do
    video_stream = create(:wowza_video_stream, is_official: false)
    refute video_stream.card.is_official
    video_stream.update!(name: "updated name", is_official: true)
    assert_equal video_stream.reload.card.title, "updated name"
    assert video_stream.card.is_official
  end

  test 'pushes card edited influx event on update' do
    Card.any_instance.unstub :push_card_edited_influx_event
    video_stream = create :wowza_video_stream
    # It's actually called 3 times, but we can test "at least once".
    # These are not duplicate calls, they're being called with different args
    Analytics::MetricsRecorderJob
      .expects(:perform_later)
      .with(has_entry(event: 'card_edited'))
      .at_least(1)

    video_stream.update(is_official: !video_stream.is_official)
  end

  test 'pushes card deleted influx event on delete' do
    UserContentsQueue.stubs(:dequeue_content)
    org = create :organization

    author = create :user, organization: org
    card = create :card, author: author
    video_stream = create :wowza_video_stream, card: card
    Analytics::MetricsRecorderJob
      .expects(:perform_later)
      .with(has_entry(event: 'card_deleted'))
      .once
    video_stream.destroy!
  end

  test 'promoting video stream pushes card_promoted influx event' do
    org = create :organization
    video_stream = create :wowza_video_stream, is_official: false
    refute video_stream.card.is_official
    video_stream.card.expects(:record_card_promote_event)
    video_stream.update is_official: true
    assert video_stream.card.reload.is_official
  end

  test 'unpromoting video stream pushes card_unpromoted influx event' do
    org = create :organization
    video_stream = create :wowza_video_stream, is_official: true
    assert video_stream.card.is_official
    video_stream.card.expects(:record_card_unpromote_event)
    video_stream.update is_official: false
    refute video_stream.card.reload.is_official
  end

  class TopicsTest < ActiveSupport::TestCase
    setup do
      skip
      @video_stream = build(:wowza_video_stream)
      @video_stream.topics = "Web, Ruby"
      @video_stream.save
    end

    test 'handle empty topics' do
      @video_stream.update_attributes(name:"something", topics: [])
      @video_stream.reload
      assert_equal true, @video_stream.tags.empty?
    end

    test 'handle updates when topics arent updated' do
      video_stream_new_object = VideoStream.find(@video_stream.id)
      video_stream_new_object.status = "live"
      video_stream_new_object.save
      video_stream_new_object.reload
      assert_equal ["Web", "Ruby"], video_stream_new_object.tags.collect(&:name)
    end
  end

  class VideoStreamCardStateTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false

    setup do
      VideoStream.any_instance.stubs(:create_index).returns true
      VideoStream.any_instance.stubs(:update_index).returns true
      EclCardPropagationJob.stubs(:perform_later).returns(nil)
      VideoStream.any_instance.stubs(:schedule_reminder_push_notification)
    end

    test 'should update card state #publish' do
      video_stream = create(:iris_video_stream, state: 'draft')
      card = create(:card, video_stream: video_stream, state: 'draft', card_type: 'video_stream')

      video_stream.publish!
      card.reload

      assert_equal video_stream.state, card.state
    end

    test 'should update card state #delete_from_cms' do
      video_stream = create(:iris_video_stream, state: 'published')
      card = create(:card, video_stream: video_stream, state: 'published', card_type: 'video_stream')

      video_stream.delete_from_cms!
      card.reload

      assert card.deleted?
    end
  end
end

require 'test_helper'

class OrganizationLevelMetricTest < ActiveSupport::TestCase

  setup do
  end

  teardown do
  end

  test 'increment events count' do
    user1, user2 = create_list(:user, 2)
    card = create(:card)
    video_stream = create(:wowza_video_stream)
    content_type = "card"
    action = "impression"

    [user1, user2].each do |user|
      [[:all_time, 0], [:day, 0], [:day, 1], [:week, 0], [:month, 0], [:year, 0]].each do |period_with_offset|
        create(:organization_level_metric, user: user, organization: user.organization, content_type: content_type, period: period_with_offset[0], offset: period_with_offset[1], action: action)
      end
    end

    # Update existing metric
    assert_no_difference ->{OrganizationLevelMetric.count} do
      OrganizationLevelMetric.increment_events_count({organization_id: user1.organization_id, user_id: user1.id, content_type: content_type, action: action}, 1, PeriodOffsetCalculator::BEGINNING_OF_TIME + 1.day)
    end

    # The metric for days 1, week 0 and all time should have changed
    assert_equal 1, OrganizationLevelMetric.get_events_count({organization_id: user1.organization_id, user_id: user1.id, action: action, content_type: content_type}, :day, 1)
    assert_equal 1, OrganizationLevelMetric.get_events_count({organization_id: user1.organization_id, user_id: user1.id, action: action, content_type: content_type}, :week, 0)
    assert_equal 1, OrganizationLevelMetric.get_events_count({organization_id: user1.organization_id, user_id: user1.id, action: action, content_type: content_type}, :all_time, 0)
    assert_equal 1, OrganizationLevelMetric.get_events_count({organization_id: user1.organization_id, user_id: user1.id, action: action, content_type: content_type}, :month, 0)
    assert_equal 1, OrganizationLevelMetric.get_events_count({organization_id: user1.organization_id, user_id: user1.id, action: action, content_type: content_type}, :year, 0)

    # Make sure everything else is still 0
    assert_equal 5, OrganizationLevelMetric.all.map(&:events_count).reduce(:+)


    # Create new entries, now we are in week 2
    assert_difference ->{OrganizationLevelMetric.count}, 2 do
      OrganizationLevelMetric.increment_events_count({organization_id: user1.organization_id, user_id: user1.id, content_type: content_type, action: action}, 1, PeriodOffsetCalculator::BEGINNING_OF_TIME + 9.days)
    end

    assert_equal 2, OrganizationLevelMetric.get_events_count({organization_id: user1.organization_id, user_id: user1.id, content_type: content_type, action: action}, :all_time, 0)
    assert_equal 2, OrganizationLevelMetric.get_events_count({organization_id: user1.organization_id, user_id: user1.id, content_type: content_type, action: action}, :year, 0)
    assert_equal 2, OrganizationLevelMetric.get_events_count({organization_id: user1.organization_id, user_id: user1.id, content_type: content_type, action: action}, :month, 0)
    assert_equal 1, OrganizationLevelMetric.get_events_count({organization_id: user1.organization_id, user_id: user1.id, content_type: content_type, action: action}, :day, 9)
    assert_equal 1, OrganizationLevelMetric.get_events_count({organization_id: user1.organization_id, user_id: user1.id, content_type: content_type, action: action}, :week, 1)

    # Make sure nothing else has changed
    assert_equal 10, OrganizationLevelMetric.all.map(&:events_count).reduce(:+)
  end

end

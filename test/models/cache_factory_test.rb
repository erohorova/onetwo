require 'test_helper'

class CacheFactoryTest < ActiveSupport::TestCase

  test 'should not cache nil value' do
    client = create(:client)
    org = create(:organization, client: client)
    client_resource_id = SecureRandom.hex
    result = CacheFactory.fetch_group(organization_id: org.id, client_resource_id: client_resource_id)
    assert_nil result

    group = create(:group, client_resource_id: client_resource_id, organization: org)
    result = CacheFactory.fetch_group(organization_id: org.id, client_resource_id: client_resource_id)
    assert_equal result, group, 'should return a new group now'
  end
end
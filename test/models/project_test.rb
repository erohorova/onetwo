require 'test_helper'

class ProjectTest < ActiveSupport::TestCase
  should have_db_column(:reviewer_id).of_type(:integer)
  should have_db_column(:card_id).of_type(:integer)

  should belong_to(:card)
  should belong_to(:reviewer)
  should have_many :project_submissions
end

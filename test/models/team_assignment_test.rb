require 'test_helper'

class TeamAssignmentTest < ActiveSupport::TestCase
  should belong_to :team
  should belong_to :assignment
  should belong_to :assignor
  include ActiveJob::TestHelper

  test "responds to generate_email_notification callback" do
    mailer = mock
    mailer.expects(:deliver_later)
    org = create(:organization)
    team = create(:team, organization: org)
    card = create(:card, author: create(:user, organization: org))
    assignment = create(:assignment, assignable: card, assignee: create(:user, organization: org))
    user = create(:user, organization: org)

    UserMailer.expects(:assignment_notification).with(team, assignment, user, nil).returns(mailer)

    team_assignment = create(:team_assignment, team: team, assignment: assignment, assignor: user)
    team_assignment.run_callbacks(:commit)
  end

  test "should not respond to generate_email_notification and assignment notification callback if assignor assigns card to himself" do
    mailer = mock
    mailer.expects(:deliver_later).never
    org = create(:organization)
    card = create(:card, author: create(:user, organization: org))
    user = create(:user, organization: org)
    assignment = create(:assignment, assignable: card, assignee: user)

    UserTeamAssignmentNotificationJob.expects(:perform_later).never
    UserMailer.expects(:assignment_notification).never

    team_assignment = create(:team_assignment, team: nil, assignment: assignment, assignor: user)
    team_assignment.run_callbacks(:commit)
  end

  test 'should invoke assignment notification generator job' do
    UserTeamAssignmentNotificationJob.expects(:perform_later).once
    org = create(:organization)
    team = create(:team, organization: org)
    card = create(:card, author: create(:user, organization: org))
    assignment = create(:assignment, assignable: card, assignee: create(:user, organization: org))
    user = create(:user, organization: org)

    team_assignment = create(:team_assignment, team: team, assignment: assignment, assignor: user)
    team_assignment.run_callbacks(:commit)
  end

  test 'should invoke pubnub notifier' do
    UserTeamAssignmentNotificationJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    org = create(:organization)
    team = create(:team, organization: org)
    card = create(:card, author: create(:user, organization: org))
    assignment = create(:assignment, assignable: card, assignee: create(:user, organization: org))
    user = create(:user, organization: org)

    team_assignment = create(:team_assignment, team: team, assignment: assignment, assignor: user)
    notification = Notification.new(notifiable: assignment, user_id: assignment.assignee.id, notification_type: Notification::TYPE_ASSIGNED)
    notification.causal_users << team_assignment.assignor
    opts = { user_id: assignment.assignee.id, deep_link_id: card.id, deep_link_type:  'card', item: assignment, notification_type: Notification::TYPE_ASSIGNED, content: notification.message, host_name: assignment.assignee.organization.host }
    PubnubNotifier.expects(:notify).with(opts,false)
    perform_enqueued_jobs do
      team_assignment.run_callbacks(:commit)
    end
  end

  test 'should invoke pubnub notifier with right parameters for video stream cards' do
    UserTeamAssignmentNotificationJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    org = create(:organization)
    team = create(:team, organization: org)
    video_stream = create(:iris_video_stream, creator: create(:user, organization: org))
    assignment = create(:assignment, assignable: video_stream.card, assignee: create(:user, organization: org))
    user = create(:user, organization: org)

    team_assignment = create(:team_assignment, team: team, assignment: assignment, assignor: user)

    TeamAssignment.any_instance.stubs(:assignment).returns assignment
    notification = Notification.new(notifiable: assignment, user_id: assignment.assignee.id, notification_type: Notification::TYPE_ASSIGNED)
    notification.causal_users << team_assignment.assignor
    opts = { user_id: assignment.assignee.id, deep_link_id: video_stream.card.id, deep_link_type:  'card', item: assignment, notification_type: Notification::TYPE_ASSIGNED, content: notification.message, host_name: assignment.assignee.organization.host }
    PubnubNotifier.expects(:notify).with(opts,false)
    perform_enqueued_jobs do
      team_assignment.run_callbacks(:commit)
    end
  end

  test 'should invoke pubnub notifier with right parameters for collection cards' do
    UserTeamAssignmentNotificationJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    org = create(:organization)
    team = create(:team, organization: org)
    assignable = create(:card, author: create(:user, organization: org), card_type: 'pack', card_subtype: 'simple')
    assignment = create(:assignment, assignable: assignable, assignee: create(:user, organization: org))
    user = create(:user, organization: org)
    team_assignment = create(:team_assignment, team: team, assignment: assignment, assignor: user)

    TeamAssignment.any_instance.stubs(:assignment).returns assignment
    notification = Notification.new(notifiable: assignment, user_id: assignment.assignee.id, notification_type: Notification::TYPE_ASSIGNED)
    notification.causal_users << team_assignment.assignor
    opts = { user_id: assignment.assignee.id, deep_link_id: assignable.id, deep_link_type: 'collection', item: assignment, notification_type: Notification::TYPE_ASSIGNED, content: notification.message, host_name: assignment.assignee.organization.host }
    PubnubNotifier.expects(:notify).with(opts,false)
    perform_enqueued_jobs do
      team_assignment.run_callbacks(:commit)
    end
  end

  test 'should enqueue team assignment metric' do
    org = create(:organization)
    team = create(:team, organization: org)
    card = create(:card, author: create(:user, organization: org))
    assignment = create(:assignment, assignable: card, assignee: create(:user, organization: org))
    assignor, another_assignor = create_list(:user, 2, organization: org)

    team_assignment1 = create(:team_assignment, team: team, assignment: assignment, assignor: assignor)
    team_assignment1.run_callbacks(:commit)

    assignment.start!
    team_assignment2 = create(:team_assignment, team: team, assignment: assignment, assignor: another_assignor)
    team_assignment2.run_callbacks(:commit)
  end

  class AssignedTeamTest < ActiveSupport::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      @teams_list = create_list(:team, 3, organization: @org)
      @card_list = create_list(:card, 2, organization: @organization, author: @user)
      @assignment1 = create(:assignment, assignable: @card_list[0], assignee: create(:user, organization: @org))
      assignment2 = create(:assignment, assignable: @card_list[1], assignee: create(:user, organization: @org))
      create(:team_assignment, team_id: @teams_list[0].id, assignment_id: @assignment1.id, assignor_id: @user.id)
      create(:team_assignment, team_id: @teams_list[1].id, assignment_id: @assignment1.id, assignor_id: @user.id)
    end

    test 'should fetch teams the content has been assigned to' do
      assigned_teams = TeamAssignment.assigned_teams(q: nil, assignor_id: nil, content_id: @card_list[0].id)
      assert_equal 2, assigned_teams.count
      assert_same_elements [@teams_list[0].name, @teams_list[1].name], assigned_teams.pluck(:name)
      assert_not_includes assigned_teams.pluck(:id), @teams_list[2].id
    end

    test 'should fetch teams content has been assigned to by a specific assignor' do
      assignor = create(:user, organization: @org)
      create(:team_assignment, team_id: @teams_list[2].id, assignment_id: @assignment1.id, assignor_id: assignor.id)
      
      assigned_teams = TeamAssignment.assigned_teams(q: nil, assignor_id: @user.id, content_id: @card_list[0].id)
      assert_same_elements [@teams_list[0].id, @teams_list[1].id], assigned_teams.pluck(:id)
      assert_not_includes assigned_teams.pluck(:id), @teams_list[2].id
    end

    test 'should fetch teams content has been assigned to by query' do
      team = create(:team, organization: @org, name: 'abc xxx')
      create(:team_assignment, team_id: team.id, assignment_id: @assignment1.id, assignor_id: @user.id)
      
      assigned_teams = TeamAssignment.assigned_teams(q: 'abc x', assignor_id: nil, content_id: @card_list[0].id)
      assert_equal [team.id], assigned_teams.pluck(:id)
    end

    test 'should fetch teams content has been assigned to by query and assignor' do
      assigned_teams = TeamAssignment.assigned_teams(q: 'team', assignor_id: @user, content_id: @card_list[0].id)
      assert_same_elements assigned_teams.pluck(:id), [@teams_list[0].id, @teams_list[1].id]
      assert_not_includes assigned_teams.pluck(:id), @teams_list[2].id
    end
  end

end

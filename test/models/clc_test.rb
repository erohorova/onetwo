require 'test_helper'

class ClcTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  should belong_to(:organization)
  should belong_to(:entity)
  should validate_presence_of(:entity_id)
  should validate_presence_of(:entity_type)
  should validate_presence_of(:from_date)
  should validate_presence_of(:to_date)
  should validate_presence_of(:target_score)

  should accept_nested_attributes_for(:clc_badgings)

  test "process new clc" do
    org = create(:organization)
    old_clc = create(:clc, name: 'test clc', entity: org, organization: org)
    assert_nil old_clc.reload.deleted_at

    new_clc = build(:clc, name: 'test clc', entity: org, organization: org)
    new_clc.save
    assert_not_nil old_clc.reload.deleted_at
  end

  test "validate from date" do
    org = create(:organization)
    clc = build(:clc, name: 'test clc', entity: org, organization: org, from_date: Date.today - 1)
    refute clc.valid?

    clc1 = build(:clc, name: 'test clc', entity: org, organization: org, from_date: Date.today)
    assert clc1.valid?
  end

  test "validate duration" do
    org = create(:organization)
    clc = build(:clc, name: 'test clc', entity: org, organization: org, from_date: Date.today, to_date: Date.today)
    refute clc.valid?

    clc1 = build(:clc, name: 'test clc', entity: org, organization: org, from_date: Date.today, to_date: Date.today + 5.days)
    assert clc1.valid?
  end

  test "CLC destroy should remove set deleted at" do
    org = create(:organization)
    clc = create(:clc, name: 'test clc', entity: org, organization: org)
    clc.destroy
    assert_not_nil clc.deleted_at
  end

  test "should create new clc notification if clc is started today" do
    org = create(:organization)
    clc = create(:clc, name: 'test clc', entity: org, organization: org, from_date: Date.today, to_date: (Date.today + 1.months))
    NotificationGeneratorJob.expects(:perform_later).returns(nil).once
    Clc.create_notification_for_started_clc
  end

  class ClcRecorderTest < ActiveSupport::TestCase
    test "should push influx org_clc_created event" do
      TestAfterCommit.enabled = true
      org = create :organization
      user = create :user, organization: org
      clc_badging = Clc.new(
        id: 1000001,
        entity: org, 
        from_date: Time.now, 
        to_date: Time.now + 2.weeks, 
        target_score: 100,
        target_steps: 100,
        organization_id: org.id,
        name: "CLC BADGE TEST"
      )
      stub_time = Time.now
      
      
      metadata = { user_id: user.id }    
      
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries(
          recorder: "Analytics::OrgBadgingMetricsRecorder",
          event: "org_clc_created",
          actor: Analytics::MetricsRecorder.user_attributes(user),
          org: Analytics::MetricsRecorder.org_attributes(org),
          timestamp: stub_time.to_i,
          clc: Analytics::MetricsRecorder.clc_attributes(clc_badging),
          additional_data: metadata
          )
        )
      Timecop.freeze(stub_time) do
        clc_badging.save
      end    
    end
    
    test "should push influx org_clc_deleted event" do
      TestAfterCommit.enabled = true
      org = create :organization
      user = create :user, organization: org
      clc_badging = Clc.create(
        entity: org, 
        from_date: Time.now, 
        to_date: Time.now + 2.weeks, 
        target_score: 100,
        target_steps: 100,
        organization_id: org.id,
        name: "CLC BADGE TEST"
      )
      stub_time = Time.now
      
      
      metadata = { user_id: user.id }    
      
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries(
          recorder: "Analytics::OrgBadgingMetricsRecorder",
          event: "org_clc_deleted",
          actor: Analytics::MetricsRecorder.user_attributes(user),
          org: Analytics::MetricsRecorder.org_attributes(org),
          timestamp: stub_time.to_i,
          clc: Analytics::MetricsRecorder.clc_attributes(clc_badging),
          additional_data: metadata
          )
        )
      Timecop.freeze(stub_time) do
        clc_badging.destroy
      end    
    end

    test "should push influx org_clc_edited event" do
      org = create :organization
      user = create :user, organization: org
      clc_badging = Clc.create(
        entity: org, 
        from_date: Time.now, 
        to_date: Time.now + 2.weeks, 
        target_score: 100,
        target_steps: 100,
        organization_id: org.id,
        name: "CLC BADGE TEST"
      )
      stub_time = Time.now
      clc_badging.name = "CLC BADGE TEST EDITED"   

      metadata = { user_id: user.id } 
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries(
          recorder: "Analytics::OrgBadgingMetricsRecorder",
          event: "org_clc_edited",
          actor: Analytics::MetricsRecorder.user_attributes(user),
          org: Analytics::MetricsRecorder.org_attributes(org),
          timestamp: stub_time.to_i,
          clc: Analytics::MetricsRecorder.clc_attributes(clc_badging),
          additional_data: metadata,
          changed_column: 'name',
          old_val: "CLC BADGE TEST",
          new_val: "CLC BADGE TEST EDITED"
          )
        )
      Timecop.freeze(stub_time) do
        clc_badging.save
      end    
    end
  end
end
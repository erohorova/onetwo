require 'test_helper'

class TeamLevelMetricTest < ActiveSupport::TestCase

  setup do
    @org = create(:organization)
    @team = create(:team, organization: @org)

    time_period_offsets_from = PeriodOffsetCalculator.applicable_offsets((Time.now - 1.year).utc.beginning_of_day)[:month]
    time_period_offsets_to = PeriodOffsetCalculator.applicable_offsets(Time.now.utc.beginning_of_day)[:month]
    @offsets = [time_period_offsets_from, time_period_offsets_to]

  end

  test "get_team_metrics_over_span should return appropriate data for team_level_metrics" do
    Timecop.freeze do

      now = Time.now.utc
      
      smartbites_consumed = 0
      smartbites_created = 0
      (1..10).each do |indx|
        create(:team_level_metric, team: @team, smartbites_created: indx, smartbites_consumed: (10-indx), time_spent: indx*5, smartbites_comments_count: indx*13, smartbites_liked: indx*17, period: :month, offset: PeriodOffsetCalculator.applicable_offsets(now - indx.month)[:month])
        smartbites_created += indx
        smartbites_consumed += (10-indx)
      end
      
      team_metrics = TeamLevelMetric.get_team_metrics_over_span(
        team_id: @team.id, period: :month, begin_offset: @offsets.first, end_offset: @offsets.last)

      assert_equal [:smartbites_created, :smartbites_consumed], team_metrics.keys
      assert_equal smartbites_consumed, team_metrics[:smartbites_consumed]
      assert_equal smartbites_created, team_metrics[:smartbites_created]
    end
  end

  test "get_team_data_for should return appropriate drilldown data for team level metrics" do
    Timecop.freeze do

      now = Time.now.utc
      
      (1..10).each do |indx|
        create(:team_level_metric, team: @team, smartbites_created: indx, smartbites_consumed: (10-indx), time_spent: indx*5, smartbites_comments_count: indx*13, smartbites_liked: indx*17, period: :month, offset: PeriodOffsetCalculator.applicable_offsets(now - indx.month)[:month])
      end

      drill_down_period_and_offsets = PeriodOffsetCalculator.drill_down_period_and_offsets_for_timerange(period: :year, timerange: [(Time.now - 1.year).utc.beginning_of_day, Time.now.utc.beginning_of_day])
      team_data = TeamLevelMetric.get_team_data_for(organization_id: @org.id, team_id: @team.id, period: drill_down_period_and_offsets.first, offsets: drill_down_period_and_offsets.last, timerange: [(Time.now - 1.year).utc.beginning_of_day, Time.now.utc.beginning_of_day])
      
      assert_equal [0, 0, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1], team_data[:smartbites_created]
      assert_equal [0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9], team_data[:smartbites_consumed]
      assert_same_elements [:smartbites_created, :smartbites_consumed, :average_session, :active_users], team_data.keys
    end
  end

end
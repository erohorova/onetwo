require 'test_helper'

class SkillTest < ActiveSupport::TestCase
  should have_many :skills_users
  should validate_presence_of(:name)
  should validate_uniqueness_of(:name)
end

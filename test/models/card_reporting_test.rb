require 'test_helper'
class CardReportingTest < ActiveSupport::TestCase
  should have_db_column(:card_id).of_type(:integer)
  should have_db_column(:user_id).of_type(:integer)
  should have_db_column(:reason).of_type(:string)
  should have_db_column(:deleted_at).of_type(:datetime)

  should belong_to :card
  should belong_to :user

  test 'validates uniqueness of card scoped to user' do
    org = create(:organization)

    card = create(:card, author_id: nil, organization: org)
    user = create(:user, organization: org)
    cr_1 = create(:card_reporting, user: user, card: card, reason: 'some reason')
    cr_2 = CardReporting.new(user: user, card: card, reason: 'some reason')

    refute cr_2.valid?
  end

  test 'should belong to same org' do
    org1 = create(:organization)
    org2 = create(:organization)

    c1 = create(:card, author_id: nil, organization: org1)
    c2 = create(:card, author_id: nil, organization: org2)
    user = create(:user, organization: org1)

    assert CardReporting.new(user: user, card: c1, reason: 'some reason').valid?
    refute CardReporting.new(user: user, card: c2, reason: 'some reason').valid?
  end
end

require 'test_helper'

class UserConnectionTest < ActiveSupport::TestCase

  setup do
    skip
    @user = create(:user)
    @user_fb_friend = create(:user)
    @user_li_friend = create(:user)

    create(:user_connection, :facebook, user: @user, connection_id: @user_fb_friend.id)
    create(:user_connection, :linkedin, user: @user, connection_id: @user_li_friend.id)
  end
  test "should show facebook scope connections" do
    fb_conn = UserConnection.facebook
    assert_equal 1, fb_conn.count
    assert_equal 'facebook', fb_conn.first.network
    assert_equal @user_fb_friend.id, fb_conn.first.connection_id
  end

  test "should show linkedin scope connections" do
    li_conn = UserConnection.linkedin
    assert_equal 1, li_conn.count
    assert_equal 'linkedin', li_conn.first.network
    assert_equal @user_li_friend.id, li_conn.first.connection_id
  end

  test 'all connections in group should work' do
    conns = @user.all_connections
    assert_equal 2, conns.count

    g = create(:group)
    g.add_member @user_fb_friend

    @user.reload
    conns_in_group = @user.all_connections_in_group(g)
    assert_equal [@user_fb_friend], conns_in_group
  end

end

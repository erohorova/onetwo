require 'test_helper'

class SourcesCredentialTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper
  should validate_presence_of(:source_id)

  class UserOnboardingRecorderTest < ActiveSupport::TestCase
    test "should push to influx source credential created event when source credential created" do
      TestAfterCommit.enabled = true
      org   = create :organization
      user  = create(:user, organization: org, organization_role: 'member')
      actor = create(:user, organization: org, organization_role: 'admin')

      auth_api_info = {"auth_required":true, "url":" asdsa ", "method":"POST", "host":"asdsa das"}

      sources_credential = SourcesCredential.new(
        id:             10000001,
        organization:   org, 
        source_id:      "8b5ab2f1", 
        shared_secret:  "11a8a57",
        auth_api_info:  auth_api_info,
        source_name:    "TEST"
      )
      stub_time = Time.now
      metadata = { user_id: actor.id }    
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries(
          recorder:         "Analytics::OrgMetricsRecorder",
          event:            "org_sources_credential_created",
          actor:            Analytics::MetricsRecorder.user_attributes(actor),
          org:              Analytics::MetricsRecorder.org_attributes(org),
          sources_credential:  Analytics::MetricsRecorder.sources_credential_attributes(sources_credential),
          timestamp:        stub_time.to_i, 
          additional_data:  metadata
        )
      )
      Timecop.freeze(stub_time) do
        sources_credential.save
      end    
    end

    test "should push to influx source credential created event when source credential deleted" do
      TestAfterCommit.enabled = true
      org   = create :organization
      user  = create(:user, organization: org, organization_role: 'member')
      actor = create(:user, organization: org, organization_role: 'admin')

      auth_api_info = {"auth_required":true, "url":"asdsa", "method":"POST", "host":"asdsa das"}

      sources_credential = SourcesCredential.create(
        id:             10000001,
        organization:   org, 
        source_id:      "8b5ab2f1", 
        shared_secret:  "11a8a57",
        auth_api_info:  auth_api_info,
        source_name:    "TEST"
      )
      stub_time = Time.now
      metadata  = { user_id: actor.id }    
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries(
          recorder:         "Analytics::OrgMetricsRecorder",
          event:            "org_sources_credential_deleted",
          actor:            Analytics::MetricsRecorder.user_attributes(actor),
          org:              Analytics::MetricsRecorder.org_attributes(org),
          sources_credential:  Analytics::MetricsRecorder.sources_credential_attributes(sources_credential),
          timestamp:        stub_time.to_i, 
          additional_data:  metadata
        )
      )
      Timecop.freeze(stub_time) do
        sources_credential.destroy
      end    
    end

    test "should push to influx source credential created event when source credential edited" do
      TestAfterCommit.enabled = true
      org   = create :organization
      user  = create(:user, organization: org, organization_role: 'member')
      actor = create(:user, organization: org, organization_role: 'admin')

      auth_api_info = {"auth_required":true, "url":"asdsa", "method":"POST", "host":"asdsa das"}

      sources_credential = SourcesCredential.create(
        id:             10000001,
        organization:   org, 
        source_id:      "8b5ab2f1", 
        shared_secret:  "11a8a57",
        auth_api_info:  auth_api_info,
        source_name:    "TEST"
      )
      stub_time = Time.now
      metadata  = { user_id: actor.id }    
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      sources_credential.source_name = "Test source name edited"
      Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries(
          recorder:         "Analytics::OrgEditedMetricsRecorder",
          event:            "org_sources_credential_edited",
          actor:            Analytics::MetricsRecorder.user_attributes(actor),
          org:              Analytics::MetricsRecorder.org_attributes(org),
          sources_credential:  Analytics::MetricsRecorder.sources_credential_attributes(sources_credential),
          timestamp:        stub_time.to_i, 
          additional_data:  metadata,
          changed_column: 'source_name',
          old_val: "TEST",
          new_val: "Test source name edited"
        )
      )
      Timecop.freeze(stub_time) do
        sources_credential.save
      end    
    end
  end
end

require 'test_helper'

class ClcTeamsRecordTest < ActiveSupport::TestCase
  should belong_to(:team)
  should belong_to(:user)
  should belong_to(:card)
  should validate_presence_of(:team_id)
  should validate_presence_of(:user_id)
  should validate_presence_of(:card_id)


  test 'validates uniquess of user scoped to card - team' do
    org = create(:organization)

    card = create(:card, author_id: nil, organization: org)
    user = create(:user, organization: org)
    user1 = create(:user, organization: org)

    team = create(:team, organization: org)
    team1 = create(:team, organization: org)

    clc_team_record  = create(:clc_teams_record, team: team, card: card, user: user)
    clc_team_record1 = build(:clc_teams_record, team: team, card: card, user: user)

    refute clc_team_record1.valid?
    assert_equal "User has already been taken", clc_team_record1.full_errors

    clc_team_record2  = build(:clc_teams_record, team: team, card: card, user: user1)
    assert clc_team_record2.valid?
  end
end

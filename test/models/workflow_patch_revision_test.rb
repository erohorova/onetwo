require 'test_helper'

class WorkflowPatchRevisionTest < ActiveSupport::TestCase
  setup do
    @org = create(:organization)
    @user = create(:user, organization: @org)

    @sample = WorkflowPatchRevision.create(
      organization_id: @org.id,
      wuid: '11112',
      data_id: '111',
      workflow_id: 1,
      output_source_id: 2,
      workflow_type: 'workflow_type',
      patch_id: 'patch_id',
      patch_type: 'patch_type',
      org_name: 'org_name',
      data: {},
      params: {},
      meta: {},
      completed: false,
      in_progress: false,
      processing_info: {info: [], errors: []},
      version: 0,
      job_id: nil,
      failed: false
    )
  end

  class PatchRevisionOnCommit < ActiveJob::TestCase
    include ActiveJob::TestHelper

    setup do
      WorkflowsJob.unstub(:perform_later)
      WorkflowsV2Job.unstub(:perform_later)
      EdcastActiveJob.unstub(:perform_later)
    end

    test 'should enqueue a job after_commit' do
      assert_enqueued_with(job: WorkflowsJob, queue: 'bulk') do
        WorkflowPatchRevision.create(
          wuid: 'unique_workflow_identifier',
          failed: false
        ).committed!
      end
    end

    test 'should enqueue a job after_commit version 2' do
      assert_enqueued_with(job: WorkflowsV2Job, queue: 'bulk') do
        WorkflowPatchRevision.create(
          wuid: 'uniq_workflow_identifier',
          version: 2,
          failed: false
        ).committed!
      end
    end

    test 'should not enqueue a job after_commit' do
      assert_no_enqueued_jobs do
        WorkflowPatchRevision.create(
          failed: true
        ).committed!
      end
    end
  end

  test '#update_info should properly add object to processing_info' do
    info = { info: 'some info' }.with_indifferent_access
    @sample.update_info(info)

    assert_equal info, @sample.processing_info['info'].first

    @sample.update_attributes(processing_info: nil)
    assert_nil @sample.processing_info

    @sample.update_info(info)

    exp_processing_info = {
      info: [{ info: 'some info' }]
    }.with_indifferent_access
    assert_equal exp_processing_info, @sample.processing_info
  end

  test '#update_errors should properly add error description to processing_info' do
    err = { error: 'some error' }.with_indifferent_access
    @sample.update_errors(err)

    assert_equal err, @sample.processing_info['errors'].first
    assert @sample.failed

    @sample.update_attributes(processing_info: nil, failed: false)
    assert_nil @sample.processing_info
    refute @sample.failed

    @sample.update_errors(err)

    exp_processing_info = {
      errors: [{ error: 'some error' }]
    }.with_indifferent_access
    assert_equal exp_processing_info, @sample.processing_info
  end

  test '#update_info should validate passing argv' do
    assert_raise RuntimeError do
      @sample.update_info('')
    end
  end

  test '#update_errors should validate passing argv' do
    assert_raise RuntimeError do
      @sample.update_errors('')
    end
  end

  test '#set_processing' do
    # would update to initial state in case manual retry of particular patch
    stub_time = "2018-11-22T17:59:21.500Z"
    Timecop.freeze stub_time do
      @sample.set_processing('sample_job_id')
    end
    @sample.reload

    refute @sample.failed
    refute @sample.completed
    assert_equal 'sample_job_id', @sample.job_id
    assert_equal stub_time, @sample.processing_info['info'].first['started_at']
  end
end

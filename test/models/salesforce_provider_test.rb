require 'test_helper'

class SalesforceProviderTest < ActiveSupport::TestCase

  test 'should allow exacttarget.com email to go through' do
    assert_nothing_raised do
      SalesforceProvider.is_verified?({info: {email: 'user@exacttarget.com'}, extra: {email_verified: false}})
    end
  end

  test 'should allow salesforce.com email to go through' do
    assert_nothing_raised do
      SalesforceProvider.is_verified?({info: {email: 'user@salesforce.com'}, extra: {email_verified: false}})
    end
  end
end

require 'test_helper'

class TopicDailyLearningTest < ActiveSupport::TestCase

  test 'should set initial state on creation' do
    skip
    tdl = create(:topic_daily_learning)
    assert_equal DailyLearningProcessingStates::INITIALIZED, tdl.processing_state
    assert tdl.requested_by_user
  end

  class TopicDailyLearningProcessTest < ActiveSupport::TestCase
    setup do
      skip
      @org = create(:organization)
      @user = create(:user, organization: @org)
      @date = Time.now.utc.to_date.to_s
      @topic_id = "xyx"
      @topic_name = "edcast.xyz"
      @tdl = create(:topic_daily_learning, :initialized, organization_id: @org.id, date: @date, topic_id: @topic_id, topic_name: @topic_name)
    end

    test 'process should call service and mark as completed' do
      cards = create_list(:card, 3, author_id: nil, organization_id: @org.id)
      TopicDailyLearningService.any_instance.expects(:get_ecl_ids).with(@topic_name, @org.id, @date).returns(cards.map(&:id))
      @tdl.process!
      @tdl.reload
      assert_equal DailyLearningProcessingStates::COMPLETED, @tdl.processing_state
      assert_equal cards.map(&:id), @tdl.ecl_ids
    end

    test 'process should mark as errored on error and raise again' do
      TopicDailyLearningService.any_instance.expects(:get_ecl_ids).with(@topic_name, @org.id, @date).raises(StandardError)
      assert_raises(StandardError) do
        @tdl.process!
      end
      @tdl.reload
      assert_equal DailyLearningProcessingStates::ERROR, @tdl.processing_state
    end
  end
end

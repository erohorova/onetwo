require 'test_helper'

class ChannelsCuratorTest < ActiveSupport::TestCase
  should belong_to(:channel)
  should belong_to(:user)

  class ValidationTest < ActiveSupport::TestCase
    test 'user and channel should be in the same org' do
      user = create(:user)
      channel = create(:channel, organization: create(:organization))
      channels_curator = ChannelsCurator.new(user: user, channel: channel)
      assert_equal false, channels_curator.valid?
    end
  end

  class HelperMethodsTest < ActiveSupport::TestCase
    test "#should_belong_to_same_org" do
      org1 = create(:organization)
      channel1 = create :channel, organization: org1
      user1 = create :user, organization: org1
      org2 = create :organization
      user2 = create :user, organization: org2
      valid_channels_curator = ChannelsCurator.new(channel: channel1, user: user1)
      invalid_channels_curator = ChannelsCurator.new(channel: channel1, user: user2)
      [valid_channels_curator, invalid_channels_curator].each(&:should_belong_to_same_org)
      assert_empty valid_channels_curator.errors
      assert_equal invalid_channels_curator.errors.to_h, {
        base: "User and Channel Organizations don't match"
      }
    end

    test "#record_add_curator_event" do
      org = create(:organization)
      actor = create :user, organization: org
      curator = create :user, organization: org
      channel = create :channel, organization: org

      now = Time.now
      Time.stubs(:now).returns now

      metadata = { user_id: actor.id }
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      channels_curator = ChannelsCurator.new channel: channel, user: curator
      event = 'channel_curator_added'
      Analytics::MetricsRecorderJob.unstub(:perform_later)
      Analytics::MetricsRecorderJob.expects(:perform_later).with(
        recorder: 'Analytics::ChannelCuratorRecorder',
        event: event,
        channel: Analytics::MetricsRecorder.channel_attributes(channel),
        curator: Analytics::MetricsRecorder.user_attributes(curator),
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        additional_data: metadata,
        timestamp: now.to_i
      )
      channels_curator.save
      channels_curator.run_callbacks(:commit)
    end

    test "#record_remove_curator_event" do
      org = create(:organization)
      actor = create :user, organization: org
      curator = create :user, organization: org
      channel = create :channel, organization: org
      metadata = { foo: "bar", user_id: actor.id }
      now = Time.now
      Time.stubs(:now).returns now

      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      channels_curator = ChannelsCurator.new channel: channel, user: curator
      event = 'channel_curator_removed'
      Analytics::MetricsRecorderJob.unstub(:perform_later)
      Analytics::MetricsRecorderJob.expects(:perform_later).with(
        recorder: 'Analytics::ChannelCuratorRecorder',
        event: event,
        channel: Analytics::MetricsRecorder.channel_attributes(channel),
        curator: Analytics::MetricsRecorder.user_attributes(curator),
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        additional_data: metadata,
        timestamp: now.to_i
      )
      channels_curator.destroy
    end
  end

end

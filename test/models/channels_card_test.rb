require 'test_helper'

class ChannelsCardTest < ActiveSupport::TestCase

  test 'should removed card from structured_item if removed from channel' do
    organization = create(:organization)
    user = create(:user, organization: organization)
    card = create(:card, card_type: 'media', card_subtype: 'text', author_id: user.id, title: 'This is private card', message: 'This is message',  is_public: true, organization_id: organization.id)
    channel = create(:channel, organization: organization, is_open: true)
    channel_card = create(:channels_card, channel_id: channel.id, card_id: card.id)
    channel1 = create(:channel, organization: organization, is_open: true)
    channel_card1 = create(:channels_card, channel_id: channel1.id, card_id: card.id)
    structure = create(:structure, creator_id: user.id, organization_id: organization.id, parent_id: channel.id, parent_type: 'Channel', slug: 'channel-cards')
    structure_item = create(:structured_item, structure: structure, entity: card)

    structure1 = create(:structure, creator_id: user.id, organization_id: organization.id, parent_id: channel1.id, parent_type: 'Channel', slug: 'channel-cards')
    structure_item1 = create(:structured_item, structure: structure1, entity: card)

    structure_item_id = structure_item.id
    channel_card.destroy
    #Should remove structured item from channel
    assert_nil StructuredItem.find_by_id(structure_item_id)

    #Should not remove structured item from channel1
    assert_equal structure_item1.id, StructuredItem.find_by_id(structure_item1.id).id
  end

  test 'should belong to same org' do
    org1 = create(:organization)
    org2 = create(:organization)

    c1 = create(:card, author_id: nil, organization: org1)
    c2 = create(:card, author_id: nil, organization: org2)
    ch = create(:channel, organization: org1)

    assert ChannelsCard.new(channel: ch, card: c1).valid?
    refute ChannelsCard.new(channel: ch, card: c2).valid?
  end

  test 'validates uniquess of card scoped to channel' do
    skip("INTERMITTENT FAILURE ON TRAVIS")
    org = create(:organization)

    card = create(:card, author_id: nil, organization: org)
    channel  = create(:channel, organization: org)
    channel2 = create(:channel, organization: org)

    card.channels << channel
    card.channels << channel2

    assert_raises(ActiveRecord::RecordInvalid) { card.channels << channel }
  end

  test 'set default state is curated if channel is not curate only' do
    org = create(:organization)
    user = create(:user, organization: org)
    channel = create(:channel, organization: org, curate_only: false)
    card = create(:card, organization: org, author: user)
    Timecop.freeze do
      ChannelCardEnqueueJob.expects(:perform_later).never
      card.channels << channel
      assert_equal ChannelsCard::STATE_CURATED, ChannelsCard.where(channel: channel, card: card).first.state.to_sym
      assert_equal Time.now.utc.to_i, ChannelsCard.where(channel: channel, card: card).first.curated_at.to_i
    end
  end

  test 'set default state is curated for UGC cards' do
    org = create(:organization)
    user = create(:user, organization: org)
    channel = create(:channel, organization: org, curate_only: true)
    card = create(:card, organization: org, author: user)
    card.current_user = user
    Timecop.freeze do
      ChannelCardEnqueueJob.expects(:perform_later).never
      card.channels << channel
      assert_equal ChannelsCard::STATE_CURATED, ChannelsCard.where(channel: channel, card: card).first.state.to_sym
      assert_equal Time.now.utc.to_i, ChannelsCard.where(channel: channel, card: card).first.curated_at.to_i
    end
  end


  test "should set appropriate rank to related cards when assigned rank to card in a carousel" do
    org = create(:organization)
    user = create(:user, organization: org)
    channel = create(:channel, organization: org, curate_only: true)
    card_1 = create(:card, organization: org, author: user, state: 'published')
    card_2 = create(:card, organization: org, author: user, state: 'published')
    card_3 = create(:card, organization: org, author: user, state: 'published')
    card_4 = create(:card, organization: org, author: user, state: 'published')
    card_5 = create(:card, organization: org, author: user, state: 'published')
    channel.cards << [card_1, card_2, card_3, card_4, card_5]
    channel.channels_cards.each_with_index do |cc, index|
      cc.state = "curated"
      cc.save
    end

    cc_1 = ChannelsCard.find_by({channel_id: channel.id, card_id: card_1.id})
    cc_2 = ChannelsCard.find_by({channel_id: channel.id, card_id: card_2.id})
    cc_3 = ChannelsCard.find_by({channel_id: channel.id, card_id: card_3.id})
    cc_4 = ChannelsCard.find_by({channel_id: channel.id, card_id: card_4.id})
    cc_5 = ChannelsCard.find_by({channel_id: channel.id, card_id: card_5.id})

    #current rank of channels_cards [{cc_1 => nil, cc_2 => nil, cc_3 => nil, cc_4 => nil, cc_5 => nil}]
    #set rank 1 to card_1 in channel(from nil to 1)
    cc_1.rank = 1
    cc_1.save

    assert_equal 1, cc_1.rank
    assert_nil cc_2.rank
    assert_nil cc_3.rank
    assert_nil cc_4.rank
    assert_nil cc_5.rank

    #current rank of channels_cards [{cc_1 => 1, cc_2 => nil, cc_3 => nil, cc_4 => nil, cc_5 => nil}]
    #set rank 1 to card_2 in channel(from nil to 1). card_1 rank should become 2
    cc_2.rank = 1
    cc_2.save

    [cc_1, cc_2, cc_3, cc_4, cc_5].each{|c| c.reload}

    assert_equal 1, cc_2.rank
    assert_equal 2, cc_1.rank
    assert_nil cc_3.rank
    assert_nil cc_4.rank
    assert_nil cc_5.rank

    #current rank of channels_cards [{cc_1 => 2, cc_2 => 1, cc_3 => nil, cc_4 => nil, cc_5 => nil}]
    #set rank to other cards and check if set correctly
    cc_3.rank = 2
    cc_3.save

    cc_4.rank = 4
    cc_4.save

    cc_5.rank = 5
    cc_5.save

    [cc_1, cc_2, cc_3, cc_4, cc_5].each{|c| c.reload}

    assert_equal 1, cc_2.rank
    assert_equal 2, cc_3.rank
    assert_equal 3, cc_1.rank
    assert_equal 4, cc_4.rank
    assert_equal 5, cc_5.rank

    #current rank of channels_cards [{cc_1 => 3, cc_2 => 1, cc_3 => 2, cc_4 => 4, cc_5 => 5}]
    #chaning cc_5 from rank 5 to 3
    cc_5.reload
    cc_5.rank = 3
    cc_5.save

    [cc_1, cc_2, cc_3, cc_4, cc_5].each{|c| c.reload}

    assert_equal 1, cc_2.rank
    assert_equal 2, cc_3.rank
    assert_equal 3, cc_5.rank
    assert_equal 4, cc_1.rank
    assert_equal 5, cc_4.rank

    #current rank of channels_cards [{cc_1 => 4, cc_2 => 1, cc_3 => 2, cc_4 => 5, cc_5 => 3}]
    #chaning cc_2 from rank 1 to 4
    cc_2.rank = 4
    cc_2.save

    [cc_1, cc_2, cc_3, cc_4, cc_5].each{|c| c.reload}

    assert_equal 1, cc_3.rank
    assert_equal 2, cc_5.rank
    assert_equal 3, cc_1.rank
    assert_equal 4, cc_2.rank
    assert_equal 5, cc_4.rank

    #current rank of channels_cards [{cc_1 => 3, cc_2 => 4, cc_3 => 1, cc_4 => 5, cc_5 => 2}]
    #destroring card at rank 3 should reorder cards
    cc_1.destroy
    [cc_2, cc_3, cc_4, cc_5].each{|c| c.reload}

    assert_equal 1, cc_3.rank
    assert_equal 2, cc_5.rank
    assert_equal 3, cc_2.rank
    assert_equal 4, cc_4.rank
  end

  test "should reorder rank after destroy" do
    org = create(:organization)
    user = create(:user, organization: org)
    channel = create(:channel, organization: org, curate_only: true)
    card_1 = create(:card, organization: org, author: user, state: 'published')
    card_2 = create(:card, organization: org, author: user, state: 'published')
    card_3 = create(:card, organization: org, author: user, state: 'published')
    channel.cards << [card_1, card_2, card_3]
    channel.channels_cards.each_with_index do |cc, index|
      cc.state = "curated"
      cc.rank = index + 1
      cc.save
    end
    channel_card = channel.channels_cards.order(:rank)[2]
    channel_card.destroy
    assert_equal [1, 2], channel.channels_cards.pluck(:rank).sort
  end

  test 'set default state is new if channel is curate only' do
    org = create(:organization)
    channel = create(:channel, organization: org, curate_only: true)
    user = create(:user, organization: org)
    card = create(:card, organization: org, author: nil)
    Timecop.freeze do
      card.channels << channel
      assert_equal ChannelsCard::STATE_NEW, ChannelsCard.where(channel: channel, card: card).first.state.to_sym
      assert_equal nil, ChannelsCard.where(channel: channel, card: card).first.curated_at
    end
    ChannelCardEnqueueJob.expects(:perform_later).with(channel.id, card.id)
    channels_card = ChannelsCard.where(channel: channel, card: card).first
    channels_card.curate
    channels_card.save
  end

  test 'non ugc cards posted by curator should not require curation' do
    org = create(:organization)
    channel = create(:channel, organization: org, curate_only: true)
    user = create(:user, organization: org)
    card = create(:card, organization: org, author: nil)
    card.current_user = user
    channel.curators << user

    ChannelCardEnqueueJob.expects(:perform_later).never
    card.channels << channel
    assert ChannelsCard.last.curated?
  end

  test 'non ugc cards posted by curator should require curation with permission if trusted_collaborators_enabled and user follower' do
    org = create(:organization, enable_role_based_authorization: true)
    create(:config, configable: org, name: 'trusted_collaborators_enabled', value: true)
    channel = create(:channel, organization: org, curate_only: true)
    user = create(:user, organization: org)
    card = create(:card, organization: org, author: nil)
    Role.create(name: 'custom', organization_id: org.id, master_role: false)
    user.add_role('custom')
    create(:role_permission, role: Role.find_by(name: 'custom'),
      name: Permissions::BYPASS_CURATION
    )
    card.current_user = user
    channel.followers << user

    ChannelCardEnqueueJob.expects(:perform_later).never
    card.channels << channel
    assert ChannelsCard.last.new?
  end

  test 'non ugc cards posted by curator should not require curationif trusted_collaborators_enabled' do
    org = create(:organization, enable_role_based_authorization: true)
    create(:config, configable: org, name: 'trusted_collaborators_enabled', value: true)
    channel = create(:channel, organization: org, curate_only: true)
    user = create(:user, organization: org)
    card = create(:card, organization: org, author: nil)
    card.current_user = user
    channel.curators << user

    ChannelCardEnqueueJob.expects(:perform_later).never
    card.channels << channel
    assert ChannelsCard.last.curated?
  end

  test 'non ugc cards posted by other than curators should require curation' do
    org = create(:organization)
    channel = create(:channel, organization: org, curate_only: true)
    user = create(:user, organization: org)
    card = create(:card, organization: org, author: nil)
    card.current_user = user

    card.channels << channel
    channel_card = ChannelsCard.last
    assert channel_card.new?
    assert_not channel_card.curated?

    ChannelCardEnqueueJob.expects(:perform_later).with(channel.id, card.id)
    channel_card.curate!
    assert  channel_card.curated?
  end

  test 'set default state is curate for UGC cards with collaborators without bypass permission' do
    org = create(:organization)
    create(
      :config, configable_id: org.id, configable_type: 'Organization',
      name: 'channel/curation_for_followers_card', data_type: 'boolean',
      value: 'true'
    )

    user = create(:user, organization: org)
    channel = create(:channel, organization: org, curate_only: true)
    card = create(:card, organization: org, author: user)
    user1 = create(:user, organization: org)
    channel.authors << user1
    card.current_user = user1
    ChannelCardEnqueueJob.expects(:perform_later).never
    card.channels << channel
    state = ChannelsCard.where(channel: channel, card: card).first.state.to_sym
    assert_equal ChannelsCard::STATE_CURATED, state
  end

  test 'set default state is curated for UGC cards with followers if curation is not required' do
    org = create(:organization)
    create(
      :config, configable_id: org.id, configable_type: 'Organization',
      name: 'channel/curation_for_followers_card', data_type: 'boolean',
      value: 'false'
    )
    user = create(:user, organization: org)
    user1 = create(:user, organization: org)
    channel = create(:channel, organization: org, curate_only: true)
    card = create(:card, organization: org, author: user)
    channel.add_followers [user1]

    card.current_user = user1
    ChannelCardEnqueueJob.expects(:perform_later).never
    card.channels << channel
    assert_equal ChannelsCard::STATE_CURATED, ChannelsCard.where(channel: channel, card: card).first.state.to_sym
    # STOP WRITING SUCH FRAGILE TEST ASSERTIONS
    # assert_equal Time.now.utc.to_i, ChannelsCard.where(channel: channel, card: card).first.curated_at.to_i
  end

  test 'set default state is new for UGC cards with followers if curation is required' do
    org = create(:organization)
    config   = create :config, configable_id: org.id, configable_type: 'Organization', name: 'channel/curation_for_followers_card', data_type: "boolean", value: 'true'

    user = create(:user, organization: org)
    user1 = create(:user, organization: org)
    channel = create(:channel, organization: org, curate_only: true)
    card = create(:card, organization: org, author: user)

    card.current_user = user1
    channel.add_followers [user1]

    ChannelCardEnqueueJob.expects(:perform_later).never
    card.channels << channel

    assert_equal ChannelsCard::STATE_NEW, ChannelsCard.where(channel: channel, card: card).first.state.to_sym
  end

  test 'should trigger UsersCardsManagementJob after creating channels_card' do
    org = create :organization
    user = create :user, organization: org
    author = create :user, organization: org
    card = create :card, organization: org, author: author
    channel = create :channel, organization: org

    channels_card = create :channels_card, card_id: card.id, channel_id: channel.id
    channels_card.card.current_user = user

    values = [user.id, card.id, UsersCardsManagement::ACTION_POST_TO_CHANNEL, channel.id]
    UsersCardsManagementJob.expects(:perform_later).with(*values).once

    channels_card.run_callbacks(:commit)
  end

  test 'skip curation process if user channel_author have bypass_curation permission and role collaborator for trusted_collaborators_enabled' do
    org = create(:organization)
    create(:config, configable: org, name: 'trusted_collaborators_enabled', value: true)
    org.update(enable_role_based_authorization: true)
    Role.create(
      name: 'custom', organization_id: org.id,
      default_name: 'collaborator', master_role: false
    )
    Role.create(name: 'new_role', organization_id: org.id, master_role: false)
    user, user2 = create_list(:user, 2, organization: org)

    user.add_role('collaborator')
    user2.add_role('new_role')

    create(:role_permission, role: Role.find_by(name: 'custom'),
      name: Permissions::BYPASS_CURATION
    )

    channel = create(
      :channel, organization: org, curate_only: true, curate_ugc: true
    )

    card, card2 = create_list(:card, 2, organization: org, author: user)

    card.current_user = user
    card2.current_user = user2

    channel.add_authors [user, user2]

    ChannelCardEnqueueJob.expects(:perform_later).never
    card.channels << channel
    card2.channels << channel
    ch_card = ChannelsCard.find_by(channel: channel, card: card).state.to_sym
    ch_card2 = ChannelsCard.find_by(channel: channel, card: card2).state.to_sym


    assert user.authorize?(Permissions::BYPASS_CURATION)
    refute user2.authorize?(Permissions::BYPASS_CURATION)
    assert_equal ChannelsCard::STATE_CURATED, ch_card
    assert_equal ChannelsCard::STATE_NEW, ch_card2
  end

  test '#curation_required? without config trusted_collaborators_enabled' do
    org = create(:organization, enable_role_based_authorization: true)
    user, user1, user2 = create_list(:user, 3, organization: org)
    channel = create(
      :channel, organization: org, curate_only: true, curate_ugc: true
    )
    card, card1, card2 = create_list(:card, 3, organization: org, author: user)

    # curator
    channel.add_curators([user])
    card.current_user = user
    ch_card = ChannelsCard.new(channel: channel, card: card)
    refute ch_card.curation_required?

    # collaborator(author)
    channel.add_authors([user1])
    card1.current_user = user1
    ch_card1 = ChannelsCard.new(channel: channel, card: card1)
    refute ch_card1.curation_required?

    # follower
    channel.add_followers([user2])
    card2.current_user = user2
    ch_card2 = ChannelsCard.new(channel: channel, card: card2)
    assert ch_card2.curation_required?
  end

  test '#curation_required? with config trusted_collaborators_enabled' do
    org = create(:organization, enable_role_based_authorization: true)
    create(:config, configable: org, name: 'trusted_collaborators_enabled', value: true)
    role = Role.create(name: 'collaborator', organization_id: org.id, master_role: true)
    create(:role_permission, role: role, name: Permissions::BYPASS_CURATION)
    user, user1, user2 = create_list(:user, 3, organization: org)
    channel = create(
      :channel, organization: org, curate_only: true, curate_ugc: true
    )
    card, card1, card2 = create_list(:card, 3, organization: org, author: user)

    # curator
    channel.add_curators([user])
    card.current_user = user
    ch_card = ChannelsCard.new(channel: channel, card: card)
    refute ch_card.curation_required?

    # collaborator(author)
    channel.add_authors([user1])
    card1.current_user = user1
    ch_card1 = ChannelsCard.new(channel: channel, card: card1)
    # without BYPASS_CURATION
    assert ch_card1.curation_required?
    # with BYPASS_CURATION
    user1.add_role('collaborator')
    refute ch_card1.curation_required?

    # trusted_collaborator(trusted_author)
    channel.add_authors([user1], trusted: true)
    card1.current_user = user1
    ch_card1 = ChannelsCard.new(channel: channel, card: card1)
    refute ch_card1.curation_required?

    # follower
    channel.add_followers([user2])
    card2.current_user = user2
    ch_card2 = ChannelsCard.new(channel: channel, card: card2)
    assert ch_card2.curation_required?
  end

  class SharedChannelCardSyncTest < ActiveSupport::TestCase
    setup do
      TestAfterCommit.enabled = true
      @org1, @org2 = create_list(:organization, 2)
      @user = create(:user, organization: @org1)
      @org1_channel = create(:channel, organization: @org1, shareable: true)
      @org2_channel = create(:channel, organization: @org2, parent_id: @org1_channel.id)
      @card = create(:card, organization: @org1, author: @user)
    end

    test "should trigger sync channel cards job on adding card to shareable non-curated channel" do
      @card.current_user = @user

      job = mock()
      SyncCloneChannelCardsJob.expects(:set).with({wait_until: 10.seconds}).returns(job)
      job.expects(:perform_later).with({parent_card_id: @card.id, parent_channel_id: @org1_channel.id, action: 'add'})

      @card.channels << @org1_channel
    end

    test "should trigger sync channel cards job on deleting card from shareable channel" do
      @card.current_user = @user
      @card.channels << @org1_channel

      job = mock()
      SyncCloneChannelCardsJob.expects(:set).with({wait_until: 10.seconds}).returns(job)
      job.expects(:perform_later).with({parent_card_id: @card.id, parent_channel_id: @org1_channel.id, action: 'destroy'})

      ChannelsCard.where(card_id: @card.id, channel_id: @org1_channel).first.destroy
    end

    test "should not trigger sync channel cards job on adding card to shareable curated channel" do
      org1_channel = create(:channel, organization: @org1, shareable: true, curate_only: true, curate_ugc: true)
      org2_channel = create(:channel, organization: @org2, parent_id: org1_channel.id)
      card = create(:card, organization: @org1, author: @user)
      card.current_user = @user

      SyncCloneChannelCardsJob.expects(:perform_later).never
      card.channels << org1_channel
    end
  end

  class CardChannelMetricRecorderTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false
    setup do
      Analytics::MetricsRecorderJob.stubs(:perform_later)
      ChannelsCard.any_instance.stubs :create_users_management_card
    end

    test 'should invoke card channel metric recorder job #add_to_channel' do
      skip("INTERMITTENT FAILURE ON TRAVIS")
      IndexJob.stubs(:perform_later).returns true
      Card.any_instance.stubs(:record_card_create_event).returns true
      org = create(:organization)
      actor = create(:user, organization: org)
      metadata = { foo: "bar", user_id: actor.id }
      RequestStore.stubs(:read).with(:request_metadata).returns metadata

      author = create(:user, organization: org)
      card = create(:card, author: author, organization: org)
      channel = create(:channel, organization: org)

      stub_time = Time.now
      Time.stubs(:now).returns stub_time

      Analytics::MetricsRecorderJob.unstub :perform_later
      Analytics::MetricsRecorderJob.expects(:perform_later).once.with(
        recorder: "Analytics::CardChannelRecorder",
        org: Analytics::MetricsRecorder.org_attributes(org),
        channel: Analytics::MetricsRecorder.channel_attributes(channel),
        card: Analytics::MetricsRecorder.card_attributes(card),
        timestamp: stub_time.to_i,
        event: "card_added_to_channel",
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        additional_data: metadata
      )
      Timecop.freeze stub_time do
        card.channels << channel
      end
    end

    test 'should invoke card channel metric recorder job when state changed to curated #add_to_channel' do
      IndexJob.stubs(:perform_later).returns true
      Card.any_instance.stubs(:record_card_create_event).returns true
      org = create(:organization)
      author = create(:user, organization: org)
      card = create(:card, author: author)
      channel = create(:channel, organization: org, curate_only: true, user: author)
      card.channels << channel
      Analytics::MetricsRecorderJob.expects(:perform_later).once
      card.channels_cards.first.curate!
    end

    test 'should invoke card metric recorder job #remove_from_channel' do
      RequestStore.unstub(:read)
      Organization.any_instance.stubs(:create_admin_user)
      IndexJob.stubs(:perform_later).returns true
      Card.any_instance.stubs(:record_card_create_event).returns true
      ChannelsCard.any_instance.stubs(:record_add_to_channel_event).returns true
      org = create(:organization)
      actor = create(:user, organization: org)
      card = create(:card, author: actor)
      channel = create(:channel, organization: org)
      card.channels << channel

      RequestStore.store[:request_metadata] = {
        platform: 'ios', user_agent: 'Edcast iPhone', user_id: actor.id
      }
      Timecop.freeze(2.days.from_now) do
        Analytics::MetricsRecorderJob.expects(:perform_later).with({
          recorder: "Analytics::CardChannelRecorder",
          org: Analytics::MetricsRecorder.org_attributes(org),
          card: Analytics::MetricsRecorder.card_attributes(card),
          channel: Analytics::MetricsRecorder.channel_attributes(channel),
          timestamp: Time.now.to_i,
          event: 'card_removed_from_channel',
          actor: Analytics::MetricsRecorder.user_attributes(actor),
          additional_data: {
            platform: 'ios',
            user_agent: 'Edcast iPhone',
            user_id: actor.id
          }
        }).returns true
        card.channels_cards.last.destroy
      end
    end

  end
end

require 'test_helper'

class MailerConfigTest < ActiveSupport::TestCase
  should belong_to :organization
  should validate_presence_of(:address)
  should validate_presence_of(:port)
  should validate_presence_of(:user_name)
  should validate_presence_of(:password)
  should validate_presence_of(:webhook_keys)
  should validate_presence_of(:domain)
  should validate_presence_of(:from_name)
  should validate_presence_of(:from_address)
  should validate_presence_of(:organization_id)
  should validate_length_of(:port)

  test "domain name validation" do
    mailer_config = build(:mailer_config)
    assert_raise(ActiveRecord::RecordInvalid) do
      @event = MailerConfig.create!(mailer_config.attributes.merge(domain: "domain"))
    end
    mailer_config.domain = "gmail.com"
    assert mailer_config.valid?
  end

  test "address name validation" do
    mailer_config = build(:mailer_config)
    assert_raise(ActiveRecord::RecordInvalid) do
      @event = MailerConfig.create!(mailer_config.attributes.merge(address: "domain"))
    end
    mailer_config.address = "smtp.gmail.com"
    assert mailer_config.valid?
  end

  test "should not delete mailer config if it is active" do
    org = create :organization
    mailer_config = create(:mailer_config, from_name: 'test', from_address: 'test@edcast.com', 
      from_admin: true, organization: org, is_active: true)

    mailer_config.destroy
    assert_equal "Current Sender cannot be deleted", mailer_config.errors.messages[:base].first
  end

  test "records event upon creation" do
    Analytics::MetricsRecorderJob.unstub(:perform_later)
    EdcastActiveJob.unstub(:perform_later)

    TestAfterCommit.enabled = true

    stub_time = Time.now
    Timecop.freeze(stub_time) do
      org = create :organization
      actor = create(:user, organization: org, organization_role: 'admin')
      stub_id = (MailerConfig.last&.id || 0) + 2000
      mailer_config = build :mailer_config, {
        id: stub_id,
        from_address: "foo@bar.com",
        from_name: "foo bar",
        is_active: false,
        verification: {},
        organization: org
      }

      metadata = { user_id: actor.id }
      RequestStore.stubs(:read).with(:request_metadata).returns metadata

      Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries({
        recorder:         "Analytics::OrgMetricsRecorder",
        event:            "org_email_sender_created",
        actor:            Analytics::MetricsRecorder.user_attributes(actor),
        org:              Analytics::MetricsRecorder.org_attributes(org),
        mailer_config:    Analytics::MetricsRecorder.mailer_config_attributes(mailer_config),
        timestamp:        stub_time.to_i,
        additional_data:  metadata
      }))
      mailer_config.save!
    end
  end

  test "records event upon deletion" do
    Analytics::MetricsRecorderJob.unstub(:perform_later)
    EdcastActiveJob.unstub(:perform_later)

    TestAfterCommit.enabled = true

    stub_time = Time.now
    Timecop.freeze(stub_time) do
      org = create :organization
      actor = create(:user, organization: org, organization_role: 'admin')
      mailer_config = create :mailer_config, {
        from_address: "foo@bar.com",
        from_name: "foo bar",
        is_active: false,
        verification: {},
        organization: org
      }

      metadata = { user_id: actor.id }
      RequestStore.stubs(:read).with(:request_metadata).returns metadata

      Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries({
        recorder:         "Analytics::OrgMetricsRecorder",
        event:            "org_email_sender_deleted",
        actor:            Analytics::MetricsRecorder.user_attributes(actor),
        org:              Analytics::MetricsRecorder.org_attributes(org),
        mailer_config:    Analytics::MetricsRecorder.mailer_config_attributes(mailer_config),
        timestamp:        stub_time.to_i,
        additional_data:  metadata
      }))
      mailer_config.destroy
    end
  end

  test "records event upon being edited" do
    Analytics::MetricsRecorderJob.unstub(:perform_later)
    EdcastActiveJob.unstub(:perform_later)

    TestAfterCommit.enabled = true

    stub_time = Time.now
    Timecop.freeze(stub_time) do
      org = create :organization
      actor = create(:user, organization: org, organization_role: 'admin')
      mailer_config = create :mailer_config, {
        from_address: "foo@bar.com",
        from_name: "foo bar",
        is_active: false,
        verification: {},
        organization: org
      }

      metadata = { user_id: actor.id }
      RequestStore.stubs(:read).with(:request_metadata).returns metadata

      Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries({
        recorder:         "Analytics::OrgMailerConfigEditedMetricsRecorder",
        event:            "org_email_sender_edited",
        actor:            Analytics::MetricsRecorder.user_attributes(actor),
        org:              Analytics::MetricsRecorder.org_attributes(org),
        mailer_config:    Analytics::MetricsRecorder.mailer_config_attributes(mailer_config).merge(
          "is_sender_verified" => "1"
        ),
        timestamp:        stub_time.to_i,
        additional_data:  metadata,
        changed_column:   'verification',
        old_val:          { },
        new_val:          { "domain" => true }
      }))
      mailer_config.update verification: { domain: true }
    end
  end
end

require 'test_helper'

class GroupMetricsAggregationTest < ActiveSupport::TestCase

  setup do
    @klass = GroupMetricsAggregation
  end

  test 'validations' do
    record = @klass.new
    required_attrs = %i{
      team_id            organization_id
      smartbites_created smartbites_consumed
      time_spent_minutes total_users
      active_users       new_users
      engagement_index   num_comments
      num_likes          start_time
      end_time
    }
    expected = required_attrs.each_with_object({}) do |attr, memo|
      memo[attr] = ["can't be blank"]
    end
    actual = record.tap(&:valid?).errors.messages
    assert_equal expected, actual
  end

  # NOTE: this is intentional behavior.
  # We bulk-create these records using data we get from influx.
  # Therefore the referenced ids may no longer exist in SQL
  test 'does not check validity of foreign keys' do
    record = @klass.new(
      team_id:             0,
      organization_id:     0,
      smartbites_created:  0,
      smartbites_consumed: 0,
      time_spent_minutes:  0.0,
      total_users:         0,
      active_users:        0,
      new_users:           0,
      engagement_index:    0,
      num_comments:        0,
      num_likes:           0,
      start_time:          0,
      end_time:            0,
    )
    assert record.valid?
    record.save
    assert record.persisted?
  end

end

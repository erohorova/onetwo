require 'test_helper'

class YoutubeDataExtractorTest < ActiveSupport::TestCase
  test "should extract youtube data" do
    url = "https://www.youtube.com/watch?v=abcdefgh"
    tags = ['tag1', 'multi word tag', 'low scoring tag3']
    stub_request(:get, "#{YoutubeDataExtractor::GOOGLE_API}/youtube/v3/videos?fields=items(id,snippet/tags)&id=abcdefgh&key=#{Settings.google.api_key}&part=snippet").
      to_return(:status => 200, :body => {
       "items": [
        {
         "id": "abcdefgh",
         "snippet": {
          "tags": [
            'tag1',
            'multi word tag',
            'low scoring tag3'
          ]
         }
        }
       ]
      }.to_json, :headers => {})

    yte = YoutubeDataExtractor.new(url: url)
    yte.extract!
    assert_equal tags, yte.tags
  end

  test "should return empty array for non youtube url" do
    url = "https://www.youtuebe.com/watch?v=abcdefgh"

    yte = YoutubeDataExtractor.new(url: url)
    yte.extract!
    assert_empty yte.tags
  end
end
require 'test_helper'

class TransactionAuditTest < ActiveSupport::TestCase

  should have_db_column(:transaction_id).of_type(:integer)
  should have_db_column(:order_data).of_type(:text)
  should have_db_column(:orderable_prices_data).of_type(:text)
  should have_db_column(:orderable_data).of_type(:text)
  should have_db_column(:user_data).of_type(:text)
  should have_db_column(:transaction_data).of_type(:text)

  should belong_to(:user_transaction).class_name('Transaction')

  should validate_presence_of(:transaction_id)
end

require 'test_helper'

class EventTest < ActiveSupport::TestCase
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    Event.any_instance.stubs :schedule_reminder_email

    @org = create(:organization)
    @group, @user = create_gu(user_opts: {}, group_opts: {organization_id: @org.id, client_resource_id: 1}, role: 'member')
  end

  should validate_presence_of :name
  should validate_presence_of :event_start
  should validate_presence_of :event_end
  should validate_presence_of :state
  should validate_presence_of :privacy

  should belong_to :group

  should validate_inclusion_of(:state).in_array(['open', 'cancelled'])
  should validate_inclusion_of(:privacy).in_array(['public', 'private'])

  test "validate webex detail for webex conference" do
    e = build(:event, user: @user, group: @group, conference_type: "webex")
    assert_equal e.valid?, false
    assert_equal e.errors.full_messages, ["Conference email can't be blank", "Conference email is not a valid email address", "Conference email can't be blank", "Webex can't be blank", "Webex password can't be blank"]

    e1 = build(:event, user: @user, group: @group, conference_type: "hangout")
    assert_equal e1.valid?, true
  end

  test "should show past and upcoming events" do
    create(:event, user: @user, group: @group, event_start: Time.now - 2.day, event_end: Time.now - 1.day)
    create(:event, user: @user, group: @group, event_start: Time.now - 2.day, event_end: Time.now + 1.day)

    create(:event, user: @user, group: @group, event_start: Time.now + 1.day)
    create(:event, user: @user, group: @group, event_start: Time.now + 2.day)

    assert_equal 3, Event.upcoming.count
    assert_equal 1, Event.past.count
  end

  test 'user should belong to group' do
    create_default_org
    u = create(:user)
    g = create(:group)
    e = build(:event, user: u, group: g)
    assert_not e.save

    g.add_member(u)
    e = build(:event, user: u, group: g)
    assert e.save
  end

  test "should return count of users going" do
    event = create(:event, user: @user, group: @group, event_start: Time.now + 1.day)
    create(:events_user, :rsvp_yes, event: event, user: create(:user))
    create(:events_user, :rsvp_no, event: event, user: create(:user))

    # 2, including default yes for creator
    assert event.rsvp_yes_count, 2
  end

  test "should return user images for event" do
    event = create(:event, user: @user, group: @group, event_start: Time.now + 1.day)
    create(:events_user, :rsvp_yes, event: event, user: create(:user))
    create(:events_user, :rsvp_yes, event: event, user: create(:user))
    create(:events_user, :rsvp_yes, event: event, user: create(:user))
    create(:events_user, :rsvp_yes, event: event, user: create(:user))
    create(:events_user, :rsvp_yes, event: event, user: create(:user))
    create(:events_user, :rsvp_yes, event: event, user: create(:user))
    create(:events_user, :rsvp_yes, event: event, user: create(:user))
    create(:events_user, :rsvp_yes, event: event, user: create(:user, picture_url: 'http://notincluded.com'))

    tu = event.rsvp_top_users
    assert_equal tu.count, 7
    assert_equal tu.first, @user.photo
    assert_not tu.include?('http://notincluded.com')
  end

  test "should create icalendar attachment" do
    event = create(:event, user: @user, group: @group)
    assert event.ical
    assert_equal event.ical_content_type, "text/calendar"
  end

  test 'going method should work' do
    e = create(:event, user: @user, group: @group)
    u1 = create(:user)
    u2 = create(:user)
    u3 = create(:user)
    u4 = create(:user)
    create(:events_user, :rsvp_yes, user: u1, event: e)
    create(:events_user, :rsvp_no, user: u2, event: e)
    create(:events_user, :rsvp_maybe, user: u3, event: e)
    assert e.going? u1
    assert_not e.going? u2
    assert_not e.going? u3
    assert_not e.going? u4
    assert_not e.going?(nil)
  end

  test 'create a event with webex meeting' do
    WebexApi::Client.any_instance.stubs(:create_meeting).returns("123")
    WebexApi::Client.any_instance.stubs(:add_attendee_to_meeting)
    WebexApi::Client.any_instance.stubs(:get_meeting_join_url).returns("www.example.com")
    WebexApi::Client.any_instance.stubs(:get_meeting_host_url).returns("www.example1.com")
    webex_config = create(:webex_config, organization: @group.organization)
    e = build(:event, conference_type: 'webex', webex_id: "123", webex_password: "password",
       conference_email: "test@ex.com", user: @user, group: @group)
    e.save
    assert_equal e.conference_url, "www.example.com"
    assert_equal e.host_url, "www.example1.com"
    assert_equal e.webex_conference_key, "123"
    WebexApi::Client.any_instance.unstub(:create_meeting)
    WebexApi::Client.any_instance.unstub(:add_attendee_to_meeting)
    WebexApi::Client.any_instance.unstub(:get_meeting_join_url)
    WebexApi::Client.any_instance.unstub(:get_meeting_host_url)
  end

  test 'should retun error if credentials are empty' do
    e = build(:event, conference_type: 'webex', webex_id: "123", webex_password: "password",
       conference_email: "test@ex.com", user: @user, group: @group)
    e.save
    assert_equal e.full_errors, "Conference type Webex Credential can not be blank"
  end
end

require 'test_helper'

class StructuredItemTest < ActiveSupport::TestCase
  should belong_to(:structure)
  should belong_to(:entity)

  should validate_presence_of(:structure)
  should validate_presence_of(:entity)


  setup do
    @organization = create :organization
    @user         = create :user, organization: @organization
    @card         = create :card, author: @user, organization: @organization
    @structure    = create :structure, organization: @organization, creator: @user,parent: @organization
  end

  test 'maintains unique structure - entity' do
    structure_item1 = create :structured_item, structure: @structure, entity: @card
    structure_item2 = build :structured_item, structure: @structure, entity: @card
    refute structure_item2.valid?
  end

  test 'pushes metric upon creation' do
    channel        = create :channel, organization: @organization
    structure_item = build :structured_item, structure: @structure, entity: channel
    metadata = { user_id: @user.id }
    RequestStore.expects(:read).with(:request_metadata).returns metadata
    stub_time
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::ChannelCarouselMetricsRecorder",
      timestamp: Time.now.to_i,
      additional_data: metadata,
      actor: Analytics::MetricsRecorder.user_attributes(@user),
      event: "channel_added_to_channel_carousel",
      channel: Analytics::MetricsRecorder.channel_attributes(channel),
      structure: Analytics::MetricsRecorder.structure_attributes(@structure)
    )
    structure_item.save
    structure_item.run_callbacks :commit

    assert structure_item.persisted?
  end

  test 'pushes metric upon deletion' do
    channel        = create :channel, organization: @organization
    structure_item = create :structured_item, structure: @structure, entity: channel
    metadata = { user_id: @user.id }
    RequestStore.expects(:read).with(:request_metadata).returns metadata
    stub_time
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::ChannelCarouselMetricsRecorder",
      timestamp: Time.now.to_i,
      additional_data: metadata,
      actor: Analytics::MetricsRecorder.user_attributes(@user),
      channel: Analytics::MetricsRecorder.channel_attributes(channel),
      event: "channel_removed_from_channel_carousel",
      structure: Analytics::MetricsRecorder.structure_attributes(@structure)
    )

    structure_item.destroy
    refute structure_item.persisted?
  end

  test 'doesnt push metric if it doesnt have the required traits' do
    # Structure Item must have a Channel entity for metrics to be recorded
    structure_item = build :structured_item, structure: @structure, entity: @card
    Analytics::MetricsRecorderJob.expects(:perform_later).never
    structure_item.save
  end

  test '#fetch_by_entity to return the desired structured_item when entity_type is User' do
    user_1 = create :user, organization: @organization
    structure = create :structure, organization: @organization, creator: @user,parent: @organization,context: 1, slug: 'discover-users'
    structure_item_1 = create :structured_item, structure: structure, entity: @user
    structure_item_2 = create :structured_item, structure: structure, entity: user_1
    struct_item_for_entity_1 = StructuredItem.fetch_by_entity(@user.id, 'User', structure)
    struct_item_for_entity_2 = StructuredItem.fetch_by_entity(user_1.id, 'User', structure)

    assert_equal structure_item_1.id, struct_item_for_entity_1.id
    assert_equal structure_item_2.id, struct_item_for_entity_2.id
  end

  test '#fetch_by_entity to return & not return the desired structured_item when entity_type is Card' do
    card_1 = create :card, author: @user, organization: @organization
    structure  = create :structure, organization: @organization, creator: @user,parent: @organization,context: 1, slug: 'discover-cards'
    structure_item = create :structured_item, structure: structure, entity: @card
    result = StructuredItem.fetch_by_entity(@card.id, 'Card', structure)
    assert_equal structure_item.id, result.id

    #negative test case
    result = StructuredItem.fetch_by_entity(card_1.id, 'Card', structure)
    assert_nil result
  end

  test '#fetch_by_entity to return the desired structured_item when entity_type is Channel' do
    channel  = create :channel, organization: @organization
    structure = create :structure, organization: @organization, creator: @user,parent: @organization,context: 1, slug: 'discover-channels'
    structure_item = create :structured_item, structure: structure, entity: channel
    result = StructuredItem.fetch_by_entity(channel.id, 'Channel', structure)
    assert_equal structure_item.id, result.id
  end

  test '#reorder_and_destroy to return the reordered list if some item is deleted' do
    channel, channel_1, channel_2, channel_3  = create_list :channel, 4, organization: @organization
    structure = create :structure, organization: @organization, creator: @user,parent: @organization,context: 1, slug: 'discover-channels'
    structure_item_1 = create :structured_item, structure: structure, entity: channel
    structure_item_2 = create :structured_item, structure: structure, entity: channel_2
    structure_item_3 = create :structured_item, structure: structure, entity: channel_1
    structure_item_4 = create :structured_item, structure: structure, entity: channel_3

    item_to_be_deleted = StructuredItem.where(id: structure_item_2.id)
    assert_equal 3, structure.structured_items.where(id:structure_item_3).first.position

    result = StructuredItem.reorder_and_destroy(item_to_be_deleted)
    structure.reload
    assert_equal 2, structure.structured_items.where(id:structure_item_3).first.position
  end
end

require 'test_helper'

class DeveloperAdminInvitationTest < ActiveSupport::TestCase

  should validate_presence_of(:recipient_email)

  test 'should set recipient ref and token' do
    user = create(:user)
    invite = create(:developer_admin_invitation, recipient_email: user.email)

    assert_not_nil invite.token
    assert_equal invite.recipient, user
    assert_not_nil invite.first_name
    assert_not_nil invite.last_name
    assert_not_nil invite.sender
    assert_not_nil invite.client
  end

end
require 'test_helper'

class CardUserShareTest < ActiveSupport::TestCase
  should have_db_column(:card_id).of_type(:integer)
  should have_db_column(:user_id).of_type(:integer)

  should belong_to(:card)
  should belong_to(:user)

   setup do
    @organization = create(:organization)
    @author = create :user, organization: @organization
    @user = create(:user, organization: @organization)
    @card = create :card , author_id: @author.id
   end

  test 'records card_shared event on create' do
    stub_time
    org = create :organization
    actor = create :user, organization: org
    user = create :user, organization: org
    card = create :card, organization: org, author_id: nil
    metadata = { foo: "bar", user_id: actor.id }
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    # This recorder uses the SharedCard#user as the actor,
    # instead of looking up from the RequestStore metadata
    Analytics::MetricsRecorderJob.unstub(:perform_later)
    EdcastActiveJob.unstub(:perform_later)
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        recorder: "Analytics::CardSharedMetricsRecorder",
        card: Analytics::MetricsRecorder.card_attributes(card),
        timestamp: Time.now.to_i,
        event: "card_shared",
        org: Analytics::MetricsRecorder.org_attributes(org),
        shared_to_user: Analytics::MetricsRecorder.user_attributes(user),
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        additional_data: metadata
      )
    )
    shared_card = CardUserShare.create(user: user, card: card)
    shared_card.run_callbacks :commit
  end

  test 'should trigger NotificationGeneratorJob after creating card_user_share' do
    TestAfterCommit.with_commits(true) do
      NotificationGeneratorJob.expects(:perform_later).once
	    create :card_user_share, user: @user, card: @card 
	  end  
  end

  test 'should no trigger NotificationGeneratorJob for a card that is not accessible to the user' do
    card = create(:card, is_public: false, author: @author)
    create(:card_user_permission, card_id: card.id, user_id: @author.id)
    TestAfterCommit.with_commits(true) do
      NotificationGeneratorJob.expects(:perform_later).never
      create :card_user_share, user: @user, card: card
    end
  end

  test "should delete notification if card_user_share get destroyed" do
  	TestAfterCommit.with_commits(true) do
	    card_user_share = create :card_user_share, user: @user, card: @card
	    card_user_share.run_callbacks(:commit)
	    notification = create(:notification, notification_type: 'user_card_share', notifiable: @card, custom_message: "Smartcard is share with you.", user_id: @user.id )
	    card_user_share.destroy
	    notification_after_destroy = Notification.count
	    assert_equal  0 ,notification_after_destroy
  	end
  end

end

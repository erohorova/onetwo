require 'test_helper'

class WalletPaymentGatewayTest < ActiveSupport::TestCase
  setup do
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @card = create :card, organization: @organization, author: @user
    @price = create(:price, card: @card)
    @order = create :order, orderable: @card, user_id: @user.id,
                   organization_id: @organization.id
    @transaction = create :transaction, order: @order, user: @user
  end

  test '#validate_payment must return true if balance is available and amount is correct' do
    create(:wallet_balance, user: @user, amount: BigDecimal.new("200"))
    assert_equal true, WalletPaymentGateway.validate_amount(amount: @transaction.amount, user_id:@user.id)
  end

  test '#validate_payment must return false if wallet balance is unavailable' do
    create(:wallet_balance, user: @user, amount: BigDecimal.new("50"))
    assert_equal false, WalletPaymentGateway.validate_amount(amount: @transaction.amount, user_id:@user.id)
  end

  test '#validate_payment must return false if amount is negative' do
    create(:wallet_balance, user: @user, amount: BigDecimal.new("200"))
    assert_equal false, WalletPaymentGateway.validate_amount(amount: -2.0, user_id:@user.id)
  end

  test '#initiate_wallet_transaction creates wallet_transaction with state initiated' do
    WalletPaymentGateway.new.initiate_wallet_transaction(order: @order, amount: @price.amount)
    assert WalletTransaction.last.initiated?
  end

  test '#authorize_payment success' do
    create(:wallet_transaction, payment_state: 0, order: @order, user: @user, payment_type: 1, amount: @transaction.amount)
    WalletPaymentGateway.stubs(:validate_amount).returns(true)
    gateway = WalletPaymentGateway.new
    result = gateway.authorize_payment(@transaction, {})

    assert result.success?
  end

  test '#authorize_payment failure' do
    create(:wallet_transaction, payment_state: 0, order: @order, user: @user, payment_type: 1, amount: @transaction.amount)
    WalletPaymentGateway.stubs(:validate_amount).returns(false)
    gateway = WalletPaymentGateway.new
    result = gateway.authorize_payment(@transaction, {})

    refute result.success?
    assert_equal 'Insufficient balance in wallet', result.errors.full_messages.to_sentence
  end
  
  test '#authorize_payment must raise exception if wallet transaction record is not present' do
    gateway = WalletPaymentGateway.new
    exception = assert_raises(Exception) {
      gateway.authorize_payment(@transaction, {})
    }
    assert_equal exception.class, WalletTransactionNotFoundError
  end

  test '#void_payment should update payment state of wallet transaction to voided' do
    wallet_transaction = create(:wallet_transaction, payment_state: 5)
    result = WalletPaymentGateway.new.void_payment(gateway: wallet_transaction)
    assert_equal true, result
    assert wallet_transaction.voided?
  end

  test '#capture_payment should update payment state of wallet transaction to successful' do
    wallet_transaction = create(:wallet_transaction, payment_state: 5)
    result = WalletPaymentGateway.new.capture_payment(transaction: @transaction, gateway: wallet_transaction)
    assert_equal true, result
    assert wallet_transaction.successful?
  end

end
require 'test_helper'

class JourneyPackRelationTest < ActiveSupport::TestCase
  should belong_to :cover

  setup do
    stub_request(:post, 'http://localhost:9200/cards_test/_refresh')
    org = create(:organization, host_name: 'exz')
    @user = create(:user, organization: org)
    setup_journey_card(@user)

    Card.any_instance.stubs(:remove_card_from_queue)
  end

  test 'should properly retrieve journey sections' do
    assert_same_ids @journey_pack.collect(&:id), JourneyPackRelation.journey_relations(cover_id: @journey.id).collect(&:from_id)
  end

  test 'should remove the section from the journey' do
    stub_request(:post, "http://localhost:9200/cards_test/_refresh").
      with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Content-Length'=>'0', 'User-Agent'=>'Faraday v0.9.2'}).
      to_return(:status => 200, :body => "", :headers => {})

    assert_difference -> { JourneyPackRelation.count }, -1 do
      JourneyPackRelation.remove_from_journey(cover_id: @journey.id, remove_id: @section1.id, destroy_hidden: true)
    end
    ids = @journey.id, @section2.id, @section3.id
    assert_same_ids ids, JourneyPackRelation.journey_relations(cover_id: @journey.id).collect(&:from_id)
  end

  test 'should add the new section to journey' do
    new_section = create(:card, card_type: 'pack', card_subtype: 'simple')
    assert_difference -> { JourneyPackRelation.count }, 1 do
      JourneyPackRelation.add(cover_id: @journey.id, add_id: new_section.id)
    end
  end

  test 'should call PackJourneyCardReprocessJob after adding the new section to journey' do
    new_section = create(:card, card_type: 'pack', card_subtype: 'simple')
    TestAfterCommit.with_commits(true) do
      PackJourneyCardReprocessJob.expects(:perform_later).once
      JourneyPackRelation.add(cover_id: @journey.id, add_id: new_section.id)
    end
  end

  test 'should call PackJourneyCardReprocessJob after remove the section from the journey' do
    TestAfterCommit.with_commits(true) do
      PackJourneyCardReprocessJob.expects(:perform_later).once
      JourneyPackRelation.remove_from_journey(cover_id: @journey.id, remove_id: @section1.id, destroy_hidden: true)
    end
  end

  test 'should content proper date for the weekly journey' do
    setup_weekly_journey_card(@user)
    pack = JourneyPackRelation.journey_relations(cover_id: @journey_weekly.id)
    initial_start_date = pack[0].start_date
    assert_same_ids @journey_pack_weekly.collect(&:id), JourneyPackRelation.journey_relations(cover_id: @journey_weekly.id).collect(&:from_id)
    assert_equal initial_start_date, pack[1].start_date
    assert_equal initial_start_date + 7.days, pack[2].start_date
    assert_equal initial_start_date + 14.days, pack[3].start_date
  end

  test 'should content proper date for the weekly journey after removing a section' do
    setup_weekly_journey_card(@user)

    assert_difference -> { JourneyPackRelation.count }, -1 do
      JourneyPackRelation.remove_from_journey(cover_id: @journey_weekly.id, remove_id: @section2w.id, destroy_hidden: true)
    end
    pack = JourneyPackRelation.journey_relations(cover_id: @journey_weekly.id)
    initial_start_date = pack[0].start_date
    ids = @journey_weekly.id, @section1w.id, @section3w.id
    assert_same_ids ids, JourneyPackRelation.journey_relations(cover_id: @journey_weekly.id).collect(&:from_id)
    assert_equal initial_start_date, pack[1].start_date
    assert_equal initial_start_date + 7.days, pack[2].start_date
  end

  test 'should properly reorder journey sections' do
    JourneyPackRelation.reorder_journey_relations(cover_id: @journey.id, from_pos: 3, to_pos: 1)
    reordered_ids = @journey.id, @section3.id, @section1.id, @section2.id
    assert_same_ids reordered_ids, JourneyPackRelation.journey_relations(cover_id: @journey.id).collect(&:from_id)
  end

  test 'should content proper date for the weekly journey after reordering' do
    setup_weekly_journey_card(@user)

    JourneyPackRelation.reorder_journey_relations(cover_id: @journey_weekly.id, from_pos: 3, to_pos: 1)
    pack = JourneyPackRelation.journey_relations(cover_id: @journey_weekly.id)
    initial_start_date = pack[0].start_date
    reordered_ids = @journey_weekly.id, @section3w.id, @section1w.id, @section2w.id
    assert_same_ids reordered_ids, JourneyPackRelation.journey_relations(cover_id: @journey_weekly.id).collect(&:from_id)
    assert_equal initial_start_date, pack[1].start_date
    assert_equal initial_start_date + 7.days, pack[2].start_date
    assert_equal initial_start_date + 14.days, pack[3].start_date
  end

  test 'should create root section for the journey card if it not exists' do
    org = create(:organization)
    user = create(:user, organization: org)
    card = create(:card, author: user, organization: org, card_type: 'journey', card_subtype: 'self_paced')
    # remove relation
    JourneyPackRelation.find_by(cover_id: card.id, from_id: card.id).delete
    # should be created root section in process of checking journey_relations
    assert_difference -> { JourneyPackRelation.count }, 1 do
      JourneyPackRelation.journey_relations(cover_id: card.id)
    end
    assert JourneyPackRelation.exists?(cover_id: card.id, from_id: card.id)
  end
end

class RecordJourneyEventsTest < ActiveSupport::TestCase
  setup do
    TestAfterCommit.enabled = true
  end

  teardown do
    TestAfterCommit.enabled = false
  end

  test 'should invoke card metrics recorder when journey section is added' do
    stub_time
    author = create(:user)
    org = author.organization
    pathway_card = create(:card, card_type: 'pack', card_subtype: 'simple', author: author)

    # No event will be recorded at this point
    journey_card = create(:card,
      card_type: 'journey',
      card_subtype: 'self_paced',
      author: author,
      title: "journey title"
    )
    Analytics::MetricsRecorderJob.unstub :perform_later

    Analytics::MetricsRecorderJob
      .expects(:perform_later)
      .with(
        has_entries(
          recorder: 'Analytics::CardMetricsRecorder',
          org: Analytics::MetricsRecorder.org_attributes(org),
          card: Analytics::MetricsRecorder.card_attributes(pathway_card),
          event: 'card_added_to_journey',
          actor: Analytics::MetricsRecorder.user_attributes(author),
          timestamp: Time.now.to_i,
          additional_data: {
            journey_id: journey_card.id,
            journey_name: journey_card.title
          }
        )
      ).once

    RequestStore.stubs(:read).returns {}
    JourneyPackRelation.add cover_id: journey_card.id, add_id: pathway_card.id
  end

  test 'should invoke card metrics recorder when journey section is removed' do
    stub_time
    author = create(:user)
    org = author.organization
    pathway_card = create(:card, card_type: 'pack', card_subtype: 'simple', author: author)

    # No event will be recorded at this point
    JourneyPackRelation.any_instance.stubs(:record_add_to_journey_event).returns true

    journey_card = create(:card,
      card_type: 'journey',
      card_subtype: 'self_paced',
      author: author,
      title: "journey title"
    )
    JourneyPackRelation.add cover_id: journey_card.id, add_id: pathway_card.id

    Analytics::MetricsRecorderJob.unstub :perform_later
    metadata =  {
      journey_id: journey_card.id.to_s,
      journey_name: journey_card.title
    }
    Analytics::MetricsRecorderJob
      .expects(:perform_later)
      .with(
        has_entries(
          recorder: 'Analytics::CardMetricsRecorder',
          org: Analytics::MetricsRecorder.org_attributes(org),
          card: Analytics::MetricsRecorder.card_attributes(pathway_card),
          event: 'card_removed_from_journey',
          actor: Analytics::MetricsRecorder.user_attributes(author),
          timestamp:  Time.now.to_i,
          additional_data: metadata
        )
      ).once

    RequestStore.stubs(:read).returns metadata
    # Expect remove event is called
    JourneyPackRelation.where(cover_id: journey_card.id).last.destroy
  end

end

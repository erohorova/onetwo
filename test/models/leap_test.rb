require 'test_helper'

class LeapTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  setup do
    stub_request(:post, "http://localhost:9200/cards_test/_refresh")
    stub_request(:get, "https://maps.googleapis.com/maps/api/geocode/json")
    stub_request(:get, "http://localhost:9200/_aliases")

    @cover = create(:card, card_type: 'pack', card_subtype: 'simple')
    @card1, @card2, @card3 = create_list(:card, 3)
    @cards = @cover, @card1, @card2, @card3

    [@card1, @card2, @card3].each do |card|
      CardPackRelation.add(cover_id: @cover.id, add_id: card.id, add_type: card.class.name)
    end
    @poll = create(:card, card_type: 'poll', card_subtype: 'text')
    CardPackRelation.add(cover_id: @cover.id, add_id: @poll.id, add_type: 'Card')
  end

  test 'should be created for poll card' do
    poll = create(:card, card_type: 'poll', card_subtype: 'text')
    card, card2 = create_list(:card, 2, card_type: 'media', card_subtype: 'text')
    assert_difference -> { Leap.count }, 1 do
      create(:leap, card: poll, correct_id: card.id, wrong_id: card2.id)
    end
  end

  test 'should be valid only for poll cards' do
    card, card2, card3 = create_list(:card, 3, card_type: 'media', card_subtype: 'text')
    leap = build(:leap, card: card, correct_id: card2.id, wrong_id: card3.id)
    assert_not leap.valid?
    assert leap.errors.full_messages.include?("Card should be poll type")
  end

  test 'should be unique leap for the poll card in the same pathway' do
    poll = create(:card, card_type: 'poll', card_subtype: 'text')
    pack = create(:card, card_type: 'pack', card_subtype: 'simple')
    card, card2, card3, card4 = create_list(:card, 4, card_type: 'media', card_subtype: 'text')
    assert_difference -> { Leap.count }, 1 do
      create(:leap, card: poll, correct_id: card.id, wrong_id: card2.id)
    end
    leap = build(:leap, card: poll, correct_id: card3.id, wrong_id: card4.id)
    assert_not leap.valid?
    assert leap.errors.full_messages.include?("Card already has a leap")
  end

  test 'should content only pack cards in pathway_id field' do
    poll = create(:card, card_type: 'poll', card_subtype: 'text')
    leap = build(:leap, card: @poll, pathway: poll, correct_id: @card1.id, wrong_id: @card2.id)
    assert_not leap.valid?
    assert leap.errors.full_messages.include?("Pathway should be pack type")
  end

  test 'should be destroyed after deleting a poll card' do
    poll = create(:card, card_type: 'poll', card_subtype: 'text')
    card, card2 = create_list(:card, 2, card_type: 'media', card_subtype: 'text')
    assert_difference -> { Leap.count }, 1 do
      create(:leap, card: poll, correct_id: card.id, wrong_id: card2.id)
    end
    assert_difference -> { Leap.count }, -1 do
      poll.destroy
    end
  end

  test 'should not be valid if correct and wrong fields content the same card' do
    poll = create(:card, card_type: 'poll', card_subtype: 'text')
    card = create(:card, card_type: 'media', card_subtype: 'text')
    leap = build(:leap, card: poll, correct: card, wrong: card)
    assert_not leap.valid?
    assert leap.errors.full_messages.include?("Leap cards should be different for correct and wrong answers")
  end

  test 'should not be valid if correct field blank' do
    poll = create(:card, card_type: 'poll', card_subtype: 'text')
    card = create(:card, card_type: 'media', card_subtype: 'text')
    leap = build(:leap, card: poll, wrong: card)
    assert_not leap.valid?
    assert leap.errors.full_messages.include?("Correct can't be blank")
  end

  test 'should not be valid if wrong field blank' do
    poll = create(:card, card_type: 'poll', card_subtype: 'text')
    card = create(:card, card_type: 'media', card_subtype: 'text')
    leap = build(:leap, card: poll, correct: card)
    assert_not leap.valid?
    assert leap.errors.full_messages.include?("Wrong can't be blank")
  end

  test 'should be destroyed after removing a card from pathway' do
    poll = create(:card, card_type: 'poll', card_subtype: 'text')
    card = create(:card, card_type: 'media', card_subtype: 'text')
    leap = build(:leap, card: poll, correct: card)
    assert_not leap.valid?
    assert leap.errors.full_messages.include?("Wrong can't be blank")
  end

  test 'should be valid for cards in pathway for non standalone card' do
    card_not_in_pathway = create(:card, card_type: 'media', card_subtype: 'text')
    leap = build(:leap, card: @poll, pathway: @cover, wrong: @card1, correct: card_not_in_pathway)
    leap2 =  build(:leap, card: @poll, pathway: @cover, wrong: card_not_in_pathway, correct: @card1)
    leap3 =  build(:leap, card: @poll, pathway: @cover, wrong: @card2, correct: @card1)
    assert_not leap.valid?
    assert_not leap2.valid?
    assert leap3.valid?
    assert leap.errors.full_messages.include?("Leap card not in this pathway #{card_not_in_pathway.id} for correct answer")
    assert leap2.errors.full_messages.include?("Leap card not in this pathway #{card_not_in_pathway.id} for wrong answer")
  end

  test 'should be valid for quiz in pathway for non standalone card' do
    poll = create(:card, card_type: 'poll', card_subtype: 'text')
    leap = build(:leap, card: poll, pathway: @cover, wrong: @card1, correct: @card2)
    assert_not leap.valid?
    assert leap.errors.full_messages.include?('Quiz card should be in this pathway')
  end

  test 'should be destroyed after removing a card from pathway for which leaps implemented' do
    @poll2 = create(:card, card_type: 'poll', card_subtype: 'text')
    CardPackRelation.add(cover_id: @cover.id, add_id: @poll2.id, add_type: 'Card')
    assert_difference -> { Leap.count }, 2 do
      create(:leap, card: @poll, pathway: @cover, wrong: @card1, correct: @card2)
      create(:leap, card: @poll2, pathway: @cover, wrong: @card1, correct: @card2)
    end
    assert_difference -> { Leap.count }, -2 do
      assert CardPackRelation.remove_from_pack(
          cover_id: @cover.id,
          remove_id: @card1.id,
          remove_type: 'Card',
      )
    end
  end

  test 'should be destroyed after removing a pathway' do
    assert_difference -> { Leap.count }, 1 do
      create(:leap, card: @poll, pathway: @cover, wrong: @card1, correct: @card2)
    end
    assert_difference -> { Leap.count }, -1 do
      assert CardPackRelation.remove(
          cover_id: @cover.id,
          destroy_hidden: true,
          inline_indexing: true
      )
    end
  end

end
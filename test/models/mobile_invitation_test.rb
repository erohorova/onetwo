require 'test_helper'

class MobileInvitationTest < ActiveSupport::TestCase

  should validate_presence_of(:recipient_email)

  test 'should set recipient ref and token' do
    user = create(:user)
    mb_invite = create(:mobile_invitation, recipient_email: user.email)
    assert_not_nil mb_invite.token
    assert_equal mb_invite.recipient, user
  end

end


require 'test_helper'

class StructureTest < ActiveSupport::TestCase
  should belong_to(:creator).class_name('User')
  should belong_to(:organization)
  should belong_to(:parent)

  should validate_presence_of(:context)
  should validate_presence_of(:creator)
  should validate_presence_of(:slug)

  test '#current_context resolves context' do
    user = create :user
    structure = create :structure, current_context: 'discover', current_slug: 'cards', creator: user

    assert_equal structure.reload.context, 1
    assert_equal structure.reload.slug, 'discover-cards'
  end

  test '#current_slug resolves slug' do
    user = create :user
    structure = create :structure, current_context: 'discover', current_slug: 'cards', creator: user

    assert_equal structure.reload.slug, 'discover-cards'
  end

  test "update org config on enable/disable structure" do
    org = create :organization
    user = create :user, organization: org
    structure = create :structure, parent: org, current_context: 'discover', current_slug: 'cards', creator: user, organization: org, enabled: true

    key = "discover/carousel/customCarousel/card/#{structure.id}"
    config_discover = {
        "version":"4.7",
        "discover":{
          "#{key}":{"id":structure.id,"defaultLabel":"#{structure.display_name}","index":0,"visible":true},
        }}.to_json


    config = create(:config, name: "OrgCustomizationConfig", value: config_discover, data_type: 'json', configable_id: org.id, configable_type: 'Organization')
    assert config.reload.parsed_value["discover"][key]["visible"]

    ###### Disable carousel
    structure.update_attributes(enabled: "false")
    structure.update_org_config
    refute config.reload.parsed_value["discover"][key]["visible"]

    ###### Enable Carousel
    structure.update_attributes(enabled: "true")
    structure.update_org_config
    assert config.reload.parsed_value["discover"][key]["visible"]

    ###### Delete carousel callback
    structure.remove_from_org_config
    assert_nil config.reload.parsed_value["discover"][key]
  end

  test 'should call update_entity_and_config when last item of structure has got deleted' do
    org = create :organization
    user = create :user, organization: org
    card = create :card, organization: org, author: user
    structure = create :structure, parent: org, current_context: 'discover', current_slug: 'cards', creator: user, organization: org, enabled: true
    key = "discover/carousel/customCarousel/card/#{structure.id}"
    config_discover = {
        "version":"4.7",
        "discover":{
          "#{key}":{"id":structure.id,"defaultLabel":"#{structure.display_name}","index":0,"visible":true},
        }}.to_json

    structured_item = create(:structured_item, entity: card, structure: structure)
    config = create(:config, name: "OrgCustomizationConfig", value: ActiveSupport::JSON.encode({"discover": {}}), data_type: 'json', configable_id: org.id, configable_type: 'Organization')
    structure.structured_items.destroy_all
    structure.update_entity_and_config(org: org)

    refute structure.enabled
  end

  test 'should have context if not present' do
    org = create :organization
    user = create :user, organization: org
    structure = build :structure, parent: org, current_context: '', creator: user, organization: org, enabled: true
    assert "Context can't be blank", structure.errors.full_messages[0]
  end

  test 'doesnt call recorders for structures without organization/channel parents' do
    org = create :organization
    actor = create :user, organization: org
    creator = create :user, organization: org
    card = create :card, author: actor, message: "foo"
    structure = build :structure, {
      current_context: 'discover',
      current_slug: 'discover-users',
      parent: card,
      creator: creator
    }
    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(:perform_later).never
    structure.save
    assert structure.persisted?
    structure.update slug: "discover-channels"
    assert_equal structure.reload.slug, "discover-channels"
    structure.destroy
    refute structure.persisted?
  end

  test 'calls recorder job upon creation with org parent' do
    org = create :organization
    actor = create :user, organization: org
    creator = create :user, organization: org
    channel = create :channel, organization: org
    structure = create :structure, {
      current_context: 'discover',
      current_slug: 'channel-cards',
      parent: org,
      creator: creator
    }
    stub_time
    metadata = {
      is_admin_request: 0,
      platform: "platform",
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      user_id: actor.id
    }
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::ChannelCarouselMetricsRecorder",
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      structure: Analytics::MetricsRecorder.structure_attributes(structure),
      event: "channel_carousel_created",
      timestamp: Time.now.to_i,
      additional_data: metadata
    )
    structure.run_callbacks :commit
  end

  test 'calls recorder job upon creation with channel parent' do
    org = create :organization
    actor = create :user, organization: org
    creator = create :user, organization: org
    channel = create :channel, organization: org
    structure = create :structure, {
      current_context: 'discover',
      current_slug: 'channel-cards',
      parent: channel,
      creator: creator
    }
    stub_time
    metadata = {
      is_admin_request: 0,
      platform: "platform",
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      user_id: actor.id
    }
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::ChannelCarouselMetricsRecorder",
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      structure: Analytics::MetricsRecorder.structure_attributes(structure),
      channel: Analytics::MetricsRecorder.channel_attributes(channel),
      event: "channel_carousel_created",
      timestamp: Time.now.to_i,
      additional_data: metadata
    )
    structure.run_callbacks :commit
  end

  test 'pushes metric when deleted with org parent' do
    org = create :organization
    actor = create :user, organization: org
    creator = create :user, organization: org
    channel = create :channel, organization: org
    structure = create :structure, {
      current_context: 'channel',
      current_slug: 'channel-cards',
      parent: org,
      creator: creator
    }
    metadata = {
      is_admin_request: 0,
      platform: "platform",
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      user_id: actor.id
    }
    stub_time
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::ChannelCarouselMetricsRecorder",
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      structure: Analytics::MetricsRecorder.structure_attributes(structure),
      event: "channel_carousel_deleted",
      timestamp: Time.now.to_i,
      additional_data: metadata
    )
    structure.destroy
    refute structure.persisted?
  end

  test 'pushes metric when deleted with channel parent' do
    org = create :organization
    actor = create :user, organization: org
    creator = create :user, organization: org
    channel = create :channel, organization: org
    structure = create :structure, {
      current_context: 'channel',
      current_slug: 'channel-cards',
      parent: channel,
      creator: creator
    }
    metadata = {
      is_admin_request: 0,
      platform: "platform",
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      user_id: actor.id
    }
    stub_time
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::ChannelCarouselMetricsRecorder",
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      structure: Analytics::MetricsRecorder.structure_attributes(structure),
      event: "channel_carousel_deleted",
      channel: Analytics::MetricsRecorder.channel_attributes(channel),
      timestamp: Time.now.to_i,
      additional_data: metadata
    )
    structure.destroy
    refute structure.persisted?
  end

  test 'pushes metric when edited with org parent' do
    org = create :organization
    actor = create :user, organization: org
    creator = create :user, organization: org
    Structure.any_instance.stubs(:push_channel_carousel_created_metric)
    channel = create :channel, organization: org
    structure = create :structure, {
      current_context: 'channel',
      current_slug: 'channel-cards',
      parent: org,
      creator: creator,
    }
    stub_time
    metadata = {
      is_admin_request: 0,
      platform: "platform",
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      user_id: actor.id
    }
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::ChannelCarouselMetricsRecorder",
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      structure: Analytics::MetricsRecorder.structure_attributes(structure).merge(
        "display_name" => "foo"
      ),
      changed_column: "display_name",
      old_val: nil,
      new_val: "foo",
      event: "channel_carousel_edited",
      timestamp: Time.now.to_i,
      additional_data: metadata
    )
    structure.update(display_name: "foo")
  end

  test 'pushes metric when edited with channel parent' do
    org = create :organization
    actor = create :user, organization: org
    creator = create :user, organization: org
    Structure.any_instance.stubs(:push_channel_carousel_created_metric)
    channel = create :channel, organization: org
    structure = create :structure, {
      current_context: 'channel',
      current_slug: 'channel-cards',
      parent: channel,
      creator: creator,
    }
    stub_time
    metadata = {
      is_admin_request: 0,
      platform: "platform",
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      user_id: actor.id
    }
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::ChannelCarouselMetricsRecorder",
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      structure: Analytics::MetricsRecorder.structure_attributes(structure).merge(
        "display_name" => "foo"
      ),
      changed_column: "display_name",
      old_val: nil,
      new_val: "foo",
      event: "channel_carousel_edited",
      channel: Analytics::MetricsRecorder.channel_attributes(channel),
      timestamp: Time.now.to_i,
      additional_data: metadata
    )
    structure.update(display_name: "foo")
  end

  test 'calls job when whitelisted columnn is updated' do
    assert Analytics::ChannelCarouselMetricsRecorder.whitelisted_change_attrs.include?(
      "display_name"
    )
    org = create(:organization)
    creator = create :user, organization: org
    channel = create :channel, organization: org, label: "asd"
    structure = create :structure, {
      current_context: 'channel',
      current_slug: 'channel-cards',
      parent: org,
      creator: creator,
      enabled: false,
      display_name: "foo"
    }
    Analytics::MetricsRecorderJob.unstub(:perform_later)
    Analytics::MetricsRecorderJob.expects(:perform_later).once
    structure.update display_name: "@foobar"
  end

  test 'doesnt call job for non-whitelisted column' do
    refute Analytics::ChannelCarouselMetricsRecorder.whitelisted_change_attrs.include?(
      "slug"
    )
    org = create(:organization)
    creator = create :user, organization: org
    channel = create :channel, organization: org, label: "asd"
    structure = create :structure, {
      current_context: 'channel',
      current_slug: 'channel-cards',
      parent: org,
      creator: creator,
      enabled: false,
      slug: "foo"
    }
    Analytics::MetricsRecorderJob.unstub(:perform_later)
    Analytics::MetricsRecorderJob.expects(:perform_later).never
    structure.update slug: "@foobar"
  end
end

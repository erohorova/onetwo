require 'test_helper'

class UserSubscriptionTest < ActiveSupport::TestCase
  should have_db_column(:org_subscription_id).of_type(:integer)
  should have_db_column(:organization_id).of_type(:integer)
  should have_db_column(:user_id).of_type(:integer)
  should have_db_column(:order_id).of_type(:integer)
  should have_db_column(:amount).of_type(:decimal)
  should have_db_column(:currency).of_type(:string)
  should have_db_column(:start_date).of_type(:date)
  should have_db_column(:end_date).of_type(:date)
  should have_db_column(:gateway_id).of_type(:string)

  should belong_to(:org_subscription)

  should validate_presence_of(:org_subscription_id)
  should validate_presence_of(:organization_id)
  should validate_presence_of(:user_id)
  should validate_presence_of(:order_id)
  should validate_presence_of(:amount)
  should validate_presence_of(:currency)
  should validate_presence_of(:start_date)
  should validate_presence_of(:end_date)
  should validate_presence_of(:gateway_id)

  test 'should trigger email notification job on create' do
    user = create(:user)
    user_subscription = create(:user_subscription, user_id: user.id)

    mailer_mock = mock()
    mailer_mock.expects(:deliver_later).returns(nil).times(1)
    PaymentMailer.expects(:user_subscription_notification).with(user_subscription.id).returns(mailer_mock).once

    user_subscription.run_callbacks(:commit)
  end

  test 'annual org subscription should return false' do
    organization = create(:organization)
    user = create(:user, organization: organization)

    user_subscription = UserSubscription.paid_annual_org_subscription?(
      org_id: organization.id,
      user_id: user.id)

    assert_equal false, user_subscription
  end
end

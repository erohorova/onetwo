require 'test_helper'

class DailyGroupStatTest < ActiveSupport::TestCase

  should belong_to :group
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
  end

  test 'empty daily group stat is considered inactive' do
    assert_equal false, DailyGroupStat.new.active_day?
    assert_equal true, DailyGroupStat.new(trending_tags: 'hello#10').active_day?
    assert_equal true, DailyGroupStat.new(total_active_forum_users: 10).active_day?
  end

  test 'active day condition excludes non-stats attributes' do
    daily_stat = DailyGroupStat.new(group_id:10,created_at:Time.now,updated_at:Time.now,id:10,date:'2004-10-10')
    assert_equal false, daily_stat.active_day?
    daily_stat.total_posts = 100
    assert_equal true, daily_stat.active_day?
  end

  test 'get daily stats inserts inactive entries in returned array' do
    group = create(:resource_group)
    group.save

    DailyGroupStat.create(group_id: group.id,
                          date: Time.now.utc-8.days, total_users_with_posts: 1,
                          total_users_with_comments: 0, total_active_forum_users: 11,
                          total_posts: 15, total_unanswered_posts: 10, total_comments: 1,
                          total_answered_posts: 5, trending_tags: 'hello#2,goodbye#4')
    DailyGroupStat.create(group_id: group.id,
                          date: Time.now.utc-6.days, total_users_with_posts: 1,
                          total_users_with_comments: 0, total_active_forum_users: 11,
                          total_posts: 15, total_unanswered_posts: 10, total_comments: 1,
                          total_answered_posts: 5, trending_tags: 'hello#2,goodbye#4')
    DailyGroupStat.create(group_id: group.id,
                          date: Time.now.utc-4.days, total_users_with_posts: 1,
                          total_users_with_comments: 0, total_active_forum_users: 11,
                          total_posts: 15, total_unanswered_posts: 10, total_comments: 1,
                          total_answered_posts: 5, trending_tags: 'hello#2,goodbye#4')
    DailyGroupStat.create(group_id: group.id,
                          date: Time.now.utc-2.days, total_users_with_posts: 1,
                          total_users_with_comments: 0, total_active_forum_users: 11,
                          total_posts: 15, total_unanswered_posts: 10, total_comments: 1,
                          total_answered_posts: 5, trending_tags: 'hello#2,goodbye#4')

    stats = DailyGroupStat.get_daily_stats(group.id, Time.now.utc, 20)
    assert_equal 4, stats.select{|s| s.active_day?}.count
    assert_equal 16, stats.select{|s| !s.active_day?}.count
  end

end

require 'test_helper'

class CardHistoryTest < ActiveSupport::TestCase
  class AssociationChangesTest < ActiveSupport::TestCase
    test "return prices changes" do
      skip "INTERMITTENT TEST FAILURE"
      card = create(:card)
      # when new price is added
      card.prices << build(:price, currency: "INR", amount: 12)
      card.save
      result = CardHistory.association_changes(card)
      assert_equal ["", "₹12.00"], result[:prices_attributes]

      # when another price is added
      card.prices << build(:price, currency: "USD", amount: 5)
      card.save
      result = CardHistory.association_changes(card)
      assert_equal ["₹12.00", "₹12.00, $5.00"], result[:prices_attributes]

      # when existing price is updated
      card.prices.last.update(amount: 10)
      result = CardHistory.association_changes(card)
      assert_equal ["₹12.00, $5.00", "₹12.00, $10.00"], result[:prices_attributes]
    end

    test "For poll cards: return quiz_question_options changes" do
      skip "INTERMITTENT TEST FAILURE"
      # when new option is added
      card = create(:card, card_type: "poll")
      card.quiz_question_options << build(:quiz_question_option, label: 'a')
      card.save
      result = CardHistory.association_changes(card)
      assert_equal ["", "a"], result[:options]

      # when another option is added
      card.quiz_question_options << build(:quiz_question_option, label: 'b')
      card.save
      result = CardHistory.association_changes(card)
      assert_equal ["a", "a, b"], result[:options]

      # when existing is updated and another option is added
      card.quiz_question_options.last.update(label: "b1")
      card.quiz_question_options << build(:quiz_question_option, label: 'c')
      card.save
      result = CardHistory.association_changes(card)
      assert_equal ["a, b", "a, b1, c"], result[:options]
    end

    test "For quiz cards: return quiz_question_options changes" do
      # when new option is added
      card = create(:card)
      card.quiz_question_options << build(:quiz_question_option, label: 'a', is_correct: true)
      card.quiz_question_options << build(:quiz_question_option, label: 'b')
      card.save
      result = CardHistory.association_changes(card)
      assert_equal ["", "a: answer, b"], result[:options]

      # when is_correct option is updated
      card.quiz_question_options.first.update(is_correct: false)
      card.quiz_question_options.last.update(is_correct: true)
      result = CardHistory.association_changes(card)
      assert_equal ["a: answer, b", "a, b: answer"], result[:options]
    end

    test "returns resource changes" do
      resource = create(:resource, url: "http://somewhere.com", image_url: "http://someimage.com")
      card = create(:card, resource_id: resource.id)
      result = CardHistory.association_changes(card)
      expected = [
        {"url"=>nil, "image_url"=>nil, "video_url"=>nil, "embed_html"=>nil},
        {"url"=>"http://somewhere.com", "image_url"=>"http://someimage.com",
        "video_url"=>"http://www.google.com?autoplay=1", "embed_html"=>nil}
      ]
      assert_equal expected, result["resource"]
    end

    test "returns level changes" do
      card = build(:card)
      card.build_card_metadatum(level: "advanced")
      card.save

      result = CardHistory.association_changes(card)
      assert_not_nil result[:card_metadatum_attributes]
      assert_equal [nil, "advanced"], result[:card_metadatum_attributes]["level"]

      card.card_metadatum.update(level: "beginner")

      result = CardHistory.association_changes(card)
      assert_equal ["advanced", "beginner"], result[:card_metadatum_attributes]["level"]
    end

    test "returns author changes" do
      user = create :user
      user1 = create :user, organization: user.organization
      card = create(:card, author: user)
      updated_fields = {
        "author_id"=>[user.id, user1.id]
      }
      CardHistory.stubs(:fetch_versions).returns(updated_fields)

      card.update(author_id: user1.id)
      result = CardHistory.fetch_versions(card.id)

      assert_equal [user.id, user1.id], result["author_id"]
    end
  end

  class CreateNewVersionTest < ActiveSupport::TestCase
    test "creates elasticsearch index for changes" do
      stub_request(:post, "http://localhost:9200/card_history_test/card_history")
        .to_return(status: 200, body: "", headers: {})
      CardHistory.create_new_version(1, 1, {title: ["", "test"]})
    end
  end

  class MiscChangesTest < ActiveSupport::TestCase
    test "returns changes if changes include resource" do
      changes = {"resource"=>[{"image_url"=>"something old"}, {"image_url"=>"something new"}]}
      result = CardHistory.misc_changes(1, changes)
      assert_equal changes, result
    end

    test "returns readable_card_type changes" do
      obj = {"readable_card_type"=>[7, 11]}
      result = CardHistory.misc_changes(1, obj)
      assert_equal ["Endorsement", "Music"], result["readable_card_type"]
    end

    test "returns existing changes if card not present" do
      changes = {"title" => ["Old", "New"]}
      result = CardHistory.misc_changes(1, changes)
      assert_equal changes, result
    end

    test "return resource changes" do
      resource = create(:resource)
      card = create(:card, resource_id: resource.id)
      saved_changes = {
        "url": "https://medium.com/s/story/how-natural-selection-screwed-us-be08be6ed482",
        "image_url": "https://cdn.filestackcontent.com/yyq9KFueS6KMmDu0Pk5L",
        "video_url": nil,
        "embed_html": nil
      }
      updated_fields = OpenStruct.new(updated_fields: saved_changes)
      CardResourceHistory.stubs(:find).returns(updated_fields)

      result = CardHistory.misc_changes(card.id, {"title" => ["Old", "new"]})
      assert_equal ["title", "resource"], result.keys
    end
  end
end

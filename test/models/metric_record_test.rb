
require 'test_helper'

class MetricRecordTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper
  self.use_transactional_fixtures = false

  setup do
    WebMock.disable!
    VideoStream.searchkick_index.delete if VideoStream.searchkick_index.exists?
    MetricRecord.create_index! force: true
  end

  teardown do
    WebMock.enable!
  end

  test 'should perform aggregations func' do
    # object id nil
    refute MetricRecord.new(action: "entry", actor_id: 1, object_type: 'card', object_id: nil).should_perform_aggregations?

    # unsupported object type
    refute MetricRecord.new(action: "entry", actor_id: 1, object_type: 'organization', object_id: 1).should_perform_aggregations?

    # object type nil
    refute MetricRecord.new(action: "login", actor_id: 1, object_type: nil, object_id: nil).should_perform_aggregations?

    # Cms cards
    refute MetricRecord.new(action: "create", object_type: "card", object_id: 1, owner_id: nil, actor_id: nil).should_perform_aggregations?

    # Support types of video streams
    assert MetricRecord.new(action: "impression", actor_id: 1, object_type: 'wowza_video_stream', object_id: 1).should_perform_aggregations?

    # support channel follow
    assert MetricRecord.new(action: "follow", actor_id: 1, object_type: 'channel', object_id: 1).should_perform_aggregations?

    # support user follow
    assert MetricRecord.new(action: "follow", actor_id: 1, object_type: 'user', object_id: 1).should_perform_aggregations?

    # support team creation
    assert MetricRecord.new(action: "create", actor_id: 1, object_type: 'team', object_id: 1).should_perform_aggregations?

    # support channel creation
    assert MetricRecord.new(action: "create", actor_id: 1, object_type: 'channel', object_id: 1).should_perform_aggregations?

    # support poll creation
    assert MetricRecord.new(action: "poll_create", actor_id: 1, object_type: 'card', object_id: 1).should_perform_aggregations?
  end

  test 'call jobs only if perform aggregations returns true' do
    MetricRecord.any_instance.stubs(:should_perform_aggregations?).returns(false)
    MetricRecord.any_instance.expects(:perform_aggregations).never
    MetricRecord.new.save

    MetricRecord.any_instance.stubs(:should_perform_aggregations?).returns(true)
    MetricRecord.any_instance.stubs(:perform_aggregations).returns(true).once
    MetricRecord.new.save
  end

  test 'should cast active record into id and type' do
    user = create(:user)
    card = create(:card)

    m = MetricRecord.new(actor: user, device_id: '12345', object: card, action: 'like', platform: 'web', properties: {duration: 10})
    m.save

    saved_record = MetricRecord.find(m.id)
    assert_equal 'user', saved_record.actor_type
    assert_equal user.id, saved_record.actor_id
    assert_equal '12345', saved_record.device_id
    assert_equal 'card', saved_record.object_type
    assert_equal card.id, saved_record.object_id
    assert_equal 'like', saved_record.action
    assert_equal Time.now.utc.to_date.to_s, saved_record.created_at.to_date.to_s
    assert_equal 'web', saved_record.platform
    assert_equal 10, saved_record.properties.duration
    assert_not_nil saved_record.created_at
    assert_not_nil saved_record.updated_at
  end

  test 'when a metric is created for a video stream it should update the video stream score' do
    VideoStreamUpdateScoreJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    # VideoStream.any_instance.stubs :create_index
    org = create(:organization)
    creator = create(:user, organization: org)
    video_stream = create(:wowza_video_stream, creator: creator)
    user = create(:user, organization: org)
    other_user = create(:user, organization: org)
    VideoStream.searchkick_index.refresh
    VideoStreamUpdateScoreJob.expects(:perform_later).with(video_stream)
    mr = MetricRecord.new(
      actor:       user,
      object_type: 'video_stream',
      object_id:   video_stream.id,
      owner:       other_user,
      action:      'playback',
      platform:    'web',
      device_id:   'device123',
      created_at:  2.days.ago
    )
    mr.save
    Search::VideoStreamSearch.new.search_by_id(video_stream.id)
    VideoStream.searchkick_index.refresh
    elasticsearch_object = Search::VideoStreamSearch.new.search_by_id(video_stream.id)
    assert_equal 1, elasticsearch_object.views_count
  end

  test 'metric record view count' do
    org = create(:organization)
    creator = create(:user, organization: org)
    video_stream = create(:wowza_video_stream, creator: creator)
    VideoStream.searchkick_index.refresh
    user = create(:user, organization: org)
    other_user = create(:user, organization: org)
    5.times do |i|
      mr = MetricRecord.new(actor: user, object_type: 'video_stream', object_id: video_stream.id, owner: other_user, action: 'playback', platform: 'web', device_id: 'device123', created_at: i.days.ago)
      mr.save
    end
    mr = MetricRecord.new(actor: user, object_type: 'video_stream', object_id: video_stream.id, owner: other_user, action: 'playback', platform: 'web', device_id: 'device123', created_at: 12.days.ago)
    mr.save

    MetricRecord.gateway.refresh_index!
    assert_equal 5, MetricRecord.video_stream_view_count(video_stream, 7.days.ago)
    assert_equal 6, MetricRecord.video_stream_view_count(video_stream, 30.days.ago)
  end

  test 'get oldest view' do
    org = create(:organization)
    creator = create(:user, organization: org)
    video_stream = create(:wowza_video_stream, creator: creator)
    VideoStream.searchkick_index.refresh
    user = create(:user, organization: org)
    other_user = create(:user, organization: org)
    mrs = []
    5.times do |i|
      mr = MetricRecord.new(actor: user, object_type: 'video_stream', object_id: video_stream.id, owner: other_user, action: 'playback', platform: 'web', device_id: 'device123', created_at: i.days.ago)
      mr.save
      mrs << mr
    end
    mr = MetricRecord.new(actor: user, object_type: 'video_stream', object_id: video_stream.id, owner: other_user, action: 'playback', platform: 'web', device_id: 'device123', created_at: 12.days.ago)
    mr.save
    MetricRecord.gateway.refresh_index!
    assert ( MetricRecord.oldest_view(video_stream, 7.days.ago) - 4.days.ago < 10)
    assert ( MetricRecord.oldest_view(video_stream, 30.days.ago) - 12.days.ago < 10 )
  end

  test 'is first such event' do
    user = create(:user)
    card = create(:card)
    Timecop.freeze(2.hours.ago) do
      first_impression = MetricRecord.new(actor: user, object_type: "card", object_id: card.id, action: "impression").save
      MetricRecord.gateway.refresh_index!
      impressions = MetricRecord.search(query: {filtered: {filter: {and: [{term: {actor_id: user.id}}, {term: {action: "impression"}}, {term: {object_type: "card"}}, {term: {object_id: card.id}}]}}}).results
      assert_equal 1, impressions.count
      assert MetricRecord.is_first_such_event?(impressions.first.attributes.except(:created_at, :updated_at).merge({created_at_to_i: impressions.first.created_at.to_i}))
    end

    second_impression = MetricRecord.new(actor: user, object_type: "card", object_id: card.id, action: "impression").save
    MetricRecord.gateway.refresh_index!
    impressions = MetricRecord.search(query: {filtered: {filter: {and: [{term: {actor_id: user.id}}, {term: {action: "impression"}}, {term: {object_type: "card"}}, {term: {object_id: card.id}}]}}}).results
    impressions.sort!{|a,b| a.created_at <=> b.created_at}
    assert_equal 2, impressions.length
    assert MetricRecord.is_first_such_event?(impressions.first.attributes.except(:created_at, :updated_at).merge({created_at_to_i: impressions.first.created_at.to_i}))
    refute MetricRecord.is_first_such_event?(impressions.last.attributes.except(:created_at, :updated_at).merge({created_at_to_i: impressions.last.created_at.to_i}))
  end

  test 'partial impression test for first such event' do
    user = create(:user)
    card = create(:card)
    Timecop.freeze(2.hours.ago) do
      partial_impression = MetricRecord.new(actor: user, object_type: "card", object_id: card.id, action: "impression", properties: {view: "summary"}).save
    end

    full_impression = MetricRecord.new(actor: user, object_type: "card", object_id: card.id, action: "impression", properties: {view: "full"}).save
    MetricRecord.gateway.refresh_index!
    impressions = MetricRecord.search(query: {filtered: {filter: {and: [{term: {actor_id: user.id}}, {term: {action: "impression"}}, {term: {object_type: "card"}}, {term: {object_id: card.id}}]}}}).results
    impressions.sort!{|a,b| a.created_at <=> b.created_at}
    assert_equal 2, impressions.length
    assert MetricRecord.is_first_such_event?(impressions.first.attributes_for_activejob)
    assert MetricRecord.is_first_such_event?(impressions.last.attributes_for_activejob)
  end

  test 'is partial impression' do
    MetricRecord.new(action: "impression", properties: {view: "summary"}).save
    MetricRecord.new(action: "impression", properties: {view: "full"}).save
    MetricRecord.gateway.refresh_index!

    partial = MetricRecord.search(query: {filtered: {filter: {and: [{term: {action: "impression"}}, {term: {"properties.view" => "summary"}}]}}}).results
    assert_equal 1, partial.length
    assert MetricRecord.is_partial_impression?(partial.first.attributes_for_activejob)

    full = MetricRecord.search(query: {filtered: {filter: {and: [{term: {action: "impression"}}, {term: {"properties.view" => "full"}}]}}}).results
    assert_equal 1, full.length
    refute MetricRecord.is_partial_impression?(full.first.attributes_for_activejob)
  end

  test 'should update smartbite score' do
    user = create(:user)
    card = create(:card)

    MetricRecord.stubs(:is_first_such_event?).returns(true)
    mr = MetricRecord.new(actor: user, object_type: "card", object_id: card.id, action: "impression")
    assert MetricRecord.should_increment_smartbite_score?(mr.attributes_for_activejob)

    MetricRecord.stubs(:is_first_such_event?).returns(false)
    refute MetricRecord.should_increment_smartbite_score?(mr.attributes_for_activejob)

    # Comments should always increment score
    mr = MetricRecord.new(actor: user, object_type: "card", object_id: card.id, action: "comment")
    assert MetricRecord.should_increment_smartbite_score?(mr.attributes_for_activejob)

    MetricRecord.stubs(:is_first_such_event?).returns(true)
    # draft pack activity should not update smartbite score
    draft_card = create(:card, card_type: 'pack', state: 'draft', card_subtype: 'simple')
    refute MetricRecord.should_increment_smartbite_score?(MetricRecord.new(action: "create", object_type: "card", object_id: draft_card.id).attributes_for_activejob)

    # Partial impression should not update smartbite score
    refute MetricRecord.should_increment_smartbite_score?(MetricRecord.new(action: "impression", properties: {view: "summary"}).attributes_for_activejob)
  end

  test 'is creation event' do
    assert MetricRecord.is_creation_event?(MetricRecord.new(object_type: "card", action: "create").attributes_for_activejob)
    assert MetricRecord.is_creation_event?(MetricRecord.new(object_type: "card", action: "videostream_create").attributes_for_activejob)
    assert MetricRecord.is_creation_event?(MetricRecord.new(object_type: "card", action: "publish").attributes_for_activejob)
    assert MetricRecord.is_creation_event?(MetricRecord.new(object_type: "card", action: "poll_create").attributes_for_activejob)
    refute MetricRecord.is_creation_event?(MetricRecord.new(object_type: "card", action: "comment").attributes_for_activejob)

    draft_card = create(:card, card_type: 'pack', state: 'draft', card_subtype: 'simple')
    refute MetricRecord.is_creation_event?(MetricRecord.new(action: "create", object_type: "card", object_id: draft_card.id))
  end

  test 'is consumption event' do
    MetricRecord.stubs(:is_first_such_event?).returns(true)
    refute MetricRecord.is_consumption_event?(MetricRecord.new(action: "create").attributes_for_activejob)
    assert MetricRecord.is_consumption_event?(MetricRecord.new(action: "impression").attributes_for_activejob)

    # Partial impression should not update smartbite score
    refute MetricRecord.is_consumption_event?(MetricRecord.new(action: "impression", properties: {view: "summary"}).attributes_for_activejob)

    MetricRecord.stubs(:is_first_such_event?).returns(false)
    refute MetricRecord.is_consumption_event?(MetricRecord.new(action: "impression").attributes_for_activejob)
  end

  test 'is comment event' do
    assert MetricRecord.is_comment_event?(MetricRecord.new(action: "comment").attributes_for_activejob)
    refute MetricRecord.is_comment_event?(MetricRecord.new(action: "impression").attributes_for_activejob)
  end

  test 'is uncomment event' do
    assert MetricRecord.is_uncomment_event?(MetricRecord.new(action: "uncomment").attributes_for_activejob)
    refute MetricRecord.is_uncomment_event?(MetricRecord.new(action: "impression").attributes_for_activejob)
  end

  test 'is like event' do
    MetricRecord.stubs(:is_first_such_event?).returns(true)
    assert MetricRecord.is_like_event?(MetricRecord.new(action: "like").attributes_for_activejob)
    refute MetricRecord.is_like_event?(MetricRecord.new(action: "comment").attributes_for_activejob)

    MetricRecord.stubs(:is_first_such_event?).returns(false)
    refute MetricRecord.is_like_event?(MetricRecord.new(action: "like").attributes_for_activejob)
  end

  test 'is unlike event' do
    MetricRecord.stubs(:is_first_such_event?).returns(true)
    assert MetricRecord.is_unlike_event?(MetricRecord.new(action: "unlike").attributes_for_activejob)
    refute MetricRecord.is_unlike_event?(MetricRecord.new(action: "comment").attributes_for_activejob)

    MetricRecord.stubs(:is_first_such_event?).returns(false)
    refute MetricRecord.is_unlike_event?(MetricRecord.new(action: "unlike").attributes_for_activejob)
  end

  test 'is mark completed event' do
    MetricRecord.stubs(:is_first_such_event?).returns(true)
    assert MetricRecord.is_mark_completed_event?(MetricRecord.new(action: "mark_completed_assigned").attributes_for_activejob)
    assert MetricRecord.is_mark_completed_event?(MetricRecord.new(action: "mark_completed_self").attributes_for_activejob)
    refute MetricRecord.is_mark_completed_event?(MetricRecord.new(action: "like").attributes_for_activejob)

    MetricRecord.stubs(:is_first_such_event?).returns(false)
    refute MetricRecord.is_mark_completed_event?(MetricRecord.new(action: "mark_completed_assigned").attributes_for_activejob)
  end

  test 'is bookmark event' do
    MetricRecord.stubs(:is_first_such_event?).returns(true)
    assert MetricRecord.is_bookmark_event?(MetricRecord.new(action: "bookmark").attributes_for_activejob)
    refute MetricRecord.is_bookmark_event?(MetricRecord.new(action: "like").attributes_for_activejob)

    MetricRecord.stubs(:is_first_such_event?).returns(false)
    refute MetricRecord.is_bookmark_event?(MetricRecord.new(action: "bookmark").attributes_for_activejob)
  end

  test 'should aggregate user level metrics' do
    UserLevelMetricsAggregatorJob.expects(:perform_later).returns(nil).once
    MetricRecord.new(actor_id: 1, object_id: 1, object_type: "card", owner_id: 2, action: "impression").save
  end

  test 'should aggregate user content level metrics' do
    UserContentLevelMetricsAggregatorJob.expects(:perform_later).returns(nil).once
    MetricRecord.new(actor_id: 1, object_id: 1, object_type: "card", owner_id: 2, action: "impression").save
  end

  test 'should aggregate org level metrics' do
    OrgLevelMetricsAggregatorJob.expects(:perform_later).returns(nil).once
    MetricRecord.new(actor_id: 1, object_id: 1, object_type: "card", owner_id: 2, action: "impression").save
  end

  test 'should aggregate content level metrics' do
    ContentLevelMetricsAggregatorJob.expects(:perform_later).returns(nil).once
    MetricRecord.new(actor_id: 1, object_id: 1, object_type: "card", owner_id: 2, action: "impression").save
  end

  test 'should aggregate team level metrics' do
    TeamLevelMetricsAggregatorJob.expects(:perform_later).returns(nil).once
    MetricRecord.new(actor_id: 1, object_id: 1, object_type: "card", owner_id: 2, action: "impression").save
  end

  test 'should aggregate user topic level metrics' do
    UserTopicLevelMetricsAggregatorJob.expects(:perform_later).returns(nil).once
    MetricRecord.new(actor_id: 1, object_id: 1, object_type: "card", owner_id: 2, action: "impression").save
  end

  test 'should reaggregate user level metrics' do
    user1, user2 = create_list(:user, 2)
    (1..10).each do |i|
      MetricRecord.new(actor_id: user1.id, object_id: 1, object_type: 'card', action: "impression").save
    end

    MetricRecord.new(actor_id: user2.id, object_id: 1, object_type: 'card', action: "impression").save

    MetricRecord.gateway.refresh_index!

    MetricRecord.search(query: {filtered: {filter: {and: [{term: {actor_id: user1.id}}]}}}).results.each do |mr|
      UserLevelMetricsAggregatorJob.expects(:perform_later).with(mr.attributes_for_activejob).returns(nil).once
      UserContentLevelMetricsAggregatorJob.expects(:perform_later).with(mr.attributes_for_activejob).returns(nil).once
    end
    MetricRecord.reaggregate_user_level_metrics(user1.id)
  end

  test 'should reaggregate time spent metrics' do
    user1, user2 = create_list(:user, 2)
    (1..10).each do |i|
      MetricRecord.new(actor_id: user1.id, object_id: 1, object_type: 'card', action: "impression").save
    end

    MetricRecord.new(actor_id: user2.id, object_id: 1, object_type: 'card', action: "impression").save

    MetricRecord.gateway.refresh_index!

    MetricRecord.search(query: {filtered: {filter: {and: [{term: {actor_id: user1.id}}]}}}).results.each do |mr|
      UserTimeSpentAggregatorJob.expects(:perform_later).with(mr.attributes_for_activejob).returns(nil).once
    end
    MetricRecord.reaggregate_time_spent_metrics(user1.id)
  end

  test 'should reaggregate org level metrics' do
    org1, org2 = create_list(:organization, 2)
    org1_users = create_list(:user, 2, organization: org1)
    org2_users = create_list(:user, 2, organization: org2)
    (org1_users | org2_users).each do |user|
      MetricRecord.new(actor_id: user.id, object_id: 1, object_type: 'card', action: "impression").save
    end

    MetricRecord.gateway.refresh_index!

    MetricRecord.search(query: {filtered: {filter: {and: [{terms: {actor_id: org1_users.map(&:id)}}]}}}).results.each do |mr|
      OrgLevelMetricsAggregatorJob.expects(:perform_later).with(mr.attributes_for_activejob).returns(nil).once
    end
    MetricRecord.reaggregate_org_level_metrics(org1.id)
  end

  test 'should reaggregate team level metrics' do
    org1, org2 = create_list(:organization, 2)
    org1_teams = create_list(:team, 2, organization: org1)
    org2_teams = create_list(:team, 2, organization: org2)

    org1_user = create(:user, organization: org1)
    org2_user = create(:user, organization: org2)

    org1_teams.first.add_user(org1_user)
    org2_teams.first.add_user(org2_user)

    MetricRecord.new(actor_id: org1_user.id, object_id: 1, object_type: 'card', action: "impression").save
    MetricRecord.new(actor_id: org2_user.id, object_id: 1, object_type: 'card', action: "impression").save

    MetricRecord.gateway.refresh_index!

    MetricRecord.search(query: {filtered: {filter: {and: [{term: {actor_id: org1_user.id}}]}}}).results.each do |mr|
      TeamLevelMetricsAggregatorJob.expects(:perform_later).with(mr.attributes_for_activejob).returns(nil).once
    end
    MetricRecord.reaggregate_team_level_metrics(org1_teams.first)
  end

  test 'should reaggregate content level metrics' do
    org1, org2 = create_list(:organization, 2)

    org1_user = create(:user, organization: org1)
    org2_user = create(:user, organization: org2)

    MetricRecord.new(actor_id: org1_user.id, object_id: 1, object_type: 'card', action: "impression").save
    MetricRecord.new(actor_id: org2_user.id, object_id: 1, object_type: 'card', action: "like").save
    MetricRecord.new(actor_id: org2_user.id, object_id: 2, object_type: 'video_stream', action: "comment").save

    MetricRecord.gateway.refresh_index!

    MetricRecord.search(query: {filtered: {filter: {and: [{term: {object_id: 1}}, {term: {object_type: "card"}}]}}}).results.each do |mr|
      ContentLevelMetricsAggregatorJob.expects(:perform_later).with(mr.attributes_for_activejob).returns(nil).once
    end
    MetricRecord.reaggregate_content_level_metrics(1, 'card')
  end

  test 'should reaggregate user topic level metrics' do
    interests = create_list(:interest, 2)

    cards = create_list(:card, 2)
    cards.each {|card| card.tags << interests.first}

    MetricRecord.new(actor_id: 1, object_id: cards.first.id, object_type: 'card', action: "impression").save
    MetricRecord.new(actor_id: 1, object_id: cards.first.id, object_type: 'card', action: "like").save

    MetricRecord.gateway.refresh_index!

    MetricRecord.search(query: {filtered: {filter: {and: [{term: {object_id: cards.first.id}}, {term: {object_type: "card"}}]}}}).results.each do |mr|
      UserTopicLevelMetricsAggregatorJob.expects(:perform_later).with(mr.attributes_for_activejob).returns(nil).once
    end
    MetricRecord.reaggregate_user_topic_level_metrics(interests.first)
  end

  test 'should call mark actor as complete only when destiny event' do
    MetricRecord.any_instance.stubs(:is_destiny_event?).returns(false)
    MetricRecord.any_instance.expects(:mark_actor_as_destiny_user).never
    MetricRecord.new.save

    MetricRecord.any_instance.stubs(:is_destiny_event?).returns(true)
    MetricRecord.any_instance.stubs(:mark_actor_as_destiny_user).returns(true).once
    MetricRecord.new.save
  end

  test 'mark actor as destiny user' do
    u = create(:user, is_complete: false)
    mr = MetricRecord.new(actor_id: u.id)
    mr.mark_actor_as_destiny_user
    assert u.reload.is_complete
  end

  test 'is destiny event' do
    MetricRecord.any_instance.stubs(:should_perform_aggregations?).returns(false)
    mr = MetricRecord.new
    refute mr.is_destiny_event?

    MetricRecord.any_instance.stubs(:should_perform_aggregations?).returns(true)
    mr = MetricRecord.new(platform: 'forum')
    refute mr.is_destiny_event?

    mr = MetricRecord.new(platform: 'web')
    assert mr.is_destiny_event?
  end

  test 'get users for action performed' do
    users = create_list(:user, 20)
    card  = create(:card)
    users.each do |user|
      MetricRecord.new(actor_id: user.id, object_id: card.id, object_type: 'card', action: "impression").save
    end

    # partial impression should be skipped
    other_user = create(:user)
    MetricRecord.new(actor_id: other_user.id, object_id: card.id, object_type: 'card', action: "impression", properties: {view: 'summary'}).save

    MetricRecord.gateway.refresh_index!

    metric_users = MetricRecord.get_users(action: 'impression', content_id: card.id, content_type: 'card', limit: 10, offset: 0)
    assert_same_elements users.first(10).map(&:id), metric_users.map(&:id)

    metric_users = MetricRecord.get_users(action: 'impression', content_id: card.id, content_type: 'card', limit: 10, offset: 10)
    assert_same_elements users.last(10).map(&:id), metric_users.map(&:id)

    assert_empty MetricRecord.get_users(action: 'impression', content_id: card.id, content_type: 'card', limit: 10, offset: 20)
  end

  test 'get unique views counts for card' do
    users = create_list(:user, 5)
    card  = create(:card)
    users.each do |user|
      # same user views card 2 times
      2.times do
        MetricRecord.new(actor_id: user.id, object_id: card.id, object_type: 'card', action: "impression").save
      end
    end

    # partial impression should be skipped
    other_user = create(:user)
    MetricRecord.new(actor_id: other_user.id, object_id: card.id, object_type: 'card', action: "impression", properties: {view: 'summary'}).save

    MetricRecord.gateway.refresh_index!
    assert_equal users.length, MetricRecord.views_count(content_id: card.id, content_type: 'card')
  end

  test 'should consume card on other actionable events' do
    card = create(:card)

    # like action
    user = create(:user)
    MetricRecord.new(actor_id: user.id, actor_type: 'user', object_id: card.id, object_type: 'card', action: "like").save
    MetricRecord.gateway.refresh_index!

    impressions = MetricRecord.search(query: {filtered: {filter: {and: [{term: {actor_id: user.id}}, {term: {action: "impression"}}, {term: {object_type: "card"}}, {term: {object_id: card.id}}]}}}).results
    assert_equal 1, impressions.count

    # visit action
    other_user = create(:user)
    MetricRecord.new(actor_id: other_user.id, actor_type: 'user', object_id: card.id, object_type: 'card', action: "visit").save
    MetricRecord.gateway.refresh_index!

    impressions = MetricRecord.search(query: {filtered: {filter: {and: [{term: {actor_id: other_user.id}}, {term: {action: "impression"}}, {term: {object_type: "card"}}, {term: {object_id: card.id}}]}}}).results
    assert_equal 1, impressions.count
  end

  test 'should normalize object and type' do 
    org = create(:organization)
    user = create(:user, organization: org)
    cards = create_list(:card, 2, hidden: true, author: user)
    pathway = create(:card, author: user, card_type: 'pack')
    cards.each do |card|
      CardPackRelation.add(cover_id: pathway.id, add_id: card.id, add_type: card.class.name)
    end
    pathway.publish!

    met_record1= MetricRecord.new(actor_id: user.id, object_id: cards.last.id, object_type: 'card')
    met_record2= MetricRecord.new(actor_id: user.id, object_id: cards.last.id, object_type: 'card')
    met_record3= MetricRecord.new(actor_id: user.id, object_id: pathway.id, object_type: 'collection')
    assert_equal 'card', met_record1.normalized_object_type_and_id[:object_type]
    assert_equal 'card', met_record2.normalized_object_type_and_id[:object_type]
    assert_equal 'collection', met_record3.normalized_object_type_and_id[:object_type]

  end
end

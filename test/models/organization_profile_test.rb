require 'test_helper'

class OrganizationProfileTest < ActiveSupport::TestCase
  should belong_to(:organization)
end

require 'test_helper'

class QuizQuestionOptionTest < ActiveSupport::TestCase

  test 'as json' do
    poll = create(:card, card_type: 'poll', card_subtype: 'text')

    opt1 = create(:quiz_question_option, quiz_question: poll, label: 'option1', is_correct: false)
    assert_equal({'id' => opt1.id, 'label' => 'option1', 'is_correct' => false, 'image_url' => nil}, opt1.as_json)

    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr = FileResource.create(attachment: pic.open)
    opt2 = create(:quiz_question_option, quiz_question: poll, label: 'option2', is_correct: true)
    opt2.file_resource = fr

    assert_equal({'id' => opt2.id, 'label' => 'option2', 'is_correct' => true, 'image_url' => fr.file_url}, opt2.as_json)
  end
end

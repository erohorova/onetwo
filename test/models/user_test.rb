# -*- coding: utf-8 -*-
require 'test_helper'

class UserTest < ActiveSupport::TestCase
  setup do
    VideoStream.any_instance.stubs(:create_index)
    VideoStream.any_instance.stubs(:update_index)

    # mocha requires a a record in db to be successfully able to run validations
    # if not found it will try to create its own and bad things happen.
    # https://github.com/thoughtbot/shoulda-matchers/issues/336
    # @org = create(:organization)
    # create(:user, organization: @org)
    stub_okta_user_update

    stub_request(:post, "https://api.branch.io/v1/url").to_return(:status => 200, :body => "", :headers => {})
  end

  should have_many :card_reportings
  should have_many :cards_users
  should have_many :user_preferences
  should have_many(:private_groups).class_name('Group').through(:groups_users).source(:group).conditions(type: 'PrivateGroup')

  should have_many(:identity_providers).dependent(:destroy)

  should have_many :teams_users
  should have_many(:teams).through :teams_users

  should have_many :users_external_courses
  should have_many(:external_courses).through :users_external_courses

  should have_many :users_integrations
  should have_many(:integrations).through :users_integrations

  should have_many :interests
  should have_many :cards
  should have_many :schools
  should have_many :assignments
  should have_many(:team_assignments).through(:assignments)
  should have_one(:external_profile).class_name('Profile')

  # should have a unique handle
  # INTERMITTENTLY FAILING
  # should validate_uniqueness_of(:handle).scoped_to(:organization_id)
  should validate_length_of(:bio).is_at_most(1000).with_long_message('is too long (maximum is 1000 characters)')

  test 'validates email' do
    # email = "person...@example.com"
    user = build :user
    assert user.valid?

    user = build :user, email: 'example+1@domain.com'
    assert user.valid?

    user = build :user, email: "example D'souza@domain.com"
    refute user.valid?

    user = build :user, email: "example souza@@domain.com"
    refute user.valid?

    user = build :user, email: "example D'souza@"
    refute user.valid?

    user = build :user, email: "example D'souza"
    refute user.valid?
  end

  test 'data_export_status enum' do
    org = create :organization
    user = create :user, organization: org
    assert user.data_export_status.nil?
    expected = {"started"=>0, "done"=>1, "error"=>2}
    actual = User.data_export_statuses
    assert_equal expected, actual
  end

  test 'upload_mode virtual attribute' do
    u = User.new
    assert_equal nil, u.upload_mode
    u.upload_mode = "foo"
    assert_equal "foo", u.upload_mode
    refute u.attributes.key? "upload_mode"
  end

  test 'unique email address' do
    email = generate(:email_address)
    organization = create(:organization)
    user = create(:user, email: email, organization: organization)
    assert_raise ActiveRecord::RecordInvalid do
      user = create(:user, email: email, organization: organization)
    end
  end

  test 'shoulde return custom_quarter_dates ' do
    org = create(:organization)
    user = create :user, organization: org
    current_year = Time.current.year
    quarter1 = create(:quarter, name: "quarter-1", start_date: "#{current_year}-02-02", end_date: "#{current_year}-05-01", organization_id: org.id)
    quarter = Assignment.custom_quarter_dates(user, "quarter-1")
    assert_equal "#{current_year}-02-02" , quarter[:begin_date].to_s
    assert_equal "#{current_year}-05-01" , quarter[:end_date].to_s
  end

  test "should return user for the reset token" do
    organization = create(:organization)
    user = create(:user, organization: organization)
    token = user.send_reset_password_instructions

    fetch_user = User.devise_find_by_reset_password_token(token)

    assert_equal user, fetch_user
  end

  test 'should delete structure_item if user is deleted/suspended(custom user carousel)' do
    skip('structure item should not be hard deleted when user suspended')
    organization = create(:organization)
    user = create(:user, organization: organization)
    user1 = create(:user, organization: organization)
    structure = create(:structure, current_context: 'channel', current_slug: 'users', creator: user1)
    structure_item = create(:structured_item, structure: structure, entity: user)
    structure_item_id = structure_item.id
    user.suspend!
    assert_nil StructuredItem.find_by_id(structure_item_id)
  end

  test "should not return user for the reset token" do
    organization = create(:organization)
    user = create(:user, organization: organization)
    token = user.send_reset_password_instructions

    fetch_user = User.devise_find_by_reset_password_token(user.reset_password_token)

    assert_not_equal user, fetch_user
  end

  test 'should compete card for user' do
    org = create(:organization)
    user = create(:user, organization_id: org.id)
    card = create(:card, author: user, organization_id: org.id)
    assert_difference -> {UserContentCompletion.count}, 1 do
      user.complete_card(card)
      ucc = UserContentCompletion.where(user_id: user.id, completable_id: card.id).first
      assert_equal ucc.user_id, user.id
      assert_equal ucc.completable_id, card.id
    end
  end

  test '#expire_card_subscription should update end_date of card_subscription with current time' do
    initial_date = Time.now - 2.days
    org = create(:organization)
    user = create(:user, organization_id: org.id)
    card = create(:card, author: user, organization_id: org.id)
    card_subscription = create(:card_subscription, card: card, user: user, start_date: initial_date)

    user.expire_card_subscription(card: card)
    assert_not_equal initial_date, card_subscription.reload.end_date.to_date
  end

  test 'set config' do
    user = create(:user)
    user.set_config(:key1, 'value1')
    assert_equal '{"key1":"value1"}', user.configs
    user.set_config(:key2, true)
    assert_equal '{"key1":"value1","key2":true}', user.configs
  end

  test 'get config' do
    user = create(:user, configs: '{"key1":"value1","key2":true}')
    assert_equal "value1", user.get_config(:key1)
    assert_equal "value1", user.get_config('key1')
    assert_equal true, user.get_config(:key2)
    assert_equal true, user.get_config('key2')

    no_configs_user = create(:user)
    assert_equal nil, no_configs_user.get_config(:key1)
  end

  test 'set time of accept invitation after user create' do
    EdcastActiveJob.unstub :perform_later
    Users::AfterCommitOnCreateJob.unstub :perform_later

    organization = create(:organization)
    user_email = Faker::Internet.email
    invitation = create(:invitation, recipient_email: user_email,
      invitable: organization, sender: create(:user, organization: organization))
    User.any_instance.unstub :cleanup_invitation
    user = create(:user, email: user_email, organization: organization)
    user.run_callbacks :commit

    assert invitation.reload.accepted_at
  end

  test "admin user" do
    assert_difference ->{SuperAdminUser.count} do
      user = create(:user, :admin)
      assert user.is_admin?
    end
  end

  test "non-admin user" do
    assert_no_difference ->{SuperAdminUser.count} do
      user = create(:user, email:"a@b.com")

      assert_not user.is_admin?

      user2 = create(:user, email:nil)
      assert_not user2.is_admin?
    end

  end

  test "ends with coursemaster bug" do
    user = create(:user, email:"a@ccourse-master.com")
    assert_not user.is_admin?
  end

  test "should have unique email in org" do
    org = create(:organization)
    assert build(:user, email: "a@a.com", organization: org).save
    assert_not build(:user, email: "a@a.com", organization: org).save
    assert build(:user, email:"a@a.com", organization: create(:organization)).save
  end

  test 'should save user without email' do
    assert build(:user, email: nil).save
  end

  test 'should not save user with blank email' do
    assert_not build(:user, email: '').save
  end

  test "should have groups" do
    user = create(:user)
    group = create(:resource_group)
    assert_difference -> {user.groups.count}, 1 do
      group.add_member user
    end
  end

  test "get a user's classmates" do
    user1 = create(:user, email: 'a@a.com')
    user2 = create(:user, email: 'b@b.com')
    group = create(:resource_group)
    group.add_member user1
    group.add_member user2
    classmates = user1.classmates group
    assert_equal classmates.count, 1
    assert_equal classmates[0], user2
  end

  test "no students in group, should return empty array" do
    group = create(:resource_group)
    user = create(:user, email: 'a@a.com')
    classmates = user.classmates group
    assert_equal classmates.count, 0
  end

  test "no classmates in group, should return empty array" do
    user = create(:user, email: 'a@a.com')
    group = create(:resource_group)
    group.add_member user
    classmates = user.classmates group
    assert_equal classmates.count, 0
  end

  test "group if invalid, return empty array" do
    user = create(:user, email: 'a@a.com')
    classmates = user.classmates nil
    assert_equal classmates.count, 0
  end

  test "should return items pending for curation for curatable channel" do
    org = create :organization
    user = create :user, organization: org
    channel = create :channel, curate_only: true, organization: org
    channels_curator = create :channels_curator, user: user, channel: channel
    card = create :card, created_at: 5.days.ago, organization: org, author: user
    channels_card = create :channels_card, card: card, created_at: 5.days.ago, channel: channel

    items = user.channels_to_curate
    assert_equal channel.id, items.first.channel_id

    channel.update(curate_only: false)

    items = user.channels_to_curate
    assert_equal true, items.empty?
  end

  test "#viewable_channels method to only show unique accessible channels for a user" do
    org = create(:organization)
    user = create(:user, organization: org)
    channel_array = []
    team1 = create(:team, organization: org)
    team2 = create(:team, organization: org)
    team1.add_member(user)
    team2.add_member(user)

    pub_channel1 = create(:channel, organization: org)
    pub_channel2 = create(:channel, organization: org)
    private_channel1 = create(:channel, organization: org, is_private: true)
    private_channel2 = create(:channel, organization: org, is_private: true)
    private_channel3 = create(:channel, organization: org, is_private: true)
    create(:teams_channels_follow, team: team1, channel: private_channel3)
    create(:teams_channels_follow, team: team2, channel: private_channel3)
    create(:follow, followable: private_channel1, user: user)

    channel_array << pub_channel1
    channel_array << private_channel1
    channel_array << private_channel2
    channel_array << private_channel3
    channel_array << pub_channel2

    viewable_channels = user.viewable_channels(channel_array.map(&:id)).pluck(:id)
    assert_not_includes viewable_channels, private_channel2.id
    assert_equal [pub_channel1.id, pub_channel2.id, private_channel1.id, private_channel3.id].sort, viewable_channels.sort
  end

  test '#viewable_channels should not return channels user has been removed from' do
    org = create(:organization)
    user = create(:user, organization: org)

    private_channel1 = create(:channel, organization: org, is_private: true)
    create(:follow, user_id: user.id, followable: private_channel1)
    private_channel2 = create(:channel, organization: org, is_private: true)
    create(:follow, user_id: user.id, followable: private_channel2)

    assert_same_elements [private_channel1, private_channel2], user.viewable_channels([private_channel1.id, private_channel2.id])
    follows = Follow.find_by(user_id: user.id, followable: private_channel2)
    follows.delete
    assert_not_includes user.viewable_channels([private_channel1.id, private_channel2.id]), private_channel2
  end

  test '#full_name returns preferred_name from auth_info or user name or truncated email' do
    user = create(:user, first_name: 'Bill', last_name: 'Wilkins')
    identity_provider = create :identity_provider, :saml, user: user
    user.reload
    assert_equal user.full_name, 'Bill Wilkins'

    user = create(:user, first_name: 'Bill', last_name: 'Wilkins')
    assert_equal user.full_name, 'Bill Wilkins'

    user = create(:user, first_name: '', last_name: 'Wilkins')
    assert_equal user.full_name, 'Wilkins'

    user = create(:user, first_name: 'Valek', last_name: nil)
    assert_equal user.full_name, 'Valek'

    user = create(:user, first_name: nil, last_name: '', email: 'valek@conjuring.com')
    assert_equal user.full_name, 'valek'
  end

  test '#name returns user name or truncated email' do
    user = create(:user, first_name: 'Bharath', last_name: 'Bhat')
    assert_equal user.name, 'Bharath Bhat'

    user = create(:user, first_name: '', last_name: 'Bhat')
    assert_equal user.name, 'Bhat'

    user = create(:user, first_name: 'Bharath', last_name: nil)
    assert_equal user.name, 'Bharath'

    user = create(:user, first_name: nil, last_name: '', email: 'bbb@here.com')
    assert_equal user.name, 'bbb'
  end

  test "should get score for user in group" do
    Post.any_instance.stubs(:p_read_by_creator)
    org = create(:organization)
    user = create(:user, organization: org)
    group = create(:resource_group, organization: org)
    group.add_member user
    faculty = create(:user, organization: org)
    group.add_admin faculty

    post = create(:post, user: user, group: group)
    comment = create(:comment, user: user, commentable: post)

    voter = create(:user, organization: org)
    group.add_member voter
    create(:vote, votable: post, voter: voter)
    create(:vote, votable: comment, voter: voter)
    create(:approval, approvable: post, approver: faculty)

    score_entry = user.score group
    assert_equal score_entry[:score], 2
    assert_equal score_entry[:stars], 1
  end

  test 'should return nil if score entry does not exist' do
    user = create(:user)
    group = create(:resource_group)

    user_score = user.score(group)
    assert_equal user_score[:stars], 0
    assert_equal user_score[:score], 0
  end

  test 'should validate user picture' do
    stub_request(:get, /http.*/).
    with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
    to_return(:status => 200, :body => File.read(Rails.root.join('test/fixtures/images/user2.jpg')), :headers => {content_type:'image/jpg'})

    user = create(:user)
    user.picture_url = 'not a picture'
    assert_not user.save

    user.picture_url = 'http://notanimage.html'
    assert user.save

    user.picture_url = 'https://facebook.png'
    assert user.save

    user.picture_url = 'http://user.png?1232323'
    assert user.save

    user.picture_url = 'http://actualimage.png'
    assert user.save
  end

  test 'should trigger set time zone job for user' do
    user = create(:user)
    SetUserTimeZoneJob.expects(:perform_later).never
    user.set_time_zone(ip_address: nil)

    SetUserTimeZoneJob.expects(:perform_later).with(user_id: user.id, ip_address: "0.0.0.0").once
    user.set_time_zone(ip_address: "0.0.0.0")
  end

  # class SlowTest < ActiveSupport::TestCase
  #   self.use_transactional_fixtures = false
  #   self.use_shared_db_connection = false
  #   setup do
  #     stub_request(:post, "http://localhost:9200/_bulk")
  #   end

  #   test 'should associate to forum user if present' do
  #     default_org = Organization.default_org
  #     client = create(:client)
  #     custom_org = create(:organization, client: client)
  #     default_org_user = create(:user, organization: Organization.default_org)
  #     create(:clients_user, user: default_org_user, client: client, client_user_id: default_org_user.id)

  #     custom_org_user = create(:user, email: default_org_user.email, organization: custom_org)
  #     custom_org_user.reload
  #     assert_equal default_org_user, custom_org_user.parent_user

  #     # Dont try to associate for nil emails
  #     custom_org_user_no_email = create(:user, email: nil, organization: custom_org)
  #     custom_org_user_no_email.reload
  #     assert_nil custom_org_user_no_email.parent_user_id

  #     # Don't associate if clients_user entry not present
  #     default_org_user_no_client = create(:user, organization: Organization.default_org)
  #     custom_org_user_no_client = create(:user, email: default_org_user_no_client.email, organization: custom_org)
  #     custom_org_user_no_client.reload
  #     assert_nil custom_org_user_no_client.parent_user_id
  #   end

  #   test 'should not call avatar image for updated users' do
  #     AnonImageJob.unstub(:perform_later)
  #     AvatarImageJob.unstub(:perform_later)
  #     stub_request(:get, /http.*/).
  #     with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
  #     to_return(:status => 200, :body => File.read(Rails.root.join('test/fixtures/images/user2.jpg')), :headers => {content_type:'image/jpg'})

  #     AvatarImageJob.any_instance.expects(:perform).once
  #     user = build(:user, skip_generate_images: false)
  #     user.picture_url = 'http://someimage.png'
  #     user.save!

  #     # should not call any job
  #     AnonImageJob.any_instance.expects(:perform).never
  #     user.reload
  #     user.first_name = 'name change'
  #     user.save!

  #     # set picture attribute again
  #     AvatarImageJob.any_instance.expects(:perform).once
  #     user.picture_url = 'http://someotherimage.png'
  #     user.save!

  #     # Teardown
  #     AnonImageJob.stubs(:perform_later)
  #     AvatarImageJob.stubs(:perform_later)
  #   end

  #   test 'photo method should work' do
  #     stub_request(:get, /http.*/).
  #     with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
  #     to_return(:status => 200, :body => File.read(Rails.root.join('test/fixtures/images/user2.jpg')), :headers => {content_type:'image/jpg'})

  #     user = create(:user, skip_generate_images: false)
  #     default_avatar = '/assets/new-anonymous-user-small.jpeg'
  #     assert_equal default_avatar, URI.parse(user.photo).path

  #     user.picture_url = 'http://someimage.png'
  #     user.save
  #     user.reload #image upload is done in the background. so we need to refresh it
  #     assert_not_nil user.avatar.url(:small)
  #     assert_not_nil user.photo(:small, send_default: false)
  #     refute_equal default_avatar, URI.parse(user.photo).path
  #   end
  # end

  test "should remove anonimage" do
    user = create(:user)
    assert_not File.exists?(Rails.root.join("app/assets/images/anonimage-user.png"))
  end

  test "should work with facebook connections" do
    friend = create(:user)
    user = create(:user)
    assert_difference -> { UserConnection.count } do
      user.facebook_connections << friend
    end

    assert user.facebook_connections, [friend]
    assert user.facebook_connection? friend
  end

  test "should work with linkedin connections" do
    friend = create(:user)
    user = create(:user)
    user.linkedin_connections << friend

    assert user.linkedin_connections, [friend]
    assert user.linkedin_connection? friend
  end

  test "should update connections" do
    user = create(:user)
    fb_friends = create_list(:user, 4)
    user.facebook_connections << fb_friends
    new_fb_friends = create_list(:user, 3)
    final_friends = [fb_friends.first] + new_fb_friends
    user.facebook_connections_update final_friends.map(&:id)
    assert_equal final_friends.map(&:id), user.facebook_connections.map(&:id)
  end

  test 'all_connections' do
    user = create(:user)
    fb_friends = create_list(:user, 4)
    user.facebook_connections << fb_friends
    linkedin_friends = create_list(:user, 3)
    user.linkedin_connections << linkedin_friends
    assert_equal 4, user.all_facebook_connections.count
    assert_equal 3, user.all_linkedin_connections.count
    assert_equal 7, user.all_connections.count
  end

  test "don't get closed groups" do
    u = create(:user)
    g_open = create(:group)
    g_open.add_member(u)
    g_closed = create(:group)
    g_closed.add_member(u)
    g_closed.update_attributes(close_access: true)

    user_groups = u.group_ids
    assert_equal 1, user_groups.count
    assert_equal g_open.id, user_groups[0]
  end

  test 'not banned from groups association should work' do
    u = create(:user)
    g_open = create(:group)
    g_open.add_member(u)
    g_closed = create(:group)
    g_closed.add_member(u)
    g_closed.update_attributes(close_access: true)

    g_banned = create(:group)
    g_banned.add_member(u)
    g_banned.ban_user(u)

    user_groups = u.not_banned_from_group_ids
    assert_equal 2, user_groups.count
    assert_equal [g_open.id, g_closed.id].sort, user_groups.sort

    all_groups = u.all_group_ids
    assert_equal 3, all_groups.count
    assert_equal [g_open.id, g_closed.id, g_banned.id].sort, all_groups.sort
  end

  test 'user preferences group opt-out' do
    owner = create(:user)
    user = create(:user)
    resource_group = create(:resource_group)
    resource_group.add_member(user)
    private_group = create(:private_group, creator: owner, resource_group: resource_group)

    assert_not user.pref_group_opt_out?(resource_group, 'notification.announcement')
    assert_not user.pref_group_opt_out?(resource_group, 'notification.post')
    assert_not user.pref_group_opt_out?(resource_group, 'notification.group_welcome')

    assert_not user.pref_group_opt_out?(private_group, 'notification.announcement')
    assert_not user.pref_group_opt_out?(private_group, 'notification.post')
    assert_not user.pref_group_opt_out?(private_group, 'notification.group_welcome')

    p1 = create(:user_preference, user: user, preferenceable: private_group,
                key: 'notification.announcement.opt_out', value: 'true')
    p2 = create(:user_preference, user: user, preferenceable: private_group,
                key: 'notification.post.opt_out', value: 'true')
    p3 = create(:user_preference, user: user, preferenceable: private_group,
                key: 'notification.group_welcome.opt_out', value: 'true')

    assert_not user.pref_group_opt_out?(resource_group, 'notification.announcement')
    assert_not user.pref_group_opt_out?(resource_group, 'notification.post')
    assert_not user.pref_group_opt_out?(resource_group, 'notification.group_welcome')

    assert user.pref_group_opt_out?(private_group, 'notification.announcement')
    assert user.pref_group_opt_out?(private_group, 'notification.post')
    assert user.pref_group_opt_out?(private_group, 'notification.group_welcome')

    p1.delete
    p2.delete
    p3.delete

    create(:user_preference, user: user, preferenceable: resource_group,
           key: 'notification.announcement.opt_out', value: 'true')
    create(:user_preference, user: user, preferenceable: resource_group,
           key: 'notification.post.opt_out', value: 'true')
    create(:user_preference, user: user, preferenceable: resource_group,
           key: 'notification.group_welcome.opt_out', value: 'true')

    assert user.pref_group_opt_out?(resource_group, 'notification.announcement')
    assert user.pref_group_opt_out?(resource_group, 'notification.post')
    assert user.pref_group_opt_out?(resource_group, 'notification.group_welcome')

    assert user.pref_group_opt_out?(private_group, 'notification.announcement')
    assert user.pref_group_opt_out?(private_group, 'notification.post')
    assert user.pref_group_opt_out?(private_group, 'notification.group_welcome')

  end

  test 'user preferences for non-preferenceable context' do
    user = create(:user)
    assert_not user.pref_email_opt_out?('notification.follower')
    p = create(:user_preference, user: user, preferenceable: nil, key: 'notification.follower.opt_out', value: 'false')
    assert_not user.pref_email_opt_out?('notification.follower')
    p.value = 'true'
    p.save!
    assert user.pref_email_opt_out?('notification.follower')
  end

  test "should not count blacklisted email as mailable" do
    user = create(:user, email: 'blacklist@example.com')
    email = create(:email, user: user, email: 'blacklist@example.com', confirmed: true, bounced_at: DateTime.now)
    # despite user.email existing, there is no mailable email because it has been
    # blacklisted in emails table
    assert_nil user.mailable_email

    user = create(:user, email: nil)
    email = create(:email, user: user, email: 'blacklist@example.com', confirmed: true, bounced_at: DateTime.now)

    assert_nil user.mailable_email

    user = create(:user, email: nil)
    email = create(:email, user: user, email: 'blacklist@example.com', confirmed: true, bounced_at: DateTime.now)
    email = create(:email, user: user, email: 'nonblacklist@example.com', confirmed: true)

    assert user.mailable_email, 'nonblacklist@example.com'
  end

  def streaming_setup
    notification_generator_mock = mock ()
    NotificationGeneratorJob.stubs(:set).returns notification_generator_mock
    mock.stubs :perform_later

    @org = create(:organization)
    @fc = create(:channel, autopull_content: true, label: 'l1', organization: @org)
    @nfc = create(:channel, autopull_content: true, label: 'l2', organization: @org)

    @u, @f, @nf = create_list(:user, 3, organization: @org)
    create(:follow, user: @u, followable: @f)
    create(:follow, user: @u, followable: @fc)

    @inf1 = create(:user, handle: 'influencer1', organization: @org)

    create(:follow, user: @inf1, followable: @fc)
  end

  test 'top live video stream' do
    u = create(:user)
    vs1 = create(:wowza_video_stream, creator: u, status: 'live')
    vs2 = create(:wowza_video_stream, creator: u, status: 'live')
    another_user_vs = create(:wowza_video_stream, creator: create(:user), status: 'live')
    vs_past = create(:wowza_video_stream, status: 'past', creator: u)
    vs_upcoming = create(:wowza_video_stream, status: 'upcoming', creator: u)
    assert_equal vs2, u.top_live_video_stream
  end

  test 'my video streams should work' do
    streaming_setup

    # one live and one upcoming event in fc
    fc_live = create(:wowza_video_stream, status: 'live', name: '#followingChannel live stream', start_time: Time.now.utc, creator: @inf1)
    fc_upcoming = create(:wowza_video_stream, status: 'upcoming', name: '#followingChannel upcoming stream', start_time: Time.now.utc + 12.hours, creator: @inf1)

    # live event in g created by the user, should be excluded from recommented streams, shown in my streams
    live_self = create(:wowza_video_stream, status: 'live', start_time: Time.now.utc, creator: @u)
    upcoming_self = create(:wowza_video_stream, status: 'upcoming', start_time: Time.now.utc + 1.hour, creator: @u)
    # past self, should be excluded from my streams
    past_self_1 = create(:wowza_video_stream, status: 'past', start_time: Time.now.utc - 1.day, creator: @u)
    past_self_2 = create(:wowza_video_stream, status: 'past', start_time: Time.now.utc - 2.day, creator: @u)
    past_self_wowza_1 = create(:wowza_video_stream, status: 'past', start_time: Time.now.utc - 1.day, creator: @u)
    past_self_wowza_2 = create(:wowza_video_stream, status: 'past', start_time: Time.now.utc - (1.5).day, creator: @u)
    past_self_wowza_1_recording = create(:recording, transcoding_status: 'available', video_stream: past_self_wowza_1)
    past_self_wowza_2_recording = create(:recording, transcoding_status: 'available', video_stream: past_self_wowza_2)

    # was supposed to start 15 minutes ago
    upcoming_late = create(:wowza_video_stream, status: 'upcoming', start_time: Time.now.utc - 15.minutes, creator: @u)
    live_early_mine = create(:wowza_video_stream, status: 'live', start_time: Time.now.utc + 2.hours, creator: @u)

    my_video_streams = @u.my_video_streams_visible_to(viewer: @u)

    assert_equal [live_self, live_early_mine, upcoming_late, upcoming_self, past_self_wowza_1, past_self_wowza_2], my_video_streams

  end

  test 'adds @ to user handle' do

    user = create(:user, handle: 'blueman1')
    assert_equal '@blueman1', user.handle

    user = create(:user, handle: '@blueman123')
    assert_equal '@blueman123', user.handle

  end

  test 'assigns handle automatically when user is saved' do
    user1 = create(:user, first_name: 'John', last_name: 'Doe', handle: nil)
    assert_equal '@johndoe', user1.handle

    user2 = create(:user, first_name: 'Adélaïde', last_name: 'Éléonore-Françoise', handle: nil)
    assert_equal '@adelaideeleonorefrancoise', user2.handle
  end

  test 'do not change handle if handle already exists' do
    user1 = create(:user, first_name: 'John', last_name: 'Doe', handle: '@iamjohn')
    assert_equal '@iamjohn', user1.handle
  end

  test 'do not assign random handle if first name and last name and email is blank' do
    user1 = create(:user, first_name: '', last_name: 'Doe', handle: nil)
    assert_equal '@doe', user1.handle

    user2 = create(:user, first_name: 'John', last_name: '', handle: nil)
    assert_equal '@john', user2.handle

    user3 = create(:user, first_name: nil, last_name: nil, email: nil, handle: nil)
    assert_nil user3.handle

    user3.first_name = 'Adélaïde'
    user3.last_name = 'Éléonore-Françoise'
    user3.handle = nil
    user3.save
    assert_equal '@adelaideeleonorefrancoise', user3.handle
  end

  test 'user handle variations' do
    user = create(:user)
    variations = user.get_variations('john','doe')
    assert_match '@johndoe', variations[0]
    assert_match '@john_doe', variations[1]
    assert_match '@john-doe', variations[2]
    assert_match '@johndoe1', variations[3]
    assert_match '@john_doe1', variations[4]
    assert_match '@john-doe1', variations[5]
  end

  test 'normalize string' do
    user = create(:user)
    user.first_name = 'Adélaïde'
    user.last_name = 'Éléonore-Françoise'
    assert_match 'adelaide', user.normalize_string(user.first_name)
    assert_match 'eleonorefrancoise', user.normalize_string(user.last_name)
  end

  test 'should not allow user with duplicate handle' do
    org = create(:organization)
    user1 = create(:user, organization: org)
    user2 = create(:user, organization: org)

    user2.handle = user1.handle
    assert_not user2.save
  end

  test 'should yield following[_user_id]?' do
    org = create(:organization)
    user, yep, nope = create_list(:user, 3, organization: org)

    create(:follow, user: user, followable: yep)

    assert user.following? yep
    assert user.following_user_id? yep.id

    assert_not user.following? nope
    assert_not user.following_user_id? nope.id
  end

  test 'following?' do
    Post.any_instance.stubs :p_read_by_creator
    org = create(:organization)
    user = create :user, organization: org
    u1 = create :user, organization: org
    u2 = create :user, organization: org
    group = create :group, organization: org
    group.add_member user
    post = create(:post, user: user, group: group)
    create(:follow, user: u1, followable: post)

    assert u1.following? post

    assert_not user.following? u2
  end

  test 'followed users and channels' do
    org = create(:organization)
    u1, u2, u3, u4 = create_list(:user, 4, organization: org)
    create(:follow, user: u1, followable: u2)
    create(:follow, user: u1, followable: u3)

    c1 = create(:channel, :robotics, organization: org)
    c2 = create(:channel, :architecture, organization: org)
    create(:follow, user: u1, followable: c1)

    assert_same_elements [u2, u3], u1.followed_users
    assert_equal [], u3.followed_users

    assert_same_elements [c1], u1.followed_channels
    assert_equal [], u3.followed_channels
  end

  test "first level path test" do
    paths = Paths.first_level
    user = build(:user, handle: paths.sample)
    assert_not user.save
    assert user.errors.full_messages.to_sentence.include?('Handle unavailable')
  end

  test 'onboarding_candidates' do
    org = create(:organization)
    Role.create_master_roles(org)
    *_, u_nononb, u_onb, u_onb_following, user = create_list(:user, 4, organization: org)
    [u_onb, u_onb_following].each do |u| u.make_sme end

    c_p_onb = create(:channel, :good_music, :private, organization: org, visible_during_onboarding: false)

    c_onb = create(:channel, :architecture, organization: org)

    result = user.onboarding_candidates
    assert_same_elements [c_onb], result[:channels]
    assert_same_elements [u_onb, u_onb_following], result[:influencers]

    # following two things; no onboarding
    create(:follow, followable: u_onb_following, user: user)
    create(:follow, followable: c_onb, user: user)
    assert_nil user.onboarding_candidates
  end

  test '#reset_onboarding!' do
    user = create(:user, is_complete: true)
    user.reset_onboarding!
    refute user.reload.is_complete
  end

  test 'cannot force the user to follow more than available' do
    skip
    # create_default_org
    Organization.any_instance.unstub :create_general_channel
    organization = create(:organization)
    client = create(:client, organization: organization)
    user1 = create(:user, organization_id: organization.id, organization_role: 'member', organization: organization)
    user2 = create(:user, organization_id: organization.id, organization_role: 'member', organization: organization)
    #channel = create(:channel, :architecture, organization: organization, client: client, visible_during_onboarding: true)
    #create(:follow, followable: channel, user: user1)

    assert_equal true, user1.following?(organization.general_channel)
    result = user1.onboarding_candidates
    assert_equal nil, result

    Follow.where(user:user2, followable: organization.general_channel).delete_all

    assert_equal false, user2.following?(organization.general_channel)
    result = user2.onboarding_candidates
    assert_equal [organization.general_channel], result[:channels]
  end

  test 'get list of channels to subscribe to' do
    org = create(:organization)
    u = create(:user, organization: org)

    # four users, out of which u is following f1 and f2
    fs = create_list(:user, 4, organization: org)
    fs.first(2).each {|f| create(:follow, followable: f, user: u)}

    # two channels, user is following one
    chs = create_list(:channel, 2, organization: org)
    create(:follow, followable: chs.first, user: u)
    assert_same_elements [u.private_pubnub_channel_name, Settings.pubnub.global_channel_name, fs[0].public_pubnub_channel_name, fs[1].public_pubnub_channel_name, chs.first.public_pubnub_channel_name, u.organization.public_pubnub_channel_name], u.pubnub_channels_to_subscribe_to
  end

  test 'pubnub channel' do
    u = create(:user)
    public_channel_name = private_channel_name = nil
    assert_difference ->{PubnubChannel.count}, 2 do
      public_channel_name = u.public_pubnub_channel_name
      assert_not_nil public_channel_name
      private_channel_name = u.private_pubnub_channel_name
      assert_not_nil private_channel_name
    end

    # should use existing
    u.reload
    assert_no_difference ->{PubnubChannel.count} do
      assert_equal public_channel_name, u.public_pubnub_channel_name
      assert_equal private_channel_name, u.private_pubnub_channel_name
    end
  end

  class UserFollowNotificationTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false
    test 'should notify change in follow unfollow list to pubnub' do
      org = create(:organization)
      OnFollowEnqueueJob.stubs(:perform_later)
      u = create(:user, organization: org)
      follower = create(:user, organization: org)
      PubnubNotifyChangeJob.expects(:perform_later).with("subscribe", follower, u.public_pubnub_channel_name).returns(nil).once
      f = create(:follow, followable: u, user: follower)
      PubnubNotifyChangeJob.expects(:perform_later).with("unsubscribe", follower, u.public_pubnub_channel_name).returns(nil).once
      f.destroy
    end
  end

  class UserIndexingTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false
    include ActiveJob::TestHelper

    test 'indexes and reindexes a user after create' do
      assert_equal 'edcast_users_test', User.searchkick_index.name
      assert_enqueued_with(job: IndexJob, queue: 'reindex_urgent') do
        create(:user)
      end
    end

    test 'indexes and reindexes a user after update' do
      assert_equal 'edcast_users_test', User.searchkick_index.name
      user = create(:user)
      assert_enqueued_with(job: IndexJob, queue: 'reindex_urgent') do
        user.update(first_name: 'updating first name')
      end
    end

    test 'should reindex follower on follow' do
      org = create(:organization)
      u, f = create_list(:user, 2, organization: org)
      assert_enqueued_with(job: IndexJob, queue: 'reindex_urgent') do
        flw = create(:follow, followable: u, user: f)
      end
    end

    test 'should reindex follower on unfollow' do
      org = create(:organization)
      u, f = create_list(:user, 2, organization: org)
      flw = create(:follow, followable: u, user: f)
      assert_enqueued_with(job: IndexJob, queue: 'reindex_urgent') do
        flw.destroy
      end
    end

    test 'should set org role for an user' do
      u = create(:user, organization_role: 'member')

      assert_equal 'member', u.organization_role
      u.set_org_role 'admin'
      assert_equal 'admin', u.organization_role

      u.set_org_role 'member'
      assert_equal 'member', u.organization_role
    end

    test 'should not set invalid role for org user' do
      u = create(:user, organization_role: 'member')

      assert_equal 'member', u.organization_role
      u.set_org_role 'admin2'
      u.reload
      assert_equal 'member', u.organization_role
    end
  end

  test "should return paged followers" do
    user1 = create :user
    users = create_list(:user, 5, organization: user1.organization)
    users.each do |user|
      create :follow, followable: user1, user: user
    end
    user1.reload

    assert_equal 5, user1.followed_by_users.count
    assert_equal 2, user1.followed_by_users(offset: 3).count
  end

  test 'should autofollow channels on create / update' do
    stub_request(:post, "http://localhost:9200/_bulk")
    user1 = create(:user, is_complete: false)
    AutoFollowChannelsJob.expects(:perform_later).never
    user1.run_callbacks :commit

    AutoFollowChannelsJob.expects(:perform_later).with(user_id: user1.id).returns(nil).once
    user1.update!(is_complete: true)
    user1.run_callbacks :commit
    user1.reload

    user2 = create(:user, is_complete: true)
    AutoFollowChannelsJob.expects(:perform_later).with(user_id: user2.id).returns(nil).once
    user2.run_callbacks :commit

    AutoFollowChannelsJob.expects(:perform_later).never
    user2.update!(first_name: 'mike') #already following
    user2.run_callbacks :commit

    new_org = create(:organization)
    user_new_org = create(:user, organization: new_org, is_complete: true)
    AutoFollowChannelsJob.expects(:perform_later).with(user_id: user_new_org.id).returns(nil).once
    user_new_org.run_callbacks :commit
  end

  test 'should run avatar image job update of picture url' do
    user1 = create(:user)
    picture_url = 'https://edcast-qa-paperclip-store.s3.amazonaws.com/organization_15/temp_uploads/79f7befe-3f9f-46d7-a1ed-b4cb119e1a87/download.png'

    AvatarImageJob.expects(:perform_later).with(user_id: user1.id, picture_url: user1.picture_url).returns(nil).once
     user1.send :perform_avatar_image_job
  end

  test 'should not call perform avatar job on update of firstname' do
    user1 = create(:user)
    picture_url = 'https://edcast-qa-paperclip-store.s3.amazonaws.com/organization_15/temp_uploads/79f7befe-3f9f-46d7-a1ed-b4cb119e1a87/download.png'
      User.any_instance().expects(:perform_avatar_image_job).never
      user1.update!(first_name: "ABC")
      user1.run_callbacks :commit
  end


  test 'get newsletter preference for a user' do
    user1 = create(:user)
    assert_equal false, user1.pref_newsletter_opt_in?

    pref = UserPreference.create(user: user1, key: 'edcast_newsletter.opt_in', value: true)
    assert_equal true, user1.pref_newsletter_opt_in?

    pref.value = false
    pref.save
    assert_equal false, user1.pref_newsletter_opt_in?
  end

  test 'set newsletters opt_in preference for a user' do
    user1 = create(:user)
    assert_equal false, user1.pref_newsletter_opt_in?

    user1.pref_newsletter_opt_in=(true)
    assert_equal true, user1.pref_newsletter_opt_in?

    user1.pref_newsletter_opt_in=(false)
    assert_equal false, user1.pref_newsletter_opt_in?

    ActiveSupport::Deprecation.silence do
      user1.pref_newsletter_opt_in=('not_a_valid_value')
      assert_equal false, user1.pref_newsletter_opt_in?
    end
  end

  test 'pref get/set bool' do
    u = create(:user)

    assert_not u.pref_get_bool('nonexistent')
    assert u.pref_get_bool('nonexistent', default: true)

    assert_difference ->{ UserPreference.count } do
      u.pref_set_bool('blah', nil) # false
    end
    assert_not u.pref_get_bool('blah', default: true)
    assert_no_difference ->{ UserPreference.count } do
      u.pref_set_bool('blah', '1') # true
    end
    assert u.pref_get_bool('blah', default: false)

    assert create(:user).pref_get_bool('blah', default: true)
  end

  test 'notification prefs' do
    u = create(:user)
    assert u.pref_notification_content
    u.pref_notification_content = false
    assert_not u.pref_notification_content

    assert u.pref_notification_weekly_activity
    u.pref_notification_weekly_activity = false
    assert_not u.pref_notification_weekly_activity

    assert u.pref_notification_activity_on_content
    u.pref_notification_activity_on_content = false
    assert_not u.pref_notification_activity_on_content
  end

  test 'get number of followers for a user' do
    org = create(:organization)
    user1 = create(:user, organization: org)
    users = create_list(:user, 3, organization: org)
    users.each do |user|
      create :follow, followable: user1, user: user
    end
    user1.reload

    assert_equal 3, user1.followers_count
  end

  test 'get number of users following a user' do
    org = create(:organization)
    u1, u2, u3 = create_list(:user, 3, organization: org)

    create(:follow, user: u1, followable: u2)
    create(:follow, user: u1, followable: u3)
    create(:follow, user: u2, followable: u1)

    assert_equal 2, u1.following_count
    assert_equal 1, u2.following_count
    assert_equal 0, u3.following_count
  end

  test 'get list of users following a user' do
    org = create(:organization)
    u1, u2, u3, u4, u5 = create_list(:user, 5, organization: org)
    u4.update({is_suspended: true})
    u5.update({is_complete: false})

    create(:follow, user: u1, followable: u2)
    create(:follow, user: u1, followable: u3)
    create(:follow, user: u1, followable: u4) # follow suspended user
    create(:follow, user: u1, followable: u5) # follow incomplete user

    assert_equal 2, u1.following.count
    assert_includes u1.following, u2
    assert_not_includes u1.following, u4
    assert_not_includes u1.following, u5
  end

  test 'writable_channels' do
    org = create(:organization)
    u = create(:user, organization: org)
    c0 = create(:channel, organization: org)
    c1 = create(:channel, :robotics, organization: org)
    c2 = create(:channel, :architecture, organization: org)
    c1.authors << u
    c2.authors << u
    create(:channel, :health, organization: org)
    assert_same_elements [c1, c2], u.writable_channels
  end

  test "should allow two users with same email" do
    org1, org2 = create_list(:organization, 2)
    u = build :user, email: '1@1.com', organization_id: org1.id
    assert u.save
    u = build :user, email: '1@1.com', organization_id: org2.id
    assert u.save
  end

  test 'clone for org, parent user id' do
    org = create(:organization)
    u = create(:user)
    newu = nil
    assert_difference ->{User.count} do
      newu = u.clone_for_org(org, 'member')
    end
    assert_equal u, newu.parent_user
  end

  test 'clone for org' do
    org = create(:organization)
    u = create(:user)
    ip1 = create(:identity_provider, type: 'FacebookProvider', user: u, auth: true)
    ip2 = create(:identity_provider, type: 'LinkedinProvider', user: u, auth: true)

    newu = nil
    assert_difference ->{User.count} do
      assert_difference ->{IdentityProvider.count}, 2 do
        newu = u.clone_for_org(org, 'member')
      end
    end
    assert_equal newu.name, u.name
    assert_not_nil newu.confirmed_at
    assert newu.password_reset_required
    assert_equal 2, IdentityProvider.where(user: newu).count
  end

  test 'clone for org, suppress tours' do
    org = create(:organization)
    u = create(:user, configs: ActiveSupport::JSON.encode({"user-feeds-tour":"1",
                                                           "something-else":"1",
                                                           "do-not-include":"1",
                                                           "chrome-extension-tour":"0",
                                                           "card-tour":"1"}))

    assert_difference ->{User.count} do
        newu = u.clone_for_org(org, 'member')
        expected = {"user-feeds-tour":"1","chrome-extension-tour":"0","card-tour":"1"}
        assert_equal ActiveSupport::JSON.encode(expected), newu.configs
    end
  end

  test 'courses' do
    # org1 is linked to cli1. cli2 isn't linked to any org.
    # user is part of groups g1 and g2. Only g1 belongs to cli1, and hence only g1 should be visible inside org1

    #as per the clisent cleanup every client should have org and every course should also have org
    #so updating assertion
    org = create(:organization)
    admin = create(:user, organization: org, email: 'admin@course-master.com')
    cli1, cli2 = create_list(:client, 2, user: admin, organization: org)
    g1, g2, g3, g4 = create_list(:group, 4)
    g1.update_attributes(client: cli1, organization: cli1.organization)
    g2.update_attributes(client: cli2)
    g3.update_attributes(client: cli1)

    # closed group
    g4.update_attributes(client: cli1, close_access: true)

    create_default_org
    default_org_user = create(:user, organization: Organization.default_org)
    cloned_user = default_org_user.clone_for_org(org, 'member')
    [g1,g2,g4].each {|g| g.add_member(default_org_user)}

    assert_same_elements [g1], default_org_user.courses
    assert_same_elements [g1], cloned_user.courses
  end

  test 'teams_grouped' do
    user = create(:user)
    org = user.organization
    team1, team2, team3 = teams = create_list(:team, 3, organization: org)

    # some irrelevant users
    teams.each do |team|
      team.admins << create(:user, organization: org)
      team.members << create(:user, organization: org)
    end

    team1.admins << user
    team2.members << user
    team3.members << user

    grouped = user.teams_grouped
    assert_same_elements %w[admin member], grouped.keys
    assert_equal [team1], grouped[:admin]
    assert_same_elements [team2, team3], grouped[:member]
  end

  test 'user interests' do
    i = create(:interest, name: 'Ruby on Rails')
    u = create(:user)
    Tagging.create!(taggable: u, tag: i)
    assert_equal [i], u.interests
  end

  test 'forum user' do
    create_default_org
    org = create(:organization)
    u = create(:user, organization: Organization.default_org)
    assert_equal u.id, u.forum_user.id

    org_user = create(:user, organization: org, organization_role: 'member', parent_user: u)
    assert_equal u.id, org_user.forum_user.id
  end

  test 'inactive user should not be able to login' do
    user1 = create(:user, is_active: false)
    assert_not user1.active_for_authentication?

    user2 = create(:user)
    assert user2.active_for_authentication?
  end

  test 'locked user should not be able to login' do 
    user1 = create(:user,locked_at: Time.now)
    refute user1.active_for_authentication?

    user2 = create(:user)
    assert user2.active_for_authentication?
  end

  test 'locked and suspended user should not be able to login' do 
    user1 = create(:user,locked_at: Time.now,is_suspended: true, status: 'suspended')
    refute user1.active_for_authentication?

    user2 = create(:user)
    assert user2.active_for_authentication?   
  end

  test "#accessible_video_streams" do
    org = create(:organization)
    u1 = create :user, organization: org
    u2 = create :user, organization: org

    stream1 = create(:wowza_video_stream, status: 'live', start_time: Time.now.utc, creator: u1)
    stream2 = create(:wowza_video_stream, status: 'live', start_time: Time.now.utc , creator: u1)
    assert_includes u1.accessible_video_streams(u2), stream1

    stream3 = create(:wowza_video_stream, status: 'live', creator: create(:user))
    assert_not_includes u1.accessible_video_streams(u2), stream3
  end

  test 'accessible video streams should include draft streams' do
    org = create(:organization)
    u1 = create :user, organization: org
    u2 = create :user, organization: org

    draft_stream = create(:wowza_video_stream, state: 'draft', status: 'live', start_time: Time.now.utc, creator: u1)
    published_stream = create(:wowza_video_stream, state: 'published', status: 'live', start_time: Time.now.utc , creator: u1)

    accessible_streams = u1.accessible_video_streams(u1)
    assert_same_elements [draft_stream, published_stream].map(&:id), accessible_streams.map(&:id)

    # For some other user
    accessible_streams = u1.accessible_video_streams(u2)
    assert_same_elements [published_stream].map(&:id), accessible_streams.map(&:id)
  end

  test 'active for auth' do
    u1 = create(:user)
    assert u1.active_for_authentication?

    u1.update_columns(is_active: false)
    refute u1.active_for_authentication?

    u2 = create(:user)
    assert u2.active_for_authentication?

    u2.update_columns(is_suspended: true)
    refute u2.active_for_authentication?
  end

  test '#active_for_authentication? not active for auth if user suspended' do
    user = create(:user, status: User::SUSPENDED_STATUS)
    refute user.active_for_authentication?
  end

  test 'should delete associated objects when user suspended' do
    Follow.any_instance.stubs(:record_user_follow_action)
    Vote.any_instance.stubs(:record_card_unlike_event)
    Comment.any_instance.stubs(:record_card_uncomment_event)

    org = create(:organization)
    user, another_user, one_more_user = create_list(:user, 3, organization: org)
    ch = create(:channel, organization: org)
    ch.authors << user

    card = create(:card, author_id: nil, organization: org)
    create(:vote, votable: card, voter: user)

    comment = create(:comment, commentable: card, user: user)
    create(:vote, votable: comment, voter: user)

    create(:follow, followable: another_user, user: user)
    create(:follow, followable: user, user: one_more_user)

    User.any_instance.expects(:reindex_async).returns(nil).once

    assert_differences [[->{Comment.count}, -1], [->{Vote.count}, -2]] do
      user.suspend!
    end

    refute Follow.where(user: user).any?
    refute Follow.where(followable: user).any?
  end

  test 'suspending a user should call metrics recorder' do
    @org = create(:organization)
    user, actor = create_list(:user, 2, organization: @org)
    stub_time = Time.now
    metadata = { user_id: actor.id }
    RequestStore.stubs(:read).with(:request_metadata).returns(metadata)
    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        recorder: "Analytics::UserSuspensionMetricsRecorder",
        org: Analytics::MetricsRecorder.org_attributes(@org),
        timestamp: stub_time.to_i,
        event: "user_suspended",
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        suspended_user: Analytics::MetricsRecorder.user_attributes(user).merge(
          "is_suspended" => true,
          "is_active" => false,
          "status" => User::SUSPENDED_STATUS,
          "updated_at" => stub_time.to_i
        ),
        additional_data: metadata
      )
    )

    Timecop.freeze(stub_time) do
      user.suspend!
    end
  end

  test 'unsuspending a user should call metrics recorder' do
    @org = create(:organization)
    user, actor = create_list(:user, 2, organization: @org)
    stub_time = Time.now
    metadata = { user_id: actor.id }
    user.suspend!
    user.reload
    RequestStore.stubs(:read).with(:request_metadata).returns(metadata)
    User.any_instance.expects(:reindex_async).returns(nil).once
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::UserSuspensionMetricsRecorder",
      org: Analytics::MetricsRecorder.org_attributes(@org),
      timestamp: stub_time.to_i,
      event: "user_unsuspended",
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      suspended_user: Analytics::MetricsRecorder.user_attributes(user).merge(
        "status" => "active",
        "is_active" => true,
        "is_suspended" => false
      ),
      additional_data: metadata
    )
    Timecop.freeze(stub_time) do
      user.unsuspend!
    end

  end

  test 'skips user confirmation in sso sign_up for external app' do
    org = create(:organization)
    auth_hash = {
      provider: 'facebook',
      uid: '1234567',
      info: {
          nickname: 'jbloggs',
          email: 'joe@bloggs.com',
          name: 'Joe Bloggs',
          first_name: 'Joe',
          last_name: 'Bloggs',
          image: 'http://graph.facebook.com/1234567/picture?type=square',
          urls: { Facebook: 'http://www.facebook.com/jbloggs' },
          location: 'Palo Alto, California',
          verified: true
      },
      organization_id: org.id
    }

    # All sso flow visit this flow. so should be skipped by default
    # bypass confirmation if savannah app
    user = User.get_or_create_from_social(auth_hash: auth_hash)
    assert_not_nil user.confirmed_at
  end

  test "#integration_connected" do
    u = create(:user)
    integrations = create_list :integration, 2
    u.integrations << integrations[0]
    assert_not_nil u.integration_connected(integrations[0])
    assert_nil u.integration_connected(integrations[1])
  end

  test "#fields_for" do
    user1 = create(:user)
    fields1 = [{name: "coursera_uid", label: "Coursera UID", placeholder: "Coursera public ID"}, {name: "coursera_username", label: "Coursera user name"}]
    fields2 = [{name: "udemy_id", label: "Udemy ID"}]
    integration1 = create(:integration, fields: fields1.to_json)
    integration2 = create(:integration, fields: fields2.to_json)
    integration3 = create(:integration)
    ui1 = create :users_integration, user: user1, integration_id: integration1.id, field_values: {"coursera_uid" => "test id", "coursera_username" => "test_name"}.to_json

    assert_not_nil user1.fields_for(integration1)
    assert_equal user1.fields_for(integration1), [{"name"=>"coursera_uid", "label"=>"Coursera UID", "placeholder"=>"Coursera public ID", "value"=>"test id"}, {"name"=>"coursera_username", "label"=>"Coursera user name", "value"=>"test_name"}]
    assert_not_nil user1.fields_for(integration2)
    assert_equal user1.fields_for(integration2), [{"name"=>"udemy_id", "label"=>"Udemy ID"}]
    assert_nil user1.fields_for(integration3)
  end

  test "#my_external_courses" do
    user1 = create(:user)
    courses = create_list(:users_external_course, 2, user: user1)
    assert_equal user1.load_external_courses.length, 0

    user1.integrations << Integration.all
    assert_equal user1.load_external_courses.length, 2
  end

  test "return profile preferences" do
    user = create(:user)
    refute user.pref_profile("invalid")
    assert user.pref_profile("show_education")

    user.user_preferences.create(key: "show_competencies", value: "true")
    assert_equal user.pref_profile("show_competencies"), true
  end

  test 'is edcast dot com user' do
    u1 = create(:user, email: "a@edcast.com")
    u2 = create(:user, email: "a@a.com")
    assert u1.is_edcast_dot_com_user?
    refute u2.is_edcast_dot_com_user?
  end

  test 'should add user to everyone group' do
    EdcastActiveJob.unstub :perform_later
    Users::AfterCommitOnCreateJob.unstub :perform_later
    Users::AddUserToEveryoneGroupJob.unstub :perform_later

    Organization.any_instance.unstub :create_org_level_team
    User.any_instance.unstub :add_user_to_everyone_team

    org = create(:organization)
    user = create(:user, organization: org)

    User.any_instance.expects(:reindex_async_urgent).returns(nil)

    org.run_callbacks(:commit)
    user.run_callbacks(:commit)
    org.reload
    everyonegrp = org.teams.where(is_everyone_team: true).first

    assert_includes everyonegrp.users.collect(&:id), user.id
  end

  test 'should add user to groups whilelisted to users domain' do
    AddUserToWhitelistedTeamsJob.unstub :perform_later
    org = create(:organization)
    team = create :team, organization: org, name: "foo", domains: "edcast.com"

    AddUserToWhitelistedTeamsJob.expects(:perform_later).once
    user = create(:user, organization: org, email: "test_user@edcast.com")
    user.send(:add_user_to_whitelisted_teams)
  end

  test 'should create a user onboarding record when created' do
    User.any_instance.unstub :generate_user_onboarding
    user = create(:user, :with_onboarding_complete, is_complete: true)
    assert_equal 3, user.user_onboarding.current_step
    assert_equal "completed", user.user_onboarding.status #default to completed in tests so that people can test.
  end

  test 'new assignments count' do
    org = create(:organization)
    user = create(:user, organization: org)
    card = create(:card, author: create(:user, organization: org))
    assignment = create(:assignment, assignee: user, assignable: card, state: 'assigned')
    assert_equal 1, user.new_assignments.count

    assignment.start!
    assignment.reload
    assert_equal 0, user.new_assignments.count

    card2 = create(:card, author_id: nil, organization: org)
    assignment2 = create(:assignment, assignee: user, assignable: card2, created_at: Time.now.utc - 2.hour)
    assert_equal 1, user.new_assignments.count

    AssignmentNoticeStatus.create!(user: user, notice_dismissed_at: Time.now.utc)
    assignment2.start!
    assignment2.reload
    assert_equal 0, user.new_assignments.count
  end

  #follow_all_team_users
  test "should hit job of team users follow" do
    EdcastActiveJob.unstub :perform_later
    FollowTeamUsersJob.unstub :perform_later

    org = create(:organization)
    team = create(:team, organization: org)
    user = create(:user, organization: org)
    FollowTeamUsersJob.any_instance.expects(:perform).once
    user.follow_all_team_users(team, {})
  end

  #show_follow_all_for_team
  test "should return false(true) if all users are followed(not followed)" do
    org = create(:organization)
    team = create(:team, organization: org)
    user = create(:user, organization: org)
    team.add_admin user
    member1, member2 = create_list(:user, 2, organization: org)
    team.add_member member1
    team.add_member member2

    assert user.show_follow_all_for_team(team)

    create(:follow, user: user, followable: member1)
    create(:follow, user: user, followable: member2)
    refute user.show_follow_all_for_team(team)
  end

  #team_user_ids
  test "it should returns id of team users except me" do
    org = create(:organization)
    team = create(:team, organization: org)
    user = create(:user, organization: org)
    team.add_admin user
    member1, member2 = create_list(:user, 2, organization: org)
    team.add_member member1
    team.add_member member2

    assert_same_ids user.team_user_ids(team), [member1.id, member2.id]

  end

  test "completion state for" do
    org = create(:organization)
    user = create(:user, organization: org)
    cards = create_list(:card, 2, author_id: nil, organization: org)
    create(:user_content_completion, :completed, completable: cards.first, user: user)
    assert_equal "completed", user.completion_state_for("Card", cards.first.id)
    assert_nil user.completion_state_for("Card", cards.last.id)
    assert_nil create(:user).completion_state_for("Card", cards.first.id)
  end


  test 'cover images v2' do
    user = create(:user)
    assert_equal [nil], user.fetch_coverimage_urls_v2.values.uniq

    pic = Rails.root.join('test/fixtures/images/logo.png')
    user.coverimage = pic.open
    user.save

    coverimages = user.fetch_coverimage_urls_v2
    assert_not_nil coverimages["profile"]
    assert_not_nil coverimages["banner"]
    assert_not_nil coverimages["medium"]
    assert_not_nil coverimages["large"]
  end

  test 'search_by_name' do
    user = create :user
    new_user = create :user
    result = User.search_by_name(user.first_name)
    assert_includes result, user
    assert_not_includes result, new_user
  end

  test 'should check if user already exists' do
    org = create(:organization)
    other_org = create(:organization)
    refute User.already_exists?(organization: org)

    user = create :user, organization: org
    assert User.already_exists?(organization: org, email: user.email)

    invitation = create(:invitation,
      roles: 'member', recipient_email: 'abc@example.com', invitable: org, sender: create(:user, organization: org)
    )
    assert User.already_exists?(organization: org, email: 'abc@example.com')

    refute User.already_exists?(organization: org, provider: 'facebook', uid: '1')
  end

  test 'should send assessment report' do
    org = create(:organization)
    author = create(:user, organization: org)
    card = create(:card, author: author)
    aws_url = mock()
    mailer = mock()
    mailer.expects(:deliver_later).once

    UserMailer.expects(:send_assessment_report).with(s3_url: aws_url, user_id: author.id, card_title: card.snippet).returns(mailer)
    author.send_assessment_report_email_for(aws_url: aws_url, card_id: card.id)
  end

  class UserEveryoneGroupTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false

    setup do
      EdcastActiveJob.unstub :perform_later
      Users::AddUserToEveryoneGroupJob.unstub :perform_later
      User.any_instance.stubs(:reindex_async).returns(true)
      Channel.any_instance.stubs(:reindex_async).returns(true)
      Channel.any_instance.stubs(:after_followed).returns(nil)
    end

    test 'add user to everyone group after confirmation' do
      Organization.any_instance.unstub :create_org_level_team
      User.any_instance.unstub :update_everyone_group_members
      User.any_instance.unstub :add_user_to_everyone_team

      org = create(:organization)
      team = create(:team, organization: org, is_everyone_team: true)
      user = nil
      assert_no_difference -> {TeamsUser.count} do
        user = create(:user, confirmed_at: nil, organization: org)
      end

      assert_difference -> {TeamsUser.count} do
        user.confirm
      end
    end

    test 'should not add user to everyone group if email doesnt exist' do
      org = create(:organization)
      team = create(:team, organization: org, is_everyone_team: true)
      user = nil
      assert_no_difference -> {TeamsUser.count} do
        user = create(:user, email: nil, confirmed_at: Time.now, organization: org)
      end
    end
  end

  class UserRoleUpdateTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false
    setup do
      EdcastActiveJob.unstub :perform_later
      Users::AddUserToEveryoneGroupJob.unstub :perform_later
    end

    test 'user role is update to org admin should make them leader of everyone team' do
      Organization.any_instance.unstub :create_org_level_team
      User.any_instance.unstub :add_user_to_everyone_team

      org = create(:organization)
      user = create(:user, organization: org, organization_role: 'member')
      User.any_instance.stubs(:reindex_async).returns(true)
      user.set_org_role('admin')
      assert_equal true , org.everyone_team.is_admin?(user)
    end

    test 'user role is demoted to org member should demote them to member of everyone team' do
      Organization.any_instance.unstub :create_org_level_team
      User.any_instance.unstub :add_user_to_everyone_team

      org = create(:organization)
      org.run_callbacks(:commit)
      user = create(:user, organization: org, organization_role: 'admin')
      User.any_instance.stubs(:reindex_async).returns(true)
      user.set_org_role('member')
      assert_equal false , org.everyone_team.is_admin?(user)
      assert_equal true , org.everyone_team.is_user?(user)
    end
  end

  test "#setup_lms_user" do
    EdcastActiveJob.unstub :perform_later
    LmsUserSyncJob.unstub :perform_later
    User.any_instance.unstub :setup_lms_user

    stub_request(:post, "http://localhost:9200/_bulk")

    LmsUserSyncJob.expects(:perform_later).twice
    org = create(:organization)
    user = create(:user, organization: org, organization_role: 'member')
    user.run_callbacks :commit

    user.update(organization_role: 'admin')
    user.run_callbacks :commit
  end

  test '#onboarding_options returns list of onboarding options at an org level' do
    default_steps  = UserOnboarding::DEFAULT_ONBOARDING_STEPS
    optional_steps = UserOnboarding::OPTIONAL_ONBOARDING_STEPS

    org    = create :organization
    user   = create :user, organization: org
    config = create :config, configable_id: org.id, configable_type: 'Organization',
      name: 'onboarding_options', value: default_steps.merge(optional_steps).to_json, data_type: 'json'
    user_profile = create :user_profile, user: user, onboarding_options: default_steps.merge({ add_interests: 2 })

    # 1. Returns list of steps configured at user level
    #   In this case, only `profile_setup` and `add_interests` are in
    user_profile.update(onboarding_options: optional_steps)

    assert_equal default_steps.merge(optional_steps), user.reload.onboarding_options

    # 2. Mandate profile_setup as mandatory option.
    #   In this case, all of the 3 options are in.
    user_profile.delete
    assert_equal default_steps.merge(optional_steps), user.reload.onboarding_options

    # 3. Return org's onboarding options if no options set at user level
    #   In this case, all of the 3 options are in.
    user_profile.delete
    assert_equal config.parsed_value, user.reload.onboarding_options

    # 4. Enforces profile_setup as a mandatory step if no org level steps configured
    config.delete
    assert_equal default_steps, user.reload.onboarding_options
  end

  test '#auth_landing_url returns either login url or user magic link' do
    create_default_ssos
    organization = create :organization
    user = create :user, organization: organization

    # empty all existing ssos
    organization.ssos = []
    organization.reload

    # Use-case-1: User must receive magic link if org has only email-password auth
    organization.ssos = [Sso.find_by(name: 'email')]
    organization.reload
    assert_match /api\/v2\/users\/login_landing/, user.auth_landing_url

    # Use-case-2: User must receive magic link if org has email-password auth alongwith other SSOs
    organization.ssos += [Sso.find_or_create_by(name: 'saml')]
    organization.reload
    assert_match /api\/v2\/users\/login_landing/, user.auth_landing_url

    # Use-case-3: User must receive login URL if org has only SSOs
    organization.ssos = [Sso.find_by(name: 'saml')]
    organization.reload
    assert_match /log_in/, user.auth_landing_url
  end

  test 'should generate random password with all the password rules fulfilled ' do
    password = User.random_password
    assert_match /\A(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?["!#$%&'()*+,-.\/\\:;<=>?@\[\]^_`{|}~])(?=.{8,}).*\z/x, password
  end

  test 'should not pass password validations' do
    email = generate(:email_address)
    organization = create(:organization)

    #Length check
    user = build(:user, email: email, organization: organization,password: 'aA1234@')
    refute user.valid?
    assert_equal "Password is too short (minimum is 8 characters) and Password is invalid", user.full_errors

    #Upper Case Character
    user = build(:user, email: email, organization: organization,password: 'aaa1234@')
    refute user.valid?
    assert_equal "Password is invalid", user.full_errors

    #Lower Case Character
    user = build(:user, email: email, organization: organization,password: 'AAA1234@')
    refute user.valid?
    assert_equal "Password is invalid", user.full_errors

    #Number Character
    user = build(:user, email: email, organization: organization,password: 'Abcdefg@')
    refute user.valid?
    assert_equal "Password is invalid", user.full_errors

    #Symbol Character
    user = build(:user, email: email, organization: organization,password: 'Abcdefg1')
    refute user.valid?
    assert_equal "Password is invalid", user.full_errors

    #Excluded Characters (Spaces)
    user = build(:user, email: email, organization: organization,password: 'Abcdef1 ')
    refute user.valid?
    assert_equal "Password is invalid", user.full_errors

  end

  test 'should pass password validations' do
    email = generate(:email_address)
    organization = create(:organization)

    #correct password
    user = build(:user, email: email, organization: organization,password: 'aA1234@=')
    assert user.valid?

    #check for special characters
    user = build(:user, email: email, organization: organization,password: 'Aaa1234~')
    assert user.valid?

    user = build(:user, email: email, organization: organization,password: 'AAa1234"')
    assert user.valid?

    user = build(:user, email: email, organization: organization,password: 'Abcdefg1[')
    assert user.valid?

    user = build(:user, email: email, organization: organization,password: 'Abcdefg1=')
    assert user.valid?

    user = build(:user, email: email, organization: organization,password: 'Abcdef1.')
    assert user.valid?

    #Random valid cases
    user = build(:user, email: email, organization: organization,password: 'Aswini123#')
    assert user.valid?

    user = build(:user, email: email, organization: organization,password: 'Aswini123/')
    assert user.valid?

    user = build(:user, email: email, organization: organization,password: 'Aswini123\\')
    assert user.valid?

    user = build(:user, email: email, organization: organization,password: 'Aswinias1#')
    assert user.valid?
  end

  test 'should generate jwt token' do
    create_default_org
    org = create(:organization)
    user = create(:user, organization: org)

    web_session_timeout = Faker::Number.between(5, 100)
    create(:config, name: 'web_session_timeout', value: web_session_timeout, data_type: 'integer', configable: org)

    token = user.jwt_token({is_web: true})
    decode_token = JwtAuth.decode(token)

    assert_equal decode_token['host_name'], org.client.try(:name)
    assert_equal decode_token['user_id'], user.id
    assert_equal decode_token['is_org_admin'], user.is_admin_user?
    assert_equal decode_token['is_superadmin'], user.is_admin?
    assert_equal decode_token['organization_id'], org.id
    assert decode_token['timestamp']
    assert decode_token['exp']
  end

  test 'jwt token should not include expire time if config web_session_timeout does not exist' do
    create_default_org
    org = create(:organization)
    user = create(:user, organization: org)

    token = user.jwt_token({is_web: true})
    decode_token = JwtAuth.decode(token)

    assert_not decode_token['exp']
  end

  test 'jwt token should not include expire time if config web_session_timeout equal 0' do
    create_default_org
    org = create(:organization)
    user = create(:user, organization: org)
    create(:config, name: 'web_session_timeout', value: 0, data_type: 'integer', configable: org)

    token = user.jwt_token({is_web: true})
    decode_token = JwtAuth.decode(token)

    assert_not decode_token['exp']
  end

  class UserMetricRecorderTest < ActiveSupport::TestCase
    setup do
      IndexJob.stubs(:perform_later)
    end

    test 'calls MetricsRecorderJob with right args' do
      org = create(:organization)
      user = create(:user, organization: org)
      actor = create(:user, organization: org)
      metadata = { foo: "bar", user_id: actor.id }
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      stub_time

      Analytics::MetricsRecorderJob.unstub(:perform_later)
      Analytics::MetricsRecorderJob.expects(:perform_later).with(
        has_entries(
          recorder: "Analytics::UserMetricsRecorder",
          org: Analytics::MetricsRecorder.org_attributes(org),
          user: Analytics::MetricsRecorder.user_attributes(user),
          actor: Analytics::MetricsRecorder.user_attributes(actor),
          event: "user_created",
          additional_data: metadata,
          timestamp: Time.now.to_i
        )
      )
      user.run_callbacks(:commit)
    end

  end

  test 'assign handle based on email if first name and last name are blank' do
    user1 = create(:user, first_name: nil, last_name: nil, email: 'sample@example.com', handle: nil)
    assert_equal '@sample', user1.handle
  end

  test 'accept invitation based on the invitation id for alias email' do
    organization = create(:organization)
    invitation = create(:invitation, recipient_email: 'abc@example.org', invitable: organization,
      sender: create(:user, organization: organization))
    user = create(:user, email: "abc@example.com", organization: organization)
    user.accept_invitation(invitation_id: invitation.id)

    assert invitation.reload.accepted_at
  end

  test '#channel_ids_followed_via_teams should return ids of channels followed via team only' do
    org = create(:organization)
    user = create(:user, organization: org)
    explicit_user = create(:user, organization: org)
    channel = create(:channel, :architecture, organization_id: org.id)
    team = create(:team, organization: org)
    channel.followed_teams << team
    team.add_member(user)
    channel.add_followers [explicit_user]
    assert_equal user.channel_ids_followed_via_teams, [channel.id]
    assert_same_elements channel.all_followers.pluck(:id), [user.id, explicit_user.id]
  end

  test "#add_to_org_general_channel does nothing if there's no general channel for the org" do
    org = create(:organization)
    assert_equal(org.general_channel, nil)
    user = User.new organization: org, password: "ValidPassword1!", organization_role: "admin"
    user.expects(:organization_role).never # doesn't get far enough to check role
    user.add_to_org_general_channel
  end

  test "#add_to_org_general_channel adds admin user an an author of the channel" do
    User.any_instance.unstub :add_to_org_general_channel
    org = create(:organization)
    channel = create :channel, is_general: true, organization: org
    user = User.new organization: org, password: "ValidPassword1!", organization_role: "admin"
    user.stubs(:organization).returns org
    org.stubs(:general_channel).returns channel
    channel.expects(:add_authors).with([user])
    channel.expects(:add_followers).never
    user.add_to_org_general_channel
  end

  test "#add_to_org_general_channel adds non-admin user an a follower of the channel" do
    User.any_instance.unstub :add_to_org_general_channel
    org = create(:organization)
    channel = create :channel, is_general: true, organization: org
    user = User.new organization: org, password: "ValidPassword1!", organization_role: "member"
    user.stubs(:organization).returns org
    org.stubs(:general_channel).returns channel
    channel.expects(:add_authors).never
    channel.expects(:add_followers).with([user])
    user.add_to_org_general_channel
  end

  ###### CustomLockable ######
  test 'should be valid for authentication with a unlocked user' do
    org = create(:organization)
    Organization.any_instance.stubs(:account_lockout_enabled?).returns(true)
    user = create(:user, organization: org)
    user.lock_account!
    user.unlock_account!
    assert user.valid_for_authentication?{ true }
  end

  test "should verify whether a user is locked or not" do
    org = create(:organization)
    user = create(:user, organization: org)
    refute user.account_locked?
    user.lock_account!
    assert user.account_locked?
  end

  test "should unlock a user by cleaning locked_at, failed_attempts and unlock_token" do
    org = create(:organization)
    user = create(:user, organization: org)
    user.lock_account!
    assert_not_nil user.reload.locked_at

    user.unlock_account!
    assert_nil user.reload.locked_at
    assert_equal 0, user.reload.failed_attempts
  end

  test "new user should not be locked and should have zero failed_attempts" do
    org = create(:organization)
    user = create(:user, organization: org)
    refute user.account_locked?
    assert_equal 0, user.failed_attempts
  end

  test "should increment failed_attempts on un-successful if locking is enabled for org" do
    org = create(:organization)
    Organization.any_instance.stubs(:account_lockout_enabled?).returns(true)
    user = create(:user, organization: org)
    assert_equal 0, user.reload.failed_attempts

    5.times { user.valid_for_authentication?{ false } }
    assert user.account_locked?
    assert_equal 5, user.reload.failed_attempts
  end

  test "should not touch failed_attempts if locking is disabled" do
    org = create(:organization)
    Organization.any_instance.stubs(:account_lockout_enabled?).returns(false)
    user = create(:user, organization: org)

    3.times { user.valid_for_authentication?{ false } }
    assert !user.account_locked?
    assert_equal 0, user.failed_attempts
  end

  test 'calls job when whitelisted columnn is updated' do
    assert Analytics::UserMetricsRecorder.whitelisted_change_attrs.include?(
      "handle"
    )
    org = create(:organization)
    user = create :user, organization: org
    Analytics::MetricsRecorderJob.expects(:perform_later).once
    user.update handle: "@foobar"
  end

  test 'doesnt call job for non-whitelisted column' do
    refute Analytics::UserMetricsRecorder.whitelisted_change_attrs.include?(
      "current_sign_in_ip"
    )
    org = create(:organization)
    user = create :user, organization: org
    Analytics::MetricsRecorderJob.expects(:perform_later).never
    user.update current_sign_in_ip: "@foobar"
  end

  test '#push_user_edited_metric calls recorder job' do
    org = create(:organization)
    user = create :user, organization: org, handle: "@old_handle", default_team_id: 123
    actor = create :user, organization: org
    metadata = { foo: "bar", user_id: actor.id }
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    stub_time
    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub :perform_later
    {
      'handle'                   => "@new_handle",
      'hide_from_leaderboard'    => true,
      'exclude_from_leaderboard' => true,
      'default_team_id'          => 456
    }.each do |col_name, new_val|
      Analytics::MetricsRecorderJob.expects(:perform_later).with(
        recorder: "Analytics::UserMetricsRecorder",
        org: Analytics::MetricsRecorder.org_attributes(org),
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        user: Analytics::MetricsRecorder.user_attributes(user).merge(
          col_name => new_val,
          "updated_at" => Time.now.to_i
        ),
        event: "user_edited",
        changed_column: col_name,
        old_val: user.send(col_name.to_sym)&.to_s,
        new_val: new_val&.to_s,
        additional_data: metadata,
        timestamp: Time.now.to_i
      )
      user.update col_name => new_val
    end
  end

  class AssignHandleTest < ActiveSupport::TestCase

    test 'should properly set handle if it already exist' do
      org = create(:organization)
      existed_user = create(:user, email: 'admin@email.com', organization: org, handle: '@admin')
      user_initialized = User.where(email: 'admin@gmail.com', organization_id: org.id).first_or_initialize do |usr|
        usr.password = User.random_password
        usr.is_complete = false
        usr.password_reset_required = true
        usr.organization_role = 'member'
        usr.skip_confirmation!
      end
      user_initialized.assign_handle
      assert user_initialized.valid?
      assert_equal '@admin1', user_initialized.handle
      assert_equal '@admin', existed_user.handle
      assert user_initialized.save!
    end
  end


  class UserStatusSwitch < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org, status: 'inactive')
    end

    test 'user status should switch to active' do
      assert_equal User::INACTIVE_STATUS, @user.status
      @user.switch_to_active_status
      assert_equal User::ACTIVE_STATUS, @user.status
    end
  end

  test 'should include channel cards in visible card list to member user' do
    org = create :organization
    admin = create :user, organization: org, organization_role: 'admin'
    user = create :user, organization: org
    cards = create_list :card, 3, organization: org, author: admin
    channel = create :channel, organization: org
    channel_card = create :channels_card, card: cards.last, channel: channel
    visible_cards = user.visible_cards(cards)

    assert_equal 3, visible_cards.count
    assert_includes visible_cards, cards.last
  end

  test '#is_sub_admin?' do
    org = create :organization
    Role.create_standard_roles org
    users = create_list(:user, 2,  organization: org)
    member_role = Role.find_by(name: 'member')
    curator_role = Role.find_by(name: 'curator')
    create(:role_permission, role: curator_role, name: Permissions::MANAGE_ANALYTICS)
    member_role.add_user(users.first)
    curator_role.add_user(users.second)

    assert users.second.is_sub_admin?
    refute users.second.is_group_sub_admin?
  end

  test '#is_admin_user? for sub_admin and group_sub_admin' do
    org = create :organization
    Role.create_standard_roles org
    admin = create(:user, organization: org)
    users = create_list(:user, 2,  organization: org)
    team = create :team, organization: org
    admin_role = Role.find_by(name: 'admin')
    member_role = Role.find_by(name: 'member')
    curator_role = Role.find_by(name: 'curator')
    create(:role_permission, role: member_role, name: Permissions::MANAGE_GROUP_ANALYTICS)
    create(:role_permission, role: curator_role, name: Permissions::MANAGE_ANALYTICS)
    create(:role_permission, role: admin_role, name: Permissions::ADMIN_ONLY)
    member_role.add_user(users.first)
    curator_role.add_user(users.second)
    admin_role.add_user(admin)
    team.add_sub_admin(users.first)

    assert users.first.is_admin_user?
    assert users.second.is_admin_user?
    assert admin.is_admin_user?
  end

  test '#pending_teams should return teams only from pending invitations' do
    org = create :organization
    admin = create(:user, organization: org)
    user = create(:user, organization: org)
    teams = create_list(:team, 2, organization: org)
    user.invitations.create(
      sender_id: admin.id,
      invitable_id: teams.first.id,
      invitable_type: 'Team',
      recipient_email: user.email,
      roles: 'member'
    )
    user.invitations.create(
      sender_id: admin.id,
      invitable_id: teams.second.id,
      invitable_type: 'Team',
      recipient_email: user.email,
      roles: 'member',
      accepted_at: Time.current
    )

    assert_equal [teams.first.id], user.pending_teams.pluck(:id)
  end

  test '#all_user_teams shoudl return all(open/invited/and teams user is a part of) accessible teams for the user' do
    org = create :organization
    user = create(:user, organization: org)
    admin = create(:user, organization: org)
    open_teams = create_list(:team, 2, organization: org, is_private: 0)

    open_teams.first.add_member(user)

    private_teams = create_list(:team, 2, is_private: 1, organization: org)
    user.invitations.create(
      sender_id: admin.id,
      invitable_id: private_teams.first.id,
      invitable_type: 'Team',
      recipient_email: user.email,
      roles: 'member'
    )

    assert_same_elements [open_teams.map(&:id), private_teams.first.id].flatten, user.all_user_teams.pluck(:id)
    assert_not_includes user.all_user_teams.pluck(:id), private_teams.second.id
  end

  test 'should create external_profile after create user' do
    TestAfterCommit.enabled = true

    org = create :organization
    user = create(:user, organization: org)

    assert user.external_profile
    assert_not_nil user.external_profile.external_id
    assert_not_nil user.external_profile.uid
    assert_not_nil user.external_profile.organization_id
  end

  test '#has_access_assign_content? for admin' do
    org = create :organization
    Role.create_standard_roles org
    admin = create(:user, organization: org)

    admin_role = Role.find_by(name: 'admin')

    create(:role_permission, role: admin_role, name: Permissions::ADMIN_ONLY)

    admin_role.add_user(admin)

    assert admin.has_access_assign_content?
  end

  test '#has_access_assign_content? for sub_admins' do
    org = create :organization
    Role.create_standard_roles org
    sub_admin = create(:user, organization: org)
    group_sub_admin = create(:user, organization: org)
    team = create :team, organization: org

    member_role = Role.find_by(name: 'member')
    admin_role = Role.find_by(name: 'admin')

    create(:role_permission, role: member_role, name: Permissions::MANAGE_GROUP_ANALYTICS)
    create(:role_permission, role: admin_role, name: Permissions::MANAGE_ANALYTICS)
    admin_role.add_user(sub_admin)
    member_role.add_user(group_sub_admin)
    team.add_to_team([sub_admin, group_sub_admin], as_type: 'sub_admin')

    assert sub_admin.has_access_assign_content?
    assert group_sub_admin.has_access_assign_content?
  end

  test '#has_access_assign_content? for user with ASSIGN_CONTENT permission' do
    org = create :organization
    Role.create_standard_roles org
    users = create_list(:user, 2, organization: org)
    member_role = Role.find_by(name: 'member')
    create(:role_permission, role: member_role, name: Permissions::ASSIGN_CONTENT)
    member_role.add_user(users.first)

    assert users.first.has_access_assign_content?
    refute users.second.has_access_assign_content?
  end

  test '#request_personal_data should call the user mailer method' do
    mailer = mock()
    mailer.expects(:deliver_later).once
    user = create(:user)
    UserMailer.expects(:send_user_data_export_request).with(user.id).returns(mailer)
    user.request_personal_data
  end

  test 'should prepare user profile in background' do
    EdcastActiveJob.unstub :perform_later
    Users::AfterCommitOnCreateJob.expects(:perform_later).once

    org = create(:organization)
    user = create(:user, organization: org)
    user.run_callbacks(:commit)
  end

  test 'add to everyone team should be invoked through background job' do
    org = create(:organization)
    team = create(:team, organization: org, is_everyone_team: true)
    user = create(:user, organization: org)

    User.any_instance.unstub(:add_user_to_everyone_team)
    EdcastActiveJob.unstub :perform_later
    Users::AddUserToEveryoneGroupJob.expects(:perform_later).once

    user.send :add_user_to_everyone_team
  end

  test 'should set status or return error' do
    refute build(:user, status: 'qwe').valid?
    user = build :user
    assert user.valid?
    user.save
    refute user.update(status: 'qwe')
    assert_includes user.errors[:status], 'is not included in the list'
  end

  class UserTranscript < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      @badges = create_list(:badge, 5 , organization: @org)
      api_v2_sign_in @user
    end


    test 'quarter_wise_content should return all types of assignment when admin setting for show mlp of content type is all Type Content' do
      Org_customization_config_value = {"web": {"mlp": {"web/mlp/myAssignments": {"defaultValue": "allContentType"}}}}

      create :config, name: 'OrgCustomizationConfig', configable: @org, data_type: 'json', value: ActiveSupport::JSON.encode(Org_customization_config_value)

      user2 = create(:user, organization: @org)
      smart_cards = create_list(:card, 2, author: user2, organization: @org, card_type: "media")
      pack_cards = create_list(:card, 2, author: user2, organization: @org, card_type: "pack", state: 'published')
      journey_cards = create_list(:card, 2, author: user2, organization: @org,
        card_type: "journey", card_subtype: 'self_paced', state: 'published' )
      course_cards = create_list(:card, 2, author: user2, organization: @org, card_type:"course")

      pathway1, pathway2 = pack_cards.each do |cover|
        CardPackRelation.add(cover_id: cover.id, add_id: create(:card, author: user2).id)
        cover.publish!
      end

      journey_cards.each do |cover|
        JourneyPackRelation.add(cover_id: cover.id, add_id: pathway1.id)
        cover.publish!
      end
      current_year = Time.current.year

      q1_assignment1 = create(:assignment, assignable: smart_cards[0], assignee: @user, state: "completed", created_at: "#{current_year}-01-22 19:00:01")
      q1_assignment2 = create(:assignment, assignable: smart_cards[1], assignee: @user, state: "completed", created_at: "#{current_year}-01-22 19:00:01")
      q2_assignment1 = create(:assignment, assignable: pack_cards[0], assignee: @user, state: "completed", created_at: "#{current_year}-04-22 19:00:01")
      q2_assignment2 = create(:assignment, assignable: pack_cards[1], assignee: @user, state: "completed", created_at: "#{current_year}-04-22 19:00:01")
      q3_assignment1 = create(:assignment, assignable: journey_cards[0], assignee: @user, state: "completed", created_at: "#{current_year}-08-22 19:00:01")
      q3_assignment2 = create(:assignment, assignable: journey_cards[1], assignee: @user, state: "completed", created_at: "#{current_year}-08-22 19:00:01")
      q4_assignment1 = create(:assignment, assignable: course_cards[0], assignee: @user, state: "completed", created_at: "#{current_year}-11-22 19:00:01")
      q4_assignment2 = create(:assignment, assignable: course_cards[1], assignee: @user, state: "completed", created_at: "#{current_year}-11-22 19:00:01")

      stats = @user.quarter_wise_content.flatten.map do |obj|
        obj.dig(:assignable, :cardType)
      end

      refute_includes stats.uniq, %w[poll video_stream project]
    end

    test 'quarter_wise_content should return only pack, journey and course type assignment when admin setting for show mlp of content type is not all Type Content' do
      Org_customization_config_value = {"web": {"mlp": {"web/mlp/myAssignments": {"defaultValue": "notAllContentType"}}}}

      create :config, name: 'OrgCustomizationConfig', configable: @org, data_type: 'json', value: ActiveSupport::JSON.encode(Org_customization_config_value)

      user2 = create(:user, organization: @org)
      smart_cards = create_list(:card, 2, author: user2, organization: @org, card_type: "media")
      pack_cards = create_list(:card, 2, author: user2, organization: @org, card_type: "pack", state: 'published')
      journey_cards = create_list(:card, 2, author: user2, organization: @org,
        card_type: "journey", card_subtype: 'self_paced', state: 'published' )
      course_cards = create_list(:card, 2, author: user2, organization: @org, card_type:"course")

      pathway1, pathway2 = pack_cards.each do |cover|
        CardPackRelation.add(cover_id: cover.id, add_id: create(:card, author: user2).id)
        cover.publish!
      end

      journey_cards.each do |cover|
        JourneyPackRelation.add(cover_id: cover.id, add_id: pathway1.id)
        cover.publish!
      end
      current_year = Time.current.year

      q1_assignment1 = create(:assignment, assignable: smart_cards[0], assignee: @user, state: "completed", created_at: "#{current_year}-01-22 19:00:01")
      q1_assignment2 = create(:assignment, assignable: smart_cards[1], assignee: @user, state: "completed", created_at: "#{current_year}-01-22 19:00:01")
      q2_assignment1 = create(:assignment, assignable: pack_cards[0], assignee: @user, state: "completed", created_at: "#{current_year}-04-22 19:00:01")
      q2_assignment2 = create(:assignment, assignable: pack_cards[1], assignee: @user, state: "completed", created_at: "#{current_year}-04-22 19:00:01")
      q3_assignment1 = create(:assignment, assignable: journey_cards[0], assignee: @user, state: "completed", created_at: "#{current_year}-08-22 19:00:01")
      q3_assignment2 = create(:assignment, assignable: journey_cards[1], assignee: @user, state: "completed", created_at: "#{current_year}-08-22 19:00:01")
      q4_assignment1 = create(:assignment, assignable: course_cards[0], assignee: @user, state: "completed", created_at: "#{current_year}-11-22 19:00:01")
      q4_assignment2 = create(:assignment, assignable: course_cards[1], assignee: @user, state: "completed", created_at: "#{current_year}-11-22 19:00:01")

      stats = @user.quarter_wise_content.flatten.map do |obj|
        obj.dig(:assignable, :cardType)
      end

      refute_includes stats.uniq, %w[media poll video_stream project]
    end

    test 'quarter_wise_content should not return dismissed assignments' do
      user2 = create(:user, organization: @org)
      smart_cards = create_list(:card, 8, author: user2, organization: @org, card_type: "media")

      current_year = Time.current.year

      q1_assignment1 = create(:assignment, assignable: smart_cards[0], assignee: @user, state: "completed", created_at: "#{current_year}-01-22 19:00:01")
      q1_assignment2 = create(:assignment, assignable: smart_cards[1], assignee: @user, state: "dismissed", created_at: "#{current_year}-01-22 19:00:01")
      q2_assignment1 = create(:assignment, assignable: smart_cards[2], assignee: @user, state: "completed", created_at: "#{current_year}-04-22 19:00:01")
      q2_assignment2 = create(:assignment, assignable: smart_cards[3], assignee: @user, state: "dismissed", created_at: "#{current_year}-04-22 19:00:01")
      q3_assignment1 = create(:assignment, assignable: smart_cards[4], assignee: @user, state: "completed", created_at: "#{current_year}-08-22 19:00:01")
      q3_assignment2 = create(:assignment, assignable: smart_cards[5], assignee: @user, state: "dismissed", created_at: "#{current_year}-08-22 19:00:01")
      q4_assignment1 = create(:assignment, assignable: smart_cards[6], assignee: @user, state: "completed", created_at: "#{current_year}-11-22 19:00:01")
      q4_assignment2 = create(:assignment, assignable: smart_cards[7], assignee: @user, state: "dismissed", created_at: "#{current_year}-11-22 19:00:01")

      stats = @user.quarter_wise_content.flatten.map { |obj| obj[:state]}.uniq
      refute_includes stats, 'dismissed'
    end

    test 'get_earned_bagdes' do
      card_badge = create(:card_badge, organization: @org)
      card = create(:card, author_id: @user.id, organization: @org, title: 'this is title', message: 'this is message')
      card_badging = create(:card_badging, badge_id: card_badge.id, badgeable_id: card.id)
      user_card_badge = create(:user_badge, user_id: @user.id, badging_id: card_badging.id )

      clc = create :clc, entity_id: @org.id, entity_type: "Organization", name: "Test_CLC"
      clc_badge = create(:clc_badge, organization_id: @org.id)
      clc_badging = create(:clc_badging, clc_badge: clc_badge, clc: clc)
      user_clc_badge = create(:user_badge, user_id: @user.id, badging_id: clc_badging.id )

      assert_equal 2 , @user.earned_bagdes.size
      assert_equal [ user_card_badge.id, user_clc_badge.id] , @user.earned_bagdes.map(&:id)
    end

    test 'user_stats' do
      user2 = create(:user, organization: @org)
      published_cards = create_list(:card, 4, author_id: @user.id, organization: @org, state: 'published' )
      completed_cards = create_list(:card, 2, author_id: user2.id, organization: @org, state: 'published' )
      badge_card = create(:card, author_id: user2.id, organization: @org, state: 'published' )
      completed_courses = create_list(:card, 2, author_id: user2.id, organization: @org, state: 'published', card_type: 'course' )
      completed_cards.each do |c|
        create(:assignment, assignable: c, user_id: @user.id, state: "completed")
        create(:user_content_completion, completable: c, user_id: @user.id, state: "completed")
      end
      completed_courses.each do |c|
        create(:assignment, assignable: c, user_id: @user.id, state: "completed")
        create(:user_content_completion, completable: c, user_id: @user.id, state: "completed")
      end
      clc = create :clc, entity_id: @org.id, entity_type: "Organization", name: "Test_CLC"
      clc_badge = create(:clc_badge, organization_id: @org.id)
      clc_badging = create(:clc_badging, clc_badge: clc_badge, clc: clc)

      clc_progress = create(:clc_progress, user_id: @user.id, clc_score: 63, clc_id: clc.id)

      card_badge = create(:card_badge, organization: @org)
      card_badging = create(:card_badging, badge_id: card_badge.id, badgeable_id: badge_card.id)
      user_card_badge = create(:user_badge, user_id: @user.id, badging_id: card_badging.id )
      stats =  @user.user_stats

      assert_equal stats["Annual Leaning Goal"] , "50h"
      assert_equal stats["Continous Learning Hrs"] , "1h 3m"
      assert_equal stats["Published Content"] , 4
      assert_equal stats["Completed Content"] , 2
      assert_equal stats["Courses Completed"] , 2
      assert_equal stats["Badges Earned"] , 1

    end
  end

  test 'should return empty hash' do
    user = create(:user)
    user_custom_fields_hash = user.user_custom_fields_hash()
    assert user_custom_fields_hash.is_a?(Hash)
    assert_empty user_custom_fields_hash
  end

  test 'should return empty hash if for user has no custom_fields entries' do
    org = create(:organization)
    user, user2 = create_list(:user, 2, organization: org)
    custom_field = create(
      :custom_field, organization: org,
      display_name: 'Burrito Wrap'
    )
    create(
      :user_custom_field, custom_field: custom_field, user: user, value: 'juarez'
    )
    user_custom_fields = UserCustomField.where(user: [user, user2])
    custom_fields_hash = user_custom_fields.group_by(&:user_id)
      .map { |user_id, fields| [user_id, fields] }.to_h
    user_custom_fields_hash = user2.user_custom_fields_hash(custom_fields_hash)

    assert user_custom_fields_hash.is_a?(Hash)
    assert_empty user_custom_fields_hash
  end

  test 'should return related to user deleted cards' do
    org = create(:organization)
    author, user = create_list(:user, 2, organization: org)
    card1, card2, card3, card4, card5, card6, card7 = create_list(:card, 7,
      author: author
    )
    #user related cards
    user_card = create(:card, author: user)
    UsersCardsManagement.create(
      card: card1, action: 'added_to_pathway', user: user, target_id: 1
    )
    create(:vote, votable: card3, user_id: user.id)
    create(
      :learning_queue_item, :from_assignments,
      queueable: card5, user: user, state: 'completed'
    )

    #author related cards
    UsersCardsManagement.create(
      card: card2, action: 'added_to_pathway', user: author, target_id: 1
    )
    create(:vote, votable: card4, user_id: author.id)
    create(
      :learning_queue_item, :from_assignments,
      queueable: card6, user: author, state: 'completed'
    )

    cards = [card1, card2, card3, card4, card5, card6, card7, user_card]
    cards.each(&:destroy)
    cards.each(&:reload)

    user_related_card_ids = [card1, card3, card5, user_card].map(&:id)
    deleted_user_related_cards_ids = user.user_related_deleted_cards.pluck(:id)

    assert_same_elements user_related_card_ids, deleted_user_related_cards_ids
  end

  test '#strip_all_tags_from_user_input strips the HTML tags' do
    user = create :user

    input_text = "<p>Hello & Hello - Hello // Hello ? Hello's -> 'Hello' - `Hello`</p>"
    user.first_name = user.last_name = user.bio = input_text
    User.any_instance.unstub(:strip_all_tags_from_user_input)
    user.send(:strip_all_tags_from_user_input)

    expected_output = "Hello & Hello - Hello // Hello ? Hello's -> 'Hello' - `Hello`"
    assert_equal expected_output, user.bio
    assert_equal expected_output, user.first_name
    assert_equal expected_output, user.last_name

    user.first_name = user.last_name = user.bio = nil
    user.send(:strip_all_tags_from_user_input)

    assert_empty user.bio
    assert_empty user.first_name
    assert_empty user.last_name
  end

  test '#users_fields should properly store/fetch custom fields' do
    begin
      cache = Rails.cache
      Rails.cache = ActiveSupport::Cache::MemoryStore.new
      stub_time = Time.now
      fields = %w[handle first_name last_name email]
      org = create(:organization)

      #custom field created
      Timecop.freeze stub_time do
        cf1 = create(:custom_field, organization: org, display_name: 'country')
        cf1.set_value_for_config('enable_in_filters', true)
      end

      assert_same_elements fields.push('country'), User.users_fields(org)

      #custom_field added new
      Timecop.freeze stub_time + 1.seconds do
        cf2 = create(:custom_field, organization: org, display_name: 'city')
        cf2.set_value_for_config('enable_in_filters', true)
      end

      assert_same_elements fields + ['city'], User.users_fields(org)

      #custom_field updated
      Timecop.freeze stub_time + 2.seconds do
        org.custom_fields.find_by(display_name: 'city').update_attributes(
          display_name: 'new_city'
        )
      end

      assert_same_elements fields.push('new_city'), User.users_fields(org)

      #custom_field config changed
      Timecop.freeze stub_time + 3.seconds do
        org.custom_fields.find_by(display_name: 'country')
          .set_value_for_config('enable_in_filters', false)
      end

      updated_fields = fields.delete_if { |x| x == 'country' }
      assert_same_elements updated_fields, User.users_fields(org)
    ensure
      Rails.cache = cache
    end
  end

  test '#suspended? represents the state as `suspended`' do
    user = create :user, organization_role: 'member', is_active: false, is_suspended: true, status: 'suspended'

    assert user.suspended?

    user.update_attributes(is_active: false, is_suspended: false)
    refute user.reload.suspended?

    user.update_attributes(is_active: true, is_suspended: true)
    refute user.reload.suspended?

    user.update_attributes(is_active: true, is_suspended: false)
    refute user.reload.suspended?
  end

  test '#active? represents the state as `active`' do
    user = create :user, organization_role: 'member', is_active: true, is_suspended: false, status: 'active'

    assert user.active?

    user.update_attributes(is_active: false, is_suspended: false)
    refute user.reload.active?

    user.update_attributes(is_active: true, is_suspended: true)
    refute user.reload.active?

    user.update_attributes(is_active: false, is_suspended: true)
    refute user.reload.active?
  end

  test '#inactive? represents the state as `inactive`' do
    user = create :user, organization_role: 'member', is_active: true, is_suspended: false, status: 'inactive'

    assert user.inactive?

    user.update_attributes(is_active: false, is_suspended: false)
    refute user.reload.inactive?

    user.update_attributes(is_active: true, is_suspended: true)
    refute user.reload.inactive?

    user.update_attributes(is_active: false, is_suspended: true)
    refute user.reload.inactive?
  end
end

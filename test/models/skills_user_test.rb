require 'test_helper'

class SkillsUserTest < ActiveSupport::TestCase
  should belong_to :skill
  should belong_to :user
  should validate_presence_of(:user_id)
  should validate_presence_of(:skill_id)

  test 'uniquness of skill with user' do
    skill_user = create(:skills_user)
    refute build(:skills_user, user: skill_user.user, skill: skill_user.skill).valid?
  end

  test 'add skill_level to skill_users' do
    level = 'beginner'
    skill_user = create(:skills_user, skill_level: level)
    assert_equal level, SkillsUser.last.skill_level
  end
end

class UserSkillEventTest < ActiveSupport::TestCase
  setup do
    stub_time

    EdcastActiveJob.unstub :perform_later
  end

  test 'should record user skill created event' do
    user = create(:user)
    skill = create(:skill)
    skill_user = build(:skills_user, id: rand(100), user: user, skill: skill)

    metadata = { foo: "bar", user_id: user.id }
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    Analytics::MetricsRecorderJob.unstub(:perform_later)

    Analytics::MetricsRecorderJob.expects(:perform_later)
      .with({
        recorder: "Analytics::UserSkillMetricsRecorder",
        org: Analytics::MetricsRecorder.org_attributes(user.organization),
        skill: Analytics::MetricsRecorder.skill_attributes(skill),
        user_skill: Analytics::MetricsRecorder.skills_user_attributes(skill_user),
        timestamp: Time.now.to_i,
        event: "user_skill_created",
        actor: Analytics::MetricsRecorder.user_attributes(user),
        additional_data: metadata
      }).once

    skill_user.save
    skill_user.run_callbacks(:commit)
  end

  test 'should record user skill edited event' do
    TestAfterCommit.with_commits do

      user = create(:user)
      skill = create(:skill)
      skill_user = create(:skills_user, user: user, skill: skill)

      metadata = { foo: "bar", user_id: user.id }
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      Analytics::MetricsRecorderJob.unstub(:perform_later)

      Analytics::MetricsRecorderJob.expects(:perform_later)
        .with({
          recorder: "Analytics::UserSkillMetricsRecorder",
          org: Analytics::MetricsRecorder.org_attributes(user.organization),
          skill: Analytics::MetricsRecorder.skill_attributes(skill),
          user_skill: Analytics::MetricsRecorder.skills_user_attributes(skill_user)
            .merge!('skill_level' => 'expert'),
          timestamp: Time.now.to_i,
          event: "user_skill_edited",
          actor: Analytics::MetricsRecorder.user_attributes(user),
          changed_column: 'skill_level',
          old_val: skill_user.skill_level,
          new_val: 'expert',
          additional_data: metadata
        }).once

      skill_user.update(skill_level: 'expert')
    end
  end

  test 'should record user skill deleted event' do
    user = create(:user)
    skill = create(:skill)
    skill_user = create(:skills_user, user: user, skill: skill)

    metadata = { foo: "bar", user_id: user.id }
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    Analytics::MetricsRecorderJob.unstub(:perform_later)

    Analytics::MetricsRecorderJob.expects(:perform_later)
      .with({
        recorder: "Analytics::UserSkillMetricsRecorder",
        org: Analytics::MetricsRecorder.org_attributes(user.organization),
        skill: Analytics::MetricsRecorder.skill_attributes(skill),
        user_skill: Analytics::MetricsRecorder.skills_user_attributes(skill_user),
        timestamp: Time.now.to_i,
        event: "user_skill_deleted",
        actor: Analytics::MetricsRecorder.user_attributes(user),
        additional_data: metadata
      }).once

    skill_user.destroy
  end
end

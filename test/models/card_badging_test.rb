require 'test_helper'

class CardBadgingTest < ActiveSupport::TestCase
  should have_db_column(:title).of_type(:string)
  should have_db_column(:badge_id).of_type(:integer)
  should have_db_column(:badgeable_id).of_type(:integer)
  should have_db_column(:type).of_type(:string)

  should belong_to(:card_badge)
  should belong_to(:card)

  should have_many :user_badges

  should validate_presence_of(:badge_id)
  should validate_presence_of(:title)

  test 'should assign org before save' do
    org = create(:organization)
    user = create(:user, organization: org)
    card_badge = create(:card_badge, organization: org)
    card = create(:card, author_id: user.id, title: 'this is title', message: 'this is message')
    card_badging = create(:card_badging, badge_id: card_badge.id, badgeable_id: card.id)
    user_badges = create(:user_badge, user_id: user.id, badging_id: card_badging.id )

    assert_equal org.id, card_badge.organization_id
    assert_equal card_badge.type, 'CardBadge'
    assert_equal card_badging.type, 'CardBadging'
    assert_equal card_badging.badge_id, card_badge.id
    assert_equal card_badging.badgeable_id, card.id
  end
end

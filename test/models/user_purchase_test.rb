require 'test_helper'

class UserPurchaseTest < ActiveSupport::TestCase

  should have_db_column(:transaction_id).of_type(:integer)
  should have_db_column(:order_id).of_type(:integer)
  should have_db_column(:user_id).of_type(:integer)

  should belong_to(:user_transaction).class_name('Transaction')
  should belong_to(:order)

  should validate_presence_of(:transaction_id)
end

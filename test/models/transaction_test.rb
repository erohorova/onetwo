require 'test_helper'

class TransactionTest < ActiveSupport::TestCase

  should have_db_column(:order_id).of_type(:integer)
  should have_db_column(:gateway).of_type(:string)
  should have_db_column(:gateway_id).of_type(:string)
  should have_db_column(:gateway_data).of_type(:text)
  should have_db_column(:amount).of_type(:decimal)
  should have_db_column(:currency).of_type(:string)
  should have_db_column(:payment_state).of_type(:integer)
  should have_db_column(:user_id).of_type(:integer)
  should have_db_column(:source_name).of_type(:string)
  should have_db_column(:order_type).of_type(:string)

  should belong_to(:order)
  should belong_to(:user)

  should have_many(:transaction_audits)
  should have_many(:user_purchases)

  should validate_presence_of(:order_id)
  should validate_presence_of(:amount)
  should validate_presence_of(:currency)
  should validate_presence_of(:payment_state)
  should validate_presence_of(:user_id)

  setup do
    ['create_transaction_audit', 'create_wallet_transaction', 'create_card_subscription', 'create_user_subscription'].map do |stub_method|
      Transaction.any_instance.stubs(stub_method).returns(true)
    end
    @payment_object = PayPal::SDK::REST::Payment.new(id: 'PAY-A1B2C3D4E5F7G8H9', payer_id: 'ABCDE12345')
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @org_subscription = create :org_subscription, organization: @organization
    @recharge = create :recharge
    @author = create(:user, organization: @organization)
    @card = create :card, organization: @organization, author: @author
    @subscription_order = create :order, orderable: @org_subscription, user_id: @user.id,
                    organization_id: @organization.id
    @recharge_order = create :order, orderable: @recharge, user_id: @user.id,
                    organization_id: @organization.id
    @card_order = create :order, orderable: @card, user_id: @user.id,
                organization_id: @organization.id
  end

  test 'create transaction' do
    transaction1 = Transaction.initialize_payment!(
      order_id: @subscription_order.id,
      gateway: 'braintree',
      amount: BigDecimal.new("10"),
      currency: '$',
      user_id: 1,
      source_name: "",
      order_type: @subscription_order.orderable_type
      )

    transaction2 = Transaction.initialize_payment!(
      order_id: @subscription_order.id,
      gateway: 'braintree',
      amount: BigDecimal.new("10"),
      currency: '$',
      user_id: 1,
      source_name: "",
      order_type: @subscription_order.orderable_type)

    assert_not_nil transaction1
    assert_not_nil transaction2
    assert_equal transaction1.id, transaction2.id
  end

  test 'initialize_payment must store source name and order type in transactions for Card' do
    source_type_name = 'test-source-type'
    transaction = Transaction.initialize_payment!(
      order_id: @card_order.id,
      gateway: 'braintree',
      amount: BigDecimal.new("10"),
      currency: '$',
      user_id: @user.id,
      source_name: source_type_name,
      order_type: @card_order.orderable_type
      )

    assert_equal source_type_name, transaction.source_name
    assert_equal "Card", transaction.order_type
  end

  test 'should create transaction_audit after each transaction' do
    Transaction.any_instance.unstub(:create_transaction_audit)
    assert_difference -> {TransactionAudit.count} do
      transaction = create :transaction, order_id: @subscription_order.id, amount: BigDecimal.new("10"),
                           user_id: @user.id
      assert_equal TransactionAudit.last.transaction_id, transaction.id
    end
  end

  test 'should save gateway_data and payment_state in transaction_data after each transaction' do
    Transaction.any_instance.unstub(:create_transaction_audit)
    transaction = create :transaction, order_id: @subscription_order.id, amount: BigDecimal.new("10"),
                           user_id: @user.id

    braintree_success_result =
      YAML.load_file("#{Rails.application.root}/test/fixtures/braintree_success_result.yml")
    transaction_data = { gateway_data: braintree_success_result, payment_state: 'successful' }

    transaction.update(transaction_data)

    assert_equal TransactionAudit.last.transaction_id, transaction.id
    assert_equal braintree_success_result.transaction.id, TransactionAudit.last.transaction_data[:gateway_data].transaction.id
    assert_equal 'successful', TransactionAudit.last.transaction_data[:payment_state]
  end

  test 'should create credit wallet transaction if transaction was successful for Wallet Recharge' do
    Transaction.any_instance.unstub(:create_wallet_transaction)
    transaction = create :transaction, order_id: @recharge_order.id, amount: BigDecimal.new("10"),
                  user_id: @user.id
    assert_difference -> {WalletTransaction.count} do
      transaction.update(payment_state: 'successful')
    end
    assert_equal WalletTransaction.last.order_id, @recharge_order.id
    assert_equal WalletTransaction.last.payment_type, 'credit'
  end

  test 'should create card_subscription if transaction was successful for card' do
    Transaction.any_instance.unstub(:create_card_subscription)
    transaction = create :transaction, order_id: @card_order.id, amount: BigDecimal.new("10"),
                         user_id: @user.id
    assert_difference -> {CardSubscription.count} do
      transaction.update(payment_state: 'successful')
    end
    assert_equal CardSubscription.last.card_id, @card.id
    assert_equal CardSubscription.last.user_id, @user.id
  end

  test 'should create user_subscription if transaction was successful for organization' do
    Transaction.any_instance.unstub(:create_user_subscription)
    transaction = create :transaction, order_id: @subscription_order.id, amount: BigDecimal.new("10"),
                         user_id: @user.id
    assert_difference -> {UserSubscription.count} do
      transaction.update(payment_state: 'successful', gateway_id: 'ABCDE12345')
    end
    assert_equal UserSubscription.last.org_subscription_id, @org_subscription.id
    assert_equal UserSubscription.last.user_id, @user.id
  end

  test '#process_payment! for external and success' do
    Transaction.any_instance.stubs(:update_order_status).returns(true)
    transaction = create :transaction, order_id: 1, amount: BigDecimal.new("10"),
                         user_id: 1

    braintree_success_result =
      YAML.load_file("#{Rails.application.root}/test/fixtures/braintree_success_result.yml")

    result = {
      status: PaymentGateway::STATUS_SUCCESSFUL,
      gateway_id: "asdfasdf",
      gateway_data: braintree_success_result
    }

    BraintreePaymentGateway.any_instance.stubs(:authorize_payment).returns(Braintree::SuccessfulResult.new)
    Transaction.any_instance.stubs(:confirm_order).returns(true)
    BraintreePaymentGateway.any_instance.expects(:capture_payment).returns(result)

    transaction.process_payment!(nonce: 'fake-valid-nonce')
    assert transaction.successful?
  end


  test '#process_payment! failure' do
    Transaction.any_instance.stubs(:update_order_status).returns(true)
    transaction = create :transaction, order_id: 1, amount: BigDecimal.new("10"),
                         user_id: 1

    braintree_failure_result =
      YAML.load_file("#{Rails.application.root}/test/fixtures/braintree_failure_result.yml")

    result = {
      status: PaymentGateway::STATUS_FAILED,
      gateway_id: "asdfasdf",
      gateway_data: braintree_failure_result
    }

    BraintreePaymentGateway.any_instance.stubs(:authorize_payment).returns(Braintree::SuccessfulResult.new)
    Transaction.any_instance.stubs(:confirm_order).returns(true)
    BraintreePaymentGateway.any_instance.expects(:capture_payment).returns(result)
    
    transaction.process_payment!(nonce: 'fake-valid-nonce')
    assert transaction.successful?
  end

  test '#process_payment! raises PaymentAlreadyProcessedError' do
    Transaction.any_instance.stubs(:update_order_status).returns(true)
    order = create(:order)
    transaction = create :transaction, payment_state: :successful,
                         order: order, amount: 1, user_id: 1

    assert_raises(PaymentAlreadyProcessedError) {
      transaction.process_payment!(nonce: 'fake-valid-nonce')
    }
  end

  test '#process_payment! authorization failed for braintree' do
    Transaction.any_instance.stubs(:update_order_status).returns(true)
    Braintree::Transaction.stubs(:sale).returns(Braintree::ErrorResult.new(:gateway, :errors => {}))

    transaction = create :transaction
    transaction.process_payment!(nonce: 'fake-valid-nonce')
    assert transaction.reload.failed?
  end

  test '#process_payment! authorization failed for paypal' do
    Transaction.any_instance.stubs(:update_order_status).returns(true)
    PayPal::SDK::REST::Payment.stubs(:find).returns(@payment_object)
    @payment_object.stubs(:success?).returns(false)
    PaypalPaymentGateway.any_instance.stubs(:get_authorization_object).returns(PayPal::SDK::REST::Authorization.new)
    PayPal::SDK::REST::Payment.any_instance.stubs(:execute!).returns(false)

    transaction = create :transaction, gateway: 'paypal'
    transaction.process_payment!(payment_id: 'PAY-A1B2C3D4E5F7G8H9', payer_id: 'ABCDE12345')
    assert transaction.reload.failed?
  end

  test 'authorization succeded but order processing failed for Card' do
    Transaction.any_instance.stubs(:update_order_status).returns(true)
    Transaction.any_instance.stubs(:confirm_order).returns(false)
    stub_paypal_authorization
    PayPal::SDK::REST::Authorization.any_instance.stubs(:void).returns(true)

    transaction = create :transaction, order_id: @card_order.id, amount: BigDecimal.new("10"),
                         user_id: @user.id, gateway: 'paypal'

    PaypalPaymentGateway.any_instance.expects(:void_payment).returns(true)
    transaction.process_payment!(payment_id: 'PAY-A1B2C3D4E5F7G8H9', payer_id: 'ABCDE12345')
    assert transaction.reload.voided?
  end

  test 'authorization succeded but order processing errored for Card' do
    Transaction.any_instance.stubs(:update_order_status).returns(true)
    stub_paypal_authorization
    PayPal::SDK::REST::Authorization.any_instance.stubs(:void).returns(true)
    Card.any_instance.stubs(:authorize_course).raises(StandardError)

    transaction = create :transaction, order_id: @card_order.id, amount: BigDecimal.new("10"),
                         user_id: @user.id, gateway: 'paypal'

    PaypalPaymentGateway.any_instance.expects(:void_payment).returns(true)
    transaction.process_payment!(payment_id: 'PAY-A1B2C3D4E5F7G8H9', payer_id: 'ABCDE12345')
    assert transaction.reload.voided?
  end

  test 'authorization succeded, order processed for card, but capture failed' do
    Transaction.any_instance.stubs(:update_order_status).returns(true)
    Transaction.any_instance.stubs(:confirm_order).returns(true)
    stub_paypal_authorization
    PayPal::SDK::REST::Authorization.any_instance.stubs(:capture).returns(false)

    transaction = create :transaction, order_id: @card_order.id, amount: BigDecimal.new("10"),
                         user_id: @user.id, gateway: 'paypal'

    PaypalPaymentGateway.any_instance.expects(:capture_payment).returns(false)
    transaction.process_payment!(payment_id: 'PAY-A1B2C3D4E5F7G8H9', payer_id: 'ABCDE12345')
    assert transaction.reload.failed?
  end

  test 'authorization succeded, order processed for card, and capture successful' do
    Transaction.any_instance.stubs(:update_order_status).returns(true)
    Transaction.any_instance.stubs(:confirm_order).returns(true)
    stub_paypal_authorization
    PayPal::SDK::REST::Authorization.any_instance.stubs(:capture).returns(true)

    transaction = create :transaction, order_id: @card_order.id, amount: BigDecimal.new("10"),
                         user_id: @user.id, gateway: 'paypal'

    PaypalPaymentGateway.any_instance.expects(:capture_payment).returns(true)
    transaction.process_payment!(payment_id: 'PAY-A1B2C3D4E5F7G8H9', payer_id: 'ABCDE12345')
    assert transaction.reload.successful?
  end

  test '#confirm order should always return true for orderable Recharge' do
    transaction = create :transaction, order_id: @recharge_order.id, amount: BigDecimal.new("10"),
                  user_id: @user.id

    assert transaction.confirm_order
  end

  test '#confirm order should always return true for orderable OrgSubscription' do
    transaction = create :transaction, payment_state: :successful,
                         order_id: @subscription_order.id, amount: 1, user_id: @user.id

    assert transaction.confirm_order
  end

  test '#confirm order should return false for orderable Card if course not authorized' do
    Card.any_instance.stubs(:authorize_course).returns(false)
    transaction = create :transaction, order_id: @card_order.id, amount: BigDecimal.new("10"),
                         user_id: @user.id, gateway: 'paypal'

    refute transaction.confirm_order
  end

  test '#confirm order should return true for orderable Card if course is authorized' do
    Card.any_instance.stubs(:authorize_course).returns(true)
    transaction = create :transaction, order_id: @card_order.id, amount: BigDecimal.new("10"),
                         user_id: @user.id, gateway: 'paypal'

    assert transaction.confirm_order
  end

  private

  def stub_paypal_authorization
    PaypalPaymentGateway.any_instance.stubs(:authorize_payment).returns(@payment_object)
    PaypalPaymentGateway.any_instance.stubs(:get_authorization_object).returns(PayPal::SDK::REST::Authorization.new)
  end
end

require 'test_helper'

class ReportTest < ActiveSupport::TestCase

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
  end

  test "user not belonging to a group cannot report" do
    create_default_org
    group = create(:resource_group)
    user = create(:user)
    group.add_member user
    q = create(:question, user: user, group: group)

    report = build(:report, reportable: q, reporter: create(:user))
    assert_not report.save
  end

  test "cannot mark faculty item as reported" do
    group = create(:resource_group)
    faculty = create(:user)
    user = create(:user)
    group.add_admin faculty
    group.add_member user

    q = create(:question, group: group, user: faculty)
    report = build(:report, reportable: q, reporter: user)
    assert_not report.save
  end

  test 'get reports by user' do
    create_default_org
    user = create(:user)
    group = create(:resource_group)
    group.add_member user

    p1 = create(:post, user: user, group: group)
    p2 = create(:post, user: user, group: group)

    reporter = create(:user)
    group.add_member reporter

    create(:report, reporter: reporter, reportable: p1)

    user_reported_ids = Report.get_reports_by_user(reporter,
                                            reportable_ids: [p1.id, p2.id],
                                            reportable_type: 'Post')
    assert_equal user_reported_ids, [p1.id]

    blank_result = Report.get_reports_by_user(reporter,
                                              reportable_ids: [],
                                              reportable_type: 'Post')
    assert_equal blank_result, []

    # bad type
    bad_type = Report.get_reports_by_user(reporter,
                                          reportable_ids: [p1.id, p2.id],
                                          reportable_type: 'Blah')
    assert_equal bad_type, []

    #no type
    no_type = Report.get_reports_by_user(reporter,
                                         reportable_ids: [p1.id, p2.id],
                                        )
    assert_equal no_type, []
  end
end


require 'test_helper'

class PinTest < ActiveSupport::TestCase
   test "validate uniqueness of pin scoped to object" do
    org = create(:organization)
    user = create(:user, organization: org)
    channel = create(:channel, organization: org)
    card = create(:card, organization: org, author_id: nil)
    create(:pin, pinnable: channel, user: user, object: card)

    duplicate_pin = build(:pin, pinnable: channel, user: user, object: card)
    assert_not duplicate_pin.save
  end

  test "should update pins for pinnable if number of pins are lesser than the limit" do
    org = create(:organization)
    channel = create(:channel, organization: org)
    cards = create_list(:card, 5, author_id: nil, organization: org).each do |card|
      ChannelsCard.create(card: card, channel: channel)
      create_list(:comment, 2, commentable: card, user: create(:user, organization: org))
    end
    pins = Pin.fetch_featured_pins(channel, count: 4)
    assert_equal 4, pins.count
    assert_equal channel.id, pins.map(&:pinnable_id).uniq.first
    pins.map(&:object_id).each{|object|
      assert_includes  cards.map(&:id), object
    }
  end

  test "should update pins for pinnable with trending cards" do
    org = create(:organization)
    channel = create(:channel, organization: org)
    cards = create_list(:card, 3, author_id: nil, organization: org).each do |card|
      ChannelsCard.create(card: card, channel: channel)
    end
    create_list(:comment, 7, commentable: cards[0], user: create(:user, organization: org))
    create_list(:comment, 5, commentable: cards[1], user: create(:user, organization: org))
    create_list(:comment, 8, commentable: cards[2], user: create(:user, organization: org))

    assert_difference -> { Pin.count }, 3 do
      Pin.update_pinned_for_channel(channel.id)
    end
  end

  test "should delete previous pins before updating pins with new trending cards" do
    org = create(:organization)
    channel = create(:channel, organization: org)

    cards = create_list(:card, 3, author_id: nil, organization: org).each do |card|
      Pin.create(pinnable: channel, object: card)
      ChannelsCard.create(card: card, channel: channel)
    end

    trending_cards = create_list(:card, 2, author_id: nil, organization: org).each do |card|
      ChannelsCard.create(card: card, channel: channel)
      create_list(:comment, 5, commentable: card, user: create(:user, organization: org))
    end
    create_list(:comment, 7, commentable: cards[0], user: create(:user, organization: org))
    updated_pins = Pin.update_pinned_for_channel(channel.id)

    assert_includes updated_pins.map(&:id), cards[0].id
    assert_includes updated_pins.map(&:id), trending_cards[0].id
    assert_includes updated_pins.map(&:id), trending_cards[1].id

    assert_not_includes updated_pins.map(&:id), cards[1].id
    assert_not_includes updated_pins.map(&:id), cards[2].id
  end

  class CardPinMetricRecorderTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false
    setup do
      Analytics::MetricsRecorderJob.stubs(:perform_later)
    end

    test 'should invoke pin metric recorder job #pin' do
      IndexJob.stubs(:perform_later).returns true
      Card.any_instance.stubs(:record_card_create_event).returns true

      org =  create(:organization)
      user = create(:user, organization: org)
      channel = create(:channel, organization: org)
      card = create(:card, author: user)
      Analytics::MetricsRecorderJob.unstub :perform_later
      Analytics::MetricsRecorderJob.expects(:perform_later)
      .with(
        has_entries(
          event: 'card_pinned',
          recorder: 'Analytics::CardPinRecorder'
        )
      ).once

      create(:pin, pinnable: channel, user: user, object: card)
    end

    test 'should invoke card metric recorder job #unpin' do
      IndexJob.stubs(:perform_later).returns true
      Card.any_instance.stubs(:record_card_create_event).returns true
      Pin.any_instance.stubs(:record_card_pin_event).returns true
      RequestStore.unstub(:read)
      Organization.any_instance.stubs(:create_admin_user)

      org =  create(:organization)
      user = create(:user, organization: org)
      channel = create(:channel, organization: org)
      card = create(:card, author: user, organization: org)
      pin = create(:pin, pinnable: channel, user: user, object: card)
      metadata = {
        platform: 'web',
        user_agent: 'Edcast iPhone'
      }
      RequestStore.store[:request_metadata] = metadata
      Analytics::MetricsRecorderJob.unstub :perform_later
      # NOTE: this recorder uses Pin#user as the actor instead of reading the user_id from metadata.
      Timecop.freeze(2.days.from_now) do
        Analytics::MetricsRecorderJob.expects(:perform_later).with(
          has_entries(
            recorder: "Analytics::CardPinRecorder",
            # pin_id: pin.id,
            org: Analytics::MetricsRecorder.org_attributes(org),
            card: Analytics::MetricsRecorder.card_attributes(card),
            # channel: Analytics::MetricsRecorder.channel_attributes(channel),
            # actor: Analytics::MetricsRecorder.user_attributes(user),
            # timestamp: Time.now.to_i,
            event: 'card_unpinned',
            # additional_data: metadata
          )
        ).returns true
        pin.destroy
      end
    end

  end
end

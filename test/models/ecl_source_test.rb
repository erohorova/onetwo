require 'test_helper'

class EclSourceTest < ActiveSupport::TestCase
  should belong_to(:channel)

  should validate_uniqueness_of(:ecl_source_id).scoped_to(:channel_id)
  should validate_presence_of(:ecl_source_id)
  should validate_presence_of(:channel)

  test 'should setup ECL source fetch job for config channel_programming_v2 = false/nil' do
    channel = create(:channel)
    TestAfterCommit.with_commits do
      EclSource.any_instance.expects(:setup_fetch).once
      ecl_source = create(:ecl_source, channel: channel, ecl_source_id: 'abc-123-56')
    end
  end

  test 'should not setup fetch job for config channel_programming_v2 = true' do
    organization = create(:organization)
    create(:config, category: 'settings',
                    name: 'channel_programming_v2',
                    data_type: 'boolean',
                    value: 'true',
                    configable_id: organization.id,
                    configable_type: 'Organization')

    channel = create(:channel, organization: organization)

    TestAfterCommit.with_commits do
      EclSource.any_instance.expects(:setup_fetch).never
      ecl_source = create(:ecl_source, channel: channel, ecl_source_id: 'abc-123-56')
    end
  end

  test 'should perform ecl search on channel with topics' do
    TestAfterCommit.enabled = true
    EclSourceFetchJob.expects(:perform_later)

    org = create(:organization)
    channel = create(:channel, organization: org, topics: [{'id' => '11-11-11', 'name' => 'edcast.test', 'label' => 'test'}])
    ecl_source = create(:ecl_source, channel: channel, ecl_source_id: 'abc-123-56')

    ecl_search = mock
    EclApi::EclSearch.expects(:new).with(org.id).returns(ecl_search).once
    ecl_search.expects(:search).with(query: '', limit: 10, offset: 0, filter_params: {type: 'channel_programming', source_id: [ecl_source.ecl_source_id], topic: ['edcast.test'], topic_ids: ['11-11-11'], sort_by_recency: true}).returns(OpenStruct.new(results: [], aggregations: [])).once
    ecl_source.search_ecl
  end

  test 'should perform ecl search on channel without topics' do
    TestAfterCommit.enabled = true
    EclSourceFetchJob.expects(:perform_later)

    org2 = create(:organization)
    channel = create(:channel, organization: org2)
    ecl_source = create(:ecl_source, channel: channel, ecl_source_id: 'abc-123-57')

    ecl_search = mock
    EclApi::EclSearch.expects(:new).with(org2.id).returns(ecl_search).once
    ecl_search.expects(:search).with(query: '', limit: 10, offset: 0, filter_params: {type: 'channel_programming', source_id: [ecl_source.ecl_source_id], topic: [], topic_ids: [], sort_by_recency: true}).returns(OpenStruct.new(results: [], aggregations: [])).once
    ecl_source.search_ecl
  end

  test 'should return ecl fetch rate' do
    TestAfterCommit.enabled = true
    EclSourceFetchJob.expects(:perform_later).at_most(3)

    # hour
    ecl_source1 = create(:ecl_source, rate_interval: 2, rate_unit: 'hours')
    assert_equal ecl_source1.rate, 2.hours

    # minutes
    ecl_source2 = create(:ecl_source, rate_interval: 10, rate_unit: 'minutes')
    assert_equal ecl_source2.rate, 10.minutes

    # days
    ecl_source3 = create(:ecl_source, rate_interval: 1, rate_unit: 'days')
    assert_equal ecl_source3.rate, 1.days
  end

  test 'should set next_run_at if nil while creating' do
    TestAfterCommit.enabled = true
    EclSourceFetchJob.expects(:perform_later)
    ecl_source = create(:ecl_source, next_run_at: nil)

    assert_not_nil ecl_source.next_run_at
  end

  test 'should not set next_run_at if NOT nil while creating' do
    TestAfterCommit.enabled = true
    EclSourceFetchJob.expects(:perform_later)
    earlier = Time.zone.now - 1.year
    ecl_source = create(:ecl_source, next_run_at: earlier)

    assert_equal earlier.to_a, ecl_source.next_run_at.to_a
  end

  test 'should not set next_run_at if nil but only updating' do
    TestAfterCommit.enabled = true
    EclSourceFetchJob.expects(:perform_later)
    ecl_source = create(:ecl_source)
    ecl_source.update(next_run_at: nil)

    assert_nil ecl_source.next_run_at
  end

  test "records metric on create" do
    EclSource.any_instance.stubs :setup_fetch
    org = create :organization
    actor = create :user, organization: org
    channel = create(:channel, organization: org)
    metadata = {
      user_id: actor.id,
      foo: "bar"
    }
    stub_time
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    ecl_source = build(:ecl_source, channel: channel, ecl_source_id: 'abc-123-56')
    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        recorder: "Analytics::ChannelEclSourceMetricsRecorder",
        channel: Analytics::MetricsRecorder.channel_attributes(channel),
        event: "channel_ecl_source_added",
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        timestamp: Time.now.to_i,
        additional_data: metadata,
        ecl_source_id: ecl_source.ecl_source_id
      )
    )
    ecl_source.run_callbacks(:commit) { ecl_source.save }
    assert ecl_source.persisted?
  end

  test "records metric on delete" do
    EclSource.any_instance.stubs :setup_fetch
    org = create :organization
    actor = create :user, organization: org
    channel = create(:channel, organization: org)
    metadata = {
      user_id: actor.id,
      foo: "bar"
    }
    stub_time
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    ecl_source = create(:ecl_source, channel: channel, ecl_source_id: 'abc-123-56')
    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        recorder: "Analytics::ChannelEclSourceMetricsRecorder",
        channel: Analytics::MetricsRecorder.channel_attributes(channel),
        event: "channel_ecl_source_removed",
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        timestamp: Time.now.to_i,
        additional_data: metadata,
        ecl_source_id: ecl_source.ecl_source_id
      )
    )
    ecl_source.destroy
    refute ecl_source.persisted?
  end

end

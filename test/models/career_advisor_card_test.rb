require 'test_helper'

class CareerAdvisorCardTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper
  should belong_to :card
  should belong_to :career_advisor

  class CareerCardMetricsRecorderRecorderTest < ActiveSupport::TestCase
    test "should push to influx card_added_to_career_advisor  event when career advisor created" do
      TestAfterCommit.enabled = true
      org             = create :organization
      actor           = create(:user, organization: org, organization_role: 'admin')
      career_advisor  = create(:career_advisor, organization: org)
      course_card     = create(:card, organization: org, card_type: 'course', author_id: actor.id)

      auth_api_info   = {"auth_required":true, "url":" asdsa ", "method":"POST", "host":"asdsa das"}

      career_advisor_card = CareerAdvisorCard.new(
        id:             10000001,
        card:           course_card, 
        career_advisor: career_advisor, 
        level:          "beginner"
      )
      stub_time       = Time.now
      metadata        = { user_id: actor.id }    
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries(
          recorder:             "Analytics::CardMetricsRecorder",
          event:                "card_added_to_career_advisor",
          actor:                Analytics::MetricsRecorder.user_attributes(actor),
          org:                  Analytics::MetricsRecorder.org_attributes(org),
          card:                 Analytics::MetricsRecorder.card_attributes(course_card),
          career_advisor_card:  Analytics::MetricsRecorder.career_advisor_card_attributes(career_advisor_card),
          timestamp:            stub_time.to_i, 
          additional_data:      metadata
        )
      )
      Timecop.freeze(stub_time) do
        career_advisor_card.save
      end    
    end

    test "should push to influx card_removed_from_career_advisor event when career advisor deleted" do
      TestAfterCommit.enabled = true
      org             = create :organization
      actor           = create(:user, organization: org, organization_role: 'admin')
      career_advisor  = create(:career_advisor, organization: org)
      course_card     = create(:card, organization: org, card_type: 'course', author_id: actor.id)

      auth_api_info   = {"auth_required":true, "url":" asdsa ", "method":"POST", "host":"asdsa das"}

      career_advisor_card = CareerAdvisorCard.create(
        id:               10000001,
        card:             course_card, 
        career_advisor:   career_advisor, 
        level:            "beginner"
      )
      stub_time       = Time.now
      metadata        = { user_id: actor.id }    
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries(
          recorder:             "Analytics::CardMetricsRecorder",
          event:                "card_removed_from_career_advisor",
          actor:                Analytics::MetricsRecorder.user_attributes(actor),
          org:                  Analytics::MetricsRecorder.org_attributes(org),
          card:                 Analytics::MetricsRecorder.card_attributes(course_card),
          career_advisor_card:  Analytics::MetricsRecorder.career_advisor_card_attributes(career_advisor_card),
          timestamp:            stub_time.to_i, 
          additional_data:      metadata
        )
      )
      Timecop.freeze(stub_time) do
        career_advisor_card.destroy
      end    
    end

    test "should push to influx card_edited_from_career_advisor event when career advisor edited" do
      TestAfterCommit.enabled = true
      org             = create :organization
      actor           = create(:user, organization: org, organization_role: 'admin')
      career_advisor  = create(:career_advisor, organization: org)
      course_card     = create(:card, organization: org, card_type: 'course', author_id: actor.id)

      auth_api_info   = {"auth_required":true, "url":" asdsa ", "method":"POST", "host":"asdsa das"}

      career_advisor_card = CareerAdvisorCard.create(
        id:               10000001,
        card:             course_card, 
        career_advisor:   career_advisor, 
        level:             "beginner"
      )
      stub_time       = Time.now
      metadata        = { user_id: actor.id }  
      career_advisor_card.level = 'intermediate'  
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries(
          recorder:             "Analytics::CardEditedMetricsRecorder",
          event:                "card_edited_from_career_advisor",
          actor:                Analytics::MetricsRecorder.user_attributes(actor),
          org:                  Analytics::MetricsRecorder.org_attributes(org),
          card:                 Analytics::MetricsRecorder.card_attributes(course_card),
          career_advisor_card:  Analytics::MetricsRecorder.career_advisor_card_attributes(career_advisor_card),
          timestamp:            stub_time.to_i, 
          additional_data:      metadata,
          changed_column:       'level',
          old_val:              'beginner',
          new_val:              'intermediate'
        )
      )
      Timecop.freeze(stub_time) do
        career_advisor_card.save
      end    
    end
  end
end

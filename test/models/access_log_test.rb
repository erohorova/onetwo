require 'test_helper'
# DEPRECATED
class AccessLogTest < ActiveSupport::TestCase
  test 'should not allow more than once access log per day' do
    skip 'deprecated'

    user = create(:user)
    assert_difference ->{AccessLog.count}, 1 do
      access_log_1 = create(:access_log, user: user)
      assert_raise ActiveRecord::RecordNotUnique do
        access_log_2 = create(:access_log, user: user)
      end
    end
  end

  test 'should allow multiple daily entries with different platform' do
    skip 'deprecated'

    user = create(:user)
    assert_difference ->{AccessLog.count}, 2 do
      access_log_1 = create(:access_log, user: user, platform: 'web')
      access_log_2 = create(:access_log, user: user, platform: 'ios')
    end
  end
  test 'should allow next day entry' do
    skip 'deprecated'

    user = create(:user)
    assert_difference ->{AccessLog.count}, 2 do
      access_log_1 = create(:access_log, user: user, platform: 'web', access_at: Time.now.to_date)
      access_log_2 = create(:access_log, user: user, platform: 'web', access_at: 1.day.from_now.to_date)
    end
  end
end
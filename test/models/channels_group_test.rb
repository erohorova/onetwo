require 'test_helper'

class ChannelsGroupTest < ActiveSupport::TestCase
  test 'should belong to same org' do
    org1 = create(:organization)
    org2 = create(:organization)

    g1 = create(:resource_group, organization: org1)
    g2 = create(:resource_group, organization: org2)
    ch = create(:channel, organization: org1)

    assert ChannelsGroup.new(channel: ch, group: g1).valid?
    refute ChannelsGroup.new(channel: ch, group: g2).valid?
  end

  test 'records event on creation if course_code is set' do
    time = Time.now
    Time.stubs(:now).returns time

    Analytics::MetricsRecorder.unstub(:perform_later)
    EdcastActiveJob.unstub(:perform_later)
    
    TestAfterCommit.enabled = true
    org = create :organization
    actor = create :user, organization: org
    RequestStore.stubs(:read).with(:request_metadata).returns({ user_id: actor.id })
    id = (ResourceGroup.last&.id || 0) + 1
    channel = create :channel, organization: org
    course = ResourceGroup.create!(
      organization: org,
      creator:      actor,
      name:         "foo",
      description:  "bar",
      course_code:  "123"
    )
    channels_group = ChannelsGroup.new(
      group: course,
      channel: channel
    )
    Analytics::MetricsRecorderJob.expects(:perform_later).with($x={
      event: "channel_course_added",
      recorder: "Analytics::ChannelMetricsRecorder",
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      org: Analytics::MetricsRecorder.org_attributes(org),
      channel: Analytics::MetricsRecorder.channel_attributes(channel),
      course: Analytics::MetricsRecorder.resource_group_attributes(course),
      additional_data: { user_id: actor.id },
      timestamp: time.to_i
    })
    channels_group.save!
  end

  test 'records event on deletion if course_code is set' do
    time = Time.now
    Time.stubs(:now).returns time

    Analytics::MetricsRecorder.unstub(:perform_later)
    EdcastActiveJob.unstub(:perform_later)
    
    TestAfterCommit.enabled = true
    org = create :organization
    actor = create :user, organization: org
    RequestStore.stubs(:read).with(:request_metadata).returns({ user_id: actor.id })
    id = (ResourceGroup.last&.id || 0) + 1
    channel = create :channel, organization: org
    course = ResourceGroup.create!(
      organization: org,
      creator:      actor,
      name:         "foo",
      description:  "bar",
      course_code:  "123"
    )
    channels_group = ChannelsGroup.create!(
      group: course,
      channel: channel
    )
    Analytics::MetricsRecorderJob.expects(:perform_later).with($x={
      event: "channel_course_removed",
      recorder: "Analytics::ChannelMetricsRecorder",
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      org: Analytics::MetricsRecorder.org_attributes(org),
      channel: Analytics::MetricsRecorder.channel_attributes(channel),
      course: Analytics::MetricsRecorder.resource_group_attributes(course),
      additional_data: { user_id: actor.id },
      timestamp: time.to_i
    })
    channels_group.destroy
  end

  test 'records no event on creation if course_code is nil' do
    time = Time.now
    Time.stubs(:now).returns time

    Analytics::MetricsRecorder.unstub(:perform_later)
    EdcastActiveJob.unstub(:perform_later)
    
    TestAfterCommit.enabled = true
    org = create :organization
    actor = create :user, organization: org
    RequestStore.stubs(:read).with(:request_metadata).returns({ user_id: actor.id })
    id = (ResourceGroup.last&.id || 0) + 1
    channel = create :channel, organization: org
    course = ResourceGroup.create!(
      organization: org,
      creator:      actor,
      name:         "foo",
      description:  "bar",
      course_code:  nil
    )
    channels_group = ChannelsGroup.new(
      group: course,
      channel: channel
    )
    Analytics::MetricsRecorderJob.expects(:perform_later).never
    channels_group.save!
  end

end

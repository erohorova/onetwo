require 'test_helper'
class WowzaVideoStreamTest < ActiveSupport::TestCase

  should validate_presence_of(:organization)
  should validate_presence_of(:start_time)
  should validate_presence_of(:name)

  setup do
    VideoStream.any_instance.stubs :create_index
    VideoStream.any_instance.stubs :update_index
    VideoStream.any_instance.stubs :delete_index
    VideoStream.any_instance.stubs :reindex_async
    NotificationGeneratorJob.stubs :set
  end

  test 'slugs to be unique across orgs' do
    org1, org2 = create_list(:organization, 2)
    v1 = create(:wowza_video_stream, creator: nil, name: 'name', organization_id: org1.id)
    v2 = create(:wowza_video_stream, creator: nil, name: 'name', organization_id: org2.id)
    assert_equal 'name', v1.slug
    assert_equal v2.slug, v1.slug
  end

  test 'should be able to create a stream without an author' do
    v = build(:wowza_video_stream, creator: nil, organization_id: create(:organization).id)
    assert v.save
  end

  test 'should create a random uuid before create' do
    @org = create(:organization)
    @admin = create(:user, organization: @org)

    v = create(:wowza_video_stream, creator: @admin)
    assert_not_nil v.uuid

    v = create(:wowza_video_stream, uuid: 'custom-id', creator: @admin)
    assert_not_nil v.uuid
    assert_equal 'custom-id', v.reload.uuid
  end

  test 'uuid uniqueness validations' do
    @org = create(:organization)
    @admin = create(:user, organization: @org)

    v = create(:wowza_video_stream, uuid: 'a-b', creator: @admin)

    another_v = build(:wowza_video_stream, uuid: 'a-b', creator: @admin)
    assert_not another_v.save
  end

  test 'should be able to attach an image' do
    @org = create(:organization)
    @admin = create(:user, organization: @org)

    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)

    v = create(:wowza_video_stream, creator: @admin)
    fr1.update(attachable: v.card)

    v.reload
    assert_equal fr1, v.file_resource
  end

  test 'image method should work and return image from video_stream card' do
    @org = create(:organization)
    @admin = create(:user, organization: @org)

    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)

    v = create(:wowza_video_stream, creator: @admin)
    # fr1.update(attachable: v)
    v.file_resource = fr1

    v.reload
    assert_equal fr1.file_url, v.image

    without_image = create(:wowza_video_stream, creator: @admin)
    assert_nil without_image.image
  end

  test 'should get publish url' do
    @org = create(:organization)
    @admin = create(:user, organization: @org)

    v = create(:wowza_video_stream, uuid: 'abc', status: 'upcoming', creator: @admin, start_time: Time.now.utc + 1.day)
    assert_equal 'http://localhost:1935/live/abc/playlist.m3u8', v.playback_url
    time_now = Time.now
    Time.stubs(:now).returns(time_now)
    token = Digest::SHA256.hexdigest([Edcast::Application.config.secret_key_base, v.uuid, time_now.to_i].join('|'))
    token_with_timestamp = "#{token}-#{time_now.to_i}"
    assert_equal 'rtmp://localhost:1935/live', v.publish_url
    assert_equal token_with_timestamp, v.publish_token
  end

  test 'relevant channels should work' do
    @org = create(:organization)

    user1 = create(:user, handle: 'user1', organization: @org)
    random_user = create(:user, handle: 'randomuser', organization: @org)
    user2 = create(:user, handle: 'user2', organization: @org)

    c1 = create(:channel, autopull_content: true, label: 'first', organization: @org)
    c2 = create(:channel, autopull_content: true, label: 'second', organization: @org)
    c_noq = create(:channel, label: 'third', organization: @org)
    c_noautopull = create(:channel, autopull_content: false, organization: @org)
    c_direct = create(:channel, autopull_content: false, organization: @org)

    v1 = create(:wowza_video_stream, creator: user1, name: '#tag1')
    v1.channels << c_direct
    v1.channels << c1
    v2 = create(:wowza_video_stream, creator: user2, name: 'some name')
    v3 = create(:wowza_video_stream, creator: random_user, name: '#tag1')
    v3.channels << c1

    assert_same_elements [c1, c_direct], v1.relevant_channels
    assert_equal [], v2.relevant_channels

    # should work for all users not just influencers
    assert_equal [c1], v3.relevant_channels
  end

  test 'update score' do
    VideoStream.any_instance.unstub :create_index
    VideoStream.any_instance.unstub :update_index
    VideoStream.any_instance.unstub :delete_index
    VideoStream.any_instance.unstub :reindex_async

    WebMock.disable!
    MetricRecord.create_index! force: true

    org = create(:organization)
    video_stream = create(:wowza_video_stream, creator: create(:user, organization: org))
    users = create_list(:user, 5, organization: org)
    video_stream.run_callbacks(:commit)
    VideoStreamUpdateScoreJob.stubs(:perform_later).returns(true)
    Timecop.freeze
    users.each do |user|
      create(:comment, commentable: video_stream, user:user, created_at: Time.now - 8.days)
    end
    create(:comment, commentable: video_stream, user:users[0], created_at: Time.now - 5.days)
    users.each do |user|
      create(:vote, votable: video_stream, voter:user)
    end
    VideoStream.searchkick_index.refresh
    video_stream.update_score
    VideoStream.searchkick_index.refresh
    video_stream.reindex
    elasticsearch_object = Search::VideoStreamSearch.new.search_by_id(video_stream.id)
    assert_equal 30, elasticsearch_object[:weekly_score]
    assert_equal 55, elasticsearch_object[:monthly_score]
    weekly_valid_till = Time.zone.parse(elasticsearch_object[:weekly_score_valid_till])
    quarterly_valid_till = Time.zone.parse(elasticsearch_object[:quarterly_score_valid_till])
    monthly_valid_till = Time.zone.parse(elasticsearch_object[:monthly_score_valid_till])
    #valid time can't to be exact second, so comparing with error refernce of upto 10 secs
    assert_equal weekly_valid_till.utc.to_i, 2.days.from_now.utc.to_i
    assert_equal monthly_valid_till.utc.to_i, 22.days.from_now.utc.to_i
    assert_equal quarterly_valid_till.utc.to_i, 82.days.from_now.utc.to_i
    Timecop.return
    WebMock.enable!
  end

  test 'should delete notifications and remove from user queue on video stream delete' do
    v = create(:wowza_video_stream)
    v.stubs(:create_activity_stream) 
    v.run_callbacks(:commit)

    u1, u2 = create_list(:user, 2, organization: v.creator.organization)
    
    [u1, u2].each do |u|
      Notification.create!(notification_type: 'stream_started', notifiable: v, user: u)
    end

    UserContentsQueue.expects(:dequeue_content).with(content_id: v.id, content_type: 'VideoStream').returns(nil).once
    
    assert_equal 2, Notification.where(notifiable: v).count

    v.destroy
    v.run_callbacks(:commit)
  
    assert_equal 0, Notification.where(notifiable: v).count
  end

  class StreamGoingLiveTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false

    setup do
      EnqueueStreamsJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later

      User.any_instance.stubs(:reindex_async).returns(true)
      VideoStream.any_instance.stubs :create_index
      VideoStream.any_instance.stubs :update_index
      VideoStream.any_instance.stubs :delete_index
      VideoStream.any_instance.stubs :reindex_async
      NotificationGeneratorJob.stubs :set
    end

    test 'should call enqueue job on going live' do
      skip("INTERMITTENT FAILURE ON TRAVIS")
      v = create(:wowza_video_stream, status: 'upcoming')
      EnqueueStreamsJob.expects(:perform_later).returns(nil).once
      v.update_attributes(status: 'live')
    end

    test 'should create a notification on moving a stream from upcoming to live' do
      skip("INTERMITTENT FAILURE ON TRAVIS")
      EnqueueStreamsJob.stubs(:perform_later).returns(nil)

      creator = create(:user)
      v = create(:wowza_video_stream, status: 'upcoming', creator: creator, start_time: Time.now.utc)
      NotificationGeneratorJob.unstub :perform_later

      PubnubNotifier.expects(:notify).with(has_entries(item: v.card, notification_type: 'stream_started', excluded_user_ids: [v.creator_id], deep_link_id: v.card_id, deep_link_type: 'card', content: "#{creator.name} is now live: '#{v.snippet}'")).returns(nil).once
      v.update_attributes(status: 'live')

      NotificationGeneratorJob.expects(:perform_later).never
      v.update_attributes(status: 'past')

      v.update_attributes(status: 'upcoming')
      NotificationGeneratorJob.expects(:perform_later).with(item_id: v.id, item_type: 'video_stream', notification_type: 'stream_started').once
      v.update_attributes(status: 'live')
      NotificationGeneratorJob.stubs :perform_later
    end
  end

  class PastStreamProcessTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false

    setup do
      User.any_instance.stubs(:reindex_async).returns(true)
      VideoStream.any_instance.stubs(:reindex_async)

      VideoStream.any_instance.stubs :create_index
      VideoStream.any_instance.stubs :update_index
      VideoStream.any_instance.stubs :delete_index
      VideoStream.any_instance.stubs :reindex_async
      NotificationGeneratorJob.stubs :set
    end

    test 'should call pubnub get history on stream end' do
      skip("INTERMITTENT FAILURE ON TRAVIS")
      PubnubGetHistoryJob.unstub(:perform_later)
      EdcastActiveJob.unstub :perform_later

      v = create(:wowza_video_stream, status: 'upcoming')
      PubnubGetHistoryJob.any_instance.expects(:perform).with(v.id).returns(nil).once
      v.update_attributes(status: 'live')
      v.update_attributes(status: 'past')
    end

    test 'stoping a stream should trigger aws upload job' do
      skip("INTERMITTENT FAILURE ON TRAVIS")
      WowzaVideoStreamServerUploadJob.unstub(:perform_later)
      v = create(:wowza_video_stream, creator: create(:user), status: 'upcoming')
      NotificationGeneratorJob.expects(:perform_later).once
      WowzaVideoStreamServerUploadJob.expects(:perform_later).once.with(video_stream_id: v.id)
      v.update_attributes(status: 'live')
      v.update_attributes(status: 'past')
    end
  end
end

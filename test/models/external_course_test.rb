require 'test_helper'

class ExternalCourseTest < ActiveSupport::TestCase
  should have_db_column(:name).of_type(:string)
  should have_db_column(:description).of_type(:text)
  should have_db_column(:image_url).of_type(:string)
  should have_db_column(:course_url).of_type(:string)
  should have_db_column(:integration_id).of_type(:integer)
  should have_db_column(:course_key).of_type(:string)

  should belong_to :integration
  should have_many :users_external_courses
  should have_many(:users).through(:users_external_courses)

  should validate_presence_of(:name)
  should validate_presence_of(:integration_id)
  should validate_presence_of(:course_url)
  should validate_presence_of(:course_key)
end

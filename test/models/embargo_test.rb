require 'test_helper'

class EmbargoTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper
  setup do
    # create a record which is used by validation tests
    @org = create(:organization)
    @user = create(:user, organization: @org)
    @embargo = Embargo.create(user: @user, organization: @org, value: 'value', category: 'unapproved_words')
  end

  should belong_to :organization
  should belong_to :user

  should validate_presence_of :value
  should validate_presence_of :category
  should validate_presence_of :user_id
  should validate_presence_of :organization_id
  should validate_uniqueness_of(:value).scoped_to(:organization_id)

  # test "the truth" do
  #   assert true
  # end

  test "should push influx org_embargo_added event when embargo is created" do
    TestAfterCommit.enabled = true
    org = create :organization
    user = create :user, organization: org
    actor = create(:user, organization: org, organization_role: 'admin')
    embargo = Embargo.new(
        organization_id: org.id, category: "approved_sites", 
        value: "https://approved.sites", user: user
        )
    stub_time = Time.now
    metadata = { user_id: actor.id }    
    
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries(
        recorder: "Analytics::OrgMetricsRecorder",
        event: "org_embargo_added",
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        org: Analytics::MetricsRecorder.org_attributes(org),
        timestamp: stub_time.to_i,
        additional_data: metadata
        )
      )
    Timecop.freeze(stub_time) do
      embargo.save
    end    
  end
  
  test "should push influx org_embargo_edited event when embargo is edited" do
    TestAfterCommit.enabled = true
    org = create :organization
    user = create :user, organization: org
    actor = create(:user, organization: org, organization_role: 'admin')
    embargo = Embargo.create!(
        organization_id: org.id, category: "approved_sites", 
        value: "https://approved.sites", user: user
        )
    stub_time = Time.now
    metadata = { user_id: actor.id } 
    embargo.value = "https://approvededited.sites"   
    
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries(
        recorder: "Analytics::OrgEditedMetricsRecorder",
        event: "org_embargo_edited",
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        org: Analytics::MetricsRecorder.org_attributes(org),
        timestamp: stub_time.to_i,
        embargo: Analytics::MetricsRecorder.embargo_attributes(embargo),
        additional_data: metadata,
        changed_column: "value",
        old_val: "https://approved.sites",
        new_val: "https://approvededited.sites"
        )
      )
    Timecop.freeze(stub_time) do
      embargo.save
    end    
  end

  test "should push influx org_embargo_removed event when embargo is deleted" do
    TestAfterCommit.enabled = true
    org = create :organization
    user = create :user, organization: org
    actor = create(:user, organization: org, organization_role: 'admin')
    embargo = Embargo.create!(
        organization_id: org.id, category: "approved_sites", 
        value: "https://approved.sites", user: user
        )
    stub_time = Time.now
    metadata = { user_id: actor.id }   

    
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries(
        recorder: "Analytics::OrgMetricsRecorder",
        event: "org_embargo_removed",
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        org: Analytics::MetricsRecorder.org_attributes(org),
        timestamp: stub_time.to_i,
        embargo: Analytics::MetricsRecorder.embargo_attributes(embargo),
        additional_data: metadata
        )
      )
    Timecop.freeze(stub_time) do
      embargo.destroy
    end    
  end
end
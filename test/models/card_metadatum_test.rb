require 'test_helper'

class CardMetadatumTest < ActiveSupport::TestCase
  test "creation of metadata with valid plan type" do
    assert_raise ActiveRecord::RecordInvalid do
      card = build(:card_metadatum, plan: 'media')
      card.save!
    end

    assert_nothing_raised do
      c1 = build(:card_metadatum, plan: 'free')
      c1.save!
      c2 = build(:card_metadatum, plan: 'paid')
      c2.save!
      c3 = build(:card_metadatum, plan: 'premium')
      c3.save!
      c4 = build(:card_metadatum, plan: 'subscription')
      c4.save!
      c4 = build(:card_metadatum, plan: 'free/paid')
      c4.save!
    end
  end

  test "creation of metadata with valid level" do
    assert_raise ActiveRecord::RecordInvalid do
      card = build(:card_metadatum, level: 'test')
      card.save!
    end

    assert_nothing_raised do
      c1 = build(:card_metadatum, level: 'beginner')
      c1.save!
      c2 = build(:card_metadatum, level: 'intermediate')
      c2.save!
      c3 = build(:card_metadatum, level: 'advanced')
      c3.save!
    end
  end

  test 'reindex card and push to ecl on card_metadata create or update of level' do
    TestAfterCommit.with_commits(true) do
      CardMetadatum.any_instance.expects(:reindex_card).twice
      CardMetadatum.any_instance.expects(:push_to_ecl).twice
      card = create :card
      card_metadata = create :card_metadatum, card: card, level: 'beginner'
      card_metadata.update level: 'intermediate'
    end
  end

  test 'reindex card and push to ecl on card_metadata when there is an actual change in floor value of average_rating' do
    TestAfterCommit.with_commits(true) do
      CardMetadatum.any_instance.expects(:reindex_card).once
      CardMetadatum.any_instance.expects(:push_to_ecl).once
      card = create :card
      card_metadata = create :card_metadatum, card: card, average_rating: 1
      card_metadata.update average_rating: 1.2
      card_metadata.update average_rating: 1.4
      card_metadata.update average_rating: 1.6
      card_metadata.update average_rating: 1.9
      CardMetadatum.any_instance.expects(:reindex_card).once
      CardMetadatum.any_instance.expects(:push_to_ecl).once
      card_metadata.update average_rating: 2
    end
  end
end

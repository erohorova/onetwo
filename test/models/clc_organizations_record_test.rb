require 'test_helper'

class ClcOrganizationsRecordTest < ActiveSupport::TestCase
  should belong_to(:organization)
  should belong_to(:user)
  should belong_to(:card)
  should validate_presence_of(:organization_id)
  should validate_presence_of(:user_id)
  should validate_presence_of(:card_id)


  test 'validates uniquess of user scoped to card' do
    org = create(:organization)

    card = create(:card, author_id: nil, organization: org)
    user = create(:user, organization: org)
    user1 = create(:user, organization: org)
    clc_org_record  = create(:clc_organizations_record, organization: org, card: card, user: user)
    clc_org_record1 = build(:clc_organizations_record, organization: org, card: card, user: user)

    refute clc_org_record1.valid?
    assert_equal "User has already been taken", clc_org_record1.full_errors

    clc_org_record2  = build(:clc_organizations_record, organization: org, card: card, user: user1)
    assert clc_org_record2.valid?
  end

end

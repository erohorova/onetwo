require 'test_helper'

class WebexConfigTest < ActiveSupport::TestCase
  should belong_to :organization
  should validate_presence_of(:site_name)
  should validate_presence_of(:site_id)
  should validate_presence_of(:partner_id)
  should validate_presence_of(:organization_id)
end

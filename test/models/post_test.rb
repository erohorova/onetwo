require 'test_helper'

class PostTest < ActiveSupport::TestCase
  #TODO: Again, dependency on live url should be removed

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    @org = create(:organization)
    @client = create(:client, organization: @org)
    @group = create(:resource_group, organization: @org, client: @client)
    @user = create(:user, organization: @org)
    @faculty = create(:user, organization: @org)
    @group.add_member @user
    @group.add_admin @faculty
    stub_request(:get, "http://url.com").
          to_return(:status => 200,
                    :body => '<html>
                    <head>
                      <title>title</title>
                      <meta property="og:title" content="og:title">
                      <meta property="og:description" content="og:description">
                      <meta property="og:site_name" content="og:site_name">
                      <meta property="og:image" content="http://imageurl.com">
                      <meta property="og:type" content="article">
                    </head>
                    </html>',
                    :headers => {})
    stub_request(:get, 'http://post.com')
    stub_request(:get, 'http://www.post.com')
    stub_request(:get, 'http://www.posta.com')
    stub_request(:get, 'http://www.postb.com')
    stub_request(:get, 'https://www.post.com')
    stub_request(:get, 'http://send.post.com')
  end

  test 'should be able to destroy a post' do
    creator = create(:user, organization: @org)
    @group.add_member creator
    voter = create(:user, organization: @org)
    @group.add_member voter
    follower = create(:user, organization: @org)
    @group.add_member follower
    commenter = create(:user, organization: @org)
    @group.add_member commenter

    post = create(:post, group: @group, user: creator)
    create(:vote, votable: post, voter: voter)
    create(:follow, followable: post, user: follower)
    create(:comment, commentable: post, user: commenter)
    post.reload
    assert post.destroy
  end

  test "post must belong to user" do
    post = create(:post, group: @group, user: @user)
    assert post.user
  end

  test "should strip strip tag and its contents before saving post obejct" do
    post = create(:post, group: @group, user: @user, message: "<script>I text here should be gone</script><p>Hello</p>")
    post.save
    assert post.message, "<p>Hello</p>"
  end

  test "post can have an empty message" do
    assert build(:post, user: @user, group: @group).save
    assert build(:post, user: @user, group: @group, message: '').save
  end

  test "create resource on post creation with link" do
    assert_difference ->{Resource.count}, 1 do
      post = create(:post, group: @group, user: @user, message: 'some message text and a link http://www.post.com')
    end
    assert_difference ->{Resource.count}, 1 do
      post = create(:post, group: @group, user: @user, message: 'some message text and a link https://www.post.com')
    end
    assert_difference ->{Resource.count}, 1 do
      post = create(:post, group: @group, user: @user, message: 'some message text and a link www.post.com')
    end
    assert_difference ->{Resource.count}, 1 do
      post = create(:post, group: @group, user: @user, message: 'some message text and a link http:post.com')
      assert Resource.last.url ,'http://post.com'
    end
    assert_difference ->{Resource.count}, 1 do
      post = create(:post, group: @group, user: @user, message: 'some message text and a link send.post.com')
      assert Resource.last.url ,'http://send.post.com'
    end
    assert_no_difference ->{Resource.count} do
      post = create(:post, group: @group, user: @user, message: 'some message text and a link http://')
    end
  end

  test "do not create resource on post creation without link" do
    assert_no_difference -> {Resource.count} do
      post = create(:post, group: @group, user: @user, message: 'message')
    end
  end

  test "populate resource fields correctly for post with link" do
    url = 'http://url.com'
    post = create(:post, group: @group, user: @user, message: url)

    resource = post.resource
    assert_not_nil resource

    assert_equal resource.url, url
    assert_equal resource.title, 'title'
    assert_equal resource.type, 'Article'

    image_url = 'http://imageurl.com'
    assert_equal image_url, resource.image_url

    assert_equal resource.site_name, 'og:site_name'

  end

  test "create duplicate resource for an existing url just link" do
    resource = create(:resource, url: 'http://url.com')
    assert_difference -> {Resource.count} do
      post = create(:post, message: resource.url, user: @user, group: @group)
      assert_not_nil post.resource
    end
    r = Resource.last
    assert_equal resource.url, r.url
  end

  test "should create tags for post with tags" do
    message = '#tag1 #tag2 some text'
    post = create(:post, user: @user, group: @group, message: message)
    assert_equal post.tags.count, 2
    assert_equal post.tags[0].name, 'tag1'
    assert_equal post.tags[1].name, 'tag2'
  end

  test "should not create tags for post without tags" do
    message = 'no tag here'
    post = create(:post, user: @user, group: @group, message: message)
    assert_equal post.tags.count, 0
  end

  test 'should create tags from both title and message' do
    message = '#tag1 #tag2 some text'
    title = 'some text #titletag some other text'
    post = create(:post, user: @user, group: @group,
                  message: message, title: title)
    assert_equal post.tags.count, 3
    assert_equal post.tags[0].name, 'tag1'
    assert_equal post.tags[1].name, 'tag2'
    assert_equal post.tags[2].name, 'titletag'
  end

  test "post can have votes" do
    user2 = create(:user)
    @group.add_member user2
    post = create(:post, user: @user, group: @group)
    vote = create(:vote, votable: post, voter: user2)
    assert_equal post.upvoted_by.first.id, user2.id
    assert_equal post.upvoted_by.count, 1
  end

  test "user can vote on post only once" do
    user2 = create(:user)
    @group.add_member user2
    post = create(:post, user: @user, group: @group)
    assert build(:vote, votable: post, voter: user2).save
    assert_not build(:vote, votable: post, voter: user2).save
    assert_not build(:vote, votable: post, voter: user2).save
  end

  test "user can vote on multiple posts" do
    user2 = create(:user)
    @group.add_member user2
    post1 = create(:post, user: @user, group: @group)
    post2 = create(:post, user: @user, group: @group)
    assert build(:vote, votable: post1, voter: user2).save
    assert build(:vote, votable: post2, voter: user2).save
    assert_not build(:vote, votable: post2, voter: user2).save
  end

  test "multiple users can vote on same posts" do
    user1 = create(:user)
    user2 = create(:user)
    @group.add_member user1
    @group.add_member user2
    post = create(:post, user: @user, group: @group)
    assert build(:vote, votable: post, voter: user1).save
    assert build(:vote, votable: post, voter: user2).save
  end

  test "posts should cache votes" do
    post = create(:post, user: @user, group: @group)
    create_list(:user, 10).each do |user|
      @group.add_member user
      create(:vote, votable: post, voter: user)
    end
    post.reload
    assert_equal post.votes_count, 10
  end

  test "faculty approved posts" do
    faculty = create(:user)
    @group.add_admin faculty
    post = create(:post, user: @user, group: @group)
    create(:approval, approver: faculty, approvable: post)
    assert post.has_approval?

    # vote by a faculty
    post_not_approved = create(:post, user: @user, group: @group)
    create(:vote, voter: faculty, votable: post_not_approved)
    assert_not post_not_approved.has_approval?

    # no votes, no approvals
    post_no_votes = create(:post, user: @user, group: @group)
    assert_not post_no_votes.has_approval?
  end

  test 'is staff post' do
      create_default_org
    faculty = create(:user)
    @group.add_admin faculty

    post = create(:post, user: @user, group: @group)
    assert_not post.created_by_staff?

    post = create(:post, user: faculty, group: @group)
    assert post.created_by_staff?
  end

  test 'report action on post valid' do
      create_default_org
    faculty = create(:user)
    @group.add_admin faculty

    student_post = create(:post, user: @user, group: @group)
    faculty_post = create(:post, user: faculty, group: @group)

    valid, error = student_post.get_report_action_validity faculty
    assert valid
    assert_equal error, ''

    invalid, error = faculty_post.get_report_action_validity @user
    assert_not invalid
    assert_equal error, 'You cannot report a faculty post'

    create(:report, :spam_report, reportable: student_post, reporter: faculty)
    invalid, error = student_post.get_report_action_validity faculty
    assert_not invalid
    assert_equal error, 'Reported'
  end

  test 'vote action on post valid' do
    faculty = create(:user)
    @group.add_admin faculty

    student_post = create(:post, user: @user, group: @group)
    faculty_post = create(:post, user: faculty, group: @group)

    valid, error = student_post.get_vote_action_validity faculty
    assert valid
    assert_equal error, ''

    invalid, error = student_post.get_vote_action_validity @user
    assert_not invalid
    assert_equal error, 'You cannot upvote your own post'

    create(:vote, voter: faculty, votable: student_post)
    invalid, error = student_post.get_vote_action_validity faculty
    assert_not invalid
    assert_equal error, 'You have already upvoted this post'
  end

  test 'vote validity with user vote array should work' do
    p1 = create(:post, user: @user, group: @group)
    p2 = create(:post, user: @user, group: @group)

    voter = create(:user)
    @group.add_member voter

    create(:vote, voter: voter, votable: p1)

    valid,error = p1.get_vote_action_validity(voter, [p1.id, p2.id])
    assert_not valid
    assert_equal error, 'You have already upvoted this post'
  end

  test 'read_by? functionality should work' do
    post = create(:post, user: @user, group: @group)
    reader = create(:user, organization: @org)
    @group.add_member reader

    #stub ElasticSearch
    stub_request(:get, %r{http://localhost:9200/#{PostRead.index_name}/post_read.*}).        to_return(:status => 200,
                  :body => '{"took":2,"timed_out":false,"_shards":{"total":5,"successful":5,"failed":0},"hits":{"total":1,"max_score":0.0,"hits":[]}}',
                  :headers => {'Content-Type' => 'application/json'},
    )
    assert post.read_by?(reader)

    unreader = create(:user, organization: @org)
    #stub ElasticSearch
    stub_request(:get, %r{http://localhost:9200/#{PostRead.index_name}/post_read.*}).        to_return(:status => 200,
                  :body => '{"took":2,"timed_out":false,"_shards":{"total":5,"successful":5,"failed":0},"hits":{"total":0,"max_score":0.0,"hits":[]}}',
                  :headers => {'Content-Type' => 'application/json'},
    )
    assert_not post.read_by?(unreader)
  end

  test 'should follow on create' do
    post = create(:post, user: @user, group: @group)
    assert_includes post.followers, post.user
  end

  test 'has approved comment should work' do
    post = create(:post, user: @user, group: @group)
    comment = create(:comment, commentable: post, user: @user)
    create(:approval, approver: @faculty, approvable: comment)
    assert post.has_approved_comment?

    post = create(:post, user: @user, group: @group)
    comment = create(:comment, commentable: post, user: @user)
    assert_not post.has_approved_comment?
  end

  test "should show correct number of faculty and student answers" do
    faculty = create(:user, email: 'faculty@stanford.edu')
    ta = create(:user, email: 'poorta@stanford.edu')
    @group.add_admin ta
    @group.add_admin faculty
    q = create(:post, user: @user, group: @group)
    student_answer = create(:answer, commentable: q, user: @user)
    faculty_answer_1 = create(:answer, commentable: q, user: faculty)
    faculty_answer_2 = create(:answer, commentable: q, user: ta)

    q.reload
    assert_equal q.faculty_answer_count, 2
    assert_equal q.student_answer_count, 1
  end

  test 'should get student and instructor answer count' do
    post = create(:post, user: @user, group: @group)
    sa1 = create(:comment, commentable: post, user: @user)
    sa2 = create(:comment, commentable: post, user: @user)
    ia = create(:comment, commentable: post, user: @faculty)

    assert_equal 2, post.student_answer_count
    assert_equal 1, post.faculty_answer_count

    client = create(:client)
    pg = create(:private_group, client: client, resource_group: @group)
    admin = create(:user)
    member = create(:user)
    pg.add_user(user: admin, role: 'admin')
    pg.add_user(user: member, role: 'member')

    p = create(:post, user: admin, group: pg)
    ma = create(:comment, commentable: p, user: member)
    aa1 = create(:comment, commentable: p, user: admin)
    aa2 = create(:comment, commentable: p, user: admin)

    assert_equal 2, p.faculty_answer_count
    assert_equal 1, p.student_answer_count
  end

  test 'is mobile should work' do
    post = create(:post, user: @user, group: @group)
    assert_not post.is_mobile?

    mobile_post = create(:post, user: @user, group: @group, is_mobile: true)
    assert mobile_post.is_mobile?
  end

  test 'creator label should work' do

    # not explicity set, should default to role name
    post = create(:post, user: @user, group: @group)
    assert_equal 'member', post.creator_label

    # explicitly set
    admin = create(:user)
    @group.add_user(user: admin, role: 'admin', label: 'assistant')
    admin_post = create(:post, user: admin, group: @group)
    assert_equal 'assistant', admin_post.creator_label
  end
end

class SlowTest < ActiveSupport::TestCase

  self.use_transactional_fixtures = false

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    WebMock.disable!
    @org = create(:organization)
    @client = create(:client, organization: @org)
    @group = create(:resource_group, client: @client)
    @user = create(:user, organization: @org)
    @faculty = create(:user, organization: @org)
    @group.add_member @user
    @group.add_admin @faculty
  end

  teardown do
    WebMock.enable!
  end

  test 'exact macth on tag search' do
    create_default_org
    create(:post, user: @user, group: @group, message: 'this is a post with tag #robotics and #bluemoon')
    create(:post, user: @user, group: @group, message: 'this is another post with tag #milky_way and #bluemoon')
    create(:post, user: @user, group: @group, message: 'this is yet another post with anther tag #lab123 and #bluemoon')

    Post.reindex

    results = Post.retrieve(filter_params: {group_id: @group.id})
    assert_equal 3, results.count

    results = Post.retrieve(filter_params: {tag_names: 'milk', group_id: @group.id})
    assert_equal 0, results.count

    results = Post.retrieve(filter_params: {tag_names: 'milky', group_id: @group.id})
    assert_equal 0, results.count

    results = Post.retrieve(filter_params: {tag_names: 'milky_way', group_id: @group.id})
    assert_equal 1, results.count

    results = Post.retrieve(filter_params: {tag_names: 'robot', group_id: @group.id})
    assert_equal 0, results.count

    results = Post.retrieve(filter_params: {tag_names: 'robotics', group_id: @group.id})
    assert_equal 1, results.count

    results = Post.retrieve(filter_params: {tag_names: 'lab', group_id: @group.id})
    assert_equal 0, results.count

    results = Post.retrieve(filter_params: {tag_names: 'lab12', group_id: @group.id})
    assert_equal 0, results.count

    results = Post.retrieve(filter_params: {tag_names: 'lab123', group_id: @group.id})
    assert_equal 1, results.count

    results = Post.retrieve(filter_params: {tag_names: 'blue', group_id: @group.id})
    assert_equal 0, results.count

    results = Post.retrieve(filter_params: {tag_names: 'bluemoon', group_id: @group.id})
    assert_equal 3, results.count
  end

  test 'free text search on post title and message' do
    create_default_org
    create(:post, user: @user, group: @group, message: 'this is a robotics post')
    create(:post, user: @user, group: @group, title: 'my fav. robotic title', message: 'this is another post with tag #milky_way and #bluemoon')

    Post.reindex

    results = Post.retrieve(filter_params: {group_id: @group.id})
    assert_equal 2, results.count

    results = Post.retrieve(search_query: 'robot', filter_params: {group_id: @group.id})
    assert_equal 2, results.count

    results = Post.retrieve(search_query: 'robots', filter_params: {group_id: @group.id})
    assert_equal 2, results.count

    results = Post.retrieve(search_query: 'robotics', filter_params: {group_id: @group.id})
    assert_equal 2, results.count
  end

  test 'search results with posts :flagged/:reported' do
    create_default_org
    flagged   = create(:post, user: @user, group: @group, message: 'Post 1', hidden: true)
    unflagged = create(:post, user: @user, group: @group, message: 'Post 2')

    Post.reindex

    results = Post.retrieve(filter_params: { reported: true })
    hit_ids = results.hits.collect{ |h| h['_id'].to_i }

    assert_equal 1, results.count
    assert_includes hit_ids, flagged.id
    refute_includes hit_ids, unflagged.id
  end

  test 'search results with posts :follower_ids' do
    create_default_org
    user = create :user
    @group.add_member user
    following   = create(:post, user: @user, group: @group, message: 'Post 1')
    Follow.create(user: @user, followable: following)
    unfollowing = create(:post, user: user, group: @group, message: 'Post 2')

    following.reload
    Post.reindex

    results = Post.retrieve(filter_params: { follower_ids: @user.id })
    hit_ids = results.hits.collect{ |h| h['_id'].to_i }

    assert_equal 1, results.count
    assert_includes hit_ids, following.id
    refute_includes hit_ids, unfollowing.id
  end

  test 'search results with posts :user_id' do
    create_default_org
    user = create :user
    @group.add_member user
    post1 = create(:post, user: @user, group: @group, message: 'Post 1')
    Follow.create(user: @user, followable: post1)
    post2 = create(:post, user: user, group: @group, message: 'Post 2')

    post1.reload
    Post.reindex

    results = Post.retrieve(filter_params: { user_id: @user.id })
    hit_ids = results.hits.collect{ |h| h['_id'].to_i }

    assert_equal 1, results.count
    assert_includes hit_ids, post1.id
    refute_includes hit_ids, post2.id
  end

  test 'should return default page limit for different postables' do
    channel_post_limit = Post.get_postable_default_page_limit("Channel")
    group_post_limit = Post.get_postable_default_page_limit("Group")

    assert_equal Post::DEFAULT_POSTABLE_PER_PAGE_LIMIT["Channel"], channel_post_limit
    assert_equal Post::DEFAULT_POSTABLE_PER_PAGE_LIMIT["Group"], group_post_limit
  end

  test 'should return default page limit for random postable' do
    random_postable_post_limit = Post.get_postable_default_page_limit("RandomPostable")
    assert_equal Post::DEFAULT_PER_PAGE, random_postable_post_limit
  end

end
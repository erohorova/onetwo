require 'test_helper'

class AnnouncementTest < ActiveSupport::TestCase
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    Post.any_instance.stubs(:p_read_by_creator)
    @faculty1 = create(:user)
    @group1 = create(:group)
    @group1.add_admin(@faculty1)
    @a = create(:announcement, group: @group1, user: @faculty1)
  end

  test "instructor of group can create announcement" do
    assert @a.errors.empty?
  end

  test "#notify_group send email for announcement " do
    AnnouncementJob.unstub(:perform_later)
    AnnouncementJob.expects(:perform_later).once.returns(true)
    assert @a.notify_group

    #skip notification mail if group.forum_notifications_enabled is false
    group2 = create(:group, forum_notifications_enabled: false)
    group2.add_admin(@faculty1)
    a = create(:announcement, group: group2, user: @faculty1)
    assert_nil a.notify_group
  end

  test "student cannot create announcement" do
    create_default_org
    user1 = create :user
    group1 = create(:group)
    group1.add_member(user1)
    assert_not build(:announcement, group: group1, user: user1).save
  end

  class FullCommitTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false

    setup do
      Post.any_instance.stubs(:p_read_by_creator)
      User.any_instance.stubs(:reindex_async).returns(true)
      NotificationGeneratorJob.unstub(:perform_later)

      @f = create(:user)
      @u = create(:user)
      @g = create(:group)
      @g.add_admin(@f)
      @g.add_member(@u)
    end

    test 'integration test for mobile notification' do
      skip("INTERMITTENT FAILURE ON TRAVIS")
      NotificationGeneratorJob.expects(:perform_later).once
      a = create(:announcement, user: @f, group: @g)
    end
  end
end

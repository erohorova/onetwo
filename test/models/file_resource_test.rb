require 'test_helper'

class FileResourceTest < ActiveSupport::TestCase

  test 'should create multiple image sizes' do
    fr = build(:file_resource)
    valid_sizes = %w[small medium large random]
    fr.attachment.styles.stringify_keys.assert_valid_keys(valid_sizes)
  end

  test 'should make absolute url out of file path' do
    fr = create(:file_resource)
    assert_match 'http', fr.file_absolute_url
  end

  test 'should trigger remove file from filestack job on destroy' do
    filestack = {
      'filename': 'Strix_nebulosaRB.jpg',
      'mimetype': 'image/jpeg',
      'size': 106176,
      'source': 'local_file_system',
      'url': 'https://cdn.filestackcontent.com/yT4BBpg7T5efeiriQv37',
      'handle': 'yT4BBpg7T5efeiriQv37',
      'status': 'Stored',
      'key': 'VYgpc6fnTOKJkFWIkzti_Strix_nebulosaRB.jpg',
      'container': 'your_bucket'
    }.with_indifferent_access
    skip
    fr = create(:file_resource, filestack: filestack, filestack_handle: filestack['handle'])
    RemoveFileFromFilestackJob.expects(:perform_later).with(handle: fr.filestack_handle, card_id: fr.attachable_id).once
    fr.destroy
  end

  test 'should not trigger remove file from filestack job on destroy if handle empty' do
    filestack = {
      'filename': 'Strix_nebulosaRB.jpg',
      'mimetype': 'image/jpeg',
      'size': 106176,
      'source': 'local_file_system',
      'url': 'https://cdn.filestackcontent.com/yT4BBpg7T5efeiriQv37',
      'handle': '',
      'status': 'Stored',
      'key': 'VYgpc6fnTOKJkFWIkzti_Strix_nebulosaRB.jpg',
      'container': 'your_bucket'
    }.with_indifferent_access

    fr = create(:file_resource, filestack: filestack, filestack_handle: filestack['handle'])
    RemoveFileFromFilestackJob.expects(:perform_later).never
    fr.destroy
  end


  test 'should not trigger RemoveFileFromFilestackJob on fileresource destroy if handle is present in default card image list' do
    filestack = {
      'filename': 'Strix_nebulosaRB.jpg',
      'mimetype': 'image/jpeg',
      'size': 106176,
      'source': 'local_file_system',
      'url': 'https://cdn.filestackcontent.com/fXWEu1IQpGFXs0WKCmhB',
      'handle': 'fXWEu1IQpGFXs0WKCmhB',
      'status': 'Stored',
      'key': 'VYgpc6fnTOKJkFWIkzti_Strix_nebulosaRB.jpg',
      'container': 'your_bucket'
    }.with_indifferent_access

    fr = create(:file_resource, filestack: filestack, filestack_handle: filestack['handle'])

    assert_equal true, CARD_STOCK_IMAGE.any? {|h| h[:handle] == fr.filestack_handle }

    RemoveFileFromFilestackJob.expects(:perform_later).never
    fr.destroy
  end

  test '#document? confirms valid mime type for an attachment' do
    #### Allows standard documents mentioned below ####

    # RTF
    fr = build :file_resource, attachment: Rack::Test::UploadedFile.new('test/fixtures/file_resources/rtf_file.rtf', 'application/rtf')
    assert fr.valid_mime_type?

    # PDF
    fr = build :file_resource, attachment: Rack::Test::UploadedFile.new('test/fixtures/file_resources/pdf_file.pdf', 'application/pdf')
    assert fr.valid_mime_type?

    # MS Word
    fr = build :file_resource, attachment: Rack::Test::UploadedFile.new('test/fixtures/file_resources/document.doc', 'application/msword')
    assert fr.valid_mime_type?

    # XLS
    fr = build :file_resource, attachment: Rack::Test::UploadedFile.new('test/fixtures/file_resources/excel.xls', 'application/excel')
    assert fr.valid_mime_type?

    # PPTX
    fr = build :file_resource, attachment: Rack::Test::UploadedFile.new('test/fixtures/file_resources/ppt.pptx', 'application/vnd.openxmlformats-officedocument.presentationml.presentation')
    assert fr.valid_mime_type?

    # PPT
    fr = build :file_resource, attachment: Rack::Test::UploadedFile.new('test/fixtures/file_resources/ppt_file.ppt', 'application/vnd.ms-powerpoint')
    assert fr.valid_mime_type?

    # XLSX
    fr = build :file_resource, attachment: Rack::Test::UploadedFile.new('test/fixtures/file_resources/excel.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    assert fr.valid_mime_type?

    # DOCX
    fr = build :file_resource, attachment: Rack::Test::UploadedFile.new('test/fixtures/file_resources/docx_file.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document')
    assert fr.valid_mime_type?

    # JSON
    fr = build :file_resource, attachment: Rack::Test::UploadedFile.new('test/fixtures/file_resources/json.json', 'application/json')
    assert fr.valid_mime_type?

    # CSV
    fr = build :file_resource, attachment: Rack::Test::UploadedFile.new('test/fixtures/file_resources/csv_file.csv', 'text/comma-separated-values')
    assert fr.valid_mime_type?

    # HTML
    fr = build :file_resource, attachment: Rack::Test::UploadedFile.new('test/fixtures/file_resources/csv_file.csv', 'text/html')
    assert fr.valid_mime_type?

    #### Rejects files mentioned below ####
    # SWF
    fr = build :file_resource, attachment: Rack::Test::UploadedFile.new('test/fixtures/file_resources/flash.swf', 'application/x-shockwave-flash')
    refute fr.valid_mime_type?

    # PHP
    fr = build :file_resource, attachment: Rack::Test::UploadedFile.new('test/fixtures/file_resources/shell.PHP', 'application/x-httpd-php')
    refute fr.valid_mime_type?

    # RAR
    fr = build :file_resource, attachment: Rack::Test::UploadedFile.new('test/fixtures/file_resources/sample_rar.rar', 'application/x-rar-compressed')
    refute fr.valid_mime_type?

    # DEB
    fr = build :file_resource, attachment: Rack::Test::UploadedFile.new('test/fixtures/file_resources/sample_deb.deb', 'application/x-debian-package')
    refute fr.valid_mime_type?
  end

  test 'should return boolean value for file type check' do
    pdf_file_res = create(:file_resource, attachment_content_type: 'application/pdf')
    csv_file_res = create(:file_resource, attachment_content_type: 'application/csv')
    xls_file_res = create(:file_resource, attachment_content_type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    doc_file_res = create(:file_resource, attachment_content_type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document')
    pptx_file_res = create(:file_resource, attachment_content_type: 'application/vnd.openxmlformats-officedocument.presentationml.presentation')
    ppt_file_res = create(:file_resource, attachment_content_type: 'application/vnd.ms-powerpoint')
    html_file_res = create(:file_resource, attachment_content_type: 'text/html')
    json_file_res = create(:file_resource, attachment_content_type: 'application/json')
    rtf_file_res = create(:file_resource, attachment_content_type: 'application/rtf')
    webp_file_res = create(:file_resource, attachment_content_type: 'image/webp')
    audio_file_res = create(:file_resource, attachment_content_type: 'audio/mpeg')
    audio_mp4_file_res = create(:file_resource, attachment_content_type: 'audio/mp4')
    odt_text_file_res = create(:file_resource, attachment_content_type: 'application/vnd.oasis.opendocument.text')
    zip_file_res = create(:file_resource, attachment_content_type: 'application/x-zip-compressed')

    assert pdf_file_res.pdf?
    assert csv_file_res.csv?
    assert xls_file_res.xls?
    assert doc_file_res.doc?
    assert pptx_file_res.ppt?
    assert ppt_file_res.ppt?
    assert html_file_res.html?
    assert json_file_res.json?
    assert rtf_file_res.rtf?
    assert webp_file_res.image?
    assert audio_file_res.audio?
    assert audio_mp4_file_res.audio?
    assert odt_text_file_res.text?
    assert zip_file_res.zip?

  end

  test 'should return correct file type' do
    pdf_file_res = create(:file_resource, attachment_content_type: 'application/pdf')
    jpeg_file_res = create(:file_resource, attachment_content_type: 'image/jpeg')
    webp_file_res = create(:file_resource, attachment_content_type: 'image/webp')
    mp3_file_res = create(:file_resource, attachment_content_type: 'audio/mp3')
    mp4_file_res = create(:file_resource, attachment_content_type: 'audio/mp4')
    mpeg_file_res = create(:file_resource, attachment_content_type: 'audio/mpeg')
    csv_file_res = create(:file_resource, attachment_content_type: 'application/csv')
    xls_file_res = create(:file_resource, attachment_content_type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    doc_file_res = create(:file_resource, attachment_content_type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document')
    pptx_file_res = create(:file_resource, attachment_content_type: 'application/vnd.openxmlformats-officedocument.presentationml.presentation')
    ppt_file_res = create(:file_resource, attachment_content_type: 'application/vnd.ms-powerpoint')
    text_file_res = create(:file_resource, attachment_content_type: 'text/plain')
    odt_file_res = create(:file_resource, attachment_content_type: 'application/vnd.oasis.opendocument.text')
    html_file_res = create(:file_resource, attachment_content_type: 'text/html')
    json_file_res = create(:file_resource, attachment_content_type: 'application/json')
    rtf_file_res = create(:file_resource, attachment_content_type: 'application/rtf')
    zip_comp_file_res = create(:file_resource, attachment_content_type: 'application/x-zip-compressed')
    zip_file_res = create(:file_resource, attachment_content_type: 'application/zip')

    assert_equal 'image', jpeg_file_res.file_type
    assert_equal 'image', webp_file_res.file_type
    assert_equal 'audio', mp3_file_res.file_type
    assert_equal 'audio', mp4_file_res.file_type
    assert_equal 'audio', mpeg_file_res.file_type
    assert_equal 'zip', zip_comp_file_res.file_type
    assert_equal 'zip', zip_file_res.file_type

    assert_equal ['file'], [pdf_file_res, csv_file_res, xls_file_res, doc_file_res, pptx_file_res,
      ppt_file_res,text_file_res,odt_file_res, html_file_res, json_file_res, rtf_file_res].map(&:file_type).uniq
  end
end

require 'test_helper'

class UserSessionTest < ActiveSupport::TestCase

  test 'should assign uuid' do
    us = create(:user_session)
    refute us.uuid.nil?
  end

  test 'new user session' do
    now = Time.now.utc
    us = create(:user_session, start_time: now - 5.minutes, end_time: now - 4.minutes)

    assert_no_difference ->{UserSession.count} do
      assert_equal 240, UserSession.update_session(us.user_id, now)
    end

    us.reload
    assert_equal now.to_i, us.end_time.to_i

    assert_difference ->{UserSession.count} do
      assert_equal 5, UserSession.update_session(us.user_id, now + 30.minutes)
    end

    new_session = UserSession.last
    assert_equal us.user_id, new_session.user_id
    assert_equal (now + 30.minutes).to_i, new_session.start_time.to_i
    assert_equal (now + 30.minutes + 5.seconds).to_i, new_session.end_time.to_i
  end

  test 'should pick up last session time of user' do
    now = Time.now.utc
    user = create(:user)
    previous_session = create(:user_session, user_id: user.id, start_time: now - 30.minutes, end_time: now - 20.minutes)
    session = create(:user_session, user_id: user.id, start_time: now - 10.minutes, end_time: now - 5.minutes)

    # picks up last session time of user ie (now - 5.minutes)
    assert_no_difference -> {UserSession.count} do
      assert_equal 300, UserSession.update_session(user.id, now)
    end
    session.reload
    assert_equal now.to_i, session.end_time.to_i
  end

  test 'very first session' do
    now = Time.now.utc
    user = create(:user)

    assert_difference ->{UserSession.count} do
      assert_equal 5, UserSession.update_session(user.id, now)
    end
  end

  test 'do nothing if event behind current end time' do
    now = Time.now.utc
    user = create(:user)
    session = create(:user_session, user_id: user.id, start_time: now - 5.minutes, end_time: now + 5.minutes)
    assert_no_difference ->{UserSession.count} do
      assert_equal 0, UserSession.update_session(user.id, now)
    end
    session.reload
    assert_equal (now+5.minutes).to_i, session.end_time.to_i
  end
end

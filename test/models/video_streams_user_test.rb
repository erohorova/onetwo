require 'test_helper'

class VideoStreamsUserTest < ActiveSupport::TestCase

  setup do
    @org = create :organization
    @user = create :user, organization: @org
    @card = create :card, organization: @org, author: @user
    @video_stream = VideoStream.create(
      card: @card,
      organization: @org,
      start_time: Time.now,
      name: "foo"
    )
    # create a record which is used by validation tests
    @video_streams_user = VideoStreamsUser.create(user: @user, video_stream: @video_stream)
  end

  should belong_to :video_stream
  should belong_to :user
  should validate_presence_of :user
  should validate_presence_of :video_stream
  should validate_uniqueness_of(:user_id).scoped_to(:video_stream_id)

  test "record_start_viewing_metric" do
    metadata = { platform: "foo", user_id: @user.id }
    RequestStore.stubs(:read).with(:request_metadata).returns(metadata)
    stub_time = Time.now
    RequestStore.stubs(:read).with(:request_metadata).returns(metadata)
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::CardMetricsRecorder",
      org: Analytics::MetricsRecorder.org_attributes(@org),
      card: Analytics::MetricsRecorder.card_attributes(@card),
      event: 'card_video_stream_started_viewing',
      actor: Analytics::MetricsRecorder.user_attributes(@user),
      timestamp: stub_time.to_i,
      additional_data: metadata
    )
    Timecop.freeze(stub_time) do
      @video_streams_user.run_callbacks :commit
    end
  end

  test "record_stop_viewing_metric" do
    stub_time = Time.now
    metadata = { user_id: @user.id, platform: "foo" }
    RequestStore.stubs(:read).with(:request_metadata).returns(metadata)
    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::CardMetricsRecorder",
      timestamp: stub_time.to_i,
      event: "card_video_stream_stopped_viewing",
      actor: Analytics::MetricsRecorder.user_attributes(@user),
      additional_data: metadata,
      card: Analytics::MetricsRecorder.card_attributes(@card),
      org: Analytics::MetricsRecorder.org_attributes(@org),
    )
    Timecop.freeze(stub_time) do
      @video_streams_user.destroy!
    end
  end
end
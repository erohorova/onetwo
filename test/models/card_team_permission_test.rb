require 'test_helper'

class CardTeamPermissionTest < ActiveSupport::TestCase
  should have_db_column(:card_id).of_type(:integer)
  should have_db_column(:team_id).of_type(:integer)
  should have_db_column(:show).of_type(:boolean)

  should belong_to(:card)
  should belong_to(:team)
end

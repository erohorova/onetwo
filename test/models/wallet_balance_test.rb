require 'test_helper'

class WalletBalanceTest < ActiveSupport::TestCase
  should belong_to(:user)

  should validate_presence_of(:amount)
  should validate_presence_of(:user)
end

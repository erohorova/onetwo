require 'test_helper'

class UploadTest < ActiveSupport::TestCase

  setup do
    @user = create(:user, email: 'a@a.com')
    @admin = create(:user, email: 'a@edcast.com')
  end

  test 'should validate file present' do
    upload = Upload.new(sender: @admin, file: nil)
    assert_not upload.save
  end
end

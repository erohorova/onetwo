require 'test_helper'

class CareerAdvisorTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper
  should belong_to :organization
  should have_many :career_advisor_cards

  setup do
    @org = create(:organization)
    @career_advisor = create(:career_advisor, organization: @org)
  end

  test 'should return error if skill value not default skill in career_advisor' do
    career_advisor = CareerAdvisor.new(skill: 'data', links: 'http://link.com')
    assert_not career_advisor.valid?
  end
  
  class OrgCareerAdvisorMetricsRecorderTest < ActiveSupport::TestCase
    
    test "should push influx org_career_advisor_created event when career advisor is created" do
      TestAfterCommit.enabled = true
      org             = create :organization
      actor           = create(:user, organization: org, organization_role: 'admin')
      career_advisor  = CareerAdvisor.new(
        id:           10000001 , 
        organization: org, 
        skill:        'data_scientist', 
        links:        ["https://qa.cmnetwork.co"]
      )

      stub_time       = Time.now
      metadata        = { user_id: actor.id }    
      
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries(
          recorder:         "Analytics::OrgCareerAdvisorMetricsRecorder",
          event:            "org_career_advisor_created",
          actor:            Analytics::MetricsRecorder.user_attributes(actor),
          org:              Analytics::MetricsRecorder.org_attributes(org),
          career_advisor:   Analytics::MetricsRecorder.career_advisor_attributes(career_advisor),
          timestamp:        Time.now.to_i,
          additional_data:  metadata
          )
        )
      Timecop.freeze(stub_time) do
        career_advisor.save
      end    
    end

    test "should push influx org_career_advisor_deleted event when career advisor is deleted" do
      TestAfterCommit.enabled = true
      org             = create :organization
      actor           = create(:user, organization: org, organization_role: 'admin')
      career_advisor  = CareerAdvisor.create(
        id:           10000001, 
        organization: org, 
        skill:        'data_scientist', 
        links:         ["https://qa.cmnetwork.co"]
      )
      stub_time       = Time.now
      metadata        = { user_id: actor.id }
      
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries(
          recorder:         "Analytics::OrgCareerAdvisorMetricsRecorder",
          event:            "org_career_advisor_deleted",
          actor:            Analytics::MetricsRecorder.user_attributes(actor),
          org:              Analytics::MetricsRecorder.org_attributes(org),
          career_advisor:   Analytics::MetricsRecorder.career_advisor_attributes(career_advisor),
          timestamp:        Time.now.to_i,
          additional_data:  metadata
          )
        )
      Timecop.freeze(stub_time) do
        career_advisor.destroy
      end    
    end

    test "should push influx org_career_advisor_edited event when career advisor is edited" do
      TestAfterCommit.enabled = true
      org             = create :organization
      actor           = create(:user, organization: org, organization_role: 'admin')
      career_advisor  = CareerAdvisor.create(
        id:           10000001, 
        organization: org, 
        skill:        'data_scientist', 
        links:        ["https://qa.cmnetwork.co"]
      )
      stub_time       = Time.now
      metadata        = { user_id: actor.id }

      career_advisor.links = 'https://qa.cmnetworkedited.co'
      
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries(
          recorder:         "Analytics::OrgCareerAdvisorMetricsRecorder",
          event:            "org_career_advisor_edited",
          actor:            Analytics::MetricsRecorder.user_attributes(actor),
          org:              Analytics::MetricsRecorder.org_attributes(org),
          career_advisor:   Analytics::MetricsRecorder.career_advisor_attributes(career_advisor),
          timestamp:        Time.now.to_i,
          additional_data:  metadata,
          changed_column:   'links',
          old_val:          '["https://qa.cmnetwork.co"]',
          new_val:          'https://qa.cmnetworkedited.co'
          )
        )
      Timecop.freeze(stub_time) do
        career_advisor.save
      end    
    end
  end
end
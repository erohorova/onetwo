require 'test_helper'

class RoleTest < ActiveSupport::TestCase
  setup do
    @org = create(:organization)
  end

  should belong_to :organization
  should have_many :role_permissions
  should have_many :users

  test 'should correctly validate role name in scope of organization' do
    org = create(:organization)
    role_name = 'SME'
    create(:role, name: 'sme', organization: @org)

    new_role_valid = Role.new(name: role_name, organization_id: org.id).valid?
    new_role_invalid = Role.new(name: role_name, organization_id: @org.id).valid?

    assert new_role_valid
    refute new_role_invalid
  end

  test '#update should keep default_name for standard role' do
    Role.create_master_roles(@org)
    sme_role = Role.sme
    assert_equal 1, sme_role.count
    sme_role.first.update_attributes(name: 'SMR')

    assert_equal 'SMR', Role.sme.first.name
    assert_equal 1, Role.sme.count
  end

  test '#update should set default_name eq name for non standard role' do
    role = create(:role, name: 'sample_name', organization: @org)
    assert_equal 'sample_name', role.default_name

    role.update_attributes(name: 'renamed_name')
    assert_equal 'renamed_name', role.reload.default_name
  end

  test 'roles should have proper default names' do
    Role.create_master_roles(@org)
    default_names = Role.all.pluck(:default_name).uniq
    assert_same_elements Role::MASTER_ROLES, default_names
  end

  test 'should record org_role_created event on create' do
    skip
    # NOTE, can't figure out how to get test to pass :/

    TestAfterCommit.enabled = true

    actor = create :user, organization: @org
    metadata = { user_id: actor.id }
    RequestStore.store[:request_metadata] = metadata

    stub_time = Time.now
    Time.stubs(:now).returns stub_time

    role = Role.new(
      name: "sample_name",
      organization: @org,
      master_role: true,
      threshold: 5
    )

    $x = recorder_args = hash_including(
      recorder: "Analytics::OrgRoleMetricsRecorder",
      timestamp: stub_time.to_i,
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      org: Analytics::MetricsRecorder.org_attributes(@org),
      event: "org_role_created"
    )

    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(:perform_later).with(recorder_args)

    role.save!
  end

  test 'should record org_role_deleted event on delete' do
    skip # TODO
  end

end

require 'test_helper'

class CardBadgeTest < ActiveSupport::TestCase
  should have_db_column(:organization_id).of_type(:integer)
  should have_db_column(:type).of_type(:string)

  should belong_to(:organization)

  should validate_presence_of(:organization_id)

  should have_many :card_badgings
  should have_many :badgings
end

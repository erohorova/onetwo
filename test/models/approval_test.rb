require 'test_helper'

class ApprovalTest < ActiveSupport::TestCase
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    Post.any_instance.stubs(:p_read_by_creator)
    @org = create(:organization)
    @faculty = create(:user, organization: @org)
    @user = create(:user, organization: @org)

    @group = create(:resource_group, organization: @org)
    @group.add_member @user
    @group.add_admin @faculty

    @post = create(:post, group: @group, user: @user)
    @comment = create(:comment, commentable: @post, user: @user)
  end

  test 'should touch question on approval creation' do
    updated_at_post = @post.updated_at
    updated_at_comment = @comment.updated_at

    create(:approval, approver: @faculty, approvable: @post)
    create(:approval, approver: @faculty, approvable: @comment)

    assert_not_equal updated_at_post, @post.reload.updated_at
    assert_not_equal updated_at_comment, @comment.reload.updated_at
  end

  test 'faculty should be able to approve' do
    approval = build(:approval, approver: @faculty, approvable: @post)
    assert approval.save

    approval = build(:approval, approver: @faculty, approvable: @comment)
    assert approval.save
  end

  test 'student should not be able to approve' do
    create_default_org
    user = create(:user)
    @group.add_member user

    assert_not(build(:approval, approver: user, approvable: @post).save)
  end

  test 'only one approval per item' do
    create(:approval, approver: @faculty, approvable: @post)
    assert_not(build(:approval, approver: @faculty, approvable: @post).save)
  end

  test 'should increment stars after approval' do
    assert_difference ->{@user.score(@group)[:stars]} do
      create(:approval, approver: @faculty, approvable: @post)
    end
  end

  test 'get approval should work' do
    p1 = create(:post, user: @user, group: @group)
    create(:approval, approver: @faculty, approvable: @post)

    approved_ids = Approval.get_approvals(approvable_ids: [p1.id, @post.id],
                                          approvable_type: 'Post')
    assert_equal approved_ids, [@post.id]

    blank_result = Approval.get_approvals(approvable_ids: [],
                                          approvable_type: 'Post')
    assert_equal blank_result, []

    # bad type
    bad_type = Approval.get_approvals(approvable_ids: [p1.id, @post.id])
    assert_equal bad_type, []
  end

  test "should not update stars for anonymous post" do
    post = create(:post, user: @user, anonymous: true, group: @group)

    score = @user.score(@group)['score']

    @approval = Approval.new
    @approval.approvable = post
    @approval.approver = @faculty
    @approval.save!

    newscore = @user.score(@group)['score']
    assert_equal score, newscore
  end

  test 'create and delete new active stream when user approve something' do
    stub_request(:put, %r{http://localhost:9200/posts_test/question/.*})

    EdcastActiveJob.unstub :perform_later
    StreamCreateJob.unstub :perform_later

    question = create(:question, user: @user, group: @group)
    user2 = create(:user)
    @group.add_member user2
    answer = create(:answer, commentable: question, user: user2)
    qapproval = create(:approval, approver: @faculty, approvable: question)
    aapproval = create(:approval, approver: @faculty, approvable: answer)
    assert_difference -> {Stream.count} do
      qapproval.run_callbacks(:commit)
      stream = Stream.last
      assert_equal 'approved', stream.action
      assert_equal question, stream.item
      assert_equal @faculty, stream.user
      assert_equal @group, stream.group
    end
    assert_difference -> {Stream.count} do
      aapproval.run_callbacks(:commit)
      stream = Stream.last
      assert_equal 'approved', stream.action
      assert_equal answer, stream.item
      assert_equal @faculty, stream.user
      assert_equal @group, stream.group
    end

    # deleting the question should delete both approval streams
    assert_difference -> {Stream.count}, -2 do
      question.destroy
    end
  end
end

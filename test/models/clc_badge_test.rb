require 'test_helper'

class ClcBadgeTest < ActiveSupport::TestCase
  should have_db_column(:organization_id).of_type(:integer)
  should have_db_column(:type).of_type(:string)

  should belong_to(:organization)

  should validate_presence_of(:organization_id)

  should have_many :clc_badgings

  test 'should destroy dependent clc_badgings when destroying clc_badge' do
  	org = create :organization
  	clc = create :clc, entity_id: org.id, entity_type: "Organization", name: "Test_CLC"
  	clc_badge = create(:clc_badge, organization_id: org.id)
  	clc_badging = create(:clc_badging, clc_badge: clc_badge, clc: clc)

  	clc_badge.destroy
  	assert_raise(ActiveRecord::RecordNotFound) { clc_badging.reload }
  	assert_equal ClcBadging.count, 0
  end
end

require 'test_helper'

class EventsUserTest < ActiveSupport::TestCase

  should belong_to(:event)
  should belong_to(:user)

  should validate_inclusion_of(:rsvp).in_array(['yes', 'no', 'maybe'])

  test 'update webex meeting' do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    WebexApi::Client.any_instance.stubs(:create_meeting).returns("123")
    WebexApi::Client.any_instance.stubs(:get_meeting_join_url).returns("www.example.com")
    WebexApi::Client.any_instance.stubs(:get_meeting_host_url).returns("www.example1.com")
    @group, @user = create_gu
    org = create(:organization)
    @group, @user = create_gu(user_opts: {}, group_opts: {organization_id: org.id, client_resource_id: 1}, role: 'member')
    u = create(:user, organization: org)
    u1 = create(:user, organization: org)
    @group.add_member(u)
    webex_config = create(:webex_config, organization: @group.organization)
    WebexApi::Client.any_instance.expects(:add_attendee_to_meeting).twice
    event = create(:event, user: @user, group: @group, conference_type: "webex", webex_id: "123", webex_password: "password", conference_email: "test@ex.com")
    eu = create(:events_user, user: u, event: event, rsvp: "yes")

    WebexApi::Client.any_instance.expects(:delete_attendee_from_meeting).once
    eu = create(:events_user, user: u1, event: event, rsvp: "no")

    WebexApi::Client.any_instance.unstub(:create_meeting)
    WebexApi::Client.any_instance.unstub(:get_meeting_join_url)
    WebexApi::Client.any_instance.unstub(:get_meeting_host_url)
  end
end

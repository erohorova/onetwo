require 'test_helper'
class CommentReportingTest < ActiveSupport::TestCase
  should have_db_column(:comment_id).of_type(:integer)
  should have_db_column(:user_id).of_type(:integer)
  should have_db_column(:reason).of_type(:string)
  should have_db_column(:deleted_at).of_type(:datetime)

  should belong_to :comment
  should belong_to :user

  test 'validates uniqueness of comment scoped to user' do
    org = create(:organization)
    card = create(:card, author_id: nil, organization: org)
    commenting_user, reporting_user = create_list(:user, 2, organization: org)
    comment = create(:comment, commentable: card, user: commenting_user)

    comment_rep_1 = create(:comment_reporting, comment: comment, user: reporting_user)
    comment_rep_2 = build(:comment_reporting, comment: comment, user: reporting_user)

    refute comment_rep_2.valid?
  end
end

require 'test_helper'

class CardsRatingTest < ActiveSupport::TestCase

  setup do
    org = create(:organization)
    CardsRating.create(
      card: create(:card, author_id: nil, organization: org),
      user: create(:user, organization: org)
    )
  end

  should belong_to :user
  should belong_to :card
  should validate_presence_of :user
  should validate_presence_of(:card)
  should validate_uniqueness_of(:card).scoped_to(:user_id)

  test 'level should allowed to take defined values' do
    card_rating = build(:cards_rating, rating: 3, level: "beginner")
    assert card_rating.valid?

    card_rating1 = build(:cards_rating, rating: 3, level: "dummy")
    refute card_rating1.valid?

    card_rating2 = build(:cards_rating, level: "beginner")
    assert card_rating2.valid?

    card_rating3 = build(:cards_rating, rating: 3)
    assert card_rating3.valid?

    card_rating4 = build(:cards_rating, rating: 9000)
    refute card_rating4.valid?
  end

  test "record_card_rated_event gets called on create if enabled" do
    org = create(:organization)
    card = create(:card, organization: org, author_id: nil)
    user = create(:user, organization: org)

    cards_rating = CardsRating.new(card: card, user: user)
    cards_rating.expects(:record_card_rated_event)
    cards_rating.run_callbacks(:commit) { cards_rating.save! }
  end

  test "#influx recorder is called if rating/level changed" do
    CardsRating.any_instance.unstub :record_card_rated_event
    org = create :organization
    user = create(:user, organization: org)
    card = create(:card, organization: org, author: user)
    cards_rating = CardsRating.new(
      card: card,
      user: user,
      rating: 5,
      level: "beginner"
    )
    metadata = { foo: "bar" }
    stub_time = Time.now
    RequestStore.expects(:read).with(:request_metadata).returns metadata
    # This recorder uses the card's author as the "actor".
    # It doesn't determine the actor based off the RequestStore metadata.
    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::CardRatedMetricsRecorder",
      timestamp: stub_time.to_i,
      event: "card_relevance_rated",
      org: Analytics::MetricsRecorder.org_attributes(org),
      card: Analytics::MetricsRecorder.card_attributes(card),
      actor: Analytics::MetricsRecorder.user_attributes(user),
      additional_data: metadata,
      cards_rating: Analytics::MetricsRecorder.cards_rating_attributes(cards_rating).merge(
        "updated_at" => stub_time.to_i,
        "created_at" => stub_time.to_i,
      )
    )
    Timecop.freeze stub_time do
      cards_rating.save!
    end
  end

  test "#record_card_rated_event calls influx recorder on update if rating/level changed" do
    Analytics::MetricsRecorderJob.stubs(:perform_later)
    org = create :organization
    user = create(:user, organization: org)
    card = create(:card, organization: org, author: user)
    cards_rating = CardsRating.create(
      card: card,
      user: user,
      rating: 5,
      level: "beginner"
    )
    metadata = { foo: "bar" }
    stub_time = Time.now
    Time.stubs(:now).returns stub_time
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    # Recorder is called when rating is changed
    new_rating = 4
    Analytics::MetricsRecorderJob.unstub(:perform_later)
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        recorder: 'Analytics::CardRatedMetricsRecorder',
        timestamp: stub_time.to_i,
        org: Analytics::MetricsRecorder.org_attributes(org),
        event: "card_relevance_rated",
        card: Analytics::MetricsRecorder.card_attributes(card),
        actor: Analytics::MetricsRecorder.user_attributes(user),
        additional_data: metadata,
        cards_rating: Analytics::MetricsRecorder.cards_rating_attributes(cards_rating).merge(
          "rating" => new_rating,
          "updated_at" => stub_time.to_i
        )
      )
    )
    Timecop.freeze stub_time do
      cards_rating.update!(rating: new_rating)
    end
    # Recorder is called when level is changed
    new_level = "intermediate"
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        recorder: "Analytics::CardRatedMetricsRecorder",
        timestamp: stub_time.to_i,
        event: "card_relevance_rated",
        org: Analytics::MetricsRecorder.org_attributes(org),
        card: Analytics::MetricsRecorder.card_attributes(card),
        actor: Analytics::MetricsRecorder.user_attributes(user),
        additional_data: metadata,
        cards_rating: Analytics::MetricsRecorder.cards_rating_attributes(cards_rating).merge(
          "level" => new_level,
          "updated_at" => stub_time.to_i
        )
      )
    )
    Timecop.freeze stub_time do
      cards_rating.update!(level: new_level)
    end
  end

  test 'should update level on card_metadata if changed by author of card' do
    TestAfterCommit.with_commits(true) do
      org = create :organization
      user = create :user, organization: org
      card = create :card, organization: org, author: user

      cards_rating = create :cards_rating, card: card, user: user, level: 'beginner', rating: 4
      assert_equal 1, CardMetadatum.all.count
      assert_equal 'beginner', CardMetadatum.first.level
    end
  end

  test 'should update level on card_metadata if changed by user with admin role' do
    TestAfterCommit.with_commits(true) do
      org = create(:organization)
      user = create(:user, organization: org)
      card = create(:card, organization: org, author_id: user)

      user1 = create(:user, organization: org)
      admin_role = create :role, name: 'admin', organization: org

      role_permission = create :role_permission, role: admin_role, name: Permissions::ADMIN_ONLY
      admin_role.role_permissions << role_permission

      user1.roles << admin_role

      cards_rating = create(:cards_rating,card: card, user: user1, level: 'intermediate', rating: 4)
      assert_equal 1, CardMetadatum.all.count
      assert_equal 'intermediate', CardMetadatum.first.level
    end
  end

  test 'should not update level on card_metadata if changed by user who is neither admin nor author of that card' do
    TestAfterCommit.with_commits(true) do
      org = create(:organization)
      user = create(:user, organization: org)
      card = create(:card, organization: org, author_id: user)

      user1 = create(:user, organization: org)

      cards_rating = create(:cards_rating,card: card, user: user1, level: 'intermediate')

      assert_nil CardMetadatum.first.level
    end
  end

  test 'update rating on card_metadata when rating is changed' do
    TestAfterCommit.with_commits(true) do
      org = create(:organization)
      user = create(:user, organization: org)
      card = create(:card, organization: org, author_id: user)

      user1 = create(:user, organization: org)

      cards_rating = create(:cards_rating, card: card, user: user1, level: 'intermediate', rating: 1)

      assert_equal 1, CardMetadatum.first.average_rating
    end
  end
end
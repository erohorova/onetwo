require 'test_helper'

class UserContentCompletionTest < ActiveSupport::TestCase
  should validate_presence_of(:user)
  should validate_presence_of(:completable)

  self.use_transactional_fixtures = false
  setup do
    IndexJob.stubs(:perform_later)
    InfluxdbRecorder.stubs(:record).returns :true
    VideoStream.any_instance.stubs(:create_index)

    VideoStream.any_instance.stubs(:reindex_async)
    VideoStream.any_instance.stubs(:delete_index)
    VideoStream.any_instance.stubs(:schedule_reminder_push_notification).returns(nil)
  end

  test 'should remove user content completion when completable is deleted' do
    ContentsBulkDequeueJob.any_instance.stubs :perform
    org = create(:organization)
    user = create(:user, organization: org)
    video_stream = create(:iris_video_stream, creator: create(:user, organization: org))
    ucc = create(:user_content_completion, :started, completable: video_stream, user: user)
    UserContentsQueue.expects(:dequeue_content)

    assert_difference -> {UserContentCompletion.count}, -1  do
      video_stream.destroy
    end
  end

  test 'should remove item from user content queue when user content is completed' do
    org = create(:organization)
    user = create(:user, organization: org)
    video_stream = create(:iris_video_stream, creator: create(:user, organization: org))

    # removes item from user queue once assignment is completed
    UserContentsQueue.expects(:remove_from_queue).with(user, video_stream)
    ucc = create(:user_content_completion, completable: video_stream, user: user)
    ucc.complete!
  end

  test 'should add item to learning queue when user content is completed' do
    org = create(:organization)
    user = create(:user, organization: org)
    video_stream = create(:iris_video_stream, creator: create(:user, organization: org))

    ucc = create(:user_content_completion, completable: video_stream, user: user)
    UserContentsQueue.expects(:remove_from_queue)

    # adds item to learning queue
    assert_difference -> { LearningQueueItem.count }, 1 do
      ucc.complete!
    end

    lq = LearningQueueItem.last
    assert_equal 'completed', lq.state
    assert_equal user.id, lq.user_id
    assert_equal 'assignments', lq.source
  end

  test 'should create activity on completion of card/video_stream' do
    UserContentsQueue.stubs(:remove_from_queue)
    ActivityStreamCreateJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    org = create(:organization)

    card = create :card, author_id: nil, organization: org
    user = create :user, organization: org
    ucc = create(:user_content_completion, completable: card, user: user)
    ucc.complete!

    assert_equal ActivityStream.last.action, 'smartbite_completed'
    assert_equal ActivityStream.last.streamable, ucc
  end

  test 'should not create activity for hidden cards' do
    UserContentsQueue.stubs(:remove_from_queue)
    ActivityStreamCreateJob.unstub :perform_later

    org = create(:organization)
    card = create :card, author_id: nil, organization: org, hidden: true
    user = create :user, organization: org

    ActivityStreamCreateJob.expects(:perform_later).never
    ucc = create(:user_content_completion, completable: card, user: user)
    ucc.complete!
  end

  test 'should record mark as completed event' do
    RequestStore.unstub(:read)
    Organization.any_instance.stubs(:create_admin_user)

    org = create(:organization)
    author, user = create_list(:user, 2, organization: org)
    card = create :card, author: author, organization: org
    ucc = create(:user_content_completion, completable: card, user: user)

    metadata = {platform: 'web', user_agent: 'Edcast iPhone'}
    RequestStore.store[:request_metadata] = metadata

    stub_time

    # NOTE: This model uses author as the value of actor and does not read from metadata.
    UserContentsQueue.stubs(:remove_from_queue)
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::CardMetricsRecorder",
      org: Analytics::MetricsRecorder.org_attributes(org),
      card: Analytics::MetricsRecorder.card_attributes(card),
      event: 'card_marked_as_complete',
      actor: Analytics::MetricsRecorder.user_attributes(user),
      timestamp: Time.now.to_i,
      additional_data: metadata
    )
    ucc.complete!
  end

  test 'should record mark as uncompleted event' do
    RequestStore.unstub(:read)
    Organization.any_instance.stubs(:create_admin_user)

    org = create(:organization)
    author, user = create_list(:user, 2, organization: org)
    card = create :card, author: author, organization: org
    ucc = create(:user_content_completion, completable: card, user: user, state: "completed",
                 started_at: Time.now - 1.day, completed_at: Time.now - 1.day)
    metadata = {platform: 'web', user_agent: 'Edcast iPhone'}
    RequestStore.store[:request_metadata] = metadata

    stub_time
    # NOTE: This model uses author as the value of actor and does not read from metadata.
    UserContentsQueue.stubs(:remove_from_queue)
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::CardMetricsRecorder",
        org: Analytics::MetricsRecorder.org_attributes(org),
        card: Analytics::MetricsRecorder.card_attributes(card),
        event: 'card_marked_as_uncomplete',
        actor: Analytics::MetricsRecorder.user_attributes(user),
        timestamp: Time.now.to_i,
        additional_data: metadata
    )
    ucc.uncomplete!
  end

  test 'should update assignment and once called ActivityStreamCreateJob after complete user_content_completion' do
    UserContentsQueue.stubs(:remove_from_queue)
    ActivityStreamCreateJob.unstub(:perform_later)
    EdcastActiveJob.unstub(:perform_later)
    ActivityStreamCreateJob.expects(:perform_later).once

    org = create(:organization)

    card = create :card, author_id: nil, organization: org
    user = create :user, organization: org
    assignment = create(:assignment, assignable: card, assignee: user)
    ucc = create(:user_content_completion, completable: card, user: user)
    ucc.complete!

    assert ucc.completed?
    assert assignment.reload.completed?
  end
end

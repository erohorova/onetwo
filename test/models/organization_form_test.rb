require 'test_helper'

class NotificationFormTest < ActiveSupport::TestCase
  setup do
    skip
  end

  test "organization_forms should have created_at field" do
    form = OrganizationForm.create(first_name: "John", last_name: "Doe", organization_name: 'org name', email: 'example@mail.com', host_name: 'org_name', interests: 'cooking' )
    assert form.created_at
  end
end

require 'test_helper'

class BraintreePaymentGatewayTest < ActiveSupport::TestCase
  setup do
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @org_subscription = create :org_subscription, organization: @organization
    @order = create :order, orderable: @org_subscription, user_id: @user.id,
                   organization_id: @organization.id
    @transaction = create :transaction, order: @order, user: @user
  end

  test '#authorize payment success' do
    Braintree::Transaction.stubs(:sale).returns(Braintree::SuccessfulResult.new)
    auth_obj = BraintreePaymentGateway.new.authorize_payment(@transaction, nonce: 'fake-valid-nonce')
    assert auth_obj.success?
  end

  test '#authorize payment failure' do
    Braintree::Transaction.stubs(:sale).returns(Braintree::ErrorResult.new(:gateway, :errors => {}))
    auth_obj = BraintreePaymentGateway.new.authorize_payment(@transaction, nonce: 'fake-valid-nonce')
    refute auth_obj.success?
  end

  test '#void_payment should return status as failed if payment was voided successfully' do
    transaction = Braintree::Transaction._new(:gateway, id: 12345)
    gateway = Braintree::SuccessfulResult.new(transaction: transaction)
    Braintree::Transaction.stubs(:void).returns(gateway)
    result = BraintreePaymentGateway.new.void_payment(gateway: gateway)

    assert_equal true, result
  end

  test '#capture_payment success' do
    # Set expectations
    braintree_success_result =
      YAML.load_file("#{Rails.application.root}/test/fixtures/braintree_success_result.yml")
    result = {
      status: PaymentGateway::STATUS_SUCCESSFUL,
      gateway_id: "asdfasdf",
      gateway_data: braintree_success_result
    }

    BraintreePaymentGateway.any_instance.expects(:capture_payment).returns(result)
    gateway = BraintreePaymentGateway.new
    result = gateway.capture_payment(@transaction, nonce: 'fake-valid-nonce')

    assert_equal PaymentGateway::STATUS_SUCCESSFUL, result[:status]
    assert_not_nil result[:gateway_data]
    assert_not_nil result[:gateway_id]
  end

  test '#capture_payment failure' do
    # Set expectations
    braintree_failure_result =
      YAML.load_file("#{Rails.application.root}/test/fixtures/braintree_failure_result.yml")
    result = {
      status: PaymentGateway::STATUS_FAILED,
      gateway_id: "asdfasdf",
      gateway_data: braintree_failure_result
    }

    BraintreePaymentGateway.any_instance.expects(:capture_payment).returns(result)
    gateway = BraintreePaymentGateway.new
    result = gateway.capture_payment(@transaction, nonce: 'fake-valid-nonce')

    assert_equal PaymentGateway::STATUS_FAILED, result[:status]
    assert_not_nil result[:gateway_data]
    assert_not_nil result[:gateway_id]
  end

  test '#merge_gateway_params should merge client token to the resp hash' do
    resp_params = { transaction: Transaction.new, order: Order.new, token: 'AA11BB22CC33DD44EE55' }
    BraintreePaymentGateway.any_instance.stubs(:generate_client_token).returns('dummy-client-token')
    result = BraintreePaymentGateway.new.merge_gateway_params(
                                       params: {
                                         resp_params: resp_params,
                                         orderable: Card.new,
                                         price: Price.new
                                       }
                                     )
    assert_equal 'dummy-client-token', result[:client_token]
  end
end

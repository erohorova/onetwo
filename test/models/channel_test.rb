require 'test_helper'

class ChannelTest < ActiveSupport::TestCase
  # to enable after_commit to get exceuted

  should have_db_column(:content_discovery_freq).of_type(:string)
  should have_db_column(:last_discovered_at).of_type(:datetime)
  should have_db_column(:ecl_enabled).of_type(:boolean)

  should belong_to :organization

  should have_many :followers
  should have_many :follows

  should validate_presence_of :label
  should validate_uniqueness_of(:label).scoped_to(:organization_id)

  should have_many :interests
  should have_many :ecl_sources
  should have_many :curated_channels_cards
  should have_many :widgets

  test 'search data should return correct response' do
    org = create(:organization)
    channel_author, direct_follow_user, follows_via_team_user = create_list(:user, 3, organization: org)

    public_channel = create(:channel, organization: org, user: channel_author, label: 'Public Channel')
    private_channel = create(:channel, organization: org, user: channel_author, label: 'Private Channel', is_private: true)

    # Create team
    team = create(:team, organization: org)

    # direct_follow_user will follow public and private channels directly
    direct_follow_user.follows.create(followable: public_channel)
    direct_follow_user.follows.create(followable: private_channel)

    # follows_via_team_user will follow public and private channels via team
    team.add_member(follows_via_team_user)
    team.teams_channels_follows.create(channel: public_channel)
    team.teams_channels_follows.create(channel: private_channel)

    public_channel.add_collaborators(from: channel_author, users: [direct_follow_user, follows_via_team_user])
    private_channel.add_collaborators(from: channel_author, users: [direct_follow_user, follows_via_team_user])

    expected_addition_response = {
      'curators' => [],
      'collaborators' => [
                          {
                            id: direct_follow_user.id,
                            name: direct_follow_user.name
                          },
                          {
                            id: follows_via_team_user.id,
                            name: follows_via_team_user.name
                          }
                        ],
      'followers' => [{ id: direct_follow_user.id, name: direct_follow_user.name }],
      'followed_team_ids' => [team.id]
    }

    assert_equal public_channel.as_json.merge!(expected_addition_response), public_channel.search_data
    assert_equal private_channel.as_json.merge!(expected_addition_response), private_channel.search_data
  end

  class ReadableChannelsTest < ActiveSupport::TestCase
    # Readable channels = Channels followed by user directly
    #                     + Channels followed by user via Team
    #                     + Public channels of organization
    setup do
      @org = create(:organization)
      @channel_author, @user = create_list(:user, 2, organization: @org)

      # Create 5 public channels
      @public_1 = create(:channel, organization: @org, user: @channel_author, label: 'Public Channel 1')
      @public_2 = create(:channel, organization: @org, user: @channel_author, label: 'Public Channel 2')
      @public_3 = create(:channel, organization: @org, user: @channel_author, label: 'Public Channel 3')
      @public_4 = create(:channel, organization: @org, user: @channel_author, label: 'Public Channel 4')
      @public_5 = create(:channel, organization: @org, user: @channel_author, label: 'Public Channel 5')

      # Create 5 private channels
      @private_1 = create(:channel, organization: @org, user: @channel_author, label: 'Private Channel 1', is_private: true)
      @private_2 = create(:channel, organization: @org, user: @channel_author, label: 'Private Channel 2', is_private: true)
      @private_3 = create(:channel, organization: @org, user: @channel_author, label: 'Private Channel 3', is_private: true)
      @private_4 = create(:channel, organization: @org, user: @channel_author, label: 'Private Channel 4', is_private: true)
      @private_5 = create(:channel, organization: @org, user: @channel_author, label: 'Private Channel 5', is_private: true)

      # Create team
      @team = create(:team, organization: @org)

      # User will follow 2 public channels and 1 private channel directly
      @user.follows.create(followable: @public_1)
      @user.follows.create(followable: @public_2)
      @user.follows.create(followable: @private_1)

      # User will follow 1 public channel and 2 private channels via team
      @team.add_member(@user)
      @team.teams_channels_follows.create(channel: @public_3)
      @team.teams_channels_follows.create(channel: @private_2)
      @team.teams_channels_follows.create(channel: @private_3)
    end

    test 'should return readable channels for user' do
      # Readable channels = 3 (followed directly) + 3 (followed via team) + 2 (Remaining public channels)
      readable_channels = Channel.readable_channels(@org.channels, @user.id, @org.id)
      assert_equal 8, readable_channels.count
      assert_same_elements [@public_1.id, @public_2.id, @public_3.id, @public_4.id, @public_5.id, @private_1.id, @private_2.id, @private_3.id], readable_channels.map(&:id)
      refute_includes readable_channels.map(&:id), @private_4.id
      refute_includes readable_channels.map(&:id), @private_5.id
    end

    test 'should not return private channels on unfollowing' do
      # Unfollow a private channel directly
      @user.follows.where(followable: @private_1).first.delete
      readable_channels = Channel.readable_channels(@org.channels, @user.id, @org.id)
      assert_equal 7, readable_channels.count
      assert_same_elements [@public_1.id, @public_2.id, @public_3.id, @public_4.id, @public_5.id, @private_2.id, @private_3.id], readable_channels.map(&:id)
      refute_includes readable_channels.map(&:id), @private_1.id

      # Unfollow a private channel via team
      @team.teams_channels_follows.where(channel: @private_2).first.delete
      readable_channels = Channel.readable_channels(@org.channels, @user.id, @org.id)
      assert_equal 6, readable_channels.count
      assert_same_elements [@public_1.id, @public_2.id, @public_3.id, @public_4.id, @public_5.id, @private_3.id], readable_channels.map(&:id)
      refute_includes readable_channels.map(&:id), @private_2.id
    end

    test 'should not return private channels when user is removed from team' do
      TeamsUser.where(user_id: @user.id, team_id: @team.id).first.delete

      readable_channels = Channel.readable_channels(@org.channels, @user.id, @org.id)
      assert_equal 6, readable_channels.count
      assert_same_elements [@public_1.id, @public_2.id, @public_3.id, @public_4.id, @public_5.id, @private_1.id], readable_channels.map(&:id)
      refute_includes readable_channels.map(&:id), @private_2.id
      refute_includes readable_channels.map(&:id), @private_3.id
    end
  end

  class FollowingChannelsTest < ActiveSupport::TestCase
    # Following channels = Channels followed by user directly
    #                     + Channels followed by user via Team
    setup do
      @org = create(:organization)
      @channel_author, @user = create_list(:user, 2, organization: @org)

      # Create 5 public channels
      @public_1 = create(:channel, organization: @org, user: @channel_author, label: 'Public Channel 1')
      @public_2 = create(:channel, organization: @org, user: @channel_author, label: 'Public Channel 2')
      @public_3 = create(:channel, organization: @org, user: @channel_author, label: 'Public Channel 3')
      @public_4 = create(:channel, organization: @org, user: @channel_author, label: 'Public Channel 4')
      @public_5 = create(:channel, organization: @org, user: @channel_author, label: 'Public Channel 5')

      # Create 5 private channels
      @private_1 = create(:channel, organization: @org, user: @channel_author, label: 'Private Channel 1', is_private: true)
      @private_2 = create(:channel, organization: @org, user: @channel_author, label: 'Private Channel 2', is_private: true)
      @private_3 = create(:channel, organization: @org, user: @channel_author, label: 'Private Channel 3', is_private: true)
      @private_4 = create(:channel, organization: @org, user: @channel_author, label: 'Private Channel 4', is_private: true)
      @private_5 = create(:channel, organization: @org, user: @channel_author, label: 'Private Channel 5', is_private: true)

      # Create team
      @team = create(:team, organization: @org)

      # User will follow 2 public channels and 1 private channel directly
      @user.follows.create(followable: @public_1)
      @user.follows.create(followable: @public_2)
      @user.follows.create(followable: @private_1)

      # User will follow 1 public channel and 2 private channels via team
      @team.add_member(@user)
      @team.teams_channels_follows.create(channel: @public_3)
      @team.teams_channels_follows.create(channel: @private_2)
      @team.teams_channels_follows.create(channel: @private_3)
    end

    test 'should return following channels for user' do
      # Following channels = 3 (followed directly) + 3 (followed via team)
      following_channels = Channel.following_channels(@org.channels, true, @user.id)
      assert_equal 6, following_channels.count
      assert_same_elements [@public_1.id, @public_2.id, @public_3.id, @private_1.id, @private_2.id, @private_3.id], following_channels.map(&:id)
      refute_includes following_channels.map(&:id), @public_4.id
      refute_includes following_channels.map(&:id), @public_5.id
      refute_includes following_channels.map(&:id), @private_4.id
      refute_includes following_channels.map(&:id), @private_5.id
    end

    test 'should not returned unfollowed channels' do
      # Unfollow a private channel directly
      @user.follows.where(followable: @private_1).first.delete
      following_channels = Channel.following_channels(@org.channels, true, @user.id)
      assert_equal 5, following_channels.count
      assert_same_elements [@public_1.id, @public_2.id, @public_3.id, @private_2.id, @private_3.id], following_channels.map(&:id)
      refute_includes following_channels.map(&:id), @private_1.id

      # Unfollow a private channel via team
      @team.teams_channels_follows.where(channel: @private_2).first.delete
      following_channels = Channel.following_channels(@org.channels, true, @user.id)
      assert_equal 4, following_channels.count
      assert_same_elements [@public_1.id, @public_2.id, @public_3.id, @private_3.id], following_channels.map(&:id)
      refute_includes following_channels.map(&:id), @private_2.id
    end

    test 'should not return channel which were followed via team on removing user from team' do
      TeamsUser.where(user_id: @user.id, team_id: @team.id).first.delete

      following_channels = Channel.following_channels(@org.channels, true, @user.id)
      assert_equal 3, following_channels.count
      assert_same_elements [@public_1.id, @public_2.id, @private_1.id], following_channels.map(&:id)
      refute_includes following_channels.map(&:id), @public_3.id
      refute_includes following_channels.map(&:id), @private_2.id
      refute_includes following_channels.map(&:id), @private_3.id
    end

    test 'should return not followed public channels if is_following is false' do
      not_following_channels = Channel.following_channels(@org.channels, false, @user.id)
      assert_equal 2, not_following_channels.count
      assert_same_elements [@public_4.id, @public_5.id], not_following_channels.map(&:id)
    end
  end

  class AutoFollowTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false

    test '#all_followers should retrun all followers explicit and team folllowers' do
      org = create(:organization)

      explicit_user1 = create(:user, organization: org)
      explicit_user2 = create(:user, organization: org)
      user1 = create(:user, organization: org)
      user2 = create(:user, organization: org)
      user3 = create(:user, organization: org)
      user4 = create(:user, organization: org)

      channel = create(:channel, :architecture, organization_id: org.id)
      team = create(:team, organization: org)

      channel.followed_teams << team
      team.add_member(user1)
      team.add_member(user2)
      team.add_member(user3)
      team.add_member(user4)
      channel.add_followers [explicit_user1,explicit_user2]

      assert_equal channel.followers.count, [explicit_user1, explicit_user2].size
      assert_same_elements channel.followers, [explicit_user1, explicit_user2]
      assert_same_elements channel.all_followers, [user1, user2, user3, user4, explicit_user1, explicit_user2]
      assert_equal channel.all_followers.count, [user1, user2, user3, user4, explicit_user1, explicit_user2].size
    end

    test '#follows_via_team? should retrun true when user id following via team' do
      org = create(:organization)
      team_follower = create(:user, organization: org)
      channel_follower = create(:user, organization: org)
      non_follower = create(:user, organization: org)
      channel = create(:channel, :architecture, organization_id: org.id)
      team = create(:team, organization: org)

      channel.add_followers [channel_follower]
      channel.followed_teams << team
      team.add_member(team_follower)

      assert_equal channel.follows_via_team?(team_follower), true
      assert_equal channel.follows_via_team?(channel_follower), false
      assert_equal channel.follows_via_team?(non_follower), false
    end

    test '#individual user should also be allowed to add to the channel after adding as team_user' do
      org = create(:organization)
      team_follower = create(:user, organization: org)

      channel = create(:channel, :architecture, organization_id: org.id)
      team = create(:team, organization: org)
      team.add_member(team_follower)

      channel.followed_teams << team
      channel.add_followers([team_follower], true)

      assert_equal channel.follows_via_team?(team_follower), true
      assert_equal channel.followers.include?(team_follower), true
    end

    test 'should not call add followers job if auto follow is false' do
      AddFollowersToChannelJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later

      AddFollowersToChannelJob.expects(:perform_later).never
      ch = create(:channel)
      ch.update_attributes(label: "different name")
    end

    test 'should call add followers job if auto follow is true on create' do
      AddFollowersToChannelJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later

      AddFollowersToChannelJob.expects(:perform_later).once
      ch = create(:channel, auto_follow: true)
    end

    test 'should call add followers job if auto follow is updated to true' do
      ch = create(:channel)
      AddFollowersToChannelJob.expects(:perform_later).once
      ch.update_attributes(auto_follow: true)
    end

    test 'should auto follow by creator' do
      AddFollowersToChannelJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later
      Analytics::MetricsRecorderJob.stubs(:perform_later)

      user = create(:user)
      AddFollowersToChannelJob.expects(:perform_later).once
      auto_follow_channel = create(:channel, auto_follow: true, user: user, organization: user.organization)
      assert auto_follow_channel.is_follower?(auto_follow_channel.creator)

      non_auto_follow_channel = create(:channel, user: user, organization: user.organization)
      assert non_auto_follow_channel.is_follower?(non_auto_follow_channel.creator)
    end
  end

  class ChannelProgrammingTest < ActiveSupport::TestCase
    test 'program channel with given params and fetch data immediately' do
      opts = {channel: {
              language: ["en", "gu"],
              content_type: ["media", "poll"],
              card_fetch_limit: 20,
              topics:[{id:"4999080280199627265",
                      name:"deloitte.industries.life_sciences_and_health_care.health_care.health_care_providers.other_providers",
                      label:"Other Providers",
                      domain:{id:"4999080275303973909",
                              label:"industries",
                              name:"deloitte.industries"}
                    }],
              ecl_sources:[
                {source_id:"9c07ce8b-cf28-4d22-b653-ddafc4c6c7e0",display_name:"Forbes RSS Mansi",private_content: false},
                {source_id:"32a0b385-b479-417e-9a1a-42f881fed69b",display_name:"Shraddha AI690",private_content: true}
              ]}
            }
      channel = create(:channel)
      ChannelProgrammingJob.expects(:perform_later).with(channel.id).returns(nil).once
      channel.program(opts[:channel])
      assert_not_empty channel.language
      assert_not_empty channel.content_type
      assert_not_empty channel.topics
      assert_not_empty channel.ecl_sources
      assert_equal 20, channel.card_fetch_limit
      assert !channel.ecl_sources.find_by(ecl_source_id: '9c07ce8b-cf28-4d22-b653-ddafc4c6c7e0').private_content
      assert channel.ecl_sources.find_by(ecl_source_id: '32a0b385-b479-417e-9a1a-42f881fed69b').private_content
    end
  end

  test 'should auto add creator as curator' do
    Analytics::MetricsRecorderJob.stubs(:perform_later)
    org = create(:organization)
    user = create(:user, organization: org)
    ch = create(:channel, organization: org, user: user)
    ch.run_callbacks :commit
    ch.reload
    assert_includes ch.curators.pluck(:id), user.id
  end

  test 'flush followers cache should clear rails cache' do
    org = create(:organization)
    channel = create(:channel, org: org)
    Rails.cache = ActiveSupport::Cache::MemoryStore.new

    key = Channel.send :fetch_followers_cache_key, channel.id

    Rails.cache.fetch(key) {1}
    assert_equal 1, Rails.cache.read(key)

    Channel.invalidate_followers_cache(channel.id)
    assert_nil Rails.cache.read(key)
    #Rails cache is using null store class  in testcase
    #Testing purpose we changed it to MemoryStore
    #Converting back to Nullstore since other model test cases are failing because of MemoryStore
    Rails.cache = ActiveSupport::Cache::NullStore.new
  end

  test 'add curator should also be added as author' do
    org = create(:organization)
    user = create(:user, organization: org)
    ch = create(:channel, organization: org)

    ch.add_curators([user])
    ch.reload
    assert_includes ch.authors.pluck(:id), user.id
    assert_includes ch.curators.pluck(:id), user.id
  end

  test 'invokes channel after_follow on follow' do
    org = create(:organization)

    channel = create(:channel, :robotics, organization: org)
    user = create(:user, organization: org)
    channel.expects(:after_followed).with(user)
    create(:follow, user: user, followable: channel).run_callbacks(:commit)
  end

  test 'invokes channel after_unfollow on unfollow' do
    org = create(:organization)
    channel = create(:channel, :robotics, organization: org)
    user = create(:user, organization: org)
    channel.expects(:after_followed).with(user)
    follow = create(:follow, user: user, followable: channel)

    channel.expects(:after_unfollowed).with(user)
    follow.destroy.run_callbacks(:commit)
  end

  test 'snippet' do
    channel = create(:channel, :robotics)
    assert_equal channel.snippet, "Robotics"
  end

  test 'generates slugs' do
    org = create(:organization)
    assert_equal 'robotics', create(:channel, :robotics, organization: org).slug
    channel = create(:channel, label: 'robotics', organization: org)
    assert_equal "robotics-#{channel.id}", channel.slug

    # reserved string
    channel = create(:channel, label: 'admin', organization: org)
    assert_equal "admin-#{channel.id}", channel.slug
  end

  test 'regenerates slugs when label is changed' do
    channel = create(:channel, :robotics)
    assert_equal 'robotics', channel.slug
    channel.update(:label => 'robotics123')
    assert_equal 'robotics123', channel.slug

    # reserved string
    channel.update(label: 'new')
    assert_equal "new-#{channel.id}", channel.slug
  end

  test 'scopes slugs to orgs' do
    orgs = create_list(:organization, 2)
    ch1 = create(:channel, label: 'one', organization: orgs.first)
    ch2 = create(:channel, label: 'one', organization: orgs.last)
    assert_equal 'one', ch1.slug
    assert_equal 'one', ch2.slug

    assert_raise ActiveRecord::RecordInvalid do
      create(:channel, label: 'one', organization: orgs.first)
    end

    ch11 = create(:channel, slug: 'one', organization: orgs.first)
    assert_not_equal 'one', ch11.slug
  end

  test 'get video streams' do
    VideoStream.any_instance.stubs :create_index
    VideoStream.any_instance.stubs :update_index

    org = create(:organization)
    # no autopull based on hashtags
    ch1 = create(:channel, autopull_content: false, organization: org)
    v1, v2 = create_list(:wowza_video_stream, 2, creator: create(:user, organization: org), status: 'live')
    red5_pro_video = create(:red5_pro_video_stream, :live, name: 'red 5 pro', creator: create(:user, organization: org))
    red5_card = red5_pro_video.card
    red5_card.channels << ch1
    c1 = v1.card
    c1.channels << ch1

    video_streams = Channel.video_stream_feed([ch1])
    assert_equal [v1, red5_pro_video], video_streams

    ch2 = create(:channel, autopull_content: true, organization: org)
    ch3 = create(:channel, autopull_content: false, organization: org)

    directly_posted = create(:wowza_video_stream, status: 'live', creator: create(:user, organization: org))
    directly_card = directly_posted.card
    directly_card.channels << ch2

    # These will not show up any more. Streams have to be directly posted to channels
    posted_with_hashtag = create(:wowza_video_stream, name: "#robotics", status: 'live', creator: create(:user, organization: org))
    posted_by_u1 = create(:wowza_video_stream, name: 'something', creator: create(:user, handle: "@u1", organization: org), status: 'live')

    assert_same_elements [directly_posted], Channel.video_stream_feed([ch2])

    # matches hashtags, but autopull is false
    assert_equal [], Channel.video_stream_feed([ch3])

    # both ch1 and ch2
    assert_same_elements [v1, red5_pro_video, directly_posted], Channel.video_stream_feed([ch1, ch2])

    assert_equal [], Channel.video_stream_feed([create(:channel, autopull_content: false, organization: org)])
    assert_equal [], Channel.video_stream_feed([create(:channel, autopull_content: true, organization: org)])

    # video stream with hashtag posted to private channel should not be picked up by public channels that
    # have autopull set up
    private_channel = create(:channel, is_private: true, organization: org)
    private_stream = create(:wowza_video_stream, name: "something private in #robotics", status: 'live', creator: create(:user, organization: org))
    private_stream.channels << private_channel
    assert_equal [private_stream], Channel.video_stream_feed([private_channel])
    assert_not Channel.video_stream_feed([ch2]).include?(private_stream)
    assert Channel.video_stream_feed([ch2, private_channel]).include?(private_stream)
  end

  test 'video stream feed should exclude deleted streams' do
    VideoStream.any_instance.stubs :create_index
    VideoStream.any_instance.stubs :update_index
    Card.any_instance.stubs(:remove_card_from_queue)
    Card.any_instance.stubs(:delete_ecl_card)

    org = create(:organization)
    vs = create_list(:wowza_video_stream, 2, creator: create(:user, organization: org))
    ch = create(:channel, organization: org)
    vs.each {|v| v.channels << ch}
    assert_same_elements vs, Channel.video_stream_feed([ch])

    vs.last.delete_from_cms!
    assert_equal [vs.first], Channel.video_stream_feed([ch])
  end

  test "should return only curated cards" do
    org = create(:organization)
    user = create(:user, organization: org)
    cards = create_list(:card, 5, organization: org, author: user)
    video_streams = create_list(:wowza_video_stream, 5, creator: create(:user, organization: org))
    channel = create(:channel, organization: org)

    channel.cards << cards
    ChannelsCard.last.update_column(:state, :new)

    assert_equal channel.curated_smartbites.length, 4
  end

  test 'read access' do
    create_default_org
    default_org = Organization.default_org
    robotics = create(:channel, :robotics, visible_during_onboarding: true, organization: default_org)
    u = create(:user, organization: default_org)
    assert robotics.has_read_access?(u)

    private_channel = create(:channel, :private, organization: default_org)
    assert_not private_channel.has_read_access?(u)
    assert_same_elements [robotics], Channel.readable_channels_for(u, _organization: default_org)

    create(:follow, followable: private_channel, user: u)
    assert_same_elements [robotics], Channel.readable_channels_for(u, _organization: default_org)

    # onboarding visibility cases
    assert_same_elements [robotics, private_channel],
                         Channel.readable_channels_for(u, _organization: default_org, onboarding: false)

    # channels in org
    org = create(:organization)
    public_channel_org = create(:channel, is_private: false, organization: org)
    private_channel_org = create(:channel, is_private: true, organization: org)

    # new channels shouldn't show up for this user
    assert_same_elements [robotics, private_channel], Channel.readable_channels_for(u, _organization: default_org, onboarding: false)
    assert_same_elements [robotics], Channel.readable_channels_for(nil, _organization: default_org, onboarding: false)
  end

  test 'read access to user added as a follower to private channel' do
    # create_default_org
    org = create(:organization)
    u = create(:user, organization: org)
    private_channel = create(:channel, :private, organization: org)
    assert_not private_channel.has_read_access?(u)

    # now add user as a follower
    private_channel.add_followers([u])
    assert private_channel.has_read_access?(u)

    org = create(:organization)
    public_channel_org = create(:channel, is_private: false, organization: org)
    private_channel_org = create(:channel, is_private: true, organization: org)
    [public_channel_org, private_channel_org].each {|ch| assert_not ch.has_read_access?(u)}
  end

  test 'display show authors flag' do
    org = create(:organization)

    u1 = create(:user, organization: org)
    robotics = create(:channel, :robotics, organization: org)
    robotics.authors << u1
    assert_equal false, robotics.show_authors
  end

  test 'pubnub channel' do
    ch = create(:channel, :robotics)
    public_channel_name = nil
    assert_difference ->{PubnubChannel.count} do
      public_channel_name = ch.public_pubnub_channel_name
    end

    assert_no_difference ->{PubnubChannel.count} do
      assert_equal public_channel_name, ch.reload.public_pubnub_channel_name
    end
  end

  test 'gets writable channels' do
    org1 = create(:organization)
    u = create(:user, organization: org1)
    c0 = create(:channel, organization: org1)
    c1 = create(:channel, :robotics, organization: org1)
    c2 = create(:channel, :architecture, organization: org1)
    c1.authors << u
    c2.authors << u
    create(:channel, :health, organization: org1)

    # different organization channels, make sure these dont show up
    org = create(:organization)
    public_channel_org = create(:channel, is_private: false, organization: org, only_authors_can_post: false)
    private_channel_org = create(:channel, is_private: true, organization: org, only_authors_can_post: true)

    assert_same_elements [c1, c2], Channel.writable_channels_for(u)

    # two new channels, one of which allows followers to post
    c3 = create(:channel, is_private: true, only_authors_can_post: true, organization: org1)
    c4 = create(:channel, is_private: true, only_authors_can_post: false, organization: org1)
    [c3, c4].each {|c| create(:follow,followable: c, user: u)}
    assert_same_elements [c1, c2, c4], Channel.writable_channels_for(u)
  end

  test 'should return only writables channels ids' do
    Organization.any_instance.unstub :create_general_channel
    User.any_instance.unstub :add_to_org_general_channel
    EdcastActiveJob.unstub :perform_later
    Users::AfterCommitOnCreateJob.unstub :perform_later

    org1 = create(:organization)
    user1 = create(:user, organization: org1)
    user1.run_callbacks :commit

    channel1 = create(:channel, :robotics, organization_id: org1.id)
    channel2 = create(:channel, :architecture, organization_id: org1.id)
    channel3 = create(:channel, :health, organization_id: org1.id)
    channel4 = create(
      :channel, :good_music, organization_id: org1.id,
      only_authors_can_post: false
    )
    channel5 = create(:channel, :cars, organization_id: org1.id)

    general_channel = user1.organization.channels.where(
      organization_id: org1.id, label: 'General'
    ).first

    channel1.authors << user1
    channel2.curators << user1
    channel3.followers << user1
    channel4.followers << user1
    channel5.add_authors([user1], true)

    #writables channels only, when writable flag is true.
    is_writable = Channel.fetch_eligible_channels_for_user(
      user: user1, q: nil, filter_params: {writables: true}
    )
    expected_writable_channels = [
      channel1, channel2, channel4, general_channel, channel5
    ]
    assert_same_elements expected_writable_channels.map(&:id).sort, is_writable.map(&:id).sort
    #assert_same_elements expected_writable_channels, is_writable

    #non writable channels only, when writable flag is false.
    non_writables = Channel.fetch_eligible_channels_for_user(
      user: user1, q: nil, filter_params: {writables: false}
    )
    assert_same_elements [channel3], non_writables

    # should return channels where a user is trusted collaborator
    is_trusted = Channel.fetch_eligible_channels_for_user(
      user: user1, q: nil, filter_params: {is_trusted: true}
    )
    assert_same_elements [channel5], is_trusted
  end

  test 'should return public channels, curated,followable and followable via teams channels' do
    org = create(:organization)
    user = create(:user, organization: org)
    chs = create_list(:channel, 5, organization: org)
    private_author_channel = create(:channel, :private, organization: org)
    private_non_following_channel = create(:channel, label: "Non Follow", is_private: true, organization: org)
    team_channel = create(:channel, label: "Team Following", is_private: true, organization_id: org.id)
    team = create(:team, organization: org)

    team_channel.followed_teams << team
    team.add_member(user)


    private_author_channel.add_authors([user])
    private_author_channel.reload
    expected_channles = chs
    expected_channles << private_author_channel
    expected_channles << team_channel

    eligiable_channel = Channel.fetch_eligible_channels_for_user(user: user)

    assert_same_elements expected_channles.map(&:id).sort, eligiable_channel.map(&:id).sort
    refute expected_channles.map(&:id).include? private_non_following_channel.id
  end

  test 'should return open channel as writable, curatable if followed by user' do
    Organization.any_instance.unstub :create_general_channel
    User.any_instance.unstub :add_to_org_general_channel

    org  = create(:organization)
    follower = create(:user, organization: org)

    open_channel = create(:channel, :robotics, organization_id: org.id, is_open: true, only_authors_can_post: false)
    no_open_channel = create(:channel, :robotics, label: 'some label', organization_id: org.id, is_open: false)

    open_channel.followers << follower
    no_open_channel.followers << follower

    assert_same_elements [open_channel], Channel.fetch_eligible_channels_for_user(user: follower, q: nil, filter_params: {writables: true})
    assert_same_elements [open_channel], Channel.fetch_eligible_channels_for_user(user: follower, q: nil, filter_params: {is_curator: true})
  end

  test 'should not return channels that user has unfollowed' do
    org  = create(:organization)
    user = create(:user, organization_id: org.id)

    channels_list = create_list(:channel, 3, only_authors_can_post: 0, organization: org)
    channels_list.each do |ch|
      create(:follow, user_id: user.id, followable: ch)
    end
    assert_same_elements channels_list, Channel.fetch_eligible_channels_for_user(user: user, q: nil, filter_params: {writables: true})

    Follow.find_by(user_id: user.id, followable: channels_list.first).delete
    assert_not_includes Channel.fetch_eligible_channels_for_user(user: user, q: nil, filter_params: {writables: true}), channels_list.first
  end

  test 'should get recommeded channel for user' do
    org = create(:organization)
    u = create(:user, organization: org)

    pc1, pc2 = create_list(:channel, 2, is_private: false, organization: org)
    private_channel_org = create(:channel, is_private: true, organization: org)

    pc1.add_followers [u]

    channels = Channel.recommend_channels_for_user(user: u, organization: org)

    assert_equal 1, channels.count
    assert_not_includes channels, private_channel_org
    assert_not_includes channels, pc1
    assert_includes channels, pc2
  end

  test 'should be able to add author to channel' do
    org1 = create(:organization)
    author = create(:user, organization: org1)

    c1 = create(:channel, organization: org1)
    c1.add_authors [author]

    assert_equal 1, c1.authors.count
    assert_includes c1.authors, author
  end

  test 'should be not be able to add author to channel from different org' do
    org1 = create(:organization)
    org2 = create(:organization)

    author = create(:user, organization: org2)

    c1 = create(:channel, organization: org1)
    c1.add_authors [author]

    assert_equal 0, c1.authors.count
  end

  test 'should include channels that are collaborated by user' do
    # create_default_org
    org = create(:organization)
    user = create(:user, organization: org)
    chs = create_list(:channel, 5, organization: org)
    private_channel = create(:channel, :private, organization: org)

    assert_not private_channel.has_read_access?(user)

    private_channel.add_authors([user])
    private_channel.reload
    assert private_channel.has_read_access?(user)
    Channel.readable_channels_for(user, _organization: org, onboarding: false)
    assert_includes Channel.readable_channels_for(user, _organization: org, onboarding: false), private_channel
  end

  test 'channel and its authors should belong to the same org' do
    org = create(:organization)
    user = create(:user, organization: org)
    channel = create(:channel)
    assert_raises(ActiveRecord::RecordInvalid) { channel.authors << user }
    assert_equal true, channel.authors.collect(&:id).index(user.id).nil?
  end

  test "pins should be deleted on channel deletion" do
    Pin.any_instance.stubs(:record_card_unpin_event)
    org = create(:organization)
    user = create(:user, organization: org)
    channel = create(:channel, organization: org)
    cards = create_list(:card, 5, author: user, organization: org)
    cards.each do |card|
      Pin.create(pinnable: channel, object: card)
    end
    assert_difference -> {Pin.count}, -5 do
      channel.destroy
    end
  end

  test 'carousel structured items should be deleted on channel deletion' do
    org = create(:organization)
    user = create(:user, organization: org)

    channel = create(:channel, organization: org)
    structure = create(:structure, parent: org, current_context: 'discover', current_slug: 'cards', creator: user, display_name: 'Test')
    structure_item = create(:structured_item, structure: structure, entity: channel)
    structure_item_id = structure_item.id
    channel.destroy
    assert_nil StructuredItem.find_by_id(structure_item_id)
  end

  test "check if user is channel curator or not" do
    org = create(:organization)
    user = create(:user, organization: org)
    ch = create(:channel, organization: org)

    ch.add_curators([user])
    assert ch.is_curator?(user)
  end

  test "open channel should allow channel following user to be curator" do
    org = create(:organization)
    user = create(:user, organization: org)

    ch = create(:channel, organization: org, is_open: true)
    ch.add_followers([user])
    assert ch.is_curator?(user)
  end

  test 'make sure only public channel canbe made open' do
    org = create(:organization)
    ch = build(:channel, organization: org, is_open: true, is_private: true)
    refute ch.valid?
    assert ch.errors.full_messages.include?("only public channel can be made open")

    ch.is_private = false
    assert ch.valid?
    assert_empty ch.errors
  end

  test "has provider and provider_image columns" do
    channel = create :channel, organization: create(:organization)
    assert channel.persisted?
    assert_equal nil, channel.provider
    assert_equal nil, channel.provider_image
    channel.update provider: "foo", provider_image: "bar"
    assert_equal "foo", channel.provider
    assert_equal "bar", channel.provider_image
  end

  test "return mobile image with protocol" do
    channel = create :channel, organization: create(:organization)
    url = 'https://cdn-test-host.com/assets/default_banner_image_mobile.jpg'
    assert_equal url, channel.mobile_photo
    assert channel.mobile_photo.start_with?('https')
  end

  test "should return only active curators of given channels" do
    Organization.any_instance.unstub :create_general_channel
    User.any_instance.unstub :add_to_org_general_channel

    org1 = create(:organization)
    user1 = create(:user, organization: org1)
    user2 = create(:user, organization: org1)

    channel1 = create(:channel, :robotics, organization_id: org1.id)

    channel1.curators << user1
    channel1.curators << user2

    user2.update(is_suspended: true)
    assert_not_includes channel1.curators, user2
    assert_includes channel1.curators, user1
  end

  test 'should return only active authors of given channels' do
    Organization.any_instance.unstub :create_general_channel
    User.any_instance.unstub :add_to_org_general_channel

    org1 = create(:organization)
    user1 = create(:user, organization: org1)
    user2 = create(:user, organization: org1)

    channel1 = create(:channel, :robotics, organization_id: org1.id)

    channel1.authors << user1
    channel1.authors << user2

    UserTransitions::Suspend.new(users: [user2], organization: org1).mark

    assert_not_includes channel1.authors, user2
    assert_includes channel1.authors, user1
  end

  class ChannelFollowNotificationTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false
    setup do
      NotificationGeneratorJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later
    end

    test 'should notify change in follow unfollow list to pubnub' do
      org = create(:organization)
      OnFollowEnqueueJob.stubs(:perform_later)
      u = create(:user, organization: org)
      ch = create(:channel, :robotics, organization: org)
      PubnubNotifyChangeJob.expects(:perform_later).with("subscribe", u, ch.public_pubnub_channel_name).returns(nil).once
      f = create(:follow, followable: ch, user: u)

      PubnubNotifyChangeJob.expects(:perform_later).with("unsubscribe", u, ch.public_pubnub_channel_name).returns(nil).once
      f.destroy
    end
  end

  class ChannelIndexJobTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false

    test 'should invoke indexing job after commit' do
      Channel.any_instance.expects(:reindex_async_urgent).with(Channel, create(:channel).id).once
    end
  end

  class RecordEventTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false

    test 'should record channel create event' do
      IndexJob.stubs(:perform_later)
      Analytics::MetricsRecorderJob.expects(:perform_later)
        .with(has_entry(event: 'channel_created')).once

      org = create(:organization)
      author = create(:user, organization: org)
      create(:channel, organization: org, user: author)
    end

    test 'should record channel delete event' do
      IndexJob.stubs(:perform_later)
      Organization.any_instance.stubs(:create_admin_user)
      Channel.any_instance.stubs(:record_channel_create_event)

      org = create(:organization)
      actor = create(:user, organization: org)
      channel = create :channel, organization: org

      stub_time = Time.now
      metadata = {
        user_id: actor.id,
        platform: 'web',
        user_agent: 'chrome',
      }
      RequestStore.stubs(:read).returns metadata
      Analytics::MetricsRecorderJob.expects(:perform_later).with(
        recorder: 'Analytics::ChannelMetricsRecorder',
        channel: Analytics::MetricsRecorder.channel_attributes(channel),
        event: "channel_deleted",
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        additional_data: metadata,
        timestamp: stub_time.to_i
      )
      Analytics::MetricsRecorderJob.unstub(:perform_later)

      Timecop.freeze(stub_time) do
        channel.destroy
      end
    end

    test 'record_channel_edited_event calls recorder' do
      IndexJob.stubs(:perform_later)
      Channel.any_instance.stubs(:record_channel_create_event)
      org = create(:organization)
      channel = create :channel, organization: org, label: "A"
      actor = create(:user, organization: org)
      metadata = {
        user_id: actor.id,
        platform: 'web',
        user_agent: 'chrome',
      }
      stub_time = Time.now
      RequestStore.unstub(:read)
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      EdcastActiveJob.unstub(:perform_later)
      Analytics::MetricsRecorderJob.unstub(:perform_later)
      Analytics::MetricsRecorderJob.expects(:perform_later).with({
        recorder: 'Analytics::ChannelMetricsRecorder',
        channel: Analytics::MetricsRecorder.channel_attributes(channel).merge("description" => 'updated description'),
        event: "channel_edited",
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        additional_data: metadata,
        timestamp: stub_time.to_i,
        changed_column: "description",
        old_val: nil,
        new_val: "updated description"
      })

      Timecop.freeze stub_time do
        channel.update description: 'updated description'
      end
    end

    test 'doesnt call job for non-whitelisted column' do
      refute Analytics::ChannelMetricsRecorder.whitelisted_change_attrs.include?(
        "image_file_name"
      )
      org = create(:organization)
      channel = create :channel, organization: org, label: "asd"
      Analytics::MetricsRecorderJob.unstub(:perform_later)
      Analytics::MetricsRecorderJob.expects(:perform_later).never

      channel.update image_file_name: "@foobar"
    end

    test 'should record channel_promoted event' do
      IndexJob.stubs(:perform_later)
      Channel.any_instance.stubs(:record_channel_create_event)
      org = create(:organization)
      actor = create(:user, organization: org)
      channel = create :channel, organization: org, label: "A", is_promoted: false
      metadata = {
        user_id: actor.id,
        platform: 'web',
        user_agent: 'chrome',
      }
      stub_time = Time.now
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      Analytics::MetricsRecorderJob.unstub(:perform_later)
      Analytics::MetricsRecorderJob.expects(:perform_later).with(
        recorder: 'Analytics::ChannelMetricsRecorder',
        channel: Analytics::MetricsRecorder.channel_attributes(channel).merge(
          "is_promoted" => true
        ),
        event: "channel_promoted",
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        additional_data: metadata,
        timestamp: stub_time.to_i,
      )

      Timecop.freeze stub_time do
        channel.run_callbacks(:commit) do
          channel.update(is_promoted: true)
        end
      end
      assert channel.is_promoted
    end

    test 'should record channel_unpromoted event' do
      IndexJob.stubs(:perform_later)
      Channel.any_instance.stubs(:record_channel_create_event)
      EdcastActiveJob.unstub(:perform_later)
      Analytics::MetricsRecorderJob.unstub(:perform_later)
      org = create(:organization, host_name: "foo")
      actor = create(:user, organization: org)
      actor.run_callbacks(:commit)
      channel = create :channel, organization: org, label: "A", is_promoted: true
      metadata = {
        user_id: actor.id,
        platform: 'web',
        user_agent: 'chrome',
      }
      stub_time = Time.now
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      Analytics::MetricsRecorderJob.expects(:perform_later).with({
        recorder: 'Analytics::ChannelMetricsRecorder',
        channel: Analytics::MetricsRecorder.channel_attributes(channel).merge(
          "is_promoted" => false
        ),
        event: "channel_unpromoted",
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        additional_data: metadata,
        timestamp: stub_time.to_i,
      })
      # mock out another channel event, to avoid interference
      Channel.send(:define_method, :record_channel_edited_event) do |&blk|
        blk&.call
      end
      Timecop.freeze stub_time do
        channel.update(is_promoted: false)
        channel.run_callbacks(:commit)
      end
      refute channel.is_promoted
    end

    test 'should push channel visisted event' do
      RequestStore.unstub(:read)
      org = create(:organization)
      viewer = create(:user, organization: org)
      channel = create(:channel, organization: org)

      req_data = {platform: 'ios', platform_version_number: 2, user_agent: 'chrome'}
      RequestStore.store[:request_metadata] = req_data
      now = Time.now.utc
      Time.stubs(:now).returns now

      Analytics::MetricsRecorderJob.expects(:perform_later).with(
        recorder: "Analytics::ChannelMetricsRecorder",
        channel: Analytics::MetricsRecorder.channel_attributes(channel),
        actor: Analytics::MetricsRecorder.user_attributes(viewer),
        event: 'channel_visited',
        timestamp: now.to_i,
        additional_data: req_data
      )
      channel.push_view_event(viewer)
    end
  end

  class TestOverrideCuratorsAssociations < ActiveSupport::TestCase
    test 'check curators override for open channel' do
      org = create(:organization)
      open_channel = create(:channel, is_open: true, organization: org)
      user = create(:user, organization: org)
      open_channel.curators << user

      assert_empty open_channel.curators
      assert_equal User.none, open_channel.curators
      assert_empty open_channel.channels_curators
      assert_empty open_channel.curators.search_by_name("#{user.first_name}")
    end

    test 'check curators override for non open channel' do
      org = create(:organization)
      channel = create(:channel, organization: org)
      user = create(:user, organization: org)
      channel.curators << user

      assert_equal [user], channel.curators
      assert_not_empty channel.channels_curators
      assert_equal [user], channel.curators.search_by_name("#{user.first_name}")
    end
  end

  class SharedChannelTest < ActiveSupport::TestCase
    setup do
      @org1, @org2 = create_list(:organization, 2)
      @parent_channel = create(:channel, org: @org1, shareable: 1)
    end

    test 'should not be able to clone the same channel twice in the same org' do
      clone_channel1 = Channel.new(organization: @org2, label: "clone channel 1", parent_id: @parent_channel.id)
      assert clone_channel1.save

      clone_channel2 = Channel.new(organization: @org2, label: "clone channel 2", parent_id: @parent_channel.id)
      refute clone_channel2.save

      assert_equal "Parent has already been taken", clone_channel2.full_errors
    end

    test 'should not call unlink cloned channels method if the channel is not shareable or does not have any clones' do
      # unshareable channel destroyed
      unshared_channel = create(:channel, organization: @org1, shareable: 0)

      unshared_channel.expects(:unlink_cloned_channels).never
      unshared_channel.destroy

      # shareable but un-cloned channel destroyed
      shareable_channel = create(:channel, organization: @org1, shareable: 1)

      shareable_channel.expects(:unlink_cloned_channels).never
      shareable_channel.destroy

      # shareable and cloned channel destroyed
      shareable_channel_cloned = create(:channel, organization: @org1, shareable: 1)
      cloned_channel = create(:channel, organization: @org2, label: 'cloned channel 1', parent_id: shareable_channel_cloned.id)

      shareable_channel_cloned.expects(:unlink_cloned_channels).once
      shareable_channel_cloned.destroy
    end

    test 'should unlink cloned channels from the parent channel when parent channel is made unshareable' do
      cloned_channel = create(:channel, organization: @org2, label: 'cloned channel 1', parent_id: @parent_channel.id)
      assert_includes @parent_channel.cloned_channels.map(&:id), cloned_channel.id

      @parent_channel.destroy

      cloned_channel.reload
      assert_nil cloned_channel.parent_id
    end

    test 'returns organization name of with whom the channel is shared with' do
      parent_channel = create(:channel, organization: @org1, label: 'parent channel')
      cloned_channel1 = create(:channel, organization: @org2, label: 'cloned channel 1', parent_id: parent_channel.id)
      org3 = create(:organization)
      cloned_channel2 = create(:channel, organization: org3, label: 'cloned channel 2', parent_id: parent_channel.id)
      orgs = Channel.shared_with_orgs([parent_channel.id])
      assert_same_elements orgs[parent_channel.id].map(&:name), [@org2.name, org3.name]
    end

    test 'returns organization name of the channel' do
      channel1 = create(:channel, organization: @org1, label: 'channel1')
      channel2 = create(:channel, organization: @org2, label: 'channel2')
      orgs = Channel.organization_name([channel1.id, channel2.id])
      assert_equal orgs[channel1.id].map(&:name).first, @org1.name
      assert_equal orgs[channel2.id].map(&:name).first, @org2.name
    end

    test 'returns if current_organization has cloned given channel' do
      current_org = @org2
      channel1 = create(:channel, organization: @org1, label: 'channel1')
      channel2 = create(:channel, organization: @org2, label: 'channel2', parent_id: channel1.id)
      cloned_channels = Channel.cloned_channel_id([channel1.id], current_org.id)
      assert_equal cloned_channels[channel1.id][0].id, channel2.id
    end
  end

  class AddTrustedAuthorTest < ActiveSupport::TestCase
    setup do
      @org = create(:organization)
      @channel = create(:channel, is_open: true, organization: @org)
      @user1, @user2 = create_list(:user, 2, organization: @org)
    end

    test 'should add users as trusted authors to the channel' do
      # add trusted author to channel users
      @channel.add_authors([@user1, @user2], true)
      ch_user_types = @channel.trusted_channels_authors.pluck(:as_type)

      assert_equal 2, @channel.trusted_channels_authors.count
      assert 'trusted_author', ch_user_types.first
    end
  end

  class AddCollaboratorsTest < ActiveSupport::TestCase
    setup do
      @org = create(:organization)
      Role.create_standard_roles(@org)
      @channel = create(:channel, is_open: true, organization: @org)
      @user = create(:user, organization: @org)
      @users = create_list(:user, 2, organization: @org)
    end

    test 'should add users as collaborators in the channel' do
      @channel.add_collaborators(from: @user, users: @users)

      assert_equal 2, @channel.authors.size
      assert_equal ['collaborator'], @users.map { |author| author.roles.map(&:name) }.flatten.uniq
    end

    test 'should add users as trusted collaborators in the channel' do
      @channel.add_collaborators(from: @user, users: @users, as_trusted: true)

      assert_equal 2, @channel.trusted_channels_authors.size
      assert_equal ['trusted_author'], @channel.trusted_channels_authors.pluck(:as_type).uniq
      assert_equal ['trusted_collaborator'], @users.map { |author| author.roles.map(&:name) }.flatten.uniq
    end
  end
end

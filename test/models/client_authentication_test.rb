require 'test_helper'

class ClientAuthenticationTest < ActiveSupport::TestCase
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    @org = create(:organization)
    client = create(:client, organization: @org)
  end

  test "should return organization" do
    request = Hash.new
    request.stubs(:headers).returns({})
    cred = @org.client.valid_credential
    token = JWT.encode({"redirect_uri" => "example.com"}, cred.shared_secret, 'HS256')
    params = {"token" => token, "api_key" => cred.api_key}
    org, jwt_payload, jwt_header = ClientAuthentication.get_org(request: request , params: params)
    assert_equal org, @org
    assert_equal jwt_payload, {"redirect_uri"=> "example.com"}
    assert_equal jwt_header, {"typ"=>"JWT", "alg"=>"HS256"}

    #with headers
    request.stubs(:headers).returns({"X-API-TOKEN" => token, "X-API-KEY" => cred.api_key})
    org, jwt_payload, jwt_header = ClientAuthentication.get_org(request: request , params: {})
    assert_equal org, @org
  end

  test "should return nil if credentials are not present" do
    request = Hash.new
    request.stubs(:headers).returns({})
    token = JWT.encode({}, "whatever", 'HS256')
    params = {"token" => token, "api_key" => "invalid"}
    org, jwt_payload, jwt_header = ClientAuthentication.get_org(request: request , params: params)
    assert_nil org
    assert_nil jwt_payload
    assert_nil jwt_header
  end

  class ApiAuthTest < ActiveSupport::TestCase
    setup do
      @org = create(:organization)
      client = create(:client, organization: @org)
    end

    test "should return nil if params are not present" do
      assert_nil ClientAuthentication.api_auth!
    end

    test "should return nil if credentials is not present" do
      #invalid apikey
      assert_nil ClientAuthentication.api_auth!(params: {api_key: "invalid"})
    end

    test "should returns nil if request token is invalid" do
      params = {api_key: @org.client.valid_credential.api_key}
      ClientAuthentication.stubs(:is_api_client_request_valid?).returns(false)
      assert_nil ClientAuthentication.api_auth!(params: params)
    end

    test "should set the current user as existing client user" do
      create_default_org
      create_user

      #creating client_user consider 123 is savannah_user_id
      assert_no_difference -> {User.count} do
        ClientsUser.create(client_user_id: 123, user_id: @u.id, client_id: @client.id)
        params = {user_id: 123, api_key: @client.valid_credential.api_key}
        org, current_user, auth_params, widget_request = ClientAuthentication.api_auth!(params: params)
        assert_equal org, @org
        assert_equal current_user, @u
        assert_equal auth_params, {:api_key=>@client.valid_credential.api_key, :user_id=>123}
        assert widget_request
      end
    end

    test "should set current_user as current org user with email" do
      create_user

      # org_user = create(:user, organization: @u.org, email: @u.email)

      #client_user is not present with savannah_user id 123
      params = {user_id: 123, user_email: @u.email, api_key: @client.valid_credential.api_key}
      assert_no_difference -> {User.count} do
        org, current_user, auth_params, widget_request = ClientAuthentication.api_auth!(params: params)
        assert_equal org, @org
        assert_equal current_user, @u
        assert_equal current_user.email, @u.email
        assert widget_request
      end
    end

    test "should not create new user in default_org if user is not present" do
      create_user

      #if email is present
      email = "test@test.com"
      params = {user_id: 123, user_email: email, api_key: @client.valid_credential.api_key}
      assert_difference -> {User.count} do
        org, current_user, auth_params, widget_request = ClientAuthentication.api_auth!(params: params)
        assert_equal org, @org
        assert_not_equal current_user, @u
        assert_equal current_user.email, email
        assert widget_request
      end

      #without email
      params1 = {user_id: 12345, api_key: @client.valid_credential.api_key}
      assert_difference -> {User.count} do
        org, current_user, auth_params, widget_request = ClientAuthentication.api_auth!(params: params1)
        assert_equal org, @org
        assert_not_nil current_user
        assert_not_equal current_user, @u
        assert_nil current_user.email
        assert widget_request
      end
    end

    def create_user
      @u = create(:user, organization: @org)
      @client = create(:client, organization: @org)
      ClientAuthentication.stubs(:is_api_client_request_valid?).returns(true)
    end
  end

  class IsApiClientRequestValidTest < ActiveSupport::TestCase
    setup do
      @org = create(:organization)
      @client = create(:client, organization: @org)
      @u = create(:user, organization: @org)
      @p = {credential: @org.client.valid_credential, timestamp: Time.now, user_id: 123,
        user_email: @u.email, user_role: "member", resource_id: "1"}
      parts = [@p[:credential].shared_secret, @p[:timestamp], @p[:user_id],
        @p[:user_email], @p[:user_role], @p[:resource_id]].compact
      @token = Digest::SHA256.hexdigest(parts.join('|'))
    end

    test "should return true for valid token" do
      @p[:token] = @token
      assert ClientAuthentication.is_api_client_request_valid?(@p)
    end

    test "should return true with destiny_user_id" do
      parts = [@p[:credential].shared_secret, @p[:timestamp], @p[:user_id],
        @p[:user_email], @p[:user_role], @p[:resource_id], 1234].compact
      @token = Digest::SHA256.hexdigest(parts.join('|'))

      @p[:token] = @token
      @p[:destiny_user_id] = 1234
      assert ClientAuthentication.is_api_client_request_valid?(@p)
    end

    test "should return false for invalid token" do
      @p[:token] = @token
      refute ClientAuthentication.is_api_client_request_valid?(@p.merge!(resource_id: "invalid"))
    end
  end
end

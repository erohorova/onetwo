require 'test_helper'

class StreamTest < ActiveSupport::TestCase
  should belong_to(:user)
  should belong_to(:item)
  should belong_to(:group)

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
  end

  test 'should not create stream for private group unless open' do
    StreamCreateJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    pg = create(:private_group, access: 'open', resource_group: create(:resource_group))
    u1 = create(:user)
    assert_difference ->{Stream.count} do
      gu = GroupsUser.create(group: pg, user: u1, as_type: 'member')
      gu.run_callbacks(:commit)
    end

    pg.update_attributes(access: 'invite')
    u2 = create(:user)
    assert_no_difference ->{Stream.count} do
      gu = GroupsUser.create(group: pg, user: u2, as_type: 'member')
      gu.run_callbacks(:commit)
    end
  end

  test 'Message for all kind of posts should be appropriate' do

    @group = create(:group)
    private_group = create(:private_group, parent_id: @group.id)
    creator = create(:user)
    @group.add_member creator
    commenter = create(:user)
    @group.add_member commenter
    follower = create(:user)
    @group.add_member follower
    faculty1 = create(:user)
    faculty2 = create(:user)
    faculty3 = create(:user)
    @group.add_admin(faculty1)
    @group.add_admin(faculty2)

    announcement = create(:announcement, group: @group, user: faculty1)
    stream0 = create(:stream, item: announcement, user: faculty1, action: "posted")
    stream02 = create(:stream, item: announcement, user: commenter, action: "liked")
    stream03 = create(:stream, item: announcement, user: faculty2, action: "approved")
    assert_equal stream0.message, "made an announcement"
    assert_equal stream02.message, "liked the announcement"
    assert_equal stream03.message, "approved the announcement"

    com = create(:comment, commentable: announcement, user: commenter)
    stream1 = create(:stream, item: com, user: commenter, action: "commented")
    stream11 = create(:stream, item: com, user: follower, action: "liked")

    assert_equal stream1.message, "commented on discussion"
    assert_equal stream11.message, "liked the comment"

    question = build(:question, group: @group, user: commenter)
    stream2 = create(:stream, item: question, user: commenter, action: "asked")
    com1 = create(:comment, commentable: question, user: commenter)
    stream21 = create(:stream, item: com1, user: commenter, action: "commented")
    stream22 = create(:stream, item: question, user: follower, action: "liked")
    stream23 = create(:stream, item: question, user: faculty2, action: "approved")
    assert_equal stream2.message, "started discussion"
    assert_equal stream21.message, "commented on discussion"
    assert_equal stream22.message, "liked the discussion"
    assert_equal stream23.message, "approved the discussion"
    stream24 = create(:stream, item: private_group, user: faculty3, action: "joined")
    assert_equal stream24.message, "joined a group"

  end

  test 'should retrieve activities' do
    group     = create(:group)
    user      = create(:user)
    user2     = create(:user)
    commenter = create(:user)

    group.add_member user
    group.add_member user2
    group.add_member commenter

    question  = create(:question, group: group, user: user)
    stream1   = create(:stream, item: question, user: user, action: "posted")
    stream2   = create(:stream, item: question, user: commenter, action: "liked")
    stream3   = create(:stream, item: question, user: user2, action: "approved")
    stream4   = create(:stream, item: question, user: user2, action: "asked")
    stream5   = create(:stream, item: question, user: user2, action: "commented")

    # only posted, commented, asked stream activities
    assert_equal [stream1, stream4, stream5], Stream.forum_activities
  end

  test 'should fetch item snippet' do
    group     = create(:group)
    user      = create(:user)
    group.add_member user

    question  = create(:question, group: group, user: user, title: "message")
    stream1   = create(:stream, item: question, user: user, action: "posted")
    assert_equal 'message', stream1.snippet

    answer = create(:answer, commentable: question, user: user, message: "<p>this is paragraph</p>")
    stream2 = create(:stream, item: answer, user: user, action: "commented")
    assert_equal 'this is paragraph', stream2.snippet

    reply = create(:answer, commentable: answer, user: user, message: "<p>this is reply</p>")
    stream3 = create(:stream, item: reply, user: user, action: "commented")
    assert_equal 'this is reply', stream3.snippet
  end
end

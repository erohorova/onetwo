require 'test_helper'

class CardMetricsAggregationTest < ActiveSupport::TestCase

  setup do
    @klass = CardMetricsAggregation
  end

  test 'validations' do
    record = @klass.new
    required_attrs = [
      :organization_id, :card_id,
      :num_likes,       :num_views,
      :num_completions, :num_bookmarks,
      :num_comments,    :end_time,
      :start_time,      :num_assignments
    ]
    expected = required_attrs.each_with_object({}) do |attr, memo|
      memo[attr] = ["can't be blank"]
    end
    actual = record.tap(&:valid?).errors.messages
    assert_equal expected, actual
  end

  # NOTE: this is intentional behavior.
  # We bulk-create these records using data we get from influx.
  # Therefore the referenced ids may no longer exist in SQL
  test 'does not check validity of foreign keys' do
    record = @klass.new(
      organization_id: 0,
      card_id: 0,
      num_likes: 0,
      num_completions: 0,
      num_comments: 0,
      start_time: 0,
      end_time: 0,
      num_views: 0,
      num_bookmarks: 0,
      num_assignments: 0
    )
    assert record.valid?
    record.save
    assert record.persisted?
  end

end

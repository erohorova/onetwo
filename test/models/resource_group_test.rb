require 'test_helper'

class ResourceGroupTest < ActiveSupport::TestCase
  should have_many(:private_groups).with_foreign_key(:parent_id)

  should validate_presence_of :creator
  should validate_presence_of :description

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    @user     = create :user
    @s3_json  = File.read("#{Rails.root}/test/fixtures/api/s3_course_structure.json")
    @org = create(:organization, client: create(:client))
    @group    = create :resource_group, course_data_url: 'http://s3-url.com/text', start_date: "2016-05-23", organization: @org
    @group.admins << @user
    @credential = @group.valid_credential
    @group.reload
  end

  test 'cache_key with `client_resource_id` and `user_id` when user is passed in' do
    Rails.cache.expects(:fetch).with("savannah-course-simple-#{@group.client_resource_id}-#{@user.id}", expires_in: 1.hour, unless_nil: true)
    @group.get_cache_course_structure({user: @user})
  end

  test 'cache_key with `client_resource_id` when user is not passed in' do
    Rails.cache.expects(:fetch).with("savannah-course-#{@group.client_resource_id}", expires_in: 1.hour, unless_nil: true)
    @group.get_cache_course_structure
  end

  test 'proper headers with client.name and user.email when user is passed in' do
    stub_request(:get,
      "http://localhost:3000/api/offerings/#{@group.client_resource_id}/course_blocks").
      with(headers: {
        'Accept' => 'application/json',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'Content-Type' => 'application/json',
        'User-Agent' => 'Faraday v0.9.2',
        'X-Api-Key' => @credential.api_key,
        'X-Shared-Secret' => @credential.shared_secret,
        'X-Client-Name' => @group.app_name,
        'X-User-Email' => @user.email
      }).
      to_return(status: 200, body: @s3_json, headers: {})
    @group.get_cache_course_structure({user: @user})
  end

  test 'no headers with client.name and user.email when user is not passed in' do
    stub_request(:get,
      "http://localhost:3000/api/offerings/#{@group.client_resource_id}/course_blocks").
      with(headers: {
        'Accept' => 'application/json',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'Content-Type' => 'application/json',
        'User-Agent' => 'Faraday v0.9.2',
        'X-Api-Key' => @credential.api_key,
        'X-Shared-Secret' => @credential.shared_secret
      }).
      to_return(status: 200, body: @s3_json, headers: {})
    @group.get_cache_course_structure
  end

  test 'resource group should have creator' do
    org = create(:organization)
    group = build :resource_group, creator: nil, organization: org
    refute group.valid?
    assert_equal group.errors.full_messages[0], "Creator can't be blank"
  end
end

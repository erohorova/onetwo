require 'test_helper'

class MentionTest < ActiveSupport::TestCase

  test 'should extract plain mentions' do
    users = []
    org = create(:organization)
    users << simple_handle_user = create(:user, handle: '@simple', organization: org)
    users << number_handle_user = create(:user, handle: '@fnamelname1', organization: org)
    users << dot_handle_user = create(:user, handle: '@fname-lname', organization: org)
    users << dash_handle_user = create(:user, handle: '@fname_lname', organization: org)
    users << complicated_handle_user = create(:user, handle: '@fname1-lname2-lname3somemore', organization: org)
    text = "#{simple_handle_user.handle} some text #{number_handle_user.handle} #{dot_handle_user.handle} some more text #{dash_handle_user.handle} #{complicated_handle_user.handle} some more text @non-existanthandle"
    mentioned_users = Mention.extract_mentioned_users(text, org.id)
    assert_equal 5, mentioned_users.count
    assert_same_elements users, mentioned_users
  end

  test 'should not create mention notification for same @handle with other org' do
    org = create(:organization)
    org1 = create(:organization)
    simple_handle_user = create(:user, handle: '@simple', organization: org)
    other_org_user = create(:user, handle: '@simple', organization: org1)
    text = "@simple some text"
    assert_same_elements [simple_handle_user], Mention.extract_mentioned_users(text, org.id)
    assert_same_elements [other_org_user], Mention.extract_mentioned_users(text, org1.id)
  end

  test "card_not_hidden_and_published? #not published" do
    card = create(:card, card_type: 'media', state: 'draft', card_subtype: 'text')
    mention = create(:mention, mentionable: card)
    assert_equal mention.card_not_hidden_and_published?, false

    card = create(:card, card_type: 'media', card_subtype: 'text')
    mention = create(:mention, mentionable: card)
    assert mention.card_not_hidden_and_published?
  end

  test 'card_not_hidden_and_published? #hidden' do
    card = create(:card, hidden: true)
    mention = create(:mention, mentionable: card)
    assert_equal mention.card_not_hidden_and_published?, false

    card = create(:card)
    mention = create(:mention, mentionable: card)
    assert mention.card_not_hidden_and_published?
  end

  test '#in_published_comment?' do
    org = create(:organization)
    author, commentor, mention_user = create_list(:user, 3, organization: org)
    card = create(:card, author: author)
    comment = create(:comment, commentable: card, hidden: true, user: commentor)
    mention = create(:mention, mentionable: comment, user: mention_user)
    assert_equal mention.in_published_comment?, false

    card = create(:card, author: author)
    comment = create(:comment, commentable: card, user: commentor)
    mention = create(:mention, mentionable: comment, user: mention_user)
    assert mention.in_published_comment?
  end

  test '.notify?' do
    org = create(:organization)
    author, commentor, mention_user = create_list(:user, 3, organization: org)
    card = create(:card, author: author)

    comment = create(:comment, commentable: card, user: commentor)
    mention = create(:mention, mentionable: comment, user: mention_user)
    assert mention.notify?

    card = create(:card, card_type: 'pack', state: 'draft', card_subtype: 'text')
    mention = create(:mention, mentionable: card, user: mention_user)
    assert_equal mention.notify?, false
  end
end

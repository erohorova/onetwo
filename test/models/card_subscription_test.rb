require 'test_helper'

class CardSubscriptionTest < ActiveSupport::TestCase
  should have_db_column(:organization_id).of_type(:integer)
  should have_db_column(:card_id).of_type(:integer)
  should have_db_column(:user_id).of_type(:integer)
  should have_db_column(:start_date).of_type(:datetime)
  should have_db_column(:end_date).of_type(:datetime)
  should have_db_column(:transaction_id).of_type(:integer)

  should belong_to(:card)
  should belong_to(:user)

  should validate_presence_of(:organization_id)
  should validate_presence_of(:card_id)
  should validate_presence_of(:user_id)
  should validate_presence_of(:start_date)
  should validate_presence_of(:transaction_id)

  test 'should trigger email notification job on create' do
    user = create(:user)
    card = create(:card)
    order = create(:order, orderable: card)
    transaction = create(:transaction, order: order)
    user_card_subscription = create(:card_subscription, user_id: user.id, card_id: card.id, transaction_id: transaction.id)

    mailer_mock = mock()
    mailer_mock.expects(:deliver_later).returns(nil).times(1)
    PaymentMailer.expects(:user_card_subscription_notification).with(user_card_subscription.id)
                 .returns(mailer_mock).once

    user_card_subscription.run_callbacks(:commit)
  end

end

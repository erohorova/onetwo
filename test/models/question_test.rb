require 'test_helper'

class QuestionTest < ActiveSupport::TestCase

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    @org = create(:organization)
    @client = create(:client, organization: @org)
    @user = create(:user, organization: @org)
    @group = create(:resource_group, organization: @org, client: @client)
    @group.add_member @user
  end

  test "should save question" do
    question = build(:question, group: @group, user: @user)
    assert question.save
  end

  test 'question creator should have read the question on create' do
    Post.any_instance.unstub :p_read_by_creator

    post_id = 999
    PostRead.expects(:create).with(id: "post-id-#{post_id}-user-id-#{@user.id}", user_id: @user.id, post_id: post_id, group_id: @group.id)
    create(:question, user: @user, group: @group, id: post_id)
  end

  test "should not save question without group" do
    question = build(:question, user: @user, group_id: nil)
    question.postable = nil
    assert_not question.save
    assert question.errors.full_messages.include?("Postable can't be blank")
  end

  test "should not save question without creator" do
    question = build(:question, user: nil, group: @group)
    assert_not question.save
    assert question.errors.full_messages.include?("User can't be blank")
  end

  test "should be able to save question without a message" do
    question = build(:question, message: '', user: @user, group: @group)
    assert question.save
  end

  test "should not save question without a title" do
    question = build(:question, title: '', user: @user, group: @group)
    assert_not question.save
  end

  test "access answers for a question" do
    question = create(:question, user: @user, group: @group)
    answer = create(:answer, commentable: question, user: @user)
    assert_equal question.answers.count, 1

    assert_equal question.answers[0].message, answer.message
  end

  test 'should create and delete activity stream' do
    StreamCreateJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    stub_request(:post, %r{http://localhost:9200/_bulk})
    question = nil
    assert_difference -> {Stream.count} do
      question = create(:question, user: @user, group: @group)
      question.run_callbacks(:commit)
    end

    stream = Stream.last

    assert_equal stream.user, @user
    assert_equal stream.group, @group
    assert_equal stream.item, question

    assert_difference -> {Stream.count}, -1 do
      question.destroy
    end
  end

  test 'should not create a new activity stream if post is anonymous' do
    stub_request(:post, %r{http://localhost:9200/_bulk})
    assert_no_difference -> {Stream.count} do
      question = create(:question, user: @user, group: @group, anonymous: true)
      question.run_callbacks(:commit)
    end
  end
end

require 'test_helper'

class AssignmentTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper
  include CardHelperMethods

  should belong_to :assignee
  should belong_to :assignable
  should have_many :team_assignments
  should have_many(:teams).through(:team_assignments)

  test "should not create assignment for suspended user" do
    org = create :organization
    assignee = build :user, is_suspended: true, organization: org
    card = build :card, organization: org
    assignment = Assignment.new(assignable: card, assignee: assignee)

    refute assignment.valid?
    assert_not_empty assignment.errors
  end

  test "assignment creation pushes no event if assignable is not a card" do
    org = create :organization
    stream = ActivityStream.new organization: org
    assignee = create :user, organization: org
    Analytics::MetricsRecorderJob.unstub(:perform_later)
    Analytics::MetricsRecorderJob.expects(:perform_later).never
    assignment = create :assignment, assignable: stream, assignee: assignee
    assignment.run_callbacks(:commit)
  end

  test "assignment update pushes no event if assignable is not a card" do
    org = create :organization
    stream = ActivityStream.new organization: org
    assignee = create :user, organization: org
    assignment = create :assignment, assignable: stream, assignee: assignee
    Analytics::MetricsRecorderJob.unstub(:perform_later)
    Analytics::MetricsRecorderJob.expects(:perform_later).never
    assignment.dismiss!
    assignment.run_callbacks(:commit)
  end

  test "assignment update pushes no event if state is not updated to dismissed" do
    org = create :organization
    assignee = create :user, organization: org
    card = Card.create author: assignee, message: "foo"
    assignment = create :assignment, assignable: card, assignee: assignee, state: "dismissed"
    Analytics::MetricsRecorderJob.unstub(:perform_later)
    Analytics::MetricsRecorderJob.expects(:perform_later).never
    assignment.update! state: "assigned"
    assignment.run_callbacks(:commit)
  end

  test "assignment update records event if card is updated to dismissed state" do
    org = create :organization
    actor = create :user, organization: org
    card = create :card, organization: org, author: nil
    assignee = create :user, organization: org
    assignment = create :assignment, assignable: card, assignee: assignee

    RequestStore.unstub(:read)
    metadata = {
      user_id: actor.id,
      platform: "foo"
    }
    RequestStore.stubs(:read).returns metadata
    stub_time = Time.now

    Analytics::MetricsRecorderJob.unstub(:perform_later)

    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::CardAssignmentMetricsRecorder",
      org: Analytics::MetricsRecorder.org_attributes(org),
      card: Analytics::MetricsRecorder.card_attributes(card),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      assignee: Analytics::MetricsRecorder.user_attributes(assignee),
      timestamp: stub_time.to_i,
      additional_data: metadata,
      event: "card_assignment_dismissed"
    )

    Timecop.freeze(stub_time) do
      assignment.dismiss!
    end
  end

  test "assignment creation records event if assignable is a card" do
    org = create :organization
    actor = create :user, organization: org
    card = create :card, organization: org, author: nil
    assignee = create :user, organization: org

    RequestStore.unstub(:read)
    metadata = {
      user_id: actor.id,
      platform: "foo"
    }
    RequestStore.stubs(:read).returns metadata
    stub_time = Time.now

    Analytics::MetricsRecorderJob.unstub(:perform_later)

    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::CardAssignmentMetricsRecorder",
      org: Analytics::MetricsRecorder.org_attributes(org),
      card: Analytics::MetricsRecorder.card_attributes(card),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      assignee: Analytics::MetricsRecorder.user_attributes(assignee),
      timestamp: stub_time.to_i,
      additional_data: metadata,
      event: "card_assigned"
    )

    Timecop.freeze(stub_time) do
      assignment = create :assignment, assignable: card, assignee: assignee
      team_assignment =  create(:team_assignment, assignment: assignment, assignor: actor)
      assignment.run_callbacks :commit
    end
  end

  test 'should be able to assign card as an assignment' do
    org = create(:organization)
    card = create(:card, organization: org, author_id: nil)
    assignee = create(:user, organization: org)
    assignment = create(:assignment, assignable: card, assignee: assignee)

    assert_equal card, assignment.assignable
    assert_equal assignee, assignment.assignee
  end

  test 'should be able to assign pathways as an assignment' do
    setup_one_card_pack

    assignee = create(:user, organization: @org)
    assignment = create(:assignment, assignable: @cover, assignee: assignee)

    assert_equal @cover, assignment.assignable
    assert_equal assignee, assignment.assignee
  end

  test 'should be able to assign courses as an assignment' do
    organization = create(:organization)
    group = create(:resource_group, organization: organization)
    assignee = create(:user, organization: organization)
    assignment = create(:assignment, assignable: group, assignee: assignee)

    assert_equal group, assignment.assignable
    assert_equal assignee, assignment.assignee
  end

  test 'should not be able to change assignment actions' do
    actions = Assignment::EVENT_ACTIONS
    assert_raises RuntimeError do
      actions[:some_action] = "bam"
    end
  end

  test 'should set assignment notification due jobs' do
    org = create(:organization)
    assignee = create(:user, organization: org)
    assignor = create(:user, organization: org)
    card = create(:card, title: 'test-card', organization: org, author_id: nil)

    assert_enqueued_with(job: AssignmentDueNotificationJob) do
      assignment = create(:assignment, assignable: card, assignee: assignee, due_at: Date.current + 10.days)
      assignment.run_callbacks :commit
    end
  end

  test 'should add to learning queue on assignment create' do
    org = create(:organization)
    assignee = create(:user, organization: org)
    assignor = create(:user, organization: org)
    card = create(:card, title: 'test-card', organization: org, author_id: nil)

    assert_difference ->{LearningQueueItem.count}, 1 do
      assignment = create(:assignment, assignable: card, assignee: assignee, due_at: Date.current + 10.days)
      assignment.run_callbacks :commit
    end

    lq = LearningQueueItem.last
    assert_equal assignee, lq.user
    assert_equal card, lq.queueable
    assert_equal "assignments", lq.source
  end

  test 'should mark learning queue item as completed on assignment complete' do
    org = create(:organization)
    assignee = create(:user, organization: org)
    assignor = create(:user, organization: org)
    card = create(:card, title: 'test-card', organization: org, author_id: nil)
    assignment = nil
    assert_difference ->{LearningQueueItem.count}, 1 do
      assignment = create(:assignment, assignable: card, assignee: assignee, due_at: Date.current + 10.days)
      assignment.run_callbacks :commit
    end
    lq = LearningQueueItem.last
    assert_equal LearningQueueItem::ACTIVE_STATE, lq.state

    assignment.start!
    lq.reload
    assert_equal LearningQueueItem::ACTIVE_STATE, lq.state

    assignment.complete!
    lq.reload
    assert_equal LearningQueueItem::COMPLETION_STATE, lq.state
  end

  test 'assignment title should be less than 255 characters' do
    card = create(:card, title: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\\n\\n")
    assignment = build(:assignment)
    assignment.assignable = card
    assignment.save
    assert_equal 255, assignment.title.length
  end

  test 'should enqueue right number of delayed jobs' do
    org = create(:organization)

    assignee = create(:user, organization: org)
    assignor = create(:user, organization: org)
    card1, card2, card3, card4 = create_list(:card, 4, title: 'test-card1', author_id: nil, organization: org)

    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub :perform_later
    RequestStore.stubs(:read).returns({user_id: assignor.id})
    assert_enqueued_jobs 4 do
      assignment = create(:assignment, assignable: card1, assignee: assignee, due_at: Date.current + 11.days)
      team_assignment = create(:team_assignment, assignment: assignment, assignor: assignor)
      assignment.run_callbacks :commit
    end

    assert_enqueued_jobs 3 do
      assignment = create(:assignment, assignable: card2, assignee: assignee, due_at: Date.current + 5.days)
      assignment.run_callbacks :commit
    end

    assert_enqueued_jobs 2 do
      assignment = create(:assignment, assignable: card3, assignee: assignee, due_at: Date.current + 2.days)
      assignment.run_callbacks :commit
    end

    assert_enqueued_jobs 1 do
      assignment = create(:assignment, assignable: card4, assignee: assignee)
      assignment.run_callbacks :commit
    end
  end

  test 'email snippet' do
    user = create(:user)
    assignable = create(:card, message: "**this is bold**", title: "**this is bold**", author: user)
    assignment = create(:assignment, assignable: assignable, assignee: user)
    assert_equal '<p><strong>this is bold</strong></p>', assignment.email_snippet
  end

  #create notification
  test 'it should create notification for an assignment' do
    org = create(:organization)
    team = create(:team, organization: org)

    causal_user, assignee = create_list(:user, 2, organization: org)
    card = create(:card, author: causal_user)
    assignment = create(:assignment, assignable: card, assignee: assignee)
    team_assignment = create(:team_assignment, team_id: team, assignor: causal_user)

    notification = nil
    assert_difference -> {Notification.count} do
      notification = assignment.create_notification(causal_user_id: causal_user.id, notification_type: 'assigned', message: "This is a must read")
    end

    assert_not_nil notification
    assert_equal assignment, notification.notifiable
    assert_equal 'assigned', notification.notification_type
    assert_match /This is a must read/, notification.custom_message
  end

  #create notification
  test 'it should not create notification is user is not active' do
    org = create(:organization)
    team = create(:team, organization: org)
    causal_user, assignee = create_list(:user, 2, organization: org)
    card = create(:card, author: assignee)
    assignment = create(:assignment, assignable: card, assignee: assignee)
    team_assignment = create(:team_assignment, team_id: team, assignor: causal_user)
    assignee.update(is_suspended: true)
    notification = assignment.create_notification(causal_user_id: causal_user.id, notification_type: 'assigned', message: "This is a must read")

    assert_equal false, notification.valid?
    assert_equal 0, Notification.count
  end

  # create push notification
  test 'it should create push notification for an assignment' do
    org = create(:organization)
    team = create(:team, organization: org)

    causal_user, assignee = create_list(:user, 2, organization: org)
    card = create(:card, author: causal_user)
    assignment = create(:assignment, assignable: card, assignee: assignee)
    team_assignment = create(:team_assignment, team_id: team, assignor: causal_user)

    PubnubNotifier.expects(:notify).with({ user_id: assignee.id, deep_link_id: card.id, deep_link_type: 'card', item: assignment, notification_type: 'assignment_notice', content: 'Congratulations!', host_name: assignee.organization.host}, false).returns(true)
    assignment.create_push_notification(notification_type: 'assignment_notice', message: 'Congratulations!')
  end

  test 'assignments_per_assignor' do
    assert scrub(Assignment.assignments_per_assignor(user_id: 1, assignor_id: 2).to_sql).include?("assignments.user_id = 1 and team_assignments.assignor_id = 2")
  end

  test '#sort_by_due_date returns valid query' do
    org = create(:organization)
    user = create(:user, organization: org)
    upcoming_assignments_query = "assignments.due_at > DATE(NOW()) ORDER BY assignments.due_at asc) as upcoming_assignments"
    past_assignments_query = "assignments.due_at <= DATE(NOW()) ORDER BY assignments.due_at desc) as past_assignments"
    assignments_with_no_due_date = "AND assignments.due_at IS NULL ORDER BY assignments.created_at desc) as no_due_date_assignments"
    query = Assignment.sort_by_due_date(user, opts: { state: ['started', 'assigned'], limit: 10, viewer: user })
    assert scrub(query).include?(upcoming_assignments_query)
    assert scrub(query).include?(past_assignments_query)
    assert scrub(query).include?(assignments_with_no_due_date)
  end

  test '#sort_by_due_date should check for accessibility for org members' do
    org = create(:organization)
    user = create(:user, organization: org, organization_role: 'member')

    Assignment.expects(:get_accessibility_query).returns("").once
    Assignment.expects(:get_accessibility_condition).returns("").once

    query = Assignment.sort_by_due_date(user, opts: { state: ['started', 'assigned'], limit: 10 , viewer: user})
  end

  test '#sort_by_due_date should not check for accessibility for org admins' do
    org = create(:organization)
    admin_user = create(:user, organization: org, organization_role: 'admin')

    Assignment.expects(:get_accessibility_query).returns(" ").never
    Assignment.expects(:get_accessibility_condition).returns(" ").never

    query = Assignment.sort_by_due_date(admin_user, opts: { state: ['started', 'assigned'], limit: 10, viewer: admin_user })
  end

  test 'should belong to same org' do
    org1, org2 = create_list(:organization, 2)
    user = create(:user, organization: org1)
    card = create(:card, organization: org2, author_id: nil)
    assignment = build(:assignment, assignable: card, assignee: user)

    refute assignment.valid?
  end


  test 'schedule_due_notifications should trigger assignment due notifications when due_at is not nil' do
    org = create(:organization)
    user_list = create_list(:user, 2, organization: org)
    card = create(:card, author: user_list[0])

    assert_enqueued_with(job: AssignmentDueNotificationJob) do
      assignment = create(:assignment, user_id: user_list[1].id, assignable: card, due_at: Date.current + 10.days)
      assignment.run_callbacks :commit
    end
  end

  test 'schedule_due_notifications should not trigger assignment due notifications for completed assignment' do
    org = create(:organization)
    user_list = create_list(:user, 2, organization: org)
    card = create(:card, author: user_list[0])
    
    # To ensure AssignmentDueNotificationJob is not enqueued
    assert_no_enqueued_jobs do
      # No job should be enqueued from this block
      assignment = create(:assignment, user_id: user_list[1].id, assignable: card, state: 'completed', 
        completed_at: Time.now, due_at: (Time.now + 10.day))
      assignment.run_callbacks :commit
    end
  end

  class AssignedUserTest < ActiveSupport::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      @card_list = create_list(:card, 2, author: @user)
      @user_list = create_list(:user, 2, organization: @org)
      @team = create(:team, organization: @org)
    end

    test 'should fetch users the content is assigned to' do
      assignment1 = create(:assignment, assignable: @card_list[0], user_id: @user_list[0].id)
      assignment2 = create(:assignment, assignable: @card_list[0], user_id: @user_list[1].id)
      create(:team_assignment, team_id: nil, assignment_id: assignment1.id)
      create(:team_assignment, team_id: nil, assignment_id: assignment2.id)
      
      assigned_users = Assignment.assigned_users(q: nil, assignor_id: nil, team_id: nil,
        content_id: @card_list[0].id, state: nil)
      assert_same_elements assigned_users.map(&:assignee).map(&:id), @user_list.map(&:id)
    end

    test 'should fetch users the content is assigned to through teams and/or assignor id' do
      users_list = create_list(:user, 2, organization: @org)
      assignor = create(:user, organization: @org)
      users_list.each do |user|
        @team.add_member user
      end
      
      assignment1 = create(:assignment, assignable: @card_list[0], user_id: users_list[0].id)
      assignment2 = create(:assignment, assignable: @card_list[0], user_id: users_list[1].id)

      create(:team_assignment, team_id: @team.id, assignment_id: assignment1.id, assignor_id: assignor.id)
      create(:team_assignment, team_id: @team.id, assignment_id: assignment2.id)

      assigned_users = Assignment.assigned_users(q: nil, assignor_id: nil, team_id: @team.id,
        content_id: @card_list[0].id, state: nil)
      
      assert_same_elements users_list.map(&:id), assigned_users.map(&:assignee).map(&:id)
      assert_not_includes assigned_users.map(&:assignee).map(&:id), @user_list[0].id
      assert_not_includes assigned_users.map(&:assignee).map(&:id), @user_list[1].id

      assigned_users_through_assignor = Assignment.assigned_users(q: nil, assignor_id: assignor.id, team_id: @team.id,
        content_id: @card_list[0].id, state: nil)
      assert_equal assigned_users_through_assignor.map(&:assignee).map(&:id), [ users_list[0].id]
    end

    test 'should fetch users the content is assigned to based on query' do
      user1 = create(:user, organization: @org, first_name: 'abz', last_name: 'ert')
      user2 = create(:user, organization: @org, first_name: 'ertbnm', last_name: 'bnt')
      
      @team.add_member user1
      @team.add_member user2

      assignment1 = create(:assignment, assignable: @card_list[0], user_id: user1.id)
      assignment2 = create(:assignment, assignable: @card_list[0], user_id: user2.id)
      create(:team_assignment, team_id: @team.id, assignment_id: assignment1.id)
      create(:team_assignment, team_id: @team.id, assignment_id: assignment2.id)

      assigned_users = Assignment.assigned_users(q: 'abz', assignor_id: nil, team_id: @team.id,
        content_id: @card_list[0].id, state: nil)
      assert_same_elements [user1.id], assigned_users.map(&:assignee).map(&:id)
      assert_not_includes assigned_users.map(&:assignee).map(&:id), user2.id
    end

    test 'should fetch users the content is assigned to based on state of assignemnt' do
      users_list = create_list(:user, 2, organization: @org)
      users_list.each do |user|
        @team.add_member user
      end
      
      assignment1 = create(:assignment, assignable: @card_list[0], user_id: users_list[0].id, state: 'assigned')
      assignment2 = create(:assignment, assignable: @card_list[0], user_id: users_list[1].id, state: 'started')

      create(:team_assignment, team_id: @team.id, assignment_id: assignment1.id)
      create(:team_assignment, team_id: @team.id, assignment_id: assignment2.id)

      assigned_users = Assignment.assigned_users(q: nil, assignor_id: nil, team_id: nil,
        content_id: @card_list[0].id, state: 'started')
      assert_equal 1, assigned_users.count
      assert_same_elements [users_list[1].id], assigned_users.map(&:assignee).map(&:id)
      assert_not_includes assigned_users.map(&:assignee).map(&:id), users_list[0].id
    end
  end
end

require 'test_helper'

class OrganizationPriceTest < ActiveSupport::TestCase
  should belong_to(:org_subscription)

  should validate_presence_of(:amount)
  should validate_presence_of(:currency)


  test 'validates uniquess of card id scoped to currency' do
    org = create(:organization)
    org_subscription = org.org_subscriptions.create!(name: 'EdCast Spark - Annual Subscription Fees',
                                                     description: 'Your Spark subscription.',
                                                     start_date: Date.today,
                                                     end_date: Date.today + 1.year,
                                                     sku: "EdCast Instance name: Spark - Annual Subscription Fees")

    price  = create(:organization_price, org_subscription_id: org_subscription.id, currency: "INR", amount: "12")
    price1 = build(:organization_price, org_subscription_id: org_subscription.id, currency: "INR", amount: "12")
    refute price1.valid?
  end
end

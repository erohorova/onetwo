require 'test_helper'

class TeamTest < ActiveSupport::TestCase

  setup do
    @org = create(:organization)
    @team = create(:team, organization: @org)
    @user = create(:user, organization: @org)
  end

  should validate_presence_of(:name)
  should validate_presence_of(:description)
  should validate_presence_of(:organization)

  should have_many :team_assignments
  should have_many(:assignments).through(:team_assignments)

  def membership_groups
    g = @team.users.select('users.*, teams_users.as_type').group_by(&:as_type)
    g.default = []
    g.with_indifferent_access
  end

  test 'cleanup_team_users gets called' do
    @team.expects(:cleanup_team_users).once
    @team.destroy
  end

  test 'send_group_created_metric calls recorder' do
    stub_time
    Team.any_instance.unstub(:send_group_created_metric)
    @team = create :team, organization: @org, name: "foo"
    actor = User.first
    metadata = { user_id: actor.id }
    RequestStore.stubs(:read).returns(metadata)
    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        recorder: "Analytics::GroupExistenceMetricsRecorder",
        org: Analytics::MetricsRecorder.org_attributes(@org),
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        group: Analytics::MetricsRecorder.team_attributes(@team),
        timestamp: Time.now.to_i,
        event: "group_created",
        additional_data: metadata
      )
    )

    @team.run_callbacks(:commit) {@team.save}
  end

  test 'send_group_destroyed_metric calls recorder' do
    actor = User.first
    metadata = { user_id: actor.id }
    RequestStore.stubs(:read).returns(metadata)
    stub_time
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::GroupExistenceMetricsRecorder",
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      group: Analytics::MetricsRecorder.team_attributes(@team),
      org: Analytics::MetricsRecorder.org_attributes(@org),
      timestamp: Time.now.to_i,
      event: "group_deleted",
      additional_data: metadata
    )
    @team.destroy
    refute @team.persisted?
  end

  test 'updating group calls influx recorder' do
    org = create :organization
    actor = create :user, organization: org
    Team.any_instance.stubs :send_group_created_metric
    @team = create :team, organization: org, name: "foo", description: "A"
    stub_time
    metadata = { user_id: actor.id }
    RequestStore.expects(:read).with(:request_metadata).returns metadata
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::GroupEditedMetricsRecorder",
      event: "group_edited",
      changed_column: "description",
      old_val: "A",
      new_val: "B",
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      group: Analytics::MetricsRecorder.team_attributes(@team).merge(
        "updated_at" => Time.now.to_i,
        "description" => "B",
      ),
      additional_data: metadata,
      timestamp: Time.now.to_i
    )
    @team.update description: "B"
  end

  test 'membership methods' do
    @team.add_member(u2 = create(:user, organization: @org))

    assert_not @team.is_user?(@user)
    assert_not @team.is_admin?(@user)
    assert_not @team.is_member?(@user)
    mem = membership_groups
    assert_equal [], mem[:admin]
    assert_equal [u2], mem[:member]

    assert_difference ->{TeamsUser.count} do
      assert @team.add_member @user
      assert_not @team.add_member create(:user) # different org
    end

    assert @team.is_user?(@user)
    assert_not @team.is_admin?(@user)
    assert @team.is_member?(@user)
    mem = membership_groups
    assert_equal [], mem[:admin]
    assert_same_elements [@user, u2], mem[:member]

    counts = @team.membership_counts
    assert_same_elements %w[admin member], counts.keys
    assert_equal 0, counts[:admin]
    assert_equal 2, counts[:member]

    assert_no_difference ->{TeamsUser.count} do
      @team.add_admin @user
    end

    assert @team.is_user?(@user)
    assert @team.is_admin?(@user)
    assert_not @team.is_member?(@user)
    mem = membership_groups
    assert_equal [@user], mem[:admin]
    assert_equal [u2], mem[:member]
  end

  test 'removes users' do
    @team.add_admin @user
    @team.add_member(u2 = create(:user, organization: @org))

    assert_differences [
                           [->{TeamsUser.count}, -1],
                           [->{@team.teams_users.count}, -1],
                           [->{@team.users.count}, -1],
                           [->{@team.admins.count}, 0],
                           [->{@team.members.count}, -1],
                       ] do
      assert_equal [true, nil], @team.modify_user(u2, op: :remove)
      assert_equal [false, :not_in_team], @team.modify_user(create(:user), op: :remove)
      assert_equal [false, :sole_admin], @team.modify_user(@user.id, op: :remove)
    end
    mem = membership_groups
    assert_equal [@user], mem[:admin]
    assert_equal [], mem[:member]

    u3 = create(:user, organization: @org)
    @team.add_admin u3
    assert_difference ->{@team.admins.count}, -1 do
      assert_equal [true, nil], @team.modify_user(@user.id, op: :remove) # there's another admin now
    end
    assert_equal [u3], membership_groups[:admin]
  end

  test 'promotes/demotes users' do
    @team.add_admin @user
    @team.add_member(u2 = create(:user, organization: @org))

    assert_equal [false, :sole_admin], @team.modify_user(@user, op: :switch_role)
    assert_equal [true, nil], @team.modify_user(u2, op: :switch_role)
    assert @team.is_admin? u2
    assert_equal [true, nil], @team.modify_user(u2, op: :switch_role)
    assert @team.is_member? u2
  end

  test 'should update team slug on name change' do
    team = create(:team, name: 'test team')
    old_slug = team.slug

    team.update name: 'Product team'
    team.reload

    assert_not_equal old_slug, team.slug
  end

  test 'should create assignment for team members' do
    org = create(:organization)
    team = create(:team, organization: org)

    users = create_list(:user, 10, organization: org)
    admin_user = create(:user, organization: org)

    users.each {|u| team.add_member(u)}
    team.add_admin(admin_user)

    card = create(:card, author_id: admin_user.id)

    EDCAST_NOTIFY.notification_name_to_notifiers[Notify::NOTE_ASSIGNED_CONTENT].set_enabled(:email, :single)
    SendSingleNotificationJob.expects(:perform_later).never
    TestAfterCommit.enabled = true
    assert_differences [[->{ Assignment.count}, 10], [->{ TeamAssignment.count}, 10]] do
      team.create_team_assignments_for(assignee_ids: users.map(&:id), assignor_id: admin_user.id, assignable_type: 'card', assignable_id: card.id)
    end

    assert_equal TeamAssignment.last.assignor.id, admin_user.id
    assert_equal Assignment.last.assignable.id, card.id
    assert_includes Assignment.last(10).map(&:assignee).map(&:id), users.sample.id
  end

  test 'team assignment email notification should not send if disabled' do
    org = create(:organization)
    team = create(:team, organization: org)

    users = create_list(:user, 10, organization: org)
    admin_user = create(:user, organization: org)

    users.each {|u| team.add_member(u)}
    team.add_admin(admin_user)

    card = create(:card, author_id: admin_user.id)

    notifier =  EDCAST_NOTIFY.notification_name_to_notifiers[Notify::NOTE_ASSIGNED_CONTENT]
    notifier.set_disabled(:email, :single)

    EDCAST_NOTIFY.expects(:send_email).never

    assert_differences [[->{ Assignment.count}, 10], [->{ TeamAssignment.count}, 10]] do
      team.create_team_assignments_for(assignee_ids: users.map(&:id), assignor_id: admin_user.id, assignable_type: 'card', assignable_id: card.id)
    end
  end

  test 'should return list of active users in team' do
    User.any_instance.unstub :generate_user_onboarding
    @user.generate_user_onboarding
    @user.user_onboarding.start!
    @user.user_onboarding.complete!
    @team.add_admin @user

    new_user = create(:user, is_complete: false, organization: @org)
    @team.add_user @user
    assert_includes @team.active_users, @user
    assert_not_includes @team.active_users, new_user
  end

  test 'should validate team-name' do
    @new_team = build :team, name: @team.name, organization: @org
    refute @new_team.valid?
    assert_includes @new_team.errors[:name], "has already been taken"
  end

  test 'should validate team-name without case-sensitive' do
    team = create :team, organization: @org, name: 'foo', description: 'A'
    new_team = build :team, name: 'Foo', organization: @org

    refute new_team.valid?
    assert_includes new_team.errors[:name], 'has already been taken'
  end

  test 'should validate ability to make team mandatory' do
    team1 = build :team, name: 'some new team', organization: @org, is_mandatory: true
    team2 = build :team, name: 'some new team 2', organization: @org, is_mandatory: true, is_private: true
    team3 = build :team, name: 'some new team 2', organization: @org, is_dynamic: true
    refute team1.valid?
    assert team2.valid?
    assert team3.valid?
  end

  test 'should set is_mandatory to true for dynamic team' do
    org = create(:organization)
    team = create(:team, organization: org, is_dynamic: true)
    assert team.is_dynamic
    assert team.is_mandatory
  end

  test 'should set type admin and sub_admin for team' do
    org = create(:organization)
    Role.create_standard_roles org
    team = create(:team, organization: org)
    user = create(:user, organization: org)
    assert_difference -> {TeamsUser.count}, 2 do
      team.add_sub_admin(user)
      team.add_admin(user)
    end

    assert team.is_admin?(user)
    assert team.is_sub_admin?(user)
  end

  test 'should set only member type if user admin and sub_admin for team' do
    org = create(:organization)
    Role.create_standard_roles org
    team = create(:team, organization: org)
    user = create(:user, organization: org)
    team.add_sub_admin(user)
    team.add_admin(user)
    assert_difference -> {TeamsUser.count}, -1 do
      team.add_member(user)
    end

    refute team.is_admin?(user)
    refute team.is_sub_admin?(user)
  end

  test '#remove_users_type should set only member type if user admin or sub_admin remove type for team' do
    org = create(:organization)
    Role.create_standard_roles org
    team = create(:team, organization: org)
    user = create(:user, organization: org)
    team.add_sub_admin(user)
    team.remove_users_type([user.id], 'sub_admin')

    assert team.is_member?(user)
    refute team.is_sub_admin?(user)
  end

  test '#remove_users_type should remove type if user admin and sub_admin type for team' do
    org = create(:organization)
    Role.create_standard_roles org
    team = create(:team, organization: org)
    user = create(:user, organization: org)
    team.add_sub_admin(user)
    team.add_admin(user)
    team.remove_users_type([user.id], 'sub_admin')

    assert team.is_admin?(user)
    refute team.is_sub_admin?(user)
  end

  test '#add_admin should add role group_leader for admin user' do
    org = create(:organization)
    Role.create_standard_roles org
    team = create(:team, organization: org)
    user = create(:user, organization: org)
    team.add_admin(user)

    assert_includes user.user_roles_default_names, 'group_leader'
  end

  test '#add_sub_admin should add role group_admin for sub_admin user' do
    org = create(:organization)
    Role.create_standard_roles org
    team = create(:team, organization: org)
    user = create(:user, organization: org)
    team.add_sub_admin(user)

    assert_includes user.user_roles_default_names, 'group_admin'
  end

  test 'should not add role group_admin|group_leader for user type member in team' do
    org = create(:organization)
    Role.create_standard_roles org
    team = create(:team, organization: org)
    user = create(:user, organization: org)
    team.add_member(user)

    assert_not_includes user.user_roles_default_names, 'group_admin'
    assert_not_includes user.user_roles_default_names, 'group_leader'
  end

  test 'pins should be deleted on team deletion' do
    Pin.any_instance.stubs(:record_card_unpin_event)
    org = create(:organization)
    user = create(:user, organization: org)
    team = create(:team, organization: org)
    cards = create_list(:card, 2, author: user, organization: org)
    cards.each do |card|
      Pin.create(pinnable: team, object: card)
    end
    assert_difference -> {Pin.count}, -2 do
      team.destroy
    end
  end

  class TeamAnalyticsTest < ActiveSupport::TestCase
    setup do
      @org = create(:organization)
      @team = create(:team, organization: @org)

      @time_period_offsets_from = PeriodOffsetCalculator.applicable_offsets((Time.now - 1.year).utc.beginning_of_day)[:year]
      @time_period_offsets_to = PeriodOffsetCalculator.applicable_offsets(Time.now.utc.beginning_of_day)[:year]
      @timeranges = [[(Time.now - 1.year).utc.beginning_of_day, nil], [nil, Time.now.utc.beginning_of_day]]
    end

    # top contributors
    test "should return top contributors" do
      assignee = create(:user, organization: @org)
      team_user1, team_user2 = create_list(:user, 2, organization: @org)
      @team.add_user(team_user1)
      @team.add_user(team_user2)

      create_list(:card, 3, author_id: team_user1.id).each{|card|
        create(:shared_card, card_id: card.id, user_id: team_user1.id, team_id: @team.id)
      }

      assigned_cards = create_list(:card, 3, author_id: team_user2.id, organization: @org)
      assignment = create(:assignment, assignable: assigned_cards.first, assignee: assignee)
      create(:team_assignment, team_id: @team.id, assignment_id: assignment.id, assignor: team_user1)

      assigned_cards.last(2).each{|card|
        assignment = create(:assignment, assignable: card, assignee: assignee)
        create(:team_assignment, team_id: @team.id, assignment_id: assignment.id, assignor: team_user2)
      }

      top_contributors = @team.top_contributors(10, 0)

      assert_equal team_user1, top_contributors[0][:user]
      # 3 shared cards + 1 assgined card
      assert_equal 4, top_contributors[0][:score]
      assert_equal team_user2, top_contributors[1][:user]
      # 2 assgined card
      assert_equal 2, top_contributors[1][:score]
    end

    # metrics_over_span
    test "should return all metric values for the team" do

      TeamLevelMetric.expects(:get_team_metrics_over_span).returns({:smartbites_created=>0, :smartbites_consumed=>0})
      @team.expects(:get_new_users_count_over_span).returns(3)
      @team.expects(:real_users_count_before).returns(4)
      UserLevelMetric.expects(:get_organization_metrics_over_span).returns({:average_session=>0, :active_users=>0, :engagement_index=>0})

      metrics = @team
        .metrics_over_span(organization: @org, team_id: @team.id, period: :year, offsets: [@time_period_offsets_from, @time_period_offsets_to], timeranges: @timeranges)
      assert_equal 0, metrics[:smartbites_created]
      assert_equal 0, metrics[:smartbites_consumed]
      assert_equal 4, metrics[:total_users]
      assert_equal 3, metrics[:new_users]
      assert_equal 0, metrics[:average_session]
      assert_equal 0, metrics[:active_users]
      assert_equal 0, metrics[:engagement_index]
    end

    # drill_down_data_for_custom_period
    test "should return drilldowns of metric parameters for the team" do
      TeamLevelMetric.expects(:get_team_drill_down_data_for_timerange)
        .returns({:smartbites_created=>[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          :smartbites_consumed=>[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          :average_session=>[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          :active_users=>[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]})
      Team.any_instance.expects(:get_new_users_drill_down_data_for_timeranges).returns({:new_users=>[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]})

      drilldown_data = @team.drill_down_data_for_custom_period(organization: @org, team_id: @team.id, timerange: [(Time.now - 1.year).utc.beginning_of_day, Time.now.utc.beginning_of_day],period: :year)
      assert_same_elements [:smartbites_created, :smartbites_consumed, :average_session, :active_users, :total_users, :engagement_index, :new_users], drilldown_data.keys
    end

    test "should not create duplicate teams_users" do
      assignee = create(:user, organization: @org)
      team_user1, team_user2 = create_list(:user, 2, organization: @org)
      @team.add_user(team_user1)
      @team.add_user(team_user2)
      assert_equal 2, @team.teams_users.size

      @team.add_user(team_user1)
      assert_equal 2, @team.teams_users.size
    end

    test 'should create teams_users if user is not present as `member`' do
      assignee = create(:user, organization: @org)
      team_user1, team_user2 = create_list(:user, 2, organization: @org)
      @team.add_user(team_user1, as_type: 'admin')
      assert_equal 'admin', @team.teams_users.first.as_type
      assert_equal 1, @team.teams_users.size

      @team.add_user(team_user1)
      assert_equal 'member', @team.teams_users.first.as_type
      assert_equal 1, @team.teams_users.size
    end
  end
 end

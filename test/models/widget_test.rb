require 'test_helper'
class WidgetTest < ActiveSupport::TestCase
  # Associations
  should belong_to(:organization)
  should belong_to(:creator).class_name('User')
  should belong_to(:parent)

  # Validations
  should validate_presence_of(:creator)
  should validate_presence_of(:organization)
  should validate_presence_of(:parent)
  should validate_presence_of(:code)
  should validate_presence_of(:context)
  should validate_inclusion_of(:context).in_array(['channel'])
end

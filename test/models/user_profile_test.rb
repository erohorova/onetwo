require 'test_helper'

class UserProfileTest < ActiveSupport::TestCase

  should belong_to(:user)

  should have_db_column(:expert_topics).of_type(:text)
  should have_db_column(:learning_topics).of_type(:text)
  should have_db_column(:created_at).of_type(:datetime)
  should have_db_column(:updated_at).of_type(:datetime)

  should validate_presence_of(:user)

  setup do
    @sample_topic_1, @sample_topic_2 = get_sample_topics.map &:stringify_keys

    @new_topic = {
      "topic_name" => "new topic 1",
      "topic_id" => "new topic id 1",
      "topic_label" => "new topic label 1",
      "domain_name" => "new domain name 1",
      "domain_id" => "new domain id 1",
      "domain_label" => "new domain label 1"
    }
  end

  test '#should set tac_accepted_at if tac_accepted is set to true during creation' do
    profile = create :user_profile, tac_accepted: true
    assert_equal Date.today, profile.tac_accepted_at.to_date
  end

  test '#should not set tac_accepted_at if tac_accepted is set to false during creation' do
    profile = create :user_profile, tac_accepted: false
    assert_equal nil, profile.tac_accepted_at
  end

  test '#should set tac_accepted_at if tac_accepted is set to true during updation' do
    profile = create :user_profile
    profile.tac_accepted = true
    profile.save
    assert_equal Date.today, profile.tac_accepted_at.to_date
  end

  test '#expert_topics must hold duplicate keywords if keywords are from different domains' do
    topics  = Taxonomies.domain_topics
    profile = create :user_profile, expert_topics: topics

    assert_equal profile.reload.expert_topics, topics
  end

  test '#learning_topics must hold duplicate keywords if keywords are from different domains' do
    topics  = Taxonomies.domain_topics
    profile = create :user_profile, learning_topics: topics

    profile.save
    assert_equal profile.reload.learning_topics, topics
  end

  test 'saves valid time zone' do
    profile = create :user_profile

    profile.update(time_zone: "Asia/London")
    assert_includes profile.errors.messages[:time_zone], "Invalid time zone"

    profile.update(time_zone: "Asia/Kolkata")
    assert_equal "Asia/Kolkata", profile.time_zone
  end

  test 'validates dob to be greater than or equal to 13' do
    profile = create :user_profile
    profile.update(dob: Date.today - 5.years)
    assert_includes profile.errors.messages[:dob], "Users age has to be greater than or equal to 13"
  end

  test 'saves dob greater than or equal to 13' do
    profile = create :user_profile
    profile.update(dob: Date.today - 14.years)
    assert_empty profile.errors.messages
    assert_equal (Date.today - 14.years), profile.reload.dob
  end

  test 'validates value of level for learning_topics' do
    profile = create :user_profile
    @new_topic[:level] = '7'
    profile.update(learning_topics: [@new_topic])
    assert_equal profile.errors.messages[:learning_topics], ["Invalid value for learning topic level"]
  end

  test 'saves valid value of level for learning_topics' do
    profile = create :user_profile
    @new_topic[:level] = 2
    profile.update(learning_topics: [@new_topic])
    assert_equal 2, profile.learning_topics.first[:level]
  end

  test "will call record_user_profile_update_event" do
    org = create :organization
    user = create :user, organization: org
    profile = create :user_profile, user: user
    profile.expects(:record_user_profile_update_event)
    profile.run_callbacks(:commit) do
      profile.update(learning_topics: [@sample_topic_1])
    end
  end

  test "will call MetricsRecorderJob if learning_topics is changed" do
    org = create :organization
    user = create :user, organization: org
    stub_time = Time.now
    actor = create :user, organization: org
    profile = create :user_profile, user: user, learning_topics: [@sample_topic_1]
    UserProfile.any_instance.stubs(:find_actor).returns(actor)
    UserProfile.any_instance.unstub(:record_user_profile_update_event)
    metadata = { foo: "bar", user_id: actor.id }
    RequestStore.stubs(:read).returns metadata
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::UserProfileRecorder",
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      org: Analytics::MetricsRecorder.org_attributes(org),
      user_profile: Analytics::MetricsRecorder.user_profile_attributes(profile).merge(
        "updated_at" => stub_time.to_i),
      changed_profile_keys: { "learning_topics" => [[@sample_topic_1], [@sample_topic_2]] },
      timestamp: stub_time.to_i,
      metadata: metadata
    )
    profile.run_callbacks(:commit) do
      Timecop.freeze(stub_time) { profile.update(learning_topics: [@sample_topic_2]) }
    end
  end

  test "will call MetricsRecorderJob if expert_topics is changed" do
    org = create :organization
    user = create :user, organization: org
    actor = create :user, organization: org
    profile = create :user_profile, user: user, expert_topics: [@sample_topic_1]
    stub_time = Time.now
    metadata = { foo: "bar", user_id: actor.id }
    RequestStore.stubs(:read).returns metadata
    UserProfile.any_instance.unstub(:record_user_profile_update_event)
    UserProfile.any_instance.stubs(:find_actor).returns(actor)
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::UserProfileRecorder",
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      org: Analytics::MetricsRecorder.org_attributes(org),
      user_profile: Analytics::MetricsRecorder.user_profile_attributes(profile).merge(
        "updated_at" => stub_time.to_i
      ),
      changed_profile_keys: { "expert_topics" => [[@sample_topic_1], [@sample_topic_2]] },
      timestamp: stub_time.to_i,
      metadata: metadata
    )
    profile.run_callbacks(:commit) do
      Timecop.freeze(stub_time) { profile.update(expert_topics: [@sample_topic_2]) }
    end
  end

  test "will not call MetricsRecorderJob if another attribute is changed" do
    org = create :organization
    user = create :user, organization: org
    profile = create :user_profile, user: user
    Analytics::MetricsRecorderJob.expects(:perform_later).never
    profile.run_callbacks(:commit) do
      profile.update(language: "Tagalog")
    end
  end

  test "auto_follow_associated_channels will be triggered only when learning or expert topics are updated" do
    TestAfterCommit.with_commits do
      user_profile = create(:user_profile)
      create(:config, name: 'enable_channel_autofollow_based_on_topic', value: true, configable: user_profile.user.organization)
      channel = create(:channel, organization: user_profile.user.organization, topics: [@sample_topic_1])
      UserProfile.any_instance.expects(:auto_follow_associated_channels).once
      user_profile.update(learning_topics: [@sample_topic_1], expert_topics: [@sample_topic_2])
    end
  end

  test "auto_follow_associated_channels will not be triggered when config  is not set" do
    TestAfterCommit.with_commits do
      user_profile = create(:user_profile)
      UserProfile.any_instance.expects(:auto_follow_associated_channels).never
      user_profile.update(learning_topics: [@sample_topic_1], expert_topics: [@sample_topic_2])
    end
  end

  test "auto_follow_associated_channels should enqueue channel cards and follow channel based on topic association" do
    TestAfterCommit.with_commits do
      domain_topics = Taxonomies.domain_topics
      topics = [domain_topics[0], domain_topics[1], domain_topics[2]]
      user_profile = create(:user_profile)
      create(:config, name: 'enable_channel_autofollow_based_on_topic', value: true, configable: user_profile.user.organization)
      channel = create(:channel, organization: user_profile.user.organization, topics: [topics[0], topics[2]])

      create_list(:card, 3).each do |card|
        ChannelsCard.create(channel: channel, card: card)
      end

      OnFollowEnqueueJob.expects(:perform_later)
        .with(id: channel.id, type: channel.class.name, follower_id: user_profile.user_id).once
      user_profile.update(learning_topics: [topics[0], topics[1]], expert_topics: [topics[2]])

      assert_equal user_profile.user_id, channel.followers.first.id
    end
  end

  test "auto_follow_associated_channels should enqueue channel cards and follow channel based on topic association for all types of taxonomies" do
    TestAfterCommit.with_commits do
      topics = [{"topic":{"id": "4831898292327726074","path": "edcast.c6.cloud_computing.cloud_software_engineer",
              "name": "cloud_software_engineer","label": "Cloud Software Engineer"}}]
      user_profile = create(:user_profile)
      create(:config, name: 'enable_channel_autofollow_based_on_topic', value: true, configable: user_profile.user.organization)
      channel = create(:channel, organization: user_profile.user.organization, topics: topics)

      OnFollowEnqueueJob.expects(:perform_later)
          .with(id: channel.id, type: channel.class.name, follower_id: user_profile.user_id).once
        user_profile.update(learning_topics: topics)

      assert_equal user_profile.user_id, channel.followers.first.id
    end
  end

  test "auto_follow_associated_channels should not follow private channel based on topic association" do
    domain_topics = Taxonomies.domain_topics
    TestAfterCommit.with_commits do
      topics = [domain_topics[0], domain_topics[1], domain_topics[2]]
      user_profile = create(:user_profile)
      create(:config, name: 'enable_channel_autofollow_based_on_topic', value: true, configable: user_profile.user.organization)
      channel = create(:channel, organization: user_profile.user.organization, topics: [topics[0], topics[2]])
      private_channel = create(:channel, organization: user_profile.user.organization, is_private: true, topics: [topics[0], topics[2]])

      OnFollowEnqueueJob.expects(:perform_later)
        .with(id: channel.id, type: channel.class.name, follower_id: user_profile.user_id).once

      OnFollowEnqueueJob.expects(:perform_later)
        .with(id: private_channel.id, type: private_channel.class.name, follower_id: user_profile.user_id).never

      user_profile.update(learning_topics: [topics[0], topics[1]], expert_topics: [topics[2]])
      assert_equal [user_profile.user_id], channel.followers.pluck(:user_id)
      assert_equal [], private_channel.followers.pluck(:user_id)
    end
  end


  test 'should not auto assign pathways for users if config not enabled' do
    AutoAssignPathwaysForLearningJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    TestAfterCommit.with_commits do
      user = create :user
      AutoAssignPathwaysForLearningJob.expects(:perform_later).with(user.id).never
      user_profile = create(:user_profile, user: user)
    end
  end

  test 'should auto assign pathways for users if config enabled' do
    TestAfterCommit.with_commits do
      user = create :user
      create(:config, name: 'auto_assignment_feature_enabled', value: 'true', data_type: 'boolean', configable: user.organization)
      AutoAssignPathwaysForLearningJob.expects(:perform_later).with(user.id).once
      user_profile = create(:user_profile, user: user)
    end
  end

  test 'should not auto assign pathways for users if config enabled and learning_topics are not changed' do
    TestAfterCommit.with_commits do
      user = create :user
      create(:config, name: 'auto_assignment_feature_enabled', value: 'true', data_type: 'boolean', configable: user.organization)
      AutoAssignPathwaysForLearningJob.expects(:perform_later).with(user.id).never
      user_profile = create(:user_profile, user: user, learning_topics: nil)
    end
  end

  test 'should not add language the profile if not included in the list VALID_LANGUAGES' do
    org = create(:organization)
    user = create(:user, organization: org)
    user_profile = user.build_profile(language: 'invalid')
    user_profile.save
    message = user_profile.errors.messages[:language]

    assert_equal ['is not included in the list'], message
  end

  test 'should not update the profile if not included in the list VALID_LANGUAGES' do
    org = create(:organization)
    user = create(:user, organization: org)
    user.create_profile

    assert_nil user.language

    update_profile = user.profile.update(language: 'test')
    refute update_profile
  end

  test 'Should not be case sensitive when converting a language' do
    org = create(:organization)
    user = create(:user, organization: org)
    user_profile = user.build_profile(language: 'russian')
    user_profile.save

    assert_equal 'ru', user.language

    user.profile.update(language: 'En')

    assert_equal 'en', user.language

    user.profile.update(language: 'HINDI')

    assert_equal 'hi', user.language
  end

  test "records user_edited event when profile attributes change" do
    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub(:perform_later)

    stub_time = Time.now
    Time.stubs(:now).returns stub_time

    org = create :organization
    user = create :user, organization: org
    actor = create :user, organization: org
    profile = create :user_profile, user: user
    
    metadata = { user_id: actor.id }
    RequestStore.store[:request_metadata] = metadata

    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::UserMetricsRecorder",
      event: "user_edited",
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      org: Analytics::MetricsRecorder.org_attributes(org),
      user: Analytics::MetricsRecorder.user_attributes(user),
      changed_column: "dob",
      old_val: profile.dob.to_s,
      new_val: Date.new(0).to_s,
      timestamp: stub_time.to_i,
      additional_data: metadata
    )
    profile.update(dob: Date.new(0))
    profile.run_callbacks(:commit)
  end
end

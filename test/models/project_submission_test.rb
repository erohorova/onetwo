require 'test_helper'

class ProjectSubmissionTest < ActiveSupport::TestCase
  should have_db_column(:description).of_type(:text)
  should have_db_column(:submitter_id).of_type(:integer)
  should have_db_column(:project_id).of_type(:integer)
  should have_db_column(:filestack).of_type(:text)

  should belong_to(:submitter)
  should belong_to(:project)
  should have_many :project_submission_reviews

  test 'should list reviews_required project submissions only' do
    org = create(:organization)
    reviewer = create(:user, organization: org)
    card = create(:card, organization: org,
                  author: reviewer,
                  card_type: 'project',
                  card_subtype: 'text')
    users = create_list(:user, 6, organization: org)
    users.each_with_index do |user|
      create(:project_submission, project_id: card.project.id, submitter_id: user.id)
    end
    ProjectSubmissionReview.create(description: "something", project_submission_id: ProjectSubmission.first.id, reviewer_id: reviewer.id, status: 1)
    project_submissions = ProjectSubmission.reviews_required([card.project.id])

    assert_equal 5, project_submissions.count
    assert_equal ProjectSubmission.last(5).map { |ps| ps.id }, project_submissions.map { |ps| ps.id }
  end

  test 'should list awaiting_reviews project submissions only' do
    org = create(:organization)
    reviewer = create(:user, organization: org)
    card = create(:card, organization: org,
                  author: reviewer,
                  card_type: 'project',
                  card_subtype: 'text')
    users = create_list(:user, 5, organization: org)
    users.each_with_index do |user|
      create(:project_submission, project_id: card.project.id, submitter_id: user.id)
    end
    project_submissions = ProjectSubmission.reviews_required([card.project.id])

    assert_equal 5, project_submissions.count
    assert_equal ProjectSubmission.last(5).map { |ps| ps.id }, project_submissions.map { |ps| ps.id }
  end
end

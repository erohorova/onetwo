require 'test_helper'

class RechargePriceTest < ActiveSupport::TestCase
  should have_db_column(:recharge_id).of_type(:integer)
  should have_db_column(:currency).of_type(:string)
  should have_db_column(:amount).of_type(:decimal)

  should validate_presence_of(:amount)
  should validate_presence_of(:currency)
end

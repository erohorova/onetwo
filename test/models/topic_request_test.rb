require 'test_helper'

class TopicRequestTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper
  should validate_presence_of(:label)
  should validate_presence_of(:requestor_id)
  should validate_presence_of(:organization_id)

  should belong_to :requestor
  should belong_to :approver

  test "should not create duplicate label in same org" do
    org = create(:organization)
    topic_request = create(:topic_request, organization_id: org.id, label: "test")
    topic_request1 = build(:topic_request, organization_id: org.id, label: "test")
    refute topic_request1.valid?
  end

  test "should delete notification if topic request get destroyed" do
    TestAfterCommit.with_commits(true) do
      org = create(:organization)
      requestor = create(:user, organization: org)
      topic_request = create(:topic_request, organization_id: org.id, label: "test",requestor_id: requestor.id)
      notification = create(:notification, 
                            notifiable_type: 'TopicRequest', 
                            notifiable: topic_request, 
                            custom_message: "Your learning topic 'Drawings' has been approved and published.",
                            user_id: requestor.id ,
                            app_deep_link_id: topic_request.id)
      assert_difference -> {Notification.count}, -1 do
        topic_request.destroy
      end
    end
  end

  test "should push influx event when topic request is reject,approve,publish" do
    org = create :organization
    requester = create :user, organization: org
    actor = create(:user, organization: org, organization_role: 'admin')
    topic_request = create(
        :topic_request, organization_id: org.id, label: "test", 
        requestor_id: requester.id
        )
    
    metadata = { user_id: actor.id }
    
    states = ['pending','rejected','approved','published']
    states.each_with_index do |action,index|
      next if index == 0
      topic_request_attributes = {
          'id' => topic_request.id, 
          'label' => 'test', 
          'requestor_id' => topic_request.requestor_id, 
          'approver_id' => topic_request.approver_id, 
          'state' => action, 
          'publisher_id' => topic_request.publisher_id
        }
        
        RequestStore.expects(:read).with(:request_metadata).returns metadata
        Analytics::MetricsRecorderJob.expects(:perform_later).with(
            recorder: "Analytics::OrgEditedMetricsRecorder",
            event: "org_taxonomy_#{action}",
            actor: Analytics::MetricsRecorder.user_attributes(actor),
            org: Analytics::MetricsRecorder.org_attributes(org),
            timestamp: Time.now.to_i,
            taxonomy: topic_request_attributes,
            additional_data: metadata,
            changed_column: 'state',
            old_val: states[(index-1)],
            new_val: action
          )

    
        topic_request.reject! if action == 'rejected'
        topic_request.approve! if action == 'approved'
        topic_request.publish! if action == 'published'
        assert_equal topic_request.state, action
    end
  end

  test "should push influx event when topic request is created" do
    TestAfterCommit.enabled = true
    org = create :organization
    requester = create :user, organization: org
    actor = create(:user, organization: org, organization_role: 'admin')
    topic_request = TopicRequest.new(
        organization_id: org.id, label: "test", 
        requestor_id: requester.id
        )
    stub_time = Time.now
    metadata = { user_id: actor.id }    
    
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries(
        recorder: "Analytics::OrgMetricsRecorder",
        event: "org_taxonomy_created",
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        org: Analytics::MetricsRecorder.org_attributes(org),
        timestamp: Time.now.to_i,
        additional_data: metadata
        )
      )
    Timecop.freeze(stub_time) do
      topic_request.save
    end    
  end

  test "should push influx event when topic request is deleted" do
    TestAfterCommit.enabled = true
    org = create :organization
    requester = create :user, organization: org
    actor = create(:user, organization: org, organization_role: 'admin')
    topic_request = TopicRequest.create(
        organization_id: org.id, label: "test", 
        requestor_id: requester.id
        )
    stub_time = Time.now
    metadata = { user_id: actor.id }
    
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    Analytics::MetricsRecorderJob.expects(:perform_later).with( has_entries(
        recorder: "Analytics::OrgMetricsRecorder",
        event: "org_taxonomy_deleted",
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        org: Analytics::MetricsRecorder.org_attributes(org),
        taxonomy: Analytics::MetricsRecorder.topic_request_attributes(topic_request),
        timestamp: Time.now.to_i,
        additional_data: metadata
        )
      )
    Timecop.freeze(stub_time) do
      topic_request.destroy
    end    
  end

end

require 'test_helper'

class UserTopicLevelMetricTest < ActiveSupport::TestCase

  test 'update from increments' do
    user = create(:user)
    topic = create(:interest)

    assert_difference ->{UserTopicLevelMetric.count}, 5 do
      UserTopicLevelMetric.update_from_increments(opts: {user_id: user.id, tag_id: topic.id}, increments: [[:smartbites_score, 10], [:smartbites_created, 1], [:smartbites_consumed, 1]], timestamp: Time.at(Time.now.to_i))
    end
  end

  class KnowledgeMapTest < ActiveSupport::TestCase

    setup do
      skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'

      @org1, @org2 = create_list(:organization, 2)
      @org1_users = create_list(:user, 4, organization: @org1)
      @org2_users = create_list(:user, 2, organization: @org2)

      @topics = create_list(:interest, 5)
    end

    test 'top topics for an org' do

      @topics.each_with_index do |topic, indx|
        (@org1_users + @org2_users).each do |user|
          create(:user_topic_level_metric, tag_id: topic.id, user_id: user.id, period: :all_time, offset: 0, smartbites_score: (indx+1)*10)
        end
      end

      org1_top_topics = UserTopicLevelMetric.top_topics_for_organization(@org1.id)
      expected_topics = @topics.map.with_index{|topic, indx| {'id' => topic.id, 'name' => topic.name, 'smartbites_score' => (indx+1)*10*@org1_users.size, 'users_count' => @org1_users.size}}.reverse
      assert_equal(expected_topics, org1_top_topics)

      # blank state
      org3 = create(:organization)
      assert_equal([], UserTopicLevelMetric.top_topics_for_organization(org3))
    end

    test 'top topics for a user' do
      @topics.first(2).each_with_index do |topic, indx|
        create(:user_topic_level_metric, user: @org1_users.first, tag: topic, period: :all_time, offset: 0, smartbites_score: indx+1)
      end
      user1_topics = UserTopicLevelMetric.top_topics_for_user(@org1_users.first.id)
      expected_topics = @topics.first(2).map.with_index{|topic, indx| {'id' => topic.id, 'name' => topic.name, 'smartbites_score' => (indx+1), 'users_count' => 0}}.reverse
      assert_equal(expected_topics, user1_topics)

      # blank state
      new_user = create(:user)
      assert_equal([], UserTopicLevelMetric.top_topics_for_user(new_user.id))
    end

    test 'top users for a topic' do
      @org1_users.first(3).each_with_index do |user, indx|
        create(:user_topic_level_metric, user: user, tag: @topics.first, period: :all_time, offset: 0, smartbites_score: indx+1)
      end
      top_users = UserTopicLevelMetric.top_users_for_topic_in_organization(@org1.id, @topics.first.id)
      expected_users = @org1_users.first(3).map.with_index{|user, indx| {'id' => user.id, 'name' => user.name, 'smartbites_score' => indx+1}}.reverse
      assert_equal(expected_users, top_users)

      new_topic = create(:interest)
      assert_equal([], UserTopicLevelMetric.top_users_for_topic_in_organization(@org1.id, new_topic.id))
    end

    test 'knowledge map for organization' do
      limit = 5
      expected_knowledge_map = [{'id' => @topics.first.id, 'name' => @topics.first.name, 'smartbites_score' => 10, 'users_count' => 2}]
      UserTopicLevelMetric.expects(:top_topics_for_organization).with(@org1.id, limit).returns(expected_knowledge_map).once
      assert_equal({"topics" => expected_knowledge_map}, UserTopicLevelMetric.knowledge_map(@org1, @org1, limit))
    end

    test 'knowledge map for a user' do
      limit = 5
      expected_knowledge_map = [{'id' => @topics.first.id, 'name' => @topics.first.name, 'smartbites_score' => 10, 'users_count' => 2}]
      UserTopicLevelMetric.expects(:top_topics_for_user).with(@org1_users.first.id, limit).returns(expected_knowledge_map).once
      assert_equal({"topics" => expected_knowledge_map}, UserTopicLevelMetric.knowledge_map(@org1, @org1_users.first, limit))
    end

    test 'knowledge map for a topic' do
      limit = 5
      expected_knowledge_map = [{'id' => @org1_users.first.id, 'name' => @topics.first.name, 'smartbites_score' => 10, 'users_count' => 2}]
      UserTopicLevelMetric.expects(:top_users_for_topic_in_organization).with(@org1.id, @topics.first.id, limit).returns(expected_knowledge_map).once
      assert_equal({"users" => expected_knowledge_map}, UserTopicLevelMetric.knowledge_map(@org1, @topics.first, limit))
    end
  end
end

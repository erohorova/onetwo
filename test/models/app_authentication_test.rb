require 'test_helper'

class AppAuthenticationTest < ActiveSupport::TestCase
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    @org = create(:organization)
  end

  test "get organization if savannah_app_id is present" do
    request = Hash.new
    request.stubs(:headers).returns({'X-SAVANNAH-APP-ID' => @org.savannah_app_id})
    token = JWT.encode({"app_id" => @org.savannah_app_id}, Settings.savannah_token, 'HS256')
    params = {"auth_token" => token}
    org, jwt_payload, jwt_header = AppAuthentication.get_org(request: request , params: params)
    assert_equal org, @org
    assert_equal jwt_payload, {"app_id"=>@org.savannah_app_id}
    assert_equal jwt_header, {"typ"=>"JWT", "alg"=>"HS256"}

    #with headers
    request.stubs(:headers).returns({"X-API-AUTH-TOKEN" => token, "X-SAVANNAH-APP-ID" => @org.savannah_app_id})
    org, jwt_payload, jwt_header = AppAuthentication.get_org(request: request , params: {})
    assert_equal org, @org
    assert_equal jwt_payload, {"app_id"=>@org.savannah_app_id}
    assert_equal jwt_header, {"typ"=>"JWT", "alg"=>"HS256"}
  end

  test "should return nil if savannah_app_id is not present" do
    request = Hash.new
    request.stubs(:headers).returns({})
    token = JWT.encode({"app_id" => @org.savannah_app_id}, Settings.savannah_token, 'HS256')
    params = { "savannah_app_id" => nil, "auth_token" => token}
    org, jwt_payload, jwt_header = AppAuthentication.get_org(request: request , params: params)
    assert_nil org
    assert_nil jwt_payload
    assert_nil jwt_header

    #with headers
    request.stubs(:headers).returns({"X-API-AUTH-TOKEN" => token, "X-SAVANNAH-APP-ID" => nil})
    org, jwt_payload, jwt_header = AppAuthentication.get_org(request: request , params: {})
    assert_nil org
    assert_nil jwt_payload
    assert_nil jwt_header
  end

  class ApiAuthTest < ActiveSupport::TestCase
    setup do
      @org = create(:organization)
    end

    test "should nil if params are not present" do
      assert_nil AppAuthentication.api_auth!
    end

    test "should nil if savannah_app_id is not prsent in params" do
      assert_nil AppAuthentication.api_auth!(params: {})
    end

    test "should nil if destiny_user_id is not present" do
      assert_nil AppAuthentication.api_auth!(params: {savannah_app_id: @org.savannah_app_id})
    end

    test "should returns nil if token is invalid" do
      u = create(:user, organization: @org)
      params = {destiny_user_id: u.id, savannah_app_id: @org.savannah_app_id}
      AppAuthentication.stubs(:is_api_client_request_valid?).returns(false)
      assert_nil AppAuthentication.api_auth!(params: params)
    end

    test "should set the current user as destiny_user_id user passed as params" do
      create_user
      #set current_user with destiny_user_id
      params = {destiny_user_id: @u.id, savannah_app_id: @org.savannah_app_id}
      assert_no_difference -> {User.count} do
        org, current_user, auth_params, widget_request = AppAuthentication.api_auth!(params: params)
        assert_equal org, @org
        assert_equal current_user, @u
        assert_equal auth_params, {:destiny_user_id=>@u.id, savannah_app_id: @org.savannah_app_id}
        assert widget_request
      end
    end

    test "should create and set current_user as current_org user with email not from default org" do
      create_user

      user1 = create(:user)
      user2 = create(:user, email: user1.email, organization: @org)
      params = {destiny_user_id: 99999999, user_email: user1.email, savannah_app_id: @org.savannah_app_id}
      assert_no_difference -> {User.count} do
        org, current_user, auth_params, widget_request = AppAuthentication.api_auth!(params: params)
        assert_equal org, @org
        assert_equal current_user.email, user1.email
        assert_not_equal current_user, user1
        assert_equal auth_params, {:destiny_user_id=> 99999999, :user_email=> user1.email, savannah_app_id: @org.savannah_app_id}
        assert widget_request
      end
    end

    test "should not create new user for API request with same email with same savannah_app_id" do
      create_user
      #if email is present
      params = {destiny_user_id: 99999999, user_email: @u.email, savannah_app_id: @org.savannah_app_id}
      assert_no_difference -> {User.count} do
        org, current_user, auth_params, widget_request = AppAuthentication.api_auth!(params: params)
        assert_equal org, @org
        assert_equal current_user, @u
        assert_equal current_user.email, @u.email
        assert_equal auth_params, {:destiny_user_id=> 99999999, :user_email=> @u.email, savannah_app_id: @org.savannah_app_id}
        assert widget_request
      end

      #without email
      params = {destiny_user_id: 99999999, savannah_app_id: @org.savannah_app_id}
      assert_difference -> {User.count} do
        org, current_user, auth_params, widget_request = AppAuthentication.api_auth!(params: params)
        assert_equal org, @org
        assert_not_equal current_user, @u
        assert_nil current_user.email
        assert_equal auth_params, {:destiny_user_id=> 99999999, savannah_app_id: @org.savannah_app_id}
        assert widget_request
      end
    end

    def create_user
      @u = create(:user, organization: @org)
      AppAuthentication.stubs(:is_api_client_request_valid?).returns(true)
    end
  end

  class IsApiClientRequestValidTest < ActiveSupport::TestCase
    setup do
      @org = create(:organization)
      @u = create(:user, organization: @org)
      @p = {savannah_app_id: @org.savannah_app_id, timestamp: Time.now, user_id: 123, destiny_user_id: @u.id,
        user_email: @u.email, user_role: "member", resource_id: "1"}
      parts = [@p[:savannah_app_id], @p[:timestamp], @p[:user_id],
        @p[:user_email], @p[:user_role], @p[:resource_id], @p[:destiny_user_id]].compact
      @token = Digest::SHA256.hexdigest(parts.join('|'))
      @p[:auth_token] = @token
    end

    test "should return true for valid token" do
      assert AppAuthentication.is_api_client_request_valid?(@p)
    end

    test "should return false for invalid token" do
      refute AppAuthentication.is_api_client_request_valid?(@p.merge!(resource_id: "invalid"))
    end
  end
end
require 'test_helper'

class ClcChannelsRecordTest < ActiveSupport::TestCase
  should belong_to(:channel)
  should belong_to(:user)
  should belong_to(:card)
  should validate_presence_of(:channel_id)
  should validate_presence_of(:user_id)
  should validate_presence_of(:card_id)


  test 'validates uniquess of user scoped to card - channel' do
    org = create(:organization)

    card = create(:card, author_id: nil, organization: org)
    user = create(:user, organization: org)
    user1 = create(:user, organization: org)

    channel = create(:channel, organization: org)
    channel1 = create(:channel, organization: org)

    clc_channel_record  = create(:clc_channels_record, channel: channel, card: card, user: user)
    clc_channel_record1 = build(:clc_channels_record, channel: channel, card: card, user: user)

    refute clc_channel_record1.valid?
    assert_equal "User has already been taken", clc_channel_record1.full_errors

    clc_channel_record2  = build(:clc_channels_record, channel: channel, card: card, user: user1)
    assert clc_channel_record2.valid?
  end
end

require 'test_helper'

class XapiActivityTest < ActiveSupport::TestCase
  include CardHelperMethods
  should belong_to(:organization)

  setup do
    XapiActivity.any_instance.stubs(:push_to_lrs)
    @org = create(:organization, xapi_enabled: true)
    @user = create(:user, organization: @org)
  end

  test 'should be created when an activity stream is created' do
    card = create(:card, author: @user)
    assert_difference ->{ XapiActivity.count } do
      activity = ActivityStream.create(user: @user, organization: @org, streamable: card, action: ActivityStream::ACTION_CREATED_CARD)
      activity.run_callbacks(:commit)
    end
    xapi_activity = XapiActivity.last
    assert_equal xapi_activity.object_id, card.id
    assert_equal xapi_activity.object_definition, "http://adlnet.gov/expapi/activities/lesson"
    assert_equal xapi_activity.verb, XapiActivity::VERB_SHARED
  end

  test "should add ecl_id to the object id" do
    ecl_id = "fdff1507-6cab-4a93-bdab-29434d7c5f23"
    card = create(:card, author: @user, ecl_id: ecl_id)
    activity = ActivityStream.create(user: @user, organization: @org, streamable: card, action: ActivityStream::ACTION_CREATED_CARD)
    activity.run_callbacks(:commit)
    xapi_activity = XapiActivity.last
    new_object_url = xapi_activity.object_path
    assert_includes new_object_url, ecl_id
  end

  test 'should be created when an activity stream is created for pathway' do
    Card.any_instance.stubs :reindex
    pathway = create_pathway_without_index(@org, @user)
    assert_difference ->{ XapiActivity.count } do
      activity = ActivityStream.create(user: @user, organization: @org, streamable: pathway, action: ActivityStream::ACTION_CREATED_PATHWAY)
      activity.run_callbacks(:commit)
    end
    xapi_activity = XapiActivity.last
    assert_equal xapi_activity.object_id, pathway.id
    assert_equal xapi_activity.object_definition, "http://adlnet.gov/expapi/activities/module"
    assert_equal xapi_activity.verb, XapiActivity::VERB_SHARED
  end

  test 'should be created when an activity stream is created for upvoting a video-stream' do
    video_stream = create(:iris_video_stream, creator: @user)
    voter = create(:user, organization: @org)
    vote = create(:vote, votable: video_stream, voter: voter)
    assert_difference ->{ XapiActivity.count } do
      activity = ActivityStream.create(user: voter , organization: @org, streamable: vote, action: ActivityStream::ACTION_UPVOTE)
      activity.run_callbacks(:commit)
    end
    xapi_activity = XapiActivity.last
    assert_equal xapi_activity.object_id, video_stream.id
    assert_equal xapi_activity.object_definition, "http://adlnet.gov/expapi/activities/media"
    assert_equal xapi_activity.verb, XapiActivity::VERB_PREFERRED
  end

  test 'should be created when an activity stream is created for commenting a video-stream' do
    video_stream = create(:iris_video_stream, creator: @user)
    commentor = create(:user, organization: @org)
    comment = create(:comment, commentable: video_stream, user: commentor )
    assert_difference ->{ XapiActivity.count } do
      activity = ActivityStream.create(user: commentor, organization: @org, streamable: comment, action: ActivityStream::ACTION_COMMENT)
      activity.run_callbacks(:commit)
    end
    xapi_activity = XapiActivity.last
    assert_equal xapi_activity.object_id, video_stream.id
    assert_equal xapi_activity.object_definition, "http://adlnet.gov/expapi/activities/media"
    assert_equal xapi_activity.verb, XapiActivity::VERB_RESPONDED
  end

  test 'should not be created when an activity stream is created for pathway' do
    org = create(:organization)
    user = create(:user, organization: org)
    pathway = create_pathway_without_index(org, user)
    assert_no_difference ->{ XapiActivity.count } do
      activity = ActivityStream.create(user: user, organization: org, streamable: pathway, action: ActivityStream::ACTION_CREATED_PATHWAY)
      activity.run_callbacks(:commit)
    end
  end

  test 'should create activitystream when card author is changed' do
    card = create(:card, author: @user)
    assert_difference ->{ XapiActivity.count } do
      activity = ActivityStream.create(user: @user, organization: @org, streamable: card, action: ActivityStream::ACTION_CHANGED_AUTHOR)
      activity.run_callbacks(:commit)
    end
    xapi_activity = XapiActivity.last
    assert_equal xapi_activity.object_id, card.id
    assert_equal xapi_activity.object_definition, "http://adlnet.gov/expapi/activities/lesson"
    assert_equal xapi_activity.verb, XapiActivity::VERB_AUTHORED
  end

  test 'should push to lrs after creation' do
    XapiActivity.any_instance.unstub(:push_to_lrs)

    xapi_credential = create(:xapi_credential, organization: @org)
    xapi_activity = create(:xapi_activity, organization: @org)
    stub_post = stub_request(:post, "http://xapi.example.com/lrs/statements").
      to_return(:status => 200, :body => "", :headers => {})
    xapi_activity.run_callbacks(:commit)
    assert_requested(stub_post)
  end

  test "lrs hash should contain title and message" do
    card = create(:card, author: @user)
    activity = ActivityStream.create(user: @user, organization: @org, streamable: card, action: ActivityStream::ACTION_CHANGED_AUTHOR)
    activity.run_callbacks(:commit)
    xapi_activity = XapiActivity.last
    object = {:name=>{"en-US"=>"This is a cool card"}, :description=>{"en-US"=>nil}, :type=>"http://adlnet.gov/expapi/activities/lesson"}

    assert_equal xapi_activity.object_detail, object
  end
end

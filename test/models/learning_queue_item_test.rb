require 'test_helper'

class LearningQueueItemTest < ActiveSupport::TestCase

  setup do
    @user = create(:user)
    @org = @user.organization
  end

  test 'should set display type' do
    video_resource = create(:resource, type: 'Video')
    card = create(:card, card_type: 'media', card_subtype: 'video', resource: video_resource, author: @user)
    user = create(:user, organization: @org)
    lq = LearningQueueItem.new(user: user, queueable: card, source: "bookmarks")
    assert lq.valid?, message: lq.errors.full_messages
    assert(lq.display_type == "video")
  end

  test 'validate in same org' do
    org1, org2 = create_list(:organization, 2)
    org1_user = create(:user, organization: org1)
    org2_user = create(:user, organization: org2)

    card = create(:card, organization: org1, author_id: nil)

    assert LearningQueueItem.new(user: org1_user, source: "bookmarks",  queueable: card).valid?
    refute LearningQueueItem.new(user: org2_user, source: "bookmarks", queueable: card).valid?
  end
end

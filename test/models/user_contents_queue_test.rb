require 'test_helper'

class UserContentsQueueTest < ActiveSupport::TestCase

  self.use_transactional_fixtures = false

  setup do
    create_default_org
    User.any_instance.stubs(:reindex_async).returns(true)
    Card.any_instance.stubs(:record_card_create_event)

    WebMock.disable!
    UserContentsQueue.create_index! force: true
    UserContentsQueue.refresh_index!


    @org = create(:organization)
    @group = create(:group, organization: @org)
    @card = create(:card, card_type: 'media', card_subtype: 'video', author: create(:user, organization: @org))

    @reindex_cards = -> do
      if Card.searchkick_index.exists?
        Card.searchkick_index.delete
      end
      Card.reindex
      Card.searchkick_index.refresh
    end
    @reindex_cards.call
  end

  teardown do
    User.any_instance.unstub(:reindex_async)
    WebMock.enable!
  end

  def setup_queues
    # create queues for two users
    @u1id = create(:user).id
    @u2id = create(:user).id
    @queues = {@u1id => (1..5).to_a, @u2id => [10,3,7,8]}
    card_types = %w(type1 type2)
    @queues.each do |k,v|
      v.each {|cid| UserContentsQueue.create(user_id: k, content_id: cid, content_type: 'Card',
                                          metadata: [{image_url: 'http://someimage.png', text: 'boo'}],
                                          group_id: cid % 3, card_type: card_types[cid % 2])}
    end

    # some posts too
    @post_queues = {@u1id => [11,12,13,14], @u2id => [20,18,17,19]}
    @post_queues.each do |k,v|
      v.each {|pid| UserContentsQueue.create(user_id: k, content_id: pid, content_type: 'Post',
                                          metadata: [{image_url: 'http://someimage.png', text: 'boo'}],
                                          group_id: pid % 3)}
    end

    UserContentsQueue.gateway.refresh_index!
  end

  test 'should be able to save and retrieve an entry' do
    uc = UserContentsQueue.new(user_id: 1, content_id: 1, content_type: 'Card',
                            metadata: [{image_url: 'http://someimage.png', text: 'because a friend liked this'}],
                            group_id: 7, card_type: 'pokemon')
    assert uc.save

    UserContentsQueue.gateway.refresh_index!
    # Retrieve
    res = UserContentsQueue.search(filter: {term: { user_id: 1}})
    assert_equal 1, res.size
    uc_retrieved = res.first
    assert_equal 1, uc_retrieved.user_id
    assert_equal 1, uc_retrieved.content_id
    assert_equal 7, uc_retrieved.group_id
    assert_equal 'pokemon', uc_retrieved.card_type
  end

  test 'should be able to retrieve queue for a user' do
    setup_queues

    u1queue = UserContentsQueue.get_cards_queue_for_user(user_id: @u1id)
    assert_equal 5, u1queue.count
    assert_same_elements @queues[@u1id], u1queue.map{|ucq| ucq.content_id}

    # test posts retrieval
    u1queue_posts = UserContentsQueue.get_posts_queue_for_user(user_id: @u1id)
    assert_equal 4, u1queue_posts.count
    assert_same_elements @post_queues[@u1id], u1queue_posts.map{|ucq| ucq.content_id}

    u2queue = UserContentsQueue.get_cards_queue_for_user(user_id: @u2id)
    assert_equal 4, u2queue.count
    assert_same_elements @queues[@u2id], u2queue.map{|ucq| ucq.content_id}

    # posts
    u2queue_posts = UserContentsQueue.get_posts_queue_for_user(user_id: @u2id)
    assert_equal 4, u2queue_posts.count
    assert_same_elements @post_queues[@u2id], u2queue_posts.map{|ucq| ucq.content_id}

    # empty result set test
    empty = UserContentsQueue.get_cards_queue_for_user(user_id: 99)
    assert_equal [], empty

    # testing limit parameter
    top2_for_u1 = UserContentsQueue.get_cards_queue_for_user(user_id: @u1id, limit: 2)
    assert_equal 2, top2_for_u1.count
    # assert_equal @queues[@u1id].reverse.first(2), top2_for_u1.map{|ucq| ucq.content_id}

    # testing offset parameter
    next2_for_u1 = UserContentsQueue.get_cards_queue_for_user(user_id: @u1id, limit: 2, offset: 2)
    assert_equal 2, next2_for_u1.count
    # assert_equal @queues[@u1id].reverse.first(4).last(2), next2_for_u1.map{|ucq| ucq.content_id}

    assert_empty top2_for_u1.map{|ucq| ucq.content_id} & next2_for_u1.map{|ucq| ucq.content_id}

    # testing inclusion/exclusion
    assert_same_elements [1,4],
                         UserContentsQueue.get_cards_queue_for_user(
                             user_id: @u1id,
                             filter_params: {include_group_ids: [1]}).
                             map(&:content_id)
    assert_same_elements [1,3,5],
                         UserContentsQueue.get_cards_queue_for_user(
                             user_id: @u1id,
                             filter_params: {exclude_card_types: ['type1']}).
                             map(&:content_id)
    assert_equal [1],
                 UserContentsQueue.get_cards_queue_for_user(
                     user_id: @u1id,
                     filter_params: {
                       include_group_ids: [1],
                       exclude_card_types: ['type1']
                     }).
                     map(&:content_id)
    assert_same_elements [2,4],
                         UserContentsQueue.get_cards_queue_for_user(
                             user_id: @u1id,
                             filter_params: {include_card_types: ['type1']}).
                             map(&:content_id)
    # ignore empty
    assert_same_elements [2,4],
                         UserContentsQueue.get_cards_queue_for_user(
                             user_id: @u1id,
                             filter_params: {
                               include_group_ids: [],
                               include_card_types: ['type1']
                             }).
                             map(&:content_id)
    # ignore nil
    assert_same_elements [2,4],
                         UserContentsQueue.get_cards_queue_for_user(
                             user_id: @u1id,
                             filter_params: {
                               include_group_ids: nil,
                               include_card_types: ['type1']
                             }).
                             map(&:content_id)
  end

  test 'should be able to count queue for a user' do
    setup_queues

    assert_equal 5, UserContentsQueue.get_cards_queue_for_user(user_id: @u1id, method: :count)
    assert_equal 4, UserContentsQueue.get_cards_queue_for_user(user_id: @u2id, method: :count)
  end

  test 'should be able to enqueue for a user' do

    UserContentsQueue.gateway.refresh_index!
    uid = create(:user).id
    card_id = @card.id
    another_card = create(:card, card_type: 'media', card_subtype: 'video')

    assert UserContentsQueue.enqueue_for_user(user_id: uid, content_id: another_card.id, content_type: 'Card', metadata: {k1: 'vc2k1', k2: 'vc2k2'}, queue_timestamp: 1.minute.ago)
    assert UserContentsQueue.enqueue_for_user(user_id: uid, content_id: card_id, content_type: 'Card', metadata: {k1: 'vc1k1', k2: 'vc1k2'}, queue_timestamp: 2.minutes.ago)

    UserContentsQueue.gateway.refresh_index!

    queues = UserContentsQueue.get_cards_queue_for_user(user_id: uid)
    assert_equal [another_card.id, card_id], queues.map{|q| q.content_id}

    assert UserContentsQueue.enqueue_for_user(user_id: uid, content_id: card_id, content_type: 'Card', metadata: {k1: 'vc1k1_updated', k3: 'vc2k3'})

    UserContentsQueue.gateway.refresh_index!

    queues = UserContentsQueue.get_cards_queue_for_user(user_id: uid)
    assert_equal [card_id, another_card.id], queues.map{|q| q.content_id}

    c2 = queues.first
    assert_equal 1, c2.metadata.count
    assert_equal({'k1' => 'vc1k1_updated', 'k3' => 'vc2k3'}, c2.metadata.first)
  end

  test 'should not enqueue dismissed content for a user' do

    UserContentsQueue.gateway.refresh_index!
    uid = create(:user).id
    card_id = @card.id
    another_card = create(:card, card_type: 'media', card_subtype: 'video')
    DismissedContent.create(content:another_card, user_id:uid)

    assert_not UserContentsQueue.enqueue_for_user(user_id: uid, content_id: another_card.id, content_type: 'Card', metadata: {k1: 'vc2k1', k2: 'vc2k2'}, queue_timestamp: 1.minute.ago)
    assert UserContentsQueue.enqueue_for_user(user_id: uid, content_id: card_id, content_type: 'Card', metadata: {k1: 'vc1k1', k2: 'vc1k2'}, queue_timestamp: 2.minutes.ago)

    UserContentsQueue.gateway.refresh_index!

    queues = UserContentsQueue.get_cards_queue_for_user(user_id: uid)
    assert_equal [card_id], queues.map{|q| q.content_id}
  end

  test 'should not enqueue completed content for a user' do
    UserContentsQueue.gateway.refresh_index!
    uid = create(:user, organization: @org).id
    card_id = @card.id
    another_card = create(:card, author: create(:user, organization: @org))
    create(:user_content_completion, :completed, user_id: uid, completable: another_card)
    # UserContentCompletion.create(completable: another_card, user_id: uid, state: 'COMPLETED')

    assert_not UserContentsQueue.enqueue_for_user(user_id: uid, content_id: another_card.id, content_type: 'Card', metadata: {k1: 'vc2k1', k2: 'vc2k2'}, queue_timestamp: 1.minute.ago)
    assert UserContentsQueue.enqueue_for_user(user_id: uid, content_id: card_id, content_type: 'Card', metadata: {k1: 'vc1k1', k2: 'vc1k2'}, queue_timestamp: 2.minutes.ago)

    UserContentsQueue.gateway.refresh_index!

    queues = UserContentsQueue.get_cards_queue_for_user(user_id: uid)
    assert_equal [card_id], queues.map{|q| q.content_id}
  end

  test 'should not enqueue bookmarked content for a user' do
    UserContentsQueue.gateway.refresh_index!
    uid = create(:user, organization: @org).id
    card_id = @card.id
    another_card = create(:card, author: create(:user, organization: @org))
    Bookmark.create(bookmarkable: another_card, user_id: uid)

    assert_not UserContentsQueue.enqueue_for_user(user_id: uid, content_id: another_card.id, content_type: 'Card', metadata: {k1: 'vc2k1', k2: 'vc2k2'}, queue_timestamp: 1.minute.ago)
    assert UserContentsQueue.enqueue_for_user(user_id: uid, content_id: card_id, content_type: 'Card', metadata: {k1: 'vc1k1', k2: 'vc1k2'}, queue_timestamp: 2.minutes.ago)

    UserContentsQueue.gateway.refresh_index!

    queues = UserContentsQueue.get_cards_queue_for_user(user_id: uid)
    assert_equal [card_id], queues.map{|q| q.content_id}
  end

  test 'should be able to enqueue for a user with merged metadata' do
    uid = create(:user).id
    queue_user = create(:user)

    assert UserContentsQueue.enqueue_for_user(
               user_id: uid,
               content_id: queue_user.id,
               content_type: 'User',
               metadata: {'about' => %w[one]},
               merge_metadata: true,
           )
    assert UserContentsQueue.enqueue_for_user(
               user_id: uid,
               content_id: queue_user.id,
               content_type: 'User',
               metadata: {'about' => %w[two]},
               merge_metadata: true,
           )
    UserContentsQueue.gateway.refresh_index!

    queued = UserContentsQueue.get_queue_for_user(user_id: uid, content_type: 'User')
    assert_equal [queue_user.id], queued.map{|q| q.content_id}
    assert_same_elements %w[one two], queued.first.metadata.first['about']
  end

  test 'should be able to dequeue for a user' do
    UserContentsQueue.gateway.refresh_index!
    uid = create(:user).id
    card_id = @card.id

    assert UserContentsQueue.enqueue_for_user(user_id: uid, content_id: card_id, content_type: 'Card', metadata: {k1: 'vc1k1', k2: 'vc1k2'})

    # make sure it exists
    UserContentsQueue.gateway.refresh_index!

    queues = UserContentsQueue.get_cards_queue_for_user(user_id: uid)
    assert_equal [card_id], queues.map{|q| q.content_id}

    # testing dequeue
    assert UserContentsQueue.dequeue_for_user(user_id: uid, content_id: card_id, content_type: 'Card')
    UserContentsQueue.gateway.refresh_index!

    queues = UserContentsQueue.get_cards_queue_for_user(user_id: uid)
    assert_equal [], queues

    # trying to remove a non existant card from queue
    assert_not UserContentsQueue.dequeue_for_user(user_id: uid, content_id: card_id, content_type: 'Card')
  end

  def reject_timestamps! hash
    hash.reject!{|k,_| ['created_at', 'updated_at', 'timestamp'].include? k}
  end

  test 'should retrieve queue card contents for a user' do
    user = create(:user, organization: @org)
    cards = create_list(:card, 8, author: create(:user, organization: @org))
    cards.each_with_index do |card, i|
      UserContentsQueue.enqueue_for_user(
          user_id: user.id,
          content_id: card.id,
          content_type: 'Card',
          queue_timestamp: (10 - i).hours.ago,
      )
    end

    UserContentsQueue.gateway.refresh_index!
    @reindex_cards.call

    response = UserContentsQueue.get_cards_for_user(
        user_id: user.id,
        limit: 5,
        offset: 0,
    )
    assert_equal 5, response.limit
    assert_equal 0, response.offset
    assert_equal 8, response.total
    assert_equal cards.last(5).reverse.map(&:id), response.results.map{|c| c['id']}

    response = UserContentsQueue.get_cards_for_user(
        user_id: user.id,
        limit: 5,
        offset: 5,
    )
    assert_equal 5, response.limit
    assert_equal 5, response.offset
    assert_equal 8, response.total
    assert_equal cards.first(3).reverse.map(&:id), response.results.map{|c| c['id']}

    response = UserContentsQueue.get_cards_for_user(
        user_id: user.id,
        limit: 5,
        offset: 10,
    )
    assert_equal 5, response.limit
    assert_equal 10, response.offset
    assert_equal 8, response.total
    assert_empty response.results
  end

  test 'should retrieve contents in random order' do
    user = create(:user)
    queue_users = create_list(:user, 25)
    queue_users.each_with_index do |qu, i|
      UserContentsQueue.enqueue_for_user(
          user_id: user.id,
          content_id: qu.id,
          content_type: 'User',
          queue_timestamp: (25 - i).hours.ago,
      )
    end

    response = UserContentsQueue.get_queue_for_user(
        user_id: user.id,
        content_type: 'User',
        limit: 25,
        sort: :random,
    )
    assert_equal 25, response.size

    queue_user_ids = queue_users.reverse.map(&:id)
    response_ids = response.map(&:content_id)
    assert_same_elements queue_user_ids, response_ids
    # we could win the fields medal by proving randomness, or...cross fingers
    assert_not_equal queue_user_ids.reverse, response_ids
    assert_not_equal queue_user_ids, response_ids
  end

  def setup_bulk_queue
    @uid = create(:user).id
    @users = create_list(:user, 22)
    UserContentsQueue.enqueue_bulk_for_user(
        user_id: @uid,
        content_type: 'User',
        items: @users.map{|u| {
            content_id: u.id,
            metadata: {me: ["I'm #{u.id}"]},
        }},
    )
  end

  test 'should enqueue and retrieve bulk' do
    setup_bulk_queue

    queues = UserContentsQueue.get_queue_for_user(
        user_id: @uid,
        content_type: 'User',
        limit: 50,
    )
    assert_same_elements @users.map(&:id), queues.map(&:content_id)
    expected = @users.map{|u| [{'me' => ["I'm #{u.id}"]}]}
    result = queues.map(&:metadata)
    assert_same_elements expected, result, "#{expected - result}; #{result - expected}"

    # test updating & metadata merging
    UserContentsQueue.enqueue_bulk_for_user(
        user_id: @uid,
        content_type: 'User',
        items: @users.map{|u| {
            content_id: u.id,
            metadata: {'me' => ["I'm still #{u.id}"]},
        }},
        merge_metadata: true,
    )

    queues = UserContentsQueue.get_queue_for_user(
        user_id: @uid,
        content_type: 'User',
        limit: 50,
    )
    assert_equal @users.size, queues.size
    pick = queues.sample
    assert_same_elements ["I'm #{pick.content_id}", "I'm still #{pick.content_id}"],
                         pick.metadata.first['me']
  end

  test 'should enqueue and retrieve bulk without dismissed_content' do
    user = create(:user)
    cards= create_list(:card, 10)
    dismissed_card = cards.last
    DismissedContent.create(content:dismissed_card, user:user)

    UserContentsQueue.enqueue_bulk_for_user(
        user_id: user.id,
        content_type: 'Card',
        items: cards.map{|card| {
            content_id: card.id,
            metadata: {'content' => ["I'm still #{card.id}"]},
        }},
        merge_metadata: true,
    )

    queues = UserContentsQueue.get_queue_for_user(
        user_id: user.id,
        content_type: 'Card',
        limit: 50,
    )
    assert_equal 9, queues.size
    expected_cards = cards - [dismissed_card]
    assert_same_elements expected_cards.map(&:id), queues.map(&:content_id)
  end

  test 'should enqueue and retrieve bulk without completed content' do
    user = create(:user, organization: @org)
    cards = create_list(:card, 10, author: create(:user, organization: @org))
    completed_card = cards.last
    create(:user_content_completion, :completed, completable: completed_card, user: user)

    UserContentsQueue.enqueue_bulk_for_user(
        user_id: user.id,
        content_type: 'Card',
        items: cards.map{|card| {
            content_id: card.id,
            metadata: {'content' => ["I'm still #{card.id}"]},
        }},
        merge_metadata: true,
    )

    queues = UserContentsQueue.get_queue_for_user(
        user_id: user.id,
        content_type: 'Card',
        limit: 50,
    )
    assert_equal 9, queues.size
    expected_cards = cards - [completed_card]
    assert_same_elements expected_cards.map(&:id), queues.map(&:content_id)
  end

  test 'should enqueue and retrieve bulk without bookmarked content' do
    user = create(:user, organization: @org)
    cards = create_list(:card, 10, author: create(:user, organization: @org))

    bookmarked_card = cards.last
    Bookmark.create(bookmarkable: bookmarked_card, user: user)

    UserContentsQueue.enqueue_bulk_for_user(
        user_id: user.id,
        content_type: 'Card',
        items: cards.map{|card| {
            content_id: card.id,
            metadata: {'content' => ["I'm still #{card.id}"]},
        }},
        merge_metadata: true,
    )

    queues = UserContentsQueue.get_queue_for_user(
        user_id: user.id,
        content_type: 'Card',
        limit: 50,
    )
    assert_equal 9, queues.size
    expected_cards = cards - [bookmarked_card]
    assert_same_elements expected_cards.map(&:id), queues.map(&:content_id)
  end

  def set_buf_size(size)
    ElasticsearchPersistenceBuffer.send(
        :remove_const, :ENQUEUE_BUFFER_SIZE)
    ElasticsearchPersistenceBuffer.const_set(
        :ENQUEUE_BUFFER_SIZE, size)
  end

  test 'should buffer properly' do
    old_size = ElasticsearchPersistenceBuffer::ENQUEUE_BUFFER_SIZE
    set_buf_size 10

    # index twice with 10 items, once with 2
    bulk = sequence('bulk')
    UserContentsQueue.gateway.client.expects(:bulk).in_sequence(bulk).
        with {|pars| pars[:body].size == 10 }
    UserContentsQueue.gateway.client.expects(:bulk).in_sequence(bulk).
        with {|pars| pars[:body].size == 10 }
    UserContentsQueue.gateway.client.expects(:bulk).in_sequence(bulk).
        with {|pars| pars[:body].size == 2 }

    setup_bulk_queue

    set_buf_size old_size
  end

  test 'dequeue content' do
    UserContentsQueue.create(user_id: 1, content_id: 1, content_type: 'Card',
                          metadata: [{}])

    # video stream with id 1 also enqueued for users 1 and 2
    UserContentsQueue.create(user_id: 1, content_id: 1, content_type: 'VideoStream',
                          metadata: [{}])
    UserContentsQueue.create(user_id: 2, content_id: 1, content_type: 'VideoStream',
                          metadata: [{}])

    UserContentsQueue.gateway.refresh_index!

    res = UserContentsQueue.get_queue_for_user(user_id: 1, content_type: 'VideoStream')
    assert_equal [1], res.map(&:content_id)
    res = UserContentsQueue.get_queue_for_user(user_id: 2, content_type: 'VideoStream')
    assert_equal [1], res.map(&:content_id)

    UserContentsQueue.dequeue_content(content_id: 1, content_type: 'VideoStream')
    UserContentsQueue.gateway.refresh_index!


    res = UserContentsQueue.get_queue_for_user(user_id: 1, content_type: 'VideoStream')
    assert_equal [], res
    res = UserContentsQueue.get_queue_for_user(user_id: 2, content_type: 'VideoStream')
    assert_equal [], res
  end

  test 'priority for queue' do
    # update: All the content type have same priority
    # Streams
    assert_equal 1, UserContentsQueue.get_queue_priority_for(content: VideoStream.new)

    # Videos
    c = Card.new(card_type: 'media', card_subtype: 'video')
    assert_equal 1, UserContentsQueue.get_queue_priority_for(content: c)

    # Pack
    c = Card.new(card_type: 'pack', card_subtype: 'text')
    assert_equal 1, UserContentsQueue.get_queue_priority_for(content: c)

    c = Card.new(card_type: 'media', card_subtype: 'text')
    assert_equal 1, UserContentsQueue.get_queue_priority_for(content: c)

    c = Card.new(card_type: 'media', card_subtype: 'link')
    assert_equal 1, UserContentsQueue.get_queue_priority_for(content: c)

    assert_equal 1, UserContentsQueue.get_queue_priority_for(content: User.new)
  end

  test 'should retrieve topics from queue' do
    u1id = 1
    u2id = 2
    queues = {u1id => (1..30).to_a, u2id => (1..50).to_a}
    card_types = %w(type1 type2)
    # tag3=20, tag4=15, tag2=10, tag1=5
    card_tags1 = ['tag3 x', 'tag4', 'tag2 a', 'tag1']
    # tag3=20, tag5=20, tag=6, tag4=15, tag2=10, tag1=5
    card_tags2 = ['tag3 x', 'tag5', 'tag6', 'tag4', 'tag2 a', 'tag1']
    queues.each do |k,v|
      v.each do |cid|
        tags = ['tag2 a'] if cid <= 10
        tags = ['tag1', 'tag3 x'] if cid > 10 && cid <= 15
        tags = ['tag3 x', 'tag4'] if cid > 15 && cid <= 30
        tags = ['tag5', 'tag6'] if cid > 30 && cid <= 50
        UserContentsQueue.create(user_id: k, content_id: cid, content_type: 'Card',
          metadata: [{image_url: 'http://someimage.png', text: 'boo'}],
          group_id: cid % 3, card_type: card_types[cid % 2], tags: tags,
          created_at: 1.day.ago
        )
      end
    end
    UserContentsQueue.gateway.refresh_index!
    topics = UserContentsQueue.feed_topics_for_user(user_id: u1id)
    assert_equal card_tags1, topics

    topics = UserContentsQueue.feed_topics_for_user(user_id: u2id)
    assert_equal card_tags2, topics
  end

  test 'should not fetch topics for content that enqueued more than month ago' do
    u1id = 1
    queues = {u1id => (1..5).to_a}
    card_types = %w(type1 type2)
    card_tags1 = %w(tag1 tag2)
    card_tags2 = %w(tag3 tag4)
    queues.each do |k,v|
      v.each {|cid| UserContentsQueue.create(user_id: k, content_id: cid, content_type: 'Card',
                                          metadata: [{image_url: 'http://someimage.png', text: 'boo'}],
                                          group_id: cid % 3, card_type: card_types[cid % 2], tags: [card_tags1[cid % 2]],
                                          created_at: 35.days.ago
                                          )}
    end

    queues.each do |k,v|
      v.each {|cid| UserContentsQueue.create(user_id: k, content_id: cid, content_type: 'VideoStream',
                                            metadata: [{image_url: 'http://someimage.png', text: 'boo'}],
                                            group_id: cid % 3, card_type: card_types[cid % 2], tags: [card_tags2[cid % 2]],
                                            created_at: 2.days.ago
                                          )}
    end

    UserContentsQueue.gateway.refresh_index!
    topics = UserContentsQueue.feed_topics_for_user(user_id: u1id)
    assert_same_elements card_tags2, topics
  end

  test 'remove from queue' do
    UserContentsQueue.unstub :remove_from_queue

    user = create(:user, organization: @org)
    stream = create(:wowza_video_stream, creator: create(:user, organization: @org))
    UserContentsQueue.expects(:dequeue_for_user).with(user_id: user.id, content_id:stream.id, content_type:'VideoStream')
    UserContentsQueue.remove_from_queue(user, stream)
  end

  test 'clear docs older than' do
    Timecop.freeze(2.months.ago) do
      # Older content
      UserContentsQueue.create(user_id: 1, content_id: 1, content_type: 'card', priority: 1)
    end

    # New content
    UserContentsQueue.create(user_id: 1, content_id: 1, content_type: 'video_stream', priority: 2)

    UserContentsQueue.gateway.refresh_index!
    assert_equal 2, UserContentsQueue.count(query: {filtered: {filter: {term: {user_id: 1}}}})

    UserContentsQueue.clear_docs_older_than(1.month.ago.to_i)
    UserContentsQueue.gateway.refresh_index!
    assert_equal 1, UserContentsQueue.count(query: {filtered: {filter: {term: {user_id: 1}}}})

    ucq = UserContentsQueue.search(query: {filtered: {filter: {term: {user_id: 1}}}}).results.first
    assert_equal 'video_stream', ucq.content_type
  end

  test 'honors from time filter' do
    user = create(:user)
    followable = create(:user)
    cards = create_list(:card, 6, author_id: followable.id)
    older_cards = create_list(:card, 6, author_id: followable.id, created_at: 2.weeks.ago)

    cards.each do |card|
      UserContentsQueue.create(user_id: user.id, content_id: card.id, content_type: 'Card', metadata: [{'rationale' => {'type' => 'following_user', 'user_id' => followable.id}}], last_enqueue_event_time: Time.now.utc - 15.hours)
    end
    older_cards.each do |card|
      UserContentsQueue.create(user_id: user.id, content_id: card.id, content_type: 'Card', metadata: [{'rationale' => {'type' => 'following_user', 'user_id' => followable.id}}], last_enqueue_event_time: 2.weeks.ago)
    end
    UserContentsQueue.gateway.refresh_index!
    recent_queued_cards = UserContentsQueue.get_queue_for_user(user_id: user.id, filter_params: { from_time: 1.week.ago}, limit: 100)
    whole_queue = UserContentsQueue.get_queue_for_user(user_id: user.id, limit: 100)
    assert_equal 6, recent_queued_cards.count
    assert_same_elements cards.collect(&:id), recent_queued_cards.collect(&:content_id)
    assert_equal 12, whole_queue.count
  end

  test 'honors from exclude content ids filter' do
    user = create(:user)
    followable = create(:user)
    cards = create_list(:card, 9, author_id: followable.id)
    excluded_ids = cards.last(2).collect(&:id)

    cards.each do |card|
      UserContentsQueue.create(user_id: user.id, content_id: card.id, content_type: 'Card', metadata: [{'rationale' => {'type' => 'following_user', 'user_id' => followable.id}}], last_enqueue_event_time: Time.now.utc - 15.hours)
    end
    UserContentsQueue.gateway.refresh_index!
    results = UserContentsQueue.get_queue_for_user(user_id: user.id, filter_params: { exclude_content_ids: excluded_ids}, limit: 100)
    assert_equal 7, results.count
    assert_same_elements cards.first(7).collect(&:id), results.collect(&:content_id)
  end


  test 'should not enqueue same content for same user twice' do
    user = create :user, organization: @org
    card = create :card, author: create(:user, organization: @org), organization: @org
    UserContentsQueue.create(user_id: user.id, content_id: card.id, content_type: 'Card', metadata: [{'rationale' => {'type' => 'following_channel', 'channel_id' => [1, 2, 3]}}])
    UserContentsQueue.gateway.refresh_index!

    assert_equal 1, UserContentsQueue.all.length

    queues = UserContentsQueue.get_queue_for_user(
        user_id: user.id,
        content_type: 'Card',
        limit: 50
    )

    # assert that `card` is already present in `UserContentsQueue`.
    assert_equal card.id, queues.first.content_id
    assert_equal card.class.to_s, queues.first.content_type

    cards = create_list :card, 10, author: create(:user, organization: @org), organization: @org
    # add existing enqueued card
    cards << card

    # bulk insert 11 cards into `UserContentsQueue`
    assert_equal 11, cards.length
    UserContentsQueue.enqueue_bulk_for_user(
      user_id: user.id,
      content_type: 'Card',
      items: cards.map do |c|
        {
          content_id: c.id,
          metadata: {'content' => ["I'm still #{c.id}"]},
        }
      end,
      merge_metadata: true
    )

    queues = UserContentsQueue.get_queue_for_user(
      user_id: user.id,
      content_type: 'Card',
      limit: 50
    )

    # total should not be 12
    assert_equal 11, queues.length
  end

  class FeedV2Test < ActiveSupport::TestCase
    setup do
      WebMock.disable!
    end

    teardown do
      WebMock.enable!
    end

    test 'v2 feed should handle following channel' do
      user = create(:user)
      card = create(:card)
      channel = create(:channel)

      UserContentsQueue.create(user_id: user.id, content_id: card.id, content_type: 'Card', metadata: [{'rationale' => {'type' => 'following_channel', 'channel_id' => channel.id}}], last_enqueue_event_time: Time.now.utc - 15.hours)
      UserContentsQueue.gateway.refresh_index!
      feed = UserContentsQueue.feed_content_for_v2(user: user)
      assert_equal 1, feed.length
      assert_equal({'item' => card, 'rationale' => "Because you are following #{channel.label}"}, feed.first)
    end

    test 'v2 feed should handle following user' do
      user = create(:user)
      card = create(:card)
      another_user = create(:user)

      UserContentsQueue.create(user_id: user.id, content_id: card.id, content_type: 'Card', metadata: [{'rationale' => {'type' => 'following_user', 'user_id' => another_user.id}}], last_enqueue_event_time: Time.now.utc - 15.hours)
      UserContentsQueue.gateway.refresh_index!
      feed = UserContentsQueue.feed_content_for_v2(user: user)
      assert_equal 1, feed.length
      assert_equal({'item' => card, 'rationale' => "Because you are following #{another_user.name}"}, feed.first)
    end

    test 'v2 feed should handle user interest' do
      user = create(:user)
      card = create(:card)
      interest = create(:interest)

      UserContentsQueue.create(user_id: user.id, content_id: card.id, content_type: 'Card', metadata: [{'rationale' => {'type' => 'user_interest', 'interest_id' => interest.id}}], last_enqueue_event_time: Time.now.utc - 15.hours)
      UserContentsQueue.gateway.refresh_index!
      feed = UserContentsQueue.feed_content_for_v2(user: user)
      assert_equal 1, feed.length
      assert_equal({'item' => card, 'rationale' => "Because you are interested in #{interest.name}"}, feed.first)
    end

    test 'v2 feed should not return cards not in published state' do
      user = create(:user)
      followable = create(:user)
      cards = create_list(:card, 2, author_id: followable.id)

      cards.each do |card|
        UserContentsQueue.create(user_id: user.id, content_id: card.id, content_type: 'Card', metadata: [{'rationale' => {'type' => 'following_user', 'user_id' => followable.id}}], last_enqueue_event_time: Time.now.utc - 15.hours)
      end
      UserContentsQueue.gateway.refresh_index!
      feed = UserContentsQueue.feed_content_for_v2(user: user)
      assert_equal 2, feed.length
      assert_same_elements cards.map(&:id), feed.map{|f| f['item']['id']}

      cards.last.archive!
      feed = UserContentsQueue.feed_content_for_v2(user: user)
      assert_equal 1, feed.length
      assert_equal cards.first.id, feed.first['item']['id']
    end

    test 'v2 feed should filter cards by user language' do
      user = create(:user)
      create(:user_profile, language: 'ru', user: user)
      user2 = create(:user)
      ru_card = create(:card, author_id: user2.id, language: 'ru')
      en_card = create(:card, author_id: user2.id, language: 'en')
      without_lang_card = create(:card, author_id: user2.id)

      [ru_card, en_card, without_lang_card].each do |card|
        UserContentsQueue.create(user_id: user.id, content_id: card.id, content_type: 'Card', metadata: [{'rationale' => {'type' => 'user', 'user_id' => user2.id}}], last_enqueue_event_time: Time.now.utc - 15.hours)
      end
      UserContentsQueue.gateway.refresh_index!
      feed = UserContentsQueue.feed_content_for_v2(
        user: user, filter_params: { filter_by_language: true }
      )
      assert_equal 2, feed.length
      assert_same_elements [ru_card, without_lang_card].map(&:id), feed.map{|f| f['item']['id']}
    end
  end
end

require 'test_helper'
class TagSuggestTest < ActiveSupport::TestCase
  self.use_transactional_fixtures = false

  setup do
    skip
    WebMock.disable!
    TagSuggest.create_index! force: true
  end

  teardown do
    WebMock.enable!
  end

  test 'should create and update index on tagging' do
    skip 'TagAutosuggestIndexerJob is disabled'

    g = create(:group)
    u = create(:user)
    g.add_member u

    p = create(:post, message: 'this is a message with a #hashtag', group: g, user: u)
    TagSuggest.gateway.refresh_index!
    assert_equal 1, TagSuggest.count

    p1 = create(:post, message: 'this is a second #tagname', group: g, user: u)
    TagSuggest.gateway.refresh_index!
    assert_equal 2, TagSuggest.count
    tgs = TagSuggest.search(filter: {and: [{term: {name: 'tagname'}}, {term: {group_id: g.id}}]}).first
    assert_equal 1, tgs.suggest_name['weight']

    # a post with the same hash tag
    p2 = create(:post, message: 'this is the second usage of #tagname', group: g, user: u)
    TagSuggest.gateway.refresh_index!
    assert_equal 2, TagSuggest.count

    tgs = TagSuggest.search(filter: {and: [{term: {name: 'tagname'}}, {term: {group_id: g.id}}]}).first
    assert_equal 2, tgs.suggest_name['weight']

    c = create(:card, title: "this is #tagname")
    c.add_tags
    TagSuggest.gateway.refresh_index!
    assert_equal 3, TagSuggest.count

  end

  test 'interest suggestions should work' do
    skip 'TagAutosuggestIndexerJob is disabled'

    interest = create(:interest, name: 'homebound')
    cards = create_list(:card, 2)
    cards.each do |card|
      card.update_attributes(message: '#homework')
      card.update_tags
    end

    user = create(:user)
    assert_difference ->{Tagging.count} do
      user.interests << interest
    end

    another_interest = create(:interest, name: 'rails')
    user.interests << another_interest

    # When suggesting all tags, should be in the order: ['homework', 'homebound']
    # When suggesting interests only, should just be: ['homebound']
    TagSuggest.gateway.refresh_index!
    all_suggestions = TagSuggest.suggestions_for_destiny(term: 'ho', organization_id: Organization.default_org.id)
    assert_equal(['homework', 'homebound'], all_suggestions)

    interest_suggestions = TagSuggest.suggestions_for_destiny(term: 'ho', organization_id: Organization.default_org.id, tag_type: 'Interest')
    assert_equal(['homebound'], interest_suggestions)

    default_suggestions = TagSuggest.suggestions_for_destiny(term: '', organization_id: Organization.default_org.id, tag_type: 'Interest')
    assert_same_elements(['rails', 'homebound'], default_suggestions)
  end

  test 'suggestions should work' do
    skip 'TagAutosuggestIndexerJob is disabled'

    # test data:
    # In group g1, we are creating a two posts and a comment, with hashtags used in the following order:
    # homework1, homebound, politeEnquiry
    # In group g2, we have got #homebound twice
    # In cards and video streams, we got #politeEnquiry, #robotics in that order
    g1, u = create_gu
    p1 = create(:post, user: u, group: g1, message: 'this is about #homework1 #politeEnquiry')
    c1 = create(:comment, user: u, commentable: p1, message: 'whatever, Im going home. #homebound')

    p2 = create(:post, user: u, group: g1, message: 'this is about #homework1 again')

    g2 = create(:group)
    g2.add_member u

    p3 = create(:post, user: u, group: g2, message: '#homebound')
    p4 = create(:post, user: u, group: g2, message: '#homebound')

    TagSuggest.gateway.refresh_index!

    suggestions = TagSuggest.suggestions_for_forum(term: 'home', group_id: g1.id)
    assert_equal(['homework1', 'homebound'], suggestions)

    suggestions = TagSuggest.suggestions_for_forum(term: 'homework', group_id: g1.id)
    assert_equal(['homework1'], suggestions)

    # for group 2
    suggestions = TagSuggest.suggestions_for_forum(term: 'home', group_id: g2.id)
    assert_equal(['homebound'], suggestions)

    # Same For destiny. In org1, we have tags used in the following order:
    # politeEnquiry(2), politeEnglish(1), homebound(1), robotics(1)
    # In org2, we have got #politeEnglish twice
    org1 = create(:organization)
    author_org1 = create(:user, organization: org1)
    c1 = create(:card, author_id: author_org1.id, message: '#politeEnquiry #homebound')
    c1.add_tags

    c2 = create(:card, author_id: author_org1.id, message: '#politeEnquiry #politeEnglish')
    c2.add_tags

    v1 = create(:wowza_video_stream, name: 'robotics', creator: author_org1)
    v1.topics = "robotics"
    v1.save

    org2 = create(:organization)
    author_org2 = create(:user, organization: org2)
    c1_org2 = create(:card, author_id: author_org2.id, message: "#politeEnglish")
    c1_org2.add_tags


    c2_org2 = create(:card, author_id: author_org2.id, message: "#politeEnglish")
    c2_org2.add_tags

    TagSuggest.gateway.refresh_index!

    # destiny
    # Org 1
    suggestions = TagSuggest.suggestions_for_destiny(term: 'po', organization_id: org1.id)
    assert_equal(['politeEnquiry', 'politeEnglish'], suggestions)

    # Org 2
    suggestions = TagSuggest.suggestions_for_destiny(term: 'po', organization_id: org2.id)
    assert_equal(['politeEnglish'], suggestions)

    # separate search
    assert_equal(['robotics'], TagSuggest.suggestions_for_destiny(term: 'robotics', organization_id: org1.id))

    # no spell check
    assert_equal([], TagSuggest.suggestions_for_destiny(term: 'rbot', organization_id: org1.id))

    # nothing for 'rob' in org2
    assert_equal([], TagSuggest.suggestions_for_destiny(term: 'rob', organization_id: org2.id))
  end

end

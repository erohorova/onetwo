require 'test_helper'

class PronouncementTest < ActiveSupport::TestCase
  # should belong_to :scope

  # should have_db_column(:label).of_type(:text)
  # should have_db_column(:scope_id).of_type(:integer)
  # should have_db_column(:scope_type).of_type(:string)
  # should have_db_column(:start_date).of_type(:datetime)
  # should have_db_column(:end_date).of_type(:datetime)
  # should have_db_column(:is_active).of_type(:boolean)

  # should validate_presence_of(:label)
  # should validate_presence_of(:scope_id)
  # should validate_presence_of(:scope_type)
  # should validate_presence_of(:start_date)

  setup do
    @org = create(:organization)
    @actor = create(:user, organization: @org)
    @label = "announcement_label"
    @start_date = Time.now - 3.days
    @end_date = Time.now - 2.days
    @time = Time.now
    Time.stubs(:now).returns @time
    @metadata = { user_id: @actor.id }
    RequestStore.stubs(:read).with(:request_metadata).returns @metadata
  end

  test 'records event on create' do
    pronouncement = Pronouncement.create!(
      scope: @org,
      label: @label,
      start_date: @start_date,
      end_date: @end_date
    )
    Analytics::MetricsRecorderJob.unstub(:perform_later)
    EdcastActiveJob.unstub(:perform_later)
    recorder_args = {
      event: "org_announcement_created",
      recorder: "Analytics::OrgMetricsRecorder",
      timestamp: @time.to_i,
      actor: Analytics::MetricsRecorder.user_attributes(@actor),
      org: Analytics::MetricsRecorder.org_attributes(@org),
      announcement: Analytics::MetricsRecorder.pronouncement_attributes(pronouncement),
      additional_data: @metadata
    }
    Analytics::MetricsRecorderJob.expects(:perform_later).with(recorder_args)
    pronouncement.run_callbacks(:commit)
  end

  test 'records event on destroy' do
    pronouncement = Pronouncement.create!(
      scope: @org,
      label: @label,
      start_date: @start_date,
      end_date: @end_date
    )
    Analytics::MetricsRecorderJob.unstub(:perform_later)
    EdcastActiveJob.unstub(:perform_later)
    recorder_args = {
      event: "org_announcement_deleted",
      recorder: "Analytics::OrgMetricsRecorder",
      timestamp: @time.to_i,
      actor: Analytics::MetricsRecorder.user_attributes(@actor),
      org: Analytics::MetricsRecorder.org_attributes(@org),
      announcement: Analytics::MetricsRecorder.pronouncement_attributes(pronouncement),
      additional_data: @metadata
    }
    Analytics::MetricsRecorderJob.expects(:perform_later).with(recorder_args)
    pronouncement.destroy
  end

  test 'records event on edit to label and is_active' do
    pronouncement = Pronouncement.create!(
      scope: @org,
      label: @label,
      start_date: @start_date,
      end_date: @end_date,
    )
    Analytics::MetricsRecorderJob.unstub(:perform_later)
    EdcastActiveJob.unstub(:perform_later)
    new_label = 'new label'
    {
      'label' => 'new label',
      'is_active' => false
    }.each do |col_name, new_val|
      recorder_args = {
        event: "org_announcement_edited",
        recorder: "Analytics::OrgEditedMetricsRecorder",
        timestamp: @time.to_i,
        actor: Analytics::MetricsRecorder.user_attributes(@actor),
        org: Analytics::MetricsRecorder.org_attributes(@org),
        announcement: Analytics::MetricsRecorder.pronouncement_attributes(pronouncement).merge(
          col_name => new_val
        ),
        additional_data: @metadata,
        changed_column: col_name,
        old_val: pronouncement.send(col_name.to_sym)&.to_s,
        new_val: new_val&.to_s
      }
      Analytics::MetricsRecorderJob.expects(:perform_later).with(recorder_args)
      pronouncement.update(col_name => new_val)
    end
  end

  test 'records event on edit to start_date and end_date' do
    pronouncement = Pronouncement.create!(
      scope: @org,
      label: @label,
      start_date: @start_date,
      end_date: @end_date
    )
    Analytics::MetricsRecorderJob.unstub(:perform_later)
    EdcastActiveJob.unstub(:perform_later)
    {
      'start_date' => Time.now,
      'end_date' => Time.now + 5.minutes
    }.each do |col_name, new_val|
      recorder_args = {
        event: "org_announcement_edited",
        recorder: "Analytics::OrgEditedMetricsRecorder",
        timestamp: @time.to_i,
        actor: Analytics::MetricsRecorder.user_attributes(@actor),
        org: Analytics::MetricsRecorder.org_attributes(@org),
        announcement: Analytics::MetricsRecorder.pronouncement_attributes(pronouncement).merge(
          col_name => new_val.to_i
        ),
        additional_data: @metadata,
        changed_column: col_name,
        old_val: pronouncement.send(col_name.to_sym).to_i.to_s,
        new_val: new_val.to_i.to_s
      }
      Analytics::MetricsRecorderJob.expects(:perform_later).with(recorder_args)
      pronouncement.update(col_name => new_val)
    end
  end


end

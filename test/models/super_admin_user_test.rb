require 'test_helper'

class SuperAdminUserTest < ActiveSupport::TestCase
  should belong_to(:user)

  test 'add/remove by email' do
    create_default_org
    default_org_user = create(:user, organization: Organization.default_org)
    org = create(:organization)
    org_user = create(:user, organization: org, organization_role: 'member')

    assert_difference -> {SuperAdminUser.count} do
      assert_equal default_org_user, SuperAdminUser.add_by_email(default_org_user.email)
      assert_equal false, SuperAdminUser.add_by_email(org_user.email)
    end

    assert_difference -> {SuperAdminUser.count}, -1 do
      assert_equal true, SuperAdminUser.remove_by_email(default_org_user.email)
      assert_equal false, SuperAdminUser.remove_by_email(org_user.email)
    end
  end
end

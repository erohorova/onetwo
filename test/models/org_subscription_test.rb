require 'test_helper'

class OrgSubscriptionTest < ActiveSupport::TestCase
  should have_db_column(:name).of_type(:string)
  should have_db_column(:description).of_type(:text)
  should have_db_column(:start_date).of_type(:date)
  should have_db_column(:end_date).of_type(:date)
  should have_db_column(:organization_id).of_type(:integer)
  should have_db_column(:sku).of_type(:string)

  should belong_to(:organization)
  should have_many(:prices).class_name('OrganizationPrice')
  should have_many(:orders)

  should validate_presence_of(:name)
  should validate_presence_of(:description)
  should validate_presence_of(:start_date)
  should validate_presence_of(:end_date)
  should validate_presence_of(:organization_id)
  should validate_presence_of(:sku)

  test '#redirect_page_url must return url for home page' do
    org = create(:organization, host_name: 'test')
    org_subscription = create(:org_subscription)

    assert_equal 'http://test.test.host', org_subscription.redirect_page_url(org: org)
  end
end

require 'test_helper'

class BackgroundJobTest < ActiveSupport::TestCase

  test 're enqueue function' do
    class DummyJob < EdcastActiveJob
      queue_as :dummy_queue
      def perform opt1, opt2
      end
    end

    EdcastActiveJob::WHITELISTED_JOB_CLASS << "BackgroundJobTest::DummyJob"
    DummyJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    opt1 = {"a" => 1}
    opt2 = 2

    metadata = { foo: "bar" }
    RequestStore.unstub(:read)
    RequestStore.stubs(:read).with(:request_metadata).returns metadata

    assert_difference ->{BackgroundJob.count} do
      DummyJob.perform_later(opt1, opt2)
    end

    bk_job = BackgroundJob.last

    args = [opt1, opt2, { request_store_data: metadata }]
    DummyJob.expects(:perform_later).with(*args).once
    bk_job.re_enqueue
  end

  test 're enqueue with no arguments' do
    class DummyJob < EdcastActiveJob
      queue_as :dummy_queue
      def perform
      end
    end

    EdcastActiveJob::WHITELISTED_JOB_CLASS << "BackgroundJobTest::DummyJob"
    DummyJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    assert_difference ->{BackgroundJob.count} do
      DummyJob.perform_later
    end

    bk_job = BackgroundJob.last

    DummyJob.expects(:perform_later).once
    bk_job.re_enqueue
  end

  test 're enqueue with activerecord object arguments' do
    class DummyJob < EdcastActiveJob
      queue_as :dummy_queue
      def perform user
      end
    end

    EdcastActiveJob::WHITELISTED_JOB_CLASS << "BackgroundJobTest::DummyJob"
    DummyJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    u = create(:user)

    metadata = { foo: "bar" }
    RequestStore.unstub(:read)
    RequestStore.stubs(:read).with(:request_metadata).returns metadata

    assert_difference ->{BackgroundJob.count} do
      DummyJob.perform_later u
    end

    bk_job = BackgroundJob.last

    args = [u, { request_store_data: metadata }]
    DummyJob.expects(:perform_later).with(*args).once
    bk_job.re_enqueue
  end
end

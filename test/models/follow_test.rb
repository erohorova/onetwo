require 'test_helper'

class FollowTest < ActiveSupport::TestCase

  setup do
    Post.any_instance.stubs :p_read_by_creator
  end

  test 'should follow once' do
    @org = create(:organization)
    @user = create(:user, organization: @org)
    @user2 = create(:user, organization: @org)
    @group = create(:resource_group, organization: @org)
    @group.add_member @user
    @group.add_member @user2
    @post = create(:post, user: @user2, group: @group)

    assert_difference ->{ Follow.count } do
      @post.follow @user
    end
    assert_includes @post.followers, @user

    assert_not Follow.new(user: @user, followable: @post).valid?
    assert Follow.new(user: @user, followable: create(:post, user: @user2, group: @group)).valid?
  end

  test 'should destroy follow on post destroy' do
    @org = create(:organization)
    @user = create(:user, organization: @org)
    @user2 = create(:user, organization: @org)
    @group = create(:resource_group, organization: @org)
    @group.add_member @user
    @group.add_member @user2
    @post = create(:post, user: @user2, group: @group)

    @post.follow @user
    assert_difference ->{ Follow.count }, -@post.follows.count do
      @post.destroy
    end
  end

  test 'should destroy follow on user destroy' do
    @org = create(:organization)
    @user = create(:user, organization: @org)
    @user2 = create(:user, organization: @org)
    @group = create(:resource_group, organization: @org)
    @group.add_member @user
    @group.add_member @user2
    @post = create(:post, user: @user2, group: @group)

    @post.follow @user
    assert_difference ->{ Follow.count }, -1 do
      @user.destroy
    end
  end

  test '.remove_followers remove users as followers from post' do
    u1   = create :user
    u2   = create :user
    g    = create :group

    [u1, u2].each { |u| GroupsUser.create user_id: u.id, group_id: g.id, as_type: 'member' }
    post = Question.create(title: 'title', message: 'description', postable: g, user_id: u1.id)
    [u1, u2].each { |u| Follow.create(user: u, followable: post) }
    post.reload

    assert_includes post.followers, u1
    assert_includes post.followers, u2

    Follow.remove_followers(g.reload.posts, u1)
    post.reload

    assert_includes post.followers, u2
    refute_includes post.followers, u1
  end

  test 'invokes after_followed' do
    skip
    # should work for any model; using tag here
    tag = create(:tag)
    tag.instance_eval do
      def after_followed(follower)
        @follower = "follower: #{follower.id}"
      end
      def get_follower
        @follower
      end
    end

    user = create(:user)
    assert_nil tag.get_follower
    create(:follow, user: user, followable: tag).run_callbacks(:commit)
    assert_equal "follower: #{user.id}", tag.get_follower
  end

  test 'cannot follow channel if not in org' do
    org = create(:organization)
    u = create(:user, organization: org)
    ch = create(:channel, organization: org, is_private: false)
    assert build(:follow, followable: ch, user: u).save

    same_org_ch = create(:channel)
    assert_not build(:follow, followable: same_org_ch, user: u).save
  end

  test 'cannot follow user if not in org' do
    organization = create(:organization)
    u = create(:user, organization: organization)
    f = create(:user, organization: organization)

    assert build(:follow, followable: f, user: u).save
    another_org = create(:organization)
    user_in_another_org = create(:user, organization: another_org)
    assert_not build(:follow, followable: user_in_another_org, user: u).save
  end

  test 'check if the user is eligible for content enqueue for a given card' do
    @org = create :organization
    channel = create :channel, organization: @org
    private_card_author = create :user, organization: @org
    private_card = create :card, organization: @org, is_public: false, author: private_card_author
    # private card is posted in a channel
    private_card.channels << channel
    # user following the channel
    channel_follower = create :user, organization: @org
    create :follow, user: channel_follower, followable: channel

    assert_equal 1, Follow.all.count

    follow = Follow.first
    assert_equal channel_follower.id, follow.user.id
    assert_equal channel.id, follow.followable_id
    assert_equal 'Channel', follow.followable_type

    assert_not_equal false, follow.enqueue_for_follower(card: private_card)

    # author of the card follows the channel
    create :follow, user: private_card_author, followable: channel

    follow = Follow.find_by(user: private_card_author)
    assert_nil follow.enqueue_for_follower(card: private_card)
  end

  class UserFollowMetricRecorderTest < ActiveSupport::TestCase
    test 'should record user follow event' do
      Follow.any_instance.unstub(:record_follow_event)
      IndexJob.stubs(:perform_later)
      org = create(:organization)
      user = create(:user, organization: org)

      RequestStore.stubs(:read).returns(user_id: user.id)
      followee, follower = create_list(:user, 2, organization: org)
      TestAfterCommit.with_commits do
        Analytics::MetricsRecorderJob.expects(:perform_later).once
        create(:follow, user: follower, followable: followee)
      end
    end

    test 'should record user unfollow event' do
      Analytics::MetricsRecorderJob.unstub(:perform_later)
      EdcastActiveJob.unstub(:perform_later)
      RequestStore.unstub(:read)
      ChannelsUser.any_instance.stubs(:push_collaborator_added_event)
      time = Time.now
      timestamp = time.to_i
      Time.stubs(:now).returns time
      IndexJob.stubs(:perform_later)
      org = create(:organization)
      followee, follower = create_list(:user, 2, organization: org)
      user = create :user, organization: org
      Follow.any_instance.stubs(:find_actor).returns user
      metadata = { user_id: user.id}
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      expected_args = {
        recorder: 'Analytics::UserFollowRecorder',
        timestamp: timestamp,
        org: Analytics::MetricsRecorder.org_attributes(org),
        actor: Analytics::MetricsRecorder.user_attributes(user),
        follower: Analytics::MetricsRecorder.user_attributes(follower),
        followed: Analytics::MetricsRecorder.user_attributes(followee),
        event: 'user_unfollowed',
        additional_data: metadata
      }
      TestAfterCommit.with_commits do
        Follow.any_instance.stubs(:record_user_follow_action)
        User.any_instance.stubs(:after_unfollowed).returns true
        follow = create(:follow, user: follower, followable: followee)
        Follow.any_instance.unstub(:record_user_follow_action)
        Analytics::MetricsRecorderJob.expects(:perform_later)
        .with(expected_args).once

        follow.destroy
      end
    end

  end

  class ChannelFollowMetricRecorderTest < ActiveSupport::TestCase
    test 'should record channel follow event' do
      User.any_instance.stubs(:record_user_create_event).returns true
      Channel.any_instance.stubs(:record_channel_event).returns true

      org = create(:organization)
      metadata = {
        platform: 'web',
        user_agent: 'Edcast iPhone',
        user_id: create(:user, organization: org).id
      }
      RequestStore.stubs(:read).returns(metadata)
      follower = create(:user, organization: org)
      channel = create(:channel, organization: org)

      Analytics::MetricsRecorderJob.unstub :perform_later
      Analytics::MetricsRecorderJob.expects(:perform_later)
      .with(has_entry(event: 'channel_followed')).once

      follow = create(:follow, user: follower, followable: channel)
      follow.run_callbacks :commit
    end

    test 'should record channel unfollow event' do
      time = Time.now
      Time.stubs(:now).returns time
      timestamp = time.to_i
      IndexJob.stubs(:perform_later)
      org = create(:organization)
      user = create(:user, organization: org)
      RequestStore.unstub(:read)
      metadata = {
        platform: 'web',
        user_agent: 'Edcast iPhone',
        user_id: user.id
      }
      RequestStore.store[:request_metadata] = metadata
      follower = create(:user, organization: org)
      channel = create(:channel, organization: org, user: user)
      follow = create(:follow, user: follower, followable: channel)

      follow.run_callbacks(:commit) do
        Follow.any_instance.stubs(:record_follow_event)
        Channel.any_instance.stubs(:record_channel_event).returns true
        Channel.any_instance.stubs(:after_unfollowed).returns true
        Analytics::MetricsRecorderJob.expects(:perform_later)
        .with(
          recorder: 'Analytics::ChannelFollowRecorder',
          timestamp: timestamp,
          actor: Analytics::MetricsRecorder.user_attributes(user),
          follower: Analytics::MetricsRecorder.user_attributes(follower),
          followed: Analytics::MetricsRecorder.channel_attributes(channel),
          event: 'channel_unfollowed',
          additional_data: metadata
        ).once
        follow.destroy
      end
    end

  end

  class SlowTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false

    setup do
      skip

      WebMock.disable!

      @group = create(:resource_group)
      @user = create(:user)
      @group.add_member @user
      @post  = create(:post, user: @user, group: @group, message: 'Post 1')
    end

    teardown do
      WebMock.enable!
    end

    test 'reindex post if follower is added' do
      create_default_org
      @post.reload
      Post.reindex
      results = Post.retrieve(filter_params: { follower_ids: @user.id })
      hit_ids = results.hits.collect{ |h| h['_id'].to_i }

      assert_equal 1, results.count
      assert_includes hit_ids, @post.id
    end

    test 'reindex post if follower is destroyed' do
      create_default_org
      Follow.where(followable: @post, user: @user).destroy_all

      @post.reload
      Post.reindex

      assert_empty Post.retrieve(filter_params: { follower_ids: @user.id })
    end
  end
end

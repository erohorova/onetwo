require 'test_helper'

class ProjectSubmissionReviewTest < ActiveSupport::TestCase
  should have_db_column(:description).of_type(:text)
  should have_db_column(:status).of_type(:integer)
  should have_db_column(:reviewer_id).of_type(:integer)
  should have_db_column(:project_submission_id).of_type(:integer)

  should belong_to(:project_submission)
  should belong_to(:reviewer)

  test 'should list approved_review project submission reviews only' do
    org = create(:organization)
    reviewer = create(:user, organization: org)
    card = create(:card, organization: org,
                  author: reviewer,
                  card_type: 'project',
                  card_subtype: 'text')
    users = create_list(:user, 5, organization: org)
    users.each_with_index do |user|
      create(:project_submission, project_id: card.project.id, submitter_id: user.id)
    end
    ProjectSubmissionReview.create(description: "something", project_submission_id: ProjectSubmission.first.id, reviewer_id: reviewer.id, status: 1)

    project_submission_reviews = ProjectSubmissionReview.approved_review(ProjectSubmission.ids)

    assert_equal 1, project_submission_reviews.count
    assert_equal ProjectSubmissionReview.first.id, project_submission_reviews.first.id
  end
end

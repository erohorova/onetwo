require 'test_helper'

class UserContentLevelMetricTest < ActiveSupport::TestCase

  setup do
  end

  teardown do
  end

  test 'increment smartbite score' do
    user1, user2 = create_list(:user, 2)
    card = create(:card)
    video_stream = create(:wowza_video_stream)

    [user1, user2].each do |user|
      [['card', card.id], ['video_stream', video_stream.id]].each do |content|
        [[:all_time, 0], [:day, 0], [:day, 1], [:week, 0], [:month, 0], [:year, 0]].each do |period_with_offset|
          create(:user_content_level_metric, user: user, content_type: content[0], content_id: content[1], period: period_with_offset[0], offset: period_with_offset[1])
        end
      end
    end

    # Update existing metric
    assert_no_difference ->{UserContentLevelMetric.count} do
      UserContentLevelMetric.increment_smartbites_score({user_id: user1.id, content_id: card, content_type: "card"}, 1, PeriodOffsetCalculator::BEGINNING_OF_TIME + 1.day)
    end

    # The metric for days 1, week 0 and all time should have changed
    assert_equal 1, UserContentLevelMetric.get_smartbites_score({user_id: user1.id, content_id: card.id, content_type: 'card'}, :day, 1)
    assert_equal 1, UserContentLevelMetric.get_smartbites_score({user_id: user1.id, content_id: card.id, content_type: 'card'}, :week, 0)
    assert_equal 1, UserContentLevelMetric.get_smartbites_score({user_id: user1.id, content_id: card.id, content_type: 'card'}, :all_time, 0)
    assert_equal 1, UserContentLevelMetric.get_smartbites_score({user_id: user1.id, content_id: card.id, content_type: 'card'}, :month, 0)
    assert_equal 1, UserContentLevelMetric.get_smartbites_score({user_id: user1.id, content_id: card.id, content_type: 'card'}, :year, 0)

    # Make sure everything else is still 0
    assert_equal 5, UserContentLevelMetric.all.map(&:smartbites_score).reduce(:+)


    # Create new entries, now we are in week 2
    assert_difference ->{UserContentLevelMetric.count}, 2 do
      UserContentLevelMetric.increment_smartbites_score({user_id: user1.id, content_id: card.id, content_type: "card"}, 1, PeriodOffsetCalculator::BEGINNING_OF_TIME + 9.days)
    end

    assert_equal 2, UserContentLevelMetric.get_smartbites_score({user_id: user1.id, content_id: card.id, content_type: "card"}, :all_time, 0)
    assert_equal 2, UserContentLevelMetric.get_smartbites_score({user_id: user1.id, content_id: card.id, content_type: "card"}, :month, 0)
    assert_equal 2, UserContentLevelMetric.get_smartbites_score({user_id: user1.id, content_id: card.id, content_type: "card"}, :year, 0)
    assert_equal 1, UserContentLevelMetric.get_smartbites_score({user_id: user1.id, content_id: card.id, content_type: "card"}, :day, 9)
    assert_equal 1, UserContentLevelMetric.get_smartbites_score({user_id: user1.id, content_id: card.id, content_type: "card"}, :week, 1)

    # Make sure nothing else has changed
    assert_equal 10, UserContentLevelMetric.all.map(&:smartbites_score).reduce(:+)
  end

end

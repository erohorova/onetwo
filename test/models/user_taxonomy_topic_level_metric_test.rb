require 'test_helper'

class UserTaxonomyTopicLevelMetricTest < ActiveSupport::TestCase
  test 'update from increments' do
    skip
    user = create(:user)
    assert_difference ->{UserTaxonomyTopicLevelMetric.count}, 5 do
      UserTaxonomyTopicLevelMetric.update_from_increments(opts: {user_id: user.id, taxonomy_label: "edcast.technology.data_science_and_analytics.big_data"}, increments: [[:smartbites_score, 10], [:smartbites_created, 1], [:smartbites_consumed, 1]], timestamp: Time.at(Time.now.to_i))
    end
  end
end

require 'test_helper'

class UserLevelMetricTest < ActiveSupport::TestCase

  setup do
  end

  teardown do
  end

  def _test_metrics_updates(metric, metric_getter, user, period_with_offsets, value)
    period_with_offsets.each do |po|
      assert_equal value, UserLevelMetric.send(metric_getter, {user_id: user.id}, po[0], po[1]), message: "Failed for period: #{po[0]}, offset: #{po[1]}"
    end

    # Make sure everything else is 0
    assert_equal value*period_with_offsets.length, UserLevelMetric.all.map{|u| u.send(metric)}.reduce(:+)
  end

  test 'uniqueness constraint' do
    user = create(:user)
    create(:user_level_metric, user_id: user.id, period: :all_time, offset: 0)
    refute build(:user_level_metric, user_id: user.id, period: :all_time, offset: 0).valid?
  end

  test 'update from increments' do
    user1, user2 = create_list(:user, 2)

    [user1, user2].each do |user|
      [[:all_time, 0], [:day, 0], [:day, 1], [:week, 0], [:month, 0], [:year, 0]].each do |period_with_offset|
        create(:user_level_metric, user: user, period: period_with_offset[0], offset: period_with_offset[1])
      end
    end

    # Update existing metric
    assert_no_difference ->{UserLevelMetric.count} do
      UserLevelMetric.update_from_increments(increments: [[:smartbites_score, 4], [:smartbites_consumed, 1], [:smartbites_created, 1], [:smartbites_comments_count, 1], [:smartbites_liked, 1], [:time_spent, 5]], timestamp: PeriodOffsetCalculator::BEGINNING_OF_TIME + 1.day, opts: {user_id: user1.id})
    end

    # The metric for days 1, week 0 and all time should have changed
    applicable_period_offsets = [[:day, 1], [:week, 0], [:month, 0], [:year, 0], [:all_time, 0]]
    _test_metrics_updates(:smartbites_score, :get_smartbites_score, user1, applicable_period_offsets, 4)
    _test_metrics_updates(:smartbites_created, :get_smartbites_created, user1, applicable_period_offsets, 1)
    _test_metrics_updates(:smartbites_consumed, :get_smartbites_consumed, user1, applicable_period_offsets, 1)
    _test_metrics_updates(:time_spent, :get_time_spent, user1, applicable_period_offsets, 5)
    _test_metrics_updates(:smartbites_comments_count, :get_smartbites_comments_count, user1, applicable_period_offsets, 1)
    _test_metrics_updates(:smartbites_liked, :get_smartbites_liked, user1, applicable_period_offsets, 1)

    # Create new entries, now we are in week 2
    assert_difference ->{UserLevelMetric.count}, 2 do
      UserLevelMetric.update_from_increments(opts: {user_id: user1.id}, increments: [[:smartbites_score, 4], [:smartbites_consumed, 1], [:smartbites_created, 1], [:time_spent, 5]], timestamp: PeriodOffsetCalculator::BEGINNING_OF_TIME + 9.days)
    end
  end

  test 'increment smartbite score' do
    user1, user2 = create_list(:user, 2)

    [user1, user2].each do |user|
      [[:all_time, 0], [:day, 0], [:day, 1], [:week, 0], [:month, 0], [:year, 0]].each do |period_with_offset|
        create(:user_level_metric, user: user, period: period_with_offset[0], offset: period_with_offset[1])
      end
    end

    # Update existing metric
    assert_no_difference ->{UserLevelMetric.count} do
      UserLevelMetric.increment_smartbites_score({user_id: user1.id}, 1, PeriodOffsetCalculator::BEGINNING_OF_TIME + 1.day)
    end

    # The metric for days 1, week 0 and all time should have changed
    assert_equal 1, UserLevelMetric.get_smartbites_score({user_id: user1.id}, :day, 1)
    assert_equal 1, UserLevelMetric.get_smartbites_score({user_id: user1.id}, :week, 0)
    assert_equal 1, UserLevelMetric.get_smartbites_score({user_id: user1.id}, :all_time, 0)
    assert_equal 1, UserLevelMetric.get_smartbites_score({user_id: user1.id}, :month, 0)
    assert_equal 1, UserLevelMetric.get_smartbites_score({user_id: user1.id}, :year, 0)

    # Make sure everything else is still 0
    assert_equal 5, UserLevelMetric.all.map(&:smartbites_score).reduce(:+)


    # Create new entries, now we are in week 2
    assert_difference ->{UserLevelMetric.count}, 2 do
      UserLevelMetric.increment_smartbites_score({user_id: user1.id}, 1, PeriodOffsetCalculator::BEGINNING_OF_TIME + 9.days)
    end

    assert_equal 2, UserLevelMetric.get_smartbites_score({user_id: user1.id}, :all_time, 0)
    assert_equal 2, UserLevelMetric.get_smartbites_score({user_id: user1.id}, :month, 0)
    assert_equal 2, UserLevelMetric.get_smartbites_score({user_id: user1.id}, :year, 0)
    assert_equal 1, UserLevelMetric.get_smartbites_score({user_id: user1.id}, :day, 9)
    assert_equal 1, UserLevelMetric.get_smartbites_score({user_id: user1.id}, :week, 1)

    # Make sure nothing else has changed
    assert_equal 10, UserLevelMetric.all.map(&:smartbites_score).reduce(:+)
  end

  test 'increment smartbites created metric' do
    user1, user2 = create_list(:user, 2)

    [user1, user2].each do |user|
      [[:all_time, 0], [:day, 0], [:day, 1], [:week, 0], [:month, 0], [:year, 0]].each do |period_with_offset|
        create(:user_level_metric, user: user, period: period_with_offset[0], offset: period_with_offset[1])
      end
    end

    assert_no_difference ->{UserLevelMetric.count} do
      UserLevelMetric.increment_smartbites_created({user_id: user1.id}, 1, PeriodOffsetCalculator::BEGINNING_OF_TIME + 1.day)
    end

    # The metric for days 1, week 0 and all time should have changed
    assert_equal 1, UserLevelMetric.get_smartbites_created({user_id: user1.id}, :day, 1)
    assert_equal 1, UserLevelMetric.get_smartbites_created({user_id: user1.id}, :week, 0)
    assert_equal 1, UserLevelMetric.get_smartbites_created({user_id: user1.id}, :all_time, 0)
    assert_equal 1, UserLevelMetric.get_smartbites_created({user_id: user1.id}, :month, 0)
    assert_equal 1, UserLevelMetric.get_smartbites_created({user_id: user1.id}, :year, 0)

    # Make sure everything else is still 0
    assert_equal 5, UserLevelMetric.all.map(&:smartbites_created).reduce(:+)
  end

  test 'increment smartbites consumed metric' do
    user1, user2 = create_list(:user, 2)

    assert_difference ->{UserLevelMetric.count}, 5 do
      UserLevelMetric.increment_smartbites_consumed({user_id: user1.id}, 1, PeriodOffsetCalculator::BEGINNING_OF_TIME + 1.day)
    end

    # The metric for days 1, week 0 and all time should have changed
    assert_equal 1, UserLevelMetric.get_smartbites_consumed({user_id: user1.id}, :day, 1)
    assert_equal 1, UserLevelMetric.get_smartbites_consumed({user_id: user1.id}, :all_time, 0)
    assert_equal 1, UserLevelMetric.get_smartbites_consumed({user_id: user1.id}, :month, 0)
    assert_equal 1, UserLevelMetric.get_smartbites_consumed({user_id: user1.id}, :year, 0)

    # Make sure everything else is still 0
    assert_equal 5, UserLevelMetric.all.map(&:smartbites_consumed).reduce(:+)
  end

  test 'increment smartbites completed metric' do
    user1, user2 = create_list(:user, 2)

    assert_difference ->{UserLevelMetric.count}, 5 do
      UserLevelMetric.increment_smartbites_completed({user_id: user1.id}, 1, PeriodOffsetCalculator::BEGINNING_OF_TIME + 1.day)
    end

    # The metric for days 1, week 0 and all time should have changed
    assert_equal 1, UserLevelMetric.get_smartbites_completed({user_id: user1.id}, :day, 1)
    assert_equal 1, UserLevelMetric.get_smartbites_completed({user_id: user1.id}, :all_time, 0)
    assert_equal 1, UserLevelMetric.get_smartbites_completed({user_id: user1.id}, :month, 0)
    assert_equal 1, UserLevelMetric.get_smartbites_completed({user_id: user1.id}, :year, 0)

    # Make sure everything else is still 0
    assert_equal 5, UserLevelMetric.all.map(&:smartbites_completed).reduce(:+)
  end

  test 'increment time spent' do
    user1, user2 = create_list(:user, 2)

    assert_difference ->{UserLevelMetric.count}, 5 do
      UserLevelMetric.increment_time_spent({user_id: user1.id}, 10, PeriodOffsetCalculator::BEGINNING_OF_TIME + 1.day)
    end

    # The metric for days 1, week 0 and all time should have changed
    assert_equal 10, UserLevelMetric.get_time_spent({user_id: user1.id}, :day, 1)
    assert_equal 10, UserLevelMetric.get_time_spent({user_id: user1.id}, :week, 0)
    assert_equal 10, UserLevelMetric.get_time_spent({user_id: user1.id}, :all_time, 0)
    assert_equal 10, UserLevelMetric.get_time_spent({user_id: user1.id}, :month, 0)
    assert_equal 10, UserLevelMetric.get_time_spent({user_id: user1.id}, :year, 0)

    # Make sure everything else is still 0
    assert_equal 50, UserLevelMetric.all.map(&:time_spent).reduce(:+)
  end

  test 'organization metrics over span' do
    org1, org2 = create_list(:organization, 2)

    # three users in org1, week 1 -> with smartbites consumed 1,2,3 respectively, smartbites_created 10,20,30 respectively. time spent - 5,10,15
    org1_users = create_list(:user, 3, organization: org1)
    org1_users.each_with_index do |org1_user, indx|
      create(:user_level_metric, user_id: org1_user.id, period: 'week', offset: 0, smartbites_consumed: indx+1, smartbites_created: (indx+1)*10, time_spent: (indx+1)*5, smartbites_score: indx+1)
    end

    # suspended user, should not change assertions
    suspended_org1_user = create(:user, organization: org1, is_suspended: true)
    create(:user_level_metric, user_id: suspended_org1_user.id, period: 'week', offset: 0, smartbites_consumed: 1, smartbites_created: 20, time_spent: 5, smartbites_score: 11)

    # inactive user(with time_spent=0 for period), should not change assertions
    inactive_org1_user = create(:user, organization: org1)
    create(:user_level_metric, user_id: inactive_org1_user.id, period: 'week', offset: 0, smartbites_consumed: 0, smartbites_created: 10, time_spent: 0, smartbites_score: 11)

    # three users in org1, week 2 -> with smartbites consumed 4,5,6 respectively, smartbites_created 40,50,60 respectively. time spent - 20,25,30
    org1_users.each_with_index do |org1_user, indx|
      create(:user_level_metric, user_id: org1_user.id, period: 'week', offset: 1, smartbites_consumed: indx+4, smartbites_created: (indx+4)*10, time_spent: (indx+4)*5, smartbites_score: indx+1)
    end

    # Some data for distraction from week 3
    org1_users.each_with_index do |org1_user, indx|
      create(:user_level_metric, user_id: org1_user.id, period: 'week', offset: 2, smartbites_consumed: indx+4, smartbites_created: (indx+4)*10, time_spent: (indx+4)*5, smartbites_score: indx+1)
    end

    data = UserLevelMetric.get_organization_metrics_over_span(organization_id: org1.id, team_id: nil, period: 'week', begin_offset: 0, end_offset: 1)
    assert_equal({smartbites_consumed: 21, smartbites_created: 210, active_users: 3, average_session: 17}, data)
  end

  test 'organization metrics' do
    org1, org2 = create_list(:organization, 2)

    # three users in org1, with smartbites consumed 1,2,3 respectively, smartbites_created 4,5,6 respectively. time spent - 5,10,15
    org1_users = create_list(:user, 3, organization: org1)
    org1_users.each_with_index do |org1_user, indx|
      create(:user_level_metric, user_id: org1_user.id, period: 'week', offset: 0, smartbites_consumed: indx+1, smartbites_created: indx+4, time_spent: (indx+1)*5, smartbites_score: indx+1)
    end

    # two users in org2, with smartbites consumed 1,2 respectively, smartbites_created 3, 4 respectively. time spent - 5,10
    org2_users = create_list(:user, 2, organization: org2)
    org2_users.each_with_index do |org2_user, indx|
      create(:user_level_metric, user_id: org2_user.id, period: 'week', offset: 0, smartbites_consumed: indx+1, smartbites_created: indx+4, time_spent: (indx+1)*5, smartbites_score: indx+1)
    end

    # Some dummy data for distraction
    # One data point in some different time period
    common_pars = {smartbites_consumed: 1, smartbites_created: 4, time_spent: 5, smartbites_score: 1}
    org1_users.each do |org1_user|

      common_pars.merge!({user_id: org1_user.id})
      create(:user_level_metric, common_pars.merge({period: 'all_time', offset: 0}))
      create(:user_level_metric, common_pars.merge({period: 'day', offset: 0}))
      create(:user_level_metric, common_pars.merge({period: 'week', offset: 1})) # different offset
      create(:user_level_metric, common_pars.merge({period: 'month', offset: 0}))
      create(:user_level_metric, common_pars.merge({period: 'year', offset: 0}))
    end

    # suspended user, should not change assertions
    suspended_org1_user = create(:user, organization: org1, is_suspended: true)
    create(:user_level_metric, user_id: suspended_org1_user.id, period: 'week', offset: 0, smartbites_consumed: 1, smartbites_created: 20, time_spent: 5, smartbites_score: 11)

    # inactive user(with time_spent=0 for period), should not change assertions
    inactive_org1_user = create(:user, organization: org1)
    create(:user_level_metric, user_id: inactive_org1_user.id, period: 'week', offset: 0, smartbites_consumed: 0, smartbites_created: 10, time_spent: 0, smartbites_score: 11)

    data = UserLevelMetric.get_organization_metrics(organization_id: org1.id, period: 'week', offset: 0)
    assert_equal({smartbites_consumed: 6, smartbites_created: 15, active_users: 3, average_session: 10}, data)
  end

  test 'organization metrics empty state' do
    org = create(:organization)
    data = UserLevelMetric.get_organization_metrics(organization_id: org.id, period: 'week', offset: 0)
    assert_equal({smartbites_consumed: 0, smartbites_created: 0, active_users: 0, average_session: 0}, data)
  end

  test 'percentile for inactive users' do
    org = create(:organization)
    users = create_list(:user, 4, organization: org)
    create(:user_level_metric, user_id: users[0].id, smartbites_score: 10, period: :day, offset: 0, time_spent: 20)
    create(:user_level_metric, user_id: users[1].id, smartbites_score: 10, period: :day, offset: 0, time_spent: 0) #inactive user

    # suspended user, should not change assertions
    suspended_org_user = create(:user, organization: org, is_suspended: true)
    create(:user_level_metric, user_id: suspended_org_user.id, period: 'day', offset: 0, smartbites_consumed: 1, smartbites_created: 20, time_spent: 5, smartbites_score: 11)

    assert_equal 75, UserLevelMetric.get_percentile_for_inactive_users(organization_id: org.id, period: :day, offset: 0)
  end

  test 'get percentile should fetch inactive percentile' do
    org = create(:organization)
    users = create_list(:user, 4, organization: org)
    create(:user_level_metric, user_id: users[0].id, smartbites_score: 10, percentile: 100, period: :day, offset: 0, time_spent: 20)
    create(:user_level_metric, user_id: users[1].id, smartbites_score: 10, percentile: 75, period: :day, offset: 0, time_spent: 0) #inactive user

    assert_equal 100, UserLevelMetric.get_percentile(user_id: users[0].id, period: :day, offset: 0)
    assert_equal 75, UserLevelMetric.get_percentile(user_id: users[1].id, period: :day, offset: 0)
    assert_equal 75, UserLevelMetric.get_percentile(user_id: users[2].id, period: :day, offset: 0)
    assert_equal 75, UserLevelMetric.get_percentile(user_id: users[3].id, period: :day, offset: 0)
  end

  test 'organization metrics drill down data' do
    org1, org2 = create_list(:organization, 2)

    # user1 has smartbites_comsumed 1,2,3,4, smartbites_created 10,20,30,40, time_spent - 5,10,15,20 on days 0,1,2,3
    # user2 has smartbites_consumed 5, smartbites_created 50, time_spent 25 on day 0 only
    org1_users = create_list(:user, 2, organization: org1)
    [0,1,2,3].each do |day_offset|
      create(:user_level_metric, user_id: org1_users.first.id, period: 'day', offset: day_offset, smartbites_consumed: day_offset+1, smartbites_created: (day_offset+1)*10, time_spent: (day_offset+1)*5, smartbites_score: day_offset+1)
    end

    create(:user_level_metric, user_id: org1_users.last.id, period: 'day', offset: 0, smartbites_consumed: 5, smartbites_created: 50, time_spent: 25, smartbites_score: 10)

    # two users in org2, with smartbites consumed 1,2 respectively, smartbites_created 3, 4 respectively. time spent - 5,10, all on week 0
    org2_users = create_list(:user, 2, organization: org2)
    org2_users.each_with_index do |org2_user, indx|
      create(:user_level_metric, user_id: org2_user.id, period: 'day', offset: 0, smartbites_consumed: indx+1, smartbites_created: indx+4, time_spent: (indx+1)*5, smartbites_score: indx+1)
    end

    # suspended user, should not change assertions
    suspended_org1_user = create(:user, organization: org1, is_suspended: true)
    create(:user_level_metric, user_id: suspended_org1_user.id, period: 'day', offset: 0, smartbites_consumed: 1, smartbites_created: 20, time_spent: 5, smartbites_score: 11)

    # inactive user(with time_spent= 0 for period), should not change assertions
    inactive_org1_user = create(:user, organization: org1)
    create(:user_level_metric, user_id: inactive_org1_user.id, period: 'day', offset: 0, smartbites_consumed: 0, smartbites_created: 10, time_spent: 0, smartbites_score: 11)

    expected_data = {
      smartbites_consumed: [6,2,3,4,0,0,0],
      smartbites_created: [60,20,30,40,0,0,0],
      average_session: [15,10,15,20,0,0,0],
      active_users: [2,1,1,1,0,0,0]
    }
    data = UserLevelMetric.get_organization_drill_down_data(organization_id: org1.id, team_id: nil, period: 'week', offset: 0)

    assert_equal expected_data, data
  end

  test 'organization metrics drill down data over timerange' do
    org1, org2 = create_list(:organization, 2)

    # user1 has smartbites_comsumed 1,2,3,4, smartbites_created 10,20,30,40, time_spent - 5,10,15,20 on days offsets over last 4 days
    # user2 has smartbites_consumed 5, smartbites_created 50, time_spent 25 on day 0 only
    org1_users = create_list(:user, 2, organization: org1)

    [0,1,2,3].each do |offset|
      day_offset = PeriodOffsetCalculator.applicable_offsets(Time.current - offset.send(:day))[:day]
      create(:user_level_metric, user_id: org1_users.first.id, period: 'day', offset: day_offset, smartbites_consumed: offset + 1, smartbites_created: (offset + 1)*10, time_spent: (offset + 1)*5, smartbites_score: offset + 1)
    end

    expected_data = {
      smartbites_consumed: [0, 0, 4, 3],
      smartbites_created: [0, 0, 40, 30],
      average_session: [0, 0, 20, 15],
      active_users: [0, 0, 1, 1]
    }

    timerange = [Time.current - 5.days, Time.current - 1.day]
    data = UserLevelMetric.get_organization_drill_down_data_for_timerange(organization_id: org1.id, team_id: nil, period: 'day', timerange: timerange)
    assert_equal expected_data, data
  end


  test 'fetch weekly stats per user and organization' do
    Timecop.freeze do
      org = create(:organization)
      users = create_list(:user, 4, organization: org)
      offset = PeriodOffsetCalculator.applicable_offsets(Time.now.utc)[:week]

      create(:user_level_metric, user_id: users[0].id, time_spent: 2000, smartbites_score: 10, percentile: 100, period: :week, offset: offset)
      create(:user_level_metric, user_id: users[1].id, time_spent: 23, smartbites_score: 0, percentile: 75, period: :week, offset: offset)
      create(:user_level_metric, user_id: users[2].id, time_spent: 1234, smartbites_score: 30, percentile: 85, period: :week, offset: offset)
      create(:user_level_metric, user_id: users[3].id, time_spent: 34159, smartbites_score: 10, percentile: 78, period: :week, offset: offset)

      data = UserLevelMetric.fetch_stats_for_weekly_activity_email(organization_id: org.id, user_id: users[0].id)
      assert_equal data[:points_earned], 10
      assert_equal data[:team_standing], 2
      assert_equal data[:avg_points_earned_by_team], 12

      data = UserLevelMetric.fetch_stats_for_weekly_activity_email(organization_id: org.id, user_id: users[1].id)
      assert_equal data[:points_earned], 0
      assert_equal data[:team_standing], 4
      assert_equal data[:avg_points_earned_by_team], 12

      data = UserLevelMetric.fetch_stats_for_weekly_activity_email(organization_id: org.id, user_id: users[2].id)
      assert_equal data[:points_earned], 30
      assert_equal data[:team_standing], 1
      assert_equal data[:avg_points_earned_by_team], 12

      data = UserLevelMetric.fetch_stats_for_weekly_activity_email(organization_id: org.id, user_id: users[3].id)
      assert_equal data[:points_earned], 10
      assert_equal data[:team_standing], 2
      assert_equal data[:avg_points_earned_by_team], 12
    end
  end
end

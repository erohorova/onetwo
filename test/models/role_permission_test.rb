require 'test_helper'

class RolePermissionTest < ActiveSupport::TestCase
  setup do
    Organization.any_instance.unstub :create_or_update_roles
    Role.any_instance.unstub :create_master_roles
    Role.any_instance.unstub :create_standard_roles
    Role.any_instance.unstub :create_role_permissions
  end

  test 'create role permissions for role based authorization enabled orgs' do
    assert_difference -> { RolePermission.count }, 80 do
      random_org = create(:organization, enable_role_based_authorization: true)
      random_org.run_callbacks(:commit)
    end
  end

  test 'correct permissions for different roles' do
    random_org = create(:organization, enable_role_based_authorization: true)
    random_org.run_callbacks(:commit)
    assert_equal 40, random_org.roles.where(name: 'admin').first.role_permissions.count

    assert_same_elements Permissions.get_permissions_for_role('admin').keys, random_org.roles.where(name: 'admin').first.role_permissions.collect(&:name)
    assert_same_elements Permissions.get_permissions_for_role('member').keys, random_org.roles.where(name: 'member').first.role_permissions.collect(&:name)
    assert_same_elements Permissions.get_permissions_for_role('collaborator').keys, random_org.roles.where(name: 'collaborator').first.role_permissions.collect(&:name)
    assert_same_elements Permissions.get_permissions_for_role('curator').keys, random_org.roles.where(name: 'curator').first.role_permissions.collect(&:name)
    assert_same_elements Permissions.get_permissions_for_role('trusted_collaborator').keys, random_org.roles.where(name: 'trusted_collaborator').first.role_permissions.collect(&:name)
  end

  test 'record when permission is enabled' do
    role_perm = data_event_role_permission('org_role_permission_enabled')
    role_perm.enable!
  end

  test 'record when permission is disabled' do
    role_perm = data_event_role_permission('org_role_permission_disabled')
    role_perm.disable!
  end

  def data_event_role_permission(event)
    stub_time
    org = create(:organization)
    actor = create(:user, organization: org)
    metadata = {user_id: actor.id, user_agent: 'chrome'}
    role = create(:role, organization: org)
    role_permission = create(:role_permission, role: role,
      name: 'MANAGE_CARD'
    )

    RequestStore.stubs(:read).returns(metadata)
    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub :perform_later

    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      org: Analytics::MetricsRecorder.org_attributes(org),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      timestamp: Time.now.to_i,
      event: event,
      recorder: 'Analytics::OrgRoleMetricsRecorder',
      additional_data: metadata.merge!(
        role_id:         role.id.to_s,
        role_name:       role.name,
        role_permission: role_permission.name,
      )
    )
    role_permission
  end
end

require 'test_helper'

class GroupTest < ActiveSupport::TestCase
  should have_db_column(:connect_to_social_mediums).of_type(:boolean).with_options(default: true)
  should have_db_column(:enable_mobile_app_download_buttons).of_type(:boolean).with_options(default: true)
  should have_db_column(:share_to_social_mediums).of_type(:boolean).with_options(default: true)
  should have_db_column(:daily_digest_enabled).of_type(:boolean).with_options(default: false)

  should belong_to :client
  should have_many :daily_group_stats
  validate_presence_of :creator

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    @org = create(:organization)
  end

  test "must have name and description" do
    assert build(:resource_group, organization: @org).save
    assert_not build(:resource_group, description: "").save
    assert_not build(:resource_group, name: "").save
  end

  test "member relationship" do
    student = create(:user, organization: @org)
    group = create(:resource_group, organization: @org)
    group.add_member student
    group.reload
    assert_equal group.members.count, 1
  end

  test "should return unanswered questions count" do
    group = create(:resource_group, organization: @org)
    user = create(:user, organization: @org)
    group.add_member user
    q1 = create(:question, user: user, group: group)
    q2 = create(:question, user: user, group: group)
    q3 = create(:question, user: user, group: group)
    a1 = create(:answer, user: user, commentable: q1)

    assert_equal group.unanswered_questions_count, 2
  end

  test 'should add user once' do
    group = create(:resource_group, organization: @org)
    user = create(:user, organization: @org)

    assert_difference ->{group.members.count}, 1do
      group.add_user(user: user, role: 'member')
    end
    assert_no_difference ->{group.members.count} do
      group.add_user(user: user, role: 'member')
    end

    assert_difference ->{group.admins.count}, 1 do
      group.add_user(user: user, role: 'admin')
    end
    assert_no_difference ->{group.admins.count} do
      group.add_user(user: user, role: 'admin')
    end

    assert_no_difference -> {group.users.count} do
      group.add_user(user: create(:user, organization: @org), role: 'bad role')
    end
  end

  test 'is user should work' do
    instructor = create(:user, organization: @org)
    student = create(:user, organization: @org)

    group = create(:resource_group, organization: @org)
    group.add_member student
    group.add_admin instructor

    assert group.is_user? instructor
    assert group.is_user? student

    pg = create(:private_group, resource_group: group)
    pg.add_member student
    pg.add_admin instructor

    assert pg.is_user? instructor
    assert pg.is_user? student
  end

  test 'is instructor should work' do
    instructor = create(:user, organization: @org)
    student = create(:user, organization: @org)

    group = create(:resource_group, organization: @org)
    group.add_member student
    group.add_admin instructor

    assert group.is_admin? instructor
    assert_not group.is_admin? student
  end

  test 'is student should work' do
    instructor = create(:user, organization: @org)
    student = create(:user, organization: @org)

    group = create(:resource_group, organization: @org)
    group.add_member student
    group.add_admin instructor

    assert group.is_member? student
    assert_not group.is_member? instructor
  end

  class UnpromoteTest < ActiveSupport::TestCase
    self.use_transactional_fixtures = false
    setup do
      skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
      WebMock.disable!
    end

    teardown do
      WebMock.enable!
    end

    test 'should unpromote on making private' do
      g = create(:resource_group, organization: @org, is_private: false)

      Promotion.expects(:unpromote).with(promotable: g).returns(nil).once
      g.update_attributes(is_private: true)
    end
  end

  test "should get tags" do
    group = create(:resource_group, organization: @org, created_at: 2.weeks.ago)
    user = create(:user, organization: @org)
    group.add_member user
    post = create(:post, group: group, user: user,
                  created_at: 1.week.ago,
                  title: 'title', message: 'post on #hw1')

    tags = group.get_tags(group.created_at, Time.now)
    assert_equal tags[0]['tag_name'], 'hw1'
    assert_equal tags[0]['tag_count'], 1

    post = create(:post, group: group, user: user,
                  title: 'title', message: 'another post on #hw1',
                  created_at: 1.hour.ago)
    post = create(:post, group: group, user: user,
                  title: 'title', message: 'post on #hw2',
                  created_at: 2.hours.ago)

    comment = create(:comment, user: user, commentable: post,
                     message: ' #comment')

    comment = create(:comment, user: user, commentable: post,
                     message: ' #comment ')

    tags = group.get_tags(group.created_at, Time.now)

    assert_equal tags.count, 3
    assert_equal tags[0]['tag_name'], 'hw1'
    assert_equal tags[0]['tag_count'], 2
    assert_equal tags[1]['tag_name'], 'comment'
    assert_equal tags[1]['tag_count'], 2
    assert_equal tags[2]['tag_name'], 'hw2'
    assert_equal tags[2]['tag_count'], 1

    new_group = create(:resource_group, organization: @org, created_at: 1.day.ago)
    new_group.add_member user
    post_without_tag = create(:post, group: new_group, user: user,
                              created_at: 1.hour.ago,
                              title: 'title', message: 'message')
    tags = new_group.get_tags(new_group.created_at, Time.now)
    assert_equal tags.first, nil
  end

  test "should get top tags" do
    group = create(:resource_group, organization: @org, created_at: 2.weeks.ago)
    user = create(:user, organization: @org)
    group.add_member user
    post = create(:post, group: group, user: user,
                  created_at: 1.week.ago,
                  title: 'title', message: 'post on #hw1')

    #creating tags
    post = create(:post, group: group, user: user,
                  title: 'title', message: 'another post on #hw1',
                  created_at: 1.hour.ago)
    post = create(:post, group: group, user: user,
                  title: 'title', message: 'post on #hw2',
                  created_at: 2.hours.ago)
    post = create(:post, group: group, user: user,
                  title: 'title', message: 'post on #science',
                  created_at: 2.hours.ago)
    post = create(:post, group: group, user: user,
                  title: 'title', message: 'post on #science',
                  created_at: 2.hours.ago)
    post = create(:post, group: group, user: user,
                  title: 'title', message: 'post on #science',
                  created_at: 2.hours.ago)
    post = create(:post, group: group, user: user,
                  title: 'title', message: 'post on #science',
                  created_at: 2.hours.ago)

    comment = create(:comment, user: user, commentable: post,
                     message: ' #comment')
    comment = create(:comment, user: user, commentable: post,
                     message: ' #comment ')
    comment = create(:comment, user: user, commentable: post,
                     message: ' #comment ')
    tags = group.get_top_tags(group.created_at, Time.now, 3)
    assert_equal tags[0]['tag_name'], 'science'
    assert_equal tags[0]['tag_count'], 4
    assert_equal tags[1]['tag_name'], 'comment'
    assert_equal tags[1]['tag_count'], 3
    assert_equal tags[2]['tag_name'], 'hw1'
    assert_equal tags[2]['tag_count'], 2
  end

  test 'get stats when no data exists' do
    group = create(:group)
    faculty = create(:user, organization: @org)
    group.add_admin faculty

    # stubs ElasticSearch
    sk_results = Searchkick::Results.new(Post, {}, {})
    Post.stubs(:search).returns(sk_results)
    PostRead.stubs(:reads_count).returns(0)

    stats = group.get_stats(faculty, Time.now.utc.to_date)

    assert_equal 0, stats[:weekly][:num_threads]
    assert_equal 0, stats[:weekly][:num_threads_without_replies]
    assert_equal 0, stats[:weekly][:num_replies]
    assert_equal [], stats[:weekly][:trending_tags]

    assert_equal 0, stats[:alltime][:num_threads]
    assert_equal 0, stats[:alltime][:num_threads_without_replies]
    assert_equal 0, stats[:alltime][:num_replies]

    assert_equal 30, stats[:daily_stats].count

    assert_equal 30, stats[:daily_user_stats].count
  end

  test "should return savannah? correctly" do
    client = create(:client, organization: @org)
    group1 = create(:resource_group, organization: @org)
    group1.app_user.email = 'notadmin'
    assert_not group1.savannah?
    group2 = create(:resource_group, organization: @org)
    group2.app_user.skip_reconfirmation!
    client.user.update(email: 'admin@course-master.com')
    group2.save

    group3 = create(:resource_group, website_url: nil, organization: @org)
    client.update(user: group2.app_user)
    assert_not group3.savannah?
    assert group2.savannah?
  end

  test "should return promotion_eligible? correctly" do
    client = create(:client, organization: @org)
    group2 = create(:resource_group, is_private: false, client: client, organization: @org)
    group2.app_user.skip_reconfirmation!
    group2.app_user.update(email: 'admin@course-master.com')
    group2.course_data_url = 'http://someurl.com'
    group2.end_date = nil
    group2.save

    assert group2.promotion_eligible?

    group2.update(end_date: 1.week.since)
    assert group2.promotion_eligible?

    group2.update(is_private: true)
    assert_not group2.promotion_eligible?

    group2.update(is_private: false, end_date: 1.week.ago)
    assert_not group2.promotion_eligible?
  end

  test "should delegate social enabled" do
    client = create(:client, organization: @org)
    group = create(:resource_group, client: client, organization: @org)
    assert group.social_enabled
    client.update!(social_enabled: false)
    assert_not group.social_enabled
  end

  test 'pubnub channel' do
    g = create(:group)
    public_channel_name = nil
    assert_difference ->{PubnubChannel.count} do
      public_channel_name = g.public_pubnub_channel_name
    end

    assert_no_difference ->{PubnubChannel.count} do
      assert_equal public_channel_name, g.reload.public_pubnub_channel_name
    end
  end

  test 'client_private_groups' do
    client = create(:client)
    assert scrub(Group.client_private_groups(client, 1).to_sql).include?("WHERE groups.type = 'PrivateGroup' AND groups.parent_id = 1 AND groups.client_id = " + client.id.to_s)
  end

  test 'open groups' do
    assert scrub(Group.open.to_sql).include?("WHERE groups.access = 'open'")
  end

  test '#update_memory_store updates memory store chache' do
    skip("Pending")
    # c = create(:client)
    org = create(:organization)
    g  = create(:group, organization: org)
    cr = g.client_resource_id
    refute CacheFactory.fetch_group(organization_id: org.id, client_resource_id: cr).daily_digest_enabled

    assert_difference 'Group.with_digest_enabled.count', 1 do
      g.update(daily_digest_enabled: true)
      g.update_memory_store
      assert CacheFactory.fetch_group(organization_id: org.id, client_resource_id: cr).daily_digest_enabled
    end
  end

  test 'access control for users in orgs who access forum through parent user' do
    create_default_org
    u = create(:user, organization: Organization.default_org)
    g1, g2 = create_list(:group, 2)
    g1.add_member(u)
    g2.add_admin(u)

    org = create(:organization)
    org_user = create(:user, organization: org, organization_role: 'member', parent_user: u)
    [g1,g2].each {|g| assert g.is_active_user?(org_user)}
    assert g1.is_active_member?(org_user)
    assert_not g1.is_active_admin?(org_user)
    assert g2.is_active_admin?(org_user)

    random_user = create(:user, organization: @org)
    random_org_user = create(:user, organization: org, organization_role: 'member', parent_user: random_user)
    [g1,g2].each do |g|
      assert_not g.is_active_user?(random_user)
      assert_not g.is_active_user?(random_org_user)
    end
  end

  test 'returns digest attributes' do
    group = create(:group)
    assert_equal [group.name, group.course_code, group.website_url], group.digest_attrs
  end

  class ScopesTest < ActiveSupport::TestCase
    setup do
      skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
      @org = create(:organization)
    end

    test '.latest sorts groups by latest activity (either joined or created)' do
      u1  = create :user, organization: @org
      u2  = create :user, organization: @org
      c   = create :client, user_id: u1.id
      rg  = create :resource_group, creator: u1

      i_pg1 = create :private_group, parent_id: rg.id, creator: u1, access: 'invite', client: c, created_at: Time.now - 5.minutes
      o_pg1 = create :private_group, parent_id: rg.id, creator: u1, client: c

      i_pg2 = create :private_group, parent_id: rg.id, creator: u2, access: 'invite', client: c
      o_pg2 = create :private_group, parent_id: rg.id, creator: u2, client: c

      # u1: create his/her own groups and later joins other group
      GroupsUser.create(user_id: u1.id, group_id: o_pg2.id, as_type: 'member', created_at: Time.now - 3.hour)
      GroupsUser.where(user_id: u1.id, group_id: o_pg1.id).first.update(created_at: Time.now - 3.hour)

      # u2: joins other group and then create his/her own groups
      GroupsUser.create(user_id: u2.id, group_id: o_pg1.id, as_type: 'member', created_at: Time.now - 1.hour)
      GroupsUser.where(user_id: u2.id, group_id: i_pg2.id).first.update({created_at: Time.now - 2.hour})
      GroupsUser.where(user_id: u2.id, group_id: o_pg2.id).first.update(created_at: Time.now - 3.hour)

      [u1, u2].each { |u| u.reload }
      _list1 = u1.groups.client_private_groups(c.id, rg.id).latest
      _list2 = u2.groups.client_private_groups(c.id, rg.id).latest

      # u1
      assert_equal _list1.count, 3
      assert_equal _list1, [i_pg1, o_pg1, o_pg2]

      # u2
      assert_equal _list2.count, 3
      assert_equal _list2, [o_pg1, i_pg2, o_pg2]
    end

    test '.with_digest_enabled retrieves group with `daily_digest_enabled` as TRUE' do
      rg1 = create :resource_group, organization: @org, daily_digest_enabled: true
      rg2 = create :resource_group, organization: @org, daily_digest_enabled: true
      rg3 = create :resource_group, organization: @org
      rg4 = create :resource_group, organization: @org

      groups = ResourceGroup.with_digest_enabled
      assert_same_ids groups, [rg1, rg2]
      refute_includes groups, rg3
      refute_includes groups, rg4
    end

  end
end

require 'test_helper'

class MdpUserCardTest < ActiveSupport::TestCase
  self.use_transactional_fixtures = true

  setup do
    @org = create(:organization)
    @user = create(:user, organization: @org)
    author = create(:user, organization: @org)
    @card = create(:card, organization: @org, author: author)
  end

  test 'should not create multiple MDP record for user per card' do
    mdp_record = @user.mdp_user_cards.create(card_id: @card.id,
      form_details: {
        source_of_mdp: "Discussion with Manager",
        sub_competency_value: "Understand Business Environment",
        additional_comments: "comments for MDP",
        learning_activity: "GVC Learning"
      }
    )

    dup_mdp_record = @user.mdp_user_cards.build(card_id: @card.id,
      form_details: {
        source_of_mdp: "Discussion with Manager",
        sub_competency_value: "Understand Business Environment",
        additional_comments: "comments for MDP",
        learning_activity: "GVC Learning"
      }
    )

    assert_raise ActiveRecord::RecordNotUnique do
      dup_mdp_record.save!
    end
  end

  test 'should create user_content_completion on creating mdp_card' do
    TestAfterCommit.enabled = true

    assert_difference -> {UserContentCompletion.count},1 do 
      @user.mdp_user_cards.create(card_id: @card.id,
        form_details: {
          source_of_mdp: "Discussion with Manager",
          sub_competency_value: "Understand Business Environment",
          additional_comments: "comments for MDP",
          learning_activity: "GVC Learning"
        }
      )
    end
  end
end
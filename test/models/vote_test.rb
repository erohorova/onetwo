require 'test_helper'

class VoteTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper
  # more coverage of vote with edge cases are done in
  # answer and question tests

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    ActiveJob::Base.queue_adapter = :inline
    @org = create(:organization)
    @group = create(:resource_group, organization: @org)
    @user = create(:user, organization: @org)
    @group.add_member @user
  end

  teardown do
    ActiveJob::Base.queue_adapter = :inline
  end

  test "validate uniqueness of voter id scoped to votable" do
    question = create(:question, user: @user, group: @group)
    user2 = create(:user, organization: @org)
    @group.add_member user2
    create(:vote, votable: question, voter: user2)

    duplicate_vote = build(:vote, votable: question, voter: user2)
    assert_not duplicate_vote.save

    answer = create(:answer, id: question.id, user: user2, commentable: question)
    vote = build(:vote, votable: answer, voter: @user)
    assert vote.save
  end

  test "answer vote" do
    question = create(:question, user: @user, group: @group)
    user2 = create(:user, organization: @org)
    @group.add_member user2
    answer = create(:answer, commentable: question, user: user2)
    create(:vote, votable: answer, voter: @user)
    answer.reload
    assert_equal answer.votes_count, 1
  end

  test "question vote" do
    question = create(:question, user: @user, group: @group)
    user2 = create(:user, organization: @org)
    @group.add_member user2
    create(:vote, votable: question, voter: user2)
    question.reload
    assert_equal question.votes_count, 1
  end

  test 'get votes by user' do
    user = create(:user, organization: @org)
    group = create(:resource_group)
    group.add_member user

    p1 = create(:post, user: user, group: group)
    p2 = create(:post, user: user, group: group)

    voter = create(:user, organization: @org)
    group.add_member voter

    create(:vote, voter: voter, votable: p1)

    user_voted_ids = Vote.get_votes_by_user(voter,
                                            votable_ids: [p1.id, p2.id],
                                            votable_type: 'Post')
    assert_equal user_voted_ids, [p1.id]

    blank_result = Vote.get_votes_by_user(voter,
                                              votable_ids: [],
                                              votable_type: 'Post')
    assert_equal blank_result, []

    # bad type
    bad_type = Vote.get_votes_by_user(voter,
                                          votable_ids: [p1.id, p2.id],
                                          votable_type: 'Blah')
    assert_equal bad_type, []

    #no type
    no_type = Vote.get_votes_by_user(voter,
                                         votable_ids: [p1.id, p2.id],
                                        )
    assert_equal no_type, []
  end

  test "no score on anonymous post" do
    post = create(:post, user: @user, anonymous: true, group: @group)
    user2 = create(:user, organization: @org)
    @group.add_member user2
    score = @user.score(@group)['score']

    vote = Vote.new
    vote.votable = post
    vote.voter = user2
    vote.weight = 1
    vote.save

    newscore = @user.score(@group)['score']
    assert_equal score, newscore
  end

  test 'validate user belongs to group' do
    poster = create(:user, organization: @org)
    g = create(:resource_group)
    g.add_member poster

    u = create(:user, organization: @org)
    g.add_member u
    p = create(:post, group: g, user: poster)
    v = Vote.new(votable: p, voter: u, weight: 1)
    assert v.save

    i = create(:user, organization: @org)
    g.add_admin i
    v = Vote.new(votable: p, voter: i, weight: 1)
    assert v.save

    n = create(:user, organization: @org)
    v = Vote.new(votable: p, voter: n, weight: 1)
    assert_not v.save
  end

  test 'validate user belongs to private group' do
    poster = create(:user, organization: @org)
    g = create(:private_group, resource_group: @group)
    g.members << poster

    u = create(:user, organization: @org)
    g.members << u
    p = create(:post, group: g, user: poster)
    v = Vote.new(votable: p, voter: u, weight: 1)
    assert v.save

    i = create(:user, organization: @org)
    g.admins << i
    v = Vote.new(votable: p, voter: i, weight: 1)
    assert v.save

    n = create(:user, organization: @org)
    v = Vote.new(votable: p, voter: n, weight: 1)
    assert_not v.save
  end

  test 'create and delete new active stream when user upvote something' do
    EdcastActiveJob.unstub :perform_later
    VideoStreamUpdateScoreJob.unstub :perform_later
    StreamCreateJob.unstub :perform_later

    stub_request(:post, %r{http://localhost:9200/_bulk})
    question = create(:question, user: @user, group: @group)
    user2 = create(:user, organization: @org)
    @group.add_member user2
    vote = create(:vote, votable: question, voter: user2)

    # stub update code

    Vote.any_instance.stubs(:generate_following_update).returns(nil)

    assert_difference -> {Stream.count} do
      vote.run_callbacks(:commit)
      stream = Stream.last
      assert_equal 'liked', stream.action
      assert_equal question, stream.item
      assert_equal user2, stream.user
      assert_equal @group, stream.group
    end

    # create one more vote and destroy it
    user3 = create(:user, organization: @org)
    @group.add_member(user3)
    v = create(:vote, votable: question, voter: user3)
    v.run_callbacks(:commit)

    assert_difference -> {Stream.count}, -1 do
      v.destroy
    end

    # now delete question, should delete the one remaining stream item
    assert_difference -> {Stream.count}, -1 do
      question.destroy
    end

    Vote.any_instance.unstub(:generate_following_update)
  end

  test 'video stream vote score update' do
    EdcastActiveJob.unstub :perform_later
    VideoStreamUpdateScoreJob.unstub :perform_later
    ActiveJob::Base.queue_adapter = :test
    user = create(:user, organization: @org)
    video_stream = create(:wowza_video_stream)
    assert_enqueued_with(job: VideoStreamUpdateScoreJob, args: [video_stream]) do
      vote = create(:vote, votable: video_stream.card, voter: user)
      vote.run_callbacks(:commit)
    end
  end

  test 'should not generate notification for own comment' do
    org = create(:organization)
    user1, user2 = create_list(:user, 2, organization: org)
    card = create(:card, organization: org, author_id: user1.id)

    comment = create(:comment, commentable: card, user: user1, message: "Awesoome")
    NotificationGeneratorJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    assert_difference ->{Notification.count}, 1 do
      vote1 = create(:vote, votable: comment, voter: user1)
      vote1.run_callbacks(:commit)
      vote2 = create(:vote, votable: comment, voter: user2)
      vote2.run_callbacks(:commit)
    end
  end

  test 'should generate notification' do
    IndexJob.stubs(:perform_later)
    NotificationGeneratorJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    Vote.any_instance.stubs(:record_card_like_event).returns true

    org = create(:organization)
    user1, user2 = create_list(:user, 2, organization: org)
    card = create(:card, organization: org, author_id: user1.id)

    assert_difference ->{Notification.count} do
      vote = create(:vote, votable: card, voter: user2)
      vote.run_callbacks(:commit)
    end
    IndexJob.unstub(:perform_later)
  end

  test 'video stream voted by' do
    user = create(:user, organization: @org)
    video_stream = create(:iris_video_stream)
    vote = create(:vote, votable: video_stream, voter: user)
    assert_equal true, video_stream.voted_by?(user)
  end

  class CardVoteMetricRecorderTest < ActiveSupport::TestCase
    test 'should invoke vote metric recorder job #vote' do
      IndexJob.stubs(:perform_later).returns true

      org = create(:organization)
      author, voter = create_list(:user,2, organization: org)
      card = create(:card, author: author)
      RequestStore.store[:request_metadata] = {
        platform: 'android',
        user_agent: 'Edcast Android',
        user_id: voter.id
      }
      vote = create(:vote, voter: voter, votable: card)
      EdcastActiveJob.unstub :perform_later
      Analytics::MetricsRecorderJob.unstub :perform_later
      Analytics::MetricsRecorderJob.expects(:perform_later)
      .with(
        has_entries(
          recorder: "Analytics::CardVoteRecorder",
          card: Analytics::MetricsRecorder.card_attributes(card),
          org: Analytics::MetricsRecorder.org_attributes(org),
          actor: Analytics::MetricsRecorder.user_attributes(voter),
          timestamp: Time.now.to_i,
          event: 'card_liked',
          additional_data: {
            platform: 'android',
            user_agent: 'Edcast Android',
            user_id: voter.id
          }
        )
      ).once
      vote.run_callbacks :commit
    end

    test 'should invoke card metric recorder job #unvote' do
      RequestStore.unstub(:read)
      Organization.any_instance.stubs(:create_admin_user)
      RequestStore.store[:request_metadata] = {platform: 'android', user_agent: 'Edcast Android'}

      org = create(:organization)
      author, voter = create_list(:user,2, organization: org)
      card = create(:card, author: author, organization: org)
      vote = create(:vote, voter: voter, votable: card)

      Timecop.freeze(2.days.from_now) do
        Analytics::MetricsRecorderJob.expects(:perform_later).with({
          recorder: "Analytics::CardVoteRecorder",
          card: Analytics::MetricsRecorder.card_attributes(card),
          org: Analytics::MetricsRecorder.org_attributes(org),
          actor: Analytics::MetricsRecorder.user_attributes(voter),
          timestamp: Time.now.to_i,
          event: 'card_unliked',
          additional_data: {platform: 'android', user_agent: 'Edcast Android'}
        })
        vote.destroy
      end
    end

  end
end

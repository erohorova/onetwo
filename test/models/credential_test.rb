require 'test_helper'

class CredentialTest < ActiveSupport::TestCase

  should belong_to :client
  should validate_uniqueness_of(:api_key)

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
  end

  test 'should not save credential without a client id' do
    cred = Credential.new(client: nil)
    assert_not cred.save
  end

  test 'should generate random api key and shared secret' do

    client = create(:client)
    credential1 = Credential.new(client: client)
    credential2 = Credential.new(client: client)

    credential1.save
    credential2.save

    assert_not_empty credential1.api_key
    assert_not_empty credential1.shared_secret



    assert_not_empty credential2.api_key
    assert_not_empty credential2.shared_secret

    assert_not_equal credential1.api_key, credential2.api_key
    assert_not_equal credential1.shared_secret, credential2.shared_secret
  end

  test 'should query admin_list' do
    clients = create_list(:client, 4)
    credential_ids = clients.map {|c| c.credentials.inject([]) {|arr, cred| arr << cred.id} }.flatten
    user = create(:user)

    clients.each { |c| c.admins << user}
    list = Credential.admin_list(user)

    assert_equal list.map(&:id), credential_ids
    assert_equal list.count, clients.count

  end
end

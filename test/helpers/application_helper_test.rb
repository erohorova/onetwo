require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase

  test 'should return nil if no co-branding image uploaded for org' do
    @current_org = create(:organization)
    assert_nil get_co_branding_image
  end

  test 'should return image if co-branding image uploaded for org' do
    @current_org = create(:organization)
    test_image = 'test/fixtures/images/test.png'
    @current_org.co_branding_logo = Rack::Test::UploadedFile.new(test_image, 'image/png')
    @current_org.save!
    assert_match @current_org.co_branding_logo.url, get_co_branding_image
  end

  class PageTitle < ActionView::TestCase
    setup do
      @long_content  = Faker::Lorem.paragraph(10)
      @user          = build :user
    end

    test 'truncates long text if text is greater than 60 characters' do
      assert_match '...', page_title(content: @long_content)
    end

    test 'does not truncates short text less than 60 characters' do
      refute_match '...', page_title(content: Faker::Lorem.characters(10))
    end

    test 'returns text with add user\'s name' do
      assert_match "#{@user.name} on EdCast", page_title(content: @long_content, user: @user)
    end
  end

  test 'returns handle without @ sign' do
    assert_match 'samplehandle', handle_no_at('@samplehandle')
  end

  class NotificationBadge < ActionView::TestCase
    include Devise::Test::ControllerHelpers

    setup do
      @user = create :user
    end

    test 'return 0 when current_user does not exist' do
      self.stubs(:current_user).returns(nil)
      assert_equal 0, get_unseen_notifications_count
    end

    test 'return unseen_count when unseen_count is set' do
      self.stubs(:current_user).returns(@user)
      @unseen_count = 5
      assert_equal 5, get_unseen_notifications_count
    end

    test 'return unread notifications for a user' do
      self.stubs(:current_user).returns(@user)
      @unseen_count = nil
      card = create :card
      UserNotificationRead.create(user: @user, seen_at: Time.now)
      Notification.create(user_id: @user.id, notifiable_id: card.id, notifiable_type: 'Card',
                          notification_type: 'new_card_upvote', unseen: true, app_deep_link_id: card.id, app_deep_link_type: 'card')
      assert_equal 1, get_unseen_notifications_count
    end

    def teardown
      self.unstub(:current_user)
    end
  end

  test "displays proper text after sanitization striping tags and symbols" do
    string = "<b>This should not display a &amp; symbol or HTML tags</b>"
    assert_equal(
      "This should not display a & symbol or HTML tags",
      ApplicationController.new.strip_tags(string)
    )
  end

  test "should display & symbol as it is" do
    string = "The & symbol here should be displayed as it is"
    assert_equal(
      "The & symbol here should be displayed as it is",
      ApplicationController.new.strip_tags(string)
    )
  end

  test "should display special characters as is and remove any markdown characters" do
    string = "The & <p>symbol</p> here _should_ be **displayed** as it is? Also, what about Antoine'Poole. 6 > 7???"
    assert_equal(
      "The & symbol here should be displayed as it is? Also, what about Antoine'Poole. 6 > 7???",
      ApplicationController.new.simplify_encoded_string(string)
    )
  end
end

require 'test_helper'

class PromotionsHelperTest < ActionView::TestCase
  test "duration" do
    start_date = Date.new(2015, 11, 10)
    end_date = Date.new(2015, 11, 20)
    course1 = create(:group, start_date: start_date, end_date: end_date)
    assert_equal duration(course1), "10 days"

    course2 = create(:group)
    assert_equal duration(course2), "self-paced"
  end
end

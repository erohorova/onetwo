require 'test_helper'

class CardsHelperTest < ActionView::TestCase
  include Api::V2::CardsHelper

  setup do
    @user = create(:user)
    @org = @user.organization
    @array_of_cards = CARD_STOCK_IMAGE
  end

  test "should check if array length is unchanged, currently, 100" do
    assert_equal 100, @array_of_cards.length
  end

  test "should send a default_image hash in an array for project_card" do
    card = create(:card, card_type:'project', card_subtype:'text')
    index = card.created_at.to_i % 100
    fixed_default_image = default_image(card, 'filestack')
    assert_equal [@array_of_cards[index]],fixed_default_image
  end

  test "should send a 0th image when image_index has some invalid value hash" do
    card = create(:card, card_type:'project', card_subtype:'text')
    stock_image_index = 101
    fixed_default_image = @array_of_cards[stock_image_index] || @array_of_cards[0]
    assert_equal nil, @array_of_cards[stock_image_index]
    assert_equal @array_of_cards[0],fixed_default_image
  end

  test "should not return a random image_url to be shown in view" do
    resource_article = create(:resource, type: 'Article' ,url: "https://www.honeybadger.io/",user_id: @user.id)
    card_1 = create(:card, card_type:"media", card_subtype: "link", resource:resource_article)
    response_url_1 = card_resource(card_1)
    refute_equal card_1&.resource&.image_url, response_url_1
  end

  test "should return a random image_url to be shown in view" do
    resource_article = create(:resource, type: 'Article' ,url: "https://www.honeybadger.io/",user_id: @user.id)
    resource_article.update_column(:image_url, nil)
    card_2 = create(:card, card_type:"media", card_subtype: "link", resource:resource_article)
    response_url_2 = card_resource(card_2)
    index = card_2.created_at.to_i % 100

    assert_equal @array_of_cards[index][:url], response_url_2
  end

  test "should return a filestack image to be shown in view" do
    card = create(:card, card_type:'project', card_subtype:'text')
    response_hash = card_filestack(card, @user.id, @org)
    index = card.created_at.to_i % 100

    assert_equal [@array_of_cards[index]], response_hash
  end

  test "should return card with updated thumbnail in filestack object" do
    filestack = [
        {
            "filename": "mp4_video.mp4",
            "mimetype": "video/mp4",
            "size": 383631,
            "source": "local_file_system",
            "url": "https://cdn.filestackcontent.com/abchsnejhdkfjdjlf",
            "handle": "5nwSqyLaRuSvhSzBWvNG",
            "status": "Stored"            
        },
        {
            "filename": "1.png",
            "mimetype": "image/png",
            "size": 121363,
            "source": "local_file_system",
            "url": "https://cdn.filestackcontent.com/new_thumbnail",
            "handle": "TZVXwFKHSZyckCCndbLn",
            "status": "Stored"
        }
    ]
    card = create(:card, card_type:'media', card_subtype:'video', filestack: filestack)
    response_hash = card_filestack(card, @user.id, @org)
    assert_equal response_hash[0][:thumbnail] , "https://cdn.filestackcontent.com/new_thumbnail"
  end

  test '#secured_url processes validation only for filestack URLs' do
    url = "http://www.marvel.com/avengers"
    response = secured_url(url, @user.id, @org)

    assert_equal url, response
  end

  test '#secured_url returns authenticated filestack URL when `ENV["SECURE_AUTHENTICATED_IMAGES"]` is enabled' do
    Settings.stubs(:serve_authenticated_images).returns('true')
    url = "http://cdn.filestackcontent.com/handle"

    response = secured_url(url, @user.id, @org)

    assert_match /\/uploads/, response
  end

  test '#secured_url returns security-enabled filestack URL when `ENV["SECURE_AUTHENTICATED_IMAGES"]` is disabled' do
    Settings.stubs(:serve_authenticated_images).returns('false')
    url = "http://cdn.filestackcontent.com/handle"

    response = secured_url(url, @user.id, @org)

    assert_match /\/security=p/, response
  end

  test '#secured_video_stream returns cloudfront url if video url fails validation' do
    path = "s3_bucket/folder1/file.m4a"
    assert_equal "https://#{Settings.aws_cdn_host}/#{path}", secured_video_stream(path, @user.id, @org)
  end

  test '#secured_video_stream returns cloudfront url if `ENV["SECURE_AUTHENTICATED_IMAGES"]` is disabled' do
    Settings.stubs(:serve_authenticated_images).returns('false')
    path = "s3_bucket/folder1/file.m4a"

    assert_equal "https://#{Settings.aws_cdn_host}/#{path}", secured_video_stream(path, @user.id, @org)
  end

  test '#secured_video_stream returns secured url if `ENV["SECURE_AUTHENTICATED_IMAGES"]` is disabled' do
    Settings.stubs(:serve_authenticated_images).returns('true')
    path = "video-streams-output-v2/folder1/folder2/file.m4a"

    assert_match /#{@org.host}\/past_stream\//, secured_video_stream(path, @user.id, @org)
  end
end

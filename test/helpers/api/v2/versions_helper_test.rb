require 'test_helper'

class VersionsHelperTest < ActionView::TestCase
  include Api::V2::VersionsHelper
  include Api::V2::CardsHelper

  setup do
    @user = create(:user)
    @org = @user.organization
  end

  test '#updated_fields_with_signed_filestack_urls returns secured filestack urls' do
    url = "https://cdn.filestackcontent.com/12345678"
    fields = [{"filestack" => [[], [OpenStruct.new(url: url)]]}]
    response = updated_fields_with_signed_filestack_urls(fields, @user, @org, false)
    assert_match /cdn.filestackcontent.com\/security=p:/, response[0]["filestack"][1][0].url
  end

  test '#updated_fields_with_signed_filestack_urls returns filestack urls for native apps' do
    url = "https://cdn.filestackcontent.com/12345678"
    fields = [{"filestack" => [[], [OpenStruct.new(url: url)]]}]
    response = updated_fields_with_signed_filestack_urls(fields, @user, @org, true)
    assert_equal url, response[0]["filestack"][1][0].url
  end

  test '#updated_fields_with_signed_filestack_urls returns encoded filestack urls' do
    Settings.stubs(:serve_authenticated_images).returns("true")
    url = "https://cdn.filestackcontent.com/12345678"
    fields = [{"filestack" => [[], [OpenStruct.new(url: url)]]}]
    response = updated_fields_with_signed_filestack_urls(fields, @user, @org, false)
    assert_match /#{@org.host}\/uploads\//, response[0]["filestack"][1][0].url
  end
end

require 'test_helper'

class FollowsHelperTest < ActionView::TestCase
  class ConfigureFollowLinkMethodTest < ActionView::TestCase
    setup do
      skip
      @u1 = create :user
      @u2 = create :user
    end

    test 'follow link for signed-in user' do
      html  = Nokogiri::HTML(configure_follow_link(opts: { css_class: 'follow', followable: @u1, user: @u2 }))
      attrs = html.xpath('/html/body/a').first.attributes
      child = html.xpath('/html/body/a').first.children

      assert_match /false/, attrs['data-following'].value
      assert_match /follows\?user_id\=#{@u1.id}/, attrs['data-request-link'].value
      assert_match /follow/, attrs['class'].value
      assert_match /javascript:/, attrs['href'].value
      assert_equal 'user', attrs['data-ec'].value
      assert child.length, 1
    end

    test 'unfollow link for signed-in user' do
      Follow.create(user: @u2, followable: @u1)
      @u2.reload
      html  = Nokogiri::HTML(configure_follow_link(opts: { followable: @u1, user: @u2 }))
      attrs = html.xpath('/html/body/a').first.attributes

      assert_match /true/, attrs['data-following'].value
      assert_match /follows\?user_id\=#{@u1.id}/, attrs['data-request-link'].value
      assert_equal 'user', attrs['data-ec'].value
    end

    test 'follow link for non-signed-in user' do
      html  = Nokogiri::HTML(configure_follow_link(opts: { followable: @u1, user: nil,
        key: :user_id, css_class: 'follow-button' }, show_image: false))
      attrs = html.xpath('/html/body/a').first.attributes
      child = html.xpath('/html/body/a').first.children

      assert_match /follow-button/, attrs['class'].value
      refute_equal 'follow-button follow', attrs['class'].value
      assert_match /javascript:false/, attrs['href'].value
      assert_match /Follow/, child.first.children.first.text
      assert_equal 'user', attrs['data-ec'].value
      assert_nil attrs['data-following']
      assert_not_nil attrs['data-request-link']
      assert child.length, 1
    end
  end
end

require 'test_helper'

class CardsHelperTest < ActionView::TestCase
  # class RenderTagListMethod < ActionView::TestCase
  #   test 'show prompt message if tags are empty' do
  #     html = Nokogiri::HTML(render_tag_list)

  #     prompt = html.xpath("//div[contains(@class,'tags-pitch')]").first
  #     refute_match /hide/, prompt.attributes['class'].value

  #     tags  = html.xpath("//div[contains(@class,'tags-list')]").first
  #     assert_match /hide/, tags.attributes['class'].value
  #   end

  #   test 'show tags if tags are present' do
  #     html      = Nokogiri::HTML(render_tag_list(['#apple', '#mango']))

  #     prompt    = html.xpath("//div[contains(@class,'tags-pitch')]").first
  #     assert_match /hide/, prompt.attributes['class'].value

  #     tags_list = html.xpath("//div[contains(@class,'tags-list')]").first
  #     refute_match /hide/, tags_list.attributes['class'].value

  #     tags      = html.xpath("//p[contains(@class,'tags')]").first.children.first.text
  #     refute_match /hide/, tags
  #   end
  # end

  # test 'return poll card answered or not' do
    # Analytics::MetricsRecorderJob.stubs(:perform_later)

  #   poll_card = create(:card, card_type: 'poll', card_subtype: 'text')
  #   user = create :user
  #   assert (not answered?(poll_card.id, user))
  #   QuizQuestionAttempt.create(quiz_question_id: poll_card.id, quiz_question_type: 'Card', user_id: user.id)
  #   assert answered?(poll_card.id, user)
  # end

  # test 'return default url if course image_url not present' do
  #   course = create :resource_group
  #   assert course_image_url(course), image_url("card_default_image_min.jpg")
  # end

  # test 'check formatted duration metadata' do
  #   card_with_no_duration = create(:card)
  #   card_with_duration = create(:card, duration: 70)
  #   card_with_ecl_duration = create(:card, ecl_metadata: {'duration_metadata' => {'calculated_duration' => 125 }})

  #   assert_nil formatted_duration_metadata(card_with_no_duration)
  #   assert_equal(
  #     {
  #       "calculated_duration" => 70,
  #       "calculated_duration_display" => "1 minute"
  #     },
  #     formatted_duration_metadata(card_with_duration)
  #   )
  #   assert_equal(
  #     {
  #       "calculated_duration" => 125,
  #       "calculated_duration_display" => "2 minutes"
  #     },
  #     formatted_duration_metadata(card_with_ecl_duration)
  #   )
  # end
end

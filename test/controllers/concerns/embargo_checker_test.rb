# frozen_string_literal: true

require 'test_helper'
require 'action_controller'

class EmbargoCheckerTest < ActionController::TestCase
  class OnlyTestFakeController < ActionController::Base
    include OrganizationContext
    include EmbargoChecker

    def embargo
      head :ok unless embargo_content_check
    end
  end

  setup do
    Rails.application.routes.draw do
      get 'embargo', controller: 'embargo_checker_test/only_test_fake', action: 'embargo'
    end

    @controller = OnlyTestFakeController.new
    @org = create(:organization)
    @user = create(:user, organization: @org)
    set_host @org
    sign_in @user

    # unapproved words
    Embargo.create(
      user: @user, organization: @org, value: 'value',
      category: 'unapproved_words'
    )

    # unapproved sites
    Embargo.create(
      user: @user, organization: @org,
      value: '*.com', category: 'unapproved_sites'
    )
    Embargo.create(
      user: @user, organization: @org,
      value: '*.net', category: 'unapproved_sites'
    )
    Embargo.create(
      user: @user, organization: @org,
      value: 'sample.com', category: 'unapproved_sites'
    )

    # approved sites
    Embargo.create(
      user: @user, organization: @org,
      value: 'http://good_url.com', category: 'approved_sites'
    )
    Embargo.create(
      user: @user, organization: @org,
      value: 'awesome.net', category: 'approved_sites'
    )
    Embargo.create(
      user: @user, organization: @org,
      value: '*.org', category: 'approved_sites'
    )
    @slash_ending = Embargo.create(
      user: @user, organization: @org,
      value: 'https://www.youtube.com/', category: 'unapproved_sites'
    )

    @controller.stubs(:new_user_session_url).returns('/')
  end

  teardown do
    Rails.application.reload_routes!
  end

  test 'should forbid #card params' do
    params = {card: { message: 'value', title: '', options: [{label: 'test'}, {label: 'value'}]}}
    get :embargo, params, format: :json

    assert_response :unprocessable_entity
  end

  test 'should forbid #card params if applyed strikethrough text style' do
    params = {card: { message: '~~value~~', title: '' }}
    get :embargo, params, format: :json
    assert_response :unprocessable_entity
  end

  test 'should forbid #card params if applyed underlined text style' do
    params = {card: { message: '++value++', title: '' }}
    get :embargo, params, format: :json
    assert_response :unprocessable_entity
  end

  test 'should forbid #card params if applyed italic text style' do
    params = {card: { message: '_value_', title: '' }}
    get :embargo, params, format: :json
    assert_response :unprocessable_entity
  end

  test 'should forbid #card params if applyed bold text style' do
    params = {card: { message: '**value**', title: '' }}
    get :embargo, params, format: :json
    assert_response :unprocessable_entity
  end

  test 'should forbid #card params if applyed combined text style' do
    params = {card: { message: "**v**_a_~~lu~~++e++", title: '' }}
    get :embargo, params, format: :json
    assert_response :unprocessable_entity
  end


  test 'should forbid #card params with embargo content in tags' do
    params = {
      card: {
        message: 'test',
        title: '',
        options: [
          {label: 'test'},
          {label: 'test'}
        ],
        topics: ['value', 'test', 'tags']
      }
    }
    get :embargo, params, format: :json

    assert_response :unprocessable_entity
  end

  test 'should forbid #comment params' do
    params = {comment: { message: 'value'}}
    get :embargo, params, format: :json

    assert_response :unprocessable_entity
  end

  test 'should forbid #resource params' do
    params = {resource: { link: 'http://test.com'}}
    get :embargo, params, format: :json

    assert_response :unprocessable_entity
  end

  test 'should forbid #resource embargo without wild character' do
    params = {resource: { link: 'sample.com'}}
    get :embargo, params, format: :json

    assert_response :unprocessable_entity
  end

  test 'should forbid #resource if url host in embargo' do
    # path in url should not affect to embargo checking
    params = {resource: { link: 'http://www.sample.com/some/path/names'}}
    get :embargo, params, format: :json

    assert_response :unprocessable_entity
  end

  test 'should pass #card params check' do
    params = {card: { message: 'good string', title: '', options: [{label: 'feature'}, {label: 'awesome'}]}}
    get :embargo, params, format: :json

    assert_response :ok
  end

  test 'should pass #comment params check' do
    params = {comment: { message: 'good'}}
    get :embargo, params, format: :json

    assert_response :ok
  end

  test 'should pass #resource params check' do
    params = {resource: { link: 'http://test.org'}}
    get :embargo, params, format: :json

    assert_response :ok
  end

  test 'should pass #resource params check for url with prefix' do
    # embargo approved site set as just: `awesome.org`
    # forbidden all *.org
    params = {resource: { link: 'https://awesome.net'}}
    get :embargo, params, format: :json

    assert_response :ok
  end

  test 'should forbid params which contain URL and no any matches in approved and unapproved sites' do
    params = {resource: { link: 'https://non-existed-url-in-both-categories.net'}}
    get :embargo, params, format: :json

    assert_response :unprocessable_entity
  end

  test 'should pass params which contain URL and non-full match detected in approved list' do
    # for URI like: *.org
    params = {resource: { link: 'https://not-fulle-matched-approved.org'}}
    get :embargo, params, format: :json

    assert_response :ok
  end

  test 'should not forbid content, if embargo does not exists in the organization' do
    org = create(:organization)
    user = create(:user, organization: org)
    set_host org
    sign_in user
    assert_empty org.embargos

    params = {resource: { link: 'sample.com'}}
    get :embargo, params, format: :json

    assert_response :ok
  end

  test 'should limit content by approved category also' do
    org = create(:organization)
    user = create(:user, organization: org)
    Embargo.create(
      user: user, organization: org,
      value: 'http://good_url.com', category: 'approved_sites'
    )
    set_host org
    sign_in user

    params = {resource: { link: 'good_url.org'}}
    get :embargo, params, format: :json

    assert_response :unprocessable_entity
  end

  test 'should pass check for sites and values ends with `/`' do
    params = {resource: { link: 'https://www.youtube.com/'}}
    get :embargo, params, format: :json
    assert_response :unprocessable_entity

    @slash_ending.update_attributes(category: 'approved_sites')
    get :embargo, params, format: :json
    assert_response :ok

    #should ignore path names
    params_new = {resource: { link: 'https://www.youtube.com/asdf'}}
    get :embargo, params_new, format: :json
    assert_response :ok
  end
end

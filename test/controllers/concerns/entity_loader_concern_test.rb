# frozen_string_literal: true

require 'test_helper'
require 'action_controller'

class EntityLoaderTest < ActionController::TestCase
  class OnlyTestFakeController < ActionController::Base
    include OrganizationContext
    include CompoundIdReader
    include EntityLoader

    def show
      @entity = load_entity_from_type_and_id(params[:entity_type], params[:entity_id])
      head :ok
    end

    def bulk_show
      @entities = load_entities_from_type_and_ids(params[:entity_type], params[:entity_ids])
      head :ok
    end
  end

  setup do
    Rails.application.routes.draw do
      get 'show', controller: 'entity_loader_test/only_test_fake', action: 'show'
      get 'bulk_show', controller: 'entity_loader_test/only_test_fake', action: 'bulk_show'
    end

    @controller = OnlyTestFakeController.new
    @org = create(:organization)
    @user = create(:user, organization: @org)
    set_host @org
    sign_in @user

    @controller.stubs(:new_user_session_url).returns('/')
  end

  teardown do
    Rails.application.reload_routes!
  end

  test 'should load entity #card' do
    card = create(:card, organization: @org, author_id: @user.id)
    get :show, entity_type: 'Card', entity_id: card.id

    assert_response :ok
    assert_equal card, assigns(:entity)
  end

  test 'should load entity #channel' do
    channel = create(:channel, organization: @org)
    get :show, entity_type: 'Channel', entity_id: channel.id

    assert_response :ok
    assert_equal channel, assigns(:entity)
  end

  test 'should load entity #organization' do
    get :show, entity_type: 'Organization', entity_id: @org.id

    assert_response :ok
    assert_equal @org, assigns(:entity)
  end

  test 'should load entity #team' do
    team = create(:team, organization: @org)
    get :show, entity_type: 'Team', entity_id: team.id

    assert_response :ok
    assert_equal team, assigns(:entity)
  end

  test 'should not load entity for non white listed entity_type' do
    get :show, entity_type: 'SomeUnknownType', entity_id: 1

    assert_response :ok
    assert_nil assigns(:entity)
  end

  test 'should load #cards from array of entities' do
    card = create(:card, organization: @org, author_id: @user.id)
    card2 = create(:card, organization: @org, author_id: @user.id)
    get :bulk_show, entity_type: 'Card', entity_ids: [card.id, card2.id]

    assert_response :ok
    assert_equal [card, card2], assigns(:entities)
  end

  test 'should load #channels from array of entities' do
    channel = create(:channel, organization: @org)
    channel2 = create(:channel, organization: @org)
    get :bulk_show, entity_type: 'Channel', entity_ids: [channel.id, channel2.id]

    assert_response :ok
    assert_equal [channel, channel2], assigns(:entities)
  end

  test 'should load #users from array of entities' do
    user = create(:user, organization: @org)
    user2 = create(:user, organization: @org)
    get :bulk_show, entity_type: 'User', entity_ids: [user.id, user2.id]

    assert_response :ok
    assert_equal [user, user2], assigns(:entities)
  end

end

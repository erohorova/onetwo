require 'test_helper'

class AccessLogFilterControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  class DummiesController < ActionController::Base
    include AccessLogFilter
    include NativeAppDetection
    include DetectPlatform
    def index
      head :ok
    end

    def different_uri
      @access_uri = 'signup.here'
      head :ok
    end
    def signup_uri
      @access_uri = 'signup.web'
      head :ok
    end
  end

  self.controller_class= DummiesController

  setup do
    skip #DEPRECATED
    AccessLogJob.unstub(:perform_later)
    @old_tracker = Tracking.instance_variable_get(:@tracker)
    Tracking.instance_variable_set(:@tracker, true)
  end

  teardown do
    Tracking.instance_variable_set(:@tracker, @old_tracker)
  end

  def with_dummy_routing
    with_routing do |set|
      set.draw do
        get 'access_log_index', :controller=>"access_log_filter_controller_test/dummies", :action=>"index"
        get 'access_log_different_uri', :controller=>"access_log_filter_controller_test/dummies", :action=>"different_uri"
        get 'access_log_signup_uri', :controller=>"access_log_filter_controller_test/dummies", :action=>"signup_uri"
        get 'access_log_not_found', :controller=>"access_log_filter_controller_test/dummies", :action=>"not_found"
      end
      yield
    end
  end

  test 'should not log access because user is not logged in' do
    with_dummy_routing do
      assert_no_difference -> {AccessLog.count} do
        get :index
        assert_response :success
      end
    end
  end

  test 'should log access because user is logged in' do
    user = create(:user)
    sign_in user
    with_dummy_routing do
      assert_difference -> {AccessLog.count} do
        get :index
        assert_response :success
        assert_not_nil cookies['access']
        assert_not_equal 'signup.here', AccessLog.last.access_uri
      end
    end
  end

  test 'should user the over ridden access_uri instance variable' do
    user = create(:user)
    sign_in user
    with_dummy_routing do
      assert_difference -> {AccessLog.count} do
        get :different_uri
        assert_response :success
        assert_not_nil cookies['access']
        assert_equal 'signup', JSON::parse(cookies['entry'])['type']
        assert_equal 'here', JSON::parse(cookies['entry'])['provider']
        assert_equal 'signup.here', AccessLog.last.access_uri
      end
    end
  end

  test 'should only log access once a day' do
    user = create(:user)
    sign_in user
    access_log = create(:access_log, user: user, platform: 'web', access_at: DateTime.current.midnight.to_date)
    with_dummy_routing do
      assert_no_difference -> {AccessLog.count} do
        get :index
        assert_response :success
        assert_not_nil cookies['access']
      end
    end
  end

  test 'should not call access log job if forum request in same day' do
    DummiesController.any_instance.stubs(:get_platform).returns("forum")
    user = create(:user)
    sign_in user
    access_log = create(:access_log, user: user, platform: 'forum', access_at: DateTime.current.midnight.to_date)
    AccessLogJob.expects(:perform_later).never
    with_dummy_routing do
      get :index
      assert_response :success
    end
    sign_out user
  end

  test 'should not write to the database if the access cookie exists' do
    user = create(:user)
    sign_in user
    request.cookies['access'] = DateTime.current.midnight.to_date.to_s(:db)
    with_dummy_routing do
      assert_no_difference -> {AccessLog.count} do
        get :index
        assert_response :success
        assert_not_nil cookies['access']
      end
    end
  end

  test 'should write to the database if the access cookie exists and older than a day' do
    user = create(:user)
    sign_in user
    request.cookies['access'] = (DateTime.current.midnight.to_date-2).to_s(:db)
    with_dummy_routing do
      assert_difference -> {AccessLog.count} do
        get :index
        assert_response :success
        assert_not_nil cookies['access']
      end
    end
  end

  test 'should write to the database if the access cookie exists and the access_uri is signup' do
    user = create(:user)
    sign_in user
    request.cookies['access'] = DateTime.current.midnight.to_date.to_s(:db)
    with_dummy_routing do
      assert_difference -> {AccessLog.count} do
        get :signup_uri
        assert_response :success
        assert_not_nil cookies['access']
        assert_equal 'repeat_signup', AccessLog.last.annotation
      end
    end
  end
end

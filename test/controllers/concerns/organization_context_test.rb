require 'test_helper'
require 'action_controller'

class OrganizationContextTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers
  class DummyController < ActionController::Base
    include OrganizationContext
    before_action :read_org_permissions, only: :share_card

    def show
      render text: 'hello'
    end

    def share_card
      render text: 'Hi'
    end

    def root_url(host: '/')
      host
    end
  end

  setup do
    @org = create :organization

    @controller = DummyController.new

    Rails.application.routes.draw do
      # Fun times
      get 'show', controller: 'organization_context_test/dummy', action: 'show'
      get 'share_card', controller: 'organization_context_test/dummy', action: 'share_card'
    end

    Settings.stubs(:platform_host).returns('www.edcast.com')
    Settings.stubs(:platform_domain).returns('edcast.com')
  end

  teardown do
    Rails.application.reload_routes!
  end

  test "open orgs" do
    @controller.stubs(:new_user_session_url).returns('/')
    open_org, closed_org = create_list(:organization, 2)
    open_org.update_attributes(is_open: true)
    @request.host = "#{open_org.host_name}.#{Settings.platform_host}"
    get :show
    assert_response :ok
  end

  test 'closed org' do
    @controller.stubs(:new_user_session_url).returns('/')
    open_org, closed_org = create_list(:organization, 2)

    @request.host = "#{closed_org.host_name}.#{Settings.platform_host}"
    get :show
    assert_response :redirect
    assert_redirected_to "/"
  end

  test "should set organization context" do
    @controller.expects(:set_organization_context).once
    get :show
  end

  test "should set organization context with params" do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @controller.stubs(:new_user_session_url).returns('/')

    get :show, org: @org.host_name
    org = assigns :current_org
    assert org
    assert_equal org.id, @org.id
  end

  test "should set organization context with request host" do
    @controller.stubs(:new_user_session_url).returns('/')
    @request.host = "#{@org.host_name}.#{Settings.platform_host}"
    get :show
    org = assigns :current_org
    assert org
    assert_equal org.id, @org.id
  end

  test "should get current org" do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @controller.stubs(:new_user_session_url).returns('/')

    get :show, org: @org.host_name
    assert_equal @controller.current_org, @org
  end

  test "404 for numeric ip" do
    @request.host = "1.2.3.4"
    get :show
    assert_redirected_to @controller.root_url(host: Settings.platform_host)
  end

  test 'should set org permission instance variables' do
    Organization.any_instance.unstub :create_default_config
    @controller.stubs(:new_user_session_url).returns('/')
    org = create(:organization, is_open: true)
    user = create(:user, organization: org)
    @request.host = "#{org.host_name}.#{Settings.platform_host}"

    sign_in(user)

    get :share_card
    assert_response :ok

    assert assigns(:can_share_on_social_nw)
    assert assigns(:can_create_channel)
  end
end

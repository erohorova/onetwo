require 'test_helper'

class Api::V2::TeamsControllerTest < ActionController::TestCase

  class AnalyticsV2Endpoint < ActionController::TestCase

    setup do
      @org = create(:organization)
      @team = create(:team, organization_id: @org.id)
      base_time = Time.now
      # These are the params sent over with the request
      @request_params = {
        from_date: (base_time - 1.days).strftime("%m/%d/%Y"),
        to_date: base_time.strftime("%m/%d/%Y"),
        id: @team.id.to_s
      }
      # These are the params that get forwarded to the query service
      @query_params = @request_params
        .slice(:from_date, :to_date)
        .merge(team_ids: [@team.id])
        .stringify_keys
    end

    test '#analytics_v2 team admin should be able to hit endpoint' do
      admin = create(:user, organization_id: @org.id)
      @team.add_admin(admin)
      api_v2_sign_in admin
      Team.expects(:fetch_v2_analytics).with(
        params: @query_params,
        current_org: @org
      ).returns ["stub_response"]

      get :analytics_v2, format: :json, **@request_params

      assert_equal 200, response.status
      assert_equal ["stub_response"], get_json_response(response)
    end

    test '#analytics_v2 ignores offset, limit, or other params' do
      admin = create(:user, organization_id: @org.id)
      @team.add_admin(admin)
      api_v2_sign_in admin
      Team.expects(:fetch_v2_analytics).with(
        params: @query_params,
        current_org: @org
      ).returns ["stub_response"]

      get :analytics_v2, format: :json, **@request_params.merge(
        limit: 123,
        offset: 456,
        foo: "bar"
      )

      assert_equal 200, response.status
      assert_equal ["stub_response"], get_json_response(response)
    end

    test '#analytics_v2 org admin should be able to hit endpoint' do
      admin = create(:user, organization_id: @org.id, organization_role: "admin")
      api_v2_sign_in admin
      Team.expects(:fetch_v2_analytics).with(
        params: @query_params,
        current_org: @org
      ).returns ["stub_response"]

      get :analytics_v2, format: :json, **@request_params

      assert_equal 200, response.status
      assert_equal ["stub_response"], get_json_response(response)
    end

    test '#analytics_v2 non-admin user is unauthorized' do
      user = create(:user, organization_id: @org.id)
      api_v2_sign_in user
      Team.expects(:fetch_v2_analytics).never

      get :analytics_v2, format: :json, **@request_params

      assert_equal 401, response.status
    end

    test '#analytics_v2 raises error with invalid team id' do
      admin = create(:user, organization_id: @org.id, organization_role: "admin")
      api_v2_sign_in admin
      Team.expects(:fetch_v2_analytics).never

      get :analytics_v2, format: :json, **@request_params.merge(id: "0")

      assert_equal 404, response.status
    end

    test '#analytics_v2 raises error if from_date or to_date not given' do
      admin = create(:user, organization_id: @org.id, organization_role: "admin")
      api_v2_sign_in admin
      %i{from_date to_date}.each do |key|

        get :analytics_v2, format: :json, **@request_params.except(key)

        assert_equal 422, response.status
        assert_equal(
          "`from_date` or `to_date` is missing",
          get_json_response(response)["message"]
        )
      end
    end
  end

  class MetricsV2Endpoint < ActionController::TestCase
    setup do
      @org = create(:organization)
      @team = create(:team, organization_id: @org.id)
      base_time = Time.now
      # These are the params sent over with the request
      @request_params = {
        from_date: (base_time - 1.days).strftime("%m/%d/%Y"),
        to_date: base_time.strftime("%m/%d/%Y"),
        team_ids: [@team.id]
      }
      # These are the params that get forwarded to the query service
      @query_params = @request_params
        .merge(limit: 10, offset: 0)
        .stringify_keys
    end

    test '#metrics_v2 team admin is forbidden from reaching endpoint' do
      admin = create(:user, organization_id: @org.id)
      @team.add_admin(admin)
      api_v2_sign_in admin
      Team.expects(:fetch_v2_analytics).never

      get :metrics_v2, format: :json, **@request_params

      assert_equal 401, response.status
    end

    test '#metrics_v2 regular member is forbidden from reaching endpoint' do
      user = create(:user, organization_id: @org.id)
      api_v2_sign_in user
      Team.expects(:fetch_v2_analytics).never

      get :metrics_v2, format: :json, **@request_params

      assert_equal 401, response.status
    end

    test '#metrics_v2 org admin can hit endpoint (with minimum param set)' do
      admin = create(:user, organization_id: @org.id, organization_role: "admin")
      api_v2_sign_in admin
      Team.expects(:fetch_v2_analytics).with(
        params: @query_params,
        current_org: @org
      ).returns ["stub_response"]

      get :metrics_v2, format: :json, **@request_params

      assert_equal 200, response.status
      assert_equal ["stub_response"], get_json_response(response)
    end

    test '#metrics_v2 allows custom limit and offset' do
      admin = create(:user, organization_id: @org.id, organization_role: "admin")
      api_v2_sign_in admin
      Team.expects(:fetch_v2_analytics).with(
        params: @query_params.merge('limit' => 20, 'offset' => 15),
        current_org: @org
      ).returns ["stub_response"]

      get :metrics_v2, format: :json, **@request_params.merge(
        limit: 20, 
        offset: 15
      )

      assert_equal 200, response.status
      assert_equal ["stub_response"], get_json_response(response)
    end

    test '#metrics_v2 prevents invalid limit and offset from being passed' do
      admin = create(:user, organization_id: @org.id, organization_role: "admin")
      api_v2_sign_in admin
      Team.expects(:fetch_v2_analytics).with(
        # limit and offset are converted to 0.
        params: @query_params.merge('limit' => 0, 'offset' => 0),
        current_org: @org
      ).returns ["stub_response"]

      get :metrics_v2, format: :json, **@request_params.merge(
        # Note: even if the limit and offset were passed like so,
        # It wouldn't cause SQL injection, because we use parameterization
        # for the queries.
        limit: "; DROP TABLE loljk",
        offset: "foobar"
      )

      assert_equal 200, response.status
      assert_equal ["stub_response"], get_json_response(response)
    end

    test '#metrics_v2 ignores other custom params' do
      admin = create(:user, organization_id: @org.id, organization_role: "admin")
      api_v2_sign_in admin
      Team.expects(:fetch_v2_analytics).with(
        params: @query_params,
        current_org: @org
      ).returns ["stub_response"]

      get :metrics_v2, format: :json, **@request_params.merge(foo: "bar")

      assert_equal 200, response.status
      assert_equal ["stub_response"], get_json_response(response)
    end

    test '#metrics_v2 raises error if from_date or to_date params are missing' do
      admin = create(:user, organization_id: @org.id, organization_role: "admin")
      api_v2_sign_in admin
      %i{from_date to_date}.each do |key|

        get :metrics_v2, format: :json, **@request_params.except(key)

        assert_equal 422, response.status
        assert_equal(
          "`from_date` or `to_date` is missing",
          get_json_response(response)["message"]
        )
      end
    end

  end
end
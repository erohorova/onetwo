require 'test_helper'
require 'action_controller'

class LimitOffsetConcernTest < ActionController::TestCase

  teardown do
    Rails.application.reload_routes!
  end

  test 'should only work for supplied actions' do
    class OnlyTestFakeController < ActionController::Base
      include LimitOffsetConcern
      has_limit_offset_constraints only: [:index], limit_default:5

      def show
        head :ok
      end

      def index
        head :ok
      end
    end
    Rails.application.routes.draw do
      get 'show', controller: 'limit_offset_concern_test/only_test_fake', action: 'show'
      get 'index', controller: 'limit_offset_concern_test/only_test_fake', action: 'index'
    end
    @controller = OnlyTestFakeController.new
    get :show
    assert_equal nil, assigns[:_limit]
  end

  test 'only test' do
    class OnlyTestFakeController < ActionController::Base
      include LimitOffsetConcern
      has_limit_offset_constraints only: [:index], limit_default:5

      def show
        head :ok
      end

      def index
        head :ok
      end
    end
    Rails.application.routes.draw do
      get 'show', controller: 'limit_offset_concern_test/only_test_fake', action: 'show'
      get 'index', controller: 'limit_offset_concern_test/only_test_fake', action: 'index'
    end
    @controller = OnlyTestFakeController.new
    get :index
    assert_equal 5, assigns[:_limit]
  end

  test 'except option should work for supplied actions' do
    class ExceptTestFakeController < ActionController::Base
      include LimitOffsetConcern
      has_limit_offset_constraints except: [:show], limit_default:8

      def show
        head :ok
      end
    end
    Rails.application.routes.draw do
      get 'show', controller: 'limit_offset_concern_test/except_test_fake', action: 'show'

    end
    @controller = ExceptTestFakeController.new
    assert_equal nil, assigns[:_limit]
  end

  test 'should work for all' do
    class FakeController < ActionController::Base
      include LimitOffsetConcern
      has_limit_offset_constraints  limit_default:10, offset_default:10

      def index
        head :ok
      end
    end
    Rails.application.routes.draw do
      get 'index', controller: 'limit_offset_concern_test/fake', action: 'index'
    end
    @controller = FakeController.new
    get :index
    assert_equal 10, assigns[:_limit]

  end

  test 'should set max limit' do
    class FakeController < ActionController::Base
      include LimitOffsetConcern
      has_limit_offset_constraints  limit_default:10, offset_default:10,  max_limit: 50

      def index
        head :ok
      end
    end
    Rails.application.routes.draw do
      get 'index', controller: 'limit_offset_concern_test/fake', action: 'index'
    end
    @controller = FakeController.new
    get :index, limit: 100
    assert_equal 50, assigns[:_limit]
  end

  #Please see history of LimitOffsetConcern we used @@_options and this test would fail if that is used again
  test 'limit offset options should not be overriden when used in the separate controller' do
    class TestClassAttributeController < ActionController::Base
      include LimitOffsetConcern
      has_limit_offset_constraints  limit_default:20

      def index
        head :ok
      end
    end

    class TestClassAttributeChangedController < ActionController::Base
      include LimitOffsetConcern
      has_limit_offset_constraints  limit_default:15

      def changed_index
        head :ok
      end
    end
    Rails.application.routes.draw do
      get 'index', controller: 'limit_offset_concern_test/test_class_attribute', action: 'index'
      get 'changed_index', controller: 'limit_offset_concern_test/test_class_attribute_changed'
    end
    @old_controller = TestClassAttributeController.new
    @controller = TestClassAttributeChangedController.new

    get :changed_index
    assert_equal 15, @controller.limit_offset_options[:limit_default]

    @controller = TestClassAttributeController.new
    get :index
    assert_equal 20, @controller.limit_offset_options[:limit_default]
  end
end

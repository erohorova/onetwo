require 'test_helper'
require 'action_controller'

class CompoundIdReaderTest < ActionController::TestCase

  class OnlyTestFakeController < ActionController::Base
    include OrganizationContext
    include CompoundIdReader

    def show
      @card_from_compound_id = load_card_from_compound_id(params[:content_id])
      head :ok
    end
  end

  setup do

    Rails.application.routes.draw do
      get 'show', controller: 'compound_id_reader_test/only_test_fake', action: 'show'
    end

    @controller = OnlyTestFakeController.new
    @org = create(:organization)
    @user = create(:user, organization: @org)
    @card = create(:card, organization: @org, author_id: @user.id)
    set_host @org
    sign_in @user
  end

  teardown do
    Rails.application.reload_routes!
  end

  test 'should read destiny ids as normal' do
    get :show, content_id: @card.id.to_s, format: :json
    assert_equal @card, assigns(:card_from_compound_id)
    assert_response :ok
  end

  test 'should call ecl module and get card from ecl' do
    ecl_id = "fdfd-fdf2"
    EclApi::CardConnector.any_instance.expects(:card_from_ecl_id).returns(@card)
    get :show, content_id: "ECL-#{ecl_id}", format: :json
    assert_equal @card, assigns(:card_from_compound_id)
    assert_response :ok
  end

end

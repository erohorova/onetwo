require 'test_helper'
require 'action_controller'

class DetectPlatformTest < ActionController::TestCase

  class OnlyTestFakeController < ActionController::Base
    include OrganizationContext
    include DetectPlatform

    def show
      head :ok
    end
  end

  setup do

    Rails.application.routes.draw do
      get 'show', controller: 'detect_platform_test/only_test_fake', action: 'show'
    end

    @controller = OnlyTestFakeController.new
    @org = create(:organization)
    @user = create(:user, organization: @org)
    set_host @org
    sign_in @user
  end

  teardown do
    Rails.application.reload_routes!
  end

  test 'should veriy request from branch metrics' do
    @request.user_agent = 'Branch Metrics API es=0.00'

    get :show, format: :json
    assert_response :ok
    assert_equal true, @controller.is_branch_metrics_request?
  end

end

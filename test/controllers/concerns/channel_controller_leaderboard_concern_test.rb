require 'test_helper'

class Api::V2::ChannelsControllerTest < ActionController::TestCase

  class ChannelControllerLeaderboardConcernTest < ActionController::TestCase
    setup do
      @start_time = Time.now.utc.beginning_of_day - 1.day
      @end_time = Time.now.utc.end_of_day - 1.day
      Time.stubs(:now).returns @end_time
      @org = create :organization
      @admin = create :user, organization: @org, organization_role: "admin"
      @random_user = create :user, organization: @org, organization_role: "member"
      @curator = create :user, organization: @org, organization_role: "member"
      @channel = create :channel, organization: @org

      @expected_query = (
        "select * from \"channel_aggregation_daily\" " +
        "where org_id = %{organization_id} " +
        "AND time >= %{start_date}s " +
        "AND time <= %{end_date}s " +
        "AND analytics_version = %{version} " +
        "AND channel_id = %{channel_id} " +
        "ORDER BY ASC ;"
      )
      @expected_params = {
        organization_id: @org.id.to_s,
        start_date: @start_time.to_i,
        end_date: @end_time.to_i,
        version: '2.0.0',
        channel_id: @channel.id.to_s,
        order: 'ASC'
      }
      @stub_influx_response = [{
        "tags" => {},
        "values" => [{
          "time" => "#{@start_time.strftime("%Y-%m-%d")}T00:00:00Z",
          "org_id" => @org.id.to_s,
          "channel_id" => @channel.id.to_s,
          "num_monthly_active_users_total" => 1,
          "num_monthly_active_users_in_web_platform" => 2,
          "num_monthly_active_users_in_ios_platform" => 3,
          "num_monthly_active_users_in_android_platform" => 4,
          "num_monthly_active_users_in_null_platform" => 5,
          "foo" => "bar"
        }]
      }]
      @apply_channel_stubs = -> (**opts) do
        opts = {
          limit: 10,
          offset: 0,
          start_date: @start_time.to_i,
          end_date: @end_time.to_i
        }.merge opts
        Channel
          .any_instance
          .stubs(:get_top_cards_data)
          .with(opts)
          .returns "stub_top_cards_data"
        Channel
          .any_instance
          .stubs(:get_top_contributors)
          .with(opts)
          .returns "stub_top_contributors"
        Channel
          .any_instance
          .stubs(:get_top_scoring_users)
          .with(opts)
          .returns "stub_top_scoring_users"
      end
      @expected_result = [{
        "tags" => {
          "top_cards" => "stub_top_cards_data",
          "top_contributors" => "stub_top_contributors",
          "top_scoring_users" => "stub_top_scoring_users"
        },
        "values" => [{
          "time" => "#{@start_time.strftime("%Y-%m-%d")}T00:00:00Z",
          "org_id" => @org.id.to_s,
          "channel_id" => @channel.id.to_s,
          "num_monthly_active_users" => {
            "total"   => 1,
            "web"     => 2,
            "ios"     => 3,
            "android" => 4,
            "null"    => 5,
          },
          "foo" => "bar"
        }],
        "name"=>"channel_aggregation_daily"
      }]
      create :channels_curator, user: @curator, channel: @channel
    end

    test '/aggregated_metrics requires start date' do
      api_v2_sign_in @admin
      get :aggregated_metrics, id: @channel.id
      assert_equal "422", response.code
      assert_equal "No start date given", JSON.parse(response.body)["message"]
    end

    test '/top_cards requires start date' do
      api_v2_sign_in @admin
      get :top_cards, id: @channel.id
      assert_equal "422", response.code
      assert_equal "No start date given", JSON.parse(response.body)["message"]
    end

    test '/top_users requires start date' do
      api_v2_sign_in @admin
      get :top_users, id: @channel.id
      assert_equal "422", response.code
      assert_equal "No start date given", JSON.parse(response.body)["message"]
    end

    test '/aggregated_metrics accepts end date' do
      @apply_channel_stubs.call(end_date: @end_time.to_i)
      api_v2_sign_in @admin
      InfluxQuery.expects(:fetch).with(
        @expected_query,
        @expected_params.merge(end_date: @end_time.to_i)
      ).returns @stub_influx_response
      get :aggregated_metrics, {
        id: @channel.id,
        start_date: @start_time.to_i,
        end_date: @end_time.to_i
      }
      assert_equal '200', response.code
      assert_equal @expected_result, JSON.parse(response.body)
    end

    test '/aggregated_metrics provides records for all days in range' do
      @apply_channel_stubs.call(
        start_date: (@start_time - 1.days).to_i,
        end_date: @end_time.to_i
      )
      api_v2_sign_in @admin
      InfluxQuery.expects(:fetch).with(
        @expected_query,
        @expected_params.merge(
          start_date: (@start_time - 1.days).to_i,
          end_date: @end_time.to_i
        )
      ).returns @stub_influx_response
      get :aggregated_metrics, {
        id: @channel.id,
        # This query includes the day before yesterday, and there is no
        # data for that day. So a zero-activity record gets added.
        start_date: (@start_time - 1.days).to_i,
        end_date: @end_time.to_i
      }
      assert_equal '200', response.code
      actual = JSON.parse(response.body)
      @expected_result[0]["values"].push(
        # This is the default record which is pushed for the day that
        # have no activity.
        "time" => "#{(@start_time - 1.days).utc.strftime("%Y-%m-%d")}T00:00:00Z",
        "analytics_version" => Settings.influxdb.event_tracking_version,
        "channel_id" => @channel.id.to_s,
        "num_cards_added_to_channel" => 0,
        "num_cards_commented_in_channel" => 0,
        "num_cards_completed_in_channel" => 0,
        "num_cards_liked_in_channel" => 0,
        "num_cards_removed_from_channel" => 0,
        "num_channel_follows" => 0,
        "num_channel_visits" => 0,
        "num_monthly_active_users" => {
          "total" => 0,
          "android" => 0,
          "extension" => 0,
          "ios" => 0,
          "web" => 0
        },
        "org_id" => @org.id.to_s
      )
      @expected_result[0]["values"].sort_by! do |record|
        Time.parse(record["time"]).to_i
      end
      assert_equal @expected_result, actual
    end

    test '/top_cards accepts end date' do
      @apply_channel_stubs.call(end_date: @end_time.to_i)
      api_v2_sign_in @admin
      get :top_cards, {
        id: @channel.id,
        start_date: @start_time.to_i,
        end_date: @end_time.to_i
      }
      assert_equal '200', response.code
      assert_equal(
        @expected_result[0]["tags"].slice("top_cards"),
        JSON.parse(response.body)
      )
    end

    test '/top_users accepts end date' do
      @apply_channel_stubs.call(end_date: @end_time.to_i)
      api_v2_sign_in @admin
      get :top_users, {
        id: @channel.id,
        start_date: @start_time.to_i,
        end_date: @end_time.to_i
      }
      assert_equal '200', response.code
      assert_equal(
        @expected_result[0]["tags"].slice("top_contributors", "top_scoring_users"),
        JSON.parse(response.body)
      )
    end

    test '/aggregated_metrics raises error if channel not found' do
      api_v2_sign_in @admin
      get :aggregated_metrics, id: "fake", start_date: @start_time.to_i
      assert_equal "404", response.code
    end

    test '/top_cards raises error if channel not found' do
      api_v2_sign_in @admin
      get :top_cards, id: "fake", start_date: @start_time.to_i
      assert_equal "404", response.code
    end

    test '/top_users raises error if channel not found' do
      api_v2_sign_in @admin
      get :top_users, id: "fake", start_date: @start_time.to_i
      assert_equal "404", response.code
    end

    test '/aggregated_metrics request is valid for org admins' do
      @apply_channel_stubs.call
      api_v2_sign_in @admin
      InfluxQuery.expects(:fetch).with(
        @expected_query,
        @expected_params
      ).returns @stub_influx_response
      get :aggregated_metrics, id: @channel.id, start_date: @start_time.to_i
      assert_equal '200', response.code
      assert_equal @expected_result, JSON.parse(response.body)
    end

    test '/top_cards request is valid for org admins' do
      @apply_channel_stubs.call
      api_v2_sign_in @admin
      get :top_cards, id: @channel.id, start_date: @start_time.to_i
      assert_equal '200', response.code
      assert_equal(
        @expected_result[0]["tags"].slice("top_cards"),
        JSON.parse(response.body)
      )
    end

    test '/top_users request is valid for org admins' do
      @apply_channel_stubs.call
      api_v2_sign_in @admin
      get :top_users, id: @channel.id, start_date: @start_time.to_i
      assert_equal '200', response.code
      assert_equal(
        @expected_result[0]["tags"].slice("top_contributors", "top_scoring_users"),
        JSON.parse(response.body)
      )
    end

    test '/aggregated_metrics request is valid for channel curators' do
      @apply_channel_stubs.call
      api_v2_sign_in @curator
      InfluxQuery.expects(:fetch).with(
        @expected_query,
        @expected_params
      ).returns @stub_influx_response
      get :aggregated_metrics, id: @channel.id, start_date: @start_time.to_i
      assert_equal '200', response.code
      assert_equal @expected_result, JSON.parse(response.body)
    end

    test '/top_cards request is valid for channel curators' do
      @apply_channel_stubs.call
      api_v2_sign_in @curator
      get :top_cards, id: @channel.id, start_date: @start_time.to_i
      assert_equal '200', response.code
      assert_equal(
        @expected_result[0]["tags"].slice("top_cards"),
        JSON.parse(response.body)
      )
    end

    test '/top_users request is valid for channel curators' do
      @apply_channel_stubs.call
      api_v2_sign_in @curator
      get :top_users, id: @channel.id, start_date: @start_time.to_i
      assert_equal '200', response.code
      assert_equal(
        @expected_result[0]["tags"].slice("top_contributors", "top_scoring_users"),
        JSON.parse(response.body)
      )
    end

    test '/aggregated_metrics request is invalid unless user is org admin or channel curator' do
      @apply_channel_stubs.call
      api_v2_sign_in @random_user
      get :aggregated_metrics, id: @channel.id, start_date: @start_time.to_i
      assert_equal '401', response.code
      assert_equal(
        "must be curator of channel or organization admin",
        JSON.parse(response.body)["message"]
      )
    end

    test '/aggregated_metrics fills provides fill records even if there are no actual records' do
      @apply_channel_stubs.call
      api_v2_sign_in @curator
      InfluxQuery.expects(:fetch).with(
        @expected_query,
        @expected_params
      ).returns []
      get :aggregated_metrics, id: @channel.id, start_date: @start_time.to_i
      assert_equal '200', response.code
      expected = [
        {
          "name" => "channel_aggregation_daily",
          "tags" => {
            "top_cards" => "stub_top_cards_data",
            "top_contributors" => "stub_top_contributors",
            "top_scoring_users" => "stub_top_scoring_users"
          },
          "values" => [{
            # This is the default record for a day that has no actual activity.
            "analytics_version" => "2.0.0",
            "channel_id" => @channel.id.to_s,
            "num_cards_added_to_channel" => 0,
            "num_cards_commented_in_channel" => 0,
            "num_cards_completed_in_channel" => 0,
            "num_cards_liked_in_channel" => 0,
            "num_cards_removed_from_channel" => 0,
            "num_channel_follows" => 0,
            "num_channel_visits" => 0,
            "org_id" => @org.id.to_s,
            "time" => "#{@start_time.beginning_of_day.strftime("%Y-%m-%d")}T00:00:00Z",
            "num_monthly_active_users" => {
              "total" => 0,
              "android" => 0,
              "extension" => 0,
              "ios" => 0,
              "web" => 0
            }
          }]
        }
      ]
      assert_equal expected, JSON.parse(response.body)
    end

    # Note: this seems like a very specific test case, and it is.
    # We had errors caused by this situation in QA environment.
    test '/aggregated_metrics works if there are results, but tags key is nil' do
      @apply_channel_stubs.call
      api_v2_sign_in @curator
      InfluxQuery.expects(:fetch).with(
        @expected_query,
        @expected_params
      ).returns [{
        "values" => [],
        "name" => Analytics::ChannelMetricsAggregationRecorder::MEASUREMENT
      }]
      get :aggregated_metrics, id: @channel.id, start_date: @start_time.to_i
      assert_equal '200', response.code
      expected = [
        {
          "name" => "channel_aggregation_daily",
          "tags" => {
            "top_cards" => "stub_top_cards_data",
            "top_contributors" => "stub_top_contributors",
            "top_scoring_users" => "stub_top_scoring_users"
          },
          "values" => [{
            # This is the default record for a day that has no actual activity.
            "analytics_version" => "2.0.0",
            "channel_id" => @channel.id.to_s,
            "num_cards_added_to_channel" => 0,
            "num_cards_commented_in_channel" => 0,
            "num_cards_completed_in_channel" => 0,
            "num_cards_liked_in_channel" => 0,
            "num_cards_removed_from_channel" => 0,
            "num_channel_follows" => 0,
            "num_channel_visits" => 0,
            "org_id" => @org.id.to_s,
            "time" => "#{@start_time.beginning_of_day.strftime("%Y-%m-%d")}T00:00:00Z",
            "num_monthly_active_users" => {
              "total" => 0,
              "android" => 0,
              "extension" => 0,
              "ios" => 0,
              "web" => 0
            }
          }]
        }
      ]
      assert_equal expected, JSON.parse(response.body)
    end

    test '/top_cards request is invalid unless user is org admin or channel curator' do
      @apply_channel_stubs.call
      api_v2_sign_in @random_user
      get :top_cards, id: @channel.id, start_date: @start_time.to_i
      assert_equal '401', response.code
      assert_equal(
        "must be curator of channel or organization admin",
        JSON.parse(response.body)["message"]
      )
    end

    test '/top_users request is invalid unless user is org admin or channel curator' do
      @apply_channel_stubs.call
      api_v2_sign_in @random_user
      get :top_users, id: @channel.id, start_date: @start_time.to_i
      assert_equal '401', response.code
      assert_equal(
        "must be curator of channel or organization admin",
        JSON.parse(response.body)["message"]
      )
    end

  end
end
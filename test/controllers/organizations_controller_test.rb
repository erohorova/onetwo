require 'test_helper'

class OrganizationsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  test 'create the organization' do
    create_default_org
    user = create(:user, organization: Organization.default_org)
    sign_in user
    Organization.any_instance.unstub :create_admin_user
    assert_difference ->{User.count}, 2 do
      assert_difference ->{Organization.count} do
        post :create, organization: {name: 'test organization', host_name: 'test', description: 'test description'}
      end
    end
    organization = Organization.last
    newuser = User.last
    assert_redirected_to organization.home_page
    assert organization.is_admin?(newuser)
  end

# -----------------------------------------------------------------------------
# COMMENTED OUT UNTIL WE FIX BROWSERIFY IN TEST ENVIRONMENT
# -----------------------------------------------------------------------------

=begin

  test 'render the new template' do
    user = create(:user)
    sign_in user
    get :new
    assert_template :new
  end

=end

  test 'show action only for org members' do
    org = create(:organization)
    u = create(:user, organization: org)
    sign_in u
    get :show, id: org.slug
    assert_response :not_found
    sign_out u
  end

  test 'show action #wwww' do
    org = create(:organization)
    u = create(:user, organization: org, organization_role: 'member')

    # wrong org
    sign_in u
    get :show
    assert_response :not_found
    sign_out u
  end

# -----------------------------------------------------------------------------
# COMMENTED OUT UNTIL WE FIX BROWSERIFY IN TEST ENVIRONMENT
# -----------------------------------------------------------------------------

=begin

  test 'show action #non-www' do
    create_default_org
    org = create(:organization)
    u = create(:user, organization: org, organization_role: 'member')

    set_host(org)
    sign_in u

    get :show

    assert_response :ok
    assert_equal org, assigns(:organization)
    sign_out u
  end

=end

  test 'host name validity' do
    create_default_org
    Settings.stubs(:disallowed_hosts).returns(['try', 'unbounce'])
    u = create(:user)
    set_host u.organization
    sign_in u
    org = create(:organization, host_name: 'ge')
    get :host_name_check, host_name: 'ge'
    assert_response :unprocessable_entity

    get :host_name_check, host_name: 'www'
    assert_response :unprocessable_entity

    get :host_name_check, host_name: 'try'
    assert_response :not_acceptable

    get :host_name_check, host_name: 'www.try'
    assert_response :not_acceptable

    get :host_name_check, host_name: 'ok'
    assert_response :no_content
    Settings.unstub(:disallowed_hosts)
    sign_out u
  end

  def org_add_user_params(user_id, user_email, user_role)
    {user_id: user_id, user_email: user_email, user_role: user_role}
  end
  def set_api_request_header(payload,api_key,shared_secret)
    token = JWT.encode(payload, shared_secret,'HS256')
    request.headers['X-API-KEY'] = api_key
    request.headers['X-API-TOKEN'] = token
  end

  test 'add user should not try to clone if client mapped to default org' do
    create_default_org
    cli = create(:client)
    cred = cli.valid_credential
    user_id = 1
    user_email = 'a@a.com'
    user_role = 'admin'

    default_org = Organization.default_org
    default_org.update_attributes(client_id: cli.id)

    request.headers['X-API-KEY'] = cred.api_key
    request.headers['X-API-TOKEN'] = "somerandomtoken"
    org_params = org_add_user_params(user_id, user_email, user_role)
    set_api_request_header(org_params,cred.api_key, cred.shared_secret)
    # Client is mapped to default org. This is for edcast.org use case on savannah, which is mapped to default org
    User.any_instance.expects(:clone_for_org).never
    assert_difference  [->{ClientsUser.count}, ->{User.count}] do
      post :add_user, org_params.merge(format: :json)
      assert_response :ok
    end

    u = User.last
    assert_equal default_org.id, u.organization_id
  end

  test 'add user for never seen before user' do
    create_default_org
    User.any_instance.expects(:reindex_async).returns(true).times(4) # 5 cases in this test where a new user is created or existing user is updated. But case 1 is excluded because it is an after commit
    cli = create(:client)
    # org = create(:organization, client: client)
    cred = cli.valid_credential
    user_id = 1
    user_email = 'a@a.com'
    user_role = 'admin'
    request.headers['X-API-KEY'] = cred.api_key
    request.headers['X-API-TOKEN'] = "somerandomtoken"
    # Case 1: This client is not associated with an org yet
    #this case is deprecated, every valid client should belongs to a organization
    org_params = org_add_user_params(user_id, user_email, user_role)
    set_api_request_header(org_params,cred.api_key, cred.shared_secret)

    assert_no_difference  [->{ClientsUser.count}, ->{User.count}] do
      post :add_user, org_params.merge(format: :json)
      assert_response :unauthorized
    end

    #it creates user only in current org not in default org
    org = create(:organization, client: cli)
    assert_difference ->{ClientsUser.count} do
      assert_difference  ->{User.count}, 1 do
        post :add_user, org_params.merge(format: :json)
        assert_response :ok
      end
    end

    u = User.last
    org_u = User.last
    assert_equal org, org_u.organization
    assert_equal 'admin', org_u.organization_role
    assert_equal user_email, org_u.email
    assert_equal u, org_u

    # user already exists in both org and clients user. third call should do nothing
    assert_no_difference  [->{User.count}, ->{ClientsUser.count}] do
      post :add_user, org_params.merge(format: :json)
      assert_response :ok
    end

    # now a fourth call with a different role, should change roles
    user_role = 'member'
    org_params = org_add_user_params(user_id, user_email, user_role)
    set_api_request_header(org_params,cred.api_key, cred.shared_secret)
    assert_no_difference  [->{User.count}, ->{ClientsUser.count}] do
      post :add_user, org_params.merge(format: :json)
      assert_response :ok
    end
    assert_equal 'member', org_u.reload.organization_role

    # fifth case. Only org user exists then also it should not create user in www or
    user_id = 2
    user_email = 'b@b.com'
    user_role = 'member'
    org_u = create(:user, organization: org, organization_role: 'member', email: user_email)
    org_params = org_add_user_params(user_id, user_email, user_role)
    set_api_request_header(org_params,cred.api_key, cred.shared_secret)
    assert_difference  [->{ClientsUser.count}] do
      assert_no_difference ->{User.count} do
        post :add_user, org_params.merge(format: :json)
        assert_response :ok
      end
    end
    u = User.last
    assert_equal u, org_u.reload.parent_user

    # bad credentials
    request.headers['X-API-TOKEN'] = "somerandomtoken"
    post :add_user,  user_id: user_id, user_email: user_email, user_role: user_role, format: :json
    assert_response :unauthorized
  end

# -----------------------------------------------------------------------------
# COMMENTED OUT UNTIL WE FIX BROWSERIFY IN TEST ENVIRONMENT
# -----------------------------------------------------------------------------

=begin

  test 'it should show error when organization creation fails' do
    user = create(:user)
    sign_in user
    assert_no_difference ->{Organization.count} do
      post :create, organization: {name: '', host_name: '', description: ''}
    end
    assert_match(/Team is not created! Please enter a valid data./, flash[:error].to_s)
    sign_out user
  end

=end

end

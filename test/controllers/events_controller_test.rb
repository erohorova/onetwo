require 'test_helper'

class EventsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    Event.any_instance.stubs :schedule_reminder_email

    create_default_org
    @group = create(:group)
    @user = create(:user)

    @group.add_member @user

    create_list(:event, 4, :public_real, group: @group, user: @user, event_start: Time.now.utc + 7.days, event_end: Time.now.utc + 8.days)
  end

  def response_setup
    @event = create(:event, user: @user, group: @group, event_start: Time.now.utc + 3.days, event_end: Time.now.utc + 4.days)
    @event.reload
    @expected_event = {
                       'id' => @event.id,
                       'name' => @event.name,
                       'description' => @event.description,
                       'address' => @event.address,
                       'event_start' => @event.event_start.strftime("%Y-%m-%dT%H:%M:%S.%LZ"),
                       'event_end' => @event.event_end.strftime("%Y-%m-%dT%H:%M:%S.%LZ"),
                       'latitude' => @event.latitude,
                       'longitude' => @event.longitude,
                       'state' => @event.state,
                       'privacy' => @event.privacy,
                       'rsvp_yes_count' => 1,
                       'current_user' => (@event.user_rsvp @user).as_json(only: [:id, :rsvp]),
                       'owner' => @user.as_json(only: [:id, :name, :first_name,
                                                      :last_name]),
                       'ical' => @event.ical.url,
                       'googlecal_url'=> @event.googlecal_url,
                       'owner_id' => @event.user_id,
                       'conference_type' => @event.conference_type,
                       'conference_url' => @event.conference_url,
                       'host_url' => @event.host_url,
                       'conference_email' => @event.conference_email,
                       'hangout_discussion_enabled' => @event.hangout_discussion_enabled,
                       'context_enabled' => @event.context_enabled,
                       'youtube_url'=> @event.youtube_url
                      }

    @expected_event['owner']['name'] = @user.name
    @expected_event['owner']['bio'] = @user.bio
    @expected_event['owner']['roles'] = ['member']
    @expected_event['owner']['picture'] = @user.photo(:medium)
    @expected_event['owner']['handle'] = @user.handle
    @expected_event['owner']['followers_count'] = @user.followers_count
    @expected_event['owner']['self'] = true
    @expected_event['owner']['full_name'] = @user.full_name
    @expected_event['owner']['is_suspended'] = false
    @expected_event['owner']['status'] = @user.status
    @expected_event['owner']['coverimages'] = @user.fetch_coverimage_urls

  end

  test ":api should list all events in the group" do
    user = create(:user)
    @group.add_member user
    event = create(:event, :public_real, group: @group, user: user, event_start: Time.now.utc + 1.day, event_end: Time.now.utc + 2.days)

    # this should not show up
    completed_event = create(:event, :public_real, group: @group, user: user, event_start: 2.days.ago, event_end: 1.day.ago)

    # this should show up
    ongoing_event = create(:event, :public_real, group: @group, user: user, event_start: 1.day.ago, event_end: Time.now.utc + 1.day)

    sign_in user

    get :index, group_id: @group.id, format: :json
    assert_response :success
    resp = get_json_response(response)
    assert_equal 6, resp.count

    get :index, group_id: @group.id, limit: 2, format: :json
    assert_response :success
    resp = get_json_response(response)
    assert_equal 2, resp.count

    # order of events should be by start date
    assert_equal [ongoing_event.id, event.id], resp.map{|r| r['id']}

    get :show, group_id: @group.id, id: event.id, format: :json
    assert_response :success
    assert_template :show

    post :create, {
                   :group_id => @group.id,
                   event: {
                           :name => "name", :description => "desc",
                           :address => "place", :event_start => Time.now,
                           :event_end => Time.now + 1.hour,
                           :state => "open", :privacy => "public"
                          },
                   format: :json
                  }
    assert_response :created

    patch :update, {
                    :group_id => @group.id,
                    :id => event.id,
                    event: {
                            name: "someothername"
                           },
                    format: :json
                   }
    assert_response :success

    event = create(:event, group: @group, user: user)

    delete :destroy, {
                      :group_id => @group.id,
                      :id => event.id,
                      format: :json
                     }
    assert_response :success

    group = create(:group)
    group.add_member user

    create_list(:event, 20, user: user, group: group, latitude: 10, longitude: 20)

    get :plot, group_id: group.id, limit: 4, format: :json
    assert_response :success

    sign_out user
  end

  test "should get the concerned event" do
    response_setup
    sign_in @user
    get :show, group_id: @event.group.id, id: @event.id, format: :json
    resp = get_json_response(response)

    assert_equal resp['events'].except('callback_token'), @expected_event
    assert_equal resp['events']['id'], ActiveSupport::MessageEncryptor.
        new(Edcast::Application.config.secret_key_base).
        decrypt_and_verify(resp['events']['callback_token'])

    sign_out @user
  end

  test "should create a new event" do
    sign_in @user
    assert_difference -> { Event.count }, 1 do
      post :create, {
                     :group_id => @group.id,
                     event: {
                             :name => "event1", :description => "desc",
                             :address => "somewhere", :event_start => Time.now,
                             :event_end => Time.now + 1.hour,
                             :state => "open", :privacy => 'public'
                            },
                     format: :json
                    }
    end

    event = get_json_response(response)

    events_user = EventsUser.where(:user_id => @user.id, :event_id => event['events']['id']).first
    assert_template :show
    assert event
    assert_equal event['events']['name'], 'event1'
    assert_equal event['events']['privacy'], 'public'
    assert_equal event['rsvps'][0]['events_user_id'], events_user.id
    assert_equal event['rsvps'][0]['self'], true
    assert_equal event['rsvps'].count, 1
    assert_nil event['events']['conference_type']
    sign_out @user
  end


  test "should set hangout type and discussion enabled" do
    sign_in @user
    assert_difference -> { Event.count }, 1 do
      post :create, {
          :group_id => @group.id,
          event: {
              :name => "event1", :description => "desc",
              :address => "somewhere", :event_start => Time.now,
              :event_end => Time.now + 1.hour,
              :state => "open", :privacy => 'public',
              :conference_type =>"onair", :hangout_discussion_enabled => true
          },
          format: :json
      }
    end

    event = get_json_response(response)
    event_from_db = Event.last
    @event_id = ActiveSupport::MessageEncryptor.
        new(Edcast::Application.config.secret_key_base).
        decrypt_and_verify(event['events']['callback_token'])

    assert_equal event['events']['id'], @event_id
    assert_equal event_from_db.name, 'event1'
    assert_template :show
    assert event
    assert_equal event['events']['conference_type'], 'onair'
    assert event['events']['hangout_discussion_enabled']
    sign_out @user
  end

  test "should update hangout with correct callback token" do
    sign_in @user
    assert_difference -> { Event.count }, 1 do
      post :create, {
          :group_id => @group.id,
          event: {
              :name => "event1", :description => "desc",
              :address => "somewhere", :event_start => Time.now,
              :event_end => Time.now + 1.hour,
              :state => "open", :privacy => 'public',
              :conference_type =>"onair", :hangout_discussion_enabled => true
          },
          format: :json
      }
    end


    event_from_db = Event.last
    url = 'http://somesite.com'
    post :hangout, {
        :data => event_from_db.encrypt,
        :url => url
    }
    event_from_db = Event.last
    assert_response 200
    assert_equal event_from_db.conference_url, url
  end



  test "should not update hangout with incorrect callback token" do
    sign_in @user
    assert_difference -> { Event.count }, 1 do
      post :create, {
          :group_id => @group.id,
          event: {
              :name => "event1", :description => "desc",
              :address => "somewhere", :event_start => Time.now,
              :event_end => Time.now + 1.hour,
              :state => "open", :privacy => 'public',
              :conference_type =>"onair", :hangout_discussion_enabled => true
          },
          format: :json
      }
    end

    event_from_db = Event.last
    post :hangout, {
        :data => 'someincorrecttoken',
        :url => 'http://somesite.com'
    }
    assert_response 401
    assert_equal event_from_db, Event.last
  end


  test "should update youtube with correct callback token" do
    sign_in @user
    assert_difference -> { Event.count }, 1 do
      post :create, {
          :group_id => @group.id,
          event: {
              :name => "event1", :description => "desc",
              :address => "somewhere", :event_start => Time.now,
              :event_end => Time.now + 1.hour,
              :state => "open", :privacy => 'public',
              :conference_type =>"onair", :hangout_discussion_enabled => true
          },
          format: :json
      }
    end


    event_from_db = Event.last
    youtube_url = 'http://somesite.com'
    post :hangout, {
        :data => event_from_db.encrypt,
        :youtube_url => youtube_url
    }
    event_from_db = Event.last
    assert_response 200
    assert_equal event_from_db.youtube_url, youtube_url
  end



  test "should not update youtube with incorrect callback token" do
    sign_in @user
    assert_difference -> { Event.count }, 1 do
      post :create, {
          :group_id => @group.id,
          event: {
              :name => "event1", :description => "desc",
              :address => "somewhere", :event_start => Time.now,
              :event_end => Time.now + 1.hour,
              :state => "open", :privacy => 'public',
              :conference_type =>"onair", :hangout_discussion_enabled => true
          },
          format: :json
      }
    end

    event_from_db = Event.last
    post :hangout, {
        :data => 'someincorrecttoken',
        :youtube_url => 'http://somesite.com'
    }
    assert_response 401
    assert_equal event_from_db, Event.last
  end



  test "should only make callback token available to event creator " do
    user = create(:user)
    user1 = create(:user)
    group = create(:group)
    group.add_member user
    group.add_member user1
    event = create(:event, :public_real, group: group, user: user)

    sign_in user

    get :index, group_id: group.id, limit: 2, format: :json
    assert_response :success
    resp = get_json_response(response)
    first_event = resp[0]
    assert first_event['callback_token']

    sign_out user

    sign_in user1

    get :index, group_id: group.id, limit: 2, format: :json
    assert_response :success
    resp = get_json_response(response)
    first_event = resp[0]
    assert_nil first_event['callback_token']

  end


  test "should not allow a group non member to access event" do
    u = create(:user)
    sign_in u
    post :create, {
                   :group_id => @group.id,
                   event: {
                           :name => "event1", :description => "desc",
                           :address => "somewhere", :event_start => Time.now,
                           :event_date => (Time.now + 2.hour), :state => "open",
                           :privacy => 'public'
                          },
                   format: :json
                  }

    assert_response :unauthorized

    get :index, group_id: @group.id, format: :json
    assert_response :unauthorized

    get :show, group_id: @group.id, id: @group.events.first.id, format: :json
    assert_response :unauthorized

    event = create(:event, group: @group, user: @user)
    delete :destroy, group_id: @group.id, id: event.id, format: :json
    assert_response :unauthorized

    event = create(:event, group: @group, user: @user)
    patch :update, {
                    :group_id => @group.id,
                    id: event.id,
                    event: {
                            :name => "another event name"
                           },
                    format: :json
                   }
    assert_response :unauthorized
    sign_out u
  end

  test "should delete the event" do
    event = create(:event, :public_real, user: @user, group: @group)

    sign_in @user
    assert_difference -> { Event.count }, -1 do
      delete :destroy, group_id: @group.id, id: event.id, format: :json
    end
    sign_out @user
  end

  test "should raise not found on updating non existent event" do
    sign_in @user
    patch :update, {
                    :group_id => @group.id,
                    id: 'x',
                    event: {
                            :name => "somename"
                           },
                    format: :json
                   }
    assert_response :not_found
    sign_out @user
  end

  test "should keep comments count" do
    user = create(:user)
    group = create(:group)
    group.add_member user

    event = create(:event, user: user, group: group)

    old_count = event.comments_count

    event.comments << create(:comment, user: user, commentable: event)
    event.reload

    assert_not_equal old_count, event.comments_count
  end

  test "should not allow non creator to update or delete event" do
    user1 = create(:user)
    user2 = create(:user)

    group = create(:group)
    group.add_member user1
    group.add_member user2

    event = create(:event, group: group, user: user1)

    patch :update, { group_id: group.id, id: event.id, event: { name: "asda" }, format: :json}
    assert_response :unauthorized

    delete :destroy, :group_id => group.id, id: event.id, format: :json
    assert_response :unauthorized
  end

  test "return 'myevents' for user" do
    user = create(:user)
    user2 = create(:user)
    group = create(:group)

    group.add_member user
    group.add_member user2

    create_list(:event, 10, user: user2, group: group, event_start: Time.now + 2.day,
                event_end: Time.now + 2.day + 3.hour)
    event = create(:event, user: user2, group: group, event_start: Time.now + 1.day,
                   event_end: Time.now + 1.day + 3.hour)
    create(:events_user, user: user, event: event, rsvp: 'yes')
    create_list(:event, 10, user: user, group: group, event_start: Time.now + 1.day,
                event_end: Time.now + 1.day + 3.hour)

    sign_in user
    get :index, group_id: group.id, myevents: true, format: :json
    assert_response :success
    resp = get_json_response(response)
    assert_equal resp.count, 11 # all 10 created by user and one with rsvp yes
    sign_out user
  end

  test "should plot all events" do
    user = create(:user)
    group = create(:group)
    group.add_member user

    create_list(:event, 20, user: user, group: group, latitude: 10, longitude: 20, event_start: 1.day.from_now)

    sign_in user
    get :plot, group_id: group.id, format: :json
    assert_response :success
    resp = get_json_response(response)
    assert_equal resp.count, 20
    assert_equal resp[0]['longitude'], 20
    assert_equal resp[0]['latitude'], 10
    sign_out user
  end

  # -----------------------------------------------------------------------------
  # COMMENTED OUT UNTIL WE FIX BROWSERIFY IN TEST ENVIRONMENT
  # -----------------------------------------------------------------------------
=begin

  test 'watch action should work' do
    c = create(:client)
    org = create(:organization, client: c)
    api_key = c.valid_credential.api_key
    shared_secret = c.valid_credential.shared_secret

    u = create(:user)
    user_email = u.email
    user_id = 1
    ClientsUser.create(client: c, user: u, client_user_id: 1)

    resource_id = 1
    g = create(:group, organization: org, client_resource_id: resource_id)
    g.add_member(u)
    user_role = 'student'

    timestamp = Time.now.to_i

    parts = [shared_secret, timestamp, user_id, user_email, user_role, resource_id].compact
    token = Digest::SHA256.hexdigest(parts.join('|'))

    request_params = {api_key: api_key, token: token, user_id: user_id, user_email: user_email, resource_id: resource_id, timestamp: timestamp, user_role: user_role}
    e = create(:event, youtube_url: 'xxxx123', group: g, conference_type: 'normal', hangout_discussion_enabled: true, context_enabled: true, user: u)

    get :watch, request_params.merge({group_id: g.id, id: e.id})
    assert_response :success
    assert_template 'watch'

    expected = {api_key: api_key, token: token, timestamp: timestamp, user_email: user_email, user_role: user_role, resource_id: resource_id, user_id: user_id, youtube_url: e.youtube_url, context_label: e.name, context_id: e.id, discussion_enabled: true}
    [:api_key, :token, :user_email, :user_id, :resource_id, :youtube_url, :timestamp, :user_role, :discussion_enabled].each do |k|
      assert_equal expected[k].to_s, assigns(k).to_s, message: "#{k} doesn't match"
    end

    # now make the user say no the event
    eu = EventsUser.where(event: e, user: u).first
    eu.update_attributes(rsvp: 'no')

    get :watch, request_params.merge({group_id: g.id, id: e.id})
    assert_response :unauthorized

    # non existant / event without a youtube url
    get :watch, request_params.merge({group_id: 'xxx', id: e.id})
    assert_response :bad_request

    e.update_attributes(youtube_url: nil)
    get :watch, request_params.merge({group_id: g.id, id: e.id})
    assert_response :bad_request

    # authorization failure
    get :watch, request_params.merge({token: 'bad token', group_id: g.id, id: e.id})
    assert_response :forbidden

    # event without edcast
    e.update_attributes(youtube_url: 'xxxx123', hangout_discussion_enabled: false)
    eu.update_attributes(rsvp: 'yes')
    get :watch, request_params.merge({group_id: g.id, id: e.id})
    assert_response :success
    assert_not /edcast-forum/.match(response.body)
  end

=end

  test "should return webex enabled or not for events" do
    org = create(:organization)
    user = create(:user)
    group = create(:group, organization: org)
    group.add_member user
    sign_in user

    get :webex_enabled, group_id: group.id, format: :json
    assert_response :success
    resp = get_json_response(response)
    assert_equal resp["enabled"], false

    webex_config = create(:webex_config, organization: group.organization)
    get :webex_enabled, group_id: group.id, format: :json
    assert_response :success
    resp = get_json_response(response)
    assert_equal resp["enabled"], true

    sign_out user
  end
end

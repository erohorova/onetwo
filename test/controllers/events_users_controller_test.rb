require 'test_helper'

class EventsUsersControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'

    Event.any_instance.stubs :schedule_reminder_email
    @org = create(:organization)
    @client = create(:client, organization: @org)
    @user = create(:user, organization: @org)
    @rsvpuser = create(:user, organization: @org)
    @group = create(:group, client: @client, organization: @org)

    @group.add_member @rsvpuser
    @group.add_member @user

    @event = create(:event, group: @group, user: @user)
  end

  test ":api should create or update rsvp" do
    sign_in @user
    u = create(:user)
    @group.add_member(u)
    event = create(:event, group: @group, user: u)
    assert_difference -> { EventsUser.count }, 1 do
      post :create, {
                     event_id: event.id,
                     events_user: {
                                   :rsvp => 'yes'
                                  },
                     format: :json
                    }
    end
    assert_response :success
    assert_equal event.rsvp_yes_count, ActiveSupport::JSON.decode(response.body)

    sign_out @user

    sign_in @user
    user = create(:user)
    @group.add_member user
    event = create(:event, group: @group, user: user)
    event_user = create(:events_user, :rsvp_yes, event: event, user: @user)

    patch :update, {
                    event_id: event.id,
                    id: event_user.id,
                    events_user: {
                                  :rsvp => 'maybe'
                                 },
                    format: :json
                   }
    assert_response :success
    [event, event_user].each(&:reload)
    assert_equal event.rsvp_yes_count, ActiveSupport::JSON.decode(response.body)
    assert_equal event_user.rsvp, 'maybe'
    sign_out @user
  end

  test "should only allow group user to rsvp" do
    create_default_org
    user = create(:user)
    sign_in user

    event = create(:event, group: @group, user: @user)

    post :create, {
                    event_id: event.id,
                    events_user: {
                                  :rsvp => 'maybe'
                                 },
                    format: :json
                   }
    assert_response :unauthorized
    sign_out user
  end

  test "should allow only event user to update their rsvp" do
    user = create(:user)
    group = create(:group)
    group.add_member user
    event = create(:event, group: group, user: user)
    event_user = EventsUser.where(user: user, event: event).first

    u = create(:user)
    group.add_member u
    sign_in u
    patch :update, {
                    event_id: event.id,
                    id: event_user.id,
                    events_user: {
                                  :rsvp => 'maybe'
                                 },
                    format: :json
                   }
    assert_response :unauthorized
    sign_out u
  end
end

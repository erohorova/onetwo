require 'test_helper'

class ApprovalsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    @faculty = create(:user)
    @user = create(:user)
    @group = create(:resource_group)
    @group.add_admin @faculty
    @group.add_member @user
    @post = create(:post, group: @group, user: @user)
    @comment = create(:comment, commentable: @post, user: @user)
    @question = create(:question, group: @group, user: @user)
    @answer = create(:answer, commentable: @question, user: @user)
  end

  test "approve a post" do
    sign_in @faculty
    assert_difference ->{Approval.count} do
      post :create, {:post_id => @post.id, :format => :json}
    end
    assert_response :created
    assert @post.has_approval?
    sign_out @faculty
  end

  test "vote for a question" do
    sign_in @faculty
    assert_difference ->{Approval.count} do
      post :create, {:post_id => @question.id, :format => :json}
    end
    assert_response :created
    assert @question.has_approval?
    sign_out @faculty
  end

  test "vote for an answer" do
    sign_in @faculty
    assert_difference ->{Approval.count} do
      post :create, {:comment_id => @answer.id, :format => :json}
    end
    assert_response :created
    assert @answer.has_approval?
    sign_out @faculty

  end

  test "vote for a comment" do
    sign_in @faculty
    assert_difference ->{Approval.count} do
      post :create, {:comment_id => @comment.id, :format => :json}
    end
    assert_response :created
    assert @comment.has_approval?
    sign_out @faculty
  end

  test "no duplicate approvals" do
    sign_in @faculty
    create(:approval, approvable: @post, approver: @faculty)
    assert_no_difference ->{Approval.count} do
      post :create, {:post_id => @post.id, :format => :json}
    end
    assert_response :unprocessable_entity
    sign_out @faculty
  end

  test "no student approval" do
    create_default_org
    sign_in @user
    assert_no_difference ->{Approval.count} do
      post :create, {:post_id => @post.id, :format => :json}
    end
    assert_response :unprocessable_entity
    sign_out @user
  end

  test "status forbidden if user not signed in" do
    assert_no_difference ->{Approval.count} do
      post :create, {:post_id => @post.id, :format => :json}
    end
    assert_response :unauthorized
  end

  test "disable scoring on anonymous post" do
    sign_in @faculty
    newpost = create(:post, user: @user, anonymous: true, group: @group)
    score = @user.score(@group)['score']
    post :create, {:post_id => newpost.id, :format => :json}
    @user.reload
    newscore = @user.score(@group)['score']
    assert_equal score, newscore
  end

  test "disapprove for a post" do
    sign_in @faculty
    @approve=create(:approval, approvable: @post, approver: @faculty)
    assert_difference ->{Approval.count},-1 do
      delete :destroy, {:post_id => @post.id, :format => :json}
    end
    assert_response :ok
    assert_not @post.has_approval?
    sign_out @faculty
  end

  test "disapprove forbidden if user not signed in" do
    @approve=create(:approval, approvable: @post, approver: @faculty)
    assert_no_difference ->{Approval.count} do
      delete :destroy, {:post_id => @post.id, :format => :json}
    end
    assert_response :unauthorized
    assert @post.has_approval?
  end
  test "disapproving a post by a student should send bad request" do
    sign_in @user
    @approve=create(:approval, approvable: @post, approver: @faculty)
    assert_no_difference ->{Approval.count} do
      delete :destroy, {:post_id => @post.id, :format => :json}
    end
    assert_response :bad_request
    sign_out @user
  end
  test "deleting a non existant approval should send bad request" do
    sign_in @faculty
    assert_no_difference ->{Approval.count} do
      delete :destroy, {:post_id => @post.id, :format => :json}
    end
    assert_response :bad_request
    sign_out @faculty
  end
end

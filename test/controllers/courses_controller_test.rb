require 'test_helper'

class CoursesControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    create_default_org
    @user = create(:user)
    @org = create(:organization, client: create(:client))
    @client = @org.client
  end

  test '#link out to savannah if there is an organization addon' do
    course = create(:resource_group, website_url: 'https://somewhere.com/course/id', organization: @org)
    assert course.organization_id
    @client.update(redirect_uri: 'https://somewhere.com/auth/edcast/callback')
    # organization = create(:organization, client: course.client)
    assert_equal true, Organization.exists?(client_id: course.client_id)

    get :link, id: course.id
    assert_equal true, redirect_to_url.start_with?('https://somewhere.com/course/id')

    @user.update_columns(organization_id: @org.id, organization_role: 'member')
    sign_in @user
    set_host(@org)

    get :link, id: course.id
    assert_equal true, redirect_to_url.start_with?('https://somewhere.com/auth/edcast/callback')

    get :link, id: course.id, auto_enroll: 'true'
    assert_equal true, redirect_to_url.start_with?('https://somewhere.com/auth/edcast/callback')
    uri = URI.parse(redirect_to_url)
    query_params = Rack::Utils.parse_nested_query(uri.query)
    assert_equal 'https://somewhere.com/course/id?auto_enroll=true', query_params["redirect_uri"]

    course.update(website_url: 'https://somewhere.com/course/id?key=value')
    get :link, id: course.id, auto_enroll: 'true'
    assert_equal true, redirect_to_url.start_with?('https://somewhere.com/auth/edcast/callback')
    uri = URI.parse(redirect_to_url)
    query_params = Rack::Utils.parse_nested_query(uri.query)
    assert_equal 'https://somewhere.com/course/id?key=value&auto_enroll=true', query_params["redirect_uri"]

    # Assuming user is authenticated by SAML
    @controller.user_session.merge!(authentication_method: 'saml')

    get :link, id: course.id, auto_enroll: 'true', external_uid: "emccustomertest1@gmail.com",
      raw_info: { 'Email' => ['emccustomertest1@gmail.com'], 'Family name' => ['Customer_test_1'], 'Given name' => ['EMC Education'], 'IdentityType' => ['C']}
    assert redirect_to_url.start_with?('https://somewhere.com/auth/edcast/callback')
    uri = URI.parse(redirect_to_url)
    query_params = Rack::Utils.parse_nested_query(uri.query)

    assert query_params['app_data']
    assert_equal query_params['app_data']['auth_provider'], 'saml'
  end

  #as per the client cleanup we should have one organization for each client
  #following test should be removed
  test 'should not link out to savannah because there is no organization addon' do
    skip 'deprecated'
    course = create(:resource_group, website_url: 'https://somewhere.com/course/id', organization: @org)
    assert course.organization_id
    assert_equal false, Organization.exists?(client_id: course.client_id)
    sign_in @user
    get :link, id: course.id
    assert_response :redirect
    assert_equal true, redirect_to_url.start_with?('https://somewhere.com/course/id')
  end

  class RedirectActionTest < ActionController::TestCase
    include Devise::Test::ControllerHelpers
    setup do
      skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
      @user   = create(:user)
      @org    = create(:organization, client: create(:client))
      @client = @org.client

      @course = create(:resource_group,
        website_url: 'https://somewhere.com/course/id',
        organization: @org )
      @client.update(redirect_uri: 'https://somewhere.com/auth/edcast/callback')

      set_host(@org)
      sign_in @user
    end

    test 'redirects to savannah' do
      get :redirect, course_id: @course.client_resource_id
      assert_equal true, redirect_to_url.start_with?('https://somewhere.com/auth/edcast/callback')
    end

    test 'respects `auto_enroll` from the current url parameter and send over to savannah' do
      get :redirect, course_id: @course.client_resource_id, auto_enroll: 'true'
      assert_equal true, redirect_to_url.start_with?('https://somewhere.com/auth/edcast/callback')

      uri = URI.parse(redirect_to_url)
      query_params = Rack::Utils.parse_nested_query(uri.query)
      assert_equal 'https://somewhere.com/course/id?auto_enroll=true', query_params["redirect_uri"]
    end

    test 'respects other default URL parameters and send over to savannah' do
      @course.update(website_url: 'https://somewhere.com/course/id?key=value')

      get :redirect, course_id: @course.client_resource_id, auto_enroll: 'true'
      assert_equal true, redirect_to_url.start_with?('https://somewhere.com/auth/edcast/callback')

      uri = URI.parse(redirect_to_url)
      query_params = Rack::Utils.parse_nested_query(uri.query)
      assert_equal 'https://somewhere.com/course/id?key=value&auto_enroll=true', query_params["redirect_uri"]
    end

    test 'respects authentication_method of the current user' do
      @controller.user_session.merge!(authentication_method: 'saml')

      get :redirect, course_id: @course.client_resource_id, auto_enroll: 'true', external_uid: "emccustomertest1@gmail.com",
        raw_info: {
          'Email' => ['emccustomertest1@gmail.com'],
          'Family name' => ['Customer_test_1'],
          'Given name' => ['EMC Education'],
          'IdentityType' => ['C'] }
      assert redirect_to_url.start_with?('https://somewhere.com/auth/edcast/callback')
      uri = URI.parse(redirect_to_url)
      query_params = Rack::Utils.parse_nested_query(uri.query)

      assert query_params['app_data']
      assert_equal query_params['app_data']['auth_provider'], 'saml'
    end
  end
end

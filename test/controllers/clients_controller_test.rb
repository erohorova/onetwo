require 'test_helper'

class ClientsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
  end

# -----------------------------------------------------------------------------
# COMMENTED OUT UNTIL WE FIX BROWSERIFY IN TEST ENVIRONMENT
# -----------------------------------------------------------------------------

=begin

  test 'should create client and render show' do
    admin = create(:user, email: 'b@course-master.com')
    sign_in admin

    assert_difference ->{Client.count} do
      post :create, :client => {:name => 'some client'}
    end

    assigned_client = assigns(:client)
    assert_equal assigned_client.name, 'some client'
    assert_template 'show'
  end

=end

  test 'client create api should create client' do
    create_default_org
    create(:user, email: 'admin@course-master.com', password: User.random_password,
           first_name: 'Admin', last_name: 'User', organization: Organization.default_org)
    assert_difference ->{Client.count} do
      post :create, :client => {:name => 'some app'},
        :token => Settings.savannah_token,
        :format => :json
    end

    assigned_client = assigns(:client)
    assert_not_nil assigned_client
    assert_not_nil assigns(:credential)
    assert_equal assigned_client.name, 'some app'

    credential = Client.find_by_name('some app').valid_credential
    resp = get_json_response response
    expected_response = {'client_name' => 'some app',
                         'owner_name' => 'Admin User',
                         'id' => assigned_client.id,
                         'api_key' => credential.api_key,
                         'shared_secret' => credential.shared_secret,
                         'credential_state' => credential.state,
                         'social_enabled' => true,
                         'created_at' => assigned_client.created_at.strftime('%d/%m/%Y')}
    assert_equal resp, expected_response
  end

  test 'client create API should associate org to new client' do
    create_default_org
    create(:user, email: 'admin@course-master.com', password: User.random_password,
           first_name: 'Admin', last_name: 'User', organization: Organization.default_org)
    ge = create(:organization, host_name: 'ge')
    assert_difference ->{Client.count} do
      post :create, :client => {:name => 'some app'},
        :organization => {:host_name => 'ge'},
        :token => Settings.savannah_token,
        :format => :json
    end

    assert_equal ge.reload.client_id, Client.last.id
  end

  test 'associate org to existing client' do
    create_default_org
    create(:user, email: 'admin@course-master.com', password: User.random_password,
           first_name: 'Admin', last_name: 'User', organization: Organization.default_org)
    cli = create(:client, name: 'ge', user: User.cm_admin)
    org = create(:organization, host_name: 'ge')
    assert_no_difference ->{Client.count} do
      post :create, :client => {:name => 'ge'},
        :organization => {:host_name => 'ge'},
        :token => Settings.savannah_token,
        :format => :json
    end
    assert_equal org.reload.client_id, cli.id
  end

  test 'set savannah_app_id and redirect_uri for existing org' do
    create_default_org
    create(:user, email: 'admin@course-master.com', password: User.random_password,
           first_name: 'Admin', last_name: 'User', organization: Organization.default_org)
    org1 = create(:organization, host_name: 'ge')
    post :create, :client => {:name => 'ge'},
        :organization => {:host_name => 'ge', savannah_app_id: 123, redirect_uri: "http://example.com/"},
        :token => Settings.savannah_token,
        :format => :json
    assert_equal org1.reload.savannah_app_id, 123
    assert_equal org1.reload.redirect_uri, "http://example.com/"
  end

  test 'set savannah_app_id and redirect_uri for new org from client' do
    create_default_org
    create(:user, email: 'admin@course-master.com', password: User.random_password,
           first_name: 'Admin', last_name: 'User', organization: Organization.default_org)
    post :create, :client => {:name => 'test'},
        :organization => {name: 'new org', description: 'description', :host_name => 'test', savannah_app_id: 12322, redirect_uri: "http://example.com/"},
        :token => Settings.savannah_token,
        :format => :json
    assert_equal Organization.last.savannah_app_id, 12322
    assert_equal Organization.last.redirect_uri, "http://example.com/"
  end

  test 'client create API should fail if missing params or wrong password' do
    create_default_org
    create(:user, email: 'admin@course-master.com', password: User.random_password,
           first_name: 'Admin', last_name: 'User', organization: Organization.default_org)
    assert_no_difference ->{Client.count} do
      post :create, :client => {:name => 'some app'},
        :format => :json
    end

    assert_no_difference ->{Client.count} do
      post :create, :client => {:name => 'some app'},
        :token => 'badtoken',
        :format => :json
    end
    assert_response :unauthorized

    assert_no_difference ->{Client.count} do
      post :create, :client => {:name => 'some app'},
        :user_email => 'admin@course-master.com',
        :password => User.random_password,
        :format => :json
    end
    assert_response :unauthorized

    assert_no_difference ->{Client.count} do
      post :create, :client => {key: 'value'},
        :token => Settings.savannah_token,
        :format => :json
    end
    assert_response :bad_request

  end

# -----------------------------------------------------------------------------
# COMMENTED OUT UNTIL WE FIX BROWSERIFY IN TEST ENVIRONMENT
# -----------------------------------------------------------------------------

=begin

  test 'should show client' do
    admin = create(:user, email: 'b@course-master.com')
    sign_in admin
    client = create(:client, user: admin)
    get :show, id: client.id

    assert_response :success
    assigned_client = assigns(:client)
    assert_equal assigned_client, client

    assert_template 'show'
    sign_out admin
  end

=end

  test 'should not show client belonging to a different user' do
    create_default_org
    admin = create(:user, email: 'b@course-master.com', organization: Organization.default_org)
    sign_in admin
    client = create(:client, organization: Organization.default_org)
    get :show, id: client.id
    assert_response :forbidden

    create_default_org
    create(:user, email: 'admin@course-master.com',
             password: User.random_password,
             first_name: 'Admin', last_name: 'User', organization: Organization.default_org)

    get :show, id: client.id,
      user_email: 'admin@course-master.com',
      password: User.random_password,
      format: :json

    assert_response :forbidden
  end

  test 'show api should work' do
    create_default_org
    savannah_user = create(:user, email: 'admin@course-master.com',
                           password: User.random_password,
                           first_name: 'Admin', last_name: 'User', organization: Organization.default_org)
    savannah_client = create(:client, user: savannah_user, name: 'some app')
    get :show, :id => savannah_client.id,
      :token => Settings.savannah_token,
      :format => :json

    assigned_client = assigns(:client)
    assert_not_nil assigned_client
    assert_not_nil assigns(:credential)
    assert_equal assigned_client, savannah_client

    credential = Client.find_by_name('some app').valid_credential
    resp = get_json_response response
    expected_response = {'client_name' => 'some app',
                         'owner_name' => 'Admin User',
                         'api_key' => credential.api_key,
                         'shared_secret' => credential.shared_secret,
                         'credential_state' => credential.state,
                         'id' => assigned_client.id,
                         'social_enabled' => true,
                         'created_at' => assigned_client.created_at.strftime('%d/%m/%Y')}
    assert_equal resp, expected_response
  end

# -----------------------------------------------------------------------------
# COMMENTED OUT UNTIL WE FIX BROWSERIFY IN TEST ENVIRONMENT
# -----------------------------------------------------------------------------

=begin

  test 'should render new form' do
    admin = create(:user, email: 'b@course-master.com')
    sign_in admin

    get :new
    assert_response :success
    assert_template 'new'
  end

=end

  test 'should list clients and keys' do
    create_default_org
    user = create(:user, organization: Organization.default_org)
    sign_in user
    owned_client = create(:client, user: user, organization: Organization.default_org)
    clients = create_list(:client, 3, organization: Organization.default_org)
    user.administered_clients << clients
    get :index,
      :token => Settings.savannah_token,
      :format => :json
    assigned_clients = assigns(:clients)
    assert_not_nil assigned_clients
    total_clients = clients
    total_clients << owned_client
    assert_same_elements assigned_clients, total_clients


  end

  test "should update social settings for client" do
    create_default_org
    user = create(:user, organization: Organization.default_org)
    sign_in user
    org = create(:organization)
    client = create(:client, user: user, organization: org)
    assert client.social_enabled
    put :update, id: client.id, client: {social_enabled: false}, format: :json
    client.reload
    assert_not client.organization.social_enabled
    assert_not client.social_enabled
    put :update, id: client.id, client: {social_enabled: true}, format: :json
    assert client.organization.reload.social_enabled
  end

  test "should create a new org" do
    create_default_org
    create(:user, email: 'admin@course-master.com', password: User.random_password,
           first_name: 'Admin', last_name: 'User', organization: Organization.default_org)

    assert_difference ['Organization.count', 'Client.count'] do
      assert_difference ->{User.count}, 2 do
      post :create, :client => {:name => 'some app'},
           :organization => {name: 'ge', description: 'description', :host_name => 'ge'},
           :token => Settings.savannah_token,
           :format => :json
      end
    end
    client = Client.last
    org = Organization.last
    user = User.last

    assert_equal 'some app', client.name
    assert_equal 'ge', org.name
    assert_equal 'ge', org.host_name
    assert_equal 'description', org.description
    assert org.is_admin? user
  end
end

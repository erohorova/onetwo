require 'test_helper'

class ApplicationControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  class DummiesControllerTest < ApplicationController
    def index
      head :ok
    end
  end

  test "should catch unknown routes" do
    create_default_org
    bad_route = 'really/bad/route'
    # THIS IS CAUSING INTERMITTENT FAILURE ON TRAVIS. COMMENTING OUT:
    # assert_recognizes(
    #   { controller: 'application', action: 'unknown_route', path: bad_route },
    #   bad_route
    # )
    get :unknown_route, path: bad_route
    assert_redirected_to not_found_path
  end

  test 'should set request store' do
    Rails.application.routes.draw do
      get 'show', controller: 'application_controller_test/dummies_controller_test', action: 'index'
    end

    @controller = DummiesControllerTest.new
    @controller.expects(:set_request_store).once

    org = create(:organization)
    user = create(:user, organization: org)
    set_host org

    sign_in user
    get :index

    Rails.application.reload_routes!
  end

  test '#set_request_store adds metadata to RequestStore' do
    Rails.application.routes.draw do
      get 'show', controller: 'application_controller_test/dummies_controller_test', action: 'index'
    end
    @controller = DummiesControllerTest.new
    org = create(:organization)
    user = create(:user, organization: org)
    set_host org
    sign_in user
    @request.headers["HTTP_X_VERSION_NUMBER"] = "foo"
    get :index
    Rails.application.reload_routes!
    RequestStore.unstub(:read)
    expected = {
      user_id: user.id,
      platform: "web",
      platform_version_number: "foo",
      is_admin_request: 0,
      user_agent: "Rails Testing"
    }
    actual = RequestStore.read(:request_metadata)
    assert_equal expected, actual
  end

  test '.is_admin_request?' do
    @request.headers['X-ADMIN'] = "true"
    assert_equal ApplicationController.is_admin_request?(@request), 1
    @request.headers['X-ADMIN'] = "false"
    assert_equal ApplicationController.is_admin_request?(@request), 0
    @request.headers['X-ADMIN'] = nil
    assert_equal ApplicationController.is_admin_request?(@request), 0
  end

  test '.is_admin_request? reads referer as fallback' do
    @request.headers['X-ADMIN'] = nil
    @request.stubs(:referer).returns "http://some.url/admin/something"
    assert_equal ApplicationController.is_admin_request?(@request), 1
  end

  test '#is_admin_request?' do
    [0, 1].each do |num|
      ApplicationController.
        expects(:is_admin_request?).
        with(@request).
        returns(num)
      assert_equal @controller.is_admin_request?, num
    end
  end

end

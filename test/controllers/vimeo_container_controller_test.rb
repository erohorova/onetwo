require 'test_helper'

class VimeoContainerControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  test 'should embed vimeo video' do
    stub_request(:get, "http://vimeo.com/api/oembed.json?url=http://vimeo.com/76979871").to_return(:status => 200, :body => {'html' => "<iframe src=\"//player.vimeo.com/76979871\" />"}.to_json)
    u = create(:user)
    sign_in u
    get :show, id: '76979871'
    assert_response :success
    assert_match(/src="\/\/player\.vimeo\.com\/76979871"/, response.body)
    sign_out u
  end

  test 'should render error message if embed api fails' do
    stub_request(:get, "http://vimeo.com/api/oembed.json?url=http://vimeo.com/76979871").to_return(:status => 404, :body => {}.to_json)
    u = create(:user)
    sign_in u
    get :show, id: '76979871'
    assert_response :success
    assert_match(/Sorry, Something went wrong/, response.body)
    sign_out u
  end

  test 'user should be signed in' do
    get :show, id: '76979871'
    assert_redirected_to new_user_session_path
  end

end

require 'test_helper'

class PasswordsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    stub_okta_group_creation
    stub_okta_app_creation
    stub_okta_group_app_link
  end

  test 'should reset password through mobile requests' do
    create_default_org
    @request.env["devise.mapping"] = Devise.mappings[:user]

    device_id = 'device123'
    request.cookies[:_d] = device_id
    email = 'bender@futurama.com'
    user = create(:user, email: email)
    mail_args = {}
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
        mail_args[:full_name] = message[:message][:to].first[:name]
        mail_args[:email] = message[:message][:to].first[:email]
    }.once

    post :create, user: {email: email, organization_id: user.organization_id}, format: :json
    assert user.full_name, mail_args[:full_name]
    assert user.email, mail_args[:email]
  end

  test 'should prevent hackers from guessing user whose email address if invalid by sending status code 200' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @request.env["HTTP_REFERER"] = 'http://test.com/forgot_password'
    email = "QWgqwejqwe@jwkehwke.com"
    org = create(:organization, is_open: true)
    set_host(org)
    MandrillMailer.any_instance.expects(:send_api_email).never
    post :create, user: { organization_id: org.id, email: email }, format: :json
    assert_response :success
  end

  test 'should prevent password reset by suspended user' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @request.env["HTTP_REFERER"] = 'http://test.com/forgot_password'
    email = generate(:email_address)
    org = create(:organization, is_open: true)
    user = create(:user, email: email, organization: org, is_suspended: true )
    set_host(org)
    MandrillMailer.any_instance.expects(:send_api_email).never
    post :create, user: { organization_id: org.id, email: email, commit: "Reset my password" }
    assert_redirected_to 'http://test.com/forgot_password'
    assert_equal "Invalid Request. Your account has been suspended. Please contact your team or edcast administrator for further details.", flash[:error]
  end

  test 'should allow password reset by non suspended user' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @request.env["HTTP_REFERER"] = 'http://test.com/forgot_password'
    email = generate(:email_address)
    org = create(:organization, is_open: true)
    user = create(:user, email: email, organization: org, is_suspended: false )
    set_host(org)
    MandrillMailer.any_instance.expects(:send_api_email).once
    post :create, user: { organization_id: org.id, email: email, commit: "Reset my password" }
    assert_nil flash[:notice]
    assert_redirected_to '/log_in?sent_reset_password_mail=true'
  end

  # update
  test 'should update user password #html' do
    @request.env["devise.mapping"] = Devise.mappings[:user]

    email = generate(:email_address)
    org = create(:organization, is_open: true)
    user = create(:user, email: email, organization: org, is_suspended: false )
    token = user.send :send_reset_password_instructions
    user.reload

    set_host(org)
    password = User.random_password
    put :update, {user: {reset_password_token: token, password: password, password_confirmation: password}}

    assert_redirected_to "/"
    assert_equal 'Your password was changed successfully. You are now signed in.', flash[:notice]
  end

  test 'should update user password #json' do
    create_default_org
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @request.headers['Accept'] = Mime::JSON
    @request.headers['Content-Type'] = Mime::JSON.to_s

    email = generate(:email_address)
    org = create(:organization, is_open: true)
    user = create(:user, email: email, organization: org, is_suspended: false )
    token = user.send :send_reset_password_instructions
    user.reload

    set_host(org)
    password = User.random_password
    put :update, {user: {reset_password_token: token, password: password, password_confirmation: password}}

    resp = get_json_response(response)
    token = resp['jwt_token']

    assert_not_empty token
    payload, header = JWT.decode(token, Settings.features.integrations.secret)
    assert_equal user.id, payload['user_id']
    assert_equal user.email, resp['email']
  end

  test 'should send short password error #json' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @request.headers['Accept'] = Mime::JSON
    @request.headers['Content-Type'] = Mime::JSON.to_s

    email = generate(:email_address)
    org = create(:organization, is_open: true)
    user = create(:user, email: email, organization: org, is_suspended: false )
    token = user.send :send_reset_password_instructions
    user.reload

    set_host(org)
    password = "1234aA@"
    put :update, {user: {reset_password_token: token, password: password, password_confirmation: password}}

    assert_equal "Password is too short (minimum is 8 characters), Password is invalid", get_json_response(response)['message']
  end

  test 'should allow to update password for valid token (okta)' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @request.headers['Accept'] = Mime::JSON
    @request.headers['Content-Type'] = Mime::JSON.to_s

    email = generate(:email_address)
    org = create(:organization, is_open: true)
    user = create(:user, email: email, organization: org)

    create :config, :boolean, name: 'app.config.okta.enabled', value: 'true', configable: org
    create :config, name: 'OktaApiToken', value: SecureRandom.hex, configable: org

    token = user.send :send_reset_password_instructions
    stub_okta_user_password_update

    set_host(org)
    password = "1234aA@a"
    put :update, {user: {reset_password_token: token, password: password, password_confirmation: password}}

    assert_response 200
  end

  test 'should not allow to update password for invalid token (okta)' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @request.headers['Accept'] = Mime::JSON
    @request.headers['Content-Type'] = Mime::JSON.to_s

    email = generate(:email_address)
    org = create(:organization, is_open: true)
    user = create(:user, email: email, organization: org)

    create :config, :boolean, name: 'app.config.okta.enabled', value: 'true', configable: org
    create :config, name: 'OktaApiToken', value: SecureRandom.hex, configable: org

    token = 'tamperedtoken'
    stub_okta_user_update

    set_host(org)
    password = "1234aA@a"
    put :update, {user: {reset_password_token: token, password: password, password_confirmation: password}}

    assert_equal "Reset password token is invalid", get_json_response(response)['message']
  end

  test 'update password okta check the response' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @request.headers['Accept'] = Mime::JSON
    @request.headers['Content-Type'] = Mime::JSON.to_s

    email = generate(:email_address)
    org = create(:organization, is_open: true)
    user = create(:user, email: email, organization: org)

    create :config, :boolean, name: 'app.config.okta.enabled', value: 'true', configable: org
    create :config, name: 'OktaApiToken', value: SecureRandom.hex, configable: org

    token = user.send :send_reset_password_instructions
    stub_okta_user_password_update

    set_host(org)
    password = "1234aA@a"
    put :update, {user: {reset_password_token: token, password: password, password_confirmation: password}}

    assert_response 200
    resp = get_json_response(response)

    assert_includes resp.keys, 'consumer_redirect_uri'
  end

  test 'should not allow to update password for invalid(reused) token (okta)' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @request.headers['Accept'] = Mime::JSON
    @request.headers['Content-Type'] = Mime::JSON.to_s

    email = generate(:email_address)
    org = create(:organization, is_open: true)
    user = create(:user, email: email, organization: org)

    create :config, :boolean, name: 'app.config.okta.enabled', value: 'true', configable: org
    create :config, name: 'OktaApiToken', value: SecureRandom.hex, configable: org

    token = user.send :send_reset_password_instructions
    stub_okta_user_password_update

    set_host(org)
    password = "1234aA@a"
    put :update, {user: {reset_password_token: token, password: password, password_confirmation: password}}

    assert_response 200

    password = "1234aA@a"
    put :update, {user: {reset_password_token: token, password: password, password_confirmation: password}}

    assert_equal "Reset password token is invalid", get_json_response(response)['message']
  end

  test 'should respond with 404 if invalid format resquested' do
    get :new, format: :ddwdw
    assert_response 404

    get :new, format: :bak
    assert_response 404

    get :new, format: :pqr
    assert_response 404

    get :new, format: :abc
    assert_response 404
  end

  test 'should not respond with 404 if valid format resquested' do
    get :new
    assert_response 302

  end
end

# frozen_string_literal: true

require 'test_helper'

class SessionsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers
  include OAuthStubs, OktaFederatedStubs

  setup do
    Organization.any_instance.unstub :default_sso

    @facebook_sso = Sso.find_by(name: 'facebook')
    @linkedin_sso = Sso.find_by(name: 'linkedin')
    @google_sso   = Sso.find_by(name: 'google')
    stub_okta_user_update

    stub_ip_lookup(ip_address: '0.0.0.0', response_body: { "time_zone"  => "Asia/Tokyo" })
  end

  test "failing SSO login does not trigger MetricsRecorderJob" do
    stub_facebook_oauth_request_to_fail
    create_default_org
    set_host Organization.default_org

    request.env["omniauth.auth"] = setup_fb_omniauth
    Analytics::MetricsRecorderJob.expects(:perform_later).never
    get :create, provider: :facebook
  end

  test "successful SSO login triggers MetricsRecorderJob" do
    create_default_ssos

    stub_facebook_oauth_request
    create_default_org

    request.env["omniauth.auth"] = setup_fb_omniauth
    stub_time = Time.now
    stub_view_runtime = 123.456
    stub_user = create :user, organization: Organization.default_org
    UserInfo::Aggregator.any_instance.stubs(:user).returns(stub_user)
    SessionsController.any_instance.stubs(:view_runtime).returns(stub_view_runtime)
    assert_empty stub_user.identity_providers
    org_uid = request.env["omniauth.auth"][:uid]
    payload = {
      controller: "SessionsController",
      action: "create",
      method: "GET",
      path: "/auth/facebook/callback",
      user_id: stub_user.id,
      platform: "web",
      request_ip: "0.0.0.0",
      request_host: "www.test.host",
      build_number: nil,
      platform_version_number: nil,
      user_agent: "Rails Testing",
      status: 200,
      org_id: stub_user.organization_id,
      view_runtime: stub_view_runtime,
      searchkick_runtime: 0,
      db_runtime: 0.0,
      referrer: nil,
      host: "www.test.host"
    }
    recorder_args = {
      recorder: "Analytics::UserLoginMetricsRecorder",
      metadata: payload,
      timestamp: stub_time.to_i,
      actor: Analytics::MetricsRecorder.user_attributes(stub_user).merge(
        "current_sign_in_at" => stub_time.to_i,
        "current_sign_in_ip" => "0.0.0.0",
        "last_sign_in_at" => stub_time.to_i,
        "updated_at" => stub_time.to_i,
        "remember_created_at" => stub_time.to_i,
        "sign_in_count" => 1,
        "last_sign_in_ip" => "0.0.0.0",
        "confirmed_at" => stub_time.to_i,
        "picture_url" => "http://graph.facebook.com/1234567/picture?type=square",
        "user_org_uid" => org_uid
      ),
      org: Analytics::MetricsRecorder.org_attributes(stub_user.organization),
    }
    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(:perform_later).with(recorder_args)
    Timecop.freeze(stub_time) do
      get :create, provider: :facebook
    end
    identity_providers = stub_user.identity_providers
    assert_equal identity_providers.count, 1
    assert_equal identity_providers.first.uid, org_uid
  end

  test 'should link up facebook auth' do
    stub_facebook_oauth_request
    request.env['omniauth.auth'] = setup_fb_omniauth
    create_default_org
    set_host Organization.default_org

    user = create(:user, first_name: nil, last_name: nil, picture_url: nil, organization: Organization.default_org)
    old_email = user.email
    assert old_email
    request.env['omniauth.params'] = { 'user_id' => CustomCipher.new.encrypt(context: user.id) }

    assert_difference -> {IdentityProvider.count} do
      assert_difference -> {Email.count} do
        get :create, provider: :facebook
        assert_template :create, 'do not redirect unless there is current_host'
      end
    end
    user.reload
    linked_identity = IdentityProvider.last

    assert_equal '1234567', linked_identity.uid
    assert_equal 'ABCDEF...', linked_identity.token
    assert_equal 'FacebookProvider', linked_identity.type
    assert_equal Time.at(1321747205), linked_identity.expires_at
    assert_equal user, linked_identity.user
    assert_equal 'Joe', user.first_name
    assert_equal 'Bloggs', user.last_name
    assert_equal old_email, user.email, 'Should not update the email'
    assert_equal true, Email.last.confirmed
    assert_equal 'joe@bloggs.com', Email.last.email
    assert_equal 'http://graph.facebook.com/1234567/picture?type=square', user.picture_url, 'should update the user image'
  end

  test 'should not allow social connection with impersonation - linking' do
    create_default_org
    impersonator = create(:user, organization: Organization.default_org)
    user = create(:user, first_name: nil, last_name: nil, picture_url: nil, organization: Organization.default_org)
    session['devise.impersonatee'] = user.name
    session['devise.impersonator'] = impersonator.id
    request.env['omniauth.params'] = {"user_id" => CustomCipher.new.encrypt(context: user.id) }

    assert_no_difference -> {IdentityProvider.count} do
      assert_no_difference -> {Email.count} do
        get :create, provider: :facebook
        assert_response :forbidden
      end
    end
  end

  test 'should not allow sso login because it is not enabled' do
    create_default_ssos

    create_default_org
    @controller = SessionsController.new

    @request.env['omniauth.auth'] = setup_fb_omniauth

    user = create(:user, first_name: nil, last_name: nil, picture_url: nil, organization: Organization.default_org)
    organization = create(:organization)
    facebook_sso = Sso.where(name: "facebook").first
    org_sso = organization.org_ssos.where(sso: facebook_sso).first!
    org_sso.update!(is_enabled: false)

    @request.env['omniauth.params'] = {
        'current_host' => ActiveSupport::MessageEncryptor.
            new(Edcast::Application.config.secret_key_base).
            encrypt_and_sign(organization.host)
    }

    assert_no_difference -> {IdentityProvider.count} do
      assert_no_difference -> {Email.count} do
        get :create, provider: :facebook
        assert_response :forbidden
      end
    end
  end

  test 'should not allow social connection with impersonation - authenticable' do
    create_default_org
    impersonator = create(:user, organization: Organization.default_org)
    user = create(:user, first_name: nil, last_name: nil, picture_url: nil, organization: Organization.default_org)
    session['devise.impersonatee'] = user.name
    session['devise.impersonator'] = impersonator.id

    assert_no_difference -> {IdentityProvider.count} do
      assert_no_difference -> {Email.count} do
        get :create, provider: :facebook
        assert_response :forbidden
      end
    end
  end

  test 'should hook up with linkedin' do
    create_default_org
    set_host Organization.default_org

    stub_linkedin_oauth_request

    request.env['omniauth.auth'] = setup_linkedin_omniauth

    user = create(:user, first_name: nil, last_name: nil, email: nil, picture_url: nil, organization: Organization.default_org)
    request.env['omniauth.params'] = {"user_id" => CustomCipher.new.encrypt(context: user.id) }

    assert_difference -> {IdentityProvider.count} do
      get :create, provider: :linkedin
    end
    linked_identity = IdentityProvider.last
    user.reload
    assert_equal '6mnOvjmlEW', linked_identity.uid
    assert_equal 'mytoken', linked_identity.token
    assert_equal 'LinkedinProvider', linked_identity.type
    assert_equal Time.at(1403573866), linked_identity.expires_at
    assert_equal user, linked_identity.user
    assert_equal Settings.linkedin.scope, linked_identity.token_scope

    assert_equal 'Trung', user.first_name
    assert_equal 'Pham', user.last_name
    assert_equal 'trung@phamcom.com', user.email, 'Should update the contact email'
    assert_equal 'http://m.c.lnkd.licdn.com/mpr/mprx/0_gPLYktnxYeqCrzUc01X3kA9hYulmpzUc0AA3krFxTW5YiluBAvztoKPlKGAlx-sRyKF8wBz9KizD', user.picture_url, 'should update the user image'
  end

  test 'should set close tab to true by default' do
    create_default_ssos
    stub_facebook_oauth_request
    create_default_org
    set_host Organization.default_org

    request.env['omniauth.auth'] = setup_fb_omniauth
    get :create, provider: :facebook
    assert assigns(:should_close_tab)
  end

  test 'should set close tab to false if sent so in params' do
    stub_facebook_oauth_request
    create_default_org
    set_host Organization.default_org

    request.env['omniauth.auth'] = setup_fb_omniauth
    request.env['omniauth.params'] = {'close_tab' => 'false'}
    get :create, provider: :facebook
    refute assigns(:should_close_tab)
  end

  test 'should create a new user from facebook' do
    create_default_ssos
    stub_facebook_oauth_request
    create_default_org
    set_host Organization.default_org

    request.env['omniauth.auth'] = setup_fb_omniauth

    assert_difference -> {IdentityProvider.count} do
      assert_difference -> {Email.count} do
        assert_difference -> {User.count} do
          get :create, provider: :facebook
        end
      end
    end
    identity = IdentityProvider.last
    user = User.last
    assert_equal '1234567', identity.uid
    assert_equal 'ABCDEF...', identity.token
    assert_equal 'FacebookProvider', identity.type
    assert_equal Time.at(1321747205), identity.expires_at
    assert_equal user, identity.user
    assert_nil identity.token_scope
    assert_equal 'Joe', user.first_name
    assert_equal 'Bloggs', user.last_name
    assert_equal true, Email.last.confirmed
    assert_equal 'joe@bloggs.com', Email.last.email
    assert_equal 'http://graph.facebook.com/1234567/picture?type=square', user.picture_url, 'should update the user image'
    assert_equal false, user.is_complete
    assert_nil user.federated_identifier
  end

  test 'should set time zone of user if not present' do
    create_default_ssos
    User.any_instance.unstub :set_time_zone
    SetUserTimeZoneJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    stub_facebook_oauth_request
    create_default_org

    request.env['omniauth.auth'] = setup_fb_omniauth

    assert_difference -> {UserProfile.count} do
      assert_difference -> {User.count} do
        get :create, provider: :facebook
      end
    end

    user = User.last
    assert_equal "Asia/Tokyo", user.profile.time_zone

    stub_ip_lookup(ip_address: "0.0.0.0", response_body: {"time_zone" => "America/Los_Angeles"})
    assert_no_difference -> {UserProfile.count} do
      assert_no_difference -> {User.count} do
        get :create, provider: :facebook
      end
    end

    # doesn't change time zone if present
    assert_equal "Asia/Tokyo", user.profile.time_zone
  end

  test 'should set terms accepted status and date of user if not present' do
    create_default_ssos

    stub_facebook_oauth_request
    create_default_org

    request.env['omniauth.auth'] = setup_fb_omniauth
    request.env['omniauth.params'] = { terms_accepted: true }

    assert_difference -> {UserProfile.count} do
      assert_difference -> {User.count} do
        get :create, provider: :facebook
      end
    end

    user = User.last
    assert_equal true, user.profile.tac_accepted
    assert_equal Date.today, user.profile.tac_accepted_at.to_date

    assert_no_difference -> {UserProfile.count} do
      assert_no_difference -> {User.count} do
        get :create, provider: :facebook
      end
    end
  end

  test 'should create a new user from google' do
    create_default_ssos
    request.env['omniauth.auth'] = setup_google_omniauth
    create_default_org
    set_host Organization.default_org

    assert_difference -> {IdentityProvider.count} do
      assert_difference -> {Email.count} do
        assert_difference -> {User.count} do
          get :create, provider: :google
        end
      end
    end
    identity = IdentityProvider.last
    user = User.last
    assert_equal '123456789', identity.uid
    assert_equal 'token', identity.token
    assert_equal 'GoogleProvider', identity.type
    assert_equal Time.at(1354920555), identity.expires_at
    assert_equal user, identity.user
    assert_equal Settings.google.scope, identity.token_scope
    assert_equal 'John', user.first_name
    assert_equal 'Doe', user.last_name
    assert_equal true, Email.last.confirmed
    assert_equal 'john@companyname.com', Email.last.email
    assert_equal 'https://lh3.googleusercontent.com/url/photo.jpg', user.picture_url, 'should update the user image'
    assert_nil user.federated_identifier
  end

  test 'should create a new user from google with inactive state' do
    create_default_ssos

    pending_org = create(:organization, status: 'pending')
    stub_request(:post, %r{http://localhost:9200/_bulk})

    request.env['omniauth.params'] = {
      'current_host' => ActiveSupport::MessageEncryptor.
        new(Edcast::Application.config.secret_key_base).
        encrypt_and_sign("#{pending_org.host_name}.test.host")
    }
    request.env['omniauth.auth'] = {
        :provider => "google",
        :uid => "123456789",
        :info => {
            :name => "John Doe",
            :email => "john_doe@companyname.com",
            :first_name => "John",
            :last_name => "Doe",
            :image => "https://lh3.googleusercontent.com/url/photo.jpg"
        },
        :credentials => {
            :token => "token",
            :refresh_token => "another_token",
            :expires_at => 1354920555,
            :expires => true
        },
        :extra => {
            :raw_info => {
                :sub => "123456789",
                :email => "user@domain.example.com",
                :email_verified => true,
                :name => "John Doe",
                :given_name => "John",
                :family_name => "Doe",
                :profile => "https://plus.google.com/123456789",
                :picture => "https://lh3.googleusercontent.com/url/photo.jpg",
                :gender => "male",
                :birthday => "0000-06-25",
                :locale => "en",
                :hd => "company_name.com"
            }
        }
    }

    assert_no_difference -> {IdentityProvider.count} do
      assert_difference -> {User.count} do
        get :create, provider: :google
      end
    end
    user = User.last
    refute user.is_active
  end

  test 'should reuse an existing facebook account and convert it into an authenticable account' do
    create_default_ssos
    stub_facebook_oauth_request
    create_default_org
    set_host Organization.default_org

    request.env['omniauth.auth'] = setup_fb_omniauth
    user = create(:user, email: 'joe@bloggs.com', first_name: nil, last_name: nil, picture_url: nil,
      created_at: 5.days.ago, is_complete: true, organization: Organization.default_org)
    facebook_identity = create(:identity_provider, :facebook, user: user, uid: '1234567')
    assert_nil facebook_identity.auth
    assert_no_difference -> {IdentityProvider.count} do
      assert_difference -> {Email.count} do
        assert_no_difference -> {User.count} do
          get :create, provider: :facebook
        end
      end
    end
    user.reload
    identity = IdentityProvider.last
    assert_equal facebook_identity.id, identity.id

    assert_equal '1234567', identity.uid
    assert_equal 'ABCDEF...', identity.token
    assert_equal 'FacebookProvider', identity.type
    assert_equal Time.at(1321747205), identity.expires_at
    assert_equal user, identity.user
    assert_equal 'Joe', user.first_name
    assert_equal 'Bloggs', user.last_name
    assert_equal true, Email.last.confirmed
    assert_equal 'joe@bloggs.com', Email.last.email
    assert_equal 'http://graph.facebook.com/1234567/picture?type=square', user.picture_url, 'should update the user image'
  end

  test 'should not use non-verified external account' do
    stub_facebook_oauth_request
    create_default_org
    set_host Organization.default_org

    fb_omniauth = setup_fb_omniauth
    fb_omniauth[:info].merge!(verified: false)
    request.env['omniauth.auth'] = fb_omniauth

    user = create(:user, email: 'joe@bloggs.com', first_name: nil, last_name: nil, picture_url: nil, organization: Organization.default_org)
    facebook_identity = create(:identity_provider, :facebook, user: user, uid: '1234567')
    assert_nil facebook_identity.auth
    assert_no_difference -> {IdentityProvider.count} do
      assert_no_difference -> {Email.count} do
        assert_no_difference -> {User.count} do
          get :create, provider: :facebook
        end
      end
    end
  end

  test 'should attach external provider to a matching email address' do
    create_default_ssos
    stub_facebook_oauth_request
    create_default_org
    set_host Organization.default_org

    request.env['omniauth.auth'] = setup_fb_omniauth

    user = create(:user, email: 'joe@bloggs.com', first_name: nil, last_name: nil, picture_url: nil, organization: Organization.default_org)
    assert_difference -> {IdentityProvider.count} do
      assert_difference -> {Email.count} do
        assert_no_difference -> {User.count} do
          get :create, provider: :facebook
        end
      end
    end

    assert_equal user, FacebookProvider.last.user
    assert_equal true, FacebookProvider.last.auth
    assert_equal true, user.reload.is_complete
  end

  class OrgSocialAuth < ActionController::TestCase
    include OAuthStubs
    setup do
      Config.any_instance.stubs :create_okta_group_config
      stub_ip_lookup(ip_address: '0.0.0.0')
      stub_facebook_oauth_request
      request.env['omniauth.auth'] = setup_fb_omniauth
    end

    test "should create new user in default org context" do
      create_default_ssos
      Organization.any_instance.unstub :default_sso

      create_default_org
      request.env['omniauth.params'] = {
        'current_host' => CustomCipher.new.encrypt(context: 'abc.test.host')
      }

      assert_difference -> {IdentityProvider.count} do
        assert_difference -> {User.count} do
          get :create, provider: :facebook
        end
      end

      u = User.last
      assert_equal u.email, 'joe@bloggs.com'
      assert_equal u.organization_id, Organization.default_org.id
    end

    test "should create new user in org context" do
      create_default_org
      create_default_ssos
      Organization.any_instance.unstub :default_sso

      u = create :user, email: 'joe@bloggs.com', organization_id: Organization.default_org.id

      o = create :organization, host_name: 'abc', is_open: true
      request.env['omniauth.params'] = {
        'current_host' => CustomCipher.new.encrypt(context: "#{o.host_name}.test.host")
      }

      assert_difference -> {IdentityProvider.count} do
        assert_difference -> {User.count} do
          get :create, provider: :facebook
        end
      end

      u = User.last
      assert_equal u.email, 'joe@bloggs.com'
      assert_equal u.organization_id, o.id

      # same user signing in now. Shouldn't create a new user

      assert_no_difference -> {IdentityProvider.count} do
        assert_no_difference -> {User.count} do
          get :create, provider: :facebook
        end
      end
      assert_equal u.id, assigns(:current_user).id
    end

    test 'should update sign_out_at to nil on sign in' do
      # Assuming the SSO is facebook. Same will be applied to other SSOs
      create_default_org
      create_default_ssos
      Organization.any_instance.unstub :default_sso

      user = create :user, email: 'joe@bloggs.com', organization_id: Organization.default_org.id, sign_out_at: Time.now
      organization = create :organization, host_name: 'abc', is_open: true
      request.env['omniauth.params'] = {
        'current_host' => CustomCipher.new.encrypt(context: "#{organization.host_name}.test.host")
      }
      get :create, provider: :facebook

      recent_user = User.last
      assert_equal recent_user.email, user.email
      assert_nil recent_user.sign_out_at
    end

    test 'should handle redirect_uri parameter' do
      create_default_ssos
      Organization.any_instance.unstub :default_sso
      organization = create(:organization, is_open: true)

      client = create(:client, organization: organization, redirect_uri: 'https://www.example.com/auth/edcast/callback')
      # set_host organization

      request.env['omniauth.params'] = {
        'redirect_uri' => 'https://www.example.com/auth/edcast/callback?next=/go/here/when/done/',
        'current_host' => CustomCipher.new.encrypt(context: "#{organization.host_name}.test.host")
      }
      assert_difference -> {IdentityProvider.count} do
        assert_difference -> {Email.count} do
          assert_difference -> {User.count} do
            get :create, provider: :facebook
          end
        end
      end
      assert_response :redirect
      assert_match "#{organization.host_name}.test.host", response.redirect_url
      assert_match "user_id=", response.redirect_url
      assert_match "authenticable=true", response.redirect_url
      assert_match({redirect_uri: 'https://www.example.com/auth/edcast/callback?next=/go/here/when/done/'}.to_param, response.redirect_url)
    end

    test 'should redirect back to savannah' do
      client = create(:client, redirect_uri: 'https://www.example.com/auth/edcast/callback')
      organization = create(:organization, client: client)
      user = create(:user, organization_id: organization.id, organization_role: 'member')
      get :finish, user_id: CustomCipher.new.encrypt(context: "#{user.id}"), provider: 'facebook',
        redirect_uri: "https://www.example.com/auth/edcast/callback?next=#{CGI.escape('/go/here/when/done/')}"

      assert_template :create
      assert_equal user, @controller.current_user
      assert_match %r{https://www.example.com/auth/edcast/callback\?code=.*&next=#{CGI.escape('/go/here/when/done/')}}, assigns[:target_url]
    end

    test "should sign in correct user based on org context" do
      create_default_ssos
      Organization.any_instance.unstub :default_sso
      create_default_org

      o = create :organization, host_name: 'abc'

      create :user, email: 'joe@bloggs.com', organization_id: Organization.default_org.id
      u2 = create :user, email: 'joe@bloggs.com', organization_id: o.id

      request.env['omniauth.params'] = {
        'current_host' => CustomCipher.new.encrypt(context: "#{o.host_name}.test.host")
      }

      assert_difference -> {IdentityProvider.count} do
        get :create, provider: :facebook
      end
      u = assigns(:current_user)

      assert_equal u.email, 'joe@bloggs.com'
      assert_equal u.id, u2.id
      assert_equal u.organization_id, o.id
    end

    test "should create two users for two sign ups in diff organization" do
      create_default_ssos
      Organization.any_instance.unstub :default_sso

      o1 = create :organization, host_name: 'abc', is_open: true
      o2 = create :organization, host_name: 'abcd', is_open: true

      create :user, email: 'joe@bloggs.com', organization_id: o1.id

      request.env['omniauth.params'] = {
        'current_host' => CustomCipher.new.encrypt(context: "#{o1.host_name}.test.host"),
        'cb' => 'cbFunc'
      }

      assert_difference -> {IdentityProvider.count} do
        get :create, provider: :facebook
        assert_response :redirect
        assert_match "#{o1.host_name}.test.host", response.redirect_url
        assert_match "cb=cbFunc", response.redirect_url
        assert_match "user_id=", response.redirect_url
        assert_match "authenticable=true", response.redirect_url
      end
      u = assigns(:current_user)
      assert_equal u.organization_id, o1.id

      request.env['omniauth.params'] = {
        'current_host' => CustomCipher.new.encrypt(context: "#{o2.host_name}.test.host")
      }

      assert_difference -> {IdentityProvider.count} do
        assert_difference -> { User.count } do
          get :create, provider: :facebook
        end
      end
      u = assigns(:current_user)
      assert_equal u.organization_id, o2.id

      # now the second user tries to sign in, no new user created. sign in the exising user
      assert_no_difference -> {IdentityProvider.count} do
        assert_no_difference -> {User.count} do
          get :create, provider: :facebook
        end
      end
      assert_equal u.id, assigns(:current_user).id

      # try for the first user too
      request.env['omniauth.params'] = {
        'current_host' => CustomCipher.new.encrypt(context: "#{o1.host_name}.test.host")
      }

      assert_no_difference -> {IdentityProvider.count} do
        assert_no_difference -> {User.count} do
          get :create, provider: :facebook
        end
      end
      assert_equal o1.id, assigns(:current_user).organization_id
    end
  end

  test 'should render the finish action' do
    org = create :organization, host_name: 'abcd'

    user = create :user, email: 'joe@bloggs.com', organization_id: org.id

    get :finish, user_id: CustomCipher.new.encrypt(context: "#{user.id}"), provider: 'facebook',
        cb: 'cbFunc'

    assert_template :create
    assert_equal user, @controller.current_user
  end

  test 'should render finish action with correct value of should close tab' do
    org = create :organization, host_name: 'abcd'

    user = create :user, email: 'joe@bloggs.com', organization_id: org.id

    get :finish, user_id: ActiveSupport::MessageEncryptor.
                          new(Edcast::Application.config.secret_key_base).
                          encrypt_and_sign("#{user.id}"), provider: 'facebook',
        cb: 'cbFunc', delete_reserved_handle_cookie: 'true'

    assert_template :create
    assert_equal user, @controller.current_user
    assert assigns(:should_close_tab)

    get :finish, user_id: ActiveSupport::MessageEncryptor.
                          new(Edcast::Application.config.secret_key_base).
                          encrypt_and_sign("#{user.id}"), provider: 'facebook',
        cb: 'cbFunc', close_tab: 'false', delete_reserved_handle_cookie: 'true'

    assert_template :create
    assert_equal user, @controller.current_user
    refute assigns(:should_close_tab)
  end

  test 'should not allow new user registration even with blank email if org is closed for registration' do
    org = create(:organization, is_open: false)
    create(:config, name: 'AllowedEmailDomains', value: 'none', configable: org)

    create(:user, email: nil, organization: org)
    request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: org.host)
    }
    request.env['omniauth.auth'] = setup_fb_omniauth
    request.env['omniauth.auth'][:info][:email] = nil

    assert_no_difference -> {IdentityProvider.count} do
      assert_no_difference -> {Email.count} do
        assert_no_difference ->{User.count} do
          get :create, provider: :facebook
        end
      end
    end
    assert_includes response.body, 'error_code=invite_only'
  end

  test 'existing users should be able to sign in even after domains get whitelisted' do
    create_default_ssos
    org = create(:organization, is_open: false)
    user = create(:user, email: nil, organization: org)

    create(:identity_provider, :facebook, uid: 1234567, user: user, auth: true)
    create(:config, name: 'AllowedEmailDomains', value: 'abc.com,example.com', configable: org)

    request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: org.host)
    }
    request.env['omniauth.auth'] = setup_fb_omniauth
    request.env['omniauth.auth'][:info][:email] = nil

    assert_no_difference -> {IdentityProvider.count} do
      assert_no_difference ->{User.count} do
        get :create, provider: :facebook
      end
    end

    assert_response :redirect
  end

  test 'should allow new user registration even with blank email for open organization' do
    create_default_ssos

    org = create(:organization, is_open: false)
    create(:config, name: 'AllowedEmailDomains', value: 'all', configable: org)

    request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: org.host)
    }
    request.env['omniauth.auth'] = setup_fb_omniauth
    request.env['omniauth.auth'][:info][:email] = nil

    assert_difference -> {IdentityProvider.count} do
      assert_difference ->{User.count} do
        get :create, provider: :facebook
      end
    end
    assert_response :redirect
  end

  test 'should allow new user registration for every email domain in org' do
    create_default_ssos
    org = create(:organization, is_open: false)
    create(:config, name: 'AllowedEmailDomains', value: 'all', configable: org)

    request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: org.host)
    }
    request.env['omniauth.auth'] = setup_fb_omniauth
    request.env['omniauth.auth'][:info][:email] = "test@example.com"

    assert_difference -> {IdentityProvider.count} do
      assert_difference -> {Email.count} do
        assert_difference ->{User.count} do
          get :create, provider: :facebook
        end
      end
    end
    assert_response :redirect
  end

  test 'should allow new user registration for certain whitelisted email domains in org' do
    create_default_ssos

    org = create(:organization, is_open: false)
    create(:config, name: 'AllowedEmailDomains', value: 'example.com,abc.com', configable: org)

    request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: org.host)
    }
    request.env['omniauth.auth'] = setup_fb_omniauth
    request.env['omniauth.auth'][:info][:email] = "test@xyz.com"

    assert_no_difference -> {IdentityProvider.count} do
      assert_no_difference -> {Email.count} do
        assert_no_difference ->{User.count} do
          get :create, provider: :facebook
        end
      end
    end

    assert_includes response.body, 'error_code=invite_only'

    request.env['omniauth.auth'][:info][:email] = "test@abc.com"
    assert_difference -> {IdentityProvider.count} do
      assert_difference -> {Email.count} do
        assert_difference ->{User.count} do
          get :create, provider: :facebook
        end
      end
    end

    assert_response :redirect
  end

  test 'should allow new user registration for invited user in closed org' do
    create_default_ssos
    org = create(:organization, is_open: false)
    create(:config, name: 'AllowedEmailDomains', value: 'none', configable: org)

    invitation = create(:invitation, invitable: org, recipient_email: setup_fb_omniauth[:info][:email], roles: 'member')

    request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: org.host)
    }
    request.env['omniauth.auth'] = setup_fb_omniauth

    assert_difference -> {IdentityProvider.count} do
      assert_difference -> {Email.count} do
        assert_difference ->{User.count} do
          get :create, provider: :facebook
        end
      end
    end
  end

  test 'should allow new user registration for invited user even if org has whitelisted domains' do
    create_default_ssos
    org = create(:organization)
    create(:config, name: 'AllowedEmailDomains', value: 'abc.com,example.com', configable: org)

    invitation = create(:invitation, invitable: org, recipient_email: setup_fb_omniauth[:info][:email], roles: 'member')

    request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: org.host)
    }
    request.env['omniauth.auth'] = setup_fb_omniauth

    assert_difference -> {IdentityProvider.count} do
      assert_difference -> {Email.count} do
        assert_difference ->{User.count} do
          get :create, provider: :facebook
        end
      end
    end
  end

  test 'should redirect to native scheme path from finish if native scheme param present' do
    org = create :organization, host_name: 'abcd'

    user = create :user, email: 'joe@bloggs.com', organization_id: org.id

    native_scheme = 'edcastapp://cookiecutter/'
    get :finish, user_id: CustomCipher.new.encrypt(context: "#{user.id}"), provider: 'facebook', native_scheme: native_scheme

    assert_redirected_to native_scheme_redirect_path(native_scheme: native_scheme)
  end

  test 'should redirect to native scheme even if we dont redirect to finish' do
    create_default_ssos

    stub_facebook_oauth_request
    request.env['omniauth.auth'] = setup_fb_omniauth
    org = create(:organization)
    native_scheme = 'edcastapp://cookiecutter/'
    request.env['omniauth.params'] = {
      'native_scheme' => native_scheme,
      'current_host' => CustomCipher.new.encrypt(context: "#{org.host_name}.test.host")}

    set_host org
    get :create, provider: :facebook
    assert_redirected_to native_scheme_redirect_path(native_scheme: native_scheme)
  end

  test 'should not allow social account user to sign up for a new account if the org is not open' do
    org = create(:organization, is_open: false)
    create(:config, name: 'AllowedEmailDomains', value: 'none', configable: org)

    request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: org.host)
    }
    request.env['omniauth.auth'] = setup_fb_omniauth

    assert_no_difference -> {IdentityProvider.count} do
      assert_no_difference -> {Email.count} do
        assert_no_difference ->{User.count} do
          get :create, provider: :facebook
        end
      end
    end
    assert_includes response.body, 'error_code=invite_only'
  end

  test 'should redirect to origin url if organization is not open and widget enabled' do
    org = create(:organization, is_open: false)
    create(:config, name: 'AllowedEmailDomains', value: 'none', configable: org)
    url = 'http://widget.app.com'
    request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: org.host),
      'origin' => url,
      'widget' => 'true'
    }
    request.env['omniauth.auth'] = setup_google_omniauth

    assert_no_difference -> {IdentityProvider.count} do
      assert_no_difference -> {Email.count} do
        assert_no_difference ->{User.count} do
          get :create, provider: :google
        end
      end
    end
    assert_response :success
    url += '?' + { error: 'The user is not registered' }.to_param
    assert_includes response.body, url
  end

  test 'should redirect to origin url if user is not registered in organization and widget enabled' do
    org = create(:organization)
    create(:config, name: 'AllowedEmailDomains', value: 'none', configable: org)
    url = 'http://widget.app.com'
    request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: org.host),
      'origin' => url,
      'widget' => 'true'
    }
    request.env['omniauth.auth'] = setup_google_omniauth

    assert_no_difference -> {IdentityProvider.count} do
      assert_no_difference -> {Email.count} do
        assert_no_difference ->{User.count} do
          get :create, provider: :google
        end
      end
    end
    assert_response :success
    url += '?' + { error: 'The user is not registered' }.to_param
    assert_includes response.body, url
  end

  test 'iteration should not break if no raw_info present' do
    create_default_ssos

    org = create :organization
    request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: org.host)
    }
    fb_omniauth = setup_fb_omniauth
    fb_omniauth.merge!(extra: {})
    request.env['omniauth.auth'] = fb_omniauth

    assert_nothing_raised NoMethodError do
      get :create, provider: :facebook
      assert User.find_by(email: 'joe@bloggs.com', organization_id: org.id)
    end
  end

  test 'setup custom oauth (org_oauth) for an organization' do
    org = create :organization
    org_oauth = create(:sso, name: 'org_oauth')
    org.ssos << org_oauth
    @request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: org.host)
    }

    request.env['omniauth.auth'] = setup_org_oauth_omniauth

    get :create, provider: :org_oauth
    user     = User.find_by(email: 'joe@bloggs.com', organization_id: org.id)
    provider = user.identity_providers.first

    assert user
    assert provider
    assert_match /ABCDEF.../, provider.token
    assert provider.expires_at
    assert_nil provider.secret
  end

  test 'setup custom lxp_oauth for an organization' do
    org = create :organization
    org_oauth = create(:sso, name: 'lxp_oauth')
    org.ssos << org_oauth
    org_sso = create :org_sso, sso: org_oauth, organization: org
    @request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: org.host),
      'connector' => JWT.encode(org_sso.id.to_s, Settings.features.integrations.secret, 'HS256'),
      'provider' => 'google'
    }

    request.env['omniauth.auth'] = setup_lxp_oauth_omniauth

    get :create, provider: :lxp_oauth

    user     = User.find_by(email: 'okta-user@gmail.com', organization_id: org.id)
    provider = user.identity_providers.first

    assert user
    assert provider
    assert_match /00ud29p6s62vb5NWq0h7/, provider.uid
    assert_match /bvZVOpjjakh6b57aGDX4lG-rxI4y4ztPLRUqVyDl4TI/, provider.refresh_token
    assert_equal '1589184311142599-okta_user@gmail.com', user.federated_identifier
  end

  test 'setup salesforce for an organization' do
    org = create :organization
    org_oauth = create(:sso, name: 'salesforce')
    org.ssos << org_oauth

    @request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: org.host)
    }

    request.env['omniauth.auth'] = setup_salesforce_omniauth

    get :create, provider: :org_oauth
    user     = User.find_by(email: 'trung@phamcom.com', organization_id: org.id)
    assert SalesforceProvider.find_by(user: user)
    assert_nil user.federated_identifier
  end

  test 'setup salesforceusers for an organization' do
    org = create :organization
    org_oauth = create(:sso, name: 'salesforceusers')
    org.ssos << org_oauth


    @request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: org.host)
    }

    request.env['omniauth.auth'] = setup_salesforce_omniauth.merge!(provider: 'salesforceusers')

    get :create, provider: :org_oauth
    user     = User.find_by(email: 'trung@phamcom.com', organization_id: org.id)
    provider = SalesforceuserProvider.find_by(user: user)

    assert user
    assert provider
    assert_match /faketoken/, provider.token
    refute provider.expires_at
    assert_nil provider.secret
    assert_nil user.federated_identifier
  end

  test 'setup salesforcedevelopers for an organization' do
    org = create :organization
    org_oauth = create(:sso, name: 'salesforcedevelopers')
    org.ssos << org_oauth


    @request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: org.host)
    }

    request.env['omniauth.auth'] = setup_salesforce_omniauth.merge!(provider: 'salesforcedevelopers')

    get :create, provider: :org_oauth
    user     = User.find_by(email: 'trung@phamcom.com', organization_id: org.id)
    assert SalesforcedeveloperProvider.find_by(user: user)
    assert_nil user.federated_identifier
  end

  test 'setup salesforcepartners for an organization' do
    org = create :organization
    org_oauth = create(:sso, name: 'salesforcepartners')
    org.ssos << org_oauth


    @request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: org.host)
    }

    request.env['omniauth.auth'] = setup_salesforce_omniauth.merge!(provider: 'salesforcepartners')

    get :create, provider: :org_oauth
    user     = User.find_by(email: 'trung@phamcom.com', organization_id: org.id)
    assert SalesforcepartnerProvider.find_by(user: user)
    assert_nil user.federated_identifier
  end

  test 'setup office365 for an organization' do
    org = create :organization
    org_oauth = create(:sso, name: 'office365')
    org.ssos << org_oauth


    @request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: org.host)
    }

    request.env['omniauth.auth'] = setup_salesforce_omniauth.merge!(provider: 'office365')

    get :create, provider: :org_oauth
    user     = User.find_by(email: 'trung@phamcom.com', organization_id: org.id)
    assert Office365Provider.find_by(user: user)
    assert_nil user.federated_identifier
  end

  test 'should invoke request recorder #sso login' do
    create_default_ssos
    create_default_org
    Analytics::MetricsRecorderJob.unstub(:perform_later)
    request.env['omniauth.auth'] = setup_google_omniauth
    # event -> user_created, user_edited, user_logged_in
    Analytics::MetricsRecorderJob.expects(:perform_later).times(3)

    assert_difference -> {IdentityProvider.count} do
      assert_difference -> {Email.count} do
        assert_difference -> {User.count} do
          get :create, provider: :google
        end
      end
    end

    identity = IdentityProvider.last
    assert_equal '123456789', identity.uid
  end

  test 'redirects to FEDERATED server to create session when lxp_oauth and provisioning is enabled for a provider' do
    Config.any_instance.stubs :create_okta_group_config
    @organization = create(:organization)

    stub_okta_user

    create :config, :boolean, configable: @organization, name: 'app.config.okta.enabled', value: 'true'
    create :config, configable: @organization, name: 'OktaApiToken', value: 'api-token'
    user = create(:user, organization_id: @organization.id, organization_role: 'member')
    get :finish, user_id: CustomCipher.new.encrypt(context: "#{user.id}"), provider: 'lxp_oauth',
      redirect_uri: "https://www.example.com/auth/edcast/callback?next=#{CGI.escape('/go/here/when/done/')}",
      provider_type: 'salesforce', app_data: { external_uid: 'abc123' }

    assert_template :create
    assert_equal user, @controller.current_user
    assert_match %r{https://dev-579575.oktapreview.com/login/sessionCookieRedirect}, assigns[:target_url]
    assert_nil user.federated_identifier
  end

  test 'federated url must retain the next redirect_uri' do
    Config.any_instance.stubs :create_okta_group_config
    client = create(:client, redirect_uri: 'https://www.example.com/auth/edcast/callback')
    @organization = create(:organization, client: client)

    stub_okta_user

    create :config, :boolean, configable: @organization, name: 'app.config.okta.enabled', value: 'true'
    create :config, configable: @organization, name: 'OktaApiToken', value: 'api-token'
    user = create(:user, organization_id: @organization.id, organization_role: 'member')
    get :finish, user_id: CustomCipher.new.encrypt(context: "#{user.id}"), provider: 'lxp_oauth',
      redirect_uri: "https://www.example.com/auth/edcast/callback?next=#{CGI.escape('/go/here/when/done/')}",
      provider_type: 'salesforce', app_data: { external_uid: 'abc123' }

    assert_template :create
    assert_equal user, @controller.current_user
    assert_match %r{https://dev-579575.oktapreview.com/login/sessionCookieRedirect}, assigns[:target_url]
    redirect_url = CGI.parse(URI.parse(assigns[:target_url]).query)['redirectUrl']
    assert_match %r{https://www.example.com/auth/edcast/callback}, redirect_url[0]
    assert_equal '2-okta@edcast.com', @controller.current_user.federated_identifier
  end

  test 'should  redirect_uri if origin is present in omniauth parameter' do
    create_default_ssos
    Organization.any_instance.unstub :default_sso
    organization = create(:organization, is_open: true)
    request.env['omniauth.auth'] = setup_google_omniauth

    request.env['omniauth.params'] = {
      'origin' => 'http://qa.lvh.me:4001/insights/testsett',
      'current_host' => CustomCipher.new.encrypt(context: "#{organization.host_name}.test.host")
    }
    get :create, provider: :google

    assert_match({redirect_uri: 'http://qa.lvh.me:4001/insights/testsett'}.to_param, response.redirect_url)
    assert_no_match(/jwtToken|firstname|lastname/, response.redirect_url)
  end

  test 'should redirect_uri with jwtToken, firstname and lastname user if origin is present and widget value true in omniauth parameter' do
    create_default_ssos
    Organization.any_instance.unstub :default_sso
    organization = create(:organization, is_open: true)
    request.env['omniauth.auth'] = setup_google_omniauth

    request.env['omniauth.params'] = {
      'origin' => 'http://qa.lvh.me:4001/insights/testsett',
      'widget' => 'true',
      'current_host' => CustomCipher.new.encrypt(context: "#{organization.host_name}.test.host")
    }
    get :create, provider: :google

    assert_match({redirect_uri: 'http://qa.lvh.me:4001/insights/testsett'}.to_param, response.redirect_url)
    assert_match(/jwtToken|firstname|lastname/, response.redirect_url)
  end

  test 'should not set remember_created_at when config is not present' do
    create_default_ssos
    stub_facebook_oauth_request
    create_default_org
    set_host Organization.default_org

    request.env['omniauth.auth'] = setup_fb_omniauth
    user = create(:user, email: 'joe@bloggs.com', first_name: nil, last_name: nil, picture_url: nil,
      created_at: 5.days.ago, is_complete: true, organization: Organization.default_org)
    facebook_identity = create(:identity_provider, :facebook, user: user, uid: '1234567')
    assert_nil facebook_identity.auth
    assert_no_difference -> {IdentityProvider.count} do
      assert_difference -> {Email.count} do
        assert_no_difference -> {User.count} do
          get :create, provider: :facebook
          assert_not_nil User.find_by(email: 'joe@bloggs.com').remember_created_at
        end
      end
    end
  end

  test 'should not set remember_created_at when config is present and set to false' do
    create_default_ssos
    stub_facebook_oauth_request
    create_default_org
    set_host Organization.default_org
    create :config, :boolean, name: 'sessionExpireOnBrowserClose', value: false, configable: Organization.default_org
    request.env['omniauth.auth'] = setup_fb_omniauth
    user = create(:user, email: 'joe@bloggs.com', first_name: nil, last_name: nil, picture_url: nil,
      created_at: 5.days.ago, is_complete: true, organization: Organization.default_org)
    facebook_identity = create(:identity_provider, :facebook, user: user, uid: '1234567')
    assert_nil facebook_identity.auth
    assert_no_difference -> {IdentityProvider.count} do
      assert_difference -> {Email.count} do
        assert_no_difference -> {User.count} do
          get :create, provider: :facebook
          assert_not_nil User.find_by(email: 'joe@bloggs.com').remember_created_at
        end
      end
    end
  end

  test 'should set remember_created_at when config is present and set to true' do
    create_default_ssos
    stub_facebook_oauth_request
    create_default_org
    set_host Organization.default_org
    create :config, :boolean, name: 'sessionExpireOnBrowserClose', value: true, configable: Organization.default_org
    request.env['omniauth.auth'] = setup_fb_omniauth
    user = create(:user, email: 'joe@bloggs.com', first_name: nil, last_name: nil, picture_url: nil,
      created_at: 5.days.ago, is_complete: true, organization: Organization.default_org)
    facebook_identity = create(:identity_provider, :facebook, user: user, uid: '1234567')
    assert_nil facebook_identity.auth
    assert_no_difference -> {IdentityProvider.count} do
      assert_difference -> {Email.count} do
        assert_no_difference -> {User.count} do
          get :create, provider: :facebook
          assert_nil User.find_by(email: 'joe@bloggs.com').remember_created_at
        end
      end
    end
  end

  test 'should set terms accepted status and date of user for okta enabled org' do
    create_default_org
    create_default_ssos
    set_host Organization.default_org
    Organization.any_instance.stubs(:okta_enabled?).returns(true)
    organization = create :organization, is_open: true, host_name: 'abc'

    stub_okta_group_creation
    stub_okta_app_creation
    stub_okta_group_app_link
    stub_okta_idp_creation

    revised_lxp_oauth = Sso.find_by(name: 'lxp_oauth')
    revised_lxp_oauth_org_sso = create :org_sso, sso: revised_lxp_oauth, organization: organization, label_name: 'Okta facebook', parameters: {idp: '0oag06wxehcYVdJ5F0h7', provider_name: 'facebook' }, oauth_client_id: 'daprMRQG0721!', oauth_client_secret: 'yHmhn1x5XknzChDAU8Q8tEUucb2748brLv_ydORx'

    request.env['omniauth.auth'] = setup_lxp_oauth_omniauth
    request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: "#{organization.host_name}.test.host"),
      'connector' => JWT.encode(revised_lxp_oauth_org_sso.id.to_s, Settings.features.integrations.secret, 'HS256'),
      'provider' => 'facebook',
      'terms_accepted' => true
    }

    get :create, provider: :lxp_oauth

    assert_response :found

    user = User.last
    assert_equal true, user.profile.tac_accepted
    assert_equal Date.today, user.profile.tac_accepted_at.to_date
  end

  # Though both the facebook buttons(legacy and revised) will not be visible to end-users at ONCE, this test case
  # is just to ensure that they both work flawlessly when both are ON.
  test 'legacy google and revised google should work when both enabled' do
    create_default_org
    create_default_ssos
    set_host Organization.default_org
    Organization.any_instance.unstub :default_sso, :okta_enabled?
    organization = create :organization, is_open: true, host_name: 'abc'

    # Legacy sso working
    request.env['omniauth.auth'] = setup_fb_omniauth
    # we dont send provider in omniauth params in legacy sso
    request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: "#{organization.host_name}.test.host")
    }

    get :create, provider: :facebook

    assert_response :found
    assert_equal 1, User.all.length
    assert_equal 1, IdentityProvider.all.length

    user = User.last
    assert_equal user.id, assigns(:current_user).id
    assert_nil user.federated_identifier

    # Revised sso working
    stub_okta_group_creation
    stub_okta_app_creation
    stub_okta_group_app_link
    stub_okta_idp_creation
    create :config, configable: organization, name: 'app.config.okta.enabled', data_type: 'boolean', value: 'true'
    create :config, configable: organization, name: 'OktaApiToken', data_type: 'string', value: SecureRandom.hex

    assert organization.okta_enabled?

    revised_lxp_oauth = Sso.find_by(name: 'lxp_oauth')
    revised_lxp_oauth_org_sso = create :org_sso, sso: revised_lxp_oauth, organization: organization, label_name: 'Okta facebook', parameters: {idp: '0oag06wxehcYVdJ5F0h7', provider_name: 'facebook' }, oauth_client_id: 'daprMRQG0721!', oauth_client_secret: 'yHmhn1x5XknzChDAU8Q8tEUucb2748brLv_ydORx'

    request.env['omniauth.auth'] = setup_lxp_oauth_omniauth
    request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: "#{organization.host_name}.test.host"),
      'connector' => JWT.encode(revised_lxp_oauth_org_sso.id.to_s, Settings.features.integrations.secret, 'HS256'),
      'provider' => 'facebook'
    }

    get :create, provider: :lxp_oauth

    assert_response :found
    assert_equal 2, User.all.length
    assert_equal 2, IdentityProvider.all.length

    user = User.last

    assert_equal user.federated_identifier, '1589184311142599-okta_user@gmail.com'
  end

  # Here, we will add 2 social login ssos for an org and check for following conditions. Both the conditions should work flawlessly.
  # 1. Both sso ON
  # 2. First added is OFF and second added is ON.
  test 'multiple lxp_oauth social login ssos with lxp_oauth' do
    create_default_ssos
    create_default_org
    set_host Organization.default_org
    Organization.any_instance.unstub :default_sso, :okta_enabled?
    stub_okta_group_creation
    stub_okta_app_creation
    stub_okta_group_app_link
    stub_okta_idp_creation
    organization = create :organization, is_open: true, host_name: 'abc'
    create :config, configable: organization, name: 'app.config.okta.enabled', data_type: 'boolean', value: 'true'
    create :config, configable: organization, name: 'OktaApiToken', data_type: 'string', value: SecureRandom.hex

    assert organization.okta_enabled?

    revised_lxp_oauth = Sso.find_by(name: 'lxp_oauth')
    # First SSO, facebook
    revised_facebook_org_sso = create :org_sso, sso: revised_lxp_oauth, organization: organization, label_name: 'Okta facebook', parameters: {idp: '0oag06wxehcYVdJ5F0h7', provider_name: 'facebook' }, oauth_client_id: 'daprMRQG0721!', oauth_client_secret: 'yHmhn1x5XknzChDAU8Q8tEUucb2748brLv_ydORx'

    # Second SSO, google
    revised_google_org_sso = create :org_sso, sso: revised_lxp_oauth, organization: organization, label_name: 'Okta google', parameters: {idp: '0oag06wxehcYVdJ5F0h7', provider_name: 'google' }, oauth_client_id: 'daprMRQG0721!', oauth_client_secret: 'yHmhn1x5XknzChDAU8Q8tEUucb2748brLv_ydORx'

    # 1. Both sso ON
    request.env['omniauth.auth'] = setup_lxp_oauth_omniauth
    request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: "#{organization.host_name}.test.host"),
      'connector' => JWT.encode(revised_facebook_org_sso.id.to_s, Settings.features.integrations.secret, 'HS256'),
      'provider' => 'facebook'
    }

    get :create, provider: :lxp_oauth

    assert_response :found
    assert_equal 1, IdentityProvider.all.length
    assert_equal 1, User.all.length


    request.env['omniauth.auth'] = setup_lxp_oauth_omniauth
    request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: "#{organization.host_name}.test.host"),
      'connector' => JWT.encode(revised_google_org_sso.id.to_s, Settings.features.integrations.secret, 'HS256'),
      'provider' => 'google'
    }

    request.env['omniauth.auth'][:uid] = '00uhrfiiyx6vS6mjU2p6'
    request.env['omniauth.auth'][:extra][:raw_info][:profile] = 'https://plus.google.com/102152619356432138077'

    get :create, provider: :lxp_oauth

    assert_response :found
    assert_equal 2, IdentityProvider.all.length
    assert_equal 1, User.all.length

    # 2. First added is OFF and second added is ON.
    # Facebook, is OFF.
    # Google, is ON.
    revised_facebook_org_sso.update_attributes!(is_enabled: false)

    refute revised_facebook_org_sso.reload.is_enabled

    request.env['omniauth.auth'] = setup_lxp_oauth_omniauth
    request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: "#{organization.host_name}.test.host"),
      'connector' => JWT.encode(revised_google_org_sso.id.to_s, Settings.features.integrations.secret, 'HS256'),
      'provider' => 'google'
    }

    request.env['omniauth.auth'][:uid] = '00uhrfiiyx6vS6mjU2p6'
    request.env['omniauth.auth'][:extra][:raw_info][:profile] = 'https://plus.google.com/102152619356432138077'

    get :create, provider: :lxp_oauth

    # Works even if Facebook is OFF.
    assert_response :found
  end

  # Here, we will add 2 ssos, 1 social login and other saml, for an org and check for following conditions. Both the conditions should work flawlessly.
  # 1. Both sso ON
  # 2. First added is OFF and second added is ON.
  test 'multiple lxp_oauth ssos with lxp_oauth' do
    create_default_ssos
    create_default_org
    set_host Organization.default_org
    Organization.any_instance.unstub :default_sso, :okta_enabled?
    stub_okta_group_creation
    stub_okta_app_creation
    stub_okta_group_app_link
    stub_okta_idp_creation
    organization = create :organization, is_open: true, host_name: 'abc'
    create :config, configable: organization, name: 'app.config.okta.enabled', data_type: 'boolean', value: 'true'
    create :config, configable: organization, name: 'OktaApiToken', data_type: 'string', value: SecureRandom.hex

    assert organization.okta_enabled?

    revised_lxp_oauth = Sso.find_by(name: 'lxp_oauth')
    # First SSO, facebook
    revised_facebook_org_sso = create :org_sso, sso: revised_lxp_oauth, organization: organization, label_name: 'Okta facebook', parameters: {idp: '0oag06wxehcYVdJ5F0h7', provider_name: 'facebook' }, oauth_client_id: 'daprMRQG0721!', oauth_client_secret: 'yHmhn1x5XknzChDAU8Q8tEUucb2748brLv_ydORx'

    # Second SSO, saml
    revised_saml_org_sso = create :org_sso, sso: revised_lxp_oauth, organization: organization, label_name: 'Okta saml', parameters: {idp: '0oag06wxehcYVdJ5F0h6', provider_name: 'saml' }, oauth_client_id: 'daprMRQG0721!', oauth_client_secret: 'yHmhn1x5XknzChDAU8Q8tEUucb2748brLv_ydORx'

    # 1. Both sso ON
    request.env['omniauth.auth'] = setup_lxp_oauth_omniauth
    request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: "#{organization.host_name}.test.host"),
      'connector' => JWT.encode(revised_facebook_org_sso.id.to_s, Settings.features.integrations.secret, 'HS256'),
      'provider' => 'facebook'
    }

    get :create, provider: :lxp_oauth

    assert_response :found

    assert_equal 1, IdentityProvider.all.length
    assert_equal 1, User.all.length


    request.env['omniauth.auth'] = sso_info
    request.env['omniauth.auth']['uid'] = '212413405'
    request.env['omniauth.auth']['provider'] = 'lxp_oauth'
    request.env['omniauth.auth']['info'] = {email: 'okta-user@gmail.com'}
    request.env['omniauth.auth']['credentials'] = {
      token: "eyJraWQiOiJfUlhlWFh4R1NXMFRxSUQ5SmxhSjF3dl9ENWI0dkU3Z1BxUDgzb3lOZlBvIiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULjh0R2FPaEI5VUF2c29kUTkwblJuN2owXzB2R3lZU2piMEVlWlN1WjdBR0EiLCJpc3MiOiJodHRwczovL2Rldi01Nzk1NzUub2t0YXByZXZpZXcuY29tL29hdXRoMi9kZWZhdWx0IiwiYXVkIjoiYXBpOi8vZGVmYXVsdCIsImlhdCI6MTUxNDI3MDkwMCwiZXhwIjoxNTE0Mjc0NTAwLCJjaWQiOiIwb2FkMDNtdzkxdVNHWjJEVjBoNyIsInVpZCI6IjAwdWQyOXA2czYydmI1TldxMGg3Iiwic2NwIjpbImVtYWlsIiwib3BlbmlkIiwicHJvZmlsZSJdLCJzdWIiOiIxNTg5MTg0MzExMTQyNTk5a2Fub2ppeWFrYXZpdGFAZ21haWwuY29tIn0.QV_Go1wHhHxcgD5u8ZZbGzaj90Klch2rE_ELRe7kJVco1FKPw6yhinm-oEgeUoCRZi_QnvNi2OmQOsO7-lV0X2CzyGfAquaxOkSFne7TU3J9IKka1ip4TRNZQ3tQY1Pqbbrq9Jrn62wmdr_7Z2akuri5qyum8PkX_tjQRiFpyHuIFZXBVb2S4WgngFE5phJIWXNhQw3rxYiW9Y3K4ltqSsn0uApI0N9hu3CcsAIg-jiaNDL3wqpGHpx7eJhcF3Fk2zY9Hxr5kJHReyMfJXNkmJ2Jyc5ZxS5lDGBiQdyqEmFY8PjzFYoN7IUumXeoHz42xAkNWOIliAlUS5kOLRp-jQ",
      expires_at: 1514274650,
      expires: true,
      id_token: "eyJraWQiOiJfUlhlWFh4R1NXMFRxSUQ5SmxhSjF3dl9ENWI0dkU3Z1BxUDgzb3lOZlBvIiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiIwMHVkMjlwNnM2MnZiNU5XcTBoNyIsIm5hbWUiOiJLYXZpdGEgS2Fub2ppeWEiLCJlbWFpbCI6Imthbm9qaXlha2F2aXRhQGdtYWlsLmNvbSIsInZlciI6MSwiaXNzIjoiaHR0cHM6Ly9kZXYtNTc5NTc1Lm9rdGFwcmV2aWV3LmNvbS9vYXV0aDIvZGVmYXVsdCIsImF1ZCI6IjBvYWQwM213OTF1U0daMkRWMGg3IiwiaWF0IjoxNTE0MjcwOTAwLCJleHAiOjE1MTQyNzQ1MDAsImp0aSI6IklELjhLaUNUTGc1TllLSmdEV0ZEdDgzdEEzWmRkTEVIeGZmMkp2ZUtDNkdBdGciLCJhbXIiOlsicHdkIl0sImlkcCI6IjBvYWQwY202YXJjRHRxbXNvMGg3Iiwibm9uY2UiOiJZc0c3NmpvIiwicHJlZmVycmVkX3VzZXJuYW1lIjoiMTU4OTE4NDMxMTE0MjU5OWthbm9qaXlha2F2aXRhQGdtYWlsLmNvbSIsImF1dGhfdGltZSI6MTUxNDI3MDg5OCwiYXRfaGFzaCI6InFwdmpZY09HMU5TMWlpc3VJbGJUcHcifQ.iafYiUluNghHf55H-Voxo6VRp5V1XHX_wFl7ize5KQ3Bk8nuVdGsIChjINFW3p1HCKFYBaA2lpm5YLBY6ABgW-zlqQfkYms04cxcPQCHiJG737QntGYyVwcFSxUdtiJloraV38mNMaGvdQw_QnuCkb_ofELVVT1BiOj-QUppQbT270mF-mAyQrdaFP9tzdkO17j-b6EVmHaZ7cEXSvfyydl5g3hZnJKi4YuBUF1LlsUqOV6GqOLfw_OpF2_aDbpp4X6as7KTrN6H2fuerLqTCFt-4OcnXzihnOtqRrpJEnS-_-Cc3AlfvNvgdY9p8POCJdz0Cg13ZFMxNyixPh8QEw",
      refresh_token: 'bvZVOpjjakh6b57aGDX4lG-rxI4y4ztPLRUqVyDl4TI'
    }
    request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: "#{organization.host_name}.test.host"),
      'connector' => JWT.encode(revised_saml_org_sso.id.to_s, Settings.features.integrations.secret, 'HS256'),
      'provider' => 'saml'
    }

    get :create, provider: :lxp_oauth

    assert_response :found
    assert_equal 2, IdentityProvider.all.length
    assert_equal 1, User.all.length

    # 2. First added is OFF and second added is ON.
    # LxpOauth - Facebook, is OFF.
    # LxpOauth - SAML, is ON.
    revised_facebook_org_sso.update_attributes!(is_enabled: false)

    refute revised_facebook_org_sso.reload.is_enabled

    request.env['omniauth.auth'] = sso_info
    request.env['omniauth.auth']['uid'] = '212413405'
    request.env['omniauth.auth']['provider'] = 'lxp_oauth'
    request.env['omniauth.auth']['info'] = {email: 'okta-user@gmail.com'}
    request.env['omniauth.auth']['credentials'] = {
      token: "eyJraWQiOiJfUlhlWFh4R1NXMFRxSUQ5SmxhSjF3dl9ENWI0dkU3Z1BxUDgzb3lOZlBvIiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULjh0R2FPaEI5VUF2c29kUTkwblJuN2owXzB2R3lZU2piMEVlWlN1WjdBR0EiLCJpc3MiOiJodHRwczovL2Rldi01Nzk1NzUub2t0YXByZXZpZXcuY29tL29hdXRoMi9kZWZhdWx0IiwiYXVkIjoiYXBpOi8vZGVmYXVsdCIsImlhdCI6MTUxNDI3MDkwMCwiZXhwIjoxNTE0Mjc0NTAwLCJjaWQiOiIwb2FkMDNtdzkxdVNHWjJEVjBoNyIsInVpZCI6IjAwdWQyOXA2czYydmI1TldxMGg3Iiwic2NwIjpbImVtYWlsIiwib3BlbmlkIiwicHJvZmlsZSJdLCJzdWIiOiIxNTg5MTg0MzExMTQyNTk5a2Fub2ppeWFrYXZpdGFAZ21haWwuY29tIn0.QV_Go1wHhHxcgD5u8ZZbGzaj90Klch2rE_ELRe7kJVco1FKPw6yhinm-oEgeUoCRZi_QnvNi2OmQOsO7-lV0X2CzyGfAquaxOkSFne7TU3J9IKka1ip4TRNZQ3tQY1Pqbbrq9Jrn62wmdr_7Z2akuri5qyum8PkX_tjQRiFpyHuIFZXBVb2S4WgngFE5phJIWXNhQw3rxYiW9Y3K4ltqSsn0uApI0N9hu3CcsAIg-jiaNDL3wqpGHpx7eJhcF3Fk2zY9Hxr5kJHReyMfJXNkmJ2Jyc5ZxS5lDGBiQdyqEmFY8PjzFYoN7IUumXeoHz42xAkNWOIliAlUS5kOLRp-jQ",
      expires_at: 1514274650,
      expires: true,
      id_token: "eyJraWQiOiJfUlhlWFh4R1NXMFRxSUQ5SmxhSjF3dl9ENWI0dkU3Z1BxUDgzb3lOZlBvIiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiIwMHVkMjlwNnM2MnZiNU5XcTBoNyIsIm5hbWUiOiJLYXZpdGEgS2Fub2ppeWEiLCJlbWFpbCI6Imthbm9qaXlha2F2aXRhQGdtYWlsLmNvbSIsInZlciI6MSwiaXNzIjoiaHR0cHM6Ly9kZXYtNTc5NTc1Lm9rdGFwcmV2aWV3LmNvbS9vYXV0aDIvZGVmYXVsdCIsImF1ZCI6IjBvYWQwM213OTF1U0daMkRWMGg3IiwiaWF0IjoxNTE0MjcwOTAwLCJleHAiOjE1MTQyNzQ1MDAsImp0aSI6IklELjhLaUNUTGc1TllLSmdEV0ZEdDgzdEEzWmRkTEVIeGZmMkp2ZUtDNkdBdGciLCJhbXIiOlsicHdkIl0sImlkcCI6IjBvYWQwY202YXJjRHRxbXNvMGg3Iiwibm9uY2UiOiJZc0c3NmpvIiwicHJlZmVycmVkX3VzZXJuYW1lIjoiMTU4OTE4NDMxMTE0MjU5OWthbm9qaXlha2F2aXRhQGdtYWlsLmNvbSIsImF1dGhfdGltZSI6MTUxNDI3MDg5OCwiYXRfaGFzaCI6InFwdmpZY09HMU5TMWlpc3VJbGJUcHcifQ.iafYiUluNghHf55H-Voxo6VRp5V1XHX_wFl7ize5KQ3Bk8nuVdGsIChjINFW3p1HCKFYBaA2lpm5YLBY6ABgW-zlqQfkYms04cxcPQCHiJG737QntGYyVwcFSxUdtiJloraV38mNMaGvdQw_QnuCkb_ofELVVT1BiOj-QUppQbT270mF-mAyQrdaFP9tzdkO17j-b6EVmHaZ7cEXSvfyydl5g3hZnJKi4YuBUF1LlsUqOV6GqOLfw_OpF2_aDbpp4X6as7KTrN6H2fuerLqTCFt-4OcnXzihnOtqRrpJEnS-_-Cc3AlfvNvgdY9p8POCJdz0Cg13ZFMxNyixPh8QEw",
      refresh_token: 'bvZVOpjjakh6b57aGDX4lG-rxI4y4ztPLRUqVyDl4TI'
    }
    request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: "#{organization.host_name}.test.host"),
      'connector' => JWT.encode(revised_saml_org_sso.id.to_s, Settings.features.integrations.secret, 'HS256'),
      'provider' => 'saml'
    }

    get :create, provider: :lxp_oauth

    # Works even if LxpOauth - Facebook is OFF.
    assert_response :found
  end

  # Here, we will add 2 ssos, both lxp_oauth saml, for an org and check for following conditions. Both the conditions should work flawlessly.
  # 1. Both sso ON
  # 2. First added is OFF and second added is ON.
  test 'multiple lxp_oauth ssos with lxp_oauth scenario 2' do
    create_default_ssos
    create_default_org
    set_host Organization.default_org
    Organization.any_instance.unstub :default_sso, :okta_enabled?
    stub_okta_group_creation
    stub_okta_app_creation
    stub_okta_group_app_link
    stub_okta_idp_creation
    organization = create :organization, is_open: true, host_name: 'abc'
    create :config, configable: organization, name: 'app.config.okta.enabled', data_type: 'boolean', value: 'true'
    create :config, configable: organization, name: 'OktaApiToken', data_type: 'string', value: SecureRandom.hex

    assert organization.okta_enabled?

    revised_lxp_oauth = Sso.find_by(name: 'lxp_oauth')
    # First SSO, saml - eg: cyient
    cyient_org_sso = create :org_sso, sso: revised_lxp_oauth, organization: organization, label_name: 'Okta saml', parameters: {idp: '0oag06wxehcYVdJ5F0h7', provider_name: 'saml' }, oauth_client_id: '0oah1vuw7eODvjvS42p6', oauth_client_secret: '7KuQ9SS0rZy_KBGhTZrV7HGSfFH-7NvB8pSiYAnw'

    # Second SSO, saml - eg: techm
    techm_org_sso = create :org_sso, sso: revised_lxp_oauth, organization: organization, label_name: 'Okta saml', parameters: {idp: '0oag06wxehcYVdJ5F0h6', provider_name: 'saml' }, oauth_client_id: 'daprMRQG0721!', oauth_client_secret: 'yHmhn1x5XknzChDAU8Q8tEUucb2748brLv_ydORx'

    # 1. Both sso ON
    request.env['omniauth.auth'] = sso_info
    request.env['omniauth.auth']['uid'] = '111111111'
    request.env['omniauth.auth']['provider'] = 'lxp_oauth'
    request.env['omniauth.auth']['info'] = {email: 'okta-user@gmail.com'}
    request.env['omniauth.auth']['credentials'] = {
      token: "eyJraWQiOiJfUlhlWFh4R1NXMFRxSUQ5SmxhSjF3dl9ENWI0dkU3Z1BxUDgzb3lOZlBvIiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULjh0R2FPaEI5VUF2c29kUTkwblJuN2owXzB2R3lZU2piMEVlWlN1WjdBR0EiLCJpc3MiOiJodHRwczovL2Rldi01Nzk1NzUub2t0YXByZXZpZXcuY29tL29hdXRoMi9kZWZhdWx0IiwiYXVkIjoiYXBpOi8vZGVmYXVsdCIsImlhdCI6MTUxNDI3MDkwMCwiZXhwIjoxNTE0Mjc0NTAwLCJjaWQiOiIwb2FkMDNtdzkxdVNHWjJEVjBoNyIsInVpZCI6IjAwdWQyOXA2czYydmI1TldxMGg3Iiwic2NwIjpbImVtYWlsIiwib3BlbmlkIiwicHJvZmlsZSJdLCJzdWIiOiIxNTg5MTg0MzExMTQyNTk5a2Fub2ppeWFrYXZpdGFAZ21haWwuY29tIn0.QV_Go1wHhHxcgD5u8ZZbGzaj90Klch2rE_ELRe7kJVco1FKPw6yhinm-oEgeUoCRZi_QnvNi2OmQOsO7-lV0X2CzyGfAquaxOkSFne7TU3J9IKka1ip4TRNZQ3tQY1Pqbbrq9Jrn62wmdr_7Z2akuri5qyum8PkX_tjQRiFpyHuIFZXBVb2S4WgngFE5phJIWXNhQw3rxYiW9Y3K4ltqSsn0uApI0N9hu3CcsAIg-jiaNDL3wqpGHpx7eJhcF3Fk2zY9Hxr5kJHReyMfJXNkmJ2Jyc5ZxS5lDGBiQdyqEmFY8PjzFYoN7IUumXeoHz42xAkNWOIliAlUS5kOLRp-jQ",
      expires_at: 1514274650,
      expires: true,
      id_token: "eyJraWQiOiJfUlhlWFh4R1NXMFRxSUQ5SmxhSjF3dl9ENWI0dkU3Z1BxUDgzb3lOZlBvIiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiIwMHVkMjlwNnM2MnZiNU5XcTBoNyIsIm5hbWUiOiJLYXZpdGEgS2Fub2ppeWEiLCJlbWFpbCI6Imthbm9qaXlha2F2aXRhQGdtYWlsLmNvbSIsInZlciI6MSwiaXNzIjoiaHR0cHM6Ly9kZXYtNTc5NTc1Lm9rdGFwcmV2aWV3LmNvbS9vYXV0aDIvZGVmYXVsdCIsImF1ZCI6IjBvYWQwM213OTF1U0daMkRWMGg3IiwiaWF0IjoxNTE0MjcwOTAwLCJleHAiOjE1MTQyNzQ1MDAsImp0aSI6IklELjhLaUNUTGc1TllLSmdEV0ZEdDgzdEEzWmRkTEVIeGZmMkp2ZUtDNkdBdGciLCJhbXIiOlsicHdkIl0sImlkcCI6IjBvYWQwY202YXJjRHRxbXNvMGg3Iiwibm9uY2UiOiJZc0c3NmpvIiwicHJlZmVycmVkX3VzZXJuYW1lIjoiMTU4OTE4NDMxMTE0MjU5OWthbm9qaXlha2F2aXRhQGdtYWlsLmNvbSIsImF1dGhfdGltZSI6MTUxNDI3MDg5OCwiYXRfaGFzaCI6InFwdmpZY09HMU5TMWlpc3VJbGJUcHcifQ.iafYiUluNghHf55H-Voxo6VRp5V1XHX_wFl7ize5KQ3Bk8nuVdGsIChjINFW3p1HCKFYBaA2lpm5YLBY6ABgW-zlqQfkYms04cxcPQCHiJG737QntGYyVwcFSxUdtiJloraV38mNMaGvdQw_QnuCkb_ofELVVT1BiOj-QUppQbT270mF-mAyQrdaFP9tzdkO17j-b6EVmHaZ7cEXSvfyydl5g3hZnJKi4YuBUF1LlsUqOV6GqOLfw_OpF2_aDbpp4X6as7KTrN6H2fuerLqTCFt-4OcnXzihnOtqRrpJEnS-_-Cc3AlfvNvgdY9p8POCJdz0Cg13ZFMxNyixPh8QEw",
      refresh_token: 'bvZVOpjjakh6b57aGDX4lG-rxI4y4ztPLRUqVyDl4TI'
    }
    request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: "#{organization.host_name}.test.host"),
      'connector' => JWT.encode(cyient_org_sso.id.to_s, Settings.features.integrations.secret, 'HS256'),
      'provider' => 'saml'
    }

    # login for cyient
    get :create, provider: :lxp_oauth

    assert_response :found

    assert_equal 1, IdentityProvider.all.length
    assert_equal 1, User.all.length


    request.env['omniauth.auth'] = sso_info
    request.env['omniauth.auth']['uid'] = '222222222'
    request.env['omniauth.auth']['provider'] = 'lxp_oauth'
    request.env['omniauth.auth']['info'] = {email: 'okta-user@gmail.com'}
    request.env['omniauth.auth']['credentials'] = {
      token: "eyJraWQiOiJfUlhlWFh4R1NXMFRxSUQ5SmxhSjF3dl9ENWI0dkU3Z1BxUDgzb3lOZlBvIiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULjh0R2FPaEI5VUF2c29kUTkwblJuN2owXzB2R3lZU2piMEVlWlN1WjdBR0EiLCJpc3MiOiJodHRwczovL2Rldi01Nzk1NzUub2t0YXByZXZpZXcuY29tL29hdXRoMi9kZWZhdWx0IiwiYXVkIjoiYXBpOi8vZGVmYXVsdCIsImlhdCI6MTUxNDI3MDkwMCwiZXhwIjoxNTE0Mjc0NTAwLCJjaWQiOiIwb2FkMDNtdzkxdVNHWjJEVjBoNyIsInVpZCI6IjAwdWQyOXA2czYydmI1TldxMGg3Iiwic2NwIjpbImVtYWlsIiwib3BlbmlkIiwicHJvZmlsZSJdLCJzdWIiOiIxNTg5MTg0MzExMTQyNTk5a2Fub2ppeWFrYXZpdGFAZ21haWwuY29tIn0.QV_Go1wHhHxcgD5u8ZZbGzaj90Klch2rE_ELRe7kJVco1FKPw6yhinm-oEgeUoCRZi_QnvNi2OmQOsO7-lV0X2CzyGfAquaxOkSFne7TU3J9IKka1ip4TRNZQ3tQY1Pqbbrq9Jrn62wmdr_7Z2akuri5qyum8PkX_tjQRiFpyHuIFZXBVb2S4WgngFE5phJIWXNhQw3rxYiW9Y3K4ltqSsn0uApI0N9hu3CcsAIg-jiaNDL3wqpGHpx7eJhcF3Fk2zY9Hxr5kJHReyMfJXNkmJ2Jyc5ZxS5lDGBiQdyqEmFY8PjzFYoN7IUumXeoHz42xAkNWOIliAlUS5kOLRp-jQ",
      expires_at: 1514274650,
      expires: true,
      id_token: "eyJraWQiOiJfUlhlWFh4R1NXMFRxSUQ5SmxhSjF3dl9ENWI0dkU3Z1BxUDgzb3lOZlBvIiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiIwMHVkMjlwNnM2MnZiNU5XcTBoNyIsIm5hbWUiOiJLYXZpdGEgS2Fub2ppeWEiLCJlbWFpbCI6Imthbm9qaXlha2F2aXRhQGdtYWlsLmNvbSIsInZlciI6MSwiaXNzIjoiaHR0cHM6Ly9kZXYtNTc5NTc1Lm9rdGFwcmV2aWV3LmNvbS9vYXV0aDIvZGVmYXVsdCIsImF1ZCI6IjBvYWQwM213OTF1U0daMkRWMGg3IiwiaWF0IjoxNTE0MjcwOTAwLCJleHAiOjE1MTQyNzQ1MDAsImp0aSI6IklELjhLaUNUTGc1TllLSmdEV0ZEdDgzdEEzWmRkTEVIeGZmMkp2ZUtDNkdBdGciLCJhbXIiOlsicHdkIl0sImlkcCI6IjBvYWQwY202YXJjRHRxbXNvMGg3Iiwibm9uY2UiOiJZc0c3NmpvIiwicHJlZmVycmVkX3VzZXJuYW1lIjoiMTU4OTE4NDMxMTE0MjU5OWthbm9qaXlha2F2aXRhQGdtYWlsLmNvbSIsImF1dGhfdGltZSI6MTUxNDI3MDg5OCwiYXRfaGFzaCI6InFwdmpZY09HMU5TMWlpc3VJbGJUcHcifQ.iafYiUluNghHf55H-Voxo6VRp5V1XHX_wFl7ize5KQ3Bk8nuVdGsIChjINFW3p1HCKFYBaA2lpm5YLBY6ABgW-zlqQfkYms04cxcPQCHiJG737QntGYyVwcFSxUdtiJloraV38mNMaGvdQw_QnuCkb_ofELVVT1BiOj-QUppQbT270mF-mAyQrdaFP9tzdkO17j-b6EVmHaZ7cEXSvfyydl5g3hZnJKi4YuBUF1LlsUqOV6GqOLfw_OpF2_aDbpp4X6as7KTrN6H2fuerLqTCFt-4OcnXzihnOtqRrpJEnS-_-Cc3AlfvNvgdY9p8POCJdz0Cg13ZFMxNyixPh8QEw",
      refresh_token: 'bvZVOpjjakh6b57aGDX4lG-rxI4y4ztPLRUqVyDl4TI'
    }
    request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: "#{organization.host_name}.test.host"),
      'connector' => JWT.encode(techm_org_sso.id.to_s, Settings.features.integrations.secret, 'HS256'),
      'provider' => 'saml'
    }

    # login for techm
    get :create, provider: :lxp_oauth

    assert_response :found
    assert_equal 2, IdentityProvider.all.length
    assert_equal 1, User.all.length

    # 2. First added is OFF and second added is ON.
    # LxpOauth - SAML - cyient, is OFF.
    # LxpOauth - SAML - techm, is ON.
    cyient_org_sso.update_attributes!(is_enabled: false)

    refute cyient_org_sso.reload.is_enabled

    request.env['omniauth.auth'] = sso_info
    request.env['omniauth.auth']['uid'] = '222222222'
    request.env['omniauth.auth']['provider'] = 'lxp_oauth'
    request.env['omniauth.auth']['info'] = {email: 'okta-user@gmail.com'}
    request.env['omniauth.auth']['credentials'] = {
      token: "eyJraWQiOiJfUlhlWFh4R1NXMFRxSUQ5SmxhSjF3dl9ENWI0dkU3Z1BxUDgzb3lOZlBvIiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULjh0R2FPaEI5VUF2c29kUTkwblJuN2owXzB2R3lZU2piMEVlWlN1WjdBR0EiLCJpc3MiOiJodHRwczovL2Rldi01Nzk1NzUub2t0YXByZXZpZXcuY29tL29hdXRoMi9kZWZhdWx0IiwiYXVkIjoiYXBpOi8vZGVmYXVsdCIsImlhdCI6MTUxNDI3MDkwMCwiZXhwIjoxNTE0Mjc0NTAwLCJjaWQiOiIwb2FkMDNtdzkxdVNHWjJEVjBoNyIsInVpZCI6IjAwdWQyOXA2czYydmI1TldxMGg3Iiwic2NwIjpbImVtYWlsIiwib3BlbmlkIiwicHJvZmlsZSJdLCJzdWIiOiIxNTg5MTg0MzExMTQyNTk5a2Fub2ppeWFrYXZpdGFAZ21haWwuY29tIn0.QV_Go1wHhHxcgD5u8ZZbGzaj90Klch2rE_ELRe7kJVco1FKPw6yhinm-oEgeUoCRZi_QnvNi2OmQOsO7-lV0X2CzyGfAquaxOkSFne7TU3J9IKka1ip4TRNZQ3tQY1Pqbbrq9Jrn62wmdr_7Z2akuri5qyum8PkX_tjQRiFpyHuIFZXBVb2S4WgngFE5phJIWXNhQw3rxYiW9Y3K4ltqSsn0uApI0N9hu3CcsAIg-jiaNDL3wqpGHpx7eJhcF3Fk2zY9Hxr5kJHReyMfJXNkmJ2Jyc5ZxS5lDGBiQdyqEmFY8PjzFYoN7IUumXeoHz42xAkNWOIliAlUS5kOLRp-jQ",
      expires_at: 1514274650,
      expires: true,
      id_token: "eyJraWQiOiJfUlhlWFh4R1NXMFRxSUQ5SmxhSjF3dl9ENWI0dkU3Z1BxUDgzb3lOZlBvIiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiIwMHVkMjlwNnM2MnZiNU5XcTBoNyIsIm5hbWUiOiJLYXZpdGEgS2Fub2ppeWEiLCJlbWFpbCI6Imthbm9qaXlha2F2aXRhQGdtYWlsLmNvbSIsInZlciI6MSwiaXNzIjoiaHR0cHM6Ly9kZXYtNTc5NTc1Lm9rdGFwcmV2aWV3LmNvbS9vYXV0aDIvZGVmYXVsdCIsImF1ZCI6IjBvYWQwM213OTF1U0daMkRWMGg3IiwiaWF0IjoxNTE0MjcwOTAwLCJleHAiOjE1MTQyNzQ1MDAsImp0aSI6IklELjhLaUNUTGc1TllLSmdEV0ZEdDgzdEEzWmRkTEVIeGZmMkp2ZUtDNkdBdGciLCJhbXIiOlsicHdkIl0sImlkcCI6IjBvYWQwY202YXJjRHRxbXNvMGg3Iiwibm9uY2UiOiJZc0c3NmpvIiwicHJlZmVycmVkX3VzZXJuYW1lIjoiMTU4OTE4NDMxMTE0MjU5OWthbm9qaXlha2F2aXRhQGdtYWlsLmNvbSIsImF1dGhfdGltZSI6MTUxNDI3MDg5OCwiYXRfaGFzaCI6InFwdmpZY09HMU5TMWlpc3VJbGJUcHcifQ.iafYiUluNghHf55H-Voxo6VRp5V1XHX_wFl7ize5KQ3Bk8nuVdGsIChjINFW3p1HCKFYBaA2lpm5YLBY6ABgW-zlqQfkYms04cxcPQCHiJG737QntGYyVwcFSxUdtiJloraV38mNMaGvdQw_QnuCkb_ofELVVT1BiOj-QUppQbT270mF-mAyQrdaFP9tzdkO17j-b6EVmHaZ7cEXSvfyydl5g3hZnJKi4YuBUF1LlsUqOV6GqOLfw_OpF2_aDbpp4X6as7KTrN6H2fuerLqTCFt-4OcnXzihnOtqRrpJEnS-_-Cc3AlfvNvgdY9p8POCJdz0Cg13ZFMxNyixPh8QEw",
      refresh_token: 'bvZVOpjjakh6b57aGDX4lG-rxI4y4ztPLRUqVyDl4TI'
    }
    request.env['omniauth.params'] = {
      'current_host' => CustomCipher.new.encrypt(context: "#{organization.host_name}.test.host"),
      'connector' => JWT.encode(techm_org_sso.id.to_s, Settings.features.integrations.secret, 'HS256'),
      'provider' => 'saml'
    }

    get :create, provider: :lxp_oauth

    # Works even if LxpOauth - SAML is OFF.
    assert_response :found
  end

end

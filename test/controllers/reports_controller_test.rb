require 'test_helper'

class ReportsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    @user = create(:user)
    @group = create(:resource_group)
    @group.add_member @user
    @post = create(:post, group: @group, user: @user)
    @comment = create(:comment, commentable: @post, user: @user)
    @question = create(:question, group: @group, user: @user)
    @answer = create(:answer, commentable: @question, user: @user)
  end

  def report_stat_setup
    @user1 = create(:user)
    @user2 = create(:user)
    @user3 = create(:user)
    @group.add_member @user1
    @group.add_member @user2
    @group.add_member @user3
  end

  test "report stats should return default 0" do
    create_default_org
    report_stat_setup
    sign_in @user
    get :stats, :post_id => @post.id, :format => :json

    assert_response :success
    response_object = DeepSnakeCaser.deep_snake_case!(ActiveSupport::JSON.decode response.body)
    assert_equal response_object['spam'], 0
    assert_equal response_object['inappropriate'], 0
    assert_equal response_object['compromised'], 0
    sign_out @user
  end

  test "should show status forbidden if user not signed in" do
    assert_no_difference ->{Report.count} do
      post :create, {:post_id => @post.id, :type => 'spam', :format => :json}
    end
    assert_response :unauthorized

    get :stats, {:post_id => @post.id, :format => :json}
    assert_response :unauthorized
  end

  test "bad report type" do
    sign_in @user
    assert_no_difference [->{Report.count}, ->{@post.reports.count}] do
      post :create, {:post_id => @post.id, :type => 'offensive'}
    end
    assert_response :unprocessable_entity

    assert_no_difference [->{Report.count}, ->{@post.reports.count}] do
      post :create, {:post_id => @post.id}
    end
    assert_response :unprocessable_entity
  end
end

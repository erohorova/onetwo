require 'test_helper'

class SavannahApi::MailerConfigsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'

    @org = create(:organization)
    @client  = create(:client, organization: @org)
    @api_key = @client.valid_credential.api_key
    @secret  = @client.valid_credential.shared_secret

    @mc_params = {
      address: 'address.addr.com',
      port: 123,
      user_name: 'user_name',
      password: User.random_password,
      domain: 'domain.com',
      from_address: 'sender@addr.com',
      from_name: 'user_name',
      webhook_keys: 'webhook_keys',
      enable_starttls_auto: true,
      authentication: 'authentication'
    }
    jwt_token = JWT.encode(@mc_params, @secret,'HS256')
    request.headers['X-API-KEY'] = @api_key
    request.headers['X-API-TOKEN'] = jwt_token
  end

  test 'creates mailer config properly' do
    post :save, @mc_params
    assert_response :success
    config = @org.reload.mailer_config
    assert config.address, @mc_params[:address]
    assert config.port, @mc_params[:port]
    assert config.user_name, @mc_params[:user_name]
    assert config.password, @mc_params[:password]
    assert config.domain, @mc_params[:domain]
    assert config.from_address, @mc_params[:from_address]
    assert config.from_name, @mc_params[:from_name]
    assert config.webhook_keys, @mc_params[:webhook_keys]
    assert config.enable_starttls_auto
    assert config.authentication, @mc_params[:authentication]
  end

  test 'updates mailer config properly' do
    create :mailer_config, organization: @org

    assert @org.reload.mailer_config

    @mc_params.merge!(user_name: 'updated_user_name')
    post :save, @mc_params
    assert_response :success

    config = MailerConfig.where(organization_id: @org.id).first
    assert config.user_name, 'updated_user_name'
  end

  test 'unauthorized for invalid api_token when saving mailer config' do
    @mc_params.merge!(address: 'bad.addr.com')

    jwt_token = JWT.encode(@mc_params, @secret,'HS256')
    request.headers['X-API-KEY'] = @api_key
    request.headers['X-API-TOKEN'] = "somerandomtoken"

    post :save, @mc_params
    assert_response :unauthorized
  end

  test 'destroys mailer config properly' do
    create :mailer_config, organization: @org

    delete :destroy, format: :json
    assert_nil @org.reload.mailer_config
  end
end

require 'test_helper'

class SavannahApi::AppConfigsControllerTest < ActionController::TestCase

  class WithApiKey < ActionController::TestCase
    include Devise::Test::ControllerHelpers
    setup do
      skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'

      @org = create(:organization)
      client = create(:client, organization: @org)
      @api_key = @org.client.valid_credential.api_key
      @secret  = @org.client.valid_credential.shared_secret

      @config_params = {
          category: "mail",
          name: "test",
          value: "123"}


      jwt_token = JWT.encode(@config_params, @secret,'HS256')
      request.headers['X-API-KEY'] = @api_key
      request.headers['X-API-TOKEN'] = jwt_token
    end

    test 'creates config properly' do
      post :save, @config_params
      assert_response :success

      config = @org.reload.configs.first
      assert config.name, @config_params[:name]
      assert config.category, @config_params[:category]
      assert config.value, @config_params[:value]
    end

    test 'updates config properly' do
      create :config, configable: @org

      assert @org.reload.configs.first

      @config_params.merge!(name: 'V2')
      post :save, @config_params
      assert_response :success

      config = @org.configs.first
      assert config.name, 'V2'
    end

    test 'unauthorized for invalid api_token when saving config' do
      @config_params.merge!(address: 'bad.addr.com')

      jwt_token = JWT.encode(@config_params, @secret,'HS256')
      request.headers['X-API-KEY'] = @api_key
      request.headers['X-API-TOKEN'] = "somerandomtoken"

      post :save, @config_params
      assert_response :unauthorized
    end

    test 'destroys mailer config properly' do
      cfg = create :config, @config_params.merge!(configable: @org)
      assert_difference -> {Config.count}, -1 do
        delete :destroy, {name: cfg.name}, format: :json
      end
    end
  end

  class WithAppId < ActionController::TestCase
    include Devise::Test::ControllerHelpers
    setup do
      skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'

      @org = create(:organization, savannah_app_id: 1)

      @config_params = {
          category: "mail",
          name: "test",
          value: "123"}


      jwt_token = JWT.encode(@config_params, Settings.savannah_token,'HS256')
      request.headers['X-SAVANNAH-APP-ID'] = 1
      request.headers['X-API-AUTH-TOKEN'] = jwt_token
    end

    test 'creates config properly' do
      post :save, @config_params
      assert_response :success

      config = @org.reload.configs.first
      assert config.name, @config_params[:name]
      assert config.category, @config_params[:category]
      assert config.value, @config_params[:value]
    end

    test 'updates config properly' do
      create :config, configable: @org

      assert @org.reload.configs.first

      @config_params.merge!(name: 'V2')
      post :save, @config_params
      assert_response :success

      config = @org.configs.first
      assert config.name, 'V2'
    end

    test 'unauthorized for invalid api_token when saving config' do
      @config_params.merge!(address: 'bad.addr.com')

      jwt_token = JWT.encode(@config_params, Settings.savannah_token,'HS256')
      request.headers['X-API-KEY'] = @api_key
      request.headers['X-SAVANNAH-APP-ID'] = 32
      request.headers['X-API-AUTH-TOKEN'] = "somerandomtoken"

      post :save, @config_params
      assert_response :unauthorized
    end

    test 'destroys mailer config properly' do
      cfg = create :config, @config_params.merge!(configable: @org)
      assert_difference -> {Config.count}, -1 do
        delete :destroy, {name: cfg.name}, format: :json
      end
    end
  end
end

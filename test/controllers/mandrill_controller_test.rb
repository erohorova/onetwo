require 'test_helper'

class MandrillControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  test "should verify signature only on post" do
    get :get_blacklist
    assert_response :success

    post :blacklist
    assert_response :forbidden
  end

  test "should verify request for blacklisting" do
    payload = {"mandrill_events"=>"[{\"msg\": {\"email\":\"blacklist\",\"sender\":\"sender@gmail.com\"},\"ts\":1416202141}]"}
    signature = generate_signature payload
    @request.env['HTTP_X_MANDRILL_SIGNATURE'] = signature
    post :blacklist, payload
    assert_response :success

    @request.env['HTTP_X_MANDRILL_SIGNATURE'] = 'bad signature'
    post :blacklist, payload
    assert_response :forbidden
  end

  test "should blacklist email and retain user email" do
    user = create(:user, email: 'bademail22@example.com')
    email = create(:email, user: user, email: 'bademail22@example.com', confirmed: true)
    payload = {"mandrill_events"=>"[{\"msg\": {\"email\":\"bademail22@example.com\",\"sender\":\"sender@gmail.com\"},\"ts\":1416202331}]"}
    signature = generate_signature payload
    @request.env['HTTP_X_MANDRILL_SIGNATURE'] = signature
    post :blacklist, payload
    email.reload
    user.reload
    assert_response :success
    assert email.bounced_at
    assert_not_nil user.email
  end

  test "should blacklist email" do
    user = create(:user, email: "goodemail@example.com")
    email = create(:email, user: user, email: "bademail1@example.com", confirmed: true)
    payload = {"mandrill_events"=>"[{\"msg\": {\"email\":\"bademail1@example.com\",\"sender\":\"sender@gmail.com\"},\"ts\":1416202331}]"}
    signature = generate_signature payload
    @request.env['HTTP_X_MANDRILL_SIGNATURE'] = signature
    post :blacklist, payload
    email.reload
    user.reload
    assert_response :success
    assert_not_nil user.email
    assert email.bounced_at
  end

  test "should remove user email" do
    user = create(:user, email: "bademail@example.com")

    payload = {"mandrill_events"=>"[{\"msg\": {\"email\":\"bademail@example.com\",\"sender\":\"sender@gmail.com\"},\"ts\":1416202331}]"}
    signature = generate_signature payload
    @request.env['HTTP_X_MANDRILL_SIGNATURE'] = signature

    post :blacklist, payload
    user.reload
    assert_response :success
    assert_not_nil user.email
  end

  test "should create email if doesnt exist" do
    user = create(:user, email: "blacklist@example.com")
    payload = {"mandrill_events"=>"[{\"msg\": {\"email\":\"blacklist@example.com\",\"sender\":\"sender@gmail.com\"},\"ts\":1416202331}]"}
    signature = generate_signature payload
    @request.env['HTTP_X_MANDRILL_SIGNATURE'] = signature

    assert_difference -> { Email.count } do
      post :blacklist, payload
      assert_response :success
    end

    user.reload
    email = Email.where(user: user).first
    assert email.bounced_at
  end

  test "should log error if email creation fails" do
    user = create(:user, email: "blacklist@example.com")

    payload = {"mandrill_events"=>"[{\"msg\": {\"email\":\"blacklist@example.com\",\"sender\":\"sender@gmail.com\"},\"ts\":1416202331}]"}
    signature = generate_signature payload
    @request.env['HTTP_X_MANDRILL_SIGNATURE'] = signature

    Email.any_instance.expects(:save).returns(false)
    log.expects(:error).with("Couldnt save bounced_at email for user: #{user.id} with email: blacklist@example.com")
    assert_no_difference -> { Email.count } do
      post :blacklist, payload
      assert_response :success
    end
    user.reload
    email = Email.where(user: user).first
    assert_nil email
  end

  test "should use custom webhook_keys if available" do
    user = create(:user, email: "bademail@example.com")
    mc= create :mailer_config
    payload = {"mandrill_events"=>"[{\"msg\": {\"email\":\"bademail@example.com\",\"sender\":\"#{mc.from_address}\"},\"ts\":1416202331}]"}
    signature = generate_signature payload, mc: mc.id
    @request.env['HTTP_X_MANDRILL_SIGNATURE'] = signature

    post :blacklist, payload
    user.reload
    assert_response :success
    email = Email.where(user: user).first
    assert email.bounced_at
  end

  private
  def generate_signature params, mc: nil
    if mc
      conf = MailerConfig.find(mc)
      key = conf.webhook_keys.split(',').first
      sender = conf.from_address
    else
      key = Settings.mandrill.webhook_keys.first
      sender = 'sender@gmail.com'
    end

    event = JSON.parse(params['mandrill_events']).first
    email = event['msg']['email']
    ts = event['ts']
    email = email.gsub(/@/, '%40')
    signed_data = "http://www.test.host/blacklist_email?mandrill_events=%5B%7B%22msg%22%3A+%7B%22email%22%3A%22#{email}%22%2C%22sender%22%3A%22#{sender.gsub(/@/, '%40')}%22%7D%2C%22ts%22%3A#{ts}%7D%5D"
    signed_data += params.sort.join
    Base64.strict_encode64(OpenSSL::HMAC.digest('sha1', key, signed_data))
  end
end

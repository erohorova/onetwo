require 'test_helper'

class InvitationsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  class OrganizationInvitationsTest < ActionController::TestCase
    include Devise::Test::ControllerHelpers
    setup do
      Invitation.any_instance.stubs :send_mail

      @organization = create(:organization)
      @admin_user = create(:user, organization: @organization, organization_role: '@admin')
      set_host @organization
      stub_request(:post, "https://api.branch.io/v1/url").
          to_return(:status => 200, :body => "{\"url\":\"http://branch.io\"}", :headers => {})
    end

    test 'should create an invitation by email' do
      MultiInvitationsJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later

      # create_default_org
      sign_in @admin_user
      email = generate(:email_address)
      assert_difference ->{Invitation.count} do
        post :create, email: email, invitable_type: @organization.class.name.camelcase(:lower), invitable_id: @organization.id, roles: 'admin', format: :json
      end

      assert_no_difference ->{Invitation.count} do
        post :create, email: email, invitable_type: @organization.class.name.camelcase(:lower), invitable_id: @organization.id, roles: 'admin', format: :json
      end
    end

    test 'should not create an invitation by email if it is already invited by other user' do
      MultiInvitationsJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later

      # create_default_org
      sign_in @admin_user
      email = generate(:email_address)
      assert_difference ->{Invitation.count} do
        post :create, email: email, invitable_type: @organization.class.name.camelcase(:lower), invitable_id: @organization.id, roles: 'admin', format: :json
      end
      sign_out @admin_user

      admin_user2 = create(:user, organization: @organization, organization_role: 'admin')
      sign_in admin_user2

      assert_no_difference ->{Invitation.count} do
        post :create, email: email, invitable_type: @organization.class.name.camelcase(:lower), invitable_id: @organization.id, roles: 'admin', format: :json
      end
    end

    test 'should not create an invitation by email if it already exists' do
      # create_default_org
      MultiInvitationsJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later

      sign_in @admin_user
      email = generate(:email_address)
      assert_difference ->{Invitation.count} do
        post :create, email: email, invitable_type: @organization.class.name.camelcase(:lower), invitable_id: @organization.id, roles: 'admin', format: :json
      end

      admin_user = create(:user, organization: @organization, organization_role: 'admin')

      assert_no_difference ->{Invitation.count} do
        post :create, email: admin_user.email, invitable_type: @organization.class.name.camelcase(:lower), invitable_id: @organization.id, roles: 'admin', format: :json
      end

      assert_match /Invites or users already exists for these emails: #{admin_user.email}/, get_json_response(response)['error']
    end

    test 'remove a pending invitation' do
      # create_default_org
      sign_in @admin_user
      invitation = create(:invitation, recipient_email: generate(:email_address),
                          first_name: 'trung', last_name: 'pham', invitable: @organization, roles: 'admin')

      assert_difference ->{Invitation.count}, -1 do
        delete :destroy, id: invitation.id, format: :json
      end
    end

    test 'invite new user as member' do
      MultiInvitationsJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later
      # create_default_org
      # user is member
      random_user = create(:user, organization: @organization)
      sign_in random_user
      email = generate(:email_address)
      assert_difference ->{Invitation.count} do
        post :create, email: email, invitable_type: @organization.class.name.camelcase(:lower), invitable_id: @organization.id, roles: 'admin', format: :json
      end
      assert_response :success
      sign_out random_user
    end

    test 'should get 403 for non org member' do
      # user not in @organization
      sign_in create(:user)
      email = generate(:email_address)
      assert_no_difference ->{Invitation.count} do
        post :create, email: email, invitable_type: @organization.class.name.camelcase(:lower), invitable_id: @organization.id, roles: 'admin', format: :json
        assert_response :forbidden
      end
    end

    test 'delete existing invitation as member' do
      # create_default_org
      random_user = create(:user, organization: @organization)
      sign_in random_user
      invitation = create(:invitation, recipient_email: generate(:email_address),
                          first_name: 'trung', last_name: 'pham', invitable: @organization, roles: 'admin')

      assert_difference ->{Invitation.count}, -1 do
        delete :destroy, id: invitation.id, format: :json
      end
    end

    test 'get a list of pending invitations' do
      # create_default_org
      sign_in @admin_user
      (1..20).to_a.each do
        create(:invitation, recipient_email: generate(:email_address),
               first_name: 'trung', last_name: 'pham', invitable: @organization, roles: 'admin')
      end
      get :index, invitable_type: 'organization', invitable_id: @organization.id,
          limit: 5, offset: 10, roles: 'admin', format: :json
      assert_response :success
      json_response = get_json_response(response)
      assert_equal 5, json_response['invitations'].length
      assert_equal 20, json_response['total_count']

      get :index, invitable_type: 'organization', invitable_id: @organization.id,
          limit: 5, offset: 10, roles: 'member', format: :json
      assert_response :success
      json_response = get_json_response(response)
      assert_equal 0, json_response['invitations'].length
      assert_equal 0, json_response['total_count']

      invitation = create(:invitation, recipient_email: generate(:email_address), invitable: @organization, roles: 'admin')
      get :index, invitable_type: 'organization', invitable_id: @organization.id, roles: 'admin', search_term: invitation.recipient_email, format: :json
      assert_response :success
      json_response = get_json_response(response)
      assert_equal 1, json_response['invitations'].length
      assert_equal 1, json_response['total_count']
      assert_equal invitation.id, json_response['invitations'].first["id"]
    end

    test 'resend invitation' do
      # create_default_org
      sign_in @admin_user
      invitation = create(:invitation, recipient_email: generate(:email_address),
                          first_name: 'trung', last_name: 'pham', invitable: @organization, roles: 'admin', sender: @admin_user)

      assert_no_difference ->{Invitation.count} do
        post :resend, id: invitation.id, format: :json
      end
      assert_response :success
    end

    test 'should invite by handle' do
      skip 'disabled'
      sign_in @admin_user
      user = create(:user)
      assert_difference ->{Invitation.count} do
        post :create, handle: user.handle, invitable_type: @organization.class.name.camelcase(:lower), invitable_id: @organization.id, roles: 'admin', format: :json
      end
    end

    test 'should upload bulk file' do
      EdcastActiveJob.unstub :perform_later
      BulkInvitationsJob.unstub :perform_later
      # create_default_org
      csv_rows = <<-FILE
FirstName1,LastName1,name1@example.com
FirstName2,LastName2,name2@example.com
FirstName3,LastName3,name3@example.com
      FILE

      file = Tempfile.new('new_users.csv')
      file.write(csv_rows)
      file.rewind

      invalid_csv_rows = <<-FILE
oneperson@example.com
"2person@example.com",
""
  "3person@example.com"
,4person@example.com
,5person@example.com,
,
6person@example.com
      FILE

      invalid_file = Tempfile.new('new_users.csv')
      invalid_file.write(invalid_csv_rows)
      invalid_file.rewind

      sign_in @admin_user

      request.env["HTTP_REFERER"] = '/'

      last_invitation_file = nil
      assert_difference ->{InvitationFile.count} do
        post :upload,
             invitable_type: @organization.class.name.camelcase(:lower),
             invitable_id: @organization.id, roles: 'admin', file: Rack::Test::UploadedFile.new(file, 'text/csv')

        last_invitation_file = InvitationFile.last
        assert_equal @admin_user, last_invitation_file.sender
        assert_equal csv_rows, last_invitation_file.data

      end

      assert_difference ->{Invitation.count}, 3 do
        last_invitation_file.process_invitations
      end


      assert_no_difference ->{InvitationFile.count} do
        post :upload,
             invitable_type: @organization.class.name.camelcase(:lower),
             invitable_id: @organization.id, roles: 'admin', file: Rack::Test::UploadedFile.new(invalid_file, 'text/csv')
      end

       invalid_csv_rows = <<-FILE
oneperson1@example.com
"2perso1n@example.com",

      FILE
      file = Tempfile.new('new_users.csv')
      file.write(invalid_csv_rows)
      file.rewind

       assert_difference ->{InvitationFile.count} do
        post :upload,
             invitable_type: @organization.class.name.camelcase(:lower),
             invitable_id: @organization.id, file: Rack::Test::UploadedFile.new(file, 'text/csv')

        last_invitation_file = InvitationFile.last
        assert_equal "member", last_invitation_file.roles

      end
    end

    test 'should send multiple invites' do
      # create_default_org
      emails = ['abc@example.com', 'pqr@example.com', 'xyz@example.com']
      sign_in @admin_user

      MultiInvitationsJob.expects(:perform_later).with(
        emails: emails,
        sender_id: @admin_user.id,
        invitable_id: @organization.id,
        invitable_type: 'Organization',
        roles: 'admin'
      ).once
      post :create, email: emails, invitable_type: @organization.class.name.camelcase(:lower), invitable_id: @organization.id, roles: 'admin', format: :json
      assert_response :success
    end

    test 'should return error for already sent multiple invites' do
      # create_default_org
      i1 = FactoryGirl.create(:invitation, invitable:  @organization, roles: 'admin')
      i2 = FactoryGirl.create(:invitation, invitable:  @organization, roles: 'admin')

      emails = [i1.recipient_email, i2.recipient_email, 'xyz@example.com']
      sign_in @admin_user

      MultiInvitationsJob.expects(:perform_later).with(
        emails: ['xyz@example.com'],
        sender_id: @admin_user.id,
        invitable_id: @organization.id,
        invitable_type: 'Organization',
        roles: 'admin'
      ).once
      post :create, email: emails, invitable_type: @organization.class.name.camelcase(:lower), invitable_id: @organization.id, roles: 'admin', format: :json

      assert_equal(
        "Invites or users already exists for these emails: #{i1.recipient_email}, #{i2.recipient_email}",
         JSON.parse(response.body)['error']
      )
    end

    test 'should not support invalid invitable type' do
      # create_default_org
      sign_in @admin_user
      card = create(:card)
      get :index, invitable_type: 'card', invitable_id: card.id,
          limit: 5, offset: 10, roles: 'admin', format: :json
      assert_response :unprocessable_entity
    end
  end
end

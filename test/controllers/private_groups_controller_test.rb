require 'test_helper'

class PrivateGroupsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    create_default_org
    @client = create(:client)
    @org = create(:organization, client: @client)
    @api_key = @client.valid_credential.api_key
    @shared_secret = @client.valid_credential.shared_secret
    @resource_id = SecureRandom.hex
    @timestamp = Time.now.to_i
    @user_id = '123'
    @user = create(:user)
    ClientsUser.create!(user: @user, client: @client, client_user_id: @user_id)
    token = make_token(email: nil, user_id: @user_id, role: 'student')
    @base_params = {api_key: @api_key, resource_id: @resource_id, resource_name: 'test name',
                resource_description: 'test description',
                parent_url: 'http://localhost/ui/demo?edcast_ref=1&group=first',
                resource_url: 'http://path/to/resource',
                user_id: @user_id,
                token: token,
                timestamp: @timestamp,
                user_role: 'student',
                format: 'json'}
  end

  def make_token(email: nil, user_id: nil, role: nil)
    parts = [@shared_secret, @timestamp, user_id, email, role, @resource_id].compact

    Digest::SHA256.hexdigest(parts.join('|'))
  end

  test ':api group creator can delete private group' do
    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id)
    pg = create(:private_group, organization: @org, resource_group: resource_group, creator: @user)
    pg_id = pg.id
    delete :destroy, {id: pg.id}.merge(@base_params)
    assert_response :success

    assert_nil PrivateGroup.find_by_id(pg_id)
  end

  test ':api group admin can delete private group' do
    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id)
    creator = create(:user)
    pg = create(:private_group, organization: @org, resource_group: resource_group, creator: creator)
    pg_id = pg.id
    pg.add_admin(@user)

    delete :destroy, {id: pg.id}.merge(@base_params)
    assert_response :success

    assert_nil PrivateGroup.find_by_id(pg_id)
  end

  test ':api group instructor can delete private group' do
    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id)
    creator = create(:user)
    pg = create(:private_group, organization: @org, resource_group: resource_group, creator: creator)
    pg_id = pg.id
    resource_group.add_admin(@user)

    delete :destroy, {id: pg.id}.merge(@base_params)
    assert_response :success

    assert_nil PrivateGroup.find_by_id(pg_id)
  end

  test ':api group member cannot delete private group' do
    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id)
    creator = create(:user)
    pg = create(:private_group, organization: @org, resource_group: resource_group, creator: creator)
    pg_id = pg.id
    pg.add_member(@user)

    delete :destroy, {id: pg.id}.merge(@base_params)
    assert_response :forbidden

    assert_not_nil PrivateGroup.find_by_id(pg_id)
  end

  test ':api get a list of logged in users private groups' do
    resource_group1 = create(:resource_group)
    resource_group2 = create(:resource_group, client_resource_id: 'some other forum')

    g1 = create(:private_group, organization: @org, name: 'pg1', resource_group: resource_group1, access: 'open')
    g2 = create(:private_group, organization: @org, name: 'pg2', resource_group: resource_group1, access: 'open')
    g3 = create(:private_group, organization: @org, name: 'pg3', resource_group: resource_group1, access: 'invite')
    g4 = create(:private_group, organization: @org, name: 'pg4', resource_group: resource_group1, access: 'invite')
    g1.add_member @user
    g3.add_member @user

    g5 = create(:private_group, organization: @org, name: 'pg5', resource_group: resource_group2, access: 'invite')
    g6 = create(:private_group, organization: @org, name: 'pg6', resource_group: resource_group2, access: 'invite')
    g5.add_member @user
    g6.add_member @user

    get :index, {id: resource_group1.id}.merge(@base_params)
    json_response = get_json_response response
    enrolled_groups = json_response['enrolled_groups']

    assert_equal(2, enrolled_groups.length)
    assert_equal [g1.id, g3.id], enrolled_groups.collect{|g| g['id']}.sort
  end

  test 'enrolled_groups and open groups of user' do
      resource_group1 = create(:resource_group, organization: @org)
      user_grps = create_list(:private_group, 7, organization: @org, resource_group: resource_group1, access: 'open')
      user_grps.map{|g| g.add_member(@user)}
      open_grps = create_list(:private_group, 7, organization: @org, resource_group: resource_group1, access: 'open')
      get :index, {id: resource_group1.id, type: "my_groups"}.merge(@base_params)
      assert_response :ok
      json_response = get_json_response response
      enrolled_groups = json_response['enrolled_groups']
      assert_equal(7, enrolled_groups.length)

      get :index, {id: resource_group1.id, type: "open"}.merge(@base_params)
      json_response = get_json_response response
      open_groups = json_response['open_groups']
      assert_equal(7, open_groups.length)
  end

  test 'pagination for enrolled_groups of user' do
    resource_group1 = create(:resource_group)
    user_grps = create_list(:private_group, 7, organization: @org, resource_group: resource_group1, access: 'open')
    user_grps.map{|g| g.add_member(@user)}
    open_grps = create_list(:private_group, 5, organization: @org, resource_group: resource_group1, access: 'open')
    get :enrolled_groups, {id: resource_group1.id, limit: 3}.merge(@base_params)
    json_response = get_json_response response
    enrolled_groups = json_response['enrolled_groups']
    assert_equal(3, enrolled_groups.length)
  end

  test 'pagination for open groups' do
    resource_group1 = create(:resource_group)
    user_grps = create_list(:private_group, 7, organization: @org, resource_group: resource_group1, access: 'open')
    user_grps.map{|g| g.add_member(@user)}
    open_grps = create_list(:private_group, 5, organization: @org, resource_group: resource_group1, access: 'open')

    get :open_groups, {id: resource_group1.id, limit: 3}.merge(@base_params)
    json_response = get_json_response response
    open_groups = json_response['open_groups']
    assert_equal(3, open_groups.length)
  end


  test ':api get a list of puublic groups logged in user is not a member' do
    resource_group1 = create(:resource_group)
    resource_group2 = create(:resource_group, client_resource_id: 'some other forum')

    g1 = create(:private_group, organization: @org, name: 'pg1', resource_group: resource_group1, access: 'open')
    g2 = create(:private_group, organization: @org, name: 'pg2', resource_group: resource_group1, access: 'open')
    g3 = create(:private_group, organization: @org, name: 'pg3', resource_group: resource_group1, access: 'invite')
    g4 = create(:private_group, organization: @org, name: 'pg4', resource_group: resource_group1, access: 'invite')
    g1.add_member @user
    g3.add_member @user

    g5 = create(:private_group, organization: @org, name: 'pg5', resource_group: resource_group2, access: 'invite')
    g6 = create(:private_group, organization: @org, name: 'pg6', resource_group: resource_group2, access: 'invite')
    g6.add_member @user

    get :index, {id: resource_group1.id}.merge(@base_params)
    json_response = get_json_response response
    open_groups = json_response['open_groups']

    assert_equal(1, open_groups.length)
    assert_equal [g2.id], open_groups.collect{|g| g['id']}.sort
  end

  test ':api show a single group detail' do

    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id)

    g1 = create(:private_group, organization: @org, resource_group: resource_group, creator: @user)
    u1 = create(:user)
    u2 = create(:user)
    g1.add_user user: u1, role: 'member'
    g1.add_user user: u2, role: 'member'
    get :show, {id: g1.id}.merge(@base_params)
    assert_response :success
    json_response = get_json_response response
    assert_equal g1.name, json_response['name']
    assert_equal 3, json_response['users_count']
  end


  test ':api show all the users in this group' do
    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id)

    g1 = create(:private_group, organization: @org, resource_group: resource_group, creator: @user)
    u1 = create(:user)
    u2 = create(:user)
    u3 = create(:user)
    g1.add_user user: u1, role: 'member'
    g1.add_user user: u2, role: 'member'
    g1.add_user user: u3, role: 'admin'
    get :users, {id: g1.id}.merge(@base_params)
    assert_response :success
    json_response = get_json_response response
    assert_equal 4, json_response.length
    assert_equal 'admin', json_response.last['roles'][g1.id.to_s]
    assert_equal u3.name, json_response.last['name']
    assert_equal u3.photo, json_response.last['photo']
    assert_equal false, json_response.last['banned'][g1.id.to_s]
  end

  test ':api create group' do
    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id, creator: @user)
    resource_group.add_member @user

    really_long_name = 'Discussion of what a globally acceptable climate treaty should be like Discussion of what a globally acceptable climate treaty should be like Discussion of what a globally acceptable climate treaty should be like Discussion of what a globally acceptable climate treaty should be like'
    post :create, {id: resource_group.id, name: really_long_name, access: 'open'}.merge(@base_params)
    assert_response :success
    assert_equal JSON.parse(response.body)["isAdmin"], false
    group = Group.last
    assert_equal really_long_name.truncate(255, separator: ' '), group.name
    assert_equal 'open', group.access
    assert_equal @org, group.organization
    assert group.is_admin?(group.creator), 'Creator should be added as the group admin'
  end

  test ':api private group creator should be resource group user' do
    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id)

    user = create(:user)
    ClientsUser.create!(user: user, client: @client, client_user_id: 789)
    token = make_token(email: nil, user_id: 789, role: 'student')
    params = @base_params.merge({id: resource_group.id, name: 'test group 77', access: 'open', user_id: 789, token: token})

    post :create, params
    assert_response :forbidden
    resource_group.reload
    assert_equal 0, resource_group.private_groups.count

    resource_group.add_member user
    post :create, params
    assert_response :success
    resource_group.reload
    assert_equal 1, resource_group.private_groups.count

    group = resource_group.private_groups.first!
    assert_equal 'test group 77', group.name
    assert_equal 'open', group.access
    assert_equal @org, group.organization
  end

  test ':api update group' do
    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id)
    group = create(:private_group, organization: @org, name: 'test group', resource_group: resource_group, creator: @user)

    patch :update, {id: group.id, name: 'new name', access: 'invite'}.merge(@base_params)
    assert_response :success
    group.reload
    assert_equal 'new name', group.name
    assert_equal 'invite', group.access

  end

  test 'members cannot update group' do
    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id)
    creator = create(:user)
    group = create(:private_group, organization: @org, name: 'test group', resource_group: resource_group, creator: creator)
    group.add_member(@user)

    patch :update, {id: group.id, name: 'new name', access: 'invite'}.merge(@base_params)
    assert_response :forbidden
    group.reload
    assert_not_equal 'new name', group.name
    assert_not_equal 'invite', group.access

    group.add_admin(@user)

    patch :update, {id: group.id, name: 'new name', access: 'invite'}.merge(@base_params)
    assert_response :success
    group.reload
    assert_equal 'new name', group.name
    assert_equal 'invite', group.access
  end

  test 'cannot change creator role' do
    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id)
    creator = create(:user)
    group = create(:private_group, organization: @org, name: 'test group', resource_group: resource_group, creator: creator)
    group.add_admin(@user)
    post :add_user, {id: group.id, member_id: creator.id ,role: 'member'}.merge(@base_params)
    assert_response :forbidden
    group.reload
    assert group.is_admin?(creator)
  end

  test 'non-members cannot add new users' do
    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id)
    creator = create(:user)
    group = create(:private_group, organization: @org, name: 'test group', resource_group: resource_group, creator: creator)
    user = create(:user)
    post :add_user, {id: group.id, member_id: user.id ,role: 'member'}.merge(@base_params)
    assert_response :forbidden
    group.reload
    assert_not group.is_user?(user)
  end

  test 'only admins can change existing member roles' do
    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id)
    creator = create(:user)
    group = create(:private_group, organization: @org, name: 'test group', resource_group: resource_group, creator: creator)

    user = create(:user)

    group.add_member(user)
    post :add_user, {id: group.id, member_id: user.id ,role: 'admin'}.merge(@base_params)
    assert_response :forbidden
    group.reload
    assert_not group.is_admin?(user)
    group.add_admin(user)
    post :add_user, {id: group.id, member_id: user.id ,role: 'member'}.merge(@base_params)
    assert_response :forbidden
    group.reload
    assert_not group.is_member?(user)

    group.add_member(@user)
    group.add_member(user)
    post :add_user, {id: group.id, member_id: user.id ,role: 'admin'}.merge(@base_params)
    assert_response :forbidden
    group.reload
    assert_not group.is_admin?(user)
    group.add_admin(user)
    post :add_user, {id: group.id, member_id: user.id ,role: 'member'}.merge(@base_params)
    assert_response :forbidden
    group.reload
    assert_not group.is_member?(user)

    group.add_admin(@user)
    group.add_member(user)
    post :add_user, {id: group.id, member_id: user.id ,role: 'admin'}.merge(@base_params)
    assert_response :success
    group.reload
    assert group.is_admin?(user)
    post :add_user, {id: group.id, member_id: user.id ,role: 'member'}.merge(@base_params)
    assert_response :success
    group.reload
    assert group.is_member?(user)
  end

  test 'only admin can create new admins' do
    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id)
    creator = create(:user)
    group = create(:private_group, organization: @org, name: 'test group', resource_group: resource_group, creator: creator)

    user = create(:user)

    post :add_user, {id: group.id, member_id: user.id ,role: 'admin'}.merge(@base_params)
    assert_response :forbidden
    group.reload
    assert_not group.is_admin?(user)
    assert_not group.is_user?(user)

    group.add_member(@user)
    post :add_user, {id: group.id, member_id: user.id ,role: 'admin'}.merge(@base_params)
    assert_response :forbidden
    group.reload
    assert_not group.is_admin?(user)
    assert_not group.is_user?(user)

    group.add_admin(@user)
    post :add_user, {id: group.id, member_id: user.id ,role: 'admin'}.merge(@base_params)
    assert_response :success
    group.reload
    assert group.is_admin?(user)
  end

  test ':api group members can add others to the group' do
    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id)
    creator = create(:user)
    group = create(:private_group, organization: @org, name: 'test group', resource_group: resource_group, creator: creator)

    user = create(:user)
    group.reload
    assert_not group.is_user? user

    post :add_user, {id: group.id, member_id: user.id ,role: 'member'}.merge(@base_params)
    assert_response :forbidden
    group.reload
    assert_not group.is_user? user

    group.add_member(@user)
    post :add_user, {id: group.id, member_id: user.id ,role: 'member'}.merge(@base_params)
    assert_response :success
    group.reload
    assert group.is_member? user
  end

  test 'users can add themselves to open groups only' do
    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id)

    creator = create(:user)
    group = create(:private_group, organization: @org, access: 'open', name: 'test group1', resource_group: resource_group, creator: creator)

    post :add_user, {id: group.id, member_id: @user.id ,role: 'member'}.merge(@base_params)
    assert_response :success
    group.reload
    assert group.is_user? @user

    group = create(:private_group, organization: @org, access: 'invite', name: 'test group2', resource_group: resource_group, creator: creator)

    post :add_user, {id: group.id, member_id: @user.id ,role: 'member'}.merge(@base_params)
    assert_response :forbidden
    group.reload
    assert_not group.is_user? @user
  end

  test 'non admin user cannot add an admin user to the group' do
    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id)
    creator = create(:user)
    group = create(:private_group, organization: @org, name: 'test group', resource_group: resource_group, creator: creator)

    user = create(:user)
    group.reload
    assert_not group.is_user? user

    group.add_member(@user)
    post :add_user, {id: group.id, member_id: user.id ,role: 'admin'}.merge(@base_params)
    assert_response :forbidden
    group.reload
    assert_not group.is_user? user

    group.add_admin(@user)
    post :add_user, {id: group.id, member_id: user.id ,role: 'admin'}.merge(@base_params)
    assert_response :success
    group.reload
    assert group.is_admin? user
  end

  test ':api cannot remove creator from group' do
    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id)
    creator = create(:user)
    group = create(:private_group, organization: @org, name: 'test group', resource_group: resource_group, creator: creator)

    group.add_admin(@user)
    delete :remove_user, {id: group.id, member_id: creator.id}.merge(@base_params)
    assert_response :forbidden
    group.reload
    assert group.is_admin? creator
  end

  test ':api cannot change creator admin role' do
    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id)
    creator = create(:user)
    group = create(:private_group, organization: @org, name: 'test group', resource_group: resource_group, creator: creator)

    group.add_admin(@user)
    post :add_user, {id: group.id, member_id: creator.id ,role: 'member'}.merge(@base_params)
    assert_response :forbidden
    group.reload
    assert group.is_admin? creator
  end

  test ':api only admin user and self can remove user from group' do
    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id)
    creator = create(:user)
    group = create(:private_group, organization: @org, name: 'test group', resource_group: resource_group, creator: creator)

    user = create(:user)
    group.add_member(user)

    # @user, as a member, trying to remove 'user' - should fail
    group.add_member(@user)
    delete :remove_user, {id: group.id, member_id: user.id}.merge(@base_params)
    assert_response :forbidden
    group.reload
    assert group.is_member? user

    # @user, as an admin, trying to remove 'user' - should pass
    group.add_admin(@user)
    delete :remove_user, {id: group.id, member_id: user.id}.merge(@base_params)
    assert_response :success
    group.reload
    assert_not group.is_user? user

    # @user, as a member, trying to remove self - should pass
    group.add_member(@user)
    delete :remove_user, {id: group.id, member_id: @user.id}.merge(@base_params)
    assert_response :success
    group.reload
    assert_not group.is_member? @user

    # already deleted. Another request should send ok
    delete :remove_user, {id: group.id, member_id: @user.id}.merge(@base_params)
    assert_response :success
    group.reload
    assert_not group.is_member?(@user)
  end

  test ':api only admin user can ban user from group' do
    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id)
    creator = create(:user)
    group = create(:private_group, organization: @org, name: 'test group', resource_group: resource_group, creator: creator)

    user = create(:user)
    group.add_member(user)

    group.add_member(@user)
    post :ban_user, {id: group.id, member_id: user.id}.merge(@base_params)
    assert_response :forbidden
    group.reload
    assert group.is_active_user? user

    group.add_admin(@user)
    post :ban_user, {id: group.id, member_id: user.id}.merge(@base_params)
    assert_response :success
    group.reload
    assert_not group.is_active_user? user
  end

  test ':api only admin user can unban user to the group' do
    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id)
    creator = create(:user)
    group = create(:private_group, organization: @org, name: 'test group', resource_group: resource_group, creator: creator)

    user = create(:user)
    group.add_member(user)
    group.ban_user(user)

    group.add_member(@user)
    delete :unban_user, {id: group.id, member_id: user.id}.merge(@base_params)
    assert_response :forbidden
    group.reload
    assert_not group.is_active_user? user

    group.add_admin(@user)
    delete :unban_user, {id: group.id, member_id: user.id}.merge(@base_params)
    assert_response :success
    group.reload
    assert group.is_active_user? user
  end

  test ':api cannot ban group creator' do
    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id)
    creator = create(:user)
    group = create(:private_group, organization: @org, name: 'test group', resource_group: resource_group, creator: creator)

    group.add_admin(@user)
    post :ban_user, {id: group.id, member_id: creator.id}.merge(@base_params)
    assert_response :forbidden
    group.reload
    assert group.is_active_user? creator
  end

  test 'should email new users added to private groups' do
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(false)
    resource_group = create(:resource_group, organization: @org, client_resource_id: @resource_id)
    creator = create(:user)
    group = create(:private_group, organization: @org, name: 'test group', resource_group: resource_group, creator: creator)

    group.add_admin(@user)
    user = create(:user)
    MandrillMailer.any_instance.expects(:send_api_email).once
    post :add_user, {id: group.id, member_id: user.id ,role: 'member'}.merge(@base_params)
    assert_response :success
  end

  test '#search fetch both enrolled_groups & open_groups' do
    WebMock.disable!
    resource_group1 = create(:resource_group)
    resource_group2 = create(:resource_group)

    g1 = create(:private_group, organization: @org, name: 'pg1', resource_group: resource_group1, access: 'open')
    g2 = create(:private_group, organization: @org, name: 'pg2', resource_group: resource_group1, access: 'open')
    g3 = create(:private_group, organization: @org, name: 'pg3', resource_group: resource_group1, access: 'invite')
    g4 = create(:private_group, organization: @org, name: 'pg4', resource_group: resource_group1, access: 'invite')

    g5 = create(:private_group, organization: @org, name: 'pg5', resource_group: resource_group2, access: 'open')
    g6 = create(:private_group, organization: @org, name: 'pg6', resource_group: resource_group2, access: 'open')

    g1.add_member @user
    g3.add_member @user
    g5.add_member @user
    g6.add_member @user

    @user.reload
    PrivateGroup.reindex
    PrivateGroup.searchkick_index.refresh

    # fetches searched results matching `:q`
    get :search, { id: resource_group1.id, q: 'pg' }.merge(@base_params)
    results = get_json_response(response)
    assert_equal results['enrolled_groups_count'], 2
    assert_same_ids results['enrolled_groups'].collect { |g| g['id'] }, [g1.id, g3.id], ordered: false
    assert_same_ids results['open_groups'].collect { |g| g['id'] }, [g2.id], ordered: false
    assert_equal results['open_groups_count'], 1

    # fetches searched results non-matching `:q`
    get :search, { id: resource_group1.id, q: 'no-group' }.merge(@base_params)
    results = get_json_response(response)
    assert_equal results['enrolled_groups_count'], 0
    assert_equal results['open_groups_count'], 0

    # fetches searched results matching `:q` with limit and offset
    get :search, { id: resource_group1.id, q: 'pg', limit: '1', offset: '1' }.merge(@base_params)
    results = get_json_response(response)
    assert_equal results['enrolled_groups_count'], 2
    assert_same_ids results['enrolled_groups'].collect { |g| g['id'] }, [g3.id], ordered: false
    refute_includes results['enrolled_groups'].collect { |g| g['id'] }, g1.id
    assert_empty results['open_groups']
    assert_equal results['open_groups_count'], 1

    # doesnot list groups from different resource group
    get :search, { id: resource_group1.id, q: 'pg' }.merge(@base_params)
    results = get_json_response(response)
    refute_includes results['enrolled_groups'].collect { |g| g['id'] }, g5.id
    refute_includes results['enrolled_groups'].collect { |g| g['id'] }, g6.id

    WebMock.enable!
  end
end

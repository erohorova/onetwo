require 'test_helper'

class CommentsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'

    Comment.any_instance.stubs(:record_comment_event)
    @client = create(:client)
    @user = create(:user)
    @faculty = create(:user)
    @voter = create(:user)
    @group = create(:resource_group, client_id: @client.id)
    @group.add_member @user
    @group.add_member @voter
    @group.add_admin @faculty
    request.cookies['_d'] = 'deviceid123'
    request.env['HTTP_REFERER'] = 'http://edcast.com'
  end

  def api_setup
    @question = create(:question, group: @group, user: @user)
    @answer = create(:answer, commentable: @question, user: @user)
    @resource = create(:resource)
    @tag = create(:tag)
    @vote = create(:vote, votable: @answer, voter: @voter)
    @answer.resource = @resource
    @answer.tags << @tag
    @answer.save
    @answer.reload
    @expected_answer = {'id' => @answer.id,
                        'message' => @answer.message,
                        'user' => @user.as_json(only:[:id,:first_name,:last_name, :handle]),
                        'resource' => @resource.as_json(only:[:id,:url,:title,:description,:image_url,:video_url,:type,:embed_html,
                                                              :site_name]).merge({"created_at" => @resource.created_at.strftime("%Y-%m-%dT%H:%M:%S.000Z")}),
                        'tags' => [@tag.as_json(only:[:id,:name])],
                        'mentions' => [],
                        'votes_count' => @answer.votes_count,
                        'reporting_disabled' => false,
                        'reporting_disabled_message' => '',
                        'is_staff' => false,
                        'creator_label' => @answer.creator_label,
                        'is_faculty_approved' => false,
                        'created_at' => @answer.created_at.strftime('%Y-%m-%dT%H:%M:%S.%LZ'),
                        'updated_at' => @answer.updated_at.strftime('%Y-%m-%dT%H:%M:%S.%LZ'),
                        'hidden' => false,
                        'up_voted' => false,
                        'anonymous' => false,
                        'is_mobile' => false,
                        'comments_count' => 0,
                        'file_resources' => []
                      }
    @expected_answer['user']['name'] = @user.name
    @expected_answer['user']['self'] = false
    @expected_answer['user']['picture'] = @user.photo(:medium)
    @expected_answer['user']['bio'] = @user.bio
    @expected_answer['user']['roles'] = ['member']
    @expected_answer['user']['following'] = false
    @expected_answer['user']['followers_count'] = @user.followers_count
    @expected_answer['user']['is_suspended'] = false
    score_entry = @user.score @group
    @expected_answer['user']['score'] = score_entry[:score]
    @expected_answer['user']['stars'] = score_entry[:stars]
    @expected_answer['user']['full_name'] = @user.full_name
    @expected_answer['user']['coverimages'] = @user.fetch_coverimage_urls
  end

  test 'Comment type should be Answer for Announcement comment' do
    sign_in @user
    announcement = create(:post, type: 'Announcement', group: @group, user: @user)
    post :create, {post_id: announcement.id, message: 'This is a comment on Announcement', format: :json}
    assert_equal Comment.last.type, 'Answer'
    sign_out @user
  end

  test 'comment get save in HTML markup' do
    sign_in @user
    announcement = create(:post, type: 'Announcement', group: @group, user: @user)
    post :create, {post_id: announcement.id, message: '<p>This is a comment on Announcement...</p>', format: :json}
    assert_equal Comment.last.message, '<p>This is a comment on Announcement...</p>'
    sign_out @user
  end

  test "status forbidden if user not logged in" do
    g, u = create_gu
    p = create(:post, group: g, user: u)
    c = create(:comment, commentable: p, user: u)
    get :index, {post_id: p.id, format: :json}
    assert_response :unauthorized

    get :show, {post_id: p.id, id: c.id, format: :json}
    assert_response :unauthorized

    assert_no_difference ->{Answer.count} do
      post :create, {message: 'message',post_id: p.id, format: :json}
    end
    assert_response :unauthorized

    patch :update, {:post_id => p.id, :id => c.id,
                    :message => 'm', :format => :json}
    assert_response :unauthorized

    delete :destroy, {:post_id => p.id, :id => c.id, :format => :json}
    assert_response :unauthorized
  end

  test "save answer for an existing question" do
    sign_in @user
    question = create(:question, postable: @group, user_id: @user.id)
    answer = build(:answer, commentable_id: question.id, commentable_type: 'Question')
    assert_difference -> { question.answers.count } do
      post :create, {message: answer.message,
                     post_id: answer.commentable_id,
                     type: 'Answer',
                     format: :json}
    end
    sign_out @user
  end

  test "reject answer for a non-existing question" do
    sign_in @user
    question = build(:question, group: @group, user: @user)
    answer = build(:answer, commentable: question)
    assert_no_difference -> { Answer.count } do
      post :create, { message: answer.message,
                     post_id: 'xxx',
                     :format => :json }
      assert_response :not_found
    end
    sign_out @user
  end

  test "bad save request missing message param" do
    question = create(:question, group: @group, user:@user)
    sign_in @user
    assert_no_difference -> { Answer.count } do
      post :create, {post_id: question.id, :format => :json}
      assert_response :bad_request
    end
    sign_out @user
  end

  test "bad save request empty message" do
    question = create(:question, group: @group, user: @user)
    sign_in @user
    assert_no_difference -> {Answer.count} do
      post :create, {:post_id => question.id, :message => '', :format => :json}
    end
    assert_response :unprocessable_entity
    sign_out @user
  end

  # Attach :api at the beginning. API documentation
  # should be generated from this test.
  test "should be able to update comment" do
    sign_in @user
    @card = create(:card)
    @comment = create(:answer, commentable: @card, user: @user)
    patch :update, {:card_id => @card.id, :id => @comment.id,
                    :message => 'new message edited', :format => :json}

    assert_response :success
    @comment.reload
    assert_equal @comment.message, 'new message edited'
    sign_out @user
  end

  test ":api get all answers for an existing question" do
    api_setup
    # create object and necessary associations
    u = create(:user)
    @group.add_member u
    sign_in u

    # call api and assert success
    get :index, {:post_id => @question.id, :format => :json}
    assert_response :success

    # test that the controller assigns the correct object
    answers = assigns(:comments)
    assert_not_nil answers
    assert_equal answers.count,1
    assert_equal answers[0], @answer

    # test that the JSON response returns the correct object
    response_object = get_json_response response
    assert_equal response_object.count, 1
    response_answer = response_object[0]

    assert_equal response_answer, @expected_answer
    sign_out u
  end

  test 'status unauthorized if user not in group' do
    api_setup
    u = create(:user)
    sign_in u

    get :index, { post_id: @question.id, format: :json }
    assert_response :unauthorized
    sign_out u
  end

  test "get answers for a non existing question" do
    sign_in @user
    get :index, {:post_id => 'some_random_id', :format => :json}
    assert_response :not_found
    sign_out @user
  end

  test ":api show particular answer" do
    api_setup
    u = create(:user)
    sign_in u

    get :show, {:post_id => @question.id,
                :id => @answer.id,
                :format => :json}
    assert_response :success

    # test that the controller assigns the correct object
    assigned_answer = assigns(:comment)
    assert_not_nil assigned_answer
    assert_equal assigned_answer, @answer

    # test that the JSON response returns the correct object
    response_object = get_json_response response
    response_answer = response_object
    assert_equal response_answer, @expected_answer
    sign_out u
  end

  test "show if staff answer" do
    api_setup
    sign_in @user
    ans = create(:answer, commentable: @question, user: @faculty)

    get :show, {:post_id => @question.id,
                :id => ans.id,
                :format => :json}
    assert_response :success
    response_answer = get_json_response response
    assert_equal response_answer['is_staff'], true
  end

  test "show if user has voted" do
    sign_in @user
    user2 = create(:user)
    @group.add_member user2
    question = create(:question, user: @user, group: @group)
    answer = create(:answer, commentable: question, user: user2)
    get :show, {:post_id => question.id,
                :id => answer.id,
                :format => :json}
    resp = get_json_response response
    assert_not resp['up_voted']

    self_answer = create(:answer, commentable: question, user: user2)
    create(:vote, votable: self_answer, voter: @user)
    get :show, {:post_id => question.id,
                :id => self_answer.id,
                :format => :json}
    resp = get_json_response response
    assert_equal resp['votes_count'], 1
    assert resp['up_voted']

    create(:vote, votable: answer, voter: @voter)
    create(:vote, votable: answer, voter: @user)
    get :show, {:post_id => question.id,
                :id => answer.id,
                :format => :json}
    resp = get_json_response response
    assert_equal resp['votes_count'], 2
  end

  def multiple_answer_setup
    @voter2 = create(:user)
    @group.add_member @voter2
    @question = create(:question, user: @user, group: @group)
    @ans1 = create(:answer, user: @user, commentable: @question)
    @ans2 = create(:answer, user: @user, commentable: @question)
    @ans3 = create(:answer, user: @user, commentable: @question)
    create(:vote, votable: @ans1, voter: @voter)
    create(:vote, votable: @ans1, voter: @voter2)
    create(:vote, votable: @ans2, voter: @voter)
  end

  test ':api sort answer by time, first posted first' do
    multiple_answer_setup
    @ans1.created_at = 1.days.ago
    @ans2.created_at = 2.days.ago
    @ans3.created_at = 3.days.ago
    @ans1.save
    @ans2.save
    @ans3.user = @faculty #set the user to staff
    @ans3.save
    sign_in @user
    get :index, {post_id: @question.id, format: :json}
    resp = get_json_response response
    assert_equal resp.count, 3
    assert_equal @ans3.id, resp[0]['id']
    assert_equal @ans2.id, resp[1]['id']
    assert_equal @ans1.id, resp[2]['id']
    assert_equal true, resp[0]['is_staff']
    sign_out @user
  end

  test 'get answers ordered by date, descending' do
    multiple_answer_setup
    sign_in @user
    @ans1.created_at = Time.now
    @ans1.save!
    @ans2.created_at = Time.now - 1.hour
    @ans2.save!
    @ans3.created_at = Time.now - 1.day
    @ans3.save!
    get :index, {post_id: @question.id, format: :json,
                 sort: 'date', order: 'desc'}
    resp = get_json_response response
    assert_equal resp.count, 3
    assert_equal resp[0]['id'], @ans1.id
    assert_equal resp[1]['id'], @ans2.id
    assert_equal resp[2]['id'], @ans3.id

    sign_out @user
  end

  test ':api sort answer by votes, descending' do
    multiple_answer_setup
    @ans1.created_at = 1.days.ago
    @ans2.created_at = 2.days.ago
    @ans3.created_at = 3.days.ago
    @ans1.save
    @ans2.save
    @ans3.save
    sign_in @user
    get :index, {post_id: @question.id, sort: 'votes',
                 order: 'desc', format: :json}
    resp = get_json_response response
    assert_equal resp.count, 3
    assert_equal @ans1.id, resp[0]['id']
    assert_equal @ans2.id, resp[1]['id']
    assert_equal @ans3.id, resp[2]['id']
    sign_out @user
  end


  test 'api: get the next page, accept offset, and limit, returns size+1 results' do
    @group.add_member @user
    @question = create(:question, group: @group, user: @user)
    (1..20).to_a.each do |i|
      create(:answer, message: "message #{i}", created_at: (Time.now - 1.day.ago + i), commentable: @question, user: @user)
    end
    sign_in @user
    get :index, {post_id: @question.id, offset: 7, limit: 5, format: :json}
    resp = get_json_response response
    assert 5, resp.size
    assert_equal "message 8", resp.first['message'] #skip the first 7 answers
    assert_equal "message 12", resp.last['message']
  end

  test 'get answers ordered by date, ascending' do
    multiple_answer_setup
    sign_in @user
    @ans1.created_at = Time.now
    @ans1.save!
    @ans2.created_at = Time.now - 1.hour
    @ans2.save!
    @ans3.created_at = Time.now - 1.day
    @ans3.save!

    # ans3 and ans2 have same number of votes, test secondary sort
    #
    create(:vote, votable: @ans3, voter: @voter)
    get :index, {post_id: @question.id, format: :json,
                 sort: 'votes', order: 'asc'}
    resp = get_json_response response
    assert_equal resp.count, 3
    assert_equal resp[0]['id'], @ans2.id
    assert_equal resp[1]['id'], @ans3.id
    assert_equal resp[2]['id'], @ans1.id

    sign_out @user
  end

  test 'bad sort request for questions' do
    sign_in @user
    get :index, {post_id: create(:question, user: @user, group: @group).id, format: :json, sort: 'coolness'}
    assert_response :unprocessable_entity
    sign_out @user
  end

  test 'bad order request for questions' do
    sign_in @user
    get :index, {post_id: create(:question, user: @user, group: @group).id, format: :json, order: 'backwards'}
    assert_response :unprocessable_entity
    sign_out @user
  end

  test "404 if answer doesn't exist" do
    sign_in @user
    get :show, {:post_id => 'non_existent_question',
                :id => 1, :format => :json}
    assert_response :not_found

    question = create(:question, user: @user, group: @group)

    get :show, {:post_id => question.id,
                :id => 1, :format => :json}
    assert_response :not_found
    sign_out @user
  end

  test "should not show reported answers" do
    q = create(:question, group: @group, user: @user)
    a1 = create(:answer, commentable: q, user: @user)
    a2 = create(:answer, commentable: q, user: @user)
    a3 = create(:answer, commentable: q, user: @user)

    r1 = create(:user)
    r2 = create(:user)
    @group.add_member r1
    @group.add_member r2

    create(:report, reportable: a1, reporter: r1)
    create(:report, reportable: a1, reporter: r2)
    create(:report, reportable: a2, reporter: r1)

    sign_in @user
    get :index, {:post_id => q.id, :format => :json}

    resp = get_json_response response
    assert_equal resp.count, 3
    assert resp.by_id(a1.id)['hidden']
    assert_not resp.by_id(a2.id)['hidden']
    assert_not resp.by_id(a3.id)['hidden']

    teacher = create(:user, email: 'c@c.com')
    @group.add_admin teacher
    create(:report, reportable: a3, reporter: teacher)
    get :index, {:post_id => q.id, :format => :json}

    resp = get_json_response response
    assert_equal resp.count, 3
    assert resp.by_id(a1.id)['hidden']
    assert_not resp.by_id(a2.id)['hidden']
    assert resp.by_id(a3.id)['hidden']

    sign_out @user

  end

  test 'should show answer as hidden if current user has reported' do
    q = create(:question, group: @group, user: @user)
    a1 = create(:answer, commentable: q, user: @user)
    a2 = create(:answer, commentable: q, user: @user)

    viewer = create(:user)
    @group.add_member viewer

    create(:report, reporter: viewer, reportable: a1)

    sign_in viewer
    get :index, {:post_id => q.id, :format => :json}

    resp = get_json_response response
    assert resp.by_id(a1.id)['hidden']
    assert_not resp.by_id(a2.id)['hidden']
  end

  test 'should show reporting disabled with message' do
    q = create(:question, group: @group, user: @user)
    faculty_answer = create(:answer, commentable: q, user: @faculty)
    sign_in @user
    get :show, {:post_id => q.id, :id => faculty_answer.id,
                :format => :json}
    resp = get_json_response response
    assert resp['reporting_disabled']
    assert_equal resp['reporting_disabled_message'],
      'You cannot report a faculty post'
    sign_out @user

    student_answer = create(:answer, commentable: q, user: @user)
    create(:report, reporter: @faculty, reportable: student_answer)
    sign_in @faculty
    get :show, {:post_id => q.id, :id => student_answer.id,
                :format => :json}
    resp = get_json_response response
    assert resp['reporting_disabled']
    assert_equal resp['reporting_disabled_message'],
      'Reported'
    sign_out @faculty

  end

  def update_answer_setup
    @resource = create(:resource)
    @q = create(:question, user: @user, group: @group)
    @a = create(:answer, commentable: @q,
                user: @user, message: 'old message #oldtag')
    @a.resource = @resource
    @a.save
  end

  test 'should update answer' do
    update_answer_setup
    sign_in @user

    patch :update, {:post_id => @q.id, :id => @a.id,
                    :message => 'new message #newtag', :format => :json}
    assert_response :success
    @a.reload
    assert_equal @a.message, 'new message #newtag'

    assert_equal @a.tags.count, 1
    assert_equal @a.tags[0].name, 'newtag'

    assert_nil @a.resource

    sign_out @user
  end

  test 'bad update request without message param' do
    update_answer_setup
    sign_in @user
    patch :update, {:post_id => @q.id, :id => @a.id, :format => :json}
    assert_response :bad_request
    sign_out @user
  end

  test 'bad update request with empty message' do
    update_answer_setup
    sign_in @user
    patch :update, {:post_id => @q.id, :id => @a.id, :message => '',
                    :format => :json}
    assert_response :unprocessable_entity
    assert @a.reload.message, 'old message #oldtag'
  end

  test 'user can only edit own post' do
    update_answer_setup
    u = create(:user)
    @group.add_member u

    sign_in u

    patch :update, {:post_id => @q.id, :id => @a.id,
                    :message => 'new message', :format => :json}

    assert_response :forbidden
    assert_equal @a.reload.message, 'old message #oldtag'
  end

  test 'should delete answer' do
    q = create(:question, group: @group, user: @user)
    a = create(:answer, commentable: q, user: @user)

    sign_in @user
    assert_difference ->{q.answers.count}, -1 do
      delete :destroy, {:post_id => q.id, :id => a.id, :format => :json}
    end

    sign_out @user
  end

  test 'should trigger NotificationCleanUpJob on assignment destroy' do

    e = create(:event, group: @group, user: @user)
    comment = create(:comment, commentable: e, user: @user)

    NotificationCleanUpJob.expects(:perform_later).with(id: comment.id, type: comment.class.name).once

    sign_in @user
    delete :destroy, {:event_id => e.id, :id => comment.id, :format => :json}
    sign_out @user

  end

  test 'user can only delete own post' do
    q  = create(:question, group: @group, user: @user)
    a = create(:answer, commentable: q, user: @user)

    u = create(:user)
    sign_in u

    assert_no_difference ->{q.answers.count} do
      delete :destroy, {:post_id => q.id, :id => a.id, :format => :json}
    end
    assert_response :forbidden

    sign_out u
  end

  test 'should restore answer' do
    q = create(:question, group: @group, user: @user)

    a = create(:answer, commentable: q, user: @user)
    r1 = create(:user)
    r2 = create(:user)

    @group.add_member r1
    @group.add_member r2
    create(:report, reporter: r1, reportable: a)
    create(:report, reporter: r2, reportable: a)

    # normal user cannot restore
    sign_in @user
    post :restore, {:post_id => q.id, :id => a.id,
                    :format => :json}
    assert_response :forbidden
    assert a.reload.hidden
    sign_out @user

    # faculty can restore posts and clear all reports
    sign_in @faculty
    post :restore, {:post_id => q.id, :id => a.id,
                    :format => :json}
    assert_response :ok
    assert_not a.reload.hidden
    assert_equal a.valid_reports.count, 0
    sign_out @faculty

    #faculty restoring own reported post
    create(:report, reporter: @faculty, reportable: a)
    sign_in @faculty
    assert_difference ->{a.reports.count}, -1 do
      post :restore, {:post_id => q.id, :id => a.id,
                    :format => :json}
    end
    assert_response :ok
    assert_not a.reload.hidden
    assert_equal a.valid_reports.count, 0
    sign_out @faculty
  end

  test "allow creating anonymous answer" do
    question = create(:question, user: @user, group: @group)

    sign_in @user
    assert_difference -> { Answer.count }, 1 do
      post :create, {message: "aa", post_id: question.id, type: 'Answer',
                     anonymous: true, :format => :json}
    end

    answer = assigns(:comment)
    assert answer
    assert answer.anonymous
    sign_out @user
  end

  test "hide user information on anonymous comment" do
    sign_in @user
    question = create(:question, group: @group, user: @user)
    answer = create(:answer, user: @user, commentable: question, anonymous: true)

    get :show, {:id => answer.id, :post_id => question.id, :format => :json}

    answer = assigns(:comment)
    assert answer
    resp = get_json_response response

    assert resp['user']['name'], "Anonymous"
    assert_nil resp['user']['first_name']
    assert_nil resp['user']['score']
    assert_nil resp['user']['stars']
    sign_out @user
  end

  test "show comments for an event" do
    user = create(:user)
    @group.add_member user

    sign_in @user
    event = create(:event, group: @group, user: @user)
    create_list(:comment, 10, commentable: event, user: user)

    get :index, event_id: event.id, format: :json
    assert_response :success

    resp = get_json_response(response)

    assert_equal resp.count, 10
    sign_out @user
  end

  def update_comment_setup
    @resource = create(:resource)
    @e = create(:event, user: @user, group: @group)
    @c = create(:comment, commentable: @e,
                user: @user, message: 'old message #oldtag')
    @c.resource = @resource
    @c.save
  end

  test 'should update comment' do
    update_comment_setup
    sign_in @user

    patch :update, {:event_id => @e.id, :id => @c.id,
                    :message => 'new message #newtag', :format => :json}
    assert_response :success
    @c.reload
    assert_equal @c.message, 'new message #newtag'

    assert_equal @c.tags.count, 1
    assert_equal @c.tags[0].name, 'newtag'

    assert_nil @c.resource

    sign_out @user
  end

  test 'bad update request without message param event' do
    update_comment_setup
    sign_in @user
    patch :update, {:event_id => @e.id, :id => @c.id, :format => :json}
    assert_response :bad_request
    sign_out @user
  end

  test 'bad update request with empty message event' do
    update_comment_setup
    sign_in @user
    patch :update, {:event_id => @e.id, :id => @c.id, :message => '',
                    :format => :json}
    assert_response :unprocessable_entity
    assert @c.reload.message, 'old message #oldtag'
  end

  test 'user can only edit own post event' do
    update_comment_setup
    u = create(:user)
    @group.add_member u

    sign_in u

    patch :update, {:event_id => @e.id, :id => @c.id,
                    :message => 'new message', :format => :json}

    assert_response :forbidden
    assert_equal @c.reload.message, 'old message #oldtag'
  end

  test 'should delete comment event' do
    e = create(:event, group: @group, user: @user)
    c = create(:comment, commentable: e, user: @user)

    sign_in @user
    assert_difference ->{e.comments.count}, -1 do
      delete :destroy, {:event_id => e.id, :id => c.id, :format => :json}
    end

    sign_out @user
  end

  test 'user can only delete own post event' do
    e = create(:event, group: @group, user: @user)
    c = create(:comment, commentable: e, user: @user)

    u = create(:user)
    sign_in u

    assert_no_difference ->{e.comments.count} do
      delete :destroy, {:event_id => e.id, :id => c.id, :format => :json}
    end
    assert_response :forbidden

    sign_out u
  end

  test 'should restore comment event' do
    e = create(:event, group: @group, user: @user)

    c = create(:comment, commentable: e, user: @user)
    r1 = create(:user)
    r2 = create(:user)
    fac = create(:user)
    @group.add_admin fac
    @group.add_member r1
    @group.add_member r2
    create(:report, reporter: r1, reportable: c)
    create(:report, reporter: r2, reportable: c)

    # normal user cannot restore
    sign_in @user
    post :restore, {:event_id => e.id, :id => c.id,
                    :format => :json}
    assert_response :forbidden
    assert c.reload.hidden
    sign_out @user

    # faculty can restore posts and clear all reports
    sign_in fac
    post :restore, {:event_id => e.id, :id => c.id,
                    :format => :json}
    assert_response :ok
    assert_not c.reload.hidden
    assert_equal c.valid_reports.count, 0
    sign_out fac

    #faculty restoring own reported post
    create(:report, reporter: fac, reportable: c)
    sign_in fac
    assert_difference ->{c.reports.count}, -1 do
      post :restore, {:event_id => e.id, :id => c.id,
                    :format => :json}
    end
    assert_response :ok
    assert_not c.reload.hidden
    assert_equal c.valid_reports.count, 0
    sign_out fac
  end

  test 'should send is mobile in api response' do
    sign_in @user
    q = create(:post, user: @user, group: @group)
    comment = create(:comment, commentable: q, user: @user, is_mobile: true)

    get :show, post_id: q.id, id: comment.id, format: :json
    resp = get_json_response(response)
    assert resp['is_mobile']
  end

  test 'should attach files on comment create' do
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)
    fr2 = FileResource.create(attachment: pic.open)

    p = create(:post, user: @user, group: @group)

    sign_in @user

    assert_difference -> {Comment.count} do
      post :create, {post_id: p.id, message: 'm',
                     file_ids: [fr1.id, fr2.id],
                     format: :json}
    end

    c = Comment.last
    assert_equal 2, c.file_resources.count
    assert_equal c.file_resources.pluck(:id).sort, [fr1.id, fr2.id].sort

    assert_difference -> {Comment.count} do
      post :create, {post_id: p.id, message: 'm', file_ids: [fr1.id, fr2.id], format: :json}
    end
    c = Comment.last
    assert_equal 2, c.file_resources.count
    assert_equal c.file_resources.pluck(:id).sort, [fr1.id, fr2.id].sort
  end

  test 'should attach files on comment update' do
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)
    fr2 = FileResource.create(attachment: pic.open)

    sign_in @user

    p = create(:post, user: @user, group: @group)
    c = create(:comment, commentable: p, user: @user)
    c.file_resources << fr1

    patch :update, {id: c.id, message: 'm', post_id: p.id,
                    file_ids: [fr2.id], format: :json}

    c.reload
    assert_equal 1, c.file_resources.count
    assert_equal fr2.id, c.file_resources.first.id
  end

  test 'should send file resource in api response' do
    api_setup
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)
    fr2 = FileResource.create(attachment: pic.open)

    sign_in @user

    @answer.file_resources << [fr1, fr2]

    get :show, id: @answer.id, post_id: @question.id, format: :json
    resp = get_json_response(response)

    expected_answer_with_file = @expected_answer.clone
    expected_answer_with_file['file_resources'] = @answer.file_resources.as_json.map{|a| a.merge({'available' => true})}
    expected_answer_with_file['user']['self'] = true
    assert_equal expected_answer_with_file, resp
  end

  def card_setup
    @card = create(:card, card_type: 'media', card_subtype: 'text')
  end

  test 'should be able to comment on a card' do
    card_setup
    @card.update_attributes(is_public: true)
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)
    fr2 = FileResource.create(attachment: pic.open)

    res = Article.create(url: 'http://www.youtube.com')
    res_dup = res.dup
    res_dup.save
    Resource.stubs(:extract_resource).returns(res_dup)

    u = create(:user)
    sign_in u
    assert_difference [-> {Comment.count}, ->{@card.comments.count}] do
      post :create, {card_id: @card.id, message: 'some text with a link. http://www.youtube.com',
                     file_ids: [fr1.id, fr2.id],
                     format: :json}
    end

    comment = @card.comments.last
    assert_equal 2, comment.file_resources.count
    assert_equal [fr1.id, fr2.id].sort, comment.file_resource_ids.sort

    assert_equal res_dup, comment.resource

    sign_out u
  end

  test 'index should work with first item id' do
    p = create(:post, user: @user, group: @group)
    answers = create_list(:comment, 4, commentable: p, user: @user)
    sign_in @user
    get :index, post_id: p.id, first_item_id: answers.last.id, limit: 2, format: :json
    assert_response :success
    resp = get_json_response(response)
    assert_equal [answers[1].id, answers[2].id], resp.map{|r| r['id']}

    get :index, post_id: p.id, first_item_id: answers.last.id, format: :json
    assert_response :success
    resp = get_json_response(response)
    assert_equal answers.first(3).map{|a| a.id}, resp.map{|r| r['id']}
  end

  test 'index should work with last fetch time' do
    q = create(:post, user: @user, group: @group)
    a1 = create(:comment, user: @user, commentable: q, created_at: 1.day.ago)
    a2 = create(:comment, user: @user, commentable: q, created_at: 5.hours.ago)

    sign_in @user
    get :index, post_id: q.id, last_fetch_time: 2.days.ago, format: :json
    resp = get_json_response(response)
    assert_equal 2, resp.count
    assert_equal [a1.id, a2.id], [resp[0]['id'], resp[1]['id']]

    get :index, post_id: q.id, last_fetch_time: 6.hours.ago, format: :json
    resp = get_json_response(response)
    assert_equal 1, resp.count
    assert_equal a2.id, resp[0]['id']

    get :index, post_id: q.id, last_fetch_time: Time.now, format: :json
    resp = get_json_response(response)
    assert_equal 0, resp.count
    sign_out @user
  end

  test 'commenting on a card' do
    c = create(:card, message: 'message', card_type: 'media', card_subtype: 'video', is_public: true)

    Tracking.expects(:track_act).with(
        user_id: @user.id,
        object: 'card',
        object_id: c.id,
        card: show_insight_url(c),
        action: 'comment',
        platform: 'web',
        device_id: 'deviceid123',
        organization_id: @user.organization_id,
        referer: 'http://edcast.com'
    )

    sign_in @user
    assert_difference ->{Comment.count} do
      post :create, card_id: c.id, message: 'some comment', format: :json
      assert_response :created
    end
    sign_out @user
  end

  test 'should be able to get comments on a card' do
    sign_in @user
    card = create(:public_media_card, author_id: @user.id)
    c1 = create(:comment, commentable: card, user: @user)
    c2 = create(:comment, commentable: card, user: @user)
    get :index, card_id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal [c1.id, c2.id], resp.map{|r| r['id']}
    sign_out @user
  end

  test 'should delete comment event if user doesnot belong to a group' do
    card = create(:public_media_card, author_id: @user.id)
    comment = create(:comment, commentable: card, user: @user)
    another_comment = create(:comment, commentable: card, user: create(:user))

    Tracking.expects(:track_act).with(
        user_id: @user.id,
        object: 'card',
        object_id: card.id,
        card: show_insight_url(card),
        action: 'uncomment',
        platform: 'web',
        device_id: 'deviceid123',
        organization_id: @user.organization_id,
        referer: 'http://edcast.com'
    )

    sign_in @user
    assert_difference ->{card.comments.count}, -1 do
      delete :destroy, {:card_id => card.id, :id => comment.id, :format => :json}
      assert_response :no_content
    end

    assert_no_difference ->{card.comments.count} do
      delete :destroy, card_id: card.id, id: another_comment.id, format: :json
      assert_response :forbidden
    end

    sign_out @user
  end

  test 'update, create, delete and restore replies to a comment' do
    g, u = create_gu
    p = create(:post, group: g, user: u)
    c = create(:comment, commentable: p, user: u)
    sign_in u
    assert_difference ->{Comment.count} do
      post :create, comment_id: c.id, message: 'some reply', format: :json
      assert_response :created
    end
    r = Comment.last
    assert_equal c, r.commentable

    patch :update, comment_id: c.id, id: r.id, message: 'edited reply', format: :json
    assert_response :ok
    assert_equal "edited reply", r.reload.message

    assert_difference ->{Comment.count}, -1 do
      delete :destroy, comment_id: c.id, id: r.id, format: :json
      assert_response :no_content
    end
    assert_equal 0, c.reload.comments_count
  end

  test 'public forum read' do
    ch = create(:channel, is_private: false)
    u = create(:user)
    create(:follow, followable: ch, user: u)
    p = create(:post, channel: ch, user: u)
    comments = create_list(:comment, 3, commentable: p, user: u)
    replies = create_list(:comment, 3, commentable: comments.first, user: u)

    get :index, post_id: p.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_same_elements comments.map(&:id), resp.map{|r| r['id']}

    # commentable doesnt get reset otherwise. doh
    @controller = CommentsController.new
    get :index, comment_id: comments.first.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_same_elements replies.map(&:id), resp.map{|r| r['id']}

    @controller = CommentsController.new
    get :show, post_id: p.id, id: comments.first.id, format: :json
    assert_response :ok

    @controller = CommentsController.new
    get :show, comment_id: comments.first.id, id: replies.first.id, format: :json
    assert_response :ok
  end

end

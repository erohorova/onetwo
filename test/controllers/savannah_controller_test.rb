require 'test_helper'
class SavannahControllerTest < ActionController::TestCase

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'

    # create org
    @org = create :organization
    @client = create(:client, organization: @org)
    @cred = create(:credential, client: @client)
    @regular_user = create :user, organization: @org, organization_role: 'member'
    @admin_user = create :user, organization: @org, organization_role: 'admin'

    # create sso
    @saml = create :sso, name: 'saml'
    @org.ssos << @saml
    @org_sso = @org.org_ssos.where(sso_id: @saml.id).first
  end

  test "should return list of ssos" do
    create_default_ssos
    Organization.any_instance.unstub :default_sso
    @org.send :default_sso

    set_host(@org)
    sign_in @admin_user
    oauth2_sso = create :sso, name: 'oauth2'
    facebook_sso = Sso.where(name: 'facebook').first

    params = {
      host: @org.host_name,
      format: :json
    }
    set_api_request_header(params)
    get :list_ssos, params
    json_response = get_json_response response
    # 4 default ones, 1 saml, 1 oauth2
    assert_equal 6, json_response.length, json_response
  end

  test "should enable sso" do
    params = {
      user_id: @admin_user.id,
      user_email: @admin_user.email,
      user_role: "admin",
      name: @saml.name,
      format: :json
    }
    set_api_request_header(params)
    set_host(@org)
    sign_in @admin_user
    @org_sso.update is_enabled: false
    assert_equal false, @org_sso.is_enabled
    post :enable_sso, params
    assert_response :success
    assert_equal true, @org_sso.reload.is_enabled

    set_host(@org)

    sign_in @admin_user
    @org_sso.destroy!
    assert_difference ->{OrgSso.count} do
      post :enable_sso, params, format: :json
    end
    assert_response :success
    new_org_sso = OrgSso.where(organization_id: @org.id, sso_id: @saml.id).first!
    assert_equal true, new_org_sso.reload.is_enabled
  end

  test "should disable ssos" do
    set_host(@org)
    sign_in @admin_user
    @org_sso.update is_enabled: true
    assert_equal true, @org_sso.is_enabled

    params = {

      user_id: @admin_user.id,
      user_email: @admin_user.email,
      user_role: "admin",
      name: "saml",
      format: :json
    }
    set_api_request_header(params)
    post :disable_sso, params
    assert_response :success
    assert_equal false, @org_sso.reload.is_enabled
  end

  test 'can savannah sso login' do
    set_host(@org)
    exp = Time.now.to_i + 100
    redirect_url = "https://#{@org.host}"
    api_key = @cred.api_key
    payload = {
      :email=> @regular_user.email,
      :redirect_url=> redirect_url,
      :exp=>exp
    }
    token = JWT.encode(payload, @cred.shared_secret,'HS256')
    pars = {
      :token=>token,
      :api_key=>api_key
    }
    get :sso_login, pars

    assert_response :success
    assert_equal redirect_url,assigns(:redirect_uri)
  end

  test 'should return only enabled and visible ssos' do
    set_host(@org)
    params = {
      host: @org.host_name,
      format: :json
    }

    set_api_request_header(params)

    google_sso = create :sso, name: 'google'
    lxp_oauth_sso = create :sso, name: 'lxp_oauth'

    legacy_google_org_sso = create :org_sso, sso: google_sso, is_visible: false, organization: @org
    lxp_oauth_org_sso = create :org_sso, sso: lxp_oauth_sso, is_visible: false, organization: @org

    assert legacy_google_org_sso.is_enabled

    get :list_ssos, host: @org.host_name, format: :json
    assert_response :success
    resp = get_json_response(response)

    assert_same_elements [@org_sso.id], resp.map { |sso| sso['id'] }.compact
    assert_not_includes [legacy_google_org_sso.id, lxp_oauth_org_sso.id], resp.map { |sso| sso['id'] }.compact
  end

  def set_api_request_header(payload)
    token = JWT.encode(payload, @cred.shared_secret,'HS256')
    request.headers['X-API-KEY'] = @cred.api_key
    request.headers['X-API-TOKEN'] = token
  end
end

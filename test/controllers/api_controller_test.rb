require 'test_helper'

class ApiControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  class DummiesController < ApiController
    def create
      @modified_params = params
      @modified_params.delete(:controller)
      @modified_params.delete(:action)
      head :ok
    end

    def index
      head :ok
    end

    def next
      head :ok
    end

    def noauth
      head :no_content
    end

    def oauth2_action
      head :ok
    end

    def user
      render json: { id: current_user.id }
    end

  end

  self.controller_class= DummiesController

  def with_dummy_routing
    with_routing do |set|
      set.draw do
        root 'welcome#index'
        get 'dummy_index', :controller=>"api_controller_test/dummies", :action=>"index"
        get 'dummy_nouath', :controller=>"api_controller_test/dummies", :action=>"noauth"
        get 'dummy_user', :controller=>"api_controller_test/dummies", :action=>"user"
        post 'dummy_create', :controller=>"api_controller_test/dummies", :action=>"create"
        post 'oauth2_action', :controller=>"api_controller_test/dummies", :action=>"oauth2_action"

      end
      yield
    end
  end

  setup do
    skip
  end
  test 'should work with api based request if the credential match up in json format' do
    org = create(:organization)
    client = create(:client, organization: org)
    client_user = create(:clients_user, client: client)
    user = client_user.user
    user_role = 'teacher'
    api_key = client.valid_credential.api_key
    shared_secret = client.valid_credential.shared_secret
    timestamp = Time.now.to_i
    with_dummy_routing do
      post :create, test_key: 'test_value', api_key: api_key, user_id: client_user.client_user_id, user_role: user_role,
           token: Digest::SHA256.hexdigest([shared_secret, timestamp, client_user.client_user_id, user_role].join('|')), timestamp: timestamp, format: 'json'
      assert_response :success
      assert_equal 'test_value', assigns[:modified_params][:test_key], 'should make it through the security filter'
      assert_not response.headers['X-App-Version'], 'should not include X-App-Version header for non native app requests'
    end
  end

  test 'should add the native app version number in the response header' do
    CacheFactory.clear
    org = create(:organization)
    client = create(:client, organization: org)
    client_user = create(:clients_user, client: client)
    user = client_user.user
    user_role = 'teacher'
    api_key = client.valid_credential.api_key
    shared_secret = client.valid_credential.shared_secret
    timestamp = Time.now.to_i

    request.headers['User-Agent'] = 'EdCast/1.0 (iPhone; iOS 8.1.1; Scale/2.00)'
    release = create(:release, platform: 'ios', form_factor: 'phone')
    with_dummy_routing do
      post :create, test_key: 'test_value', api_key: api_key, user_id: client_user.client_user_id, user_role: user_role,
           token: Digest::SHA256.hexdigest([shared_secret, timestamp, client_user.client_user_id, user_role].join('|')), timestamp: timestamp, format: 'json'
      assert_response :success
      assert_equal 'test_value', assigns[:modified_params][:test_key], 'should make it through the security filter'
      assert_equal release.version, response.headers['X-App-Version']
      assert_equal release.version, response.headers['X-Latest-Version']
      assert_equal release.build, response.headers['X-Latest-Build']
    end
  end


  test 'should fail if api credential does not match' do
    org = create(:organization)
    client = create(:client, organization: org)
    client_user = create(:clients_user, client: client)
    user = client_user.user
    api_key = client.valid_credential.api_key
    shared_secret = client.valid_credential.shared_secret
    timestamp = Time.now.to_i
    with_dummy_routing do
      post :create, test_key: 'test_value', api_key: api_key, user_id: client_user.client_user_id, token: 'junk', timestamp: timestamp, format: 'json'
      assert_response :forbidden
    end
  end

  test 'should authenticate with id when email not present' do
    # client_user = create(:clients_user)
    # client = client_user.client
    org = create(:organization)
    client = create(:client, organization: org)
    client_user = create(:clients_user, client: client)
    user = client_user.user
    group = create(:group, client_resource_id: 'group', organization: org)
    group.add_user(user: user, role: 'student')
    api_key = client.valid_credential.api_key
    shared_secret = client.valid_credential.shared_secret
    timestamp = Time.now.to_i
    with_dummy_routing do
      post :create, api_key: api_key, resource_id: 'group', user_id: client_user.client_user_id, user_role: 'student',
           token: Digest::SHA256.hexdigest([shared_secret, timestamp, client_user.client_user_id, nil, 'student', 'group'].compact.join('|')), timestamp: timestamp, format: 'json'
      assert_response :success
    end
  end

  test 'should snake case the parameters for json request' do
    sign_in create(:user)
    with_dummy_routing do
      post :create, {:thisIsMyKey => {:keyOne => {:keyTwo => 'value_one', :keyThree => ['value_two', 'value_three']}}, :anotherKey => 'another_value', :format => 'json'}
      assert_equal(assigns[:modified_params],
                   {'this_is_my_key' => {'key_one' => { 'key_two' => 'value_one', 'key_three' => ['value_two', 'value_three']}}, 'another_key' => 'another_value', 'format' => 'json'}
      )
    end
  end

  test 'should not snake case the parameters for non-json request' do
    sign_in create(:user)
    with_dummy_routing do
      post :create, {:thisIsMyKey => {:keyOne => {:keyTwo => 'value_one', :keyThree => ['value_two', 'value_three']}}, :anotherKey => 'another_value'}
      assert_equal(assigns[:modified_params],
                   {'thisIsMyKey' => {'keyOne' => {'keyTwo' => 'value_one', 'keyThree' => ['value_two', 'value_three']}}, 'anotherKey' => 'another_value'}
      )
    end
  end

  test 'should work with access_token' do
    user = create(:user)
    credential = create(:credential)
    application = Doorkeeper::Application.find(credential.client.id)
    access_token = create(:clientless_access_token, resource_owner_id: user.id, application: application)

    with_dummy_routing do
      post :oauth2_action, access_token: access_token.token
      assert_response :success
      assert_equal user, @controller.current_resource_owner
    end
  end

  test 'should reject invalid access token' do
    with_dummy_routing do
      post :oauth2_action, access_token: 'junk'
      assert_response :unauthorized
    end
  end

  test 'should reject access token if user suspended' do
    user = create(:user, is_suspended: true)
    credential = create(:credential)
    application = Doorkeeper::Application.find(credential.client.id)
    access_token = create(:clientless_access_token, resource_owner_id: user.id, application: application)

    with_dummy_routing do
      post :oauth2_action, access_token: access_token.token
      assert_response :unauthorized
    end
  end

  test 'should accept jwt token and set current user' do
    # create_default_org
    user = create(:user)
    token = user.jwt_token
    with_dummy_routing do
      set_host(user.organization)
      @request.headers["X-Edcast-JWT"] = "bar"
      post :index, token: token
      assert_response :ok
      assert assigns[:current_user], user.id
    end
  end

  test 'should reject jwt token if trying to access the wrong org' do
    # create_default_org
    user = create(:user)
    org = create(:organization)
    token = user.jwt_token
    with_dummy_routing do
      set_host(org)
      @request.headers["X-Edcast-JWT"] = "bar"
      post :index, token: token
      assert_response :not_found
    end
  end

  test 'should reject jwt token if user suspeneded' do
    # create_default_org
    user = create(:user, is_suspended: true )
    token = user.jwt_token
    with_dummy_routing do
      set_host(user.organization)
      @request.headers["X-Edcast-JWT"] = "bar"
      post :index, token: token
      assert_response :not_found
    end
  end

  test 'user should be logged in using only the JWT token' do
    # create_default_org
    user =  create(:user)
    token = user.jwt_token
    with_dummy_routing do
      set_host user.organization
      @request.headers["X-Edcast-JWT"] = "bar"
      get :user, token: token
      json_response = get_json_response(response)
      assert_equal json_response["id"], user.id
    end
  end

  test 'should set request store' do
    @controller.expects(:set_request_store).once

    org = create(:organization)
    user = create(:user, organization: org)
    set_host org

    sign_in user
    with_dummy_routing do
      get :index
    end
  end

  test '#set_request_store adds metadata to RequestStore' do
    org = create(:organization)
    user = create(:user, organization: org)
    set_host org
    sign_in user
    @request.headers["HTTP_X_VERSION_NUMBER"] = "foo"
    RequestStore.store.expects(:[]=).with(:request_metadata,
      user_id: user.id,
      platform: "web",
      platform_version_number: "foo",
      user_agent: "Rails Testing",
      is_admin_request: 0
    )
    with_dummy_routing do
      get :index
    end
  end
end

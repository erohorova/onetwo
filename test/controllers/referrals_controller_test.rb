require 'test_helper'

class ReferralsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    stub_request(:post, 'http://localhost:9200/_bulk')
    stub_request(:post, 'https://api.branch.io/v1/url').to_return(status: 200, body: '', headers: {})
    Organization.any_instance.stubs(:create_org_level_team).returns true

    @org        = create(:organization)
    @admin_user = create :user, organization: @org, organization_role: 'admin'
    @invitation = Invitation.create(recipient_email: 'b@b.com', first_name: 'Bharath', last_name: 'Bhat', invitable: @org, sender: @admin_user)
  end

  test 'non existant invitation should redirect to root' do
    post :join, token: '123'
    assert_redirected_to root_path

    get :join, token: '123', format: :json
    assert_response :unprocessable_entity
  end

  test 'GET request should render a javascript code to submit a form POST' do
    set_host(@org)
    assert_no_difference -> {User.count} do
      get :join, token: @invitation.token, format: :html
    end
    assert_response :success
    assert_template :join
    assert_match '.submit()', response.body
  end

  test 'look up token and populate user default params' do
    set_host(@org)
    assert_difference -> {User.count} do
      post :join, token: @invitation.token
    end

    assert_equal '1', cookies[:collect_user_data]
    assert_redirected_to root_path

    user = assigns(:user)
    assert_equal user.first_name, 'Bharath'
    assert_equal user.last_name, 'Bhat'
    assert_equal user.email, 'b@b.com'

    invitation = assigns(:invitation)
    assert_equal invitation, @invitation
    assert @controller.user_signed_in?
  end

  test 'look up token and populate user default params, json format' do
    set_host(@org)
    assert_difference -> {User.count} do
      get :join, token: @invitation.token, format: :json
    end

    assert_equal '1', cookies[:collect_user_data]

    assert_response :success

    assert @controller.user_signed_in?
  end

  test 'should redirect the user to dashboard if the user clicked on the invite link again' do
    create(:user, organization: @org, email: @invitation.recipient_email, first_name: @invitation.first_name, last_name: @invitation.last_name, password: User.random_password)
    set_host(@org)
    assert_no_difference -> {User.count} do
      post :join, token: @invitation.token
    end
    assert_redirected_to root_path
  end

  test 'should redirect the user to dashboard if the user clicked on the invite link again, json format' do
    # create_default_org
    create(:user, organization: @org, email: @invitation.recipient_email, first_name: @invitation.first_name, last_name: @invitation.last_name, password: User.random_password)
    set_host(@org)
    assert_no_difference -> {User.count} do
      get :join, token: @invitation.token, format: :json
    end
    assert_no_difference -> {User.count} do
      get :join, token: @invitation.token, format: :json
    end
    assert_response :unprocessable_entity
  end

  test 'confirm unconfirmed user automatically' do
    org = create(:organization)
    user = create(:user, confirmed_at: nil, organization: org)
    invitation = create(:invitation, recipient: user, recipient_email: user.email,
                      first_name: user.first_name, last_name: user.last_name, invitable: org,
                      roles: 'admin', sender: create(:user, organization: org))

    assert_equal false, user.confirmed?
    set_host(org)
    post :join, token: invitation.token
    user.reload
    assert_equal true, user.confirmed?
  end

  test 'should invite user into an org as a member' do
    organization = create(:organization)

    sender = create(:user, organization: organization)
    invitation = create(:invitation, recipient_email: generate(:email_address),
                        first_name: 'trung', last_name: 'pham', invitable: organization, roles: 'member', sender: sender)
    set_host(organization)
    assert_difference -> {User.count} do
      post :join, token: invitation.token
    end
    assert_redirected_to root_path(host: organization.host)
    assert_equal true, organization.is_member?(User.last)
  end
  
  test 'should not set remember_created_at for user when config is not present' do
    organization = create(:organization)

    sender = create(:user, organization: organization)
    invitation = create(:invitation, recipient_email: generate(:email_address),
                        first_name: 'trung', last_name: 'pham', invitable: organization, roles: 'member', sender: sender)
    set_host(organization)
    assert_difference -> {User.count} do
      post :join, token: invitation.token
      assert_not_nil User.last.remember_created_at
    end
    assert_redirected_to root_path(host: organization.host)
    assert_equal true, organization.is_member?(User.last)
  end
  
  test 'should not set remember_created_at for user when config is present and set to false' do
    organization = create(:organization)

    sender = create(:user, organization: organization)
    invitation = create(:invitation, recipient_email: generate(:email_address),
                        first_name: 'trung', last_name: 'pham', invitable: organization, roles: 'member', sender: sender)
    set_host(organization)
    create :config, :boolean, name: 'sessionExpireOnBrowserClose', value: false, configable: organization
    assert_difference -> {User.count} do
      post :join, token: invitation.token
      assert_not_nil User.last.remember_created_at
    end
    assert_redirected_to root_path(host: organization.host)
    assert_equal true, organization.is_member?(User.last)
  end

  test 'should set remember_created_at for user when config is present and set to true' do
    organization = create(:organization)

    sender = create(:user, organization: organization)
    invitation = create(:invitation, recipient_email: generate(:email_address),
                        first_name: 'trung', last_name: 'pham', invitable: organization, roles: 'member', sender: sender)
    set_host(organization)
    create :config, :boolean, name: 'sessionExpireOnBrowserClose', value: true, configable: organization
    assert_difference -> {User.count} do
      post :join, token: invitation.token
      assert_nil User.last.remember_created_at
    end
    assert_redirected_to root_path(host: organization.host)
    assert_equal true, organization.is_member?(User.last)
  end

  test 'should invite user into an org as an admin' do
    organization = create(:organization)
    sender = create(:user, organization: organization)

    set_host(organization)
    invitation = create(:invitation, recipient_email: generate(:email_address),
                        first_name: 'trung', last_name: 'pham', invitable: organization, roles: 'admin', sender: sender)
    assert_difference -> {User.count} do
      post :join, token: invitation.token
    end
    assert_redirected_to root_path(host: organization.host)
    assert_equal true, organization.is_admin?(User.last)
  end

  test 'should skip onboarding for user if org onboarding is kept off' do
    organization = create(:organization)
    sender = create(:user, organization: organization)
    organization.update_column(:show_onboarding, false)

    invitation = create(:invitation, recipient_email: generate(:email_address),
                        first_name: 'abc', last_name: 'xyz', invitable: organization, roles: 'member', sender: sender)
    set_host(organization)
    assert_difference -> {User.count}, 1 do
      post :join, token: invitation.token
    end

    assert_redirected_to root_path(host: organization.host)
    assert_equal true, organization.is_member?(User.last)
    assert_equal true, User.last.onboarding_completed? # onboarding skipped
  end

  test 'non org member should be added into group and org too' do
    # create_default_org
    email = generate(:email_address)

    # adding user as `admin`
    team = create(:team)
    set_host(team.organization)
    sender = create(:user, organization: team.organization)
    invitation = create(:invitation, recipient_email: email, first_name: 'trung', last_name: 'pham', invitable: team, roles: 'admin', sender: sender)
    assert_difference -> {User.count} do
      post :join, token: invitation.token
    end
    assert team.is_admin? assigns(:user)
    refute team.is_member? assigns(:user)
    assert_redirected_to team_url(team, host: team.organization.host)

    # same user as `member`
    invitation = create(:invitation, recipient_email: email, first_name: 'trung', last_name: 'pham', invitable: team, roles: 'member', sender: sender)
    assert_no_difference -> {User.count} do
      post :join, token: invitation.token
    end
    refute team.is_admin? assigns(:user)
    assert team.is_member? assigns(:user)
    assert_redirected_to team_url(team, host: team.organization.host)
  end

  test 'should invite user into a team' do
    # create_default_org
    org = create(:organization)
    sender = create(:user, organization: org)

    user = create(:user, organization: org)
    email = user.email

    set_host(org)

    %w[admin member].each do |role|
      team = create(:team, organization: org)
      invitation = create(:invitation, recipient_email: email,
                          first_name: 'trung', last_name: 'pham', invitable: team, roles: role, sender: sender)
      assert_no_difference -> {User.count} do
        post :join, token: invitation.token
      end
      assert_redirected_to team_url(team, host: org.host)
      assert team.organization.is_member?(user)
    end
    t1, t2 = Team.last(2)
    assert t1.is_admin? user
    assert t2.is_member? user
  end

  test 'track team joining' do
    skip #deprecated
    org = create(:organization)
    sender = create(:user, organization: org)
    user = create(:user, organization: org)
    email = user.email
    set_host(org)
    team = create(:team, organization: org)
    invitation = create(:invitation, recipient_email: email,
                        first_name: 'abc', last_name: 'xyz', invitable: team, roles: 'member', sender: sender)
    request.cookies['_d'] = 'device123'
    request.env['HTTP_REFERER'] = 'http://example.com'

    MetricRecord.unstub(:create)
    MetricRecord.expects(:create).with(
      actor_type: 'user',
      actor_id: user.id,
      object_type: 'team',
      object_id: team.id,
      owner_type: nil,
      owner_id: nil,
      action: 'join',
      platform: 'web',
      device_id: 'device123',
      properties: nil,
      organization_id: user.organization_id,
      referer: 'http://example.com',
      ecl_id: nil
    )

    assert_no_difference -> {User.count} do
      post :join, token: invitation.token
    end

    refute team.is_admin?(user)
    assert team.is_member?(user)
  end

  test '#join attaches channels to recipient' do
    channel = create :channel, organization: @org
    parameters = { 'channel_ids' => [channel.id] }
    @invitation.update(parameters: parameters)

    TestAfterCommit.enabled = true

    set_host(@org)
    post :join, token: @invitation.token

    @invitation.reload
    user = User.find_by(email: @invitation.recipient_email, organization: @org)

    assert_includes user.followed_channels.pluck(:id), channel.id
  end

  test '#join attaches teams to recipient' do
    team = create :team, organization: @org
    parameters = { 'team_ids' => [team.id] }
    @invitation.update(parameters: parameters)

    TestAfterCommit.enabled = true

    set_host(@org)
    post :join, token: @invitation.token

    @invitation.reload
    user = User.find_by(email: @invitation.recipient_email, organization: @org)

    assert_includes user.teams.pluck(:id), team.id
  end

  test '#join attaches custom fields to recipient' do
    cf_1 = create :custom_field, organization: @org, display_name: 'Burrito Wrap', abbreviation: nil
    cf_2 = create :custom_field, organization: @org, display_name: 'Cheese', abbreviation: nil
    cf_3 = create :custom_field, organization: @org, display_name: 'Tea', abbreviation: nil

    parameters = { cf_1.abbreviation => 'juarez', cf_2.abbreviation => 'mission' }
    @invitation.update(parameters: parameters)

    TestAfterCommit.enabled = true

    set_host(@org)
    post :join, token: @invitation.token

    @invitation.reload
    user = User.find_by(email: @invitation.recipient_email, organization: @org)

    assert_equal 'juarez', UserCustomField.find_by(custom_field: cf_1, user: user).value
    assert_equal 'mission', UserCustomField.find_by(custom_field: cf_2, user: user).value
    assert_nil UserCustomField.find_by(custom_field: cf_3, user: user)
  end

  test '#join attaches profile (i.e language) to recipient' do
    parameters = { 'language' => 'pt-BR' }
    @invitation.update(parameters: parameters)

    TestAfterCommit.enabled = true

    set_host(@org)
    post :join, token: @invitation.token

    @invitation.reload
    user = User.find_by(email: @invitation.recipient_email, organization: @org)

    assert user.profile
    assert_equal 'pt-BR', user.profile.language
  end
end

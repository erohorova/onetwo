require 'test_helper'

class LtiControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
  end

  test 'should route to the "events" lti' do
    assert_routing({ method: 'POST', path: '/lti/events' }, {controller: 'lti', action: 'events'})
  end

  test 'should route to the forum v1 lti' do
    assert_recognizes({controller: 'lti', action: 'forum_v1'}, { method: 'POST', path: '/lti/forum/v1' })
  end
end

require 'test_helper'

class PostsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'

    WebMock.disable!

    @org = create(:organization)
    @user = create(:user, organization: @org)
    @faculty = create(:user, email: 'faculty@stanford.edu', organization: @org)
    @group = create(:resource_group, organization: @org)
    @group.add_admin @faculty
    @group.add_member @user
  end

  test "status forbidden if user not logged in" do
    g,u = create_gu
    get :index, {:group_id => g.id, :format => :json}
    assert_response :unauthorized

    p = create(:post, group: g, user: u)
    get :show, {:group_id => g.id, :id => p.id, :format => :json}
    assert_response :unauthorized

    assert_no_difference ->{Question.count} do
      post :create, {:title => 'title',
                     :message => 'message',
                     :group_id => g.id,
                     :format => :json}
    end
    assert_response :unauthorized

    patch :update, {:group_id => g.id, :id => p.id,
                    :title => 'newTitle', :format => :json}
    assert_response :unauthorized

    delete :destroy, {:group_id => g.id, :id => p.id,
                     :format => :json}
    assert_response :unauthorized

    get :filters, { group_id: '1', format: :json }
    assert_response :unauthorized
  end

  test "create a question for a course" do
    sign_in @user
    assert_difference '@group.questions.count' do
      post :create, {title: "this is a title",
                     type: 'Question',
                     message: 'this is a message',
                     group_id: @group.id, :format => :json}
    end
    assert_template :show
    sign_out @user
  end

  test "create a question for a course and share the question" do
    sign_in @user
    ShareJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    facebook = create(:identity_provider, :facebook, user: @user)
    linkedin = create(:identity_provider, :linkedin, user: @user)

    FacebookProvider.any_instance.stubs(:resolve_token_scope)
    FacebookProvider.any_instance.expects(:share).with(anything()).returns('http://www.facebook.com/link/to/post')
    LinkedinProvider.any_instance.expects(:share).with(anything()).returns('http://www.linkedin.com/link/to/post')

    assert_difference '@group.questions.count' do
      post :create, {title: "this is a title",
                     message: 'this is a message',
                     type: 'Question',
                     networks: ['facebook', 'linkedin', 'twitter'],
                     group_id: @group.id, :format => :json}
    end

    assert_template :show
    sign_out @user
  end

  test "should not share the question if network is empty" do
    sign_in @user
    facebook = create(:identity_provider, :facebook, user: @user)
    linkedin = create(:identity_provider, :linkedin, user: @user)

    FacebookProvider.any_instance.expects(:share).never
    LinkedinProvider.any_instance.expects(:share).never

    assert_difference '@group.questions.count' do
      post :create, {title: "this is a title",
                     message: 'this is a message',
                     type: 'Question',
                     networks: nil,
                     group_id: @group.id, :format => :json}
    end

    assert_template :show
    sign_out @user
  end

  test "bad create request without message param" do
    sign_in @user
    assert_no_difference 'Question.count' do
      post :create , {title: 'some title',
                      group_id: @group.id, :format => :json}
      assert_response :bad_request
    end
    sign_out @user
  end

  test 'bad create request without type param' do
    ShareJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    sign_in @user
    assert_no_difference -> {Post.count} do
      post :create, {group_id: @group.id, message: 'some message',
                     title: 'some title', type: 'badtype', format: :json}
      assert_response :unprocessable_entity
    end
    sign_out @user
  end

  test "bad create request without title param" do
    sign_in @user
    assert_no_difference 'Question.count' do
      post :create , {group_id: @group.id, message: 'some message',
                      :format => :json}
      assert_response :bad_request
    end
    sign_out @user
  end

  test 'should not save question with empty title' do
    sign_in @user
    assert_no_difference ->{Question.count} do
      post :create, {:group_id => @group.id, title: "",
                     :message => 'some message', :format => :json}
    end
    assert_response :unprocessable_entity
    sign_out @user
  end

  test 'should save question with empty message' do
    sign_in @user
    assert_difference 'Question.count' do
      post :create, {:group_id => @group.id, title: "some title",
                     :type => 'Question', :message => '', :format => :json}
    end
    assert_response :created
    sign_out @user
  end

  test 'should create announcement' do
    sign_in @faculty
    assert_difference -> {Announcement.count} do
      post :create, group_id: @group.id, title: 'some announcement',
        message: 'announcement message', type: 'Announcement', format: :json
      assert_response :created
    end
    sign_out @faculty
  end

  test 'should create a question with context_id' do
    sign_in @user
    assert_difference 'Question.count' do
      post :create, {:group_id => @group.id, title: "some title",
                     :type => 'Question', :message => 'blah', context_id: 'abc123', :format => :json}
    end
    assert_response :created
    assert_equal 'abc123', Question.last.context_id
    sign_out @user
  end

  def multiple_question_setup
    @group1 = create(:resource_group)
    @group1.add_member @user
    @question1 = create(:question, group: @group1, user: @user,
                        created_at: 1.days.ago)
    @question2 = create(:question, group: @group1, user: @user,
                        created_at: 2.days.ago)
    @question3 = create(:question, group: @group1, user: @user,
                        created_at: 3.days.ago)
    voter = create(:user)
    @group1.add_member voter
    voter2 = create(:user)
    @group1.add_member voter2
    create(:vote, votable: @question1, voter: voter)
    create(:vote, votable: @question1, voter: voter2)
    create(:vote, votable: @question2, voter: voter)
  end

  test 'should update question title and message' do
    sign_in @user
    q = create(:question,
               user: @user,
               group: @group,
               message: 'old message',
               title: 'title')

    patch :update, {:group_id => @group.id,
                    :id => q.id,
                    :message => 'new message',
                    :title => 'new title',
                    :format => :json
                    }

    assert_response :success
    q.reload
    assert_equal q.message, 'new message'
    assert_equal q.title, 'new title'
    sign_out @user
  end

  test 'update should fail if title empty' do
    sign_in @user
    q = create(:question,
               user: @user,
               group: @group,
               message: 'message',
               title: 'title')

    patch :update, {:group_id => @group.id,
                    :id => q.id,
                    :message => 'new message',
                    :title => '',
                    :format => :json}
    assert_response :unprocessable_entity
    assert_equal q.reload.title, 'title'
  end

  test 'should update tags and resource' do
    sign_in @user
    q = create(:question,
               user: @user,
               group: @group,
               message: 'message',
               title: 'title')

    patch :update, {:group_id => @group.id,
                    :id => q.id,
                    :message => '#newtag new message',
                    :title => 'new title #titletag',
                    :format => :json
                    }

    assert_response :success
    q.reload

    assert_equal q.tags.count, 2
    assert_equal q.tags[0].name, 'newtag'
    assert_equal q.tags[1].name, 'titletag'

    assert_nil q.resource
  end

  test 'user can only edit own post' do
    u = create(:user)
    @group.add_member u
    q = create(:question, user: @user, group: @group, message: 'm')

    sign_in u

    patch :update, {:group_id => @group.id,
                    :id => q.id,
                    :message => 'new message',
                    :title => 'title',
                    :format => :json
                   }

    assert_response :forbidden

    assert_equal q.reload.message, 'm'
    sign_out u
  end

  test 'should delete question' do
    q = create(:question, group: @group, user: @user)

    sign_in @faculty
    assert_difference ->{Question.count}, -1 do
      delete :destroy, {:group_id => @group.id, :id => q.id,
                        :format => :json}
    end
    assert_response :ok
    sign_out @faculty
  end

  test 'only faculty or owner can delete question' do
    fac = create(:user)
    @group.add_admin(fac)
    q = create(:post, group: @group, user: @user)

    # owner
    sign_in @user
    assert_difference ->{Post.count}, -1 do
      delete :destroy, {:group_id => @group.id, :id => q.id,
                        :format => :json}
    end
    assert_response :ok
    sign_out @user

    # faculty
    q = create(:post, group: @group, user: @user)
    sign_in fac
    assert_difference ->{Post.count}, -1 do
      delete :destroy, {:group_id => @group.id, :id => q.id,
                        :format => :json}
    end
    assert_response :ok
    sign_out fac

    # random user
    u = create(:user)
    @group.add_member(u)
    sign_in u
    q = create(:post, group: @group, user: @user)
    assert_no_difference ->{Post.count} do
      delete :destroy, {:group_id => @group.id, :id => q.id,
                        :format => :json}
    end
    assert_response :forbidden
    sign_out u
  end

  test "allow creating anonymous questions" do
    sign_in @user
    assert_difference -> { Question.count }, 1 do
      post :create, :message => "aa", :title => "aa", :group_id => @group,
        :type => 'Question', :anonymous => true, format: :json
    end
    question = assigns(:post)
    assert question
    assert question.anonymous
    sign_out @user
  end

  test 'allow creating pinned posts' do
    sign_in @faculty
    now = DateTime.now
    DateTime.stubs(:now).returns(now)
    assert_difference -> { Question.count } do
      post :create, :message => 'aa', :title => 'aa', :group_id => @group.id,
        :type => 'Question', :anonymous => false, :pinned => true, format: :json
    end
    assert_response :created
    q = Question.last
    assert q.pinned
    assert_equal now.to_i, q.pinned_at.to_i
    sign_out @faculty
  end

  test 'ignore pinned field for non faculty creators' do
    sign_in @user
    assert_difference -> { Question.count } do
      post :create, :message => 'aa', :title => 'aa', :group_id => @group.id,
        :type => 'Question', :anonymous => false, :pinned => true, format: :json
    end
    assert_response :created
    q = Question.last
    assert_not q.pinned
    assert_not q.pinned_at
    sign_out @user
  end

  test 'update pinned field for existing post' do
    q = create(:question, user: @faculty, group: @group, pinned: false)
    sign_in @faculty
    post :pin, id: q.id, format: :json, state: true
    assert_response :ok
    assert q.reload.pinned
    assert q.reload.pinned_at

    post :pin, id: q.id, format: :json, state: false
    assert_response :ok
    assert_not q.reload.pinned
    assert q.reload.pinned_at
    sign_out @faculty
  end

  test 'do not let students update pinned field' do
    q = create(:question, user: @user, group: @group, pinned: false)
    sign_in @user
    patch :pin, id: q.id, state: true, format: :json
    assert_response :forbidden
    assert_not q.reload.pinned
    assert_not q.reload.pinned_at
    sign_out @user
  end

  test 'should trim trailing whitespace from post message' do
    sign_in @user
    expected_message = "<p>Hello whitespace</p>\n<p>&nbsp;</p><p>Hello whitespace</p>"
    post :create , {title: 'some title',
                    type: 'Question',
                    message: "<p>Hello whitespace</p>\n<p>&nbsp;</p><p>Hello whitespace</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>",
                    group_id: @group.id, :format => :json}
    last_post = Question.last
    assert_equal last_post.message, expected_message
    sign_out @user
  end

  test 'should tag posts created from mobile' do
    # if no token present, then it is automatically tagged as a mobile request
    sign_in @user

    assert_difference -> {Post.count} do
      post :create, {title: 't', message: 'm', type: 'Question', group_id: @group.id,
                     format: :json}
      assert_not assigns[:widget_request]
    end

    p = Post.last
    assert p.is_mobile?
    sign_out @user
  end

  test 'should not tag posts created from mobile' do
    # pass in all the token params to simulate a widget request (no explicit sign in)
    org = create(:organization)
    client = create(:client, organization: org)
    api_key = client.valid_credential.api_key
    shared_secret = client.valid_credential.shared_secret
    resource_id = SecureRandom.hex
    timestamp = Time.now.to_i
    user_id = '123'
    user = create(:user, organization: org)
    group = create(:group, client_resource_id: resource_id, organization: org)
    group.add_member(user)
    role = 'student'
    ClientsUser.create!(user: user, client: client, client_user_id: user_id)
    parts = [shared_secret, timestamp, user_id, user.email, role, resource_id].compact

    token = Digest::SHA256.hexdigest(parts.join('|'))
    base_params = {api_key: api_key, resource_id: resource_id, resource_name: 'test name',
                resource_description: 'test description',
                parent_url: 'http://localhost/ui/demo?edcast_ref=1&group=first',
                resource_url: 'http://path/to/resource',
                user_email: user.email,
                user_id: user_id,
                token: token,
                timestamp: timestamp,
                user_role: 'student',
                format: 'json'}

    assert_difference -> {Post.count} do
      post :create, {
        title: 't',
        message: 'm',
        type: 'Question',
        group_id: group.id
      }.merge(base_params)
      assert assigns[:widget_request]
    end

    p = Post.last
    assert_not p.is_mobile?
  end

  test 'should attach files on post create' do
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)
    fr2 = FileResource.create(attachment: pic.open)

    sign_in @user

    assert_difference -> {Post.count} do
      post :create, {title: 't', message: 'm', type: 'Question', group_id: @group.id,
                     file_ids: [fr1.id, fr2.id],
                     format: :json}
    end

    p = Post.last
    assert_equal 2, p.file_resources.count
    assert_equal p.file_resources.pluck(:id).sort, [fr1.id, fr2.id].sort

    assert_difference -> {Post.count} do
      post :create, post: {title: 't', message: 'm', type: 'Question', group_id: @group.id}, file_ids: [fr1.id, fr2.id], format: :json
    end
    p = Post.last
    assert_equal 2, p.file_resources.count
    assert_equal p.file_resources.pluck(:id).sort, [fr1.id, fr2.id].sort
  end

  test 'should attach files on post update' do
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)
    fr2 = FileResource.create(attachment: pic.open)

    sign_in @user

    p = create(:post, user: @user, group: @group)
    p.file_resources << fr1

    patch :update, {id: p.id, title: 't', message: 'm', type: 'Question', group_id: @group.id,
                    file_ids: [fr2.id], format: :json}

    p.reload
    assert_equal 1, p.file_resources.count
    assert_equal fr2.id, p.file_resources.first.id

    patch :update, {id: p.id, title: 't', message: 'm', type: 'Question', group_id: @group.id, file_ids: [], format: :json}
    p.reload
    assert_equal 0, p.file_resources.count
  end

  test 'faculty should be able to create card from post' do
    sign_in @faculty
    UrlMetadataExtractor.expects(:get_metadata).returns({'og:type' => 'video', 'title' => 'somev'}).once
    p = create(:question, message: 'http://youtube.com?v=342', user: @faculty, group: @group)

    assert_difference -> {Card.count}, 1 do
      post :convert_to_card, id: p.id
      assert_response :created
    end
  end

  test 'student should not be able to create card from post' do
    sign_in @user
    UrlMetadataExtractor.expects(:get_metadata).returns({'og:type' => 'video', 'title' => 'somev'}).once
    p = create(:question, message: 'http://youtube.com?v=342', user: @faculty, group: @group)

    assert_difference -> {Card.count}, 0 do
      post :convert_to_card, id: p.id
      assert_response :unauthorized
    end
  end

  test 'should not create card in absence of resource' do
    sign_in @faculty
    p = create(:question, user: @faculty, group: @group)

    assert_difference -> {Card.count}, 0 do
      post :convert_to_card, id: p.id
      assert_response :unprocessable_entity
      response = get_json_response(@response)
      assert_equal 'Cannot create a card since the post does not have a media resource.', response['error']
    end
  end

  test 'should process new tags on card' do
    sign_in @faculty
    UrlMetadataExtractor.expects(:get_metadata).returns({'og:type' => 'video', 'title' => 'somev'}).once
    p = create(:question, message: 'http://youtube.com?v=342', user: @faculty, group: @group)

    assert_difference -> {Card.count}, 1 do
      post :convert_to_card, id: p.id, tags: '#tag1 #tag2 #tag3'
      assert_response :created
      response = get_json_response(@response)
      card = Card.find(response['card']['id'])
      assert_equal ['tag1', 'tag2', 'tag3'], card.tags.pluck(:name)
    end
  end

  class ChannelPostsTest < ActionController::TestCase
    include Devise::Test::ControllerHelpers
    setup do
      skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'

      @controller = PostsController.new
      @org = create(:organization)
      @user = create(:user, organization: @org)
      @channel = create(:channel, organization: @org)
      create(:follow, followable: @channel, user: @user)
    end

    test ':api create a post in a channel' do
      sign_in @user
      assert_difference ->{Post.count} do
        post :create, {:title => 'title',
                       :message => 'message',
                       :type => 'Question',
                       :channel_id => @channel.id,
                       :format => :json}
        assert_response :created
      end

      p = Post.last
      patch :update, channel_id: @channel.id, id: p.id, title: "updated title", format: :json
      assert_response :ok
      assert_equal "updated title", p.reload.title

      assert_difference ->{Post.count}, -1 do
        delete :destroy, channel_id: @channel.id, id: p.id, format: :json
        assert_response :ok
      end
      sign_out @user
    end
  end

  test "#filter returns posts count for filtered results" do
    skip if ENV['exclude_search_tests']=='true'

    WebMock.disable!

    post1 = create :question, group: @group, user: @faculty, pinned: true
    post2 = create :question, group: @group, user: @user
    announcement = create :announcement, group: @group, user: @faculty
    flagged = create :question, group: @group, user: @faculty, hidden: true
    flagged_pinned_post = create :question, group: @group, user: @faculty, pinned: true, hidden: true
    Post.reindex
    Post.searchkick_index.refresh
    sign_in @faculty

    get :filters, { group_id: @group.id, format: :json }

    body = JSON.parse(response.body)

    assert_equal body['allDiscussions'], 3
    assert_equal body['pinnedDiscussions'], 1
    assert_equal body['flaggedDiscussions'], 2
    assert_equal body['announcements'], 1
    assert_equal body['myDiscussions'], 2
    assert_equal body['followingDiscussions'], 2

    WebMock.enable!
  end

  test '#index returns exact search results for hash_tag keywords' do
    skip if ENV['exclude_search_tests']=='true'

    WebMock.disable!

    post1 = create :question, group: @group, user: @faculty, pinned: true, title: 'Non-hashtag'
    post2 = create :question, group: @group, user: @user, title: '#hashtag'
    announcement = create :announcement, group: @group, user: @faculty, title: '#hashtag1'
    flagged = create :question, group: @group, user: @faculty, hidden: true
    flagged_pinned_post = create :question, group: @group, user: @faculty, pinned: true, hidden: true
    Post.reindex
    Post.searchkick_index.refresh
    sign_in @faculty

    # exact search for hash-tag
    get :index, group_id: @group.id, search: '#hashtag', format: :json
    assert_equal JSON.parse(response.body).collect{ |p| p['id'] } , [post2.id]

    # exact search for another hash-tag
    get :index, group_id: @group.id, search: '#hashtag1', format: :json
    assert_equal JSON.parse(response.body).collect{ |p| p['id'] } , [announcement.id]

    # fuzzy search for regular string
    get :index, group_id: @group.id, search: 'hashtag', format: :json
    assert_equal JSON.parse(response.body).collect{ |p| p['id'] }.to_set, [post1.id, post2.id, announcement.id].to_set

    WebMock.enable!
  end

  test '#index returns post using client_resource_id' do
    skip if ENV['exclude_search_tests']=='true'

    WebMock.disable!
    @group.update_attribute(:client_resource_id,  1234)
    post1 = create :question, group: @group, user: @faculty, pinned: true, title: 'Non-hashtag'
    post2 = create :question, group: @group, user: @user, title: '#hashtag'
    announcement = create :announcement, group: @group, user: @faculty, title: '#hashtag1'
    flagged = create :question, group: @group, user: @faculty, hidden: true
    flagged_pinned_post = create :question, group: @group, user: @faculty, pinned: true, hidden: true
    Post.reindex
    Post.searchkick_index.refresh
    PostRead.create_index! force: true
    sign_in @faculty

    # exact search for hash-tag
    get :index, client_resource_id: @group.client_resource_id, search: '#hashtag', format: :json

    assert_equal JSON.parse(response.body).collect{ |p| p['id'] } , [post2.id]

    # exact search for another hash-tag
    get :index, group_id: @group.id, search: '#hashtag1', format: :json
    assert_equal JSON.parse(response.body).collect{ |p| p['id'] } , [announcement.id]

    # fuzzy search for regular string
    get :index, group_id: @group.id, search: 'hashtag', format: :json
    assert_equal JSON.parse(response.body).collect{ |p| p['id'] }.to_set, [post1.id, post2.id, announcement.id].to_set

    WebMock.enable!
  end

end

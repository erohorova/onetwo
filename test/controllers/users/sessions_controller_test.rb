require 'test_helper'

class Users::SessionsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    stub_ip_lookup(ip_address: '0.0.0.0', response_body: {"time_zone" => "Asia/Tokyo"})
  end

  test "failing email login does not trigger MetricsRecorderJob" do
    organization = create(:organization)
    client = create(:client, organization: organization)
    user = create(:user, organization: organization)

    @controller.stubs(:identity_cache_memoization)
    @request.env["devise.mapping"] = Devise.mappings[:user]

    org_user = user.clone_for_org(organization, 'member')
    user.clients_users.create(client: client, client_user_id: '12345')
    credential = client.valid_credential
    set_host(organization)
    Analytics::MetricsRecorderJob.expects(:perform_later).never
    post :create, api_key: credential.api_key, token: "someinvalidtoken", next: '/somewhere'
  end

  test "successful email login triggers MetricsRecorderJob" do
    create_default_ssos
    Organization.any_instance.unstub :default_sso

    create_default_org
    stub_view_runtime = 123.456
    stub_time = Time.now
    stub_user = create(:user, organization: Organization.default_org)
    set_host stub_user.organization

    @request.env["devise.mapping"] = Devise.mappings[:user]
    Users::SessionsController.any_instance.stubs(:view_runtime).returns(stub_view_runtime)
    payload = {
      controller: "Users::SessionsController",
      action: "create",
      method: "POST",
      path: "/auth/users/sign_in",
      user_id: stub_user.id,
      platform: "web",
      request_ip: "0.0.0.0",
      request_host: "www.test.host",
      build_number: nil,
      platform_version_number: nil,
      user_agent: "Rails Testing",
      status: 302,
      org_id: stub_user.organization_id,
      view_runtime: stub_view_runtime,
      searchkick_runtime: 0,
      db_runtime: 0.0,
      referrer: nil,
      host: "www.test.host"
    }
    recorder_args = {
      recorder: "Analytics::UserLoginMetricsRecorder",
      metadata: payload,
      timestamp: stub_time.to_i,
      actor: Analytics::MetricsRecorder.user_attributes(stub_user).merge(
        "current_sign_in_at" => stub_time.to_i,
        "current_sign_in_ip" => "0.0.0.0",
        "last_sign_in_at" => stub_time.to_i,
        "remember_created_at" => stub_time.to_i,
        "sign_in_count" => 1,
        "last_sign_in_ip" => "0.0.0.0",
        "updated_at" => stub_time.to_i
      ),
      org: Analytics::MetricsRecorder.org_attributes(stub_user.organization),
    }
    EdcastActiveJob.unstub :perform_later
    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(:perform_later).with(recorder_args)
    Timecop.freeze(stub_time) do
      post :create, {
        user: {
          email: stub_user.email, password: stub_user.password
        }, next: '/next/path'
      }
    end
  end

  test 'should redirect user to the next parameter' do
    # create_default_org
    create_default_ssos
    Organization.any_instance.unstub :default_sso

    user = create(:user)
    set_host user.organization
    @request.env["devise.mapping"] = Devise.mappings[:user]
    post :create, user: {email: user.email, password: user.password}, next: '/next/path'

    assert_redirected_to '/next/path'
  end

# -----------------------------------------------------------------------------
# COMMENTED OUT UNTIL WE FIX BROWSERIFY IN TEST ENVIRONMENT
# -----------------------------------------------------------------------------

=begin

  test 'should render the page to select different org' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    get :choose_organization
    assert_response :success
  end

=end

  test 'switch organization' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    org = create(:organization)
    set_host org

    post :switch_organization, organization: {host_name: org.host_name}
    assert_equal org.host, URI.parse(response.redirect_url).hostname
    assert_redirected_to new_user_session_url(host: org.host)
  end

# -----------------------------------------------------------------------------
# COMMENTED OUT UNTIL WE FIX BROWSERIFY IN TEST ENVIRONMENT
# -----------------------------------------------------------------------------

=begin

  test 'switch organization should fail on bad host name' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    post :switch_organization, organization: {host_name: 'junk'}
    assert assigns[:error]
    assert_template :choose_organization
  end

=end

  test 'should send redirect uri after sign in' do
    # create_default_org
    # WebMock.disable!
    create_default_ssos
    Organization.any_instance.unstub :default_sso

    user = create(:user)
    Search::UserSearch.any_instance.stubs(:search_by_id).returns(user.search_data)
    redirect_url = 'https://www.google.com'
    set_host user.organization

    @request.env["devise.mapping"] = Devise.mappings[:user]
    post :create, user: {email: user.email, password: user.password}, consumer_redirect_uri:  redirect_url, format: :json
    assert_response :ok
    resp = get_json_response response
    assert_equal user.id, resp['id']

    assert_not_nil resp['consumer_redirect_uri']
    assert_equal redirect_url, resp['consumer_redirect_uri']

    # WebMock.enable!
  end

  test 'should redirect on sign_in for signed in users' do
    user = create(:user)
    sign_in user
    @request.env['HTTP_REFERER'] = 'http://www.google.com'
    @request.env["devise.mapping"] = Devise.mappings[:user]
    get :new
    assert_response :redirect
    assert_redirected_to root_path
  end

  test 'should authenticate and redirect user to the next page' do
    client = create(:client)
    user = create(:user)
    organization = create(:organization, savannah_app_id: "123")
    org_user = user.clone_for_org(organization, 'member')
    @request.env["devise.mapping"] = Devise.mappings[:user]
    set_host(organization)
    token = JWT.encode({savannah_app_id: "123", destiny_user_id: org_user.id}, Settings.savannah_token)
    post :create, savannah_app_id: "123", destiny_user_id: org_user.id, auth_token: token, next: '/somewhere'
    assert_equal org_user, @controller.current_user
  end

# -----------------------------------------------------------------------------
# COMMENTED OUT UNTIL WE FIX BROWSERIFY IN TEST ENVIRONMENT
# -----------------------------------------------------------------------------

=begin

  test 'should fail to authenticate if token is invalid' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    client = create(:client)
    user = create(:user)
    organization = create(:organization, client: client)
    org_user = user.clone_for_org(organization, 'member')
    user.clients_users.create(client: client, client_user_id: '12345')
    credential = client.valid_credential
    set_host(organization)
    post :create, api_key: credential.api_key, token: "someinvalidtoken", next: '/somewhere'
    assert_equal false, @controller.user_signed_in?
    assert_template :new, 'render the log in page'
  end

=end

# -----------------------------------------------------------------------------
# COMMENTED OUT UNTIL WE FIX BROWSERIFY IN TEST ENVIRONMENT
# -----------------------------------------------------------------------------

=begin

  test 'should fail to authenticate if auth_token is invalid with savannah_app_id' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    user = create(:user)
    organization = create(:organization, savannah_app_id: "123")
    org_user = user.clone_for_org(organization, 'member')
    set_host(organization)
    post :create, savannah_app_id: "123", destiny_user_id: org_user.id, auth_token: "invalidtoken", next: '/somewhere'
    assert_equal false, @controller.user_signed_in?
    assert_template :new, 'render the log in page'
  end


  test 'should fail authentication if the organization host name does not match with the apikey' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @request.host = Organization.default_org.host
    client = create(:client)
    user = create(:user)
    organization = create(:organization, client: client)
    org_user = user.clone_for_org(organization, 'member')
    user.clients_users.create(client: client, client_user_id: '12345')
    credential = client.valid_credential
    post :create, api_key: credential.api_key, token: JWT.encode({userId: '12345'}, credential.shared_secret), next: '/somewhere'
    assert_equal false, @controller.user_signed_in?
    assert_template :new_default, 'render the log in page'
  end

  test 'should not allow user to login if email option is disabled' do
    email_sso = Sso.where(name: "email").first
    org_sso = Organization.default_org.org_ssos.where(sso: email_sso).first!
    org_sso.update!(is_enabled: false)
    user = create(:user)
    @request.env["devise.mapping"] = Devise.mappings[:user]
    post :create, user: {email: user.email, password: user.password}
    assert_response :forbidden
  end

  test 'should deeplink to facebook' do
    org = create(:organization)
    facebook = Sso.where(name: "facebook").first
    org.ssos = [facebook]
    assert_equal org.available_ssos, { "facebook" => nil }
    set_host(org)
    @request.env["devise.mapping"] = Devise.mappings[:user]
    get :new, deeplink: 'true'
    assert_response :redirect
    assert_match '/auth/facebook', redirect_to_url
  end


  test 'should render the find organization page' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    get :find_organization
    assert_response :success
  end

  test 'should render the confirmation reminder page' do
    org1 = create(:organization, is_open: true)
    org2 = create(:organization, is_open: true)
    email = generate(:email_address)
    user1 = create(:user, email: email, organization: org1, organization_role: 'member')
    user2 = create(:user, email: email, organization: org2, organization_role: 'member')
    MandrillMailer.any_instance.expects(:send_api_email).once
    @request.env["devise.mapping"] = Devise.mappings[:user]

    post :find_organization, user: {email: email}
    assert_response :success
  end

=end

  test 'should save time zone of user if not present' do
    SetUserTimeZoneJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    User.any_instance.unstub :set_time_zone

    create_default_ssos
    Organization.any_instance.unstub :default_sso

    # create_default_org
    user = create(:user)
    set_host user.organization
    @request.env["devise.mapping"] = Devise.mappings[:user]

    assert_difference ->{UserProfile.count} do
      post :create, user: {email: user.email, password: user.password}
    end

    assert_equal "Asia/Tokyo", user.profile.time_zone

    stub_ip_lookup(ip_address: "0.0.0.0", response_body: {"time_zone" => "America/Los_Angeles"})
    assert_no_difference ->{UserProfile.count} do
      post :create, user: {email: user.email, password: user.password}
    end

    # doesn't change time zone if present
    assert_equal "Asia/Tokyo", user.profile.time_zone
  end

  test 'should save terms accepted status and date of user if not present' do
    EdcastActiveJob.unstub :perform_later

    create_default_ssos
    Organization.any_instance.unstub :default_sso

    # create_default_org
    user = create(:user)
    set_host user.organization
    @request.env["devise.mapping"] = Devise.mappings[:user]

    assert_difference ->{UserProfile.count} do
      post :create, user: {email: user.email, password: user.password, terms_accepted: true}
    end

    assert_equal true, user.profile.tac_accepted
    assert_equal Date.today, user.profile.tac_accepted_at.to_date

    assert_no_difference ->{UserProfile.count} do
      post :create, user: {email: user.email, password: user.password, terms_accepted: true}
    end
  end

  test 'should sign out user #custom_domain' do
    org = create(:organization, custom_domain: 'something.cool.com')
    user = create(:user, organization: org)
    @request.env["devise.mapping"] = Devise.mappings[:user]
    set_host org
    sign_in user

    delete :destroy

    assert_redirected_to "/log_in"
  end

  test 'should respond with 404 if invalid format resquested' do
    get :new, format: :ddwdw
    assert_response 404

    get :new, format: :bak
    assert_response 404

    get :new, format: :pqr
    assert_response 404

    get :new, format: :abc
    assert_response 404
  end

  test 'should not respond with 404 if valid format resquested' do
    get :new
    assert_response 302
  end

  test 'should invoke request recorder #sign_in_request' do
    # create_default_org
    Analytics::MetricsRecorderJob.unstub(:perform_later)

    user = create(:user)
    set_host user.organization
    @request.env["devise.mapping"] = Devise.mappings[:user]
    Analytics::MetricsRecorderJob.expects(:perform_later)

    post :create, user: {email: user.email, password: user.password}
  end

  test 'should lock account after 5 unsuccessful login if locking enabled' do
    create_default_ssos
    Organization.any_instance.unstub :default_sso

    org = create(:organization)
    set_host org
    user = create(:user, failed_attempts: 4, organization: org)
    Organization.any_instance.stubs(:account_lockout_enabled?).returns(true)
    @request.env["devise.mapping"] = Devise.mappings[:user]
    post :create, user: {email: user.email, password: "wrong password"}, format: :json
    assert_response :unauthorized
    assert user.reload.account_locked?
    assert_equal 5, user.failed_attempts
    Organization.any_instance.unstub(:account_lockout_enabled?)
  end

  test 'should not set failed attempts in order to lock account if locking is disabled' do
    create_default_ssos
    Organization.any_instance.unstub :default_sso

    org = create(:organization)
    set_host org
    user = create(:user, organization: org)
    @request.env["devise.mapping"] = Devise.mappings[:user]
    post :create, user: {email: user.email, password: "wrong password"}, format: :json
    assert_response :unauthorized
    refute user.reload.account_locked?
    assert_equal 0, user.failed_attempts
  end

  test 'should remember user when config is not present' do
    create_default_ssos
    Organization.any_instance.unstub :default_sso
    org = create(:organization)
    set_host org
    user = create(:user, organization: org)
    @request.env["devise.mapping"] = Devise.mappings[:user]
    create :config, :boolean, name: 'sessionExpireOnBrowserClose', value: false, configable: org

    post :create, user: {email: user.email, password: user.password}, format: :json

    assert_not_nil user.reload.remember_created_at
  end

  test 'should remember user when config is set to false' do
    create_default_ssos
    Organization.any_instance.unstub :default_sso
    org = create(:organization)
    set_host org
    user = create(:user, organization: org)
    @request.env["devise.mapping"] = Devise.mappings[:user]
    create :config, :boolean, name: 'sessionExpireOnBrowserClose', value: false, configable: org

    post :create, user: {email: user.email, password: user.password}, format: :json

    assert_not_nil user.reload.remember_created_at
  end

  test 'should not remember user when config is set to true' do
    create_default_ssos
    Organization.any_instance.unstub :default_sso
    org = create(:organization)
    set_host org
    user = create(:user, organization: org)
    @request.env["devise.mapping"] = Devise.mappings[:user]
    create :config, :boolean, name: 'sessionExpireOnBrowserClose', value: true, configable: org

    post :create, user: {email: user.email, password: user.password}, format: :json

    assert_nil user.reload.remember_created_at
  end

  test 'should login with encrypted payload password1' do
    create_default_ssos
    Organization.any_instance.unstub :default_sso
    org = create(:organization)
    set_host org

    create :config, :boolean, name: 'encryption_payload', value: 'true', configable: org

    user = create(:user, organization: org)
    # Encryption of Password
    public_key = OpenSSL::PKey::RSA.new(ENV['JS_PUBLIC_KEY_RAILS_TEST'].gsub("\\n", "\n"))
    encrypted_password = Base64.encode64(public_key.public_encrypt(user.password))

    # Decryption of Password
    private_key = OpenSSL::PKey::RSA.new(ENV['JS_PRIVATE_KEY_TEST'].gsub("\\n", "\n"))
    user.password = private_key.private_decrypt(Base64.decode64(encrypted_password))

    @request.env["devise.mapping"] = Devise.mappings[:user]
    post :create, user: {email: user.email, password: user.password}, format: :json
    assert_response :ok
  end

  class FederatedServiceTest < ActionController::TestCase
    include Devise::Test::ControllerHelpers

    setup do
      Config.any_instance.stubs :create_okta_group_config

      create_default_ssos
      Organization.any_instance.unstub :default_sso
      stub_ip_lookup(ip_address: '0.0.0.0', response_body: { 'time_zone' => 'Asia/Tokyo' })
      stub_request(:get, "http://localhost:9200/edcast_users_test/_search").to_return(status: 200, body: '', headers: {})

      create_default_org
      @org  = Organization.default_org
      @user = create :user, organization: @org
      # set_host @org
      create :config, :boolean, name: 'app.config.okta.enabled', value: 'true', configable: @org
      create :config, name: 'OktaApiToken', value: SecureRandom.hex, configable: @org

      @request.env['devise.mapping'] = Devise.mappings[:user]
    end

    test 'sets `:consumer_redirect_uri` when Okta API token is configured' do
      @organization = @user.organization
      stub_okta_user

      post :create, user: { email: @user.email, password: @user.password }, format: :json

      assert_response :ok
      resp = get_json_response response

      assert_match /https:\/\/dev-579575.oktapreview.com\/login\/sessionCookieRedirect/, resp['consumer_redirect_uri']
    end

    test 'creates user in the federated system when `federated_identifier` is blank' do
      user_json = File.read(Rails.root.join('test/fixtures/api/okta_user.json'))
      # Stub API to find existing user but returns NIL
      Okta::InternalFederation.any_instance.stubs(:federated_user).returns(nil)

      # Stub API to create new user in federated system and is successful
      Okta::InternalFederation.any_instance.expects(:create_user).once

      post :create, user: { email: @user.email, password: @user.password }, format: :json

      assert_response :ok
    end

    test 'links user with the new federated user by updating `federated_identifier`' do
      user_json = JSON.parse(File.read(Rails.root.join('test/fixtures/api/okta_user.json')))
      # Stub API to find existing user but returns NIL
      Okta::InternalFederation.any_instance.stubs(:federated_user).returns(nil)

      # Stub API to create new user in federated system and is successful
      Okta::InternalFederation.any_instance.expects(:create_user).returns(user_json).once
      #Stub API to create sessionToken
      stub_okta_session_token

      post :create, user: { email: @user.email, password: @user.password }, format: :json

      assert_response :ok

      assert_equal '2-okta@edcast.com', @user.reload.federated_identifier
      assert_nil LxpOauthProvider.find_by(user: @user)
    end

    test 'links user with the existing federated user by updating `federated_identifier`' do
      user_json = JSON.parse(File.read(Rails.root.join('test/fixtures/api/okta_user.json')))
      # Stub API to find existing user and is successful
      Okta::InternalFederation.any_instance.stubs(:federated_user).returns(user_json).once

      #Stub API to create sessionToken
      stub_okta_session_token

      post :create, user: { email: @user.email, password: @user.password }, format: :json

      assert_response :ok

      assert_equal '2-okta@edcast.com', @user.reload.federated_identifier
      assert_nil LxpOauthProvider.find_by(user: @user)
    end

    test 'sets :consumer_redirect_uri when Okta API token is configured for mobile' do
      stub_request(:get, "http://localhost:9200/edcast_users_test/_search").to_return(status: 200, body: '', headers: {})

      user_json = JSON.parse(File.read(Rails.root.join('test/fixtures/api/okta_user.json')))
      # Stub API to find existing user but returns NIL
      Okta::InternalFederation.any_instance.stubs(:federated_user).returns(nil)
      # Stub API to create new user in federated system and is successful
      Okta::InternalFederation.any_instance.expects(:create_user).returns(user_json).once

      stub_okta_session_token

      # Ensures HTTP_USER_AGENT = `Android`
      @request.user_agent = 'Android'
      post :create, user: { email: @user.email, password: @user.password }, format: :json

      assert_response :ok
      resp = get_json_response response
      mobile_redirect_uri = CGI.parse(URI.parse(resp['consumer_redirect_uri']).query)['redirectUrl'][0]
      assert_match /http:\/\/www.test.host\/auth\/native_scheme_redirect/, mobile_redirect_uri
    end
  end

  class UserStatusAfterLogin < ActionController::TestCase

    setup do
      create_default_ssos
      Organization.any_instance.unstub :default_sso
      @org = create(:organization)
      set_host @org
    end

    test '#create should return unauthorized if status suspended' do
      user = create(:user, organization: @org, status: User::SUSPENDED_STATUS)
      @request.env["devise.mapping"] = Devise.mappings[:user]
      Search::UserSearch.any_instance.stubs(:search_by_id).returns(user.search_data)
      post :create, user: {email: user.email, password: user.password}, format: :json
      assert_response :unauthorized
    end

    test '#create should switch user status to active' do
      create_default_ssos
      Organization.any_instance.unstub :default_sso
      org = create(:organization)
      set_host org
      user = create(:user, organization: org, status: User::INACTIVE_STATUS)
      @request.env["devise.mapping"] = Devise.mappings[:user]
      Search::UserSearch.any_instance.stubs(:search_by_id).returns(user.search_data)
      post :create, user: {email: user.email, password: user.password}, format: :json
      assert_response :ok
      assert_equal User::ACTIVE_STATUS, user.reload.status
    end

    test 'should update sign_out_at to nil on sign in' do
      user = create(:user, organization: @org, sign_out_at: DateTime.now)
      @request.env["devise.mapping"] = Devise.mappings[:user]
      post :create, user: { email: user.email, password: user.password }, format: :json
      resp = get_json_response(response)
      assert_equal user.id, resp['id']
      assert_nil user.reload.sign_out_at
    end
  end

  class UserStatusAfterSignOut < ActionController::TestCase
    setup do
      create_default_ssos
      Organization.any_instance.unstub :default_sso
      @org = create(:organization)
      set_host @org
    end

    test 'should set sign_out_at after sign out' do
      user = create(:user, organization: @org)
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in user
      delete :destroy
      assert_redirected_to '/log_in'
      assert_not_nil user.reload.sign_out_at
    end

    test 'removes filestack cookie after logout' do
      user = create(:user, organization: @org)
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in user
      assert_equal user, @controller.current_user
      assert cookies[:filestack].present?

      delete :destroy
      refute cookies[:filestack].present?
    end
  end
end

require 'test_helper'
require 'jwt'

class Api::Developer::V5::UsersControllerTest < ActionController::TestCase

  setup do
    @organization = create(:organization, enable_role_based_authorization: true)
    @user = create(:user, organization: @organization)
    creator = create(:user, organization: @organization)

    Role.create_standard_roles(@organization)
    member_role =  @organization.roles.find_by(name: 'member')
    member_role.add_user(@user)
    create(:role_permission, role: member_role, name: Permissions::DEVELOPER )
    @credential = create(:developer_api_credential,
      organization: @organization,
      creator: creator
    )
  end

  test "clc should return progress with for user" do
    set_dev_v5_api_request_header(@user, @credential)
    clc = create(:clc, name: 'test clc', entity: @organization, organization: @organization)
    clc_progress = create(:clc_progress, user: @user, clc: clc)

    get :clc, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal clc.id, resp["data"]["clc_id"]
  end
end
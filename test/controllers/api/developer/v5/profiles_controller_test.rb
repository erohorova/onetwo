require 'test_helper'
require 'jwt'

class Api::Developer::V5::ProfilesControllerTest < ActionController::TestCase
  setup do
    @org = create(:organization, enable_role_based_authorization: true)
    @user = create(:user, organization: @org)
    creator = create(:user, organization: @org)

    Role.create_standard_roles(@org)
    member_role =  @org.roles.find_by(default_name: 'member')
    member_role.add_user(@user)
    create(:role_permission, role: member_role, name: Permissions::DEVELOPER )
    @credential = create(:developer_api_credential,
                         organization: @org,
                         creator: creator
    )
    @users = create_list(:user, 3, organization: @org)
    # external profile creation is done by Background job.
    [@user, creator, @users].flatten.each {|user| user.create_external_profile}
  end

  class ProfileIndexTest < Api::Developer::V5::ProfilesControllerTest
    test 'authenticate_api_request! method' do

      set_dev_v5_api_request_header(@user, @credential)

      request.host = 'sagar'

      get :index, format: :json
      resp = get_json_response response
      assert_response :unauthorized
      assert_equal 'Could not authenticate with API KEY and ACCESS TOKEN', resp['message']
    end

    test 'should return profiles' do
      set_dev_v5_api_request_header(@user, @credential)

      get :index, format: :json

      assert_response :ok
      resp = get_json_response response

      assert_equal 5, resp['profiles'].count
      assert resp['profiles'][0].keys.include?('job_title')
    end
  end

  class ProfileCreateTest < Api::Developer::V5::ProfilesControllerTest
    test 'should return new profile' do
      pic = Rails.root.join('test/fixtures/images/logo.png')
      set_dev_v5_api_request_header(@user, @credential)
      profile = {
        email: 'new@test.com',
        external_id: 'zxc4123',
        uid: '12545654abc',
        first_name: 'User',
        last_name: 'New',
        avatar: pic.open
      }

      post :create, profiles: [profile], format: :json

      assert_response :ok
      resp = get_json_response(response)['profiles'][0]
      assert_equal 'new@test.com', resp['email']
      assert_equal 'zxc4123', resp['external_id']
      assert_equal 'User', resp['first_name']
      assert_equal 'New', resp['last_name']
      assert_not_empty resp['avatar']
      assert_nil User.last.confirmed_at
    end

    test 'should create profile with field language' do
      set_dev_v5_api_request_header(@user, @credential)

      profile = {
        email: 'qwe@qwe.qwe',
        external_id: 'zxc4123',
        uid: '12545654abc',
        first_name: 'User',
        last_name: 'New',
        language: 'en'
      }

      assert_difference -> {Profile.count}, 1 do
        post :create, profiles: [profile], format: :json
      end

      assert_response :ok
      resp = get_json_response(response)['profiles'][0]

      assert_equal 'en', resp['language']
    end

    test 'should return new profile and set role member by default' do
      # Organization.any_instance.unstub(:create_default_roles)
      set_dev_v5_api_request_header(@user, @credential)
      role = @org.roles.find_by(default_name: 'member')
      profile = {
        email: 'qwe@qwe.qwe',
        external_id: 'zxc4123',
        uid: '12545654abc',
        first_name: 'User',
        last_name: 'New'
      }
      assert_difference -> {Profile.count}, 1 do
        post :create, profiles: [profile], format: :json
      end

      assert_response :ok
      resp = get_json_response(response)['profiles'][0]

      user = User.last
      assert_equal user.email, resp['email']
      assert_equal 'zxc4123', resp['external_id']
      assert_includes role.user_ids, user.id
    end

    test 'should return new profile and set role for user' do
      set_dev_v5_api_request_header(@user, @credential)
      role_member = @org.roles.find_by(default_name: 'member')
      role_admin = @org.roles.find_by(default_name: 'admin')
      invalid_role = ['test']
      profile = {
        email: 'qwe@qwe.qwe',
        external_id: 'zxc4123',
        uid: '12545654abc',
        first_name: 'User',
        last_name: 'New',
        roles: ['admin'].push(*invalid_role)
      }

      assert_difference -> {Profile.count}, 1 do
        post :create, profiles: [profile], format: :json
      end

      assert_response :ok
      full_response = get_json_response(response)
      resp = full_response['profiles'][0]
      message = full_response['messages']['0']
      user = User.last
      assert_equal user.email, resp['email']
      assert_equal 'zxc4123', resp['external_id']
      assert_same_elements ['member', 'admin'], resp['roles']
      assert_includes role_member.user_ids,  user.id
      assert_includes role_admin.user_ids,  user.id
      assert_includes message, "Invalid role names: #{invalid_role}"
      assert_not_includes user.roles.pluck(:name), invalid_role
    end

    test 'should created with custom fields' do
      set_dev_v5_api_request_header(@user, @credential)

      @org.custom_fields.create(display_name: 'manager_name')
      @org.custom_fields.create(display_name: 'country')
      profile = {
        email: 'qwe@qwe.qwe',
        external_id: 'zxc4123',
        first_name: 'User',
        last_name: 'New',
        country: 'Japan',
        manager_name: 'David',
        manager_id: '5'
      }
      post :create, profiles: [profile], format: :json

      assert_response :ok
      resp = get_json_response(response)['profiles'][0]
      assert_equal 'qwe@qwe.qwe', resp['email']
      assert_equal 'David', resp['manager_name']
      assert_equal 'Japan', resp['country']
      assert resp.exclude?('manage_id')
    end

    test 'should return missing required params' do
      set_dev_v5_api_request_header(@user, @credential)
      profile = {
        email: @users.first.email,
      }
      post :create, profiles: [profile], format: :json

      assert_response :ok
      message = get_json_response(response)['messages']['0']
      assert_includes message, 'Missing required params'
    end

    test 'should return message external already exist' do
      set_dev_v5_api_request_header(@user, @credential)
      external_id = @user.external_profile.external_id

      profile_params = {
        email: 'new_user@test.com',
        first_name: 'User',
        last_name: 'New',
        external_id: external_id
      }
      post :create, profiles: [profile_params], format: :json

      assert_response :ok
      resp = get_json_response(response)
      assert_includes resp['messages']['0'], 'External has already been taken'
    end

    test 'should create many profiles' do
      set_dev_v5_api_request_header(@user, @credential)
      profile = [
        {
          email: 'qwe@qwe.qwe',
          external_id: 'qwe123',
          first_name: 'User',
          last_name: 'New',
          uid: '123321qwe'
        },
        {
          email: 'asd@asd.asd',
          external_id: 'asd123',
          first_name: 'User1',
          last_name: 'New1',
          uid: '123321asd'
        }
      ]

      assert_difference [->{User.count},-> {Profile.count}], 2 do
        post :create, profiles: profile, format: :json
      end

      assert_response :ok
      resp = get_json_response(response)['profiles']

      assert_equal 'qwe@qwe.qwe', resp.first['email']
      assert_equal 'asd@asd.asd', resp.second['email']
      assert_equal 'qwe123', resp.first['external_id']
      assert_equal 'asd123', resp.second['external_id']
    end

    test 'should return new profile without confirmation if send_email_notification string value' do
      pic = Rails.root.join('test/fixtures/images/logo.png')
      set_dev_v5_api_request_header(@user, @credential)

      profile = {
        email: 'new@test.com',
        external_id: 'zxc4123',
        uid: '12545654abc',
        first_name: 'User',
        last_name: 'New',
        avatar: pic.open,
        send_email_notification: 'false'
      }
      post :create, profiles: [profile], format: :json

      assert_response :ok
      resp = get_json_response(response)['profiles'][0]
      assert_equal 'new@test.com', resp['email']
      assert_equal 'zxc4123', resp['external_id']
      assert_equal 'User', resp['first_name']
      assert_equal 'New', resp['last_name']
      assert_not_empty resp['avatar']
      assert_not_nil User.last.confirmed_at
    end

    test 'should return new profile without confirmation if send_email_notification boolean value' do
      pic = Rails.root.join('test/fixtures/images/logo.png')
      set_dev_v5_api_request_header(@user, @credential)

      profile = {
        email: 'new@test.com',
        external_id: 'zxc4123',
        uid: '12545654abc',
        first_name: 'User',
        last_name: 'New',
        avatar: pic.open,
        send_email_notification: false
      }
      post :create, profiles: [profile], format: :json

      assert_response :ok
      resp = get_json_response(response)['profiles'][0]
      assert_equal 'new@test.com', resp['email']
      assert_equal 'zxc4123', resp['external_id']
      assert_equal 'User', resp['first_name']
      assert_equal 'New', resp['last_name']
      assert_not_empty resp['avatar']
      assert_not_nil User.last.confirmed_at
    end

    test 'should return new profile with confirmation' do
      pic = Rails.root.join('test/fixtures/images/logo.png')
      set_dev_v5_api_request_header(@user, @credential)

      profile = {
        email: 'new@test.com',
        external_id: 'zxc4123',
        uid: '12545654abc',
        first_name: 'User',
        last_name: 'New',
        avatar: pic.open,
        send_email_notification: 'true'
      }
      post :create, profiles: [profile], format: :json

      assert_response :ok
      resp = get_json_response(response)['profiles'][0]
      assert_equal 'new@test.com', resp['email']
      assert_equal 'zxc4123', resp['external_id']
      assert_equal 'User', resp['first_name']
      assert_equal 'New', resp['last_name']
      assert_not_empty resp['avatar']
      assert_nil User.last.confirmed_at
    end


    test 'should return error message if not valid email format' do
      set_dev_v5_api_request_header(@user, @credential)
      profile = {
        email: 'new_user@test',
        external_id: 'zxc4123',
        uid: '12545654abc',
        first_name: 'User',
        last_name: 'New',
      }
      assert_no_difference -> { User.count } do
        post :create, profiles: [profile], format: :json
      end
      assert_response :ok
      resp = get_json_response(response)
      assert_includes resp['messages']['0'], 'Email address is invalid'
      assert_empty resp['profiles']
    end

    test 'should create new profile with job_title' do
      set_dev_v5_api_request_header(@user, @credential)
      profile = {
          external_id: "123abc",
          email: "dev@edcast.com",
          first_name: "Fname",
          last_name: "Lname",
          job_title: "Software Engineer",
          roles: ["member"]
      }

      post :create, profiles: [profile], format: :json

      assert_response :ok
      resp = get_json_response(response)
      assert_empty resp["messages"]["0"]
    end

    test 'should display error message if job_title validation fails' do
      set_dev_v5_api_request_header(@user, @credential)

      profile = {
          external_id: "1234abcd",
          email: "dev1@edcast.com",
          first_name: "Fname",
          last_name: "Lname",
          job_title: "A",
          roles: ["member"]
      }

      post :create, profiles: [profile], format: :json

      assert_response :ok
      resp = get_json_response(response)
      assert_nil resp['job_title']
      assert_equal 1, resp["messages"]["0"].length
      assert_equal "Job title is too short (minimum is 2 characters)", resp["messages"]["0"][0]
    end
  end

  class ProfileUpdateTest < Api::Developer::V5::ProfilesControllerTest
    test 'should return update profile' do
      set_dev_v5_api_request_header(@user, @credential)

      uid = @users.first.external_profile.uid
      profile = {
        external_id: 'zxc4123'
      }

      put :update, id: uid, profile: profile, format: :json

      assert_response :ok
      resp = get_json_response response

      assert_equal @users.first.email, resp['email']
      assert_equal 'zxc4123', resp['external_id']
    end

    test 'should update value custom_field' do
      set_dev_v5_api_request_header(@user, @credential)

      field = @org.custom_fields.create(display_name: 'country')
      @users.first.assigned_custom_fields.create(
        custom_field_id: field.id,
        value: 'Mexico'
      )
      uid = @users.first.external_profile.uid
      profile = {
        external_id: 'zxc4123',
        country: 'Japan',
        manage_id: 1
      }

      put :update, id: uid, profile: profile, format: :json

      assert_response :ok
      resp = get_json_response response

      assert_equal @users.first.email, resp['email']
      assert_equal 'Japan', resp['country']
      assert_includes resp['message'],'manage_id key is missing in custom fields'
    end

    test 'should update language' do
      set_dev_v5_api_request_header(@user, @credential)

      @users.first.create_profile(language: 'kz')
      uid = @users.first.external_profile.uid
      profile = {
        external_id: 'zxc4123',
        language: 'en',
        manage_id: 1
      }

      put :update, id: uid, profile: profile, format: :json

      assert_response :ok
      resp = get_json_response response

      assert_equal 'en', resp['language']
    end

    test 'should add role for user' do
      set_dev_v5_api_request_header(@user, @credential)
      role = @org.roles.find_by(default_name: 'admin')
      invalid_role = ['test']
      uid = @users.first.external_profile.uid
      profile = {
        roles: ['admin'].push(*invalid_role),
      }
      put :update, id: uid, profile: profile, format: :json

      assert_response :ok
      resp = get_json_response response

      assert_includes role.user_ids, @users.first.id
      assert_includes resp['message'], "Invalid role names: #{invalid_role}"
      assert_not_includes @users.first.roles.pluck(:name), invalid_role
    end

    test 'should remove role for user' do
      set_dev_v5_api_request_header(@user, @credential)

      member_role = @org.roles.find_by(default_name: 'member')
      admin_role = @org.roles.find_by(default_name: 'admin')
      member_role.add_user(@users.first)
      invalid_role = ['test']
      uid = @users.first.external_profile.uid
      profile = {
        roles: ['admin'].push(*invalid_role),
      }
      put :update, id: uid, profile: profile, format: :json

      assert_response :ok
      resp = get_json_response response

      assert_includes admin_role.user_ids, @users.first.id
      assert_not_includes member_role.user_ids, @users.first.id
      assert_includes resp['message'], "Invalid role names: #{invalid_role}"
      assert_not_includes @user.roles.pluck(:name), invalid_role
    end

    test 'should update avatar profile' do
      pic = Rails.root.join('test/fixtures/images/logo.png')
      set_dev_v5_api_request_header(@user, @credential)
      uid = @users.first.external_profile.uid
      profile = {
        avatar: pic.open
      }
      put :update, id: uid, profile: profile, format: :json

      assert_response :ok
      resp = get_json_response response

      assert_equal @users.first.email, resp['email']
      assert_not_empty resp['avatar']
    end

    test 'should update user job_title' do
      # create a user
      user = create :user, organization: @org

      # give role member as it is having DEVELOPER permission already
      member_role =  @org.roles.find_by(default_name: 'member')
      member_role.add_user(user)

      external_profile = user.create_external_profile
      profile = create :user_profile, user: user, job_title: 'A job title'
      set_dev_v5_api_request_header(@user, @credential)

      uid = user.external_profile.uid
      profile = {job_title: 'New job title'}
      put :update, id: uid, profile: profile, format: :json
      assert_response :ok
      resp = get_json_response response
      assert_equal 'New job title', resp['job_title']
      assert_equal 'New job title', user.reload.job_title
    end

    test 'should set user profile by email for user ' do
      # create a user
      user = create :user, organization: @org, email: 'test934@edcast.com'

      # give role member as it is having DEVELOPER permission already
      member_role =  @org.roles.find_by(default_name: 'member')
      member_role.add_user(user)

      external_profile = user.create_external_profile
      profile = create :user_profile, user: user, job_title: 'A job title'
      set_dev_v5_api_request_header(@user, @credential)

      profile = {job_title: 'New job title', email: 'test934@edcast.com'}
      put :update, id: 'cd-91-sh-23', profile: profile, format: :json
      assert_response :ok
      resp = get_json_response response
      assert_equal 'New job title', resp['job_title']
      assert_equal 'New job title', user.reload.job_title
    end
  end

  test 'anonymize a suspended user' do
    stub_request(:get, /new-anonymous-user-medium/).to_return(status: 200, body: "", headers: {})
    set_dev_v5_api_request_header(@user, @credential)
    user_to_update = @users.first
    uid = user_to_update.external_profile.uid
    user_to_update.update(status: User::SUSPENDED_STATUS, is_suspended: true)

    put :update, id: uid, profile: {'anonymize' => 'true'}, format: :json
    user_to_update.reload
    assert user_to_update.is_anonymized
  end

  test 'anonymize an active user' do
    set_dev_v5_api_request_header(@user, @credential)
    uid = @users.first.external_profile.uid
    put :update, id: uid, profile: {'anonymize' => 'true'}, format: :json
    resp = JSON.parse(response.body)
    assert_response :success
    assert_equal "Cannot anonymize user", resp["message"][0]
  end

  test 'cannot update anonymized user' do
    set_dev_v5_api_request_header(@user, @credential)
    user_to_update = @users.first
    uid = user_to_update.external_profile.uid
    user_to_update.update(status: User::SUSPENDED_STATUS, is_suspended: true, is_anonymized: true)
    user_to_update.reload
    put :update, id: uid, profile: {'first_name' => 'Test User'}, format: :json
    resp = JSON.parse(response.body)
    assert_response :unprocessable_entity
    assert_equal "Cannot update anonymized user", resp["message"]
  end

  test 'should return profile after update user data' do
    Users::AfterCommitOnCreateJob.unstub :perform_later
    set_dev_v5_api_request_header(@user, @credential)
    external_id = @users.first.external_profile.uid
    profile = {
      first_name: 'User',
      last_name: 'New',
    }
    put :update, id: external_id, profile: profile, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal external_id, resp['external_id']
    get :show, id: external_id, format: :json
    assert_response :ok
    resp_show = get_json_response(response)
    assert_equal external_id, resp_show['external_id']
  end

  test 'should destroy profile' do
    set_dev_v5_api_request_header(@user, @credential)
    uid = @users.first.external_profile.uid

    assert_difference -> {Profile.count}, -1 do
      delete :destroy, id: uid, format: :json
    end

    assert_response :ok
  end

  class ProfileCustomFieldsTest < Api::Developer::V5::ProfilesControllerTest
    test 'should return custom_fields profile' do
      set_dev_v5_api_request_header(@user, @credential)
      @user.organization.custom_fields.create(
        display_name: 'test_field',
        enable_people_search: true
      )

      get :index_custom_fields, format: :json
      assert_response :ok

      resp = get_json_response response
      assert_equal 1, resp['total_count']
      assert_equal 1, resp['custom_fields'].count
      assert_equal 'test_field', resp['custom_fields'].first['display_name']
    end

    test 'should create custom_fields' do
      set_dev_v5_api_request_header(@user, @credential)


      post :create_custom_fields, name_fields: ['manage_id'], format: :json
      assert_response :ok

      resp = get_json_response response

      assert_equal 'manage_id', resp['custom_fields'][0]['display_name']
      assert resp['custom_fields'][0].key? 'id'
    end
  end
end

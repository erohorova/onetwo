require 'test_helper'
require 'jwt'

class Api::Developer::V5::FeedControllerTest < ActionController::TestCase

  setup do
    @organization = create(:organization, enable_role_based_authorization: true)
    @user = create(:user, organization: @organization)
    creator = create(:user, organization: @organization)

    Role.create_standard_roles(@organization)
    member_role =  @organization.roles.find_by(name: 'member')
    member_role.add_user(@user)
    create(:role_permission, role: member_role, name: Permissions::DEVELOPER )
    @credential = create(:developer_api_credential,
      organization: @organization,
      creator: creator
    )
  end

  test ':api get feed for a user' do
    cards = create_list(:card, 2, author: create(:user, organization: @organization))
    UserContentsQueue.expects(:feed_content_for_v2).with(user: @user, limit: 10, offset: 0).returns([{'rationale' => 'rat 1', 'item' => cards.first}, {'rationale' => 'rat 2', 'item' => cards.last}]).once
    set_dev_v5_api_request_header(@user, @credential)
    get :index, limit: 10, offset: 0, format: :json
    resp = get_json_response(response)
    assert_response :ok
    assert_equal cards.map(&:id), resp["items"].map{|r| r['item']['id']}
    assert_equal ['rat 1', 'rat 2'], resp["items"].map{|r| r['rationale']}
  end

  test 'feed api pagination' do
    cards = create_list(:card, 2, author: create(:user, organization: @organization))
    UserContentsQueue.expects(:feed_content_for_v2).with(user: @user, limit: 5, offset: 10).returns([{'rationale' => 'rat 1', 'item' => cards.first}, {'rationale' => 'rat 2', 'item' => cards.last}]).once
    set_dev_v5_api_request_header(@user, @credential)
    get :index, limit: 5, offset: 10, format: :json
    assert_response :ok
  end
end
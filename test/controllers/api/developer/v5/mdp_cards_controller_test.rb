require 'test_helper'
require 'jwt'

class Api::Developer::V5::MdpCardsControllerTest < ActionController::TestCase

  setup do
    @organization = create(:organization, enable_role_based_authorization: true)
    @user = create(:user, organization: @organization)
    creator = create(:user, organization: @organization)

    Role.create_standard_roles(@organization)
    member_role =  @organization.roles.find_by(name: 'member')
    member_role.add_user(@user)
    create(:role_permission, role: member_role, name: Permissions::DEVELOPER )
    @credential = create(:developer_api_credential,
      organization: @organization,
      creator: creator
    )
  end

  test "should get mdp_card details of user for organization" do
    set_dev_v5_api_request_header(@user, @credential)
    author = create(:user, organization: @organization)
    mdp_cards = create_list(:card, 10, organization: @organization, author: author)
    TestAfterCommit.enabled = true
    mdp_cards.each do |card|
      MdpUserCard.create(card: card, user: @user,
        form_details: {
          source_of_mdp: "Discussion with Manager",
          sub_competency_value: "Understand Business Environment",
          additional_comments: "comments for MDP",
          learning_activity: "GVC Learning"
        }
      )
    end

    get :index, format: :json
    assert_response :ok
    resp = get_json_response(response)

    card_keys = ["card_id", "user_email", "status", "form_details"]
    assert_equal 10, resp['response_data']['catalog_data'].length
    assert_equal ["prev_page_offset", "next_page_offset", "catalog_data"], resp['response_data'].keys
    assert_equal card_keys, resp['response_data']['catalog_data'].first.keys
    assert_same_elements mdp_cards.map(&:id), resp['response_data']['catalog_data'].map{|c| c['card_id']}
    assert_same_elements [@user.email], resp['response_data']['catalog_data'].map{|c| c['user_email']}.uniq
  end

  test "should get mdp_card details of user for organization with limit and offset" do
    set_dev_v5_api_request_header(@user, @credential)
    author = create(:user, organization: @organization)
    mdp_cards = create_list(:card, 10, organization: @organization, author: author)
    TestAfterCommit.enabled = true
    mdp_cards.each do |card|
      MdpUserCard.create(card: card, user: @user,
        form_details: {
          source_of_mdp: "Discussion with Manager",
          sub_competency_value: "Understand Business Environment",
          additional_comments: "comments for MDP",
          learning_activity: "GVC Learning"
        }
      )
    end

    get :index, limit: 4, offset: 4, format: :json
    assert_response :ok
    resp = get_json_response(response)

    assert_equal 4, resp['response_data']['catalog_data'].length
    assert_equal 0, resp['response_data']['prev_page_offset']
    assert_equal 8, resp['response_data']['next_page_offset']
  end

  test "should get mdp_card details of user for organization only updated from given date" do
    TestAfterCommit.enabled = true
    set_dev_v5_api_request_header(@user, @credential)
    author = create(:user, organization: @organization)
    old_mdp_card = create(:card, organization: @organization, author: author)
    Timecop.freeze(4.days.ago) do
      MdpUserCard.create(
        card: old_mdp_card,
        user: @user,
        form_details: {
          source_of_mdp: "Manager",
          sub_competency_value: "Environment",
          learning_activity: "GVC Learning"
        }
      )
      UserContentCompletion.create(user: @user, completable: old_mdp_card)
    end

    mdp_cards = create_list(:card, 3, organization: @organization, author: author)
    mdp_cards.each do |card|
      MdpUserCard.create(card: card, user: @user,
        form_details: {
          source_of_mdp: "Discussion with Manager",
          sub_competency_value: "Understand Business Environment",
          additional_comments: "comments for MDP",
          learning_activity: "GVC Learning"
        }
      )
    end

    get :index, limit: 10, offset: 0, last_fetched_at: 2.days.ago.to_date.to_s, format: :json
    assert_response :ok
    resp = get_json_response(response)

    assert_equal 3, resp['response_data']['catalog_data'].length
    assert resp['response_data']['catalog_data'].map{|c| c['card_id']}.exclude?(old_mdp_card.id)
  end
end

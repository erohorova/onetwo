require 'test_helper'
require 'jwt'

class Api::Developer::V5::ExternalAuthenticationControllerTest < ActionController::TestCase
  setup do
    @org = create(:organization, enable_role_based_authorization: true)
    @user = create(:user, organization: @org)
    Role.create_standard_roles(@org)
    member_role =  @org.roles.find_by(name: 'member')
    member_role.add_user(@user)
    create(:role_permission, role: member_role, name: Permissions::DEVELOPER )
    @credential = create(:developer_api_credential,
      organization: @org,
      creator: @user
    )
  end

  test 'should return access_token' do
    payload = { 'email' => @user.email }
    jwt_token = JWT.encode(payload, @credential.shared_secret)
    request.headers['X-API-KEY'] = @credential.api_key
    request.headers['X-AUTH-TOKEN'] = jwt_token
    set_host(@user.organization)

    get :auth, format: :json

    assert_response :ok
    resp = get_json_response response
    refute resp['jwt_token'].nil?
  end

  test 'should return error message if accessing from different host' do
    payload = { 'email' => @user.email }
    jwt_token = JWT.encode(payload, @credential.shared_secret)
    request.headers['X-API-KEY'] = @credential.api_key
    request.headers['X-AUTH-TOKEN'] = jwt_token
    set_host(@user.organization)
    request.host =  'sagar'
    get :auth, format: :json

    assert_response 401
    resp = get_json_response response
    assert_equal 'Could not authenticate with API KEY and AUTH TOKEN', resp['message']
  end
end

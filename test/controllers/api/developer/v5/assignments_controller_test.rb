require 'test_helper'
require 'jwt'

class Api::Developer::V5::AssignmentsControllerTest < ActionController::TestCase
  setup do
    @organization = create(:organization, enable_role_based_authorization: true)
    @user = create(:user, organization: @organization)
    creator = create(:user, organization: @organization)

    Role.create_standard_roles(@organization)
    member_role =  @organization.roles.find_by(name: 'member')
    member_role.add_user(@user)
    create(:role_permission, role: member_role, name: Permissions::DEVELOPER )
    @credential = create(:developer_api_credential,
      organization: @organization,
      creator: creator
    )
  end

  test ':api should return latest completed assignments for user' do
    set_dev_v5_api_request_header(@user, @credential)
    pathway_card = create(:card, organization_id: @user.organization_id,
      author_id: nil, card_type: 'pack', card_subtype: 'simple')
    pathway_assignment = create(:assignment,
      assignable: pathway_card, assignee: @user,
      state: 'completed', completed_at: 2.days.ago
    )
    UserContentCompletion.create(user: @user, completable: pathway_card, state: 'completed')

    get :index, state: 'completed',format: :json
    assert_response :ok

    resp = get_json_response(response)
    item = resp["assignments"][0]

    assert_equal 1, resp["assignments"].count
    assert_equal pathway_assignment.id, item["id"]
    assert_equal pathway_assignment.completed_at.strftime('%FT%T.000Z'), item["completed_at"]
    assert_equal 100, pathway_assignment.assignable.completed_percent(@user)
  end

  test 'should return all active assignments for user' do
    set_dev_v5_api_request_header(@user, @credential)
    pathway_card = create(:card, organization_id: @user.organization_id,
      author_id: nil, card_type: 'pack', card_subtype: 'simple')
    pathway_assignment = create(:assignment, assignable: pathway_card, assignee: @user, state: 'started')

    get :index, state: 'started', format: :json
    assert_response :ok

    resp = get_json_response(response)
    item = resp["assignments"][0]

    assert_equal 1, resp["assignments"].count
    assert_equal pathway_assignment.id, item["id"]
    assert_equal item["state"], "started"
  end

  test 'should return all assigned assignments for user' do
    set_dev_v5_api_request_header(@user, @credential)
    pathway_card = create(:card, organization_id: @user.organization_id,
      author_id: nil, card_type: 'pack', card_subtype: 'simple')
    pathway_assignment = create(:assignment, assignable: pathway_card, assignee: @user, state: 'assigned')

    get :index, state: 'assigned', format: :json
    assert_response :ok

    resp = get_json_response(response)
    item = resp["assignments"][0]

    assert_equal 1, resp["assignments"].count
    assert_equal pathway_assignment.id, item["id"]
    assert_equal item["state"], "assigned"
  end

  test ':api should return error for invalid state' do
    set_dev_v5_api_request_header(@user, @credential)
    pathway_card = create(:card, organization_id: @user.organization_id,
      author_id: nil, card_type: 'pack', card_subtype: 'simple')
    create(:assignment, assignable: pathway_card, assignee: @user, state: 'completed', completed_at: 2.days.ago)

    get :index, state: 'complete',format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal "state is invalid, valid states: started, completed, assigned", resp["error"]
  end
end

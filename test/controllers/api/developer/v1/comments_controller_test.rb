require 'test_helper'

class Api::Developer::V1::CommentsControllerTest < ActionController::TestCase
  setup do
    Comment.any_instance.stubs(:record_comment_event)
    @organization = create(:organization)
    @credential = create(:developer_api_credential, organization: @organization)
    @user = create :user, organization: @organization
    @user1 = create :user, organization: @organization
    set_dev_api_request_header(@user, @credential)
  end

  test 'return comments on card' do
    card = create(:card, organization_id: @organization.id, author: @user)
    comments = []
    (1..3).each do |i|
      comments << create(:comment, commentable: card, user_id: @user1.id, created_at: Time.now.utc - i.hours)
    end
    get :index, card_id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal comments.reverse.map(&:id), resp['comments'].map{|c| c['id']}
  end

  test 'should send upvote data' do
    card = create(:card, organization_id: @organization.id, author: @user)
    comments = []
    (1..2).each do |i|
      comments << create(:comment, commentable: card, user_id: @user1.id, created_at: Time.now.utc - i.hours)
    end

    create(:vote, votable: comments.last, voter: @user)
    get :index, card_id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal comments.reverse.map(&:id), resp['comments'].map{|c| c['id']}
    assert_equal [true, false], resp['comments'].map{|c| c['voted']}
  end

  test 'return comment details' do
    card = create(:card, organization_id: @organization.id, author: @user)
    comment = create(:comment, commentable: card, user_id: @user1.id)
    get :show, card_id: card.id, id: comment.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal comment.id, resp['id']
  end

  test 'create validation errors' do
    card = create(:card, organization_id: @organization.id, author: @user)
    assert_no_difference ->{card.comments.count} do
      post :create, card_id: card.id, comment: {message: nil}, format: :json
      assert_response :unprocessable_entity
      resp = get_json_response(response)
      assert_match /Message can't be blank/, resp["message"]
    end
  end

  test 'only user can delete its own comment' do
    create_default_org
    card = create(:card, organization_id: @organization.id, author: @user)
    comment = create(:comment, commentable: card, user: @user)
    assert_difference ->{card.comments.count}, -1 do
      delete :destroy, card_id: card.id, id: comment.id, format: :json
      assert_response :ok
    end
  end

  test 'card author should be able to delete any comment on card' do
    create_default_org
    card = create(:card, author_id: @user.id)
    comment = create(:comment, commentable: card, user: @user1)

    assert_difference ->{card.comments.count}, -1 do
      delete :destroy, card_id: card.id, id: comment.id, format: :json
      assert_response :ok
    end
  end

  test "should not be able to delete other users' comments" do
    card = create(:card, organization_id: @organization.id, author: @user1)
    another_user = create(:user, organization_id: @organization.id)
    comment = create(:comment, commentable: card, user: another_user)
    assert_no_difference ->{card.comments.count} do
      delete :destroy, card_id: card.id, id: comment.id, format: :json
      assert_response :not_found
    end
  end
end

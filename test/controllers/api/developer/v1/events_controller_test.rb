require 'test_helper'

class Api::Developer::V1::EventsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    @org = create(:organization)
    @credential = create(:developer_api_credential, organization: @org)
    @user = create :user, organization: @org
    @dummy_query = "select * from %{entity}"
    @dummy_params = {entity: "foo"}
    @start_date = (Date.today - 7.days).to_time.to_i
  end

  ##### index ########
  test ':api should return list of events with default params' do
    set_dev_api_request_header(@user, @credential)

    InfluxQuery.expects(:get_query_for_entity)
      .with(entity: 'cards', organization_id: @org.id, filters: {
        'limit' => 200_000,
        'offset' => 0,
        'start_date' => @start_date
      })
      .returns([@dummy_query, @dummy_params])
    InfluxQuery.expects(:fetch).with(@dummy_query, @dummy_params)
    get :index, entity: 'cards', start_date: @start_date, format: :json
    assert_response :ok
  end

  test 'real data test for debugging purposes' do
    # =========================================
    # This is to test that the endpoint works with real data.
    # We don't need to run this usually.
    skip
    # =========================================
    set_dev_api_request_header(@user, @credential)
    WebMock.disable!
    Object.const_set :INFLUXDB_CLIENT, InfluxDB::Client.new('test5',
      host: 'influxdb-qa.edcastcloud.net',
      username: "root",
      password: "root",
      use_ssl: false,
      time_precision: 'ns',
      chunk_size: 10_000
    )
    query, params = InfluxQuery.get_query_for_entity(
      entity:  'cards',
      organization_id: '15',
      start_date: @start_date
    )
    results = InfluxQuery.fetch(query, params)
    assert results[0]["values"].length > 3000
    WebMock.enable!
  end

  test 'should remove null tags and values from the response' do
    set_dev_api_request_header(@user, @credential)
    InfluxQuery.expects(:fetch).returns([{
      "name" => "foo",
      "tags" => { "a" => "b", "c" => nil },
      "values" => [{ "d" => "e", "c" => nil }]
    }])
    get :index, entity: 'cards', start_date: @start_date, format: :json
    assert_response :ok
    expected = [{
      "name" => "foo",
      "tags" => { "a" => "b" },
      "values" => [{ "d" => "e" }]
    }]
    assert_equal expected, JSON.parse(response.body)
  end

  test 'should return list of events with entity #users' do
    set_dev_api_request_header(@user, @credential)

    InfluxQuery.expects(:get_query_for_entity)
      .with(entity: 'users', organization_id: @org.id, filters: {
        'limit' => 200_000,
        'offset' => 0,
        'start_date' => @start_date
      })
      .returns([@dummy_query, @dummy_params])
    InfluxQuery.expects(:fetch).with(@dummy_query, @dummy_params)

    get :index, entity: 'users', start_date: @start_date, format: :json
    assert_response :ok
  end

  test 'should return list of events with group by and daterange' do
    set_dev_api_request_header(@user, @credential)

    start_date = (Time.now - 10.days).beginning_of_day.to_i
    end_date = Time.now.end_of_day.to_i

    InfluxQuery.expects(:get_query_for_entity)
      .with(
        entity: 'cards',
        organization_id: @org.id,
        filters: {
          'start_date' => start_date,
          'end_date' => end_date,
          'group_by' => 'event,user_id',
          'limit' => 200_000,
          'offset' => 0
        }
      ).returns([@dummy_query, @dummy_params])

    InfluxQuery.expects(:fetch).with(@dummy_query, @dummy_params).returns(@dummy_response)

    get :index, {
      entity: 'cards',
      start_date: start_date,
      end_date: end_date,
      group_by: "event,user_id",
      format: :json
    }
    assert_response :ok
  end

  test 'should paginate results using limit and offset' do
    set_dev_api_request_header(@user, @credential)

    start_date = (Time.now - 10.days).beginning_of_day.to_i
    end_date = Time.now.end_of_day.to_i
    limit = 1_000_000
    offset = 2_000_000

    InfluxQuery.expects(:get_query_for_entity)
      .with(
        entity: 'cards',
        organization_id: @org.id,
        filters: {
          'start_date' => start_date,
          'end_date' => end_date,
          'group_by' => 'event,user_id',
          'limit' => limit,
          'offset' => offset
        }
      ).returns([@dummy_query, @dummy_params])

    InfluxQuery.expects(:fetch).with(@dummy_query, @dummy_params)

    get :index, {
      entity: 'cards',
      start_date: start_date,
      end_date: end_date,
      limit: limit,
      offset: offset,
      group_by: "event,user_id",
      format: :json
    }
    assert_response :ok
  end

  test 'REALLY paginates results using limit and offset (integration test)' do
    # =========================================
    # SKIP THIS UNLESS TESTING LOCALLY
    skip
    # =========================================
    WebMock.disable!
    INFLUXDB_CLIENT.unstub(:write_points)
    InfluxdbRecorder.unstub(:record)
    # clear our and bulk record some measurements for tesing.
    # any entity supported by the endpoint could be used instead
    measurement = Analytics::OrgScoreContinuousQueries::MEASUREMENT
    INFLUXDB_CLIENT.query("DROP MEASUREMENT #{measurement}")
    date = Date.today - 100.days
    records = 50.times.map do |i|
      time = (date + i.days).to_time.to_i
      {
        timestamp: time,
        tags: { foo: "bar", org_id: @org.id },
        values: { bar: "foo", idx: i }
      }
    end
    InfluxdbRecorder.bulk_record(measurement, records)
    # Get the response with pagination
    set_dev_api_request_header(@user, @credential)
    response = get :index, {
      entity: measurement,
      start_date: '0',
      limit: 10,
      offset: 15,
      format: :json
    }
    assert_equal "200", response.code
    results = JSON.parse(response.body)[0]["values"]
    assert_equal (15...25).to_a, results.map { |result| result["idx"] },
    WebMock.enable!
  end

  test 'should use default limit and pagination' do
    set_dev_api_request_header(@user, @credential)
    now = Time.now.utc
    Time.stubs(:now).returns now

    start_date = (now - 10.days).beginning_of_day.to_i
    end_date = now.end_of_day.to_i

    InfluxQuery.expects(:get_query_for_entity)
      .with(
        entity: 'cards',
        organization_id: @org.id,
        filters: {
          'start_date' => start_date,
          'end_date' => end_date,
          'group_by' => 'event,user_id',
          'limit' => 200_000,
          'offset' => 0
        }
      ).returns([@dummy_query, @dummy_params])

    InfluxQuery.expects(:fetch).with(@dummy_query, @dummy_params)

    get :index, {
      entity: 'cards',
      start_date: start_date,
      end_date: end_date,
      group_by: "event,user_id",
      format: :json
    }
    assert_response :ok
  end

  test 'should return list of events with matching card id' do
    set_dev_api_request_header(@user, @credential)
    now = Time.now.utc
    Time.stubs(:now).returns now
    card_id = create(:card, author: @user).id
    InfluxQuery.expects(:get_query_for_entity)
      .with(
        entity: 'cards',
        organization_id: @org.id,
        filters: {
          'card_id' => card_id,
          'limit' => 200_000,
          'offset' => 0,
          'start_date' => @start_date
        }
      ).returns([@dummy_query, @dummy_params])

    InfluxQuery.expects(:fetch).with(@dummy_query, @dummy_params)

    get :index, {
      entity: 'cards',
      card_id: card_id,
      start_date: @start_date,
      format: :json
    }
    assert_response :ok
  end

  test 'should return list of events with matching user id' do
    set_dev_api_request_header(@user, @credential)
    user_id = User.first!.id
    now = Time.now.utc
    Time.stubs(:now).returns now

    InfluxQuery.expects(:get_query_for_entity)
      .with(
        entity: 'cards',
        organization_id: @org.id,
        filters: {
          'user_id' => user_id,
          'limit' => 200_000,
          'offset' => 0,
          'start_date' => @start_date
        }
      ).returns([@dummy_query, @dummy_params])

    InfluxQuery.expects(:fetch).with(@dummy_query, @dummy_params)

    get :index, {
      entity: 'cards',
      user_id: user_id,
      start_date: @start_date,
      format: :json
    }
    assert_response :ok
  end

  test 'should return list of events with matching group id' do
    set_dev_api_request_header(@user, @credential)
    group_id = create(:team, organization: @org).id
    now = Time.now.utc
    Time.stubs(:now).returns now

    InfluxQuery.expects(:get_query_for_entity)
      .with(
        entity: 'cards',
        organization_id: @org.id,
        filters: {
          'group_id' => group_id,
          'limit' => 200_000,
          'offset' => 0,
          'start_date' => @start_date
        }
      ).returns([@dummy_query, @dummy_params])

    InfluxQuery.expects(:fetch).with(@dummy_query, @dummy_params)

    get :index, {
      entity: 'cards',
      group_id: group_id,
      'start_date' => @start_date,
      format: :json
    }
    assert_response :ok
  end

  test 'should return list of events with matching channel id' do
    Organization.any_instance.unstub :create_general_channel
    @org.send :create_general_channel

    set_dev_api_request_header(@user, @credential)
    channel_id = Channel.first!.id
    now = Time.now.utc
    Time.stubs(:now).returns now

    InfluxQuery.expects(:get_query_for_entity)
      .with(
        entity: 'cards',
        organization_id: @org.id,
        filters: {
          'channel_id' => channel_id,
          'limit' => 200_000,
          'offset' => 0,
          'start_date' => @start_date
        }
      ).returns([@dummy_query, @dummy_params])

    InfluxQuery.expects(:fetch).with(@dummy_query, @dummy_params)

    get :index, {
      entity: 'cards',
      channel_id: channel_id,
      start_date: @start_date,
      format: :json
    }
    assert_response :ok
  end

  test 'should return list of events with matching event' do
    event = 'card_viewed'
    set_dev_api_request_header(@user, @credential)

    now = Time.now.utc
    Time.stubs(:now).returns now

    InfluxQuery.expects(:get_query_for_entity)
      .with(
        entity: 'cards',
        organization_id: @org.id,
        filters: {
          'event' => event,
          'limit' => 200_000,
          'offset' => 0,
          'start_date' => @start_date
        }
      ).returns([@dummy_query, @dummy_params])

    InfluxQuery.expects(:fetch).with(@dummy_query, @dummy_params)

    get :index, {
      entity: 'cards',
      event_name: event,
      start_date: @start_date,
      format: :json
    }
    assert_response :ok
  end

  test "should sanitize start_date and end_date by converting to ints" do
    set_dev_api_request_header(@user, @credential)
    user_id = User.first!.id
    InfluxQuery.expects(:get_query_for_entity).with(
      entity: 'cards',
      organization_id: @org.id,
      filters: {
        'start_date' => 0,
        'end_date' => 0,
        'user_id' => user_id,
        'limit' => 200_000,
        'offset' => 0
      }
    ).returns([@dummy_query, @dummy_params])
    InfluxQuery.expects(:fetch).with(@dummy_query, @dummy_params)
    get :index,
        entity: 'cards',
        user_id: user_id,
        format: :json,
        start_date: "foo",
        end_date: "bar"
    assert_response :ok
  end

  test 'should return filter by event with entity #users' do
    set_dev_api_request_header(@user, @credential)

    InfluxQuery.expects(:get_query_for_entity)
      .with(
        entity: 'users',
        organization_id: @org.id,
        filters: {
          'limit' => 200_000,
          'offset' => 0,
          'event' => 'user_created',
          'start_date' => @start_date
        }
      )
      .returns([@dummy_query, @dummy_params])
    InfluxQuery.expects(:fetch).with(@dummy_query, @dummy_params)

    get :index, {
      entity: 'users',
      event_name: 'user_created',
      start_date: @start_date,
      format: :json
    }
    assert_response :ok
  end

  test 'returns 422 if missing start_date' do
    set_dev_api_request_header(@user, @credential)
    get :index, entity: 'users', format: :json
    assert_equal '422', response.code
    assert_equal(
      {'message' => "missing start date"},
      JSON.parse(response.body)
    )
  end

  test 'returns 422 for invalid event_name' do
    set_dev_api_request_header(@user, @credential)
    get :index, entity: 'users', event_name: "asdads", format: :json
    assert_equal '422', response.code
    assert_equal(
      {'message' => "given event_name doesn't exist"},
      JSON.parse(response.body)
    )
  end

  test 'returns 422 for invalid or missing entity' do
    [
      {},
      {entity: 'foo'}
    ].each do |params_set|
      set_dev_api_request_header(@user, @credential)
      get :index, { format: :json, start_date: Time.now.to_i }.merge(params_set)
      assert_equal '422', response.code
      assert_equal(
        {'message' => "unknown entity"},
        JSON.parse(response.body)
      )
    end
  end

end

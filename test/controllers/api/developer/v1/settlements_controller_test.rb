require 'test_helper'

class Api::Developer::V1::SettlementsControllerTest < ActionController::TestCase
  setup do 
    @org = create(:organization)
    @user = create(:user, organization: @org)
  end

  test 'should return transactions' do
    card1 = create(:card, organization: @org, author: @user)
    price = create(:price, card: card1)
    card2 = create(:card, organization: @org, author: @user)
    price = create(:price, card: card2)

    order1 = create(:order, orderable: card1, state: 2, organization_id: @org.id, user: @user)
    transaction1 = create(:transaction, order: order1, payment_state: 1, order_type: "Card")

    order2 = create(:order, orderable: card2, state: 2, organization_id: @org.id, user: @user)
    transaction2 = create(:transaction, order: order2, payment_state: 1, order_type: "Card", updated_at: Time.now + 1.minute)

    token = Settings.settlements['shared_secret']
    payload = {"url" => "https://integrations.edcast.com", "date" => Date.today.to_s}
    request.headers['AUTH-TOKEN'] = JWT.encode(payload, token, "HS256")

    get :transactions, limit: 2, offset: 0, format: :json
    resp = get_json_response(response)

    assert_equal [transaction2.id, transaction1.id], resp['transactions'].map{|c| c['id']}
  end

  test 'should return source name in the api response' do
    source_name = Faker::Name.name
    card = create(:card, organization: @org, author: @user)
    order = create(:order, orderable: card, state: 2, organization_id: @org.id, user: @user)
    transaction = create(:transaction, order: order, payment_state: 1, order_type: "Card", source_name: source_name)

    token = Settings.settlements['shared_secret']
    payload = {"url" => "https://integrations.edcast.com", "date" => Date.today.to_s}
    request.headers['AUTH-TOKEN'] = JWT.encode(payload, token, "HS256")

    get :transactions, format: :json
    resp = get_json_response(response)

    assert_equal transaction.source_name, resp['transactions'].first['source_name']
  end
end

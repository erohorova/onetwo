require 'test_helper'
require 'jwt'

class Api::Developer::V1::ChannelsControllerTest < ActionController::TestCase

  include Devise::Test::ControllerHelpers
  # self.use_transactional_fixtures = false

  setup do
    # WebMock.disable!
    @organization = create(:organization)
    @credential = create(:developer_api_credential, organization: @organization)
    @user = create :user, organization: @organization
    @channels = create_list(:channel, 5, organization: @organization)
  end

  # teardown do
  #   WebMock.enable!
  # end

  ##### index ########
  test 'should return list of channels with valid jwt_token' do
    Organization.any_instance.unstub :create_general_channel
    @organization.send :create_general_channel
    set_dev_api_request_header(@user, @credential)

    get :index, format: :json
    resp = get_json_response response

    assert_response :ok
    assert resp["channels"]
    assert_not_nil resp["channels"]

    #it includes general channels too
    assert_equal resp["channels"].length, 6
  end

  test 'should return unauthorized for request from other org' do
    set_dev_api_request_header(@user, @credential)
    org = create(:organization)
    set_host(org)
    get :index, format: :json
    assert_response :unauthorized
  end

  test "should return a channel with search keyword" do
    set_dev_api_request_header(@user, @credential)

    channel = create(:channel, label: 'Robotics', organization: @organization)

    get :index, q: "Robotics", format: :json
    resp = get_json_response response

    assert resp["channels"]
    assert_equal resp["channels"][0]["label"], channel.label
  end

  test 'should not return with invalid jwt_token' do
    token = "invalid_token"
    request.headers['X-DEVELOPER-API-KEY'] = @credential.api_key
    request.headers['X-JWT-TOKEN'] = token

    get :index, format: :json

    assert_response 400
  end

  test 'should return proper message of expired  jwt token' do
    payload = {
      host_name: @user.organization.client.try(:name),
      user_id: @user.id,
      email: @user.email,
      timestamp: Time.now,
      exp: Time.now.to_i
    }
    jwt_token = JWT.encode(payload, @credential.shared_secret, 'HS256')

    request.headers['X-DEVELOPER-API-KEY'] = @credential.api_key
    request.headers['X-JWT-TOKEN'] = jwt_token

    get :index, format: :json

    resp = get_json_response response
    assert_equal resp["message"], "JWT token has expired"
  end

  ##### show ######
  test 'should return channel information' do
    set_dev_api_request_header(@user, @credential)

    channel= create(:channel, organization: @user.organization)
    channel.add_followers [@user]
    channel.reload

    get :show, id: channel.id, format: :json
    resp = get_json_response(response)
    assert_equal channel.id, resp["id"]
    assert_equal channel.label, resp["label"]
  end
end

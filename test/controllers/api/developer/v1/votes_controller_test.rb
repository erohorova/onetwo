require 'test_helper'

class Api::Developer::V1::VotesControllerTest < ActionController::TestCase

  setup do
    @organization = create(:organization)
    @credential = create(:developer_api_credential, organization: @organization)
    @user, @user1 = create_list(:user, 2, organization: @organization)
    set_dev_api_request_header(@user, @credential)
  end

  test ':api create video stream card upvote' do
    video_stream = create(:iris_video_stream, organization: @organization, creator: @user1)
    assert_difference -> { Vote.count } do
      post :create, vote: {content_id: video_stream.card.id, content_type: 'Card' }
      assert_response :ok
    end
    vote = Vote.last
    assert_equal @user.id, vote.voter.id
    assert_equal video_stream.card, vote.votable
    assert video_stream.voted_by?(@user)
  end

  test ':api create card upvote' do
    card = create(:card, organization: @organization, author: @user1)
    assert_difference -> { Vote.count } do
      post :create, vote: {content_id: card.id, content_type: 'Card' }
      assert_response :ok
    end

    vote = Vote.last
    assert_equal @user.id, vote.voter.id
    assert_equal card, vote.votable
    assert_equal 1, card.reload.votes_count
    assert card.voted_by?(@user)
  end

  test 'create validation error' do
    card = create(:card, organization: @organization, author: @user1)
    vote = create(:vote, votable: card, voter: @user)
    assert_no_difference -> { Vote.count } do
      post :create, vote: {content_id: card.id, content_type: 'Card' }
      assert_response :unprocessable_entity
      resp = get_json_response(response)
      assert_match /User has already upvoted this item/, resp["message"]
    end
  end

  test 'create comment upvote' do
    card = create(:card, organization: @organization, author: @user1)
    comment = create(:comment, commentable: card, user: @user1)
    assert_difference -> { Vote.count } do
      post :create, vote: {content_id: comment.id, content_type: 'Comment' }
      assert_response :ok
    end

    vote = Vote.last
    assert_equal @user.id, vote.voter.id
    assert_equal comment, vote.votable
  end

  test 'destroy' do
    video_stream = create(:iris_video_stream, organization: @organization, creator: @user1)
    vote = create(:vote, voter: @user, votable: video_stream)
    assert_difference -> { Vote.count }, -1 do
      delete :destroy, vote: {content_id: video_stream.id, content_type: "VideoStream"}, format: :json
      assert_response :ok
    end

    refute Vote.where(votable: @video_stream, voter: @user).present?
    refute video_stream.voted_by?(@user)
  end

end

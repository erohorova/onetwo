require 'test_helper'

class Api::Developer::V1::OrganizationsControllerTest < ActionController::TestCase
  setup do
    @org = create(:organization)
    @credential = create(:developer_api_credential, organization: @org)
    @xapi_credential = create(:xapi_credential, organization: @org )
  end

  test ':api should get LRS credentials of current organization only for admin user' do
    @admin_user = create(:user, organization_role: 'admin', organization: @org)
    set_dev_api_request_header(@admin_user, @credential)
    set_host(@org)
    get :lrs_credentials,format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal @xapi_credential.id, resp['id']
  end

  test 'should not get LRS credentials of current organization for non-admin user' do
    @user = create :user, organization: @org
    set_dev_api_request_header(@user, @credential)
    set_host(@org)
    get :lrs_credentials,format: :json
    assert_response :unauthorized
  end

  test 'should not get LRS credentials of current organization for non-admin user and non developer api' do
    @user = create :user, organization: @org
    api_v2_sign_in(@user)
    get :lrs_credentials,format: :json
    assert_response :unauthorized
  end
end

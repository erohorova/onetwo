require 'test_helper'

class Api::Developer::V1::SearchControllerTest < ActionController::TestCase

  setup do
    Organization.any_instance.unstub(:create_default_roles)

    @organization = create(:organization)
    @credential = create(:developer_api_credential, organization: @organization)
    @user = create :user, organization: @organization
  end

  test 'search api failure cases' do
    set_dev_api_request_header(@user, @credential)

    # No query parameter
    get :index, format: :json
    assert_response :unprocessable_entity
  end

  test ':api simple search' do
    Card.any_instance.stubs(:reindex).returns true

    # media cards
    media_link_card = create(:card, card_type: 'media', card_subtype: 'link', resource: create(:resource))
    media_video_card = create(:card, card_type: 'media', card_subtype: 'video', resource: create(:resource))
    media_insight_card = create(:card, message: 'some message', card_type: 'media', card_subtype: 'text')
    media_image_card = create(:card, card_type: 'media', card_subtype: 'image')
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)
    media_image_card.file_resources << fr1

    # poll
    poll_card = create(:card, card_type: "poll", card_subtype: 'text')
    poll_card.quiz_question_options << create(:quiz_question_option, quiz_question: poll_card)

    # pathway
    pathway_card = create(:card, card_type: 'pack', card_subtype: 'simple')
    hidden_cards = create_list(:card, 2, hidden: true)

    hidden_cards.each do |card|
      CardPackRelation.add(cover_id: pathway_card.id, add_id: card.id, add_type: card.class.name)
    end

    # video stream card
    video_stream_card = create(:card, card_type: 'video_stream', card_subtype: 'simple')
    vs = create(:iris_video_stream, card_id: video_stream_card.id, status: 'past')
    vs.recordings << create(:recording, transcoding_status: 'available', mp4_location: 'http://something.mp4', hls_location: 'http://something.m3u8')

    cards = [media_link_card, media_video_card, media_insight_card, media_image_card, poll_card, pathway_card, video_stream_card]

    query = "query"
    limit = 10
    offset = 0

    ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items"]
    ecl_search = mock()
    EclApi::EclSearch.expects(:new).with(@organization.id, @user.id).returns(ecl_search).once
    ecl_search.expects(:search).with(has_entries(query: query, limit: limit, load_tags: true, offset: offset, :query_type => 'and', filter_params: has_entries({content_type: nil, source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, rank_params: {user_ids: []}}))).returns(OpenStruct.new(results: cards, aggregations: ecl_response['aggregations'])).once

    set_dev_api_request_header(@user, @credential)
    get :index, q: "query", limit: limit, offset: offset, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal cards.map(&:id), resp['cards'].map{|r| r['id'].to_i}

    aggregations = ["Articles", "Document", "Books", "Lynda", "Coursera", "Gemba", "English", "Business Management"]
    assert_equal aggregations, resp["aggregations"].map{|a| a["display_name"]}
  end

  test 'content type filter for search' do
    query = "query"
    limit = 10
    offset = 0

    ecl_search = mock()
    EclApi::EclSearch.expects(:new).with(@organization.id, @user.id).returns(ecl_search).once
    ecl_search.expects(:search).with(has_entries(query: query, load_tags: true, limit: limit, offset: offset, :query_type => 'and', filter_params: has_entries({content_type: ['Books'], source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, rank_params: {user_ids: []}}))).returns(OpenStruct.new(results: [], aggregations: [])).once

    set_dev_api_request_header(@user, @credential)
    get :index, q: "query", content_type: ["Books"], format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal [], resp['cards']
  end

  test 'query_type or filter for search' do
    limit = 10
    offset = 0

    ecl_search = mock()
    EclApi::EclSearch.expects(:new).with(@organization.id, @user.id).returns(ecl_search).once

    set_dev_api_request_header(@user, @credential)
    ecl_search.expects(:search).with(has_entries(query: "query test", limit: limit, load_tags: true, offset: offset, :query_type => 'or', filter_params: has_entries({content_type: nil, source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, rank_params: {user_ids: []}}))).returns(OpenStruct.new(results: [], aggregations: [])).once
    get :index, q: "query test", query_type: "or", format: :json
  end
end

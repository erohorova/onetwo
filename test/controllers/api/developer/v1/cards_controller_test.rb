require 'test_helper'

class Api::Developer::V1::CardsControllerTest < ActionController::TestCase

  setup do
    @organization = create(:organization)
    @credential = create(:developer_api_credential, organization: @organization)
    @user = create :user, organization: @organization
    @author = create(:user, organization: @organization)
    set_dev_api_request_header(@user, @credential)
    set_host(@organization)
    @url = 'http://url.com'
    stub_request(:get, @url).
          to_return(:status => 200, :body => '<html><head></head><body></body></html>', headers: {})

    WebMock.allow_net_connect!
  end

  teardown do
    WebMock.disable_net_connect!
  end

  ######## Index ########
  test ':api should get cards in an org' do
    cards = create_list(:card, 5, author: @author , card_type: 'media')

    get :index, format: :json

    assert_response :success
    assert assigns(:cards)
    assert_same_elements cards.map(&:id), assigns(:cards).map{|c| c['id']}
  end

  test 'should send poll cards and topics if API request' do

    cards = create_list(:card, 2, card_type: 'poll', author: @author)
    option = create(:quiz_question_option, label: 'a', quiz_question: cards.first)
    attempt = create(:quiz_question_attempt, user: @user, quiz_question: cards.first, selections: [option.id])
    stats = create(:quiz_question_stats, quiz_question: cards.first, attempt_count: 1, options_stats: {option.id.to_s => 1})

    get :index, load_topics: true, format: :json

    assert_response :success
    resp = get_json_response(response)
    assert_same_elements cards.map(&:id), resp['cards'].map{|c| c['id'].to_i}

    first_card_response_object = resp['cards'].select{|c| c['id'] = cards.first.id.to_s}.first
    # Test attempt count
    assert_equal 1, first_card_response_object['attempt_count']

    # Test has attempted
    assert first_card_response_object['has_attempted']

    # Test individual option stats
    assert_equal 1, first_card_response_object['quiz_question_options'].select{|opt| opt['id'] == option.id}.first['count']
    assert_includes resp['cards'].first.keys, "topics"
  end

  test 'should return org pathways when term is empty' do
    org_cards = create_list(:card, 2, card_type: 'pack', author_id: @user.id, message: 'Pathway card', organization: @user.organization)
    org_cards.each do |cover|
      CardPackRelation.add(cover_id: cover.id, add_id: create(:card, author: @user).id)
      cover.publish!
    end

    get :index, card_type: ['pack'], sort: :promoted_first, format: :json
    assert_response :ok
    resp = get_json_response response
    assert_equal 2, resp['cards'].count
    assert_same_elements org_cards.map(&:id), resp['cards'].map{|c| c['id'].to_i}
  end

  test 'should return video_stream cards' do
    vcard = create(:card, author_id: @user.id, card_type: 'video_stream')
    vs = create(:wowza_video_stream, card: vcard, status: 'upcoming', creator: @user, organization: @user.organization)

    get :index, is_official: 'true', card_type: ['video_stream'], format: :json
    assert_response :success
    assert assigns(:cards)
    resp = get_json_response response
    response_card = resp["cards"][0]
    assert_equal response_card["id"], vcard.id.to_s
    assert_equal response_card["card_type"], "video_stream"
  end

  test 'channel id filter for cards index action' do
    uncurated_cards = create_list(:card, 4, author: @author)

    channel = create(:channel, organization: @organization , curate_only: false)
    uncurated_cards.each { |card| card.channels << channel}

    in_curation_cards = create_list(:card, 4, organization_id: @organization.id,author: nil)
    curated_channel = create(:channel, organization: @organization , curate_only: true)
    in_curation_cards.each {|card| card.channels << curated_channel}
    to_be_curated_cards = in_curation_cards.first(2)
    #curate the first two cards
    curated_channel.channels_cards.where(card_id:to_be_curated_cards.collect(&:id)).each do |channels_card|
      channels_card.curate
      channels_card.save
    end

    get :index, channel_id: [ channel.id, curated_channel.id ], format: :json
    assert_response :success

    expected_ids = uncurated_cards.collect(&:id) + to_be_curated_cards.collect(&:id)
    resp = get_json_response response

    assert_equal 6, resp['cards'].length
    assert_same_elements expected_ids, resp['cards'].map { |c| c['id'].to_i }
  end

  ###### Create ######
  test 'should not create card for invalid params' do
    assert_no_difference -> {Card.count} do
      post :create, {message: 'This is first card in v2.0'}
    end
    assert_response :bad_request
  end

  test 'should return 422 for empty message' do
    assert_no_difference -> {Card.count} do
      post :create, card: {message: nil}, format: :json
    end
    assert_response :unprocessable_entity
  end

  test ':api should create card with message and topics' do
    message = 'This is first card in v2.0'
    topics = [ "Web", "Ruby"]
    assert_difference -> {Card.count}, 1 do
      post :create, card: {message: message, topics: topics}, format: :json
    end
    assert_response :ok
    card = Card.last
    assert_equal 'media', card.card_type
    assert_equal @user.id, card.author_id
    assert_equal message, card.message
    assert_equal 0, card.mentioned_users.count
    assert_not_nil card.published_at
    assert_same_elements topics, card.tags.collect(&:name)
  end

  test "create resource from resource_url and create card" do
    res = create(:resource)
    Resource.stubs(:extract_resource).returns(res)
    message = 'message test'
    assert_difference -> {Card.count}, 1 do
      post :create, card: {message: message, resource_url: "www.google.com"}, format: :json
    end
    card = Card.last
    assert_equal res.url, card.resource.url
  end


  test 'should create card with channel_ids' do
    assert_difference -> {Card.count} do
      channel_ids = create_list(:channel, 2).map(&:id)
      post :create, card: {message: "test", channel_ids: channel_ids}, format: :json
    end
    assert_response :ok
  end

  ###### Update ######
  test 'only creator should be able to update card' do
    create_default_org
    user = create(:user)
    card = create(:card, author_id: user.id, card_type: 'media')

    put :update, id: card.id, card: {message: 'message'}, format: :json
    assert_response :not_found
  end

  test 'should be able to update message, channels and topics' do
    card = create(:card, author_id: @user.id, card_type: 'media')
    ch1 = create(:channel, :robotics, organization: @user.organization)
    ch2 = create(:channel, :architecture, organization: @user.organization)
    topics = [ "Web", "Ruby"]
    ch1.authors << @user
    ch2.authors << @user
    edited_message = 'message edited'

    put :update, id: card.id, card: {channel_ids: [ch1.id, ch2.id], message: edited_message, topics: topics}, format: :json
    card = card.reload
    assert_equal [ch1.id, ch2.id], card.channel_ids
    assert_equal edited_message, card.message
    assert_same_elements topics, card.tags.collect(&:name)
  end

  test 'should not update card with invalid details' do
    create_default_org
    card = create(:card, author_id: @user.id, card_type: 'media')

    put :update, id: card.id, card:{message: nil}, format: :json
    assert_response :unprocessable_entity
  end

  ###### Delete ######
  test 'only creator should be able delete cards' do
    create_default_org
    user = create(:user)
    card = create(:card, author_id: user.id, card_type: 'media')
    card1 = create(:card, author_id: @user.id, card_type: 'media')

    assert_no_difference ->{Card.count} do
      delete :destroy, id: card.id
      assert_response :not_found
    end

    assert_difference ->{Card.count}, -1 do
      delete :destroy, id: card1.id
      assert_response :ok
    end
  end


  #### Show ####
  test 'should render card details' do
    ch1 = create(:channel, :robotics, organization: @user.organization)
    ch1.authors << @user
    message = "http://goodreads.com"
    resource = Resource.extract_resource(message)

    card = create(:card, message: message, resource_id: resource.id, card_type: 'media', author_id: @user.id, channel_ids: [ch1.id])

    get :show, id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal resource.id, resp["resource"]["id"]
    assert_equal resource.image_url, resp["resource"]["image_url"]
  end

  test 'should retur 404 for non org cards' do
    org = create(:organization)
    user = create(:user, organization: org)
    card = create(:card, organization: org, author: user)

    get :show, id: card.id, format: :json
    assert_response :not_found
  end

end

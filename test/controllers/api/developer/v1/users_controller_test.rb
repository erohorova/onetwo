require 'test_helper'
require 'jwt'

class Api::Developer::V1::UsersControllerTest < ActionController::TestCase

  include Devise::Test::ControllerHelpers
  self.use_transactional_fixtures = false

  setup do
    @organization = create(:organization)
    @credential = create(:developer_api_credential, organization: @organization)
    @user = create :user, organization: @organization
    @user_profile = create(:user_profile, user: @user, learning_topics: Taxonomies.domain_topics)
    set_host(@organization)
  end

  def generate_token(payload, shared_secret)
    JWT.encode(payload, shared_secret, "HS256")
  end

 def set_header(auth_hash)
    payload = {auth_hash: auth_hash}
    auth_token = JWT.encode(payload, @credential.shared_secret, "HS256")

    request.headers['X-DEVELOPER-API-KEY'] = @credential.api_key
    request.headers['X-DEVELOPER-AUTH-TOKEN'] = auth_token
  end

  test 'should authenticate developer api request and return jwt_token' do
    create_default_ssos
    Organization.any_instance.unstub :default_sso
    @user.organization.send :default_sso

    #existing user
    auth_hash = {provider: "email", email: @user.email}

    set_header(auth_hash)
    assert_no_difference -> {User.count} do
      get :auth, format: :json
      assert_response :ok
    end
    resp = get_json_response response
    assert_not_nil resp["user"]["jwt_token"]
    assert_not_nil Taxonomies.domain_topics, resp["user"]["learning_topics"]

    assert_equal @user.id, resp["user"]["id"]

    #new user
    auth_hash = {provider: "email", email: 'random@test.com'}
    set_header(auth_hash)

    assert_difference -> {User.count} do
      get :auth, format: :json
      assert_response :ok
    end
    resp = get_json_response response
    assert_not_nil resp["user"]["jwt_token"]
  end

  test "new payload with email should authenticate" do
    create_default_ssos
    Organization.any_instance.unstub :default_sso
    @user.organization.send :default_sso

    payload = {email: @user.email}

    auth_token = JWT.encode(payload, @credential.shared_secret, "HS256")

    request.headers['X-DEVELOPER-API-KEY'] = @credential.api_key
    request.headers['X-DEVELOPER-AUTH-TOKEN'] = auth_token

    get :auth, format: :json
    assert_response :ok
    resp = get_json_response response
    assert_not_nil resp["user"]["jwt_token"]

  end

  test 'should return 401 with invalid auth_token' do
    auth_token = "invalid_token"
    request.headers['X-DEVELOPER-API-KEY'] = @credential.api_key
    request.headers['X-DEVELOPER-AUTH-TOKEN'] = auth_token

    get :auth, format: :json

    assert_response 400
    resp = get_json_response response
    assert_equal "Token is invalid", resp["message"]
  end

  test 'should create identity provider for SSO details' do
    create_default_ssos
    Organization.any_instance.unstub :default_sso
    @user.organization.send :default_sso

    auth_hash = {provider: "facebook", email: @user.email, uid: "222", token: "abcd"}
    set_header(auth_hash)

    assert_difference -> {IdentityProvider.count} do
      get :auth, format: :json
    end

    assert_response :ok
    resp = get_json_response response
    assert_not_nil resp["user"]["jwt_token"]
    assert_equal  @user.id, resp["user"]["id"]

    #should not create IP again
    assert_no_difference -> {IdentityProvider.count} do
      get :auth, format: :json
    end
    resp = get_json_response response
    assert_not_nil resp["user"]["jwt_token"]
  end

  test 'return unauthorized request from other org' do
    auth_hash = {provider: "email", email: @user.email}

    set_header(auth_hash)
    org = create(:organization)
    set_host(org)
    assert_no_difference -> {User.count} do
      get :auth, format: :json
      assert_response :unauthorized
    end
  end

  test "should respond as unauthorized" do
    post :create, { }, format: :json
    assert_response :unauthorized
  end

  class UserOperationTest < ActionController::TestCase

    setup do
      @organization = create(:organization)
      @credential = create(:developer_api_credential, organization: @organization)
      @user = create :user, organization: @organization, email: "admin@edcast.com"
      set_dev_api_request_header(@user, @credential)
      set_host(@organization)
      @url = 'http://url.com'
      stub_request(:get, @url).
            to_return(:status => 200, :body => '<html><head></head><body></body></html>', headers: {})

      WebMock.allow_net_connect!
    end

    teardown do
      WebMock.disable_net_connect!
    end

    test "should respond as invalid user data" do
      params = { email: "admin@edcast.com", "payload": { "users": [] }}
      post :create, params
      json_response = get_json_response(response)
      assert_response 422
    end

    test "should create an invitation request" do
      payload = { "users": [{"first_name": "test",
                              "last_name": "test_last",
                              "email": "test@edcast.com",
                              "groups": "java",
                              "picture_url": "https://www.google.com/image",
                              "password": "password"
                            }]
                }
      params = { "payload": payload }
      post :create, params
      json_response = get_json_response(response)
      assert_equal 1, InvitationFile.count
    end

    test "should suspend an user" do
      suspending_user = create :user, organization: @organization, email: "test_suspend@edcast.com"
      payload = {
                  "users": [{ "email": "test_suspend@edcast.com" }]
                }
      params = { "payload": payload }
      post :suspend, params
      json_response = get_json_response(response)
      assert_equal "suspended", suspending_user.reload.status
    end

    test "should return invalid data" do
      suspending_user = create :user, organization: @organization, email: "test_suspend@edcast.com"
      payload = { }
      params = { "payload": payload }
      post :suspend, params
      json_response = get_json_response(response)
      assert_response 422
    end
  end
end

require 'test_helper'

class Api::V2::PartnerCentersControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    @organization = create :organization
    @admin        = create :user, organization: @organization
    @embed_link   = 'https://dev-579575.oktapreview.com/app/edcastdev579575_savannahsaml_1/exkd0ql7q4VnKqkiW0h7/sso/saml'

    api_v2_sign_in(@admin)
  end

  test ':api #create creates partner center accordingly' do
    post :create, partner_center: { description: 'Test description', embed_link: @embed_link, integration: 'edcast_cloud' }, format: :json

    result = get_json_response(response)['partner_center']
    assert assigns(:partner_center).persisted?
    assert_equal 'Test description', result['description']
    assert_equal 'https://dev-579575.oktapreview.com/app/edcastdev579575_savannahsaml_1/exkd0ql7q4VnKqkiW0h7/sso/saml', result['embed_link']
    assert_equal 'edcast_cloud', result['integration']
  end

  test ':api #index lists all available custom fields added to an organization' do
    pc1 = create :partner_center, organization: @organization, user: @admin
    pc2 = create :partner_center, organization: @organization, user: @admin
    pc3 = create :partner_center, organization: @organization, user: @admin

    get :index, format: :json

    result = get_json_response(response)['partner_centers']

    assert_equal result.collect { |r| r['id'] }.to_set, [pc1.id, pc2.id, pc3.id].to_set
  end

  test ':api #edit returns details of a partner_center' do
    partner_center = create :partner_center, organization: @organization, user: @admin

    get :edit, id: partner_center.id, format: :json

    result = get_json_response(response)['partner_center']

    assert_equal partner_center.as_json.slice(*['id', 'description', 'embed_link', 'integration', 'enabled']), result.slice(*['id', 'description', 'embed_link', 'integration', 'enabled'])
  end

  test ':api #destroy deletes a partner_center' do
    partner_center = create :partner_center, organization: @organization, user: @admin

    delete :destroy, id: partner_center.id, format: :json

    assert_response :ok
  end

  test ':api #update updates details of a partner_center' do
    partner_center = create :partner_center, organization: @organization, user: @admin

    patch :update, id: partner_center.id, partner_center: { description: 'Test description updated' }, format: :json

    partner_center.reload
    assert_equal 'Test description updated', partner_center.description
  end
end

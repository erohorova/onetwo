require 'test_helper'
class Api::V2::WidgetsControllerTest < ActionController::TestCase
  class WidgetsControllerIndexTest < ActionController::TestCase
    setup do
      @organization = create(:organization)
      @user = create(:user, organization: @organization, organization_role: 'admin')
      @channel = create(:channel, organization: @organization)
      api_v2_sign_in(@user)

      @widget_1 = create(:widget, organization: @organization, creator: @user,
        parent: @channel, context: 'channel', code: 'Widget 1')
      @widget_2 = create(:widget, organization: @organization, creator: @user,
        parent: @channel, context: 'channel', code: 'Widget 2', enabled: true)
    end

    test ':api should return widgets of an organization' do
      get :index, widget: { parent_id: @channel.id, parent_type: 'Channel' }, format: :json
      json_response = get_json_response(response)
      assert_response :ok
      assert_equal 2, json_response['widgets'].size
      assert_equal ['id', 'code', 'context', 'enabled', 'parent_id', 'parent_type', 'creator'].sort, json_response['widgets'].first.keys.sort
    end

    test 'should return enabled widgets if passed as param' do
      get :index, widget: { parent_id: @channel.id, parent_type: 'Channel', enabled: 'true' }, format: :json
      json_response = get_json_response(response)
      assert_response :ok
      assert_equal 1, json_response['widgets'].size
      assert_equal [@widget_2.id], json_response['widgets'].map { |widget| widget['id'] }
    end

    test 'should return unprocessable entity if parent is not passed' do
      get :index, widget: { parent_id: '', parent_type: ''}, format: :json
      json_response = get_json_response(response)
      assert_response :unprocessable_entity
      assert_equal 'Blank parent_type or parent_id', json_response['message']
    end

    test 'should return unprocessable entity if parent is not valid' do
      another_org = create(:organization)
      another_channel = create(:channel, organization: another_org)
      get :index, widget: { parent_id: another_channel.id, parent_type: 'Channel'}, format: :json
      json_response = get_json_response(response)
      assert_response :unprocessable_entity
      assert_equal 'Invalid parent', json_response['message']
    end
  end

  class WidgetsControllerCreateTest < ActionController::TestCase
    setup do
      @organization = create(:organization)
      @user = create(:user, organization: @organization, organization_role: 'admin')
      @channel = create(:channel, organization: @organization)
      api_v2_sign_in(@user)
    end

    test ':api should create a widget with valid params' do
      post :create, widget: { parent_id: @channel.id, parent_type: 'Channel', context: :channel, code: 'HTML Widget'}, format: :json
      json_response = get_json_response(response)
      assert_response :ok
      assert_equal ['id', 'code', 'context', 'enabled', 'parent_id', 'parent_type', 'creator'].sort, json_response.keys.sort
    end

    test 'should return unprocessable entity if data is invalid' do
      post :create, widget: { parent_id: @channel.id, parent_type: 'Channel'}, format: :json
      json_response = get_json_response(response)
      assert_response :unprocessable_entity
      assert_equal "Code can't be blank, Context can't be blank or is invalid, and Context is not included in the list", json_response['message']
    end

    test 'should return unprocessable entity if parent is not passed' do
      post :create, widget: { parent_id: '', parent_type: ''}, format: :json
      json_response = get_json_response(response)
      assert_response :unprocessable_entity
      assert_equal 'Blank parent_type or parent_id', json_response['message']
    end

    test 'should return unprocessable entity if parent is not valid' do
      another_org = create(:organization)
      another_channel = create(:channel, organization: another_org)
      post :create, widget: { parent_id: another_channel.id, parent_type: 'Channel'}, format: :json
      json_response = get_json_response(response)
      assert_response :unprocessable_entity
      assert_equal 'Invalid parent', json_response['message']
    end

    test 'should return unprocessable entity if invalid context' do
      post :create, widget: { parent_id: @channel.id, parent_type: 'Channel', context: 'dummy', code: 'HTML Widget' }, format: :json
      json_response = get_json_response(response)
      assert_response :unprocessable_entity
      assert_equal "Context is not included in the list", json_response['message']
    end

    test 'should return unauthorized for non admin user' do
      non_admin_user = create(:user, organization: @organization)
      api_v2_sign_in(non_admin_user)

      post :create, widget: { parent_id: @channel.id, parent_type: 'Channel', context: :channel, code: 'HTML Widget'}, format: :json
      json_response = get_json_response(response)
      assert_response :unauthorized
      assert_equal 'User should be curator or creator of channel or organization admin', json_response['message']
    end

    test 'should authorize create action if the non admin user is curator' do
      non_admin_curator = create(:user, organization: @organization)
      @channel.curators << [non_admin_curator]
      api_v2_sign_in(non_admin_curator)

      post :create, widget: { parent_id: @channel.id, parent_type: 'Channel', context: :channel, code: 'HTML Widget'}, format: :json
      assert_response :ok
    end

    test 'should authorize create action if the non admin user is creator' do
      non_admin_creator = create(:user, organization: @organization)
      another_channel = create(:channel, organization: @organization, user_id: non_admin_creator.id)
      api_v2_sign_in(non_admin_creator)

      post :create, widget: { parent_id: another_channel.id, parent_type: 'Channel', context: :channel, code: 'HTML Widget'}, format: :json
      assert_response :ok
    end
  end

  class WidgetsControllerShowTest < ActionController::TestCase
    setup do
      @organization = create(:organization)
      @user = create(:user, organization: @organization, organization_role: 'admin')
      api_v2_sign_in(@user)
    end

    test ':api should return widget by id' do
      channel = create(:channel, organization: @organization)
      widget = create(:widget, organization: @organization, creator: @user,
        parent: channel, context: 'channel', code: 'Widget 1')

      get :show, id: widget.id, format: :json
      json_response = get_json_response(response)
      assert_response :ok
      assert_equal ['id', 'code', 'context', 'enabled', 'parent_id', 'parent_type', 'creator'].sort, json_response.keys.sort
      assert_equal json_response['id'], widget.id
    end

    test 'should return not found for invalid widget id' do
      get :show, id: 0, format: :json
      json_response = get_json_response(response)
      assert_response :not_found
      assert_equal 'Widget not found with id 0', json_response['message']
    end
  end

  class WidgetsControllerUpdateTest < ActionController::TestCase
    setup do
      @organization = create(:organization)
      @user = create(:user, organization: @organization, organization_role: 'admin')
      @channel = create(:channel, organization: @organization)
      @widget = create(:widget, organization: @organization, creator: @user,
        parent: @channel, context: 'channel', code: 'Widget 1')
      api_v2_sign_in(@user)
    end

    test ':api should update widget' do
      put :update, id: @widget.id, widget: { code: 'Widget 2' }, format: :json
      json_response = get_json_response(response)
      assert_response :ok
      assert_equal 'Widget 2', json_response['code']
    end

    test 'should return unauthorized for non admin user' do
      non_admin_user = create(:user, organization: @organization)
      api_v2_sign_in(non_admin_user)

      put :update, id: @widget.id, widget: { code: 'Widget 2' }, format: :json
      json_response = get_json_response(response)
      assert_response :unauthorized
      assert_equal 'User should be curator or creator of channel or organization admin', json_response['message']
    end

    test 'should authorize create action if the non admin user is curator' do
      non_admin_curator = create(:user, organization: @organization)
      @channel.curators << [non_admin_curator]
      api_v2_sign_in(non_admin_curator)

      put :update, id: @widget.id, widget: { code: 'Widget 2' }, format: :json
      assert_response :ok
    end

    test 'should authorize create action if the non admin user is creator' do
      non_admin_creator = create(:user, organization: @organization)
      another_channel = create(:channel, organization: @organization, user_id: non_admin_creator.id)
      api_v2_sign_in(non_admin_creator)

      another_widget = create(:widget, organization: @organization, creator: @user,
        parent: another_channel, context: 'channel', code: 'Widget 1')

      put :update, id: another_widget.id, widget: { code: 'Widget 2' }, format: :json
      assert_response :ok
    end
  end

  class WidgetsControllerDestroyTest < ActionController::TestCase
    setup do
      @organization = create(:organization)
      @user = create(:user, organization: @organization, organization_role: 'admin')
      @channel = create(:channel, organization: @organization)
      @widget = create(:widget, organization: @organization, creator: @user,
        parent: @channel, context: 'channel', code: 'Widget 1')
      api_v2_sign_in(@user)
    end

    test ':api should delete a widget' do
      put :destroy, id: @widget.id, format: :json
      assert_response :ok
    end

    test 'should return unauthorized for non admin user' do
      non_admin_user = create(:user, organization: @organization)
      api_v2_sign_in(non_admin_user)

      put :destroy, id: @widget.id, format: :json
      json_response = get_json_response(response)
      assert_response :unauthorized
      assert_equal 'User should be curator or creator of channel or organization admin', json_response['message']
    end

    test 'should authorize create action if the non admin user is curator' do
      non_admin_curator = create(:user, organization: @organization)
      @channel.curators << [non_admin_curator]
      api_v2_sign_in(non_admin_curator)

      put :destroy, id: @widget.id, format: :json
      assert_response :ok
    end

    test 'should authorize create action if the non admin user is creator' do
      non_admin_creator = create(:user, organization: @organization)
      another_channel = create(:channel, organization: @organization, user_id: non_admin_creator.id)
      api_v2_sign_in(non_admin_creator)

      another_widget = create(:widget, organization: @organization, creator: @user,
        parent: another_channel, context: 'channel', code: 'Widget 1')

      put :destroy, id: another_widget.id, format: :json
      assert_response :ok
    end
  end
end

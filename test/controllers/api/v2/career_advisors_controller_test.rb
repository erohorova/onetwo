require 'test_helper'

class Api::V2::CareerAdvisorsControllerTest < ActionController::TestCase
  class CareerAdvisorShowTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org, organization_role: 'admin')
      @career_advisor = CareerAdvisor.find_by(skill: 'data_scientist')
      api_v2_sign_in @user
    end

    test ':api should return career_advisor' do
      get :show, skill: @career_advisor.skill , format: :json
      resp = get_json_response(response)
      assert_response :ok
      assert_equal @career_advisor.skill, resp['skill']
    end

    test ':api should return card_label for career_advisor_cards' do
      skip("INTERMITTENT FAILURE ON TRAVIS")
      card = create(:card, card_type: 'course', organization: @org, author_id: @user.id)
      CareerAdvisorCard.create(card_id: card.id, level: 'beginner', career_advisor_id: @career_advisor.id)
      get :show, format: :json
      resp = get_json_response(response)
      assert_response :ok
      assert_equal card.message , resp['career_advisor_cards'][0]['card_label']
    end
  end
  class CareerAdvisorUpdateTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org, organization_role: 'admin')
      @career_advisor = CareerAdvisor.find_by(skill: 'data_scientist', organization_id: @org.id)
      api_v2_sign_in @user
    end

    test ':api should return error if not found' do
      put :update, career_advisor: {skill: 'skill'}, format: :json
      resp = get_json_response(response)
      assert_response :not_found
      assert_equal 'CareerAdvisor not found', resp['message']
    end

    test ':api should update params' do
      links = 'http://new_url.com, http://url.ru'
      card = create(:card, card_type: 'course', organization: @org, author_id: @user.id)
      assert_difference -> { CareerAdvisorCard.count }, 1 do
        put :update, career_advisor: {
          skill: @career_advisor.skill,
          links: links,
          career_advisor_cards_attributes: [{ card_id: card.id, level: 'beginner' }]
        }, format: :json
      end
      resp = get_json_response(response)
      assert_response :ok
      assert_equal links, resp['links']
      assert_equal 1, resp['career_advisor_cards'].count
      assert_equal card.id, resp['career_advisor_cards'][0]['card_id']
      assert_equal 'beginner', resp['career_advisor_cards'][0]['level']
    end

    test ':api should not create career_advisor_cards if card_id is not courses' do
      card = create(:card, card_type: 'pack', organization: @org, author_id: @user.id)
      assert_no_difference -> { CareerAdvisorCard.count } do
        put :update, career_advisor: {
          skill: @career_advisor.skill,
          career_advisor_cards_attributes: [{ card_id: card.id, level: 'beginner' }]
        }, format: :json
      end
      resp = get_json_response(response)
      assert_response :unprocessable_entity
      assert_equal 'Career advisor cards card is not a course' ,resp['message']
    end

    test ':api should destroy career_advisor_cards' do
      card = create(:card, card_type: 'course', organization: @org, author_id: @user.id)
      ca_card = CareerAdvisorCard.create(card_id: card.id, level: 'beginner', career_advisor_id: @career_advisor.id)
      assert_difference -> { CareerAdvisorCard.count }, -1 do
        put :update, career_advisor: {
          skill: @career_advisor.skill,
          career_advisor_cards_attributes: [id: ca_card.id, _destroy: 1]
        }, format: :json
      end
      resp = get_json_response(response)
      assert_response :ok
      assert_equal 0, resp['career_advisor_cards'].count
    end

    test 'should return an error if card_id is not unique' do
      card = create(:card, card_type: 'course', organization: @org, author_id: @user.id)
      CareerAdvisorCard.create(card_id: card.id, level: 'beginner', career_advisor_id: @career_advisor.id)
      put :update, career_advisor: {
        skill: @career_advisor.skill,
        career_advisor_cards_attributes: [{ card_id: card.id, level: 'beginner' }]
      }, format: :json
      resp = get_json_response(response)
      assert_response :unprocessable_entity
      assert_equal 'Career advisor cards card has already been taken' ,resp['message']
    end

    test 'should create career_advisor_card for the same course if another skill' do
      card = create(:card, card_type: 'course', organization: @org, author_id: @user.id)
      CareerAdvisorCard.create(card_id: card.id, level: 'beginner', career_advisor_id: @career_advisor.id)
      ca = CareerAdvisor.find_by(skill: 'data_architect')
      put :update, career_advisor: {
          skill: ca.skill,
          career_advisor_cards_attributes: [{ card_id: card.id, level: 'beginner' }]
      }, format: :json
      resp = get_json_response(response)
      assert_response :ok
      assert_equal card.id, resp['career_advisor_cards'][0]['card_id']
    end

    test 'should create career_advisor_card if ecl card' do
      ecl_id = 'qwe1-ewq1'
      card = create(:card, card_type: 'course', ecl_id: ecl_id, organization: @org, author_id: @user.id)
      EclApi::CardConnector.any_instance.expects(:card_from_ecl_id).returns(card)
      put :update, career_advisor: {
          skill: @career_advisor.skill,
          career_advisor_cards_attributes: [{ card_id: "ECL-#{ecl_id}", level: 'beginner' }]
      }, format: :json
      resp = get_json_response(response)
      assert_response :ok
      assert_equal card.id, resp['career_advisor_cards'][0]['card_id']
    end

    test 'should not update career_advisor_cards if not changes' do
      card = create(:card, card_type: 'course', organization: @org, author_id: @user.id)
      CareerAdvisorCard.create(card_id: card.id, level: 'beginner', career_advisor_id: @career_advisor.id)
      put :update, career_advisor: {
          skill: @career_advisor.skill,
          career_advisor_cards_attributes: []
      }, format: :json
      resp = get_json_response(response)
      assert_response :ok
      assert_equal card.id, resp['career_advisor_cards'][0]['card_id']
    end
  end
end

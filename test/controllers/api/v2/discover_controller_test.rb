require 'test_helper'

class Api::V2::DiscoverControllerTest < ActionController::TestCase
  setup do
    Organization.any_instance.unstub(:create_general_channel)
    org1 = create(:organization)
    Role.create_master_roles org1

    sme1 = org1.roles.find_by_name(Role::TYPE_SME)
    influencer1 = create :role, organization: org1, name: 'influencer'

    org1_infs = create_list(:user, 4, organization: org1)

    user_without_handle = create(:user, first_name: nil, last_name: nil, handle: nil, organization: org1)
    forum_user = create(:user, is_complete: false, organization: org1)
    suspended_user = create(:user, organization: org1, is_suspended: true)

    sme1.add_user(org1_infs[0])
    influencer1.add_user(org1_infs[1])
    sme1.add_user(org1_infs[2])
    influencer1.add_user(org1_infs[3])

    @user = create(:user, organization: org1)
    author = create(:user, organization: org1)
    create(:follow, followable: org1_infs.first, user: @user)
    covers = create_list(:card, 10, card_type: 'pack', state: 'published', author: author)
    Card.any_instance.stubs(:reindex).returns(true)
    covers.each do |cover|
      CardPackRelation.add(cover_id: cover.id, add_id: create(:card, author: @user).id)
      cover.publish!
    end
    create_list(:card, 3, card_type: 'video_stream', state: 'published', author: author)

    @channel1 = create(:channel, :robotics, organization: org1)
    @channel2 = create(:channel, :architecture, organization: org1)

    rg = create_list(:resource_group, 2, organization: org1, is_promoted: true)
    @user_org = org1
  end

  test ':api should get discover items counts' do
    api_v2_sign_in @user

    get :items_count, format: :json
    assert_response :success

    resp = get_json_response(response)
    assert_equal @user_org.users.count, resp['total_users_count']
    assert_equal 4, resp['total_suggested_users_count']
    # 3 channels [general + robotics + architecture]
    assert_equal 3, resp['channels_count']

    # suggested users [org1_infs[1] + org1_infs[2] + org1_infs[3]]
    assert_equal 3, resp['suggested_users_count']

    assert_equal 10, resp['pathways_count']
    assert_equal 3, resp['video_streams_count']
    assert_equal 2, resp['courses_count']
  end

  class ChannelsActionTest < ActionController::TestCase
    setup do
      @org   = create :organization
      @owner = create :user, organization: @org
      @user  = create :user, organization: @org
      @user_profile = create(:user_profile, user: @user, learning_topics: Taxonomies.domain_topics)

      @promoted_channel_1 = create :channel, organization: @org, user_id: @owner.id, is_promoted: true, label: 'Architecture',
        topics: [{'label' => 'Architecture', 'name' => 'edcast.technology.software_development.architecture'}]
      @promoted_channel_2 = create :channel, organization: @org, user_id: @owner.id, is_promoted: true, label: 'Machine',
        topics: [{'label' => 'Machine', 'name' => 'edcast.technology.software_development.machine'}]
      @promoted_channel_3 = create :channel, organization: @org, user_id: @owner.id, is_promoted: true, label: 'Learning',
        topics: [{'label' => 'Learning', 'name' => 'edcast.technology.software_development.learning'}]
      @channel_1 = create :channel, organization: @org, user_id: @owner.id
      @channel_2 = create :channel, organization: @org, user_id: @owner.id, label: 'Technology',
        topics: [{'label' => 'c#', 'name' => 'edcast.technology.software_development.c#'}]
      @channel_3 = create :channel, organization: @org, user_id: @owner.id, label: 'Finance',
        topics: [{'label' => 'Finance', 'name' => 'edcast.finance.finance'}]

      @promoted_channel_1.add_followers([@user])
      @channel_1.add_followers([@user])

      api_v2_sign_in @user
    end

    test ':api must return list of channels' do
      DiscoveryService::Channels.any_instance.expects(:discover).returns([
        @promoted_channel_1, @promoted_channel_2, @promoted_channel_3,
        @channel_1, @channel_2, @channel_3
      ])

      get :channels, limit: 6, offset: 0, format: :json

      assert_response :success
      resp = get_json_response(response)
      assert_equal 6, resp["channels"].count
    end
  end

  class TrendingCardsTest < ActionController::TestCase
    setup do
      @org   = create :organization
      @owner = create :user, organization: @org
      @user  = create :user, organization: @org
      @user_profile = create(:user_profile, user: @user, learning_topics: Taxonomies.domain_topics, language: 'ru')
      @latest_cards = []
      topics = Taxonomies.domain_topics.collect{|k| k["topic_name"] }

      # TRENDING CARDS
      now = Time.now.utc
      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

      7.times do |n|

        resource = create(:resource)
        file_resource = create(:file_resource, attachable: resource)
        @latest_cards << (card = create :card, author: @owner, taxonomy_topics: topics, resource: resource)
        applicable_offsets.each do |k, v|
          create(:content_level_metric, content_id: card.id, content_type: "card", views_count: n*2, comments_count: n, likes_count: n, completes_count: n, period: k, offset: v)
        end
      end

      api_v2_sign_in @user
    end

    test ':api must return list of trending cards' do
      Organization.any_instance.stubs(:reporting_content_enabled?).returns(true)
      DiscoveryService::UserCardDiscovery.any_instance.expects(:trending_cards).returns(@latest_cards.first(5))

      card_record = @latest_cards.first
      expected_keys = %w[
        id title message card_type card_subtype slug state is_official provider
        provider_image readable_card_type share_url average_rating skill_level
        filestack votes_count comments_count published_at prices is_assigned
        is_bookmarked is_reported hidden is_public author resource is_paid
        mark_feature_disabled_for_source completion_state all_ratings
        is_upvoted is_clone created_at paid_by_user due_at subscription_end_date
      ]
      fields = expected_keys.join(',')

      get :trending_cards, limit: 5, offset: 0, fields: fields, format: :json
      assert_response :success

      resp = get_json_response(response)
      card = resp['cards'].first
      assert_same_elements expected_keys, card.keys

      assert_equal card_record.id.to_s, card['id']
      assert_equal 5, resp["cards"].count
    end

    test ':api must return is_new key for trending cards if last_access_at was passed' do
      Organization.any_instance.stubs(:reporting_content_enabled?).returns(true)
      DiscoveryService::UserCardDiscovery.any_instance.expects(:trending_cards).returns(@latest_cards.first(5))

      get :trending_cards, fields: 'is_new', last_access_at: (Time.now - 1.week).to_i, limit: 5, offset: 0, format: :json
      assert_response :success

      resp = get_json_response(response)
      assert_includes resp['cards'].first, 'is_new'
    end

    test ':api must return list of trending cards filtered by language' do
      Organization.any_instance.stubs(:reporting_content_enabled?).returns(true)
      DiscoveryService::UserCardDiscovery.any_instance.expects(:trending_cards).returns(@latest_cards.first(5))
      @latest_cards[0].update_attributes(language: 'en')
      @latest_cards[1].update_attributes(language: 'ru')
      @latest_cards[2].update_attributes(language: 'es')

      get :trending_cards, filter_by_language: true, limit: 5, offset: 0, format: :json
      assert_response :success
      resp = get_json_response(response)
      assert_not_includes resp["cards"].map{|c| c['id'].to_i}, @latest_cards.last(2)
    end
  end

  class TrendingPathwaysTest < ActionController::TestCase
    setup do
      @org   = create :organization
      @owner = create :user, organization: @org
      @user  = create :user, organization: @org
      @user_profile = create(:user_profile, user: @user, learning_topics: Taxonomies.domain_topics, language: 'ru')
      @latest_cards = []
      @old_cards = []
      topics = Taxonomies.domain_topics.collect{|k| k["topic_name"] }

      # TRENDING PATHWAYS
      now = Time.now.utc
      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

      7.times do |n|

        resource = create(:resource)
        file_resource = create(:file_resource, attachable: resource)
        cover = create :card, author: @owner, taxonomy_topics: topics, resource: resource, card_type: 'pack'
        CardPackRelation.add(cover_id: cover.id, add_id: create(:card, author: @owner).id)
        cover.publish!

        @latest_cards << cover
        applicable_offsets.each do |k, v|
          create(:content_level_metric, content_id: cover.id, content_type: "card", views_count: n*2, comments_count: n, likes_count: n, completes_count: n, period: k, offset: v)
        end
      end
      api_v2_sign_in @user
    end

    test ':api must return list of trending pathways on demand' do
      DiscoveryService::UserCardDiscovery.any_instance.expects(:personalized_trending_pathway).returns(@latest_cards.first(5))
      fields = 'title,id,slug,filestack,pack_cards_count,message,card_type,due_at,subscription_end_date,paid_by_user'
      card_record = @latest_cards.first
      get :trending_pathways, limit: 5, offset: 0, fields: fields, format: :json
      assert_response :success

      resp = get_json_response(response)
      card = resp['cards'].first
      assert_same_elements ['title','id','filestack','pack_cards_count','message','slug', 'card_type',
                            'subscription_end_date', 'due_at', 'paid_by_user'], card.keys

      assert_equal card_record.id, card['id'].to_i
      assert_equal 1, card['pack_cards_count']
      assert_equal 5, resp['cards'].count
    end

    test ':api must return is_new key for trending pathways if last_access_at was passed' do
      DiscoveryService::UserCardDiscovery.any_instance.expects(:personalized_trending_pathway).returns(@latest_cards.first(5))

      get :trending_pathways, fields: 'is_new', last_access_at: (Time.now - 1.week).to_i, limit: 5, offset: 0, format: :json
      assert_response :success

      resp = get_json_response(response)
      assert_includes resp['cards'].first, 'is_new'
    end

    test ':api must return list of trending pathways filtered by language' do
      DiscoveryService::UserCardDiscovery.any_instance.expects(:personalized_trending_pathway).returns(@latest_cards.first(3))
      get :trending_pathways, filter_by_language: true, limit: 5, offset: 0, format: :json
      assert_response :success
      resp = get_json_response(response)
      assert_equal 3, resp['cards'].count
      assert_equal resp['cards'].map{|c| c['id'].to_i}, @latest_cards.first(3).map(&:id)
    end

    test ':api should not return draft pathways' do
      resource = create(:resource)
      file_resource = create(:file_resource, attachable: resource)
      draft_pathway = create :card, author: @owner, resource: resource, card_type: 'pack', state: 'draft'
      CardPackRelation.add(cover_id: draft_pathway.id, add_id: create(:card, author: @owner).id)
      @latest_cards << draft_pathway

      DiscoveryService::UserCardDiscovery.any_instance.expects(:personalized_trending_pathway).returns(@latest_cards.first(7))

      publish_pathway = @latest_cards.first(7)
      get :trending_pathways, limit: 5, offset: 0, format: :json
      assert_response :success

      resp = get_json_response(response)
      assert_not_includes publish_pathway, draft_pathway
    end
  end

  class TrendingOrgPathwaysTest < ActionController::TestCase
    setup do
      @user = create(:user)
      create(:config, name: 'enable_trending_org_pathway', value: 'true', configable: @user.organization)
      @pathways = create_list(:card, 7, card_type: 'pack', author: @user)

      now = Time.now.utc
      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

      applicable_offsets.each do |k, v|
        create(:content_level_metric, content_id: @pathways[0].id, content_type: "collection", views_count: 20, comments_count: 10, likes_count: 1, completes_count: 1, bookmarks_count: 6, period: k, offset: v) #620
        create(:content_level_metric, content_id: @pathways[1].id, content_type: "collection", views_count: 30, comments_count: 20, likes_count: 2, completes_count: 2, bookmarks_count: 9, period: k, offset: v) #1050
        create(:content_level_metric, content_id: @pathways[2].id, content_type: "collection", views_count: 40, comments_count: 30, likes_count: 3, completes_count: 4, bookmarks_count: 10, period: k, offset: v) #1445
        create(:content_level_metric, content_id: @pathways[3].id, content_type: "collection", views_count: 20, comments_count: 10, likes_count: 1, completes_count: 3, bookmarks_count: 8, period: k, offset: v) #730
        create(:content_level_metric, content_id: @pathways[4].id, content_type: "collection", views_count: 0, comments_count: 0, likes_count: 0, completes_count: 0, bookmarks_count: 1, period: k, offset: v) #30
        create(:content_level_metric, content_id: @pathways[5].id, content_type: "collection", views_count: 2, comments_count: 0, likes_count: 0, completes_count: 0, bookmarks_count: 2, period: k, offset: v) #80
        create(:content_level_metric, content_id: @pathways[6].id, content_type: "collection", views_count: 11, comments_count: 0, likes_count: 20, completes_count: 0, bookmarks_count: 4, period: k, offset: v) #530
      end
      api_v2_sign_in @user
    end

    test 'trending pathways api should trigger appropriate method for trending org pathway' do
      DiscoveryService::UserCardDiscovery.any_instance.expects(:trending_pathways).returns(@pathways).once
      get :trending_pathways, limit: 5, offset: 0, format: :json
    end

    test 'should get trending org pathways based on interaction parameters when config is set' do
      DiscoveryService::UserCardDiscovery.any_instance.expects(:trending_pathways).returns(
        [@pathways[2], @pathways[1], @pathways[3], @pathways[0], @pathways[6]]
      )
      get :trending_pathways, limit: 5, offset: 0, format: :json
      resp = get_json_response(response)
      assert_equal 5, resp['cards'].count
      assert_not_includes resp['cards'].map{|d| d['id'].to_i}, @pathways[5].id
      assert_equal resp['cards'].map{|d| d['id'].to_i}, [@pathways[2].id, @pathways[1].id, @pathways[3].id, @pathways[0].id, @pathways[6].id]
    end
  end

  class TrendingJourneysTest < ActionController::TestCase
    setup do
      @org   = create :organization
      @owner = create :user, organization: @org
      @user  = create :user, organization: @org
      @user_profile = create(:user_profile, user: @user, learning_topics: Taxonomies.domain_topics, language: 'ru')
      @latest_cards = []
      @old_cards = []
      topics = Taxonomies.domain_topics.collect{|k| k["topic_name"] }

      # TRENDING PATHWAYS
      now = Time.now.utc
      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

      7.times do |n|
        cover = create :card, author: @owner, taxonomy_topics: topics, card_type: 'journey', card_subtype: 'self_paced'
        cover.publish!

        @latest_cards << cover
        applicable_offsets.each do |k, v|
          create(:content_level_metric, content_id: cover.id, content_type: "card", views_count: n*2, comments_count: n, likes_count: n, completes_count: n, period: k, offset: v)
        end
      end
      api_v2_sign_in @user
    end

    test ':api must return list of trending journeys on demand' do
      DiscoveryService::UserCardDiscovery.any_instance.expects(:personalized_trending_journey).returns(@latest_cards.first(5))

      card_record = @latest_cards.first
      fields = 'title,id,slug,filestack,journey_packs_count,message,card_type,due_at,subscription_end_date,paid_by_user'
      get :trending_journeys, limit: 5, offset: 0, fields: fields, format: :json
      assert_response :success

      resp = get_json_response(response)
      card = resp["cards"].first
      assert_same_elements ['title','id','filestack','journey_packs_count','message','slug', 'card_type',
                             'subscription_end_date', 'due_at', 'paid_by_user'], card.keys

      assert_equal card_record.id, card['id'].to_i
      assert_equal 0, card['journey_packs_count']
      assert_equal 5, resp["cards"].count
    end

    test ':api must return is_new key for trending journeys if last_access_at was passed' do
      journey = create(
        :card, card_type: 'journey', card_subtype: 'self_paced',
        author: @user, organization_id: @org.id
      )
      section = create(
        :card, hidden: true, state: 'draft', card_type: 'pack',
        card_subtype: 'simple', author: @user, organization_id: @org.id
      )

      JourneyPackRelation.add(cover_id: journey.id, add_id: section.id)

      DiscoveryService::UserCardDiscovery.any_instance.expects(:personalized_trending_journey).returns([journey])

      get :trending_journeys, fields: 'is_new', last_access_at: (Time.now - 1.week).to_i, limit: 5, offset: 0, format: :json
      assert_response :success

      resp = get_json_response(response)
      assert_includes resp['cards'].first, 'is_new'
    end

    test ':api must return list of trending journeys filtered by language' do
      DiscoveryService::UserCardDiscovery.any_instance.expects(:personalized_trending_journey).returns(@latest_cards.first(2))

      get :trending_journeys, filter_by_language: true, limit: 5, offset: 0, format: :json
      assert_response :success
      resp = get_json_response(response)
      assert_equal 2, resp['cards'].count
      assert_equal resp['cards'].map{|c| c['id'].to_i}, @latest_cards.first(2).map(&:id)
    end

    test ':api should not return draft journeys' do
      resource = create(:resource)
      file_resource = create(:file_resource, attachable: resource)
      pathway = create :card, author: @owner, resource: resource, card_type: 'pack', state: 'published'
      draft_journey = create :card, author: @owner, card_type: 'journey', card_subtype: 'self_paced', state: 'draft'
      CardPackRelation.add(cover_id: pathway.id, add_id: create(:card, author: @owner).id)
      JourneyPackRelation.add(cover_id: draft_journey.id, add_id: pathway.id)
      @latest_cards << draft_journey

      DiscoveryService::UserCardDiscovery.any_instance.expects(:personalized_trending_journey).returns(@latest_cards.first(7))

      publish_journey = @latest_cards.first(7)
      get :trending_journeys, limit: 5, offset: 0, format: :json
      assert_response :success

      resp = get_json_response(response)
      assert_not_includes publish_journey, draft_journey
    end

  end

  class TrendingOrgJourneysTest < ActionController::TestCase
    setup do
      @user = create(:user)
      create(:config, name: 'enable_trending_org_journey', value: 'true', configable: @user.organization)
      @journeys = create_list(:card, 7, card_type: 'journey', card_subtype: 'self_paced', author: @user)

      now = Time.now.utc
      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

      applicable_offsets.each do |k, v|
        create(:content_level_metric, content_id: @journeys[0].id, content_type: "card", views_count: 20, comments_count: 10, likes_count: 1, completes_count: 1, bookmarks_count: 6, period: k, offset: v) #620
        create(:content_level_metric, content_id: @journeys[1].id, content_type: "card", views_count: 30, comments_count: 20, likes_count: 2, completes_count: 2, bookmarks_count: 9, period: k, offset: v) #1050
        create(:content_level_metric, content_id: @journeys[2].id, content_type: "card", views_count: 40, comments_count: 30, likes_count: 3, completes_count: 4, bookmarks_count: 10, period: k, offset: v) #1445
        create(:content_level_metric, content_id: @journeys[3].id, content_type: "card", views_count: 20, comments_count: 10, likes_count: 1, completes_count: 3, bookmarks_count: 8, period: k, offset: v) #730
        create(:content_level_metric, content_id: @journeys[4].id, content_type: "card", views_count: 0, comments_count: 0, likes_count: 0, completes_count: 0, bookmarks_count: 1, period: k, offset: v) #30
        create(:content_level_metric, content_id: @journeys[5].id, content_type: "card", views_count: 2, comments_count: 0, likes_count: 0, completes_count: 0, bookmarks_count: 2, period: k, offset: v) #80
        create(:content_level_metric, content_id: @journeys[6].id, content_type: "card", views_count: 11, comments_count: 0, likes_count: 20, completes_count: 0, bookmarks_count: 4, period: k, offset: v) #530
      end
      api_v2_sign_in @user
    end

    test 'trending journeys api should trigger appropriate method for trending org journey' do
      DiscoveryService::UserCardDiscovery.any_instance.expects(:trending_journeys).returns(@journeys).once
      get :trending_journeys, limit: 5, offset: 0, format: :json
    end

    test 'should get trending org journeys based on interaction parameters when config is set' do
      DiscoveryService::UserCardDiscovery.any_instance.expects(:trending_journeys).returns(
        [@journeys[2], @journeys[1], @journeys[3], @journeys[0], @journeys[6]]
      )
      get :trending_journeys, limit: 5, offset: 0, format: :json
      resp = get_json_response(response)
      assert_equal 5, resp['cards'].count
      assert_not_includes resp['cards'].map{|d| d['id'].to_i}, @journeys[5].id
      assert_equal resp['cards'].map{|d| d['id'].to_i}, [@journeys[2].id, @journeys[1].id, @journeys[3].id, @journeys[0].id, @journeys[6].id]
    end
  end

  class InterestedPathwaysJourneysCoursesTest < ActionController::TestCase
    setup do
      @org   = create :organization
      @owner = create :user, organization: @org
      @user  = create :user, organization: @org
      @user2  = create :user, organization: @org
      @user_profile = create(:user_profile, user: @user, learning_topics: Taxonomies.domain_topics)
      @channel1 = create(:channel, :architecture, organization_id: @org.id)
      @channel2 = create(:channel, :good_music, organization_id: @org.id)
      @user.follows.create(followable: @channel1)
      @user.follows.create(followable: @channel2)
      @latest_cards = []
      @non_taxonomies_cards = []
      @old_cards = []
      @packs = []
      @courses = []
      @journeys = []
      topics = Taxonomies.domain_topics.collect{|k| k["topic_name"] }

      # TRENDING PATHWAYS
      # now = Time.now.utc
      # applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

      2.times do |n|

        resource = create(:resource)
        file_resource = create(:file_resource, attachable: resource)
        cover = create :card, author: @owner, taxonomy_topics: topics, resource: resource, card_type: 'pack'
        CardPackRelation.add(cover_id: cover.id, add_id: create(:card, author: @owner).id)
        cover.publish!

        @packs << cover
      end

      3.times do |n|

        cover = create :card, author: @owner, taxonomy_topics: topics, card_type: 'course'
        cover.publish!

        @courses << cover
      end

      @latest_cards = [@packs, @courses].flatten

      #waiting for journey test cases
      # 2.times do |n|
      #   resource = create(:resource)
      #   file_resource = create(:file_resource, attachable: resource)
      #   cover = create :card, author: @owner, taxonomy_topics: topics, resource: resource, card_type: 'journey', card_subtype: 'self_paced'
      #   resource = create(:resource)
      #   file_resource = create(:file_resource, attachable: resource)
      #   pack = create :card, author: @owner, taxonomy_topics: topics, resource: resource, card_type: 'pack', card_subtype: 'simple'
      #   CardPackRelation.add(cover_id: pack.id, add_id: create(:card, author: @owner).id)
      #   JourneyPackRelation.add(cover_id:  cover.id, add_id: pack.id)
      #   debugger
      #   cover.publish!

      #   @journeys << cover
      # end

      3.times do |n|

        resource = create(:resource)
        file_resource = create(:file_resource, attachable: resource)
        cover = create :card, author: @owner, resource: resource, card_type: 'pack'
        CardPackRelation.add(cover_id: cover.id, add_id: create(:card, author: @owner).id)
        cover.publish!

        @non_taxonomies_cards << cover
      end

      2.times do |n|

        cover = create :card, author: @owner, card_type: 'course'
        cover.publish!

        @non_taxonomies_cards << cover
      end

      @channel1_posted_cards = @non_taxonomies_cards.last(3)
      @channel2_posted_cards = @non_taxonomies_cards.first(3)

      @channel1_posted_cards.each do |card|
        card.channels << @channel1
      end

      @channel2_posted_cards.each do |card|
        card.channels << @channel2
      end

      @user.reload
      api_v2_sign_in @user
    end

    test ':api should return 204 if no cards to return' do
      Search::CardSearch.any_instance.expects(:search)
        .returns(Search::CardSearchResponse.new(results: ([]).map(&:search_data), total: 0))

      get :interested_cards, limit: 5, offset: 1000, format: :json
      assert_response 204
      assert "No Content", response.message
    end

    test ':api must return list of interested and journey pathways' do
      DiscoveryService::UserCardDiscovery.any_instance.expects(:interested_cards).returns(
        @latest_cards
      )
      card_record = @latest_cards.first
      get :interested_cards, limit: 5, offset: 0, format: :json
      assert_response :success

      resp = get_json_response(response)
      cards = resp["cards"]
      keys = %w[
        id title share_url completed_percentage ecl_duration_metadata message
        card_type card_subtype slug state is_official created_at updated_at
        published_at comments_count views_count votes_count channel_ids
        non_curated_channel_ids average_rating user_rating all_ratings is_upvoted
        is_bookmarked is_assigned completion_state resource mentions
        author tags file_resources channels pack_cards filestack voters teams
        hidden ecl_id  skill_level readable_card_type auto_complete provider
        user_taxonomy_topics users_with_access is_public is_paid
        prices mark_feature_disabled_for_source paid_by_user payment_enabled language
      ]
      assert_same_elements keys, cards[0].keys

      assert_equal @latest_cards.map(&:id), cards.collect{|i| i['id'].to_i}
      assert_equal 5, resp["cards"].count
    end

    test ':api must return list of interested cards from followed channels' do
      topics = Taxonomies.domain_topics.collect{|k| k["topic_name"] }
      @cards = []
      @not_interested_cards = []

      2.times do |n|
        card = create :card, author: @owner, taxonomy_topics: topics, state: 'published'
        @cards << card
      end

      2.times do |n|
        card = create :card, author: @owner, state: 'published'
        @not_interested_cards << card
      end
      @interested_cards = [@cards].flatten
      @user.reload
      api_v2_sign_in @user
      DiscoveryService::UserCardDiscovery.any_instance.expects(:relevent_interested_cards).returns(
        @interested_cards
      )
      get :user_interested_cards, limit: 10, offset: 0, format: :json
      assert_response :success
      resp = get_json_response(response)
      assert_equal @interested_cards.map { |card| card['id'].to_i }, resp["cards"].map { |card| card['id'].to_i }
      assert_equal @interested_cards.length,resp["cards"].length
      assert_not_equal @not_interested_cards.map { |card| card['id'].to_i }, resp["cards"].map { |card| card['id'].to_i }
    end

    test 'api must return list of interested cards from followed channels which are curated' do
      topics = Taxonomies.domain_topics.collect{|k| k["topic_name"] }
      @cards = []
      2.times do |n|
        card = create :card, author: @owner, taxonomy_topics: topics, state: 'published'
        @cards << card
      end

      non_curatable_channel = create(:channel, organization: @org)
      curatable_channel = create(:channel, organization: @org, curate_only: true)

      create(:follow, followable: curatable_channel, user_id: @user.id)
      create(:follow, followable: non_curatable_channel, user_id: @user.id)

      channel_card1 = create(:channels_card, channel_id: curatable_channel.id, card_id: @cards[0].id)
      channel_card2 = create(:channels_card, channel_id: non_curatable_channel.id, card_id: @cards[1].id)

      @cards.map { |card| card.reindex }
      @user.reload
      api_v2_sign_in @user2
      @interested_cards = [@cards[1]].flatten
      DiscoveryService::UserCardDiscovery.any_instance.expects(:relevent_interested_cards).returns(
        @interested_cards
      )
      get :user_interested_cards, limit: 10, offset: 0, format: :json
      resp = get_json_response(response)
      assert_equal @interested_cards.map { |card| card['id'].to_i }, resp["cards"].map { |card| card['id'].to_i }
      assert_equal @interested_cards.length,resp["cards"].length
    end

    test ':api must return list of interserted pathways and journey for particular channels' do
      get :cards_by_channels, limit: 5, offset: 0, format: :json
      assert_response :success

      resp = get_json_response(response)
      cards = resp["cards"]
      keys = %w[
        id title share_url completed_percentage ecl_duration_metadata
        message card_type card_subtype slug state is_official created_at
        updated_at published_at comments_count views_count votes_count
        channel_ids non_curated_channel_ids average_rating user_rating all_ratings
        is_upvoted is_bookmarked is_assigned completion_state resource
        mentions author tags file_resources channels pack_cards filestack voters
        teams hidden ecl_id  skill_level readable_card_type auto_complete provider
        user_taxonomy_topics users_with_access is_public is_paid
        prices mark_feature_disabled_for_source paid_by_user payment_enabled language
      ]
      assert_same_elements keys, cards[0].keys

      assert_equal @non_taxonomies_cards.map(&:id), cards.collect{|i| i['id'].to_i}.sort
      assert_equal 5, cards.count
    end

    test 'include users card in cards_by_channels response' do
      channel = create(:channel, organization: @org)
      card = create(:card, card_type: 'course', card_subtype: 'link', author: @user)
      channel.add_followers [@user]
      card.channels << channel

      get :cards_by_channels, limit: 10, offset: 0, format: :json
      assert_response :success

      resp = get_json_response(response)
      assert_includes resp["cards"].map{|c| c["id"].to_i}, card.id
    end

    test 'include users team followed channels cards in cards_by_channels response' do
      team = create(:team, organization: @org)
      team.add_user @user

      channel = create(:channel, organization: @org)
      create(:teams_channels_follow, team: team, channel: channel)

      card = create(:card, card_type: 'course', card_subtype: 'link', author: @owner)
      card.channels << channel

      get :cards_by_channels, limit: 10, offset: 0, format: :json
      assert_response :success

      resp = get_json_response(response)
      assert_includes resp["cards"].map{|c| c["id"].to_i}, card.id
    end

    test ':api must return 204 when trying to fetch channel cards for the user who doesnt follows any channel' do
      api_v2_sign_in @user2
      get :cards_by_channels, limit: 5, offset: 0, format: :json
      assert_response 204
      assert "No Content", response.message
    end
  end

  class VideosTest < ActionController::TestCase
    setup do
      @org   = create :organization
      @owner = create :user, organization: @org
      @user  = create :user, organization: @org
      @user_profile = create(:user_profile, user: @user, learning_topics: Taxonomies.domain_topics, language: 'ru')

      # TRENDING CARDS
      @latest_cards = create_list :video_card, 2, author: @owner
      @search_card  = create(:video_card, author: @owner, title: 'Search card')

      @user.reload

      api_v2_sign_in @user
    end

    test ':api must return list of video cards' do
      Organization.any_instance.stubs(:reporting_content_enabled?).returns(true)
      card_record = @latest_cards.flatten.first
      DiscoveryService::Videos.any_instance.expects(:video_cards).returns([card_record])
      expected_keys = %w[
        id title message card_type card_subtype slug state is_official provider
        provider_image readable_card_type share_url average_rating skill_level
        filestack votes_count comments_count published_at prices is_assigned
        is_bookmarked is_reported hidden is_public is_paid
        mark_feature_disabled_for_source completion_state is_upvoted
        all_ratings author resource is_clone created_at paid_by_user due_at
        subscription_end_date
      ]
      fields = expected_keys.join(',')

      get :videos, fields: fields, format: :json
      assert_response :success

      resp = get_json_response(response)
      card = resp['videos'].first
      assert_same_elements expected_keys, card.keys
      assert_equal card_record.id.to_s, card['id']
    end

    test ':api must return is_new key for video cards if last_access_at was passed' do
      Organization.any_instance.stubs(:reporting_content_enabled?).returns(true)
      DiscoveryService::Videos.any_instance.expects(:video_cards).returns([@latest_cards.flatten.first])

      get :videos, fields: 'is_new', last_access_at: (Time.now - 1.week).to_i, limit: 5, offset: 0, format: :json
      assert_response :success

      resp = get_json_response(response)
      assert_includes resp['videos'].first, 'is_new'
    end

    test ':api must return list of video cards filtered by language' do
      Organization.any_instance.stubs(:reporting_content_enabled?).returns(true)
      DiscoveryService::Videos.any_instance.expects(:video_cards).returns(@latest_cards.first(2))
      get :videos, filter_by_language: true, format: :json

      assert_response :success
      resp = get_json_response(response)
      assert_equal 2, resp["videos"].count
      assert_equal @latest_cards.first(2).map(&:id), resp["videos"].map { |c| c['id'].to_i }
    end

    test 'must return list of paginated video cards' do
      DiscoveryService::Videos.any_instance.expects(:video_cards).returns(@latest_cards + [@search_card])

      get :videos, limit: 10, offset: 0, format: :json
      assert_response :success
      cards = get_json_response(response)["videos"]
      assert_equal 3, cards.count
    end

    test 'must return video cards by search term' do
      DiscoveryService::Videos.any_instance.expects(:video_cards).returns([])

      get :videos, limit: 10, offset: 0, q: 'test', format: :json
      assert_response :success
      cards = get_json_response(response)["videos"]
      assert_equal 0, cards.count

      DiscoveryService::Videos.any_instance.expects(:video_cards).returns([@search_card])

      get :videos, limit: 10, offset: 0, q: 'search', format: :json
      assert_response :success
      cards = get_json_response(response)["videos"]
      assert_equal 1, cards.count
      assert_equal @search_card.id.to_s, cards.first["id"]
    end

    test 'must return video cards considering visibility of content' do
      cards_list = create_list(:card, 3, author: @owner, is_public: false)
      team = create(:team, organization: @org)
      team.add_member @owner
      team.add_member @user

      create(:shared_card, team_id: team.id, card_id: cards_list[0].id)
      create(:card_user_permission, card_id: cards_list[0].id, user_id: @user.id)
      create(:card_user_share, card_id:  cards_list[1].id, user_id: @user.id)

      Search::CardSearch
        .any_instance
        .expects(:search)
        .returns(Search::CardSearchResponse.new(results: cards_list), total: 3)
      
      get :videos, limit: 3, offset: 0, format: :json
      resp = get_json_response(response)

      assert_equal 2, resp["videos"].count
      assert_same_elements resp["videos"].map{|d| d['id'].to_i}, [cards_list[0].id, cards_list[1].id]
      assert_not_includes resp["videos"].map{|d| d['id'].to_i}, cards_list[2].id
    end

  end

  class CarouselContentTest < ActionController::TestCase
    setup do
      org   = create :organization
      owner = create :user, organization: org
      @user  = create :user, organization: org

      @cards = (create_list :card, 2, author: owner)
      @cards1 = create_list(:card, 2, author: owner, is_public: false)
      create(:card_user_share, card_id: @cards1[0].id, user_id: @user.id)

      card_hash = []
      (@cards + @cards1).each_with_index do |card, index|
        card_hash << {"card_id" => card.id, "index" => index}
      end

      carousel_item = {
        "defaultLabel" => "User given label",
        "visible" => true,
        "index" => 1,
        "cards" => card_hash
      }

      DiscoveryService::CarouselContent.any_instance.stubs(:get_carousel).returns(carousel_item)
      api_v2_sign_in @user
    end

    test ':api must return list of carousel_content' do
      DiscoveryService::CarouselContent.any_instance.expects(:get_content).returns @cards
      get :carousel_content, uuid: "1234", format: :json
      assert_response :success
      resp = get_json_response(response)
      assert_same_elements @cards.map(&:id), resp["cards"].map{|c| c['id'].to_i}
    end

    test "return 422 if uuid is not present" do
      get :carousel_content, format: :json
      assert_response :unprocessable_entity
      resp = get_json_response response
      assert_equal "UUID not present", resp["message"]
    end

    test "should not return cards not accessible to user even if it's present in the carousel" do
      get :carousel_content, uuid: "1234", format: :json
      assert_response :success
      resp = get_json_response(response)
      assert_same_elements resp['cards'].map{|d| d['id'].to_i}, [@cards[0].id, @cards[1].id, @cards1[0].id]
      assert_not_includes resp['cards'].map{|d| d['id'].to_i}, @cards1[1].id
    end
  end
end

require 'test_helper'

class Api::V2::RechargesControllerTest < ActionController::TestCase

  test ':api should list all active recharges available for wallet recharge' do
    org = create(:organization)
    user = create(:user, organization: org)

    api_v2_sign_in(user)

    recharge = create(:recharge)
    recharge_price = create(:recharge_price, recharge: recharge)

    get :index, format: :json
    resp = get_json_response(response)

    assert_equal Recharge.count, resp["recharges"].count
    assert_equal Recharge.first.id, resp["recharges"].first["id"]
    assert_not_empty resp["recharges"].first["prices"]
  end
end

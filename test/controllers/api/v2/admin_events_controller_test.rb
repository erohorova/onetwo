require 'test_helper'

class Api::V2::AdminEventsControllerTest < ActionController::TestCase

  setup do
    skip("Endpoints have been disabled")

    @org = create :organization
    @admin = create :user, organization: @org, organization_role: "admin"
    @member = create :user, organization: @org
    @integration_name = "foo"
    @integration_attributes = {
      "a" => "b",
      "c" => "1"
    }

    @time = Time.now
    Time.stubs(:now).returns @time
    @timestamp = @time.to_i

    @ensure_endpoint_requires_admin = -> (endpoint) do
      api_v2_sign_in(@member)
      post endpoint
      body = get_json_response response
      assert_equal 401, response.status
      assert_equal "unauthorized action", body["message"]
    end

    @send_request = -> (endpoint, params={}) do
      post endpoint, params
      body = get_json_response response
      [response.status, body]
    end

    @metadata = {
      user_id: @admin.id,
      platform: "web",
      user_agent: "Rails Testing",
      platform_version_number: nil,
      is_admin_request: 0
    }

    @stub_request_store = -> do
      RequestStore.stubs(:read).with(:request_metadata).returns(@metadata)
    end

    @require_integration_name = -> (endpoint) do
      # raises error if integration_name is absent or not a string
      [{}, integration_name: []].each do |param_set|
        status, body = @send_request.call endpoint, param_set
        assert_equal 422, status
        assert_equal(
          (
            "Param integration_name is required. " +
            "It was either not given or is not of type String."
          ),
          body['message']
        )
      end
    end

    @require_integration_attributes = -> (endpoint) do
      # raises error if integration_name is absent or not a string
      [
        { integration_name: "foo" },
        { integration_name: "foo", integration_attributes: "bar" }
      ].each do |param_set|
        status, body = @send_request.call endpoint, param_set
        assert_equal 422, status
        assert_equal(
          (
            "Param integration_attributes is required. " +
            "It was either not given or is not of type Hash."
          ),
          body['message']
        )
      end
    end

    @recorder_job = Analytics::MetricsRecorderJob
    @default_recorder_params = {
      actor: Analytics::MetricsRecorder.user_attributes(@admin),
      org: Analytics::MetricsRecorder.org_attributes(@org),
      recorder: "Analytics::IntegrationMetricsRecorder",
      timestamp: @timestamp,
      additional_data: @metadata
    }

    Analytics::MetricsRecorderJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
  end

  test 'integration_ecl_content_access_enabled/disabled' do
    # These two endpoints function the same, just with a different event name.
    endpoints = %i{
      integration_ecl_content_access_enabled
      integration_ecl_content_access_disabled
    }
    endpoints.each do |endpoint|
      event = endpoint.to_s
      @ensure_endpoint_requires_admin.call endpoint

      api_v2_sign_in @admin

      @stub_request_store.call

      @recorder_job.expects(:perform_later).with(
        @default_recorder_params.merge(event: event)
      )

      status, body = @send_request.call endpoint
      assert_equal 200, status
    end
  end

  test 'endpoints which require integration_name and integration_attributes' do
    # These endpoints function the same, just with a different event name.
    endpoints = %i{
      integration_added
      integration_edited
      lrs_integration_added
      lrs_integration_edited
    }
    endpoints.each do |endpoint|
      event = endpoint.to_s
      @ensure_endpoint_requires_admin.call endpoint

      api_v2_sign_in @admin

      @stub_request_store.call

      @require_integration_name.call endpoint
      @require_integration_attributes.call endpoint

      @recorder_job.expects(:perform_later).with(
        @default_recorder_params.merge(
          event: event,
          integration_name: @integration_name,
          integration_attributes: @integration_attributes
        )
      )

      status, body = @send_request.call(
        endpoint,
        integration_name: @integration_name,
        integration_attributes: @integration_attributes
      )
      assert_equal 200, status
    end
  end

  test 'endpoints which require integration_name' do
    # These endpoints function the same, just with a different event name.
    endpoints = %i{
      integration_removed
      integration_activated
      integration_deactivated
      integration_featured
      integration_unfeatured
      integration_content_fetched
      lrs_integration_activated
      lrs_integration_deactivated
      lrs_integration_removed
    }
    endpoints.each do |endpoint|
      event = endpoint.to_s
      @ensure_endpoint_requires_admin.call endpoint

      api_v2_sign_in @admin

      @stub_request_store.call

      @require_integration_name.call endpoint

      @recorder_job.expects(:perform_later).with(
        @default_recorder_params.merge(
          event: event,
          integration_name: @integration_name,
        )
      )

      status, body = @send_request.call(
        endpoint,
        integration_name: @integration_name,
      )
      assert_equal 200, status
    end
  end

end

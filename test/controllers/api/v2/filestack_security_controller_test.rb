require 'test_helper'

class Api::V2::FilestackSecurityControllerTest < ActionController::TestCase
  setup do
    @user = create(:user)
    api_v2_sign_in(@user)
  end

  test "returns secured filestack URL" do
    get :signed_url, url: "http://cdn.filestackcontent.com/handle"
    resp = JSON.parse(response.body)
    url = resp["signed_url"]

    uri = URI.parse(url)
    path = uri.path

    assert url.present?
    assert_includes path, "handle"
    assert_includes path, "security=p:"
  end

  test "returns error message when url not present" do
    get :signed_url
    resp = JSON.parse(response.body)
    assert_response :bad_request
    assert_equal "url param is missing in the request", resp["message"]
  end

  test "returns upload policy and signature valid for 1 hour" do
    obj = Filestack::Security.new
    expected_policy = Base64.urlsafe_encode64({
      call: ["pick", "store", "read"],
      expiry: (Time.now + 1.hour).to_i
    }.to_json)
    expected_signature = OpenSSL::HMAC.hexdigest('sha256', Filestack::Security::APP_SECRET, expected_policy)

    get :upload_policy_and_signature
    resp = JSON.parse(response.body)

    assert_equal expected_policy, resp["policy"]
    assert_equal expected_signature, resp["signature"]
  end
end

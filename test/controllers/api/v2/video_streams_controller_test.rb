require 'test_helper'

class Api::V2::VideoStreamsControllerTest < ActionController::TestCase

  setup do
    IrisVideoStream.any_instance.stubs(:populate_meta_data).returns(true)

    @org = create(:organization)
    @user, @another_user = create_list(:user, 2, organization: @org)

    api_v2_sign_in @user
  end

  class VideoStreamUuidTest < Api::V2::VideoStreamsControllerTest
    test ':api should return #uuid' do
      uuid = VideoStream.new.assign_uuid
      VideoStream.any_instance.stubs(:assign_uuid).returns(uuid)

      get :uuid

      assert_response :success
      assert_equal uuid, get_json_response(response)['uuid']
    end
  end

  class VideoStreamStartTest < Api::V2::VideoStreamsControllerTest

    # start
    test ':api should be able to #start the live stream' do
      card = create(:card, card_type: 'video_stream', author_id: @user.id, video_stream: create(:iris_video_stream, :upcoming))
      @upcoming_vs = card.video_stream

      post :start, id: @upcoming_vs.id, format: :json
      assert_response :ok

      @upcoming_vs.reload
      assert @upcoming_vs.live?
    end

    test 'only author should be able to #start the live stream' do
      card = create(:card, card_type: 'video_stream', author_id: @user.id, video_stream: create(:iris_video_stream, :pending))
      @pending_vs = card.video_stream

      api_v2_sign_in @another_user

      post :start, id: @pending_vs.id, format: :json
      assert_response :unauthorized

      @pending_vs.reload
      assert @pending_vs.pending?
    end

    test 'only upcoming or pending stream should be able to #start' do
      card = create(:card, card_type: 'video_stream', author_id: @user.id, video_stream: create(:iris_video_stream, :pending))
      @pending_vs = card.video_stream

      card = create(:card, card_type: 'video_stream', author_id: @user.id, video_stream: create(:iris_video_stream, :upcoming))
      @upcoming_vs = card.video_stream

      card = create(:card, card_type: 'video_stream', author_id: @user.id, video_stream: create(:iris_video_stream, :past))
      @past_vs = card.video_stream

      # pending
      post :start, id: @pending_vs.id, format: :json
      assert_response :ok

      @pending_vs.reload
      assert @pending_vs.live?

      # upcoming
      post :start, id: @upcoming_vs.id, format: :json
      assert_response :ok

      @upcoming_vs.reload
      assert @upcoming_vs.live?

      # past
      assert_raise AASM::InvalidTransition do
        post :start, id: @past_vs.id, format: :json
      end

      @past_vs.reload
      refute @past_vs.live?
    end
  end

  class VideoStreamStopTest < Api::V2::VideoStreamsControllerTest

    # stop
    test 'should be able to #stop live streaming' do
      card = create(:card, card_type: 'video_stream', author_id: @user.id, video_stream: create(:iris_video_stream, :live, last_stream_at: Time.current))
      @live_vs = card.video_stream

      post :stop, id: @live_vs.id, format: :json
      assert_response :ok

      @live_vs.reload
      assert @live_vs.past?
    end

    test 'only author should be able to #stop live streaming' do
      card = create(:card, card_type: 'video_stream', author_id: @user.id, video_stream: create(:iris_video_stream, :live, last_stream_at: Time.current))
      @live_vs = card.video_stream

      api_v2_sign_in @another_user

      post :stop, id: @live_vs.id, format: :json
      assert_response :unauthorized

      @live_vs.reload
      assert @live_vs.live?
    end

    test 'only live video streaming video should be able to #stop' do
      card = create(:card, card_type: 'video_stream', author_id: @user.id, video_stream: create(:iris_video_stream, :pending))
      @pending_vs = card.video_stream

      post :stop, id: @pending_vs.id, format: :json
      assert_response :unprocessable_entity

      @pending_vs.reload
      refute @pending_vs.past?
      assert @pending_vs.pending?
    end
  end

  class VideoStreamJoinTest < Api::V2::VideoStreamsControllerTest
    # join
    test 'should be able to #join live streaming' do
      card = create(:card, card_type: 'video_stream', author_id: @user.id, video_stream: create(:iris_video_stream, :live, last_stream_at: Time.current))
      @live_vs = card.video_stream

      api_v2_sign_in @another_user

      assert_difference -> {@live_vs.watchers.count} do
        post :join, id: @live_vs.id, format: :json
        assert_response :ok

        assert_equal @another_user.id, @live_vs.watchers.last.try(:id)
      end
    end

    test 'should not be able to join video stream that is not in live state' do
      card = create(:card, card_type: 'video_stream', author_id: @user.id, video_stream: create(:iris_video_stream, :pending))
      @pending_vs = card.video_stream

      api_v2_sign_in @another_user

      assert_no_difference -> {@pending_vs.watchers.count} do
        post :join, id: @pending_vs.id, format: :json
        assert_response :unprocessable_entity
      end
    end
  end

  class VideoStreamLeaveTest < Api::V2::VideoStreamsControllerTest

    # leave
    test 'should be able to #leave live streaming' do
      card = create(:card, card_type: 'video_stream', author_id: @user.id, video_stream: create(:iris_video_stream, :live, last_stream_at: Time.current))
      @live_vs = card.video_stream

      @live_vs.watchers << @another_user

      api_v2_sign_in @another_user

      assert_difference -> {@live_vs.watchers.count}, -1 do
        post :leave, id: @live_vs.id, format: :json
        assert_response :ok
      end
    end

    test 'should not be able to leave video stream that is not in live state' do
      card = create(:card, card_type: 'video_stream', author_id: @user.id, video_stream: create(:iris_video_stream, :pending))
      @pending_vs = card.video_stream

      api_v2_sign_in @another_user

      assert_no_difference -> {@pending_vs.watchers.count} do
        post :leave, id: @pending_vs.id, format: :json
        assert_response :unprocessable_entity
      end
    end
  end

  class VideoStreamPingTest < Api::V2::VideoStreamsControllerTest
    # ping
    test 'should be able to ping live video stream' do
      card = create(:card, card_type: 'video_stream', author_id: @user.id, video_stream: create(:iris_video_stream, :live, last_stream_at: Time.current))
      @live_vs = card.video_stream

      prev_last_stream_at = @live_vs.last_stream_at

      Timecop.freeze(2.minutes.from_now) do
        post :ping, id: @live_vs.id, format: :json
      end

      @live_vs.reload

      assert_not_equal prev_last_stream_at, @live_vs.last_stream_at
    end

    test 'only live video stream should be able to ping' do
      card = create(:card, card_type: 'video_stream', author_id: @user.id, video_stream: create(:iris_video_stream, :pending))
      @pending_vs = card.video_stream

      prev_last_stream_at = @pending_vs.last_stream_at

      Timecop.freeze(2.minutes.from_now) do
        post :ping, id: @pending_vs.id, format: :json
        assert_response :unprocessable_entity
      end

      @pending_vs.reload
      assert_equal prev_last_stream_at, @pending_vs.last_stream_at
    end
  end

  class VideoStreamCreateRecordingTest < Api::V2::VideoStreamsControllerTest
    # create_recording
    test 'should create the recording' do
      card = create(:card, card_type: 'video_stream', author_id: @user.id, video_stream: create(:iris_video_stream, :past))
      @past_vs = card.video_stream

      assert_difference -> {Recording.count}, 1 do
        post :create_recording, id: @past_vs.id, sequence_number: 0,
             location: 'https://somewhere.com/somebucket/2323/vid.mpg4',
             bucket: 'somebucket',
             key: '/somebucket/2323/vid.mp4', checksum: '123'
      end

      recording = Recording.last
      assert_equal 0, recording.sequence_number
      assert_equal 'https://somewhere.com/somebucket/2323/vid.mpg4', recording.location
      assert_equal 'somebucket', recording.bucket
      assert_equal '123', recording.checksum
      assert_equal '/somebucket/2323/vid.mp4', recording.key
    end

    test 'should create a new client side recording' do
      card = create(:card, card_type: 'video_stream', author_id: @user.id, video_stream: create(:iris_video_stream, :past))
      @past_vs = card.video_stream

      assert_difference -> {Recording.count}, 1 do
        post :create_recording, id: @past_vs.id, sequence_number: 0,
             location: 'https://somewhere.com/somebucket/2323/vid.mpg4',
             bucket: 'somebucket',
             key: '/somebucket/2323/vid.mp4', checksum: '123'
      end

      recording = Recording.last
      assert_equal 0, recording.sequence_number
      assert_equal 'https://somewhere.com/somebucket/2323/vid.mpg4', recording.location
      assert_equal 'somebucket', recording.bucket
      assert_equal '123', recording.checksum
      assert_equal '/somebucket/2323/vid.mp4', recording.key
    end

    test 'should override the recording with the same sequence number' do
      card = create(:card, card_type: 'video_stream', author_id: @user.id, video_stream: create(:iris_video_stream, :past))
      @past_vs = card.video_stream

      existing_recording = create(:recording, video_stream: @past_vs)

      assert_no_difference -> {Recording.count}do
        post :create_recording, id: @past_vs.id, sequence_number: 0,
             location: 'https://somewhere.com/somebucket/2323/vid2.mpg4',
             bucket: 'somebucket2',
             key: '/somebucket/2323/vid2.mp4', checksum: '123'
      end

      recording = Recording.last
      assert_equal existing_recording, recording
      assert_equal 0, recording.sequence_number
      assert_equal 'https://somewhere.com/somebucket/2323/vid2.mpg4', recording.location
      assert_equal 'somebucket2', recording.bucket
      assert_equal '/somebucket/2323/vid2.mp4', recording.key
      assert_equal '123', recording.checksum
    end
  end

  class VideoStreamRecordingsTest < Api::V2::VideoStreamsControllerTest
    # recordings
    test 'should return an array of recording for wowza stream' do
      card = create(:card, card_type: 'video_stream', author_id: @user.id, video_stream: create(:iris_video_stream, :past))
      @past_vs = card.video_stream

      first_recording = create(:recording, video_stream: @past_vs, transcoding_status: 'available')
      second_recording = create(:recording, video_stream: @past_vs, sequence_number: 1, transcoding_status: 'available')
      get :recordings, id: @past_vs.id, format: :json

      resp = get_json_response(response)
      assert_equal 2, resp['recordings'].count
      assert_same_elements [first_recording, second_recording].map(&:id), resp['recordings'].map {|r| r['id']}
      assert_not_nil resp['recordings'][0]['hls_location']
      assert_not_nil resp['recordings'][0]['mp4_location']
    end

    test 'return secured video_stream recording' do
      card = create(:card, card_type: 'video_stream', organization: @org, author_id: @user.id, video_stream: create(:iris_video_stream, :past))
      past_vs = card.video_stream

      recording = create :recording, hls_location: 'video-streams-output-v2/client/13311/3c2cfba2-d1ee-4ea3-b9df-8631e903fd28/12356/0/hls_playlist.m3u8',
        mp4_location: 'video-streams-output-v2/client/13311/3c2cfba2-d1ee-4ea3-b9df-8631e903fd28/12356/0/mp4_output.mp4', video_stream: past_vs
      Settings.stubs(:serve_authenticated_images).returns('true')

      get :recordings, id: past_vs.id, format: :json

      resp = get_json_response(response)
      assert_equal 1, resp['recordings'].count
      assert_match /#{@org.host}\/past_stream\//, resp['recordings'][0]['hls_location']
      assert_match /#{@org.host}\/past_stream\//, resp['recordings'][0]['mp4_location']
    end
  end

  class VideoStreamViewersTest < Api::V2::VideoStreamsControllerTest
    # viewers
    test 'get viewers for a stream' do
      card = create(:card, card_type: 'video_stream', author_id: @user.id, video_stream: create(:iris_video_stream, :live, last_stream_at: Time.current))
      @live_vs = card.video_stream

      viewer1, viewer2 = create_list(:user, 2)
      @live_vs.watchers << [viewer1, viewer2]

      get :viewers, id: @live_vs.id, format: :json
      assert_response :ok

      resp = get_json_response response
      assert_equal 2, resp['total']
      assert_same_elements [viewer1, viewer2].map(&:id), resp['users'].map {|u| u['id']}
    end

    # viewers
    test 'should not return viewers for past video streams' do
      card = create(:card, card_type: 'video_stream', author_id: @user.id, video_stream: create(:iris_video_stream, :live, last_stream_at: Time.current))
      @live_vs = card.video_stream

      viewer1, viewer2 = create_list(:user, 2)
      @live_vs.watchers << [viewer1, viewer2]
      @live_vs.stop!

      get :viewers, id: @live_vs.id, format: :json
      assert_response :unauthorized
    end
  end

  class VideoStreamRetranscodeTest < Api::V2::VideoStreamsControllerTest
    # retransacode
    test 'presigned_post_url and retranscode simple ok' do
      card = create(:card, card_type: 'video_stream', author_id: @user.id, video_stream: create(:iris_video_stream, :past))
      @past_vs = card.video_stream

      recording = create(:recording, video_stream: @past_vs, transcoding_status: 'pending')

      post :retranscode, id: @past_vs.id
      assert_response :ok
    end

    test 'presigned_post_url and retranscode by random user should redirect' do
      card = create(:card, card_type: 'video_stream', author_id: @user.id, video_stream: create(:iris_video_stream, :past))
      @past_vs = card.video_stream

      user = create(:user, organization: @org)
      api_v2_sign_in user

      recording = create(:recording, video_stream: @past_vs, transcoding_status: 'pending')
      post :retranscode, id: @past_vs.id

      assert_response :unauthorized
    end
  end

  class VideoStreamPresignedPostUrlTest < Api::V2::VideoStreamsControllerTest
    # presigned post url
    test ':api should return presigned post url' do
      admin = create(:user, organization: @org, organization_role: "admin")
      video_stream = create(:wowza_video_stream, creator_id: admin.id, name: 'past stream', status: 'past', uuid: 'xx1')

      api_v2_sign_in admin

      get :presigned_post_url, id: video_stream.id
      assert_response :ok

      resp = get_json_response(response)

      assert_not_nil resp['presigned_post']
      assert_not_nil resp['recording_id']
      assert_not_nil resp['url']
    end

    test 'only creator can request presigned post url' do
      creator = create(:user, organization: @org)
      video_stream = create(:wowza_video_stream, creator_id: creator.id, name: 'past stream', status: 'past', uuid: 'xx1')

      get :presigned_post_url, id: video_stream.id
      assert_response :unauthorized
    end
  end

  class VideoStreamShowTest < Api::V2::VideoStreamsControllerTest
    test ':api should return video stream details' do
      creator = create(:user, organization: @org)
      video_stream = create(:iris_video_stream, creator: creator, status: 'past')
      recording =  create(:recording, video_stream: video_stream)

      get :show, id: video_stream.id, format: :json

      assert_response :ok
      resp = get_json_response(response)
      assert_equal video_stream.id, resp['id']

      resp.assert_valid_keys(['id', 'uuid', 'status', 'start_time', 'width', 'height',
        'image_url', 'playback_url', 'embed_playback_url', 'recording','watchers'])
    end

    test 'should not return video stream details #private channel' do
      creator = create(:user, organization: @org)
      private_channel = create(:channel, is_private: true, organization: @org)
      video_stream = create(:iris_video_stream, creator: creator, status: 'past')
      video_stream.channels << private_channel

      get :show, id: video_stream.id, format: :json
      assert_response :unauthorized
    end

    test 'should return video stream watchers count for past video_stream' do
      creator = create(:user, organization: @org)
      user_one = create(:user, organization: @org)
      user_two = create(:user, organization: @org)
      video_stream = create(:iris_video_stream, creator: creator, status: 'past')
      recording =  create(:recording, video_stream: video_stream)
      video_stream_user_one = VideoStreamsUser.create(user_id: user_one.id,video_stream_id: video_stream.id)
      video_stream_user_two = VideoStreamsUser.create(user_id: user_two.id,video_stream_id: video_stream.id)
      get :show, id: video_stream.id, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal video_stream.id, resp['id']
      assert_equal 2, resp['watchers']
    end

    test 'should not return video stream watchers count for live video_stream' do
      creator = create(:user, organization: @org)
      user_one = create(:user, organization: @org)
      video_stream = create(:iris_video_stream, creator: creator, status: 'live')
      recording =  create(:recording, video_stream: video_stream)
      video_stream_user_one = VideoStreamsUser.create(user_id: user_one.id,video_stream_id: video_stream.id)
      get :show, id: video_stream.id, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal video_stream.id, resp['id']
      assert_equal nil, resp['watchers']
    end
  end

  class VideoStreamDownloadMetricTest < Api::V2::VideoStreamsControllerTest
    test 'should push metric when video is downloaded' do
      actor = @user
      card = create :card, author: actor
      video_stream = create :wowza_video_stream, card: card
      api_v2_sign_in actor
      metadata = { foo: "bar" }
      stub_time = Time.now
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      Analytics::MetricsRecorderJob.expects(:perform_later).with(
        recorder: "Analytics::CardMetricsRecorder",
        org: Analytics::MetricsRecorder.org_attributes(@org),
        card: Analytics::MetricsRecorder.card_attributes(card),
        event: "card_video_stream_downloaded",
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        timestamp: stub_time.to_i,
        additional_data: metadata
      )
      Timecop.freeze stub_time do
        post :record_download_metric, id: video_stream.id, format: :json
      end
    end

    test 'should push metric when video is previewed' do
      actor = @user
      card = create :card, author: actor
      video_stream = create :wowza_video_stream, card: card
      api_v2_sign_in actor
      metadata = { foo: "bar" }
      stub_time = Time.now
      RequestStore.stubs(:read).with(:request_metadata).returns metadata
      Analytics::MetricsRecorderJob.expects(:perform_later).with(
        recorder: "Analytics::CardMetricsRecorder",
        org: Analytics::MetricsRecorder.org_attributes(@org),
        card: Analytics::MetricsRecorder.card_attributes(card),
        event: "card_video_stream_downloaded",
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        timestamp: stub_time.to_i,
        additional_data: metadata
      )
      Timecop.freeze stub_time do
        post :record_download_metric, id: video_stream.id, format: :json
      end
    end
  end

end

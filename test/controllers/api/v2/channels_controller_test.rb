require 'test_helper'

class Api::V2::ChannelsControllerTest < ActionController::TestCase
  class ChannelCreateTest < ActionController::TestCase
    setup do
      @user = create :user
      api_v2_sign_in @user
    end

    test ':api should not create channel for invalid params' do
      assert_no_difference -> { Channel.count } do
        post :create, { label: 'This is first channel in v2.0' }
      end
      assert_response :bad_request
    end

    test ':api should return unauthorized if user does not have permission' do
      Organization.any_instance.unstub(:create_default_roles)
      org = create(:organization, enable_role_based_authorization: true)
      member_role = org.roles.where(name: Role::TYPE_MEMBER)

      user = create(:user, organization: org)
      user.roles << member_role

      api_v2_sign_in user
      post :create, channel: { label: 'This is first channel' }, format: :json
      assert_response :unauthorized
    end

    test ':api should be created with curate only option' do
      assert_difference -> { Channel.count } do
        post :create, channel: { label: 'This is first channel in v2.0', curate_only: true }, format: :json
      end
      channel = Channel.last
      assert channel.curate_only
      assert_response :success
    end

    test ':api should create channel with label and add followers' do
      label = 'This is first channel in v2.0'
      followers = create_list :user, 2
      TeamsChannelsAutofollowJob.expects(:perform_later).once
      assert_difference -> {Channel.count}, 1 do
        post :create, channel: { label: label, follower_ids: followers.map(&:id) }, format: :json
      end
      assert_response :ok
      channel = Channel.last
      assert_equal label, channel.label
      assert_equal @user, channel.creator
    end

    test 'should not send follower invite mail for auto_follow channels' do
      TeamsChannelsAutofollowJob.expects(:perform_later).never
      post :create, channel: { label: 'Test Channel',description: 'Test Channel', auto_follow: true, curate_only: true }, format: :json
      assert_response :ok
    end

    test 'should send follower invite mail for non auto_follow channels' do
      followers = create_list :user, 1

      TeamsChannelsAutofollowJob.expects(:perform_later).once
      post :create, channel: { label: 'Test Channel',description: 'Test Channel', follower_ids: followers.map(&:id) }, format: :json
      assert_response :ok
    end

    test 'channel with image url should work' do
      image_url = "http://example.com/logo.jpg"
      stub_request(:get, /logo.jpg/).to_return( :status => 200,
        :body => File.read('test/fixtures/images/test.png'),
        :headers => { 'content-type':'image/png' })
      label = "this is a label #{image_url}"

      assert_difference ->{ Channel.count } do
        post :create, channel: { label: label, image: image_url, mobile_image: image_url }, format: :json
        assert_response :ok
      end
      channel = Channel.last
      assert channel.image?
      assert_equal channel.image_file_name, "logo.jpg"
      assert channel.mobile_image?
    end

    test 'channel should be created with ecl source id' do
      label = 'This is first channel in v2.0'
      ecl_source1_id = '123-abc'
      ecl_source2_id = '123-abcd'

      assert_differences [[ ->{Channel.count}, 1], [->{EclSource.count}, 2]] do
        post :create, channel: { label: label, ecl_sources_attributes: [{ecl_source_id: ecl_source1_id}, {ecl_source_id: ecl_source2_id}] }, format: :json
      end
      ch = Channel.last
      assert_equal label, ch.label
      assert_same_elements [ecl_source1_id, ecl_source2_id], ch.ecl_sources.map(&:ecl_source_id)
    end

    test ':api channel should create with provider and provider_image' do
      response = post(
        :create,
        channel: {
          label: "some label",
          provider: "some provider",
          provider_image: "some provider image"
        },
        format: :json
      )
      channel = get_json_response response
      assert_equal Integer, channel["id"].class
      assert_equal "some provider", channel["provider"]
      assert_equal "some provider image", channel["provider_image"]
    end

    test ':api create with mobile_image being sent from frontend' do
      mobile_image_url = "http://dp598loym07sk.cloudfront.net/assets/default_banner_image_min-9164e115397bff8e5b0fe8746a78941a.jpg"
      stub_request(:get, mobile_image_url).
        to_return(
          status: 200,
          body: File.read('test/fixtures/images/test.png'),
          headers: { 'content-type':'image/png' }
        )

      post :create, channel: {label: "Testing", mobile_image: mobile_image_url}, format: :json
      assert_response :success
    end

    test ':api - create with user uploaded image' do
      image_url = "https://cdn.filestackcontent.com/abcdefghi"
      stub_request(:get, /cdn.filestackcontent.com\/security=p:[a-zA-Z0-9=]*\,s:[a-zA-Z0-9=]*\/abcdefghi/).
        to_return(status: 200, body: File.read('test/fixtures/images/test.png'), headers: { 'content-type':'image/png' })

      post :create, channel: {label: "Testing", mobile_image: image_url}, format: :json
      assert_response :success
    end
  end

  class ChannelUpdateTest < ActionController::TestCase
    setup do
      @user = create(:user)
      @org = @user.organization
      api_v2_sign_in @user
    end

    test ':api should update channel' do
      label = 'This is updated channel in v2.0'
      channel = create :channel, organization: @org
      put :update, id: channel.id, channel: { label: label }, format: :json
      assert_response :ok
      assert_equal label, channel.reload.label
    end

    test ':api curator can update channels_card rank ' do
      label = 'This is updated channel in v2.0'
      channel = create :channel, organization: @org, curators: [@user]
      card = create :card, organization: @org, author: @user, card_type: 'project', card_subtype: 'text'
      channel_card = create :channels_card, channel: channel, card: card

      put :reorder_card, id: channel.id, card_id: card.id, rank: 1, format: :json
      assert_response :ok
      assert_equal 1, channel_card.reload.rank
    end

    test 'creator can update channels_card rank' do
      channel = create :channel, organization: @org, creator: @user
      card = create :card, organization: @org, author: @user, card_type: 'project', card_subtype: 'text'
      channel_card = create :channels_card, channel: channel, card: card

      put :reorder_card, id: channel.id, card_id: card.id, rank: 2, format: :json
      assert_response :ok
      assert_equal 2, channel_card.reload.rank
    end

    test ':follower cannot update rank ' do
      label = 'This is updated channel in v2.0'
      channel = create :channel, organization: @org
      card = create :card, organization: @org, author: @user, card_type: 'project', card_subtype: 'text'
      channel_card = create :channels_card, channel: channel, card: card
      channel.followers = [@user]
      rank = 1
      put :reorder_card, id: channel.id, card_id: card.id, rank: 1, format: :json
      assert_response :unauthorized
    end

    test 'should save carousels information' do
      admin_user = create(:user, organization_role: 'admin', organization: @org)
      channel = create :channel, organization: @org
      carousels = [{default_label: "default label", index: 2, visible: false, custom_carousel: true, id: 1}]

      api_v2_sign_in(admin_user)

      put :carousels, id: channel.id, carousels: carousels, format: :json
      assert_response :ok
      carousels = get_json_response response
      assert_equal "default label", carousels['carousels'][0]['default_label']
      assert_equal 2, carousels["carousels"][0]['index']
      assert_equal false, carousels["carousels"][0]['visible']
      assert_equal true, carousels["carousels"][0]['custom_carousel']
      assert_equal 1, carousels["carousels"][0]['id']
    end

    test "should update provider and provider_image" do
      channel = create :channel, organization: @org
      assert channel.persisted?
      assert_equal channel.provider, nil
      assert_equal channel.provider_image, nil
      response = put(
        :update,
        id: channel.id,
        channel: {
          provider: "foo",
          provider_image: "bar"
        },
        format: :json
      )
      updated_channel = get_json_response response
      assert_equal updated_channel["id"], channel.id
      assert_equal "foo", updated_channel["provider"]
      assert_equal "bar", updated_channel["provider_image"]
    end

    test 'should add followers to channel' do
      followers = create_list :user, 2, organization: @org
      channel = create :channel, organization: @org
      TeamsChannelsAutofollowJob.expects(:perform_later).with(team_ids: [], followers_ids: followers.map(&:id), channel_id: channel.id, sender_id: @user.id).once

      put :update, id: channel.id, channel: { follower_ids: followers.map(&:id), followers: { group_ids: nil } }, format: :json
      assert_response :ok
    end

    test 'should add followers from groups(teams) to channel' do
      users = create_list :user, 3, organization: @org
      team = create :team, organization: @org
      users.each { |u| team.add_member u }

      channel = create :channel, organization: @org
      TeamsChannelsAutofollowJob.expects(:perform_later).with(team_ids: [team.id], followers_ids: [], channel_id: channel.id, sender_id: @user.id).once
      put :update, id: channel.id, channel: {followers: {group_ids: [team.id]} }, format: :json
      assert_includes channel.teams_channels_follows.map(&:team_id), team.id
      assert_response :ok
    end

    test 'should not process follow job for existing team' do
      team = create :team, organization: @org
      channel = create :channel, organization: @org

      teams_channel_follow = create :teams_channels_follow, team: team, channel: channel

      TeamsChannelsAutofollowJob.expects(:perform_later).with(team_ids: [team.id], followers_ids: [], channel_id: channel.id, sender_id: @user.id).never
      put :update, id: channel.id, channel: {followers: {group_ids: [team.id]} }, format: :json
      assert_response :ok
    end

    test 'should add followers and followers from groups(teams) to channel without duplication' do
      users = create_list :user, 3, organization: @org
      team = create :team, organization: @org
      users.each { |u| team.add_member u }
      channel = create :channel, organization: @org

      TeamsChannelsAutofollowJob.expects(:perform_later).with(team_ids: [team.id], followers_ids: [users.first.id], channel_id: channel.id, sender_id: @user.id).once
      put :update, id: channel.id, channel: { follower_ids: [users.first.id], followers: {group_ids: [team.id]} }, format: :json
      assert_response :ok
    end

    test 'should not add followers or followers_via_teams when they already exists' do
      team_users = create_list :user, 3, organization: @org
      users = create_list :user, 2, organization: @org
      channel = create :channel, organization: @org
      channel.add_followers users
      team = create :team, organization: @org
      channel.followed_teams << team
      team_users.each { |u| team.add_member u}
      #should treat team users as followers
      put :update, id: channel.id, channel: {follower_ids: users.map{|u| u.id}, followers: {group_ids: [team.id]}}, format: :json
      assert_response :ok
      assert_equal channel.all_followers.count, (team_users + users).count
    end

    test 'should allow update of language and content-type' do
      channel = create :channel, organization: @org
      response = put :update, id: channel.id, channel: {language: ['en'], content_type: ['article']}, format: :json
      updated_channel = get_json_response response
      channel.reload
      assert_response :ok
      assert_same_elements ['en'], channel.language
      assert_same_elements ['article'], channel.content_type
      #Verify json response has language and content_type fields
      assert_not_empty updated_channel["language"]
      assert_not_empty updated_channel["content_type"]

      #Set language and content-type to nil
      response = put :update, id: channel.id, channel: {language: [], content_type: []}, format: :json
      updated_channel = get_json_response response
      channel.reload
      assert_response :ok
      assert_empty channel.language
      assert_empty channel.content_type
    end

    test ':api should be able to add more or remove ecl sources' do
      ecl_source2_id = '123-abc'
      channel = create(:channel, organization: @org)
      ecl_source1 = create(:ecl_source, channel: channel)
      channel.ecl_sources << ecl_source1

      post :update, id: channel.id, channel: {ecl_sources_attributes: [{id: ecl_source1.id, _destroy: true}, {ecl_source_id: ecl_source2_id}] }, format: :json
      channel.reload

      assert_equal 1, channel.ecl_sources.count
      assert_same_elements [ecl_source2_id], channel.ecl_sources.map(&:ecl_source_id)
    end

    test ':api should be able to update the ecl_enabled flag for channels' do
      ecl_source2_id = '123-abc'
      channel = create(:channel, organization: @org)
      refute channel.ecl_enabled

      put :update, id: channel.id, channel: { ecl_enabled: true }, format: :json
      channel.reload
      assert channel.ecl_enabled
    end

    test ':api should be able to update the auto_pin_cards flag for channels' do
      channel = create(:channel, organization: @org)
      refute channel.auto_pin_cards

      response = put :update, id: channel.id, channel: { auto_pin_cards: true }, format: :json
      updated_channel = get_json_response response
      channel.reload
      assert channel.auto_pin_cards
      assert updated_channel["auto_pin_cards"]
    end

    test ':api should update with default mobile_image url' do
      channel = create(:channel, organization: @org)
      mobile_image_url = "http://dp598loym07sk.cloudfront.net/assets/default_banner_image_min-9164e115397bff8e5b0fe8746a78941a.jpg"
      stub_request(:get, mobile_image_url).
        to_return(
          status: 200,
          body: File.read('test/fixtures/images/test.png'),
          headers: { 'content-type':'image/png' }
        )

      put :update, id: channel.id, channel: {label: "Testing", mobile_image: mobile_image_url}, format: :json
      assert_response :success
    end

    test ':api - update with user uploaded image' do
      channel = create(:channel, organization: @org)
      image_url = "https://cdn.filestackcontent.com/abcdefghi"
      stub_request(:get, /cdn.filestackcontent.com\/security=p:[a-zA-Z0-9=]*\,s:[a-zA-Z0-9=]*\/abcdefghi/).
        to_return(status: 200, body: File.read('test/fixtures/images/test.png'), headers: { 'content-type':'image/png' })

      put :update, id: channel.id, channel: {label: "Testing", mobile_image: image_url}, format: :json
      assert_response :success
    end
  end

  class ChannelCuratorTest < ActionController::TestCase
    setup do
      @user = create :user
      @org = @user.organization
      Role.create_standard_roles(@org)

      api_v2_sign_in @user
    end

    test 'should send curator invite mail for users added as curators to channel' do
      users = create_list :user, 2, organization: @org
      Channel.any_instance.expects(:send_curator_invites).with(tos: users, from: @user).once
      channel = create :channel, curate_only: true, organization: @org
      put :add_curators, id: channel.id, user_ids: users.map(&:id)
      assert_response :ok
    end

    test 'if org has a curator role it should make the user a curator when added has channel curator' do
      role = @org.roles.where(name: Role::TYPE_CURATOR).first_or_create
      users = create_list :user, 2, organization: @org
      Channel.any_instance.expects(:send_curator_invites).with(tos: users, from: @user).once
      channel = create :channel, curate_only: true, organization: @org

      put :add_curators, id: channel.id, user_ids: users.map(&:id)

      assert_response :ok
      assert_includes users.first.roles.collect(&:name), Role::TYPE_CURATOR
      assert_includes users.last.roles.collect(&:name), Role::TYPE_CURATOR
    end

    test ':api should add curators' do
      channel = create :channel, organization: @org
      new_user = create :user, organization: @org
      channel.add_curators [@user]

      Channel.any_instance.expects(:send_curator_invites).once
      assert_difference -> { channel.curators.count }  do
        put :add_curators, id: channel.id, user_ids: [new_user.id, @user.id]
      end
      assert_equal 2, channel.curators.count
    end
    test ':api should remove curators' do
      channel = create :channel, organization: @org
      new_user = create :user, organization: @org
      channel.add_curators [@user]

      # remove only valid collaborators of channel
      assert_difference -> { channel.curators.count }, -1 do
        put :remove_curators, id: channel.id, user_ids: [new_user.id, @user.id]
      end
      assert_equal 0, channel.curators.count
    end

    test ':api fetch curators' do
      curators = []
      channel = create(:channel, organization: @org)
      users = create_list(:user, 5, organization: @org)
      last_curator = create(:user, organization: @org)
      Timecop.freeze 1.hours.ago do
        curators = create_list(:user, 3, organization: @org)
        channel.add_curators curators
      end
      channel.add_curators [last_curator]

      get :curators, id: channel, format: :json
      json_response = get_json_response(response)
      assert_equal 4, json_response['curators'].length
      assert_equal 4, json_response['total']
      assert_equal last_curator.id, json_response['curators'][0]['id']

      #limit test
      get :curators, id: channel, limit: 2, format: :json
      json_response = get_json_response(response)
      assert_equal 2, json_response['curators'].length

      # search curator by name
      get :curators, id: channel, search_term: last_curator.first_name, format: :json
      json_response = get_json_response(response)
      assert_equal 1, json_response['curators'].length
      assert_equal last_curator.id, json_response['curators'].first['id'].to_i
    end

    test ':api fetch users who are not curators' do
      channel = create(:channel, organization: @org)
      curators = create_list(:user, 3, organization: @org)
      channel.add_curators curators

      @user1 = create(:user, organization: @org, email: 'fname@edcast.com', first_name: 'fname', last_name: 'lname', handle: @fname)
      @user2 = create(:user, organization: @org, email: 'fname123@edcast.com', first_name: 'fname', last_name: 'lname', handle: @lname)
      @user3 = create(:user, organization: @org, email: 'lname+x123@edcast.com', first_name: 'lname', last_name: 'fname', handle: @lnamefname)
      users = [@user1, @user2, @user3]

      Search::UserSearch.any_instance.expects(:search)
        .with(q: 'fname@edcast.com', viewer: @user, offset: 0, limit: 10, allow_blank_q: true, filter: {exclude_ids: curators.map(&:id)}, load: true)
        .returns(Search::UserSearchResponse.new(results: users, total: 3)).once

      get :not_curators, id: channel.id, search_term: 'fname@edcast.com', format: :json
      json_response = get_json_response(response)
      assert_equal 3, json_response['not_curators'].length
      assert_equal 3, json_response['total']
    end
  end

  class ChannelCollaboratorTest < ActionController::TestCase
    setup do
      @user = create :user
      @org = @user.organization
      Role.create_standard_roles(@org)
    end

    test ':api should remove collaborators only if current user is author or admin' do
      channel = create(:channel, organization: @org)
      collaborator = create(:user, organization: @org)
      channel.add_authors [collaborator]
      admin_user = create(:user, organization_role: 'admin', organization: @org)
      api_v2_sign_in(admin_user)
      put :remove_collaborators, id: channel.id, user_ids: [collaborator.id]
      assert !channel.is_author?(collaborator)
    end

    test 'should not remove collaborators if current user is neither author nor admin' do
      channel = create(:channel, organization: @org)
      new_user = create(:user, organization: @org)
      channel.add_authors [new_user]
      unauthorized_user = create(:user, organization: @org)
      api_v2_sign_in(unauthorized_user)
      put :remove_collaborators, id: channel.id, user_ids: [new_user.id]
      assert_equal 1, channel.authors.count
      assert_response :unauthorized
    end

    test ':api should add collaborators by user ids' do
      channel = create :channel, organization: @org
      admin_user = create(:user, organization_role: 'admin', organization: @org)
      api_v2_sign_in(admin_user)

      users = create_list(:user, 2, organization: @org)

      put :add_collaborators, id: channel.id, user_ids: users.map(&:id)
      assert_response :ok
      assert_equal 2, channel.authors.size
      assert_equal ['collaborator'], users.map { |author| author.roles.map(&:name) }.flatten.uniq
    end

    test ':api should add collaborators by role' do
      channel = create :channel, organization: @org
      role = @org.roles.where(name: Role::TYPE_CURATOR).first_or_create
      admin_user = create(:user, organization_role: 'admin', organization: @org)
      api_v2_sign_in(admin_user)

      users = create_list(:user, 2, organization: @org)
      users.each { |user| user.add_role(Role::TYPE_CURATOR) }

      ChannelCollaboratorsJob.expects(:perform_later).once

      put :add_collaborators, id: channel.id, role_ids: [role.id]
      assert_response :ok
    end

    test ':api should add collaborators by role and user ids' do
      channel = create :channel, organization: @org
      role = @org.roles.where(name: Role::TYPE_CURATOR).first_or_create
      admin_user = create(:user, organization_role: 'admin', organization: @org)
      api_v2_sign_in(admin_user)

      users = create_list(:user, 3, organization: @org)
      users.each { |user| user.add_role(Role::TYPE_CURATOR) }

      ChannelCollaboratorsJob.expects(:perform_later).once

      put :add_collaborators, id: channel.id, user_ids: users.map(&:id), role_ids: [role.id]
      assert_response :ok
    end

    test ':api should remove collaborators' do
      channel = create :channel, organization: @org
      new_user = create :user, organization: @org
      channel.add_authors [@user]
      admin_user = create(:user, organization_role: 'admin', organization: @org)
      api_v2_sign_in(admin_user)

      # remove only valid collaborators of channel
      assert_difference -> { channel.authors.count }, -1 do
        put :remove_collaborators, id: channel.id, user_ids: [new_user.id, @user.id]
      end
      assert_equal 0, channel.authors.count
    end

    test ':api fetch not collaborators' do
      channel = create(:channel, organization: @org)
      users = create_list(:user, 2, organization: @org)
      channel.add_authors([@user])
      api_v2_sign_in(@user)

      Search::UserSearch.any_instance.expects(:search)
        .with(q: '', viewer: @user, offset: 0, limit: 10, allow_blank_q: true, filter: { exclude_ids: [@user.id] }, load: true)
        .returns(Search::UserSearchResponse.new(results: users, total: 2)).once

      get :not_contributors, id: channel.id, search_term: '', format: :json
      json_response = get_json_response(response)
      assert_response :ok
      assert_equal 2, json_response['not_authors'].length
      assert_equal 2, json_response['total']
    end

    test 'fetch users by matching the email for not_contributors' do
      channel = create(:channel, organization: @org)
      user = create(:user, organization: channel.organization, email: 'trent@plimpton.com')
      channel.add_authors([@user])
      api_v2_sign_in(@user)

      Search::UserSearch.any_instance.expects(:search)
        .with(q: 'plimpton', viewer: @user, offset: 0, limit: 10, allow_blank_q: true, filter: { exclude_ids: [@user.id] }, load: true)
        .returns(Search::UserSearchResponse.new(results: [user], total: 1)).once

      get :not_contributors, id: channel.id, search_term: 'plimpton', format: :json
      resp = get_json_response(response)
      assert_includes resp['not_authors'].map{|d| d['id'].to_i}, user.id
    end

    test 'fetch users without email for not_contributors' do
      channel = create(:channel, organization: @org)
      user = create(:user, organization: channel.organization, email: nil, first_name: 'trent', last_name: 'plimpton')
      channel.add_authors([@user])
      api_v2_sign_in(@user)

      Search::UserSearch.any_instance.expects(:search)
        .with(q: 'plimpton', viewer: @user, offset: 0, limit: 10, allow_blank_q: true, filter: { exclude_ids: [@user.id] }, load: true)
        .returns(Search::UserSearchResponse.new(results: [user], total: 1)).once

      get :not_contributors, id: channel.id, search_term: 'plimpton', format: :json
      resp = get_json_response(response)
      assert_includes resp['not_authors'].map{|d| d['id'].to_i}, user.id
    end
  end

  class ChannelContributorsTest < ActionController::TestCase
    test ':api fetch authors' do
      org = create(:organization)
      user = create(:user, organization: org)
      set_api_request_header(user)
      set_host(org)
      channel = create(:channel, organization: org)
      users = create_list(:user, 5, organization: org)
      last_author = create(:user, organization: org)
      Timecop.freeze 1.hours.ago do
        authors = create_list(:user, 3, organization: org)
        channel.add_authors authors
      end
      channel.add_authors [last_author]

      get :contributors, id: channel, format: :json
      json_response = get_json_response(response)
      assert_equal 4, json_response['authors'].length
      assert_equal 4, json_response['total']
      assert_equal last_author.id, json_response['authors'][0]['id']

      #limit test
      get :contributors, id: channel, limit: 2, format: :json
      json_response = get_json_response(response)
      assert_equal 2, json_response['authors'].length

      # search contributors by name
      get :contributors, id: channel, search_term: last_author.first_name, format: :json
      json_response = get_json_response(response)
      assert_equal 1, json_response['authors'].length
    end
  end

  class ChannelFollowersTest < ActionController::TestCase
    test ':api fetch followers' do
      org = create(:organization)
      user = create(:user, organization: org)
      set_api_request_header(user)
      set_host(org)
      channel = create(:channel, organization: org)
      authors = create_list(:user, 4, organization: org)
      last_follower = create(:user, organization: org)
      Timecop.freeze 1.hour.ago do
        followers = create_list(:user, 4, organization: org)
        channel.add_followers followers
      end
      channel.add_followers [last_follower]
      get :followers, id: channel, format: :json
      json_response = get_json_response(response)
      assert_equal 5, json_response['followers'].length
      assert_equal 5, json_response['total']
      assert_equal last_follower.id, json_response['followers'][0]['id']

      #limit test
      get :followers, id: channel, limit: 2, format: :json
      json_response = get_json_response(response)
      assert_equal 2, json_response['followers'].length

      # search contributors by name
      get :followers, id: channel, search_term: last_follower.first_name, format: :json
      json_response = get_json_response(response)
      assert_equal 1, json_response['followers'].length
    end

    test 'api get followers by type' do
      org = create(:organization)
      users = create_list(:user, 4, organization: org)
      api_v2_sign_in(users.first)
      channel = create(:channel, organization: org)
      channel.add_followers users
      get :followers, id: channel, type: 'individual', format: :json
      json_response = get_json_response(response)
      assert_equal 4, json_response['followers'].length
    end

    test 'admin should be able to remove followers from channel' do
      admin = create(:user, organization_role: 'admin')
      org = admin.organization

      followers = create_list :user, 3, organization: org
      channel = create :channel, organization: org
      channel.add_followers followers

      api_v2_sign_in(admin)
      user_ids = followers.map(&:id)

      put :remove_followers, id: channel.id, user_ids: [followers.first.id, followers.last.id]

      assert_response :ok
      assert_equal 1, channel.all_followers.count
      assert_not_includes channel.all_followers, followers.first
      assert_not_includes channel.all_followers, followers.last
    end

    test 'member should not be able to remove followers from channel' do
      member = create(:user)
      org = member.organization

      fake_id = 001
      channel = create :channel, organization: org

      api_v2_sign_in(member)

      put :remove_followers, id: channel.id, user_ids: [fake_id]

      assert_response :unauthorized
    end

    test ':api fetch uniq followers ' do
      org = create(:organization)
      user = create(:user, organization: org)
      set_api_request_header(user)
      set_host(org)
      channel = create(:channel, organization: org)
      author = create(:user, organization: org)
      author2 = create(:user, organization: org)

      # Doing below changes for EP-17666
      follow1 = Follow.new(user_id: author.id, followable: channel)
      follow1.save(vaild: false)
      follow2 = Follow.new(user_id: author2.id, followable: channel)
      follow2.save(vaild: false)
      follow3 = Follow.new(user_id: author.id, followable: channel)
      follow3.save(vaild: false)

      get :followers, id: channel, type: 'individual', format: :json
      json_response = get_json_response(response)

      assert_equal 2, json_response['followers'].length
      assert_equal 2, json_response['total']
    end
  end

  class ChannelTopicTest < ActionController::TestCase
    setup do
      @user = create :user
      @org = @user.organization

      api_v2_sign_in @user
    end

    test ':api list channel topics' do
      channel = create(:channel, organization: @org, topics: [{id: 'topic1'}, {id: 'topic2'}])

      get :list_topics, id: channel.id, format: :json
      json_response = get_json_response(response)
      assert_equal 2, json_response['topics'].length
      assert_equal 'topic1', json_response['topics'][0]['id']
      assert_equal 'topic2', json_response['topics'][1]['id']
    end

    test 'add topic blank topics' do
      channel = create(:channel, organization: @org)

      put :add_topic, id: channel.id, topic: {id: 'topic2'}, format: :json
      json_response = get_json_response(response)
      assert_equal 'topic2', json_response['id']
    end

    test ':api add channel topic' do
      channel = create(:channel, organization: @org, topics: [{id: 'topic1'}])

      put :add_topic, id: channel.id, topic: {id: 'topic2'}, format: :json
      json_response = get_json_response(response)
      assert_equal 'topic2', json_response['id']
    end

    test ':api add channel topic list' do
      channel = create(:channel, organization: @org, topics: [{id: 'topic1'}])

      put :add_topic_list, id: channel.id, topic: [{id: 'topic2'}, {id: 'topic3'}]
      json_response = get_json_response(response)
      assert_equal [{'id'=>'topic1'}, {'id'=>'topic2'}, {'id'=>'topic3'}], json_response
    end

    test ':api remove channel topic' do
      channel = create(:channel, organization: @org, topics: [{id: 'topic1'}])

      delete :remove_topic, id: channel.id, topic: {id: 'topic1'}, format: :json

      channel.reload
      assert_equal [], channel.topics
    end

    test ':api remove channel topic list' do
      channel = create(:channel, organization: @org, topics: [{id: 'topic1'}, {id: 'topic2'}])

      delete :remove_topic_list, id: channel.id, topic: {topic_ids: ['topic1', 'topic2']}, format: :json

      channel.reload
      assert_equal [], channel.topics
    end
  end

  class ChannelEclTest < ActionController::TestCase
    setup do
      @user = create :user
      @org = @user.organization

      api_v2_sign_in @user
    end

    test ':api list ecl sources' do
      user = create(:user, first_name: 'Admin', last_name: 'User', organization: @org)
      team = create(:team, name: 'test', organization: @org)

      stub_request(:get, Settings.ecl.api_endpoint + '/api/v1/sources?limit=1000&is_default=false&is_enabled=true').
        to_return(status: 200, body: {data: [{id: '123', display_name: '123',restricted_teams: [team.id],restricted_users: [user.id] }]}.to_json, headers: {})
      stub_request(:get, Settings.ecl.api_endpoint + '/api/v1/sources?limit=1000&is_default=true&is_enabled=true').
        to_return(status: 200, body: {data: [{id: '123', display_name: '123',restricted_teams: [team.id], restricted_users: [user.id] }]}.to_json, headers: {})

      channel = create(:channel, organization: @org)
      ecl_source1 = create(:ecl_source, channel: channel, ecl_source_id: '123')
      ecl_source2 = create(:ecl_source, channel: channel, ecl_source_id: '456')

      get :list_ecl_sources, id: channel.id, format: :json

      json_response = get_json_response(response)
      assert_equal 2, json_response['ecl_sources'].length
      assert_equal '123', json_response['ecl_sources'][0]['display_name']
      assert_equal [{"id"=> user.id, "first_name"=> user.first_name, "last_name"=> user.last_name}], json_response['ecl_sources'][0]['restricted_users']
      assert_equal [{"id"=> team.id, "name"=> team.name}], json_response['ecl_sources'][0]['restricted_teams']
    end

    test ':api add ecl source' do
      stub_request(:get, Settings.ecl.api_endpoint + '/api/v1/sources?limit=1000&is_default=false&is_enabled=true&source_id=123').
        to_return(status: 200, body: {data: [{id: '123', display_name: '123'}]}.to_json, headers: {})
      stub_request(:get, Settings.ecl.api_endpoint + '/api/v1/sources?limit=1000&is_default=true&is_enabled=true&source_id=123').
        to_return(status: 200, body: {data: [{id: '123', display_name: '123'}]}.to_json, headers: {})
      channel = create(:channel, organization: @org)

      put :add_ecl_source, id: channel.id, ecl_source: {ecl_source_id: '123'}, format: :json
      json_response = get_json_response(response)

      assert_equal 1, EclSource.count
      assert_equal '123', json_response['display_name']
    end

    test 'api to program a channel' do
      opts = {channel: {
              language: ["en", "gu"],
              content_type: ["media", "poll"],
              topics:[{id:"4999080280199627265",
                      name:"deloitte.industries.life_sciences_and_health_care.health_care.health_care_providers.other_providers",
                      label:"Other Providers",
                      domain:{id:"4999080275303973909",
                              label:"industries",
                              name:"deloitte.industries"}
                    }],
              ecl_sources:[
                {source_id:"9c07ce8b-cf28-4d22-b653-ddafc4c6c7e0",display_name:"Forbes RSS Mansi",private_content: false},
                {source_id:"32a0b385-b479-417e-9a1a-42f881fed69b",display_name:"Shraddha AI690",private_content: true}
              ]}
            }
      channel = create(:channel, organization: @org)
      Channel.any_instance.expects(:program)
      put :programming, id: channel.id, channel: opts[:channel], format: :json
      assert_response :success
    end

    test ':api add ecl source and make private' do
      stub_request(:get, Settings.ecl.api_endpoint + '/api/v1/sources?limit=1000&is_default=false&is_enabled=true&source_id=123').
        to_return(status: 200, body: {data: [{id: '123', display_name: '123'}]}.to_json, headers: {})
      stub_request(:get, Settings.ecl.api_endpoint + '/api/v1/sources?limit=1000&is_default=true&is_enabled=true&source_id=123').
        to_return(status: 200, body: {data: [{id: '123', display_name: '123'}]}.to_json, headers: {})
      channel = create(:channel, organization: @org)

      put :add_ecl_source, id: channel.id, ecl_source: {ecl_source_id: '123', private_content: true}, format: :json
      json_response = get_json_response(response)

      assert_equal 1, EclSource.count
      assert_equal '123', json_response['display_name']
      assert EclSource.where(ecl_source_id: '123').first.private_content
    end

    test ':api add ecl source list' do
      path_not_default = '/api/v1/sources?is_default=false&is_enabled=true&limit=1000&source_id='
      path_default = '/api/v1/sources?is_default=true&is_enabled=true&limit=1000&source_id='
      stub_request(:get, Settings.ecl.api_endpoint + path_not_default + '123').
        to_return(status: 200, body: {data: [{id: '123', display_name: '123'}]}.to_json, headers: {})
      stub_request(:get, Settings.ecl.api_endpoint + path_default + '123').
        to_return(status: 200, body: {data: [{id: '123', display_name: '123'}]}.to_json, headers: {})
      stub_request(:get, Settings.ecl.api_endpoint + path_not_default + '321').
        to_return(status: 200, body: {data: [{id: '321', display_name: '321'}]}.to_json, headers: {})
      stub_request(:get, Settings.ecl.api_endpoint + path_default+ '321').
        to_return(status: 200, body: {data: [{id: '321', display_name: '321'}]}.to_json, headers: {})

      channel = create(:channel, organization: @org)
      put :add_ecl_source_list, id: channel.id, ecl_source: {ecl_source_ids: ['123', '321']}, format: :json
      json_response = get_json_response(response)
      assert_equal 2, EclSource.count
      assert_equal '123', json_response['ecl_sources'][0]['display_name']
    end

    test ':api delete ecl source' do
      stub_request(:get, Settings.ecl.api_endpoint + '/api/v1/sources?limit=1000&is_default=false&is_enabled=true&source_id=123').
        to_return(status: 200, body: {data: [{id: '123', display_name: '123'}]}.to_json, headers: {})

      channel = create(:channel, organization: @org)
      ecl_source1 = create(:ecl_source, channel: channel, ecl_source_id: '123')
      ecl_source2 = create(:ecl_source, channel: channel, ecl_source_id: '456')

      delete :remove_ecl_source, id: channel.id, ecl_source: {ecl_source_id: '123'}, format: :json

      assert_equal 1, EclSource.count
      assert_equal '456', EclSource.first.ecl_source_id
    end

    test ':api delete ecl source list' do
      addition_path = '/api/v1/sources?limit=1000&is_default=false&is_enabled=true&source_id='
      stub_request(:get, Settings.ecl.api_endpoint + addition_path + '123').
        to_return(status: 200, body: {data: [{id: '123', display_name: '123'}]}.to_json, headers: {})
      stub_request(:get, Settings.ecl.api_endpoint + addition_path + '456').
        to_return(status: 200, body: {data: [{id: '456', display_name: '456'}]}.to_json, headers: {})

      channel = create(:channel, organization: @org)
      ecl_source1 = create(:ecl_source, channel: channel, ecl_source_id: '123')
      ecl_source2 = create(:ecl_source, channel: channel, ecl_source_id: '456')
      delete :remove_ecl_source_list, id: channel.id, ecl_source: {ecl_source_ids: ['123', '456']}, format: :json
      assert_equal 0, EclSource.count
    end

    test ':api failed delete ecl source' do
      addition_path = '/api/v1/sources?limit=1000&is_default=false&is_enabled=true&source_id='
      stub_request(:get, Settings.ecl.api_endpoint + addition_path + '123').
        to_return(status: 200, body: {data: [{id: '123', display_name: '123'}]}.to_json, headers: {})
      stub_request(:get, Settings.ecl.api_endpoint + addition_path + '456').
        to_return(status: 200, body: {data: [{id: '456', display_name: '456'}]}.to_json, headers: {})

      channel = create(:channel, organization: @org)
      ecl_source1 = create(:ecl_source, channel: channel, ecl_source_id: '123')
      ecl_source2 = create(:ecl_source, channel: channel, ecl_source_id: '456')
      delete :remove_ecl_source_list, id: channel.id, ecl_source: {ecl_source_ids: ['888', '999']}, format: :json

      json_response = get_json_response(response)
      assert_equal 2, EclSource.count
    end
  end

  class ChannelShowTest < ActionController::TestCase
    setup do
      @user = create(:user)
      @org = @user.organization
      api_v2_sign_in @user
    end
    #TODO ALL THESE SHOW NEED TO BE REFACTORED
    # show
    test ':api should add channel following information' do
      channel, channel1 = create_list(:channel, 2, organization: @org)
      channel.add_followers [@user]
      channel.reload

      # channel
      get :show, id: channel.id, format: :json
      assert get_json_response(response)['is_following']

      # channel1
      get :show, id: channel1.id, format: :json
      refute get_json_response(response)['is_following']
    end

    test 'expect card_fetch_limit in json response' do
      channel = create(:channel, organization: @org, card_fetch_limit: 20)
      get :show, id: channel.id, format: :json
      res = get_json_response(response)
      assert 20, res['card_fetch_limit']
    end

    test ':api should return default custom_carousels if channels config is not present' do
      channel = create(:channel, organization: @org)
      default_carousels = [{"custom_carousel"=>false, "default_label"=>"SmartCards", "index"=>0, "visible"=>true},
        {"custom_carousel"=>false, "default_label"=>"Pathways", "index"=>1, "visible"=>true},
        {"custom_carousel"=>false, "default_label"=>"Journeys", "index"=>2, "visible"=>true},
        {"custom_carousel"=>false, "default_label"=>"Streams", "index"=>3, "visible"=>true},
        {"custom_carousel"=>false, "default_label"=>"Courses", "index"=>4, "visible"=>true}]
      # channel
      get :show, id: channel.id, format: :json
      assert get_json_response(response)['carousels']
      assert_equal default_carousels, get_json_response(response)['carousels']
    end

    test ':api should return 401 for private channel' do
      channel = create(:channel, organization: @org)
      channel1 = create(:channel, is_private: true, organization: @org)
      channel.reload

      # channel
      get :show, id: channel.id, format: :json
      assert_response :success

      # channel1
      get :show, id: channel1.id, format: :json
      assert_response :unauthorized

      channel1.add_followers [@user]
      get :show, id: channel1.id, format: :json
      assert_response :success
    end

    test ':api should allow org admins and super admins to view and edit private channel as well' do
      admin_user2 = create(:user, organization_role: 'admin', organization: @org)


      # admin user who created the private channel
      api_v2_sign_in(admin_user2)
      channel1 = create(:channel, is_private: true, organization: @org, user_id: admin_user2)

      get :show, id: channel1.id, format: :json
      assert_response :success
      sign_out admin_user2
    end

    test 'should include team information that follow channel' do
      channel = create(:channel, organization: @org)
      teams = create_list(:team, 2, organization: @org).each do |team|
        create(:teams_channels_follow, team: team, channel: channel)
      end
      get :show, id: channel.id, format: :json

      resp = get_json_response response

      assert_equal channel.id, resp['id']
      assert_equal teams.size, resp['teams'].size
      assert_same_elements teams.map(&:id), resp['teams'].map {|team| team['id']}
    end

    # test ':api a non author non admin will not be able to access' do
    #   admin_user1 = create(:user, organization_role: 'admin', organization: @org)
    #   user = create(:user, organization: @org)
    #   channel1 = create(:channel, is_private: true, organization: @org, user_id: admin_user1)
    #   # non author and non admin user should not be able to update
    #   api_v2_sign_in(user)
    #   label = 'This is updated channel in v3.0'
    #   put :update, id: channel1.id, channel: { label: label }, format: :json
    #   assert_response :unauthorized
    # end

    # test ':api a non author admin will be able to access' do
    #   admin_user2 = create(:user, organization_role: 'admin', organization: @org)
    #   admin_user1 = create(:user, organization_role: 'admin', organization: @org)
    #   channel1 = create(:channel, is_private: true, organization: @org, user_id: admin_user2)
    #   # admin user who is not the author
    #   api_v2_sign_in(admin_user1)

    #   label = 'This is updated channel in v2.0'
    #   put :update, id: channel1.id, channel: { label: label }, format: :json

    #   assert_response :success
    # end

    test ':api should return extended channel details' do
      curator1, curator2 = create_list(:user, 2, organization: @org)

      channel = create(:channel,
        organization: @org,
        user_id: @user.id,
        provider: "foo",
        provider_image: "bar"
      )
      channel.curators << [curator1, curator2]

      smarbite1, smarbite2 = create_list(:card, 2, organization: @org, author_id: @user.id)

      video_streams = create_list(:video_stream_card, 5, author_id: @user.id, organization: @org)
      #should not include deleted cards
      archived_streams = create(:video_stream_card, state: "archived", author_id: @user.id, organization: @org)

      pathway1, pathway2 = create_list(:card, 2, organization: @org, card_type: 'pack', author_id: @user.id)

      journey1, journey2 = create_list(:card, 2, organization: @org, card_type: 'journey', card_subtype: 'self_paced', author_id: @user.id)

      journey_pack1, journey_pack2 = create_list(:card, 2, organization: @org, card_type: 'pack', card_subtype: 'simple', author_id: @user.id)
      [journey_pack1, journey_pack2].each do |cover|
        CardPackRelation.add(cover_id: cover.id, add_id: create(:card, author: @user).id)
      end

      JourneyPackRelation.add(cover_id: journey1.id, add_id: journey_pack1.id)
      journey1.publish!

      unpublished_pathway = create(:card, organization: @org, card_type: 'pack', author_id: @user.id, state: 'draft')
      [pathway1, pathway2].each do |cover|
        CardPackRelation.add(cover_id: cover.id, add_id: create(:card, author: @user).id)
        cover.publish!
      end

      channel.cards << [smarbite1, smarbite2, video_streams.flatten, pathway1, pathway2, journey1, journey2, unpublished_pathway]
      get :show, id: channel.id, format: :json
      resp = get_json_response(response)
      assert_equal 2, resp['smartbites_count']
      assert_equal 2, resp['published_pathways_count']
      assert_equal 1, resp['published_journeys_count']
      assert_equal 5, resp['video_streams_count']
      assert_equal "foo", resp["provider"]
      assert_equal "bar", resp["provider_image"]
      assert resp['curators']
      assert resp['creator']
    end

    test 'should return owner flag, followers count' do
      channel1 = create(:channel, organization: @org)
      channel2 = create(:channel, organization: @org, creator: @user)
      channel1.add_followers [@user]
      channel1.reload

      # channel1
      get :show, id: channel1.id, format: :json
      assert_equal 1, get_json_response(response)['followers_count']
      refute get_json_response(response)['is_owner']

      # channel2
      get :show, id: channel2.id, format: :json
      assert_equal 0, get_json_response(response)['followers_count']
      assert get_json_response(response)['is_owner']
    end

    test 'should return courses count' do
      channel = create(:channel, organization_id: @org.id)
      course_cards = create_list(:card, 3, organization_id: @org.id, author: nil, card_type: "course")
      other_cards = create_list(:card, 2, organization_id: @org.id, author: nil)
      course_cards.each {|c| c.channels << channel}
      other_cards.each {|c| c.channels << channel}

      get :show, id: channel.id, format: :json
      resp = get_json_response(response)
      assert_equal 3, resp['courses_count']
    end

    test 'should return all_authors_types count' do
      channel = create(:channel, organization_id: @org.id)
      author = create(:user, organization_id: @org.id)
      trusted_author = create(:user, organization_id: @org.id)
      channel.add_authors([author])
      channel.add_authors([trusted_author], true)

      get :show, id: channel.id, format: :json
      resp = get_json_response(response)
      assert_equal 2, resp['authors_count']
    end
  end

  class ChannelIndexTest < ActionController::TestCase
    setup do
      Organization.any_instance.unstub(:create_general_channel)
      @user = create(:user)
      @org = @user.organization
      api_v2_sign_in @user
    end

    # index
    test ':api should return all eligible channels' do
      channel1 = create(:channel, :robotics, organization: @org)
      channel2 = create(:channel, :architecture, organization: @org)

      create(:follow, followable: channel1, user: @user)

      get :index, format: :json
      resp = get_json_response(response)

      assert_equal 3, resp['total'] #includes general channel
      assert resp['channels'].select{|channel| channel['id'] == channel1.id}[0]['is_following']
      refute resp['channels'].select{|channel| channel['id'] == channel2.id}[0]['is_following']
    end

    test 'should be able to search channel by term' do
      channel1 = create(:channel, :robotics, organization: @org)
      channel2 = create(:channel, :architecture, organization: @org)

      get :index, q: "robot", format: :json
      resp = get_json_response(response)

      assert_equal 1, resp['total']
      refute resp['channels'].select{|channel| channel['id'] == channel1.id}[0]['is_following']
    end

    test 'should be able to order by promoted' do
      channel1 = create(:channel, :robotics, organization: @org, is_promoted: true)
      channel2 = create(:channel, :architecture, organization: @org)

      get :index, format: :json
      resp = get_json_response(response)

      assert_equal 3, resp['total']
      assert_equal channel1.id, resp['channels'].first['id']
    end

    test 'should be able to sort by channel creators name' do
      user_1 = create(:user, organization: @org, first_name: 'abc')
      channel_1 = create(:channel, organization: @org, user: user_1)
      user_2 = create(:user, organization: @org, first_name: 'pqr')
      channel_2 = create(:channel, organization: @org, user: user_2)

      get :index, sort: 'creator', order_label: 'desc', format: :json
      resp = get_json_response(response)
      assert_equal 3, resp['total']
      assert_equal [channel_2.id, channel_1.id, @org.general_channel.id], resp['channels'].map { |channel| channel['id'].to_i }
    end

    test 'should be able to filter by is following' do
      channels = create_list(:channel, 2, organization: @org)
      channels.first.authors << @user

      get :index, format: :json
      resp = get_json_response(response)

      assert_equal 3, resp['total']

      channel_resp = resp['channels'].select{|r| r['id'] == channels.first.id}[0]
      assert_not_empty channel_resp['banner_image_urls']
      image_urls = channel_resp['banner_image_urls']

      assert_not_nil image_urls['small_url']
      assert_not_nil image_urls['medium_url']
    end

    test 'should be able to filter by is curator' do
      channels = create_list(:channel, 2, organization: @org)
      ChannelsCurator.create!(channel: channels.first, user: @user)

      get :index, is_curator: 't', format: :json
      resp = get_json_response(response)

      assert_equal 1, resp['total']
      assert_same_elements [channels.first].map(&:id), resp['channels'].map{|r| r['id']}

      get :index, is_curator: 'f', format: :json
      resp = get_json_response(response)

      assert_equal 2, resp['total'] # includes general channel
      assert_same_elements [channels.last, @org.general_channel].map(&:id), resp['channels'].map{|r| r['id']}
    end

    test 'should be able to filter by is_trusted' do
      channel = create(:channel, organization: @org)
      channel.add_authors([@user], true)

      get :index, is_trusted: 't', format: :json
      resp = get_json_response(response)

      assert_equal 1, resp['total']
      assert_equal [channel.id], resp['channels'].map { |ch| ch['id'] }
    end

    test 'is curator when not curating anything' do
      channels = create_list(:channel, 2, organization: @org)

      get :index, is_curator: 't', format: :json
      resp = get_json_response(response)

      assert_equal 0, resp['total']
      assert_equal [], resp['channels']

      get :index, is_curator: 'f', format: :json
      resp = get_json_response(response)

      assert_equal 3, resp['total'] # includes general channel
      assert_same_elements (channels << @org.general_channel).map(&:id), resp['channels'].map{|r| r['id']}
    end

    test 'should be able to filter by is author' do
      channels = create_list(:channel, 2, organization: @org)
      channels.first.authors << @user

      get :index, is_author: 't', format: :json
      resp = get_json_response(response)

      assert_equal 1, resp['total']
      assert_same_elements [channels.first].map(&:id), resp['channels'].map{|r| r['id']}

      get :index, is_author: 'f', format: :json
      resp = get_json_response(response)

      assert_equal 2, resp['total'] # includes general channel
      assert_same_elements [channels.last, @org.general_channel].map(&:id), resp['channels'].map{|r| r['id']}
    end

    test 'should return banner image urls' do
      channels = create_list(:channel, 2, organization: @org)

      get :index, format: :json
      resp = get_json_response(response)

      assert_equal 3, resp['total']
      channel_resp = resp['channels'].select{|r| r['id'] == channels.first.id}[0]

      assert_not_empty channel_resp['profile_image_urls']
      image_urls = channel_resp['profile_image_urls']

      assert_not_nil image_urls['small_url']
      assert_not_nil image_urls['medium_url']
    end

    test 'should return profile image urls' do
      User.any_instance.unstub(:add_to_org_general_channel)
      @user.send :add_to_org_general_channel

      channels = create_list(:channel, 2, organization: @org)
      create(:follow, followable: channels.first, user: @user)

      get :index, is_following: 't', format: :json
      resp = get_json_response(response)

      assert_equal 2, resp['total'] #includes general channel
      assert_same_elements [@org.general_channel, channels.first].map(&:id), resp['channels'].map{|r| r['id']}

      get :index, is_following: 'f', format: :json
      resp = get_json_response(response)

      assert_equal 1, resp['total']
      assert_same_elements [channels.last].map(&:id), resp['channels'].map{|r| r['id']}
    end

    test 'check for open channel response' do
      channel1 = create(:channel, :robotics, organization: @org, is_open: true)
      channel2 = create(:channel, :architecture, organization: @org)

      create(:follow, followable: channel1, user: @user)

      get :index, is_cms: true, format: :json
      resp = get_json_response(response)

      assert_includes resp['channels'].map{|i| i['id']}, channel1.id
      open_channel = resp['channels'].select{|c| c['id'] == channel1.id}.first
      closed_channel = resp['channels'].select{|c| c['id'] == channel2.id}.first

      assert_empty open_channel['curators']
      assert open_channel['is_open']
      refute closed_channel['is_open']
    end

    test 'should return created_at, author_name and authors_count' do
      get :index, is_cms: true, format: :json
      resp = get_json_response(response)
      keys = resp['channels'][0].keys
      assert_includes keys, 'created_at'
      assert_includes keys, 'author_name'
      assert_includes keys, 'authors_count'
    end

    test 'should return error if params[:sort] is not a channel params' do
      get :index, sort: 'created_at', format: :json
      assert_response :ok

      get :index, sort: 'CreatedAt', format: :json
      assert_response :unprocessable_entity
      resp = get_json_response(response)
      assert_equal 'CreatedAt is not a valid value for channel', resp['message']
    end

    test 'respect `is_cms` to return all channels to CMS admin' do
      admin_user = create(:user, organization: @org, organization_role: 'admin')
      channel1 = create(:channel, :robotics, organization: @org, created_at: Time.now - 2.days)
      channel2 = create(:channel, :architecture, organization: @org, created_at: Time.now - 1.days)
      channel3 = create(:channel, :animals, organization: @org, is_private: true, created_at: Time.now)
      api_v2_sign_in(admin_user)

      get :index, is_cms: 'true', sort: 'created_at', order_label: 'desc', format: :json
      resp = get_json_response(response)

      assert_equal 4, resp['total'] #includes general channel
      assert_same_elements [@org.general_channel.id, channel3.id, channel2.id, channel1.id], resp['channels'].collect { |c| c['id'] }
    end

    test 'able to search channel in CMS admin with sort and order_label params' do
      admin_user = create(:user, organization: @org, organization_role: 'admin')
      channel1 = create(:channel, :robotics, organization: @org, created_at: Time.now - 2.days)
      channel2 = create(:channel, :architecture, organization: @org, created_at: Time.now - 1.days)
      channel3 = create(:channel, :animals, organization: @org, is_private: true, created_at: Time.now)
      api_v2_sign_in(admin_user)

      get :index, is_cms: 'true', sort: 'created_at', order_label: 'desc', q: 'architecture', format: :json
      resp = get_json_response(response)

      assert_equal 1, resp['total']
    end

    test ':api org admin should be able to search channel by term from admin console' do
      channel1 = create(:channel, :robotics, organization: @org)
      channel2 = create(:channel, :architecture, organization: @org)
      admin_user = create(:user, organization: @org, organization_role: 'admin')
      admin_user.add_role(Role::TYPE_ADMIN)
      api_v2_sign_in(admin_user)

      get :index, q: "robot", format: :json
      resp = get_json_response(response)

      assert_equal 1, resp['total']
    end

    test 'should return correct count channel followers' do
      user = create(:user, organization: @org)
      user_suspend = create(:user, organization: @org, is_suspended: true)
      user_fail = create(:user, organization: @org, is_suspended: true, status: 'active')
      channel = create(:channel, organization: @org)
      channel.add_followers([user, user_suspend, user_fail], true)

      get :index, is_cms: true,  format: :json
      assert_response :ok
      resp = get_json_response(response)

      assert_equal 1, resp['channels'].last['followers_count']
    end
  end

  class ChannelFollowUnfollowTest < ActionController::TestCase
    # follow
    test ':api current user should follow channel' do
      org = create(:organization)
      follower = create(:user, organization: org)
      following_channel = create(:channel, organization: org)

      api_v2_sign_in follower

      assert_difference -> {Follow.count} do
        post :follow, id: following_channel.id, format: :json
        assert_response :ok
      end

      follow = Follow.last
      assert_equal follower.id, follow.user_id
      assert_equal following_channel, follow.followable
    end

    # unfollow
    test ':api current user should be able to unfollow following channel' do
      org = create(:organization)
      follower = create(:user, organization: org)
      following_channel = create(:channel, organization: org)
      follow = create(:follow, user_id: follower.id, followable: following_channel)

      api_v2_sign_in follower

      assert_difference -> {Follow.count}, -1 do
        post :unfollow, id: following_channel.id, format: :json
        assert_response :ok
      end

      refute Follow.exists?(follow.id)
    end

    # users following a channel via team(group) should not be able to unfollow
    # read comments on EP-7027 for further clarification
    test ':api followers via team should not be able to unfollow' do
      org = create(:organization)
      team_follower = create(:user, organization: org)
      team = create(:team, organization: org)
      channel = create(:channel, organization: org)
      channel.followed_teams << team
      team.add_member(team_follower)

      api_v2_sign_in team_follower

      assert_no_difference -> {channel.all_followers.count} do
        post :unfollow, id: channel.id, format: :json
        assert_response 422
      end
    end
  end

  class ChannelsAsCuratorTest < ActionController::TestCase
    test ':api should get list of channels that a user is a curator for and if curate_only flag is true' do
      @user = create(:user)
      @org = @user.organization
      api_v2_sign_in @user

      channels = create_list(:channel, 4, organization: @org, curate_only: true)
      curator_for_channels = channels.last(3)

      curator_for_channels.each { |channel| channel.add_curators([@user]) }

      non_curatable_channel = create(:channel, organization: @org, curate_only: false)
      non_curatable_channel.add_curators([@user])

      get :as_curator, limit: 5 , format: :json
      json_response = get_json_response(response)

      assert_equal 3, json_response['total']
      assert_same_elements curator_for_channels.map(&:id), json_response['channels'].map { |ch| ch['id'].to_i }
    end
  end

  class ChannelPromoteTest < ActionController::TestCase
    setup do
      @admin = create(:user, organization_role: 'admin')
      @org = @admin.organization
      api_v2_sign_in @admin
    end
    # promote
    test ':api admin should be able to promote channel' do
      channels = create_list(:channel, 2, organization: @org, is_promoted: false)
      last_channel = channels.last

      put :promote, id: last_channel.id, format: :json
      last_channel.reload

      assert_response :ok
      assert last_channel.is_promoted
    end

    test ':api admin should be able to promote batch of channels' do
      channels = create_list(:channel, 2, organization: @org, is_promoted: false)
      last_channel = channels.last
      first_channel = channels.first

      post :batch_promote, channel_ids: channels.map(&:id), format: :json
      channels.each(&:reload)

      assert_response :ok
      assert last_channel.is_promoted
      assert first_channel.is_promoted
    end

    test ':api batch_promote should return error for empty array' do
      post :batch_promote, channel_ids: [], format: :json
      assert_response :unprocessable_entity
    end

    test 'only admin should be able to promote channel' do
      channel_creator = create(:user, organization: @org)
      channels = create_list(:channel, 2, organization: @org, is_promoted: false)
      channels.each {|ch| ch.add_authors [channel_creator]}
      last_channel = channels.last

      api_v2_sign_in channel_creator

      put :promote, id: last_channel.id, format: :json
      last_channel.reload

      assert_response :unauthorized
      refute last_channel.is_promoted
    end
  end

  class ChannelDemoteTest < ActionController::TestCase
    setup do
      @admin = create(:user, organization_role: 'admin')
      @org = @admin.organization
      api_v2_sign_in @admin
    end

    # demote
    test ':api admin should be able to demote channel' do
      channels = create_list(:channel, 2, organization: @org, is_promoted: true)
      last_channel = channels.last

      put :demote, id: last_channel.id, format: :json
      last_channel.reload

      assert_response :ok
      refute last_channel.is_promoted
    end

    test ':api admin should be able to demote batch of channels' do
      channels = create_list(:channel, 2, organization: @org, is_promoted: true)
      last_channel = channels.last
      first_channel = channels.first

      post :batch_demote, channel_ids: channels.map(&:id), format: :json
      channels.each(&:reload)

      assert_response :ok
      refute last_channel.is_promoted
      refute first_channel.is_promoted
    end

    test ':api batch_demote should return error for empty array' do
      post :batch_demote, channel_ids: [], format: :json
      assert_response :unprocessable_entity
    end

    test 'only admin should be able to demote channel' do
      channel_creator = create(:user, organization: @org)
      channels = create_list(:channel, 2, organization: @org, is_promoted: true)
      last_channel = channels.last

      api_v2_sign_in channel_creator

      put :demote, id: last_channel.id, format: :json
      last_channel.reload

      assert_response :unauthorized
      assert last_channel.is_promoted
    end
  end

  class ChannelDestroyTest < ActionController::TestCase
    # destroy
    test ':api should delete a channel' do
      TestAfterCommit.enabled = false
      org = create(:organization)
      channel, another_channel = create_list(:channel, 2, organization: org)
      user, admin_user = create_list(:user, 2, organization: org)
      admin_user.add_role(Role::TYPE_ADMIN)
      admin_user.update(organization_role: 'admin')

      create(:follow, followable: channel, user: user)

      card = create(:card, author: user)
      card.channels << channel

      vs = create(:iris_video_stream, creator: user)
      vs.card.channels << [channel, another_channel]

      #There is no video_stream for channel directly
      Card.any_instance.expects(:reindex_async).returns(nil).twice
      api_v2_sign_in admin_user

      assert_differences [[->{Channel.count}, -1], [->{Follow.count}, -1], [->{ChannelsCard.count}, -2]] do
        delete :destroy, id: channel.id, format: :json
      end

      assert_equal [another_channel], vs.card.reload.channels
      assert_equal [], card.reload.channels
      assert_response :ok
    end

    test 'only admin can delete channel' do
      org = create(:organization)
      channel, another_channel = create_list(:channel, 2, organization: org)
      user, admin_user = create_list(:user, 2, organization: org)
      admin_user.update(organization_role: 'admin')

      api_v2_sign_in user
      delete :destroy, id: channel.id, format: :json

      assert_response :unauthorized
      assert Channel.exists?(channel.id)
    end
  end

  class ChannelSuggestionsTest < ActionController::TestCase
    test ':api should fetch public channels for org member' do
      org = create(:organization)
      public_channel = create_list(:channel, 2, organization: org)
      user = create(:user, organization: org)

      Search::ChannelSearch.any_instance.expects(:autosuggest_for_cms).
          with('test', org.id, {filter: {}}).
          returns(Search::ChannelSearchResponse.new(
                    results: public_channel,
                    total: 2,
        )).once

      api_v2_sign_in user
      get :suggestions, q: 'test', format: :json

      assert_response :ok
      json_response = get_json_response(response)
      assert_equal 2, json_response["results"].count
    end

    test ':api should fetch all channels for org admin' do
      org = create(:organization)
      public_channel = create_list(:channel, 2, organization: org)
      private_channel = create_list(:channel, 3, organization: org, is_private: true)
      admin_user = create(:user, organization: org, organization_role: 'admin')

      Search::ChannelSearch.any_instance.expects(:autosuggest_for_cms).
          with("test", org.id, {filter: {include_private: true}}).
          returns(Search::ChannelSearchResponse.new(
                    results: public_channel + private_channel,
                    total: 5,
        )).once

      api_v2_sign_in admin_user

      get :suggestions, q: "test", format: :json

      assert_response :ok
      json_response = get_json_response(response)
      assert_equal 5, json_response["results"].count
    end
  end

  class ChannelSearchTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)

      @channel1 = create(:channel, label: 'test 1', organization: @org)
      @channel1.add_followers [@user]
      @channel2 = create(:channel, label: 'test 2', organization: @org)
      @channel2.add_followers [@user]
      @channel3 = create(:channel, label: 'technical', organization: @org)
      @private_channel = create(:channel, label: 'test 3', is_private: true, organization: @org)

      @query = 'test'
    end

    test ':api should search all channels for org member' do
      Search::ChannelSearch.any_instance.expects(:search)
        .with(q: @query, viewer: @user, filter: {include_private: true, skip_aggs: true}, offset: 0, limit: 10, sort: [{key: '_score', order: :desc}], load: true)
        .returns(Search::ChannelSearchResponse.new(results: [@channel1, @channel2], total: 2)).once

      api_v2_sign_in @user
      get :search, q: 'test', format: :json

      assert_response :ok
      json_response = get_json_response(response)

      assert_equal 2, json_response["channels"].count
      assert_equal [@channel1.id, @channel2.id], json_response["channels"].map{|c| c['id']}
    end

    test ':api should search only private channels for org member' do
      Search::ChannelSearch.any_instance.expects(:search)
        .with(q: @query, viewer: @user, filter: {include_private: true, is_private: true, skip_aggs: true}, offset: 0, limit: 10, sort: [{key: '_score', order: :desc}], load: true)
        .returns(Search::ChannelSearchResponse.new(results: [@private_channel], total: 1)).once

      api_v2_sign_in @user
      get :search, q: 'test', is_private: true, format: :json

      assert_response :ok
      json_response = get_json_response(response)

      assert_equal 1, json_response["channels"].count
      assert_equal @private_channel.id, json_response["channels"].first['id']
    end

    test ':api should search only public channels for org member' do
      Search::ChannelSearch.any_instance.expects(:search)
        .with(q: 'te', viewer: @user, filter: {include_private: true, is_private: 'false', skip_aggs: true}, offset: 0, limit: 10, sort: [{key: '_score', order: :desc}], load: true)
        .returns(Search::ChannelSearchResponse.new(results: [@channel1, @channel2, @channel3], total: 3)).once

      api_v2_sign_in @user
      get :search, q: 'te', is_private: 'false', format: :json

      assert_response :ok
      json_response = get_json_response(response)

      assert_equal 3, json_response["channels"].count
      assert_equal [@channel1, @channel2, @channel3].map(&:id), json_response["channels"].map{|c| c['id']}
    end

    test ':api should search only channels a user is following.' do
      Search::ChannelSearch.any_instance.expects(:search)
        .with(q: nil, viewer: @user, filter: {include_private: true, only_my_channels: true, skip_aggs: true}, offset: 0, limit: 10, sort: [{key: '_score', order: :desc}], load: true)
        .returns(Search::ChannelSearchResponse.new(results: [@channel1, @channel2], total: 2)).once

      api_v2_sign_in @user
      get :search, only_my_channels: true, format: :json

      assert_response :ok
      json_response = get_json_response(response)

      assert_equal 2, json_response["channels"].count
      assert_equal [@channel1, @channel2].map(&:id), json_response["channels"].map{|c| c['id']}
    end
  end

  class ChannelRemoveCardsTest < ActionController::TestCase
    setup do
      ChannelsCard.any_instance.stubs :create_users_management_card
      @author = create(:user, organization_role: 'admin')
      @org = @author.organization
      @channel = create(:channel, organization: @org)
      @smartbite1, @smarbite2 = create_list(:card, 2, organization: @org, author_id: @author.id)
      @channel.cards << [@smartbite1, @smarbite2]
    end

    test '#creator can remove card from channel' do
      creator = create(:user, organization: @org)
      @channel.creator = creator
      @channel.save
      api_v2_sign_in creator

      delete :remove_cards, id: @channel.id, card_ids: [@smartbite1.id]
      assert_response :ok
      assert_equal @channel.cards.count, 1
    end

    test '#curator can remove card from channel' do
      curator = create(:user, organization: @org)
      @channel.add_curators [curator]

      api_v2_sign_in curator

      delete :remove_cards, id: @channel.id, card_ids: [@smartbite1.id]

      assert_response :ok
      assert_equal @channel.cards.count, 1
    end

    test '#followers can remove cards from open channel' do
      @channel.update(is_open: true)

      follower = create(:user, organization: @org)
      @channel.add_followers [follower]

      api_v2_sign_in follower

      delete :remove_cards, id: @channel.id, card_ids: [@smartbite1, @smarbite2].map(&:id)

      assert_response :ok
      assert_empty @channel.reload.cards
    end

    test '#followers cannot remove cards from channel' do
      curator = create(:user, organization: @org)
      api_v2_sign_in curator

      assert_no_difference ->{@channel.cards.count} do
        delete :remove_cards, id: @channel.id, card_ids: [@smartbite1.id]
        assert_response :unauthorized
      end
    end

    test '#should not removes card from channel if the card is not posted in channel' do
      curator = create(:user, organization: @org)
      smarbite3 = create(:card, author: curator)
      @channel.add_curators [curator]

      api_v2_sign_in curator

      assert_no_difference ->{@channel.cards.count} do
        delete :remove_cards, id: @channel.id, card_ids: [smarbite3.id]
        assert_response  :not_found
      end
    end

    # TODO: MOVE THIS TEST TO MODEL.
    test '#destroy remove cards from pinned list' do
      curator = create(:user, organization: @org)
      smarbite3 = create(:card, organization: @org, author_id: @author.id)
      pinned_card_1 = create :pin, pinnable: @channel, object: @smartbite1, user: @author
      pinned_card_2 = create :pin, pinnable: @channel, object: @smarbite2, user: @author


      @channel.add_curators [curator]
      @channel.cards << smarbite3

      api_v2_sign_in curator

      TestAfterCommit.enabled = true

      assert_equal 2, @channel.pins.count

      delete :remove_cards, id: @channel.id, card_ids: [@smartbite1.id]
      assert_response  :ok

      @channel.reload
      assert_equal 1, @channel.pins.count
      assert_equal [@smarbite2], @channel.pins.map(&:object)
    end
  end

  class ChannelTrustedCollaboratorTest < ActionController::TestCase
    setup do
      @user = create :user
      @org = @user.organization
      Role.create_standard_roles(@org)
    end

    test ':api should remove trusted collaborators only if current user is author or admin' do
      channel = create(:channel, organization: @org)
      collaborator = create(:user, organization: @org)
      trusted = true
      channel.add_authors([collaborator], trusted)
      admin_user = create(:user, organization_role: 'admin', organization: @org)
      api_v2_sign_in(admin_user)
      put :remove_collaborators, id: channel.id, user_ids: [collaborator.id], trusted: true
      refute channel.is_trusted_author?(collaborator)
    end

    test 'should not remove trusted collaborators if current user is neither author nor admin' do
      channel = create(:channel, organization: @org)
      new_user = create(:user, organization: @org)
      trusted = true
      channel.add_authors([new_user], trusted)
      unauthorized_user = create(:user, organization: @org)
      api_v2_sign_in(unauthorized_user)
      put :remove_collaborators, id: channel.id, user_ids: [new_user.id], trusted: true
      assert_equal 1, channel.trusted_authors.count
      assert_response :unauthorized
    end

    test ':api should add trusted collaborators by user ids' do
      channel = create :channel, organization: @org
      admin_user = create(:user, organization_role: 'admin', organization: @org)
      api_v2_sign_in(admin_user)

      users = create_list(:user, 2, organization: @org)

      put :add_collaborators, id: channel.id, user_ids: users.map(&:id), trusted: true
      assert_response :ok
      assert_equal 2, channel.trusted_channels_authors.size
      assert_equal ['trusted_author'], channel.trusted_channels_authors.pluck(:as_type).uniq
      assert_equal ['trusted_collaborator'], users.map { |author| author.roles.map(&:name) }.flatten.uniq
    end

    test ':api should add trusted collaborators by role' do
      channel = create :channel, organization: @org
      role = @org.roles.where(name: Role::TYPE_CURATOR).first_or_create
      admin_user = create(:user, organization_role: 'admin', organization: @org)
      api_v2_sign_in(admin_user)

      users = create_list(:user, 2, organization: @org)
      users.each { |user| user.add_role(Role::TYPE_CURATOR) }

      ChannelCollaboratorsJob.expects(:perform_later).once

      put :add_collaborators, id: channel.id, role_ids: [role.id], trusted: true
      assert_response :ok
    end

    test ':api should add trusted collaborators also by role and user ids' do
      channel = create :channel, organization: @org
      role = @org.roles.where(name: Role::TYPE_CURATOR).first_or_create
      admin_user = create(:user, organization_role: 'admin', organization: @org)
      api_v2_sign_in(admin_user)

      users = create_list(:user, 3, organization: @org)
      users.each { |user| user.add_role(Role::TYPE_CURATOR) }

      ChannelCollaboratorsJob.expects(:perform_later).once

      put :add_collaborators, id: channel.id, user_ids: users.map(&:id), role_ids: [role.id], trusted: true
      assert_response :ok
    end

    test '#not_contributors should fetch users which could be added as trusted collaborators' do
      channel = create(:channel, organization: @org)
      trusted_authors = create_list(:user, 2, organization: @org)
      authors = create_list(:user, 3, organization: @org)
      channel.add_authors(authors)
      channel.add_authors(trusted_authors, true)
      api_v2_sign_in(@user)

      Search::UserSearch.any_instance.expects(:search)
        .with(q: '', viewer: @user, offset: 0, limit: 10, allow_blank_q: true, filter: { exclude_ids: trusted_authors.map(&:id) }, load: true)
        .returns(Search::UserSearchResponse.new(results: [@user] + authors, total: 4)).once

      get :not_contributors, id: channel.id, trusted: true, search_term: '', format: :json
      json_response = get_json_response(response)
      assert_equal 4, json_response['not_authors'].length
      assert_equal 4, json_response['total']
      user_ids = json_response['not_authors'].map { |usr| usr['id'] }
      refute_includes trusted_authors.map(&:id), user_ids
    end

    test '#not_contributors should fetch users matching email which could be added as trusted collaborators' do
      channel = create(:channel, organization: @org)
      trusted_authors = create_list(:user, 2, organization: @org)
      author = create(:user, organization: @org, email: 'testemail@abc.com')
      channel.add_authors([author])
      channel.add_authors(trusted_authors, true)
      api_v2_sign_in(@user)

      Search::UserSearch.any_instance.expects(:search)
        .with(q: 'testemail', viewer: @user, offset: 0, limit: 10, allow_blank_q: true, filter: { exclude_ids: trusted_authors.map(&:id) }, load: true)
        .returns(Search::UserSearchResponse.new(results: [@user, author], total: 2)).once

      get :not_contributors, id: channel.id, trusted: true, search_term: 'testemail', format: :json
      json_response = get_json_response(response)
      assert_equal 2, json_response['not_authors'].length
      assert_equal 2, json_response['total']
      user_ids = json_response['not_authors'].map { |usr| usr['id'] }
      refute_includes trusted_authors.map(&:id), user_ids
    end

    test '#not_contributors should fetch users matching first_name which could be added as trusted collaborators' do
      channel = create(:channel, organization: @org)
      trusted_authors = create_list(:user, 2, organization: @org)
      author = create(:user, organization: @org, email: 'testemail@abc.com', first_name: 'dummy', last_name: 'last_name')
      channel.add_authors([author])
      channel.add_authors(trusted_authors, true)
      api_v2_sign_in(@user)

      Search::UserSearch.any_instance.expects(:search)
        .with(q: 'dummy', viewer: @user, offset: 0, limit: 10, allow_blank_q: true, filter: { exclude_ids: trusted_authors.map(&:id) }, load: true)
        .returns(Search::UserSearchResponse.new(results: [@user, author], total: 2)).once

      get :not_contributors, id: channel.id, trusted: true, search_term: 'dummy', format: :json
      json_response = get_json_response(response)
      assert_equal 2, json_response['not_authors'].length
      assert_equal 2, json_response['total']
      user_ids = json_response['not_authors'].map { |usr| usr['id'] }
      refute_includes trusted_authors.map(&:id), user_ids
    end

    test ':api #contributors should fetch trusted collaborators only' do
      channel = create(:channel, organization: @org)
      create_list(:user, 2, organization: @org)
      authors = create_list(:user, 3, organization: @org)
      trusted_authors = create_list(:user, 3, organization: @org)
      channel.add_authors(authors)
      channel.add_authors(trusted_authors, true)
      api_v2_sign_in(@user)

      get :contributors, id: channel.id, trusted: true, format: :json
      json_response = get_json_response(response)
      assert_equal 3, json_response['authors'].length
      assert_equal 3, json_response['total']
      user_ids = json_response['authors'].map { |usr| usr['id'] }
      assert_same_elements trusted_authors.map(&:id), user_ids
    end

    test ':api #contributors should fetch authors of the channel without duplications' do
      channel = create(:channel, organization: @org)
      users = create_list(:user, 2, organization: @org)
      authors = create_list(:user, 3, organization: @org)
      channel.add_authors(authors)
      channel.add_authors(authors, true)
      channel.add_authors(users, true)
      api_v2_sign_in(@user)

      get :contributors, id: channel.id, format: :json
      json_response = get_json_response(response)
      assert_equal 3, json_response['authors'].length
      assert_equal 3, json_response['total']
      user_ids = json_response['authors'].map { |usr| usr['id'] }
      assert_same_elements authors.map(&:id), user_ids
    end

    test ':api should fetch trusted collaborators only' do
      channel = create(:channel, organization: @org)
      trusted_authors = create_list(:user, 3, organization: @org)
      channel.add_authors(trusted_authors, true)
      api_v2_sign_in(@user)

      get :trusted_collaborators, id: channel.id, format: :json
      json_response = get_json_response(response)
      tr_ids = json_response['trusted_collaborators'].map { |i| i['id'] }
      assert_equal 3, json_response['trusted_collaborators'].length
      assert_equal 3, json_response['total']
      assert_same_elements trusted_authors.map(&:id), tr_ids
    end

    test 'should fetch active trusted collaborators' do
      channel = create(:channel, organization: @org)
      trusted_authors = create_list(:user, 3, organization: @org)
      channel.add_authors(trusted_authors, true)
      # suspend the user
      trusted_authors.last.suspend!

      api_v2_sign_in(@user)

      get :trusted_collaborators, id: channel.id, format: :json
      json_response = get_json_response(response)
      tr_ids = json_response['trusted_collaborators'].map { |i| i['id'] }
      assert_equal 2, json_response['trusted_collaborators'].length
      assert_equal 2, json_response['total']
      assert_same_elements [trusted_authors[0].id, trusted_authors[1].id], tr_ids
    end

    test ':api should fetch trusted collaborators and support limit' do
      channel = create(:channel, organization: @org)
      trusted_authors = create_list(:user, 2, organization: @org)
      channel.add_authors(trusted_authors, true)
      api_v2_sign_in(@user)

      get :trusted_collaborators, id: channel.id, limit: 1, format: :json
      json_response = get_json_response(response)
      tr_id = json_response['trusted_collaborators'].first['id']
      assert_equal 1, json_response['trusted_collaborators'].size
      assert_equal trusted_authors.first.id, tr_id
    end

    test ':api should fetch trusted collaborators and support limit/offset' do
      channel = create(:channel, organization: @org)
      trusted_authors = create_list(:user, 2, organization: @org)
      channel.add_authors(trusted_authors, true)
      api_v2_sign_in(@user)

      get :trusted_collaborators, id: channel.id, limit: 1, offset: 1, format: :json
      json_response = get_json_response(response)
      tr_id = json_response['trusted_collaborators'].first['id']
      assert_equal 1, json_response['trusted_collaborators'].size
      assert_equal trusted_authors.last.id, tr_id
    end
  end

  class ChannelSearchCardsTest < ActionController::TestCase
    setup do
      @organization = create(:organization)
      @channel = create(:channel, label: 'test channel', organization: @organization)

      # Create author for cards to be added to channel
      @author = create(:user, organization: @organization)

      @channel_user = create(:user, organization: @organization)
      api_v2_sign_in(@channel_user)
    end

    def create_channel_cards_for_entitlements
      @public_card_1 = create(:card, organization: @organization, author: @author, title: 'Public card 1')
      @public_card_2 = create(:card, organization: @organization, author: @author, title: 'Public card 2')
      @private_card_1 = create(:card, organization: @organization, author: @author, title: 'Private card 1', is_public: false)
      @private_card_2 = create(:card, organization: @organization, author: @author, title: 'Private card 2', is_public: false)
      @channel.cards << [@public_card_1, @public_card_2, @private_card_1, @private_card_2]
    end

    test ':api user should be able to search for cards within channel' do
      create_channel_cards_for_entitlements
      @channel.add_followers([@channel_user])

      Search::CardSearch.any_instance.expects(:search_in_channel)
        .with(
          q: 'card',
          channel_id: @channel.id,
          viewer: @channel_user,
          organization_id: @organization.id,
          filter: { skip_aggs: false, is_cms: false },
          limit: 10,
          offset: 0,
          load: true
        )
        .returns({
          results: [@public_card_1, @public_card_2, @private_card_1, @private_card_2],
          total: 4,
          aggs: {}
        })

      get :search_cards, id: @channel.id, q: 'card', format: :json
      json_response = get_json_response(response)
      assert_response :ok
      assert_equal @channel.cards.map(&:id), json_response['cards'].map{ |card| card['id'].to_i }
      assert_equal 4, json_response['total']
    end

    test 'should return all cards of the channel if search query is blank or *' do
      create_channel_cards_for_entitlements
      @channel.add_followers([@channel_user])

      Search::CardSearch.any_instance.expects(:search_in_channel)
        .with(
          q: '',
          channel_id: @channel.id,
          viewer: @channel_user,
          organization_id: @organization.id,
          filter: { skip_aggs: false, is_cms: false },
          limit: 10,
          offset: 0,
          load: true
        )
        .returns({
          results: [@public_card_1, @public_card_2, @private_card_1, @private_card_2],
          total: 4,
          aggs: {}
        })

      get :search_cards, id: @channel.id, q: '', format: :json
      json_response = get_json_response(response)
      assert_response :ok
      assert_equal @channel.cards.map(&:id), json_response['cards'].map{ |card| card['id'].to_i }
      assert_equal 4, json_response['total']

      Search::CardSearch.any_instance.expects(:search_in_channel)
        .with(
          q: '*',
          channel_id: @channel.id,
          viewer: @channel_user,
          organization_id: @organization.id,
          filter: { skip_aggs: false, is_cms: false },
          limit: 10,
          offset: 0,
          load: true
        )
        .returns({
          results: [@public_card_1, @public_card_2, @private_card_1, @private_card_2],
          total: 4,
          aggs: {}
        })

      get :search_cards, id: @channel.id, q: '*', format: :json
      json_response = get_json_response(response)
      assert_response :ok
      assert_equal @channel.cards.map(&:id), json_response['cards'].map{ |card| card['id'].to_i }
      assert_equal 4, json_response['total']
    end

    test 'should not be able to get private cards with * search for a user not following the channel' do
      create_channel_cards_for_entitlements
      Search::CardSearch.any_instance.expects(:search_in_channel)
        .with(
          q: '*',
          channel_id: @channel.id,
          viewer: @channel_user,
          organization_id: @organization.id,
          filter: { skip_aggs: false, is_cms: false },
          limit: 10,
          offset: 0,
          load: true
        )
        .returns({
          results: [@public_card_1, @public_card_2],
          total: 2,
          aggs: {}
        })

      get :search_cards, id: @channel.id, q: '*', format: :json
      json_response = get_json_response(response)
      assert_response :ok
      assert_equal [@public_card_1.id, @public_card_2.id], json_response['cards'].map{ |card| card['id'].to_i }
      assert_equal 2, json_response['total']
    end
  end

  class ChannelCardsTest < ActionController::TestCase
    setup do
      @organization = create(:organization)
      @channel = create(:channel, label: 'test channel', organization: @organization)

      # Create author for cards to be added to channel
      @author = create(:user, organization: @organization)

      @channel_user = create(:user, organization: @organization)
      api_v2_sign_in(@channel_user)
    end

    test ':api should return cards of channels' do
      card_1 = create(:card, organization: @organization, author: @author)
      card_2 = create(:card, organization: @organization, author: @author)
      @channel.cards << [card_1, card_2]

      get :cards, id: @channel.id, format: :json
      json_response = get_json_response(response)
      assert_response :ok
      assert_equal 2, json_response["total"]
      assert_same_elements [card_1.id, card_2.id], json_response['cards'].map { |card| card['id'].to_i }
    end

    test ':api should return cards sorted by rank as default sort order' do
      card_1, card_2, card_3, card_4 = create_list(:card, 4, organization: @organization, author: @author)
      @channel.cards << [card_1, card_2, card_3, card_4]

      # Update channel_card created_at of card_1 and card_2
      channel_card_1 = ChannelsCard.find_by(channel: @channel, card: card_1)
      channel_card_1.update(created_at: Date.today - 5)
      channel_card_2 = ChannelsCard.find_by(channel: @channel, card: card_2)
      channel_card_2.update(created_at: Date.today - 4)

      # Set rank for card_3 and card_4
      channel_card_3 = ChannelsCard.find_by(channel: @channel, card: card_3)
      channel_card_3.update(rank: 10, created_at: Date.today - 6)
      channel_card_4 = ChannelsCard.find_by(channel: @channel, card: card_4)
      channel_card_4.update(rank: 5, created_at: Date.today - 7)

      get :cards, id: @channel.id, format: :json
      json_response = get_json_response(response)
      assert_response :ok
      assert_equal 4, json_response["total"]
      assert_same_elements [card_4.id, card_3.id, card_2.id, card_1.id], json_response['cards'].map { |card| card['id'].to_i }

      # Rank should be returned for cards with non null rank values
      assert_includes(json_response['cards'].detect {|card| card['id'] == card_3.id.to_s}.keys, 'rank')
      refute_includes(json_response['cards'].detect {|card| card['id'] == card_1.id.to_s}.keys, 'rank')
    end

    test 'should return non curated cards to channel curators when channel_card_state is new' do
      curatable_channel = create(:channel, organization: @organization, curate_only: true)
      curatable_channel.add_followers([@channel_user])
      curatable_channel.add_curators([@channel_user])

      card_1 = create(:card, organization: @organization, author: @author)
      card_2 = create(:card, organization: @organization, author: @author)
      curatable_channel.cards << [card_1, card_2]

      # Curate card_1
      ChannelsCard.where(card_id: card_1.id, channel_id: curatable_channel.id).first.curate!

      get :cards, id: curatable_channel.id, channel_card_state: 'new', format: :json
      json_response = get_json_response(response)
      assert_response :ok
      assert_equal 1, json_response["total"]
      assert_same_elements [card_2.id], json_response['cards'].map { |card| card['id'].to_i }
    end

    test 'should return unauthorized if user is not curator when channel_card_state is new' do
      curatable_channel = create(:channel, organization: @organization, curate_only: true)
      get :cards, id: curatable_channel.id, channel_card_state: 'new', format: :json
      assert_response :unauthorized
    end
  end

  # TODO: refactor these tests.
  # see comment here https://github.com/edcast/edcast/pull/6899/files#r162718510
  # class ChannelVersionsTest < ActionController::TestCase
  #   setup do
  #     @author = create(:user, organization_role: 'admin')
  #     api_v2_sign_in @author
  #   end

  #   test 'should log the channel creation history' do
  #     TestAfterCommit.enabled = true

  #     post :create, channel: { label: 'Test Channel',description: 'Test Channel'}, format: :json
  #     channel = Channel.last

  #     get :versions, id: channel.id , format: :json
  #     assert_response :ok
  #     json_response = get_json_response(response)

  #     assert_equal 3, json_response['total_count']
  #     assert_equal ['Follower', 'Curator','Channel'], json_response['versions'].collect{|i| i["item_type"]}
  #   end

  #   test 'should log the channel creation and updation history' do
  #     TestAfterCommit.enabled = true

  #     post :create, channel: { label: 'Test Channel',description: 'Test Channel'}, format: :json
  #     channel = Channel.last

  #     put :update, id: channel.id, channel: { curate_only: true }, format: :json


  #     get :versions, id: channel.id , format: :json
  #     assert_response :ok
  #     json_response = get_json_response(response)

  #     assert_equal 4, json_response['total_count']
  #     assert_equal ["Channel",'Follower', 'Curator','Channel'], json_response['versions'].collect{|i| i["item_type"]}
  #   end

  #   test 'should log the card addition and deletion from a channel' do
  #     TestAfterCommit.enabled = true

  #     post :create, channel: { label: 'Test Channel',description: 'Test Channel'}, format: :json
  #     channel = Channel.last

  #     put :update, id: channel.id, channel: { curate_only: true }, format: :json

  #     smarbite1, smarbite2 = create_list(:card, 2, organization: @org, author_id: @author.id)
  #     channel.cards << [smarbite1, smarbite2]
  #     channel.channels_cards.destroy_all

  #     get :versions, id: channel.id , format: :json
  #     assert_response :ok
  #     json_response = get_json_response(response)
  #     assert_equal 8, json_response['total_count']
  #   end
  # end
end

require 'test_helper'

class Api::V2::JourneysControllerTest < ActionController::TestCase
  class ShowJourneysControllerTest < ActionController::TestCase
    setup do
      @user = create(:user)
      @org = @user.organization
      api_v2_sign_in @user
    end

    test 'returns journeys details when journey id provided' do
      Analytics::CardViewMetricsRecorder.stubs(:set_initial_card_view_count).returns(0)

      journey_card = create(:card, author: @user, organization: @org, card_type: 'journey', card_subtype: 'self_paced')
      create :card_metadatum, card: journey_card, plan: 'free', level: 'beginner', average_rating: 5

      pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack')
      journey_card.add_card_to_journey(pathway_card.id)

      pathway_card1 = create(:card, author: @user, organization: @org, card_type: 'pack')
      journey_card.add_card_to_journey(pathway_card1.id)

      card = create(:card, author: @user, organization: @org)
      pathway_card.add_card_to_pathway(card.id, 'Card')
      pathway_card1.add_card_to_pathway(card.id, 'Card')

      get :show, id: journey_card.id, is_standalone_page: true, format: :json

      json_response = get_json_response(response)

      assert_response :ok

      keys = %w[
        id               title           message              card_type
        card_subtype     slug            state                is_official
        provider         provider_image  readable_card_type   share_url
        average_rating   skill_level     filestack            votes_count
        comments_count   published_at    prices               is_assigned
        is_bookmarked    hidden          is_public            is_paid
        completion_state is_upvoted      all_ratings          card_metadatum
        author           is_clone        completed_percentage views_count
        channel_ids      voters          tags                 channels
        teams            journey_section created_at           mark_feature_disabled_for_source
        language
      ]
      assert_same_elements keys, json_response.keys
      assert_equal 2, json_response['journey_section'].length
    end

    test 'render 404 if the id passed is not of a journey' do
      card = create :card, author: @user, organization: @org

      journey_card = create(:card, author: @user, organization: @org, card_type: 'journey', card_subtype: "self_paced")

      pathway = create :card, author: @user, card_type: 'pack', organization: @org
      journey_card.add_card_to_journey(pathway.id)
      create :card_pack_relation, cover_id: pathway.id, from_id: card.id, from_type: 'Card'

      get :show, id: pathway.id, format: :json

      assert_response :not_found
    end

    test 'should return empty array for children cards if not present' do
      # Create pathway
      journey_card = create(:card, author: @user, organization: @org, card_type: 'journey', card_subtype: "self_paced")
      get :show, id: journey_card.id, is_standalone_page: false, format: :json
      json_response = get_json_response(response)
      assert_response :ok
      assert_empty json_response['journey_section']
    end

    test 'should return completed_percentage of cover card to 98 percent even though some cards are deleted from journey and if cardPackRelations deleted is set to true' do
      journey_card = create(:card, author: @user, organization: @org, card_type: 'journey', card_subtype: "self_paced")
      pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack')
      pathway_card.publish!
      journey_card.add_card_to_journey(pathway_card.id)
      journey_card.publish!
      card, deleted_card = create_list(:card, 2, organization: @org, author: @user)
      [card, deleted_card].each { |card| pathway_card.add_card_to_pathway(card.id, 'Card') }
      deleted_card.update_attributes(deleted_at: Time.now)
      CardPackRelation.find_by(cover_id: pathway_card.id, from_id: deleted_card.id).update_attributes!(deleted: true)
      UserContentCompletion.create(completable: card, state: 'completed', user_id: @user.id, completed_percentage: 100)

      get :show, id: journey_card.id, is_standalone_page: false, format: :json
      json_response = get_json_response(response)
      assert_response :ok
      # Journey completed percentage should be 98 percent when we complete pathways present in a journey
      assert_equal 98 , json_response['completed_percentage']
      # Pathway present in journey completed_percen
      assert_equal 100, json_response['journey_section'][0]['completed_percentage']
    end

    test 'should return total_count correctly if it contain some deleted cardPackRelations' do
      journey_card = create(:card, author: @user, organization: @org, card_type: 'journey', card_subtype: "self_paced")
      pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack')
      pathway_card.publish!
      journey_card.add_card_to_journey(pathway_card.id)
      journey_card.publish!
      card, deleted_card = create_list(:card, 2, organization: @org, author: @user)
      [card, deleted_card].each { |card| pathway_card.add_card_to_pathway(card.id, 'Card') }
      deleted_card.update_attributes(deleted_at: Time.now)
      CardPackRelation.find_by(cover_id: pathway_card.id, from_id: deleted_card.id).update_attributes!(deleted: true)
      UserContentCompletion.create(completable: card, state: 'completed', user_id: @user.id, completed_percentage: 100)

      get :pathway_cards, id: journey_card.id, is_standalone_page: true, pathway_id: pathway_card.id, format: :json
      json_response = get_json_response(response)
      assert_response :ok
      # total_count is 1 even though we have 2 card_pack_card relation.
      assert_equal 1 , json_response['total_count']
    end

    test 'should return visible key for non-admin user' do
      user = create(:user, organization: @user.organization)
      journey_card = create(:card, author: user, organization: @org, card_type: 'journey', card_subtype: "self_paced")
      pathway_card_visible = create(:card, author: user, organization: @org, card_type: 'pack', is_public: true)
      pathway_card_not_visible = create(:card, author: user, organization: @org, card_type: 'pack', is_public: false)

      pathway_card_visible.publish!
      pathway_card_not_visible.publish!

      journey_card.add_card_to_journey(pathway_card_visible.id)
      journey_card.add_card_to_journey(pathway_card_not_visible.id)

      journey_card.publish!

      get :show, id: journey_card.id, is_standalone_page: false, format: :json
      resp = get_json_response(response)
      assert_response :ok

      assert_equal 2, resp['journey_section'].length
      
      assert resp['journey_section'].detect{|d| d['id'] == pathway_card_visible.id}['visible']
      refute resp['journey_section'].detect{|d| d['id'] == pathway_card_not_visible.id}['visible']
    end

    test 'returns visible true for admin' do
      user = create(:user, organization: @user.organization)
      admin = create(:user, organization_role: 'admin', organization: @user.organization)

      api_v2_sign_in admin

      journey_card = create(:card, author: user, organization: @org, card_type: 'journey', card_subtype: "self_paced")
      pathway_card_visible = create(:card, author: user, organization: @org, card_type: 'pack', is_public: true)
      pathway_card_not_visible = create(:card, author: user, organization: @org, card_type: 'pack', is_public: false)

      pathway_card_visible.publish!
      pathway_card_not_visible.publish!

      journey_card.add_card_to_journey(pathway_card_visible.id)
      journey_card.add_card_to_journey(pathway_card_not_visible.id)

      journey_card.publish!

      get :show, id: journey_card.id, is_standalone_page: false, format: :json
      resp = get_json_response(response)
      assert_response :ok

      assert_equal 2, resp['journey_section'].length
      assert resp['journey_section'].detect{|d| d['id'] == pathway_card_visible.id}['visible']
      assert resp['journey_section'].detect{|d| d['id'] == pathway_card_not_visible.id}['visible']
    end

    test 'should return false for visible key when no sections are accessible' do
      user = create(:user, organization: @user.organization)

      journey_card = create(:card, author: user, organization: @org, card_type: 'journey', card_subtype: "self_paced")
      pathway_card_not_visible = create(:card, author: user, organization: @org, card_type: 'pack', is_public: false)

      pathway_card_not_visible.publish!
      journey_card.add_card_to_journey(pathway_card_not_visible.id)

      journey_card.publish!

      get :show, id: journey_card.id, is_standalone_page: false, format: :json
      resp = get_json_response(response)
      assert_response :ok

      assert_equal 1, resp['journey_section'].length
      refute resp['journey_section'][0]['visible']
    end
  end

  class PathwayCardsJourneysControllerTest < ActionController::TestCase
    setup do
      @user = create(:user)
      @org = @user.organization
      api_v2_sign_in @user
    end

    test ':api should return all the cards in the pathway' do
      journey_card = create(:card, author: @user, organization: @org, card_type: 'journey', card_subtype: "self_paced")
      pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack')
      card = create(:card, author: @user, organization: @org)
      journey_card.add_card_to_journey(pathway_card.id)
      pathway_card.add_card_to_pathway(card.id, 'Card')

      get :pathway_cards, id: journey_card.id, pathway_id: pathway_card.id, format: :json

      json_response = get_json_response(response)
      assert_response :ok
      assert_same_elements ['cards', 'total_count'], json_response.keys
      assert_equal 1, json_response['cards'].length
      assert_equal 1, json_response['total_count']
      assert_same_elements ['id', 'title', 'message', 'card_type', 'card_subtype',
        'slug', 'state', 'is_official', 'provider', 'provider_image', 'readable_card_type',
        'share_url', 'average_rating', 'skill_level', 'filestack', 'votes_count',
        'comments_count', 'published_at', 'prices', 'is_assigned', 'is_bookmarked',
        'hidden', 'is_public', 'is_paid', 'mark_feature_disabled_for_source',
        'completion_state', 'is_upvoted', 'all_ratings', 'author',
        'is_clone', 'is_locked', 'created_at'], json_response['cards'].first.keys
    end

    test 'should return cards based on limit and offset' do
      journey_card = create(:card, author: @user, organization: @org, card_type: 'journey', card_subtype: "self_paced")
      pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack')
      journey_card.add_card_to_journey(pathway_card.id)

      cards = create_list(:card, 15, author: @user, organization: @org)
      cards.each { |card| pathway_card.add_card_to_pathway(card.id, 'Card') }

      # Should return cards with default limit - 15 and offset - 0 if not passed
      get :pathway_cards, id: journey_card.id, pathway_id: pathway_card.id, format: :json
      json_response = get_json_response(response)
      assert_response :ok
      assert_equal 10, json_response['cards'].length
      assert_equal 15, json_response['total_count']
      assert_equal cards.map(&:id)[0..9], json_response['cards'].map { |card| card['id'].to_i }

      # Should return cards with passed limit & offset
      get :pathway_cards, id: journey_card.id, pathway_id: pathway_card.id, limit: 3, offset: 10, format: :json
      json_response = get_json_response(response)
      assert_response :ok
      assert_equal 3, json_response['cards'].length
      assert_equal 15, json_response['total_count']
      assert_equal cards.map(&:id)[10..12], json_response['cards'].map { |card| card['id'].to_i }
    end

    test 'should return true if the card is locked else false' do
      journey_card = create(:card, author: @user, organization: @org, card_type: 'journey', card_subtype: "self_paced")
      pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack')
      journey_card.add_card_to_journey(pathway_card.id)

      locked_card, unlocked_card = create_list(:card, 2, organization: @org, author: @user)
      [locked_card, unlocked_card].each { |card| pathway_card.add_card_to_pathway(card.id, 'Card') }

      CardPackRelation.find_by(cover_id: pathway_card.id, from_id: locked_card.id).update_attributes!(locked: true)

      get :pathway_cards, id: journey_card.id, pathway_id: pathway_card.id, format: :json
      json_response = get_json_response(response)
      assert_response :ok
      assert json_response['cards'].detect { |entity| entity['id'] == locked_card.id.to_s }['is_locked']
      assert_not json_response['cards'].detect { |entity| entity['id'] == unlocked_card.id.to_s }['is_locked']
    end

    test 'should return include_default_fields along with fields' do
      journey_card = create(:card, author: @user, organization: @org, card_type: 'journey', card_subtype: "self_paced")
      pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack')
      journey_card.add_card_to_journey(pathway_card.id)

      locked_card, unlocked_card = create_list(:card, 2, organization: @org, author: @user)
      [locked_card, unlocked_card].each { |card| pathway_card.add_card_to_pathway(card.id, 'Card') }

      channel = create(:channel, organization: @org)
      locked_card.channels << channel

      CardPackRelation.find_by(cover_id: pathway_card.id, from_id: locked_card.id).update_attributes!(locked: true)

      get :pathway_cards, id: journey_card.id, pathway_id: pathway_card.id, fields: 'channel_ids,channels(id,label),channels', format: :json
      json_response = get_json_response(response)
      assert_response :ok

      assert_same_elements ['channel_ids', 'channels', 'id', 'title', 'message', 'card_type', 'card_subtype', 'slug', 'state',
        'is_official', 'provider', 'provider_image', 'readable_card_type', 'share_url', 'average_rating', 'skill_level',
        'filestack', 'votes_count', 'comments_count', 'published_at', 'prices', 'is_assigned', 'is_bookmarked', 'hidden',
        'is_public', 'is_paid', 'mark_feature_disabled_for_source', 'completion_state', 'is_upvoted', 'all_ratings', 'author',
        'is_clone', 'is_locked', 'created_at'], json_response['cards'].first.keys
    end

    test 'packaging cards should contain information about children_cards the lock status' do
      journey_card = create(:card,
        author: @user, organization: @org,
        card_type: 'journey', card_subtype: 'self_paced'
      )
      pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack')
      journey_card.add_card_to_journey(pathway_card.id)

      locked_card, unlocked_card = create_list(:card, 2, organization: @org, author: @user)
      [locked_card, unlocked_card].each { |card| pathway_card.add_card_to_pathway(card.id, 'Card') }

      CardPackRelation.find_by(
        cover_id: pathway_card.id, from_id: locked_card.id
      ).update_attributes(locked: true)

      get :pathway_cards, id: journey_card.id, pathway_id: pathway_card.id, format: :json

      assert_response :ok
      resp = get_json_response(response)

      assert resp['cards'][0]['is_locked']
      assert_equal locked_card.id.to_s, resp['cards'][0]['id']
      refute resp['cards'][1]['is_locked']
    end
  end
  class VisibilityCheckForPathwayCardsTest < ActionController::TestCase

    setup do
      @org = create(:organization)
      @author = create(:user, organization: @org)
      
      @journey_card = create(:card, author: @author, organization: @org, card_type: 'journey', card_subtype: "self_paced")
      @pathway_card = create(:card, author: @author, organization: @org, card_type: 'pack', is_public: true)

      @journey_card.add_card_to_journey(@pathway_card.id)

      @card_list = create_list(:card, 3, author: @author, organization: @org)
      @card_list[0].update(is_public: false)
      @card_list[2].update(is_public: false)

      @card_list.each{|card| @pathway_card.add_card_to_pathway(card.id, 'Card')}
      Role.create_standard_roles @org
    end
    
    test 'should return visible cards of the pathway of a journey' do
      user = create(:user, organization: @org)
      api_v2_sign_in user

      team = create(:team, organization_id: @org.id)
      team.add_member user
      
      create(:card_user_share, card_id: @card_list[0].id, user_id: user.id)
      create(:teams_card, card_id: @card_list[2].id, team_id: team.id, type: 'SharedCard')
      create(:card_user_permission, card_id: @card_list[2].id, user_id: create(:user, organization: @org).id, show: true)

      get :pathway_cards, id: @journey_card.id, pathway_id: @pathway_card.id, format: :json
      
      json_response = get_json_response(response)
      
      assert_includes json_response['cards'].map{|d| d['id'].to_i}, @card_list[0].id
      assert_includes json_response['cards'].map{|d| d['id'].to_i}, @card_list[1].id
      assert_not_includes json_response['cards'].map{|d| d['id'].to_i}, @card_list[2].id
      assert_equal "You are not authorized to access this card", json_response['cards'][2]['message']
    end

    test 'should not call #private_accessible_cards for admin user' do
      org_admin = create(:user, organization: @org, organization_role: 'admin')
      role_admin = @org.roles.find_by(name: 'admin')
      role_admin.add_user(org_admin)

      api_v2_sign_in org_admin
      get :pathway_cards, id: @journey_card.id, pathway_id: @pathway_card.id, format: :json
      json_response = get_json_response(response)
      assert_same_elements json_response['cards'].map{|d| d['id'].to_i}, @card_list.map(&:id)
    end

    test 'should call #private_accessible_cards for group admin' do
      team = create(:team, organization: @org)
      role_member = @org.roles.find_by(name: 'member')
      
      create(:role_permission, role: role_member, name: Permissions::MANAGE_GROUP_CONTENT)
      group_admin = create(:user, organization: @org, organization_role: 'member')
      role_member.add_user(group_admin)
      
      api_v2_sign_in group_admin
      get :pathway_cards, id: @journey_card.id, pathway_id: @pathway_card.id, format: :json
      json_response = get_json_response(response)

      assert_includes json_response['cards'].map{|d| d['id'].to_i}, @card_list[1].id
      assert_equal "You are not authorized to access this card", json_response['cards'][0]['message']
      assert_equal "You are not authorized to access this card", json_response['cards'][2]['message']
    end
  end
end

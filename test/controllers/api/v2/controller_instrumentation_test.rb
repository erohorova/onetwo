require 'test_helper'
require 'action_controller'

class Api::V2::ControllerInstrumentationTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  class DummyController < ActionController::Base
    include NativeAppDetection
    include DetectPlatform

    def show
      render json: {}
    end
  end

  setup do
    skip "disabled at the moment"
    @org = create :organization
    @user = create(:user, organization: @org)

    @controller = DummyController.new

    Rails.application.routes.draw do
      get 'show', controller: 'api/v2/controller_instrumentation_test/dummy', action: 'show'
    end
  end

  teardown do
    Rails.application.reload_routes!
  end

  test 'should store data in reqeust store from user request #logged in user' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @controller.stubs(:new_user_session_url).returns('/')
    @request.headers['X-Version-Number'] = 10.2
    @request.headers['User-Agent'] = 'Edcast iPhone'

    sign_in @user
    get :show, format: :json

    metadata =  RequestStore.read(:request_metadata)

    assert_equal 'ios', metadata[:platform]
    assert_equal 10.2, metadata[:platform_version_number]
    assert_equal @user.id, metadata[:user_id]
    assert_equal 'Edcast iPhone', metadata[:user_agent]
  end

  test 'should store data in reqeust store from user request #logged out user' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @controller.stubs(:new_user_session_url).returns('/')
    @request.headers['X-Version-Number'] = 10.2
    @request.headers['User-Agent'] = 'Edcast iPhone'

    get :show, format: :json

    metadata =  RequestStore.read(:request_metadata)

    assert_equal 'ios', metadata[:platform]
    assert_equal 10.2, metadata[:platform_version_number]
    assert_nil metadata[:user_id]
    assert_equal 'Edcast iPhone', metadata[:user_agent]
  end
end
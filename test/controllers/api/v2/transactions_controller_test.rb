require 'test_helper'

class Api::V2::TransactionsControllerTest < ActionController::TestCase

  class InitiateTransactionTests < ActionController::TestCase
    setup do
      @user = create(:user)
      @org = @user.organization
      create(:config, configable: @org, name: 'payment_gateway', value: {braintree: 1, paypal: 1}.to_json)
      api_v2_sign_in(@user)
      organization_profile = @org.create_organization_profile!(member_pay: true)

      @org_subscription = create(:org_subscription, organization: @org)

      @org_subscription_price = @org_subscription.prices.create!(
        currency: 'INR',
        amount: BigDecimal.new("3500"))
    end

    test 'api should initiate transaction for org_subscription using braintree' do
      BraintreePaymentGateway.any_instance.expects(:generate_client_token).returns('asdfasdfas')
      post :initiate_transaction, orderable_id: @org_subscription.id,
           orderable_type: 'org_subscription',
           price_id: @org_subscription_price.id,
           gateway: 'braintree', format: :json
      resp = get_json_response(response)

      assert_not_empty resp['transaction_details']
      assert_equal @org_subscription.id, resp['transaction_details']['order']['orderable_id']
      assert_equal @org_subscription.class.name, resp['transaction_details']['order']['orderable_type']
      assert_not_empty resp['transaction_details']['token']
      assert_not_empty resp['transaction_details']['client_token']
    end

    test 'api should initiate transaction for card using paypal' do
      payment_object = PayPal::SDK::REST::Payment.new(id: 'PAY-A1B2C3D4E5F7G8H9')
      payment_object.stubs(:approval_url).returns('http://mock-paypal-approval-url.com')
      PaypalPaymentGateway.any_instance.expects(:create_payment_object).returns(payment_object)
      card = create(:card, organization: @org, author: @user)
      price = create(:price, card: card)
      post :initiate_transaction, orderable_id: card.id,
           orderable_type: 'Card',
           price_id: price.id,
           gateway: 'paypal', format: :json
      resp = get_json_response(response)
      assert_not_empty resp['transaction_details']
      assert_equal card.id, resp['transaction_details']['order']['orderable_id']
      assert_equal card.class.name, resp['transaction_details']['order']['orderable_type']
      assert_not_empty resp['transaction_details']['token']
      assert_equal resp['transaction_details']['url'], 'http://mock-paypal-approval-url.com'
    end

    test 'api should return transaction params even if the previous transaction had failed' do
      card = create(:card, organization: @org, author: @user)
      price = create(:price, card: card)
      order = create(:order, orderable: card, state: 2, organization_id: @org.id, user: @user)
      transaction = create(:transaction, order: order, payment_state: 2)
      BraintreePaymentGateway.any_instance.expects(:generate_client_token).returns('asdfasdfas')
      post :initiate_transaction, orderable_id: card.id,
           orderable_type: 'card',
           price_id: price.id,
           gateway: 'braintree', format: :json
      resp = get_json_response(response)

      assert_not_empty resp['transaction_details']
      assert_equal card.id, resp['transaction_details']['order']['orderable_id']
      assert_equal card.class.name, resp['transaction_details']['order']['orderable_type']
      assert_not_empty resp['transaction_details']['token']
      assert_not_empty resp['transaction_details']['client_token']
    end

    test 'api should not initiate transaction for org_subscription without prices' do
      post :initiate_transaction, orderable_id: @org_subscription.id,
           orderable_type: 'org_subscription', format: :json

      assert_response :not_found
      resp = get_json_response(response)
      assert_equal "Price can not be found.", resp["message"]
    end

    test 'api should not initiate transaction without orderable' do
      post :initiate_transaction, format: :json

      assert_response :not_found
      resp = get_json_response(response)
      assert_equal "Order can not be found.", resp["message"]
    end

    test 'api should not create a duplicate order if card subscription has not expired' do
      BraintreePaymentGateway.any_instance.expects(:generate_client_token).returns('asdfasdfas')
      card = create(:card, organization: @org, author: @user)
      price = create(:price, card: card, amount: BigDecimal.new('100'))
      order = create(:order, orderable: card, user: @user, organization_id: @org.id)

      post :initiate_transaction, orderable_id: card.id,
           orderable_type: 'Card',
           price_id: price.id, gateway: 'braintree', format: :json
      resp = get_json_response(response)

      assert_equal order.id, resp['transaction_details']['order']['id']
      assert_equal 1, card.orders.count
    end

    test 'api should create a new order if card subscription has expired' do
      BraintreePaymentGateway.any_instance.expects(:generate_client_token).returns('asdfasdfas')
      card = create(:card, organization: @org, author: @user)
      price = create(:price, card: card, amount: BigDecimal.new('100'))
      order = create(:order, orderable: card, user: @user, organization_id: @org.id, state: 1)
      subscription = create(:card_subscription, card_id: card.id, user_id: @user.id,
                                                start_date: Date.today-3.days, end_date: Date.today-1.days)

      post :initiate_transaction, orderable_id: card.id,
           orderable_type: 'Card',
           price_id: price.id, gateway: 'braintree', format: :json
      resp = get_json_response(response)

      assert_not_equal order.id, resp['transaction_details']['order']['id']
      assert_equal 2, card.orders.count
    end

    test 'api should raise validation error if card subscription has not expired' do
      BraintreePaymentGateway.any_instance.stubs(:generate_client_token).returns('asdfasdfas')
      card = create(:card, organization: @org, author: @user)
      price = create(:price, card: card, amount: BigDecimal.new('100'))
      order = create(:order, orderable: card, user: @user, organization_id: @org.id, state: 1)
      subscription = create(:card_subscription, card_id: card.id, user_id: @user.id,
                                                start_date: Date.today-3.days, end_date: Date.today+2.days)

      post :initiate_transaction, orderable_id: card.id,
           orderable_type: 'Card',
           price_id: price.id, gateway: 'braintree', format: :json
      resp = get_json_response(response)

      assert_match /Payment has already been initiated/, resp["message"]
      # assert_equal true, resp["card_purchased"]
      assert_equal 1, card.orders.count
    end

    test 'api should create a new order record if previous order failed and card has not expired' do
      BraintreePaymentGateway.any_instance.stubs(:generate_client_token).returns('asdfasdfas')
      card = create(:card, organization: @org, author: @user)
      price = create(:price, card: card, amount: BigDecimal.new('100'))
      order = create(:order, orderable: card, user: @user, organization_id: @org.id, state: 2)
      subscription = create(:card_subscription, card_id: card.id, user_id: @user.id,
                                                start_date: Date.today-3.days, end_date: Date.today+2.days)

      post :initiate_transaction, orderable_id: card.id,
           orderable_type: 'Card',
           price_id: price.id, gateway: 'braintree', format: :json

      assert_equal 2, card.orders.count
    end

    test 'api should create a new order record if previous order failed for org_subscription' do
      BraintreePaymentGateway.any_instance.stubs(:generate_client_token).returns('asdfasdfas')
      order = create(:order, orderable: @org_subscription, user: @user, organization_id: @org.id, state: 2)

      post :initiate_transaction, orderable_id: @org_subscription.id,
           orderable_type: 'org_subscription',
           price_id: @org_subscription_price.id, gateway: 'braintree', format: :json

      assert_equal 2, @org_subscription.orders.count
    end

    test 'api should not create a new order record if previous order was successful for org_subscription' do
      BraintreePaymentGateway.any_instance.stubs(:generate_client_token).returns('asdfasdfas')
      order = create(:order, orderable: @org_subscription, user: @user, organization_id: @org.id, state: 1)

      post :initiate_transaction, orderable_id: @org_subscription.id,
           orderable_type: 'org_subscription',
           price_id: @org_subscription_price.id, gateway: 'braintree', format: :json

      assert_equal 1, @org_subscription.orders.count
    end

    test 'api should create a new order record if previous order failed for recharge' do
      BraintreePaymentGateway.any_instance.stubs(:generate_client_token).returns('asdfasdfas')
      recharge = create(:recharge)
      recharge_price = create(:recharge_price, recharge_id: recharge.id)
      order = create(:order, orderable: recharge, user: @user, organization_id: @org.id, state: 2)

      post :initiate_transaction, orderable_id: recharge.id,
           orderable_type: 'recharge',
           price_id: recharge_price.id, gateway: 'braintree', format: :json

      assert_equal 2, Order.where(orderable:recharge).count
    end
  end

  class ProcessPaymentTransactionTests < ActionController::TestCase
    setup do
      @user = create(:user)
      @org = @user.organization
      api_v2_sign_in(@user)
      Transaction.any_instance.stubs(:create_wallet_transaction).returns(true)
      Transaction.any_instance.stubs(:create_card_subscription).returns(true)
      Transaction.any_instance.stubs(:create_transaction_audit).returns(true)
    end

    test 'should return error message if payment has already been successful for the order' do
      card = create(:card)
      order = create(:order, orderable: card)
      transaction = create(:transaction, order: order, payment_state: :successful)
      token = JWT.encode({payment_id: transaction.id},
                             Edcast::Application.config.secret_key_base, 'HS256')
      post :process_payment, payment_method_nonce: 'fake-valid-nonce',
           token: token, gateway: 'braintree', format: :json
      resp = get_json_response(response)
      assert_equal 'This payment has already been processed.', resp['message'].squish
    end

    class TransactionBraintreeGatewayTests < ActionController::TestCase
      setup do
        @user = create(:user)
        @org = @user.organization
        api_v2_sign_in(@user)
        organization_profile = @org.create_organization_profile!(member_pay: true)

        @org_subscription = create(:org_subscription, organization: @org)

        @org_subscription_price = @org_subscription.prices.create!(
          currency: 'INR',
          amount: BigDecimal.new("3500"))
        Transaction.any_instance.stubs(:create_wallet_transaction).returns(true)
        Transaction.any_instance.stubs(:create_card_subscription).returns(true)
      end

      test 'api should create successful transaction' do
        order = Order.initialize_order!(
          args: {orderable: @org_subscription,
          user_id: @user.id,
          organization_id: @org.id}, orderable_type: 'OrgSubscription')

        transaction = Transaction.initialize_payment!(
          order_id: order.id,
          gateway: PaymentGateway.gateway_for_currency(@org_subscription_price.currency),
          amount: @org_subscription_price.amount,
          currency: @org_subscription_price.currency,
          user_id: @user.id,
          source_name: "",
          order_type: order.orderable_type)
        transaction.run_callbacks(:commit)

        braintree_success_result =
          YAML.load_file("#{Rails.application.root}/test/fixtures/braintree_success_result.yml")

        token = JWT.encode({payment_id: transaction.id},
                               Edcast::Application.config.secret_key_base, 'HS256')

        BraintreePaymentGateway.any_instance.stubs(:authorize_payment).returns(braintree_success_result)
        Transaction.any_instance.stubs(:confirm_order).returns(true)
        BraintreePaymentGateway.any_instance.expects(:capture_payment).returns(true)

        post :process_payment, payment_method_nonce: 'fake-valid-nonce',
             token: token, gateway: 'braintree', format: :json
        resp = get_json_response(response)

        assert_equal 'Payment is Successful', resp['message']
        assert Order.last.processed?
        assert Transaction.last.successful?
        assert_equal 1, UserSubscription.count
        assert_equal 1, UserPurchase.count
        assert_equal 4, TransactionAudit.count
      end

      test 'api should create failure transaction' do
        order = Order.initialize_order!(
          args: {orderable: @org_subscription,
          user_id: @user.id,
          organization_id: @org.id}, orderable_type: 'OrgSubscription')

        transaction = Transaction.initialize_payment!(
          order_id: order.id,
          gateway: PaymentGateway.gateway_for_currency(@org_subscription_price.currency),
          amount: @org_subscription_price.amount,
          currency: @org_subscription_price.currency,
          user_id: @user.id,
          source_name: "",
          order_type: order.orderable_type)
        transaction.run_callbacks(:commit)

        braintree_failure_result =
          YAML.load_file("#{Rails.application.root}/test/fixtures/braintree_failure_result.yml")

        token = JWT.encode({payment_id: transaction.id},
                               Edcast::Application.config.secret_key_base, 'HS256')

        BraintreePaymentGateway.any_instance.stubs(:authorize_payment).returns(braintree_failure_result)

        post :process_payment, payment_method_nonce: 'fake-valid-nonce',
             token: token, gateway: 'braintree', format: :json

        resp = get_json_response(response)
        assert_equal 'Authorization failed. Credit card number is not an accepted test number.', resp['message'].squish
        assert Order.last.processing_error?
        assert Transaction.last.failed?
        assert_equal 0, UserSubscription.count
        assert_equal 0, UserPurchase.count
        assert_equal 3, TransactionAudit.count
        assert_response :unprocessable_entity
      end

      test 'should return unprocessable entity error if payment_method_nonce or token is blank' do
        post :process_payment, gateway: 'braintree', format: :json
        resp = get_json_response(response)

        assert_response :unprocessable_entity
      end

    end

    class TransactionPaypalGatewayTests < ActionController::TestCase

      setup do
        @user = create(:user)
        @org = @user.organization
        api_v2_sign_in(@user)
        @card = create :card, organization: @org, author: @user
        @price = create(:price, card: @card)

        Transaction.any_instance.stubs(:create_wallet_transaction).returns(true)
        Transaction.any_instance.stubs(:create_user_subscription).returns(true)
      end

      test 'api should create successful transaction for paypal' do
        order = Order.initialize_order!(
          args: {orderable: @card,
          user_id: @user.id,
          organization_id: @org.id}, orderable_type: 'Card')

        transaction = Transaction.initialize_payment!(
          order_id: order.id,
          gateway: 'paypal',
          amount: @price.amount,
          currency: @price.currency,
          user_id: @user.id,
          source_name: "Card",
          order_type: order.orderable_type)
        transaction.run_callbacks(:commit)

        payment_object = PayPal::SDK::REST::Payment.new(id: 'PAY-A1B2C3D4E5F7G8H9', payer_id: 'ABCDE12345')

        token = JWT.encode({payment_id: transaction.id},
                                 Edcast::Application.config.secret_key_base, 'HS256')

        PaypalPaymentGateway.any_instance.stubs(:authorize_payment).returns(payment_object)
        payment_object.stubs(:success?).returns(true)
        Transaction.any_instance.stubs(:confirm_order).returns(true)
        PaypalPaymentGateway.any_instance.stubs(:capture_payment).returns(true)

        post :process_payment, paypal_data: {payment_id: 'PAY-A1B2C3D4E5F7G8H9', payer_id: 'ABCDE12345', token: 'ABCD1234567'},
             token: token, gateway: 'paypal', format: :json
        resp = get_json_response(response)

        assert_equal 'Payment is Successful', resp['message']
        assert Order.last.processed?
        assert Transaction.last.successful?
        assert_equal 1, CardSubscription.count
        assert_equal 4, TransactionAudit.count
      end

      test 'api should create failure transaction for paypal' do
        order = Order.initialize_order!(
          args:{orderable: @card,
          user_id: @user.id,
          organization_id: @org.id}, orderable_type: 'Card')

        transaction = Transaction.initialize_payment!(
          order_id: order.id,
          gateway: 'paypal',
          amount: @price.amount,
          currency: @price.currency,
          user_id: @user.id,
          source_name: "Card",
          order_type: order.orderable_type)
        transaction.run_callbacks(:commit)

        payment_object = PayPal::SDK::REST::Payment.new(id: 'PAY-A1B2C3D4E5F7G8H9', payer_id: 'ABCDE12345')
        PaypalPaymentGateway.any_instance.stubs(:get_authorization_object).returns(PayPal::SDK::REST::Authorization.new)

        token = JWT.encode({payment_id: transaction.id},
                                 Edcast::Application.config.secret_key_base, 'HS256')

        PaypalPaymentGateway.any_instance.stubs(:authorize_payment).returns(payment_object)
        payment_object.stubs(:success?).returns(false)

        post :process_payment, paypal_data: {payment_id: 'PAY-A1B2C3D4E5F7G8H9', payer_id: 'ABCDE12345', token: 'ABCD1234567'},
             token: token, gateway: 'paypal', format: :json
        resp = get_json_response(response)

        assert_equal 'Authorization failed.', resp['message'].squish
        assert Order.last.processing_error?
        assert Transaction.last.failed?
        assert_equal 0, CardSubscription.count
        assert_equal 3, TransactionAudit.count
      end

      test 'should return unprocessable entity error if paypal data or token is blank' do
        post :process_payment, gateway: 'paypal', format: :json
        resp = get_json_response(response)

        assert_response :unprocessable_entity
      end
    end

  end

end

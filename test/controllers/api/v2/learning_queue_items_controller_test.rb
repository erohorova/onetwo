require 'test_helper'

class Api::V2::LearningQueueItemsControllerTest < ActionController::TestCase
  setup do
    Assignment.any_instance.stubs(:schedule_due_notifications).returns true
    org = create(:organization)
    @user1, user2 = create_list(:user, 2, organization: org)

    link_resource = create(:resource, type: 'Article')
    @media_card = create(:card, card_type: 'media', card_subtype: 'link', resource_id: link_resource.id, author: user2)
    @poll_card = create(:card, card_type: 'poll', card_subtype: 'text', author: user2)
    @pack_card = create(:card, card_type: 'pack', card_subtype: 'simple', author: user2)
    @video_resource = create :resource,
      url: 'https://www.ted.com/talks/trevor_copp_jeff_fox_ballroom_dance_that_breaks_gender_roles',
      type: 'Video'
    @link_video_card = create(:card, card_type: 'media', card_subtype: 'video', resource: @video_resource, author: user2)
    @link_video_card.reload

    @user1_queue = []
    @user1_queue << create(:learning_queue_item, :from_assignments, queueable: @media_card, user: @user1)
    @user1_queue << create(:learning_queue_item, :from_assignments, queueable: @poll_card, user: @user1)
    @user1_queue << create(:learning_queue_item, :from_assignments, queueable: @pack_card, user: @user1)
    @user1_queue << create(:learning_queue_item, :from_assignments, queueable: @link_video_card, user: @user1)

    @video_stream = create(:iris_video_stream, creator: user2)
    @user1_queue << create(:learning_queue_item, :from_bookmarks, queueable: @video_stream.card, user: @user1)
    @bookmark = create(:bookmark, bookmarkable: @video_stream.card, user: @user1)
    another_media_card = create(:card, card_type: 'media', card_subtype: 'link', author: user2)

    create(:learning_queue_item, :from_bookmarks, queueable: another_media_card, user: user2)

    IrisVideoStream.any_instance.stubs(:populate_meta_data).returns(nil)
    @vs = create(:iris_video_stream, status: 'live', creator_id: user2, organization_id: user2.organization_id)
    @user1_queue << create(:learning_queue_item, :from_assignments, queueable: @vs.card, user: @user1)

    @media_card_assignment = create(:assignment, assignable: @media_card, assignee: @user1, due_at: Time.current + 3.days)
    @vs_assignment = create(:assignment, assignable: @vs.card, assignee: @user1, due_at: Time.current + 2.days)

    ta1 = create(:team_assignment, assignment: @media_card_assignment, assignor: user2)
    ta2 = create(:team_assignment, assignment: @vs_assignment, assignor: user2)

    api_v2_sign_in @user1
  end

  test ':api index' do
    get :index, format: :json
    assert_response :ok

    resp = get_json_response(response)

    assert_same_elements @user1_queue.map(&:id), resp['learning_queue_items'].map{|r| r['id']}
  end

  test 'deleted card should not show up' do
    ContentsBulkDequeueJob.stubs(:perform_later)
    Card.any_instance.expects(:delete_ecl_card)

    deleted_card_item = @user1_queue.first
    deleted_card_item.queueable.destroy

    assert_equal LearningQueueItem::DELETION_STATE, deleted_card_item.reload.state

    # Getting everything
    get :index, include_queueable_details: 'true', format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_same_elements @user1_queue.map(&:id), resp['learning_queue_items'].map{|r| r['id']}

    # Exclude deleted
    get :index, include_queueable_details: 'true', state: ["active", "completed"], format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_same_elements @user1_queue.reject{|item| item.id == deleted_card_item.id}.map(&:id), resp['learning_queue_items'].map{|r| r['id']}
  end

  test 'hidden card should not show up' do
    Card.any_instance.stubs(:reindex).returns true

    hidden_card = create(:card, author_id: @user1.id, hidden: 1)
    external_card = create(:card, author_id: @user1.id)
    CardPackRelation.add(cover_id: @pack_card.id, add_id: hidden_card.id, add_type: 'Card')
    CardPackRelation.add(cover_id: @pack_card.id, add_id: external_card.id, add_type: 'Card')
    # # Getting everything
    @user1_queue << create(:learning_queue_item, :from_assignments, queueable: hidden_card, user: @user1)
    @user1_queue << create(:learning_queue_item, :from_assignments, queueable: external_card, user: @user1)
    get :index, include_queueable_details: 'true', format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_includes resp['learning_queue_items'].map{|d| d['queueable']['id'].to_i}, external_card.id
    assert_not_includes resp['learning_queue_items'].map{|d| d['queueable']['id'].to_i}, hidden_card.id
  end

  test 'deleted stream should not show up' do
    Card.any_instance.expects(:delete_ecl_card)
    deleted_video_stream_item = @user1_queue.first
    deleted_video_stream_item.queueable.destroy
    assert_equal LearningQueueItem::DELETION_STATE, deleted_video_stream_item.reload.state

    get :index, state: ["active", "completed"], format: :json
    assert_response :ok

    resp = get_json_response(response)
    assert_same_elements @user1_queue.reject{|item| item.id == deleted_video_stream_item.id}.map(&:id), resp['learning_queue_items'].map{|r| r['id']}
  end

  test 'learning queue items only for existing bookmarks should be returned' do
    bookmarked_and_completed = create(:card, author: @user1)
    create(:bookmark, bookmarkable: bookmarked_and_completed, user: @user1)
    create(:learning_queue_item, :from_bookmarks, queueable: bookmarked_and_completed, user: @user1)
    # bookmarked item is marked as complete
    @user1.learning_queue_items.find_by(queueable: bookmarked_and_completed).update(state: 'completed')
    create(:user_content_completion, :completed, completable: bookmarked_and_completed, user: @user1, )

    #bookmark of bookmarked_and_completed is removed
    @user1.bookmarks.find_by(bookmarkable: bookmarked_and_completed).destroy

    get :index, include_queueable_details: 'true', source: ['bookmarks'], format: :json
    assert_response :ok
    resp = get_json_response(response)

    # bookmark learning queue item converted to source: assignment and state: completed
    assert_not_includes resp['learning_queue_items'].map{|d| d['queueable']['id'].to_i}, bookmarked_and_completed.id
    assert_includes resp['learning_queue_items'].map{|d| d['queueable']['id'].to_i}, @video_stream.card_id
  end

  test 'should return learning queue items for bookmarks according to search query' do
    org = create(:organization)
    user = create(:user, organization: org)
    api_v2_sign_in user
    bookmarked1 = create(:card, author: user, message: 'Low frequency emission')
    bookmarked2 = create(:card, author: user, message: 'High resolution fiber channel')
    create(:bookmark, bookmarkable: bookmarked1, user: user)
    create(:bookmark, bookmarkable: bookmarked2, user: user)
    create(:learning_queue_item, :from_bookmarks, queueable: bookmarked1, user: user)
    create(:learning_queue_item, :from_bookmarks, queueable: bookmarked2, user: user)
    Search::CardSearch.
      any_instance.
      expects(:search).
      with(
        'freque',
        org,
        viewer: user,
        filter_params: { id: [bookmarked1.id, bookmarked2.id] },
        load: false
      ).
      returns(Search::CardSearchResponse.new(results: [bookmarked1]), total: 1)

    get :index, include_queueable_details: 'true', source: ['bookmarks'], query: 'freque', format: :json
    assert_response :ok
    resp = get_json_response(response)['learning_queue_items']

    assert resp.all? { |lqi| lqi['queueable']['id'] === bookmarked1.id.to_s }
  end

  test 'api index with include item details set to true' do
    get :index, include_queueable_details: 'true', format: :json
    assert_response :ok

    resp = get_json_response(response)
    assert_same_elements @user1_queue.map(&:queueable_id).map(&:to_s).to_set, resp['learning_queue_items'].map{|r| r['queueable']['id']}.to_set

    assert_not_empty resp['learning_queue_items'].select{|r| r['queueable']['id'] == @media_card_assignment.assignable.id.to_s}.first['queueable']['assignment']
    assert_not_empty resp['learning_queue_items'].select{|r| r['queueable']['id'] == @vs_assignment.assignable.id.to_s}.first['queueable']['assignment']
  end

  test 'api should be able to filter by source' do
    get :index, source: ['assignments'], format: :json
    assert_response :ok

    resp = get_json_response(response)
    assert_equal 5, resp['learning_queue_items'].count
  end

  test ':api should be able to filter by content type' do
    get :index, content_type: ['card'], format: :json
    assert_response :ok

    resp = get_json_response(response)
    assert_equal 3, resp['learning_queue_items'].count

    get :index, content_type: ['video_stream'], format: :json
    assert_response :ok

    resp = get_json_response(response)
    assert_equal 2, resp['learning_queue_items'].count

    get :index, content_type: ['collection'], format: :json
    assert_response :ok

    resp = get_json_response(response)
    assert_equal 1, resp['learning_queue_items'].count
  end


  test 'api should be able to filter by state' do
    @user1.learning_queue_items.last.update(state: 'completed')

    # active
    get :index, state: ['active'], format: :json
    assert_response :ok

    resp = get_json_response(response)
    assert_equal 5, resp['learning_queue_items'].count

    # completed
    get :index, state: ['completed'], format: :json
    assert_response :ok

    resp = get_json_response(response)
    assert_equal 1, resp['learning_queue_items'].count
  end

  test 'should support limit and offset' do
    get :index, limit: 3, offset: 1, format: :json
    assert_response :ok

    resp = get_json_response(response)
    assert_equal 3, resp['learning_queue_items'].count
  end

  test 'should return learning_queue_items source bookmarks and pinned learning_queue_item in first position for current_user' do
    bookmark = create(:bookmark, bookmarkable: @pack_card, user: @user1)

    Pin.create(pinnable: bookmark, object: @pack_card, user: @user1)

    get :index, include_queueable_details: 'true', source: ['bookmarks'], format: :json

    assert_response :ok

    resp = get_json_response(response)
    assert_equal 2, resp['learning_queue_items'].count
    assert_equal @pack_card.id.to_s, resp['learning_queue_items'][0]['queueable']['id']
    assert_equal @video_stream.card.id.to_s, resp['learning_queue_items'][1]['queueable']['id']
    assert_equal @pack_card.id,  resp['pinnable_card_id']
  end

  test 'should return pinned learning_queue_item in first position if source bookmarks for current_user with query' do
    bookmark = create(:bookmark, bookmarkable: @pack_card, user: @user1)

    Pin.create(pinnable: bookmark, object: @pack_card, user: @user1)

    Search::CardSearch.any_instance.expects(:search).with(
        'qwe',
        @user1.organization,
        viewer: @user1,
        filter_params: { id: [@pack_card.id, @video_stream.card.id] },
        load: false
      ).returns(Search::CardSearchResponse.new(results: []), total: 0)

    get :index, include_queueable_details: 'true', source: ['bookmarks'], query: 'qwe', format: :json

    assert_response :ok

    resp = get_json_response(response)
    assert_equal 1, resp['learning_queue_items'].count
    assert_equal @pack_card.id.to_s, resp['learning_queue_items'][0]['queueable']['id']
    assert_equal @pack_card.id, resp['pinnable_card_id']
  end

  test 'should return bookmark_id in LQI for source bookmark' do
    get :index, include_queueable_details: 'true', source: ['bookmarks'], format: :json

    assert_response :ok

    resp = get_json_response(response)
    assert_equal 1, resp['learning_queue_items'].count
    assert_equal @video_stream.card.id.to_s, resp['learning_queue_items'][0]['queueable']['id']
    assert_equal @bookmark.id, resp['learning_queue_items'][0]['bookmark_id']
    assert_nil resp['pinnable_card_id']
  end

  test 'should not return bookmark_id in LQI for any source' do
    get :index, include_queueable_details: 'true', source: ['assignments'], format: :json

    assert_response :ok

    resp = get_json_response(response)
    refute resp['learning_queue_items'][0].key? 'bookmark_id'
  end
end

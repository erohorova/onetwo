require 'test_helper'

class Api::V2::OrgSsosControllerTest < ActionController::TestCase
  setup do
    stub_okta_group_creation
    stub_okta_app_creation
    stub_okta_group_app_link
    stub_okta_idp_creation
    @org     = create :organization
    @admin   = create :user, :admin, organization: @org, organization_role: 'admin'
    @member  = create :user, organization: @org
    @sso     = create :sso, name: 'lxp_oauth'

    @org.configs.create(name: 'OktaApiToken', value: SecureRandom.hex)
    @org.configs.create(name: 'app.config.okta.enabled', value: "true")

    @params = {
      oauth_redirect_uri: 'http://test-app.lvh.me:4000/auth/lxp_oauth/callback',
      oauth_login_url: 'https://dev-579575.oktapreview.com/',
      oauth_authorize_url: '/oauth2/default/v1/authorize',
      oauth_token_url: '/oauth2/default/v1/token',
      oauth_user_info_url: '/oauth2/default/v1/userinfo',
      label_name: 'Sign in with email',
      oauth_client_id: '0oad0ipbmpTemalZO0h7',
      oauth_client_secret: 'JZzitxUG-ojLtzHIKgpIT-7Ze7LPwAuXd9lpi_4d',
      oauth_scopes: 'openid email profile',
      idp: '0oad0cm6arcDtqmso0h7',
      provider_name: 'facebook'
    }

    @org_sso = @sso.org_ssos.build(@params)
    @org_sso.organization = @org
    @org_sso.save
  end

  test ':api creates org_sso' do
    org     = create :organization
    admin   = create :user, :admin, organization: org, organization_role: 'admin'
    member  = create :user, organization: org

    api_v2_sign_in(admin)


    org.configs.create(name: 'OktaApiToken', value: SecureRandom.hex)
    org.configs.create(name: 'app.config.okta.enabled', value: "true")

    post :create, org_sso: @params, sso_name: 'lxp_oauth', format: :json

    result = get_json_response(response)['org_sso']
    assert_equal @params[:oauth_redirect_uri], result['saml_assertion_consumer_service_url']
    assert_equal @params[:oauth_login_url], result['oauth_login_url']
    assert_equal @params[:oauth_authorize_url], result['oauth_authorize_url']
    assert_equal @params[:oauth_token_url], result['oauth_token_url']
    assert_equal @params[:oauth_user_info_url], result['oauth_user_info_url']
    assert_equal @params[:label_name], result['label_name']
    assert_equal @params[:oauth_client_id], result['oauth_client_id']
    assert_equal @params[:oauth_client_secret], result['oauth_client_secret']
    assert_equal @params[:oauth_scopes], result['oauth_scopes']
    assert_equal @params[:idp_id], result['idp']
    assert_equal @params[:provider_name], result['provider_name']
  end

  test '#create revokes access to member' do
    api_v2_sign_in(@member)

    post :create, org_sso: @params, sso_id: @sso.id, format: :json

    assert_response :unauthorized
  end

  test ':api updates org_sso' do
    api_v2_sign_in(@admin)

    put :update, org_sso: @params.merge(label_name: 'Updated label_name'), id: @org_sso.id, format: :json

    result = get_json_response(response)['org_sso']
    assert_equal @params[:oauth_redirect_uri], result['saml_assertion_consumer_service_url']
    assert_equal @params[:oauth_login_url], result['oauth_login_url']
    assert_equal @params[:oauth_authorize_url], result['oauth_authorize_url']
    assert_equal @params[:oauth_token_url], result['oauth_token_url']
    assert_equal @params[:oauth_user_info_url], result['oauth_user_info_url']
    assert_equal 'Updated label_name', result['label_name']
    assert_equal @params[:oauth_client_id], result['oauth_client_id']
    assert_equal @params[:oauth_client_secret], result['oauth_client_secret']
    assert_equal @params[:oauth_scopes], result['oauth_scopes']
    assert_equal @params[:idp_id], result['idp']
    assert_equal @params[:provider_name], result['provider_name']
  end

  test '#update revokes access to member' do
    api_v2_sign_in(@member)

    put :update, org_sso: @params.merge(label_name: 'Updated label_name'), id: @org_sso.id, format: :json

    assert_response :unauthorized
  end

  test ':api toggles `:is_enabled`' do
    api_v2_sign_in(@admin)

    put :toggle, org_sso: { is_enabled: 'true' }, id: @org_sso.id, format: :json

    assert_response :ok

    result = get_json_response(response)['org_sso']
    assert result['is_enabled']
  end

  test '#toggle revokes access to member' do
    api_v2_sign_in(@member)

    put :toggle, org_sso: { is_enabled: 'true' }, id: @org_sso.id, format: :json

    assert_response :unauthorized
  end

  test 'should return is_visible false when org_sso is updated' do
    api_v2_sign_in(@admin)

    # testing the default value of is_visible
    assert @org_sso.is_visible

    put :update, org_sso: @params.merge(is_visible: false), id: @org_sso.id, format: :json

    # testing if is_visible is off
    refute @org_sso.reload.is_visible
  end

  test 'should multiple update and create org_sso with params position and is_enabled' do
    api_v2_sign_in(@admin)
    sso = create :sso, name: 'google'
    params = {
      update_org_ssos: [
        { id: @org_sso.id, position: 3, is_enabled: true },
        { id: @org_sso.id + 4, position: 4, is_enabled: false }
      ],
      create_org_ssos: [
        { sso_name: 'google', position: 5, is_enabled: false }
      ]
    }
    assert_difference ->{ OrgSso.count }, 1 do
      put :batch_update, params, format: :json
    end

    resp = get_json_response(response)

    new_org_sso = sso.org_ssos.first.reload
    @org_sso.reload
    assert resp['create_org_ssos']
    assert_equal 'OrgSso record not found', resp['update_org_ssos']['1']
    assert_equal [3, true], [@org_sso.position, @org_sso.is_enabled]
    assert_equal [5, false], [new_org_sso.position, new_org_sso.is_enabled]
  end

  test 'should return error for create org ssos if sso not found' do
    api_v2_sign_in(@admin)

    params = {
      create_org_ssos: [
        { sso_name: 'google', position: 5, is_enabled: false }
      ]
    }
    assert_no_difference ->{ OrgSso.count } do
      put :batch_update, params, format: :json
    end
    resp = get_json_response(response)

    assert 'Sso record not found', resp['create_org_ssos']['0']
  end

  test 'should return error for create org ssos if invalid format' do
    api_v2_sign_in(@admin)

    params = {
      create_org_ssos: ''
    }
    put :batch_update, params, format: :json
    resp = get_json_response(response)

    assert 'params create_org_ssos must be Array', resp['message']
  end
end

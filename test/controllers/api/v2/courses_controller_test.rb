require 'test_helper'

class Api::V2::CoursesControllerTest < ActionController::TestCase
  test ":api fetch_external_courses" do
    CoursesScraperJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    user = create(:user)
    CoursesScraperJob.expects(:perform_later).once.returns(true)
    CoursesScraperJob.expects(:perform_later).once.returns(true)
    CoursesScraperJob.expects(:perform_later).once.returns(true)
    CoursesScraperJob.expects(:perform_later).once.returns(true)

    post :refresh_external_courses, fields: {external_uid: "11222"}, provider: "Coursera", format: :json

    assert_response :unauthorized

    api_v2_sign_in user

    #without provider
    post :refresh_external_courses, fields: {external_uid: "11222"}, format: :json
    assert_response 422

    #without fields
    post :refresh_external_courses, format: :json
    assert_response 422

    post :refresh_external_courses, fields: {external_uid: "11222"}, provider: "Coursera", format: :json
    assert_response :ok

    post :refresh_external_courses, fields: {external_uid: "11222"}, provider: "Udemy", format: :json
    assert_response :ok

    post :refresh_external_courses, fields: {external_uid: "11222"}, provider: "Future Learn", format: :json
    assert_response :ok

    post :refresh_external_courses, fields: {external_uid: "11222"}, provider: "Skill Share", format: :json
    assert_response :ok
  end

  class ExportActionTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      @client = create(:client, organization: @org)
      @org_admin = create(:user, organization_role: 'admin', organization: @org)
      api_v2_sign_in(@org_admin)

      5.times do
        create :resource_group, creator: @org_admin, organization_id: @org.id, client: @client
      end

      2.times do
        create :group, creator: @org_admin, organization_id: @org.id, type: 'PrivateGroup', client: @client
      end
    end

    test 'considers only resource groups' do
      ExportCoursesJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later

      resource_groups = @org.resource_groups
      private_groups  = @org.groups.where(type: 'PrivateGroup')

      ExportResourceGroups.any_instance.expects(:run).once

      get :export, course_ids: @org.groups.pluck(:client_resource_id), format: :json

      refute_includes assigns(:groups).map(&:id), private_groups[0].id
      refute_includes assigns(:groups).map(&:id), private_groups[1].id
      assert_equal assigns(:groups).map(&:id).to_set, resource_groups.pluck(:id).to_set
    end

    test 'unauthorizes for non-admin user' do
      user = create(:user, organization_role: 'member', organization: @org)
      api_v2_sign_in(user)

      get :export, course_ids: @org.groups.pluck(:client_resource_id), format: :json

      assert_response :unauthorized
    end
  end
end

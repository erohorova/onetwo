require 'test_helper'

class Api::V2::OrganizationsControllerTest < ActionController::TestCase
  test ':api should get org details for non okta enabled org' do
    create_default_ssos

    saml = create :sso, :saml
    org = create(:organization, host_name: 'a', custom_domain: 'custom.example.com')
    set_host org

    org.ssos << Sso.all

    get :show, format: :json

    assert_response :ok
    resp = get_json_response(response)
    assert_equal 'custom.example.com', resp['custom_domain']

    assert_equal ['email', 'google', 'linkedin', 'facebook', 'saml'].to_set, resp['ssos'].map{ |sso| sso['name'] }.to_set
    assert_equal ['email', 'google', 'linkedin', 'facebook', 'saml'].to_set, resp['ssos'].map{ |sso| sso['sso_name'] }.to_set
  end

  test ':api should get org details okta enabled org' do
    # Organization.any_instance.unstub(:default_sso)
    stub_okta_group_creation
    stub_okta_app_creation
    stub_okta_idp_creation
    stub_okta_group_app_link

    create_default_ssos

    lxp_oauth = Sso.find_by(name: 'lxp_oauth')

    saml = create :sso, :saml
    org = create(:organization, host_name: 'a', custom_domain: 'custom.example.com')
    config = create :config, configable: org, name: 'OktaApiToken',
      value: { 'okta_domain': Settings.okta.domain, 'client_id': 'ABC123', 'client_secret': 'ABC123' }.to_json,
      data_type: 'json'
    Organization.any_instance.stubs(:linked_okta_app).returns(config)
    set_host org

    org.ssos << saml

    org.configs.create(name: 'app.config.okta.enabled', value: "true")
    org.configs.create(name: 'OktaApiToken', value: SecureRandom.hex)
    create :org_sso, organization: org, sso: lxp_oauth, provider_name: 'salesforce'
    create :org_sso, organization: org, sso: lxp_oauth, provider_name: 'saml'

    get :show, format: :json

    assert_response :ok
    resp = get_json_response(response)
    assert_equal 'custom.example.com', resp['custom_domain']
    assert_equal ['salesforce', 'saml'].to_set, resp['ssos'].map{ |sso| sso['name'] }.to_set
    assert_equal ['lxp_oauth'].to_set, resp['ssos'].map{ |sso| sso['sso_name'] }.uniq.to_set
  end

  test 'retrieves active SSOs considering customised label name' do
    Organization.any_instance.unstub(:default_sso)
    create_default_ssos
    facebook = Sso.where(name: "facebook").first!
    saml = create(:sso, name: 'saml')
    org_oauth = create(:sso, name: 'org_oauth')
    a = create(:organization, host_name: 'a')
    set_host a
    saml_sso = create :org_sso, organization: a, sso: saml, label_name: 'Login with SAML credentials'
    oo_sso = create :org_sso, organization: a, sso: org_oauth, is_enabled: false

    a.reload
    get :show, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_same_elements ['facebook', saml_sso.label_name, 'google', 'linkedin', 'email'].sort, resp['ssos'].collect{ |s| s['label_name'] }.sort
  end


  test 'should retrieve active and visible SSOs only in response' do
    create_default_ssos
    org = create :organization, host_name: 'qa'
    Organization.stubs(:find_by_request_host).returns(org)

    # Find ssos
    google = Sso.find_by(name: 'google')
    lxp_oauth = Sso.find_by(name: 'lxp_oauth')

    # Create org_ssos
    legacy_google_sso = create :org_sso, organization: org, sso: google, label_name: 'Google', is_visible: false
    lxp_oauth_sso = create :org_sso, organization: org, sso: lxp_oauth, label_name: 'Okta SAML', is_visible: false

    get :show, format: :json
    assert_response :ok
    resp = get_json_response(response)

    # Even if org_ssos are enabled they are not in response as they are not visible
    assert legacy_google_sso.is_enabled
    assert lxp_oauth_sso.is_enabled
    assert_empty resp['ssos']
  end

  # should only return pathways with assessments
  test 'returns data for assessments' do
    org = create(:organization)
    user = create(:user, organization_id: org.id)

    api_v2_sign_in user
    cover, cover1 = create_list(:card, 2, author: user, organization_id: org.id, card_type: 'pack')

    user1 = create(:user, organization_id: org.id)
    user2 = create(:user, organization_id: org.id)

    card = create(:card, card_type: 'poll', author: user, organization_id: org.id)
    option1 = create(:quiz_question_option, label: 'a', quiz_question: card, is_correct: true)
    option2 = create(:quiz_question_option, label: 'b', quiz_question: card)

    card1 = create(:card, card_type: 'poll', author: user, organization_id: org.id)
    option3 = create(:quiz_question_option, label: 'c', quiz_question: card1, is_correct: true)
    option4 = create(:quiz_question_option, label: 'd', quiz_question: card1)
    option5 = create(:quiz_question_option, label: 'e', quiz_question: card1)

    card2 = create(:card, card_type: 'poll', author: user, organization_id: org.id)
    option6 = create(:quiz_question_option, label: 'f', quiz_question: card2, is_correct: true)
    option7 = create(:quiz_question_option, label: 'g', quiz_question: card2)

    CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    CardPackRelation.add(cover_id: cover.id, add_id: card1.id, add_type: 'Card')
    CardPackRelation.add(cover_id: cover1.id, add_id: card2.id, add_type: 'Card')

    attempt = create(:quiz_question_attempt, user: user1, quiz_question: card, selections: [option1.id])
    attempt1 = create(:quiz_question_attempt, user: user2, quiz_question: card, selections: [option1.id])
    attempt2 = create(:quiz_question_attempt, user: user1, quiz_question: card1, selections: [option3.id])
    attempt3 = create(:quiz_question_attempt, user: user2, quiz_question: card1, selections: [option4.id])
    attempt4 = create(:quiz_question_attempt, user: user2, quiz_question: card2, selections: [option6.id])

    get :assessments, format: :json
    resp = get_json_response(response)

    assert_equal 2, resp['assessment_details'].count
    resp['assessment_details'].first.assert_valid_keys("id", "title", "completion_count", "attempted_count","assignment_url","average_percentage")
    resp['assessment_details'].second.assert_valid_keys("id", "title", "completion_count", "attempted_count","assignment_url","average_percentage")
  end

  test 'filter data for assessments by search term' do
    org = create(:organization)
    user = create(:user, organization_id: org.id)

    api_v2_sign_in user
    cover = create(:card, author: user, organization_id: org.id, card_type: 'pack', title: 'First')
    cover1 = create(:card, author: user, organization_id: org.id, card_type: 'pack', title: 'Second')

    user1 = create(:user, organization_id: org.id)
    user2 = create(:user, organization_id: org.id)

    card = create(:card, card_type: 'poll', author: user, organization_id: org.id)
    option1 = create(:quiz_question_option, label: 'a', quiz_question: card, is_correct: true)
    option2 = create(:quiz_question_option, label: 'b', quiz_question: card)

    card1 = create(:card, card_type: 'poll', author: user, organization_id: org.id)
    option3 = create(:quiz_question_option, label: 'c', quiz_question: card1, is_correct: true)
    option4 = create(:quiz_question_option, label: 'd', quiz_question: card1)

    card2 = create(:card, card_type: 'poll', author: user, organization_id: org.id)
    option5 = create(:quiz_question_option, label: 'f', quiz_question: card2, is_correct: true)
    option6 = create(:quiz_question_option, label: 'g', quiz_question: card2)

    CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    CardPackRelation.add(cover_id: cover.id, add_id: card1.id, add_type: 'Card')
    CardPackRelation.add(cover_id: cover1.id, add_id: card2.id, add_type: 'Card')

    attempt = create(:quiz_question_attempt, user: user1, quiz_question: card, selections: [option1.id])
    attempt1 = create(:quiz_question_attempt, user: user2, quiz_question: card, selections: [option1.id])
    attempt2 = create(:quiz_question_attempt, user: user1, quiz_question: card1, selections: [option3.id])
    attempt3 = create(:quiz_question_attempt, user: user2, quiz_question: card1, selections: [option4.id])
    attempt4 = create(:quiz_question_attempt, user: user2, quiz_question: card2, selections: [option5.id])

    get :assessments, q: 'second', format: :json
    resp = get_json_response(response)

    assert_equal 1, resp['assessment_details'].count
    assert_equal resp['assessment_details'].first['id'], cover1.id
  end

  test 'should returns cards for group_sub_admin user' do
    org = create(:organization)
    user = create(:user, organization_id: org.id)
    team = create(:team, organization: org)
    Role.create_standard_roles org
    role_member = org.roles.find_by(name: 'member')
    create(:role_permission, role: role_member, name: Permissions::MANAGE_GROUP_ANALYTICS)
    role_member.add_user(user)
    team.add_sub_admin(user)
    api_v2_sign_in user

    cover, cover1 = create_list(:card, 2, author: user, organization_id: org.id, card_type: 'pack')

    user1 = create(:user, organization_id: org.id)
    user2 = create(:user, organization_id: org.id)

    card = create(:card, card_type: 'poll', author: user, organization_id: org.id)
    option1 = create(:quiz_question_option, label: 'a', quiz_question: card, is_correct: true)
    option2 = create(:quiz_question_option, label: 'b', quiz_question: card)

    card1 = create(:card, card_type: 'poll', author: user, organization_id: org.id)
    option3 = create(:quiz_question_option, label: 'c', quiz_question: card1, is_correct: true)
    option4 = create(:quiz_question_option, label: 'd', quiz_question: card1)
    option5 = create(:quiz_question_option, label: 'e', quiz_question: card1)

    CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    CardPackRelation.add(cover_id: cover1.id, add_id: card1.id, add_type: 'Card')

    attempt = create(:quiz_question_attempt, user: user1, quiz_question: card, selections: [option1.id])
    attempt1 = create(:quiz_question_attempt, user: user2, quiz_question: card, selections: [option1.id])
    attempt2 = create(:quiz_question_attempt, user: user1, quiz_question: card1, selections: [option1.id])
    attempt3 = create(:quiz_question_attempt, user: user2, quiz_question: card1, selections: [option1.id])

    get :assessments, format: :json
    resp = get_json_response(response)

    assert_equal 2, resp['assessment_details'].count
    assert_equal cover.id, resp['assessment_details'].first['id']
  end

  test 'include cobranding details' do
    org1 = create(:organization, name: 'a', host_name: 'a')
    org1.configs.create(category: 'cobrand', name: 'label/insight', value: 'smartbit')
    org1.configs.create(category: 'cobrand', name: 'label/something/else', value: 'somevalue')
    set_host org1

    get :show, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_not_empty resp['cobrands']
    assert_equal 'smartbit', resp['cobrands']['label/insight']
    assert_equal 'somevalue', resp['cobrands']['label/something/else']
  end

  test 'include ssos details' do
    Organization.any_instance.unstub(:default_sso)

    org1 = create(:organization, name: 'a', host_name: 'a')
    set_host(org1) # required when user authentication is not required and
                   # need to find current org in controller action

    saml = create(:sso, name: 'saml')
    org1.ssos << saml

    get :show, format: :json

    assert_response :ok
    resp = get_json_response(response)
    assert_equal 'a', resp['name']
    assert_not_nil resp['image_url']
    assert_not_nil resp['banner_url']
    assert_equal 'http://a.test.host', resp['home_page']
    assert_not_empty resp['ssos']

    assert_includes resp['ssos'].map{|sso| sso.except("web_authentication_url")}, {"name"=>"saml",
                                   "label_name"=>"saml",
                                   "icon_url"=>"http://cdn-test-host.com/assets/sso/saml.png",
                                   "authentication_url"=>"http://#{org1.host}/auth/saml",
                                   "sso_name" => 'saml', "position" => 1}
    assert_equal 1, resp['ssos'].length
  end

  test 'include org profile details' do
    skip # NO IDEA WHY THIS IS FAILING, SKIPPING, PLEASE FIX
    org1 = create(:organization, name: 'a', host_name: 'a')
    profile = create(:organization_profile, member_pay: true, organization: org1)
    org_price = create(:organization_price, organization: org1, amount: 123)

    set_host org1

    get :show, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert resp["member_pay"]
    assert_not_empty resp["prices"]
  end

  test 'include org mailer_config from_address' do
    org1 = create(:organization, name: 'a', host_name: 'a')
    mailer_config = create(:mailer_config, organization: org1, from_address: 'test@edcast.com')

    set_host org1

    get :show, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal 'test@edcast.com', resp['from_address']
  end

  test 'include org default from_address if mailer_config not present' do
    org1 = create(:organization, name: 'a', host_name: 'a')

    set_host org1

    get :show, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal 'admin@edcast.com', resp['from_address']
  end

  test 'should return position for ssos' do
    Organization.any_instance.unstub(:default_sso)
    create_default_ssos
    saml = create(:sso, name: 'saml')
    org_oauth = create(:sso, name: 'org_oauth')
    a = create(:organization, host_name: 'a')
    set_host a
    create :org_sso, organization: a, sso: saml, position: 2
    create :org_sso, organization: a, sso: org_oauth, is_enabled: false, position: 3

    a.reload
    get :show, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_same_elements [1,1,1,1,2], resp['ssos'].map{ |s| s['position'] }
  end

  test 'should put#update with errors' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'member')

    api_v2_sign_in user

    put :update, id: org.id, organization: {}, format: :json
    assert_response :unauthorized
  end

  test 'put#update for email' do
    org    = create(:organization, send_weekly_activity: false)
    user   = create(:user, organization: org, organization_role: 'admin')

    api_v2_sign_in user

    put :update, id: org.id, organization: {send_weekly_activity: true}, format: :json
    org.reload
    assert_equal true, org.send_weekly_activity
    assert_response :ok

    put :update, id: org.id, organization: {send_weekly_activity: false}, format: :json
    org.reload
    assert_equal false, org.send_weekly_activity
    assert_response :ok
  end

  test ':api should put#update form data' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'admin')
    org.configs << (config = create :config)

    api_v2_sign_in user

    image_url = "http://example.com/logo.jpg"
    stub_request(:get, image_url).to_return( :status => 200,
      :body => File.read('test/fixtures/images/test.png'),
      :headers => {'content-type':'image/png'})

    put :update, id: org.id, organization: { is_open: true, name: "new-name",
      description: "Edcast 2.0 org", image: image_url, mobile_image: image_url,
      host_name: 'www3', configs_attributes: [{ id: nil, name: "content_life_span", value: "90" },
      { id: config.id, value: "100" }], show_onboarding: false},
      format: :json

    resp = get_json_response(response)
    assert_not_empty resp
    assert_equal resp["id"], org.id
    assert_equal resp["name"], 'new-name'
    assert_not_equal resp["host_name"], 'www3' #host name cannot be updated
    assert_includes resp["configs"].map{|x| x["name"]}, "content_life_span" # new config added
    assert_equal config.reload.value, "100" # update existing config

    assert_equal false, org.reload.show_onboarding
    assert_equal false, resp['show_onboarding']
    assert_equal true, resp['is_onboarding_configurable']
  end

  test ':api put#update updates paperclip attrs - favicon, splash_image, co_branding_logo, mobile_image, image' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'admin')
    org.configs << (config = create :config)

    api_v2_sign_in user

    Settings.stubs(:serve_authenticated_images).returns("true")
    stub_request(:get, /https:\/\/cdn.filestackcontent.com\//).to_return( :status => 200,
      :body => File.read('test/fixtures/images/test.png'),
      :headers => {'content-type':'image/png'})

    mobile_image_url = "#{org.host}/uploads/3c21fc757847b85ac2c64e1f9bd3c0fb:dde45bd29e095e7067a3fa352283f23f6f44e4d5696580788d1d48b3243d278d2316996b6b007c8f3a4e772088a6461c374bea8e257466c38a10752eefbb427a64ffc090b32aeac4a549f8b265ee478280cfcac66883f2f42893b8fd2ecd85f1469796f08697448ccc65cca9bc6b9f6a1906be149733036a0e36474be12229a2074aa54083841bd04e8d90316e0fdff4d3e53d39e00666137969e89eb3b72e12eb61efcbf62dc0ef9ab43ae6d3b00449b9af69ea273fd5fa82613b5944f41c8f0f88fbc4ce37112e6fc3bcc21c288997d9e81dc52b313ac6946117dd010665ff383a9cced07780ab2402a9876dfbedc7"

    put :update, id: org.id, organization: {
        favicon: mobile_image_url,
        splash_image: mobile_image_url,
        co_branding_logo: mobile_image_url,
        mobile_image: mobile_image_url,
        image: mobile_image_url,
    }, format: :json
    resp = get_json_response(response)

    assert resp["favicons"]["large"].include?("/organizations/favicons/")
    assert resp["co_branding_logo"].include?("/organizations/co_branding_logos/")
    assert resp["image_url"].include?("/organizations/mobile_images/")
    assert resp["banner_url"].include?("/organizations/images/")
  end

  test ':api should put#update quarters data' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'admin')
    org.configs << (config = create :config)

    api_v2_sign_in user

    image_url = "http://example.com/logo.jpg"
    stub_request(:get, image_url).to_return( :status => 200,
      :body => File.read('test/fixtures/images/test.png'),
      :headers => {'content-type':'image/png'})

    put :update, id: org.id, organization: { is_open: true, name: "new-name",
      description: "Edcast 2.0 org", image: image_url, mobile_image: image_url,
      host_name: 'www3', configs_attributes: [{ id: nil, name: "content_life_span", value: "90" },
      { id: config.id, value: "100" }], quarters_attributes: [
      { id: nil, name: "quarter-1", start_date: "2018-01-11" , end_date: "2018-04-10" },
      { id: nil, name: "quarter-2", start_date: "2018-04-11" , end_date: "2018-07-10" },
      { id: nil, name: "quarter-3", start_date: "2018-07-11" , end_date: "2018-11-10" },
      { id: nil, name: "quarter-4", start_date: "2018-11-11" , end_date: "2019-01-10" }], show_onboarding: false},
      format: :json
    resp = get_json_response(response)
    assert_not_empty resp
    assert_equal resp["id"], org.id
    assert_equal resp["name"], 'new-name'
    assert_includes resp["quarters"].map{|x| x["name"]}, "quarter-1"
    assert_includes resp["quarters"].map{|x| x["name"]}, "quarter-2"
    assert_includes resp["quarters"].map{|x| x["name"]}, "quarter-3"
    assert_includes resp["quarters"].map{|x| x["name"]}, "quarter-4"

  end

  test ':should not create quarter if start_date is greater than end date' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'admin')
    org.configs << (config = create :config)

    api_v2_sign_in user

    image_url = "http://example.com/logo.jpg"
    stub_request(:get, image_url).to_return( :status => 200,
      :body => File.read('test/fixtures/images/test.png'),
      :headers => {'content-type':'image/png'})

    put :update, id: org.id, organization: { is_open: true, name: "new-name",
      description: "Edcast 2.0 org", image: image_url, mobile_image: image_url,
      host_name: 'www3', configs_attributes: [{ id: nil, name: "content_life_span", value: "90" },
      { id: config.id, value: "100" }], quarters_attributes: [
      { id: nil, name: "quarter-1", start_date: "2018-01-11" , end_date: "2018-01-10" },
      { id: nil, name: "quarter-2", start_date: "2018-01-11" , end_date: "2018-07-10" },
      { id: nil, name: "quarter-3", start_date: "2018-07-11" , end_date: "2018-11-10" },
      { id: nil, name: "quarter-4", start_date: "2018-11-11" , end_date: "2019-01-10" }], show_onboarding: false},
      format: :json
    resp = get_json_response(response)
    assert_not_empty resp
    assert_equal resp["message"], "quarter-1: end date must be after the start date"

  end

  test ':should not create quarter with invalid end date' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'admin')
    org.configs << (config = create :config)

    api_v2_sign_in user

    image_url = "http://example.com/logo.jpg"
    stub_request(:get, image_url).to_return( :status => 200,
      :body => File.read('test/fixtures/images/test.png'),
      :headers => {'content-type':'image/png'})

    put :update, id: org.id, organization: { is_open: true, name: "new-name",
      description: "Edcast 2.0 org", image: image_url, mobile_image: image_url,
      host_name: 'www3', configs_attributes: [{ id: nil, name: "content_life_span", value: "90" },
      { id: config.id, value: "100" }], quarters_attributes: [
      { id: nil, name: "quarter-1", start_date: "2018-01-11" , end_date: "2018-04-10" },
      { id: nil, name: "quarter-2", start_date: "2018-04-11" , end_date: "2018-07-10" },
      { id: nil, name: "quarter-3", start_date: "2018-07-11" , end_date: "2018-11-10" },
      { id: nil, name: "quarter-4", start_date: "2018-11-11" , end_date: "2019-01-09" }], show_onboarding: false},
      format: :json
    resp = get_json_response(response)
    assert_not_empty resp
    assert_equal resp["message"], "Quarter 4: Invalid end date"

  end

  test ':should not create a quarter if dates are overlapping with existing quarters' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'admin')
    org.configs << (config = create :config)

    api_v2_sign_in user

    image_url = "http://example.com/logo.jpg"
    stub_request(:get, image_url).to_return( :status => 200,
      :body => File.read('test/fixtures/images/test.png'),
      :headers => {'content-type':'image/png'})

    put :update, id: org.id, organization: { is_open: true, name: "new-name",
      description: "Edcast 2.0 org", image: image_url, mobile_image: image_url,
      host_name: 'www3', configs_attributes: [{ id: nil, name: "content_life_span", value: "90" },
      { id: config.id, value: "100" }], quarters_attributes: [
      { id: nil, name: "quarter-1", start_date: "2018-01-11" , end_date: "2018-04-10" },
      { id: nil, name: "quarter-2", start_date: "2018-04-09" , end_date: "2018-04-10" },
      { id: nil, name: "quarter-3", start_date: "2018-07-11" , end_date: "2018-11-10" },
      { id: nil, name: "quarter-4", start_date: "2018-11-11" , end_date: "2019-01-10" }], show_onboarding: false},
      format: :json
    resp = get_json_response(response)
    assert_not_empty resp
    assert_equal resp["message"], "quarter-1: dates are overlapping"
  end


  test 'should update favicon' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'admin')

    api_v2_sign_in user

    image_url = "http://example.com/logo.jpg"
    stub_request(:get, image_url).to_return( :status => 200,
      :body => File.read('test/fixtures/images/test.png'),
      :headers => {'content-type':'image/png'})

    put :update, id: org.id, organization: {  name: "new-org-name",
      image: image_url, favicon: image_url}, format: :json

    resp = get_json_response(response)
    assert_equal resp["name"], 'new-org-name'

    assert_equal resp["favicons"].keys, ["tiny", "small", "medium", "large"]
  end

  test 'should return empty object for favicon' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'admin')

    api_v2_sign_in user

    image_url = "http://example.com/logo.jpg"
    stub_request(:get, image_url).to_return( :status => 200,
      :body => File.read('test/fixtures/images/test.png'),
      :headers => {'content-type':'image/png'})

    put :update, id: org.id, organization: {  name: "new-org-name",
      image: image_url}, format: :json

    resp = get_json_response(response)
    assert_equal resp["name"], 'new-org-name'
    assert_empty resp["favicons"].keys
  end

  test 'should put#update return form data with errors' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'admin')

    api_v2_sign_in user

    image_url = "http://example.com/logo.jpg"
    stub_request(:get, image_url).to_return( :status => 200,
      :body => File.read('test/fixtures/images/test.png'),
      :headers => {'content-type':'image/png'})

    put :update, id: org.id, organization: {is_open: true, name: "", description: "Edcast 2.0 org", image: image_url, mobile_image: image_url, host_name: 'www3'}, format: :json

    resp = get_json_response(response)

    assert_not_empty resp
    assert_not_empty resp['message']
    assert_includes "Name can't be blank", resp['message']
  end

  test 'should be able to toggle display of sub brand image on login page' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'admin')

    api_v2_sign_in user

    image_url = "http://example.com/logo.jpg"
    stub_request(:get, image_url).to_return( :status => 200,
                                             :body => File.read('test/fixtures/images/test.png'),
                                             :headers => {'content-type':'image/png'})

    get :show, format: :json
    resp = get_json_response(response)
    assert_equal true, resp['show_sub_brand_on_login']

    put :update, id: org.id, organization: {show_sub_brand_on_login: false}, format: :json
    resp = get_json_response(response)

    assert_not_empty resp
    assert_equal false, resp['show_sub_brand_on_login']
  end

  test 'enable analytics reporting #with reporting' do
    org    = create(:organization, enable_analytics_reporting: false)
    user   = create(:user, organization: org, organization_role: 'admin')
    config = create(:config, name: 'AnalyticsReportingConfig', value: {
      summary_feed: false, content_activity_feed: true, social_activity_feed: true
    }.to_json, data_type: 'json', configable: org)

    api_v2_sign_in user

    OrganizationAnalyticsJob.expects(:perform_later).with(org.id).never
    UserContentAnalyticsJob.expects(:perform_later).with(org.id).once
    SocialAnalyticsReportJob.expects(:perform_later).with(org.id).once

    put :update, id: org.id, organization: {enable_analytics_reporting: "true"}, send_analytics_reporting: 'true', format: :json
    assert_response :ok

    org.reload
    assert_equal true, org.enable_analytics_reporting
  end

  test 'enable analytics reporting #without reporting' do
    org    = create(:organization, enable_analytics_reporting: false)
    user   = create(:user, organization: org, organization_role: 'admin')
    api_v2_sign_in user

    OrganizationAnalyticsJob.expects(:perform_later).never
    UserContentAnalyticsJob.expects(:perform_later).never
    SocialAnalyticsReportJob.expects(:perform_later).never

    put :update, id: org.id, organization: {enable_analytics_reporting: "true"}, send_analytics_reporting: 'false', format: :json
    assert_response :ok

    org.reload
    assert_equal true, org.enable_analytics_reporting
  end

  test 'should set custom domain' do
    org    = create(:organization)
    custom_domain = "learn.something.com"
    admin_user   = create(:user, organization: org, organization_role: 'admin')
    api_v2_sign_in admin_user

    put :update, id: org.id, organization: {custom_domain: custom_domain}, format: :json
    assert_response :ok

    org.reload
    assert_equal custom_domain, org.custom_domain
  end

  # settings
  test 'organization member should not have access to the settings' do
    org = create(:organization)
    user = create(:user, organization: org, organization_role: 'member')

    api_v2_sign_in user

    get :settings, format: :json

    assert_response :unauthorized
  end

  test 'organization admin should be able to get settings' do
    org = create(:organization)
    user = create(:user, organization: org, organization_role: 'admin')

    api_v2_sign_in user

    get :settings, format: :json
    resp = get_json_response(response)

    assert_response :ok
    assert_equal org.configs.map(&:id), resp['configs'].map{|c| c['id']}
  end

  # metrics
  test 'only org admin or team admin can access org metrics if team_id is present' do
    org = create(:organization)
    admin = create(:user, organization: org, organization_role: 'admin')
    users = create_list(:user, 2, organization: org)
    team = create(:team, organization: org)
    team.add_admin(users[1])
    from_date = (Time.now.utc - 5.days).strftime("%m/%d/%Y")
    to_date = (Time.now.utc).strftime("%m/%d/%Y")
    User.any_instance.stubs(:authorize?).returns(true)

    api_v2_sign_in admin
    get :metrics, from_date: from_date, to_date: to_date, format: :json

    assert_response :ok

    User.any_instance.stubs(:authorize?).returns(false)
    api_v2_sign_in users[0]
    get :metrics, format: :json
    assert_response :unauthorized

    api_v2_sign_in users[1]
  end

  test ':api get org metrics for custom period' do
    org = create(:organization)
    admin = create(:user, organization: org, organization_role: 'admin')
    Timecop.freeze do
      api_v2_sign_in admin
      from_date = (Time.now.utc - 5.days).strftime("%m/%d/%Y")
      to_date = (Time.now.utc).strftime("%m/%d/%Y")

      timerange = [Time.strptime(from_date, "%m/%d/%Y").utc.beginning_of_day, Time.strptime(to_date, "%m/%d/%Y").utc.end_of_day]
      time_period_offsets_from = PeriodOffsetCalculator.applicable_offsets(timerange[0])[:day]
      time_period_offsets_to = PeriodOffsetCalculator.applicable_offsets(timerange[1])[:day]

      expected_data = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/custom_period_metrics_drill_down_data.json")).with_indifferent_access
      clc_data = {"clc_progress": {"value": 6, "drilldown": {"values"=>[0, 0, 0, 0, 0, 0]}}}
      expected_drill_down_data_current = Hash[[:smartbites_consumed, :smartbites_created, :average_session, :new_users, :total_users, :active_users, :engagement_index].map{|metric| [metric, expected_data[metric][:drilldown][:values]]}]
      OrganizationMetricsGatherer.expects(:drill_down_data_for_custom_period).with(organization: org, team_id: nil, timerange: timerange, period: :day).returns(expected_drill_down_data_current).once

      current_values = Hash[OrganizationMetricsGatherer::METRICS.map{|metric| [metric, expected_data[metric]['value']]}]
      OrganizationMetricsGatherer.expects(:metrics_over_span).with(organization: org, team_id: nil, period: :day, offsets: [time_period_offsets_from, time_period_offsets_to], timeranges: [[timerange[0], nil], [nil, timerange[1]]]).returns(current_values).once

      User.any_instance.stubs(:authorize?).returns true
      get :metrics, from_date: from_date, to_date: to_date, format: :json
      assert_response :ok

      actual_response = get_json_response(response)

      assert_equal(expected_data.merge(clc_data), actual_response)
    end
  end

  test 'get org metrics for custom period #missing params' do
    org = create(:organization)
    admin = create(:user, organization: org, organization_role: 'admin')
    User.any_instance.stubs(:authorize?).returns true

    api_v2_sign_in admin
    from_date = (Time.now.utc - 5.days).strftime("%m/%d/%Y")
    to_date = (Time.now.utc).strftime("%m/%d/%Y")

    get :metrics, format: :json
    assert_response :unprocessable_entity
  end

  test 'get org metrics for custom period #from date > to date' do
    org = create(:organization)
    admin = create(:user, organization: org, organization_role: 'admin')
    User.any_instance.stubs(:authorize?).returns true

    api_v2_sign_in admin

    to_date = (Time.now.utc - 5.days).strftime("%m/%d/%Y")
    from_date = (Time.now.utc).strftime("%m/%d/%Y")

    get :metrics, from_date: from_date, to_date: to_date, format: :json
    assert_response :unprocessable_entity
  end

  test 'get org metrics for custom period #from date = to date' do
    org = create(:organization)
    admin = create(:user, organization: org, organization_role: 'admin')
    Timecop.freeze do
      api_v2_sign_in admin
      from_date = (Time.now.utc).strftime("%m/%d/%Y")
      to_date = from_date

      timerange = [Time.strptime(from_date, "%m/%d/%Y").utc.beginning_of_day, Time.strptime(to_date, "%m/%d/%Y").utc.end_of_day]
      time_period_offsets_from = PeriodOffsetCalculator.applicable_offsets(timerange[0])[:day]
      time_period_offsets_to = PeriodOffsetCalculator.applicable_offsets(timerange[1])[:day]

      expected_data = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/custom_period_metrics_drill_down_data.json")).with_indifferent_access
      clc_data = {"clc_progress": {"value": 1, "drilldown": {"values"=>[0]}}}
      expected_drill_down_data_current = Hash[[:smartbites_consumed, :smartbites_created, :average_session, :new_users, :total_users, :active_users, :engagement_index].map { |metric| [metric, expected_data[metric][:drilldown][:values]] }]
      OrganizationMetricsGatherer.expects(:drill_down_data_for_custom_period).with(organization: org, team_id: nil, timerange: timerange, period: :day).returns(expected_drill_down_data_current).once

      current_values = Hash[OrganizationMetricsGatherer::METRICS.map { |metric| [metric, expected_data[metric]['value']] }]
      OrganizationMetricsGatherer.expects(:metrics_over_span).with(organization: org, team_id: nil, period: :day, offsets: [time_period_offsets_from, time_period_offsets_to], timeranges: [[timerange[0], nil], [nil, timerange[1]]]).returns(current_values).once
      User.any_instance.stubs(:authorize?).returns true

      get :metrics, from_date: from_date, to_date: to_date, format: :json
      assert_response :ok

      actual_response = get_json_response(response)

      assert_equal(expected_data.merge(clc_data), actual_response)
    end
  end

  test 'should not allow to exceed 2 years date range' do
    org = create(:organization)
    admin = create(:user, organization: org, organization_role: 'admin')

    api_v2_sign_in admin

    to_date = Time.now.utc.strftime("%m/%d/%Y")
    from_date = (Time.now.utc - 3.years).strftime("%m/%d/%Y")
    User.any_instance.stubs(:authorize?).returns true

    get :metrics, from_date: from_date, to_date: to_date, format: :json
    assert_response :unprocessable_entity
    resp = get_json_response(response)

    assert_match /Exceeded maximum date range/, resp['message']
  end

  # create setting
  test 'non admin member should not create settings' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'member')
    config = create(:config, configable: org, value: 'yes', data_type: 'boolean')

    api_v2_sign_in user
    assert_no_difference -> {Config.count} do
      put :create_setting, setting: {value: 'false'}, format: :json
      assert_response :unauthorized
    end
  end

  test ':api should create org settings' do
    org    = create(:organization)
    admin   = create(:user, organization: org, organization_role: 'admin', sign_out_at: Time.now)

    api_v2_sign_in admin
    assert_difference -> {Config.count} do
      put :create_setting, setting: {value: 'false', name: 'new setting', description: 'custom page setting', data_type: 'boolean'}, format: :json
      assert_response :ok
    end

    resp = get_json_response(response)

    config = Config.last
    assert_response :ok
    assert_equal false, config.parsed_value
    assert_equal config.data_type, resp['data_type']
    assert_equal config.name, resp['name']
  end

  test 'should create settings #JSON value' do
    org    = create(:organization)
    admin   = create(:user, organization: org, organization_role: 'admin')
    json_value = {web: { topMenu: "web/topMenu/aboutus" } }.with_indifferent_access

    api_v2_sign_in admin
    assert_difference -> {Config.count} do
      put :create_setting, setting: {value: json_value.to_json, name: 'json setting', description: 'custom page setting', data_type: 'json'}, format: :json
      assert_response :ok
    end

    resp = get_json_response(response)

    config = Config.last
    assert_response :ok
    assert_equal json_value, config.parsed_value
    assert_equal config.data_type, resp['data_type']
    assert_equal config.name, resp['name']
  end

  # update setting
  test 'non admin member should not update settings' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'member')
    config = create(:config, configable: org, value: 'yes', data_type: 'boolean')

    api_v2_sign_in user
    put :update_setting, id: config.id, setting: {value: 'false'}, format: :json

    config.reload
    assert_response :unauthorized
    assert_equal 'yes', config.value
  end

  test ':api should update org settings' do
    org    = create(:organization)
    admin   = create(:user, organization: org, organization_role: 'admin')
    config = create(:config, configable: org, value: 'yes', data_type: 'boolean')

    api_v2_sign_in admin
    put :update_setting, id: config.id, setting: {value: 'false'}, format: :json

    config.reload
    assert_response :ok
    assert_equal false, config.parsed_value
  end

  test 'should not update data type of org settings' do
    org    = create(:organization)
    admin   = create(:user, organization: org, organization_role: 'admin')
    config = create(:config, configable: org, value: 'yes', data_type: 'boolean')

    api_v2_sign_in admin
    put :update_setting, id: config.id, setting: {value: 'false', data_type: 'string'}, format: :json

    config.reload
    assert_response :ok
    assert_equal 'boolean', config.data_type
  end

  test 'should update the settings #JSON value' do
    org    = create(:organization)
    admin   = create(:user, organization: org, organization_role: 'admin')
    config = create(:config, configable: org, value: {"web" => { "topMenu" => "web/topMenu/home"} }.to_json, data_type: 'json')
    new_value = {web: { topMenu: "web/topMenu/aboutus" } }.with_indifferent_access

    api_v2_sign_in admin
    put :update_setting, id: config.id, setting: {value: new_value.to_json}, format: :json

    config.reload
    assert_response :ok
    assert_equal new_value, config.parsed_value
  end

  test ':api should get badges in an org' do
    Organization.any_instance.unstub(:create_org_card_badges)

    org = create(:organization)
    user = create(:user, organization: org)
    api_v2_sign_in(user)

    get :badges, type: 'CardBadge', format: :json

    resp = get_json_response(response)
    assert_response :success
    assert_not_nil resp['badges'].first['id']
  end

  test ':api should get badges with is_default and response return total ' do
    Organization.any_instance.unstub(:create_org_card_badges)
    image_url = "http://example.com/logo.jpg"
    stub_request(:get, image_url).to_return(
      status: 200,
      body: File.read('test/fixtures/images/test.png'),
      headers: { 'content-type':'image/png' }
    )
    org = create(:organization)
    org.card_badges.create(image: image_url)
    user = create(:user, organization: org)
    api_v2_sign_in(user)

    get :badges, type: 'CardBadge', is_default: 'false', format: :json

    resp = get_json_response(response)
    assert_response :ok
    assert_equal 1, resp['total']
  end

  test 'should create badge' do
    org = create(:organization)
    user = create(:user, organization: org)
    image_url = "http://example.com/logo.jpg"
    stub_request(:get, image_url).to_return(
      status: 200,
      body: File.read('test/fixtures/images/test.png'),
      headers: { 'content-type':'image/png' }
    )
    api_v2_sign_in user

    assert_difference -> { Badge.count }, 1 do
      post :create_badges, type: 'CardBadge', image_url: image_url , format: :json
    end

    assert_response :ok

    assert_no_difference -> { Badge.count } do
      post :create_badges, type: 'CardBadge', image_url: image_url , format: :json
    end

    assert_response :ok
    resp = get_json_response(response)
    refute resp['is_default']
  end

  # -----------------------------Leaderboard Start------------------------------
  # if admin or leader get leaderboard list - he get this list for all users in current organization,
  # if simple user - users list only in current user groups
  class LeaderboardTest < ActionController::TestCase
    setup do
      topics = Taxonomies.domain_topics
      User.any_instance.unstub :update_role
      Organization.any_instance.stubs(:leaderboard_enabled?).returns(true)

      @org1, @org2 = create_list(:organization, 2)
      create_org_master_roles @org1
      create_org_master_roles @org2

      @org1_users = create_list(:user, 5, organization: @org1, profile_attributes: { expert_topics: topics})
      @org2_users = create_list(:user, 2, organization: @org2)

      [@org1, @org2].each do |org|
        everyone_team = create(:team, name: 'Everyone', description: 'This is default team', organization: org, is_everyone_team: true)
        org.users.each do |user|
          everyone_team.add_user(user, as_type: user.organization_role)
        end
      end

      @user = create(:user, organization: @org1)
      @org1_users << @user
      @org1.everyone_team.add_user(@user, as_type: @user.organization_role)
      api_v2_sign_in @user
    end

    teardown do
    end

    test ':api should return 404 if leaderboard is disabled for org' do
      Organization.any_instance.unstub(:leaderboard_enabled?)
      get :leaderboard, show_my_result: false, format: :json
      assert_response :not_found
    end

    test ':api should get leaderboard for organization with minimum score as 1' do
      Timecop.freeze do
        now = Time.now.utc
        applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

        (@org1_users | @org2_users).each_with_index do |user, indx|
          create(:user_level_metric, user: user, smartbites_score: (indx + 1)*10, smartbites_created: indx, smartbites_consumed: indx+1, time_spent: indx*5, smartbites_comments_count: indx*13, smartbites_liked: indx*17, smartbites_completed: indx*2, period: :day, offset: applicable_offsets[:day])
        end

        # Suspended user should be excluded
        org1_suspended_user = create(:user, organization: @org1, is_suspended: true)
        create(:user_level_metric, user: org1_suspended_user, smartbites_score: 1000, smartbites_created: 1000, smartbites_consumed: 1000, time_spent: 1000, period: :day, offset: applicable_offsets[:day])

        get :leaderboard, show_my_result: false, format: :json
        assert_response :ok

        resp = get_json_response response

        range = 0..5

        assert_equal ['users', 'current_user', 'total_count'], resp.keys
        assert_empty resp['current_user']
        assert_equal @org1_users.map(&:id).reverse, resp['users'].map{ |r| r['user']['id'] }
        assert_equal (range).map{|i| (i + 1)*10}.reverse, resp['users'].map{ |r| r['smartbites_score'] }
        assert_equal (range).to_a.reverse, resp['users'].map{ |r| r['smartbites_created'] }
        assert_equal (range).map{|i| i+1}.reverse, resp['users'].map{ |r| r['smartbites_consumed'] }
        assert_equal (range).map{|i| i*5}.reverse, resp['users'].map{ |r| r['time_spent'] }
        assert_equal (range).map{|i| i*13}.reverse, resp['users'].map{ |r| r['smartbites_comments_count'] }
        assert_equal (range).map{|i| i*17}.reverse, resp['users'].map{ |r| r['smartbites_liked'] }
        assert_equal (range).map{|i| i*2}.reverse, resp['users'].map{ |r| r['smartbites_completed'] }
        assert_same_elements ['id', 'email', 'order_by_score', 'name', 'bio', 'handle', 'picture', 'roles_default_names',
                              'following', 'channel_count', 'group_count', 'followers_count', 'expert_topics', 'roles'], resp['users'].first['user'].keys
        assert_same_elements ['smartbites_score', 'smartbites_consumed', 'smartbites_created', 'time_spent', 'smartbites_comments_count', 'smartbites_liked', 'smartbites_completed', 'user'], resp['users'].first.keys
      end
    end

    test 'should return current user metrics if show_my_result is true' do
      Timecop.freeze do
        now = Time.now.utc
        applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

        @org1_users.each_with_index do |user, indx|
          create(:user_level_metric, user: user, smartbites_score: (indx + 1)*10, smartbites_created: indx, smartbites_consumed: indx+1, time_spent: indx*5, smartbites_comments_count: indx*13, smartbites_liked: indx*17, smartbites_completed: indx*2, period: :day, offset: applicable_offsets[:day])
        end

        get :leaderboard, show_my_result: true, format: :json
        resp = get_json_response(response)

        assert_response :ok
        assert_equal @org1_users.map(&:id).reverse, resp['users'].map{ |r| r['user']['id'] }
        assert_not_empty resp['current_user']
        assert_equal @user.id, resp['current_user']['user']['id']
      end
    end

    test 'should return current user metrics even if his score is zero' do
      Timecop.freeze do
        get :leaderboard, show_my_result: true, format: :json
        resp = get_json_response(response)

        assert_response :ok
        assert_equal @user.id, resp['current_user']['user']['id']
        assert_equal '-', resp['current_user']['user']['order_by_score']
      end
    end

    test 'mobile when passes limit 1 and show_my_result as true lists metrics of current_user only' do
      Timecop.freeze do
        applicable_offsets = PeriodOffsetCalculator.applicable_offsets(Time.now.utc)

        another_user = create(:user, organization: @org1)

        [@user, another_user].each_with_index do |user, indx|
          create(:user_level_metric, user: user, smartbites_score: (indx + 1)*10, smartbites_created: indx, smartbites_consumed: indx+1, time_spent: indx*5, smartbites_comments_count: indx*13, smartbites_liked: indx*17, smartbites_completed: indx*2, period: :day, offset: applicable_offsets[:day])
          @org1.everyone_team.add_user(user, as_type: user.organization_role)
        end

        # returns first user from leaderboard
        get :leaderboard, show_my_result: false, limit: 1, format: :json
        resp = get_json_response response
        assert_equal 1, resp['users'].count
        assert_equal another_user.id, resp['users'][0]['user']['id']

        # returns current user from leaderboard
        get :leaderboard, show_my_result: true, limit: 1, format: :json
        resp = get_json_response response
        assert_equal 1, resp['users'].count
        assert_equal @user.id, resp['users'][0]['user']['id']
      end
    end

    test 'should not return any user when no scoring data is present' do
      Timecop.freeze do
        get :leaderboard, show_my_result: false, format: :json
        assert_response :ok

        resp = get_json_response response
        assert_empty resp["users"]
      end
    end

    test 'should return data for n number of days' do
      Timecop.freeze do
        now = Time.now.utc
        applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

        (@org1_users | @org2_users).each_with_index do |user, indx|
          # If only today is considered, leaderboard is in reverse order of users
          day_minus_0 = create(:user_level_metric, user_id: user.id, smartbites_score: (indx + 1)*10, period: :day, offset: applicable_offsets[:day] - 0)

          # WHereas if two days are considered, leaderboard is in SAME order as users
          day_minus_1 = create(:user_level_metric, user_id: user.id, smartbites_score: (10-indx)*100, period: :day, offset: applicable_offsets[:day] - 1)
        end

        # Consider today only
        get :leaderboard, show_my_result: false, days: 0, format: :json
        assert_response :ok
        resp = get_json_response(response)
        assert_equal @org1_users.map(&:id).reverse, resp["users"].map{|r| r['user']['id']}

        # Consider over two days
        get :leaderboard, show_my_result: false, days: 1, format: :json
        assert_response :ok
        resp = get_json_response(response)
        assert_equal @org1_users.map(&:id), resp["users"].map{|r| r['user']['id']}
      end
    end

    test 'should return data for a specific period' do
      Timecop.freeze do
        now = Time.now.utc
        applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

        applicable_offsets.each do |k,v|
          (@org1_users | @org2_users).each_with_index do |user, indx|
            score = (indx + 1)*(v+1)*10
            unless k == :day
              score = (100-(indx + 1))*(v+1)*10
            end
            create(:user_level_metric, user_id: user.id, smartbites_score: score, period: k, offset: v)
          end
        end

        get :leaderboard, show_my_result: false, period: 'day', format: :json
        assert_response :ok
        resp = get_json_response(response)
        assert_equal @org1_users.map(&:id).reverse, resp['users'].map{|i| i['user']['id']}

        get :leaderboard, show_my_result: false, period: 'week', format: :json
        assert_response :ok

        resp = get_json_response(response)
        assert_equal @org1_users.map(&:id), resp['users'].map{|i| i['user']['id']}

        get :leaderboard, show_my_result: false, period: 'all_time', format: :json
        assert_response :ok
        resp = get_json_response(response)
        assert_equal @org1_users.map(&:id), resp['users'].map{|i| i['user']['id']}
      end
    end

    test 'should return data for a time range' do
      Timecop.freeze do
        now = Time.now.utc
        applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

        @day_minus_15 = []
        @org1_users.each_with_index do |user, indx|
          @day_minus_15 << create(:user_level_metric, user_id: user.id, smartbites_score: 500 + indx, period: :day, offset: applicable_offsets[:day] - 15)
          create(:user_level_metric, user_id: user.id, smartbites_score: 1000 + indx, period: :day, offset: applicable_offsets[:day] - 5)
        end

        from_date = (Date.today - 20.days).strftime("%m/%d/%Y")
        to_date = (Date.today - 10.days).strftime("%m/%d/%Y")

        get :leaderboard, show_my_result: false, from_date: from_date, to_date: to_date, format: :json
        assert_response :ok
        resp = get_json_response(response)
        assert_same_ids @org1_users.map(&:id), resp["users"].map{|r| r['user']['id']}
        assert_equal @day_minus_15.reverse.map(&:smartbites_score), resp["users"].map{|r| r['smartbites_score']}
      end
    end

    test 'should return data for a time range along with search term' do
      Timecop.freeze do
        applicable_offsets = PeriodOffsetCalculator.applicable_offsets(Time.now.utc)

        term_matching_user = create(:user, organization: @org1, first_name: 'uniq')
        [@user, term_matching_user].each_with_index do |user, indx|
          create(:user_level_metric, user_id: user.id, smartbites_score: 500 + indx, period: :day, offset: applicable_offsets[:day] - 15)
          create(:user_level_metric, user_id: user.id, smartbites_score: 1000 + indx, period: :day, offset: applicable_offsets[:day] - 5)
          @org1.everyone_team.add_user(user, as_type: user.organization_role)
        end

        from_date = (Date.today - 20.days).strftime("%m/%d/%Y")
        to_date = (Date.today - 10.days).strftime("%m/%d/%Y")

        # Fetch users for given time range
        get :leaderboard, show_my_result: false, from_date: from_date, to_date: to_date, format: :json
        assert_response :ok
        resp = get_json_response(response)
        assert_equal 2, resp['users'].size
        assert_equal [term_matching_user.id,@user.id], resp["users"].map{|r| r['user']['id']}

        # Fetch users for given time range along with search term
        User.stubs(:search).returns(OpenStruct.new(results: [term_matching_user], response: true))
        get :leaderboard, show_my_result: false, from_date: from_date, to_date: to_date, q: 'uniq', format: :json
        assert_response :ok
        resp = get_json_response(response)
        assert_equal [term_matching_user.id], resp["users"].map{|r| r['user']['id']}
      end
    end

    test 'should return only active users' do
      @inactive_org1_users = create_list(:user, 2, organization: @org1)
      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(Time.now.utc)

      applicable_offsets.each do |k,v|
        @org1_users.each do |user|
          create(:user_level_metric, user_id: user.id, smartbites_score: 4, period: k, offset: v, time_spent: 20)
        end

        @inactive_org1_users.each do |user|
          create(:user_level_metric, user_id: user.id, smartbites_score: 4, period: k, offset: v, time_spent: 0)
        end
      end

      get :leaderboard, show_my_result: false, period: 'all_time', active: 'true', format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_same_elements @org1_users.map(&:id), resp['users'].map{|i| i['user']['id']}
    end

    test 'should returns users matching with the search term' do
      u1 = create(:user, first_name: "uniq name", organization: @org1)
      u2 = create(:user, first_name: "whatever", organization: @org1)

      Timecop.freeze do
        now = Time.now.utc
        applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)
        [u1, u2].each_with_index  do |user, indx|
          create(:user_level_metric, user: user, smartbites_score: (indx + 1)*10, smartbites_created: indx,
                 smartbites_consumed: indx+1, time_spent: indx*5, smartbites_comments_count: indx*13,
                 smartbites_liked: indx*17, smartbites_completed: indx*2, period: :all_time, offset: 0)
          @org1.everyone_team.add_user(user, as_type: user.organization_role)
        end

        User.stubs(:search).returns(OpenStruct.new(results: [u1], response: true))

        get :leaderboard, period: 'all_time', q: "uniq", format: :json
        assert_response :ok
        resp = get_json_response(response)
        assert_equal [u1.id], resp['users'].map{|r| r['user']['id']}

        get :leaderboard, period: 'all_time', format: :json
        assert_response :ok
        resp = get_json_response(response)
        assert_same_elements [u1, u2].map(&:id), resp['users'].map{|r| r['user']['id']}
      end
    end

    test 'should return only users with selected roles' do
      Timecop.freeze do
        @admin = create(:user, organization: @org1, organization_role: 'admin')
        @admin.roles.find_by(organization_id: @org1.id).role_permissions
            .find_or_create_by(name: 'GET_LEADERBOARD_INFORMATION').enable!

        teams = create_list(:team, 10, organization: @org1)
        @org1_users << @admin
        @org1.everyone_team.add_user(@admin, as_type: @admin.organization_role)
        @org1_users.each do |u|
          teams[0].add_member(u)
        end

        now = Time.now.utc
        applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

        (@org1_users | @org2_users).each_with_index do |user, indx|
          create(:user_level_metric, user: user, smartbites_score: (indx + 1)*10, smartbites_created: indx,
                 smartbites_consumed: indx+1, time_spent: indx*5, smartbites_comments_count: indx*13,
                 smartbites_liked: indx*17, smartbites_completed: indx*2, period: :day, offset: applicable_offsets[:day])
        end

        # Suspended user should be excluded
        org1_suspended_user = create(:user, organization: @org1, is_suspended: true)
        create(:user_level_metric, user: org1_suspended_user, smartbites_score: 1000, smartbites_created: 1000,
               smartbites_consumed: 1000, time_spent: 1000, period: :day, offset: applicable_offsets[:day])
        api_v2_sign_in @admin

        get :leaderboard, show_my_result: false, roles: ["member"], format: :json
        assert_response :ok
        resp = get_json_response response
        assert_equal @org1_users.map(&:organization_role) - [@admin.organization_role],
                     User.where(id: resp['users'].map{|i| i['user']['id']}).pluck(:organization_role)
      end
    end

    test 'should return only users with selected roles matching search term' do
      Timecop.freeze do
        applicable_offsets = PeriodOffsetCalculator.applicable_offsets(Time.now.utc)

        admin_1 = create(:user, organization: @org1, organization_role: 'admin', first_name: 'uniq')
        admin_2 = create(:user, organization: @org1, organization_role: 'admin')

        [admin_1, admin_2].each do |admin|
          @org1_users << admin
          @org1.everyone_team.add_user(admin, as_type: admin.organization_role)
        end

        @org1_users.each_with_index do |user, indx|
          create(:user_level_metric, user: user, smartbites_score: (indx + 1)*10, smartbites_created: indx,
                 smartbites_consumed: indx+1, time_spent: indx*5, smartbites_comments_count: indx*13,
                 smartbites_liked: indx*17, smartbites_completed: indx*2, period: :day, offset: applicable_offsets[:day])
        end

        # Fetch users with role as admin
        get :leaderboard, show_my_result: false, roles: ['admin'], format: :json
        assert_response :ok
        resp = get_json_response response
        assert_equal 2, resp['users'].size
        assert_equal ['admin'], User.where(id: resp['users'].map{|i| i['user']['id']}).pluck(:organization_role).uniq

        # Fetch users with role as admin and search term as 'uniq'
        User.stubs(:search).returns(OpenStruct.new(results: [admin_1], response: true))
        get :leaderboard, show_my_result: false, roles: ['admin'], q: 'uniq', format: :json
        assert_response :ok
        resp = get_json_response response
        assert_equal 1, resp['users'].size
        assert_equal ['admin'], User.where(id: resp['users'].map{|i| i['user']['id']}).pluck(:organization_role)
      end
    end

    test 'should return users who are members of selected groups' do
      Timecop.freeze do
        @admin = create(:user, organization: @org1, organization_role: 'admin')
        @admin.roles.find_by(organization_id: @org1.id).role_permissions
            .find_or_create_by(name: 'GET_LEADERBOARD_INFORMATION').enable!
        teams = create_list(:team, 10, organization: @org1)
        @org1_users << @admin
        @org1_users.each do |u|
          teams[0].add_member(u)
        end

        now = Time.now.utc
        applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

        (@org1_users | @org2_users).each_with_index do |user, indx|
          create(:user_level_metric, user: user, smartbites_score: (indx + 1)*10, smartbites_created: indx,
                 smartbites_consumed: indx+1, time_spent: indx*5, smartbites_comments_count: indx*13,
                 smartbites_liked: indx*17, smartbites_completed: indx*2, period: :day, offset: applicable_offsets[:day])
        end

        api_v2_sign_in @admin

        get :leaderboard, show_my_result: false, group_ids: [teams[0].id], format: :json
        assert_response :ok
        resp = get_json_response response

        User.where(id: resp['users'].map{|i| i['user']['id']} ).each do |u|
          assert (u.teams.pluck(:id) & [teams[0].id]).any?
        end
      end
    end

    test 'should return users who are members of selected group matching search term' do
      Timecop.freeze do
        applicable_offsets = PeriodOffsetCalculator.applicable_offsets(Time.now.utc)
        team = create(:team, organization: @org1)
        term_matching_user = create(:user, organization: @org1, first_name: 'uniq')
        @org1_users << term_matching_user

        @org1_users.each_with_index do |user, indx|
          create(:user_level_metric, user: user, smartbites_score: (indx + 1)*10, smartbites_created: indx,
                   smartbites_consumed: indx+1, time_spent: indx*5, smartbites_comments_count: indx*13,
                   smartbites_liked: indx*17, smartbites_completed: indx*2, period: :day, offset: applicable_offsets[:day])
          team.add_member(user)
        end

        # Fetch users part of the team
        get :leaderboard, show_my_result: false, group_ids: [team.id], format: :json
        assert_response :ok
        resp = get_json_response response
        assert_equal @org1_users.size, resp['users'].size
        User.where(id: resp['users'].map{|i| i['user']['id']} ).each do |user|
          assert_includes user.teams.pluck(:id), team.id
        end

        # Fetch users part of team and matching search term
        User.stubs(:search).returns(OpenStruct.new(results: [term_matching_user], response: true))
        get :leaderboard, show_my_result: false, group_ids: [team.id], q: 'uniq', format: :json
        assert_response :ok
        resp = get_json_response(response)
        assert_equal [term_matching_user.id], resp["users"].map{|r| r['user']['id']}
      end
    end

    test 'should return users with selected expertises' do
      Timecop.freeze do
        @user = create(:user, organization: @org1, organization_role: 'admin')
        @user.roles.find_by(organization_id: @org1.id).role_permissions
            .find_or_create_by(name: 'GET_LEADERBOARD_INFORMATION').enable!

        user_without_expertises = create(:user, organization: @org1)

        @org1_users << user_without_expertises

        now = Time.now.utc
        applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

        (@org1_users | @org2_users).each_with_index do |user, indx|
          create(:user_level_metric, user: user, smartbites_score: (indx + 1)*10, smartbites_created: indx,
                 smartbites_consumed: indx+1, time_spent: indx*5, smartbites_comments_count: indx*13,
                 smartbites_liked: indx*17, smartbites_completed: indx*2, period: :day, offset: applicable_offsets[:day])
        end

        api_v2_sign_in @user

        User.stubs(:search).returns(OpenStruct.new(results: [@user], response: true))

        get :leaderboard, show_my_result: false, expertises_names: [Taxonomies.domain_topics[0]['topic_name'], Taxonomies.domain_topics[1]['topic_name']], format: :json
        assert_response :ok

        resp = get_json_response response
        assert resp['users'].count != @org1_users.count

        expertises_names = [Taxonomies.domain_topics[0]['topic_name'], Taxonomies.domain_topics[1]['topic_name']]

        User.where(id: resp['users'].map{|i| i['user']['id']} ).each do |u|
          assert true do
            (u.profile.expert_topics.map{|t| t[:topic_name]} & expertises_names).any?
          end
        end
      end
    end

    test 'should return only inactive users' do
      @inactive_org1_users = create_list(:user, 2, organization: @org1)
      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(Time.now.utc)

      @user = create(:user, organization: @org1)
      @inactive_org1_users << @user

      applicable_offsets.each do |k,v|
        @org1_users.each do |user|
          create(:user_level_metric, user_id: user.id, smartbites_score: 4, period: k, offset: v, time_spent: 20)
        end

        @inactive_org1_users.each do |user|
          create(:user_level_metric, user_id: user.id, smartbites_score: 4, period: k, offset: v, time_spent: 0)
          @org1.everyone_team.add_user(user, as_type: user.organization_role)
        end
      end

      api_v2_sign_in @user

      get :leaderboard, show_my_result: false, period: 'all_time', active: 'false', format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_same_elements @inactive_org1_users.map(&:id), resp['users'].map{|i| i['user']['id']}
    end

    test 'should return users list with sorted params' do
      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(Time.now.utc)

      applicable_offsets.each do |k,v|
        @org1_users.each_with_index do |user, indx|
          create(:user_level_metric, user_id: user.id, smartbites_completed: indx, smartbites_score: 10 - indx, period: k, offset: v, time_spent: 20)
        end
      end

      # desc order
      get :leaderboard, show_my_result: false, period: 'all_time', order_attr: 'smartbites_completed', order: 'desc', format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_same_elements @org1_users.map(&:id), resp['users'].map{|i| i['user']['id']}
      assert_equal (0...@org1_users.count).to_a.reverse, resp['users'].map{|i| i['smartbites_completed']}

      # asc order
      get :leaderboard, show_my_result: false, period: 'all_time', order_attr: 'smartbites_completed', order: 'asc', format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_same_elements @org1_users.map(&:id), resp['users'].map{|i| i['user']['id']}
      assert_equal (0...@org1_users.count).to_a, resp['users'].map{|i| i['smartbites_completed']}
    end

    test 'should return total count of users in response' do
      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(Time.now.utc)

      applicable_offsets.each do |k,v|
        @org1_users.each do |user|
          create(:user_level_metric, user_id: user.id, smartbites_score: 4, period: k, offset: v, time_spent: 20)
        end
      end

      get :leaderboard, show_my_result: false, period: 'all_time', format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_same_elements @org1_users.map(&:id), resp['users'].map{|i| i['user']['id']}
      assert_equal @org1_users.length, resp["total_count"]
    end

    test 'should return only users present in db' do
      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(Time.now.utc)

      applicable_offsets.each do |k,v|
        @org1_users.each do |user|
          create(:user_level_metric, user_id: user.id, smartbites_score: 4, period: k, offset: v, time_spent: 20)
        end
      end

      # total @org1_users is 5, delete one user
      @org1_users.first.destroy

      present_users = @org1_users.last(5)

      get :leaderboard, show_my_result: false, period: 'all_time', limit: 5, offset: 0, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_same_elements present_users.map(&:id), resp['users'].map{|i| i['user']['id']}
      assert_equal resp['total_count'], present_users.length
    end

    test 'should not return users with exclude_from_leaderboard flag set' do
      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(Time.now.utc)

      applicable_offsets.each do |k,v|
        @org1_users.each do |user|
          create(:user_level_metric, user_id: user.id, smartbites_score: 4,
            period: k, offset: v, time_spent: 20)
        end
      end

      # total @org1_users are 5, set flag to excude last one from leaderboard
      @org1_users.last.update_attribute('exclude_from_leaderboard', true)
      show_on_leaderboard_users = @org1_users.first(4)

      get :leaderboard, show_my_result: false, period: 'all_time', format: :json
      assert_response :ok
      resp = get_json_response(response)
      refute_includes resp['users'].map{|user| user['user']['id']}, @org1_users.last.id
    end

    test 'should not return users with hide_from_leaderboard flag set' do
      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(Time.now.utc)

      applicable_offsets.each do |k,v|
        @org1_users.each do |user|
          create(:user_level_metric, user_id: user.id, smartbites_score: 4,
            period: k, offset: v, time_spent: 20)
        end
      end

      # total @org1_users are 5, set flag to hide last one from leaderboard
      @org1_users.last.update_attribute('hide_from_leaderboard', true)
      show_on_leaderboard_users = @org1_users.first(4)

      get :leaderboard, show_my_result: false, period: 'all_time', format: :json
      assert_response :ok
      resp = get_json_response(response)
      refute_includes resp['users'].map{|user| user['user']['id']}, @org1_users.last.id
      show_on_leaderboard_users.each do |user|
        assert_includes resp['users'].map{|user| user['user']['id']}, user.id
      end
    end
  end
  # -----------------------------Leaderboard End--------------------------------

  # list ssos
  test ':api should list of ssos available for current org' do
    create_default_ssos
    # Default + custom ssos

    org = create(:organization)
    admin_user = create(:user, organization: org, organization_role: 'admin')
    ssos = []
    (1..4).each do
      ssos << create(:sso, name: Faker::Name.name)
    end

    org.ssos = ssos

    api_v2_sign_in admin_user
    get :list_ssos, format: :json
    resp = get_json_response(response)
    assert_equal 10, resp['ssos'].size
    assert_same_elements ssos.map(&:name) + (Sso::DEFAULT_SSO_NAMES + ['lxp_oauth', 'internal_content']), resp['ssos'].map {|sso| sso['name']}
    # grouped by enabled followed by disabled ones

    assert resp['ssos'].first['is_enabled']
    refute resp['ssos'].last['is_enabled']
  end

  test 'member should not have access to list of ssos' do
    org    = create(:organization)
    member = create(:user, organization: org, organization_role: 'member')

    api_v2_sign_in member
    get :list_ssos, format: :json

    assert_response :unauthorized
  end

  test ':api should enable sso' do
    org = create(:organization)
    admin_user = create(:user, organization: org, organization_role: 'admin')
    new_sso = create(:sso, name: 'custom_oauth')

    api_v2_sign_in admin_user

    assert_difference -> {OrgSso.count}, 1 do
      put :enable_sso, sso_name: new_sso.name, format: :json
    end

    resp = get_json_response(response)

    assert_response :ok
    assert org.org_ssos.last.is_enabled
    assert resp['is_enabled']
  end

  test 'member should not have access to enable sso' do
    org    = create(:organization)
    member = create(:user, organization: org, organization_role: 'member')
    new_sso = create(:sso, name: 'custom_oauth')

    api_v2_sign_in member
    get :enable_sso, sso_name: new_sso.name, format: :json

    assert_response :unauthorized
  end

  test ':api should disable sso' do
    org = create(:organization)
    admin_user = create(:user, organization: org, organization_role: 'admin')
    sso = create(:sso, name: 'custom_oauth')
    org.ssos << sso

    api_v2_sign_in admin_user

    assert_no_difference -> {OrgSso.count} do
      put :disable_sso, sso_name: sso.name, format: :json
    end

    resp = get_json_response(response)

    assert_response :ok
    refute org.org_ssos.last.is_enabled
    refute resp['is_enabled']
  end

  test 'member should not have access to disable sso' do
    org    = create(:organization)
    member = create(:user, organization: org, organization_role: 'member')
    sso = create(:sso, name: 'custom_oauth')
    org.ssos << sso

    api_v2_sign_in member
    get :disable_sso, sso_name: sso.name, format: :json

    assert_response :unauthorized
  end

  test 'add_lms_users' do
    org = create(:organization)
    admin_user = create(:user, organization: org, organization_role: 'admin')
    api_v2_sign_in admin_user

    LmsAddOrganizationUsersJob.expects(:perform_later)
    put :add_lms_users, format: :json
    resp = get_json_response(response)
    assert_response :ok
  end

  test ':api push_ecl_content_to_channels' do
    org = create(:organization)
    channels = create_list(:channel, 2, organization: org)
    admin_user = create(:user, organization: org, organization_role: 'admin')
    api_v2_sign_in admin_user
    channel_ids = channels.map{|c| c.id}

    PushECLContentToChannelsJob.expects(:perform_later).with(organization_id: org.id, channel_ids: channel_ids, ecl_ids: ["test-ecl-id-1"]).once
    post :push_ecl_content_to_channels, channel_ids: channel_ids, ecl_ids:  ["test-ecl-id-1"], format: :json
    resp = get_json_response(response)
    assert_response :ok
  end

  # content level metrics
  class ContentMetrics < ActionController::TestCase
    setup do
      @org1, @org2 = create_list(:organization, 2)
      @org1_user = create(:user, organization: @org1)
      @admin_user = create(:user, organization: @org1, organization_role: 'admin')

      @card = create(:card, organization: @org1, author: @org1_user)
      @video_stream_card = create(:card, message: 'video stream card', author: @org1_user)
      @video_stream = create(:wowza_video_stream, organization: @org1, creator: @org1_user, card: @video_stream_card)
      @card_pack = create(:card, card_type: 'pack', card_subtype: 'simple', organization: @org1, author: @org1_user)

      # In a different org
      @org2_user = create(:user, organization: @org2)
      @org2_card = create(:card, organization: @org2, author: @org2_user)
    end

    teardown do
    end

    test ':api metrics by content' do
      Timecop.freeze do
        api_v2_sign_in @admin_user

        now = Time.now.utc
        applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

        applicable_offsets.each do |k, v|
          card_metric = create(:content_level_metric, content_id: @card.id, content_type: "card", views_count: 20, comments_count: 10, likes_count: 1, completes_count: 1, period: k, offset: v)
          video_stream_metric = create(:content_level_metric, content_id: @video_stream.card.id, content_type: "card", views_count: 30, comments_count: 20, likes_count: 2, completes_count: 2, bookmarks_count: 1, period: k, offset: v)
          collection_metric = create(:content_level_metric, content_id: @card_pack.id, content_type: "collection", views_count: 40, comments_count: 30, likes_count: 3, completes_count: 4, bookmarks_count: 3, period: k, offset: v)
          org2_card_metric = create(:content_level_metric, content_id: @org2_card.id, content_type: "card", views_count: 20, comments_count: 10, likes_count: 1, completes_count: 3, period: k, offset: v)
        end

        from_date = (Date.today - 2.days).strftime("%m/%d/%Y")
        to_date = Date.today.strftime("%m/%d/%Y")

        get :content_metrics, from_date: from_date, to_date: to_date, format: :json

        assert_response :ok
        resp = get_json_response(response)

        assert_equal [@card_pack, @video_stream_card, @card].map(&:id), resp['items'].map{|i| i['item']['id']}
        assert_equal [@card_pack, @video_stream_card, @card].map(&:snippet), resp['items'].map{|i| i['item']['title']}

        assert_equal [40,30,20], resp['items'].map{|i| i['views_count']}
        assert_equal [30,20,10], resp['items'].map{|i| i['comments_count']}
        assert_equal [3,2,1], resp['items'].map{|i| i['likes_count']}
        assert_equal [4,2,1], resp['items'].map{|i| i['completes_count']}
        assert_equal [3,1,0], resp['items'].map{|i| i['bookmarks_count']}
      end
    end

    test 'return data for a specific search term' do
      Timecop.freeze do
        now = Time.now.utc
        applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

        applicable_offsets.each do |k,v|
          create(:content_level_metric, content_id: @card.id, content_type: "card", views_count: 10, period: k, offset: v)
          create(:content_level_metric, content_id: @video_stream_card.id, content_type: "card", views_count: 2, period: k, offset: v)
        end

        @card.update(message: "test card")

        api_v2_sign_in @admin_user

        from_date = (Date.today - 2.days).strftime("%m/%d/%Y")
        to_date = Date.today.strftime("%m/%d/%Y")

        get :content_metrics, search_term: 'test', from_date: from_date, to_date: to_date, format: :json

        assert_response :ok
        resp = get_json_response(response)
        assert_equal [[@card.id, 'card']], resp['items'].map{|i| [i['item']['id'], i['item']['type']]}

        get :content_metrics, search_term: 'video', from_date: from_date, to_date: to_date, format: :json
        assert_response :ok
        resp = get_json_response(response)
        assert_equal [[@video_stream_card.id, 'card']], resp['items'].map{|i| [i['item']['id'], i['item']['type']]}

        get :content_metrics, search_term: 'check', from_date: from_date, to_date: to_date, format: :json
        assert_response :ok
        resp = get_json_response(response)
        assert_empty resp['items']
      end
    end

    test 'blank state' do
      api_v2_sign_in @admin_user

      from_date = (Date.today - 2.days).strftime("%m/%d/%Y")
      to_date = Date.today.strftime("%m/%d/%Y")

      get :content_metrics, from_date: from_date, to_date: to_date, format: :json
      assert_response :ok

      resp = get_json_response(response)
      assert_equal({"items" => []}, resp)
    end

    test 'send deleted key' do
      UserContentsQueue.stubs(:delete_by_query).returns(nil).once
      Analytics::MetricsRecorderJob.stubs(:perform_later)

      Timecop.freeze do
        now = Time.now.utc
        applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

        deleted_card = create(:card, organization_id: @org1.id, author_id: @org1_user.id)
        create(:content_level_metric, content_id: deleted_card.id, content_type: "card", views_count: 20, period: :day, offset: applicable_offsets[:day])
        create(:content_level_metric, content_id: @card.id, content_type: "card", views_count: 10, period: :day, offset: applicable_offsets[:day])
        deleted_card.destroy

        from_date = (Date.today - 2.days).strftime("%m/%d/%Y")
        to_date = Date.today.strftime("%m/%d/%Y")

        api_v2_sign_in @admin_user

        get :content_metrics, from_date: from_date, to_date: to_date, format: :json
        assert_response :ok

        resp = get_json_response(response)
        assert_equal [true, false], resp['items'].map{|i| i['item']['is_deleted']}
      end
      UserContentsQueue.unstub(:delete_by_query)
    end

    test 'only org admin can access content metrics' do
      api_v2_sign_in @org1_user
      get :content_metrics, format: :json
      assert_response :unauthorized
    end

    test 'should return content metrics for permission MANAGE_ANALYTICS' do
      Timecop.freeze do
        Role.create_standard_roles @org1
        member_role = @org1.roles.find_by(name: 'member')
        create(:role_permission, role: member_role, name: Permissions::MANAGE_ANALYTICS)
        @org1_user.add_role('member')
        team = create(:team, organization: @org1)
        team.add_sub_admin(@org1_user)
        api_v2_sign_in @org1_user

        now = Time.now.utc
        applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

        applicable_offsets.each do |k, v|
          create(
            :content_level_metric, content_id: @card.id, content_type: "card",
            views_count: 20, comments_count: 10, likes_count: 1, completes_count: 1,
            period: k, offset: v
          )
        end

        from_date = (Date.today - 2.days).strftime("%m/%d/%Y")
        to_date = Date.today.strftime("%m/%d/%Y")

        get :content_metrics, from_date: from_date, to_date: to_date, format: :json

        assert_response :ok
        resp = get_json_response(response)

        assert_equal @card.id, resp['items'].first['item']['id']
        assert_equal @card.snippet, resp['items'].first['item']['title']
        assert_equal([20, 10, 1, 1, 0], resp['items'].first.values_at('views_count',
                                                                      'comments_count',
                                                                      'likes_count',
                                                                      'completes_count',
                                                                      'bookmarks_count')
        )
      end
    end

    test 'should return unauthorized for limited user if not MANAGE_ANALYTICS permission' do
      Role.create_standard_roles @org1
      member_role = @org1.roles.find_by(name: 'member')
      create(:role_permission, role: member_role, name: Permissions::MANAGE_GROUP_CONTENT)
      @org1_user.add_role('member')
      api_v2_sign_in @org1_user

      from_date = (Date.today - 2.days).strftime("%m/%d/%Y")
      to_date = Date.today.strftime("%m/%d/%Y")

      get :content_metrics, from_date: from_date, to_date: to_date, format: :json

      assert_response :unauthorized
    end

    test 'should return content metrics for group_sub_admin only for teams where user is group admin' do
      member_user = create(:user, organization: @org1)
      card1 = create(:card, organization: @org1, author: @org1_user)
      card2 = create(:card, organization: @org1, author: member_user)
      user = create(:user, organization: @org1)

      Role.create_standard_roles @org1
      role_member = @org1.roles.find_by(name: 'member')
      create(:role_permission, role: role_member, name: Permissions::MANAGE_GROUP_ANALYTICS)
      role_member.add_user(user)

      team, team1 = create_list(:team, 2, organization: @org1)

      team.add_user(@org1_user)
      team1.add_user(member_user)

      team1.add_user(user, as_type: 'sub_admin')
      team.add_user(user, as_type: 'sub_admin')

      Timecop.freeze do
        api_v2_sign_in user

        now = Time.now.utc
        applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

        applicable_offsets.each do |k, v|
          create(
            :content_level_metric, content_id: card1.id, content_type: 'card',
            views_count: 2, comments_count: 1, likes_count: 1, completes_count: 1,
            period: k, offset: v
          )
          create(
            :content_level_metric, content_id: card2.id, content_type: 'card',
            views_count: 2, comments_count: 1, likes_count: 1, completes_count: 1,
            period: k, offset: v
          )
        end

        from_date = (Date.today - 2.days).strftime("%m/%d/%Y")
        to_date = Date.today.strftime("%m/%d/%Y")

        get :content_metrics, from_date: from_date, to_date: to_date, team_ids: [team.id], format: :json

        assert_response :ok
        resp = get_json_response(response)

        assert_equal 1, resp['items'].count
        assert_equal card1.id, resp['items'].first['item']['id']
        assert_not_includes resp['items'].map{|d| d['item']['id']}, card2.id
        assert_equal card1.snippet, resp['items'].first['item']['title']
        assert_equal([2, 1, 1, 1, 0], resp['items'].first.values_at('views_count',
                                                                    'comments_count',
                                                                    'likes_count',
                                                                    'completes_count',
                                                                    'bookmarks_count')
        )
      end
    end

    test 'verify order_attr and sort' do
      Timecop.freeze do
        api_v2_sign_in @admin_user
        now                 = Time.now.utc
        cards               = create_list(:card, 5, organization: @org1, author: @org1_user)
        applicable_offsets  = PeriodOffsetCalculator.applicable_offsets(now)
        previous_days       = [now-1.day, now-2.day, now-3.day, now-4.day, now-5.day]
        previous_applicable_offsets = []
        previous_days.each do |day|
          previous_applicable_offsets << PeriodOffsetCalculator.applicable_offsets(day)[:day]
        end

        cards.each_with_index do |card, indx|
          random_numbers    = 5.times.map{ Random.rand(100) }
          random_numbers.each_with_index do |value,new_index|
            create(:content_level_metric,
              organization:     @org1,
              content_id:       card.id,
              content_type:     "card",
              views_count:      indx,
              comments_count:   value,
              likes_count:      indx*5,
              completes_count:  indx*13,
              bookmarks_count:  indx*17,
              period:           :day,
              offset:           previous_applicable_offsets[new_index]
            )
          end
        end

        from_date   = (Date.today - 5.days).strftime("%m/%d/%Y")
        to_date     = Date.today.strftime("%m/%d/%Y")

        sorted_content_by_comments_count = ContentLevelMetric.where(organization_id: @org1.id)
                                            .select("sum(comments_count) as comments_count,content_id")
                                            .group(:content_id)
                                            .order("comments_count")
        sorted_content_id = sorted_content_by_comments_count.map{ |t| t.content_id}

        get :content_metrics, order_attr: "comments_count", order: "asc", from_date: from_date, to_date: to_date, format: :json
        assert_response :ok

        resp = get_json_response(response)
        content_metric_response = resp['items']
        assert_equal sorted_content_id, content_metric_response.map{|r| r['item']['id']}
      end
    end
  end

  test "should return all teams of the organization to org admin" do
    org = create(:organization)
    user = create(:user, organization: org, organization_role: "admin")
    teams = create_list(:team, 10, organization: org)
    teams[0].add_member(user)

    api_v2_sign_in user
    get :teams, format: :json
    resp = get_json_response(response)

    assert_equal 10, resp["teams"].length
    assert_equal 1, user.teams.length
  end

  test "should return everyone team of the organization to org admin" do
    org = create(:organization)
    user = create(:user, organization: org, organization_role: "admin")
    team1 = create(:team, organization: org)
    team2 = create(:team, is_everyone_team: true, organization: org)

    api_v2_sign_in user
    get :teams, is_everyone: 'true', format: :json
    resp = get_json_response(response)

    assert_equal 1, resp["teams"].length
    assert resp["teams"][0]["is_everyone_team"]
  end

  test "should return non-everyone team of the organization to org admin" do
    org = create(:organization)
    user = create(:user, organization: org, organization_role: "admin")
    teams = create_list(:team, 2, is_everyone_team: false, organization: org)
    team2 = create_list(:team, 1, is_everyone_team: true, organization: org)

    api_v2_sign_in user
    get :teams, is_everyone: 'false', format: :json
    resp = get_json_response(response)

    assert_equal 2, resp["teams"].length
    refute resp["teams"][0]["is_everyone_team"]
  end

  test "should search teams  with search term" do
    org = create(:organization)
    user = create(:user, organization: org, organization_role: "admin")
    teams = create_list(:team, 2, organization: org)
    team = create(:team, name: "test", organization: org)

    api_v2_sign_in user
    get :teams, q: "test", format: :json
    resp = get_json_response(response)
    assert_equal 1, resp["teams"].length
  end

  test "should not return teams with ids from exclude_team_ids" do
    org = create(:organization)
    user = create(:user, organization: org, organization_role: "admin")
    teams = create_list(:team, 2, organization: org)
    team = create(:team, name: "test", organization: org)

    api_v2_sign_in user
    get :teams, exclude_team_ids: [team.id], format: :json
    resp = get_json_response(response)
    assert resp["teams"].map{ |t| t['id'] }.exclude?(team.id)
    assert_equal 2, resp["teams"].length
  end

  test "should return teams  with team_ids" do
    org = create(:organization)
    user = create(:user, organization: org, organization_role: "admin")
    teams = create_list(:team, 2, organization: org)
    team1 = create(:team, organization: org)

    api_v2_sign_in user
    get :teams, team_ids: teams.map(&:id), format: :json
    resp = get_json_response(response)
    assert_equal 2, resp["teams"].length
  end

  test "should return teams for group_sub_admin user" do
    org = create(:organization)
    Role.create_standard_roles org
    member_role = org.roles.find_by_name('member')
    create(:role_permission, role: member_role, name: Permissions::MANAGE_GROUP_USERS)
    user = create(:user, organization: org)
    user.add_role('member')
    teams = create_list(:team, 2, organization: org)
    teams.first.add_user(user, as_type: 'sub_admin')

    api_v2_sign_in user
    get :teams, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal 1, resp["teams"].length
    assert_equal teams.first.id, resp["teams"].first['id']
  end

  test "should return all teams for org admins who are also group_sub_admin user" do
    org = create(:organization)
    Role.create_standard_roles org

    admin_role = org.roles.find_by_name('member')
    create(:role_permission, role: admin_role, name: Permissions::MANAGE_GROUP_USERS)

    user = create(:user, organization: org, organization_role: 'admin')
    create(:user_role, user_id: user.id, role_id: admin_role.id)

    teams = create_list(:team, 2, organization: org)
    teams.first.add_user(user, as_type: 'sub_admin')

    api_v2_sign_in user
    get :teams, format: :json
    assert_response :ok

    resp = get_json_response(response)
    assert_equal 2, resp["teams"].length
    assert_same_elements  resp['teams'].map{|d| d['id']}, teams.map(&:id)
  end

  test "should return unauthorized for group_sub_admin user if not MANAGE_GROUP_USERS permission" do
    org = create(:organization)
    Role.create_standard_roles org
    member_role = org.roles.find_by_name('member')
    create(:role_permission, role: member_role, name: Permissions::MANAGE_GROUP_CONTENT)
    user = create(:user, organization: org)
    user.add_role('member')
    teams = create_list(:team, 2, organization: org)
    teams.first.add_user(user)

    api_v2_sign_in user
    get :teams, format: :json
    assert_response :unauthorized
  end

  test 'should return proper keys in full response' do
    org = create(:organization)
    admin = create(:user, organization: org, organization_role: 'admin')
    teams = create_list(:team, 2, organization: org)
    teams[0].add_member(admin)

    api_v2_sign_in(admin)
    get :teams, full_response: true, format: :json
    resp = get_json_response(response)

    assert_equal 2, resp['teams'].length
    assert 'is_dynamic_selected'.in?(resp['teams'].first.keys)
  end

  test ":api get all channels of the organization to org admin depending on only_public key" do
    Organization.any_instance.unstub(:create_general_channel)
    org = create(:organization)
    user = create(:user, organization: org, organization_role: "admin")
    channels = create_list(:channel, 5, organization: org, is_private: false)
    private_channels = create_list(:channel, 5, organization: org, is_private: true)

    api_v2_sign_in user
    get :channels, only_public: 'true', format: :json
    resp = get_json_response(response)
    assert_empty resp["channels"].select{|d| d["is_private"] == 1}
    # the public channels(5) + the general channel created with every org(1)
    assert_equal 6, resp["channels"].length
  end

  test ":api search channels  with search term" do
    org = create(:organization)
    user = create(:user, organization: org, organization_role: "admin")
    channels = create_list(:channel, 2, organization: org)
    channel = create(:channel, label: "test", organization: org)

    api_v2_sign_in user
    get :channels, q: "test", format: :json
    resp = get_json_response(response)
    assert_equal 1, resp["channels"].length
    assert_equal channel.id, resp["channels"][0]['id']
    assert_equal channel.label, resp["channels"][0]['label']
  end

  test ":api get all channels of an organization with pagination" do
    org = create(:organization)
    user = create(:user, organization: org, organization_role: "admin")
    channels = create_list(:channel, 5, organization: org, is_private: false)
    private_channels = create_list(:channel, 5, organization: org, is_private: true)

    api_v2_sign_in user

    # returns all channels of an org when only_public is not passed with default limit: 10
    get :channels, format: :json
    resp = get_json_response(response)
    assert_equal 10,resp["channels"].length

    # to test pagination
    get :channels, limit: 5, format: :json
    resp = get_json_response(response)
    assert_equal 5, resp["channels"].length
  end

  test 'api show should render not found if host_name not available' do
    org1 = Organization.new(name: 'a', host_name: 'a')
    set_host org1
    get :show, format: :json
    assert_response :not_found
  end

  test 'guide me config should render encrypted org key' do
    org = create(:organization)
    config = create(:config, name: 'guide_me_config', value: {
      edition: "ent", guide_me_enabled: true, org_key: "a77a159aa", org_secret: "9IDPLE05"
    }.to_json, data_type: 'json', configable: org)

    set_host org
    get :show, format: :json

    assert_response :ok
    resp = get_json_response(response)
    guide_me_config_resp = resp['configs'].find{|c| c['name'] == 'guide_me_config'}['value']
    assert guide_me_config_resp['encrypted_org_key']
    assert_equal "a77a159aa", guide_me_config_resp['org_key']
  end

  class ChannelPerformanceTest < ActionController::TestCase
    setup do
      stub_request(:get, "http://localhost:9200/#{MetricRecord.index_name}/_search")

      @org = create(:organization)
      @user = create(:user, organization: @org)
      @admin = create(:user, organization: @org, organization_role: 'admin')

      @public_channel = create(:channel, organization: @org)
      @private_channel = create(:channel, organization: @org, is_private: true)

      @cards = create_list(:card, 2, organization: @org, author: @user)
      @public_channel.cards  <<  @cards.first
      @private_channel.cards << @cards.last

      create(:follow, followable: @public_channel, user: @user)

      AnalyticsReport::ChannelPerformance.any_instance.stubs(:get_consumed_cards).returns(
        {
          "view" => { @cards.first.id => 5, @cards.last.id => 3},
          "like" => { @cards.first.id => 3, @cards.last.id => 2}
        }
      )
    end

    test ':api return channel performance data' do
      api_v2_sign_in @admin
      get :channel_performance, format: :json

      resp = get_json_response(response)
      public_channel_data = resp["channel_performance"].select{|c| c["channel_id"] == @public_channel.id}.first
      private_channel_data = resp["channel_performance"].select{|c| c["channel_id"] == @private_channel.id}.first

      assert_not_empty resp['channel_performance']
      assert_equal(public_channel_data,
        {
          "channel_id" => @public_channel.id,
          "channel_label" => @public_channel.label,
          "is_private" => false,
          "followers_count" => 1,
          "published_cards_count" => 1,
          "likes_count" => 3,
          "bookmarks_count" => 0,
          "comments_count" => 0,
          "views_count" => 5,
          "completes_count" => 0
        }
      )

      assert_equal(private_channel_data,
        {
          "channel_id" => @private_channel.id,
          "channel_label" => @private_channel.label,
          "is_private" => true,
          "followers_count" => 0,
          "published_cards_count" => 1,
          "likes_count" => 2,
          "bookmarks_count" => 0,
          "comments_count" => 0,
          "views_count" => 3,
          "completes_count" => 0
        }
      )
    end

    test ':api should filter channel performance data by search term' do
      api_v2_sign_in @admin
      get :channel_performance, q: @public_channel.label, format: :json

      resp = get_json_response(response)
      assert_equal 1, resp['channel_performance'].count
      assert_equal @public_channel.label, resp['channel_performance'].first['channel_label']
    end

    test 'should check date range for channel performance data' do
      api_v2_sign_in @admin
      get :channel_performance, from_date: 2.days.ago.strftime("%m/%d/%Y"), to_date: 1.days.ago.strftime("%m/%d/%Y"), format: :json

      resp = get_json_response(response)
      public_channel_data = resp["channel_performance"].select{|c| c["channel_id"] == @public_channel.id}.first
      private_channel_data = resp["channel_performance"].select{|c| c["channel_id"] == @private_channel.id}.first

      assert_not_empty resp['channel_performance']
      assert_equal(public_channel_data,
        {
          "channel_id" => @public_channel.id,
          "channel_label" => @public_channel.label,
          "is_private" => false,
          "followers_count" => 0,
          "published_cards_count" => 0,
          "likes_count" => 3,
          "bookmarks_count" => 0,
          "comments_count" => 0,
          "views_count" => 5,
          "completes_count" => 0
        }
      )

      assert_equal(private_channel_data,
        {
          "channel_id" => @private_channel.id,
          "channel_label" => @private_channel.label,
          "is_private" => true,
          "followers_count" => 0,
          "published_cards_count" => 0,
          "likes_count" => 2,
          "bookmarks_count" => 0,
          "comments_count" => 0,
          "views_count" => 3,
          "completes_count" => 0
        }
      )
    end

    test 'should allow only admins to read channel performance data' do
      api_v2_sign_in @user
      get :channel_performance, format: :json
      assert_response :unauthorized
    end

    test '#get_channel_performance' do
      from_date = 2.days.ago
      to_date = 1.days.ago
      channel_perfomance = @org.get_channel_performance(from_date, to_date)

      public_channel = {
        @public_channel.id => {
          label: @public_channel.label,
          is_private: false,
          followers_count: 0,
          published_cards_count: 0,
          likes_count: 3,
          bookmarks_count: 0,
          comments_count: 0,
          views_count: 5,
          completes_count: 0
        }
      }

      private_channel = {
        @private_channel.id => {
          label: @private_channel.label,
          is_private: true,
          followers_count: 0,
          published_cards_count: 0,
          likes_count: 2,
          bookmarks_count: 0,
          comments_count: 0,
          views_count: 3,
          completes_count: 0
        }
      }

      assert_same_elements public_channel.merge(private_channel), channel_perfomance
    end

    test 'should return channel_performance for permission MANAGE_ANALYTICS user' do
      Role.create_standard_roles @org
      member_role = @org.roles.find_by(name: 'member')
      create(:role_permission, role: member_role, name: Permissions::MANAGE_ANALYTICS)
      @user.add_role('member')
      team = create(:team, organization: @org)
      team.add_sub_admin(@user)
      api_v2_sign_in @user

      get :channel_performance, {
        from_date: 2.days.ago.strftime("%m/%d/%Y"),
        to_date: 1.days.ago.strftime("%m/%d/%Y"),
        format: :json
      }

      assert_response :ok
      resp = get_json_response(response)
      public_channel_data = resp['channel_performance'].select{|c| c['channel_id'] == @public_channel.id}.first
      private_channel_data = resp['channel_performance'].select{|c| c['channel_id'] == @private_channel.id}.first

      assert_not_empty resp['channel_performance']
      assert_equal(public_channel_data, {
        'channel_id' => @public_channel.id,
        'channel_label' => @public_channel.label,
        'is_private' => false,
        'followers_count' => 0,
        'published_cards_count' => 0,
        'likes_count' => 3,
        'bookmarks_count' => 0,
        'comments_count' => 0,
        'views_count' => 5,
        'completes_count' => 0
      })

      assert_equal(private_channel_data, {
        'channel_id' => @private_channel.id,
        'channel_label' => @private_channel.label,
        'is_private' => true,
        'followers_count' => 0,
        'published_cards_count' => 0,
        'likes_count' => 2,
        'bookmarks_count' => 0,
        'comments_count' => 0,
        'views_count' => 3,
        'completes_count' => 0
      })
    end
  end

  class RestrictAccessToTest < ActionController::TestCase

    setup do
      @user = create(:user)
      @organization_id = @user.organization_id
      @other_user1 = create(:user, first_name: 'gigi nana', organization_id: @organization_id)
      @other_user2 = create(:user, first_name: 'rifi hadid', organization_id: @organization_id)

      @team = create(:team, name: 'sales and all', organization_id: @organization_id)
      @channel = create(:channel, label: 'eng', organization_id: @organization_id)

      Search::UserSearch.any_instance.stubs(:search).
        returns(Search::UserSearchResponse.new(results: [@other_user1.as_json, @other_user2.as_json], total: 2))

      Search::TeamSearch.any_instance.stubs(:search).
        returns(Search::TeamSearchResponse.new(results: [@team.as_json], total: 1))
    end

    test '#restrictable_users_or_teams should not be called when neither team or channel are selected' do
      api_v2_sign_in @user
      get :restrictable_users_or_teams, q: 'gigi', format: :json
      resp = get_json_response(response)
      assert_response :unprocessable_entity
      assert_equal "Cannot restrict to user/group when both channel_ids and group_ids is empty", resp['message']
    end

    test 'restrictable_users_or_teams should be called when team is selected' do
      api_v2_sign_in @user

      get :restrictable_users_or_teams, q: 'gigi', team_ids: [@team.id], format: :json
      resp = get_json_response(response)
      assert_empty resp['groups']
      assert_same_elements ["id", "name", "channel_ids", "team_ids"], resp['users'].first.keys
      assert_same_elements [@other_user1.id, @other_user2.id], resp['users'].map{|d| d['id']}
    end

    test 'restrictable_users_or_teams should return team only when channel is selected' do
      api_v2_sign_in @user

      get :restrictable_users_or_teams, q: 'gigi', team_ids: [@team.id], channel_ids: [@channel.id], format: :json
      resp = get_json_response(response)
      assert_equal [@team.id], resp['groups'].map{|d| d['id']}
      assert_same_elements ["id", "name", "channel_ids"], resp['groups'].first.keys
      assert_same_elements [@other_user1.id, @other_user2.id], resp['users'].map{|d| d['id']}
    end

    test 'restrictable_users_or_teams should not return card author and current_user' do
      api_v2_sign_in @user

      card = create :card, author: @other_user1

      get :restrictable_users_or_teams, q: 'gigi', team_ids: [@team.id], card_id: card.id, format: :json
      resp = get_json_response(response)
      assert_same_elements [@other_user2.id], resp['users'].map{|d| d['id']}
    end
  end

  class ShareableChannelTest < ActionController::TestCase

    setup do
      org1, org2, org3 = create_list(:organization, 3)

      # org_mappings
      create(:config, name: 'feature/shared_channels', value: 'true', data_type: 'boolean', configable: org1)
      OrgMapping.create(child_organization_id: org2.id, parent_organization_id: org1.id)

      # channels
      @channels1 = create_list(:channel, 2, organization: org1, shareable: 1)
      @channels3 = create_list(:channel, 2, organization: org3)
      @channel = create(:channel, organization: org1, label: 'test', shareable: 1)

      @admin = create(:user, organization: org2, organization_role: 'admin')
      @user1 = create(:user, organization: org2, organization_role: 'member')
    end

    test 'should search shareable channels with search term' do
      api_v2_sign_in @admin
      get :shareable_channels, q: 'test', format: :json
      resp = get_json_response(response)
      assert_equal 1, resp['channels'].length
      assert_equal @channel.id, resp['channels'][0]['id']
      assert_equal @channel.label, resp['channels'][0]['label']
    end

    test 'should not allow non admin to access list of shareable channels' do
      api_v2_sign_in @user1
      get :shareable_channels, limit: 10, offset: 0, format: :json
      assert_response :unauthorized
    end

    test 'should not return channels from orgs that have not shared with current org' do
      api_v2_sign_in @admin
      get :shareable_channels, limit: 10, offset: 0, format: :json
      assert_response :success
      resp = get_json_response(response)
      assert_equal 3, resp['total']
      assert_same_elements resp['channels'].map{|d| d['id']}, @channels1.map(&:id) + [@channel.id]
      assert_not_includes resp['channels'].map{|d| d['id']}, @channels3[0].id
      assert_not_includes resp['channels'].map{|d| d['id']}, @channels3[1].id
    end

    test 'should not return channels that are not shareable' do
      unshareable_channel = create(:channel, organization: @channels1[0].organization)
      api_v2_sign_in @admin
      get :shareable_channels, limit: 10, offset: 0, format: :json
      assert_response :success
      resp = get_json_response(response)
      assert_equal 3, resp['total']
      assert_not_includes resp['channels'].map{|d| d['id']}, unshareable_channel.id
    end

    test 'should return channels that have already been cloned by the current org as well' do
      shared_channel = create(:channel, organization: @admin.organization, parent_id: @channels1[0].id)
      api_v2_sign_in @admin
      get :shareable_channels, limit: 10, offset: 0, format: :json
      assert_response :success
      resp = get_json_response(response)
      assert_equal 3, resp['total']

      assert_includes resp['channels'].map{|d| d['id']}, @channels1[0].id
      assert_includes resp['channels'].map{|d| d['id']}, @channels1[1].id
    end

    test 'should return all channels that the current org can clone' do
      api_v2_sign_in @admin
      get :shareable_channels, limit: 10, offset: 0, format: :json
      assert_response :success
      resp = get_json_response(response)
      assert_equal 3, resp['total']
      assert_includes resp['channels'].map{|d| d['id']}, @channels1[0].id
      assert_includes resp['channels'].map{|d| d['id']}, @channels1[1].id
      assert_includes resp['channels'].map{|d| d['id']}, @channel.id
      assert_not_equal @admin.organization_id, resp['channels'][0]['organization_id'].to_i
    end

    test 'should cater to pagination' do
      api_v2_sign_in @admin
      get :shareable_channels, limit: 1, offset: 0, format: :json
      assert_response :success
      resp = get_json_response(response)
      assert_equal 3, resp['total']
      assert_equal 1, resp['channels'].count
      assert_same_elements [@channels1[0].id], resp['channels'].map{|d| d['id']}
    end

    test 'should not return channels from org if shared feature config is false' do
      org = create(:organization)
      create(:config, name: 'feature/shared_channels', value: 'false', data_type: 'boolean', configable: org)
      OrgMapping.create(child_organization_id: @admin.organization_id, parent_organization_id: org.id)
      shared_channel = create(:channel, organization: org, shareable: true)

      api_v2_sign_in @admin
      get :shareable_channels, limit: 3, offset: 0, format: :json
      assert_response :success
      resp = get_json_response(response)

      assert_equal 3, resp['total']
      assert_same_elements @channels1.map(&:id) + [@channel.id], resp['channels'].map{|d| d['id']}
      assert_not_includes resp['channels'].map{|d| d['id']}, shared_channel.id
    end

    test 'non admins should not be able to access cloned channels' do
      non_admin = create(:user, organization: @channels1[0].organization)
      api_v2_sign_in non_admin
      get :cloned_channels, format: :json
      assert_response :unauthorized
    end

    test 'non admins should not be able to access shared channels' do
      non_admin = create(:user, organization: @channels1[0].organization)
      api_v2_sign_in non_admin
      get :shared_channels, format: :json
      assert_response :unauthorized
    end

    test 'org admin should be able to fetch shared channels of the org only' do
      non_org_shareable = create(:channel, organization: @admin.organization, shareable: true)
      admin = create(:user, organization: @channels1[0].organization, organization_role: 'admin')
      api_v2_sign_in admin
      get :shared_channels, format: :json
      resp = get_json_response(response)
      assert_same_elements @channels1.map(&:id) + [@channel.id], resp['channels'].map{|d| d['id']}
      assert_not_includes resp['channels'].map{|d| d['id']}, non_org_shareable.id
    end

    test 'should search shared channels with search term' do
      admin = create(:user, organization: @channels1[0].organization, organization_role: 'admin')
      api_v2_sign_in admin
      get :shared_channels, q: 'test', format: :json
      resp = get_json_response(response)
      assert_equal 1, resp['channels'].length
      assert_equal @channel.id, resp['channels'][0]['id']
      assert_equal @channel.label, resp['channels'][0]['label']
    end

    test 'org admin should be able to fetch the cloned channels of the org only' do
      cloned_channel = create(:channel, organization: @admin.organization, parent_id: @channels1[0].id)
      cloned_on_org3 = create(:channel, organization: @channels3[0].organization, parent_id: @channels1[0].id)
      api_v2_sign_in @admin
      get :cloned_channels, format: :json
      resp = get_json_response(response)
      assert_same_elements [cloned_channel.id], resp['channels'].map{|d| d['id']}
      assert_not_includes resp['channels'].map{|d| d['id']}, cloned_on_org3.id
    end

    test 'shared channels api handles sort order' do
      org = create(:organization)
      admin = create(:user, organization_id: org.id, organization_role: 'admin')

      OrgMapping.create(child_organization_id: org.id, parent_organization_id: @channels1.first.organization_id)

      channel1 = create(:channel, organization_id: @channels1.first.organization_id, created_at: Time.now + 1.day, shareable: 1)
      channel2 = create(:channel, organization_id: @channels1.first.organization_id, created_at: Time.now + 2.days, shareable: 1)
      channel3 = create(:channel, organization_id: @channels1.first.organization_id, created_at: Time.now + 3.days, shareable: 1)

      api_v2_sign_in admin
      get :shareable_channels, order: :desc, format: :json

      resp = get_json_response(response)
      assert_equal resp['channels'].map{|d| d['id']}, ([channel3.id, channel2.id] + [channel1.id] + @channels1.map(&:id) + [@channel.id])
    end

    test 'ensuring shareable_channels returns the necessary keys in the response' do
      api_v2_sign_in @admin
      get :shareable_channels, order: :desc, is_cms: true, format: :json
      resp = get_json_response(response)
      assert_includes resp['channels'].first.keys, 'parent_org'
    end
  end

  test 'api create_setting, update_setting -should  restore  RequestStore.store[:request_metadata] user_id' do
    org    = create(:organization)
    admin   = create(:user, organization: org, organization_role: 'admin', sign_out_at: Time.now)

    api_v2_sign_in admin
    assert_difference -> {Config.count} do
      put :create_setting, setting: {value: 'false', name: 'new setting', description: 'custom page setting', data_type: 'boolean'}, format: :json
      assert_response :ok
    end

    assert_equal admin.id, RequestStore.read(:request_metadata)[:user_id]
  end

end

require 'test_helper'

class Api::V2::CustomFieldsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    @organization = create(:organization)
    @admin = create(:user, organization: @organization)
    @user = create(:user, organization: @organization)

    api_v2_sign_in(@admin)
  end

  test ':api #create creates custom field accordingly' do
    post :create, custom_field: {}, format: :json
    assert_response 400

    post :create, custom_field: { display_name: 'A' }, format: :json

    assert assigns(:custom_field).persisted?
    assert_equal 'a', assigns(:custom_field).abbreviation
    assert_equal 'A', assigns(:custom_field).display_name
  end

  test ':api #create should validate maximum length' do
    name = 'very long long long long long long long long long long long long name'
    post :create, custom_field: { display_name: name }, format: :json
    assert_response :unprocessable_entity
    res = get_json_response(response)
    expected_message = 'Abbreviation is too long (maximum is 64 characters) and '
    addition = 'Display name is too long (maximum is 64 characters)'

    assert_equal expected_message + addition, res['message']
  end

  test ':api #create should save enable_people_search param for custom field' do
    post :create, custom_field: { display_name: 'A', enable_people_search: true }, format: :json
    assert_response :ok
    custom_field = CustomField.last
    assert_equal 'A', custom_field.display_name
    assert custom_field.enable_people_search
  end

  test ':api #update should change enable_people_search param for custom field' do
    cf = create :custom_field, organization: @organization, display_name: 'A',
                enable_people_search: true
    put :update, id: cf.id, custom_field: {enable_people_search: false}, format: :json
    assert_response :ok
    custom_field = CustomField.last
    assert_equal false, custom_field.reload.enable_people_search
  end

  test ':api #index lists all available custom fields added to an organization' do
    cf1 = create :custom_field, organization: @organization, display_name: 'A',
                 abbreviation: 'a', enable_people_search: true, created_at: Time.now + 2
    cf2 = create :custom_field, organization: @organization, display_name: 'B',
                 enable_people_search: false, abbreviation: 'b', created_at: Time.now + 1
    cf3 = create :custom_field, organization: @organization, display_name: 'C',
                 enable_people_search: true, abbreviation: 'c', created_at: Time.now

    expected_response = [cf1, cf2, cf3].map do |cf|
      cf.attributes.slice('id', 'display_name', 'abbreviation', 'enable_people_search')
        .merge({'enable_in_filters' => false, 'show_in_profile' => false, 'editable_by_users' => false})
    end

    get :index, format: :json

    result = get_json_response(response)
    assert_equal expected_response, result['custom_fields']
  end

  test ':api #destroy should delete custom_field and dependent user_custom_fields' do
    cf = create(:custom_field, organization: @organization, display_name: 'A', abbreviation: 'a')
    ucf = create(:user_custom_field, user_id: @user.id, custom_field_id: cf.id)
    delete :destroy, id: cf.id, format: :json
    assert_response :ok
    refute CustomField.exists?(id: cf.id)
    refute UserCustomField.exists?(id: ucf.id)
  end

  test ':api #destroy should be available for admin_only' do
    org = create(:organization, enable_role_based_authorization: true)
    regular_user = create(:user, organization: org)
    cf = create(:custom_field, organization: org, display_name: 'A', abbreviation: 'a')

    api_v2_sign_in(regular_user)
    delete :destroy, id: cf.id, format: :json
    assert_response :unauthorized
    res = get_json_response(response)

    assert_equal 'unauthorized action', res['message']
    assert CustomField.exists?(id: cf.id)
  end

  test ':api should create config enable in filters or user profile' do
    cf = create(:custom_field, organization: @organization, display_name: 'A', abbreviation: 'a')

    post :custom_field_config, id: cf.id, config_name: 'enable_in_filters', value: 'true', format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal cf.id, resp['id']
    assert resp['enable_in_filters']

    post :custom_field_config, id: cf.id, config_name: 'show_in_profile', value: 'true', format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal cf.id, resp['id']
    assert resp['show_in_profile']

    post :custom_field_config, id: cf.id, config_name: 'editable_by_users', value: 'true', format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal cf.id, resp['id']
    assert resp['editable_by_users']
  end

  test 'api should update config enable in filters or user profile' do
    cf = create(:custom_field, organization: @organization, display_name: 'A', abbreviation: 'a')
    cf.set_value_for_config('enable_in_filters', true)
    cf.set_value_for_config('show_in_profile', true)
    cf.set_value_for_config('editable_by_users', true)

    post :custom_field_config, id: cf.id, config_name: 'enable_in_filters', value: 'false', format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal cf.id, resp['id']
    refute resp['enable_in_filters']

    post :custom_field_config, id: cf.id, config_name: 'show_in_profile', value: 'false', format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal cf.id, resp['id']
    refute resp['show_in_profile']

    post :custom_field_config, id: cf.id, config_name: 'editable_by_users', value: 'false', format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal cf.id, resp['id']
    refute resp['editable_by_users']
  end

  test '#custom_field_config should return error if missing key enable_in_filters' do
    cf = create(:custom_field, organization: @organization, display_name: 'A', abbreviation: 'a')
    post :custom_field_config, id: cf.id, format: :json
    assert_response :unprocessable_entity
    resp = get_json_response(response)
    assert_equal 'Config_name is not valid', resp['message']

    post :custom_field_config, id: cf.id, config_name: 'show_in_profile', format: :json
    assert_response :unprocessable_entity
    resp = get_json_response(response)
    assert_equal 'Value can`t be empty', resp['message']
  end
end

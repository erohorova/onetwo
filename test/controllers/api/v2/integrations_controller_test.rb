require 'test_helper'

class Api::V2::IntegrationsControllerTest < ActionController::TestCase

  test ":api should returns enable or disable intgration" do
    user1 = create(:user)
    integration1 = create(:integration, name: "Coursera")
    integration2 = create(:integration, name: "Skill Share")

    user1.integrations << integration1 << integration2

    get :toggle_integration, id: integration1.id, connect: true, format: :json
    assert_response 401
    api_v2_sign_in user1

    #should updtae if connect false
    get :toggle_integration, id: integration1.id, connect: false, format: :json
    assert_response :ok

    #should not connect if fields are empty
    get :toggle_integration, id: integration1.id, connect: true, format: :json
    assert_response 422

    CoursesScraperJob.expects(:perform_later).once.returns(true)
    get :toggle_integration, id: integration1.id, fields: {public_id: "123"}, provider: "Coursera", connect: true, format: :json
    assert_response :ok

    CoursesScraperJob.expects(:perform_later).once.returns(true)
    get :toggle_integration, id: integration2.id, fields: {public_id: "123"}, provider: "Skill Share", connect: true, format: :json
    assert_response :ok

    CoursesScraperJob.expects(:perform_later).once.returns(true)
    get :toggle_integration, id: integration2.id, fields: {public_id: "123"}, provider: "Future Learn", connect: true, format: :json
    assert_response :ok

    CoursesScraperJob.expects(:perform_later).once.returns(true)
    get :toggle_integration, id: integration2.id, fields: {public_id: "123"}, provider: "Plural Sight", connect: true, format: :json
    assert_response :ok

    CoursesScraperJob.expects(:perform_later).once.returns(true)
    get :toggle_integration, id: integration2.id, fields: {public_id: "123"}, provider: "Tree House", connect: true, format: :json
    assert_response :ok

    CoursesScraperJob.expects(:perform_later).once.returns(true)
    get :toggle_integration, id: integration2.id, fields: {public_id: "123"}, provider: "Code School", connect: true, format: :json
    assert_response :ok

    CoursesScraperJob.expects(:perform_later).once.returns(true)
    get :toggle_integration, id: integration2.id, fields: {public_id: "123"}, provider: "Alison", connect: true, format: :json
    assert_response :ok
  end
end

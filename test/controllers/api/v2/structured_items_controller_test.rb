require 'test_helper'

class Api::V2::StructuredItemsControllerTest < ActionController::TestCase
  class GetBulkStructureItemsTest < ActionController::TestCase
    setup do
      @org  = create(:organization)
      @user = create(:user, organization: @org)
      @user1 = create(:user, organization: @org, organization_role: 'admin')

      @channel1 = create :channel, organization: @org
      @channel2 = create :channel, organization: @org, is_private: true
      create(:follow, followable: @channel2, user: @user1)
      @channel_structure = create :structure, current_context: 'discover', current_slug: 'channels', creator: @user, organization: @org, enabled: true
      @channel_structure.structured_items.create(entity: @channel1)
      @channel_structure.structured_items.create(entity: @channel2)
      @st_1 = StructuredItem.find_by(structure: @channel_structure, entity: @channel1)
      @st_2 = StructuredItem.find_by(structure: @channel_structure, entity: @channel2)

      @carousel_user = create(:user, organization: @org)
      @user_structure = create :structure, current_context: 'discover', current_slug: 'users', creator: @user, organization: @org, enabled: true
      @user_structure.structured_items.create(entity: @carousel_user)
      @st_3 = StructuredItem.find_by(structure: @user_structure, entity: @user_structure)

      @card1 = create :card, organization: @org, author: @user
      @card2 = create :card, organization: @org, author: @user, is_public: false
      @card_structure = create :structure, current_context: 'discover', current_slug: 'cards', creator: @user, organization: @org, enabled: true
      @card_structure.structured_items.create(entity: @card1)
      @card_structure.structured_items.create(entity: @card2)
      @st_4 = StructuredItem.find_by(structure: @card_structure, entity: @card1)
      @st_5 = StructuredItem.find_by(structure: @card_structure, entity: @card2)
    end


    test 'expect set of attributes for channel entity when asked for' do
      api_v2_sign_in(@user1)

      get :entities_data, structure_ids: [@channel_structure.id], channel_fields: 'id,label,description,is_private,allow_follow,banner_image_urls,profile_image_url,profile_image_urls,is_following,updated_at,slug,followers_count', format: :json
      resp = get_json_response(response)
      assert_same_elements ["id", "label", "description", "is_private", "allow_follow", "banner_image_urls", "profile_image_url", "profile_image_urls", "is_following", "updated_at", "slug", "followers_count"], resp[@channel_structure.id.to_s]["1"]["entity"].keys
    end

    test 'lists all the items in a structure to admin user' do
      api_v2_sign_in(@user1)

      get :entities_data, structure_ids: [@channel_structure.id, @user_structure.id, @card_structure.id], format: :json
      resp = get_json_response(response)
      assert_same_elements [@channel_structure, @user_structure, @card_structure].map(&:id), resp.keys.map(&:to_i)

      resp_entity_ids = []
      resp.values.each{|a| a.each{|k,v|  resp_entity_ids << v['entity']&.dig('id').to_i}}
      assert_same_elements [@channel1, @channel2, @carousel_user, @card1, @card2].map(&:id), resp_entity_ids
      assert_equal ["id", "handle", "avatarimages", "roles", "name", "is_following", "following_count", "followers_count", "expert_skills", "status", "company"], resp[@user_structure.id.to_s]["1"]["entity"].keys
    end

    test 'do not list inaccessible private channels and cards in a structure to member user' do
      member_user = create(:user, organization: @org)
      api_v2_sign_in(member_user)

      get :entities_data, structure_ids: [@channel_structure.id, @user_structure.id, @card_structure.id], format: :json
      resp = get_json_response(response)

      resp_entity_ids = []
      resp.values.each{|a| a.each{|k,v|  resp_entity_ids << v['entity']&.dig('id').to_i}}
      assert_same_elements [@channel1, @carousel_user, @card1].map(&:id), resp_entity_ids
    end

    test 'list private channels in a structure to member user if he is following private channel' do
      follower_user = create(:user, organization: @org)
      create(:follow, followable: @channel2, user:follower_user)
      api_v2_sign_in(follower_user)

      get :entities_data, structure_ids: [@channel_structure.id], format: :json
      resp = get_json_response(response)

      resp_entity_ids = []
      resp.values.each{|a| a.each{|k,v|  resp_entity_ids << v['entity']&.dig('id').to_i}}
      assert_same_elements [@channel1, @channel2].map(&:id), resp_entity_ids
    end

    test 'return empty message if any custom carousel not active in organization' do
      org  = create(:organization)
      user = create(:user, organization: org)
      api_v2_sign_in(user)

      get :entities_data, structure_ids: [1], format: :json
      resp = get_json_response(response)

      assert_equal "Active Custom Carousels not found.", resp['message']
    end
  end

  class IndexMethodTest < ActionController::TestCase
    # Note: Please maintain the segregation of channels, cards & users while
    # adding your test cases.
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
    end

    # ----------------------------- For Channels -------------------------------
    def setup_for_channel_carousal
      @channel1 = create :channel, organization: @org
      @channel2 = create :channel, organization: @org, is_private: true
      @channel3 = create :channel, organization: @org, is_private: true

      @structure = create :structure, current_context: 'discover', current_slug: 'channels', creator: @user, organization: @org

      @structure.structured_items.create(entity: @channel1)
      @structure.structured_items.create(entity: @channel2)
      @structure.structured_items.create(entity: @channel3)

      @st_1 = StructuredItem.find_by(structure: @structure, entity: @channel1)
      @st_2 = StructuredItem.find_by(structure: @structure, entity: @channel2)
      @st_3 = StructuredItem.find_by(structure: @structure, entity: @channel3)
    end

    test 'lists all the channels in a structure except entities user doesnt have access to' do
      setup_for_channel_carousal
      team = create(:team, organization: @org)
      team.add_member(@user)
      create(:teams_channels_follow, team: team, channel: @channel2)
      api_v2_sign_in(@user)

      get :index, structure_id: @structure.id, format: :json

      resp = get_json_response(response)
      channels = resp['structured_items'].collect { |st| st['entity'] }

      assert_response :ok
      assert_equal 2, channels.size
      assert_equal [@channel1.id, @channel2.id], channels.collect { |c| c['id'] }
      assert_not_includes channels.collect { |c| c['id'] }, @channel3.id
    end

    test 'lists all the items in a structure to admin user when accessing from cms' do
      setup_for_channel_carousal
      admin_user = create(:user, organization: @org, organization_role: 'admin')
      api_v2_sign_in(admin_user)

      get :all_items, structure_id: @structure.id, is_cms: 'true', format: :json
      resp = get_json_response(response)
      assert_equal [@st_1, @st_2, @st_3].map(&:id), resp['structured_items'].collect { |st| st['id'] }
    end

    test 'retrieve channels in a structure in a specific order' do
      setup_for_channel_carousal

      # current user now follows @channel2 and @channel3 which are private channels.
      create(:follow, followable: @channel2, user: @user)
      create(:follow, followable: @channel3, user: @user)

      _structured_items = @structure.structured_items
      @st_1.update(position: 3)
      @st_2.update(position: 1)
      @st_3.update(position: 2)

      api_v2_sign_in(@user)
      get :index, structure_id: @structure.id, format: :json

      resp = get_json_response(response)
      channel_ids = resp['structured_items'].collect { |st| st['entity']['id'] }
      assert_response :ok
      assert_equal [@channel2.id, @channel3.id, @channel1.id], channel_ids
    end

    test 'unauthorizes user if not signed in' do
      setup_for_channel_carousal
      get :index, structure_id: @structure.id, format: :json
      assert_response :unauthorized
    end

    test 'should return 404 when passed structure_id is not present' do
      api_v2_sign_in(@user)
      get :index, structure_id: -1, format: :json
      assert_response :not_found
    end

    # ---------------------------- For Cards -----------------------------------
    def setup_for_card_carousal
      @author = create(:user, organization: @org)
      @card_1 = create(:card, author: @author, organization: @org)
      @card_2 = create(:card, author: @author, organization: @org)
      @card_3 = create(:card, author: @author, organization: @org)
      @structure = create(:structure, organization: @org, creator: @user, current_context: 'channel', current_slug: 'cards')

      [@card_1, @card_2, @card_3].each { |card| @structure.structured_items.create(entity: card) }

      @st_1 = StructuredItem.find_by(entity_id: @card_1.id)
      @st_2 = StructuredItem.find_by(entity_id: @card_2.id)
      @st_3 = StructuredItem.find_by(entity_id: @card_3.id)
    end

    test 'should fetch cards only if user has access to them' do
      setup_for_card_carousal

      private_card = create(:card, author: @author, organization: @org, is_public: false)
      @structure.structured_items.create(entity: private_card)

      api_v2_sign_in(@user)
      get :index, structure_id: @structure.id, format: :json

      resp = get_json_response(response)
      card_ids = resp['structured_items'].map { |d| d['entity']['id'].to_i }
      assert_response :ok
      assert_equal [@card_1.id, @card_2.id, @card_3.id], card_ids
      refute_includes card_ids, private_card.id
    end

    test 'should not trigger visibility check if the logged in user is the admin' do
      setup_for_card_carousal
      admin_user = create(:user, organization_role: 'admin', organization: @org)
      api_v2_sign_in(admin_user)

      Card.any_instance.expects(:visible_to_userV2).never
      get :index, structure_id: @structure.id, format: :json
      assert_response :ok
    end

    test 'should return cards in ascending order of structured items for channels' do
      setup_for_card_carousal
      @st_3.update(position: 1)
      @st_1.update(position: 2)
      @st_2.update(position: 3)

      api_v2_sign_in(@user)

      get :index, structure_id: @structure.id, format: :json
      resp = get_json_response(response)
      assert_response :ok
      assert_same_elements [@card_3.id, @card_1.id, @card_2.id], resp['structured_items'].map { |st| st['entity']['id'].to_i }
    end

    test 'should not trigger visibility check if there are no cards to be processed' do
      structure = create(:structure, organization: @org, creator: @user, current_context: 'channel', current_slug: 'cards')
      api_v2_sign_in(@user)

      Card.any_instance.expects(:visible_to_userV2).never
      get :index, structure_id: structure.id, format: :json
      assert_response :ok
    end

    test 'should not return dismissed cards in a carousal' do
      setup_for_card_carousal

      create(:dismissed_content, content_id: @card_1.id, content_type: 'Card', user_id: @user.id)

      api_v2_sign_in(@user)
      get :index, structure_id: @structure.id, format: :json

      resp = get_json_response(response)
      card_ids = resp['structured_items'].map { |d| d['entity']['id'].to_i }
      assert_response :ok
      assert_same_elements [@card_2.id, @card_3.id], card_ids
      refute_includes card_ids, @card_1.id
    end

    test 'should return dismissed cards also if the request is from cms' do
      setup_for_card_carousal

      create(:dismissed_content, content_id: @card_1.id, content_type: 'Card', user_id: @user.id)

      api_v2_sign_in(@user)
      get :index, structure_id: @structure.id, is_cms: true, format: :json

      resp = get_json_response(response)
      card_ids = resp['structured_items'].map { |d| d['entity']['id'].to_i }
      assert_response :ok
      assert_equal 3, card_ids.size
      assert_same_elements [@card_1.id, @card_2.id, @card_3.id], card_ids
    end

    # ----------------------------- For Users ----------------------------------
    def setup_for_user_carousal
      @carousel_user = create(:user, organization: @org)
      @user_structure = create(:structure, creator: @user, organization: @org, parent: @org, enabled: true, slug: 'discover-users')
      @user_structure_item = create(:structured_item, structure: @user_structure, entity: @carousel_user)
    end

    test 'list user entity' do
      setup_for_user_carousal
      api_v2_sign_in(@user)

      get :index, structure_id: @user_structure.id, format: :json

      json_response = get_json_response(response)
      user = json_response['structured_items'][0]['entity']

      assert_equal @carousel_user.id, user['id']
      assert_equal @carousel_user.handle, user['handle']
      assert_equal 'active', user['status']
      assert_same_elements ['tiny', 'small', 'medium', 'large'], user['avatarimages'].keys
      assert_nil user['expert_skills']
      assert_empty user['roles']
      assert_equal user['followers_count'], @carousel_user.followers_count
      assert_equal user['following_count'], @carousel_user.following_count
      assert_match /#{@carousel_user.name}/, user['name']
      refute user['is_following']
    end

    test 'lists all users in a carousel in specific order' do
      setup_for_user_carousal
      carousel_user2 = create(:user, organization: @org)
      carousel_user3 = create(:user, organization: @org)
      carousel_user4 = create(:user, organization: @org)

      user_structure_item2 = @user_structure.structured_items.create(entity: carousel_user2)
      user_structure_item3 = @user_structure.structured_items.create(entity: carousel_user3)
      user_structure_item4 = @user_structure.structured_items.create(entity: carousel_user4)

      @user_structure_item.update(position: 3)
      user_structure_item2.update(position: 1)
      user_structure_item3.update(position: 4)
      user_structure_item4.update(position: 2)

      api_v2_sign_in(@user)

      get :index, structure_id: @user_structure.id, format: :json
      resp = get_json_response(response)

      users = resp['structured_items'].collect { |st| st['entity']['id'] }
      assert_equal [carousel_user2.id, carousel_user4.id, @carousel_user.id, carousel_user3.id], users
    end
  end

  class CreateMethodTest < ActionController::TestCase
    setup do
      @org  = create(:organization)
      @user = create(:user, organization: @org)
      @user1 = create(:user, organization: @org)

      @card_1 = create :card, organization: @org, author: @user
      @card_2 = create :card, organization: @org, author: @user

      @structure = create :structure, current_context: 'discover', current_slug: 'cards', creator: @user, organization: @org

      @user_structure = create :structure, creator: @user, organization: @org, parent: @org, enabled: true, slug: 'discover-users'
      @channel_structure = create :structure, creator: @user, organization: @org, parent: @org, enabled: true, slug: 'discover-channels'
    end

    test 'unauthorizes user if not signed in' do
      user = create :user, organization: @org

      post :create, structure_id: @structure.id, entity_id: @card_1.id, entity_type: 'Card', format: :json

      assert_response :unauthorized
    end

    test 'add an entity to a structure' do
      api_v2_sign_in(@user)

      post :create, structure_id: @structure.id, entity_id: @card_1.id, entity_type: 'Card', format: :json

      assert_response :success
      @structure.reload
      assert_equal 1, @structure.structured_items.count
    end

    test 'add the set of entities to a structure' do
      api_v2_sign_in(@user)
      ids = [@card_1.id, @card_2.id]
      post :bulk_create, structure_id: @structure.id,
        entity_ids: ids, entity_type: 'Card', format: :json

      assert_response :success
      resp = get_json_response(response)
      assert resp.key?('new_entity_ids')
      assert_equal ids, resp['new_entity_ids']
      @structure.reload
      assert_equal 2, @structure.structured_items.count
    end

    test 'add the set of entities to a structure and get proper response' do
      api_v2_sign_in(@user)

      not_existed_card_id = 99999
      post :bulk_create, structure_id: @structure.id,
        entity_ids: [not_existed_card_id, @card_2.id], entity_type: 'Card', format: :json

      assert_response :success
      resp = get_json_response(response)
      assert resp.key?('entity_errors_ids')
      assert_equal [not_existed_card_id], resp['entity_errors_ids']
    end

    test 'add set of entities to a user structure in the order as they are received as params' do
      api_v2_sign_in(@user)

      user1, user2, user3 = create_list :user, 3, organization: @org
      # Freezed to maintain its position
      entity_ids = [user3.id, user1.id, user2.id].freeze

      post :bulk_create, structure_id: @user_structure.id,
        entity_ids: entity_ids, entity_type: 'User', format: :json

      assert_response :success
      resp = get_json_response(response)
      assert_equal entity_ids, resp['new_entity_ids']
    end

    test 'add set of entities to a channel structure in the order as they are received as params' do
      api_v2_sign_in(@user)

      channel1, channel2, channel3 = create_list :channel, 3, organization: @org
      # Freezed to maintain its position
      entity_ids = [channel3.id, channel1.id, channel2.id].freeze

      post :bulk_create, structure_id: @channel_structure.id,
        entity_ids: entity_ids, entity_type: 'Channel', format: :json

      assert_response :success
      resp = get_json_response(response)
      assert_equal entity_ids, resp['new_entity_ids']
    end

    test 'add set of entities to a card structure in the order as they are received as params' do
      api_v2_sign_in(@user)

      card1, card2, card3 = create_list :card, 3, author: @user, organization: @org
      # Freezed to maintain its position
      entity_ids = [card3.id, card1.id, card2.id].freeze

      post :bulk_create, structure_id: @structure.id,
        entity_ids: entity_ids, entity_type: 'Card', format: :json

      assert_response :success
      resp = get_json_response(response)
      assert_equal entity_ids, resp['new_entity_ids']
    end

    test 'should get 422 response for blank entity_ids parameter' do
      api_v2_sign_in(@user)
      post :bulk_create, structure_id: @structure.id, entity_ids: [], entity_type: 'Card', format: :json
      assert_response :unprocessable_entity
    end

    test '#bulk_create should get 404 response if entries does not exist for provided ids' do
      api_v2_sign_in(@user)
      post :bulk_create, structure_id: @structure.id, entity_ids: [400, 500], entity_type: 'Card', format: :json
      assert_response :not_found
    end

    test 'add user entity to a structure' do
      api_v2_sign_in(@user)

      post :create, structure_id: @user_structure.id, entity_id: @user1.id, entity_type: 'User', format: :json
      assert_response :success
      @structure.reload
      assert_equal 1, @user_structure.structured_items.count
    end
  end

  class RemoveMethodTest < ActionController::TestCase
    setup do
      @org  = create(:organization)
      @user = create(:user, organization: @org)

      @card_1 = create :card, organization: @org, author: @user
      @card_2 = create :card, organization: @org, author: @user

      @structure = create :structure, current_context: 'discover', current_slug: 'cards', creator: @user, organization: @org
      item2 = create(:structured_item, structure: @structure, entity: @card_2)
      item1 = create(:structured_item, structure: @structure, entity: @card_1)
    end

    test 'unauthorizes user if not signed in' do
      user = create :user, organization: @org
      delete :remove, structure_id: @structure.id, entity_id: @card_1.id, entity_type: 'Card', format: :json
      assert_response :unauthorized
    end

    test 'remove an entity from a structure' do
      api_v2_sign_in(@user)
      delete :remove, structure_id: @structure.id, entity_id: @card_1.id, entity_type: 'Card', format: :json
      assert_response :success
      @structure.reload
      assert_equal 1, @structure.structured_items.count
    end

    test 'remove last entity from a structure & disable structure if its the last entity of structure' do
      api_v2_sign_in(@user)
      delete :remove, structure_id: @structure.id, entity_id: @card_1.id, entity_type: 'Card', format: :json
      delete :remove, structure_id: @structure.id, entity_id: @card_2.id, entity_type: 'Card', format: :json
      assert_response :success
      @structure.reload
      assert_equal 0, @structure.structured_items.count
      assert_equal false, @structure.enabled
    end

    test 'remove the set of entities from a structure' do
      api_v2_sign_in(@user)
      ids = [@card_1.id, @card_2.id]
      delete :bulk_remove, structure_id: @structure.id,
        entity_ids: ids, entity_type: 'Card', format: :json

      assert_response :success
      resp = get_json_response(response)
      assert resp.key?('removed_entity_ids')
      assert_equal ids, resp['removed_entity_ids']
      @structure.reload
      assert_equal 0, @structure.structured_items.count
    end

    test 'remove the set of entities from a structure and get proper response' do
      api_v2_sign_in(@user)

      not_existed_card_id = 99999
      delete :bulk_remove, structure_id: @structure.id,
        entity_ids: [not_existed_card_id, @card_2.id], entity_type: 'Card', format: :json

      assert_response :success
      @structure.reload
      assert_equal 1, @structure.structured_items.count
      resp = get_json_response(response)
      assert resp.key?('entity_errors_ids')
      assert_equal [not_existed_card_id], resp['entity_errors_ids']
    end

    test 'when last structured item of the structure gets deleted, config should get updated and structure should be disabled' do
      api_v2_sign_in(@user)
      structure = create :structure, current_context: 'discover', display_name: "wow" ,current_slug: 'cards', creator: @user, organization: @org, enabled:true
      item2 = create(:structured_item, structure: structure, entity: @card_2)
      item1 = create(:structured_item, structure: structure, entity: @card_1)
      config_discover = {
        "version":"4.7",
        "discover":{
          "discover/carousel/customCarousel/card/#{structure.id}":{"id":structure.id,"defaultLabel":"#{structure.display_name}","index":0,"visible":true},
        }}.to_json

      config = create(:config, name: "OrgCustomizationConfig", value: config_discover, data_type: 'json', configable_id: @org.id, configable_type: 'Organization')
      delete :bulk_remove, structure_id: structure.id,
        entity_ids: [@card_2.id, @card_1.id], entity_type: 'Card', format: :json

      assert_response :success
      structure.reload
      structure.update_org_config
      org_customization_config = @org.configs.find_by(name: "OrgCustomizationConfig")
      parsed_value = org_customization_config&.parsed_value
      _key = "discover/carousel/customCarousel/card/#{structure.id}"

      assert_equal 0, structure.structured_items.count
      assert_equal false , structure.enabled
      assert_equal parsed_value["discover"][_key]["visible"], structure.enabled
    end

    test 'should get 422 response for blank entity_ids parameter' do
      api_v2_sign_in(@user)
      delete :bulk_remove, structure_id: @structure.id, entity_ids: [], entity_type: 'Card', format: :json
      assert_response :unprocessable_entity
    end

    test '#bulk_remoive should get 404 response if entries does not exist for provided ids' do
      api_v2_sign_in(@user)
      delete :bulk_remove, structure_id: @structure.id, entity_ids: [300, 500], entity_type: 'Card', format: :json
      assert_response :not_found
    end
  end
end

require 'test_helper'

class Api::V2::ManageEmailSendersControllerTest < ActionController::TestCase
  setup do
    @org = create(:organization)
    @admin = create(:user, organization: @org)
    @user = create(:user, organization: @org)
    @sender = create(:user, organization: @org)

    Role.create(
      name: 'admin',
      organization_id: @org.id,
      master_role: false
    )
    create(
      :role_permission, role: Role.find_by(name: 'admin'),
      name: Permissions::ADMIN_ONLY
    )
    @admin.add_role('admin')
    api_v2_sign_in(@admin)
  end

  class IndexMailerConfigTest < Api::V2::ManageEmailSendersControllerTest
    setup do
      create(:mailer_config, from_name: 'test', from_address: 'test@edcast.com', from_admin: true, organization: @org)
    end

    test 'index should contain proper keys in the response' do
      keys = %w[
        id from_name from_address is_active domain_verified
      ]
      get :index, format: :json
      assert_response :ok
      resp = get_json_response(response)
      senders_list = resp['manage_senders']

      assert_same_elements keys, senders_list.first.keys
      assert_equal 1, resp['count']
    end

    test 'index should contain mailer configs only added from admin' do
      create(:mailer_config, from_name: 'test', from_address: 'test2@edcast.com', organization: @org)
      get :index, format: :json
      assert_response :ok
      resp = get_json_response(response)

      assert_equal 1, resp['count']
    end
  end

  class CreateMailerConfigTest < Api::V2::ManageEmailSendersControllerTest
    test 'should create mailer config with proper fields' do
      mailer_config = {
        from_name: 'test',
        from_address: 'test@edcast.com'
      }
      post :create, mailer_config: mailer_config, format: :json
      assert_response :ok
      resp = get_json_response(response)

      assert_equal mailer_config[:from_name], resp['from_name']
      assert_equal mailer_config[:from_address], resp['from_address']
      refute resp['domain_verified']
      refute resp['email_verified']
    end

    test 'should set default fields for mailer config' do
      mailer_config = {
        from_name: 'test1',
        from_address: 'test1@edcast.com'
      }
      post :create, mailer_config: mailer_config, format: :json
      assert_response :ok
      config = MailerConfig.where(from_address: 'test1@edcast.com', from_admin: true).first

      assert_equal Settings.mandrill.address, config.address
      assert_equal Settings.mandrill.port, config.port
      assert_equal Settings.mandrill.user_email, config.user_name
      assert_equal Settings.mandrill.password, config.password
      assert_equal 'edcast.com', config.domain
      assert_equal Settings.mandrill.webhook_keys.first, config.webhook_keys
      assert_equal Settings.mandrill.enable_starttls_auto, config.enable_starttls_auto
      assert_equal 'plain', config.authentication
    end
  end

  class UpdateMailerConfigTest < Api::V2::ManageEmailSendersControllerTest
    setup do
      @mailer_config = create(:mailer_config, from_name: 'test', from_address: 'test@edcast.com', 
        from_admin: true, organization: @org, verification: {domain: true, email: true})
    end

    test 'should allow update only from_name and is_active' do
      params = {from_name: 'test 1', is_active: true}
      put :update, id: @mailer_config.id, mailer_config: params, format: :json
      assert_response :ok
      resp = get_json_response(response)

      assert_equal 'test 1', resp['from_name']
      assert resp['is_active']
    end

    test 'should not allow to update is_active to true if verification of domain and email is not done' do
      new_mailer_config = create(:mailer_config, from_name: 'test', from_address: 'test2@edcast.com', from_admin: true, organization: @org)
      params = {is_active: true}
      put :update, id: new_mailer_config.id, mailer_config: params, format: :json
      assert_response :ok
      resp = get_json_response(response)

      refute resp['is_active']
    end

    test 'should not allow update of default fields' do
      params = {from_admin: false}
      put :update, id: @mailer_config.id, mailer_config: params, format: :json
      assert_response :ok
      assert @mailer_config.reload.from_admin
    end
  end

  class DestroyMailerConfigTest < Api::V2::ManageEmailSendersControllerTest

    test 'should allow delete mailer config' do
      mailer_config = create(:mailer_config, from_name: 'test', from_address: 'test@edcast.com', from_admin: true, organization: @org)
      delete :destroy, id: mailer_config.id, format: :json
      assert_response :no_content
    end

    test 'should not allow to delete mailer config which is not created from admin' do
      mailer_config = create(:mailer_config, from_name: 'test', from_address: 'test@edcast.com', organization: @org)
      delete :destroy, id: mailer_config.id, format: :json
      assert_response :not_found
    end
  end

  class VerifyMailerConfigTest < Api::V2::ManageEmailSendersControllerTest
    test 'should send domain verification request to mandrill for checking domain is authenticate' do
      mailer_config = create(:mailer_config, from_name: 'test', from_address: 'test@edcast.com', from_admin: true, organization: @org)
      VerifySenderDomainJob.expects(:perform_later).with({config_id: mailer_config.id}).once

      get :verify_domain, id: mailer_config.id, format: :json
      assert_response :ok
    end
  end

  class AccessingMailerConfigTest < Api::V2::ManageEmailSendersControllerTest

    setup do
      @mailer_config = create(:mailer_config, organization: @org, from_admin: true)
      api_v2_sign_in(@user)
    end

    test '#index should forbid access for non-admin user' do
      get :index, format: :json
      assert_response :unauthorized
    end

    test '#create should forbid access for non-admin user' do
      mailer_config = {
        from_name: 'test',
        from_address: 'test@edcast.com'
      }
      post :create, mailer_config: mailer_config, format: :json
      assert_response :unauthorized
    end

    test '#update should forbid access for non-admin user' do
      mailer_config = {
        from_name: 'test1'
      }
      put :update, id: @mailer_config.id, mailer_config: mailer_config, format: :json
      assert_response :unauthorized
    end

    test '#destroy should forbid access for non-admin user' do
      delete :destroy, id: @mailer_config.id, format: :json
      assert_response :unauthorized
    end

    test '#activate should forbid access for non-admin user' do
      get :verify_domain, id: @mailer_config.id, format: :json
      assert_response :unauthorized
    end
  end
end

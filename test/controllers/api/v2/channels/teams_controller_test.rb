require 'test_helper'

class Api::V2::Channels::TeamsControllerTest < ActionController::TestCase

  setup do
    @org = create :organization
    @user = create :user, organization: @org
    @channel = create :channel, organization: @org
    @teams = create_list :team, 4, organization: @org
    api_v2_sign_in @user
    @channel.followed_teams << @teams
  end

  test ':api #index display teams following a channel' do
    get :index, channel_id: @channel.id, format: :json
    resp = get_json_response(response)
    assert_equal 4, resp['teams'].length
    assert_equal @teams.first.id, resp['teams'].first['id']
    assert_equal @teams.first.name, resp['teams'].first['name']
    assert_equal 4, resp['total']
  end

  test ':api #index display teams following a channel with limit and offset' do
    get :index, channel_id: @channel.id, limit: 2, offset: 1, format: :json
    resp = get_json_response(response)
    assert_equal 2, resp['teams'].length
    assert_equal @teams[1..2].map(&:id), resp['teams'].map{|t| t['id']}
    assert_equal 4, resp['total']
  end

  test ':api #destroy remove a group from followers list of a channel' do
    delete :destroy, channel_id: @channel.id, ids: @teams.map(&:id), format: :json
    assert_response :ok
    assert_empty @channel.teams_channels_follows.reload
  end
end

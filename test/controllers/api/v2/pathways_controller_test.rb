
require 'test_helper'

class Api::V2::PathwaysControllerTest < ActionController::TestCase
  self.use_transactional_fixtures = true

  setup do
    @user = create(:user)
    @org = @user.organization
    api_v2_sign_in @user
    unstub_background_jobs
  end

  teardown do
    stub_background_jobs
  end

  test ':api show should return assigner and due_at fields in response' do
    Analytics::CardViewMetricsRecorder.stubs(:set_initial_card_view_count).returns(0)
    pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack')
    card = create(:card, author: @user, organization: @org)
    pathway_card.add_card_to_pathway(card.id, 'Card')
    assignment = create(
      :assignment,
      assignable: pathway_card,
      assignee: @user,
      due_at: Date.parse("#{Date.current.year}/06/01")
    )
    create(:team_assignment,assignment: assignment, assignor: @user, team_id: nil)
    keys = %w(id handle full_name first_name last_name avatarimages)

    get :show, id: pathway_card.id, is_standalone_page: true, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal assignment['due_at'], resp['due_at']
    assert_same_elements keys, resp['assigner'].keys
  end

  test 'returns pathway details when pathway id provided' do
    Analytics::CardViewMetricsRecorder.stubs(:set_initial_card_view_count).returns(0)

    pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack', language: 'en')
    card = create(:card, author: @user, organization: @org)
    pathway_card.add_card_to_pathway(card.id, 'Card')

    get :show, id: pathway_card.id, is_standalone_page: false, format: :json

    assert_response :ok
    json_response = get_json_response(response)

    # Assert that there is one card in pathway
    assert_equal 1, json_response['pack_cards'].length

    # Assert that this card returns default fields
    assert_equal pathway_card['language'], json_response['language']
    assert_equal 32, json_response['pack_cards'].first.keys.length
    assert_includes json_response, 'auto_complete'
  end

  test 'returns attempted_option in object when pathway contains poll card' do
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, organization: @org, title: 'pack cover')
    card = create(:card, card_type: 'poll', author: @user, organization: @org)
    option1 = create(:quiz_question_option, label: 'a', quiz_question: card, is_correct: true)
    option2 = create(:quiz_question_option, label: 'b', quiz_question: card)
    create(:quiz_question_attempt, user: @user, quiz_question: card, selections: [option1.id])
    cover.add_card_to_pathway(card.id, 'Card')

    get :show, id: cover.id, is_standalone_page: false, format: :json

    assert_response :ok
    json_response = get_json_response(response)

    assert_equal option1.id, json_response['pack_cards'][0]['attempted_option']['id']
    assert_equal option1.is_correct, json_response['pack_cards'][0]['attempted_option']['is_correct']
    assert_equal option1.label, json_response['pack_cards'][0]['attempted_option']['label']
  end

  test 'render 404 if the id passed is not of a pathway' do
    card = create :card, author: @user, organization: @org
    pathway = create :card, author: @user, card_type: 'pack', organization: @org
    create :card_pack_relation, cover_id: pathway.id, from_id: card.id, from_type: 'Card'

    get :show, id: card.id, format: :json

    assert_response :not_found
  end

  test 'return message as `You are not authorized to access this card` for pathway children cards those are private and not accessible to the user' do
    Analytics::CardViewMetricsRecorder.stubs(:get_initial_card_view_count).returns(0)

    # create an admin user
    admin_user = create :user, organization: @org, organization_role: 'admin'
    admin_role = create :role, name: 'admin', organization: @org
    admin_role.create_role_permissions
    admin_user.add_role('admin')

    # admin_user creates cover card
    pathway = create :card, author: admin_user, card_type: 'pack', organization: @org

    # admin_user creates children cards
    c1, c2, c3, c4, c5, c6, c7, c8, c9, c10 = create_list :card, 10, author: admin_user, organization: @org

    # add these cards(c1, c2, ...) to the cover card
    Card.where.not(card_type: 'pack').each do |card|
      pathway.add_card_to_pathway(card.id, 'Card')
    end

    # mark some cards as private
    c6.update_columns(is_public: false)
    c7.update_columns(is_public: false)
    c8.update_columns(is_public: false)
    c9.update_columns(is_public: false)
    c10.update_columns(is_public: false)

    # create a channel
    channel = create :channel, organization: @org, is_open: true

    # one of the private cards will be posted in a channel
    c6.post_to_channel(channel.id)

    # one of the private card will be shared in a group
    team = create :team, organization: @org, is_private: true
    create :shared_card, card: c7, team: team
    # @user is in the group
    create :teams_user, user: @user, team: team

    # @user is following the above channel
    @user.direct_following_channels << channel

    # share the pathway to the @user
    create :card_user_share, user: @user, card: pathway

    get :show, id: pathway.id, is_standalone_page: true, format: :json

    assert_response :ok
    json_response = get_json_response(response)
    assert_includes json_response, 'auto_complete'
    assert_equal 3, json_response['pack_cards'].map {|e| e['message'] if e['message'] == 'You are not authorized to access this card'}.compact.length
  end

  test 'return not authorized message if pathway is private' do
    Analytics::CardViewMetricsRecorder.stubs(:get_initial_card_view_count).returns(0)

    pathway_author = create(:user, organization: @org)
    pathway_card = create(:card, author: pathway_author, organization: @org, card_type: 'pack', is_public: false)
    card = create(:card, author: pathway_author, organization: @org)
    pathway_card.add_card_to_pathway(card.id, 'Card')

    get :show, id: pathway_card.id, is_standalone_page: true, format: :json

    assert_response :not_found
    json_response = get_json_response(response)

    assert_equal 'You are not authorized to access this card', json_response['message']
  end

  test 'return message as `You are not authorized to access this card` if not restricted to the user' do
    Analytics::CardViewMetricsRecorder.stubs(:get_initial_card_view_count).returns(0)

    # create an admin user
    admin_user = create :user, organization: @org, organization_role: 'admin'
    admin_role = create :role, name: 'admin', organization: @org
    admin_role.create_role_permissions
    admin_user.add_role('admin')

    # admin_user creates cover card
    pathway = create :card, author: admin_user, card_type: 'pack', organization: @org

    # admin_user creates children cards
    c1, c2, c3, c4, c5 = create_list :card, 5, author: admin_user, organization: @org

    # add these cards(c1, c2, ...) to the cover card
    Card.where.not(card_type: 'pack').each do |card|
      pathway.add_card_to_pathway(card.id, 'Card')
    end

    # mark some cards as private
    c3.update_columns(is_public: false)
    c4.update_columns(is_public: false)
    c5.update_columns(is_public: false)

    # one of the private card will be shared in a group
    team = create :team, organization: @org, is_private: true
    create :shared_card, card: c3, team: team
    # @user is in the group
    create :teams_user, user: @user, team: team

    # add a random user to the group
    random_user = create :user, organization: @org
    create :teams_user, user: random_user, team: team

    # give permission(restrict) of the card to the random user of the group
    create :card_user_permission, card: c3, user: random_user

    # share the pathway to the @user
    create :card_user_share, user: @user, card: pathway

    get :show, id: pathway.id, is_standalone_page: true, format: :json

    assert_response :ok
    json_response = get_json_response(response)

    # even though c3 was in the group which was followed by the @user it is not accessible to the @user because
    # restricted users priority is more
    assert_equal 3, json_response['pack_cards'].map {|e| e['message'] if e['message'] == 'You are not authorized to access this card'}.compact.length
  end

  test 'pathway child card should be accessible if restricted to the user' do
    Analytics::CardViewMetricsRecorder.stubs(:get_initial_card_view_count).returns(0)

    # create an admin user
    admin_user = create :user, organization: @org, organization_role: 'admin'
    admin_role = create :role, name: 'admin', organization: @org
    admin_role.create_role_permissions
    admin_user.add_role('admin')

    # admin_user creates cover card
    pathway = create :card, author: admin_user, card_type: 'pack', organization: @org

    # admin_user creates children cards
    c1, c2, c3, c4, c5 = create_list :card, 5, author: admin_user, organization: @org

    # add these cards(c1, c2, ...) to the cover card
    Card.where.not(card_type: 'pack').each do |card|
      pathway.add_card_to_pathway(card.id, 'Card')
    end

    # mark some cards as private
    c3.update_columns(is_public: false)
    c4.update_columns(is_public: false)
    c5.update_columns(is_public: false)

    # one of the private card will be shared in a group
    team = create :team, organization: @org, is_private: true
    create :shared_card, card: c3, team: team
    # @user is in the group
    create :teams_user, user: @user, team: team

    # add a random user to the group
    random_user = create :user, organization: @org
    create :teams_user, user: random_user, team: team

    # give permission(restrict) of the card to the @user of the group
    create :card_user_permission, card: c3, user: @user

    # share the pathway to the @user
    create :card_user_share, user: @user, card: pathway

    get :show, id: pathway.id, is_standalone_page: true, format: :json

    assert_response :ok
    json_response = get_json_response(response)

    # c3 is restricted to the @user in the group. Hence, accessible
    assert_equal 2, json_response['pack_cards'].map {|e| e['message'] if e['message'] == 'You are not authorized to access this card'}.compact.length
  end

  test 'pathway child card should be accessible if restricted to a team and the user is in that team ' do
    Analytics::CardViewMetricsRecorder.stubs(:get_initial_card_view_count).returns(0)

    # create an admin user
    admin_user = create :user, organization: @org, organization_role: 'admin'
    admin_role = create :role, name: 'admin', organization: @org
    admin_role.create_role_permissions
    admin_user.add_role('admin')

    # admin_user creates cover card
    pathway = create :card, author: admin_user, card_type: 'pack', organization: @org

    # admin_user creates children cards
    c1, c2, c3, c4, c5 = create_list :card, 5, author: admin_user, organization: @org

    # add these cards(c1, c2, ...) to the cover card
    Card.where.not(card_type: 'pack').each do |card|
      pathway.add_card_to_pathway(card.id, 'Card')
    end

    # mark some cards as private
    c3.update_columns(is_public: false)
    c4.update_columns(is_public: false)
    c5.update_columns(is_public: false)

    # create a channel
    channel = create :channel, organization: @org, is_open: true

    # one of the private cards will be posted in a channel
    c5.post_to_channel(channel.id)

    # create a group
    team = create :team, organization: @org, is_private: true

    # @user is in the group
    create :teams_user, team: team, user: @user

    # team will follow this channel
    create :teams_channels_follow, team: team, channel: channel

    # restrict the card to the team
    create :card_team_permission, team: team, card: c5

    api_v2_sign_in @user
    get :show, id: pathway.id, is_standalone_page: true, format: :json

    assert_response :ok
    json_response = get_json_response(response)

    # c5 is restricted to a team whose part is @user. Hence, accessible
    assert_equal 2, json_response['pack_cards'].map {|e| e['message'] if e['message'] == 'You are not authorized to access this card'}.compact.length
  end

  test 'admin and author should be able to access all private cards in a pathway' do
    Analytics::CardViewMetricsRecorder.stubs(:get_initial_card_view_count).returns(0)

    # create an admin user
    admin_user = create :user, organization: @org, organization_role: 'admin'
    admin_role = create :role, name: 'admin', organization: @org
    admin_role.create_role_permissions
    admin_user.add_role('admin')

    # admin_user creates cover card
    pathway = create :card, author: @user, card_type: 'pack', organization: @org

    # admin_user creates children cards
    c1, c2, c3, c4, c5 = create_list :card, 5, author: @user, organization: @org

    # add these cards(c1, c2, ...) to the cover card
    Card.where.not(card_type: 'pack').each do |card|
      pathway.add_card_to_pathway(card.id, 'Card')
    end

    # mark some cards as private
    c3.update_columns(is_public: false)
    c4.update_columns(is_public: false)
    c5.update_columns(is_public: false)

    # admin signing in
    api_v2_sign_in admin_user
    get :show, id: pathway.id, is_standalone_page: true, format: :json

    assert_response :ok
    json_response = get_json_response(response)

    # logged in user is admin, so he/she can access all child cards of a pathway
    assert_empty json_response['pack_cards'].map {|e| e['message'] if e['message'] == 'You are not authorized to access this card'}.compact

    # author i.e. @user signing in
    api_v2_sign_in @user
    get :show, id: pathway.id, is_standalone_page: true, format: :json

    assert_response :ok
    json_response = get_json_response(response)

    # logged in user is admin, so he/she can access all child cards of a pathway
    assert_empty json_response['pack_cards'].map {|e| e['message'] if e['message'] == 'You are not authorized to access this card'}.compact
  end

  test 'should return pathway details in order' do
    # Create pathway and add cards
    pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack')
    cards = create_list(:card, 4, author: @user, organization: @org)
    cards.each {|card| pathway_card.add_card_to_pathway(card.id, 'Card')}

    get :show, id: pathway_card.id, is_standalone_page: false, format: :json
    json_response = get_json_response(response)

    assert_response :ok
    assert_equal cards.map(&:id), json_response['pack_cards'].map {|card| card['id'].to_i}

    # Reorder cards in pathway
    new_order = [cards[2].id, cards[3].id, cards[1].id, cards[0].id]
    pathway_card.relations_reorder(order: new_order)

    get :show, id: pathway_card.id, is_standalone_page: false, format: :json
    json_response = get_json_response(response)
    assert_response :ok
    assert_equal new_order, json_response['pack_cards'].map {|card| card['id'].to_i}
  end

  test 'should return empty array for children cards if not present' do
    # Create pathway
    pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack')

    get :show, id: pathway_card.id, is_standalone_page: false, format: :json
    json_response = get_json_response(response)
    assert_response :ok
    assert_empty json_response['pack_cards']
  end

  test 'returns pathway details when editing a pathway' do
    Analytics::CardViewMetricsRecorder.stubs(:set_initial_card_view_count).returns(0)

    pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack')
    card = create(:card, author: @user, organization: @org)
    pathway_card.add_card_to_pathway(card.id, 'Card')

    api_v2_sign_in @user
    get :edit, id: pathway_card.id, is_standalone_page: false, format: :json

    assert_response :ok
    json_response = get_json_response(response)

    # Assert that there is one card in pathway
    assert_equal 1, json_response['pack_cards'].length

    # Assert that this card returns necessary fields
    assert_equal 55, json_response['pack_cards'].first.keys.length
  end

  test 'returns only requested pathway details when editing a pathway' do
    Analytics::CardViewMetricsRecorder.stubs(:set_initial_card_view_count).returns(0)

    pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack')
    card = create(:card, author: @user, organization: @org)
    pathway_card.add_card_to_pathway(card.id, 'Card')

    api_v2_sign_in @user
    get :edit, id: pathway_card.id, is_standalone_page: false, fields: "id,title,slug", format: :json

    assert_response :ok
    json_response = get_json_response(response)


    # Assert that this card returns necessary fields
    assert_equal 3, json_response.keys.length
  end

  test 'returns pathway details when editing a pathway when auto_complete is true' do
    Analytics::CardViewMetricsRecorder.stubs(:set_initial_card_view_count).returns(0)

    pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack', auto_complete: true)
    card = create(:card, author: @user, organization: @org)
    pathway_card.add_card_to_pathway(card.id, 'Card')

    api_v2_sign_in @user
    get :edit, id: pathway_card.id, is_standalone_page: false, format: :json

    assert_response :ok
    json_response = get_json_response(response)

    # Assert that there is auto_complete in pathway
    assert json_response['auto_complete']
  end

  test 'returns pathway details when editing a pathway when auto_complete is false' do
    Analytics::CardViewMetricsRecorder.stubs(:set_initial_card_view_count).returns(0)

    pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack', auto_complete: false)
    card = create(:card, author: @user, organization: @org)
    pathway_card.add_card_to_pathway(card.id, 'Card')

    api_v2_sign_in @user
    get :edit, id: pathway_card.id, is_standalone_page: false, format: :json

    assert_response :ok
    json_response = get_json_response(response)

    # Assert that there is auto_complete in pathway
    assert_equal false, json_response['auto_complete']
  end

  test 'render 404 if the id passed for edit is not of a pathway' do
    card = create :card, author: @user, organization: @org
    pathway = create :card, author: @user, card_type: 'pack', organization: @org
    create :card_pack_relation, cover_id: pathway.id, from_id: card.id, from_type: 'Card'

    get :edit, id: card.id, format: :json

    assert_response :not_found
  end

  test 'should return field leaps for edit pathway' do
    Analytics::CardViewMetricsRecorder.stubs(:set_initial_card_view_count).returns(0)

    pathway = create :card, author: @user, card_type: 'pack', organization: @org

    get :edit, id: pathway.id, format: :json

    assert_response :ok
    resp = get_json_response(response)
    assert resp.key?('leaps')
  end

  test ':api edit pack cards should content info about lock status' do
    Analytics::CardViewMetricsRecorder.stubs(:set_initial_card_view_count).returns(0)

    pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack')
    card = create(:card, author: @user, organization: @org)
    card1 = create(:card, author: @user, organization: @org)
    pathway_card.add_card_to_pathway(card.id, 'Card')
    pathway_card.add_card_to_pathway(card1.id, 'Card')
    CardPackRelation.find_by(cover_id: pathway_card.id, from_id: card.id)
        .update_attributes(locked: true)

    api_v2_sign_in @user
    get :edit, id: pathway_card.id, is_standalone_page: false, format: :json

    assert_response :ok
    resp = get_json_response(response)

    assert resp['pack_cards'][0]['locked']
    assert_equal card.id, resp['pack_cards'][0]['id'].to_i
    refute resp['pack_cards'][1]['locked']
  end

  test 'should add cards to pathways' do
    NotifyUserJob.expects(:perform_later).twice
    section_1 = create :card, card_type: 'pack', published_at: DateTime.now, state: 'published', organization: @org, author: @user
    section_1_card_1 = create :card, organization: @org, author: @user
    section_1_card_2 = create :card, organization: @org, author: @user

    section_2 = create :card, card_type: 'pack', published_at: DateTime.now, state: 'published', organization: @org, author: @user
    section_2_card_1 = create :card, organization: @org, author: @user
    section_2_card_2 = create :card, organization: @org, author: @user

    params = {
      data: [
        {
          section_id: section_1.id,
          content_ids: [section_1_card_1.id, section_1_card_2.id]
        },
        {
          section_id: section_2.id,
          content_ids: [section_2_card_1.id, section_2_card_2.id]
        }
      ]
    }

    post :add_cards, params, format: :json

    cpr = CardPackRelation.all

    assert_equal 6, cpr.length

    # assert that cards are appended
    assert_same_elements [section_2_card_1.id, section_2_card_2.id], cpr.pluck(:from_id).last(2)

    #check order cards in card_pack_relation
    section_1_relation = CardPackRelation.pack_relations(cover_id: section_1.id)

    assert_equal section_1_relation.first.cover_id, section_1_relation.first.from_id

    assert_equal section_1_relation.first.to_id, section_1_relation.detect {|e| e.from_id == section_1_card_1.id}.id

    assert_equal section_1_relation.detect {|e| e.from_id == section_1_card_1.id}.to_id,
                 section_1_relation.detect {|e| e.from_id == section_1_card_2.id}.id
  end

  test 'invalid params for adding cards to pathways' do
    NotifyUserJob.expects(:perform_later).never

    section_1 = create :card, card_type: 'pack', published_at: DateTime.now, state: 'published', organization: @org, author: @user
    section_1_card_1 = create :card, organization: @org, author: @user
    section_1_card_2 = create :card, organization: @org, author: @user

    section_2 = create :card, card_type: 'pack', published_at: DateTime.now, state: 'published', organization: @org, author: @user
    section_2_card_1 = create :card, organization: @org, author: @user
    section_2_card_2 = create :card, organization: @org, author: @user

    params = {
      data: [
        {
          section_id: section_1.id,
          content_ids: [nil]
        },
        {
          section_id: nil,
          content_ids: [section_2_card_1.id, section_2_card_2.id]
        }
      ]
    }

    post :add_cards, params, format: :json

    assert_equal 2, CardPackRelation.count
  end

  test 'process ecl cards for adding cards to pathways' do
    NotifyUserJob.expects(:perform_later).once
    EclApi::CardConnector.any_instance.expects(:card_from_ecl_id).once
    ecl_id = 'ECL-this-is-ecl-id'
    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{Card.get_ecl_id(ecl_id)}").
        to_return(status: 200, body: get_ecl_card_response(Card.get_ecl_id(ecl_id)).to_json)

    section_1 = create :card, card_type: 'pack', published_at: DateTime.now, state: 'published', organization: @org, author: @user
    section_1_card_1 = create :card, organization: @org, author: @user
    section_1_card_2 = create :card, organization: @org, author: @user

    params = {
      data: [
        {
          section_id: section_1.id,
          content_ids: [section_1_card_1.id, section_1_card_2.id, ecl_id]
        }
      ]
    }
    post :add_cards, params, format: :json

    assert_response :ok
  end

  test ':api show pack cards should content info about lock status' do
    Analytics::CardViewMetricsRecorder.stubs(:set_initial_card_view_count).returns(0)

    pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack')
    card = create(:card, author: @user, organization: @org)
    card1 = create(:card, author: @user, organization: @org)
    pathway_card.add_card_to_pathway(card.id, 'Card')
    pathway_card.add_card_to_pathway(card1.id, 'Card')
    CardPackRelation.find_by(cover_id: pathway_card.id, from_id: card.id)
      .update_attributes(locked: true)

    api_v2_sign_in @user
    get :show, id: pathway_card.id, is_standalone_page: 'true', format: :json

    assert_response :ok
    resp = get_json_response(response)

    assert resp['pack_cards'][0]['locked']
    assert_equal card.id.to_s, resp['pack_cards'][0]['id']
    refute resp['pack_cards'][1]['locked']
  end

  test 'should give cover cards team information' do

    pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack', auto_complete: false)
    card = create(:card, author: @user, organization: @org)
    pathway_card.add_card_to_pathway(card.id, 'Card')

    # create two teams
    team = create :team, organization: @org
    team2 = create :team, organization: @org

    # add the card to this team
    create :shared_card, team: team, card: pathway_card
    create :shared_card, team: team2, card: pathway_card

    api_v2_sign_in @user
    get :edit, id: pathway_card.id, fields: 'channels,teams(id,label),teams', format: :json

    assert_response :ok
    resp = get_json_response(response)

    assert_equal team.name, resp['teams'][0]['label']
  end

  test 'api edit should return language field in response' do
    pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack', language: 'en')
    card = create(:card, author: @user, organization: @org)
    pathway_card.add_card_to_pathway(card.id, 'Card')
    api_v2_sign_in @user

    get :edit, id: pathway_card.id, fields: 'language', format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal pathway_card.language, resp['language']
  end

  test 'api edit should return language field in response without fields parameter' do
    Analytics::CardViewMetricsRecorder.stubs(:set_initial_card_view_count).returns(0)
    pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack', language: 'en')
    card = create(:card, author: @user, organization: @org)
    pathway_card.add_card_to_pathway(card.id, 'Card')
    api_v2_sign_in @user

    get :edit, id: pathway_card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)

    assert_equal pathway_card.language, resp['language']
  end
end

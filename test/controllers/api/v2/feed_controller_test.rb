require 'test_helper'

class Api::V2::FeedControllerTest < ActionController::TestCase
  setup do
    @user = create(:user)
    @org = @user.organization
    api_v2_sign_in(@user)
  end

  test ':api get feed for a user' do
    cards = create_list(:card, 2, author: create(:user, organization: @org))
    create(:assignment, assignable: cards.first, assignee: @user, due_at: Time.now)
    create(:card_subscription, card_id: cards.first.id, user_id: @user.id, end_date: Time.now + 1.days)
    UserContentsQueue.expects(:feed_content_for_v2).with(
      user: @user, limit: 10, offset: 0,
      filter_params: {filter_by_language: false}
    ).returns(
      [
        {'rationale' => 'rat 1', 'item' => cards.first},
        {'rationale' => 'rat 2', 'item' => cards.last}
      ]
    ).once

    get :index, fields: 'id,due_at,subscription_end_date', format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal ['cards'], resp.keys
    assert_equal cards.map(&:id), resp['cards'].map { |card| card['id'].to_i }
    assert resp['cards'].first.key?('due_at' && 'subscription_end_date')
  end

  test ':api must return is_new key for feed for a user if last_access_at was passed' do
    cards = create_list(:card, 2, author: create(:user, organization: @org))

    UserContentsQueue.expects(:feed_content_for_v2).with(
      user: @user, limit: 10, offset: 0,
      filter_params: {filter_by_language: false}
    ).returns(
      [
        {'rationale' => 'rat 1', 'item' => cards.first},
        {'rationale' => 'rat 2', 'item' => cards.last}
      ]
    ).once

    get :index, fields: 'is_new', last_access_at: (Time.now - 1.week).to_i, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_includes resp['cards'].first, 'is_new'
  end

  test ':api filter feed for a user by language' do
    @user.create_profile(language: 'ru')
    not_specified_card = create(:card, author: create(:user, organization: @org))
    en_card = create(:card, author: create(:user, organization: @org), language: 'en')
    ru_card = create(:card, author: create(:user, organization: @org), language: 'ru')
    UserContentsQueue.expects(:feed_content_for_v2).with(
      user: @user, limit: 10, offset: 0,
      filter_params: {filter_by_language: true}
    ).returns(
      [
        {'rationale' => 'rat 1', 'item' => not_specified_card},
        {'rationale' => 'rat 3', 'item' => ru_card}
      ]
    ).once
    get :index, filter_by_language: true, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal [not_specified_card.id, ru_card.id], resp['cards'].map{ |card| card['id'].to_i }
  end

  test 'feed api pagination' do
    cards = create_list(:card, 2, author: create(:user, organization: @org))

    UserContentsQueue.expects(:feed_content_for_v2).with(
      user: @user, limit: 5, offset: 10,
      filter_params: {filter_by_language: false}
    ).returns(
      [
        {'rationale' => 'rat 1', 'item' => cards.first},
        {'rationale' => 'rat 2', 'item' => cards.last}
      ]
    ).once

    get :index, limit: 5, offset: 10, format: :json
    assert_response :ok
  end

  test ':api should return count and ids of feed items' do
    cards = create_list(:card, 10, author: create(:user, organization: @org))
    from_time = (Time.current - 1.day).to_i
    search_response = Search::SearchResponse.new(
      total: 10,
      results: cards
    )

    UserContentsQueue
      .expects(:get_cards_queue_for_user)
      .with(user_id: @user.id, filter_params: {from_time: Time.at(from_time).to_datetime.utc}, method: :count)
      .returns(10)

    get :feed_items_count, from_time: "#{from_time}", format: :json
    assert_response :ok
    assert_equal 10, get_json_response(response)['items_count']
  end

  test 'should pass from_time to get items count' do
    get :feed_items_count, format: :json
    assert_response :unprocessable_entity
  end

  test 'should return pack_cards in response for native app' do
    @controller.stubs(:is_native_app?).returns(true)
    pathway = create(:card, author: @user, organization: @org, card_type: 'pack')

    UserContentsQueue.expects(:feed_content_for_v2).with(
      user: @user, limit: 5, offset: 10,
      filter_params: {filter_by_language: false}
    ).returns(
      [
        {'rationale' => 'rat 1', 'item' => pathway }
      ]
    ).once

    get :index, limit: 5, offset: 10, format: :json
    json_response = get_json_response(response)
    assert_response :ok
    assert_equal [pathway.id], json_response['cards'].map { |card| card['id'].to_i }
    assert_includes json_response['cards'].first.keys, 'pack_cards'
  end

  test 'should return journey_section in response for native app' do
    @controller.stubs(:is_native_app?).returns(true)
    journey = create(:card, author: @user, organization: @org, card_type: 'journey', card_subtype: 'self_paced')
    pathway = create(:card, author: @user, organization: @org, card_type: 'pack')
    journey.add_card_to_journey(pathway.id)

    UserContentsQueue.expects(:feed_content_for_v2).with(
      user: @user, limit: 5, offset: 10,
      filter_params: {filter_by_language: false}
    ).returns(
      [
        {'rationale' => 'rat 1', 'item' => journey }
      ]
    ).once

    get :index, limit: 5, offset: 10, format: :json
    json_response = get_json_response(response)
    assert_response :ok
    assert_equal [journey.id], json_response['cards'].map { |card| card['id'].to_i }
    assert_includes json_response['cards'].first.keys, 'journey_section'
  end
end

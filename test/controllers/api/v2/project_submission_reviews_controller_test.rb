require 'test_helper'

class Api::V2::ProjectSubmissionReviewsControllerTest < ActionController::TestCase
  test 'should not create project submission review' do
    org = create(:organization)
    submitter = create(:user, organization: org)
    reviewer = create(:user, organization: org)
    card = create(:card, organization: org,
                  author: reviewer,
                  card_type: 'project',
                  card_subtype: 'text')
    project_submission = create(:project_submission, description: "something", project_id: card.project.id, submitter_id: submitter.id)
    post :create, project_submission_review: {description: "something", status: "approved"}, project_submission_id: project_submission.id, format: :json
    assert_response :unauthorized
  end

  test ':api should create project submission review' do
    org = create(:organization)
    submitter = create(:user, organization: org)
    reviewer = create(:user, organization: org)
    api_v2_sign_in(reviewer)
    card = create(:card, organization: org,
                  author: reviewer,
                  card_type: 'project',
                  card_subtype: 'text')
    project_submission = create(:project_submission, description: "something", project_id: card.project.id, submitter_id: submitter.id)
    assert_difference -> {ProjectSubmissionReview.count} do
      post :create, project_submission_review: {description: "something"}, project_submission_id: project_submission.id, format: :json
      assert_response :ok
    end
  end

  test ':api should list project submission reviews for a project submission' do
    org = create(:organization)
    reviewer = create(:user, organization: org)
    submitter = create(:user, organization: org)
    api_v2_sign_in(submitter)
    reviewer = create(:user, organization: org)
    card = create(:card, organization: org,
                  author: reviewer,
                  card_type: 'project',
                  card_subtype: 'text',
                  project_attributes: {reviewer_id: reviewer.id})
    api_v2_sign_in(reviewer)
    project_submission = create(:project_submission, description: "something", project_id: card.project.id, submitter_id: submitter.id)
    api_v2_sign_in(submitter)
    create(:project_submission_review, description: "something", status: "approved", project_submission_id: project_submission.id, reviewer_id: reviewer.id)
    get :index, project_submission_id: project_submission.id, format: :json
    resp = get_json_response(response)
    assert_equal project_submission.id, resp["project_submission_reviews"][0]["project_submission_id"]
    assert_equal reviewer.id, resp["project_submission_reviews"][0]["reviewer_id"]
  end

  test 'should list project submission reviews for a project submission with filter, limit and offset' do
    org = create(:organization)
    reviewer = create(:user, organization: org)
    submitter = create(:user, organization: org)

    api_v2_sign_in(reviewer)

    card = create(:card, organization: org,
                  author: reviewer,
                  card_type: 'project',
                  card_subtype: 'text',
                  project_attributes: {reviewer_id: reviewer.id})

    users = create_list(:user, 10, organization: org)
    project_submission = ProjectSubmission.create(project_id: card.project.id, submitter_id: submitter.id)
    10.times do
      ProjectSubmissionReview.create(description: "something", project_submission_id: project_submission.id, reviewer_id: reviewer.id)
    end

    get :index, project_submission_id: project_submission.id, limit: 5, format: :json
    resp = get_json_response(response)
    assert_equal 5, resp["project_submission_reviews"].count
    assert_same_elements ProjectSubmissionReview.last(5).map{ |psr| psr.reviewer_id}.sort, resp["project_submission_reviews"].map {|psr| psr["reviewer_id"]}.sort

    get :index, project_submission_id: project_submission.id, limit: 5, offset: 5, format: :json
    resp = get_json_response(response)
    assert_equal 5, resp["project_submission_reviews"].count
    assert_same_elements ProjectSubmissionReview.last(5).map{ |psr| psr.reviewer_id}.sort, resp["project_submission_reviews"].map {|psr| psr["reviewer_id"]}.sort
  end
end

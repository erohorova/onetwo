require 'test_helper'

class Api::V2::EmailTemplatesControllerTest < ActionController::TestCase

  setup do
    @org = create(:organization)
    @admin = create(:user, organization: @org)
    @user = create(:user, organization: @org)
    @sender = create(:user, organization: @org)

    # create default org templates
    create_list(:email_template, 5, organization: @org)

    Role.create(
      name: 'admin',
      organization_id: @org.id,
      master_role: false
    )
    create(
      :role_permission, role: Role.find_by(name: 'admin'),
      name: Permissions::ADMIN_ONLY
    )
    @def_template = @org.email_templates.first
    @active_template = create(
      :email_template, organization: @org, creator: @admin,
      title: "#{@def_template.title}_custom_1", default_id: @def_template.id
    )
    @admin.add_role('admin')
    @sender.add_role('admin')
    api_v2_sign_in(@admin)
  end

  class IndexActionEmailTemplatesTest < Api::V2::EmailTemplatesControllerTest

    test '#index should contain proper keys in the response' do
      keys = %w[
        id title default_id creator_id language organization_id
        is_active state design created_at updated_at template_variables
        default_template_title readable_default_title subject
      ]
      sub_keys = %w[template global]
      get :index, format: :json
      assert_response :ok
      resp = get_json_response(response)
      e_templates = resp['email_templates']
      customs = e_templates.select{ |i| i['default_id'] }

      assert customs.first.keys.include?('active_pair_title')
      assert_same_elements keys, e_templates.first.keys
      assert_same_elements sub_keys, e_templates.first['template_variables'].keys
      assert 'total'.in?(resp.keys)
      assert_equal 6, e_templates.count
    end

    test '#index should support limit option' do
      # order default
      template = @org.email_templates.first
      get :index, limit: 1, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal 1, resp['email_templates'].count
      assert_equal template.id, resp['email_templates'].first['id']
      assert_equal 6, resp['total']
    end

    test '#index should support offset option' do
      # order default
      template = @org.email_templates.second
      get :index, limit: 1, offset: 1, format: :json
      assert_response :ok
      resp = get_json_response(response)['email_templates']
      assert_equal 1, resp.count
      assert_equal template.id, resp.first['id']
    end

    test '#index should skip default templates' do
      get :index, skip_default: true, format: :json
      assert_response :ok
      resp = get_json_response(response)['email_templates']
      assert_equal 1, resp.count
      assert_equal @active_template.id, resp.first['id']
    end

    test '#index should return only active custom templates' do
      get :index, is_active: 'true', skip_default: true, format: :json
      assert_response :ok
      resp = get_json_response(response)['email_templates']
      assert_equal 1, resp.count
      assert_equal @active_template.id, resp.first['id']
    end

    test '#index should return only inactive custom templates' do
      draft_template = create(
        :email_template, organization: @org, creator: @admin,
        title: "#{@def_template.title}_custom_2", default_id: @def_template.id,
        state: 'draft', is_active: false
      )
      get :index, is_active: 'false', skip_default: true, format: :json
      assert_response :ok
      resp = get_json_response(response)['email_templates']

      assert_equal 1, resp.count
      assert_equal draft_template.id, resp.first['id']
    end

    test '#index should return custom templates filtered by state' do
      draft_template = create(
        :email_template, organization: @org, creator: @admin,
        title: "#{@def_template.title}_custom_2", default_id: @def_template.id,
        state: 'draft'
      )
      get :index, skip_default: true, state: 'draft', format: :json
      assert_response :ok
      resp = get_json_response(response)['email_templates']
      assert_equal 1, resp.count
      assert_equal draft_template.id, resp.first['id']
    end

    test '#index should return an email_templates to search for :q' do
      draft_template = create(
        :email_template, organization: @org, creator: @admin,
        title: "#{@def_template.title}_custom_2", default_id: @def_template.id,
        state: 'draft'
      )
      get :index, q: draft_template.title.capitalize, format: :json
      assert_response :ok
      resp = get_json_response(response)['email_templates']
      assert_equal 1, resp.count
      assert_equal draft_template.id, resp.first['id']
    end

    test '#index should return all email_templates to search for blank params :q' do
      get :index, q: '', format: :json
      assert_response :ok
      resp = get_json_response(response)['email_templates']
      assert_equal 6, resp.count
    end
  end

  class ShowActionETemplatesTest < Api::V2::EmailTemplatesControllerTest
    test '#show should contain proper keys in the response' do
      def_template = @org.email_templates.first
      keys = %w[
        id title default_id creator_id language organization_id
        is_active state design created_at updated_at template_variables
        default_template_title readable_default_title subject
      ]
      sub_keys = %w[template global]
      get :show, id: def_template.id, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_same_elements keys, resp.keys
      assert_same_elements sub_keys, resp['template_variables'].keys
      assert_equal def_template.id, resp['id']
      assert_equal def_template.title, resp['default_template_title']
    end

    test '#show should contain proper default title for custom template' do
      get :show, id: @active_template.id, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal @def_template.title, resp['default_template_title']
    end

    test '#show should return subject for the template as well' do
      custom_template = create(
        :email_template, organization: @org, creator: @admin,
        title: "#{@def_template.title}_custom_2", default_id: @def_template.id,
        state: 'published', subject: '{{team_name}} for {{user_name}}'
      )
      get :show, id: custom_template.id, format: :json
      resp = get_json_response(response)
      assert_equal custom_template.subject, resp['subject']
    end
  end

  class CreateActionETemplatesTest < Api::V2::EmailTemplatesControllerTest
    test '#create should create template with proper fields' do
      def_template = @org.email_templates.first
      parameters = {
        title: "#{def_template.title}_custom_22", default_id: def_template.id,
        state: 'draft', content: 'custom_html', design: 'custom_json',
        language: 'en', creator_id: @admin.id, organization_id: @org.id
      }
      post :create, email_template: parameters, format: :json
      assert_response :ok
      resp = get_json_response(response)

      assert_equal "#{def_template.title}_custom_22", resp['title']
      assert_equal def_template.id, resp['default_id']
      assert_equal @org.id, resp['organization_id']
      assert_equal @admin.id, resp['creator_id']
    end

    test '#create should set creator and organization eq to current_user' do
      def_template = @org.email_templates.first
      parameters = {
        title: "#{def_template.title}_custom_22", default_id: def_template.id,
        state: 'draft', content: 'custom_html', design: 'custom_json',
        language: 'en'
      }
      post :create, email_template: parameters, format: :json
      assert_response :ok
      resp = get_json_response(response)

      assert_equal @org.id, resp['organization_id']
      assert_equal @admin.id, resp['creator_id']
    end

    test '#create should allow to set custom subject as well' do
      def_template = @org.email_templates.first
      parameters = {
        title: "#{def_template.title}_custom_22", default_id: def_template.id,
        state: 'draft', content: 'custom_html', design: 'custom_json',
        language: 'en', subject: 'welcome {{user_name}} to {{team_name}}'
      }
      post :create, email_template: parameters, format: :json
      resp = get_json_response(response)

      assert_equal @org.id, resp['organization_id']
      assert_equal parameters[:subject], resp['subject']
    end
  end

  class DestroyActionETemplatesTest < Api::V2::EmailTemplatesControllerTest

    test '#destory should allow delete only custom template' do
      def_template = @org.email_templates.first
      delete :destroy, id: def_template.id, format: :json
      assert_response :unprocessable_entity
    end

    test '#destroy should destroy custom template' do
      def_template = @org.email_templates.first
      template = create(
        :email_template, organization: @org, creator: @admin,
        title: "#{def_template}_custom_1", default_id: def_template.id
      )
      delete :destroy, id: template.id, format: :json
      assert_response :no_content
    end
  end

  class UpdateActionETemplatesTest < Api::V2::EmailTemplatesControllerTest
    test '#update should allow update only custom template' do
      def_template = @org.email_templates.first
      params = { content: 'custom_html' }
      put :update, id: def_template.id, email_template: params, format: :json
      assert_response :unprocessable_entity
    end

    test '#update should allow updating subject of custom templates' do
      def_template = @org.email_templates.first
      template = create(
        :email_template, organization: @org, creator: @admin,
        title: "#{def_template.title}_custom_2", default_id: def_template.id,
        subject: "Card on {{team_name}} for {{user_name}}"
      )
      params = { subject: "Pathway on {{team_name}} for {{user_name}}" }
      
      put :update, id: template.id, email_template: params, format: :json
      resp = get_json_response(response)
      assert_equal params[:subject], resp['subject']
    end
  end

  class ActivateActionETemplatesTest < Api::V2::EmailTemplatesControllerTest

    test '#activate should activate email template with the published state' do
      custom_template = create(
        :email_template, organization: @org, creator: @admin,
        title: "#{@def_template.title}_custom_2", default_id: @def_template.id,
        is_active: false
      )
      get :activate, id: custom_template.id, format: :json
      assert_response :ok
      resp = get_json_response(response)
      [custom_template, @active_template].each(&:reload)
      assert custom_template.is_active?
      refute @active_template.is_active?
      assert resp.keys.include?('disabled')
      assert_equal @active_template.id, resp['disabled'].first['id']

    end

    test '#activate should return an error if email template in the draft state' do
      custom_template = create(
        :email_template, organization: @org, creator: @admin,
        title: "#{@def_template.title}_custom_2", default_id: @def_template.id,
        is_active: false, state: 'draft'
      )
      get :activate, id: custom_template.id, format: :json
      assert_response :unprocessable_entity
    end
  end

  class DisableActionETemplatesTest < Api::V2::EmailTemplatesControllerTest

    test '#disable should disable template' do
      custom_template = create(
        :email_template, organization: @org, creator: @admin,
        title: "#{@def_template.title}_custom_2", default_id: @def_template.id,
        is_active: true, state: 'published'
      )
      get :disable, id: custom_template.id, format: :json
      assert_response :ok
      custom_template.reload
      resp = get_json_response(response)
      assert_equal custom_template.id, resp['id']
      refute custom_template.is_active?
      assert_equal 'draft', resp['state']
    end
  end

  class AccessingETemplatesTest < Api::V2::EmailTemplatesControllerTest

    setup do
      api_v2_sign_in(@user)
    end

    test 'should have the proper message in response when a user has no permissions to access' do
      get :index, format: :json
      assert_response :unauthorized
      resp = get_json_response(response)
      assert_equal 'Permission denied', resp['message']
    end

    test '#index should forbid access for non-admin user' do
      get :index, format: :json
      assert_response :unauthorized
    end

    test '#show should forbid access for non-admin user' do
      template = @org.email_templates.first
      get :show, id: template.id, format: :json
      assert_response :unauthorized
    end

    test '#create should forbid access for non-admin user' do
      parameters = {
        title: 'title_custom_1', default_id: 1, language: 'en',
        state: 'draft', content: 'custom_html', design: 'custom_json'
      }
      post :create, email_template: parameters, format: :json
      assert_response :unauthorized
    end

    test '#update should forbid access for non-admin user' do
      parameters = {
        title: 'title_custom_1'
      }
      put :update, id: 2, email_template: parameters, format: :json
      assert_response :unauthorized
    end

    test '#destroy should forbid access for non-admin user' do
      delete :destroy, id: 1, format: :json
      assert_response :unauthorized
    end

    test '#activate should forbid access for non-admin user' do
      get :activate, id: 1, format: :json
      assert_response :unauthorized
    end

    test '#send_custom_template should forbid access for non-admin user' do
      post :send_custom_template, format: :json
      assert_response :unauthorized
    end

    test '#disable should forbid access for non-admin user' do
      get :disable, id: 1, format: :json
      assert_response :unauthorized
    end
  end

  class SendETemplatesTest < Api::V2::EmailTemplatesControllerTest

    setup do
      @email_template = EmailTemplate.create(
        is_active: false,
        title: 'title', content: 'content',
        organization: @org, design: 'design', language: 'en'
      )
      api_v2_sign_in(@sender)
    end

    test '#send_custom_template should sent template to user email' do
      mailer = mock
      mailer.expects(:deliver_later).once
      TestTemplateMailer.expects(:send_custom_template).with(
        template_content: @email_template.content,
        title: @email_template.title,
        user_id: @sender.id
      ).returns(mailer)
      params = { content: @email_template.content, title: @email_template.title }
      post :send_custom_template, params, format: :json
      assert_response :ok
    end
  end
end

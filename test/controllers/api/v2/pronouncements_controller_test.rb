require 'test_helper'

class Api::V2::PronouncementsControllerTest < ActionController::TestCase

  setup do
    @org = create(:organization)
    @team = create(:team, organization: @org)
    @user = create(:user, organization: @org)
    api_v2_sign_in(@user)
  end

  class PronouncementIndexControllerTest < Api::V2::PronouncementsControllerTest
    def setup_pronouncement_data
      create(:pronouncement, scope: @org, label: 'Label 1', start_date: Time.now - 2.days, end_date: Time.now - 1.days)
      create(:pronouncement, scope: @org, label: 'Label 2', start_date: Time.now + 2.days, end_date: Time.now + 3.days)
      create(:pronouncement, scope: @org, label: 'Label 3', start_date: Time.now + 2.days, is_active: false)

      create(:pronouncement, scope: @org, label: 'Label 4', start_date: Time.now + 2.days, is_active: false)
      create(:pronouncement, scope: @org, label: 'Label 5', start_date: Time.now - 2.days)
      create(:pronouncement, scope: @org, label: 'Label 6', start_date: Time.now)

      create(:pronouncement, scope: @team, label: 'Label 1', start_date: Time.now - 2.days, end_date: Time.now - 1.days)
      create(:pronouncement, scope: @team, label: 'Label 2', start_date: Time.now + 2.days, end_date: Time.now + 3.days)
    end

    test ":api should return announcements for org and which are active when is_active parameter value is true" do
      setup_pronouncement_data

      get :index, pronouncement: {scope_id: @org.id, scope_type: "Organization", is_active: "true"}, format: :json
      resp = get_json_response response
      assert_equal 2, resp["pronouncements"].length
      assert_same_elements @org.pronouncements.live_pronouncement.pluck(:id), resp["pronouncements"].map{|a| a["id"]}
    end

    test "should return all announcements for org when is_active parameter value is false" do
      setup_pronouncement_data

      get :index, pronouncement: {scope_id: @org.id, scope_type: "Organization"}, limit: 20, format: :json
      resp = get_json_response response
      assert_equal 6, resp["pronouncements"].length
      assert_same_elements @org.pronouncements.pluck(:id), resp["pronouncements"].map{|a| a["id"]}
    end

    test "should return announcements for team and which are active when is_active parameter value is true" do
      setup_pronouncement_data

      get :index, pronouncement: {scope_id: @team.id, scope_type: "Team", is_active: "true"}, format: :json
      resp = get_json_response response
      assert_empty resp["pronouncements"]
      assert_same_elements @team.pronouncements.live_pronouncement.pluck(:id), resp["pronouncements"].map{|a| a["id"]}
    end

    test "should return all announcements for team when is_active parameter value is false" do
      setup_pronouncement_data

      get :index, pronouncement: {scope_id: @team.id, scope_type: "Team"}, limit: 20, format: :json
      resp = get_json_response response
      assert_equal 2, resp["pronouncements"].length
      assert_same_elements @team.pronouncements.pluck(:id), resp["pronouncements"].map{|a| a["id"].to_i}
    end

    test 'should return 422 when scope_type not passed to index' do
      get :index, pronouncement: {scope_id: @org.id}, format: :json
      assert_response :unprocessable_entity
    end

    test 'should return 422 when scope_id not passed to index' do
      get :index, pronouncement: {scope_type: "Team"}, format: :json
      assert_response :unprocessable_entity
    end
  end

  class PronouncementUpdateControllerTest < Api::V2::PronouncementsControllerTest
    test ':api should update existing announcement' do
      start_date = (Time.now + 2.days).utc.strftime("%m/%d/%Y %H:%M:%S")
      end_date = (Time.now + 10.days).utc.strftime("%m/%d/%Y %H:%M:%S")
      pronouncement = create(:pronouncement, scope: @org, label: 'Label 1', start_date: Time.now - 2.days, end_date: Time.now - 1.days)

      put :update, id: pronouncement.id, pronouncement: {label: "This is just for test. Don't worry.", start_date: (Time.now + 2.days).utc.strftime("%m/%d/%Y %H:%M:%S"), end_date: (Time.now + 10.days).utc.strftime("%m/%d/%Y %H:%M:%S"), is_active: false}, format: :json
      resp = get_json_response response
      assert_equal "This is just for test. Don't worry.", resp['label']
      assert_equal start_date, resp['start_date'].to_datetime.utc.strftime("%m/%d/%Y %H:%M:%S")
      assert_equal end_date, resp['end_date'].to_datetime.utc.strftime("%m/%d/%Y %H:%M:%S")
      refute resp['is_active']
    end
  end

  class PronouncementCreateControllerTest < Api::V2::PronouncementsControllerTest
    test ':api create announcement and same should be returned with approapriate params' do
      start_date = (Time.now + 2.days).utc.strftime("%m/%d/%Y %H:%M:%S")
      end_date   = (Time.now + 20.days).utc.strftime("%m/%d/%Y %H:%M:%S")

      post :create, pronouncement: {scope_id: @team.id, scope_type: "Team", label: "This is just for test. Don't worry.", start_date: start_date, end_date: end_date, is_active: false}, format: :json

      resp = get_json_response response
      pronouncement =  Pronouncement.last
      assert_equal "This is just for test. Don't worry.", resp['label']
      assert_equal start_date, resp['start_date'].to_datetime.utc.strftime("%m/%d/%Y %H:%M:%S")
      assert_equal end_date, resp['end_date'].to_datetime.utc.strftime("%m/%d/%Y %H:%M:%S")
      refute resp['is_active']
      assert_equal @team, pronouncement.scope
    end

    test "create announcement without end_date and is_active params, in response should return default values for them" do
      start_date =  (Time.now + 2.days).utc.strftime("%m/%d/%Y %H:%M:%S")
      post :create, pronouncement: {scope_id: @org.id, scope_type: "Organization", label: "This is just for test. Don't worry.", start_date: start_date}, format: :json
      resp = get_json_response response

      pronouncement =  Pronouncement.last
      assert_equal "This is just for test. Don't worry.", resp['label']
      assert_equal start_date, resp['start_date'].to_datetime.utc.strftime("%m/%d/%Y %H:%M:%S")
      assert_nil resp['end_date']
      assert resp['is_active']
      assert_equal @org, pronouncement.scope
    end

    test 'should return 422 when scope_type or label or start_date or scope_id params are not passed while announcement creation' do
      post :create, pronouncement: {scope_type: "Team"}, format: :json
      resp = get_json_response response
      assert_response :unprocessable_entity
      assert_equal "Label can't be blank, Scope can't be blank, and Start date can't be blank", resp["message"]
    end
  end

  class PronouncementDestroyControllerTest < Api::V2::PronouncementsControllerTest
    test ':api should be able to delete announcement' do
      pronouncement = create(:pronouncement)
      assert_difference -> {Pronouncement.count}, -1 do
        delete :destroy, id: pronouncement.id, format: :json
        assert_response :ok
      end
    end
  end
end

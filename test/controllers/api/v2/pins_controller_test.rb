require 'test_helper'

class Api::V2::PinsControllerTest < ActionController::TestCase
  class PinsControllerIndexTest < ActionController::TestCase
    setup do
      @user = create(:user)
      @org = @user.organization
      @user.create_profile(language: 'en')
      @author = create(:user, organization: @org)
      api_v2_sign_in(@user)
    end

    test ':api index' do
      cards = create_list(:card, 3, organization: @org, author: @author)
      channel = create(:channel, organization: @org)
      channel.curators << @user
      cards.each { |card| Pin.create(pinnable: channel, object: card, user: @user) }

      get :index, pin: { pinnable_id: channel.id, pinnable_type: 'Channel' }, format: :json
      resp = get_json_response(response)
      assert_equal 3, resp['pins'].count
      assert_equal cards.map(&:id).reverse, resp['pins'].map { |card| card['id'].to_i }
    end

    test 'should return empty array if there are no pins' do
      channel = create(:channel, organization: @org)

      get :index, pin: { pinnable_id: channel.id, pinnable_type: 'Channel' }, format: :json
      resp = get_json_response(response)
      assert_empty resp['pins']
    end

    test 'should filter cards by language' do
      card_1 = create(:card, organization: @org, author: @author, language: 'ru')
      card_2 = create(:card, organization: @org, author: @author, language: 'en')
      card_3 = create(:card, organization: @org, author: @author)
      channel = create(:channel, organization: @org)
      channel.curators << @user
      [card_1, card_2, card_3].each { |card| Pin.create(pinnable: channel, object: card, user: @user) }

      get :index, pin: { pinnable_id: channel.id, pinnable_type: 'Channel' },
          filter_by_language: true, format: :json
      resp = get_json_response(response)
      assert_equal 2, resp['pins'].count
      assert_equal [card_3.id, card_2.id], resp['pins'].map { |card| card['id'].to_i }
    end

    test 'should not return any pins when there are no trending cards' do
      channel = create(:channel, organization: @org, auto_pin_cards: true)
      Pin.expects(:fetch_featured_pins).once
      get :index, pin: { pinnable_id: channel.id, pinnable_type: "Channel" }, format: :json
      resp = get_json_response(response)
      assert_empty resp['pins']
    end

    test 'refetch and calculate featured pin cards when pins are lesser than the limit' do
      commentor = create(:user, organization: @org)
      cards = create_list(:card, 2, organization: @org, author: @author)
      channel = create(:channel, organization: @org, auto_pin_cards: true)
      channel.curators << @user
      cards.each {|card|
        create(:channels_card, card: card, channel: channel)
      }
      create_list(:comment, 5, commentable: cards[0], user_id: commentor.id)
      create_list(:comment, 3, commentable: cards[1], user_id: commentor.id)

      get :index, pin: { pinnable_id: channel.id, pinnable_type: "Channel" }, limit: 7, format: :json
      resp = get_json_response(response)
      assert_equal 2, resp['pins'].count
    end

    test 'should return teams pinned cards' do
      cards = create_list(:card, 3, organization: @org, author: @author)
      team = create(:team, organization: @org)
      cards.each { |card| Pin.create(pinnable: team, object: card, user: @user) }

      get :index, pin: { pinnable_type: 'Team', pinnable_id: team.id }, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal 3, resp['pins'].count
      assert_equal cards.map(&:id).reverse, resp['pins'].map { |card| card['id'].to_i }
    end

    test 'should return the pins in descending order i.e default sort order' do
      cards = create_list(:card, 3, organization: @org, author: @author)
      channel = create(:channel, organization: @org)
      cards.each { |card| Pin.create(pinnable: channel, object: card, user: @user) }

      get :index, pin: { pinnable_id: channel.id, pinnable_type: 'Channel' }, format: :json
      resp = get_json_response(response)
      assert_equal 3, resp['pins'].count
      assert_equal cards.map(&:id).reverse, resp['pins'].map { |card| card['id'].to_i }
    end

    test 'should return pins sorted by created_at of cards' do
      card_1 = create(:card, organization: @org, author: @author, created_at: Date.today + 1)
      card_2 = create(:card, organization: @org, author: @author, created_at: Date.today + 3)
      card_3 = create(:card, organization: @org, author: @author, created_at: Date.today + 2)
      channel = create(:channel, organization: @org)
      [card_1, card_2, card_3].each { |card| Pin.create(pinnable: channel, object: card, user: @user) }

      # Descending order
      get :index, pin: { pinnable_id: channel.id, pinnable_type: 'Channel' },
        object: { sort: 'created_at', order: 'desc' }, format: :json
      resp = get_json_response(response)
      assert_equal 3, resp['pins'].count
      assert_equal [card_2.id, card_3.id, card_1.id], resp['pins'].map { |card| card['id'].to_i }
    end

    test 'should return pins sorted by likes count of cards' do
      card_1 = create(:card, organization: @org, author: @author)
      card_2 = create(:card, organization: @org, author: @author)
      card_3 = create(:card, organization: @org, author: @author)
      channel = create(:channel, organization: @org)
      [card_1, card_2, card_3].each { |card| Pin.create(pinnable: channel, object: card, user: @user) }

      create(:content_level_metric, content_type: 'card', content_id: card_1.id, period: 'all_time', offset: 0, likes_count: 10)
      create(:content_level_metric, content_type: 'card', content_id: card_2.id, period: 'all_time', offset: 0, likes_count: 20)
      create(:content_level_metric, content_type: 'card', content_id: card_3.id, period: 'all_time', offset: 0, likes_count: 30)

      # Descending order
      get :index, pin: { pinnable_id: channel.id, pinnable_type: 'Channel' },
        object: { sort: 'likes_count', order: 'desc' }, format: :json
      resp = get_json_response(response)
      assert_equal 3, resp['pins'].count
      assert_equal [card_3.id, card_2.id, card_1.id], resp['pins'].map { |card| card['id'].to_i }
    end

    test 'should return pins sorted by views count of cards' do
      card_1 = create(:card, organization: @org, author: @author)
      card_2 = create(:card, organization: @org, author: @author)
      card_3 = create(:card, organization: @org, author: @author)
      channel = create(:channel, organization: @org)
      [card_1, card_2, card_3].each { |card| Pin.create(pinnable: channel, object: card, user: @user) }

      create(:content_level_metric, content_type: 'card', content_id: card_1.id, period: 'all_time', offset: 0, views_count: 10)
      create(:content_level_metric, content_type: 'card', content_id: card_2.id, period: 'all_time', offset: 0, views_count: 20)
      create(:content_level_metric, content_type: 'card', content_id: card_3.id, period: 'all_time', offset: 0, views_count: 30)

      # Descending order
      get :index, pin: { pinnable_id: channel.id, pinnable_type: 'Channel' },
        object: { sort: 'views_count', order: 'desc' }, format: :json
      resp = get_json_response(response)
      assert_equal 3, resp['pins'].count
      assert_equal [card_3.id, card_2.id, card_1.id], resp['pins'].map { |card| card['id'].to_i }
    end
  end

  class PinsControllerPinTest < ActionController::TestCase
    setup do
      @user = create(:user)
      @user.create_profile(language: 'ru')
      @org = @user.organization
      api_v2_sign_in(@user)
    end

    test ':api create pin card for channel' do
      author = create(:user, organization: @org)
      card = create(:card, organization: @org, author: author)
      channel = create(:channel, organization: @org)
      channel.curators << @user
      params = {
        object_id: card.id,
        object_type: 'Card',
        pinnable_id: channel.id,
        pinnable_type: 'Channel'
      }
      assert_difference -> { Pin.count } do
        post :pin, pin: params
        assert_response :ok
      end

      pin = Pin.last
      assert_equal @user.id, pin.user_id
      assert_equal channel, pin.pinnable
      assert_equal card, pin.object
    end

    test 'Unathorized: create pin card for channel for normal user' do
      author = create(:user, organization: @org)
      card = create(:card, organization: @org, author: author)
      channel = create(:channel, organization: @org)

      post :pin, pin: {object_id: card.id, object_type: 'Card', pinnable_id: channel.id, pinnable_type: "Channel"}
      assert_response :unauthorized
    end

    test 'should create pin card for channel if creator' do
      author = create(:user, organization: @org)
      card = create(:card, organization: @org, author: author)
      channel = create(:channel, organization: @org, user: @user)
      params = {
        object_id: card.id,
        object_type: 'Card',
        pinnable_id: channel.id,
        pinnable_type: 'Channel'
      }
      assert_difference -> { Pin.count } do
        post :pin, pin: params
        assert_response :ok
      end

      pin = Pin.last
      assert_equal @user.id, pin.user_id
      assert_equal channel, pin.pinnable
      assert_equal card, pin.object
    end

    test 'should create pin for bookmark and card' do
      author = create(:user, organization: @org)
      card = create(:card, organization: @org, author: author)
      bookmark = create(:bookmark, bookmarkable: card, user: @user)
      params = {
        object_id: card.id,
        object_type: 'Card',
        pinnable_id: bookmark.id,
        pinnable_type: 'Bookmark',
        format: :json
      }
      assert_difference -> { Pin.count } do
        post :pin, pin: params
        assert_response :ok
      end

      pin = Pin.last
      assert_equal @user.id, pin.user_id
      assert_equal bookmark, pin.pinnable
      assert_equal card, pin.object
    end

    test 'should removed old pin for bookmark and card and create new' do
      author = create(:user, organization: @org)
      card = create(:card, organization: @org, author: author)
      card1 = create(:card, organization: @org, author: author)
      bookmark = create(:bookmark, bookmarkable: card, user: @user)
      bookmark1= create(:bookmark, bookmarkable: card1, user: @user)
      Pin.create(pinnable: bookmark, object: card, user: @user)
      params = {
        object_id: card1.id,
        object_type: 'Card',
        pinnable_id: bookmark1.id,
        pinnable_type: 'Bookmark',
        format: :json
      }

      post :pin, pin: params
      assert_response :ok

      pin = Pin.last
      assert_equal @user.id, pin.user_id
      assert_equal bookmark1, pin.pinnable
      assert_equal card1, pin.object
      assert 1, Pin.count
    end
  end

  class PinsControllerUnPinTest < ActionController::TestCase
    setup do
      @user = create(:user)
      @user.create_profile(language: 'ru')
      @org = @user.organization
      api_v2_sign_in(@user)
    end

    test ':api destroy' do
      Pin.any_instance.stubs(:record_card_unpin_event)
      author = create(:user, organization: @org)
      card = create(:card, organization: @org, author: author)
      channel = create(:channel, organization: @org)
      channel.curators << @user
      pin = Pin.create(pinnable: channel, object: card, user: @user)

      assert_difference -> { Pin.count }, -1 do
        delete :unpin, pin: {object_id: card.id, object_type: 'Card', pinnable_id: channel.id, pinnable_type: "Channel"}
        assert_response :ok
      end
    end

    test 'returns entity_not_found if pin is missing' do
      author = create(:user, organization: @org)
      card = create(:card, organization: @org, author: author)
      channel = create(:channel, organization: @org)
      channel.curators << @user

      delete :unpin, pin: {object_id: card.id, object_type: "Card", pinnable_id: channel.id, pinnable_type: "Channel"}
      assert_response :not_found
    end
  end
end

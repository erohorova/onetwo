require 'test_helper'
class Api::V2::CardReportingsControllerTest < ActionController::TestCase
  class CardreportingCreateTest < ActionController::TestCase
    setup do
      org = create(:organization)

      @user = create(:user, organization: org)
      @user1 = create(:user, organization: org)

      @card = create(:card, author: @user, organization: org)
      api_v2_sign_in @user
    end

    test ':api should create card reporting with reason for card' do
      post :create, card_id: @card.id, reason: 'some reason', format: :json
      resp = get_json_response(response)
      assert_response :ok
    end

    test ':api should create card reporting with reason for ECL Cards' do
      ecl_id = '2a62b7d4-1bcf-4b66-a06e-3452a1602252'

      stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id}")
        .to_return(status: 200, body: get_ecl_card_response(ecl_id).to_json)

      card = create(:card, ecl_id: ecl_id)
      post :create, card_id: 'ECL-2a62b7d4-1bcf-4b66-a06e-3452a1602252', reason: 'some reason', format: :json
      resp = get_json_response(response)
      assert_response :ok
    end

    test 'should list reported cards if you are org admin' do
      org = create(:organization, enable_role_based_authorization: true)
      Role.create_standard_roles org
      admin_role = org.roles.find_by_name('admin')
      create(:role_permission, role: admin_role, name: Permissions::ADMIN_ONLY)

      user1 = create(:user, organization: org)
      user2 = create(:user, organization: org)
      user1.add_role('admin')

      card = create(:card, author: user1, organization: org)
      card_reporting1 = create(:card_reporting, card: card, user: user2)
      card_reporting2 = create(:card_reporting, card: card, user: user1)

      api_v2_sign_in user1
      get :index, state:'reported', format: :json
      resp = get_json_response(response)

      assert_response :ok
    end

    test 'should list trashed cards if you are org admin' do
      org = create(:organization, enable_role_based_authorization: true)
      Role.create_standard_roles org
      admin_role = org.roles.find_by_name('admin')
      create(:role_permission, role: admin_role, name: Permissions::ADMIN_ONLY)

      user1 = create(:user, organization: org)
      user2 = create(:user, organization: org)
      user1.add_role('admin')

      card = create(:card, author: user1, organization: org)
      card_reporting1 = create(:card_reporting, card: card, user: user2)
      card_reporting2 = create(:card_reporting, card: card, user: user1)

      api_v2_sign_in user1
      get :index, state:'reported', format: :json
      resp = get_json_response(response)

      assert_response :ok
    end

    test 'should not list reported cards only if you are not a org admin' do
      org = create(:organization, enable_role_based_authorization: true)
      Role.create_standard_roles org

      user1 = create(:user, organization: org)
      api_v2_sign_in user1

      card_reporting1 = create(:card_reporting, card: @card, user: @user, deleted_at: Date.today)
      card_reporting2 = create(:card_reporting, card: @card, user: @user1, deleted_at: Date.today)

      get :index, state:'trashed', format: :json
      resp = get_json_response(response)

      assert_response 401
    end

    test 'should not list trashed cards only if you are not a org admin' do
      org = create(:organization, enable_role_based_authorization: true)
      Role.create_standard_roles org

      user1 = create(:user, organization: org)
      api_v2_sign_in user1

      card_reporting1 = create(:card_reporting, card: @card, user: @user, deleted_at: Date.today)
      card_reporting2 = create(:card_reporting, card: @card, user: @user1, deleted_at: Date.today)

      get :index, state:'trashed', format: :json
      resp = get_json_response(response)

      assert_response 401
    end

    test 'should not list trashed cards only if you are not a org admin & RBAC not enabled' do
      get :index, state:'trashed', format: :json
      resp = get_json_response(response)
      assert_response :unauthorized
    end

    test 'should not list reported cards only if you are not a org admin & RBAC not enabled' do
      get :index, state:'reported', format: :json
      resp = get_json_response(response)
      assert_response :unauthorized
    end
  end

  class CardReportingIndexCard < ActionController::TestCase
    setup do
      skip #NO NETWORK CLASS TO ES
      WebMock.disable!
      org = create(:organization)

      @user = create(:user, organization: org)
      @user1 = create(:user, organization: org)

      @card = create(:card, author: @user, organization: org)
      api_v2_sign_in @user
    end

    teardown do
      WebMock.enable!
    end

    def setup_for_index
      FlagContent::CardReportingSearchService.create_index! force: true
      EdcastActiveJob.unstub :perform_later
      CardReportingIndexingJob.unstub :perform_later
      FlagContent::CardReportingSearchService.gateway.refresh_index!
    end

    test 'should index the card to CardReporting Index when create API gets called' do
      setup_for_index
      post :create, card_id: @card.id, reason: 'some reason', format: :json
      FlagContent::CardReportingSearchService.gateway.delete_index!
      assert_response :ok
    end
  end
end

require 'test_helper'

class Api::V2::KnowledgeGraphControllerTest < ActionController::TestCase

  setup do
    @stub_results = { foo: "bar" }
  end

  test '#knowledge_graph requires valid start_time and end_time params' do
    org = create(:organization)
    admin = create(:user, organization: org, organization_role: "admin")
    api_v2_sign_in admin

    [
      # Missing end date
      [{ start_time: "1/1/1111" }, "end_time" ],
      # Missing start date
      [{ end_time:   "1/1/1111" }, "start_time" ],
      # Invalid start date
      [{ start_time: "fake", end_time: "1/1/1111" }, "start_time" ],
      # Invalid end date
      [{ start_time: "1/1/1111", end_time: "fake" }, "end_time" ],
    ].each do |(param_set, missing_param)|
      get :knowledge_graph, param_set
      assert_equal '422', response.code
      assert_equal(
        "#{missing_param} is missing or not dd/mm/yyyy format",
        get_json_response(response)["message"]
      )
    end
  end


  test '#knowledge_graph returns error if start_time is greater than end_time' do
    org = create(:organization)
    admin = create(:user, organization: org, organization_role: "admin")
    api_v2_sign_in admin

    get :knowledge_graph, { start_time: "1/1/2020", end_time: "1/1/2019" }
    assert_equal '422', response.code
    assert_equal(
      "end_time must be greater than start_time",
      get_json_response(response)["message"]
    )
  end

  test '#knowledge graph with valid start_time and end_time params' do
    org = create(:organization)
    admin = create(:user, organization: org, organization_role: "admin")
    api_v2_sign_in admin

    start_time = "31/12/2017"
    end_time = "31/12/2018"

    KnowledgeGraphQueryService.expects(:run).with(
      org_id:          org.id,
      start_time:      DateTime.parse(start_time).utc.beginning_of_day.to_i,
      end_time:        DateTime.parse(end_time).utc.end_of_day.to_i,
      num_top_topics:  3,
      num_top_users:   50,
      current_user_id: admin.id,
      is_admin:        true,
    ).returns @stub_results

    get :knowledge_graph, start_time: start_time, end_time: end_time

    assert_equal '200', response.code
    assert_equal(@stub_results.stringify_keys, get_json_response(response))
  end

  test '#knowledge graph passes is_admin: false if user is a member' do
    org = create(:organization)
    member = create(:user, organization: org, organization_role: "member")
    api_v2_sign_in member

    start_time = "31/12/2017"
    end_time = "31/12/2018"

    KnowledgeGraphQueryService.expects(:run).with(
      org_id:          org.id,
      start_time:      DateTime.parse(start_time).utc.beginning_of_day.to_i,
      end_time:        DateTime.parse(end_time).utc.end_of_day.to_i,
      num_top_topics:  3,
      num_top_users:   50,
      current_user_id: member.id,
      is_admin:        false,
    ).returns @stub_results

    get :knowledge_graph, start_time: start_time, end_time: end_time

    assert_equal '200', response.code
    assert_equal(@stub_results.stringify_keys, get_json_response(response))
  end

  test '#knowledge graph with user_ids and team_ids params' do
    org = create(:organization)
    admin = create(:user, organization: org, organization_role: "admin")
    api_v2_sign_in admin

    start_time = "31/12/2017"
    end_time = "31/12/2018"
    user_ids = [1,2,3]
    team_ids = [4,5,6]

    KnowledgeGraphQueryService.expects(:run).with(
      org_id:          org.id,
      start_time:      DateTime.parse(start_time).utc.beginning_of_day.to_i,
      end_time:        DateTime.parse(end_time).utc.end_of_day.to_i,
      user_ids:        user_ids,
      team_ids:        team_ids,
      num_top_topics:  3,
      num_top_users:   50,
      current_user_id: admin.id,
      is_admin:        true,
    ).returns @stub_results

    get :knowledge_graph, {
      start_time: start_time,
      end_time: end_time,
      user_ids: user_ids.join(","),
      team_ids: team_ids.join(","),
    }

    assert_equal '200', response.code
    assert_equal(@stub_results.stringify_keys, get_json_response(response))
  end

  test '#knowledge graph with custom num_top_topics and num_top_users params' do
    org = create(:organization)
    admin = create(:user, organization: org, organization_role: "admin")
    api_v2_sign_in admin

    start_time = "31/12/2017"
    end_time = "31/12/2018"
    user_ids = [1,2,3]
    team_ids = [4,5,6]

    KnowledgeGraphQueryService.expects(:run).with(
      org_id:          org.id,
      start_time:      DateTime.parse(start_time).utc.beginning_of_day.to_i,
      end_time:        DateTime.parse(end_time).utc.end_of_day.to_i,
      user_ids:        user_ids,
      team_ids:        team_ids,
      num_top_topics:  99,
      num_top_users:   500,
      current_user_id: admin.id,
      is_admin:        true,
    ).returns @stub_results

    get :knowledge_graph, {
      start_time:     start_time,
      end_time:       end_time,
      user_ids:       user_ids.join(","),
      team_ids:       team_ids.join(","),
      num_top_topics: "99",
      num_top_users:  "500"
    }

    assert_equal '200', response.code
    assert_equal(@stub_results.stringify_keys, get_json_response(response))
  end


end

require 'test_helper'

class Api::V2::UserContentCompletionsControllerTest < ActionController::TestCase
  setup do
    @org = create(:organization)
    @user = create(:user, organization: @org)
    @creator = create(:user, organization: @org)

    api_v2_sign_in @user
  end

  test 'should return unprocessable_entity for invalid card type' do
    get :index, card_type: 'random', format: :json
    assert_response :unprocessable_entity
  end

  test 'should return unprocessable_entity for invalid state' do
    get :index, state: 'random', card_type: 'media', format: :json
    assert_response :unprocessable_entity
  end

  test 'should return cards based on state passed' do
    packcards = create_list(:card, 2, card_type: 'pack', author: @creator)
    
    create(:user_content_completion, completable_id: packcards.first.id, completable_type: 'Card',
      state: 'started', user_id: @user.id)
    create(:user_content_completion, completable_id: packcards.last.id, completable_type: 'Card',
      state: 'completed', user_id: @user.id)
    
    get :index, card_type: 'pack', state: 'started', format: :json
    resp = get_json_response(response)
    assert_includes resp['cards'].map{|d| d['id'].to_i}, packcards.first.id
    assert_not_includes resp['cards'].map{|d| d['id'].to_i}, packcards.last.id
  end

  test 'should return cards based on the value of the card_type passed' do
    media_cards = create_list(:card, 2, author: @creator, card_type: 'media')
    course = create(:card, card_type: 'course', author: @creator)

    media_cards.each do |card|
      create(:user_content_completion, completable_id: card.id, completable_type: 'Card',
        state: 'started',user_id:  @user.id)
    end

    get :index, card_type: 'media', format: :json
    resp = get_json_response(response)
    assert_same_elements media_cards.map(&:id), resp['cards'].map{|d| d['id'].to_i}
    assert_not_includes resp['cards'].map{|d| d['id'].to_i}, course.id
  end

  test 'should cater to limit/offset' do
    pack_cards = create_list(:card, 5, author: @creator, card_type: 'pack')

    pack_cards.each do |card|
      create(:user_content_completion, completable_id: card.id, completable_type: 'Card',
        state: 'started',user_id:  @user.id)
    end

    get :index, card_type: 'pack', limit: 4, offset: 1, format: :json
    resp = get_json_response(response)
    assert_same_elements pack_cards.last(4).map(&:id), resp['cards'].map{|d| d['id'].to_i}
    assert_not_includes resp['cards'].map{|d| d['id'].to_i}, pack_cards.first.id
    assert_equal 4, resp['cards'].length
  end
end 
require 'test_helper'

class Api::V2::ClcsControllerTest < ActionController::TestCase
  setup do
    @admin = create :user, organization_role: "admin"
    @org = @admin.organization
    @user = create(:user, organization: @org)

    api_v2_sign_in(@admin)
  end

  test ':api should not create CLC for invalid params' do
    assert_no_difference -> { Clc.count } do
      post :create, { target_score: 12 },format: :json
    end
    assert_response 422
  end

  test ':api should not create CLC without required params' do
    assert_no_difference -> { Clc.count } do
      post :create, clc: { target_score: 12 }, format: :json
    end
    assert_response :unprocessable_entity
  end

  test 'only admin can create CLC' do
    api_v2_sign_in(@user)
    post :create, clc: { target_score: 12 }, format: :json
    assert_response :unauthorized
  end

  test ':api should create CLC with valid details for org' do
    name = "Test clc"
    assert_difference -> {Clc.count}, 1 do
      post :create, clc: {name: name , from_date: Date.today.strftime("%m/%d/%Y"), to_date: (Date.today + 10.days).strftime("%m/%d/%Y"), entity_id: @org.id,
        entity_type: "Organization", target_score: 50, target_steps: "25, 50" }, format: :json
    end
    assert_response :ok
    clc = Clc.last
    assert_equal clc.name, name
    assert_equal clc.from_date, Date.today
    assert_equal @org, clc.entity
  end

  test ':api should create CLC with valid details for channel' do
    channel = create(:channel, organization: @org)
    name = "Test clc"
    assert_difference -> {Clc.count}, 1 do
      post :create, clc: {name: name, from_date: Date.today.strftime("%m/%d/%Y"), to_date: (Date.today + 10.days).strftime("%m/%d/%Y"), entity_id: channel.id,
        entity_type: "Channel", target_score: 50, target_steps: "25, 50" }, format: :json
    end
    assert_response :ok
    clc = Clc.last
    assert_equal clc.name, name
    assert_equal clc.from_date, Date.today
    assert_equal channel, clc.entity
    assert_equal clc.organization_id,  @org.id
  end

  #get_clc
  test ":api clc should return clc with for entity" do
    channel = create(:channel, organization: @org)
    clc = create(:clc,name: 'test clc', entity: channel, organization: @org)

    get :index, clc: {entity_id: channel.id, entity_type: "Channel"}, format: :json
    assert_response :ok
    resp = get_json_response(response)

    assert_equal clc.id, resp['clcs'][0]["clc_id"]
  end

  test ':api #get should not raise ActionView::Template::Error if badge does not exist' do
    org = create(:organization)
    clc_badge = create(:clc_badge, organization: org)
    clc = create(:clc, name: 'test clc', entity: org, organization: org,
      clc_badgings_attributes: [
        { badge_id: clc_badge.id, title: 'clc-25', target_steps: 25 }
      ]
    )

    Badge.last.delete

    get :index, clc: { entity_id: org.id, entity_type: 'Organization' }, format: :json
    assert_response :ok
    resp = get_json_response(response)

    assert_equal clc.id, resp['clcs'].first['clc_id']
  end

  test "should return record not found if CLC not found" do
    get :index, clc: {entity_id: 1213, entity_type: "Channel"}, format: :json
    assert_response :ok
    resp = get_json_response(response)

    assert_equal resp['total'], 0 
  end
end

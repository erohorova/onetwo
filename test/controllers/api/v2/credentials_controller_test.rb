require 'test_helper'

class Api::V2::CredentialsControllerTest < ActionController::TestCase
  setup do
    @user = create(:user)
    @edcast_user = create(:user, email: "sunil@edcast.com")
  end

  test ':api create credentials for edcast_user' do
    api_v2_sign_in(@edcast_user)
    assert_difference -> { DeveloperApiCredential.count } do
      post :create, format: :json
      assert_response :ok
    end
    recent_generated = DeveloperApiCredential.last
    assert_equal @edcast_user, recent_generated.creator
    assert_not_nil recent_generated.api_key
    assert_not_nil recent_generated.shared_secret
  end

  test 'should create credentials for user with permission Developer' do
    org = create (:organization)
    member = create(:user, organization: org)
    Role.create_standard_roles org
    member_role = Role.find_by(name: 'member')
    create(:role_permission, role: member_role, name: Permissions::DEVELOPER)
    member_role.add_user(member)
    api_v2_sign_in(member)

    assert_difference -> { DeveloperApiCredential.count } do
      post :create, format: :json
      assert_response :ok
    end
    recent_generated = DeveloperApiCredential.last
    assert_equal member, recent_generated.creator
    assert_not_nil recent_generated.api_key
    assert_not_nil recent_generated.shared_secret
  end

  test 'should return 401 for user without permissions' do
    api_v2_sign_in(@user)
    assert_no_difference -> { DeveloperApiCredential.count } do
      post :create, format: :json
      assert_response 401
    end
  end

  test ':api destroy for super_admin' do
    api_v2_sign_in(@edcast_user)
    credential = create(:developer_api_credential,
      organization_id: @edcast_user.organization_id,
      creator: @edcast_user
    )

    assert_difference -> { DeveloperApiCredential.count }, -1 do
      delete :destroy, id: credential.id, format: :json
      assert_response :ok
    end
  end

  test ":api return list of credentials" do
    @admin = create(:user, organization_id: @user.organization_id, organization_role: "admin")
    api_v2_sign_in(@admin)

    crs = create_list(:developer_api_credential, 3,
      organization_id: @user.organization_id,
      creator: @edcast_user
    )

    get :index, format: :json

    resp = get_json_response(response)
    assert_equal 3, resp["credentials"].length
  end
end

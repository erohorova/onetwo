require 'test_helper'

class Api::V2::FileResourcesControllerTest < ActionController::TestCase

  setup do
    @user = create(:user)
    @pic = Rails.root.join('test/fixtures/images/logo.png')
    @file = Rack::Test::UploadedFile.new(@pic, 'image/png')

    api_v2_sign_in(@user)
  end

  test ':api should be able to create file_resouce with valid link' do
    assert_difference ->{FileResource.count}, 1 do
      post :create, file: @file, format: :json
    end
    resp = get_json_response(response)

    assert resp["id"]
    assert resp["file_urls"]
  end

  test 'should not destroy file_resouce' do
    file_resource = create(:file_resource, user_id: @user.id)

    user2 = create(:user)
    api_v2_sign_in(user2)

    assert_no_difference -> {FileResource.count} do
      delete :destroy, id: file_resource.id, format: :json
      assert_response 404
    end
  end

  test ':api should delete file resource' do
    file_resource = create(:file_resource, user_id: @user.id)
    api_v2_sign_in(@user)
    assert_difference -> {FileResource.count}, -1 do
      delete :destroy, id: file_resource.id, format: :json
      assert_response :ok
    end
  end

  test ':api should be able to get file_resouce by id' do
    file_resource = create(:file_resource, user_id: @user.id)

    get :show, id: file_resource.id, format: :json
    resp = get_json_response(response)

    assert_response :ok

    assert_equal resp["id"], file_resource.id
    assert_equal resp["file_urls"], file_resource.fetch_attachment_urls
    assert_equal resp["filestack"], file_resource.filestack
  end
end
require 'test_helper'

class Api::V2::RecommendationsControllerTest < ActionController::TestCase

  test "Should return recommended cards" do
    org = create(:organization)
    logged_in_user = create(:user, organization: org)
    recommended_cards = create_list(:card, 2, author: create(:user, organization: org))
    card = create(:card, author: create(:user, organization: org), ecl_id: 'abc-1235')
    api_v2_sign_in logged_in_user

    recommender_ser = mock ()

    SociativeRecommenderService.expects(:new)
    .with(user_id: logged_in_user.id, content_id: card.ecl_id, tag: 'CC')
    .returns recommender_ser

    recommender_ser.expects(:recommendations)
      .returns([200, {cards: recommended_cards, content_weights: {}}])

    get :index, content_id: card.ecl_id, format: :json

    resp = get_json_response(response)

    assert_equal 2, resp['cards'].size
    assert_same_elements recommended_cards.map(&:id).map(&:to_s), resp['cards'].map {|c| c['id']}
  end

  test "Should return recommended topics" do
    org = create(:organization)
    logged_in_user = create(:user, organization: org)
    recommended_topics = [
      'edcast.health.health_and_fitness.mental_health',
      'edcast.technology.game_design_and_development.gaming_technology'
    ].each_with_object([]) do |node, memo|
        memo << {name: node, label: node.split(',').last.humanize}
    end

    card = create(:card, author: create(:user, organization: org), ecl_id: 'abc-1235')
    api_v2_sign_in logged_in_user

    recommender_ser = mock ()

    SociativeRecommenderService.expects(:new)
    .with(user_id: logged_in_user.id, content_id: card.ecl_id, tag: 'CT')
    .returns recommender_ser

    recommender_ser.expects(:recommendations).returns([200, {topics: recommended_topics}])

    get :index, content_id: card.ecl_id, tag: 'CT', format: :json

    resp = get_json_response(response)

    assert_equal 2, resp['topics'].size
    assert_same_elements recommended_topics.map {|t| t[:name]}, resp['topics'].map {|t| t['name']}
  end

  test "Should return recommended users" do
    org = create(:organization)
    logged_in_user = create(:user, organization: org)
    recommended_users = create_list(:user, 2, organization: org)

    card = create(:card, author: create(:user, organization: org), ecl_id: 'abc-1235')
    api_v2_sign_in logged_in_user

    recommender_ser = mock ()

    SociativeRecommenderService.expects(:new)
    .with(user_id: logged_in_user.id, content_id: card.ecl_id, tag: 'CU')
    .returns recommender_ser

    recommender_ser.expects(:recommendations).returns([200, {users: recommended_users}])

    get :index, content_id: card.ecl_id, tag: 'CU', format: :json

    resp = get_json_response(response)

    assert_equal 2, resp['users'].size
    assert_same_elements recommended_users.map(&:id), resp['users'].map {|u| u['id']}
  end

  test "Should return error code for invalid sociative resp" do
    org = create(:organization)
    logged_in_user = create(:user, organization: org)
    card = create(:card, author: create(:user, organization: org), ecl_id: 'abc-1235')
    api_v2_sign_in logged_in_user

    recommender_ser = mock ()

    SociativeRecommenderService.expects(:new)
    .with(user_id: logged_in_user.id, content_id: card.ecl_id, tag: 'CC')
    .returns recommender_ser

    recommender_ser.expects(:recommendations).returns([500, {error: 'Could not find recommendations'}])

    get :index, content_id: card.ecl_id, format: :json

    assert_match /Error occurred while getting content: Could not find recommendations/,
        get_json_response(response)['message']

    assert_response :unprocessable_entity
  end

  test 'should return content weight' do
    org = create(:organization)
    logged_in_user = create(:user, organization: org)
    recommended_cards = []
    recommended_cards << create(:card, author: create(:user, organization: org), ecl_id: 'abc-121==')
    recommended_cards << create(:card, author: create(:user, organization: org), ecl_id: 'abc-122==')

    card = create(:card, author: create(:user, organization: org), ecl_id: 'abc-1235')
    api_v2_sign_in logged_in_user

    recommender_ser = mock ()

    SociativeRecommenderService.expects(:new)
    .with(user_id: logged_in_user.id, content_id: card.ecl_id, tag: 'CC')
    .returns recommender_ser

    recommender_ser.expects(:recommendations).returns(
      [200, {
          cards: recommended_cards,
          content_weights: {'abc-121==' => 0.53, 'abc-122==' => 0.21}
        }
      ]
    )

    expected_hash = {'abc-121==' => 0.53, 'abc-122==' => 0.21}
    get :index, content_id: card.ecl_id, format: :json

    resp = get_json_response(response)

    actual_hash = {}
    resp['cards'].collect {|card| actual_hash[card['ecl_id']] = card['sim_wt']}

    assert_equal 2, resp['cards'].size
    assert_equal expected_hash, actual_hash
  end

  test 'should return 404 for content not found' do
    org = create(:organization)
    logged_in_user = create(:user, organization: org)
    api_v2_sign_in logged_in_user

    get :index, format: :json

    assert_response :not_found
  end
end

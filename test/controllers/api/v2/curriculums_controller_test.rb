require 'test_helper'

class Api::V2::CurriculumsControllerTest < ActionController::TestCase
  class CurricullumCreateTest < ActionController::TestCase
    setup do
      TestAfterCommit.enabled = true
      org = create(:organization)
      @user = create(:user, organization: org)
      @journey1 = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: @user)
      @journey2 = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: @user)
      api_v2_sign_in @user
    end

    test "creates pathway" do
      assert_difference -> {Card.count}, 1 do
        post :create, curriculum: { title: "Pathway", message: "Pathway message" }, format: :json
      end
      assert_response :ok
      card = Card.last
      assert_equal 'pack', card.card_type
      assert_equal 'curriculum', card.card_subtype
      assert_equal 'draft', card.state
      assert_equal card.author_id, @user.id
    end

    test "links journey with new pathway" do
      assert_difference -> {CardPackRelation.count}, 3 do
        post :create, 
          curriculum: { 
            title: "Pathway", message: "Pathway message", journey_ids: [@journey1.id, @journey2.id] 
          }, format: :json
      end
      assert_response :ok
      pack = Card.last
      relations = CardPackRelation.last(3)
      relation1 = relations[0]
      relation2 = relations[1]
      relation3 = relations[2]

      assert relations.all?{ |a| a.cover_id == pack.id }
      assert_equal [pack.id, relation2.id], [relation1.from_id, relation1.to_id]
      assert_equal [@journey1.id, relation3.id], [relation2.from_id, relation2.to_id]
      assert_equal [@journey2.id, nil], [relation3.from_id, relation3.to_id]
    end

    test "returns error when no params is passed" do
      post :create, format: :json
      
      assert_response :bad_request # 400
      assert_equal "param is missing or the value is empty: curriculum", JSON.parse(response.body)["message"]
    end

    test "returns error when pathway is invalid" do
      post :create, curriculum: { state: "draft" }, format: :json
      
      assert_response :unprocessable_entity # 422
      assert_equal "Message can't be blank", JSON.parse(response.body)["message"]
    end
   end
end
require 'test_helper'

class Api::V2::SuggestionsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    skip
    @org = create(:organization)
    @user1 = create(:user, organization: @org)

    @users = create_list(:user, 2, organization: @org)
    @channels = create_list(:channel, 2, organization: @org)
    @teams = create_list(:team, 2, organization: @org)

    create(:follow, followable: @users.first, user: @user1)
    create(:follow, followable: @channels.first, user: @user1)
    @limit = 10
    @offset = 0
    api_v2_sign_in(@user1)
  end

  test 'should get suggestions for content, users and channels results' do
    media_link_card = create(:card, card_type: 'media', card_subtype: 'link', organization: @org, author: @users[0])
    media_video_card = create(:card, card_type: 'media', card_subtype: 'video', organization: @org,author: @users[0])
    media_insight_card = create(:card, message: 'some message', card_type: 'media', card_subtype: 'text', organization: @org, author: @users[0])
    cards = [media_link_card, media_video_card, media_insight_card]
    query = "query"

    Search::UserSearch.any_instance.expects(:search).with(limit: @limit, offset: @offset, load: true, q: query, viewer: @user1).returns(Search::UserSearchResponse.new(results: @users, total: 20)).once
    Search::ChannelSearch.any_instance.expects(:search).with(limit: @limit, offset: @offset, load: true, :filter => {:include_private => true}, q: query, viewer: @user1, sort: [{:key => '_score', :order => :desc}]).returns(Search::ChannelSearchResponse.new(results: @channels, total: 10)).once

    ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items"]
    ecl_search = mock()
    EclApi::EclSearch.expects(:new).with(@org.id, @user1.id).returns(ecl_search).once
    ecl_search.expects(:search).with(has_entries(query: query, limit: @limit, offset: @offset, :query_type => 'or', load_tags: true, load_hidden: true, filter_params: has_entries({content_type: nil, source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, rank_params: {user_ids: []}}))).returns(OpenStruct.new(results: cards, aggregations: ecl_response['aggregations'])).once

    TagSuggest.expects(:suggestions_for_destiny).with({term: query, size: 10, organization_id: @org.id}).returns(['query1', 'query3']).once
    get :index, q: query, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal(['query1', 'query3'], resp['tags'].map{|r| r['name']})
    assert_not_empty resp["channels"]
    assert_not_empty resp["teams"]
    assert_not_empty resp["users"]
    assert_not_empty resp["cards"]
  end

  test "should return only channels suggestions" do
    Search::ChannelSearch.any_instance.expects(:search).with(limit: @limit, offset: @offset, load: true, :filter => {:include_private => true},  q: 'booo', viewer: @user1, sort: [{:key => '_score', :order => :desc}]).returns(Search::ChannelSearchResponse.new(results: @channels, total: 10)).once
    TagSuggest.expects(:suggestions_for_destiny).with({term: 'booo', size: 10, organization_id: @org.id}).returns([]).once
    get :index, q: 'booo', content_type: ["channel"], format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_empty resp['tags']
    assert_empty resp['users']
    assert_not_empty resp['channels']
  end

  test "should return only teams suggestions" do
    Search::TeamSearch.any_instance.expects(:search).with(limit: @limit, offset: @offset, load: true, :filter => {:skip_aggs => true},  q: 'booo', viewer: @user1, sort: [{:key => '_score', :order => :desc}]).returns(Search::ChannelSearchResponse.new(results: @teams, total: 10)).once
    TagSuggest.expects(:suggestions_for_destiny).with({term: 'booo', size: 10, organization_id: @org.id}).returns([]).once
    get :index, q: 'booo', content_type: ["team"], format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_empty resp['tags']
    assert_empty resp['users']
    assert_empty resp['channels']
    assert_empty resp['teams']
  end

  test "should return only users" do
    Search::UserSearch.any_instance.expects(:search).with(limit: @limit, offset: @offset, load: true, q: 'booo', viewer: @user1).returns(Search::UserSearchResponse.new(results: @users, total: 20)).once
    TagSuggest.expects(:suggestions_for_destiny).with({term: 'booo', size: 10, organization_id: @org.id}).returns([]).once
    get :index, q: 'booo', content_type: ["user"], format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_empty resp['tags']
    assert_empty resp['channels']

    assert_not_empty resp['users']
    assert_equal @users.map(&:handle), resp['users'].map{|u| u["handle"]}
  end

  test "should return only users searched by skills" do
    Search::UserSearch.any_instance.expects(:search).with(limit: @limit, offset: @offset, load: true, q: 'booo', viewer: @user1).returns(Search::UserSearchResponse.new(results: @users, total: 20)).once
    TagSuggest.expects(:suggestions_for_destiny).with({term: 'booo', size: 10, organization_id: @org.id}).returns([]).once
    get :index, q: 'booo', content_type: ["user"], search_by: 'skills', format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_empty resp['tags']
    assert_empty resp['channels']
    assert_not_empty resp['users']
    assert_equal @users.map(&:handle), resp['users'].map{|u| u["handle"]}
  end

  test "should return only users searched by name" do
    Search::UserSearch.any_instance.expects(:search).with(limit: @limit, offset: @offset, load: true, q: 'booo', viewer: @user1).returns(Search::UserSearchResponse.new(results: @users, total: 20)).once
    TagSuggest.expects(:suggestions_for_destiny).with({term: 'booo', size: 10, organization_id: @org.id}).returns([]).once
    get :index, q: 'booo', content_type: ["user"], search_by: 'name', format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_empty resp['tags']
    assert_empty resp['channels']
    assert_not_empty resp['users']
    assert_equal @users.map(&:handle), resp['users'].map{|u| u["handle"]}
  end

  test " should not return users if flag value of 'disable_user_search' is true" do
    create(:config, name:'disable_user_search',data_type:"boolean",value: "t",configable_type: "Organization",configable_id: @user1.organization_id, category: nil)
    TagSuggest.expects(:suggestions_for_destiny).with({term: 'booo', size: 10, organization_id: @org.id}).returns([]).once
    get :index, q: 'booo', content_type: ["user"], format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_empty resp['users']
  end
end

require 'test_helper'

class Api::V2::TranslationsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    @date = Date.new(2019, 01, 10)
    api_v2_sign_in create(:user)
  end

  teardown do
    REDIS_CLIENT.del("20190110")
  end

  test "create: creates a new redis entry for language and string" do
    Timecop.freeze(@date) do
      post :create, language: 'fr', strings: ['hello'], format: :json
      assert_response :ok
      assert_equal ['hello'], JSON.parse(REDIS_CLIENT.hget("20190110", "fr"))
    end
  end

  test "create: updates language entry with new string" do
    RedisHashKey.new(key: "20190110", field: "fr", value: "hello1").set_as_array
    Timecop.freeze(@date) do
      post :create, language: 'fr', strings: ['hello2'], format: :json
      assert_response :ok
      assert_equal ['hello1', 'hello2'], JSON.parse(REDIS_CLIENT.hget("20190110", "fr"))
    end
  end

  test "create: updates language entry with multiple strings" do
    RedisHashKey.new(key: "20190110", field: "fr", value: "hello1").set_as_array
    Timecop.freeze(@date) do
      post :create, language: 'fr', strings: ['hello1','hello2', 'hello3'], format: :json
      assert_response :ok
      assert_equal ['hello1', 'hello2', 'hello3'], JSON.parse(REDIS_CLIENT.hget("20190110", "fr"))
    end
  end
end

require 'test_helper'

class Api::V2::OrganizationsControllerTest < ActionController::TestCase

  class UserLeaderboardV2Test < ActionController::TestCase
    setup do
      @base_time = Time.now.utc
      Time.stubs(:now).returns @base_time
      @org = create :organization
      @users = create_list :user, 3, organization: @org, organization_role: "org_role"
      @user, @user2, @user3 = @users

      Organization.any_instance.stubs(:leaderboard_enabled?).returns(true)

      # Make each of the user follow 1-3 channels,
      # add them to 1-3 teams,
      # give them some unique expertise,
      # roles, and default role names
      @teams = create_list :team, 3, organization: @org
      @roles = 2.times.map do |i|
        create :role,
          organization: @org,
          name: i.to_s,
          default_name: (i.to_s + "-default")
      end

      # Create some expert topics for the users
      @topics = 3.times.map { |i| { "topic_id" => i } }

      @users.each_with_index do |user, idx|
        # Make the users each follow 1-3 channels
        (idx + 1).times do
          Follow.create!(
            followable_type: "Channel",
            user_id: user.id,
            followable: create(:channel, organization: @org)
          )
        end


        # Create profiles for each the user, with some expertise topics.
        UserProfile.create!(
          user: user,
          expert_topics: @topics.first(idx + 1)
        )

        # Create roles for th users
        # Note that one of the users will have 0 roles.
        # The organization_role will be used in that case.
        @roles.first(idx).each do |role|
          create :user_role, user: user, role: role
        end
      end

      # Make the other users follow @user
      [@user2, @user3].each do |user|
        Follow.create!(
          followable_type: "User",
          user_id: user.id,
          followable: @user
        )
      end

      # This user won't have any analytics recorded
      @other_user = create :user, organization: @org

      # Add all users to 2 teams
      @teams.first(2).each { |team| team.add_to_team [*@users, @other_user] }

      5.times do |num_days_ago|
        @users.each_with_index do |user, idx|
          UserMetricsAggregation.create!(
            user:                       user,
            organization:               @org,
            smartbites_commented_count: num_days_ago + 1 + idx,
            smartbites_completed_count: num_days_ago + 2 + idx,
            smartbites_consumed_count:  num_days_ago + 3 + idx,
            smartbites_created_count:   num_days_ago + 4 + idx,
            smartbites_liked_count:     num_days_ago + 5 + idx,
            time_spent_minutes:         num_days_ago + 6 + idx,
            total_smartbite_score:      num_days_ago + 7 + idx,
            total_user_score:           num_days_ago + 8 + idx,
            end_time:                   (@base_time - num_days_ago.days).end_of_day,
            start_time:                 (@base_time - num_days_ago.days).beginning_of_day
          )
        end
      end

      # These are the standard times returned in the results
      @start_time = (@base_time - 4.days).beginning_of_day.to_i
      @end_time = (@base_time).end_of_day.to_i

      # These user objects are referenced many times in the tests.
      # Best to make them shared variables so the code doesn't balloon in size.
      @user_attrs = -> (order_num, options = {}) do
        admin          = !!options.delete(:admin)
        order_by_score = options.delete(:order_by_score)
        order_by_score = order_by_score.nil? ? true : order_by_score
        {
          "smartbitesCommentsCount"    => 15,
          "smartbitesCompleted"        => 20,
          "smartbitesConsumed"         => 25,
          "smartbitesCreated"          => 30,
          "smartbitesLiked"            => 35,
          "timeSpent"                  => 40,
          "smartbitesScore"            => 45,
          "totalUserScore"             => 50,
          "organizationId"             => @org.id,
          "startTime"                  => @start_time.to_i,
          "endTime"                    => @end_time.to_i,
          "user" => {
            "id"                       => @user.id,
            "name"                     => @user.name,
            "email"                    => @user.email,
            "bio"                      => @user.bio,
            "handle"                   => @user.handle,
            "picture"                  => @user.photo(:medium),
            "orderByScore"             => order_num,
            "following"                => 0,
            "channelCount"             => 1,
            "groupCount"               => 2,
            "followersCount"           => 2,
            "expertTopics"             => @topics.first(1),
            "roles"                    => [admin ? "admin" : "org_role"],
            "rolesDefaultNames"        => [admin ? "admin" : "org_role"]
          }
        }.deep_merge(options).tap do |attrs|
          attrs["user"].delete("orderByScore") unless order_by_score
        end
      end

      @user2_attrs = -> (order_num, options = {}) do
        {
          "smartbitesCommentsCount"   => 20,
          "smartbitesCompleted"       => 25,
          "smartbitesConsumed"        => 30,
          "smartbitesCreated"         => 35,
          "smartbitesLiked"           => 40,
          "timeSpent"                 => 45,
          "smartbitesScore"           => 50,
          "totalUserScore"            => 55,
          "organizationId"            => @org.id,
          "startTime"                 => @start_time.to_i,
          "endTime"                   => @end_time.to_i,
          "user" => {
            "id"                      => @user2.id,
            "name"                    => @user2.name,
            "email"                   => @user2.email,
            "bio"                     => @user2.bio,
            "handle"                  => @user2.handle,
            "picture"                 => @user2.photo(:medium),
            "orderByScore"            => order_num,
            "following"               => 1,
            "channelCount"            => 2,
            "groupCount"              => 2,
            "followersCount"          => 0,
            "expertTopics"            => @topics.first(2),
            "roles"                   => @roles.first(1).map(&:name),
            "rolesDefaultNames"       => @roles.first(1).map(&:default_name)
          }
        }.deep_merge(options)
      end

      @user3_attrs = -> (order_num, options = {}) do
        {
          "smartbitesCommentsCount"   => 25,
          "smartbitesCompleted"       => 30,
          "smartbitesConsumed"        => 35,
          "smartbitesCreated"         => 40,
          "smartbitesLiked"           => 45,
          "timeSpent"                 => 50,
          "smartbitesScore"           => 55,
          "totalUserScore"            => 60,
          "organizationId"            => @org.id,
          "startTime"                 => @start_time.to_i,
          "endTime"                   => @end_time.to_i,
          "user" => {
            "id"                      => @user3.id,
            "name"                    => @user3.name,
            "email"                   => @user3.email,
            "bio"                     => @user3.bio,
            "handle"                  => @user3.handle,
            "picture"                 => @user3.photo(:medium),
            "orderByScore"            => order_num,
            "following"               => 1,
            "channelCount"            => 3,
            "groupCount"              => 2,
            "followersCount"          => 0,
            "expertTopics"            => @topics.first(3),
            "roles"                   => @roles.first(2).map(&:name),
            "rolesDefaultNames"       => @roles.first(2).map(&:default_name)
          }
        }.deep_merge(options)
      end

    end

    test 'hitting leaderboard_v2 endpoint without logging in' do
      result = get :leaderboard_v2, format: :json, show_my_result: "true"
      assert_equal 401, result.status
    end

    test "hitting leaderboard_v2 endpoint as member with default params" do
      api_v2_sign_in(@user)
      result = get :leaderboard_v2, format: :json, show_my_result: "true"
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      expected = {
        "totalCount"=> 3,
        "currentUser"=>@user_attrs.(3),
        "users"=>[
          @user3_attrs.(1),
          @user2_attrs.(2),
          @user_attrs.(3)
        ]
      }
      assert_equal expected, actual
    end

    test "total count can be greater than the number of results returned" do
      api_v2_sign_in(@user)
      result = get :leaderboard_v2, format: :json, limit: 2, offset: 1, show_my_result: "true"
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      expected = {
        "totalCount"=> 3,
        "currentUser"=>@user_attrs.(2),
        "users"=>[
          @user2_attrs.(1),
          @user_attrs.(2)
        ]
      }
      assert_equal expected, actual
    end

    test "hitting leaderboard_v2 endpoint when current user has exclude_from_leaderboard=true" do
      api_v2_sign_in(@user)
      @user.update! exclude_from_leaderboard: true
      result = get :leaderboard_v2, format: :json, show_my_result: "true"
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      expected = {
        "totalCount"=> 2,
        "currentUser"=>@user_attrs.(nil, order_by_score: false),
        "users"=>[
          @user3_attrs.(1),
          @user2_attrs.(2),
        ]
      }
      assert_equal expected, actual
    end


    test "hitting leaderboard_v2 endpoint when current user has hide_from_leaderboard=true" do
      api_v2_sign_in(@user)
      @user.update! hide_from_leaderboard: true
      result = get :leaderboard_v2, format: :json, show_my_result: "true"
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      expected = {
        "totalCount"=> 2,
        "currentUser"=>@user_attrs.(nil, order_by_score: false),
        "users"=>[
          @user3_attrs.(1),
          @user2_attrs.(2),
        ]
      }
      assert_equal expected, actual
    end

    test "leaderboard_v2 endpoint doesnt factor deleted follows into followers_count and following" do
      # set @user.followers_count to zero.
      # This also has the effect of changing @user3 and @user2 to have 0 follows.
      Follow.where(followable_type: "User", followable_id: @user.id).destroy_all

      api_v2_sign_in(@user)
      result = get :leaderboard_v2, format: :json, show_my_result: "true"
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      expected = {
        "totalCount"=> 3,
        "currentUser"=>@user_attrs.(3, { "user" => { "followersCount" => 0 } }),
        "users"=>[
          @user3_attrs.(1, { "user" => { "following" => 0 } }),
          @user2_attrs.(2, { "user" => { "following" => 0 } }),
          @user_attrs.(3, { "user" => { "followersCount" => 0 } })
        ]
      }
      assert_equal expected, actual
    end


    test "leaderboard_v2 endpoint doesnt factor deleted channel follows into channel_count" do
      # Delete all @user's follows of channels
      @user.follows.where(followable_type: "Channel").destroy_all

      api_v2_sign_in(@user)
      result = get :leaderboard_v2, format: :json, show_my_result: "true"
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      expected = {
        "totalCount"=> 3,
        "currentUser"=>@user_attrs.(3, { "user" => { "channelCount" => 0 } }),
        "users"=>[
          @user3_attrs.(1),
          @user2_attrs.(2),
          @user_attrs.(3, { "user" => { "channelCount" => 0 } })
        ]
      }
      assert_equal expected, actual
    end

    test 'returns error when given unparseable time' do
      api_v2_sign_in(@user)
      result = get :leaderboard_v2, format: :json, from_date: "123123123"
      assert_equal 422, result.status
      assert_equal(
        "cant parse from_date or to_date param",
        JSON.parse(result.body)["message"]
      )
    end

    test "excludes users with 0 total smartbite score" do
      api_v2_sign_in(@user)
      # Update all of @user3 and @user records to have 0 smartbite score.
      # This way @user3 and @user won't be included in the "users" results.
      # However @user will still be included in the "currentUser" results.
      UserMetricsAggregation.where(user: [@user, @user3]).update_all(
        total_smartbite_score: 0
      )
      result = get :leaderboard_v2, format: :json, show_my_result: "true"
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      expected = {
        "totalCount" => 1,
        "currentUser" => @user_attrs.(nil, order_by_score: false, "smartbitesScore" => 0),
        "users" => [
          @user2_attrs.(1),
        ]
      }
      assert_equal expected, actual
    end

    test "hitting leaderboard_v2 endpoint as member with show_my_result=false or nil" do
      api_v2_sign_in(@user)
      # A value of nil or false is interpreted as false
      results = [nil, "false"].map do |val|
        get :leaderboard_v2, format: :json, show_my_result: val
      end
      results.each do |result|
        assert_equal 200, result.status
        actual = JSON.parse(result.body)
        expected = {
          "totalCount"=> 3,
          "users"=>[
            @user3_attrs.(1),
            @user2_attrs.(2),
            @user_attrs.(3)
          ]
        }
        assert_equal expected, actual
      end
    end

    test "special case when limit=1 and show_my_result=true" do
      api_v2_sign_in(@user)
      result = get :leaderboard_v2, format: :json, show_my_result: "true", limit: 1
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      expected = {
        "totalCount"=> 3,
        "currentUser"=>@user_attrs.(1),
        "users"=>[
          @user_attrs.(1)
        ]
      }
      assert_equal expected, actual
    end

    test 'stores data in cache' do
      api_v2_sign_in(@user)
      expected_results = {
        "totalCount"=> 3,
        "currentUser"=>@user_attrs.(1),
        "users"=>[
          @user_attrs.(1),
          @user2_attrs.(2),
          @user3_attrs.(3)
        ]
      }
      params_code = {
        "order" => "asc",
        # Note: this key isn't present in the original params, but it gets
        # a default value added.
        "order_attr" => "smartbites_score",
        "current_user_id" => @user.id,
        "show_my_result" => "true"
      }.hash
      cache_key = "organizations/#{@org.id}/user-leaderboard/#{params_code}"
      Rails.cache.expects(:write).with(
        cache_key,
        expected_results,
        expires_in: 30.minutes
      )
      # Just to make sure the 'meaningful params' aren't empty, give a
      # non-default value for the 'order' param.
      result = get :leaderboard_v2, order: "asc", format: :json, show_my_result: "true"
      actual = JSON.parse(result.body)
      assert_equal 200, result.status
      assert_equal expected_results, actual
    end

    test 'uses cached results if available' do
      api_v2_sign_in(@user)
      stub_results = ["foobar"]
      params_code = {
        "order" => "asc",
        # Note: this key isn't present in the original params, but it gets
        # a default value added.
        "order_attr" => "smartbites_score",
        "current_user_id" => @user.id,
        "show_my_result" => "true"
      }.hash
      cache_key = "organizations/#{@org.id}/user-leaderboard/#{params_code}"
      Rails.cache.expects(:fetch).with(cache_key).returns(stub_results)
      # Just to make sure the 'meaningful params' aren't empty, give a
      # non-default value for the 'order' param.
      result = get :leaderboard_v2, order: "asc", format: :json, show_my_result: "true"
      actual = JSON.parse(result.body)
      assert_equal 200, result.status
      assert_equal stub_results, actual
    end

    test "doesnt include data for users that are deleted" do
      api_v2_sign_in(@user)
      # Destroy @user2 so their results aren't included
      @user2.destroy!
      result = get :leaderboard_v2, format: :json, show_my_result: "true"
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      expected = {
        "totalCount"=> 2,
        "currentUser"=>@user_attrs.(2, { "user" => { "followersCount" => 1 } }),
        "users"=>[
          @user3_attrs.(1),
          @user_attrs.(2, { "user" => { "followersCount" => 1 } })
        ]
      }
      assert_equal expected, actual
    end

    test "hitting leaderboard_v2 endpoint when current user has no activity" do
      api_v2_sign_in(@other_user)
      result = get :leaderboard_v2, format: :json, show_my_result: "true"
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      expected = {
        "totalCount"=> 3,
        "currentUser"=>nil,
        "users"=>[
          @user3_attrs.(1, { "user" => { "following" => 0 } }),
          @user2_attrs.(2, { "user" => { "following" => 0 } }),
          @user_attrs.(3),
        ]
      }
      assert_equal expected, actual
    end

    test "users teams are used as a filter by default" do
      api_v2_sign_in(@user)
      # Remove @user3 from all teams so they are not included in the result.
      TeamsUser.where(user: @user3).destroy_all
      result = get :leaderboard_v2, show_my_result: "true", format: :json
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      expected = {
        "totalCount"=> 2,
        "currentUser"=>@user_attrs.(2),
        "users"=> [
          @user2_attrs.(1),
          @user_attrs.(2)
        ]
      }
      assert_equal expected, actual
    end

    test "everyone team is excluded" do
      api_v2_sign_in(@user)
      # Remove @user3 from the first team
      TeamsUser.where(user: @user3, team: @teams[0]).destroy_all
      # Set the second team to is_everyone_team: true.
      # This wasy @user3 won't show up at all
      @teams[1].update is_everyone_team: true
      result = get :leaderboard_v2, show_my_result: "true", format: :json
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      expected = {
        "totalCount"=> 2,
        "currentUser"=>@user_attrs.(2),
        "users"=> [
          @user2_attrs.(1),
          @user_attrs.(2)
        ]
      }
      assert_equal expected, actual
    end

    test "hitting leaderboard_v2 endpoint with time filter" do
      api_v2_sign_in(@user)
      # We are only using 1 days worth of data here
      @start_time = (@base_time - 2.days).beginning_of_day
      start_time_str = @start_time.strftime("%m/%d/%Y")
      @end_time = (@base_time - 2.days).end_of_day
      end_time_str = @end_time.strftime("%m/%d/%Y")
      result = get :leaderboard_v2,
        from_date: start_time_str,
        to_date: end_time_str,
        format: :json,
        show_my_result: "true"
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      # All the counts are significantly lower because we take into account 1 day only
      expected = {
        "totalCount"=> 3,
        "currentUser"=> @user_attrs.(3, {
          "smartbitesCommentsCount"  => 3,
          "smartbitesCompleted"      => 4,
          "smartbitesConsumed"       => 5,
          "smartbitesCreated"        => 6,
          "smartbitesLiked"          => 7,
          "timeSpent"                => 8,
          "smartbitesScore"          => 9,
          "totalUserScore"           => 10,
        }),
        "users"=>[
          @user3_attrs.(1, {
            "smartbitesCommentsCount"  => 5,
            "smartbitesCompleted"      => 6,
            "smartbitesConsumed"       => 7,
            "smartbitesCreated"        => 8,
            "smartbitesLiked"          => 9,
            "timeSpent"                => 10,
            "smartbitesScore"          => 11,
            "totalUserScore"           => 12
          }),
          @user2_attrs.(2, {
            "smartbitesCommentsCount"  => 4,
            "smartbitesCompleted"      => 5,
            "smartbitesConsumed"       => 6,
            "smartbitesCreated"        => 7,
            "smartbitesLiked"          => 8,
            "timeSpent"                => 9,
            "smartbitesScore"          => 10,
            "totalUserScore"           => 11
          }),
          @user_attrs.(3, {
            "smartbitesCommentsCount"  => 3,
            "smartbitesCompleted"      => 4,
            "smartbitesConsumed"       => 5,
            "smartbitesCreated"        => 6,
            "smartbitesLiked"          => 7,
            "timeSpent"                => 8,
            "smartbitesScore"          => 9,
            "totalUserScore"           => 10
          })
        ]
      }
      assert_equal expected, actual
    end


    test "hitting leaderboard_v2 endpoint can use custom ordering" do
      api_v2_sign_in(@user)
      # We will sort time_spent. To differentiate this from the other test cases,
      # we can update one record that would otherwise be in the middle, to have
      # a high value for this column.
      UserMetricsAggregation.where(user_id: @user2.id).each do |agg|
        agg.update(time_spent_minutes: agg.time_spent_minutes + 200)
      end
      result = get :leaderboard_v2,
        format: :json,
        order_attr: "time_spent",
        order: "asc",
        show_my_result: "true"
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      expected = {
        "totalCount"=> 3,
        "currentUser"=>@user_attrs.(1),
        "users"=>[
          @user_attrs.(1),
          @user3_attrs.(2),
          @user2_attrs.(3, { "timeSpent" => 245 })
        ]
      }
    end

    test "group_ids param filters results by group membership" do
      # User needs to have admin permissions to use group_ids param
      @user.update organization_role: "admin"
      api_v2_sign_in(@user)
      # Delete @user3 from the first two teams.
      # Asking for data only on the first 2 teams should return only results
      # of the first two users.
      @teams.first(2).each do |team|
        team.teams_users.where(user: @user3).destroy_all
      end
      result = get :leaderboard_v2,
        format: :json,
        group_ids: @teams.first(2).map(&:id),
        show_my_result: "true"
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      expected = {
        "totalCount"=> 2,
        "currentUser"=>@user_attrs.(2, admin: true),
        "users"=>[
          @user2_attrs.(1),
          @user_attrs.(2, admin: true)
        ]
      }
      assert_equal expected, actual
    end

    test 'limit and offset params' do
      api_v2_sign_in(@user)
      result = get :leaderboard_v2,
        format: :json,
        # NOTE: cannot use a limit of 1 here!
        # If we do that, it will be caught by the special case handler
        # and only show the current user results!
        limit: 2,
        offset: 1,
        show_my_result: "true"
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      expected = {
        "totalCount"=> 3,
        # current user is not included in the 'users' list
        "currentUser"=> @user_attrs.(2),
        "users"=>[
          @user2_attrs.(1),
          @user_attrs.(2)
        ]
      }
      assert_equal expected, actual
    end

    test 'filter on active=false' do
      # We change all of user2's aggregations to have time_spent = 0.
      # Thus they are the only inactive user.
      UserMetricsAggregation.where(user: @user2).update_all(time_spent_minutes: 0)
      api_v2_sign_in(@user)
      result = get :leaderboard_v2,
        format: :json,
        active: false,
        show_my_result: "true"
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      expected = {
        "totalCount"=> 1,
        # current user is not included in the 'users' list
        "currentUser"=>@user_attrs.(nil, order_by_score: false),
        "users"=>[
          @user2_attrs.(1, { "timeSpent" => 0 })
        ]
      }
      assert_equal expected, actual
    end

    test 'filter on active=true' do
      # We change all of user2's aggregations to have time_spent = 0.
      # Thus they are the only inactive user.
      UserMetricsAggregation.where(user: @user2).update_all(time_spent_minutes: 0)
      api_v2_sign_in(@user)
      result = get :leaderboard_v2,
        format: :json,
        active: true,
        show_my_result: "true"
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      expected = {
        "totalCount"=> 2,
        "currentUser"=>@user_attrs.(2),
        "users"=>[
          @user3_attrs.(1),
          @user_attrs.(2)
        ]
      }
      assert_equal expected, actual
    end

    test 'roles param' do
      # user needs to be admin to use roles param
      @user.update organization_role: "admin"
      api_v2_sign_in(@user)
      # Query for the last role in the list.
      # Only one user (@user3) has this role.
      result = get :leaderboard_v2,
        format: :json,
        roles: [@roles[-1].name],
        show_my_result: "true"
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      expected = {
        "totalCount"=> 1,
        # current user is not included in the 'users' list
        "currentUser"=>@user_attrs.(nil, order_by_score: false, admin: true),
        "users"=>[
          @user3_attrs.(1)
        ]
      }
      assert_equal expected, actual
    end

    test 'when user is a non-admin manager' do
      # Stub the user to be a "manager"
      User
        .any_instance
        .stubs(:group_sub_admin_authorize?)
        .with(Permissions::MANAGE_GROUP_ANALYTICS)
        .returns(true)
      # Change one of the teams records to only have @user as a member
      # Also make @user a 'sub_admin' of this team.
      TeamsUser.where(team: @teams[0], user: [@user2, @user3]).destroy_all
      TeamsUser.find_by(team: @teams[0], user: @user).update(as_type: "sub_admin")
      api_v2_sign_in(@user)
      result = get :leaderboard_v2,
        format: :json,
        show_my_result: "true"
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      expected = {
        "totalCount"=> 1,
        "currentUser"=>@user_attrs.(1),
        "users"=>[
          @user_attrs.(1)
        ]
      }
      assert_equal expected, actual
    end

    test 'when roles/group_ids/expertise_names give but user isnt admin' do
      forbidden_params = [
        { roles: ["foo"] },
        { group_ids: [123] },
        { expertise_names: ["foo"] }
      ]
      api_v2_sign_in(@user)
      forbidden_params.each do |params_hash|
        result = get :leaderboard_v2, { format: :json }.merge(params_hash)
        assert_equal 401, result.status
      end
    end

    test 'expertise_names param' do
      # user needs to be admin to use roles param
      @user.update organization_role: "admin"
      api_v2_sign_in(@user)
      # Stub out the search query to elasticsearch so it returns the id of @user3
      topic_name = "foo"
      Search::UserSearch.any_instance.expects(:search).with(
        filter: {
          expert_topic_names: ['foo']
        },
        allow_blank_q: true,
        viewer: @user
      ).returns OpenStruct.new(results: [@user3])
      result = get :leaderboard_v2,
        format: :json,
        expertise_names: [topic_name],
        show_my_result: "true"
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      expected = {
        "totalCount"=> 1,
        # current user is not included in the 'users' list
        "currentUser"=>@user_attrs.(nil, order_by_score: false, admin: true),
        "users"=>[
          @user3_attrs.(1)
        ]
      }
      assert_equal expected, actual
    end

    test 'q param' do
      # user needs to be admin to use roles param
      @user.update organization_role: "admin"
      api_v2_sign_in(@user)
      # Stub out the search query to elasticsearch so it returns the id of @user3
      topic_name = "foo"
      Search::UserSearch.any_instance.expects(:search).with(
        q: 'potato',
        viewer: @user
      ).returns OpenStruct.new(results: [@user3])
      result = get :leaderboard_v2,
        format: :json,
        q: "potato",
        show_my_result: "true"
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      expected = {
        "totalCount"=> 1,
        # current user is not included in the 'users' list
        "currentUser"=>@user_attrs.(nil, order_by_score: false, admin: true),
        "users"=>[
          @user3_attrs.(1)
        ]
      }
      assert_equal expected, actual
    end

    test 'q param is not applied if it is blank' do
      # user needs to be admin to use roles param
      @user.update organization_role: "admin"
      api_v2_sign_in(@user)
      # Stub out the search query to elasticsearch so it returns the id of @user3
      topic_name = "foo"
      Search::UserSearch.any_instance.expects(:search).never
      result = get :leaderboard_v2,
        format: :json,
        q: "",
        show_my_result: "true"
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      expected = {
        "totalCount"=> 3,
        "currentUser"=>@user_attrs.(3).deep_merge(
          'user' => { 'roles' => ['admin'], 'rolesDefaultNames' => ['admin'] }
        ),
        "users"=>[
          @user3_attrs.(1),
          @user2_attrs.(2),
          @user_attrs.(3).deep_merge(
            'user' => { 'roles' => ['admin'], 'rolesDefaultNames' => ['admin'] }
          )
        ]
      }
      assert_equal expected, actual
    end


    test 'q param when there are no user results' do
      # user needs to be admin to use roles param
      @user.update organization_role: "admin"
      api_v2_sign_in(@user)
      # Stub out the search query to elasticsearch so it returns the id of @user3
      topic_name = "foo"
      Search::UserSearch.any_instance.expects(:search).with(
        q: 'potato',
        viewer: @user
      ).returns OpenStruct.new(results: [])
      result = get :leaderboard_v2,
        format: :json,
        q: "potato",
        show_my_result: "true"
      assert_equal 200, result.status
      actual = JSON.parse(result.body)
      expected = {
        "totalCount"=> 0,
        # current user is not included in the 'users' list
        "currentUser"=>@user_attrs.(nil, order_by_score: false, admin: true),
        "users"=>[]
      }
      assert_equal expected, actual
    end

  end
end

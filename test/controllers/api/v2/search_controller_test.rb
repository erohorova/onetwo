require 'test_helper'

class Api::V2::SearchControllerTest < ActionController::TestCase

  setup do
    # THIS IS SUPER ANNOYING, NEED TO INVESTIGATE MORE.
    # IDEALLY WE SHOULDNT BE UNSTUBBING THIS. THIS IS SLOWING
    # DOWN TEST SUIT
    Organization.any_instance.unstub(:create_default_roles)
  end

  test 'search api failure cases' do
    user = create(:user)
    api_v2_sign_in(user)

    # No query parameter
    get :index, format: :json
    assert_response :unprocessable_entity
  end

  test ':api simple search' do
    org = create(:organization)
    users = create_list(:user, 2, organization: org)
    signed_in_user = create(:user, organization: org)
    channels = create_list(:channel, 2, organization: org)

    create(:follow, followable: users.first, user: signed_in_user)
    create(:follow, followable: channels.first, user: signed_in_user)

    # media cards
    media_link_card = create(:card, card_type: 'media', card_subtype: 'link', resource: create(:resource))
    media_video_card = create(:card, card_type: 'media', card_subtype: 'video', resource: create(:resource))
    media_insight_card = create(:card, message: 'some message', card_type: 'media', card_subtype: 'text')
    media_image_card = create(:card, card_type: 'media', card_subtype: 'image')
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)
    media_image_card.file_resources << fr1

    # poll
    poll_card = create(:card, card_type: "poll", card_subtype: 'text')
    poll_card.quiz_question_options << create(:quiz_question_option, quiz_question: poll_card)

    # pathway
    pathway_card = create(:card, card_type: 'pack', card_subtype: 'simple')
    hidden_cards = create_list(:card, 2, hidden: true)
    Card.any_instance.stubs(:reindex).returns true

    hidden_cards.each do |card|
      CardPackRelation.add(cover_id: pathway_card.id, add_id: card.id, add_type: card.class.name)
    end

    # video stream card
    video_stream_card = create(:card, card_type: 'video_stream', card_subtype: 'simple', author: users.first)
    vs = create(:iris_video_stream, card_id: video_stream_card.id, status: 'past')
    vs.recordings << create(:recording, transcoding_status: 'available', mp4_location: 'http://something.mp4', hls_location: 'http://something.m3u8')

    video_stream_card.reload
    cards = [media_link_card, media_video_card, media_insight_card, media_image_card, poll_card, pathway_card, video_stream_card]

    query = "query"
    limit = 10
    offset = 0
    Search::UserSearch.any_instance.expects(:search)
        .with(limit: limit, offset: offset, load: true, filter: {}, q: query, viewer: signed_in_user)
        .returns(Search::UserSearchResponse.new(results: users, total: 20)).once

    Search::ChannelSearch.any_instance.expects(:search)
        .with(limit: limit, offset: offset, :filter => {:include_private => true, skip_aggs: true}, load: true, q: query,
              viewer: signed_in_user, sort: [{:key => '_score', :order => :desc}])
        .returns(Search::ChannelSearchResponse.new(results: channels, total: 10)).once


    ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items"]
    ecl_search = mock()
    EclApi::EclSearch.expects(:new)
        .with(org.id, signed_in_user.id)
        .returns(ecl_search).once

    ecl_search.expects(:search)
        .with(has_entries(query: query, limit: limit, offset: offset, :query_type => 'and', load_tags: true,
                          filter_params: has_entries({content_type: nil, source_id: nil, source_type_name: nil,
                                                      exclude_ecl_card_id: nil, topic: nil, type: "search",
                                                      rank: true, rank_params: {user_ids: []} })
              ))
        .returns(OpenStruct.new(results: cards, aggregations: ecl_response['aggregations'])).once

    api_v2_sign_in(signed_in_user)
    get :index, q: "query", limit: limit, offset: offset, format: :json

    assert_response :ok
    resp = get_json_response(response)
    assert_equal users.map(&:id), resp['users'].map{|r| r['id']}
    assert_equal [true, false], resp['users'].map{|r| r['is_following']}
    assert_includes resp['users'][0].keys, "roles"
    assert_includes resp['users'][0].keys, "expert_skills"
    assert_includes resp['users'][0].keys, "job_title"
    assert_includes resp['users'][0].keys, "coverimages"
    assert_includes resp['users'][0].keys, "followers_count"
    assert_includes resp['users'][0].keys, "following_count"

    assert_equal channels.map(&:id), resp['channels'].map{|r| r['id']}
    assert_equal [true, false], resp['channels'].map{|r| r['is_following']}
    assert_equal cards.map(&:id).map(&:to_s), resp['cards'].map{|r| r['id']}
    assert_equal limit, resp["limit"]
    assert_equal offset, resp["offset"]
    assert_equal query, resp["query"]
    assert_equal 10, resp["aggregations"].select{|agg| agg["id"] == "channel" && agg["type"] == "content_type"}.first["count"]
    assert_equal 20, resp["aggregations"].select{|agg| agg["id"] == "user" && agg["type"] == "content_type"}.first["count"]
    expected_keys = ['id', 'label', 'description', 'is_private', 'allow_follow', 'banner_image_urls', 'profile_image_url',
      'profile_image_urls', 'is_following', 'slug', 'updated_at']

    # validate channel keys
    assert_equal expected_keys.to_set, resp['channels'].first.keys.to_set
  end

  test 'returns minimal data in the channels response' do
    org   = create :organization
    user  = create :user, organization: org
    admin = create :user, organization: org, organization_role: 'admin'

    channel = create :channel, organization: org
    channel.add_authors([admin])
    channel.add_followers([user])

    headers = { 'Content-Type' => 'application/json', 'Accept' => 'application/json', 'X-Edcast-JWT' => 1, 'X-API-TOKEN' => user }

    User.stubs(:pubnub_channels_to_subscribe_to_for_id).returns([])

    Search::ChannelSearch.any_instance.expects(:search)
      .with(limit: 10, offset: 0, filter: { include_private: true, skip_aggs: true }, load: true, q: 'query',
        viewer: user, sort: [{ key: '_score', order: :desc }])
      .returns(Search::ChannelSearchResponse.new(results: [channel], total: 1)).once

    api_v2_sign_in(user)

    get :index, q: 'query', content_type: ['channel'], format: :json
    json_details  = get_json_response(response)
    response_keys = json_details['channels'][0].keys

    assert_same_elements ['id', 'label', 'description', 'is_private', 'allow_follow',
      'banner_image_urls', 'profile_image_url', 'profile_image_urls',
      'is_following', 'slug', 'updated_at'], response_keys
  end

  test 'content type filter for search' do
    org = create(:organization)
    users = create_list(:user, 2, organization: org)
    signed_in_user = create(:user, organization: org)
    query = "query"
    Search::UserSearch.any_instance.expects(:search)
        .with(limit: 10, offset: 0, load: true, filter: {}, q: query, viewer: signed_in_user)
        .returns(Search::UserSearchResponse.new(results: users, total: 10)).once
    Search::ChannelSearch.any_instance.expects(:search).never
    Search::TeamSearch.any_instance.expects(:search).never

    api_v2_sign_in(signed_in_user)
    get :index, q: "query", content_type: ["user"], format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal users.map(&:id), resp['users'].map{|r| r['id']}
    assert_equal [], resp['channels']
    assert_equal [], resp['cards']
    assert_equal [], resp['teams']
  end

  test 'should search card and channnel together' do
    org = create(:organization)
    users = create_list(:user, 2, organization: org)
    signed_in_user = create(:user, organization: org)
    query = "query"

    cards = build_list(:card, 5, organization: org)
    ecl_search = mock()
    EclApi::EclSearch.expects(:new).with(org.id, signed_in_user.id).returns(ecl_search).once
    ecl_search.expects(:search).with(has_entries(query: query, limit: 10, offset: 0, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: ["article"], source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: cards, aggregations: [])).once

    Search::UserSearch.any_instance.expects(:search).with(limit: 10, offset: 0, load: true, q: query, filter: {}, viewer: signed_in_user).returns(Search::UserSearchResponse.new(results: users, total: 10)).once
    Search::ChannelSearch.any_instance.expects(:search).never
    Search::TeamSearch.any_instance.expects(:search).never

    api_v2_sign_in(signed_in_user)
    get :index, q: "query", content_type: ["article", "user"], format: :json
    assert_response :ok
  end

  test 'do not search for user or channel when search through source' do
    org = create(:organization)
    user = create(:user, organization: org)
    signed_in_user = create(:user, organization: org)
    cards = create_list(:card, 2, organization: org, author: user)

    ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items"]
    ecl_search = mock()
    EclApi::EclSearch.expects(:new).with(org.id, signed_in_user.id).returns(ecl_search).once
    ecl_search.expects(:search)
      .with(has_entries(:query => 'query', :limit => 10, :offset => 0, :query_type => 'and', load_tags: true, :filter_params => has_entries({:content_type => nil, :source_id => ['some-source-id'], source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, :rank_params => {:user_ids => []} })))
      .returns(OpenStruct.new(results: cards, aggregations: ecl_response['aggregations'])).once

    Search::UserSearch.any_instance.expects(:search).never
    Search::ChannelSearch.any_instance.expects(:search).never

    api_v2_sign_in(signed_in_user)

    get :index, q: "query", source_id: ['some-source-id'], format: :json

    assert_response :ok
    resp = get_json_response(response)

    assert_empty resp['channels']
    assert_empty resp['users']
  end

  test 'should be able to handle non persisted ecl cards' do
    org = create(:organization)
    signed_in_user = create(:user, organization: org)
    query = "some query"
    limit = 10
    offset = 0
    content_type = 'article'

    cards = build_list(:card, 5, organization: org)
    ecl_search = mock()
    EclApi::EclSearch.expects(:new).with(org.id, signed_in_user.id).returns(ecl_search).once
    ecl_search.expects(:search).with(has_entries(query: query, limit: limit, offset: offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: [content_type], source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: cards, aggregations: [])).once

    api_v2_sign_in(signed_in_user)
    get :index, q: query, limit: limit, offset: offset, content_type: [content_type], format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_same_elements cards.map{|card| "ECL-#{card.ecl_id}"}, resp['cards'].map{|r| r['id']}
  end

  test 'should be able to show same stock images of non persisted ecl cards if it request comes from mobile and web' do
    org = create(:organization)
    signed_in_user = create(:user, organization: org)
    query = "some query"
    limit = 10
    offset = 0
    content_type = 'article'

    resource_1, resource_2 = create_list(:resource, 2, url: "www.youtube.com")
    card_1 = create(:card,hidden:false, card_type: 'media', card_subtype: 'video',resource_id: resource_1.id, created_at: Time.now)
    card_2 = create(:card,hidden:false, card_type: 'media', card_subtype: 'video',resource_id: resource_2.id, created_at: Date.today-1)

    resource_1.update_attribute(:image_url, nil)
    resource_2.update_attribute(:image_url, nil)

    cards = [card_1, card_2]
    image_index_1, image_index_2 = cards.map{|card| card.created_at.to_i % 100}

    stock_image_1 = CARD_STOCK_IMAGE[image_index_1][:url]
    stock_image_2 = CARD_STOCK_IMAGE[image_index_2][:url]

    ecl_search = mock()
    EclApi::EclSearch.expects(:new).with(org.id, signed_in_user.id).returns(ecl_search).once

    ecl_search.expects(:search).with(has_entries(query: query, limit: limit, offset: offset, :query_type => 'and',
     load_tags: true,
     filter_params: has_entries({content_type: [content_type],
      source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil,
       topic: nil, type: "search", rank: true,
        rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: cards, aggregations: [])).once

    api_v2_sign_in(signed_in_user)
    Filestack::Security.any_instance.stubs(:signed_url).returns(stock_image_1)

    get :index, q: query, limit: limit, offset: offset, user_agent: 'Edcast iPhone', content_type: [content_type], format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal stock_image_1, resp["cards"][0]['resource']['image_url']
  end

  test 'should turn off ranking if rank is false' do
    @org = create(:organization)
    @signed_in_user = create(:user, organization: @org)
    api_v2_sign_in(@signed_in_user)

    cards = build_list(:card, 5, organization: @org)
    @ecl_search = mock
    EclApi::EclSearch.expects(:new).with(@org.id, @signed_in_user.id).returns(@ecl_search)

    @ecl_search.expects(:search).returns(OpenStruct.new(results: cards, aggregations: [])).with do |params|
      !params['rank']
    end

    get :index, q: 'query', rank: 'false', content_type: ['article'], format: :json
  end

  test 'should pass sort attributes to search' do
    @org = create(:organization)
    @signed_in_user = create(:user, organization: @org)
    api_v2_sign_in(@signed_in_user)

    cards = build_list(:card, 5, organization: @org)
    @ecl_search = mock
    EclApi::EclSearch.expects(:new).with(@org.id, @signed_in_user.id).returns(@ecl_search)

    @ecl_search.expects(:search).with(has_entries(query: "query", limit: 10, offset: 0, :query_type => 'and', load_tags: true,
                          filter_params: has_entries({content_type: ['article'], source_id: nil, source_type_name: nil,
                                                      exclude_ecl_card_id: nil, topic: nil, type: "search",
                                                      rank: true, rank_params: {user_ids: []}, sort_attr: "created_at", sort_order: "desc"})
              ))
        .returns(OpenStruct.new(results: cards, aggregations: [])).once

    get :index, q: 'query', rank: 'false', content_type: ['article'], sort_attr: "created_at", sort_order: "desc", format: :json
  end

  test 'should have ranking on by default' do
    @org = create(:organization)
    @signed_in_user = create(:user, organization: @org)
    api_v2_sign_in(@signed_in_user)

    cards = build_list(:card, 5, organization: @org)
    @ecl_search = mock
    EclApi::EclSearch.expects(:new).with(@org.id, @signed_in_user.id).returns(@ecl_search)

    @ecl_search.expects(:search).returns(OpenStruct.new(results: cards, aggregations: [])).with do |params|
      !params['rank']
    end

    get :index, q: 'query', content_type: ['article'], format: :json
  end

  test 'should be able to handle excluded ecl cards' do
    org = create(:organization)
    signed_in_user, author = create_list(:user, 2, organization: org)
    completed_cards = create_list(:card, 3, author_id: author.id)
    cards = create_list(:card, 3, author_id: author.id)
    completed_cards.each do |card|
      card.update(ecl_id: Faker::Number.number(10).to_s)
      create(:user_content_completion, user: signed_in_user, completable: card, state: UserContentCompletion::COMPLETED)
    end

    query = "some query"
    limit = 10
    offset = 0
    content_type = 'article'

    ecl_search = mock()
    EclApi::EclSearch.expects(:new).with(org.id, signed_in_user.id).returns(ecl_search).once
    ecl_search.expects(:search).with(has_entries(
      query: query,
      limit: limit,
      offset: offset,
      :query_type => 'and',
      load_tags: true,

      filter_params: has_entries({
        content_type: [content_type],
        source_id: nil,
        source_type_name: nil,
        rank: true,
        rank_params: {user_ids: []},
        exclude_ecl_card_id: completed_cards.map(&:ecl_id),
        topic: nil,
        type: "search"      }))).returns(OpenStruct.new(results: cards, aggregations: [])).once

    api_v2_sign_in(signed_in_user)
    get :index, q: query, limit: limit, offset: offset, content_type: [content_type], exclude_cards: ['completed'], format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_empty completed_cards.map(&:id) & resp['cards'].map{|r| r['id']}
  end

  test ':api should return end_date, paid_by_user and due_at fields in response' do
    org = create(:organization)
    user = create(:user, organization: org)
    card = create(:card, author_id: user.id)
    is_paid = card.is_paid_card && card.paid_by_user?(user)
    assignment = create(:assignment, assignable: card, assignee: user, due_at: Date.parse("#{Date.current.year}/06/01"))
    card_subscription = create(
      :card_subscription,
      card_id: card.id,
      user_id: user.id,
      end_date: Date.parse("#{Date.current.year}/06/01")
    )
    ecl_search = mock()
    EclApi::EclSearch.expects(:new)
      .with(org.id, user.id)
      .returns(ecl_search).once
    filter_params = {
      content_type: ['article'],
      source_id: nil,
      source_type_name: nil,
      rank: true,
      rank_params: { user_ids: [] },
      topic: nil,
      type: 'search'
    }
    ecl_search.expects(:search).with( has_entries(
                                        query: 'some query',
                                        limit: 10,
                                        offset: 0,
                                        :query_type => 'and',
                                        load_tags: true,
                                        filter_params: has_entries(filter_params))
    ).returns(OpenStruct.new(results: [card], aggregations: [])).once
    api_v2_sign_in(user)

    get :index, q: 'some query', limit: 10, offset: 0, content_type: ['article'],
        fields: 'due_at,subscription_end_date,paid_by_user', format: :json
    assert_response :ok
    resp = get_json_response(response)['cards'].first
    assert_equal assignment['due_at'], resp['due_at']
    assert_equal card_subscription['end_date'], resp['subscription_end_date']
    assert_equal is_paid, resp['paid_by_user']
  end

  #level
  test "should return cards by level" do
    level_and_rating_setup

    @ecl_search.expects(:search)
      .with(has_entries(:query => 'query', :limit => 10, :offset => 0, :query_type => 'and', load_tags: true, :filter_params => has_entries({:content_type => nil, :source_id => nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", level: ['intermediate', 'beginner', 'advanced'], rank: true, :rank_params => {:user_ids => []} })))
      .returns(OpenStruct.new(results: @cards, aggregations: @ecl_response['aggregations'])).once

    get :index, q: "query", level: ['intermediate', 'beginner', 'advanced'], limit: 10, offset: 0, format: :json

    assert_response :ok
  end

  test "should return whose level beginner" do
    level_and_rating_setup

    @ecl_search.expects(:search)
      .with(has_entries(:query => 'query', :limit => 10, :offset => 0, :query_type => 'and', load_tags: true, :filter_params => has_entries({:content_type => nil, :source_id => nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", level: ['beginner'], rank: true, :rank_params => {:user_ids => []} })))
      .returns(OpenStruct.new(results: @cards, aggregations: @ecl_response['aggregations'])).once

    get :index, q: "query", level: ['beginner'], limit: 10, offset: 0, format: :json

    assert_response :ok
  end

  test "should return whose level intermediate" do
    level_and_rating_setup

    @ecl_search.expects(:search)
      .with(has_entries(:query => 'query', :limit => 10, :offset => 0, :query_type => 'and', load_tags: true, :filter_params => has_entries({:content_type => nil, :source_id => nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", level: ['intermediate'], rank: true, :rank_params => {:user_ids => []} })))
      .returns(OpenStruct.new(results: @cards, aggregations: @ecl_response['aggregations'])).once

    get :index, q: "query", level: ['intermediate'], limit: 10, offset: 0, format: :json

    assert_response :ok
  end

  test "should return whose level advanced" do
    level_and_rating_setup

    @ecl_search.expects(:search)
      .with(has_entries(:query => 'query', :limit => 10, :offset => 0, :query_type => 'and', load_tags: true, :filter_params => has_entries({:content_type => nil, :source_id => nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", level: ['advanced'], rank: true, :rank_params => {:user_ids => []} })))
      .returns(OpenStruct.new(results: @cards, aggregations: @ecl_response['aggregations'])).once

    get :index, q: "query", level: ['advanced'], limit: 10, offset: 0, format: :json

    assert_response :ok
  end

  #rating
  test "should return cards by rating" do
    level_and_rating_setup

    @ecl_search.expects(:search)
      .with(has_entries(:query => 'query', :limit => 10, :offset => 0, :query_type => 'and', load_tags: true, :filter_params => has_entries({:content_type => nil, :source_id => nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rating: ['1', '2', '4'], rank: true, :rank_params => {:user_ids => []} })))
      .returns(OpenStruct.new(results: @cards, aggregations: @ecl_response['aggregations'])).once

    get :index, q: "query", rating: ['1', '2', '4'], limit: 10, offset: 0, format: :json

    assert_response :ok
  end

  #price
  test "should return cards by price" do
    price_setup

    @ecl_search.expects(:search)
      .with(has_entries(:query => 'query', :limit => 10, :offset => 0, :query_type => 'and', load_tags: true, :filter_params => has_entries({:content_type => nil, :source_id => nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", currency: "USD", amount_range: ["['15.0', '20.0']"], rank: true, :rank_params => {:user_ids => []} })))
      .returns(OpenStruct.new(results: @cards, aggregations: @ecl_response['aggregations'])).once

    get :index, q: "query", currency: "USD", amount_range: ["['15.0', '20.0']"], limit: 10, offset: 0, format: :json

    assert_response :ok
  end

  #free/paid
  test "should return cards by free or paid status" do
    free_paid_setup

    @ecl_search.expects(:search)
      .with(has_entries(:query => 'query', :limit => 10, :offset => 0, :query_type => 'and', load_tags: true, :filter_params => has_entries({:content_type => nil, :source_id => nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", is_paid: true, rank: true, :rank_params => {:user_ids => []} })))
      .returns(OpenStruct.new(results: @cards, aggregations: @ecl_response['aggregations'])).once

    get :index, q: "query", is_paid: true, limit: 10, offset: 0, format: :json

    assert_response :ok
  end

  # card_metadata plans
  test "should return cards by plans - free, paid, premium, subscription " do
    price_plan_setup

    @ecl_search.expects(:search)
      .with(has_entries(:query => 'query', :limit => 10, :offset => 0, :query_type => 'and', load_tags: true, :filter_params => has_entries({:content_type => nil, :source_id => nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", plan: ["paid"], rank: true, :rank_params => {:user_ids => []} })))
      .returns(OpenStruct.new(results: @cards, aggregations: @ecl_response['aggregations'])).once

    get :index, q: "query", plan: ["paid"], limit: 10, offset: 0, format: :json

    assert_response :ok
  end


  #liked by
  test "should return cards liked by followers" do
    liked_by_setup

    @ecl_search.expects(:search).with(has_entries(query: @query, limit: @limit, offset: @offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: nil, source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, ecl_card_ids: [@followers_votable.ecl_id], rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: [], aggregations: @ecl_response['aggregations'])).once
    get :index, q: "query", liked_by: {followers: true}, limit: @limit, offset: @offset, format: :json
    assert_response :ok
  end

  test "should return cards liked by following" do
    liked_by_setup

    @ecl_search.expects(:search).with(has_entries(query: @query, limit: @limit, offset: @offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: nil, source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, ecl_card_ids: [@following_votable.ecl_id], rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: [], aggregations: @ecl_response['aggregations'])).once
    get :index, q: "query", liked_by: {following: true}, limit: @limit, offset: @offset, format: :json
    assert_response :ok
  end

  test "should return cards liked by other user" do
    liked_by_setup

    @ecl_search.expects(:search).with(has_entries(query: @query, limit: @limit, offset: @offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: nil, source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, ecl_card_ids: [@other_user_card.ecl_id], rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: [], aggregations: @ecl_response['aggregations'])).once
    get :index, q: "query", liked_by: {user_ids: "#{@other_user.id}"}, limit: @limit, offset: @offset, format: :json
    assert_response :ok
  end

  test "should return all liked cards" do
    liked_by_setup
    @ecl_search.expects(:search).with(has_entries(query: @query, limit: @limit, offset: @offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: nil, source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, ecl_card_ids: [@following_votable.ecl_id, @followers_votable.ecl_id, @other_user_card.ecl_id], rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: [], aggregations: @ecl_response['aggregations'])).once
    get :index, q: "query", liked_by: {followers: true, following: true, user_ids: "#{@other_user.id}"}, limit: @limit, offset: @offset, format: :json
    assert_response :ok
  end

  #commented by
  test "should return cards commented by followers" do
    commented_by_setup

    @ecl_search.expects(:search).with(has_entries(query: @query, limit: @limit, offset: @offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: nil, source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, ecl_card_ids: [@followers_commentable.ecl_id], rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: [], aggregations: @ecl_response['aggregations'])).once
    get :index, q: "query", commented_by: {followers: true}, limit: @limit, offset: @offset, format: :json
    assert_response :ok
  end

  test "should return cards commented by following" do
    commented_by_setup

    @ecl_search.expects(:search).with(has_entries(query: @query, limit: @limit, offset: @offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: nil, source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, ecl_card_ids: [@following_commentable.ecl_id], rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: [], aggregations: @ecl_response['aggregations'])).once
    get :index, q: "query", commented_by: {following: true}, limit: @limit, offset: @offset, format: :json
    assert_response :ok
  end

  test "should return cards commented by other user" do
    commented_by_setup

    @ecl_search.expects(:search).with(has_entries(query: @query, limit: @limit, offset: @offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: nil, source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, ecl_card_ids: [@other_user_card.ecl_id], rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: [], aggregations: @ecl_response['aggregations'])).once
    get :index, q: "query", commented_by: {user_ids: "#{@other_user.id}"}, limit: @limit, offset: @offset, format: :json
    assert_response :ok
  end

  test "should return all commented cards" do
    commented_by_setup
    @ecl_search.expects(:search).with(has_entries(query: @query, limit: @limit, offset: @offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: nil, source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, ecl_card_ids: [@following_commentable.ecl_id, @followers_commentable.ecl_id, @other_user_card.ecl_id], rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: [], aggregations: @ecl_response['aggregations'])).once
    get :index, q: "query", commented_by: {followers: true, following: true, user_ids: "#{@other_user.id}"}, limit: @limit, offset: @offset, format: :json
    assert_response :ok
  end

  #bookmarked by
  test "should return cards bookmarked by followers" do
    bookmarked_by_setup

    @ecl_search.expects(:search).with(has_entries(query: @query, limit: @limit, offset: @offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: nil, source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, ecl_card_ids: [@followers_card.ecl_id], rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: [], aggregations: @ecl_response['aggregations'])).once
    get :index, q: "query", bookmarked_by: {followers: true}, limit: @limit, offset: @offset, format: :json
    assert_response :ok
  end

  test "should return cards bookmarked by following" do
    bookmarked_by_setup

    @ecl_search.expects(:search).with(has_entries(query: @query, limit: @limit, offset: @offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: nil, source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, ecl_card_ids: [@following_card.ecl_id], rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: [], aggregations: @ecl_response['aggregations'])).once
    get :index, q: "query", bookmarked_by: {following: true}, limit: @limit, offset: @offset, format: :json
    assert_response :ok
  end

  test "should return cards bookmarked by other user" do
    bookmarked_by_setup

    @ecl_search.expects(:search).with(has_entries(query: @query, limit: @limit, offset: @offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: nil, source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, ecl_card_ids: [@other_user_card.ecl_id], rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: [], aggregations: @ecl_response['aggregations'])).once
    get :index, q: "query", bookmarked_by: {user_ids: "#{@other_user.id}"}, limit: @limit, offset: @offset, format: :json
    assert_response :ok
  end

  test "should return all bookmarked cards" do
    bookmarked_by_setup
    @ecl_search.expects(:search).with(has_entries(query: @query, limit: @limit, offset: @offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: nil, source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, ecl_card_ids: [@following_card.ecl_id, @followers_card.ecl_id, @other_user_card.ecl_id], rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: [], aggregations: @ecl_response['aggregations'])).once
    get :index, q: "query", bookmarked_by: {followers: true, following: true, user_ids: "#{@other_user.id}"}, limit: @limit, offset: @offset, format: :json
    assert_response :ok
  end

  #posted by
  test "should return cards posted by followers" do
    posted_by_setup("followers")

    @ecl_search.expects(:search).with(has_entries(query: @query, limit: @limit, offset: @offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: nil, source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, ecl_card_ids: [@followers_postable.ecl_id], rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: [], aggregations: @ecl_response['aggregations'])).once
    get :index, q: "query", posted_by: {followers: true}, limit: @limit, offset: @offset, format: :json
    assert_response :ok
  end

  test "should return cards posted by following" do
    posted_by_setup("following")

    @ecl_search.expects(:search).with(has_entries(query: @query, limit: @limit, offset: @offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: nil, source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, ecl_card_ids: [@following_postable.ecl_id], rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: [], aggregations: @ecl_response['aggregations'])).once
    get :index, q: "query", posted_by: {following: true}, limit: @limit, offset: @offset, format: :json
    assert_response :ok
  end

  test "should return cards posted by other user" do
    posted_by_setup("other")

    @ecl_search.expects(:search).with(has_entries(query: @query, limit: @limit, offset: @offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: nil, source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, ecl_card_ids: [@other_user_card.ecl_id], rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: [], aggregations: @ecl_response['aggregations'])).once
    get :index, q: "query", posted_by: {user_ids: "#{@other_user.id}"}, limit: @limit, offset: @offset, format: :json
    assert_response :ok
  end

  test "should return all posted cards" do
    posted_by_setup("all")
    @ecl_search.expects(:search).with(has_entries(query: @query, limit: @limit, offset: @offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: nil, source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, ecl_card_ids: [@following_postable.ecl_id, @followers_postable.ecl_id, @other_user_card.ecl_id], rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: [], aggregations: @ecl_response['aggregations'])).once
    get :index, q: "query", posted_by: {followers: true, following: true, user_ids: "#{@other_user.id}"}, limit: @limit, offset: @offset, format: :json
    assert_response :ok
  end


  test "should return all posted cards with date filters" do
    posted_by_setup("all")
    @ecl_search.expects(:search).with(has_entries(query: @query, limit: @limit, offset: @offset, :query_type => 'and', load_tags: true, filter_params: has_entries({from_date: "01/06/2017", till_date: "10/06/2017", content_type: nil, source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, ecl_card_ids: [@following_postable.ecl_id, @followers_postable.ecl_id, @other_user_card.ecl_id], rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: [], aggregations: @ecl_response['aggregations'])).once
    get :index, q: "query", from_date: "01/06/2017", till_date: "10/06/2017", posted_by: {followers: true, following: true, user_ids: "#{@other_user.id}"}, limit: @limit, offset: @offset, format: :json
    assert_response :ok
  end

  #custom limits
  test "custom limit and offset with request" do
    cards_limit = 40
    cards_offset = 15
    channels_limit = 10
    channels_offset = 10
    users_limit = 20
    users_offset = 20

    posted_by_setup("all")

    Search::UserSearch.any_instance.expects(:search).with(limit: users_limit, offset: users_offset, filter: {}, load: true, q: "query", viewer: @signed_in_user).returns(Search::UserSearchResponse.new(results: [], total: 20)).once
    Search::ChannelSearch.any_instance.expects(:search).with(limit: channels_limit, offset: channels_offset, load: true, filter: { :include_private => true, skip_aggs: true },q: "query", viewer: @signed_in_user, sort: [{:key => '_score', :order => :desc}]).returns(Search::ChannelSearchResponse.new(results: [], total: 10)).once

    @ecl_search.expects(:search).with(has_entries(query: @query, limit: cards_limit, offset: cards_offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: nil, source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: [], aggregations: @ecl_response['aggregations'])).once
    get :index, q: "query", cards_limit: cards_limit, cards_offset: cards_offset,
        channels_limit: channels_limit, channels_offset: channels_offset,
        users_limit: users_limit, users_offset: users_offset, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert resp["cards_limit"], cards_limit
    assert resp["cards_offset"], cards_offset
    assert resp["channels_limit"], channels_limit
    assert resp["channels_offset"], channels_offset
    assert resp["users_limit"], users_limit
    assert resp["users_offset"], users_offset
  end

  test "search should return private channels accessible to user" do
    create_default_org
    org = create(:organization)
    signed_in_user = create(:user, organization: org)

    api_v2_sign_in(signed_in_user)
    channels = create_list(:channel, 2, organization: org)
    private_ch = create(:channel, is_private: true, organization: org)
    private_ch1 = create(:channel, is_private: true, organization: org)

    create(:follow, followable: private_ch, user: signed_in_user)

    Search::ChannelSearch.any_instance.expects(:search).with(
      limit: 10, offset: 0, load: true,
      :filter => {:include_private => true, skip_aggs: true},
      q: "query", viewer: signed_in_user,
      sort: [{:key => '_score', :order => :desc}]
    ).returns(Search::ChannelSearchResponse.new(
      results: (channels + [private_ch]), total: 10)
    ).once
    get :index, q: "query", content_type: ["channel"], limit: @limit, offset: @offset, format: :json
    resp = get_json_response(response)
    assert_includes resp["channels"].map{|ch| ch["id"]},  private_ch.id
    assert_not_includes resp["channels"].map{|ch| ch["id"]},  private_ch1.id
  end

  test "without custom limit and offset with request" do
    posted_by_setup("all")

    Search::UserSearch.any_instance.expects(:search).with(limit: @limit, offset: @offset, load: true, filter: {}, q: "query", viewer: @signed_in_user).returns(Search::UserSearchResponse.new(results: [], total: 20)).once
    Search::ChannelSearch.any_instance.expects(:search).with(limit: @limit, offset: @offset, load: true, filter: { include_private: true, skip_aggs: true }, q: "query", viewer: @signed_in_user, sort: [{:key => '_score', :order => :desc}]).returns(Search::ChannelSearchResponse.new(results: [], total: 10)).once

    @ecl_search.expects(:search).with(has_entries(query: @query, limit: @limit, offset: @offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: nil, source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: [], aggregations: @ecl_response['aggregations'])).once
    get :index, q: "query", limit: @limit, offset: @offset, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert resp["cards_limit"], @limit
    assert resp["cards_offset"], @offset
    assert resp["channels_limit"], @limit
    assert resp["channels_offset"], @offset
    assert resp["users_limit"], @limit
    assert resp["users_offset"], @offset
  end

  test 'should not hit ecl if its interactive card search and no interaction found' do
    @org = create(:organization)
    @users = create_list(:user, 5, organization: @org)
    @signed_in_user = create(:user, organization: @org)
    api_v2_sign_in(@signed_in_user)

    @ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items"]
    @ecl_search = mock()
    EclApi::EclSearch.expects(:new).with(@org.id, @signed_in_user.id).returns(@ecl_search).never

    get :index, q: "query", posted_by: {user_ids: [@users[2].id]}, liked_by: {user_ids: [@users[3].id]}, limit: @limit, offset: @offset, format: :json
    assert_response :ok
  end

  #Seggregate pathways and journeys in search
  test 'search with seggregate pathways and journeys' do
    seggregate_pathway_search_setup

    Search::UserSearch.any_instance.expects(:search).returns(Search::UserSearchResponse.new(results: [], total: 10)).once
    Search::ChannelSearch.any_instance.expects(:search).returns(Search::ChannelSearchResponse.new(results: [], total: 10)).once

    pathway = create(:card, card_type: 'pack', card_subtype: 'simple')
    pathway.publish!

    setup_journey_card(@signed_in_user)
    @journey.publish!

    EclApi::EclSearch.expects(:new).with(@org.id, @signed_in_user.id).returns(@ecl_search).times(3)

    @ecl_search.expects(:search).with(
      has_entries(
        query: @query, limit: @limit,
        offset: @offset, :query_type => 'and',
        load_tags: true,
        filter_params: has_entries(
          {
            content_type: EclApi::EclSearch::CARD_CONTENT_TYPE,
            source_id: nil, source_type_name: nil,
            exclude_ecl_card_id: nil, topic: nil,
            type: "search", rank: true,
            rank_params: {user_ids: []}

          }
        )
      )
    ).returns(OpenStruct.new(results: @cards, aggregations: [])).once

    @ecl_search.expects(:search).with(
      has_entries(
        query: @query, limit: @limit,
        offset: @offset, :query_type => 'and',
        load_tags: true,
        filter_params: has_entries(
          {
            content_type: [EclApi::EclSearch::PATHWAY_CONTENT_TYPE],
            source_id: nil, source_type_name: nil,
            exclude_ecl_card_id: nil, topic: nil,
            type: "search", rank: true,
            rank_params: {user_ids: []}
          }
        )
      )
    ).returns(OpenStruct.new(results: [pathway], aggregations: [])).once

    @ecl_search.expects(:search).with(
      has_entries(
        query: @query, limit: @limit,
        offset: @offset, :query_type => 'and',
        load_tags: true,
        filter_params: has_entries(
          {
            content_type: [EclApi::EclSearch::JOURNEY_CONTENT_TYPE],
            source_id: nil, source_type_name: nil,
            exclude_ecl_card_id: nil, topic: nil,
            type: "search", rank: true,
            rank_params: {user_ids: []}
          }
        )
      )
    ).returns(OpenStruct.new(results: [@journey], aggregations: [])).once

    get :index, q: @query, segregate_pathways: "true", segregate_journeys: "true", format: :json
    assert_response :ok
    resp = get_json_response(response)

    assert_includes resp.keys, "pathways"
    assert_includes resp.keys, "journeys"

    assert_equal 5, resp["cards"].count
    assert_equal 1, resp["pathways"].count
    assert_equal 1, resp["journeys"].count
  end

  test 'search with seggregate pathways with content_type article should not search pathway' do
    seggregate_pathway_search_setup
    EclApi::EclSearch.expects(:new).with(@org.id, @signed_in_user.id).returns(@ecl_search).once

    @ecl_search.expects(:search).with(has_entries(query: @query, limit: @limit, offset: @offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: ['article'], source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: [], aggregations: [])).once
    @ecl_search.expects(:search).with(has_entries(query: @query, limit: @limit, offset: @offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: [EclApi::EclSearch::PATHWAY_CONTENT_TYPE], source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, rank_params: {user_ids: []} }))).never

    get :index, q: @query, segregate_pathways: "true", content_type: ["article"], format: :json
    assert_response :ok
    resp = get_json_response(response)
  end

  test 'search with segregate journeys with content_type journey should not hit search for non journey cards' do
    Card.any_instance.stubs(:reindex)
    seggregate_pathway_search_setup
    EclApi::EclSearch.expects(:new)
      .with(@org.id, @signed_in_user.id).returns(@ecl_search).once

    @ecl_search.expects(:search).with(
      has_entries(
        query: @query, limit: @limit, offset: @offset,
        :query_type => 'and', load_tags: true,
        filter_params: has_entries(
          {
            content_type: EclApi::EclSearch::CARD_CONTENT_TYPE,
            source_id: nil, source_type_name: nil,
            exclude_ecl_card_id: nil, topic: nil,
            type: "search", rank: true,
            rank_params: {user_ids: []}
          }
        )
      )
    ).returns(OpenStruct.new(results: [], aggregations: [])).never

    @ecl_search.expects(:search).with(
      has_entries(
        query: @query, limit: @limit,
        offset: @offset, :query_type => 'and',
        load_tags: true,
        filter_params: has_entries(
          {
            content_type: [EclApi::EclSearch::JOURNEY_CONTENT_TYPE],
            source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil,
            topic: nil, type: "search", rank: true,
            rank_params: {user_ids: []}
          }
        )
      )
    ).returns(OpenStruct.new(results: @cards, aggregations: [])).once

    get :index, q: @query, segregate_pathways: "true", content_type: ["journey"], format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_empty resp["cards"]
  end

  test 'search with seggregate pathways with content_type pathway should not hit search for  non pathway cards' do
    seggregate_pathway_search_setup

    # seggregate_pathway_search_setup
    EclApi::EclSearch.expects(:new).with(@org.id, @signed_in_user.id).returns(@ecl_search).once

    @ecl_search.expects(:search).with(
      has_entries(
        query: @query, limit: @limit, offset: @offset,
        :query_type => 'and', load_tags: true,
        filter_params: has_entries(
          {
            content_type: EclApi::EclSearch::CARD_CONTENT_TYPE,
            source_id: nil, source_type_name: nil,
            exclude_ecl_card_id: nil, topic: nil,
            type: "search", rank: true,
            rank_params: {user_ids: []}
          }
        )
      )
    ).returns(OpenStruct.new(results: [], aggregations: [])).never

    @ecl_search.expects(:search).with(
      has_entries(
        query: @query, limit: @limit,
        offset: @offset, :query_type => 'and',
        load_tags: true,
        filter_params: has_entries(
          {
            content_type: [EclApi::EclSearch::PATHWAY_CONTENT_TYPE],
            source_id: nil,
            source_type_name: nil,
            exclude_ecl_card_id: nil,
            topic: nil, type: "search", rank: true,
            rank_params: {user_ids: []}
          }
        )
      )
    ).returns(OpenStruct.new(results: @cards, aggregations: [])).once

    get :index, q: @query, segregate_pathways: "true", content_type: ["pathway"], format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_empty resp["cards"]
  end

  test 'search with seggregate pathways with content_type pathway with video' do
    seggregate_pathway_search_setup
    EclApi::EclSearch.expects(:new).with(@org.id, @signed_in_user.id).returns(@ecl_search).twice

    @ecl_search.expects(:search).with(has_entries(query: @query, limit: @limit, offset: @offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: ['video'], source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: [], aggregations: [])).once
    @ecl_search.expects(:search).with(has_entries(query: @query, limit: @limit, offset: @offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: [EclApi::EclSearch::PATHWAY_CONTENT_TYPE], source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: @cards, aggregations: [])).once

    get :index, q: @query, segregate_pathways: "true", content_type: ["pathway", "video"], format: :json
    assert_response :ok
  end

  test 'pathways and journeys count should be exactly what in response there should not be any DB call for pathways and journeys' do
    Card.any_instance.stubs(:reindex)
    seggregate_pathway_search_setup

    Search::UserSearch.any_instance.expects(:search).returns(Search::UserSearchResponse.new(results: [], total: 10)).once
    Search::ChannelSearch.any_instance.expects(:search).returns(Search::ChannelSearchResponse.new(results: [], total: 10)).once

    @pathway3 = create(:card, card_type: 'pack', card_subtype: 'simple')
    @pathway3.publish!

    setup_journey_card(@signed_in_user)
    @journey.publish!

    EclApi::EclSearch.expects(:new).with(@org.id, @signed_in_user.id).returns(@ecl_search).times(3)

    @ecl_search.expects(:search).with(
      has_entries(
        query: @query, limit: @limit,
        offset: @offset,
        :query_type => 'and', load_tags: true,
        filter_params: has_entries(
          {
            content_type: EclApi::EclSearch::CARD_CONTENT_TYPE,
            source_id: nil,
            source_type_name: nil,
            exclude_ecl_card_id: nil,
            topic: nil, type: "search",
            rank: true, rank_params: {user_ids: []}
          }
        )
      )
    ).returns(OpenStruct.new(results: [], aggregations: [])).once

    @ecl_search.expects(:search).with(
      has_entries(
        query: @query, limit: @limit,
        offset: @offset,
        :query_type => 'and', load_tags: true,
        filter_params: has_entries(
          {
            content_type: [EclApi::EclSearch::PATHWAY_CONTENT_TYPE],
            source_id: nil,
            source_type_name: nil,
            exclude_ecl_card_id: nil,
            topic: nil, type: "search", rank: true,
            rank_params: {user_ids: []}
          }
        )
      )
    ).returns(OpenStruct.new(results: [@pathway3], aggregations: [])).once

    @ecl_search.expects(:search).with(
      has_entries(
        query: @query, limit: @limit, offset: @offset,
        :query_type => 'and', load_tags: true,
        filter_params: has_entries(
          {
            content_type: [EclApi::EclSearch::JOURNEY_CONTENT_TYPE],
            source_id: nil, source_type_name: nil,
            exclude_ecl_card_id: nil,
            topic: nil, type: "search", rank: true,
            rank_params: {user_ids: []}
          }
        )
      )
    ).returns(OpenStruct.new(results: [@journey], aggregations: [])).once

    get :index, q: @query, segregate_pathways: "true", segregate_journeys: "true", format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_empty resp["cards"]
    assert_equal 1, resp["pathways"].count
    assert_equal 1, resp["journeys"].count
  end

  def seggregate_pathway_search_setup
    @org = create(:organization)
    @users = create_list(:user, 5, organization: @org)
    @signed_in_user = create(:user, organization: @org)
    @cards = build_list(:card, 5, organization: @org)
    Card.any_instance.stubs(:can_comment?).returns(true)

    @following = @users.first
    @follower = @users[1]
    @other_user = @users.last

    create(:follow, followable: @following, user: @signed_in_user)
    create(:follow, followable: @signed_in_user, user: @follower)

    @query = "query"
    @limit = 10
    @offset = 0
    api_v2_sign_in(@signed_in_user)

    @ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items"]
    @ecl_search = mock()
  end

  def search_setup
    @org = create(:organization)
    @users = create_list(:user, 5, organization: @org)
    @signed_in_user = create(:user, organization: @org)
    Card.any_instance.stubs(:can_comment?).returns(true)

    @following = @users.first
    @follower = @users[1]
    @other_user = @users.last

    create(:follow, followable: @following, user: @signed_in_user)
    create(:follow, followable: @signed_in_user, user: @follower)

    @query = "query"
    @limit = 10
    @offset = 0
    api_v2_sign_in(@signed_in_user)

    @ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items"]
    @ecl_search = mock()
    EclApi::EclSearch.expects(:new).with(@org.id, @signed_in_user.id).returns(@ecl_search).once
  end

  def create_cards
    @cards = []
    5.times do |i|
      @cards << create(:card, organization_id: @org.id, author: @users[i], ecl_id: "ecl_#{i}")
    end
  end

  def level_and_rating_setup
    @org = create(:organization)
    @user = create(:user, organization: @org)
    @signed_in_user = create(:user, organization: @org)
    @cards = create_list(:card, 2, organization: @org, author: @user)

    api_v2_sign_in(@signed_in_user)

    @ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items"]
    @ecl_search = mock()
    EclApi::EclSearch.expects(:new).with(@org.id, @signed_in_user.id).returns(@ecl_search).once
  end

  def price_setup
    @org = create(:organization)
    @user = create(:user, organization: @org)
    @signed_in_user = create(:user, organization: @org)
    @cards = create_list(:card, 2, organization: @org, author: @user)

    api_v2_sign_in(@signed_in_user)

    @ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items"]
    @ecl_search = mock()
    EclApi::EclSearch.expects(:new).with(@org.id, @signed_in_user.id).returns(@ecl_search).once
  end

  def free_paid_setup
    @org = create(:organization)
    @user = create(:user, organization: @org)
    @signed_in_user = create(:user, organization: @org)
    @cards = create_list(:card, 2, organization: @org, author: @user)

    api_v2_sign_in(@signed_in_user)

    @ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items"]
    @ecl_search = mock()
    EclApi::EclSearch.expects(:new).with(@org.id, @signed_in_user.id).returns(@ecl_search).once
  end

  def price_plan_setup
    @org = create(:organization)
    @user = create(:user, organization: @org)
    @signed_in_user = create(:user, organization: @org)
    @cards = create_list(:card, 2, organization: @org, author: @user)

    api_v2_sign_in(@signed_in_user)

    @ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items"]
    @ecl_search = mock()
    EclApi::EclSearch.expects(:new).with(@org.id, @signed_in_user.id).returns(@ecl_search).once
  end

  def liked_by_setup
    search_setup
    create_cards

    #liked by following
    @following_votable = @cards[0]
    create(:vote, votable: @following_votable, voter: @following)

    #liked_by follower
    @followers_votable = @cards[1]
    create(:vote, votable: @followers_votable, voter: @follower)

    #liked by other user id
    @other_user_card = @cards[2]
    create(:vote, votable: @other_user_card, voter: @other_user)
  end

  def commented_by_setup
    search_setup
    create_cards
    #commented by following
    @following_commentable = @cards[0]
    create(:comment, commentable: @following_commentable, user: @following)

    #commented_by follower
    @followers_commentable = @cards[1]
    create(:comment, commentable: @followers_commentable, user: @follower)

    #commented by other user id
    @other_user_card = @cards[2]
    create(:comment, commentable: @other_user_card, user: @other_user)
  end

  def bookmarked_by_setup
    search_setup
    create_cards
    Bookmark.any_instance.stubs(:add_to_learning_queue)

    #bookmarked by following
    @following_card = @cards[0]
    create(:bookmark, bookmarkable: @following_card, user: @following)

    #bookmarked_by follower
    @followers_card = @cards[1]
    create(:bookmark, bookmarkable: @followers_card, user: @follower)

    #bookmarked by other user id
    @other_user_card = @cards[2]
    create(:bookmark, bookmarkable: @other_user_card, user: @other_user)
  end

  def posted_by_setup(term)
    search_setup

    #posted by following
    @following_postable = create(:card, organization_id: @org.id, author: @following, ecl_id: "ecl_1#{term}")
    #posted_by follower
    @followers_postable = create(:card, organization_id: @org.id, author: @follower, ecl_id: "ecl_2#{term}")
    #posted by other user id
    @other_user_card = create(:card, organization_id: @org.id, author: @other_user, ecl_id: "ecl_3#{term}")
  end

  test 'should accept sociative as parameter' do
    org = create(:organization)
    Config.create(configable_id: org.id, configable_type: 'Organization', name: 'sociative/search', data_type: 'boolean', value: true);
    signed_in_user = create(:user, organization: org)
    query = 'query'
    cards = build_list(:card, 5, organization: org)

    #when sociative param is true
    Search::ChannelSearch.any_instance.expects(:search).never
    Search::UserSearch.any_instance.expects(:search).never
    Search::TeamSearch.any_instance.expects(:search).never
    ecl_search = mock()
    EclApi::EclSearch.expects(:new).with(org.id, signed_in_user.id).returns(ecl_search).once
    ecl_search.expects(:search).with(has_entries(query: query, limit: 10, offset: 0, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: ["article"], source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, rank_params: {user_ids: []}, sociative: true }))).returns(OpenStruct.new(results: cards, aggregations: [])).once

    api_v2_sign_in(signed_in_user)
    get :index, q: query, content_type: ["article"], limit: 10, offset: 0, sociative: true, format: :json
  end

  test "should not trigger MetricsRecorderJob if any offsets are given" do
    assert_equal @controller.params, {}
    @controller.params[:offset] = 1
    Analytics::MetricsRecorderJob.expects(:perform_later).never
    @controller.send :record_search_metrics
  end

  test "should trigger MetricsRecorderJob on request to index route if users org has metrics enabled" do
    org = create(:organization)
    users = create_list(:user, 2, organization: org)
    signed_in_user = create(:user, organization: org)
    channels = create_list(:channel, 2, organization: org)
    create(:follow, followable: users.first, user: signed_in_user)
    create(:follow, followable: channels.first, user: signed_in_user)

    # media cards
    media_link_card = create(:card, card_type: 'media', card_subtype: 'link', resource: create(:resource))
    media_video_card = create(:card, card_type: 'media', card_subtype: 'video', resource: create(:resource))
    media_insight_card = create(:card, message: 'some message', card_type: 'media', card_subtype: 'text')
    media_image_card = create(:card, card_type: 'media', card_subtype: 'image')
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)
    media_image_card.file_resources << fr1

    # poll
    poll_card = create(:card, card_type: "poll", card_subtype: 'text')
    poll_card.quiz_question_options << create(:quiz_question_option, quiz_question: poll_card)

    # pathway
    pathway_card = create(:card, card_type: 'pack', card_subtype: 'simple')
    hidden_cards = create_list(:card, 2, hidden: true)
    Card.any_instance.stubs(:reindex).returns true

    hidden_cards.each do |card|
      CardPackRelation.add(cover_id: pathway_card.id, add_id: card.id, add_type: card.class.name)
    end

    # video stream card
    video_stream_card = create(:card, card_type: 'video_stream', card_subtype: 'simple', author: users.first)
    vs = create(:iris_video_stream, card_id: video_stream_card.id, status: 'past')
    vs.recordings << create(:recording, transcoding_status: 'available', mp4_location: 'http://something.mp4', hls_location: 'http://something.m3u8')

    video_stream_card.reload
    cards = [media_link_card, media_video_card, media_insight_card, media_image_card, poll_card, pathway_card, video_stream_card]

    query = "query"
    limit = 10
    offset = 0
    Search::UserSearch.any_instance.expects(:search).with(limit: limit, offset: offset, filter: {}, load: true, q: query, viewer: signed_in_user).returns(Search::UserSearchResponse.new(results: users, total: 20)).once
    Search::ChannelSearch.any_instance.expects(:search).with(limit: limit, offset: offset, :filter => {:include_private => true, skip_aggs: true}, load: true, q: query, viewer: signed_in_user, sort: [{:key => '_score', :order => :desc}]).returns(Search::ChannelSearchResponse.new(results: channels, total: 10)).once

    ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items"]
    ecl_search = mock()
    EclApi::EclSearch.expects(:new).with(org.id, signed_in_user.id).returns(ecl_search).once
    ecl_search.expects(:search).with(has_entries(query: query, limit: limit, offset: offset, :query_type => 'and', load_tags: true, filter_params: has_entries({content_type: nil, source_id: nil, source_type_name: nil, exclude_ecl_card_id: nil, topic: nil, type: "search", rank: true, rank_params: {user_ids: []} }))).returns(OpenStruct.new(results: cards, aggregations: ecl_response['aggregations'])).once

    api_v2_sign_in(signed_in_user)
    stub_time = Time.now
    @controller.stubs(:get_metadata_payload).returns foo: "bar"
    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        recorder: "Analytics::SearchRecorder",
        actor: Analytics::MetricsRecorder.user_attributes(signed_in_user),
        additional_data: { foo: "bar" },
        search_query: "query",
        results_count: 59,
        org: Analytics::MetricsRecorder.org_attributes(org),
        timestamp: stub_time.to_i,
        event: "homepage_searched"
      )
    )
    Timecop.freeze(stub_time) do
      get :index, q: "query", limit: limit, offset: offset, format: :json
    end
    assert_response :ok
  end

  class AccessControlTest < ActionController::TestCase
    setup do
      Organization.any_instance.unstub :create_general_channel
      User.any_instance.unstub :add_to_org_general_channel

      @organization = create :organization
      @user = create :user, organization: @organization
      @ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items"]

      api_v2_sign_in(@user)

      Search::ChannelSearch.any_instance.expects(:search)
        .returns(Search::ChannelSearchResponse.new(results: [], total: 0))
        .once

      Search::UserSearch.any_instance.expects(:search).
        returns(Search::UserSearchResponse.new(results: [], total: 0))
        .once
    end

    test 'send users_with_access with the current user id' do
      # Assert that users_with_access is present in the search query to the ECL
      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/#{Settings.ecl.api_version}/content_items/search")
        .with(body: hash_including(users_with_access: [@user.id]))
        .to_return(body: @ecl_response.to_json, status: 200)

      get :index, q: 'query', format: :json
    end

    test 'send groups_with_access with all groups that the user belongs to' do
      # Create groups and add user to it
      group1 = create :team, organization: @organization
      group2 = create :team, organization: @organization

      group1.add_user @user
      group2.add_user @user

      # Assert that groups_with_access is present with the group ids
      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/#{Settings.ecl.api_version}/content_items/search")
        .with(body: hash_including(groups_with_access: [group1.id, group2.id]))
        .to_return(body: @ecl_response.to_json, status: 200)

      get :index, q: 'query', format: :json
    end

    test 'send channels_with_access with all channels that the user follows' do
      # Create channels and add user to it
      channel1 = create :channel, organization: @organization
      channel2 = create :channel, organization: @organization
      general_channel = @organization.general_channel

      create :follow, user: @user, followable: channel1
      create :follow, user: @user, followable: channel2
      create :follow, user: @user, followable: general_channel

      stub_request(:post, "http://test.localhost/api/v1/content_items/search")

      # Assert that groups_with_access is present with the group ids
      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/#{Settings.ecl.api_version}/content_items/search")
        .with(body: hash_including(channels_with_access: [general_channel.id, channel1.id, channel2.id]))
        .to_return(body: @ecl_response.to_json, status: 200)

      get :index, q: 'query', format: :json
    end
  end

  test 'INTEGRATION: returns all private contents across sources' do
    @organization = create :organization
    @user1 = create :user, organization: @organization
    api_v2_sign_in(@user1)

    @user2 = create :user, organization: @organization

    group1 = create :team, organization: @organization
    group2 = create :team, organization: @organization
    group3 = create :team, organization: @organization

    group1.add_user @user1
    group2.add_user @user2
    group2.add_user @user1

    channel1 = create :channel, organization: @organization
    channel2 = create :channel, organization: @organization
    channel3 = create :channel, organization: @organization

    general_channel = @organization.general_channel

    create :follow, user: @user1, followable: channel1
    create :follow, user: @user2, followable: channel2

    @pathway1 = create(:card, card_type: 'pack', card_subtype: 'simple', is_public: false, organization_id: @organization.id, author_id: @user1.id)
    @pathway1.publish!

    @pathway2 = create(:card, card_type: 'pack', card_subtype: 'simple', is_public: true, organization_id: @organization.id, author_id: @user1.id)
    @pathway2.publish!

    @pathway3 = create(:card, card_type: 'pack', card_subtype: 'simple', is_public: false, organization_id: @organization.id, author_id: @user2.id)
    @pathway3.publish!

    @pathway4 = create(:card, card_type: 'pack', card_subtype: 'simple', is_public: false, organization_id: @organization.id, author_id: @user2.id)
    @pathway4.publish!

    @private_card = create(:card, card_type: 'media', card_subtype: 'text', is_public: false, organization_id: @organization.id, author_id: @user1.id)
    @public_card = create(:card, card_type: 'media', card_subtype: 'text', is_public: true, organization_id: @organization.id, author_id: @user1.id)

    create(:card_user_share, user_id: @user1.id, card_id: @pathway1.id)
    create(:card_user_share, user_id: @user1.id, card_id: @pathway3.id)
    create(:card_user_share, user_id: @user1.id, card_id: @private_card.id)
    create(:card_user_share, user_id: @user2.id, card_id: @public_card.id)

    create(:teams_card, team_id: group1.id, card_id: @private_card.id, type: 'SharedCard')
    create(:teams_card, team_id: group2.id, card_id: @public_card.id, type: 'SharedCard')
    create(:teams_card, team_id: group1.id, card_id: @pathway1.id, type: 'SharedCard')
    create(:teams_card, team_id: group2.id, card_id: @pathway2.id, type: 'SharedCard')
    create(:teams_card, team_id: group1.id, card_id: @pathway3.id, type: 'SharedCard')
    create(:teams_card, team_id: group2.id, card_id: @private_card.id, type: 'SharedCard')
    create(:teams_card, team_id: group3.id, card_id: @pathway4.id, type: 'SharedCard')

    create(:channels_card, channel_id: channel1.id, card_id: @private_card.id)
    create(:channels_card, channel_id: channel2.id, card_id: @public_card.id)
    create(:channels_card, channel_id: channel1.id, card_id: @pathway1.id)
    create(:channels_card, channel_id: channel2.id, card_id: @pathway2.id)
    create(:channels_card, channel_id: channel1.id, card_id: @pathway3.id)
    create(:channels_card, channel_id: channel2.id, card_id: @private_card.id)
    create(:channels_card, channel_id: channel3.id, card_id: @pathway4.id)

    EclApi::CardConnector.any_instance.expects(:get_private_content_from_ecl)
      .returns([@private_card, @pathway1, @pathway3])

    get :private_content, q: 'query', only_private: true, format: :json

    resp = get_json_response(response)

    assert_equal 3, resp['cards'].count
    assert_same_elements [@private_card.id, @pathway1.id, @pathway3.id], resp['cards'].map{ |d| d['id'].to_i }
    assert_not_includes resp['cards'].map{ |c| c['id'].to_i }, [@pathway2.id, @public_card.id, @pathway4.id]
  end

  class AdditionalFilteringTest < ActionController::TestCase
    test 'send group filters' do
      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/#{Settings.ecl.api_version}/content_items/search")
        .with(body: hash_including(groups: [1,2,3]))
        .to_return(body: @ecl_response.to_json, status: 200)

      get :index, q: 'query', group_id: [1,2,3], format: :json
    end

    test 'send channel filters' do
      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/#{Settings.ecl.api_version}/content_items/search")
        .with(body: hash_including(channels: [1,2,3]))
        .to_return(body: @ecl_response.to_json, status: 200)

      get :index, q: 'query', channel_id: [1,2,3], format: :json
    end
  end

  test 'honor load_hidden passed in the search query and send it to the ECL' do
    # Initialization
    org = create(:organization)
    signed_in_user = create(:user, organization: org)

    api_v2_sign_in(signed_in_user)

    # Stub content search results
    ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items"]
    ecl_search = mock
    EclApi::EclSearch.expects(:new)
        .with(org.id, signed_in_user.id)
        .returns(ecl_search).once

    hidden_cards = create_list :card, 2, hidden: true

    # has_entries for some stupid reason doesn't work
    ecl_search.expects(:search)
        .with(:query => 'query', :load_tags => true, :limit => 10, :offset => 0, :query_type => 'and',
              :filter_params => {:content_type => nil, :type => 'search', :source_id => nil, :provider_name => nil,subjects: nil,languages: nil,
                                 :source_type_name => nil, :rank => true, :rank_params => {:user_ids => []}, :exclude_ecl_card_id => nil,
                                 :topic => nil, :users_with_access => [signed_in_user.id], :groups_with_access => [], :channels_with_access => [],
                                 :only_private => nil, :group_id => nil, :channel_id => nil, :is_admin => false, :load_hidden => true})
        .returns(OpenStruct.new(results: hidden_cards, aggregations: ecl_response['aggregations'], status: 200))

    # Stub user and channel and team search
    Search::UserSearch.any_instance.expects(:search).returns(Search::UserSearchResponse.new(results: [], total: 10)).once
    Search::ChannelSearch.any_instance.expects(:search).returns(Search::ChannelSearchResponse.new(results: [], total: 10)).once

    # Execution
    get :index, q: 'query', "load_hidden" => "true", format: :json

    # Assertions
    resp = get_json_response(response)
    card_ids = resp['cards'].map{|card| card['id']}

    assert_same_elements card_ids, hidden_cards.map{|card| card['id'].to_s}
  end
end

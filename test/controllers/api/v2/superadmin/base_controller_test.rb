require 'test_helper'

class Api::V2::Superadmin::BaseControllerTest < ActionController::TestCase
  class FakeController < Api::V2::Superadmin::BaseController
    def show
      head :ok
    end
  end

  setup do
    Rails.application.routes.draw do
      get 'show', controller: 'api/v2/superadmin/base_controller_test/fake', action: 'show'
    end
    @controller = FakeController.new
  end

  teardown do
    Rails.application.reload_routes!
  end

  test 'should require superadmin login' do
    create_default_org
    normal_user = create(:user, organization: Organization.default_org)
    api_v2_sign_in(normal_user)

    get :show, format: :json
    assert_response :unauthorized

    superadmin_user = create(:user, organization: Organization.default_org)
    SuperAdminUser.add_by_email(superadmin_user.email)

    api_v2_sign_in(superadmin_user)
    get :show, format: :json
    assert_response :ok
  end
end

require 'test_helper'

class Api::V2::Superadmin::PlatformMetricsControllerTest < ActionController::TestCase

  setup do
    create_default_org
    @user = create(:user, organization: Organization.default_org)
    SuperAdminUser.add_by_email(@user.email)
    api_v2_sign_in(@user)
  end

  test ':api send platform metrics' do
    offset = 12
    org = create(:organization)
    PeriodOffsetCalculator.expects(:month_offset).with(Time.utc("2016", "1")).returns(offset).once

    expected_data = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/metrics_drill_down_data.json")).with_indifferent_access
    OrganizationMetricsGatherer.expects(:current_period_data).with(organization: org, period: :month, offset: offset).returns({smartbites_consumed: 6, smartbites_created: 15, active_users: 3, average_session: 10, new_users: 1, engagement_index: 75, total_users: 4}).once
    expected_drill_down_data = Hash[OrganizationMetricsGatherer::METRICS.map{|metric| [metric, expected_data[metric][:drilldown][:current]]}]
    OrganizationMetricsGatherer.expects(:drill_down_data).with(organization: org, period: :month, offset: offset).returns(expected_drill_down_data).once

    get :index, team: org.host_name, year: "2016", month: "1", format: :json
    assert_response :ok
    json_response = get_json_response(response)

    # See fixture for dau calculation. It is just the average of active_users in the current drilldown array
    expected_response = {"team" => org.host_name, "year" => "2016", "month" => "1", "mau" => 3, "dau" => 2.5, "users_count" => 4}
    assert_equal(expected_response, json_response)
  end
end

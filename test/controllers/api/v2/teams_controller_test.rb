require 'test_helper'

class Api::V2::TeamsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    BranchmetricsApi.stubs :generate_link
    Invitation.any_instance.stubs :send_mail

    @org   = create :organization, name: 'some_org'
    role = create(:role, name: 'admin', organization: @org)
    @user  = create :user, organization: @org
    @admin = create :user, organization: @org
    @admin.roles << role

    @everyone_team = @org.teams.where(is_everyone_team: true).first

    @teams = []
    (1..10).each do |i|
      @teams << create(:team, organization: @org, created_at: i.minutes.ago)
    end

    @team1, @team2, @team3 = *@teams[0..2]
    @team4, @team5, @team6 = *@teams[3..5]
    @team7, @team8, @team9, @team10 = *@teams[6..10]

    @member1, @member2, @member3, @member4 = create_list :user, 4, organization: @org
    @users = [@member1, @member2, @member3, @member4]
    # user `@admin` is an admin to 2 teams
    @team1.add_admin @admin
    @team2.add_admin @admin

    @team1.update_attributes(is_private: true)
    @team4.update_attributes(is_private: true)

    @inv1 = create(:invitation, invitable: @team2, recipient_email: @member2.email, recipient_id: @member2.id, roles: 'member')
    @inv2 = create(:invitation, invitable: @team4, recipient_email: @member2.email, recipient_id: @member2.id, roles: 'member')

    # user `@member1` is an admin to @team1 and member to rest of the teams
    @team1.add_admin @member1
    [@team2, @team3, @team4, @team5, @team6, @team7, @team8, @team9, @team10].each { |team| team.add_member @member1 }

    # user `@member2`, `@member3`, `@member4` is a member to @team1 respectively
    @team1.add_member @member2
    @team1.add_member @member3
    @team1.add_member @member4

    # @card1 for team1
    @card1 = create(:card, author: @user, organization: @org)

    # @card1 shared to @team2
    @shared_card = create(:shared_card, team: @team2, card: @card1)

    # @card1 assigned to @team2
    @assignment = create(:assignment, assignee: @user, assignable: @card1)
    create(:team_assignment, team_id: @team2.id, assignment_id: @assignment.id)

    @ecl_id = "rerf-rfde"

    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{@ecl_id}").
      to_return(status: 200, body: get_ecl_card_response(@ecl_id).to_json)

    UrlMetadataExtractor.stubs(:get_metadata).returns({
        'og:type'    => 'video',
        'embed_html' => '<iframe />',
        'title'      => 'somev' })


    api_v2_sign_in @admin
  end

  class TeamsIndexTests < ActionController::TestCase
    setup do
      BranchmetricsApi.stubs :generate_link
      Invitation.any_instance.stubs :send_mail

      @org   = create :organization, name: 'some_org'
      role = create(:role, name: 'admin', organization: @org)
      @user  = create :user, organization: @org
      @admin = create :user, organization: @org
      @admin.roles << role

      @everyone_team = @org.teams.where(is_everyone_team: true).first

      @teams = []
      (1..10).each do |i|
        @teams << create(:team, organization: @org, created_at: i.minutes.ago)
      end

      @team1, @team2, @team3 = *@teams[0..2]
      @team4, @team5, @team6 = *@teams[3..5]
      @team7, @team8, @team9, @team10 = *@teams[6..10]

      @member1, @member2, @member3, @member4 = create_list :user, 4, organization: @org
      @users = [@member1, @member2, @member3, @member4]
      # user `@admin` is an admin to 2 teams
      @team1.add_admin @admin
      @team2.add_admin @admin

      # user `@member1` is an admin to @team1 and member to rest of the teams
      @team1.add_admin @member1
      [@team2, @team3, @team4, @team5, @team6, @team7, @team8, @team9, @team10].each { |team| team.add_member @member1 }

      # user `@member2`, `@member3`, `@member4` is a member to @team1 respectively
      @team1.add_member @member2
      @team1.add_member @member3
      @team1.add_member @member4

      # @card1 for team1
      @card1 = create(:card, author: @user)

      # @card1 shared to @team2
      @shared_card = create(:shared_card, team: @team2, card: @card1)

      # @card1 assigned to @team2
      @assignment = create(:assignment, assignee: @user, assignable: @card1)
      create(:team_assignment, team_id: @team2.id, assignment_id: @assignment.id)

      @ecl_id = "rerf-rfde"

      stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{@ecl_id}").
        to_return(status: 200, body: get_ecl_card_response(@ecl_id).to_json)

      UrlMetadataExtractor.stubs(:get_metadata).returns({
                                                          'og:type'    => 'video',
                                                          'embed_html' => '<iframe />',
                                                          'title'      => 'somev' })


      api_v2_sign_in @admin
    end

    test ':api should return all teams for admin' do
      everyone_team = @org.teams.find_by(is_everyone_team: true)
      get :index, assign_id: ('ECL-' + @ecl_id), assign_type: 'card', limit: 15, format: :json

      resp = get_json_response(response)
      assert_equal 2, resp['teams'].count
      assert_equal [@team1, @team2].map(&:id).to_set, resp['teams'].collect { |t| t['id'] }.to_set
    end

    test 'should return teams with current user as admin role ' do
      api_v2_sign_in @member1
      get :index, assign_id: ('ECL-' + @ecl_id), assign_type: 'card', role: ['admin'], limit: 15, format: :json
      resp = get_json_response(response)
      assert_same_elements [@team1].map(&:id), resp['teams'].collect { |t| t['id'] }.to_set
      assert_equal 1, resp['teams'].count
    end

    test 'should return teams with current user as member role ' do
      api_v2_sign_in @member1
      get :index, assign_id: ('ECL-' + @ecl_id), assign_type: 'card', role: 'member', limit: 15, format: :json
      resp = get_json_response(response)
      assert_same_elements [@team2, @team3, @team4, @team5, @team6, @team7, @team8, @team9, @team10].map(&:id), resp['teams'].collect { |t| t['id'] }.to_set
      assert_equal 9, resp['teams'].count
    end

    test 'should lists teams by role #member' do
      api_v2_sign_in @member1

      get :index, assign_id: ('ECL-' + @ecl_id), assign_type: 'card', role: ['member'], format: :json

      resp = get_json_response(response)
      assert_equal [@team2, @team3, @team4, @team5, @team6, @team7, @team8, @team9, @team10].map(&:id).to_set, resp['teams'].collect { |t| t['id'] }.to_set
    end

    test 'lists teams by search term' do
      api_v2_sign_in @member1

      get :index, assign_id: ('ECL-' + @ecl_id), assign_type: 'card', role: ['member'], search_term: @team2.name, format: :json

      resp = get_json_response(response)
      assert_equal [@team2.id].to_set, resp['teams'].collect { |t| t['id'] }.to_set

      get :index, assign_id: ('ECL-' + @ecl_id), assign_type: 'card', role: ['member'], search_term: 'non-exact', format: :json

      resp = get_json_response(response)
      assert_empty resp['teams']
    end

    test 'request should be successful if no assign_id is passed in' do
      api_v2_sign_in @member1

      assert_nothing_raised do
        get :index, assign_type: 'card', role: ['member'], search_term: @team2.name, format: :json
      end

      assert_response :success
    end

    test 'should sort teams by joining date' do
      teams = []
      # The reason future date is used because, member is part of
      # other groups in setup block
      # Team created today + 1 day
      Timecop.freeze(Time.now + 1.days) do
        teams = create_list(:team, 2, organization: @org)
      end

      # Member added Today + 2.days
      Timecop.freeze(Time.now + 2.days) do
        teams.first.add_member @member2
      end

      # Member added Today + 3.days
      Timecop.freeze(Time.now + 3.days) do
        teams.last.add_member @member2
      end

      api_v2_sign_in @member2

      get :index, sort: 'team_joined_at', order: 'desc', limit: 2, format: :json
      resp = get_json_response response
      assert_equal teams.reverse.map(&:id), resp['teams'].map {|team| team['id']}
    end

    test 'should sort team by title' do
      teams = []
      member = create(:user, organization: @org)
      # Team created today + 1 day
      Timecop.freeze(Time.now + 1.days) do
        teams << create(:team, name: 'alpha', organization: @org)
        teams << create(:team, name: 'beta', organization: @org)
      end

      # Member added Today + 2.days
      Timecop.freeze(Time.now + 2.days) do
        teams.first.add_member member
      end

      # Member added Today + 3.days
      Timecop.freeze(Time.now + 3.days) do
        teams.last.add_member member
      end

      api_v2_sign_in member

      get :index, sort: 'name', order: 'desc', limit: 2, format: :json
      resp = get_json_response response
      assert_equal teams.reverse.map(&:id), resp['teams'].map {|team| team['id']}
    end

    test 'supports teams list with offset and limit' do
      api_v2_sign_in @member1

      get :index, assign_type: 'card', role: ['member'], offset: 5, limit: 5, format: :json
      resp  = get_json_response(response)
      teams = resp['teams'].collect { |t| t['id'] }

      assert_equal [@team7, @team8, @team9, @team10].map(&:id).to_set, teams.to_set
      assert_equal 9, resp['total']
    end

    test 'should return is_everyone_team flag for team details' do
      Organization.any_instance.unstub :create_org_level_team
      @org.send :create_org_level_team
      normal_team = create(:team, organization: @org)
      everyone_team = @org.teams.find_by(is_everyone_team: true)
      everyone_team.add_admin @admin
      normal_team.add_admin @admin

      get :index, format: :json

      assert_response :ok
      resp = get_json_response(response)
      assert_equal everyone_team.id, resp["teams"].select{|team| team["is_everyone_team"] == true}.first["id"]
      assert_empty resp["teams"].select{|team| team.key?("is_everyone_team") == false}
    end

    test 'should return my open and private team' do
      user = create(:user, organization: @org)
      team = create(:team, organization: @org)
      team1 = create(:team, organization: @org, is_private: true)
      team2 = create(:team, organization: @org, is_private: false)
      team.add_member(user)
      team1.add_member(user)

      api_v2_sign_in user

      get :index, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal 2, resp['total']
      assert_same_elements [team.id, team1.id], resp['teams'].map { |i| i['id'] }
      refute_includes resp['teams'].map { |i| i['id'] }, team2.id
    end

    test 'should return my private team' do
      user = create(:user, organization: @org)
      team = create(:team, organization: @org)
      team1 = create(:team, organization: @org, is_private: true)
      team.add_member(user)
      team1.add_member(user)

      api_v2_sign_in user

      get :index, is_private: 'true', format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal 1, resp['total']
      assert_equal team1.id, resp['teams'].first['id']
    end

    test 'should return teams pending, user' do
      user = create(:user, organization: @org)
      team = create(:team, organization: @org)
      team1 = create(:team, organization: @org, is_private: true)
      team.add_member(user)
      user.invitations.create(
        sender_id: @admin.id,
        invitable_id: team1.id,
        invitable_type: 'Team',
        recipient_email: user.email,
        roles: 'member'
      )
      api_v2_sign_in user

      get :index, pending: 'true', format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal 1, resp['total']
      assert_equal team1.id, resp['teams'].first['id']
    end

    test 'should return all open, user open/private and pending teams' do
      org = create(:organization)
      user = create(:user, organization: org)
      admin = create(:user, organization: org, organization_role: 'admin')
      team = create(:team, organization: org)
      team1 = create(:team, organization: org, is_private: true)
      team2 = create(:team, organization: org)
      team.add_member(user)
      user.invitations.create(
        sender_id: admin.id,
        invitable_id: team1.id,
        invitable_type: 'Team',
        recipient_email: user.email,
        roles: 'member'
      )
      api_v2_sign_in user

      get :index, all: 'true', format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal 3, resp['total']
      assert_same_ids [team.id, team1.id, team2.id], resp['teams'].map { |e| e['id'] }
    end

    test 'should return teams with which user can share the card if writables flag is true for some teams' do
      user = create(:user, organization: @org)
      first_team = create(:team, organization: @org)
      second_team = create(:team, organization: @org, only_admin_can_post: true)
      third_team = create(:team, organization: @org, only_admin_can_post: true)
      first_team.add_member(user)
      second_team.add_admin(user)
      third_team.add_sub_admin(user)

      api_v2_sign_in user

      get :index, writables: 'true' , format: :json
      assert_response :ok
      resp = get_json_response(response)

      assert_equal 3, resp['total']
      assert_equal first_team.id, resp['teams'].first['id']
      assert_equal second_team.id, resp['teams'].second['id']
      assert_equal third_team.id, resp['teams'].third['id']
    end

    test 'should return teams where user is admin if writables is true & role is admin' do
      user = create(:user, organization: @org)
      first_team = create(:team, organization: @org)
      second_team = create(:team, organization: @org, only_admin_can_post: true )
      third_team = create(:team, organization: @org, only_admin_can_post: true)
      first_team.add_member(user)
      second_team.add_admin(user)
      third_team.add_member(user)

      api_v2_sign_in user

      get :index, writables: "true" , format: :json
      assert_response :ok
      resp = get_json_response(response)

      assert_equal 2, resp['total']
      assert_equal first_team.id, resp['teams'].first['id'] #regardless of role sent, where flag is false, those teams will be visible
      assert_equal second_team.id, resp['teams'].second['id']
    end

    test 'should return teams where user is sub_admin if writables is true & role is sub_admin' do
      user = create(:user, organization: @org)
      first_team = create(:team, organization: @org)
      second_team = create(:team, organization: @org, only_admin_can_post: true )
      third_team = create(:team, organization: @org, only_admin_can_post: true)
      first_team.add_admin(user)
      second_team.add_member(user)
      third_team.add_sub_admin(user)

      api_v2_sign_in user

      get :index, writables: "true" , format: :json
      assert_response :ok
      resp = get_json_response(response)

      assert_equal 2, resp['total']
      assert_equal first_team.id, resp['teams'].first['id'] #regardless of role sent, where flag is false, those teams will be visible
      assert_equal third_team.id, resp['teams'].second['id']
    end

    test 'should return teams where user is member if writables is true & role is member' do
      user = create(:user, organization: @org)
      user_1 = create(:user, organization: @org)
      first_team = create(:team, organization: @org)
      second_team = create(:team, organization: @org, only_admin_can_post: true )
      third_team = create(:team, organization: @org, only_admin_can_post: true)

      first_team.add_member(user)
      second_team.add_member(user)
      third_team.add_member(user)

      api_v2_sign_in user

      get :index, writables: "true" , format: :json
      assert_response :ok
      resp = get_json_response(response)

      assert_equal 1, resp['total']
      assert_equal first_team.id, resp['teams'].first['id'] #regardless of role sent, where flag is false, those teams will be visible
    end

    test 'should return all teams where user has a role when writables flag is not passed' do
      user = create(:user, organization: @org)
      user_1 = create(:user, organization: @org)
      first_team = create(:team, organization: @org)
      second_team = create(:team, organization: @org, only_admin_can_post: true )
      third_team = create(:team, organization: @org, only_admin_can_post: true)
      fourth_team = create(:team, organization: @org)

      first_team.add_member(user)
      second_team.add_member(user)
      third_team.add_member(user)
      fourth_team.add_admin(user)

      api_v2_sign_in user

      get :index, role:['admin'], format: :json
      assert_response :ok
      resp = get_json_response(response)

      assert_equal 1, resp['total']
      assert_equal fourth_team.id, resp['teams'].first['id']
    end
  end

  test 'should search org teams' do
    Search::TeamSearch.any_instance.expects(:search).
      with(
        q: '', viewer: @member1,
        filter: {skip_aggs: true},
        offset: 0, limit: 10,
        load: true
      ).
      returns(Search::TeamSearchResponse.new(
        q: '',
        results: @teams,
        status: 'success',
        offset: 0,
        limit: 10,
        total: 10,
        ))
    api_v2_sign_in @member1

    get :search, q: '', format: :json

    resp = get_json_response(response)
    teams = resp["teams"]

    assert_equal true, teams.first['is_team_admin']
    assert_equal false, teams.first['is_team_sub_admin']
    assert_equal false, teams.first['is_member']
    assert_equal false, teams.first['is_pending']

    assert_equal 10, resp['total']
    assert_same_ids @teams, teams
  end

  test 'should search only private org teams' do
    Search::TeamSearch.any_instance.expects(:search).
      with(
        q: '', viewer: @member1,
        filter: {is_private: 'true', skip_aggs: true},
        offset: 0, limit: 10,
        load: true
      ).
      returns(Search::TeamSearchResponse.new(
        q: '',
        results: [@team1, @team4],
        status: 'success',
        offset: 0,
        limit: 10,
        total: 2,
        ))
    api_v2_sign_in @member1

    get :search, q: '', is_private: 'true', format: :json

    resp = get_json_response(response)
    teams = resp["teams"]
    assert_equal 2, resp['total']
    assert_same_ids [@team1, @team4], teams
  end

  test 'should search only open org teams' do
    Search::TeamSearch.any_instance.expects(:search).
      with(
        q: '', viewer: @member1,
        filter: {is_private: 'false', skip_aggs: true},
        offset: 0, limit: 10,
        load: true
      ).
      returns(Search::TeamSearchResponse.new(
        q: '',
        results: @teams - [@team1, @team4],
        status: 'success',
        offset: 0,
        limit: 10,
        total: 8,
        ))
    api_v2_sign_in @member1

    get :search, q: '', is_private: 'false', format: :json

    resp = get_json_response(response)
    teams = resp["teams"]
    assert_equal 8, resp['total']
    assert_same_ids @teams - [@team1, @team4], teams
  end

  test "should search only current user teams" do
    Search::TeamSearch.any_instance.expects(:search).
      with(
        q: '', viewer: @member2,
        filter: {current_user_teams: true, skip_aggs: true},
        offset: 0, limit: 10,
        load: true
      ).
      returns(Search::TeamSearchResponse.new(
        q: '',
        results: [@team1],
        status: 'success',
        offset: 0,
        limit: 10,
        total: 1,
        ))
    api_v2_sign_in @member2

    get :search, q: '', current_user_teams: true, format: :json

    resp = get_json_response(response)
    teams = resp["teams"]
    assert_equal 1, resp['total']
    assert_same_ids [@team1], teams
  end

  test "should search only current user teams with pending requests" do
    Search::TeamSearch.any_instance.expects(:search).
      with(
        q: '', viewer: @member2,
        filter: {current_user_pending_teams: true, skip_aggs: true},
        offset: 0, limit: 10,
        load: true
      ).
      returns(Search::TeamSearchResponse.new(
        q: '',
        results: [@team2, @team4],
        status: 'success',
        offset: 0,
        limit: 10,
        total: 2,
        ))
    api_v2_sign_in @member2

    get :search, q: '', current_user_pending_teams: true, format: :json

    resp = get_json_response(response)
    teams = resp["teams"]
    assert_equal 2, resp['total']
    assert_same_ids [@team2, @team4], teams
  end

  test 'top contributors should return nil when no cards by any members' do
    get :top_contributors,id: @team1.id, format: :json
    resp = get_json_response(response)
    assert_equal 0, resp['top_contributors'].count
  end

  test 'top contributors should respect limit and offet' do
    # member 1's assigned cards
    create_list(:card, 2, author_id: @member1.id).each{|card|
      assignment = create(:assignment, assignable: card, assignee: @users.last)
      team_assignment = create(:team_assignment, assignment: assignment, assignor: @member1, team_id: @team1.id)
    }

    # member 2's shared and assigned cards
    create_list(:card, 2, author_id: @member2.id).each{|card|
      create(:shared_card, team_id: @team1.id, card_id: card.id, user_id: @member2.id)
    }
    create_list(:card, 2, author_id: @member2.id).each{|card|
      assignment = create(:assignment, assignable: card, assignee: @users.first)
      team_assignment = create(:team_assignment, assignment: assignment, assignor: @member2, team_id: @team1.id)
    }

    get :top_contributors, id: @team1.id, limit: 2, offset: 0, format: :json
    resp = get_json_response(response)

    assert_equal 2, resp['top_contributors'].count
    assert_equal @member2.id, resp['top_contributors'].first['id']
    assert_equal @member2.cards.count, resp['top_contributors'].first['score']
    assert_equal @member1.id, resp['top_contributors'].last['id']
    assert_equal @member1.cards.count, resp['top_contributors'].last['score']
  end

  test 'should return team members' do
    results = search_results(@users)
    inv = create(:invitation, recipient_email: generate(:email_address), invitable: @team1, roles: 'member')

    Search::UserSearch.any_instance.expects(:search).
        with(
            q: nil,
            viewer: @member1,
            offset: 0,
            limit: 10,
            allow_blank_q: true,
            sort: :created_at,
            order: :desc,
            filter: {scope: @team1, uncomplete: true}
        ).
        returns(Search::UserSearchResponse.new(
            q: nil,
            results: results,
            status: 'success',
            offset: 0,
            limit: 10,
            total: 3,
        ))

    api_v2_sign_in @member1

    get :users, id: @team1.id, format: :json

    resp = get_json_response(response)
    members = resp["members"]

    members = resp["members"]
    pending = resp["pending"]

    assert_equal 3, members['total']
    assert_nil resp['query']
    assert_same_ids @users, members['users']

    user_member = members['users'].by_id(@member1.id)
    mem1 = members['users'].by_id(@member2.id)
    mem2 = members['users'].by_id(@member3.id)

    assert_equal @member3.name, mem2['name']
    assert_equal 'member', mem2['role']
    assert_not mem2['pending']

    assert_not_empty pending['users']
    assert_equal 1, pending['total']
  end

  test 'should only return pending members' do
    inv = create(:invitation,
        recipient_email: generate(:email_address),
        invitable: @team1, roles: 'member')

    api_v2_sign_in @member1

    get :users, id: @team1.id, user_type: 'pending', format: :json
    resp = get_json_response(response)
    pending = resp['pending']

    assert_equal 1, pending['total']
    assert_equal inv.recipient_email, pending['users'][0]['email']
    assert_equal inv.recipient_id, pending['users'][0]['user_id']
    assert_empty resp['members']['users']
  end

  test 'users who are not team or org admins should not be able to invite' do
    user = create(:user, organization: @admin.organization)
    api_v2_sign_in @member2

    post :invite, id: @team1.id, roles:'member', email: user.email, format: :json
    assert_response :unauthorized
  end

  test 'users should only be invited as admin or member' do
    user = create(:user, organization: @admin.organization)
    api_v2_sign_in @admin

    post :invite, id: @team1.id, roles:'unknown_role', emails: [user.email], format: :json
    assert_response :unprocessable_entity
  end

  test 'show pending members count' do
    assert_empty @team1.invitations.pending

    create(:invitation, invitable: @team2)
    get :show, id: @team2.id, format: :json
    resp = get_json_response(response)
    assert_equal 2, resp['pending_members_count']
  end

  test ':api show should have proper value for is_dynamic_selected field' do
    usr = create(:user, organization: @org)
    @team1.add_member(usr)
    create(
      :dynamic_group_revision,
      group_id: @team1.id,
      organization_id: @org.id,
      external_id: 'external_id'
    )

    api_v2_sign_in(usr)
    get :show, id: @team1.id, format: :json
    assert_response :ok
    resp = get_json_response(response)

    assert resp['is_dynamic_selected']
  end

  test 'existing team users and already invited users should not be invited' do
    MultiInvitationsJob.unstub(:perform_later)
    EdcastActiveJob.unstub(:perform_later)
    users = create_list(:user, 2, organization: @admin.organization)
    invited_user = create(:invitation, invitable: @team1, recipient_email: users[1].email, roles: 'member')
    api_v2_sign_in @admin
    assert_difference -> {Invitation.count}, 1 do
      post :invite, id: @team1.id, roles:'member', emails: [users[0].email, @member1.email, invited_user.recipient_email], format: :json
    end
    assert_response :ok
    assert_match "Some users were already invited:", response.body
    assert_match @member1.email,response.body
    assert_match invited_user.recipient_email,response.body
  end

  test 'should be able to invite users to team as admin' do
    MultiInvitationsJob.unstub(:perform_later)
    EdcastActiveJob.unstub(:perform_later)

    users = create_list(:user, 2, organization: @admin.organization)
    api_v2_sign_in @admin
    post :invite, id: @team1.id, roles:'admin', emails: users.map(&:email), format: :json
    assert_response :ok

    invitations = Invitation.where(invitable: @team1, recipient_email: users.map(&:email))
    assert_equal users[0].email, invitations[0].recipient_email
    assert_equal users[0].id, invitations[0].recipient_id
    assert_equal users[1].email, invitations[1].recipient_email
    assert_equal users[1].id, invitations[1].recipient_id
    assert_equal @team1.id, invitations[0].invitable_id
  end

  test ':api should be able to invite users to team as member' do
    MultiInvitationsJob.unstub(:perform_later)
    EdcastActiveJob.unstub(:perform_later)

    user = create(:user, organization: @admin.organization)
    api_v2_sign_in @admin

    post :invite, id: @team1.id, roles:'member', emails: [user.email], format: :json
    assert_response :ok

    invitation = Invitation.find_by(invitable: @team1, recipient_email: user.email)
    assert_equal user.email, invitation.recipient_email
    assert_equal user.id, invitation.recipient_id
    assert_equal @team1.id, invitation.invitable_id
    assert_equal 'member', invitation.roles
  end

  test 'should return admin team members' do
    results = search_results(@team1.admins)

    Search::UserSearch.any_instance.expects(:search).
        with(
            q: nil,
            viewer: @member1,
            offset: 0,
            limit: 10,
            allow_blank_q: true,
            sort: :created_at,
            order: :desc,
            filter: {role: 'admin', scope: @team1, uncomplete: true}
        ).
        returns(Search::UserSearchResponse.new(
            q: nil,
            results: results,
            status: 'success',
            offset: 0,
            limit: 10,
            total: 2,
        ))

    api_v2_sign_in @member1

    get :users, id: @team1.id, user_type: 'admin', format: :json

    resp = get_json_response(response)
    admin_members = resp['members']
    assert_equal 'admin', admin_members['users'][0]['role']
    assert_equal 'admin', admin_members['users'][1]['role']
    assert_empty admin_members['users'].select{|d| d['role'] == 'member'}
    assert_equal 2, admin_members['total']
    assert_empty resp['pending']['users']
  end

  test 'should return members only' do
    results = search_results(@team1.members)

    Search::UserSearch.any_instance.expects(:search).
        with(
            q: nil,
            viewer: @member1,
            offset: 0,
            limit: 10,
            allow_blank_q: true,
            sort: :created_at,
            order: :desc,
            filter: {role: 'member', scope: @team1, uncomplete: true}
        ).
        returns(Search::UserSearchResponse.new(
            q: nil,
            results: results,
            status: 'success',
            offset: 0,
            limit: 10,
            total: 3,
        ))

    api_v2_sign_in @member1

    get :users, id: @team1.id, user_type: 'member', format: :json

    resp = get_json_response(response)
    members = resp['members']

    assert_empty members.select{|d| d['role'] == 'admin'}
    assert_equal 3, members['total']
    assert_empty resp['pending']['users']
  end

  test 'should only return non suspended members' do
    @member4.suspend!
    results = search_results(@users - [@member4])
    Search::UserSearch.any_instance.expects(:search).
    with(
        q: nil,
        viewer: @member1,
        offset: 0,
        limit: 10,
        allow_blank_q: true,
        sort: :created_at,
        order: :desc,
        filter: {scope: @team1, uncomplete: true}
        ).
    returns(Search::UserSearchResponse.new(
        q: nil,
        results: results,
        status: 'success',
        offset: 0,
        limit: 10,
        total: 2,
        ))

    api_v2_sign_in @member1
    get :users, id: @team1.id,  format: :json
    resp = get_json_response(response)
    members =  resp['members']
    assert_equal 2, members['total']
    assert_not_includes members['users'].map{|m| m[:id]}, @member4.id
  end

  test 'should return not found for invalid role value' do
    results = search_results(@users)
    inv = create(:invitation, recipient_email: generate(:email_address), invitable: @team1, roles: 'member')

    Search::UserSearch.any_instance.expects(:search).
        with(
            q: nil,
            viewer: @member1,
            offset: 0,
            limit: 10,
            allow_blank_q: true,
            sort: :created_at,
            order: :desc,
            filter: {scope: @team1, uncomplete: true}
        ).
        returns(Search::UserSearchResponse.new(
            q: nil,
            results: results,
            status: 'success',
            offset: 0,
            limit: 10,
            total: 3,
        )).never

    api_v2_sign_in @member1

    get :users, id: @team1.id, user_type: 'test', format: :json
    assert_response 404
  end

  test ':api search non-pending team members' do
    results = search_results [@member1, @member2]
    # more params
    Search::UserSearch.any_instance.expects(:search).
        with(
            q: 'query',
            viewer: @member1,
            offset: 1,
            limit: 2,
            allow_blank_q: true,
            sort: :created_at,
            order: :desc,
            filter: {scope: @team1, uncomplete: true},
        ).
        returns(Search::UserSearchResponse.new(
          q: nil,
          results: results,
          status: 'success',
          offset: 1,
          limit: 2,
          total: 2,
        ))

    api_v2_sign_in @member1

    get :users, id: @team1.id, q: 'query', limit: 2, offset: 1, format: :json

    resp = get_json_response(response)
    members = resp["members"]

    assert_equal 2, members['total']
    assert_not_nil resp['query']
    assert_same_ids [@member1, @member2], members['users']

    mem1 = members['users'].by_id(@member1.id)
    mem2 = members['users'].by_id(@member2.id)

    assert_equal @member2.name, mem2['name']
    assert_equal 'member', mem2['role']
    assert_not mem2['pending']
  end

  # create
  test ':api creates team and returns team information' do
    image = File.open("#{Rails.root}/test/fixtures/images/architecture.jpg")
    Filestack::Security.any_instance.stubs(:signed_url).returns(image)

    api_v2_sign_in @user

    post :create, team: { name: 'Team title', description: 'Team description', image: image, auto_assign_content: true }, format: :json

    assert_response :ok
    resp = get_json_response(response)

    assert_match 'Team title', resp['name']
    assert_match 'Team description', resp['description']

    assert_equal @org.id, resp['organization_id']
    assert_equal 1, resp['members_count']
    assert_equal assigns(:team).image(:small), resp['image_urls']['small']
    assert_equal assigns(:team).image(:medium), resp['image_urls']['medium']
    assert_equal assigns(:team).image(:large), resp['image_urls']['large']
    assert_equal true, resp['auto_assign_content']
  end

  test 'admins selected on admin panel should be added as team admins and creator should not be added as admin' do
    api_v2_sign_in @admin

    post :create, team: { name: 'Team title', description: 'Team description'},
         is_cms: true, admins_ids: [@member1.id, @user.id], format: :json
    assert_response :ok
    resp = get_json_response(response)
    team = Team.find(resp['id'])
    assert_same_elements team.admins.pluck(:id), [@member1.id, @user.id]
    assert_not_includes team.admins, @admin
  end

  test "test should create a team with name consisting single quote(') " do
    api_v2_sign_in @user

    post :create, team: { name: "Team's title", description: "Team description", auto_assign_content: true }, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_match "Team's title", resp['name']
  end

  test 'team can be created without admins from cms' do
    api_v2_sign_in @admin

    post :create, team: { name: 'Team title', description: 'Team description'},
         is_cms: true, format: :json
    assert_response :ok
    resp = get_json_response(response)
    team = Team.find(resp['id'])
    assert_empty team.admins
  end

  test 'returns error if team is invalid' do
    image = File.open("#{Rails.root}/test/fixtures/images/architecture.jpg")
    Filestack::Security.any_instance.stubs(:signed_url).returns(image)

    api_v2_sign_in @user
    post :create, team: { name: '', description: 'Team description', image: image }, format: :json

    assert_response :unprocessable_entity
    resp = get_json_response(response)
    assert_equal "Name can't be blank", resp['message']
  end

  test 'should create team default open' do
    team_params = {
      name: 'team_test',
      description: 'Team description'
    }
    post :create, team: team_params, format: :json
    assert_response :ok
    resp = get_json_response(response)
    refute resp['is_private']
  end

  test 'should create team with param is_private' do
    team_params = {
      name: 'team_test',
      description: 'Team description',
      is_private: true
    }
    post :create, team: team_params, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert resp['is_private']
  end

  test 'should create team with param is_dynamic' do
    team_params = {
      name: 'team_test',
      description: 'Team description',
      is_dynamic: true
    }
    post :create, team: team_params, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert resp['is_dynamic']
  end

  test 'should create team with param is_mandatory' do
    team_params = {
      name: 'team_test',
      description: 'Team description',
      is_private: true,
      is_mandatory: true
    }
    post :create, team: team_params, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert resp['is_mandatory']
  end


  test 'returns error if try to create mandatory team that is not private' do
    team_params = {
      name: 'team_test',
      description: 'Team description',
      is_mandatory: true
    }
    post :create, team: team_params, format: :json
    assert_response :unprocessable_entity

    resp = get_json_response(response)
    assert_equal 'Only private and dynamic team can be mandatory', resp['message']
  end

  test 'returns unauthorized if user without create_group permision tries to create group' do
    org = create(:organization)
    org.update(enable_role_based_authorization: true)

    user = create(:user, organization: org)

    role = create(:role, name: 'member', organization: org)

    create(:user_role, user_id: user.id, role_id: role.id)

    team_params = {
      name: 'team_test',
      description: 'Team description',
      is_private: true,
      is_mandatory: true
    }
    api_v2_sign_in user

    post :create, team: team_params, format: :json
    assert_response :unauthorized
  end

  test 'destroys team successfully' do
    delete :destroy, id: @team1.id, format: :json
    assert_response :no_content
  end

  test 'revokes destroy access for non-admins' do
    api_v2_sign_in @member4

    delete :destroy, id: @team1.id, format: :json
    assert_response :unauthorized
  end

  test "should be able to delete everyone team" do
    everyone_team = create(:team, organization: @org, is_everyone_team: true)
    everyone_team.add_admin @admin

    delete :destroy, id: everyone_team.id, format: :json
    assert_response :no_content
  end

  # update
  test ':api should be able to update team #org_admin' do
    edited_name = "Engineering and Technology"
    edited_description = "New trends in technology"
    put :update, id: @team1.id, team: {name: edited_name, description: edited_description}, format: :json

    resp = get_json_response(response)

    assert_response :ok
    assert_equal edited_name, resp['name']
    assert_equal edited_description, resp['description']
  end

  test 'should be able to update team #team_admin' do
    user = create(:user, organization: @org)
    @team1.add_admin user

    api_v2_sign_in user

    edited_name = "Engineering and Technology"
    edited_description = "New trends in technology"

    put :update, id: @team1.id, team: {name: edited_name, description: edited_description, auto_assign_content: false}, format: :json

    resp = get_json_response(response)

    assert_response :ok
    assert_equal edited_name, resp['name']
    assert_equal edited_description, resp['description']
    assert_equal false , resp['auto_assign_content']
  end

  test 'should be able to add team admins from admin panel' do
    api_v2_sign_in @admin
    assert_same_elements @team1.admins.pluck(:id), [@member1.id, @admin.id]
    put :update, id: @team1.id, is_cms: true, admins_ids: [@member3.id, @member4.id],
        team: {name: 'Some beautiful name', description: 'Description'}, format: :json
    assert_response :ok

    resp = get_json_response(response)
    assert_same_elements @team1.admins.pluck(:id), [@member1.id, @admin.id, @member3.id, @member4.id]
  end

  test 'should not be able to add team admins from admin panel if current uer is not admin' do
    api_v2_sign_in @member2
    assert_same_elements @team1.admins.pluck(:id), [@member1.id, @admin.id]
    put :update, id: @team1.id, is_cms: true, admins_ids: [@member3.id, @member4.id],
        team: {name: 'Some beautiful name', description: 'Description'}, format: :json
    assert_response :unauthorized
  end

  test 'should not be able to udate team #team_members' do
    api_v2_sign_in @member2

    edited_name = "Engineering and Technology"
    edited_description = "New trends in technology"

    put :update, id: @team1.id, team: {name: edited_name, description: edited_description}, format: :json
    assert_response :unauthorized

    @team1.reload
    assert_not_equal edited_name, @team1.name
    assert_not_equal edited_description, @team1.description
  end

  test 'should update team if sub_admin' do
    @team2.add_sub_admin(@member2)
    role = @org.roles.find_by(name: 'admin')
    create(:role_permission, role: role, name: Permissions::MANAGE_CONTENT)
    role.add_user(@member2)
    api_v2_sign_in @member2

    edited_name = "Engineering and Technology"
    edited_description = "New trends in technology"

    put :update, id: @team1.id,
        team: {name: edited_name, description: edited_description},
        format: :json
    assert_response :ok
    resp = get_json_response(response)

    assert_equal edited_name, resp['name']
    assert_equal edited_description, resp['description']
  end

  test ':api should be able to update is_mandatory for team' do
    put :update, id: @team1.id, team: {is_mandatory: true}, format: :json
    resp = get_json_response(response)
    assert_response :ok
    assert resp['is_mandatory']
  end

  test ':api should be able to update only_admin_can_post for team' do
    put :update, id: @team1.id, team: {only_admin_can_post: true}, format: :json
    resp = get_json_response(response)
    assert_response :ok
    assert_equal true , resp['only_admin_can_post']
  end

  test ':api should update only description and image for dynamic group' do
    image = File.open("#{Rails.root}/test/fixtures/images/architecture.jpg")
    Filestack::Security.any_instance.stubs(:signed_url).returns(image)

    team = create(:team, organization: @org, is_dynamic: true)
    team.add_admin(@admin)
    param = {
       name: 'New-title',
       description: 'New-description',
       image: image,
       is_dynamic: false
    }
    put :update, id: team.id, team: param, format: :json
    resp = get_json_response(response)
    assert_response :ok
    team.reload

    assert_equal team.image(:small), resp['image_urls']['small']
    assert_equal team.image(:medium), resp['image_urls']['medium']
    assert_equal team.image(:large), resp['image_urls']['large']
    assert_not_equal 'New-title', team.name
    assert_equal 'New-description', team.description
    assert team.is_dynamic
  end

  # change_user_role
  test ':api should toggle user role' do
    user = create :user, organization: @org
    @team1.add_member user

    api_v2_sign_in @admin

    put :change_user_role, id: @team1['id'], user_id: user['id'], format: :json
    resp = get_json_response(response)
    assert_response :ok
    assert_equal resp['user']['role'], 'admin'
  end

  test 'team`s admin should be able to toggle user role in team' do
    user = create :user, organization: @org
    @team1.add_admin user

    api_v2_sign_in @member1

    put :change_user_role, id: @team1['id'], user_id: user['id'], format: :json
    resp = get_json_response(response)
    assert_response :ok
    assert_equal resp['user']['role'], 'member'
  end

  test 'member should not be able to toggle other user role in team' do
    user = create :user, organization: @org
    @team1.add_member user

    api_v2_sign_in @member2

    put :change_user_role, id: @team1['id'], user_id: user['id'], format: :json
    assert_response :unauthorized
  end

  test ':api show team details only to users who are a part of the team' do
    user = create :user, organization: @org
    @team1.add_member user

    api_v2_sign_in user
    get :show, id: @team1['id'], format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal @team1.id, resp['id']
    assert_equal @team1.name, resp['name']
  end

  test 'show should not fetch owners who are suspended' do
    org = create(:organization)
    user_list = create_list(:user, 2, organization: org)
    team = create(:team, organization: org)
    team.add_admin(user_list[0])
    team.add_admin(user_list[1])
    assert_equal user_list.map(&:id), team.admins.not_suspended.map(&:id)

    user_list[1].suspend!

    api_v2_sign_in user_list[0]
    get :show, id: team.id, format: :json
    resp = get_json_response(response)
    assert_not_includes resp['owners'].map{|o| o['id']}, user_list[1].id
    assert_includes resp['owners'].map{|o| o['id']}, user_list[0].id
  end

  test 'show should sub_admin and is_sub_admin' do
    org = create(:organization)
    user_list = create_list(:user, 2, organization: org)
    team = create(:team, organization: org)
    team.add_admin(user_list[0])
    team.add_sub_admin(user_list[1])

    api_v2_sign_in user_list[1]
    get :show, id: team.id, format: :json

    resp = get_json_response(response)
    assert_includes resp.keys, 'is_team_sub_admin'
    assert_includes resp.keys, 'sub_admins'
    assert_includes resp['owners'].map{|o| o['id']}, user_list[0].id
    assert_includes resp['sub_admins'].map{|o| o['id']}, user_list[1].id
  end

  test 'should return team if open' do
    org = create(:organization)
    user = create(:user, organization: org)
    team = create(:team, organization: org)
    api_v2_sign_in user

    get :show, id: team.id, format: :json
    assert_response :ok
    resp = get_json_response(response)

    assert_equal team.id, resp['id']
  end

  test 'should have only_admin_can_post field in response and return team if open' do
    org = create(:organization)
    user = create(:user, organization: org)
    team = create(:team, organization: org)
    api_v2_sign_in user

    get :show, id: team.id, format: :json
    assert_response :ok
    resp = get_json_response(response)

    assert_equal team.id, resp['id']
    assert_equal team.only_admin_can_post, resp['only_admin_can_post']
  end

  test ':api show channels followed by the group' do
    user_list = create_list(:user, 3, organization: @org)

    @team1.add_member user_list[0]
    @team1.add_member user_list[1]
    @team1.add_member user_list[2]

    channel_list = create_list(:channel, 2, organization: @org)
    channel = create(:channel, organization: @org)

    @team1.users.each do |user|
      create(:follow, followable: channel_list[0], user: user)
      create(:follow, followable: channel_list[1], user: user)
    end
    create(:teams_channels_follow, team: @team1, channel: channel_list[0])
    create(:teams_channels_follow, team: @team1, channel: channel_list[1])
    api_v2_sign_in user_list[0]

    get :channels, id: @team1['id'], format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal channel_list.map{|d| d.id}, resp['channels'].map{|d| d['id']}
    assert_not_includes resp['channels'].map{|d| d['id']}, channel.id
  end

  test ':api should return channels that can be added to this team exept already added' do
    user = create(:user, organization: @org)
    @team1.add_member user

    channel_list = create_list(:channel, 2, organization: @org)
    channel = create(:channel, organization: @org)

    create(:teams_channels_follow, team: @team1, channel: channel_list[0])
    create(:teams_channels_follow, team: @team1, channel: channel_list[1])
    api_v2_sign_in user

    get :addable_channels, id: @team1['id'], format: :json
    assert_response :ok
    resp = get_json_response(response)

    assert_equal channel.id, resp['channels'].first['id']
    assert_not_includes resp['channels'].map{|d| d['id']}, channel_list.map(&:id)
  end

  test ':api should return channels that matches with query with limit and offset' do
    user = create(:user, organization: @org)
    @team1.add_member user

    channel1 = create(:channel, :architecture, organization: @org)
    channel2 = create(:channel, label: 'Modern architecture', organization: @org)
    channel3 = create(:channel, label: 'Classical architecture', organization: @org)

    api_v2_sign_in user

    get :addable_channels, id: @team1['id'], q: 'architecture', limit: 2, offset: 1, format: :json

    assert_response :ok
    resp = get_json_response(response)

    assert_equal [channel2.id, channel3.id], resp['channels'].map{|d| d['id']}
    assert_not_includes resp['channels'].map{|d| d['id']}, channel1.id
  end

  test ':api show assignment cards of a team with pagination' do
    cards = create_list(:card, 15, organization: @org, author: @admin)
    cards.each do |card|
      assignment = create(:assignment, assignable: card, user_id: @admin.id)
      team_assignment = create(:team_assignment, assignment: assignment,team_id: @team1.id, assignor: @admin)
    end
    api_v2_sign_in @member1
    get :cards, id: @team1['id'], type: 'assigned', limit: 3, offset: 1, format: :json

    resp = get_json_response(response)

    assert_equal 3,resp['cards'].count
    assert_equal resp['team'], @team1.id
  end

  test ':api show shared cards of a team with pagination' do
    user, user1 = create_list(:user, 2, organization: @org)
    @team1.add_member user
    @team1.add_member user1

    card, card1, card2 = create_list(:card, 3, author: @user)

    shared_card = create(:shared_card, team: @team1, card: card)
    shared_card1 = create(:shared_card, team: @team1, card: card1)
    shared_card2 = create(:shared_card, team: @team1, card: card2)

    api_v2_sign_in user

    get :cards, id: @team1.id, type: 'shared', limit: 2, format: :json

    resp = get_json_response(response)
    assert_equal @team1.id, resp['team']
    assert_equal 2, resp['cards'].count
    assert_not_includes resp['cards'].map{|d| d['id']}, card2.id
  end

  test 'get team cards with language as user language or not specified' do
    @user.create_profile(language: 'ru')
    @team1.add_member @user
    card1 = create(:card, organization_id: @org.id, author: @user, language: 'ru')
    card2 = create(:card, organization_id: @org.id, author: @user, language: 'en')
    card3 = create(:card, organization_id: @org.id, author: @user)

    [card1, card2, card3].each { |card| create(:shared_card, team: @team1, card: card)}

    api_v2_sign_in @user

    get :cards, id: @team1.id, filter_by_language: true, type: 'shared', format: :json
    assert_response :ok
    json_response = get_json_response response

    assert_equal 2, json_response['total']
    assert_same_elements [card1, card3].map(&:id), json_response['cards'].map{|c| c['id'].to_i}
  end

  test 'show shared cards even if the same card is shared and assigned' do
    user = create(:user, organization: @org)
    @team2.add_member user

    api_v2_sign_in user

    get :cards, id: @team2, type: 'shared', format: :json
    resp = get_json_response(response)
    assert_includes resp['cards'].map{|d| d['id'].to_i}, @card1.id
  end

  test 'show assigned cards even if the same card is shared and assigned' do
    user = create(:user, organization: @org)
    @team2.add_member user

    api_v2_sign_in user

    get :cards, id: @team2, type: 'assigned', format: :json
    resp = get_json_response(response)
    assert_includes resp['cards'].map{|d| d['id'].to_i}, @card1.id
  end

  test 'should not fetch draft pathways for shared cards' do
    user = create(:user, organization: @org)
    draft_pathway = create(:card, card_type: 'pack', organization: @org, state: 'draft', author: @user)
    create(:shared_card, team_id: @team2.id, card_id: draft_pathway.id)

    @team2.add_member user

    api_v2_sign_in user
    get :cards, id: @team2, type: 'shared', format: :json
    resp = get_json_response(response)
    assert_not_includes resp['cards'].map{|d| d['id'].to_i}, draft_pathway.id
  end

  test 'should not fetch draft pathways for assigned cards' do
    user = create(:user, organization: @org)
    draft_pathway = create(:card, card_type: 'pack', organization: @org, state: 'draft', author: @user)

    assignment = create(:assignment, assignee: @user, assignable: draft_pathway, user_id: user.id)
    create(:team_assignment, team_id: @team2.id, assignment_id: assignment.id)
    create(:shared_card, team_id: @team2.id, card_id: draft_pathway.id)

    @team2.add_member user

    api_v2_sign_in user
    get :cards, id: @team2, type: 'assigned', format: :json
    resp = get_json_response(response)
    assert_not_includes resp['cards'].map{|d| d['id'].to_i}, draft_pathway.id
  end

  test 'should not fetch cards user does not have access to even if assigned' do
    user = create(:user, organization: @org)
    card = create(:card, is_public: 1, author: @user, card_type: 'media', message: 'Echo Echo')
    assignment = create(:assignment, assignee: @user, assignable: card, user_id: user.id)
    create(:team_assignment, team_id: @team2.id, assignment_id: assignment.id)
    card.update_attributes(is_public: 0)

    api_v2_sign_in user
    get :cards, id: @team2, type: 'assigned', format: :json
    resp = get_json_response(response)
    assert_not_includes resp['cards'].map{|d| d['id'].to_i}, card.id
  end

  test ':api to allow teams owner to remove shared card from team' do
    user, user1 = create_list(:user, 2, organization: @org)
    @team1.add_admin user
    @team1.add_member user1

    card, card1 = create_list(:card, 2, author: @user)
    shared_card = create(:shared_card, team: @team1, card: card)
    shared_card1 = create(:shared_card, team: @team1, card: card1)

    api_v2_sign_in user

    delete :remove_shared_card, id: @team1.id, card_id: card.id, format: :json
    assert_response :ok
    assert_equal 1, @team1.shared_cards.count
    assert_not_includes @team1.shared_cards.pluck(:id), card.id
  end

  class TeamTopContentTests < ActionController::TestCase
    setup do
      org = create(:organization)
      @team = create(:team, organization: org)

      @users = create_list(:user, 2, organization: org)

      @team.add_admin(@users[0])
      @team.add_member(@users[1])

      @cards = create_list(:card, 5, author_id: @users[1].id)

      @cards.first(4).each do |card|
        create(:shared_card, team_id: @team.id, card_id: card.id)
      end

      assignment  = create(:assignment, assignable: @cards[4], assignee: @users.first)
      create(:team_assignment, team_id: @team.id, assignment_id: assignment.id)

      create(:content_level_metric, content_id: @cards[0].id,  views_count: 2)
      create(:content_level_metric, content_id: @cards[1].id,  views_count: 4)
      create(:content_level_metric, content_id: @cards[2].id,  views_count: 9)
      create(:content_level_metric, content_id: @cards[3].id,  views_count: 1)
      create(:content_level_metric, content_id: @cards[4].id,  views_count: 20)

      #expected order will be 4, 2, 1, 0, 3
    end

    test 'only admin of team should be able to access the api' do
      # should not be accessible to members of team
      api_v2_sign_in @users[1]
      get :top_content, id: @team.id, format: :json
      assert_response :unauthorized

      # should be accessible to admin of team
      api_v2_sign_in @users[0]
      get :top_content, id: @team.id, format: :json
      assert_response :success
    end

    test ':api to fetch top content of the team' do
      api_v2_sign_in @users[0]

      get :top_content, id: @team.id, format: :json
      assert_response :success
      resp = get_json_response(response)

      # cards in expected order: desc order of views count -> 20, 9, 4, 2, 1
      assert_equal [@cards[4].id, @cards[2].id, @cards[1].id, @cards[0].id, @cards[3].id], resp['top_content'].map{|d| d['id']}
      assert_equal ["id", "score", "title"], resp['top_content'].first.keys.sort
    end

    test 'to fetch as per limit/offset passed' do
      api_v2_sign_in @users[0]

      get :top_content, id: @team.id, limit: 2, offset: 1, format: :json
      assert_response :success
      resp = get_json_response(response)

      # should fetch only top 2 content after 1st
      assert_equal 2, resp['top_content'].count

      # cards in expected order: desc order of views count -> 9, 4
      assert_equal [@cards[2].id, @cards[1].id], resp['top_content'].map{|d| d['id']}
      assert_equal [9, 4], resp['top_content'].map{|d| d['score']}
    end
  end

  class AddToTeamTests < ActionController::TestCase
    setup do
      TeamAddUsersJob.unstub(:perform_later)
      EdcastActiveJob.unstub(:perform_later)
      org = create(:organization)
      @team_admin = create(:user, organization: org)
      @team = create(:team, organization: org)
      @team.add_admin(@team_admin)

      api_v2_sign_in @team_admin
    end

    test ':api admin of team to be able to add user to team as member without invite' do
      user = create(:user, organization: @team_admin.organization)
      post :add_users, id: @team.id, user_ids: [user], role: 'member'
      assert @team.is_member?(user)
    end

    test 'admin of team to be able to add user to team as admin without invite' do
      user = create(:user, organization: @team_admin.organization)
      assert_no_difference -> {Invitation.count} do
        post :add_users, id: @team.id, user_ids: [user], role: 'admin'
      end
      assert @team.is_admin?(user)
    end

    test 'users will be able to added for specific role types only' do
      user = create(:user, organization: @team_admin.organization)
      assert_no_difference -> {TeamsUser.count} do
        post :add_users, id: @team.id, user_ids: [user], role: 'random_text'
      end
      assert_response :unprocessable_entity
      assert_equal "Invalid role for team user", get_json_response(response)['message']
    end

    test 'only admin user should be able to add users to a team' do
      users = create_list(:user, 2, organization: @team_admin.organization)
      @team.add_member(users[0])
      api_v2_sign_in users[0]
      post :add_users, id: @team.id, user_ids: [users[1]], role: 'member'
      assert_response :unauthorized
    end

    test ':api admin of team to be able to add user to team as sub_admin without invite' do
      user = create(:user, organization: @team_admin.organization)
      post :add_users, id: @team.id, user_ids: [user], role: 'sub_admin'
      assert @team.is_sub_admin?(user)
    end
  end

  class TeamAnalyticsTests < ActionController::TestCase
    setup do
      @org = create(:organization)
      @team = create(:team, organization_id: @org.id)
    end

    test 'only team admin should be able to access the analytics api not members' do
      member = create(:user, organization_id: @org.id)
      @team.add_member(member)
      api_v2_sign_in member

      get :analytics, id: @team.id, from_date: '02/20/2017', to_date: '02/20/2018', format: :json
      assert_response :unauthorized
    end

    test 'returns error status if from_date or to_date not given' do
      admin = create(:user, organization_id: @org.id)
      @team.add_admin(admin)
      api_v2_sign_in admin
      [
        {},
        {from_date: '02/20/2017'},
        {to_date: '02/20/2018'},
      ].each do |param_set|
        get :analytics, { id: @team.id, format: :json }.merge(param_set)
        resp = get_json_response(response)
        assert_response :unprocessable_entity
        assert_equal "`from_date or `to date` is missing", resp['message']
      end
    end

    test ':api should get team specific analytics' do
      admin = create(:user, organization_id: @org.id)
      @team.add_admin(admin)
      api_v2_sign_in admin

      Team.any_instance.expects(:metrics_over_span)
        .returns({:smartbites_created=>0, :smartbites_consumed=>0, :total_users=>0, :new_users=>0, :average_session=>0, :active_users=>0, :engagement_index=>0})
      Team.any_instance.expects(:drill_down_data_for_custom_period)
        .returns({:smartbites_created=>[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          :smartbites_consumed=>[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          :average_session=>[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          :active_users=>[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          :total_users=>[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          :engagement_index=>[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          :new_users=>[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]})
      get :analytics, id: @team.id, from_date: '02/20/2017', to_date: '02/20/2018', format: :json
      resp = get_json_response(response)
      assert_same_elements ["smartbites_consumed", "smartbites_created", "average_session", "total_users", "new_users", "active_users", "engagement_index", "clc_progress"], resp.keys
      assert_same_elements ["value", "drilldown"], resp.values.first.keys
    end
  end

  class TeamMetricsTests < ActionController::TestCase
    setup do
      @org1, @org2 = create_list(:organization, 2)
      @org1_teams = create_list(:team, 5, organization: @org1)
      @org2_teams = create_list(:team, 2, organization: @org2)
      @admin = create(:user, organization: @org1, organization_role: 'admin')
      api_v2_sign_in @admin
    end

    teardown do
    end

    test ':api should get team analytics for org' do
      Timecop.freeze do
        User.any_instance.unstub :generate_user_onboarding
        now = Time.now.utc
        applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

        (@org1_teams | @org2_teams).each_with_index do |team, indx|
          create(:team_level_metric, team: team, smartbites_created: indx, smartbites_consumed: indx+1, time_spent: indx*5, smartbites_comments_count: indx*13, smartbites_liked: indx*17, period: :day, offset: applicable_offsets[:day])
        end

        team_members = create_list(:user, 2, :with_onboarding_complete, organization: @org1)
        team_members.each do |u|
          @org1_teams.first.add_member u
        end

        from_date = (Date.today - 2.days).strftime("%m/%d/%Y")
        to_date = Date.today.strftime("%m/%d/%Y")

        get :metrics, from_date: from_date, to_date: to_date, format: :json

        assert_response :ok
        resp = get_json_response response
        teams_response = resp['teams']

        assert_equal @org1_teams.map(&:id).reverse, teams_response.map{|r| r['team']['id']}
        assert_equal (0..4).to_a.reverse, teams_response.map{|r| r["smartbites_created"]}
        assert_equal (0..4).map{|i| i+1}.reverse, teams_response.map{|r| r["smartbites_consumed"]}
        assert_equal (0..4).map{|i| i*5}.reverse, teams_response.map{|r| r["time_spent"]}
        assert_equal (0..4).map{|i| i*13}.reverse, teams_response.map{|r| r["smartbites_comments_count"]}
        assert_equal (0..4).map{|i| i*17}.reverse, teams_response.map{|r| r["smartbites_liked"]}
        assert_equal team_members.count, teams_response.last['team']['members_count']
      end
    end

    test 'sort, search and order param' do
      Timecop.freeze do
        now = Time.now.utc
        applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

        @org1_teams.each_with_index do |team, indx|
          create(:team_level_metric, team: team, smartbites_created: indx, smartbites_consumed: (10-indx), time_spent: indx*5, smartbites_comments_count: indx*13, smartbites_liked: indx*17, period: :day, offset: applicable_offsets[:day])
        end

        from_date = (Date.today - 2.days).strftime("%m/%d/%Y")
        to_date = Date.today.strftime("%m/%d/%Y")

        get :metrics, order_attr: "smartbites_created", order: "asc", from_date: from_date, to_date: to_date, format: :json

        assert_response :ok
        resp = get_json_response(response)
        teams_response = resp['teams']

        assert_equal @org1_teams.map(&:id), teams_response.map{|r| r['team']['id']}

        uniq_name_team = create(:team, organization: @org1, name: "uniq name")
        create(:team_level_metric, team: uniq_name_team, smartbites_created: 9, smartbites_consumed: 52, time_spent: 5, smartbites_comments_count: 13, smartbites_liked: 17, period: :day, offset: applicable_offsets[:day])

        get :metrics, search_term: "uniq", from_date: from_date, to_date: to_date, order_attr: "smartbites_liked", order: "asc", format: :json

        assert_response :ok
        resp = get_json_response(response)
        teams_response = resp['teams']
        assert_equal [uniq_name_team.id], teams_response.map{|r| r['team']['id']}
      end
    end

    test 'blank state' do
      Timecop.freeze do
        @admin = create(:user, organization: @org1)
        from_date = (Date.today - 2.days).strftime("%m/%d/%Y")
        to_date = Date.today.strftime("%m/%d/%Y")

        get :metrics, from_date: from_date, to_date: to_date, format: :json

        assert_response :ok
        resp = get_json_response response
        assert_equal [], resp["teams"]
      end
    end

    test 'only admin can have access to metrics' do
      user = create(:user, organization: @org1)
      api_v2_sign_in user

      get :metrics, format: :json

      assert_response :unauthorized
    end

    test 'should return teams metrics for group_sub_admin user' do
      now = Time.now.utc
      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)
      org = create(:organization)
      user = create(:user, organization_id: org.id)
      Role.create_standard_roles org
      role_member = org.roles.find_by(name: 'member')
      create(:role_permission, role: role_member, name: Permissions::MANAGE_GROUP_ANALYTICS)
      role_member.add_user(user)
      teams = create_list(:team, 2, organization: org)
      teams.first.add_user(user, as_type: 'sub_admin')

      teams.each_with_index do |team, i|
        create(:team_level_metric, {
          team: team,
          smartbites_created: i,
          smartbites_consumed: (10-i),
          time_spent: i*5,
          smartbites_comments_count: i*13,
          smartbites_liked: i*17,
          period: :day,
          offset: applicable_offsets[:day]
        })
      end

      api_v2_sign_in user

      from_date = (Date.today - 2.days).strftime("%m/%d/%Y")
      to_date = Date.today.strftime("%m/%d/%Y")

      get :metrics, from_date: from_date, to_date: to_date, format: :json
      assert_response :ok

      resp = get_json_response(response)
      assert_equal [teams.first.id], resp['teams'].map{|r| r['team']['id']}
    end

    test 'verify order_attr and sort' do
      Timecop.freeze do
        now = Time.now.utc
        applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)
        previous_applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now-1.days)
        random_smartbites_created = 5.times.map{ Random.rand(100) } 

        @org1_teams.each_with_index do |team, indx|
          create(:team_level_metric, team: team, smartbites_created: indx, smartbites_consumed: (10-indx), time_spent: indx*5, smartbites_comments_count: indx*13, smartbites_liked: indx*17, period: :day, offset: applicable_offsets[:day])
        end
        @org1_teams.each_with_index do |team, indx|
          create(:team_level_metric, team: team, smartbites_created: indx, smartbites_consumed: random_smartbites_created[indx], time_spent: indx*5, smartbites_comments_count: indx*13, smartbites_liked: indx*17, period: :day, offset: previous_applicable_offsets[:day])
        end

        from_date = (Date.today - 2.days).strftime("%m/%d/%Y")
        to_date = Date.today.strftime("%m/%d/%Y")

        sorted_teams_by_consumed_count = TeamLevelMetric.where(team_id: @org1_teams.map(&:id)).select("sum(smartbites_consumed) as smartbites_consumed_count,team_id").group(:team_id).order("smartbites_consumed_count")
        sorted_team_id = sorted_teams_by_consumed_count.map{ |t| t.team_id}

        get :metrics, order_attr: "smartbites_consumed", order: "asc", from_date: from_date, to_date: to_date, format: :json
        assert_response :ok

        resp = get_json_response(response)
        teams_response = resp['teams']
        assert_equal sorted_team_id, teams_response.map{|r| r['team']['id']}
      end
    end
  end

  test ':api should remove users from team if admin' do
    user, user1 = create_list(:user, 2, organization: @org)
    @team1.add_member user
    @team1.add_member user1

    api_v2_sign_in @admin

    delete :remove_users, id: @team1.id, user_ids: [user.id, user1.id], format: :json
    assert_response :no_content
    refute @team1.users.exists?(id: user.id)
    refute @team1.users.exists?(id: user1.id)
  end

  test 'remove_users should return errors for missing required params' do
    api_v2_sign_in @admin

    delete :remove_users, id: @team1.id, user_ids: ['', nil, []], format: :json
    assert_response :unprocessable_entity
    resp = get_json_response(response)
    assert_equal 'Missing required params', resp['message']
  end

  test ':api remove_users should remove pending users from team' do
    user = create(:user, organization: @org)
    @invite = create(
      :invitation, sender_id: @admin.id, recipient_email: user.email,
      invitable_id: @team1.id, roles: 'member', invitable_type: 'Team'
    )
    api_v2_sign_in @admin

    delete :remove_users, id: @team1.id, user_ids: [user.id], format: :json
    assert_response :no_content
    refute @team1.invitations.exists?(recipient_id: user.id)
  end

  test ':api should remove users from team if leader team' do
    user, user1 = create_list(:user, 2, organization: @org)
    @team1.add_member user
    @team1.add_admin user1

    api_v2_sign_in user1

    delete :remove_users, id: @team1.id, user_ids: [user.id], format: :json
    assert_response :no_content
    refute @team1.users.exists?(id: user.id)
  end

  test ':api should not remove users from team if not leader team or admin' do
    user, user1 = create_list(:user, 2, organization: @org)
    @team1.add_member user
    @team1.add_member user1

    api_v2_sign_in user1

    delete :remove_users, id: @team1.id, user_ids: [user.id], format: :json
    assert_response :unauthorized
  end

  class TeamAcceptInviteTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      @inviter = create(:user, organization: @org)
      @invited_user = create(:user, organization: @org)
      @group = create(:team, organization: @org)
      @invite = create(:invitation, sender_id: @inviter.id,
                      recipient_email: @invited_user.email,
                      invitable_id: @group.id,
                      roles: 'member',
                      invitable_type: 'Team')
      api_v2_sign_in @invited_user
    end

    test ':api should accept join invitation' do
      assert @invite.accepted_at.nil?
      post :accept_invite, id: @group.id, format: :json
      resp = get_json_response(response)
      assert_response :ok

      #assert team users are created with respective roles
      team_user = TeamsUser.last
      assert_equal team_user.user_id, @invited_user.id
      assert_equal team_user.team_id, @group.id
      assert_equal team_user.as_type, @invite.roles
      assert_equal @group.id, resp['id']
    end

    test 'should return 404 if no invite found' do
      @group_without_invite = create(:team, organization: @org)
      post :accept_invite, id: @group_without_invite.id
      resp = get_json_response(response)
      assert_response :not_found
      assert_equal "Invite for team #{@group_without_invite.id} not found", resp['message']
    end

    test 'should not make any changes if team invite already accepted' do
      @invite.update(accepted_at: Time.now)
      post :accept_invite, id: @group.id, format: :json
      resp = get_json_response(response)
      assert_response :not_found
      assert_equal "Invite for team #{@group.id} not found", resp['message']
    end
  end

  class FetchTeamCards < ActionController::TestCase
    setup do
      @team = create(:team)

      @user, @user1, @user2 = create_list(:user, 3, organization_id: @team.organization.id)
      @team.add_member @user1
      @team.add_member @user
    end

    test 'should return card if card is public and if no permissions are mentioned' do
      card = create(:card, author: @user1, is_public: true)
      create(:shared_card, team_id: @team.id, card_id: card.id)
      api_v2_sign_in @user
      get :cards, id: @team.id, type: 'shared', format: :json
      resp = get_json_response(response)

      assert_includes resp['cards'].map{|d| d['id'].to_i}, card.id
    end

    test 'should not show card to users who it is not restricted to' do
      card = create(:card, author: @user2, is_public: false)
      create(:shared_card, team_id: @team.id, card_id: card.id)
      create(:card_user_permission, card_id: card.id, user_id: @user1.id)

      api_v2_sign_in @user
      get :cards, id: @team.id, type: 'shared', format: :json
      resp = get_json_response(response)
      assert_empty resp['cards']
    end

    test 'should show card to only users who have permission for it' do
      card = create(:card, author: @user, is_public: false)
      create(:shared_card, team_id: @team.id, card_id: card.id)
      create(:card_user_permission, card_id: card.id, user_id: @user1.id)

      api_v2_sign_in @user1
      get :cards, id: @team.id, type: 'shared', format: :json
      resp = get_json_response(response)

      assert_includes resp['cards'].map{|d| d['id'].to_i}, card.id
    end

    test 'should return all cards for org/team admin' do
      card = create(:card, author: @user, is_public: false)
      create(:shared_card, team_id: @team.id, card_id: card.id)
      create(:card_user_permission, card_id: card.id, user_id: @user1.id)

      admin = create(:user, organization: @team.organization, organization_role: 'admin')
      api_v2_sign_in admin
      get :cards, id: @team.id, type: 'shared', format: :json
      resp = get_json_response(response)

      assert_includes resp['cards'].map{|d| d['id'].to_i}, card.id
    end
  end

  class TeamFeedActivities < ActionController::TestCase

    setup do
      @org = create(:organization)
      @user, @viewer, @team_member = create_list(:user, 3, organization: @org)
      @team = create(:team, organization: @org)
      @team2 = create(:team, organization: @org)
      @team3 = create(:team, organization: @org)
      @team.add_member @team_member
      @team.add_member @user
      @team2.add_member @team_member
      @team3.add_member @user
      @team3.add_member @team_member
      @card = create(:card, author: @user)
      @team_comment = create(:comment, message: 'team', commentable: @team, user: @user)
      @team3_comment = create(:comment, message: 'team3', commentable: @team3, user: @user)
      @org_comment = create(:comment, message: 'organization', commentable: @user.organization, user: @user)
      @activity_team_cmnt = create(:activity_stream, action: 'comment', streamable: @team_comment, user: @user, organization: @org)
      @activity_team3_cmnt = create(:activity_stream, action: 'comment', streamable: @team3_comment, user: @user, organization: @org)
      api_v2_sign_in @team_member
    end

    test ':api activity_streams should get team comments if no assignment cards' do
      get :activity_streams, id: @team.id, format: :json
      assert_response :ok
      res = get_json_response(response)
      assert_equal 1, res['activity_streams'].count
      assert_equal @team.id, res['activity_streams'][0]['linkable']['id']
    end

    test 'should not get activity feed of users who are not part of team' do
      user = create(:user, organization: @org)
      card = create(:card, author: user)
      activity_stream = create(:activity_stream, action: 'created_card', streamable: card,
        user: user, organization: @org, card_id: card.id)
      create :teams_card, card: card, team: @team, type: 'SharedCard'

      get :activity_streams, id: @team.id, format: :json
      assert_response :ok
      res = get_json_response(response)
      assert_equal 1, res['activity_streams'].count
      assert_not_includes [activity_stream.id], res['activity_streams'].map { |a| a['id'] }
    end

    test ':api activity_streams should get activities related to team card after card assignment' do
      assignment = create(:assignment, user_id: @user.id, assignable: @card)
      create(:team_assignment, team_id: @team.id, assignment_id: assignment.id)
      create(:activity_stream, action: 'created_card', streamable: @card, user: @user, organization: @org, card_id: @card.id)
      get :activity_streams, id: @team.id, format: :json
      assert_response :ok
      res = get_json_response(response)['activity_streams']
      assert_equal 2, res.count
      assert_same_elements [@team.id, @card.id], [res[0]['linkable']['id'], res[1]['linkable']['id']]
    end

    test ':api activity_streams should get only conversation activities' do
      assignment = create(:assignment, user_id: @user.id, assignable: @card)
      create(:team_assignment, team_id: @team.id, assignment_id: assignment.id)
      as = create(:activity_stream, action: 'created_card',
        streamable: @card, user: @user, organization: @org, card_id: @card.id
      )
      get :activity_streams, id: @team.id, only_conversations: true, format: :json
      assert_response :ok
      res = get_json_response(response)['activity_streams']
      assert_equal 1, res.count
      assert_not_includes [as.id], res.map { |a| a['id'] }
    end

    test ':api activity_streams should get only conversation activities for selected team' do
      assignment = create(:assignment, user_id: @user.id, assignable: @card)
      create(:team_assignment, team_id: @team.id, assignment_id: assignment.id)
      create(:team_assignment, team_id: @team3.id, assignment_id: assignment.id)
      as = create(:activity_stream, action: 'created_card',
                  streamable: @card, user: @user, organization: @org, card_id: @card.id
      )
      get :activity_streams, id: @team3.id, only_conversations: true, format: :json
      assert_response :ok
      res = get_json_response(response)['activity_streams']
      assert_equal 1, res.count
      assert_not_includes [as.id], res.map { |a| a['id'] }
    end

    test ':api activity_streams should response with empty array if no team comments and team cards' do
      get :activity_streams, id: @team2.id, format: :json
      assert_response :ok
      res = get_json_response(response)['activity_streams']
      assert_equal [], res
    end

    test ':api checking presence of all common keys in response' do
      keys = %w[created_at action id updated_at user_id organization_id comments_count votes_count
                comments voters snippet linkable streamable_id streamable_type user is_upvoted]

      assignment = create(:assignment, user_id: @user.id, assignable: @card)
      create(:team_assignment, team_id: @team.id, assignment_id: assignment.id)
      create(:activity_stream, action: 'created_card', streamable: @card, user: @user, organization: @org, card_id: @card.id)

      get :activity_streams, id: @team.id, format: :json
      assert_response :ok
      res = get_json_response(response)['activity_streams']

      team_comment_keys = res.detect { |as| %w(team organization).include?(as['linkable']['type']) }.keys
      except_comments_keys = res.detect { |as| %w(team organization).exclude?(as['linkable']['type']) }.keys
      assert_equal 2, res.count
      assert_same_elements keys, except_comments_keys
      assert_same_elements (keys + ['message']), team_comment_keys
    end

    test ':api activity_streams entry should have is_upvoted true value' do
      create(:vote, votable: @activity_team_cmnt, voter: @user)

      api_v2_sign_in @user

      get :activity_streams, id: @team.id, format: :json
      assert_response :ok
      res = get_json_response(response)['activity_streams']
      assert res.first['is_upvoted']
    end

    test ':api activity_streams entry should have is_upvoted false value' do
      create(:vote, votable: @activity_team_cmnt, voter: @user)

      api_v2_sign_in @team_member

      get :activity_streams, id: @team.id, format: :json
      assert_response :ok
      res = get_json_response(response)['activity_streams']

      refute res.first['is_upvoted']
    end

    test 'activity stream list with limit and offset' do
      first, second = create_list(:activity_stream, 2, action: 'comment', streamable: @team_comment, user: @user,
                                  organization: @org)
      get :activity_streams, id: @team.id, limit: 1, offset: 1, format: :json
      
      resp  = get_json_response(response)['activity_streams']
      assert_equal 1, resp.count
      assert_equal first.id, resp.first['id']
    end

    test 'activity stream should not fetch acitivity with cards that the user does not have access to' do
      user = create(:user, organization: @org)
      create(:teams_user, team_id: @team.id, user_id: user.id)
      card = create(:card, author: @user, is_public: false)
      activity_stream = create(:activity_stream, action: 'created_card', streamable: card,
        user: user, organization: @org, card_id: card.id)
      create :teams_card, card: card, team: @team, type: 'SharedCard'
      
      create(:card_user_permission, user_id: user.id, card_id: card.id)
      get :activity_streams, id: @team.id, format: :json
      assert_response :ok
      res = get_json_response(response)
      assert_not_includes res['activity_streams'].map { |a| a['id']}, activity_stream.id

      api_v2_sign_in user
      get :activity_streams, id: @team.id, format: :json
      assert_response :ok
      res = get_json_response(response)
      assert_includes res['activity_streams'].map { |a| a['id']}, activity_stream.id
    end

    test 'activity stream should fetch activity with cards that are just restricted to the user/users teams also' do
      user = create(:user, organization: @org)
      create(:teams_user, team_id: @team.id, user_id: user.id)
      card_list = create_list(:card, 3, author: @user, is_public: false)

      card_list.each do |card|
        assignment = create(:assignment, user_id: user.id, assignable: card)
        create(:team_assignment, team_id: @team.id, assignment_id: assignment.id)
      end
      
      activity_stream1 = create(:activity_stream, action: 'created_card', streamable: card_list[0],
        user: user, organization: @org, card_id: card_list[0].id)
      activity_stream2 = create(:activity_stream, action: 'created_card', streamable: card_list[1],
        user: user, organization: @org, card_id: card_list[1].id)
      activity_stream3 = create(:activity_stream, action: 'created_card', streamable: card_list[2],
        user: user, organization: @org, card_id: card_list[2].id)
      
      create(:card_user_permission, card_id: card_list[0].id, user_id: user.id)
      create(:card_user_permission, card_id: card_list[1].id, user_id: create(:user, organization_id: @org.id).id)
      create(:card_team_permission, card_id: card_list[2].id, team_id: @team.id)

      api_v2_sign_in user
      get :activity_streams, id: @team.id, format: :json
      assert_response :ok
      res = get_json_response(response)

      assert_includes res['activity_streams'].map { |a| a['id']}, activity_stream1.id
      assert_includes res['activity_streams'].map { |a| a['id']}, activity_stream3.id
      assert_not_includes res['activity_streams'].map { |a| a['id']}, activity_stream2.id
    end

    test 'activity stream should fetch all activities for org admin' do
      admin_user = create(:user, organization: @org, organization_role: 'admin')

      member_user = create(:user, organization: @org)
      @team.add_member member_user

      refute @team.is_user? admin_user
      
      card_list = create_list(:card, 2, author: @user, is_public: false)

      card_list.each do |card|
        create(:shared_card, card_id: card.id, team_id: @team.id)
      end

      activity_stream1 = create(:activity_stream, action: 'created_card', streamable: card_list[0],
        user: member_user, organization: @org, card_id: card_list[0].id)
      activity_stream2 = create(:activity_stream, action: 'created_card', streamable: card_list[1],
        user: member_user, organization: @org, card_id: card_list[1].id)

      api_v2_sign_in admin_user
      get :activity_streams, id: @team.id, format: :json
      assert_response :ok
      
      res = get_json_response(response)
      assert_same_elements [@activity_team_cmnt.id, activity_stream1.id, activity_stream2.id],
        res['activity_streams'].map { |a| a['id']}
    end


    test 'activity stream should return field handle in response' do
      user = create(:user, organization: @org)
      card = create(:card, author: user)
      create(
        :activity_stream, action: 'created_card', streamable: card,
        user: user, organization: @org, card_id: card.id
      )
      create(:teams_card, card: card, team: @team, type: 'SharedCard')

      get :activity_streams, id: @team.id, format: :json
      assert_response :ok
      res = get_json_response(response)['activity_streams'].first
      assert_includes res['user'], 'handle'
    end
  end

  test ':api remove_teams_type should return error missing params' do
    api_v2_sign_in @admin

    delete :remove_teams_type, id: @team1.id, user_ids: [], format: :json
    assert_response :unprocessable_entity
    resp = get_json_response(response)

    assert_equal 'Missing required params', resp['message']
  end

  test ':api decline_invite' do
    org = create(:organization)
    inviter = create(:user, organization: org)
    invited_user = create(:user, organization: org)
    team = create(:team, organization: org, is_private: true)
    create(:invitation, sender_id: inviter.id,
                        recipient_email: invited_user.email,
                        invitable_id: team.id,
                        roles: 'member',
                        invitable_type: 'Team')
    api_v2_sign_in invited_user

    post :decline_invite, id: team.id, format: :json
    assert_response :ok
    resp = get_json_response(response)

    assert invited_user.invitations.blank?
    assert_equal team.id, resp['id']
  end

  test ':api join' do
    org = create(:organization)
    user = create(:user, organization: org)
    team = create(:team, organization: org)
    api_v2_sign_in user

    post :join, id: team.id, format: :json
    assert_response :ok
    resp = get_json_response(response)

    assert_includes team.user_ids, user.id
    assert_equal team.id, resp['id']
  end

  test 'join should return error message if user already exist in team' do
    org = create(:organization)
    user = create(:user, organization: org)
    team = create(:team, organization: org)
    team.add_member(user)
    api_v2_sign_in user

    post :join, id: team.id, format: :json
    assert_response :unprocessable_entity
    resp = get_json_response(response)

    assert_equal 'User is already exist in this team', resp['message']
  end

  test 'join should return error message if team private' do
    org = create(:organization)
    user = create(:user, organization: org)
    team = create(:team, organization: org, is_private: true)
    api_v2_sign_in user

    post :join, id: team.id, format: :json
    assert_response :unprocessable_entity
    resp = get_json_response(response)

    assert_equal 'Team is not open', resp['message']
  end

  test ':api leave' do
    org = create(:organization)
    user = create(:user, organization: org)
    team = create(:team, organization: org)
    team.add_member(user)
    api_v2_sign_in user

    delete :leave, id: team.id, format: :json
    assert_response :ok
    resp = get_json_response(response)

    assert_not_includes team.user_ids, user.id
    assert_equal team.id, resp['id']
  end

  test 'leave should return error message if user not exist in team' do
    org = create(:organization)
    user = create(:user, organization: org, organization_role: 'admin')
    team = create(:team, organization: org)
    api_v2_sign_in user

    delete :leave, id: team.id, format: :json
    assert_response :unprocessable_entity
    resp = get_json_response(response)

    assert_equal 'User does not exist in this team', resp['message']
  end

  test 'leave should return error message if team is mandatory' do
    org = create(:organization)
    user = create(:user, organization: org)
    team = create(:team, organization: org, is_mandatory: true, is_private: true)
    team.add_member(user)
    api_v2_sign_in user

    delete :leave, id: team.id, format: :json
    assert_response :unprocessable_entity
    resp = get_json_response(response)
    assert_equal 'You can not leave mandatory team', resp['message']
  end

  test 'leave should remove invitation' do
    org = create(:organization)
    user1 = create(:user, organization: org)
    team = create(:team, organization: org)
    team.invitations.create(
      recipient_id: user1.id,
      recipient_email: user1.email,
      sender_id: 1,
      accepted_at: Time.current
    )
    team.add_member(user1)
    api_v2_sign_in user1
    assert_difference -> { Invitation.count }, -1 do
      delete :leave, id: team.id, format: :json
    end
    assert_response :ok
  end

  class TeamAnnouncementTest < ActionController::TestCase

    setup do
      org = create(:organization)
      @team = create(:team, organization: org, is_everyone_team: false)

      create_list(:user, 2, organization: org).each do |user|
        @team.add_member(user)
      end

      @admin = create(:user, organization: org, organization_role: 'admin')
      @team.add_admin(@admin)
    end

    test 'non admin should not be able to send notifications to the team' do
      user = create(:user, organization: @admin.organization, organization_role: 'member')
      api_v2_sign_in user

      TeamPushNotificationJob.expects(:perform_later).never
      TeamUsersMailerJob.expects(:perform_later).never
      post :announcement, id: @team.id, mobile_notification: true, email_notification: true, notification: {content: 'new content'}
      assert_response :unauthorized
    end

    test 'org admin should be able to send notifications to the team' do
      api_v2_sign_in @admin

      TeamPushNotificationJob.expects(:perform_later).with({content: 'new content', team_id: @team.id})
      TeamUsersMailerJob.expects(:perform_later).with({message: 'new content', team_id: @team.id, sender_id: @admin.id}).once
      post :announcement, id: @team.id, mobile_notification: true, email_notification: true, notification: {content: 'new content'}
      assert_response :success
    end

    test 'everyone team should not be notified' do
      team = create(:team, organization: @team.organization, is_everyone_team: true)
      api_v2_sign_in @admin

      TeamPushNotificationJob.expects(:perform_later).never
      TeamUsersMailerJob.expects(:perform_later).never

      post :announcement, id: team.id, mobile_notification: true, email_notification: true, notification: {content: 'new content'}
      assert_response :unprocessable_entity
      assert "Cannot announce to Everyone team", get_json_response(response)['message']
    end
  end

  class TeamActivityAndVisibility < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user, @viewer, @team_member = create_list(:user, 3, organization: @org)
      @team1, @team2 = create_list(:team, 2, organization: @org)
      @channel1, @channel2 = create_list(:channel, 2, organization: @org)
      @team1.add_member @team_member
      @team1.add_member @user
      @team1.add_member @viewer
      @team2.add_member @team_member
      @card = create(:card, author: @user)
      api_v2_sign_in @team_member
    end

    test 'team member should see public card shared with team in acitivity' do
      card = create(:card, author: @user)

      activity_stream = create(:activity_stream, action: 'created_card', streamable: card,
        user: @user, organization: @org, card_id: card.id)
      create :teams_card, card: card, team: @team1, type: 'SharedCard'

      get :activity_streams, id: @team1.id, format: :json
      
      resp = get_json_response(response)
      assert_equal 1, resp['activity_streams'].count
      assert_same_elements [activity_stream.id], resp['activity_streams'].map { |a| a['id'] }
    end

    test 'team member should not see card made private and inaccessible to user after assigning' do
      card = create(:card, author: @user, is_public: false)
      assignment = create(:assignment, assignable: card, user_id: @team_member.id)
      create(:team_assignment, assignment_id: assignment.id, team_id: @team1.id)

      activity_stream = create(:activity_stream, action: 'created_card', streamable: card,
        user: @user, organization: @org, card_id: card.id)
      card.update_attributes(is_public: false)

      get :activity_streams, id: @team1.id, format: :json
      resp = get_json_response(response)
      assert_equal 0, resp['activity_streams'].count
    end

    test 'team member should not see activity for card shared with team but restricted on different user' do
      card = create(:card, author: @user, is_public: false, organization: @org)

      activity_stream = create(:activity_stream, action: 'created_card', streamable: card,
        user: @user, organization: @org, card_id: card.id)
      create :teams_card, card: card, team: @team1, type: 'SharedCard'
      create :card_user_permission, card: card, user: @viewer

      get :activity_streams, id: @team1.id, format: :json
      resp = get_json_response(response)
      assert_equal 0, resp['activity_streams'].count
    end

    test 'team member should see activity for card shared with team but restricted on different team' do
      card = create(:card, author: @user, is_public: false, organization: @org)

      activity_stream = create(:activity_stream, action: 'created_card', streamable: card,
        user: @user, organization: @org, card_id: card.id)
      create :teams_card, card: card, team: @team1, type: 'SharedCard'
      create :card_team_permission, card: card, team: @team2

      get :activity_streams, id: @team1.id, format: :json
      resp = get_json_response(response)
      assert_equal 1, resp['activity_streams'].count
      assert_same_elements [activity_stream.id], resp['activity_streams'].map { |a| a['id'] }
    end

    test 'should save carousels order information' do
      admin_user = create(:user, organization_role: 'admin', organization: @org)
      team = create :team, organization: @org
      carousels = [{default_label: "default label", index: 2, visible: false}]

      api_v2_sign_in(admin_user)

      put :carousels, id: team.id, carousels: carousels, format: :json
      assert_response :ok
      carousels = get_json_response response
      assert_equal "default label", carousels['carousels'][0]['default_label']
      assert_equal 2, carousels["carousels"][0]['index']
      assert_equal false, carousels["carousels"][0]['visible']
    end

    test ':api should return default carousels if team config is not present' do
      team = create(:team, organization: @org)
      default_carousels = [{"default_label"=>"Featured", "index"=>0, "visible"=>true}, 
                           {"default_label"=>"Assigned", "index"=>1, "visible"=>true}, 
                           {"default_label"=>"Shared", "index"=>2, "visible"=>true}, 
                           {"default_label"=>"Channels", "index"=>3, "visible"=>true}]

      get :show, id: team.id, format: :json
      assert get_json_response(response)['carousels']
      assert_equal default_carousels, get_json_response(response)['carousels']
    end
  end
end

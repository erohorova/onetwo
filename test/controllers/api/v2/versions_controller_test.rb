require 'test_helper'

class Api::V2::VersionsControllerTest < ActionController::TestCase
  setup do
    @user1 = create(:user, first_name: "FName1", last_name: "LName1")
    @user2 = create(:user, first_name: "FName2", last_name: "LName2")
    api_v2_sign_in(@user1)
  end

  test "returns error if entity_type is invalid" do
    get :index, entity_type: "user", format: :json
    resp = JSON.parse(response.body)
    assert_equal resp["message"], "Wrong entity passed"
  end

  test "calls CardHistory to fetch versions" do
    CardHistory.expects(:fetch_versions).with(id: 10)
    get :index, entity_type: "card", entity_id: 10, format: :json
  end

  test "returns all versions of a card" do
    fake_resp = OpenStruct.new(
      results: [
        OpenStruct.new(card_id: 10, user_id: @user1.id, updated_fields: [{title: ["", "Test"], created_at: Time.now - 2.days}]),
        OpenStruct.new(card_id: 10, user_id: @user2.id, updated_fields: [{title: ["Test", "Hello"], created_at: Time.now - 1.days}]),
        OpenStruct.new(card_id: 10, user_id: @user2.id, updated_fields: [{topics: ["", "Tag1, Tag2"], created_at: Time.now}]),
      ]
    )
    CardHistory.stubs(:fetch_versions).returns(fake_resp)

    get :index, entity_type: "card", entity_id: 10, format: :json
    resp = JSON.parse(response.body)
    versions = resp["versions"]

    assert_equal 3, versions.count
    assert_equal "FName1 LName1", versions.first["user"]
    assert_equal ["title", "created_at"], versions.first["updatedFields"][0].keys
    assert_equal ["topics", "created_at"], versions[2]["updatedFields"][0].keys
  end

  test "returns name of the old and new author when author changed" do
    fake_resp = OpenStruct.new(
      results: [
        OpenStruct.new(card_id: 10, user_id: @user1.id, updated_fields: [{"author_id" => [@user1.id, @user2.id], created_at: Time.now - 2.days}])
      ]
    )
    CardHistory.stubs(:fetch_versions).returns(fake_resp)

    get :index, entity_type: "card", entity_id: 10, format: :json
    resp = JSON.parse(response.body)
    versions = resp["versions"]

    assert_equal ["FName1 LName1", "FName2 LName2"], versions.first["updatedFields"][0]["author"]
  end
end

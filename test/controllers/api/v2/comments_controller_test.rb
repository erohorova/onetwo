require 'test_helper'

class Api::V2::CommentsControllerTest < ActionController::TestCase
  class UnreportActionTest < Api::V2::CommentsControllerTest
    setup do
      # org
      @org = create(:organization, enable_role_based_authorization: true)
      Role.create_standard_roles @org
      reporter_role = @org.roles.find_by_name('admin')
      create(:role_permission, role: reporter_role, name: Permissions::REPORT_COMMENT)

      # users
      @user = create(:user, organization: @org, organization_role: 'admin')
      @comment_author = create(:user, organization: @org)
      @user.add_role('admin')

      # card
      @card = create(:card, organization: @org, author: @user)

      # comment on card
      @card_comment= create(:comment, commentable:@card, user:@comment_author)

      # reported comments
      @comment_reporting_1 = create(:comment_reporting, comment: @card_comment, user: @user)
      @comment_reporting_2 = create(:comment_reporting, comment: @card_comment, user: @comment_author)
    end

    test 'should be able to unreport the comment' do
      api_v2_sign_in(@user)
      put :unreport, id: @card_comment.id, format: :json
      resp = get_json_response(response)

      assert_response 200
    end

    test 'should not be able to unreport the comment if admin non authorized to REPORT_COMMENT permission' do
      RolePermission.find_by_name(Permissions::REPORT_COMMENT).destroy
      api_v2_sign_in(@user)
      put :unreport, id: @card_comment.id, format: :json
      assert_response 401
    end

    test 'should return no comment reportings after comment is unreported' do
      api_v2_sign_in(@user)
      resp = get_json_response(response)

      assert_difference ->{CommentReporting.count},-2 do
        put :unreport, id: @card_comment.id, format: :json
      end
      assert_equal 2, @card_comment.reload.comment_reportings.with_deleted.count
    end

    test 'should be able to unreport the comment if RBAC is enabled and REPORT_COMMENT permission authorized' do
      org = create(:organization, enable_role_based_authorization: true)
      Role.create_standard_roles org
      admin_role = org.roles.find_by_name('admin')
      create(:role_permission, role: admin_role, name: Permissions::ADMIN_ONLY)
      create(:role_permission, role: admin_role, name: Permissions::REPORT_COMMENT)

      comment_author = create(:user, organization: org)
      user = create(:user, organization: org)
      user.add_role('admin')
      card = create(:card, organization: org, author: user)
      card_comment = create(:comment, commentable:card, user:comment_author)
      create(:comment_reporting, comment: card_comment, user: user)

      api_v2_sign_in(user)
      put :unreport, id: card_comment.id, format: :json
      get_json_response(response)

      assert_response 200
    end

    test 'should not be able to unreport the comment if RBAC is disabled' do
      org = create(:organization, enable_role_based_authorization: false)
      Role.create_standard_roles org
      admin_role = org.roles.find_by_name('admin')
      create(:role_permission, role: admin_role, name: Permissions::ADMIN_ONLY)

      comment_author = create(:user, organization: org)
      user = create(:user, organization: org)

      card = create(:card, organization: org, author: user)

      card_comment = create(:comment, commentable:card, user:comment_author)

      comment_reporting_1 = create(:comment_reporting, comment: card_comment, user: user)

      api_v2_sign_in(user)
      put :unreport, id: card_comment.id, format: :json
      resp = get_json_response(response)

      assert_response 401
    end

  end

  class IndexActionTest < Api::V2::CommentsControllerTest
    setup do
      @org = create(:organization)
      @user, @comment_author = create_list(:user, 2, organization: @org)
      @card = create(:card, organization: @org, author: @user)
      @activity_stream = create(:activity_stream, streamable: @card, organization: @org, user: @user)
      api_v2_sign_in(@user)
    end

    test ':api index' do
      comments = []
      (1..3).each do |i|
        comments << create(:comment, commentable: @card, created_at: Time.now.utc - i.hours, user: @comment_author)
      end
      get :index, card_id: @card.id, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal comments.reverse.map(&:id), (resp['comments'].map { |c| c['id'] })
    end

    test ':api index should use order params' do
      comments = []
      (1..3).each do |i|
        comments << create(:comment, commentable: @card, created_at: Time.now.utc + i.hours, user: @comment_author)
      end
      get :index, card_id: @card.id, order: 'desc', format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal comments.last.id, resp['comments'].first['id']
    end

    test 'should return true for if comment is reported by current user' do
      comment = create(:comment, commentable: @card, created_at: Time.now.utc, user: @comment_author)
      create(:comment_reporting, comment: comment, user: @user)
      get :index, card_id: @card.id, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal [true], resp['comments'].map { |c| c['is_reported'] }
    end

    test 'should return false for if comment is not reported by current user' do
      comment = create(:comment, commentable: @card, created_at: Time.now.utc, user: @comment_author)
      get :index, card_id: @card.id, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal [false], resp['comments'].map { |c| c['is_reported'] }
    end

    test 'should get comments for activity' do
      comments = []
      (1..3).each do |i|
        comments << create(:comment, commentable: @activity_stream, created_at: Time.now.utc - i.hours, user: @comment_author)
      end
      get :index, activity_stream_id: @activity_stream.id, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal comments.reverse.map(&:id), (resp['comments'].map { |c| c['id'] })
    end

    test 'getting comments for activity should support order params' do
      comments = []
      (1..3).each do |i|
        comments << create(:comment, commentable: @activity_stream, created_at: Time.now.utc + i.hours, user: @comment_author)
      end
      get :index, activity_stream_id: @activity_stream.id, order: 'desc', format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal comments.last.id, resp['comments'].first['id']
    end

    test 'getting comments for activity should send upvote data' do
      comments = []
      (1..2).each do |i|
        comments << create(:comment, commentable: @activity_stream, created_at: Time.now.utc - i.hours, user: @comment_author)
      end

      create(:vote, votable: comments.last, voter: @user)
      get :index, activity_stream_id: @activity_stream.id, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal comments.reverse.map(&:id), (resp['comments'].map { |c| c['id'] })
      assert_equal [true, false], (resp['comments'].map { |c| c['is_upvoted'] })
    end

    test 'should set max limit for pagination' do
      comments = []
      (1..55).each do |i|
        comments << create(:comment, commentable: @activity_stream, created_at: Time.now.utc - i.hours, user: @comment_author)
      end

      get :index, activity_stream_id: @activity_stream.id, limit: 60, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal 50, resp['comments'].count
    end

    test 'should send upvote data' do
      comments = []
      (1..2).each do |i|
        comments << create(:comment, commentable: @card, created_at: Time.now.utc - i.hours, user: @comment_author)
      end

      create(:vote, votable: comments.last, voter: @user)
      get :index, card_id: @card.id, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal comments.reverse.map(&:id), (resp['comments'].map { |c| c['id'] })
      assert_equal [true, false], (resp['comments'].map { |c| c['is_upvoted'] })
    end

    test 'should get voters who liked comments' do
      comments = []
      (1..2).each do |i|
        comments << create(:comment, commentable: @card, created_at: Time.now.utc - i.hours, user: @comment_author)
      end
      voter = create(:vote, votable: comments.last, voter: @user)
      get :index, card_id: @card.id, format: :json, load_voters: "true"
      assert_response :ok
      resp = get_json_response(response)
      assert_equal voter.user_id, resp['comments'][0]['voters'][0]['id']
    end

    test 'should get voters who recently liked comments in desc order' do
      @another_user= create(:user, organization: @org)
      comments = []
      (1..2).each do |i|
        comments << create(:comment, commentable: @card, created_at: Time.now.utc - i.hours, user: @comment_author)
      end
      voter1 = create(:vote, votable: comments.last, voter: @user)
      voter2 = create(:vote, votable: comments.last, voter: @another_user)
      get :index, card_id: @card.id, format: :json, load_voters: "true"
      assert_response :ok
      resp = get_json_response(response)
      assert_equal [voter2.user_id,voter1.user_id], resp['comments'][0]['voters'].map {|voter| voter['id']}
    end
  end

  class ShowActionTest < Api::V2::CommentsControllerTest
    setup do
      @org = create(:organization)
      @user, @comment_author = create_list(:user, 2, organization: @org)
      @card = create(:card, organization: @org, author: @user)
      @activity_stream = create(:activity_stream, streamable: @card, organization: @org, user: @user)
      api_v2_sign_in(@user)
    end

    test ':api show' do
      comment = create(:comment, commentable: @card, user: @comment_author)
      get :show, card_id: @card.id, id: comment.id, format: :json
      resp = get_json_response(response)
      assert_equal comment.id, resp['id']
    end

    test 'should get comment for activity by ids' do
      comment = create(:comment, commentable: @activity_stream, user: @comment_author)
      api_v2_sign_in(@comment_author)
      get :show, activity_stream_id: @activity_stream.id, id: comment.id, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal comment.id, resp['id']
    end

  end

  class CreateActionTest < Api::V2::CommentsControllerTest
    setup do
      @org = create(:organization)
      @user, @comment_author = create_list(:user, 2, organization: @org)
      @card = create(:card, organization: @org, author: @user)
      @activity_stream = create(:activity_stream, streamable: @card, organization: @org, user: @user)
      api_v2_sign_in(@user)
    end

    test ':api create' do
      TestAfterCommit.enabled = true
      resp = nil
      message = 'this is a comment'
      pic = Rails.root.join('test', 'fixtures', 'images', 'logo.png')
      fr1 = FileResource.create(attachment: pic.open)
      fr2 = FileResource.create(attachment: pic.open)
      file_ids = [fr1.id, fr2.id]

      EDCAST_NOTIFY.notification_name_to_notifiers[Notify::NOTE_NEW_SMARTBITE_COMMENT].set_enabled(:email, :single)

      SendSingleNotificationJob.expects(:perform_later).never #new_smart_bite_comment + new_activity_on_commented_smartbite

      api_v2_sign_in(@comment_author)
      assert_differences [[-> { @card.comments.count }, 1], [-> { NotificationEntry.count }, 1]] do
        post :create, card_id: @card.id, comment: {message: message, file_ids: file_ids}, format: :json
        assert_response :ok, response.body
        resp = get_json_response(response)
      end

      comment = @card.comments.last
      assert_equal comment.id, resp['id']
      assert_equal comment.commentable_id, resp['commentable_id']
      assert_equal comment.commentable_type, resp['commentable_type']
      assert_equal @comment_author.id, comment.user_id
      assert_equal message, comment.message
      assert_same_elements file_ids, comment.file_resources.map(&:id)
    end

    test ':api create with #string of file_ids' do
      resp = nil
      message = 'this is a comment'
      pic = Rails.root.join('test', 'fixtures', 'images', 'logo.png')
      fr1 = FileResource.create(attachment: pic.open)
      fr2 = FileResource.create(attachment: pic.open)
      file_ids = [fr1.id, fr2.id]

      api_v2_sign_in(@comment_author)
      assert_difference -> { @card.comments.count } do
        post :create, card_id: @card.id, comment: {message: message, file_ids: file_ids.map(&:to_s)}, format: :json
        assert_response :ok, response.body
        resp = get_json_response(response)
      end

      comment = @card.comments.last
      assert_equal comment.id, resp['id']
      assert_equal @comment_author.id, comment.user_id
      assert_equal message, comment.message
      assert_same_elements file_ids, comment.file_resources.map(&:id)
    end

    test 'create validation errors' do
      api_v2_sign_in(@comment_author)
      assert_no_difference -> { @card.comments.count } do
        post :create, card_id: @card.id, comment: {message: nil}, format: :json
        assert_response :unprocessable_entity
        resp = get_json_response(response)
        assert_match /Message can't be blank/, resp['message']
      end
    end

    test ':api create should use order params #curator' do
      @org.update(enable_role_based_authorization: true)
      Role.create_standard_roles(@org)
      @user.role_ids = [@org.roles.where(name: Role::TYPE_CURATOR).first.id]
      @user.save
      post :create, card_id: @card.id, comment: {message: 'Random Comment'}, format: :json
      assert_response :unauthorized
    end

    test ':api create should use order params #member' do
      Role.any_instance.unstub(:create_role_permissions)

      @org.update(enable_role_based_authorization: true)
      Role.create_master_roles(@org)
      @user.role_ids = [@org.roles.where(name: Role::TYPE_MEMBER).first.id]
      @user.save

      assert_difference -> { @card.comments.count } do
        post :create, card_id: @card.id, comment: {message: 'Random Comment'}, format: :json
        assert_response :ok
      end
    end

    test ':api create should use order params #member when create card permissions disabled' do
      Role.any_instance.unstub(:create_role_permissions)
      @org.update(enable_role_based_authorization: true)
      Role.create_master_roles(@org)
      member_role = @org.roles.where(name: Role::TYPE_MEMBER).first
      role_permission = member_role.role_permissions.where(name: Permissions::CREATE_COMMENT).first
      role_permission.enabled = false
      role_permission.save
      post :create, card_id: @card.id, comment: {message: 'Random Comment'}, format: :json
      assert_response :unauthorized
    end

    test 'should create comment for activity' do
      api_v2_sign_in(@comment_author)

      post :create, activity_stream_id: @activity_stream.id, comment: {message: 'message'}, format: :json
      assert_response :ok, response.body
      assert_equal 1, @activity_stream.comments.count

      resp = get_json_response(response)
      comment = @activity_stream.comments.last
      assert_equal comment.id, resp['id']
      assert_equal comment.commentable_id, resp['commentable_id']
      assert_equal comment.commentable_type, resp['commentable_type']
      assert_equal @comment_author.id, comment.user_id
      assert_equal 'message', comment.message
    end

    test 'should create comment for activity with #string of file_ids' do
      resp = nil
      message = 'this is a comment'
      pic = Rack::Test::UploadedFile.new('test/fixtures/images/logo.png', 'image/png')
      fr1, fr2= create_list(:file_resource, 2, attachment: pic)
      file_ids = [fr1.id, fr2.id]
      api_v2_sign_in(@comment_author)
      assert_difference -> { @activity_stream.comments.count } do
        post :create, activity_stream_id: @activity_stream.id, comment: {message: message, file_ids: file_ids.map(&:to_s)}, format: :json
        assert_response :ok, response.body
        resp = get_json_response(response)
      end

      comment = @activity_stream.comments.last
      assert_equal comment.id, resp['id']
      assert_equal @comment_author.id, comment.user_id
      assert_equal message, comment.message
      assert_same_elements file_ids, comment.file_resources.map(&:id)
    end

    test 'should not create comment for activity with validation errors' do
      api_v2_sign_in(@comment_author)
      assert_no_difference -> { @activity_stream.comments.count } do
        post :create, activity_stream_id: @activity_stream.id, comment: {message: nil}, format: :json
        assert_response :unprocessable_entity
        resp = get_json_response(response)
        assert_match /Message can't be blank/, resp['message']
      end
    end

    test ':api create for the team' do
      team = create(:team, organization: @org)
      post :create, team_id: team.id, comment: {message: 'comment'}, format: :json
      assert_response :ok
      resp = get_json_response(response)
      comment = team.comments.last
      assert_equal comment.id, resp['id']
      assert_equal comment.commentable_id, resp['commentable_id']
      assert_equal comment.commentable_type, resp['commentable_type']
      assert_equal @user.id, comment.user_id
      assert_equal 'comment', comment.message
    end

    test ':api create for the organization' do
      org = @user.organization
      resp = nil
      message = 'this is a comment'
      pic = Rails.root.join('test', 'fixtures', 'images', 'logo.png')
      fr1 = FileResource.create(attachment: pic.open)
      fr2 = FileResource.create(attachment: pic.open)
      file_ids = [fr1.id, fr2.id]

      assert_difference -> { org.comments.count }, 1 do
        post :create, organization_id: org.id, comment: {message: message, file_ids: file_ids}, format: :json
        assert_response :ok, response.body
        resp = get_json_response(response)
      end

      comment = org.comments.last
      assert_equal comment.id, resp['id']
      assert_equal comment.commentable_id, resp['commentable_id']
      assert_equal comment.commentable_type, resp['commentable_type']
      assert_equal @user.id, comment.user_id
      assert_equal message, comment.message
      assert_same_elements file_ids, comment.file_resources.map(&:id)
    end

    test ':api create should send notifications if enabled' do
      TestAfterCommit.enabled = true
      message = 'this is a comment'

      notifier1 = EDCAST_NOTIFY.notification_name_to_notifiers[Notify::NOTE_NEW_ACTIVITY_COMMENTED_SMARTBITE]

      notifier = EDCAST_NOTIFY.notification_name_to_notifiers[Notify::NOTE_NEW_SMARTBITE_COMMENT]

      #send push, not email
      notifier.set_disabled(:email, :single)
      notifier.set_enabled(:push, :single)

      notifier1.set_disabled(:email, :single)
      notifier1.set_disabled(:push, :single)

      EDCAST_NOTIFY.expects(:send_email).never
      EDCAST_NOTIFY.expects(:send_push).never
      api_v2_sign_in(@comment_author)
      assert_differences [[-> { @card.comments.count }, 1], [-> { NotificationEntry.count }, 1]] do
        post :create, card_id: @card.id, comment: {message: message}, format: :json
        assert_response :ok, response.body
      end

      #send email not push
      notifier.set_disabled(:push, :single)
      notifier.set_enabled(:email, :single)

      EDCAST_NOTIFY.expects(:send_email).never
      EDCAST_NOTIFY.expects(:send_push).never
      TestAfterCommit.enabled = true
      post :create, card_id: @card.id, comment: {message: message}, format: :json

      #No push no email
      notifier.set_disabled(:push, :single)
      notifier.set_disabled(:email, :single)

      EDCAST_NOTIFY.expects(:send_email).never
      EDCAST_NOTIFY.expects(:send_push).never

      post :create, card_id: @card.id, comment: {message: message}, format: :json
    end
  end

  class DestroyActionTest < Api::V2::CommentsControllerTest
    setup do
      @org = create(:organization)
      @team = @org.teams.first
      @admin = create(:user, organization: @org, organization_role: 'admin')
      @user, @comment_author = create_list(:user, 2, organization: @org)
      @card = create(:card, organization: @org, author: @user)
      @activity_stream = create(:activity_stream, streamable: @card, organization: @org, user: @user)
      api_v2_sign_in(@user)
    end

    test ':api destroy' do
      comment = create(:comment, commentable: @card, user: @comment_author)
      assert_difference -> { @card.comments.count }, -1 do
        delete :destroy, card_id: @card.id, id: comment.id, format: :json
        assert_response :ok
      end
    end

    test 'org admin should be able to delete any comment' do
      comment = create(:comment, commentable: @card, user: @comment_author)
      api_v2_sign_in(@admin)
      assert_difference -> { @card.comments.count }, -1 do
        delete :destroy, card_id: @card.id, id: comment.id, format: :json
        assert_response :ok
      end
    end

    test 'card author should be able to delete any comment on card' do
      comment = create(:comment, commentable: @card, user: @comment_author)
      assert_difference -> { @card.comments.count }, -1 do
        delete :destroy, card_id: @card.id, id: comment.id, format: :json
        assert_response :ok
      end
    end

    test "should not be able to delete other users' comments" do
      card = create(:card, author: @comment_author)
      another_user = create(:user, organization: @org)
      comment = create(:comment, commentable: card, user: another_user)
      assert_no_difference -> { card.comments.count } do
        delete :destroy, card_id: card.id, id: comment.id, format: :json
        assert_response :not_found
      end
    end

    test 'should destroy comment for activity' do
      comment = create(:comment, commentable: @activity_stream, user: @comment_author)
      api_v2_sign_in(@comment_author)
      assert_difference -> { @activity_stream.comments.count }, -1 do
        delete :destroy, activity_stream_id: @activity_stream.id, id: comment.id, format: :json
        assert_response :ok
      end
    end

    test 'org admin should be able to delete any comment on activity' do
      comment = create(:comment, commentable: @activity_stream, user: @comment_author)
      api_v2_sign_in(@admin)
      assert_difference -> { @activity_stream.comments.count }, -1 do
        delete :destroy, activity_stream_id: @activity_stream.id, id: comment.id, format: :json
        assert_response :ok
      end
    end

    test 'comment author should be able to delete his comment on activity' do
      comment = create(:comment, commentable: @activity_stream, user: @comment_author)
      api_v2_sign_in(@comment_author)
      assert_difference -> { @activity_stream.comments.count }, -1 do
        delete :destroy, activity_stream_id: @activity_stream.id, id: comment.id, format: :json
        assert_response :ok
      end
    end

    test 'card author should not be able to delete comment from other users on activity of card' do
      comment = create(:comment, commentable: @activity_stream, user: @comment_author)
      assert_no_difference -> { @activity_stream.comments.count } do
        delete :destroy, activity_stream_id: @activity_stream.id, id: comment.id, format: :json
        assert_response :not_found
      end
    end

    test 'organization admin should be able to delete any comment on team' do
      @team = create(:team, organization: @org)

      comment = create(:comment, commentable: @team, user: @user)
      api_v2_sign_in(@admin)
      assert_difference -> { @team.comments.count }, -1 do
        delete :destroy, team_id: @team.id, id: comment.id, format: :json
      end
    end

    test 'user should be able to delete his comment on team' do
      @team = create(:team, organization: @org)

      comment = create(:comment, commentable: @team, user: @user)
      assert_difference -> { @team.comments.count }, -1 do
        delete :destroy, team_id: @team.id, id: comment.id, format: :json
      end
    end

    test 'should not be able to delete other users comments on team' do
      @team = create(:team, organization: @org)

      comment = create(:comment, commentable: @team, user: @comment_author)
      assert_no_difference -> { @team.comments.count } do
        delete :destroy, team_id: @team.id, id: comment.id, format: :json
      end
    end

    test 'organization admin should be able to delete any comment on organization' do
      comment = create(:comment, commentable: @org, user: @user)
      api_v2_sign_in(@admin)
      assert_difference -> { @org.comments.count }, -1 do
        delete :destroy, organization_id: @org.id, id: comment.id, format: :json
      end
    end

    test 'user should be able to delete his comment on organization' do
      comment = create(:comment, commentable: @org, user: @user)
      assert_difference -> { @org.comments.count }, -1 do
        delete :destroy, organization_id: @org.id, id: comment.id, format: :json
      end
    end

    test 'should not be able to delete other users comments on organization' do
      comment = create(:comment, commentable: @org, user: @comment_author)
      assert_no_difference -> { @org.comments.count } do
        delete :destroy, organization_id: @org.id, id: comment.id, format: :json
      end
    end

    test 'should be able to delete associated comment_reportings' do
      comment = create(:comment, commentable: @card, user: @comment_author)
      reporter = create(:user, organization: @org)
      create(:comment_reporting, comment_id: comment.id, user_id: reporter.id)

      assert_difference -> { comment.comment_reportings.count }, -1 do
        delete :destroy, card_id: @card.id, id: comment.id, format: :json
      end
    end

    test 'should be able to delete a comment after being reported and unreported' do
      api_v2_sign_in(@admin)
      comment = create(:comment, commentable: @card, user: @comment_author)
      reporter = create(:user, organization: @org)
      create(:comment_reporting, comment_id: comment.id, user_id: reporter.id)

      put :unreport, id: comment.id, format: :json
      resp = get_json_response(response)
      assert_response 200

      assert_difference -> { @card.comments.count }, -1 do
        delete :destroy, card_id: @card.id, id: comment.id, format: :json
        assert_response :ok
      end
    end
  end

  class TrashActionTest < Api::V2::CommentsControllerTest
    setup do
      @org = create(:organization, enable_role_based_authorization: true)
      Role.create_standard_roles @org
      reporter_role = @org.roles.find_by_name('admin')
      create(:role_permission, role: reporter_role, name: Permissions::REPORT_COMMENT)

      @user, @comment_author = create_list(:user, 2, organization: @org)
      @admin = create(:user, organization: @org, organization_role: 'admin')
      @card = create(:card, organization: @org, author: @user)
      @activity_stream = create(:activity_stream, streamable: @card, organization: @org, user: @user)

      @comment = create(:comment, commentable: @card, user: @comment_author)
      @admin.add_role('admin')
      api_v2_sign_in(@admin)
    end

    test ':api trash' do
      assert_difference -> { @card.comments.count }, -1 do
        delete :trash, id: @comment.id, format: :json
        assert_response :ok
      end
    end

    test 'admin user should be able to trash any comment if RBAC is not enabled' do
      delete :trash, id: @comment.id, format: :json
      assert_response :ok
    end

    test 'admin user should not be able to trash any comment if not authorized to REPORT_COMMENT permission' do
      RolePermission.find_by_name(Permissions::REPORT_COMMENT).destroy
      delete :trash, id: @comment.id, format: :json
      assert_response :unauthorized
    end

    test 'admin user authorized to REPOR_CONTENT permission should be able to trash any comment if RBAC is enabled' do
      org = create(:organization, enable_role_based_authorization: true)
      Role.create_standard_roles org
      admin_role = org.roles.find_by_name('admin')
      create(:role_permission, role: admin_role, name: Permissions::ADMIN_ONLY)
      create(:role_permission, role: admin_role, name: Permissions::REPORT_COMMENT)

      rbac_admin = create(:user, organization: org)
      rbac_admin.add_role('admin')

      api_v2_sign_in rbac_admin
      delete :trash, id: @comment.id, format: :json
      assert_response :ok
    end

    test 'non admin user should not be able to trash any comment if RBAC is enabled' do
      org = create(:organization, enable_role_based_authorization: true)
      Role.create_standard_roles org

      api_v2_sign_in @user
      delete :trash, id: @comment.id, format: :json
      assert_response 401
    end
  end
end

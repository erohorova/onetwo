require 'test_helper'

class Api::V2::CardsControllerTest < ActionController::TestCase
  self.use_transactional_fixtures = true

  setup do
    stub_video_stream_index_calls
    EdcastActiveJob.stubs :perform_later
    IndexJob.stubs :perform_later

    @user = create(:user)
    @org = @user.organization

    api_v2_sign_in @user
    @url = 'http://url.com'
    stub_request(:get, @url).
          to_return(:status => 200, :body => '<html><head></head><body></body></html>', headers: {})
  end

  # Add card to pathway
  test ':api adds a card to a pack' do
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')

    cards = create_list(:card, 3, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end

    add = create(:card, card_type: 'media', author_id: @user.id)

    post :add, id: cover.id, content_id: add.id, content_type: 'Card', format: :json
    assert_response :ok

    pack_cover_data = CardPackRelation.pack_relations cover_id: cover.id

    # including cover card
    assert_equal 5, pack_cover_data.count
    assert_same_ids [cover.id] + cards.map(&:id) + [add.id], pack_cover_data.map(&:from_id)
  end

  test 'adds a card to a pack #member' do
    Organization.any_instance.unstub :create_or_update_roles
    Role.any_instance.unstub :create_role_permissions
    Role.create_standard_roles @org
    @user.roles << @org.roles.find_by_name('member')

    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')

    cards = create_list(:card, 3, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end

    add = create(:card, card_type: 'media', author_id: @user.id)

    post :add, id: cover.id, content_id: add.id, content_type: 'Card', format: :json
    assert_response :ok

    pack_cover_data = CardPackRelation.pack_relations cover_id: cover.id

    # including cover card
    assert_equal 5, pack_cover_data.count
    assert_same_ids [cover.id] + cards.map(&:id) + [add.id], pack_cover_data.map(&:from_id)
  end

  test 'adds a card to a pack #curator' do
    Organization.any_instance.unstub :create_or_update_roles
    @org.update(enable_role_based_authorization: true)
    @org.run_callbacks(:commit)

    @user.role_ids = [@org.roles.where(name: Role:: TYPE_CURATOR).first.id]
    @user.save

    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')

    cards = create_list(:card, 3, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end

    add = create(:card, card_type: 'media', author_id: @user.id)

    post :add, id: cover.id, content_id: add.id, content_type: 'Card', format: :json
    assert_response :unauthorized
  end

  test 'should delete its associated card_pack_relations when a card is destroyed' do
    TestAfterCommit.with_commits(true) do
      pathway = create :card, author: @user, organization: @org, card_type: 'pack', card_subtype: 'text'
      card = create :card, organization: @org, author: @user
      CardPackRelation.add(cover_id: pathway.id, add_id: card.id, add_type: pathway.class.name)

      CardDeletionJob.expects(:perform_later).with(card.id).once
      card.destroy
    end
  end

  test 'adds a card only to own pack' do
    user = create(:user)
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: user.id, title: 'pack cover')
    another_cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')

    add = create(:card, card_type: 'media', author_id: user.id)

    api_v2_sign_in(user)
    post :add, id: cover.id, content_id: add.id, content_type: 'Card', format: :json
    assert_response :ok

    post :add, id: another_cover.id, content_id: add.id, content_type: 'Card', format: :json
    assert_response :not_found
  end

  test 'adds a card with long title' do
    user = create(:user)
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: user.id, title: 'pack cover')
    add = create(:card, card_type: 'media', author_id: user.id, title: "0"*1000)

    api_v2_sign_in(user)
    post :add, id: cover.id, content_id: add.id, content_type: 'Card', format: :json
    assert_response :ok
  end

  test 'converts ECL card to destiny card and adds to a pathway' do
    cover = create(:card, message: 'pathway cover', card_type: 'pack', author_id: @user.id, title: 'pack cover')
    ecl_id = "ecl-card"

    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id}").
      to_return(status: 200, body: get_ecl_card_response(ecl_id).to_json)

    UrlMetadataExtractor.stubs(:get_metadata).returns({
      'og:type'    => 'video',
      'embed_html' => '<iframe />',
      'title'      => 'somev' })

    post :add, id: cover.id, content_id: ('ECL-' + ecl_id), content_type: 'Card', format: :json

    resp = get_json_response(response)
    assert_response :ok

    card = Card.find_by(ecl_id: ecl_id)
    pack_cover_data = CardPackRelation.pack_relations cover_id: cover.id
    assert_includes pack_cover_data.map(&:from_id), cover.id
    assert_includes pack_cover_data.map(&:from_id), card.id
  end
  #Add pathway ends

  # Add array of cards to pathway
  test ':api adds a array of cards to a pack' do
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')
    cards = create_list(:card, 3, author_id: @user.id, card_type: 'media')

    NotifyUserJob.expects(:perform_later)
      .with({action: 'added_to', card_ids: cards.map(&:id).sort, cover_ids: [cover.id], actor_id: @user.id}).once
    post :batch_add, id: cover.id, content_ids: cards.map(&:id), content_type: 'Card', format: :json

    assert_response :ok

    card_pack = CardPackRelation.where(cover_id: cover.id)

    assert_equal 4, card_pack.count
    assert_equal 1, card_pack.where(to_id: nil).count
    assert_same_ids [cover.id] + cards.map(&:id), card_pack.map(&:from_id)
  end

  test ':api should invoke job to recalculate completions after adding new cards to pathway' do
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')
    cards = create_list(:card, 3, author_id: @user.id, card_type: 'media')

    UpdatePathwayCompletionsJob.expects(:perform_later)
      .with({cover_id: cover.id, org_id: @org.id}).once
    post :batch_add, id: cover.id, content_ids: cards.map(&:id), content_type: 'Card', format: :json

    assert_response :ok
  end

  test ':api should add a array of cards to a pack in correct order' do
    skip #TODO: THIS TEST IS FAILING INTERMITTENTLY
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')
    cards = create_list(:card, 3, author_id: @user.id, card_type: 'media')

    post :batch_add, id: cover.id, content_ids: cards.map(&:id).reverse, content_type: 'Card', format: :json

    assert_response :ok
    card_pack = CardPackRelation.where(cover_id: cover.id)
    assert_equal [cover.id] + cards.map(&:id).reverse, card_pack.map(&:from_id)
  end

  test 'allow adding array of cards to a pack for admin' do
    admin_user  = create(:user, organization_role: 'admin', organization: @org)
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')
    cards = create_list(:card, 2, author_id: admin_user.id, card_type: 'media')

    api_v2_sign_in(admin_user)
    NotifyUserJob.expects(:perform_later)
      .with({action: 'added_to', card_ids: cards.map(&:id).sort, cover_ids: [cover.id], actor_id: admin_user.id}).once
    post :batch_add, id: cover.id, content_ids: cards.map(&:id), content_type: 'Card', format: :json

    assert_response :ok

    card_pack = CardPackRelation.where(cover_id: cover.id)

    assert_equal 3, card_pack.count
    assert_equal 1, card_pack.where(to_id: nil).count
    assert_same_ids [cover.id] + cards.map(&:id), card_pack.map(&:from_id)
  end

  test 'adding a map already existing in the pathway does not cause an error or duplication' do
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')
    cards = create_list(:card, 3, author_id: @user.id, card_type: 'media')

    post :add, id: cover.id, content_id: cards.first.id, content_type: 'Card', format: :json
    NotifyUserJob.expects(:perform_later)
      .with({action: 'added_to', card_ids: cards.map(&:id).sort, cover_ids: [cover.id], actor_id: @user.id}).once
    post :batch_add, id: cover.id, content_ids: cards.map(&:id), content_type: 'Card', format: :json

    assert_response :ok
    card_pack = CardPackRelation.where(cover_id: cover.id)

    assert_equal 4, card_pack.count
    assert_same_ids [cover.id] + cards.map(&:id), card_pack.map(&:from_id)
  end
  #Add array of cards ends

  # Add card to array of pathways
  test ':api adds card to array of packs' do
    pathway1 = create(:card,  message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover', card_subtype: 'simple')
    pathway2 = create(:card,  message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover', card_subtype: 'simple')
    covers = [pathway1, pathway2]
    card = create(:card, author_id: @user.id, card_type: 'media')

    NotifyUserJob.expects(:perform_later)
      .with({action: 'added_to', card_ids: [card.id], cover_ids: covers.map(&:id).sort, actor_id: @user.id}).once
    post :add_to_pathways, id: card.id, pathway_ids: covers.map(&:id), format: :json

    assert_response :ok

    card_pack_1 = CardPackRelation.where(cover_id: covers.first.id)
    card_pack_2 = CardPackRelation.where(cover_id: covers.last.id)

    assert_equal 2, card_pack_1.count
    assert_same_ids [covers.first.id, card.id], card_pack_1.map(&:from_id)
    assert_equal 2, card_pack_2.count
    assert_same_ids [covers.last.id, card.id], card_pack_1.map(&:from_id)
  end

  test 'user should able to add card to own packs with draft state' do
    covers = create_list(:card, 2, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover', state: 'draft')
    card = create(:card, author_id: @user.id, card_type: 'media')
    NotifyUserJob.expects(:perform_later)
      .with({action: 'added_to', card_ids: [card.id], cover_ids: covers.map(&:id).sort, actor_id: @user.id}).once
    post :add_to_pathways, id: card.id, pathway_ids: covers.map(&:id), format: :json

    assert_response :ok

    card_pack_1 = CardPackRelation.where(cover_id: covers.first.id)
    card_pack_2 = CardPackRelation.where(cover_id: covers.last.id)

    assert_includes card_pack_1.map(&:from_id), card.id
    assert_includes card_pack_2.map(&:from_id), card.id
  end

  test 'user should not able to add card to packs of other authors' do
    other_user_1 = create :user
    other_user_2 = create :user
    cover_1 = create(:card, message: 'some message', card_type: 'pack', author_id: other_user_1.id, title: 'pack cover', state: 'published')
    cover_2 = create(:card, message: 'some message', card_type: 'pack', author_id: other_user_2.id, title: 'pack cover', state: 'draft')
    card = create(:card, author_id: @user.id, card_type: 'media')
    NotifyUserJob.expects(:perform_later)
      .with({action: 'added_to', card_ids: [card.id], cover_ids: [cover_1.id, cover_2.id].sort, actor_id: @user.id}).once
    post :add_to_pathways, id: card.id, pathway_ids: [cover_1.id, cover_2.id], format: :json

    assert_response :ok

    card_pack_1 = CardPackRelation.where(cover_id: cover_1.id)
    card_pack_2 = CardPackRelation.where(cover_id: cover_2.id)

    assert_not_includes card_pack_1.map(&:from_id), card.id
    assert_not_includes card_pack_2.map(&:from_id), card.id
  end

  test 'user should not able to add card to packs with deleted and archived state' do
    deleted_pathway = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover', state: 'published')
    archived_pathway = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover', state: 'draft')
    cards = create_list(:card, 3, author_id: @user.id, card_type: 'media', hidden: true)
    cards.each do |card|
      CardPackRelation.add(cover_id: archived_pathway.id, add_id: card.id, add_type: 'Card')
      CardPackRelation.add(cover_id: deleted_pathway.id, add_id: card.id, add_type: 'Card')
    end
    archived_pathway.archive!
    deleted_pathway.destroy

    card = create(:card, author_id: @user.id, card_type: 'media')
    NotifyUserJob.expects(:perform_later)
      .with({action: 'added_to', card_ids: [card.id], cover_ids: [archived_pathway.id, deleted_pathway.id].sort, actor_id: @user.id}).once
    post :add_to_pathways, id: card.id, pathway_ids: [deleted_pathway.id, archived_pathway.id], format: :json

    assert_response :ok

    card_pack_1 = CardPackRelation.where(cover_id: deleted_pathway.id)
    card_pack_2 = CardPackRelation.where(cover_id: archived_pathway.id)

    assert_not_includes card_pack_1.map(&:from_id), card.id
    assert_not_includes card_pack_2.map(&:from_id), card.id
  end
  #Add to array of pathways ends

  # Remove from pathway
  test ":api remove cards from the pack and make it visible when `:dependent` is `FALSE`" do
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')

    cards = create_list(:card, 3, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end

    card = cards.first
    assert_equal 4, CardPackRelation.pack_relations(cover_id: cover.id).count

    assert_difference -> {CardPackRelation.count}, -1 do
      post :remove, id: cover.id, content_id: card.id, content_type: 'Card', delete_dependent: 'false', format: :json
    end

    card.reload
    cover.reload

    assert Card.exists?(card.id)
    refute card.hidden

    pack_cover_data = CardPackRelation.pack_relations cover_id: cover.id
    cards.shift
    expected_ids = [cover.id] + cards.map(&:id)

    # including cover card
    assert_equal 3, pack_cover_data.count
    assert_same_ids expected_ids, pack_cover_data.map(&:from_id)
  end

  test ":api remove array of cards from the pack" do
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')

    cards = create_list(:card, 3, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end
    card_to_remove_first = cards.first
    card_to_remove_second = cards.second
    NotifyUserJob.expects(:perform_later)
      .with({action: 'removed_from', card_ids: [card_to_remove_first.id, card_to_remove_second.id].sort, cover_ids: [cover.id], actor_id: @user.id}).once

    assert_equal 4, CardPackRelation.pack_relations(cover_id: cover.id).count
    assert_difference -> {CardPackRelation.count}, -2 do
      post :batch_remove, id: cover.id, card_ids: [card_to_remove_first.id, card_to_remove_second.id], delete_dependent: 'false'
    end

    card_to_remove_first.reload
    card_to_remove_second.reload

    cover.reload

    assert Card.exists?(card_to_remove_first.id)
    assert Card.exists?(card_to_remove_second.id)
    refute card_to_remove_first.hidden
    refute card_to_remove_second.hidden

    pack_cover_data = CardPackRelation.pack_relations cover_id: cover.id
    cards.shift(2)
    expected_ids = [cover.id] + cards.map(&:id)

    # including cover card
    assert_equal 2, pack_cover_data.count
    assert_same_ids expected_ids, pack_cover_data.map(&:from_id)
  end

  test ":api remove array of pathways from the journey and make them visible in a draft state" do
    stub_request(:post, "http://localhost:9200/cards_test/_refresh")

    cover = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: @user, organization_id: @org.id)
    pack1, pack2, pack3 = create_list(
      :card, 3, hidden: true, state: 'draft', card_type: 'pack',
      card_subtype: 'simple', author: @user, organization_id: @org.id
    )
    card1, card2, card3 = create_list(:card, 3, hidden: true, organization: @org, author: @user)

    CardPackRelation.add(cover_id: pack1.id, add_id: card1.id, add_type: pack1.class.name)
    CardPackRelation.add(cover_id: pack2.id, add_id: card2.id, add_type: pack2.class.name)
    CardPackRelation.add(cover_id: pack3.id, add_id: card3.id, add_type: pack3.class.name)

    pathways = pack1, pack2, pack3
    pathways.each do |pack|
      JourneyPackRelation.add(cover_id: cover.id, add_id: pack.id)
    end

    assert_equal 4, JourneyPackRelation.journey_relations(cover_id: cover.id).count
    assert_difference -> {JourneyPackRelation.count}, -2 do
      post :journey_batch_remove, id: cover.id, card_ids: [pack1.id, pack2.id], delete_dependent: 'false'
    end

    cover.reload
    pack1.reload
    pack2.reload

    refute pack1.hidden
    refute pack2.hidden
    cover_data = JourneyPackRelation.journey_relations cover_id: cover.id
    pathways.shift(2)
    expected_ids = [cover.id] + pathways.map(&:id)
    # including cover card
    assert_equal 2, cover_data.count
    assert_same_ids expected_ids, cover_data.map(&:from_id)
  end

  test 'allow removing array of cards from pack for admin' do
    admin_user  = create(:user, organization_role: 'admin', organization: @org)
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')

    cards = create_list(:card, 2, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end

    card_to_remove_first = cards.first
    card_to_remove_second = cards.second
    assert_equal 3, CardPackRelation.pack_relations(cover_id: cover.id).count

    api_v2_sign_in(admin_user)
    NotifyUserJob.expects(:perform_later)
      .with({action: 'removed_from', card_ids: [card_to_remove_first.id, card_to_remove_second.id].sort, cover_ids: [cover.id], actor_id: admin_user.id}).once
    assert_difference -> {CardPackRelation.count}, -2 do
      post :batch_remove, id: cover.id, card_ids: [card_to_remove_first.id, card_to_remove_second.id], delete_dependent: 'false'
    end
  end

  test ":api remove cards from the pack and hard-delete card when `:dependent` is `TRUE`" do
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')

    cards = create_list(:card, 3, author_id: @user.id, card_type: 'media', hidden: true)
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end

    card = cards.first

    assert_differences [[-> {CardPackRelation.count}, -1],[->{Card.count}, -1]] do
      post :remove, id: cover.id, content_id: card.id, content_type: 'Card', delete_dependent: 'true', format: :json
    end
  end

  test ":api should remove cards only from own pack" do
    user = create(:user)
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: user.id, title: 'pack cover')

    cards = create_list(:card, 3, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end

    card = cards.first.reload
    assert_no_difference -> {CardPackRelation.count} do
      delete :remove, id: cover.id, content_id: card.id, content_type: 'Card', format: :json
    end
  end

  test "remove cards from the pack #member" do
    Role.any_instance.unstub :create_role_permissions
    @org.update(enable_role_based_authorization: true)
    Role.create_standard_roles @org
    @user.roles << @org.roles.find_by_name('member')

    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')

    cards = create_list(:card, 3, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end

    card = cards.first
    assert_equal 4, CardPackRelation.pack_relations(cover_id: cover.id).count

    assert_difference -> {CardPackRelation.count}, -1 do
      post :remove, id: cover.id, content_id: card.id, content_type: 'Card', delete_dependent: 'false', format: :json
    end

    card.reload
    cover.reload

    assert Card.exists?(card.id)
    refute card.hidden
  end

  test "remove cards from the pack #curator" do
    Organization.any_instance.unstub :create_or_update_roles
    @org.update(enable_role_based_authorization: true)
    @org.run_callbacks(:commit)

    @user.role_ids = [@org.roles.where(name: Role::TYPE_CURATOR).first.id]
    @user.save
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')

    cards = create_list(:card, 3, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end

    card = cards.first
    assert_equal 4, CardPackRelation.pack_relations(cover_id: cover.id).count

    assert_no_difference -> {CardPackRelation.count} do
      post :remove, id: cover.id, content_id: card.id, content_type: 'Card', delete_dependent: 'false', format: :json
      assert_response :unauthorized
    end
  end

  #Remove from pathway ends

  # publish pathway
  test ':api publishes a pack' do
    request.env['HTTP_REFERER'] = 'http://edcast.com'
    cover = create(:card, message: 'some message', card_type: 'pack', state: 'draft', author_id: @user.id, title: 'pack cover')

    cards = create_list(:card, 2, author_id: @user.id, card_type: 'media', hidden: true)
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end

    Tracking.expects(:track_act).with(
        user_id: @user.id,
        object: 'collection',
        object_id: cover.id,
        card: ClientOnlyRoutes.show_insight_url(cover),
        action: 'publish',
        platform: 'web',
        device_id: nil,
        organization_id: @org.id,
        referer: 'http://edcast.com'
    )

    post :publish, id: cover.id

    cover.reload
    assert_response :ok
    assert_equal 'pack', cover.card_type
  end

  test 'publishes a pack #member' do
    @org.update(enable_role_based_authorization: true)
    Role.any_instance.unstub :create_role_permissions
    Role.create_standard_roles @org
    @user.roles << @org.roles.find_by_name('member')

    cover = create(:card, message: 'some message', card_type: 'pack', state: 'draft', author_id: @user.id, title: 'pack cover')

    cards = create_list(:card, 2, author_id: @user.id, card_type: 'media', hidden: true)
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end

    post :publish, id: cover.id

    cover.reload
    assert_response :ok
    assert_equal 'pack', cover.card_type
  end

  test 'should not publishes a pack #curator' do
    Organization.any_instance.unstub :create_or_update_roles
    @org.update(enable_role_based_authorization: true)
    @org.run_callbacks(:commit)
    @user.role_ids = [@org.roles.where(name: Role::TYPE_CURATOR).first.id]
    @user.save

    cover = create(:card, message: 'some message', card_type: 'pack', state: 'draft', author_id: @user.id, title: 'pack cover')
    cards = create_list(:card, 2, author_id: @user.id, card_type: 'media', hidden: true)

    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end

    post :publish, id: cover.id

    cover.reload
    assert_response :unauthorized
  end

  test ':api publishes only packs with 2+ cards' do
    cover = create(:card, message: 'some message', card_type: 'pack', state: 'draft', author_id: @user.id, title: 'pack cover')

    post :publish, id: cover.id

    assert_response :unprocessable_entity
    resp = get_json_response(response)

    assert_match /Please add cards/, resp['message']
  end

  test ':api publishes only own pack' do
    user = create(:user)
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: user.id, title: 'pack cover')

    cards = create_list(:card, 2, author_id: user.id, card_type: 'media', hidden: true)
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end
    post :publish, id: cover.id
  end

  test ':api publish_cards action' do
    c1, c2 = create_list(:card, 2, state: 'new', organization: @org, author_id: nil)
    c2.publish!
    post :publish_cards, card_ids: [c1.id, c2.id], format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_includes resp['successes'], c1.id
    assert_includes resp['errors'], c2.id
    assert c1.reload.published?
  end

  #publish pathway

  # delete pathway
  test 'destroys a pack' do
    refresh_mock = mock()
    Card.stubs(:searchkick_index).returns refresh_mock
    refresh_mock.stubs :reindex_record
    refresh_mock.stubs :refresh

    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')

    cards = create_list(:card, 2, author_id: @user.id, card_type: 'media', hidden: true)
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end

    assert_difference -> {Card.count}, -1 do
      delete :destroy, id: cover.id
      assert_response :ok
    end

    refute Card.exists?(cover.id)
    assert_nil CardPackRelation.pack(cover_id: cover.id)
  end

  test 'destroys a pack and dependent cards' do
    refresh_mock = mock()
    Card.stubs(:searchkick_index).returns refresh_mock
    refresh_mock.stubs :reindex_record

    card_index = Card.searchkick_index
    card_index.stubs(:refresh).returns true

    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')
    cards = create_list(:card, 2, author_id: @user.id, card_type: 'media', hidden: true)

    card = cards.second
    card.update hidden: false

    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end

    assert_difference ->{Card.count}, -2 do
      delete :destroy, id: cover.id, delete_dependent: 'true', format: :json
      assert_response :ok
    end

    refute Card.exists?(cover.id)
    assert Card.exists? card.id
    assert_nil CardPackRelation.pack(cover_id: cover.id)
  end

  test 'destroys only own pack' do
    refresh_mock = mock()
    Card.stubs(:searchkick_index).returns refresh_mock
    refresh_mock.stubs :reindex_record

    user = create(:user, organization: @org)
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: user.id, title: 'pack cover')

    cards = create_list(:card, 2, author_id: user.id, card_type: 'media', hidden: true)
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end

    assert_no_difference -> {Card.count} do
      delete :destroy, id: cover.id, format: :json
    end
    assert Card.exists?(cover.id)
  end
  # delete pathway ends

  # pathway reorder
  test 'reorder should return success with proper params' do
    cover = create(:card, message: 'some message', card_type: 'pack', state: 'draft', author_id: @user.id, title: 'pack cover')

    cards = create_list(:card, 4, author_id: @user.id, card_type: 'media', hidden: true)

    card = cards.second
    card.update hidden: false

    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end

    cpr_old_ids = CardPackRelation.pack_relations(cover_id: cover.id).map(&:id)
    post :reorder, id: cover.id, from: 1, to: 2, format: :json
    assert_response :success
    cover.reload

    cpr_new_ids = CardPackRelation.pack_relations(cover_id: cover.id).map(&:id)
    assert_not_equal cpr_old_ids[1], cpr_new_ids[1]
    assert_equal cpr_old_ids[1], cpr_new_ids[2]
  end

  test 'reorder should return success#member' do
    @org.update(enable_role_based_authorization: true)
    Role.any_instance.unstub :create_role_permissions
    Role.create_standard_roles @org
    @user.roles << @org.roles.find_by_name('member')

    cover = create(:card, message: 'some message', card_type: 'pack', state: 'draft', author_id: @user.id, title: 'pack cover')
    cards = create_list(:card, 4, author_id: @user.id, card_type: 'media', hidden: true)

    card = cards.second
    card.update hidden: false

    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end

    cpr_old_ids = CardPackRelation.pack_relations(cover_id: cover.id).map(&:id)
    post :reorder, id: cover.id, from: 1, to: 2, format: :json
    assert_response :success
    cover.reload

    cpr_new_ids = CardPackRelation.pack_relations(cover_id: cover.id).map(&:id)
    assert_not_equal cpr_old_ids[1], cpr_new_ids[1]
    assert_equal cpr_old_ids[1], cpr_new_ids[2]
  end

  test 'should not reorder #curator' do
    Organization.any_instance.unstub :create_or_update_roles
    @org.update(enable_role_based_authorization: true)
    @org.run_callbacks(:commit)

    @user.role_ids = [@org.roles.where(name: Role::TYPE_CURATOR).first.id]
    @user.save

    cover = create(:card, message: 'some message', card_type: 'pack', state: 'draft', author_id: @user.id, title: 'pack cover')
    cards = create_list(:card, 4, author_id: @user.id, card_type: 'media', hidden: true)

    card = cards.second
    card.update hidden: false

    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end

    cpr_old_ids = CardPackRelation.pack_relations(cover_id: cover.id).map(&:id)
    post :reorder, id: cover.id, from: 1, to: 2, format: :json
    assert_response :unauthorized
  end

  test 'returns error if params are missing' do
    cover = create(:card, message: 'some message', card_type: 'pack', state: 'draft', author_id: @user.id, title: 'pack cover')

    cards = create_list(:card, 4, author_id: @user.id, card_type: 'media', hidden: true)

    card = cards.second
    card.update hidden: false

    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end

    post :reorder, id: cover.id, format: :json
    assert_response :unprocessable_entity
  end
  # pathway reorder ends

  test ':api add, remove reorder and publish valid to only pathway' do
    card1, card2 = create_list(:card, 2, card_type: 'media', author_id: @user.id)

    # add
    post :add, id: card1.id, content_id: card2.id, content_type: 'Card', format: :json
    assert_response :unprocessable_entity
    assert_match /not a pathway/, get_json_response(response)['message']

    # remove
    post :remove, id: card1.id, content_id: card2.id, content_type: 'Card', format: :json
    assert_response :unprocessable_entity
    assert_match /not a pathway/, get_json_response(response)['message']


    # reorder
    post :reorder, id: card1.id, from: 1, to: 2, format: :json
    assert_response :unprocessable_entity
    assert_match /not a pathway/, get_json_response(response)['message']

    # publish
    post :publish, id: card1.id
    assert_response :unprocessable_entity
    assert_match /not a pathway/, get_json_response(response)['message']
  end

  test 'should dequeue and reenqueue on channell update' do
    channels = create_list(:channel,4, organization: @org)
    channels.each { |ch| ch.add_authors([@user]) }
    card = create(:card, is_public: true, author_id: @user.id)
    card.channel_ids = [channels.first.id]
    new_channels = channels.last(2).collect(&:id)
    ContentsBulkDequeueJob.expects(:perform_later).with({content_ids: [card.id], content_type:'Card', reenqueue: true}).once
    put :update, id: card.id, card: {message: 'Updated Card', channel_ids: new_channels}, format: :json

    resp = get_json_response(response)
    card.reload
    assert_same_elements new_channels, card.channel_ids
  end

  test 'should no dequeue if channels not updated' do
    channels = create_list(:channel, 2, organization: @org)
    channels.each { |ch| ch.add_authors([@user]) }
    card = create(:card, is_public: true, author_id: @user.id)
    card.channel_ids = channels.collect(&:id)
    ContentsBulkDequeueJob.expects(:perform_later).with({content_ids: [card.id], content_type:'Card', reenqueue: true}).never
    put :update, id: card.id, card: {message: 'Updated Card', channel_ids: card.channel_ids }, format: :json
  end

  test 'should auto curate cards posted in channel by collaborators with bypass permission' do
    channels = create_list(:channel, 2, organization: @org, curate_only: true)
    channels.each { |ch| ch.authors << @user }
    card = create(:card, is_public: true, author_id: @user.id)
    channel_ids = channels.collect(&:id)

    Role.create(
      name: 'custom',
      organization_id: @org.id,
      master_role: false
    )
    create(
      :role_permission, role: Role.find_by(name: 'custom'),
      name: Permissions::BYPASS_CURATION
    )
    @user.add_role('custom')

    #added config for followers that should not break flow for collaborators
    create(
      :config,
      configable_id: @org.id,
      configable_type: 'Organization',
      name: 'channel/curation_for_followers_card',
      data_type: "boolean", value: 'true'
    )

    ContentsBulkDequeueJob.expects(:perform_later).with(
      {
        content_ids: [card.id],
        content_type: 'Card',
        reenqueue: true
      }
    ).once

    assert_difference -> { ChannelsCard.count }, 2 do
      put :update, id: card.id, card: { message: 'Updated Card', channel_ids: channel_ids }, format: :json
      assert_equal ['curated'], card.channels_cards.pluck(:state).uniq
    end
  end

  test 'should auto curate cards posted in channel by followers and followers cards curation is not required' do
    channels = create_list(:channel, 2, organization: @org, curate_only: true, only_authors_can_post: false)
    channels.each { |ch| ch.add_followers([@user]) }
    card = create(:card, is_public: true, author_id: @user.id)
    channel_ids = channels.collect(&:id)
    config = create :config, configable_id: @org.id, configable_type: 'Organization', name: 'channel/curation_for_followers_card', data_type: "boolean", value: 'false'

    ContentsBulkDequeueJob.expects(:perform_later).with({content_ids: [card.id], content_type:'Card', reenqueue: true}).once
    assert_difference -> {ChannelsCard.count}, 2 do
      put :update, id: card.id, card: {message: 'Updated Card', channel_ids: channel_ids }, format: :json
      assert_equal card.channels_cards.pluck(:state).uniq, ["curated"]
    end
  end

  test 'should auto curate cards posted in channel by followers and followers cards curation is required' do
    channels = create_list(:channel, 2, organization: @org, curate_only: true, only_authors_can_post: false)
    channels.each { |ch| ch.add_followers([@user]) }
    card = create(:card, is_public: true, author_id: @user.id)
    channel_ids = channels.collect(&:id)
    config = create :config, configable_id: @org.id, configable_type: 'Organization', name: 'channel/curation_for_followers_card', data_type: "boolean", value: 'true'

    ContentsBulkDequeueJob.expects(:perform_later).with({content_ids: [card.id], content_type:'Card', reenqueue: true}).once
    assert_difference -> {ChannelsCard.count}, 2 do
      put :update, id: card.id, card: {message: 'Updated Card', channel_ids: channel_ids }, format: :json
      assert_equal card.channels_cards.pluck(:state).uniq, ["new"]
    end
  end

  test 'should handle card channelids as string ' do
    card_message = Faker::Lorem.sentence

    channels = create_list(:channel,2, organization: @org)
    channels.each { |channel| channel.add_authors([@user]) }
    post :create, card: {message: card_message, channel_ids: channels.collect(&:id).collect(&:to_s) }, format: :json
    card = Card.last

    assert_equal card_message, card.message
    assert_same_elements channels.collect(&:id), card.channel_ids
  end

  class PathwayTest < ActionController::TestCase
    include Devise::Test::ControllerHelpers
    self.use_transactional_fixtures = false

    setup do
      # WebMock.disable!
      @user = create(:user)
      @org = @user.organization
      api_v2_sign_in @user
    end

    teardown do
      # WebMock.enable!
    end

    # packs starts
    test 'create new pathway' do
      pathway_message = Faker::Lorem.sentence

      assert_difference [-> {Card.count}, -> {CardPackRelation.count}] do
        post :create, card: {message: pathway_message, card_type: 'pack', card_subtype: 'simple' }, format: :json
      end

      resp = get_json_response(response)
      assert_not_nil resp['id']
      assert_equal 'pack', resp['card_type']
      assert_equal pathway_message, resp['message']
    end

    test 'create new pathway with badge' do
      pathway_message = Faker::Lorem.sentence
      card_badge = create(:card_badge, organization: @org)

      assert_difference [-> {Card.count}, -> {CardPackRelation.count}, -> {CardBadging.count}] do
        post :create, card: {message: pathway_message, card_type: 'pack', card_subtype: 'simple', card_badging_attributes: {title: 'badge title', badge_id: card_badge.id} }, format: :json
      end

      resp = get_json_response(response)
      assert_equal Card.first.id, CardBadging.first.badgeable_id
      assert_not_nil resp['id']
      assert_equal 'pack', resp['card_type']
      assert_equal pathway_message, resp['message']
      assert_equal Card.first.card_badging.title, "badge title"
    end

    test 'create pathway with badge and update badge' do
      pathway_message = Faker::Lorem.sentence
      card_badge = create(:card_badge, organization: @org)
      card = create(:card, is_public: true, author_id: @user.id, card_badging_attributes: {title: 'badge title', badge_id: card_badge.id})

      put :update, id: Card.last.id, card: {message: pathway_message, card_type: 'pack', card_subtype: 'simple', card_badging_attributes: {title: 'badge title updated', badge_id: card_badge.id} }, 
        fields: 'id,card_type,message', format: :json

      resp = get_json_response(response)
      assert_equal Card.first.id, CardBadging.first.badgeable_id
      assert_not_nil resp['id']
      assert_equal 'pack', resp['card_type']
      assert_equal pathway_message, resp['message']
      assert_equal Card.first.card_badging.title, "badge title updated"
    end

    test 'create new pathway with prices' do
      pathway_message = Faker::Lorem.sentence

      assert_difference [-> {Card.count}, -> {CardPackRelation.count}, -> {Price.count}] do
        post :create, card: {message: pathway_message, card_type: 'pack', card_subtype: 'simple',
                             is_paid: "true",
                             prices_attributes: [{ currency: "USD", amount: 123 }]}, format: :json
      end

      resp = get_json_response(response)
      assert true, resp["is_paid"]
      assert_equal Card.first.id, Price.last.card_id
    end

    test 'create pathway with prices and update prices' do
      pathway_message = Faker::Lorem.sentence

      card = create(:card, is_public: true, author_id: @user.id,
                    is_paid: "true",
                    prices_attributes: [{ currency: "USD", amount: 123 }])
      price = card.prices.last
      put :update, id: Card.last.id, card: {message: pathway_message, card_type: 'pack', card_subtype: 'simple',
                                            is_paid: "true",
                                            prices_attributes: [{ currency: "USD", amount: 321, id: price.id }]}, format: :json

      resp = get_json_response(response)
      assert_equal BigDecimal("321"), Card.last.prices.last.amount
    end

    test 'should add pathways to channels' do
      pathway_message = Faker::Lorem.sentence

      channels = create_list(:channel, 2, organization: @org)
      channels.each {|ch| ch.authors << @user}

      assert_difference -> {Card.count} do
        post :create, card: {message: pathway_message, channel_ids: channels.map(&:id)}, format: :json
      end

      resp = get_json_response(response)
      assert_equal channels.map(&:id), resp['channels'].map {|ch| ch['id']}
    end
    # packs ends
  end
  # addable pathways
  test ':api should return list of pathways that card can be added to' do
    org = create(:organization)
    author, another_author = create_list(:user, 2, organization: org)
    card = create(:card, author: another_author)
    author_pathways = create_list(:card, 2, card_type: 'pack', author: author)
    addable_pathways = create_list(:card, 2, card_type: 'pack', author: author)
    another_author_pathways = create_list(:card, 3, card_type: 'pack', author: another_author)

    author_pathways.each do |cover|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: card.class.name)
    end

    another_author_pathways.each do |cover|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: card.class.name)
    end

    api_v2_sign_in author

    get :addable_pathways, id: card.id, format: :json
    resp = get_json_response(response)

    assert_same_elements addable_pathways.map(&:id), resp['cards'].map {|card| card['id']}
    assert_equal 2, resp['total']
  end

  test 'should return list of pathways in asc order based on message' do
    org = create(:organization)
    author, another_author = create_list(:user, 2, organization: org)
    card = create(:card, author: another_author)
    author_pathways = create_list(:card, 2, card_type: 'pack', author: author)
    pathway_msg_z = create(:card, card_type: 'pack', author: author, message: 'z last msg')
    pathway_msg_a = create(:card, card_type: 'pack', author: author, message: 'a first msg')
    pathway_msg_r = create(:card, card_type: 'pack', author: author, message: 'r third msg')
    pathway_msg_p = create(:card, card_type: 'pack', author: author, message: 'p second msg')

    author_pathways.each do |cover|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: card.class.name)
    end

    api_v2_sign_in author

    get :addable_pathways, id: card.id, format: :json, sort: 'message', order: 'ASC'
    resp = get_json_response(response)
    assert_same_elements [pathway_msg_a.id,pathway_msg_p.id,pathway_msg_r.id,pathway_msg_z.id], resp['cards'].map {|card| card['id']}
  end

  test 'should return list of pathways in desc order based on title' do
    org = create(:organization)
    author, another_author = create_list(:user, 2, organization: org)
    card = create(:card, author: another_author)
    author_pathways = create_list(:card, 2, card_type: 'pack', author: author)
    pathway_msg_z = create(:card, card_type: 'pack', author: author, title: 'z last title')
    pathway_msg_a = create(:card, card_type: 'pack', author: author, title: 'a first title')
    pathway_msg_r = create(:card, card_type: 'pack', author: author, title: 'r third title')
    pathway_msg_p = create(:card, card_type: 'pack', author: author, title: 'p second title')

    author_pathways.each do |cover|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: card.class.name)
    end

    api_v2_sign_in author

    get :addable_pathways, id: card.id, format: :json, sort: 'title', order: 'DESC'
    resp = get_json_response(response)
    assert_same_elements [pathway_msg_z.id,pathway_msg_r.id,pathway_msg_p.id,pathway_msg_a.id], resp['cards'].map {|card| card['id']}
  end

  test 'should return list of pathways in by default desc order based on message' do
    org = create(:organization)
    author, another_author = create_list(:user, 2, organization: org)
    card = create(:card, author: another_author)
    author_pathways = create_list(:card, 2, card_type: 'pack', author: author)
    pathway_msg_z = create(:card, card_type: 'pack', author: author, message: 'z last msg')
    pathway_msg_a = create(:card, card_type: 'pack', author: author, message: 'a first msg')
    pathway_msg_r = create(:card, card_type: 'pack', author: author, message: 'r third msg')
    pathway_msg_p = create(:card, card_type: 'pack', author: author, message: 'p second msg')

    author_pathways.each do |cover|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: card.class.name)
    end

    api_v2_sign_in author

    get :addable_pathways, id: card.id, format: :json, sort: 'message'
    resp = get_json_response(response)
    assert_same_elements [pathway_msg_z.id,pathway_msg_r.id,pathway_msg_p.id,pathway_msg_a.id], resp['cards'].map {|card| card['id']}
  end

  test 'should return list of pathways without sorting' do
    org = create(:organization)
    author, another_author = create_list(:user, 2, organization: org)
    card = create(:card, author: another_author)
    author_pathways = create_list(:card, 2, card_type: 'pack', author: author)
    pathway_msg_z = create(:card, card_type: 'pack', author: author, message: 'z last msg')
    pathway_msg_a = create(:card, card_type: 'pack', author: author, message: 'a first msg')
    pathway_msg_r = create(:card, card_type: 'pack', author: author, message: 'r third msg')
    pathway_msg_p = create(:card, card_type: 'pack', author: author, message: 'p second msg')

    author_pathways.each do |cover|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: card.class.name)
    end

    api_v2_sign_in author

    get :addable_pathways, id: card.id, format: :json
    resp = get_json_response(response)
    assert_same_elements [pathway_msg_z.id,pathway_msg_a.id,pathway_msg_r.id,pathway_msg_p.id], resp['cards'].map {|card| card['id']}
  end

  test 'addable_pathways should only have response which are required by frontend' do
    card = create :card, author: @user
    pathway = create :card, card_type: 'pack', author: @user
    get :addable_pathways, id: card.id, format: :json
    response_card = get_json_response(response)['cards'].first
    assert_equal %w(id title message author updated_at), response_card.keys
    assert_equal %w(name handle), response_card['author'].keys
  end

  test 'should return list of pathways that card can be added to #member' do
    Role.any_instance.unstub :create_role_permissions
    Role.create_standard_roles @org
    @user.roles << @org.roles.find_by_name('member')

    author, another_author = create_list(:user, 2, organization: @org)
    author.roles << @org.roles.find_by(name: 'member')

    card = create(:card, author: another_author)
    author_pathways = create_list(:card, 2, card_type: 'pack', author: author)
    addable_pathways = create_list(:card, 2, card_type: 'pack', author: author)
    another_author_pathways = create_list(:card, 3, card_type: 'pack', author: another_author)

    author_pathways.each do |cover|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: card.class.name)
    end

    another_author_pathways.each do |cover|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: card.class.name)
    end

    api_v2_sign_in author

    get :addable_pathways, id: card.id, format: :json
    resp = get_json_response(response)
    assert_same_elements addable_pathways.map(&:id), resp['cards'].map {|card| card['id']}
    assert_equal 2, resp['total']
  end

  test 'should return list of pathways that card can be added to #curator' do
    Organization.any_instance.unstub(:create_or_update_roles)
    @org.update(enable_role_based_authorization: true)
    @org.run_callbacks(:commit)

    author, another_author = create_list(:user, 2, organization: @org)
    author.role_ids = [@org.roles.where(name: Role::TYPE_CURATOR).first.id]
    author.save

    card = create(:card, author: another_author)
    author_pathways = create_list(:card, 2, card_type: 'pack', author: author)
    addable_pathways = create_list(:card, 2, card_type: 'pack', author: author)
    another_author_pathways = create_list(:card, 3, card_type: 'pack', author: another_author)

    author_pathways.each do |cover|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: card.class.name)
    end

    another_author_pathways.each do |cover|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: card.class.name)
    end

    api_v2_sign_in author

    get :addable_pathways, id: card.id, format: :json
    assert_response :unauthorized
  end

  test ':api should return error if not found card or was lost connection with ECL' do
    card_ecl_id = 'd1322929-b492-4350-9ccf-349178d541bd'
    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{card_ecl_id}").
        to_return(status: 500)
    get :addable_pathways, id: "ECL-#{card_ecl_id}", format: :json
    resp = get_json_response(response)
    assert_response :not_found
    assert_equal 'Card not found or was lost connection with ECL',resp['message']
  end

  test 'should return info about when every user add card to pathway' do
    card = create(:card, author: @user, organization: @org)
    cover = create(:card, author: @user, card_type: 'pack', organization: @org)
    create(:card_pack_relation, cover_id: cover.id, from_id: card.id, from_type: 'Card')

    report_mock = mock()
    AnalyticsReport::ContentActivityReport.expects(:new).with({card_id: card.id, from_date: nil,
          to_date: nil, action_types: ['add_to_pathway']}).returns(report_mock)

    report_mock.expects(:generate_report).returns({total: 1, data: [{cover_id: cover.id}]})

    get :time_range_analytics, id: card.id, action_types: ['add_to_pathway'], format: :json

    assert_response :ok
    resp = get_json_response(response)

    assert_equal 1, resp['metrics']['total']
    assert_equal cover.id, resp['metrics']['data'][0]['cover_id']
  end

  test 'addable_pathways should have response as card object when requested from mobile app' do
    request.headers['HTTP_USER_AGENT'] = "Edcast mobile"
    api_v2_sign_in @user
    card = create :card, author: @user
    pathway = create :card, card_type: 'pack', author: @user
    get :addable_pathways, id: card.id, format: :json
    assert_response :ok
    keys = %w[
      id card_type card_subtype slug state is_official is_public created_at
      updated_at published_at hidden ecl_id provider
      comments_count votes_count readable_card_type title message
      language views_count share_url completed_percentage
      mark_feature_disabled_for_source ecl_duration_metadata
      voters channel_ids non_curated_channel_ids user_rating
      all_ratings average_rating skill_level is_upvoted
      auto_complete is_bookmarked is_assigned completion_state
      filestack mentions author tags file_resources channels
      teams users_with_access pack_cards user_taxonomy_topics
      paid_by_user is_paid payment_enabled prices
    ]
    assert_equal keys, get_json_response(response)['cards'].first.keys
  end
end

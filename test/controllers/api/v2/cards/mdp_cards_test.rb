require 'test_helper'

class Api::V2::CardsControllerTest < ActionController::TestCase

  class MdpUserCardsTest < ActionController::TestCase
    self.use_transactional_fixtures = true

    setup do
      EdcastActiveJob.stubs :perform_later
      IndexJob.stubs :perform_later

      @mdp_org = create(:organization)
      @mdp_user = create(:user, organization: @mdp_org)
      author = create(:user, organization: @mdp_org)
      @mdp_card = create(:card, organization: @mdp_org, author: author)

      api_v2_sign_in @mdp_user
    end

    test 'should create MDP record for user' do
      post :add_to_mdp, id: @mdp_card.id, form_details: {
        source_of_mdp: "Discussion with Manager", sub_competency_value: "Understand Business Environment", 
        additional_comments: "comments for MDP", learning_activity: "GVC Learning"
      }, format: :json
      resp = get_json_response(response)

      assert_equal @mdp_card.id, resp['card_id']
      assert_equal @mdp_user.id, resp['user_id']
      assert_equal "Discussion with Manager", resp['form_details']['source_of_mdp']
      assert_equal "Understand Business Environment", resp['form_details']['sub_competency_value']
      assert_equal "comments for MDP", resp['form_details']['additional_comments']
      assert_equal "GVC Learning", resp['form_details']['learning_activity']
    end

    test 'should create MDP user card record for virtual card' do
      ecl_id = 'abc-def-ijk'

      ecl_card = create :card
      ecl = mock
      ecl.expects(:card_from_ecl_id).returns(ecl_card)
      EclApi::CardConnector.expects(:new).with(ecl_id: ecl_id, organization_id: @mdp_org.id).returns(ecl)

      post :add_to_mdp, id: 'ECL-'+ecl_id, form_details: {source_of_mdp: "Discussion with Manager", 
        sub_competency_value: "Understand Business Environment", learning_activity: "GVC Learning"
      }, format: :json
      resp = get_json_response(response)
      assert_equal ecl_card.id, resp['card_id']
    end

    test 'should not create MDP record for user if form_details is empty' do
      post :add_to_mdp, id: @mdp_card.id, form_details: {}, format: :json
      resp = get_json_response(response)

      assert_equal 422, response.status
      assert_equal "Error while creating MDP Record Form details can't be blank", resp['message']
    end

    test 'should create MDP record for user if additional_comments is empty' do
      post :add_to_mdp, id: @mdp_card.id, form_details: {
        source_of_mdp: "Discussion with Manager", sub_competency_value: "Understand Business Environment", 
        additional_comments: nil, learning_activity: "GVC Learning"
      }, format: :json
      resp = get_json_response(response)

      assert_equal @mdp_card.id, resp['card_id']
      assert_equal @mdp_user.id, resp['user_id']
      assert_equal "Discussion with Manager", resp['form_details']['source_of_mdp']
      assert_equal "Understand Business Environment", resp['form_details']['sub_competency_value']
      assert_equal "GVC Learning", resp['form_details']['learning_activity']
      refute resp['form_details']['additional_comments']
    end

    test 'should not create MDP record for user if source_of_mdp is empty' do
      post :add_to_mdp, id: @mdp_card.id, form_details: {
        source_of_mdp: nil, sub_competency_value: "Understand Business Environment", 
        additional_comments: "comments for MDP", learning_activity: "GVC Learning"
      }, format: :json
      resp = get_json_response(response)
      message = "Error while creating MDP Record Form details source_of_mdp can't be blank"

      assert_equal 422, response.status
      assert_equal message, resp['message']
    end

    test 'should not create MDP record for user if sub_competency_value is empty' do
      post :add_to_mdp, id: @mdp_card.id, form_details: {
        source_of_mdp: "Discussion with Manager", sub_competency_value: nil, 
        additional_comments: "comments for MDP", learning_activity: "GVC Learning"
      }, format: :json
      resp = get_json_response(response)
      message = "Error while creating MDP Record Form details sub_competency_value can't be blank"

      assert_equal 422, response.status
      assert_equal message, resp['message']
    end

    test 'should not create MDP record for user if learning_activity is empty' do
      post :add_to_mdp, id: @mdp_card.id, form_details: {
        source_of_mdp: "Discussion with Manager", sub_competency_value: "Understand Business Environment", 
        additional_comments: "comments for MDP", learning_activity: nil
      }, format: :json
      resp = get_json_response(response)
      message = "Error while creating MDP Record Form details learning_activity can't be blank"

      assert_equal 422, response.status
      assert_equal message, resp['message']
    end

    test 'should not create MDP record for user if required keys are empty' do
      post :add_to_mdp, id: @mdp_card.id, form_details: {
        source_of_mdp: "Discussion with Manager", additional_comments: "comments for MDP"
      }, format: :json
      resp = get_json_response(response)
      message = "Error while creating MDP Record Form details sub_competency_value,learning_activity not present"

      assert_equal 422, response.status
      assert_equal message, resp['message']
    end

    test 'should show MDP record for user' do
      mdp_record = @mdp_user.mdp_user_cards.create(card_id: @mdp_card.id,
        form_details: {
          source_of_mdp: "Discussion with Manager",
          sub_competency_value: "Understand Business Environment",
          additional_comments: "comments for MDP",
          learning_activity: "GVC Learning"
        })

      get :mdp_card_detail, id: @mdp_card.id, format: :json
      resp = get_json_response(response)

      assert_equal @mdp_card.id, resp['card_id']
      assert_equal @mdp_user.id, resp['user_id']
      assert_equal "Discussion with Manager", resp['form_details']['source_of_mdp']
      assert_equal "Understand Business Environment", resp['form_details']['sub_competency_value']
      assert_equal "GVC Learning", resp['form_details']['learning_activity']
      assert_equal "comments for MDP", resp['form_details']['additional_comments']
    end

    test 'should show error message if MDP record for user is not found' do
      get :mdp_card_detail, id: @mdp_card.id, format: :json
      resp = get_json_response(response)

      assert_equal 422, response.status
      assert_equal "MDP User Record not found for card", resp['message']
    end
  end
end
require 'test_helper'

class Api::V2::CardsControllerTest < ActionController::TestCase
  self.use_transactional_fixtures = true

  setup do
    stub_video_stream_index_calls
    EdcastActiveJob.stubs :perform_later
    IndexJob.stubs :perform_later

    @user = create(:user)
    @org = @user.organization

    api_v2_sign_in @user
    @url = 'http://url.com'
    stub_request(:get, @url).
          to_return(:status => 200, :body => '<html><head></head><body></body></html>', headers: {})
  end
    # show
  test ':api should render card details' do

    ch1 = create(:channel, :robotics, organization: @org)
    ch1.authors << @user
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)

    message = @url
    resource = Resource.extract_resource(message)

    card = create(:card, message: message, title: message, card_type: 'media', author_id: @user.id, channel_ids: [ch1.id], file_resource_ids: [fr1.id])

    get :show, id: card.id, format: :json

    assert_response :ok
  end

  test 'should return job_title field in response' do
    user = create(:user)
    card = create(:card, author_id: user.id, card_type: 'pack')
    profile = create(:user_profile, user: user, job_title: 'aaa')
    api_v2_sign_in user

    get :show, id: card.id, format: :json
    resp = get_json_response(response)['author']
    assert profile.job_title, resp['job_title']
  end

  test ':api should return paid_by_user true if user has paid for card in card details' do
    CardSubscription.any_instance.stubs(:send_email_notification)
    @user = create(:user)
    card = create(:card, is_paid: true, author: @user)
    create(:card_subscription, card_id: card.id, user_id: @user.id)

    api_v2_sign_in(@user)

    get :show, id: card.id, format: :json
    resp = get_json_response(response)

    assert resp['paid_by_user']
  end

  test ':api should return payment_enabled true if auth api information is present for that source' do
    @user = create(:user)
    card = create(:card, author: @user, ecl_metadata: {source_id: 'test-source-id'}, organization_id: @user.organization_id)
    source_cred1 = create :sources_credential, auth_api_info: {auth_required: true}, source_id: 'test-source-id',
                                               organization_id: @user.organization_id

    api_v2_sign_in(@user)

    get :show, id: card.id, format: :json
    resp = get_json_response(response)

    assert resp['payment_enabled']
  end

  test ':api should return rounded off value in amount' do
    @user = create(:user)
    card = create(:card, author: @user)
    create(:price, card: card, amount: 25.5, currency: 'INR')
    create(:price, card: card, amount: 10.2, currency: 'SKILLCOIN')
    create(:price, card: card, amount: 5.7, currency: 'USD')

    api_v2_sign_in(@user)

    get :show, id: card.id, format: :json
    resp = get_json_response(response)

    assert_equal 26, resp['prices'][0]['amount']
    assert_equal 10, resp['prices'][1]['amount']
    assert_equal 6, resp['prices'][2]['amount']
  end

  test ':api should render card details with image_url' do
    ch1 = create(:channel, :robotics, organization: @org)
    ch1.authors << @user

    message = @url
    card = create(:card, message: message, title: message, card_type: 'media', author_id: @user.id, channel_ids: [ch1.id])

    get :show, id: card.id, format: :json
    resp = get_json_response(response)
    assert_equal "https://cdn-test-host.com/assets/default_banner_image_mobile.jpg", resp["channels"][0]["image_url"]
  end

  #test ':api should return 404 for private cards not accessible to user' do
  #EP-16550 Now public cards are visible to user irrespective of which container they belong to
  #end

  #test ':api should return 404 for hidden cards whose cover card is shared to private channel' do
  #EP-16550 Now public cards are visible to user irrespective of which container they belong to
  #end

  test ':api should return 404 for hidden cards whose cover card is not accessible to user' do
    author = create(:user, organization: @user.organization)
    private_pathway = create(:card, organization_id: @org.id, author: author, card_type: 'pack', is_public: false)
    card = create(:card, message: "test", title: "test title", card_type: 'media', author: author, is_public: true, hidden: true)
    CardPackRelation.add(cover_id: private_pathway.id, add_id: card.id, add_type: 'Card')

    get :show, id: card.id, format: :json
    assert_response 404
  end

  test 'user should be able get own draft card details' do
    card = create(:card, author_id: @user.id, state: 'draft')

    get :show, id: card.id, format: :json
    resp = get_json_response(response)

    assert_response :ok
    assert_equal resp['id'].to_i, card.id
  end

  test 'admin should be able get draft card details' do
    Role.any_instance.unstub :create_role_permissions
    Role.create_standard_roles @org
    admin_user = create(:user, organization: @org)
    admin_user.roles << @org.roles.where(name: 'admin').first
    card = create(:card, author_id: @user.id, state: 'draft')

    api_v2_sign_in(admin_user)

    get :show, id: card.id, format: :json
    resp = get_json_response(response)

    assert_response :ok
    assert_equal resp['id'].to_i, card.id
  end

  test 'should render card title and message' do
    message = @url
    resource = Resource.extract_resource(message)

    card = create(:card, message: message, title: message, card_type: 'media', author_id: @user.id)

    get :show, id: card.id, format: :json
    resp = get_json_response(response)
    assert_equal "", resp['title']
    assert_equal message, resp['message']

    assert_response :ok
  end

  test 'should not show non published cards' do
    card = create(:card, state: 'archived', author: create(:user, organization: @org))
    get :show, id: card.id, format: :json
    assert_response :not_found
  end

  test 'show should handle ecl cards' do
    ecl_id = "ecl-card"

    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id}").
      to_return(status: 200, body: get_ecl_card_response(ecl_id).to_json)

    assert_difference -> {Card.count} do
      get :show, id: "ECL-#{ecl_id}", format: :json
      assert_response :ok
    end
  end

  test 'should include share URL in response' do
    card = create(:card, author: @user)
    get :show, id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal "#{card.organization.home_page}/insights/#{card.id}", resp['share_url']
  end

  test ':api should render card details and should not show non curated channels' do
    channels = create_list(:channel, 2, organization: @org)
    curated_channel = create(:channel, curate_only: true, organization: @org)

    curated_channel.authors << @user
    channels.each {|ch| ch.authors << @user }
    channel_ids = channels.map(&:id) << curated_channel.id

    message = "Awesome message #{@url}"
    card = create(:card, message: message,  card_type: 'media', author_id: nil, organization: @user.organization, channel_ids: channel_ids )

    get :show, id: card.id, format: :json
    json_response = get_json_response(response)
    assert_same_elements channels.map(&:id), json_response['channel_ids']
    assert_same_elements channels.map(&:id), json_response['channels'].map { |ch| ch['id'].to_i }
  end

  test 'should include provider and provider_image url in response' do
    card = create(:card, provider: 'test provider',
      provider_image: 'https://cdn.filestackcontent.com/LcdP4ZIQRF6VUJktoPSS', author: @user)
    Filestack::Security.any_instance.stubs(:signed_url).returns(card.provider_image)
    get :show, id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal card.provider, resp['provider']
    assert_equal card.provider_image, resp['provider_image']
  end

  test 'is upvoted should be correct' do
    card = create(:card, author: create(:user, organization: @org))
    get :show, id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    refute resp['is_upvoted']

    create(:vote, votable: card, voter: @user)
    card.reload
    get :show, id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert resp['is_upvoted']
  end

  test 'should return voters information' do
    card = create(:card, author: @user)

    pic = Rails.root.join('test/fixtures/images/logo.png')
    voters = create_list(:user, 2, organization: @org, avatar: pic.open)
    create(:vote, votable: card, voter: voters.first)
    create(:vote, votable: card, voter: voters.last)

    get :show, id: card.id, format: :json
    resp = get_json_response(response)

    assert_equal voters.map(&:id), resp['voters'].map { |hash| hash['id'] }
    assert_equal voters.map(&:full_name), resp['voters'].map { |hash| hash['name'] }
    assert_equal voters.map(&:handle), resp['voters'].map { |hash| hash['handle'] }
    assert_not_empty resp['voters'].map { |hash| hash['avatarimages'] }
  end

  test 'is bookmarked should be correct' do
    card = create(:card, author: create(:user, organization: @org))
    get :show, id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    refute resp['is_bookmarked']

    create(:bookmark, bookmarkable: card, user: @user)
    card.reload
    get :show, id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert resp['is_bookmarked']
  end

  test 'mark as completed should be retained' do
    card = create(:card, author: @user)
    get :show, id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    refute resp['completion_state']

    create(:user_content_completion, completable: card, user: @user, state: 'completed')
    card.reload
    get :show, id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal 'COMPLETED', resp['completion_state']
  end

  test 'rating keys should be correct' do
    card = create(:card, author: @user)
    users = create_list(:user, 3, organization: @org)
    users.each_with_index do |user, i|
      TestAfterCommit.with_commits(true) { create(:cards_rating, card: card, user: user, rating: i+1) }
    end

    # Current User has not yet rated
    get :show, id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal "2.0", resp['average_rating']

    # DEPRECATED
    # assert_equal({"1"=>1,"2"=>1,"3"=>1}, resp['all_ratings'])
    # assert_nil resp['user_rating']

    # Current User gives a 2 star rating
    create(:cards_rating, card: card, user: @user, rating: 2)
    card.reload
    get :show, id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal "2.0", resp['average_rating']
    # DEPRECATED
    # assert_equal({"1"=>1,"2"=>2,"3"=>1}, resp['all_ratings'])
    # assert_equal 2, resp['user_rating']
  end

  test 'should show user level' do
    # Skipping because of EP-14189.
    # User level change of BIA is disabled. Flag name: allow-consumer-modify-level
    # Only author or admin will be able to change BIA of a card.
    skip
    card = create(:card, author: create(:user, organization: @org))
    create(:cards_rating, card: card, user: @user, rating: 2, level: 'beginner')
    card.reload

    get :show, id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)

    assert_equal 'beginner', resp['skill_level']
  end

  test 'try to first show global skill_level and then authors level' do
    card = create :card, org: @org, author: @user
    card_rating = create :cards_rating, card: card, level: 'beginner', user: @user
    get :show, id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal 'beginner', resp['skill_level']

    admin = create :user, organization: @org
    admin_role = create :role, name: 'admin', organization: @org

    role_permission = create :role_permission, role: admin_role, name: Permissions::ADMIN_ONLY
    admin_role.role_permissions << role_permission

    admin.roles << admin_role

    TestAfterCommit.with_commits(true) do
      card_rating = create :cards_rating, card: card, level: 'advanced', user: admin
    end

    get :show, id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal 'advanced', resp['skill_level']
  end

  test 'should have filestack information' do
    card = create(:card, author: @user)
    fr = create(:file_resource, attachable: card, filestack: {handle: 'yT4BBpg7T5efeiriQv37'}, filestack_handle: 'yT4BBpg7T5efeiriQv37')

    get :show, id: card.id, format: :json
    resp = get_json_response(response)

    assert_not_empty resp['file_resources'][0]['filestack']
    assert_equal 'yT4BBpg7T5efeiriQv37', resp['file_resources'][0]['filestack']['handle']
  end

  test 'should have user taxonomy topics info' do
    card = create(:card, user_taxonomy_topics: [{'label' => 'label1', 'path' => 'label.one'}], author: @user)
    get :show, id: card.id, format: :json
    resp = get_json_response(response)
    assert_equal [{'label' => 'label1', 'path' => 'label.one'}],  resp['user_taxonomy_topics']
  end

  test 'should include followed author information in card details #followed' do
    organization = @user.organization
    user, followed_author = create_list(:user, 2, organization: organization)
    create(:follow, followable: followed_author, user: user)

    followed_author_card = create(:card, author: followed_author)
    api_v2_sign_in user

    get :show, id: followed_author_card.id, format: :json

    assert_response :ok
    assert get_json_response(response)['author']['is_following']
  end

  test 'should include followed author information in card details #not_followed' do
    organization = @user.organization
    user, another_user = create_list(:user, 2, organization: organization)

    card = create(:card, author: another_user)
    api_v2_sign_in user

    get :show, id: card.id, format: :json

    assert_response :ok
    refute get_json_response(response)['author']['is_following']
  end

  test 'should include assignment details in card details' do
    org = @user.organization
    user, author = create_list(:user, 2, organization: org)
    card = create(:card, author: author)
    team = create(:team, organization: org)
    due_at = Time.now + 3.days
    assignment = create(:assignment, assignee: user, assignable: card, due_at: due_at)
    team_assignment = create(:team_assignment, assignment: assignment, team: team, assignor: author)

    api_v2_sign_in user
    get :show, id: card.id, format: :json

    resp = get_json_response(response)

    assert resp['is_assigned']
    assert_not_empty resp['assignment']
    assert_equal card.id, resp['assignment']['assignable_id']
    assert_equal 'Card', resp['assignment']['assignable_type']
    assert_not_nil resp['assignment']['due_at']
    assert_equal team.id, resp['assignment']['teams'][0]['id']
  end

  test ':api show pack cards should content info about lock status' do
    org = create(:organization)
    user = create(:user, organization: org)
    cover = create(:card, card_type: 'pack', card_subtype: 'simple', author: user)
    card1, card2, card3 = create_list(:card, 3, card_type: 'media', card_subtype: 'text', author: user)
    [card1, card2, card3].each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: card.class.name)
    end
    CardPackRelation.find_by(cover_id: cover.id, from_id: card1.id).update_attributes(locked: true)
    api_v2_sign_in user
    get :show, id: cover.id, format: :json

    resp = get_json_response(response)
    assert resp['pack_cards'][0]['locked']
    assert_equal card1.id, resp['pack_cards'][0]['card_id']
    refute resp['pack_cards'][1]['locked']
  end

  test 'should not return message and title in pack cards if the card belonging to the pathway is private' do
    org = create(:organization)
    user = create(:user, organization: org)
    author = create(:user, organization: org)
    cover = create(:card, card_type: 'pack', card_subtype: 'simple', author: author)
    private_card = create(:card, card_type: 'media', card_subtype: 'text', author: author, is_public: false, message: 'Test private card')
    public_card = create(:card, card_type: 'media', card_subtype: 'text', author: author, is_public: true, message: 'Test public card')
    [private_card, public_card].each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: card.class.name)
    end
    cover.publish!
    api_v2_sign_in user
    get :show, id: cover.id, format: :json

    resp = get_json_response(response)
    assert_nil resp['pack_cards'][0]['message']
    assert_equal public_card.message, resp['pack_cards'][1]['message']
  end

  test 'should return title, message, hidden, state field for pathway in journey' do
    org = create(:organization)
    user = create(:user, organization: org)
    author = create(:user, organization: org)
    pathway_cover = create(:card, card_type: 'pack', card_subtype: 'simple', author: author, title: "pathway title", message: "pathway description")
    journey_cover = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: author)
    public_card = create(:card, card_type: 'media', card_subtype: 'text', author: author, is_public: true)
    CardPackRelation.add(cover_id: pathway_cover.id, add_id: public_card.id, add_type: public_card.class.name)
    pathway_cover.publish!
    JourneyPackRelation.add(cover_id: journey_cover.id, add_id: pathway_cover.id)
    journey_cover.publish!
    api_v2_sign_in user
    get :show, id: journey_cover.id, format: :json

    resp = get_json_response(response)
    assert_equal pathway_cover.title, resp["journey_section"][0]["block_title"]
    assert_equal pathway_cover.message, resp["journey_section"][0]["block_message"]
    assert_equal pathway_cover.hidden, resp["journey_section"][0]["hidden"]
    assert_equal pathway_cover.state, resp["journey_section"][0]["state"]
  end

  test ':api should return can_be_reanswered for poll_card' do
    card_poll = create(:card, author_id: @user.id, card_type: 'poll')
    api_v2_sign_in @user

    get :show, id: card_poll.id, format: :json
    resp = get_json_response(response)
    assert_response :ok
    assert_includes resp.keys, 'can_be_reanswered'
  end

  test ':api should not return can_be_reanswered for other card' do
    card = create(:card, author_id: @user.id, card_type: 'media')
    api_v2_sign_in @user

    get :show, id: card.id, format: :json
    resp = get_json_response(response)
    assert_response :ok
    assert_not_includes resp.keys, 'can_be_reanswered'
  end

  test ':api should return last answer for re-answered' do
    poll = create(:card, card_type: 'poll', is_public: true, author_id: @user.id, can_be_reanswered:true)
    opt1 = create(:quiz_question_option, label: 'opt 1', is_correct: false, quiz_question: poll)
    opt2 = create(:quiz_question_option, label: 'opt 2', is_correct: true, quiz_question: poll)
    user2 = create(:user, organization: @org)
    set_api_request_header user2
    QuizQuestionAttempt.create( quiz_question: poll, user: user2, selections: [opt1.id])
    QuizQuestionAttempt.create( quiz_question: poll, user: user2, selections: [opt2.id])
    get :show, id: poll.id, format: :json
    resp = get_json_response(response)
    assert_response :ok
    assert_equal opt2.id, resp['attempted_option']['id']
  end

  test 'should have view count for cards' do
    Card.any_instance.unstub(:all_time_views_count)

    org = create(:organization)
    user, author = create_list(:user, 2, organization: org)
    card = create(:card, author: author)

    # view calls to influx
    Card.any_instance.expects(:all_time_views_count).returns(10)

    api_v2_sign_in user
    get :show, id: card.id, format: :json

    resp = get_json_response(response)

    # card all view count
    assert_equal 10, resp['views_count']
  end

  test 'should return permission related details along with show_restricted key' do
    org = create(:organization)
    team = create(:team, organization: org)
    channel = create(:channel, organization: org)
    create(:teams_channels_follow, team_id: team.id, channel_id: channel.id)
    user, user1, author = create_list(:user, 3, organization: org)
    create(:teams_user, team_id: team.id, user_id: user1.id)
    card = create(:card, author: author)

    # card has been completed
    create(:user_content_completion, user_id: user.id, completable: card, state: 'completed')

    #permissions
    create(:card_team_permission, card_id: card.id, team_id: team.id)

    api_v2_sign_in user1
    get :show, id: card.id, format: :json

    resp = get_json_response(response)
    assert_includes resp.keys, "teams_permitted"
    assert_includes resp.keys, "users_permitted"
    assert_includes resp.keys, "show_restrict"
    refute resp['show_restrict']
    assert_equal [team.id], resp['teams_permitted'].map{|d| d['id']}
    assert_equal [channel.id], resp['teams_permitted'].map{|d| d['channel_ids']}.first
  end

  # end show
  class CardHelperTests < ActionController::TestCase
    include Api::V2::CardsHelper

    setup do
      @user = create(:user)
      @org = @user.organization
      api_v2_sign_in @user
    end

    test 'should show default image_url field for media-link card with empty string resource image_url' do
      resource_article = create(:resource, type: nil ,url: "https://www.honeybadger.io/",image_url: "" ,user_id: @user.id)
      media_link_card_without_resource_image = create(:card, card_type: "media", card_subtype:"link", resource: resource_article,created_at: Date.today, author_id: @user.id, filestack: [])

      uniq_filestack_hash_object_1 = default_image(media_link_card_without_resource_image, 'resource')
      Filestack::Security.any_instance.stubs(:signed_url).returns(uniq_filestack_hash_object_1)

      get :show, id: media_link_card_without_resource_image.id, format: :json
      resp_1 = get_json_response(response)
      assert_equal uniq_filestack_hash_object_1, resp_1['resource']['image_url']
    end

    test 'should show same image_url field when refreshed for media-link card with no resource image_url' do
      resource_article = create(:resource, type: nil ,url: "https://www.honeybadger.io/",image_url: nil ,user_id: @user.id)
      media_link_card_without_resource_image = create(:card, card_type: "media", card_subtype:"link", resource: resource_article,created_at: Date.today, author_id: @user.id, filestack: [])

      uniq_filestack_hash_object_1 = default_image(media_link_card_without_resource_image, 'resource')
      Filestack::Security.any_instance.stubs(:signed_url).returns(uniq_filestack_hash_object_1)
      get :show, id: media_link_card_without_resource_image.id, format: :json
      resp_1 = get_json_response(response)


      uniq_filestack_hash_object_2 = default_image(media_link_card_without_resource_image, 'resource')
      Filestack::Security.any_instance.stubs(:signed_url).returns(uniq_filestack_hash_object_2)
      get :show, id: media_link_card_without_resource_image.id, format: :json
      resp_2 = get_json_response(response)

      assert_equal resp_1['resource']['image_url'], resp_2['resource']['image_url']
      assert_equal uniq_filestack_hash_object_1, uniq_filestack_hash_object_2
      assert_equal resp_1['resource']['image_url'], uniq_filestack_hash_object_2
      assert_equal resp_1['resource']['image_url'], uniq_filestack_hash_object_1
    end

    test 'should not send filestack field for media-video card with no resource and filestack uploaded' do
      stub_request(:get, "https://cdn.filestackcontent.com/VUhaEubQAG9EXu8O8uGQ")

      filestack_hash = [
        {:filename =>"SampleVideo_1280x720_1mb.mp4",
          :container => "edcast-file-stack-temp",
        :handle =>"VUhaEubQAG9EXu8O8uGQ",
        :mimetype =>"video/mp4",
        :original_path =>"SampleVideo_1280x720_1mb.mp4",
        :key => "MwnWtFbQothoYcbOiHAf_SampleVideo_1280x720_1mb.mp4",
        :size =>1055736,
        :status=>"Stored",
        :thumbnail => "",
        :source =>"local_file_system",
        :url => "https://cdn.filestackcontent.com/VUhaEubQAG9EXu8O8uGQ",
        :upload_id =>"91d4622c52ec677eb93c7f167c978d90",
        :original_file =>
          { :name=>"SampleVideo_1280x720_1mb.mp4",
            :type=>"video/mp4",
            :size=>1055736
            }}]
      FileResource.any_instance.stubs(:valid_mime_type?).returns true

      media_video_card_with_filestack = create(:card, card_type: "media", card_subtype:"video", created_at: Date.today, author_id: @user.id, filestack: filestack_hash)
      get :show, id: media_video_card_with_filestack.id, format: :json
      resp = get_json_response(response)
      assert_equal media_video_card_with_filestack.filestack, [resp['filestack'][0].deep_symbolize_keys]
    end

    test 'should not send image_url field for media-link card with valid resource and filestack empty' do
      resource_article = create(:resource, type: 'Article',url: "https://www.honeybadger.io/", user_id: @user.id)
      media_link_card_with_resource = create(:card, card_type: "media", card_subtype:"link", resource: resource_article, created_at: Date.today, author_id: @user.id, filestack: [])
      get :show, id: media_link_card_with_resource.id, format: :json
      resp = get_json_response(response)
      assert_equal media_link_card_with_resource.resource.image_url, resp['resource']['image_url']
    end

    test 'should show image_url field for media-link card with no resource image_url and filestack empty' do
      resource_article = create(:resource, type: 'Article',url: "https://www.honeybadger.io/",image_url: nil, user_id: @user.id)
      media_link_card_without_resource = create(:card, card_type: "media", card_subtype:"link", resource: resource_article,created_at: Date.today, author_id: @user.id, filestack: [])

      uniq_filestack_hash_object = default_image(media_link_card_without_resource, 'resource')
      Filestack::Security.any_instance.stubs(:signed_url).returns(uniq_filestack_hash_object)

      get :show, id: media_link_card_without_resource.id, format: :json
      resp = get_json_response(response)
      assert_equal uniq_filestack_hash_object, resp['resource']['image_url']
    end

    test 'should not show resource image_url for media-video card with valid resource and filestack empty' do
      resource_video = create(:resource, type: 'Video' , url: "https://www.youtube.com/watch?v=4WiUQtOhfIc",user_id: @user.id)
      media_video_card_with_resource = create(:card, card_type: "media", card_subtype:"video", resource: resource_video, created_at: Date.today - 1.day, author_id: @user.id, filestack: [])
      get :show, id: media_video_card_with_resource.id, format: :json
      resp = get_json_response(response)
      assert_equal media_video_card_with_resource.resource.image_url, resp['resource']['image_url']
    end

    test 'should show random fixed image_url field for media-video card with no resource image url and filestack empty' do
      resource_video = create(:resource, type: 'Video' , url: "https://www.youtube.com/watch?v=4WiUQtOhfIc", image_url: nil,user_id: @user.id)
      media_video_card_without_resource = create(:card, card_type: "media", card_subtype:"video", resource: resource_video,created_at: Date.today, author_id: @user.id, filestack: [])

      uniq_filestack_hash_object = default_image(media_video_card_without_resource, 'resource')
      Filestack::Security.any_instance.stubs(:signed_url).returns(uniq_filestack_hash_object)

      get :show, id: media_video_card_without_resource.id, format: :json
      resp = get_json_response(response)
      assert_equal uniq_filestack_hash_object, resp['resource']['image_url']
    end

    test 'should not show random image_url field for course-link card with valid resource and filestack empty' do
      resource_course = create(:resource, type: 'Course', url: "https://www.pluralsight.com/courses/java-persistence-api-21", user_id: @user.id)
      course_link_card_ecl = create(:card, organization: @org, message: 'ecl_card', card_type: 'course',card_subtype:"link",author_id: nil, ecl_id: "ECL-1234-e456-f345ce", ecl_metadata: {source_display_name: 'source name 1'}, ecl_source_name: 'source name 1', resource:resource_course)
      get :show, id: course_link_card_ecl.id, format: :json
      resp = get_json_response(response)
      assert_equal course_link_card_ecl.resource.image_url, resp['resource']['image_url']
    end

    test 'should show resource image_url field for course-link card with no resource image and filestack empty' do
      resource_course = create(:resource, type: 'Course', url: "https://www.pluralsight.com/courses/java-persistence-api-21", image_url: nil, user_id: @user.id)
      course_link_card_ecl = create(:card, organization: @org, message: 'ecl_card', card_type: 'course',card_subtype:"link",author_id: nil, ecl_id: "ECL-1234-e456-f345ce", ecl_metadata: {source_display_name: 'source name 1'}, ecl_source_name: 'source name 1', resource:resource_course)

      uniq_image_url = default_image(course_link_card_ecl, 'resource')
      Filestack::Security.any_instance.stubs(:signed_url).returns(uniq_image_url)

      get :show, id: course_link_card_ecl.id, format: :json
      resp = get_json_response(response)
      assert_equal uniq_image_url, resp['resource']['image_url']
    end

    test 'should show filestack field for project-text card with no resource and filestack empty' do
      project_text_card = create(:card, organization: @org, author: @user, card_type: 'project',card_subtype: 'text')
      uniq_filestack_hash_object = default_image(project_text_card ,'filestack')
      Filestack::Security.any_instance.stubs(:signed_url).returns(uniq_filestack_hash_object[0][:url])
      get :show, id: project_text_card.id, format: :json
      resp = get_json_response(response)
      uniq_filestack_hash_object = default_image(project_text_card ,'filestack')
      assert_equal uniq_filestack_hash_object, [resp['filestack'][0]]
    end

    test 'should show resource image url field for poll-video card with no resource image and filestack empty' do
      resource_video = create(:resource, type: 'Video' , url: "https://www.youtube.com/watch?v=4WiUQtOhfIc", image_url: nil,user_id: @user.id)
      poll_video_card = create(:card, organization: @org, author: @user, card_type: 'poll',card_subtype: 'video', filestack:[], resource:resource_video)
      uniq_url = default_image(poll_video_card, 'resource')
      Filestack::Security.any_instance.stubs(:signed_url).returns(uniq_url)
      get :show, id: poll_video_card.id, format: :json
      resp = get_json_response(response)
      assert_equal uniq_url, resp['resource']['image_url']
    end

    test 'should show image url field for poll-link card with no resource image and filestack empty' do
      resource_article = create(:resource, type: 'Article',url: "https://www.honeybadger.io/",image_url: nil, user_id: @user.id)
      poll_link_card = create(:card, organization: @org, author: @user, card_type: 'poll',card_subtype: 'link', filestack:[],resource:resource_article)

      uniq_url = default_image(poll_link_card, 'resource')
      Filestack::Security.any_instance.stubs(:signed_url).returns(uniq_url)

      get :show, id: poll_link_card.id, format: :json
      resp = get_json_response(response)
      assert_equal uniq_url, resp['resource']['image_url']
    end

    test 'should show filestack field for pack-simple card and filestack empty' do
      pack_simple_card_with_filestack = create(:card, card_type: 'pack', card_subtype: 'simple', author_id: @user.id, message: 'Pathway card')
      get :show, id: pack_simple_card_with_filestack.id, format: :json
      resp = get_json_response(response)
      uniq_filestack_hash_object = default_image(pack_simple_card_with_filestack, 'filestack')
      assert_equal uniq_filestack_hash_object, [resp['filestack'][0]]
    end

    test 'should not show filestack field for pack-journey card and filestack empty' do
      pack_journey_card = create(:card, card_type: 'pack', card_subtype: 'journey', author_id: @user.id, filestack: [])
      get :show, id: pack_journey_card.id, format: :json
      resp = get_json_response(response)
      assert_equal [], resp['filestack']
    end

    test 'should show filestack field for journey-weekly card and filestack empty' do
      journey_weekly_card = create(:card, card_type: 'journey', card_subtype: 'weekly', author: @user)
      get :show, id: journey_weekly_card.id, format: :json
      resp = get_json_response(response)
      uniq_filestack_hash_object = default_image(journey_weekly_card, 'filestack')
      assert_equal uniq_filestack_hash_object, [resp['filestack'][0]]
    end

    test 'should show filestack field for journey-selfpaced card and filestack empty' do
      journey_selfpaced_card = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: @user, message: 'Journey card')
      get :show, id: journey_selfpaced_card.id, format: :json
      resp = get_json_response(response)
      uniq_filestack_hash_object = default_image(journey_selfpaced_card, 'filestack')
      assert_equal uniq_filestack_hash_object, [resp['filestack'][0]]
    end

    test 'should show filestack field if its user uploaded, and should not return image from backend' do
      stub_request(:get, "https://cdn.filestackcontent.com/sFHJvwZfRzmtca5MoaRH")

      filestack_hash = [
        { :mimetype=>"image/jpeg",
          :size=>121477,
          :source=>"local_file_system",
          :url=>"https://cdn.filestackcontent.com/sFHJvwZfRzmtca5MoaRH",
          :handle=>"sFHJvwZfRzmtca5MoaRH",
          :status=>"Stored"}
      ]
      FileResource.any_instance.stubs(:valid_mime_type?).returns true
      TestAfterCommit.with_commits do
        media_text_card = create(:card, card_type: 'media', card_subtype: 'text', author: @user, filestack:filestack_hash)
        get :show, id: media_text_card.id, format: :json
        resp = get_json_response(response)
        assert_equal  [filestack_hash[0].stringify_keys], resp['filestack']
      end
    end

    test 'should include assignment details in card details' do
      org = @user.organization
      user, author = create_list(:user, 2, organization: org)
      card = create(:card, author: author)
      team = create(:team, organization: org)
      due_at = Time.now + 3.days
      assignment = create(:assignment, assignee: user, assignable: card, due_at: due_at,state: 'dismissed')

      api_v2_sign_in user
      get :show, id: card.id, format: :json

      resp = get_json_response(response)

      refute resp['is_assigned']
    end
  end

  test 'should send restriction related data in card show' do
    card = create(:card, author: @user)
    user = create(:user, organization: @user.organization)
    team = create(:team, organization: @user.organization)

    channels_list = create_list(:channel, 2, organization: @user.organization)
    create(:follow, user_id: user.id, followable: channels_list[1])

    create(:teams_channels_follow, team_id: team.id, channel_id: channels_list[0].id)
    create(:teams_user, team_id: team.id, user_id: user.id)

    create(:card_user_permission, card_id: card.id, user_id: user.id)
    create(:card_team_permission, card_id: card.id, team_id: team.id)

    api_v2_sign_in @user
    get :show, id: card.id, format: :json
    resp = get_json_response(response)

    assert_equal team.id, resp['teams_permitted'][0]['id']
    assert_includes resp['teams_permitted'][0]['channel_ids'], channels_list[0].id
    assert_equal user.id, resp['users_permitted'][0]['id'], user.id
    assert_includes resp['users_permitted'][0]['team_ids'], team.id
    assert_includes resp['users_permitted'][0]['channel_ids'], channels_list[1].id
  end

  test 'should not return is_paid true for premium or subscription cards' do
    user = create(:user, organization: @user.organization)
    card = create(:card, author: user)
    create(:card_metadatum, plan: 'premium', card: card)

    api_v2_sign_in user
    get :show, id: card.id, format: :json
    resp = get_json_response(response)

    refute resp['is_paid']
    assert_equal 'premium', resp['card_metadatum']['plan']
  end

  test 'should not return anonymized users in card response of users_with_access ' do
    org = create(:organization)
    author = create(:user, organization: org)
    author.update(organization_role: 'admin')
    active_user = create_list(:user, 2, organization: org)
    anonymized_users = create_list(:user, 2, organization: org,
                                        is_suspended: true,
                                        is_anonymized: true)
    card = create(:card, author: author)
    card_user_share = create :card_user_share, user: active_user[0], card: card
    card_user_share = create :card_user_share, user: active_user[1], card: card
    card_user_share = create :card_user_share, user: anonymized_users[0], card: card
    card_user_share = create :card_user_share, user: anonymized_users[1], card: card
    card.reload
    api_v2_sign_in author
    get :show, id: card.id, format: :json
    resp = get_json_response(response)
    assert_equal active_user.map(&:id), resp['users_with_access'].map{ |i| i['id'].to_i }
  end

  test 'api should return in assignment assignment_id for team_assignments' do
    org = create(:organization)
    team = create(:team, organization: org)
    author, assignee = create_list(:user, 2, organization: org)
    assignment_card = create(:card, author: author)

    assignment = create(:assignment, assignable: assignment_card, assignee: assignee)
    create(:team_assignment, assignment: assignment, assignor: author, team: team)

    api_v2_sign_in(assignee)

    get :show, id: assignment_card.id, format: :json

    resp = get_json_response(response)['assignment']['teams']
    assert resp.first.key? 'assignment_id'
  end

  test "api should have is_reported key when config is enabled for an org" do
    @controller.stubs(:is_native_app?).returns(true)
    Organization.any_instance.stubs(:reporting_content_enabled?).returns(true)

    org = create(:organization)
    author = create(:user, organization: org)
    card = create(:card, author: author)

    api_v2_sign_in author
    get :show, id: card.id, format: :json
    resp = get_json_response(response)
    assert resp.keys.include?("is_reported")
  end

  test "api should not have is_reported key when config is disabled for an org" do
    @controller.stubs(:is_native_app?).returns(true)
    Organization.any_instance.stubs(:reporting_content_enabled?).returns(false)

    org = create(:organization)
    author = create(:user, organization: org)
    card = create(:card, author: author)

    api_v2_sign_in author
    get :show, id: card.id, format: :json
    resp = get_json_response(response)
    refute resp.keys.include?("is_reported")
  end
  
  test 'should have author name and author_id present for a section present inside a joureny' do
    @org = create(:organization)
    @user = create(:user, organization: @org)

    @journey = create(
      :card, card_type: 'journey', card_subtype: 'self_paced',
      author: @user, organization_id: @org.id
    )
    @section1, @section2, @section3 = create_list(
      :card, 3, hidden: true, state: 'draft', card_type: 'pack',
      card_subtype: 'simple', author: @user, organization_id: @org.id
    )
    @sections = @section1, @section2, @section3
    @journey_pack = @journey, @section1, @section2, @section3
    @card1, @card2, @card3 = create_list(:card, 3, hidden: true, organization: @org, author: @user)
    @cards = @card1, @card2, @card3
    CardPackRelation.add(cover_id: @section1.id, add_id: @card1.id, add_type: @section1.class.name)
    CardPackRelation.add(cover_id: @section2.id, add_id: @card2.id, add_type: @section1.class.name)
    CardPackRelation.add(cover_id: @section3.id, add_id: @card3.id, add_type: @section1.class.name)

    @pathway = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user, organization_id: @org.id)
    @card4 = create(:card, hidden: true, organization: @org, author: @user)
    CardPackRelation.add(cover_id: @pathway.id, add_id: @card4.id, add_type: @pathway.class.name)

    @sections.each do |s|
      JourneyPackRelation.add(cover_id: @journey.id, add_id: s.id)
    end
    api_v2_sign_in @user

    get :show, id: @journey.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal @card1.author_id.to_s, resp['journey_section'][0]['author_id']
    assert_equal @card1.author_name, resp['journey_section'][0]['author_name']
  end
end

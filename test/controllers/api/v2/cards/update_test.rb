require 'test_helper'

class Api::V2::CardsControllerTest < ActionController::TestCase
  include Api::V2::CardsHelper
  self.use_transactional_fixtures = true

  setup do
    stub_video_stream_index_calls
    EdcastActiveJob.stubs :perform_later
    IndexJob.stubs :perform_later

    @user = create(:user)
    @org = @user.organization

    api_v2_sign_in @user
    @url = 'http://url.com'
    stub_request(:get, @url).
          to_return(:status => 200, :body => '<html><head></head><body></body></html>', headers: {})
  end

  def setup_for_index(card, card_reporting)
    WebMock.disable!
    FlagContent::CardReportingSearchService.create_index! force: true
    FlagContent::CardReportingSearchService.new(
      id: card.id,
      reportings: card_reporting,
      card_message: card.title || card.message,
      card_type: card.card_type,
      card_created_at: card.created_at,
      card_author_id: card.author_id,
      card_organization_id: card.organization_id,
      card_deleted_at: card.deleted_at,
      card_source: card.source_type_name).save

    EdcastActiveJob.unstub :perform_later
    CardReportingIndexingJob.unstub :perform_later
    FlagContent::CardReportingSearchService.gateway.refresh_index!
  end

  test 'api should index the card to Card Reporting index' do
    skip #DO NOT MAKE NETWORK CALLS TO ES

    card = create(:card, author_id: @user.id, card_type: 'media')
    card_reporting = create(:card_reporting, user_id: @user.id, card_id: card.id, reason: 'Wwwow')
    setup_for_index(card, card_reporting)
    put :update, id: card.id, card: {message: 'update'}, format: :json
    resp = get_json_response(response)
    FlagContent::CardReportingSearchService.gateway.delete_index!
    WebMock.enable!
    assert_equal "update", resp["message"]
    assert_response :ok
  end

  test 'should send resource image_url if updated card is media_link with no resource_image url & filestack is empty' do
    resource_course = create(:resource, type: 'Course', url: "https://www.pluralsight.com/courses/java-persistence-api-21", image_url: nil, user_id: @user.id)
    course_link_card_ecl = create(:card, organization: @org, message: 'ecl_card', card_type: 'course',card_subtype:"link",author_id: @user.id, ecl_id: "ECL-1234-e456-f345ce", ecl_metadata: {source_display_name: 'source name 1'}, ecl_source_name: 'source name 1', resource:resource_course)
    # calling show api to get the random image for resource.image_url

    uniq_image_url = default_image(course_link_card_ecl, 'resource')
    Filestack::Security.any_instance.stubs(:signed_url).returns(uniq_image_url)

    get :show, id: course_link_card_ecl.id, format: :json
    response_before_update = get_json_response(response)

    assert_equal uniq_image_url, response_before_update['resource']['image_url']

    put :update, id: course_link_card_ecl.id, card: {message: 'update'}, fields: 'resource', format: :json
    course_link_card_ecl.reload
    response_after_update = get_json_response(response)
    assert_equal response_before_update['resource']['image_url'], response_after_update['resource']['image_url']
  end

  test 'should send filestack field for project-text card with no resource and filestack empty' do
    Card.any_instance.stubs(:created_at).returns(Time.new(2018, 12, 07, 12, 10, 07))
    Filestack::Security.any_instance.stubs(:signed_url).returns("https://cdn.filestackcontent.com/fnSCf9HtQf2NUc04R2Vf")

    project_text_card = create(:card, organization: @org, author: @user, card_type: 'project',card_subtype: 'text')
    get :show, id: project_text_card.id, format: :json
    resp = get_json_response(response)

    put :update, id: project_text_card.id, card: {message: 'update'}, fields: 'filestack', format: :json
    project_text_card.reload
    resp = get_json_response(response)
    uniq_filestack_hash_object = default_image(project_text_card, 'filestack')
    assert_equal uniq_filestack_hash_object, [resp['filestack'][0]]
  end

  test 'should send resource image_url for media-video card with valid resource and filestack empty' do
    resource_video = create(:resource, type: 'Video' , url: "https://www.youtube.com/watch?v=4WiUQtOhfIc",image_url: nil,user_id: @user.id)
    media_video_card_with_resource = create(:card, card_type: "media", card_subtype:"video", resource: resource_video, created_at: Date.today - 1.day, author_id: @user.id, filestack: [])
    uniq_url = default_image(media_video_card_with_resource, 'resource')
    Filestack::Security.any_instance.stubs(:signed_url).returns(uniq_url)

    put :update, id: media_video_card_with_resource.id, card: {message: 'update'}, fields: 'resource', format: :json

    resp = get_json_response(response)
    assert_equal uniq_url, resp['resource']['image_url']
  end

#start update
  test 'api should update card with metadata' do
    card = create(:card, author_id: @user.id, card_type: 'media')
    card_metadatum = create(:card_metadatum, card_id: card.id, plan: 'free', level: "beginner")
    card.reload
    put :update, id: card.id, card: {
      message: 'update',
      card_metadatum_attributes:{ id: card.card_metadatum.id, plan: "subscription", level: "advanced" }
    }, fields: 'card_metadatum', format: :json
    resp = get_json_response(response)
    card.reload
    assert_equal "advanced", card.card_metadatum.level
    assert_equal "subscription", card.card_metadatum.plan
    assert_equal "subscription", resp["card_metadatum"]["plan"]
  end

  test 'api should update card with metadatas level with null ' do
    card = create(:card, author_id: @user.id, card_type: 'media')
    card_metadatum = create(:card_metadatum, card_id: card.id, plan: 'free', level: "beginner")
    card.reload
    put :update, id: card.id, card: {
      message: 'update',
      card_metadatum_attributes:{ id: card.card_metadatum.id, level: nil }
    }, fields: 'card_metadatum', format: :json
    resp = get_json_response(response)
    card.reload
    assert_nil card.card_metadatum.level
    assert_nil resp['skill_level']
  end

  test 'api should update cards_rating to null for current_user if metadatas level is null' do
    card = create(:card, author_id: @user.id, card_type: 'media')
    cards_rating = create(:cards_rating, card_id: card.id, user_id: @user.id, rating: 3, level: 'advanced')
    card_metadatum = create(:card_metadatum, card_id: card.id, plan: 'free', level: "beginner")
    card.reload
    put :update, id: card.id, card: {
      message: 'update',
      card_metadatum_attributes:{ id: card.card_metadatum.id, level: nil }
    }, fields: 'card_metadatum', format: :json
    resp = get_json_response(response)
    card.reload
    assert_nil card.card_metadatum.level
    assert_nil card.cards_ratings.first.level
    assert_equal 3, card.cards_ratings.first.rating
    assert_nil resp['skill_level']
  end

  test 'api should update card without metadata' do
    card = create(:card, message: "test", author_id: @user.id, card_type: 'media')
    put :update, id: card.id, card: {message: 'update'}, format: :json
    card.reload
    assert_equal "update", card.message
    resp = get_json_response(response)
    assert_equal "update", resp["message"]
  end

  test 'api should create card metadata with update call' do
    card = create(:card, message: "test", author_id: @user.id, card_type: 'media')
    put :update, id: card.id, card: {
      message: 'update', is_paid: false,
      card_metadatum_attributes: {plan: "subscription", level: "intermediate"}
    }, fields: 'message,card_metadatum', format: :json
    card.reload
    assert_equal "update", card.message
    assert_equal "intermediate", card.card_metadatum.level
    resp = get_json_response(response)
    assert_equal "update", resp["message"]
    assert_equal "subscription", resp["card_metadatum"]["plan"]
  end

  test 'calls LogHistoryJob' do
    card = create(:card, message: "test", author_id: @user.id)
    LogHistoryJob.expects(:perform_later).once
    put :update, id: card.id, card: {message: 'update'}, format: :json
  end

  test 'records tag changes' do
    skip('This will be fixed in Sprint 47')
    card = create(:card, author_id: @user.id)
    tag1, tag2, tag3 = create_list(:tag, 3)
    card.tags << [tag1, tag2]
    card.save

    LogHistoryJob.expects(:perform_later).with(
      has_entry("changes" => {"topics" => ["tag1,tag2", "tag1,tag3"]}.as_json)
    )
    put :update, id: card.id, card: {topics: [tag1.name, tag3.name]}, format: :json
  end

  test 'update without topics - should not log exception' do
    user = create(:user, organization: @org)
    card = create(:card, author_id: user.id, card_type: 'media')

    api_v2_sign_in(user)

    PrefixLogger.any_instance.expects(:error).never
    put :update, id: card.id, card: {message: 'message'}, format: :json
  end

  test ':api only creator and admin should be able to update card' do
    user = create(:user, organization: @org)
    card = create(:card, author_id: user.id, card_type: 'media')

    put :update, id: card.id, card: {message: 'message'}, format: :json
    assert_response :not_found

    admin_user = create(:user, organization_role: 'admin', organization: @org)
    api_v2_sign_in(admin_user)
    put :update, id: card.id, card: {message: 'message'}, format: :json
    assert_response :ok
  end

  test 'admin should be able update draft card' do
    card = create(:card, author_id: @user.id, state: 'draft')

    admin_user = create(:user, organization_role: 'admin', organization: @org)
    api_v2_sign_in(admin_user)

    put :update, id: card.id, card: {message: 'message'}, format: :json
    resp = get_json_response(response)

    assert_response :ok
    assert_equal resp['id'].to_i, card.id
    assert_equal resp['message'], 'message'
  end

  test ':api only creator with #member role can update' do
    organization = @user.organization
    Role.any_instance.unstub :create_role_permissions
    Role.create_standard_roles organization
    @user.roles << organization.roles.find_by_name('member')

    organization.update(enable_role_based_authorization: true)

    card = create(:card, author_id: @user.id, card_type: 'media')

    put :update, id: card.id, card: {message: 'message'}, format: :json
    assert_response :ok
  end

  test ':api only creator with #curator role can update' do
    Organization.any_instance.unstub(:create_or_update_roles)
    organization = @user.organization
    organization.update(enable_role_based_authorization: true)
    organization.run_callbacks(:commit)
    @user.role_ids = [organization.roles.where(name: Role::TYPE_CURATOR).first.id]
    @user.save

    card = create(:card, author_id: @user.id, card_type: 'media')

    put :update, id: card.id, card: {message: 'message'}, format: :json
    assert_response :unauthorized
  end

  test "should update  card details if channel is not changed" do
    card = create(:card, author_id: @user.id, card_type: 'media')
    ch1 = create(:channel, :robotics, organization: @org)
    ch1.authors << @user
    card.channels << ch1
    put :update, id: card.id, card: {message: "new message"}, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal "new message", resp["message"]
  end

  test 'should remove pins of channels  for card' do
    card = create(:card, author_id: @user.id, card_type: 'media')
    ch1 = create(:channel, :robotics, organization: @user.organization)
    ch1.cards << card
    Pin.create(pinnable: ch1, object: card, user: @user)
    assert_equal card.pins.length, 1

    assert_difference -> {Pin.count}, -1 do
      put :update, id: card.id, card: {channel_ids: [], message: "test"}, format: :json
      card = card.reload
      assert_equal card.pins.length, 0
      assert_equal [], card.channel_ids
    end
  end

  test 'create card from virtual card on updating card' do
    ecl_id = 'abc-def-ijk'

    ecl_card = create :card
    ecl = mock
    ecl.expects(:card_from_ecl_id).returns(ecl_card)
    EclApi::CardConnector.expects(:new).with(ecl_id: ecl_id, organization_id: @user.organization_id).returns(ecl)

    put :update, id: "ECL-#{ecl_id}", card: {channel_ids: [], message: 'abcd'}, format: :json

    assert_response :ok
    resp = get_json_response(response)
    assert_equal ecl_card.id.to_s, resp['id']
  end

  test ':api should be able to update only start_time for video stream cards and channels' do
    card = create(:card, author_id: @user.id, card_type: 'video_stream', card_subtype: 'simple')
    vs = create(:iris_video_stream, card: card, status: 'upcoming', creator: @user, organization: @user.organization)
    ch1 = create(:channel, :robotics, organization: @org)
    ch2 = create(:channel, :architecture, organization: @org)
    ch1.authors << @user
    ch2.authors << @user
    title = 'video stream title edited'
    updated_time = (Time.now + 2.hours).utc.strftime('%FT%TZ')

    put :update, id: card.id, card: {channel_ids: [ch1.id, ch2.id], title: title, message: "edited messge", video_stream: {name: title, start_time: updated_time}},
      fields: 'message,card_subtype,card_type,video_stream', format: :json
    card = card.reload
    resp = get_json_response(response)
    assert_equal "edited messge", resp["message"]
    assert_equal "video_stream", resp["card_type"]
    assert_equal "simple", resp["card_subtype"]
    assert_not_equal title, resp["video_stream"]["name"]
    assert_equal 'upcoming', resp["video_stream"]["status"]
    assert_equal DateTime.parse(updated_time), DateTime.parse(resp["video_stream"]["start_time"])
  end

  test 'card_update should be be able repost to a channel from which the card was rejected' do
    card = create(:card, author_id: @user.id)

    curatable_channel = create(:channel, organization: @org, curate_only: true, only_authors_can_post: false)
    create(:follow, followable: curatable_channel, user_id: @user.id)
    non_curatable_channel = create(:channel, organization: @org, curate_only: false, only_authors_can_post: false)
    create(:follow, followable: non_curatable_channel, user_id: @user.id)

    channel_card = create(:channels_card, channel_id: curatable_channel.id, card_id: card.id, state: ChannelsCard::STATE_SKIPPED)

    put :update, id: card.id, card: {channel_ids: [non_curatable_channel.id, curatable_channel.id]}, fields:'non_curated_channel_ids,channel_ids', format: :json

    resp = get_json_response(response)

    assert_includes resp['non_curated_channel_ids'], curatable_channel.id
    assert_includes resp['channel_ids'], non_curatable_channel.id
    assert_equal ChannelsCard::STATE_NEW.to_s, ChannelsCard.find_by(channel_id: curatable_channel.id, card_id: card.id).state
  end

  test 'should be able to update only name for video stream cards' do
    card = create(:card, author_id: @user.id, card_type: 'video_stream', card_subtype: 'simple')
    vs = create(:iris_video_stream, card: card, status: 'upcoming', creator: @user, organization: @user.organization)

    put :update, id: card.id, card: {message: "edited VS messge"}, fields: 'card_type,card_subtype,message,video_stream', format: :json
    card = card.reload
    resp = get_json_response(response)
    assert_equal "edited VS messge", resp["message"]
    assert_equal "video_stream", resp["card_type"]
    assert_equal "simple", resp["card_subtype"]
    assert_equal 'upcoming', resp["video_stream"]["status"]
  end

  test 'should not update card and video_stream with invalid attributes' do
    card = create(:card, author_id: @user.id, card_type: 'video_stream')
    vs = create(:wowza_video_stream, card: card, status: 'upcoming', creator: @user, organization: @user.organization)
    title = 'video stream message edited'

    #wrong attributes
    assert_raise do
      put :update, id: card.id, title: title, card_type: "video_stream", format: :json
    end
  end

  test ':api should be able to update message and writable channels' do
    card = create(:card, author_id: @user.id, card_type: 'media')
    ch1 = create(:channel, :robotics, organization: @org)
    ch2 = create(:channel, :architecture, is_private: true, organization: @org)

    ch1.authors << @user
    edited_message = 'message edited'

    put :update, id: card.id, card: {channel_ids: [ch1.id, ch2.id], message: edited_message}, format: :json
    card.reload

    assert_response :ok
    assert_equal edited_message, card.message
    assert_equal [ch1.id], card.channel_ids

  end

  test ':api should be able to update message and curatable channels to user' do
    card = create(:card, author_id: @user.id, card_type: 'media')
    ch1 = create(:channel, :robotics, organization: @org)
    ch2 = create(:channel, :architecture, is_private: true, organization: @org)

    ch1.curators << @user
    edited_message = 'message edited'

    put :update, id: card.id, card: {channel_ids: [ch1.id, ch2.id], message: edited_message}, format: :json
    card.reload

    assert_response :ok
    assert_equal edited_message, card.message
    assert_equal [ch1.id], card.channel_ids

  end

  test 'should update card if not posting to channel' do
    user = create(:user, organization: @user.organization)
    card = create(:card, author_id: user.id, card_type: 'media')

    put :update, id: card.id, card: {message: "test"}, format: :json
    assert_response 404
  end

  test 'should not update card author #admin' do
    Role.any_instance.unstub :create_role_permissions
    org = create(:organization)
    Role.create_standard_roles(org)

    admin, member = create_list(:user, 2, organization: org)
    admin.roles << org.roles.find_by_name('admin')

    card = create(:card, author: member)

    api_v2_sign_in admin

    assert_no_difference -> {Card.count}, 1 do
      put :update, id: card.id, card: {message: 'some message'}, format: :json
      assert_response :ok
    end

    card.reload
    assert_equal member, card.author
    assert_equal 'some message', card.message
  end

  test ':api should remove channels' do
    card = create(:card, author_id: @user.id, card_type: 'media')
    ch1 = create(:channel, :robotics, organization: @org)

    ch1.authors << @user
    card.channels << ch1

    edited_message = 'message edited'

    put :update, id: card.id, card: {channel_ids: [], message: edited_message}, format: :json

    card.reload
    assert_empty card.channel_ids
    assert_equal edited_message, card.message
  end

  test ':api should not remove channels' do
    card = create(:card, author_id: @user.id, card_type: 'media')
    ch1 = create(:channel, :robotics, organization: @org)

    ch1.authors << @user
    card.channels << ch1

    edited_message = 'message edited'

    put :update, id: card.id, card: { message: edited_message}, format: :json

    card.reload
    assert_not_empty card.channel_ids
    assert_equal edited_message, card.message
  end

  test ':api should remove file resource ids' do
    card = create(:card, author_id: @user.id, card_type: 'media')
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)

    card.file_resources << fr1
    edited_message = 'message edited'

    put :update, id: card.id, card: {file_resource_ids: [], message: edited_message}, format: :json

    card.reload
    assert_empty card.file_resources
    assert_equal edited_message, card.message

    # Since we are not supporting card subtype editing
    # assert_equal 'text', card.card_subtype
  end

  test ':api should not remove file resource ids' do
    card = create(:card, author_id: @user.id, card_type: 'media')
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)

    card.file_resources << fr1
    edited_message = 'message edited'

    put :update, id: card.id, card: { message: edited_message}, format: :json

    card.reload
    assert_not_empty card.file_resources
    assert_equal edited_message, card.message
  end

  # DO NOT REMOVE THIS TEST. THIS TEST IS FAILING AND SUSPECT THAT
  # THERE IS BUG WHEN FILESTACK IMAGE IS REMOVED FROM CARD
  # test ':api should remove filestack files' do
  #   stub_request(:get, "https://cdn.filestackcontent.com/oaXtaDBRrO0oPhOjMyfN")
    # .to_return(
    #   :status => 200,
    #   :body => File.read(Rails.root.join('test/fixtures/images/user2.jpg')),
    #   :headers => {content_type:'image/jpg'}
    # )
  #   card = create(:card, author_id: @user.id, card_type: 'media', filestack:
  #       [{"mimetype"=>"image/png", "size"=>44887, "source"=>"local_file_system", "status"=>"Stored",
  #         "url"=>"https://cdn.filestackcontent.com/oaXtaDBRrO0oPhOjMyfN", "handle"=>"oaXtaDBRrO0oPhOjMyfN"}] )

  #   edited_message = 'message edited'

  #   put :update, id: card.id, card: {filestack: [], message: edited_message, resource_id: nil}, format: :json

  #   card.reload
  #   assert_empty card.filestack
  #   assert_equal edited_message, card.message
  #   assert_equal 'text', card.card_subtype
  # end

  test ':api should not remove filestack files' do
    stub_request(:get, "https://cdn.filestackcontent.com/oaXtaDBRrO0oPhOjMyfN")
    .to_return(
      :status => 200,
      :body => File.read(Rails.root.join('test/fixtures/images/user2.jpg')),
      :headers => {content_type:'image/jpg'}
    )

    card = create(:card, author_id: @user.id, card_type: 'media', filestack:
        [{"mimetype"=>"image/png", "size"=>44887, "source"=>"local_file_system", "status"=>"Stored",
          "url"=>"https://cdn.filestackcontent.com/oaXtaDBRrO0oPhOjMyfN", "handle"=>"oaXtaDBRrO0oPhOjMyfN", }] )

    edited_message = 'message edited'
    put :update, id: card.id, card: { message: edited_message}, format: :json

    card.reload
    assert_not_empty card.filestack
    assert_equal edited_message, card.message
  end

  test 'should not update card type' do
    unedited_message = 'cool article, https://www.medium.com/'
    resource = create(:resource, url: 'https://www.medium.com/')
    card = create(:card, message: unedited_message, author_id: @user.id, card_type: 'media', card_subtype: 'link')
    edited_message = 'check out article, https://www.medium.com/'

    put :update, id: card.id, card: {message: edited_message, resource_id: resource.id}, format: :json
    card = card.reload

    assert_equal edited_message, card.message
    assert_equal 'media', card.card_type
    assert_equal 'link', card.card_subtype
  end

  test 'should update card subtype to text' do
    unedited_message = 'cool article, https://www.medium.com/'
    resource = create(:resource, url: 'https://www.medium.com/', user_id: @user.id)

    card = create(:card, message: unedited_message, author_id: @user.id, card_type: 'media', card_subtype: 'link', resource_id: resource.id)
    edited_message = 'This is a cool text'

    put :update, id: card.id, card: { message: edited_message, resource_id: nil }, format: :json
    card = card.reload

    resp = get_json_response(response)
    assert_equal edited_message, card.message

    assert_equal 'media', card.card_type
    assert_equal 'text', card.card_subtype
  end

  test 'should update card subtype from file resource' do
    skip('DEPRECATED')

    stub_request(:get, "https://cdn.filestackcontent.com/T9owRNBtQvufu8UdI9zT")
    stub_request(:get, "https://cdn.filestackcontent.com/879sKJ0RqaQ5odTvFHNU")
    FileResource.any_instance.stubs(:valid_mime_type?).returns true
    FileResource.any_instance.stubs(:file_type).returns('file')

    card = create(:card, author_id: @user.id, card_type: 'media', filestack:
        [{"mimetype"=>"application/pdf", "size"=>433994, "source"=>"local_file_system", "status"=>"Stored",
          "url"=>"https://cdn.filestackcontent.com/T9owRNBtQvufu8UdI9zT", "handle"=>"T9owRNBtQvufu8UdI9zT", }] )

    assert_equal 'media', card.card_type
    assert_equal 'file', card.card_subtype_before_type_cast, card.card_subtype

    FileResource.any_instance.stubs(:file_type).returns('video')

    put :update, id: card.id, card: { card_subtype: "video", filestack:
        [{"mimetype"=>"video/mp4", "size"=>2107842, "source"=>"local_file_system", "status"=>"Stored",
          "url"=>"https://cdn.filestackcontent.com/879sKJ0RqaQ5odTvFHNU", "handle"=>"879sKJ0RqaQ5odTvFHNU" }] }, format: :json

    card = card.reload

    assert_equal 'media', card.card_type
    assert_equal 'video', card.card_subtype_before_type_cast, card.card_subtype

    assert_equal 1, card.file_resources.count
  end

  test 'should create or update card with correct subtype when uploading file and thumbnail image' do
    stub_request(:get, "https://cdn.filestackcontent.com/T9owRNBtQvufu8UdI9zT")
    stub_request(:get, "https://cdn.filestackcontent.com/oaXtaDBRrO0oPhOjMyfN")
    FileResource.any_instance.stubs(:valid_mime_type?).returns true
    FileResource.any_instance.stubs(:file_type).returns('file')

    card = create(:card, author_id: @user.id, card_type: 'media', card_subtype: 'file', filestack:
        [{"mimetype"=>"application/pdf", "size"=>433994, "source"=>"local_file_system", "status"=>"Stored",
          "url"=>"https://cdn.filestackcontent.com/T9owRNBtQvufu8UdI9zT", "handle"=>"T9owRNBtQvufu8UdI9zT"},
         {"mimetype"=>"image/png", "size"=>44887, "source"=>"local_file_system", "status"=>"Stored",
          "url"=>"https://cdn.filestackcontent.com/oaXtaDBRrO0oPhOjMyfN", "handle"=>"oaXtaDBRrO0oPhOjMyfN"}])

    assert_equal 'media', card.card_type
    assert_equal 'file', card.card_subtype
    assert_empty card.file_resources
  end

  test 'card subtype should not be changed after updating text card with an image' do
    stub_request(:get, "https://cdn.filestackcontent.com/oaXtaDBRrO0oPhOjMyfN")
    .to_return(
      :status => 200,
      :body => File.read(Rails.root.join('test/fixtures/images/user2.jpg')),
      :headers => {content_type:'image/jpg'}
    )
    card = create(:card, author_id: @user.id, card_type: 'media', card_subtype: 'text')

    assert_equal 'media', card.card_type
    assert_equal 'text', card.card_subtype

    ProcessFilestackImagesJob.expects(:perform_later).with(card.id)
    put :update, id: card.id, card: { filestack:
      [{"mimetype"=>"image/png", "size"=>44887, "source"=>"local_file_system", "status"=>"Stored",
        "url"=>"https://cdn.filestackcontent.com/oaXtaDBRrO0oPhOjMyfN", "handle"=>"oaXtaDBRrO0oPhOjMyfN", }]  }, format: :json

    card = card.reload

    assert_equal 'media', card.card_type
    assert_equal 'text', card.card_subtype
    assert_empty card.file_resources
  end

  test 'should return card_subtype as video along with the video thumbnail image' do
    FileResource.any_instance.stubs(:file_type).returns('video')
    FileResource.any_instance.stubs(:valid_mime_type?).returns true
    stub_request(:get, "https://cdn.filestackcontent.com/879sKJ0RqaQ5odTvFHNU")
    stub_request(:get, "https://cdn.filestackcontent.com/oaXtaDBRrO0oPhOjMyfN")
    card = create(:card, author_id: @user.id, card_type: 'media', card_subtype: 'video', filestack:
        [{"mimetype"=>"video/mp4", "size"=>2107842, "source"=>"local_file_system", "status"=>"Stored",
          "url"=>"https://cdn.filestackcontent.com/879sKJ0RqaQ5odTvFHNU", "handle"=>"879sKJ0RqaQ5odTvFHNU" }] )

    assert_equal 'media', card.card_type
    assert_equal 'video', card.card_subtype_before_type_cast, card.card_subtype
    assert_empty card.file_resources

    put :update, id: card.id, card: { card_subtype: "video", filestack:
        [{"mimetype"=>"video/mp4", "size"=>2107842, "source"=>"local_file_system", "status"=>"Stored",
          "url"=>"https://cdn.filestackcontent.com/879sKJ0RqaQ5odTvFHNU", "handle"=>"879sKJ0RqaQ5odTvFHNU" },
        {"mimetype"=>"image/png", "size"=>44887, "source"=>"local_file_system", "status"=>"Stored",
          "url"=>"https://cdn.filestackcontent.com/oaXtaDBRrO0oPhOjMyfN", "handle"=>"oaXtaDBRrO0oPhOjMyfN", }
          ] }, format: :json

    card = card.reload

    assert_equal 'media', card.card_type
    assert_equal 'video', card.card_subtype_before_type_cast, card.card_subtype

    assert_empty card.file_resources
  end

  # poll
  test ':api update action should add, update and delete option(s) to the poll card' do
    now = Time.now - 2.hours
    Time.stubs(:now).returns(now)

    card = create(:card, card_type: 'poll', is_public: true, author_id: @user.id)
    opt1 = create(:quiz_question_option, label: 'opt 1', quiz_question: card)
    opt2 = create(:quiz_question_option, label: 'opt 2', quiz_question: card)
    # card.quiz_question_options << [opt1, opt2]

    updated_message = "update poll card title"
    assert_equal now.to_i, card.published_at.to_i
    Time.unstub(:now)

    # update options
    patch :update, id: card.id, card: {message: updated_message, options: [{id: opt1.id, label: 'opt1 modified'}, {id: opt2.id, label: 'opt2'}]}, format: :json
    assert_response :ok
    card.reload

    assert_equal 'opt1 modified', card.quiz_question_options.as_json.first['label']

    # add options
    patch :update, id: card.id, card: {message: updated_message, options: [{id: opt1.id, label: 'opt1'}, {id: opt2.id, label: 'opt2'}, {label: 'yes'}, {label: 'no'}]}, format: :json
    assert_response :ok

    card.reload
    assert_equal updated_message, card.message
    assert_equal 4, card.quiz_question_options.count

    # delete options
    card.reload
    qqo1, qqo2 = card.quiz_question_options.last(2)

    patch :update, id: card.id, card: {message: updated_message, options: [{id: opt1.id, label: 'opt 1'}, {id: opt2.id, label: 'opt 2'}, {id: qqo1.id, _destroy: true}, {id: qqo2.id, _destroy: true}]}, format: :json
    assert_response :ok

    card.reload
    assert_equal 2, card.quiz_question_options.count
    assert_equal 'opt 1', card.quiz_question_options[0]['label']
    assert_equal 'opt 2', card.quiz_question_options[1]['label']

    # should not update for invalid blank options
    patch :update, id: card.id, card: {message: updated_message, options: [{id: opt1.id, label: '  '}, {id: opt2.id, label: 'opt2'}]}, format: :json

    assert_response :unprocessable_entity
    assert_equal "Label can't be blank", get_json_response(response)['message']

    card.reload
    assert_equal 2, card.quiz_question_options.count
    assert_equal 'opt 1', card.quiz_question_options[0]['label']
    assert_equal 'opt 2', card.quiz_question_options[1]['label']
  end

  test 'video stream card subtype on create and edit' do
    message = "this is a VideoStream"
    params = {
      message: message,
      card_subtype: 'role_play',

      video_stream: {
        start_time: Time.now.utc.strftime('%FT%TZ'),
        status: 'live'
      }
    }

    assert_difference ->{Card.count} do
      assert_difference ->{VideoStream.count} do
        post :create, card: params, format: :json
        assert_response :ok
      end
    end

    vs = VideoStream.last
    card = Card.last
    assert_equal 'role_play', card.card_subtype

    put :update, id: card.id, card: { message: 'this is a new video stream'}, format: :json

    card.reload
    assert_equal 'role_play', card.card_subtype
    assert_equal "live", vs.status
    assert_equal 'this is a new video stream', card.message
  end

  test "should return 200 if card found to update subscription" do
    resource = create(:resource, url: 'https://www.medium.com/', user_id: @user.id)
    card = create(:card, author_id: @user.id, card_type: 'course', card_subtype: 'link', resource_id: resource.id)
    request.headers['X-API-KEY'] = "test-sample-key"
    request.headers['X-EMAIL'] = @user.email

    @controller.stubs(:get_source_organization).returns(@org)
    LrsAssignmentService.any_instance.stubs(:run)

    put :course_status, {url: "https://www.medium.com/", status: "started"}
    assert_equal 200, response.status
  end

  test "should return 404 if no card found to update subscription" do
    request.headers['X-API-KEY'] = "test-sample-key"
    request.headers['X-EMAIL'] = @user.email
    @controller.stubs(:get_source_organization).returns(@org)

    put :course_status, {url: "https://www.example.com/",status: "start"}
    resp = get_json_response(response)

    assert_equal 404, response.status
    assert_equal resp["message"], "card not found with given url"
  end

  test 'should allow to update markdown urls' do
    card = create(:card, author_id: @user.id, is_public: false, message: "https://www.google.com")

    message = "https://www.google.com [markdown link](https://www.google.com)"
    put :update, id: card.id, card: {message: message}, format: :json

    assert_response :ok
    resp = get_json_response(response)
    assert_equal message, resp["message"]
  end

  test 'should allow to update card restriction permission' do
    card = create(:card, author_id: @user.id, is_public: false)
    user_list = create_list(:user, 2, organization: @user.organization)
    card_user_permission = create(:card_user_permission, card_id: card.id, user_id: user_list[0].id)

    assert_includes card.users_with_permission_ids, user_list[0].id

    put :update, id: card.id, card: {users_with_permission_ids: [user_list[1].id]}, format: :json

    assert_response :ok
    card.reload
    assert_equal user_list[1].id, card.users_with_permission_ids[0]
    assert_not_includes card.users_with_permission_ids, user_list[0].id
  end

  test 'should allow to remove card restriction permission' do
    card = create(:card, author_id: @user.id, is_public: false)
    user = create(:user, organization: @user.organization)
    team = create(:team, organization: @user.organization)

    create(:card_user_permission, card_id: card.id, user_id: user.id)
    create(:card_team_permission, card_id: card.id, team_id: team.id)

    assert_includes card.users_with_permission_ids, user.id

    put :update, id: card.id, card: {users_with_permission_ids: [], teams_with_permission_ids: []}, format: :json

    assert_response :ok
    card.reload
    assert_empty card.users_with_permission_ids
    assert_empty card.teams_with_permission_ids
  end
  # poll ends
  # end update

  test "should not remove reference to channel the card is posted in where channel is not writable for the current user" do
    # use cases:
    # card existing in non writable channel
    # 1. post to writable channel: result -> card exists in both writable and non writable channel
    # 2. post to another non writable channel -> card continues to exist in the existing channels but does not get posted
    # to non writable channel
    non_writable_channels = create_list(:channel, 2, organization: @user.organization)
    writable_channel = create(:channel, organization: @user.organization, only_authors_can_post: false)
    curatable_channel = create(:channel, organization: @user.organization, curate_only: true, only_authors_can_post: false)

    non_writable_channels[0].add_followers([@user])
    writable_channel.add_followers([@user])
    curatable_channel.add_followers([@user])

    card = create(:card, author: create(:user, organization: @user.organization))

    create(:channels_card, channel_id: non_writable_channels[0].id, card_id: card.id)

    # user can post to writable_channel, curatable_channel only
    api_v2_sign_in @user
    put :update, id: card.id, card: {channel_ids: [non_writable_channels[0].id, non_writable_channels[1].id, curatable_channel.id,
      writable_channel.id]}, fields: 'channel_ids', format: :json

    # non_writable_channels[0], curatable_channel, writable_channel should be present
    resp = get_json_response(response)

    # posting to a non_writable_channel the card was not posted in should not work
    assert_not_includes resp['channel_ids'], non_writable_channels[1].id
    assert_same_elements [non_writable_channels[0].id, curatable_channel.id, writable_channel.id], resp['channel_ids']
  end

  test ':api should be able to promote card' do
    card = create(:card, author_id: @user.id)
    put :promote, id: card.id, format: :json
    card.reload
    assert_response :ok
    assert card.is_official
  end

  test ':api should return unprocessable_entity if card already promoted' do
    card = create(:card, author_id: @user.id, is_official: true)
    put :promote, id: card.id, format: :json
    assert_response :unprocessable_entity
  end

  test ':api should be able to unpromote card' do
    card = create(:card, author_id: @user.id, is_official: true)
    put :unpromote, id: card.id, format: :json
    card.reload
    assert_response :ok
    assert !card.is_official
  end

  test ':api should return unprocessable_entity if card is not promoted' do
    card = create(:card, author_id: @user.id, is_official: false)
    put :unpromote, id: card.id, format: :json
    assert_response :unprocessable_entity
  end

  test ':api should update journey card without root relations element' do
    @journey = create(
      :card, card_type: 'journey', card_subtype: 'self_paced', author: @user
    )
    JourneyPackRelation.where(cover_id: @journey.id).delete_all

    put :update, id: @journey.id, card: { title: 'new_title' }, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal 'new_title', resp['title']
  end

  test ':api should update journey cards start date' do
    org = create(:organization)
    @user = create(:user, organization: org)
    setup_weekly_journey_card(@user)

    Card.any_instance.stubs(:remove_card_from_queue)

    @cover = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user)
    @card1, @card2, @card3 = create_list(:card, 3, author: @user)
    @cards = @cover, @card1, @card2, @card3

    [@card1, @card2, @card3].each do |card|
      CardPackRelation.add(cover_id: @cover.id, add_id: card.id, add_type: card.class.name)
    end

    api_v2_sign_in @user

    Card.any_instance.stubs(:remove_from_queue_and_reenqueue)

    put :update, id: @journey_weekly.id, card: { journey_start_date: 1549737000 }, format: :json
    assert_equal Time.zone.at(1549737000), JourneyPackRelation.find_by(cover: @journey_weekly).start_date
  end

  test ':api member should be able to update author of thier own cards' do
    organization = @user.organization
    Role.any_instance.unstub :create_role_permissions
    Role.create_standard_roles organization
    member_role = organization.roles.find_by_name('member')
    @user.roles << member_role
    stub_request(:post, "http://localhost:9200/card_history_test/card_history")
        .to_return(status: 200, body: "", headers: {})

    user1 = create :user, organization: organization
    card = create(:card, author_id: @user.id, organization: organization)
    put :transfer_ownership, id: card.id, user_id: user1.id, format: :json
    card.reload
    assert_response :ok
    assert_equal card.author_id, user1.id
  end

  test ':api member should be able to update author of their own cards' do
    organization = @user.organization
    Role.any_instance.unstub :create_role_permissions
    Role.create_standard_roles organization
    member_role = organization.roles.find_by_name('member')
    user = create :user, organization: organization, organization_role: "member"
    user.roles << member_role
    user1 = create :user, organization: organization
    card = create(:card, author_id: user.id, organization: organization)
    stub_request(:post, "http://localhost:9200/card_history_test/card_history")
         .to_return(status: 200, body: "", headers: {})
    LogHistoryJob.expects(:perform_later).once

    api_v2_sign_in user
    put :transfer_ownership, id: card.id, user_id: user1.id, format: :json
    card.reload
    assert_response :ok
    assert_equal card.author_id, user1.id
  end

  test ':api admin should be able to update author any cards' do
    organization = @user.organization
    Role.any_instance.unstub :create_role_permissions
    Role.create_standard_roles organization
    admin_role = organization.roles.find_by_name('admin')
    admin_user = create :user, organization: organization, organization_role: "admin"
    admin_user.roles << admin_role
    stub_request(:post, "http://localhost:9200/card_history_test/card_history")
        .to_return(status: 200, body: "", headers: {})

    user = create :user, organization: organization
    user1 = create :user, organization: organization
    card = create(:card, author_id: user.id, organization: organization)

    api_v2_sign_in admin_user
    put :transfer_ownership, id: card.id, user_id: user1.id, format: :json
    card.reload
    assert_response :ok
    assert_equal card.author_id, user1.id
  end

  test ':api should not update author if the card is paid' do
    organization = @user.organization
    Role.any_instance.unstub :create_role_permissions
    Role.create_standard_roles organization
    member_role = organization.roles.find_by_name('member')
    @user.roles << member_role

    user1 = create :user, organization: @org
    card = create(:card, author_id: @user.id, is_paid: true)

    put :transfer_ownership, id: card.id, user_id: user1.id, format: :json
    resp = get_json_response(response)
    card.reload
    assert_response :unauthorized
    assert_equal "Permission denied - This is paid content", resp['message']
  end

  test ':api should not update author for non-ugc card' do
    organization = @user.organization
    Role.any_instance.unstub :create_role_permissions
    Role.create_standard_roles organization
    admin_role = organization.roles.find_by_name('admin')
    admin_user = create :user, organization: organization, organization_role: "admin"
    admin_user.roles << admin_role

    user1 = create :user, organization: @org
    api_v2_sign_in admin_user
    card = create(:card, author_id: nil, is_paid: true, organization: @org)
    put :transfer_ownership, id: card.id, user_id: user1.id, format: :json
    resp = get_json_response(response)
    card.reload
    assert_response :unauthorized
    assert_equal "Permission denied - This is NON-UGC content", resp['message']
  end

  test ':api update should be able to update author of cards' do
    organization = @user.organization
    Role.any_instance.unstub :create_role_permissions
    Role.create_standard_roles organization
    user = create :user, organization: organization
    user1 = create :user, organization: organization
    card = create(:card, author_id: user.id, organization: organization)
    admin_role = organization.roles.find_by_name('admin')
    admin_user = create :user, organization: organization, organization_role: "admin"
    admin_user.roles << admin_role

    api_v2_sign_in admin_user
    put :update, id: card.id, card: {author_id: user1.id}, format: :json
    card.reload
    assert_response :ok
    assert_equal card.author_id, user1.id
  end

  test ':api should not removed teams_cards after card update' do
    team = create(:team, organization: @org)
    card = create(:card, organization: @org, author: @user)
    shared = TeamsCard.create(
      team_id: team.id, card_id: card.id,
      type: 'SharedCard', user_id: card.author.id
    )

    # see EP-18021 for details:
    # key -> `team_ids` : lack of the key in the response and empty array value is interpreted as `nil`,
    #    but semantically, it has difference
    # to remove a card from teams need to pass the key explicitly
    put :update, id: card.id, card: { message: 'new_message' }, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal 'new_message', resp['message']
    assert TeamsCard.exists?(id: shared.id)
  end

  test ':api should return teams in response' do
    team = create(:team, organization: @org)
    card = create(:card, organization: @org, author: @user)
    shared = TeamsCard.create(
      team_id: team.id, card_id: card.id,
      type: 'SharedCard', user_id: card.author.id
    )

    put :update, id: card.id, card: { message: 'new_message' }, fields: 'teams,message', format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal 'new_message', resp['message']
    assert_equal team.id, resp['teams'][0]['id']
  end

  test ':api should return channels in response' do
    card = create(:card, author_id: @user.id, card_type: 'media')
    ch1 = create(:channel, :robotics, organization: @org)
    ch1.authors << @user
    card.channels << ch1
    put :update, id: card.id, card: {message: "new message"}, fields: 'channels,message', format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal "new message", resp["message"]
    assert_equal ch1.id, resp['channels'][0]['id']
  end

  test ':api should remove teams_cards after card update' do
    team = create(:team, organization: @org)
    card = create(:card, organization: @org, author: @user)
    shared = TeamsCard.create(
      team_id: team.id, card_id: card.id,
      type: 'SharedCard', user_id: card.author.id
    )

    put :update, id: card.id, card: { message: 'new_message', team_ids: nil }, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal 'new_message', resp['message']
    refute TeamsCard.exists?(id: shared.id)
  end

  test ':api should strip off security attributes from filestack params' do
    card = create(:card, organization: @org, author: @user)

    put :update, id: card.id, card: {"filestack" => [{"mimetype"=>"image/png", "size"=>44887, "source"=>"local_file_system", "status"=>"Stored", "url"=>"https://cdn.filestackcontent.com/security=p:123456,s:987654/oaXtaDBRrO0oPhOjMyfN", "handle"=>"oaXtaDBRrO0oPhOjMyfN" }] }, format: :json
    assert_response :ok
    assert_equal "https://cdn.filestackcontent.com/oaXtaDBRrO0oPhOjMyfN", card.reload.filestack[0]["url"]
  end
end

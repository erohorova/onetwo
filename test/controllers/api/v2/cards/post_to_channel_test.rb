require 'test_helper'

class Api::V2::CardsControllerTest < ActionController::TestCase
  self.use_transactional_fixtures = true

  setup do
    @user = create(:user)
    @org = @user.organization

    api_v2_sign_in @user
  end

  test "should not call card.save" do
    ch1 = create(:channel, organization: @org)
    author = create(:user, organization: @org)
    card = create(:card, message: "test", title: "test title", card_type: 'media', author: author)

    card.expects(:save).never

    post :post_to_channel, id: card.id, card: {channel_ids: [ch1.id]}, format: :json
  end


  test "should not post to channel if user is not related to channel" do
    ch1 = create(:channel, organization: @org)
    author = create(:user, organization: @org)
    card = create(:card, message: "test", title: "test title", card_type: 'media', author: author)

    post :post_to_channel, id: card.id, card: {channel_ids: [ch1.id]}, format: :json

    assert_response :ok
    assert_equal 0, ChannelsCard.where(channel_id: ch1.id, card_id: card.id).count
  end

  #follower
  test "post card to channel when channel is open" do
    ch1 = create(:channel, organization: @org, is_open: true)
    create(:follow, followable: ch1, user_id: @user.id)
    author = create(:user, organization: @org)
    card = create(:card, message: "test", title: "test title", card_type: 'media', author: author)

    post :post_to_channel, id: card.id, card: {channel_ids: [ch1.id]}, format: :json

    assert_response :ok
    assert_equal 1, ChannelsCard.where(channel_id: ch1.id, card_id: card.id).count
    assert_equal ChannelsCard::STATE_CURATED.to_s, ChannelsCard.where(channel_id: ch1.id, card_id: card.id).first.state

    resp = get_json_response(response)
    assert_includes resp["channel_ids"], ch1.id
  end

  test "should not post card to channel when only_authors_can_post is true" do
    ch1 = create(:channel, organization: @org, only_authors_can_post: true)
    create(:follow, followable: ch1, user_id: @user.id)
    author = create(:user, organization: @org)
    card = create(:card, message: "test", title: "test title", card_type: 'media', author: author)

    post :post_to_channel, id: card.id, card: {channel_ids: [ch1.id]}, format: :json

    assert_response :ok
    assert_equal 0, ChannelsCard.where(channel_id: ch1.id, card_id: card.id).count
  end


  test "post card to channel when curate_only and curate_ugc is on" do
    ch1 = create(:channel, organization: @org, curate_only: true, curate_ugc: true, only_authors_can_post: false)
    create(:follow, followable: ch1, user_id: @user.id)
    author = create(:user, organization: @org)
    card = create(:card, message: "test", title: "test title", card_type: 'media', author: author)

    post :post_to_channel, id: card.id, card: {channel_ids: [ch1.id]}, format: :json

    assert_response :ok
    assert_equal 1, ChannelsCard.where(channel_id: ch1.id, card_id: card.id).count
    assert_equal ChannelsCard::STATE_NEW.to_s, ChannelsCard.where(channel_id: ch1.id, card_id: card.id).first.state

    resp = get_json_response(response)
    assert_includes resp["non_curated_channel_ids"], ch1.id
  end

  test "post card to channel when curatable but curate_ugc is off" do
    ch1 = create(:channel, organization: @org, curate_only: true, curate_ugc: false, only_authors_can_post: false)
    create(:follow, followable: ch1, user_id: @user.id)
    author = create(:user, organization: @org)
    card = create(:card, message: "test", title: "test title", card_type: 'media', author: author)

    post :post_to_channel, id: card.id, card: {channel_ids: [ch1.id]}, format: :json

    assert_response :ok
    assert_equal 1, ChannelsCard.where(channel_id: ch1.id, card_id: card.id).count
    assert_equal ChannelsCard::STATE_CURATED.to_s, ChannelsCard.where(channel_id: ch1.id, card_id: card.id).first.state

    resp = get_json_response(response)
    assert_includes resp["channel_ids"], ch1.id
  end

  #curator
  test "post curated card to channel when not curatable and only_authors_can_post true" do
    ch1 = create(:channel, organization: @org, only_authors_can_post: true)
    create(:follow, followable: ch1, user_id: @user.id)
    create(:channels_curator, channel_id: ch1.id, user_id: @user.id)
    author = create(:user, organization: @org)
    card = create(:card, message: "test", title: "test title", card_type: 'media', author: author)

    post :post_to_channel, id: card.id, card: {channel_ids: [ch1.id]}, format: :json

    assert_response :ok
    assert_equal 1, ChannelsCard.where(channel_id: ch1.id, card_id: card.id).count
    assert_equal ChannelsCard::STATE_CURATED.to_s, ChannelsCard.where(channel_id: ch1.id, card_id: card.id).first.state

    resp = get_json_response(response)
    assert_includes resp["channel_ids"], ch1.id
  end

  #channel_author
  test 'post curated card to channel when user is channel author' do
    ch1 = create(:channel, organization: @org, curate_only: true, curate_ugc: true)
    ch1.authors << @user
    user = create(:user, organization: @org)
    card = create(:card, message: 'test', title: 'test title', card_type: 'media', author: user)

    post :post_to_channel, id: card.id, card: {channel_ids: [ch1.id]}, format: :json

    channel_card_state = ChannelsCard.find_by(channel_id: ch1.id, card_id: card.id).state

    assert_response :ok
    resp = get_json_response(response)
    assert_includes resp['channel_ids'], ch1.id
    assert_equal 1, ChannelsCard.where(channel_id: ch1.id, card_id: card.id).count
    assert_equal ChannelsCard::STATE_CURATED.to_s, channel_card_state
  end

  #org_admin
  test "card requires curation when channel curatable and user is org admin but not channel author/curator" do
    ch1 = create(:channel, organization: @org, curate_only: true, curate_ugc: true)
    user = create(:user, organization_role: 'admin', organization: @org)
    create(:follow, followable: ch1, user_id: @user.id)
    card = create(:card, message: "test", title: "test title", card_type: 'media', author: user)

    api_v2_sign_in(user)
    post :post_to_channel, id: card.id, card: {channel_ids: [ch1.id]}, format: :json

    assert_response :ok
    assert_equal 1, ChannelsCard.where(channel_id: ch1.id, card_id: card.id).count
    assert_equal ChannelsCard::STATE_NEW.to_s, ChannelsCard.where(channel_id: ch1.id, card_id: card.id).first.state

    resp = get_json_response(response)
    assert_includes resp["non_curated_channel_ids"], ch1.id
  end

  #repost
  test 'post_to_channel should be be able repost to a channel from which the card was rejected' do
    card = create(:card, author_id: @user.id, organization: @org)

    curatable_channel = create(:channel, organization: @org, curate_only: true, only_authors_can_post: false)
    create(:follow, followable: curatable_channel, user_id: @user.id)

    channel_card = create(:channels_card, channel_id: curatable_channel.id, card_id: card.id, state: ChannelsCard::STATE_SKIPPED)

    put :post_to_channel, id: card.id, card: {channel_ids: [curatable_channel.id]}, format: :json

    resp = get_json_response(response)

    assert_response :ok
    assert_includes resp['non_curated_channel_ids'], curatable_channel.id
    assert_equal ChannelsCard::STATE_NEW.to_s, ChannelsCard.find_by(channel_id: curatable_channel.id, card_id: card.id).state
  end

  #some_cases
  test "card posted in group, then posted to channel, should continue to work for both" do
    ch1 = create(:channel, organization: @org, curate_only: true, curate_ugc: true)
    ch1.authors << @user
    user = create(:user, organization: @org)
    card = create(:card, message: "test", title: "test title", card_type: 'media', author: user)
    team = create(:team, organization: @org)
    teams_card = create(:teams_card, team_id: team.id, card_id: card.id, type: "SharedCard")

    post :post_to_channel, id: card.id, card: {channel_ids: [ch1.id]}, format: :json

    assert_response :ok
    assert_equal 1, TeamsCard.where(team_id: team.id, card_id: card.id).count
    assert_equal 1, ChannelsCard.where(channel_id: ch1.id, card_id: card.id).count
  end

  test "card posted in channel, then posted to group, should continue to work for both" do
    ch1 = create(:channel, organization: @org, curate_only: true, curate_ugc: true)
    card = create(:card, message: "test", title: "test title", card_type: 'media', author: @user)
    team = create(:team, organization: @org)
    team.add_member @user

    channel_card = create(:channels_card, channel_id: ch1.id, card_id: card.id)

    post :share, id: card.id, team_ids: [team.id], format: :json

    assert_response :ok
    assert_equal 1, TeamsCard.where(team_id: team.id, card_id: card.id).count
    assert_equal 1, ChannelsCard.where(channel_id: ch1.id, card_id: card.id).count
  end

  test "should post card to channel when user follows team, team follows channel" do
    ch1 = create(:channel, organization: @org, curate_only: true, curate_ugc: true, only_authors_can_post: false)
    card = create(:card, message: "test", title: "test title", card_type: 'media', author: @user)
    team = create(:team, organization: @org)

    team.add_member @user
    create(:teams_channels_follow, team_id: team.id, channel_id: ch1.id)

    post :post_to_channel, id: card.id, card: {channel_ids: [ch1.id]}, format: :json

    assert_response :ok
    assert_equal 1, ChannelsCard.where(channel_id: ch1.id, card_id: card.id).count
  end

  test 'should be be able post to a non_curatable channel when already posted to curatable channel' do
    card = create(:card, author_id: @user.id)

    curatable_channel = create(:channel, organization: @org, curate_only: true, only_authors_can_post: false)
    create(:follow, followable: curatable_channel, user_id: @user.id)
    non_curatable_channel = create(:channel, organization: @org, curate_only: false, only_authors_can_post: false)
    create(:follow, followable: non_curatable_channel, user_id: @user.id)

    channel_card = create(:channels_card, channel_id: curatable_channel.id, card_id: card.id, state: ChannelsCard::STATE_NEW)

    post :post_to_channel, id: card.id, card: {channel_ids: [non_curatable_channel.id]}, format: :json

    resp = get_json_response(response)

    assert_includes resp['non_curated_channel_ids'], curatable_channel.id
    assert_includes resp['channel_ids'], non_curatable_channel.id
    assert_equal ChannelsCard::STATE_NEW.to_s, ChannelsCard.find_by(channel_id: curatable_channel.id, card_id: card.id).state
  end

  test 'should be be able post to a curatable channel when already posted to non_curatable channel' do
    card = create(:card, author_id: @user.id)

    curatable_channel = create(:channel, organization: @org, curate_only: true, curate_ugc: true, only_authors_can_post: false)
    create(:follow, followable: curatable_channel, user_id: @user.id)
    non_curatable_channel = create(:channel, organization: @org, curate_ugc: false, curate_only: false, only_authors_can_post: false)
    create(:follow, followable: non_curatable_channel, user_id: @user.id)

    channel_card = create(:channels_card, channel_id: non_curatable_channel.id, card_id: card.id)

    post :post_to_channel, id: card.id, card: {channel_ids: [curatable_channel.id]}, format: :json

    resp = get_json_response(response)

    assert_includes resp['non_curated_channel_ids'], curatable_channel.id
    assert_includes resp['channel_ids'], non_curatable_channel.id
    assert_equal ChannelsCard::STATE_CURATED.to_s, ChannelsCard.find_by(channel_id: non_curatable_channel.id, card_id: card.id).state
  end

  test 'should be be able post to a curatable channel when already posted to curatable channel' do
    card = create(:card, author_id: @user.id)

    curatable_channel1 = create(:channel, organization: @org, curate_only: true, curate_ugc: true, only_authors_can_post: false)
    create(:follow, followable: curatable_channel1, user_id: @user.id)
    curatable_channel2 = create(:channel, organization: @org, curate_only: true, curate_ugc: true, only_authors_can_post: false)
    create(:follow, followable: curatable_channel2, user_id: @user.id)

    channel_card = create(:channels_card, channel_id: curatable_channel1.id, card_id: card.id)

    post :post_to_channel, id: card.id, card: {channel_ids: [curatable_channel2.id]}, format: :json

    resp = get_json_response(response)

    assert_equal resp['non_curated_channel_ids'], [curatable_channel1.id, curatable_channel2.id]
    assert_equal resp['channel_ids'], []
  end

  test 'should be be able post to a non-curatable channel when already posted to non-curatable channel' do
    card = create(:card, author_id: @user.id, is_public: false)

    non_curatable_channel1 = create(:channel, organization: @org, only_authors_can_post: false)
    create(:follow, followable:  non_curatable_channel1, user_id: @user.id)
    non_curatable_channel2 = create(:channel, organization: @org, only_authors_can_post: false)
    create(:follow, followable: non_curatable_channel2, user_id: @user.id)

    channel_card = create(:channels_card, channel_id:  non_curatable_channel1.id, card_id: card.id)

    post :post_to_channel, id: card.id, card: {channel_ids: [non_curatable_channel2.id]}, format: :json

    resp = get_json_response(response)

    assert_equal resp['channel_ids'], [non_curatable_channel1.id, non_curatable_channel2.id]
  end

  test 'should be be able post to a private channel when already posted to private channel' do
    card = create(:card, author_id: @user.id)

    private_channel1 = create(:channel, organization: @org, curate_only: true, curate_ugc: true, only_authors_can_post: false, is_private: true)
    create(:follow, followable: private_channel1, user_id: @user.id)
    private_channel2 = create(:channel, organization: @org, only_authors_can_post: false, is_private: true)
    create(:follow, followable: private_channel2, user_id: @user.id)

    channel_card = create(:channels_card, channel_id: private_channel1.id, card_id: card.id)

    post :post_to_channel, id: card.id, card: {channel_ids: [private_channel2.id]}, format: :json

    resp = get_json_response(response)

    assert_includes resp['non_curated_channel_ids'], private_channel1.id
    assert_includes resp['channel_ids'], private_channel2.id
  end

  test 'should be be able post to a public channel when already posted to private channel' do
    card = create(:card, author_id: @user.id)

    private_channel = create(:channel, organization: @org, curate_only: true, curate_ugc: true, only_authors_can_post: false, is_private: true)
    create(:follow, followable: private_channel, user_id: @user.id)
    public_channel = create(:channel, organization: @org, only_authors_can_post: false, is_open: true)
    create(:follow, followable: public_channel, user_id: @user.id)

    channel_card = create(:channels_card, channel_id: private_channel.id, card_id: card.id)

    post :post_to_channel, id: card.id, card: {channel_ids: [public_channel.id]}, format: :json

    resp = get_json_response(response)

    assert_includes resp['non_curated_channel_ids'], private_channel.id
    assert_includes resp['channel_ids'], public_channel.id
  end

  test 'should be be able post to a private channel when already posted to public channel' do
    card = create(:card, author_id: @user.id)

    private_channel = create(:channel, organization: @org, curate_only: true, curate_ugc: true, only_authors_can_post: false, is_private: true)
    create(:follow, followable: private_channel, user_id: @user.id)
    public_channel = create(:channel, organization: @org, only_authors_can_post: false, is_open: true)
    create(:follow, followable: public_channel, user_id: @user.id)

    channel_card = create(:channels_card, channel_id: public_channel.id, card_id: card.id)

    post :post_to_channel, id: card.id, card: {channel_ids: [private_channel.id]}, format: :json

    resp = get_json_response(response)

    assert_includes resp['non_curated_channel_ids'], private_channel.id
    assert_includes resp['channel_ids'], public_channel.id
  end

  test 'should be be able post to a channel after shared with a user' do
    card = create(:card, author_id: @user.id)

    private_channel = create(:channel, organization: @org, curate_only: true, curate_ugc: true, only_authors_can_post: false, is_private: true)
    create(:follow, followable: private_channel, user_id: @user.id)
    create(:card_user_share, card_id: card.id, user_id: @user.id)

    post :post_to_channel, id: card.id, card: {channel_ids: [private_channel.id]}, format: :json

    resp = get_json_response(response)

    assert_includes resp['non_curated_channel_ids'], private_channel.id
  end

  test 'should have in response info about curation requirements for channels' do
    ch1 = create(
      :channel, organization: @org, only_authors_can_post: false,
      curate_only: true
    )

    ch4, ch5 = create_list(
      :channel, 2, organization: @org,
      curate_ugc: true, curate_only: true,
      is_open: true
    )

    create(:channels_curator, channel_id: ch1.id, user_id: @user.id)
    card = create(
      :card, message: 'test', title: 'test title',
      card_type: 'media', author: @user
    )

    create(:follow, followable: ch1, user_id: @user.id)
    create(:follow, followable: ch4, user_id: @user.id)
    create(:follow, followable: ch5, user_id: @user.id)
    channel_ids = [ch1.id, ch4.id, ch5.id]
    curation_required = [ch4.id, ch5.id]
    curation_not_required = [ch1.id]
    post :post_to_channel, id: card.id, card: {channel_ids: channel_ids }, format: :json
    assert_response :ok
    resp = get_json_response(response)

    assert_same_elements curation_not_required, resp['not_required_curation']
    assert_same_elements curation_required, resp['required_curation']
  end

  test 'should return unauthorized if enable_role_based_authorization enable and user does not have permission MANAGE_CARD' do
    org = create(:organization, enable_role_based_authorization: true)
    user = create(:user, organization: org)
    card = create(:card, card_type: 'media', author: user)
    channel = create(:channel,
      organization: org, only_authors_can_post: false, curate_only: true
    )
    api_v2_sign_in user
    post :post_to_channel, id: card.id, card: {channel_ids: [channel.id] }, format: :json
    assert_response :unauthorized
  end

  test 'should posted to channel if enable_role_based_authorization enable and user has permission MANAGE_CARD' do
    org = create(:organization, enable_role_based_authorization: true)
    user = create(:user, organization: org)
    role = Role.create(name: 'custom', organization_id: @org.id, master_role: false)
    create(
      :role_permission, role: role,
      name: Permissions::MANAGE_CARD
    )
    role.add_user(user)
    card = create(:card, card_type: 'media', author: user)
    channel = create(:channel,
      organization: org, only_authors_can_post: false, curate_only: true
    )
    api_v2_sign_in user
    post :post_to_channel, id: card.id, card: {channel_ids: [channel.id] }, format: :json
    assert_response :ok
  end
end

require 'test_helper'
class Api::V2::CardsControllerTest < ActionController::TestCase
  class TrashTest < ActionController::TestCase
    setup do
      WebMock.disable!
      @org = create(:organization, enable_role_based_authorization: true)
      Role.create_standard_roles @org
      reporter_role = @org.roles.find_by_name('admin')
      create(:role_permission, role: reporter_role, name: Permissions::REPORT_CONTENT)
      create(:role_permission, role: reporter_role, name: Permissions::ADMIN_ONLY)

      @logged_in_user = create(:user, organization: @org)
      @logged_in_user.add_role('admin')
      @card = create(:card, author: @logged_in_user, organization: @org)

      @user = create(:user, organization: @org)
      @card_reportings = @org.users.each do |user|
                          create(:card_reporting, user: user, card: @card)
                        end
      api_v2_sign_in @logged_in_user
    end

    teardown do
      WebMock.enable!
    end

    def setup_for_index
      FlagContent::CardReportingSearchService.create_index! force: true
      FlagContent::CardReportingSearchService.new(
        id: @card.id,
        reportings: @card_reporting,
        card_message: @card.title || @card.message,
        card_type: @card.card_type,
        card_created_at: @card.created_at,
        card_author_id: @card.author_id,
        card_organization_id: @card.organization_id,
        card_deleted_at: @card.deleted_at,
        card_source: @card.source_type_name).save

      EdcastActiveJob.unstub :perform_later
      CardReportingIndexingJob.unstub :perform_later
      FlagContent::CardReportingSearchService.gateway.refresh_index!
    end

    test 'should index the card to CardReporting Index when trash API gets called' do
      skip #DO NOT MAKE NETWORK CALLS
      setup_for_index
      post :trash, id: @card.id, format: :json
      FlagContent::CardReportingSearchService.gateway.delete_index!
      assert_response :ok
    end

    test ':api should trash a content' do
      assert_difference -> { Card.count }, -1 do
        post :trash, id: @card.id, format: :json
        assert_response :ok
      end
    end

    test ':api should not allow to trash a content to non-autorized REPORT_CONTENT permission user' do
      api_v2_sign_in @user
      post :trash, id: @card.id, format: :json
      assert_response 401
    end

    test "should not be able to use trash if not an authorized REPORT_CONTENT permission and RBAC is not enabled" do
      RolePermission.find_by_name(Permissions::REPORT_CONTENT).destroy
      user2 = create(:user,organization: @org)
      user2.add_role('admin')
      trashed_card = create(:card, author: @user, organization: @org)
      create(:card_reporting, user: user2, card:trashed_card)
      api_v2_sign_in user2
      post :trash, id: trashed_card.id, format: :json
      assert_response :unauthorized
    end

    test 'should soft delete related card_reportings' do
      post :trash, id: @card.id, format: :json
      resp = get_json_response(response)

      @card.reload
      assert_empty @card.card_reportings
    end

    test 'should return card not found if card is not present' do
      @card.destroy

      post :trash, id: @card.id, format: :json
      resp = get_json_response(response)

      assert_equal 404, response.status
      assert_equal 'Record not found', resp['message']
    end

    test 'should remove card from pathway when trashed' do
      pathway = create :card, author: @user, organization: @org, card_type: 'pack', card_subtype: 'text'
      CardPackRelation.add(cover_id: pathway.id, add_id: @card.id)
      pathway.reload

      assert_difference -> { CardPackRelation.where(cover_id: pathway.id, deleted: false).count }, -1 do
        post :trash, id: @card.id, format: :json
        assert_response :ok
      end
    end
  end
end

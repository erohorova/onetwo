require 'test_helper'

class Api::V2::CardsControllerTest < ActionController::TestCase
  class UnreportTest < ActionController::TestCase
    setup do
      WebMock.disable!
      @org = create(:organization, enable_role_based_authorization: true)
      Role.create_standard_roles @org
      reporter_role = @org.roles.find_by_name('admin')
      create(:role_permission, role: reporter_role, name: Permissions::REPORT_CONTENT)
      create(:role_permission, role: reporter_role, name: Permissions::ADMIN_ONLY)

      @user1 = create(:user,organization: @org)
      @user1.add_role('admin')
      user2 = create(:user,organization: @org)

      @reported_card = create(:card, author: @user1, organization: @org)
      card_reportings = @org.users.each { |user| create(:card_reporting, user: user, card: @reported_card) }
      api_v2_sign_in @user1
    end

    teardown do
      WebMock.enable!
    end

    def setup_for_index
      FlagContent::CardReportingSearchService.create_index! force: true
      FlagContent::CardReportingSearchService.new(
        id: @reported_card.id,
        reportings: @card_reporting,
        card_message: @reported_card.title || @reported_card.message,
        card_type: @reported_card.card_type,
        card_created_at: @reported_card.created_at,
        card_author_id: @reported_card.author_id,
        card_organization_id: @reported_card.organization_id,
        card_deleted_at: @reported_card.deleted_at,
        card_source: @reported_card.source_type_name).save

      EdcastActiveJob.unstub :perform_later
      CardReportingIndexingJob.unstub :perform_later
      CardReportingTrashingJob.unstub :perform_later
      FlagContent::CardReportingSearchService.gateway.refresh_index!
    end

    test 'should destroy the card which is already indexed when unreported' do
      skip #DO NOT MAKE NETWORK CALLS
      setup_for_index
      put :unreport, id: @reported_card.id, format: :json
      FlagContent::CardReportingSearchService.gateway.delete_index!
      assert_response :ok
    end

    test "should unreport a reported content" do
      assert_difference -> { @reported_card.card_reportings.count }, -2 do
        put :unreport, id: @reported_card.id, format: :json
      end
      assert_equal 2, @reported_card.reload.card_reportings.with_deleted.count
    end

    test "should not be able to use unreport if not authorized for REPOR_CONTENT permission" do
      RolePermission.find_by_name(Permissions::REPORT_CONTENT).destroy
      user2 = create(:user,organization: @org)
      user2.add_role('admin')

      api_v2_sign_in user2
      put :unreport, id: @reported_card.id, format: :json
      assert_response 401
    end

    test "should not be able to use unreport if not an admin and RBAC is not enabled" do
      org = create(:organization)
      user1 = create(:user, organization: org)
      user3 = create(:user, organization: org)
      reported_card = create(:card, author: user1, organization: org)
      card_reporting = create(:card_reporting, user: user3, card:reported_card)
      api_v2_sign_in user3
      put :unreport, id: reported_card.id, format: :json
      assert_response :unauthorized
    end

    test "should recover trashed reported content" do
      @reported_card.card_reportings.update_all(deleted_at: DateTime.now)
      @reported_card.update_column(:deleted_at, Time.now)

      assert_difference -> { Card.count }, 1 do
        put :unreport, id: @reported_card.id, format: :json
      end
    end

    test 'return record not found if card is not present' do
      put :unreport, id: @reported_card.id + 10, format: :json
      resp = get_json_response(response)
      assert_equal 404, response.status
      assert_equal 'Record not found', resp['message']
    end

    test 'return card not found if card_reportings is not present' do
      card = create(:card, author: @user1, organization: @org)
      put :unreport, id: card.id, format: :json
      resp = get_json_response(response)
      assert_equal 404, response.status
      assert_equal 'Card not found', resp['message']
    end

    test 'recover trashed card from pathway' do
      pathway = create :card, author: @user1, organization: @org, card_type: 'pack', card_subtype: 'text'
      pathway.add_card_to_pathway(@reported_card.id, 'Card')

      card_pack_relation = CardPackRelation.where(from_id: @reported_card.id).first
      @reported_card.card_reportings.update_all(deleted_at: DateTime.now)
      @reported_card.update_column(:deleted_at, Time.now)
      card_pack_relation.update_attributes(deleted: true)

      put :unreport, id: @reported_card.id, format: :json
      assert_equal false, card_pack_relation.reload.deleted
      assert_equal nil, @reported_card.reload.deleted_at
    end

    test 'recover content from ecl' do
      EclCardRecoverJob.expects(:perform_later).once
      @reported_card.update_columns(ecl_id: 'ecl-1234', deleted_at: Time.now)
      @reported_card.card_reportings.update_all(deleted_at: DateTime.now)

      put :unreport, id: @reported_card.id, format: :json
    end
  end
end

require 'test_helper'

class Api::V2::CardsControllerTest < ActionController::TestCase
  self.use_transactional_fixtures = true

  test 'should return details required for edit of smartcard' do
    user = create :user
    org = user.organization
    tag = create :tag
    card = create :card, author: user, organization: org, language: 'en'
    card.tags << tag
    shared_card = create :shared_card, card: card, user: user
    api_v2_sign_in user

    expected_keys = %w[
      auto_complete           duration      completed_percentage teams_permitted
      users_permitted         show_restrict file_resources       channel_ids
      non_curated_channel_ids paid_by_user  readable_card_type   channels
      users_with_access       tags          teams                language
      filestack               provider      provider_image
    ]

    get :edit, id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_same_elements expected_keys, resp.keys
    assert_same_elements %w(id label), resp['teams'].first.keys
    assert_same_elements %w(id name), resp['tags'].first.keys
  end

  test 'should return `teams_permitted`' do
    user = create :user
    org = user.organization
    card = create :card, author: user, organization: org, is_public: false
    api_v2_sign_in user

    # create team
    team = create :team, organization: org
    team2 = create :team, organization: org

    # add channels to that team
    channel = create :channel, organization: org
    team.following_channels << channel

    channel2 = create :channel, organization: org
    team2.following_channels << channel2

    # give the permission of the card to both the team
    create :card_team_permission, team: team, card: card
    create :card_team_permission, team: team2, card: card

    get :edit, id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    teams_permitted = resp['teams_permitted']

    assert_equal team.id, teams_permitted.first['id']
    assert_equal team.name, teams_permitted.first['name']
    assert_same_elements [channel.id], teams_permitted.first['channel_ids']

    assert_equal team2.id, teams_permitted.second['id']
    assert_equal team2.name, teams_permitted.second['name']
    assert_same_elements [channel2.id], teams_permitted.second['channel_ids']
  end

  test 'should return `users_permitted`' do
    user = create :user
    org = user.organization
    card = create :card, author: user, organization: org, is_public: false
    api_v2_sign_in user

    # create a user
    user2 = create :user, organization: org

    # users have permission of a card
    create :card_user_permission, user: user, card: card
    create :card_user_permission, user: user2, card: card

    # user directly following channel
    channel = create :channel, organization: org
    create :follow, followable: channel, user: user

    channel2 = create :channel, organization: org
    create :follow, followable: channel2, user: user2

    # add user to a team
    team = create :team, organization: org
    create :teams_user, team: team, user: user

    team2 = create :team, organization: org
    create :teams_user, team: team2, user: user2

    get :edit, id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    users_permitted_response = resp['users_permitted']

    # two users permitted
    assert_equal 2, users_permitted_response.length
    assert_same_elements [user.id, user2.id], users_permitted_response.map { |e| e['id'] }
    assert_same_elements [user.name, user2.name], users_permitted_response.map { |e| e['name'] }
    assert_same_elements [channel.id, channel2.id], users_permitted_response.map { |e| e['channel_ids'] }.flatten
    assert_same_elements [team.id, team2.id], users_permitted_response.map { |e| e['team_ids'] }.flatten
  end

  test 'should return `show_restrict` as true when card is given access to a user via team' do
    user = create :user
    org = user.organization
    card = create :card, author: user, organization: org
    api_v2_sign_in user

    # give access of this card to the user via team
    team = create :team, organization: org
    create :teams_user, team_id: team.id, user_id: user.id
    create :card_team_permission, card_id: card.id, team_id: team.id

    get :edit, id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)

    assert resp['show_restrict']
  end

  # -- show_restrict based on card completion --
  test 'should return `show_restrict` as false when card is given access to a user via team but completed' do
    user = create :user
    org = user.organization
    card = create :card, author: user, organization: org
    api_v2_sign_in user

    # give access of this card to the user via team
    team = create :team, organization: org
    create :teams_user, team_id: team.id, user_id: user.id
    create :card_team_permission, card_id: card.id, team_id: team.id

    create :user_content_completion, user_id: user.id, completable: card, state: 'completed'

    get :edit, id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)

    refute resp['show_restrict']
  end

  # -- show_restrict based on card assignment --
  test 'should return `show_restrict` as false when card is given access to a user via team but assigned' do
    user = create :user
    org = user.organization
    card = create :card, author: user, organization: org
    api_v2_sign_in user

    # give access of this card to the user via team
    team = create :team, organization: org
    create :teams_user, team_id: team.id, user_id: user.id
    create :card_team_permission, card_id: card.id, team_id: team.id

    # assign to someone
    create :assignment, assignable: card, assignee: user

    get :edit, id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)

    refute resp['show_restrict']
  end

  # -- show_restrict based on card bookmark --
  test 'should return `show_restrict` as false when card is given access to a user via team but bookmarked' do
    user = create :user
    org = user.organization
    card = create :card, author: user, organization: org
    api_v2_sign_in user

    # give access of this card to the user via team
    team = create :team, organization: org
    create :teams_user, team_id: team.id, user_id: user.id
    create :card_team_permission, card_id: card.id, team_id: team.id

    # someone bookmarks thid card
    create :bookmark, bookmarkable: card, user: create(:user, organization: org)

    get :edit, id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)

    refute resp['show_restrict']
  end

  test 'should return `users_with_access`' do
    user = create :user
    org = user.organization
    card = create :card, author: user, organization: org
    api_v2_sign_in user

    user1, user2 = create_list :user, 2, organization: org
    card.users_with_access << user1 << user2

    get :edit, id: card.id, format: :json
    assert_response :ok
    resp = get_json_response(response)

    assert_equal 2, resp['users_with_access'].length
    assert_same_elements [user1.id, user2.id], resp['users_with_access'].map {|user| user['id']}
  end

  test 'should return resource field if card is link card- UGC or ECL' do
    user = create :user
    org = user.organization
    ecl_id = 'qwe1-ewq1'
    resource = create(:resource, url: "http://www.gr0wing.com/how-to-choose-a-spiritual-master/")
    card = create(:card, card_type: 'course', ecl_id: ecl_id, organization: org, author_id: user.id, resource_id: resource.id)
    EclApi::CardConnector.any_instance.expects(:card_from_ecl_id).returns(card)
    api_v2_sign_in user

    expected_keys = %w(auto_complete duration completed_percentage teams_permitted users_permitted show_restrict
        file_resources channel_ids non_curated_channel_ids paid_by_user readable_card_type channels users_with_access tags
        teams language resource filestack provider provider_image)

    get :edit, id: "ECL-#{ecl_id}", format: :json
    resp = get_json_response(response)
    assert_same_elements expected_keys, resp.keys
  end

  test 'should not return resource field if card is not a link card' do
    user = create :user
    org = user.organization
    resource = create(:resource, url: "http://www.gr0wing.com/how-to-choose-a-spiritual-master/")
    card = create(:card, title: "Wow", organization: org, author_id: user.id)
    api_v2_sign_in user

    expected_keys = %w(auto_complete duration completed_percentage teams_permitted users_permitted show_restrict
        file_resources channel_ids non_curated_channel_ids paid_by_user readable_card_type channels users_with_access tags
        teams language filestack provider provider_image)

    get :edit, id: card.id, format: :json
    resp = get_json_response(response)
    assert_same_elements expected_keys, resp.keys
  end
end

require 'test_helper'

class Api::V2::CardsControllerTest < ActionController::TestCase
  self.use_transactional_fixtures = true

  setup do
    skip # NOT WORKED ON TRAVIS, locale - passed
    @org = create(:organization)
    @author, @user = create_list(:user, 2, organization: @org)
    @card1, @card2, @card3, @card4, @card5, @card6, @card7 = create_list(:card, 7,
      author: @author
    )
    #user related cards
    @user_card = create(:card, author: @user)
    UsersCardsManagement.create(
      card: @card1, action: 'added_to_pathway', user: @user, target_id: 1
    )
    create(:vote, votable: @card3, user_id: @user.id)
    create(
      :learning_queue_item, :from_assignments,
      queueable: @card5, user: @user, state: 'deleted'
    )

    #author related cards
    UsersCardsManagement.create(
      card: @card2, action: 'added_to_pathway', user: @author, target_id: 1
    )
    create(:vote, votable: @card4, user_id: @author.id)
    create(
      :learning_queue_item, :from_assignments,
      queueable: @card6, user: @author, state: 'completed'
    )
    @cards = [@card1, @card2, @card3, @card4, @card5, @card6, @card7, @user_card]

    # delete cards
    i = 1
    @cards.each do |card|
      card.update_attributes(deleted_at: Time.current + i.seconds)
      i+=1
    end

    @cards.each(&:reload)

    api_v2_sign_in(@user)
  end

  test 'should return user related deleted cards' do
    user_related_card_ids = [@card1, @card3, @card5, @user_card].map(&:id)
    get :deleted_cards, user_id: @user.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_same_elements user_related_card_ids, resp['cards'].map{|i| i['id'].to_i}
    assert_equal 4, resp['total']
  end

  test 'should return deleted cards where user is author' do
    get :deleted_cards, user_id: @user.id, only_own: true, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_same_elements [@user_card.id], resp['cards'].map { |i| i['id'].to_i }
  end

  test 'should support limit/offset parameter' do
    user_related_card_ids = [@card1, @card3, @card5, @user_card].map(&:id)
    get :deleted_cards, user_id: @user.id, limit: 1, offset: 1, format: :json
    assert_response :ok
    resp = get_json_response(response)
    result_card = Card.only_deleted.where(
      id: user_related_card_ids
    ).order(deleted_at: :desc).limit(1).offset(1).ids
    assert_equal 1, resp['cards'].count
    assert_same_elements result_card, resp['cards'].map { |i| i['id'].to_i }
  end
end

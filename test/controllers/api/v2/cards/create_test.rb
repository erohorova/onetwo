require 'test_helper'

class Api::V2::CardsControllerTest < ActionController::TestCase
  self.use_transactional_fixtures = true

  setup do
    stub_video_stream_index_calls
    EdcastActiveJob.stubs :perform_later
    IndexJob.stubs :perform_later

    @user = create(:user)
    @org = @user.organization

    api_v2_sign_in @user
    @url = 'http://url.com'
    stub_request(:get, @url).
          to_return(:status => 200, :body => '<html><head></head><body></body></html>', headers: {})
  end

  #Start create
  test 'should not create card for invalid params' do
    assert_no_difference -> {Card.count} do
      assert_raise do
        post :create, {message: 'This is first card in v2.0'}
      end
    end
  end

  test 'should create card with metadata and correct plan type' do
    post :create, card: {
      message: "Text card\n", is_public: true, is_paid: true, title:'',readable_card_type_name: 'Article',
      card_metadatum_attributes: {plan: 'paid'}
    }, format: :json
    resp = get_json_response(response)
    assert_not_nil Card.find(resp["id"].to_i).card_metadatum
    assert_equal 'paid', Card.find(resp["id"].to_i).card_metadatum.plan
  end

  test 'should create card with and update resource description' do
    resource = create(:resource, description: "description")
    post :create, card: {
      message: "Message", is_public: true, is_paid: true, title:'Test', resource_id: resource.id,
      resource_attributes: {id: resource.id, description: "New description"}
    }, format: :json
    resp = get_json_response(response)
    assert_equal "New description", resp["resource"]["description"]
  end

  test ':api should create card with BIA level sent in params' do
    post :create, card: { message: "Test card", card_metadatum_attributes: {level: "advanced"} }, format: :json
    resp = get_json_response(response)
    card_metadatum = Card.find(resp["id"].to_i).card_metadatum

    assert_not_nil card_metadatum
    assert_equal "advanced", card_metadatum.level
  end

  test 'should call upload_to_scorm method for scormcards' do
    Card.any_instance.expects(:upload_to_scorm)
    post :create, card: {message: 'This is first card in v2.0', filestack: [scorm_course: true]}, format: :json
    resp = get_json_response(response)
    assert resp["filestack"].first['scorm_course']
  end

  test 'should not call #upload_to_scorm method for non scormcards' do
    Card.any_instance.expects(:upload_to_scorm).never
    post :create, card: {message: 'This is first card in v2.0'}, format: :json
    resp = get_json_response(response)
    assert_equal [], resp["filestack"]
  end

  test ':api should create card with message and topics' do
    message = 'This is first card in v2.0'
    topics = [ "Web", "Ruby"]
    assert_difference -> {Card.count}, 1 do
      post :create, card: {message: message, topics: topics}, format: :json
    end
    assert_response :ok
    card = Card.last
    assert_equal 'media', card.card_type
    assert_equal @user.id, card.author_id
    assert_equal message, card.message
    assert_equal 0, card.mentioned_users.count
    assert_same_elements topics, card.tags.collect(&:name)
    assert_not_nil card.published_at
  end

  test 'should create card with user taxonomy topics' do
    user_topics = [{'label' => 'label1', 'path' => 'label.one'}, {'label' => 'label2', 'path' => 'label.two'}]
    assert_difference -> {Card.count}, 1 do
      post :create, card: {message: 'test', user_taxonomy_topics: user_topics}, format: :json
    end
    assert_response :ok
    card = Card.last
    assert_equal 'media', card.card_type
    assert_equal @user.id, card.author_id
    assert_equal 'test', card.message
    assert_equal user_topics, card.user_taxonomy_topics
    assert_not_nil card.published_at
  end

  test 'should create card carousel card without author' do
    message = 'This is hidden card'
    assert_difference -> {Card.count}, 1 do
      post :create, card: {message: message, is_carousel: 'true'}, format: :json
    end
    assert_response :ok
    card = Card.last
    assert_equal 'media', card.card_type
    assert_equal card.author_id, nil
  end

  test 'should create hidden card when hidden passed as true in params' do
    user = create(:user)
    api_v2_sign_in user

    message = 'This is hidden card'
    assert_difference -> {Card.count}, 1 do
      post :create, card: {message: message, hidden: true}, format: :json
    end
    assert_response :ok
    card = Card.last
    assert card.hidden?
    assert_equal 'media', card.card_type
    assert_equal card.author_id, user.id
  end

  test ':api should create project card' do
    assert_difference ['Card.count', 'Project.count'] do
      post :create, card: {message: 'Start writing here', users_with_access_ids: nil,
       channel_ids: [], team_ids: nil, is_public: true, prices_attributes: nil,
       title: 'Project name', project_attributes: {reviewer_id: ''}, card_type: 'project',
       topics: [], duration: 0, hidden: true}, format: :json
    end
    assert_response :ok
  end

  test 'on card destroy project should be deleted' do
    TestAfterCommit.with_commits(true) do
      card = create :card, organization: @org, author: @user, card_type: 'project', card_subtype: 'text'
      assert_equal 'project', card.card_type
      assert_equal card.id, Project.last.card_id
      delete :destroy, id: card.id
      assert_equal Project.count, 0
      assert_equal Card.count, 0
    end
  end

  test 'on card destroy mentions should be deleted' do

    TestAfterCommit.with_commits(true) do
      u1, u2 = create_list(:user, 2, organization: @org)
      message = "this is a message with  #{u1.handle} and #{u2.handle}."
      card = create :card, organization: @org, author: @user, message: message

      assert_same_elements [u1.id, u2.id], card.mentions.map(&:user_id)

      delete :destroy, id: card.id
      assert_equal Card.count, 0
      assert_equal Mention.count, 0
    end
  end

  test 'should not affect channel association with card when anything else edited by admin user' do
    users_list = create_list(:user, 2, organization: @org, organization_role: 'admin')
    channel = create(:channel, organization: @org)
    create(:follow, followable: channel, user_id: users_list[0].id)
    create(:follow, followable: channel, user_id: users_list[1].id)
    card = create(:card, author: users_list[0])
    ChannelsCard.create(channel_id: channel.id, card_id: card.id)

    api_v2_sign_in users_list[1]

    put :update, id: card.id, card: {topics: ["test-card"]}, fields: 'channel_ids,tags', format: :json
    resp = get_json_response(response)

    assert_equal card.channel_ids, resp['channel_ids']
    assert_equal "test-card", resp['tags'][0]['name']
  end

  test 'should not create hidden card when hidden passed along with channels in params' do
    user = create(:user)
    api_v2_sign_in user
    channel_list = create_list(:channel, 3, organization: user.organization)
    message = 'This is hidden card'
    assert_difference -> {Card.count}, 1 do
      post :create, card: {message: message, hidden: true, channel_ids: channel_list.map(&:id)}, format: :json
    end
    assert_response :ok
    card = Card.last
    refute card.hidden?
    assert_equal 'media', card.card_type
    assert_equal card.author_id, user.id
  end

  test 'should convert a hidden card of a pathway to a visible card when updated to post to a channel' do
    user = create(:user)
    api_v2_sign_in user
    hidden_card = create(:card, author: user, hidden: true)
    assert hidden_card.hidden?
    channel_follow = create(:follow, user: user, followable: create(:channel, organization: user.organization, only_authors_can_post: false))
    assert_difference -> {ChannelsCard.count}, 1 do
      put :update, id: hidden_card.id, card: {channel_ids: [channel_follow.followable.id] }, format: :json
    end
    refute  hidden_card.reload.hidden?
    assert_includes hidden_card.channels.map(&:id), channel_follow.followable_id
  end

  test 'should update the card with user_id when team_ids passed to the card' do
    user = create(:user)
    api_v2_sign_in user
    card = create(:card, author: user)
    team = create(:team, organization: user.organization)
    team.add_member(user)
    assert_difference -> {SharedCard.count}, 1 do
      put :update, id: card.id, card: {team_ids: [team.id]}, format: :json
    end
    assert_equal user.id, card.shared_cards.first.user_id
    assert_equal card.id, card.shared_cards.first.card.id
    assert_equal team.id, card.shared_cards.first.team_id
  end

  test 'should create card with user_id when team_ids passed during creation' do
    user = create(:user)
    team = create(:team, organization: user.organization)
    team.add_member(user)
    api_v2_sign_in user
    assert_difference -> {Card.count}, 1 do
      post :create, card: {message: "this is terrific", team_ids: [team.id]}, format: :json
    end
    resp = get_json_response(response)
    assert_equal SharedCard.last.card_id, resp['id'].to_i
    assert_equal team.id, resp['teams'][0]['id']
    assert_equal user.id, SharedCard.last.user_id
  end

  test 'should be able to add and delete users a private card is shared to' do
    org = create(:organization)
    users = create_list(:user, 3, organization: org)
    api_v2_sign_in users[0]
    card = create(:card, author: users[0])
    create(:card_user_share, user: users[1], card: card)
    assert_includes card.users_with_access_ids, users[1].id
    put :update, id: card.id, card: {users_with_access_ids: [users[2].id]}, format: :json
    assert_includes card.reload.users_with_access_ids, users[2].id
    assert_not_includes card.reload.users_with_access_ids, users[1].id
  end

  test 'should remove all users when no value sent for user_access_ids' do
    org = create(:organization)
    users = create_list(:user, 2, organization: org)
    api_v2_sign_in users[0]
    card = create(:card, author: users[0])
    create(:card_user_share, user: users[1], card: card)
    assert_includes card.users_with_access_ids, users[1].id

    put :update, id: card.id, card: {message: "new new new"}, format: :json
    assert_not_includes card.reload.users_with_access_ids, users[1].id
  end

  test 'should be able to remove teams a card has been shared in' do
    user = create(:user)
    api_v2_sign_in user
    card = create(:card, author: user)
    team = create(:team, organization: user.organization)
    create(:shared_card, team_id: team.id, card_id: card.id, user_id: user.id)
    assert_includes card.shared_with_team_ids, team.id

    put :update, id: card.id, card: {team_ids: nil}, format: :json
    assert_not_includes card.reload.shared_with_team_ids, team.id
  end

  test 'should be able to remove and add teams from a card' do
    user = create(:user)
    api_v2_sign_in user
    card = create(:card, author: user)
    team_list = create_list(:team, 3, organization: user.organization)
    create(:shared_card, team_id: team_list[0].id, card_id: card.id, user_id: user.id)
    create(:shared_card, team_id: team_list[1].id, card_id: card.id, user_id: user.id)

    put :update, id: card.id, card: {team_ids: [team_list[0].id, team_list[2].id]}, fields: 'teams', format: :json
    resp = get_json_response(response)
    assert_same_elements resp['teams'].map{|team| team['id']}, [team_list[0].id, team_list[2].id]
    assert_not_includes resp['teams'].map{|team| team['id']}, team_list[1].id
  end

  test 'should create visible card when hidden not passed in params' do
    user = create(:user)
    api_v2_sign_in user

    message = 'This is a normal card'
    assert_difference -> {Card.count}, 1 do
      post :create, card: {message: message}, format: :json
    end
    assert_response :ok
    card = Card.last
    refute card.hidden?
    assert_equal 'media', card.card_type
    assert_equal card.author_id, user.id
  end

  test 'creates card with `readable_card_type`' do
    user = create(:user)
    api_v2_sign_in user

    message = 'This is a normal card'
    type = 'Webinar'
    assert_difference -> {Card.count}, 1 do
      post :create, card: { message: message, readable_card_type_name: type }, format: :json
    end
    assert_response :ok
    card = Card.last
    assert_equal Card.readable_card_type(type), card.readable_card_type
    assert_equal Card.readable_card_type_name(card.readable_card_type), 'webinar'
    assert_equal card.card_type_display_name, type
  end

  test 'creating card with `Curricula` card_type' do
    type = 'Curricula'
    assert_difference -> { Card.count }, 1 do
      post :create, card: { message: 'message', readable_card_type_name: type }, format: :json
    end
    card = Card.last
    assert_response :ok

    assert_equal Card.readable_card_type(type), card.readable_card_type
    assert_equal Card.readable_card_type_name(card.readable_card_type), 'curricula'
    assert_equal card.card_type_display_name, type
  end

  test 'creating card with `card_types`' do
    card_types = {
      vlabs: 'Vlabs',
      simulation: 'Simulation',
      diagnostics: 'Diagnostics',
      books: 'Books',
      audio_books: 'Audio Books',
      jobs: 'Jobs',
      introduction: 'Introduction'
    }
    card_types.each do | key, type |
      assert_difference -> { Card.count }, 1 do
        post :create, card: { message: 'message', readable_card_type_name: type }, format: :json
      end
      assert_response :ok
      card = Card.last
      assert_equal Card.readable_card_type(type), card.readable_card_type
      assert_equal Card.readable_card_type_name(card.readable_card_type), key.to_s
      assert_equal card.card_type_display_name, type
    end
  end

  test ':api card with file resource should be made an image card' do
    message = "this is a message"
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)
    fr2 = FileResource.create(attachment: pic.open)

    assert_difference ->{Card.count} do
      post :create, card: {message: message, file_resource_ids: [fr1.id, fr2.id]}, format: :json
      assert_response :ok
    end
    card = Card.last

    assert_nil card.resource_id
    assert_same_elements [fr1, fr2], card.file_resources
  end

  test 'pack card should have the right subtype simple' do
    message = "this is a message"
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)
    fr2 = FileResource.create(attachment: pic.open)

    assert_difference ->{Card.count} do
      post :create, card: {card_type: 'pack', message: message, file_resource_ids: [fr1.id, fr2.id]}, format: :json
      assert_response :ok
    end
    card = Card.last

    assert_equal 'pack', card.card_type
    assert_equal 'simple', card.card_subtype
    assert_same_elements [fr1, fr2], card.file_resources
  end

  test 'pack card should have the right auto_complete value' do
    assert_difference ->{Card.count} do
      post :create, card: { card_type: 'pack', message: 'message', card_subtype: 'simple', auto_complete: false }, format: :json
      assert_response :ok
    end
    card = Card.last
    assert_equal false, card.auto_complete
  end

  test 'journey card should have the right auto_complete value' do
    assert_difference ->{Card.count} do
      post :create, card: { card_type: 'journey', message: 'message', card_subtype: 'self_paced', auto_complete: false }, format: :json
      assert_response :ok
    end
    card = Card.last
    assert_equal false, card.auto_complete
  end

  # MOVE THIS TEST TO MODEL
  # test 'notifications on card creation #push_notification_enabled' do
  #   Card.any_instance.stubs(:reindex_card)
  #   TestAfterCommit.enabled = true
  #   #card by followers
  #   u1 = create :user, organization: @org
  #   u2 = create :user, organization: @org
  #   follow = u1.follows.create(followable: @user)
  #   message = "this is a message"

  #   notifier =  EDCAST_NOTIFY.notification_name_to_notifiers[Notify::NOTE_NEW_CONTENT_BY_FOLLOWED_USER]
  #   notifier.set_disabled(:email, :single)
  #   notifier.set_enabled(:push, :single)

  #   EDCAST_NOTIFY.expects(:send_email).never
  #   EDCAST_NOTIFY.expects(:send_push).never
  #   post :create, card: {message: message}, format: :json
  #   assert_equal NotificationEntry.last.user, u1
  # end

  test 'notifications on card creation #push_notification_disabled' do
    Card.any_instance.stubs(:reindex_card)
    TestAfterCommit.enabled = true
    #card by followers
    u1 = create :user
    u2 = create :user
    follow = u1.follows.create(followable: @user)
    message = "this is a message"

    notifier =  EDCAST_NOTIFY.notification_name_to_notifiers[Notify::NOTE_NEW_CONTENT_BY_FOLLOWED_USER]

    notifier.set_disabled(:push, :single)
    notifier.set_enabled(:email, :single)

    EDCAST_NOTIFY.expects(:send_email).never
    EDCAST_NOTIFY.expects(:send_push).never
    post :create, card: {message: message}, format: :json

    #no notification for u2
    assert_empty u2.notification_entries
  end

  test ':api card with video stream with valid attributes' do
    message = "this is a VideoStream"
    params = {
      message: message,
      video_stream: {
        start_time: Time.now.utc.strftime('%FT%TZ'),
        status: 'live'
      }
    }

    assert_difference ->{Card.count} do
      assert_difference ->{VideoStream.count} do
        post :create, card: params, format: :json
        assert_response :ok
      end
    end
    vs = VideoStream.last
    assert_equal vs.type, "IrisVideoStream"
    assert_equal "live", vs.status
    assert_equal vs.card, Card.last
    assert_equal message, vs.name
    resp = get_json_response(response)
    assert_equal resp["message"], message
    assert_equal resp["card_type"], "video_stream"
    assert_equal resp["video_stream"]["id"], vs.id
  end

  test 'card with video stream and with #draft state' do
    message = "this is a VideoStream"
    params = {
      message: message,
      state: 'draft',
      video_stream: {
        start_time: Time.now.utc.strftime('%FT%TZ'),
        status: 'live'
      }
    }

    assert_difference ->{Card.count} do
      assert_difference ->{VideoStream.count} do
        post :create, card: params, format: :json
        assert_response :ok
      end
    end
    vs = VideoStream.last
    card = Card.last
    assert_equal 'simple', card.card_subtype
    assert_equal "live", vs.status
    assert_equal card, vs.card
    assert_equal 'draft', vs.state
    assert_equal 'draft', card.state
  end

  test "should create video_stream card with recording" do
    EdcastActiveJob.unstub :perform_later
    TranscoderJob.unstub :perform_later
    aws_client = AWS::ElasticTranscoder::Client.new
    AWS::ElasticTranscoder::Client.stubs(:new).returns(aws_client)

    create_job_mock = mock()
    create_job_mock.stubs(:data).returns({job: {id:'job123'}})
    aws_client.stubs(:create_job).with(){|params|
      !params.has_key?(:playlists) }.returns(create_job_mock)

    message = "this is a test VideoStream"
    params = {
      message: message,
      video_stream: {
        start_time: Time.now.utc.strftime('%FT%TZ'),
        status: 'past',
        recording: {
          location: 'https://somewhere.com/somebucket/2323/vid.mpg4',
          bucket: 'somebucket',
          key: '/somebucket/2323/vid.mp4',
          checksum: '123'
        }
      }
    }
    post :create, card: params, format: :json
    assert_response :ok
    resp = get_json_response(response)

    recording = Recording.last
    recording.submit_aws_transcoder_job
    recording.reload

    assert_equal resp["card_type"], "video_stream"
    assert_equal 0, recording.sequence_number
    assert_equal 'https://somewhere.com/somebucket/2323/vid.mpg4', recording.location
    assert_equal 'somebucket', recording.bucket
    assert_equal '123', recording.checksum
    assert_equal '/somebucket/2323/vid.mp4', recording.key
    assert_equal 'submitted', recording.transcoding_status

    assert_equal recording.video_stream.card_id.to_s, resp["id"]
  end

  test 'should not create video_stream and card with invalid params' do
    message = "this is a invalid VideoStream"

    assert_no_difference ->{Card.count} do
      assert_no_difference ->{VideoStream.count} do
        post :create, card: {message: message, video_stream: {start_time: nil, status: 'pending'}}, format: :json
        assert_response :unprocessable_entity
        assert_equal get_json_response(response)['message'], "Start time can't be blank"
      end
    end
  end

  test 'card of sub type `file`' do
    title = "this is a title with a #tag1"
    message = "this is a message with a url: #{@url}"
    test_file = Rails.root.join('test/fixtures/files/test.csv')
    fr1 = FileResource.create(attachment: test_file.open)

    assert_difference ->{Card.count}, 1 do
      post :create, card: {message: message, file_resource_ids: [fr1.id]}, format: :json
      assert_response :ok
    end

    assert_not_empty Card.last.file_resources
  end

  test 'should reject cards with edcast.com link' do
    #good link
    assert_difference ->{Card.count} do
      post :create, card: {message: 'some message \n http://www.yahoo.com/'}, format: :json
      assert_response :ok
    end

    assert_difference ->{Card.count} do
      post :create, card: {message: 'some message http://www.edcast.com/ssdf'}, format: :json
      assert_response :ok
    end
    assert_nil Card.last.resource
  end

  test 'should extract mentions from cards' do
    now = Time.now
    Time.stubs(:now).returns(now)
    u1, u2 = create_list(:user, 2, organization: @org)
    message = "this is a message with and a #{u1.handle} #{u2.handle}."
    expected_message = "this is a message with and a #{u1.handle} #{u2.handle}."

    assert_difference ->{Card.count} do
      post :create, card: {message: message}, format: :json
      assert_response :success
    end
    card = Card.last
    card.run_callbacks(:commit)

    assert_same_elements [u1.id, u2.id], card.mentions.map(&:user_id)
    assert_equal now.to_i, card.published_at.to_i
    Time.unstub(:now)
  end

  test 'should set card subtype from params' do
    message = "test"
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)

    assert_difference ->{Card.count} do
      post :create, card: {message: message, card_subtype: "image", file_resource_ids: [fr1.id]}, format: :json
      assert_response :success
    end
    card = Card.last
    assert_equal "image", card.card_subtype
  end

  test 'should add rating to card' do
    card = create(:card, author_id: @user.id, card_type: 'media')

    # Make sure rating is created
    assert_difference ->{CardsRating.count} do
      put :rate, id: card.id, rating: 4, format: :json
      assert_response :success
    end

    # Make sure CardsRating is correct
    rating = CardsRating.last
    assert_equal 4, rating.rating
    assert_equal card.id, rating.card_id
    assert_equal @user.id, rating.user_id
  end

  test 'should add rating to card with level' do
    card = create(:card, author_id: @user.id, card_type: 'media')

    # Make sure rating is created
    assert_difference ->{CardsRating.count} do
      put :rate, id: card.id, rating: 4, level: "advanced", format: :json
      assert_response :success
    end

    # Make sure CardsRating is correct
    rating = CardsRating.last
    assert_equal "advanced", rating.level
    assert_equal @user.id, rating.user_id
  end

  test 'should add rating to card without level' do
    card = create(:card, author_id: @user.id, card_type: 'media')

    # Make sure rating is created
    assert_difference ->{CardsRating.count} do
      put :rate, id: card.id, rating: 4, format: :json
      assert_response :success
    end

    # Make sure CardsRating is correct
    rating = CardsRating.last
    assert_equal @user.id, rating.user_id
  end

  test 'should add level to card without rating' do
    card = create(:card, author_id: @user.id, card_type: 'media')

    # Make sure rating is created
    assert_difference ->{CardsRating.count} do
      put :rate, id: card.id, level: "advanced", format: :json
      assert_response :success
    end

    # Make sure CardsRating is correct
    rating = CardsRating.last
    assert_equal "advanced", rating.level
  end


  test 'should not add rating to card with wrong level' do
    card = create(:card, author_id: @user.id, card_type: 'media')

    put :rate, id: card.id, rating: 4, level: "dummy", format: :json
    assert_response :unprocessable_entity
  end

  test 'rating api should update existing rating' do
    card = create(:card, author_id: @user.id, card_type: 'media')
    rating = create(:cards_rating, rating: 2, user: @user, card: card)

    assert_no_difference ->{CardsRating.count} do
      put :rate, id: card.id, rating: "3", format: :json
      assert_response :ok
    end

    # Make sure new rating is correct
    rating.reload
    assert_equal 3, rating.rating
  end

  test 'rating failure cases' do
    card = create(:card, author_id: @user.id, card_type: 'media')
    rating = create(:cards_rating, rating: 2, user: @user, card: card)
    # Make sure invalid rating fails
    put :rate, id: card.id, rating: "abc", format: :json
    assert_response :unprocessable_entity

    # Make sure new rating is correct
    rating.reload
    assert_equal 2, rating.rating
  end

  test 'records level' do
    LogHistoryJob.expects(:perform_later).with(
      has_entries("changes" => {"level" => [nil, "beginner"]}.as_json)
    ).once
    card = create(:card, author_id: @user.id, card_type: 'media')
    put :rate, id: card.id, level: "beginner", format: :json
  end

  test 'doesnt call LogHistoryJob if level is not present in params' do
    LogHistoryJob.expects(:perform_later).never
    card = create(:card, author_id: @user.id, card_type: 'media')
    put :rate, id: card.id, rating: 2, format: :json
    assert_response :success
  end

  test 'should handle unsuccessful card creation' do
    assert_difference -> {Card.count}, 0 do
      channel_ids = create_list(:channel, 2).map(&:id)
      post :create, card: {channel_ids: channel_ids}, format: :json
    end

    assert_response :unprocessable_entity
    assert_equal "Message can't be blank", get_json_response(response)['message']
  end


  test 'should auto curate cards posted in channel by collaborators on card creation' do
    org = @user.organization
    channel = create(:channel, organization: org, curate_only: true, only_authors_can_post: false)
    channel.authors << @user

    assert_difference -> {ChannelsCard.count} do
      post :create, card: {channel_ids: [channel.id], message: 'Create Card'}, format: :json
      assert_equal ChannelsCard.last.state, "curated"
    end
  end

  test 'should auto curate cards posted in channel by followers and followers cards curation is not required on create' do
    org = @user.organization
    channel = create(:channel, organization: org, curate_only: true, only_authors_can_post: false)
    channel.followers << @user
    card = create(:card, is_public: true, author_id: @user.id)

    config = create :config, configable_id: org.id, configable_type: 'Organization', name: 'channel/curation_for_followers_card', data_type: "boolean", value: 'false'
    assert_difference -> {ChannelsCard.count}, 1 do
      put :update, id: card.id, card: {message: 'Updated Card', channel_ids: [channel.id] }, format: :json
      assert_equal ChannelsCard.last.state, "curated"
    end
  end

  test 'should auto curate cards posted in channel by followers and followers cards curation is required on create' do
    org = @user.organization
    channel = create(:channel, organization: org, curate_only: true, only_authors_can_post: false)
    channel.followers << @user
    card = create(:card, is_public: true, author_id: @user.id)
    config = create :config, configable_id: org.id, configable_type: 'Organization', name: 'channel/curation_for_followers_card', data_type: "boolean", value: 'true'

    assert_difference -> {ChannelsCard.count}, 1 do
      put :update, id: card.id, card: {message: 'Updated Card', channel_ids: [channel.id] }, format: :json
      assert_equal ChannelsCard.last.state, "new"
    end
  end

  test 'should save filestack information' do
    stub_request(:get, "https://cdn.filestackcontent.com/Ct0DbtZ0TtmzI5AHv8Yh")
    filestack = [
      { name: "Presentation1.pptx", mimetype: "application/vnd.openxmlformats-officedocument.presentationml.presentation",
        size: 701380, source: "local_file_system", url: "https://cdn.filestackcontent.com/Ct0DbtZ0TtmzI5AHv8Yh",
        handle: "Ct0DbtZ0TtmzI5AHv8Yh", status: "Stored"
      }
    ]
    Filestack::Security.any_instance.stubs(:signed_url).returns("https://cdn.filestackcontent.com/Ct0DbtZ0TtmzI5AHv8Yh")
    ProcessFilestackImagesJob.expects(:perform_later)
    assert_difference -> {Card.count}, 1 do
      post :create, card: {message: Faker::Lorem.sentence, filestack: filestack}, format: :json
    end
    card = Card.last
    assert_not_nil card.filestack
    assert_response :ok
    assert_equal card.filestack, get_json_response(response)['filestack']
  end

  test 'should assign appropriate card subtype for video upload cards' do
    resource = create(:resource, type: 'Video')
    params = {author_id: @user.id, card_subtype: 'video', title: 'title-for-card', message: 'message-for-card', resource_id: resource.id}

    post :create, card: params, format: :json
    assert_response :success
    card = Card.last
    assert_equal 'video', card.card_subtype
    assert_equal 'media', card.card_type
  end

  test ':api should create card with message #member' do
    Role.any_instance.unstub :create_role_permissions
    org = @user.organization
    org.update(enable_role_based_authorization: true)
    Role.create_standard_roles org
    @user.roles << org.roles.find_by_name('member')

    message = 'This is first card in v2.0'
    assert_difference -> {Card.count}, 1 do
      post :create, card: {message: message}, format: :json
    end
    assert_response :ok
  end

  test ':api should create card with message #curator' do
    Organization.any_instance.unstub(:create_or_update_roles)

    org = @user.organization
    org.update(enable_role_based_authorization: true)
    org.run_callbacks(:commit)

    @user.role_ids = [org.roles.where(name: Role::TYPE_CURATOR).first.id]
    @user.save

    message = 'This is first card in v2.0'
    assert_no_difference -> {Card.count}, 1 do
      post :create, card: {message: message}, format: :json
    end
    assert_response :unauthorized
  end

  test 'should set sharer details when creating card with team details' do
    team = create(:team, organization: @user.organization)
    create(:teams_user, team_id: team.id, user_id: @user.id)
    post :create, card: {message: 'new new new', team_ids: [team.id]}, format: :json
    assert_response :ok
    assert_equal @user.id, SharedCard.last.user_id
  end

  # poll
  test ':api should create polls' do
    assert_difference ->{Card.count} do
      post :create, card: {message: 'Who da best?', options: [{label: 'me'}, {label: 'you'}]}, format: :json
      assert_response :success
    end

    card = Card.last
    assert_equal 'poll', card.card_type
    assert_equal 2, card.quiz_question_options.count
    assert_equal ['me', 'you'], card.quiz_question_options.map(&:label)
    assert_equal 'Who da best?', card.message

    #  now some options with images
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)
    fr2 = FileResource.create(attachment: pic.open)

    assert_difference ->{Card.count} do
      post :create, card: {message: 'Who da best? #musing', options: [{label: 'me', file_resource_id: fr1.id}, {label: 'you', file_resource_id: fr2.id}]}, format: :json
      assert_response :success
    end
    card = Card.last
    assert_equal [fr1, fr2].map(&:file_url), card.quiz_question_options.map(&:as_json).map{|qqo| qqo['image_url']}
  end


  test ':api should return 422 without options' do
    assert_no_difference ->{Card.count} do
      post :create, card: {message: 'poll without options', options: [{label: ''}, {label: ''}]}, format: :json
      assert_response :unprocessable_entity
      assert_equal "Label can't be blank", get_json_response(response)['message']
    end
  end

  test ':api poll card with file resource should be made an image card' do
    message = "this is a title with a #tag1"
    pic = Rails.root.join('test/fixtures/images/logo.png')
    fr1 = FileResource.create(attachment: pic.open)

    assert_difference ->{Card.count} do
      post :create, card: {message: message, file_resource_ids: [fr1.id], options: [{label: 'me'}, {label: 'you'}]}, format: :json
      assert_response :ok
    end
    card = Card.last

    assert_equal 'image', card.card_subtype
    assert_equal fr1.file_name, card.file_resources.first.as_json['file_name']
    assert_equal fr1.file_url, card.file_resources.first.as_json['file_url']
  end

  # poll response
  test 'record stats on polls' do
    EdcastActiveJob.unstub :perform_later
    QuizQuestionStatsRecorderJob.unstub :perform_later

    request.env['HTTP_REFERER'] = 'http://edcast.com'
    poll = create(:card, card_type: "poll", is_public: true, author_id: @user.id)
    opt1 = create(:quiz_question_option, label: 'opt 1', is_correct: false, quiz_question: poll)
    opt2 = create(:quiz_question_option, label: 'opt 2', is_correct: true, quiz_question: poll)

    Tracking.expects(:track_act).with(
        user_id: @user.id,
        object: 'card',
        object_id: poll.id,
        card: ClientOnlyRoutes.show_insight_url(poll),
        action: 'poll_respond',
        platform: 'web',
        device_id: nil,
        organization_id: @user.organization_id,
        referer: 'http://edcast.com'
    )

    assert_difference ->{QuizQuestionAttempt.count}, 1 do
      post :responses, id: poll.id, selections: [opt1.id], format: :json
      assert_response :success
    end

    attempt = QuizQuestionAttempt.last
    # After commit callbacks are not running. Invoking after commit method
    attempt.send(:record_quiz_question_stats)
    assert_equal @user, attempt.user

    assert_equal 1, poll.quiz_question_stats.as_json['attempt_count']
    assert_equal({opt1.id.to_s => 1}, poll.quiz_question_stats.as_json['options_stats'])

    user2 = create(:user, organization: @org)
    set_api_request_header user2

    Tracking.expects(:track_act).with(
        user_id: user2.id,
        object: 'card',
        object_id: poll.id,
        card: ClientOnlyRoutes.show_insight_url(poll),
        action: 'poll_respond',
        platform: 'web',
        device_id: nil,
        organization_id: user2.organization_id,
        referer: 'http://edcast.com'
    )

    assert_difference ->{QuizQuestionAttempt.count} do
      post :responses, id: poll.id, selections: [opt1.id, opt2.id], format: :json
      assert_response :success
    end

    poll.reload
    attempt = QuizQuestionAttempt.last
    # # After commit callbacks are not running. Invoking after commit method
    attempt.send(:record_quiz_question_stats)

    assert_equal 2, poll.quiz_question_stats.as_json['attempt_count']
    assert_equal({opt1.id.to_s => 2, opt2.id.to_s => 1}, poll.quiz_question_stats.as_json['options_stats'])
  end

  test 'should not allow user to update poll cards if already answered' do
    poll = create(:card, card_type: 'poll', is_public: true, author_id: @user.id)
    opt1 = create(:quiz_question_option, label: 'opt 1', is_correct: false, quiz_question: poll)
    opt2 = create(:quiz_question_option, label: 'opt 2', is_correct: true, quiz_question: poll)

    user2 = create(:user, organization: @org)
    set_api_request_header user2

    post :responses, id: poll.id, selections: [opt1.id], format: :json
    assert_response :success

    assert poll.reload.question_attempted?

    set_api_request_header @user
    put :update, id: poll.id, card: {message: 'updated poll card', options: [{id: opt1.id, label: 'opt1'}, {id: opt2.id, label: 'opt2'}, {label: 'yes'}, {label: 'no'}]}, format: :json

    resp = get_json_response(response)

    poll.reload

    assert_response :unprocessable_entity
    assert_match /Edit functionality is disabled to maintain integrity of the poll./, resp['message']
    assert_not_equal 'updated poll card', poll.title
    assert_equal 2, poll.quiz_question_options.length
  end

  test 'should return errors for quiz card re-answer if can_be_reanswered:false' do
    poll = create(:card, card_type: 'poll', is_public: true, author_id: @user.id, can_be_reanswered: false)
    opt1 = create(:quiz_question_option, label: 'opt 1', is_correct: false, quiz_question: poll)
    opt2 = create(:quiz_question_option, label: 'opt 2', is_correct: true, quiz_question: poll)
    user2 = create(:user, organization: @org)
    set_api_request_header user2
    QuizQuestionAttempt.create( quiz_question: poll, user: user2, selections: [opt1.id])
    post :responses, id: poll.id, selections: [opt1.id], format: :json
    resp = get_json_response(response)
    assert_response :unprocessable_entity
    assert_equal 'Can not re-answer this card', resp['message']
  end

  test "#get_poll_card" do
    @controller.stubs(:current_org).returns @user.organization
    poll = create(:card, card_type: "poll", is_public: true, author_id: @user.id)
    assert_equal @controller.send(:get_poll_card, poll.id), poll
  end

  test "#get_quiz_question_attempt" do
    qqa = QuizQuestionAttempt.new
    QuizQuestionAttempt.expects(:new).with(
      quiz_question: "quiz question",
      user: "user",
      selections: "selections"
    ).returns qqa
    assert_equal(
      qqa,
      @controller.send(
        :get_quiz_question_attempt,
        "quiz question", "user", "selections"
      )
    )
  end

  test 'should create course type cards for scrom upload' do
    Card.any_instance.expects(:upload_to_scorm)
    post :create, card: {message: 'This is first card in v2.0', filestack: [scorm_course: true]}, format: :json
    resp = get_json_response(response)
    assert_equal resp['card_type'], 'course'
  end

  test 'api response should contains users_with_access with id, fullname, handle and avatars keys' do
    user = create(:user)
    team = create(:team, organization: user.organization)
    team.add_member(user)
    api_v2_sign_in user
    post :create, card: {message: "Check card response", users_with_access_ids: [user.id], team_ids: [team.id]}, format: :json
    resp = get_json_response(response)
    assert resp["users_with_access"].present?
    assert resp["users_with_access"][0]["id"].present?
    assert resp["users_with_access"][0]["full_name"].present?
    assert_equal resp["users_with_access"][0]["full_name"], user.full_name
    assert resp["users_with_access"][0]["handle"].present?
    assert_equal resp["users_with_access"][0]["handle"], user.handle
    assert resp["users_with_access"][0]["avatarimages"].present?
  end

  test "should trigger notification job if card is shared with user while creating" do
    @author = create :user, organization: @org, first_name: "ABC",last_name: "XYZ"
    user1 = create(:user, organization: @org)
    TestAfterCommit.with_commits(true) do
      NotificationGeneratorJob.expects(:perform_later).once
      post :create, card: {message: 'Ruby Development', users_with_access_ids: [user1.id]}, format: :json
      assert_equal 200, response.status
    end
  end

  # poll ends
  # create ends

end

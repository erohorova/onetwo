require 'test_helper'

class Api::V2::CardsControllerTest < ActionController::TestCase
  self.use_transactional_fixtures = true

  setup do
    stub_video_stream_index_calls
    EdcastActiveJob.stubs :perform_later
    IndexJob.stubs :perform_later

    @user = create(:user)
    @org = @user.organization

    api_v2_sign_in @user
    @url = 'http://url.com'
    stub_request(:get, @url).
          to_return(:status => 200, :body => '<html><head></head><body></body></html>', headers: {})
  end

  #card_reportings
  test ':api should return card reportings for reported cards in descending order' do
    org = create(:organization, enable_role_based_authorization: true)
    user1, user2, user3 = create_list(:user,3,organization: org)
    Role.create_standard_roles org
    admin_role = org.roles.find_by_name('admin')
    create(:role_permission, role: admin_role, name: Permissions::ADMIN_ONLY)

    user1.add_role('admin')
    card1 = create(:card, author: user1, organization: org)
    reported_card_once = create(:card_reporting, card_id: card1.id, user_id: user1.id, created_at: Date.today)
    reported_card_twice = create(:card_reporting, card_id: card1.id, user_id: user2.id, created_at: Date.today + 2.days)
    reported_card_thrice = create(:card_reporting, card_id: card1.id, user_id: user3.id, created_at: Date.today + 3.days)

    api_v2_sign_in user1
    get :card_reportings, id: card1.id, format: :json
    resp = get_json_response(response)

    assert_equal reported_card_thrice.created_at.to_date, resp['card_reportings'][0]['reported_at'].to_date
    assert_equal reported_card_once.created_at.to_date, resp['card_reportings'][2]['reported_at'].to_date
  end

  test ':api should return card reportings for reported cards when RBAC is enabled' do
    org = create(:organization, enable_role_based_authorization: true)
    @user1 = create(:user,organization: org)
    Role.create_standard_roles org
    admin_role = org.roles.find_by_name('admin')
    create(:role_permission, role: admin_role, name: Permissions::ADMIN_ONLY)

    @user1.add_role('admin')
    card1, card2 = create_list(:card, 2, author: @user1, organization: org)
    reported_card = create(:card_reporting, card_id: card1.id, user_id: @user1.id)
    trashed_card = create(:card_reporting, card_id: card2.id, user_id: @user1.id, deleted_at: Date.today)

    api_v2_sign_in @user1
    get :card_reportings, id: card1.id, format: :json
    resp = get_json_response(response)

    assert_equal 1, resp['card_reportings'].length
  end

  test ':api should return card reportings for trashed cards when RBAC is enabled' do
    org = create(:organization, enable_role_based_authorization: true)
    @user1 = create(:user, organization: org)
    @user2 = create(:user, organization: org)
    Role.create_standard_roles org
    admin_role = org.roles.find_by_name('admin')
    create(:role_permission, role: admin_role, name: Permissions::ADMIN_ONLY)

    @user1.add_role('admin')
    card1, card2, card3 = create_list(:card, 3, author: @user1, organization: org)
    reported_card = create(:card_reporting, card_id: card1.id, user_id: @user2.id)
    trashed_card1 = create(:card_reporting, card_id: card2.id, user_id: @user2.id, deleted_at: Date.today)
    trashed_card2 = create(:card_reporting, card_id: card2.id, user_id: @user2.id, deleted_at: Date.today - 1.day)

    api_v2_sign_in @user1
    get :card_reportings, id: card2.id, format: :json
    resp = get_json_response(response)

    assert_equal 2, resp['card_reportings'].length
  end

  test 'should not list card reportings when user is not with admin access' do
    @user1 = create(:user, organization: @org)
    card1, card2, card3 = create_list(:card, 3, author: @user1, organization: @org)
    reported_card = create(:card_reporting, card_id: card1.id, user_id: @user1.id)
    trashed_card1 = create(:card_reporting, card_id: card2.id, user_id: @user1.id, deleted_at: Date.today)
    trashed_card2 = create(:card_reporting, card_id: card2.id, user_id: @user1.id, deleted_at: Date.today - 1.day)

    api_v2_sign_in @user
    get :card_reportings, id: card2.id, format: :json
    resp = get_json_response(response)

    assert_response :unauthorized
  end

  test ':api should return error message for cards which doesnt have card reportings' do
    @user1 = create(:user, organization: @org)
    card1, card2, card3 = create_list(:card, 3, author: @user1, organization: @org)
    reported_card = create(:card_reporting, card_id: card1.id, user_id: @user1.id)
    trashed_card1 = create(:card_reporting, card_id: card2.id, user_id: @user1.id, deleted_at: Date.today)
    get :card_reportings, id: card3.id, format: :json
    resp = get_json_response(response)
    expected_response = "Card not found"
    assert_equal expected_response , resp['message']
  end

  # start destroy
  test ':api only creator should be able delete cards' do
    user = create(:user)
    card = create(:card, author_id: user.id, card_type: 'media')
    card1 = create(:card, author_id: @user.id, card_type: 'media')

    assert_no_difference ->{Card.count} do
      delete :destroy, id: card.id
    end

    assert_difference ->{Card.count}, -1 do
      delete :destroy, id: card1.id
      assert_response :ok
    end
  end

  test 'should complete assignment if user_content_completion record is started & assignment is in assigned state' do
    TestAfterCommit.enabled = true
    @card = create(:card, author_id: @user.id)
    @author = create(:user, organization: @org)
    ucc = create(:user_content_completion, user_id: @user.id, completable: @card, state: 'started')
    assignment = create(:assignment, assignable: @card, assignee: @user, state: "assigned")
    team_assignment = create(:team_assignment, assignment: assignment, assignor: @author)

    assert ucc.started?
    assert assignment.assigned?

    post :complete, id: @card.id, state: 'complete', format: :json
    assert_response :success

    assert ucc.reload.completed?
    assert assignment.reload.completed?
  end

  test ':api creator should be able delete cards #member' do
    @org.update(enable_role_based_authorization: true)
    Role.any_instance.unstub :create_role_permissions
    Role.create_standard_roles @org
    @user.roles << @org.roles.find_by_name('member')

    card1 = create(:card, author_id: @user.id, card_type: 'media')

    assert_difference ->{Card.count}, -1 do
      delete :destroy, id: card1.id
      assert_response :ok
    end
  end

  test ':api curator should not be able delete cards #curator' do
    Organization.any_instance.unstub(:create_or_update_roles)
    organization = @user.organization
    organization.update(enable_role_based_authorization: true)
    organization.run_callbacks(:commit)
    @user.role_ids = [organization.roles.where(name: Role::TYPE_CURATOR).first.id]
    @user.save
    card1 = create(:card, author_id: @user.id, card_type: 'media')

    assert_no_difference ->{Card.count} do
      delete :destroy, id: card1.id
      assert_response :unauthorized
    end
  end

  test 'should trigger NotificationCleanUpJob on card destroy' do
    card = create(:card, author_id: @user.id, card_type: 'media')
    NotificationCleanUpJob.expects(:perform_later).with(id: card.id, type: card.class.name).once
    delete :destroy, id: card.id
  end

  test "pins object should be removed" do
    card = create(:card, author_id: @user.id, card_type: 'media')
    ch1 = create(:channel, :robotics, organization: @org)
    ch1.cards << card
    Pin.create(pinnable: ch1, object: card, user: @user)
    assert_equal card.pins.length, 1

    assert_difference -> {Pin.count}, -1 do
      put :destroy, id: card.id, card: {channel_ids: [], message: "test"}, format: :json
      assert_response :ok
    end
  end

  test 'admin should be able to destroy any org card' do
    admin_user = create(:user, organization_role: 'admin', organization: @org)
    admin_card = create(:card, author_id: admin_user.id, card_type: 'media')
    member_card = create(:card, author_id: @user.id, card_type: 'media')
    another_member_card = create(:card, author_id: @user.id, card_type: 'media', state: 'draft')

    api_v2_sign_in(admin_user)

    assert_difference -> {Card.count}, -1 do
      delete :destroy, id: admin_card.id
      assert_response :ok
    end

    assert_difference ->{Card.count}, -1 do
      delete :destroy, id: member_card.id
      assert_response :ok
    end

    assert_difference ->{Card.count}, -1 do
      delete :destroy, id: another_member_card.id
      assert_response :ok
    end
  end

  test 'should respond 404 if card not found for destroy' do
    admin_user = create(:user, organization_role: 'admin', organization: @org)
    api_v2_sign_in(admin_user)

    delete :destroy, id: '-1'
    assert_response :not_found
  end

  test 'member should not be able destroy ecl card' do
    member_user = create(:user, organization_role: 'member', organization: @org)
    api_v2_sign_in(member_user)

    delete :destroy, id: 'ECL-testcard'
    assert_response :not_found
  end

  test 'admin should be able to destroy ecl card' do
    admin_user = create(:user, organization_role: 'admin', organization: @org)
    api_v2_sign_in(admin_user)

    EclCardArchiveJob.expects(:perform_later).with('testcard', @org.id).returns(true)
    delete :destroy, id: 'ECL-testcard'
    assert_response :ok
  end

  test 'admin should be able to destroy card using ecl_id' do
    admin_user = create(:user, organization_role: 'admin', organization: @org)
    create(:card, author_id: nil, card_type: 'media', organization: @org, ecl_id: 'testcard')
    api_v2_sign_in(admin_user)

    assert_difference -> { Card.count }, -1 do
      EclCardArchiveJob.expects(:perform_later).with('testcard', @org.id).returns(true)
      delete :destroy, id: 'ECL-testcard'
    end
    assert_response :ok
  end

  test ':api destroy_cards action' do
    c1 = create(:card, organization: @org, author_id: nil)
    c2 = 321
    assert_difference ->{Card.count}, -1 do
      post :destroy_cards, card_ids: [c1.id, c2], format: :json
    end
    assert_response :ok
    resp = get_json_response(response)
    assert_same_ids [c2], resp['errors']
  end

  test ':api destroy_cards action should not return error if string id in card_ids' do
    c1 = create(:card, organization: @org, author_id: nil)
    assert_difference ->{Card.count}, -1 do
      post :destroy_cards, card_ids: [c1.id.to_s], format: :json
    end
    assert_response :ok
  end
  # ends destroy

  test 'update with topics' do
    card = create(:card, is_public: true, author_id: @user.id)
    card.topics = ["Web", "Ruby", "Java"]
    card.update_topics

    api_v2_sign_in @user
    topics = ["Web", "Ruby"]

    put :update, id: card.id, card: { message: card.message, topics: topics}, format: :json
    card.reload
    assert_same_elements topics, card.tags.collect(&:name)

    put :update, id: card.id, card: { message: "Something" }, format: :json
    card.reload
    assert_same_elements topics, card.tags.collect(&:name)

    put :update, id: card.id, card: { message: card.message, topics: []}, format: :json
    card.reload
    assert_empty card.tags
  end

  test "update topics if key is present and its nil" do

    card = create(:card, is_public: true, author_id: @user.id)
    card.topics = ["Web", "Ruby", "Java"]
    card.update_topics
    api_v2_sign_in @user

    put :update, id: card.id, card: { message: card.message, topics: nil}, format: :json
    card.reload
    assert_empty card.tags
  end

  #complete
  class CardCompleteTests < ActionController::TestCase
    self.use_transactional_fixtures = false
    setup do
      # WebMock.allow_net_connect!
      Assignment.any_instance.stubs(:push_assignment_metric)
      org = create(:organization)
      @user, @author = create_list(:user, 2, organization: org)

      @card = create(:card, author: @author)
      api_v2_sign_in @user
    end

    teardown do
      # WebMock.disable_net_connect!
    end

    test 'should create user content completion #complete' do
      assert_difference -> { UserContentCompletion.count } do
        post :complete, id: @card.id, format: :json
        assert_response :success
      end

      assert_not_nil get_json_response(response)['completed_at']

      ucc = @user.user_content_completions.last
      assert ucc.completed?
    end

    test 'should set previous state #complete' do
      assert_difference -> {UserContentCompletion.count}  do
        post :complete, id: @card.id, format: :json
        assert_response :success
      end

      assert_not_nil get_json_response(response)['completed_at']

      ucc = @user.user_content_completions.last
      assert ucc.completed?
    end

    test 'should CompletionPercentageService after state changes to #complete' do
      CompletionPercentageService.expects(:set_completed_percentage).once
      post :complete, id: @card.id, format: :json
      assert_response :success
      ucc = @user.user_content_completions.last
      assert ucc.completed?
    end

    test 'should create user content completion and user should earn a badge #complete' do
      card_badge = create(:card_badge, organization: @user.organization)
      card_badging = @card.create_card_badging(badge_id: card_badge.id, title: "dummy title")

      assert_difference -> {UserContentCompletion.count}  do
        post :complete, id: @card.id, format: :json, type: 'CardBadge'
        assert_response :success
      end
      resp = get_json_response(response)
      assert_not_nil get_json_response(response)['completed_at']
      assert_not_nil resp['user_badge']
      assert_equal card_badging.id, resp['user_badge']['id']
      ucc = @user.user_content_completions.last
      assert ucc.completed?
    end

    test 'should call UpdateClcScoreForUserJob after user #complete a card' do
      org = @user.organization
      clc = create(:clc,name: 'test clc', entity: org, organization: org)

      UpdateClcScoreForUserJob.expects(:perform_later).with(@card.id, @user.id, org.id, "Organization").once
      post :complete, id: @card.id, format: :json
    end

    test 'should create user content completion #complete #member' do
      org = @user.organization
      org.update(enable_role_based_authorization: true)
      Role.any_instance.unstub :create_role_permissions
      Role.create_standard_roles org
      @user.roles << org.roles.find_by_name('member')

      assert_difference -> {UserContentCompletion.count}  do
        post :complete, id: @card.id, state: 'complete', format: :json
        assert_response :success
      end

      ucc = @user.user_content_completions.last
      assert ucc.completed?
    end

    test 'should create user content completion #complete #curator' do
      Organization.any_instance.unstub :create_or_update_roles

      organization = @user.organization
      organization.update(enable_role_based_authorization: true)
      organization.run_callbacks(:commit)

      @user.role_ids = [organization.roles.where(name: Role::TYPE_CURATOR).first.id]
      @user.save

      assert_no_difference -> { UserContentCompletion.count }  do
        post :complete, id: @card.id, state: 'complete', format: :json
        assert_response :unauthorized
      end
    end

    test 'should create user content completion #complete and #topics' do
      topics = %w[awesome amazing]

      assert_difference -> { UserContentCompletion.count }  do
        post :complete, id: @card.id, topics: topics, format: :json
        assert_response :success
      end

      ucc = @user.user_content_completions.last
      assert ucc.completed?
      assert_equal topics, ucc.topics
    end

    test 'should update assigned cards status to STARTED if it is in ASSIGNED state' do
      assignment = create(:assignment, assignable: @card, assignee: @user)
      team_assignment = create(:team_assignment, assignment: assignment, assignor: @author)

      assert assignment.assigned?

      assert_difference -> { UserContentCompletion.count } do
        put :start, id: @card.id, format: :json
      end

      assignment.reload
      assert_response :success
      assert assignment.started?
    end

    test 'should not #start if card already completed' do
      ucc = create(:user_content_completion, user_id: @user.id, completable: @card, state: 'completed')

      assert_no_difference -> { UserContentCompletion.count } do
        put :start, id: @card.id, format: :json
      end
      assert_response :unprocessable_entity
      res = get_json_response(response)
      assert res['message'].include?('Can not start already started or completed card')
    end

    test 'should start an assignment #member' do
      org = @user.organization
      org.update(enable_role_based_authorization: true)
      Role.any_instance.unstub :create_role_permissions
      Role.create_standard_roles org
      @user.roles << org.roles.find_by_name('member')

      assignment = create(:assignment, assignable: @card, assignee: @user)
      team_assignment = create(:team_assignment, assignment: assignment, assignor: @author)

      assert assignment.assigned?

      # Should update the state of a card that is just ASSIGNED
      put :start, id: @card.id, format: :json
      assert_response :success

      assignment.reload
      assert assignment.started?
    end

    test 'should start an assignment #curator' do
      org = @user.organization

      assignment = create(:assignment, assignable: @card, assignee: @user)
      team_assignment = create(:team_assignment, assignment: assignment, assignor: @author)

      assert assignment.assigned?

      put :start, id: @card.id, format: :json
      assert_response :success

      assignment.reload
      assert assignment.started?
    end

    test 'should update assignment status with completed' do
      assignment = create(:assignment, assignable: @card, assignee: @user)
      team_assignment = create(:team_assignment, assignment: assignment, assignor: @author)

      assert_difference -> {UserContentCompletion.count}  do
        post :complete, id: @card.id, format: :json
        assert_response :success
      end

      ucc = @user.user_content_completions.last
      assert ucc.completed?

      assignment.reload
      assert assignment.completed?
    end

    test 'should not create duplicate user content completions' do
      ucc = create(:user_content_completion, completable: @card, user: @user)
      assert_no_difference -> {UserContentCompletion.count}  do
        post :complete, id: @card.id, format: :json
        assert_response :success
      end
    end

    test 'should not create user content creation with invalid params' do
      assert_no_difference -> {UserContentCompletion.count}  do
        post :complete, id: Faker::Number.number(5), format: :json
        assert_response :not_found
      end
    end

    # test 'should support ECL cards' do
    #   ecl_id = "rerf-rfde"

    #   stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id}").
    #     to_return(status: 200, body: get_ecl_card_response(ecl_id).to_json)

    #   assert_difference -> {UserContentCompletion.count}  do
    #     post :complete, id: "ECL-#{ecl_id}", format: :json
    #     assert_response :ok
    #   end
    # end

    test 'should track mark as completion' do
      assignment = create(:assignment, assignable: @card, assignee: @user)
      request.cookies[:_d] = 'device123'

      @controller.expects(:track_mark_as_completion).once
      assert_difference -> {UserContentCompletion.count} do
        post :complete, id: @card.id, format: :json
      end
    end

    test 'should not update assignment status to complete for mark feature disabled cards' do
      @card.update(ecl_metadata: {'mark_feature_disabled' => true})
      assignment = create(:assignment, assignable: @card, assignee: @user)
      team_assignment = create(:team_assignment, assignment: assignment, assignor: @author)

      post :complete, id: @card.id, format: :json
      assert_response :unprocessable_entity
      assert_equal "feature is disabled for this card", get_json_response(response)["message"]
    end

    test 'should create user content completion #complete with completed_percentage as 100' do
      assert_difference -> { UserContentCompletion.count } do
        post :complete, id: @card.id, format: :json
        assert_response :success
      end
      assert_not_nil get_json_response(response)['completed_at']
      ucc = @user.user_content_completions.last
      assert ucc.completed?
      assert 100, ucc.completed_percentage
    end

    test 'should call CompletionPercentageService once if card is mark as completed.' do
      CompletionPercentageService.expects(:set_completed_percentage).once
      post :complete, id: @card.id, format: :json
      ucc = @user.user_content_completions.last
      assert ucc.completed?
      assert 100, ucc.completed_percentage
    end
  end

  #uncomplete
  class CardUncompleteTests < ActionController::TestCase
    self.use_transactional_fixtures = false
    setup do
      org = create(:organization)
      @user, @author = create_list(:user, 2, organization: org)
      @card1 = create(:card, author: @author)
      @card2 = create(:card, author: @author)
      #completed from 'started' state
      @ucc_from_started = create(:user_content_completion, completable: @card1,
                    user: @user, state: "completed", started_at: Time.now - 2.days,
                    completed_at: Time.now - 1.day)
      #completed from 'initialized' state
      @ucc_from_initialized = create(:user_content_completion, completable: @card2,
                          user: @user, state: "completed", started_at: Time.now - 1.day,
                          completed_at: Time.now - 1.day)


      api_v2_sign_in @user
    end

    test 'should update user content completion to previous state #uncomplete' do

      assert_no_difference -> {UserContentCompletion.count}  do
        post :uncomplete, id: @card1.id, format: :json
        assert_response :success
      end

      assert_nil get_json_response(response)['completed_at']
      @ucc_from_started.reload

      assert @ucc_from_started.initialized?
    end


    test 'should update assignment status with uncomplete' do

      assignment = create(:assignment, assignable: @card2, assignee: @user, state: "completed", started_at: Time.now - 1.day,
                          completed_at: Time.now - 1.day )
      team_assignment = create(:team_assignment, assignment: assignment, assignor: @author)

      assert_no_difference -> {UserContentCompletion.count}  do
        post :uncomplete, id: @card2.id, format: :json
        assert_response :success
      end

      @ucc_from_initialized.reload
      assert @ucc_from_initialized.initialized?

      assignment.reload
      assert assignment.assigned?
    end

    test 'should not update assignment status to uncomplete for mark feature disabled cards' do
      @card1.update(ecl_metadata: {'mark_feature_disabled' => true})

      post :uncomplete, id: @card1.id, format: :json
      assert_response :unprocessable_entity
      assert_equal "feature is disabled for this card", get_json_response(response)["message"]
    end
  end

  # start
  test 'should update assigned cards status to STARTED' do
    TestAfterCommit.enabled = true
    org = create(:organization)
    user, author = create_list(:user, 2, organization: org)

    card = create(:card, author: author)
    api_v2_sign_in user

    assignment = create(:assignment, assignable: card, assignee: user)
    team_assignment = create(:team_assignment, assignment: assignment, assignor: author)

    assert assignment.assigned?

    assert_difference -> {UserContentCompletion.count} do
      put :start, id: card.id, format: :json
    end

    # UserContentCompletion should be in the same state
    ucc = UserContentCompletion.last
    assert_equal UserContentCompletion::STARTED, ucc.state

    assignment.reload
    assert_response :success
    assert_not_nil get_json_response(response)['started_at']
    assert assignment.started?
  end

  test 'should not update assignment status to STARTED for mark feature disabled cards' do
    org = create(:organization)
    user, author = create_list(:user, 2, organization: org)

    card = create(:card, author: author, ecl_metadata: {'mark_feature_disabled' => true})
    api_v2_sign_in user

    assignment = create(:assignment, assignable: card, assignee: user)
    team_assignment = create(:team_assignment, assignment: assignment, assignor: author)

    assert assignment.assigned?

    put :start, id: card.id, format: :json
    assert_response :unprocessable_entity
    assert_equal "feature is disabled for this card", get_json_response(response)["message"]
  end

  # dismiss content
  test ':api should dismiss content' do
    UserContentsQueue.unstub(:remove_from_queue)
    author, user = create_list(:user, 2, organization: @org)
    card = create(:card, author: author)
    UserContentsQueue.expects(:dequeue_for_user).with(user_id: user.id, content_id: card.id, content_type:'Card')

    api_v2_sign_in user
    assert_difference -> {DismissedContent.count} do
      post :dismiss, id: card.id, format: :json
      assert_response :ok
    end

    dismissed_content = DismissedContent.last
    dismissed_content.run_callbacks(:commit)
  end

  test 'dismiss content action should hit load_card_from_compound_id' do
    author, user = create_list(:user, 2)
    card = create(:card, author: author)

    api_v2_sign_in user
    @controller.expects(:load_card_from_compound_id).returns(card)
    post :dismiss, id: card.id, format: :json
    assert_response :ok

    dismissed_content = DismissedContent.last
  end

  test 'should dismiss content #member' do
    Role.any_instance.unstub :create_role_permissions
    @org.update(enable_role_based_authorization: true)
    Role.create_standard_roles @org

    UserContentsQueue.unstub(:remove_from_queue)

    organization = @user.organization
    organization.update(enable_role_based_authorization: true)
    organization.run_callbacks(:commit)

    author, user = create_list(:user, 2, organization: @org)

    card = create(:card, author: author)
    user.roles << organization.roles.find_by(name: 'member')

    UserContentsQueue.expects(:dequeue_for_user).with(user_id: user.id, content_id: card.id, content_type:'Card')

    api_v2_sign_in user
    assert_difference -> {DismissedContent.count} do
      post :dismiss, id: card.id, format: :json
      assert_response :ok
    end

    dismissed_content = DismissedContent.last
    dismissed_content.run_callbacks(:commit)
  end

  test 'should not dismiss content #curator' do
    Organization.any_instance.unstub(:create_or_update_roles)

    organization = @user.organization
    organization.update(enable_role_based_authorization: true)
    organization.run_callbacks(:commit)

    author, user = create_list(:user, 2, organization: @org)
    user.role_ids = [organization.roles.where(name: Role::TYPE_CURATOR).first.id]
    user.save

    card = create(:card, author: author)

    api_v2_sign_in user
    assert_no_difference -> {DismissedContent.count} do
      post :dismiss, id: card.id, format: :json
      assert_response :unauthorized
    end
  end

  # curate
  test 'should curate a card in a channel' do
    org = @user.organization
    signed_in_user = create(:user, organization: org)
    user = create(:user, organization: org)
    channel = create(:channel, organization: org, curate_only: true)
    channel.add_curators([signed_in_user])
    card = create(:card, author: nil, organization: org)
    card.channels << channel
    api_v2_sign_in(signed_in_user)
    ChannelCardEnqueueJob.expects(:perform_later).once

    put :curate, channel_id: channel.id, id: card.id, format: :json

    assert ChannelsCard.where(card: card, channel: channel).first.curated?
  end

  test 'should return error for state `skip` if invalid state change' do
    org = @user.organization
    channel = create(:channel, organization: org, curate_only: true)
    channel.add_curators([@user])
    card = create(:card, author: @user, organization: org)
    card.channels << channel
    channel.channels_cards.last.skip!
    api_v2_sign_in(@user)

    put :skip, channel_id: channel.id, id: card.id, format: :json
    assert_response :unprocessable_entity
    resp = get_json_response(response)
    assert_match /Skip can not be set for skipped/, resp['message']
  end

  test 'should return error for state `curate` if invalid state change' do
    org = @user.organization
    channel = create(:channel, organization: org, curate_only: true)
    channel.add_curators([@user])
    card = create(:card, author: @user, organization: org)
    card.channels << channel
    channel.channels_cards.last.curate!
    api_v2_sign_in(@user)

    put :curate, channel_id: channel.id, id: card.id, format: :json
    assert_response :unprocessable_entity
    resp = get_json_response(response)
    assert_match /Curate can not be set for curated/, resp['message']
  end

  test 'should return error for state `archive` if invalid state change' do
    org = @user.organization
    channel = create(:channel, organization: org, curate_only: true)
    channel.add_curators([@user])
    card = create(:card, author: @user, organization: org)
    card.channels << channel
    channel.channels_cards.last.archive!
    api_v2_sign_in(@user)

    put :archive, channel_id: channel.id, id: card.id, format: :json
    assert_response :unprocessable_entity
    resp = get_json_response(response)
    assert_match /Archive can not be set for archived/, resp['message']
  end

  test 'should not curate a card in a channel #member' do
    Organization.any_instance.unstub(:create_or_update_roles)

    org = @user.organization
    org.update(enable_role_based_authorization: true)
    org.run_callbacks(:commit)

    signed_in_user = create(:user, organization: org)

    user = create(:user, organization: org)
    channel = create(:channel, organization: org, curate_only: true)
    channel.add_curators([signed_in_user])

    card = create(:card, organization_id: org.id, author: nil)
    card.channels << channel

    api_v2_sign_in(signed_in_user)

    put :curate, channel_id: channel.id, id: card.id, format: :json
    assert_response :unauthorized

    refute ChannelsCard.where(card: card, channel: channel).first.curated?
  end

  test 'should curate a card in a channel #curator' do
    Role.any_instance.unstub :create_role_permissions

    org = @user.organization
    org.update(enable_role_based_authorization: true)
    Role.create_standard_roles org

    signed_in_user = create(:user, organization: org)
    signed_in_user.role_ids = [org.roles.where(name: Role::TYPE_CURATOR).first.id]
    signed_in_user.save

    user = create(:user, organization: org)
    channel = create(:channel, organization: org, curate_only: true)
    channel.add_curators([signed_in_user])

    card = create(:card, organization: org, author: nil)
    card.channels << channel

    api_v2_sign_in(signed_in_user)

    put :curate, channel_id: channel.id, id: card.id, format: :json

    assert ChannelsCard.where(card: card, channel: channel).first.curated?
  end

  test 'should curate a card in an open channel by follower' do
    org = create(:organization)
    Organization.any_instance.unstub(:create_or_update_roles)
    org.update(enable_role_based_authorization: true)
    org.run_callbacks(:commit)

    signed_in_user = create(:user, organization: org)
    signed_in_user.role_ids = [org.roles.where(name: Role::TYPE_CURATOR).first.id]
    signed_in_user.save

    user = create(:user, organization: org)
    channel = create(:channel, organization: org, curate_only: true, is_open: true)
    channel.add_followers([signed_in_user])

    card = create(:card, organization: org, author: nil)
    card.channels << channel

    api_v2_sign_in(signed_in_user)

    put :curate, channel_id: channel.id, id: card.id, format: :json

    assert ChannelsCard.where(card: card, channel: channel).first.curated?
  end

  # TODO: DO NOT DO MULTIPLE REQUESTS IN SINGLE TEST
  test 'should only allow access to channel curator' do
    org = create(:organization)
    signed_in_user = create(:user, organization: org)
    user = create(:user, organization: org)
    curator = create(:user, organization: org)

    channel = create(:channel, organization: org, curate_only: true)
    channel.add_curators([curator])

    card = create(:card, author: user)
    card.channels << channel

    api_v2_sign_in(signed_in_user)

    put :archive, channel_id: channel.id, id: card.id, format: :json
    assert_response :unauthorized

    put :skip, channel_id: channel.id, id: card.id, format: :json
    assert_response :unauthorized

    put :curate, channel_id: channel.id, id: card.id, format: :json
    assert_response :unauthorized
  end

  test 'addable pathways should support ecl id' do
    ecl_id = "ecl-card"

    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id}").
      to_return(status: 200, body: get_ecl_card_response(ecl_id).to_json)

    assert_difference -> {Card.count} do
      get :addable_pathways, id: "ECL-#{ecl_id}", format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal [], resp['cards']
    end
  end

  # skip
  test 'should skip a card in a channel' do
    org = create(:organization)
    signed_in_user = create(:user, organization: org)
    user = create(:user, organization: org)
    channel = create(:channel, organization: org, curate_only: true)
    channel.add_curators([signed_in_user])
    card = create(:card, author: nil, organization_id: org.id)
    card.channels << channel
    api_v2_sign_in(signed_in_user)

    put :skip, channel_id: channel.id, id: card.id, format: :json

    assert ChannelsCard.where(card: card, channel: channel).first.skipped?
  end

  test 'should call record_card_rejected_event when skipping card' do
    org = create(:organization)
    signed_in_user = create(:user, organization: org)
    user = create(:user, organization: org)
    channel = create(:channel, organization: org, curate_only: true)
    channel.add_curators([signed_in_user])
    card = create(:card, author: nil, organization_id: org.id)
    card.channels << channel
    api_v2_sign_in(signed_in_user)

    @controller.expects(:record_card_rejected_event).once
    put :skip, channel_id: channel.id, id: card.id, format: :json
  end

  test 'should call record_card_approved_event when approving card' do
    org = create(:organization)
    signed_in_user = create(:user, organization: org)
    user = create(:user, organization: org)
    channel = create(:channel, organization: org, curate_only: true)
    channel.add_curators([signed_in_user])
    card = create(:card, author: nil, organization_id: org.id)
    card.channels << channel
    api_v2_sign_in(signed_in_user)

    @controller.expects(:record_card_approved_event).once
    put :curate, channel_id: channel.id, id: card.id, format: :json
  end

  test 'record_card_rejected_event should call record_card_moderation_event' do
    event = 'card_rejected_for_channel'
    @controller.expects(:record_card_moderation_event).with(event)
    @controller.send(:record_card_rejected_event)
  end

  test 'record_card_approved_event should call record_card_moderation_event' do
    event = 'card_approved_for_channel'

    @controller.expects(:record_card_moderation_event).with(event)
    @controller.send(:record_card_approved_event)
  end

  test "record_card_moderation_event calls job" do
    org = create(:organization)
    actor = create :user, organization: org
    card = create :card, organization: org, author: actor
    channel = create :channel, organization: org
    channels_card = ChannelsCard.new(card: card, channel: channel)
    @controller.instance_variable_set("@channels_card", channels_card)
    @controller.stubs(:current_user).returns actor
    metadata = {
      platform: "platform",
      user_agent: "user_agent",
      platform_version_number: "platform_version_number",
      user_id: actor.id
    }
    RequestStore.stubs(:read).with(:request_metadata).returns(metadata)
    stub_time = Time.now
    Analytics::MetricsRecorderJob.unstub :perform_later
    Analytics::MetricsRecorderJob.expects(
      :perform_later
    ).once.with(
      recorder: "Analytics::CardModerationMetricsRecorder",
      timestamp: stub_time.to_i,
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      card: Analytics::MetricsRecorder.card_attributes(card),
      channel: Analytics::MetricsRecorder.channel_attributes(channel),
      org: Analytics::MetricsRecorder.org_attributes(org),
      event: "card_approved_for_channel",
      additional_data: metadata
    )
    Analytics::CardMetricsRecorder.stubs :record
    Timecop.freeze(stub_time) do
      @controller.send(:record_card_moderation_event, "card_approved_for_channel")
    end
  end

  test 'should not skip a card in a channel #member' do
    org = create(:organization)
    org.update(enable_role_based_authorization: true)
    org.run_callbacks(:commit)

    signed_in_user = create(:user, organization: org)

    user = create(:user, organization: org)
    channel = create(:channel, organization: org, curate_only: true)
    channel.add_curators([signed_in_user])

    card = create(:card, organization: org, author: nil)
    card.channels << channel

    api_v2_sign_in(signed_in_user)

    put :skip, channel_id: channel.id, id: card.id, format: :json
    assert_response :unauthorized

    refute ChannelsCard.where(card: card, channel: channel).first.skipped?
  end

  test 'should skip a card in a channel #curator' do
    org = @user.organization
    org.update(enable_role_based_authorization: true)
    Role.any_instance.unstub :create_role_permissions
    Role.create_standard_roles org

    signed_in_user = create(:user, organization: org)
    signed_in_user.role_ids = [org.roles.where(name: Role::TYPE_CURATOR).first.id]
    signed_in_user.save

    user = create(:user, organization: org)
    channel = create(:channel, organization: org, curate_only: true)
    channel.add_curators([signed_in_user])

    card = create(:card, author: nil, organization: org)
    card.channels << channel

    api_v2_sign_in(signed_in_user)

    put :skip, channel_id: channel.id, id: card.id, format: :json
    assert_response :ok

    assert ChannelsCard.where(card: card, channel: channel).first.skipped?
  end

  # archive
  test 'should archive a card in a channel' do
    signed_in_user = create(:user, organization: @org)
    channel = create(:channel, organization: @org, curate_only: true)
    channel.add_curators([signed_in_user])
    card = create(:card, organization: @org, author: nil)
    card.channels << channel
    api_v2_sign_in(signed_in_user)

    put :archive, channel_id: channel.id, id: card.id, format: :json

    assert ChannelsCard.where(card: card, channel: channel).first.archived?
  end

  test 'should not archive a card in a channel #member' do
    @org.update(enable_role_based_authorization: true)
    @org.run_callbacks(:commit)

    signed_in_user = create(:user, organization: @org)

    channel = create(:channel, organization: @org, curate_only: true)
    channel.add_curators([signed_in_user])

    card = create(:card, author: @user)
    card.channels << channel

    api_v2_sign_in(signed_in_user)
    put :archive, channel_id: channel.id, id: card.id, format: :json
    assert_response :unauthorized

    refute ChannelsCard.where(card: card, channel: channel).first.archived?
  end

  test 'should archive a card in a channel #curator' do
    Role.any_instance.unstub :create_role_permissions
    Role.create_standard_roles @org

    signed_in_user = create(:user, organization: @org)
    signed_in_user.role_ids = [@org.roles.where(name: Role::TYPE_CURATOR).first.id]
    signed_in_user.save

    channel = create(:channel, organization: @org, curate_only: true)
    channel.add_curators([signed_in_user])

    card = create(:card, author: @user)
    card.channels << channel

    api_v2_sign_in(signed_in_user)
    put :archive, channel_id: channel.id, id: card.id, format: :json
    assert_response :ok

    assert ChannelsCard.where(card: card, channel: channel).first.archived?
  end

  test ':api archive_cards action' do
    c1, c2 = create_list(:card, 2, state: 'new', organization: @org, author_id: nil)
    c2.archive!
    post :archive_cards, card_ids: [c1.id, c2.id], format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_includes resp['successes'], c1.id
    assert_includes resp['errors'], c2.id
    assert c1.reload.archived?
  end

  class CardMetric < ActionController::TestCase
    setup do
      @user = create(:user)
      @org = @user.organization
      @card = create(:card, author: @user)
      api_v2_sign_in @user
    end

    test ':api metric' do
      Card.any_instance.unstub(:all_time_views_count)
      Card.any_instance.expects(:all_time_views_count).returns(28)

      create_list(:user, 3, organization: @org).each do |user|
        create(:comment, commentable: @card, user: user)
      end
      voters = create_list(:user, 2, organization: @org).each do |user|
        create(:vote, votable: @card, voter: user)
      end

      create_list(:user, 4, organization: @org).each do |user|
        create(:bookmark, bookmarkable: @card, user: user)
      end
      create_list(:user_content_completion, 1, completable: @card, state: 'completed', user: @org.users.last)

      get :metric, id: @card.id, format: :json
      assert_response :ok
      json_response = get_json_response(response)

      assert_equal(
        { 'bookmarks_count' => 4, 'comments_count' => 3, 'likes_count' => 2, 'views_count' => 28, 'completes_count' => 1 },
        json_response
      )
    end

    test ':api metric #no_view_event' do
      Card.any_instance.unstub(:all_time_views_count)
      Card.any_instance.expects(:all_time_views_count).returns(0)

      create_list(:user_content_completion, 1, completable: @card, state: 'completed', user: @org.users.last)

      get :metric, id: @card.id, format: :json
      assert_response :ok
      json_response = get_json_response(response)

      assert_equal(
        { 'bookmarks_count' => 0, 'comments_count' => 0, 'likes_count' => 0, 'views_count' => 0, 'completes_count' => 1 },
        json_response
      )
    end

    test ':api metric to show poll question specific attributes' do
      Card.any_instance.unstub(:all_time_views_count)
      Card.any_instance.expects(:all_time_views_count).returns(2)

      card = create(:card, card_type: 'poll', author: @user)
      option1 = create(:quiz_question_option, label: 'a', quiz_question: card, is_correct: true)
      option2 = create(:quiz_question_option, label: 'b', quiz_question: card)
      attempt = create(:quiz_question_attempt, user: @user, quiz_question: card, selections: [option1.id])
      stats = create(:quiz_question_stats, quiz_question: card, attempt_count: 1, options_stats: {option1.id.to_s => 1})
      get :metric, id: card.id, format: :json
      assert_response :ok
      json_response = get_json_response(response)
      assert_equal(
        { 'bookmarks_count' => 0, 'comments_count' => 0, 'likes_count' => 0, 'views_count' => 2,
          'completes_count' => 0, 'total_answers_count' => 1, 'correct_answers' => 1, 'wrong_answers' => 0 },
        json_response
      )
    end

    test 'should allow only card authors and users of maps with permission: `VIEW_CARD_ANALYTICS`' do
      INFLUXDB_CLIENT.stubs(:query).returns [{"name"=>"cards", "tags"=>nil, "values"=>[{"time"=>"1970-01-01", "count"=>2}]}]

      get :metric, id: @card.id, format: :json
      assert_response :ok
      sign_out @user
      create_org_master_roles @user.organization
      other_user = create(:user, organization: @user.organization, organization_role: 'curator')
      api_v2_sign_in(other_user)
      get :metric, id: @card.id, format: :json
      assert_response :unauthorized
      assert_equal "Permission denied", get_json_response(response)["message"]
      get :time_range_analytics, id: @card.id, action_types: ['like'], format: :json
      assert_response :unauthorized
      assert_equal "Permission denied", get_json_response(response)["message"]
      other_user.add_role('member')
      assert_includes other_user.get_user_permissions, Permissions::VIEW_CARD_ANALYTICS
      get :metric, id: @card.id, format: :json
      assert_response :ok
    end

    test 'should check if card exists or not' do
      get :metric, id: -1, format: :json

      assert_response :not_found
      assert_equal "Record not found", get_json_response(response)["message"]

      org = create(:organization)
      user = create(:user, organization: org)
      other_org_card = create(:card, organization: org, author: user)
      get :metric, id: other_org_card.id, format: :json

      assert_response :not_found
      assert_equal "Record not found", get_json_response(response)["message"]
    end

    test ':api metric_users' do
      @org.roles.create(name: 'member')
      voter = create(:user, organization: @org)
      voter.roles << @org.roles.find_by(name: 'member')
      vote  = create(:vote, votable: @card, voter: voter)

      get :metric_users, id: @card.id, action_type: 'like', format: :json
      assert_response :ok
      json_response = get_json_response(response)

      assert_not_empty json_response
      assert_equal voter.id, json_response.first['id']
      assert_equal voter.get_user_roles, json_response.first['roles']
    end

    test 'should list users with pagination' do
      voters = create_list(:user, 20, organization: @org).map do |user|
        create(:vote, votable: @card, voter: user)
      end

      get :metric_users, id: @card.id, action_type: 'like', format: :json

      assert_response :ok
      json_response = get_json_response(response)

      assert_not_empty json_response
      assert_equal 10, json_response.length
      assert_same_elements voters.first(10).map(&:user_id), json_response.map{|user| user['id']}

      get :metric_users, id: @card.id, action_type: 'like', limit: 10, offset: 10, format: :json

      assert_response :ok
      json_response = get_json_response(response)

      assert_not_empty json_response
      assert_equal 10, json_response.length
      assert_same_elements voters.last(10).map(&:user_id), json_response.map{|user| user['id']}
    end

    test 'should list users with pagination for views count' do
      skip #TODO: refactor this method. stub all calls to metric recorder
      WebMock.disable!
      MetricRecord.create_index! force: true

      users = create_list(:user, 20)
      users.each do |user|
        MetricRecord.new(actor_id: user.id, object_id: @card.id, object_type: 'card', action: "impression").save
      end

      MetricRecord.gateway.refresh_index!

      get :metric_users, id: @card.id, action_type: 'view', format: :json

      assert_response :ok
      json_response = get_json_response(response)

      assert_not_empty json_response
      assert_same_elements users.first(10).map(&:id), json_response.map{|user| user['id']}

      get :metric_users, id: @card.id, action_type: 'view', limit: 10, offset: 10, format: :json

      assert_response :ok
      json_response = get_json_response(response)

      assert_not_empty json_response
      assert_same_elements users.last(10).map(&:id), json_response.map{|user| user['id']}

      # collection card
      collection_card = create(:card, author: @user, organization: @user.organization, card_type: 'pack')

      users.each do |user|
        MetricRecord.new(actor_id: user.id, object_id: collection_card.id, object_type: 'collection', action: "impression").save
      end

      get :metric_users, id: collection_card.id, action_type: 'view', format: :json

      assert_response :ok
      json_response = get_json_response(response)

      assert_not_empty json_response
      assert_same_elements users.first(10).map(&:id), json_response.map{|user| user['id']}

      # video stream card
      video_stream = create(:iris_video_stream, state: 'published')
      video_stream_card = create(:card, video_stream: video_stream, author: @user, organization: @user.organization, card_type: 'video_stream')

      users.each do |user|
        MetricRecord.new(actor_id: user.id, object_id: video_stream.id, object_type: 'video_stream', action: "impression").save
      end

      get :metric_users, id: video_stream_card.id, action_type: 'view', format: :json

      assert_response :ok
      json_response = get_json_response(response)

      assert_not_empty json_response
      assert_same_elements users.first(10).map(&:id), json_response.map{|user| user['id']}

      WebMock.enable!
    end

    test 'should list users for mark as complete count' do
      users = create_list(:user, 10, organization: @org)

      users.each do |user|
        create(:user_content_completion, user_id: user.id,  completable_id: @card.id, completable_type: 'Card', state: 'completed')
      end

      get :metric_users, id: @card.id, action_type: 'mark_as_complete', offset: 5, limit: 5, format: :json
      assert_response :ok

      json_response = get_json_response(response)
      assert_not_empty json_response
      assert_same_elements users.last(5).map(&:id), json_response.map{|user| user['id']}
    end
  end

  class CardsUserMetic < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      @card = create(:card, author: @user)
      @video_stream_card = create(:card, author: @user)
      @video_stream = create(:wowza_video_stream, card: @video_stream_card)
      @card_pack = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user)
    end

    teardown do
    end

    test ':api list of metrics for user' do
      Timecop.freeze do
        api_v2_sign_in @user

        now = Time.now.utc
        applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

        applicable_offsets.each do |k, v|
          card_score = create(:user_content_level_metric, user_id: @user.id, content_id: @card.id, content_type: "card", smartbites_score: 20, period: k, offset: v)
          video_stream_score = create(:user_content_level_metric, user_id: @user.id, content_id: @video_stream_card.id, content_type: "card", smartbites_score: 40, period: k, offset: v)
          collection_score = create(:user_content_level_metric, user_id: @user.id, content_id: @card_pack.id, content_type: "collection", smartbites_score: 30, period: k, offset: v)
        end

        get :cards_user_metric, format: :json
        assert_response :ok
        resp = get_json_response(response)
        assert_equal [@video_stream_card, @card_pack, @card].map(&:id), resp['cards'].map{|i| i['id']}
        assert_equal [@video_stream_card, @card_pack, @card].map(&:snippet), resp['cards'].map{|i| i['title']}
        assert_equal [40,30,20], resp['cards'].map{|i| i['score']}
      end
    end

    test 'blank state' do
      api_v2_sign_in @user

      get :cards_user_metric, format: :json
      assert_response :ok

      resp = get_json_response(response)
      assert_equal({"cards" => []}, resp)
    end

    test 'send deleted key' do
      Timecop.freeze do
        now = Time.now.utc
        applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

        create(:user_content_level_metric, user_id: @user.id, content_id: 9999, content_type: "card", smartbites_score: 20, period: :all_time, offset: applicable_offsets[:all_time])
        create(:user_content_level_metric, user_id: @user.id, content_id: @card.id, content_type: "card", smartbites_score: 10, period: :all_time, offset: applicable_offsets[:all_time])

        api_v2_sign_in @user
        get :cards_user_metric, format: :json

        assert_response :ok
        resp = get_json_response(response)
        assert_equal [true, false], resp['cards'].map{|i| i['is_deleted']}
      end
    end

  end

  # time_range_analytics
  test 'should return info about when every user add card to channel' do
    card = create(:card, author: @user, organization: @user.organization)
    channel = create(:channel, organization: @user.organization)
    card.channels << channel

    report_mock = mock()
    AnalyticsReport::ContentActivityReport.expects(:new).with({card_id: card.id, from_date: nil,
          to_date: nil, action_types: ['add_to_channel']}).returns(report_mock)

    report_mock.expects(:generate_report).returns({total: 1,
                                                    data: [
                                                            {channel_id: channel.id,
                                                              curated_at: ChannelsCard.find_by(card_id: card.id).curated_at}
                                                          ]
                                                  })

    get :time_range_analytics, id: card.id, action_types: ['add_to_channel'], format: :json

    assert_response :ok
    resp = get_json_response(response)

    assert_equal 1, resp['metrics']['total']
    assert_equal channel.id, resp['metrics']['data'][0]['channel_id']
    assert_equal ChannelsCard.find_by(card_id: card.id).curated_at, resp['metrics']['data'][0]['curated_at']
  end

  # new_content_count
  test 'admin can get number of new cards in curated, featured and required content' do
    Role.any_instance.unstub :create_role_permissions
    Role.create_standard_roles @org

    @user.roles << @org.roles.where(name: 'admin').first

    last_access_at = (Time.now - 1.hour).to_s
    content_types = ['featured', 'curated', 'required']

    count_response = {
        featured_count: Faker::Number.number(10),
        curated_count: Faker::Number.number(10),
        required_count: Faker::Number.number(10)
    }

    mock = mock()
    AnalyticsReport::NewContentReport.expects(:new)
        .with(last_access: last_access_at, content_types: content_types, viewer: @user).returns mock
    mock.expects(:generate_report).returns(count_response)

    get :new_content_count, format: :json, last_access_at: last_access_at, content_types: content_types
    assert_response :ok

    resp = get_json_response(response)
    assert_equal count_response[:featured_count], resp['featured_count']
    assert_equal count_response[:curated_count], resp['curated_count']
    assert_equal count_response[:required_count], resp['required_count']
  end

  test 'curator can get number of new cards in curated, featured and required content' do
    role = create(:role, name: 'curator', organization: @org)
    role.role_permissions.find_or_create_by(name: 'CURATE_CONTENT').enable!
    @user.roles << role

    last_access_at = (Time.now - 1.hour).to_s
    content_types = ['featured', 'curated', 'required']

    count_response = {
        featured_count: Faker::Number.number(10),
        curated_count: Faker::Number.number(10),
        required_count: Faker::Number.number(10)
    }

    mock = mock()
    AnalyticsReport::NewContentReport.expects(:new)
        .with(last_access: last_access_at, content_types: content_types, viewer: @user).returns mock
    mock.expects(:generate_report).returns(count_response)


    get :new_content_count, format: :json, last_access_at: last_access_at, content_types: content_types
    assert_response :ok

    resp = get_json_response(response)
    assert_equal count_response[:featured_count], resp['featured_count']
    assert_equal count_response[:curated_count], resp['curated_count']
    assert_equal count_response[:required_count], resp['required_count']
  end

  test 'member can`t get number of new cards in curated content' do
    last_access_at = (Time.now - 1.hour).to_s

    get :new_content_count, format: :json, last_access_at: last_access_at, content_types: ['curated']
    assert_response :ok

    resp = get_json_response(response)
    assert_empty resp
  end

  test 'member can get number of new cards in featured and required content' do
    last_access_at = (Time.now - 1.hour).to_s
    content_types = ['featured', 'required']

    count_response = {
        featured_count: Faker::Number.number(10),
        required_count: Faker::Number.number(10)
    }

    mock = mock()
    AnalyticsReport::NewContentReport.expects(:new)
        .with(last_access: last_access_at, content_types: content_types, viewer: @user).returns mock
    mock.expects(:generate_report).returns(count_response)

    get :new_content_count, format: :json, last_access_at: last_access_at, content_types: content_types
    assert_response :ok

    resp = get_json_response(response)
    assert_equal count_response[:featured_count], resp['featured_count']
    assert_equal count_response[:required_count], resp['required_count']
  end

  test 'returns error if params is missing' do
    get :new_content_count, format: :json
    assert_response :unprocessable_entity
  end

  test 'load cards from compound_ids should return cards' do
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')
    cards = create_list(:card, 3, author_id: @user.id, card_type: 'media')
    ids = cards.map(&:id)

    post :batch_add, id: cover.id, content_ids: ids.flatten, content_type: 'Card', format: :json
    assert_response :ok

    card_pack = CardPackRelation.where(cover_id: cover.id)

    assert_equal 4, card_pack.count
    assert_equal 1, card_pack.where(to_id: nil).count
    assert_same_ids [cover.id] + cards.map(&:id), card_pack.map(&:from_id)
  end

  test 'should return error if blank content_ids after check_compound_ids' do
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')
    ids = ['', nil, {}]

    post :batch_add, id: cover.id, content_ids: ids, content_type: 'Card', format: :json
    assert_response :unprocessable_entity
  end

  test 'should return error if blank content_ids' do
    cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')
    post :batch_add, id: cover.id, content_ids: [], content_type: 'Card', format: :json
    resp = get_json_response(response)
    assert_response :unprocessable_entity
    assert_equal 'Missing required params', resp['message']
  end

  class CardLock < ActionController::TestCase
    setup do
      org = create(:organization)
      @user = create(:user, organization: org)
      @cover = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user)
      @card1, @card2, @card3 = create_list(:card, 3, author: @user)
      @cards = @cover, @card1, @card2, @card3
      [@card1, @card2, @card3].each do |card|
        CardPackRelation.add(cover_id: @cover.id, add_id: card.id, add_type: card.class.name)
      end
      @card4 = create(:card, author: @user)
      api_v2_sign_in @user
    end

    test ':api should create lock on card in pathway' do
      post :set_lock, format: :json, id: @card1.id, locked: 'true', pathway_id: @cover.id
      assert_response :ok
      cpr = CardPackRelation.find_by(from_id: @card1.id, cover_id: @cover.id)
      assert cpr.locked
    end

    test ':api should not create lock on card with wrong parameters' do
      post :set_lock, format: :json, id: @card1.id
      resp = get_json_response(response)
      assert_response :unprocessable_entity
      assert_equal "Missing required params", resp['message']
    end

    test ':api pathway should exist' do
      post :set_lock, format: :json, id: @card1.id, pathway_id: 99999, locked: true
      resp = get_json_response(response)
      assert_response :unprocessable_entity
      assert_equal 'Pathway card not found', resp['message']
    end

    test ':api card should be in provided pathway' do
      post :set_lock, format: :json, id: @card4.id, pathway_id: @cover.id, locked: true
      resp = get_json_response(response)
      assert_response :unprocessable_entity
      assert_equal 'Card should be in provided pathway', resp['message']
    end
  end

  class CardGroupsTest < ActionController::TestCase
    setup do
      IndexJob.stubs :perform_later
      @organization = create :organization
      @user = create :user, organization: @organization
      api_v2_sign_in @user
    end

    test 'should accept group_ids while creating a new card' do
      team1 = create :team, name: 'Group 1', organization: @organization
      team2 = create :team, name: 'Group 2', organization: @organization

      # Add user to the team
      team1.add_member @user
      team2.add_member @user

      assert_difference -> {Card.count}, 1 do
        post :create, card: {team_ids: [team1.id, team2.id], message: 'Message'}, format: :json
      end
      assert_response :ok

      json_response = get_json_response response

      assert_equal 'Message', json_response['message']
      assert_equal [team1.id, team2.id], json_response['teams'].map{|x| x['id']}
    end

    test 'should filter group_ids that the user does not belong to during create' do
      team1 = create :team, name: 'Group 1', organization: @organization
      team2 = create :team, name: 'Group 2', organization: @organization

      assert_difference -> {Card.count}, 1 do
        post :create, card: {team_ids: [team1.id, team2.id], message: 'Message'}, format: :json
      end
      assert_response :ok

      json_response = get_json_response response

      assert_equal 'Message', json_response['message']
      assert_equal [], json_response['teams'].map{|x| x['id']}
    end
  end

  class CardUserAccessTest < ActionController::TestCase
    setup do
      @organization = create :organization
      @user = create :user, organization: @organization
      api_v2_sign_in @user
    end

    test 'should accept user_access_ids when creating a new card' do
      user1 = create :user, organization: @organization
      user2 = create :user, organization: @organization

      assert_difference -> {Card.count}, 1 do
        post :create, card: {users_with_access_ids: [user1.id, user2.id], message: 'Message'}, format: :json
      end
      assert_response :ok

      json_response = get_json_response response

      assert_equal 'Message', json_response['message']
      assert_equal [user1.id, user2.id], json_response['users_with_access'].map{|x| x['id']}
    end
  end

  class IsPublicTest < ActionController::TestCase
    setup do
      @organization = create :organization
      @user = create :user, organization: @organization
      api_v2_sign_in @user
    end

    test 'is_public should be true by default' do
      assert_difference -> {Card.count}, 1 do
        post :create, card: {message: 'Message'}, format: :json
      end
      assert_response :ok

      json_response = get_json_response response

      assert_equal 'Message', json_response['message']
      assert json_response['is_public']
    end

    test 'is_public should be set to true if true is passed' do
      assert_difference -> {Card.count}, 1 do
        post :create, card: {message: 'Message', is_public: 'true'}, format: :json
      end
      assert_response :ok

      json_response = get_json_response response

      assert_equal 'Message', json_response['message']
      assert json_response['is_public']
    end

    test 'is_public should be set to false if false is passed' do
      assert_difference -> {Card.count}, 1 do
        post :create, card: {message: 'Message', is_public: 'false'}, format: :json
      end
      assert_response :ok

      json_response = get_json_response response

      assert_equal 'Message', json_response['message']
      assert_not json_response['is_public']
    end
  end

  class JourneyCardTests < ActionController::TestCase

    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)

      @journey = create(
          :card, card_type: 'journey', card_subtype: 'self_paced',
          author: @user, organization_id: @org.id
      )
      @section1, @section2, @section3 = create_list(
        :card, 3, hidden: true, state: 'draft', card_type: 'pack',
        card_subtype: 'simple', author: @user, organization_id: @org.id
      )
      @sections = @section1, @section2, @section3
      @journey_pack = @journey, @section1, @section2, @section3
      @card1, @card2, @card3 = create_list(:card, 3, hidden: true, organization: @org, author: @user)
      @cards = @card1, @card2, @card3
      CardPackRelation.add(cover_id: @section1.id, add_id: @card1.id, add_type: @section1.class.name)
      CardPackRelation.add(cover_id: @section2.id, add_id: @card2.id, add_type: @section1.class.name)
      CardPackRelation.add(cover_id: @section3.id, add_id: @card3.id, add_type: @section1.class.name)

      @pathway = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user, organization_id: @org.id)
      @card4 = create(:card, hidden: true, organization: @org, author: @user)
      CardPackRelation.add(cover_id: @pathway.id, add_id: @card4.id, add_type: @pathway.class.name)

      @sections.each do |s|
        JourneyPackRelation.add(cover_id: @journey.id, add_id: s.id)
      end

      api_v2_sign_in @user

      Card.any_instance.stubs(:remove_from_queue_and_reenqueue)
      stub_request(:post, 'http://localhost:9200/cards_test/_refresh')
    end

    test ':api should return correct pathway completion percentage' do
      post :complete, id: @card1.id, format: :json
      assert_response :ok
      get :show, id: @journey.id, format: :json
      assert_response :ok
      res = get_json_response(response)
      assert_equal res["journey_section"].find{|i| i['card_id'] == @section1.id}['completed_percentage'], 100
    end

    test ':api should return correct journey completion percentage' do
      post :complete, id: @card1.id, format: :json
      assert_response :ok
      get :show, id: @journey.id, format: :json
      assert_response :ok
      res = get_json_response(response)
      assert_equal res["completed_percentage"], 33
    end

    test ':api should reorder journey sections with proper parameters' do
      put :journey_reorder, id: @journey.id, from: 3, to: 1, format: :json
      assert_response :ok
      reordered_ids = @journey.id, @section3.id, @section1.id, @section2.id
      assert_same_ids reordered_ids, JourneyPackRelation.journey_relations(cover_id: @journey.id).collect(&:from_id)
    end

    test ':api should response with 422 on reorder journey action with wrong parameters' do
      put :journey_reorder, id: @journey.id, from: 3, to: 3, format: :json
      assert_response :unprocessable_entity
    end

    test ':api should expose hidden cards in journey if delete_dependent parameter false or not passed' do
      assert_difference -> { Card.count }, -4 do
        delete :destroy, id: @journey.id, format: :json
      end
      assert_response :ok
      refute Card.where(id: @cards.map(&:id)).detect(&:hidden)
    end

    test ':api should expose hidden in section' do

      assert @section1.hidden

      assert_no_difference ->{Card.count} do
        delete :journey_remove, id: @journey.id,
          content_id: @section1.id, delete_dependent: false, format: :json
      end
      assert_response :ok

      @section1.reload

      refute @section1.hidden
      assert @section1.pack_draft?
    end

    test ':api should not delete not user owned pathway' do
      org = create(:organization)
      public_user = create(:user, organization: org)
      card = create(:card, hidden: true, organization: org, author: public_user)
      pathway = create(:card, card_type: 'pack', card_subtype: 'simple',
        author: public_user, organization_id: org.id
      )
      CardPackRelation.add(cover_id: pathway.id, add_id:
        card.id, add_type: pathway.class.name
      )
      JourneyPackRelation.add(cover_id: @journey.id, add_id: pathway.id)
      assert_no_difference -> { Card.count } do
        delete :journey_remove, id: @journey.id,
               content_id: pathway.id, delete_dependent: true, format: :json
      end
      assert_response :ok
      assert Card.exists?(id: pathway.id)
      assert Card.exists?(id: card.id)
    end

    test ':api should not delete not hidden user owned pathway' do
      card = create(:card, hidden: true, organization: @user.organization, author: @user)
      pathway = create(:card, card_type: 'pack', card_subtype: 'simple',
        author: @user, organization_id: @user.organization.id, state: 'published'
      )
      CardPackRelation.add(cover_id: pathway.id, add_id:
        card.id, add_type: pathway.class.name
      )
      JourneyPackRelation.add(cover_id: @journey.id, add_id: pathway.id)
      assert_no_difference -> { Card.count } do
        delete :journey_remove, id: @journey.id,
               content_id: pathway.id, delete_dependent: true, format: :json
      end
      assert_response :ok
      assert Card.exists?(id: pathway.id)
      assert Card.exists?(id: card.id)
    end

    test ':api should delete hidden cards in journey if delete_dependent parameter true' do
      assert_difference -> { Card.count }, -7 do
        delete :destroy, id: @journey.id, delete_dependent: 'true', format: :json
      end
      assert_response :ok
      assert Card.where(id: @cards.map(&:id)).empty?
    end

    test ':api should add pathway to multiple journeys' do
      post :add_to_journeys, id: @pathway.id, journey_ids: [@journey.id] , format: :json
      assert_response :ok
      new_journey_pack_relations = [@journey.id].concat(@sections.map(&:id))
      new_journey_pack_relations.push(@pathway.id)
      relations = JourneyPackRelation.journey_relations(cover_id: @journey.id)
        .collect(&:from_id)
      assert_equal relations, new_journey_pack_relations
    end

    test ':api should not add already add pathway to multiple journeys' do
      @journey.add_card_to_journey(@pathway.id)

      post :add_to_journeys, id: @pathway.id, journey_ids: [@journey.id] , format: :json
      assert_response :ok

      new_journey_pack_relations = [@journey.id].concat(@sections.map(&:id))
      new_journey_pack_relations.push(@pathway.id)
      relations = JourneyPackRelation.journey_relations(cover_id: @journey.id)
        .collect(&:from_id)
      assert_equal relations, new_journey_pack_relations
    end

    test 'should return error message if missing required params' do
      post :add_to_journeys, id: @pathway.id, journey_ids: [nil, ''] , format: :json
      assert_response :unprocessable_entity
      resp = get_json_response(response)
      assert_equal 'Missing required params', resp['message']
    end

    test 'should add someone pathway to journeys' do
      user = create(:user, organization: @org)
      pathway = create(:card, card_type: 'pack', card_subtype: 'simple',
                       author: user, organization_id: @org.id)

      post :add_to_journeys, id: pathway.id, journey_ids: [@journey.id] , format: :json
      assert_response :ok

      new_journey_pack_relations = [@journey.id].concat(@sections.map(&:id))
      new_journey_pack_relations.push(pathway.id)
      relations = JourneyPackRelation.journey_relations(cover_id: @journey.id)
        .collect(&:from_id)
      assert_equal relations, new_journey_pack_relations
    end
  end

  class AddableJourneyTest < ActionController::TestCase
    setup do
      org = create(:organization)
      @user = create(:user, organization: org)
      @user2 = create(:user, organization: org)
      @journey = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: @user)
      @section1, @section2, @section3 = create_list(:card, 3, card_type: 'pack', card_subtype: 'simple', author: @user)
      @sections = @section1, @section2, @section3
      @journey_pack = @journey, @section1, @section2, @section3
      @sections.each do |cover|
        CardPackRelation.add(cover_id: cover.id, add_id: create(:card).id, add_type: cover.class.name)
      end
      @sections.each do |s|
        JourneyPackRelation.add(cover_id: @journey.id, add_id: s.id)
      end

      api_v2_sign_in @user
    end

    test ':api should return list of journeys and sections where card can be added' do
      card = create(:card, author: @user)
      CardPackRelation.add(cover_id: @section1.id, add_id: card.id, add_type: card.class.name)
      get :addable_journeys, id: card.id, format: :json
      assert_response :ok
      res = get_json_response(response)
      sections = res['cards'][0]['journey_sections']
      assert_equal 2, res['cards'][0]['journey_sections'].count
      assert_same_ids [@section2.id, @section3.id], [sections[0]['card_id'], sections[1]['card_id']]
      assert_equal 1, res['total']
    end

    test ':api should return empty array if nor acceptable entries' do
      card = create(:card, author: @user)
      api_v2_sign_in @user2
      get :addable_journeys, id: card.id, format: :json
      assert_response :ok
      res = get_json_response(response)
      assert_empty res['cards']
    end

    test ':api should return list of journeys and sections where pathway can be added' do
      card = create(:card, author: @user)
      pack = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user)
      CardPackRelation.add(cover_id: pack.id, add_id: card.id, add_type: card.class.name)
      get :addable_journeys, id: pack.id, type: 'pack', format: :json
      assert_response :ok
      res = get_json_response(response)
      sections = res['cards'][0]['journey_sections']
      assert_equal 1, res['cards'].count
      assert_same_ids res['cards'].collect{|card| card['id']}, [@journey.id]
    end

    test ':api should return empty array if not acceptable entries for pathway' do
      api_v2_sign_in @user2
      get :addable_journeys, id: @section2.id, type: 'pack', format: :json
      assert_response :ok
      res = get_json_response(response)
      assert_empty res['cards']
    end

    test ':api should return 422 if trying to add journey card to journey' do
      api_v2_sign_in @user2
      get :addable_journeys, id: @journey.id, format: :json
      assert_response :unprocessable_entity
    end

    test ':api should return list of journeys and sections where card can be added and handle if any of the card is deleted from the journey' do
      @section3.destroy
      card = create(:card, author: @user)
      get :addable_journeys, id: card.id, format: :json
      assert_response :ok
      res = get_json_response(response)
      sections = res['cards'][0]['journey_sections']
      assert_equal 2, res['cards'][0]['journey_sections'].count
      assert_same_ids [@section2.id, @section3.id], [sections[0]['card_id'], sections[1]['card_id']]
      assert_equal 1, res['total']
    end
  end

  class CustomReorderTests < ActionController::TestCase

    setup do
      org = create(:organization)
      @user = create(:user, organization: org)
      setup_weekly_journey_card(@user)
      setup_journey_card(@user)

      Card.any_instance.stubs(:remove_card_from_queue)

      @cover = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user)
      @card1, @card2, @card3 = create_list(:card, 3, author: @user)
      @cards = @cover, @card1, @card2, @card3

      [@card1, @card2, @card3].each do |card|
        CardPackRelation.add(cover_id: @cover.id, add_id: card.id, add_type: card.class.name)
      end

      api_v2_sign_in @user

      Card.any_instance.stubs(:remove_from_queue_and_reenqueue)
    end

    test ':api should reorder journey sections' do
      reordered = [@section2.id, @section1.id, @section3.id]
      post :custom_order, id: @journey.id, content_ids: reordered, format: :json
      assert_response :ok
      jpr_card_ids = JourneyPackRelation.journey_relations(cover_id: @journey.id).map(&:from_id)
      assert_equal reordered.unshift(@journey.id), jpr_card_ids
    end

    test ':api should reorder cards in pathway' do
      ordered = [@card2.id, @card1.id, @card3.id]
      post :custom_order, id: @cover.id, content_ids: ordered, format: :json
      assert_response :ok
      cpr_card_ids = CardPackRelation.pack_relations(cover_id: @cover.id).map(&:from_id)
      assert_equal ordered.unshift(@cover.id), cpr_card_ids
    end

    test ':api should reorder cards after deleting subcard in pathway' do
      card = create(:card, author: @user)
      CardPackRelation.add(cover_id: @cover.id, add_id: card.id, add_type: card.class.name)
      CardPackRelation.find_by_from_id(card.id).update(deleted: true)
      ordered = [@card2.id, @card1.id, @card3.id]
      post :custom_order, id: @cover.id, content_ids: ordered, format: :json

      cpr_card_ids = CardPackRelation.pack_relations(cover_id: @cover.id).reject(&:deleted).map(&:from_id)
      assert_equal ordered.unshift(@cover.id), cpr_card_ids
    end

    test ':api should reorder sections and set proper date for weekly journey' do
      valid_start_dates = JourneyPackRelation.journey_relations(cover_id: @journey_weekly.id).map(&:start_date)
      reordered = [@section2w.id, @section1w.id, @section3w.id]
      post :custom_order, id: @journey_weekly.id, content_ids: reordered, format: :json
      assert_response :ok
      jpr_card_ids = JourneyPackRelation.journey_relations(cover_id: @journey_weekly.id).map(&:from_id)
      jpr_start_dates = JourneyPackRelation.journey_relations(cover_id: @journey_weekly.id).map(&:start_date)
      assert_equal reordered.unshift(@journey_weekly.id), jpr_card_ids
      assert_equal valid_start_dates, jpr_start_dates
    end
  end

  class CurationRequiredForCardTests < ActionController::TestCase
    setup do
      @org = create(:organization)
      @channel = create(:channel, organization: @org, curate_only: true, curate_ugc: true, only_authors_can_post: false)
      @user = create(:user, organization: @org)
      @user_with_permission = create(:user, organization: @org)

      Role.create(
        name: 'custom',
        organization_id: @org.id,
        master_role: false
      )
      create(
        :role_permission, role: Role.find_by(name: 'custom'),
        name: Permissions::BYPASS_CURATION
      )
      @user_with_permission.add_role('custom')
    end

    test 'card should not be stuck in curate queue when posted by channels collaborator with permission for enabled config' do
      channel = create(:channel, organization: @org, curate_only: true)
      create(:channels_user, channel_id: @channel.id, user_id: @user_with_permission.id)
      card = create(:card, author: @user)

      api_v2_sign_in @user_with_permission
      put :update, id: card.id, card: {channel_ids: [@channel.id] }, format: :json
      resp = get_json_response(response)
      assert_equal resp['id'].to_i, ChannelsCard.last.card_id
      assert_equal @channel.id, ChannelsCard.last.channel_id
      assert_equal 'curated', ChannelsCard.last.state
    end

    test 'card should not be stuck in curate queue when posted by channels collaborator' do
      user = create(:user, organization: @org)
      create(:channels_user, channel_id: @channel.id, user_id: user.id)
      card = create(:card, author: @user)

      api_v2_sign_in user
      put :update, id: card.id, card: {channel_ids: [@channel.id] }, format: :json
      resp = get_json_response(response)
      assert_equal resp['id'].to_i, ChannelsCard.last.card_id
      assert_equal @channel.id, ChannelsCard.last.channel_id
      assert_equal 'curated', ChannelsCard.last.state
    end

    test 'card should not be stuck in curate queue when posted by channels curator' do
      user = create(:user, organization: @org)
      create(:channels_curator, channel_id: @channel.id, user_id: user.id)
      card = create(:card, author: @user)

      api_v2_sign_in user
      put :update, id: card.id, card: {channel_ids: [@channel.id] }, format: :json
      resp = get_json_response(response)
      assert_equal resp['id'].to_i, ChannelsCard.last.card_id
      assert_equal @channel.id, ChannelsCard.last.channel_id
      assert_equal 'curated', ChannelsCard.last.state
    end

    test 'card should stuck in curate queue when posted by channels curator' do
      user = create(:user, organization: @org)
      create(:channels_curator, channel_id: @channel.id, user_id: user.id)
      card = create(:card, author: @user)

      api_v2_sign_in user
      put :update, id: card.id, card: {channel_ids: [@channel.id] }, format: :json
      resp = get_json_response(response)
      assert_equal resp['id'].to_i, ChannelsCard.last.card_id
      assert_equal @channel.id, ChannelsCard.last.channel_id
      assert_equal 'curated', ChannelsCard.last.state
    end

    test 'card should be stuck in curate queue when posted by channels followers' do
      user = create(:user, organization: @org)
      card = create(:card, author: @user)
      create(:follow, user_id: user.id, followable: @channel)


      api_v2_sign_in user
      put :update, id: card.id, card: {channel_ids: [@channel.id] }, format: :json
      resp = get_json_response(response)
      assert_equal resp['id'].to_i, ChannelsCard.last.card_id
      assert_equal @channel.id, ChannelsCard.last.channel_id
      assert_equal 'new', ChannelsCard.last.state
    end
  end

  class CourseStatusTests < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
    end

    test "should return 200 if card found to update subscription" do
      resource = create(:resource, url: 'https://www.medium.com/', user_id: @user.id)
      card = create(:card, author_id: @user.id, card_type: 'course', card_subtype: 'link', resource_id: resource.id)
      request.headers['X-API-KEY'] = "test-sample-key"
      request.headers['X-EMAIL'] = @user.email

      @controller.stubs(:get_source_organization).returns(@org)
      LrsAssignmentService.any_instance.stubs(:run)

      put :course_status, { url: "https://www.medium.com/",status: "started" }

      assert_equal 200, response.status
    end

    test "should return 200 if card found to update subscription if status is start" do
      resource = create(:resource, url: 'https://www.medium.com/', user_id: @user.id)
      card = create(:card, author_id: @user.id, card_type: 'course', card_subtype: 'link', resource_id: resource.id)
      request.headers['X-API-KEY'] = "test-sample-key"
      request.headers['X-EMAIL'] = @user.email

      @controller.stubs(:get_source_organization).returns(@org)
      LrsAssignmentService.any_instance.stubs(:run)

      put :course_status, { url: "https://www.medium.com/",status: "start" }

      assert_equal 200, response.status
    end

    test "should complete card based on card name and source id" do
      resource = create(:resource, url: 'https://www.medium.com/', user_id: @user.id)
      card = create(:card, author_id: @user.id, card_type: 'course', card_subtype: 'link', resource_id: resource.id)
      request.headers['X-API-KEY'] = "test-sample-key"
      request.headers['X-EMAIL'] = @user.email

      Card.stubs(:find_in_source_with_message).returns(card)

      @controller.stubs(:get_source_organization).returns(@org)
      LrsAssignmentService.any_instance.stubs(:run)

      put :course_status, { name: "something",status: "started" }

      assert_equal 200, response.status
    end

    test "should return 404 if no card found to update subscription" do
      request.headers['X-API-KEY'] = "test-sample-key"
      request.headers['X-EMAIL'] = @user.email
      @controller.stubs(:get_source_organization).returns(@org)

      put :course_status, {url: "https://www.example.com/", status: "start"}
      resp = get_json_response(response)

      assert_equal 404, response.status
      assert_equal resp["message"], "card not found with given url"
    end

    test 'should update end date of card_subscription if status is completed or expired' do
      resource = create(:resource, url: 'https://www.medium.com/', user_id: @user.id)
      card = create(:card, author_id: @user.id, card_type: 'course', card_subtype: 'link', resource_id: resource.id)
      request.headers['X-API-KEY'] = "test-sample-key"
      request.headers['X-EMAIL'] = @user.email

      card_subscription = create(:card_subscription, card: card, user: @user, start_date: Time.now-2.days)

      @controller.stubs(:get_source_organization).returns(@org)
      Card.stubs(:find_in_source_with_message).returns(card)
      LrsAssignmentService.any_instance.stubs(:run)

      put :course_status, { url: "https://www.medium.com/", status: "completed"}
      resp = get_json_response(response)

      assert_equal Date.today, card_subscription.reload.end_date.to_date
    end

    test 'should update end date of card_subscription if status is complete' do
      resource = create(:resource, url: 'https://www.medium.com/', user_id: @user.id)
      card = create(:card, author_id: @user.id, card_type: 'course', card_subtype: 'link', resource_id: resource.id)
      request.headers['X-API-KEY'] = "test-sample-key"
      request.headers['X-EMAIL'] = @user.email

      card_subscription = create(:card_subscription, card: card, user: @user, start_date: Time.now-2.days)

      @controller.stubs(:get_source_organization).returns(@org)
      Card.stubs(:find_in_source_with_message).returns(card)
      LrsAssignmentService.any_instance.stubs(:run)

      put :course_status, { url: "https://www.medium.com/", status: "complete"}
      resp = get_json_response(response)

      assert_equal Date.today, card_subscription.reload.end_date.to_date
    end

    test 'should return unprocessable_entity if status is not like assigned, started, completed, expired' do
      resource = create(:resource, url: 'https://www.medium.com/', user_id: @user.id)
      card = create(:card, author_id: @user.id, card_type: 'course', card_subtype: 'link', resource_id: resource.id)
      request.headers['X-API-KEY'] = "test-sample-key"
      request.headers['X-EMAIL'] = @user.email
      card_subscription = create(:card_subscription, card: card, user: @user, start_date: Time.now-2.days)
      @controller.stubs(:get_source_organization).returns(@org)
      Card.stubs(:find_in_source_with_message).returns(card)
      LrsAssignmentService.any_instance.stubs(:run)
      put :course_status, { url: "https://www.medium.com/", status: "startedasdasdas"}
      assert_response :unprocessable_entity
    end

    test 'should return unprocessable_entity if status is blank' do
      resource = create(:resource, url: 'https://www.medium.com/', user_id: @user.id)
      card = create(:card, author_id: @user.id, card_type: 'course', card_subtype: 'link', resource_id: resource.id)
      request.headers['X-API-KEY'] = "test-sample-key"
      request.headers['X-EMAIL'] = @user.email
      card_subscription = create(:card_subscription, card: card, user: @user, start_date: Time.now-2.days)
      @controller.stubs(:get_source_organization).returns(@org)
      Card.stubs(:find_in_source_with_message).returns(card)
      LrsAssignmentService.any_instance.stubs(:run)
      put :course_status, { url: "https://www.medium.com/", status: ""}
      assert_response :unprocessable_entity
    end
  end

    class AssignableCardTest < ActionController::TestCase
    setup do
      org = create(:organization)
      @user = create(:user, organization: org, organization_role: 'admin')
      @author = create(:user, organization: org)
      @team = create(:team, organization: org)
      @card = create(:card, author: @author, organization: org)

      @other_user1 = create(:user, first_name: 'gigi nana', organization_id: org.id)
      @other_user2 = create(:user, first_name: 'rifi hadid', organization_id: org.id)

      @team2 = create(:team, name: 'sales and all', organization_id: org.id)
      create :teams_user, team_id: @team.id, user_id: @other_user1.id, as_type: 'admin'
    end

    test ':api should render unprocessable entity if type parameter is missing' do
      assignment = create(:assignment, assignable: @card, assignee: @user, state: "assigned")
      team_assignment = create(:team_assignment, assignment: assignment, team: @team, assignor: @author)

      api_v2_sign_in @user
      get :assignable, id: @card.id
      resp = get_json_response(response)
      assert_equal 'Missing required params', resp['message']
      assert_response :unprocessable_entity
    end

    test ':api should render unprocessable entity if we provide invalid type' do
      assignment = create(:assignment, assignable: @card, assignee: @user, state: "assigned")
      team_assignment = create(:team_assignment, assignment: assignment, team: @team, assignor: @author)

      api_v2_sign_in @user
      get :assignable, id: @card.id, type: 'in', format: :json
      resp = get_json_response(response)
      assert_equal 'Invalid type', resp['message']
      assert_response :unprocessable_entity
    end

    test ':api should list of teams if type is `teams`' do
      Search::TeamSearch.any_instance.stubs(:search).
        returns(Search::TeamSearchResponse.new(results: [@team, @team2], total: 2))

      assignment = create(:assignment, assignable: @card, assignee: @user, state: "assigned")
      team_assignment = create(:team_assignment, assignment: assignment, team: @team, assignor: @author)

      api_v2_sign_in @user
      get :assignable, id: @card.id, type: 'teams', format: :json

      resp = get_json_response(response)

      # when card is assigned to group
      assert_equal @team.id, resp['assignable']['teams'][0]['id']
      assert_equal @team.name, resp['assignable']['teams'][0]['name']
      assert_equal true, resp['assignable']['teams'][0]['assigned']

      # when card is not assigned to group
      assert_equal @team2.id, resp['assignable']['teams'][1]['id']
      assert_equal @team2.name, resp['assignable']['teams'][1]['name']
      refute resp['assignable']['teams'][1]['assigned']
    end

    test ':api should list teams where current user is leader of any team' do
      Search::TeamSearch.any_instance.stubs(:search).
        returns(Search::TeamSearchResponse.new(results: [@team], total: 2))

      assignment = create(:assignment, assignable: @card, assignee: @user, state: "assigned")
      team_assignment = create(:team_assignment, assignment: assignment, team: @team, assignor: @author)

      api_v2_sign_in @other_user1
      get :assignable, id: @card.id, type: 'teams', format: :json

      resp = get_json_response(response)

      # when card is assigned to group
      assert_equal @team.id, resp['assignable']['teams'][0]['id']
      assert_equal @team.name, resp['assignable']['teams'][0]['name']
      assert resp['assignable']['teams'][0]['assigned']
    end

    test ':api should list of users if type is individuals' do
      Search::UserSearch.any_instance.stubs(:search).
        returns(Search::UserSearchResponse.new(results: [@other_user1, @other_user2], total: 2))

      assignment = create(:assignment, assignable: @card, assignee: @other_user1, state: "assigned")
      team_assignment = create(:team_assignment, assignment: assignment, team_id: nil, assignor: @author)

      api_v2_sign_in @user
      get :assignable, id: @card.id, type: 'individuals', format: :json

      resp = get_json_response(response)

      # when card is assigned to individuals
      assert_equal @other_user1.id, resp['assignable']['users'][0]['id']
      assert_equal @other_user1.name, resp['assignable']['users'][0]['name']
      assert resp['assignable']['users'][0]['assigned']

      # when card is not assigned to individuals
      assert_equal @other_user2.id, resp['assignable']['users'][1]['id']
      assert_equal @other_user2.name, resp['assignable']['users'][1]['name']
      refute resp['assignable']['users'][1]['assigned']
    end

    test ':api should list of users when `q` is present' do
      Search::UserSearch.any_instance.stubs(:search).
        returns(Search::UserSearchResponse.new(results: [@other_user1], total: 1))

      assignment = create(:assignment, assignable: @card, assignee: @other_user1, state: "assigned")
      team_assignment = create(:team_assignment, assignment: assignment, team_id: nil, assignor: @author)

      api_v2_sign_in @user
      get :assignable, id: @card.id, type: 'individuals', q: 'gi', format: :json

      resp = get_json_response(response)

      # when card is assigned to individuals
      assert_equal @other_user1.id, resp['assignable']['users'][0]['id']
      assert_equal @other_user1.name, resp['assignable']['users'][0]['name']
      assert resp['assignable']['users'][0]['assigned']
    end
  end

  class CardDeleteForCardReportingIndex < ActionController::TestCase
    setup do
      skip #NO NETWORK CALLS
      WebMock.disable!
      EdcastActiveJob.unstub :perform_later
      CardReportingIndexingJob.unstub :perform_later
      CardReportingTrashingJob.unstub :perform_later

      @user = create(:user)
      @org = @user.organization

      @card = create(:card, author_id: @user.id, card_type: 'media')
      @card_reporting = create(:card_reporting, user_id: @user.id, card_id: @card.id, reason: 'Nice')

      api_v2_sign_in @user
    end

    teardown do
      WebMock.enable!
    end

    def setup_for_index(card, card_reporting)
      FlagContent::CardReportingSearchService.create_index! force: true
      FlagContent::CardReportingSearchService.new(
        id: card.id,
        reportings: card_reporting,
        card_message: card.title || card.message,
        card_type: card.card_type,
        card_created_at: card.created_at,
        card_author_id: card.author_id,
        card_organization_id: card.organization_id,
        card_deleted_at: card.deleted_at,
        card_source: card.source_type_name).save
      FlagContent::CardReportingSearchService.gateway.refresh_index!
    end

    test 'should destroy the card when the card gets deleted' do
      skip #what are we testing here?
      setup_for_index(@card, @card_reporting)

      delete :destroy, id: @card.id, format: :json
      FlagContent::CardReportingSearchService.gateway.delete_index!

      assert_response :ok
    end
  end

  test 'should return unprocessable entity if params are missing' do
    org = create(:organization)
    user = create(:user, organization: org, organization_role: 'admin')
    card = create(:card, author_id: user.id)
    api_v2_sign_in(user)

    delete :remove_assignments, id: card.id, format: :json

    assert_response :unprocessable_entity
  end

  test 'should remove team_assignments for card' do
    org = create(:organization)
    admin = create(:user, organization: org, organization_role: 'admin')
    card1, card2 = create_list(:card, 2, author_id: admin.id)
    team1 = create(:team, name: 'team1', organization: org)
    team2 = create(:team, name: 'team2', organization: org)
    assign1 = create(
      :assignment,  assignable: card1, assignee: admin, state: 'assigned'
    )
    assign2 = create(
      :assignment,  assignable: card2, assignee: admin, state: 'assigned'
    )
    create(
      :team_assignment, team_id: team1.id,
      assignment_id: assign1.id, assignor_id: admin.id
    )
    team_assignment1 = create(
      :team_assignment, team_id: team2.id,
      assignment_id: assign1.id, assignor_id: admin.id
    )
    team_assignment2 = create(
      :team_assignment, team_id: team1.id,
      assignment_id: assign2.id, assignor_id: admin.id
    )
    
    
    api_v2_sign_in(admin)
    
    assignor = {}
    assignor[team1.id] = admin.id
    assignor[team2.id] = admin.id
    
    Card.any_instance.expects(:reprocess_team_assignment_metric)
    .with([team1.id], assignor)
    .once

    assert_difference -> {TeamAssignment.count}, -1 do
      delete :remove_assignments, id: card1.id, team_ids: [team1.id], format: :json
    end
    assert_response :ok
    
    assert_equal team_assignment1.id, TeamAssignment.first.id
    assert_equal team_assignment2.id, TeamAssignment.second.id
    assert_equal 2, Assignment.count
  end

  test 'should trigger the team_assign_metric reprocessing for removing user assignments' do
    org = create(:organization)
    admin = create(:user, organization: org, organization_role: 'admin')
    card = create(:card, author_id: admin.id)
    user_list = create_list(:user, 2, organization: org)
    
    team1 = create(:team, name: 'team1', organization: org)
    team2 = create(:team, name: 'team2', organization: org)

    assign1 = create(
      :assignment,  assignable: card, assignee: user_list[0], state: 'assigned'
    )
    assign2 = create(
      :assignment,  assignable: card, assignee: user_list[1], state: 'assigned'
    )

    team_assignment1 = create(
      :team_assignment, team_id: team2.id,
      assignment_id: assign1.id, assignor_id: admin.id
    )
    team_assignment2 = create(
      :team_assignment, team_id: team1.id,
      assignment_id: assign2.id, assignor_id: admin.id
    )

    assignor = {}
    assignor[team2.id] = admin.id

    Card.any_instance.expects(:reprocess_team_assignment_metric)
    .with([team2.id], assignor)
    .once

    api_v2_sign_in(admin)

    assert_difference -> {TeamAssignment.count}, -1 do
      delete :remove_assignments, id: card.id, assignment_ids: [assign1.id], format: :json
    end

    assert_response :ok
    assert_equal team_assignment2.id, TeamAssignment.first.id
    assert_equal 1, Assignment.count
  end

  test 'should return not found if assignments are not present' do
    org = create(:organization)
    user = create(:user, organization: org, organization_role: 'admin')
    card = create(:card, author_id: user.id)
    api_v2_sign_in(user)

    delete :remove_assignments, id: card.id, assignment_ids: [rand(0..100)], format: :json
    assert_response :not_found

    team = create(:team, name: 'team1', organization: org)
    delete :remove_assignments, id: card.id, team_ids: [team.id], format: :json
    assert_response :not_found
  end

  test 'should remove assignments from card' do
    org = create(:organization)
    user = create(:user, organization: org, organization_role: 'admin')
    card = create(:card, author_id: user.id)

    assignment = create(
      :assignment, assignable: card, assignee: user, state: 'assigned'
    )
    api_v2_sign_in(user)

    assert_difference -> {Assignment.count}, -1 do
      delete :remove_assignments, id: card.id, assignment_ids: [assignment.id], format: :json
    end

    assert_response :ok
    assert_empty card.assignments
  end

  test 'should remove team assignments from card' do
    org = create(:organization)
    admin = create(:user, organization: org, organization_role: 'admin')
    author = create(:user, organization: org, organization_role: 'admin')
    card = create(:card, author_id: author.id)
    team = create(:team, name: 'team1', organization: org)

    assign = create(
      :assignment,  assignable: card, assignee: admin, state: 'assigned'
    )
    assign1 = create(
      :assignment,  assignable: card, assignee: author, state: 'assigned'
    )
    create(
      :team_assignment, team_id: team.id,
      assignment_id: assign.id, assignor_id: author.id
    )
    assert_equal 2, card.assignments.count
    api_v2_sign_in(admin)

    assert_difference ['Assignment.count', 'TeamAssignment.count'], -1 do
      delete :remove_assignments, id: card.id, team_ids: [team.id], format: :json
    end

    assert_response :ok
    assert_equal 1, card.assignments.count
  end

  test ':api duplicate_cards should return error invalid data type' do
    get :duplicate_cards, resource_url: [], format: :json

    assert_response :unprocessable_entity
    resp = get_json_response(response)
    assert_equal 'resource_url param has an invalid data type', resp['message']
  end

  test ':api duplicate_cards should return error missing required params' do
    get :duplicate_cards, resource_url: '', format: :json

    assert_response :unprocessable_entity
    resp = get_json_response(response)
    assert_equal 'Invalid resource url', resp['message']
  end

  test 'should return duplicate cards by resource_url' do
    resource = create(:resource, url: @url, user_id: @user.id)
    card = create(:card, author_id: @user.id, card_type: 'course', card_subtype: 'link', resource_id: resource.id)
    card1 = create(:card, author_id: @user.id, card_type: 'media', card_subtype: 'link', resource_id: resource.id)
    card2 = create(:card, author_id: @user.id)
    card_ids = [card.id, card1.id, card2.id]
    api_v2_sign_in @user

    Search::CardSearch.any_instance.expects(:search).with(nil, @org, {
      viewer: @user,
      filter_params: { id: card_ids, link_url: @url },
      offset: 0, limit: 15, load: true
    }
    ).returns(Search::CardSearchResponse.new(results: [card, card1], total: 2))

    get :duplicate_cards, resource_url: @url, format: :json

    assert_response :ok
    resp = get_json_response(response)
    assert_equal 2, resp['total']
    assert_equal 2, resp['cards'].count
    assert_equal card.id, resp['cards'].first['id'].to_i
    assert_equal card1.id, resp['cards'].second['id'].to_i
  end

  test 'should return duplicate cards by resource_url, visible for user' do
    user = create(:user, organization: @user.organization)
    resource = create(:resource, url: @url, user_id: @user.id)
    card = create(
      :card, author_id: @user.id, card_type: 'course',
      card_subtype: 'link', resource_id: resource.id
    )
    card1 = create(
      :card, author_id: @user.id, card_type: 'media',
      card_subtype: 'link', resource_id: resource.id
    )
    card2 = create(
      :card, author_id: user.id, card_type: 'media',
      card_subtype: 'link', resource_id: resource.id, is_public: false
    )

    card_ids = [card.id, card1.id]

    api_v2_sign_in @user

    Search::CardSearch.any_instance.expects(:search).with(nil, @org, {
        viewer: @user,
        filter_params: { id: card_ids, link_url: @url },
        offset: 0, limit: 15, load: true
      }
    ).returns(Search::CardSearchResponse.new(results: [card, card1], total: 2))

    get :duplicate_cards, resource_url: @url, format: :json

    assert_response :ok
    resp = get_json_response(response)
    assert_equal 2, resp['total']
    assert_equal card.id, resp['cards'].first['id'].to_i
    assert_equal card1.id, resp['cards'].second['id'].to_i
  end

  test 'returns secured urls of smartcard resource' do
    resource = create(:resource, image_url: "http://cdn.filestackcontent.com/abcdefgh", user_id: @user.id)
    card = create(
      :card, author_id: @user.id, card_type: 'course',
      card_subtype: 'link', resource_id: resource.id
    )
    api_v2_sign_in @user
    get :secured_urls, id: card.id
    resp = JSON.parse(response.body)

    assert_includes resp["resource"]["imageUrl"], "https://cdn.filestackcontent.com/security=p:"
  end

  test 'returns secured filestack urls of smartcard' do
    card = create(
      :card, author_id: @user.id, card_type: 'media', filestack: [
      { "name" => "Presentation1.pptx", "mimetype" => "application/vnd.openxmlformats-officedocument.presentationml.presentation",
        "size" => 701380, "source" => "local_file_system", "url" => "https://cdn.filestackcontent.com/Ct0DbtZ0TtmzI5AHv8Yh",
        "handle" => "Ct0DbtZ0TtmzI5AHv8Yh", "status" => "Stored"
      }
    ]
    )
    api_v2_sign_in @user
    get :secured_urls, id: card.id
    resp = JSON.parse(response.body)
    assert_includes resp["filestack"][0]["url"], "https://cdn.filestackcontent.com/security=p:"
  end

  test 'returns secured filestack urls of pathway card' do
    card = create(
      :card, author_id: @user.id, card_type: 'pack', filestack: [
      { "name" => "Presentation1.pptx", "mimetype" => "application/vnd.openxmlformats-officedocument.presentationml.presentation",
        "size" => 701380, "source" => "local_file_system", "url" => "https://cdn.filestackcontent.com/Ct0DbtZ0TtmzI5AHv8Yh",
        "handle" => "Ct0DbtZ0TtmzI5AHv8Yh", "status" => "Stored"
      }
    ]
    )
    api_v2_sign_in @user
    get :secured_urls, id: card.id
    resp = JSON.parse(response.body)
    assert_includes resp["filestack"][0]["url"], "https://cdn.filestackcontent.com/security=p:"
  end
end

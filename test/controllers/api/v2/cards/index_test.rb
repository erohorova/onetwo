require 'test_helper'

class Api::V2::CardsControllerTest < ActionController::TestCase
  self.use_transactional_fixtures = true

  setup do
    stub_video_stream_index_calls
    EdcastActiveJob.stubs :perform_later
    IndexJob.stubs :perform_later

    @user = create(:user)
    @org = @user.organization

    api_v2_sign_in @user
    @url = 'http://url.com'
    stub_request(:get, @url).
          to_return(:status => 200, :body => '<html><head></head><body></body></html>', headers: {})
  end
    # Index start
  test ':api should get cards in an org' do
    cards = create_list(:card, 5, author: @user, card_type: 'media')

    get :index, format: :json
    assert_response :success
    assert assigns(:cards)
    assert_equal 5, assigns(:cards).length
    assert_same_elements cards.map(&:id), assigns(:cards).map{|c| c['id']}
  end

  test ':api should get cards in an org filtered by language' do
    @user.create_profile(language: 'ru')
    ru_cards = create_list(:card, 2, author: @user, language: 'ru')
    en_cards = create_list(:card, 2, author: @user, language: 'en')
    cards_without_language = create_list(:card, 2, author: @user)

    get :index, filter_by_language: true, format: :json
    assert_response :success
    resp = get_json_response(response)
    assert_same_elements ru_cards.map(&:id) + cards_without_language.map(&:id), resp['cards'].map{|c| c['id'].to_i}
  end

  test 'get call should return card metadata if present' do
    cards = create_list(:card, 5, author: @user, card_type: 'media', is_paid: false)
    card_metadatum = create(:card_metadatum, card_id: cards[0].id, plan: 'free')
    cards[0].reload
    get :index, format: :json
    assert_response :success
    resp = get_json_response(response)
    assert_equal 'free', resp["cards"].detect{|x| x["id"] == cards[0].id.to_s}["card_metadatum"]["plan"]
    assert_nil resp["cards"].detect{|x| x["id"] == cards[1].id.to_s}["card_metadatum"]
  end


  test ':api should get cards in an org based on readable card type' do
    course_draft_card = create(:card, author: @user, readable_card_type_name: 'course', state: 'draft')
    webinar_published_card = create(:card, author: @user, readable_card_type_name: 'webinar', state: 'published')
    excel_published_card = create(:card, author: @user, readable_card_type_name: 'excel', state: 'published')
    podcast_published_card = create(:card, author: @user, readable_card_type_name: 'podcast', state: 'published')
    quiz_draft_card = create(:card, author: @user, readable_card_type_name: 'quiz', state: 'draft')
    blog_post_card = create(:card, author: @user, readable_card_type_name: 'blog_post', state: 'published')

    get :index, readable_card_type: ['Webinar', 'Quiz', 'Blog Post'], state: ['draft', 'published'], format: :json
    assert_response :success
    assert_equal 2, assigns(:cards).length
    resp = get_json_response(response)
    assert_same_elements [webinar_published_card,blog_post_card].map(&:id), assigns(:cards).map{|c| c['id']}
    assert_same_elements ['Webinar', 'Blog Post'], resp['cards'].map{ |a| a['readable_card_type'] }.uniq
  end

  test ':api should return card posted in private channel if user is author' do
    uncurated_card = create(:card, author: @user)

    channel = create(:channel, is_private: true, curate_only: false,  organization: @org)
    uncurated_card.channels << channel

    author = create(:user, organization: @org)
    card = create(:card, organization: @org, author: author, card_type: 'media')

    get :index, format: :json
    assert_response :success
    assert_equal 2, assigns(:cards).length
    assert_same_elements [uncurated_card.id, card.id], assigns(:cards).map{|c| c['id']}

    public_channel = create(:channel, is_private: false, curate_only: false,  organization: @org)
    new_card = create(:card, organization: @org, author: @user)
    new_card.channels << public_channel

    get :index, format: :json
    assert_response :success
    assert_equal 3, assigns(:cards).length
  end

  #test ':api should not return card posted in private channel if user is not author' do
  #EP-16550 Now public cards are visible to user irrespective of which container they belong to
  #end

  test ':api should be able to do simple search cards' do
    card = create(:card, message: 'something happened', author: @user)
    another_card = create(:card, title: 'something didnt happen', author: @user)
    wont_appear_in_results_card = create(:card, message: 'everthing happened', author: @user)

    get :index, q:'something', format: :json
    assert_response :success
    expected_cards = [card, another_card]
    assert_same_elements expected_cards.map(&:id), assigns(:cards).map{|c| c['id']}
  end

  test 'should get video stream cards with urls in an org' do
    skip #skipping these as we done modification to improve performance and it is present for mobile.
    vs = create(:iris_video_stream, organization: @user.organization, creator_id: @user.id, state: 'published')

    get :index, format: :json
    resp = get_json_response(response)
    assert_not_nil resp["cards"][0]["video_stream"]["embed_playback_url"]
  end

  test 'should return user_profile in response' do
    user = create(:user)
    create_list(:card, 2, author_id: user.id)
    profile = create(:user_profile, user: user, job_title: 'aaa')
    api_v2_sign_in user

    get :index, format: :json

    resp = get_json_response(response)['cards'][0]['author']
    assert_equal profile.job_title, resp['profile']['job_title']
  end

  test ':api should send poll cards' do
    cards = create_list(:card, 2, card_type: 'poll', author: @user)
    first_card = cards.first
    option = create(:quiz_question_option, label: 'a', quiz_question: first_card)
    attempt = create(:quiz_question_attempt, user: @user, quiz_question: first_card, selections: [option.id])
    stats = create(:quiz_question_stats, quiz_question: first_card, attempt_count: 1, options_stats: {option.id.to_s => 1})

    get :index, format: :json

    assert_response :success
    resp = get_json_response(response)
    assert_same_elements cards.map(&:id).map(&:to_s), resp['cards'].map{|c| c['id']}

    first_card_response_object = resp['cards'].select{|c| c['id'] == first_card.id.to_s}.first
    # Test attempt count
    assert_equal 1, first_card_response_object['attempt_count']

    # Test has attempted
    assert first_card_response_object['has_attempted']

    # Test individual option stats
    assert_equal 1, first_card_response_object['quiz_question_options'].select{|opt| opt['id'] == option.id}.first['count']
  end

  test ':api should support card type pack filter' do
    org_cards = create_list(:card, 2, card_type: 'pack', card_subtype: 'simple', author_id: @user.id, message: 'Pathway card')
    org_cards.each do |pathway|
      add_cards_to_cover(pathway)
      pathway.publish!
    end

    get :index, card_type: ['pack'], format: :json
    assert_response :ok
    resp = get_json_response response

    assert_equal 2, resp['cards'].count
    assert_same_elements org_cards.map(&:id).map(&:to_s), resp['cards'].map{|c| c['id']}
  end
  
  test ':api should return hidden value in response' do
    org = create(:organization)
    users = create_list(:user, 2, organization: org)
    admin_user = create(:user, organization: org, organization_role: 'admin')

    api_v2_sign_in(admin_user)

    media_card = create(:card, organization: org, author_id: users[0].id, card_type: 'media',  hidden: true)
    course_card = create(:card, organization: org, author_id: users[0].id, card_type: 'course', hidden: true)
    poll_card = create(:card, organization: org, author_id: users[0].id, card_type: 'poll', hidden: true)

    get :index, card_type: ['media', 'course', 'poll'], state: ['draft', 'published'], is_cms:'true', format: :json

    assert_response :success
    resp = get_json_response(response)
    assert_equal 3, resp['total']
    assert_equal media_card.hidden, resp['cards'][0]['hidden']
  end

  test 'api should support sort by created at' do
    cards = (0..3).to_a.reverse.map do |i|
      create(:card, author_id: nil, organization_id: @org.id, card_type: 'video_stream', card_subtype: 'simple', message: 'messsage', title: 'title', created_at: i.hours.ago)
    end

    # video streams
    cards.each do |card|
      create(:iris_video_stream, card: card)
    end

    get :index, sort: "created", card_type: ['video_stream'], limit: 2, offset: 0, format: :json
    assert_response :ok
    resp = get_json_response response
    assert_equal cards.reverse.first(2).map(&:id), resp['cards'].map{|c| c['id'].to_i}
  end

  test 'api should return cards sort by views_count in desc order' do
    cards = create_list(:card, 4, author_id: nil, organization_id: @org.id, card_type: 'media', message: 'messsage', title: 'title')
    
    create(:card_metrics_aggregation, card_id: cards[0].id, num_views: 8 , organization_id:@org.id )
    create(:card_metrics_aggregation, card_id: cards[1].id, num_views: 10,organization_id:@org.id )
    create(:card_metrics_aggregation, card_id: cards[2].id, num_views: 5, organization_id:@org.id )

    get :index, sort: "views_count",order: "desc", card_type: ['media'], limit: 3, offset: 0, format: :json
    assert_response :ok
    resp = get_json_response response
    assert_equal [cards[1].id,cards[0].id,cards[2].id ], resp['cards'].map{|c| c['id'].to_i}
  end

  test 'api should support sort by views_count in asc order' do
    cards = create_list(:card, 4, author_id: nil, organization_id: @org.id, card_type: 'media', message: 'messsage', title: 'title')
    
    create(:card_metrics_aggregation, card_id: cards[0].id, num_views: 8, organization_id:@org.id )
    create(:card_metrics_aggregation, card_id: cards[1].id, num_views: 10, organization_id:@org.id )
    create(:card_metrics_aggregation, card_id: cards[2].id, num_views: 5, organization_id:@org.id )

    get :index, sort: "views_count", order: "asc", card_type: ['media'], limit: 3, offset: 0, format: :json
    assert_response :ok
    resp = get_json_response response
    assert_equal [cards[3].id,cards[2].id,cards[0].id ], resp['cards'].map{|c| c['id'].to_i}
  end

  test 'api should trigger the assessment report generation job exactly once for each time it is triggered' do
    @card = create(:card, card_type: 'pack', author_id: @user.id)
    AssessmentReportGenerationJob.expects(:perform_later).with({card_id: @card.id.to_s, user_id: @user.id}).once

    get :report, id: @card.id
  end

  test 'api should support sort by updated at' do
    cards = (0..3).to_a.reverse.map do |i|
      create(:card, author_id: nil, organization_id: @org.id, card_type: 'video_stream',
        card_subtype: 'simple', message: 'messsage', title: 'title', updated_at: i.hours.ago)
    end

    # video streams
    cards.each do |card|
      create(:iris_video_stream, card: card)
    end

    get :index, sort: "updated", card_type: ['video_stream'], limit: 2, offset: 0, format: :json
    assert_response :ok
    resp = get_json_response response
    assert_equal cards.reverse.first(2).map(&:id), resp['cards'].map{|c| c['id'].to_i}
  end

  test 'api should support sort by promoted' do
    srand(0)
    cards = create_list(:card, 3, author_id: nil, organization_id: @org.id, card_type: 'video_stream', card_subtype: 'simple')

    # video streams
    cards.each do |card|
      create(:iris_video_stream, card: card)
    end

    # Last card is promoted, with promoted_at 3 hours ago
    cards.last.update_attributes(is_official: true, promoted_at: 3.hours.ago)

    # second card is promoted, with promoted at 1 hours ago
    cards[1].update_attributes(is_official: true, promoted_at: 1.hour.ago)

    # first card is not promoted
    cards.first.update_attributes(is_official: false)

    get :index, sort: "promoted_first", card_type: ['video_stream'], limit: 3, offset: 0, format: :json
    assert_response :ok
    resp = get_json_response response
    assert_equal [cards[1], cards.last, cards.first].map(&:id), resp['cards'].map{|c| c['id'].to_i}
    srand()
  end

  test 'api should support sort by title' do
    cards = create_list(:card, 3, author_id: nil, organization_id: @org.id)
    cards.first.update_attributes(title: 'B')
    cards.second.update_attributes(title: 'C')
    cards.last.update_attributes(title: 'A')

    get :index, sort: "title", limit: 3, offset: 0, format: :json
    assert_response :ok
    resp = get_json_response response
    assert_equal ["C", "B", "A"], resp['cards'].map{|c| c['title']}
  end


  test 'api should support sort by message' do
    cards = create_list(:card, 3, author_id: nil, organization_id: @org.id)
    cards.first.update_attributes(message: 'Message 3')
    cards.second.update_attributes(message: 'Message 1')
    cards.last.update_attributes(message: 'Message 2')

    get :index, sort: "message", limit: 3, offset: 0, format: :json
    assert_response :ok
    resp = get_json_response response
    assert_equal ["Message 3", "Message 2", "Message 1"], resp['cards'].map{|c| c['message']}
  end

  test 'api should support sort by author_name from admin panel' do
    users = create_list(:user, 3, organization: @org)
    users.each do |user|
      create(:card, author_id: user.id, organization_id: @org.id)
    end

    admin_user = create(:user, organization: @org, organization_role: 'admin')

    api_v2_sign_in(admin_user)
    get :index, sort: 'author_name', is_cms: true, format: :json
    assert_response :ok
    resp = get_json_response response
    expected_order = users.map(&:full_name).reverse
    actual_order = resp['cards'].map { |c| c['author']['full_name'] }

    assert_equal expected_order, actual_order
  end

  test 'api should support sort by likes count' do
    voters = create_list(:user, 3, organization: @org)

    cards = create_list(:card, 3, author_id: nil, organization_id: @org.id, card_type: 'video_stream', card_subtype: 'simple')

    # video streams
    cards.each do |card|
      create(:iris_video_stream, card: card)
    end

    # First card has 3 votes
    voters.each do |voter|
      create(:vote, votable: cards.first, voter: voter)
    end

    # second card has 1 vote
    create(:vote, votable: cards[1], voter: voters.first)
    # third card has none

    get :index, sort: "like_count", card_type: ['video_stream'], limit: 3, offset: 0, format: :json
    assert_response :ok
    resp = get_json_response response
    assert_equal cards.map(&:id), resp['cards'].map{|c| c['id'].to_i}
  end

  test ':api share card with individual users' do
    card = create(:card, author: @user)
    users = create_list(:user, 2, organization: @user.organization)

    post :share, id: card.id, user_ids: users.map(&:id), format: :json

    assert_response :ok
    assert_not_empty card.users_with_access.pluck(:user_id) & users.map(&:id)

  end

  test ':api unshare card with individual users' do
    card = create(:card, author: @user)
    users = create_list(:user, 3, organization: @user.organization)
    users.each do |u|
      create(:card_user_share, card_id: card.id, user_id: u.id)
    end

    post :share, id: card.id, remove_user_ids: [users[0].id,users[2].id], format: :json

    assert_response :ok
    assert_empty card.users_with_access.pluck(:user_id) & [users[0].id,users[2].id]

  end

  test ':api unshare card with teams users' do
    team_list = create_list(:team, 2, organization: @user.organization).each{|team| team.add_member @user}
    card = create(:card, author: @user)
    team_list.each do |team|
      SharedCard.create(team_id: team.id, card_id: card.id, user_id: @user.id)
    end

    post :share, id: card.id, remove_team_ids: [team_list[0].id], format: :json

    assert_response :ok
    assert_empty card.shared_with_teams.pluck(:team_id) & [team_list[0].id]

  end

  test ':api #index should support only_deleted option to show deleted cards' do
    card1, *cards = create_list(:card, 5, author: @user)
    cards.each do |card|
      card.destroy
      card.reload
    end

    parameters = {
      author_id: [@user.id],
      only_deleted: true,
      format: :json
    }

    get :index, parameters
    assert_response :ok
    resp = get_json_response(response).with_indifferent_access
    response_card_ids = resp['cards'].map{ |card| card['id'].to_i }
    is_all_cards_destroyed = cards.map{|card| !!card.deleted_at }.all?
    assert_equal 4, resp['cards'].count
    assert_equal cards.map(&:id), response_card_ids
    assert is_all_cards_destroyed, 'Cards should be destroyed'
    refute_includes [card1.id], response_card_ids
  end


  class CardSharePermissionTest < ActionController::TestCase
    setup do
      Organization.any_instance.unstub(:create_default_roles)
      org = create(:organization, enable_role_based_authorization: true)
      @member_role = org.roles.where(name: Role::TYPE_MEMBER)

      @user = create(:user, organization: org)
      @user.roles << @member_role

      @card_to_share = create(:card, author_id: @user.id, card_type: 'media', card_subtype: 'link')
      @team1 = create(:team, organization: org)

      @share_role_permission = @member_role.first.role_permissions.find_or_create_by(name: 'SHARE')
      api_v2_sign_in(@user)
    end

    test 'user should not be able to share if he does not have the permission to' do
      @share_role_permission.disable!
      post :share, id: @card_to_share.id, team_ids: [@team1.id], format: :json
      assert_response :unauthorized
    end

    test 'user should be able to share if he does has the permission to' do
      Card.any_instance.expects(:shareable?).with(@user).returns(true)
      post :share, id: @card_to_share.id, team_ids: [@team1.id], format: :json
      assert_response :ok
    end
  end

  test 'api should support sorting by average rating' do
    org = create(:organization)
    users = create_list(:user, 3, organization: org)
    card1, card2, card3 = create_list(:card, 3, author: users.last)
    users.each_with_index do |user, index|
      create(:cards_rating, card: card1, user: user, rating: index+1)
      create(:cards_rating, card: card2, user: user, rating: index+2)
    end
    api_v2_sign_in users.first
    get :index, sort: 'rating', format: :json

    resp = get_json_response(response)
    assert_equal [card2, card1, card3].map(&:id).map(&:to_s), resp['cards'].map {|c| c['id']}
  end

  test 'api should support sort by added_to_channel for course card in channel' do
    org = @user.organization
    cards = create_list(:card, 3, card_type: 'course', author: @user)
    channel = create(:channel, organization: @org)

    cards.each_with_index do |card, index|
      Timecop.freeze(Date.today + index) do
        card.channels << channel
      end
    end

    get :index, sort: 'added_to_channel', card_type: ["course"], channel_id: channel[:id], format: :json
    assert_response :ok
    resp = get_json_response response
    assert_equal cards.reverse.map(&:id), resp['cards'].map{|c| c['id'].to_i}
  end

  test 'api should support sort by ecl_source_name for course card' do
    org = create(:organization)
    user = create(:user, organization_role: 'member', organization: org)

    cards = []
    ['source name 2', 'source name 1', 'source name 3'].each do |source_name|
      cards << create(:card,  organization: org, author_id: nil, card_type: 'course',
        ecl_metadata: {source_display_name: source_name}, ecl_source_name: source_name)
    end

    api_v2_sign_in user

    get :index, sort: 'ecl_source_name', card_type: ["course"], format: :json
    assert_response :ok
    resp = get_json_response response
    assert_equal ['source name 3', 'source name 2', 'source name 1'], resp['cards'].map {|c| c['ecl_source_display_name']}
  end

  test ':api should return only official cards, exclude dismissed' do
    official_cards = create_list(:card, 5, is_official: true, card_type: 'media', author: @user)
    regular_cards = create_list(:card, 5, is_official: false, card_type: 'media', author: @user)
    dismiss_official_cards = create_list(:card, 5, is_official: true, card_type: 'media', author: @user)

    get :index, is_official: 'true', card_type: ['media'], format: :json
    assert_same_elements assigns(:cards).collect(&:id), (official_cards+dismiss_official_cards).map(&:id)

    dismiss_official_cards.each do |card|
      DismissedContent.create(content: card, user_id: @user.id)
    end
    get :index, is_official: 'true', card_type: ['media'], format: :json
    assert_response :success
    assert assigns(:cards)
    assert_same_elements assigns(:cards).collect(&:id), official_cards.collect(&:id)
    assert_response :ok
  end

  test 'api should  return completed cards in featured tab' do
    author = create(:user, organization: @org)
    cards = create_list(:card, 10, organization: @org, is_official: true, author: author, card_type: 'media')

    cards_to_complete = cards.first(3)
    cards_to_complete.each do |card|
      create(:user_content_completion, completable: card, user: @user, state: 'completed')
      card.reload
    end

    bookmarked_cards = cards.last(2)
    bookmarked_cards.each do |card|
      create(:bookmark, bookmarkable: card, user: @user)
      card.reload
    end

    get :index, is_official: 'true', format: :json
    assert_response :success
    assert assigns(:cards)
    assert_equal 10, assigns(:cards).length
    assert_same_elements cards.map(&:id), (assigns(:cards).map { |c| c['id'] })
  end

  test 'api should return mark_feature_disabled flag properly' do
    org = create(:organization)
    user = create(:user, organization_role: 'member', organization: org)

    first_card = create(:card,  organization: org, author_id: nil, card_type: 'course', ecl_metadata: {mark_feature_disabled: true})
    second_card = create(:card,  organization: org, author_id: nil, card_type: 'course', ecl_metadata: {mark_feature_disabled: false})

    api_v2_sign_in user

    get :index, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_same_elements [true, false], resp['cards'].map {|c| c['mark_feature_disabled_for_source']}
  end

  test 'api should skip content bookmarked' do
    author = create(:user, organization: @org)
    cards = create_list(:card, 10, organization: @org, is_official: true, author: author, card_type: 'media')

    bookmarked_cards = cards.last(2)
    bookmarked_cards.each do |card|
      create(:bookmark, bookmarkable: card, user: @user)
      card.reload
    end
    bookmarked_ids = bookmarked_cards.map(&:id)

    get :index, is_official: 'true', skip_content: {bookmarked: "true"}, format: :json
    assert_response :success
    assert assigns(:cards)
    assert_equal 8, assigns(:cards).length
    assert_same_elements cards.map(&:id).reject { |id| bookmarked_ids.include? id }, assigns(:cards).map{|c| c['id']}
  end

  test 'api should skip content completed' do
    author = create(:user, organization: @org)
    cards = create_list(:card, 10, is_official: true, author: @user, card_type: 'media')

    cards_to_complete = cards.first(3)
    cards_to_complete.each do |card|
      create(:user_content_completion, completable: card, user: @user, state: 'completed')
      card.reload
    end

    completed_ids = cards_to_complete.map(&:id)

    get :index, is_official: 'true', skip_content: {completed: "true"}, format: :json
    assert_response :success
    assert assigns(:cards)
    assert_equal 7, assigns(:cards).length
    assert_same_elements cards.map(&:id).reject { |id| completed_ids.include? id }, assigns(:cards).map{|c| c['id']}
  end

  class VideoStreamCardFetchTest < ActionController::TestCase
    setup do
      Card.any_instance.stubs :create_activity_stream
      VideoStream.any_instance.stubs :create_activity_stream
      stub_video_stream_index_calls

      org = create(:organization)
      @user = create(:user, organization: org)
      vcard1 = create(:card, author_id: @user.id, card_type: 'video_stream', card_subtype: 'simple')
      vs1 = create(:iris_video_stream, card: vcard1, status: 'upcoming', creator: @user, organization: @user.organization)
      vcard2 = create(:card, author_id: @user.id, card_type: 'video_stream', card_subtype: 'role_play')
      vs2 = create(:iris_video_stream, card: vcard2, status: 'upcoming', creator: @user, organization: @user.organization)
      api_v2_sign_in @user
    end

    test 'should return video_stream cards' do

      get :index, card_type: ['video_stream'], format: :json
      assert_response :success
      assert assigns(:cards)
      resp = get_json_response response
      response_card = resp["cards"][0]
      assert_equal 2, resp["cards"].count
      assert_equal "video_stream", response_card["card_type"]
      assert_response :ok
    end

    test 'should return simple subtype video_stream cards' do

      get :index, card_type: ['video_stream'], card_subtype: ['simple'], format: :json
      resp = get_json_response response
      response_card = resp["cards"][0]
      assert_equal 1, resp["cards"].count
      assert_equal "simple", response_card["card_subtype"]
      assert_response :ok
    end

    test 'should return role_play subtype video_stream cards' do

      get :index, card_type: ['video_stream'], card_subtype: ['role_play'], format: :json
      resp = get_json_response response
      response_card = resp["cards"][0]
      assert_equal 1, resp["cards"].count
      assert_equal "role_play", response_card["card_subtype"]
      assert_response :ok
    end
  end

  test 'should return system generated cards for is_ugc false' do
    ugc_card = create(:card, author_id: @user.id, card_type: 'media', organization: @user.organization)
    non_ugc_card = create(:card, author_id: nil, card_type: 'media', ecl_id: 'abc-def-ijk', organization: @user.organization)

    get :index, is_ugc: 'true', format: :json
    assert_equal 1, assigns(:cards).count
    assert_equal ugc_card.id, assigns(:cards)[0]["id"]

    get :index, is_ugc: 'false', format: :json
    assert_equal 1, assigns(:cards).count
    assert_equal non_ugc_card.id, assigns(:cards)[0]["id"]
  end

  test 'should return view count 0 for ecl cards' do
    skip #skipping these as we done modification to improve performance and it is present for mobile.
    Card.any_instance.stubs(:id).returns(nil)
    ecl_id = 'abc-def-ijk'
    non_ugc_card = create(:card, author_id: nil, card_type: 'media',
      ecl_id: ecl_id, organization: @user.organization
    )

    INFLUXDB_CLIENT.expects(:query).never

    get :index, is_ugc: 'false', format: :json

    resp = get_json_response(response)["cards"]

    assert_equal 1, resp.size
    assert_equal "ECL-#{ecl_id}", resp[0]["id"]
    assert_equal 0, resp[0]['views_count']
  end

  test 'channel id filter for cards index action' do
    uncurated_cards = create_list(:card, 4, author: @user)

    channel = create(:channel, curate_only: false, organization: @user.organization)
    uncurated_cards.each { |card| card.channels << channel}

    in_curation_cards = create_list(:card, 4, organization: channel.organization, author: nil)
    curated_channel = create(:channel, curate_only: true, organization: @user.organization)
    in_curation_cards.each {|card| card.channels << curated_channel}
    to_be_curated_cards = in_curation_cards.first(2)
    #curate the first two cards
    curated_channel.channels_cards.where(card_id:to_be_curated_cards.collect(&:id)).each do |channels_card|
      channels_card.curate
      channels_card.save
    end

    get :index, channel_id: [ channel.id, curated_channel.id ], format: :json
    assert_response :success

    expected_ids = uncurated_cards.collect(&:id) + to_be_curated_cards.collect(&:id)
    resp = get_json_response response

    assert_equal 6, resp['cards'].length
    assert_same_elements expected_ids, resp['cards'].map { |c| c['id'].to_i }
  end

  test 'channel id filter for cards index action from admin panel' do
    channel = create(:channel, is_private: true, organization: @org)
    card = create(:card, message: "test", title: "test title", card_type: 'media', channel_ids: [channel.id], author: @user)
    admin_user = create(:user, organization: @org, organization_role: 'admin')
    api_v2_sign_in(admin_user)
    get :index, channel_id: [channel.id], is_cms: true, format: :json
    assert_response :success
    resp = get_json_response response
    assert_equal 1, resp['cards'].length
    assert_equal [card.id], resp['cards'].map { |c| c['id'].to_i }
  end

  test 'should return is_paid from admin panel' do
    card = create(:card, message: "test", title: "test title", card_type: 'media', author: @user)
    admin_user = create(:user, organization: @org, organization_role: 'admin')
  
    api_v2_sign_in(admin_user)
    get :index, is_cms: true, format: :json
    assert_response :success
    resp = get_json_response response

    assert_equal [card.is_paid], resp['cards'].map { |c| c['is_paid'] }
  end

  test 'should return unauthorized for non admin/non team content admin user' do
    card = create(:card, message: "test", title: "test title", card_type: 'media', author: @user)
    team = create(:team, organization: @org)
    member_user = create(:user, organization: @org, organization_role: 'member')
    
    Role.create_standard_roles @org
    role_member = @org.roles.find_by(name: 'member')
    role_member.add_user(member_user)
    create(:role_permission, role: role_member, name: Permissions::MANAGE_GROUP_ANALYTICS)
    team.add_sub_admin member_user
    
    api_v2_sign_in(member_user)
    
    get :index, is_cms: true, format: :json
    assert_response :unauthorized
  end

  test 'should return all org content to admin user in admin pannel' do
    card = create(:card, message: "test", title: "test title", card_type: 'media', author: @user)
    team = create(:team, organization: @org)
    admin_user = create(:user, organization: @org, organization_role: 'admin')
    
    api_v2_sign_in(admin_user)
    
    get :index, is_cms: true, format: :json
    assert_response :success
    resp = get_json_response(response)
    assert_equal resp['cards'].map{|d| d['id'].to_i}, [card.id]
  end

  test 'topics filter for cards index action' do
    cards = create_list(:card, 10, author: @user)

    cards_with_both_tags = cards[0..1]
    cards_with_both_tags.each do |card|
      card.topics = "Web, Ruby"
      card.update_topics
    end
    cards_with_one_tag = cards[2..3]
    cards_with_one_tag.each do |card|
      card.topics = "Web"
      card.update_topics
    end
    cards_with_another_tag = cards[4..5]
    cards_with_another_tag.each do |card|
      card.topics = "Web"
      card.update_topics
    end

    cards_with_random_tag = cards[6..7]
    cards_with_random_tag.each do |card|
      card.topics = "randomTag"
      card.update_topics
    end

    expected_cards = cards[0..5]
    get :index, topics: ["Ruby", "Web"], format: :json
    assert_response :success
    resp = get_json_response response
    assert_equal 6, resp['cards'].length
    assert_same_elements expected_cards.collect(&:id), resp['cards'].map { |c| c['id'].to_i }
  end

  test ':api should return only user created cards' do
    user1 , user2 = create_list(:user, 2, organization: @user.organization)
    cards2 = create_list(:card, 15, author_id: user2.id, card_type: 'media')

    api_v2_sign_in user1

    get :index, author_id: user2.id, format: :json

    assert_response :success
    assert assigns(:cards)
    assert_same_elements cards2.map(&:id), assigns(:cards).map{|c| c['id']}
  end

  test 'api should include assignment information' do
    skip #skipping these as we done modification to improve performance and it is present for mobile.
    org = create(:organization)
    team = create(:team, organization: org)
    author, assignee = create_list(:user, 2, organization: org)
    assignment_card, non_assignment_card = create_list(:card, 2, author: author)

    assignment = create(:assignment, assignable: assignment_card, assignee: assignee)
    team_assignment = create(:team_assignment, assignment: assignment, assignor: author, team: team)

    api_v2_sign_in(assignee)

    get :index, format: :json

    resp = get_json_response(response)
    assignment_card_resp = resp['cards'].select {|card| card['id'] == assignment_card.id.to_s}.first
    assert_not_empty assignment_card_resp['assignment']
    assert_equal assignment.id, assignment_card_resp['assignment']['id']

    non_assignment_card_resp = resp['cards'].select {|card| card['id'] == non_assignment_card.id.to_s}.first
    assert_nil non_assignment_card_resp['assignment']
  end

  test 'should not return hidden cards' do
    org = create(:organization)
    user = create(:user, organization: org)
    api_v2_sign_in(user)
    author = create(:user, organization: org)
    cards = create_list(:card, 10, organization: org, author: author, card_type: 'media')
    hidden_cards = create_list(:card, 2, organization: org, author: author, card_type: 'media', hidden: true)

    get :index, format: :json
    assert_response :success
    assert assigns(:cards)
    assert_equal 10, assigns(:cards).length
    assert_same_elements cards.map(&:id), assigns(:cards).map{|c| c['id']}
    assert_empty hidden_cards.map(&:id) & assigns(:cards).map{|c| c['id']}
  end

  test 'return draft cards for author' do
    org = @user.organization
    author, another_author = create_list(:user, 2, organization: org)

    published_cards = create_list(:card, 3, author_id: author.id)
    draft_cards = create_list(:card, 2, author_id: author.id, state: 'draft')

    another_author_cards = create_list(:card, 2, author_id: another_author.id)

    api_v2_sign_in author

    get :index, author_id: author.id, state: ['draft'], format: :json

    resp = get_json_response(response)
    assert_equal 2, resp['cards'].size
    assert_same_elements draft_cards.map(&:id).map(&:to_s), resp['cards'].map {|c| c['id']}
  end

  test 'return draft and published cards for author' do
    org = @user.organization
    author, another_author = create_list(:user, 2, organization: org)

    published_cards = create_list(:card, 3, author_id: author.id)
    draft_cards = create_list(:card, 2, author_id: author.id, state: 'draft')

    another_author_cards = create_list(:card, 2, author_id: another_author.id)

    api_v2_sign_in author

    get :index, author_id: author.id, state: ['published', 'draft'], format: :json

    resp = get_json_response(response)
    assert_equal 5, resp['cards'].size
    assert_same_elements (published_cards + draft_cards).map(&:id).map(&:to_s), resp['cards'].map {|c| c['id']}
  end

  test 'should send only published cards for requested author' do
    org = @user.organization
    author, another_author = create_list(:user, 2, organization: org)

    published_cards = create_list(:card, 3, author_id: author.id)
    draft_cards = create_list(:card, 2, author_id: author.id, state: 'draft')

    api_v2_sign_in another_author

    get :index, author_id: author.id, state: ['published', 'draft'], format: :json

    resp = get_json_response(response)
    assert_equal 3, resp['cards'].size
    assert_same_elements published_cards.map(&:id).map(&:to_s), resp['cards'].map {|c| c['id']}
  end

  test 'should return filtered state cards for org admin' do
    org = @user.organization
    author, admin = create_list(:user, 2, organization: org)
    admin.update(organization_role: 'admin')

    published_cards = create_list(:card, 2, author_id: author.id, state: 'published')
    draft_cards     = create_list(:card, 2, author_id: author.id, state: 'draft')
    archived_cards  = create_list(:card, 2, author_id: author.id, state: 'archived')
    new_cards       = create_list(:card, 2, author_id: author.id, state: 'new')

    accessible_cards = archived_cards + new_cards

    api_v2_sign_in admin

    get :index, state: ['archived', 'new'], format: :json

    resp = get_json_response(response)
    assert_equal 4, resp['cards'].size
    assert_same_elements accessible_cards.map(&:id).map(&:to_s), resp['cards'].map {|c| c['id']}
  end

  test "should not return deleted, archived, new state cards if state is not present #admin" do
    org = @user.organization
    author, admin = create_list(:user, 2, organization: org)
    admin.update(organization_role: 'admin')

    archived_cards  = create_list(:card, 2, author_id: author.id, state: 'archived')
    new_cards       = create_list(:card, 2, author_id: author.id, state: 'new')
    published_cards       = create_list(:card, 2, author_id: author.id, state: 'published')

    api_v2_sign_in admin

    get :index, format: :json

    resp = get_json_response(response)
    assert_equal 2, resp['cards'].size
    assert_same_elements published_cards.map(&:id).map(&:to_s), resp['cards'].map {|c| c['id']}
  end

  test 'should include current user following information' do
    skip #skipping these as we done modification to improve performance and it is present for mobile.
    org = @user.organization
    user = create(:user, organization: org)
    api_v2_sign_in(user)

    author, followed_author = create_list(:user, 2, organization: org)
    create(:follow, followable: followed_author, user: user)
    card = create(:card, organization: org, author: author, card_type: 'media')
    followed_author_card = create(:card, organization: org, author: followed_author, card_type: 'media')

    get :index, format: :json
    assert_response :success
    resp_cards = get_json_response(response)['cards']

    assert_equal 2, resp_cards.size
    assert_same_elements [card, followed_author_card].map(&:id).map(&:to_s), resp_cards.map{|c| c['id']}
    refute resp_cards.select {|c| c['id'] == card.id.to_s}.first['author']['is_following']
    assert resp_cards.select {|c| c['id'] == followed_author_card.id.to_s}.first['author']['is_following']
  end

  test 'should return all cards of the organization with total count key for admin content items' do
    org = @user.organization
    users = create_list(:user, 2, organization: org)
    admin_user = create(:user, organization: org, organization_role: 'admin')

    api_v2_sign_in(admin_user)

    media_draft_card = create(:card, organization: org, author_id: users[0].id, card_type: 'media', state: 'draft')
    course_published_card = create(:card, organization: org, author_id: users[1].id, card_type: 'course', state: 'published')
    poll_published_card = create(:card, organization: org, author_id: admin_user.id, card_type: 'poll', state: 'published')
    user2_poll_published_card = create(:card, organization: org, author_id: users[1].id, card_type: 'poll', state: 'published')
    user1_poll_published_card = create(:card, organization: org, author_id: users[0].id, card_type: 'course', state: 'published')
    pack_draft_card = create(:card, organization: org, author_id: users[0].id, card_type: 'pack', state: 'draft')
    user1_video_stream_card = create(:card, organization: org, author_id: users[1].id, card_type: 'video_stream', state: 'published')

    get :index, card_type: ['media', 'course', 'poll'], state: ['draft', 'published'], is_cms: 'true', format: :json

    assert_response :success
    resp = get_json_response(response)

    assert_equal 5, resp['total']
    assert_same_elements [ users[0].id, users[1].id, admin_user.id, users[1].id, users[0].id ], resp['cards'].map{ |a| a['author']['id']   }
    assert_same_elements ['media', 'course', 'poll'], resp['cards'].map{ |a| a['card_type'] }.uniq
  end

  test 'should return cards of the organization for admin with sorted key' do
    org = create(:organization)
    users = create_list(:user, 2, organization: org)
    admin_user = create(:user, organization: org, organization_role: 'admin')

    api_v2_sign_in(admin_user)

    media_card = create(:card, organization: org, author_id: users[0].id, card_type: 'media', created_at: 2.minutes.from_now)
    course_card = create(:card, organization: org, author_id: users[0].id, card_type: 'course', created_at: 4.minutes.from_now)
    poll_card = create(:card, organization: org, author_id: users[0].id, card_type: 'poll', created_at: 3.minutes.from_now)

    get :index, card_type: ['media', 'course', 'poll'], state: ['draft', 'published'], is_cms:'true', sort: 'created', order: 'desc', format: :json

    assert_response :success
    resp = get_json_response(response)

    assert_equal 3, resp['total']
    assert_equal [course_card.id, poll_card.id, media_card.id], resp['cards'].map{ |a| a['id'].to_i}
  end

  test 'should query ugc/non-ugc cards for admin' do
    org = create(:organization)
    users = create_list(:user, 2, organization: org)
    admin_user = create(:user, organization: org, organization_role: 'admin')

    api_v2_sign_in(admin_user)

    ecl_card = create(:card, organization: org, message: 'ecl_card', card_type: 'media', author_id: nil, ecl_id: "ECL-1234-3456-23452")
    ugc_card = create(:card, organization: org, message: 'ugc_card', card_type: 'media', author_id: users[0].id)

    get :index, card_type: ['media', 'course', 'poll'], state: ['draft', 'published'], is_cms:'true', is_ugc: 'true', q: 'card', format: :json

    assert_response :success
    resp = get_json_response(response)

    assert_equal 1, resp['total']
    assert_equal [ugc_card.id], resp['cards'].map{ |a| a['id'].to_i}

    get :index, card_type: ['media', 'course', 'poll'], state: ['draft', 'published'], is_cms:'true', is_ugc: 'false', q: 'card', format: :json
    assert_response :success
    resp = get_json_response(response)

    assert_equal 1, resp['total']
    assert_equal [ecl_card.id], resp['cards'].map{ |a| a['id'].to_i}
  end

  test 'should return all cards except hidden_cards where is_cms is not supplied for admin content items' do
    org = @user.organization
    user1 = create(:user, organization: org)
    user2 = create(:user, organization: org)
    admin_user = create(:user,organization: org, organization_role: 'admin')
    api_v2_sign_in(admin_user)

    media_draft_card = create(:card, organization: org, author_id: user1.id, card_type: 'media', state: 'draft')
    course_published_card = create(:card, organization: org, author_id: user2.id, card_type: 'course', state: 'published')
    poll_published_card = create(:card, organization: org, author_id: admin_user.id, card_type: 'poll', state: 'published')
    user2_poll_published_card = create(:card, organization: org, author_id: user2.id, card_type: 'poll', state: 'published')
    hidden_user1_poll_published_card = create(:card, organization: org, author_id: user1.id, card_type: 'course', state: 'published', hidden: true)
    pack_draft_card = create(:card, organization: org, author_id: user1.id, card_type: 'pack', state: 'draft')
    user1_video_stream_card = create(:card, organization: org, author_id: user2.id, card_type: 'video_stream', state: 'published')

    get :index, card_type: ['media', 'course', 'poll'], state: ['draft', 'published'], format: :json

    assert_response :success
    resp = get_json_response(response)

    assert_equal 4, resp['total']
    assert_same_elements ['media', 'course', 'poll',], resp['cards'].map{ |a| a['card_type'] }.uniq
  end

  test 'normal user should not see others cards when is_cms is passed' do
    org = @user.organization
    users = create_list(:user, 2, organization: org, organization_role: 'member')
    api_v2_sign_in(users[0])

    media_draft_card = create(:card, organization: org, author_id: users[0].id, card_type: 'media', state: 'draft')
    course_published_card = create(:card, organization: org, author_id: users[1].id, card_type: 'course', state: 'published')
    user2_poll_published_card = create(:card, organization: org, author_id: users[1].id, card_type: 'poll', state: 'published')
    user1_poll_published_card = create(:card, organization: org, author_id: users[0].id, card_type: 'course', state: 'published')
    pack_draft_card = create(:card, organization: org, author_id: users[0].id, card_type: 'pack', state: 'draft')
    user1_video_stream_card = create(:card, organization: org, author_id: users[1].id, card_type: 'video_stream', state: 'published')

    get :index, card_type: ['media', 'course', 'poll'], state: ['draft', 'published'], author_id: users[0].id, format: :json, is_cms: 'true'

    assert_response :success
    resp = get_json_response(response)
    assert_equal 2, resp['total']
    assert_same_elements ['media', 'course'], resp['cards'].map{ |a| a['card_type'] }.uniq
  end

  test 'should search cards in cms for full name author' do
    user = create(:user, organization: @org, organization_role: 'admin')
    cards = create_list(:card, 2, author: @user)
    api_v2_sign_in(user)

    get :index, q: @user.full_name, is_cms: true, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal 2, resp['cards'].count
    assert_same_ids cards.map(&:id), resp['cards'].map{ |c| c['id'] }
  end

  test 'should return cards for group_sub_admin user' do
    team = create(:team, organization: @org)
    Role.create_standard_roles @org
    role_member = @org.roles.find_by(name: 'member')
    create(:role_permission, role: role_member, name: Permissions::MANAGE_GROUP_CONTENT)
    user = create(:user, organization: @org)
    user1 = create(:user, organization: @org)
    role_member.add_user(user)
    team.add_sub_admin(user)
    team.add_member(user1)
    card = create(:card, author: user)
    card1 = create(:card, author: user1)


    api_v2_sign_in(user)
    get :index, is_cms: true, format: :json
    assert_response :ok

    resp = get_json_response(response)
    assert_equal 2, resp['cards'].count
    assert_equal card.id.to_s, resp['cards'].first['id']
    assert_equal card1.id.to_s, resp['cards'].second['id']
  end

  test 'should return cards filter by group_id if is_cms: true for group_sub_admin' do
    teams = create_list(:team, 2, organization: @org)
    Role.create_standard_roles @org
    role_member = @org.roles.find_by(name: 'member')
    create(:role_permission, role: role_member, name: Permissions::MANAGE_GROUP_CONTENT)
    user = create(:user, organization: @org)
    role_member.add_user(user)
    users = create_list(:user, 2, organization: @org)
    teams.first.add_sub_admin(user)
    teams.first.add_member(users.first)
    teams.second.add_to_team(users)
    teams.second.add_sub_admin(user)
    card = create(:card, author: user)
    card1 = create(:card, author: users.first)
    create(:card, author: users.second)


    api_v2_sign_in(user)
    get :index, group_id: [teams.first.id.to_s], is_cms: true, format: :json
    assert_response :ok

    resp = get_json_response(response)
    assert_equal 2, resp['cards'].count
    assert_equal card.id.to_s, resp['cards'].first['id']
    assert_equal card1.id.to_s, resp['cards'].second['id']
  end

  test 'should return private cards sub_admin in team type he sub_admin' do
    teams = create_list(:team, 2, organization: @org)
    Role.create_standard_roles @org
    role_member = @org.roles.find_by(name: 'member')
    create(:role_permission, role: role_member, name: Permissions::MANAGE_CONTENT)
    user = create(:user, organization: @org)
    role_member.add_user(user)
    users = create_list(:user, 2, organization: @org)
    teams.first.add_sub_admin(user)
    teams.first.add_member(users.first)
    teams.second.add_to_team(users)
    teams.second.add_member(user)
    card = create(:card, author: user)
    card1 = create(:card, author: users.first, is_public: false)
    create(:card, author: users.second, is_public: false)

    api_v2_sign_in(user)
    get :index, is_cms: 'true', format: :json
    assert_response :ok

    resp = get_json_response(response)
    assert_equal 2, resp['cards'].count
    assert_equal card.id.to_s, resp['cards'].first['id']
    assert_equal card1.id.to_s, resp['cards'].second['id']
  end

  test 'should show private cards that the user has access to on non_cms API even if user is group_sub_admin' do
    team = create(:team, organization: @org)
    Role.create_standard_roles @org
    role_member = @org.roles.find_by(name: 'member')
    create(:role_permission, role: role_member, name: Permissions::MANAGE_GROUP_CONTENT)
    user = create(:user, organization: @org)
    team.add_sub_admin(user)
    role_member.add_user(user)
    user1 = create(:user, organization: @org)
    private_card = create(:card, organization: @org, author_id: user1.id, is_public: false)
    create(:card_user_share, card_id: private_card.id, user_id: user.id)

    api_v2_sign_in(user)
    get :index, format: :json
    assert_response :ok
    
    resp = get_json_response(response)
    assert_equal 1, resp['cards'].length
    assert private_card.id, resp['cards'].map{|d| d['id'].to_i}.first
  end

  test 'should show private cards of the users who are from the group_sub_admins team' do
    team = create(:team, organization: @org)
    
    user = create(:user, organization: @org)
    user1 = create(:user, organization: @org)
    user2 = create(:user, organization: @org)

    Role.create_standard_roles @org
    role_member = @org.roles.find_by(name: 'member')
    create(:role_permission, role: role_member, name: Permissions::MANAGE_GROUP_CONTENT)
    role_member.add_user(user)

    team.add_sub_admin(user)
    team.add_member(user1)

    private_card = create(:card, organization: @org, author_id: user1.id, is_public: false)
    private_card1 = create(:card, organization: @org, author_id: user2.id, is_public: false)
    
    create(:card_user_share, card_id: private_card1.id, user_id: user.id)
    
    api_v2_sign_in(user)
    get :index, is_cms: true, format: :json
    assert_response :ok
    
    resp = get_json_response(response)
    assert_equal 1, resp['cards'].length
    assert_includes resp['cards'].map{|d| d['id'].to_i}, private_card.id
    assert_not_includes resp['cards'].map{|d| d['id'].to_i}, private_card1.id
  end

  test 'should list all active purchased cards for a user' do
    user1 = create(:user, organization: @org)
    user2 = create(:user, organization: @org)
    card1 = create(:card, author: @user1, organization_id: @org.id)
    card2 = create(:card, author: @user1, organization_id: @org.id)
    card3 = create(:card, author: @user1, organization_id: @org.id)

    api_v2_sign_in(user2)

    user_card_subscription1 = create(:card_subscription, user_id: user2.id, card_id: card1.id, transaction_id: 1)
    user_card_subscription2 = create(:card_subscription, user_id: user2.id, card_id: card2.id, transaction_id: 2)
    user_card_subscription3 = create(:card_subscription, user_id: user2.id, card_id: card3.id, transaction_id: 2,
                                     start_date: Date.yesterday-1, end_date: Date.yesterday)
    get :purchased, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal 2, resp['cards'].count
    assert_same_ids [card1.id, card2.id], resp['cards'].map{ |c| c['id'] }
  end

  test 'api should not include dismised assignment information and isAssigned should be false' do
    org = create(:organization)
    team = create(:team, organization: org)
    author, assignee = create_list(:user, 2, organization: org)
    assignment_card, dismiss_assignment_card = create_list(:card, 2, author: author)

    assigned_assignment = create(:assignment, assignable: assignment_card, assignee: assignee, state: 'assigned')
    dismissed_assignment = create(:assignment, assignable: dismiss_assignment_card, assignee: assignee, state: 'dismissed')

    api_v2_sign_in(assignee)

    get :index, format: :json

    resp = get_json_response(response)
    assigned_card_resp = resp['cards'].select {|card| card['id'] == assignment_card.id.to_s}.first
    assert assigned_card_resp['is_assigned']

    dismissed_assignment_card_resp = resp['cards'].select {|card| card['id'] == dismiss_assignment_card.id.to_s}.first
    refute dismissed_assignment_card_resp['is_assigned']
  end

  test ':api should  return cards requested by id' do
    user1 = create(:user, organization: @org)
    uncurated_card = create(:card, author: user1)

    channel = create(:channel, is_private: true, curate_only: false,  organization: @org)
    uncurated_card.channels << channel

    author = create(:user, organization: @org)
    card = create(:card, organization: @org, author: author, card_type: 'media')

    public_channel = create(:channel, is_private: false, curate_only: false,  organization: @org)
    new_card = create(:card, organization: @org, author: @user)
    new_card.channels << public_channel

    get :card_by_ids, card_ids: [uncurated_card.id, card.id, new_card.id], format: :json
    resp = get_json_response(response)
    assert_response :success
    assert_equal 3, resp["cards"].length
  end

  class CardPermissionTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      @users_list = create_list(:user, 3, organization: @org)

      @channel = create(:channel, organization: @org, only_authors_can_post: false)
      @users_list.each do |user|
        create(:follow, followable: @channel, user_id: user.id)
      end

      @card = create(:card, author_id: @users_list[0].id, is_public: false)

      create(:channels_card, channel_id: @channel.id, card_id: @card.id)
      create(:card_user_permission, card_id: @card.id, user_id: @users_list[1].id)
    end

    test 'restricted card should not show to users of channel if they dont have access to it' do
      api_v2_sign_in @users_list[2]
      get :index, channel_id: [@channel.id], format: :json
      resp = get_json_response(response)
      assert_empty resp['cards']
    end

    test 'restricted card should be visible to author if he has access to the channel' do
      api_v2_sign_in @users_list[0]
      get :index, channel_id: [@channel.id], format: :json
      resp = get_json_response(response)
      assert_equal @card.id, resp['cards'][0]['id'].to_i
    end

    test 'restricted card should be visible to user card is explicity restricted to if he has access to the channel' do
      api_v2_sign_in @users_list[1]
      get :index, channel_id: [@channel.id], format: :json
      resp = get_json_response(response)
      assert_equal @card.id, resp['cards'][0]['id'].to_i
    end

    test 'restricted card through team should be visible only to user who belongs to the team' do
      team_list = create_list(:team, 2, organization_id: @org.id)
      channel = create(:channel, organization: @org, only_authors_can_post: false)

      create(:teams_user, user_id: @users_list[0].id, team_id: team_list[0].id)
      create(:teams_user, user_id: @users_list[0].id, team_id: team_list[1].id)

      card = create(:card, is_public: false, author_id: @users_list[1].id, organization_id: @org.id)
      create(:channels_card, channel_id: @channel.id, card_id: card.id)

      create(:card_team_permission, card_id: @card.id, team_id: team_list[1].id)

      api_v2_sign_in @users_list[0]
      get :index, channel_id: [@channel.id], format: :json
      resp = get_json_response(response)
      assert_includes resp['cards'].map{|d| d['id'].to_i}, card.id
    end
  end
end

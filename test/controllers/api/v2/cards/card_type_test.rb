require 'test_helper'

class Api::V2::CardsControllerTest < ActionController::TestCase
  setup do
    stub_video_stream_index_calls
    EdcastActiveJob.stubs :perform_later
    IndexJob.stubs :perform_later
    stub_request(:get, "https://cdn.filestackcontent.com/5Gw6KbtrSUaAl9SokjBJ")
    stub_request(:get, "https://cdn.filestackcontent.com/5Gw6KbtrSUaAl9SokjCK")

    @user = create(:user)
    @org = @user.organization

    api_v2_sign_in @user

    # @filestack1 & @filestack2 contains:
    # 0: image
    # 1: video
    # 2: audio
    # 3: file
    @filestack1 = [{
      filename: "image1.png",
      handle: "5Gw6KbtrSUaAl9SokjBJ",
      mimetype: "image/png",
      size:156333,
      source: "local_file_system",
      status: "Stored",
      url: "https://cdn.filestackcontent.com/5Gw6KbtrSUaAl9SokjBJ"
    },
    {
      filename: "SampleVideo_1280x720_1mb.mp4",
      handle: "5Gw6KbtrSUaAl9SokjBJ",
      mimetype: "video/mp4",
      size:156333,
      source: "local_file_system",
      status: "Stored",
      url: "https://cdn.filestackcontent.com/5Gw6KbtrSUaAl9SokjBJ"
    },
    {
      filename: "audio.mp3",
      handle: "5Gw6KbtrSUaAl9SokjBJ",
      mimetype: "audio/mp3",
      size:156333,
      source: "local_file_system",
      status: "Stored",
      url: "https://cdn.filestackcontent.com/5Gw6KbtrSUaAl9SokjBJ"
    },
    {
      filename: "test_csv.csv",
      handle: "5Gw6KbtrSUaAl9SokjBJ",
      mimetype: "application/json",
      size:156333,
      source: "local_file_system",
      status: "Stored",
      url: "https://cdn.filestackcontent.com/5Gw6KbtrSUaAl9SokjBJ"
    }]

    @filestack2 = [{
      filename: "image2.png",
      handle: "5Gw6KbtrSUaAl9SokjCK",
      mimetype: "image/png",
      size:156333,
      source: "local_file_system",
      status: "Stored",
      url: "https://cdn.filestackcontent.com/5Gw6KbtrSUaAl9SokjCK"
    },
    {
      filename: "SampleVideo_1280x720_1mb.mp4",
      handle: "5Gw6KbtrSUaAl9SokjCK",
      mimetype: "video/mp4",
      size:156333,
      source: "local_file_system",
      status: "Stored",
      url: "https://cdn.filestackcontent.com/5Gw6KbtrSUaAl9SokjCK"
    },
    {
      filename: "audio.mp3",
      handle: "5Gw6KbtrSUaAl9SokjCK",
      mimetype: "audio/mp3",
      size:156333,
      source: "local_file_system",
      status: "Stored",
      url: "https://cdn.filestackcontent.com/5Gw6KbtrSUaAl9SokjCK"
    },
    {
      filename: "test_csv.csv",
      handle: "5Gw6KbtrSUaAl9SokjCK",
      mimetype: "application/json",
      size:156333,
      source: "local_file_system",
      status: "Stored",
      url: "https://cdn.filestackcontent.com/5Gw6KbtrSUaAl9SokjCK"
    }]
  end

  test '#create do not create file resources for media-image card' do
    post :create, card: { message: Faker::Lorem.sentence, filestack: [@filestack1[0]], card_subtype: 'image' }, format: :json

    card = Card.last
    assert_empty card.file_resources
  end

  test '#update do not create file resources for media-image card' do
    test_file = Rails.root.join('test/fixtures/images/logo.png')

    card = create :card, filestack: [@filestack1[0]], author: @user, organization: @org, card_subtype: 'image', card_type: 'media'
    # Consider adding file resource to the card.
    file_resource = FileResource.create(attachment: test_file.open, filestack: [@filestack1[0]], attachable: card)

    # Now change the filestack content.
    put :update, card: { message: Faker::Lorem.sentence, filestack: [@filestack2[0]], card_subtype: 'image',
      file_resource_ids: [file_resource.id] }, id: card.id, format: :json

    assert_equal 1, card.reload.file_resources.count
    # file_resources must not be changed to the new content.
    assert_equal [@filestack1[0].stringify_keys], card.file_resources[0].filestack
  end

  test '#create do not create file resources for poll-image card' do
    options = [{ label: 'X' }, { label: 'Y' }]

    post :create, card: { message: Faker::Lorem.sentence, options: options,
      filestack: [@filestack1[0]], card_subtype: 'image', card_type: 'poll' },
      format: :json

    card = Card.last

    assert_empty card.file_resources
  end

  test '#update do not create file resources for poll-image card' do
    test_file = Rails.root.join('test/fixtures/images/logo.png')

    card = create :card, filestack: [@filestack1[0]], author: @user, organization: @org, card_subtype: 'image', card_type: 'poll'
    option = create(:quiz_question_option, quiz_question: card, label: 'X')
    # Consider adding file resource to the card.
    file_resource = FileResource.create(attachment: test_file.open, filestack: [@filestack1[0]], attachable: card)

    # Now change the filestack content.
    put :update, card: { message: Faker::Lorem.sentence,
      filestack: [@filestack2[0]], card_subtype: 'image',
      options: [{ label: option.label, id: option.id }],
      file_resource_ids: [file_resource.id] }, id: card.id, format: :json

    assert_equal 1, card.reload.file_resources.count
    # file_resources must not be changed to the new content.
    assert_equal [@filestack1[0].stringify_keys], card.file_resources[0].filestack
  end

  test '#create do not create file resources for media-video card' do
    post :create, card: { message: Faker::Lorem.sentence, filestack: [@filestack1[1]], card_subtype: 'video' }, format: :json

    card = Card.last
    assert_empty card.file_resources
  end

  test '#update do not create file resources for media-video card' do
    test_file = Rails.root.join('test/fixtures/images/logo.png')

    card = create :card, filestack: [@filestack1[1]], author: @user, organization: @org, card_subtype: 'video', card_type: 'media'
    # Consider adding file resource to the card.
    file_resource = FileResource.create(attachment: test_file.open, filestack: [@filestack1[1]], attachable: card)

    # Now change the filestack content.
    put :update, card: { message: Faker::Lorem.sentence,
      filestack: [@filestack2[1]], card_subtype: 'video',
      file_resource_ids: [file_resource.id] }, id: card.id, format: :json

    assert_equal 1, card.reload.file_resources.count
    # file_resources must not be changed to the new content.
    assert_equal [@filestack1[1].stringify_keys], card.file_resources[0].filestack
  end

  test '#create do not create file resources for media-audio card' do
    post :create, card: { message: Faker::Lorem.sentence, filestack: [@filestack1[2]], card_subtype: 'audio' }, format: :json

    card = Card.last
    assert_empty card.file_resources
  end

  test '#update do not create file resources for media-audio card' do
    test_file = Rails.root.join('test/fixtures/images/logo.png')

    card = create :card, filestack: [@filestack1[2]], author: @user, organization: @org, card_subtype: 'audio', card_type: 'media'
    # Consider adding file resource to the card.
    file_resource = FileResource.create(attachment: test_file.open, filestack: [@filestack1[2]], attachable: card)

    # Now change the filestack content.
    put :update, card: { message: Faker::Lorem.sentence,
      filestack: [@filestack2[2]], card_subtype: 'audio',
      file_resource_ids: [file_resource.id] }, id: card.id, format: :json

    assert_equal 1, card.reload.file_resources.count
    # file_resources must not be changed to the new content.
    assert_equal [@filestack1[2].stringify_keys], card.file_resources[0].filestack
  end

  test '#create do not create file resources for media-file card' do
    post :create, card: { message: Faker::Lorem.sentence, filestack: [@filestack1[3]], card_subtype: 'file' }, format: :json

    card = Card.last
    assert_empty card.file_resources
  end

  test '#update do not create file resources for media-file card' do
    test_file = Rails.root.join('test/fixtures/files/test.csv')

    card = create :card, filestack: [@filestack1[3]], author: @user, organization: @org, card_subtype: 'file', card_type: 'media'
    # Consider adding file resource to the card.
    file_resource = FileResource.create(attachment: test_file.open, filestack: [@filestack1[3]], attachable: card)

    # Now change the filestack content.
    put :update, card: { message: Faker::Lorem.sentence,
      filestack: [@filestack2[3]], card_subtype: 'file',
      file_resource_ids: [file_resource.id] }, id: card.id, format: :json

    assert_equal 1, card.reload.file_resources.count
    # file_resources must not be changed to the new content.
    assert_equal [@filestack1[3].stringify_keys], card.file_resources[0].filestack
  end

  test '#create creates file resource for a resource associated to a media-link card' do
    resource = create :resource

    post :create, card: { message: Faker::Lorem.sentence, resource_id: resource.id }, format: :json

    card = Card.last
    assert_empty card.file_resources
    assert card.resource
    assert_empty card.file_resources
  end

  test '#update creates file resource for a resource associated to a media-link card' do
    test_file = Rails.root.join('test/fixtures/images/logo.png')

    # Consider adding a resource to the card.
    resource1 = create :resource
    card = create :card, author: @user, organization: @org, resource_id: resource1

    # update the card with another link.
    resource2 = create :resource
    put :update, card: { message: Faker::Lorem.sentence, resource_id: resource2.id }, id: card.id, format: :json

    card.reload
    assert_equal resource2.id, card.resource_id
    # file_resources still remains empty
    assert_empty card.file_resources
  end

  test '#create do not create a for a pathway' do
    resource = create :resource

    post :create, card: { card_type: 'pack', message: Faker::Lorem.sentence }, format: :json

    card = Card.last
    assert_empty card.file_resources
  end

  test '#update do not create a file resource for a pathway' do
    test_file = Rails.root.join('test/fixtures/images/logo.png')

    card = create :card, author: @user, organization: @org
        # Consider adding a file resource to the card.
    file_resource = FileResource.create(attachment: test_file.open, attachable: card, filestack: [@filestack2[0]])

    # Now change the filestack content.
    put :update, card: { message: Faker::Lorem.sentence, filestack: [@filestack2[0]],
      file_resource_ids: [file_resource.id] }, id: card.id, format: :json

    card.reload
    # file_resources must not be changed to the new content.
    assert_equal 1, card.reload.file_resources.count
    assert_equal [@filestack2[0].stringify_keys], card.file_resources[0].filestack
  end

  test '#create do not create a for a journey' do
    resource = create :resource

    post :create, card: { card_type: 'journey', card_subtype: 'self_paced', message: Faker::Lorem.sentence }, format: :json

    card = Card.last

    assert_empty card.file_resources
    assert_equal 'journey', card.card_type
    assert_equal 'self_paced', card.card_subtype
  end

  test '#update do not create a file resource for a journey' do
    test_file = Rails.root.join('test/fixtures/images/logo.png')

    card = create :card, author: @user, organization: @org
    # Consider adding a file resource to the card.
    file_resource = FileResource.create(attachment: test_file.open, attachable: card, filestack: [@filestack2[0]])

    # Now change the filestack content.
    put :update, card: { message: Faker::Lorem.sentence, filestack: [@filestack2[0]],
      file_resource_ids: [file_resource.id] }, id: card.id, format: :json

    card.reload
    # file_resources must not be changed to the new content.
    assert_equal 1, card.reload.file_resources.count
    assert_equal [@filestack2[0].stringify_keys], card.file_resources[0].filestack
  end
end

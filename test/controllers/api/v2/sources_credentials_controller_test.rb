require 'test_helper'

class Api::V2::SourcesCredentialsControllerTest < ActionController::TestCase
  setup do
    @user = create(:user)
    @admin = create(:user, organization_id: @user.organization_id, organization_role: "admin")
    @edcast_user = create(:user, email: "sunil@edcast.com", organization_id: @user.organization_id)
  end

  test ':api create credentials and generate secret key' do
    api_v2_sign_in(@edcast_user)
    assert_difference -> { SourcesCredential.count } do
      post :create, source_id: 'dummy-source-id', auth_api_info: { hostname: 'www.dummyhost.com' }, source_name: 'dummy source', format: :json
      assert_response :ok
    end
    recent_generated = SourcesCredential.last
    assert_not_nil recent_generated.shared_secret
  end

  test "should return 401 for user other than admin" do
    api_v2_sign_in(@user)
    assert_no_difference -> { SourcesCredential.count } do
      post :create, format: :json
      assert_response 401
    end
  end

  test ':api update credentials must update the auth api info and source name' do
    api_v2_sign_in(@edcast_user)
    source_credential = create(:sources_credential,
      organization_id: @user.organization_id,
      source_id: 'dummy-source-id', auth_api_info: { hostname: 'www.dummyhost.com' }, source_name: 'dummy source'
    )

    post :update, id: source_credential.id, auth_api_info: { hostname: 'www.dummyhost-new.com' }, source_name: 'dummy source new', format: :json

    recent_generated = SourcesCredential.last

    assert_equal 'www.dummyhost-new.com', recent_generated.auth_api_info['hostname']
    assert_equal 'dummy source new', recent_generated.source_name
  end

  test ':api destroy a source credential' do
    api_v2_sign_in(@edcast_user)
    source_credential = create(:sources_credential,
      organization_id: @user.organization_id,
      source_id: 'dummy-source-id', auth_api_info: { hostname: 'www.dummyhost.com' }, source_name: 'dummy source'
    )

    assert_difference -> { SourcesCredential.count }, -1 do
      delete :destroy, id: source_credential.id, format: :json
      assert_response :ok
    end
    assert_empty SourcesCredential.all
  end

  test ":api return list of sources credentials" do
    api_v2_sign_in(@admin)

    crs = create_list(:sources_credential, 3,
      organization_id: @user.organization_id
    )

    get :index, format: :json

    resp = get_json_response(response)
    assert_equal 3, resp["sources_credentials"].length
  end

  test ":api return info of a source credential" do
    api_v2_sign_in(@admin)

    source_credential = create(:sources_credential,
      organization_id: @user.organization_id,
      source_id: 'dummy-source-id', auth_api_info: { hostname: 'www.dummyhost.com' }, source_name: 'dummy source'
    )

    get :show, id: source_credential.id,  format: :json

    resp = get_json_response(response)
    assert_equal 'dummy-source-id', resp["sources_credential"]["source_id"]
    assert_equal 'www.dummyhost.com', resp["sources_credential"]["auth_api_info"]["hostname"]
  end

end

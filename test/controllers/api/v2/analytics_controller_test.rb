require 'test_helper'

class Api::V2::AnalyticsControllerTest < ActionController::TestCase
  self.use_transactional_fixtures = false

  test 'record_event calls GenericEventRecorder and returns 200' do
    org  = create(:organization)
    user = create(:user, organization: org)
    api_v2_sign_in(user)

    params    = {
      measurement: "cards",
      event:       "card_created",
      card_id:     "123",
      attributes:  { 'a' => '1' }
    }
    event_obj = { mock: "mock" }

    recorder_mock = mock
    Analytics::GenericEventRecorder.expects(:new).with(
      user_id: user.id.to_s,
      org_id:  org.id.to_s,
      params:  params.stringify_keys
    ).returns recorder_mock
    recorder_mock.expects(:enqueue_recording_job).returns [event_obj, nil]

    post :record_event, **params

    assert_response :ok
    assert_equal event_obj, get_json_response(response, symbolize: true)
  end

  test 'record_event calls GenericEventRecorder and returns 422 upon error' do
    org  = create(:organization)
    user = create(:user, organization: org)
    api_v2_sign_in(user)

    params    = {
      measurement: "cards",
      event:       "card_created",
      card_id:     "123",
      attributes:  { 'a' => '1' }
    }

    recorder_mock = mock
    Analytics::GenericEventRecorder.expects(:new).with(
      user_id: user.id.to_s,
      org_id:  org.id.to_s,
      params:  params.stringify_keys
    ).returns recorder_mock
    recorder_mock.expects(:enqueue_recording_job).returns [nil, "error"]

    post :record_event, **params

    assert_response :unprocessable_entity
    assert_equal({message: "error"}, get_json_response(response, symbolize: true))
  end

  # NOTE: deprecated
  test 'should return 410 for create api' do
    org = create(:organization)
    user = create(:user, organization: org)
    card = create(:card, author: create(:user, organization: org))

    api_v2_sign_in(user)

    post :create, event: {
      type: "impression",
      content_id: card.id,
      content_type: 'Card',
      properties: {view: 'full'}
    }, format: :json
    assert_response :gone
  end

  test ':api get group performance' do
    org = create(:organization)
    admin = create(:user, organization_role: 'admin', organization: org)
    member = create(:user, organization: org)

    team = create(:team, organization: org)
    team.add_admin(admin)

    group_query = "select * from \"groups\" where org_id = '%{org_id}' AND group_id = %{group_id} AND event = 'group_user_added'"
    group_query_params = {
      org_id: org.id,
      group_id: team.id
    }
    group_query_resp = influxdb_group_query_resp(team, [admin, member])
    INFLUXDB_CLIENT.expects(:query).with(
      group_query,
      params: group_query_params
    ).returns(group_query_resp)

    group_performance_query = "select count(card_id) from cards where _user_id =~ /^(#{[admin.id, member.id].join('|')})$/ group by event"
    INFLUXDB_CLIENT.expects(:query).with(group_performance_query).returns(influxdb_group_performance_resp)

    api_v2_sign_in(admin)
    get :group_performance, group_id: team.id, format: :json

    resp = get_json_response(response, symbolize: true)
    assert_equal resp, influxdb_group_performance_resp
  end

  test 'invalid group id request for group performance' do
    org, another_org = create_list(:organization, 2)
    admin = create(:user, organization_role: 'admin', organization: org)
    team = create(:team, organization: another_org)

    api_v2_sign_in(admin)
    get :group_performance, group_id: team.id, format: :json

    assert_response :not_found
  end

  test 'missing group id for group performance request' do
    org = create(:organization)
    admin = create(:user, organization_role: 'admin', organization: org)

    api_v2_sign_in(admin)
    get :group_performance, format: :json

    assert_response :unprocessable_entity
  end

  test ':api /visit records view event on card' do
    org = create(:organization)
    viewer = create(:user, organization: org)
    card = create(:card, author: create(:user, organization: org))

    api_v2_sign_in viewer

    Analytics::ViewEventMetricRecorder.
      expects(:record_view_event).
      with(entity: card, viewer: viewer)

    post :visit, entity_type: 'Card', entity_id: card.id
    assert_response :ok
  end

  test 'api /visit should handle ecl card visits' do
    org = create(:organization)
    viewer = create(:user, organization: org)

    Analytics::ViewEventMetricRecorder.expects(:record_view_event).once

    ecl_id = 'abc-def-ijk'

    ecl_card = create :card
    ecl = mock
    ecl.expects(:card_from_ecl_id).returns(ecl_card)
    EclApi::CardConnector.expects(:new).with(ecl_id: ecl_id, organization_id: viewer.organization_id).returns(ecl)

    api_v2_sign_in viewer
    post :visit, entity_id: ('ECL-' + ecl_id), entity_type: 'Card', format: :json

    assert_response :ok
  end

  def influxdb_group_performance_resp
    [    {
        "name": "cards",
        "tags": {
            "event": "card_created"
        },
        "values": [
            {
                "time": "1970-01-01T00:00:00Z",
                "count": 1
            }
        ]
    },
    {
        "name": "cards",
        "tags": {
            "event": "card_deleted"
        },
        "values": [
            {
                "time": "1970-01-01T00:00:00Z",
                "count": 2
            }
        ]
    }]
  end

  def influxdb_group_query_resp(group, users)
    [{
        "name" => "groups", "tags" => nil, "values" => [{
            "time" => "2017-10-13T20:10:56Z", "event" => "add_to_group",
            "group_id" => "#{group.id}", "group_name" => "edcast-mv-eng",
            "group_role" => "admin", "name" => nil, "organization_hostname" => "org47",
            "organization_id" => "#{group.organization.id}", "organization_name" => "Organization 47",
            "user_id" => "#{users.first.id}", "user_name" => "admin1"
        },
        {
            "time" => "2017-10-13T20:10:56Z", "event" => "add_to_group",
            "group_id" => "#{group.id}", "group_name" => "edcast-mv-eng",
            "group_role" => "admin", "name" => nil, "organization_hostname" => "org47",
            "organization_id" => "#{group.organization.id}", "organization_name" => "Organization 47",
            "user_id" => "#{users.last.id}", "user_name" => "admin1"
        }]
    }]
  end
end

require 'test_helper'

class Api::V2::UserCustomFieldsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    @organization = create :organization
    @admin = create(:user, organization: @organization, organization_role: 'admin')
    @cf1 = create :custom_field, organization: @organization, display_name: 'A', abbreviation: 'a'
    @cf2 = create :custom_field, organization: @organization, display_name: 'B', abbreviation: 'b'
    @cf3 = create :custom_field, organization: @organization, display_name: 'C', abbreviation: 'c'

    api_v2_sign_in(@admin)
  end

  test ':api #index lists custom fields and values for users' do
    user1 = create :user, organization: @organization
    user2 = create :user, organization: @organization
    user3 = create :user, organization: @organization
    user4 = create :user, organization: @organization

    # user1 has no custom fields

    # user2 has only one custom field
    create :user_custom_field, custom_field: @cf1, user: user2

    # user3 has all of the custom fields
    create :user_custom_field, custom_field: @cf1, user: user3
    create :user_custom_field, custom_field: @cf2, user: user3
    create :user_custom_field, custom_field: @cf3, user: user3

    # user4 has all of the custom fields
    create :user_custom_field, custom_field: @cf2, user: user4
    create :user_custom_field, custom_field: @cf3, user: user4

    get :index, user_ids: [user1.id, user2.id, user3.id, user4.id], send_array: true, format: :json

    result = get_json_response(response)
    # debugger
    # `user1` response
    assert_empty result[0]['custom_fields']

    # `user2` response
    assert_not_empty result[1]['custom_fields']
  end

  test 'lists custom fields and values for users that should be displayed in users profile' do
    user = create :user, organization: @organization
    cf1 = create :custom_field, organization: @organization, display_name: 'V', abbreviation: 'v'
    cf1.set_value_for_config('show_in_profile', true)
    cf2 = create :custom_field, organization: @organization, display_name: 'W', abbreviation: 'w'
    create :user_custom_field, custom_field: cf1, user: user
    create :user_custom_field, custom_field: cf2, user: user

    get :index, user_ids: [user.id], show_in_profile: true, format: :json
    resp = get_json_response(response)

    assert_equal 1, resp[user.id.to_s].count
  end

  test 'lists editable custom fields and values for users that should be displayed in users profile' do
    user = create :user, organization: @organization
    cf1 = create :custom_field, organization: @organization, display_name: 'V', abbreviation: 'v'
    cf1.set_value_for_config('editable_by_users', true)
    cf2 = create :custom_field, organization: @organization, display_name: 'W', abbreviation: 'w'
    create :user_custom_field, custom_field: cf1, user: user
    create :user_custom_field, custom_field: cf2, user: user

    get :index, user_ids: [user.id], editable_by_users: true, format: :json
    resp = get_json_response(response)

    assert_equal 1, resp[user.id.to_s].count
  end


  test "should return all user's custom fields if user_ids are not present" do
    users = create_list(:user, 10, organization: @organization)
    users.each do |user|
      create :user_custom_field, custom_field: @cf1, user: user
    end

    get :index, send_array: true, format: :json
    result = get_json_response(response)
    assert_equal @organization.users.length, result.length
  end

  test "should return user's custom fields with limit and offset" do
    users = create_list(:user, 10, organization: @organization)
    users.each do |user|
      create :user_custom_field, custom_field: @cf1, user: user
    end

    get :user_custom_fields_data, offset: 1, limit: 5, format: :json
    result = get_json_response(response)
    assert_equal 5, result.length
  end

  test "should return user's custom fields with updated/created from last_fetched_at" do
    old_users = create_list(:user, 5, organization: @organization)
    old_users.each do |user|
      create :user_custom_field, custom_field: @cf1, user: user, created_at: 4.days.ago, updated_at: 4.days.ago
    end

    users = create_list(:user, 5, organization: @organization)
    users.each do |user|
      create :user_custom_field, custom_field: @cf1, user: user
    end

    get :user_custom_fields_data, limit: 10, last_fetched_at: 3.days.ago.to_date.to_s, format: :json
    result = get_json_response(response)
    assert_same_elements users.map(&:id), result.keys.map(&:to_i)
  end

  test "should not return user custom fields for inactive users" do
    users = create_list(:user, 5, organization: @organization)
    users.each do |user|
      create :user_custom_field, custom_field: @cf1, user: user
    end
    inactive_user = users.first
    inactive_user.update(status: User::INACTIVE_STATUS)

    get :user_custom_fields_data, limit: 10, format: :json
    result = get_json_response(response)

    assert result.keys.map(&:to_i).exclude?(inactive_user.id)
    assert_equal 4, result.length
  end

  test ':api update custom field value for a user' do
    custom_field = create :custom_field, organization: @organization
    post :create, custom_field_id: custom_field.id, value: "test"
    result = get_json_response(response)
    assert_response :ok
    assert_equal @admin.assigned_custom_fields.last.value, "test"
  end

  test 'should return error for bad request' do
    post :create, custom_field_id: 123
    result = get_json_response(response)
    assert_response :bad_request
  end

  test ':api should create multiple user_custom_field values' do
    user = create :user, organization: @organization
    custom_field = create :custom_field, organization: @organization

    # this should create user custom field
    post :update_all, user_custom_fields: {"user_id": user.id, "custom_fields": [ {"custom_field_id": custom_field.id, "value": "dummy-data-create"}] }, format: :json

    assert_response :ok
    resp = get_json_response(response)

    assert_equal UserCustomField.where(user_id: user.id).last.value, resp["user_custom_fields"].first["value"]
  end

  test 'should update multiple user_custom_field values' do
    user = create :user, organization: @organization
    custom_field = create :custom_field, organization: @organization

    # this should update user custom field
    post :update_all, user_custom_fields: {"user_id": user.id, "custom_fields": [ {"custom_field_id": custom_field.id, "value": "dummy-data-update"}] }, format: :json
    assert_response :ok
    assert_equal 'dummy-data-update', UserCustomField.where(user_id: user.id).last.value
  end

  test 'should create no difference in db if no user passed' do
    user = create :user, organization: @organization
    custom_field = create :custom_field, organization: @organization

    assert_no_difference -> {UserCustomField.count} do
      post :update_all, user_custom_fields: {"custom_fields": [ {"custom_field_id": custom_field.id, "value": "dummy-data-update"}] }, format: :json
    end

    assert_response :unprocessable_entity
  end

  test 'should return unauthorized if user is not org admin' do
    user = create :user, organization: @organization
    api_v2_sign_in(user)
    custom_field = create :custom_field, organization: @organization

    post :update_all, user_custom_fields: {"user_id": user.id, "custom_fields": [ {"custom_field_id": custom_field.id, "value": "dummy-data-update"}] }, format: :json

    assert_response :unauthorized
  end

  test 'should removed user_custom_field if value empty string' do
    user = create :user, organization: @organization
    create(:user_custom_field, user_id: user.id, custom_field_id: @cf1.id)
    create(:user_custom_field, user_id: user.id, custom_field_id: @cf2.id)
    user_custom_fields = {
      "user_id": user.id,
      "custom_fields": [
        {"custom_field_id": @cf1.id, "value": ''},
        {"custom_field_id": @cf2.id, "value": 'new'},
        {"custom_field_id": @cf3.id, "value": 'cf3'},
      ]
    }
    post :update_all, user_custom_fields: user_custom_fields, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal 2, resp['user_custom_fields'].length
    assert_equal @cf2.id, resp['user_custom_fields'].first['custom_field_id']
    assert_equal 'new', resp['user_custom_fields'].first['value']
    assert_equal @cf3.id, resp['user_custom_fields'].second['custom_field_id']
    assert_equal 'cf3', resp['user_custom_fields'].second['value']
  end

  test 'should removed user_custom_field if value blank' do
    user = create :user, organization: @organization
    create(:user_custom_field, user_id: user.id, custom_field_id: @cf1.id)
    create(:user_custom_field, user_id: user.id, custom_field_id: @cf2.id)
    user_custom_fields = {
      "user_id": user.id,
      "custom_fields": [
        {"custom_field_id": @cf1.id, "value": nil},
        {"custom_field_id": @cf2.id},
      ]
    }
    post :update_all, user_custom_fields: user_custom_fields, format: :json

    assert_response :ok
    resp = get_json_response(response)
    assert_equal 0, resp['user_custom_fields'].length
  end


  test ':api should delete user_custom_field' do
    user = create :user, organization: @organization
    user_custom_field = create(:user_custom_field, user_id: @admin.id, custom_field_id: @cf1.id)

    assert_difference ->{UserCustomField.count}, -1 do
      delete :destroy, id: user_custom_field.id, format: :json
    end
  end


  test 'should return 404 if invalid id' do
    assert_difference ->{UserCustomField.count}, 0 do
      delete :destroy, id: 12, format: :json
      assert_response :not_found
    end
  end
end

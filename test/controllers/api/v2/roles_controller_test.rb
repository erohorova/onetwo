require 'test_helper'

class Api::V2::RolesControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    User.any_instance.unstub :update_role
    Role.any_instance.unstub :create_role_permissions

    @org = create(:organization)
    Role.create_standard_roles(@org)

    @org_admin = create(:user, organization: @org, organization_role: 'admin')
    @collaborator_role = @org.roles.where(name: 'collaborator').first
    api_v2_sign_in @org_admin
  end

  test ':api create should create an organization role' do
    assert_difference -> { Role.count }, 1 do
      post :create, name: 'test_role', format: :json
      assert_response :ok
    end
    res = get_json_response(response)
    assert_equal res['name'], 'test_role'
  end

  test ':api create should create an organization role with permissions' do
    assert_difference -> { Role.count }, 1 do
      post :create, name: 'test_role', enable_permissions: ['ADMIN_ONLY'], format: :json
      assert_response :ok
    end
    res = get_json_response(response)
    rp = RolePermission.find_by(role_id: res['id'])
    assert_equal res['name'], 'test_role'
    assert_equal 'ADMIN_ONLY', rp.name
    assert rp.enabled
  end

  test ':api create should not create an organization role' do
    assert_no_difference -> { Role.count } do
      post :create, name: '', format: :json
      assert_response :unprocessable_entity
    end
  end

  test ':api update organization roles' do
    put :update, id: @collaborator_role.id, name: 'Org collaborator', threshold: 100, format: :json
    assert_response :ok
    assert_equal @collaborator_role.reload.name, 'Org collaborator'
    assert_equal @collaborator_role.threshold, 100
  end

  test ':api update name of role to reindex users and initiate the job' do
    TestAfterCommit.enabled = true
    EdcastActiveJob.unstub :perform_later
    BulkPartialReindexUserRolesJob.unstub :perform_later
    users = create_list(:user, 2, organization: @org)

    users[0].add_role(@collaborator_role)
    users[1].add_role(@collaborator_role)

    Role.any_instance.unstub :reindex_users
    BulkPartialReindexUserRolesJob.expects(:perform_later).with(
      org_id: @collaborator_role.organization_id,
      id: @collaborator_role.id
      )
    put :update, id: @collaborator_role.id, name: 'new name', format: :json

    assert_response :ok
    assert_equal @collaborator_role.reload.name, 'new name'
  end

  test ':api should delete role and dependent role permissions' do
    role = create(:role, name: 'test_role', organization: @org)
    create(:role_permission, role: role, name: Permissions::LIKE_CONTENT)

    assert_differences [[->{Role.count}, -1], [->{RolePermission.count}, -1]] do
      delete :destroy, id: role.id, format: :json
      assert_response :ok
    end
  end

  test ':api should not delete standard role' do
    role = @org.roles.find_by(name: 'admin')

    delete :destroy, id: role.id, format: :json
    assert_response :unprocessable_entity
  end

  test ':api show organization role details' do
    get :show, id: @collaborator_role.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_same_elements %w[id name threshold image_url default_name], resp.keys
  end

  test ':api get organization roles' do
    Role.create_master_roles @org
    # collaborator
    # curator
    # admin and member from users table
    roles = @org.roles
    get :index, format: :json

    resp = get_json_response(response)
    assert_response :ok

    assert_same_elements roles.map(&:name), (resp['roles'].map { |r| r['name'] })
    assert_equal 8, resp['roles'].size
  end

  test ':api get should be ordered by id in desc' do
    Role.create_master_roles @org
    # latest role created
    last_role = @org.roles.last
    get :index, format: :json
    resp = get_json_response(response)
    assert_response :ok

    assert_equal last_role.id, resp['roles'].first['id']
  end

  test 'show organization role details with pagination' do
    get :index, limit: 2, offset: 1, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal 2, resp['roles'].count
    resp['roles'].map{|d| d['name']}.each do |org_role|
      assert_includes @org.roles.pluck(:name), org_role
    end
  end

  test 'get organization roles with permissions' do
    roles = @org.roles
    first_permission = roles.select{|role| role.name == 'member'}.first.role_permissions.first
    first_permission.update enabled: false

    get :index, format: :json

    resp = get_json_response(response)
    assert_response :ok
    assert_same_elements roles.collect(&:id), resp['roles'].collect {|role| role['id']}
    resp_role = resp['roles'].select {|role| role['name'] == 'member'}.first
    assert_not_empty resp_role['permissions']

    disabled_permission = resp_role['permissions'].select {|rp| rp['name'] == first_permission.name}.first
    enabled_permission = resp_role['permissions'].select {|rp| rp['name'] != first_permission.name}

    refute disabled_permission['enabled']
    enabled_permission.each {|rp| assert rp['enabled']}
  end

  test 'only org admin can access org roles' do
    member = create(:user, organization: @org)
    api_v2_sign_in member

    get :index, format: :json
    assert_response :unauthorized
  end

  test 'role name is required to update permission' do
    put :role_permission, format: :json
    assert_response :unprocessable_entity
  end

  test 'disable permission for role' do
    member_role = @org.roles.where(name: 'member').first
    permissions = Permissions.get_permissions_for_role('member')
    permission = permissions.keys.first
    role_permission = member_role.role_permissions.where(name: permission).first

    assert role_permission.enabled

    put :role_permission, role_name: 'member', disable_permissions: [permission], format: :json
    assert_response :ok
    role_permission.reload

    refute role_permission.enabled
  end

  test 'enable permission for role' do
    member_role = @org.roles.where(name: 'member').first
    permissions = Permissions.get_permissions_for_role('member')
    permission = permissions.keys.first
    role_permission = member_role.role_permissions.where(name: permission).first
    role_permission.disable!

    refute role_permission.enabled

    put :role_permission, role_name: 'member', enable_permissions: [permission], format: :json
    assert_response :ok
    role_permission.reload

    assert role_permission.enabled
  end

  test 'enable permission for a role which does not exist in role_permissions' do
    Role.create_master_roles @org
    sme_role = @org.roles.where(name: 'sme').first

    put :role_permission, role_name: 'sme', enable_permissions: ['CURATE_CONTENT','CREATE_COMMENT'], format: :json
    assert_equal ["CURATE_CONTENT", "CREATE_COMMENT"], sme_role.role_permissions.pluck(:name)
    assert sme_role.role_permissions.where(name: 'CURATE_CONTENT').first.enabled
    assert sme_role.role_permissions.where(name: 'CREATE_COMMENT').first.enabled

    put :role_permission, role_name: 'sme', disable_permissions: ['CURATE_CONTENT'], format: :json
    refute sme_role.role_permissions.where(name: 'CURATE_CONTENT').first.enabled
  end

  test 'enable permission for role with id support' do
    member_role = @org.roles.where(name: 'member').first
    permissions = Permissions.get_permissions_for_role('member')
    permission = permissions.keys.first
    role_permission = member_role.role_permissions.where(name: permission).first
    role_permission.disable!

    refute role_permission.enabled

    put :role_permission, id: member_role.id, enable_permissions: [permission], format: :json
    assert_response :ok
    role_permission.reload

    assert role_permission.enabled
  end

  test 'enable/disable permissions for role' do
    member_role = @org.roles.where(name: 'member').first
    permissions = Permissions.get_permissions_for_role('member')

    first_permission = member_role.role_permissions.where(name: permissions.keys[0]).first
    second_permission = member_role.role_permissions.where(name: permissions.keys[1]).first

    assert first_permission.enabled
    assert second_permission.enabled

    second_permission.disable!
    refute second_permission.enabled

    put :role_permission, role_name: 'member', disable_permissions: [permissions.keys[0]], enable_permissions: [permissions.keys[1]], format: :json

    assert_response :ok
    first_permission.reload
    second_permission.reload

    refute first_permission.enabled
    assert second_permission.enabled
  end
end

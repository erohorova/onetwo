require 'test_helper'
def add_members_to_team(team, users)
  users.each do |user|
    team.add_member(user)
  end
end


class Api::V2::AssignmentsControllerTest < ActionController::TestCase


  class AssignmentsCreateTest < ActionController::TestCase
    test ':api should create team assignments for cards' do
      org = create(:organization)
      admin = create(:user, organization: org, organization_role: 'admin')
      card = create(:card, author_id: admin.id, card_type: 'media', card_subtype: 'link')
      team1, team2 = create_list(:team, 2, organization: org)

      users1 = create_list(:user, 2, organization: org)
      users2 = create_list(:user, 2, organization: org)

      team1.add_admin(admin)
      team2.add_admin(admin)

      add_members_to_team(team1, users1)
      add_members_to_team(team2, users2)

      api_v2_sign_in(admin)

      TeamAssignmentJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later

      additional_data = {
        user_id: admin.id,
        platform: 'web'
      }
      RequestStore.stubs(:read).returns(additional_data)

      TeamAssignmentJob.expects(:perform_later).with({team_id: team1.id,
        assignor_id: admin.id,
        assignee_ids: [],
        assignable_id: card.id,
        assignable_type: 'Card',
        opts: {self_assign: false}}
      ).once.returns(nil)

      TeamAssignmentJob.expects(:perform_later).with({team_id: team2.id,
        assignor_id: admin.id,
        assignee_ids: [],
        assignable_id: card.id,
        assignable_type: 'Card',
        opts: {self_assign: false}}
      ).once.returns(nil)

      post :create, assignment: {team_ids: [team1.id, team2.id], content_type: 'Card', content_id: card.id, assignment_type: 'group'}, format: :json

      assert_response :ok
    end

    test 'admin should be able to assign to teams the admin is not a part of' do
      org = create(:organization)
      admin = create(:user, organization: org, organization_role: 'admin')
      card = create(:card, author_id: admin.id, card_type: 'media', card_subtype: 'link')
      team = create(:team, organization_id: org.id)

      user = create(:user, organization: org)
      team.add_member user

      api_v2_sign_in(admin)

      TeamAssignmentJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later

      TeamAssignmentJob.expects(:perform_later).with({team_id: team.id,
        assignor_id: admin.id,
        assignee_ids: [],
        assignable_id: card.id,
        assignable_type: 'Card',
        opts: {self_assign: false}}
      ).once.returns(nil)

      post :create, assignment: {team_ids: [team.id], content_type: 'Card', content_id: card.id, assignment_type: 'group'}, format: :json
      assert_response :ok
    end

    test 'should not send notifications or emails if user assigns content to himself' do
      org = create(:organization)
      notifier =  EDCAST_NOTIFY.notification_name_to_notifiers[Notify::NOTE_ASSIGNED_CONTENT]
      notifier.set_enabled(:email, :single)
      user = create(:user, organization: org)
      user1 = create(:user, organization: org)
      card = create(:card, author_id: user1.id)
      api_v2_sign_in(user)

      EDCAST_NOTIFY.expects(:trigger_event).never
      IndividualAssignmentJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later

      post :create, assignment: {content_type: 'Card', content_id: card.id, assignment_type: 'individual', assignee_ids: [user.id], self_assign: true}, format: :json
      assert_response :ok
      assert_equal user.id, Assignment.last.team_assignments.first.assignor_id
    end

    test 'should call individual assignments job if excluded team user ids given' do
      org = create(:organization)
      admin = create(:user, organization: org, organization_role: 'admin')
      card = create(:card, author_id: admin.id, card_type: 'media', card_subtype: 'link')
      team = create(:team, organization: org)
      users = create_list(:user, 2, organization: org)
      team.add_admin(admin)
      add_members_to_team(team, users)

      api_v2_sign_in(admin)

      IndividualAssignmentJob.expects(:perform_later).once

      post :create, assignment: {
        team_ids: [team.id],
        content_type: 'Card',
        content_id: card.id,
        assignment_type: 'group',
        exclude_user_ids: [users.last.id]
        },
        format: :json

      assert_response :ok
    end

    test 'should not call individual assignments job if excluded team user ids not given' do
      org = create(:organization)
      admin = create(:user, organization: org, organization_role: 'admin')
      card = create(:card, author_id: admin.id, card_type: 'media', card_subtype: 'link')
      team = create(:team, organization: org)
      users = create_list(:user, 2, organization: org)
      team.add_admin(admin)
      add_members_to_team(team, users)

      api_v2_sign_in(admin)

      IndividualAssignmentJob.expects(:perform_later).never

      post :create, assignment: {
        team_ids: [team.id],
        content_type: 'Card',
        content_id: card.id,
        assignment_type: 'group'
        },
      format: :json

      assert_response :ok
    end

    test 'api should create team assignments for cards with self assign params' do
      org = create(:organization)
      admin = create(:user, organization: org, organization_role: 'admin')
      card = create(:card, author_id: admin.id, card_type: 'media', card_subtype: 'link')
      team = create(:team, organization: org)

      users = create_list(:user, 2, organization: org)

      team.add_admin(admin)

      add_members_to_team(team, users)

      api_v2_sign_in(admin)

      TeamAssignmentJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later

      additional_data = {
        user_id: admin.id,
        platform: 'web'
      }
      RequestStore.stubs(:read).returns(additional_data)

      TeamAssignmentJob.expects(:perform_later).with({team_id: team.id,
        assignor_id: admin.id,
        assignee_ids: [],
        assignable_id: card.id,
        assignable_type: 'Card',
        opts: {self_assign: true}}
      ).once.returns(nil)

      post :create, assignment: {team_ids: [team.id], content_type: 'Card', content_id: card.id, assignment_type: 'group', self_assign: true}, format: :json

      assert_response :ok
    end

    test 'api should pass message if message exists when assigning content' do
      org = create(:organization)
      admin = create(:user, organization: org, organization_role: 'admin')
      card = create(:card, author_id: admin.id, card_type: 'media', card_subtype: 'link')

      team1 = create(:team, organization: org)

      users1 = create_list(:user, 2, organization: org)

      team1.add_admin(admin)

      add_members_to_team(team1, users1)

      api_v2_sign_in(admin)

      TeamAssignmentJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later

      additional_data = {
        user_id: admin.id,
        platform: 'web'
      }
      RequestStore.stubs(:read).returns(additional_data)

      TeamAssignmentJob.expects(:perform_later).with(
        {
          team_id: team1.id,
          assignor_id: admin.id,
          assignee_ids: [],
          assignable_id: card.id,
          assignable_type: 'Card',
          opts: {
            self_assign: false,
            message: "this is a test message"
          }
        }).once.returns(nil)

      post :create, assignment: {team_ids: [team1.id], content_type: 'Card', content_id: card.id, assignment_type: 'group', message: "this is a test message"}, format: :json

      assert_response :ok
    end

    test 'api should create individual card assignments' do
      @org = create(:organization)
      admin = create(:user, organization: @org, organization_role: 'admin')
      card = create(:card, author_id: admin.id, card_type: 'media', card_subtype: 'link', organization_id: @org.id)
      users = create_list(:user, 2, organization: @org)

      api_v2_sign_in(admin)

      EDCAST_NOTIFY.notification_name_to_notifiers[Notify::NOTE_ASSIGNED_CONTENT].set_enabled(:email, :single)
      Card.any_instance.stubs(:add_to_learning_queue)
      SendSingleNotificationJob.expects(:perform_later).never
      TestAfterCommit.enabled = true
      IndividualAssignmentJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later

      assert_differences [[ -> {Assignment.count}, 2], [ -> {TeamAssignment.count}, 2]] do
        post :create, assignment: {assignee_ids: users.map(&:id), content_type: 'Card', content_id: card.id, assignment_type: 'individual'}, format: :json
      end
      assert_response :ok
    end

    # This test should be part of EDCAST_NOTIFY
    # test 'should not send card assignement notifications if disabled' do
    #   org = create(:organization)
    #   admin = create(:user, organization: org, organization_role: 'admin')
    #   card = create(:card, author_id: admin.id, card_type: 'media', card_subtype: 'link')

    #   users = create_list(:user, 2, organization: org)

    #   api_v2_sign_in(admin)

    #   notifier = EDCAST_NOTIFY.notification_name_to_notifiers[Notify::NOTE_ASSIGNED_CONTENT]
    #   notifier.set_disabled(:email, :single)

    #   EDCAST_NOTIFY.expects(:send_email).never

    #   assert_differences [[ -> {Assignment.count}, 2], [ -> {TeamAssignment.count}, 2]] do
    #     post :create, assignment: {assignee_ids: users.map(&:id), content_type: 'Card', content_id: card.id, assignment_type: 'individual'}, format: :json
    #   end

    #   assert_response :ok
    # end

    test 'api should get an error when required params are missing' do
      org = create(:organization)
      admin = create(:user, organization: org, organization_role: 'admin')
      card = create(:card, author_id: admin.id, card_type: 'media', card_subtype: 'link')

      users = create_list(:user, 2, organization: org)

      api_v2_sign_in(admin)

      assert_no_difference -> {Assignment.count} do
        post :create, assignment: {assignee_ids: users.map(&:id), content_type: 'Card', content_id: card.id}, format: :json
      end

      assert_response :unprocessable_entity
    end

    test 'should not create individual assignments for suspended users' do
      org = create(:organization, host_name: 'spark2')
      admin = create(:user, organization: org, organization_role: 'admin')
      card = create(:card, author_id: admin.id, card_type: 'media', card_subtype: 'link')

      users = create_list(:user, 3, organization: org)
      suspended_user = users.last
      suspended_user.update(is_suspended: true)

      api_v2_sign_in(admin)

      IndividualAssignmentJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later

      assert_differences [[ -> {Assignment.count}, 2], [ -> {TeamAssignment.count}, 2]] do
        post :create, assignment: {assignee_ids: users.map(&:id), content_type: 'Card', content_id: card.id, assignment_type: 'individual'}, format: :json
      end
      assert_response :ok

      assigned_users_ids = Assignment.last(2).map(&:user_id)
      assert_equal assigned_users_ids, users.first(2).map(&:id)
      assert_not_includes assigned_users_ids, suspended_user.id
    end

    test 'user with permission can assign content #admin' do
      Role.any_instance.unstub :create_role_permissions
      User.any_instance.unstub :update_role

      org = create(:organization, enable_role_based_authorization: true)
      Role.create_master_roles org

      admin_role = org.roles.where(name: Role::TYPE_ADMIN).first
      admin = create(:user, organization: org)

      # admin.roles << admin_role

      card = create(:card, author_id: admin.id, card_type: 'media', card_subtype: 'link')
      team1 = create(:team, organization: org)

      team1.add_admin(admin)
      api_v2_sign_in(admin)

      TeamAssignmentJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later

      additional_data = {
        user_id: admin.id,
        platform: 'web'
      }
      RequestStore.stubs(:read).returns(additional_data)

      TeamAssignmentJob.expects(:perform_later).with({
        team_id: team1.id,
        assignor_id: admin.id,
        assignee_ids: [],
        assignable_id: card.id,
        assignable_type: 'Card',
        opts: {self_assign: false}}
      ).once.returns(nil)

      post :create, assignment: {team_ids: [team1.id], content_type: 'Card', content_id: card.id, assignment_type: 'group'}, format: :json

      assert_response :ok
    end

    test 'user without permission can assign content to self' do
      Organization.any_instance.unstub(:create_or_update_roles)
      org = create(:organization, enable_role_based_authorization: true)
      org.run_callbacks(:commit)

      user = create(:user, organization: org)

      card = create(:card, author_id: user.id, card_type: 'media', card_subtype: 'link')

      api_v2_sign_in(user)

      post :create, assignment: {assignee_ids: [user.id], content_type: 'Card', content_id: card.id, assignment_type: 'individual', self_assign: true}, format: :json

      assert_response :ok
    end

    test 'user with permission can assign content #curator' do
      Organization.any_instance.unstub(:create_or_update_roles)
      org = create(:organization, enable_role_based_authorization: true)
      org.run_callbacks(:commit)

      admin_role = org.roles.where(name: Role::TYPE_ADMIN).first
      curator_role = org.roles.where(name: Role::TYPE_CURATOR).first

      curator = create(:user, organization: org)
      curator.role_ids =  [ curator_role.id ]
      curator.save

      card = create(:card, author_id: curator.id, card_type: 'media', card_subtype: 'link')
      team1 = create(:team, organization: org)

      users1 = create_list(:user, 2, organization: org)
      add_members_to_team(team1, users1)

      api_v2_sign_in(curator)

      post :create, assignment: {team_ids: [team1.id], content_type: 'Card', content_id: card.id, assignment_type: 'group'}, format: :json

      assert_response :unauthorized
    end

    test 'org admin should be able to assign content to any org group #org admin' do
      org = create(:organization)
      admin = create(:user, organization: org, organization_role: 'admin')
      card = create(:card, author_id: admin.id, card_type: 'media', card_subtype: 'link')
      team1, team2 = create_list(:team, 2, organization: org)

      users1 = create_list(:user, 2, organization: org)
      users2 = create_list(:user, 2, organization: org)

      add_members_to_team(team1, users1)
      add_members_to_team(team2, users2)

      api_v2_sign_in(admin)

      TeamAssignmentJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later

      additional_data = {
        user_id: admin.id,
        platform: 'web'
      }
      RequestStore.stubs(:read).returns(additional_data)

      TeamAssignmentJob.expects(:perform_later).with({
        team_id: team1.id,
        assignor_id: admin.id,
        assignee_ids: [],
        assignable_id: card.id,
        assignable_type: 'Card',
        opts: {self_assign: false}}
      ).once.returns(nil)

      TeamAssignmentJob.expects(:perform_later).with({
        team_id: team2.id,
        assignor_id: admin.id,
        assignee_ids: [],
        assignable_id: card.id,
        assignable_type: 'Card',
        opts: {self_assign: false}}
      ).once.returns(nil)

      post :create, assignment: {team_ids: [team1.id, team2.id], content_type: 'Card', content_id: card.id, assignment_type: 'group'}, format: :json

      assert_response :ok
    end

    test 'Should exclude user in group assignment' do
      org = create(:organization)
      admin = create(:user, organization: org, organization_role: 'admin')
      card = create(:card, author_id: admin.id, card_type: 'media', card_subtype: 'link')
      team = create(:team, organization: org)
      users = create_list(:user, 2, organization: org)
      team.add_admin(admin)
      add_members_to_team(team, users)

      api_v2_sign_in(admin)

      IndividualAssignmentJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later

      assert_differences [[ -> {Assignment.count}, 2], [ -> {TeamAssignment.count}, 2]] do
        post :create, assignment: {
          team_ids: [team.id],
          content_type: 'Card',
          assignment_type: 'Individual',
          content_id: card.id,
          exclude_user_ids: [users.last.id]
        }, format: :json
      end

      puts get_json_response(response)

      assert_response :ok
      # These are a few queries, making sure right type of assignments
      # are being created
      # excluded user
      refute Assignment.exists?(assignable_id: card.id, user_id: users.last.id)
      # included user
      assert Assignment.exists?(assignable_id: card.id, user_id: users.first.id)
      # No team id for remaining user assignments
      assert_nil TeamAssignment.where(assignment: Assignment.last).last.team_id
    end

    test ':api should create team assignments for individual, even if the user already dismissed assignment' do
      org = create(:organization)
      admin = create(:user, organization: org, organization_role: 'admin')
      card = create(:card, author_id: admin.id, card_type: 'media', card_subtype: 'link')
      team = create(:team, organization: org)
      users = create_list(:user,2, organization: org)
      team.add_admin(admin)
      add_members_to_team(team, users)
      assignment = create(:assignment, assignable: card, assignee: users[0], state: "dismissed")

      api_v2_sign_in(admin)

      TeamAssignmentJob.unstub :perform_later
      TeamBatchAssignmentJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later
      additional_data = {
        user_id: admin.id,
        platform: 'web'
      }
      RequestStore.stubs(:read).returns(additional_data)


      post :create, assignment: {team_ids: [team.id], content_type: 'Card', content_id: card.id, assignment_type: 'group'}, format: :json
      assignment.reload

      assert_response :ok
      assert_equal 'assigned', assignment.state
    end

    test 'api should reassign assignment if dismissed' do
      org = create(:organization)
      admin = create(:user, organization: org, organization_role: 'admin')
      card = create(:card, author_id: admin.id, card_type: 'media', card_subtype: 'link', organization_id: org.id)
      user = create(:user, organization: org)
      assignment = create(:assignment, assignable: card, assignee: user, state: "dismissed")

      api_v2_sign_in(admin)

      EDCAST_NOTIFY.notification_name_to_notifiers[Notify::NOTE_ASSIGNED_CONTENT].set_enabled(:email, :single)
      Card.any_instance.stubs(:add_to_learning_queue)
      SendSingleNotificationJob.expects(:perform_later).never
      TestAfterCommit.enabled = true
      IndividualAssignmentJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later

      post :create, assignment: {assignee_ids: [user.id], content_type: 'Card', content_id: card.id, assignment_type: 'individual'}, format: :json
      assignment.reload
      assert_response :ok
      assert_equal 'assigned', assignment.state
    end

  end

  class AssignmentsDestroyTest < ActionController::TestCase
    test 'should trigger NotificationCleanUpJob on assignment destroy' do

      org = create(:organization)
      team = create(:team, organization: org)
      causal_user, assignee = create_list(:user, 2, organization: org)
      card = create(:card, author: causal_user, organization: org)
      assignment = create(:assignment, assignable: card, assignee: assignee)
      team_assignment = create(:team_assignment, team_id: team, assignor: causal_user)

      NotificationCleanUpJob.expects(:perform_later).with({id: assignment.id, type: assignment.class.name}).once

      assignment.destroy
    end
  end

  class AssignmentsIndexTest < ActionController::TestCase
    setup do
      TestAfterCommit.enabled = false
      @org = create(:organization, host_name: 'spark')
      @first_org_admin, @another_org_admin = create_list(:user, 2, organization: @org, organization_role: 'admin')

      # team assignments
      @first_team, @second_team  = create_list(:team, 2, organization: @org)
      @first_assignee, @another_assignee = create_list(:user, 2, organization: @org)
      @first_assignor, @another_assignor = create_list(:user, 2, organization: @org)

      [@first_team, @second_team].each do |team|
        team.add_admin @first_assignor
        team.add_admin @another_assignor
      end
    end

    test ':api should return user assignments include search query in card title' do
      card1 = create(:card, author: @first_org_admin, title: 'abcdefg')
      card2 = create(:card, author: @first_org_admin, title: 'xyzgfhg')
      create(:assignment, assignable: card1, assignee: @first_assignee)
      create(:assignment, assignable: card2, assignee: @first_assignee)

      Search::CardSearch
        .any_instance
        .expects(:search)
        .returns(Search::CardSearchResponse.new(results: [card1]), total: 1)

      api_v2_sign_in(@first_assignee)

      get :index, mlp_period: 'other', query: 'abcde', format: :json
      resp = get_json_response(response)['assignments']
      assert_equal 1, resp.count
      assert_equal card1.title, resp[0]['assignable']['title']
    end

    test ':api should return user assignments include search query in card message' do
      card1 = create(:card, author: @first_org_admin, message: 'abcdefg')
      card2 = create(:card, author: @first_org_admin, message: 'xyzgfhg')
      create(:assignment, assignable: card1, assignee: @first_assignee)
      create(:assignment, assignable: card2, assignee: @first_assignee)

      Search::CardSearch
        .any_instance
        .expects(:search)
        .returns(Search::CardSearchResponse.new(results: [card1]), total: 1)

      api_v2_sign_in(@first_assignee)

      get :index, mlp_period: 'other', query: 'abcde', format: :json
      resp = get_json_response(response)['assignments']
      assert_equal 1, resp.count
      assert_equal card1.message, resp[0]['assignable']['message']
    end

    # index starts
    test ':api should return user assignments' do
      cards = create_list(:card, 2, author: @first_org_admin)
      assignments = []
      cards.each do |card|
        assignment = create(:assignment, assignable: card, assignee: @first_assignee)
        create(:team_assignment, team: @first_team, assignment: assignment, assignor: @first_assignor)
        assignments << assignment
      end

      api_v2_sign_in(@first_assignee)

      get :index, mlp_period: 'other', limit: 50, format: :json

      team_assignment = TeamAssignment.where(assignment: @first_assignee.assignments).where.not(team_id: nil).first

      resp = get_json_response(response)
      team_assignment_resp =  resp['assignments'].select {|a| a['id'] == team_assignment.assignment.id}.first

      assert_equal 2, resp['assignments'].size
      assert_same_elements cards.map(&:id), resp['assignments'].map {|a| a['assignable']['id'].to_i}
      assert_not_empty team_assignment_resp['assignor']
    end

    test ':api should return user assignments of particular assignments when user_id is passed' do
      cards = create_list(:card, 2, author: @first_org_admin)
      user1, user2 = create_list(:user,2, organization: @org)
      assignment1 = create(:assignment, assignable: cards[0], user_id: user1.id, assignee: @first_assignee )
      assignment2 = create(:assignment, assignable: cards[1], user_id: user2.id, assignee: @first_assignee  )
      api_v2_sign_in(@first_assignee)
      get :index, user_id: user2.id, limit: 50, format: :json
      resp = get_json_response(response)
      assert_equal 1, resp['assignments'].size
      assert_equal [assignment2.id], resp['assignments'].map {|a| a['id'].to_i}
    end

    test ':api should not return private assignments of particular user when user_id is passed' do
      card1 = create(:card, author: @first_org_admin, is_public: false)
      card2 = create(:card, author: @first_org_admin)
      user1, user2 = create_list(:user,2, organization: @org)
      assignment1 = create(:assignment, assignable: card1, user_id: user2.id, assignee: @first_assignee)
      assignment2 = create(:assignment, assignable: card2, user_id: user2.id, assignee: @first_assignee)
      api_v2_sign_in(@first_assignee)
      get :index, user_id: user2.id, limit: 50, format: :json
      resp = get_json_response(response)
      assert_equal 1, resp['assignments'].size
      assert_equal [assignment2.id], resp['assignments'].map {|a| a['id'].to_i}
    end

    test ':api should return private assignments for the user when user_id is passed and user has access to that private assignment' do
      user1, user2 = create_list(:user,2, organization: @org)
      card1 = create(:card, author: user1, is_public: false)
      card2 = create(:card, author: user1, is_public: false)
      assignment1 = create(:assignment, assignable: card1, user_id: user1.id, assignee: @first_assignee)
      assignment2 = create(:assignment, assignable: card2, user_id: user1.id, assignee: @first_assignee)
      create(:card_user_permission, card_id: card1.id, user_id: user2.id)

      api_v2_sign_in(user2)
      get :index, user_id: user1.id, limit: 50, format: :json
      resp = get_json_response(response)
      assert_equal 1, resp['assignments'].size
      assert_equal [assignment1.id], resp['assignments'].map {|a| a['id'].to_i}
    end

    test ':api should return user assignments filtered by language' do
      cards = create_list(:card, 2, author: @first_org_admin, language: 'es')
      ru_card = create(:card, author: @first_org_admin, language: 'ru')
      not_specified_card = create(:card, author: @first_org_admin)
      cards << ru_card
      cards << not_specified_card
      assignments = []
      cards.each do |card|
        assignment = create(:assignment, assignable: card, assignee: @first_assignee)
        assignments << assignment
      end
      @first_assignee.create_profile(language: 'ru')
      api_v2_sign_in(@first_assignee)

      get :index, filter_by_language: true, limit: 50, format: :json

      resp = get_json_response(response)
      assert_equal 2, resp['assignments'].length
      assert_same_elements [ru_card.id, not_specified_card.id], resp['assignments'].map {|a| a['assignable']['id'].to_i}
    end

    test "should not return deleted card's assignments" do
      cards = create_list(:card, 2, author: @first_org_admin)
      assignments = []
      cards.each do |card|
        assignment = create(:assignment, assignable: card, assignee: @first_assignee)
        create(:team_assignment, team: @first_team, assignment: assignment, assignor: @first_assignor)
        assignments << assignment
      end

      to_be_deleted = cards.first

      to_be_deleted.destroy

      api_v2_sign_in(@first_assignee)

      get :index, limit: 50, format: :json
      resp = get_json_response(response)
      resp_assignable_ids = resp['assignments'].map {|a| a['assignable']['id']}
      assert_equal 1, resp['assignments'].size
      refute_includes resp_assignable_ids, to_be_deleted.id
    end

    test 'returns associated resource_group details' do
      skip # skiping these because it is use in mobile as we modifiy assignments response.
      ## ECL-card assignments associated to a resource-group ##
      # 1. Create resource groups and associate couple of ECL cards
      # 2. ECL cards must have ecl_metadata[`external_id`] = resource.client_resource_id
      @resource_group = create :resource_group, organization: @org, client_resource_id: '123'
      @other_resource_group = create :resource_group, organization: @org, client_resource_id: '456'
      @ecl_data = {
        "source_id" => "a36643fb-ed6c-443b-b473-5362c1cacf18",
        "source_display_name" => "Marketing Land - Internet Marketing News, Strategies & Tips",
        "source_type_name" => "edcast_cloud",
        "duration_metadata" => nil
      }
      @ecl_card = create :card, organization: @org, ecl_id: SecureRandom.hex, ecl_metadata: @ecl_data.merge('external_id' => @resource_group.client_resource_id.to_s), author_id: nil, is_public: true, is_manual: false
      @other_ecl_card = create :card, organization: @org, ecl_id: SecureRandom.hex, ecl_metadata: @ecl_data.merge('external_id' => @other_resource_group.client_resource_id.to_s), author_id: nil, is_public: true, is_manual: false
      # 3. Make @first_assignee to be a valid user in one of the groups
      @resource_group.add_member(@first_assignee)
      @resource_group.reload
      # 4. Assign both ECL cards to @first_assignee
      [@ecl_card, @other_ecl_card].each do |card|
        assignment = create(:assignment, assignable: card, assignee: @first_assignee)
      end

      api_v2_sign_in(@first_assignee)

      get :index, limit: 50, format: :json

      ecl_card_assignment = Assignment.find_by(assignable: @ecl_card, user_id: @first_assignee.id)
      other_ecl_card_assignment = Assignment.find_by(assignable: @other_ecl_card, user_id: @first_assignee.id)

      resp = get_json_response(response)

      # Assignment in which user is enrolled
      assignment =  resp['assignments'].select {|a| a['id'] == ecl_card_assignment.id}.first

      assert_equal 'edcast_cloud', assignment['assignable']['ecl_source_type_name']
      assert_equal @resource_group.client_resource_id.to_i, assignment['assignable']['resource_group']['course_id']
      assert_equal @resource_group.id, assignment['assignable']['resource_group']['id']
      assert assignment['assignable']['resource_group']['is_user_enrolled']

      # Assignment in which user is not enrolled
      other_assignment =  resp['assignments'].select {|a| a['id'] == other_ecl_card_assignment.id}.first

      assert_equal 'edcast_cloud', other_assignment['assignable']['ecl_source_type_name']
      assert_equal @other_resource_group.client_resource_id.to_i, other_assignment['assignable']['resource_group']['course_id']
      assert_equal @other_resource_group.id, other_assignment['assignable']['resource_group']['id']
      refute other_assignment['assignable']['resource_group']['is_user_enrolled']
    end

    test 'api should return user assignments sorted by due_date' do
      user = create(:user, organization: @org)
      cards = create_list(:card, 7, author: @first_org_admin)

      #create assignments
      # past assignments
      assign1 = create(:assignment, assignable: cards[0], assignee: user, due_at: Time.now - 3.day)
      assign2 = create(:assignment, assignable: cards[1], assignee: user, due_at: Time.now - 2.day)
      assign3 = create(:assignment, assignable: cards[2], assignee: user, due_at: Time.now - 1.day)

      # assignments with no due date
      assign4 = create(:assignment, assignable: cards[3], assignee: user, created_at: Time.now - 1.day)
      assign5 = create(:assignment, assignable: cards[4], assignee: user, created_at: Time.now - 2.day)

      # upcoming assignments
      assign6 = create(:assignment, assignable: cards[5], assignee: user, due_at: Time.now + 3.day)
      assign7 = create(:assignment, assignable: cards[6], assignee: user, due_at: Time.now + 1.day)

      api_v2_sign_in(user)
      get :index, state: ['started', 'assigned'], limit: 10, format: :json

      resp = get_json_response(response)
      assert_equal 7, resp['assignments'].size
      assert_equal [assign7.id, assign6.id, assign3.id, assign2.id, assign1.id, assign4.id, assign5.id].sort, resp['assignments'].map {|a| a['id']}.sort
    end

    test 'api should return user assignments sorted by -due_date in asc/desc order with null due_at records in last' do
      user = create(:user, organization: @org)
      cards = create_list(:card, 7, author: @first_org_admin)

      #create assignments
      # past assignments
      assign1 = create(:assignment, assignable: cards[0], assignee: user, due_at: Time.now - 3.day)
      assign2 = create(:assignment, assignable: cards[1], assignee: user, due_at: Time.now - 2.day)
      assign3 = create(:assignment, assignable: cards[2], assignee: user, due_at: Time.now - 1.day)

      # assignments with no due date
      assign4 = create(:assignment, assignable: cards[3], assignee: user, created_at: Time.now - 1.day)
      assign5 = create(:assignment, assignable: cards[4], assignee: user, created_at: Time.now - 2.day)

      # upcoming assignments
      assign6 = create(:assignment, assignable: cards[5], assignee: user, due_at: Time.now + 3.day)
      assign7 = create(:assignment, assignable: cards[6], assignee: user, due_at: Time.now + 1.day)

      api_v2_sign_in(user)
      get :index, state: ['started', 'assigned'], sort: "-due_at", order: "desc" ,limit: 10, format: :json

      resp = get_json_response(response)
      assert_equal 7, resp['assignments'].size
      assert_equal [assign1.id, assign2.id, assign3.id, assign7.id, assign6.id, assign4.id, assign5.id].sort, resp['assignments'].map {|a| a['id']}.sort

      get :index, state: ['started', 'assigned'], sort: "-due_at", order: "asc" ,limit: 10, format: :json

      resp = get_json_response(response)
      assert_equal [assign4.id, assign5.id, assign6.id, assign7.id, assign3.id, assign2.id, assign1.id].sort, resp['assignments'].map {|a| a['id']}.sort
    end

    test 'api should return user assignments for mlp_periods quarter 1' do
      user = create(:user, organization: @org)
      api_v2_sign_in(user)
      card = create(:card, author: @first_org_admin)

      assign1 = create(:assignment, assignable: card, assignee: user, due_at: Date.new(Date.current.year,01,01))

      get :index, mlp_period: 'quarter-1', limit: 10, format: :json
      resp = get_json_response(response)
      assert_equal assign1.id, resp['assignments'].first['id']
    end

    test 'api should return user assignments for mlp_periods quarter 1,2,3,4 of particular year if year_filter is passed' do
      user = create(:user, organization: @org)
      api_v2_sign_in(user)
      card1,card2,card3,card4 = create_list(:card, 4, author: @first_org_admin)

      assign1 = create(:assignment, assignable: card1, assignee: user, due_at: "2015-01-01 13:50:51")
      assign2 = create(:assignment, assignable: card2, assignee: user, due_at: "2015-04-01 13:50:51")
      assign3 = create(:assignment, assignable: card3, assignee: user, due_at: "2015-07-01 13:50:51")
      assign4 = create(:assignment, assignable: card4, assignee: user, due_at: "2015-10-01 13:50:51")
      get :index, mlp_period: 'quarter-1', year_filter: 2015, limit: 10, format: :json
      resp = get_json_response(response)
      assert_equal assign1.id, resp['assignments'].first['id']
      assert_equal 1, resp['assignments'].count
      get :index, mlp_period: 'quarter-2', year_filter: 2015, limit: 10, format: :json
      resp = get_json_response(response)
      assert_equal assign2.id, resp['assignments'].first['id']
      assert_equal 1, resp['assignments'].count
      get :index, mlp_period: 'quarter-3', year_filter: 2015, limit: 10, format: :json
      resp = get_json_response(response)
      assert_equal assign3.id, resp['assignments'].first['id']
      assert_equal 1, resp['assignments'].count
      get :index, mlp_period: 'quarter-4', year_filter: 2015, limit: 10, format: :json
      resp = get_json_response(response)
      assert_equal assign4.id, resp['assignments'].first['id']
      assert_equal 1, resp['assignments'].count
    end

    test 'api should return user assignments for mlp_periods quarter 2' do
      user = create(:user, organization: @org)
      api_v2_sign_in(user)
      card = create(:card, author: @first_org_admin)

      assign2 = create(:assignment, assignable: card, assignee: user, due_at: Date.new(Date.current.year,04,01))

      get :index, mlp_period: 'quarter-2', limit: 10, format: :json
      resp = get_json_response(response)
      assert_equal assign2.id, resp['assignments'].first['id']
    end

    test 'api should return user assignments for mlp_periods quarter 3' do
      user = create(:user, organization: @org)
      api_v2_sign_in(user)
      card = create(:card, author: @first_org_admin)

      assign3 = create(:assignment, assignable: card, assignee: user, due_at: Date.new(Date.current.year,07,01))

      get :index, mlp_period: 'quarter-3', limit: 10, format: :json
      resp = get_json_response(response)
      assert_equal assign3.id, resp['assignments'].first['id']
    end


    test 'api should return user assignments for mlp_periods quarter 4' do
      user = create(:user, organization: @org)
      api_v2_sign_in(user)
      card = create(:card, author: @first_org_admin)

      assign4 = create(:assignment, assignable: card, assignee: user, due_at: Date.new(Date.current.year,10,01))

      get :index, mlp_period: 'quarter-4', limit: 10, format: :json
      resp = get_json_response(response)
      assert_equal assign4.id, resp['assignments'].first['id']
    end

    test 'api should return user assignments for mlp_periods untimed' do
      user = create(:user, organization: @org)
      api_v2_sign_in(user)
      card = create(:card, author: @first_org_admin)

      assign5 = create(:assignment, assignable: card, assignee: user, created_at: Time.now - 2.day)

      get :index, mlp_period: 'untimed', limit: 10, format: :json
      resp = get_json_response(response)
      assert_equal assign5.id, resp['assignments'].first['id']
    end

    test 'api should return is_new key for user assignments assigned after specific date' do
      user = create(:user, organization: @org)
      api_v2_sign_in(user)
      card = create(:card, author: @first_org_admin)
      card2 = create(:card, author: @first_org_admin)

      assign5 = create(:assignment, assignable: card, assignee: user, created_at: Time.now - 6.days)
      assign6 = create(:assignment, assignable: card2, assignee: user, created_at: Time.now - 3.days)

      get :index, last_access_at: (Time.now - 4.days).to_i, format: :json
      resp = get_json_response(response)
      assert_includes  resp['assignments'].first['assignable'], 'is_new'
    end

    test 'api should return user assignments for skill-1' do
      user = create(:user, organization: @org)
      api_v2_sign_in(user)
      skill_1_card = create(:card, author: @first_org_admin, taxonomy_topics: ["edcast.art_and_design.3d_and_animation.storyboarding"])
      skill_2_card = create(:card, author: @first_org_admin, taxonomy_topics: ["edcast.technology.data_science_and_analytics.advanced_data_visualization"])

      skill_1_assign = create(:assignment, assignable: skill_1_card, assignee: user, created_at: Time.now - 2.day)
      skill_2_assign = create(:assignment, assignable: skill_2_card, assignee: user, created_at: Time.now - 2.day)

      get :index, mlp_period: 'skill-wise', learning_topics_name: "edcast.art_and_design.3d_and_animation.storyboarding", limit: 10, format: :json
      resp = get_json_response(response)
      assert_equal skill_1_assign.id, resp['assignments'].first['id']
      assert_not_equal skill_2_assign.id, resp['assignments'].first['id']
    end

    test 'api should return user assignments for other' do
      user = create(:user, organization: @org)
      api_v2_sign_in(user)
      create(:user_profile, learning_topics: Taxonomies.domain_topics, user: user)

      card = create(:card, author: @first_org_admin)
      assign = create(:assignment, assignable: card, assignee: user, created_at: Time.now - 2.day)

      get :index, mlp_period: 'other', limit: 10, format: :json
      resp = get_json_response(response)
      assert_equal assign.id, resp['assignments'].first['id']
    end

    test 'should return completed assignments' do
      api_v2_sign_in(@first_assignee)

      cards = create_list(:card, 2, author: @first_org_admin)
      assignments = []
      cards.each do |card|
        assignment = create(:assignment, assignable: card, assignee: @first_assignee)
        assignment.start!
        assignment.complete!
        assignments << assignment
      end
      get :index, state: ['completed'], format: :json

      resp = get_json_response(response)
      assert_equal 2, resp['assignments'].size
      assert_same_elements cards.map(&:id).map(&:to_s), resp['assignments'].map {|a| a['assignable']['id']}
    end

    test 'api should return user assignments for other when user doesnot have learning_topics' do
      user = create(:user, organization: @org)
      api_v2_sign_in(user)
      create(:user_profile, user: user)

      card = create(:card, author: @first_org_admin)
      assign = create(:assignment, assignable: card, assignee: user, created_at: Time.now - 2.day)

      get :index, mlp_period: 'other', limit: 10, format: :json
      resp = get_json_response(response)
      assert_equal assign.id, resp['assignments'].first['id']
    end

    test 'should return deep link id and type' do
      skip # skiping these because it is use in mobile as we modifiy assignments response.
      vcard = create(:card, card_type: 'video_stream')
      vs = create(:iris_video_stream, card: vcard, status: 'past')

      vs_assignment = create(:assignment, assignable: vcard, assignee: @first_assignee)
      create(:team_assignment, team: @first_team, assignment: vs_assignment)

      api_v2_sign_in @first_assignee

      get :index, limit: 15, format: :json
      resp = get_json_response(response)

      assert_equal 1, resp['assignments'].size
      assert_same_elements [vcard.id].map(&:to_s), resp['assignments'].map {|a| a['assignable']['id']}

      assert_equal vs.card.id, resp['assignments'].select {|a| vcard.id.to_s == a['assignable']['id']}[0]['deep_link_id']
      assert_equal 'card', resp['assignments'].select {|a| vcard.id.to_s == a['assignable']['id']}[0]['deep_link_type']
    end

    test 'should return user assignments #limit' do
      cards = create_list(:card, 3, author: @first_org_admin)
      assignments = []
      cards.each do |card|
        assignment = create(:assignment, assignable: card, assignee: @first_assignee)
        assignments << assignment
      end

      api_v2_sign_in @first_assignee

      get :index, limit: 1, format: :json

      resp = get_json_response(response)
      assert_equal 1, resp['assignments'].size
    end

    test 'should return user total assignments count' do
      cards = create_list(:card, 3, author: @first_org_admin)
      assignments = []
      cards.each do |card|
        assignment = create(:assignment, assignable: card, assignee: @first_assignee)
        assignments << assignment
      end

      api_v2_sign_in @first_assignee

      get :index, format: :json
      resp = get_json_response(response)
      assert_equal 3, resp['assignment_count']
    end

    test 'should return user total assignments count even if limit given' do
      cards = create_list(:card, 3, author: @first_org_admin)
      assignments = []
      cards.each do |card|
        assignment = create(:assignment, assignable: card, assignee: @first_assignee)
        assignments << assignment
      end

      api_v2_sign_in @first_assignee

      get :index, limit: 1, format: :json
      resp = get_json_response(response)
      assert_equal 3, resp['assignment_count']
    end

    test ':api should include individual assignments' do
      cards = create_list(:card, 3, author: @first_org_admin)
      assignments = []
      cards.each do |card|
        assignment = create(:assignment, assignable: card, assignee: @another_assignee)
        create(:team_assignment, assignment: assignment, team: nil, assignor: @first_org_admin)
        assignments << assignment
      end

      api_v2_sign_in @another_assignee
      get :index, offset: 1, limit: 2, format: :json

      resp = get_json_response(response)
      assert_equal 2, resp['assignments'].size
    end

    test 'should return user assignments with card_type pack and poll not media' do
      user = create(:user, organization: @org)
      pack_card = create(:card, message: 'superman at edcast', card_type: "pack", organization: @org, author: user)
      poll_card = create(:card, message: 'superman at edcast', card_type: "poll", organization: @org, author: user)
      media_card = create(:card, message: 'superman at edcast', card_type: "media", organization: @org, author: user)

      card = create(:card, message: 'Funny things to do', card_type: "media", organization: @org, author: user)
      CardPackRelation.add(cover_id: pack_card.id, add_id: card.id, add_type: 'Card')

      pack_card.publish!

      assignment = create(:assignment, assignable: pack_card, assignee: @first_assignee)
      create(:team_assignment, team: @first_team, assignment: assignment, assignor: @first_assignor)
      assignment1 = create(:assignment, assignable: poll_card, assignee: @first_assignee)
      create(:team_assignment, team: @first_team, assignment: assignment1, assignor: @first_assignor)
      assignment2 = create(:assignment, assignable: media_card, assignee: @first_assignee)
      create(:team_assignment, team: @first_team, assignment: assignment2, assignor: @first_assignor)

      api_v2_sign_in(@first_assignee)

      get :index, card_type: ['pack', 'poll'], limit: 50, format: :json
      resp = get_json_response(response)
      assert_same_elements ["pack", "poll"], resp["assignments"].map{|ass| ass["assignable"]["card_type"]}
      assert_equal 2, resp["assignments"].length
    end
    # index ends
  end

  class AssignmentsMetricsTest < ActionController::TestCase

    setup do
      Role.any_instance.unstub :create_role_permissions
      @org = create(:organization, host_name: 'spark')
      Role.create_master_roles @org
      User.any_instance.unstub :update_role

      @first_org_admin, @another_org_admin = create_list(:user, 2, organization: @org, organization_role: 'admin')

      # team assignments
      @first_team, @second_team  = create_list(:team, 2, organization: @org)
      @first_assignee, @another_assignee = create_list(:user, 2, organization: @org)
      @first_assignor, @another_assignor = create_list(:user, 2, organization: @org)

      [@first_team, @second_team].each do |team|
        team.add_admin @first_assignor
        team.add_admin @another_assignor
      end
    end

    # assignment metrics
    test ':api return metrics for organization assignments' do
      skip # this test failing intermittently.
      srand(0)
      first_assignor_metric_for_team_1  = create(:team_assignment_metric, team_id: @first_team.id, assignor_id: @first_assignor.id, started_count: 3, assigned_count: 1, completed_count: 2)
      first_assignor_metric_for_team_2  = create(:team_assignment_metric, team_id: @second_team.id, assignor_id: @first_assignor.id, completed_count: 4)
      second_assignor_metric_for_team_1 = create(:team_assignment_metric, team_id: @first_team.id, assignor_id: @another_assignor.id, completed_count: 2)

      api_v2_sign_in(@first_org_admin)
      get :metrics, limit: 20, format: :json

      assert_response :ok
      resp = get_json_response(response)

      assert_not_empty resp['metrics']
      assert_equal 3, resp['metrics'].count

      first_metric = resp['metrics'].first
      second_metric =  resp['metrics'].second
      third_metric = resp['metrics'].third

      assert_equal 1, first_metric['assigned_count']
      assert_equal 3, first_metric['started_count']
      assert_equal 2, first_metric['completed_count']
      assert_equal @first_team.id, first_metric['team']['id']
      assert_equal @first_assignor.id, first_metric['assignor']['id']

      assert_equal 0, second_metric['assigned_count']
      assert_equal 0, second_metric['started_count']
      assert_equal 4, second_metric['completed_count']
      assert_equal @second_team.id, second_metric['team']['id']
      assert_equal @first_assignor.id, second_metric['assignor']['id']

      assert_equal 0, third_metric['assigned_count']
      assert_equal 0, third_metric['started_count']
      assert_equal 2, third_metric['completed_count']
      assert_equal @first_team.id, third_metric['team']['id']
      assert_equal @another_assignor.id, third_metric['assignor']['id']
      srand()
    end

    test 'return metrics for organization assignments #search' do
      card1 = create(:card, message: 'ruby on rails', author: create(:user, organization: @org))
      card2 = create(:card, message: 'Python', author: create(:user, organization: @org))
      card3 = create(:card, title: 'java', message: nil, author: create(:user, organization: @org))

      first_metric = create(:team_assignment_metric, assignor: @first_assignor, card: card1)
      second_metric = create(:team_assignment_metric, assignor: @another_assignor, card: card2)
      third_metric = create(:team_assignment_metric, assignor: @another_assignor, card: card3)

      api_v2_sign_in(@first_org_admin)

      get :metrics, q: 'ruby', search_type: 'card',limit: 20, format: :json

      assert_response :ok
      resp = get_json_response(response)

      assert_not_empty resp['metrics']
      assert_equal 1, resp['metrics'].count
      assert_equal [first_metric.id], resp['metrics'].map {|m| m['id']}

      # java
      get :metrics, q: 'java', search_type: 'card',limit: 20, format: :json

      assert_response :ok
      resp = get_json_response(response)

      assert_not_empty resp['metrics']
      assert_equal 1, resp['metrics'].count
      assert_equal [third_metric.id], resp['metrics'].map {|m| m['id']}
      assert_equal 'java', resp['metrics'][0]['content_name']

      get :metrics, q: @another_assignor.name, search_type: 'assignor', limit: 20, format: :json

      assert_response :ok
      resp = get_json_response(response)

      assert_not_empty resp['metrics']
      assert_equal 2, resp['metrics'].count
      assert_same_elements [second_metric.id, third_metric.id], resp['metrics'].map {|m| m['id']}

      assert_equal @another_assignor.first_name, resp['metrics'][0]['assignor']['first_name']
      assert_equal @another_assignor.last_name, resp['metrics'][0]['assignor']['last_name']
    end

    test 'should return users for assignment metric' do
      batman_card = create(:card, title: 'batman at edcast', author: @first_org_admin)
      assignment = create(:assignment, assignable: batman_card, assignee: @first_assignee, state: 'started')
      team_assignment = create(:team_assignment, assignment: assignment, assignor: @first_assignor, team: @second_team)

      api_v2_sign_in(@first_org_admin)

      get :metric_users,
        assignor_id: @first_assignor.id,
        content_id: batman_card.id,
        team_id: @second_team.id,
        limit: 20,
        offset: 0,
        format: :json
      assert_response :ok

      resp = get_json_response(response)

      assert_not_empty resp['users']
      assert_equal 1, resp['users'].count
    end

    test 'return metrics for team assignments for a team' do
      metric_for_team1  = create(:team_assignment_metric, team_id: @first_team.id, assignor_id: @first_assignor.id, started_count: 3, assigned_count: 1, completed_count: 2)
      metric_for_team2  = create(:team_assignment_metric, team_id: @first_team.id, assignor_id: @first_assignor.id, completed_count: 4)
      metric_for_team3 = create(:team_assignment_metric, team_id: @first_team.id, assignor_id: @another_assignor.id, completed_count: 2)

      api_v2_sign_in(@first_assignor)
      get :metrics, team_id: @first_team.id, limit: 5, format: :json

      resp = get_json_response(response)
      assert_equal [metric_for_team1.card_id, metric_for_team2.card_id, metric_for_team3.card_id].sort, resp['metrics'].map{|d| d['card_id']}.sort
      assert_equal @first_team.id, resp['metrics'].map{|d| d['team']['id']}.uniq.first
      assert_equal 3, resp['total']
    end

    test 'return unauthorized for non team/org admin' do
      metric_for_team1  = create(:team_assignment_metric, team_id: @first_team.id, assignor_id: @first_assignor.id, started_count: 3, assigned_count: 1, completed_count: 2)

      user = create(:user, organization: @org)
      @first_team.add_member(user)
      api_v2_sign_in(user)

      get :metrics, team_id: @first_team.id, limit: 5, format: :json
      assert_response :unauthorized
    end

    test 'should return users for assignment metric with search term' do
      batman_card = create(:card, title: 'batman at edcast', author: @first_org_admin)
      assignment = create(:assignment, assignable: batman_card, assignee: @first_assignee, state: 'started')
      team_assignment = create(:team_assignment, assignment: assignment, assignor: @first_assignor, team: @second_team)

      api_v2_sign_in(@first_org_admin)

      get :metric_users,
        q: @first_assignee.email,
        content_id: batman_card.id,
        assignor_id: @first_assignor.id,
        team_id: @second_team.id,
        state: [Assignment::COMPLETED, Assignment::STARTED],
        format: :json

      assert_response :ok

      resp = get_json_response(response)

      assert_not_empty resp['users']
      assert_equal 1, resp['users'].count
      assert_equal @first_assignee.first_name, resp['users'][0]['first_name']
      assert_equal @first_assignee.name, resp['users'][0]['name']
    end

    test 'should return metrics for group_sub_admin user' do
      org = @first_assignor.organization
      user = create(:user, organization: org)
      Role.create_standard_roles org
      member_role = Role.find_by(name: 'member')
      create(:role_permission, role: member_role, name: Permissions::MANAGE_GROUP_ANALYTICS)
      member_role.add_user(user)
      @second_team.add_sub_admin(user)
      metric_for_team1  = create(
        :team_assignment_metric,
        team_id: @first_team.id,
        assignor_id: @first_assignor.id,
        started_count: 3,
        assigned_count: 1,
        completed_count: 2
      )
      metric_for_team2  = create(
        :team_assignment_metric,
        team_id: @second_team.id,
        assignor_id: @another_assignor.id,
        completed_count: 4
      )
      api_v2_sign_in(user)
      get :metrics, format: :json
      resp = get_json_response(response)

      assert_equal [ metric_for_team2.card_id], resp['metrics'].map{|d| d['card_id']}
      assert_equal 1, resp['total']
    end

    test 'should return metrics for assignment assined by other assignor' do
      batman_card = create(:card, title: 'batman at edcast', author: @first_org_admin)
      assignment = create(:assignment, assignable: batman_card, assignee: @first_assignee, state: 'started')
      team_assignment = create(:team_assignment, assignment: assignment, assignor: @first_assignor, team: @second_team)

      api_v2_sign_in(@another_org_admin)

      get :metric_users,
        q: @first_assignee.email,
        content_id: batman_card.id,
        assignor_id: @first_assignor.id,
        team_id: @second_team.id,
        state: [Assignment::COMPLETED, Assignment::STARTED],
        format: :json

      assert_response :ok

      resp = get_json_response(response)

      assert_not_empty resp['users']
      assert_equal 1, resp['users'].count
      assert_equal @first_assignee.first_name, resp['users'][0]['first_name']
    end

    test 'should not return results for invalid team or assignor' do
      batman_card = create(:card, title: 'batman at edcast', author: @first_org_admin)
      api_v2_sign_in(@another_org_admin)

      invalid_team = create(:team)
      invalid_assignor = create(:user)

      get :metric_users,
        q: @first_assignee.email,
        content_id: batman_card.id,
        assignor_id: @first_assignor.id,
        team_id: invalid_team.id,
        state: [Assignment::COMPLETED, Assignment::STARTED],
        format: :json

      assert_response :unprocessable_entity

      resp = get_json_response(response)
      assert_match /Team does not exists/, resp['message']

      get :metric_users,
        q: @first_assignee.email,
        content_id: batman_card.id,
        assignor_id: invalid_assignor.id,
        team_id: @second_team.id,
        state: [Assignment::COMPLETED, Assignment::STARTED],
        format: :json

      assert_response :unprocessable_entity

      resp = get_json_response(response)
      assert_match /Assignor does not exists/, resp['message']
    end

    test "should return metrics for assignment irrespective of assignor" do
      batman_card = create(:card, title: 'batman at edcast', author: @first_org_admin)
      assignee1, assignee2 = create_list(:user, 2, organization: @org)
      assignment1 = create(:assignment, assignable: batman_card, assignee: @first_assignee, state: 'started')
      assignment2 = create(:assignment, assignable: batman_card, assignee: assignee1, state: 'started')
      assignment3 = create(:assignment, assignable: batman_card, assignee: assignee2, state: 'started')

      team_assignment = create(:team_assignment, assignment: assignment1, assignor: @first_assignor, team: @second_team)
      team_assignment = create(:team_assignment, assignment: assignment2, assignor: @first_assignor, team: @second_team)
      team_assignment = create(:team_assignment, assignment: assignment3, assignor: @second_assignor, team: @second_team)

      api_v2_sign_in(@another_org_admin)

      get :metric_users,
        content_id: batman_card.id,
        team_id: @second_team.id,
        state: [Assignment::COMPLETED, Assignment::STARTED],
        format: :json

      assert_response :ok

      resp = get_json_response(response)

      assert_not_empty resp['users']
      assert_equal 3, resp['users'].count
      assert_equal @first_assignee.first_name, resp['users'][0]['first_name']
    end

    test "should return metrics for assignment irrespective of assignor for admin and team admin" do
      batman_card = create(:card, title: 'batman at edcast', author: @first_org_admin)
      assignee1, assignee2 = create_list(:user, 2, organization: @org)
      assignment1 = create(:assignment, assignable: batman_card, assignee: @first_assignee, state: 'started')
      assignment2 = create(:assignment, assignable: batman_card, assignee: assignee1, state: 'started')
      assignment3 = create(:assignment, assignable: batman_card, assignee: assignee2, state: 'started')

      team_assignment = create(:team_assignment, assignment: assignment1, assignor: @first_assignor, team: @second_team)
      team_assignment = create(:team_assignment, assignment: assignment2, assignor: @first_assignor, team: @second_team)
      team_assignment = create(:team_assignment, assignment: assignment3, assignor: @second_assignor, team: @second_team)

      api_v2_sign_in(@first_assignor)

      get :metric_users,
        content_id: batman_card.id,
        team_id: @second_team.id,
        state: [Assignment::COMPLETED, Assignment::STARTED],
        format: :json

      assert_response :ok

      resp = get_json_response(response)

      assert_not_empty resp['users']
      assert_equal 3, resp['users'].count
      assert_equal @first_assignee.first_name, resp['users'][0]['first_name']
    end

    test 'should return user.id and total' do
      batman_card = create(:card, title: 'batman at edcast', author: @first_org_admin)
      assignee = create(:user,  organization: @org)
      assignment = create(:assignment, assignable: batman_card, assignee: assignee, state: 'started')
      team_assignment = create(:team_assignment, assignment: assignment, assignor: @first_assignor, team: @second_team)

      api_v2_sign_in(@another_org_admin)

      get :metric_users,
          content_id: batman_card.id,
          team_id: @second_team.id,
          state: [Assignment::COMPLETED, Assignment::STARTED],
          format: :json

      assert_response :ok
      resp = get_json_response(response)

      assert resp['users'][0].key?('id')
      assert resp.key?('total')
    end
  end

  class AssignmentsNoticsTest < ActionController::TestCase
    # Notice
    test ':api assignment notice details' do
      org = create(:organization)
      author, assignee = create_list(:user, 2, organization: org)
      card = create(:card, organization: org, author: author)

      create(:assignment, assignable: card, assignee: assignee)
      api_v2_sign_in assignee

      get :notice, format: :json
      assert_response :ok
      resp = get_json_response(response)

      assert_equal 1, resp['new_assignments_count']
    end

    test 'assignment notice dismiss' do
      org = create(:organization)
      author, assignee = create_list(:user, 2, organization: org)
      card = create(:card, organization: org, author: author)

      create(:assignment, assignable: card, assignee: assignee)
      api_v2_sign_in assignee

      Timecop.freeze do
        assert_difference ->{AssignmentNoticeStatus.count} do
          post :dismiss_notice
          assert_equal Time.now.utc.to_i, AssignmentNoticeStatus.where(user: assignee).first.notice_dismissed_at.utc.to_i
        end
      end
    end
  end

  class AssignmentsNotifyTest < ActionController::TestCase
    setup do
      @org = create(:organization, host_name: 'spark')

      @first_org_admin, @another_org_admin = create_list(:user, 2, organization: @org, organization_role: 'admin')

      # team assignments
      @first_team, @second_team  = create_list(:team, 2, organization: @org)
      @first_assignee, @another_assignee = create_list(:user, 2, organization: @org)
      @first_assignor, @another_assignor = create_list(:user, 2, organization: @org)

      [@first_team, @second_team].each do |team|
        team.add_admin @first_assignor
        team.add_admin @another_assignor
      end
    end

    # notify
    test ':api it should notify all assignees with #assignment ids' do
      cards = create_list(:card, 2, author: @first_org_admin)
      completed_assignments = []
      cards.each do |card|
        assignment = create(:assignment, assignable: card, assignee: @first_assignee, state: Assignment::COMPLETED)
        create(:team_assignment, team: @second_team, assignment: assignment, assignor: @first_assignor)
        completed_assignments << assignment
      end

      api_v2_sign_in @first_org_admin

      TeamAssignment.where(assignment: completed_assignments).each do |ta|
        UserTeamAssignmentNotificationJob.expects(:perform_later)
          .with(
            causal_user_id: @first_org_admin.id,
            notification_type: 'assignment_notice',
            message: 'Congratulations!',
            team_id: ta.team_id,
            team_assignment_id: ta.id
          )
      end
      post :notify, assignment_ids: completed_assignments.map(&:id), message: 'Congratulations!'

      assert_response :ok
      resp = get_json_response(response)

      assert_match /#{completed_assignments.count} were notified/, resp['message']
    end

    test 'should notify all assignees with #states' do
      cards = create_list(:card, 4, author: @first_org_admin)
      completed_assignments, started_assignments = [], []
      cards.first(2).each do |card|
        assignment = create(:assignment, assignable: card, assignee: @first_assignee, state: Assignment::COMPLETED)
        create(:team_assignment, team: @second_team, assignment: assignment, assignor: @first_assignor)
        completed_assignments << assignment
      end

      cards.last(2).each do |card|
        assignment = create(:assignment, assignable: card, assignee: @first_assignee, state: Assignment::STARTED)
        create(:team_assignment, team: @second_team, assignment: assignment, assignor: @first_assignor)
        started_assignments << assignment
      end

      api_v2_sign_in @first_org_admin

      assignments = completed_assignments + started_assignments
      TeamAssignment.where(assignment: assignments).each do |ta|
        UserTeamAssignmentNotificationJob.expects(:perform_later)
        .with(
          causal_user_id: @first_org_admin.id,
          notification_type: 'assignment_notice',
          message: 'Congratulations!',
          team_id: ta.team_id,
          team_assignment_id: ta.id
        )
      end
      post :notify, state: ['started', 'completed'], message: 'Congratulations!'

      assert_response :ok
      resp = get_json_response(response)

      assert_match /#{assignments.count} were notified/, resp['message']
    end

    test 'should notify all assignees with #team id' do
      assignments = []
      cards = create_list(:card, 2, author: @first_org_admin)
      cards.each do |card|
        assignment = create(:assignment, assignable: card, assignee: @another_assignee, state: Assignment::STARTED)
        create(:team_assignment, team: @first_team, assignment: assignment, assignor: @first_assignor)
        assignments << assignment
      end

      api_v2_sign_in @first_org_admin

      TeamAssignment.where(assignment: assignments).each do |ta|
        UserTeamAssignmentNotificationJob.expects(:perform_later).with(causal_user_id: @first_org_admin.id, notification_type: 'assignment_notice', message: 'Congratulations!', team_id: ta.team_id, team_assignment_id: ta.id)
      end
      post :notify, team_id: @first_team.id, message: 'Congratulations!'

      assert_response :ok
      resp = get_json_response(response)

      assert_match /#{assignments.count} were notified/, resp['message']
    end

    test 'should notify all assignees with #assignor id' do
      assignments = []
      cards = create_list(:card, 2, author: @first_org_admin)
      cards.each do |card|
        assignment = create(:assignment, assignable: card, assignee: @another_assignee, state: Assignment::STARTED)
        create(:team_assignment, team: @first_team, assignment: assignment, assignor: @first_assignor)
        assignments << assignment
      end

      api_v2_sign_in @first_org_admin

      TeamAssignment.where(assignment: assignments).each do |ta|
        UserTeamAssignmentNotificationJob.expects(:perform_later).with(causal_user_id: @first_org_admin.id, notification_type: 'assignment_notice', message: 'Congratulations!', team_id: ta.team_id, team_assignment_id: ta.id)
      end
      post :notify, assignor_id: @first_assignor.id, message: 'Congratulations!'

      assert_response :ok
      resp = get_json_response(response)

      assert_match /#{assignments.count} were notified/, resp['message']
    end

    test 'should notify all assignees with #content id' do
      card = create(:card, author: @first_org_admin)
      assignment = create(:assignment, assignable: card, assignee: @another_assignee, state: Assignment::STARTED)
      create(:team_assignment, team: @first_team, assignment: assignment, assignor: @first_assignor)

      api_v2_sign_in @first_org_admin
      TeamAssignment.where(assignment: assignment).each do |ta|
        UserTeamAssignmentNotificationJob.expects(:perform_later).with(causal_user_id: @first_org_admin.id, notification_type: 'assignment_notice', message: 'Congratulations!', team_id: ta.team_id, team_assignment_id: ta.id)
      end
      post :notify, content_id: card.id, message: 'Congratulations!'

      assert_response :ok
      resp = get_json_response(response)

      assert_match /1 were notified/, resp['message']
    end

    test 'only admin can notify' do
      api_v2_sign_in @first_assignee

      post :notify, assignment_ids: [1, 2], message: 'hi'
      assert_response :unauthorized
    end

    test 'required parameters for notify #no message' do
      api_v2_sign_in @first_org_admin

      post :notify, format: :json

      assert_response :unprocessable_entity
      assert_match /Message can not be blank/, get_json_response(response)['message']
    end

    test 'required parameters for notify #optinal params' do
      api_v2_sign_in @first_org_admin

      post :notify, message: 'Congratulations', format: :json

      assert_response :unprocessable_entity
      assert_match /One of team_id, assignor_id, content_id, state,assignment_ids is required/, get_json_response(response)['message']
    end

    test 'Invalid assignments' do
      api_v2_sign_in @another_org_admin

      post :notify, assignment_ids: [0], message: 'Congratulations!'

      assert_response :unprocessable_entity
      assert_match /No assignments found/, get_json_response(response)['message']
    end
  end

  class CompletionBeforeAssignmentTest < ActionController::TestCase

    setup do
      @new_org = create(:organization)
    end

    test "should set the state to 'completed' when user completes a card before & the card gets assigned after" do
      user_who_assigns = create(:user, organization: @new_org, organization_role: 'admin')
      user_who_completes = create(:user, organization: @new_org)
      cards = create_list(:card, 2, author:user_who_assigns, organization: @new_org)
      create(:user_content_completion, completable: cards[0], user: user_who_completes,state: 'completed')

      TestAfterCommit.with_commits do
        assign1 = create(:assignment, assignable: cards[0], assignee: user_who_completes, due_at: Date.new(Date.current.year,01,01))
        assign2 = create(:assignment, assignable: cards[1], assignee: user_who_completes, due_at: Date.new(Date.current.year,01,01))
        api_v2_sign_in(user_who_completes)
        get :index, state: ['started', 'assigned'], limit: 10, format: :json

        resp = get_json_response(response)
        assert_equal 1, resp['assignments'].size
      end
    end
  end

  class DismissAssignmentTest < ActionController::TestCase

    test 'should dismiss assignment, but user content completion should remain unaffected ' do
      org = create(:organization)
      user = create(:user, organization: org, organization_role: 'member')
      card = create(:card, author_id: user.id)
      author = create(:user, organization: org)
      ucc = create(:user_content_completion, user_id: user.id, completable: card, state: 'started')
      assignment = create(:assignment, assignable: card, assignee: user, state: 'assigned')
      api_v2_sign_in(user)
      assert_difference -> {UserContentCompletion.count}, 0 do
        post :dismiss, content_id: card.id,  format: :json
      end

      assert_response :success
      assert assignment.reload.dismissed?
    end

    test 'should dismiss assignment, but learning queue should remain unaffected ' do
      org = create(:organization)
      user = create(:user, organization: org, organization_role: 'member')
      card = create(:card, author_id: user.id)
      author = create(:user, organization: org)
      ucc = create(:user_content_completion, user_id: user.id, completable: card, state: 'started')
      lqi = create(:learning_queue_item, user_id: user.id, queueable: card, state: 'completed', source:'assignments')
      assignment = create(:assignment, assignable: card, assignee: user, state: 'assigned')
      api_v2_sign_in(user)
      assert_difference -> {LearningQueueItem.count}, 0 do
        post :dismiss, content_id: card.id,  format: :json
      end

      assert_response :success
      assert assignment.reload.dismissed?
    end
  end


  class CustomQuarterAssignmentTest < ActionController::TestCase

    setup do
      TestAfterCommit.enabled = false
      @org = create(:organization, host_name: 'spark')
      @first_org_admin, @another_org_admin = create_list(:user, 2, organization: @org, organization_role: 'admin')
      @quarter1 = create(:quarter, name: "quarter-1", start_date: "2018-02-02", end_date: "2018-05-01", organization_id: @org.id)
      @quarter2 = create(:quarter, name: "quarter-2", start_date: "2018-05-02", end_date: "2018-08-01", organization_id: @org.id)
      @quarter3 = create(:quarter, name: "quarter-3", start_date: "2018-08-02", end_date: "2018-11-01", organization_id: @org.id)
      @quarter4 = create(:quarter, name: "quarter-4", start_date: "2018-11-02", end_date: "2019-02-01", organization_id: @org.id)
      @org.configs.create( name: "enable_custom_quarters", data_type: "boolean", value: "t")
      @user = create(:user, organization: @org)
      api_v2_sign_in(@user)
      @card = create(:card, author: @first_org_admin)
    end

    test 'api should return user assignments for mlp_periods quarter 1 based on custom quarter' do
      assignment = create(:assignment, assignable: @card, assignee: @user, due_at: Date.parse("#{Date.current.year}/04/01"))
      get :index, mlp_period: 'quarter-1', limit: 10, format: :json
      resp = get_json_response(response)
      assert_equal assignment.id, resp['assignments'].first['id']
    end

    test 'api should return user assignments for mlp_periods quarter 2 based on custom quarter ' do
      assignment = create(:assignment, assignable: @card, assignee: @user, due_at: Date.parse("#{Date.current.year}/06/01"))
      get :index, mlp_period: 'quarter-2', limit: 10, format: :json
      resp = get_json_response(response)
      assert_equal assignment.id, resp['assignments'].first['id']
    end

    test 'api should return user assignments for mlp_periods quarter 3 based on custom quarter' do
      assignment = create(:assignment, assignable: @card, assignee: @user, due_at: Date.parse("#{Date.current.year}/09/01"))
      get :index, mlp_period: 'quarter-3', limit: 10, format: :json
      resp = get_json_response(response)
      assert_equal assignment.id, resp['assignments'].first['id']
    end

    test 'api should return user assignments for mlp_periods quarter 4 based on custom quarter' do
      assignment = create(:assignment, assignable: @card, assignee: @user, due_at: Date.parse("#{Date.current.year}/12/01"))
      get :index, mlp_period: 'quarter-4', limit: 10, format: :json
      resp = get_json_response(response)
      assert_equal assignment.id, resp['assignments'].first['id']
    end

    test 'api should return user assignments for mlp_periods quarter 1,2,3,4 based on custom quarter and selected year is 2015' do
      cards = create_list(:card, 4, author: @first_org_admin)
      quarter_assignmnets_due_at = {"quarter-1": ["2015/04/01", cards[0]],
                                    "quarter-2": ["2015/06/01", cards[1]],
                                    "quarter-3": ["2015/09/01", cards[2]],
                                    "quarter-4": ["2015/12/01", cards[3]]}
      quarter_assignmnets_due_at.each do |key, value|
        assignment = create(:assignment, assignable: value[1], assignee: @user, due_at: Date.parse(value[0]))
        get :index, mlp_period: "#{key}", year_filter: 2015, limit: 10, format: :json
        resp = get_json_response(response)
        assert_equal assignment.id, resp['assignments'].first['id']
      end
    end

     test 'should not create quarter with duplicate name in a organization' do
      quarter = build(:quarter, name: "quarter-1", start_date: "2018-11-02", end_date: "2018-11-01", organization_id: @org.id)
      refute quarter.valid?
      assert quarter.errors.full_messages.include?("Name has already been taken")
    end
  end

  class CustomQuarterCreateTest < ActionController::TestCase

    setup do
      TestAfterCommit.enabled = false
      @org = create(:organization, host_name: 'spark')
      @first_org_admin, @another_org_admin = create_list(:user, 2, organization: @org, organization_role: 'admin')
      @org.configs.create( name: "enable_custom_quarters", data_type: "boolean", value: "t")
      @card = create(:card, author: @first_org_admin)
      @user = create(:user, organization: @org)
      api_v2_sign_in(@user)
    end

    test 'should return current year user assignments if custom quarters are defined in previous year' do
      @org.configs.create(name: "enable_custom_quarters", data_type: "boolean", value: "t")
      quarter1 = create(:quarter, name: "quarter-1", start_date: "2016-02-02", end_date: "2016-05-01", organization_id: @org.id)
      quarter2 = create(:quarter, name: "quarter-2", start_date: "2016-05-02", end_date: "2016-08-01", organization_id: @org.id)
      quarter3 = create(:quarter, name: "quarter-3", start_date: "2016-08-02", end_date: "2016-11-01", organization_id: @org.id)
      quarter4 = create(:quarter, name: "quarter-4", start_date: "2016-11-02", end_date: "2017-02-01", organization_id: @org.id)

      assignment = create(:assignment, assignable: @card, assignee: @user, due_at: Date.new(Date.current.year,04,01))

      get :index, mlp_period: 'quarter-1', limit: 10, format: :json
      resp = get_json_response(response)
      assert_equal assignment.id, resp['assignments'].first['id']
    end

    test 'should not create quarter with invalid quarter name' do
      quarter = build(:quarter, name: "quarter", start_date: "2018-11-02", end_date: "2019-02-01", organization_id: @org.id)
      refute quarter.valid?
      assert quarter.errors.full_messages.include?("Quarter name is invalid")
    end
  end

  class CurrentlyAssignedTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      @card = create(:card, author: @user)
      api_v2_sign_in @user
    end

    test 'should return unprocessable_entity if content is not provided' do
      get :assigned, format: :json
      assert_response :unprocessable_entity
    end

    test 'should called assignment assigned_user with correct params for user search' do
      Assignment.expects(:assigned_users).with(
        q: 'abc',assignor_id: nil, team_id: nil, content_id: @card.id, state: nil)
          .returns(Assignment.where(assignable: @card))
      
      TeamAssignment.expects(:assigned_teams).with(
        q: nil, assignor_id: nil, content_id: @card.id)
          .returns(Team.where(organization: @org).select("DISTINCT(teams.id), teams.name"))

      get :assigned, content_id: @card.id, search_type: 'user', q: 'abc', format: :json
    end

    test 'should called assignment assigned_user with correct params for team search' do
      Assignment.expects(:assigned_users).with(
        q: nil, assignor_id: nil, team_id: nil, content_id: @card.id, state: nil)
          .returns(Assignment.where(assignable: @card))

      TeamAssignment.expects(:assigned_teams).with(
        q: 'abc',assignor_id: nil, content_id: @card.id)
        .returns(Team.where(organization: @org).select("DISTINCT(teams.id), teams.name"))

      get :assigned, content_id: @card.id, search_type: 'team', q: 'abc', format: :json
    end

    test 'should called assignment and team_assignment method with correct params for assignor' do
      Assignment.expects(:assigned_users).with(
        q: nil, assignor_id: @user.id, team_id: nil, content_id: @card.id, state: nil)
          .returns(Assignment.where(assignable: @card))

      TeamAssignment.expects(:assigned_teams).with(
        q: nil,assignor_id: @user.id, content_id: @card.id)
        .returns(Team.where(organization: @org).select("DISTINCT(teams.id), teams.name"))

      get :assigned, content_id: @card.id, assignor_id: @user.id,  format: :json
    end

    test 'should send the necessary details in response for assigned users/teams' do
      card = create(:card, author: create(:user, organization: @org))
      user_list = create_list(:user, 2, organization: @org)    
      team = create(:team, organization: @org)
      
      user_list.each{|user| team.add_member user}
      
      assignment1 = create(:assignment, assignable: card, user_id: user_list[0].id)
      assignment2 = create(:assignment, assignable: card, user_id: user_list[1].id)
      assignment3 = create(:assignment, assignable: card, user_id: @user.id)
      
      create(:team_assignment, team_id: team.id, assignment_id: assignment1.id)
      create(:team_assignment, team_id: team.id, assignment_id: assignment2.id)

      get :assigned, content_id: card.id,  format: :json
      resp = get_json_response(response)
      
      assert_equal 2, resp['users'].count
      assert_equal 1, resp['teams'].count
      
      assert_same_elements user_list.map(&:id), resp['users'].map{|d| d['id']}
      assert_not_includes resp['users'].map{|d| d['id']}, @user.id

      assert_same_elements [team.id], resp['teams'].map{|d| d['id']}
      assert_equal team.name, resp['teams'][0]['name']
    end
  end

  class AccessibleAssignmentsTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      @admin_user = create(:user, organization: @org, organization_role: 'admin')
      @user = create(:user, organization: @org)
    end

    test 'should fetch only accessible assignments for member user' do
      card_list = create_list(:card, 2, author_id: @admin_user.id, is_public: false)
      public_card = create(:card, author_id: @admin_user.id)

      create(:card_user_permission, card_id: card_list[0].id, user_id: @user.id)
      assignment1 = create(:assignment, assignable: card_list[0], user_id: @user.id)
      assignment2 = create(:assignment, assignable: card_list[1], user_id: @user.id)
      assignment3 = create(:assignment, assignable: public_card, user_id: @user.id)

      api_v2_sign_in @user

      get :index, state: ['started', 'assigned'], limit: 10, format: :json
      resp = get_json_response(response)

      assert_response :ok
      assert_equal 2, resp['assignments'].count
      assert_not_includes resp['assignments'].map{|d| d['assignable']['id'].to_i}, card_list[1].id
      assert_includes resp['assignments'].map{|d| d['assignable']['id'].to_i}, card_list[0].id
      assert_includes resp['assignments'].map{|d| d['assignable']['id'].to_i}, public_card.id
    end

    test 'should fetch all assigned content for admin' do
      card_list = create_list(:card, 2, author_id: @user.id, is_public: false)
      public_card = create(:card, author_id: @user.id)

      assignment1 = create(:assignment, assignable: card_list[0], user_id: @admin_user.id)
      assignment2 = create(:assignment, assignable: card_list[1], user_id: @admin_user.id)
      assignment3 = create(:assignment, assignable: public_card, user_id: @admin_user.id)

      api_v2_sign_in @admin_user
      
      get :index, state: ['started', 'assigned'], limit: 10, format: :json
      resp = get_json_response(response)
      
      assert_response :ok
      assert_equal 3, resp['assignments'].count
      assert_same_elements (card_list + [public_card]).map(&:id), resp['assignments'].map{|d| d['assignable']['id'].to_i}
    end
  end

  test "should return assignments as per due_at asc order when order and sort parameters are passed " do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      cards = create_list(:card, 4, author: @user)
      api_v2_sign_in @user

      assign1 = create(:assignment, assignable: cards[0], assignee: @user, due_at: Time.now - 3.day)
      assign2 = create(:assignment, assignable: cards[1], assignee: @user, due_at: Time.now - 2.day)
      assign3 = create(:assignment, assignable: cards[2], assignee: @user, due_at: Time.now - 1.day)
      assign4 = create(:assignment, assignable: cards[3], assignee: @user, due_at: Time.now + 1.day)

      assign2.start!
      assign3.start!
      assign4.start!

      get :index, state: ["started"],sort: "due_at", order: "asc" ,limit: 50, format: :json
      resp = get_json_response(response)
      assert_equal [assign2.id, assign3.id, assign4.id], resp['assignments'].map {|a| a['id']}
  end

  test "should return assignments as per due_at desc order when only sort by parameter is passed " do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      cards = create_list(:card, 4, author: @user)
      api_v2_sign_in @user

      assign1 = create(:assignment, assignable: cards[0], assignee: @user, due_at: Time.now - 3.day)
      assign2 = create(:assignment, assignable: cards[1], assignee: @user, due_at: Time.now - 2.day)
      assign3 = create(:assignment, assignable: cards[2], assignee: @user, due_at: Time.now - 1.day)
      assign4 = create(:assignment, assignable: cards[3], assignee: @user, due_at: Time.now + 1.day)

      assign2.start!
      assign3.start!
      assign4.start!

      get :index, state: ["started"],sort: "due_at",limit: 50, format: :json
      resp = get_json_response(response)
      assert_equal [assign4.id, assign3.id, assign2.id], resp['assignments'].map {|a| a['id']}
  end
end

require 'test_helper'

class Api::V2::UserLevelMetricsControllerTest < ActionController::TestCase

  setup do
    @org  = create(:organization)
    @user = create(:user, organization: @org)
    api_v2_sign_in @user
  end
  # index
  test ':api metrics for user' do
    Timecop.freeze do
      create(:user_level_metric, user_id: @user.id, smartbites_score: 10, smartbites_consumed: 2, smartbites_created: 1, time_spent: 20, percentile: 40, period: :all_time, offset: 0)

      topics = create_list(:interest, 3)

      topics.each_with_index do |topic, indx|
        create(:user_topic_level_metric, user_id: @user.id, tag_id: topic.id, smartbites_score: indx, smartbites_consumed: 2, smartbites_created: 1, time_spent: 20, period: :all_time, offset: 0)
      end

      taxonomy_topics = ["edcast.finance.personal_finance.financial_advising","edcast.technology.data_science_and_analytics.big_data", "edcast.technology.it_infrastructure.network_and_information_security", "edcast.finance.personal_finance.retirement_planning", "edcast.technology.data_science_and_analytics.statistical_analysis", "edcast.finance.personal_finance.wealth_management", "edcast.business.project_management.trello", "edcast.technology.information_management.records_management", "edcast.finance.personal_finance.portfolio_management", "edcast.finance.corporate_finance.brokerage"]
      user_profile = @user.build_profile(learning_topics: [{"topic_name"=>"edcast.technology.data_science_and_analytics.big_data", "topic_id"=>"4e2b0411-5d45-4474-8029-db508cd0517d", "topic_label"=>"Big data", "domain_name"=>"edcast.technology", "domain_id"=>"30a3e1db-4898-49d8-b169-b60cfb888957", "domain_label"=>"Technology"}])
      user_profile.save
      taxonomy_topics.each_with_index do |topic,indx|
        create(:user_taxonomy_topic_level_metric, user_id: @user.id, taxonomy_label: topic, smartbites_score: indx, smartbites_consumed: 2, smartbites_created: 1, time_spent: 20, period: :all_time, offset: 0)
      end

      get :index, format: :json
      assert_response :ok

      resp = get_json_response(response)

      assert_equal 10, resp["smartbites_score"]
      assert_equal 1, resp["smartbites_created"]
      assert_equal 2, resp["smartbites_consumed"]
      assert_equal 20, resp["time_spent"]
      assert_equal 40, resp["percentile"]
      assert_equal topics.reverse.each_with_index.map{|topic, indx| {'id' => topic.id, 'name' => topic.name, 'score' => 2 - indx}}, resp['topics']
      assert_equal  [{"taxonomy_label"=> "edcast.technology.data_science_and_analytics.big_data","name" =>"Big data","score"=>1}],resp['taxonomy_topics']
    end
  end

  test ':api metrics for particular user when user_id is passed' do
    @user2 = create(:user, organization: @org)

    Timecop.freeze do
      create(:user_level_metric, user_id: @user2.id, smartbites_score: 10, smartbites_consumed: 2, smartbites_created: 1, time_spent: 20, percentile: 40, period: :all_time, offset: 0)

      topics = create_list(:interest, 3)

      topics.each_with_index do |topic, indx|
        create(:user_topic_level_metric, user_id: @user2.id, tag_id: topic.id, smartbites_score: indx, smartbites_consumed: 2, smartbites_created: 1, time_spent: 20, period: :all_time, offset: 0)
      end

      taxonomy_topics = ["edcast.finance.personal_finance.financial_advising","edcast.technology.data_science_and_analytics.big_data", "edcast.technology.it_infrastructure.network_and_information_security", "edcast.finance.personal_finance.retirement_planning", "edcast.technology.data_science_and_analytics.statistical_analysis", "edcast.finance.personal_finance.wealth_management", "edcast.business.project_management.trello", "edcast.technology.information_management.records_management", "edcast.finance.personal_finance.portfolio_management", "edcast.finance.corporate_finance.brokerage"]
      user_profile = @user2.build_profile(learning_topics: [{"topic_name"=>"edcast.technology.data_science_and_analytics.big_data", "topic_id"=>"4e2b0411-5d45-4474-8029-db508cd0517d", "topic_label"=>"Big data", "domain_name"=>"edcast.technology", "domain_id"=>"30a3e1db-4898-49d8-b169-b60cfb888957", "domain_label"=>"Technology"}])
      user_profile.save
      taxonomy_topics.each_with_index do |topic,indx|
        create(:user_taxonomy_topic_level_metric, user_id: @user2.id, taxonomy_label: topic, smartbites_score: indx, smartbites_consumed: 2, smartbites_created: 1, time_spent: 20, period: :all_time, offset: 0)
      end

      get :index,user_id: @user2.id , format: :json
      assert_response :ok

      resp = get_json_response(response)

      assert_equal 10, resp["smartbites_score"]
      assert_equal 1, resp["smartbites_created"]
      assert_equal 2, resp["smartbites_consumed"]
      assert_equal 20, resp["time_spent"]
      assert_equal 40, resp["percentile"]
      assert_equal topics.reverse.each_with_index.map{|topic, indx| {'id' => topic.id, 'name' => topic.name, 'score' => 2 - indx}}, resp['topics']
      assert_equal  [{"taxonomy_label"=> "edcast.technology.data_science_and_analytics.big_data","name" =>"Big data","score"=>1}],resp['taxonomy_topics']
    end
  end


  test 'percentile for user with no activity' do
    user = create(:user, organization: @org)
    api_v2_sign_in user

    get :index, format: :json
    assert_response :ok

    resp = get_json_response(response)
    assert_equal 0, resp['percentile']
  end

  test 'support period param' do
    Timecop.freeze do
      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(Time.now.utc)
      create(:user_level_metric, user_id: @user.id, smartbites_score: 10, smartbites_consumed: 2, smartbites_created: 1, time_spent: 20, period: :all_time, offset: 0)
      create(:user_level_metric, user_id: @user.id, smartbites_score: 5, smartbites_consumed: 1, smartbites_created: 0, time_spent: 5, period: :month, offset: applicable_offsets[:month])

      get :index, period: :month, format: :json

      assert_response :ok

      resp = get_json_response(response)

      assert_equal 5, resp["smartbites_score"]
      assert_equal 0, resp["smartbites_created"]
      assert_equal 1, resp["smartbites_consumed"]
      assert_equal 5, resp["time_spent"]
    end
  end

  # trend
  test ':api get trend for n number of days' do
    Timecop.freeze do
      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(Time.now.utc)

      # Create data for 30 days
      ((applicable_offsets[:day]-30+1)..applicable_offsets[:day]).each_with_index do |offset, indx|
        create(:user_level_metric, period: :day, offset: offset, user_id: @user.id, smartbites_score: indx, smartbites_consumed: indx*10, smartbites_created: indx*20, time_spent: indx*100)
      end

      get :trend, period: :day, lookback: 20, format: :json
      assert_response :ok

      resp = get_json_response(response)

      response_metrics = resp["metrics"]
      assert_equal (10..29).to_a, response_metrics["smartbites_score"]
      assert_equal (10..29).map{|i| i * 10}, response_metrics["smartbites_consumed"]
      assert_equal (10..29).map{|i| i * 20}, response_metrics["smartbites_created"]
      assert_equal (10..29).map{|i| i * 100}, response_metrics["time_spent"]
    end
  end
end

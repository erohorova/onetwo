require 'test_helper'

class Api::V2::VotesControllerTest < ActionController::TestCase
  setup do
    @user = create(:user)
    @org = @user.organization
    api_v2_sign_in(@user)
  end

  test ':api create video stream card upvote' do
    video_stream = create(:iris_video_stream, organization: @org, creator_id: nil)
    assert_difference -> { Vote.count } do
      post :create, vote: {content_id: video_stream.card.id, content_type: 'Card' }
      assert_response :ok
    end
    vote = Vote.last
    assert_equal @user.id, vote.voter.id
    assert_equal video_stream.card, vote.votable
  end

  test ':api create card upvote' do
    author = create(:user, organization: @org)
    card = create(:card, author: author)
    assert_difference -> { Vote.count } do
      post :create, vote: {content_id: card.id, content_type: 'Card' }
      assert_response :ok
    end

    vote = Vote.last
    assert_equal @user.id, vote.voter.id
    assert_equal card, vote.votable
    assert_equal 1, card.reload.votes_count
  end

  test 'create activity stream upvote' do
    user = create(:user, organization: @org)
    card = create(:card, author: user)
    activity_stream = create(:activity_stream, user: user, organization: user.organization, streamable: card)

    voter = @user

    api_v2_sign_in voter

    assert_differences [[-> {Vote.where(user_id: voter.id, votable: activity_stream).count}, 1],
                          [-> {activity_stream.votes_count}, 1]] do
      post :create, vote: { content_id: activity_stream.id, content_type: 'ActivityStream' }
      assert_response :ok
      activity_stream.reload
    end
  end

  test ':api create card upvote with authorization' do
    Role.any_instance.unstub(:create_role_permissions)
    @org.update enable_role_based_authorization: true
    Role.create_standard_roles @org

    @user.roles << @org.roles.find_by_name('member')

    card = create(:card, organization_id: @org.id, author_id: nil)
    assert_difference -> { Vote.count } do
      post :create, vote: {content_id: card.id, content_type: 'Card' }
      assert_response :ok
    end

    vote = Vote.last
    assert_equal @user.id, vote.voter.id
    assert_equal card, vote.votable
    assert_equal 1, card.reload.votes_count
    assert card.voted_by?(@user)

    curate_only_user = create(:user, organization: @org)
    curate_only_user.role_ids = [@org.roles.where(name: Role::TYPE_CURATOR).first.id]
    curate_only_user.save
    api_v2_sign_in(curate_only_user)
    assert_no_difference -> { Vote.count } do
      post :create, vote: {content_id: card.id, content_type: 'Card' }
      assert_response :unauthorized
    end
  end

  test 'create card upvote with ecl_id' do
    card = create(:card, ecl_id: "123abcd", organization_id: @org.id, author_id: nil)
    assert_difference -> { Vote.count } do
      post :create, vote: {content_id: "ECL-123abcd", content_type: 'Card' }
      assert_response :ok
    end

    assert_difference -> { Vote.count }, -1 do
      delete :destroy, vote: {content_id: "ECL-123abcd", content_type: 'Card' }
      assert_response :ok
    end
  end

  test 'create validation error' do
    card = create(:card, organization_id: @org.id, author_id: nil)
    vote = create(:vote, votable: card, voter: @user)
    assert_no_difference -> { Vote.count } do
      post :create, vote: {content_id: card.id, content_type: 'Card' }
      assert_response :unprocessable_entity
      resp = get_json_response(response)
      assert_match /User has already upvoted this item/, resp["message"]
    end
  end

  test ':api create comment upvote' do
    card = create(:card, organization_id: @org.id, author_id: nil)
    comment = create(:comment, commentable: card, user_id: @user.id)
    assert_difference -> { Vote.count } do
      post :create, vote: {content_id: comment.id, content_type: 'Comment' }
      assert_response :ok
    end

    vote = Vote.last
    assert_equal @user.id, vote.voter.id
    assert_equal comment, vote.votable
  end

  test ':api destroy' do
    video_stream = create(:iris_video_stream, organization_id: @org.id, creator_id: nil)
    vote = create(:vote, voter: @user, votable: video_stream)
    assert_difference -> { Vote.count }, -1 do
      delete :destroy, vote: {content_id: video_stream.id, content_type: "VideoStream"}, format: :json
      assert_response :ok
    end

    refute Vote.where(votable: @video_stream, voter: @user).present?
    refute video_stream.voted_by?(@user)
  end

  test ':api destroy should render not_found' do
    video_stream = create(:iris_video_stream, organization_id: @org.id, creator_id: nil)
    delete :destroy, vote: {content_id: video_stream.id, content_type: "VideoStream"}, format: :json
    assert_response :not_found
  end

  test 'destroy activity stream upvote' do
    user = create(:user, organization: @org)
    card = create(:card, author: user)
    activity_stream = create(:activity_stream, user: user, organization: user.organization, streamable: card)

    voter = @user
    vote = create(:vote, votable: activity_stream, voter: voter)

    api_v2_sign_in voter

    assert_differences [[-> {Vote.where(user_id: voter.id, votable: activity_stream).count}, -1],
                        [-> {activity_stream.votes_count}, -1]] do
      delete :destroy, vote: { content_id: activity_stream.id, content_type: 'ActivityStream' }
      assert_response :ok
      activity_stream.reload
    end
  end

end

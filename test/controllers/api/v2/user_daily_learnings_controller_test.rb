require 'test_helper'

class Api::V2::UserDailyLearningsControllerTest < ActionController::TestCase
  # Should process current day learning once exceeds (User time zone/ 6 AM PST default) else refer last day learning

  PST_1130_PM = ActiveSupport::TimeZone["Pacific Time (US & Canada)"].parse("2017-03-01 23:30:00")
  PST_0530_AM = ActiveSupport::TimeZone["Pacific Time (US & Canada)"].parse("2017-03-02 05:30:00")
  PST_0630_AM = ActiveSupport::TimeZone["Pacific Time (US & Canada)"].parse("2017-03-02 06:30:00")

  IST_1130_PM = ActiveSupport::TimeZone["Asia/Kolkata"].parse("2017-03-01 23:30:00")
  IST_0530_AM = ActiveSupport::TimeZone["Asia/Kolkata"].parse("2017-03-02 05:30:00")
  IST_0630_AM = ActiveSupport::TimeZone["Asia/Kolkata"].parse("2017-03-02 06:30:00")

  setup do
    Organization.any_instance.stubs(:get_settings_for).with('NumberOfDailyLearningItems').returns 5
    Organization.any_instance.stubs(:get_settings_for).with('filestack_url_expire_after_seconds').returns 20
    Organization.any_instance.stubs(:get_settings_for)
    .with('auto_mark_as_complete_scorm_cards').returns false
    Organization.any_instance.stubs(:get_settings_for)
    .with('reporting_content').returns false
    Organization.any_instance.stubs(:get_settings_for)
    .with('feature/shared_channels').returns false

    @org = create(:organization)
    create(:config, name:'web_session_timeout', configable: @org, value: 10, data_type: 'integer', category: nil)
    @user = create(:user, organization: @org)
    @author = create(:user, organization: @org)
    api_v2_sign_in(@user)

    Organization.any_instance.stubs(:current_todays_learning).returns('v2')
    Organization.any_instance.stubs(:web_session_timeout).returns(10)
    Organization.any_instance.stubs(:inactive_web_session_timeout).returns(10)
    Organization.any_instance.stubs(:get_settings_for).with('duration_format').returns nil
  end

  test ':api index' do
    Timecop.freeze(PST_1130_PM) do
      UserDailyLearning.expects(:initialize_and_start).never
      cards = create_list(:card, 5, author: @author)
      create(
        :assignment,
        assignable: cards.first,
        assignee: @user,
        due_at: Time.now
      )
      create(
        :card_subscription,
        card_id: cards.first.id,
        user_id: @user.id,
        end_date: Time.now
      )
      daily_learning = create(:user_daily_learning, user: @user, date: Time.now.in_time_zone("Pacific Time (US & Canada)").strftime("%F"))
      daily_learning.card_ids = cards.collect(&:id)
      daily_learning.processing_state = DailyLearningProcessingStates::COMPLETED
      daily_learning.save
      get :index, fields: 'id,due_at,subscription_end_date', format: :json
      assert_response :ok
      assert assigns(:cards)
      json_response = get_json_response(response)
      assert_same_elements cards.collect(&:id), json_response['cards'].map { |c| c['id'].to_i }
      assert_equal DailyLearningProcessingStates::COMPLETED, json_response['state']
      assert json_response['cards'].first.key?('due_at' && 'subscription_end_date')
    end
  end

  test ':api should filter cards by user language' do
    Timecop.freeze(PST_1130_PM) do
      @user.create_profile(language: 'ru')
      not_specified_card = create(:card, author: @author)
      ru_card = create(:card, language: 'ru', author: @author)
      es_card = create(:card, language: 'es', author: @author)
      daily_learning = create(:user_daily_learning, user: @user, date: Time.now.in_time_zone("Pacific Time (US & Canada)").strftime("%F"))
      daily_learning.card_ids = [not_specified_card.id, ru_card.id, es_card.id]
      daily_learning.processing_state = DailyLearningProcessingStates::COMPLETED
      daily_learning.save
      get :index, filter_by_language: true,  format: :json
      assert_response :ok
      json_response = get_json_response(response)
      assert_same_elements [not_specified_card.id, ru_card.id], json_response['cards'].map { |c| c['id'].to_i }
      assert_equal DailyLearningProcessingStates::COMPLETED, json_response['state']
    end
  end

  test 'should not include dismissed cards index' do
    Timecop.freeze(PST_1130_PM) do
      UserDailyLearning.expects(:initialize_and_start).never
      cards = create_list(:card, 5, author: @author)
      daily_learning = create(:user_daily_learning, user: @user, date: Time.now.in_time_zone("Pacific Time (US & Canada)").strftime("%F"))
      daily_learning.card_ids = cards.collect(&:id)
      daily_learning.processing_state = DailyLearningProcessingStates::COMPLETED
      daily_learning.save

      dismiss_card = cards.last
      DismissedContent.create(content: dismiss_card, user_id: @user.id)
      get :index, format: :json
      assert_response :ok
      assert assigns(:cards)
      json_response = get_json_response(response)
      assert_equal 4, json_response['cards'].length
      assert_not_includes json_response['cards'].map { |c| c['id'].to_i }, dismiss_card.id
    end
  end

  test 'should not include bookmarked cards index' do
    UserContentsQueue.stubs(:remove_from_queue)
    Timecop.freeze(PST_1130_PM) do
      UserDailyLearning.expects(:initialize_and_start).never
      cards = create_list(:card, 5, author: @author)
      daily_learning = create(:user_daily_learning, user: @user, date: Time.now.in_time_zone("Pacific Time (US & Canada)").strftime("%F"))
      daily_learning.card_ids = cards.collect(&:id)
      daily_learning.processing_state = DailyLearningProcessingStates::COMPLETED
      daily_learning.save

      bookmarked_card = cards.last
      create(:bookmark, bookmarkable: bookmarked_card, user_id: @user.id)
      get :index, format: :json
      assert_response :ok
      assert assigns(:cards)
      json_response = get_json_response(response)
      assert_equal 4, json_response['cards'].length
      assert_not_includes json_response['cards'].map { |c| c['id'].to_i }, bookmarked_card.id
    end
  end

  test 'should not include completed cards index' do
    Timecop.freeze(PST_1130_PM) do
      UserDailyLearning.expects(:initialize_and_start).never
      cards = create_list(:card, 5, author: @author)
      daily_learning = create(:user_daily_learning, user: @user, date: Time.now.in_time_zone("Pacific Time (US & Canada)").strftime("%F"))
      daily_learning.card_ids = cards.collect(&:id)
      daily_learning.processing_state = DailyLearningProcessingStates::COMPLETED
      daily_learning.save

      completed_card = cards.last
      create(:user_content_completion, completable: completed_card, user_id: @user.id)
      get :index, format: :json
      assert_response :ok
      assert assigns(:cards)
      json_response = get_json_response(response)
      assert_equal 4, json_response['cards'].length
      assert_not_includes json_response['cards'].map { |c| c['id'].to_i }, completed_card.id
    end
  end

  test 'index when no daily learning' do
    Timecop.freeze do
      UserDailyLearning.expects(:initialize_and_start)
      get :index, format: :json
      assert_response :ok
      assert assigns(:cards)
      json_response = get_json_response(response)
      assert_equal DailyLearningProcessingStates::INITIALIZED, json_response['state']
      assert_equal 0, json_response['cards'].length
    end
  end

  test 'index should not return cards that the user does not have access to' do
    Timecop.freeze(PST_1130_PM) do
      UserDailyLearning.expects(:initialize_and_start).never
      author = create(:user, organization: @org)
      cards = create_list(:card, 3, author: author)

      daily_learning = create(:user_daily_learning, user: @user, date: Time.now.in_time_zone("Pacific Time (US & Canada)").strftime("%F"))
      daily_learning.card_ids = cards.collect(&:id)
      daily_learning.processing_state = DailyLearningProcessingStates::COMPLETED
      daily_learning.save

      user = create(:user, organization: @org)
      cards[0].update(is_public: false)
      CardUserShare.create(user_id: user.id, card_id: cards[0].id)

      get :index, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal 2, resp['cards'].count
      assert_same_elements [cards[1].id, cards[2].id], resp['cards'].map{|d| d['id'].to_i}
      assert_not_includes resp['cards'].map{|d| d['id'].to_i}, cards[0].id
    end
  end

  test 'index should not process cards for visibility if logged in user is the admin' do
    Timecop.freeze(PST_1130_PM) do
      UserDailyLearning.expects(:initialize_and_start).never
      author = create(:user, organization: @org)
      cards = create_list(:card, 2, author: author)

      admin_user = create(:user, organization_role: 'admin', organization: @org)
      daily_learning = create(:user_daily_learning, user: admin_user, date: Time.now.in_time_zone("Pacific Time (US & Canada)").strftime("%F"))
      daily_learning.card_ids = cards.collect(&:id)
      daily_learning.processing_state = DailyLearningProcessingStates::COMPLETED
      daily_learning.save
      api_v2_sign_in admin_user
      cards.expects(:visible_to_userV2).never
      get :index, format: :json
      assert_response :ok
    end
  end

  test ':api availability' do
    get :available, format: :json
    assert_response :ok
    json_response = get_json_response(response)
    assert_equal DailyLearningProcessingStates::COMPLETED, json_response['state']
  end

  test ':api search_user_daily_learnings' do
    setup_search_user_daily_learnings
    # For every domain, return the resp. cards
    Taxonomies.domain_topics.each_with_index do |topic, index|
      TodaysLearning::V2.any_instance.expects(:do_search)
        .with(HashWithIndifferentAccess.new(
          { q: "#{topic['topic_label']}",
            exclude_ecl_card_id: [],
            rank: false,
            load_tags: false,
            content_type: %w(pathway poll article video insight video_stream course) }
        )).returns([@cfd[index], nil])
    end
    @cfd.each do |block|
      block.each do |card|
        create(
          :assignment,
          assignable: card,
          assignee: @user,
          due_at: Time.now
        )
        create(
          :card_subscription,
          card_id: card.id,
          user_id: @user.id,
          end_date: Date.parse("#{Date.current.year}/06/01")
        )
      end
    end

    get :search_user_daily_learnings, fields: 'id,state,due_at,subscription_end_date,paid_by_user', format: :json

    assert_response :ok
    assert assigns(:cards)
    json_response = get_json_response(response)
    cards = json_response['cards']
    card_ids = cards.map {|x| x['id'].to_i}
    assert Set.new(@cfd[0].map{|x| x.id}).intersect?(Set.new(card_ids))
    assert Set.new(@cfd[1].map{|x| x.id}).intersect?(Set.new(card_ids))
    assert Set.new(@cfd[2].map{|x| x.id}).intersect?(Set.new(card_ids))
    assert_equal DailyLearningProcessingStates::COMPLETED, json_response['state']
    assert cards.all? { |card| card.key?('subscription_end_date' && 'due_at' && 'paid_by_user') }
  end

  test ':api search_user_daily_learnings should filter cards by user language' do
    setup_search_user_daily_learnings
    # For every domain, return the resp. cards
    Taxonomies.domain_topics.each_with_index do |topic, index|
      if (0..1).include? index
        @cfd[index].first.update_attributes(language: 'ru')
      else
        @cfd[index].first.update_attributes(language: 'es')
      end
      TodaysLearning::V2.any_instance.expects(:do_search).with(HashWithIndifferentAccess.new({q: "#{topic['topic_label']}",
                                                                                              exclude_ecl_card_id: [], rank: false, load_tags: false, content_type: %w(pathway poll article video insight video_stream course)}))
        .returns([@cfd[index], nil])
    end

    get :search_user_daily_learnings, filter_by_language: true, format: :json

    assert_response :ok
    json_response = get_json_response(response)
    card_ids = json_response['cards'].map {|x| x['id'].to_i}
    assert_not_includes card_ids, json_response['cards'].select { |x| x['language'] == 'es' }
  end

  test 'should not include dismissed cards search_user_daily_learnings' do
    setup_search_user_daily_learnings

    # Just have one domain topic
    @profile.update(learning_topics: [Taxonomies.domain_topics[0]])

    dismiss_card = @cfd[0].last
    DismissedContent.create(content: dismiss_card, user_id: @user.id)

    topic_label = Taxonomies.domain_topics[0]['topic_label']
    TodaysLearning::V2.any_instance.expects(:do_search).with(HashWithIndifferentAccess.new({q: "#{topic_label}",
                                         exclude_ecl_card_id: [dismiss_card.ecl_id], rank: false, load_tags: false,
                                         content_type: %w(pathway poll article video insight video_stream course)}))

    get :search_user_daily_learnings, format: :json
  end

  test 'should not include bookmarked cards search_user_daily_learnings' do
    setup_search_user_daily_learnings

    UserContentsQueue.stubs(:remove_from_queue)

    # Just have one domain topic
    @profile.update(learning_topics: [Taxonomies.domain_topics[0]])

    bookmarked_card = @cfd[0].last
    create(:bookmark, bookmarkable: bookmarked_card, user_id: @user.id)

    topic_label = Taxonomies.domain_topics[0]['topic_label']
    TodaysLearning::V2.any_instance.expects(:do_search).with(HashWithIndifferentAccess.new({q: "#{topic_label}",
                                         exclude_ecl_card_id: [bookmarked_card.ecl_id], rank: false, load_tags: false,
                                         content_type: %w(pathway poll article video insight video_stream course)}))

    get :search_user_daily_learnings, format: :json
  end

  test 'should not include completed cards search_user_daily_learnings' do
    setup_search_user_daily_learnings

    # Just have one domain topic
    @profile.update(learning_topics: [Taxonomies.domain_topics[0]])

    completed_card = @cfd[0].last
    create(:user_content_completion, completable: completed_card, user_id: @user.id)

    topic_label = Taxonomies.domain_topics[0]['topic_label']
    TodaysLearning::V2.any_instance.expects(:do_search).with(HashWithIndifferentAccess.new({q: "#{topic_label}",
                                         exclude_ecl_card_id: [completed_card.ecl_id], rank: false, load_tags: false,
                                         content_type: %w(pathway poll article video insight video_stream course)}))

    get :search_user_daily_learnings, format: :json
  end

  test 'should not include duplicate cards search_user_daily_learnings' do
    setup_search_user_daily_learnings

    # Just have one domain topic
    @profile.update(learning_topics: [Taxonomies.domain_topics[0], Taxonomies.domain_topics[1]])

    topic_label = Taxonomies.domain_topics[0]['topic_label']

    TodaysLearning::V2.any_instance.expects(:do_search).with(HashWithIndifferentAccess.new({q: "#{topic_label}",
                                         exclude_ecl_card_id: [], rank: false, load_tags: false,
                                         content_type: %w(pathway poll article video insight video_stream course)}))
                                   .returns([@cfd[0], nil])

    topic_label = Taxonomies.domain_topics[1]['topic_label']
    domain_label = Taxonomies.domain_topics[1]['domain_label']
    TodaysLearning::V2.any_instance.expects(:do_search).with(HashWithIndifferentAccess.new({q: "#{topic_label}",
                                         exclude_ecl_card_id: [], rank: false, load_tags: false,
                                         content_type: %w(pathway poll article video insight video_stream course)}))
                                   .returns([@cfd[0], nil])

    get :search_user_daily_learnings, format: :json

    json_response = get_json_response(response)

    card_ids = json_response['cards'].map { |c| c['id'].to_i }
    assert_same_elements card_ids.uniq, card_ids
  end

  test 'should have diverse card types search_user_daily_learnings' do
    setup_search_user_daily_learnings

    # course, media, pack, poll, video_stream
    first_interest_cards = [
      create(:card, author: @author, ecl_id: SecureRandom.hex, card_type: 'course'),
      create(:card, author: @author, ecl_id: SecureRandom.hex, card_type: 'course'),
      create(:card, author: @author, ecl_id: SecureRandom.hex, card_type: 'course'),
      create(:card, author: @author, ecl_id: SecureRandom.hex, card_type: 'media'),
      create(:card, author: @author, ecl_id: SecureRandom.hex, card_type: 'media')
    ]

    second_interest_cards = [
      create(:card, author: @author, ecl_id: SecureRandom.hex, card_type: 'pack'),
      create(:card, author: @author, ecl_id: SecureRandom.hex, card_type: 'pack'),
      create(:card, author: @author, ecl_id: SecureRandom.hex, card_type: 'pack'),
      create(:card, author: @author, ecl_id: SecureRandom.hex, card_type: 'poll'),
      create(:card, author: @author, ecl_id: SecureRandom.hex, card_type: 'poll')
    ]

    # Just have one domain topic
    @profile.update(learning_topics: [Taxonomies.domain_topics[0], Taxonomies.domain_topics[1]])

    topic_label = Taxonomies.domain_topics[0]['topic_label']
    TodaysLearning::V2.any_instance.expects(:do_search).with(HashWithIndifferentAccess.new({q: "#{topic_label}",
                                                                        exclude_ecl_card_id: [], rank: false, load_tags: false,
                                                                        content_type: %w(pathway poll article video insight video_stream course)}))
                                   .returns([first_interest_cards, nil])

    topic_label = Taxonomies.domain_topics[1]['topic_label']
    TodaysLearning::V2.any_instance.expects(:do_search).with(HashWithIndifferentAccess.new({q: "#{topic_label}",
                                                                        exclude_ecl_card_id: [], rank: false, load_tags: false,
                                                                        content_type: %w(pathway poll article video insight video_stream course)}))
                                   .returns([second_interest_cards, nil])

    get :search_user_daily_learnings, format: :json

    json_response = get_json_response(response)

    card_types = json_response['cards'].map { |c| c['card_type'] }
    assert_equal 4, card_types.uniq.count
  end

  test 'should offset depending on date search_user_daily_learnings' do
    setup_search_user_daily_learnings

    cards = mock
    cards.expects(:count).returns(10).at_least_once
    cards.expects(:slice).with(0, 5).returns(@cfd[0].slice(0, 5))
    cards.expects(:slice).with(5, 5).returns(@cfd[0].slice(0, 5)).twice
    TodaysLearning::V2.any_instance.expects(:get_todays_learning_items).returns(cards).at_least_once

    Timecop.freeze(Time.utc(2017,5,1,0,0,0)) do
      get :search_user_daily_learnings, format: :json
    end

    Timecop.freeze(Time.utc(2017,5,2,0,0,0)) do
      get :search_user_daily_learnings, format: :json
    end

    Timecop.freeze(Time.utc(2017,5,12,0,0,0)) do
      get :search_user_daily_learnings, format: :json
    end
  end

  test 'should exclude cards already interacted with search_user_daily_learnings' do
    setup_search_user_daily_learnings
    cards = @cfd[0].slice(0, 5)
    TodaysLearning::V2.any_instance.expects(:get_todays_learning_items).returns(cards).at_least_once

    dismiss_card = @cfd[0].last
    DismissedContent.create(content: dismiss_card, user_id: @user.id)

    get :search_user_daily_learnings, format: :json

    json_response = get_json_response(response)
    assert_equal 4, json_response['cards'].length

    card_ecl_ids = json_response['cards'].map{|card| card['ecl_id']}
    assert_not card_ecl_ids.include?(dismiss_card.ecl_id)
  end

  test 'should reload cards from database search_user_daily_learnings' do
    # Cards are not persisted since they're from ECL
    cards = (1..5).map do
      build(:card, ecl_id: SecureRandom.hex, organization: @org)
    end

    # Now, persist one of the cards from above, giving it the same ecl_id
    persisted_card = create(:card, organization: @org, author: nil, ecl_id: cards[0].ecl_id)

    # Controller gets cards from ECL, but some of them might already exist in destiny
    TodaysLearning::V2.any_instance.expects(:get_todays_learning_items).returns(cards).at_least_once

    get :search_user_daily_learnings, format: :json
    json_response = get_json_response(response)

    card_ids = json_response['cards'].map{|card| card['id']}
    assert_equal [persisted_card.id.to_s, "ECL-#{cards[1].ecl_id}", "ECL-#{cards[2].ecl_id}",
                  "ECL-#{cards[3].ecl_id}", "ECL-#{cards[4].ecl_id}"], card_ids
  end

  test 'should read set value for learning items count from config' do
    Organization.any_instance.unstub(:get_settings_for)

    org1 = create :organization
    config = create :config, name:'NumberOfDailyLearningItems', data_type: 'integer', value: 21,
      configable_type: 'Organization', configable_id: org1.id

    assert_equal 21, org1.number_of_learning_items
  end

  test 'should cache valid results' do
    setup_search_user_daily_learnings

    User.any_instance.expects(:learning_topics).returns([{topic_label: 'hello'}]).twice
    learning_keywords = (@user.learning_topics || []).map{|x| x['topic_label']}
    interests_hash = Digest::SHA256.hexdigest learning_keywords.join('|').slice(0,6)
    cache_key = "search_user_daily_learnings/#{@user.id}/#{interests_hash}"

    cards = mock
    cards.expects(:count).returns(10).at_least_once
    cards.expects(:slice).with(0, 5).returns(@cfd[0].slice(0, 5))

    TodaysLearning::V2.any_instance.expects(:get_todays_learning_items).returns(cards).at_least_once

    Rails.cache.expects(:fetch).with(cache_key).returns([])
    Rails.cache.expects(:write).with(cache_key, cards, expires_in: 3.days)
    Rails.cache.expects(:fetch).with(@org.send(:trashed_ecl_ids_cache_key), { expires_in: 30.days }).returns([])

    Timecop.freeze(Time.utc(2017,5,1,0,0,0)) do
      get :search_user_daily_learnings, format: :json
    end
  end

  test 'should NOT cache empty results' do
    setup_search_user_daily_learnings

    # Controller gets cards from ECL, but some of them might already exist in destiny
    TodaysLearning::V2.any_instance.expects(:get_todays_learning_items).returns([]).at_least_once

    Rails.cache.expects(:write).never

    Timecop.freeze(Time.utc(2017,5,1,0,0,0)) do
      get :search_user_daily_learnings, format: :json
    end
  end

  test 'should NOT re-fetch if cache has content' do
    setup_search_user_daily_learnings

    cards = mock
    cards.expects(:count).returns(10).at_least_once
    cards.expects(:slice).with(0, 5).returns(@cfd[0].slice(0, 5))

    User.any_instance.expects(:learning_topics).returns([{topic_label: 'hello'}]).twice
    learning_keywords = (@user.learning_topics || []).map{|x| x['topic_label']}
    interests_hash = Digest::SHA256.hexdigest learning_keywords.join('|').slice(0,6)
    cache_key = "search_user_daily_learnings/#{@user.id}/#{interests_hash}"

    TodaysLearning::V2.any_instance.expects(:get_todays_learning_items).returns(cards).never

    Rails.cache.expects(:fetch).with(cache_key).returns(cards)
    Rails.cache.expects(:fetch).with(@org.send(:trashed_ecl_ids_cache_key), { expires_in: 30.days }).returns([])

    Timecop.freeze(Time.utc(2017,5,1,0,0,0)) do
      get :search_user_daily_learnings, format: :json
    end
  end

  test 'should re-fetch if cache has blank content' do
    setup_search_user_daily_learnings

    cards = mock
    cards.expects(:count).returns(10).at_least_once
    cards.expects(:slice).with(0, 5).returns(@cfd[0].slice(0, 5))

    User.any_instance.expects(:learning_topics).returns([{topic_label: 'hello'}]).twice
    learning_keywords = (@user.learning_topics || []).map{|x| x['topic_label']}
    interests_hash = Digest::SHA256.hexdigest learning_keywords.join('|').slice(0,6)
    cache_key = "search_user_daily_learnings/#{@user.id}/#{interests_hash}"

    TodaysLearning::V2.any_instance.expects(:get_todays_learning_items).returns(cards).at_least_once

    Rails.cache.expects(:fetch).with(cache_key).returns([])
    Rails.cache.expects(:fetch).with(@org.send(:trashed_ecl_ids_cache_key), { expires_in: 30.days }).returns([])

    Timecop.freeze(Time.utc(2017,5,1,0,0,0)) do
      get :search_user_daily_learnings, format: :json
    end
  end

  test '#search_user_daily_learnings adjusts random offsets and returns cards appropriately for day 1' do
    setup_search_user_daily_learnings

    User.any_instance.expects(:learning_topics).returns([{topic_label: 'hello'}]).twice
    learning_keywords = (@user.learning_topics || []).map{|x| x['topic_label']}
    interests_hash = Digest::SHA256.hexdigest learning_keywords.join('|').slice(0,6)
    cache_key = "search_user_daily_learnings/#{@user.id}/#{interests_hash}"

    Rails.cache.expects(:fetch).with(cache_key).returns([])
    Rails.cache.expects(:fetch).with(@org.send(:trashed_ecl_ids_cache_key), { expires_in: 30.days }).returns([])

    # Day 1 - offset is 0
    Timecop.freeze(Time.utc(2017, 5, 1, 0, 0, 0)) do
      set_of_cards = (@cfd[0] + @cfd[1] + @cfd[2])
      set_of_cards.expects(:slice).with(0, 5).returns(set_of_cards[0..4]).once

      TodaysLearning::V2.any_instance.expects(:get_todays_learning_items).returns(set_of_cards).at_least_once

      get :search_user_daily_learnings, format: :json

      json_response = get_json_response(response)
      assert_equal set_of_cards[0..4].map { |c| c['id'].to_s }.to_set, json_response['cards'].collect { |c| c['id'] }.to_set
    end
  end

  test '#search_user_daily_learnings adjusts random offsets and returns cards appropriately for day 2' do
    setup_search_user_daily_learnings

    User.any_instance.expects(:learning_topics).returns([{topic_label: 'hello'}]).twice
    learning_keywords = (@user.learning_topics || []).map{|x| x['topic_label']}
    interests_hash = Digest::SHA256.hexdigest learning_keywords.join('|').slice(0,6)
    cache_key = "search_user_daily_learnings/#{@user.id}/#{interests_hash}"

    Rails.cache.expects(:fetch).with(cache_key).returns([])
    Rails.cache.expects(:fetch).with(@org.send(:trashed_ecl_ids_cache_key), { expires_in: 30.days }).returns([])

    # Day 2 - offset is 5
    Timecop.freeze(Time.utc(2017, 5, 2, 0, 0, 0)) do
      set_of_cards = (@cfd[0] + @cfd[1] + @cfd[2] + @cfd[0])
      set_of_cards.expects(:slice).with(5, 5).returns(set_of_cards[5..9]).once

      TodaysLearning::V2.any_instance.expects(:get_todays_learning_items).returns(set_of_cards).at_least_once

      get :search_user_daily_learnings, format: :json

      json_response = get_json_response(response)
      assert_equal set_of_cards[5..9].map { |c| c['id'].to_s }.to_set, json_response['cards'].collect { |c| c['id'] }.to_set
    end
  end

  test '#search_user_daily_learnings detects TL version from organization config' do
    # when TL version is set to `v2`
    setup_search_user_daily_learnings

    Organization.any_instance.stubs(:current_todays_learning).returns('v2')

    cards = mock
    cards.expects(:count).returns(10).at_least_once
    cards.expects(:slice).with(0, 5).returns(@cfd[0].slice(0, 5))

    User.any_instance.expects(:learning_topics).returns([{topic_label: 'hello'}]).twice
    learning_keywords = (@user.learning_topics || []).map{|x| x['topic_label']}
    interests_hash = Digest::SHA256.hexdigest learning_keywords.join('|').slice(0,6)
    cache_key = "search_user_daily_learnings/#{@user.id}/#{interests_hash}"

    TodaysLearning::V2.any_instance.expects(:get_todays_learning_items).returns(cards).at_least_once

    Rails.cache.expects(:fetch).with(cache_key).returns([])
    Rails.cache.expects(:fetch).with(@org.send(:trashed_ecl_ids_cache_key), { expires_in: 30.days }).returns([])

    Timecop.freeze(Time.utc(2017,5,1,0,0,0)) do
      get :search_user_daily_learnings, format: :json
    end

    # when TL version is set to `v3`
    Organization.any_instance.stubs(:current_todays_learning).returns('v3')

    ecl_results   = JSON.parse(File.read(File.join('test', 'fixtures', 'files', 'ecl_sociative_results.json')))
    EclApi::EclSearch.any_instance.stubs(:todays_learning).returns(ecl_results)

    TodaysLearning::V3.any_instance.expects(:process).returns(@cfd[0]).at_least_once

    Timecop.freeze(Time.utc(2017,5,1,0,0,0)) do
      get :search_user_daily_learnings, format: :json
    end
  end

  private

  def setup_search_user_daily_learnings
    @profile = create(:user_profile, user: @user, learning_topics: Taxonomies.domain_topics, language: 'ru')

    # Create cards for each of the domain
    @author = create(:user, organization: @org)
    @cfd = Taxonomies.domain_topics.map do |topic|
      (1..5).map do
        create(:card, author: @author, ecl_id: SecureRandom.hex)
      end
    end
  end
end

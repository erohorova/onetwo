require 'test_helper'

class Api::V2::EclControllerTest < ActionController::TestCase

  test "should respond as unauthorized" do
    post :sync_card
    assert_response :unauthorized
  end

  test "if ecl_id not sent should respond as bad request" do
    set_ecl_api_request_header("123")
    post :sync_card
    assert_response 400
  end

  test "if incorrect card details sent then should respond as 400" do
    card = {id: 'sos'}
    set_ecl_api_request_header(card)
    post :sync_card, card

    assert_response 400
  end

  test 'should check organization id for card finding' do
    organization = create(:organization)
    card = create(:card, organization: organization, author_id: nil, ecl_id: 'ecl-test-123')
    ecl_card = JSON.parse(card.to_json)

    set_ecl_api_request_header(ecl_card)
    ecl_card.merge!(source_logo_url: "http://www.abc.com/xyz.png", organization_id: -1)

    post :sync_card, ecl_card, format: :json
    assert_response 400
    assert_nil card.reload.ecl_metadata
  end

  test "if card details sent then should respond as 200" do
    card = create(:card, ecl_id: 'ecl-test-123')
    ecl_card = JSON.parse(card.to_json)

    set_ecl_api_request_header(ecl_card)

    duration_metadata = {
      "duration_metadata" => {
        "calculated_duration" => "180",
        "calculated_duration_display" => "3 minutes"
      }
    }
    ecl_card.merge!(duration_metadata)

    post :sync_card, ecl_card, format: :json

    assert_response 200
    card.reload
    assert_equal card.ecl_metadata['duration_metadata'], duration_metadata['duration_metadata']
  end

  test "card ecl metadata should store ecl source logo url if provided" do
    card = create(:card, ecl_id: 'ecl-test-123')
    ecl_card = JSON.parse(card.to_json)

    set_ecl_api_request_header(ecl_card)
    ecl_card.merge!(source_logo_url: "http://www.abc.com/xyz.png")

    post :sync_card, ecl_card

    assert_response :success
    card.reload
    assert_equal "http://www.abc.com/xyz.png", card.ecl_metadata["source_logo_url"]
  end

  test "should trigger card deletion job for the source" do
    org = create(:organization)
    card = create(:card, ecl_id: 'ecl-test-123')
    ecl_card = JSON.parse(card.to_json)

    set_ecl_api_request_header(ecl_card)

    file_url = 'http://ecl-test.s3.amazonaws.com/deleted-source-ecl-test-source-123.csv'
    CardDeletionByEclSourceJob.unstub(:perform_later)
    CardDeletionByEclSourceJob.expects(:perform_later).once
    delete :delete_cards_of_source, source_id: 'ecl-test-source-123', organization_id: org.id, file_url: file_url
    assert_response :success
  end

  test "should delete source from ecl sources table if source channel integration is done when source is deleted" do
    org = create(:organization)
    card = create(:card, ecl_id: 'ecl-test-123')
    ecl_card = JSON.parse(card.to_json)
    channel = create(:channel, organization: org)
    ecl_source = create(:ecl_source, ecl_source_id: 'ecl-test-source-123',channel_id: channel.id)

    set_ecl_api_request_header(ecl_card)

    file_url = 'http://ecl-test.s3.amazonaws.com/deleted-source-ecl-test-source-123.csv'
    CardDeletionByEclSourceJob.stubs(:perform_later)
    delete :delete_cards_of_source, source_id: 'ecl-test-source-123', organization_id: org.id, file_url: file_url
    assert_response :success
    assert_equal nil, EclSource.where(ecl_source_id: 'ecl-test-source-123').first
  end

  test "should respond as unauthorized for sync_source_cards" do
    post :sync_source_cards
    assert_response :unauthorized
  end

  test "if source_id not sent should respond as bad request" do
    set_ecl_api_request_header("123")
    post :sync_source_cards
    assert_response 400
  end

  test "should trigger cards update job for the source" do
    org = create(:organization)
    card = create(:card, ecl_id: 'ecl-test-123', ecl_metadata: { source_id: "ecl-test-source-123"})
    ecl_card = JSON.parse(card.to_json)

    set_ecl_api_request_header(ecl_card)

    EclCardUpdateBySource.unstub(:perform_later)
    EclCardUpdateBySource.expects(:perform_later).once
    post :sync_source_cards, source_id: 'ecl-test-source-123', organization_id: org.id, ecl_metadata: { mark_feature_disabled: true }
    assert_response :success
  end

  class TitleMessageSync < ActionController::TestCase

    # card will carry ecl 'name' attribute
    test 'card update when title is blank' do
      card = create(:card, ecl_id: 'ecl-test-123', title: nil, message: "existing message")
      ecl_card = JSON.parse(card.to_json)

      set_ecl_api_request_header(ecl_card)
      ecl_card.merge!(name: "title check", description: "message check")

      post :sync_card, ecl_card

      assert_response :success
      card.reload
      assert_equal "title check", card.title
      assert_equal "title check", card.message
    end

    # card will carry ecl 'name' and 'description' attribute
    test 'card update when title and messsage are different' do
      card = create(:card, ecl_id: 'ecl-test-123', title: "existing title", message: "existing message")
      ecl_card = JSON.parse(card.to_json)

      set_ecl_api_request_header(ecl_card)
      ecl_card.merge!(name: "title check", description: "message check")

      post :sync_card, ecl_card

      assert_response :success
      card.reload
      assert_equal "title check", card.title
      assert_equal "message check", card.message
    end
  end

  class SyncTeamSourcesTest < ActionController::TestCase
    setup do
      @organization = create(:organization)
      @team1 = create(:team, organization: @organization)
      @team2 = create(:team, organization: @organization, source_ids: ['source-123-abc'])
    end

    test "should add source to team" do
      payload = {new_team_ids: [@team1.id], source_id: 'source-123-abc', organization_id: @organization.id}
      set_ecl_api_request_header(payload)

      post :sync_team_sources, payload

      assert_response :success
      assert_same_elements ['source-123-abc'], @team1.reload.source_ids
    end

    test "should delete source from team" do
      payload = {del_team_ids: [@team2.id], source_id: 'source-123-abc', organization_id: @organization.id}
      set_ecl_api_request_header(payload)

      post :sync_team_sources, payload

      assert_response :success
      assert_empty @team2.reload.source_ids
    end

    test "should add/delete source from teams" do
      payload = {new_team_ids: [@team1.id], del_team_ids: [@team2.id], source_id: 'source-123-abc', organization_id: @organization.id}
      set_ecl_api_request_header(payload)

      post :sync_team_sources, payload

      assert_response :success
      assert_same_elements ['source-123-abc'], @team1.reload.source_ids
      assert_empty @team2.reload.source_ids
    end

    test "should render bad request if source and organization params missing" do
      payload = {new_team_ids: [@team1.id], del_team_ids: [@team2.id]}
      set_ecl_api_request_header(payload)

      post :sync_team_sources, payload

      assert_response :bad_request
    end
  end

  class DeleteCard < ActionController::TestCase
    test "should delete ugc card" do
      user = create(:user)
      card = create(:card, ecl_id: 'ugc-123', organization_id: user.organization_id, author_id: user.id)
      ecl_card = JSON.parse(card.to_json)
      set_ecl_api_request_header(ecl_card)

      EclCardArchiveJob.expects(:perform_later).with('ugc-123', user.organization_id).never
      assert_difference ->{Card.count}, -1 do
        delete :destroy, id: 'ugc-123'
        assert_response :success
      end
    end

    test "should delete non ugc card" do
      org = create(:organization)
      card = create(:card, ecl_id: 'non-ugc-123', organization_id: org.id, author_id: nil)
      ecl_card = JSON.parse(card.to_json)
      set_ecl_api_request_header(ecl_card)

      EclCardArchiveJob.expects(:perform_later).with('non-ugc-123', org.id).never
      assert_difference ->{Card.count}, -1 do
        delete :destroy, id: 'non-ugc-123'
        assert_response :success
      end
    end

    test 'should not delete if card not found' do
      org = create(:organization)
      card = create(:card, ecl_id: 'non-ugc-123', organization_id: org.id, author_id: nil)
      ecl_card = JSON.parse(card.to_json)
      set_ecl_api_request_header(ecl_card)

      assert_no_difference ->{Card.count} do
        delete :destroy, id: 'non-ugc-123-skip'
        assert_response :not_found
      end
    end
  end
end

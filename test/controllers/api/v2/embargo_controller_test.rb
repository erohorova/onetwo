require 'test_helper'

class Api::V2::EmbargoControllerTest < ActionController::TestCase
  setup do
    @org = create(:organization)
    @admin_user = create(:user, organization_role: 'admin', organization: @org)
    api_v2_sign_in(@admin_user)
  end

  class EmbargoControllerCreateTest < Api::V2::EmbargoControllerTest
    test ':api should create array of embargo content' do
      assert_difference -> {Embargo.count}, 3 do
        post :batch_create, values: ['one', 'two', 'three'], category: 'approved_sites', format: :json
      end
      assert_response :ok
    end

    test ':api should not create content with not valid category' do
      post :batch_create, values: ['one', 'two', 'free'], category: 'some_category', format: :json
      assert_response :unprocessable_entity
    end

    test ':api should not create content with the same value within one organization' do
      create(:embargo, value: 'one', category: 'unapproved_words', organization: @org, user: @admin_user)
      assert_no_difference -> {Embargo.count} do
        post :batch_create, values: ['one'], category: 'unapproved_words', format: :json
      end
      resp = get_json_response(response)
      assert_response :ok
      assert_equal resp['message'], 'There were errors when adding the following content: one'
    end

    test 'only admin user can create embargo content' do
      member = create(:user, organization: @org)
      api_v2_sign_in(member)
      post :batch_create, values: ['one'], category: 'approved_sites', format: :json
      assert_response :unauthorized
    end
  end

  class EmbargoControllerIndexTest < Api::V2::EmbargoControllerTest
    setup do
      @approved_sites = create_list(:embargo, 1, category: 'approved_sites', organization: @org, user: @admin_user)
      @unapproved_sites = create_list(:embargo, 2, category: 'unapproved_sites', organization: @org, user: @admin_user)
      @unapproved_words = create_list(:embargo, 3, category: 'unapproved_words', organization: @org, user: @admin_user)
    end

    test ':api should return all embargo content for org' do
      get :index, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal 6, resp['total']
    end

    test ':api should return embargo content with limit' do
      get :index, limit: 2, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal 2, resp.count
    end

    test 'should be able to search embargo by term' do
      embargo = create(:embargo, :unapproved_words, organization: @org, user: @admin_user)
      get :index, q: 'bad_word', format: :json
      resp = get_json_response(response)
      assert_equal 1, resp['total']
      assert_equal embargo.id, resp['embargoes'].first['id']
    end

    test 'should be able to sort by value' do
      create(:embargo, value: 'bb', organization: @org, user: @admin_user, category: 'unapproved_words')
      create(:embargo, value: 'aa', organization: @org, user: @admin_user, category: 'unapproved_words')
      get :index, order: 'asc', order_attr: 'value', format: :json
      resp = get_json_response(response)
      assert_equal 'aa', resp['embargoes'].first['value']
    end


    test 'should be able to filter by category' do
      get :index, category: 'unapproved_sites', format: :json
      resp = get_json_response(response)
      assert_equal @unapproved_sites.count, resp['total']
      resp['embargoes'].each do |e|
        assert_equal 'unapproved_sites', e['category']
      end
    end
  end

  class EmbargoControllerShowTest < Api::V2::EmbargoControllerTest
    test ':api should get detailed info about embargo content' do
      embargo = create(:embargo, :unapproved_words, organization: @org, user: @admin_user)
      get :show, id: embargo.id, format: :json

      assert_response :ok
      resp = get_json_response(response)
      assert_equal 'unapproved_words', resp['category']
      assert_equal 'bad_word', resp['value']
      assert_equal @admin_user.id, resp['user_id']
      assert_equal @admin_user.full_name, resp['added_by']
      assert_equal @org.id, resp['organization_id']
    end
  end

  class EmbargoControllerDestroyTest < Api::V2::EmbargoControllerTest
    test ':api should delete array of embargo content' do
      word1 = create(:embargo, value: 'bb', organization: @org, user: @admin_user, category: 'unapproved_words')
      word2 = create(:embargo, value: 'aa', organization: @org, user: @admin_user, category: 'unapproved_words')
      assert_difference -> {Embargo.count}, -2 do
        post :batch_remove, embargo_ids: [word1.id, word2.id], format: :json
      end
      assert_response :ok
    end
  end

  class EmbargoControllerUpdateTest < Api::V2::EmbargoControllerTest
    test ':api should update value of content' do
      embargo = create(:embargo, :unapproved_words, organization: @org, user: @admin_user)
      assert_equal 'bad_word', embargo.value
      put :update, id: embargo.id, embargo: {value: 'very_very_bad_word', category: 'unapproved_words'}, format: :json
      assert_response :ok
      assert_equal 'very_very_bad_word', embargo.reload.value
    end
  end

  class EmbargoControllerImportTest < Api::V2::EmbargoControllerTest
    setup do
      csv_rows = File.open("#{Rails.root}/test/fixtures/files/embargo.csv").read
      @embargo_file = create(:embargo_file,
                             user_id: @admin_user.id,
                             organization_id: @org.id,
                             data: csv_rows)
    end

    test 'should check whether file url is csv or not before preview' do
      get :preview_csv, csv_file_url: 'https://Sheet1.txt'
      resp = JSON.parse(response.body)
      assert_response :unprocessable_entity
      assert_equal 'Invalid file url extension.', resp['message']
    end

    test 'should check csv headers before preview' do
      stub_request(:get, 'https://Sheet1.csv').
          with(:headers => {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Faraday v0.9.2'}).
          to_return(:status => 200,
                    :body => "SOME_HEADER,category\nWord,unapproved_words\nfacebook.com,approved_sites\ninstagram.com,unapproved_words\nSecond3",
                    :headers => {})

      get :preview_csv, csv_file_url: 'https://Sheet1.csv'
      resp = JSON.parse(response.body)

      assert_equal false, resp['status']
      assert_equal resp['message'], "Invalid CSV headers. Use format value, category"
    end

    test 'should send preview of uploaded csv and contain rows count in response' do
      stub_request(:get, "https://Sheet1.csv").
          with(:headers => {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Faraday v0.9.2'}).
          to_return(:status => 200,
                    :body => "value,category\nWord,unapproved_words\nfacebook.com,approved_sites\ninstagram.com,unapproved_words\nSecond3",
                    :headers => {})


      get :preview_csv, csv_file_url: 'https://Sheet1.csv'
      resp = JSON.parse(response.body)
      assert_equal true, resp['status']
      assert_equal resp['message'], 'CSV is successfully uploaded'
      assert_equal 3, resp['embargoes'].count
      assert_equal 3, resp['rows_total_count']
    end

    test 'should trigger job to import embargo content from the uploaded CSV' do
      BulkImportEmbargoesJob.expects(:perform_later).with({
                                                              file_id: @embargo_file.id}).once
      post :bulk_upload, file_id: @embargo_file.id

      assert_equal Hash.new, JSON.parse(response.body)
    end

    test 'should load specific data from existed file if file_id present' do
      get :preview_csv, file_id: @embargo_file.id, limit: 2, offset: 2, format: :json
      assert_response :ok
      resp = get_json_response(response)

      expected_total = 8
      expected_rows = [
        {value: 'facebook.com', category: 'approved_sites'},
        {value: 'yahoo.com', category: 'approved_sites'}
      ].map(&:with_indifferent_access)

      assert_equal expected_total, resp['rows_total_count']
      assert_equal expected_rows, resp['embargoes']
    end

    test 'should return 404 if file not found' do
      get :preview_csv, file_id: 999, limit: 2, offset: 2, format: :json
      assert_response :not_found
      resp = get_json_response(response)

      assert_equal 'File does not exists', resp['message']
    end
  end
end

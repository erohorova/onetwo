require 'test_helper'

class Api::V2::BookmarksControllerTest < ActionController::TestCase
  setup do
    @user = create(:user)
    @organization = @user.organization

    api_v2_sign_in(@user)
  end

  test ':api should return all bookmarks' do
    card = create(:card, author: @user)
    post :create, bookmark: {content_id: card.id, content_type: 'Card' }
    get :index, format: :json
    json_response = get_json_response(response)
    assert_response :ok
    assert_equal json_response['bookmarks']['cards'].count, @user.bookmarks.count
  end

  test ':api should return all the bookmarked cards of user' do
    card_1 = create(:card, author: @user)
    card_2 = create(:card, author: @user)
    post :create, bookmark: {content_id: card_1.id, content_type: 'Card' }
    post :create, bookmark: {content_id: card_2.id, content_type: 'Card' }
    get :index, format: :json
    json_response = get_json_response(response)
    assert_equal json_response['bookmarks']['cards'].count, @user.bookmarks.count
  end

  test ':api should return bookmarked cards of a user in completed state' do
    card_1 = create(:card, author: @user)
    post :create, bookmark: {content_id: card_1.id, content_type: 'Card' }
    create :learning_queue_item, :from_assignments, queueable: card_1, user: @user, state: "completed"
    get :index, format: :json
    json_response = get_json_response(response)
    assert_equal json_response['bookmarks']['cards'].count, @user.bookmarks.count
  end

  test ':api should not return unbookmarked cards of a user in completed state' do
    card_1 = create(:card, author: @user)
    create :learning_queue_item, :from_assignments, queueable: card_1, user: @user, state: "completed"
    get :index, format: :json
    json_response = get_json_response(response)
    assert_equal json_response['bookmarks']['cards'].count, @user.bookmarks.count
  end

  test ':api should return bookmarked cards of a user in uncompleted state' do
    card_1 = create(:card, author: @user)
    post :create, bookmark: {content_id: card_1.id, content_type: 'Card' }
    create :learning_queue_item, :from_assignments, queueable: card_1, user: @user, state: "active"
    get :index, format: :json
    json_response = get_json_response(response)
    assert_equal json_response['bookmarks']['cards'].count, @user.bookmarks.count
  end

  test ':api should not return unbookmarked cards of a user in uncompleted state' do
    card_1 = create(:card, author: @user)
    create :learning_queue_item, :from_assignments, queueable: card_1, user: @user, state: "active"
    get :index, format: :json
    json_response = get_json_response(response)
    assert_equal json_response['bookmarks']['cards'].count, @user.bookmarks.count
  end

  test ':api should return completed or uncompleted bookmarks of a users' do
    card_1 = create(:card, author: @user)
    card_2 = create(:card, author: @user)
    create :learning_queue_item, :from_assignments, queueable: card_1, user: @user, state: "active"
    create :learning_queue_item, :from_assignments, queueable: card_2, user: @user, state: "completed"
    post :create, bookmark: {content_id: card_1.id, content_type: 'Card' }
    get :index, format: :json
    json_response = get_json_response(response)
    assert_equal json_response['bookmarks']['cards'].count, @user.bookmarks.count
  end

  test ':api should not return completed or uncompleted unbookmarks of a users' do
    card_1 = create(:card, author: @user)
    card_2 = create(:card, author: @user)
    create :learning_queue_item, :from_assignments, queueable: card_1, user: @user, state: "active"
    create :learning_queue_item, :from_assignments, queueable: card_2, user: @user, state: "completed"
    get :index, format: :json
    json_response = get_json_response(response)
    assert_equal json_response['bookmarks']['cards'].count, @user.bookmarks.count
  end

  test 'should not return bookmarked cards the user does not have access to' do
    user = create(:user, organization: @user.organization)
    card_list = create_list(:card, 2, author: user)
    
    card_list.each do |card|
      create(:bookmark, user: @user, bookmarkable: card)
    end
    
    card_list[0].update(is_public: false)
    get :index, format: :json
    json_response = get_json_response(response)
    assert_equal 1, json_response['bookmarks']['cards'].count
    assert_includes  json_response['bookmarks']['cards'].map{|d| d['id'].to_i}, card_list[1].id
    assert_not_includes  json_response['bookmarks']['cards'].map{|d| d['id'].to_i}, card_list[0].id
  end

  test ':api create bookmark for card' do
    author = create(:user, organization: @organization)
    card = create(:card, author: author)
    assert_difference -> { @user.bookmarks.count } do
      post :create, bookmark: {content_id: card.id, content_type: 'Card' }
      assert_response :ok
    end
  end

  test 'authorized user can create bookmark for card #collaborator' do
    @organization.update(enable_role_based_authorization: true)
    Role.create_standard_roles(@organization)

    collaborator_role = @organization.roles.where(name: Role::TYPE_COLLABORATOR).first
    @user.roles << collaborator_role

    card = create(:card, author_id: @user.id)
    assert_no_difference -> { @user.bookmarks.count } do
      post :create, bookmark: {content_id: card.id, content_type: 'Card' }
      assert_response :unauthorized
    end
  end

  test 'completed card on bookmarking should update the source of the learning queue item' do
    card = create(:card, author: @user)
    # marking the card as completed
    completed_item = create(:learning_queue_item, :from_assignments, queueable: card, user: @user, state: "completed")
    assert_equal 'assignments', completed_item.source
    assert_equal 'completed', completed_item.state
    # bookmarking the card now
    assert_difference -> { @user.bookmarks.count } do
      post :create, bookmark: {content_id: card.id, content_type: 'Card' }
      assert_response :ok
    end
    learning_queue_item = LearningQueueItem.find_by(queueable: card)
    assert_equal 'bookmarks', learning_queue_item.source
    assert_equal 'completed', learning_queue_item.state
  end

  test ':api destroy' do
    creator = create(:user, organization: @organization)
    video_stream = create(:iris_video_stream, creator: creator)
    bookmark = create(:bookmark, user: @user, bookmarkable: video_stream.card)
    assert_difference -> { @user.bookmarks.count }, -1 do
      delete :destroy, bookmark: {content_id: video_stream.card.id, content_type: 'Card'}
    end
  end

  #with ECL-id
  test 'create bookmark with ecl_id' do
    card = create(:card, ecl_id: "123abcd", author: nil, organization: @organization)
    assert_difference -> { @user.bookmarks.count } do
      post :create, bookmark: {content_id: "ECL-123abcd", content_type: 'Card' }
      assert_response :ok
    end

    assert_difference -> { @user.bookmarks.count }, -1 do
      delete :destroy, bookmark: {content_id: "ECL-123abcd", content_type: 'Card' }
      assert_response :ok
    end
  end

  test 'should track bookmark action' do
    author = create(:user, organization: @organization)
    card = create(:card, author: author)
    request.cookies[:_d] = 'device123'

    Tracking.expects(:track_act).once

    assert_difference -> { @user.bookmarks.count } do
      post :create, bookmark: {content_id: card.id, content_type: 'Card' }
      assert_response :ok
    end
  end
end

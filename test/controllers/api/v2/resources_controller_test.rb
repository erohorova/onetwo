require 'test_helper'

class Api::V2::ResourcesControllerTest < ActionController::TestCase

  setup do
    @org = create(:organization)
    @user = create(:user, organization: @org)
    @video_url = 'http://videourl123.com'

    stub_request(:get, @video_url).
          to_return(:status => 200,
                    :body => '<html>
                    <head>
                      <title>title</title>
                      <link rel="alternate" type="application/json" />
                      <meta property="og:title" content="og:title">
                      <meta property="og:description" content="og:description">
                      <meta property="og:site_name" content="og:site_name">
                      <meta property="og:image" content="http://imageurl.com">
                      <meta property="og:video:url" content="http://videourl.com">
                      <meta property="og:type" content="video">
                    </head>
                    </html>',
                    :headers => {})
    @video_link = 'http://techslides.com/demos/sample-videos/small.mp4'
    stub_request(:get, @video_link).
        to_return(:status => 200,
                  :headers => {
                    'Content-Type': 'video/mp4'
                  })
    @client = Aws::S3::Client.new(region: 'bucket_region',stub_responses: true)
    @client.stub_responses(
      :list_buckets,
      {buckets: [{name: 'bucket_name'}]}
    )
    @client.stub_responses(
      :head_object,
      {content_type: 'image/jpeg'}
    )

    @link = 'https://testurl.com'
    stub_request(:get, @link).to_return(
      status:200,
      :body => '<html>
                    <head>
                      <title>title</title>
                      <link rel="alternate" type="application/json" />
                      <meta property="og:title" content="og:title">
                      <meta property="og:description" content="og:description">
                      <meta property="og:site_name" content="og:site_name">
                    </head>
                </html>',
      :headers => {}
    )

    api_v2_sign_in(@user)
  end


  test ":api should be able to create resource with a stock image_url if image_url is blank" do
    resource_url = "https://stark.lvh.me:4001/api/scorm/launch?course_id=Micro_5_Role_of_a_Humble_Mentor_SCORM98a65c14-c439-492c-920c-42abc28d5017"

    stub_request(:get, resource_url).
          to_return(:status => 200,
                    :body => '<html>
                    <head>
                      <title>title</title>
                      <link rel="alternate" type="application/json" />
                      <meta property="og:title" content="og:title">
                      <meta property="og:description" content="og:description">
                      <meta property="og:site_name" content="og:site_name">
                      <meta property="og:image" content="">
                      <meta property="og:video:url" content="http://videourl.com">
                      <meta property="og:type" content="video">
                    </head>
                    </html>',
                    :headers => {})

    post :create, resource: {link: resource_url}, format: :json
    resp = get_json_response(response)
    refute resp[:image_url]
    assert_response :ok
  end

  test ":api should be able to create resouce with valid link" do

    assert_difference -> {Resource.count} do
      post :create, resource: {link: @video_url}, format: :json
    end

    resp = get_json_response(response)

    assert_equal "title", resp['title']
    assert_equal "Video", resp['type']
    assert_equal "http://imageurl.com", resp['image_url']
  end

  test ':api should create proper resource for valid video links' do
    assert_difference -> { Resource.count } do
      post :create, resource: {link: @video_link}, format: :json
    end

    resp = get_json_response(response)

    assert_equal '', resp['title']
    assert_equal 'Video', resp['type']
    assert_equal @video_link + '?autoplay=1', resp['video_url']
  end

  test ':api should create resource with image_url from s3 if s3_bucket enabled and data present' do
    create(:config, configable: @org, name: 'custom_images_bucket_name', value: 'bucket_name')
    create(:config, configable: @org, name: 'custom_images_region', value: 'bucket_region')
    @client.stub_responses(
      :list_objects,
      Aws::S3::Types::ListObjectsOutput.new(
        is_truncated: false,
        contents: [
          {'key': 'image.jpeg'},
          {'key': 'image2.jpeg'},
          {'key': 'image3.jpeg'}
        ]
      )
    )
    S3ObjectExtractorService.stubs(:s3_client_default).returns(@client)
    urls = %w[
        https://bucket_name.s3.amazonaws.com/image.jpeg
        https://bucket_name.s3.amazonaws.com/image2.jpeg
        https://bucket_name.s3.amazonaws.com/image3.jpeg
    ]

    image_extractor = S3ObjectExtractorService.new(
      bucket: 'bucket_name', region: 'bucket_region'
    )

    image_extractor.stubs(:s3_client).returns(@client)

    image_extractor.stubs(:extract_image_objects).returns(urls)

    post :create, resource: {link: @link, s3_bucket: true}, format: :json

    resp = get_json_response(response)

    assert_includes urls, resp['image_url']
  end

  test ':api should create resource with image_url from CARD_STOCK_IMAGE if s3_bucket enabled and data blank' do
    Filestack::Security.any_instance.stubs(:signed_url).returns(CARD_STOCK_IMAGE.sample['url'])
    create(:config, configable: @org, name: 'custom_images_bucket_name', value: 'bucket_name')
    create(:config, configable: @org, name: 'custom_images_region', value: 'bucket_region')
    S3ObjectExtractorService.stubs(:s3_client_default).returns(@client)

    image_extractor = S3ObjectExtractorService.new(
      bucket: 'bucket_name', region: 'bucket_region'
    )

    image_extractor.stubs(:s3_client).returns(@client)

    image_extractor.stubs(:extract_image_objects).returns([])

    post :create, resource: {link: @link, s3_bucket: true}, format: :json

    resp = get_json_response(response)

    assert CARD_STOCK_IMAGE.any? {|obj| obj[:url] == resp['image_url']}
  end

  test ':api should create resource with image_url from CARD_STOCK_IMAGE if s3_bucket enabled and data present but not created one of the configs' do
    Filestack::Security.any_instance.stubs(:signed_url).returns(CARD_STOCK_IMAGE.sample['url'])
    create(:config, configable: @org, name: 'custom_images_bucket_name', value: 'bucket_name')
    S3ObjectExtractorService.stubs(:s3_client_default).returns(@client)
    urls = %w[
        https://bucket_name.s3.amazonaws.com/image.jpeg
        https://bucket_name.s3.amazonaws.com/image2.jpeg
        https://bucket_name.s3.amazonaws.com/image3.jpeg
    ]
    image_extractor = S3ObjectExtractorService.new(
      bucket: 'bucket_name', region: 'bucket_region'
    )

    image_extractor.stubs(:s3_client).returns(@client)

    image_extractor.stubs(:extract_image_objects).returns(urls)
    post :create, resource: {link: @link, s3_bucket: true}, format: :json

    resp = get_json_response(response)

    assert CARD_STOCK_IMAGE.any? {|obj| obj[:url] == resp['image_url'] }
  end

  test ':api should update resource details' do
    resource = create(:resource, title: 'EdCast Learning', description: 'Learn more', image_url: 'http://edcast.com/edcastlogo.png', user_id: @user.id)

    image_url = "http://edcast.com/edcastnewlogo.png"
    title = "Edcast learning 2.0"

    put :update, id: resource.id, resource: {image_url: image_url, title: title}

    resource.reload
    assert_response :ok
    assert_equal title, resource.title
    assert_equal image_url, resource.image_url
  end

  test 'should not all resource link update' do
    resource = create(:resource, title: 'EdCast Learning', description: 'Learn more', image_url: 'http://edcast.com/edcastlogo.png', user_id: @user.id)

    url = "http://edcast.com/"
    put :update, id: resource.id, resource: {url: url}

    resource.reload

    assert_response :ok
    assert_not_equal url, resource.url
  end

  test 'admin should be able to update resource image url' do
    admin = create(:user, organization_id: @user.organization_id, organization_role: 'admin')
    api_v2_sign_in(admin)

    resource_card = create(:card, card_type: 'media',
      card_subtype: 'link', author: @user,
      resource: create(:resource, title: 'EdCast Learning', description: 'Learn more', image_url: 'http://edcast.com/edcastlogo.png', user_id: @user.id)
    )

    resource = resource_card.resource

    put :update, id: resource.id, resource: {image_url: "http://edcast.com/edcastlogo2.png"}

    resource.reload
    assert_response :ok
    assert_equal "http://edcast.com/edcastlogo2.png", resource.image_url
  end

  test 'should send error message when resource was not updated' do
    admin = create(:user, organization_id: @user.organization_id, organization_role: 'admin')
    api_v2_sign_in(admin)

    resource_card = create(:card, card_type: 'media',
      card_subtype: 'link', author: @user,
      resource: create(:resource, title: 'EdCast Learning', description: 'Learn more', image_url: 'http://edcast.com/edcastlogo.png', user_id: @user.id)
    )

    Resource.any_instance.expects(:update_attributes).returns false
    resource = resource_card.resource

    put :update, id: resource.id, resource: { image_url: "http://edcast.com/edcastlogo2.png" }

    resource.reload
    # resp = get_json_response(response)
    assert_response :unprocessable_entity
  end
end

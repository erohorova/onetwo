require 'test_helper'

class Api::V2::WorkflowsControllerTest < ActionController::TestCase
  setup do
    @org = create(:organization)
    Role.create_standard_roles @org
    @admin = create(:user, organization: @org, is_active: true)
    @admin.add_role('admin')
    user1, user2, user3, user4, user5 = create_list(:user, 5, organization: @org, is_suspended: false)
    @users = [user1, user2, user3, user4, user5]
    @u_profiles = [user1, user2, user3, user4, user5].map do |usr|
      Profile.create(
        user_id: usr.id, organization_id: @org.id,
        external_id: "ext_id-#{usr.id}", uid: "uid-#{usr.id}"
      )
    end
    api_v2_sign_in(@admin)
  end

  test 'should throw error for invalid ID' do
    get :list, id: 'not_valid', format: :json
    assert :unprocessable_entity
    resp = get_json_response(response)
    assert_equal 'Not valid ID', resp['message']
  end

  test 'should return profile data in proper format' do
    expected_total = 5
    expected_columns_keys = %w[
      id profile_id profile_external_id profile_uid
      user_email user_first_name user_last_name user_status
      user_created_at
    ]
    last_p = Profile.last
    user = User.find_by(id: last_p.user_id)
    expected_row_format = {
      'profile_id' => last_p.id,
      'id' => last_p.user_id.to_s,
      'profile_external_id' => last_p.external_id,
      'profile_uid' => last_p.uid,
      'user_email' => user.email,
      'user_first_name' => user.first_name,
      'user_last_name' => user.last_name,
      'user_status' => user.status,
      'user_created_at' => user.created_at.to_datetime.strftime('%Q').to_i
    }
    get :list, id: 'profile', format: :json
    assert :ok
    resp = get_json_response(response)
    assert_equal expected_total, resp['rows_total_count']
    columns_real_keys = resp['columns'].map { |el| el['id'] }
    assert_same_elements expected_columns_keys, columns_real_keys
    assert expected_row_format == resp['rows'].first
  end

  test 'should return only active and not anonymized users' do
    expected_total = 2
    suspended = @users[0].tap {|u| u.update_attributes(status: 'suspended')}
    is_suspended = @users[1].tap {|u| u.update_attributes(is_suspended: true)}
    is_anonymized = @users[2].tap {|u| u.update_attributes(is_anonymized: true)}
    @users.each(&:reload)
    not_active_users = [suspended, is_suspended, is_anonymized]

    get :list, id: 'profile', format: :json
    assert :ok
    resp = get_json_response(response)
    assert_equal expected_total, resp['rows_total_count']
    expected_ids = resp['rows'].map { |row| row['id'].to_i }
    assert expected_ids.exclude?(not_active_users.map(&:id))
  end

  test 'should return custom_fields in profile data if they present' do
    cf = create(
      :custom_field, organization: @org,
      display_name: 'custom', abbreviation: 'custom'
    )
    expected_total = 5
    expected_columns_keys = %w[
      id profile_id profile_external_id profile_uid
      user_email user_first_name user_last_name user_status
      user_created_at
    ]
    last_p = Profile.last
    user = User.find_by(id: last_p.user_id)
    create(
      :user_custom_field, user_id: user.id, custom_field_id: cf.id,
      value: 'last_user_value'
    )
    expected_row_format = {
      'profile_id' => last_p.id,
      'id' => last_p.user_id.to_s,
      'profile_external_id' => last_p.external_id,
      'profile_uid' => last_p.uid,
      'user_email' => user.email,
      'user_first_name' => user.first_name,
      'user_last_name' => user.last_name,
      'user_status' => user.status,
      'user_created_at' => user.created_at.to_datetime.strftime('%Q').to_i,
      'custom_custom' => 'last_user_value'
    }
    expected_columns_keys.push('custom_' + cf.abbreviation)
    get :list, id: 'profile', format: :json
    assert :ok
    resp = get_json_response(response)
    assert_equal expected_total, resp['rows_total_count']
    columns_real_keys = resp['columns'].map{ |el| el['id'] }
    assert_same_elements expected_columns_keys, columns_real_keys
    assert expected_row_format == resp['rows'].first
    assert_same_elements expected_row_format.except('custom_custom').keys,  resp['rows'].second.keys
  end

  test 'should be able to authorize action by users JWT or master token' do
    # using standard jwt auth
    get :list, id: 'profile', format: :json
    assert_response :ok

    # using master token and organization context from host
    workflow_auth(@admin)
    get :list, id: 'profile', format: :json
    assert_response :ok
  end
  
  test 'should properly support limit and have valid rows_total_count value' do
    get :list, id: 'profile', limit: 2, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal 5, resp['rows_total_count']
    assert_equal 2, resp['rows'].count
  end

  class InputSourceTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      Role.create_standard_roles @org
      @admin = create(:user, organization: @org, is_active: true)
      @admin.add_role('admin')
      user1, user2, user3, user4, user5 = create_list(:user, 5, organization: @org)
      @users = [user1, user2, user3, user4, user5]
      @u_profiles = [user1, user2, user3, user4, user5].map do |usr|
        Profile.create(
          user_id: usr.id, organization_id: @org.id,
          external_id: "ext_id-#{usr.id}", uid: "uid-#{usr.id}"
        )
      end
      api_v2_sign_in(@admin)
    end

    test 'should display error message if type is not present' do
      cf = create(
        :custom_field, organization: @org,
        display_name: 'custom', abbreviation: 'custom'
      )
      last_p = Profile.last
      user = User.find_by(id: last_p.user_id)
      create(
        :user_custom_field, user_id: user.id, custom_field_id: cf.id,
        value: 'last_user_value'
      )
      get :input_source, format: :json
      assert :ok
      resp = get_json_response(response)
      assert_equal 'Missing required parameter', resp['message']
    end

    test 'should display error message if type is other than user, channel or team' do
      cf = create(
        :custom_field, organization: @org,
        display_name: 'custom', abbreviation: 'custom'
      )
      last_p = Profile.last
      user = User.find_by(id: last_p.user_id)
      create(
        :user_custom_field, user_id: user.id, custom_field_id: cf.id,
        value: 'last_user_value'
      )
      get :input_source, type: 'teams', format: :json
      assert :ok
      resp = get_json_response(response)
      assert_equal 'Invalid source type', resp['message']
    end

    test 'should return blank array for when type is set to channel or team' do
      cf = create(
        :custom_field, organization: @org,
        display_name: 'custom', abbreviation: 'custom'
      )
      last_p = Profile.last
      user = User.find_by(id: last_p.user_id)
      create(
        :user_custom_field, user_id: user.id, custom_field_id: cf.id,
        value: 'last_user_value'
      )
      get :input_source, type: 'team', format: :json
      assert :ok
      resp = get_json_response(response)
      assert_equal [], resp['columns']
    end

    test 'should return custom_fields and preview data in input source if present' do
      cf = create(
        :custom_field, organization: @org,
        display_name: 'custom', abbreviation: 'custom'
      )
      expected_columns_keys = %w[first_name last_name]
      last_p = Profile.last
      user = User.find_by(id: last_p.user_id)
      create(
        :user_custom_field, user_id: user.id, custom_field_id: cf.id,
        value: 'last_user_value'
      )
      expected_columns_keys.push(cf.abbreviation)
      get :input_source, type: 'user', format: :json
      assert :ok
      resp = get_json_response(response)
      columns_real_keys = resp['columns'].map { |el| el['column_name'] }
      assert_same_elements expected_columns_keys, columns_real_keys
      refute_empty resp['preview_data']
    end

    test 'should return custom_fields in input source only once' do
      cf = create(
        :custom_field, organization: @org,
        display_name: 'custom', abbreviation: 'custom'
      )
      expected_total = 3
      expected_columns_keys = %w[first_name last_name]
      last_p = Profile.last
      user = User.find_by(id: last_p.user_id)
      create(
        :user_custom_field, user_id: user.id, custom_field_id: cf.id,
        value: 'last_user_value'
      )
      expected_columns_keys.push(cf.abbreviation)
      get :input_source, type: 'user', format: :json
      assert :ok
      resp = get_json_response(response)
      columns_real_keys = resp['columns'].map { |el| el['column_name'] }
      assert_same_elements expected_columns_keys, columns_real_keys
      assert_equal expected_total, columns_real_keys.length

      get :input_source, type: 'user', format: :json
      assert :ok
      resp = get_json_response(response)
      columns_real_keys = resp['columns'].map { |el| el['column_name'] }
      assert_same_elements expected_columns_keys, columns_real_keys
      assert_equal expected_total, columns_real_keys.length
    end
  end

  class CreateTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      Role.create_standard_roles @org
      admin_role = @org.roles.find_by_name('admin')
      create(:role_permission, role: admin_role, name: Permissions::ADMIN_ONLY)

      @admin = create(:user, organization: @org)
      @admin.add_role('admin')
      @user = create(:user, organization: @org)
      api_v2_sign_in(@admin)
    end

    test 'should not allow to create a workflow to non-admin user' do
      api_v2_sign_in(@user)
      workflow_params = {
        user_id: @admin.id, organization_id: @org.id,
        wf_type: 'User',
        wf_rules: nil,
        wf_input_source: [{type: 'DB', source: 'User'}],
        wf_actionable_columns: [{
          column_name: 'user_created_at', is_identifier: false,
          csv_name: nil, type: 'string', is_dynamic: true, formula: nil,
        }],
        wf_action: {
          entity: "Group",type: "create_dynamic",
          value: {
            column_name: 'created_at',
            wf_column_name: "user_created_at",
            prefix: '',
            suffix: ''
          },
          metadata: {
            is_private: true,
            description: 'Group created from workflow',
            is_mandatory: true
          }
        }
      }
      post :create, workflow: workflow_params, format: :json
      assert_response 401
    end

    test "should show error message if we don't provide name" do
      workflow_params = {
        user_id: @admin.id, organization_id: @org.id,
        wf_type: 'User',
        wf_rules: nil,
        wf_input_source: [{type: 'DB', source: 'User'}],
        wf_actionable_columns: [{
          column_name: 'user_created_at', is_identifier: false,
          csv_name: nil, type: 'string', is_dynamic: true, formula: nil,
        }],
        wf_action: {
          entity: "Group",type: "create_dynamic",
          value: {
            column_name: 'created_at',
            wf_column_name: "user_created_at",
            prefix: '',
            suffix: ''
          },
          metadata: {
            is_private: true,
            description: 'Group created from workflow',
            is_mandatory: true
          }
        }
      }
      post :create, workflow: workflow_params, format: :json
      assert :ok
      resp = get_json_response(response)
      assert_equal "Error while creating Workflow Record Name can't be blank", resp['message']
    end

    test "should show error message if we don't provide workflow actionable columns" do
      workflow_params = {
        user_id: @admin.id, organization_id: @org.id,
        wf_type: 'User',
        wf_rules: nil,
        name: 'workflow test',
        wf_input_source: [{type: 'DB', source: 'User'}],
        wf_action: {
          entity: "Group",type: "create_dynamic",
          value: {
            column_name: 'created_at',
            wf_column_name: "user_created_at",
            prefix: '',
            suffix: ''
          },
          metadata: {
            is_private: true,
            description: 'Group created from workflow',
            is_mandatory: true
          }
        }
      }
      post :create, workflow: workflow_params, format: :json
      assert :ok
      resp = get_json_response(response)
      assert_equal "Error while creating Workflow Record Workflow actionable columns can't be blank", resp['message']
    end

    test "should show error message if we don't provide workflow action" do
      workflow_params = {
        user_id: @admin.id, organization_id: @org.id,
        wf_type: 'User',
        wf_rules: nil,
        name: 'workflow test',
        wf_input_source: [{type: 'DB', source: 'User'}],
        wf_actionable_columns: [{
          column_name: 'user_created_at', is_identifier: false,
          csv_name: nil, type: 'string', is_dynamic: true, formula: nil,
        }]
      }
      post :create, workflow: workflow_params, format: :json
      assert :ok
      resp = get_json_response(response)
      assert_equal "Error while creating Workflow Record Workflow action's output columns can't be blank", resp['message']
    end

    test "should show error message if we don't provide workflow type" do
      workflow_params = {
        user_id: @admin.id, organization_id: @org.id,
        wf_type: nil,
        wf_rules: nil,
        name: 'workflow test',
        wf_input_source: [{type: 'DB', source: 'User'}],
        wf_actionable_columns: [{
          column_name: 'user_created_at', is_identifier: false,
          csv_name: nil, type: 'string', is_dynamic: true, formula: nil,
        }],
        wf_action: {
          entity: "Group",type: "create_dynamic",
          value: {
            column_name: 'created_at',
            wf_column_name: "user_created_at",
            prefix: '',
            suffix: ''
          },
          metadata: {
            is_private: true,
            description: 'Group created from workflow',
            is_mandatory: true
          }
        }
      }
      post :create, workflow: workflow_params, format: :json
      assert :ok
      resp = get_json_response(response)
      assert_equal "Error while creating Workflow Record Workflow type can't be blank", resp['message']
    end

    test "should show error message if we don't provide workflow input source" do
      workflow_params = {
        user_id: @admin.id, organization_id: @org.id,
        wf_type: 'User',
        wf_rules: nil,
        name: 'workflow test',
        wf_input_source: nil,
        wf_actionable_columns: [{
          column_name: 'user_created_at', is_identifier: false,
          csv_name: nil, type: 'string', is_dynamic: true, formula: nil,
        }],
        wf_action: {
          entity: "Group",type: "create_dynamic",
          value: {
            column_name: 'created_at',
            wf_column_name: "user_created_at",
            prefix: '',
            suffix: ''
          },
          metadata: {
            is_private: true,
            description: 'Group created from workflow',
            is_mandatory: true
          }
        }
      }
      post :create, workflow: workflow_params, format: :json
      assert :ok
      resp = get_json_response(response)
      assert_equal "Error while creating Workflow Record Workflow input source can't be blank", resp['message']
    end

    test "should create workflow" do
      workflow_params = {
        user_id: @admin.id, organization_id: @org.id,
        wf_type: 'User',
        wf_rules: {
          and:[
            {
              equals: {country: 'US'}
            },
            {
              not_equals: {location: 'SF'}
            },
            {
              must_exists: 'location'
            }
          ]
        },
        name: 'workflow test',
        wf_input_source: [{type: 'DB', source: 'User'}],
        wf_actionable_columns: [{
          column_name: 'user_created_at', is_identifier: false,
          csv_name: nil, type: 'string', is_dynamic: true, formula: nil,
        }],
        wf_action: {
          entity: "Group",type: "create_dynamic",
          value: {
            column_name: 'created_at',
            wf_column_name: ["user_created_at"],
            prefix: '',
            suffix: ''
          },
          metadata: {
            is_private: true,
            description: 'Group created from workflow',
            is_mandatory: true
          }
        },
        wf_output_result: {
          entity: 'Group',
          ids: [1,2,3,4,6]
        }
      }
      post :create, workflow: workflow_params, format: :json
      assert :ok
    end

    test "should create workflow for create_custom type" do
      workflow_params = {
        user_id: @admin.id, organization_id: @org.id,
        wf_type: 'User',
        wf_rules: {
          and:[
            {
              equals: {country: 'US'}
            },
            {
              not_equals: {location: 'SF'}
            },
            {
              must_exists: 'location'
            }
          ]
        },
        name: 'workflow test',
        wf_input_source: [{type: 'DB', source: 'User'}],
        wf_actionable_columns: [{
          column_name: 'user_created_at', is_identifier: false,
          csv_name: nil, type: 'string', is_dynamic: true, formula: nil,
        }],
        wf_action: {
          entity: "Team",type: "create_custom",
          value: {
            column_name: 'created_at',
            custom_column_name: 'Workflow Testing'
          },
          metadata: {
            is_private: true,
            description: 'Group created from workflow',
            is_mandatory: true
          }
        },
        wf_output_result: {
          entity: 'Group',
          ids: [1,2,3,4,6]
        }
      }
      post :create, workflow: workflow_params, format: :json
      assert :ok
    end

    test "should create workflow with type existing" do
      workflow_params = {
        user_id: @admin.id, organization_id: @org.id,
        wf_type: 'User',
        wf_rules: {
          and:[
            {
              equals: {country: 'US'}
            },
            {
              not_equals: {location: 'SF'}
            },
            {
              must_exists: 'location'
            }
          ]
        },
        name: 'workflow test',
        wf_input_source: [{type: 'DB', source: 'User'}],
        wf_actionable_columns: [{
          column_name: 'user_created_at', is_identifier: false,
          csv_name: nil, type: 'string', is_dynamic: true, formula: nil,
        }],
        wf_action: {
          entity: "Team",type: "existing",
          value: {
            column_name: 'id',
            id: 30
          }
        },
        wf_output_result: {
          entity: 'Team',
          ids: [1,2,3,4,6]
        }
      }
      post :create, workflow: workflow_params, format: :json
      assert :ok
    end

  end

  class RunTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      Role.create_standard_roles @org
      @admin = create(:user, organization: @org)
      @admin.add_role('admin')
      api_v2_sign_in(@admin)
    end

    test 'should process the workflow whose status is new' do
      @workflow = create(:workflow, user_id: @admin.id, organization: @org, is_enabled: false, wf_action: {entity: "Group" ,type: "create_dynamic", value: {column_name: 'created_at',wf_column_name: "user_created_at",prefix: '',suffix: '',},metadata: {is_private: true,description: 'Group created from workflow', is_mandatory: true}}) 
      WorkflowProcessorJob.expects(:perform_later).with({
        workflow_id: @workflow.id,
        model_name: @workflow.wf_type
      }).once

      post :run, id: @workflow.id, format: :json
    end

    test 'should not process the workflow whose status is completed' do
      @workflow = create(:workflow, user_id: @admin.id, organization: @org, status: 'completed', is_enabled: false, wf_action: {entity: "Group" ,type: "create_dynamic", value: {column_name: 'created_at',wf_column_name: "user_created_at",prefix: '',suffix: '',},metadata: {is_private: true,description: 'Group created from workflow', is_mandatory: true}}) 
      post :run, id: @workflow.id, format: :json
      resp = get_json_response(response)
      assert_equal "You can not process completed workflows", resp['message']
    end

    test 'should not process the workflow whose status is processing' do
      @workflow = create(:workflow, user_id: @admin.id, organization: @org, status: 'processing', is_enabled: false, wf_action: {entity: "Group" ,type: "create_dynamic", value: {column_name: 'created_at',wf_column_name: "user_created_at",prefix: '',suffix: '',},metadata: {is_private: true,description: 'Group created from workflow', is_mandatory: true}}) 
      post :run, id: @workflow.id, format: :json
      resp = get_json_response(response)
      assert_equal "You can not process processing workflows", resp['message']
    end
  end

  class ToggleTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      Role.create_standard_roles @org
      @admin = create(:user, organization: @org)
      @admin.add_role('admin')
      api_v2_sign_in(@admin)
    end

    test 'should show error if workflow does not have requried type parameter' do
      @workflow = create(:workflow, user_id: @admin.id, organization: @org, is_enabled: false, wf_action: {entity: "Group" ,type: "create_dynamic", value: {column_name: 'created_at',wf_column_name: "user_created_at",prefix: '',suffix: '',},metadata: {is_private: true,description: 'Group created from workflow', is_mandatory: true}}) 
      put :toggle, id: @workflow.id, format: :json
      resp = get_json_response(response)
      assert_equal "Missing required parameter", resp['message']
    end

    test 'should able to update the value of is_enabled according to parameter `is_enabled` case when is_enabled set to true' do
      @workflow = create(:workflow, user_id: @admin.id, organization: @org, is_enabled: false, wf_action: {entity: "Group" ,type: "create_dynamic", value: {column_name: 'created_at',wf_column_name: "user_created_at",prefix: '',suffix: '',},metadata: {is_private: true,description: 'Group created from workflow', is_mandatory: true}}) 
      put :toggle, id: @workflow.id, is_enabled: true, format: :json
      resp = get_json_response(response)
      assert_equal @workflow.reload.is_enabled, resp['is_enabled']
    end

    test 'should able to update the value of is_enabled according to parameter `is_enabled` case when is_enabled set to false' do
      @workflow = create(:workflow, user_id: @admin.id, organization: @org, is_enabled: true, wf_action: {entity: "Group" ,type: "create_dynamic", value: {column_name: 'created_at',wf_column_name: "user_created_at",prefix: '',suffix: '',},metadata: {is_private: true,description: 'Group created from workflow', is_mandatory: true}}) 
      put :toggle, id: @workflow.id, is_enabled: false, format: :json
      resp = get_json_response(response)
      assert_equal @workflow.id, resp['id']
      assert_equal @workflow.reload.is_enabled, resp['is_enabled']
    end

    test 'should not be able to enable or display csv workflows' do
      @workflow = create(:workflow, user_id: @admin.id, organization: @org, is_enabled: true, wf_input_source: [{type: 'CSV', source: 'user'}],
        wf_action: {entity: "Group" ,type: "create_dynamic", value: {column_name: 'created_at',wf_column_name: "user_created_at",prefix: '',suffix: '',},metadata: {is_private: true,description: 'Group created from workflow', is_mandatory: true}}) 
      put :toggle, id: @workflow.id, is_enabled: false, format: :json
      resp = get_json_response(response)
      assert_equal 'You can not enable or disable CSV workflows', resp['message']
    end

    test 'should display not found error if workflow does not exists' do
      @workflow = create(:workflow, user_id: @admin.id, organization: @org, is_enabled: true, wf_action: {entity: "Group" ,type: "create_dynamic", value: {column_name: 'created_at',wf_column_name: "user_created_at",prefix: '',suffix: '',},metadata: {is_private: true,description: 'Group created from workflow', is_mandatory: true}}) 
      put :toggle, id: 24, is_enabled: false, format: :json
      resp = get_json_response(response)
      assert_equal 'Record not found', resp['message']
    end
  end

  class DestroyActionTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      Role.create_standard_roles @org
      admin_role = @org.roles.find_by_name('admin')
      create(:role_permission, role: admin_role, name: Permissions::ADMIN_ONLY)

      @admin = create(:user, organization: @org)
      @admin.add_role('admin')
      @user = create(:user, organization: @org)
      @workflow = create(:workflow, user_id: @admin.id, organization: @org)
    end

    test ':api destroy' do
      api_v2_sign_in(@admin)
      assert_difference -> { Workflow.all.count }, -1 do
        delete :destroy, id: @workflow.id, format: :json
        assert_response :ok
      end
    end

    test 'should not allow to destroy a workflow to non-admin user' do
      api_v2_sign_in(@user)
      delete :destroy, id: @workflow.id, format: :json
      assert_response 401
    end

  end
  
  class ShowActionTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      Role.create_standard_roles @org
      admin_role = @org.roles.find_by_name('admin')
      create(:role_permission, role: admin_role, name: Permissions::ADMIN_ONLY)

      @admin = create(:user, organization: @org)
      @admin.add_role('admin')
      @user = create(:user, organization: @org)
      @workflow = create(:workflow, user_id: @admin.id, organization: @org)
    end

    test 'api to show workflow' do
      api_v2_sign_in(@admin)
      get :show, id: @workflow.id, format: :json
      resp = get_json_response(response)
      assert_equal @workflow.id, resp['workflows']['id']
      assert_equal @workflow.name, resp['workflows']['name']
      assert_equal @workflow.status, resp['workflows']['status']
      assert_equal @workflow.is_enabled, resp['workflows']['is_enabled']
      assert_equal @workflow.wf_type, resp['workflows']['wf_type']
      assert_equal @workflow.wf_input_source.first.stringify_keys, resp['workflows']['wf_input_source'][0]
      assert_equal @workflow.wf_actionable_columns.first.stringify_keys, resp['workflows']['wf_actionable_columns'][0]
      assert_equal @workflow.wf_action.stringify_keys, resp['workflows']['wf_action']
      assert_equal @workflow.wf_rules, resp['workflows']['wf_rules']
      assert_equal @workflow.wf_output_result, resp['workflows']['wf_output_result']
      assert_equal @workflow.author.full_name, resp['workflows']['creator']['full_name']
      assert_equal @workflow.author.id, resp['workflows']['creator']['id']
    end

    test 'should not allow to show a workflow to non-admin user' do
      api_v2_sign_in(@user)
      get :show, id: @workflow.id, format: :json
      assert_response 401
    end
    
    test 'should support on-demand ask' do
      api_v2_sign_in(@admin)
      get :show, id: @workflow.id, fields: 'name', format: :json
      resp = get_json_response(response)
      assert_equal 1, resp['workflows'].keys.size
      assert_equal @workflow.name, resp['workflows']['name']
    end  
  end

  class IndexTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      Role.create_standard_roles @org
      admin_role = @org.roles.find_by_name('admin')
      create(:role_permission, role: admin_role, name: Permissions::ADMIN_ONLY)

      @admin = create(:user, organization: @org)
      @admin.add_role('admin')
      @user = create(:user, organization: @org)
      api_v2_sign_in(@admin)
    end

    test 'should not allow to view Index page of workflows to non-admin user' do
      api_v2_sign_in(@user)
      get :index, format: :json
      assert_response 401
    end

    test ':api should get workflows in an org' do
      workflows = create_list(:workflow, 5, user_id: @admin.id, organization: @org,
        is_enabled: true,
        wf_action: {entity: "Group" ,type: "create_dynamic",
          value: {
            column_name: 'created_at',
            wf_column_name: "user_created_at",
            prefix: '',suffix: '',
          },
          metadata: {
            is_private: true,
            description: 'Group created from workflow',
            is_mandatory: true
          }
        }
      )

      get :index, format: :json
      assert_response :success
      resp = get_json_response(response)
      assert_equal 5, resp['total']
      assert_same_elements workflows.map(&:id), resp['workflows'].map{|w| w['id']}
    end

    test ':api should support on demand limit' do
      workflows = create_list(:workflow, 5, user_id: @admin.id, organization: @org,
        is_enabled: true,
        wf_action: {entity: "Group" ,type: "create_dynamic",
          value: {
            column_name: 'created_at',
            wf_column_name: "user_created_at",
            prefix: '',suffix: '',
          },
          metadata: {
            is_private: true,
            description: 'Group created from workflow',
            is_mandatory: true
          }
        }
      )

      get :index, limit: 2, offset: 0, format: :json
      assert_response :ok
      resp = get_json_response response
      assert_equal 2, resp['workflows'].length
    end

    test ':api should support default limit' do
      workflows = create_list(:workflow, 11, user_id: @admin.id, organization: @org,
        is_enabled: true,
        wf_action: {entity: "Group" ,type: "create_dynamic",
          value: {
            column_name: 'created_at',
            wf_column_name: "user_created_at",
            prefix: '',suffix: '',
          },
          metadata: {
            is_private: true,
            description: 'Group created from workflow',
            is_mandatory: true
          }
        }
      )

      get :index, format: :json
      assert_response :ok
      resp = get_json_response response
      assert_equal 10, resp['workflows'].length
    end

    test ':api should support offset' do
      workflows = create_list(:workflow, 11, user_id: @admin.id, organization: @org,
        is_enabled: true,
        wf_action: {entity: "Group" ,type: "create_dynamic",
          value: {
            column_name: 'created_at',
            wf_column_name: "user_created_at",
            prefix: '',suffix: '',
          },
          metadata: {
            is_private: true,
            description: 'Group created from workflow',
            is_mandatory: true
          }
        }
      )

      get :index, limit: 10, offset: 10, format: :json
      assert_response :ok
      resp = get_json_response response
      assert_equal 1, resp['workflows'].length
    end

    test ':api should support `is_enabled` parameter and list only active_workflows' do
      active_workflows = create_list(:workflow, 2, user_id: @admin.id, organization: @org,
        is_enabled: true,
        wf_action: {entity: "Group" ,type: "create_dynamic",
          value: {
            column_name: 'created_at',
            wf_column_name: "user_created_at",
            prefix: '',suffix: '',
          },
          metadata: {
            is_private: true,
            description: 'Group created from workflow',
            is_mandatory: true
          }
        }
      )

      inactive_workflows = create_list(:workflow, 3, user_id: @admin.id, organization: @org,
        wf_action: {entity: "Group" ,type: "create_dynamic",
          value: {
            column_name: 'created_at',
            wf_column_name: "user_created_at",
            prefix: '',suffix: '',
          },
          metadata: {
            is_private: true,
            description: 'Group created from workflow',
            is_mandatory: true
          }
        }
      )

      get :index, is_enabled: true, format: :json
      assert_response :ok
      resp = get_json_response response
      assert_equal active_workflows.length, resp['workflows'].length
    end

    test ':api should support `is_enabled` parameter and list only inactive_workflows' do
      active_workflows = create_list(:workflow, 2, user_id: @admin.id, organization: @org,
        is_enabled: true,
        wf_action: {entity: "Group" ,type: "create_dynamic",
          value: {
            column_name: 'created_at',
            wf_column_name: "user_created_at",
            prefix: '',suffix: '',
          },
          metadata: {
            is_private: true,
            description: 'Group created from workflow',
            is_mandatory: true
          }
        }
      )

      inactive_workflows = create_list(:workflow, 3, user_id: @admin.id, organization: @org,
        wf_action: {entity: "Group" ,type: "create_dynamic",
          value: {
            column_name: 'created_at',
            wf_column_name: "user_created_at",
            prefix: '',suffix: '',
          },
          metadata: {
            is_private: true,
            description: 'Group created from workflow',
            is_mandatory: true
          }
        }
      )

      get :index, is_enabled: false, format: :json
      assert_response :ok
      resp = get_json_response response
      assert_equal inactive_workflows.length, resp['workflows'].length
    end

    test ':api should gives basic workflow information' do
      workflows = create_list(:workflow, 1, user_id: @admin.id, organization: @org,
        is_enabled: true,
        wf_action: {entity: "Group" ,type: "create_dynamic",
          value: {
            column_name: 'created_at',
            wf_column_name: "user_created_at",
            prefix: '',suffix: '',
          },
          metadata: {
            is_private: true,
            description: 'Group created from workflow',
            is_mandatory: true
          }
        }
      )

      get :index, format: :json
      assert_response :ok
      resp = get_json_response response
      assert_equal workflows.first.id, resp['workflows'][0]['id']
      assert_equal workflows.first.name, resp['workflows'][0]['name']
      assert_equal workflows.first.status, resp['workflows'][0]['status']
      assert_equal workflows.first.is_enabled, resp['workflows'][0]['is_enabled']
      assert_equal workflows.first.wf_type, resp['workflows'][0]['wf_type']
      assert_equal workflows.first.wf_input_source.first.stringify_keys, resp['workflows'][0]['wf_input_source'][0]
      assert_equal workflows.first.wf_actionable_columns.first.stringify_keys, resp['workflows'][0]['wf_actionable_columns'][0]
      assert_equal workflows.first.wf_action.stringify_keys, resp['workflows'][0]['wf_action']
      assert_equal workflows.first.wf_rules, resp['workflows'][0]['wf_rules']
      assert_equal workflows.first.wf_output_result, resp['workflows'][0]['wf_output_result']
      assert_equal workflows.first.author.full_name, resp['workflows'][0]['creator']['full_name']
      assert_equal workflows.first.author.id, resp['workflows'][0]['creator']['id']
    end

    test 'should support pagination' do
      create_list(:workflow, 50, organization: @org, user_id: @admin.id)

      get :index, limit: 15, offset: 10, format: :json

      resp = get_json_response response

      assert_response :ok
      assert_equal 15, resp['workflows'].count
      assert_equal 50, resp['total']
    end

  end

  class HandlePatchesTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      Role.create_standard_roles @org
      @admin = create(:user, organization: @org, is_active: true)
      @admin.add_role('admin')
      user1, user2, user3, user4, user5 = create_list(:user, 5, organization: @org)
      @users = [user1, user2, user3, user4, user5]
      api_v2_sign_in(@admin)
      EdcastActiveJob.unstub(:perform_later)
      WorkflowPatchRevisionsJob.unstub(:perform_later)
    end

    test 'should not be authorized if token not equal to master token or user jwt' do
      request.headers['X-API-TOKEN'] = 'not_valid_token'

      post :handle_patches, {}, format: :json
      assert_response :unauthorized
      resp = get_json_response(response)
      assert_equal 'JWTToken is expired or invalid', resp['message']
    end

    test 'should proceed job with patch data and return job_id' do
      # basic example of patch data
      # per request:
      #   could content up to 5k objects at maximum
      #
      # patches validates in WorkflowPatchRevisionsJob
      #   and writes to WorkflowPatchRevision model
      params = {
        _json: [
          {
            patchId: '8c6a04f0-b821-11e8-87cd-e54aaae67708',
            patchType: 'insert',
            data: {
              id: '26336881-056b-4944-b541-abfb6f2e6fb9',
              group_name: 'patch_test',
              group_type: 'group_type',
              group_description: 'patch_test'
            },
            meta: {
              historyRevisionVersion: 1,
              historyRevisionDatetime: 1536931486000,
              historyRevisionDataJson: {}
            },
            workflowType: 'groups',
            orgName: @org.host_name,
            params: {},
            models: {
              workflowId: 1,
              outputSourceId: 1
            }
          }
        ]
      }.with_indifferent_access

      SimpleJob = Struct.new(:job_id)
      simple_job_object = SimpleJob.new('a9dc47eb-aa37-4513-9222-886c3c8d6702')

      WorkflowPatchRevisionsJob.expects(:perform_later)
        .with(patches: params[:_json], version: 0)
        .returns(simple_job_object)

      post :handle_patches, params.merge(format: :json)
      assert_response :ok
      resp = get_json_response(response)
      assert_same_elements ['job_id'], resp.keys
      assert_equal 'a9dc47eb-aa37-4513-9222-886c3c8d6702', resp['job_id']
    end
  end

  class DynamicSelectionTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      @creator = create(:user, organization: @org, is_active: true)
      @card = create(:card, organization: @org, author: @creator)
      api_v2_sign_in(@creator)

      @params_create = {
        workflow: {
          type: 'dynamic-selection-group',
          action: 'create',
          additionInfo: {
            objectType: 'card',
            objectId: @card.id.to_s,
            action: 'assign',
            message: ''
          },
          payload: {
            groupTitle: 'dynamic_selection',
            groupDescription: 'sample',
            profileFilterOptions: {
              filterConditionType: 'all',
              filterRules: [
                {
                  colId: 'favourite_value',
                  condition: 'equals',
                  targetValue: '35'
                }
              ]
            }
          }
        }
      }

      stub_request(:get, 'http://workflow.test/status').
        to_return(status: 200, body: '', headers: {})
    end

    test 'should throw 422 if server unavailable' do
      stub_request(:get, 'http://workflow.test/status').to_raise(
        Faraday::ConnectionFailed
      )
      post :dynamic_selections, {}
      assert_response :unprocessable_entity
      resp = get_json_response(response)
      assert_equal 'Server is not available', resp['message']
    end

    test 'should throw 422 if server response timeout reached' do
      stub_request(:get, 'http://workflow.test/status').to_raise(
        Faraday::TimeoutError
      )
      post :dynamic_selections, {}
      assert_response :unprocessable_entity
      resp = get_json_response(response)
      assert_equal 'Server is not available', resp['message']
    end

    test 'should not create dynamic-selection revision if server timeout reached' do
      stub_request(:post, 'http://workflow.test/api/dynamic-selections')
        .to_raise(Faraday::TimeoutError)

      post :dynamic_selections, @params_create.merge(format: :json)
      assert_response :unprocessable_entity
      resp = get_json_response(response)
      assert_equal 'Service failed', resp['message']
      assert_equal 'TimeoutError', resp['external_status']
      assert resp['external_body'].blank?
    end

    test 'should validate presence of dynamic revision' do
      DynamicGroupRevision.create(
        title: 'dynamic_selection', organization_id: @org.id
      )
      post :dynamic_selections, @params_create.merge(format: :json)
      assert_response :unprocessable_entity
      resp = get_json_response(response)
      assert_equal 'Group already exists', resp['message']
    end

    test 'should create dynamic-selection revision after good response' do
      stub_request(:post, 'http://workflow.test/api/dynamic-selections').
        to_return(status: 201, body: {
          'isDynamic': true,
          'isDraft': false,
          'description': 'This workflow had been created by edcast dynamic selections API',
          'outputSourceId': 296,
          'title': "Dynamic selections for \"dynamic_selection\"",
          'REC_TYPE': 'Workflow',
          'createdAt': 1537432863999,
          'isHiddenForWeb': true,
          'dynamicId': 'b13000a6-b692-4e6f-9b6e-ae80b347f1e6',
          'createdBy': 'master user',
          'isAutorunOnPatches': true,
          'id': 294,
          'updatedAt': 1537432867438
        }.to_json)

      expected_uid = 'b13000a6-b692-4e6f-9b6e-ae80b347f1e6'
      expected_external_id = 294

      post :dynamic_selections, @params_create.merge(format: :json)
      assert_response :ok
      resp = get_json_response(response)
      keys = %w[message external_status external_body]
      assert_same_elements keys, resp.keys
      dynamic_group_revision = DynamicGroupRevision.find_by(
        title: 'dynamic_selection',
        organization_id: @org.id, user_id: @creator.id,
      )
      assert dynamic_group_revision
      assert_equal expected_external_id, dynamic_group_revision.external_id
      assert_equal expected_uid, dynamic_group_revision.uid
    end

    test 'should delete dynamic-selection revision' do
      group = create(:team, organization: @org, name: 'dynamic_selection')
      DynamicGroupRevision.create(
        title: 'dynamic_selection', organization_id: @org.id,
        group_id: group.id, user_id: @creator.id, external_id: 1
      )
      url = 'http://workflow.test/api/dynamic-selections/1/delete'

      stub_request(:post, url).
        to_return(status: 200, body: {}.to_json)

      params_delete = {
        workflow: {
          type: 'dynamic-selection-group',
          action: 'delete',
          payload: {
            groupId: group.id,
            groupTitle: group.name
          }
        }
      }

      post :dynamic_selections, params_delete.merge(format: :json)
      assert_response :ok
      resp = get_json_response(response)
      assert_equal 'Removing of the DynamicGroup in progress', resp['message']
      assert_equal '200', resp['external_status']
      assert resp['external_body'].blank?
    end

    test 'should throw error if dynamic revision not found' do
      params_delete = {
        workflow: {
          type: 'dynamic-selection-group',
          action: 'delete',
          payload: {
            groupId: 1,
            groupTitle: 'title_not_existed'
          }
        }
      }
      post :dynamic_selections, params_delete.merge(format: :json)
      assert_response :unprocessable_entity
      resp = get_json_response(response)
      assert_equal 'Group does not exists', resp['message']
    end

    test 'should throw 422 for invalid parameters' do
      params_delete = {
        wrong: {
          and: '',
          bad: '',
          parameters: {}
        }
      }
      post :dynamic_selections, params_delete.merge(format: :json)
      assert_response :unprocessable_entity
      resp = get_json_response(response)
      assert_equal 'Params validation failed', resp['message']
    end

    test 'should throw 422 for invalid payload parameters create action' do
      params_delete = {
        workflow: {
          type: 'dynamic-selection-group',
          action: 'create',
          payload: {}
        }
      }
      post :dynamic_selections, params_delete.merge(format: :json)
      assert_response :unprocessable_entity
      resp = get_json_response(response)
      assert_equal 'Params validation failed', resp['message']
    end

    test 'should throw 422 for invalid payload parameters delete action' do
      params_delete = {
        workflow: {
          type: 'dynamic-selection-group',
          action: 'delete',
          payload: {}
        }
      }
      post :dynamic_selections, params_delete.merge(format: :json)
      assert_response :unprocessable_entity
      resp = get_json_response(response)
      assert_equal 'Params validation failed', resp['message']
    end

    test 'should properly format payload parameters and pass JSON schema' do
      stub_request(:post, 'http://workflow.test/api/dynamic-selections').
        to_return(status: 201, body: {
          'isDynamic': true,
          'isDraft': false,
          'description': 'This workflow had been created by edcast dynamic selections API',
          'outputSourceId': 296,
          'title': "Dynamic selections for \"dynamic_selection\"",
          'REC_TYPE': 'Workflow',
          'createdAt': 1537432863999,
          'isHiddenForWeb': true,
          'dynamicId': 'dynamic_selection',
          'createdBy': 'master user',
          'isAutorunOnPatches': true,
          'id': 294,
          'updatedAt': 1537432867438
        }.to_json)

      initial_rules = [
        {
          colId: 'New Field',
          condition: 'equals',
          targetValue: 'test'
        },
        {
          colId: 'name',
          condition: 'equals',
          targetValue: 'John Doe'
        }
      ]
      @params_create[:workflow][:payload][:profileFilterOptions][:filterRules] = initial_rules

      post :dynamic_selections, @params_create.merge(format: :json)
      assert_response :ok

      controller_instance = self.instance_variable_get(:@controller)
      result_parameters = controller_instance.instance_variable_get(:@prepared_params)
      field_map = %i[payload profileFilterOptions filterRules]
      result_filter_columns = result_parameters.dig(*field_map).map do |item|
        item[:colId]
      end
      expected_columns = %w[user_first_name user_last_name custom_new_field]
      assert_same_elements result_filter_columns, expected_columns
    end
  end
end

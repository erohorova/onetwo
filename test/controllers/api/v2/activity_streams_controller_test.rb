require 'test_helper'

class Api::V2::ActivityStreamsControllerTest < ActionController::TestCase

  setup do
    @org = create(:organization)
    @user, @viewer, @team_member = create_list(:user, 3, organization: @org)
    @team, @team2 = create_list(:team, 2, organization: @org)
    @team.add_member(@team_member)
    @team2.add_member(@user)
    @team2.add_member(@team_member)

    @card = create(:card, author: @user)
    @ucc = create(:user_content_completion, completable: @card, user: @viewer, state: 'completed')

    @as_completed_card = create(:activity_stream, streamable: @ucc, organization: @org, user_id: @viewer.id, card_id: @card.id, action: 'smartbite_completed')

    api_v2_sign_in(@user)
  end

  test ':api index' do
    skip # ASSERTIONS ARE VERY BRITTLE IN THIS TEST
         # PLS REFACTOR
    card2 = create(:card, author: @user)
    vote = create(:vote, votable: @card, voter: @viewer)
    comment_card = create(:comment, message: 'basic comment', commentable: @card, user: @user)
    comment_team = create(:comment, message: 'team comment', commentable: @team, user: @team_member)
    comment_org = create(:comment, message: 'org comment', commentable: @org, user: @viewer)
    ucu = create(:user_content_completion, completable: card2, user: @viewer, state: 'started')

    create(:activity_stream, streamable: @card, organization: @org, user_id: @user.id, card_id: @card.id, action: 'created_card')

    create(:activity_stream, streamable: vote, organization: @org, user_id: @viewer.id, card_id: @card.id, action: 'upvote')

    create(:activity_stream, streamable: comment_card, organization: @org, user_id: @viewer.id, card_id: @card.id, action: 'comment')

    create(:activity_stream, streamable: comment_team, organization: @org, user_id: @team_member.id, action: 'comment')

    create(:activity_stream, streamable: comment_org, organization: @org, user_id: @viewer.id, action: 'comment')

    create(:activity_stream, streamable: ucu, organization: @org, user_id: @viewer.id, card_id: card2.id, action: 'smartbite_uncompleted')

    get :index, format: :json
    assert_response :ok
    as = get_json_response(response)['activity_streams']

    assert_equal 6, as.count
    assert_equal ActivityStream::ACTION_CREATED_CARD, as[1]['action']

    assert_equal @card.id, as[1]['linkable']['id']
    assert_equal @card.snippet, as[1]['snippet']
    assert_equal 'card', as[1]['linkable']['type']
    refute as[1]['message']

    assert_equal 'Card', as[1]['streamable_type']
    assert_equal @card.id, as[1]['streamable_id']
    assert_equal @team.id, as[4]['linkable']['id']
    assert_equal @user.organization.id, as[5]['linkable']['id']
    assert_equal as[4]['message']['text'], 'team comment'
    assert_equal as[5]['message']['text'], 'org comment'

    assert_same_elements [@user.id, @viewer.id, @team_member.id], as.map { |item| item['user']['id'] }.uniq
    assert_same_elements [@user.name, @viewer.name, @team_member.name], as.map { |item| item['user']['name'] }.uniq
    assert_equal @user.fetch_avatar_urls_v2, as.first['user']['avatarimages']
  end

  #test ':api index should not show activity from private channel' do
  #EP-16550 Now public cards are visible to user irrespective of which container they belong to
  #end

  test 'should return info about votes and comments' do
    comment = create(:comment, commentable: @as_completed_card, user: @user)
    create(:vote, votable: @as_completed_card, voter: @user)

    api_v2_sign_in(@user)
    get :index, format: :json
    assert_response :ok
    as = get_json_response(response)['activity_streams'][0]

    assert_equal 1, as['comments_count']
    assert_equal comment.id, as['comments'][0]['id']
    assert_equal comment.message, as['comments'][0]['message']
    assert_equal @viewer.id, as['user']['id']

    assert_equal 1, as['votes_count']
    assert_equal 1, as['voters'].count
    assert_equal @user.id, as['voters'][0]['id']
  end

  test 'api should support limit and offset' do
    create(:activity_stream, streamable: @card, organization: @org, user_id: @user.id, card_id: @card.id, action: 'created_card')
    get :index, limit: 1, offset: 1, format: :json

    assert_response :ok
    as = get_json_response(response)['activity_streams']
    assert_equal 1, as.count
    assert_equal 2, ActivityStream.count
  end

  test ':api index when streamable is nil' do
    @card.destroy
    get :index, format: :json
    assert_response :ok

    as = get_json_response(response)['activity_streams']
    assert_empty as
  end

  test 'should not return streams of deleted cards' do
    card = create(:card, organization: @org, author_id: @user.id)
    stream = create(:activity_stream, streamable: card, organization: @org, user_id: @user.id, card_id: card.id, action: 'created_card')
    
    card.destroy
    get :index, format: :json
    resp = get_json_response(response)['activity_streams']
    assert_response :ok
    assert_not_includes resp.map{|a| a['id']}, stream.id
  end

  test 'should return only conversation org activities' do
    comment_team = create(:comment, message: 'team comment', commentable: @team, user: @team_member)
    comment_org = create(:comment, message: 'org comment', commentable: @org, user: @viewer)
    create(:activity_stream, streamable: comment_team, organization: @org, user_id: @team_member.id, action: 'comment')
    org_as = create(:activity_stream, streamable: comment_org, organization: @org, user_id: @viewer.id, action: 'comment')
    @team.add_member(@user)
    api_v2_sign_in(@user)
    get :index, only_conversations: true, format: :json
    assert_response :ok
    res = get_json_response(response)['activity_streams']
    assert_equal 1, res.count
    assert_equal [org_as.id], res.map { |a| a['id'] }
  end

  test '#destroy should remove activity and org comment' do
    comment_org = create(
      :comment, message: 'org comment',
      commentable: @org, user: @user
    )
    org_as = create(
      :activity_stream, streamable: comment_org,
      organization: @org, user_id: @user.id, action: 'comment'
    )

    api_v2_sign_in(@user)
    delete :destroy, id: org_as.id, format: :json
    assert_response :no_content
    refute ActivityStream.exists?(id: org_as.id)
    refute Comment.exists?(id: comment_org.id)
  end

  test '#destroy should remove activity and team comment' do
    comment_team = create(
      :comment, message: 'team comment',
      commentable: @team, user: @user
    )
    team_as = create(
      :activity_stream, streamable: comment_team,
      organization: @org, user_id: @user.id, action: 'comment'
    )
    api_v2_sign_in(@user)
    delete :destroy, id: team_as.id, format: :json
    assert_response :no_content
    refute ActivityStream.exists?(id: team_as.id)
    refute Comment.exists?(id: comment_team.id)
  end

  test '#destroy should not destroy if user not author or admin' do
    comment_team = create(
      :comment, message: 'team comment',
      commentable: @team, user: @team_member
    )
    team_as = create(
      :activity_stream, streamable: comment_team,
      organization: @org, user_id: @team_member.id, action: 'comment'
    )
    api_v2_sign_in(@user)
    delete :destroy, id: team_as.id, format: :json
    assert_response :forbidden
  end

  test '#destroy should remove activity_stream if user admin' do
    admin = create(:user, organization: @org, organization_role: 'admin')
    comment_team = create(
      :comment, message: 'team comment',
      commentable: @team, user: @user
    )
    team_as = create(
      :activity_stream, streamable: comment_team,
      organization: @org, user_id: @user.id, action: 'comment'
    )
    api_v2_sign_in(admin)
    delete :destroy, id: team_as.id, format: :json
    assert_response :no_content
    refute ActivityStream.exists?(id: team_as.id)
    refute Comment.exists?(id: comment_team.id)
  end

  test '#destroy should destroy only streamable with commentable_type Team or Organization' do
    card = create(:card, author: @user)
    comment = create(:comment, commentable: card, user: @user)
    card_comment_as = create(
      :activity_stream, streamable: comment,
      organization: @org, user_id: @user.id, action: 'comment'
    )
    api_v2_sign_in(@user)
    delete :destroy, id: card_comment_as.id, format: :json
    assert_response :unprocessable_entity
  end

  test ':api index should return only user activities when user_id is passed' do
    card1, card2 = create_list(:card, 2, author: @user)
    card3, card4 = create_list(:card, 2, author: @viewer)

    activity1 = create(:activity_stream, streamable: card1, organization: @org, user_id: @user.id, card_id: card1.id, action: 'created_card')

    activity2 = create(:activity_stream, streamable: card2, organization: @org, user_id: @user.id, card_id: card2.id, action: 'created_card')

    activity3 = create(:activity_stream, streamable: card3, organization: @org, user_id: @viewer.id, card_id: card3.id, action: 'created_card')

    activity4 = create(:activity_stream, streamable: card4, organization: @org, user_id: @viewer.id, action: 'created_card')

    get :index, user_id: @user.id, format: :json
    assert_response :ok
    as = get_json_response(response)['activity_streams']

    assert_equal 2, as.count
    assert_same_elements [@user.id, @user.id], as.map { |item| item['user_id'] }
    assert_same_elements [activity1.id, activity2.id], as.map { |item| item['id'] }
  end

  test ':api index should return all activities when user_id and activity_actions are not passed' do
    card1, card2 = create_list(:card, 2, author: @user)
    card3, card4 = create_list(:card, 2, author: @viewer)

    vote = create(:vote, votable: @card, voter: @viewer)
    comment_card = create(:comment, message: 'basic comment', commentable: @card, user: @user)

    activity1 = create(:activity_stream, streamable: card1, organization: @org, user_id: @user.id, card_id: card1.id, action: 'created_card')

    activity2 = create(:activity_stream, streamable: card2, organization: @org, user_id: @user.id, card_id: card2.id, action: 'created_card')

    activity3 = create(:activity_stream, streamable: card3, organization: @org, user_id: @viewer.id, card_id: card3.id, action: 'created_card')

    activity4 = create(:activity_stream, streamable: card4, organization: @org, user_id: @viewer.id, action: 'created_card')

    activity5 = create(:activity_stream, streamable: vote, organization: @org, user_id: @viewer.id, card_id: @card.id, action: 'upvote')

    activity6 = create(:activity_stream, streamable: comment_card, organization: @org, user_id: @viewer.id, card_id: @card.id, action: 'comment')

    get :index, format: :json
    assert_response :ok
    as = get_json_response(response)['activity_streams']

    assert_equal 7, as.count
    assert_same_elements [activity1.id, activity2.id, activity3.id, activity4.id, activity5.id, activity6.id, @as_completed_card.id ], as.map { |item| item['id'] }
  end

  test ':api index should return only activities of particular actions when activity_actions is passed' do
    card1, card2 = create_list(:card, 2, author: @user)
    vote = create(:vote, votable: @card, voter: @viewer)
    comment_card = create(:comment, message: 'basic comment', commentable: @card, user: @user)

    activity1 = create(:activity_stream, streamable: card1, organization: @org, user_id: @user.id, card_id: @card.id, action: 'created_card')
    activity2 = create(:activity_stream, streamable: card2, organization: @org, user_id: @user.id, card_id: @card.id, action: 'created_card')

    activity3 = create(:activity_stream, streamable: vote, organization: @org, user_id: @viewer.id, card_id: @card.id, action: 'upvote')

    activity4 = create(:activity_stream, streamable: comment_card, organization: @org, user_id: @viewer.id, card_id: @card.id, action: 'comment')

    get :index, activity_actions: ['created_card', 'upvote'], format: :json
    assert_response :ok
    as = get_json_response(response)['activity_streams']

    assert_equal 3, as.count
    assert_same_elements [activity1.id, activity2.id, activity3.id], as.map { |item| item['id'] }
  end

  test ':api index should return only public activities when user_id is passed' do
    card1 = create(:card, author: @user, is_public: false)
    card2 = create(:card, author: @user, is_public: true)
    activity1 = create(:activity_stream, streamable: card1, organization: @org, user_id: @user.id, card_id: card1.id, action: 'created_card')
    activity2 = create(:activity_stream, streamable: card2, organization: @org, user_id: @user.id, card_id: card2.id, action: 'created_card')
    api_v2_sign_in(@viewer)
    get :index, user_id: @user.id , format: :json
    assert_response :ok
    as = get_json_response(response)['activity_streams']

    assert_equal 1, as.count
    assert_same_elements [activity2.id], as.map { |item| item['id'] }
  end

  test 'activity stream should fetch activity with cards that are restricted to the specific user' do
      card1, card2, card3 = create_list(:card, 3, author: @user, is_public: false)
      activity1 = create(:activity_stream, streamable: card1, organization: @org, user_id: @user.id, card_id: card1.id, action: 'created_card')
      activity2 = create(:activity_stream, streamable: card2, organization: @org, user_id: @user.id, card_id: card2.id, action: 'created_card')
       activity3 = create(:activity_stream, streamable: card3, organization: @org, user_id: @user.id, card_id: card3.id, action: 'created_card')
      create(:card_user_permission, card_id: card1.id, user_id: @viewer.id)
      api_v2_sign_in(@viewer)
      get :index, user_id: @user.id , format: :json
      assert_response :ok
      as = get_json_response(response)['activity_streams']
      assert_equal 1, as.count
      assert_same_elements [activity1.id], as.map { |item| item['id'] }
    end

    test 'activity stream should not fetch activity with cards that are not restricted to the user' do
      card1, card2, card3 = create_list(:card, 3, author: @user, is_public: false)
      activity1 = create(:activity_stream, streamable: card1, organization: @org, user_id: @user.id, card_id: card1.id, action: 'created_card')
      activity2 = create(:activity_stream, streamable: card2, organization: @org, user_id: @user.id, card_id: card2.id, action: 'created_card')
       activity3 = create(:activity_stream, streamable: card3, organization: @org, user_id: @user.id, card_id: card3.id, action: 'created_card')
      create(:card_user_permission, card_id: card1.id, user_id: @viewer.id)
      api_v2_sign_in(@team_member)
      get :index, user_id: @user.id , format: :json
      assert_response :ok
      as = get_json_response(response)['activity_streams']
      assert_equal 0, as.count
    end

    test 'activity stream should fetch activity with cards that are just restricted to the users teams' do
      card1, card2, card3 = create_list(:card, 3, author: @user, is_public: false)
      create(:teams_user, team_id: @team.id, user_id: @viewer.id)
      create(:card_team_permission, card_id: card2.id, team_id: @team.id)
      activity1 = create(:activity_stream, streamable: card1, organization: @org, user_id: @user.id, card_id: card1.id, action: 'created_card')
      activity2 = create(:activity_stream, streamable: card2, organization: @org, user_id: @user.id, card_id: card2.id, action: 'created_card')
       activity3 = create(:activity_stream, streamable: card3, organization: @org, user_id: @user.id, card_id: card3.id, action: 'created_card')
      api_v2_sign_in(@viewer)
      get :index, user_id: @user.id , format: :json
      assert_response :ok
      as = get_json_response(response)['activity_streams']
      assert_equal 1, as.count
      assert_same_elements [activity2.id], as.map { |item| item['id'] }
    end

    test 'activity stream should fetch all activities for org admin user' do
      admin_user = create(:user, organization: @org, organization_role: 'admin')
      card1, card2, card3 = create_list(:card, 3, author: @user, is_public: false)
      activity1 = create(:activity_stream, streamable: card1, organization: @org, user_id: @user.id, card_id: card1.id, action: 'created_card')
      activity2 = create(:activity_stream, streamable: card2, organization: @org, user_id: @user.id, card_id: card2.id, action: 'created_card')
       activity3 = create(:activity_stream, streamable: card3, organization: @org, user_id: @user.id, card_id: card3.id, action: 'created_card')
      api_v2_sign_in(admin_user)
      get :index, user_id: @user.id , format: :json
      assert_response :ok
      as = get_json_response(response)['activity_streams']
      assert_equal 3, as.count
      assert_same_elements [activity1.id, activity2.id, activity3.id], as.map { |item| item['id'] }
    end

  test 'should return info about votes if card not hidden' do
    media_cards = create_list(:card, 2, author: @user, hidden: true)
    pack_card1 = create(:card, author: @user, card_type: 'pack')
    pack_card2 = create(:card, author: @user, card_type: 'pack', card_subtype: 'journey')
    journey_card = create(:card, author: @user, card_type: 'journey', card_subtype: 'self_paced')
    pack_card1.add_card_to_pathway(media_cards[0].id, 'Card')
    pack_card2.add_card_to_pathway(media_cards[1].id, 'Card')
    journey_card.add_card_to_journey(pack_card2.id)
    cards = [media_cards, pack_card1, pack_card2, journey_card].flatten
    cards.each do |card|
      vote = create(:vote, votable: card, voter: @viewer )
      create(:activity_stream, streamable: vote, organization: @org, user_id: @viewer.id, card_id: card.id, action: 'upvote')
    end

    api_v2_sign_in(@user)
    get :index, format: :json
    assert_response :ok
    as = get_json_response(response)['activity_streams']

    assert_equal 3, as.length
  end

  test 'activity stream should not fetch activity of private content for member user' do
    org = create(:organization)
    user, viewer = create_list(:user, 2, organization: org)
    card2 = create(:card, author: user, is_public: false)
    vote = create(:vote, votable: card2, voter: user)
    comment_card = create(:comment, message: 'basic comment', commentable: card2, user: user)
    ucu = create(:user_content_completion, completable: card2, user: user, state: 'started')

    create(:activity_stream, streamable: card2, organization: org, user_id: user.id, card_id: card2.id, action: 'created_card')

    create(:activity_stream, streamable: vote, organization: org, user_id: user.id, card_id: card2.id, action: 'upvote')

    create(:activity_stream, streamable: comment_card, organization: org, user_id: user.id, card_id: card2.id, action: 'comment')
   
    create(:activity_stream, streamable: ucu, organization: org, user_id: user.id, card_id: card2.id, action: 'smartbite_uncompleted')

    api_v2_sign_in(viewer)
    get :index, format: :json
    assert_response :ok
    as = get_json_response(response)['activity_streams']

    assert_equal 0, as.count
  end

  test 'activity stream should fetch  for member user if activity of private content which is restricted to member user' do
    org = create(:organization)
    user, viewer = create_list(:user, 2, organization: org)
    card2 = create(:card, author: user, is_public: false)
    create(:card_user_permission, card_id: card2.id, user_id: viewer.id)

    vote = create(:vote, votable: card2, voter: user)
    comment_card = create(:comment, message: 'basic comment', commentable: card2, user: user)
    ucu = create(:user_content_completion, completable: card2, user: user, state: 'started')

    create(:activity_stream, streamable: card2, organization: org, user_id: user.id, card_id: card2.id, action: 'created_card')

    create(:activity_stream, streamable: vote, organization: org, user_id: user.id, card_id: card2.id, action: 'upvote')

    create(:activity_stream, streamable: comment_card, organization: org, user_id: user.id, card_id: card2.id, action: 'comment')
   
    create(:activity_stream, streamable: ucu, organization: org, user_id: user.id, card_id: card2.id, action: 'smartbite_uncompleted')

    api_v2_sign_in(viewer)
    get :index, format: :json
    assert_response :ok
    as = get_json_response(response)['activity_streams']

    assert_equal 4, as.count
  end

  test 'activity stream should fetch  for member user if activity of private content which is shared to member user' do
    org = create(:organization)
    user, viewer = create_list(:user, 2, organization: org)
    card2 = create(:card, author: user, is_public: false)
    create(:card_user_share, card_id: card2.id, user_id: viewer.id)
    vote = create(:vote, votable: card2, voter: user)
    comment_card = create(:comment, message: 'basic comment', commentable: card2, user: user)
    ucu = create(:user_content_completion, completable: card2, user: user, state: 'started')

    create(:activity_stream, streamable: card2, organization: org, user_id: user.id, card_id: card2.id, action: 'created_card')

    create(:activity_stream, streamable: vote, organization: org, user_id: user.id, card_id: card2.id, action: 'upvote')

    create(:activity_stream, streamable: comment_card, organization: org, user_id: user.id, card_id: card2.id, action: 'comment')
   
    create(:activity_stream, streamable: ucu, organization: org, user_id: user.id, card_id: card2.id, action: 'smartbite_uncompleted')

    api_v2_sign_in(viewer)
    get :index, format: :json
    assert_response :ok
    as = get_json_response(response)['activity_streams']

    assert_equal 4, as.count
  end

  test 'activity stream should not fetch for viewer if private content is shared to group but only restricted to team_member' do
    org = create(:organization)
    user, viewer, team_member = create_list(:user, 3, organization: org)
    team = create(:team, organization: org)
    create(:teams_user, team_id: team.id, user_id: user.id)
    create(:teams_user, team_id: team.id, user_id: viewer.id)
    create(:teams_user, team_id: team.id, user_id: team_member.id)

    card2 = create(:card, author: user, is_public: false)
    create(:teams_card, card: card2, team: team, type: 'SharedCard', user: user)
    create(:card_user_permission, card_id: card2.id, user_id: team_member.id)

    vote = create(:vote, votable: card2, voter: user)
    comment_card = create(:comment, message: 'basic comment', commentable: card2, user: user)
    ucu = create(:user_content_completion, completable: card2, user: user, state: 'started')

    create(:activity_stream, streamable: card2, organization: org, user_id: user.id, card_id: card2.id, action: 'created_card')

    create(:activity_stream, streamable: vote, organization: org, user_id: user.id, card_id: card2.id, action: 'upvote')

    create(:activity_stream, streamable: comment_card, organization: org, user_id: user.id, card_id: card2.id, action: 'comment')
   
    create(:activity_stream, streamable: ucu, organization: org, user_id: user.id, card_id: card2.id, action: 'smartbite_uncompleted')
    api_v2_sign_in(viewer)
    get :index, format: :json
    assert_response :ok
    as = get_json_response(response)['activity_streams']
    assert_equal 0, as.count
  end
end

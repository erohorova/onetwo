require 'test_helper'

class Api::V2::AwsNotificationsControllerTest < ActionController::TestCase

  setup do
    TranscoderCompleteJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    stub_request(:get, "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-d6d679a1d18e95c2f9ffcf11f4f9e198.pem").
        with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
        to_return(:status => 200, :body => "", :headers => {})

    aws_client = AWS::ElasticTranscoder::Client.new

    AWS::ElasticTranscoder::Client.stubs(:new).returns(aws_client)

    create_job_mock = mock()
    create_job_mock.stubs(:data).returns({job: {id:'job123'}})
    aws_client.stubs(:create_job).returns(create_job_mock)
  end

  test 'should log the subscribe URL' do
    payload = '{
      "Type" : "SubscriptionConfirmation",
      "MessageId" : "fee65219-dde0-4ec4-ab76-b6d454eccee1",
      "Token" : "2336412f37fb687f5d51e6e241d7700ae02f705eccd53d3bda8f217b4f56ddaeaf0f493de4d0725748b561e805bc40455550c8bf723278cae068eb0e0b9c5617d472913ad4a2e10e0b1d4f1d6813d074aba04da57bb18aae3b4c071ccdf4463b7f71426a39860dfa2a5212152c6495e7e3d1b326b1c1e91075dec5349f439d5601553e69628db2891bab1bc888b2d90b",
      "TopicArn" : "arn:aws:sns:us-east-1:738572153508:edcast-qa-video-stream-notifications",
      "Message" : "You have chosen to subscribe to the topic arn:aws:sns:us-east-1:738572153508:edcast-qa-video-stream-notifications.\nTo confirm the subscription, visit the SubscribeURL included in this message.",
      "SubscribeURL" : "https://sns.us-east-1.amazonaws.com/?Action=ConfirmSubscription&TopicArn=arn:aws:sns:us-east-1:738572153508:edcast-qa-video-stream-notifications&Token=2336412f37fb687f5d51e6e241d7700ae02f705eccd53d3bda8f217b4f56ddaeaf0f493de4d0725748b561e805bc40455550c8bf723278cae068eb0e0b9c5617d472913ad4a2e10e0b1d4f1d6813d074aba04da57bb18aae3b4c071ccdf4463b7f71426a39860dfa2a5212152c6495e7e3d1b326b1c1e91075dec5349f439d5601553e69628db2891bab1bc888b2d90b",
      "Timestamp" : "2015-07-08T18:15:52.734Z",
      "SignatureVersion" : "1",
      "Signature" : "jpE/cO3jf3m0vbI8EAvpQHgXeUL7LsE92MzUyDE3LCzCjDbV9EmIC/iKTawsITcQ9kZonw8MZMoJzHkgopMew5L7AI7MTXv6vbg/qKcWGmHWJNUbzdxhUMjVwt1fq2dY0oIKnupRach8dQA+GIt7LJ4tlBEBNUc6YRjvO/TPFo4gMDCj1qFbJcC/Wkf/kN/DBc8n14rX1tSKF6E2zZupzrh8S15we0u7rt68DMN9vzGyZaidoJrcdbT/xe9fAYzS0TweHJqmbfR39VNd7p+1sSPJKFWnAnwtcBEs7JnDlhmDw0D7K8+8osrQE5UoopPk0Cu+6UKd0qRuXriurQOF8Q==",
      "SigningCertURL" : "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-d6d679a1d18e95c2f9ffcf11f4f9e198.pem"
      }'

    stub_request(:get, "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-d6d679a1d18e95c2f9ffcf11f4f9e198.pem").
        with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
        to_return(:status => 200, :body => "", :headers => {})

    @request.env['RAW_POST_DATA'] = payload
    aws_message = AWS::SNS::Message.new(payload)
    aws_message.expects(:authentic?).returns(true)
    AWS::SNS::Message.expects(:new).with(payload).returns(aws_message)

    log.expects(:error).with('AWS SNS SubscribeURL: https://sns.us-east-1.amazonaws.com/?Action=ConfirmSubscription&TopicArn=arn:aws:sns:us-east-1:738572153508:edcast-qa-video-stream-notifications&Token=2336412f37fb687f5d51e6e241d7700ae02f705eccd53d3bda8f217b4f56ddaeaf0f493de4d0725748b561e805bc40455550c8bf723278cae068eb0e0b9c5617d472913ad4a2e10e0b1d4f1d6813d074aba04da57bb18aae3b4c071ccdf4463b7f71426a39860dfa2a5212152c6495e7e3d1b326b1c1e91075dec5349f439d5601553e69628db2891bab1bc888b2d90b')
    post :create
  end


  test 'should change the state to available' do
    payload = '{
      "Type" : "Notification",
      "MessageId" : "86b13653-d79d-57f3-996f-78447daec7b9",
      "TopicArn" : "arn:aws:sns:us-east-1:738572153508:edcast-qa-video-stream-notifications",
      "Subject" : "Amazon Elastic Transcoder has finished transcoding job 1436395492039-14i9kt.",
      "Message" : "{\n  \"state\" : \"COMPLETED\",\n  \"version\" : \"2012-09-25\",\n  \"jobId\" : \"1436395492039-14i9kt\",\n  \"pipelineId\" : \"1436291265781-7tpdna\",\n  \"input\" : {\n    \"key\" : \"video_streams/590/7b946b63-565a-4ae6-aaae-68e4efb05a61/Movie.mp4\"\n  },\n  \"outputKeyPrefix\" : \"video-streams-output/47/\",\n  \"outputs\" : [ {\n    \"id\" : \"1\",\n    \"presetId\" : \"1351620000001-200030\",\n    \"key\" : \"c5ca376cf8c403bece0828b24decf936f48d7a8750c778960f50d10e7ad29fbd\",\n    \"segmentDuration\" : 1.0,\n    \"status\" : \"Complete\",\n    \"statusDetail\" : \"Some individual segment files for this output have a higher bit rate than the average bit rate of the transcoded media. Playlists including this output will record a higher bit rate than the rate specified by the preset.\",\n    \"duration\" : 6,\n    \"width\" : 242,\n    \"height\" : 432\n  }, {\n    \"id\" : \"2\",\n    \"presetId\" : \"1351620000001-100020\",\n    \"key\" : \"c5ca376cf8c403bece0828b24decf936f48d7a8750c778960f50d10e7ad29fbd.mp4\",\n    \"status\" : \"Complete\",\n    \"duration\" : 6,\n    \"width\" : 320,\n    \"height\" : 568\n  } ],\n  \"userMetadata\" : {\n    \"recording_id\" : \"47\"\n  }\n}",
      "Timestamp" : "2015-07-08T22:45:07.632Z",
      "SignatureVersion" : "1",
      "Signature" : "bVmn7/Gc9h/4+ZHhjTxOSRe/vVGkmwPzYNo4yqC+UQkgnQ7dLFe5mVl0bUUVzD+x7SBEqRMSkY3sdaHf8kHQNmCF/XPQPponP8P+ZvAxI8yTT3y4brRTI5qSPhqyXUw2kFKEf0cneCGn+kRm47k0CAgxyxWiCqCqbJCO4GEcdk5f4ysrGut+GgNw2hsei/Ola1JJVWIbayadaNeN4fLmGf//FOrmwRuwojR9k4jF2y2Loq1hqxV4JwfxqmyheIYqPoUUEuvjUlx034Y9dErSV0p/MFb2UgkrulmZ0jG7z7aXny2cPVLAlIzxgE5Q9S3kOORMPm/Z/3gAL+YmMi2I2w==",
      "SigningCertURL" : "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-d6d679a1d18e95c2f9ffcf11f4f9e198.pem",
      "UnsubscribeURL" : "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:738572153508:edcast-qa-video-stream-notifications:feb3fe10-1e8d-48c0-8fa2-b5d1424385d0"
      }'


    @request.env['RAW_POST_DATA'] = payload
    aws_message = AWS::SNS::Message.new(payload)
    aws_message.expects(:authentic?).returns(true)
    AWS::SNS::Message.expects(:new).with(payload).returns(aws_message)

    v = create(:wowza_video_stream, status: 'past')
    recording = create(:recording, id: 47, video_stream: v)
    VideoStream.any_instance.stubs(:reindex)#expects(:reindex).returns(nil).once

    post :create
    recording.reload
    assert_equal recording.last_aws_transcoding_message, aws_message.message
    assert_equal 'available', recording.transcoding_status
    assert_equal 'video-streams-output/47/c5ca376cf8c403bece0828b24decf936f48d7a8750c778960f50d10e7ad29fbd.m3u8', recording.hls_location
    assert_equal 'video-streams-output/47/c5ca376cf8c403bece0828b24decf936f48d7a8750c778960f50d10e7ad29fbd.mp4', recording.mp4_location
  end

  test 'should change to the state to error' do
    payload = '{   "Type" : "Notification",
      "MessageId" : "f7085825-3a46-588c-952b-16328bdac9d7",
      "TopicArn" : "arn:aws:sns:us-east-1:738572153508:edcast-qa-video-stream-notifications",
      "Subject" : "The Amazon Elastic Transcoder job 1436397395205-ox3os2 has failed.",
      "Message" : "{\n  \"state\" : \"ERROR\",\n  \"version\" : \"2012-09-25\",\n  \"jobId\" : \"1436397395205-ox3os2\",\n  \"pipelineId\" : \"1436291265781-7tpdna\",\n  \"input\" : {\n    \"key\" : \"video_streams/585/c87e1491-2127-4f7c-b175-3b11a390e051/test3.mov\",\n    \"frameRate\" : \"auto\",\n    \"resolution\" : \"auto\",\n    \"aspectRatio\" : \"auto\",\n    \"interlaced\" : \"auto\",\n    \"container\" : \"auto\"\n  },\n  \"outputKeyPrefix\" : \"video-streams-output/\",\n  \"outputs\" : [ {\n    \"id\" : \"1\",\n    \"presetId\" : \"1351620000001-200055\",\n    \"key\" : \"67893a236cfcf44c773dbb8d6621d6204bf21e1b31438da0357f7e06b96f4a9f.ts\",\n    \"thumbnailPattern\" : \"\",\n    \"rotate\" : \"auto\",\n    \"status\" : \"Error\",\n    \"statusDetail\" : \"4000 3af6aee8-01bc-44b1-a437-518f003929fc: Amazon Elastic Transcoder could not interpret the media file.\",\n    \"errorCode\" : 4000\n  }, {\n    \"id\" : \"2\",\n    \"presetId\" : \"1351620000001-100020\",\n    \"key\" : \"67893a236cfcf44c773dbb8d6621d6204bf21e1b31438da0357f7e06b96f4a9f.mp4\",\n    \"thumbnailPattern\" : \"\",\n    \"rotate\" : \"auto\",\n    \"status\" : \"Error\",\n    \"statusDetail\" : \"4000 20b95d70-eee2-4154-8953-f260ed576ba3: Amazon Elastic Transcoder could not interpret the media file.\",\n    \"errorCode\" : 4000\n  } ],\n  \"userMetadata\" : {\n    \"recording_id\" : \"47\"\n  }\n}",
      "Timestamp" : "2015-07-08T23:16:44.159Z",
      "SignatureVersion" : "1",
      "Signature" : "c9/kG8G5Sz5br82A8sJhVDGMqi2L18LJR9udRmrrUmSGNHVMeRlDLRGRoJSAo4aaf4NV4Fx1cV3KJPzFvnoyKn7g3VZpqsoGk5rWYO3fJ9SILrIi6mq2y1jbY8W05sSVvI+aHVHsLQeF5pVFnt/CxtHw+1XzpYnbLFS3YFtb0jjvgnKv9iTzh4c47kfv+0HAH78GdjPnCzyc9EspqMFvcBIig8nxKcmQKCJtd0lawzFFwo94rcJqEx6jwG8D0f8v6UvB/09BNFtZZFnS70D6bhlxgvAR9FgDREzZA+sNZ3Avrr2g5Qcef38gPqslXHlfVe1gWPep47Peevz1Go52PQ==",
      "SigningCertURL" : "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-d6d679a1d18e95c2f9ffcf11f4f9e198.pem",
      "UnsubscribeURL" : "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:738572153508:edcast-qa-video-stream-notifications:feb3fe10-1e8d-48c0-8fa2-b5d1424385d0"
      }'


    @request.env['RAW_POST_DATA'] = payload
    aws_message = AWS::SNS::Message.new(payload)
    aws_message.expects(:authentic?).returns(true)
    AWS::SNS::Message.expects(:new).with(payload).returns(aws_message)

    recording = create(:recording, id: 47)

    post :create
    recording.reload
    assert_equal recording.last_aws_transcoding_message, aws_message.message
    assert_equal 'error', recording.transcoding_status
  end

  test 'should change to the state to transcoding' do
    payload = '{   "Type" : "Notification",
      "MessageId" : "cb8659a1-4285-5265-861e-17b6f313906c",
      "TopicArn" : "arn:aws:sns:us-east-1:738572153508:edcast-qa-video-stream-notifications",
      "Subject" : "Amazon Elastic Transcoder has scheduled job 1436397395205-ox3os2 for transcoding.",
      "Message" : "{\n  \"state\" : \"PROGRESSING\",\n  \"version\" : \"2012-09-25\",\n  \"jobId\" : \"1436397395205-ox3os2\",\n  \"pipelineId\" : \"1436291265781-7tpdna\",\n  \"input\" : {\n    \"key\" : \"video_streams/585/c87e1491-2127-4f7c-b175-3b11a390e051/test3.mov\",\n    \"frameRate\" : \"auto\",\n    \"resolution\" : \"auto\",\n    \"aspectRatio\" : \"auto\",\n    \"interlaced\" : \"auto\",\n    \"container\" : \"auto\"\n  },\n  \"outputKeyPrefix\" : \"video-streams-output/\",\n  \"outputs\" : [ {\n    \"id\" : \"1\",\n    \"presetId\" : \"1351620000001-200055\",\n    \"key\" : \"67893a236cfcf44c773dbb8d6621d6204bf21e1b31438da0357f7e06b96f4a9f.ts\",\n    \"thumbnailPattern\" : \"\",\n    \"rotate\" : \"auto\",\n    \"status\" : \"Progressing\"\n  }, {\n    \"id\" : \"2\",\n    \"presetId\" : \"1351620000001-100020\",\n    \"key\" : \"67893a236cfcf44c773dbb8d6621d6204bf21e1b31438da0357f7e06b96f4a9f.mp4\",\n    \"thumbnailPattern\" : \"\",\n    \"rotate\" : \"auto\",\n    \"status\" : \"Progressing\"\n  } ],\n  \"userMetadata\" : {\n    \"recording_id\" : \"47\"\n  }\n}",
      "Timestamp" : "2015-07-08T23:16:38.353Z",
      "SignatureVersion" : "1",
      "Signature" : "kmeBJxPJ828VGAfDOKmnpJ3iRLXWXd2W2F9ia6vgg5U+K8tvso8nVhdquQ8xRRODSi2jHXsWS6uqr/vi4UQFdEGECmH2UBtOyEZrkrNGHiQwkKS3l3f3lFubWvCmuj1/tUk6XxlrEL5RukrCpdQtchUt6xacoIQS2ktQciK2O4JgcLuKSTA492Etx4jCCVAjyrFoRfENu49X5OGnsp2oeptKbiEhkEiSSRvpBtYV9BnERusyHsl3hycCY8uktbqK/MSgO7OnmU3QDca+GdZlc7R8yijINXVlaU80qGM1UZqZvL4tNMtEZ1jNZd1U34P1C4kQ8r4f9alJtcaI9YVW5w==",
      "SigningCertURL" : "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-d6d679a1d18e95c2f9ffcf11f4f9e198.pem",
      "UnsubscribeURL" : "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:738572153508:edcast-qa-video-stream-notifications:feb3fe10-1e8d-48c0-8fa2-b5d1424385d0"
      }'


    @request.env['RAW_POST_DATA'] = payload
    aws_message = AWS::SNS::Message.new(payload)
    aws_message.expects(:authentic?).returns(true)
    AWS::SNS::Message.expects(:new).with(payload).returns(aws_message)

    recording = create(:recording, id: 47)

    post :create
    recording.reload
    assert_equal recording.last_aws_transcoding_message, aws_message.message
    assert_equal 'transcoding', recording.transcoding_status
  end

end

require 'test_helper'

class Api::V2::LeapsControllerTest < ActionController::TestCase
  setup do
    org = create(:organization)
    @user = create(:user, organization: org)
    api_v2_sign_in @user

    @cover = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user)
    @card1, @card2, @card3 = create_list(:card, 3, author: @user)
    @cards = @cover, @card1, @card2, @card3

    [@card1, @card2, @card3].each do |card|
      CardPackRelation.add(cover_id: @cover.id, add_id: card.id, add_type: card.class.name)
    end
    @poll = create(:card, card_type: 'poll', card_subtype: 'text', author: @user)
    CardPackRelation.add(cover_id: @cover.id, add_id: @poll.id, add_type: 'Card')
  end

  class LeapsControllerCreateTest < Api::V2::LeapsControllerTest
    test ':api should create standalone leap' do
      assert_difference -> {Leap.count}, 1 do
        post :create, card_id: @poll.id, leap: {wrong_id: @card1.id, correct_id: @card2.id}, format: :json
      end
      assert_response :ok
    end

    test ':api should create pathway leap' do
      assert_difference -> {Leap.count}, 1 do
        post :create, card_id: @poll.id, leap: {pathway_id: @cover.id, wrong_id: @card1.id, correct_id: @card2.id}, format: :json
      end
      assert_response :ok
    end

    test ':api should create leap for ecl id' do
      ecl_id = "1234-3456-23452"
      stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id}").
          to_return(status: 200, body: get_ecl_card_response(ecl_id).to_json)

      ecl_card = create(:card, organization: @user.organization, message: 'ecl_card', card_type: 'media', author: @user, ecl_id: ecl_id)
      assert_difference -> {Leap.count}, 1 do
        post :create, card_id: @poll.id, leap: {pathway_id: nil, wrong_id: "ECL-#{ecl_card.ecl_id}", correct_id: @card2.id}, format: :json
      end
      assert_response :ok
    end
  end

  class LeapControllerDestroyTest < Api::V2::LeapsControllerTest
    test ':api should delete leap' do
      @leap1 = create(:leap, card_id: @poll.id, pathway_id: @cover.id, wrong_id: @card1.id, correct_id: @card2.id)
      @leap2 = create(:leap, card_id: @poll.id, wrong_id: @card1.id, correct_id: @card2.id)
      assert_difference -> {Leap.count}, -1 do
        delete :destroy, card_id: @poll.id, id: @leap1.id, format: :json
      end
      assert_response :ok
    end
  end

  class LeapControllerUpdateTest < Api::V2::LeapsControllerTest
    test ':api should update leap' do
      @leap = create(:leap, card_id: @poll.id, pathway_id: @cover.id, wrong_id: @card1.id, correct_id: @card2.id)
      put :update, card_id: @poll.id, id: @leap.id, leap: {wrong_id: @card2.id, correct_id: @card3.id}, format: :json
      assert_response :ok
    end
  end
end

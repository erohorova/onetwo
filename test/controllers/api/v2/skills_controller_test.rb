require 'test_helper'

class Api::V2::SkillsControllerTest < ActionController::TestCase

  class CreateTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      api_v2_sign_in @user
    end

    test ':api should create skill' do
      assert_difference -> {Skill.count} do
        assert_difference -> {SkillsUser.count} do
          post :create, skill: {name: 'test', description: 'test_decription'}, format: :json
        end
      end
    end

    test ':api should return unprocessable_entity if user try to create skill with non unique name' do
      @skill = create(:skill, name: 'test')
      skill_params = {
        description: 'test_decription',
        credential_name: 'Super name',
        credential_url: 'Super url'
      }
      @user.add_skill(@skill, skill_params)
      post :create, skill: {name: 'test', description: 'test_decription'}, format: :json
      assert_response :unprocessable_entity
    end

    test ':api should save credential_name and credential_url and expiry_date' do
      post :create, skill: {name: 'test',
                            description: 'test_decription',
                            credential_name: 'Super name',
                            credential_url: 'Super url',
                            expiry_date: Date.new(Date.current.year,01,01) }, format: :json
      assert_response :ok
      skill_user = SkillsUser.last
      assert_equal 'Super name', skill_user.credential_name
      assert_equal 'Super url', skill_user.credential_url
      assert_equal Date.new(Date.current.year,01,01), skill_user.expiry_date
    end
  end

  class UpdateTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      @skill = create(:skill, name: 'test skill')
      @skill_user = create(:skills_user, skill: @skill, user: @user,
                           description: 'test skill description', experience: '1 year',
                           credential: [], skill_level: 'beginner',
                           credential_name: 'credential name',
                           credential_url: 'credential url',
                           expiry_date: Date.new(Date.current.year,01,01))
      api_v2_sign_in @user
    end

    test ':api should update user skill' do
      put :update, id: @skill_user.id, skill: {description: 'super skill',
                                               experience: '2 years',
                                               skill_level: 'advanced',
                                               credential_name: 'super name',
                                               credential_url: 'super url',
                                               name: 'Super Skill name',
                                               expiry_date: Date.new(Date.current.year,01,02) }
      assert_response :ok
      @skill_user.reload
      @skill.reload
      assert_equal 'Super Skill name', @skill.name
      assert_equal 'super skill', @skill_user.description
      assert_equal '2 years', @skill_user.experience
      assert_equal 'advanced', @skill_user.skill_level
      assert_equal 'super name', @skill_user.credential_name
      assert_equal 'super url', @skill_user.credential_url
      assert_equal Date.new(Date.current.year,01,02), @skill_user.expiry_date
    end

    test ':api should not update user skill when same skill name taken' do
      @skill2 = create(:skill, name: 'test skill two')
      @skill_user2 = create(:skills_user, skill: @skill2, user: @user,
                           description: 'test skill description', experience: '1 year',
                           credential: [], skill_level: 'beginner',
                           credential_name: 'credential name',
                           credential_url: 'credential url')
      put :update, id: @skill_user2.id, skill: {description: 'super skill',
                                               experience: '2 years',
                                               skill_level: 'advanced',
                                               credential_name: 'super name',
                                               credential_url: 'super url',
                                               name: 'test skill' }
      assert_response :unprocessable_entity
    end

    test ':api should return not_found if skill is not present' do
      put :update, id: 555, skill: {description: 'super skill',
                                    experience: '2 years',
                                    skill_level: 'advanced',
                                    credential_name: 'super name',
                                    credential_url: 'super url'}
      assert_response  :not_found
    end
  end

  class IndexTest < ActionController::TestCase
    setup do
      org = create(:organization)
      @user = create(:user, organization: org)
      api_v2_sign_in @user
      @skills = create(:skill, name: 'test')
    end

    test ':api should return skills with search term' do
      get :index, q: 'test', format: :json
      resp = get_json_response response
      assert_equal 1, resp['skills'].length
    end

    test 'should return skills only matched one' do
      get :index, q: 'edcast', format: :json
      resp = get_json_response response
      assert_equal 0, resp['skills'].length
    end

    test 'should not return skills without search term' do
      get :index, format: :json
      assert_response :bad_request
    end
  end
end
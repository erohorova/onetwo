require 'test_helper'

class Api::V2::TranslationLanguagesControllerTest < ActionController::TestCase
  test ":api #index lists all available languages with code" do
    get :index, format: :json
    resp_body = JSON.parse(response.body)['body']
    assert_equal LANGUAGES.keys.map(&:to_s), resp_body.keys
    assert_equal LANGUAGES.values, resp_body.values
  end
end

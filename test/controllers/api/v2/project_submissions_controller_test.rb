require 'test_helper'

class Api::V2::ProjectSubmissionsControllerTest < ActionController::TestCase
  test ':api should create project submission' do
    org = create(:organization)
    user = create(:user, organization: org)
    api_v2_sign_in(user)
    author = create(:user, organization: org)
    card = create(:card, organization: org,
                  author: author,
                  card_type: 'project',
                  card_subtype: 'text',
                  project_attributes: {reviewer_id: user.id})
    assert_difference -> {ProjectSubmission.count} do
      post :create, project_submission: {description: "something", project_id: card.project.id}, format: :json
      assert_response :ok
    end
  end

  test 'should not create project submission' do
    org = create(:organization)
    user = create(:user, organization: org)

    author = create(:user, organization: org)
    card = create(:card, organization: org,
                  author: author,
                  card_type: 'project',
                  card_subtype: 'text',
                  project_attributes: {reviewer_id: user.id})

    post :create, project_submission: {description: "something", project_id: card.project.id}, format: :json
    assert_response :unauthorized
  end

  test ':api should list project submission for a project' do
    org = create(:organization)
    reviewer = create(:user, organization: org)
    submitter = create(:user, organization: org)

    api_v2_sign_in(submitter)
    card = create(:card, organization: org,
                  author: reviewer,
                  card_type: 'project',
                  card_subtype: 'text')
    create(:project_submission, description: "something", project_id: card.project.id, submitter_id: submitter.id)
    get :index, project_id: card.project.id, format: :json

    resp = get_json_response(response)
    assert_equal card.project.id, resp["project_submissions"][0]["project_id"]
    assert_equal submitter.id, resp["project_submissions"][0]["submitter_id"]
  end

  test 'api should return all submissions of a project' do
    org = create(:organization)
    reviewer = create(:user, organization: org)
    submitter = create(:user, organization: org)

    api_v2_sign_in(submitter)
    card = create(:card, organization: org,
                  author: reviewer,
                  card_type: 'project',
                  card_subtype: 'text')
    submission1 = create(:project_submission, description: "submission1", project_id: card.project.id, submitter_id: submitter.id)
    submission2 = create(:project_submission, description: "submission2", project_id: card.project.id, submitter_id: submitter.id)
    create(:project_submission_review, description: "review", status: 1, reviewer_id: reviewer.id, project_submission_id: submission1.id)

    get :index, project_id: card.project.id, filter: 'all', format: :json
    resp = get_json_response(response)

    assert_equal 2, resp['project_submissions'].count
    assert_equal card.project.id, resp['project_submissions'][0]["project_id"]
    assert_equal card.project.id, resp['project_submissions'][1]["project_id"]
  end

  test 'api should return no_content for project not belonging to user' do
    org = create(:organization)
    reviewer = create(:user, organization: org)
    submitter = create(:user, organization: org)

    api_v2_sign_in(reviewer)
    card = create(:card, organization: org,
                  author: reviewer,
                  card_type: 'project',
                  card_subtype: 'text')
    submission = create(:project_submission, description: "submission", project_id: card.project.id, submitter_id: submitter.id)

    get :index, project_id: card.project.id, filter: 'all', format: :json
    get_json_response(response)

    assert_response :no_content
  end

  test 'should list project submission for projects with filter, limit and offset' do
    org = create(:organization)
    reviewer = create(:user, organization: org)

    api_v2_sign_in(reviewer)
    card = create(:card, organization: org,
                  author: reviewer,
                  card_type: 'project',
                  card_subtype: 'text')

    users = create_list(:user, 10, organization: org)
    users.each_with_index do |user|
      create(:project_submission, project_id: card.project.id, submitter_id: user.id)
    end

    get :index, filter: "reviewer", project_id: card.project.id, limit: 5, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal 5, resp["project_submissions"].count
    assert_same_elements ProjectSubmission.last(5).map{ |ps| ps.submitter_id}.sort, resp["project_submissions"].map {|ps| ps["submitter_id"]}.sort

    get :index, filter: "reviewer", project_id: card.project.id, limit: 5, offset: 5, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal 5, resp["project_submissions"].count
    assert_same_elements ProjectSubmission.first(5).map{ |ps| ps.submitter_id}.sort, resp["project_submissions"].map {|ps| ps["submitter_id"]}.sort
  end
end

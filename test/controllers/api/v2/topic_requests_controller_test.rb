require 'test_helper'

class Api::V2::TopicRequestsControllerTest < ActionController::TestCase
  class TopicRequestCreateTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      api_v2_sign_in @user
    end

    test 'api should create topic requests' do
      post :create, topic:{ label: 'New topic'}, format: :json
      assert_response :ok
    end

    test 'api should not create topic requests if similar topic exits' do
      create(:topic_request, organization: @org, label: "New Topic", requestor: @user)
      post :create, topic:{ label: 'New topic'}, format: :json
      assert_response :unprocessable_entity
    end

    test 'api should not create topic requests if label is missing ' do
      post :create, topic:{}, format: :json
      assert_response :bad_request
    end

    test 'api should create topic requests with initial state pending ' do
      post :create, topic:{ label: 'New topic'}, format: :json
      topic_request = TopicRequest.first
      assert_response :ok
      assert_equal 'pending', topic_request.state
    end
  end
  
  class TopicRequestActionTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      @admin_user = create(:user, organization_role: 'admin', organization: @org)
    end

    test 'api should approve topic request with given id if user is admin' do
      topic_request = create(:topic_request, organization_id: @org.id, label: "test")
      api_v2_sign_in @admin_user
      put :approve, id: topic_request.id, format: :json
      assert_response :ok
      topic_request.reload
      assert topic_request.approved?
      assert_equal topic_request.approver, @admin_user
    end

    test 'api should render_not_found if topic request is not found' do
      topic_request = create(:topic_request, organization_id: @org.id, label: "test")
      api_v2_sign_in @admin_user
      put :approve,id: 2, format: :json
      assert_response :not_found
    end

    test 'api should not approve topic request with given id if user is non-admin' do
      topic_request = create(:topic_request, organization_id: @org.id, label: "test")
      api_v2_sign_in @user
      put :approve, id: topic_request.id, format: :json
      assert_response :unauthorized
      topic_request.reload
      assert_equal topic_request.state , "pending"
    end

    test 'api should reject topic request with given id if user is admin' do
      topic_request = create(:topic_request, organization_id: @org.id, label: "test")
      api_v2_sign_in @admin_user
      put :reject, id: topic_request.id, format: :json
      assert_response :ok
      topic_request.reload
      assert topic_request.rejected?
      assert_equal topic_request.approver, @admin_user
    end

    test 'should trigger NotificationGeneratorJob after rejecting topic_request' do
      topic_request = create(:topic_request, organization_id: @org.id, label: "test")
      api_v2_sign_in @admin_user
      TestAfterCommit.with_commits(true) do
        NotificationGeneratorJob.expects(:perform_later).once
        put :reject, id: topic_request.id, format: :json
      end  
    end

    test 'api should not reject topic request with given id if user is non-admin' do
      topic_request = create(:topic_request, organization_id: @org.id, label: "test")
      api_v2_sign_in @user
      put :reject, id: topic_request.id, format: :json
      assert_response :unauthorized
      topic_request.reload
      assert_equal topic_request.state , "pending"
    end

    test 'api should publish topic request with given id if user is admin and also add topic detail in user learning_topics' do
      topic_request = create(:topic_request, organization_id: @org.id, requestor_id: @user.id, label: "test",state: "approved")
      api_v2_sign_in @admin_user
      topic_params = {
        "topic_id"      => "34cd4585-208f-49e6-98be-56a9af367f95",
        "topic_name"    => "edcast.art_and_design.3d_and_animation.3d_animation",
        "topic_label"   => "3D Animation",
        "domain_id"     => "bd5f4691-e121-4782-b29a-b38b97f67f89",
        "domain_name"   => "edcast.art_and_design",
        "domain_label"  => "Art and Design"
      }
      put :publish, id: topic_request.id, taxonomy_topic: topic_params, format: :json
      assert_response :ok
      topic_request.reload
      assert topic_request.published?
      assert @user.profile.learning_topics.include?(topic_params)
    end

    test 'api should throw error if required params are missing' do
      topic_request = create(:topic_request, organization_id: @org.id, requestor_id: @user.id, label: "test",state: "approved")
      api_v2_sign_in @admin_user
      topic_params = {
        "topic_id"      => "34cd4585-208f-49e6-98be-56a9af367f95",
        "topic_name"    => "edcast.art_and_design.3d_and_animation.3d_animation",
        "domain_id"     => "bd5f4691-e121-4782-b29a-b38b97f67f89",
        "domain_name"   => "edcast.art_and_design",
        "domain_label"  => "Art and Design"
      }
      put :publish, id: topic_request.id, taxonomy_topic: topic_params, format: :json
      assert_response :unprocessable_entity
      resp = get_json_response(response)
      assert_match /Missing required topic_id, topic_name, topic_label or domain_id param/, get_json_response(response)['message']
    end
    
    test 'should trigger NotificationGeneratorJob when topic_request is published' do
      topic_request = create(:topic_request, organization_id: @org.id, requestor_id: @user.id, label: "test",state: "approved")
      api_v2_sign_in @admin_user
      topic_params = {
        "topic_id"      => "34cd4585-208f-49e6-98be-56a9af367f95",
        "topic_name"    => "edcast.art_and_design.3d_and_animation.3d_animation",
        "topic_label"   => "3D Animation",
        "domain_id"     => "bd5f4691-e121-4782-b29a-b38b97f67f89",
        "domain_name"   => "edcast.art_and_design",
        "domain_label"  => "Art and Design"
      }
      TestAfterCommit.with_commits(true) do
        NotificationGeneratorJob.expects(:perform_later).once
        put :publish, id: topic_request.id, taxonomy_topic: topic_params, format: :json
      end  
    end

    test 'api should approve topic request for rejected topic request too' do
      topic_request = create(:topic_request, organization_id: @org.id, label: "test", state: "rejected")
      api_v2_sign_in @admin_user
      put :approve, id: topic_request.id, format: :json
      assert_response :ok
      topic_request.reload
      assert topic_request.approved?
      assert_equal topic_request.approver, @admin_user
    end

    test 'api should add publisher while publishing the topic' do
      topic_request = create(:topic_request, organization_id: @org.id, requestor_id: @user.id, label: "test",state: "approved")
      api_v2_sign_in @admin_user
      topic_params = {
        "topic_id"      => "34cd4585-208f-49e6-98be-56a9af367f95",
        "topic_name"    => "edcast.art_and_design.3d_and_animation.3d_animation",
        "topic_label"   => "3D Animation",
        "domain_id"     => "bd5f4691-e121-4782-b29a-b38b97f67f89",
        "domain_name"   => "edcast.art_and_design",
        "domain_label"  => "Art and Design"
      }
      put :publish, id: topic_request.id, taxonomy_topic: topic_params, format: :json
      assert_response :ok
      topic_request.reload
      assert topic_request.published?
      assert_equal topic_request.publisher, @admin_user
    end
  end
    
  class TopicRequestIndexTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user2 = create(:user, organization: @org)
      @user3 = create(:user, organization: @org)
      @admin = create(:user, organization: @org, organization_role: "admin")
    end

    test 'api should  return list of topic request based on state' do 
      pending_topics = create_list(:topic_request, 5, organization: @org, requestor: @user2, state: "pending")
      approved_topics = create_list(:topic_request, 2, organization: @org, requestor: @user2, state: "approved")
      api_v2_sign_in @admin

      get :index, limit: 3, state: ["pending"], format: :json
      resp = get_json_response(response)

      assert_response :ok
      assert_equal resp['topic_requests'][0]['state'], "pending"
      assert_equal resp['total'], 5
    end

    test 'api should throw error if state is missing in params' do 
      pending_topics = create_list(:topic_request, 2, organization: @org, requestor: @user2, state: "pending")
      approved_topics = create_list(:topic_request, 2, organization: @org, requestor: @user2, state: "approved")
      api_v2_sign_in @admin

      get :index, limit: 3, format: :json
      assert_response :bad_request
    end

    test 'api should  return list of topic request based on state and requestor' do 
      pending_topics = create_list(:topic_request, 5, organization: @org, requestor: @user3, state: "pending")
      approved_topics = create_list(:topic_request, 2, organization: @org, requestor: @user2, state: "approved")
      api_v2_sign_in @admin

      get :index, state: ["pending"], requestor_id: @user3.id, format: :json
      resp = get_json_response(response)

      assert_response :ok
      assert_equal resp['total'], 5
    end

    test 'api should support sort by updated at desc' do
      topic_requests = (0..3).to_a.reverse.map do |i|
        create(:topic_request,  organization: @org, requestor: @user2, state: "pending", updated_at: i.hours.ago)
      end
      api_v2_sign_in @user2

      get :index, state: ["pending"], sort: "updated_at", limit: 3, offset: 0, format: :json
      assert_response :ok
      resp = get_json_response response
      assert_equal topic_requests.reverse.first(3).map(&:id), resp['topic_requests'].map{|c| c['id'].to_i}
    end

    test 'api should support sort by updated at asc' do
      topic_requests = (0..3).to_a.reverse.map do |i|
        create(:topic_request,  organization: @org, requestor: @user2, state: "pending", updated_at: i.hours.ago)
      end
      api_v2_sign_in @user2

      get :index, state: ["pending"], sort: "updated_at", order: 'asc', offset: 0, format: :json
      assert_response :ok
      resp = get_json_response response
      assert_equal topic_requests.map(&:id), resp['topic_requests'].map{|c| c['id'].to_i}
    end

    test 'api should exact matching label' do
      topic_requests = (0..3).to_a.reverse.map do |i|
        create(:topic_request,  organization: @org, label: "request#{i}", requestor: @user2, state: "pending", updated_at: i.hours.ago)
      end
      api_v2_sign_in @user2

      get :index, state: ["pending"], q: "request2", order: 'asc', offset: 0, format: :json
      assert_response :ok
      resp = get_json_response response
      assert_equal 'request2', resp['topic_requests'][0]['label']
    end

  end

  class TopicRequestBatchCreateTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      @admin_user = create(:user, organization_role: 'admin', organization: @org)
    end

    test ':api should should throw errors if topics params is missing' do
      api_v2_sign_in @user
      post :batch_create, format: :json
      resp = get_json_response(response)
      assert_response :unprocessable_entity
      assert_equal resp['message'], 'Invalid data type, topics should be array'
    end

    test ':api should should throw errors if wrong params in sent' do
      api_v2_sign_in @user
      post :batch_create, topics: 'one', format: :json
      resp = get_json_response(response)
      assert_response :unprocessable_entity
      assert_equal resp['message'], 'Invalid data type, topics should be array'
    end

    test ':api should create array of topic_request ' do
      api_v2_sign_in @user
      assert_difference -> {TopicRequest.count}, 3 do
        post :batch_create, topics: ['one', 'two', 'three'], format: :json
      end
      assert_response :ok
    end

    test ':api should not create content with the same topic within one organization' do
      create(:topic_request, label: 'one', organization: @org, requestor: @user)
      create(:topic_request, label: 'two', organization: @org, requestor: @user)
      api_v2_sign_in @user
      assert_difference -> {TopicRequest.count} do
        post :batch_create, topics: ['one', 'two', 'three'], format: :json
      end
      resp = get_json_response(response)
      assert_response :multi_status
    end
  end

  test 'api aggregate data should return summary count of all states of topic requests' do
    org = create(:organization)
    user = create(:user, organization: org)
    admin = create(:user, organization: org, organization_role: "admin")
    pending_topics = create_list(:topic_request, 5, organization: org, requestor: user, state: "pending")
    approved_topics = create_list(:topic_request, 6, organization: org, requestor: user, state: "approved")
    rejected_topics = create_list(:topic_request, 7, organization: org, requestor: user, state: "rejected")
    published_topics = create_list(:topic_request, 8, organization: org, requestor: user, state: "published")
    api_v2_sign_in admin

    get :aggregate_data, format: :json
    resp = get_json_response(response)

    assert_response :ok
    assert_equal 5, resp["pending"]
    assert_equal 6, resp["approved"]
    assert_equal 7, resp["rejected"]
    assert_equal 8, resp["published"]
  end
end

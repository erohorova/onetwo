require 'test_helper'

class Api::V2::MlpControllerTest < ActionController::TestCase

  setup do
    @user = create(:user)
    api_v2_sign_in(@user)
  end

  test ':api should return latest assignments for user' do
    pathway_card = create(:card, organization_id: @user.organization_id,
      author_id: nil, card_type: 'pack', card_subtype: 'simple', message: 'this is [facebook] (https://facebook.com)')
    pathway_assignment = create(
      :assignment, assignable: pathway_card, assignee: @user,
      state: 'completed', completed_at: 2.days.ago
    )
    # in point when assignment is completed UserContentCompletion is should exists
    create(
      :user_content_completion, :completed,
      completable: pathway_card, user: @user
    )

    get :latest_assignments, format: :json
    assert_response :ok

    resp = get_json_response(response)
    item = resp['items'][0]
    completed_at = pathway_assignment.completed_at.strftime('%FT%T.000Z')

    assert_equal 1, resp['items'].count
    assert_equal pathway_assignment.id, item['id']
    assert_equal completed_at, item['completed_at']
    assert_equal 100, item['progress']
    assert_equal 'this is [facebook] (https://facebook.com)', item['message']

    assert_not_nil item['assign_url']
  end

  test 'should allow certain card types assignments' do
    course_card = create(:card, organization_id: @user.organization_id,
      author_id: nil, card_type: 'course', card_subtype: 'link')
    course_assignment = create(
      :assignment, assignable: course_card, assignee: @user,
      state: 'completed', completed_at: 2.days.ago
    )

    get :latest_assignments, card_types: ['pack'], format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal 0, resp['items'].count

    get :latest_assignments, card_types: ['course'], format: :json
    assert_response :ok
    resp = get_json_response(response)
    item = resp['items'][0]

    assert_equal 1, resp['items'].count
    assert_equal course_assignment.id, item['id']
  end

  test 'should return default latest assignments if limit passed is blank' do
    pathway_card = create(:card, organization_id: @user.organization_id,
      author_id: nil, card_type: 'pack', card_subtype: 'simple')
    pathway_assignment = create(
      :assignment, assignable: pathway_card, assignee: @user,
      state: 'completed', completed_at: 2.days.ago
    )
    # in point when assignment is completed UserContentCompletion is should exists
    create(
      :user_content_completion, :completed,
      completable: pathway_card, user: @user
    )

    get :latest_assignments, limit: '', format: :json
    assert_response :ok

    resp = get_json_response(response)
    item = resp['items'][0]
    completed_at = pathway_assignment.completed_at.strftime('%FT%T.000Z')

    assert_equal 1, resp['items'].count
    assert_equal pathway_assignment.id, item['id']
    assert_equal completed_at, item['completed_at']
    assert_equal 100, item['progress']
    assert_not_nil item['assign_url']
  end
end


require 'test_helper'

class Api::V2::UsersControllerTest < ActionController::TestCase

  # language
  test 'should update users language' do
    org1 = create(:organization)
    user1 = create(:user, organization: org1)
    api_v2_sign_in user1

    put :update, id: user1.id, user: { profile_attributes: { language: 'ru' } }, format: :json
    resp = get_json_response(response)
    assert_equal 'ru', resp['profile']['language']
  end

  test 'should update users profile with attributes' do
    org1 = create(:organization)
    user1 = create(:user, organization: org1)
    api_v2_sign_in user1

    dashboard_info = [{'name': 'Learning Hours',
                        'visible': 'true'
                        },
                        {'name': 'Peer Learning',
                        'visible': 'true'
                        }]

    put :update, id: user1.id, user: { profile_attributes: { dashboard_info: dashboard_info, job_role: "QA", company: "Nasscom" } }, format: :json
    resp = get_json_response(response)
    assert_response :ok
    user_profile = UserProfile.where(user_id: user1.id).first
    assert_equal user_profile.dashboard_info, resp['dashboard_info']
    assert_equal user_profile.job_role, resp["profile"]["job_role"]
    assert_equal user_profile.company, resp["profile"]["company"]
  end

  class ContentSharedWithUserTest < ActionController::TestCase
    setup do
      @org = create :organization
      @author = create :user, organization: @org
      @current_user = create :user, organization: @org
      @cards = create_list(:card, 4, author_id: @author.id, card_type: 'media')
      @cards = create_list(:card, 4, author_id: @author.id, card_type: 'media')
      @team = create :team, organization: @org, name: "Ruby Development"
      @team_user = create :teams_user , user: @current_user ,team: @team
    end

    test "should return cards shared with group and user when shared_type param is not present" do
      create :teams_card, card: @cards[0], team: @team, type: 'SharedCard', user: @author
      create :teams_card, card: @cards[1], team: @team, type: 'SharedCard', user: @author
      create(:card_user_share, user: @current_user, card: @cards[1])
      create(:card_user_share, user: @current_user, card: @cards[2])
      create(:card_user_share, user: @current_user, card: @cards[3])
      api_v2_sign_in @current_user
      get :shared_cards, format: :json
      resp = get_json_response(response)
      assert_response :ok
      assert_equal 4, resp["cards"].length
      assert_same_elements @cards.map {|c| c['id'].to_s}, resp['cards'].map {|c| c['id']}
    end

    test "should return cards shared with group when sort default is created_at of teams_card and order default is DESC" do
      teams_cards = []
      (0..1).each do |i|
      teams_cards << create(:teams_card, card: @cards[i], team: @team, type: 'SharedCard', user: @author,created_at: Time.now.utc - i.hours)
      end
      create(:card_user_share, user: @current_user, card: @cards[2])
      api_v2_sign_in @current_user
      get :shared_cards, shared_type: "team_share", format: :json
      resp = get_json_response(response)
      assert_response :ok
      assert_equal 2, resp["cards"].length
      assert_equal [@cards[0].id.to_s,@cards[1].id.to_s], resp['cards'].map {|c| c['id']}
    end

    test "should return cards shared with group when order is ASC and sort default is created_at of teams_card" do
      teams_cards = []
      (0..1).each do |i|
      teams_cards << create(:teams_card, card: @cards[i], team: @team, type: 'SharedCard', user: @author,created_at: Time.now.utc - i.hours)
      end
      create(:card_user_share, user: @current_user, card: @cards[2])
      api_v2_sign_in @current_user
      get :shared_cards, shared_type: "team_share", order: "ASC", format: :json
      resp = get_json_response(response)
      assert_response :ok
      assert_equal 2, resp["cards"].length
      assert_equal [@cards[1].id.to_s,@cards[0].id.to_s], resp['cards'].map {|c| c['id']}
    end

    test "should return cards shared with user when order default is DESC and sort default is created_at of card_user_share" do
      card_user_share = []
      (0..1).each do |i|
        card_user_share << create(:card_user_share, user: @current_user, card: @cards[i],                        created_at: Time.now.utc - i.hours)
      end
      create :teams_card, card: @cards[2], team: @team, type: 'SharedCard', user: @author
      api_v2_sign_in @current_user
      get :shared_cards, shared_type: "user_share", format: :json
      resp = get_json_response(response)
      assert_response :ok
      assert_equal 2, resp["cards"].length
      assert_equal [@cards[0].id.to_s,@cards[1].id.to_s], resp['cards'].map {|c| c['id']}
    end

    test "should return cards shared with user when order is ASC and sort default is created_at of card_user_share" do
      card_user_share = []
      (0..1).each do |i|
        card_user_share << create(:card_user_share, user: @current_user, card: @cards[i],                        created_at: Time.now.utc - i.hours)
      end
      create :teams_card, card: @cards[2], team: @team, type: 'SharedCard', user: @author
      api_v2_sign_in @current_user
      get :shared_cards, shared_type: "user_share", order: "ASC", format: :json
      resp = get_json_response(response)
      assert_response :ok
      assert_equal 2, resp["cards"].length
      assert_equal [@cards[1].id.to_s,@cards[0].id.to_s], resp['cards'].map {|c| c['id']}
    end

    test "should return unprocessable entity after passing invalid required params" do
      create :teams_card, card: @cards[0], team: @team, type: 'SharedCard', user: @author
      create :teams_card, card: @cards[1], team: @team, type: 'SharedCard', user: @author
      api_v2_sign_in @current_user
      get :shared_cards, shared_type: "share_with_me", format: :json
      assert_response :unprocessable_entity
    end
  end

  test 'POST start_data_export' do
    org = create :organization
    user = create :user, organization: org
    assert user.data_export_status.nil?
    api_v2_sign_in user
    Analytics::UserDataExportJob.expects(:perform_later).with(user: user)
    post :start_data_export, format: :json
    assert user.reload.data_export_status == "started"
    assert_response :no_content
  end

  test 'GET data_export when data export is still in progress' do
    org = create :organization
    user = create :user, organization: org, data_export_status: "started"
    api_v2_sign_in user
    get :data_export, format: :json
    expected = {
      "data_export_status" => "started",
      "data_export_url" => nil,
      "last_data_export_time" => nil
    }
    assert_equal expected, JSON.parse(response.body)
    assert_equal 200, response.status
  end

  test 'GET data_export when data export is completed' do
    url = "http://foo.com"
    org = create :organization
    @datetime = DateTime.now
    user = create :user, {
      organization: org,
      data_export_status: "done",
      last_data_export_time: @datetime
    }
    api_v2_sign_in user
    s3_key = Analytics::UserDataExport.build_s3_zipfile_key(user)
    Analytics::UserDataExport.expects(:generate_link_for).with(s3_key).returns url
    get :data_export, format: :json
    expected = {
      "data_export_status" => "done",
      "data_export_url" => url,
      "last_data_export_time" => @datetime.to_i,
    }
    assert_equal expected, JSON.parse(response.body)
    assert_equal 200, response.status
  end

  # learning_topics and expertise
  test 'user should be made to follow associated channel as per the topics' do
    org1 = create(:organization)
    create(:config, name: 'enable_channel_autofollow_based_on_topic', value: true, configable: org1)
    topics = [Taxonomies.domain_topics[0], Taxonomies.domain_topics[1]]
    TestAfterCommit.with_commits do
      user1 = create(:user, organization: org1)
      channel = create(:channel, organization: org1, topics: topics)

      assert_equal 0, channel.followers.count

      # will enqueue cards of a channel that the user follows based on the association of learning and expertise topics
      Channel.any_instance.expects(:after_followed).once
      api_v2_sign_in user1
      put :update, id: user1.id, user: { profile_attributes: { expert_topics: [], learning_topics: [topics[0]] } }, format: :json

      resp = get_json_response(response)
      assert_equal Taxonomies.domain_topics[0]['topic_name'], resp['profile']['learning_topics'][0]['topic_name']
      assert_equal 1, channel.followers.count
      assert_equal user1.id, channel.followers.first.id
    end
  end

  test '#update allows empty learning and expert topics' do
    org = create(:organization)
    TestAfterCommit.with_commits do
      user = create(:user, organization: org)

      api_v2_sign_in user
      put :update, id: user.id, user: { profile_attributes: { expert_topics: [], learning_topics: [] } }, format: :json

      assert_empty user.reload.expert_topics
      assert_empty user.reload.learning_topics
    end
  end

  test 'should not update users learning_topics with invalid level' do
    org1 = create(:organization)
    user1 = create(:user, organization: org1)
    topic = Taxonomies.domain_topics[0]
    topic[:level] = '6'
    api_v2_sign_in user1

    put :update, id: user1.id, user: { profile_attributes: { learning_topics: [topic] } }, format: :json
    resp = get_json_response(response)
    assert_response :unprocessable_entity
    assert_equal 'Invalid value for learning topic level', resp['message']
  end

  # following
  test ':api show list of users that user is following' do
    me = create(:user)
    api_v2_sign_in(me)

    following = create_list(:user, 5, organization: me.organization)
    following.each{|u| Follow.create(followable_id: u.id, followable_type: 'User', user_id: me.id)}

    get :following, format: :json
    resp = get_json_response(response)

    assert_equal 5, resp['users'].count
    assert_same_ids following.map(&:id), resp['users'].map {|u| u['id']}
    assert_equal 5, resp['total']
  end

  test 'should validate reset password token true' do
    org = create(:organization)
    user = create(:user, organization: org)
    token = user.send :send_reset_password_instructions
    user.reload

    get :validate_password_reset_token, token: token

    assert_response :ok
  end

  test 'should not validate reset password token' do
    org = create(:organization)
    user = create(:user, organization: org)
    token = 'abc'

    get :validate_password_reset_token, token: token

    assert_response :unprocessable_entity
  end

  test ':api show list of users that user is following #search' do
    me = create(:user)
    org = me.organization
    api_v2_sign_in(me)

    following = create_list(:user, 2, organization: org)
    following << create(:user, handle: '@john123', organization: org)
    following.each{|u| Follow.create(followable_id: u.id, followable_type: 'User', user_id: me.id)}

    get :following, q: 'john', format: :json
    resp = get_json_response(response)

    assert_equal 1, resp['users'].count
    assert_same_ids following.last(1).map(&:id), resp['users'].map {|u| u['id']}
    assert_equal 1, resp['total']
  end

  test 'show list of users that user is following #search by full name' do
    org = create(:organization)
    me = create(:user, organization: org)
    api_v2_sign_in(me)

    first_name = Faker::Name.name
    following = create_list(:user, 3, first_name: first_name, organization: org)

    following.each{|u| Follow.create(followable_id: u.id, followable_type: 'User', user_id: me.id)}

    get :following, q: "#{first_name} #{following.first[:last_name]}", format: :json
    resp = get_json_response(response)

    assert_equal 1, resp['users'].count
    assert_equal 1, resp['total']
  end

  test 'show list of users that user is following #search by email' do
    org = create(:organization)
    create_org_master_roles org

    admin = create(:user, organization: org)
    admin.add_role(Role::TYPE_ADMIN)
    api_v2_sign_in(admin)

    following = create_list(:user, 5, organization: org)

    following.each{|u| Follow.create(followable_id: u.id, followable_type: 'User', user_id: admin.id)}

    get :following, q: "#{following.first[:email]}", format: :json
    resp = get_json_response(response)

    assert_equal 1, resp['users'].count
    assert_equal 1, resp['total']
  end

  test 'show list of unique users that a user is following' do
    org = create(:organization)
    create_org_master_roles org

    admin = create(:user, organization: org)
    admin.add_role(Role::TYPE_ADMIN)
    api_v2_sign_in(admin)

    following = create(:user, organization: org)

    2.times { Follow.create(followable_id: following.id, followable_type: 'User', user_id: admin.id) }

    get :following, format: :json

    resp = get_json_response(response)

    assert_same_ids [following.id], resp['users'].collect { |u| u['id'] }
    assert_equal 1, resp['total']
  end

  test ':api show list of users that user is following #limit' do
    me = create(:user)
    org = me.organization
    api_v2_sign_in(me)

    following = create_list(:user, 6, organization: org)
    following.each{|u| Follow.create(followable_id: u.id, followable_type: 'User', user_id: me.id)}

    get :following, limit: 5, format: :json
    resp = get_json_response(response)

    assert_equal 5, resp['users'].count
    assert_same_ids following.last(5).map(&:id), resp['users'].map {|u| u['id']}
    assert_equal 6, resp['total']
  end

  test ':should trigger update_role callback after new user creation' do
    org1 = create(:organization)
    User.any_instance.expects(:update_role).once
    user = create(:user, organization: org1, email: "okta@edcast.com", first_name: "Okta", last_name: "User", password: User.random_password)
  end

  test ':should set default role as a member for new user' do
    User.any_instance.unstub :update_role
    org = create(:organization)
    Role.create_master_roles(org)
    role_member = org.roles.find_by(default_name: 'member')
    user = create(:user, organization: org, email: "abc@edcast.com", first_name: "Okta", last_name: "User", password: User.random_password)
    user.send(:update_role)

    user = org.users.find_by(email: "abc@edcast.com")
    assert_equal 'member', user.organization_role
    assert_includes role_member.user_ids, user.id
  end

  test ':should set default role as a member for new user if Role name & default_nmae is different ' do
    User.any_instance.unstub :update_role
    org = create(:organization)
    Role.create_master_roles(org)
    role_member = org.roles.find_by(default_name: 'member')
    role_member.update(name: "learner")
    user = create(:user, organization: org, email: "abc@edcast.com", first_name: "abc", last_name: "User", password: User.random_password)
    user.send(:update_role)

    user = org.users.find_by(email: "abc@edcast.com")
    assert_equal 'member', user.organization_role
    assert_equal 'member', role_member.default_name
    assert_equal 1, user.roles.count
    assert_includes role_member.user_ids, user.id
  end

  # followers
  test ':api show list of users that are following a user' do
    me = create(:user)
    org = me.organization
    api_v2_sign_in(me)

    followers = create_list(:user, 5, organization: org)
    followers.each{|u| Follow.create(followable_id: me.id, followable_type: 'User', user_id: u.id)}

    # Follow one of the followers
    following = followers.first
    Follow.create(followable: following, user_id: me.id)

    get :followers, format: :json
    resp = get_json_response(response)

    assert_equal 5, resp['users'].count
    assert_same_ids followers.map(&:id), resp['users'].map {|u| u['id']}
    assert_equal Follow.where(followable: me).order(created_at: :desc).pluck(:user_id), resp['users'].map { |u| u['id'] }
    assert_equal 5, resp['total']

    me_following_other_users = resp['users'].select {|user| user['id'] == following.id}
    assert_equal 1, me_following_other_users.count

    assert me_following_other_users[0]['is_following']
  end

  test ':api show list of users that are following a user #search' do
    me = create(:user)
    org = me.organization
    api_v2_sign_in(me)

    followers = create_list(:user, 1, organization: org)
    followers << create(:user, handle: '@john123', organization: org)
    followers.each{|u| Follow.create(followable_id: me.id, followable_type: 'User', user_id: u.id)}

    get :followers, q: 'john', format: :json
    resp = get_json_response(response)

    assert_equal 1, resp['users'].count
    assert_same_ids followers.last(1).map(&:id), resp['users'].map {|u| u['id']}
    assert_equal 1, resp['total']
  end

  test 'show list of users that are following a user #search by full name' do
    org = create(:organization)
    me = create(:user, organization: org)
    api_v2_sign_in(me)

    first_name = Faker::Name.name
    followers = create_list(:user, 2, first_name: first_name, organization: org)
    followers.each{|u| Follow.create(followable_id: me.id, followable_type: 'User', user_id: u.id)}

    get :followers, q: "#{first_name} #{followers.first[:last_name]}", format: :json
    resp = get_json_response(response)

    assert_equal 1, resp['users'].count
    assert_equal 1, resp['total']
  end

  test 'show list of users that are following a user #search by email' do
    org = create(:organization)
    admin = create(:user, organization: org)
    create_org_master_roles org

    admin.add_role(Role::TYPE_ADMIN)
    api_v2_sign_in admin

    followers = create_list(:user, 2, organization: org)
    followers.each{|u| Follow.create(followable_id: admin.id, followable_type: 'User', user_id: u.id)}

    get :followers, q: "#{followers.first[:email]}", format: :json
    resp = get_json_response(response)

    assert_equal 1, resp['users'].count
    assert_equal 1, resp['total']
  end

  test ':api show list of users that are following a user #limit' do
    me = create(:user)
    org = me.organization

    api_v2_sign_in(me)

    followers = create_list(:user, 6, organization: org)
    followers.each{|u| Follow.create(followable_id: me.id, followable_type: 'User', user_id: u.id)}

    get :followers, limit: 5, format: :json
    resp = get_json_response(response)

    assert_equal 5, resp['users'].count
    assert_same_ids followers.last(5).map(&:id), resp['users'].map {|u| u['id']}
    assert_equal 6, resp['total']
  end

  test ':api unlock a user' do
    org = create(:organization)
    create_org_master_roles org

    admin, member = create_list(:user, 2, organization: org)
    admin.update organization_role: 'admin'
    admin.add_role(Role::TYPE_ADMIN)
    member.lock_account!

    set_host org

    # Make sure admin
    api_v2_sign_in member
    post :unlock, id: member.id, format: :json
    assert_response :unauthorized

    api_v2_sign_in admin
    post :unlock, id: member.id, format: :json
    assert_response :ok

    refute member.reload.account_locked?
  end

  test ':api should return correct number of team users count' do
    org   = create :organization, name: 'some_org'

    team = create(:team, organization: org)

    admin = create :user, organization: org, is_suspended: false, is_edcast_admin: 1
    user0 = create :user, organization: org, is_suspended: false
    user1 = create :user, organization: org, is_suspended: false
    user2 = create :user, organization: org, is_suspended: true, status: 'suspended'
    user3 = create :user, organization: org, is_suspended: false, is_edcast_admin: 1, email: nil

    team.add_admin admin
    team.add_member user0
    team.add_member user1
    team.add_member user2
    team.add_admin user3

    api_v2_sign_in admin
    get :index, team_ids: [team.id], format: :json

    resp = get_json_response(response)
    response_ids = resp['users'].map{|u| u['id'] }

    assert_response :ok
    assert_same_elements [ admin.id, user0.id, user1.id], response_ids
  end

  test ':api should return current_sign_in_at of users if current user is admin' do
    org   = create :organization, name: 'some_org'

    admin = create :user, organization: org, organization_role: 'admin'
    user = create :user, organization: org, current_sign_in_at: Time.now

    admin.add_role(Role::TYPE_ADMIN)

    api_v2_sign_in admin
    get :index, fields: 'id,full_name', format: :json

    resp = get_json_response(response)
    current_sign_in_at = resp['users'].map{|u| u['current_sign_in_at']&.to_time&.to_s }

    assert_response :ok
    assert_same_elements [nil, user.current_sign_in_at.to_time.to_s], current_sign_in_at
  end

  test ':api should change user status to active' do
    org = create(:organization)
    create_org_master_roles(org)
    admin, member = create_list(:user, 2, organization: org)
    admin.add_role(Role::TYPE_ADMIN)
    member.suspend!

    set_host org

    api_v2_sign_in admin
    User.any_instance.expects(:reindex_async).returns(nil).once
    post :user_status, id: member.id, status: 'active', format: :json
    assert_response :ok

    member.reload
    assert !member.is_suspended?
    assert_equal 'active', member.status
  end

  test ':api should change user status to inactive' do
    org = create(:organization)
    create_org_master_roles(org)
    admin, member = create_list(:user, 2, organization: org)
    admin.add_role(Role::TYPE_ADMIN)
    member.suspend!

    set_host org

    api_v2_sign_in admin
    User.any_instance.expects(:reindex_async).returns(nil).once
    post :user_status, id: member.id, status: 'inactive', format: :json
    assert_response :ok

    member.reload
    assert !member.is_suspended?
    assert_equal 'inactive', member.status
    assert member.is_active
    refute member.is_suspended
  end

  test ':api should should return error message if tried to change status for not permitted' do
    # permitted statuses are suspended, active, inactive
    org = create(:organization)
    create_org_master_roles(org)
    admin, member = create_list(:user, 2, organization: org)
    admin.add_role(Role::TYPE_ADMIN)
    member.suspend!

    api_v2_sign_in admin
    post :user_status, id: member.id, status: 'great_test_status', format: :json
    assert_response :unprocessable_entity
  end

  # suggested users
  test ':api lists SMEs + Influences that are not followed by current_user' do
    Organization.any_instance.unstub(:create_default_roles)
    org = create :organization
    org.send :create_default_roles

    user = create :user, organization: org
    sme = org.sme_role
    influencer = create :role, organization: org, name: 'influencer'

    member1 = create :user, organization: org, is_complete: true
    member2 = create :user, organization: org, is_complete: true
    member3 = create :user, organization: org, is_complete: false
    edcast_admin = create :user, organization: org, is_edcast_admin: true
    suspended_user = create :user, organization: org, is_suspended: true
    no_handle_user = create :user, organization: org
    sme1 = create :user, organization: org
    sme2 = create :user, organization: org
    influencer1 = create :user, organization: org
    influencer2 = create :user, organization: org

    no_handle_user.update_column(:handle, nil)
    create :follow, user: user, followable: member1
    create :follow, user: user, followable: sme1
    create :follow, user: user, followable: influencer1

    sme.add_user(sme1)
    sme.add_user(sme2)
    influencer.add_user(influencer1)
    influencer.add_user(influencer2)

    set_host org
    api_v2_sign_in user
    get :suggested, format: :json

    resp = get_json_response(response)
    assert_same_elements [sme2.id, influencer2.id], resp['users'].map{ |u| u['id']}
  end

  test ':api suggested users pagination' do
    Organization.any_instance.unstub(:create_default_roles)

    org1, org2 = create_list(:organization, 2)
    org1.send :create_default_roles
    org2.send :create_default_roles

    sme1 = org1.sme_role
    influencer1 = create :role, organization: org1, name: 'influencer'
    sme2 = org2.sme_role
    influencer2 = create :role, organization: org2, name: 'influencer'

    org1_infs = create_list(:user, 4, organization: org1)
    org2_infs = create_list(:user, 2, organization: org2)

    sme1.add_user(org1_infs[0])
    influencer1.add_user(org1_infs[1])
    sme1.add_user(org1_infs[2])
    influencer1.add_user(org1_infs[3])

    sme2.add_user(org2_infs[0])
    influencer2.add_user(org2_infs[1])

    user = create(:user, organization: org1)

    set_host org1
    api_v2_sign_in user
    get :suggested, limit: 3, offset: 0, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_same_elements [org1_infs[0].id, org1_infs[1].id, org1_infs[2].id], resp['users'].map { |r| r['id'] }

    get :suggested, limit: 3, offset: 3, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_same_elements [org1_infs[3].id], resp['users'].map { |r| r['id'] }

    get :suggested, limit: 3, offset: 6, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_empty resp['users'].map { |r| r['id'] }
  end

  test "lists SMEs + Influences that are unfollowed by current user" do
    org = create :organization
    create_org_master_roles org

    user = create :user, organization: org
    sme = org.sme_role
    influencer = create :role, organization: org, name: 'influencer'

    member1 = create :user, organization: org, is_complete: true
    member2 = create :user, organization: org, is_complete: true
    member3 = create :user, organization: org, is_complete: false

    edcast_admin = create :user, organization: org, is_edcast_admin: true
    suspended_user = create :user, organization: org, is_suspended: true
    no_handle_user = create :user, organization: org

    sme1 = create :user, organization: org
    sme2 = create :user, organization: org

    influencer1 = create :user, organization: org
    influencer2 = create :user, organization: org

    no_handle_user.update_column(:handle, nil)

    create :follow, user: user, followable: member1
    create :follow, user: user, followable: sme1
    create :follow, user: user, followable: influencer1

    sme.add_user(sme1)
    sme.add_user(sme2)
    influencer.add_user(influencer1)
    influencer.add_user(influencer2)

    api_v2_sign_in user
    get :suggested, format: :json

    resp = get_json_response(response)
    assert_same_elements [sme2.id, influencer2.id], resp['users'].map{ |u| u['id']}

    # unfollow sme1
    user.follows.where(followable: sme1).first.destroy

    api_v2_sign_in user
    get :suggested, format: :json

    resp = get_json_response(response)
    assert_same_elements [sme1, sme2, influencer2].map(&:id), resp['users'].map{ |u| u['id']}
  end

  # integrations
  test ":api should returns all integrations along with user enabled or not" do
    user1 = create(:user)

    fields1 = [{name: "coursera_uid", label: "Coursera UID", placeholder: "Coursera public ID"}, {name: "coursera_username", label: "Coursera user name"}]
    fields2 = [{name: "udemy_id", label: "Udemy ID"}]
    integration1 = create(:integration, fields: fields1.to_json)
    integration2 = create(:integration, fields: fields2.to_json)
    ui1 = create :users_integration, user: user1, integration_id: integration1.id, field_values: {"coursera_uid" => "test id", "coursera_username" => "test_name"}.to_json
    api_v2_sign_in user1

    get :integrations, id: user1.id, format: :json

    assert_response :ok

    resp = get_json_response(response)

    resp_intg_ids = resp['integrations'].map{|a| a["id"]}
    assert_equal [integration1.id, integration2.id], resp_intg_ids
    assert resp['integrations'][0]["connected"]
    refute resp['integrations'][1]["connected"]

    #for coursera
    assert ["name", "label", "placeholder", "value"], resp['integrations'][0]["fields"][0].keys
    assert ["coursera_uid", "Coursera UID", "Coursera public ID", "test id"], resp['integrations'][0]["fields"][0].values

    assert ["name", "label", "value"], resp['integrations'][0]["fields"][1].keys
    assert ["coursera_username", "Coursera user name", "test_name"], resp['integrations'][0]["fields"][1].values

    #for UDEMy user is not connected
    assert ["name", "label"], resp['integrations'][1]["fields"][0].keys
    assert ["udemy_id", "Udemy ID"], resp['integrations'][1]["fields"][0].values
  end

  # courses
  test ":api get courses" do
    skip
    user1 = create(:user)
    courses = create_list(:users_external_course, 2, user: user1)
    user1.integrations << Integration.all
    post :courses, id: user1.id, format: :json
    assert_response :unauthorized

    api_v2_sign_in user1

    post :courses, id: user1.id, format: :json

    assert_response :ok

    resp = get_json_response response

    assert_equal 2, resp.count
    assert_equal ["external_courses", "internal_courses"], resp.keys
    assert_same_elements ["id", "name", "description", "image_url", "course_url", "status", "integration_name", "integration_logo_url", "raw_info", "due_date", "assigned_date"], resp["external_courses"].first.keys
    assert_equal 2, resp["external_courses"].length
    assert_equal 0, resp["internal_courses"].length

    ResourceGroup.any_instance.stubs(:savannah?).returns(true)
    group = create(:resource_group)
    group.members << user1

    post :courses, id: user1.id, fields: {external_uid: "11222"}, provider: "Skill share", format: :json

    assert_response :ok

    resp = get_json_response response

    assert_equal ["external_courses", "internal_courses"], resp.keys
    assert_same_elements ["id", "name", "description", "image_url", "course_url", "status", "integration_name", "integration_logo_url", "raw_info", "due_date", "assigned_date"], resp["external_courses"].first.keys
    assert_equal ["id", "name", "description", "image_url", "course_url", "status", "duration"], resp["internal_courses"].first.keys

    assert_equal 2, resp["external_courses"].length
    assert_equal 1, resp["internal_courses"].length
  end

  # update
  test ':api should update user information' do
    user1 = create(:user)
    api_v2_sign_in user1

    pic = Rails.root.join('test/fixtures/images/logo.png')

    put :update, id: user1.id, user: { first_name: 'john', last_name: 'bob', bio: 'some bio', coverimage: pic.open,
                                        avatar: pic.open, hide_from_leaderboard: true}, format: :json

    assert_response :ok

    resp = get_json_response(response)

    assert_equal 'john', resp['first_name']
    assert_equal 'bob', resp['last_name']
    assert_equal 'some bio', resp['bio']
    assert_equal true, resp['hide_from_leaderboard']
    assert_not_empty resp['coverimages']
    assert_not_empty resp['avatarimages']
  end

  test ':api should not update first_name and last_name for SSO user' do
    user1 = create(:user)
    provider = create :identity_provider, :saml, user: user1
    api_v2_sign_in user1

    first_name = Faker::Name.name
    last_name  = Faker::Name.name
    bio        = Faker::Lorem.sentence

    put :update, id: user1.id, user: {
      first_name: first_name,
      last_name: last_name,
      bio: bio
    }, format: :json

    assert_response :ok

    resp = get_json_response(response)
    assert_not_equal first_name, resp['first_name']
    assert_not_equal last_name, resp['last_name']
    assert_equal bio, resp['bio']
  end

  test ':api should allow update first_name if blank for SSO user' do
    sso_user = create(:user, first_name: nil)
    provider = create :identity_provider, :saml, user: sso_user
    api_v2_sign_in sso_user

    first_name = Faker::Name.name
    last_name  = Faker::Name.name
    bio        = Faker::Lorem.sentence

    put :update, id: sso_user.id, user: {
          first_name: first_name,
          last_name: last_name,
          bio: bio
        }, format: :json

    assert_response :ok

    resp = get_json_response(response)
    assert_not_empty resp['first_name']
    assert_equal first_name, resp['first_name']
    assert_not_equal last_name, resp['last_name']
    assert_equal bio, resp['bio']
  end

  test ':api should allow update first_name and last_name for any user for admin' do
    org = create(:organization)
    admin_user = create(:user, organization: org)
    admin_user.update organization_role: 'admin'
    admin_user.add_role(Role::TYPE_ADMIN)

    non_admin_user = create(:user, organization: org)
    provider = create :identity_provider, :saml, user: non_admin_user

    api_v2_sign_in admin_user

    first_name = Faker::Name.name
    last_name  = Faker::Name.name
    bio        = Faker::Lorem.sentence

    put :update, id: non_admin_user.id, user: {
          first_name: first_name,
          last_name: last_name,
          bio: bio
        }, format: :json

    assert_response :ok

    resp = get_json_response(response)
    assert_equal first_name, resp['first_name']
    assert_equal last_name, resp['last_name']
    assert_equal bio, resp['bio']
  end

  test ':api should update user default_team' do
    org   = create :organization
    user = create(:user, organization: org)
    api_v2_sign_in user

    team1, team2, team3 = create_list(:team, 3, organization: org)

    team1.add_member user
    team2.add_member user

    put :update, id: user.id, user: {default_team_id: team1.id}, format: :json

    assert_response :ok
    resp = get_json_response(response)
    assert_equal team1.id, resp['default_team_id']
  end

  test ':api should not update user default_team if user is not member of it' do
    org   = create :organization
    user = create(:user, organization: org)
    api_v2_sign_in user

    team1, team2, team3 = create_list(:team, 3, organization: org)

    team1.add_member user
    team2.add_member user

    put :update, id: user.id, user: {default_team_id: team3.id}, format: :json
    assert_response :unprocessable_entity
  end


  test "update notification preferences" do
    user = create(:user)
    user.pref_notification_follow = "false"
    user.pref_newsletter_opt_in = "true"
    user.pref_notification_weekly_activity = "true"

    pref_params = {pref_notification_follow: "true", pref_newsletter_opt_in: "false", pref_notification_weekly_activity: "false"}
    api_v2_sign_in user
    put :update, id: user.id, user: pref_params, format: :json

    assert_response :ok
    assert user.pref_notification_follow
    refute user.pref_newsletter_opt_in
    refute user.pref_notification_weekly_activity
  end

  test "update expert_topics and learning_topics" do
    User.any_instance.stubs(:reindex_async).returns(true)

    org = create(:organization)
    user1, user2, admin = create_list(:user, 3, organization: org)

    admin.update_columns(organization_role: "admin")
    api_v2_sign_in admin
    topics = Taxonomies.domain_topics
    put :update, id: user1.id, user: { profile_attributes: { expert_topics: topics, learning_topics: topics } }, format: :json

    profile = user1.reload.profile
    assert_equal profile.expert_topics, topics.collect { |d_t| d_t.stringify_keys! }
    assert_equal profile.learning_topics, topics.collect { |d_t| d_t.stringify_keys! }

    api_v2_sign_in user1
    put :update, id: user1.id, user: { profile_attributes: { expert_topics: topics } }, format: :json
    assert_response :ok

    api_v2_sign_in user2
    put :update, id: user1.id, user: { profile_attributes: { expert_topics: topics } }, format: :json
    assert_response 422
  end

  test "updating profile_attributes triggers MetricsRecorderJob" do
    sample_topic_1, sample_topic_2 = get_sample_topics.map &:stringify_keys
    stub_request(:post, "http://localhost:9200/_bulk")
    stub_time = Time.now
    org = create(:organization)
    user = create(:user, organization: org)
    user_profile = create :user_profile, user: user, expert_topics: [sample_topic_1]
    metadata = {
      user_id: user.id,
      platform: 'web',
      platform_version_number: nil,
      user_agent: 'Rails Testing'
    }
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    UserProfile.any_instance.stubs(:find_actor).returns user
    UserProfile.any_instance.unstub(:record_user_profile_update_event)
    api_v2_sign_in user
    Analytics::MetricsRecorderJob.unstub(:perform_later)
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      recorder: "Analytics::UserProfileRecorder",
      actor: Analytics::MetricsRecorder.user_attributes(user),
      org: Analytics::MetricsRecorder.org_attributes(user.organization),
      user_profile: Analytics::MetricsRecorder.user_profile_attributes(user_profile).merge(
        "updated_at" => stub_time.to_i
      ),
      changed_profile_keys: {
         "expert_topics" => [[sample_topic_1], [sample_topic_2]]
      },
      timestamp: stub_time.to_i,
      metadata: metadata
    )
    Timecop.freeze(stub_time) do
      put :update, id: user.id, user: {
          profile_attributes: {
              expert_topics: [sample_topic_2],
          }
      }, format: :json
    end
  end

  test ":api should update time zone of user" do
    User.any_instance.stubs(:reindex_async).returns(true)

    org = create(:organization)
    user = create(:user, organization: org)

    api_v2_sign_in user
    put :update, id: user.id, user: { profile_attributes: { time_zone: "Asia/Tokyo" } }, format: :json
    assert_response :ok
    assert_equal "Asia/Tokyo", user.profile.time_zone

    resp = get_json_response(response)
    assert_not_nil resp['profile']
    assert_equal 'Asia/Tokyo', resp['profile']['time_zone']
  end

  test ':api should update user password' do
    password = User.random_password
    user1 = create(:user, password: password)
    api_v2_sign_in user1
    random_password = User.random_password
    put :update_password, id: user1.id, user: {password: random_password, password_confirmation: random_password, current_password: password}, format: :json

    assert_response :ok
  end

  test ':api should update the plain password when config is enabled' do
    password = User.random_password
    Organization.any_instance.unstub :default_sso
    org = create(:organization)
    set_host org

    create :config, :boolean, name: 'encryption_payload', value: 'true', configable: org

    user = create(:user, password: password, organization: org)

    put :update_password, id: user.id, user: {password: user.password, password_confirmation: user.password, current_password: user.password}, format: :json

  end

  # index
  test ':api get org users' do
    org = create(:organization)
    org_users = create_list(:user, 20, organization: org, first_name: Faker::Name.name)
    org_viewer = org_users.last

    api_v2_sign_in org_viewer

    get :index, format: :json

    resp = get_json_response response

    assert_response :ok
    assert_equal 10, resp['users'].count
    assert_equal 10, (org_users.map(&:id) & resp['users'].map{|u| u['id']}).count
  end

  test ':api should not return duplicate users' do
    org = create(:organization)
    user1, user2, user3, user4, viewer = create_list(:user, 5, organization: org)
    team1, team2 = create_list(:team, 2, organization: org)

    [user1, user2].each do |user|
      team1.add_member user
      team2.add_member user
    end
    team1.add_member user3
    team2.add_member user4

    api_v2_sign_in viewer
    get :index, team_ids: [team1.id, team2.id], format: :json

    resp = get_json_response response
    assert_response :ok
    assert_equal 4, resp['users'].count
    assert_same_elements [user1.id, user2.id, user3.id, user4.id], resp['users'].map {|u| u['id']}

  end

  test ':api should return only onboarded users' do
    org = create(:organization)
    user1, user2, user3 = create_list(:user, 3, organization: org)
    user3.update(is_complete: false)

    api_v2_sign_in user1
    get :index, include_onboarded_only: true, format: :json

    resp = get_json_response response
    assert_response :ok
    assert_equal 2, resp['users'].count
    assert_same_elements [user1.id, user2.id], resp['users'].map {|u| u['id']}
  end

  test 'should return current_sign_in_at for admin user' do
    org = create(:organization)
    org_users = create_list(:user, 20, organization: org, first_name: Faker::Name.name)
    admin = create(:user, organization: org, organization_role: 'admin')

    api_v2_sign_in org_users.last

    get :index, format: :json

    resp = get_json_response response
    resp['users'].each do |user|
      refute user.has_key?("current_sign_in_at")
    end

    api_v2_sign_in admin

    get :index, format: :json
    resp = get_json_response response

    assert_response :ok
    resp['users'].each do |user|
      assert user.has_key?("current_sign_in_at")
    end
  end

  test 'should return payment amount for each user if show_payment_info is true and org subscription is enabled' do
    org = create(:organization)
    create(:organization_profile, organization: org, member_pay: true)
    org_user1 = create(:user, organization: org, first_name: Faker::Name.name)
    user_subscription1 = create(:user_subscription, user_id: org_user1.id, amount: 1000.00, currency: 'USD')
    org_user2 = create(:user, organization: org, first_name: Faker::Name.name)
    user_subscription2 = create(:user_subscription, user_id: org_user2.id, amount: 2000.00)
    user_subscriptions = [user_subscription1, user_subscription2]

    api_v2_sign_in org_user1

    get :index, show_payment_info: 'true', format: :json
    resp = get_json_response response

    assert_same_elements user_subscriptions.map{|u| u.amount.to_f}, resp['users'].map {|u| u['payment_amount'].to_f}
  end

  test 'should not return payment amount for each user if org subscription is not enabled' do
    org = create(:organization)
    org_user1 = create(:user, organization: org, first_name: Faker::Name.name)
    user_subscription1 = create(:user_subscription, user_id: org_user1.id, amount: 1000.00, currency: 'USD')
    org_user2 = create(:user, organization: org, first_name: Faker::Name.name)
    user_subscription2 = create(:user_subscription, user_id: org_user2.id, amount: 2000.00)
    user_subscriptions = [user_subscription1, user_subscription2]

    api_v2_sign_in org_user1

    get :index, show_payment_info: 'true', format: :json
    resp = get_json_response response

    assert_same_elements [nil, nil], resp['users'].map {|u| u['payment_amount']}
  end

  test 'should not return payment amount for each user if show_payment_info is not passed' do
    org = create(:organization)
    org_users = create_list(:user, 3, organization: org, first_name: Faker::Name.name)

    api_v2_sign_in org_users.first

    get :index, format: :json
    resp = get_json_response response

    resp['users'].each do |user|
      refute user.has_key?(:payment_amount)
    end
  end



  test ':api should return only users who have paid for org subscrition' do
    org = create(:organization)
    create(:organization_profile, organization: org, member_pay: true)
    org_users = create_list(:user, 5, organization: org, first_name: Faker::Name.name)
    paid_users = create_list(:user, 2, organization: org, first_name: Faker::Name.name)
    paid_users.map{|paid_user| create(:user_subscription, user_id: paid_user.id)}

    org_viewer = org_users.last

    api_v2_sign_in org_viewer

    get :paid_users, format: :json

    resp = get_json_response response
    assert_response :ok
    assert_equal 2, resp['users'].count
    assert_same_elements paid_users.map(&:id), resp['users'].map {|u| u['id']}
  end

  test ':api should return current_sign_in_at value for paid users' do
    org = create(:organization)
    create(:organization_profile, organization: org, member_pay: true)
    paid_user = create(:user, organization: org, first_name: Faker::Name.name, current_sign_in_at: Date.today)
    create(:user_subscription, user_id: paid_user.id)

    org_viewer = create(:user, organization: org)

    api_v2_sign_in org_viewer

    get :paid_users, format: :json
    resp = get_json_response response

    assert_equal Date.today, resp['users'][0]['current_sign_in_at'].to_date
  end

  test ':api should return users in sorted order' do
    org = create(:organization)
    create(:organization_profile, organization: org, member_pay: true)
    org_viewer = create(:user, organization: org)
    paid_user1 = create(:user, organization: org, first_name: Faker::Name.name, created_at: Date.today-1.day)
    paid_user2 = create(:user, organization: org, first_name: Faker::Name.name, created_at: Date.today)
    [paid_user1, paid_user2].map{|paid_user| create(:user_subscription, user_id: paid_user.id)}

    api_v2_sign_in org_viewer

    get :paid_users, sort: 'created_at', order: 'desc', format: :json

    resp = get_json_response response
    assert_equal paid_user2.id, resp['users'].first['id']
  end

  test ':api should return users based on limit and offset' do
    org = create(:organization)
    create(:organization_profile, organization: org, member_pay: true)
    org_users = create_list(:user, 3, organization: org, first_name: Faker::Name.name)
    paid_users = create_list(:user, 5, organization: org, first_name: Faker::Name.name)
    paid_users.map{|paid_user| create(:user_subscription, user_id: paid_user.id)}

    org_viewer = org_users.last

    api_v2_sign_in org_viewer

    get :paid_users, limit: 2, offset: 0, format: :json

    resp = get_json_response response
    assert_equal 2, resp['users'].count
  end

  test ':api should return unprocessable entity error if org subscription payment is not enabled' do
    org = create(:organization)
    user = create(:user)

    api_v2_sign_in user

    get :paid_users, format: :json
    assert_response :unprocessable_entity
  end

  test 'index api should include following key' do
    org = create(:organization)
    users = create_list(:user, 2, organization: org)
    viewer = create(:user, organization: org)

    create(:follow, followable: users.first, user: viewer)
    api_v2_sign_in viewer

    get :index, format: :json

    resp = get_json_response(response)
    assert resp['users'].select {|u| u['id'] == users.first.id}[0]['is_following']
    refute resp['users'].select {|u| u['id'] == users.last.id}[0]['is_following']

    assert_equal 1, resp['users'].select {|u| u['id'] == users.first.id}[0]['followers_count']
  end

  test 'role param filter in index' do
    org = create(:organization)
    create_org_master_roles org

    org_users = create_list(:user, 4, organization: org)
    org_users.each do |user|
      user.add_role(Role::TYPE_MEMBER)
    end

    org_viewer = org_users.last
    sme = org.sme_role
    influencer = Role.create(name: 'influencer', organization: org)

    org_users[0].roles << sme
    org_users[1].roles << influencer

    api_v2_sign_in org_viewer
    #sme
    get :index, role: ['sme'], format: :json

    resp = get_json_response response

    assert_response :ok
    assert_equal 'sme', resp['users'][0]['roles'].first
    assert_equal 1, resp['users'].count

    # member
    get :index, role: ['member'], format: :json

    resp = get_json_response response

    assert_response :ok
    assert_same_elements org_users.map(&:id), resp['users'].map {|u| u['id']}
    assert_equal 4, resp['users'].count
  end

  test 'search users' do
    org = create(:organization)

    fake_name1 = Faker::Name.name
    fake_name2 = Faker::Name.name

    org_users1 = create_list(:user, 5, organization: org, first_name: fake_name1)
    org_users2 = create_list(:user, 3, organization: org, first_name: fake_name2)

    org_viewer = org_users1.last
    api_v2_sign_in org_viewer

    get :index, q: fake_name1, status: 'active', format: :json

    resp = get_json_response response
    assert_response :ok
    assert_same_elements org_users1.map(&:id), resp['users'].map{|r| r['id']}
  end

  test 'get org users #search by full name' do
    org = create(:organization)
    first_name = Faker::Name.name
    org_users = create_list(:user, 10, organization: org, first_name: first_name)
    org_viewer = org_users.last

    api_v2_sign_in org_viewer

    get :index, format: :json, :q => "#{first_name} #{org_users.first[:last_name]}"

    resp = get_json_response response

    assert_response :ok
    assert_equal 1, resp['users'].count
  end

  test 'get org users #search by email' do
    org = create(:organization)
    create_org_master_roles(org)

    org_users = create_list(:user, 2, organization: org)
    org_viewer = org_users.last
    org_viewer.add_role(Role::TYPE_ADMIN)

    api_v2_sign_in org_viewer

    get :index, format: :json, :q => "#{org_users.first[:email]}"

    resp = get_json_response response

    assert_response :ok
    assert_equal 1, resp['users'].count
    assert_equal "#{org_users.first[:email]}", resp['users'].first["email"]
  end

  test 'only admin can find users by email' do
    org = create(:organization)
    org_users = create_list(:user, 5, organization: org)
    org_viewer = org_users.last

    api_v2_sign_in org_viewer

    get :index, format: :json, :q => "#{org_users.first[:email]}"

    resp = get_json_response response

    assert_response :ok
    assert_equal 0, resp['users'].count
  end

  test 'should support pagination' do
    org = create(:organization)
    org_users = create_list(:user, 50, organization: org, first_name: Faker::Name.name)
    org_viewer = org_users.last

    api_v2_sign_in org_viewer
    get :index, limit: 15, offset: 10, format: :json

    resp = get_json_response response

    assert_response :ok
    assert_equal 15, resp['users'].count
    assert_equal 50, resp['total']
  end

  test 'should filter by team ids' do
    org = create(:organization)
    user1, user2, viewer = create_list(:user, 3, organization: org)
    team1, team2 = create_list(:team, 2, organization: org)
    some_other_team = create(:team)

    team1.add_member user1
    team2.add_member user2

    api_v2_sign_in viewer
    get :index, team_ids: [team2.id, some_other_team.id], format: :json

    resp = get_json_response response
    assert_response :ok
    assert_equal 1, resp['users'].count
    assert_same_elements [user2.id], resp['users'].map{|r| r['id']}
  end

  test 'should filter by exclude_team_members and team_ids' do
    org = create(:organization)
    user1, user2, viewer = create_list(:user, 3, organization: org)
    team1, team2 = create_list(:team, 2, organization: org)

    team1.add_member user1
    team2.add_member user2

    api_v2_sign_in viewer
    get :index, team_ids: [team1.id], exclude_team_members: true, format: :json

    resp = get_json_response response
    assert_response :ok
    assert_equal 2, resp['users'].count
    assert_same_ids [user2.id, viewer.id], resp['users'].map{|r| r['id']}
  end

  test 'should not return users with ids from exclude_user_ids' do
    org = create(:organization)
    user1, user2, viewer = create_list(:user, 3, organization: org)

    api_v2_sign_in viewer
    get :index, exclude_user_ids: [user1.id, user2.id], format: :json

    resp = get_json_response response
    assert_response :ok
    assert_equal 1, resp['users'].count
    assert_same_ids [viewer.id], resp['users'].map{|r| r['id']}
  end

  test 'should filter only by teams in current org' do
    org = create(:organization)
    other_org = create(:organization)
    user1, user2, viewer = create_list(:user, 3, organization: org)
    other_user = create(:user, organization: other_org)
    team1, team2 = create_list(:team, 2, organization: org)
    other_team = create(:team, organization: other_org)

    team1.add_member user1
    team2.add_member user2
    other_team.add_member other_user

    api_v2_sign_in viewer
    get :index, team_ids: [team1.id, other_team.id], format: :json

    resp = get_json_response response
    assert_response :ok
    assert_not_includes resp['users'].map{|r| r['id']}, other_team.id
  end

  test 'should filter by user ids' do
    org = create(:organization)
    user1, user2, viewer = create_list(:user, 3, organization: org)

    api_v2_sign_in viewer
    get :index, user_ids: [user1.id, user2.id], format: :json

    resp = get_json_response response
    assert_response :ok
    assert_equal 2, resp['users'].count
    assert_same_elements [user1.id,user2.id], resp['users'].map{|r| r['id']}
  end

  test 'should fetch users from teams I own if only_from_my_teams = true' do
    org = create(:organization)
    user1, user2, viewer = create_list(:user, 3, organization: org)

    team_list = create_list(:team, 2, organization: org)

    team_list[0].add_admin viewer
    team_list[0].add_admin user1

    team_list[1].add_member viewer
    team_list[1].add_member user2

    api_v2_sign_in viewer
    get :index, only_from_my_teams: true, format: :json
    resp = get_json_response response

    assert_response :ok
    assert_same_elements resp['users'].map{|d| d['id']}, [user1.id, viewer.id]
    assert_not_includes resp['users'].map{|d| d['id']}, user2.id
  end

  #SHOW
  test "should return user info" do
    org = create(:organization)
    user1, user2, viewer = create_list(:user, 3, organization: org)
    create(:follow, followable: user1, user: user2)

    api_v2_sign_in user2
    get :show, id: user1.id, format: :json

    assert_response :ok
    resp = get_json_response(response)
    assert_nil resp["profile"]
    assert resp['is_following']
    assert_not_nil resp['created_at']
  end

  test "should include expert and learning interests" do
    org = create(:organization)
    user1, user2, viewer = create_list(:user, 3, organization: org)

    create :user_profile, user: user1

    api_v2_sign_in user2
    get :show, id: user1.id, format: :json

    assert_response :ok
    resp = get_json_response(response)
    assert_not_nil resp["profile"]
  end
  #end SHOW

  # magic link
  test ':api should send magic link from mobile app' do
    org = create(:organization)
    user = create(:user, organization: org)
    mailer = mock()
    token = "1111222333"
    # one additional :is_native_app? is called in base controller
    @controller.expects(:is_native_app?).returns(true).times(3)
    User.any_instance.expects(:jwt_token).with({sent_from_native_app: true}).returns(token)
    UserMailer.expects(:magic_link).with(user, token).returns(mailer)
    mailer.expects(:deliver_later)
    set_host(org)

    get :magic_link, format: :json, email: user.email
    assert_response :ok
  end

  # magic link
  test ':api should send magic link from browser' do
    org = create(:organization)
    user = create(:user, organization: org)
    mailer = mock()
    token = "1111222333"
    # one additional :is_native_app? is called in base controller
    @controller.expects(:is_native_app?).returns(false).times(3)

    User.any_instance.expects(:jwt_token).with({sent_from_native_app: false}).returns(token)
    UserMailer.expects(:magic_link).with(user, token).returns(mailer)
    mailer.expects(:deliver_later)

    set_host(org)
    get :magic_link, format: :json, email: user.email

    assert_response :ok
  end

  test ':api should not send magic link if the user email doesnt exist in org' do
    org = create(:organization)
    user = create(:user)
    UserMailer.expects(:magic_link).never
    set_host(org)
    get :magic_link, format: :json, email: user.email
    assert_response :ok
  end

  test 'send magic link to the users having `sign_out_at` set' do
    org = create(:organization)
    user = create(:user, organization: org, sign_out_at: Time.now)

    mailer = mock()
    token = "1111222333"
    # one additional :is_native_app? is called in base controller
    @controller.expects(:is_native_app?).returns(true).times(3)
    User.any_instance.expects(:jwt_token).with({sent_from_native_app: true}).returns(token)
    UserMailer.expects(:magic_link).with(user, token).returns(mailer)
    mailer.expects(:deliver_later)
    set_host(org)

    get :magic_link, format: :json, email: user.email
    assert_response :ok
  end

  test ':api should not return duplicate roles for recommended users' do
    org = create(:organization)
    org_user = create(:user, organization: org, first_name: Faker::Name.name)
    admin_role = create :role, name: 'admin', organization: org
    sme_role = create :role, organization: org, name: 'sme', master_role: true
    org_user_role = create :user_role, user: org_user, role: admin_role

    peer_learners = create_list(:user, 46, organization: org)
    peer_learners.each {|u| create(:user_profile, user: u, learning_topics: Taxonomies.domain_topics.first(2)) }
    viewer = create(:user, organization: org)
    following_users = create_list(:user, 5, organization: org)
    following_users.each {|u| create(:user_profile, user: u, learning_topics: Taxonomies.domain_topics.first(2)) }

    api_v2_sign_in viewer

    Search::UserSearch.any_instance.expects(:search).
        with(q: nil, viewer: viewer, filter: {search_peer_learners: true}, allow_blank_q: true, limit: User::MAX_SEARCH, sort: :score, load: false).
        returns(Search::UserSearchResponse.new(results: (peer_learners), total: 2)).once

    Search::UserSearch.any_instance.expects(:search).
        with(q: nil, viewer: viewer, filter: {search_expertise: true, role: "sme"}, allow_blank_q: true, limit: User::MAX_SEARCH, load: false, sort: :score).
        returns(Search::UserSearchResponse.new(results: [org_user], total: 1))

    Search::UserSearch.any_instance.expects(:search).
        with(q: nil, viewer: viewer, filter: {search_expertise: true, following: true, role: "sme"}, allow_blank_q: true, limit: User::MAX_SEARCH, load: false, sort: :score).
        returns(Search::UserSearchResponse.new(results: (following_users), total: 1))

    get :recommended_users, limit: 50, format: :json
    resp = get_json_response(response)
    assert_response :ok

    assert_equal resp['users'].first['roles'], ['admin']
  end

  test ':api should authorize only org admins' do
    org = create(:organization)
    create_org_master_roles org
    member = create(:user, organization: org)
    member.add_role Role::TYPE_MEMBER
    api_v2_sign_in member
    post :exclude_from_leaderboard_status, id: member.id, format: :json
    assert_response :unauthorized
  end

  test ':api change leaderboard status of user' do
    org = create(:organization)
    create_org_master_roles org

    admin = create(:user, organization: org)
    admin.update organization_role: 'admin'
    admin.add_role(Role::TYPE_ADMIN)
    member = create(:user, organization: org)

    api_v2_sign_in admin
    post :exclude_from_leaderboard_status, id: member.id,
        exclude_from_leaderboard: true,  format: :json
    assert_response :ok
    assert member.reload.exclude_from_leaderboard
  end


  class UserSearchTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      create_org_master_roles @org

      @admin, @member1, @member2 = create_list(:user, 3, organization: @org)
      @admin.update(organization_role: 'admin')
      @member2.update(first_name: 'Ruby')
      @admin.add_role(Role::TYPE_ADMIN)
      @member1.add_role Role::TYPE_MEMBER
      @member2.add_role Role::TYPE_MEMBER
      create(:user_profile, user: @member1, expert_topics: [{'topic_label' => 'Ruby'}],
             tac_accepted: true, tac_accepted_at: Time.now, language: 'ru')
      create(:user_profile, user: @admin, expert_topics: [{'topic_label' => 'Ruby'}],
             tac_accepted: true, tac_accepted_at: Time.now, language: 'en')

      @cf_1 = create :custom_field, organization: @org, display_name: 'City', abbreviation: nil
      @cf_2 = create :custom_field, organization: @org, display_name: 'Country', abbreviation: nil
      @cf_3 = create :custom_field, organization: @org, display_name: 'Second country', abbreviation: nil

      # adds custom fields to existing user
      create :user_custom_field, custom_field: @cf_1, user: @member1, value: 'london'
      create :user_custom_field, custom_field: @cf_1, user: @member1, value: 'moscow'
      create :user_custom_field, custom_field: @cf_2, user: @member2, value: 'england'
      create :user_custom_field, custom_field: @cf_2, user: @member2, value: 'russia'
      create :user_custom_field, custom_field: @cf_3, user: @admin, value: 'russia'
    end

    #search
    test 'should return organization members' do
      api_v2_sign_in @member1

      @query = ''

      aggregations = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/users_aggs_response.json"))

      Search::UserSearch.any_instance.expects(:search).
        with(
          q: @query, viewer: @member1,
          allow_blank_q: true,
          filter: {skip_aggs: false},
          offset: 0, limit: 10,
          load: true, use_custom_fields: true,
          custom_fields_names: []
        ).returns(Search::UserSearchResponse.new(results: [@admin, @member1, @member2],
                                                 total: 3,
                                                 aggs: aggregations)
      )
      get :search, q: '', skip_aggs: false, fields: 'id,first_name,last_name,email,handle,status,created_at,is_suspended,current_sign_in_at,profile(tac_accepted,tac_accepted_at,language),profile', format: :json

      resp = get_json_response(response)
      users = resp["users"]
      assert_equal 3, resp['total']
      assert_same_ids [@admin.id, @member1.id, @member2.id], users.map{|user| user['id']}
      assert_equal 2, resp['aggregations'].count
      assert_equal ["custom_fields", "expert_topics"], resp['aggregations'].keys
      assert_equal true, resp['users'][1]["profile"]["tac_accepted"]
      assert_equal Date.today, resp['users'][1]["profile"]["tac_accepted_at"].to_date
      assert_equal @admin.created_at.to_date, resp['users'][0]['created_at'].to_date
      assert_equal @admin.language, resp['users'][0]['profile']['language']
    end

    test 'should return users with specified role' do
      api_v2_sign_in @member1

      @query = ''

      Search::UserSearch.any_instance.expects(:search).
        with(
          q: @query, viewer: @member1,
          allow_blank_q: true,
          filter: {skip_aggs: true, role: ['admin']},
          offset: 0, limit: 10,
          load: true, use_custom_fields: true,
          custom_fields_names: []
        ).returns(Search::UserSearchResponse.new(results: [@admin],
                                                 total: 1)
      )
      get :search, q: '', role: ['admin'], format: :json

      resp = get_json_response(response)
      users = resp['users']
      assert_equal 1, resp['total']
      assert_equal @admin.id, users.first['id']
    end

    test 'should return only suspended users' do
      api_v2_sign_in @member1

      @query = ''

      Search::UserSearch.any_instance.expects(:search).
        with(
          q: @query, viewer: @member1,
          allow_blank_q: true,
          filter: {skip_aggs: true, include_suspended: true, only_suspended: true},
          offset: 0, limit: 10,
          load: true, use_custom_fields: true,
          custom_fields_names: []
        ).returns(Search::UserSearchResponse.new(results: [@admin],
                                                 total: 1)
      )
      get :search, q: '', include_suspended: true, only_suspended: true, format: :json

      resp = get_json_response(response)
      users = resp['users']
      assert_equal 1, resp['total']
      assert_equal @admin.id, users.first['id']
    end

    test "should search users only by skills if param search_by is 'skills'" do
      api_v2_sign_in @member2

      @query = 'Ruby'

      Search::UserSearch.any_instance.expects(:search).
        with(
          q: @query, viewer: @member2,
          allow_blank_q: true,
          filter: { skip_aggs: true, search_by: ['expert_topics.searchable_topic_label'] },
          offset: 0, limit: 10,
          load: true, use_custom_fields: true,
          custom_fields_names: []
        ).returns(Search::UserSearchResponse.new(results: [@member1],
                                                 total: 1)
      )
      get :search, q: @query, search_by: 'skills', format: :json

      resp = get_json_response(response)
      users = resp['users']
      assert_equal 1, resp['total']
      assert_equal @member1.id, users.first['id']
    end

    test 'api should return users sorted by first name' do
      org = create(:organization)
      user1 = create(:user, organization: org, first_name: 'Abcdef')
      user2 = create(:user, organization: org, first_name: 'Nbcdef')
      user3 = create(:user, organization: org, first_name: 'Rbcdef')

      api_v2_sign_in user1

      query = 'bcdef'
      sort = 'first_name'
      order = 'asc'

      Search::UserSearch.any_instance.expects(:search).
        with(
          q: query,
          viewer: user1,
          allow_blank_q: true,
          filter: {skip_aggs: true},
          offset: 0,
          limit: 10,
          load: true,
          use_custom_fields: true,
          custom_fields_names: [],
          sort: sort,
          order: order
        ).returns(Search::UserSearchResponse.new(results: [user1, user2, user3], total: 3)
      )

      get :search, q: query, sort: sort, order: order, format: :json
      resp = get_json_response(response)['users']
      assert_equal user1.id, resp.first['id']
      assert_equal user2.id, resp.second['id']
      assert_equal user3.id, resp.last['id']
    end

    test 'api should return users sorted by last name' do
      org = create(:organization)
      user1 = create(:user, organization: org, last_name: 'Abcdef')
      user2 = create(:user, organization: org, last_name: 'Nbcdef')
      user3 = create(:user, organization: org, last_name: 'Rbcdef')

      api_v2_sign_in user1

      query = 'bcdef'
      sort = 'last_name'
      order = 'desc'

      Search::UserSearch.any_instance.expects(:search).
        with(
          q: query,
          viewer: user1,
          allow_blank_q: true,
          filter: {skip_aggs: true},
          offset: 0,
          limit: 10,
          load: true,
          use_custom_fields: true,
          custom_fields_names: [],
          sort: sort,
          order: order
        ).returns(Search::UserSearchResponse.new(results: [user3, user2, user1], total: 3)
      )

      get :search, q: query, sort: sort, order: order, format: :json
      resp = get_json_response(response)['users']
      assert_equal user3.id, resp.first['id']
      assert_equal user2.id, resp.second['id']
      assert_equal user1.id, resp.last['id']
    end

    test "should search users not by skills if param search_by is 'name'" do
      api_v2_sign_in @member1

      @query = 'Ruby'

      Search::UserSearch.any_instance.expects(:search).
        with(
          q: @query, viewer: @member1,
          allow_blank_q: true,
          filter: { skip_aggs: true, search_by: ['name.searchable', 'first_name.searchable', 'last_name.searchable'] },
          offset: 0, limit: 10,
          load: true, use_custom_fields: true,
          custom_fields_names: []
        ).returns(Search::UserSearchResponse.new(results: [@member2],
                                                 total: 1)
      )
      get :search, q: @query, search_by: 'name', format: :json

      resp = get_json_response(response)
      users = resp['users']
      assert_equal 1, resp['total']
      assert_equal @member2.id, users.first['id']
    end

    test 'should find users by custom field value' do
      api_v2_sign_in @member1

      @query = 'russia'

      Search::UserSearch.any_instance.expects(:search).
        with(
          q: @query, viewer: @member1,
          allow_blank_q: true,
          filter: {skip_aggs: true},
          offset: 0, limit: 10,
          load: true, use_custom_fields: true,
          custom_fields_names: []
        ).returns(Search::UserSearchResponse.new(results: [@member2, @admin],
                                                 total: 2)
      )
      get :search, q: @query, format: :json

      resp = get_json_response(response)
      users = resp['users']
      assert_equal 2, resp['total']
      assert_same_elements [@member2.id, @admin.id], users.map{|user| user['id']}
    end

    test 'should filter users by emails' do
      api_v2_sign_in @member1
      member1 = create(:user, email: 'member1@email.com')
      member2 = create(:user, email: 'member2@email.com')
      @query = ''
      Search::UserSearch.any_instance.expects(:search).
        with(
          q: @query, viewer: @member1,
          allow_blank_q: true,
          filter: {skip_aggs: true, emails_for_filtering: ['member1@email.com']},
          offset: 0, limit: 10,
          load: true, use_custom_fields: true,
          custom_fields_names: []
        ).returns(Search::UserSearchResponse.new(results: [member1],
                                                 total: 1)
      )
      get :search, q: '', emails_for_filtering: ['member1@email.com'], format: :json
      resp = get_json_response(response)
      users = resp['users']
      assert_equal 1, resp['total']
      assert_equal [member1.id], users.map{|user| user['id']}
    end

    test 'should filter users by names' do
      api_v2_sign_in @member1
      member1 = create(:user, first_name: 'Test1', last_name: 'User')
      member2 = create(:user, first_name: 'Test2', last_name: 'User')
      @query = ''
      Search::UserSearch.any_instance.expects(:search).
        with(
          q: @query, viewer: @member1,
          allow_blank_q: true,
          filter: {skip_aggs: true, names_for_filtering: ['test2 user']},
          offset: 0, limit: 10,
          load: true, use_custom_fields: true,
          custom_fields_names: []
        ).returns(Search::UserSearchResponse.new(results: [member2],
                                                 total: 1)
      )
      get :search, q: '', names_for_filtering: ['test2 user'], format: :json
      resp = get_json_response(response)
      users = resp['users']
      assert_equal 1, resp['total']
      assert_same_elements [member2.id], users.map{|user| user['id']}
    end

    test 'should return user_ids if params only_user_ids' do
      members = create_list(:user, 4, organization: @org)
      api_v2_sign_in @member1

      @query = 'ruby'
      for i in 0..3 do
        Search::UserSearch.any_instance.expects(:search).with(
          q: @query, viewer: @member1,
          allow_blank_q: true,
          filter: {skip_aggs: true},
          offset: i, limit: 1,
          load: true, use_custom_fields: true,
          custom_fields_names: []
        ).returns(
          Search::UserSearchResponse.new(
            results: [members[i]], total: 4
          )
        )
      end

      get :search, q: @query, limit: 1, only_user_ids: 'true', format: :json

      resp = get_json_response(response)
      user_ids = resp['user_ids']
      member_ids = members.map { |user| user['id'] }
      assert_equal 4, resp['total']
      assert_same_elements member_ids, user_ids
    end
  end

  class ExpertiseTopicsActionTest < ActionController::TestCase
    setup do
      organization = create :organization
      @user        = create :user, organization: organization
      create :user_profile, user: @user

      api_v2_sign_in @user
    end

    test ':api must return list of expert_topics' do
      get :expert_topics, id: @user.id, format: :json

      @user.reload
      expert_topics = get_json_response(response)
      assert_response :ok
      assert_equal @user.profile.expert_topics, expert_topics
    end

    test 'returns empty list if profile does not exists' do
      @user.profile.destroy

      get :expert_topics, id: @user.id, format: :json

      expert_topics = get_json_response(response)
      assert_response :ok
      assert_nil expert_topics
    end

    test 'returns :not_found if user does not exists' do
      get :expert_topics, id: 10000, format: :json

      assert_response :not_found
    end
  end

  class LearningTopicsActionTest < ActionController::TestCase
    setup do
    organization = create :organization
      @user        = create :user, organization: organization
      create :user_profile, user: @user

      api_v2_sign_in @user
    end

    test ':api must return list of learning_topics' do
      get :learning_topics, id: @user.id, format: :json

      @user.reload
      learning_topics = get_json_response(response)
      assert_response :ok
      assert_equal @user.learning_topics, learning_topics
    end

    test 'returns empty list if profile does not exists' do
      @user.profile.destroy

      get :learning_topics, id: @user.id, format: :json

      learning_topics = get_json_response(response)

      assert_response :ok
      assert_nil learning_topics
    end

    test 'returns :not_found if user does not exists' do
      get :learning_topics, id: 10000, format: :json

      assert_response :not_found
    end
  end

  class PwcRecordsActionTest < ActionController::TestCase
    setup do
      organization = create :organization
      @user        = create :user, organization: organization
      create :pwc_record, abbr: "sb", user: @user
      create :pwc_record, abbr: "ab", user: @user

      api_v2_sign_in @user
    end

    test ':api must return list of pwc records' do
      get :pwc_records, id: @user.id, format: :json

      @user.reload
      resp = get_json_response(response)
      assert_response :ok

      assert_equal 2, resp["pwc_records"].length
    end

    test 'returns :not_found if user does not exists' do
      get :pwc_records, id: 10000, format: :json

      assert_response :not_found
    end
  end

  class CardReviewStart < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      api_v2_sign_in @user
    end

    test ':api should return logged in user information' do
      create_org_master_roles @org
      @user.roles << @org.roles.find_by_name('member')
      get :info, format: :json

      resp = get_json_response(response)
      valid_resp_keys = %w[
        id email first_name last_name handle
        picture_url token coverimage_urls avatarimage_urls
        following_count followers_count roles permissions web_session_timeout
        roles_default_names onboarding_completed step default_team_id bio is_suspended
      ]
      assert_equal @user.id, resp['id']
      assert_equal @user.roles.map(&:name), resp['roles']
      assert_not_empty resp['permissions']
      assert_not_nil resp['token']
      resp.assert_valid_keys(valid_resp_keys)
    end


    test 'should return following count' do
      users = create_list(:user, 3, organization: @org)
      following_others = users.map {|user| create(:follow, user: @user, followable: user)}
      followed_by_users = users.first(2).map {|user| create(:follow, followable: @user, user: user)}

      get :info, format: :json

      resp = get_json_response(response)
      assert_equal following_others.count, resp['following_count']
      assert_equal followed_by_users.count, resp['followers_count']
    end
  end

  #get_clc
  test ":api clc should return progress with for user" do
    organization = create(:organization)
    user = create(:user, organization: organization)
    clc = create(:clc, name: 'test clc', entity: organization, organization: organization)
    clc_progress = create(:clc_progress, user: user, clc: clc)
    api_v2_sign_in(user)

    get :clc, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal clc.id, resp["clc_id"]
  end

  test ":api clc should return progress with for particular user if user_id is passed" do
    organization = create(:organization)
    user1, user2 = create_list(:user, 2, organization: organization)
    clc1 = create(:clc, name: 'test clc', entity: organization, organization: organization)
    clc2 = create(:clc, name: 'test clc2', entity: organization, organization: organization)

    clc_progress1 = create(:clc_progress, user: user1, clc: clc1)
    clc_progress2 = create(:clc_progress, user: user2, clc: clc2)

    api_v2_sign_in(user1)

    get :clc, user_id: user2.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal clc2.id, resp["clc_id"]
  end


  test ":api clc should return 0 score if no progress present" do
    organization = create(:organization)
    user = create(:user, organization: organization)
    clc = create(:clc, name: 'test clc', entity: organization, organization: organization)
    api_v2_sign_in(user)

    get :clc, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal 0, resp["score"]
  end

  test "should return record not found if clc not found" do
    organization = create(:organization)
    user = create(:user, organization: organization)
    api_v2_sign_in(user)
    get :clc, format: :json
    assert_response :not_found
  end

  test "should return appropriate badging details when user earns badge" do
    organization = create(:organization)
    user = create(:user, organization: organization)
    clc_badge1 = create(:clc_badge, organization: organization)
    clc_badge2 = create(:clc_badge, organization: organization)
    clc = create(:clc, name: 'test clc', entity: organization, organization: organization,
                 clc_badgings_attributes: [{badge_id: clc_badge1.id,
                                            title: "clc-25",
                                            target_steps:25},
                                            {badge_id: clc_badge2.id,
                                            title: "clc-50",
                                            target_steps:50}])
    clc_badging1 = clc.clc_badgings[0]
    clc_badging2 = clc.clc_badgings[1]
    user_badge1 = create(:user_badge, user_id: user.id, badging_id: clc_badging1.id)

    api_v2_sign_in(user)

    get :clc, clc: {entity_id: organization, entity_type: "Organization"}, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal clc.id, resp["clc_id"]
    assert_equal clc_badging1.id, resp['earned_badge']['id'].to_i


    user_badge2 = create(:user_badge, user_id: user.id, badging_id: clc_badging2.id)

    get :clc, clc: {entity_id: organization, entity_type: "Organization"}, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal clc.id, resp["clc_id"]
    assert_equal clc_badging2.id, resp['earned_badge']['id'].to_i
  end

  # follow
  test ':api current user should follow other user' do
    org = create(:organization)
    follower, following_user = create_list(:user, 2, organization: org)
    api_v2_sign_in follower

    assert_difference -> {Follow.count} do
      post :follow, id: following_user.id, format: :json
      assert_response :ok
    end

    assert following_user.followers.include? follower
  end

  # unfollow
  test ':api current user should be able to unfollow following user' do
    Follow.any_instance.stubs(:record_user_follow_action)
    org = create(:organization)
    follower, following_user = create_list(:user, 2, organization: org)
    follow = create(:follow, user_id: follower.id, followable: following_user)
    api_v2_sign_in follower

    assert_difference -> {Follow.count}, -1 do
      post :unfollow, id: following_user.id, format: :json
      assert_response :ok
    end

    refute following_user.followers.include? follower
  end

  # onboarding
  class UserOnboardingTest < ActionController::TestCase

    setup do
      User.any_instance.unstub(:generate_user_onboarding)

      @org = create(:organization)
      @onboarded_user = create(:user, :with_onboarding_complete, organization: @org)
      @unonboarded_user = create(:user, organization: @org, password_reset_required: true)

      # after create callback in user factory sets it to complete. restarting onbaording
      # this mimics an onboarded user, and the user is already onboarding seet test/factories.rb#L109
      @unonboarded_user.user_onboarding.update status: 'new', current_step: 1, completed_at: nil
      api_v2_sign_in @unonboarded_user
    end

    # onboarding status
    test ':api get status for unonboarded user' do
      user_onboarding = @unonboarded_user.user_onboarding
      user_onboarding.start!
      user_onboarding.current_step = 3
      user_onboarding.save

      get :onboarding_status, format: :json

      json_response = get_json_response(response)
      assert_equal 3, json_response['step']
      assert_equal user_onboarding.user.id, json_response['user']['id']
      assert_equal false, json_response['onboarding_completed']
    end

    test 'status for onboarding completed user' do
      api_v2_sign_in @onboarded_user
      get :onboarding_status, format: :json
      assert get_json_response(response)['onboarding_completed']
    end

    test 'status for onboarding completed user #camel case formatting' do
      api_v2_sign_in @onboarded_user
      get :onboarding_status, camel_case_format: 'true', format: :json
      assert get_json_response(response)['onboarding_completed']
      assert_not_nil get_json_response(response)['jwt_token']
    end

    # onboarding update
    test ":api user update onboarding" do
      onboarding_options = UserOnboarding::DEFAULT_ONBOARDING_STEPS.merge({ add_interests: 2})
      create :config, configable: @org, name: 'onboarding_options', value: onboarding_options.to_json, data_type: :json
      pic = Rails.root.join('test/fixtures/images/logo.png')
      password = User.random_password
      put :onboarding, id: @unonboarded_user.id, step: 1,  user: { first_name: "John", last_name: "Hancock", password: password, bio: "Lets be clear, my last_name is pronounced `Hance ock`, not Han cock."}, format: :json
      assert_response :ok
      @unonboarded_user.reload

      assert_equal "John", @unonboarded_user.first_name
      assert_equal "Hancock", @unonboarded_user.last_name
      assert @unonboarded_user.valid_password?(password)
      assert_equal "Lets be clear, my last_name is pronounced `Hance ock`, not Han cock.", @unonboarded_user.bio
      assert_equal 2, @unonboarded_user.user_onboarding.current_step
      assert @unonboarded_user.user_onboarding.started?
    end

    test ":api reset onboarding" do
      onboarding = @unonboarded_user.user_onboarding
      onboarding.start!
      onboarding.complete!
      assert @unonboarded_user.is_complete
      post :reset_onboarding
      assert_equal onboarding.reload.status, "new"
      refute @unonboarded_user.reload.is_complete
    end

    test "user onboarding step 4" do
      user_onboarding = @unonboarded_user.user_onboarding
      user_onboarding.start
      user_onboarding.save
      Timecop.freeze do
        put :onboarding, id: @unonboarded_user.id, step:3, user: {}
        @unonboarded_user.reload
        assert_not_nil @unonboarded_user.user_onboarding.completed_at
        assert @unonboarded_user.user_onboarding.completed?
      end
    end

    test '(flexible onboarding) completes user onboarding if `profile_setup` + `add_interests` configured' do
      topics = Taxonomies.domain_topics
      onboarding_options = UserOnboarding::DEFAULT_ONBOARDING_STEPS.merge({ add_interests: 2 })
      user_profile = create :user_profile, user: @unonboarded_user, onboarding_options: onboarding_options

      # 1. Completes `profile_setup` step
      put :onboarding, id: @unonboarded_user.id, step: 1,
        user: { first_name: "John", last_name: "Hancock", password: User.random_password,
          bio: "Lets be clear, my last_name is pronounced `Hance ock`, not Han cock."},
        custom_onboarding: 'true'

      # marks onboarding as STARTED
      refute @unonboarded_user.reload.user_onboarding.completed?
      assert @unonboarded_user.reload.user_onboarding.started?

      # 2. Completes `add_interests` step
      put :onboarding, id: @unonboarded_user.id, step: 2,
        user: { profile_attributes: { learning_topics: topics } },
        custom_onboarding: 'true'

      # marks onboarding as COMPLETED
      assert @unonboarded_user.reload.user_onboarding.completed?
    end

    test '(flexible onboarding) completes user onboarding if `profile_setup` + `add_expertise` configured' do
      topics = Taxonomies.domain_topics
      onboarding_options = UserOnboarding::DEFAULT_ONBOARDING_STEPS.merge({ add_expertise: 3 })
      user_profile = create :user_profile, user: @unonboarded_user, onboarding_options: onboarding_options

      # 1. Completes `profile_setup` step
      put :onboarding, id: @unonboarded_user.id, step: 1,
        user: { first_name: "John", last_name: "Hancock", password: User.random_password,
          bio: "Lets be clear, my last_name is pronounced `Hance ock`, not Han cock."},
        custom_onboarding: 'true'

      # marks onboarding as STARTED
      refute @unonboarded_user.reload.user_onboarding.completed?
      assert @unonboarded_user.reload.user_onboarding.started?
      assert_equal 3, @unonboarded_user.reload.user_onboarding.current_step

      # 2. Completes `add_expertise` step
      put :onboarding, id: @unonboarded_user.id, step: 3,
        user: { profile_attributes: { expert_topics: topics } },
        custom_onboarding: 'true'

      # marks onboarding as COMPLETED
      assert @unonboarded_user.reload.user_onboarding.completed?
      assert_equal 3, @unonboarded_user.reload.user_onboarding.current_step
    end

    test '(flexible onboarding) completes user onboarding if all onboarding steps are configured' do
      topics = Taxonomies.domain_topics
      onboarding_options = UserOnboarding::DEFAULT_ONBOARDING_STEPS.merge(UserOnboarding::OPTIONAL_ONBOARDING_STEPS)
      create :config, name: 'onboarding_options', value: onboarding_options.to_json, configable: @org, data_type: 'json'

      # 1. Completes `profile_setup` step
      put :onboarding, id: @unonboarded_user.id, step: 1,
        user: { first_name: "John", last_name: "Hancock", password: User.random_password },
        custom_onboarding: 'true'
      # marks onboarding as STARTED
      refute @unonboarded_user.reload.user_onboarding.completed?
      assert @unonboarded_user.reload.user_onboarding.started?
      assert_equal 2, @unonboarded_user.reload.user_onboarding.current_step

      # 2. Completes `add_interests` step
      put :onboarding, id: @unonboarded_user.id, step: 2,
        user: { profile_attributes: { learning_topics: topics } },
        custom_onboarding: 'true'
      # still onboarding is INCOMPLETE
      refute @unonboarded_user.reload.user_onboarding.completed?
      # profile is created with learning_topics
      profile = @unonboarded_user.reload.profile
      assert profile
      assert profile.learning_topics
      assert_equal 3, @unonboarded_user.reload.user_onboarding.current_step

      # 3. Completes `add_expertise` step
      put :onboarding, id: @unonboarded_user.id, step: 3,
        user: { profile_attributes: { expert_topics: topics } },
        custom_onboarding: 'true'
      # marks onboarding as COMPLETED
      assert @unonboarded_user.reload.user_onboarding.completed?
      # profile is updated with expert topics and retaining learning_interests
      existing_profile = @unonboarded_user.reload.profile
      assert_equal profile.id, existing_profile.id
      assert existing_profile
      assert existing_profile.learning_topics
      assert existing_profile.expert_topics
      assert_equal 3, @unonboarded_user.reload.user_onboarding.current_step
    end

    test 'exits silently if onboarding is completed and does not updates profile' do
      onboarding_options = UserOnboarding::DEFAULT_ONBOARDING_STEPS
      User.any_instance.stubs(:onboarding_options).returns(onboarding_options)
      user_profile = create :user_profile, user: @unonboarded_user
      user_onboarding = @unonboarded_user.user_onboarding
      user_onboarding.start!
      user_onboarding.complete!

      put :onboarding, id: @unonboarded_user.id, step: 1,
        user: { first_name: "John", last_name: "Hancock", password: User.random_password },
        custom_onboarding: 'true'
      assert_response :ok
      assert_not_equal 'John', @unonboarded_user.reload.first_name
      assert_not_equal 'Hancock', @unonboarded_user.reload.last_name
    end

    test 'skips password when user signs up with SSO' do
      # add identity provider to user
      provider = create :identity_provider, :saml, user: @unonboarded_user

      put :onboarding, id: @unonboarded_user.id, step: 1,
        user: { first_name: 'X-Men', last_name: 'Wolverine', password: 'password' }

      assert_response :success

      @unonboarded_user.reload
      assert_equal 'X-Men', @unonboarded_user.first_name
      assert_equal 'Wolverine', @unonboarded_user.last_name
    end

    test 'update plain password when encryption_payload config is enabled' do
      onboarding_options = UserOnboarding::DEFAULT_ONBOARDING_STEPS.merge({ add_interests: 2})
      create :config, :boolean, name: 'encryption_payload', value: 'true', configable: @org
      create :config, configable: @org, name: 'onboarding_options', value: onboarding_options.to_json, data_type: :json
      password = User.random_password
      put :onboarding, id: @unonboarded_user.id, step: 1,  user: { first_name: "John", last_name: "Hancock", password: password, bio: "Lets be clear, my last_name is pronounced `Hance ock`, not Han cock."}, format: :json
      assert_response :ok
    end

  end

  class UserOnboardingUpdateTest < ActionController::TestCase
    setup do
    User.any_instance.unstub(:generate_user_onboarding)
      @org = create(:organization)
      stub_okta_user_update
      @unonboarded_user, @onboarded_user = create_list(:user, 2, organization: @org)

      # after create callback in user factory sets it to complete. restarting onbaording
      # this mimics an onboarded user, and the user is already onboarding seet test/factories.rb#L109
      @unonboarded_user.user_onboarding.update status: 'new', current_step: 1, completed_at: nil
      api_v2_sign_in @unonboarded_user
    end

    test "update expert_topics" do
      @unonboarded_user.user_onboarding.start!
      @unonboarded_user.user_onboarding.update_attributes current_step: 2

      topics = Taxonomies.domain_topics
      put :onboarding, id: @unonboarded_user.id, step:2, user: { profile_attributes: { expert_topics: topics } }

      profile = @unonboarded_user.reload.profile
      assert_equal profile.expert_topics, topics.collect { |d_t| d_t.stringify_keys! }
    end

    test "update learning_topics" do
      @unonboarded_user.user_onboarding.start!
      @unonboarded_user.user_onboarding.update_attributes current_step: 2

      topics = Taxonomies.domain_topics
      put :onboarding, id: @unonboarded_user.id, step:2, user: { profile_attributes: { learning_topics: topics } }

      profile = @unonboarded_user.reload.profile
      assert_equal profile.learning_topics, topics.collect { |d_t| d_t.stringify_keys! }
    end
  end

  # recommended users
  test ':api should return recommended users' do
    org = create(:organization)
    role = create :role, organization: org, name: 'sme', master_role: true

    peer_learners = create_list(:user, 40, organization: org)
    peer_learners.each {|u| create(:user_profile, user: u, learning_topics: Taxonomies.domain_topics.first(2)) }
    expert = create(:user, organization: org)
    recommended_users = create_list(:user, 4, organization: org)

    recommended_users << expert
    recommended_users.each {|u| create(:user_profile, user: u, expert_topics: Taxonomies.domain_topics.first(2)); role.add_user(u) }
    member_user = create(:user, organization: org)

    following_users = create_list(:user, 5, organization: org)
    following_users.each {|u|
        create(:user_profile, user: u, learning_topics: Taxonomies.domain_topics.first(2))
        create(:follow, user_id: member_user.id, followable_id: u.id, followable_type: User)
    }

    Search::UserSearch.any_instance.expects(:search).
        with(q: nil, viewer: member_user, filter: {search_peer_learners: true}, allow_blank_q: true, limit: User::MAX_SEARCH, sort: :score, load: false).
        returns(Search::UserSearchResponse.new(results: peer_learners, total: 2)).once

    Search::UserSearch.any_instance.expects(:search).
        with(q: nil, viewer: member_user, filter: {search_expertise: true, role: "sme"}, allow_blank_q: true, limit: User::MAX_SEARCH, load: false, sort: :score).
        returns(Search::UserSearchResponse.new(results: recommended_users, total: 2)).once

    Search::UserSearch.any_instance.expects(:search).
        with(q: nil, viewer: member_user, filter: {search_expertise: true, following: true, role: "sme"}, allow_blank_q: true, limit: User::MAX_SEARCH, load: false, sort: :score).
        returns(Search::UserSearchResponse.new(results: following_users, total: 2)).once

    other_user = create(:user, organization: org)
    create(:user_profile, user: other_user, expert_topics: Taxonomies.domain_topics.first(1))


    create(:user_profile, user: member_user, learning_topics: Taxonomies.domain_topics)

    api_v2_sign_in member_user

    get :recommended_users, limit: 50, format: :json
    resp = get_json_response(response)

    assert_same_elements (recommended_users.map(&:id)+peer_learners.map(&:id)+following_users.map(&:id)), resp['users'].map {|u| u['id']}
    following_users.map(&:id).each do |i|
        assert resp['users'].detect{|x| x['id'] == i}['is_following']
    end
    assert_equal 50, resp['users'].count
  end

  test "should return group leaders" do
    @org   = create :organization
    @user  = create :user, organization: @org
    @sme = create :role, name: "sme", default_name: "sme", organization: @org
    @admin = create :user, organization: @org, organization_role: 'admin'
    Search::UserSearch.any_instance.expects(:search).
        with(q: nil, viewer: @user, filter: {search_expertise: true, role: "sme"}, allow_blank_q: true, limit: User::MAX_SEARCH, load: false, sort: :score).
        returns(Search::UserSearchResponse.new(results: [], total: 2)).once

    Search::UserSearch.any_instance.expects(:search).
        with(q: nil, viewer: @user, filter: {search_peer_learners: true}, allow_blank_q: true, limit: User::MAX_SEARCH, sort: :score, load: false).
        returns(Search::UserSearchResponse.new(results: [], total: 2)).once

    Search::UserSearch.any_instance.expects(:search).
        with(q: nil, viewer: @user, filter: {search_expertise: true, following: true, role: "sme"}, allow_blank_q: true, limit: User::MAX_SEARCH, load: false, sort: :score).
        returns(Search::UserSearchResponse.new(results: [], total: 2)).once

    @teams = []
    (1..4).each do |i|
      @teams << create(:team, organization: @org, created_at: i.minutes.ago)
    end

    @team1, @team2, @team3 = *@teams[0..2]
    @team4 = @teams.last

    @admin1, @admin2, @admin3, @admin4 = create_list :user, 4, organization: @org

    @team1.add_admin @admin1
    @team2.add_admin @admin2
    @team3.add_admin @admin3
    @team4.add_admin @admin4

    @team1.add_member @user
    @team2.add_member @user
    @team3.add_member @user

    my_team_admins = [@admin1, @admin2, @admin3]

    api_v2_sign_in @user

    get :recommended_users, limit: 50, format: :json
    resp = get_json_response(response)
    assert_same_elements my_team_admins.map(&:id), resp["users"].map{|a| a["id"]}

    # [DEPRECATED] only roles are not returned since they are not used.
    # assert_equal ["group_leader"], resp["users"].map{|a| a["group_role"]}.uniq

    #should not include admin of the team in which user is not a member
    assert_not_includes my_team_admins.map(&:id), @admin4.id
  end

  test 'should return recommended users with pagination' do
    org = create(:organization)
    role = create :role, organization: org, name: 'sme', master_role: true
    expert = create(:user, organization: org)
    recommended_users = create_list(:user, 4, organization: org)

    recommended_users << expert
    recommended_users.each {|u| create(:user_profile, user: u, expert_topics: Taxonomies.domain_topics.first(2)) }

    following_users = create_list(:user, 1, organization: org)
    following_users.each {|u| create(:user_profile, user: u, learning_topics: Taxonomies.domain_topics.first(2)) }

    other_user = create(:user, organization: org)
    create(:user_profile, user: other_user, expert_topics: Taxonomies.domain_topics.first(1))

    member_user = create(:user, organization: org)
    create(:user_profile, user: member_user, learning_topics: Taxonomies.domain_topics)

    Search::UserSearch.any_instance.expects(:search).
        with(q: nil, viewer: member_user, filter: {search_expertise: true, role: "sme"}, allow_blank_q: true, limit: User::MAX_SEARCH, load: false, sort: :score).
        returns(Search::UserSearchResponse.new(results: recommended_users, total: 2))

    Search::UserSearch.any_instance.expects(:search).
        with(q: nil, viewer: member_user, allow_blank_q: true, filter: {search_peer_learners: true}, limit: User::MAX_SEARCH, sort: :score, load: false).
        returns(Search::UserSearchResponse.new(results: [], total: 2)).once

    Search::UserSearch.any_instance.expects(:search).
        with(q: nil, viewer: member_user, filter: {search_expertise: true, following: true, role: "sme"}, allow_blank_q: true, limit: User::MAX_SEARCH, load: false, sort: :score).
        returns(Search::UserSearchResponse.new(results: following_users, total: 2))

    api_v2_sign_in member_user

    get :recommended_users, limit: 50, offset: 1, format: :json
    resp = get_json_response(response)
    assert_equal resp["users"].count, 4
    assert_equal resp["users"][0].keys, ["id", "handle", "avatarimages", "roles", "roles_default_names", "name", "is_following", "following_count", "followers_count", "expert_skills", "company"]
   end

  test ':api should return recommended users from groups that the current user belongs to' do
    org = create(:organization)
    team = create(:team, organization: org)
    sme = create :role, name: "sme", default_name: "sme", organization: org
    peer_learners = create_list(:user, 5, organization: org)
    peer_learners.each {|u| create(:user_profile, user: u, learning_topics: Taxonomies.domain_topics.first(2)) }
    expert = create(:user, organization: org)

    recommended_users = create_list(:user, 4, organization: org)
    recommended_users << expert
    recommended_users.each {|u| create(:user_profile, user: u, expert_topics: Taxonomies.domain_topics.first(2)) }
    member_user = create(:user, organization: org)

    team.add_to_team recommended_users
    team.add_member member_user

    following_users = create_list(:user, 5, organization: org)
    following_users.each {|u| create(:user_profile, user: u, learning_topics: Taxonomies.domain_topics.first(2)) }

    Search::UserSearch.any_instance.expects(:search).
        with(q: nil, viewer: member_user, filter: {search_expertise: true, role: "sme"}, allow_blank_q: true, limit: User::MAX_SEARCH, load: false, sort: :score).
        returns(Search::UserSearchResponse.new(results: recommended_users, total: 2)).once

    Search::UserSearch.any_instance.expects(:search).
        with(q: nil, viewer: member_user, filter: {search_expertise: true, following: true, role: "sme"}, allow_blank_q: true, limit: User::MAX_SEARCH, load: false, sort: :score).
        returns(Search::UserSearchResponse.new(results: following_users, total: 2)).once

    other_user = create(:user, organization: org)
    create(:user_profile, user: other_user, expert_topics: Taxonomies.domain_topics.first(1))
    create(:user_profile, user: member_user, learning_topics: Taxonomies.domain_topics)

    api_v2_sign_in member_user

    get :recommended_users, limit: 50, my_teams_users: 'true', format: :json
    resp = get_json_response(response)

    assert_same_elements (team.users.pluck(:id) - [member_user.id]), resp['users'].map {|u| u['id']}
    assert_equal 5, resp['users'].length
  end

  test ':api my_teams_users parameter should return an empty array if not found any user' do
    org = create(:organization)
    team = create(:team, organization: org)
    member_user = create(:user, organization: org)
    sme = create :role, name: "sme", default_name: "sme", organization: org
    team.add_member member_user

    api_v2_sign_in member_user
    Search::UserSearch.any_instance.stubs(:search)
                      .returns(Search::UserSearchResponse.new(results: User.none, total: 0))

    get :recommended_users, limit: 50, my_teams_users: 'true', format: :json
    res = get_json_response(response)
    assert_response :ok
    assert_empty res['users']
  end

  class InviteLinkWithJWTToken < ActionController::TestCase
    setup do
      stub_request(:post, 'https://api.branch.io/v1/url').to_return(status: 200, body: '', headers: {})
      Organization.any_instance.stubs(:is_auth_enabled?).returns(true)
      @org = create(:organization)
      set_host(@org)
    end

    test 'invitation link login should only work one time with 24 hours expiry' do
      user = create(:user, organization: @org, invitation_accepted: false)
      token = user.jwt_token

      assert_equal false, user.invitation_accepted
      get :login_landing, token: token, host_name: @org.host_name, invitation_link: true
      assert_response :redirect
      assert_redirected_to '/'
      assert_equal true, user.reload.invitation_accepted
    end

    test 'invitation link login should return head ok if user agent is Branch Metrics API' do
      user = create(:user, organization: @org, invitation_accepted: false)
      token = user.jwt_token

      assert_equal false, user.invitation_accepted
      request.headers['HTTP_USER_AGENT'] = 'Branch Metrics API'
      get :login_landing, token: token, host_name: @org.host_name, invitation_link: true
      assert_response :ok
      assert_equal false, user.reload.invitation_accepted
    end

    test 'invitation link login should not work if invitation_accepted' do
      user = create(:user, organization: @org, invitation_accepted: true)
      token = user.jwt_token

      assert_equal true, user.invitation_accepted
      get :login_landing, token: token, host_name: @org.host_name, invitation_link: true
      assert_response :redirect
      assert_redirected_to '/'
      assert_equal true, user.reload.invitation_accepted
    end

    test 'invitation link login should be valid for 24 hours only' do
      user = create(:user, organization: @org, invitation_accepted: false)
      token = user.jwt_token({timestamp: 48.hours.ago})

      get :login_landing, token: token, host_name: @org.host_name, invitation_link: true
      assert_response :redirect
      assert_redirected_to '/'
      assert_equal false, user.reload.invitation_accepted
    end
  end

  class InviteLinkWithWelcomeToken < ActionController::TestCase
    setup do
      stub_request(:post, 'https://api.branch.io/v1/url').to_return(status: 200, body: '', headers: {})
      Organization.any_instance.stubs(:is_auth_enabled?).returns(true)
      @org = create(:organization)
      set_host(@org)
    end

    test 'invitation link login should only work one time with 24 hours expiry' do
      Devise.token_generator.stubs(:digest).returns("abc")
      user = create(:user, organization: @org, invitation_accepted: false, welcome_token: "abc")

      get :login_landing, welcome_token: "xyz", host_name: @org.host_name, invitation_link: true
      assert_response :redirect
      assert_redirected_to '/'
      assert_equal true, user.reload.invitation_accepted
      assert_equal "abc", user.welcome_token
    end

    test 'invitation link should not work if welcome_token is not valid' do
      Devise.token_generator.stubs(:digest).returns("ac")
      user = create(:user, organization: @org, invitation_accepted: false, welcome_token: "abc")

      get :login_landing, welcome_token: "xyz", host_name: @org.host_name, invitation_link: true
      resp = JSON.parse response.body
      assert_response :unauthorized
      assert_equal "Welcome token is expired or invalid", resp["message"]
    end

    test 'invitation link should not work if user is created more 24 hours ago' do
      Devise.token_generator.stubs(:digest).returns("abc")
      user = create(:user, organization: @org, invitation_accepted: false, welcome_token: "abc", created_at: 25.hours.ago)

      get :login_landing, welcome_token: "xyz", host_name: @org.host_name, invitation_link: true
      assert_response :redirect
      assert_redirected_to '/'
      assert_equal false, user.reload.invitation_accepted
    end

    test 'invitation link login should not work if invitation is accepted' do
      Devise.token_generator.stubs(:digest).returns("abc")
      user = create(:user, organization: @org, invitation_accepted: true, welcome_token: 'abc')

      get :login_landing, welcome_token: "abc", host_name: @org.host_name, invitation_link: true
      assert_response :redirect
      assert_redirected_to '/'
    end
  end

  #magic-link
  test 'magic link login for browser' do
    org = create(:organization)
    user = create(:user, organization: org)
    token = user.jwt_token
    set_host(org)

    get :magic_link_login, token: token,  host_name: org.host_name
    assert_response :redirect
    assert_redirected_to '/'
  end

  test 'magic link login for mobile app' do
    org = create(:organization)
    user = create(:user, organization: org)
    @controller.stubs(:is_mobile_browser?).returns(true)
    token = user.jwt_token({sent_from_native_app: true})
    random_token = "11111$$$"
    User.any_instance.stubs(:jwt_token).returns(random_token)
    set_host(org)

    get :magic_link_login, token: token,  host_name: org.host_name
    assert_redirected_to "edcastapp://auth/magic_link_login?token=#{random_token}&host_name=#{org.host_name}&org_url=#{org.edcast_host}"
    assert_equal user.id, assigns(:current_user).id
  end

  test 'magic link login for white-labelled mobile app' do
    org = create(:organization)
    user = create(:user, organization: org)
    @controller.stubs(:is_mobile_browser?).returns(true)
    token = user.jwt_token({sent_from_native_app: true})
    random_token = "11111$$$"
    Organization.any_instance.stubs(:mobile_uri_scheme).returns('skillsetedcastapp')
    User.any_instance.stubs(:jwt_token).returns(random_token)
    set_host(org)

    get :magic_link_login, token: token,  host_name: org.host_name
    assert_redirected_to "skillsetedcastapp://auth/magic_link_login?token=#{random_token}&host_name=#{org.host_name}&org_url=#{org.edcast_host}"
    assert_equal user.id, assigns(:current_user).id
  end

  test 'locked user should not be able to login via magic link login ' do
    org = create(:organization)
    user = create(:user, organization: org,locked_at: Time.now)
    @controller.stubs(:is_mobile_browser?).returns(true)
    token = user.jwt_token({sent_from_native_app: true})
    random_token = "11111$$$"
    User.any_instance.stubs(:jwt_token).returns(random_token)
    set_host(org)

    get :magic_link_login, token: token,  host_name: org.host_name
    assert_response :unauthorized

  end

  test 'suspended user should not be able to login via magic link login ' do
    org = create(:organization)
    user = create(:user, organization: org,is_suspended: true,status: 'suspended')
    @controller.stubs(:is_mobile_browser?).returns(true)
    token = user.jwt_token({sent_from_native_app: true})
    random_token = "11111$$$"
    User.any_instance.stubs(:jwt_token).returns(random_token)
    set_host(org)

    get :magic_link_login, token: token,  host_name: org.host_name
    assert_response :unauthorized

  end

  test 'magic link login for browser should set fist sign in cookie' do
    org = create(:organization)
    user = create(:user, organization: org, sign_in_count: 0)
    token = user.jwt_token
    set_host(org)

    get :magic_link_login, token: token,  host_name: org.host_name
    assert_response :redirect
    assert_redirected_to '/'

    assert cookies[:first_sign_in]
  end

  test 'magic link login for browser should not set fist sign in cookie' do
    org = create(:organization)
    user = create(:user, organization: org, sign_in_count: 10)
    token = user.jwt_token
    set_host org

    get :magic_link_login, token: token,  host_name: org.host_name
    assert_response :redirect
    assert_redirected_to '/'

    refute cookies[:first_sign_in]
  end

  test 'magic link login for forgot password page with mobile browser' do
    org = create(:organization)
    set_host(org)
    user = create(:user, organization: org)
    token = user.jwt_token
    random_token = "11111$$$"
    User.any_instance.stubs(:jwt_token).returns(random_token)
    @controller.stubs(:is_mobile_browser?).returns(true)
    get :magic_link_login, token: token, host_name: org.host_name,
      sent_from_forgot_password: true
    assert_redirected_to "edcastapp://auth/magic_link_login?token=#{random_token}&host_name=#{org.host_name}&org_url=#{org.edcast_host}"
    assert_equal user.id, assigns(:current_user).id
  end

  test 'magic link login for forgot password page with white-labelled mobile browser' do
    org = create(:organization)
    set_host(org)
    user = create(:user, organization: org)
    token = user.jwt_token
    random_token = "11111$$$"
    User.any_instance.stubs(:jwt_token).returns(random_token)
    Organization.any_instance.stubs(:mobile_uri_scheme).returns('skillsetedcastapp')
    @controller.stubs(:is_mobile_browser?).returns(true)
    get :magic_link_login, token: token, host_name: org.host_name,
      sent_from_forgot_password: true
    assert_redirected_to "skillsetedcastapp://auth/magic_link_login?token=#{random_token}&host_name=#{org.host_name}&org_url=#{org.edcast_host}"
    assert_equal user.id, assigns(:current_user).id
  end

  test '#magic_link_login to the users having `sign_out_at` set' do
    org = create(:organization)
    set_host(org)
    user = create(:user, organization: org, sign_out_at: Time.now)
    token = user.jwt_token
    random_token = "11111$$$"
    User.any_instance.stubs(:jwt_token).returns(random_token)
    @controller.stubs(:is_mobile_browser?).returns(true)
    get :magic_link_login, token: token, host_name: org.host_name

    assert_response :redirect
    assert_nil user.reload.sign_out_at
  end

  # recommend-users
  test "should include SMEs and influencers" do
    Organization.any_instance.unstub(:create_default_roles)
    org = create :organization
    org.send :create_default_roles

    user = create :user, organization: org
    sme = org.sme_role
    influencer = create :role, organization: org, name: 'influencer'

    member = create :user, organization: org, is_complete: false

    sme1,sme2,influencer1,influencer2  = create_list :user, 4, organization: org
    [sme1, sme2].each {|s| sme.add_user(s)}
    [influencer1, influencer2].each {|i| influencer.add_user(i)}

    User.any_instance.stubs(:recommended_users).
        returns([sme1, sme2, influencer1, influencer2])

    api_v2_sign_in user
    get :recommended_users, format: :json
    resp = get_json_response(response)

    assert_same_elements resp["users"].map{|a| a["id"]}.uniq, [sme1.id, sme2.id, influencer1.id, influencer2.id]
    assert_not_includes resp["users"].map{|a| a["id"]}, member.id
  end

  # if only_sme_on_people_carousel is enabled then only SMEs who are not followed to be returned
  test "should return only not followed SMEs if only_sme_on_people_carousel is true" do
    Organization.any_instance.unstub(:create_default_roles)
    org = create :organization
    only_sme = create(:config, name: 'enable_only_sme_on_people_carousel', value: true, configable: org)

    user = create :user, organization: org
    sme = org.sme_role
    influencer = create :role, organization: org, name: 'influencer'

    member = create :user, organization: org, is_complete: false

    sme1,sme2, sme3, sme4, influencer1  = create_list :user, 5, organization: org
    [sme1, sme2, sme3, sme4].each {|s| sme.add_user(s)}

    influencer.add_user(influencer1)

    create(:follow, user_id: user.id, followable: sme3)

    User.any_instance.stubs(:recommended_users).
        returns([sme1, sme2, sme4])

    api_v2_sign_in user
    get :recommended_users, format: :json
    resp = get_json_response(response)
    assert_same_elements resp["users"].map{|a| a["id"]}, [sme1.id, sme2.id, sme4.id]
    assert_not_includes resp["users"].map{|a| a["id"]}, user.id
    assert_not_includes resp["users"].map{|a| a["id"]}, sme3.id
    assert_not_includes resp["users"].map{|a| a["id"]}, influencer1.id
  end

  # if only_sme_on_people_carousel is enabled
  test "should return only those sme's who are team members of current_user if params my_teams_users is true  " do
    Organization.any_instance.unstub(:create_default_roles)
    org = create :organization
    only_sme = create(:config, name: 'enable_only_sme_on_people_carousel', value: true, configable: org)

    user = create :user, organization: org
    sme = org.sme_role
    sme1, sme2, sme3, influencer  = create_list :user, 5, organization: org
    [sme1, sme2, sme3].each {|s| sme.add_user(s)}

    create(:follow, user_id: user.id, followable: sme3)

    team1, team2 = create_list :team, 2, organization: org

    #add current user to each team
    [team1, team2].each {|team| team.add_admin user}

    #add sme to current_users's team
    team1.add_member sme1

    team1_users = create_list(:user, 2, organization: org)
    team2_users = create_list(:user, 2, organization: org)

    team1_users.each do |user|
      team1.add_member(user)
    end

    team2_users.each do |user|
      team2.add_member(user)
    end

    User.any_instance.stubs(:recommended_users).
      returns([sme1, sme2, sme3])

    api_v2_sign_in user
    get :recommended_users, limit: 50, my_teams_users: 'true', format: :json
    resp = get_json_response(response)

    assert_same_elements resp["users"].map{|a| a["id"]}, [sme1.id]
    assert_not_includes resp["users"].map{|a| a["id"]}, user.id
    assert_not_includes resp["users"].map{|a| a["id"]}, sme3.id
    assert_not_includes resp["users"].map{|a| a["id"]}, influencer.id
  end

  # if only_sme_on_people_carousel is enabled
  test "should return uniq smes in recommendation api response if params my_teams_users is true " do
    Organization.any_instance.unstub(:create_default_roles)
    org = create :organization
    only_sme = create(:config, name: 'enable_only_sme_on_people_carousel', value: true, configable: org)

    user = create :user, organization: org
    sme = org.sme_role
    sme1, sme2, sme3, influencer  = create_list :user, 5, organization: org
    [sme1, sme2, sme3].each {|s| sme.add_user(s)}

    create(:follow, user_id: user.id, followable: sme3)

    team1, team2 = create_list :team, 2, organization: org

    [team1, team2].each do |team|
      team.add_admin  user
      team.add_member sme1
      team.add_member sme2
    end

    User.any_instance.stubs(:recommended_users).
      returns([sme1, sme2, sme3])

    api_v2_sign_in user
    get :recommended_users, limit: 50, my_teams_users: 'true', format: :json
    resp = get_json_response(response)
    assert_equal 2, resp["users"].count
    assert_same_elements resp["users"].map{|a| a["id"]}, [sme1.id, sme2.id]
    assert_not_includes resp["users"].map{|a| a["id"]}, user.id
  end

  test 'should return matching expert first then followed by other users' do
    org = create(:organization)
    create_org_master_roles org
    sme = org.roles.where(name: 'sme').first

    member_user = create(:user, organization: org)
    create(:user_profile, user: member_user, learning_topics: Taxonomies.domain_topics)

    expert = create(:user, organization: org)

    normal_sme = create(:user, organization: org)
    sme.add_user(normal_sme)

    skill_experts = create_list(:user, 4, organization: org)

    skill_experts << expert
    skill_experts.each {|u| create(:user_profile, user: u, expert_topics: Taxonomies.domain_topics.first(2)) }

    following_users = create_list(:user, 5, organization: org)
    following_users.each do |u|
      create(:user_profile, user: u, learning_topics: Taxonomies.domain_topics.first(2))
      create :follow, user: member_user, followable: u
    end

    other_user = create(:user, organization: org)
    create(:user_profile, user: other_user, expert_topics: Taxonomies.domain_topics.first(1))

    Search::UserSearch.any_instance.expects(:search).
        with(q: nil, viewer: member_user, filter: {search_expertise: true, role: "sme"}, allow_blank_q: true, limit: User::MAX_SEARCH, load: false, sort: :score).
        returns(Search::UserSearchResponse.new(results: skill_experts, total: 2)).once
    Search::UserSearch.any_instance.expects(:search).
        with(q: nil, viewer: member_user, filter: {search_peer_learners: true}, allow_blank_q: true, limit: User::MAX_SEARCH, sort: :score, load: false).
        returns(Search::UserSearchResponse.new(results: [], total: 2)).once
    Search::UserSearch.any_instance.expects(:search).
        with(q: nil, viewer: member_user, filter: {search_expertise: true, following: true, role: "sme"}, allow_blank_q: true, limit: User::MAX_SEARCH, load: false, sort: :score).
        returns(Search::UserSearchResponse.new(results: following_users, total: 2)).once

    api_v2_sign_in member_user

    get :recommended_users, limit: 50, format: :json
    resp = get_json_response(response)
    assert_same_elements (skill_experts.sort+[normal_sme]+following_users).map(&:id), resp['users'].map {|u| u['id']}

    #normal SME should not be last
    assert_not_equal normal_sme.id, resp['users'].last["id"]
  end

  test 'saving of notification config' do
    # Bootstrap
    skip
    org = create(:organization)
    user = create(:user, organization: org)
    create(:notification_config, user: user, organization: org)

    api_v2_sign_in user

    # Get the initial config
    get :get_notification_config

    resp = get_json_response(response)

    # Assert that realtime is enabled for new_smartbite_comment
    assert resp['user_config']['options']['new_smartbite_comment']['email'][1]['id'] = 'single'
    assert resp['user_config']['options']['new_smartbite_comment']['email'][1]['selected'] = true

    # Now, update it as the frontend would
    resp['user_config']['options']['new_smartbite_comment']['email'][1]['selected'] = false
    resp['user_config']['options']['new_smartbite_comment']['email'][2]['selected'] = true

    # Update the config
    put :update_notification_config, {options: resp['user_config']['options']}

    # Get the config again
    get :get_notification_config
    resp = get_json_response(response)

    # Assert that daily is enabled for new_smartbite_comment
    assert resp['user_config']['options']['new_smartbite_comment']['email'][2]['id'] = 'digest_daily'
    assert resp['user_config']['options']['new_smartbite_comment']['email'][2]['selected'] = true
  end

  test "get user's pubnub channels" do
    Search::UserSearch.any_instance.stubs(:search_by_id).returns(nil)

    org = create(:organization)
    user = create(:user, organization: org)

    api_v2_sign_in user

    get :pubnub_channels, format: :json
    resp = get_json_response(response)
    assert_equal resp["pubnub_channels"].length, 3
    assert_equal [user.private_pubnub_channel_name, Settings.pubnub.global_channel_name, user.organization.public_pubnub_channel_name], resp["pubnub_channels"]
  end

  test ':api should return user profile information' do
    org = create(:organization)
    user = create(:user, organization: org)
    user_profile = create(:user_profile, user: user, learning_topics: Taxonomies.domain_topics, expert_topics: Taxonomies.domain_topics)

    api_v2_sign_in user

    get :user_profile, format: :json

    assert_response :ok
    resp = get_json_response response

    valid_keys = ['learning_topics', 'expert_topics', 'language', 'tac_accepted', 'time_zone', 'company', 'job_role']
    resp.assert_valid_keys(valid_keys)

    assert_not_empty resp['learning_topics']
    assert_not_empty resp['expert_topics']
  end

  test ':api should return users public profile information' do
    org = create(:organization)
    create(:config, configable_id: org.id, configable_type: 'Organization', data_type: 'boolean', name: 'public_profile_enabled', value: "true")
    user = create(:user, organization: org)
    create_org_master_roles org
    user.add_role(Role::TYPE_MEMBER)
    user_profile = create(:user_profile, user: user, learning_topics: Taxonomies.domain_topics, expert_topics: Taxonomies.domain_topics)

    set_host(org)

    card_badge = create(:card_badge, organization: org)
    card = create(:card, is_public: true, author_id: user.id, card_badging_attributes: {title: 'badge title', badge_id: card_badge.id})
    create(:user_badge, badging_id: card.card_badging.id, user_id: user.id)

    clc_badge = create(:clc_badge, organization: org)
    clc = create(:clc, name: 'test clc', entity: org, organization: org, clc_badgings_attributes: [{badge_id: clc_badge.id, title: "clc-25"}])
    create(:user_badge, badging_id: clc.clc_badgings.first.id, user_id: user.id)

    courses = create_list(:users_external_course, 2, user: user)
    user.integrations << Integration.all

    skills = create_list(:skill, 2)
    user.skills << skills

    get :public_profile, handle: user.handle, format: :json

    assert_response :ok
    resp = get_json_response response

    assert_not_empty resp
    assert_not_empty resp['profile']
    assert_equal user.id, resp['profile']['id']
    assert_equal user.email, resp['profile']['email']
    assert_equal user.first_name, resp['profile']['first_name']
    assert_equal user.name, resp['profile']['name']
    assert_equal user.bio, resp['profile']['bio']
    assert_equal user.fetch_coverimage_urls, resp['profile']['coverimages']
    assert_equal user.roles.map { |role| role.name }, resp['profile']['roles']

    assert_not_empty resp['user_card_badges']
    assert_equal card.card_badging.id, resp['user_card_badges'].first['id']
    assert_equal card.card_badging.title, resp['user_card_badges'].first['title']

    assert_not_empty resp['user_clc_badges']
    assert_equal clc.clc_badgings.first.id, resp['user_clc_badges'].first['id']
    assert_equal clc.clc_badgings.first.title, resp['user_clc_badges'].first['title']

    assert_not_empty resp['external_courses']
    assert_equal courses.map {|course| course.id}, resp['external_courses'].map { |course| course['id'] }

    assert_not_empty resp['skills']
    assert_equal user.skills_user_ids, resp['skills'].map { |skill| skill['id'] }
    assert_equal user.skills.pluck(:name), resp['skills'].map { |skill| skill['name'] }
    assert_equal user.skills.pluck(:expiry_date), resp['skills'].map { |skill| skill['expiry_date'] }

    assert_includes resp["profile"].keys, "dashboard_info"
  end

  test 'should return 401 if public page is disabled' do
    org = create(:organization)
    set_host org
    user = create(:user, organization: org)
    get :public_profile, handle: user.handle, format: :json
    assert_response :unauthorized
  end

  test 'should allow if user is logged in' do
    org = create(:organization)
    user = create(:user, organization: org)
    api_v2_sign_in user
    get :public_profile, handle: user.handle, format: :json
    assert_response :ok
  end

  test 'should get is_following if user is logged in and follow user' do
    org = create(:organization)
    user = create(:user, organization: org)
    api_v2_sign_in user
    follow = create(:user, organization: org)
    Follow.create(followable: follow, user_id: user.id)
    get :public_profile, handle: follow.handle, format: :json
    assert_response :ok
    resp = get_json_response response

    assert_not_empty resp

    assert_not_empty resp['profile']
    assert_equal follow.id, resp['profile']['id']
    assert_equal follow.first_name, resp['profile']['first_name']
    assert_equal follow.bio, resp['profile']['bio']
    assert_equal true, resp['profile']['is_following']
  end

  test 'should get is_following if user is logged in and not follow user' do
    org = create(:organization)
    user = create(:user, organization: org)
    api_v2_sign_in user
    follow = create(:user, organization: org)
    get :public_profile, handle: follow.handle, format: :json
    assert_response :ok
    resp = get_json_response response

    assert_not_empty resp

    assert_not_empty resp['profile']
    assert_equal follow.id, resp['profile']['id']
    assert_equal follow.first_name, resp['profile']['first_name']
    assert_equal follow.bio, resp['profile']['bio']
    assert_equal false, resp['profile']['is_following']
  end

  test 'should return skill level' do
    org = create(:organization)
    user = create(:user, organization: org)
    skills = create_list(:skill, 2)
    user.skills.push(skills)

    api_v2_sign_in user

    get :public_profile, handle: user.handle, format: :json

    resp = get_json_response response
    skill = resp['skills'][0]

    assert_response :ok
    assert_includes skill, 'skill_level'
  end

  test 'should return job_title field' do
    org = create(:organization)
    user = create(:user, organization: org)

    api_v2_sign_in user

    get :public_profile, handle: user.handle, format: :json

    resp = get_json_response(response)

    assert_response :ok
    assert resp['profile'].key?('job_title')
  end

  test ':api returns filestack secured skills image when user visits another users profile' do
    org = create(:organization)
    user = create(:user, organization: org)
    skill = create(:skill)
    create(:skills_user,
      user: user,
      skill: skill,
      credential: [{
        "mimetype"=>"image/png",
        "size"=>44887,
        "source"=>"local_file_system",
        "status"=>"Stored",
        "url"=>"https://cdn.filestackcontent.com/oaXtaDBRrO0oPhOjMyfN",
        "handle"=>"oaXtaDBRrO0oPhOjMyfN"
      }]
    )
    other_user = create(:user, organization: org)
    api_v2_sign_in other_user
    get :public_profile, handle: user.handle, format: :json
    resp = get_json_response(response)

    assert_response :ok
    assert_match /security=p:[a-zA-Z0-9=]+,s:[a-zA-Z0-9=]+\/oaXtaDBRrO0oPhOjMyfN/, resp["skills"][0]["credential"][0]["url"]
  end

  test 'get dev api token' do
    admin = create(:user, organization_role: "admin")
    credential = create(:developer_api_credential, organization: admin.organization)

    api_v2_sign_in admin
    get :dev_api_token, api_key: credential.api_key, format: :json
    json_response = get_json_response(response)
    assert_not_nil json_response['dev_api_token']
  end

  test 'should return all the projects submitted by a user' do
    org = create(:organization)
    user = create(:user, organization: org)

    api_v2_sign_in user

    author = create(:user, organization: org)
    card = create(:card, organization: org,
                  author: user,
                  card_type: 'project',
                  card_subtype: 'text',
                  project_attributes: {reviewer_id: user.id})

    get :projects, format: :json

    resp = get_json_response(response)

    assert_not_empty resp["projects"]
    assert_equal card.project.id, resp["projects"][0]["project_id"]
  end

  test ':api should return skillcoin wallet balance for a user.' do
    org = create(:organization)
    user = create(:user, organization: org)

    api_v2_sign_in(user)
    create(:wallet_balance, user: user)

    get :skillcoin, format: :json
    resp = get_json_response(response)

    assert_equal user.wallet_balance.amount.round, resp["balance"]
  end

  test ':api should return all transactions for a user based on limit' do
    Transaction.any_instance.stubs(:create_transaction_audit).returns(true)
    org = create(:organization)
    user = create(:user, organization: org)

    api_v2_sign_in(user)
    card = create(:card)
    order = create(:order, orderable: card)
    first_transaction = create(:transaction, user: user, payment_state: 1, order: order)
    second_transaction = create(:transaction, user: user, payment_state: 2)
    last_transaction = create(:transaction, user: user, payment_state: 2)

    get :transactions, limit: 2, format: :json
    resp = get_json_response(response)

    assert_not_empty resp["transactions"]
    assert_equal 2, resp["transactions"].count
    assert_same_elements [last_transaction, second_transaction].map{|wt| wt.amount.to_f},
                         resp["transactions"].map{ |wt| wt['price_details']['amount'].to_f }
  end

  test 'should return all transactions for a user in descending order' do
    Transaction.any_instance.stubs(:create_transaction_audit).returns(true)
    org = create(:organization)
    user = create(:user, organization: org)

    api_v2_sign_in(user)
    card = create(:card)
    order = create(:order, orderable: card)
    first_transaction = create(:transaction, user: user, amount: 3000.0, created_at: Time.now - 1.day, payment_state: 1, order: order)
    last_transaction = create(:transaction, user: user, amount: 1000.0, created_at: Time.now, payment_state: 2)

    get :transactions, format: :json
    resp = get_json_response(response)

    assert_same_elements [last_transaction, first_transaction].map{|wt| wt.amount.to_f},
                         resp["transactions"].map{ |wt| wt['price_details']['amount'].to_f }
  end

  test 'should return correct count of all transactions for the user' do
    Transaction.any_instance.stubs(:create_transaction_audit).returns(true)
    org = create(:organization)
    user = create(:user, organization: org)

    api_v2_sign_in(user)
    card = create(:card)
    order = create(:order, orderable: card)
    create(:transaction, user: user, payment_state: 2)
    create(:transaction, user: user, payment_state: 2)
    create(:transaction, user: user, payment_state: 0)
    create(:transaction, user: user, payment_state: 1, order: order)

    get :transactions, limit: 2, format: :json
    resp = get_json_response(response)

    assert_equal 3, resp['transactions_count']
  end

  test 'should return details of transactions of card purchased using skillcoins' do
    Transaction.any_instance.stubs(:create_transaction_audit).returns(true)
    amount_in_skillcoins = BigDecimal.new("150")

    org = create(:organization)
    user = create(:user, organization: org)
    card = create(:card)
    order = create(:order, orderable: card)
    create(:transaction, user: user, payment_state: 1, order: order, gateway: 'wallet')
    create(:wallet_transaction, user: user, order: order, amount: amount_in_skillcoins)

    api_v2_sign_in(user)

    get :transactions, format: :json
    resp = get_json_response(response)

    assert_equal amount_in_skillcoins, resp["transactions"][0]['skillcoins'].to_f
  end

  test 'should not return skillcoin amount for failed debit or credit card transactions' do
    Transaction.any_instance.stubs(:create_transaction_audit).returns(true)
    amount_in_skillcoins = BigDecimal.new("20")
    amount_in_rupees = BigDecimal.new("150")

    org = create(:organization)
    user = create(:user, organization: org)
    card = create(:card)
    order = create(:order, orderable: card)

    create(:transaction, user: user, payment_state: 2, order: order, gateway: 'braintree',
                            amount: amount_in_rupees, currency: 'INR')
    create(:transaction, user: user, payment_state: 1, order: order, gateway: 'wallet',
                            amount: amount_in_skillcoins, currency: 'SKILLCOIN')
    create(:wallet_transaction, user: user, payment_state: 1, order: order, payment_type: 1,
                                  amount: amount_in_skillcoins)

    api_v2_sign_in(user)

    get :transactions, format: :json
    resp = get_json_response(response)

    # Successful wallet transaction info returns skillcoin amount but not amount in rupees/dollars
    assert_equal amount_in_skillcoins, resp["transactions"][1]['skillcoins'].to_f
    assert_nil resp["transactions"][1]['price_details']

    # Failed braintree transaction info returns amount in rupees/dollars but not skillcoins
    assert_nil resp["transactions"][0]['skillcoins']
    assert_equal amount_in_rupees, resp["transactions"][0]['price_details']['amount'].to_f
  end

  test 'should return both skillcoins and amount for wallet recharge transaction' do
    Transaction.any_instance.stubs(:create_transaction_audit).returns(true)
    amount_in_skillcoins = BigDecimal.new("20")
    amount_in_rupees = BigDecimal.new("150")

    org = create(:organization)
    user = create(:user, organization: org)
    recharge = create(:recharge, skillcoin: amount_in_skillcoins)
    recharge_prices = create(:recharge_price, amount: amount_in_rupees, currency: 'INR')
    order = create(:order, orderable: recharge)
    create(:transaction, user: user, payment_state: 1, order: order, gateway: 'braintree',
                            amount: amount_in_rupees, currency: 'INR')

    api_v2_sign_in(user)

    get :transactions, format: :json
    resp = get_json_response(response)

    assert_equal amount_in_skillcoins, resp["transactions"][0]['skillcoins'].to_f
    assert_equal amount_in_rupees, resp["transactions"][0]['price_details']['amount'].to_f
  end

  test 'should return nil for invalid api key' do
    admin = create(:user, organization_role: "admin")
    credential = create(:developer_api_credential, organization: admin.organization)

    api_v2_sign_in admin
    get :dev_api_token, api_key: "sdscdscdsfds", format: :json
    json_response = get_json_response(response)
    assert_nil json_response['dev_api_token']
  end

  test "should not return dev_api_token for men]mers" do
    member = create(:user, organization_role: "member")
    credential = create(:developer_api_credential, organization: member.organization)

    api_v2_sign_in member
    get :dev_api_token, api_key: credential.api_key, format: :json
    json_response = get_json_response(response)
    assert_nil json_response['dev_api_token']
  end

  test ':api should return user groups' do
    org = create(:organization)
    banned_group, not_banned_group = create_list(:resource_group, 2, organization: org)
    user = create(:user)
    [banned_group, not_banned_group].each  {|group| group.members << user}
    banned_group.groups_users.where(user: user).update_all banned: true

    api_v2_sign_in user

    get :enrolled_courses, format: :json
    resp = get_json_response(response)

    assert_equal 1, resp['courses'].size
    assert_equal not_banned_group.id, resp['courses'][0]['id']
    resp['courses'][0].assert_valid_keys(['id', 'name', 'savannah_id'])
  end

  test '#wallet_api_token returns JWT token with minimal user details' do
    user = create :user

    api_v2_sign_in user
    get :wallet_api_token, format: :json
    json_response = get_json_response(response)
    assert_not_nil json_response['wallet_api_token']
  end

  test 'API v2 should not send response headers X_API_TOKEN and EXPIRE_AT if request have not header REFRESH-TOKEN' do
    user =  create(:user)
    create(:config, configable_id: user.organization.id, configable_type: 'Organization', data_type: 'integer', name: 'web_session_timeout', value: 1)
    token = user.jwt_token({is_web: true})
    request.headers['X-API-TOKEN'] = token

    get :index, format: :json
    assert_not response.headers['X_API_TOKEN']
    assert_not response.headers['EXPIRE_AT']
  end

  test 'API v2 should send response headers X_API_TOKEN and EXPIRE_AT if request have header REFRESH-TOKEN' do
    user =  create(:user)
    create(:config, configable_id: user.organization.id, configable_type: 'Organization', data_type: 'integer', name: 'web_session_timeout', value: 1)
    token = user.jwt_token({is_web: true})
    request.headers['X-API-TOKEN'] = token
    request.headers['REFRESH-TOKEN'] = true

    get :index, format: :json
    assert response.headers['X_API_TOKEN']
    assert response.headers['EXPIRE_AT']
  end

  test 'API v2 should not send response headers X_API_TOKEN and EXPIRE_AT if request from mobile' do
    user =  create(:user)
    create(:config, configable_id: user.organization.id, configable_type: 'Organization', data_type: 'integer', name: 'web_session_timeout', value: 1)
    token = user.jwt_token({is_web: true})
    request.headers['X-API-TOKEN'] = token
    request.headers['REFRESH-TOKEN'] = true
    request.headers['User-Agent'] = 'EdCast/1.0 (iPhone; iOS 8.1.1; Scale/2.00)'

    get :index, format: :json
    assert_not response.headers['X_API_TOKEN']
    assert_not response.headers['EXPIRE_AT']
  end

  test "Should return unautorized request from other org" do
    org1 = create(:organization)
    org2 = create(:organization)
    user1 = create(:user, organization: org1)
    user2 = create(:user, organization: org2)

    token1 = user1.jwt_token
    token2 = user2.jwt_token

    request.headers['X-API-TOKEN'] = token1
    request.headers['User-Agent'] = 'EdCast/1.0 (iPhone; iOS 8.1.1; Scale/2.00)'

    set_host(org2)
    get :index, format: :json
    assert_response :unauthorized
  end

  test "Should return sucesss if token and request belong to same org" do
    org1 = create(:organization)
    user1 = create(:user, organization: org1)

    token1 = user1.jwt_token

    request.headers['X-API-TOKEN'] = token1
    request.headers['User-Agent'] = 'EdCast/1.0 (iPhone; iOS 8.1.1; Scale/2.00)'

    set_host(org1)
    get :index, format: :json
    assert_equal 200, response.status
  end

  test ':api should get badges won by user in an org' do
    org = create(:organization)
    user = create(:user, organization: org)
    api_v2_sign_in(user)

    card_badge = create(:card_badge, organization: org)
    card = create(:card, is_public: true, author_id: user.id, card_badging_attributes: {title: 'badge title', badge_id: card_badge.id})
    user_badge = create(:user_badge, badging_id: card.card_badging.id, user_id: user.id)

    get :badges, id: user.id, type: 'CardBadge', format: :json

    resp = get_json_response(response)
    assert_response :success
    assert_not_nil resp['user_badges'].first['id']
    assert_equal card.card_badging.id, resp['user_badges'].first['id']
    assert_equal user.user_badges.count, resp["total_count"]
    assert_not_nil resp["user_badges"][0]["card"]
    assert_equal card.message, resp["user_badges"][0]["card"]["message"]
  end

  test ':api should get badges won by particular user ' do
    org = create(:organization)
    user = create(:user, organization: org)
    api_v2_sign_in(user)

    card_badge = create(:card_badge, organization: org)
    card = create(:card, is_public: true, author_id: user.id, card_badging_attributes: {title: 'badge title', badge_id: card_badge.id})
    user_badge = create(:user_badge, badging_id: card.card_badging.id, user_id: user.id)

    get :badges, id: user.id, type: 'CardBadge', format: :json

    resp = get_json_response(response)
    assert_response :success
    assert_not_nil resp['user_badges'].first['id']
    assert_equal card.card_badging.id, resp['user_badges'].first['id']
    assert_equal user.user_badges.count, resp["total_count"]
    assert_not_nil resp["user_badges"][0]["card"]
    assert_equal card.message, resp["user_badges"][0]["card"]["message"]
  end

  test ':api should get badges won by user in an org even if the card is deleted' do
    org = create(:organization)
    user = create(:user, organization: org)
    api_v2_sign_in(user)

    card_badge = create(:card_badge, organization: org)
    card = create(:card, is_public: true, author_id: user.id, card_badging_attributes: {title: 'badge title', badge_id: card_badge.id})
    user_badge = create(:user_badge, badging_id: card.card_badging.id, user_id: user.id)

    card.destroy
    get :badges, id: user.id, type: 'CardBadge', format: :json

    resp = get_json_response(response)
    assert_response :success
    assert_not_nil resp['user_badges'].first['id']
    assert_equal card.card_badging.id, resp['user_badges'].first['id']
    assert_equal card.id, resp['user_badges'].first['card']['id']
    assert_equal user.user_badges.count, resp["total_count"]
    assert_not_nil resp["user_badges"][0]["card"]
    assert_equal card.message, resp["user_badges"][0]["card"]["message"]
  end

  test 'should pass identifier of badges won by user in an org' do
    org = create(:organization)
    user = create(:user, organization: org)
    api_v2_sign_in(user)

    card_badge = create(:card_badge, organization: org)
    card = create(:card, is_public: true, author_id: user.id, card_badging_attributes: {title: 'badge title', badge_id: card_badge.id})
    user_badge = create(:user_badge, badging_id: card.card_badging.id, user_id: user.id)

    get :badges, id: user.id, type: 'CardBadge', format: :json

    resp = get_json_response(response)
    assert_response :success
    assert_not_nil resp['user_badges'].first['id']
    assert_not_nil resp['user_badges'].first['identifier']
  end

  test ':api to fetch users from the org with pending invitations list' do
    skip # this is very brittle test and failing intermittently
         # pls get object that has pending invitation and assert invitable
         # with team
    org = create(:organization)
    admin = create(:user, organization: org)
    create_org_master_roles org
    admin.add_role(Role::TYPE_ADMIN)
    api_v2_sign_in admin
    user1 = create(:user, organization: org)
    team = create(:team, organization: org)
    invite = create(:invitation, sender_id: admin.id,
                      recipient_email: user1.email,
                      recipient_id: user1.id,
                      invitable_id: team.id,
                      roles: 'member',
                      invitable_type: 'Team')
    get :index, format: :json
    res = get_json_response(response)
    assert_response :ok
    assert_equal 2, res['total']
    # response should contains pending invitation array
    res['users'].each do |user|
      if user['id'] == user1.id
        assert_equal 1, user['pending_team_invitations'].count
      elsif user['id']== admin.id
        assert_equal 0, user['pending_team_invitations'].count
      end
    end
  end

  test 'api should not include anonymized user' do
    org = create(:organization)
    org_active_user = create(:user, organization: org)
    org_suspended_users = create_list(:user, 2, organization: org, first_name: Faker::Name.name, is_suspended: true, status: 'suspended')
    org_anonymized_users = create_list(:user, 2, organization: org, first_name: Faker::Name.name, is_suspended: true, is_anonymized: true, status: 'suspended')
    org_viewer = org_active_user

    api_v2_sign_in org_viewer

    get :index, status: 'suspended', format: :json

    resp = get_json_response response
    assert_response :ok
    assert_equal 2, resp['users'].count
    assert_same_elements org_suspended_users.map(&:id), resp['users'].map {|u| u['id']}
    refute org_anonymized_users.map(&:id).include? resp['users'].map {|u| u['id']}
  end

  test 'should return active users' do
    org = create(:organization)
    active_user = create(:user, organization: org)
    suspended_user = create(:user, organization: org, is_suspended: true, status: 'suspended')
    fake_user = create(:user, organization: org, is_suspended: true, status: 'active')

    api_v2_sign_in active_user

    get :index, status: 'active', format: :json

    resp = get_json_response response
    assert_response :ok
    assert_equal 1, resp['users'].count
    assert_equal [active_user.id], resp['users'].map {|u| u['id']}
  end

  test 'should return suspended users' do
    org = create(:organization)
    active_user = create(:user, organization: org)
    suspended_user = create(:user, organization: org, is_suspended: true, status: 'suspended')
    fake_user = create(:user, organization: org, is_suspended: false, status: 'suspended')

    api_v2_sign_in active_user

    get :index, status: 'suspended', format: :json

    resp = get_json_response response
    assert_response :ok
    assert_equal 1, resp['users'].count
    assert_equal [suspended_user.id], resp['users'].map {|u| u['id']}
  end

  test 'should return inactive users' do
    org = create(:organization)
    active_user = create(:user, organization: org)
    suspended_user = create(:user, organization: org, is_suspended: false, status: 'inactive')
    fake_user = create(:user, organization: org, is_suspended: true, status: 'inactive')

    api_v2_sign_in active_user

    get :index, status: 'inactive', format: :json

    resp = get_json_response response
    assert_response :ok
    assert_equal 1, resp['users'].count
    assert_equal [suspended_user.id], resp['users'].map {|u| u['id']}
  end

  class UserIndexForGroupSubAdmin < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      @users_list = create_list(:user, 4, organization: @org)
      @team = create(:team, organization: @org)

      role = create(:role, name: 'group_admin', default_name: 'group_admin', organization: @org)
      create(:role_permission, role_id: role.id, name: Permissions::MANAGE_GROUP_USERS)

      @team.add_sub_admin(@user)
      @team.add_member(@users_list[0])
      @team.add_member(@users_list[1])
    end

    test 'group_sub_admin users should see only users from their group when is_cms is true' do
      api_v2_sign_in @user

      get :index, is_cms: true, format: :json

      resp = get_json_response(response)
      assert_equal 2, resp.count
      assert_same_elements [@user.id, @users_list[0].id, @users_list[1].id], resp['users'].map{|d| d['id'].to_i}
      assert_not_includes  resp['users'].map{|d| d['id'].to_i}, @users_list[2].id
      assert_not_includes  resp['users'].map{|d| d['id'].to_i}, @users_list[3].id
    end

    test 'group_sub_admin users should see all org users when is_cms is not passed' do
      api_v2_sign_in @user

      get :index, format: :json

      resp = get_json_response(response)
      assert_same_elements [@users_list.map(&:id), @user.id].flatten, resp['users'].map{|d| d['id'].to_i}
      assert_equal 5, resp['users'].count
    end

    test 'should only fetch users of the selected team if team_ids is passed' do
      team = create(:team, organization: @org)
      user = create(:user, organization: @org)
      team.add_member(user)
      team.add_sub_admin(@user)

      api_v2_sign_in @user
      get :index, team_ids: [team.id], is_cms: true, format: :json

      resp = get_json_response(response)
      assert_same_elements [@user.id, user.id], resp['users'].map{|d| d['id']}
      assert_not_includes resp['users'].map{|d| d['id']}, @users_list[0].id
    end

  end

  #### START verify_badge
   class VerifyBadge < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      stub_okta_user_update
      @card_badge = create(:card_badge, organization: @org)
      @card = create(:card, is_public: true, author_id: @user.id, card_badging_attributes: {title: 'badge title', badge_id: @card_badge.id})
      @user_badge = create(:user_badge, badging_id: @card.card_badging.id, user_id: @user.id)
    end

    test "verify_badge should be a public action" do
      get :verify_badge, identifier: @user_badge.identifier, format: :json
      resp = get_json_response(response)
      assert_response :success
    end

    test 'should verify user badge by identifier of badges and return some public info' do
      get :verify_badge, identifier: @user_badge.identifier, format: :json

      resp = get_json_response(response)
      assert_response :success
      assert_equal ["organization", "user", "badge"], resp.keys
      assert_equal resp["organization"].keys, ["name", "image_url", "banner_url", "co_branding_logo"]
      assert_equal resp["badge"].keys, ["title", "image_url", "date"]
    end

    test 'should fail if identifier is wrong' do
      get :verify_badge, identifier: "test", format: :json

      resp = get_json_response(response)
      assert_response :not_found
    end

    test "should return bad request if no identifier" do
      get :verify_badge, identifier: "", format: :json

      resp = get_json_response(response)
      assert_response :bad_request
    end
  end

  #### END verify_badge
  test ':api should get assignments assigned by user only' do
    org = create(:organization)
    team = create(:team, organization: org)
    user1, user2, user3 = create_list(:user, 3, organization: org)
    assignments_ids_by_user1 = []

    assignments = create_list(:assignment, 4, assignee: user3)

    assignments.each_with_index do |assignment, indx|
        create(:team_assignment, team: team, assignment: assignment, assignor: indx < 2 ? user1 : user2)

        if indx < 2
            assignments_ids_by_user1 << assignment.id
        end
    end
    api_v2_sign_in(user3)
    get :assignments_by_assignor, assignor_id: user1.id, limit: 5, format: :json

    resp = get_json_response(response)
    assert_equal 2, resp['assignment_count']
    assert_same_elements assignments_ids_by_user1, resp['assignments'].map {|a| a['id']}
  end

  test 'pagination test for getting assignments assigned by user only' do
    org = create(:organization)
    team = create(:team, organization: org)
    user1, user2, user3 = create_list(:user, 3, organization: org)
    assignments_ids_by_user1 = []

    assignments = create_list(:assignment, 4, assignee: user3)

    assignments.each_with_index do |assignment, indx|
        create(:team_assignment, team: team, assignment: assignment, assignor: indx < 3 ? user1 : user2)

        if indx < 3
            assignments_ids_by_user1 << assignment.id
        end
    end
    api_v2_sign_in(user3)
    get :assignments_by_assignor, assignor_id: user1.id, limit: 1, offset: 2, format: :json
    resp = get_json_response(response)
    assert_equal 3, resp['assignment_count']
    assert_equal 1, resp['assignments'].length
  end

  test ':api should return users fields' do
    org = create(:organization)
    user = create(:user, organization: org)
    create(:custom_field, organization: org, display_name: 'city')
    cf = create(:custom_field, organization: org, display_name: 'country')
    cf.set_value_for_config('enable_in_filters', true)
    api_v2_sign_in(user)

    get :users_fields, format: :json
    resp = get_json_response(response)

    fields = %w[country email first_name last_name handle]
    assert_equal fields, resp['fields']
  end

  test ':api should return values for field users in default_fields' do
    org = create(:organization)
    user = create(:user, organization: org)
    user1 = create(:user, organization: org)
    api_v2_sign_in(user)

    get :field_values, name: 'first_name', format: :json
    resp = get_json_response(response)

    assert_equal [user.first_name, user1.first_name], resp['values']
  end

  test 'should return values for field users in custom fields' do
    org = create(:organization)
    user = create(:user, organization: org)
    user1 = create(:user, organization: org)
    cf = create(:custom_field, organization: org, display_name: 'country')
    cf.set_value_for_config('enable_in_filters', true)
    user_field = create(:user_custom_field, custom_field: cf, user: user, value: 'london')
    user_field1 = create(:user_custom_field, custom_field: cf, user: user1, value: 'moscow')

    api_v2_sign_in(user)

    get :field_values, name: 'country', format: :json
    resp = get_json_response(response)

    assert_equal [user_field.value, user_field1.value], resp['values']
  end

  test 'field_values should return errors if missing required params' do
    org = create(:organization)
    user = create(:user, organization: org)

    api_v2_sign_in(user)

    get :field_values, name: '', format: :json
    resp = get_json_response(response)

    assert_equal 'Missing required params', resp['message']
  end

  test 'field_values should return errors if not valid field name' do
    org = create(:organization)
    user = create(:user, organization: org)

    api_v2_sign_in(user)

    get :field_values, name: 'qwe', format: :json
    resp = get_json_response(response)

    assert_equal 'qwe is not field of users', resp['message']
  end


  test ':api should return values for field users in default_fields with limit, offset and total' do
    org = create(:organization)
    user = create(:user, organization: org)
    user1 = create(:user, organization: org)
    user2 = create(:user, organization: org)
    api_v2_sign_in(user)

    get :field_values, name: 'first_name', limit: 1, offset: 1, format: :json
    resp = get_json_response(response)

    assert_equal [user1.first_name], resp['values']
    assert_equal 3, resp['total']
  end

  class EmailSignupTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      set_host(@org)
      stub_okta_user_update
    end

    test 'email signup with blank password' do
      post :email_signup, user: { email: "xyz@email.com", first_name: "abc", last_name: "xyz" }
      assert_response :unprocessable_entity
      resp = get_json_response(response)
      assert_equal "Password can't be blank", resp['message']
    end

    test 'email signup with invalid password' do
      post :email_signup, user: { email: "xyz@email.com", first_name: "abc", last_name: "xyz", password: "abc123" }
      assert_response :unprocessable_entity
      resp = get_json_response(response)
      assert_equal "Password is too short (minimum is 8 characters) and Password is invalid", resp['message']
    end

    test 'email signup with blank email' do
      post :email_signup, user: { first_name: "abc", last_name: "xyz", password: User.random_password }
      assert_response :unprocessable_entity
      resp = get_json_response(response)
      assert_equal "Email can't be blank", resp['message']
    end

    test 'email signup with users date less than 13' do
      post :email_signup, user: { first_name: "abc", last_name: "xyz", email: "xyz@gmail.com",
       password: User.random_password, profile_attributes: { dob: (Date.today - 3.years).strftime("%m/%d/%Y") }}
      assert_response :unprocessable_entity
      resp = get_json_response(response)
      assert_equal "Users age has to be greater than or equal to 13", resp['message']
    end

    test 'signup with unregistrable email' do
      Organization.any_instance.expects(:email_registrable?).returns(false)
      post :email_signup, user: { first_name: "abc", last_name: "xyz", password: User.random_password, email: "abc@exyz.com" }
      assert_response :unprocessable_entity
      resp = get_json_response(response)
      assert_equal "Email domain is invalid for signup", resp['message']
    end

    test ':api email signup with valid parameters' do
      @organization = @org
      stub_okta_user
      stub_okta_session_token

      profile = {
        email: "okta@edcast.com",
        first_name: "Okta",
        last_name: "User",
        password: User.random_password,
        profile_attributes: {
          tac_accepted: true, dob: '12/20/1992'
        }
      }
      assert_difference -> {User.count}, 1 do
        assert_difference -> {UserProfile.count}, 1 do
          post :email_signup, user: profile
          assert_response :ok
        end
      end

      user = @org.users.find_by(email: "okta@edcast.com")
      assert_equal @org.id, user.organization_id
      assert_equal 'member', user.organization_role
      assert_equal "1992-12-20".to_date, user.profile.dob
      refute user.password_reset_required
      refute user.is_complete
      assert user.profile.tac_accepted
      assert_equal Date.today, user.profile.tac_accepted_at.to_date
    end

    test ':api email signup with okta_enabled' do
      @organization = @org
      stub_okta_user
      stub_okta_session_token
      Organization.any_instance.stubs(:okta_enabled?).returns(true)

      profile = {
        email: "okta@edcast.com",
        first_name: "Okta",
        last_name: "User",
        password: User.random_password,
        profile_attributes: {
          tac_accepted: true, dob: '12/20/1992'
        }
      }
      assert_difference -> {User.count}, 1 do
        assert_difference -> {UserProfile.count}, 1 do
          post :email_signup, user: profile
          assert_response :ok
        end
      end
      user = @org.users.find_by(email: "okta@edcast.com")
      assert_equal @org.id, user.organization_id
      assert_equal 'member', user.organization_role
      assert_equal "1992-12-20".to_date, user.profile.dob
      refute user.password_reset_required
      refute user.is_complete
      assert user.profile.tac_accepted
      assert_equal Date.today, user.profile.tac_accepted_at.to_date
    end
  end

  class ProvidersAssignmentTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      @card = create(:card, ecl_id: "1234", organization: @org, author_id: nil)
      api_v2_sign_in @user
    end

    test ':api should create providers assignment' do
      LrsAssignmentService.any_instance.expects(:run).returns(Assignment.new).once
      post :providers_assignment, ecl_id: "1234", status: 'started', format: :json
      assert_response :ok
    end

    test ':api should create providers assignment with given assignor' do
      assignor = create(:user, organization: @org)

      post :providers_assignment, ecl_id: "1234", status: 'started', assignor_id: assignor.id, format: :json
      assert_response :ok

      assignment = Assignment.where(assignable: @card, assignee: @user).first
      assert assignment
      assert TeamAssignment.find_by(assignor_id: assignor.id, assignment: assignment)
    end

    test 'should return bad request with invalid params' do
      post :providers_assignment, format: :json
      assert_response :bad_request
    end
  end

  class SkillsTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      @skills = create_list(:skill, 2)
      @user.skills << @skills
    end

    test ':api should return user skills' do
      api_v2_sign_in @user
      get :skills, id: @user.id, format: :json

      resp = get_json_response(response)
      assert_equal 2, resp['skills'].length
    end

    test 'should accessible to other user of orgs too' do
      user = create(:user, organization: @org)
      api_v2_sign_in user
      get :skills, id: @user.id, format: :json

      resp = get_json_response(response)
      assert_equal 2, resp['skills'].length
    end

    test ':api skills response should content skill_level field' do
      api_v2_sign_in @user
      get :skills, id: @user.id, format: :json

      resp = get_json_response(response)
      assert resp['skills'][0].keys.include?('skill_level')
    end

    test ':api should return user skills in asc order of created date when sort and order parameters are not provided' do
      @user2 = create(:user, organization: @org)
      @skills = create_list(:skill, 4)
      @skills_user = (0..3).to_a.reverse.map do |i|
        create(:skills_user, user: @user2, skill: @skills[i] , created_at: i.hours.ago)
      end
      api_v2_sign_in @user2
      get :skills, id: @user2.id, format: :json
      resp = get_json_response(response)
      assert_equal @skills_user.map { |skill| skill['id'] }, resp['skills'].map { |skill| skill['id'] }
    end

    test ':api should return user skills in desc order of created date when sort and order parameters are provided' do
      @user2 = create(:user, organization: @org)
      @skills = create_list(:skill, 4)
      @skills_user = (0..3).to_a.reverse.map do |i|
        create(:skills_user, user: @user2, skill: @skills[i] , created_at: i.hours.ago)
      end
      api_v2_sign_in @user2
      get :skills, id: @user2.id, sort: "created_at", order: "desc", format: :json
      resp = get_json_response(response)
      assert_equal @skills_user.reverse.map { |skill| skill['id'] }, resp['skills'].map { |skill| skill['id'] }
    end

    test ':api should return user skills in desc order of updated date when sort and order parameters are provided' do
      @user2 = create(:user, organization: @org)
      @skills = create_list(:skill, 4)
      @skills_user = (0..3).to_a.reverse.map do |i|
        create(:skills_user, user: @user2, skill: @skills[i] , updated_at: i.hours.ago)
      end
      api_v2_sign_in @user2
      get :skills, id: @user2.id, sort: "updated_at", order: "desc", format: :json
      resp = get_json_response(response)
      assert_equal @skills_user.reverse.map { |skill| skill['id'] }, resp['skills'].map { |skill| skill['id'] }
    end
  end

  class RemoveSkillTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      @skills = create_list(:skill, 2)
      @user.skills << @skills
    end

    test ':api should remove user skill' do
      api_v2_sign_in @user
      assert_difference -> {SkillsUser.count}, -1 do
        delete :remove_skill, id: @user.id, skills_user_id: @user.skills_users.first.id,  format: :json
      end
    end

    test 'should return 404 if skill is not present' do
      user = create(:user, organization: @org)
      api_v2_sign_in user
      delete :remove_skill, id: @user.id, skills_user_id: 2344,  format: :json
      assert_response :not_found
    end
  end

  class ShowActionTest < ActionController::TestCase
    setup do
      org = create(:organization)
      @user = create(:user, organization: org)
      @user_no_teams = create(:user, organization: org)
      @team, @team2 = create_list(:team, 2, organization: org)
      @team.add_admin @user
      @team2.add_member @user
      api_v2_sign_in(@user)
    end

    test ':api show should content proper keys in user_teams field' do
      expected_keys = %w[team_id name is_dynamic is_mandatory]
      get :show, id: @user.id, format: :json
      assert_response :ok
      res = get_json_response(response)

      assert res.key?('user_teams'), 'user_teams key should be present in response'
      assert_same_elements expected_keys, res['user_teams'].first.keys
      assert_equal 2, res['user_teams'].size
      assert_equal @team.id, res['user_teams'][0]['team_id']
      assert_equal @team2.id, res['user_teams'][1]['team_id']
    end

    test ':api should response with empty array for the user without a teams' do
      get :show, id: @user_no_teams.id, format: :json
      assert_response :ok
      res = get_json_response(response)
      assert res.key?('user_teams'), 'response should contain user_teams key'
      assert res['user_teams'].empty?, 'user_teams should be empty array'
    end

    test 'should return onboarding completed date' do
      @onboarding = create :user_onboarding, user: @user
      @onboarding.start!
      @onboarding.complete!
      get :show, id: @user.id, format: :json
      assert_response :ok
      res = get_json_response(response)
      assert_equal @onboarding.reload.completed_at, res['onboarding_completed_date']
    end

    test 'should return dashboard_info on show json' do
      dashboard_info = [{'name': 'Learning Hours',
                        'visible': 'true'
                        },
                        {'name': 'Peer Learning',
                        'visible': 'true'
                        }]
      user_profile = create :user_profile, user: @user, dashboard_info: dashboard_info
      get :show, id: @user.id, format: :json
      assert_response :ok
      res = get_json_response(response)
      assert_equal  user_profile.dashboard_info.map {|up| up.stringify_keys}, res['dashboard_info']
    end

    test 'api show should return password_changeable value as true in response if identity_provider of user is present' do
      user_profile = create :user_profile, user: @user
      get :show, id: @user.id, format: :json
      assert_response :ok
      res = get_json_response(response)
      assert  res['password_changeable']
    end

    test 'api show should return password_changeable value as false in response if identity_provider of user is absent' do
      # add identity provider to user
      provider = create :identity_provider, :saml, user: @user
      get :show, id: @user.id, format: :json
      assert_response :ok
      res = get_json_response(response)
      refute  res['password_changeable']
    end
  end

  class UserActivityStreamsTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user, @team_member = create_list(:user, 2, organization: @org)
      @team, @team2, @team3 = create_list(:team, 3, organization: @org)
      teams = [@team, @team2, @team3]
      teams.each { |t| t.add_member @user }
      teams[0..1].each { |t| t.add_member @team_member }
      @private_card = create(:card, author: @user, is_public: false)
      @card = create(:card, author: @user)
      @card2 = create(:card, author: @user)
      @team_comment = create(:comment, message: 'team', commentable: @team, user: @user)
      @team_comment2 = create(:comment, message: 'team', commentable: @team2, user: @user)
      @team_comment3 = create(:comment, message: 'team', commentable: @team3, user: @user)
      @org_comment = create(:comment, message: 'organization', commentable: @user.organization, user: @user)
      @activity_team_cmnt = create(:activity_stream, action: 'comment', streamable: @team_comment, user: @user, organization: @org)
      @activity_team2_cmnt = create(:activity_stream, action: 'comment', streamable: @team_comment2, user: @user, organization: @org)
      @activity_team3_cmnt = create(:activity_stream, action: 'comment', streamable: @team_comment3, user: @user, organization: @org)
      api_v2_sign_in @team_member
    end

    test ':api should get AS for the teams where user participate' do
      # user participate team and team2
      # assignment for team
      create(
        :team_assignment, team_id: @team.id,
        assignment_id: create(
          :assignment, user_id: @user.id, assignable: @card
        ).id
      )
      # assignment for team3
      create(
        :team_assignment, team_id: @team3.id,
        assignment_id: create(
          :assignment, user_id: @user.id, assignable: @card2
        ).id
      )
      create(
        :activity_stream, action: 'created_card', streamable: @card,
        user: @user, organization: @org, card_id: @card.id
      )
      create(
        :activity_stream, action: 'created_card', streamable: @card2,
        user: @user, organization: @org, card_id: @card2.id
      )

      get :activity_streams, format: :json
      assert_response :ok
      res = get_json_response(response)['activity_streams']

      team_comments_ids = res.collect do |as|
        as['linkable']['id'] if as['linkable']['type'] == 'team'
      end

      check_card_id = res.collect do |as|
        as['linkable']['id'] if as['linkable']['type'] == 'card'
      end

      assert_equal 3, res.count
      assert_equal 3, res.count
      # teams comments
      assert_same_elements [@team.id, @team2.id], team_comments_ids.compact
      # others teams AS
      assert_same_elements [@card.id], check_card_id.compact
    end

    test ':api should get conversations AS for the teams where user participate' do
      create(:team_assignment, team_id: @team.id, assignment_id: create(:assignment, user_id: @user.id, assignable: @card).id)
      create(:activity_stream, action: 'created_card', streamable: @card, user: @user, organization: @org, card_id: @card.id)

      get :activity_streams, only_conversations: true, format: :json
      assert_response :ok
      res = get_json_response(response)['activity_streams']
      team_cmnts_ids = res.collect { |as| as['linkable']['id'] if as['linkable']['type'] == 'team' }.compact
      assert_equal 2, res.count
      # teams comments
      assert_same_elements [@team.id, @team2.id], team_cmnts_ids
    end

    test 'should not return private content in activity streams' do
      create(:team_assignment, team_id: @team.id, assignment_id: create(:assignment, user_id: @user.id, assignable: @private_card).id)
      create(:activity_stream, action: 'created_card', streamable: @private_card, user: @user, organization: @org, card_id: @private_card.id)
      get :activity_streams, format: :json
      assert_response :ok
      res = get_json_response(response)['activity_streams']
      assert_equal 2, res.count
      activity_actions = res.collect {|as| as['action']}
      assert_not activity_actions.include?('created')
    end

    test 'should return private content if user has access' do
      create(:team_assignment, team_id: @team.id, assignment_id: create(:assignment, user_id: @user.id, assignable: @private_card).id)
      create(:activity_stream, action: 'created_card', streamable: @private_card, user: @user, organization: @org, card_id: @private_card.id)
      create(:card_user_share, user_id: @team_member.id, card_id: @private_card.id)
      get :activity_streams, format: :json
      assert_response :ok
      res = get_json_response(response)['activity_streams']
      assert_equal 3, res.count
      activity_actions = res.collect {|as| as['action']}
      assert activity_actions.include?('created_card')
    end
  end

  class UserDeletionActionsTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      create_org_master_roles @org
      @admin, @member = create_list(:user, 2, organization: @org)
      @admin.update(organization_role: 'admin')
      @admin.add_role(Role::TYPE_ADMIN)
      @member.add_role Role::TYPE_MEMBER
    end

    test 'admin route can delete a user' do
      api_v2_sign_in @admin
      Analytics::UserDataDeletionJob.expects(:perform_later).with(user: @member)
      post :admin_delete_account, id: @member.id

      @member.reload
      refute @member.is_active
      assert @member.is_suspended
      assert_equal @member.status, User::SUSPENDED_STATUS
      assert_response :ok
    end

    test 'admin route cant delete a user in a different org' do
      org2 = create :organization
      member2 = create :user, organization: org2
      api_v2_sign_in @admin
      Analytics::UserDataDeletionJob.expects(:perform_later).never
      @controller.expects(:sign_out).never

      post :admin_delete_account, id: member2.id

      member2.reload
      assert member2.is_active
      refute member2.is_suspended
      assert_equal member2.status, User::ACTIVE_STATUS
      assert_response :not_found
    end

    test 'only admins can use admin route' do
      api_v2_sign_in @member
      Analytics::UserDataDeletionJob.expects(:perform_later).never
      @controller.expects(:sign_out).never

      post :admin_delete_account, id: @admin.id

      @admin.reload
      assert @admin.is_active
      refute @admin.is_suspended
      assert_equal @admin.status, User::ACTIVE_STATUS
      assert_response :unauthorized
    end

    test 'admins cant delete themselves' do
      api_v2_sign_in @admin
      Analytics::UserDataDeletionJob.expects(:perform_later).never
      @controller.expects(:sign_out).never

      post :admin_delete_account, id: @admin.id

      @admin.reload
      assert @admin.is_active
      refute @admin.is_suspended
      assert_equal @admin.status, User::ACTIVE_STATUS
      assert_response :unprocessable_entity
    end

    test 'users can delete themselves' do
      api_v2_sign_in @member
      Analytics::UserDataDeletionJob.expects(:perform_later).with(user: @member)
      @controller.expects(:sign_out).with(:user)
      @controller.expects(:sign_out).with(@member)

      post :delete_account

      @member.reload
      refute @member.is_active
      assert @member.is_suspended
      assert_equal @member.status, User::SUSPENDED_STATUS
      assert_response :ok
    end

    test 'users can delete their search history' do
      api_v2_sign_in @member
      Analytics::DeleteUserSearchHistoryJob.expects(:perform_later).with(
        user_id: @member.id,
        org_id: @org.id
      )
      post :clear_search_history
      assert_response :ok
    end

  end


  class UserSuspendActionsTest < ActionController::TestCase

    setup do
      @org = create(:organization)
      create_org_master_roles @org

      @admin, @member1, @member2 = create_list(:user, 3, organization: @org)
      @admin.update(organization_role: 'admin')
      @admin.add_role(Role::TYPE_ADMIN)
      @member1.add_role Role::TYPE_MEMBER
      @member2.add_role Role::TYPE_MEMBER

      api_v2_sign_in @admin
    end

    test ':api suspend should not be available for member users' do
      api_v2_sign_in(@member1)
      post :suspend, id: @member2.id, format: :json
      assert_response :unauthorized
    end

    test ':api admins cant suspend themselves' do
      api_v2_sign_in(@admin)
      post :suspend, id: @admin.id, format: :json
      assert_response :unprocessable_entity
    end

    test ':api suspend could be available for admins only' do
      api_v2_sign_in(@admin)
      User.any_instance.expects(:reindex_async).returns(nil).once
      post :suspend, id: @member1.id, format: :json
      assert_response :ok

      assert @member1.reload.is_suspended?
    end

    test ':api suspend do not suspend already suspended user' do
      @member1.update_columns(organization_role: 'member', is_suspended: true)

      api_v2_sign_in(@admin)
      post :suspend, id: @member1.id, format: :json
      assert_response :unprocessable_entity
    end

    test ':api bulk_suspend should suspend users and change status' do
      SuspendUserJob.unstub :perform_later
      EdcastActiveJob.unstub :perform_later
      api_v2_sign_in(@admin)
      post :bulk_suspend, user_ids: [@member1.id, @member2.id], format: :json
      assert_response :ok

      assert @member1.reload.is_suspended?
      assert_equal @member1.reload.status, User::SUSPENDED_STATUS
      assert @member2.reload.is_suspended?
      assert_equal @member2.reload.status, User::SUSPENDED_STATUS
    end

    test ':api bulk_suspend should be available only for admins' do
      api_v2_sign_in(@member1)
      post :bulk_suspend, user_ids: [@member1.id, @member2.id], format: :json
      assert_response :unauthorized
    end

    test ':api unsuspend a user should be available for admin' do
      api_v2_sign_in(@member1)
      post :unsuspend, id: @member2.id, format: :json
      assert_response :unauthorized
    end

    test ':api unsuspend a user should not be invoked by member users' do
      api_v2_sign_in(@member1)
      post :unsuspend, id: @member2.id, format: :json
      assert_response :unauthorized
    end

    test 'do not unsuspend already unsuspended user' do
      @member1.update_columns(organization_role: 'member', is_suspended: false)

      api_v2_sign_in(@admin)
      post :unsuspend, id: @member1.id, format: :json
      assert_response :unprocessable_entity
    end

    test ':api should change user status to suspended' do
      api_v2_sign_in(@admin)
      post :user_status, id: @member1.id, status: 'suspended', format: :json
      assert_response :ok

      @member1.reload
      assert @member1.is_suspended?
      assert_equal 'suspended', @member1.status
    end

    test ':api should not change user status to suspended for suspended user' do
      @member1.suspend!

      api_v2_sign_in(@admin)
      post :user_status, id: @member1.id, status: 'suspended', format: :json
      assert_response :unprocessable_entity
    end
  end

  class UserAnonymizeActionsTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      create_org_master_roles @org

      @admin, @member1, @member2 = create_list(:user, 3, organization: @org)
      @admin.update(organization_role: 'admin')
      @admin.add_role(Role::TYPE_ADMIN)
      @member1.add_role Role::TYPE_MEMBER
      @member2.add_role Role::TYPE_MEMBER
    end

    test ':api anonymize should not be available for member users' do
      api_v2_sign_in(@member1)
      post :anonymize, id: @member2.id, format: :json
      assert_response :unauthorized
    end

    test ':api anonymize could be available for admins only , replacing the user name' do
      api_v2_sign_in(@admin)
      stub_request(:get, "https://d1iwkfmdo6oqxx.cloudfront.net/assets/new-anonymous-user-medium-72db35f5f02b75175f9eb46d1abd30c8.jpeg").
        with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
        to_return(:status => 200, :body => "", :headers => {})

      post :anonymize, id: @member1.id, format: :json
      assert_response :ok

      assert @member1.reload.is_suspended
      assert @member1.reload.is_anonymized
      assert_equal  Settings.anonymized_user_values.first_name, @member1.reload.first_name
      assert_equal  Settings.anonymized_user_values.last_name, @member1.reload.last_name
      assert_equal  Settings.anonymized_user_values.first_name, @member1.reload.name
    end

    test ':api anonymize do not anonymize already anonymized user' do
      @member1.update_attributes(is_anonymized: true)

      api_v2_sign_in(@admin)
      post :anonymize, id: @member1.id, format: :json
      assert_response :unprocessable_entity
    end

    test ':api unsuspend do not unsuspend to  anonymized user' do
      @member1.update_attributes(is_anonymized: true)

      api_v2_sign_in(@admin)
      post :unsuspend, id: @member1.id, format: :json
      assert_response :unprocessable_entity
    end
  end

  class UserMangedCardsTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      @author = create(:user, organization: @org)
      @public_card = create(:card, author: @author, organization: @org)
      @public_card_2 = create(:card, author: @author, organization: @org)
      api_v2_sign_in @user
    end

    test ':api managed_cards should have cards key in response' do
      get :managed_cards, format: :json
      assert_response :ok
      res = get_json_response(response)
      keys = %w[cards managed_card_actions]
      assert_same_elements keys, res.keys
    end

    test ':api managed_cards should have managed_card_actions key in response' do
      UsersCardsManagement.create(user: @user, card: @public_card, action: UsersCardsManagement::ACTION_POST_TO_CHANNEL, target_id: 1)
      get :managed_cards, format: :json
      assert_response :ok
      res = get_json_response(response)
      managed_action_keys = %w[action entries]
      managed_action_entries_keys = %w[card_id target_id]
      assert_same_elements managed_action_keys, res['managed_card_actions'].first.keys
      assert_same_elements managed_action_entries_keys, res['managed_card_actions'].first['entries'].first.keys
    end

    test ':api managed_cards content expected data in response' do
      UsersCardsManagement.create(user: @user, card: @public_card, action: UsersCardsManagement::ACTION_POST_TO_CHANNEL, target_id: 1)
      get :managed_cards, format: :json
      assert_response :ok
      res = get_json_response(response)
      action = UsersCardsManagement::ACTION_POST_TO_CHANNEL
      card_ids = res['cards'].map { |i| i['id'].to_i }
      assert_equal card_ids, [@public_card.id]
      assert_equal action, res['managed_card_actions'].first['action']
      assert_equal @public_card.id, res['managed_card_actions'].first['entries'].first['card_id']
      assert_equal 1, res['managed_card_actions'].first['entries'].first['target_id']
    end

    test ':api managed_cards should be able to set limit' do
      UsersCardsManagement.create(user: @user, card: @public_card, action: UsersCardsManagement::ACTION_POST_TO_CHANNEL, target_id: 1)
      UsersCardsManagement.create(user: @user, card: @public_card_2, action: UsersCardsManagement::ACTION_ADDED_TO_PATHWAY, target_id: 2)
      get :managed_cards, format: :json, limit: '1'
      assert_response :ok
      res = get_json_response(response)['cards']
      assert_equal 1, res.count
      assert_equal @public_card.id.to_s, res.first['id']
    end

    test ':api managed_cards should be able to set offset' do
      UsersCardsManagement.create(user: @user, card: @public_card, action: UsersCardsManagement::ACTION_POST_TO_CHANNEL, target_id: 1)
      UsersCardsManagement.create(user: @user, card: @public_card_2, action: UsersCardsManagement::ACTION_ADDED_TO_PATHWAY, target_id: 2)
      get :managed_cards, format: :json, limit: '1', offset: '1'
      assert_response :ok
      res = get_json_response(response)['cards']
      assert_equal 1, res.count
      assert_equal @public_card_2.id.to_s, res.first['id']
    end
  end

  class OnePagerTranscriptTest < ActionController::TestCase
    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      api_v2_sign_in @user
    end

    test ':api to get user one page transcript' do
      get :user_transcript
      assert_response :ok
    end
  end

  class AccessibleCandidatesTest < ActionController::TestCase
    setup do
      @o = create :organization
      @a = create :user, organization: @o
      @u = create :user, organization: @o

      @team1 = create :team, organization: @o
      @team2 = create :team, organization: @o
      @team3 = create :team, organization: @o

      # @a is an admin to @team1 and @u is a member to @team1
      @team1.add_admin(@a)
      @team1.add_member(@u)
      # @u is an admin to @team2
      @team2.add_admin(@u)

      @channel1 = create :channel, organization: @o, user_id: @a
      @channel2 = create :channel, organization: @o, user_id: @a
      @channel3 = create :channel, organization: @o, user_id: @a

      # @u is an author to @channel1
      @channel1.add_authors([@u])
      # @u is an followers to @channel2
      @channel2.add_followers([@u])

      api_v2_sign_in @u
    end

    test ':api must return the encrypted data' do
      get :accessible_candidates, format: :json

      resp = get_json_response(response)

      jsonified_data = Aes.decrypt(resp['candidates'], Settings.features.integrations.secret)
      candidates     = JSON.parse(jsonified_data)

      assert_equal [@team1.id, @team2.id].to_set, candidates['groups_with_access'].to_set
      assert_equal [@channel1.id, @channel2.id].to_set, candidates['channels_with_access'].to_set
    end
  end

  class ProfilesTest < ActionController::TestCase
    setup do
      @org1 = create :organization
      @org2 = create :organization

      @admin = create :user, organization: @org1
      @user1 = create :user, organization: @org1
      @user2 = create :user, organization: @org2

      api_v2_sign_in @admin
    end

    test 'should update users job_title' do
      org1 = create(:organization)
      user1 = create(:user, organization: org1)
      api_v2_sign_in user1
      title = (0..30).map { ('a'..'z').to_a[rand(26)] }.join

      put :update, id: user1.id, user: { profile_attributes: { job_title: title } }, format: :json
      resp = get_json_response(response)
      assert_equal title, resp['profile']['job_title']
    end

    test ':api returns user profiles belonging to an organization' do
      get :profiles, user_ids: [@user1.id, @user2.id], fields: 'id,handle,name', format: :json

      users = get_json_response(response)['users']

      org_user = users.select { |user| user['id'] == @user1.id }[0]
      assert_equal @user1.name, org_user['name']
      assert_equal @user1.handle, org_user['handle']

      assert_empty users.select { |user| user['id'] == @user2.id }
    end
  end

  test 'search users to mention them in comments' do
    @user = create :user
    @org = @user.organization
    user = create :user, first_name: 'user1 firstname', last_name: 'user1 lastname', organization: @org
    query = 'firstname'

    assert_equal @org.id, user.organization_id

    Search::UserSearch.any_instance.expects(:search).once.
      returns(Search::UserSearchResponse.new(results: [user], total: 1))

    api_v2_sign_in @user
    get :search, q: query, limit: 5, mention_user: true, format: :json
    users = get_json_response(response)['users']
    assert_equal 1, users.length
    assert_same_elements %w(id handle avatarimages name), users.first.keys
  end


  test 'join_url_mobile - returns 401 when welcome_token is not sent' do
    org = create :organization
    user = create :user, organization: org
    set_host(org)

    get :join_url_mobile
    assert_response :unauthorized
  end

  test 'join_url_mobile - uses welcome_token for authentication' do
    raw_token, encrypted_token = Devise.token_generator.generate(User, :welcome_token)
    org = create :organization
    user = create :user, organization: org , welcome_token: encrypted_token
    set_host(org)
    get :join_url_mobile, welcome_token: raw_token
    assert_response :success

    decoded_token = JwtAuth.decode response.header["X_API_TOKEN"]
    assert_equal user.id, decoded_token["user_id"]
    assert_equal 300, decoded_token["expire_after"]
  end

  test ':api last_viewed should return the data of last viewed ' do
    begin 
      orig_cache = Rails.cache
      @user = create :user
      @org = @user.organization
      Rails.cache       = ActiveSupport::Cache::MemoryStore.new
      last_activity     = Analytics::LastActivityRecorder.new(user_id: @user.id)
      last_activity.add_data_to_cache(event: "channel_visited",value: {"any_key":"any_value"})
      expected_response = {
        "event"              =>  "channel_visited", 
        "entity"             =>  "channel", 
        "last_viewed_at"     =>  nil, 
        "event_attributes"   =>  {"any_key" => "any_value"}
      }

      api_v2_sign_in @user
      get :last_viewed, format: :json
      resp = get_json_response(response)
      assert_equal expected_response, resp
    ensure
      Rails.cache = orig_cache
    end
  end

  test ':api peer_learning should return avg time spent by the org users and sum of time spent by requested user' do
    now                 = Time.now.utc
    @user               = create :user
    @org                = @user.organization
    users               = create_list(:user, 6, organization: @org)
    dates               = [now, now-1.month, now-2.month, now-3.month, now-4.month, now-5.month]

    dates.each do |d|
      users.each_with_index do |user, indx|
        create(:user_metrics_aggregation, 
          organization:         @org, 
          user:                 user,
          time_spent_minutes:   indx,
          start_time:           d.beginning_of_day,
          end_time:             d.end_of_day,
          created_at:           d
        )
      end
    end
    expected_years        = []
    expected_months       = []
    dates.sort.reverse.each do |d|
      expected_months << d.month
      expected_years  << d.year
    end

    api_v2_sign_in @user
    get :peer_learning, id: users.last.id, period: "month", format: :json

    resp                  = get_json_response(response)
    org_users_data        = resp["org_users_data"]
    user_data             = resp["user_data"]

    org_actual_avg_time   = []
    org_actual_months     = []
    org_actual_years      = []
    user_actual_avg_time  = []
    user_actual_months    = []
    user_actual_years     = []

    org_users_data.each do |data|
      org_actual_avg_time << data["avg_time_spent"].to_i
      org_actual_months   << data["month"]
      org_actual_years    << data["year"]
    end

    user_data.each do |data|
      user_actual_avg_time << data["time_spent"].to_i
      user_actual_months   << data["month"]
      user_actual_years    << data["year"]
    end

    assert_equal [2, 2, 2, 2, 2, 2],  org_actual_avg_time 
    assert_equal expected_months,     org_actual_months
    assert_equal expected_years,      org_actual_years
    assert_equal [5, 5, 5, 5, 5, 5],  user_actual_avg_time 
    assert_equal expected_months,     user_actual_months
    assert_equal expected_years,      user_actual_years
  end

  test ':api peer_learning should return unprocessable_entity if period is blank' do
    now                 = Time.now.utc
    @user               = create :user
    @org                = @user.organization
    users               = create_list(:user, 6, organization: @org)

    api_v2_sign_in @user
    get :peer_learning, id: users.last.id, format: :json

    assert_response :unprocessable_entity
  end
end


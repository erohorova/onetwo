require 'test_helper'

class Api::V2::NotificationsControllerTest < ActionController::TestCase
  self.use_transactional_fixtures = false

  setup do
    WebMock.allow_net_connect!

    NotificationGeneratorJob.unstub :perform_later
    VideoStreamBatchNotificationGeneratorJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    [VideoStream, WowzaVideoStream, IrisVideoStream].each do |klass|
      klass.any_instance.stubs :create_index
      klass.any_instance.stubs :update_index
      klass.any_instance.stubs :populate_meta_data
    end
    Comment.any_instance.stubs(:record_comment_event)

    # WebMock.disable!
    @org = create(:organization)
    @user = create(:user, organization: @org)

    api_v2_sign_in @user
  end

  teardown do
    WebMock.disable_net_connect!
  end

  test ':api see upto should work' do
    ts = 6.hours.ago.strftime('%Y-%m-%dT%H:%M:%SZ')
    assert_difference ->{UserNotificationRead.count} do
      post :seen_at, timestamp: ts, format: :json
      assert_response :ok
    end

    unr = UserNotificationRead.where(user: @user).first
    assert_equal unr.seen_at, ts

    # update exsiting on next request
    ts = Time.now.strftime('%Y-%m-%dT%H:%M:%SZ')
    assert_no_difference ->{UserNotificationRead.count} do
      post :seen_at, timestamp: ts, format: :json
      assert_response :ok
    end

    unr = UserNotificationRead.where(user: @user).first
    assert_equal unr.seen_at, ts
  end

  test ':api mark notification as seen' do
    another_user = create(:user, organization: @org)
    random_user  = create(:user, organization: @org)
    VideoStream.any_instance.stubs(:schedule_reminder_push_notification)
    card = create(:card, author: @user, organization: @org)
    card.reload
    another_card = create(:card, author: another_user, organization: @org)
    another_card.reload

    comment = create(:comment, commentable: card, user: random_user)
    comment.run_callbacks(:commit)

    another_comment = create(:comment, commentable: another_card, user: random_user)
    another_comment.run_callbacks(:commit)
    n1 = Notification.where(user: @user, notifiable: card).last
    n2 = Notification.where(user: another_user, notifiable: another_card).last
    assert n1.unseen
    assert n2.unseen

    post :read, id: n1.id, format: :json
    assert_response :ok
    assert_not n1.reload.unseen

    post :read, id: n2.id, format: :json
    assert_response :unauthorized
    assert n2.reload.unseen
  end

  test ':api push notification to mobile' do
    admin_user = create(:user, organization: @org, organization_role: 'admin')
    card = create(:card, author: @user, organization: @org)
    payload = {
      'deep_link_type'    => 'card',
      'deep_link_id'      => card.id,
      'notification_type' => 'notification'
    }

    api_v2_sign_in admin_user
    post :push_to_org, content: "Test message", payload: payload.to_json.to_s
    assert_response :ok

    # invalid payload
    payload['deep_link_id'] = -1
    post :push_to_org, content: "Test message", payload: payload.to_json.to_s
    assert_response :unprocessable_entity
    json_response = get_json_response(response)
    assert_includes json_response['message'], "Invalid payload"

    sign_out admin_user
    post :push_to_org, content: "Test message", payload: payload.to_json.to_s
    assert_response :unprocessable_entity
  end

  test ':push_to_org endpoint records event to influx' do
    admin_user = create(:user, organization: @org, organization_role: 'admin')
    card = create(:card, author: @user, organization: @org)
    metadata = { user_id: admin_user.id }
    RequestStore.stubs(:read).with(:request_metadata).returns metadata
    time = Time.now
    Time.stubs(:now).returns time

    payload = {
      'deep_link_type'    => 'card',
      'deep_link_id'      => card.id,
      'notification_type' => 'notification'
    }
    content = "Test message"

    api_v2_sign_in admin_user

    Analytics::MetricsRecorderJob.unstub(:perform_later)
    EdcastActiveJob.unstub(:perform_later)

    # expect that this event was recorded
    $expected_params = {
      recorder: "Analytics::OrgMetricsRecorder",
      event: "org_custom_notification_sent",
      org: Analytics::MetricsRecorder.org_attributes(@org),
      timestamp: time.to_i,
      actor: Analytics::MetricsRecorder.user_attributes(admin_user),
      notification: {
        'json_payload' => payload.merge(
          host_name:         @org.host,
          notification_type: "notification",
          channel_names:     [@org.public_pubnub_channel_name],
        ).to_json,
        'message' => content
      },
      additional_data: metadata
    }
    Analytics::MetricsRecorderJob.expects(:perform_later).with($expected_params)
    post :push_to_org, content: content, payload: payload.to_json.to_s
    assert_response :ok

    # invalid payload - no event will be recorded
    payload['deep_link_id'] = -1
    Analytics::MetricsRecorderJob.expects(:perform_later).never
    post :push_to_org, content: content, payload: payload.to_json.to_s
    assert_response :unprocessable_entity
  end

  # index
  test ':api should return user notifications' do
    skip("INTERMITTENT FAILURE ON TRAVIS")
    assert_difference -> {Notification.count}, 6 do
      setup_user_notifications
    end

    get :index, format: :json
    resp = get_json_response(response)
    assert_response :ok
    assert_equal 3, resp['unseen_notifications_count']
    assert_equal 3, resp['notifications'].count

    upvote_comment_notification = resp['notifications'][0]

    assert_equal 'card', upvote_comment_notification['deep_link_type']
    assert_equal @card.id, upvote_comment_notification['deep_link_id']
    assert_equal 1, upvote_comment_notification['users'].count
    assert_not_nil upvote_comment_notification['message']
    assert resp['notifications'][0]['unseen']

    comment_notification = resp['notifications'][1]

    assert_equal 'card', comment_notification['deep_link_type']
    assert_equal @card.id, comment_notification['deep_link_id']
    assert_equal 2, comment_notification['users'].count
    assert_not_nil comment_notification['message']
    assert resp['notifications'][0]['unseen']

    follow_notification = resp['notifications'][2]

    assert 'follow', follow_notification['deep_link_type']
    assert 0, follow_notification['deep_link_id']
    assert_equal 1, follow_notification['users'].count
    assert_not_nil follow_notification['message']
    assert resp['notifications'][0]['unseen']

    Notification.where(id: upvote_comment_notification['id']).first.mark_as_seen

    get :index, format: :json

    resp = get_json_response(response)

    assert_equal 2, resp['unseen_notifications_count']
    assert_equal 3, resp['notifications'].count
    refute resp['notifications'][0]['unseen']
  end

  test 'should not display notifications which are crated after a specific day' do
    setup_user_notification_limit_days
    @org.configs.create(name: 'notificationLimitDays', value: 10, data_type: 'integer')

    get :index, format: :json
    resp = get_json_response(response)

    array_of_dates = resp['notifications'].map{|n| n['created_at'].to_time.strftime('%Y-%m-%d')}
    assert_equal false, array_of_dates.include?((Date.today - 10.days).strftime('%Y-%m-%d'))
  end

  test 'should display notification which are cretaed before a specific day' do
    skip # NEED TO FIX THIS
    setup_user_notification_limit_days
    @org.configs.create(name: 'notificationLimitDays', value: 10, data_type: 'integer')

    get :index, format: :json
    resp = get_json_response(response)

    array_of_dates = resp['notifications'].map{|n| n['created_at'].to_time.strftime('%Y-%m-%d')}
    assert_equal true, array_of_dates.include?((Date.today - 2.days).strftime('%Y-%m-%d'))
  end

  test 'should support limit and offset' do
    skip("INTERMITTENT FAILURE ON TRAVIS")
    setup_user_notifications

    get :index, limit: 1, offset: 1, format: :json
    resp = get_json_response(response)

    assert_response :ok
    assert_equal 3, resp['unseen_notifications_count']
    assert_equal 1, resp['notifications'].count

    comment_notification = resp['notifications'][0]

    assert_equal 'card', comment_notification['deep_link_type']
    assert_equal @card.id, comment_notification['deep_link_id']
    assert_equal 2, comment_notification['users'].count
    assert_not_nil comment_notification['message']
    assert resp['notifications'][0]['unseen']
  end

  test 'should handle seen at time' do
    skip("INTERMITTENT FAILURE ON TRAVIS")
    setup_user_notifications

    get :index, format: :json
    resp = get_json_response(response)

    assert_response :ok
    assert_equal 3, resp['unseen_notifications_count']
    assert_equal 3, resp['notifications'].count

    Timecop.freeze(4.hours.ago) do
      UserNotificationRead.where(user: @user, seen_at: Time.current).first_or_create
    end

    get :index, format: :json
    resp = get_json_response(response)

    assert_response :ok
    assert_equal 2, resp['unseen_notifications_count']
    assert_equal 3, resp['notifications'].count
  end

  test 'should send card instead of collection as deep link' do
    skip("INTERMITTENT FAILURE ON TRAVIS")
    org = create(:organization)
    user, commenter = create_list(:user, 2, organization: org)
    cover = create(:card, card_type: 'pack', author_id: user.id, organization_id: org.id)
    cover.reload
    create(:comment, commentable: cover, user: commenter)

    api_v2_sign_in user
    get :index, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal ['collection'], resp['notifications'].map{|n| n['deep_link_type']}
    assert_equal [cover.id], resp['notifications'].map{|n| n['deep_link_id']}
  end

  test 'should send card instead of video stream for video stream notifications' do
    skip("INTERMITTENT FAILURE ON TRAVIS")
    org = create(:organization)
    user, streamer = create_list(:user, 2, organization: org)
    create(:follow, followable: streamer, user: user)

    video_stream = create(:iris_video_stream, creator: streamer, status: 'pending')
    video_stream.start!

    api_v2_sign_in user
    get :index, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal ['card'], resp['notifications'].map{|n| n['deep_link_type']}
    assert_equal [video_stream.card.id], resp['notifications'].map{|n| n['deep_link_id']}
  end

  test 'should send card instead of videostream as deep link for mention notifications' do
    VideoStream.any_instance.stubs(:schedule_reminder_push_notification)
    video_stream = create(:iris_video_stream, creator:@user)
    card = video_stream.card
    card.reload
    comment = create(:comment, commentable: card, message: Faker::Lorem.sentence, user: create(:user, organization: @org))

    noti = create(:notification, user: @user, notifiable: comment, notification_type: "mention_in_comment", app_deep_link_id: video_stream.id, app_deep_link_type: "video_stream")
    get :index, format: :json
    resp = get_json_response(response)

    resp['notifications'].each do |noti|
      assert_equal card.id, noti['deep_link_id']
      assert_equal "card", noti['deep_link_type']
    end
  end

  test 'should send card instead of videostream as deep link' do
    skip("INTERMITTENT FAILURE ON TRAVIS")
    card = nil
    Timecop.freeze(3.hours.ago) do
      VideoStream.any_instance.stubs(:schedule_reminder_push_notification)
      video_stream = create(:iris_video_stream, creator:@user)
      card = video_stream.card
      card.reload
      create(:comment, commentable: card, message: Faker::Lorem.sentence, user: create(:user, organization: @org))
    end

    get :index, format: :json
    resp = get_json_response(response)
    assert_equal card.id, resp['notifications'][0]['deep_link_id']
    assert_equal 'card', resp['notifications'][0]['deep_link_type']
  end

  test 'should send correct deep link type for pathway assignments' do
    skip("INTERMITTENT FAILURE ON TRAVIS")
    UserTeamAssignmentNotificationJob.unstub :perform_later

    assignable = create(:card, card_type: 'pack', card_subtype: 'simple', author: create(:user, organization: @org))
    assignee = create(:user, organization: @org)
    team = create(:team, organization: @org)
    assignment = create(:assignment, assignable: assignable, assignee: assignee)
    team_assignment = create(:team_assignment, team: team, assignment: assignment)

    api_v2_sign_in assignee
    get :index, format: :json
    resp = get_json_response(response)
    assert_equal 1, resp['notifications'].count
    assert_equal 'collection', resp['notifications'][0]['deep_link_type']
  end

  test 'should send correct deep link type for video stream assignments' do
    skip("INTERMITTENT FAILURE ON TRAVIS")
    UserTeamAssignmentNotificationJob.unstub :perform_later

    video_stream = create(:iris_video_stream, status: 'past', creator: create(:user, organization: @org))
    assignable = video_stream.card
    assignee = create(:user, organization: @org)
    team = create(:team, organization: @org)
    assignment = create(:assignment, assignable: assignable, assignee: assignee)
    team_assignment = create(:team_assignment, team: team, assignment: assignment)

    api_v2_sign_in assignee
    get :index, format: :json
    resp = get_json_response(response)
    assert_equal 1, resp['notifications'].count
    assert_equal 'card', resp['notifications'][0]['deep_link_type']
  end

  test 'should strip html tags in notification message' do
    skip("INTERMITTENT FAILURE ON TRAVIS")
    UserTeamAssignmentNotificationJob.unstub :perform_later
    video_stream = create(:iris_video_stream, status: 'past', creator: create(:user, organization: @org))
    assignable = video_stream.card
    assignable.reload
    assignee = create(:user, organization: @org)
    team = create(:team, organization: @org)
    assignment = create(:assignment, assignable: assignable, assignee: assignee)
    team_assignment = create(:team_assignment, team: team, assignment: assignment)

    Notification.any_instance.stubs(:message).returns("assigned you the video stream:'<p>this is my card</p>'")

    api_v2_sign_in assignee
    get :index, format: :json
    resp = get_json_response(response)
    assert_equal "assigned you the video stream:'this is my card'", resp["notifications"][0]["message"]
  end

  test 'should exclude post announcements savannah notifications' do
    skip("INTERMITTENT FAILURE ON TRAVIS")
    assert_difference -> {Notification.count}, 7 do
      setup_savannah_notifications
    end
    announcement_notifications = Notification.where(notifiable: @announcement)
    api_v2_sign_in @group_user
    get :index, format: :json
    resp = get_json_response(response)
    assert (announcement_notifications.pluck(:id) & resp['notifications'].map{|n| n['id']}).empty?
    sign_out @group_user

    api_v2_sign_in @faculty
    get :index, format: :json
    resp = get_json_response(response)
    assert (announcement_notifications.pluck(:id) & resp['notifications'].map{|n| n['id']}).empty?
  end

  test 'should return true when notification_type is group_user_promoted_to_admin' do
    team = create(:team, organization: @org)
    notification = create(:notification, user: @user, notifiable: team, notification_type: "group_user_promoted_to_admin", app_deep_link_id: team.id, app_deep_link_type: "team")

    get :index, format: :json
    resp = get_json_response(response)
    assert_response :ok
    assert_equal true, resp["notifications"][0]['group_user_promoted']
  end

  test 'should return true when notification_type is group_user_promoted_to_sub_admin' do
    team = create(:team, organization: @org)
    notification = create(:notification, user: @user, notifiable: team, notification_type: "group_user_promoted_to_sub_admin", app_deep_link_id: team.id, app_deep_link_type: "team")

    get :index, format: :json
    resp = get_json_response(response)
    assert_response :ok
    assert_equal true, resp["notifications"][0]['group_user_promoted']
  end

  def setup_savannah_notifications
    @group_user, @group_user1, @faculty = create_list(:user, 3, organization: @org)
    @group = create(:resource_group)
    @admin_user = create(:user, organization: @org, organization_role: 'admin')
    @group.add_member @group_user
    @group.add_member @group_user1
    @group.add_admin @faculty
    @group.add_admin @admin_user
    @announcement = create(:announcement, postable: @group, user_id: @faculty.id)
    approval = create(:approval, approvable: @announcement, approver: @admin_user)
    comment = create(:comment, user: @group_user1,  commentable: @announcement, message: "message", type: "Answer")
    vote = create(:vote, votable: @announcement, voter: @group_user1)
    card = create(:card, author_id: @group_user.id)
    card.reload
    card_comment = create(:comment, user: @group_user1, commentable: card, message: "message")
  end

  def setup_user_notifications
    org_user1, org_user2 = create_list(:user, 2, organization: @org)
    @card = create(:card, author_id: @user.id)
    @card.reload

    # question a user is following
    card_comment_by_user1 = card_comment_by_user2 = nil

    Timecop.freeze(5.hours.ago) do
      # new_follow
      create(:follow, user_id: org_user1.id, followable: @user)
    end

    Timecop.freeze(4.hours.ago) do
      # org_user1 is following @user #new_poll
      poll = create(:card, card_type: 'poll', card_subtype: 'link', author_id: @user.id, is_public: true)
    end

    Timecop.freeze(3.hours.ago) do
      # new_comment
      card_comment_by_user1 = create(:comment, commentable: @card, message: Faker::Lorem.sentence, user: org_user1)
      card_comment_by_user2 = create(:comment, commentable: @card, message: Faker::Lorem.sentence, user: org_user2)
    end

    Timecop.freeze(2.hours.ago) do
      # new_card_upvote
      card_upvote = create(:vote, votable: @card, user_id: org_user2.id)
    end

    Timecop.freeze(1.hours.ago) do
      #stream_started
      vs = create(:iris_video_stream, :pending, creator_id: @user.id)
      vs.update status: 'live' #use start!
    end
  end

  def setup_user_notification_limit_days
    Timecop.freeze(1.hours.ago) do
      create(:notification, user_id: @user.id, created_at: (Date.today - 2.days), app_deep_link_id: 0, app_deep_link_type: 'follow')
    end

    Timecop.freeze(1.hours.ago) do
      create(:notification, user_id: @user.id, created_at: (Date.today - 20.days), app_deep_link_id: 0, app_deep_link_type: 'follow')
    end
  end
end

require 'test_helper'
class Api::V2::CommentReportingsControllerTest < ActionController::TestCase
  class CommentReportingCreateTest < ActionController::TestCase
    setup do
      org = create(:organization)

      @user = create(:user, organization: org)
      @reporting_user = create(:user, organization: org)

      @card = create(:card, author: @user, organization: org)
      @comment = create(:comment, commentable: @card, user: @user)
      api_v2_sign_in @reporting_user
    end

    test ':api should not create comment reporting for comment which is already trashed' do
      another_comment = create(:comment, commentable: @card, user: @user, message: 'This is Wow')
      another_comment.destroy

      post :create, comment_id: another_comment.id, reason: 'some reason', format: :json
      resp = get_json_response(response)
      assert_response :unprocessable_entity
      assert_match 'Comment has been trashed', resp['message']
    end

    test ':api should create comment reporting with reason for card' do
      post :create, comment_id: @comment.id, reason: 'some reason', format: :json
      resp = get_json_response(response)
      assert_response :ok
    end

    test 'should not create duplicate comment reporting if already present' do
      @comment_reporting = create(:comment_reporting, comment: @comment, user: @reporting_user)

      post :create, comment_id: @comment.id, reason: 'some reason', format: :json
      resp = get_json_response(response)

      assert_response :unprocessable_entity
      assert_equal 'Comment is already reported by user', resp['message']
    end
  end

  class CommentReportingIndexTest < ActionController::TestCase
    setup do
      @org = create(:organization)

      @user = create(:user, organization: @org)
      @reporting_user = create(:user, organization: @org, organization_role:'admin')

      @card = create(:card, author: @user, organization: @org)
      @comment = create(:comment, commentable: @card, user: @user, message: 'I am God')
      @another_comment = create(:comment, commentable: @card, user: @user, message: 'This is Wow')

      @comment_reporting_1 = create(:comment_reporting, comment: @comment, user: @reporting_user, reason: 'What a comment', created_at: Date.today- 1.day)
      @comment_reporting_2 = create(:comment_reporting, comment: @comment, user: @user, reason: 'Not a good comment', created_at: Date.today - 2.days)


      @trashed_reporting_1 = create(:comment_reporting, comment: @another_comment, user: @reporting_user, reason: 'This should be trashed', deleted_at: Time.now)
      @trashed_reporting_2 = create(:comment_reporting, comment: @another_comment, user: @user, reason: 'This should be trashed', deleted_at: Time.now)

      api_v2_sign_in @reporting_user
    end

    test ':api should list comment reportings when state = reported' do
      get :index, state: 'reported', format: :json
      resp = get_json_response(response)
      assert_response :ok
      assert_equal 1, resp['total']
    end

    test ':api should list comment reportings when state = reported in descending order' do
      get :index, state: 'reported', format: :json
      resp = get_json_response(response)

      # checking the reported user displayed out of 2 users.
      assert_match @user.name, resp['content'][0]['content_reporter']['reported_by']
    end

    test ':api should list comment reportings when state = reported and search string is GOD' do
      get :index, state: 'reported', q: 'God', format: :json
      resp = get_json_response(response)
      assert_response :ok
      assert_equal 1, resp['total']
    end

    test ':api should list comment reportings when state = trashed and search string is Wow' do
      service = FlagContent::CommentTrashingService.new(comment: @another_comment)
      service.call

      get :index, state: 'trashed', q: 'Wow', format: :json
      resp = get_json_response(response)
      assert_response :ok
      assert_equal 1, resp['total']
    end

    test ':api should not list comment reportings when state = trashed and search string is incorrect' do
      service = FlagContent::CommentTrashingService.new(comment: @another_comment)
      service.call

      get :index, state: 'trashed', q: 'xyz', format: :json
      resp = get_json_response(response)
      assert_response :ok
      assert_equal 0, resp['total']
    end

    test ':api should list comment reportings when state = trashed' do
      service = FlagContent::CommentTrashingService.new(comment: @another_comment)
      service.call

      get :index, state: 'trashed', format: :json
      resp = get_json_response(response)
      assert_response :ok
      assert_equal 1, resp['total']
    end

    test ':api should return the card id of the comment' do
      service = FlagContent::CommentTrashingService.new(comment: @another_comment)
      service.call
      %w{trashed reported}.each do |state|
        get :index, state: state, format: :json
        resp = get_json_response(response)
        assert_response :ok
        assert_equal @card.id, resp['content'][0]['card_id']
      end
    end

    test ':api should list no of reportings of a comment with flag = reported' do
      @new_user = create(:user, organization: @org)

      @comment.comment_reportings

      # unreport the 2 prev reportings
      @comment.comment_reportings.destroy_all

      # report the comment again from a diffferent user
      @comment_reporting_3 = create(:comment_reporting, comment: @comment, user: @new_user, reason: 'Not a good comment', created_at: Date.today)

      @comment.comment_reportings.reload

      get :index, state: 'reported', comment_id: @comment.id, format: :json
      resp = get_json_response(response)

      assert_response :ok
      assert_equal 3, resp['reported_comments'].length
    end

    test ':api should list no of reportings of a comment with flag = trashed' do
      @new_user = create(:user, organization: @org)
      @comment.comment_reportings

      # unreport the 2 prev reportings
      @comment.comment_reportings.destroy_all

      # report the comment again from a diffferent user
      @comment_reporting_3 = create(:comment_reporting, comment: @comment, user: @new_user, reason: 'Not a good comment', created_at: Date.today)

      @comment.comment_reportings.reload

      # trash the comment & reportings
      service = FlagContent::CommentTrashingService.new(comment: @comment)
      service.call

      get :index, state: 'trashed', comment_id: @comment.id, format: :json
      resp = get_json_response(response)

      assert_response :ok
      assert_equal 3, resp['reported_comments'].length
    end
  end

  class CommentReportingAdminTest < ActionController::TestCase
    test 'admin user should be able to list if RBAC is enabled' do
      org = create(:organization, enable_role_based_authorization: true)
      Role.create_standard_roles org
      admin_role = org.roles.find_by_name('admin')
      create(:role_permission, role: admin_role, name: Permissions::ADMIN_ONLY)

      rbac_admin = create(:user, organization: org)
      rbac_admin.add_role('admin')

      api_v2_sign_in rbac_admin
      get :index, state: 'reported', format: :json
      assert_response :ok
    end

    test 'non admin user should not be able list if RBAC is enabled' do
      org = create(:organization, enable_role_based_authorization: false)
      Role.create_standard_roles org
      user = create(:user, organization: org)

      api_v2_sign_in user

      get :index, state: 'reported', format: :json
      assert_response :unauthorized
    end

  end
end

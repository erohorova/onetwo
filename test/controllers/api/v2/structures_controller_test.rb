require 'test_helper'

class Api::V2::StructuresControllerTest < ActionController::TestCase
  setup do
    @org =  create(:organization)
    @channel = create(:channel, organization: @org)
    @user = create(:user, organization: @org)
    @carousel_user = create(:user, organization: @org)
    api_v2_sign_in(@user)

    @structure1 = create :structure, creator: @user, organization: @org, parent: @org, enabled: true
    @structure2 = create :structure, creator: @user, organization: @org, parent: @org, enabled: false
    @structure3 = create :structure, creator: @user, organization: @org, parent: @channel, enabled: true
    @structure4 = create :structure, creator: @user, organization: @org, parent: @org, enabled: true, slug: 'discover-users'

    @item1 = create(:structured_item, structure: @structure1)
    @item2 = create(:structured_item, structure: @structure1)
    @item3 = create(:structured_item, structure: @structure1)

    @item4 = create(:structured_item, structure: @structure2)
    @item5 = create(:structured_item, structure: @structure2)
    @item6 = create(:structured_item, structure: @structure2)

    @item7 = create(:structured_item, structure: @structure3)
    @item8 = create(:structured_item, structure: @structure3)
    @item9 = create(:structured_item, structure: @structure3)

  end

  class StructuresCreateControllerTest < Api::V2::StructuresControllerTest
    test '#create creates a new structure' do
      @card = create(:card, author: @user)
      post :create, structure: {context: 'discover', slug: 'channel', display_name: 'new-discover-carousel', parent_id: @org.id, parent_type: @org.class.to_s}, format: :json
      response = get_json_response(@response)
      assert_equal 'new-discover-carousel', response['display_name']
      assert_equal 1, response['context']
    end

    test '#create fails to create a new structure' do
      @card = create(:card, author: @user)
      post :create, structure: {context: 'discover', slug: 'channel', display_name: 'new-discover-carousel'}, format: :json
      assert_response :unprocessable_entity
    end
  end

  class StructuresIndexControllerTest < Api::V2::StructuresControllerTest
    test '#index validates response' do
      get :index, all: 'true', structure: {parent_type: 'Organization', parent_id: @org.id}, limit: 15, format: :json
      response = get_json_response(@response)

      assert_equal response['structures'][0].keys.sort, ['context', 'created_at', 'creator', 'display_name', 'enabled', 'id', 'organization', 'parent', 'slug', 'updated_at']
    end

    test '#index lists all the structures of an org when `only_enabled` is set to true' do
      get :index, only_enabled: 'true', structure: {parent_type: 'Organization', parent_id: @org.id}, limit: 15, format: :json
      response = get_json_response(@response)

      assert_equal 2, response['structures'].count
      assert_equal [@structure1.id, @structure4.id].to_set, response['structures'].collect { |s| s['id'] }.to_set
    end

    test '#index lists all active structures of an org when `only_enabled` is set to false' do
      get :index, only_enabled: 'false', structure: {parent_type: 'Organization', parent_id: @org.id}, limit: 15, format: :json

      response = get_json_response(@response)

      assert_equal 3, response['structures'].count
      assert_equal [@structure1.id, @structure2.id, @structure4.id].to_set, response['structures'].collect { |s| s['id'] }.to_set
    end

    test '#index returns structures based on the value of parent_id and parent_type passed' do
      get :index, only_enabled: 'true', structure: {parent_type: 'Channel', parent_id: @channel.id}, format: :json

      response = get_json_response(@response)
      assert_equal [@structure3.id].to_set, response['structures'].collect { |s| s['id'] }.to_set
      assert_equal @channel.id, response['structures'][0]['parent']['id']
    end

    test '#index returns structures based on slug value' do
      get :index, slug: 'discover-users', only_enabled: 'true', structure: {parent_type: 'Organization', parent_id: @org.id}, format: :json
      response = get_json_response(@response)
      assert_equal 1, response['structures'].collect{|s| s['slug']}.uniq.length
      assert_equal 'discover-users', response['structures'].collect{|s| s['slug']}.uniq[0]
    end
  end

  class StructuresUpdateControllerTest < Api::V2::StructuresControllerTest

    test '#update toggles `enabled` flag' do
      put :update, id: @structure1.id, structure: { enabled: false }, format: :json

      response = get_json_response(@response)
      assert_equal @structure1.id, response['id']
      assert_equal false, response['enabled']
    end

    test '#update updates attributes' do
      # update display name for a structure
      put :update, id: @structure2.id, structure: { display_name: 'Trending cards' }, format: :json

      response = get_json_response(@response)
      @structure2.reload
      assert_equal @structure2.display_name, response['display_name']
    end

    test '#create organization custom carousel config on `enabled` flag set to true' do
      TestAfterCommit.enabled = true
      create :config, name: 'OrgCustomizationConfig', configable: @org, data_type: 'json', value: ActiveSupport::JSON.encode({"discover": {}})
      org_config = @org.configs.find_by(name: "OrgCustomizationConfig").parsed_value
      put :update, id: @structure4.id, structure: { enabled: true }, format: :json
      org_config = @org.configs.find_by(name: "OrgCustomizationConfig").parsed_value
      assert_equal @structure4.id, org_config["discover"]["discover/carousel/customCarousel/user/#{@structure4.id}"]["id"]
      assert_equal true, org_config["discover"]["discover/carousel/customCarousel/user/#{@structure4.id}"]["visible"]
    end

    test '#update organization custom carousel config on `enabled` flag switch' do
      TestAfterCommit.enabled = true
      value = {
        "version": "4.7",
        "discover": {
          "discover/carousel/customCarousel/user/#{@structure4.id}": {
            "id": @structure4.id, "defaultLabel": @structure4.display_name, "index": 0, "visible": false
          }
        }
      }
      create :config, name: 'OrgCustomizationConfig', configable: @org, data_type: 'json', value: ActiveSupport::JSON.encode(value)

      put :update, id: @structure4.id, structure: { enabled: true }, format: :json
      org_config = @org.configs.find_by(name: "OrgCustomizationConfig").parsed_value

      assert_equal @structure4.id, org_config["discover"]["discover/carousel/customCarousel/user/#{@structure4.id}"]["id"]
      assert_equal true, org_config["discover"]["discover/carousel/customCarousel/user/#{@structure4.id}"]["visible"]
    end
  end

  class StructuresDestroyControllerTest < Api::V2::StructuresControllerTest
    test '#delete deletes a structure from an org' do
      delete :destroy, id: @structure1.id, format: :json

      assert_response :success
      assert_same_elements [@structure2.id, @structure3.id, @structure4.id], @org.reload.structures.map(&:id)
    end

    test '#delete organization config custom carousel key on deleting structure' do
      value = {
        "version": "4.7",
        "discover": {
          "discover/carousel/customCarousel/user/#{@structure4.id}": {
            "id": @structure4.id, "defaultLabel": @structure4.display_name, "index": 0, "visible": false
          }
        }
      }
      create :config, name: 'OrgCustomizationConfig', configable: @org, data_type: 'json', value: ActiveSupport::JSON.encode(value)

      delete :destroy, id: @structure4.id, format: :json
      org_config = @org.configs.find_by(name: "OrgCustomizationConfig").parsed_value

      refute org_config["discover"]["discover/carousel/customCarousel/user/#{@structure4.id}"]
    end
  end

  class StructuresReorderControllerTest < Api::V2::StructuresControllerTest
    test '#reorder reorders items in a structure' do
      put :reorder, id: @structure1.id, position: 3, entity_id: @item1.entity_id, entity_type: @item1.entity_type, format: :json

      assert_response :ok
      resp = get_json_response(@response)
      assert_equal [@item2.id, @item3.id, @item1.id], resp['structured_items'].collect { |r| r['id'] }
    end
  end

  class StructuresShowControllerTest < Api::V2::StructuresControllerTest
    test '#show created structure' do
      get :show, id: @structure1.id, format: :json
      response = get_json_response(@response)
      assert_equal ["id", "display_name", "created_at", "updated_at", "enabled", "slug", "context", "organization", "parent", "creator"], response.keys
      assert_equal @user.id, response['creator']['id']
    end

    test 'return 404 if structure not found' do
      get :show, id: 0, format: :json
      assert_response :not_found
    end
  end
end

require 'test_helper'

class Api::S3UploadsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  test ':api send s3 upload config' do
    sign_in create(:user)
    get :configuration, format: :json
    assert_response :ok
    resp = ActiveSupport::JSON.decode(response.body)
    assert_not_nil resp['url']
    fields = resp['fields']
    %w[AWSAccessKeyId key policy signature success_action_status acl].each do |k|
      assert fields.has_key?(k)
    end
  end

  test 'ensure auth for config' do
    get :configuration, format: :json
    assert_response :unauthorized
  end

  class S3PublicUrlsExtractionTest < ActionController::TestCase
    setup do
      stub_request(:get, "https://bucket.s3.region.amazonaws.com/?encoding-type=url")
        .to_return(
          body: "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
            <ListBucketResult xmlns=\"http://s3.amazonaws.com/doc/2006-03-01/\">
              <Name>bucket</Name>
              <Contents>
                  <Key>image.jpeg</Key>
              </Contents>
              <Contents>
                  <Key>image1.jpeg</Key>
              </Contents>
              <Contents>
                  <Key>image2.jpeg</Key>
              </Contents>
              <Contents>
                  <Key>file.html</Key>
              </Contents>
            </ListBucketResult>"
        )

      stub_request(:get, "https://sample.s3.region.amazonaws.com/?encoding-type=url")
        .to_return(
          body: "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
            <ListBucketResult xmlns=\"http://s3.amazonaws.com/doc/2006-03-01/\">
              <Name>sample</Name>
              <Contents>
                  <Key>file.html</Key>
              </Contents>
            </ListBucketResult>"
        )

      stub_request(:head, 'https://sample.s3.region.amazonaws.com/file.html').to_return(
        headers: {"content_type": 'text/html'}
      )
      stub_request(:head, 'https://bucket.s3.region.amazonaws.com/image.jpeg').to_return(
        headers: {"content_type": 'image/jpeg'}
      )
      stub_request(:head, 'https://bucket.s3.region.amazonaws.com/image1.jpeg').to_return(
        headers: {"content_type": 'image/jpeg'}
      )
      stub_request(:head, 'https://bucket.s3.region.amazonaws.com/image2.jpeg').to_return(
        headers: {"content_type": 'image/jpeg'}
      )
      stub_request(:head, 'https://bucket.s3.region.amazonaws.com/file.html').to_return(
        headers: {"content_type": 'text/html'}
      )
      stub_request(:get, 'https://s3.amazonaws.com/').to_return(
        body: "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
          <ListAllMyBucketsResult xmlns=\"http://s3.amazonaws.com/doc/2006-03-01\">
            <Buckets>
              <Bucket>
                <Name>bucket</Name>
              </Bucket>
              <Bucket>
                <Name>sample</Name>
              </Bucket>
            </Buckets>
          </ListAllMyBucketsResult>"
      )
    end

    test ':api #get_content should have proper response bucket_info parameter' do
      info = {bucket_name: 'bucket', bucket_region: 'region'}
      sign_in create(:user)
      post :get_content, bucket_info: info, format: :json
      assert_response :ok
      resp = ActiveSupport::JSON.decode(response.body)
      keys = %w[image_links error_objects]
      assert_same_elements keys, resp.keys
      assert_equal 3, resp['image_links'].count
    end

    test ':api #get_content should have proper response for console link parameter' do
      link = 'https://s3.console.aws.amazon.com/s3/buckets/bucket/?region=region'
      sign_in create(:user)
      post :get_content, link: link, format: :json
      assert_response :ok
      resp = ActiveSupport::JSON.decode(response.body)
      keys = %w[image_links error_objects]
      assert_same_elements keys, resp.keys
      assert_equal 3, resp['image_links'].count
    end

    test ':api #get_content should have proper response for path-style link parameter' do
      link = 'http://s3-region.amazonaws.com/bucket'
      sign_in create(:user)
      post :get_content, link: link, format: :json
      assert_response :ok
      resp = ActiveSupport::JSON.decode(response.body)
      keys = %w[image_links error_objects]
      assert_same_elements keys, resp.keys
      assert_equal 3, resp['image_links'].count
    end

    test ':api #get_content should have proper response for virtual-hosted link parameter' do
      link = 'http://bucket.s3-region.amazonaws.com'
      sign_in create(:user)
      post :get_content, link: link, format: :json
      assert_response :ok
      resp = ActiveSupport::JSON.decode(response.body)
      keys = %w[image_links error_objects]
      assert_same_elements keys, resp.keys
      assert_equal 3, resp['image_links'].count
    end

    test ':api #get_content should have proper response for bucket without images' do
      info = {bucket_name: 'sample', bucket_region: 'region'}
      sign_in create(:user)
      post :get_content, bucket_info: info, format: :json
      assert_response :ok
      resp = ActiveSupport::JSON.decode(response.body)
      keys = %w[image_links error_objects]
      assert_same_elements keys, resp.keys
      assert_equal 0, resp['image_links'].count
    end

    test ':api #get_content should escaping unsafe characters' do
      S3ObjectExtractorService.any_instance
        .stubs(:extract_image_objects)
        .returns(['http://example.com/api/image 1 2 3'])
      info = {bucket: 'bucket', region: 'region'}
      data_links = S3ObjectExtractorService.new(info).send(:extract_image_objects)
      data_links = data_links.map! { |url| URI.escape(url)}
      assert_equal data_links.first, 'http://example.com/api/image%201%202%203'
    end

    test ':api #get_content should handle InvalidAccessKeyId exception' do
      client = Aws::S3::Client.new(stub_responses: true)
      client.stub_responses(
        :list_objects, ['InvalidAccessKeyId']
      )
      S3ObjectExtractorService.any_instance.stubs(:s3_client).returns(client)

      info = {bucket_name: 'exception', bucket_region: 'region'}
      sign_in create(:user)
      post :get_content, bucket_info: info, format: :json
      assert_response :unprocessable_entity
      resp = ActiveSupport::JSON.decode(response.body)

      assert_equal 'AWS access error. Invalid keys', resp['message']
    end
  end
end

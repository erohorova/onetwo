require 'test_helper'

class Api::OrganizationsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    skip #api/v1 is deprecated.
  end

  test ':api should get org details' do
    create_default_org
    org1 = create(:organization, name: 'a', host_name: 'a')
    org2 = create(:organization, host_name: 'b')
    get :show, host_name: 'a', format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal 'a', resp['name']
    assert_not_nil resp['image_url']
    assert_not_nil resp['banner_url']
    assert_equal 'http://a.test.host', resp['home_page']

    get :show, host_name: 'not-a-host-name', format: :json
    assert_response :not_found
  end

   test 'create org from api from savannah' do
    admin = create(:user, email: 'admin@course-master.com', password: User.random_password,
           first_name: 'Admin', last_name: 'User')
    sign_in admin
    assert_difference ->{User.count}, 2 do
      assert_difference ->{Organization.count} do
        post :create_from_savannah,
            :organization => {name: 'new org', description: 'description', :host_name => 'test', savannah_app_id: 12322, redirect_uri: "http://example.com/"},
            :token => Settings.savannah_token,
            :format => :json
      end
    end
    assert_equal Organization.last.savannah_app_id, 12322
    assert_equal Organization.last.redirect_uri, "http://example.com/"
  end

  test 'update existing org if org with hostname is present' do
    org = create(:organization, host_name: "test123")
    admin = create(:user, email: 'admin@course-master.com', password: User.random_password,
           first_name: 'Admin', last_name: 'User')
    assert_no_difference ->{Organization.count} do
      post :create_from_savannah,
          :organization => {name: 'test123', description: 'description', :host_name => org.host_name, savannah_app_id: 321, redirect_uri: "http://example.com/"},
          :token => Settings.savannah_token,
          :format => :json
    end
    assert_equal Organization.last.savannah_app_id, 321
  end

  test "invalid details won't create org" do
    admin = create(:user, email: 'admin@course-master.com', password: User.random_password,
           first_name: 'Admin', last_name: 'User')
    assert_no_difference ->{Organization.count} do
      post :create_from_savannah,
          :organization => {name: nil, description: 'description', :host_name => "newtest", savannah_app_id: 321, redirect_uri: "http://example.com/"},
          :token => Settings.savannah_token,
          :format => :json
      assert_response :unprocessable_entity
    end
  end

  test "return bad_request if hosname is not present" do
    admin = create(:user, email: 'admin@course-master.com', password: User.random_password,
           first_name: 'Admin', last_name: 'User')
    assert_no_difference ->{Organization.count} do
      post :create_from_savannah,
          :organization => {name: nil, description: 'description', :host_name => nil, savannah_app_id: 321, redirect_uri: "http://example.com/"},
          :token => Settings.savannah_token,
          :format => :json
      assert_response :bad_request
    end
  end


  test 'custom org request' do
    create_default_org
    new_facebook = create(:sso, name: 'new facebook')
    a = create(:organization, host_name: 'a')
    a.ssos << new_facebook
    set_host a
    get :show, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_same_elements ['email', 'google', 'linkedin', 'facebook', 'new facebook'], resp['ssos'].map{|sso| sso["name"]}
  end

  test 'retrieves active SSOs considering customised label name' do
    facebook = Sso.where(name: "facebook").first!
    saml = create(:sso, name: 'saml')
    org_oauth = create(:sso, name: 'org_oauth')
    a = create(:organization, host_name: 'a')
    saml_sso = create :org_sso, organization: a, sso: saml, label_name: 'Login with SAML credentials'
    oo_sso = create :org_sso, organization: a, sso: org_oauth, is_enabled: false

    a.reload
    set_host a
    get :show, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_same_elements resp['ssos'].collect{ |s| s['label_name'] }.sort, ['facebook', saml_sso.label_name, 'google', 'linkedin', 'email'].sort
  end

  test ':api include cobranding details' do
    create_default_org
    org1 = create(:organization, name: 'a', host_name: 'a')
    org1.configs.create(category: 'cobrand', name: 'label/insight', value: 'smartbit')
    org1.configs.create(category: 'cobrand', name: 'label/something/else', value: 'somevalue')
    get :show, host_name: 'a', format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_not_empty resp['cobrands']
    assert_equal 'smartbit', resp['cobrands']['label/insight']
    assert_equal 'somevalue', resp['cobrands']['label/something/else']
  end

  test ':api include ssos details' do
    Organization.any_instance.stubs(:default_sso).returns(true)
    org1 = create(:organization, name: 'a', host_name: 'a')
    saml = create(:sso, name: 'saml')
    facebook = Sso.where(name: "facebook").first
    org1.ssos << saml
    org1.ssos << facebook
    org_facebook = OrgSso.where(organization: org1, sso: facebook).first
    org_facebook.update!(is_enabled: false)

    get :show, host_name: org1.host_name, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal 'a', resp['name']
    assert_not_nil resp['image_url']
    assert_not_nil resp['banner_url']
    assert_equal 'http://a.test.host', resp['home_page']
    assert_not_empty resp['ssos']

    assert_includes resp['ssos'].map{|sso| sso.except("web_authentication_url")}, {"name"=>"saml",
                                   "label_name"=>"saml",
                                   "icon_url"=>"http://cdn-test-host.com/assets/sso/saml.png",
                                   "authentication_url"=>"http://#{org1.host}/auth/saml"}
    assert_equal 1, resp['ssos'].length
    Organization.any_instance.unstub(:default_sso)
  end

  test ':api non admin member should not update settings' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'member')
    config = Config.where(configable: org, category: 'settings').first!

    set_host(org)
    sign_in user

    put :set_org_settings, id: org.id, setting: {id: config.id, value: false}, format: :json

    config.reload
    assert_response :forbidden
    assert_equal true, config.parsed_value
  end

  test ':api should update org settings#permissions' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'admin')
    config = Config.where(configable: org, category: 'settings').first!

    set_host(org)
    sign_in user

    put :set_org_settings, id: org.id, setting: {id: config.id, value: false}, format: :json

    config.reload
    assert_equal false, config.parsed_value
  end

  test ':api should update the cobrand settings' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'admin')
    config = Config.create!(configable: org, category: 'cobrand', name: 'label1', value: 'oldvalue')

    set_host(org)
    sign_in user

    put :set_org_settings, id: org.id, setting: {id: config.id, value: 'newvalue'}, format: :json

    config.reload
    assert_equal 'newvalue', config.parsed_value
  end

  test ':api should update the cobrand settings #JSON value' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'admin')
    config = Config.create!(configable: org, category: 'cobrand', name: 'label1', value: {"web" => { "topMenu" => "web/topMenu/home"} }.to_json, data_type: 'json' )
    new_value = {web: { topMenu: "web/topMenu/aboutus" } }.with_indifferent_access

    set_host(org)
    sign_in user

    put :set_org_settings, setting: {id: config.id, value: new_value.to_json}, format: :json

    config.reload
    assert_equal new_value, config.parsed_value
  end

  test ':api should create a new cobrand settings' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'admin')

    set_host(org)
    sign_in user

    assert_difference ->{Config.count} do
      put :set_org_settings, id: org.id, setting: {name: 'label1', value: 'value1', category: 'cobrand'}, format: :json
    end

    resp = get_json_response(response)
    assert_equal Config.last.id, resp['id']
  end

  test ':api should update org settings#social_sharing' do
    create_default_org
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'admin')
    config = Config.where(configable: org, category: 'settings').first

    set_host(org)
    sign_in user

    put :set_org_settings, id: org.id, setting: {id: config.id, value: false}, format: :json

    config.reload
    assert_equal false, config.parsed_value
  end

  test ':api non admin member should not read org settings' do
    create_default_org
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'member')

    set_host(org)
    sign_in user

    get :read_org_settings, id: org.id, format: :json

    assert_response :forbidden
  end

  test ':api should be able to read org settings' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'admin')

    set_host(org)
    sign_in user

    channel_create_perm = 'permissions/channel/member_can_create_channel'
    share_on_social_nw = 'permissions/social_sharing/can_share_channel_on_social_nw'
    notification_enable = 'feature/notifications_and_triggers_enabled'

    get :read_org_settings, id: org.id, format: :json

    resp = get_json_response response

    assert_not_empty resp
    assert_equal true, resp.find {|s| s['name'] == channel_create_perm}['value']
    assert_equal true, resp.find {|s| s['name'] == share_on_social_nw}['value']
    assert_equal true, resp.find {|s| s['name'] == notification_enable}['value']
  end

  test ':api should be able to read org cobrand configs' do

    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'admin')

    set_host(org)
    sign_in user

    config1 = Config.create!(configable: org, category: 'cobrand', name: 'label1', value: 'value1')
    config2 = Config.create!(configable: org, category: 'cobrand', name: 'label2', value: 'value2')

    get :read_org_settings, id: org.id, category: 'cobrand', format: :json

    resp = get_json_response response

    assert_not_empty resp
    assert_equal 2, resp.length
    assert_equal true, resp.any? {|s| s['id'] == config1.id}
    assert_equal true, resp.any? {|s| s['id'] == config2.id}
  end

  # test 'should post#create organization' do
  #   org    = create(:organization)
  #   user   = create(:user, organization: org, organization_role: 'admin')

  #   set_host(org)
  #   sign_in user

  #   image_url = "http://example.com/logo.jpg"
  #   stub_request(:get, image_url).to_return( :status => 200,
  #     :body => File.read('test/fixtures/images/test.png'),
  #     :headers => {'content-type':'image/png'})

  #   post :create, organization: {is_open: true, name: "new-org", description: "Edcast 2.0 org", image: image_url, mobile_image: image_url, host_name: 'www2'}, format: :json
  #   resp = get_json_response(response)

  #   assert_not_nil resp["id"]
  #   assert_not_nil resp['home_page']
  # end

  # test 'should post#create return errors for missing params' do
  #   org    = create(:organization)
  #   user   = create(:user, organization: org, organization_role: 'admin')

  #   set_host(org)
  #   sign_in user

  #   image_url = "http://example.com/logo.jpg"
  #   stub_request(:get, image_url).to_return( :status => 200,
  #     :body => File.read('test/fixtures/images/test.png'),
  #     :headers => {'content-type':'image/png'})

  #   post :create, organization: {is_open: true, name: "new-org", description: "Edcast 2.0 org", image: image_url, mobile_image: image_url}, format: :json

  #   resp = get_json_response(response)

  #   assert_nil resp["id"]
  #   assert_not_empty resp['errors']
  #   assert_includes resp['errors'], "Host name can't be blank"
  # end


  test 'should get#setting with errors' do
    create_default_org
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'member')

    set_host(org)
    sign_in user

    get :settings, format: :json

    assert_response :not_found
  end

  test 'should get#setting form data' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'admin')

    set_host(org)
    sign_in user

    get :settings, format: :json
    resp = get_json_response(response)

    assert_not_empty resp
    assert_equal resp["id"], org.id
    assert_equal resp["name"], org.name
    assert_equal resp["description"], org.description
    assert_nil resp['co_branding_logo']
    assert_nil resp['co_branding_mobile_logo']
  end

  test 'should put#update with errors' do
    create_default_org
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'member')

    set_host(org)
    sign_in user

    put :update, id: org.id, organization: {}, format: :json

    assert_response :not_found
  end

  test ':api should put#update form data' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'admin')
    org.configs << (config = create :config)

    set_host(org)
    sign_in user

    image_url = "http://example.com/logo.jpg"
    stub_request(:get, image_url).to_return( :status => 200,
      :body => File.read('test/fixtures/images/test.png'),
      :headers => {'content-type':'image/png'})

    put :update, id: org.id, organization: { is_open: true, name: "new-name",
      description: "Edcast 2.0 org", image: image_url, mobile_image: image_url, splash_image: image_url,
      host_name: 'www3', configs_attributes: [{ id: nil, name: "content_life_span", value: "90" },
      { id: config.id, value: "100" }], show_onboarding: false},
      format: :json

    resp = get_json_response(response)
    assert_not_empty resp
    assert_equal resp["id"], org.id
    assert_equal resp["name"], 'new-name'
    assert_not_equal resp["host_name"], 'www3' #host name cannot be updated
    assert_includes resp["configs"].map{|x| x["name"]}, "content_life_span" # new config added
    assert_equal config.reload.value, "100" # update existing config

    assert_equal false, org.reload.show_onboarding
    assert_equal false, resp['show_onboarding']
    assert_equal true, resp['is_onboarding_configurable']
  end

  test 'should put#update return form data with errors' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'admin')

    set_host(org)
    sign_in user

    image_url = "http://example.com/logo.jpg"
    stub_request(:get, image_url).to_return( :status => 200,
      :body => File.read('test/fixtures/images/test.png'),
      :headers => {'content-type':'image/png'})

    put :update, id: org.id, organization: {is_open: true, name: "",
        description: "Edcast 2.0 org", splash_image: image_url, image: image_url,
        mobile_image: image_url, host_name: 'www3'},
        format: :json

    resp = get_json_response(response)

    assert_not_empty resp
    assert_equal resp["id"], org.id
    assert_not_empty resp['errors']
    assert_includes resp['errors'], "Name can't be blank"
  end

  test 'should update favicon' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'admin')

    set_host(org)
    sign_in user

    image_url = "http://example.com/logo.jpg"
    stub_request(:get, image_url).to_return( :status => 200,
      :body => File.read('test/fixtures/images/test.png'),
      :headers => {'content-type':'image/png'})

    put :update, id: org.id, organization: {  name: "new-org-name",
      image: image_url, favicon: image_url}, format: :json

    resp = get_json_response(response)
    assert_equal resp["name"], 'new-org-name'
    assert_equal resp["favicons"].keys, ["tiny", "small", "medium", "large"]
  end

  test 'should return empty object for favicons' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'admin')

    set_host(org)
    sign_in user

    image_url = "http://example.com/logo.jpg"
    stub_request(:get, image_url).to_return( :status => 200,
      :body => File.read('test/fixtures/images/test.png'),
      :headers => {'content-type':'image/png'})

    put :update, id: org.id, organization: {  name: "new-org-name",
      image: image_url}, format: :json

    resp = get_json_response(response)
    assert_equal resp["name"], 'new-org-name'
    assert_equal resp["favicons"].keys, []
  end

  test 'host name validity' do
    Settings.stubs(:disallowed_hosts).returns(['try', 'unbounce'])
    u = create(:user)
    sign_in u
    org = create(:organization, host_name: 'ge')
    get :host_name_check, host_name: 'ge'
    assert_response :unprocessable_entity

    get :host_name_check, host_name: 'www'
    assert_response :unprocessable_entity

    get :host_name_check, host_name: 'try'
    assert_response :not_acceptable

    get :host_name_check, host_name: 'www.try'
    assert_response :not_acceptable

    get :host_name_check, host_name: 'ok'
    assert_response :no_content
    Settings.unstub(:disallowed_hosts)
    sign_out u
  end

  test 'org creation failure use cases' do
    org = create(:organization, host_name: 'ge')
    SelfServeMailer.expects(:notify_staff).never
    assert_no_difference [-> {Organization.count}, ->{User.count}] do
      post :create_org_with_admin, {user: {
                                              email: 'destiny@edcast1235.com',
                                              first_name: 'test',
                                              last_name: 'last name',
                                              handle: '@testxyz',
                                              password: User.random_password
                                          },
                                    organization: {
                                              name: 'ge',
                                              host_name: 'ge',
                                              description: 'org description'
                                      }
                                    }
      assert_response :unprocessable_entity
    end
  end

  test ':api should create org with admin user' do
    mailer_mock = mock()
    mailer_mock.expects(:deliver_later).returns(nil).once
    SelfServeMailer.expects(:notify_staff).returns(mailer_mock).once
    # New admin user in new org, and edcast super admin in new org

    assert_differences [[-> {Organization.count}, 1], [->{User.count}, 2]] do
      post :create_org_with_admin, {user: {
                                              email: 'destiny@edcast1235.com',
                                              first_name: 'test',
                                              last_name: 'last name',
                                              handle: '@testxyz',
                                              password: User.random_password
                                          },
                                    organization: {
                                              name: 'org-name',
                                              host_name: 'org1234',
                                              description: 'org description'
                                      }
                                    }
    end
    org = Organization.last
    assert_response :ok
    assert_equal 'org1234', org.host_name

    user = User.last

    assert_equal 'destiny@edcast1235.com', user.email
    assert_equal false, user.confirmed?
    assert_equal true, user.is_complete?
    assert_equal org, user.organization
    assert_equal 'admin', user.organization_role
    assert_not user.is_active?
    assert_equal 2, org.users.count
  end

  test 'edcast admin should be able to activate an org' do
    org = create(:organization, status: 'pending')
    edcast_admin = User.where(organization: org, is_edcast_admin: true).first!

    mailer_mock = mock()
    mailer_mock.expects(:deliver_later).returns(nil).once
    SelfServeMailer.expects(:notify_org_admin).returns(mailer_mock).once

    admin_user = create(:user, organization: org, organization_role: 'admin', is_active: false)

    set_host(org)
    sign_in edcast_admin

    post :activate_org

    admin_user.reload
    org.reload

    assert_response :ok
    assert_equal org.home_page, get_json_response(response)['redirect_url']
    assert_equal true, admin_user.is_active
    assert_equal 'active', org.status
  end

  test 'Non edcast admin should not be able to activate org' do
    org = create(:organization, status: 'pending')
    SelfServeMailer.expects(:notify_org_admin).never

    admin_user = create(:user, organization: org, organization_role: 'admin', is_edcast_admin: false, is_active: false)
    mock_user = create(:user, organization: org, organization_role: 'admin', is_edcast_admin: false, is_active: true)

    set_host(org)
    sign_in mock_user

    post :activate_org

    admin_user.reload
    org.reload

    assert_response :unprocessable_entity
    assert_equal false, admin_user.is_active
    assert_equal 'pending', org.status
  end

  test 'should be able to toggle display of sub brand image on login page' do
    org    = create(:organization)
    user   = create(:user, organization: org, organization_role: 'admin')

    set_host(org)
    sign_in user

    image_url = "http://example.com/logo.jpg"
    stub_request(:get, image_url).to_return( :status => 200,
                                             :body => File.read('test/fixtures/images/test.png'),
                                             :headers => {'content-type':'image/png'})

    get :settings, format: :json
    resp = get_json_response(response)
    assert_equal true, resp['show_sub_brand_on_login']

    put :update, id: org.id, organization: {show_sub_brand_on_login: false}, format: :json
    resp = get_json_response(response)

    assert_not_empty resp
    assert_equal false, resp['show_sub_brand_on_login']
  end
end

require 'test_helper'

class Api::NiitControllerTest < ActionController::TestCase
  setup do
    skip if Settings.skip_tests.controllers.v1_controllers

    @org = create(:organization)
    @user = create(:user, organization: @org)
    sign_in(@user)
  end

  test 'should redirect to niit launch url' do
    NiitService.any_instance.stubs(:fetch_deeplink_url).returns('http://www.abc.com')
    get :launch, course_id: '1234', course_type: '2', domain: 'xyz'
    assert_redirected_to 'http://www.abc.com'
  end


  test 'should return not found error if launch url is blank' do
    NiitService.any_instance.stubs(:fetch_deeplink_url).returns(nil)
    get :launch, course_id: '1234', course_type: '2', domain: 'xyz'
    assert_response :not_found
    assert_includes response.body, 'Niit course deeplink generation failed for course_id: 1234, course_type: 2, please try again after sometime.'
  end
end

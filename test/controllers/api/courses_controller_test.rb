require 'test_helper'

class Api::CoursesControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  def setup
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'

    @user = create(:user)
    @org = create(:organization)
    set_host @org
    @client = create(:client, organization: @org)
  end

  test 'enroll api calls savannah enroll' do
    Group.any_instance.expects('savannah?').never
    group = create(:resource_group, organization: @org, client: @client)
    payload = {'item_id' => group.client_resource_id, 'user' => {'email' => @user.email, 'first_name' => @user.first_name, 'last_name' => @user.last_name}}
    stub_request(:post, "#{Settings.savannah_base_location}/course_enroll").
        with(body: payload.to_json).
        to_return(:status => 200, :body => "", :headers => {})

    sign_in @user
    post :enroll, id: group.client_resource_id, format: :json
    assert_response :success
  end

  test 'enroll returns savannah course id and edcast group id' do

    Group.any_instance.expects('savannah?').never
    group = create(:resource_group, organization: @org)
    payload = {'item_id' => group.client_resource_id, 'user' => {'email' => @user.email, 'first_name' => @user.first_name, 'last_name' => @user.last_name}}
    stub_request(:post, "#{Settings.savannah_base_location}/course_enroll").
        with(body: payload.to_json).
        to_return(:status => 200, :body => "", :headers => {})

    sign_in @user
    post :enroll, id: group.client_resource_id, format: :json
    assert_response :success
    json = get_json_response(response)
    assert_equal('success', json['status'])
    assert_equal(group.id, json['edcast_id'])
    assert_equal(group.client_resource_id, json['savannah_id'])

  end

  test 'enrolls user to edcast group' do
    Group.any_instance.expects('savannah?').never
    group = create(:resource_group, organization: @org)
    assert_not(group.is_member? @user)

    payload = {'item_id' => group.client_resource_id, 'user' => {'email' => @user.email, 'first_name' => @user.first_name, 'last_name' => @user.last_name}}
    stub_request(:post, "#{Settings.savannah_base_location}/course_enroll").
        with(body: payload.to_json).
        to_return(:status => 200, :body => "", :headers => {})

    sign_in @user
    post :enroll, id: group.client_resource_id, format: :json
    assert_response :success
    group.reload
    assert(group.is_member? @user)
  end

  test 'handles unauthorized requests' do
    post :enroll, id: 123, format: :json
    assert_response :unauthorized
  end

  test 'unprocessable on bad savannah response' do
    Group.any_instance.expects('savannah?').never
    group = create(:resource_group, organization: @org)
    payload = {'item_id' => group.client_resource_id, 'user' => {'email' => @user.email, 'first_name' => @user.first_name, 'last_name' => @user.last_name}}
    stub_request(:post, "#{Settings.savannah_base_location}/course_enroll").
        with(body: payload.to_json).
        to_return(:status => 500, :body => "", :headers => {})

    sign_in @user
    post :enroll, id: group.client_resource_id, format: :json
    assert_response :unprocessable_entity

  end

  test ':api courses index' do
    client = create(:client)
    org = create(:organization, client: client)
    groups = create_list(:group, 3, client: client, organization: org)
    user = create(:user, organization: org, organization_role: 'member')
    groups[0].add_admin(user)
    groups[1].add_member(user)

    set_host org
    sign_in user
    get :index, format: :json
    resp = get_json_response(response)

    assert_equal org.featured_courses.map(&:client_resource_id), resp['courses'].map { |course| course['savannah_id'] }
    assert_equal org.featured_courses.map{|c| link_course_url(id: c.id)}, resp['courses'].map{|c| c['url']}
    assert resp['courses'].select { |c| c['id'] == groups[0].id }.first['is_user_enrolled']
    assert resp['courses'].select { |c| c['id'] == groups[1].id }.first['is_user_enrolled']
    refute resp['courses'].select { |c| c['id'] == groups[2].id }.first['is_user_enrolled']
  end

  test 'courses index retrieves courses by DESC' do
    org = create(:organization)
    group1 = create :group, organization: org, created_at: Date.today, is_promoted: true
    group2 = create :group, organization: org, created_at: Date.today - 2.days, is_promoted: true

    user = create(:user, organization: org, organization_role: 'member')
    set_host org
    sign_in user
    get :index, format: :json
    resp = get_json_response(response)
    assert_equal resp['courses'].map { |course| course['id'].to_i }, [group1.id, group2.id]
  end

  test 'courses index should return count of enrolled students as well' do
    org = create(:organization)
    group1 = create :resource_group, is_promoted: true, organization: org, created_at: Time.now
    group2 = create :resource_group, is_promoted: true, organization: org, created_at: Time.now - 2.minutes
    group_users = create_list(:groups_user, 2, group: group1, role_label: 'student')
    group_users2 = create_list(:groups_user, 5, group: group2, role_label: 'student')

    user = create(:user, organization: org, organization_role: 'member')
    set_host org
    sign_in user
    get :index, format: :json
    resp = get_json_response(response)
    assert_equal [group1.enrolled_students.count, group2.enrolled_students.count], resp['courses'].map{|d| d['enrolled_students'] }
  end

  test ':api courses index should not include hidden courses' do
    org = create(:organization)

    groups1 = create_list(:group, 2, organization: org, is_promoted: true)
    groups2 = create_list(:group, 2, organization: org, is_hidden: true)
    user = create(:user, organization: org, organization_role: 'member')
    set_host org
    sign_in user
    get :index, format: :json
    resp = get_json_response(response)
    assert_same_elements groups1.map(&:client_resource_id), resp['courses'].map { |course| course['savannah_id'] }
    assert_empty groups2.map(&:client_resource_id) & resp['courses'].map { |course| course['savannah_id'].to_i }
  end

  test 'to search featured courses based on query param' do
    org = create(:organization)

    group1 = create(:resource_group, name: 'Big Data in Food business', organization: org, is_promoted: true)
    group2 = create(:resource_group, name: 'Ruby on Rails', organization: org, is_promoted: true)
    group3 = create(:resource_group, name: 'React JS', organization: org, is_promoted: true)
    group4 = create(:resource_group, name: 'React and Redux', organization: org, is_promoted: true)
    user = create(:user, organization: org, organization_role: 'member')
    set_host org
    sign_in user
    get :index, format: :json, :query => 'food'
    resp = get_json_response(response)
    assert_equal [group1.id], resp['courses'].map{|d| d['id']}

    get :index, format: :json, :query => 'rai'
    resp = get_json_response(response)
    assert_equal [group2.id], resp['courses'].map{|d| d['id']}

    get :index, format: :json, :query => 'reac'
    resp = get_json_response(response)
    assert_equal 2, resp['courses'].count
    assert_same_elements [group3.id, group4.id], resp['courses'].map{|d| d['id']}
  end

  test ':api courses index should not show courses enrolled by user when params include_enrolled is passed with false' do
    cm_admin = create :user, email: 'admin@course-master.com'
    client = create(:client, user: cm_admin)
    org = create(:organization, client: client)
    group1, group2 = create_list(:resource_group, 2, organization: org, is_promoted: true)
    user = create(:user, organization: org, organization_role: 'member')
    group1.add_member user
    set_host org
    sign_in user
    get :index, format: :json, :include_enrolled => 'false'

    assert_equal assigns(:courses), [group2]
    assert_equal assigns(:courses).count, 1
  end

  test ':api courses index should not show courses enrolled by user when params include_enrolled isnt passed' do
    cm_admin = create :user, email: 'admin@course-master.com'
    client = create(:client, user: cm_admin)
    org = create(:organization, client: client)

    group1 = create(:resource_group, organization: org, created_at: 1.day.ago, is_promoted: true)
    group2 = create(:resource_group, organization: org, is_promoted: true)

    user = create(:user, organization: org, organization_role: 'member')
    group1.add_member user

    set_host org
    sign_in user
    get :index, format: :json

    assert_equal assigns(:courses), [group2, group1]
    assert_equal assigns(:courses).count, 2
  end

  test '#get_last_accessed' do
    # Setup resource groups and users
    resource_group = create :resource_group, organization: @org
    student_user = create :user

    sign_in student_user

    # Set expectations
    instance = SavannahService::LastAccessed.new(resource_group)
    SavannahService::LastAccessed.expects(:new).with(resource_group).returns(instance)

    SavannahService::LastAccessed.any_instance().expects(:get_last_accessed)
        .with(student_user).returns([:ok, {'content' => '#get_last_accessed'}])

    # Run request and check if we're calling the service as required
    get :get_last_accessed, id: resource_group.id, format: :json
    resp = get_json_response response

    assert_response :success
    assert_equal '#get_last_accessed', resp['content']
  end

  test '#set_last_accessed' do
    # Setup resource groups and users
    resource_group = create :resource_group, organization: @org
    student_user = create :user

    sign_in student_user

    # Set expectations
    instance = SavannahService::LastAccessed.new(resource_group)
    SavannahService::LastAccessed.expects(:new).with(resource_group).returns(instance)

    SavannahService::LastAccessed.any_instance().expects(:set_last_accessed)
        .with(student_user, 'LAST_MODULE').returns([:ok, {'content' => '#set_last_accessed'}])

    # Run request and check if we're calling the service as required
    put :set_last_accessed, id: resource_group.id, last_module_id: 'LAST_MODULE', format: :json
    resp = get_json_response response

    assert_response :success
    assert_equal '#set_last_accessed', resp['content']
  end

  test '#course_progress' do
    # Setup resource groups and users
    resource_group = create :resource_group, organization: @org
    student_user = create :user

    sign_in student_user

    # Set expectations
    instance = SavannahService::CourseProgress.new(resource_group)
    SavannahService::CourseProgress.expects(:new).with(resource_group).returns(instance)

    SavannahService::CourseProgress.any_instance().expects(:get_course_progress)
        .with(student_user).returns([:ok, {'content' => '#get_course_progress'}])

    # Run request and check if we're calling the service as required
    get :course_progress, id: resource_group.id, format: :json
    resp = get_json_response response

    assert_response :success
    assert_equal '#get_course_progress', resp['content']
  end

  class CourseStructureActionTest < ActionController::TestCase
    include Devise::Test::ControllerHelpers

    setup do
      # create_default_org
      @org      = create :organization
      @user_1   = create :user, organization: @org
      @user_2   = create :user, organization: @org
      @edx_json = File.read("#{Rails.root}/test/fixtures/api/edx_course_structure_response.json")
      @s3_json  = File.read("#{Rails.root}/test/fixtures/api/s3_course_structure.json")
      @client   = create(:client, organization: @org)
      @group    = create :resource_group, organization: @org, course_data_url: 'http://s3-url.com/text', start_date: "2016-05-23", client: @client
      @group.admins << @user_1
      @config   = create :config, configable_id: @client.id, configable_type: 'Client', name: 'app.config.dogwood_release', value: 'true'
      @group.reload
      set_host @org
      sign_in @user_1
    end

    test ':api returns Edx course structure properly' do
      ResourceGroup.any_instance.stubs(:get_cache_course_structure).returns({
        status: 'success',
        course_structure: JSON.parse(@edx_json)
        }.merge!(@group.send(:course_hashed_data)))
      get :course_structure, id: @group.id, format: :json

      body = get_json_response response
      assert_response :success
      assert body['course_structure']
      assert_equal body['dogwood_release'], true
      assert_match /success/, body['status']

      assert_match body['start_date'], '2016-05-23'
      assert_match body['course_organization'], @client.name
      assert_match body['course_description'], @group.description
      assert_match body['course_title'], @group.name
      assert_match body['course_website_url'], @group.website_url
      assert_equal body['course_image'], @group.image_url
      assert_includes body['instructors'].map { |i| i['name'] }, @user_1.name
    end

    test 'returns errors if Savannah API fails' do
      ResourceGroup.any_instance.stubs(:get_cache_course_structure).returns(nil)

      get :course_structure, id: @group.id, format: :json

      body = get_json_response response
      assert_response :unprocessable_entity
      assert_match /error/, body['status']
      assert_match /Something went wrong/, body['msg']
    end

    test 'unauthorizes when user isnt a group-user' do
      sign_in @user_2

      get :course_structure, id: @group.id, format: :json
      assert_response :unauthorized
    end
  end
end

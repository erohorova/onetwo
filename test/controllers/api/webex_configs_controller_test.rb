require 'test_helper'

class Api::WebexConfigsControllerTest < ActionController::TestCase
  # include Devise::Test::ControllerHelpers
  setup do
    @client = create(:client)
    @org = create(:organization, client: @client)
    @api_key = @client.valid_credential.api_key
    @secret = @client.valid_credential.shared_secret
  end

  test "should save webex config properly" do
    name = "test"
    site_name = "apidemoeu"
    site_id = "123"
    partner_id = "123"
    app_id = 123


    wc_params = {
                 name: name,
                 site_name: site_name,
                 site_id: site_id,
                 partner_id: partner_id,
                 app_id: app_id,
                }

    token = JWT.encode(wc_params, @secret,'HS256')
    request.headers['X-API-KEY'] = @api_key
    request.headers['X-API-TOKEN'] = token

    assert_difference ->{WebexConfig.count}, 1 do
      post :create, wc_params
      assert_response :success
    end
    #should not create new record
    assert_difference ->{WebexConfig.count}, 0 do
      post :create, wc_params
      assert_response :success
    end

    wc_params = {
                 name: "12333",
                 site_name: site_name,
                 site_id: site_id,
                 partner_id: partner_id,
                 app_id: app_id,
                }

    request.headers['X-API-KEY'] = @api_key
    request.headers['X-API-TOKEN'] = "somerandomtoken"
    post :create, wc_params
    assert_response :unauthorized
  end

  test 'fetch get_webex_credentials' do
    app_id = 123
    config = create(:webex_config, organization: @org)
    token = JWT.encode({app_id: app_id}, @secret,'HS256')
    request.headers['X-API-KEY'] = @api_key
    request.headers['X-API-TOKEN'] = token

    get :get_webex_credentials, {app_id: app_id, format: :json}
    resp = get_json_response(response)
    assert_response :success
    assert_equal resp["site_id"], config.site_id
    assert_equal resp["site_name"], config.site_name
    request.headers['X-API-KEY'] = @api_key
    request.headers['X-API-TOKEN'] = "somerandomtoken"
    #without api_key
    get :get_webex_credentials, app_id: app_id,format: :json
    assert_response :unauthorized
  end
end

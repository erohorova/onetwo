require 'test_helper'

class Api::ScormControllerTest < ActionController::TestCase
  setup do
    @org = create(:organization)
    @user = create(:user, organization: @org)

    @user_context = JWT.encode({user_id: @user.id}, Settings.scorm.user_context_secret, 'HS256')
    Settings.expects(:platform_domain).returns('edcast.com').at_least(0)
  end

  test 'should redirect to scorm launch url' do
    test_xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n<rsp stat=\"ok\"><registrationreport format=\"course\" regid=\"72826-SequencingSimpleRemediation_SCORM20043rdEdition18cd465d-7c3c-4647-a3aa-50e5833e5161\" instanceid=\"0\"><complete>unknown</complete><success>unknown</success><totaltime>1538</totaltime><score>unknown</score></registrationreport></rsp>"

    service = mock()
    ScormCloud::ScormCloud.any_instance.stubs(:registration).returns(service)
    service.expects(:get_registration_result).returns(test_xml).once
    service.expects(:launch).returns("www.example.com").once

    get :launch, course_id: "213134", user_context: @user_context
    assert_includes response.redirect_url, "www.example.com"
  end

  test "should not hit registration on scorm if already registered" do
    test_xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n<rsp stat=\"ok\"><registrationreport format=\"course\" regid=\"72826-SequencingSimpleRemediation_SCORM20043rdEdition18cd465d-7c3c-4647-a3aa-50e5833e5161\" instanceid=\"0\"><complete>unknown</complete><success>unknown</success><totaltime>1538</totaltime><score>unknown</score></registrationreport></rsp>"

    service = mock()
    ScormCloud::ScormCloud.any_instance.stubs(:registration).returns(service)
    service.expects(:get_registration_result).returns(test_xml).once
    service.expects(:create_registration).never
    service.expects(:launch).returns("www.example.com").once

    get :launch, course_id: "213134", user_context: @user_context
    assert_response 302
    assert_includes response.redirect_url, "www.example.com"
  end

  test "should register on scorm if not already registered" do
    fail_xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><rsp stat=\"fail\"><err code=\"1\" msg=\"Could not find registration [72826-SequencingSimpleRemediation_SCORM20043rdEdition18cd465d-7c3c-4647-a3aa-50e5833e5161sss] associated with appid [L4NF4NVD5S]\" ></err></rsp>"

    service = mock()
    ScormCloud::ScormCloud.any_instance.stubs(:registration).returns(service)
    service.expects(:get_registration_result).returns(fail_xml).once
    service.expects(:create_registration).returns(true).once
    service.expects(:launch).returns("www.example.com").once

    get :launch, course_id: "213134", user_context: @user_context
    assert_response 302
  end

  test "lauch with organization url" do
    test_xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n<rsp stat=\"ok\"><registrationreport format=\"course\" regid=\"72826-SequencingSimpleRemediation_SCORM20043rdEdition18cd465d-7c3c-4647-a3aa-50e5833e5161\" instanceid=\"0\"><complete>unknown</complete><success>unknown</success><totaltime>1538</totaltime><score>unknown</score></registrationreport></rsp>"

    service = mock()
    ScormCloud::ScormCloud.any_instance.stubs(:registration).returns(service)
    service.expects(:get_registration_result).returns(test_xml).once
    service.expects(:launch).with("#{@user.id}-#{213134}", "#{@org.home_page}/api/scorm/close").returns("www.example.com").once

    get :launch, course_id: "213134", user_context: @user_context
    assert_response 302
  end

  test "should return error page if failed" do
    service = mock()
    ScormCloud::ScormCloud.any_instance.stubs(:registration).returns(service)

    get :launch, course_id: "213134"
    assert_response 404
  end

  test 'should complete card if scorm course completed' do
    create(:config, name: 'auto_mark_as_complete_scorm_cards', value: true, configable: @org)
    card = create(:card, organization: @org, author: @user)
    completion_response = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n<rsp stat=\"ok\">
    <registrationreport format=\"course\" regid=\"#{@user.id}-#{@org.host_name}#{card.id}\" instanceid=\"0\"><complete>complete</complete><success>unknown</success>
    <totaltime>6</totaltime><score>unknown</score></registrationreport></rsp>"

    assert_difference -> { UserContentCompletion.count }, 1 do
      post :completion, data: completion_response
      assert_equal 1, UserContentCompletion.count
      ucc = UserContentCompletion.last
      assert_equal ucc.completable_id, card.id
      assert_equal ucc.user_id, @user.id
    end
  end

  private

  def sample_scorm_url(course_id, user_id)
    resource = build :resource, url: "https://www.edcast.com/api/scorm/launch?course_id=#{course_id}"
    resource.url_with_context(user_id)
  end
end

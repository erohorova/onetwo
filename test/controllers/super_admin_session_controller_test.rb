require 'test_helper'

class SuperAdminSessionControllerTest < ActionController::TestCase

# -----------------------------------------------------------------------------
# COMMENTED OUT UNTIL WE FIX BROWSERIFY IN TEST ENVIRONMENT
# -----------------------------------------------------------------------------

=begin

  test 'should render the input form only on default org' do
    user = create(:user, :admin)
    sign_in user
    get :new
    assert_response :success
  end

=end
  setup do
    create_default_org
  end

  test 'should render the hidden form with the jwt token' do
    user = create(:user, :admin, organization: Organization.default_org)
    sign_in user
    org = create(:organization)
    post :submit, host_name: org.host_name
    assert_response :success
  end

  test 'should allow the current user to become the admin within requested org' do
    Organization.any_instance.unstub :create_admin_user

    user = create(:user, :admin, organization: Organization.default_org)
    org = create(:organization)
    exp = Time.now.to_i + 10 #must be redeemed within 10 seconds
    exp_payload = { :data => ActiveSupport::JSON.encode({'organization_id' => org.id, 'user_id' => user.id}),
                    :exp => exp }
    admin_jwt_token = JWT.encode(exp_payload, Edcast::Application.config.secret_key_base)


    set_host(org)
    assert_difference ->{SuperAdminAuditLog.count} do
      post :create, admin_jwt_token: admin_jwt_token
    end
    assert_redirected_to root_path
  end
end
require 'test_helper'

class WelcomeControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers
  setup do
    skip
  end

  test 'render homepage from wordpress site' do
    wordpress_response = '<html><head></head><body></body></html>'
    # https://www.edcast.com/corp/
    stub_request(:get, "#{Settings.edcast_wordpress_url}/corp/").
      to_return(:status => 200, :body => wordpress_response, headers: {})

    get :index
    assert_response :success
    assert_equal response.body, wordpress_response
  end

  test 'homepage should redirect to login page for custom org' do
    create_default_org
    org = create(:organization)
    set_host org

    get :index
    assert_response :moved_permanently
    assert_redirected_to new_user_session_path({from_root: true})
  end

end

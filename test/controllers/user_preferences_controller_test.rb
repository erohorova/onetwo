require 'test_helper'

class UserPreferencesControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
  end

  test 'should return the current user preferences' do
    user = create(:user)
    random_user = create(:user)
    group = create(:group)
    random_group = create(:group)

    create(:user_preference, user: user, preferenceable: group,
           key: 'notification.announcement.opt_out', value: 'true'
    )
    create(:user_preference, user: user, preferenceable: group,
           key: 'notification.post.opt_out', value: 'true'
    )
    create(:user_preference, user: user, preferenceable: group,
           key: 'notification.group_welcome.opt_out', value: 'true'
    )

    sign_in user

    get :index, preferenceable_type: 'Group', preferenceable_id: group.id, format: :json

    assert_equal 3, get_json_response(response).length

  end

  test 'should update the preference by creating a new one' do
    user = create(:user)
    sign_in user

    group = create(:group)
    assert_difference -> {UserPreference.count} do
      patch :update, key: 'notification.post.opt_out', value: 'true', preferenceable_type: 'Group', preferenceable_id: group.id, format: :json
    end
    assert_response :success
  end

  test 'should update the existing' do
    user = create(:user)
    sign_in user
    group = create(:group)
    preference1 = create(:user_preference, user: user, preferenceable: group,
                        key: 'notification.announcement.opt_out', value: 'true')
    assert_no_difference -> {UserPreference.count} do
      patch :update, key: 'notification.announcement.opt_out', value: 'false', preferenceable_type: 'Group', preferenceable_id: group.id, format: :json
    end
    assert_response :success
    assert_equal 'false', preference1.reload.value
  end

  test 'should set from token' do
    user = create(:user)
    group = create(:group)

    preference_token = ActiveSupport::MessageEncryptor.
      new(Edcast::Application.config.secret_key_base).
      encrypt_and_sign(ActiveSupport::JSON.encode({user_id: user.id,
                                                   key: 'notification.announcement.opt_out',
                                                   value: 'true', preferenceable_type: 'Group',
                                                   preferenceable_id: group.id})).gsub('=', '_')

    assert_difference -> {UserPreference.count} do
      get :set_from_token, token: preference_token
    end
    assert UserPreference.find_by(user_id: user.id, key: 'notification.announcement.opt_out', value: 'true', preferenceable_type: 'Group', preferenceable_id: group.id)
    assert_response :success

  end

  test 'should set from token with old cgi escape' do
    user = create(:user)
    group = create(:group)

    preference_token = CGI::escape(ActiveSupport::MessageEncryptor.
        new(Edcast::Application.config.secret_key_base).
        encrypt_and_sign(ActiveSupport::JSON.encode({user_id: user.id,
                                                     key: 'notification.announcement.opt_out',
                                                     value: 'true', preferenceable_type: 'Group',
                                                     preferenceable_id: group.id})))

    assert_difference -> {UserPreference.count} do
      get :set_from_token, token: preference_token
    end
    assert UserPreference.find_by(user_id: user.id, key: 'notification.announcement.opt_out', value: 'true', preferenceable_type: 'Group', preferenceable_id: group.id)
    assert_response :success

  end

  test 'should not blow up if there is a bad token' do
    assert_nothing_raised do
      get :set_from_token
      assert_response :not_found
    end

  end

  test 'should not set from token if it was already set' do


    user_preference = create(:user_preference)
    preference_token = ActiveSupport::MessageEncryptor.
      new(Edcast::Application.config.secret_key_base).
      encrypt_and_sign(ActiveSupport::JSON.encode({user_id: user_preference.user.id, key: user_preference.key,
                                                   value: user_preference.value, preferenceable_type: user_preference.preferenceable_type,
                                                   preferenceable_id: user_preference.preferenceable_id})).gsub('=', '_')

    assert_no_difference -> {UserPreference.count} do
      get :set_from_token, token: preference_token
    end
    assert_response :success
  end

  test 'should not raise an exception if token is not passed in' do
    assert_nothing_raised do
      get :set_from_token
    end
  end

  test 'updating preferences from token for non-preferenceable keys' do
    user = create(:user)

    preference_token = ActiveSupport::MessageEncryptor.
        new(Edcast::Application.config.secret_key_base).
        encrypt_and_sign(ActiveSupport::JSON.encode({user_id: user.id, key: 'notification.follower.opt_out',
                                                     value: 'true'})).gsub('=', '_')
    assert_difference -> {UserPreference.count} do
      get :set_from_token, token: preference_token
    end
    assert UserPreference.find_by(user_id: user.id, key: 'notification.follower.opt_out', value: 'true')
    assert_response :success

  end
end

require 'test_helper'

class BackgroundJobsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  class DummyJob < EdcastActiveJob
    def perform
    end
  end

  setup do
    EdcastActiveJob::WHITELISTED_JOB_CLASS << "BackgroundJobsControllerTest::DummyJob"

    EdcastActiveJob.unstub(:perform_later)
    DummyJob.unstub :perform_later
    create_default_org
    @admin_user = create(:user, email: "admin@edcast.com", organization: Organization.default_org)
    sign_in @admin_user
  end

  test 'index' do
    assert_difference ->{BackgroundJob.count} do
      DummyJob.perform_later
    end
    get :index
    assert_response :ok
  end

  test 'reenqueue' do
    assert_difference ->{BackgroundJob.count} do
      DummyJob.perform_later
    end
    bk_job = BackgroundJob.last
    post :reenqueue, id: bk_job.job_id, format: :json
    assert_response :ok
  end

end


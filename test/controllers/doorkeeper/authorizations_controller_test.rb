require 'test_helper'

class Doorkeeper::AuthorizationsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'

    @client = create(:client)
    @credential = @client.valid_credential
    @user = create(:user)
  end

# -----------------------------------------------------------------------------
# COMMENTED OUT UNTIL WE FIX BROWSERIFY IN TEST ENVIRONMENT
# -----------------------------------------------------------------------------

=begin

  test 'render the authorization page' do
    sign_in @user
    get :new, redirect_uri: @client.redirect_uri, client_id: @credential.api_key, scope: 'publish', response_type: 'code'
    assert_response :success

    get :new, redirect_uri: @client.redirect_uri, client_id: @credential.api_key, scope: 'publish', response_type: 'code', display: 'popup'
    assert_response :success
  end

=end

end

require 'test_helper'

class GroupsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    @org = create(:organization)
    @client = create(:client, organization: @org)
    @group = create(:resource_group, organization: @org, client: @client)
    @user = create(:user, organization: @org)
    @group.add_member @user

    @faculty = create(:user, organization: @org)
    @group.add_admin @faculty

    # stub activity logging on savannah
    stub_request(:post, /#{Settings.savannah_base_location}.*/).
      to_return(:status => 200, :body => "", :headers => {})

    @content = {
      meta_savannah: {
        name: "course name",
        description: "course description",
        website_url: "website url",
        logo_url: "logo url",
        organization: "some university",
        start_date: 2.days.ago.strftime('%Y-%m-%d'),
        term: "Fall 2015",
        instructors: [
          {
            'name' => "Jesse Pinkman",
            'image' => "http://someimage.png",
            'picture' => "http://someimage.png"
          },
          {
            'name' => "Walter White",
            'image' => "http://iamtheonewhoknocks.png",
            'picture' => "http://iamtheonewhoknocks.png"
          }
        ],
      },
      chapters: [
        {
          chapter_title: "chapter1",
          sequentials: [
            {
              sequential_title: "sequential1",
              verticals: [
                {
                  vertical_title: "Video",
                  videos: [
                    {
                      youtube: "v1",
                      youtube_title: "v1 title",
                      'video_id' => 'v1',
                      'video_title' => 'v1 title',
                      'video_source' => 'Youtube',
                      'video_thumbnail' => 'http://someimage.png'
                    },
                    {
                      youtube: "v2",
                      youtube_title: "v2 title",
                      'video_id' => 'v2',
                      'video_title' => 'v2 title',
                      'video_source' => 'Youtube',
                      'video_thumbnail' => 'http://someimage.png'
                    }
                  ],
                  static: [
                    {
                      static_title: 'First.pdf',
                      static_url: 'https://cloudfront.com/First.pdf'
                    },
                    {
                      static_title: 'First.ppt',
                      static_url: 'https://cloudfront.com/First.ppt'
                    },
                  ],
                },
                {
                  vertical_title: "something",
                  videos: [
                    {
                      youtube: "v3",
                      youtube_title: "v3 title",
                      'video_id' => 'v3',
                      'video_title' => 'v3 title',
                      'video_source' => 'Youtube',
                      'video_thumbnail' => 'http://someimage.png'
                    },
                  ],
                  static: [
                    {
                      static_title: 'Second.pdf',
                      static_url: 'https://cloudfront.com/Second.pdf'
                    },
                    {
                      static_title: 'Second.ppt',
                      static_url: 'https://cloudfront.com/Second.ppt'
                    },
                  ],
                }
              ]
            },
            {
              sequential_title: "sequential2",
              verticals: [
                {
                  vertical_title: "video3",
                  videos: [
                    {
                      youtube: "v4",
                      youtube_title: "v4 title",
                      'video_id' => 'v4',
                      'video_title' => 'v4 title',
                      'video_source' => 'Youtube',
                      'video_thumbnail' => 'http://someimage.png'
                    }
                  ],
                  vimeo_videos: [
                    {
                      'video_id' => 'vimeo-v1',
                      'video_title' => 'vimeo title',
                      'video_source' => 'Vimeo',
                      'video_thumbnail' => 'http://somevimeo.png'
                    }
                  ],
                  brightcove_videos: [
                    {
                      'video_id' => 'http://bcove.me/hc8szal5vvcc',
                      'video_title' => 'Brightcove title',
                      'video_source' => 'Brightcove',
                      'video_thumbnail' => nil
                    }
                  ],
                  static: [
                    {
                      static_title: 'Third.pdf',
                      static_url: 'https://cloudfront.com/Third.pdf'
                    },
                    {
                      static_title: 'Third.ppt',
                      static_url: 'https://cloudfront.com/Third.ppt'
                    },
                  ],
                }
              ]
            }
          ]
        },
        {
          chapter_title: "chaper2",
          sequentials: []
        }
      ]
    }

    stub_request(:get, 'http://url.com').
        with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
        to_return(:status => 200,
                  :body => @content.to_json,
                  :headers => {'Content-Type' => 'application/json'},
    )
  end

  test 'should send status forbidden if user not signed in' do
    get :tags, id: @group.id, :format => :json
    assert_response :unauthorized

    get :stats, id: @group.id, :format => :json
    assert_response :unauthorized

    get :right_rail_stats, id: @group.id, format: :json
    assert_response :unauthorized

    get :topic_stats, id: @group.id, topic_type: 'tag', topic_id: 'hw1', format: :json
    assert_response :unauthorized

    get :course_data, id: @group.id, format: :json
    assert_response :unauthorized

  end

  test ":api should get tags" do
    @group.created_at = 1.day.ago
    @group.save
    @q_hw1 = create(:question, postable: @group, user: @user,
                     created_at: 1.hour.ago,
                     title: 'title', message: 'this is #hw1')
    @q_hw2 = create(:question, postable: @group, user: @user,
                     created_at: 1.hour.ago,
                     title: 'title', message: 'this is #hw2')
    @another_q_hw1 = create(:question, postable: @group, user: @user,
                            created_at: 1.hour.ago,
                            title: 'title', message: 'this is #hw1 again')

    create(:comment, user: @user, commentable: @another_q_hw1,
           message: "another #comment")
    create(:comment, user: @user, commentable: @another_q_hw1,
           message: "#comment")

    sign_in @user
    get :tags, id: @group.id, format: :json
    assert_response :success
    assigned_tags = assigns(:tags)

    assert_equal assigned_tags[0]['tag_name'], 'hw1'
    assert_equal assigned_tags[0]['tag_count'], 2
    assert_equal assigned_tags[1]['tag_name'], 'comment'
    assert_equal assigned_tags[1]['tag_count'], 2
    assert_equal assigned_tags[2]['tag_name'], 'hw2'
    assert_equal assigned_tags[2]['tag_count'], 1

    response_body = DeepSnakeCaser.deep_snake_case!(ActiveSupport::JSON.decode response.body)
    assert_equal response_body[0]['tag_name'], 'hw1'
    assert_equal response_body[0]['tag_count'], 2
    assert_equal response_body[1]['tag_name'], 'comment'
    assert_equal response_body[1]['tag_count'], 2
    assert_equal response_body[2]['tag_name'], 'hw2'
    assert_equal response_body[2]['tag_count'], 1
    sign_out @user
  end

  test ":api should get top tags" do
    @group.created_at = 1.day.ago
    @group.save
    @q_hw1 = create(:question, postable: @group, user: @user,
                     created_at: 1.hour.ago,
                     title: 'title', message: 'this is #hw1')
    @onemore_q_hw1 = create(:question, postable: @group, user: @user,
                     created_at: 1.hour.ago,
                     title: 'title', message: 'this is #hw1')
    @q_hw2 = create(:question, postable: @group, user: @user,
                     created_at: 1.hour.ago,
                     title: 'title', message: 'this is #hw2')
    @another_q_hw1 = create(:question, postable: @group, user: @user,
                            created_at: 1.hour.ago,
                            title: 'title', message: 'this is #hw1 again')
    @pandora1 = create(:question, postable: @group, user: @user,
                     created_at: 1.hour.ago,
                     title: 'title', message: 'this is #pandora')
    @pandora2 = create(:question, postable: @group, user: @user,
                     created_at: 1.hour.ago,
                     title: 'title', message: 'this is #pandora')
    @pandora3 = create(:question, postable: @group, user: @user,
                     created_at: 1.hour.ago,
                     title: 'title', message: 'this is #pandora')
    @pandora4 = create(:question, postable: @group, user: @user,
                     created_at: 1.hour.ago,
                     title: 'title', message: 'this is #pandora')

    create(:comment, user: @user, commentable: @another_q_hw1,
           message: "another #comment")
    create(:comment, user: @user, commentable: @another_q_hw1,
           message: "#comment")
    create(:comment, user: @user, commentable: @another_q_hw1,
           message: "one more #comment")
    create(:comment, user: @user, commentable: @another_q_hw1,
           message: "#comment again")

    sign_in @user
    get :top_tags, id: @group.id, format: :json
    assert_response :success

    response_body = DeepSnakeCaser.deep_snake_case!(ActiveSupport::JSON.decode response.body)
    assert_equal response_body[0]['tag_name'], 'pandora'
    assert_equal response_body[0]['tag_count'], 4
    assert_equal response_body[1]['tag_name'], 'comment'
    assert_equal response_body[1]['tag_count'], 4
    assert_equal response_body[2]['tag_name'], 'hw1'
    assert_equal response_body[2]['tag_count'], 3
    sign_out @user
  end

  test 'bad group id parameter' do
    sign_in @user
    get :tags, id: 'x', format: :json
    assert_response :bad_request

    get :stats, id: 'x', format: :json
    assert_response :bad_request

    get :right_rail_stats, id: 'x', format: :json
    assert_response :bad_request

    get :topic_stats, id: 'x', format: :json
    assert_response :bad_request
    sign_out @user
  end

  class SearchTest < ActionController::TestCase
    include Devise::Test::ControllerHelpers
    self.use_transactional_fixtures = false
    setup do
      skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
      WebMock.disable!
      if Group.searchkick_index.exists?
        Group.searchkick_index.delete
      end
      Group.reindex
      Group.searchkick_index.refresh
    end

    teardown do
      WebMock.enable!
    end

    test 'show search results' do
      get :search, format: :json
      assert_response :unauthorized

      u = create(:user)
      sign_in u
      get :search, format: :json
      assert_response :ok
      resp = get_json_response response
      assert_equal [], resp

      robotics_groups = []
      (1..5).each {|i| robotics_groups << create(:group, name: "robotics #{i}")}
      robotics_groups.last.update_attributes(course_term: "Fall")
      g = create(:group, name: 'some other name')

      # a private group now
      pg = create(:private_group, resource_group: g, name: 'robotics private')

      Group.all.map(&:reindex)
      Group.searchkick_index.refresh

      get :search, q: 'robotics', format: :json
      assert_response :ok
      resp = get_json_response response
      assert_equal robotics_groups.map(&:id), resp.map{|r| r['id']}
      assert_equal robotics_groups.map(&:course_term), resp.map{|r| r['term']}

      get :search, q: 'fsdfsdf', format: :json
      assert_response :ok
      resp = get_json_response response
      assert_equal [], resp

      get :search, q: 'robotics', limit: 2, offset: 2, format: :json
      assert_response :ok
      resp = get_json_response response
      assert_equal 2, resp.count
      sign_out u
    end

  end

  test 'data dump fails with no group or bad format' do
    admin = create(:user, email: 'b@course-master.com')
    create_client_with_user(admin)
    post :data_dump, {client_id: @client1.id, id: @group1.id, format: 'xxx'}
    assert_response :unprocessable_entity
    post :data_dump, {client_id: @client1.id, id: @group1.id, format: 'XML'}
    assert_response :ok
  end

  def create_client_with_user(user, group_opts: {})

    sign_in user
    @org1 = user.organization
    @client1 = create(:client, user: user, organization: @org1)
    @group1 = create(:group, group_opts.merge(organization: @org1))
  end

  test 'data dump should only work for group owner' do

    admin = create(:user, email: 'b@course-master.com')
    client = create(:client, user: admin)
    org = create(:organization, client: client)
    group = create(:group, organization: org)
    user = create(:user)

    sign_in user
    get :data_dump, {client_id: client.id, id: group.id, format: 'JSON'}
    assert_response :forbidden
    sign_out user
  end

  test 'index should send unauthorized if nobody signed in' do
    admin = create(:user, email: 'b@course-master.com')
    client = create(:client, user: admin)
    group = create(:group, client: client)
    get :index, {client_id: client.id, format: :json}
    assert_response :unauthorized
  end

  test 'index should work if admin signed in' do
    admin = create(:user, email: 'b@course-master.com')
    create_client_with_user(admin)
    private_group = create(:group, organization: @org1, client_resource_id: '2', type: 'PrivateGroup')
    get :index, {client_id: @client1.id, format: :json}
    assert_response :ok
    resp = get_json_response response
    assert_equal resp.size, 1
    assert_equal resp[0]['id'], @group1.id
  end

  test 'index should fail if someone not admin of group' do
    admin = create(:user, email: 'a@course-master.com')
    not_admin = create(:user, email: 'b@course-master.com')
    invited_admin = create(:user, email: 'c@course-master.com')
    client = create(:client, user: admin)
    organization = create(:organization, client:client)
    client.admins << invited_admin
    group = create(:group, client: client)
    sign_in not_admin

    get :index, {client_id: client.id, format: :json}
    assert_response :forbidden
    sign_out not_admin

    sign_in invited_admin
    get :index, {client_id: client.id, format: :json}
    assert_response :ok
    sign_out invited_admin
  end

  test 'update should redirect if nobody signed in' do
    admin = create(:user, email: 'b@course-master.com')
    client = create(:client, user: admin)
    group = create(:group, client: client)
    post :update, {group: {website_url: 'test_url'}, id: group.id, client_id: client.id}
    assert_response :redirect
  end

  test 'update should work if admin signed in' do
    admin = create(:user, email: 'b@course-master.com')
    create_client_with_user(admin)
    post :update, {
      group: {
        website_url: 'test_url',
        name: 'new name',
        details_locked: true,
        close_access: true
      },
      id: @group1.id,
      client_id: @client1.id
    }
    assert_response :ok
    @group1.reload
    assert_equal @group1.website_url, 'test_url'
    assert @group1.details_locked
    assert @group1.closed?
    assert_equal @group1.name, 'new name'
  end

  test 'update should fail if someone not owner of group' do
    admin = create(:user, email: 'a@course-master.com')
    not_admin = create(:user, email: 'b@course-master.com')
    sign_in not_admin
    client = create(:client, user: admin)
    org = create(:organization, client: client)
    group = create(:group, organization: org)
    post :update, {group: {website_url: 'test_url'}, id: group.id, client_id: client.id}
    assert_response :forbidden
  end

  test 'clear_posts should redirect if nobody signed in' do
    admin = create(:user, email: 'b@course-master.com')
    client = create(:client, user: admin)
    group = create(:group, client: client)
    post :clear_posts, {id: group.id, client_id: client.id}
    assert_response :redirect
  end

  test 'clear_posts should work if admin signed in' do
    create_default_org
    admin = create(:user, email: 'b@course-master.com')
    create_client_with_user(admin)

    student = create(:user)
    faculty = create(:user)
    @group1.add_member student
    @group1.add_admin faculty

    p = create(:post, user: student, group: @group1)
    c = create(:comment, user: student, commentable: p)
    f = create(:comment, user: faculty, commentable: p)

    create(:vote, votable: p, voter: faculty)
    create(:vote, votable: c, voter: faculty)

    create(:approval, approvable: p, approver: faculty)
    create(:approval, approvable: c, approver: faculty)

    create(:report, reportable: p, reporter: faculty)
    create(:report, reportable: c, reporter: faculty)

    assert_differences [[->{Post.count}, -1], [->{Comment.count}, -2], [->{Vote.count}, -2], [->{Report.count}, -2], [->{Approval.count}, -2]] do
      post :clear_posts, {id: @group1.id, client_id: @client1.id}
    end
    assert_response :ok
  end

  test 'clear_posts should fail if someone not owner of group' do
    admin = create(:user, email: 'a@course-master.com')
    not_admin = create(:user, email: 'b@course-master.com')
    sign_in not_admin
    client = create(:client, user: admin)
    group = create(:group, organization: @org)
    post :clear_posts, {id: group.id, client_id: @org.client.id}
    assert_response :forbidden
  end

  test 'upload_s3_urls changes group urls' do
    admin = create(:user, email: 'a@course-master.com')

    create_client_with_user(admin)
    post :upload_s3_urls, token: ENV['SAVANNAH_TOKEN'], urls: [{group_id: @group1.client_resource_id, json_url: 'http://www.johnluttigrules.com'}, {group_id: -1, json_url: 'http://www.johnluttigrules.com'}], format: :json
    assert_response :ok do
      assert_equal @group1.course_data_url, 'http://www.johnluttigrules.com'
    end
  end

  test 'render 404 error page if signed-in user isnot an admin' do
    user = create(:user, email: 'a@example.com')

    create_client_with_user(user)
    post :upload_s3_urls, token: ENV['SAVANNAH_TOKEN'], urls: [{group_id: @group1.client_resource_id, json_url: 'http://www.johnluttigrules.com'}, {group_id: -1, json_url: 'http://www.johnluttigrules.com'}]

    assert_response 401
    assert request.format.html?
    assert_template 'errors/unauthorized'

    post :upload_s3_urls, token: ENV['SAVANNAH_TOKEN'], urls: [{group_id: @group1.client_resource_id, json_url: 'http://www.johnluttigrules.com'}, {group_id: -1, json_url: 'http://www.johnluttigrules.com'}], format: :js

    assert_response 401
    assert request.format.js?
  end

  test ':api course_data request should properly download and parse json' do
    FlipmodeActivityRecorderJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    admin = create(:user, email: 'a@course-master.com')
    start_date = Time.now.utc
    create_client_with_user(admin, group_opts: {client_resource_id: 'test_client_resource_id', course_data_url: 'http://url.com', start_date: start_date})

    @group1.add_member admin
    FlipmodeActivityRecorderJob.any_instance.expects(:perform).with(admin.email, @group1.client_resource_id).once
    # sign_in admin
    get :course_data, id: @group1.id, format: :json
    assert_response :success
    response_object = get_json_response response
    assert_equal(response_object, {
      'id' => @group1.id,
      'start_date' => start_date.strftime("%Y-%m-%d"),
      'term' =>  @group1.course_term,
      'course_title' => @group1.name,
      'course_description' => @group1.description,
      'course_image' => @group1.image_url,
      'course_website_url' => @content[:meta_savannah][:website_url],
      'course_organization' => @content[:meta_savannah][:organization],
      'course_intro_link' => @content[:meta_savannah][:intro_link],
      'instructors' => @content[:meta_savannah][:instructors],
      'lectures' => [
        {
          'lecture_title' => 'chapter1',
          'videos' => [
            {
              'youtube_id' => 'v1',
              'youtube_title' => 'v1 title'
            },
            {
              'youtube_id' => 'v2',
              'youtube_title' => 'v2 title'
            },
            {
              'youtube_id' => 'v3',
              'youtube_title' => 'v3 title'
            },
            {
              'youtube_id' => 'v4',
              'youtube_title' => 'v4 title'
            }
          ],
          'statics' => [
            {
              'static_title' => 'First.pdf',
              'static_url' => 'https://cloudfront.com/First.pdf'
            },
            {
              'static_title' => 'First.ppt',
              'static_url' => 'https://cloudfront.com/First.ppt'
            },
            {
              'static_title' => 'Second.pdf',
              'static_url' => 'https://cloudfront.com/Second.pdf'
            },
            {
              'static_title' => 'Second.ppt',
              'static_url' => 'https://cloudfront.com/Second.ppt'
            },
            {
              'static_title' => 'Third.pdf',
              'static_url' => 'https://cloudfront.com/Third.pdf'
            },
            {
              'static_title' => 'Third.ppt',
              'static_url' => 'https://cloudfront.com/Third.ppt'
            }
          ]
        }
      ],
      "dogwood_release" => nil
    })
  end

  test ':api course_data v1 request should properly download and parse json' do
    create_default_org
    FlipmodeActivityRecorderJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    admin = create(:user, email: 'a@course-master.com', organization: Organization.default_org)

    create_client_with_user(admin, group_opts: {client_resource_id: 'test_client_resource_id', course_data_url: 'http://url.com'})
    @group1.add_member admin
    FlipmodeActivityRecorderJob.any_instance.expects(:perform).with(admin.email, @group1.client_resource_id).once
    # sign_in admin
    @request.headers['X-Build-Number'] = '45'
    @request.headers['User-Agent'] = 'Edcast ios'
    get :course_data, id: @group1.id, format: :json
    assert_response :success
    response_object = get_json_response response
    assert_equal(response_object, {
      'id' => @group1.id,
      'start_date' => @content[:meta_savannah][:start_date],
      'term' => @group1.course_term,
      'course_title' => @group1.name,
      'course_description' => @group1.description,
      'course_image' => @group1.image_url,
      'course_website_url' => @content[:meta_savannah][:website_url],
      'course_organization' => @content[:meta_savannah][:organization],
      'course_intro_link' => @content[:meta_savannah][:intro_link],
      'instructors' => @content[:meta_savannah][:instructors],
      'lectures' => [
        {
          'lecture_title' => 'chapter1',
          'videos' => [
            {
              'id' => 'v1',
              'title' => 'v1 title',
              'source' => 'Youtube',
              'thumbnail' => 'http://someimage.png'
            },
            {
              'id' => 'v2',
              'title' => 'v2 title',
              'source' => 'Youtube',
              'thumbnail' => 'http://someimage.png'
            },
            {
              'id' => 'v3',
              'title' => 'v3 title',
              'source' => 'Youtube',
              'thumbnail' => 'http://someimage.png'
            },
            {
              'id' => 'v4',
              'title' => 'v4 title',
              'source' => 'Youtube',
              'thumbnail' => 'http://someimage.png'
            },
            {
              'id' => 'vimeo-v1',
              'title' => 'vimeo title',
              'source' => 'Vimeo',
              'thumbnail' => 'http://somevimeo.png'
            }
          ],
          'statics' => [
            {
              'static_title' => 'First.pdf',
              'static_url' => 'https://cloudfront.com/First.pdf'
            },
            {
              'static_title' => 'First.ppt',
              'static_url' => 'https://cloudfront.com/First.ppt'
            },
            {
              'static_title' => 'Second.pdf',
              'static_url' => 'https://cloudfront.com/Second.pdf'
            },
            {
              'static_title' => 'Second.ppt',
              'static_url' => 'https://cloudfront.com/Second.ppt'
            },
            {
              'static_title' => 'Third.pdf',
              'static_url' => 'https://cloudfront.com/Third.pdf'
            },
            {
              'static_title' => 'Third.ppt',
              'static_url' => 'https://cloudfront.com/Third.ppt'
            }
          ]
        }
      ],
      "dogwood_release"=>nil
    })
  end

  test ':api course_data support for brightcove videos' do
    create_default_org
    FlipmodeActivityRecorderJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    admin = create(:user, email: 'a@course-master.com', organization: Organization.default_org)

    create_client_with_user(admin, group_opts: {client_resource_id: 'test_client_resource_id', course_data_url: 'http://url.com'})
    @group1.add_member admin
    FlipmodeActivityRecorderJob.any_instance.expects(:perform).with(admin.email, @group1.client_resource_id).once
    sign_in admin
    @request.headers['X-Build-Number'] = '380'
    @request.headers['User-Agent'] = 'Edcast ios'
    get :course_data, id: @group1.id, format: :json
    assert_response :success
    response_object = get_json_response response
    assert_equal(response_object, {
      'id' => @group1.id,
      'start_date' => @content[:meta_savannah][:start_date],
      'term' => @group1.course_term,
      'course_title' => @group1.name,
      'course_description' => @group1.description,
      'course_image' => @group1.image_url,
      'course_website_url' => @content[:meta_savannah][:website_url],
      'course_organization' => @content[:meta_savannah][:organization],
      'course_intro_link' => @content[:meta_savannah][:intro_link],
      'instructors' => @content[:meta_savannah][:instructors],
      'lectures' => [
        {
          'lecture_title' => 'chapter1',
          'videos' => [
            {
              'id' => 'v1',
              'title' => 'v1 title',
              'source' => 'Youtube',
              'thumbnail' => 'http://someimage.png'
            },
            {
              'id' => 'v2',
              'title' => 'v2 title',
              'source' => 'Youtube',
              'thumbnail' => 'http://someimage.png'
            },
            {
              'id' => 'v3',
              'title' => 'v3 title',
              'source' => 'Youtube',
              'thumbnail' => 'http://someimage.png'
            },
            {
              'id' => 'v4',
              'title' => 'v4 title',
              'source' => 'Youtube',
              'thumbnail' => 'http://someimage.png'
            },
            {
              'id' => 'vimeo-v1',
              'title' => 'vimeo title',
              'source' => 'Vimeo',
              'thumbnail' => 'http://somevimeo.png'
            },
            {
              "id" => "http://#{@org1.host}/brightcove_videos?video_url=http://bcove.me/hc8szal5vvcc",
              "title" => "Brightcove title",
              "source" => "Brightcove",
              "thumbnail" => nil
            }
          ],
          'statics' => [
            {
              'static_title' => 'First.pdf',
              'static_url' => 'https://cloudfront.com/First.pdf'
            },
            {
              'static_title' => 'First.ppt',
              'static_url' => 'https://cloudfront.com/First.ppt'
            },
            {
              'static_title' => 'Second.pdf',
              'static_url' => 'https://cloudfront.com/Second.pdf'
            },
            {
              'static_title' => 'Second.ppt',
              'static_url' => 'https://cloudfront.com/Second.ppt'
            },
            {
              'static_title' => 'Third.pdf',
              'static_url' => 'https://cloudfront.com/Third.pdf'
            },
            {
              'static_title' => 'Third.ppt',
              'static_url' => 'https://cloudfront.com/Third.ppt'
            }
          ]
        }
      ],
      "dogwood_release" => nil
    })
  end

  test 'course data should return unauthorized if user not in group' do
    create_default_org
    user = create(:user)

    create_client_with_user(user)
    get :course_data, id: @group1.id, format: :json
    assert_response :unauthorized
    sign_out user
  end

  test 'course data should fail gracefully on errors' do
    user = create(:user)
    group = create(:group, course_data_url: nil)
    group.add_member user
    sign_in user
    get :course_data, id: group.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal [], resp['lectures']
  end

  test 'should fail gracefully when s3 connection errors' do
    group = create(:group, course_data_url: 'http://badurl.com')
    user = create(:user)
    group.add_member user
    sign_in user
    stub_request(:get, 'http://badurl.com').
        with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
        to_return(:status => 404,
                  :body => {}.to_json,
                  :headers => {'Content-Type' => 'application/json'}
    )
    Rails.logger.expects(:error).once
    get :course_data, id: group.id, format: :json
    assert_response :unprocessable_entity
  end

  test 'shoul fail gracefully when non url' do
    user = create(:user)
    Rails.logger.expects(:error).once
    group = create(:group, course_data_url: 'non url')
    group.add_member user
    sign_in user
    get :course_data, id: group.id, format: :json
    assert_response :unprocessable_entity
  end

  test 'json parse errors should be logged' do
    group = create(:group, course_data_url: 'http://nonjsonurl.com')
    user = create(:user)
    group.add_member user
    sign_in user

    Rails.logger.expects(:error).once
    stub_request(:get, 'http://nonjsonurl.com').
        with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
        to_return(:status => 200,
                  :body => {'fsdfsd' => ''},
                  :headers => {'Content-Type' => 'application/json'}
    )
    get :course_data, id: group.id, format: :json
    assert_response :unprocessable_entity
  end

  test 'savannah should be able to close groups' do
    org = create(:organization)
    client = create(:client, organization: org)

    group, admin = create_gu(user_opts: {organization_id: org.id}, group_opts: {organization_id: org.id, client_id: client.id, client_resource_id: 1}, role: 'admin')
    ClientsUser.create(client: client, user: admin, client_user_id: admin.id)
    cred = client.valid_credential
    parts = [cred.shared_secret, admin.id, admin.email, 'teacher', 1].compact
    token = Digest::SHA256.hexdigest(parts.join('|'))

    group_params = { user_id: admin.id, user_email: admin.email, user_role: 'teacher', api_key: cred.api_key, token: token, resource_id: 1 }
    jwt_token = JWT.encode(group_params, cred.shared_secret,'HS256')
    request.headers['X-API-KEY'] = cred.api_key
    request.headers['X-API-TOKEN'] = jwt_token
    post :close, group_params, format: :json
    assert_response :ok
    assert group.reload.closed?
    sign_out admin

    group.update_attributes(close_access: false)

    post :close, user_id: admin.id, user_email: admin.email, user_role: 'teacher', api_key: client.valid_credential.api_key, token: 'bad token', resource_id: 1, format: :json
    assert_response :forbidden
    assert_not group.reload.closed?

    group.update_attributes(close_access: false)

    student = create(:user, organization: org)
    group.add_member(student)
    ClientsUser.create(client: client, user: student, client_user_id: student.id)
    parts = [client.valid_credential.shared_secret, student.id, student.email, 'student', 1].compact
    token = Digest::SHA256.hexdigest(parts.join('|'))

    post :close, user_id: student.id, user_email: student.email, user_role: 'student', api_key: client.valid_credential.api_key, token: token, resource_id: 1, format: :json

    assert_response :unauthorized
  end

  test 'getting topic stats should work' do
    p1 = create(:post, user: @user, group: @group, message: 'this is a #hw1', context_id: 'context1', context_label: 'clabel1')
    p2 = create(:post, user: @user, group: @group, message: 'this is a #hw1', context_id: 'context2', context_label: 'clabel2')
    u = create(:user)
    @group.add_member(u)

    p3 = create(:post, user: u, group: @group, message: 'this is a #hw1', context_id: 'context1', context_label: 'clabel1')
    p4 = create(:post, user: u, group: @group, message: 'this is a #hw2', context_id: 'context1', context_label: 'clabel1')

    # different group, just for kicks
    g = create(:group)
    g.add_member(@user)
    gp = create(:post, user: @user, group: g, message: '#hw1', context_id: 'context1', context_label: 'clabel1')

    c1 = create(:comment, commentable: p1, user: @user)
    c2 = create(:comment, commentable: p1, user: u)
    c3 = create(:comment, commentable: p2, user: u)

    cgp = create(:comment, commentable: gp, user: @user)

    sign_in @user
    # hw1 has 3 posts, 3 comments, @user1 has 2 posts with this tag, and 1 comment
    get :topic_stats, id: @group.id, topic_type: 'tag', topic_id: 'hw1', format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal({'posts_count' => 3, 'comments_count' => 3, 'user_posts_count' => 2, 'user_comments_count' => 1}, resp)

    # context1 has 3 posts, 2 comments. User1 has 1 post, 1 comment with this context
    get :topic_stats, id: @group.id, topic_type: 'context', topic_id: 'context1', format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal({'posts_count' => 3, 'comments_count' => 2, 'user_posts_count' => 1, 'user_comments_count' => 1}, resp)


    # unsupported type
    get :topic_stats, id: @group.id, topic_type: 'badtype', topic_id: 'boo', format: :json
    assert_response :bad_request

    # all zeros
    get :topic_stats, id: g.id, topic_type: 'tag', topic_id: 'hw2', format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal({'posts_count' => 0, 'comments_count' => 0, 'user_posts_count' => 0, 'user_comments_count' => 0}, resp)

    # tag present in comment only, p1 does not have #hw2
    create(:comment, user: @user, commentable: p1, message: 'comment with #hw2 #hw1 post')
    # p4 has hw2 already, make sure we don't double count
    create(:comment, user: @user, commentable: p4, message: 'comment with #hw2 #hw1')
    get :topic_stats, id: @group.id, topic_type: 'tag', topic_id: 'hw2', format: :json
    resp = get_json_response(response)
    assert_equal({'posts_count' => 1, 'comments_count' => 2, 'user_posts_count' => 0, 'user_comments_count' => 2}, resp)


    sign_out @user

  end

  test 'data dump should work with private groups' do
    DataDumpJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    u = create(:user)
    org = u.organization
    cli = create(:client, user: u, organization: org)
    gr = create(:group, organization: org, client: cli)
    create(:clients_user, client: cli, user: u, client_user_id: 1)
    gr.add_member(u)

    p = create(:post, user: u, group: gr)
    c = create(:comment, user: u, commentable: p)

    pg = create(:private_group, resource_group: gr, name: 'test_private_group', organization: org)
    pg.add_member(u)

    pgp = create(:post, user: u, group: pg)
    pgc = create(:comment, user: u, commentable: pgp)

    sign_in u
    # JSON
    DataDumpJob.expects(:perform_later).at_least(3)
    post :data_dump, {client_id: cli.id, id: gr.id, format: 'JSON'}

    # XML
    post :data_dump, {client_id: cli.id, id: gr.id, format: 'XML'}

    # CSV
    post :data_dump, {client_id: cli.id, id: gr.id, format: 'CSV'}
    sign_out u
  end

  class GroupDataSyncTest < ActionController::TestCase
    include Devise::Test::ControllerHelpers
    setup do
      skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    end

    test ':api should be able to create a group' do
      create_default_org
      cm_admin = create :user, email: 'admin@course-master.com', organization: Organization.default_org

      u = create(:user)
      org = u.organization
      client = create(:client, organization: org)
      # cli = create(:client)
      tok = ENV['SAVANNAH_TOKEN']

      Promotion.expects(:promote).with do |promotable, pars|
        assert_equal Group.last, promotable
        assert_equal ({url: 'infourl', tags: 'cat'}), pars
        true
      end

      assert_difference -> {Group.count} do
        post :sync_course_data, token: tok, api_key: org.client.valid_credential.api_key, resource_id: 1, resource_name: 'gr name', resource_description: 'gr desc', resource_is_private: false, resource_course_term: "Fall 2015", resource_course_code: 'fall2015', resource_start_date: "2015-10-10", resource_end_date: "2018-11-11", language: "en", resource_image_url: 'http://g.png', promotion_data: {url: 'infourl', tags: 'cat'}, format: :json
        # assert_equal Group.last.language, "en"
        assert_response :ok
      end
      g = Group.last
      assert_equal "Fall 2015", g.course_term
      assert_equal 'fall2015', g.course_code
      assert_equal Date.parse("2015-10-10"), g.start_date
      assert_equal Date.parse("2018-11-11"), g.end_date
      assert_equal "http://g.png", g.image_url
    end

    test 'update an existing group' do
      create_default_org
      cm_admin = create :user, email: 'admin@course-master.com', organization: Organization.default_org

      org = create(:organization)
      client = create(:client, organization: org)
      group = create(:resource_group, organization: org, client_resource_id: 1, name: 'old name', image_url: "http://oldimage.png")
      tok = ENV['SAVANNAH_TOKEN']

      Promotion.expects(:promote).with(group, url: 'infourl', tags: 'cat')

      assert_no_difference ->{Group.count} do
        post :sync_course_data, token: tok, api_key: client.valid_credential.api_key, resource_id: 1, resource_name: 'new name', resource_course_code: 'sample', resource_image_url: "http://newimage.png", promotion_data: {url: 'infourl', tags: 'cat'}, format: :json
        assert_response :ok
      end
      group.reload
      assert_equal 'http://newimage.png', group.image_url
      assert_equal 'new name', group.name
      assert_equal 'sample', group.course_code
    end

    test 'fail if api key bad' do
      u = create(:user)
      cli = create(:client)
      tok = ENV['SAVANNAH_TOKEN']
      # no api key
      assert_no_difference ->{Group.count} do
        post :sync_course_data, token: tok, api_key: nil, resource_id: 1, resource_name: 'gr name', resource_description: 'gr desc', resource_is_private: false, resource_course_term: "Fall 2015", resource_start_date: "2015-10-10", resource_image_url: 'http://g.png', format: :json
        assert_response :bad_request
      end

      # bad api key
      assert_no_difference ->{Group.count} do
        post :sync_course_data, token: tok, api_key: 'xxx', resource_id: 1, resource_name: 'gr name', resource_description: 'gr desc', resource_is_private: false, resource_course_term: "Fall 2015", resource_start_date: "2015-10-10", resource_image_url: 'http://g.png', format: :json
        assert_response :unauthorized
      end

      # no token
      assert_no_difference ->{Group.count} do
        post :sync_course_data, token: 'bad token', api_key: 'xxx', resource_id: 1, resource_name: 'gr name', resource_description: 'gr desc', resource_is_private: false, resource_course_term: "Fall 2015", resource_start_date: "2015-10-10", resource_image_url: 'http://g.png', format: :json
        assert_response :unauthorized
      end
    end
  end

  class RightRailStatsTest < ActionController::TestCase
    include Devise::Test::ControllerHelpers
    setup do
      skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
      @user = create(:user)
      @group = create(:group)
      @group.add_member(@user)
      now = Time.now.utc.to_date
      # create a bunch of daily stats that mimic what actual stats
      # might look like

      # 4 days ago. We had three trending tags: 'lovingIt' , 'yo' and 'NAAT'. Two trending contexts -> 'week0', week1'
      DailyGroupStat.create(group_id: @group.id,
                            date: 4.days.ago,
                            trending_tags: 'yo#7,NAAT#6,lovingIt#5',
                            trending_contexts: [{
                              'id' => 'week0_id', 'label' => 'week0', 'count' => 4
                            }, {
                              'id' => 'week1_id', 'label' => 'week1', 'count' => 3
                            }].to_json)

      # 3 days ago. 'lovingit' is not a trending tag anymore. There's one new post with #yo. There's a new context -> 'week2'. There's a new tag. #techSupport
      DailyGroupStat.create(group_id: @group.id,
                            date: 3.days.ago,
                            trending_tags: 'yo#8,NAAT#7,techSupport#2',
                            trending_contexts: [{
                              'id' => 'week2_id', 'label' => 'week2', 'count' => 2
                            }, {
                              'id' => 'week1_id', 'label' => 'week1', 'count' => 3,
                            }, {
                              'id' => 'week0_id', 'label' => 'week0', 'count' => 4
                            }].to_json)

      # 2 days ago. week0 is not a trending context anymore. Week2 has 10 more posts. #techSupport has 4 more posts because edx was down :)
      DailyGroupStat.create(group_id: @group.id,
                            date: 2.days.ago,
                            trending_tags: 'yo#8,NAAT#7,techSupport#6',
                            trending_contexts: [{
                              'id' => 'week2_id', 'label' => 'week2', 'count' => 12,
                            }, {
                              'id' => 'week1_id', 'label' => 'week1', 'count' => 3
                            }].to_json)

      # one day ago. No trending tags, same context, not realistic
      DailyGroupStat.create(group_id: @group.id,
                            date: 1.days.ago,
                            trending_tags: nil,
                            trending_contexts: [{
                              'id' => 'week2_id', 'label' => 'week2', 'count' => 12,
                            }, {
                              'id' => 'week1_id', 'label' => 'week1', 'count' => 3
                            }].to_json)

      # today, no trending tags, no context, not realistic
      DailyGroupStat.create(group_id: @group.id,
                            date: Time.now,
                            trending_tags: nil,
                            trending_contexts: nil)

      # so today, the trending topics are
      # week2: 12, week1: 3, yo: 8, NAAT: 7, techSupport: 6, week0: 4, lovingIt: 5
      @expected_response = [{
        'type' => 'context',
        'id' => 'week2_id',
        'label' => 'week2',
        'count' => 12,
      },
      {
        'type' => 'context',
        'id' => 'week1_id',
        'label' => 'week1',
        'count' => 3
      },
      {
        'type' => 'tag',
        'id' => nil,
        'label' => 'yo',
        'count' => 8
      },
      {
        'type' => 'tag',
        'id' => nil,
        'label' => 'NAAT',
        'count' => 7
      },
      {
        'type' => 'tag',
        'id' => nil,
        'label' => 'techSupport',
        'count' => 6
      },
      {
        'type' => 'context',
        'id' => 'week0_id',
        'label' => 'week0',
        'count' => 4
      },
      {
        'type' => 'tag',
        'id' => nil,
        'label' => 'lovingIt',
        'count' => 5
      }]

    end

    test 'get right rails stats' do
      sign_in @user

      get :right_rail_stats, id: @group.id, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal @expected_response, resp

      sign_out @user
    end

    test 'right rail stats for a new group' do
      newg = create(:group)
      newg.add_member(@user)
      sign_in @user

      get :right_rail_stats, id: newg.id, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal [], resp
      sign_out @user
    end

    test 'limit at 10 labels' do
      newg = create(:group)
      newg.add_member(@user)
      DailyGroupStat.create(group_id: newg.id,
                            date: Time.now,
                            trending_tags: 'tag1#10,tag2#9,tag3#8,tag4#7,tag5#6,tag6#5,tag7#4,tag8#3',
                            trending_contexts: [{
                              'id' => 'context1_id',
                              'label' => 'context1',
                              'count' => 10
                            },
                            {
                              'id' => 'context2_id',
                              'label' => 'context2',
                              'count' => 5
                            }].to_json)

      # previous day, shouldn't be considered
      DailyGroupStat.create(group_id: newg.id,
                            date: Time.now,
                            trending_tags: 'prev1#20,prev2#30',
                            trending_contexts: [{
                              'id' => 'prevc1_id',
                              'label' => 'prevc1',
                              'count' => 10
                            },
                            {
                              'id' => 'prevc2_id',
                              'label' => 'prevc2',
                              'count' => 5
                            }].to_json)

      sign_in @user
      get :right_rail_stats, id: newg.id, format: :json
      assert_response :ok
      resp = get_json_response(response)
      assert_equal 0, (resp.map{|r| r['label']} & ['prev1', 'prev2', 'prevc1', 'prevc2']).count
    end

  end

  class ContextTests < ActionController::TestCase
    include Devise::Test::ControllerHelpers
    self.use_transactional_fixtures = false

    teardown do
      WebMock.enable!
    end

    setup do
      skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
      create_default_org
      WebMock.disable!
      if Post.searchkick_index.exists?
        Post.searchkick_index.delete
      end
      @user = create(:user)
      @group = create(:group)
      @group.add_member(@user)
      (1..5).to_a.each do |i|
        (1..i).to_a.each do |k|
          p = create(:post, user: @user, group: @group, context_id: "context_id_#{i}", context_label: "context label #{i}")
        end
      end
      p = create_list(:post, 6, user: @user, group: @group, context_id: "context_id_6", context_label: "context label 6")
      Post.reindex
      Post.searchkick_index.refresh
    end

    test 'get a list of contexts for the given group' do
      sign_in @user
      get :contexts, id: @group.id, format: :json
      json_response = get_json_response(response)
      first_context = json_response.first
      assert_equal 6, first_context['count']
      assert_equal 'context_id_6', first_context['id']
      assert_equal 'context label 6', first_context['label']
      assert_equal 'context', first_context['type']

      last_context = json_response.last
      assert_equal 1, last_context['count']
      assert_equal 'context_id_1', last_context['id']
      assert_equal 'context label 1', last_context['label']
      assert_equal 'context', last_context['type']
    end
  end

  test "should handle no static files on course data dump" do

    @content = {
      meta_savannah: {
        name: "course name",
        description: "course description",
        website_url: "website url",
        logo_url: "logo url",
        organization: "some university",
        start_date: 2.days.ago.strftime('%Y-%m-%d'),
        term: "Fall 2015",
        instructors: [
          {
            'name' => "Jesse Pinkman",
            'image' => "http://someimage.png",
            'picture' => "http://someimage.png"
          },
          {
            'name' => "Walter White",
            'image' => "http://iamtheonewhoknocks.png",
            'picture' => "http://iamtheonewhoknocks.png"
          }
        ],
      },
      chapters: [
        {
          chapter_title: "chapter1",
          sequentials: [
            {
              sequential_title: "sequential1",
              verticals: [
                {
                  vertical_title: "Video",
                  videos: [
                    {
                      youtube: "v1",
                      youtube_title: "v1 title",
                      'video_id' => 'v1',
                      'video_title' => 'v1 title',
                      'video_source' => 'Youtube',
                      'video_thumbnail' => 'http://someimage.png'
                    },
                    {
                      youtube: "v2",
                      youtube_title: "v2 title",
                      'video_id' => 'v2',
                      'video_title' => 'v2 title',
                      'video_source' => 'Youtube',
                      'video_thumbnail' => 'http://someimage.png'
                    }
                  ],
                  static: [],
                },
                {
                  vertical_title: "something",
                  videos: [
                    {
                      youtube: "v3",
                      youtube_title: "v3 title",
                      'video_id' => 'v3',
                      'video_title' => 'v3 title',
                      'video_source' => 'Youtube',
                      'video_thumbnail' => 'http://someimage.png'
                    },
                  ],
                  static: [],
                }
              ]
            },
            {
              sequential_title: "sequential2",
              verticals: [
                {
                  vertical_title: "video3",
                  videos: [
                    {
                      youtube: "v4",
                      youtube_title: "v4 title",
                      'video_id' => 'v4',
                      'video_title' => 'v4 title',
                      'video_source' => 'Youtube',
                      'video_thumbnail' => 'http://someimage.png'
                    }
                  ],
                  vimeo_videos: [
                    {
                      'video_id' => 'vimeo-v1',
                      'video_title' => 'vimeo title',
                      'video_source' => 'Vimeo',
                      'video_thumbnail' => 'http://somevimeo.png'
                    }
                  ],
                  static: [],
                }
              ]
            }
          ]
        },
        {
          chapter_title: "chaper2",
          sequentials: []
        }
      ]
    }

    stub_request(:get, 'http://url.com').
        with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
        to_return(:status => 200,
                  :body => @content.to_json,
                  :headers => {'Content-Type' => 'application/json'},
                 )

    create_default_org
    admin = create(:user, email: 'a@course-master.com', organization: Organization.default_org)
    client = create(:client, user: admin)
    org = create(:organization, client: client)
    group = create(:group, organization: org, client_resource_id: 'test_client_resource_id', course_data_url: 'http://url.com')
    group.add_member admin
    FlipmodeActivityRecorderJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    FlipmodeActivityRecorderJob.any_instance.expects(:perform).with(admin.email, group.client_resource_id).once
    sign_in admin
    get :course_data, id: group.id, format: :json
    assert_response :success
    response_object = get_json_response response
    assert_equal(response_object, {
      'id' => group.id,
      'start_date' => @content[:meta_savannah][:start_date],
      'term' => group.course_term,
      'course_title' => group.name,
      'course_description' => group.description,
      'course_image' => group.image_url,
      'course_website_url' => @content[:meta_savannah][:website_url],
      'course_organization' => @content[:meta_savannah][:organization],
      'course_intro_link' => @content[:meta_savannah][:intro_link],
      'instructors' => @content[:meta_savannah][:instructors],
      'lectures' => [
        {
          'lecture_title' => 'chapter1',
          'videos' => [
            {
              'youtube_id' => 'v1',
              'youtube_title' => 'v1 title'
            },
            {
              'youtube_id' => 'v2',
              'youtube_title' => 'v2 title'
            },
            {
              'youtube_id' => 'v3',
              'youtube_title' => 'v3 title'
            },
            {
              'youtube_id' => 'v4',
              'youtube_title' => 'v4 title'
            }
          ],
          'statics' => []
        }
      ],
      "dogwood_release"=>nil
    })

  end

  test 'update group preferences' do
    sign_in @faculty

    put :change_preference, id: @group.id, preferences: { connect_to_social_mediums: false }, format: :json
    assert_response :ok
    refute @group.reload.connect_to_social_mediums

    put :change_preference, id: @group.id, preferences: { daily_digest_enabled: true }, format: :json
    assert_response :ok
    assert @group.reload.daily_digest_enabled

    put :change_preference, id: @group.id, preferences: { forum_notifications_enabled: false }, format: :json
    assert_response :ok
    refute @group.reload.forum_notifications_enabled
  end

  test '#preferences list specific preferences and their values' do
    sign_in @faculty

    get :preferences, id: @group.id, format: :json
    assert_response :ok
    assert @group.connect_to_social_mediums
    assert @group.enable_mobile_app_download_buttons
    assert @group.share_to_social_mediums
    assert @group.forum_notifications_enabled
    refute @group.daily_digest_enabled
  end
end

class SlowGroupsTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers
  self.use_transactional_fixtures = false

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    @controller = GroupsController.new
    WebMock.disable!
    Post.any_instance.unstub :p_read_by_creator
  end

  teardown do
    WebMock.enable!
  end

  setup do
    PostRead.create_index! force: true
    @org = create(:organization)
    @client = create(:client, organization: @org)
    @group = create(:resource_group, organization: @org, client: @client)
    @user = create(:user, organization: @org)
    @group.add_member @user
    @group.created_at = 45.days.ago
    @group.save

    @faculty = create(:user, organization: @org)
    @group.add_admin @faculty

    p1 = create(:post, user: @user, group: @group)
    p2 = create(:post, user: @user, group: @group)
    p3 = create(:post, user: @user, group: @group)

    c1 = create(:comment, user: @user, commentable: p1)

    create(:score, user: @user, group: @group,
           score: 3, stars: 2)

    trending_contexts = [{'id' => 'someid1', 'label' => 'week1#boo', 'count' => 13},
                         {'id' => 'someid2', 'label' => 'week2lec3', 'count' => 10}]

    DailyGroupStat.create(group_id: @group.id,
                          date: p1.created_at, total_users_with_posts: 1,
                          total_users_with_comments: 0, total_active_forum_users: 11,
                          total_posts: 15, total_unanswered_posts: 10, total_comments: 1,
                          total_answered_posts: 5, trending_tags: 'hello#2,goodbye#4', trending_contexts: trending_contexts.to_json)
    Post.reindex
  end

  test 'should get addon stats for new user' do
    g = create(:group, organization: @org, client_resource_id: 'xxxxx')
    cred = g.valid_credential
    uid = 999
    email = 'boo@boo.com'
    parts = [cred.shared_secret, uid, email, g.client_resource_id].compact
    token = Digest::SHA256.hexdigest(parts.join('|'))
    pars = {token: token, user_id: uid, user_email: email, api_key: cred.api_key, resource_id: g.client_resource_id }

    get :addon_stats, pars.merge({format: :json, days: 8})
    assert_response :ok
  end

  test 'should get stats for instructor' do
    sign_in @faculty

    get :stats, id: @group.id, format: :json

    resp = get_json_response(response)

    assert_equal 15, resp['weekly']['num_threads']
    assert_equal 10, resp['weekly']['num_threads_without_replies']
    assert_equal 1, resp['weekly']['num_replies']
    assert_equal ['hello#2', 'goodbye#4'], resp['weekly']['trending_tags']
    assert_equal([{'context' => 'week1#boo', 'count' => 13},
                  {'context' => 'week2lec3', 'count' => 10}], resp['weekly']['trending_contexts'])

    assert_equal 3, resp['alltime']['num_threads']
    assert_equal 2, resp['alltime']['num_threads_without_replies']
    assert_equal 1, resp['alltime']['num_replies']

    today = Time.now.utc.strftime('%m/%d')

    assert_equal resp['daily_stats'].last,
      {'day' => today, 'num_threads' => 15, 'num_threads_with_replies' => 5}

    assert_equal resp['daily_user_stats'].last,
      {'day' => today, 'active_users' => 11, 'posted_users' => 1, 'answered_users' => 0}

    sign_out @faculty
  end

  test 'should get stats for student' do
    create_default_org
    sign_in @user
    get :stats, id: @group.id, format: :json

    resp = get_json_response(response)

    assert_equal 15, resp['weekly']['num_threads']
    assert_equal ['hello#2', 'goodbye#4'], resp['weekly']['trending_tags']
    assert_equal([{'context' => 'week1#boo', 'count' => 13},
                  {'context' => 'week2lec3', 'count' => 10}], resp['weekly']['trending_contexts'])

    assert_equal resp['alltime']['num_posts_unread'], 0
    assert_equal resp['score'], 3
    assert_equal resp['stars'], 2
    sign_out @user
  end

  def add_user_setup
    @api_key = @client.valid_credential.api_key
    @shared_secret = @client.valid_credential.shared_secret
    @resource_id = 'r1'
  end

  def make_token(email: nil, user_id: nil, role: nil)
    parts = [@shared_secret, user_id, email, role, @resource_id].compact
    Digest::SHA256.hexdigest(parts.join('|'))
  end

  def make_auth_token(pars)
    parts = [pars[:savannah_app_id], pars[:user_id], pars[:user_email], pars[:role], pars[:resource_id], pars[:destiny_user_id]].compact
    Digest::SHA256.hexdigest(parts.join('|'))
  end
  def set_api_request_header(payload,api_key,shared_secret)
    token = JWT.encode(payload, shared_secret,'HS256')
    request.headers['X-API-KEY'] = api_key
    request.headers['X-API-TOKEN'] = token
  end

  test 'should associate org user on add user' do
    @api_key = @client.valid_credential.api_key
    @shared_secret = @client.valid_credential.shared_secret
    @resource_id = 'r1'

    # No org user for a@a.com currently
    g = create(:group, client_resource_id: @resource_id, organization: @org, is_private: false)
    user_id = 123
    email = 'a@a.com'
    role = 'student'
    tok = make_token(email: email, user_id: user_id, role: role)
    user_params = {user_id: user_id, user_email: email, user_role: role, token: tok, api_key: @api_key, resource_id: @resource_id, resource_is_private: true}
    set_api_request_header(user_params,@api_key,@shared_secret)
    # enroll the user. The user with this email doesn't exist, so we create a new one
    assert_difference [->{User.count}, ->{ClientsUser.count}, ->{GroupsUser.count}] do
      assert_difference ->{User.where(organization: @org).count} do
        post 'add_user', user_params.merge(format: :json)
        assert_response :ok
      end
    end

    # b@b.com exists in destiny org first
    email = 'b@b.com'
    org_b = create(:user, organization: @org, email: email)
    user_id = 456
    role = 'student'
    tok = make_token(email: email, user_id: user_id, role: role)
    user_params = {user_id: user_id, user_email: email, user_role: role, token: tok, api_key: @api_key, resource_id: @resource_id, resource_is_private: true}
    set_api_request_header(user_params,@api_key,@shared_secret)
    #it shouldn't create a new user in default org
    assert_difference [->{ClientsUser.count}, ->{GroupsUser.count}] do
      assert_no_difference [->{User.count}] do
        post 'add_user', user_params.merge(format: :json)
        assert_response :ok
      end
    end
    org_b.reload
    assert_equal User.last.id, org_b.parent_user_id
  end

  test 'should add and remove user' do
    add_user_setup
    g = create(:group, client_resource_id: @resource_id, organization: @org, is_private: false)
    user_id = 123
    email = 'a@a.com'
    role = 'student'
    tok = make_token(email: email, user_id: user_id, role: role)
    user_params = {user_id: user_id, user_email: email, user_role: role, token: tok, api_key: @api_key, resource_id: @resource_id, resource_is_private: true}
    set_api_request_header(user_params,@api_key,@shared_secret)
    # enroll the user. The user with this email doesn't exist, so we create a new one
    assert_difference [->{User.count}, ->{ClientsUser.count}, ->{GroupsUser.count}] do
      post 'add_user', user_params.merge(format: :json)
      assert_response :ok
    end
    assert g.reload.is_private

    u = User.last
    assert_equal email, u.email
    assert g.is_member?(u)

    # now unenroll this user
    tok = make_token(email: email, user_id: user_id)
    user_params =  { user_id: user_id, user_email: email, token: tok, api_key: @api_key, resource_id: @resource_id }
    set_api_request_header(user_params,@api_key,@shared_secret)
    assert_difference ->{GroupsUser.count}, -1 do
      post 'remove_user',user_params.merge(format: :json)
      assert_response :ok
    end
    assert_not g.is_member?(u)

    # trying to unenroll user from a group he doesn't belong to
    @resource_id = 'r2'
    g2 = create(:group, client_resource_id: @resource_id, organization: @org)
    tok = make_token(email: email, user_id: user_id)
    user_params =   { user_id: user_id, user_email: email, token: tok, api_key: @api_key, resource_id: @resource_id }
    set_api_request_header(user_params,@api_key,@shared_secret)
    assert_no_difference ->{GroupsUser.count} do
      post 'remove_user', user_params.merge(format: :json)
      assert_response :unprocessable_entity
    end

    # bad enroll request, bad token
    user_params  = { user_id: user_id, user_email: email, user_role: role, token: 'badtoken', api_key: @api_key, resource_id: @resource_id }
    set_api_request_header(user_params,@api_key,@shared_secret)
    assert_no_difference [->{User.count}, ->{ClientsUser.count}, ->{GroupsUser.count}] do
      post 'add_user',user_params.merge(format: :json)
      assert_response :forbidden # this is bad, cm_auth doesn't respond correctly to bad json requests, TODO for another day
    end

    # enroll user first as student, then as instructor
    # no resource_is_private key, group should stay default public
    tok = make_token(email: email, user_id: user_id, role: role)
    user_params  = { user_id: user_id, user_email: email, user_role: role, token: tok, api_key: @api_key, resource_id: @resource_id }
    set_api_request_header(user_params,@api_key,@shared_secret)
    post 'add_user', user_params.merge(format: :json)
    assert_response :ok
    assert_not g2.reload.is_private
    assert g2.is_member?(u)

    # resource_is_private key exists and is set to false
    role = 'instructor'
    tok = make_token(email: email, user_id: user_id, role: role)
    user_params  = { user_id: user_id, user_email: email, user_role: role, token: tok, api_key: @api_key, resource_id: @resource_id, resource_is_private: false }
    set_api_request_header(user_params,@api_key,@shared_secret)
    post 'add_user',  user_params.merge(format: :json)
    assert_response :ok
    assert_not g2.reload.is_private
    assert_not g2.is_member?(u)
    assert g2.is_admin?(u)

    # now unenroll
    tok = make_token(email: email, user_id: user_id)
    user_params  = {user_id: user_id, user_email: email, token: tok, api_key: @api_key, resource_id: @resource_id }
    set_api_request_header(user_params,@api_key,@shared_secret)
    assert_difference ->{GroupsUser.count}, -1 do
      post 'remove_user', user_params.merge(format: :json)
      assert_response :ok
    end

    assert_not g2.is_admin?(u)
  end

  test '#remove user clears the user as followers from posts associated with course' do
    add_user_setup
    g     = create(:group, client_resource_id: @resource_id, organization: @org)
    user_id, email, role = 123, 'a@a.com', 'student'
    token = make_token(email: email, user_id: user_id, role: role)

    # enroll the user. The user with this email doesn't exist, so we create a new one
    user_params  = { user_id: user_id, user_email: email, user_role: role, token: token, api_key: @api_key, resource_id: @resource_id }
    set_api_request_header(user_params,@api_key,@shared_secret)
    post 'add_user', user_params.merge(format: :json)
    assert_response :ok

    u     = User.last
    assert g.is_member?(u)

    post  = Question.create({ title: 'Sample post title', message: 'sample post description', postable: g, user_id: u.id })
    Follow.create(user: u, followable: post)

    assert_includes g.reload.posts, post

    # now unenroll this user
    user_params  = {  user_id: user_id, user_email: email, token: token, api_key: @api_key, resource_id: @resource_id }
    set_api_request_header(user_params,@api_key,@shared_secret)
    post 'remove_user',user_params.merge(format: :json)
    assert_response :ok
    assert_empty post.reload.followers
  end

  test 'get return channels for a group' do
    add_user_setup
    g     = create(:group, client_resource_id: @resource_id, organization: @org)
    user_id, email, role = 123, 'a@a.com', 'student'
    token = make_token(email: email, user_id: user_id, role: role)

    # enroll the user. The user with this email doesn't exist, so we create a new one
    user_params  = { user_id: user_id, user_email: email, user_role: role, token: token, api_key: @api_key, resource_id: @resource_id }
    set_api_request_header(user_params,@api_key,@shared_secret)
    get :channels, user_params.merge(format: :json)
    assert_response :ok
    resp = get_json_response(response)
    assert_empty resp

    channels = create_list(:channel, 3, organization: @org)
    g.channels << channels
    get :channels, user_params.merge(format: :json)
    resp = get_json_response(response)
    assert_equal resp.length, 3
  end

  test 'get return forum channels for a group' do
    g = create(:group, organization: @org, client_resource_id: '123')
    cred = g.valid_credential
    uid = 999
    email = 'boo@boo.com'
    parts = [cred.shared_secret, uid, email, g.client_resource_id].compact
    token = Digest::SHA256.hexdigest(parts.join('|'))
    pars = {token: token, user_id: uid, user_email: email, api_key: cred.api_key, resource_id: g.client_resource_id }

    get :forum_channels, pars.merge(format: :json)
    assert_response :ok
    resp = get_json_response(response)
    assert_empty resp

    channels = create_list(:channel, 3, organization: @org)
    g.channels << channels
    get :forum_channels, pars.merge(format: :json)
    resp = get_json_response(response)
    assert_equal resp.length, 3
  end

  test "unauthorized req for channels" do
    add_user_setup
    g     = create(:group, client_resource_id: @resource_id, organization: @org)
    user_id, email, role = 123, 'a@a.com', 'student'
    token = make_token(email: email, user_id: 2321, role: role)

    user_params  = { user_id: user_id, user_email: email, user_role: role, token: token, api_key: @api_key, resource_id: @resource_id }
    set_api_request_header(user_params, "invalid token", @shared_secret)
    get :channels, user_params.merge(format: :json)
    assert_response 401
  end

  test 'should get addon stats' do
    org = @group.organization
    user = create(:user, organization: @group.organization)
    pars = { user_id: 999, user_email: user.email, savannah_app_id: org.savannah_app_id, resource_id: @group.client_resource_id, destiny_user_id: user.id }
    auth_token = make_auth_token(pars)
    user_params  = pars.merge(auth_token: auth_token)
    get :addon_stats, user_params.merge(days: 8, format: :json)
    assert_response :ok
    resp = get_json_response(response)
    assert_equal ["Date", "Active forum users", "Posts count", "Comments count", "Users posting", "Users commenting"],
                 resp['labels']
    data = resp['data']
    assert_equal 8, data.size
    assert_equal [11, 15, 1, 1, 0], data.last.drop(1)
    assert_equal DateTime.now.utc.to_date, DateTime.parse(data.last.first)

    #offset date
    get :addon_stats, user_params.merge(days: 3, to_date: 2.days.ago.to_s, format: :json)
    assert_response :ok
    resp = get_json_response(response)
    data = resp['data']
    assert_equal 3, data.size
    assert_equal 2.days.ago.to_date, DateTime.parse(data.last.first)

    # # some bad requests
    get :addon_stats, user_params.merge(days: 0)
    assert_response :unprocessable_entity

    get :addon_stats, user_params.merge(days: 3, to_date: "that ain't no date i ever heard of")
    assert_response :unprocessable_entity
    sign_out user
  end
end

require 'test_helper'

class VideoStreamsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  test 'redirect iris web player to a signed url' do
    Organization.any_instance.stubs(:get_sharing_permissions).returns true
    create_default_org
    v = create(:iris_video_stream, :live, x_id: '123456')
    get :signed_playback_url, id: v.id
    assert_response :redirect
    assert_match '123456', response.redirect_url
    assert_match 'embed.bambuser.com', response.redirect_url
    assert_match 'da_signature=', response.redirect_url
  end
end

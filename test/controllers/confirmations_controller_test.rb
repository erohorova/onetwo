require 'test_helper'

class ConfirmationsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  test 'should redirect to app' do
    user = create(:user, confirmed_at: nil, is_complete: false)
    @request.env["devise.mapping"] = Devise.mappings[:user]

    tok = user.instance_variable_get(:@raw_confirmation_token)
    set_host user.organization
    get :show, confirmation_token: tok, app_redirect: 'true', format: :html
    assert_redirected_to "edcastapp://auth/confirmations?confirmation_token=#{tok}&host=#{request.host}"
  end

  test 'should redirect to white-labelled app' do
    user = create(:user, confirmed_at: nil, is_complete: false)
    @request.env["devise.mapping"] = Devise.mappings[:user]
    Organization.any_instance.stubs(:mobile_uri_scheme).returns('skillsetedcastapp')

    tok = user.instance_variable_get(:@raw_confirmation_token)
    set_host user.organization
    get :show, confirmation_token: tok, app_redirect: 'true', format: :html
    assert_redirected_to "skillsetedcastapp://auth/confirmations?confirmation_token=#{tok}&host=#{request.host}"
  end

  test 'should respond to json' do
    Search::UserSearch.any_instance.stubs(:search_by_id).returns true
    user = create(:user, confirmed_at: nil, is_complete: false)
    @request.env["devise.mapping"] = Devise.mappings[:user]
    set_host user.organization

    get :show, confirmation_token: user.instance_variable_get(:@raw_confirmation_token), format: :json
    assert_response :ok
    resp = get_json_response response
    assert_equal user.id, resp['id']

    # bad token
    get :show, confirmation_token: 'xxx', format: :json
    assert_response :bad_request
  end

  def confirm_setup
    @user = create(:user, confirmed_at: nil, is_complete: false)
    assert_equal false, @user.confirmed?
    @request.env["devise.mapping"] = Devise.mappings[:user]

    mailermock = mock()
    DeviseMandrillMailer.expects(:send_after_signup_email).
        with(anything(), 'web', 'email').returns(mailermock)
    mailermock.expects(:deliver_later)
  end

  test 'user signing up to a different org should not send after signup email' do
    org = create(:organization)
    user = create(:user, confirmed_at: nil, is_complete: false, organization: org)
    assert_not user.confirmed?
    @request.env["devise.mapping"] = Devise.mappings[:user]
    DeviseMandrillMailer.expects(:send_after_signup_email).never
    set_host user.organization
    get :show, confirmation_token: user.instance_variable_get(:@raw_confirmation_token)
    user.reload
    assert user.confirmed?
  end

  test 'should respond to html' do
    Search::UserSearch.any_instance.stubs(:search_by_id).returns true
    user = create(:user, confirmed_at: nil, is_complete: false)
    @request.env["devise.mapping"] = Devise.mappings[:user]
    set_host user.organization

    get :show, confirmation_token: 'xxx', format: :html
    assert_equal 'Invalid Request.', flash[:error]
  end
end

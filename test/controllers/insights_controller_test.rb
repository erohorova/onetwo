require 'test_helper'

class InsightsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  self.use_transactional_fixtures = false

  setup do
    # Closed org
    @org = create :organization
    @user = create :user, organization: @org

    link_resource = create :resource, type: 'Article', url: 'http://goo.go', image_url: 'http://bah.png'
    @lcard = create(:card, card_type: 'media', card_subtype: 'link',
      is_public: true, author_id: @user.id, title: 'some title',
      message: 'some message', resource_id: link_resource.id
    )
    set_host @org
  end


  test 'redirects signed in user to the resource link when an organization is closed' do
    sign_in @user

    get :visit, id: @lcard.id

    assert_response :redirect
    assert_redirected_to @lcard.resource.url
  end

  test 'redirects non-logged in user to the resource link when an organization is closed' do

    get :visit, id: @lcard.id

    assert_response :redirect
    assert_redirected_to @lcard.resource.url
  end

  test 'redirects signed in user to the resource link when an organization is open' do
    @org.update(is_open: true)
    @org.reload

    sign_in @user

    get :visit, id: @lcard.id

    assert_response :redirect
    assert_redirected_to @lcard.resource.url
  end

  test 'redirects logged in user to the content provider when an organization (okta-enabled) is closed' do
    @lcard.update(ecl_metadata: { 'source_type_name' => 'edcast_cloud','source_id' => '003569d2-6d05-4e93-83c9-54ee3cd538b3' })
    partner_center = create :partner_center, organization: @org, user: @user,integration: '003569d2-6d05-4e93-83c9-54ee3cd538b3'

    sign_in @user

    get :visit, id: @lcard.reload.id

    assert_response :redirect
    assert_redirected_to (partner_center.embed_link + '?RelayState=')
  end

  test 'redirects non-logged in user to the resource link when an organization (okta-enabled) is closed' do
    @lcard.update(ecl_metadata: { 'source_type_name' => 'edcast_cloud','source_id' => '003569d2-6d05-4e93-83c9-54ee3cd538b3' })
    partner_center = create :partner_center, organization: @org, user: @user,integration: '003569d2-6d05-4e93-83c9-54ee3cd538b3'

    @org.reload

    get :visit, id: @lcard.id

    assert_response :redirect
    assert_redirected_to (partner_center.embed_link + '?RelayState=')
  end

  test 'redirects logged in user to the content provider when an organization (okta-enabled) is open' do
    @lcard.update(ecl_metadata: { 'source_type_name' => 'edcast_cloud','source_id' => '003569d2-6d05-4e93-83c9-54ee3cd538b3' })
    partner_center = create :partner_center, organization: @org, user: @user,integration: '003569d2-6d05-4e93-83c9-54ee3cd538b3'

    sign_in @user

    get :visit, id: @lcard.reload.id

    assert_response :redirect
    assert_redirected_to (partner_center.embed_link + '?RelayState=')
  end

  test 'redirects non-logged in user to the resource link when an organization (okta-enabled) is open' do
    @lcard.update(ecl_metadata: { 'source_type_name' => 'edcast_cloud','source_id' => '003569d2-6d05-4e93-83c9-54ee3cd538b3' })
    partner_center = create :partner_center, organization: @org, user: @user,integration: '003569d2-6d05-4e93-83c9-54ee3cd538b3'
    @org.reload

    get :visit, id: @lcard.id

    assert_response :redirect
    assert_redirected_to (partner_center.embed_link + '?RelayState=')
  end

  test ':api /visit records click_through event on card when user is logged inå' do
    Analytics::ViewEventMetricRecorder.
      expects(:record_click_through_event).
      with(entity: @lcard, viewer: @user)

    sign_in @user
    get :visit, id: @lcard.id

    assert_response :redirectå
    assert_redirected_to @lcard.resource.url
  end

end

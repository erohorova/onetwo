require 'test_helper'

class GraphqlControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    @org = create :organization
    @user = create :user, organization: @org
    @ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_test.json"))

    api_v2_sign_in(@user)

    stub_request(:get, "http://localhost:9200/channels_test/_search").to_return(:status => 200, :body => "", :headers => {})
    stub_request(:get, "http://localhost:9200/edcast_users_test/_search").to_return(:status => 200, :body => "", :headers => {})

    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items/search")
      .to_return(body: @ecl_response.to_json, status: 200)
  end

  test '#execute for cards' do
    post :execute, query: graphql_query, format: :json

    fields = assigns(:fields)

    assert_empty HashDiff.diff(
      ActiveSupport::HashWithIndifferentAccess.new(search_params),
      ActiveSupport::HashWithIndifferentAccess.new(fields['lxp'])
    )
  end

  private

  def graphql_query
<<EOF
{
  card(q:"hello", from_date: "from_date", till_date: "till_date", cards_offset: 10, cards_limit: 20,
       channels_offset: 30, channels_limit: 40, users_offset: 50, users_limit: 60, sociative: "sociative",
       source_id: ["source1", "source2"], content_type: ["ct1", "ct2"], source_type_name: ["sn1", "sn2"],
       exclude_cards: ["ex1", "ex2"],
       posted_by: {followers: [1], following: [2], user_ids: [3]},
       liked_by: {followers: [4], following: [5], user_ids: [6]},
       commented_by: {followers: [7], following: [8], user_ids: [9]},
       bookmarked_by: {followers: [10], following: [11], user_ids: [12]}) {

    title
    lxp
  }
}
EOF
  end

  def search_params
    {q:"hello", from_date: "from_date", till_date: "till_date", cards_offset: 10, cards_limit: 20,
      channels_offset: 30, channels_limit: 40, users_offset: 50, users_limit: 60, sociative: "sociative",
      source_id: ["source1", "source2"], content_type: ["ct1", "ct2"], source_type_name: ["sn1", "sn2"],
      exclude_cards: ["ex1", "ex2"],
      posted_by: {followers: [1], following: [2], user_ids: [3]},
      liked_by: {followers: [4], following: [5], user_ids: [6]},
      commented_by: {followers: [7], following: [8], user_ids: [9]},
      bookmarked_by: {followers: [10], following: [11], user_ids: [12]}}
  end
end

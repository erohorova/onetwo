require 'test_helper'

class StreamsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    StreamCreateJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    @org = create(:organization)
    @client = create(:client, organization: @org)
    @group1 = create(:resource_group, organization: @org, client: @client)
    @user1 = create(:user, organization: @org)
    @user2 = create(:user, organization: @org)
    @faculty1 = create(:user, organization: @org)

    stub_request(:post, %r{http://localhost:9200/_bulk})

    @group1.add_member @user1
    @group1.add_member @user2
    @group1.add_admin @faculty1
    @question1 = create(:question, group: @group1, user: @user1)
    @question1.run_callbacks(:commit)
    @question2 = create(:question, group: @group1, user: @user1)
    @question2.run_callbacks(:commit)
    @answer1 = create(:answer, commentable: @question1, user: @user1)
    @answer1.run_callbacks(:commit)
    @vote1 = create(:vote, votable: @question1, voter: @user2)
    @approval1 = create(:approval, approvable: @answer1, approver: @faculty1)
    @approval1.run_callbacks(:commit)

    @another_group = create(:group, organization: @org)
    @another_group.add_member @user1
    @another_post = create(:question, user: @user1, group: @another_group)
    @another_post.run_callbacks(:commit)
  end

  def auth_params(group:nil, user:nil, role:'user')
    credential = group.valid_credential
    client_user = create(:clients_user, client: group.organization.client, user: user)
    api_key = credential.api_key
    shared_secret = credential.shared_secret
    timestamp = Time.now.to_i

    parts = [shared_secret, timestamp, client_user.client_user_id, role, group.client_resource_id].compact
    token = Digest::SHA256.hexdigest(parts.join('|'))

    {
        api_key: api_key,
        timestamp: timestamp,
        token: token,
        user_role: role,
        user_id: client_user.client_user_id,
        resource_id: group.client_resource_id
    }
  end

  test 'is mobile field should work' do
    mobile_post = create(:question, user: @user1, group: @another_group, is_mobile: true)
    mobile_post.run_callbacks(:commit)
    pg = create(:private_group, resource_group: @another_group)
    pgu = GroupsUser.create(group: pg, user: @user1, as_type: 'member')
    pgu.run_callbacks(:commit)

    # force some order
    pgs = Stream.where(item: pg).first
    assert pgs
    pgs.update_attributes(updated_at: Time.now)

    non_mobile_stream = Stream.where(item: @another_post).first
    assert non_mobile_stream
    non_mobile_stream.update_attributes(updated_at: 1.day.ago)

    mobile_stream = Stream.where(item: mobile_post).first
    assert mobile_stream
    mobile_stream.update_attributes(updated_at: 5.days.ago)

    sign_in @user1
    get :index, group_id: @another_group.id, format: :json
    assert_response :success
    resp = get_json_response(response)
    assert_equal 2, resp.size
    assert_equal [false, true], resp.map{|r| r['is_mobile']}
    sign_out @user1
  end

  test 'should include streams from a given group' do
    get :index, {group_id: @group1.id, format: :json}.merge(auth_params(group:@group1, user: @user1))
    assert_response :success
    json_response = get_json_response(response)
    assert_not_empty json_response

    json_response.each do |stream|
      assert stream['user']
      assert stream['item']
      assert stream['item_class']
      assert stream['indefinite_noun']
      assert stream['action']
      assert stream['created_at']
      assert stream['snippet']
      assert stream['message']
    end
  end

  test 'should return the latest 5 items if no last_fetch_time pasted in' do
    Stream.destroy_all
    streams = []
    (0..9).to_a.each do |i|
      question = create(:question, group: @group1, user: @user1)
      stream = create(:stream, item: question, group: @group1, user: @user1, action: 'asked', updated_at: i.hours.ago)
      streams << stream
    end

    get :index, {group_id: @group1.id, limit:5, format: :json}.merge(auth_params(group:@group1, user: @user1))

    json_response = get_json_response(response)

    assert_equal streams[0..4].map(&:id), json_response.map{|stream| stream['id']}

    assert_equal streams[0].reload.updated_at.utc.strftime('%Y-%m-%dT%H:%M:%S.%LZ'), response.headers['X-Last-Fetch-Timestamp']
  end

  test "pagination for streams" do
    Stream.destroy_all
    streams = []
    (0..9).to_a.each do |i|
      question = create(:question, group: @group1, user: @user1)
      stream = create(:stream, item: question, group: @group1, user: @user1, action: 'asked', updated_at: i.hours.ago)
      streams << stream
    end

    get :index, {group_id: @group1.id, limit:5, format: :json}.merge(auth_params(group:@group1, user: @user1))

    json_response = get_json_response(response)
    assert_equal streams[0..4].map(&:id), json_response.map{|stream| stream['id']}

    get :index, {group_id: @group1.id, page:1, limit:5, format: :json}.merge(auth_params(group:@group1, user: @user1))
    json_response = get_json_response(response)
    assert_equal streams[5..9].map(&:id), json_response.map{|stream| stream['id']}
  end

  test 'should return the stream all activities since last_fetch_time ' do
    Stream.destroy_all
    streams = []
    (0..9).to_a.each do |i|
      question = create(:question, group: @group1, user: @user1)
      stream = create(:stream, item: question, group: @group1, user: @user1, action: 'asked', updated_at: i.hours.ago)
      streams << stream
    end

    get :index, {group_id: @group1.id, limit: 5, last_fetch_time: streams[8].updated_at.utc.strftime('%Y-%m-%dT%H:%M:%S.%LZ'), format: :json}.merge(auth_params(group:@group1, user: @user1))

    json_response = get_json_response(response)

    assert_equal streams[0..7].map(&:id), json_response.map{|stream| stream['id']}
    assert_equal streams[0].reload.updated_at.utc.strftime('%Y-%m-%dT%H:%M:%S.%LZ'), response.headers['X-Last-Fetch-Timestamp']
  end


  test 'should return streams for curent context' do
    Stream.destroy_all

    #### testing for : if there is a context present in the request

    # declaring question which is hidden & stream for that
    question1 = create(:question, group: @group1, user: @user1, context_id: 1, hidden: 1)
    stream1 = create(:stream, item: question1, group: @group1, user: @user1, action: 'asked')

    # declaring question which is not hidden & stream for that
    question2 = create(:question, group: @group1, user: @user1, context_id: 1)
    stream2 = create(:stream, item: question2, group: @group1, user: @user1, action: 'posted')

    # getting response from the index function of streams_controller
    get :index, {group_id: @group1.id, context_id: 1, format: :json}.merge(auth_params(group:@group1, user: @user1))
    assert_response :success

    # getting JSON response of streams index functions used file is : views/streams/index.json.builder
    json_response = get_json_response(response)

    # all the stream id's from the json response
    stream_ids = json_response.collect { |s| s['id'] }

    # id's of all the questions which are returned in the stream resultset
    item_ids = json_response.collect { |s| s['item']['id'] }

    # checking if stream id's only contains stream id which belong to question 2 only
    assert_equal stream_ids, [stream2.id]

    # checking items_id's only contains id of question2 only
    assert_equal item_ids, [question2.id]

    #### testing for : if context is not present in the request

    # getting streams resultset & json response same as above
    get :index, {group_id: @group1.id, format: :json}.merge(auth_params(group:@group1, user: @user1))
    json_response = get_json_response(response)

    stream_item_ids = json_response.map{|a| a["item"]["id"]}

    # if all streams are present in the response whose item is not hidden
    # For e.g in this case "question2"
    assert_equal stream_item_ids, [question2.id]
  end

  test 'should return streams with original question id for 2-level comments' do
    Stream.destroy_all
    comment = create(:answer, commentable: @question1, user: @user1)
    reply = create(:answer, commentable: comment, user: @user1)
    stream = create(:stream, item: reply, group: @group1, user: @user1, action: 'commented')
    get :index, {group_id: @group1.id, format: :json}.merge(auth_params(group:@group1, user: @user1))
    json_response = get_json_response(response)
    assert json_response.first['item']["question"]["id"], @question1.id
  end
end

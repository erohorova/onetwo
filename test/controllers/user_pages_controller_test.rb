require 'test_helper'

class UserPagesControllerTest < ActionController::TestCase

  test 'should record event when user profile is visited' do
    org = create(:organization)
    loggedin_user = create(:user, organization: org)
    user = create(:user, organization: org, handle: '@testuser')
    metadata = {platform: 'web'}
    RequestStore.stubs(:read).returns(metadata)

    set_host(org)
    stub_time
    sign_in loggedin_user
    Analytics::MetricsRecorderJob.expects(:perform_later).with(
      has_entries(
        recorder: "Analytics::UserProfileVisitMetricsRecorder",
        org: Analytics::MetricsRecorder.org_attributes(org),
        timestamp: Time.now.to_i,
        actor: Analytics::MetricsRecorder.user_attributes(loggedin_user),
        visited_profile_user: Analytics::MetricsRecorder.user_attributes(user),
        additional_data: metadata
      )
    )

    get :check_handle, handle: user.handle, format: :json
    assert_response :ok
  end
end
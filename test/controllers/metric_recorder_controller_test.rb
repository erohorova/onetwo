require 'test_helper'

class MetricRecorderControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    Tracking.unstub :track
    MetricRecorderJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    @org = create(:organization)
    @user = create(:user, organization: @org)
    @card = create(:card, author: create(:user, organization: @org))
    MetricRecord.unstub(:create)
    set_host @org
  end

  test 'should log even if device id or user are not available' do
    MetricRecord.expects(:create).with(actor_type: 'user', actor_id: nil, object_id: @card.id, object_type: 'card', action: 'view.partial',
                                       device_id: nil, properties: nil, owner_type: 'user', owner_id: @card.author_id, platform: 'web',
                                       organization_id: nil, referer: nil, ecl_id: @card.ecl_id
    )
    post :create, act: 'view.partial', object_id: @card.id, object_type: 'card', format: :json
  end

  test 'should log the device id cookie if available' do
    device_id = 'device123'
    request.cookies[:_d] = device_id
    MetricRecord.expects(:create).with(actor_type: 'user', actor_id: nil, object_id: @card.id, object_type: 'card', action: 'view.partial',
                                      device_id: device_id, properties: nil, owner_type: 'user', owner_id: @card.author_id, platform: 'web',
                                      organization_id: nil, referer: nil, ecl_id: @card.ecl_id
                                    )
    post :create, act: 'view.partial', object_id: @card.id, object_type: 'card', format: :json
  end

  test 'should log the logged in user if available' do
    sign_in @user
    device_id = 'device123'
    request.cookies[:_d] = device_id
    MetricRecord.expects(:create).with(actor_type: 'user', actor_id: @user.id, object_id: @card.id, object_type: 'card', action: 'view.partial',
                                       device_id: device_id, properties: nil, owner_type: 'user', owner_id: @card.author_id, platform: 'web',
                                       organization_id: @user.organization_id, referer: nil, ecl_id: @card.ecl_id
    )

    post :create, act: 'view.partial', object_id: @card.id, object_type: 'card', format: :json
  end

  test 'should log the properties if passed in' do
    sign_in @user
    device_id = 'device123'
    request.cookies[:_d] = device_id
    MetricRecord.expects(:create).with(actor_type: 'user', actor_id: @user.id, object_id: @card.id, object_type: 'card', action: 'view.partial',
                                       device_id: device_id, properties: {'key1' => 'value1', 'key2' => 'value2'}, owner_type: 'user', owner_id: @card.author_id, platform: 'web',
                                       organization_id: @user.organization_id, referer: nil, ecl_id: @card.ecl_id
    )
    post :create, act: 'view.partial', object_id: @card.id, object_type: 'card', properties: {key1: 'value1', key2: 'value2'}, format: :json
  end

  test 'should log referer, organization_id if passed in' do
    sign_in @user
    device_id = 'device123'
    request.cookies[:_d] = device_id
    MetricRecord.expects(:create).with(actor_type: 'user', actor_id: @user.id, object_id: @card.id, object_type: 'card', action: 'view.partial',
                                       device_id: device_id, properties: {'key1' => 'value1', 'key2' => 'value2'}, owner_type: 'user', owner_id: @card.author_id, platform: 'web',
                                       organization_id: @user.organization_id, referer: 'http://edcast.com', ecl_id: @card.ecl_id

    )
    post :create, act: 'view.partial', object_id: @card.id, object_type: 'card', properties: {key1: 'value1', key2: 'value2'}, referer: 'http://edcast.com', format: :json
  end

end

require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers
  setup do
    @user = create(:user)
    @org = create(:organization)
    set_host @org
    request.cookies['_d'] = 'deviceid123'
  end

  test "should return onboarding data" do
    sign_in @user
    get :info, format: :json
    resp = get_json_response(response)

    assert_includes resp.keys, "onboarding_completed"
    assert_includes resp.keys, "step"
  end

  test "should return users country code based on location from cloudfront" do
    sign_in @user
    request.headers['CloudFront-Viewer-Country'] = "IN"

    get :info, format: :json
    resp = get_json_response(response)

    assert_equal "IN", resp["country_code"]
  end

  test "should return users default country code as US" do
    sign_in @user

    get :info, format: :json
    resp = get_json_response(response)

    assert_equal "US", resp["country_code"]
  end
end

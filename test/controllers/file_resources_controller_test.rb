require 'test_helper'

class FileResourcesControllerTest < ActionController::TestCase
  setup do
    @user = create(:user)
    @xls = Rails.root.join('test/fixtures/file_resources/excel.xls')
    @xlsx = Rails.root.join('test/fixtures/file_resources/excel.xlsx')
    @csv = Rails.root.join('test/fixtures/file_resources/csv_file.csv')
    @numbers = Rails.root.join('test/fixtures/file_resources/sample_numbers_file.numbers')

    @xls = Rack::Test::UploadedFile.new(@xls, 'application/excel')
    @xlsx = Rack::Test::UploadedFile.new(@xlsx, 'application/excel')
    @csv = Rack::Test::UploadedFile.new(@csv, 'text/csv')
    @numbers = Rack::Test::UploadedFile.new(@numbers, 'application/x-iwork-keynote-sffnumbers')

    api_v2_sign_in(@user)
  end

  test ':api should be able to upload csv' do
    post :create_v1, file: @csv, format: :json
    assert_response :created
  end

  test ':api should be able to upload xls' do
    post :create_v1, file: @xls, format: :json
    assert_response :created
  end

  test ':api should be able to upload xlsx' do
    post :create_v1, file: @xlsx, format: :json
    assert_response :created
  end

  test ':api should be able to upload numbers' do
    post :create_v1, file: @numbers, format: :json
    assert_response :created
  end
end
require 'test_helper'

class VotesControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'

    @user = create(:user)
    @user2 = create(:user)
    @group = create(:resource_group)
    @group.add_member @user
    @group.add_member @user2
    @post = create(:post, user: @user, group: @group)
    @comment = create(:comment, user: @user, commentable: @post)
    @question = create(:question, user: @user, group: @group)
    @answer = create(:answer, user: @user, commentable: @question)
    request.cookies['_d'] = 'deviceid123'
  end

  test "vote for a post" do
    sign_in @user2
    assert_difference [->{Vote.count}, ->{@post.reload.votes_count}] do
      post :create, {:post_id => @post.id}
    end
    assert_response :created
    sign_out @user2
  end

  test "vote for a question" do
    sign_in @user2
    assert_difference [->{Vote.count}, ->{@question.reload.votes_count}] do
      post :create, {:post_id => @question.id}
    end
    assert_response :created
    sign_out @user2
  end

  test "vote for an answer" do
    sign_in @user2
    assert_difference [->{Vote.count}, ->{@answer.reload.votes_count}] do
      post :create, {:comment_id => @answer.id}
    end
    assert_response :created
    sign_out @user2

  end

  test "vote for a comment" do
    request.env['HTTP_REFERER'] = 'http://edcast.com'

    Tracking.expects(:track_act).with(
        user_id: @user2.id,
        object: 'comment',
        object_id: @comment.id,
        action: 'like',
        platform: 'web',
        device_id: 'deviceid123',
        organization_id: @user2.organization_id,
        referer: 'http://edcast.com'
    )

    sign_in @user2
    assert_difference [->{Vote.count}, ->{@comment.reload.votes_count}] do
      post :create, {:comment_id => @comment.id}
    end
    assert_response :created
    sign_out @user2
  end

  class FullCommitTest < ActionController::TestCase
    include Devise::Test::ControllerHelpers
    self.use_transactional_fixtures = false
    setup do
      skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
      WebMock.disable!
    end

    teardown do
      WebMock.enable!
    end

    test "upvote and unupvote for a reply" do
      g, u = create_gu
      p = create(:post, user: u, group: g)
      c = create(:comment, commentable: p, user: u)
      reply = create(:comment, commentable: c, user: u)
      sign_in u
      UserContentsQueue.expects(:enqueue_for_user).never
      assert_no_difference [->{Stream.count}, ->{Notification.count}] do
        assert_difference [->{Vote.count}, ->{reply.reload.votes_count}] do
          post :create, comment_id: reply.id, format: :json
          assert_response :created
        end

        assert_difference [->{Vote.count}, ->{reply.reload.votes_count}], -1 do
          delete :destroy, :comment_id => reply.id, format: :json
          assert_response :ok
        end
      end
      sign_out u
    end
  end

  test "no duplicate votes" do
    sign_in @user2
    create(:vote, votable: @post, voter: @user2)
    assert_no_difference [->{Vote.count}, ->{@post.votes_count}] do
      post :create, {:post_id => @post.id}
    end
    sign_out @user2
  end

  test "status forbidden if user not signed in" do
    assert_no_difference ->{Vote.count} do
      post :create, {:post_id => @post.id, :format => :json}
    end
    assert_response :unauthorized
  end

  test "vote deleted forbidden if user not signed in" do
    assert_no_difference ->{Vote.count} do
      delete :destroy, {:post_id => @post.id, :format => :json}
    end
    assert_response :unauthorized
  end

  test "unvote for a post" do
    request.env['HTTP_REFERER'] = 'http://edcast.com'

    Tracking.expects(:track_act).with(
        user_id: @user2.id,
        object: 'post',
        object_id: @post.id,
        action: 'unlike',
        platform: 'web',
        device_id: 'deviceid123',
        organization_id: @user2.organization_id,
        referer: 'http://edcast.com'
    )

    sign_in @user2
    @vote = create(:vote, votable: @post, voter: @user2)
    assert_difference [->{Vote.count}, ->{@post.reload.votes_count}], -1 do
      delete :destroy, {:post_id => @post.id}
    end
    assert_response :ok
    sign_out @user2
  end

  test "unvote for an answer" do
    sign_in @user2
    @vote = create(:vote, votable: @answer, voter: @user2)
    assert_difference [->{Vote.count}, ->{@answer.reload.votes_count}], -1 do
      delete :destroy, {:comment_id => @answer.id}
    end
    assert_response :ok
    sign_out @user2
  end

  test "no score for anonymous post vote" do
    question = create(:question, user: @user, anonymous: true, group: @group)
    score = @user.score(@group)['score']
    sign_in @user2
    post :create, {:post_id => question.id }
    newscore = @user.score(@group)['score']
    assert_equal score, newscore
    sign_out @user2
  end

  test 'vote on a video stream' do
    Vote.any_instance.stubs(:record_card_unlike_event)

    vs = create(:wowza_video_stream, creator: @user)
    sign_in @user
    assert_difference ->{ Vote.count } do
      post :create, video_stream_id: vs.id, format: :json
      assert_response :created
    end

    # no double voting
    assert_no_difference ->{ Vote.count } do
      post :create, video_stream_id: vs.id, format: :json
      assert_response :unprocessable_entity
    end

    # now remove upvote
    assert_difference ->{Vote.count}, -1 do
      delete :destroy, video_stream_id: vs.id, format: :json
      assert_response :ok
    end

    # destroy non existent vote
    assert_no_difference ->{Vote.count} do
      delete :destroy, video_stream_id: vs.id, format: :json
      assert_response :bad_request
    end
  end
end

require 'test_helper'

class Cms::Api::CardPacksControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers
  setup do
    @org1 = create(:organization)
    @org1_admin = create(:user, organization_role: 'admin', organization: @org1)
    set_host @org1
    sign_in @org1_admin
  end

  test ':api simple index' do
    @user1 = create(:user, organization: @org1)

    #covers
    @pack_covers1 = create_list(:card, 2, card_type: 'pack', card_subtype: 'simple', author_id: @user1.id, title: 'pack cover', organization: @org1)
    @pack_covers1 << create(:card, card_type: 'pack', card_subtype: 'simple', author_id: @user1.id, title: 'pack cover', organization: @org1, state: 'archived')

    # topics
    @topics = []
    @topics << create(:tag, name: 'sunny')
    @topics << create(:tag, name: 'pizza')
    @topics << create(:tag, name: 'san francisco')

    # channels
    @channels = []
    @channels << create(:channel, label: 'pogo', organization: @org1)
    @channels << create(:channel, label: 'disney', organization: @org1)

    # org 1 packs
    @pack_covers1.each_with_index do |cover, idx|
      tags, channels = if idx.odd?
        [@topics.first(2), @channels.first(1)]
      else
        [ @topics.last(1), [@channels[1]] ]
      end
      add_tags_and_channels_to_cover cover, tags, channels
      add_cards_to_cover cover
    end

    #To be more explicit, not stubbing search_pathways_for_cms
    Search::CardSearch.any_instance.expects(:search_for_cms).
      with('', @org1.id, filter_params: {card_type: ['pack']}, limit: 10, offset: 0, sort: {key: 'created_at', order: 'desc'}).
        returns(Search::CardSearchResponse.new(
                    results: search_results(@pack_covers1),
                    total: 10,
                )).once

    get :index, format: :json
    assert_response :success
    resp = get_json_response(response)["results"]

    assert_same_elements @pack_covers1.map(&:id), resp.map{|r| r['id']}
    assert_same_elements @pack_covers1.map(&:id), resp.map{|r| r['id']}

    pack_config1_cover = resp.find{|r| r['id'] == @pack_covers1[1].id}
    assert_same_elements @channels.first(1).map{|ch| {'id' => ch.id, 'label' => ch.label}}, pack_config1_cover['channels']
    assert_same_elements @topics.first(2).map{|tg| {'id' => tg.id, 'name' => tg.name}}, pack_config1_cover['topics']
  end

  test ':api filters for index' do
    Search::CardSearch.any_instance.expects(:search_for_cms).
      with('', @org1.id, filter_params: {state: 'published', channel_ids: [1, 2], topics: ['topic1', 'topic2'], card_type: ['pack']}, limit: 10, offset: 0, sort: {key: 'created_at', order: 'desc'}).
        returns(Search::CardSearchResponse.new(
                    results: search_results([]),
                    total: 10,
                )).once
    get :index, state: 'published', channel_ids: [1, 2], topics: ['topic1', 'topic2'], format: :json
    assert_response :success
  end

  test 'should be able to filter by is_official' do
    Search::CardSearch.any_instance.expects(:search_for_cms).
      with('', @org1.id, filter_params: {is_official: true, card_type: ['pack']}, limit: 10, offset: 0, sort: {key: 'created_at', order: 'desc'}).
        returns(Search::CardSearchResponse.new(
                    results: search_results([]),
                    total: 10,
                )).once
    get :index, is_official: 'true', format: :json
    assert_response :success

    Search::CardSearch.any_instance.expects(:search_for_cms).
      with('', @org1.id, filter_params: {is_official: false, card_type: ['pack']}, limit: 10, offset: 0, sort: {key: 'created_at', order: 'desc'}).
        returns(Search::CardSearchResponse.new(
                    results: search_results([]),
                    total: 10,
                )).once
    get :index, is_official: 'false', format: :json
    assert_response :success
  end

  test ':api update channel and topics and promote' do
    @user1 = create(:user, organization: @org1)

    # topics
    @topics = []
    @topics << create(:tag, name: 'sunny')
    @topics << create(:tag, name: 'pizza')
    @topics << create(:tag, name: 'san francisco')

    # channels
    @channels = []
    @channels << create(:channel, label: 'pogo', organization: @org1)
    @channels << create(:channel, label: 'disney', organization: @org1)

    pack_cover = create(:card, card_type: 'pack', card_subtype: 'simple', author_id: @user1.id, title: 'pack cover', organization: @org1)
    CardPackRelation.add(cover_id: pack_cover.id, add_id: create(:card, author: @user1).id)
    pack_cover.publish!
    prev_update = pack_cover.updated_at

    refute pack_cover.is_official

    put :update, id: pack_cover.id, is_official: 'true', channel_ids: @channels.first(2).map(&:id), topics: @topics.first(2).map(&:name), format: :json

    resp = get_json_response response
    pack_cover.reload

    assert_response :ok
    assert_not_empty resp['channels']
    assert_not_empty resp['topics']

    assert_equal @channels.first(2).map {|ch| {'id' => ch.id, 'label' => ch.label}}, resp['channels']
    assert_equal @topics.first(2).map {|t| {'id' => t.id, 'name' => t.name}}, resp['topics']
    assert_not_equal prev_update, pack_cover.updated_at #reindex card after commit. trigger after commit by updating card
    assert pack_cover.is_official
    assert_not_nil pack_cover.promoted_at
  end

  test 'should un-promote card' do
    @user1 = create(:user, organization: @org1)

    pack_cover = create(:card, card_type: 'pack', card_subtype: 'simple', author_id: @user1.id, title: 'pack cover', organization: @org1)
    CardPackRelation.add(cover_id: pack_cover.id, add_id: create(:card, author: @user1).id)
    pack_cover.publish!
    pack_cover.update(is_official: true, promoted_at: Time.now)

    assert pack_cover.is_official
    put :update, id: pack_cover.id, is_official: 'false', format: :json

    resp = get_json_response response
    pack_cover.reload

    assert_response :ok
    refute pack_cover.is_official
    assert_nil pack_cover.promoted_at
  end

  test 'should add user taxonomy topics to card' do
    @user1 = create(:user, organization: @org1)

    pack_cover = create(:card, card_type: 'pack', card_subtype: 'simple', author_id: @user1.id, title: 'pack cover', organization: @org1)
    CardPackRelation.add(cover_id: pack_cover.id, add_id: create(:card, author: @user1).id)
    pack_cover.publish!

    put :update, id: pack_cover.id, user_taxonomy_topics: [{'label' => 'label1', 'path' => 'label.one'}], format: :json

    resp = get_json_response response
    pack_cover.reload

    assert_response :ok
    assert_equal [{'label' => 'label1', 'path' => 'label.one'}], resp['user_taxonomy_topics']
  end

  test 'should clear user taxonomy topics to card if user taxonomy is empty or nil' do
    @user1 = create(:user, organization: @org1)

    pack_cover = create(:card, card_type: 'pack', card_subtype: 'simple', author_id: @user1.id, title: 'pack cover', organization: @org1)
    CardPackRelation.add(cover_id: pack_cover.id, add_id: create(:card, author: @user1).id)
    pack_cover.publish!

    put :update, id: pack_cover.id, user_taxonomy_topics: [], format: :json

    resp = get_json_response response
    pack_cover.reload

    assert_response :ok
    assert_equal [], resp['user_taxonomy_topics']
  end
  test ':api should be able to perform publish/delete actions on pathways' do
    now = Time.now
    Time.stubs(:now).returns(now)

    org2 = create(:organization)
    user3 = create(:user, organization: org2, organization_role: 'admin')
    card3, card2, card1 = create_list(:card, 3, organization: org2, author: user3)

    pack_covers = create_list(:card, 4, card_type: 'pack', card_subtype: 'simple', author_id: nil, title: 'pack cover', organization: org2, state: 'archived')
    pack_covers[3].update_attributes(state: 'published')

    set_host(org2)
    sign_in user3
    pack_covers.first(2).map(&:id).each do |cid|
      Cms::Judgement.expects(:create_content_trail).with(cid, "publish", user3, "Card").returns(nil).once
    end

    pack_covers.each do |cover|
      [card3, card2, card1].each do |card|
        cover.add_card_to_pathway(card.id, "Card")
      end
    end

    post :publish, card_ids: pack_covers.first(2).map(&:id) << pack_covers[3].id
    assert_response :ok
    resp = get_json_response(response)
    assert_same_ids pack_covers.first(2).map(&:id), resp["published_ids"]
    assert_not_includes resp["published_ids"], pack_covers[3].id

    pack_covers.each {|pc| pc.reload}
    assert pack_covers[0].published?
    assert pack_covers[1].published?
    assert_equal now.to_i, pack_covers[0].published_at.to_i

    assert_not pack_covers[2].published?

    sign_out user3
  end

  test ':api should be able to delete pathways in an org #cms' do
    ContentsBulkDequeueJob.stubs(:perform_later)
    now = Time.now
    org2 = create(:organization)
    user3 = create(:user, organization: org2, organization_role: 'admin')

    pack_covers = create_list(:card, 4, card_type: 'pack', card_subtype: 'simple', author_id: nil, title: 'pack cover', organization: org2, state: 'published', published_at: now)

    set_host(org2)
    sign_in user3
    pack_covers.first(2).map(&:id).each do |cid|
      Cms::Judgement.expects(:create_content_trail).with(cid, "delete", user3, "Card").returns(nil).once
    end
    post :delete, card_ids: pack_covers.first(2).map(&:id)

    assert_response :ok
    resp = get_json_response(response)
    assert_same_ids pack_covers.first(2).map(&:id), resp["deleted_ids"]
    assert_not_includes resp["deleted_ids"], pack_covers[3].id

    deleted_pack_covers = Card.with_deleted.where(id: resp["deleted_ids"])
    assert deleted_pack_covers[0].deleted?
    assert deleted_pack_covers[1].deleted?
  end

  test 'should be able to unset channels and topics' do
    @user1 = create(:user, organization: @org1)

    pack_cover = create(:card, card_type: 'pack', card_subtype: 'simple', author_id: @user1.id, title: 'pack cover', organization: @org1)
    channels = create_list(:channel, 2, organization: @org1)
    tags = create_list(:tag, 2)
    pack_cover.channels << channels
    pack_cover.tags << tags

    patch :update, id: pack_cover.id, channel_ids: [], topics: [], format: :json
    assert_response :ok
    pack_cover.reload
    assert_equal [], pack_cover.channels
    assert_equal [], pack_cover.tags
    resp = get_json_response(response)
    assert_equal [], resp['topics']
  end
end

require 'test_helper'

class Cms::Api::UsersControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    # create_default_org
    @org = create(:organization)
    @org_admin = create(:user, organization: @org, organization_role: 'admin')
    set_host @org
    sign_in @org_admin
  end

  test ':api should update users' do

    # Verify users is not showcased
    assert_equal false, @org_admin.showcase

    # Verify we can update channels if we're an admin
    put :update, id: @org_admin.id, showcase: true, format: :json
    assert_response :ok
    @org_admin.reload
    assert @org_admin.showcase
  end

  test ':api should add roles to users' do
    user = create(:user, organization: @org)
    role = create(:role,name: "Channel Collabrator",organization: @org)
    put :assign_roles, id: user.id, role_ids: [role.id], format: :json
    assert_response :ok
    user.reload
    assert_equal [role], user.roles.to_a
  end

  test ':api should remove user roles' do
    user = create(:user, organization: @org)
    role = create(:role,name: "Channel Collabrator",organization: @org)
    role2 = create(:role,name: "Channel Collabrator2",organization: @org)

    user.roles = [role, role2]

    put :assign_roles, id: user.id, role_ids: [role.id], format: :json
    assert_response :ok
    user.reload
    assert_equal [role], user.roles.to_a
  end

  test ':api should add roles to users and update organization role' do

    stub_request(:post, "http://localhost:9200/_bulk").to_return(:status => 200, :body => "", :headers => {})

    org = create(:organization, enable_role_based_authorization: true)
    create_org_master_roles(org)

    org_admin = create(:user, organization: org)
    admin_role = org.roles.find_by_name("admin")
    org_admin.roles << admin_role

    set_host org
    sign_in org_admin

    user = create(:user, organization: org)
    put :assign_roles, id: user.id, role_ids: [admin_role.id], format: :json
    assert_response :ok
    user.reload
    assert_equal 'admin', user.organization_role
  end


  test ':api should remove roles from users and update organization role' do
    org = create(:organization, enable_role_based_authorization: true)
    create_org_master_roles(org)

    org_admin = create(:user, organization: org)
    admin_role = org.roles.find_by_name("admin")
    member_role = org.roles.find_by(name: 'member')
    org_admin.roles << admin_role

    set_host org
    sign_in org_admin

    # Create user and add admin, member role
    user = create(:user, organization: org)
    user.roles = [member_role, admin_role]
    user.update(organization_role: 'admin')

    # Remove admin role via api call
    put :assign_roles, id: user.id, role_ids: [member_role.id], format: :json
    assert_response :ok
    user.reload

    # Admin role should not be present for the user
    assert_not_includes user.roles.reload, admin_role
    # organization_role column should become member after admin role has been removed
    assert_equal 'member', user.organization_role
  end

  test 'should check whether file url is csv or not before preview' do
    post :preview_bulk_import, csv_file_url: "https://Sheet1.txt"

    resp = JSON.parse(response.body)

    assert_equal false, resp['status']
    assert_equal "Invalid file url extension.", resp['message']
  end

  test 'should check whether csv file data is UTF-8 Format' do
    stub_request(:get, "https://Sheet1.csv").
      with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.9.2'}).
      to_return(:status => 200, :body => "first_name,last_name,email,groups\nfirst_name,last_name,test@email.com,Technology", :headers => {})

    csv_file_url = "https://Sheet1.csv"
    csv_data = Faraday.get(csv_file_url).body

    csv_data.force_encoding('ISO-8859-1')
    post :preview_bulk_import, csv_file_url: "https://Sheet1.csv"
    resp = JSON.parse(response.body)

    assert_equal true, resp['status']
    assert_equal "CSV is successfully uploaded", resp['message']
  end

  test 'should check whether csv gets uploaded successfully despite having garbled characters' do
    stub_request(:get, "https://Sheet1.csv").
      with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.9.2'}).
      to_return(:status => 200, :body => "first_name,last_name,email,groups\ra¯k_2,ascsE¾,an\xA0W@gma.co,,,", :headers => {})

    csv_file_url = "https://Sheet1.csv"
    csv_data = Faraday.get(csv_file_url).body

    post :preview_bulk_import, csv_file_url: "https://Sheet1.csv"
    resp = JSON.parse(response.body)

    assert_equal true, resp['status']
    assert_equal "CSV is successfully uploaded", resp['message']
  end

  test 'should give invalid header error if bulk_suspend is true' do
    stub_request(:get, "https://Sheet1.csv").
      with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.9.2'}).
      to_return(:status => 200, :body => "first_name,last_name,email,groups\nfirst_name,last_name,test@email.com,Technology", :headers => {})

    csv_file_url = "https://Sheet1.csv"
    csv_data = Faraday.get(csv_file_url).body
    post :preview_bulk_import, csv_file_url: "https://Sheet1.csv", bulk_suspend: true
    resp = JSON.parse(response.body)

    assert_equal false, resp['status']
    assert_equal "Invalid CSV headers. Use format email, status", resp['message']
  end

  test 'should check whether Suspend users CSV get uploaded successfully' do
    stub_request(:get, "https://Sheet1.csv").
      with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.9.2'}).
      to_return(:status => 200, :body => "email,status\ntest@email.com,suspend", :headers => {})
    
    users = { "email": "test@email.com", "status": "suspend" }

    csv_file_url = "https://Sheet1.csv"
    csv_data = Faraday.get(csv_file_url).body

    post :preview_bulk_import, csv_file_url: "https://Sheet1.csv", bulk_suspend: true
    resp = JSON.parse(response.body)
    assert_equal true, resp['status']
    assert_equal "CSV is successfully uploaded", resp['message']   

    assert_equal users.values ,resp['users'][0].values 
  end

  test 'support comma-separated emails' do
    teams = create_list(:team, 2, organization: @org)
    post :preview_bulk_import, emails: ['abc@example.com', 'def@example.com'], groups: "#{teams.map(&:name).join(',')}"
    resp = JSON.parse(response.body)

    assert resp['status']
    assert resp['file_id']
    assert_equal ['abc@example.com', 'def@example.com'].to_set, resp['users'].collect { |u| u['email'] }.to_set
    assert_equal ["#{teams.map(&:name).join(',')}"] * 2, resp['users'].collect { |u| u['groups'] }.compact
    assert_empty resp['users'].collect { |u| u['first_name'] }.reject(&:blank?)
    assert_empty resp['users'].collect { |u| u['last_name'] }.reject(&:blank?)
  end

  test 'support user_details should not be empty' do
    teams = create_list(:team, 2, organization: @org)
    post :preview_bulk_import, user_details: { "first_name" => "pallav", "last_name" => "sharma", "email" => "pallav@edcast.com"}, groups: "#{teams.map(&:name).join(',')}"
    resp = JSON.parse(response.body)

    assert resp['status']
    assert resp['file_id']
    assert_not_empty resp['users'].first['first_name']
    assert_not_empty resp['users'].first['last_name']
    assert_not_empty resp['users'].first['email']
    assert_equal ["#{teams.map(&:name).join(',')}"], resp['users'].collect { |u| u['groups'] }.compact
  end

  test 'support user_details should not be empty check empty case' do
    teams = create_list(:team, 2, organization: @org)
    post :preview_bulk_import, user_details: { "last_name" => "sharma", "email" => "pallav@edcast.com"}, groups: "#{teams.map(&:name).join(',')}"
    resp = JSON.parse(response.body)

    refute resp['status']
    assert "User details first_name, last_name and email all are mandatory.", resp['message']
  end

  test 'support user_details' do
    teams = create_list(:team, 2, organization: @org)
    post :preview_bulk_import, user_details: { "first_name" => "pallav", "last_name" => "sharma", "email" => "pallav@edcast.com"}, groups: "#{teams.map(&:name).join(',')}"
    resp = JSON.parse(response.body)

    assert resp['status']
    assert resp['file_id']
    assert_equal "pallav", resp['users'].first['first_name']
    assert_equal "sharma", resp['users'].first['last_name']
    assert_equal "pallav@edcast.com", resp['users'].first['email']
    assert_equal ["#{teams.map(&:name).join(',')}"], resp['users'].collect { |u| u['groups'] }.compact
  end

  test 'should check csv headers before preview' do
    # malformed response
    stub_request(:get, "https://Sheet1.csv").
      with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.9.2'}).
      to_return(:status => 200, :body => "[\"xyz\"]", :headers => {})

    post :preview_bulk_import, csv_file_url: "https://Sheet1.csv"
    resp = JSON.parse(response.body)

    assert_equal false, resp['status']
    assert_match /Illegal quoting in line/, resp['message']

    # invalid headers
    stub_request(:get, "https://Sheet1.csv").
      with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.9.2'}).
      to_return(:status => 200, :body => "xyz,pqr,email,groups\nfirst_name,last_name,test@email.com,Technology", :headers => {})

    post :preview_bulk_import, csv_file_url: "https://Sheet1.csv"
    resp = JSON.parse(response.body)

    assert_equal false, resp['status']
    assert_equal resp['message'], "Invalid CSV headers. Use format first_name, last_name, email, groups (optional), picture_url (optional), password (optional)"
  end

  test 'should not give error if optional header order is different' do
    stub_request(:get, "https://Sheet1.csv").
      with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.9.2'}).
      to_return(:status => 200, :body => "first_name,last_name,email,password\nfirst_name,last_name,test@email.com,Password@123", :headers => {})

    users = { "first_name": "first_name", "last_name": "last_name", "email": "test@email.com", "password": "Password@123" }

    post :preview_bulk_import, csv_file_url: "https://Sheet1.csv"
    resp = JSON.parse(response.body)
    assert_equal true, resp['status']
    assert_equal resp['message'], "CSV is successfully uploaded"
    assert_equal resp["users"][0].values, users.values
  end

  test 'should send preview of uploaded csv' do
    stub_request(:get, "https://Sheet1.csv").
      with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.9.2'}).
      to_return(:status => 200, :body => "first_name,last_name,email,groups\nfirst_name,last_name,test@email.com,Technology", :headers => {})

    users = { "first_name": "first_name", "last_name": "last_name", "email": "test@email.com", "groups": "Technology" }

    post :preview_bulk_import, csv_file_url: "https://Sheet1.csv"
    resp = JSON.parse(response.body)
    assert_equal true, resp['status']
    assert_equal resp['message'], "CSV is successfully uploaded"
    assert_equal resp["users"][0].values, users.values
  end

  test 'should trigger job to import users (V2) from the uploaded CSV for non-SSO Users' do
    invitation_file = create :invitation_file, sender_id: @org_admin.id,
      invitable: @org, data: "email,name@edact.com", roles: "member"

    options = {
      send_welcome_email: true,
      channel_ids: [],
      team_ids: nil,
      onboarding_options: nil,
      invitable: false
    }
    BulkImportUsersJobV2.expects(:perform_later).with({
      file_id: invitation_file.id.to_s,
      options: options,
      admin_id: @org_admin.id
    }).once

    put :bulk_upload, file_id: invitation_file.id, send_welcome_email: true, invitable: false, channel_ids: [], user_import_version: 'v2'

    assert_equal Hash.new, JSON.parse(response.body)
  end

  test 'should trigger job to import users from the uploaded CSV for SSO Users' do
    invitation_file = create :invitation_file, sender_id: @org_admin.id,
      invitable: @org, data: "email,name@edact.com", roles: "member"
    options = {
      send_welcome_email: true,
      channel_ids: [],
      team_ids: nil,
      onboarding_options: nil,
      invitable: true
    }
    BulkImportUsersJob.expects(:perform_later).with({
      file_id: invitation_file.id.to_s,
      options: options,
      admin_id: @org_admin.id
    }).once

    put :bulk_upload, file_id: invitation_file.id, send_welcome_email: true, invitable: true, channel_ids: []

    assert_equal Hash.new, JSON.parse(response.body)
  end

  test 'should trigger job to update status for users from uploaded Suspend Users CSV ' do 
    unstub_background_jobs
    invitation_file = create :invitation_file, sender_id: @org_admin.id,
      invitable: @org, data: "email,status\njane@edcast.com,suspend\natish+1@edcast.com,Active\natish+3@edcast.com,Inactive", roles: "member"

    BulkSuspendUsersJob.expects(:perform_later).with({
        file_id: invitation_file.id.to_s,
        admin_id: @org_admin.id
    }).once

    put :bulk_suspend_upload, file_id: invitation_file.id
    assert_equal Hash.new, JSON.parse(response.body)
    stub_background_jobs
  end

  test 'should update send_invite_email value to true when send_welcome_email params value is true' do
    invitation_file = create :invitation_file, sender_id: @org_admin.id,
      invitable: @org, data: "email,name@edact.com", roles: "member"

    options = {
      send_welcome_email: true,
      channel_ids: [],
      team_ids: nil,
      onboarding_options: nil,
      invitable: false
    }
    BulkImportUsersJobV2.expects(:perform_later).with({
      file_id: invitation_file.id.to_s,
      options: options,
      admin_id: @org_admin.id
    }).once

    put :bulk_upload, file_id: invitation_file.id, send_welcome_email: true, invitable: false, channel_ids: [], user_import_version: 'v2'

    assert_equal true, invitation_file.reload.send_invite_email
  end

  test 'should update send_invite_email value to false when send_welcome_email params value is false' do
    invitation_file = create :invitation_file, sender_id: @org_admin.id,
      invitable: @org, data: "email,name@edact.com", roles: "member"

    options = {
      send_welcome_email: false,
      channel_ids: [],
      team_ids: nil,
      onboarding_options: nil,
      invitable: false
    }
    BulkImportUsersJobV2.expects(:perform_later).with({
      file_id: invitation_file.id.to_s,
      options: options,
      admin_id: @org_admin.id
    }).once

    put :bulk_upload, file_id: invitation_file.id, send_welcome_email: false, invitable: false, channel_ids: [], user_import_version: 'v2'

    assert_equal false, invitation_file.reload.send_invite_email
  end

  test 'should allow team admin to invite new users if team_id is present in the params' do
    team = create(:team, organization: @org)
    user = create(:user, organization: @org)
    team.add_admin(user)
    sign_in user

    post :preview_bulk_import, emails: ['abc@example.com', 'def@example.com'], team_id: team.id
    resp = JSON.parse(response.body)
    assert_response :success
    assert_equal 'CSV is successfully uploaded',resp['message']

    opts = {
      channel_ids: nil,
      invitable:   true,
      onboarding_options: nil,
      send_welcome_email: false,
      team_ids:  ["#{team.id}"]
      }

    BulkImportUsersJob.expects(:perform_later).with({
      file_id: resp['file_id'].to_s,
      options: opts,
      admin_id: user.id
    }).once

    put :bulk_upload, file_id: resp['file_id'], send_welcome_email: false, team_ids: [team.id], team_id: team.id, invitable: true
    assert_response :success
  end

  test 'should not allow other team members to access the bulk_upload api' do
    team = create(:team, organization: @org)
    user = create(:user, organization: @org)
    team.add_member(user)

    sign_in user
    post :preview_bulk_import, emails: ['abc@example.com', 'def@example.com'], team_id: team.id
    assert_response :not_found

    put :bulk_upload, file_id: 2, send_welcome_email: false, team_ids: [team.id], team_id: team.id, invitable: true
    assert_response :not_found
  end
end

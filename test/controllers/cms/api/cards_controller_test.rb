require 'test_helper'

class Cms::Api::CardsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    @org = create(:organization)
    @org_admin = create(:user, organization_role: 'admin', organization: @org)
    set_host(@org)
    sign_in @org_admin
  end

  def index_setup
    @org_author = create(:user, organization: @org)
    @link = create(:resource, type: 'Article', url: 'http://url-link.com', image_url: 'http://imageurl.com', title: 'link title', description: 'link description', site_name: 'link site')
    @video = create(:resource, type: 'Video', url: 'http://url-video.com', image_url: 'http://imageurl.com', video_url: 'http://videourl.com', embed_html: '<iframe>blah</iframe>', title: 'video title', description: 'video description')
    @image = create(:resource, type: 'Image', url: 'http://url.com', image_url: 'http://someimage.png', title: 'image title', description: 'image description')

    pic = Rails.root.join('test/fixtures/images/logo.png')
    @fr1 = create(:file_resource, attachment: pic.open)

    @some_channels = create_list(:channel, 2, organization: @org)
    @some_tags = create_list(:tag, 2)

    @all_cards = []

    @all_cards << @media_link_card = create(:card, card_type: 'media', card_subtype: 'link', resource_id: @link.id, title: 'link card', message: 'link card message', state: 'new', organization: @org, author_id: @org_author.id)
    @media_link_card.tags << @some_tags
    @media_link_card.channels << @some_channels

    @all_cards << @media_video_card = create(:card, card_type: 'media', card_subtype: 'video', resource_id: @video.id, title: 'video card', message: 'video card message', organization: @org, author_id: @org_author.id)
    @media_video_card.tags << @some_tags.first
    @media_video_card.channels << @some_channels.first

    @all_cards << @media_image_card_from_image_url = create(:card, card_type: 'media', card_subtype: 'image', resource_id: @image.id, title: 'image card from image url', message: 'image card message url', organization: @org, author_id: @org_author.id)
    @all_cards << @media_image_card_from_image_upload = create(:card, card_type: 'media', card_subtype: 'image', title: 'image card from file upload', message: 'image card message file', organization: @org, author_id: @org_author.id)
    @media_image_card_from_image_upload.file_resources << @fr1

    # Should be ignored for now
    @all_cards << @poll_card_basic = create(:card, card_type: 'poll', card_subtype: 'text', title: 'image card from image url', message: 'image card message url', organization: @org, author_id: @org_author.id)
    @all_cards << @course_card = create(:card, card_type: 'course', card_subtype: 'link', organization: @org, author_id: @org_author.id)

  end

  teardown do
  end

  test ':api simple index' do
    index_setup
    content_object_card = create(:card, card_type: 'media', card_subtype: 'image')
    @all_cards << content_object_card
    Search::CardSearch.any_instance.expects(:search_for_cms).
      with('', @org.id, filter_params: {card_type: ['media', 'course', 'poll'], state: ['new','archived','published']}, limit: 10, offset: 0, sort: {key: 'created_at', order: 'desc'}).
        returns(Search::CardSearchResponse.new(
                    results: search_results(@all_cards),
                    total: 15,
                )).once

    get :index, format: :json
    assert_response :success
    resp = get_json_response(response)
    assert_same_elements @all_cards.map(&:id), resp['cards'].map{|r| r['id']}
    media_link_card_response = resp['cards'].find{|r| r['id'] == @media_link_card.id}
    assert_same_elements @some_channels.map{|ch| {'id' => ch.id, 'label' => ch.label}}, media_link_card_response['channels']
    assert_same_elements @some_tags.map{|tg| {'id' => tg.id, 'name' => tg.name}}, media_link_card_response['topics']
    assert_equal media_link_card_response['author_name'], @org_author.name

    assert_equal(@link.as_json(only: [:title, :description, :site_name, :image_url]).merge({"embed_html" => nil, "video_url" => nil}), media_link_card_response['resource'])

    media_video_card_response = resp['cards'].find{|r| r['id'] == @media_video_card.id}
    assert_equal(@video.as_json(only: [:title, :description, :site_name, :image_url, :video_url, :embed_html]), media_video_card_response['resource'])
    assert_equal media_video_card_response['author_name'], @org_author.name

    media_image_card_from_image_upload_response = resp['cards'].find{|r| r['id'] == @media_image_card_from_image_upload.id}
    assert_equal @fr1.url, media_image_card_from_image_upload_response['resource']['image_url']

    media_image_card_from_image_url_response = resp['cards'].find{|r| r['id'] == @media_image_card_from_image_url.id}
    assert_equal @image.image_url, media_image_card_from_image_url_response['resource']['image_url']

    assert resp['cards'].find{|r| r['id'] == @poll_card_basic.id}
    assert resp['cards'].find{|r| r['id'] == @course_card.id}
  end

  test ':api filters for index' do
    Search::CardSearch.any_instance.expects(:search_for_cms).
      with('', @org.id, filter_params: {state: ['published'], card_template: ['video'], channel_ids: [1, 2], topics: ['topic1', 'topic2'], card_type: ['media', 'course', 'poll']}, limit: 10, offset: 0, sort: {key: 'created_at', order: 'desc'}).
        returns(Search::CardSearchResponse.new(
                    results: search_results([]),
                    total: 15,
                )).once
    get :index, state: 'published', channel_ids: [1, 2], topics: ['topic1', 'topic2'], type: 'video', format: :json
    assert_response :success
  end

  test ':api sort for index' do
    Search::CardSearch.any_instance.expects(:search_for_cms).
      with('', @org.id, filter_params: {state: ['published'], card_type: ['media', 'course', 'poll']}, limit: 10, offset: 0, sort: {key: :title, order: :asc}).
        returns(Search::CardSearchResponse.new(
                    results: search_results([]),
                    total: 15,
                )).once
    get :index, state: 'published', sort: :title, order: :asc, format: :json
    assert_response :success
  end

  test ':api simple update' do
    index_setup
    Card.any_instance.stubs(:remove_from_queue_and_reenqueue)
    new_channel = create(:channel, organization: @org)
    new_tag_name = "New Tag"
    assert_same_elements @some_tags, @media_link_card.tags
    new_resource_params = {"title" => "new link title", "description" => "new link description", "image_url" => "http://newimage.png"}
    patch :update, id: @media_link_card.id, is_official: "true", title: 'edited title', message: "edited message", channel_ids: [@some_channels.first.id, new_channel.id], topics: [@some_tags.first.name, new_tag_name], resource: new_resource_params, format: :json
    assert_response :ok
    @media_link_card.reload
    assert_equal "edited title", @media_link_card.title
    assert_equal "edited message", @media_link_card.message
    assert_same_elements [@some_channels.first, new_channel].map(&:id), @media_link_card.channel_ids
    assert_same_elements [@some_tags.first.name, new_tag_name], @media_link_card.tags.map(&:name)
    assert_equal new_resource_params, @media_link_card.resource.as_json(only: [:title, :description, :image_url])
    assert @media_link_card.is_official

    patch :update, id: @media_link_card.id, channel_ids: [], topics: [], format: :json
    assert_response :ok
    @media_link_card.reload
    assert_equal [], @media_link_card.tags
    assert_equal [], @media_link_card.channels
    assert @media_link_card.is_official # No change to is_official

    # duplicate topics
    patch :update, id: @media_link_card.id, is_official: "false", channel_ids: [new_channel.id, new_channel.id], topics: [new_tag_name, new_tag_name], format: :json
    assert_response :ok
    @media_link_card.reload
    assert_equal [new_tag_name], @media_link_card.tags.map(&:name)
    assert_equal [new_channel], @media_link_card.channels
    refute @media_link_card.is_official

    # file upload card update
    stub_request(:get, "http://new-image.png/").to_return(:status => 200, :body => File.new(Rails.root.join('test/fixtures/images/architecture.jpg')), headers: { content_type:'image/jpg' })
    patch :update, id: @media_image_card_from_image_upload.id, resource: {image_url: 'http://new-image.png'}, format: :json
    assert_response :ok
    @media_image_card_from_image_upload.reload
    # Make sure it is a different image
    assert_not_equal @fr1.id, @media_image_card_from_image_upload.file_resources.first.id

    # image card from image url update
    patch :update, id: @media_image_card_from_image_url.id, resource: {image_url: 'http://new-image.png'}, format: :json
    assert_response :ok
    @media_image_card_from_image_url.reload
    assert_equal 'http://new-image.png', @media_image_card_from_image_url.resource.image_url

    # video card update
    patch :update, id: @media_video_card.id, resource: {image_url: 'http://new-image.png'}, format: :json
    assert_response :ok
    @media_video_card.reload
    assert_equal 'http://new-image.png', @media_video_card.resource.image_url
    assert_equal @video.attributes.except("image_url", "created_at", "updated_at"), @media_video_card.resource.attributes.except("image_url", "created_at", "updated_at")
  end

  test "update should accept prices attributes" do
    Card.any_instance.stubs(:remove_from_queue_and_reenqueue)
    card = create(:card, organization_id: @org.id, author_id: @org_admin.id)

    patch :update, id: card.id, is_paid: "true", prices_attributes: [{amount: 12, currency: "INR"}], format: :json
    assert_response :ok
    resp = get_json_response(response)

    assert true, resp["is_paid"]
    assert_not_empty resp['prices']
  end

  test "should not update if is_paid  is false" do
    Card.any_instance.stubs(:remove_from_queue_and_reenqueue)
    card = create(:card, organization_id: @org.id, author_id: @org_admin.id)

    patch :update, id: card.id, is_paid: "false", prices_attributes: [{amount: 12, currency: "INR"}], format: :json
    assert_response :ok
    resp = get_json_response(response)
    refute resp["is_paid"]
    assert_empty resp['prices']
  end

  test 'should promote poll card with or without attempts' do
    @org_author = create(:user, organization: @org)

    Card.any_instance.stubs(:remove_from_queue_and_reenqueue)

    poll = create(:card, card_type: "poll", is_public: true, organization_id: @org.id, author_id: @org_author.id)
    opt1 = create(:quiz_question_option, label: 'opt 1', is_correct: false, quiz_question: poll)
    opt2 = create(:quiz_question_option, label: 'opt 2', is_correct: true, quiz_question: poll)

    user2 = create(:user, organization_id: @org.id)
    attempt = create(:quiz_question_attempt, user: user2, quiz_question: poll, selections: [opt1.id])
    patch :update, id: poll.id, is_official: true, format: :json
    assert_response :ok
    resp = get_json_response(response)
    poll.reload

    assert_equal poll.is_official, resp["is_official"]
    assert_not_nil resp['promoted_at']
  end

  test 'should return title if different from message' do
    @org_author = create(:user, organization: @org)
    card = create(:card,
      card_type: "media",
      is_public: true,
      organization_id: @org.id,
      author_id: @org_author.id
    )
    title = 'new-title-card'
    patch :update, id: card.id, title: title, format: :json
    assert_response :ok
    resp = get_json_response(response)
    card.reload

    assert_equal card.title, resp["title"]
  end

  test 'should return blank title if equal from message' do
    @org_author = create(:user, organization: @org)
    card = create(:card,
      card_type: "media",
      is_public: true,
      organization_id: @org.id,
      author_id: @org_author.id
    )

    patch :update, id: card.id, title: card.message, format: :json
    assert_response :ok
    resp = get_json_response(response)
    card.reload

    assert_equal '', resp["title"]
  end

  test ':api publish action' do
    now = Time.now

    c1, c2, c3, c4 = create_list(:card, 4, card_type: 'media', card_subtype: 'link', state: 'new', published_at: nil, organization: @org, author_id: nil)
    c3.update_attributes(state: 'published', published_at: now)

    Time.stubs(:now).returns(now)
    [c1.id, c2.id].each do |cid|
      Cms::Judgement.expects(:create_content_trail).with(cid, "publish", @org_admin, "Card").returns(nil).once
    end
    post :publish, card_ids: [c1.id, c2.id, c3.id], format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_same_ids [c1.id, c2.id], resp["ids"]
    assert_not_includes resp["ids"], c3.id
    [c1, c2, c3, c4].each {|c| c.reload}
    assert c1.published?
    assert c2.published?
    assert c3.published?
    refute c4.published?

    [c1, c2].each do |c|
      assert_equal now.to_i, c.published_at.to_i
    end
    assert_nil c4.published_at
  end

  test ':api delete action' do
    ContentsBulkDequeueJob.stubs(:perform_later)
    c1, c2, c3 = create_list(:card, 3, card_type: 'media', card_subtype: 'link', state: 'new', organization: @org, author_id: nil)
    [c1.id, c2.id].each do |cid|
      Cms::Judgement.expects(:create_content_trail).with(cid, "delete", @org_admin, "Card").returns(nil).once
    end
    post :delete, card_ids: [c1.id, c2.id], format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_same_ids [c1.id, c2.id], resp["ids"]
    deleted_cards = Card.with_deleted.where(id: resp["ids"])

    assert deleted_cards[0].deleted?
    assert deleted_cards[1].deleted?
    assert_not c3.deleted?
  end

  test ':api archive action' do
    ContentsBulkDequeueJob.stubs(:perform_later)
    c1, c2, c3 = create_list(:card, 3, card_type: 'media', card_subtype: 'link', state: 'new', organization: @org, author_id: nil)
    c4 = create(:card, card_type: 'media', card_subtype: 'link', state: 'archived', organization: @org, author_id: nil)
    [c1.id, c2.id].each do |cid|
      Cms::Judgement.expects(:create_content_trail).with(cid, "archive", @org_admin, "Card").returns(nil).once
    end
    post :archive, card_ids: [c1.id, c2.id, c4.id], format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_same_ids [c1.id, c2.id], resp["ids"]
    # should not archive already archived card
    assert_not_includes resp["ids"], c4.id

    [c1, c2, c3].each {|c| c.reload}
    assert c1.archived?
    assert c2.archived?
    assert_not c3.archived?

  end

  test 'should track content publishing' do
    org_author = create(:user, organization: @org)
    card = create(:card, organization: @org, state: 'new', author_id: org_author.id)
    request.cookies[:_d] = 'device123'
    request.env['HTTP_REFERER'] = 'http://example.com'

    Tracking.expects(:track_act).once

    post :publish, card_ids: [card.id], format: :json
    assert_response :ok
    assert card.reload.published?
  end

  test 'should be able to promote ecl card' do
    Card.any_instance.stubs(:remove_from_queue_and_reenqueue)

    ecl_id = "rerf-rfde"

    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id}").
      to_return(status: 200, body: get_ecl_card_response(ecl_id).to_json)

    assert_difference -> {Card.count} do
      put :update, id: "ECL-#{ecl_id}", is_official: true, format: :json
      assert_response :ok
    end

    card = Card.last
    assert card.is_official
    assert_not_nil card.promoted_at
  end
end

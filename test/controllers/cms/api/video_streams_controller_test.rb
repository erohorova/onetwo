require 'test_helper'

class Cms::Api::VideoStreamsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    @org = create(:organization)
    @org_admin = create(:user, organization_role: 'admin', organization: @org)
    set_host(@org)
    sign_in @org_admin
  end

  def index_setup
    @all_streams = []
    @org_user = create(:user, organization: @org)
    @all_streams << @live_stream = create(:wowza_video_stream, status: 'live', creator: @org_user)
    @all_streams << @upcoming_stream = create(:wowza_video_stream, status: 'upcoming', start_time: Time.now.utc + 1.day, creator: @org_user)
    @all_streams << @past_stream = create(:wowza_video_stream, status: 'past', creator: @org_user)
    @recording = create(:recording, mp4_location: 'something', hls_location: 'something', transcoding_status: 'available', video_stream: @past_stream)

    @some_channels = create_list(:channel, 2, organization: @org)
    @some_tags = create_list(:tag, 2)

    @live_stream.channels << @some_channels
    @live_stream.tags << @some_tags

    @past_stream.channels << @some_channels.first
    @past_stream.tags << @some_tags.first
  end

  teardown do
  end

  test ':api simple index' do
    index_setup
    Search::VideoStreamSearch.any_instance.expects(:search_for_cms).
      with('', @org.id, filter_params: {}, limit: 10, offset: 0, sort: {key: 'created_at', order: 'desc'}).
        returns(Search::VideoStreamSearchResponse.new(
                    results: search_results(@all_streams),
                    total: 15,
                )).once

    get :index, format: :json
    assert_response :success
    resp = get_json_response(response)["results"]
    assert_same_elements @all_streams.map(&:id), resp.map{|r| r['id']}
  end

  test 'index to not be accessible to non org/team content admin' do
    user = create(:user, organization: @org, organization_role: 'member')
    sign_in user

    index_setup
    Search::VideoStreamSearch.any_instance.expects(:search_for_cms).
      with('', @org.id, filter_params: {}, limit: 10, offset: 0, sort: {key: 'created_at', order: 'desc'}).
        returns(Search::VideoStreamSearchResponse.new(
                    results: search_results(@all_streams),
                    total: 15,
                )).never
    get :index, format: :json
    assert_response :not_found
  end

  test ':api filters for index' do
    Search::VideoStreamSearch.any_instance.expects(:search_for_cms).
      with('', @org.id, filter_params: {status: 'live', state: 'published', channel_ids: [1, 2], topics: ['topic1', 'topic2']}, limit: 10, offset: 0, sort: {key: 'created_at', order: 'desc'}).
        returns(Search::VideoStreamSearchResponse.new(
                    results: search_results([]),
                    total: 15,
                )).once
    get :index, status: 'live', state: 'published', channel_ids: [1, 2], topics: ['topic1', 'topic2'], format: :json
    assert_response :success
  end

  test 'sort for index' do
    Search::VideoStreamSearch.any_instance.expects(:search_for_cms).
      with('', @org.id, filter_params: {status: 'live', state: 'published', topics: ['topic1', 'topic2']}, limit: 10, offset: 0, sort: {key: 'name', order: 'desc'}).
        returns(Search::VideoStreamSearchResponse.new(
                    results: search_results([]),
                    total: 15,
                )).once
    get :index, status: 'live', state: 'published', topics: ['topic1', 'topic2'], sort: 'name', order: 'desc', format: :json
    assert_response :success
  end

  test 'filter bu group_id for index' do
    role = @org.roles.create(name: 'member')
    create(:role_permission, role: role, name: Permissions::MANAGE_GROUP_CONTENT)
    user = create(:user, organization: @org)
    team = create(:team, organization: @org)
    role.add_user(user)
    team.add_member(@org_admin)
    team.add_sub_admin(user)
    create(:wowza_video_stream, status: 'live', creator: @org_user, organization: @org)

    sign_in user
    Search::VideoStreamSearch.any_instance.expects(:search_for_cms).
      with('', @org.id, filter_params: {
        status: 'live',
        state: 'published',
        creator_id: [@org_admin.id, user.id],
        channel_ids: []
      }, limit: 10, offset: 0, sort: {key: 'name', order: 'desc'}).
      returns(
        Search::VideoStreamSearchResponse.new(
          results: search_results([]),
          total: 15,
      )).once
    get :index, {
      status: 'live',
      state: 'published',
      group_id: [team.id],
      sort: 'name',
      order: 'desc',
      format: :json
    }
    assert_response :success
  end

  test ':api publish action' do
    now = Time.now
    v1, v2, v3, v4 = create_list(:wowza_video_stream, 4, state: 'deleted')
    v3.update_attributes(state: 'published')

    Time.stubs(:now).returns(now)
    [v1, v2, v3].map(&:id).each do |vid|
      Cms::Judgement.expects(:create_content_trail).with(vid, "publish", @org_admin, "VideoStream").returns(nil).once
    end
    post :publish, video_stream_ids: [v1.id, v2.id, v3.id], format: :json
    assert_response :ok
    [v1, v2, v3, v4].each {|v| v.reload}
    assert v1.published?
    assert v2.published?
    assert v3.published?
    refute v4.published?
  end

  test ':api delete action' do
    Card.any_instance.stubs(:remove_card_from_queue)
    Card.any_instance.stubs(:delete_ecl_card)

    v1, v2, v3 = create_list(:wowza_video_stream, 3, state: 'published')
    [v1, v2].map(&:id).each do |vid|
      Cms::Judgement.expects(:create_content_trail).with(vid, "delete", @org_admin, "VideoStream").returns(nil).once
    end
    post :delete, video_stream_ids: [v1.id, v2.id], format: :json
    assert_response :ok
    [v1, v2, v3].each {|v| v.reload}
    assert v1.deleted?
    assert v2.deleted?
    assert_not v3.deleted?
  end

  test ':api update streams' do
    index_setup
    old_image = @past_stream.image
    new_channel = create(:channel, organization: @org)
    new_tag = Tag.create(name: 'new tag')
    stub_request(:get, 'http://new-image.png/').
      to_return(status: 200,
                body: File.new(Rails.root.join('test/fixtures/images/architecture.jpg')),
                headers: { content_type: 'image/jpg' })

    patch :update, id: @past_stream.id, is_official: true, name: 'edited name',
      channel_ids: [@some_channels.first, new_channel].map(&:id),
      topics: [@some_tags.first, new_tag].map(&:name), image_url: 'http://new-image.png',
      format: :json
    assert_response :ok

    @past_stream.reload
    assert_equal 'edited name', @past_stream.name
    assert @past_stream.is_official
    assert @past_stream.card.is_official
    assert_same_elements [@some_tags.first, new_tag], @past_stream.tags
    assert_same_elements [@some_channels.first, new_channel], @past_stream.channels
    refute @past_stream.image == old_image

    patch :update, id: @past_stream.id, is_official: false, format: :json
    refute @past_stream.reload.is_official
    refute @past_stream.card.is_official
    assert @past_stream.image

    patch :update, id: @past_stream.id, name: 'new one name', is_official: true, format: :json
    assert @past_stream.reload.is_official
    assert @past_stream.card.is_official
    assert @past_stream.image
    assert_equal @past_stream.name, 'new one name'
  end

  test 'update edge cases' do
    index_setup
    old_image = @past_stream.image

    # empty channels, topics
    patch :update, id: @past_stream.id, name: 'edited name', channel_ids: [], topics: [], format: :json
    assert_response :ok

    @past_stream.reload
    assert_equal "edited name", @past_stream.name
    assert_equal [], @past_stream.tags
    assert_equal [], @past_stream.channels
    assert_equal @past_stream.image, old_image
  end

  test 'updates thumbnail image with filestack url' do
    index_setup
    stub_request(:get, /https:\/\/cdn.filestackcontent.com\/security=p:/).
      to_return(status: 200,
                body: File.new(Rails.root.join('test/fixtures/images/architecture.jpg')),
                headers: { content_type: 'image/jpg' })

    patch :update, id: @past_stream.id, image_url: "https://cdn.filestackcontent.com/12345678", format: :json
    assert_response :ok
    refute JSON.parse(response.body)["imageUrl"].nil?
  end
end

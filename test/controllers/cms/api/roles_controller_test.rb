require 'test_helper'

class Cms::Api::RolesControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers
  setup do
    @org = create(:organization)
    @org_admin = create(:user, organization: @org, organization_role: 'admin')
    @role = create(:role, name: 'contributor', organization: @org)

    set_host @org
    sign_in @org_admin
  end

  test ':api list organization roles' do
    Role.create_standard_roles @org

    get :index, format: :json
    resp = get_json_response(response)
    assert_response :ok
    assert_same_elements @org.roles.map(&:name), resp.collect {|r| r['name']}
  end

  test ':api create organization roles' do
    assert_difference -> {Role.count} do
      post :create, name: 'collabarator',  format: :json
      assert_response :ok
    end
  end

  test ':api update organization roles' do
    put :update, id: @role.id, name: 'Org collabarator', format: :json
    assert_response :ok
    assert_equal @role.reload.name, 'Org collabarator'
  end

  test ':api show role' do
    @show_channel_manager_permission = create(:role_permission, role: @role, name: Permissions::SHOW_CHANNEL_MANAGER)

    get :show, id: @role.id, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_response :ok
    assert_equal resp["name"], "contributor"
    assert_equal resp["permissions"].map { |p| p["name"] }, [Permissions::SHOW_CHANNEL_MANAGER]
  end

  test ':api assign permissions' do
    post :assign_permissions, id: @role.id, permissions: [Permissions::SHOW_CHANNEL_MANAGER, Permissions::HAS_CMS_ACCESS], format: :json
    resp = get_json_response(response)
    assert_response :ok

    @role.reload.role_permissions

    assert_equal @role.get_permissions, [Permissions::SHOW_CHANNEL_MANAGER, Permissions::HAS_CMS_ACCESS]
  end

  test ':api remove all permissions' do
    post :assign_permissions, id: @role.id, permissions: [], format: :json
    resp = get_json_response(response)
    assert_response :ok

    @role.reload.role_permissions

    assert_equal @role.get_permissions, []
  end

  test ':api delete role' do
    @show_channel_manager_permission = create(:role_permission, role: @role, name: Permissions::SHOW_CHANNEL_MANAGER)

    assert_differences [[->{Role.count}, -1], [->{RolePermission.count}, -1]] do
      delete :destroy, id: @role.id, format: :json
      assert_response :ok
    end
  end

  test ':api add users' do
    users = create_list(:user, 3, organization: @org)
    assert_difference ->{@role.users.count}, 2 do
      post :add_users, user_ids: users.first(2).map(&:id), id: @role.id, format: :json
      assert_response :ok
    end

    users.first(2).each{|user| assert UserRole.where(user: user, role: @role).present?}
    refute UserRole.where(user: users.last, role: @role).present?
  end

  test ':api remove users' do
    users = create_list(:user, 2, organization: @org)
    @role.add_user(users.first)
    assert_difference ->{@role.users.count}, -1 do
      post :remove_users, user_ids: [users.first.id], id: @role.id, format: :json
      assert_response :ok
    end

    assert_equal 0, @role.reload.users.count
  end
end

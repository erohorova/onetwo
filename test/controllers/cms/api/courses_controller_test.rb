require 'test_helper'

class Cms::Api::CoursesControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    @org = create(:organization)
    @org_admin = create(:user, organization_role: 'admin', organization: @org)
    set_host(@org)
    sign_in @org_admin
  end

  test 'get a list of courses' do
    groups = []
    (1..5).to_a.each do |i|
      groups << create(:resource_group, organization: @org, created_at: i.days.ago)
    end
    Search::CourseSearch.any_instance
      .expects(:search_for_cms)
      .with('', @org.id,
        {filter_params: {}, offset: 0, limit: 10, sort: {key: 'created_at', order: 'desc'}
        }
      )
      .returns(
        Search::CourseSearchResponse.new(
          results: (groups).map(&:search_data).map{|x| Hashie::Mash.new(x)},
          total: groups.count,
        )
      )

    get :index, format: :json
    assert_response :success

    first_object = get_json_response(response)["results"].first
    assert_not_nil first_object["id"]
    assert_not_nil first_object["channels"]
    assert_not_nil first_object["topics"]
    assert_not_nil first_object["name"]
    assert_not_nil first_object["description"]
    assert_not_nil first_object["is_promoted"]
    assert_not_nil first_object["is_hidden"]
    assert_not_nil first_object["website_url"]
    assert_not_nil first_object["image_url"]
    assert_not_nil first_object["users_count"]
    assert_not_nil first_object["created_at"]
    assert_not_nil first_object["source"]

    # sorted on created_date
    resp = get_json_response(response)["results"]
    assert_equal resp.first['id'], groups.first.id
    assert_equal resp.last['id'], groups.last.id
    assert resp.first["created_at"] > resp.last["created_at"]
  end

  test 'Fetch only resource_groups' do
    group1 = create(:resource_group, organization: @org)
    private_group = create(:private_group, is_private: true, organization: @org, resource_group: group1)

    Search::CourseSearch.any_instance
      .expects(:search_for_cms)
      .with('', @org.id,
        {filter_params: {}, offset: 0, limit: 10, sort: {key: 'created_at', order: 'desc'}
        }
      )
      .returns(
        Search::CourseSearchResponse.new(
          results: ([group1]).map(&:search_data).map{|x| Hashie::Mash.new(x)},
          total: [group1].count,
        )
      )

    get :index, format: :json
    assert_response :success
    first_object =  get_json_response(response)["results"].first
    assert_equal group1.id, first_object['id']
    assert_equal 1, get_json_response(response)["results"].length
  end

  test 'search for a matching query course' do
    group1 = create(:resource_group, organization: @org, name: 'this is me')
    group2 = create(:resource_group, organization: @org, name: 'smethingelse')

    Search::CourseSearch.any_instance
      .expects(:search_for_cms)
      .with('this is me', @org.id,
        {filter_params: {}, offset: 0, limit: 10, sort: {key: 'created_at', order: 'desc'}
        }
      )
      .returns(
        Search::CourseSearchResponse.new(
          results: ([group1]).map(&:search_data).map{|x| Hashie::Mash.new(x)},
          total: [group1].count,
        )
      )

    get :index, q: 'this is me', format: :json
    assert_response :success
    first_object =  get_json_response(response)["results"].first
    assert_equal group1.id, first_object['id']
    assert_equal 1, get_json_response(response)["results"].length
  end

  test 'search for promoted course only' do
    group1 = create(:resource_group, organization: @org, is_promoted: true)
    group2 = create(:resource_group, organization: @org, is_promoted: false)

    Search::CourseSearch.any_instance
      .expects(:search_for_cms)
      .with('', @org.id,
        {filter_params: {is_promoted: 'true'}, offset: 0, limit: 10, sort: {key: 'created_at', order: 'desc'}
        }
      )
      .returns(
        Search::CourseSearchResponse.new(
          results: ([group1]).map(&:search_data).map{|x| Hashie::Mash.new(x)},
          total: [group1].count,
        )
      )

    get :index, promoted: 'true', format: :json
    assert_response :success
    first_object =  get_json_response(response)["results"].first
    assert_equal group1.id, first_object['id']
    assert_equal 1, get_json_response(response)["results"].length
  end

  test 'promote courses' do
    group1 = create(:resource_group, organization: @org, is_promoted: false)
    group2 = create(:resource_group, organization: @org, is_promoted: true)
    group3 = create(:resource_group, organization: @org, is_promoted: false)

    post :promote, course_ids: [group1, group2, group3].map(&:id), format: :json

    assert_equal true, group1.reload.is_promoted
    assert_equal true, group2.reload.is_promoted
    assert_equal true, group3.reload.is_promoted
  end

  test 'unpromote courses' do
    group1 = create(:resource_group, organization: @org, is_promoted: true)
    group2 = create(:resource_group, organization: @org, is_promoted: true)
    group3 = create(:resource_group, organization: @org, is_promoted: false)

    post :unpromote, course_ids: [group1, group2, group3].map(&:id), format: :json

    assert_equal false, group1.reload.is_promoted
    assert_equal false, group2.reload.is_promoted
    assert_equal false, group3.reload.is_promoted
  end

  test 'hide courses' do
    group1 = create(:resource_group, organization: @org, is_hidden: false)
    group2 = create(:resource_group, organization: @org, is_hidden: true)
    group3 = create(:resource_group, organization: @org, is_hidden: false)

    post :hide, course_ids: [group1, group2, group3].map(&:id), format: :json

    assert_equal true, group1.reload.is_hidden
    assert_equal true, group2.reload.is_hidden
    assert_equal true, group3.reload.is_hidden
  end

  test 'unhide courses' do
    group1 = create(:resource_group, organization: @org, is_hidden: true)
    group2 = create(:resource_group, organization: @org, is_hidden: true)
    group3 = create(:resource_group, organization: @org, is_hidden: false)

    post :unhide, course_ids: [group1, group2, group3].map(&:id), format: :json

    assert_equal false, group1.reload.is_hidden
    assert_equal false, group2.reload.is_hidden
    assert_equal false, group3.reload.is_hidden
  end

  test 'update the course' do
    course = create(:resource_group, client_resource_id: 1, organization: @org)
    assert_equal false, course.details_locked
    assert_equal false, course.is_promoted
    patch :update, id: course.client_resource_id, is_promoted: true, format: :json
    assert_equal true, course.reload.is_promoted

    assert_equal false, course.is_hidden
    patch :update, id: course.client_resource_id, is_hidden: true, format: :json
    assert_equal true, course.reload.is_hidden

    patch :update, id: course.client_resource_id, topics: ['tag1', 'tag2'], format: :json
    tags = course.reload.tags.map(&:name)
    assert_equal ['tag1', 'tag2'], tags


    channel1 = create(:channel, organization: @org)
    channel2 = create(:channel, organization: @org)
    patch :update, id: course.client_resource_id, channel_ids: [channel1, channel2].map(&:id), format: :json
    assert_equal [channel1, channel2].map(&:id), course.reload.channels.map(&:id)

    assert_equal true, course.reload.details_locked
  end

  test 'get courses details' do
    group1 = create(:resource_group, client_resource_id: 1, organization: @org)

    get :show, id: group1.client_resource_id, format: :json
    assert_response :success

    group_object =  get_json_response(response)
    assert_equal group1.id, group_object['id']
  end
end

require 'test_helper'

class Cms::BaseControllerTest < ActionController::TestCase

  setup do
    create_default_org
  end

  teardown do
    Rails.application.reload_routes!
  end

  test 'ensure only org admin can access cms #default org' do
    class DummyController < Cms::BaseController

      def dummy
        head :ok
      end

      def root
        head :ok
      end

    end
    Rails.application.routes.draw do
      get 'dummy', controller: 'cms/base_controller_test/dummy', action: 'dummy'
      root 'cms/base_controller/dummy#root'
    end

    @controller = DummyController.new
    org = create(:organization)
    default_org = Organization.default_org

    admin_user = create(:user, organization: org, organization_role: 'admin')
    member_user = create(:user, organization: org, organization_role: 'member')
    random_user = create(:user, email: "random@random.com", organization: default_org)
    edcast_admin_user = create(:user, :admin, organization: default_org,)

    # edcast admin user has access to everything
    set_host(default_org)
    sign_in edcast_admin_user
    get :dummy
    assert_response :ok
    sign_out edcast_admin_user

    # But not to default org
    sign_in admin_user
    get :dummy, format: :json
    assert_response :not_found
    sign_out admin_user

    # random user, no access
    sign_in random_user
    get :dummy, format: :html
    assert_response :not_found
    assert_template 'errors/not_found'

    get :dummy, format: :json
    assert_response :not_found
  end


  test 'ensure only org admin can access cms #non default org' do
    class DummyController < Cms::BaseController

      def dummy
        head :ok
      end

      def root
        head :ok
      end

    end
    Rails.application.routes.draw do
      get 'dummy', controller: 'cms/base_controller_test/dummy', action: 'dummy'
      root 'cms/base_controller/dummy#root'
    end

    @controller = DummyController.new
    org = create(:organization)
    default_org = Organization.default_org

    admin_user = create(:user, organization: org, organization_role: 'admin')
    member_user = create(:user, organization: org, organization_role: 'member')
    random_user = create(:user, email: "random@random.com", organization: default_org)
    edcast_admin_user = create(:user, :admin, organization: default_org,)

    set_host(org)

    # admin user in org, has access to that org
    sign_in admin_user
    get :dummy
    assert_response :ok
    sign_out admin_user

    sign_in random_user
    get :dummy, format: :html
    assert_response :not_found
    assert_template 'errors/not_found'

    get :dummy, format: :json
    assert_response :not_found
    sign_out random_user

    # member user on org, does not have access
    sign_in member_user
    get :dummy, format: :html
    assert_response :not_found
    assert_template 'errors/not_found'

    get :dummy, format: :json
    assert_response :not_found
    sign_out member_user
  end
end

require 'test_helper'

class BrightcoveVideosControllerTest < ActionController::TestCase
  test "should redirect to modified brightcove url" do
    create_default_org
    org = create(:organization)
    user = create(:user, organization: org, organization_role: 'member')
    config = create(:config, category: 'brightcove', name: 'account_id', data_type: 'string', value: '12345', configable: org)

    stub_request(:get, "http://brightcove.me/xyz").
      with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.9.2'}).
      to_return(:status => 301, :body => "", :headers => {location: 'http://link.brightcove.com/services/player/bcpid4456565171001?bckey=9qauh3EpyluL7l&bctid=44567890&width=1080&height=1920'})

    set_host(org)
    sign_in user

    get :show, video_url: 'http://brightcove.me/xyz'

    assert_response 302
    assert_redirected_to "http://players.brightcove.net/12345/default_default/index.html?videoId=44567890&autoplay=yes"
  end

  test 'should redirect to default brightcove url if brightcove setting is missing' do
    create_default_org
    org = create(:organization)
    user = create(:user, organization: org, organization_role: 'member')

    set_host(org)
    sign_in user

    get :show, video_url: 'http://brightcove.me/xyz'

    assert_response 302
    assert_redirected_to "http://brightcove.me/xyz"
  end
end

require 'test_helper'

class DeviceCookieFilterControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  class DummiesController < ActionController::Base
    include DeviceCookieFilter
    def index
      head :ok
    end
  end

  self.controller_class= DummiesController


  def with_dummy_routing
    with_routing do |set|
      set.draw do
        get 'cookie_index', :controller=>"device_cookie_filter_controller_test/dummies", :action=>"index"
      end
      yield
    end
  end

  test 'should drop a cookie if one was not present' do
    device_id = SecureRandom.urlsafe_base64
    SecureRandom.expects(:urlsafe_base64).returns device_id
    with_dummy_routing do
      get :index
      assert_response :success
      assert_equal device_id, cookies['_d']
    end
  end

  test 'do not drop another cookie if it is already present' do
    device_id = SecureRandom.urlsafe_base64
    SecureRandom.expects(:urlsafe_base64).never
    with_dummy_routing do
      request.cookies['_d'] = device_id
      get :index
      assert_response :success
      assert_equal device_id, cookies['_d']
    end
  end

end

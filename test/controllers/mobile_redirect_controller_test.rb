require 'test_helper'

class MobileRedirectControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    create_default_org
  end

  test "should redirect to uri on mobile" do
    @request.user_agent = "Mozilla/5.0(iPhone;U;CPUiPhoneOS4_0likeMacOSX;en-us)AppleWebKit/532.9(KHTML,likeGecko)Version/4.0.5Mobile/8A293Safari/6531.22.7"
    url = 'http://www.site.com/?foo=bar&hello=world'
    get :redirect, url: url
    assert_response :success
    assert_equal assigns(:redirect_url), url
  end

  test "should redirect to regular website on desktop" do
    url = 'http://www.site.com/?edcast_ref=1'
    get :redirect, url: url
    assert_response :success
    assert_equal assigns(:redirect_url), url
  end
end

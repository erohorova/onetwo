require 'test_helper'

# -----------------------------------------------------------------------------
# COMMENTED OUT UNTIL WE FIX BROWSERIFY IN TEST ENVIRONMENT
# -----------------------------------------------------------------------------

=begin

class UiControllerTest < ActionController::TestCase


  include Devise::Test::ControllerHelpers
  self.use_transactional_fixtures = false
  self.use_shared_db_connection = false
  setup do
    IndexJob.stubs(:perform_later)
    # User.any_instance.stubs(:reindex_async).returns(true)
    cm_admin = create :user, email: 'admin@course-master.com'
    @client = create(:client)
    @org = create(:organization, client: @client, savannah_app_id: 123)
    @org.stubs(:create_general_channel).returns(true)
    @api_key = @client.valid_credential.api_key
    @shared_secret = @client.valid_credential.shared_secret
    @resource_id = SecureRandom.hex
    @timestamp = Time.now.to_i
  end

  teardown do
    # User.any_instance.unstub(:reindex_async)
    IndexJob.unstub(:perform_later)
  end

  def make_token(email: nil, user_id: nil, role: nil)
    parts = [@shared_secret, @timestamp, user_id, email, role, @resource_id].compact

    Digest::SHA256.hexdigest(parts.join('|'))
  end


  def make_auth_token(pars)
    parts = [pars[:savannah_app_id], @timestamp, pars[:user_id], pars[:email], pars[:role], pars[:resource_id], pars[:destiny_user_id]].compact
    Digest::SHA256.hexdigest(parts.join('|'))
  end

  test 'should route to the "qa" ui' do
    assert_routing '/ui/qa', {controller: 'ui', action: 'qa'}
  end

  test 'should render qa v1 launch page in json format' do
    stub_request(:post, /.*/)
    g, u = create_gu(user_opts: {organization: @org}, group_opts: {organization: @org, client: @org.client})
    get :qa_v1, widget_auth_params(group: g, user: u).merge({format: :json})
    assert_response :ok
    resp = get_json_response(response)
    assert_response :ok
    assert_equal g.id, resp['group']['id']
    assert_equal u.id, resp['user']['id']
    assert_equal g.social_enabled, resp['social_enabled']
    assert_equal g.connect_to_social_mediums, resp['group']['social_connect_enabled']
    assert_equal g.enable_mobile_app_download_buttons, resp['group']['app_download_enabled']
    assert_equal g.share_to_social_mediums, resp['group']['social_share_enabled']
    # now with edcast ref
    g1 = create(:private_group, resource_group: g, organization: @org)
    g1.members << u
    p = create(:post, user: u, group: g1)
    get :qa_v1, widget_auth_params(group: g, user: u).merge({parent_url: "http://localhost/ui/demo?edcast_ref=#{p.id}", format: :json})
    assert_response :ok
    resp = get_json_response(response)
    assert_equal g1.id, resp['launch_group']['id']
    assert_equal g.id, resp['group']['id']
    assert_equal u.id, resp['user']['id']
    assert_equal g.social_enabled, resp['social_enabled']
    assert_equal g.connect_to_social_mediums, resp['group']['social_connect_enabled']
    assert_equal g.enable_mobile_app_download_buttons, resp['group']['app_download_enabled']
    assert_equal g.share_to_social_mediums, resp['group']['social_share_enabled']
  end

  test 'should render ui page with just user id' do
    stub_request(:get, 'http://www.example.com/photos/sample.png').to_return(:status => 200)
    token = make_token(email: nil, user_id: '123', role: 'student')
    assert_difference [-> {User.count}, -> {ClientsUser.count}, ->{GroupsUser.member.count}, -> {Group.count}] do
      get :qa, api_key: @api_key, resource_id: @resource_id, resource_name: 'test name',
                resource_description: 'test description',
                parent_url: 'http://localhost/ui/demo?edcast_ref=1&group=first',
                resource_url: 'http://path/to/resource',
                user_id: '123',
                token: token,
                timestamp: @timestamp,
                user_role: 'student'
      assert_response :success
    end

    group = Group.last
    assert_equal 'ResourceGroup', group.class.name, 'needs to be ResourceGroup'
    assert_equal 'test name', group.name
    assert_equal 'test description', group.description
    assert_equal group.website_url, 'http://localhost/ui/demo?group=first'

    user = User.last
    assert_nil user.email
    assert_equal user, assigns[:user]
    assert_equal false, user.is_complete, 'user created through the api should be considered incomplete'
    assert_equal true, user.password_reset_required
    assert_equal group.organization, user.organization

    cu = ClientsUser.last.reload
    assert_equal cu.client_user_id, '123'
    assert cu.user_id

    assert group.is_member?(user)
  end

  test 'should create new user and assign user_id and email' do
    stub_request(:get, 'http://www.example.com/photos/sample.png').to_return(:status => 200)
    token = make_token(email: 'email@server.com', user_id: '123', role: 'student')
    assert_difference [-> {Group.count}, -> {ClientsUser.count}, ->{GroupsUser.member.count}, -> {User.count}] do
      assert_no_difference ->{ActionMailer::Base.deliveries.count} do
        get :qa, api_key: @api_key, resource_id: @resource_id, resource_name: 'test name',
                  resource_description: 'test description',
                  parent_url: 'http://localhost/ui/demo?edcast_ref=1&group=first',
                  resource_url: 'http://path/to/resource',
                  user_id: '123',
                  user_email: 'email@server.com',
                  token: token,
                  timestamp: @timestamp,
                  user_role: 'student'
        assert_response :success
      end
    end

    group = Group.last
    assert_equal 'test name', group.name
    assert_equal 'test description', group.description
    assert_equal group.website_url, 'http://localhost/ui/demo?group=first'

    user = User.last
    assert_equal 'email@server.com', user.email
    assert_equal user, assigns[:user]
    assert_equal false, user.is_complete, 'user created through the api should be considered incomplete'
    assert_equal true, user.password_reset_required

    cu = ClientsUser.last
    assert_equal cu.client_user_id, '123'

    assert group.is_member?(user)
  end

  test 'should link the client user id to an existing user with a matching email address' do
    IndexJob.stubs(:perform_later)
    stub_request(:get, 'http://www.example.com/photos/sample.png').to_return(:status => 200)
    user = create(:user)
    token = make_token(email: user.email, user_id: user.id, role: 'student')

    client_user = create(:clients_user, user:user)

    # org use case. same email as a user in another org.
    Organization.any_instance.stubs(:create_general_channel)
    org_user = create(:user, email: user.email, organization: @org)

    assert_difference [-> {Group.count}, ->{ClientsUser.count}, ->{GroupsUser.member.count}], 1, 'create a new group' do
      assert_no_difference -> {User.count} do
        get :qa, api_key: @api_key, resource_id: @resource_id, resource_name: 'test name',
                  resource_description: 'test description',
                  parent_url: 'http://localhost/ui/demo?edcast_ref=1&group=first',
                  resource_url: 'http://path/to/resource',
                  token: token,
                  timestamp: @timestamp,
                  user_id: user.id,
                  user_email: user.email,
                  user_role: 'student',
                  user_image: 'http://www.example.com/photos/sample.png'

      end
    end

    group = Group.last
    assert_equal 'test name', group.name
    assert_equal 'test description', group.description
    assert_equal group.website_url, 'http://localhost/ui/demo?group=first'

    cu = ClientsUser.last.reload
    assert_equal cu.client_user_id, user.id.to_s
    assert cu.user_id
    assert_equal @client, assigns[:client]
    assert_equal org_user, assigns[:user]

    # Making sure user in another org is untouched
    assert_equal @org, org_user.reload.organization
  end

  test 'create resource using savannah_ap_id for authentication' do
    IndexJob.stubs(:perform_later)
    stub_request(:get, 'http://www.example.com/photos/sample.png').to_return(:status => 200)
    user = create(:user)
    # token = make_auth_token(savannah_app_id: @org.savannah_app_id, email: user.email, user_id: user.id, role: 'student', destiny_user_id: )

    Organization.any_instance.stubs(:create_general_channel)
    org_user = create(:user, email: user.email, organization: @org)
    auth_token = make_auth_token(savannah_app_id: @org.savannah_app_id, email: user.email, user_id: user.id, role: 'student', destiny_user_id: org_user.id, resource_id: @resource_id)

    assert_difference [-> {Group.count}, ->{GroupsUser.member.count}], 1, 'create a new group' do
      assert_no_difference -> {User.count} do
        get :qa, savannah_app_id: @org.savannah_app_id, resource_id: @resource_id, resource_name: 'test name12',
                  resource_description: 'test description12',
                  parent_url: 'http://localhost/ui/demo?edcast_ref=1&group=first',
                  resource_url: 'http://path/to/resource',
                  auth_token: auth_token,
                  timestamp: @timestamp,
                  destiny_user_id: org_user.id,
                  user_id: user.id,
                  user_email: user.email,
                  user_role: 'student',
                  user_image: 'http://www.example.com/photos/sample.png'

      end
    end

    group = Group.last
    assert_equal 'test name12', group.name
    assert_equal 'test description12', group.description
    assert_equal group.website_url, 'http://localhost/ui/demo?group=first'

    assert_equal org_user, assigns[:user]

    # Making sure user in another org is untouched
    assert_equal @org, org_user.reload.organization
  end


  test 'should not render the ui page if token is bad' do
    assert_no_difference -> {Group.count} do
      get :qa, api_key: @api_key, resource_id: @resource_id, resource_name: 'test name',
          resource_description: 'test description',
          resource_url: 'http://path/to/resource',
          token: 'junk',
          timestamp: @timestamp
    end

    assert_response :forbidden
  end

  test 'should create a new user' do
    stub_request(:get, 'http://www.example.com/photos/sample.png').to_return(:status => 200)
    email = "#{SecureRandom.hex}@example.com"
    role = 'student'
    assert_difference -> {User.count}, 1, 'should create a new user' do
      assert_difference -> {GroupsUser.member.count}, 1, 'need to enroll the user to the group' do
        get :qa,
          api_key: @api_key,
          resource_id: @resource_id,
          user_id: 1,
          user_email: email,
          user_role: role,
          user_image: 'http://www.example.com/photos/sample.png',
          timestamp: @timestamp,
          token: make_token(user_id: 1,
                            email: email,
                            role: role)
      end
    end
    assert_equal assigns[:user], User.find_by_email(email)
  end

  test 'should create user with teacher role' do
    stub_request(:get, 'http://www.example.com/photos/sample.png').to_return(:status => 200)
    email = "#{SecureRandom.hex}@example.com"
    assert_difference -> {User.count}, 1, 'should create a new user' do
      assert_difference -> {GroupsUser.admin.count}, 1, 'need to enroll the user to the group' do
        get :qa, api_key: @api_key, resource_id: @resource_id,
          user_id: 1, user_role: 'teacher', user_email: email, user_image: 'http://www.example.com/photos/sample.png', timestamp: @timestamp, token: make_token(email: email, user_id: 1, role: 'teacher')
      end
    end
    assert_equal assigns[:user], User.find_by_email(email)
    assert Group.find_by_client_resource_id(@resource_id).admins.include?(assigns[:user])
  end

  test 'should create user with student role' do
    email = "#{SecureRandom.hex}@example.com"
    assert_difference -> {User.count}, 1, 'should create a new user' do
      assert_difference -> {GroupsUser.member.count}, 1, 'need to enroll the user to the group' do
        get :qa, api_key: @api_key, resource_id: @resource_id,
          user_id: 1, user_role: 'student', user_email: email,
          timestamp: @timestamp,
          token: make_token(user_id: 1, email: email, role: 'student')
      end
    end
    assert_equal assigns[:user], User.find_by_email(email)
    assert Group.find_by_client_resource_id(@resource_id).members.include?(assigns[:user])
  end

  test 'should delete user from teacher role on getting student role' do
    group = create(:group, organization: @org, client_resource_id: @resource_id)
    client_user = create(:clients_user, client: @client)
    user = client_user.user
    group.admins << user
    get :qa, api_key: @api_key, resource_id: @resource_id, user_role: 'student',
      user_id: client_user.client_user_id, user_email: user.email, timestamp: @timestamp,
      token: make_token(user_id: client_user.client_user_id, email: user.email, role: 'student')
    assert_response :success
    assert group.is_member?(user)
    assert_not_nil User.find(user.id)
    assert_not group.is_admin?(user)
  end

  test 'should delete user from student role on getting instructor role' do
    group = create(:group, organization: @org, client_resource_id: @resource_id)
    user = create(:user)
    create(:clients_user, client: @client, user: user, client_user_id: user.id)
    get :qa, api_key: @api_key, resource_id: @resource_id, user_role: 'instructor',
      user_id: user.id, user_email: user.email, timestamp: @timestamp,
      token: make_token(user_id: user.id, email: user.email, role: 'instructor')
    assert_response :success
    assert group.is_admin?(user)
    assert_not_nil User.find(user.id)
    assert_not group.is_member?(user)
  end

  test 'should create user with student role as default' do
    email = "#{SecureRandom.hex}@example.com"
    assert_difference -> {User.count}, 1, 'should create a new user' do
      assert_difference -> {GroupsUser.member.count}, 1, 'need to enroll the user to the group' do
        assert_difference -> {ClientsUser.count}, 1, 'need to create the close loop user' do
          get :qa, api_key: @api_key, resource_id: @resource_id,
            user_id: 1, user_email: email, timestamp: @timestamp,
            token: make_token(user_id: 1, email: email)

          end
      end
    end
    user = User.find_by_email(email)
    assert_equal user, assigns[:user]
    assert Group.find_by_client_resource_id(@resource_id).members.include?(user)
  end

  test 'should not create a new user' do
    client_user = create(:clients_user, client: @client)
    user = client_user.user
    assert_no_difference -> {User.count}, 'should reuse the user' do
      assert_no_difference -> {ClientsUser.count}, 'should reuse the client user' do
        assert_difference -> {GroupsUser.member.count}, 1, 'need to enroll the user to the group' do
          get :qa, api_key: @api_key, resource_id: @resource_id,
            user_id: client_user.client_user_id, user_email: user.email,
            timestamp: @timestamp, token: make_token(user_id: client_user.client_user_id,
                                                     email: user.email)
        end
      end
    end
    assert_equal user, assigns[:user]
  end

  test 'should render error screen if user id missing' do
    assert_no_difference [->{Group.count}, ->{User.count}, ->{GroupsUser.count}, ->{ClientsUser.count}] do
      get :qa, api_key: @api_key, resource_id: @resource_id,
        user_email: 'b@b.com',
        timestamp: @timestamp, token: make_token(email: 'b@b.com')
    end
    assert_template 'error'
  end

  test 'should render error screen if bad role parameter' do
    user = create(:user)
    get :qa, api_key: @api_key, resource_id: @resource_id,
      timestamp: @timestamp, user_email: user.email,
      user_role: 'bad role', token: make_token(email: user.email, role: 'bad role')
    assert_template :error
  end

  test 'should reset user image if bad url' do
    stub_request(:get, 'http://example.png').
    with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
    to_return(:status => 200, :body => File.read(Rails.root.join('test/fixtures/images/user2.jpg')), :headers => {content_type:'image/jpg'})

    user = create(:user, picture_url: 'http://example.png')
    old_photo = user.photo
    client_user = create(:clients_user, client: @client, user: user)
    get :qa, api_key: @api_key, resource_id: @resource_id,
      timestamp: @timestamp, user_id: client_user.client_user_id,
      user_email: user.email, user_role: 'student',
      user_image: 'bad url',
      token: make_token(email: user.email, role: 'student', user_id: client_user.client_user_id)
    assert_response :success

    assert_equal assigns[:user].photo, old_photo
    assert_equal user.reload.photo, old_photo
  end

  test 'should set user image' do
    AvatarImageJob.unstub(:perform_later)
    AnonImageJob.unstub(:perform_later)
    stub_request(:get, 'http://example.png').
    with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
    to_return(:status => 200, :body => File.read(Rails.root.join('test/fixtures/images/user2.jpg')), :headers => {content_type:'image/jpg'})

    client_user = create(:clients_user, client: @client)
    user = client_user.user
    get :qa, api_key: @api_key, resource_id: @resource_id,
      timestamp: @timestamp,
      user_id: client_user.client_user_id,
      user_email: user.email,
      user_role: 'student',
      user_image: 'http://example.png',
      token: make_token(email: user.email, role: 'student', user_id: client_user.client_user_id)
    assert_response :success
    assert_equal user.reload.photo, user.avatar.url(:small)
    assert_equal user.reload.picture_url, 'http://example.png'

    AvatarImageJob.stubs(:perform_later)
    AnonImageJob.stubs(:perform_later)
  end


  test 'should update user image' do
    AvatarImageJob.unstub(:perform_later)
    AnonImageJob.unstub(:perform_later)
    stub_request(:get, 'http://example.com/old-image.png').
    with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
    to_return(:status => 200, :body => File.read(Rails.root.join('test/fixtures/images/user2.jpg')), :headers => {content_type:'image/jpg'})

    stub_request(:get, 'http://example.com/new-image.png').
    with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
    to_return(:status => 200, :body => File.read(Rails.root.join('test/fixtures/images/user2.jpg')), :headers => {content_type:'image/jpg'})

    user = create(:user, picture_url: 'http://example.com/old-image.png', skip_generate_images: false)
    old_avatar_url = user.avatar.url(:tiny)
    client_user = create(:clients_user, client: @client, user: user)
    get :qa, api_key: @api_key, resource_id: @resource_id,
      timestamp: @timestamp, user_id: client_user.client_user_id,
      user_email: user.email, user_role: 'student',
      user_image: 'http://example.com/new-image.png',
      token: make_token(email: user.email, role: 'student', user_id: client_user.client_user_id)
    assert_response :success
    assert_equal user.reload.picture_url, 'http://example.com/new-image.png'
    assert_not_equal old_avatar_url, user.avatar.url(:tiny)
    AvatarImageJob.stubs(:perform_later)
    AnonImageJob.stubs(:perform_later)
    end

  test 'should set user name when given full name' do
    stub_request(:get, 'http://example.png').
    with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
    to_return(:status => 200, :body => File.read(Rails.root.join('test/fixtures/images/user2.jpg')), :headers => {content_type:'image/jpg'})

    user = create(:user, first_name: nil, last_name: nil)
    client_user = create(:clients_user, client: @client, user: user)
    get :qa, api_key: @api_key, resource_id: @resource_id,
      timestamp: @timestamp,
      user_id: client_user.client_user_id,
      user_email: user.email,
      user_role: 'student',
      user_image: 'http://example.png', user_name: 'Bharath Bhat',
      token: make_token(email: user.email, role: 'student', user_id: client_user.client_user_id)
    assert_response :success

    assert_equal user.reload.name, 'Bharath Bhat'
  end

  test 'should set user name when given first and last names' do
    stub_request(:get, 'http://example.png').
    with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
    to_return(:status => 200, :body => File.read(Rails.root.join('test/fixtures/images/user2.jpg')), :headers => {content_type:'image/jpg'})

    user = create(:user, first_name: nil, last_name: nil)
    client_user = create(:clients_user, client: @client, user: user)
    get :qa, api_key: @api_key, resource_id: @resource_id,
      timestamp: @timestamp,
      user_id: client_user.client_user_id,
      user_email: user.email,
      user_role: 'student',
      user_image: 'http://example.png', user_name: 'first last',
      user_first_name: 'first', user_last_name: 'last',
      token: make_token(email: user.email, role: 'student', user_id: client_user.client_user_id)
    assert_response :success

    assert_equal user.reload.first_name, 'first'
    assert_equal user.last_name, 'last'

  end


  test 'should update the client user email address by adding a row in the email table if email was never set' do
    stub_request(:get, 'http://example.png').
    with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
    to_return(:status => 200, :body => File.read(Rails.root.join('test/fixtures/images/user2.jpg')), :headers => {content_type:'image/jpg'})

    client_user = create(:clients_user, client: @client)
    user = client_user.user
    old_email = user.email
    assert_nil old_email
    assert_difference -> {Email.count} do
      get :qa, api_key: @api_key, resource_id: @resource_id,
        timestamp: @timestamp,
        user_id: client_user.client_user_id,
        user_email: 'new@example.com',
        user_role: 'student',
        user_image: 'http://example.png', user_name: 'first last',
        user_first_name: 'first', user_last_name: 'last',
        token: make_token(email: 'new@example.com', role: 'student', user_id: client_user.client_user_id)
    end
    assert_response :success

    assert_equal 'new@example.com', Email.last.email

    assert_equal old_email, user.email
  end

  test 'should update the client user email address by adding a row in the email table if email was already set' do
    stub_request(:get, 'http://example.png').
    with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
    to_return(:status => 200, :body => File.read(Rails.root.join('test/fixtures/images/user2.jpg')), :headers => {content_type:'image/jpg'})

    user = create(:user)
    client_user = create(:clients_user, client: @client, user: user)
    old_email = user.email
    assert old_email
    assert_difference -> {Email.count} do
      get :qa, api_key: @api_key, resource_id: @resource_id,
        timestamp: @timestamp,
        user_id: client_user.client_user_id,
        user_email: 'new@example.com',
        user_role: 'student',
        user_image: 'http://example.png', user_name: 'first last',
        user_first_name: 'first', user_last_name: 'last',
        token: make_token(email: 'new@example.com', role: 'student', user_id: client_user.client_user_id)
    end
    assert_response :success

    assert_equal 'new@example.com', Email.last.email

    assert_equal old_email, user.email
  end

  test 'should set group host url' do
    stub_request(:get, 'http://example.png').
    with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
    to_return(:status => 200, :body => File.read(Rails.root.join('test/fixtures/images/user2.jpg')), :headers => {content_type:'image/jpg'})

    user = create(:user, first_name: nil, last_name: nil)
    client_user = create(:clients_user, client: @client, user: user)
    get :qa, api_key: @api_key, resource_id: @resource_id,
      timestamp: @timestamp,
      user_id: client_user.client_user_id,
      user_email: user.email,
      user_role: 'student',
      user_image: 'http://example.png', user_name: 'Bharath Bhat',
      parent_url: 'http://edx.org',
      token: make_token(email: user.email, role: 'student', user_id: client_user.client_user_id)
    assert_response :success

    group = Group.find_by_client_resource_id(@resource_id)
    assert_equal group.website_url, 'http://edx.org?'
  end

  test 'should set existing group url if currently nil' do
    stub_request(:get, 'http://example.png').
    with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
    to_return(:status => 200, :body => File.read(Rails.root.join('test/fixtures/images/user2.jpg')), :headers => {content_type:'image/jpg'})

    user = create(:user)
    client_user = create(:clients_user, client: @client, user: user)
    group = create(:group, organization: @org, website_url: nil,
                   client_resource_id: @resource_id)

    get :qa, api_key: @api_key, resource_id: @resource_id,
      timestamp: @timestamp,
      user_id: client_user.client_user_id,
      user_email: user.email,
      user_role: 'student',
      user_image: 'http://example.png', user_name: 'Bharath Bhat',
      parent_url: 'http://cfi.org',
      token: make_token(email: user.email, role: 'student', user_id: client_user.client_user_id)
    assert_response :success

    assert_equal group.reload.website_url, 'http://cfi.org?'
  end

  test 'should update existing group if url, group name, or description are different' do
    stub_request(:get, 'http://example.png').
    with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
    to_return(:status => 200, :body => File.read(Rails.root.join('test/fixtures/images/user2.jpg')), :headers => {content_type:'image/jpg'})

    user = create(:user, organization: @org)
    client_user = create(:clients_user, client: @client, user: user)
    group = create(:group, website_url: 'http://coursemaster.me/nlp',
                   client_resource_id: @resource_id, organization: @org)
    get :qa, api_key: @api_key, resource_id: @resource_id,
      timestamp: @timestamp,
      resource_name: group.name,
      resource_description: group.description,
      user_id: client_user.client_user_id,
      user_email: user.email,
      user_role: 'student',
      user_image: 'http://example.png', user_name: 'Bharath Bhat',
      parent_url: 'http://edx.org',
      token: make_token(email: user.email, role: 'student', user_id: client_user.client_user_id)
    assert_response :success

    assert_equal 'http://edx.org?', group.reload.website_url

    get :qa, api_key: @api_key, resource_id: @resource_id,
        resource_name: 'something totally new',
        resource_description: group.description,
        timestamp: @timestamp,
        user_id: client_user.client_user_id,
        user_email: user.email,
        user_role: 'student',
        user_image: 'http://example.png', user_name: 'Bharath Bhat',
        parent_url: 'http://edx.org',
        token: make_token(email: user.email, role: 'student', user_id: client_user.client_user_id)
    assert_response :success

    assert_equal 'something totally new', group.reload.name

    get :qa, api_key: @api_key, resource_id: @resource_id,
        resource_name: group.name,
        resource_description: 'something totally newer',
        timestamp: @timestamp,
        user_id: client_user.client_user_id,
        user_email: user.email,
        user_role: 'student',
        user_image: 'http://example.png', user_name: 'Bharath Bhat',
        parent_url: 'http://edx.org',
        token: make_token(email: user.email, role: 'student', user_id: client_user.client_user_id)
    assert_response :success

    assert_equal 'something totally newer', group.reload.description
  end

  test 'should not update existing group url, name and description if details locked' do
    stub_request(:get, 'http://example.png').
    with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
    to_return(:status => 200, :body => File.read(Rails.root.join('test/fixtures/images/user2.jpg')), :headers => {content_type:'image/jpg'})

    user = create(:user)
    client_user = create(:clients_user, client: @client, user: user)
    group = create(:group, website_url: 'http://coursemaster.me/nlp',
                   client_resource_id: @resource_id, client: @client,
                   name: 'old name', description: 'old description',
                   details_locked: true)

    get :qa, api_key: @api_key, resource_id: @resource_id,
      timestamp: @timestamp,
      resource_name: group.name,
      resource_description: group.description,
      user_id: client_user.client_user_id,
      user_email: user.email,
      user_role: 'student',
      user_image: 'http://example.png', user_name: 'Bharath Bhat',
      parent_url: 'http://edx.org',
      token: make_token(email: user.email, role: 'student', user_id: client_user.client_user_id)
    assert_response :success

    assert_equal 'http://coursemaster.me/nlp', group.reload.website_url

    get :qa, api_key: @api_key, resource_id: @resource_id,
        resource_name: 'something totally new',
        resource_description: group.description,
        timestamp: @timestamp,
        user_id: client_user.client_user_id,
        user_email: user.email,
        user_role: 'student',
        user_image: 'http://example.png', user_name: 'Bharath Bhat',
        parent_url: 'http://edx.org',
        token: make_token(email: user.email, role: 'student', user_id: client_user.client_user_id)
    assert_response :success

    assert_equal 'old name', group.reload.name

    get :qa, api_key: @api_key, resource_id: @resource_id,
        resource_name: group.name,
        resource_description: 'something totally newer',
        timestamp: @timestamp,
        user_id: client_user.client_user_id,
        user_email: user.email,
        user_role: 'student',
        user_image: 'http://example.png', user_name: 'Bharath Bhat',
        parent_url: 'http://edx.org',
        token: make_token(email: user.email, role: 'student', user_id: client_user.client_user_id)
    assert_response :success

    assert_equal 'old description', group.reload.description
  end

  test 'should not use an invalid url for group' do
    stub_request(:get, 'http://example.png').
    with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
    to_return(:status => 200, :body => File.read(Rails.root.join('test/fixtures/images/user2.jpg')), :headers => {content_type:'image/jpg'})

    user = create(:user, first_name: nil, last_name: nil)
    client_user = create(:clients_user, client: @client, user: user)
    get :qa, api_key: @api_key, resource_id: @resource_id,
      timestamp: @timestamp,
      user_id: client_user.client_user_id,
      user_email: user.email,
      user_role: 'student',
      user_image: 'http://example.png', user_name: 'Bharath Bhat',
      parent_url: 'bad url',
      token: make_token(email: user.email, role: 'student', user_id: client_user.client_user_id)
    assert_response :success

    group = Group.find_by_client_resource_id(@resource_id)
    assert_nil group.website_url
  end

  test 'should accept context_id, context_label, and display_context parameters' do
    user = create(:user)
    client_user = create(:clients_user, client: @client, user: user)
    get :qa, api_key: @api_key, resource_id: @resource_id,
      timestamp: @timestamp,
      user_id: client_user.client_user_id,
      user_email: user.email,
      user_role: 'student',
      parent_url: 'http://edx.org',
      context_id: 'some-context-id',
      context_label: 'some-context-label',
      display_context: 'true',
      token: make_token(email: user.email, role: 'student', user_id: client_user.client_user_id)
    assert_response :success

    assert_match 'some-context-id', response.body
    assert_match 'some-context-label', response.body
    assert_match 'displayContext = true', response.body
  end

  test 'should assign role label' do
    user = create(:user)
    client_user = create(:clients_user, client: @client, user: user)
    get :qa, api_key: @api_key, resource_id: @resource_id,
      timestamp: @timestamp,
      user_id: client_user.client_user_id,
      user_email: user.email,
      user_role: 'assistant',
      parent_url: 'http://edx.org',
      token: make_token(email: user.email, role: 'assistant', user_id: client_user.client_user_id)
    assert_response :success

    gu = GroupsUser.where(user: user).last
    assert_equal 'assistant', gu.role_label
  end

  test 'should return false if the app.forum.left_panel.disable app config is not set' do
    stub_request(:post, /.*/)
    g, u = create_gu(user_opts: {organization: @org}, group_opts: {organization: @org, client: @client})
    get :qa_v1, widget_auth_params(group: g, user: u).merge({format: :json})
    resp = get_json_response(response)
    assert_equal g.client.are_filters_disabled?, false
    assert_equal g.client.are_filters_disabled?, resp['group']['forum_filters_disabled']
  end

  test 'should return true if the app.forum.left_panel.disable config is set to true' do
    stub_request(:post, /.*/)
    g, u = create_gu(user_opts: {organization: @org}, group_opts: {organization: @org, client: @client})
    config   = create :config, configable: g.client, configable_type: 'Client', name: 'app.forum.left_panel.disable', value: 'true'
    get :qa_v1, widget_auth_params(group: g, user: u).merge({format: :json})
    resp = get_json_response(response)
    assert_equal g.client.are_filters_disabled?, true
    assert_equal g.client.are_filters_disabled?, resp['group']['forum_filters_disabled']
  end

  test 'should return false if the app.forum.left_panel.disable config is set to false' do
    stub_request(:post, /.*/)
    g, u = create_gu(user_opts: {organization: @org}, group_opts: {organization: @org, client: @client})
    config   = create :config, configable: g.client, configable_type: 'Client', name: 'app.forum.left_panel.disable', value: 'false'
    get :qa_v1, widget_auth_params(group: g, user: u).merge({format: :json})
    resp = get_json_response(response)
    assert_equal g.client.are_filters_disabled?, false
    assert_equal g.client.are_filters_disabled?, resp['group']['forum_filters_disabled']
  end
end

=end
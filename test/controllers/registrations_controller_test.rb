require 'test_helper'

class RegistrationsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers
  self.use_transactional_fixtures = false
  setup do
    stub_ip_lookup(ip_address: '0.0.0.0')
  end

  test 'should respond to json' do
    org = create(:organization)
    set_host org
    User.stubs(:pubnub_channels_to_subscribe_to_for_id).returns([])

    @request.env["devise.mapping"] = Devise.mappings[:user]
    post :create, user: { email: generate(:email_address) }, format: :json
    assert_response :ok
    resp = get_json_response(response)
    assert_equal resp["id"], User.last.id
  end

  test 'should respond :unprocessable_entity for existing active user' do
    org = create(:organization)
    set_host org
    @request.env["devise.mapping"] = Devise.mappings[:user]
    u = create(:user, email: "test@test.com", organization: org)
    post :create, user: { email: u.email }, format: :json
    assert_response :unprocessable_entity
    resp = get_json_response(response)
    assert_match /#{I18n.t("activerecord.errors.models.user.attributes.email.taken")}/, resp['message']
  end

  test 'save new user as incomplete user until he confirms his account' do
    skip("INTERMITTENT FAILURE ON TRAVIS")
    org = create(:organization)
    set_host org
    request.headers['User-Agent'] = 'EdCast/1.0 (iPhone; iOS 8.1.1; Scale/2.00)'
    @request.env["devise.mapping"] = Devise.mappings[:user]

    MandrillMailer.any_instance.expects(:send_api_email).once
    assert_difference -> {User.count} do
      post :create, user: {email: generate(:email_address)}
    end
    user = User.last
    assert_equal false, user.is_complete
    assert_equal true, user.password_reset_required
  end

  test 'default to web confirmation template' do
    skip("INTERMITTENT FAILURE ON TRAVIS")
    org = create(:organization)
    set_host org
    file_name = ''
    MandrillMessageGenerationService.any_instance.expects(:merge_custom_attrs).with(){|args|
      file_name = args[:html_file]
    }

    @request.env["devise.mapping"] = Devise.mappings[:user]
    post :create, user: {email: generate(:email_address)}
    assert "edcast-confirmation-instruction-web.html", file_name
  end

  test 'should not treat forum user as new signup as a complete user now' do
    org = create(:organization)
    request.headers['User-Agent'] = 'EdCast/1.0 (iPhone; iOS 8.1.1; Scale/2.00)'
    @request.env["devise.mapping"] = Devise.mappings[:user]
    file_name = ''
    MandrillMessageGenerationService.any_instance.expects(:merge_custom_attrs).with(){|args|
      file_name = args[:html_file]
    }.never
    email = generate(:email_address)
    user = create(:user, email: email, is_complete: false, organization: org)
    assert_equal false, user.is_complete
    set_host org
    assert_no_difference ->{User.count} do
      post :create, user: {email: email}
    end
    user = User.last
    assert_equal false, user.is_complete, 'should not set this until user has confirmed their account'
    assert_equal false, user.password_reset_required, 'do not change this flag, else people can abuse it and randomly reset others password'
  end


  test 'treat forum user as new signup' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    email = generate(:email_address)
    user = create(:user, email: email, password_reset_required: true, is_complete: false, handle: '@notanewuser')
    assert_no_difference ->{User.count} do
      post :create, user: {email: email}
    end
    assert_equal false, user.is_complete
    assert_equal true, user.password_reset_required
    assert_equal '@notanewuser',user.handle
  end

  test 'save org on registration' do
    create_default_org
    @request.env["devise.mapping"] = Devise.mappings[:user]
    email = generate(:email_address)
    assert_difference ->{User.count} do
      post :create, user: {email: email}
    end
    user = User.last
    assert_equal Organization.default_org, user.organization
    assert_equal 'member', user.organization_role
    sign_out user

    org = create(:organization, is_open: true)
    set_host(org)
    assert_difference ->{User.count} do
      post :create, user: {email: email}
    end
    user = User.last
    assert_equal email, user.email
    assert_equal org, user.organization
  end

  test 'should prevent sign up on a closed org' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    email = generate(:email_address)
    org = create(:organization, is_open: false)
    create(:config, name: 'AllowedEmailDomains', value: 'none', configable: org)
    set_host(org)
    assert_no_difference ->{User.count} do
      post :create, user: {email: email}
    end
    assert_response :forbidden
  end

  test 'should prevent sign up for non-whitelisted email domains' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    org = create(:organization, is_open: false)
    create(:config, name: 'AllowedEmailDomains', value: 'example.com', configable: org)
    set_host(org)
    assert_no_difference ->{User.count} do
      post :create, user: {email: "test@abc.com"}
    end
    assert_response :forbidden
  end

  test 'should allow sign up for whitelisted email domains' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    org = create(:organization, is_open: false)
    create(:config, name: 'AllowedEmailDomains', value: 'example.com', configable: org)
    set_host(org)
    assert_difference ->{User.count} do
      post :create, user: {email: "test@example.com"}
    end
  end

  test 'should allow sign up for any email domains' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    email = generate(:email_address)
    org = create(:organization, is_open: false)
    create(:config, name: 'AllowedEmailDomains', value: 'all', configable: org)
    set_host(org)
    assert_difference ->{User.count} do
      post :create, user: {email: email}
    end
  end

  test 'should prevent sign up of suspended user' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    email = generate(:email_address)
    org = create(:organization, is_open: true)
    user = create(:user, email: email, organization: org, is_suspended: true )
    set_host(org)
    assert_no_difference ->{User.count} do
      post :create, user: { email: email, organization: org, is_suspended: true }
    end
    assert_redirected_to '/404'
    assert assigns(:user_suspended)
  end

  test 'should set nil if user is not available' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    org = create(:organization, is_open: true)
    set_host(org)
    post :create, user: { email: "123@test.com", organization: org, is_suspended: true }
    refute assigns(:user_suspended)
  end

  test 'should sign up user which is not suspended' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    org = create(:organization, is_open: true)
    email = generate(:email_address)
    set_host(org)
    assert_difference ->{User.count} do
      post :create, user: { email: email, organization: org }
    end
    assert_redirected_to '/'
  end

  test "should allow sign up for closed org for savannah request" do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    org = create(:organization, is_open: false, savannah_app_id: 13)
    create(:config, name: 'AllowedEmailDomains', value: 'none', configable: org)
    user = build(:user, organization: org)

    parts = ["13", user.email, 'user'].compact
    auth_token = Digest::SHA256.hexdigest(parts.join('|'))
    user_params = { email: user.email }

    set_host(org)
    assert_difference ->{User.count} do
      post :create, user: user_params, savannah_app_id: org.savannah_app_id, email: user.email, auth_token: auth_token, format: :json
    end
  end

  test 'should create user when sigining up from new edc-web signup page' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    email = generate(:email_address)
    org = create(:organization)
    set_host(org)
    assert_difference ->{User.count} do
      post :create, user: {email: email}, from_edc_web: 'true'
    end
    assert_redirected_to '/'
  end

  test 'for okta enabled org, user signing up with email and password should be able to reset their password' do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @organization = create :organization
    set_host @organization
    stub_okta_user
    Organization.any_instance.stubs(:okta_enabled?).returns(true)
    email = generate(:email_address)
    random_password = User.random_password
    post :create, user: {email: email, password: random_password}, format: :json
    assert User.last.password_reset_required
    assert_empty IdentityProvider.all
  end
end

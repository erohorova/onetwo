FactoryGirl.define do
  factory :personalized_email do
    user
    subject "4 new posts & this weeks top post: Something"
    activity_indicator_subject "4 new posts & this weeks top post: Something"
  end
end

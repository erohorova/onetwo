FactoryGirl.define do
  factory :content_level_metric do
    content_id 1
    content_type "card"
    period 'day'
    offset 0
  end
end

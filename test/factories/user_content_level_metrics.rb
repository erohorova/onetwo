FactoryGirl.define do
  factory :user_content_level_metric do
    user
    content_id 1
    content_type "card"
    period 'day'
    offset 0
  end
end

FactoryGirl.define do
  factory :xapi_credential do
    end_point "http://xapi.example.com/lrs/statements"
    lrs_login_key "example"
    lrs_password_key "password"
    lrs_api_version "1.0.0"
    organization
  end
end

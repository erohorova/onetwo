FactoryGirl.define do
  factory :ecl_source do
    channel
    ecl_source_id 'uuid-123-abc'
    rate_unit 'hours'
    rate_interval 1
    number_of_items 10
  end
end

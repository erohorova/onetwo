FactoryGirl.define do
  factory :mobile_invitation do
    recipient_email 'a@a.com'
    first_name 'a'
    last_name 'a'
  end

  factory :branchmetrics_invitation do
    recipient_email 'a@a.com'
    first_name 'a'
    last_name 'a'
  end

end


# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :team_assignment_metric do
    team {|tam| tam.association(:team) }
    card {|tam| tam.association(:card) }
    assignor {|tam| tam.association(:user) }
    assigned_count 0
    started_count 0
    completed_count 0
  end
end

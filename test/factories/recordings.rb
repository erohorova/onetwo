FactoryGirl.define do
  factory :recording do
    sequence_number 0
    location 'https://somewhere/bucket/123/x.mp4'
    bucket 'bucket'
    key '/bucket/123/x.mp4'
    checksum 'abcxyz'
    hls_location 'something.m3u8'
    mp4_location 'something.mp4'
    source 'client'
    transcoding_status 'available'
    association :video_stream, factory: :wowza_video_stream
    default true
  end
end

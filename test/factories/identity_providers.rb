FactoryGirl.define do
  factory :identity_provider do
    user
    sequence(:uid) {|n| n}
    sequence(:token) {|n| n}
    trait :facebook do
      type 'FacebookProvider'
      expires_at 60.days.from_now
    end
    trait :google do
      type 'GoogleProvider'
      expires_at 60.days.from_now
    end
    trait :linkedin do
      type 'LinkedinProvider'
      expires_at 60.days.from_now
    end
    trait :twitter do
      type 'TwitterProvider'
    end
    trait :saml do
      type 'SamlProvider'
    end

    trait :lxp_oauth do
      type 'LxpOauthProvider'
    end
  end

  factory :facebook_provider
  factory :linkedin_provider
end

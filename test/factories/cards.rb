FactoryGirl.define do
  

  factory :card_bare, class: :card do
    author_id { create(:user).id }
    is_public true
    state 'published'
    published_at { Time.now.utc }

    factory :card do
      message 'This is a cool card'
      card_type 'media'
      card_subtype 'link'
      is_official false

      after(:create) do |card|
        card.send(:create_card_pack_relation) if card.pack_card?
        card.send(:create_journey_pack_relation) if card.journey_card?
      end

      after(:build) do |card|
        if card.media_card?
          card.card_subtype ||= 'text'
        elsif card.pack_card? || card.video_stream?
          card.card_subtype = (Card::SUB_TYPES[card.card_type].include?(card.card_subtype) ? card.card_subtype : 'simple')
        elsif card.poll_card?
          card.card_subtype ||= 'text'
        end
      end
    end

    factory :public_media_card do
      message 'future learning'
      card_type 'media'
      card_subtype 'link'
      is_public true
    end
  end

  factory :cards_user do
    card
    user
  end

  factory :card_metadatum do
    card
  end

  factory :video_card, class: Card do
    association :author, factory: :user
    message         "video card"
    card_type       "media"
    card_subtype    "video"
    state           "published"
    taxonomy_topics { Taxonomies.domain_topics.collect{|k| k["topic_name"] } }

    before(:create) do |video_card|
      resource = create(:resource, type: 'Video')
      video_card.resource = resource
    end
  end

 factory :video_stream_card, class: Card do
    association :author, factory: :user
    message         "videostream card"
    card_type       "video_stream"
    card_subtype    "simple"
    state           "published"
    taxonomy_topics { Taxonomies.domain_topics.collect{|k| k["topic_name"] } }
    association :video_stream, factory: :wowza_video_stream
  end

end

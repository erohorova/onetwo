FactoryGirl.define do
  factory :users_mentor do
    user
    sequence :email do
      "random+#{n}@gmail.com"
    end
    association :mentor, factory: :user
  end
end

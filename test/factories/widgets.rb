FactoryGirl.define do
  factory :widget do
    association :creator, factory: :user
    association :organization
    association :parent, factory: :channel

    context :channel
  end
end

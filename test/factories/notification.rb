FactoryGirl.define do
  factory :notification do
  	association :user
  	notification_type "new_comment"
  	unseen true
  end
end

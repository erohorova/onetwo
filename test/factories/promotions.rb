FactoryGirl.define do
  factory :promotion do
    url "http://promo.com/target"
    association :promotable, factory: :group
    data ({taught_by: 'Mr. Prof', organization: 'Ink Inc'})
  end
end

FactoryGirl.define do
  factory :wowza_video_stream do
    association :creator, factory: :user
    sequence :name do |n|
      "awesome wowza stream #{n}"
    end
    status 'upcoming'
    start_time '2015-03-15T10:00:00Z'

    trait :pending do
      status 'pending'
    end

    trait :live do
      status 'live'
      start_time Time.now.utc
    end

    trait :past do
      status 'past'
      start_time (Time.now - 1.month).utc
    end

    trait :upcoming do
      status 'upcoming'
      start_time (Time.now + 1.month).utc
    end
  end

  factory :red5_pro_video_stream do
    association :creator, factory: :user
    sequence :name do |n|
      "awesome red5 pro stream #{n}"
    end
    status 'upcoming'
    start_time '2015-03-15T10:00:00Z'

    trait :live do
      status 'live'
      start_time Time.now.utc
    end

    trait :past do
      status 'past'
      start_time (Time.now - 1.month).utc
    end

    trait :upcoming do
      status 'upcoming'
      start_time (Time.now + 1.month).utc
    end
  end


  factory :iris_video_stream do
    association :creator, factory: :user
    sequence :name do |n|
      "awesome irisstream #{n}"
    end
    status 'upcoming'
    start_time '2020-03-15T10:00:00Z'

    trait :pending do
      status 'pending'
    end

    trait :live do
      status 'live'
      start_time Time.now.utc
    end

    trait :past do
      status 'past'
      start_time (Time.now - 1.month).utc
    end

    trait :upcoming do
      status 'upcoming'
      start_time (Time.now + 1.month).utc
    end
  end
end

FactoryGirl.define do
  factory :user_daily_learning do
    user
    date Time.now.utc.to_date.to_s

    trait :initialized do
      processing_state DailyLearningProcessingStates::INITIALIZED
    end
    
    trait :started do
      processing_state DailyLearningProcessingStates::STARTED
    end

    trait :error do
      processing_state DailyLearningProcessingStates::ERROR
    end

    trait :completed do
      processing_state DailyLearningProcessingStates::COMPLETED
    end

  end
end

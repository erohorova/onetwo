FactoryGirl.define do
  factory :assignment do
    association :assignee, factory: :user
    assignable { |a| a.association(
        :card,
        author_id: nil,
        organization: assignee.organization
      )
    }
    state Assignment::ASSIGNED
  end
end

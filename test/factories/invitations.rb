# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :invitation do
    recipient_email {generate(:email_address)}
  end
end

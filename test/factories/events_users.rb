# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :events_user do

    event
    user

    trait :rsvp_yes do
      rsvp 'yes'
    end

    trait :rsvp_no do
      rsvp 'no'
    end

    trait :rsvp_maybe do
      rsvp 'maybe'
    end
  end
end

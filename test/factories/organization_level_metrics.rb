FactoryGirl.define do
  factory :organization_level_metric do
    organization
    user
    content_type 'card'
    action 'impression'
    period 'day'
    offset 0
  end
end

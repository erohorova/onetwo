# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :embargo do
    sequence :value do |n|
      "value#{n}"
    end

    trait :unapproved_words do
      value 'bad_word'
      category 'unapproved_words'
    end
  end
end

FactoryGirl.define do
  factory :activity_stream do
    action 'smartbite_completed'
    streamable_type 'UserContentCompletion'
  end
end
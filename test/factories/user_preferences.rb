FactoryGirl.define do
  factory :user_preference do
    user
    preferenceable factory: :group
    value 'true'
    key 'some.setting'
  end
end
FactoryGirl.define do
  factory :email do
    confirmed false
    confirmation_token SecureRandom.hex(32)
    association :user, factory: :user, email: nil
    email {generate(:email_address)}
  end
end

FactoryGirl.define do
  factory :developer_api_credential do
    organization
    sequence(:api_key) {SecureRandom.hex}
    sequence(:shared_secret) {SecureRandom.hex}
    creator { create(:user, organization: organization) }
  end
end
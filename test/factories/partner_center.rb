FactoryGirl.define do
  factory :partner_center do
    organization
    user
    embed_link  'https://dev-579575.oktapreview.com/app/edcastdev579575_savannahsaml_1/exkd0ql7q4VnKqkiW0h7/sso/saml'
    enabled     { true }
    integration 'edcast_cloud'
  end
end

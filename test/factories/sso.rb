FactoryGirl.define do
  factory :sso do
    name 'email'

    trait :lxp_oauth do
      name 'lxp_oauth'
    end

    trait :saml do
      name 'saml'
    end
  end
end

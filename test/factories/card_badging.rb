FactoryGirl.define do
  factory :card_badging do
    association :card, factory: :card
    association :card_badge, factory: :card_badge

    title 'this is dummy title'
  end
end

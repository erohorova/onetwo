FactoryGirl.define do
  factory :structure do
    association :creator, factory: :user
    association :organization
    association :parent, factory: :card

    current_context :channel
    current_slug    'cards'
  end
end

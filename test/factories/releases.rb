FactoryGirl.define do
  factory :release do
    sequence(:version) do |n|
      "1.2.#{n}"
    end
    platform "ios"
    form_factor "phone"
    notes "this is a release note"
    state 'active'
    sequence(:build) do |n|
      n
    end
  end
end

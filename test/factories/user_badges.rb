FactoryGirl.define do
  factory :user_badge do
    association :card_badging, factory: :card_badging
    association :user, factory: :user
  end
end

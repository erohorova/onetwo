FactoryGirl.define do
  factory :xapi_activity do
    actor   { |a| a.association(:user) }
    object  { |a| a.association(:card) }
    organization
    verb 'shared'
  end
end

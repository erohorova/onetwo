FactoryGirl.define do
  factory :user_subscription do
    org_subscription_id 1
    organization_id 1 
    user_id 1
    order_id 1
    amount 3500.00
    currency 'INR'
    start_date Date.today
    end_date Date.today + 1.year
    gateway_id 'A1B2C3DE'
  end
end

FactoryGirl.define do
  factory :developer_admin_invitation do
    recipient_email 'a@a.com'
    association :sender, factory: :user
    client
    first_name 'a'
    last_name 'a'
  end
end
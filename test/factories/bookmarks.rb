FactoryGirl.define do
  factory :bookmark do
    user
    bookmarkable {|a| a.association(:card) }
  end  
end

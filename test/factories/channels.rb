# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :channel do

    sequence :label do |n|
      "label#{n}"
    end

    organization

    trait :robotics do
      label 'Robotics'
      description 'Learn everything new about Robotics from this Channel'
    end

    trait :architecture do
      label 'Architecture'
      description 'Learn everything new about Architecture from this Channel'
    end

    trait :health do
      label 'Health'
      description 'Learn everything new about Health from this Channel'
    end

    trait :good_music do
      label 'Good Music'
      description 'Learn everything new about Music from this Channel'
    end

    trait :animals do
      sequence :label do |n|
        "Animals #{n}"
      end
      description 'Learn everything new about Animals from this Channel'
    end

    trait :cars do
      sequence :label do |n|
        "Cars #{n}"
      end
      description 'Learn everything new about Cars from this Channel'
    end


    trait :private do
      label 'private channel'
      description 'Something super private'
      is_private true
      client
    end

    trait :another_private do
      label 'another private channel'
      description 'Something super private'
      is_private true
      client
    end

    trait :mobile_image do
      mobile_image { File.new "#{Rails.root}/test/fixtures/images/logo.png" }
    end

    image { File.new "#{Rails.root}/test/fixtures/images/default_circle.jpg" }
    visible_during_onboarding false
  end

  factory :channel_with_topics, class: :channel do
    sequence :label do |n|
      "label#{n}"
    end

    organization
    user
    topics [{'label' => 'c#', 'name' => 'edcast.technology.software_development.c#'}]

    trait :promoted do
      is_promoted true
    end
  end

end

FactoryGirl.define do
  factory :dismissed_content do
    association :content, factory: :card

    user
  end
end

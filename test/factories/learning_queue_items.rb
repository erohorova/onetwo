FactoryGirl.define do
  factory :learning_queue_item do
    association :queueable, factory: :card
    user

    trait :from_bookmarks do
      source "bookmarks"
    end

    trait :from_assignments do
      source "assignments"
    end
  end
end

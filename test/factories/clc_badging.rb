FactoryGirl.define do
  factory :clc_badging do
    association :clc, factory: :clc
    association :clc_badge, factory: :clc_badge

    title 'this is dummy title'
  end
end

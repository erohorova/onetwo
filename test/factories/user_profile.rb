FactoryGirl.define do
  factory :user_profile do
    user
    expert_topics { Taxonomies.domain_topics }
    learning_topics { Taxonomies.domain_topics }
  end
end
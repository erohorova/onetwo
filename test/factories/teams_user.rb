FactoryGirl.define do
  factory :teams_user do
    transient do
      org {create(:organization)}
    end
    team {create(:team, organization: org)}
    user {create(:user, organization: org)}
    as_type 'member'
  end
end

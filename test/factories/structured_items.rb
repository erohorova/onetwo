FactoryGirl.define do
  factory :structured_item do
    association :entity, factory: :card
    structure

    trait :parent do
      parent { true }
    end
  end
end

FactoryGirl.define do
  factory :team_assignment do
    association :assignment, factory: :assignment
    association :team, factory: :team
    association :assignor, factory: :user
  end
end
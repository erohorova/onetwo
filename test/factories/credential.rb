FactoryGirl.define do
  factory :credential do
    client
    sequence(:api_key) { |n| "abc" + n.to_s }
    sequence(:shared_secret) { |n| "xyz" + n.to_s }
    state 'active'
  end
end
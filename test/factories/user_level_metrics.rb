FactoryGirl.define do
  factory :user_level_metric do
    user
    period 'day'
    offset 0
  end
end

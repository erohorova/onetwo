# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :configuration do
    name "name"
    value "value"
    type "Configuration"
  end

  factory :config do
    category 'settings'
    name 'some setting'
    data_type 'string'
    value 'yes'
    configable { |a| a.association(:organization) }

    trait :boolean do
      data_type 'boolean'
    end
  end
end

FactoryGirl.define do
  factory :team_level_metric do
    team
    period 'day'
    offset 0
  end
end

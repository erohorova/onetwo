FactoryGirl.define do
  factory :batch_job do
    last_run_time '2015-01-01T10:00:00Z'

    trait :follow_email do
      name 'FollowBatchEmailGenerator'
    end
  end
end

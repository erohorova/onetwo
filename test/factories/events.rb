# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :event do
    name "event"
    event_start Time.now
    event_end (Time.now + 2.hour)
    state 'open'
    user
    group
    privacy 'public'

    trait :public_virtual do
      privacy "public"
    end

    trait :public_real do
      privacy "public"
      address "1901 Old Middlefield Way, Mountain View"
    end

    trait :private_virtual do
      privacy "private"
    end

    trait :private_real do
      privacy "private"
      address "1901 Old Middlefield Way, Mountain View"
    end
  end
end

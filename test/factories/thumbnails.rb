FactoryGirl.define do
  factory :thumbnail do
    recording
    uri 'https://www.somewhere.com/1.png'
    is_default false
    sequence_number 0
  end
end

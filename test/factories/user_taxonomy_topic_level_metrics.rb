FactoryGirl.define do
  factory :user_taxonomy_topic_level_metric do
    user
    taxonomy_label "edcast.technology.data_science_and_analytics.big_data"
    period 'day'
    offset 0
  end
end

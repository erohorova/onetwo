FactoryGirl.define do
  factory :card_badge do
    image { File.new("test/fixtures/images/logo.png") }

    association :organization, factory: :organization
  end
end

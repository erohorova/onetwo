FactoryGirl.define do

  factory :stream do
    association :item, factory: :question
    action 'liked'
    user
  end
end
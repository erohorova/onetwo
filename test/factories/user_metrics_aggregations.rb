FactoryGirl.define do
  factory :user_metrics_aggregation do
    smartbites_commented_count 0
    smartbites_completed_count 0
    smartbites_consumed_count  0
    smartbites_created_count   0
    smartbites_liked_count     0
    time_spent_minutes         0
    total_smartbite_score      0
    total_user_score           0
    end_time                   0
    start_time                 0
  end
end

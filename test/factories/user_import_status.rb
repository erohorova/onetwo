FactoryGirl.define do
  factory :user_import_status do
    invitation_file

    existing_user false
    resent_email false
  end
end
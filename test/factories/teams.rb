FactoryGirl.define do
  factory :team do
    sequence :name do |n|
      "team #{n}"
    end
    description "this is a team"
    organization
    is_private false
  end
end

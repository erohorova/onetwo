FactoryGirl.define do
  factory :mailer_config do
    client
    organization
    address "smtp.mandrillapp.com"
    port 123
    user_name "user_name"
    password User.random_password
    domain "mandrillapp.com"
    from_address "test@gmail.com"
    from_name "test"
    webhook_keys "123, 456"
    enable_starttls_auto false
    authentication "login"
  end
end

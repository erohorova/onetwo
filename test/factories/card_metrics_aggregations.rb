FactoryGirl.define do
  factory :card_metrics_aggregation do
    organization
    card
    num_likes 0     
    num_views 0
    num_completions 0 
    num_bookmarks 0
    num_comments 0   
    end_time "1551657600"
    start_time "1551743999"     
    num_assignments 0
  end
end
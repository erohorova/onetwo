FactoryGirl.define do
  factory :user_topic_level_metric do
    user
    tag
    period 'day'
    offset 0
  end
end

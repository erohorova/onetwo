FactoryGirl.define do
  factory :access_log do
    user
    access_at Time.now.to_date
    platform 'web'
  end
end
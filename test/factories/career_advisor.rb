FactoryGirl.define do
  factory :career_advisor do
    skill 'data_scientist'
    links 'http://links.com'
  end
end

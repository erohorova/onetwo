FactoryGirl.define do
  factory :file_resource do
    association :attachable, factory: :card
    attachment Rack::Test::UploadedFile.new('test/fixtures/images/architecture.jpg', 'image/jpg')
    user
  end
end

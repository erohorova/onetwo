FactoryGirl.define do
  factory :topic_daily_learning do
    organization
    topic_id 'some-topic-id'
    topic_name 'some-topic-name'
    date Time.now.utc.to_date.to_s

    trait :initialized do
      processing_state DailyLearningProcessingStates::INITIALIZED
    end

    trait :started do
      processing_state DailyLearningProcessingStates::STARTED
    end

    trait :error do
      processing_state DailyLearningProcessingStates::ERROR
    end

    trait :completed do
      processing_state DailyLearningProcessingStates::COMPLETED
    end

  end
end

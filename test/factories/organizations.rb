# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :organization do
    # client
    sequence :name do |n|
      "organization #{n}"
    end
    sequence :host_name do |n|
      "host#{n}"
    end
    sequence :savannah_app_id do |n|
      n
    end
    description "organization description"
    send_weekly_activity true
  end
end

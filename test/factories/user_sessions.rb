FactoryGirl.define do
  factory :user_session do
    user
    start_time Time.now.utc
    end_time Time.now.utc + 1.minute 
  end
end

FactoryGirl.define do
  factory :card_pack_relation do
    association :cover, factory: :card
    association :from, factory: :card
    association :to, factory: :card
  end
end

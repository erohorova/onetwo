FactoryGirl.define do
  factory :user_connection do
    user
    connection_id :connection

    sequence :connection do |n|
      friend = create(:user)
      friend.id
    end

    trait :linkedin do
      network 'linkedin'
    end

    trait :facebook do
      network 'facebook'
    end
  end
end

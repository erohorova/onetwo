FactoryGirl.define do
  factory :user_content_completion do
    user
    completable { |a| a.association(:card) }
    state 'initialized'

    trait :completed do
      state UserContentCompletion::COMPLETED
    end

    trait :started do
      state UserContentCompletion::STARTED
    end
  end
end
FactoryGirl.define do 
  factory :monthly_top_video_stream do
    video_stream { |a| a.association(:wowza_video_stream) } 
    month_identifier "may-2016"
    votes_count 0
    winner false
  end
end

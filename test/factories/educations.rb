FactoryGirl.define do
  factory :education, class: School do
    name 'Sales University'
    specialization 'MS in computers'
    user
    from_year 1.years.ago.year.to_s
    to_year Time.now.year.to_s
  end
end
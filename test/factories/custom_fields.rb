FactoryGirl.define do
  factory :custom_field do
    organization

    display_name { 'Work Phone 2' }
  end

  factory :user_custom_field do
    custom_field
    user

    value { rand(36 ** 10).to_s(36) }
  end
end

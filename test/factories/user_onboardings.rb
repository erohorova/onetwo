FactoryGirl.define do
  factory :user_onboarding do
    user
    current_step 1
    status :new
  end
end

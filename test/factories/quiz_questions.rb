FactoryGirl.define do
  factory :quiz_question_option do
    is_correct false
    label 'option1'
  end

  factory :quiz_question_stats do 
  end

  factory :quiz_question_attempt do
    user
  end
end

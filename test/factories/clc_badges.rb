FactoryGirl.define do
  factory :clc_badge do
    image { File.new("test/fixtures/images/logo.png") }

    association :organization, factory: :organization
  end
end

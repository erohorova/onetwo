FactoryGirl.define do
  factory :clients_user do
    client
    association :user, factory: :user, email: nil
    sequence(:client_user_id)
  end
end

module OAuthStubs
  def stub_facebook_oauth_request(response=nil)
    stub_request(:get, "http://graph.facebook.com/1234567/picture?type=square").
        with( headers: {
          'Accept' => '*/*',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'User-Agent' => 'Ruby'
        }).
        to_return(response || {
          status: 200,
          body: File.read(Rails.root.join('test/fixtures/images/user2.jpg')),
          headers: { content_type:'image/jpg' }
        })
  end

  def stub_facebook_oauth_request_to_fail
    stub_facebook_oauth_request(
      status: 401
    )
  end

  def stub_linkedin_oauth_request
    stub_request(:get, "http://m.c.lnkd.licdn.com/mpr/mprx/0_gPLYktnxYeqCrzUc01X3kA9hYulmpzUc0AA3krFxTW5YiluBAvztoKPlKGAlx-sRyKF8wBz9KizD").
      with(headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent'=>'Ruby'
      }).
      to_return(
        status: 200,
        body: File.read(Rails.root.join('test/fixtures/images/user2.jpg')),
        headers: { content_type:'image/jpg' } )
  end
end

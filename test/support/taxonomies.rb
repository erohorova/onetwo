module Taxonomies
  def self.domain_topics
    [
      {
        "topic_name" => 'edcast.technology.software_development.c#',
        "topic_id" =>  SecureRandom.hex,
        "topic_label" =>  'c#',
        "domain_name" =>  'Technology',
        "domain_id" =>  SecureRandom.hex,
        "domain_label" =>  'edcast.technology'
      },
      {
        "topic_name" =>  'edcast.technology.software_development.robots',
        "topic_id" =>  SecureRandom.hex,
        "topic_label" =>  'Robots',
        "domain_name" =>  'Medicines',
        "domain_id" =>  SecureRandom.hex,
        "domain_label" =>  'edcast.medicines'
      },
      {
        "topic_name" =>  'edcast.technology.software_development.robots',
        "topic_id" =>  SecureRandom.hex,
        "topic_label" =>  'Robots',
        "domain_name" =>  'Artificial Intelligence',
        "domain_id" =>  SecureRandom.hex,
        "domain_label" =>  'edcast.artificial_intelligence'
      }
    ]
  end
end

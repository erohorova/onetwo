module Wallet
  def stub_products(purchased: [], purchasable: [])
    expected_response = []

    product_response = Proc.new do |object, is_purchasable|
      data = {
        'id'           => object.id,
        'content_id'   => object.id,
        'content_type' => object.class.name.underscore,
        'edu_amount'   => 10
      }
      if is_purchasable
        data.merge!('date_of_purchase' => nil, 'purchased' => 0)
      else
        data.merge!('date_of_purchase' => Date.today, 'purchased' => 1)
      end

      data
    end

    purchased.each do |product|
      expected_response << product_response.call(product, false)
    end

    purchasable.each do |product|
      expected_response << product_response.call(product, true)
    end

    Wallet::Communicator.any_instance.stubs(:products).returns(expected_response)
  end
end
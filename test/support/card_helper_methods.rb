module CardHelperMethods

  def setup_card_packs
    @org = create(:organization)
    @user = create(:user, organization: @org)

    @draft_cover = create(:card, card_type: 'pack', state: 'draft', card_subtype: 'simple', author_id: @user.id, title: 'draft cover')
    @pack_cover = create(:card, card_type: 'pack', state: 'draft', card_subtype: 'simple', author_id: @user.id, title: 'pack cover')

    @cards = {}
    [@draft_cover, @pack_cover].each do |cover|
      cards = create_list(:card, 3, author_id: @user.id)
      @cards[cover] = cards
      cards.each do |card|
        CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: card.class.name)
      end
    end

    @mixed_user = create(:user, organization: @org)
    @mixed_cover = create(:card, card_type: 'pack', card_subtype: 'simple', author_id: @mixed_user.id, title: "mixed cover")

    # We've deprecated this. If user wants to add a video stream t pathway
    # it would be card(video_stream type) that should be added to pathway
    # This is partailly impacting v1 apis and related code.
    stream1, stream2, stream3 = stub_commit_callbacks(WowzaVideoStream) do
      [
          create(:wowza_video_stream, :upcoming, creator: @mixed_user),
          create(:wowza_video_stream, :live, creator: @user),
          create(:wowza_video_stream, :past, creator: @mixed_user),
      ]
    end
    # callbacks stubbed.
    # running state callback
    [stream1, stream2, stream3].map(&:update_card_state)
    card1 = create(:card, is_public: true, author: @user)
    card2 = create(:card, is_public: true, author_id: @mixed_user.id)

    @cards[@mixed_cover] = cards = [stream1.card, card1, stream2.card, card2, stream3.card]
    cards.each do |card|
      CardPackRelation.add(cover_id: @mixed_cover.id, add_id: card.id, add_type: card.class.name)
    end
    @pack_cover.publish!
    @mixed_cover.publish!

    VideoStream.reindex
  end

  def setup_one_card_pack
    @org = create(:organization)
    @author = create(:user, organization: @org)
    @cover = create(:card, card_type: 'pack', card_subtype: 'simple', author: @author)
    @card1, @card2, @card3 = create_list(:card, 3, hidden: true, author: @author)
    @cards = @cover, @card1, @card2, @card3

    [@card1, @card2, @card3].each do |card|
      CardPackRelation.add(cover_id: @cover.id, add_id: card.id, add_type: card.class.name)
    end
  end

  def setup_pack_cards
    @org1 = create(:organization)
    @org2 = create(:organization)

    @user1 = create(:user, organization: @org1)
    @user2 = create(:user, organization: @org2)

    #covers
    @pack_covers1 = create_list(:card, 4, card_type: 'pack', card_subtype: 'simple', author_id: @user1.id, title: 'pack cover', organization: @org1)
    @pack_covers1 << create(:card, card_type: 'pack', card_subtype: 'simple', author_id: @user1.id, title: 'pack cover', organization: @org1, state: 'archived')

    # topics
    @topics = []
    @topics << create(:tag, name: 'sunny')
    @topics << create(:tag, name: 'pizza')
    @topics << create(:tag, name: 'san francisco')

    # channels
    @channels = []
    @channels << create(:channel, label: 'pogo', organization: @org1)
    @channels << create(:channel, label: 'disney', organization: @org1)
    @channels << create(:channel, label: 'cartoon network', organization: @org2)

    # org 1 packs
    @pack_covers1.each_with_index do |cover, idx|
      tags, channels = if idx.odd?
        [@topics.first(2), @channels.first(1)]
      else
        [ @topics.last(1), [@channels[1]] ]
      end
      add_tags_and_channels_to_cover cover, tags, channels
      add_cards_to_cover cover
    end
  end

  def add_tags_and_channels_to_cover(cover, tags, channels)
    cover.tags      = tags
    cover.channels  = channels
  end

  def add_cards_to_cover(cover)
    cards = create_list(:card, 3, author_id: cover.author_id, organization: cover.organization)
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: card.class.name)
    end
  end

  def create_pathway_in_org(organization, user)
    pathway_cover = create_pathway_without_index(organization, user)
    Card.searchkick_index.refresh
    pathway_cover
  end

  def create_pathway_without_index(organization, user)
    pathway_cover = create(:card, card_type: 'pack', card_subtype: 'simple', author_id: user.id, title: 'pathway-in-org')
    card = create(:card, author_id: user.id)
    CardPackRelation.add(cover_id: pathway_cover.id, add_id: card.id, add_type: card.class.name)

    pathway_cover
  end

  def search_result(item)
    Hashie::Mash.new(item.search_data)
  end

  def search_results(items)
    items.map &method(:search_result)
  end

  def get_ecl_card_response_for_video_card(video_ecl_id)
    ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items/:video_ecl_id"]
    ecl_response["data"]["id"] = video_ecl_id
    ecl_response
  end
end
module RequestHelperMethods
  def set_api_request_header(user)
    request.headers['X-Edcast-JWT'] = true
    request.headers['X-API-TOKEN'] = JwtAuth.encode user
  end

  def set_dev_api_request_header(user, credential)
    jwt_token = user.api_jwt_token(@credential.shared_secret)

    request.headers['X-DEVELOPER-API-KEY'] = credential.api_key
    request.headers['X-JWT-TOKEN'] = jwt_token
    set_host(credential.organization)
  end

  def set_ecl_api_request_header(payload)
    token = JWT.encode(payload, Settings.features.integrations.secret)

    request.headers["X-Api-Token"] = token
    request.headers["Content-Type"] = "application/json"
    set_host
  end

  def set_dev_v5_api_request_header(user, credential)
    payload = { 'email' => user.email }
    jwt_token = JWT.encode(payload, credential.shared_secret)
    authenticator = DeveloperApiAuthentication.new(api_key: credential.api_key, auth_token: jwt_token)
    authenticator.authenticate_external_api_request
    access_token = authenticator.generate_jwt

    request.headers['X-API-KEY'] = credential.api_key
    request.headers['X-ACCESS-TOKEN'] = access_token
    set_host(user.organization)
  end

  def api_v2_sign_in(user)
    set_api_request_header(user)
    set_host(user.organization)
  end

  def workflow_auth(user)
    request.headers['X-API-TOKEN'] = Settings.workflows.edcast_master_token
    set_host(user.organization)
  end

  def get_ecl_card_response(ecl_id)
    ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items/:ecl_id"]
    ecl_response["data"]["id"] = ecl_id
    ecl_response
  end

  def get_ecl_card_response_v3(ecl_id)
    ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v3/content_items/:ecl_id"]
    ecl_response["data"]["id"] = ecl_id
    ecl_response
  end

  def edcast_api_setup(client: create(:client),
                       api_key: nil,
                       shared_secret: nil,
                       resource_id: SecureRandom.hex,
                       resource_name: 'base api forum',
                       resource_description: 'i got sick of api calls that depended on rigid setup code',
                       timestamp: Time.now.to_i,
                       user_xid: '123', # caller's user identity
                       user: create(:user),
                       role: 'student',
                       email: nil,
                       params: {})

    api_key = client.valid_credential.api_key if api_key.nil?
    organization = create(:organization, client: client)
    shared_secret = client.valid_credential.shared_secret if shared_secret.nil?
    client_user = ClientsUser.create!(user: user, client: client, client_user_id: user_xid)
    token = Digest::SHA256.hexdigest([shared_secret, timestamp, user_xid, email, role, resource_id].compact.join('|'))

    {
        params: {api_key: api_key, resource_id: resource_id, resource_name: resource_name,
                 resource_description: resource_description, user_id: user_xid, token: token,
                 timestamp: timestamp, user_role: role, format: 'json'}.merge(params),
        objs: {client: client, client_user: client_user, user: user, organization: organization}
    }

  end


  def get_json_response response, symbolize: false
    resp = response.body.empty? ? "{}" : response.body
    resp = DeepSnakeCaser.deep_snake_case!(ActiveSupport::JSON.decode(resp))
    symbolize ? symbolize_keys_deep(resp) : resp
  end

  def set_host(org=nil)
    @request.host =
        case org
          when Organization
            org.custom_domain ? org.custom_domain : "#{org.host_name}.test.host"
          else
            'www.test.host'
        end
  end

  def reset_custom_headers
    custom_header_keys = []
    request.headers.each do |k, v|
      if k.start_with?('HTTP_X_')
        custom_header_keys << k
      end
    end

    custom_header_keys.each do |k|
      request.headers[k] = nil
    end
  end

  def standard_sign_in(user)
    post "#{@org.secure_home_page}/auth/users/sign_in", { 'user[email]' => user.email, 'user[password]' => user.password }, headers: @headers
  end

  def symbolize_keys_deep(obj)
    case obj
      when Array
        obj.map &method(:symbolize_keys_deep)
      when Hash
        Hash[obj.map {|k,v| [k.to_sym, symbolize_keys_deep(v)]}]
      else
        obj
    end
  end

end

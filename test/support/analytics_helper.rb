module AnalyticsTestHelper
  def unstub_metrics_recorder
    EdcastActiveJob.unstub(:perform_later)
    Analytics::MetricsRecorderJob.unstub(:perform_later)
  end

  def card_field_set(org, card, actor)
    field_set = {}
    field_set[:ecl_id] = card.ecl_id.to_s if card.ecl_id
    field_set[:card_title] = card.snippet
    field_set[:card_id] = card.id.to_s
    field_set[:card_subtype] = card.card_subtype
    field_set[:card_title] = card.snippet
    field_set[:user_id] = actor&.id&.to_s
    field_set[:org_name] = org.name
    field_set[:user_agent] = 'Edcast iPhone'
    field_set[:card_author_id] = card.try(:author_id).to_s
    field_set[:card_state] = card.state
    field_set[:card_type] = card.card_type
    field_set[:is_admin_request] = '0'
    field_set[:is_user_generated] = card.author_id ? '1' : '0'
    field_set[:is_public] = card.is_public? ? '1' : '0'
    field_set[:org_hostname] = org.host_name
    field_set[:is_live_stream] = card.video_stream&.status.eql?("live") ? '1' : '0'
    field_set[:is_card_promoted] = '0'
    field_set[:ecl_source_name] = card.ecl_source_name if card.ecl_source_name
    field_set[:platform] = 'web'
    field_set[:card_duration] = card.duration
    field_set[:readable_card_type] = card.card_type_display_name
    field_set[:average_rating] = card.card_metadatum&.average_rating
    #### NOTE: PII
    field_set[:card_author_full_name] = card.author.try(:full_name)
    field_set[:user_full_name] = actor.try(:full_name)
    field_set[:user_handle] = actor&.handle
    ####
    field_set.compact
  end

  def card_tag_set(card, actor, event)
    tag_set = {_user_id: actor&.id&.to_s, _card_id: card.id.to_s}
    tag_set[:is_system_generated] = '0'
    tag_set[:org_id] = card.organization_id.to_s
    tag_set[:event] = event

    tag_set
  end
end
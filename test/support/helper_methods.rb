module HelperMethods
  def setup_fb_omniauth
    {
      provider:     'facebook',
      uid:          '1234567',
      info: {
        nickname:   'jbloggs',
        email:      'joe@bloggs.com',
        name:       'Joe Bloggs',
        first_name: 'Joe',
        last_name:  'Bloggs',
        image:      'http://graph.facebook.com/1234567/picture?type=square',
        urls: {
          Facebook: 'http://www.facebook.com/jbloggs'
        },
        location:   'Palo Alto, California',
        verified:   true
      },
      credentials: {
        token:      'ABCDEF...', # OAuth 2.0 access_token, which you may wish to store
        expires_at: 1321747205, # when the access token expires (it always will)
        expires:    true # this will always be true
      },
      extra: {
        raw_info: {
          id:         '1234567',
          name:       'Joe Bloggs',
          first_name: 'Joe',
          last_name:  'Bloggs',
          link:       'http://www.facebook.com/jbloggs',
          username:   'jbloggs',
          location: {
            id:       '123456789',
            name:     'Palo Alto, California'
          },
          gender:     'male',
          email:      'joe@bloggs.com',
          timezone:   -8,
          locale:     'en_US',
          verified:   true,
          updated_time: '2011-11-11T06:21:03+0000'
        }
      }
    }
  end

  def setup_google_omniauth
    {
      provider:     'google',
      uid:          '123456789',
      info: {
        name:       'John Doe',
        email:      'john@companyname.com',
        first_name: 'John',
        last_name:  'Doe',
        image:      'https://lh3.googleusercontent.com/url/photo.jpg'
      },
      credentials: {
        token:         'token',
        refresh_token: 'another_token',
        expires_at:    1354920555,
        expires:       true
      },
      extra: {
        raw_info: {
          sub:      '123456789',
          email:    'user@domain.example.com',
          email_verified: true,
          name:     'John Doe',
          given_name:  'John',
          family_name: 'Doe',
          profile:  'https://plus.google.com/123456789',
          picture:  'https://lh3.googleusercontent.com/url/photo.jpg',
          gender:   'male',
          birthday: '0000-06-25',
          locale:   'en',
          hd:       'company_name.com'
        }
      }
    }
  end

  def setup_linkedin_omniauth
    {
      provider: 'linkedin',
      uid: '6mnOvjmlEW',
      info: {
        name: 'Trung Pham',
        email: 'trung@phamcom.com',
        nickname: 'Trung Pham',
        first_name: 'Trung',
        last_name: 'Pham',
        location: {
          country: { code: 'us' },
          name: 'San Francisco Bay Area'
        },
        description: 'Senior Staff Software Engineer at Visa - V.me and UltimatePay',
        image: 'http://m.c.lnkd.licdn.com/mpr/mprx/0_gPLYktnxYeqCrzUc01X3kA9hYulmpzUc0AA3krFxTW5YiluBAvztoKPlKGAlx-sRyKF8wBz9KizD',
        urls: { public_profile: 'http://www.linkedin.com/in/trungphamcom' }
      },
      credentials: {
        token: 'mytoken',
        expires_at: 1403573866,
        expires: true
      },
      extra: {
        raw_info: {
          emailAddress: 'trung@phamcom.com',
          firstName: 'Trung',
          headline: 'Senior Staff Software Engineer at Visa - V.me and UltimatePay',
          id: '6mnOvjmlEW',
          industry: 'Internet',
          lastName: 'Pham',
          location: {
            country: { code: 'us' },
            name: 'San Francisco Bay Area'
          },
          pictureUrl: 'http://m.c.lnkd.licdn.com/mpr/mprx/0_gPLYktnxYeqCrzUc01X3kA9hYulmpzUc0AA3krFxTW5YiluBAvztoKPlKGAlx-sRyKF8wBz9KizD',
          publicProfileUrl: 'http://www.linkedin.com/in/trungphamcom'
        }
      }
    }
  end

  def setup_lxp_oauth_omniauth
    {
      provider: "lxp_oauth",
      uid: "00ud29p6s62vb5NWq0h7",
      info: {
        email: "okta-user@gmail.com",
        first_name: "Okta",
        image: nil,
        last_name: "User",
        name: "Okta User",
        preferred_username: "1589184311142599-okta_user@gmail.com"
      },
      credentials: {
        token: "eyJraWQiOiJfUlhlWFh4R1NXMFRxSUQ5SmxhSjF3dl9ENWI0dkU3Z1BxUDgzb3lOZlBvIiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULjh0R2FPaEI5VUF2c29kUTkwblJuN2owXzB2R3lZU2piMEVlWlN1WjdBR0EiLCJpc3MiOiJodHRwczovL2Rldi01Nzk1NzUub2t0YXByZXZpZXcuY29tL29hdXRoMi9kZWZhdWx0IiwiYXVkIjoiYXBpOi8vZGVmYXVsdCIsImlhdCI6MTUxNDI3MDkwMCwiZXhwIjoxNTE0Mjc0NTAwLCJjaWQiOiIwb2FkMDNtdzkxdVNHWjJEVjBoNyIsInVpZCI6IjAwdWQyOXA2czYydmI1TldxMGg3Iiwic2NwIjpbImVtYWlsIiwib3BlbmlkIiwicHJvZmlsZSJdLCJzdWIiOiIxNTg5MTg0MzExMTQyNTk5a2Fub2ppeWFrYXZpdGFAZ21haWwuY29tIn0.QV_Go1wHhHxcgD5u8ZZbGzaj90Klch2rE_ELRe7kJVco1FKPw6yhinm-oEgeUoCRZi_QnvNi2OmQOsO7-lV0X2CzyGfAquaxOkSFne7TU3J9IKka1ip4TRNZQ3tQY1Pqbbrq9Jrn62wmdr_7Z2akuri5qyum8PkX_tjQRiFpyHuIFZXBVb2S4WgngFE5phJIWXNhQw3rxYiW9Y3K4ltqSsn0uApI0N9hu3CcsAIg-jiaNDL3wqpGHpx7eJhcF3Fk2zY9Hxr5kJHReyMfJXNkmJ2Jyc5ZxS5lDGBiQdyqEmFY8PjzFYoN7IUumXeoHz42xAkNWOIliAlUS5kOLRp-jQ",
        expires_at: 1514274650,
        expires: true,
        id_token: "eyJraWQiOiJfUlhlWFh4R1NXMFRxSUQ5SmxhSjF3dl9ENWI0dkU3Z1BxUDgzb3lOZlBvIiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiIwMHVkMjlwNnM2MnZiNU5XcTBoNyIsIm5hbWUiOiJLYXZpdGEgS2Fub2ppeWEiLCJlbWFpbCI6Imthbm9qaXlha2F2aXRhQGdtYWlsLmNvbSIsInZlciI6MSwiaXNzIjoiaHR0cHM6Ly9kZXYtNTc5NTc1Lm9rdGFwcmV2aWV3LmNvbS9vYXV0aDIvZGVmYXVsdCIsImF1ZCI6IjBvYWQwM213OTF1U0daMkRWMGg3IiwiaWF0IjoxNTE0MjcwOTAwLCJleHAiOjE1MTQyNzQ1MDAsImp0aSI6IklELjhLaUNUTGc1TllLSmdEV0ZEdDgzdEEzWmRkTEVIeGZmMkp2ZUtDNkdBdGciLCJhbXIiOlsicHdkIl0sImlkcCI6IjBvYWQwY202YXJjRHRxbXNvMGg3Iiwibm9uY2UiOiJZc0c3NmpvIiwicHJlZmVycmVkX3VzZXJuYW1lIjoiMTU4OTE4NDMxMTE0MjU5OWthbm9qaXlha2F2aXRhQGdtYWlsLmNvbSIsImF1dGhfdGltZSI6MTUxNDI3MDg5OCwiYXRfaGFzaCI6InFwdmpZY09HMU5TMWlpc3VJbGJUcHcifQ.iafYiUluNghHf55H-Voxo6VRp5V1XHX_wFl7ize5KQ3Bk8nuVdGsIChjINFW3p1HCKFYBaA2lpm5YLBY6ABgW-zlqQfkYms04cxcPQCHiJG737QntGYyVwcFSxUdtiJloraV38mNMaGvdQw_QnuCkb_ofELVVT1BiOj-QUppQbT270mF-mAyQrdaFP9tzdkO17j-b6EVmHaZ7cEXSvfyydl5g3hZnJKi4YuBUF1LlsUqOV6GqOLfw_OpF2_aDbpp4X6as7KTrN6H2fuerLqTCFt-4OcnXzihnOtqRrpJEnS-_-Cc3AlfvNvgdY9p8POCJdz0Cg13ZFMxNyixPh8QEw",
        refresh_token: 'bvZVOpjjakh6b57aGDX4lG-rxI4y4ztPLRUqVyDl4TI'
      },
      extra: {
        raw_info:  {
          sub: "00ud29p6s62vb5NWq0h7",
          name: "Kavita Kanojiya",
          profile: "https://www.facebook.com/app_scoped_user_id/1589184311142599/",
          locale: "en-US",
          email: "okta-user@gmail.com",
          preferred_username: "2-okta-user@gmail.com",
          given_name: "Kavita",
          family_name: "Kanojiya",
          zoneinfo: "America/Los_Angeles",
          updated_at: 1511947759,
          email_verified: true
        }
      }
    }
  end

  def setup_office365_omniauth
    {
      provider: 'office365',
      uid: '6mnOvjmlEW',
      info: {
        name: 'Trung Pham',
        email: 'trung@phamcom.com',
        nickname: 'Trung Pham',
        first_name: 'Trung',
        last_name: 'Pham',
        location: {
          country: { code: 'us' },
          name: 'San Francisco Bay Area'
        },
        description: 'Senior Staff Software Engineer at Visa - V.me and UltimatePay',
        image: 'http://m.c.lnkd.licdn.com/mpr/mprx/0_gPLYktnxYeqCrzUc01X3kA9hYulmpzUc0AA3krFxTW5YiluBAvztoKPlKGAlx-sRyKF8wBz9KizD',
        urls: { public_profile: 'http://www.linkedin.com/in/trungphamcom' }
      },
      credentials: {
        token: 'mytoken',
        expires_at: 1403573866,
        expires: true
      },
      extra: {
        raw_info: {
          emailAddress: 'trung@phamcom.com',
          firstName: 'Trung',
          headline: 'Senior Staff Software Engineer at Visa - V.me and UltimatePay',
          id: '6mnOvjmlEW',
          industry: 'Internet',
          lastName: 'Pham',
          location: {
            country: { code: 'us' },
            name: 'San Francisco Bay Area'
          },
          pictureUrl: 'http://m.c.lnkd.licdn.com/mpr/mprx/0_gPLYktnxYeqCrzUc01X3kA9hYulmpzUc0AA3krFxTW5YiluBAvztoKPlKGAlx-sRyKF8wBz9KizD',
          publicProfileUrl: 'http://www.linkedin.com/in/trungphamcom'
        }
      }
    }
  end

  def setup_org_oauth_omniauth
    oauth = setup_fb_omniauth
    oauth[:info].merge!(preferred_username: 'jbloggs')
    oauth.merge!(provider: 'org_oauth')
  end

  def setup_twitter_omniauth
    {
      provider: "twitter",
      uid:      "123456",
      info: {
        nickname: "johnqpublic",
        name:     "John Q Public",
        location: "Anytown, USA",
        image:  "http://si0.twimg.com/sticky/default_profile_images/default_profile_2_normal.png",
        description: "a very normal guy.",
        urls: {
          Website: nil,
          Twitter: "https://twitter.com/johnqpublic"
        }
      },
      credentials: {
        token:  "a1b2c3d4...", # The OAuth 2.0 access token
        secret: "abcdef1234"
      },
      extra: {
        access_token: '', # An OAuth::AccessToken object
        raw_info: {
          name:         "John Q Public",
          listed_count: 0,
          profile_sidebar_border_color: "181A1E",
          url: nil,
          lang:         "en",
          statuses_count: 129,
          profile_image_url: "http://si0.twimg.com/sticky/default_profile_images/default_profile_2_normal.png",
          profile_background_image_url_https: "https://twimg0-a.akamaihd.net/profile_background_images/229171796/pattern_036.gif",
          location:     "Anytown, USA",
          time_zone:    "Chicago",
          follow_request_sent: false,
          id:           123456,
          profile_background_tile: true,
          profile_sidebar_fill_color: "666666",
          followers_count: 1,
          default_profile_image: false,
          screen_name:  '',
          following:    false,
          utc_offset:   -3600,
          verified:     false,
          favourites_count: 0,
          profile_background_color: "1A1B1F",
          is_translator: false,
          friends_count: 1,
          notifications: false,
          geo_enabled:   true,
          profile_background_image_url: "http://twimg0-a.akamaihd.net/profile_background_images/229171796/pattern_036.gif",
          protected:     false,
          description:   "a very normal guy.",
          profile_link_color: "2FC2EF",
          created_at:    "Thu Jul 4 00:00:00 +0000 2013",
          id_str:        "123456",
          profile_image_url_https: "https://si0.twimg.com/sticky/default_profile_images/default_profile_2_normal.png",
          default_profile: false,
          profile_use_background_image: false,
          entities: { description: { urls: [] } },
          profile_text_color: "666666",
          contributors_enabled: false
        }
      }
    }
  end

  def sso_info(user: nil)
    {
      'auth_provider' => 'saml',
      'external_uid'  => '123456789',
      'raw_info' => {
        'country'        => ['IN'],
        'first_name'     => [user.try(:first_name) || 'John'],
        'IdentityType'   => ['P,C', 'E'],
        'Industry Group' => ['Ge Service'],
        'last_name'      => [user.try(:last_name) || 'Doe'],
        'Preferred Name' => ['Service']
      }
    }
  end

  def setup_salesforce_omniauth
    raw_info = ActiveSupport::JSON.decode(<<-JSON
        {
          "id":"https://login.salesforce.com/id/00D50000000IZ3ZEAW/00550000001fg5OAAQ",
          "asserted_user":true,
          "user_id":"00550000001fg5OAAQ",
          "organization_id":"00D50000000IZ3ZEAW",
          "username":"trung@phamcom.com",
          "nick_name":"user1.2950476911907334E12",
          "display_name":"Sample User",
          "email":"trung@phamcom.com",
          "email_verified": true,
          "first_name": "Sample",
          "last_name": "User",
          "status":{
            "created_date":"2010-11-08T20:55:33.000+0000",
            "body":"Working on OAuth 2.0 article"
          },
          "photos":{
            "picture":"https://c.na1.content.force.com/profilephoto/005/F",
            "thumbnail":"https://c.na1.content.force.com/profilephoto/005/T"
          },
          "addr_street": null,
          "addr_city": null,
          "addr_state": "ca",
          "addr_country": "US",
          "addr_zip": null,
          "mobile_phone": null,
          "mobile_phone_verified": false,
          "urls":{
            "enterprise":"https://na1.salesforce.com/services/Soap/c/{version}/00D50000000IZ3Z",
            "metadata":"https://na1.salesforce.com/services/Soap/m/{version}/00D50000000IZ3Z",
            "partner":"https://na1.salesforce.com/services/Soap/u/{version}/00D50000000IZ3Z",
            "rest":"https://na1.salesforce.com/services/data/v{version}/",
            "sobjects":"https://na1.salesforce.com/services/data/v{version}/sobjects/",
            "search":"https://na1.salesforce.com/services/data/v{version}/search/",
            "query":"https://na1.salesforce.com/services/data/v{version}/query/",
            "recent":"https://na1.salesforce.com/services/data/v{version}/recent/",
            "profile":"https://na1.salesforce.com/00550000001fg5OAAQ"
          },
          "active":true,
          "user_type":"STANDARD",
          "language":"en_US",
          "locale":"en_US",
          "utcOffset":-28800000,
          "last_modified_date":"2011-01-14T23:28:01.000+0000",
          "is_app_installed": true,
          "custom_attributes": {
            "title": "Developer Evangelist"
          }
        }
        JSON
      )

      rack_app = []
      rack_app.stubs :call
      strategy = OmniAuth::Strategies::Salesforce.new rack_app, 'Consumer Key', 'Consumer Secret'

      client = OAuth2::Client.new 'id', 'secret', {:site => 'example.com'}
      client_id = "https://login.salesforce.com/id/00Dd0000000d45TEBQ/005d0000000fyGPCCY"
      issued_at = "1331142541514"
      signature = Base64.strict_encode64(OpenSSL::HMAC.digest('sha256', strategy.options.client_secret.to_s, client_id + issued_at))
      access_token = OAuth2::AccessToken.from_hash client, {
            'id' => client_id,
            'access_token' => 'faketoken',
            'issued_at' => issued_at,
            'instance_url' => 'http://instance.salesforce.example',
            'signature' => signature
          }
      strategy.stubs(:raw_info).returns(raw_info)
      strategy.stubs(:access_token).returns(access_token)

      strategy.auth_hash
  end

  def url_with_no_parameters(url)
    url.gsub(/\?\d+/, '')
  end

  def stub_mongo_requests
    Mongo::Collection.any_instance.stubs(:insert)
    Mongo::Collection.any_instance.stubs(:insert_many)
  end


  def stub_ip_lookup(ip_address: nil, response_body: {})
    stub_request(:get, "#{Settings.free_geo_ip}#{ip_address}").
      to_return(status: 200, body: response_body.to_json, headers: {})
  end

  def stub_model_callbacks
    VideoStream.any_instance.stubs(:schedule_reminder_push_notification).returns true
    Card.any_instance.stubs(:after_tagged)
    Card.any_instance.stubs :reindex_card
    Card.any_instance.stubs(:all_time_views_count).returns(0)
    Group.any_instance.stubs(:reindex_async)
    Team.any_instance.stubs(:send_group_created_metric)
    # TeamsUser.any_instance.stubs(:record_group_event)
    Assignment.any_instance.stubs(:push_assignment_metric)
    Role.any_instance.stubs(:create_role_permissions)
    Post.any_instance.stubs :p_read_by_creator

    Recommend.stubs(:get_recommendations)
    PubnubNotifier.stubs(:send_payload)
    MetricRecord.stubs(:create)

    stub_user_callbacks
    stub_organization_callbacks
  end

  def stub_user_callbacks
    User.any_instance.stubs(:associate_forum_user_if_present)
    User.any_instance.stubs(:cleanup_invitation)
    User.any_instance.stubs(:strip_all_tags_from_user_input)
    User.any_instance.stubs(:add_to_org_general_channel)
    User.any_instance.stubs(:generate_user_onboarding)
    User.any_instance.stubs(:update_role)
    User.any_instance.stubs(:update_interests)
    User.any_instance.stubs(:update_websites)
    User.any_instance.stubs(:update_mentors)
    User.any_instance.stubs(:update_everyone_group_members)
    User.any_instance.stubs(:setup_lms_user)
    User.any_instance.stubs(:add_user_to_everyone_team)
    User.any_instance.stubs(:generate_images)
  end

  def stub_organization_callbacks
    Organization.any_instance.stubs(:create_general_channel)
    Organization.any_instance.stubs(:default_sso)
    Organization.any_instance.stubs(:create_default_config)
    Organization.any_instance.stubs(:create_org_level_team)
    Organization.any_instance.stubs(:create_default_roles)
    Organization.any_instance.stubs(:create_admin_user)
    Organization.any_instance.stubs(:create_org_card_badges)
    Organization.any_instance.stubs(:create_or_update_roles)
    Organization.any_instance.stubs(:add_onboarding_options)
    Organization.any_instance.stubs(:create_analytics_reporting_configs)
    Organization.any_instance.stubs(:get_channel_permissions_for).returns(true)
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(true)
    Organization.any_instance.stubs(:enable_sociative)
    Organization.any_instance.stubs(:set_influx_service_token)
  end

  def stub_background_jobs
    ActiveJob::Base.descendants.each do |_class|
      _class.stubs(:perform_later)
    end
    NotificationGeneratorJob.stubs(:set)
  end

  # Wouldn't recommend using this method except one particular situation
  # Wherever required, pls unstub specific JobClass.unstub :perform_later
  # and ParentClass.unstub :perform_later -> if JobClass has parent class
  # if above option doesnt work, pls use below method to unstub all job classes
  def unstub_background_jobs
    ActiveJob::Base.descendants.each do |_class|
      _class.unstub(:perform_later)
    end
    NotificationGeneratorJob.unstub(:set)
  end

  def stub_users_content_queue
    UserContentsQueue.stubs(:remove_from_queue)
  end

  def stub_tracking
    Tracking.stubs(:track_act)
  end

  def stub_elasticsearch_requests
    stub_request(:post, "http://localhost:9200/_bulk")
    stub_request(:get, "http://localhost:9200/") #elasticsearch
    stub_request(:get, "#{Settings.google.maps_api}/maps/api/geocode/json")
    stub_request(:post, "http://localhost:9200/")
  end

  def stub_influx_requests
    InfluxdbRecorder.stubs(:record).returns OpenStruct.new(code: "204")

    stub_request(:get, "http://localhost:8086/") #influx
    stub_request(:post, "http://localhost:8086/") #influx
    INFLUXDB_CLIENT.stubs :write_point
    INFLUXDB_CLIENT.stubs :write_points
  end

  def stub_bulk_request
    stub_request(:post, "http://localhost:9200/_bulk")
  end

  def stub_workflow_patch_request
    stub_request(:post, 'http://workflow.test/api/edcast/handle-patches')
  end

  def stub_redis_requests
    Sidekiq::JobStatus.any_instance.stubs(:enqueuable?).returns(true)
    Sidekiq::JobStatus.any_instance.stubs(:set_status)
    Sidekiq::JobStatus.any_instance.stubs(:delete).returns([])
    REDIS_LOCK_MANAGER.stubs(:lock).returns true
    REDIS_LOCK_MANAGER.stubs(:unlock)
  end

  def widget_auth_params(group: nil, user: nil, role:'member')
    credential = group.valid_credential
    client_user = create(:clients_user, client: group.organization.client, user: user)
    api_key = credential.api_key
    shared_secret = credential.shared_secret
    timestamp = Time.now.to_i
    parts = [shared_secret, timestamp, client_user.client_user_id, role, group.client_resource_id].compact
    token = Digest::SHA256.hexdigest(parts.join('|'))

    {
        api_key: api_key,
        timestamp: timestamp,
        token: token,
        user_role: role,
        user_id: client_user.client_user_id,
        resource_id: group.client_resource_id
    }
  end

  def create_gu(user_opts: {}, group_opts: {}, role: 'member')
    u = create(:user, user_opts)
    group_opts.merge!(organization: u.organization)
    g = create(:group, group_opts)
    if role == 'admin'
      g.add_admin(u)
    else
      g.add_member(u)
    end
    return g, u
  end

  def create_default_ssos
    (Sso::DEFAULT_SSO_NAMES + ['lxp_oauth', 'internal_content'] ).each { |sso_name| Sso.find_or_create_by(name: sso_name) }
  end

  def create_default_org
    Organization.unstub(:default_org)
    Organization.default_org || Organization.create!(name: "EdCast", host_name: "www", is_open: true, description: "Edcast open channel")
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(true)
  end

  def sample_instrumentation_request_payload
    {:controller => "Api::V2::UsersController",
      :action => "suggested",
      :params => {
        "format" => "json",
        "controller" => "api/v2/users",
        "action" => "suggested"
      },
      :format => :json,
      :method => "GET",
      :path => "/api/v2/users/suggested.json",
      :user_id => 12,
      :platform => "web",
      :request_ip => "127.0.0.1",
      :request_host => "org47.lvh.me:4000",
      :build_number => nil,
      :platform_version_number => nil,
      :user_agent => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36",
      :status => 200,
      :org_id => 2,
      :view_runtime => 35.31672900396213,
      :searchkick_runtime => 0,
      :db_runtime => 7.948,
      :name => "process_action.action_controller",
      :start => 1509487205,
      :finish => 1509487205,
      :id => "cb843902fb936c440012"
    }
  end

  def get_sample_topics
    sample_topic_1 = {
      topic_name: "fake topic name 1",
      topic_id: "fake topic id 1",
      topic_label: "fake topic label 1",
      domain_name: "fake domain name 1",
      domain_id: "fake domain id 1",
      domain_label: "fake domain label 1"
    }

    sample_topic_2 = {
      topic_name: "fake topic name 2",
      topic_id: "fake topic id 2",
      topic_label: "fake topic label 2",
      domain_name: "fake domain name 2",
      domain_id: "fake domain id 2",
      domain_label: "fake domain label 2"
    }
    [sample_topic_1, sample_topic_2]
  end

  # Forces all threads to share the same connection between Celluloid actors
  # ActiveRecord::Base.shared_connection = ActiveRecord::Base.connection

  # assert_differences([['Model1.count', 2], ['Model2.count', 3]])
  # or
  # assert_differences([[->{Model1.count}, 2], [->{Model2.count}, 3]])
  #
  # The expression array can freely mix the String and Proc forms.
  #
  def assert_differences(expression_array, message = nil, &block)
    def result(expr, b)
      expr.is_a?(String) ? eval(expr, b) : expr.call
    end

    b = block.send(:binding)
    before = expression_array.map { |expr| result(expr[0], b) }

    yield

    expression_array.each_with_index do |pair, i|
      e = pair[0]
      difference = pair[1]
      error = "#{e.inspect} didn't change by #{difference}"
      error = "#{message}\n#{error}" if message
      assert_equal(before[i] + difference, result(e, b), error)
    end
  end

  # compare two arrays of either objects (responding to .id) or hashes (with key 'id')
  # arrays of Hashie::Mashes work
  def assert_same_ids(list1, list2, ordered: true, message: nil)
    def get_id(obj)
      case obj
        when ActiveRecord::Base, OpenStruct, Hashie::Mash
          obj.id
        when Hash
          obj.with_indifferent_access[:id]
      end
    end
    l1_ids = list1.map &method(:get_id)
    l2_ids = list2.map &method(:get_id)
    if ordered
      assert_equal l1_ids, l2_ids, message
    else
      assert_same_elements l1_ids, l2_ids, message
    end
  end

  # compares hashes or array trees of hashes
  def assert_equal_hashdiff(a, b, orig: nil)
    parents = orig || [a,b]
    types = [a,b].map(&:class).uniq
    case types
      when [Array]
        a.zip(b).each do |an, bn|
          assert_equal_hashdiff an, bn, orig: parents
        end
      when [Hash]
        orig_msg = orig && "\n-#{orig.first}\n+#{orig.last}"
        assert (a == b), "#{orig_msg}\n<-#{a}\n<+#{b}\n<>#{HashDiff.diff(a,b)}"
      else
        assert false, "assert_equal_hashdiff called with mixed types #{types}"
    end
  end

  def create_org_master_roles(org)
    Role.any_instance.unstub :create_role_permissions
    Role.create_master_roles org
  end

  def setup_journey_card(user)
    @journey = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: user)
    # section in journey (pack:simple card) is hidden and draft by default(based on recent changes)
    @section1, @section2, @section3 = create_list(:card, 3, card_type: 'pack', card_subtype: 'simple', author: user, hidden: true)
    @sections = @section1, @section2, @section3
    @journey_pack = @journey, @section1, @section2, @section3
    @sections.each do |cover|
      CardPackRelation.add(
        cover_id: cover.id,
        add_id: create(:card, author: user).id,
        add_type: cover.class.name
      )
    end
    @sections.each do |s|
      JourneyPackRelation.add(cover_id: @journey.id, add_id: s.id)
    end
  end

  def setup_weekly_journey_card(user)
    @journey_weekly = create(:card, card_type: 'journey', card_subtype: 'weekly', author: user)
    JourneyPackRelation.find_by(cover_id: @journey_weekly.id).update_attributes(start_date: Time.now + 1.hour)
    @section1w, @section2w, @section3w = create_list(:card, 3, card_type: 'pack', card_subtype: 'simple', author: user)
    @sections_weekly = @section1w, @section2w, @section3w
    @journey_pack_weekly = @journey_weekly, @section1w, @section2w, @section3w
    @sections_weekly.each do |cover|
      CardPackRelation.add(
        cover_id: cover.id,
        add_id: create(:card, author: user).id,
        add_type: cover.class.name
      )
    end
    @sections_weekly.each do |s|
      JourneyPackRelation.add(cover_id: @journey_weekly.id, add_id: s.id)
    end
  end

  def get_ecl_card_response_for_video_card(video_ecl_id)
    ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items/:video_ecl_id"]
    ecl_response["data"]["id"] = video_ecl_id
    ecl_response
  end

  def stub_video_stream_index_calls
    [VideoStream, WowzaVideoStream, IrisVideoStream].each do |klass|
      klass.any_instance.stubs :create_index
      klass.any_instance.stubs :update_index
      klass.any_instance.stubs :populate_meta_data
    end
  end

  def stub_configs_readable_card_types
    default_org_object = Organization.new(name: "EdCast", host_name: "www", is_open: true, description: "Edcast open channel")
    Organization.stubs(:default_org).returns(default_org_object)
    readable_card_types = {
                           course: 1, webinar: 2, classroom: 3, video: 4, article: 5, book: 6,
                           endorsement: 7, compressed_file: 8, excel: 9, image: 10, music: 11,
                           pdf: 12, powerpoint: 13, podcast: 14, livefeed: 15, assessment: 16,
                           blog_post: 17, mission: 18, text: 19, project: 20, webcast: 21,
                           wiki: 22, quiz: 23, curricula: 24, audio_books: 25, books: 26,
                           diagnostics: 27, jobs: 28, simulation: 29, vlabs: 30,
                           introduction: 31, action_step: 32, connect_block: 33,
                           connect_item: 34, connect_topic: 35, curriculum: 36, discussion: 37,
                           document: 38, evaluation_test: 39, event: 40,
                           external_training: 41, form: 42, library: 43, material: 44,
                           note: 45, objective: 46, observation_checklist: 47,
                           online_class: 48, online_resource: 49, q_a: 50,
                           quick_course: 51, section: 52, session: 53, test: 54,
                           topic: 55, sco: 56, social_learning_program: 57
                          }.with_indifferent_access
    Organization.any_instance.stubs(:fetch_readable_card_types_configs).returns(readable_card_types)
  end
end

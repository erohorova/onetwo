module OktaFederatedStubs
  def stub_okta_session_token
    Organization.any_instance.stubs(:current_okta_domain).returns(Settings.okta.domain)
    session_json = File.read(Rails.root.join('test/fixtures/api/okta_session_token.json'))
    Okta::InternalFederation.any_instance.stubs(:session_token).returns(JSON.parse(session_json))
  end

  def stub_okta_user_update
    # No user exists with matching email in Okta.
    Organization.any_instance.stubs(:current_okta_domain).returns(Settings.okta.domain)
    Okta::InternalFederation.any_instance.stubs(:update_user).returns(nil)
  end

  def stub_okta_group_creation
    Organization.any_instance.stubs(:current_okta_domain).returns(Settings.okta.domain)
    Okta::Provision.any_instance.stubs(:create_group).returns({'id' => "abc"})
  end

  def stub_okta_group_app_link
    Organization.any_instance.stubs(:current_okta_domain).returns(Settings.okta.domain)
    Okta::Provision.any_instance.stubs(:link_app_with_group).returns({'id' => "abc"})
  end

  def stub_okta_app_creation
    Organization.any_instance.stubs(:current_okta_domain).returns(Settings.okta.domain)
    Okta::Provision.any_instance.stubs(:create_app).returns({
                                                              'id' => "0oad0ipbmpTemalZO0h7",
                                                              'credentials' => {
                                                                'oauthClient' => {
                                                                  'client_secret' => "JZzitxUG-ojLtzHIKgpIT-7Ze7LPwAuXd9lpi_4d"
                                                                }
                                                              }
                                                            })
  end

  def stub_okta_idp_creation
    Organization.any_instance.stubs(:current_okta_domain).returns(Settings.okta.domain)
    Okta::Provision.any_instance.stubs(:create_facebook_idp).returns({'id' => "abc"})
    Okta::Provision.any_instance.stubs(:create_google_idp).returns({'id' => "abc"})
    Okta::Provision.any_instance.stubs(:create_linkedin_idp).returns({'id' => "abc"})
  end

  def stub_okta_user_password_update
    Organization.any_instance.stubs(:current_okta_domain).returns(Settings.okta.domain)
    # No user exists with matching email in Okta.
    Okta::InternalFederation.any_instance.stubs(:update_user).returns(nil)
    # No user exists with matching email in Okta.
    Okta::InternalFederation.any_instance.stubs(:federated_user).returns(nil)
    # Then proceeds to create one, but fails.
    Okta::InternalFederation.any_instance.stubs(:create_user).returns(nil)
    stub_okta_session_token
    stub_request(:get, "http://localhost:9200/edcast_users_test/_search")
  end

  def stub_okta_user
    Organization.any_instance.stubs(:current_okta_domain).returns(Settings.okta.domain)

    user_json = File.read(Rails.root.join('test/fixtures/api/okta_user.json'))
    Okta::InternalFederation.any_instance.stubs(:user).returns(JSON.parse(user_json))

    stub_okta_session_token

    @mocked_federated_user = Okta::InternalFederation.new(organization: @organization).user
  end

  def stub_failed_user_creation
    Organization.any_instance.stubs(:current_okta_domain).returns(Settings.okta.domain)
    # No user exists with matching email in Okta.
    Okta::InternalFederation.any_instance.stubs(:federated_user).returns(nil)
    # Then proceeds to create one, but fails.
    Okta::InternalFederation.any_instance.stubs(:create_user).returns(nil)
  end

  def stub_user_logout
    Organization.any_instance.stubs(:current_okta_domain).returns(Settings.okta.domain)
    # No user exists with matching email in Okta.
    Okta::InternalFederation.any_instance.stubs(:federated_user).returns({'id' => "abc"})
    # Then proceeds to create one, but fails.
    Okta::InternalFederation.any_instance.stubs(:sign_out_user).returns(true)
  end
end

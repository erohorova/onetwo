require 'test_helper'


class UserBridgeTest < ActiveSupport::TestCase
  setup do
    Role.any_instance.unstub :create_role_permissions

    @org       = create :organization
    @user      = create :user, organization: @org, bio: 'I am BATMAN. Hiding in the shadows.'
    @profile   = create :user_profile, user: @user, language: 'en', time_zone: 'Asia/Kolkata', tac_accepted: true, tac_accepted_at: Time.now

    admin_role = create :role, name: 'admin', organization: @org
    admin_role.create_role_permissions
    @user.add_role('admin')

    Organization.any_instance.stubs(:web_session_timeout).returns('100')
    User.any_instance.stubs(:get_all_user_permissions).returns(Permissions.get_permissions_for_role('admin'))
  end

  test 'returns minimal information when #fields is not specified' do
    klass = UserBridge.new([@user], user: @user)
    attributes = klass.attributes[0]

    # personal information
    assert_equal @user.id, attributes[:id]
    assert_equal @user.email, attributes[:email]
    assert_match /#{@user.full_name}/, attributes[:fullName]
    assert_match /#{@user.handle}/, attributes[:handle]
    assert_match /http:\/\/cdn-test-host.com\/assets\/new-anonymous-user-medium.jpeg/, attributes[:picture]
    assert_match /http:\/\/cdn-test-host.com\/assets\/default_org_image_original.jpg/, attributes[:organization][:imageUrl]

    # authority
    assert attributes[:isAdmin]
    refute attributes[:isSuperAdmin]

    assert_equal 40, attributes[:permissions].length

    # organisation subscribed?
    refute attributes[:orgAnnualSubscriptionPaid]

    #related organisation
    assert_equal @org.host_name, attributes[:organization][:hostName]
    assert_equal '100', attributes[:organization]['web_session_timeout']

    # profile
    assert_equal @user.profile.id, attributes[:profile][:id]
    assert_equal 'Asia/Kolkata', attributes[:profile][:timeZone]
    assert attributes[:profile][:expertTopics]
    assert attributes[:profile][:learningTopics]
    assert_equal 'en', attributes[:profile][:language]
  end

  test 'returns org default language when user language is nil' do
    user = create :user, organization: @org, bio: 'I am BATMAN. Hiding in the shadows.'
    profile = create :user_profile, user: user, time_zone: 'Asia/Kolkata', tac_accepted: true, tac_accepted_at: Time.now
    create(:config, name: 'DefaultOrgLanguage', value: 'French', configable: @org, configable_type: 'Organization')
    klass = UserBridge.new([user], user: user)
    attributes = klass.attributes[0]
    assert_equal 'French', attributes[:profile][:language]
  end

  test 'supports on-demand asks for oneself' do
    # add subscription to the user
    subscription = create(:user_subscription, user_id: @user.id, organization_id: @org.id)

    following = create :user, organization: @org
    follower  = create :user, organization: @org
    channel = create :channel, organization: @org

    create :follow, followable: following, user: @user
    create :follow, followable: @user, user: follower
    create :follow, followable: channel, user: @user

    @user.update_attribute(:hide_from_leaderboard, true)

    @user.reload

    klass = UserBridge.new([@user], user: @user, fields: 'followers_count,following_count,bio,following_channels,user_subscriptions,hide_from_leaderboard')
    attributes = klass.attributes[0]
    _subscription_response = attributes[:userSubscriptions]

    # bio
    assert_match /#{@user.bio}/, attributes[:bio]

    # hide from leaderboard
    assert_equal true, attributes[:hideFromLeaderboard]

    # count of users that follows a user
    assert_equal 1, attributes[:followingCount]

    # count of users that a user follows
    assert_equal 1, attributes[:followersCount]

    # list of channels that a user is following
    assert_equal [{ id: channel.id }], attributes[:followingChannels]

    assert_equal subscription.id, _subscription_response[:id]
    assert_equal subscription.currency, _subscription_response[:currency]
    assert_equal subscription.amount, _subscription_response[:amount]
  end

  test 'supports on-demand asks for list of users' do
    # create SME
    sme = create :role, name: 'sme', organization: @org
    member = create :role, name: 'member', organization: @org

    followed_user   = create :user, organization: @org, organization_role: 'admin'
    unfollowed_user = create :user, organization: @org
    unattended_user = create :user, organization: @org

    # followed_user is an SME
    followed_user.add_role('sme')

    unfollowed_user.add_role('member')
    unattended_user.add_role('member')
    # @user follows a `followed_user`.
    create :follow, followable: followed_user, user: @user
    # @user unfollows a `unfollowed_user`.
    follow = create :follow, followable: unfollowed_user, user: @user
    follow.destroy
    # @user does not follow `unattended_user`.
    # ...

    @user.reload
    followed_user.reload

    klass = UserBridge.new([followed_user, unfollowed_user, unattended_user], user: @user, fields: 'id,roles,is_following')
    response = klass.attributes

    # followed_user:
    user = response.select { |u| u[:id] == followed_user.id }[0]
    assert_same_elements ['sme'], user[:roles]
    assert user[:isFollowing]

    # unfollowed_user:
    user = response.select { |u| u[:id] == unfollowed_user.id }[0]
    assert_same_elements ['member'], user[:roles]
    refute user['isFollowing']

    # user:
    user = response.select { |u| u[:id] == unattended_user.id }[0]
    assert_same_elements ['member'], user[:roles]
    refute user['isFollowing']
  end

  test 'support on-demand ask: `channel_ids` for a user following the channels' do
    channel = create :channel, organization: @org
    user = create :user, organization: @org
    user.direct_following_channels << channel

    response = UserBridge.new([user], user: @user, fields: 'channel_ids').attributes
    user = response[0]

    assert_equal :channelIds, user.keys.first
    assert_same_elements [channel.id], user[:channelIds]
  end

  test 'fetch ids of teams of a user as `team_ids`' do
    team = create :team, organization: @org
    user = create :user, organization: @org
    create :teams_user, user: user, team: team

    response = UserBridge.new([user], user: @user, fields: 'team_ids').attributes
    user = response[0]

    assert_equal :teamIds, user.keys.first
    assert_same_elements [team.id], user[:teamIds]
  end

  test 'support on-demand ask: is_private of writable_channels' do
    channel = create :channel, organization: @org
    user = create :user, organization: @org
    channel.authors << user
    response = UserBridge.new([user], user: @user, fields: 'writable_channels,channels(id,slug,label,is_private)').attributes
    assert_equal channel.id, response[0][:writableChannels][0][:id]
    assert_same_elements [:id, :slug, :isPrivate, :label], response[0][:writableChannels][0].keys
  end

  test 'support on-demand ask: get default attributes of writable_channels' do
    channel = create :channel, organization: @org
    user = create :user, organization: @org
    channel.authors << user
    response = UserBridge.new([user], user: @user, fields: 'writable_channels').attributes
    assert_equal channel.id, response[0][:writableChannels][0][:id]
    assert_same_elements [:id, :slug, :label], response[0][:writableChannels][0].keys
  end

  test 'support on-demand ask: get required attributes of profile' do
    klass = UserBridge.new([@user], user: @user, fields: 'profile(tac_accepted,tac_accepted_at,language),profile')
    attributes = klass.attributes[0]
    assert_equal @user.profile.tac_accepted, attributes[:profile][:tacAccepted]
    assert_equal Date.today, attributes[:profile][:tacAcceptedAt].to_date
    assert_equal 'en', attributes[:profile][:language]
  end

  test 'support on-demand ask: get onboarding completed date' do
    @onboarding = create :user_onboarding, user: @user
    @onboarding.start!
    @onboarding.complete!
    klass = UserBridge.new([@user], user: @user, fields: 'onboarding_completed_date')
    attributes = klass.attributes[0]
    assert_equal @onboarding.reload.completed_at, attributes[:onboardingCompletedDate]
  end

  test 'should returns default respone for profile when fields for profile is not present' do
    klass = UserBridge.new([@user], user: @user)
    attributes = klass.attributes[0]
    assert_equal @user.profile.id, attributes[:profile][:id]
    assert_equal 'Asia/Kolkata', attributes[:profile][:timeZone]
    assert attributes[:profile][:expertTopics]
    assert attributes[:profile][:learningTopics]
    assert_equal 'en', attributes[:profile][:language]
  end

  test 'should returns on demand field :dashboardInfo' do
    dashboard_info = [{'name': 'Learning Hours',
                        'visible': 'true'
                        },
                        {'name': 'Peer Learning',
                        'visible': 'true'
                        }]

    @user.profile.dashboard_info = dashboard_info
    @user.save
    @user.reload
    klass = UserBridge.new([@user], user: @user, fields: 'dashboard_info')
    attributes = klass.attributes[0]
    assert_equal @user.profile.dashboard_info, attributes[:dashboardInfo]
  end

  test 'prevents sending information that is not supported' do
    klass = UserBridge.new([@user], user: @user, fields: 'id,first_name,invalid_name')
    attributes = klass.attributes[0]
    assert_equal @user.id, attributes[:id]
    assert_equal @user.first_name, attributes[:firstName]
    assert_not_includes attributes.keys, :invalid_name
  end

  test 'it should send company' do
    @profile.update_attributes(company: "Edcast", job_role: "CTO")
    klass = UserBridge.new([@user], user: @user, fields: 'company')
    attributes = klass.attributes[0]
    assert_equal @profile.company, attributes[:company]
  end
end

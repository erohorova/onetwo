require 'test_helper'

class OrganizationBridgeTest < ActiveSupport::TestCase
  setup do
    @o = create :organization
  end

  test 'returns minimal data for an organization' do
    response = OrganizationBridge.new([@o]).attributes[0]

    assert_same_elements [:id, :hostName, :name, :coBrandingLogo, :imageUrl], response.keys
    assert_equal @o.id, response[:id]
    assert_equal @o.host_name, response[:hostName]
    assert_equal @o.name, response[:name]
    assert_nil response[:coBrandingLogo]
    assert_match /http:\/\/cdn-test-host.com\/assets\/default_org_image_original.jpg/, response[:imageUrl]
  end

  test 'prevents sending information that is not supported' do
    response = OrganizationBridge.new([@o], fields: 'id,host_name,invalid_host').attributes[0]

    assert_equal @o.id, response[:id]
    assert_equal @o.host_name, response[:hostName]
    assert_not_includes response.keys, :invalid_host
  end
end

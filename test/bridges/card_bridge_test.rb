require 'test_helper'

class CardBridgeTest < ActiveSupport::TestCase
  setup do
    @o = create :organization
    @u = create :user, organization: @o
    @filestack = [{
      'elements' => {
        'filename' => 'Screen Shot 2018-06-05 at 11.51.06 AM.png',
        'mimetype' => 'image/png',
        'size' => '1946518',
        'source' => 'local_file_system',
        'url' => 'https://cdn.filestackcontent.com/d2sjlUgWT1WRJwldER6P',
        'handle' => 'd2sjlUgWT1WRJwldER6P',
        'status' => 'Stored',
      }
    }]
    Organization.any_instance.stubs(:reporting_content_enabled?).returns(true)
  end

  test 'returns minimal data of an authored card' do
    # add resource
    resource = create(:resource)
    # ARTICLE: create card with resource
    provider_image = "https://cdn.filestackcontent.com/KxaMgfERPugXDbAySGTi"
    Filestack::Security.any_instance.stubs(:signed_url).returns(provider_image)
    media_card = create(:card, author_id: @u.id, card_type: 'media', card_subtype: 'link',
      resource: resource, readable_card_type_name: 'book', provider_image: provider_image,
      provider: 'Example', votes_count: 4, comments_count: 10, organization: @o)
    create :assignment, assignable: media_card, assignee: @u
    # add more metadata to the card
    create :card_metadatum, card: media_card, plan: 'free', level: 'beginner', average_rating: 5
    # add price
    price = create :price, card: media_card, currency: '$', amount: 100, ecl_id: media_card.ecl_id
    # bookmark this card
    create :bookmark, bookmarkable: media_card, user: @u
    # report this card
    Organization.any_instance.stubs(:reporting_content_enabled?).returns(true)
    create :card_reporting, card: media_card, user: @u
    # self-assign this card
    response = CardBridge.new([media_card], user: @u).attributes
    card = response[0]

    assert_same_elements [:id, :title, :message, :cardType,
      :cardSubtype, :slug, :state, :isOfficial, :provider, :providerImage,
      :readableCardType, :shareUrl, :averageRating, :skillLevel,
      :filestack, :votesCount, :commentsCount, :publishedAt,
      :prices, :isAssigned, :isBookmarked, :isReported, :author, :resource,
      :hidden, :isPublic, :isPaid, :cardMetadatum,
      :markFeatureDisabledForSource, :completionState, :allRatings,
      :isUpvoted, :isClone, :createdAt], card.keys

    assert_equal media_card.id, card[:id].to_i
    assert_nil card[:title]
    assert_match /#{media_card.message}/, card[:message]
    assert_equal 'media', card[:cardType]
    assert_equal 'link', card[:cardSubtype]
    assert_equal 'published', card[:state]
    assert_equal false, card[:isOfficial]
    assert_equal 'Example', card[:provider]
    assert_match /https:\/\/cdn.filestackcontent.com\/KxaMgfERPugXDbAySGTi/, card[:providerImage]
    assert_equal 'Book', card[:readableCardType]
    assert_match /http:\/\/#{@o.host_name}.test.host\/insights\/#{media_card.id}/, card[:shareUrl]
    assert_equal 5, card[:averageRating]
    assert_equal 'beginner', card[:skillLevel]
    assert_empty card[:filestack]
    assert_equal 4, card[:votesCount]
    assert_equal 10, card[:commentsCount]
    assert card[:publishedAt]
    assert_equal [{ id: price.id, amount: price.amount, currency: '$', symbol: '$'}], card[:prices]
    assert card[:isAssigned]
    assert card[:isBookmarked]
    assert card[:isReported]
    assert_equal @u.handle, card[:author][:handle]
    assert_same_elements ['tiny', 'small', 'medium', 'large'], card[:author][:avatarimages].keys
    assert_match /#{@u.full_name}/, card[:author][:fullName]
    refute card[:markFeatureDisabledForSource]
  end

  test 'returns minimal data for ECL card' do
    # build a resource
    resource = build(:resource)
    # build a card
    ecl_card = build(:card, card_type: 'media', card_subtype: 'text', ecl_id: SecureRandom.hex,
      organization: @o, readable_card_type_name: 'book')
    # assign virtual resource
    ecl_card.resource = resource
    # build a price
    price = build :price, card: ecl_card, currency: '$', amount: 100, ecl_id: ecl_card.ecl_id
    ecl_card.prices_attributes = price.attributes

    response = CardBridge.new([ecl_card], user: @u).attributes
    card = response[0]

    assert_same_elements [:id, :title, :message, :cardType,
      :cardSubtype, :slug, :state, :isOfficial, :provider, :providerImage,
      :readableCardType, :shareUrl, :averageRating, :skillLevel,
      :filestack, :votesCount, :commentsCount, :publishedAt,
      :prices, :isAssigned, :isBookmarked, :isReported, :resource,
      :isPublic, :hidden, :isPaid, :markFeatureDisabledForSource,
      :completionState, :allRatings, :isUpvoted, :isClone, :createdAt], card.keys

    assert_equal 'ECL-' + ecl_card.ecl_id, card[:id]
    assert_nil card[:title]
    assert_match /#{ecl_card.message}/, card[:message]
    assert_equal 'media', card[:cardType]
    assert_equal 'text', card[:cardSubtype]
    assert_equal 'published', card[:state]
    assert_equal false, card[:isOfficial]
    assert_nil card[:provider]
    assert_nil card[:providerImage]
    assert_equal 'Book', card[:readableCardType]
    assert_match /http:\/\/#{@o.host_name}.test.host\/insights\/ECL-#{ecl_card.ecl_id}/, card[:shareUrl]
    assert_nil card[:averageRating]
    assert_nil card[:skillLevel]
    assert_empty card[:filestack]
    assert_equal 0, card[:votesCount]
    assert_equal 0, card[:commentsCount]
    assert card[:publishedAt]
    assert_equal [{ id: nil, amount: price.amount, currency: '$', symbol: '$'}], card[:prices]
    refute card[:isAssigned]
    refute card[:isBookmarked]
    refute card[:isReported]
    assert_nil card[:author]
    refute card[:markFeatureDisabledForSource]
    assert_match /http:\/\/www.google.com/, card[:resource][:imageUrl]
  end

  test 'returns minimal data for a pathway' do
    # PATHWAY: create card with resource
    pathway = create :card, author_id: @u.id, card_type: 'pack', card_subtype: 'simple', organization: @o
    # add plan, level and average rating to the pathway
    card_metadatum = create :card_metadatum, card: pathway, plan: 'free', level: 'beginner', average_rating: 5

    # add cards into the pathway
    # 1. Article
    resource_1 = create :resource
    card_1 = create :card, author: @u, organization_id: @o.id, card_type: 'media', card_subtype: 'link', resource: resource_1
    CardPackRelation.add(cover_id: pathway.id, add_id: card_1.id)

    # 2. Article
    resource_2 = create :resource
    card_2 = create :card, author: @u, organization_id: @o.id, card_type: 'media', card_subtype: 'link', resource: resource_2
    CardPackRelation.add(cover_id: pathway.id, add_id: card_2.id)

    # 3. Text
    card_3 = create :card, author: @u, organization_id: @o.id, card_type: 'media', card_subtype: 'text'
    CardPackRelation.add(cover_id: pathway.id, add_id: card_3.id)

    # bookmark this card
    create :bookmark, bookmarkable: pathway, user: @u

    # self-assign this pathway
    create :assignment, assignable: pathway, assignee: @u

    # add a badge to this pathway
    card_badge = create :card_badge, organization: @o
    card_badging = pathway.create_card_badging(badge_id: card_badge.id, title: "I am badge!!!")
    badge = Badge.find_by(id: card_badging.badge_id)

    response = CardBridge.new([pathway], user: @u).attributes
    card = response[0]

    keys = %i[
      id            title         message          cardType
      cardSubtype   slug          state            isOfficial
      provider      providerImage readableCardType shareUrl
      averageRating skillLevel    filestack        votesCount
      commentsCount publishedAt   prices           isAssigned
      isBookmarked  isReported    author           hidden
      isPublic      isPaid        cardMetadatum    completedPercentage
      allRatings    isUpvoted     completionState  badging
      isClone       createdAt     markFeatureDisabledForSource
    ]
    assert_same_elements keys, card.keys

    assert_equal pathway.id, card[:id].to_i
    assert_nil card[:title]
    assert_match /#{pathway.message}/, card[:message]
    assert_equal 'pack', card[:cardType]
    assert_equal 'simple', card[:cardSubtype]
    assert_equal 'draft', card[:state]
    refute card[:isOfficial]
    assert_nil card[:provider]
    assert_nil card[:providerImage]
    assert_nil card[:readableCardType]
    assert_match /http:\/\/#{@o.host_name}.test.host\/insights\/#{pathway.id}/, card[:shareUrl]
    assert_equal 5, card[:averageRating]
    assert_equal 'beginner', card[:skillLevel]
    assert_not_empty card[:filestack]
    assert_equal 0, card[:votesCount]
    assert_equal 0, card[:commentsCount]
    assert card[:publishedAt]
    assert_empty card[:prices]
    assert card[:isAssigned]
    assert card[:isBookmarked]
    refute card[:isReported]
    assert_equal @u.handle, card[:author][:handle]
    assert_same_elements ['tiny', 'small', 'medium', 'large'], card[:author][:avatarimages].keys
    assert_match /#{@u.full_name}/, card[:author][:fullName]
    refute card[:markFeatureDisabledForSource]
    assert_equal card_badging.id, card[:badging][:id]
    assert_equal card_badging.title, card[:badging][:title]
    assert_equal card_badging.badge_id, card[:badging][:badgeId]
    assert card_badge.image.url(:large), card[:badging][:imageUrl]
  end

  test 'returns minimal data for a project card' do
    card = create :card, author: @u, message: 'new channel card', organization: @o

    channel = create(:channel, organization: @o, only_authors_can_post: false)
    create :channels_card, card: card, channel: channel

    # cards = [card]
    cards = [card].each{|card| card.channel_card_rank = 1}
    cards << create(:card, author: @u, message: 'second channel card')

    project_card = create(:card, author: @u, message: 'second channel card', card_type: 'project', card_subtype: 'text')
    project = Project.create(card: project_card)
    cards << project_card

    response = CardBridge.new(cards, user: @u).attributes

    assert_equal response.first[:rank], 1
    assert_equal response[2][:project_id], project.id
    refute response[1][:rank]
    refute response[1][:project_id]
    assert_not_includes response[1].keys, 'rank'
  end

  test 'returns minimal data for a poll card' do
    Card.any_instance.stubs(:update_file_resources_from_filestack).returns(true)
    # create an assessment
    poll_card = create :card, card_type: 'poll', filestack: @filestack, author: @u, organization: @o
    # add few options and set one of them as an answer
    option1 = create :quiz_question_option, quiz_question: poll_card, label: 'yes', is_correct: true
    option2 = create :quiz_question_option, quiz_question: poll_card, label: 'no'
    # capture the author's attempt
    attempt = create :quiz_question_attempt, user: @u, quiz_question: poll_card, selections: [option1.id]
    # capture the stats of attempts
    create :quiz_question_stats, quiz_question: poll_card, attempt_count: 1

    response = CardBridge.new([poll_card], user: @u).attributes
    card = response[0]

    keys = %i[
      id             title          message           cardType
      cardSubtype    slug           state             isOfficial
      provider       providerImage  readableCardType  shareUrl
      averageRating  skillLevel     filestack         votesCount
      commentsCount  publishedAt    prices            isAssigned
      isBookmarked   isReported     author            quizQuestionOptions
      isAssessment   hasAttempted   attemptedOption   attemptCount
      hidden         isPublic       isPaid            completionState
      allRatings     isUpvoted      canBeReanswered   markFeatureDisabledForSource
      isClone        createdAt
    ]
    assert_same_elements keys, card.keys

    assert_equal poll_card.id, card[:id].to_i
    assert_nil card[:title]
    assert_match /#{poll_card.message}/, card[:message]
    assert_equal 'poll', card[:cardType]
    assert_equal 'published', card[:state]
    assert_equal false, card[:isOfficial]
    assert_nil card[:provider]
    assert_nil card[:providerImage]
    assert_nil card[:readableCardType]
    assert_match /http:\/\/#{@o.host_name}.test.host\/insights\/#{poll_card.id}/, card[:shareUrl]
    assert_nil card[:averageRating]
    assert_nil card[:skillLevel]
    assert_equal @filestack, card[:filestack]
    assert_equal 0, card[:votesCount]
    assert_equal 0, card[:commentsCount]
    assert card[:publishedAt]
    assert_empty card[:prices]
    refute card[:isAssigned]
    refute card[:isBookmarked]
    refute card[:isReported]
    assert_equal @u.handle, card[:author][:handle]
    assert_same_elements ['tiny', 'small', 'medium', 'large'], card[:author][:avatarimages].keys
    assert_match /#{@u.full_name}/, card[:author][:fullName]
    assert_nil card[:resource]
    assert card[:isAssessment]
    assert card[:hasAttempted]
    assert_equal option1.id, card[:attemptedOption][:id]
    assert_equal option1.is_correct, card[:attemptedOption][:is_correct]
    assert_equal 1, card[:attemptCount]
    refute card[:markFeatureDisabledForSource]
  end

  test 'does not sanitize card message for text card' do
    text_card =  create(:card, message: '<span style="display:block;;background:red">Do check this out</span>', card_subtype: 'text')

    response = CardBridge.new([text_card], user: @u).attributes
    card = response[0]

    assert_equal '<span style="display:block;;background:red">Do check this out</span>', card[:message]
  end

  test 'supports on-demand ask: `due_at`, `subscription_end_date`, `assigner`, and `paid_by_user`' do
    Organization.any_instance.stubs(:reporting_content_enabled?).returns(true)
    card = create(:card, author: @u)
    create :card_reporting, card: card, user: @u
    assignment = create :assignment, assignable: card, assignee: @u, due_at: Time.now
    create(
      :card_subscription,
      card_id: card.id,
      user_id: @u.id,
      end_date: Time.now + 1.days
    )
    create(
      :team_assignment,
      assignment: assignment,
      assignor: @u,
      team_id: nil
    )
    keys = %i(dueAt subscriptionEndDate assigner paidByUser)
    response = CardBridge.new(
      [card], user: @u,
      fields: 'due_at,subscription_end_date,assigner,paid_by_user'
    ).attributes[0]

    assert_same_elements keys, response.keys
  end

  test 'supports on-demand ask: `is_assigned`' do
    card = create(:card, author: @u)
    create :card_reporting, card: card, user: @u
    assignment = create :assignment, assignable: card, assignee: @u, due_at: Time.now
    create(
      :team_assignment,
      assignment: assignment,
      assignor: @u,
      team_id: nil
    )
    response = CardBridge.new(
      [card], user: @u,
      fields: 'is_assigned'
    ).attributes[0]
    assert_same_elements [:isAssigned], response.keys
    assert_equal true, response[:isAssigned]
  end

  test 'supports on-demand ask: `is_bookmarked`' do
    card = create(:card, author: @u)
    create :card_reporting, card: card, user: @u
    create :bookmark, bookmarkable: card, user: @u
    response = CardBridge.new(
      [card], user: @u,
      fields: 'is_bookmarked'
    ).attributes[0]
    assert_same_elements [:isBookmarked], response.keys
    assert_equal true, response[:isBookmarked]
  end
    
  test 'supports on-demand ask: `is_new` for pathways, journeys and cards if last_access_at provided' do
    Organization.any_instance.stubs(:reporting_content_enabled?).returns(true)
    card = create(:card, author: @u)

    #If new card was added to Pathway/Journey after last access to it, it should be with NEW label in response
    pathway = create :card, author_id: @u.id, card_type: 'pack', card_subtype: 'simple', organization: @o
    Timecop.freeze(2.weeks.ago) do
      card_1 = create :card, author: @u, organization_id: @o.id, card_type: 'media', card_subtype: 'link'
      card_2 = create :card, author: @u, organization_id: @o.id, card_type: 'media', card_subtype: 'link'
      CardPackRelation.add(cover_id: pathway.id, add_id: card_1.id)
      CardPackRelation.add(cover_id: pathway.id, add_id: card_2.id)
    end
    pathway.publish!

    journey = create :card, organization: @o, card_type: 'journey', card_subtype: 'self_paced', author: @u
    pack1 = create :card, organization: @o, card_type: 'pack', card_subtype: 'simple', author: @u
    pack2 = create :card, organization: @o, card_type: 'pack', card_subtype: 'simple', author: @u
    JourneyPackRelation.add(cover_id: journey.id, add_id: pack1.id)
    JourneyPackRelation.add(cover_id: journey.id, add_id: pack2.id)
    journey.publish!

    card_response = CardBridge.new(
      [card], user: @u,
      fields: 'is_new',
      options: { last_access_at: (Time.now - 1.week).to_i}
    ).attributes[0]
    assert_includes card_response.keys, :isNew
    assert_equal card_response[:isNew], true

    pathway_response = CardBridge.new(
      [pathway], user: @u,
      fields: 'is_new',
      options: { last_access_at: (Time.now - 1.week).to_i}
    ).attributes[0]
    assert_includes pathway_response.keys, :isNew
    assert_equal pathway_response[:isNew], false

    journey_response = CardBridge.new(
      [journey], user: @u,
      fields: 'is_new',
      options: { last_access_at: (Time.now - 1.week).to_i}
    ).attributes[0]
    assert_includes journey_response.keys, :isNew
    assert_equal journey_response[:isNew], true
  end

  test 'is_new for journeys should be true if card was added to any section' do
    Organization.any_instance.stubs(:reporting_content_enabled?).returns(true)

    #If new card was added to Journey after last access to it, it should be with NEW label in response
    journey = create :card, organization: @o, card_type: 'journey', card_subtype: 'self_paced', author: @u
    Timecop.freeze(2.weeks.ago) do
      pack1 = create :card, organization: @o, card_type: 'pack', card_subtype: 'simple', author: @u
      pack2 = create :card, organization: @o, card_type: 'pack', card_subtype: 'simple', author: @u
      JourneyPackRelation.add(cover_id: journey.id, add_id: pack1.id)
      JourneyPackRelation.add(cover_id: journey.id, add_id: pack2.id)
    end
    journey.publish!

    journey_response = CardBridge.new(
      [journey], user: @u,
      fields: 'is_new',
      options: { last_access_at: (Time.now - 1.week).to_i}
    ).attributes[0]
    assert_equal journey_response[:isNew], false

    card = create(:card, author: @u)
    journey.journey_pathways[0].add_card_to_pathway(card.id, "Card")

    journey_response = CardBridge.new(
      [journey], user: @u,
      fields: 'is_new',
      options: { last_access_at: (Time.now - 1.week).to_i}
    ).attributes[0]
    assert_equal journey_response[:isNew], true
  end

  test 'supports on-demand ask: `shared_by`' do
    org = create(:organization)
    user = create(:user, organization: org)
    shared_by = create(:user, organization: org)
    card = create(:card, organization: org, author: shared_by)
    card_user_share = create(:card_user_share, card_id: card.id, user: user, shared_by_id: shared_by.id)

    keys = 'shared_by'
    response = CardBridge.new(
      [card], user: user,
      fields: 'shared_by'
    ).attributes[0]

    assert_same_elements [:sharedBy], response.keys
    assert_equal response[:sharedBy][:handle], shared_by.handle
  end

  test 'returns on-demand data for a poll card' do
    Card.any_instance.stubs(:update_file_resources_from_filestack).returns(true)
    # create an assessment
    poll_card = create :card, card_type: 'poll', filestack: @filestack, author: @u, organization: @o
    # add few options and set one of them as an answer
    option1 = create :quiz_question_option, quiz_question: poll_card, label: 'yes', is_correct: true
    option2 = create :quiz_question_option, quiz_question: poll_card, label: 'no'
    # capture the author's attempt
    attempt = create :quiz_question_attempt, user: @u, quiz_question: poll_card, selections: [option1.id]
    # capture the stats of attempts
    create :quiz_question_stats, quiz_question: poll_card, attempt_count: 1

    response = CardBridge.new([poll_card], user: @u, fields: 'id,quiz').attributes
    card = response[0]
    assert card[:isAssessment]
    assert card[:hasAttempted]
    assert card[:quizQuestionOptions]
    assert_equal option1.id, card[:attemptedOption][:id]
    assert_equal option1.is_correct, card[:attemptedOption][:is_correct]
    assert_equal 1, card[:attemptCount]
  end

  test 'returns minimal data for a video stream card' do
    Card.any_instance.stubs(:update_file_resources_from_filestack).returns(true)
    stub_request(:get, %r{https://api.bambuser.com/group/media.json.*}).
        to_return(status: 200,
          body: %q{{"result":{"results":[{"gid":"32820","created":"1463090266","order_id":"3104754","type":"broadcast","id":"f6f7a6c8-8c26-445b-a931-d473c2b510e1","payload":{"author":"","country":"","created":1463090266,"device_class":"phone","device_name":"LGLS770","height":540,"id":"f6f7a6c8-8c26-445b-a931-d473c2b510e1","length":97,"owner":{"name":"a361715c-4d80-d92a-a9e3-4c81e04882e3","avatar":{"small":{"filename":"https:\/\/static.bambuser.com\/r\/img\/avatars\/default_45.gif","size":45},"large":{"filename":"https:\/\/static.bambuser.com\/r\/img\/avatars\/default_140.gif","size":140}}},"preview":"https:\/\/cf-us-west-2-archive-bambuser.birdplaneapp.com\/archive\/us-west-2-m00\/a_0001\/f6f7a6c8-8c26-445b-a931-34c012b510e1.jpg?2","tags":[],"talkback_types":"any","title":"Checkout my live stream!","type":"archived","url":"https:\/\/cf-us-west-2-archive-bambuser.birdplaneapp.com\/archive\/us-west-2-m00\/a_0001\/f6f7a6c8-8c26-445b-a931-d473c2b510e1.flv","username":"a361715c-4d80-d92a-a9e3-4c81e04882e3","vid":"6256822","views_live":2,"views_total":2,"visibility":"private","width":960}},{"gid":"32820","created":"1463066334","order_id":"3103906","type":"broadcast","id":"7e477893-0260-4173-bce0-f1d50c4d690c","payload":{"author":"","country":"India","created":1463066334,"device_class":"phone","device_name":"Samsung Galaxy S III Neo","height":960,"id":"7e477893-0260-4173-bce0-f1d50c4d690c","lat":20.593683,"length":3,"lon":78.96287,"owner":{"name":"a361715c-4d80-d92a-a9e3-4c81e04882e3","avatar":{"small":{"filename":"https:\/\/static.bambuser.com\/r\/img\/avatars\/default_45.gif","size":45},"large":{"filename":"https:\/\/static.bambuser.com\/r\/img\/avatars\/default_140.gif","size":140}}},"position_accuracy":2054220,"position_type":"country","preview":"https:\/\/archive.bambuser.com\/m\/06\/a_0008\/7e477893-0260-4173-bce0-0a53554d690c.jpg?0","tags":[],"talkback_types":"any","title":"Checkout my live stream!","type":"archived","url":"https:\/\/archive.bambuser.com\/m\/06\/a_0008\/7e477893-0260-4173-bce0-f1d50c4d690c.flv","username":"a361715c-4d80-d92a-a9e3-4c81e04882e3","vid":"6256369","views_live":1,"views_total":2,"visibility":"private","width":540}}],"next":"eyJicm9hZGNhc3Rfb3JkZXJfaWRfYWZ0ZXIiOiIxNDYzMDY2MzM0XzMxMDM5MDYiLCJpbWFnZV9jcmVhdGVkX2FmdGVyIjoiMTQ2MzA2NjMzNCJ9"}}},
          headers: {})

    # create a video stream card
    video_stream_card = create :card, card_type: 'video_stream', card_subtype: 'simple', author: @u, organization: @o

    # add a file resource to the card
    file_resource = create :file_resource
    video_stream_card.file_resources << file_resource

    # create a video stream
    video_stream = create :iris_video_stream, :live, card_id: video_stream_card.id, playback_url: 'https://cdn.bambuser.net/broadcasts/3aed1e62-3524-4631-a2d1-be616d29d32f?da_signature_method=HMAC-SHA256&da_id=9e1b1e83-657d-7c83-b8e7-0b782ac9543a&da_timestamp=1536338533&da_static=1&da_ttl=0&da_signature=fe0fa4f553c1cff1513dcf5eebb1f0e33270962d633b830747cc85d7f33a44c6'

    video_stream_card.reload

    response = CardBridge.new([video_stream_card], user: @u).attributes
    card = response[0]

    assert_same_elements [:id, :title, :message, :cardType,
      :cardSubtype, :slug, :state, :isOfficial, :provider, :providerImage,
      :readableCardType, :shareUrl, :averageRating, :skillLevel,
      :filestack, :votesCount, :commentsCount, :publishedAt,
      :prices, :isAssigned, :isBookmarked, :isReported, :videoStream, :author,
      :isPublic, :hidden, :isPaid, :markFeatureDisabledForSource,
      :completionState, :allRatings, :isUpvoted, :isClone, :createdAt], card.keys

    assert_equal video_stream_card.id, card[:id].to_i
    assert_nil card[:title]
    assert_match /#{video_stream_card.message}/, card[:message]
    assert_equal 'video_stream', card[:cardType]
    assert_equal 'published', card[:state]
    assert_equal false, card[:isOfficial]
    assert_nil card[:provider]
    assert_nil card[:providerImage]
    assert_nil card[:readableCardType]
    assert_match /http:\/\/#{@o.host_name}.test.host\/insights\/#{video_stream_card.id}/, card[:shareUrl]
    assert_nil card[:averageRating]
    assert_nil card[:skillLevel]
    assert_empty card[:filestack]
    assert_equal 0, card[:votesCount]
    assert_equal 0, card[:commentsCount]
    assert card[:publishedAt]
    assert_empty card[:prices]
    refute card[:isAssigned]
    refute card[:isBookmarked]
    refute card[:isReported]
    assert_equal @u.handle, card[:author][:handle]
    assert_same_elements ['tiny', 'small', 'medium', 'large'], card[:author][:avatarimages].keys
    assert_match /#{@u.full_name}/, card[:author][:fullName]
    assert card[:videoStream]
    assert card[:videoStream][:imageUrl]
    assert_equal 'live', card[:videoStream][:status]
    refute card[:markFeatureDisabledForSource]

    # assert video_stream response keys
    expected_video_stream_response = %i(id uuid status startTime width height imageUrl playbackUrl embedPlaybackUrl)
    assert_same_elements expected_video_stream_response, card[:videoStream].keys
  end

  test 'supports on-demand ask: Count of cards in a pathway' do
    cover = create :card, organization: @o, card_type: 'pack', card_subtype: 'simple', author: @u
    card1 = create :card, organization: @o, card_type: 'media', card_subtype: 'text', author: @u
    card2 = create :card, organization: @o, card_type: 'media', card_subtype: 'text', author: @u

    CardPackRelation.add(cover_id: cover.id, add_id: card1.id)
    CardPackRelation.add(cover_id: cover.id, add_id: card2.id)

    response = CardBridge.new([cover], user: @u, fields: 'pack_cards_count').attributes[0]

    assert_equal 2, response[:packCardsCount]
  end

  test 'supports on-demand ask: Count of pathway in a journey' do
    cover = create :card, organization: @o, card_type: 'journey', card_subtype: 'self_paced', author: @u
    pack1 = create :card, organization: @o, card_type: 'pack', card_subtype: 'simple', author: @u
    pack2 = create :card, organization: @o, card_type: 'pack', card_subtype: 'simple', author: @u
    JourneyPackRelation.add(cover_id: cover.id, add_id: pack1.id)
    JourneyPackRelation.add(cover_id: cover.id, add_id: pack2.id)

    card1 = create :card, organization: @o, card_type: 'media', card_subtype: 'text', author: @u
    card2 = create :card, organization: @o, card_type: 'media', card_subtype: 'text', author: @u
    CardPackRelation.add(cover_id: pack1.id, add_id: card1.id)
    CardPackRelation.add(cover_id: pack2.id, add_id: card2.id)

    response = CardBridge.new([cover], user: @u, fields: 'journey_packs_count').attributes[0]

    assert_equal 2, response[:journeyPacksCount]
  end

  test 'supports on-demand ask: card filestack in pathway' do
    pathway = create :card, organization: @o, card_type: 'pack', card_subtype: 'simple', author: @u, filestack: @filestack
    response = CardBridge.new([pathway], user: @u, fields: 'filestack').attributes[0]

    assert_equal @filestack, response[:filestack]
  end

  test 'considers `markFeatureDisabledForSource` for a scorm card with `scorm_course` in the filestack' do
    Organization.any_instance.stubs(:auto_mark_as_complete_enabled?).returns(true)

    # add resource.
    resource = create(:resource)
    # add SCORM card, published, scorm_course = TRUE.
    scorm_card = create(:card, author: @u, organization: @o, state:   'published',
      resource_id: resource.id, filestack: [scorm_course: true, url: 'https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv'])
    # create audit for a scorm card.
    scorm_detail = create(:scorm_detail, status: 'finished', token: 'randomtoken000', card: scorm_card)

    scorm_card.reload

    response = CardBridge.new([scorm_card], user: @u).attributes
    card = response[0]

    assert card[:markFeatureDisabledForSource]
  end

  test 'considers `markFeatureDisabledForSource` for a scorm card with SCORM url in the resource' do
    stub_request(:get, 'https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv')

    Organization.any_instance.stubs(:auto_mark_as_complete_enabled?).returns(true)

    # add resource.
    resource = create(:resource, url: 'https://qa.edcast.com/api/scorm/launch?course_id=SequencingSimpleRemediation_SCORM20043rdEdition18cd465d-7c3c-4647-a3aa-50e5833e5161')
    # add SCORM card, published.
    scorm_card = create(:card, author: @u, organization: @o, state:   'published',
      resource_id: resource.id, filestack: [url: 'https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv'])
    # create audit for a scorm card.
    scorm_detail = create(:scorm_detail, status: 'finished', token: 'randomtoken000', card: scorm_card)

    scorm_card.reload

    response = CardBridge.new([scorm_card], user: @u).attributes
    card = response[0]

    assert card[:markFeatureDisabledForSource]
  end

  test 'considers `markFeatureDisabledForSource` for an ECL source' do
    stub_request(:get, 'https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv')

    # add resource.
    resource = create(:resource)
    # add an article card, published.
    article_card = create(:card, author: @u, organization: @o, state:   'published',
      resource_id: resource.id, filestack: [url: 'https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv'],
      ecl_metadata: { 'mark_feature_disabled' => 'true' })

    article_card.reload

    response = CardBridge.new([article_card], user: @u).attributes
    card = response[0]

    assert card[:markFeatureDisabledForSource]
  end

  test 'considers resource title and description as default for article cards' do
    stub_request(:get, 'https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv')

    # add resource.
    resource = create(:resource)
    # add an article card, published.
    article_card = create(:card, author: @u, organization: @o, state:   'published',
      resource_id: resource.id, filestack: [url: 'https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv'])
    article_card.reload

    response = CardBridge.new([article_card], user: @u).attributes
    card = response[0]

    assert_includes  card[:resource].keys, :title
    assert_includes  card[:resource].keys, :description
    assert_equal resource.title, card[:resource][:title]
    assert_equal resource.description, card[:resource][:description]
  end

  test 'considers is_clone key in response if shared_channel-feature is enabled' do
    create(:config, configable: @o, name: 'feature/shared_channels', value: true)

    card = create(:card, author: @u, organization: @o, state:   'published', card_type: 'media', card_subtype: 'text')

    card.reload
    response = CardBridge.new([card], user: @u).attributes
    card = response[0]

    assert_includes card.keys, :isClone
    refute card[:isClone]
  end

  test 'do not consider `markFeatureDisabledForSource` for text cards' do
    Organization.any_instance.stubs(:auto_mark_as_complete_enabled?).returns(true)

    # add an text card, published.
    text_card = create(:card, author: @u, organization: @o, state:   'published', card_type: 'media', card_subtype: 'text')

    text_card.reload

    response = CardBridge.new([text_card], user: @u).attributes
    card = response[0]

    refute card[:markFeatureDisabledForSource]
  end

  test 'supports on-demand ask: card skill_level' do
    card = create :card, organization: @o, card_type: 'media', card_subtype: 'text', author: @u, filestack: @filestack
    card_metadatum = create :card_metadatum, card: card, plan: 'free', level: 'beginner', average_rating: 5
    response = CardBridge.new([card], user: @u, fields: 'skill_level').attributes[0]

    assert_equal card_metadatum.level, response[:skillLevel]
  end

  test 'supports on-demand ask: card badging' do
    card = create :card, organization: @o, card_type: 'media', card_subtype: 'text', author: @u, filestack: @filestack
    card_badge = create(:card_badge, organization: @o)
    card_badging = card.create_card_badging(badge_id: card_badge.id, title: "dummy title")
    response = CardBridge.new([card], user: @u, fields: 'badging').attributes[0]
    assert_equal card_badging.id, response[:badging][:id]
    assert_equal card_badge.id, response[:badging][:badgeId]
    assert_equal card_badging.title, response[:badging][:title]
  end

  test 'supports on-demand ask: card channels and channel_ids' do
    card = create :card, organization: @o, author: @u

    # create two channels
    channel = create :channel, organization: @o
    channel2 = create :channel, organization: @o

    # post this card to both the channels
    card.post_to_channel(channel.id)
    card.post_to_channel(channel2.id)

    response = CardBridge.new([card], user: @u, fields: 'id,channel_ids,channels(id,label,is_private),channels').attributes[0]
    curated_channels = response[:channels]

    assert_equal 2, curated_channels.length
    assert_same_elements [channel.id, channel2.id], curated_channels.map { |channel| channel[:id] }
    assert_same_elements [channel.label, channel2.label], curated_channels.map { |channel| channel[:label] }
    assert_same_elements [channel.id, channel2.id], response[:channelIds]
    assert_same_elements [:id, :label, :isPrivate], curated_channels[0].keys
  end

  test 'supports on-demand ask: card tags' do
    card = create :card, organization: @o, author: @u
    card.topics = ["Web", "Ruby", "Java"]
    card.update_topics
    tags = Tag.all
    response = CardBridge.new([card], user: @u, fields: 'id,tags(id,name),tags').attributes[0]

    assert_equal 3, response[:tags].length
    assert_same_elements tags.ids, response[:tags].map{|e| e[:id]}
    assert_same_elements tags.pluck(:name), response[:tags].map{|e| e[:name]}
  end

  test 'consider `completionState` in default fields for completed cards' do
    card1, card2 = create_list :card, 2, author: @u, organization: @o
    create :user_content_completion, :completed, card: card1, user: @u
    create :user_content_completion, :started, card: card2, user: @u

    response = CardBridge.new([card1, card2], user: @u).attributes

    assert_equal 'COMPLETED', response[0][:completionState]
    assert_not_equal 'COMPLETED', response[1][:completionState]
  end

  test 'consider `isUpvoted` in default fields for cards that are voted by current_user' do
    card1, card2 = create_list :card, 2, author: @u, organization: @o
    create :vote, votable: card1, voter: @u
    response = CardBridge.new([card1, card2], user: @u).attributes

    assert response[0][:isUpvoted]
    refute response[1][:isUpvoted]
  end

  test 'consider `allRatings` in default fields of a card' do
    card = create :card, author: @u, organization: @o
    card_rating = create :cards_rating, user: @u, card: card, rating: 3

    card2 = create :card, author: @u, organization: @o

    u2 = create :user, organization: @o
    card_rating = create :cards_rating, user: u2, card: card, rating: nil, level: 'beginner'

    u3 = create :user, organization: @o
    card_rating = create :cards_rating, user: u3, card: card, rating: 2, level: nil

    response = CardBridge.new([card], user: @u).attributes
    assert_equal 2, response[0][:allRatings]

    response = CardBridge.new([card2], user: @u).attributes
    assert_equal 0, response[0][:allRatings]
  end

  test 'supports on-demand ask: card author' do
    authored_card = create :card, author: @u, organization: @o
    ecl_card = create :card, card_type: 'media', card_subtype: 'text', ecl_id: SecureRandom.hex, organization: @o, author: nil

    response = CardBridge.new([authored_card, ecl_card], user: @u, fields: 'author').attributes

    assert_match /#{@u.name}/, response[0][:author][:fullName]
    refute response[1][:author]
  end

  test 'completionState should be empty if user_content_completion is not present' do
    card1, card2 = create_list :card, 2, author: @u, organization: @o
    create :user_content_completion, :completed, card: card1, user: @u

    response = CardBridge.new([card1, card2], user: @u).attributes

    assert_equal 'COMPLETED', response[0][:completionState]
    assert_nil response[1][:completionState]
  end

  test 'supports on-demand ask: `paid_by_user` if a card has an active subscription' do
    # a card with an active subscription
    card_1 = create :card, organization: @o, author: @u, is_paid: true
    card_subscription = create :card_subscription, start_date: Time.now, user: @u, card: card_1

    # a card with no active subscription
    card_2 = create :card, organization: @o, author: @u, is_paid: true

    response = CardBridge.new([card_1, card_2], user: @u, fields: 'id,paid_by_user').attributes
    card_1_response = response.select { |r| r[:id] == card_1.id.to_s }[0]
    card_2_response = response.select { |r| r[:id] == card_2.id.to_s }[0]

    assert card_1_response[:paidByUser]
    refute card_2_response[:paidByUser]
  end

  test 'supports on-demand ask: `payment_enabled` as true if auth api information is present for that source' do
    card = create :card, author: @u, ecl_metadata: {source_id: 'test-source-id'}, organization: @o
    create :sources_credential, auth_api_info: {auth_required: true}, source_id: 'test-source-id', organization: @o

    response = CardBridge.new([card], user: @u, fields: 'payment_enabled').attributes
    card_response = response[0]

    assert card_response[:paymentEnabled]
  end

  test 'supports on-demand ask: `teams`' do
    # create two teams
    team = create :team, organization: @o
    team2 = create :team, organization: @o

    # create a card
    card = create :card, organization: @o, author: @u

    # add the card to this team
    create :shared_card, team: team, card: card
    create :shared_card, team: team2, card: card

    response = CardBridge.new([card], user: @u, fields: 'id,teams(id,label,name),teams').attributes
    card_response = response[0]
    teams_response = card_response[:teams]

    assert_equal team.id, teams_response.first[:id]
    assert_equal team.name, teams_response.first[:name]
    assert_equal team.name, teams_response.first[:label]

    assert_equal team2.id, teams_response.second[:id]
    assert_equal team2.name, teams_response.second[:name]
    assert_equal team2.name, teams_response.second[:label]
  end

  test 'returns video_stream recording' do
    card = create :card, author: @u, organization: @o, card_type: 'video_stream', card_subtype: 'simple'
    video_stream = create :iris_video_stream, card: card, status: 'past', creator: @u, organization: @o
    recording = create :recording, hls_location: 'video-streams-output-v2/client/13311/3c2cfba2-d1ee-4ea3-b9df-8631e903fd28/12356/0/hls_playlist.m3u8',
      mp4_location: 'video-streams-output-v2/client/13311/3c2cfba2-d1ee-4ea3-b9df-8631e903fd28/12356/0/mp4_output.mp4', video_stream: video_stream

    response = CardBridge.new([card], user: @u).attributes
    card_response = response[0]

    assert_equal recording.id, card_response[:videoStream][:recording][:id]
    assert_equal "https://#{Settings.aws_cdn_host}/#{recording.hls_location}", card_response[:videoStream][:recording][:hlsLocation]
    assert_equal "https://#{Settings.aws_cdn_host}/#{recording.mp4_location}", card_response[:videoStream][:recording][:mp4Location]
  end

  test 'returns secured past stream recording' do
    Settings.stubs(:serve_authenticated_images).returns('true')
    card = create :card, author: @u, organization: @o, card_type: 'video_stream', card_subtype: 'simple'
    video_stream = create :iris_video_stream, card: card, status: 'past', creator: @u, organization: @o
    recording = create :recording, hls_location: 'video-streams-output-v2/client/13311/3c2cfba2-d1ee-4ea3-b9df-8631e903fd28/12356/0/hls_playlist.m3u8',
      mp4_location: 'video-streams-output-v2/client/13311/3c2cfba2-d1ee-4ea3-b9df-8631e903fd28/12356/0/mp4_output.mp4', video_stream: video_stream

    response = CardBridge.new([card], user: @u).attributes
    card_response = response[0]

    assert_equal recording.id, card_response[:videoStream][:recording][:id]
    assert_match /#{@o.secure_home_page}\/past_stream\//, card_response[:videoStream][:recording][:hlsLocation]
    assert_match /#{@o.secure_home_page}\/past_stream\//, card_response[:videoStream][:recording][:mp4Location]
  end

  test 'resolves association_fields' do
    card1, card2 = create_list :card, 2, author: @u, organization: @o
    create :user_content_completion, :completed, card: card1, user: @u
    channel = create(:channel, organization: @o)

    # create(:channels_card, channel_id: channel.id, card_id: card1.id, state: 'curated')
    channel.cards << [card1]
    response = CardBridge.new([card1, card2], user: @u, fields: 'id,channels,tags,badging,channels(id,label),tags(id,name),badging(id,title)').attributes

    assert_same_elements [:id, :channels, :tags, :badging], response[0].keys
  end

  test 'supports on-demand ask: `non_curated_channel_ids`' do
    # create a card
    card = create :card, organization: @o, author: @u

    # create two channels
    channel = create :channel, organization: @o, curate_only: true, curate_ugc: true
    channel2 = create :channel, organization: @o, curate_only: true, curate_ugc: true

    # add the card to these two channels
    create :channels_card, card: card, channel: channel, state: ChannelsCard::STATE_NEW
    create :channels_card, card: card, channel: channel2, state: ChannelsCard::STATE_NEW

    response = CardBridge.new([card], user: @u, fields: 'non_curated_channel_ids').attributes
    card_response = response[0]

    assert_same_elements [channel.id, channel2.id], card_response[:nonCuratedChannelIds]
  end

  test 'supports on-demand ask: `voters`' do
    # create a card
    card = create :card, organization: @o, author: @u

    # craete a voter
    voter = create :user, organization: @o

    # add voters
    create :vote, votable: card, voter: @u
    create :vote, votable: card, voter: voter

    response = CardBridge.new([card], user: @u, fields: 'voters').attributes
    voters = response[0][:voters]

    assert_equal 2, voters.length
    assert_same_elements [@u.id, voter.id], voters.map { |voter| voter[:id] }
  end

  test 'supports on-demand ask: `leaps`' do
    Card.any_instance.stubs(:update_file_resources_from_filestack).returns(true)

    # create a pathway that will have leaps
    pathway_with_leaps = create :card, author_id: @u.id, card_type: 'pack', card_subtype: 'simple', organization: @o
    # create a pathway that will have no leaps
    pathway_with_no_leaps = create :card, author_id: @u.id, card_type: 'pack', card_subtype: 'simple', organization: @o

    # create an assessment
    poll_card = create :card, card_type: 'poll', filestack: @filestack, author: @u, organization: @o
    poll_card_no_leap = create :card, card_type: 'poll', filestack: @filestack, author: @u, organization: @o

    # add few options and set one of them as an answer
    create :quiz_question_option, quiz_question: poll_card, label: 'yes', is_correct: true
    create :quiz_question_option, quiz_question: poll_card, label: 'no'

    create :quiz_question_option, quiz_question: poll_card_no_leap, label: 'yes'
    create :quiz_question_option, quiz_question: poll_card_no_leap, label: 'no', is_correct: true

    # create text cards
    text_card_1 = create :card, author: @u, organization_id: @o.id, card_type: 'media', card_subtype: 'text'
    text_card_2 = create :card, author: @u, organization_id: @o.id, card_type: 'media', card_subtype: 'text'
    text_card_3 = create :card, author: @u, organization_id: @o.id, card_type: 'media', card_subtype: 'text'

    # add quiz + text cards to `pathway_with_leaps`
    CardPackRelation.add(cover_id: pathway_with_leaps.id, add_id: poll_card.id)
    CardPackRelation.add(cover_id: pathway_with_leaps.id, add_id: text_card_1.id)
    CardPackRelation.add(cover_id: pathway_with_leaps.id, add_id: text_card_2.id)
    CardPackRelation.add(cover_id: pathway_with_leaps.id, add_id: text_card_3.id)

    # add quiz + text cards to `pathway_with_no_leaps`
    CardPackRelation.add(cover_id: pathway_with_no_leaps.id, add_id: poll_card.id)
    CardPackRelation.add(cover_id: pathway_with_no_leaps.id, add_id: text_card_1.id)
    CardPackRelation.add(cover_id: pathway_with_no_leaps.id, add_id: text_card_2.id)
    CardPackRelation.add(cover_id: pathway_with_no_leaps.id, add_id: text_card_3.id)

    # create a leap for a quiz in `pathway_with_no_leaps` for a correct and wrong answer.
    create :leap, pathway: pathway_with_leaps, correct_id: text_card_1.id, wrong_id: text_card_2.id, card_id: poll_card.id
    create :leap, correct_id: text_card_1.id, wrong_id: text_card_2.id, card_id: poll_card.id
    pathway_with_leaps.reload
    poll_card.reload

    cards = [pathway_with_leaps, pathway_with_no_leaps, poll_card, poll_card_no_leap]
    response = CardBridge.new(cards, user: @u, fields: 'id,leaps').attributes
    leap_pathway_response = response.select { |r| r[:id] == pathway_with_leaps.id.to_s }[0]
    non_leap_pathway_response = response.select { |r| r[:id] == pathway_with_no_leaps.id.to_s }[0]
    leap_poll_response = response.select { |r| r[:id].to_i == poll_card.id }[0]
    non_leap_poll_response = response.select { |r| r[:id].to_i == poll_card_no_leap.id }[0]

    pathway_leaps_details = leap_pathway_response[:leaps][:inPathways][0]
    poll_leaps_details = leap_poll_response[:leaps][:standalone]

    assert_equal poll_card.id, pathway_leaps_details[:cardId]
    assert_equal text_card_1.id, pathway_leaps_details[:correctId]
    assert_equal text_card_2.id, pathway_leaps_details[:wrongId]

    assert_equal poll_card.id, poll_leaps_details[:cardId]
    assert_equal text_card_1.id, poll_leaps_details[:correctId]
    assert_equal text_card_2.id, poll_leaps_details[:wrongId]

    assert_empty non_leap_pathway_response[:leaps]
    assert_empty non_leap_poll_response[:leaps]
  end

  test 'supports on-demand ask: `filestack`' do
    2.times do |n|
      cover = create :card, author: @u, card_type: 'pack', organization: @o, filestack: nil
      CardPackRelation.add(cover_id: cover.id, add_id: create(:card, author: @u, organization: @o).id)
      cover.publish!
    end

    cards = Card.where(card_type: 'pack', state: 'published', author_id: @u, organization: @o)
    response = CardBridge.new(cards, user: @u, fields: 'filestack').attributes

    assert response[0][:filestack]
    assert response[0][:filestack][0][:mimetype]
    assert response[0][:filestack][0][:url]
    assert response[0][:filestack][0][:handle]
  end

  test 'supports on-demand ask: channel_card -> rank' do
    card = create :card, author: @u, message: 'new channel card'
    channel = create(:channel, organization: @o, only_authors_can_post: false)
    create :channels_card, card: card, channel: channel

    cards = Card.where(id: card.id, state: 'published', author_id: @u, organization: @o)
    cards = cards.each{|card| card.channel_card_rank = 1}
    cards << create(:card, author: @u, message: 'second channel card')
    response = CardBridge.new(cards, user: @u, fields: '').attributes
    assert_equal response.first[:rank], 1
    refute response[1][:rank]
    assert_not_includes response[1].keys, 'rank'
  end

  test 'supports on-demand ask: `rank,project_id`' do
    card = create :card, author: @u, message: 'new channel card'
    channel = create(:channel, organization: @o, only_authors_can_post: false)
    create :channels_card, card: card, channel: channel
    cards = Card.where(id: card.id, state: 'published', author_id: @u, organization: @o)
    cards = cards.each{|card| card.channel_card_rank = 1}
    cards << create(:card, author: @u, message: 'second channel card')
    project_card = create(:card, author: @u, message: 'second channel card', card_type: 'project', card_subtype: 'text')
    project = Project.create(card: project_card)
    cards << project_card
    response = CardBridge.new(cards, user: @u, fields: 'rank,project_id').attributes
    assert_equal response.first[:rank], 1
    assert_equal response[2][:project_id], project.id
    refute response[1][:rank]
    refute response[1][:project_id]
    assert_not_includes response[1].keys, 'rank'
  end

  test 'does not send user_permitted and teams_permitted for public cards' do
    author = create(:user, organization: @o, organization_role: 'member')
    member = create(:user, organization: @o, organization_role: 'member')

    card = create(:card, author: author, is_public: true)
    cards = Card.where(state: 'published', author_id: author, organization: @o)

    response = CardBridge.new(cards, user: member).attributes    
    assert_not_includes response[0].keys, :usersPermitted
    assert_not_includes response[0].keys, :teamsPermitted
  end

  test 'sends user_permitted and teams_permitted only for private cards for member user' do
    author = create(:user, organization: @o, organization_role: 'member')

    card1 = create(:card, author: author, is_public: false, title: 'private card with author')
    card2 = create(:card, author: author, is_public: true, title: 'public card with author')

    cards = Card.where(state: 'published', author_id: author, organization: @o)
    response = CardBridge.new(cards, user: author).attributes    
    assert_includes response[0].keys, :teamsPermitted
    assert_includes response[0].keys, :usersPermitted
    assert_not_includes response[1].keys, :usersPermitted
    assert_not_includes response[1].keys, :teamsPermitted
  end

  test 'supports on-demand ask: `quiz,ecl_metadata`' do
    stub_request(:get, 'https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv')
    Card.any_instance.stubs(:update_file_resources_from_filestack).returns(true)
    cards = []
    # add resource.
    resource = create(:resource)
    # add an article card, published.
    article_card = create(:card, author: @u, organization: @o, state:   'published',
      resource_id: resource.id, filestack: [url: 'https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv'],
      ecl_metadata: { 'mark_feature_disabled' => 'true','ecl_source_display_name' => 'display', 'ecl_source_type_name' => 'source'})

    article_card.reload
    cards << article_card

    # create an poll card
    poll_card = create :card, card_type: 'poll', filestack: @filestack, author: @u, organization: @o
    # add few options and set one of them as an answer
    option1 = create :quiz_question_option, quiz_question: poll_card, label: 'yes', is_correct: true
    option2 = create :quiz_question_option, quiz_question: poll_card, label: 'no'
    # capture the author's attempt
    attempt = create :quiz_question_attempt, user: @u, quiz_question: poll_card, selections: [option1.id]
    # capture the stats of attempts
    create :quiz_question_stats, quiz_question: poll_card, attempt_count: 1
    cards << poll_card

    response = CardBridge.new(cards, user: @u,fields: 'quiz,ecl_metadata').attributes

    assert_equal 3, (response[0].with_indifferent_access.keys & %w(eclSourceDisplayName eclSourceTypeName eclSourceLogoUrl)).count
    refute (response[1].with_indifferent_access.keys & %w(eclSourceDisplayName eclSourceTypeName eclSourceLogoUrl)).any?
    assert_equal 6, (response[1].with_indifferent_access.keys & %w(quizQuestionOptions isAssessment hasAttempted attemptedOption attemptCount canBeReanswered)).count
    refute (response[0].with_indifferent_access.keys & %w(quizQuestionOptions isAssessment hasAttempted attemptedOption attemptCount canBeReanswered)).any?
  end

  test 'sends minimal default value if nested attributes are missing' do
    card = create :card, organization: @o, author: @u

    # create two channels
    channel = create :channel, organization: @o
    channel2 = create :channel, organization: @o

    # post this card to both the channels
    card.post_to_channel(channel.id)
    card.post_to_channel(channel2.id)

    # create two teams
    team = create :team, organization: @o
    team2 = create :team, organization: @o

    # add the card to this team
    create :shared_card, team: team, card: card
    create :shared_card, team: team2, card: card

    card.topics = ["Web", "Ruby", "Java"]
    card.update_topics

    response = CardBridge.new([card], user: @u, fields: 'id,channel_ids,channels,teams,tags').attributes[0]
    curated_channels = response[:channels]
    teams = response[:teams]
    tags = response[:tags]

    assert_equal 2, curated_channels.length
    assert_same_elements [:id, :label, :isPrivate], curated_channels[0].keys
    assert_same_elements [:id, :name], teams[0].keys
    assert_same_elements [:id, :name], tags[0].keys
  end

  test 'prevents sending information that is not supported' do
    card = create :card, organization: @o, author: @u
    response = CardBridge.new([card], user: @u, fields: 'id,invalid_title').attributes[0]

    assert_equal card.id.to_s, response[:id]
    assert_not_includes response.keys, :invalid_title
  end

  test 'should supports on-demand price ask' do
    media_card = create(:card, author_id: @u.id, card_type: 'media', card_subtype: 'link')
    price = create :price, card: media_card, currency: '$', amount: 100, ecl_id: media_card.ecl_id
    response = CardBridge.new([media_card], user: @u, fields: 'prices').attributes[0]
    assert_equal price.id, response[:prices][0][:id]
  end
end

require 'test_helper'

class BridgesTest < ActiveSupport::TestCase
  setup do
    @o = build :organization
    @u = build :user, organization: @o

    @c = build :card, organization: @o, author: @u
  end

  test '#initialize introduces variables' do
    klass = Bridges.new([@u], user: @u, fields: "id,first_name,channels(id,label)")

    assert_equal [@u], klass.instance_variable_get(:@objects)
    assert_equal @u, klass.instance_variable_get(:@user)
    assert_equal @o, klass.instance_variable_get(:@organization)
    assert_not_empty klass.instance_variable_get(:@fields)
    assert_not_empty klass.instance_variable_get(:@association_fields)
  end

  test '#initialize does not mandates user & organization' do
    klass = Bridges.new([@u], fields: "id,first_name,channels(id,label)")

    assert_nil klass.instance_variable_get(:@user)
    assert_nil klass.instance_variable_get(:@organization)
  end

  test '#initialize parses blank fields' do
    klass = Bridges.new([@u], fields: "")

    assert_empty klass.instance_variable_get(:@fields)
    assert_empty klass.instance_variable_get(:@association_fields)

    klass = Bridges.new([@u], fields: "  ")

    assert_empty klass.instance_variable_get(:@fields)
    assert_empty klass.instance_variable_get(:@association_fields)
  end

  test '#initialize parses NIL fields' do
    klass = Bridges.new([@u], fields: nil)

    assert_empty klass.instance_variable_get(:@fields)
    assert_empty klass.instance_variable_get(:@association_fields)
  end

  test '#initialize parses :simple fields' do
    klass = Bridges.new([@u], fields: "id,title")

    assert_not_empty klass.instance_variable_get(:@fields)
    assert_empty klass.instance_variable_get(:@association_fields)
  end

  test '#initialize parses :relationship fields' do
    klass = Bridges.new([@u], fields: "channels(id,label)")

    assert_empty klass.instance_variable_get(:@fields)
    assert_not_empty klass.instance_variable_get(:@association_fields)
  end

  test '#initialize raises an exception for incorrect syntax' do
    assert_raise ::ParserError do
      Bridges.new([@u], fields: "channels(id,label")
    end

    assert_raise ::ParserError do
      Bridges.new([@u], fields: "channels(id,label)))")
    end
  end

  test '#dig_badging returns the node matching the name as `badging`' do
    klass = Bridges.new([@c], user: @u, fields: "id,title,card_type,badging(id,title)")

    badging_schema = {
      type: :relationship,
      name: "badging",
      fields: [
        {
          type: :simple,
          name: "id"
        },
        {
          type: :simple,
          name: "title"
        }
      ]
    }
    
    result = klass.dig_badging
    assert_equal badging_schema[:fields], result
  end

  test '#dig_card returns the node matching the name as `card`' do
    klass = Bridges.new([@c], user: @u, fields: "id,start_date,card(id,title)")

    card_schema = {
      type: :relationship,
      name: "card",
      fields: [
        {
          type: :simple,
          name: "id"
        },
        {
          type: :simple,
          name: "title"
        }
      ]
    }
    
    result = klass.dig_card
    assert_equal card_schema[:fields], result
  end

  test '#dig_channels returns the node matching the name as `channels`' do
    klass = Bridges.new([@u], user: @u, fields: "id,first_name,channels(id,label)")

    channel_schema = {
      type: :relationship,
      name: "channels",
      fields: [
        {
          type: :simple,
          name: "id"
        },
        {
          type: :simple,
          name: "label"
        }
      ]
    }
    
    result = klass.dig_channels
    assert_equal channel_schema[:fields], result
  end

  test '#dig_pack_cards returns the node matching the name as `pack_cards`' do
    klass = Bridges.new([@c], user: @u, fields: "id,title,pack_cards(id,title)")

    pack_cards_schema = {
      type: :relationship,
      name: "pack_cards",
      fields: [
        {
          type: :simple,
          name: "id"
        },
        {
          type: :simple,
          name: "title"
        }
      ]
    }
    
    result = klass.dig_pack_cards
    assert_equal pack_cards_schema[:fields], result
  end

  test '#dig_profile returns the node matching the name as `profile`' do
    klass = Bridges.new([@u], user: @u, fields: "id,first_name,profile(id,dob)")

    profile_schema = {
      type: :relationship,
      name: "profile",
      fields: [
        {
          type: :simple,
          name: "id"
        },
        {
          type: :simple,
          name: "dob"
        }
      ]
    }
    
    result = klass.dig_profile
    assert_equal profile_schema[:fields], result
  end

  test '#dig_teams returns the node matching the name as `teams`' do
    klass = Bridges.new([@u], user: @u, fields: "id,first_name,teams(id,name)")

    teams_schema = {
      type: :relationship,
      name: "teams",
      fields: [
        {
          type: :simple,
          name: "id"
        },
        {
          type: :simple,
          name: "name"
        }
      ]
    }
    
    result = klass.dig_teams
    assert_equal teams_schema[:fields], result
  end

  test '#dig_tags returns the node matching the name as `tags`' do
    klass = Bridges.new([@c], user: @u, fields: "id,title,tags(id,name)")

    tags_schema = {
      type: :relationship,
      name: "tags",
      fields: [
        {
          type: :simple,
          name: "id"
        },
        {
          type: :simple,
          name: "name"
        }
      ]
    }
    
    result = klass.dig_tags
    assert_equal tags_schema[:fields], result
  end

  test '#whitelisted_keys raises NotImplementedError and has to be implemented in the child class' do
    klass = Bridges.new([@c], user: @u, fields: "id")

    exception = assert_raise NotImplementedError do
      klass.whitelisted_keys
    end

    assert_match /value method must be implemented in the child class/, exception.message
  end
end

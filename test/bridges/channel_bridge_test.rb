require 'test_helper'

class ChannelBridgeTest < ActiveSupport::TestCase
  setup do
    @o = create :organization
    @a = create :user, organization: @o, organization_role: 'admin'
    @u = create :user, organization: @o

    # a team added as a follower to a channel
    @c1 = create :channel, organization: @o, user: @a, is_private: true
    @t  = create :team, organization: @o
    @t.add_member(@u)
    create(:teams_channels_follow, team: @t, channel: @c1)

    # a user added as a follower to a channel
    @c2 = create :channel, organization: @o, user: @a
    @c2.add_followers([@u])

    # a user added as a follower to a channel and then unfollows.
    @c3 = create :channel, organization: @o, user: @a
    @c3.add_followers([@u])
    follow = Follow.find_by(followable: @c3, user: @u)
    follow.destroy

    # No activity by a user
    @c4 = create :channel, organization: @o, user: @a
  end

  test 'returns minimal data for a channel' do
    response = ChannelBridge.new([@c1, @c2, @c3, @c4], user: @u).attributes

    assert_same_elements [:id, :label, :description, :isPrivate, :allowFollow,
      :bannerImageUrls, :profileImageUrl, :profileImageUrls,
      :isFollowing, :slug, :updatedAt], response[0].keys

    # @c1:
    channel = response.select { |r| r[:id] == @c1.id }[0]
    assert_match /#{@c1.label}/, channel[:label]
    assert channel[:isPrivate]
    assert channel[:allowFollow]
    assert channel[:bannerImageUrls]
    assert_same_elements ['small_url', 'medium_url'], channel[:bannerImageUrls].keys
    assert channel[:profileImageUrl]
    assert channel[:profileImageUrls]
    assert_same_elements ['small_url', 'medium_url'], channel[:profileImageUrls].keys
    assert channel[:isFollowing]
    assert_match /#{@c1.slug}/, channel[:slug]

    # @c2:
    channel = response.select { |r| r[:id] == @c2.id }[0]
    assert_match /#{@c2.label}/, channel[:label]
    refute channel[:isPrivate]
    assert channel[:allowFollow]
    assert channel[:bannerImageUrls]
    assert_same_elements ['small_url', 'medium_url'], channel[:bannerImageUrls].keys
    assert channel[:profileImageUrl]
    assert channel[:profileImageUrls]
    assert_same_elements ['small_url', 'medium_url'], channel[:profileImageUrls].keys
    assert channel[:isFollowing]
    assert_match /#{@c2.slug}/, channel[:slug]

    # @c3:
    channel = response.select { |r| r[:id] == @c3.id }[0]
    assert_match /#{@c3.label}/, channel[:label]
    refute channel[:isPrivate]
    assert channel[:allowFollow]
    assert channel[:bannerImageUrls]
    assert_same_elements ['small_url', 'medium_url'], channel[:bannerImageUrls].keys
    assert channel[:profileImageUrl]
    assert channel[:profileImageUrls]
    assert_same_elements ['small_url', 'medium_url'], channel[:profileImageUrls].keys
    refute channel[:isFollowing]
    assert_match /#{@c3.slug}/, channel[:slug]

    # @c4:
    channel = response.select { |r| r[:id] == @c4.id }[0]
    assert_match /#{@c4.label}/, channel[:label]
    refute channel[:isPrivate]
    assert channel[:allowFollow]
    assert channel[:bannerImageUrls]
    assert_same_elements ['small_url', 'medium_url'], channel[:bannerImageUrls].keys
    assert channel[:profileImageUrl]
    assert channel[:profileImageUrls]
    assert_same_elements ['small_url', 'medium_url'], channel[:profileImageUrls].keys
    refute channel[:isFollowing]
    assert_match /#{@c4.slug}/, channel[:slug]
  end

  test 'excludes unnecessary fields' do
    response = ChannelBridge.new([@c1], user: @u).attributes

    keys = response[0].keys
    refute_includes keys, :followers_count
    refute_includes keys, :is_owner
    refute_includes keys, :authors_count
    refute_includes keys, :curators
    refute_includes keys, :carousels
    refute_includes keys, :video_streams_count
    refute_includes keys, :courses_count
    refute_includes keys, :smartbites_count
    refute_includes keys, :published_pathways_count
    refute_includes keys, :published_journeys_count
    refute_includes keys, :is_curator
    refute_includes keys, :teams
    refute_includes keys, :creator
  end

  test 'supports on-demand asks' do
    klass = ChannelBridge.new([@c1], user: @u, fields: 'id,label')
    attributes = klass.attributes[0]

    assert_same_elements [:id, :label], attributes.keys
    assert_equal @c1.id, attributes[:id]
    assert_equal @c1.label, attributes[:label]
  end

  test 'supports on-demand ask: followers_count' do
    klass = ChannelBridge.new([@c1, @c2], user: @u, fields: 'id,followers_count')
    response = klass.attributes

    # only 1 follower since team is added a follower to `@c1`
    c1 = response.select { |c| c[:id] == @c1.id }[0]
    assert_equal 1, c1[:followersCount]

    # only 1 follower since a user is added a follower to `@c2`
    c2 = response.select { |c| c[:id] == @c2.id }[0]
    assert_equal 1, c2[:followersCount]
  end

  test 'returns correct followers_count when a user is suspended, user is following a team and team is following a channel' do
    u = create(:user, organization: @o)
    @t.add_member(u)
    #Before suspending 2 users in a team
    channel = ChannelBridge.new([@c1], user: @u, fields: 'id,followers_count')
    assert_equal 2, channel.attributes[0][:followersCount]
    u.suspend!
    #After suspending `u` 1 user in a team
    channel = ChannelBridge.new([@c1], user: @u, fields: 'id,followers_count')
    assert_equal 1, channel.attributes[0][:followersCount]
  end

  test 'supports on-demand ask: channel creator' do
    klass = ChannelBridge.new([@c1, @c2], user: @u, fields: 'id,creator')
    response = klass.attributes

    c1 = response.select { |c| c[:id] == @c1.id }[0][:creator]
    assert_equal @a.name, c1[:name]
    assert_equal @a.handle, c1[:handle]
    refute @a.is_suspended

    c2 = response.select { |c| c[:id] == @c2.id }[0][:creator]
    assert_equal @a.name, c2[:name]
    assert_equal @a.handle, c2[:handle]
    refute @a.is_suspended
  end

  test 'supports on-demand ask: is_following' do
    klass = ChannelBridge.new([@c1, @c2, @c3, @c4], user: @u, fields: 'id,is_following')
    response = klass.attributes

    c1 = response.select { |c| c[:id] == @c1.id }[0]
    assert c1[:isFollowing]

    c2 = response.select { |c| c[:id] == @c2.id }[0]
    assert c2[:isFollowing]

    c3 = response.select { |c| c[:id] == @c3.id }[0]
    refute c3[:isFollowing]

    c4 = response.select { |c| c[:id] == @c4.id }[0]
    refute c4[:isFollowing]
  end

  test 'should return 0 when there are no followers of a channel' do
    # create new channel
    channel = create :channel, organization: @o
    response = ChannelBridge.new([channel], user: @u, fields: 'id,followers_count').attributes[0]
    assert_equal channel.id, response[:id]
    assert_equal 0, response[:followersCount]
  end

  test 'prevents sending information that is not supported' do
    channel = create :channel, organization: @o
    response = ChannelBridge.new([channel], user: @u, fields: 'id,invalid_count').attributes[0]

    assert_equal channel.id, response[:id]
    assert_not_includes response.keys, :invalid_count
  end
end

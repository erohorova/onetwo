require 'test_helper'
class WidgetBridgeTest < ActiveSupport::TestCase
  setup do
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @channel = create(:channel, organization: @organization)

    @wid_1 = create(:widget, organization: @organization, creator: @user,
        parent: @channel, context: 'channel', code: 'Widget 1')
    @wid_2 = create(:widget, organization: @organization, creator: @user,
        parent: @channel, context: 'channel', code: 'Widget 2')
  end

  test 'returns minimal response for a widget' do
    response = WidgetBridge.new([@wid_1, @wid_2], user: @user).attributes

    assert_same_elements [:id, :code, :context, :enabled, :parent_id, :parent_type, :creator], response[0].keys

    # @wid_1
    widget = response.select { |r| r[:id] == @wid_1.id }[0]
    assert_equal @wid_1.code, widget[:code]
    assert_equal @wid_1.context, widget[:context]
    assert_equal @wid_1.enabled, widget[:enabled]

    # @wid_2
    widget = response.select { |r| r[:id] == @wid_2.id }[0]
    assert_equal @wid_2.code, widget[:code]
    assert_equal @wid_2.context, widget[:context]
    assert_equal @wid_2.enabled, widget[:enabled]
  end

  test 'excludes unnecessary fields' do
    response = WidgetBridge.new([@wid_1], user: @user).attributes

    keys = response[0].keys
    refute_includes keys, :createdAt
    refute_includes keys, :updatedAt
    refute_includes keys, :organization
  end

  test 'supports on-demand ask: widget creator' do
    response = WidgetBridge.new([@wid_1, @wid_2], user: @user, fields: 'id,creator').attributes

    widget_1 = response.select { |w| w[:id] == @wid_1.id }[0][:creator]
    assert_equal @user.name, widget_1[:name]
    assert_equal @user.handle, widget_1[:handle]
    refute widget_1[:is_suspended]

    widget_2 = response.select { |w| w[:id] == @wid_2.id }[0][:creator]
    assert_equal @user.name, widget_2[:name]
    assert_equal @user.handle, widget_2[:handle]
    refute widget_2[:is_suspended]
  end
end


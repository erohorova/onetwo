require 'test_helper'

class JourneyBridgeTest < ActiveSupport::TestCase
  setup do
    @org = create :organization
    @user = create :user, organization: @org

    @journey_card = create(:card, author: @user, organization: @org, card_type: 'journey', card_subtype: 'self_paced')
    create :card_metadatum, card: @journey_card, plan: 'free', level: 'beginner', average_rating: 5

    @pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack')
    @journey_card.add_card_to_journey(@pathway_card.id)

    @pathway_card1 = create(:card, author: @user, organization: @org, card_type: 'pack')
    @journey_card.add_card_to_journey(@pathway_card1.id)

    @card = create(:card, author: @user, organization: @org)
    @pathway_card.add_card_to_pathway(@card.id, 'Card')
    @pathway_card1.add_card_to_pathway(@card.id, 'Card')
  end

  test 'returns minimal information' do
    response = JourneyBridge.new([@journey_card], user: @user).attributes

    assert_equal 1, response.length

    pathways = response[0][:journeySection]
    pathway1 = pathways.select { |pathway| pathway[:id] == @pathway_card.id }[0]
    assert_match /This is a cool card/, pathway1[:block_message]
    assert_nil pathway1[:block_title]
    assert_equal 0, pathway1[:completed_percentage]
    assert_equal false, pathway1[:hidden]
    assert_nil pathway1[:start_date]
    assert_equal 'draft', pathway1[:state]
    assert_equal [pathway1[:id], @card.id].to_set, pathway1[:card_pack_ids].to_set

    pathway2 = pathways.select { |pathway| pathway[:id] == @pathway_card1.id }[0]
    assert_match /This is a cool card/, pathway2[:block_message]
    assert_nil pathway2[:block_title]
    assert_equal 0, pathway2[:completed_percentage]
    assert_equal false, pathway2[:hidden]
    assert_nil pathway2[:start_date]
    assert_equal 'draft', pathway2[:state]
    assert_equal [pathway2[:id], @card.id].to_set, pathway2[:card_pack_ids].to_set
  end

  test 'prevents sending information that is not supported' do
    response = JourneyBridge.new([@journey_card], user: @user, fields: 'id,invalid_title').attributes[0]

    assert_equal @journey_card.id.to_s, response[:id]
    assert_not_includes response.keys, :invalid_title
  end
end

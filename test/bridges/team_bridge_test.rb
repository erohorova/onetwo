require 'test_helper'

class TeamBridgeTest < ActiveSupport::TestCase
  setup do
    @o = create :organization
    @t = create :team, organization: @o

    # an invited user
    @invited_user = create :user, organization: @o
    # with pending invitation
    create :invitation, invitable: @t, recipient: @invited_user, accepted_at: nil

    # a team admin
    @team_admin = create :user, organization: @o
    @t.add_admin(@team_admin)

    # a sub admin
    @team_sub_admin = create :user, organization: @o
    @t.add_sub_admin(@team_sub_admin)

    # a suspended team member
    @suspended_member = create :user, organization: @o, is_suspended: true
    @t.add_member(@suspended_member)

    # an active team member
    @team_member = create :user, organization: @o
    @t.add_member(@team_member)
  end

  test 'returns minimal data for any user' do
    response = TeamBridge.new([@t], user: @team_admin).attributes
    team = response[0]

    expected_keys = %i[
      id name description slug imageUrls membersCount isPrivate
      isTeamAdmin isTeamSubAdmin isMember isPending isEveryoneTeam isMandatory
      onlyAdminCanPost createdBy
    ]
    assert_same_elements expected_keys, team.keys
    assert_equal @t.id, team[:id]
    assert_match /#{@t.name}/, team[:name]
    assert_match /#{@t.description}/, team[:description]
    assert_match /#{@t.slug}/, team[:slug]
    assert_equal [:small, :medium, :large], team[:imageUrls].keys
    assert_equal @t.users.visible_members.uniq.count, team[:membersCount]
    assert_equal @t.is_private, team[:isPrivate]
    assert team[:isTeamAdmin]
    refute team[:isTeamSubAdmin]
    refute team[:isMember]
    refute team[:isPending]
    refute team[:onlyAdminCanPost]
  end

  test 'returns minimal data for a team' do
    response = TeamBridge.new([@t], user: @team_admin).attributes
    team = response[0]

    expected_keys = %i[
      id name description slug imageUrls membersCount isPrivate isTeamAdmin
      isTeamSubAdmin isMember isPending isEveryoneTeam isMandatory onlyAdminCanPost
      createdBy
    ]

    assert_same_elements expected_keys, team.keys

    assert_equal @t.id, team[:id]
    assert_match /#{@t.name}/, team[:name]
    assert_match /#{@t.description}/, team[:description]
    assert_match /#{@t.slug}/, team[:slug]
    assert_equal [:small, :medium, :large], team[:imageUrls].keys
    assert_equal @t.users.visible_members.uniq.count, team[:membersCount]
    assert_equal @t.is_private, team[:isPrivate]
    assert_equal @t.admins.first.try(:name), team[:createdBy]
    assert team[:isTeamAdmin]
    refute team[:isTeamSubAdmin]
    refute team[:isMember]
    refute team[:isPending]
    refute team[:onlyAdminCanPost]
  end

  test 'supports on-demand asks: members_count' do
    response = TeamBridge.new([@t], user: @team_sub_admin, fields: 'id,members_count').attributes
    team = response[0]

    assert_same_elements [:id, :membersCount], team.keys
    assert_equal @t.id, team[:id]
    assert_equal @t.users.visible_members.uniq.count, team[:membersCount]
  end

  test 'determines a sub-admin for a given team' do
    response = TeamBridge.new([@t], user: @team_sub_admin).attributes
    team = response[0]

    refute team[:isTeamAdmin]
    assert team[:isTeamSubAdmin]
    refute team[:isMember]
    refute team[:isPending]
  end

  test 'determines a member for a given team' do
    response = TeamBridge.new([@t], user: @team_member).attributes
    team = response[0]

    refute team[:isTeamAdmin]
    refute team[:isTeamSubAdmin]
    assert team[:isMember]
    refute team[:isPending]
  end

  test 'determines an invited member for a given team' do
    response = TeamBridge.new([@t], user: @invited_user).attributes
    team = response[0]

    refute team[:isTeamAdmin]
    refute team[:isTeamSubAdmin]
    refute team[:isMember]
    assert team[:isPending]
  end

  test 'support on-demand ask: `label`' do
    response = TeamBridge.new([@t], user: @team_member, fields: 'label').attributes
    team = response[0]

    assert_equal @t.name, team[:label]
  end

  test 'support on-demand ask: `channel_ids`' do
    channel = create :channel, organization: @o
    @t.following_channels << channel
    response = TeamBridge.new([@t], user: @team_member, fields: 'channel_ids').attributes
    team = response[0]

    assert_equal :channelIds, team.keys.first
    assert_same_elements [channel.id], team[:channelIds]
  end

  test 'prevents sending information that is not supported' do
    klass = TeamBridge.new([@t], user: @team_member, fields: 'id,invalid_name')
    attributes = klass.attributes[0]

    assert_equal @t.id, attributes[:id]
    assert_not_includes attributes.keys, :invalid_name
  end
end

require 'test_helper'

class PathwayBridgeTest < ActiveSupport::TestCase
  setup do
    @org = create :organization
    @user = create :user, organization: @org
  end

  test 'should give cover cards team,channels information' do

    pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack', auto_complete: false)
    card = create(:card, author: @user, organization: @org)
    pathway_card.add_card_to_pathway(card.id, 'Card')

    # create two teams
    team = create :team, organization: @org
    team2 = create :team, organization: @org

    # add the card to this team
    create :shared_card, team: team, card: pathway_card
    create :shared_card, team: team2, card: pathway_card
    
    # create two channels
    channel = create :channel, organization: @org
    channel2 = create :channel, organization: @org

    # add the card to this channels
    pathway_card.channels << channel
    pathway_card.channels << channel2

    response = PathwayBridge.new([pathway_card], user: @user, fields:'id,channels(id,label,is_private),teams(id,label),channels,teams,pack_cards(id,title,card_type),pack_cards').attributes[0]
    assert_equal team.name, response[:teams][0][:label]
    assert_equal channel.label, response[:channels][0][:label]
  end

  test 'should give `not authorized to access this card` if user dont have access to this card' do
    user = create :user, organization: @org
    pathway_card = create(:card, author: @user, organization: @org, card_type: 'pack', auto_complete: false)
    private_card = create(:card, author: @user, organization: @org, is_public: false)
    public_card = create(:card, author: @user, organization: @org)

    pathway_card.add_card_to_pathway(private_card.id, 'Card')
    pathway_card.add_card_to_pathway(public_card.id, 'Card')

    response = PathwayBridge.new([pathway_card], user: user, fields:'id,title,pack_cards(id,title,card_type,message),pack_cards').attributes[0]

    # should show not authorized message
    assert_equal 'You are not authorized to access this card', response[:packCards][0][:message]

    # should display message
    assert_equal public_card.message, response[:packCards][1][:message]
  end

end  
require 'test_helper'

class TagBridgeTest < ActiveSupport::TestCase
  setup do
    @o = create :organization
    @u = create :user, organization: @o

    @tag = create :tag
  end

  test 'returns minimal data for tag' do
    response = TagBridge.new([@tag], user: @u).attributes[0]

    assert_same_elements [:id, :name], response.keys
    assert_equal @tag.id, response[:id]
    assert_match /#{@tag.name}/, response[:name]
  end

  test 'prevents sending information that is not supported' do
    response = TagBridge.new([@tag], user: @u, fields: 'id,invalid_name').attributes[0]

    assert_equal @tag.id, response[:id]
    assert_not_includes response.keys, :invalid_name
  end
end
require 'test_helper'

class AssignmentBridgeTest < ActiveSupport::TestCase
  setup do
    @o = create :organization
    @u = create :user, organization: @o
    @filestack = [{
      'elements' => {
        'filename' => 'Screen Shot 2018-06-05 at 11.51.06 AM.png',
        'mimetype' => 'image/png',
        'size' => '1946518',
        'source' => 'local_file_system',
        'url' => 'https://cdn.filestackcontent.com/d2sjlUgWT1WRJwldER6P',
        'handle' => 'd2sjlUgWT1WRJwldER6P',
        'status' => 'Stored',
      }
    }]
  end

  test 'returns minimal data for an assignment card' do
    image = 'https://cdn.filestackcontent.com/KxaMgfERPugXDbAySGTi'
    # add resource
    resource = create(:resource)
    # ARTICLE: create card with resource
    media_card = create(
      :card, author_id: @u.id, card_type: 'media', card_subtype: 'link',
      resource: resource, readable_card_type_name: 'book', provider_image: image,
      provider: 'Example', votes_count: 4, comments_count: 10, organization: @o
    )
    # add more metadata to the card
    create(
      :card_metadatum, card: media_card, plan: 'free',
      level: 'beginner', average_rating: 5
    )
    # add price
    create(
      :price, card: media_card, currency: '$',
      amount: 100, ecl_id: media_card.ecl_id
    )
    # self-assign this card
    assignment = create(:assignment, assignable: media_card, assignee: @u)
    response = AssignmentBridge.new([assignment], user: @u).attributes[0]

    assert_same_elements %i[id assignable], response.keys
    assert_equal assignment.id, response[:id].to_i
    assert_match /#{media_card.message}/, response[:assignable][:message]
    assert_equal 'media', response[:assignable][:cardType]
    assert_equal 'link', response[:assignable][:cardSubtype]
    assert_equal 'beginner', response[:assignable][:skillLevel]
    assert_equal 4, response[:assignable][:votesCount]
    assert_equal 10, response[:assignable][:commentsCount]
    assert response[:assignable][:isAssigned]
    assert_equal @u.handle, response[:assignable][:author][:handle]

    expected_avatar_keys = %w[tiny small medium large]
    actual__avatar_keys = response[:assignable][:author][:avatarimages].keys
    assert_same_elements expected_avatar_keys, actual__avatar_keys

    assert_match /#{@u.full_name}/, response[:assignable][:author][:fullName]
  end

  test 'supports on-demand ask: for assignment cards' do
    resource = create(:resource)
    # ARTICLE: create card with resource
    media_card = create(:card, author_id: @u.id, card_type: 'media', card_subtype: 'link',
      resource: resource, readable_card_type_name: 'book', provider_image: 'https://cdn.filestackcontent.com/KxaMgfERPugXDbAySGTi',
      provider: 'Example', votes_count: 4, comments_count: 10, organization: @o)
    # add more metadata to the card
    card_metadatum = create :card_metadatum, card: media_card, plan: 'free', level: 'beginner', average_rating: 5
    # add price
    price = create :price, card: media_card, currency: '$', amount: 100, ecl_id: media_card.ecl_id
    # self-assign this card
    assignment = create :assignment, assignable: media_card, assignee: @u

    assignor_user = create(:user, organization: @o)
    team = create(:team)
    create(:team_assignment, team: team, assignment: assignment, assignor: assignor_user)

    response = AssignmentBridge.new([assignment], user: @u, fields: 'id,state,completed_at,due_at,start_date,started_at,created_at,card(id,completed_percentage,card_subtype,card_type,slug,badging,skill_level,title,message,ecl_duration_metadata),assignor,assignable').attributes[0]

    assert_same_elements [:id, :state, :completedAt, :dueAt, :startDate, :startedAt, :createdAt, :assignor, :assignable], response.keys
    assert_equal media_card.message, response[:assignable][:message]
    assert_equal 'media', response[:assignable][:cardType]
    assert_equal 'link', response[:assignable][:cardSubtype]
    assert_equal 'beginner', response[:assignable][:skillLevel]
    assert_equal 0, response[:assignable][:completedPercentage]
    assert_equal assignor_user.name, response[:assignor][:name]
  end

  test 'consider on-demand `assignor` field to return part of email when first_name and last_name of that user is nil' do
    card_author = create :user, organization: @o
    card = create :card, author: card_author, organization: @o
    user = create :user, first_name: nil, last_name: nil, email: 'person@example.com', organization: @o
    assignee = create :user, organization: @o
    assignment = create :assignment, assignable: card, assignee: assignee
    team_assignment = create :team_assignment, assignment_id: assignment.id, assignor: user

    response = AssignmentBridge.new([assignment], user: user, fields: 'assignor').attributes[0]

    assert_equal 'person', response[:assignor][:name]
  end

  test 'prevents sending information that is not supported' do
    user = create :user, first_name: nil, last_name: nil, email: 'person@example.com', organization: @o
    card = create :card, author: user, organization: @o
    assignee = create :user, organization: @o
    assignment = create :assignment, assignable: card, assignee: assignee

    klass = AssignmentBridge.new([assignment], user: assignee, fields: 'id,state,invalid_field')
    attributes = klass.attributes[0]
    assert_equal assignment.id, attributes[:id]
    assert_equal assignment.state, attributes[:state]
    assert_not_includes attributes.keys, :invalid_field
  end
end

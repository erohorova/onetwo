require 'test_helper'

class Bridges::ParserTest < ActiveSupport::TestCase
  teardown do
    Bridges::Parser::MAX_INPUT_LENGTH = 500
    Bridges::Parser::MAX_FIELDS_SIZE = 50
    Bridges::Parser::MAX_DEPTH_SIZE = 3
  end

  test 'validate schema' do
    response = Bridges::Parser.parse('id,title,channels(id,label,authors(id,first_name)),teams(id,name)')
    
    schema = {
      type: :root,
      fields: [
        {
          type: :simple,
          name: 'id'
        },
        {
          type: :simple,
          name: 'title'
        },
        {
          type: :relationship,
          name: 'channels',
          fields:[
            {
              type: :simple,
              name: 'id'
            },
            {
              type: :simple,
              name: 'label'
            },
            {
              type: :relationship,
              name: 'authors',
              fields: [
                {
                  type: :simple,
                  name: 'id'
                },
                {
                  type: :simple,
                  name: 'first_name'
                }
              ]
            }
          ]
        },
        {
          type: :relationship,
          name: 'teams',
          fields: [
            {
              type: :simple,
              name: 'id'
            },
            {
              type: :simple,
              name: 'name'
            }
          ]
        }
      ]
    }

    assert_equal schema, response
  end

  test 'tree structure always have a root and the fields' do
    response = Bridges::Parser.parse('id')

    assert_equal :root, response[:type]
    assert_equal [:type, :fields].to_set, response.keys.to_set
  end

  test 'all the attributes are pushed to fields of the root node' do
    response = Bridges::Parser.parse('id')

    root_fields = [
      {
        type: :simple,
        name: 'id'
      }
    ]
    assert_equal root_fields, response[:fields]
  end

  test 'comma-separated attributes are considered to be of type `simple`' do
    response = Bridges::Parser.parse('id')

    root_field = response[:fields][0]

    assert_equal :simple, root_field[:type]
    assert_equal 'id', root_field[:name]
  end

  test 'nested attributes are considered to be of type `relationship`' do
    response = Bridges::Parser.parse('channels(id,label)')

    root_field = response[:fields][0]

    assert_equal :relationship, root_field[:type]
    assert_equal 'channels', root_field[:name]
  end

  test 'nested attributes holds fields of type `simple`' do
    response = Bridges::Parser.parse('channels(id,label)')

    root_field = response[:fields][0]
    id    = root_field[:fields][0]
    label = root_field[:fields][1]

    assert_equal :simple, id[:type]
    assert_equal 'id', id[:name]

    assert_equal :simple, label[:type]
    assert_equal 'label', label[:name]
  end

  test 'nested attributes holds fields of type `relationship` for associations' do
    response = Bridges::Parser.parse('channels(id,authors(first_name))')

    root_field = response[:fields][0]
    id    = root_field[:fields][0]

    assert_equal :simple, id[:type]
    assert_equal 'id', id[:name]

    authors = root_field[:fields][1]
    assert_equal :relationship, authors[:type]
    assert_equal 'authors', authors[:name]

    author_fields = authors[:fields]
    author_first_name = author_fields[0]
    assert_equal :simple, author_first_name[:type]
    assert_equal 'first_name', author_first_name[:name]
  end

  test 'must not accept total length of string more than `x` value' do
    Bridges::Parser::MAX_INPUT_LENGTH = 20

    exception = assert_raises ::ParserError do
      Bridges::Parser.parse('id,title,channels(id,label)')
    end
    assert_match /Maximum input length should not be more than 20/, exception.message
  end

  test 'must not accept individual field size more than `x` value' do
    Bridges::Parser::MAX_FIELDS_SIZE = 10

    exception = assert_raises ::ParserError do
      Bridges::Parser.parse('id,title,establishment')
    end
    assert_match /Maximum fields size should not be more than 10/, exception.message
  end

  test 'must not accept depth of the nested attributes more than `x` value' do
    Bridges::Parser::MAX_DEPTH_SIZE = 2

    exception = assert_raises ::ParserError do
      Bridges::Parser.parse('id,cards(title,channels(label,authors(first_name)))')
    end
    assert_match /Maximum depth size should not be more than 2/, exception.message
  end

  test 'incomplete parantheses must throw an error' do
    exception = assert_raises ::ParserError do
      Bridges::Parser.parse('id,cards(title')
    end

    assert_match /Parentheses were not closed. Parser detected the syntax error/, exception.message
  end

  test 'incomplete field ending with `)` throws an exception' do
    exception = assert_raises ::ParserError do
      Bridges::Parser::MAX_DEPTH_SIZE = 4
      Bridges::Parser.parse('2093840sasf9i93i945923KSD(S#$(029rweoprfjsdjfjsdf012ekkm%,32490234903(03204)')
    end
    assert_match /Parentheses were not closed. Parser detected the syntax error/, exception.message
  end

  test 'incomplete field ending with `(` throws an exception' do
    exception = assert_raises ::ParserError do
      Bridges::Parser::MAX_DEPTH_SIZE = 4
      Bridges::Parser.parse('2093840sasf9i93i945923KSD(S#$(029rweoprfjsdjfjsdf012ekkm%,32490234903(')
    end
    assert_match /Parentheses were not closed. Parser detected the syntax error/, exception.message
  end

  test 'incomplete field ending with `,` throws an exception' do
    exception = assert_raises ::ParserError do
      Bridges::Parser::MAX_DEPTH_SIZE = 4
      Bridges::Parser.parse('2093840sasf9i93i945923KSD(S#$(029rweoprfjsdjfjsdf012ekkm%,32490234903(,')
    end
    assert_match /Parentheses were not closed. Parser detected the syntax error/, exception.message
  end

  test 'unmatched number of parantheses must throw an error' do
    exception = assert_raises ::ParserError do
      Bridges::Parser.parse('id,cards(title))')
    end

    assert_match /Trying to remove the root node. Check the syntax/, exception.message
  end
end

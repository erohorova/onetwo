require 'test_helper'

class LearningQueueItemBridgeTest < ActiveSupport::TestCase
  setup do
    @org = create :organization
    @user = create :user, organization: @org
    card = create :card, author: @user
    @completed_item = create :learning_queue_item, :from_assignments, queueable: card, user: @user, state: "completed"
  end

  test 'returns minimal fields in response for learning_queue_items' do
    response = LearningQueueItemBridge.new([@completed_item], user: @user).attributes

    expected_keys = %i(id displayType deepLinkId deepLinkType source updatedAt snippet queueable)
    assert_same_elements expected_keys, response[0].keys
    assert_equal @completed_item.id, response[0][:id]
  end

  test 'supports on-demand fields' do
    fields = 'id,display_type,deep_link_id,deep_link_type'
    learning_queue_item_bridge = LearningQueueItemBridge.new([@completed_item], user: @user, fields: fields)
    response = learning_queue_item_bridge.attributes
    keys = response[0].keys

    expected_keys = %i(id displayType deepLinkId deepLinkType)
    assert_same_elements expected_keys, keys
  end
end
require 'test_helper'

class BadgingBridgeTest < ActiveSupport::TestCase
  setup do
    @o = create :organization
    @u = create :user, organization: @o
    @resource = create(:resource)
    @media_card = create(:card, author_id: @u.id, card_type: 'media', card_subtype: 'link',
      resource: @resource, readable_card_type_name: 'book', provider_image: 'https://cdn.filestackcontent.com/KxaMgfERPugXDbAySGTi',
      provider: 'Example', votes_count: 4, comments_count: 10, organization: @o)

    @badge = create :badge, organization: @o, type: 'CardBadge'
    @card_badging = create :card_badging, badgeable_id: @media_card.id, title: 'new badge', type: 'CardBadging', badge_id: @badge.id
  end

  test 'returns minimal data for badge' do
    response = BadgingBridge.new([@card_badging], user: @u).attributes[0]
    assert_same_elements [:id, :title, :imageUrl, :badgeId, :badgeableId], response.keys

    assert_equal @card_badging.id, response[:id]
    assert_match /#{@card_badging.title}/, response[:title]
    assert_equal @badge.id, response[:badgeId]
    assert_equal @media_card.id, response[:badgeableId]
  end

  test 'supports on-demand ask: `image_url`' do
    response = BadgingBridge.new([@card_badging], user: @u, fields:'image_url').attributes[0]
    assert_same_elements [:imageUrl], response.keys
  end

  test 'prevents sending information that is not supported' do
    response = BadgingBridge.new([@card_badging], user: @u, fields: 'id,invalid_badge').attributes[0]

    assert_equal @card_badging.id, response[:id]
    assert_not_includes response.keys, :invalid_badge
  end
end

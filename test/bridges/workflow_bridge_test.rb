require 'test_helper'

class WorkflowBridgeTest < ActiveSupport::TestCase
  setup do
    @o = create :organization
    @u = create :user, organization: @o
    @workflow = create(:workflow, user_id: @u.id, organization: @o,
      is_enabled: true,
      wf_action: {entity: "Group" ,type: "create_dynamic",
        value: {
          column_name: 'created_at',
          wf_column_name: "user_created_at",
          prefix: '',suffix: '',
        },
        metadata: {
          is_private: true,
          description: 'Group created from workflow',
          is_mandatory: true
        }
      }
    )
  end

  test 'returns minimal data for workflows' do
    response = WorkflowBridge.new([@workflow], user: @u).attributes[0]
    assert_same_elements [:id, :name, :status, :isEnabled, :wfType, :wfInputSource, :wfActionableColumns,
      :wfRules, :wfAction, :wfOutputResult, :createdAt, :creator], response.keys

    assert_equal @workflow.id, response[:id]
    assert_match /#{@workflow.name}/, response[:name]
    assert_equal @workflow.status, response[:status]
    assert_equal @workflow.is_enabled, response[:isEnabled]
    assert_equal @workflow.wf_type, response[:wfType]
    assert_equal @workflow.wf_input_source, response[:wfInputSource]
    assert_equal @workflow.wf_actionable_columns, response[:wfActionableColumns]
    assert_equal @workflow.wf_rules, response[:wfRules]
    assert_equal @workflow.wf_action, response[:wfAction]
    assert_equal @workflow.wf_output_result, response[:wfOutputResult]
    assert_equal @workflow.created_at, response[:createdAt]
    assert_equal @u.id, response[:creator][:id]
    assert_equal @u.handle, response[:creator][:handle]
    assert_equal @u.full_name, response[:creator][:fullName]
  end

  test 'should support on-demand ask' do
    response = WorkflowBridge.new([@workflow], user: @u, fields: 'wf_type, is_enabled').attributes[0]
    assert_equal @workflow.is_enabled, response[:isEnabled]
    assert_equal @workflow.wf_type, response[:wfType]
  end
end

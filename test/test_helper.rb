require 'simplecov'
require 'sidekiq/testing'
# require 'codacy-coverage'

ENV["RAILS_ENV"] ||= "test"
ENV['MAGICK_THREAD_LIMIT'] = '1'

require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

require 'mocha/mini_test'
require 'mocha/test_unit'
require 'jwt_auth'
require 'mandrill'
require 'minitest/test_profile'
# require 'minitest/fail_fast'
# require "minitest/reporters"

# Add more helper methods to be used by all tests here...
require 'webmock/minitest'

# Codacy::Reporter.start
Sidekiq::Testing.fake!
Sidekiq::Testing.inline!
Minitest::TestProfile.use!
# Minitest::Reporters.use! [Minitest::Reporters::MeanTimeReporter.new]

Dir[Rails.root.join("test/support/**/*.rb")].each { |f| require f }

# =========================================
# UNCOMMENT THIS LINE TO MAKE A COVERAGE REPORT:
# SimpleCov.start 'rails'
# =========================================

# THIS WOULD POSSIBLY THE FIX FOR TESTS THEY FAIL
# IN TRAVIS BUT NOT IN LOCALLY.
# ADD THIS LINE TO GEMFILE:  gem 'minitest-around'
# TestMutex = Mutex.new

# class Minitest::Test
#   i_suck_and_my_tests_are_order_dependent!
#   def around(&block)
#     TestMutex.synchronize do
#       WebMock.enable!
#       WebMock.disable_net_connect!(allow_localhost: false)
#       block.call
#       WebMock.enable!
#       WebMock.disable_net_connect!(allow_localhost: false)
#     end
#   end
# end

class ActiveSupport::TestCase
  # This does absolutely nothing except give some visual organization to the tests.
  # def self.describe(_description, &blk); blk.call; end

  class_attribute :use_shared_db_connection
  self.use_shared_db_connection = true

  ActiveRecord::Migration.check_pending!
  Rails.application.eager_load!

  include FactoryGirl::Syntax::Methods, HelperMethods
  include Taxonomies, OktaFederatedStubs
  include AnalyticsTestHelper

  # Bypass paperclip validations for test
  Paperclip::MediaTypeSpoofDetector.class_eval do
    def spoofed?
      false
    end
  end

  class Paperclip::Attachment
    def post_process
      # do nothing in test env
    end
  end

  PaperTrail.enabled = false

  setup do
    # THIS IS SUPER STUPID. SOMETHING IS OVERRIDING DEFAULT LOOGER LEVEL FROM 0 to 3
    Rails.logger.level = 0 if Rails.env.test? && Rails.logger.level != 0

    WebMock.disable!
    ES_SEARCH_CLIENT.indices.delete index:"*_test*"
    WebMock.enable!

    TestAfterCommit.enabled = false

    FactoryGirl::SyntaxRunner.send(:include, Taxonomies)
    Mandrill::API.any_instance.stubs(:call).returns({})

    if self.class.use_transactional_fixtures
      DatabaseCleaner.strategy = :transaction
    else
      DatabaseCleaner.strategy = :deletion, {cache_tables: true}
    end
    DatabaseCleaner.start

    stub_bulk_request
    stub_mongo_requests
    stub_model_callbacks
    stub_background_jobs
    stub_users_content_queue
    stub_tracking
    stub_elasticsearch_requests
    stub_influx_requests
    stub_configs_readable_card_types
    stub_redis_requests
    stub_workflow_patch_request
  end

  teardown do
    DatabaseCleaner.clean
  end

  # turn off verbosity
  $-v = nil
end

#aws setup
ENV['AWS_ACCESS_KEY_ID'] = 'ACCESS_KEY_ID'
ENV['AWS_SECRET_ACCESS_KEY'] = 'SECRET_ACCESS_KEY'
AWS.stub! #stub everything

# Edcast::Application.load_tasks

# Dummy public and private keys for encryption for EP-18087
ENV['JS_PUBLIC_KEY_RAILS_TEST'] = "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt5zewcoiWRimNEw8+eO3mAmF9Jr8+wvJXYbyK8gGhtrS1mVgZvKiDILEvUeuMz3ld9z6tvO/dtAuAB6QAv4RfBqplnp1FWPRN5J509ITlDBg88/eikCg+ptivqgu6CqTwA+xnIq4frN7WBda4a8LEqGfc7L3vSo2oBweEV++pQgRgQmxNy/xbuLfAtJeZu/Io0rlVLDk8FekMplbfAieOUB6FcVW5yCLt4LPpazvF/mKSWXb+VY6fx0TwdQ1GZhbFV6a4uyZ/LyDZpV6Nu91m9jvFXPPYIzCQYkBS1aP2VAwFmLAk0zWy99fo4uTWtn4Wn0M2LafRbwV5mud4z3DuQIDAQAB\n-----END PUBLIC KEY-----\n"
ENV['JS_PRIVATE_KEY_TEST'] = "-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEAt5zewcoiWRimNEw8+eO3mAmF9Jr8+wvJXYbyK8gGhtrS1mVgZvKiDILEvUeuMz3ld9z6tvO/dtAuAB6QAv4RfBqplnp1FWPRN5J509ITlDBg88/eikCg+ptivqgu6CqTwA+xnIq4frN7WBda4a8LEqGfc7L3vSo2oBweEV++pQgRgQmxNy/xbuLfAtJeZu/Io0rlVLDk8FekMplbfAieOUB6FcVW5yCLt4LPpazvF/mKSWXb+VY6fx0TwdQ1GZhbFV6a4uyZ/LyDZpV6Nu91m9jvFXPPYIzCQYkBS1aP2VAwFmLAk0zWy99fo4uTWtn4Wn0M2LafRbwV5mud4z3DuQIDAQABAoIBAH1Oq59piaNwdQ0/8mVACW3TIf5bP8c8tgIENIW4juu9ryHuMapQcFNFc2sR3krONSpmPxxYoRQ+4wXl1baVGe2ubIprrDga1Q+uAUKUHhSGJ8R82o1TqgkdvngLY2fClu7+RuJksaCWcNkfp0BaK55deuqE+xm4E2uVhNeqlfsdq7fuuG+BLq0eT/GPqahY2ZWRvUTr/yu8Xe7iFfvFrweJSNGxZ1NCb2OAkbN8BsrWYUnJWcptaq+OwtHlWOM+kOZeFZCZ/39IUizh7SKkMTHAwvccgciyjhMVKYMTy0nNEZJGNbRrc+p6P8uTo68qZKUM1nEKWkB9JG30t3TcsHkCgYEA6h9YUagYDNQlTP8IcGo7nNUOAMpRHObYU1d4ClYARSkYRVSIz/HrGsyOsnzQNlHlSQsQGan45mVZa0mbsB0HqT7O1Vu5dKmYH/uGBkLncSLRlrlXzduwJMzd/HVNXN0YmNSttqh2B8yF1J+chUvPsalxqXMR/jtavScV3ra6bb8CgYEAyMU81fhsfSPxwNvOE/4hvy2WedWz6IyqSqTwCsUwVz9XfltFv34/dyJWM1JWhXj/HFMNII7yyrmtY/ytucpZJpL+McKXgVrtbp5PZgHImXBroeYuM8oYHVlb1VDzlY/RpKdrVwkQtUCiQYAqQ/jGO8A/7HVbHg284wywwfJTHIcCgYBF9EOxcmZtPt4+WV2uKS/Stp5OgiUmW5t7m4EUpYnDIHpODOosJ/61rTVeA4k7EdFOt+2BnZXvUY0cT4wjXSky2r92ZMfPIfWAaOks6cS5u0ugm5Suv9urw0L48hvBiZaV48Zb844A2o2KcH6WHYbbcOOzW9yeeCGurwUL3NFrhQKBgDuaUnUzSwHbqemXV5aX22zmG8YPq1leaS+/x4NrBu7f3xmj/xDaafz0NKMHzqfRQ+skvMDtryZ/+Pm2Cd0eNVk+Sjh7jH7af6JXXTrrTKJAvbNSomS9mmscHCc34RiFmbdH/JXaGxu0FHddlS6YrEEQ0elg7av+aBT/r3nkdsgBAoGBANpYeA+N5s5oL8fuUoYSx7yS5xLqcxx+y0ipF8DVKPEZDzEdF3QbxsjxUlkKNHquVCUywDfhy8DTtSYF2yvrTAda2ZtZ8BpgDzEI2Q1+uarERPkqIOGoQ+yykRP4QUyhtNIx/LStS7BzbV66VIEUashBMtp1vxln4Dthh8uyceHP\n-----END RSA PRIVATE KEY-----\n"


# allow easy selection of JSON result arrays by 'id'; assumed unique
class Array
  def by_id(id)
    find {|j| j['id'] == id}
  end
end

class ActionController::TestCase
  include Devise::Test::ControllerHelpers, HelperMethods, Taxonomies, OktaFederatedStubs, CardHelperMethods
  include RequestHelperMethods, OAuthStubs
  include WebMock::API

  setup do
    set_host
    @old_use_edcast_domain = ENV['USE_EDCAST_DOMAIN']
    ENV['USE_EDCAST_DOMAIN'] = 'true'
  end

  teardown do
    ENV['USE_EDCAST_DOMAIN'] = @old_use_edcast_domain
    reset_custom_headers
  end
end

class ActionDispatch::IntegrationTest
  include HelperMethods, OktaFederatedStubs, RequestHelperMethods

  setup do
    host! "www.test.host"
    @old_use_edcast_domain = ENV['USE_EDCAST_DOMAIN']
    ENV['USE_EDCAST_DOMAIN'] = 'true'
  end

  teardown do
    ENV['USE_EDCAST_DOMAIN'] = @old_use_edcast_domain
  end
end

#use a webmock compatible elastic search transport
ES_SEARCH_CLIENT.transport = Elasticsearch::Transport::Transport::HTTP::Faraday.new(hosts: [{host: 'localhost', port: '9200'}], options: { tracer: log, logger: log}) do |faraday|
  faraday.adapter(:net_http)
end

# monkeypatching minitest 5.5.x to retry a few times on elastic ServiceUnavailable
# RESCUE_EXCEPTIONS = [
#     Elasticsearch::Transport::Transport::Errors::ServiceUnavailable,
#     Searchkick::InvalidQueryError,
#     Faraday::ConnectionFailed,
#     Elasticsearch::Transport::Transport::Errors::NotFound
# ]
# module Minitest
#   class Test < Runnable
#     def run
#       with_info_handler do
#         time_it do
#           capture_exceptions do
#             begin
#               tries ||= 2

#               before_setup; setup; after_setup
#               run_one_method self, self.name, reporter
#             rescue *RESCUE_EXCEPTIONS => e
#               tries -= 1
#               if tries > 0
#                 TEARDOWN_METHODS.each do |hook|
#                   capture_exceptions do
#                     self.send hook
#                   end
#                 end
#                 # sleep 1
#                 retry
#               else
#                 raise e
#               end
#             end
#           end

#           TEARDOWN_METHODS.each do |hook|
#             capture_exceptions do
#               self.send hook
#             end
#           end
#         end
#       end

#       self # per contract
#     end
#   end
# end

# stuff to run whenever webmock is disabled (via disable! or allow_net_connect!)
module WebMockAfterDisabled
  def after_disabled
    UserContentsQueue.create_index!# force: true
    MetricRecord.create_index!# force: true
  end
  private :after_disabled

  def disable!
    ret = super
    after_disabled
    ret
  end

  def allow_net_connect!
    ret = super
    after_disabled
    ret
  end
end
WebMock.singleton_class.prepend WebMockAfterDisabled

def stub_commit_callbacks(klass)
  callbacks = klass._commit_callbacks.map(&:filter).select{|c| Symbol === c}
  callbacks.each &VideoStream.any_instance.method(:stubs)
  y = yield
  callbacks.each &VideoStream.any_instance.method(:unstub)
  y
end

def scrub(sql)
  sql
  .gsub(/[\"\`]/, '')
  .gsub(/\'t\'/, '1')
  .gsub(/\'f\'/, '0')
end

def reset_influxdb
  Rake::Task['admin:setup_influxdb'].invoke
end

def stub_time
  now = Time.now
  Time.stubs(:now).returns now
end

Minitest.after_run do
  if ParallelTests.first_process?
    ParallelTests.wait_for_other_processes_to_finish
    puts 'Cleaning up Paperclip assets'
    FileUtils.rm_rf(Rails.root.join("assets"))
  end
end

# use separate searchkick index (named using @env) for each parallel_tests process
module Searchkick
  def self.env
    @env ||= "test#{ENV['TEST_ENV_NUMBER']}"
  end
end

# searchkick wants @env == 'test' to limit shards, so we do that ourselves
module SearchkickTest
  def index_options
    options = super
    options[:settings].merge!(number_of_shards: 1, number_of_replicas: 0)
    return options
  end
end

module Searchkick
  class Index
    prepend SearchkickTest
  end
end

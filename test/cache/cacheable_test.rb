require 'test_helper'
class CacheableTest < ActiveSupport::TestCase
  class CacheableClass
    include Cacheable
  end
  
  class CacheableClassV2
    @@version = 2
    include Cacheable
  end
  
  class SomeRandomClass

  end
  class CacheWithValueClass
    include Cacheable
    def value
      {abc: abc, xyz: xyz, xxx: xxx, yyy: yyy}
    end
  end
  test 'initialize should only accept serializable arguments' do
    
    assert_instance_of CacheableClass, CacheableClass.new(abc: 123, xyz: 'abc')

    assert_instance_of CacheableClass, CacheableClass.new(abc: 123, xyz: ['1', '2', 3])

    assert_instance_of CacheableClass, CacheableClass.new(abc: '123', xyz: {a: '1', b: '2', c: 3})
    
    assert_raises Cacheable::UnSerializableKeyError do
      CacheableClass.new(abc: '123', xyz: SomeRandomClass.new)
    end
    
    assert_raises ArgumentError do
      CacheableClass.new('something')
    end
  end
  
  test 'should make the cache key' do
    assert_equal 'CacheableTest::CacheableClass-1-{abc:123,xyz:abc,xxx:[0,2,4]}', CacheableClass.new(abc: 123, xyz: 'abc', xxx: ['0', 2, 4]).key
    assert_equal 'CacheableTest::CacheableClass-1-{abc:123,xyz:abc}', CacheableClass.new(abc: 123, xyz: 'abc').key
    
    user = create(:user)
    assert_equal "CacheableTest::CacheableClass-1-{abc:123,xyz:abc,user:#{user.id}}", CacheableClass.new(abc: 123, xyz: 'abc', user: user).key
    assert_equal "CacheableTest::CacheableClassV2-2-{abc:123,xyz:abc,user:#{user.id}}", CacheableClassV2.new(abc: 123, xyz: 'abc', user: user).key
  end
  
  test 'not implemented' do
    assert_raise NotImplementedError do
      CacheableClass.new(abc: '123', xyz: 'abc').value
    end
  end
  
  test 'should generate the value' do
    cache = CacheWithValueClass.new(abc: '123', xyz: 'abc', xxx: [1,2,3], yyy: {a:1,b:2,c:3}) 
    assert_equal({abc: '123', xyz: 'abc', xxx: [1,2,3], yyy: {a:1,b:2,c:3}}, cache.value)
  end
end
require 'test_helper'

class UserInterests::PwcTest < ActiveSupport::TestCase
  setup do
    @default_topics = {"defaults" => [ {"topic_id" => "1","topic_label" => "Application Development"},
                      {"topic_id" => "2","topic_label" => "Big Data"},
                      {"topic_id" => "3","topic_label" => "Business Skills"},
                      {"topic_id" => "4","topic_label" => "Computer Networking"},
                      {"topic_id" => "5","topic_label" => "Customer Relationship Management (CRM)"},
                      {"topic_id" => "6","topic_label" => "Cybersecurity"},
                      {"topic_id" => "7","topic_label" => "Internet of Things"},
                      {"topic_id" => "8","topic_label" => "Project Management"}
                    ]}
    @org = create :organization
    @user = create(:user, organization: @org)
  end

  test 'should create pwc records with valid abbrs and update learning interests' do

    args = {"bs"=>"2", "cm"=>"3", "iot"=>"3", "cs"=>"2", "sd"=>"3", "bd"=>"2", "n"=>"2", "pm"=>"2", "topic"=>"cs"}
    UserInterests::Pwc.new(args, @user, @default_topics).process
    assert_equal 8, @user.pwc_records.length
    assert_not_empty @user.learning_topics
  end

  test "should not create pwc record if mapping is not present" do
    args = {"bssdd"=>"2", "topic"=>"bssdd"}
    UserInterests::Pwc.new(args, @user, @default_topics).process
    assert_empty @user.pwc_records
  end
end

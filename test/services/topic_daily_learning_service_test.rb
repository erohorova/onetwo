require 'test_helper'

class TopicDailyLearningServiceTest < ActiveSupport::TestCase
  test '#get_ecl_ids' do
    org = create(:organization)

    ecl_search = mock
    ecl_search.expects(:search).with(query: '*', limit: 100, offset: 0, filter_params: {
      topic: ['topic']
    }).returns(ecl_search_data)
    EclApi::EclSearch.expects(:new).with(org.id).returns(ecl_search)

    # Execute
    ecl_ids = TopicDailyLearningService.new.get_ecl_ids('topic', org.id, nil)
    assert_equal ecl_search_data[:results].map{|res| res['ecl_id']}, ecl_ids
  end

  private

  def ecl_search_data
    {
      results: [
        {
          'id' => nil, # non persisted card
          'ecl_id' => 'content_item1'
        },
        {
          'id' => 1, # persisted card
          'ecl_id' => 'content_item2'
        }
      ]
    }
  end
end

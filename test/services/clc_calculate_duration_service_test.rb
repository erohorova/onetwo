require 'test_helper'

class ClcCalculateDurationServiceTest < ActiveSupport::TestCase
  setup do
    @org1 = create(:organization)
    @user1= create(:user, organization: @org1)
    @author = create(:user, organization: @org1)
  end

  test "should return  calculated_duration in minutes if calculated_duration_display contains minutes" do
    ecl_metadata = { "duration_metadata":
                      { "calculated_duration":300,
                        "calculated_duration_display": "5 minutes"
                      }
                    }
    @card = create(:card, author: @author, organization: @org1, ecl_metadata: ecl_metadata)
    duration = ClcCalculateDurationService.get_duration(organization: @org1, card: @card)
    assert_equal 5, duration
  end

  test "should return  calculated_duration in minutes if calculated_duration_display contains hours" do
    ecl_metadata = { "duration_metadata":
                      { "calculated_duration":18000,
                        "calculated_duration_display": "5 hours"
                      }
                    }
    @card = create(:card, author: @author, organization: @org1, ecl_metadata: ecl_metadata)
    duration = ClcCalculateDurationService.get_duration(organization: @org1, card: @card)
    assert_equal 300, duration
  end

  test "should return  0 if calculated_duration_display contains less than an minute" do
    ecl_metadata = { "duration_metadata":
                      { "calculated_duration":45,
                        "calculated_duration_display": "less than a minute"
                      }
                    }
    @card = create(:card, author: @author, organization: @org1, ecl_metadata: ecl_metadata)
    duration = ClcCalculateDurationService.get_duration(organization: @org1, card: @card)
    assert_equal 0, duration
  end

  test "should return  calculated_duration in minutes if calculated_duration_display contains days and clc_minutes_for_day is 30 " do
    create(:config, name:'clc_minutes_for_day', configable: @org1, value: 30, data_type: 'integer', category: nil)
    ecl_metadata = { "duration_metadata":
                      { "calculated_duration":432000,
                        "calculated_duration_display": "5 days"
                      }
                    }
    @card = create(:card, author: @author, organization: @org1, ecl_metadata: ecl_metadata)
    duration = ClcCalculateDurationService.get_duration(organization: @org1, card: @card)
    assert_equal 150, duration
  end

  test "should return  calculated_duration in minutes if calculated_duration_display contains week and default clc_minutes_for_day is 60 " do
    ecl_metadata = { "duration_metadata":
                      { "calculated_duration":604800,
                        "calculated_duration_display": "1 week"
                      }
                    }
    @card = create(:card, author: @author, organization: @org1, ecl_metadata: ecl_metadata)
    duration = ClcCalculateDurationService.get_duration(organization: @org1, card: @card)
    assert_equal 420, duration
  end

  test "should return  calculated_duration in minutes if calculated_duration_display contains months and clc_minutes_for_day is 120 " do
    create(:config, name:'clc_minutes_for_day', configable: @org1, value: 120, data_type: 'integer', category: nil)
    ecl_metadata = { "duration_metadata":
                      { "calculated_duration":5184000,
                        "calculated_duration_display": "2 months"
                      }
                    }
    @card = create(:card, author: @author, organization: @org1, ecl_metadata: ecl_metadata)
    duration = ClcCalculateDurationService.get_duration(organization: @org1, card: @card)
    assert_equal 7200, duration
  end

  test "should return  calculated_duration in minutes if calculated_duration_display contains months and clc_minutes_for_day is 60 " do
    create(:config, name:'clc_minutes_for_day', configable: @org1, value: 60, data_type: 'integer', category: nil)
    ecl_metadata = { "duration_metadata":
                      { "calculated_duration":5184000,
                        "calculated_duration_display": "1 year"
                      }
                    }
    @card = create(:card, author: @author, organization: @org1, ecl_metadata: ecl_metadata)
    duration = ClcCalculateDurationService.get_duration(organization: @org1, card: @card)
    assert_equal 21900, duration
  end
end

require 'test_helper'

class TodaysLearning::V3Test < ActiveSupport::TestCase
  setup do
    ecl_results   = JSON.parse(File.read(File.join('test', 'fixtures', 'files', 'ecl_sociative_results.json')))
    EclApi::EclSearch.any_instance.stubs(:todays_learning).returns(ecl_results)

    Bookmark.any_instance.stubs(:add_to_learning_queue).returns(nil)
    DismissedContent.any_instance.stubs(:remove_item_from_queue).returns(nil)
    User.any_instance.stubs(:reindex_async).returns(nil)
    Card.any_instance.stubs(:reindex_async).returns(nil)
    Channel.any_instance.stubs(:after_followed).returns(nil)
    Channel.any_instance.stubs(:reindex_async).returns(nil)

    ecl_data = []
    5.times do |t|
      ecl_data << {
        'source_id' => SecureRandom.hex,
        'source_display_name' => "Display name #{t}",
        'source_type_name' => 'edcast_cloud',
        'duration_metadata' => nil
      }
    end

    @organization = create :organization
    @user         = create :user, organization: @organization
    @profile      = create :user_profile, user: @user

    @dismissible_card  = create :card, organization: @organization, ecl_id: 'S-YUEquw1L32Th2luktc6iAih==', ecl_metadata: ecl_data[0], author: nil
    @markable_card     = create :card, organization: @organization, ecl_id: 'S-YUEquw1L32Th2luktc6iAmq==', ecl_metadata: ecl_data[1], author: nil
    @bookmarkable_card = create :card, organization: @organization, ecl_id: 'S-YUEquw1L32Th2luktc6iAaq==', ecl_metadata: ecl_data[2], author: nil
    @learnable_card    = create :card, organization: @organization, ecl_id: 'S-YUEquw1L32Th2luktc6iAdf==', ecl_metadata: ecl_data[3], author: nil
    @trashable_card    = create :card, organization: @organization, ecl_id: 'S-YUEquw1L32Th2luktc6iAtg==', ecl_metadata: ecl_data[4], author: nil

    dismissed_card  = create :dismissed_content, content: @dismissible_card, user: @user
    bookmarked_card = create :bookmark, bookmarkable: @bookmarkable_card, user: @user
    completed_card  = create :user_content_completion, :completed, completed_at: Time.now, user: @user, completable: @markable_card

    options = { limit: 9, offset: 0 }

    @organization.stubs(:number_of_learning_items).returns(9)

    @klass  = TodaysLearning::V3.new(user: @user, opts: options)
  end

  test '#process returns sociative results' do
    results = @klass.process
    assert_equal 3, results.select { |r| r.ecl_id =~ /S-/ }.length
  end

  test '#process returns no results when no learning interests being set' do
    EclApi::EclSearch.any_instance.stubs(:todays_learning).returns({})
    @profile.update(learning_topics: nil)
    @profile.reload

    assert_empty @klass.process
  end

  test '#process rejects cards bookmarked by user' do
    results = @klass.process

    refute_includes results.map(&:ecl_id), @bookmarkable_card.ecl_id
  end

  test '#process rejects cards dismissed by user' do
    results = @klass.process

    refute_includes results.map(&:ecl_id), @dismissible_card.ecl_id
  end

  test '#process rejects cards completed by user' do
    results = @klass.process

    refute_includes results.map(&:ecl_id), @markable_card.ecl_id
  end

  test '#process rejects trashed cards' do
    # Trash the card
    @trashable_card.update_attributes(deleted_at: Time.now)
    create(:card_reporting, card: @trashable_card, user: @user, deleted_at: Time.now)
    results = @klass.process

    refute_includes results.map(&:ecl_id), @trashable_card.ecl_id
  end

  test '#process also returns physical sociative cards' do
    results = @klass.process

    physical_card = results.select { |r| r.ecl_id == @learnable_card.ecl_id }.first

    assert_equal @learnable_card.id, physical_card.id
  end

  test '#process also returns virtual sociative cards' do
    results = @klass.process

    virtual_card_1 = results.select { |r| r.ecl_id == 'S-YUEquw1L32Th2luktc6iAg==' }.first
    virtual_card_2 = results.select { |r| r.ecl_id == 'S-cadp6q8Iu1UhacYfl+b0xw==' }.first

    assert_nil virtual_card_1.id
    assert_nil virtual_card_2.id
  end

  test '#process re-fetch if cache has blank content' do
    cache_key = @klass.send :cache_key
    Rails.cache.expects(:fetch).with(cache_key).returns([])

    EclApi::EclSearch.any_instance.expects(:todays_learning).once

    @klass.process
  end

  test '#process DOES NOT re-fetch if cache has content' do
    expected_cards = []
    5.times do
      expected_cards << create(:card, author: @user, ecl_id: SecureRandom.hex)
    end

    cache_key = @klass.send :cache_key
    Rails.cache.expects(:fetch).with(cache_key).returns(expected_cards)

    EclApi::EclSearch.any_instance.expects(:todays_learning).never

    Rails.cache.expects(:fetch).with(@organization.send(:trashed_ecl_ids_cache_key), { expires_in: 30.days }).returns([])

    @klass.process
  end

  test '#process DOES NOT cache empty results' do
    cache_key = @klass.send :cache_key
    Rails.cache.expects(:fetch).with(cache_key).returns(nil)

    EclApi::EclSearch.any_instance.expects(:todays_learning).returns([])
    Rails.cache.expects(:write).with(cache_key).never

    @klass.process
  end
end

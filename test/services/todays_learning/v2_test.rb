require 'test_helper'

class TodaysLearning::V2Test < ActiveSupport::TestCase
  setup do
    @organization = create(:organization)
    options = { limit: 9, offset: 0 }
    @organization.stubs(:number_of_learning_items).returns(9)

    @user = create(:user, organization: @organization)
    @klass  = TodaysLearning::V2.new(user: @user, opts: options)
  end

  test '#process rejects trashed cards' do
    trashable_card = create(:card, organization: @organization, ecl_id: 'test-ecl-id-trashed', author: nil)
    cards = [trashable_card]

    ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items"]
    EclApi::EclSearch.any_instance.stubs(:search).returns(OpenStruct.new(results: cards, aggregations: ecl_response['aggregations']))

    # Trash the card
    trashable_card.update_attributes(deleted_at: Time.now)
    create(:card_reporting, card: trashable_card, user: @user, deleted_at: Time.now)

    results = @klass.process

    refute_includes results.map(&:ecl_id), trashable_card.ecl_id
  end
end

require 'test_helper'

class CloneRecordsServiceTest < ActiveSupport::TestCase
  setup do
    @org1, @org2 = create_list(:organization, 2)
    @user = create(:user, organization: @org1)
    @clone_record_service = CloneRecordsService.new(@org2.id)
  end

  class CloneSmartcardTest < CloneRecordsServiceTest
    setup do
      resource = create(:resource)
      @card1 = create(:card, author_id: @user.id, card_type: 'media', card_subtype: 'link',
        resource: resource, readable_card_type_name: 'book', provider_image: 'https://cdn.filestackcontent.com/KxaMgfERPugXDbAySGTi',
        provider: 'Example', votes_count: 4, comments_count: 10, organization: @org1)
      card_metadatum = create :card_metadatum, card: @card1, plan: 'free', level: 'beginner', average_rating: 5
      price1 = create :price, card: @card1, currency: 'USD', amount: 100, ecl_id: @card1.ecl_id
      price2 = create :price, card: @card1, currency: 'INR', amount: 1000, ecl_id: @card1.ecl_id
    end

    test "should clone card with its associations" do
      clone_card = @clone_record_service.run(@card1)

      assert_equal @card1.id, clone_card.super_card_id
      assert_equal @org2.id, clone_card.organization_id
      assert_equal true, clone_card.prices.present?
      assert_equal true, clone_card.card_metadatum.present?
      assert_equal true, clone_card.resource.present?
    end

    test "should clone videostream and associated card" do
      video_card = create(:card, organization: @org1, author_id: @user.id)
      create(:wowza_video_stream, organization_id: @org1.id, creator: @user, card: video_card)
      clone_card = @clone_record_service.run(video_card)

      assert_equal video_card.id, clone_card.super_card_id
      assert_equal @org2.id, clone_card.organization_id
      assert_equal true, clone_card.video_stream.present?
    end

    test "should clone course card" do
      course_card = create(:card, author_id: @user.id, card_type: 'course', card_subtype: 'link', resource_id: @card1.resource.id)
      clone_card = @clone_record_service.run(course_card)

      assert_equal course_card.id, clone_card.super_card_id
      assert_equal @org2.id, clone_card.organization_id
    end

    test "should not clone project card" do
      project_card = create(:card, card_type: 'project', card_subtype: 'text', author_id: @user.id, 
        title: 'This is project card', message: 'This is test message for project card', organization_id: @org1.id)
      clone_card = @clone_record_service.run(project_card)

      assert_equal false, clone_card
    end

    test "should not clone private cards" do
      private_card = create(:card, card_type: 'media', card_subtype: 'text', author_id: @user.id, title: 'This is private card', 
        message: 'This is message', is_public: false, organization: @org1)
      clone_card = @clone_record_service.run(private_card)

      assert_equal false, clone_card
    end

    test "should retrun already created clone card if organization already has clone card" do
      clone_card = @clone_record_service.run(@card1)
      
      # again try to clone parent card in same organization
      clone_card_dup = @clone_record_service.run(@card1)
      assert_equal clone_card.id, clone_card_dup.id
      assert_equal clone_card.super_card_id, clone_card_dup.super_card_id
      assert_equal clone_card.organization_id, clone_card_dup.organization_id
    end
  end

  class ClonePathwayTest < CloneRecordsServiceTest
    setup do
      TestAfterCommit.enabled = true
    end

    test "should clone pathway with its associations" do
      cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')
      cards = create_list(:card, 2, author_id: @user.id, card_type: 'media')
      poll = create(:card, card_type: 'poll', card_subtype: 'text')
      cards << poll
      cards.each do |card|
        CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
      end
      create(:leap, card: poll, pathway_id: cover.id, correct_id: cards.first.id, wrong_id: cards.last.id)
      cover.publish!
      clone_card = @clone_record_service.run(cover)
      clone_leap = Leap.find_by(pathway_id: clone_card.id)
      clone_cards_super_card_ids = Card.where(id: clone_card.pack_relns.map(&:from_id)).pluck(:super_card_id)

      assert_equal cover.id, clone_card.super_card_id
      assert_equal cover.state, clone_card.state
      assert_equal @org2.id, clone_card.organization_id
      assert_equal 4, clone_card.pack_relns.length
      assert_same_elements [cover.id, cards.map(&:id)].flatten, clone_cards_super_card_ids
      assert_equal true, clone_leap.present?
    end

    test "should clone curriculum with its associations" do
      cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')
      cards = create_list(:card, 2, author_id: @user.id, card_type: 'media')
      cover_journey = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: @user, organization: @org)
      section = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user, organization: @org)
      cards = create_list(:card, 2, author_id: @user.id, card_type: 'media')
      cards.each do |card|
        CardPackRelation.add(cover_id: section.id, add_id: card.id, add_type: 'Card')
      end
      JourneyPackRelation.add(cover_id: cover_journey.id, add_id: section.id)
      cover_journey.publish!
      cards << cover_journey
      cards.each do |card|
        CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
      end
      cover.publish!
      clone_card = @clone_record_service.run(cover)
      clone_cards_super_card_ids = Card.where(id: clone_card.pack_relns.map(&:from_id)).pluck(:super_card_id)

      assert_equal cover.id, clone_card.super_card_id
      assert_equal cover.state, clone_card.state
      assert_equal @org2.id, clone_card.organization_id
      assert_equal 4, clone_card.pack_relns.length
      assert_same_elements [cover.id, cards.map(&:id)].flatten, clone_cards_super_card_ids
    end

    test "should not clone pathway if it has private cards" do
      cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')
      cards = create_list(:card, 2, author_id: @user.id, card_type: 'media', is_public: false)
      cards.each do |card|
        CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
      end
      clone_card = @clone_record_service.run(cover)

      assert_equal false, clone_card
    end

    test "should clone pathway if it has non-ugc cards" do
      cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')
      card = create(:card, author_id: nil, organization: @org1, card_type: 'media', ecl_id: 'non-ugc-124')
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
      clone_card = @clone_record_service.run(cover)

      assert_equal cover.id, clone_card.super_card_id
      assert_equal @org2.id, clone_card.organization_id
    end

    test "should not clone pathway if it has ugc private cards and non-ugc cards" do
      cover = create(:card, message: 'some message', card_type: 'pack', author_id: @user.id, title: 'pack cover')
      cards = [create(:card, author_id: @user.id, card_type: 'media', is_public: false)]
      cards << create(:card, author_id: nil, organization: @org1, card_type: 'media', ecl_id: 'non-ugc-126')
      cards.each do |card|
        CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
      end
      clone_card = @clone_record_service.run(cover)

      assert_equal false, clone_card
    end
  end

  class CloneJourneyTest < CloneRecordsServiceTest
    setup do
      TestAfterCommit.enabled = true
    end
    
    test "should clone journey with its associations" do
      cover_journey = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: @user, organization: @org)
      section = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user, organization: @org)
      cards = create_list(:card, 2, author_id: @user.id, card_type: 'media')
      cards.each do |card|
        CardPackRelation.add(cover_id: section.id, add_id: card.id, add_type: 'Card')
      end
      JourneyPackRelation.add(cover_id: cover_journey.id, add_id: section.id)
      cover_journey.publish!
      clone_card = @clone_record_service.run(cover_journey)

      assert_equal cover_journey.id, clone_card.super_card_id
      assert_equal cover_journey.state, clone_card.state
      assert_equal @org2.id, clone_card.organization_id
      assert_equal 2, clone_card.journey_relns.length
    end

    test "should not clone journey if it has private cards" do
      cover_journey = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: @user, organization: @org)
      section = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user, organization: @org)
      cards = create_list(:card, 2, author_id: @user.id, card_type: 'media', is_public: false)
      cards.each do |card|
        CardPackRelation.add(cover_id: section.id, add_id: card.id, add_type: 'Card')
      end
      JourneyPackRelation.add(cover_id: cover_journey.id, add_id: section.id)
      clone_card = @clone_record_service.run(cover_journey)

      assert_equal false, clone_card
    end

    test "should clone journey if it has non-ugc course cards" do
      cover_journey = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: @user, organization: @org)
      section = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user, organization: @org)
      card = create(:card, author_id: nil, organization: @org1, card_type: 'course', ecl_id: 'non-ugc-129')
      CardPackRelation.add(cover_id: section.id, add_id: card.id, add_type: 'Card')
      JourneyPackRelation.add(cover_id: cover_journey.id, add_id: section.id)
      clone_card = @clone_record_service.run(cover_journey)

      assert_equal cover_journey.id, clone_card.super_card_id
      assert_equal @org2.id, clone_card.organization_id
      assert_equal 2, clone_card.journey_relns.length
    end
  end

  class CloneChannelCardsTest < CloneRecordsServiceTest
    setup do
      TestAfterCommit.enabled = true
      @org1_channel = create(:channel, organization: @org1, shareable: true)
    end

    test "should clone channel cards if channel is cloned" do
      cards = create_list(:card, 2, author_id: @user.id, card_type: 'media', is_public: true, ecl_source_name: 'UGC')
      cards.each do |card|
        card.current_user = @user
        card.channels << @org1_channel
      end
      org2_clone_channel = create(:channel, organization: @org2, parent_id: @org1_channel.id)
      CloneChannelCardsJob.perform_now(@org1_channel.id, org2_clone_channel.id)
      clone_cards_super_card_ids = Card.where(id: org2_clone_channel.channels_cards.pluck(:card_id)).pluck(:super_card_id)

      assert_equal true, org2_clone_channel.channels_cards.exists?
      assert_same_elements cards.map(&:id), clone_cards_super_card_ids
    end

    test "should only clone public channel cards if channel is cloned" do
      cards = create_list(:card, 2, author_id: @user.id, card_type: 'media', is_public: true, ecl_source_name: 'UGC')
      cards.each do |card|
        card.current_user = @user
        card.channels << @org1_channel
      end
      private_card = create(:card, card_type: 'media', author_id: @user.id, title: 'private card', is_public: false, organization: @org1)
      private_card.current_user = @user
      private_card.channels << @org1_channel
      org2_clone_channel = create(:channel, organization: @org2, parent_id: @org1_channel.id)
      CloneChannelCardsJob.perform_now(@org1_channel.id, org2_clone_channel.id)
      clone_cards_super_card_ids = Card.where(id: org2_clone_channel.channels_cards.pluck(:card_id)).pluck(:super_card_id)

      assert_same_elements cards.map(&:id), clone_cards_super_card_ids
      assert_equal nil, Card.find_by(super_card_id: private_card.id, organization_id: @org2.id)
    end

    test "should only clone ugc/non-ugc public channel cards if channel is cloned" do
      cards = create_list(:card, 2, author_id: @user.id, card_type: 'media', is_public: true, ecl_source_name: 'UGC')
      cards.each do |card|
        card.current_user = @user
        card.channels << @org1_channel
      end
      non_ugc_private_card = create(:card, author_id: nil, organization: @org1, is_public: false, ecl_id: 'non-ugc-123')
      non_ugc_private_card.current_user = @user
      non_ugc_private_card.channels << @org1_channel
      org2_clone_channel = create(:channel, organization: @org2, parent_id: @org1_channel.id)
      CloneChannelCardsJob.perform_now(@org1_channel.id, org2_clone_channel.id)
      clone_cards_super_card_ids = Card.where(id: org2_clone_channel.channels_cards.pluck(:card_id)).pluck(:super_card_id)

      assert_same_elements cards.map(&:id), clone_cards_super_card_ids
      assert_equal nil, Card.find_by(super_card_id: non_ugc_private_card.id, organization_id: @org2.id)
    end
  end
end
require 'test_helper'

class ScormUploadStatusServiceTest < ActiveJob::TestCase
  setup do
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @resource = create(:resource)
    @scorm_card = create(:card, author: @user, organization: @organization, state: 'processing',
      resource_id: @resource.id, filestack: [scorm_course: true,
                    url: 'https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv'])
    @scorm_detail = create(:scorm_detail, status: 'running', token: 'randomtoken000', card: @scorm_card)
  end

  test 'should set status to finished after successful import' do
    upload_status = { status: 'finished', message: 'Import Successful' }
    ScormUploadStatusService.any_instance.stubs(:get_status).returns(upload_status)
    ScormUploadStatusService.new.run(@scorm_card.id)
    @scorm_detail.reload
    assert_equal 'finished', @scorm_detail.status
    assert_equal 'Import Successful', @scorm_detail.message
  end

  test 'should set status to error after un-successful import' do
    upload_status = { status: 'error', message: 'Upload error' }
    ScormUploadStatusService.any_instance.stubs(:get_status).returns(upload_status)
    ScormUploadStatusService.new.run(@scorm_card.id)
    @scorm_card.reload
    @scorm_detail.reload
    assert_equal 'error', @scorm_detail.status
    assert_equal 'Upload error', @scorm_detail.message
    assert_equal 'error', @scorm_card.state
  end

  test 'should set status to running and invoke GetScormUploadStatusJob again' do
    GetScormUploadStatusJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later
    upload_status = { status: 'running', message: '' }
    assert_no_enqueued_jobs do
      GetScormUploadStatusJob.perform_now
    end
    ScormUploadStatusService.any_instance.stubs(:get_status).returns(upload_status)
    assert_enqueued_with(job: GetScormUploadStatusJob, args: [@scorm_card.id]) do
      ScormUploadStatusService.new.run(@scorm_card.id)
    end
    @scorm_detail.reload
    assert_equal 'running', @scorm_detail.status
    assert_empty @scorm_detail.message
  end
end
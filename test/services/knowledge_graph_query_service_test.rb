require 'test_helper'

class KnowledgeGraphQueryServiceTest < ActiveSupport::TestCase

  setup do
    @klass = KnowledgeGraphQueryService

    # Create two orgs. The second will not have data returned by the queries.
    @org1, @org2 = create_list(:organization, 2)

    # Create three users per org
    @org1_users = create_list(:user, 3, organization: @org1)
    @org2_users = create_list(:user, 3, organization: @org2)
    @org1_user1, @org1_user2, @org1_user3 = @org1_users
    @org2_user1, @org2_user2, @org2_user3 = @org2_users

    # Create a team for each org and add each org's first two users to them
    # This is used to test the automatic team membership filter.
    @org1_team = create(:team, organization: @org1)
    @org2_team = create(:team, organization: @org2)
    TeamsUser.create!(team: @org1_team, user: @org1_user1, as_type: "member")
    TeamsUser.create!(team: @org1_team, user: @org1_user2, as_type: "member")
    TeamsUser.create!(team: @org2_team, user: @org2_user1, as_type: "member")
    TeamsUser.create!(team: @org2_team, user: @org2_user2, as_type: "member")

    # Add @org1_user1 to another team, used to test team_ids param
    @other_team = create(:team, organization: @org1)
    TeamsUser.create!(team: @other_team, user: @org1_user1, as_type: "member")

    # Create 3 UsersTopicsActivity records per user, over the past 3 days.
    # Each record has activity in 5 different topics.
    @base_time = Time.now.utc.beginning_of_day
    [*@org1_users, *@org2_users].each.with_index do |user, user_idx|
      (1..3).each do |num_days_ago|
        start_time   = (@base_time - num_days_ago.days).beginning_of_day
        end_time     = start_time.end_of_day
        topic_counts = {}
        total_count = 0
        (1..5).each do |topic_num|
          topic_name = "topic_#{topic_num}"
          topic_counts[topic_name] = topic_num + user_idx
          total_count += topic_num + user_idx
        end
        UsersTopicsActivity.create!(
          organization_id:        user.organization_id,
          user_id:                user.id,
          start_time:             start_time.to_i,
          end_time:               end_time.to_i,
          topic_counts:           topic_counts,
          total_activities_count: total_count
        )
      end
    end
  end

  test "query with minimum required params" do
    # We query for the past 2 days worth. This excludes one day of records.
    start_time = (@base_time - 2.days).beginning_of_day
    end_time   = @base_time.end_of_day

    # Run the query
    result = @klass.run(
      start_time:      start_time.to_i,
      end_time:        end_time.to_i,
      org_id:          @org1.id,
      num_top_topics:  3,
      num_top_users:   100,
      current_user_id: @org1_user1.id,
      is_admin:        false,
    )
    expected = {
      users: {
        @org1_user1.id => {
          name:   @org1_user1.full_name,
          handle: @org1_user1.handle,
          groups: [
            { id: @org1_team.id, name: @org1_team.name },
            { id: @other_team.id, name: @other_team.name },
          ],
          topics: [
            { name: "topic_5", count: 10 },
            { name: "topic_4", count: 8 },
            { name: "topic_3", count: 6 },
          ]
        },
        @org1_user2.id => {
          name:   @org1_user2.full_name,
          handle: @org1_user2.handle,
          groups: [
            { id: @org1_team.id, name: @org1_team.name },
          ],
          topics: [
            { name: "topic_5", count: 12 },
            { name: "topic_4", count: 10 },
            { name: "topic_3", count: 8 },
          ]
        }
      },
      groups: {
        @org1_team.id => {
          users: [
            { id: @org1_user1.id },
            { id: @org1_user2.id },
          ]
        },
        @other_team.id => {
          users: [
            { id: @org1_user1.id },
          ]
        }
      },
      topics: {
        "topic_5" => {
          users: [
            { id: @org1_user2.id, count: 12 },
            { id: @org1_user1.id, count: 10 },
          ]
        },
        "topic_4" => {
          users: [
            { id: @org1_user2.id, count: 10 },
            { id: @org1_user1.id, count: 8 },
          ]
        },
        "topic_3" => {
          users: [
            { id: @org1_user2.id, count: 8 },
            { id: @org1_user1.id, count: 6 },
          ]
        }
      }
    }
    assert_equal expected, result
  end

  test "query with admin user applies no default team filter" do
    # Because is_admin: true, and even though they have no teams,
    # org1_user3 will be included in the results because 
    # they are in the same org as the current user (@org1_user1)

    # We query for the past 2 days worth. This excludes one day of records.
    start_time = (@base_time - 2.days).beginning_of_day
    end_time   = @base_time.end_of_day

    # Run the query
    result = @klass.run(
      start_time:      start_time.to_i,
      end_time:        end_time.to_i,
      org_id:          @org1.id,
      num_top_topics:  3,
      num_top_users:   100,
      current_user_id: @org1_user1.id,
      is_admin:        true,
    )
    expected = {
      users: {
        @org1_user1.id => {
          name:   @org1_user1.full_name,
          handle: @org1_user1.handle,
          groups: [
            { id: @org1_team.id, name: @org1_team.name },
            { id: @other_team.id, name: @other_team.name },
          ],
          topics: [
            { name: "topic_5", count: 10 },
            { name: "topic_4", count: 8 },
            { name: "topic_3", count: 6 },
          ]
        },
        @org1_user2.id => {
          name:   @org1_user2.full_name,
          handle: @org1_user2.handle,
          groups: [
            { id: @org1_team.id, name: @org1_team.name },
          ],
          topics: [
            { name: "topic_5", count: 12 },
            { name: "topic_4", count: 10 },
            { name: "topic_3", count: 8 },
          ]
        },
        @org1_user3.id => {
          name:   @org1_user3.full_name,
          handle: @org1_user3.handle,
          groups: [],
          topics: [
            { name: "topic_5", count: 14 },
            { name: "topic_4", count: 12 },
            { name: "topic_3", count: 10 },
          ]
        }
      },
      groups: {
        @org1_team.id => {
          users: [
            { id: @org1_user1.id },
            { id: @org1_user2.id },
          ]
        },
        @other_team.id => {
          users: [
            { id: @org1_user1.id },
          ]
        }
      },
      topics: {
        "topic_5" => {
          users: [
            { id: @org1_user3.id, count: 14 },
            { id: @org1_user2.id, count: 12 },
            { id: @org1_user1.id, count: 10 },
          ]
        },
        "topic_4" => {
          users: [
            { id: @org1_user3.id, count: 12 },
            { id: @org1_user2.id, count: 10 },
            { id: @org1_user1.id, count: 8 },
          ]
        },
        "topic_3" => {
          users: [
            { id: @org1_user3.id, count: 10 },
            { id: @org1_user2.id, count: 8 },
            { id: @org1_user1.id, count: 6 },
          ]
        }
      }
    }
    assert_equal expected, result
  end

  test "admins can still apply custom team filter" do
    # We query for the past 2 days worth. This excludes one day of records.
    start_time = (@base_time - 2.days).beginning_of_day
    end_time   = @base_time.end_of_day

    # Run the query
    result = @klass.run(
      start_time:      start_time.to_i,
      end_time:        end_time.to_i,
      org_id:          @org1.id,
      team_ids:        [@other_team.id],
      num_top_topics:  3,
      num_top_users:   100,
      current_user_id: @org1_user1.id,
      is_admin:        true,
    )
    expected = {
      users: {
        @org1_user1.id => {
          name:   @org1_user1.full_name,
          handle: @org1_user1.handle,
          groups: [
            { id: @other_team.id, name: @other_team.name },
          ],
          topics: [
            { name: "topic_5", count: 10 },
            { name: "topic_4", count: 8 },
            { name: "topic_3", count: 6 },
          ]
        }
      },
      groups: {
        @other_team.id => {
          users: [
            { id: @org1_user1.id },
          ]
        }
      },
      topics: {
        "topic_5" => {
          users: [
            { id: @org1_user1.id, count: 10 },
          ]
        },
        "topic_4" => {
          users: [
            { id: @org1_user1.id, count: 8 },
          ]
        },
        "topic_3" => {
          users: [
            { id: @org1_user1.id, count: 6 },
          ]
        }
      }
    }
    assert_equal expected, result
  end


  test "query with custom num_top_topics" do
    # We query for the past 2 days worth. This excludes one day of records.
    start_time = (@base_time - 2.days).beginning_of_day
    end_time   = @base_time.end_of_day

    # Run the query
    result = @klass.run(
      start_time:      start_time.to_i,
      end_time:        end_time.to_i,
      org_id:          @org1.id,
      num_top_topics:  2,
      num_top_users:   100,
      current_user_id: @org1_user1.id,
      is_admin:        false,
    )

    expected = {
      users: {
        @org1_user1.id => {
          name:   @org1_user1.full_name,
          handle: @org1_user1.handle,
          groups: [
            { id: @org1_team.id, name: @org1_team.name },
            { id: @other_team.id, name: @other_team.name },
          ],
          topics: [
            { name: "topic_5", count: 10 },
            { name: "topic_4", count: 8 },
          ]
        },
        @org1_user2.id => {
          name:   @org1_user2.full_name,
          handle: @org1_user2.handle,
          groups: [
            { id: @org1_team.id, name: @org1_team.name },
          ],
          topics: [
            { name: "topic_5", count: 12 },
            { name: "topic_4", count: 10 },
          ]
        }
      },
      groups: {
        @org1_team.id => {
          users: [
            { id: @org1_user1.id },
            { id: @org1_user2.id },
          ]
        },
        @other_team.id => {
          users: [
            { id: @org1_user1.id },
          ]
        }
      },
      topics: {
        "topic_5" => {
          users: [
            { id: @org1_user2.id, count: 12 },
            { id: @org1_user1.id, count: 10 },
          ]
        },
        "topic_4" => {
          users: [
            { id: @org1_user2.id, count: 10 },
            { id: @org1_user1.id, count: 8 },
          ]
        }
      }
    }

    assert_equal expected, result
  end

  test "query with custom num_top_users" do
    # We query for the past 2 days worth. This excludes one day of records.
    start_time = (@base_time - 2.days).beginning_of_day
    end_time   = @base_time.end_of_day

    # Run the query
    result = @klass.run(
      start_time:      start_time.to_i,
      end_time:        end_time.to_i,
      org_id:          @org1.id,
      num_top_topics:  3,
      num_top_users:   1,
      current_user_id: @org1_user1.id,
      is_admin:        false,
    )
    expected = {
      users: {
        @org1_user2.id => {
          name:   @org1_user2.full_name,
          handle: @org1_user2.handle,
          groups: [
            { id: @org1_team.id, name: @org1_team.name },
          ],
          topics: [
            { name: "topic_5", count: 12 },
            { name: "topic_4", count: 10 },
            { name: "topic_3", count: 8 },
          ]
        }
      },
      groups: {
        @org1_team.id => {
          users: [
            { id: @org1_user2.id },
          ]
        },
      },
      topics: {
        "topic_5" => {
          users: [
            { id: @org1_user2.id, count: 12 },
          ]
        },
        "topic_4" => {
          users: [
            { id: @org1_user2.id, count: 10 },
          ]
        },
        "topic_3" => {
          users: [
            { id: @org1_user2.id, count: 8 },
          ]
        }
      }
    }
    assert_equal expected, result
  end

  test 'query with team filter' do
    # We query for the past 2 days worth. This excludes one day of records.
    start_time = (@base_time - 2.days).beginning_of_day
    end_time   = @base_time.end_of_day

    # Run the query, filtering by membership in the team.
    # only org1_user1 will be returned
    result = @klass.run(
      start_time:      start_time.to_i,
      end_time:        end_time.to_i,
      org_id:          @org1.id,
      team_ids:        [@other_team.id],
      num_top_topics:  3,
      num_top_users:   100,
      current_user_id: @org1_user1.id,
      is_admin:        false,
    )

    expected = {
      users: {
        @org1_user1.id => {
          name:   @org1_user1.full_name,
          handle: @org1_user1.handle,
          groups: [
            { id: @other_team.id, name: @other_team.name },
          ],
          topics: [
            { name: "topic_5", count: 10 },
            { name: "topic_4", count: 8 },
            { name: "topic_3", count: 6 },
          ]
        },
      },
      groups: {
        @other_team.id => {
          users: [
            { id: @org1_user1.id },
          ]
        }
      },
      topics: {
        "topic_5" => {
          users: [
            { id: @org1_user1.id, count: 10 },
          ]
        },
        "topic_4" => {
          users: [
            { id: @org1_user1.id, count: 8 },
          ]
        },
        "topic_3" => {
          users: [
            { id: @org1_user1.id, count: 6 },
          ]
        }
      }
    }

    assert_equal expected, result
  end

  test 'query dedupes topic names' do
    # We query for the past 2 days worth. This excludes one day of records.
    start_time = (@base_time - 2.days).beginning_of_day
    end_time   = @base_time.end_of_day

    # Rename all topic_4 instances to aliases of topic_3.
    # They will be combined in the results.
    UsersTopicsActivity.find_each do |activity|
      activity.update(
        topic_counts: activity.topic_counts.transform_keys do |key|
          {
            "topic_3" => "SUBTYPE_A.topic_3",
            "topic_4" => "SUBTYPE_B.topic_3"
          }.fetch(key, key)
        end
      )
    end

    # Run the query, filtering by membership in the team.
    # only org1_user1 will be returned
    result = @klass.run(
      start_time:      start_time.to_i,
      end_time:        end_time.to_i,
      org_id:          @org1.id,
      team_ids:        [@other_team.id],
      num_top_topics:  3,
      num_top_users:   100,
      current_user_id: @org1_user1.id,
      is_admin:        false,
    )

    expected = {
      users: {
        @org1_user1.id => {
          name:   @org1_user1.full_name,
          handle: @org1_user1.handle,
          groups: [
            { id: @other_team.id, name: @other_team.name },
          ],
          topics: [
            { name: "topic_3", count: 6 + 8 }, # topic_3 + topic_4
            { name: "topic_5", count: 10 },
            { name: "topic_2", count: 4 },
          ]
        },
      },
      groups: {
        @other_team.id => {
          users: [
            { id: @org1_user1.id },
          ]
        }
      },
      topics: {
        "topic_5" => {
          users: [
            { id: @org1_user1.id, count: 10 },
          ]
        },
        "topic_3" => {
          users: [
            { id: @org1_user1.id, count: 6 + 8 }, # topic_3 + topic_4
          ]
        },
        "topic_2" => {
          users: [
            { id: @org1_user1.id, count: 4 }
          ]
        }
      }
    }

    assert_equal expected, result
  end

  test 'query with user ids filter' do
    # We query for the past 2 days worth. This excludes one day of records.
    start_time = (@base_time - 2.days).beginning_of_day
    end_time   = @base_time.end_of_day

    # Run the query, requesting only data from org1_user2
    result = @klass.run(
      start_time:      start_time.to_i,
      end_time:        end_time.to_i,
      org_id:          @org1.id,
      user_ids:        [@org1_user2.id],
      num_top_topics:  3,
      num_top_users:   100,
      current_user_id: @org1_user1.id,
      is_admin:        false,
    )

    expected = {
      users: {
        @org1_user2.id => {
          name:   @org1_user2.full_name,
          handle: @org1_user2.handle,
          groups: [
            { id: @org1_team.id, name: @org1_team.name },
          ],
          topics: [
            { name: "topic_5", count: 12 },
            { name: "topic_4", count: 10 },
            { name: "topic_3", count: 8 },
          ]
        }
      },
      groups: {
        @org1_team.id => {
          users: [
            { id: @org1_user2.id },
          ]
        },
      },
      topics: {
        "topic_5" => {
          users: [
            { id: @org1_user2.id, count: 12 },
          ]
        },
        "topic_4" => {
          users: [
            { id: @org1_user2.id, count: 10 },
          ]
        },
        "topic_3" => {
          users: [
            { id: @org1_user2.id, count: 8 },
          ]
        }
      }
    }

    assert_equal expected, result
  end


end
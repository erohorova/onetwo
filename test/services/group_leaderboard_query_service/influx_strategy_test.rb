require 'test_helper'

class GroupLeaderboardQueryService::InfluxStrategyTest < ActiveSupport::TestCase

  setup do
    @class = GroupLeaderboardQueryService::InfluxStrategy
    @version = Settings.influxdb.event_tracking_version
  end

  test 'Measurement' do
    assert_equal "group_user_scores_daily", @class::Measurement
  end

  test '.evaluate_query' do
    query = "SELECT FOO FROM BAR PLEASE"
    params = { foo: "bar" }
    # Stub the result to return some data
    stub_result_1 = [
      { "values" => [1,2,3] }
    ]
    InfluxQuery.expects(:fetch).with(query, params).returns stub_result_1
    result = @class.evaluate_query([query, params])
    assert_equal [1,2,3], result
    # Stub the result to return no data
    stub_result_2 = []
    InfluxQuery.expects(:fetch).with(query, params).returns stub_result_2
    result = @class.evaluate_query([query, params])
    assert_equal [], result
  end

  test 'prepare_query' do
    time = Time.now
    Time.stubs(:now).returns time
    query, params = @class.prepare_query(
      start_date: 1,
      end_date: 2,
      group_ids: [3,4],
      org_ids: [5],
      limit: 6,
      offset: 7
    )
    expected_query = <<-TXT.chomp.strip_heredoc.tr("\n", " ")
      select * from \"group_user_scores_daily\"
      where org_id = %{organization_id}
      AND time >= %{start_date}s
      AND time <= %{end_date}s
      AND analytics_version = %{version}
      AND (group_id = %{group_id_0} OR group_id = %{group_id_1})
      ORDER BY ASC
      LIMIT %{limit}
      OFFSET %{offset} ;
    TXT
    assert_equal expected_query, query
    expected_params = {
      order: "ASC",
      start_date: 1,
      end_date: 2,
      group_id_0: 3,
      group_id_1: 4,
      organization_id: '5',
      limit: 6,
      offset: 7,
      version: @version
    }
    assert_equal expected_params, params
  end

end
require 'test_helper'

class GroupLeaderboardQueryService::MongoStrategyTest < ActiveSupport::TestCase

  setup do
    skip # SKIP THIS WHOLE FILE!
    @class = GroupLeaderboardQueryService::MongoStrategy
  end

  test 'CacheKey' do
    assert_equal(
      @class::CacheKey,
      Analytics::GroupScoreAggregationRecorder::LeaderboardMongoKey
    )
  end

  test 'Ref' do
    ref = @class::Ref
    assert_equal Mongo::Collection, ref.class
    assert_equal(ref.namespace, "#{Rails.env}.#{@class::CacheKey}")
  end

  test 'Indexer' do
    indexes = OpenStruct.new
    @class::Ref.expects(:indexes).returns indexes
    indexes.expects(:create_many).with([
      { key: { user_id: 1 } },
      { key: { group_id: 1 } },
      { key: { org_id: 1 } }
    ])
    @class::Indexer.run!
  end

  test '.evaluate_query' do
    # The 'cursor' object that gets passed here is iterable
    cursor = @class::Ref.find
    assert_equal Mongo::Collection::View, cursor.class
    assert cursor.respond_to? :to_a

    # Test the method by passing in a generic iterable
    range = (1..10)
    enum = range.each
    assert_equal range.to_a, @class.evaluate_query(enum)
  end

  test '.paginate' do
    filters = {foo: "filter"}
    opts = {foo: "opts"}
    cursor = OpenStruct.new
    limit_cursor = OpenStruct.new
    offset_cursor = OpenStruct.new
    @class::Ref.expects(:find).with(filters).returns cursor
    @class.expects(:with_offset).with(cursor, opts).returns offset_cursor
    @class.expects(:with_limit).with(offset_cursor, opts).returns limit_cursor
    assert_equal limit_cursor, @class.paginate(filters, opts)
  end

  test '.combine_filters with given args' do
    opts = {
      user_ids:  [1],
      group_ids: [2],
      org_ids: [3],
      start_date: 4,
      end_date: 5
    }
    # These are just fake filters, not end-to-end values
    expected = {
      user_id: { "$in" => [1] },
      group_id: { "$in" => [2] },
      org_id: { "$in" => [3] },
      time: { "$gte" => 4, "$lte" => 5 }
    }
    actual = @class.combine_filters(opts)
    assert_equal expected, actual
  end

  test '.combine_filters with default args' do
    opts = {}
    assert_equal({}, @class.combine_filters(opts))
  end

  test '.filter_by_date when just start date given' do
    start_date = (Time.now - 3.days).to_s
    expected = { time: {"$gte" => start_date } }
    actual = @class.filter_by_date(start_date, nil)
    assert_equal expected, actual
  end

  test '.filter_by_date when just end date given' do
    end_date = (Time.now - 2.days).to_s
    expected = { time: {"$lte" => end_date } }
    actual = @class.filter_by_date(nil, end_date)
    assert_equal expected, actual
  end

  test '.filter_by_date when start and end date given' do
    start_date = (Time.now - 3.days).to_s
    end_date = (Time.now - 2.days).to_s
    expected = { time: {"$lte" => end_date, "$gte" => start_date } }
    actual = @class.filter_by_date(start_date, end_date)
    assert_equal expected, actual
  end

  test '.filter_by_date when no date given' do
    assert_equal({}, @class.filter_by_date(nil, nil))
  end

  test '.filter_by_user when user_ids is nonempty' do
    user_ids = [1,2,3]
    expected = { user_id: { "$in" => user_ids } }
    actual = @class.send :filter_by_user, user_ids
    assert_equal expected, actual
  end

  test '.filter_by_user when user_ids is empty' do
    user_ids = []
    assert_equal({}, @class.filter_by_user(user_ids))
  end

  test '.filter_by_group when group_ids is nonempty' do
    group_ids = [1,2,3]
    expected = { group_id: { "$in" => group_ids } }
    actual = @class.filter_by_group(group_ids)
    assert_equal expected, actual
  end

  test '.filter_by_group when group_ids is empty' do
    group_ids = []
    assert_equal({}, @class.filter_by_group(group_ids))
  end

  test '.filter_by_org when org_ids is nonempty' do
    org_ids = [1,2,3]
    expected = { org_id: { "$in" => org_ids } }
    actual = @class.filter_by_org(org_ids)
    assert_equal expected, actual
  end

  test '.filter_by_org when org_ids is empty' do
    org_ids = []
    assert_equal({}, @class.filter_by_org(org_ids))
  end

  test '.with_limit sets default value for limit or out of bounds' do
    cursor, limit_cursor = "cursor", "limit_cursor"
    cursor.stubs(:limit).with(@class::DEFAULT_LIMIT).returns limit_cursor
    assert_equal limit_cursor, @class.with_limit(cursor, limit: nil)
    assert_equal limit_cursor, @class.with_limit(cursor, {})
    assert_equal limit_cursor, @class.with_limit(cursor, limit: 0)
    assert_equal limit_cursor, @class.with_limit(cursor, limit: -1)
    assert_equal limit_cursor, @class.with_limit(cursor, limit: @class::MAX_LIMIT + 1)
  end

  test '.with_limit with valid limit' do
    cursor, limit_cursor = "cursor", "limit_cursor"
    opts = {limit: 5}
    cursor.expects(:limit).with(5).returns limit_cursor
    assert_equal limit_cursor, @class.with_limit(cursor, opts)
  end

  test '.with_offset does nothing if offset is nil or out of boudns' do
    cursor = :cursor
    assert_equal cursor, @class.with_offset(cursor, offset: nil)
    assert_equal cursor, @class.with_offset(cursor, {})
    assert_equal cursor, @class.with_offset(cursor, offset: 0)
    assert_equal cursor, @class.with_offset(cursor, offset: -1)
  end

  test '.with_offset with valid offset' do
    cursor, offset_cursor = "cursor", "offset_cursor"
    opts = {offset: 5}
    cursor.expects(:skip).with(5).returns offset_cursor
    assert_equal offset_cursor, @class.with_offset(cursor, opts)
  end

end
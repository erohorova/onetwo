require 'test_helper'

class ForumReportServiceTest < ActiveSupport::TestCase
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(false)
    @org_1 = create(:organization)
    @org_2 = create(:organization)

    @user_1_for_org_1 = create :user, organization: @org_1
    @user_2_for_org_1 = create :user, organization: @org_1
    @user_1_for_org_2 = create :user, organization: @org_2

    # group as per language
    @rg_en_org_1 = create :resource_group, name: 'rg_en_org_1',  end_date: nil, organization: @org_1, language: 'en'
    @rg_fr_org_1 = create :resource_group, name: 'rg_fr_org_1',  end_date: nil, organization: @org_1, language: 'fr'

    @rg_en_org_1_closed = create :resource_group, name: 'rg_en_org_1_closed', end_date: nil, organization: @org_1, language: 'en', close_access: 1

    @rg_en_org_2 = create :resource_group, name: 'rg_en_org_2', end_date: nil, organization: @org_2, language: 'en'
    @rg_fr_org_2 = create :resource_group, name: 'rg_fr_org_2', end_date: nil, organization: @org_2, language: 'fr'

    # open group
    @rg_en_org_1.add_admin(@user_1_for_org_1)
    @rg_fr_org_1.add_admin(@user_1_for_org_1)
    @rg_en_org_1.add_member(@user_2_for_org_1)
    @rg_fr_org_1.add_member(@user_2_for_org_1)

    @rg_en_org_2.add_admin(@user_1_for_org_2)
    @rg_fr_org_2.add_admin(@user_1_for_org_2)

    # closed group
    @rg_en_org_1_closed.add_admin(@user_1_for_org_1)
    @rg_en_org_1_closed.add_admin(@user_2_for_org_1)

    @today     = Time.now.utc
    @later     = @today + 4.hours
    @yesterday = @today - 1.day
    @earlier   = @today - 7.hours

    @post_1    = create :post, user: @user_2_for_org_1, group: @rg_en_org_1, created_at: @earlier
    @post_2    = create :post, user: @user_2_for_org_1, group: @rg_en_org_1, created_at: @today
    @post_3    = create :post, user: @user_1_for_org_1, group: @rg_en_org_1, created_at: @yesterday
    @post_4    = create :post, user: @user_2_for_org_1, group: @rg_fr_org_1, created_at: @today
    @post_5    = create :post, user: @user_1_for_org_1, group: @rg_fr_org_1, created_at: @earlier

    @post_6    = create :post, user: @user_1_for_org_1, group: @rg_en_org_1_closed, created_at: @later
    @post_7    = create :post, user: @user_2_for_org_1, group: @rg_en_org_1_closed, created_at: @later

    @comment_1    = create :comment, user: @user_2_for_org_1, commentable: @post_1  , created_at: @earlier
    @comment_2    = create :comment, user: @user_2_for_org_1, commentable: @post_2  , created_at: @today
    @comment_5    = create :comment, user: @user_1_for_org_1, commentable: @post_5, created_at: @earlier

    @sub_comment_1    = create :comment, user: @user_2_for_org_1, commentable: @comment_1  , created_at: @earlier
    @sub_comment_2    = create :comment, user: @user_2_for_org_1, commentable: @comment_2  , created_at: @today
    @sub_comment_5    = create :comment, user: @user_1_for_org_1, commentable: @comment_5, created_at: @earlier

    #passing only date to get the forum post and comments
    @earlier = @earlier.to_date
    @later = @later.to_date
    @yesterday = @yesterday.to_date

    @report_service = ForumReportService.new
  end

  test 'get posts of course' do
    posts = @report_service.posts_for(resource_group: @rg_en_org_1, from: @earlier, to: @later)
    assert_equal [@post_1, @post_2], posts.sort
  end

  test 'get comments of course' do
    comments = @report_service.comments_for(resource_group: @rg_en_org_1, from: @earlier, to: @later)
    assert_equal [@comment_1, @comment_2], comments.sort
  end

  test 'get subcomments of course' do
    comments = @report_service.comments_for(resource_group: @rg_en_org_1, from: @earlier, to: @later)

    sub_comments = @report_service.sub_comments_for(comment_ids: comments.pluck(:id), from: @earlier, to: @later)
    assert_equal [@sub_comment_1, @sub_comment_2], sub_comments.sort
  end

  test 'get forum_data for resource groups' do
    rows = @report_service.get_forum_data(@rg_fr_org_1.id, @yesterday, @later)

    assert_same_elements([Post.url(@post_4, @rg_fr_org_1), Post.url(@post_5, @rg_fr_org_1)],
      rows.map { |r| r[3] })
  end
end
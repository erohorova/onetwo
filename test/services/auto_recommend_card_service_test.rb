require 'test_helper'

class AutoRecommendCardServiceTest < ActiveSupport::TestCase
  setup do
    @organization = create :organization
    @user = create :user, organization: @organization
    @admin = create(:user, organization: @organization, organization_role: 'admin')
    # Note:
    #
    # level 1 -> beginner
    # level 2 -> intermediate
    create(:user_profile, user: @user, learning_topics:
      [
        { "topic_name" => "fake topic name 1", "level" => 1 },
        { "topic_name" => "fake topic name 2", "level" => 2 }
      ]
    )

    @author = create :user, organization: @organization

    @card = create(:card, card_type: 'pack', card_subtype: 'simple',
      message: 'This is pathway card', taxonomy_topics: ['fake topic name 1'], author: @author)
    create(:cards_rating, level: "beginner", card: @card, user: @author)

    CardPackRelation.add(cover_id: @card.id, add_id: create(:card, message: 'Card inside a pathway', author: @author).id)
    @card.publish!


    @private_card = create(:card, card_type: 'pack', card_subtype: 'simple', is_public: false,
      message: 'This is private pathway card', taxonomy_topics: ['fake topic name 1'], author: @author)
    CardPackRelation.add(cover_id: @private_card.id, add_id: create(:card, message: 'Card inside private pathway', author: @author).id)

    create(:cards_rating, level: "beginner", card: @private_card, user: @author)

    @private_card.publish!
  end

  test 'get recommended cards' do
    stub_recommended_cards_search
    service = AutoRecommendCardService.new(@user.id, card_types: ['pack'])
    cards = service.recommended_cards
    assert_not_empty cards
    assert_includes cards.map(&:id), @card.id
  end

  test 'auto assign recommended cards' do
    stub_recommended_cards_search
    service = AutoRecommendCardService.new(@user.id, card_types: ['pack'])

    assert_difference ->{TeamAssignment.count}, 1 do
      assert_difference ->{Assignment.count}, 1 do
        service.auto_assign_recommended_cards
      end
    end

    assignment = Assignment.last
    team_assignment = TeamAssignment.last

    assert_equal @card, assignment.assignable
    assert_equal 'Card', assignment.assignable_type
    assert_equal @user, assignment.assignee

    assert_equal @organization.admins.first, team_assignment.assignor
    assert_equal assignment, team_assignment.assignment
  end

  test 'recommended cards based on user interest and level' do
    stub_recommended_cards_based_on_user_interest_and_level_search
    service = AutoRecommendCardService.new(@user.id, card_types: ['pack'])
    service.recommended_cards_based_on_user_interest_and_level
    learning_topics_cards = service.learning_topics_cards

    assert_not_empty learning_topics_cards
    assert_includes learning_topics_cards['fake topic name 1'].map(&:id), @card.id
  end

  test 'auto assign recommended cards on user interest' do
    @organization.configs.create( name: "enabled_bia", data_type: "boolean", value: "f")
    stub_recommended_cards_search
    service = AutoRecommendCardService.new(@user.id, card_types: ['pack'])

    assert_difference ->{TeamAssignment.count}, 1 do
      assert_difference ->{Assignment.count}, 1 do
        service.auto_assign_recommended_cards_to_user
      end
    end

    assignment = Assignment.last
    team_assignment = TeamAssignment.last

    assert_equal @card, assignment.assignable
    assert_equal 'Card', assignment.assignable_type
    assert_equal @user, assignment.assignee

    assert_equal @organization.admins.first, team_assignment.assignor
    assert_equal assignment, team_assignment.assignment
  end

  test 'auto assign recommended cards based on user interest and level' do
    @organization.configs.create( name: "enabled_bia", data_type: "boolean", value: "t")
    stub_recommended_cards_based_on_user_interest_and_level_search
    service = AutoRecommendCardService.new(@user.id, card_types: ['pack'])

    assert_difference -> {TeamAssignment.count}, 1 do
      assert_difference -> {Assignment.count}, 1 do
        service.auto_assign_recommended_cards_to_user
      end
    end

    assignment = Assignment.last
    team_assignment = TeamAssignment.last

    assert_equal @card, assignment.assignable
    assert_equal 'Card', assignment.assignable_type
    assert_equal @user, assignment.assignee

    assert_equal @organization.admins.first, team_assignment.assignor
    assert_equal assignment, team_assignment.assignment
  end

  test 'auto assign recommended private cards based on user interest and level only if it is accessible to the user' do
    @organization.configs.create( name: "enabled_bia", data_type: "boolean", value: "t")
    stub_recommended_cards_based_on_user_interest_and_level_search
    AutoRecommendCardService.new(@user.id, card_types: ['pack']).auto_assign_recommended_cards_to_user
    
    assert_not_includes Assignment.all, @private_card
  end

  test 'auto assign maximum recommended cards based on user interest and level' do
    @organization.configs.create( name: "enabled_bia", data_type: "boolean", value: "t")
    cards = [@card]

    6.times do
      card = create(:card, card_type: 'pack', card_subtype: 'simple',
        message: 'This is pathway card', taxonomy_topics: ['fake topic name 2'], author: @author)
      create(:cards_rating, level: "intermediate", card: card, user: @author)
      CardPackRelation.add(cover_id: card.id, add_id: create(:card, author: @author).id)
      card.publish!

      cards << card
    end

    Search::CardSearch.any_instance.expects(:search).
      returns(Search::CardSearchResponse.new(
        results: cards.map(&:search_data).map{|x| Hashie::Mash.new(x)},
        total: 7
      )).twice

    service = AutoRecommendCardService.new(@user.id, card_types: ['pack'], max_assign_count: 3)

    assert_difference -> {TeamAssignment.count}, 3 do
      assert_difference -> {Assignment.count}, 3 do
        service.auto_assign_recommended_cards_to_user
      end
    end
  end

  def stub_recommended_cards_search
    Search::CardSearch.
      any_instance.
      expects(:search).
      with(nil, @organization,
        filter_params: {
          card_type: ['pack'], state: 'published', taxonomy_topics: ['fake topic name 1', 'fake topic name 2'], exclude_ids: []},
          viewer: @user, offset: 0, limit: 100, sort: :updated, load: true).
      returns(Search::CardSearchResponse.new(
        results: [@card].map(&:search_data).map{|x| Hashie::Mash.new(x)},
        total: 1
    ))
  end

  def stub_recommended_cards_based_on_user_interest_and_level_search
    Search::CardSearch.
      any_instance.
      expects(:search).
      with(nil, @organization,
        filter_params: {
          card_type: ['pack'], state: 'published', taxonomy_topic_with_level: { name: 'fake topic name 1', level: 1, level_name: 'beginner'}, exclude_ids: []},
          viewer: @user, offset: 0, limit: 100, sort: :updated, load: true).
      returns(Search::CardSearchResponse.new(
        results: [
          @card,
          @private_card
        ].map(&:search_data).map{|x| Hashie::Mash.new(x)},
        total: 1
    ))

    Search::CardSearch.
      any_instance.
      expects(:search).
      with(nil, @organization,
        filter_params: {
          card_type: ['pack'], state: 'published', taxonomy_topic_with_level: { name: 'fake topic name 2', level: 2, level_name: 'intermediate'}, exclude_ids: [@card.id, @private_card.id]},
          viewer: @user, offset: 0, limit: 100, sort: :updated, load: true).
      returns(Search::CardSearchResponse.new(
        results: [],
        total: 0
    ))
  end
end

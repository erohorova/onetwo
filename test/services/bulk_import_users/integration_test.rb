require 'test_helper'

class BulkImportUsers::IntegrationTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper
  def setup_csv_for_invitation(send_email: false)
    stub_request(:post, 'https://api.branch.io/v1/url').to_return(status: 200, body: '', headers: {})
    stub_request(:post, 'http://localhost:9200/_bulk')

    options = {
      channel_ids: [@channel_1.id],
      invitable:   true,
      send_welcome_email: send_email,
      team_ids:    [@tech_team.id, @engg_team.id]
    }

    @klass = BulkImportUsers.new(
      admin_id: @admin_user.id,
      file_id:  @invitation_file.id,
      options:  options
    )
    @klass.import
  end

  def setup_csv_for_creation(send_email: false)
    options = {
      channel_ids:        [@channel_1.id],
      send_welcome_email: send_email,
      team_ids:           [@tech_team.id, @engg_team.id]
    }

    @klass = BulkImportUsers.new(
      file_id:  @invitation_file.id,
      options:  options,
      admin_id: @admin_user.id
    )
    @klass.import
  end

  setup do
    Config.any_instance.stubs :create_okta_group_config

    User.any_instance.stubs(:update_details_to_okta).returns true
    stub_request(:get, 'http://breelio.com/wp-content/uploads/2014/06/sample1.jpg').
      with(headers: { 'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Ruby' }).
      to_return(status: 200, body: '', headers: {})

    @organization  = create :organization
    @admin_user    = create :user, organization: @organization, organization_role: 'admin'

    # create couple of channels
    @channel_1     = create :channel, organization: @organization, user: @admin_user
    @channel_2     = create :channel, organization: @organization, user: @admin_user

    # create couple of teams/groups
    @tech_team     = create :team, organization: @organization, name: 'Technology'
    @engg_team     = create :team, organization: @organization, name: 'Engg'

    # create an end-user
    @existing_user = create :user, organization: @organization, is_complete: false, email: 'userexists@edcast.com'

    # define few custom fields in an organization
    @cf_1 = create :custom_field, organization: @organization, display_name: 'Burrito Wrap', abbreviation: nil
    @cf_2 = create :custom_field, organization: @organization, display_name: 'Cheese', abbreviation: nil
    @cf_3 = create :custom_field, organization: @organization, display_name: 'Tea', abbreviation: nil

    # adds custom fields to existing user
    create :user_custom_field, custom_field: @cf_1, user: @existing_user, value: 'juarez'
    create :user_custom_field, custom_field: @cf_2, user: @existing_user, value: 'pocho'

    csv_rows = File.open("#{Rails.root}/test/fixtures/files/users_with_custom_fields.csv").read

    @invitation_file = create(:invitation_file,
      sender_id: @admin_user.id,
      invitable: @organization,
      data: csv_rows,
      roles: 'member')

    stub_request(:post, "https://api.branch.io/v1/url").to_return(status: 200, body: "{\"url\":\"http://bitly.co}", headers: {})
  end

  test 'rejects invalid emails' do
    setup_csv_for_creation

    assert_nil @organization.users.find_by(email: 'námè+garçon@example.com')
    assert_nil @organization.users.find_by(email: 'námè+sömê@example.com')
    assert_nil @organization.users.find_by(email: 'invalid_email')
  end

  test 'sends import details to admin' do
    setup_csv_for_creation

    mailer_mock = mock()
    mailer_mock.expects(:deliver_now).returns(nil).once
    BulkImportUserDetailsMailer.expects(:notify_admin).returns(mailer_mock).once

    BulkImportUsers.new(file_id: @invitation_file.id).import
  end

  test "should not email admin channel added followers notification if users are not valid" do
    csv_rows = "First Name,Last Name,Email,Groups,\"Picture Url\",Password\nFirstName1,LastName1,invalidemail,,,Password@123"

    invitation_file = create(:invitation_file,
      sender_id: @admin_user.id,
      invitable: @organization,
      data: csv_rows,
      roles: 'member')

    UserMailer.expects(:channel_follows_notification_to_admin).never
    EDCAST_NOTIFY.expects(:trigger_event).never

    BulkImportUsers.new(file_id: invitation_file.id).import
  end

  test 'sets `is_complete` as TRUE for existing users' do
    setup_csv_for_creation

    assert @existing_user.reload.is_complete, true
  end

  test 'update first name and last name when processing bulk upload for existing users' do
    existing_user = create :user, organization: @organization, is_complete: false, email: 'edcastuser@edcast.com'
    csv_rows = "first_name,last_name,email,groups\nFirstName,LastName,edcastuser@edcast.com,Technology"
    invitation_file = create(:invitation_file, sender_id: @admin_user.id,
                             invitable: @organization, data: csv_rows, roles: "member")
    BulkImportUsers.new(file_id: invitation_file.id, admin_id: @admin_user.id).import

    assert_equal existing_user.reload.first_name, 'FirstName'
    assert_equal existing_user.reload.last_name, 'LastName'
    assert existing_user.reload.is_complete
  end

  test 'should not update blank or nil values when processing bulk upload for existing users' do
    existing_user = create :user, organization: @organization, is_complete: false, email: 'edcastuser@edcast.com',
                            first_name: 'FirstName',last_name: 'LastName', default_team_id: @tech_team.id

    csv_rows = "first_name,last_name,email,groups\n,,edcastuser@edcast.com,"
    invitation_file = create(:invitation_file, sender_id: @admin_user.id,
                             invitable: @organization, data: csv_rows, roles: "member")
      
    BulkImportUsers.new(file_id: invitation_file.id, admin_id: @admin_user.id).import
                   
    assert_equal existing_user.reload.first_name, 'FirstName'
    assert_equal existing_user.reload.last_name, 'LastName'
    assert_equal existing_user.reload.default_team_id, @tech_team.id
    assert existing_user.reload.is_complete
  end
  
  test 'add profile when processing bulk upload for the existing users' do
    existing_user = create :user, organization: @organization, is_complete: false, email: 'edcastuser@edcast.com'
    csv_rows = "first_name,last_name,email,groups,language\nFirstName,LastName,edcastuser@edcast.com,Technology,lt-LT"
    invitation_file = create(:invitation_file, sender_id: @admin_user.id,
                             invitable: @organization, data: csv_rows, roles: 'member')
    BulkImportUsers.new(file_id: invitation_file.id, admin_id: @admin_user.id).import

    assert existing_user.reload.profile
    assert_equal 'lt-LT', existing_user.reload.profile.language
  end

  test 'update profile when processing bulk upload for the existing profiles' do
    existing_user = create :user, organization: @organization, is_complete: false, email: 'edcastuser@edcast.com'
    profile  = create :user_profile, user: existing_user, language: 'lt-LT'

    csv_rows = "first_name,last_name,email,groups,language\nFirstName,LastName,edcastuser@edcast.com,Technology,en"
    invitation_file = create(:invitation_file, sender_id: @admin_user.id,
                             invitable: @organization, data: csv_rows, roles: 'member')
    BulkImportUsers.new(file_id: invitation_file.id, admin_id: @admin_user.id).import

    assert_equal 'en', existing_user.reload.profile.language
  end

  test 'add job_title in profile when processing bulk upload for the new users' do
    csv_rows = "first_name,last_name,email,groups,job_title\nFirstName,LastName,edcastuser@edcast.com,Technology,Engineer"
    invitation_file = create(:invitation_file, sender_id: @admin_user.id, invitable: @organization, data: csv_rows, roles: 'member')
    BulkImportUsers.new(file_id: invitation_file.id, admin_id: @admin_user.id).import

    user = User.find_by(email: 'edcastuser@edcast.com')
    assert user
    assert_equal 'Engineer', user.reload.job_title
  end

  test 'add job_title in profile when processing bulk upload for the existing users' do
    existing_user = create :user, organization: @organization, is_complete: false, email: 'edcastuser@edcast.com'
    csv_rows = "first_name,last_name,email,groups,job_title\nFirstName,LastName,edcastuser@edcast.com,Technology,Engineer"
    invitation_file = create(:invitation_file, sender_id: @admin_user.id, invitable: @organization, data: csv_rows, roles: 'member')
    assert_nil existing_user.reload.profile

    BulkImportUsers.new(file_id: invitation_file.id, admin_id: @admin_user.id).import

    assert existing_user.reload.profile
    assert_equal 'Engineer', existing_user.reload.job_title
  end

  test 'update job_title in profile when processing bulk upload for the existing profiles' do
    existing_user = create :user, organization: @organization, is_complete: false, email: 'edcastuser@edcast.com'
    profile  = create :user_profile, user: existing_user, job_title: 'Developer'
    assert_equal 'Developer', existing_user.job_title

    csv_rows = "first_name,last_name,email,groups,job_title\nFirstName,LastName,edcastuser@edcast.com,Technology,Engineer"
    invitation_file = create(:invitation_file, sender_id: @admin_user.id, invitable: @organization, data: csv_rows, roles: 'member')
    BulkImportUsers.new(file_id: invitation_file.id, admin_id: @admin_user.id).import

    assert_equal 'Engineer', existing_user.reload.job_title
  end

  test 'creates teams if they do not exists' do
    setup_csv_for_creation

    # [Programming, Art] are the teams that didn't exist before import
    new_teams = @organization.reload.teams.where(name: ['Art', 'Programming'])

    assert_equal  2, new_teams.count
  end

  test 'creates teams even if they exists in other org' do
    org2 = create(:organization)
    existing_team1 = create(:team, organization: org2, name: 'Programming')
    existing_team2 = create(:team, organization: org2, name: 'Art')

    setup_csv_for_creation
    new_teams = @organization.reload.teams.where(name: ['Art', 'Programming'])

    assert_equal  2, new_teams.count
  end

  test 'adds members to newly created teams' do
    setup_csv_for_creation

    # [Programming, Art] are the teams that didn't exist before import
    @organization.reload

    teams = @organization.teams
    users = @organization.users

    art_team  = teams.find_by(name: 'Art')
    pgmg_team = teams.find_by(name: 'Programming')
    user_1 = users.find_by(email: 'name1@example.com')
    user_2 = users.find_by(email: 'name2@example.com')

    assert_equal [user_1.id], pgmg_team.members.pluck(:id)
    assert_equal [user_2.id], art_team.members.pluck(:id)
  end

  test 'adds admin user as `ADMIN` to newly created teams' do
    setup_csv_for_creation

    # [Programming, Art] are the teams that didn't exist before import
    @organization.reload
    teams = @organization.teams

    art_team  = teams.find_by(name: 'Art')
    pgmg_team = teams.find_by(name: 'Programming')

    assert_equal [@admin_user.id], pgmg_team.admins.pluck(:id)
    assert_equal [@admin_user.id], art_team.admins.pluck(:id)
  end

  test 'adds users to teams' do
    setup_csv_for_creation

    teams = @organization.teams
    users = @organization.users

    pgmg_team = teams.find_by(name: 'Programming')
    art_team  = teams.find_by(name: 'Art')

    user1 = users.find_by(email: 'name1@example.com')
    user2 = users.find_by(email: 'name2@example.com')
    user3 = users.find_by(email: 'name3@example.com')
    user4 = users.find_by(email: 'name4@example.com')

    # Add unique teams to users. Must club teams from CSV and the ones added from UI while importing CSV
    assert_equal [pgmg_team.id, @tech_team.id, @engg_team.id].to_set, user1.teams.pluck(:id).to_set
    assert_equal [art_team.id, @tech_team.id, @engg_team.id].to_set, user2.teams.pluck(:id).to_set
    assert_equal [@tech_team.id, @engg_team.id].to_set, user3.teams.pluck(:id).to_set
    assert_equal [@tech_team.id, @engg_team.id].to_set, user4.teams.pluck(:id).to_set
  end

  test 'adds users to channels' do
    setup_csv_for_creation

    users = @organization.users

    user1 = users.find_by(email: 'name1@example.com')
    user2 = users.find_by(email: 'name2@example.com')
    user3 = users.find_by(email: 'name3@example.com')
    user4 = users.find_by(email: 'name4@example.com')
    existing_user = users.find_by(email: 'userexists@edcast.com')

    assert_equal [user1.id, user2.id ,user3.id, user4.id, existing_user.id].to_set , @channel_1.followers.pluck(:id).to_set
  end

  test 'does not add channels when no channel_ids supplied' do
    setup_csv_for_creation

    options = { channel_ids: nil }

    BulkImportUsers.new(
      file_id: @invitation_file.id,
      options: options,
      admin_id: @admin_user)
    .import

    assert_empty @channel_2.followers
  end

  test 'sends welcome-email to imported users' do
    setup_csv_for_creation

    mailer_mock = mock()
    mailer_mock.expects(:deliver_later).returns(nil).once
    DeviseMandrillMailer.expects(:send_import_user_welcome_email).returns(mailer_mock).once

    csv_rows = <<-FILE
First Name,Last Name,Email,Groups,Picture,Password
FirstName5,LastName5,name5@example.com,"Programming,Technology",,Password@123
    FILE

    @invitation_file.update(data: csv_rows)

    options = { send_welcome_email: true }

    BulkImportUsers.new(
      file_id: @invitation_file.id,
      options: options
    ).import
  end

  test 'assigns custom fields and values of users' do
    setup_csv_for_creation

    users = @organization.reload.users

    user_1 = users.find_by(email: 'name1@example.com')
    user_2 = users.find_by(email: 'name2@example.com')
    user_3 = users.find_by(email: 'name3@example.com')
    user_4 = users.find_by(email: 'name4@example.com')

    # when all of the custom fields are assigned
    assert_equal ['burrito_wrap', 'cheese', 'tea'].to_set, user_1.custom_fields.pluck(:abbreviation).to_set
    assert_equal 'juarez', UserCustomField.find_by(custom_field: @cf_1, user: user_1).value
    assert_equal 'mozzarella', UserCustomField.find_by(custom_field: @cf_2, user: user_1).value
    assert_equal 'green', UserCustomField.find_by(custom_field: @cf_3, user: user_1).value

    # when subset of the custom fields are assigned
    #   user_2:
    assert_equal ['burrito_wrap', 'cheese'].to_set, user_2.custom_fields.pluck(:abbreviation).to_set
    assert_equal 'pocho', UserCustomField.find_by(custom_field: @cf_1, user: user_2).value
    assert_equal 'cheddar', UserCustomField.find_by(custom_field: @cf_2, user: user_2).value
    #   user_3:
    assert_equal ['burrito_wrap'].to_set, user_3.custom_fields.pluck(:abbreviation).to_set
    assert_equal 'mission', UserCustomField.find_by(custom_field: @cf_1, user: user_3).value

    # when none of the custom fields are assigned
    #   user_4:
    assert_empty user_4.custom_fields

    # when new value for existing custom field is assigned for an existing user
    assert_equal ['burrito_wrap', 'cheese'].to_set, @existing_user.custom_fields.pluck(:abbreviation).to_set
    assert_equal 'mission', UserCustomField.find_by(custom_field: @cf_1, user: @existing_user).value

    # never assigns blank value to existing custom field
    assert_equal 'pocho', UserCustomField.find_by(custom_field: @cf_2, user: @existing_user).value
  end

  test 'sets `password_reset_required` as TRUE when password is set in CSV' do
    setup_csv_for_creation

    users = @organization.reload.users

    user1 = users.find_by(email: 'name1@example.com')
    user2 = users.find_by(email: 'name2@example.com')
    user3 = users.find_by(email: 'name3@example.com')

    assert_not user1.password_reset_required
    assert_not user2.password_reset_required
    assert user3.password_reset_required
  end

  test 'creating an invitation must not send invite email' do
    Analytics::MetricsRecorderJob.stubs(:perform_later)
    mailer_mock = mock()
    mailer_mock.expects(:deliver_later).returns(nil).times(1)
    DeviseMandrillMailer.expects(:send_import_user_welcome_email).returns(mailer_mock).once

    Invitation.any_instance.expects(:do_send_mail).never

    TestAfterCommit.enabled = true

    setup_csv_for_invitation(send_email: true)
  end

  test 'creating an invitation sends welcome email when `send_welcome_email` is set to TRUE' do
    Analytics::MetricsRecorderJob.stubs(:perform_later)
    mailer_mock = mock()
    mailer_mock.expects(:deliver_later).returns(nil).once
    InvitationMandrillMailer.expects(:send_welcome_email).returns(mailer_mock).once

    csv_rows = <<-FILE
First Name,Last Name,Email,Groups,Picture,Password
FirstName1,LastName1,name11@example.com,"Programming,Technology",,
    FILE

    options = { send_welcome_email: true, invitable: true }

    @invitation_file.update(data: csv_rows)
    stub_request(:post, 'http://localhost:9200/_bulk')

    Invitation.any_instance.expects(:do_send_mail).never

    TestAfterCommit.enabled = true

    BulkImportUsers.new(
      file_id: @invitation_file.id,
      admin_id: @admin_user.id,
      options: options
    ).import
  end

  test 'creating an invitation retains the parameters for invitee' do
    setup_csv_for_invitation

    teams = @organization.reload.teams

    pgmg_team = teams.find_by(name: 'Programming')
    art_team  = teams.find_by(name: 'Art')

    user_1_invite = Invitation.find_by(invitable: @organization, recipient_email: 'name1@example.com')
    user_2_invite = Invitation.find_by(invitable: @organization, recipient_email: 'name2@example.com')
    user_3_invite = Invitation.find_by(invitable: @organization, recipient_email: 'name3@example.com')
    user_4_invite = Invitation.find_by(invitable: @organization, recipient_email: 'name4@example.com')

    # asserting parameters for user_1 `name1@example.com`
    assert_equal [@engg_team.id, @tech_team.id, pgmg_team.id].to_set, user_1_invite.parameters['team_ids'].to_set
    assert_equal [@channel_1.id], user_1_invite.parameters['channel_ids']
    assert_equal 'juarez', user_1_invite.parameters['burrito_wrap']
    assert_equal 'mozzarella', user_1_invite.parameters['cheese']
    assert_equal 'green', user_1_invite.parameters['tea']
    assert user_1_invite.authenticate_recipient
    assert_equal 'en', user_1_invite.parameters['language']

    # asserting parameters for user_2 `name2@example.com`
    assert_equal [@engg_team.id, @tech_team.id, art_team.id].to_set, user_2_invite.parameters['team_ids'].to_set
    assert_equal [@channel_1.id], user_2_invite.parameters['channel_ids']
    assert_equal 'pocho', user_2_invite.parameters['burrito_wrap']
    assert_equal 'cheddar', user_2_invite.parameters['cheese']
    assert_nil user_2_invite.parameters['tea']
    assert user_2_invite.authenticate_recipient
    assert_equal 'pt-BR', user_2_invite.parameters['language']

    # asserting parameters for user_3 `name3@example.com`
    assert_equal [@engg_team.id, @tech_team.id].to_set, user_3_invite.parameters['team_ids'].to_set
    assert_equal [@channel_1.id], user_3_invite.parameters['channel_ids']
    assert_equal 'mission', user_3_invite.parameters['burrito_wrap']
    assert_nil user_3_invite.parameters['cheese']
    assert_nil user_3_invite.parameters['tea']
    assert user_3_invite.authenticate_recipient
    assert_nil user_3_invite.parameters['language']

    # asserting parameters for user_4 `name4@example.com`
    assert_equal [@engg_team.id, @tech_team.id].to_set, user_4_invite.parameters['team_ids'].to_set
    assert_equal [@channel_1.id], user_4_invite.parameters['channel_ids']
    assert_nil user_4_invite.parameters['burrito_wrap']
    assert_nil user_4_invite.parameters['cheese']
    assert_nil user_4_invite.parameters['tea']
    assert user_4_invite.authenticate_recipient
    assert_nil user_4_invite.parameters['language']
  end

  test 'considers pending invitation and update parameters for non-existing user' do
    BranchmetricsApi.stubs(:generate_link).returns(true)
    invitation = create :invitation, invitable: @organization, recipient_email: 'name1@example.com',
      parameters: { channel_ids: [@channel_2.id], team_ids: [@engg_team.id], burrito_wrap: 'pocho' },
      sender: @admin_user

    setup_csv_for_invitation

    pgmg_team = @organization.teams.find_by(name: 'Programming')

    invitation = Invitation.find_by(invitable: @organization, recipient_email: invitation.recipient_email)
    assert_equal [@channel_1.id, @channel_2.id].to_set, invitation.parameters['channel_ids'].to_set
    assert_equal [pgmg_team.id, @tech_team.id, @engg_team.id].to_set, invitation.parameters['team_ids'].to_set
    assert_equal 'juarez', invitation.parameters['burrito_wrap']
    assert_equal 'mozzarella', invitation.parameters['cheese']
    assert_equal 'green', invitation.parameters['tea']
  end

  test 'maintains import status for every users, groups and channels considered for SSO users' do
    mailer_mock = mock()
    mailer_mock.expects(:deliver_later).returns(nil).times(1)
    DeviseMandrillMailer.expects(:send_import_user_welcome_email).returns(mailer_mock).times(1)

    setup_csv_for_invitation(send_email: true)

    # No new users ---
    assert_equal 5, @klass.instance_variable_get(:@count)[:total_users_count]
    assert_equal 0, @klass.instance_variable_get(:@count)[:new_users_count]
    assert_equal 1, @klass.instance_variable_get(:@count)[:old_users_count]
    assert_equal 0, @klass.instance_variable_get(:@count)[:failed_users_count]
    assert_equal 1, @klass.instance_variable_get(:@count)[:resent_emails_count]

    # Invites ---
    assert_equal 4, @klass.instance_variable_get(:@count)[:invitees_count]

    # Channels ---
    assert_equal [@channel_1.label].join(', '), @klass.instance_variable_get(:@channels_names_list)

    # Groups ---
    assert_equal 1, @klass.instance_variable_get(:@new_groups)['Programming']
    assert_equal 1, @klass.instance_variable_get(:@new_groups)['Art']
    assert_equal 5, @klass.instance_variable_get(:@old_groups)['Technology']
    assert_equal 5, @klass.instance_variable_get(:@old_groups)['Engg']
  end

  test 'maintains import status for every users, groups and channels considered for non-SSO users' do
    mailer_mock = mock()
    mailer_mock.expects(:deliver_later).returns(nil).times(5)
    DeviseMandrillMailer.expects(:send_import_user_welcome_email).returns(mailer_mock).times(5)

    setup_csv_for_creation(send_email: true)

    # Users ---
    assert_equal 5, @klass.instance_variable_get(:@count)[:total_users_count]
    assert_equal 4, @klass.instance_variable_get(:@count)[:new_users_count]
    assert_equal 1, @klass.instance_variable_get(:@count)[:old_users_count]
    assert_equal 0, @klass.instance_variable_get(:@count)[:failed_users_count]
    assert_equal 1, @klass.instance_variable_get(:@count)[:resent_emails_count]

    # No invites ---
    assert_equal 0, @klass.instance_variable_get(:@count)[:invitees_count]

    # Channels ---
    assert_equal [@channel_1.label].join(', '), @klass.instance_variable_get(:@channels_names_list)

    # Groups ---
    assert_equal 1, @klass.instance_variable_get(:@new_groups)['Programming']
    assert_equal 1, @klass.instance_variable_get(:@new_groups)['Art']
    assert_equal 5, @klass.instance_variable_get(:@old_groups)['Technology']
    assert_equal 5, @klass.instance_variable_get(:@old_groups)['Engg']
  end

  test 'resends welcome-email to existing users who have not completed onboarding' do
    User.any_instance.unstub :generate_user_onboarding
    # onboarding is in `new` state
    user1 = create(:user, :with_onboarding_incomplete, organization: @organization, email: 'name1@example.com', is_complete: false)

    # onboarding is in `started` state
    user2 = create(:user, :with_onboarding_incomplete, organization: @organization, email: 'name2@example.com', is_complete: false)
    user2.user_onboarding.start!

    # no onboarding present
    user3 = create(:user, :with_onboarding_complete, organization: @organization, email: 'name3@example.com')
    user3.user_onboarding.destroy

    # onboarding is in `completed` state
    user4 = create(:user, :with_onboarding_complete, organization: @organization, email: 'name4@example.com')

    mailer_mock = mock()
    mailer_mock.expects(:deliver_later).returns(nil).times(4)
    DeviseMandrillMailer.expects(:send_import_user_welcome_email).returns(mailer_mock).times(4)

    options = { send_welcome_email: true }

    BulkImportUsers.new(
      file_id: @invitation_file.id,
      options: options
    ).import
  end

  test 'does not provisions user to Okta if organization do not have Okta enabled' do
    User.any_instance.expects(:provision_and_sign_in).never

    setup_csv_for_creation
  end

  test 'provisions user to Okta if organization has Okta enabled and password is configured in CSV' do
    User.any_instance.stubs :update_details_to_okta
    create :config, :boolean, configable: @organization, name: 'app.config.okta.enabled', value: 'true'
    create :config, configable: @organization, name: 'OktaApiToken', value: SecureRandom.hex

    @organization.reload

    User.any_instance.expects(:provision_and_sign_in).times(4)

    setup_csv_for_creation
  end

  test 'fallback to local URL if provisioning requests fails' do
    create_default_ssos
    Organization.any_instance.unstub :default_sso
    @organization.send :default_sso

    stub_failed_user_creation

    create :config, :boolean, configable: @organization, name: 'app.config.okta.enabled', value: 'true'
    create :config, configable: @organization, name: 'OktaApiToken', value: SecureRandom.hex

    @organization.reload

    # Users do not have password in CSV
    mailer_mock = mock()
    mailer_mock.expects(:deliver_later).returns(nil).times(5)
    DeviseMandrillMailer.expects(:send_import_user_welcome_email)
    .with(has_entry(:redirect_url, includes('/api/v2/users/login_landing')))
    .returns(mailer_mock).times(5)

    setup_csv_for_creation(send_email: true)
  end

  test 'federated sessionToken URL is generated if provisioning requests succeeds and password supplied in CSV' do
    stub_okta_user

    create :config, :boolean, configable: @organization, name: 'app.config.okta.enabled', value: 'true'
    create :config, configable: @organization, name: 'OktaApiToken', value: SecureRandom.hex

    @organization.reload

    mailer_mock1 = mock()
    mailer_mock1.expects(:deliver_later).returns(nil).times(5)
    DeviseMandrillMailer
      .expects(:send_import_user_welcome_email)
      .with(has_entry(:redirect_url, includes('https://dev-579575.oktapreview.com/login/sessionCookieRedirect')))
      .returns(mailer_mock1)
      .times(5)

    setup_csv_for_creation(send_email: true)
  end

  test 'sends default first and last name to federated system' do
    create :config, :boolean, configable: @organization, name: 'app.config.okta.enabled', value: 'true'
    api_token = create :config, configable: @organization, name: 'OktaApiToken', value: SecureRandom.hex

    Okta::InternalFederation.any_instance.stubs(:federated_user).returns(nil)
    user_json = File.read(Rails.root.join('test/fixtures/api/okta_user.json'))
    stub_okta_session_token
    stub_okta_user_password_update
    
    mailer_mock = mock()
    mailer_mock.expects(:deliver_later).returns(nil).once
    DeviseMandrillMailer.expects(:send_import_user_welcome_email).returns(mailer_mock).once

    stub_request(:post, "https://dev-579575.oktapreview.com/api/v1/users?activate=true").
      with(body: {
        "profile": {
          "firstName": "First name",
          "lastName": "Last name",
          "email": "name5@example.com",
          "login": "#{@organization.id}-name5@example.com"
        },
        "credentials": {
          "password": {
            "value": "Password@123"
          }
        }
      }.to_json,
      headers: {
        'Accept' => 'application/json',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'Authorization' => "SSWS #{api_token.value}",
        'Content-Type' => 'application/json',
        'User-Agent' => 'Faraday v0.9.2'
      }).
      to_return(status: 200, body: user_json, headers: {})
stub_request(:post, "https://api.branch.io/v1/url")
    csv_rows = <<-FILE
First Name,Last Name,Email,Groups,Picture,Password
,,name5@example.com,"Programming,Technology",,Password@123
    FILE

    @invitation_file.update(data: csv_rows)

    options = { send_welcome_email: true }

    BulkImportUsers.new(
      file_id: @invitation_file.id,
      options: options
    ).import
  end

  test 'bulk_import should handle default_group for user' do
    csv_rows = 'First Name,Last Name,Email,Groups,"Picture Url",Password,"Burrito Wrap",Cheese,tea,"Default Group"
                RegularUser,RegularLastName,regular_email@mail.com,"Programming,Technology",,,,,,Technology
                RegularUser2,RegularLastName2,regular_email2@mail.com,"Programming,Technology",,,,,,NewOne'

    invitation_file = create(:invitation_file, sender_id: @admin_user.id, invitable: @organization,
                             data: csv_rows, roles: 'member')

    options = {
      channel_ids:        [@channel_1.id],
      send_welcome_email: false,
      team_ids:           [@tech_team.id, @engg_team.id]
    }

    BulkImportUsers.new(
      file_id:  invitation_file.id,
      options:  options,
      admin_id: @admin_user.id
    ).import

    usr = @organization.users.find_by(email: 'regular_email@mail.com')
    usr2 = @organization.users.find_by(email: 'regular_email2@mail.com')
    default_group = @organization.teams.find_by(name: 'Technology')
    default_group2 = @organization.teams.find_by(name: 'NewOne')

    assert usr
    assert usr2

    assert_equal usr.default_team_id, default_group.id
    assert_equal usr2.default_team_id, default_group2.id

    assert default_group.users.include?(usr)
    assert default_group2.users.include?(usr2)
  end

  test 'bulk_import should clear channels followers count cache' do
    csv_rows = 'First Name,Last Name,Email,Groups,"Picture Url",Password,"Burrito Wrap",Cheese,tea,"Default Group"
                RegularUser,RegularLastName,regular_email@mail.com,"Programming,Technology",,,,,,Technology
                RegularUser2,RegularLastName2,regular_email2@mail.com,"Programming,Technology",,,,,,NewOne'

    invitation_file = create(:invitation_file, sender_id: @admin_user.id, invitable: @organization,
                             data: csv_rows, roles: 'member')

    options = {
      channel_ids:        [@channel_1.id],
      send_welcome_email: false,
      team_ids:           [@tech_team.id, @engg_team.id]
    }

    Rails.cache = ActiveSupport::Cache::MemoryStore.new
    create(:teams_channels_follow, team: @tech_team, channel: @channel_1)
    create(:teams_channels_follow, team: @engg_team, channel: @channel_1)

    Rails.cache.fetch("channel_#{@channel_1.id}_followers_count".to_sym) {8}
    assert_equal 8, Rails.cache.read("channel_#{@channel_1.id}_followers_count".to_sym)

    BulkImportUsers.new(
      file_id:  invitation_file.id,
      options:  options,
      admin_id: @admin_user.id
    ).import
    assert_equal nil, Rails.cache.read("channel_#{@channel_1.id}_followers_count".to_sym)
  end

  test 'should trigger UserBulkUploadWorkflowJob to process workflows on user bulk upload' do
    EdcastActiveJob.unstub(:perform_later)
    UserBulkUploadWorkflowJob.unstub(:perform_later)
    create :config, configable: @organization, value: true, data_type: 'boolean', name: 'simplified_workflow'
    assert_enqueued_with(job: UserBulkUploadWorkflowJob) do
      setup_csv_for_creation
    end
    assert_enqueued_jobs 1
  end
end

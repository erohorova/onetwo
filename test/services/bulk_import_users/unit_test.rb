require 'test_helper'

class BulkImportUsers::UnitTest < ActiveSupport::TestCase
  setup do
    @organization  = create :organization
    @admin_user    = create :user, organization: @organization, organization_role: '@admin'

    # create a channel
    @channel       = create :channel, organization: @organization, user: @admin_user

    # create couple of teams/groups
    @tech_team     = create :team, organization: @organization, name: 'Technology'
    @engg_team     = create :team, organization: @organization, name: 'Engg'

    # create an end-user
    @existing_user = create :user, organization: @organization, is_complete: false, email: 'userexists@edcast.com'

    csv_rows = File.open("#{Rails.root}/test/fixtures/files/users_with_custom_fields.csv").read

    @invitation_file = create :invitation_file,
      sender_id: @admin_user.id,
      invitable: @organization,
      data: csv_rows,
      roles: 'member'
  end

  # validates #preview_csv -- START
  test '#preview_csv resolves fields and values for quick preview' do
    data = BulkImportUsers.new(file_id: @invitation_file.id).preview_csv
    assert data.length, 3
    assert_equal data.first.keys, ['first_name', 'last_name', 'email', 'groups', 'picture_url', 'password', 'language']
  end

  test '#preview_csv resolves custom fields' do
    csv_data = File.open("#{Rails.root}/test/fixtures/files/users_with_custom_fields.csv").read

    file = create :invitation_file, sender_id: @admin_user.id, invitable: @organization, data: csv_data, roles: 'member'

    cf1 = create :custom_field, organization: @organization, display_name: 'Burrito Wrap', abbreviation: nil
    cf2 = create :custom_field, organization: @organization, display_name: 'Cheese', abbreviation: nil
    cf3 = create :custom_field, organization: @organization, display_name: 'Tea', abbreviation: nil

    data = BulkImportUsers.new(file_id: file.id).preview_csv
    assert data.length, 4
    assert_equal ['first_name', 'last_name', 'email', 'groups', 'picture_url', 'password', 'language',
      'burrito_wrap', 'cheese', 'tea'].to_set, data.first.keys.to_set

    user1 = data.select { |d| d['email'] == 'name1@example.com' }.first
    assert_equal 'juarez', user1['burrito_wrap']
    assert_equal 'mozzarella', user1['cheese']
    assert_equal 'green', user1['tea']

    user2 = data.select { |d| d['email'] == 'name2@example.com' }.first
    assert_equal 'pocho', user2['burrito_wrap']
    assert_equal 'cheddar', user2['cheese']
    assert_nil user2['tea']

    user3 = data.select { |d| d['email'] == 'name3@example.com' }.first
    assert_equal 'mission', user3['burrito_wrap']
    assert_nil user3['cheese']
    assert_nil user3['tea']

    user4 = data.select { |d| d['email'] == 'name4@example.com' }.first
    assert_nil user4['burrito_wrap']
    assert_nil user4['cheese']
    assert_nil user4['tea']
  end
  # validates #preview_csv -- END

  # validates .csv_formatted_user_emails -- START
  test '.csv_formatted_user_emails generate proper CSV-formatted-data' do
    emails = ['name1@example.com', 'name2@example.com', 'name3@example.com']
    groups = "Technology,Programming"
    csv_data = BulkImportUsers.csv_formatted_user_emails(emails, groups)

    parsed_data = CSV.parse(csv_data)
    assert_includes parsed_data, ['first_name', 'last_name', 'email', 'groups']
    assert_includes parsed_data, ['', '', 'name1@example.com', "Technology,Programming"]
    assert_includes parsed_data, ['', '', 'name2@example.com', "Technology,Programming"]
    assert_includes parsed_data, ['', '', 'name3@example.com', "Technology,Programming"]
  end
  # validates .csv_formatted_user_emails -- END

  # validates .csv_headers_validation -- START
  test '.csv_headers_validation validates mandatory headers in the CSV' do
    # empty csv
    csv_data = "xyz,pqr,email,group\n"
    assert_equal "Invalid or empty csv" , BulkImportUsers.csv_headers_validation(csv_data)

    # invalid headers
    csv_data = "xyz,pqr,email,group\nfirst_name,last_name,test@email.com,Technology"
    assert_equal("Invalid CSV headers. Use format first_name, last_name, email, groups (optional), picture_url (optional), password (optional)",
                 BulkImportUsers.csv_headers_validation(csv_data))

    # valid headers
    csv_data = "first_name,last_name,email,group\nfirst_name,last_name,test@email.com,Technology"
    assert_nil BulkImportUsers.csv_headers_validation(csv_data)
  end
  # validates .csv_headers_validation -- END

  # validates .csv_formatted_user_details -- START
  test '.csv_formatted_user_details returns email, first_name, last_name and groups' do
    details = { 'first_name' => 'First Name', 'last_name' => 'Last Name', 'email' => 'test@example.com' }
    parsed_data = BulkImportUsers.csv_formatted_user_details(details, 'Technology')
    assert_equal "first_name,last_name,email,groups\nFirst Name,Last Name,test@example.com,Technology\n", parsed_data
  end
  # validates .csv_formatted_user_details -- END
end

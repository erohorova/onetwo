require 'test_helper'

class Translation::ExportUntranslatedStringsTest < ActiveSupport::TestCase
  setup do
    @languages = LANGUAGES.values
  end

  teardown do
    RedisHashKey.new(key: "20180110").clear
  end

  test "appends untranslated strings to csv" do
    @languages.each do |language|
      stub_request(:get, /translations\/#{language}\/edcast_translation.csv/).
        to_return(status: 200, body: File.read('test/fixtures/files/fr.csv'), headers: {})
    end

    Aws::S3::Object.any_instance.expects('put').with(body: "Cancel,Annuler\nor,ou\ntest,\"\"\n", acl: 'public-read', content_type: 'text/csv').times(@languages.count)
    RedisHashKey.new(key: "20180110", field: 'fr', value: 'test').set_as_array
    Translation::ExportUntranslatedStrings.new(key: "20180110").export
  end

  test "doesn't append to csv if string (case-sensitive) is already present" do
    @languages.each do |language|
      stub_request(:get, /translations\/#{language}\/edcast_translation.csv/).
        to_return(status: 200, body: File.read('test/fixtures/files/fr.csv'), headers: {})
    end

    Aws::S3::Object.any_instance.expects('put').never
    RedisHashKey.new(key: "20180110", field: 'fr', value: 'Cancel').set_as_array
    Translation::ExportUntranslatedStrings.new(key: "20180110").export
  end

  test "case-sensitive: appends string to csv" do
    @languages.each do |language|
      stub_request(:get, /translations\/#{language}\/edcast_translation.csv/).
        to_return(status: 200, body: File.read('test/fixtures/files/fr.csv'), headers: {})
    end

    Aws::S3::Object.any_instance.expects('put').with(body: "Cancel,Annuler\nor,ou\ncancel,\"\"\n", acl: 'public-read', content_type: 'text/csv').times(@languages.count)
    RedisHashKey.new(key: "20180110", field: 'fr', value: 'cancel').set_as_array
    Translation::ExportUntranslatedStrings.new(key: "20180110").export
  end

  test "updates each language csv with strings from different languages" do
    @languages.each do |language|
      stub_request(:get, /translations\/#{language}\/edcast_translation.csv/).
        to_return(status: 200, body: File.read('test/fixtures/files/fr.csv'), headers: {})
    end

    Aws::S3::Object.any_instance.expects('put').with(
      body: "Cancel,Annuler\nor,ou\ncancel1,\"\"\ncancel2,\"\"\ncancel3,\"\"\n",
      acl: 'public-read',
      content_type: 'text/csv'
    ).times(@languages.count)
    RedisHashKey.new(key: "20180110", field: 'fr', value: 'cancel1').set_as_array
    RedisHashKey.new(key: "20180110", field: 'ru', value: 'cancel2').set_as_array
    RedisHashKey.new(key: "20180110", field: 'de', value: 'cancel3').set_as_array
    Translation::ExportUntranslatedStrings.new(key: "20180110").export
  end
end

require 'test_helper'

class FlagContent::CardReportingSearchServiceTest < ActiveSupport::TestCase
  setup do
    skip #DO NOT MAKE NETWORK CALLS TO ES

    WebMock.disable!
    # create the index

    FlagContent::CardReportingSearchService.create_index! force: true
    FlagContent::CardReportingSearchService.refresh_index!
    EdcastActiveJob.unstub :perform_later
    CardReportingIndexingJob.unstub :perform_later
    cards_setup
  end

  teardown do
    FlagContent::CardReportingSearchService.gateway.delete_index!
    WebMock.enable!
  end

  def cards_setup
    @org1 = create(:organization, host_name:"host1233")
    @user = create(:user, organization: @org1)
    @user_1 = create(:user, organization: @org1)
    @user_2 = create(:user, organization: @org1)

    # cards

    @card_1 = create(:card, author: @user, deleted_at: nil, title: "Woaah", message:"Hi", organization_id: @org1.id)
    @card_2 = create(:card, author: @user, deleted_at: nil, title: " ", message:"God", organization_id: @org1.id)
    @card_3 = create(:card, author: @user, deleted_at: nil, title: " ", message:"God", organization_id: @org1.id)
    @card_4 = create(:card, author: @user, deleted_at: nil, title: " ", message:"Goddess", organization_id: @org1.id)

    # to_be_deleted cards
    @deleted_card_1 = create(:card, author: @user, title: " ", message:"God", organization_id: @org1.id)
    @deleted_card_2 = create(:card, author: @user, title: "Yaa", message:"God", organization_id: @org1.id)
    @deleted_card_3 = create(:card, author: @user, title: "Yay ", message:"God", organization_id: @org1.id)

    # reported_card_reporting
    @reported_card_1 = create(:card_reporting, user_id:@user_1.id, card_id:@card_1.id, reason: "What a reason!")
    @reported_card_2 = create(:card_reporting, user_id:@user_1.id, card_id:@card_2.id, reason: "What the Hell!")
    @reported_card_3 = create(:card_reporting, user_id:@user_1.id, card_id:@card_3.id, reason: "This is a foul card!")
    @reported_card_4 = create(:card_reporting, user_id:@user_1.id, card_id:@card_4.id, reason: "This is a foul card!")
    CardReportingIndexingJob.perform_now(card_id: @card_1.id)
    CardReportingIndexingJob.perform_now(card_id: @card_2.id)
    CardReportingIndexingJob.perform_now(card_id: @card_3.id)
    CardReportingIndexingJob.perform_now(card_id: @card_4.id)

    # trashed_card_reporting
    @trashed_card_1 = create(:card_reporting, user_id: @user_2.id, card_id: @deleted_card_1.id, reason: "You are trashed!")
    @trashed_card_2 = create(:card_reporting, user_id: @user_2.id, card_id: @deleted_card_2.id, reason: "You are trasheded!")
    @trashed_card_3 = create(:card_reporting, user_id: @user_2.id, card_id: @deleted_card_3.id, reason: "You are trashedest!")
    CardReportingIndexingJob.perform_now(card_id: @deleted_card_1.id)
    CardReportingIndexingJob.perform_now(card_id: @deleted_card_2.id)
    CardReportingIndexingJob.perform_now(card_id: @deleted_card_3.id)

    # delete to_be_deleted_cards
    [@deleted_card_1, @deleted_card_2, @deleted_card_3].map(&:delete)
    CardReportingIndexingJob.perform_now(card_id: @deleted_card_1.id)
    CardReportingIndexingJob.perform_now(card_id: @deleted_card_2.id)
    CardReportingIndexingJob.perform_now(card_id: @deleted_card_3.id)

    @deleted_card_1.card_reportings.destroy_all
    @deleted_card_2.card_reportings.destroy_all
    @deleted_card_3.card_reportings.destroy_all
    CardReportingIndexingJob.perform_now(card_id: @deleted_card_1.id)
    CardReportingIndexingJob.perform_now(card_id: @deleted_card_2.id)
    CardReportingIndexingJob.perform_now(card_id: @deleted_card_3.id)

    # refresh index (!important)
    FlagContent::CardReportingSearchService.refresh_index!
  end

  test "should return empty results when matched with no records " do
    results = FlagContent::CardReportingSearchService.search_for_card(query: "xyz", organization: @org1, offset: 0, limit: 10, state: 'reported')
    assert_equal 0, results.count
  end

  test "should search trashed cards with search string: Yaa " do
    results = FlagContent::CardReportingSearchService.search_for_card(query: "Yaa", organization: @org1, offset: 0, limit: 10, state: 'trashed')
    assert_equal 2, results.count # edge ngram search
  end

  test "should not return cards trashed cards with search string: single letter " do
    results = FlagContent::CardReportingSearchService.search_for_card(query: "Y", organization: @org1, offset: 0, limit: 10, state: 'trashed')
    assert_equal 0, results.count # edge ngram search (min-gram is set to 2)
  end

  test "should search reported cards with search string: God " do
    results = FlagContent::CardReportingSearchService.search_for_card(query: "God", organization: @org1, offset: 0, limit: 10, state: 'reported')
    assert_equal 3, results.count # edge ngram search
  end

  test "should not return cards reported cards with search string: single letter " do
    results = FlagContent::CardReportingSearchService.search_for_card(query: "G", organization: @org1, offset: 0, limit: 10, state: 'reported')
    assert_equal 0, results.count # edge ngram search (min-gram is set to 2)
  end

  test "should search all reported cards with search string: empty " do
    results = FlagContent::CardReportingSearchService.search_for_card(query: " ", organization: @org1, offset: 0, limit: 10, state: 'reported')
    assert_equal 4, results.count
  end

  test "should search all trashed cards with search string: empty " do
    results = FlagContent::CardReportingSearchService.search_for_card(query: " ", organization: @org1, offset: 0, limit: 10, state: 'trashed')
    assert_equal 3, results.count
  end
end

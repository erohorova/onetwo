require 'test_helper'
class CommentTrashingServiceTest < ActiveSupport::TestCase
  # include RequestHelperMethods
  setup do
    @org = create(:organization)
    @user, @comment_author = create_list(:user, 2, organization: @org)
    @card = create(:card, organization: @org, author: @user)
    @activity_stream = create(:activity_stream, streamable: @card, organization: @org, user: @user)

    @comment = create(:comment, commentable: @card, user: @comment_author)
    create(:comment_reporting, comment: @comment, user: @user)
  end

  test 'should destroy a comment' do
    service = FlagContent::CommentTrashingService.new(comment: @comment)
    service.call

    assert_equal true, service.status
  end

  test 'should soft delete comment_reportings of comment' do
    service = FlagContent::CommentTrashingService.new(comment: @comment)
    service.call

    assert_equal 1, @comment.comment_reportings.only_deleted.count
  end

  test 'should decrease the comments_count by 1 for commentable' do
    assert_difference -> { @card.reload.comments_count }, -1 do
      service = FlagContent::CommentTrashingService.new(comment: @comment)
      service.call
    end
  end

  test 'should soft delete mentions of a trashed comment' do
    u1, u2 = create_list(:user, 2, organization: @org)
    message = "this is a message with  #{u1.handle} and #{u2.handle}."

    mentioned_comment = create(:comment, commentable: @card, user: @comment_author, message: message)
    create(:comment_reporting, comment: mentioned_comment, user: @user)

    service = FlagContent::CommentTrashingService.new(comment: mentioned_comment)
    service.call

    assert_equal 2, mentioned_comment.reload.mentions.only_deleted.count
  end

  test 'should soft delete votes of a trashed comment' do
    voter = create(:user, organization: @org)
    vote = create(:vote, votable: @comment, voter: voter)

    service = FlagContent::CommentTrashingService.new(comment: @comment)
    service.call

    assert_equal 1, @comment.reload.up_votes.only_deleted.count
  end
end

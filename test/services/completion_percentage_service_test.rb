require 'test_helper'

class CompletionPercentageServiceTest < ActiveSupport::TestCase
  setup do
    @org = create(:organization)
    @user = create(:user, organization: @org)
    
  end

  test 'should update completd percentage of started pathways of which a smartcard is part of and smartcard is marked as completed' do
    c1, c2, c3, c4 = create_list(:card, 4, organization: @org, author: @user)
    p1, p2, p3, p4 = create_list(:card, 4, card_type: 'pack', card_subtype: 'simple', organization: @org, author: @user)
    
    p1.add_card_to_pathway(c1.id, 'Card')
    p1.add_card_to_pathway(c2.id, 'Card')
    p2.add_card_to_pathway(c2.id, 'Card')
    p2.add_card_to_pathway(c3.id, 'Card')
    p3.add_card_to_pathway(c4.id, 'Card')
    p4.add_card_to_pathway(c2.id, 'Card')
    p4.add_card_to_pathway(c4.id, 'Card')

    c1_ucc = create(:user_content_completion,completable: c1,
                    user: @user, state: "completed",completed_percentage: 100)
    p1_ucc = create(:user_content_completion,completable: p1,
                    user: @user, state: "started",completed_percentage: 50)
    c3_ucc = create(:user_content_completion,completable: c3,
                    user: @user, state: "completed",completed_percentage: 100)
    p2_ucc = create(:user_content_completion,completable: p2,
                    user: @user, state: "started",completed_percentage: 50)
    c4_ucc = create(:user_content_completion,completable: c4,
                    user: @user, state: "completed",completed_percentage: 100)
    p3_ucc = create(:user_content_completion,completable: p3,
                    user: @user, state: "started",completed_percentage: 98)
    c2_ucc = create(:user_content_completion,completable: c2,
                    user: @user, state: "completed",completed_percentage: 100)
    CompletionPercentageService.set_completed_percentage(@user, c2)
    p1_ucc.reload
    p2_ucc.reload
    p3_ucc.reload

    assert_equal 98, p1_ucc.completed_percentage
    assert_equal 98, p2_ucc.completed_percentage
    assert_equal 98, p3_ucc.completed_percentage
  end

  test 'should update completd percentage of started journeys of which a smartcard is part of and smartcard is marked as completed' do
    c1, c2, c3, c4 = create_list(:card, 4, organization: @org, author: @user)
    p1, p2, p3 = create_list(:card, 3, card_type: 'pack', card_subtype: 'simple', organization: @org, author: @user)
    j1, j2, j3 =  create_list(:card, 3, card_type: 'journey', card_subtype: 'self_paced', author: @user, organization: @org)

    j1.add_card_to_journey(p1.id)
    j2.add_card_to_journey(p2.id)
    j3.add_card_to_journey(p3.id)

    p1.add_card_to_pathway(c1.id, 'Card')
    p1.add_card_to_pathway(c2.id, 'Card')
    p2.add_card_to_pathway(c2.id, 'Card')
    p2.add_card_to_pathway(c3.id, 'Card')
    p3.add_card_to_pathway(c4.id, 'Card')

    c1_ucc = create(:user_content_completion,completable: c1,
                    user: @user, state: "completed",completed_percentage: 100)
    j1_ucc = create(:user_content_completion,completable: j1,
                    user: @user, state: "started",completed_percentage: 50)
    c3_ucc = create(:user_content_completion,completable: c3,
                    user: @user, state: "completed",completed_percentage: 100)
    j2_ucc = create(:user_content_completion,completable: p2,
                    user: @user, state: "started",completed_percentage: 50)
    c4_ucc = create(:user_content_completion,completable: c4,
                    user: @user, state: "completed",completed_percentage: 100)
    j3_ucc = create(:user_content_completion,completable: p3,
                    user: @user, state: "completed",completed_percentage: 100)
    c2_ucc = create(:user_content_completion,completable: c2,
                    user: @user, state: "completed",completed_percentage: 100)
    p3.add_card_to_pathway(c2.id, 'Card')

    CompletionPercentageService.set_completed_percentage(@user, c2)
    j1_ucc.reload
    j2_ucc.reload
    j3_ucc.reload

    assert_equal 98, j1_ucc.completed_percentage
    assert_equal 98, j2_ucc.completed_percentage
    assert_equal 100, j3_ucc.completed_percentage
  end

  test 'should update completd percentage of journeys of which a pathway is part of and pathway is marked as completed' do
    c1, c2, c3, c4 = create_list(:card, 4, organization: @org, author: @user)
    p1, p2, p3, p4 = create_list(:card, 4, card_type: 'pack', card_subtype: 'simple', organization: @org, author: @user)
    j1, j2, j3 =  create_list(:card, 3, card_type: 'journey', card_subtype: 'self_paced', author: @user, organization: @org)

    j1.add_card_to_journey(p1.id)
    j1.add_card_to_journey(p2.id)
    j2.add_card_to_journey(p2.id)
    j2.add_card_to_journey(p3.id)
    j3.add_card_to_journey(p4.id)

    p1.add_card_to_pathway(c1.id, 'Card')
    p2.add_card_to_pathway(c2.id, 'Card')
    p3.add_card_to_pathway(c3.id, 'Card')
    p4.add_card_to_pathway(c4.id, 'Card')

    c1_ucc = create(:user_content_completion,completable: c1,
                    user: @user, state: "completed",completed_percentage: 100)
    j1_ucc = create(:user_content_completion,completable: j1,
                    user: @user, state: "started",completed_percentage: 50)
    c3_ucc = create(:user_content_completion,completable: c3,
                    user: @user, state: "completed",completed_percentage: 100)
    j2_ucc = create(:user_content_completion,completable: j2,
                    user: @user, state: "started",completed_percentage: 50)
    c4_ucc = create(:user_content_completion,completable: c4,
                    user: @user, state: "completed",completed_percentage: 100)
    j3_ucc = create(:user_content_completion,completable: j3,
                    user: @user, state: "completed",completed_percentage: 100)
    c2_ucc = create(:user_content_completion,completable: c2,
                    user: @user, state: "completed",completed_percentage: 100)
    j3.add_card_to_journey(p2.id)

    CompletionPercentageService.set_completed_percentage(@user, c2)
    j1_ucc.reload
    j2_ucc.reload
    j3_ucc.reload

    assert_equal 98, j1_ucc.completed_percentage
    assert_equal 98, j2_ucc.completed_percentage
    assert_equal 100, j3_ucc.completed_percentage
  end

  test 'should recalculate completed pathwhay after uncompletion of a card inside this pathway' do
    c1, c2 = create_list(:card, 3, organization: @org, author: @user)
    p1 = create(
      :card, card_type: 'pack',
      card_subtype: 'simple',
      organization: @org, author: @user
    )
    p1.add_card_to_pathway(c1.id, 'Card')
    p1.add_card_to_pathway(c2.id, 'Card')

    create(:user_content_completion,completable: c1,
                    user: @user, state: 'completed',completed_percentage: 100)
    p1_ucc = create(:user_content_completion,completable: p1,
                    user: @user, state: 'completed',completed_percentage: 100)
    c2_ucc = create(:user_content_completion,completable: c2,
                    user: @user, state: 'completed',completed_percentage: 100)
    c2_ucc.uncomplete!
    c2_ucc.reload
    p1_ucc.reload
    assert_equal 0, c2_ucc.completed_percentage
    assert_equal 'initialized', c2_ucc.state
    assert_equal 50, p1_ucc.completed_percentage
  end
end

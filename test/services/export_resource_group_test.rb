require 'test_helper'

class ExportResourceGroupsTest < ActiveSupport::TestCase
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    @organization = create :organization
    @user = create :user, organization: @organization

    5.times do
      create :resource_group, creator: @user, organization: @organization
    end
    @groups = ResourceGroup.where(organization: @organization)

    @service = ExportResourceGroups.new(groups: @groups, user: @user)

  end

  test '#run sets s3_url' do
    @service.run

    assert @service.instance_variable_get(:@s3_url)
  end

  test '#prepare_rows exports and sends email' do
    rows = @service.send :prepare_rows
    assert_equal @groups.length, rows.length
    group = @groups[0]
    group_url = Rails.application.routes.url_helpers.link_course_url(group.id, host: @organization.home_page)
    assert_includes rows, [group.client_resource_id, group.name, group.description, group.image_url, group_url, 'deeplink', '', '']

    group = @groups[1]
    group_url = Rails.application.routes.url_helpers.link_course_url(group.id, host: @organization.home_page)
    assert_includes rows, [group.client_resource_id, group.name, group.description, group.image_url, group_url, 'deeplink', '', '']

    group = @groups[2]
    group_url = Rails.application.routes.url_helpers.link_course_url(group.id, host: @organization.home_page)
    assert_includes rows, [group.client_resource_id, group.name, group.description, group.image_url, group_url, 'deeplink', '', '']

    group = @groups[3]
    group_url = Rails.application.routes.url_helpers.link_course_url(group.id, host: @organization.home_page)
    assert_includes rows, [group.client_resource_id, group.name, group.description, group.image_url, group_url, 'deeplink', '', '']

    group = @groups[4]
    group_url = Rails.application.routes.url_helpers.link_course_url(group.id, host: @organization.home_page)
    assert_includes rows, [group.client_resource_id, group.name, group.description, group.image_url, group_url, 'deeplink', '', '']
  end
end

require 'test_helper'

class UserImport::IntegrationTest < ActiveSupport::TestCase
  def setup_csv_for_creation(send_email: false)
    options = {
      channel_ids:        [@channel_1.id],
      send_welcome_email: send_email,
      team_ids:           [@tech_team.id, @engg_team.id]
    }

    @klass = BulkImportUsersV2.new(
      file_id:  @invitation_file.id,
      options:  options,
      admin_id: @admin_user.id
    )
    @klass.import
  end
  
  def existing_user_import(send_email: false)
    additional_information = {
      'picture_url': 'http://breelio.com/wp-content/uploads/2014/06/sample1.jpg',
      'password': 'Password@123',
      'language': 'en' 
    }

    @user_import_status = UserImportStatus.create(
      invitation_file_id: @invitation_file.id,
      first_name: @existing_user.first_name,
      last_name: @existing_user.last_name,
      email: @existing_user.email,
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id],
      status: 'pending',
      custom_fields: {'burrito_wrap': 'juarez', 'cheese': 'mozzarella', 'tea': 'green'},
      additional_information: additional_information
    )
    UserImport.new(user_import_id: @user_import_status.id, admin_id: @admin_user.id).import
  end
    
  setup do
    Config.any_instance.stubs :create_okta_group_config

    User.any_instance.stubs(:update_details_to_okta).returns true
    stub_request(:get, 'http://breelio.com/wp-content/uploads/2014/06/sample1.jpg').
      with(headers: { 'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Ruby' }).
      to_return(status: 200, body: '', headers: {})

    @organization  = create :organization
    @admin_user    = create :user, organization: @organization, organization_role: 'admin'

    # create couple of channels
    @channel_1     = create :channel, organization: @organization, user: @admin_user
    @channel_2     = create :channel, organization: @organization, user: @admin_user

    # create couple of teams/groups
    @tech_team     = create :team, organization: @organization, name: 'Technology'
    @engg_team     = create :team, organization: @organization, name: 'Engg'

    # create an end-user
    @existing_user = create :user, organization: @organization, is_complete: false, email: 'userexists@edcast.com'

    # define few custom fields in an organization
    @cf_1 = create :custom_field, organization: @organization, display_name: 'Burrito Wrap', abbreviation: nil
    @cf_2 = create :custom_field, organization: @organization, display_name: 'Cheese', abbreviation: nil
    @cf_3 = create :custom_field, organization: @organization, display_name: 'Tea', abbreviation: nil

    # adds custom fields to existing user
    create :user_custom_field, custom_field: @cf_1, user: @existing_user, value: 'juarez'
    create :user_custom_field, custom_field: @cf_2, user: @existing_user, value: 'pocho'

    csv_rows = File.open("#{Rails.root}/test/fixtures/files/users_with_custom_fields.csv").read

    @invitation_file = create(:invitation_file,
      sender_id: @admin_user.id,
      invitable: @organization,
      data: csv_rows,
      send_invite_email: true,
      roles: 'member')

    stub_request(:post, "https://api.branch.io/v1/url").to_return(status: 200, body: "{\"url\":\"http://bitly.co}", headers: {})
  end
  
  test 'sets `is_complete` as TRUE for existing users' do
    existing_user_import
    assert @existing_user.reload.is_complete, true
  end
  
  test 'sets `successful` status of user_import_status after importing existing user' do 
    existing_user_import
    assert @user_import_status.reload.status, 'successful'
  end
 
  test 'should not update blank or nil values when processing bulk upload for existing users' do
    existing_user = create :user, organization: @organization, is_complete: false, email: 'edcastuser@edcast.com',
                            first_name: 'FirstName',last_name: 'LastName', default_team_id: @tech_team.id

    csv_rows = "first_name,last_name,email,groups\n,,edcastuser@edcast.com,"
    invitation_file = create(:invitation_file, sender_id: @admin_user.id,
                             invitable: @organization, data: csv_rows, roles: "member")
    @user_import_status = UserImportStatus.create(
      invitation_file_id: invitation_file.id,
      first_name: '',
      last_name: '',
      email: existing_user.email,
      custom_fields: {'burrito_wrap': 'juarez', 'cheese': 'mozzarella', 'tea': 'green'},
      additional_information: {
        default_group: ''
      }
    )
    UserImport.new(user_import_id: @user_import_status.id, admin_id: @admin_user.id).import

    assert_equal existing_user.reload.first_name, 'FirstName'
    assert_equal existing_user.reload.last_name, 'LastName'
    assert_equal existing_user.reload.default_team_id, @tech_team.id
    assert existing_user.reload.is_complete
  end

  test 'update first name and last name when processing bulk upload for existing users' do
    existing_user = create :user, organization: @organization, is_complete: false, email: 'edcastuser@edcast.com'
    csv_rows = "first_name,last_name,email,groups\nFirstName,LastName,edcastuser@edcast.com,Technology"
    invitation_file = create(:invitation_file, sender_id: @admin_user.id,
                             invitable: @organization, data: csv_rows, roles: "member")
    @user_import_status = UserImportStatus.create(
      invitation_file_id: invitation_file.id,
      first_name: 'FirstName',
      last_name: 'LastName',
      email: existing_user.email,
      custom_fields: {'burrito_wrap': 'juarez', 'cheese': 'mozzarella', 'tea': 'green'},
      additional_information: {}
    )
    UserImport.new(user_import_id: @user_import_status.id, admin_id: @admin_user.id).import

    assert_equal existing_user.reload.first_name, 'FirstName'
    assert_equal existing_user.reload.last_name, 'LastName'
    assert existing_user.reload.is_complete
  end

  test 'add profile when processing bulk upload for the existing users' do
    existing_user = create :user, organization: @organization, is_complete: false, email: 'edcastuser@edcast.com'
    csv_rows = "first_name,last_name,email,groups,language\nFirstName,LastName,edcastuser@edcast.com,Technology,lt-LT"
    invitation_file = create(:invitation_file, sender_id: @admin_user.id,
                             invitable: @organization, data: csv_rows, roles: 'member')
    @user_import_status = UserImportStatus.create(
      invitation_file_id: invitation_file.id,
      first_name: 'FirstName',
      last_name: 'LastName',
      email: existing_user.email,
      custom_fields: {'burrito_wrap': 'juarez', 'cheese': 'mozzarella', 'tea': 'green'},
      additional_information: {'language': 'lt-LT'}
    )
    UserImport.new(user_import_id: @user_import_status.id, admin_id: @admin_user.id).import

    assert existing_user.reload.profile
    assert_equal 'lt-LT', existing_user.reload.profile.language
  end

  test 'update profile when processing bulk upload for the existing profiles' do
    existing_user = create :user, organization: @organization, is_complete: false, email: 'edcastuser@edcast.com'
    profile  = create :user_profile, user: existing_user, language: 'lt-LT'

    csv_rows = "first_name,last_name,email,groups,language\nFirstName,LastName,edcastuser@edcast.com,Technology,en"
    invitation_file = create(:invitation_file, sender_id: @admin_user.id,
                             invitable: @organization, data: csv_rows, roles: 'member')
    @user_import_status = UserImportStatus.create(
      invitation_file_id: invitation_file.id,
      first_name: 'FirstName',
      last_name: 'LastName',
      email: existing_user.email,
      custom_fields: {'burrito_wrap': 'juarez', 'cheese': 'mozzarella', 'tea': 'green'},
      additional_information: {'language': 'en'}
    )
    UserImport.new(user_import_id: @user_import_status.id, admin_id: @admin_user.id).import

    assert_equal 'en', existing_user.reload.profile.language
  end

  test 'adds members to newly created teams' do
    setup_csv_for_creation

    # [Art] are the teams that didn't exist before import
    @organization.reload

    teams = @organization.teams

    art_team  = teams.find_by(name: 'Art')

    @user_import_status = UserImportStatus.create(
      invitation_file_id: @invitation_file.id,
      first_name: 'FirstName2',
      last_name: 'LastName2',
      email: 'name2@example.com',
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id, art_team.id],
      status: 'pending',
      custom_fields: {'burrito_wrap': 'juarez', 'cheese': 'mozzarella', 'tea': 'green'},
      additional_information: {}
    )
    UserImport.new(user_import_id: @user_import_status.id, admin_id: @admin_user.id).import
    
    users = @organization.users
    
    user_1 = users.find_by(email: 'name2@example.com')
    assert_equal [user_1.id], art_team.members.pluck(:id)
  end

  test 'adds admin user as `ADMIN` to newly created teams' do
    setup_csv_for_creation

    # [Programming, Art] are the teams that didn't exist before import
    @organization.reload
    teams = @organization.teams

    art_team  = teams.find_by(name: 'Art')
    pgmg_team = teams.find_by(name: 'Programming')

    assert_equal [@admin_user.id], pgmg_team.admins.pluck(:id)
    assert_equal [@admin_user.id], art_team.admins.pluck(:id)
  end

  test 'adds users to teams' do
    setup_csv_for_creation

    teams = @organization.teams
    
    pgmg_team = teams.find_by(name: 'Programming')
    
    @user_import_status = UserImportStatus.create(
      invitation_file_id: @invitation_file.id,
      first_name: 'FirstName1',
      last_name: 'LastName1',
      email: 'name1@example.com',
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id, pgmg_team.id],
      status: 'pending',
      custom_fields: {'burrito_wrap': 'juarez', 'cheese': 'mozzarella', 'tea': 'green'},
      additional_information: {}
    )
    UserImport.new(user_import_id: @user_import_status.id, admin_id: @admin_user.id).import
    
    users = @organization.users

    user1 = users.find_by(email: 'name1@example.com')
    

    # Add unique teams to users. Must club teams from CSV and the ones added from UI while importing CSV
    assert_equal [pgmg_team.id, @tech_team.id, @engg_team.id].to_set, user1.teams.pluck(:id).to_set 
  end

  test 'adds users to channels' do
    setup_csv_for_creation
    
    @user_import_status = UserImportStatus.create(
      invitation_file_id: @invitation_file.id,
      first_name: 'FirstName1',
      last_name: 'LastName1',
      email: 'name1@example.com',
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id],
      status: 'pending',
      custom_fields: {'burrito_wrap': 'juarez', 'cheese': 'mozzarella', 'tea': 'green'},
      additional_information: {}
    )
    UserImport.new(user_import_id: @user_import_status.id, admin_id: @admin_user.id).import
    
    users = @organization.users

    user1 = users.find_by(email: 'name1@example.com')

    assert_equal [user1.id].to_set , @channel_1.followers.pluck(:id).to_set
  end

  test 'does not add channels when no channel_ids supplied' do
    setup_csv_for_creation

    @user_import_status = UserImportStatus.create(
      invitation_file_id: @invitation_file.id,
      first_name: 'FirstName1',
      last_name: 'LastName1',
      email: 'name1@example.com',
      channel_ids: nil,
      team_ids: [@tech_team.id, @engg_team.id],
      status: 'pending',
      custom_fields: {'burrito_wrap': 'juarez', 'cheese': 'mozzarella', 'tea': 'green'},
      additional_information: {}
    )
    UserImport.new(user_import_id: @user_import_status.id, admin_id: @admin_user.id).import

    assert_empty @channel_2.followers
  end

  test 'sends welcome-email to imported users' do
    setup_csv_for_creation

    mailer_mock = mock()
    mailer_mock.expects(:deliver_now).returns(nil).once
    DeviseMandrillMailer.expects(:send_import_user_welcome_email).returns(mailer_mock).once

    @user_import_status = UserImportStatus.create(
      invitation_file_id: @invitation_file.id,
      first_name: 'FirstName5',
      last_name: 'LastName5',
      email: 'name5@example.com',
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id],
      status: 'pending',
      custom_fields: {'burrito_wrap': 'juarez', 'cheese': 'mozzarella', 'tea': 'green'},
      additional_information: {}
    )
    UserImport.new(user_import_id: @user_import_status.id, admin_id: @admin_user.id).import
  end

  test 'assigns custom fields and values of users' do
    setup_csv_for_creation
    
    @user_import_status = UserImportStatus.create(
      invitation_file_id: @invitation_file.id,
      first_name: 'FirstName1',
      last_name: 'LastName1',
      email: 'name1@example.com',
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id],
      status: 'pending',
      custom_fields: {'burrito_wrap': 'juarez', 'cheese': 'mozzarella', 'tea': 'green'},
      additional_information: {}
    )
    UserImport.new(user_import_id: @user_import_status.id, admin_id: @admin_user.id).import

    @existing_user_import_status = UserImportStatus.create(
      invitation_file_id: @invitation_file.id,
      first_name: @existing_user.first_name,
      last_name: @existing_user.last_name,
      email: @existing_user.email,
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id],
      status: 'pending',
      custom_fields: {},
      additional_information: {}
    )
    UserImport.new(user_import_id: @existing_user_import_status.id, admin_id: @admin_user.id).import

    users = @organization.reload.users

    user_1 = users.find_by(email: 'name1@example.com')
    

    # when all of the custom fields are assigned
    assert_equal ['burrito_wrap', 'cheese', 'tea'].to_set, user_1.custom_fields.pluck(:abbreviation).to_set
    assert_equal 'juarez', UserCustomField.find_by(custom_field: @cf_1, user: user_1).value
    assert_equal 'mozzarella', UserCustomField.find_by(custom_field: @cf_2, user: user_1).value
    assert_equal 'green', UserCustomField.find_by(custom_field: @cf_3, user: user_1).value

    

    # never assigns blank value to existing custom field
    assert_equal 'pocho', UserCustomField.find_by(custom_field: @cf_2, user: @existing_user).value
  end

  test 'sets `password_reset_required` as TRUE when password is set in CSV' do
    setup_csv_for_creation
    
    @organization.reload

    teams = @organization.teams

    pgmg_team = teams.find_by(name: 'Programming')

    @user1_import_status = UserImportStatus.create(
      invitation_file_id: @invitation_file.id,
      first_name: 'FirstName1',
      last_name: 'LastName1',
      email: 'name1@example.com',
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id, pgmg_team.id],
      custom_fields: {'burrito_wrap': 'juarez', 'cheese': 'mozzarella', 'tea': 'green'},
      additional_information: {'password': 'Password@123'}
    )
    UserImport.new(user_import_id: @user1_import_status.id, admin_id: @admin_user.id).import
    
    @user3_import_status = UserImportStatus.create(
      invitation_file_id: @invitation_file.id,
      first_name: 'FirstName3',
      last_name: 'LastName3',
      email: 'name3@example.com',
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id],
      custom_fields: {'burrito_wrap': 'mission'},
      additional_information: {}
    )
    UserImport.new(user_import_id: @user3_import_status.id, admin_id: @admin_user.id).import

    users = @organization.reload.users

    user1 = users.find_by(email: 'name1@example.com')
    user3 = users.find_by(email: 'name3@example.com')

    assert_not user1.password_reset_required
    assert user3.password_reset_required
  end

  test 'resends welcome-email to existing users who have not completed onboarding and count resent mail' do
    User.any_instance.unstub :generate_user_onboarding
    # onboarding is in `new` state
    user1 = create(:user, :with_onboarding_incomplete, organization: @organization, email: 'name1@example.com', is_complete: false)

    # onboarding is in `started` state
    user2 = create(:user, :with_onboarding_incomplete, organization: @organization, email: 'name2@example.com', is_complete: false)
    user2.user_onboarding.start!

    # no onboarding present
    user3 = create(:user, :with_onboarding_complete, organization: @organization, email: 'name3@example.com')
    user3.user_onboarding.destroy

    # onboarding is in `completed` state
    user4 = create(:user, :with_onboarding_complete, organization: @organization, email: 'name4@example.com')

    mailer_mock = mock()
    mailer_mock.expects(:deliver_now).returns(nil).times(3)
    DeviseMandrillMailer.expects(:send_import_user_welcome_email).returns(mailer_mock).times(3)

    @user1_import_status = UserImportStatus.create(
      invitation_file_id: @invitation_file.id,
      first_name: user1.first_name,
      last_name: user1.last_name,
      email: user1.email,
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id],
      custom_fields: {'burrito_wrap': 'juarez', 'cheese': 'mozzarella', 'tea': 'green'},
      additional_information: {'password': 'Password@123'}
    )
    UserImport.new(user_import_id: @user1_import_status.id, admin_id: @admin_user.id).import

    @user2_import_status = UserImportStatus.create(
      invitation_file_id: @invitation_file.id,
      first_name: user2.first_name,
      last_name: user2.last_name,
      email: user2.email,
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id],
      custom_fields: {'burrito_wrap': 'juarez', 'cheese': 'mozzarella', 'tea': 'green'},
      additional_information: {'password': 'Password@123'}
    )
    UserImport.new(user_import_id: @user2_import_status.id, admin_id: @admin_user.id).import

    @user3_import_status = UserImportStatus.create(
      invitation_file_id: @invitation_file.id,
      first_name: user3.first_name,
      last_name: user3.last_name,
      email: user3.email,
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id],
      custom_fields: {'burrito_wrap': 'juarez', 'cheese': 'mozzarella', 'tea': 'green'},
      additional_information: {'password': 'Password@123'}
    )
    UserImport.new(user_import_id: @user3_import_status.id, admin_id: @admin_user.id).import

    @user4_import_status = UserImportStatus.create(
      invitation_file_id: @invitation_file.id,
      first_name: user4.first_name,
      last_name: user4.last_name,
      email: user4.email,
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id],
      custom_fields: {'burrito_wrap': 'juarez', 'cheese': 'mozzarella', 'tea': 'green'},
      additional_information: {'password': 'Password@123'}
    )
    UserImport.new(user_import_id: @user4_import_status.id, admin_id: @admin_user.id).import

    assert_equal 3, UserImportStatus.where(resent_email: true).count
  end

  test 'does not provisions user to Okta if organization do not have Okta enabled' do
    User.any_instance.expects(:provision_and_sign_in).never
    
    @user_import_status = UserImportStatus.create(
      invitation_file_id: @invitation_file.id,
      first_name: 'FirstName1',
      last_name: 'LastName1',
      email: 'name1@example.com',
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id],
      custom_fields: {'burrito_wrap': 'juarez', 'cheese': 'mozzarella', 'tea': 'green'},
      additional_information: {'password': 'Password@123'}
    )
    UserImport.new(user_import_id: @user_import_status.id, admin_id: @admin_user.id).import

  end

  test 'provisions user to Okta if organization has Okta enabled and password is configured in CSV' do
    User.any_instance.stubs :update_details_to_okta
    create :config, :boolean, configable: @organization, name: 'app.config.okta.enabled', value: 'true'
    create :config, configable: @organization, name: 'OktaApiToken', value: SecureRandom.hex

    @organization.reload

    User.any_instance.expects(:provision_and_sign_in).times(1)

    @user_import_status = UserImportStatus.create(
      invitation_file_id: @invitation_file.id,
      first_name: 'FirstName1',
      last_name: 'LastName1',
      email: 'name1@example.com',
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id],
      custom_fields: {'burrito_wrap': 'juarez', 'cheese': 'mozzarella', 'tea': 'green'},
      additional_information: {'password': 'Password@123'}
    )
    UserImport.new(user_import_id: @user_import_status.id, admin_id: @admin_user.id).import

  end
  
  test 'provisions user to Okta if organization has Okta enabled and password is not configured in CSV' do
    create :config, :boolean, configable: @organization, name: 'app.config.okta.enabled', value: 'true'
    create :config, configable: @organization, name: 'OktaApiToken', value: SecureRandom.hex

    @organization.reload

    User.any_instance.expects(:provision_and_sign_in).with(has_entry(:args, includes('password'))).once
    @user_import_status = UserImportStatus.create(
      invitation_file_id: @invitation_file.id,
      first_name: 'FirstName1',
      last_name: 'LastName1',
      email: 'name1@example.com',
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id],
      custom_fields: {'burrito_wrap': 'juarez', 'cheese': 'mozzarella', 'tea': 'green'},
      additional_information: {}
    )
    UserImport.new(user_import_id: @user_import_status.id, admin_id: @admin_user.id).import
  end

  test 'fallback to local URL if provisioning requests fails' do
    create_default_ssos
    Organization.any_instance.unstub :default_sso
    @organization.send :default_sso

    stub_failed_user_creation

    create :config, :boolean, configable: @organization, name: 'app.config.okta.enabled', value: 'true'
    create :config, configable: @organization, name: 'OktaApiToken', value: SecureRandom.hex

    @organization.reload

    # Users do not have password in CSV
    mailer_mock = mock()
    mailer_mock.expects(:deliver_now).returns(nil).times(1)
    DeviseMandrillMailer.expects(:send_import_user_welcome_email)
    .with(has_entry(:redirect_url, includes('/api/v2/users/login_landing')))
    .returns(mailer_mock).times(1)

    @user_import_status = UserImportStatus.create(
      invitation_file_id: @invitation_file.id,
      first_name: 'FirstName1',
      last_name: 'LastName1',
      email: 'name1@example.com',
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id],
      custom_fields: {'burrito_wrap': 'juarez', 'cheese': 'mozzarella', 'tea': 'green'},
      additional_information: {}
    )
    UserImport.new(user_import_id: @user_import_status.id, admin_id: @admin_user.id).import
  end

  test 'federated sessionToken URL is generated if provisioning requests succeeds and password supplied in CSV' do
    stub_okta_user

    create :config, :boolean, configable: @organization, name: 'app.config.okta.enabled', value: 'true'
    create :config, configable: @organization, name: 'OktaApiToken', value: SecureRandom.hex

    @organization.reload

    mailer_mock1 = mock()
    mailer_mock1.expects(:deliver_now).returns(nil).times(1)
    DeviseMandrillMailer
      .expects(:send_import_user_welcome_email)
      .with(has_entry(:redirect_url, includes('https://dev-579575.oktapreview.com/login/sessionCookieRedirect')))
      .returns(mailer_mock1)
      .times(1)

    @user_import_status = UserImportStatus.create(
      invitation_file_id: @invitation_file.id,
      first_name: 'FirstName1',
      last_name: 'LastName1',
      email: 'name1@example.com',
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id],
      custom_fields: {'burrito_wrap': 'juarez', 'cheese': 'mozzarella', 'tea': 'green'},
      additional_information: {'password': 'Password@123'}
    )
    UserImport.new(user_import_id: @user_import_status.id, admin_id: @admin_user.id).import
  end

  test 'sends default first and last name to federated system' do
    create :config, :boolean, configable: @organization, name: 'app.config.okta.enabled', value: 'true'
    api_token = create :config, configable: @organization, name: 'OktaApiToken', value: SecureRandom.hex

    Okta::InternalFederation.any_instance.stubs(:federated_user).returns(nil)
    user_json = File.read(Rails.root.join('test/fixtures/api/okta_user.json'))
    stub_okta_session_token
    stub_okta_user_password_update
    
    mailer_mock = mock()
    mailer_mock.expects(:deliver_now).returns(nil).once
    DeviseMandrillMailer.expects(:send_import_user_welcome_email).returns(mailer_mock).once

    stub_request(:post, "https://dev-579575.oktapreview.com/api/v1/users?activate=true").
      with(body: {
        "profile": {
          "firstName": "First name",
          "lastName": "Last name",
          "email": "name1@example.com",
          "login": "#{@organization.id}-name1@example.com"
        },
        "credentials": {
          "password": {
            "value": "Password@123"
          }
        }
      }.to_json,
      headers: {
        'Accept' => 'application/json',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'Authorization' => "SSWS #{api_token.value}",
        'Content-Type' => 'application/json',
        'User-Agent' => 'Faraday v0.9.2'
      }).
      to_return(status: 200, body: user_json, headers: {})
    stub_request(:post, "https://api.branch.io/v1/url")
  
    @user_import_status = UserImportStatus.create(
      invitation_file_id: @invitation_file.id,
      first_name: 'First Name',
      last_name: 'Last Name',
      email: 'name1@example.com',
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id],
      custom_fields: {'burrito_wrap': 'juarez', 'cheese': 'mozzarella', 'tea': 'green'},
      additional_information: {'password': 'Password@123'}
    )
    UserImport.new(user_import_id: @user_import_status.id, admin_id: @admin_user.id).import
  end

  test 'bulk_import should handle default_group for user' do
    csv_rows = 'First Name,Last Name,Email,Groups,"Picture Url",Password,"Burrito Wrap",Cheese,tea,"Default Group"
                RegularUser,RegularLastName,regular_email@mail.com,"Programming,Technology",,,,,,Technology
                RegularUser2,RegularLastName2,regular_email2@mail.com,"Programming,Technology",,,,,,NewOne'

    invitation_file = create(:invitation_file, sender_id: @admin_user.id, invitable: @organization,
                             data: csv_rows, send_invite_email: true, roles: 'member')

    options = {
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id]
    }

    BulkImportUsersV2.new(
      file_id:  invitation_file.id,
      options:  options,
      admin_id: @admin_user.id
    ).import
    
    @user1_import_status = UserImportStatus.create(
      invitation_file_id: invitation_file.id,
      first_name: 'RegularUser',
      last_name: 'RegularLastName',
      email: 'regular_email@mail.com',
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id],
      custom_fields: {},
      additional_information: {'default_group': 'Technology'}
    )
    UserImport.new(user_import_id: @user1_import_status.id, admin_id: @admin_user.id).import
    
    @organization.reload

    teams = @organization.teams

    newone_team = teams.find_by(name: 'NewOne')

    @user2_import_status = UserImportStatus.create(
      invitation_file_id: invitation_file.id,
      first_name: 'RegularUser2',
      last_name: 'RegularLastName2',
      email: 'regular_email2@mail.com',
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id, newone_team.id],
      custom_fields: {},
      additional_information: {'default_group': 'NewOne'}
    )
    UserImport.new(user_import_id: @user2_import_status.id, admin_id: @admin_user.id).import

    usr = @organization.users.find_by(email: 'regular_email@mail.com')
    usr2 = @organization.users.find_by(email: 'regular_email2@mail.com')
    default_group = @organization.teams.find_by(name: 'Technology')
    default_group2 = @organization.teams.find_by(name: 'NewOne')

    assert usr
    assert usr2

    assert_equal usr.default_team_id, default_group.id
    assert_equal usr2.default_team_id, default_group2.id

    assert default_group.users.include?(usr)
    assert default_group2.users.include?(usr2)
  end

  test 'bulk_import should give count of new user count, existing user count, failed user count, total user count of user' do
    csv_rows = 'First Name,Last Name,Email,Groups,"Picture Url",Password,"Burrito Wrap",Cheese,tea,"Default Group"
                RegularUser,RegularLastName,regular_email@mail.com,"Programming,Technology",,,,,,Technology
                RegularUser2,RegularLastName2,regular_email2@mail.com,"Programming,Technology",,,,,,NewOne
                RegularUser3,RegularLastName3,regular_email3@mail.com,"Programming,Technology",,password@123,,,,NewOne'

    invitation_file = create(:invitation_file, sender_id: @admin_user.id, invitable: @organization,
                             data: csv_rows, send_invite_email: true, roles: 'member')

    additional_information = {
      'picture_url': 'http://breelio.com/wp-content/uploads/2014/06/sample1.jpg',
      'password': 'Password@123',
      'language': 'en' 
    }
    
    @user_import_status = UserImportStatus.create(
      invitation_file_id: invitation_file.id,
      first_name: @existing_user.first_name,
      last_name: @existing_user.last_name,
      email: @existing_user.email,
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id],
      status: 'pending',
      custom_fields: {'burrito_wrap': 'juarez', 'cheese': 'mozzarella', 'tea': 'green'},
      additional_information: additional_information
    )
    UserImport.new(user_import_id: @user_import_status.id, admin_id: @admin_user.id).import
    
    @user1_import_status = UserImportStatus.create(
      invitation_file_id: invitation_file.id,
      first_name: 'RegularUser',
      last_name: 'RegularLastName',
      email: 'regular_email@mail.com',
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id],
      custom_fields: {},
      additional_information: {'default_group': 'Technology'}
    )
    UserImport.new(user_import_id: @user1_import_status.id, admin_id: @admin_user.id).import

    @user2_import_status = UserImportStatus.create(
      invitation_file_id: invitation_file.id,
      first_name: 'RegularUser2',
      last_name: 'RegularLastName2',
      email: 'regular_email2@mail.com',
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id],
      custom_fields: {},
      additional_information: {'default_group': 'NewOne'}
    )
    UserImport.new(user_import_id: @user2_import_status.id, admin_id: @admin_user.id).import
    
    @user3_import_status = UserImportStatus.create(
      invitation_file_id: invitation_file.id,
      first_name: 'RegularUser3',
      last_name: 'RegularLastName3',
      email: 'regular_email3@mail.com',
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id],
      custom_fields: {},
      additional_information: {'default_group': 'NewOne', 'password': 'password@123'}
    )
    UserImport.new(user_import_id: @user3_import_status.id, admin_id: @admin_user.id).import

    old_users_count = UserImportStatus.where(existing_user: true).count

    new_users_count = UserImportStatus.where(existing_user: false).count

    failed_users_count = UserImportStatus.where(status: 'error').count

    total_users_count = UserImportStatus.where(invitation_file_id: invitation_file.id).count

    assert_equal 1, old_users_count
    assert_equal 2, new_users_count
    assert_equal 1, failed_users_count
    assert_equal 4, total_users_count
  end

  test 'bulk_import should clear channels followers count cache' do
    csv_rows = 'First Name,Last Name,Email,Groups,"Picture Url",Password,"Burrito Wrap",Cheese,tea,"Default Group"
                RegularUser,RegularLastName,regular_email@mail.com,"Programming,Technology",,,,,,Technology
                RegularUser2,RegularLastName2,regular_email2@mail.com,"Programming,Technology",,,,,,NewOne'

    invitation_file = create(:invitation_file, sender_id: @admin_user.id, invitable: @organization,
                             data: csv_rows, send_invite_email: true, roles: 'member')

    options = {
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id]
    }

    BulkImportUsersV2.new(
      file_id:  invitation_file.id,
      options:  options,
      admin_id: @admin_user.id
    ).import
    Rails.cache = ActiveSupport::Cache::MemoryStore.new
    create(:teams_channels_follow, team: @tech_team, channel: @channel_1)
    create(:teams_channels_follow, team: @engg_team, channel: @channel_1)

    Rails.cache.fetch("channel_#{@channel_1.id}_followers_count".to_sym) {8}
    assert_equal 8, Rails.cache.read("channel_#{@channel_1.id}_followers_count".to_sym)

    @user1_import_status = UserImportStatus.create(
      invitation_file_id: invitation_file.id,
      first_name: 'RegularUser',
      last_name: 'RegularLastName',
      email: 'regular_email@mail.com',
      channel_ids: [@channel_1.id],
      team_ids: [@tech_team.id, @engg_team.id],
      custom_fields: {},
      additional_information: {'default_group': 'Technology'}
    )
    UserImport.new(user_import_id: @user1_import_status.id, admin_id: @admin_user.id).import

    assert_equal nil, Rails.cache.read("channel_#{@channel_1.id}_followers_count".to_sym)
  end

  test 'trigger UserBulkUploadWorkflowJob after bulk import is done' do
    EdcastActiveJob.unstub :perform_later
    UserBulkUploadWorkflowJob.unstub :perform_later
    create :config, configable: @organization, value: true, data_type: 'boolean', name: 'simplified_workflow'
    UserBulkUploadWorkflowJob.expects(:perform_later).never
    setup_csv_for_creation

    emails = UserImportStatus.pluck(:email)
    expected_emails = %w[name1@example.com name2@example.com name3@example.com name4@example.com userexists@edcast.com]
    assert_same_elements expected_emails, emails

    user_import_status1 = UserImportStatus.find_by(email: expected_emails[0])
    user_import_status1.update_columns(status: UserImportStatus::SUCCESS)
    user_import = UserImport.new(
      user_import_id: user_import_status1.id,
      admin_id: @admin_user.id,
      user_import_metadata: {total_csv_users: 5, user_import_counter: 1}
    )
    user_import.import
    assert_includes user_import.instance_variables, :@total_csv_users
    assert_includes user_import.instance_variables, :@user_import_counter
    assert_equal 5, user_import.instance_variable_get(:@total_csv_users)
    assert_equal 1, user_import.instance_variable_get(:@user_import_counter)

    user_import_status2 = UserImportStatus.find_by(email: expected_emails[1])
    user_import_status2.update_columns(status: UserImportStatus::SUCCESS)
    UserImport.new(
      user_import_id: user_import_status2.id,
      admin_id: @admin_user.id,
      user_import_metadata: {total_csv_users: 5, user_import_counter: 2}
    ).import

    user_import_status3 = UserImportStatus.find_by(email: expected_emails[2])
    user_import_status3.update_columns(status: UserImportStatus::SUCCESS)
    UserImport.new(
      user_import_id: user_import_status3.id,
      admin_id: @admin_user.id,
      user_import_metadata: {total_csv_users: 5, user_import_counter: 3}
    ).import

    user_import_status4 = UserImportStatus.find_by(email: expected_emails[3])
    user_import_status4.update_columns(status: UserImportStatus::SUCCESS)
    UserImport.new(
      user_import_id: user_import_status4.id,
      admin_id: @admin_user.id,
      user_import_metadata: {total_csv_users: 5, user_import_counter: 4}
    ).import

    user_import_status5 = UserImportStatus.find_by(email: expected_emails[4])
    user_import_status5.update_columns(status: UserImportStatus::SUCCESS)
    UserBulkUploadWorkflowJob.expects(:perform_later).once
    user_import = UserImport.new(
      user_import_id: user_import_status5.id,
      admin_id: @admin_user.id,
      user_import_metadata: {total_csv_users: 5, user_import_counter: 5}
    )
    assert_includes user_import.instance_variables, :@total_csv_users
    assert_includes user_import.instance_variables, :@user_import_counter
    assert_equal 5, user_import.instance_variable_get(:@total_csv_users)
    assert_equal 5, user_import.instance_variable_get(:@user_import_counter)
    user_import.import
  end
end

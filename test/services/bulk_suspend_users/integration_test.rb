require 'test_helper'

class BulkSuspendUsers::IntegrationTest < ActiveSupport::TestCase

  setup do

    @organization  = create :organization
    @admin_user    = create :user, organization: @organization, organization_role: 'admin'

    @user1 = create :user, email: 'test@edca.com',organization: @organization, organization_role: 'member', status: 'active'
    @user2 = create :user, email: 'test2@edca.com',organization: @organization, organization_role: 'member', status: 'inactive'
    @user3 = create :user, email: 'test3@edca.com',organization: @organization, organization_role: 'member', status: 'suspended'

    csv_rows = "email,status\ntest@edca.com,suspend\ntest2@edca.com,active\ntest3@edca.com,inactive"

    @invitation_file = create(:invitation_file,
      sender_id: @admin_user.id,
      invitable: @organization,
      data: csv_rows,
      roles: 'member')
  end

  def import_csv
    @klass = BulkSuspendUsers.new(
      file_id:  @invitation_file.id,
      admin_id: @admin_user.id
    )
    @klass.import
  end

  #update_status
  test 'check whether user status gets updated ' do
    import_csv
    assert_equal 'suspended', @organization.reload.users.find_by(email: @user1.email).status
    assert_equal 'active', @organization.reload.users.find_by(email: @user2.email).status
    assert_equal 'inactive', @organization.reload.users.find_by(email: @user3.email).status
  end

  #preview_csv
  test 'should remove row if not valid email' do 
    csv_rows = "email,status\ntest@edca.com,suspend\ntest2@edca.com,active\ntest,inactive"

    @invitation_file = create(:invitation_file,
      sender_id: @admin_user.id,
      invitable: @organization,
      data: csv_rows,
      roles: 'member')

    user_records = BulkSuspendUsers.new(
      file_id: @invitation_file.id,
      admin_id: @admin_user.id
    ).preview_csv

    assert user_records.length, 2
    assert_not_equal user_records.last['email'],'test' 
  end

end

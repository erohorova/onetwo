require 'test_helper'

class RedisHashKeyTest < ActiveSupport::TestCase
  teardown do
    REDIS_CLIENT.del("test_key")
  end

  # Start tests for set_as_array
  #
  test "set_as_array: save value in redis if key is not present" do
    assert_equal 0, REDIS_CLIENT.hlen("test_key")
    RedisHashKey.new(key: "test_key", field: "test_field", value: "test_value").set_as_array
    assert_equal 1, REDIS_CLIENT.hlen("test_key")
  end

  test "set_as_array: saves value as array" do
    RedisHashKey.new(key: "test_key", field: "test_field", value: "test_value").set_as_array
    assert_equal ["test_value"], JSON.parse(REDIS_CLIENT.hget("test_key", "test_field"))
  end

  test "set_as_array: saves value when key is present but field is not present" do
    REDIS_CLIENT.hset("test_key", "test_field1", "test_value")
    RedisHashKey.new(key: "test_key", field: "test_field2", value: "test_value").set_as_array
    assert_equal 2, REDIS_CLIENT.hlen("test_key")
  end

  test "set_as_array: saves value in array when key and field both are present" do
    RedisHashKey.new(key: "test_key", field: "test_field", value: "test_value1").set_as_array
    RedisHashKey.new(key: "test_key", field: "test_field", value: "test_value2").set_as_array
    assert_equal ["test_value1", "test_value2"], JSON.parse(REDIS_CLIENT.hget("test_key", "test_field"))
  end
  # End tests for set_as_array
  #

  # Start tests for get_array_values
  #
  test "get_array_values: get array values stored in redis key" do
    RedisHashKey.new(key: "test_key", field: "test_field", value: "test_value1").set_as_array
    RedisHashKey.new(key: "test_key", field: "test_field", value: "test_value2").set_as_array

    result = RedisHashKey.new(key: "test_key", field: "test_field").get_array_values
    assert_equal ["test_value1", "test_value2"], result
  end

  test "get_array_values: returns nil if key is not present" do
    refute RedisHashKey.new(key: "test_key", field: "test_field").get_array_values
  end

  test "get_array_values: returns values stored in field" do
    REDIS_CLIENT.hset("test_key", "test_field", "test_value")
    assert_equal ["test_value"], RedisHashKey.new(key: "test_key", field: "test_field").get_array_values
  end
  #
  # End tests for get_array_values

  # Start test for clear
  #
  test "clear: deletes/clears the redis key" do
    REDIS_CLIENT.hset("test_key", "test_field", "test_value")
    RedisHashKey.new(key: "test_key").clear
    assert_equal 0, REDIS_CLIENT.hlen("test_key")
  end
  #
  # End test for clear
end

require 'test_helper'

class InstructorDigestReportServiceTest < ActiveSupport::TestCase
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(false)
    @org_1 = create(:organization)
    @org_2 = create(:organization)

    @user_1_for_org_1 = create :user, organization: @org_1
    @user_2_for_org_1 = create :user, organization: @org_1
    @user_1_for_org_2 = create :user, organization: @org_2

    # group as per language
    @rg_en_org_1 = create :resource_group, name: 'rg_en_org_1',  end_date: nil, organization: @org_1, language: 'en'
    @rg_fr_org_1 = create :resource_group, name: 'rg_fr_org_1',  end_date: nil, organization: @org_1, language: 'fr'

    @rg_en_org_1_closed = create :resource_group, name: 'rg_en_org_1_closed', end_date: nil, organization: @org_1, language: 'en', close_access: 1

    @rg_en_org_2 = create :resource_group, name: 'rg_en_org_2', end_date: nil, organization: @org_2, language: 'en'
    @rg_fr_org_2 = create :resource_group, name: 'rg_fr_org_2', end_date: nil, organization: @org_2, language: 'fr'

    # open group
    @rg_en_org_1.add_admin(@user_1_for_org_1)
    @rg_fr_org_1.add_admin(@user_1_for_org_1)
    @rg_en_org_1.add_member(@user_2_for_org_1)
    @rg_fr_org_1.add_member(@user_2_for_org_1)

    @rg_en_org_2.add_admin(@user_1_for_org_2)
    @rg_fr_org_2.add_admin(@user_1_for_org_2)

    # closed group
    @rg_en_org_1_closed.add_admin(@user_1_for_org_1)
    @rg_en_org_1_closed.add_admin(@user_2_for_org_1)

    @today     = Time.now.utc
    @later     = @today + 4.hours
    @yesterday = @today - 1.day
    @earlier   = @today - 7.hours

    @post_1    = create :post, user: @user_2_for_org_1, group: @rg_en_org_1, created_at: @earlier
    @post_2    = create :post, user: @user_2_for_org_1, group: @rg_en_org_1, created_at: @today
    @post_3    = create :post, user: @user_1_for_org_1, group: @rg_en_org_1, created_at: @yesterday
    @post_4    = create :post, user: @user_2_for_org_1, group: @rg_fr_org_1, created_at: @today
    @post_5    = create :post, user: @user_1_for_org_1, group: @rg_fr_org_1, created_at: @earlier

    @post_6    = create :post, user: @user_1_for_org_1, group: @rg_en_org_1_closed, created_at: @later
    @post_7    = create :post, user: @user_2_for_org_1, group: @rg_en_org_1_closed, created_at: @later

    @comment_1    = create :comment, user: @user_2_for_org_1, commentable: @post_1  , created_at: @earlier
    @comment_2    = create :comment, user: @user_2_for_org_1, commentable: @post_2  , created_at: @today
    @comment_5    = create :comment, user: @user_1_for_org_1, commentable: @post_5, created_at: @earlier


    @report_service = InstructorDigestReportService.new
  end

  test 'get instructors with resource groups of org' do
    instructors_for_org_1 = @report_service.instructors_with_resource_groups(@org_1.id)
    instructors_for_org_2 = @report_service.instructors_with_resource_groups(@org_2.id)

    assert_same_elements(
      [ {"id" => @user_1_for_org_1.id, "email" => @user_1_for_org_1.email, "group_ids" => @rg_en_org_1.id.to_s, "language" => 'en'},
        {"id" => @user_1_for_org_1.id, "email" => @user_1_for_org_1.email, "group_ids" => @rg_fr_org_1.id.to_s, "language" => 'fr'} ],
      instructors_for_org_1.map(&:attributes)
    )

    assert_same_elements(
      [ {"id" => @user_1_for_org_2.id, "email" => @user_1_for_org_2.email, "group_ids" => @rg_en_org_2.id.to_s, "language" => 'en'},
        {"id" => @user_1_for_org_2.id, "email" => @user_1_for_org_2.email, "group_ids" => @rg_fr_org_2.id.to_s, "language" => 'fr' } ],
      instructors_for_org_2.map(&:attributes)
    )
  end

  test 'get instructors of org' do
    instructors_for_org_1 = @report_service.instructors(@org_1.id)
    instructors_for_org_2 = @report_service.instructors(@org_2.id)

    assert_same_elements(
      [ {"id" => @user_1_for_org_1.id, "language" => @rg_en_org_1.language, "group_ids" => [@rg_en_org_1.id.to_s]},
        {"id" => @user_1_for_org_1.id, "language" => @rg_fr_org_1.language, "group_ids" => [@rg_fr_org_1.id.to_s]}],
      instructors_for_org_1
    )

    assert_same_elements(
      [ {"id" => @user_1_for_org_2.id, "language" => @rg_en_org_2.language, "group_ids" => [@rg_en_org_2.id.to_s]},
        {"id" => @user_1_for_org_2.id, "language" => @rg_fr_org_2.language, "group_ids" => [@rg_fr_org_2.id.to_s]} ],
      instructors_for_org_2
    )
  end

  test 'get posts of course' do
    posts = @report_service.posts_for(resource_group: @rg_en_org_1, from: @earlier, to: @later)
    assert_equal [@post_1, @post_2], posts.sort

    posts = @report_service.posts_for(resource_group: @rg_fr_org_1, from: @yesterday, to: @today)
    assert_equal [@post_5], posts
  end

  test 'get comments of course' do
    comments = @report_service.comments_for(resource_group: @rg_en_org_1, from: @earlier, to: @later)
    assert_equal [@comment_1, @comment_2], comments.sort

    comments = @report_service.comments_for(resource_group: @rg_fr_org_1, from: @yesterday, to: @today)
    assert_equal [@comment_5], comments
  end

  test 'get post rows for resource groups' do
    rows = @report_service.posts_rows_for(group_ids: [@rg_fr_org_1.id], from: @yesterday, to: @later)

    assert_same_elements([Post.url(@post_4, @rg_fr_org_1), Post.url(@post_5, @rg_fr_org_1)],
      rows.map { |r| r[6] })
  end

  test 'generate and mail report to instructor' do
    instructor_data = {
      "id"        => @user_1_for_org_1.id,
      "group_ids" => @rg_en_org_1.id.to_s,
      "language"  => 'en'
    }

    mail_args = []
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
        mail_args << {:first_name => message[:message][:global_merge_vars][0]['content'],
          :url => message[:message][:global_merge_vars][3]['content'],
          :language => message[:message][:global_merge_vars][4]['content'],
          :email => message[:message][:global_merge_vars][1]['content']
        }
    }.once
    InstructorDigestReportService.any_instance.stubs(:upload_report_to_s3).returns("http://www.abc.com")
    @report_service.generate_report(instructor_data: instructor_data, from: @earlier, to: @later)

    assert_equal  @user_1_for_org_1.first_name, mail_args[0][:first_name]
    assert_equal  'http://www.abc.com', mail_args[0][:url]
    assert_equal  'English', mail_args[0][:language]
    assert_equal  @user_1_for_org_1.mailable_email, mail_args[0][:email]
  end
end

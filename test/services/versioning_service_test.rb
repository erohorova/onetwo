require 'test_helper'

class VersioningServiceTest < ActiveSupport::TestCase
  test "get_klass" do
    assert_equal CardHistory, VersioningService.get_klass("card")
    assert_equal CardResourceHistory, VersioningService.get_klass("resource")

    exception = assert_raises(ArgumentError) { VersioningService.get_klass("test") }
    assert_equal( "Wrong entity passed", exception.message )
  end

  test "return card model changes" do
    # on create
    changes = {"title" => [nil, "test"], "message" => [nil, "message"]}
    CardHistory.expects(:create_new_version).with(1, 1, changes).once
    VersioningService.new(entity_type: 'card', entity_id: 1, changes: changes, user_id: 1).create_version

    # on update
    changes = {"title" => ["Text", "Test"], "message" => ["String", "Message"]}
    CardHistory.expects(:create_new_version).with(1, 1, changes).once
    VersioningService.new(entity_type: 'card', entity_id: 1, changes: changes, user_id: 1).create_version
  end

  test "skips attributes that are not whitelisted" do
    changes = {"title" => [nil, "test"], "votes_count" => [1, 2]}
    CardHistory.expects(:create_new_version).with(1, 1, changes.except("votes_count")).once
    VersioningService.new(entity_type: 'card', entity_id: 1, changes: changes, user_id: 1).create_version
  end

  test "logs error when entity_type is invalid" do
    PrefixLogger.any_instance.expects(:error).with("Wrong entity passed").once
    obj = VersioningService.new(entity_type: 'user', entity_id: 1, user_id: 1, changes: {"title" => ["1", "2"]})
    obj.create_version
  end

  test "doesnt call create_new_version if there is no updates to record" do
    changes = {"votes_count" => [1, 2], "promoted_at" => [nil, Time.now]}
    CardHistory.expects(:create_new_version).with(1, 1, changes).never
    VersioningService.new(entity_type: 'card', entity_id: 1, changes: changes, user_id: 1).create_version
  end
end

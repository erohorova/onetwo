# TODO: More coverage to come as per the enhancement.
require 'test_helper'

class BulkImportUsersV3IntegrationTest < ActiveSupport::TestCase
  setup do
    skip('Not yet released')

    connection = Rails.configuration.database_configuration[Rails.env]
    @client = Mysql2::Client.new(username: connection['username'], password: connection['password'], database: connection['database'], local_infile: true, socket: connection['socket'])

    @organization = create :organization
    @admin = create :user, organization_role: 'admin', organization_id: @organization.id
    @rows = "first_name,last_name,email,language\nFirstName1,LastName1,abc@example.com,en\nFirstName2,LastName2,def@example.com"
    @channel1 = create :channel, organization: @organization
    @channel2 = create :channel, organization: @organization, auto_follow: true
    @team = create :team, organization: @organization
    @everyone_team = create :team, organization: @organization, is_everyone_team: true
    @member_role = create :role, name: 'member', organization_id: @organization.id, master_role: 1, default_name: 'member'

    BulkHandleAssignmentJob.expects(:perform_later).once
    BulkReindexJob.expects(:perform_later).times(3)

    @klass = BulkImport::Runner.new(data: @rows, organization_id: @organization.id, opts: { channels: [@channel1.id], teams: [@team.id] })
    @klass.execute

    @users = @client.query("select * from users where email in ('abc@example.com', 'def@example.com') and organization_id = #{@organization.id}").entries
  end

  test 'creates user with no password' do
    assert_equal 2, @users.length

    user1 = @users.select { |u| u['email'] == 'abc@example.com' }[0]
    user2 = @users.select { |u| u['email'] == 'def@example.com' }[0]

    assert_equal 'FirstName1', user1['first_name']
    assert_equal 'LastName1', user1['last_name']
    assert_equal 'member', user1['organization_role']
    assert_equal 'active', user1['status']
    assert user1['created_at']
    assert user1['updated_at']
    assert user1['confirmed_at']
    assert_equal '', user1['encrypted_password']

    assert_equal 'FirstName2', user2['first_name']
    assert_equal 'LastName2', user2['last_name']
    assert_equal 'member', user2['organization_role']
    assert_equal 'active', user2['status']
    assert user2['created_at']
    assert user2['updated_at']
    assert user2['confirmed_at']
    assert_equal '', user2['encrypted_password']
  end

  test 'creates user profiles' do
    user1 = @users.select { |u| u['email'] == 'abc@example.com' }[0]
    user2 = @users.select { |u| u['email'] == 'def@example.com' }[0]

    profile1 = @client.query("select * from user_profiles where user_id in (#{user1['id']})").entries[0]
    profile2 = @client.query("select * from user_profiles where user_id in (#{user2['id']})").entries[0]

    assert_equal 'en', profile1['language']
    assert profile1['created_at']
    assert profile1['updated_at']
    assert_nil profile2
  end

  test 'initializes user onboarding' do
    user1 = @users.select { |u| u['email'] == 'abc@example.com' }[0]
    user2 = @users.select { |u| u['email'] == 'def@example.com' }[0]

    onboarding1 = @client.query("select * from user_onboardings where user_id in (#{user1['id']})").entries[0]
    onboarding2 = @client.query("select * from user_onboardings where user_id in (#{user2['id']})").entries[0]

    assert_equal 'new', onboarding1['status']
    assert_equal 'new', onboarding2['status']
    assert onboarding1['created_at']
    assert onboarding2['created_at']
    assert onboarding1['updated_at']
    assert onboarding2['updated_at']
  end

  test 'add users as followers to the channels to the added channels' do
    user1 = @users.select { |u| u['email'] == 'abc@example.com' }[0]
    user2 = @users.select { |u| u['email'] == 'def@example.com' }[0]

    followees1 = @client.query("select * from follows where user_id = #{user1['id']} and followable_id = #{@channel1.id} and followable_type = 'Channel'").entries[0]
    followees2 = @client.query("select * from follows where user_id = #{user2['id']} and followable_id = #{@channel1.id} and followable_type = 'Channel'").entries[0]

    assert followees1
    assert followees1['created_at']
    assert_nil followees1['deleted_at']

    assert followees2
    assert followees2['created_at']
    assert_nil followees2['deleted_at']
  end

  test 'add users as followers to the channels marked as auto-follow' do
    user1 = @users.select { |u| u['email'] == 'abc@example.com' }[0]
    user2 = @users.select { |u| u['email'] == 'def@example.com' }[0]

    followees1 = @client.query("select * from follows where user_id = #{user1['id']} and followable_id = #{@channel2.id} and followable_type = 'Channel'").entries[0]
    followees2 = @client.query("select * from follows where user_id = #{user2['id']} and followable_id = #{@channel2.id} and followable_type = 'Channel'").entries[0]

    assert followees1
    assert followees1['created_at']
    assert_nil followees1['deleted_at']

    assert followees2
    assert followees2['created_at']
    assert_nil followees2['deleted_at']
  end

  test 'set users as member in the organization' do
    user1 = @users.select { |u| u['email'] == 'abc@example.com' }[0]
    user2 = @users.select { |u| u['email'] == 'def@example.com' }[0]

    user_role1 = @client.query("select * from user_roles where user_id = #{user1['id']}").entries[0]
    user_role2 = @client.query("select * from user_roles where user_id = #{user2['id']}").entries[0]

    assert user_role1
    assert user_role2
  end

  test 'add users to the configured team' do
    user1 = @users.select { |u| u['email'] == 'abc@example.com' }[0]
    user2 = @users.select { |u| u['email'] == 'def@example.com' }[0]

    team_user1 = @client.query("select * from teams_users where user_id = #{user1['id']} and team_id = #{@team.id}").entries[0]
    team_user2 = @client.query("select * from teams_users where user_id = #{user2['id']} and team_id = #{@team.id}").entries[0]

    assert_equal 'member', team_user1['as_type']
    assert team_user1['created_at']
    assert team_user1['updated_at']

    assert_equal 'member', team_user2['as_type']
    assert team_user2['created_at']
    assert team_user2['updated_at']
  end

  test 'add users to the everyone team' do
    user1 = @users.select { |u| u['email'] == 'abc@example.com' }[0]
    user2 = @users.select { |u| u['email'] == 'def@example.com' }[0]

    team_user1 = @client.query("select * from teams_users where user_id = #{user1['id']} and team_id = #{@everyone_team.id}").entries[0]
    team_user2 = @client.query("select * from teams_users where user_id = #{user2['id']} and team_id = #{@everyone_team.id}").entries[0]

    assert_equal 'member', team_user1['as_type']
    assert team_user1['created_at']
    assert team_user1['updated_at']

    assert_equal 'member', team_user2['as_type']
    assert team_user2['created_at']
    assert team_user2['updated_at']
  end

  test 'captures the status of the import' do
    assert_not_empty @klass.status[:messages]
    assert_equal 2, @klass.status[:users_imported]
    assert_equal [@channel1.id, @channel2.id].to_set, @klass.status[:channels_added].to_set
    assert_equal [@team.id, @everyone_team.id].to_set, @klass.status[:teams_added].to_set
  end
end

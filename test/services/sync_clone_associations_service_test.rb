require 'test_helper'

class SyncCloneAssociationsServiceTest < ActiveSupport::TestCase
  setup do
    @org1, @org2 = create_list(:organization, 2)
    @user = create(:user, organization: @org1)
    @org1_channel = create(:channel, organization: @org1, shareable: true)
    @org2_channel = create(:channel, organization: @org2, parent_id: @org1_channel.id)
  end

  test "should sync clone cards on updating parent card which has clone" do
    card = create(:card, card_type: 'media', card_subtype: 'text', author_id: @user.id, title: 'This is card', 
      message: 'This is message', organization: @org1, ecl_source_name: 'UGC')
    card.current_user = @user
    card.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
    card.update(title: 'Updated title of card')
    SyncCloneAssociationsJob.perform_now({parent_card_id: card.id, sync_association_type: 'Card'})

    clone_card = Card.find_by(super_card_id: card.id)
    assert_equal card.title, clone_card.title
  end

  test "should sync clone cards on updating parent card resource which has clone" do
    resource = create(:resource)
    card = create(:card, card_type: 'media', resource: resource, author_id: @user.id, title: 'This is card', 
      organization: @org1, ecl_source_name: 'UGC')
    card.current_user = @user
    card.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
    new_resource = create(:resource, title: 'changed title', url: "https://www.edcast.com", description: "changed description")
    card.resource = new_resource
    card.save
    SyncCloneAssociationsJob.perform_now({parent_card_id: card.id, sync_association_type: 'Card'})

    clone_card = Card.find_by(super_card_id: card.id)
    assert_equal card.resource.title, clone_card.resource.title
    assert_equal card.resource.description, clone_card.resource.description
    assert_equal card.resource.url, clone_card.resource.url
  end

  test "should sync clone cards on updating parent card metadata which has clone" do
    card = create(:card, card_type: 'media', card_subtype: 'text', author_id: @user.id, title: 'This is card', 
      message: 'This is message', organization: @org1, ecl_source_name: 'UGC')
    card_metadata = create :card_metadatum, card: card, plan: 'free', level: 'beginner', average_rating: 5
    card.current_user = @user
    card.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
    card_metadata.update_attributes(average_rating: 4, plan: 'paid')
    SyncCloneAssociationsJob.perform_now({parent_card_id: card.id, sync_association_type: 'CardMetadatum'})

    clone_card = Card.find_by(super_card_id: card.id)
    assert_equal card.card_metadatum.average_rating, clone_card.card_metadatum.average_rating
    assert_equal card.card_metadatum.plan, clone_card.card_metadatum.plan
  end

  test "should sync clone cards on updating parent card price which has clone" do
    card = create(:card, card_type: 'media', card_subtype: 'text', author_id: @user.id, title: 'This is card', 
      message: 'This is message', organization: @org1, ecl_source_name: 'UGC')
    price = create :price, card: card, currency: 'USD', amount: 100
    card.current_user = @user
    card.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
    price.update(amount: 1000)
    SyncCloneAssociationsJob.perform_now({parent_card_id: card.id, sync_association_type: 'Price'})

    clone_card_price = Card.find_by(super_card_id: card.id).prices.first
    assert_equal price.amount, clone_card_price.amount
  end

  test "should sync clone cards on adding price to parent card which has clone" do
    card = create(:card, card_type: 'media', card_subtype: 'text', author_id: @user.id, title: 'This is card', 
      message: 'This is message', organization: @org1, ecl_source_name: 'UGC')
    price1 = create :price, card: card, currency: 'USD', amount: 10
    card.current_user = @user
    card.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)

    price2 = create :price, card: card, currency: 'INR', amount: 500
    clone_card = Card.find_by(super_card_id: card.id)
    SyncCloneAssociationsJob.perform_now({parent_card_id: card.id, sync_association_type: 'Price'})

    assert_equal 2, clone_card.prices.count
  end

  test "should sync clone cards on deleting parent card price which has clone" do
    card = create(:card, card_type: 'media', card_subtype: 'text', author_id: @user.id, title: 'This is card', 
      message: 'This is message', organization: @org1, ecl_source_name: 'UGC')
    price1 = create :price, card: card, currency: 'USD', amount: 10
    price2 = create :price, card: card, currency: 'INR', amount: 500
    card.current_user = @user
    card.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)

    clone_card = Card.find_by(super_card_id: card.id)
    assert_equal 2, clone_card.prices.count

    price1.destroy # delete price1 of parent card
    SyncCloneAssociationsJob.perform_now({parent_card_id: card.id, sync_association_type: 'Price'})

    assert_equal 1, clone_card.prices.count
  end

  test "should sync clone cards on deleting parent card poll options which has clone" do
    card = create(:card, card_type: 'poll', author_id: @user.id, organization: @org1, ecl_source_name: 'UGC')
    option1 = create :quiz_question_option, quiz_question: card, label: 'opt1'
    option2 = create :quiz_question_option, quiz_question: card, label: 'opt2'
    option3 = create :quiz_question_option, quiz_question: card, label: 'opt3'
    card.current_user = @user
    card.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
    option3.destroy
    SyncCloneAssociationsJob.perform_now({parent_card_id: card.id, sync_association_type: 'QuizQuestionOption'})

    clone_card = Card.find_by(super_card_id: card.id)
    assert_equal 2, clone_card.quiz_question_options.count
  end

  test "should sync clone cards on adding parent card poll options which has clone" do
    card = create(:card, card_type: 'poll', author_id: @user.id, organization: @org1, ecl_source_name: 'UGC')
    option1 = create :quiz_question_option, quiz_question: card, label: 'opt1'
    option2 = create :quiz_question_option, quiz_question: card, label: 'opt2'
    card.current_user = @user
    card.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
    option3 = create :quiz_question_option, quiz_question: card, label: 'opt3'
    SyncCloneAssociationsJob.perform_now({parent_card_id: card.id, sync_association_type: 'QuizQuestionOption'})

    clone_card = Card.find_by(super_card_id: card.id)
    assert_equal 3, clone_card.quiz_question_options.count
  end

  test "should sync clone cards on adding card to parent pathway which has clone" do
    TestAfterCommit.enabled = true
    cover = create(:card, card_type: 'pack', author_id: @user.id, title: 'pack cover', organization: @org1, ecl_source_name: 'UGC')
    cards = create_list(:card, 2, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end
    cover.current_user = @user
    cover.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
    clone_card = Card.find_by(super_card_id: cover.id)
    assert_equal 3, clone_card.pack_relns.length

    new_card = create(:card, author_id: @user.id, card_type: 'media')
    CardPackRelation.add(cover_id: cover.id, add_id: new_card.id, add_type: 'Card')
    SyncCloneAssociationsJob.perform_now({parent_card_id: cover.id, sync_association_type: 'CardPackRelation'})

    assert_equal 4, clone_card.pack_relns.length
  end

  test "should not sync clone cards on adding private card to parent pathway which has clone" do
    TestAfterCommit.enabled = true
    cover = create(:card, card_type: 'pack', author_id: @user.id, title: 'pack cover', organization: @org1, ecl_source_name: 'UGC')
    cards = create_list(:card, 2, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end
    cover.current_user = @user
    cover.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
    clone_card = Card.find_by(super_card_id: cover.id)
    assert_equal 3, clone_card.pack_relns.length

    new_card = create(:card, author_id: @user.id, card_type: 'media', is_public: false)
    CardPackRelation.add(cover_id: cover.id, add_id: new_card.id, add_type: 'Card')
    SyncCloneAssociationsJob.perform_now({parent_card_id: cover.id, sync_association_type: 'CardPackRelation'})

    assert_equal 4, cover.pack_relns.length
    assert_equal 3, clone_card.pack_relns.length
  end

  test "should sync clone cards on updating card from parent pathway which has clone" do
    TestAfterCommit.enabled = true
    cover = create(:card, card_type: 'pack', author_id: @user.id, title: 'pack cover', organization: @org1, ecl_source_name: 'UGC')
    cards = create_list(:card, 2, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end
    cover.current_user = @user
    cover.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
    clone_card = Card.find_by(super_card_id: cover.id)

    new_title = 'new updated title'
    cards.first.update(title: new_title)
    SyncCloneAssociationsJob.perform_now({parent_card_id: cards.first.id, sync_association_type: 'Card'})

    updated_clone_card_pack = Card.where(title: new_title).where(id: clone_card.pack_relns.map(&:from_id)).first
    assert_equal new_title, updated_clone_card_pack.title
  end

  test "should sync clone cards on deleting card from parent pathway which has clone" do
    TestAfterCommit.enabled = true
    cover = create(:card, card_type: 'pack', author_id: @user.id, title: 'pack cover', organization: @org1, ecl_source_name: 'UGC')
    cards = create_list(:card, 2, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id, add_type: 'Card')
    end
    cover.current_user = @user
    cover.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
    clone_card = Card.find_by(super_card_id: cover.id)
    assert_equal 3, clone_card.pack_relns.length

    cover.remove_card_from_pathway(cards.last.id, 'Card', {delete_dependent: true})
    SyncCloneAssociationsJob.perform_now({parent_card_id: cover.id, sync_association_type: 'CardPackRelation'})

    assert_equal 2, clone_card.pack_relns.length
  end

  test "should sync clone cards on adding pathway to parent journey which has clone" do
    TestAfterCommit.enabled = true
    cover_journey = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: @user, organization: @org, ecl_source_name: 'UGC')
    section = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user, organization: @org)
    cards = create_list(:card, 2, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: section.id, add_id: card.id, add_type: 'Card')
    end
    JourneyPackRelation.add(cover_id: cover_journey.id, add_id: section.id)
    cover_journey.current_user = @user
    cover_journey.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
    clone_card = Card.find_by(super_card_id: cover_journey.id)
    assert_equal 2, clone_card.journey_relns.length

    new_section = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user, organization: @org)
    new_card = create(:card, author_id: @user.id, card_type: 'media')
    CardPackRelation.add(cover_id: new_section.id, add_id: new_card.id, add_type: 'Card')
    JourneyPackRelation.add(cover_id: cover_journey.id, add_id: new_section.id)
    SyncCloneAssociationsJob.perform_now({parent_card_id: cover_journey.id, sync_association_type: 'JourneyPackRelation'})

    assert_equal 3, clone_card.journey_relns.length
  end

  test "should not sync clone cards on adding private pathway to parent journey which has clone" do
    TestAfterCommit.enabled = true
    cover_journey = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: @user, organization: @org, ecl_source_name: 'UGC')
    section = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user, organization: @org)
    cards = create_list(:card, 2, author_id: @user.id, card_type: 'media')
    cards.each do |card|
      CardPackRelation.add(cover_id: section.id, add_id: card.id, add_type: 'Card')
    end
    JourneyPackRelation.add(cover_id: cover_journey.id, add_id: section.id)
    cover_journey.current_user = @user
    cover_journey.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
    clone_card = Card.find_by(super_card_id: cover_journey.id)
    assert_equal 2, clone_card.journey_relns.length

    new_section = create(:card, card_type: 'pack', card_subtype: 'simple', author: @user, organization: @org, is_public: false)
    new_card = create(:card, author_id: @user.id, card_type: 'media')
    CardPackRelation.add(cover_id: new_section.id, add_id: new_card.id, add_type: 'Card')
    JourneyPackRelation.add(cover_id: cover_journey.id, add_id: new_section.id)
    SyncCloneAssociationsJob.perform_now({parent_card_id: cover_journey.id, sync_association_type: 'JourneyPackRelation'})

    assert_equal 3, cover_journey.journey_relns.length
    assert_equal 2, clone_card.journey_relns.length
  end

  test "should sync clone cards on deleting pathway from parent journey which has clone" do
    TestAfterCommit.enabled = true
    cover_journey = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: @user, organization: @org, ecl_source_name: 'UGC')
    sections = create_list(:card, 2, card_type: 'pack', card_subtype: 'simple', author: @user, organization: @org)
    cards = create_list(:card, 2, author_id: @user.id, card_type: 'media')
    CardPackRelation.add(cover_id: sections.first.id, add_id: cards.first.id, add_type: 'Card')
    CardPackRelation.add(cover_id: sections.last.id, add_id: cards.last.id, add_type: 'Card')
    sections.each do |section|
      JourneyPackRelation.add(cover_id: cover_journey.id, add_id: section.id)
    end
    cover_journey.current_user = @user
    cover_journey.channels << @org1_channel
    CloneChannelCardsJob.perform_now(@org1_channel.id, @org2_channel.id)
    clone_card = Card.find_by(super_card_id: cover_journey.id)
    assert_equal 3, clone_card.journey_relns.length

    cover_journey.remove_card_from_journey(sections.last.id, true)
    SyncCloneAssociationsJob.perform_now({parent_card_id: cover_journey.id, sync_association_type: 'JourneyPackRelation'})

    assert_equal 2, clone_card.journey_relns.length
  end
end
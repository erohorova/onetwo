require 'test_helper'

class MandrillMessageGenerationServiceTest < ActiveSupport::TestCase
  test '#initialize' do
    service_object = MandrillMessageGenerationService.new
    expected = {
      "inline_css" => true,
      "merge_language" => "handlebars",
      "preserve_recipients" => false,
      "important" => false,
      "url_strip_qs" => false,
      "track_clicks" => true,
      "track_opens" => true 
    }

    assert_equal expected, service_object.instance_variable_get(:@message)
  end


  test 'MandrillMessageGenerationService should not convert slim to html if html file is passed' do
    template = 'header' + '.html'
    subject = "New Forum Announcement"
    Slim::Template.any_instance.expects(:render).never

    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: template,
                                    subject: subject,
                                    from_email: "admin@edcast.com",
                                    from_name: "EdCast Announcement",
                                    to: ["abc@edcast.com"],
                                    merge_vars: nil,
                                    attachments: nil,
                                    mailer: nil,
                                    global_merge_vars: {})
  end

  test 'verify message created by merge_custom_attrs' do
    template = 'header' + '.html.slim'
    subject = "New Forum Announcement"

    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: template,
                                    subject: subject,
                                    from_email: "admin@edcast.com",
                                    from_name: "EdCast Admin",
                                    to: ["abc@edcast.com"],
                                    merge_vars: nil,
                                    attachments: nil,
                                    mailer: nil,
                                    global_merge_vars: {})
    
    assert_equal 'handlebars', message['merge_language']
    assert_equal 'New Forum Announcement', message[:subject]
    assert message[:html].present?
    assert_equal "admin@edcast.com", message[:from_email]
    assert_equal "EdCast Admin", message[:from_name]
    assert_equal "EdCast Admin", message[:from_name]
    assert_equal ["abc@edcast.com"], message[:to]
  end

  test 'get expected custom template for mailer config' do
    mailer = create :mailer_config
    template = 'daily-digest.html'
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: template,
                                    subject: nil,
                                    to: ["abc@edcast.com"],
                                    merge_vars: nil,
                                    attachments: nil,
                                    mailer: mailer,
                                    global_merge_vars: {})
    assert_not_nil message[:html]
    assert_equal mailer.from_address, message[:from_email]
  end

  test 'fallback to default directory if org specific template not found' do
    mailer = create :mailer_config
    template = 'courses-report.html'
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: template,
                                    subject: nil,
                                    to: ["abc@edcast.com"],
                                    merge_vars: nil,
                                    attachments: nil,
                                    mailer: mailer,
                                    global_merge_vars: {})
    assert_not_nil message[:html]
    assert_equal mailer.from_address, message[:from_email]
  end

  test 'check if mail template not found' do
    mailer = create :mailer_config
    template = 'invalid-mail-template.html'
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: template,
                                    subject: nil,
                                    to: ["abc@edcast.com"],
                                    merge_vars: nil,
                                    attachments: nil,
                                    mailer: mailer,
                                    global_merge_vars: {})
    assert_nil message[:html]
    assert_equal mailer.from_address, message[:from_email]
  end

  class CustomContentTest < ActiveSupport::TestCase

    setup do
      @org = create(:organization)
      @user = create(:user, organization: @org)
      default_template = EmailTemplate.create(
        is_active: true, title: 'test_mailer', content: 'content',
        organization: @org, design: 'design', language: 'en', state: 'published'
      )
      content_en = <<-TXT.strip_heredoc
        <!DOCTYPE html>
        <html>
          <body>
            <p>Text in english</p>
            <p>User first name: {{user.first_name}}</p>
          </body>
        </html>
      TXT

      content_fr = <<-TXT.strip_heredoc
        <!DOCTYPE html>
        <html>
          <body>
            <p>texte en français</p>
            <p>Dans le premier utilisateur nom: {{user.first_name}}</p>
          </body>
        </html>
      TXT

      content_de = <<-TXT.strip_heredoc
        <!DOCTYPE html>
        <html>
          <body>
            <p>Text in Deutsch</p>
            <p>Vorname des Benutzers: {{user.first_name}}</p>
          </body>
        </html>
      TXT
      @custom_template_en = create(:email_template,
        is_active: true, title: 'test_mailer_en', content: content_en,
        organization: @org, design: 'design', language: 'en',
        default_id: default_template.id, creator_id: @user.id,
        state: 'published'
      )

      @custom_template_fr = create(:email_template,
        is_active: true, title: 'test_mailer_fr', content: content_fr,
        organization: @org, design: 'design', language: 'fr',
        default_id: default_template.id, creator_id: @user.id,
        state: 'published'
      )

      @custom_template_de = create(:email_template,
        is_active: true, title: 'test_mailer_ge', content: content_de,
        organization: @org, design: 'design', language: 'de',
        default_id: default_template.id, creator_id: @user.id,
        state: 'published'
      )
    end


    test 'should properly extract custom template based on language settings' do
      create(:config, configable: @org, name: 'emailTemplatesEnabled', value: 'true')
      file_name = 'test_mailer.html.slim'

      args = {user: @user, file_name: file_name, mailer: nil, organization: @org}
      content = MandrillMessageGenerationService.new.send(:get_template, args)
      # base language 'en'
      #   lowest priority
      assert_equal @custom_template_en.content, content

      create(:config, configable: @org, name: 'DefaultOrgLanguage', value: 'fr')
      content = MandrillMessageGenerationService.new.send(:get_template, args)
      # default organization language if it present
      assert_equal @custom_template_fr.content, content

      # user preferred language if it present
      #   highest priority
      create(:user_profile, user: @user, language: 'de')
      content = MandrillMessageGenerationService.new.send(:get_template, args)

      assert_equal @custom_template_de.content, content
    end
  end

  class CustomTemplateTest < ActiveSupport::TestCase
    setup do
      @org = create(:organization)
      @admin = create(:user, organization: @org, organization_role: 'admin')
      create(:config, name: 'emailTemplatesEnabled', value: true, configable: @org)
      @user = create(:user, organization: @org)

      @default_template = create(
        :email_template, organization: @org,
        title: 'admin-bulk-import-welcome'
      )
    end
    
    test 'only published custom_template should be fetched' do
      custom_template = create(
        :email_template, default_id: @default_template.id, language: 'en',
        content: 'simple_content', design: 'simple_design', creator_id: @admin.id,
        is_active: true, state: 'published', title: 'admin-bulk-import-welcome-custom',
        organization: @org
      )
      message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'admin-bulk-import-welcome.html.slim',
                  current_user: @user,
                  subject: "Welcome to #{@org.name} on EdCast!",
                  to: ["abc@edcast.com"],
                  merge_vars: nil,
                  attachments: nil,
                  global_merge_vars: {})

      assert_equal custom_template.content, message[:html]
      assert_equal "Welcome to #{@org.name} on EdCast!", message[:subject]
    end

    test 'draft custom_template should not be fetched' do
      custom_template = create(
        :email_template, default_id: @default_template.id, language: 'en',
        content: 'simple_content', design: 'simple_design', creator_id: @admin.id,
        is_active: true, state: 'draft', title: 'admin-bulk-import-welcome-custom',
        organization: @org
      )

      message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'admin-bulk-import-welcome.html.slim',
                  current_user: @user,
                  subject: "Welcome to #{@org.name} on EdCast!",
                  to: ["abc@edcast.com"],
                  merge_vars: nil,
                  attachments: nil,
                  global_merge_vars: {})
      assert_not_equal custom_template.content, message[:html]
    end

    test 'custom subject of the draft custom_template should not be fetched' do
      custom_template = create(
        :email_template, default_id: @default_template.id, language: 'en',
        content: 'simple_content', design: 'simple_design', creator_id: @admin.id,
        is_active: true, state: 'draft', title: 'admin-bulk-import-welcome-custom',
        subject: '{{first_name}}, welcome to {{team_name}}',
        organization: @org
      )
      message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'admin-bulk-import-welcome.html.slim',
                  current_user: @user,
                  subject: "Welcome to #{@org.name} on EdCast!",
                  to: ["abc@edcast.com"],
                  merge_vars: nil,
                  attachments: nil,
                  global_merge_vars: {})
      assert_not_equal custom_template.subject, message[:subject]
      assert_equal "Welcome to #{@org.name} on EdCast!", message[:subject]
    end

    test 'custom subject of the published custom_template should be fetched' do
      custom_template = create(
        :email_template, default_id: @default_template.id, language: 'en',
        content: 'simple_content', design: 'simple_design', creator_id: @admin.id,
        is_active: true, state: 'published', title: 'admin-bulk-import-welcome-custom',
        subject: '{{first_name}}, welcome to {{team_name}}',
        organization: @org
      ) 

      message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'admin-bulk-import-welcome.html.slim',
                  current_user: @user,
                  subject: "Welcome to #{@org.name} on EdCast!",
                  to: ["abc@edcast.com"],
                  merge_vars: nil,
                  attachments: nil,
                  global_merge_vars: {})
      assert_equal custom_template.subject, message[:subject]
      assert_not_equal "Welcome to #{@org.name} on EdCast!", message[:subject]
    end
  end

end

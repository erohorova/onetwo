require 'test_helper'

class BulkImportUsersV2::IntegrationTest < ActiveSupport::TestCase
  def setup_csv_for_invitation(send_email: false)
    stub_request(:post, 'https://api.branch.io/v1/url').to_return(status: 200, body: '', headers: {})
    stub_request(:post, 'http://localhost:9200/_bulk')

    options = {
      channel_ids: [@channel_1.id],
      invitable:   true,
      send_welcome_email: send_email,
      team_ids:    [@tech_team.id, @engg_team.id]
    }

    @klass = BulkImportUsersV2.new(
      admin_id: @admin_user.id,
      file_id:  @invitation_file.id,
      options:  options
    )
    @klass.import
  end

  def setup_csv_for_creation(send_email: false)
    options = {
      channel_ids:        [@channel_1.id],
      send_welcome_email: send_email,
      team_ids:           [@tech_team.id, @engg_team.id]
    }

    @klass = BulkImportUsersV2.new(
      file_id:  @invitation_file.id,
      options:  options,
      admin_id: @admin_user.id
    )
    @klass.import
  end

  setup do
    Config.any_instance.stubs :create_okta_group_config

    User.any_instance.stubs(:update_details_to_okta).returns true
    stub_request(:get, 'http://breelio.com/wp-content/uploads/2014/06/sample1.jpg').
      with(headers: { 'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Ruby' }).
      to_return(status: 200, body: '', headers: {})

    @organization  = create :organization
    @admin_user    = create :user, organization: @organization, organization_role: 'admin'

    # create couple of channels
    @channel_1     = create :channel, organization: @organization, user: @admin_user
    @channel_2     = create :channel, organization: @organization, user: @admin_user

    # create couple of teams/groups
    @tech_team     = create :team, organization: @organization, name: 'Technology'
    @engg_team     = create :team, organization: @organization, name: 'Engg'

    # create an end-user
    @existing_user = create :user, organization: @organization, is_complete: false, email: 'userexists@edcast.com'

    # define few custom fields in an organization
    @cf_1 = create :custom_field, organization: @organization, display_name: 'Burrito Wrap', abbreviation: nil
    @cf_2 = create :custom_field, organization: @organization, display_name: 'Cheese', abbreviation: nil
    @cf_3 = create :custom_field, organization: @organization, display_name: 'Tea', abbreviation: nil

    # adds custom fields to existing user
    create :user_custom_field, custom_field: @cf_1, user: @existing_user, value: 'juarez'
    create :user_custom_field, custom_field: @cf_2, user: @existing_user, value: 'pocho'

    csv_rows = File.open("#{Rails.root}/test/fixtures/files/users_with_custom_fields.csv").read

    @invitation_file = create(:invitation_file,
      sender_id: @admin_user.id,
      invitable: @organization,
      data: csv_rows,
      roles: 'member')

    stub_request(:post, "https://api.branch.io/v1/url").to_return(status: 200, body: "{\"url\":\"http://bitly.co}", headers: {})
  end

  test 'rejects invalid emails' do
    setup_csv_for_creation

    assert_nil @organization.users.find_by(email: 'námè+garçon@example.com')
    assert_nil @organization.users.find_by(email: 'námè+sömê@example.com')
    assert_nil @organization.users.find_by(email: 'invalid_email')
  end

  test "should not email admin channel added followers notification if users are not valid" do
    csv_rows = "First Name,Last Name,Email,Groups,\"Picture Url\",Password\nFirstName1,LastName1,invalidemail,,,Password@123"

    invitation_file = create(:invitation_file,
      sender_id: @admin_user.id,
      invitable: @organization,
      data: csv_rows,
      roles: 'member')

    UserMailer.expects(:channel_follows_notification_to_admin).never
    EDCAST_NOTIFY.expects(:trigger_event).never

    BulkImportUsersV2.new(file_id: invitation_file.id).import
  end

  test 'creates teams if they do not exist' do
    skip # intermittent failure
    setup_csv_for_creation

    # [Programming, Art] are the teams that didn't exist before import
    new_teams = @organization.reload.teams.where(name: ['Art', 'Programming'])
    assert_equal  2, new_teams.count
  end

  test 'get count of existing_teams and new_teams' do
    setup_csv_for_creation
    invitation_file = @invitation_file.reload

    # Gives count of newly created teams
    assert_equal 2, invitation_file.parameter['new_groups_count']

    # Gives count of old teams
    assert_equal 1, invitation_file.parameter['old_groups_count']
  end

  test 'creates teams even if they exists in other org' do
    skip # intermittent failure
    org2 = create(:organization)
    existing_team1 = create(:team, organization: org2, name: 'Programming')
    existing_team2 = create(:team, organization: org2, name: 'Art')

    setup_csv_for_creation
    new_teams = @organization.reload.teams.where(name: ['Art', 'Programming'])

    assert_equal  2, new_teams.count
  end
end

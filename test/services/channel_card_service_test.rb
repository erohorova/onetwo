require 'test_helper'
class ChannelCardServiceTest < ActiveSupport::TestCase
  # Note: Please maintain the segregation of test cases into two sections viz.
  # filters and sorting options
  setup do
    @organization = create(:organization)
    @channel = create(:channel, label: 'test channel', organization: @organization)

    # Create author for cards to be added to channel
    @author = create(:user, organization: @organization)
    @card_1 = create(:card, organization: @organization, author: @author)
    @card_2 = create(:card, organization: @organization, author: @author)
    @channel.cards << [@card_1, @card_2]

    @channel_user = create(:user, organization: @organization)
  end

  def create_channel_cards_for_entitlements
    @public_card_1 = create(:card, organization: @organization, author: @author, title: 'Public card 1')
    @public_card_2 = create(:card, organization: @organization, author: @author, title: 'Public card 2')
    @private_card_1 = create(:card, organization: @organization, author: @author, title: 'Private card 1', is_public: false)
    @private_card_2 = create(:card, organization: @organization, author: @author, title: 'Private card 2', is_public: false)
    @channel.cards << [@public_card_1, @public_card_2, @private_card_1, @private_card_2]
  end

  # ********************** Test Cases for Filters ******************************

  test 'should return cards of channel with curation state' do
    curatable_channel = create(:channel, organization: @organization, curate_only: true)
    curatable_channel.cards << [@card_1, @card_2]

    # Curate card_1
    ChannelsCard.where(card_id: @card_1.id, channel_id: curatable_channel.id).first.curate!

    # Get non curated card
    service_resp = ChannelCardService.new(
                    channel: curatable_channel,
                    params: { channel_card_state: 'new' },
                    viewer: @channel_user,
                    limit: 10,
                    offset: 0).call
    assert_equal 1, service_resp[:total]
    assert_same_elements [@card_2.id], service_resp[:cards].map(&:id)

    # Get curated card
    service_resp = ChannelCardService.new(
                    channel: curatable_channel,
                    params: { channel_card_state: 'curated' },
                    viewer: @channel_user,
                    limit: 10,
                    offset: 0).call
    assert_equal 1, service_resp[:total]
    assert_same_elements [@card_1.id], service_resp[:cards].map(&:id)
  end

  test 'should only return published cards' do
    # Create a draft pathway
    pathway = create(:card, organization: @organization, author: @author, state: Card::STATE_DRAFT)
    pathway.add_card_to_pathway(@card_1.id, 'Card')
    pathway.add_card_to_pathway(@card_2.id, 'Card')

    @channel.cards << [pathway]

    service_resp = ChannelCardService.new(
                    channel: @channel,
                    params: { card_type: ['pack'] },
                    viewer: @channel_user,
                    limit: 10,
                    offset: 0).call
    assert_equal 0, service_resp[:total]
  end

  test 'should not return dismissed cards for non admin user' do
    # Dismiss card for channel user
    create(:dismissed_content, content_id: @card_1.id, content_type: 'Card', user_id: @channel_user.id)

    service_resp = ChannelCardService.new(
                    channel: @channel,
                    params: { },
                    viewer: @channel_user,
                    limit: 10,
                    offset: 0).call
    assert_equal 1, service_resp[:total]
    assert_same_elements [@card_2.id], service_resp[:cards].map(&:id)
  end

  test 'should not return private cards for a user not following the channel' do
    create_channel_cards_for_entitlements

    service_resp = ChannelCardService.new(
                    channel: @channel,
                    params: { },
                    viewer: @channel_user,
                    limit: 10,
                    offset: 0).call
    assert_equal 4, service_resp[:total]
    assert_same_elements [@card_1.id, @card_2.id, @public_card_1.id, @public_card_2.id], service_resp[:cards].map(&:id)
  end

  test 'private card restricted to a user should not be visible to other channel followers' do
    create_channel_cards_for_entitlements
    # Restrict a private card to logged-in user
    create(:card_user_permission, card_id: @private_card_1.id, user_id: @channel_user.id)

    # Create another user as follower to channel
    user = create(:user, organization: @organization)
    @channel.add_followers([user])

    service_resp = ChannelCardService.new(
                    channel: @channel,
                    params: { },
                    viewer: user,
                    limit: 10,
                    offset: 0).call
    assert_equal 5, service_resp[:total]
    assert_same_elements [@card_1.id, @card_2.id, @public_card_1.id, @public_card_2.id, @private_card_2.id], service_resp[:cards].map(&:id)
  end

  test 'private card restricted to a team should not be visible to other channel followers' do
    create_channel_cards_for_entitlements
    # Create a team and add logged-in user to the team
    team = create(:team, organization: @organization)
    create(:teams_user, team_id: team.id, user_id: @channel_user.id)

    # Restrict a private card to team
    create(:card_team_permission, card_id: @private_card_1.id, team_id: team.id)

    # Create another user as follower to channel
    user = create(:user, organization: @organization)
    @channel.add_followers([user])

    service_resp = ChannelCardService.new(
                    channel: @channel,
                    params: { },
                    viewer: user,
                    limit: 10,
                    offset: 0).call
    assert_equal 5, service_resp[:total]
    assert_same_elements [@card_1.id, @card_2.id, @public_card_1.id, @public_card_2.id, @private_card_2.id], service_resp[:cards].map(&:id)
  end

  test 'should return cards of channel by card type and card subtype' do
    # @card_1 & @card_2 have card_type as media
    card_3 = create(:card, organization: @organization, author: @author, card_type: 'poll')
    card_4 = create(:card, organization: @organization, author: @author, card_type: 'course')
    card_5 = create(:card, organization: @organization, author: @author, card_type: 'pack')
    @channel.cards << [card_3, card_4, card_5]

    service_resp = ChannelCardService.new(
                    channel: @channel,
                    params: { card_type: ['media', 'poll'] },
                    viewer: @channel_user,
                    limit: 10,
                    offset: 0).call
    assert_equal 3, service_resp[:total]
    assert_same_elements [@card_1.id, @card_2.id, card_3.id], service_resp[:cards].map(&:id)

    service_resp = ChannelCardService.new(
                    channel: @channel,
                    params: { card_type: ['course'] },
                    viewer: @channel_user,
                    limit: 10,
                    offset: 0).call
    assert_equal 1, service_resp[:total]
    assert_same_elements [card_4.id], service_resp[:cards].map(&:id)

    service_resp = ChannelCardService.new(
                    channel: @channel,
                    params: { card_type: ['media'], card_subtype: ['text'] },
                    viewer: @channel_user,
                    limit: 10,
                    offset: 0).call
    assert_equal 2, service_resp[:total]
    assert_same_elements [@card_1.id, @card_2.id], service_resp[:cards].map(&:id)
  end

  test 'should return only UGC cards of channel' do
    card_3 = create(:card, organization: @organization, author: nil)
    @channel.cards << [card_3]

    service_resp = ChannelCardService.new(
                    channel: @channel,
                    params: { ugc: true },
                    viewer: @channel_user,
                    limit: 10,
                    offset: 0).call
    assert_equal 2, service_resp[:total]
    assert_same_elements [@card_1.id, @card_2.id], service_resp[:cards].map(&:id)
    refute_includes service_resp[:cards].map(&:id), card_3.id
  end

  test 'should return cards filtered by language' do
    @channel_user.create_profile(language: 'ru')

    card_3 = create(:card, organization: @organization, author: @author, language: 'ru')
    card_4 = create(:card, organization: @organization, author: @author, language: 'en')
    @channel.cards << [card_3, card_4]

    service_resp = ChannelCardService.new(
                    channel: @channel,
                    params: { language_filter: true },
                    viewer: @channel_user,
                    limit: 10,
                    offset: 0).call
    assert_equal 3, service_resp[:total]
    assert_same_elements [@card_1.id, @card_2.id, card_3.id], service_resp[:cards].map(&:id)
    refute_includes service_resp[:cards].map(&:id), card_4.id
  end

  test 'should return cards created in a particular date range' do
    card_3 = create(:card, organization: @organization, author: @author, created_at: Date.today.beginning_of_year)
    @channel.cards << [card_3]

    service_resp = ChannelCardService.new(
                    channel: @channel,
                    params: { from_date: "#{Date.today.beginning_of_year - 1.month}", to_date: "#{Date.today}" },
                    viewer: @channel_user,
                    limit: 10,
                    offset: 0).call
    assert_equal 3, service_resp[:total]
    assert_same_elements [@card_1.id, @card_2.id, card_3.id], service_resp[:cards].map(&:id)
  end

  test 'should return cards added to channel after specific date' do
    card_3 = create(:card, organization: @organization, author: @author)
    create(:channels_card, channel_id: @channel.id, card_id: card_3.id, created_at: Date.today - 5)

    card_4 = create(:card, organization: @organization, author: @author)
    create(:channels_card, channel_id: @channel.id, card_id: card_4.id, created_at: Date.today - 3)

    service_resp = ChannelCardService.new(
                    channel: @channel,
                    params: { last_access_at: (Date.today - 4).to_time.to_i },
                    viewer: @channel_user,
                    limit: 10,
                    offset: 0).call

    assert_equal 4, service_resp[:total]
    assert_equal 3, service_resp[:new_cards_total]
  end

  # ********************** Test Cases for Sorting ******************************

  test 'should return cards sorted by default order if sort params are not passed' do
    # Default : rank asc of channel_card
    card_3 = create(:card, organization: @organization, author: @author)
    card_4 = create(:card, organization: @organization, author: @author)
    @channel.cards << [card_3, card_4]

    # Set rank for card_3 and card_4
    channel_card_3 = ChannelsCard.find_by(channel: @channel, card: card_3)
    channel_card_3.update(rank: 10, created_at: Date.today - 6)
    channel_card_4 = ChannelsCard.find_by(channel: @channel, card: card_4)
    channel_card_4.update(rank: 5, created_at: Date.today - 7)

    # Update channel_card created_at of card_1 and card_2
    channel_card_1 = ChannelsCard.find_by(channel: @channel, card: @card_1)
    channel_card_1.update(created_at: Date.today - 5)
    channel_card_2 = ChannelsCard.find_by(channel: @channel, card: @card_2)
    channel_card_2.update(created_at: Date.today - 4)

    service_resp = ChannelCardService.new(
                    channel: @channel,
                    params: {},
                    viewer: @channel_user,
                    limit: 10,
                    offset: 0).call
    assert_equal 4, service_resp[:total]
    assert_equal [card_4.id, card_3.id, @card_2.id, @card_1.id], service_resp[:cards].map(&:id)
  end

  test 'should return cards sorted by created at' do
    card_3 = create(:card, organization: @organization, author: @author, created_at: Date.today - 5)
    card_4 = create(:card, organization: @organization, author: @author, created_at: Date.today - 3)
    @channel.cards << [card_3, card_4]

    # Default sort order : DESC
    service_resp = ChannelCardService.new(
                    channel: @channel,
                    params: { sort: 'created_at' },
                    viewer: @channel_user,
                    limit: 10,
                    offset: 0).call
    assert_equal 4, service_resp[:total]
    assert_equal [@card_1.id, @card_2.id, card_4.id, card_3.id], service_resp[:cards].map(&:id)

    # sort order : ASC
    service_resp = ChannelCardService.new(
                    channel: @channel,
                    params: { sort: 'created_at', order: 'asc' },
                    viewer: @channel_user,
                    limit: 10,
                    offset: 0).call
    assert_equal 4, service_resp[:total]
    assert_equal [card_3.id, card_4.id, @card_1.id, @card_2.id], service_resp[:cards].map(&:id)
  end

  test 'should return cards sorted by likes count/ views count' do
    card_3 = create(:card, organization: @organization, author: @author)
    card_4 = create(:card, organization: @organization, author: @author)
    @channel.cards << [card_3, card_4]

    create(:content_level_metric, content_type: 'card', content_id: @card_1.id, period: 'all_time', offset: 0, likes_count: 10, views_count: 40)
    create(:content_level_metric, content_type: 'card', content_id: @card_2.id, period: 'all_time', offset: 0, likes_count: 20, views_count: 30)
    create(:content_level_metric, content_type: 'card', content_id: card_3.id, period: 'all_time', offset: 0, likes_count: 30, views_count: 20)
    create(:content_level_metric, content_type: 'card', content_id: card_4.id, period: 'all_time', offset: 0, likes_count: 40, views_count: 10)

    # Likes desc
    service_resp = ChannelCardService.new(
                    channel: @channel,
                    params: { sort: 'likes_count' },
                    viewer: @channel_user,
                    limit: 10,
                    offset: 0).call
    assert_equal 4, service_resp[:total]
    assert_equal [card_4.id, card_3.id, @card_2.id, @card_1.id], service_resp[:cards].map(&:id)

    # Likes asc
    service_resp = ChannelCardService.new(
                    channel: @channel,
                    params: { sort: 'likes_count', order: 'asc' },
                    viewer: @channel_user,
                    limit: 10,
                    offset: 0).call
    assert_equal 4, service_resp[:total]
    assert_equal [@card_1.id, @card_2.id, card_3.id, card_4.id], service_resp[:cards].map(&:id)

    # Views desc
    service_resp = ChannelCardService.new(
                    channel: @channel,
                    params: { sort: 'views_count' },
                    viewer: @channel_user,
                    limit: 10,
                    offset: 0).call
    assert_equal 4, service_resp[:total]
    assert_equal [@card_1.id, @card_2.id, card_3.id, card_4.id], service_resp[:cards].map(&:id)

    # Views asc
    service_resp = ChannelCardService.new(
                    channel: @channel,
                    params: { sort: 'views_count', order: 'asc' },
                    viewer: @channel_user,
                    limit: 10,
                    offset: 0).call
    assert_equal 4, service_resp[:total]
    assert_equal [card_4.id, card_3.id, @card_2.id, @card_1.id], service_resp[:cards].map(&:id)
  end
end

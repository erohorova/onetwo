require 'test_helper'

class ContentProvider::AuthorizeCourseTest < ActiveSupport::TestCase
  class NonAuthTest < ActionController::TestCase
    test "should allow to access if auth_required false" do
      source_cred1 = create :sources_credential, auth_api_info: {auth_required: false}
      service = ContentProvider::AuthorizeCourse.new({source_id: source_cred1.source_id})
      assert service.authorize
    end
  end

  class AuthAPITest < ActionController::TestCase
    setup do
      @org = create :organization
      @user = create :user, organization_id: @org.id
      @author = create :user, organization_id: @org.id
      @card = create :card, organization_id: @org.id, author: @author
    end

    test "should hit given AUTH API to authorize course" do
      ContentProvider::AuthorizeCourse.any_instance.expects(:process).returns(:true).once

      source_cred = create :sources_credential, auth_api_info: {path: "/api/v2/auth",method: "post",host: "www.edcast.com"}
      service = ContentProvider::AuthorizeCourse.new({
        source_id: source_cred.source_id, user: @user.attributes, card: @card.attributes, payment: {amount: 15}
      })
      assert service.authorize
    end

    test "should return false if auth_api_info is not present" do
      source_cred = create :sources_credential
      service = ContentProvider::AuthorizeCourse.new({
        source_id: source_cred.source_id, user: @user.attributes, card: @card.attributes, payment: {amount: 15}
      })
      refute service.authorize
    end

    test "authorize course should contains auth_api_info in headers" do
      auth_info =  {"url":"courses/tenants/EdcastNasscom/enroll","method":"post","host":"http://localhost.com",headers: {token: "zsxdcerrdummytoken"}}
      source_cred = create :sources_credential, auth_api_info: auth_info
      service = ContentProvider::AuthorizeCourse.new({
        source_id: source_cred.source_id, user: @user.attributes, card: @card.attributes, payment: {amount: 15}
      })

      params = {data: service.payload, auth_token: service.auth_token}
      stub_request(:post, "http://localhost.com/courses/tenants/EdcastNasscom/enroll")
      .with(:body => params.to_json,
       :headers => {'Accept'=>'application/json', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Content-Type'=>'application/json', 'token'=>'zsxdcerrdummytoken', 'User-Agent'=>'Ruby'})
      .to_return(:status => 200, :body => "", :headers => {})

      service.authorize
      assert_response :success
    end
  end
end
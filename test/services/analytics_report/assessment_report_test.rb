require 'test_helper'

class AssessmentReportTest < ActiveSupport::TestCase
  setup do
    @org = create(:organization)
    @user = create(:user, organization_id: @org.id)

    @user1 = create(:user, organization_id: @org.id)
    @user2 = create(:user, organization_id: @org.id)

    @cover = create(:card, author: @user, organization_id: @org.id, card_type: 'pack')

    @card = create(:card, card_type: 'poll', author: @user, organization_id: @org.id)
    option1 = create(:quiz_question_option, label: 'a', quiz_question: @card, is_correct: true)
    option2 = create(:quiz_question_option, label: 'b', quiz_question: @card)

    @card1 = create(:card, card_type: 'poll', author: @user, organization_id: @org.id)
    option3 = create(:quiz_question_option, label: 'c', quiz_question: @card1, is_correct: true)
    option4 = create(:quiz_question_option, label: 'd', quiz_question: @card1)
    option5 = create(:quiz_question_option, label: 'e', quiz_question: @card1)

    CardPackRelation.add(cover_id: @cover.id, add_id: @card.id, add_type: 'Card')
    CardPackRelation.add(cover_id: @cover.id, add_id: @card1.id, add_type: 'Card')

    @attempt = create(:quiz_question_attempt, user: @user1, quiz_question: @card, selections: [option1.id])
    @attempt2 = create(:quiz_question_attempt, user: @user1, quiz_question: @card1, selections: [option3.id])
    @attempt1 = create(:quiz_question_attempt, user: @user2, quiz_question: @card, selections: [option1.id])

    # @attempt.run_callbacks(:commit)
    # @attempt1.run_callbacks(:commit)
    # @attempt2.run_callbacks(:commit)

    @obj = AnalyticsReport::AssessmentReport.new(card_id: @cover.id, user_id: @user.id)
  end

  test 'checking the data for assessment detail' do
    assessment_detail = @obj.build_assessment_detail
    assert 3, assessment_detail.count
    assert_includes assessment_detail, [@cover.snippet]
    assert_includes assessment_detail, [@card.snippet, @card.associated_correct_option]
    assert_includes assessment_detail, [@card1.snippet, @card1.associated_correct_option]
  end

  test 'checking the data for assessment report headers' do
    question_cards_count = @cover.associated_poll_cards.count
    headers = @obj.build_headers.first
    assert_includes headers, "email"
    assert_includes headers, "score(%)"
    assert_includes headers, "question #{question_cards_count}"
    assert_includes headers, "question #{question_cards_count - 1}"
  end

  test 'checking the data for users assessment performance' do
    data = @obj.build_data
    assert_includes data[0], @user1.percentage_correct(@cover.id)
    assert_includes data[0], @attempt.selected_option.label
    assert_includes data[0], @attempt2.selected_option.label
    assert_includes data[0], @user1.email

    assert_includes data[1], @user2.percentage_correct(@cover.id)
    assert_includes data[1], @attempt1.selected_option.label
    assert_includes data[1], "-"
    assert_includes data[1], @user2.email
  end

  test 'to check the generate report method' do
    ReportService.any_instance.expects(:generate_csv).with(@obj.build_assessment_detail + @obj.build_headers + @obj.build_data).once.returns(Tempfile.new(['', '.csv']))
    @obj.generate_report
  end
end

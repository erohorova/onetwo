require 'test_helper'

class ChannelPerformanceTest < ActiveSupport::TestCase
  setup do
    @org = create(:organization)
    user = create(:user, organization: @org)

    @public_channel = create(:channel, organization: @org)
    @private_channel = create(:channel, organization: @org, is_private: true)

    @cards = create_list(:card, 2, organization: @org, author: user)
    @public_channel.cards  <<  @cards.first
    @private_channel.cards << @cards.last

    create(:follow, followable: @public_channel, user: user)

    WebMock.disable!
    MetricRecord.create_index! force: true

    Timecop.freeze(2.hours.ago) do
      @cards.each do |card|
        MetricRecord.new(actor_id: 1, actor_type: 'user', object_id: card.id, object_type: 'card', action: 'impression', organization_id: @org.id,  properties: {view: 'full'}).save
      end
      MetricRecord.gateway.refresh_index!
    end

    %w(like comment bookmark).each do |action|
      MetricRecord.new(actor_id: 1, actor_type: 'user', object_id: @cards.first.id, object_type: 'card', action: action, organization_id: @org.id).save
    end

    %w(mark_completed_assigned mark_completed_self).each do |action|
      MetricRecord.new(actor_id: 1, actor_type: 'user', object_id: @cards.last.id, object_type: 'card', action: action, organization_id: @org.id).save
    end

    MetricRecord.gateway.refresh_index!
  end

  teardown do
    WebMock.enable!
  end

  test 'check elasticsearch query result' do
    service = AnalyticsReport::ChannelPerformance.new(
      organization_id: @org.id, from_date: 1.day.ago, to_date: Time.now.utc)

    # Example format for aggregation_query:
    # {
    #   "took"=>2,
    #   "timed_out"=>false,
    #   "_shards"=>{"total"=>5, "successful"=>5, "failed"=>0},
    #   "hits"=>{"total"=>11, "max_score"=>0.0, "hits"=>[]},
    #   "aggregations"=>
    #    { "actions"=>
    #       { "doc_count_error_upper_bound"=>0,
    #         "sum_other_doc_count"=>0,
    #         "buckets"=>
    #         [{ "key" => "impression",
    #             "doc_count"=>6,
    #             "contents"=>
    #               { "doc_count_error_upper_bound"=>0,
    #                 "sum_other_doc_count"=>0,
    #                 "buckets"=>
    #                   [{"key"=>6891, "doc_count"=>4},
    #                    {"key"=>6892, "doc_count"=>2} ]
    #               }
    #          }, ....]
    #       }
    #    }
    # }
    query_result = service.aggregation_query
    assert_same_elements %W(like comment bookmark impression mark_completed_assigned mark_completed_self), query_result["aggregations"]["actions"]["buckets"].map{|c| c['key']}
  end

  test 'collect consumed cards date from elasticsearch query result' do
    service = AnalyticsReport::ChannelPerformance.new(
      organization_id: @org.id, from_date: 2.days.ago, to_date: Time.now)
    query_result = service.aggregation_query

    # Example format for get_consumed_cards:
    # {
    #   "view"=>{6899=>4, 5623=>3, 2534=>1},
    #   "bookmark"=>{6899=>1, 3234=>4},
    #   "comment"=>{6899=>1},
    #   "like"=>{6899=>1},
    #   "complete"=>{6900=>1}
    # }
    consumed_cards = service.get_consumed_cards(query_result)

    assert_equal %w(view bookmark comment like complete), consumed_cards.keys
    assert_equal 1, consumed_cards["like"].values.sum
    assert_equal 1, consumed_cards["bookmark"].values.sum
    assert_equal 1, consumed_cards["comment"].values.sum
    assert_equal 1, consumed_cards["complete"].values.sum
    assert_equal 2, consumed_cards["view"].keys.count
  end

  test 'get channel performance records as per date range' do
    service = AnalyticsReport::ChannelPerformance.new(
      organization_id: @org.id, from_date: 2.days.ago, to_date: Time.now)
    records = service.get_records

    assert_not_empty records
    assert_equal(
      {
        label: @public_channel.label,
        is_private: false,
        followers_count: 1,
        published_cards_count: 1,
        likes_count: 1,
        bookmarks_count: 1,
        comments_count: 1,
        views_count: 1,
        completes_count: 0
      },
      records[@public_channel.id]
    )

    assert_equal(
      {
        label: @private_channel.label,
        is_private: true,
        followers_count: 0,
        published_cards_count: 1,
        likes_count: 0,
        bookmarks_count: 0,
        comments_count: 0,
        views_count: 1,
        completes_count: 1
      },
      records[@private_channel.id]
    )

    service = AnalyticsReport::ChannelPerformance.new(
      organization_id: @org.id, from_date: 2.days.ago, to_date: 1.day.ago)
    records = service.get_records

    assert_equal(
      {
        label: @public_channel.label,
        is_private: false,
        followers_count: 0,
        published_cards_count: 0,
        likes_count: 0,
        bookmarks_count: 0,
        comments_count: 0,
        views_count: 0,
        completes_count: 0
      },
      records[@public_channel.id]
    )

    assert_equal(
      {
        label: @private_channel.label,
        is_private: true,
        followers_count: 0,
        published_cards_count: 0,
        likes_count: 0,
        bookmarks_count: 0,
        comments_count: 0,
        views_count: 0,
        completes_count: 0
      },
      records[@private_channel.id]
    )
  end

  test 'get followers count for channels' do
    service = AnalyticsReport::ChannelPerformance.new(
      organization_id: @org.id, from_date: 1.day.ago, to_date: Time.now)

    assert_empty service.followers_count_for_date_range([@public_channel.id, @private_channel.id], 2.day.ago, 1.day.ago)

    assert_equal({@public_channel.id => 1}, service.followers_count_for_date_range([@public_channel.id, @private_channel.id], 1.day.ago, Time.now))
  end

  test 'get published cards count for channels' do
    service = AnalyticsReport::ChannelPerformance.new(
      organization_id: @org.id, from_date: 1.day.ago, to_date: Time.now)

    assert_empty(service.published_cards_count_for_date_range([@public_channel.id, @private_channel.id], 2.day.ago, 1.day.ago))

    assert_equal({@public_channel.id => 1, @private_channel.id => 1}, service.published_cards_count_for_date_range([@public_channel.id, @private_channel.id], 1.day.ago, Time.now))
  end

  test 'should return count of followers from teams for channels' do
    team = create(:team, organization: @org)
    user1, user2 = create_list(:user,2, organization: @org)
    team.add_to_team([user1, user2])
    TeamsChannelsFollow.create(channel_id: @public_channel.id, team_id: team.id)
    service = AnalyticsReport::ChannelPerformance.new(
        organization_id: @org.id, from_date: 1.day.ago, to_date: Time.now)
    assert_equal({@public_channel.id => 3}, service.followers_count_for_date_range([@public_channel.id, @private_channel.id], 1.day.ago, Time.now))
  end

  test 'should return uniq count followers for channels' do
    team = create(:team, organization: @org)
    user1, user2 = create_list(:user,2, organization: @org)
    team.add_to_team([user1, user2])
    user1.follows.create(followable: @public_channel)
    TeamsChannelsFollow.create(channel_id: @public_channel.id, team_id: team.id)
    service = AnalyticsReport::ChannelPerformance.new(
        organization_id: @org.id, from_date: 1.day.ago, to_date: Time.now)
    assert_equal({@public_channel.id => 3}, service.followers_count_for_date_range([@public_channel.id, @private_channel.id], 1.day.ago, Time.now))
  end
end

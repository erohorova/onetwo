require 'test_helper'

class AnalyticsReport::UserContentActivityReportTest < ActiveSupport::TestCase
  self.use_transactional_fixtures = false

  setup do
    IndexJob.stubs(:perform_later)
    ChannelsCard.any_instance.stubs :create_users_management_card

    @org, @another_org = create_list(:organization, 2)
    @user, @actor, @owner = create_list(:user, 3, organization: @org)
    @another_user, @another_actor, @another_owner = create_list(:user, 3, organization: @org)

    WebMock.disable!
    MetricRecord.create_index! force: true
    MetricRecord.gateway.refresh_index!

    @first_card, @second_card = create_list(:card, 2, author: @owner)
  end

  teardown do
    WebMock.enable!
  end

  test 'should generate content analytics report data #week' do
    Timecop.freeze(Time.now.utc.prev_week + 2.days) do
      valid_mr = MetricRecord.new(action: "create", actor_id: @actor.id, object_type: 'card', object_id: @first_card.id, organization_id: @org.id, owner_type: 'user', owner_id: @owner.id, properties: {view: 'full'}, platform: 'web')
      valid_mr.save
      invalid_mr = MetricRecord.new(action: "create", actor_id: @another_actor.id, object_type: 'card', object_id: @first_card.id, organization_id: @another_org.id, owner_type: 'user', owner_id: @another_owner.id, properties: {view: 'full'}, platform: 'web')
      invalid_mr.save
    end

    MetricRecord.gateway.refresh_index!
    timestamp = Time.now.utc.prev_week

    @user_content_activity_report = AnalyticsReport::UserContentActivityReport.new(organization: @org, timestamp: timestamp, period: :week)
    AnalyticsReport::UserContentActivityReport.any_instance.expects(:upload_to_s3).once
    created_at = Time.now.utc.prev_week + 2.days
    data = [[created_at.strftime("%m/%d/%Y"), created_at.strftime("%H:%M:%S %z"), @actor.email, @actor.full_name, @first_card.message, "Smartbite", @owner.full_name, "Created"]]
    report_details = [["Organization: #{@org.name}"], ["Period: #{PeriodOffsetCalculator::timestamp_to_readable_period(timestamp: timestamp, period: :week)}"]]
    headers = [AnalyticsReport::UserContentActivityReport::REPORT_HEADERS]

    AnalyticsReport::UserContentActivityReport.any_instance.expects(:generate_csv).with(report_details + headers + data).returns(Tempfile.new(['', '.csv']))
    @user_content_activity_report.generate_report
  end

  test 'should generate social analytics report data #week' do
    # first card Metric records
    ['like', 'unlike', 'assign'].each_with_index do |action, index|
      Timecop.freeze(Time.now.utc.prev_week + (index + 3).days) do
        valid_mr = MetricRecord.new(action: action, actor_id: @actor.id, object_type: 'card', object_id: @first_card.id, organization_id: @org.id, owner_type: 'user', owner_id: @owner.id, platform: 'web')
        valid_mr.save
      end
    end

    # second card event
    ['playback', 'comment', 'assign'].each_with_index do |action, index|
      Timecop.freeze(Time.now.utc.prev_week + index.days) do
        valid_mr = MetricRecord.new(action: action, actor_id: @actor.id, object_type: 'card', object_id: @second_card.id, organization_id: @org.id, owner_type: 'user', owner_id: @owner.id, platform: 'web')
        valid_mr.save
      end
    end

    MetricRecord.gateway.refresh_index!
    timestamp = Time.now.utc.prev_week

    @user_content_activity_report = AnalyticsReport::UserContentActivityReport.new(organization: @org, timestamp: timestamp, period: :week, report_type: 'social')
    AnalyticsReport::UserContentActivityReport.any_instance.expects(:upload_to_s3).once
    created_ats = [Time.now.utc.prev_week + 5.days, Time.now.utc.prev_week + 4.days, Time.now.utc.prev_week + 3.days,
      Time.now.utc.prev_week + 2.days, Time.now.utc.prev_week + 1.days, Time.now.utc.prev_week]

    dates_formatted = created_ats.map {|date| date.strftime("%m/%d/%Y")}
    timestamps_formatted = created_ats.map { |timestamp| timestamp.strftime("%H:%M:%S %z") }
    data = [
      [dates_formatted[0], timestamps_formatted[0], @actor.email, @actor.full_name, @first_card.message, "Smartbite", @owner.full_name, "Assigned"],
      [dates_formatted[1], timestamps_formatted[1], @actor.email, @actor.full_name, @first_card.message, "Smartbite", @owner.full_name, "Unliked"],
      [dates_formatted[2], timestamps_formatted[2], @actor.email, @actor.full_name, @first_card.message, "Smartbite", @owner.full_name, "Liked"],

      [dates_formatted[3], timestamps_formatted[3], @actor.email, @actor.full_name, @second_card.message, "Smartbite", @owner.full_name, "Assigned"],
      [dates_formatted[4], timestamps_formatted[4], @actor.email, @actor.full_name, @second_card.message, "Smartbite", @owner.full_name, "Commented"],
      [dates_formatted[5], timestamps_formatted[5], @actor.email, @actor.full_name, @second_card.message, "Smartbite", @owner.full_name, "Played"]
    ]
    report_details = [["Organization: #{@org.name}"], ["Period: #{PeriodOffsetCalculator::timestamp_to_readable_period(timestamp: timestamp, period: :week)}"]]
    headers = [AnalyticsReport::UserContentActivityReport::REPORT_HEADERS]

    AnalyticsReport::UserContentActivityReport.any_instance.expects(:generate_csv).with(report_details + headers + data).returns(Tempfile.new(['', '.csv']))
    @user_content_activity_report.generate_report
  end

  test 'should generate social analytics report #follow, #unfollow channel and user' do
    Analytics::MetricsRecorderJob.stubs(:perform_later)
    user = create(:user, organization: @org)
    channel = create(:channel, organization: @org, user: @owner)

    Timecop.freeze(Time.now.utc.prev_week) do
      create(:follow, followable: user, user: @actor)
      create(:follow, followable: channel, user: @actor)

      user_follow = MetricRecord.new(action: "follow", actor_id: @actor.id, object_type: 'user', object_id: user.id, organization_id: @org.id, owner_type: 'user', owner_id: user.id, platform: 'web')
      user_follow.save

      channel_follow = MetricRecord.new(created_at: Time.current + 2.days, action: "follow", actor_id: @actor.id, object_type: 'channel', object_id: channel.id, organization_id: @org.id, owner_type: 'user', owner_id: @owner.id, platform: 'web')
      channel_follow.save

      user_unfollow = MetricRecord.new(created_at: Time.current + 4.days, action: "unfollow", actor_id: @actor.id, object_type: 'user', object_id: user.id, organization_id: @org.id, owner_type: 'user', owner_id: user.id, platform: 'web')
      user_unfollow.save

      channel_unfollow = MetricRecord.new(created_at: Time.current + 5.days, action: "unfollow", actor_id: @actor.id, object_type: 'channel', object_id: channel.id, organization_id: @org.id, owner_type: 'user', owner_id: @owner.id, platform: 'web')
      channel_unfollow.save
    end

    MetricRecord.gateway.refresh_index!
    timestamp = Time.now.utc.prev_week

    @user_content_activity_report = AnalyticsReport::UserContentActivityReport.new(organization: @org, timestamp: timestamp, period: :week, report_type: 'social')
    AnalyticsReport::UserContentActivityReport.any_instance.expects(:upload_to_s3).once

    created_ats = [Time.now.utc.prev_week + 5.days, Time.now.utc.prev_week + 4.days,
      Time.now.utc.prev_week + 2.days, Time.now.utc.prev_week]

    dates_formatted = created_ats.map {|date| date.strftime("%m/%d/%Y")}
    timestamps_formatted = created_ats.map { |timestamp| timestamp.strftime("%H:%M:%S %z") }

    data = [
      [dates_formatted[0], timestamps_formatted[0], @actor.email, @actor.full_name, channel.label, "Channel", @owner.full_name, "Unfollowed"],
      [dates_formatted[1], timestamps_formatted[1], @actor.email, @actor.full_name, "", "User", user.full_name, "Unfollowed"],

      [dates_formatted[2], timestamps_formatted[2], @actor.email, @actor.full_name, channel.label, "Channel", @owner.full_name, "Followed"],
      [dates_formatted[3], timestamps_formatted[3], @actor.email, @actor.full_name, "", "User", user.full_name, "Followed"]
    ]

    report_details = [["Organization: #{@org.name}"], ["Period: #{PeriodOffsetCalculator::timestamp_to_readable_period(timestamp: timestamp, period: :week)}"]]
    headers = [AnalyticsReport::UserContentActivityReport::REPORT_HEADERS]

    AnalyticsReport::UserContentActivityReport.any_instance.expects(:generate_csv).with(report_details + headers + data).returns(Tempfile.new(['', '.csv']))
    @user_content_activity_report.generate_report
  end

  test 'should generate social analytics report data #week #bookmark and #completed' do
    ['bookmark', 'mark_completed_self', 'mark_completed_assigned'].each_with_index do |action, index|
      Timecop.freeze(Time.now.utc.prev_week + (index + 3).days) do
        valid_mr = MetricRecord.new(action: action, actor_id: @actor.id, object_type: 'card', object_id: @first_card.id, organization_id: @org.id, owner_type: 'user', owner_id: @owner.id, platform: 'web')
        valid_mr.save
      end
    end

    MetricRecord.gateway.refresh_index!
    timestamp = Time.now.utc.prev_week

    @user_content_activity_report = AnalyticsReport::UserContentActivityReport.new(organization: @org, timestamp: timestamp, period: :week, report_type: 'social')
    AnalyticsReport::UserContentActivityReport.any_instance.expects(:upload_to_s3).once

    created_ats = [Time.now.utc.prev_week + 5.days, Time.now.utc.prev_week + 4.days, Time.now.utc.prev_week + 3.days]

    dates_formatted = created_ats.map {|date| date.strftime("%m/%d/%Y")}
    timestamps_formatted = created_ats.map { |timestamp| timestamp.strftime("%H:%M:%S %z") }

    data = [
      [dates_formatted[0], timestamps_formatted[0], @actor.email, @actor.full_name, @first_card.message, "Smartbite", @owner.full_name, "Completed"],
      [dates_formatted[1], timestamps_formatted[1], @actor.email, @actor.full_name, @first_card.message, "Smartbite", @owner.full_name, "Completed"],
      [dates_formatted[2], timestamps_formatted[2], @actor.email, @actor.full_name, @first_card.message, "Smartbite", @owner.full_name, "Bookmarked"],
    ]

    report_details = [["Organization: #{@org.name}"], ["Period: #{PeriodOffsetCalculator::timestamp_to_readable_period(timestamp: timestamp, period: :week)}"]]
    headers = [AnalyticsReport::UserContentActivityReport::REPORT_HEADERS]

    AnalyticsReport::UserContentActivityReport.any_instance.expects(:generate_csv).with(report_details + headers + data).returns(Tempfile.new(['', '.csv']))
    @user_content_activity_report.generate_report
  end
end
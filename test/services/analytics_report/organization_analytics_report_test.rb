require 'test_helper'

class AnalyticsReport::OrganizationAnalyticsReportTest < ActiveSupport::TestCase
  setup do
    @org = create(:organization)
    @user = create(:user, organization: @org)
  end

  test 'should generate org analytics report data #day' do
    timestamp = Time.current.utc - 1.day
    @org_analytics_report = AnalyticsReport::OrganizationAnalyticsReport.new(organization: @org, timestamp: timestamp, period: :day)
    aws_url = mock()

    metrics_data = {smartbites_consumed: 6, smartbites_created: 15, active_users: 3, average_session: 10, new_users: 1, engagement_index: 75, total_users: 4}
    report_details = [["Organization: #{@org.name}"], ["Period: #{PeriodOffsetCalculator::timestamp_to_readable_period(timestamp: timestamp, period: :day)}"]]
    headers = AnalyticsReport::OrganizationAnalyticsReport::REPORT_HEADERS

    OrganizationMetricsGatherer.expects(:current_period_data).returns metrics_data
    AnalyticsReport::OrganizationAnalyticsReport.any_instance.expects(:upload_to_s3).once.returns(aws_url)

    # main assertion
    ReportService.any_instance.expects(:generate_csv).with(report_details + [headers] + [metrics_data.values]).returns(Tempfile.new(['', '.csv']))
    @org_analytics_report.generate_report
  end
end
require 'test_helper'

class AnalyticsReport::NewContentReportTest < ActiveSupport::TestCase
  setup do
    @org = create(:organization)
    @user = create(:user, organization_role: 'admin', organization: @org)
    @last_access_at = (Time.now - 1.hour).to_i
  end

  test 'should generate content activity report' do
    featured_count = Faker::Number.number(10)
    Search::CardSearch.any_instance.expects(:search).with(
      nil,
      @org,
      viewer: @user,
      filter_params: {
        card_type: Card::EXPOSED_TYPES,
        state: 'published',
        is_official: true,
        ranges: [{promoted_at: {gt: Time.at(@last_access_at)}}]
      },
      limit: 0
    ).returns(Search::CardSearchResponse.new(results: [], total: featured_count)).once

    curated_count = Faker::Number.number(10)
    Search::CardSearch.
        any_instance.
        expects(:search_for_cms).
        with('', @org[:id], filter_params: {state: ['new','archived','published'], ranges: [{created_at: {gt: Time.at(@last_access_at)}}]}, limit: 0).
        returns(Search::CardSearchResponse.new( results: [], total: curated_count)).once

    card = create(:card, author: @user)
    create(:assignment, assignable: card, assignee: @user, created_at: Time.current)

    report = AnalyticsReport::NewContentReport
                            .new(last_access: @last_access_at, content_types: ['featured', 'curated', 'required'], viewer: @user)
                            .generate_report

    assert_equal featured_count, report[:featured_count]
    assert_equal curated_count, report[:curated_count]
    assert_equal 1, report[:required_count]
  end
end

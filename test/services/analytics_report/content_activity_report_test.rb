require 'test_helper'

class AnalyticsReport::UserContentActivityReportTest < ActiveSupport::TestCase
  self.use_transactional_fixtures = false

  setup do
    IndexJob.stubs(:perform_later)
    ChannelsCard.any_instance.stubs :create_users_management_card

    @org = create(:organization)
    @user, @actor, @owner = create_list(:user, 3, organization: @org)
    @another_user, @another_actor, @another_owner = create_list(:user, 3, organization: @org)

    WebMock.disable!
    MetricRecord.create_index! force: true
    MetricRecord.gateway.refresh_index!

    @first_card, @second_card = create_list(:card, 2, author: @owner)
  end

  teardown do
    WebMock.enable!
  end

  test 'should generate content activity report' do
    Timecop.freeze(2.days.ago) do
      # add to channel
      channel = create(:channel, organization: @org)
      @first_card.channels << channel

      # add to pathway
      pathway_card = create(:card, card_type: 'pack', author: @owner)
      CardPackRelation.add(cover_id: pathway_card.id, add_id: @first_card.id, add_type: 'Card')
      pathway_card.publish!

      # Like Metric record
      like_mr = MetricRecord.new(action: "like", actor_id: @actor.id, object_type: 'card', object_id: @first_card.id, organization_id: @org.id, owner_type: 'user', owner_id: @owner.id, properties: {}, platform: 'web')
      like_mr.save

      #
      like_mr = MetricRecord.new(action: "bookmark", actor_id: @actor.id, object_type: 'card', object_id: @first_card.id, organization_id: @org.id, owner_type: 'user', owner_id: @owner.id, properties: {}, platform: 'web')
      like_mr.save
    end
    MetricRecord.gateway.refresh_index!

    from_date = (Date.today - 5.days).strftime("%m/%d/%Y")
    to_date = Date.today.strftime("%m/%d/%Y")
    metrics = AnalyticsReport::ContentActivityReport.new(card_id: @first_card.id, from_date: from_date,
      to_date: to_date, action_types: ['add_to_pathway', 'add_to_channel', 'like']).generate_report

    assert_equal 3, metrics[:total]
    assert_same_elements ['add_to_channel', 'add_to_pathway', 'like'], metrics[:data].map {|m| m[:action]}
  end
end
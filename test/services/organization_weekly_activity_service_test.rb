require 'test_helper'
class OrganizationWeeklyActivityServiceTest < ActiveSupport::TestCase
  # self.use_transactional_fixtures = false
  setup do
    skip #deprecated
    WebMock.disable!
    @org = create(:organization)
    @user = create(:user, organization_id: @org.id)
    @author =  create(:user, organization_id: @org.id)
  end

  teardown do
    WebMock.enable!
  end

  test "top insights" do
    resource = create(:resource)
    file_resource = create(:file_resource, attachable: resource)
    cards = create_list(:card, 5, author:@author, organization_id: @org.id, resource: resource)
    old_card = create(:card, author:@author, organization_id: @org.id, created_at: 10.days.ago, resource: resource)
    5.times { create(:vote, voter:  create(:user, organization_id: @org.id) , votable:old_card)  }
    cards_with_votes = [cards[0], cards[2], cards[4]]
    cards_with_votes.each { |card| create(:vote, voter:  @user , votable: card) }
    Card.searchkick_index.refresh
    service = OrganizationWeeklyActivityService.new(@org)
    assert_equal service.activity[:insights].map(&:id).sort, cards_with_votes.map(&:id).sort
  end

  test "cards and pathways without images don't show up in the weekly update mail" do
    resource = create(:resource)
    file_resource = create(:file_resource, attachable: resource)
    card = create(:card, organization: @org, author: nil)
    card_with_resource = create(:card, organization: @org, author: nil, resource: resource)
    pathway = create(:card, card_type: "pack", card_subtype: "simple", author: nil, organization_id: @org.id)
    pathway_with_resource = create(:card, card_type: "pack", card_subtype: "simple", author: nil, organization_id: @org.id,
      resource: resource, state: 'published')
    [pathway, pathway_with_resource].each do |cover|
      CardPackRelation.add(cover_id: cover.id, add_id: card.id)
      cover.publish!
    end

    5.times { create(:cards_user, user:  create(:user, organization_id: @org.id) , card:card, state: 'upvote')  }
    4.times { create(:cards_user, user:  create(:user, organization_id: @org.id) , card:pathway, state: 'upvote') }
    weekly_activity = @org.weekly_activity_mail_attributes
    refute_includes(weekly_activity[:insights].map{|i| i.slug}, card.slug)
    assert_includes(weekly_activity[:insights].map{|i| i.slug}, card_with_resource.slug)
    refute_includes(weekly_activity[:pathways].map{|i| i.slug}, pathway.slug)
    assert_includes(weekly_activity[:pathways].map{|i| i.slug}, pathway_with_resource.slug)
  end

  test "top video streams" do
    video_streams = create_list(:wowza_video_stream, 5, creator: @author, organization_id: @org.id, state: 'published')
    old_video_stream = create(:wowza_video_stream, creator: @author, organization_id: @org.id, state: 'published', created_at: 10.days.ago)
    3.times { create(:vote, voter:  create(:user, organization_id: @org.id), votable: old_video_stream)  }
    video_streams_with_votes = [video_streams[0], video_streams[2], video_streams[4]]
    video_streams_with_votes.each { |video_stream| create(:vote, voter:  @user , votable: video_stream)  }
    VideoStream.searchkick_index.refresh
    service = OrganizationWeeklyActivityService.new(@org)
    assert_equal service.activity[:video_streams].map(&:id).sort, video_streams_with_votes.map(&:id).sort
  end

  test "top pathways" do
    resource = create(:resource)
    file_resource = create(:file_resource, attachable: resource)
    cards = create_list(:card, 5, card_type: 'pack', card_subtype: 'simple', author: @author, organization_id: @org.id, resource: resource)
    cards.each do |cover|
      CardPackRelation.add(cover_id: cover.id, add_id: create(:card, author: @author).id)
      cover.publish!
    end
    cards_with_votes = [cards[0], cards[2], cards[4]]
    cards_with_votes.each { |card| create(:vote, voter:  @user , votable:card)  }
    Card.searchkick_index.refresh
    service = OrganizationWeeklyActivityService.new(@org)
    assert_equal service.activity[:pathways].map(&:id).sort, cards_with_votes.map(&:id).sort
  end

  test 'top insights or pathways from cms or ecl shouldnot error for mail attributes' do
    resource = create(:resource)
    file_resource = create(:file_resource, attachable: resource)
    card = create(:card, organization: @org, author: nil, resource: resource)
    video_stream = create(:wowza_video_stream, creator: nil, organization_id: @org.id, state: 'published')
    pathway = create(:card, card_type: "pack", card_subtype: "simple", author: nil, organization_id: @org.id,
      resource: resource)
    CardPackRelation.add(cover_id: pathway.id, add_id: card.id)
    pathway.publish!
    Card.reindex

    5.times { create(:vote, voter:  create(:user, organization_id: @org.id), votable: card)  }
    4.times { create(:vote, voter:  create(:user, organization_id: @org.id), votable: pathway)  }
    4.times { create(:vote, voter:  create(:user, organization_id: @org.id), votable: video_stream)  }
    weekly_activity = @org.weekly_activity_mail_attributes

    assert_equal card.slug, weekly_activity[:insights][0][:slug]
    assert_equal pathway.slug, weekly_activity[:pathways][0][:slug]
    assert_equal video_stream.slug, weekly_activity[:video_streams][0][:slug]
  end

end

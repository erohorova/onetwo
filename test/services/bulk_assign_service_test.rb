require 'test_helper'

class CardImageCleanupServiceTest < ActiveSupport::TestCase
  setup do
    @org = create(:organization)
    @team = create(:team, organization: @org)
    @users = create_list(:user, 20, organization: @org)
    @admin_user = create(:user, organization: @org)

    @users.each {|user| @team.add_member(user)}
    @team.add_admin @admin_user

    @card = create(:card, author_id: @users.last.id)
  end

  test 'should bulk create assignment, team_assignments, learning queue for team users' do
    assert_differences [
                          [-> {Assignment.count}, 20],
                          [->{TeamAssignment.count}, 20],
                          [->{LearningQueueItem.count}, 20]] do
      BulkAssignService.new(team: @team, assignor_id: @admin_user.id, assignable_type: 'Card', 
        assignable_id: @card.id, opts: {message: "this is test assignment message"}).run(assignee_ids: @users.map(&:id), batch_size: 200)
    end

    assert_equal [@card.id], @users.first.assignments.map(&:assignable).map(&:id)
    assert_same_elements @users.map(&:id), @team.assignments.map(&:user_id).uniq
    assert_same_elements @users.map(&:id), LearningQueueItem.where(queueable: @card).pluck(:user_id).uniq
  end

  test 'should not create assignment if already present for user' do
    user1 = @users.first
    @team.add_admin @admin_user

    assignment = create(:assignment, assignable: @card, assignee: user1)
    create(:team_assignment, assignment: assignment, assignor: @admin_user)

    assert_differences [
                          [-> {Assignment.count}, 19],
                          [->{TeamAssignment.count}, 20]] do
      BulkAssignService.new(team: @team, assignor_id: @admin_user.id, assignable_type: 'Card', 
        assignable_id: @card.id, opts: {message: "this is test assignment message"}).run(assignee_ids: @users.map(&:id), batch_size: 200)
    end

    assert_same_elements @users.map(&:id), @team.assignments.map(&:user_id).uniq
    assert_same_elements @team.assignments.map(&:id), @team.team_assignments.map(&:assignment_id).uniq
  end

  test 'should create bulk assign notification job on bulk create assignment for team users' do
    BulkAssignAnalyticsRecordNotificationJob.expects(:perform_later).returns(nil).once

    BulkAssignService.new(team: @team, assignor_id: @admin_user.id, assignable_type: 'Card', 
        assignable_id: @card.id, opts: {message: "this is test assignment message"}).run(assignee_ids: @users.map(&:id), batch_size: 200)
  end
end
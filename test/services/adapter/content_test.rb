require 'test_helper'

class Adapter::ContentTest < ActiveSupport::TestCase
  setup do
    @org = create(:organization)
  end

  test "content channel_programming should call sociative and add card to channel" do
    channel = create(:channel, organization: @org)
    sociative_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/sociative/sociative.json"))["/v1/content/set"]
    Adapter::Sociative.any_instance.expects(:channel_programming).once.returns(sociative_response["cards"])
    Adapter::Content.new({organization_id: @org.id}).channel_programming(channel.id, {})
    card = Card.find_by_ecl_id(sociative_response["cards"][0]["id"])
    assert_not_nil Card.find_by_ecl_id(sociative_response["cards"][0]["id"])
    assert_same_elements [channel.id], card.channels.map(&:id)
  end

  test "content channel_programming should call sociative and add private card if source is private" do
    channel = create(:channel, organization: @org)
    sociative_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/sociative/sociative.json"))["/v1/content/set"]
    ecl_source = create(:ecl_source, channel: channel, ecl_source_id: sociative_response["cards"][0]["source_id"], private_content: true)
    Adapter::Sociative.any_instance.expects(:channel_programming).once.returns(sociative_response["cards"])
    Adapter::Content.new({organization_id: @org.id}).channel_programming(channel.id, {})
    card = Card.find_by_ecl_id(sociative_response["cards"][0]["id"])
    assert_not_nil Card.find_by_ecl_id(sociative_response["cards"][0]["id"])
    assert_same_elements [channel.id], card.channels.map(&:id)
    assert !card.is_public
  end
end
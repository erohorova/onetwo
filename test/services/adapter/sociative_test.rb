require 'test_helper'

class Adapter::SociativeTest < ActiveSupport::TestCase
  test "testing channel_programming response from sociative" do
  	org = create(:organization)
  	sociative_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/sociative/sociative.json"))["/v1/content/set"]
  	stub_request(:post, "#{Settings.sociative.sociative_url}/v1/content/set").to_return(:status => 200, :body => sociative_response.to_json)
  	content = Adapter::Sociative.new({organization_id: org.id}).channel_programming({})
  	assert_equal sociative_response["cards"], content
  end
end
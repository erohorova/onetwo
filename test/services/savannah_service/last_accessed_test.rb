require 'test_helper'

class SavannahService::LastAccessedTest < ActiveSupport::TestCase
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    initialize_resource_group
    @user = create :user, email: 'abcd@mail.com'
  end

  test '#get_last_accessed' do
    # Request stub
    stub_request(:get, Settings.savannah_base_location + '/api/offerings/CLIENT_RESOURCE_ID/last_accessed')
        .with(headers: {'Content-Type' => 'application/json', 'Accept' => 'application/json',
                        'X-Api-Key' => 'SAVANNAH_API_KEY', 'X-Shared-Secret' => 'SAVANNAH_SHARED_SECRET',
                        'X-Client-Name' => @resource_group.app_name, 'X-User-Email' => @user.email,
                        'X-Savannah-App-Id' => @org.savannah_app_id})
        .to_return(status: 200, body: '{"CONTENT": "#get_last_accessed"}', headers: {})

    status, data = SavannahService::LastAccessed.new(@resource_group).get_last_accessed(@user)

    assert_equal '#get_last_accessed', data['CONTENT']
    assert_equal :ok, status
  end

  test '#set_last_accessed' do
    # Request stub
    stub_request(:put, Settings.savannah_base_location + '/api/offerings/CLIENT_RESOURCE_ID/last_accessed')
        .with(headers: {'Content-Type' => 'application/json', 'Accept' => 'application/json',
                        'X-Api-Key' => 'SAVANNAH_API_KEY', 'X-Shared-Secret' => 'SAVANNAH_SHARED_SECRET',
                        'X-Client-Name' => @resource_group.app_name, 'X-User-Email' => @user.email,
                        'X-Savannah-App-Id' => @org.savannah_app_id},
              body: '{"last_module_id":"XYZ"}')
        .to_return(status: 200, body: '{"CONTENT": "#set_last_accessed"}', headers: {})

    status, data = SavannahService::LastAccessed.new(@resource_group).set_last_accessed(@user, "XYZ")

    assert_equal '#set_last_accessed', data['CONTENT']
    assert_equal :ok, status
  end

  private

  def initialize_resource_group
    credential = create :credential, api_key: 'SAVANNAH_API_KEY', shared_secret: 'SAVANNAH_SHARED_SECRET'
    client = create :client, credentials: [credential], name: 'DESTINY_CLIENT'
    @org = create(:organization, client: client)
    @resource_group = create :resource_group, organization: @org, client_resource_id: 'CLIENT_RESOURCE_ID'
  end
end

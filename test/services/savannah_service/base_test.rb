require 'test_helper'

class SavannahService::BaseTest < ActiveSupport::TestCase
  test '#json_request' do
    initialize_resource_group
    
    # Request stub
    stub_request(:get, Settings.savannah_base_location + '/ABCD')
        .with(headers: {'Content-Type' => 'application/json', 'Accept' => 'application/json',
                        'X-Api-Key' => 'SAVANNAH_API_KEY', 'X-Shared-Secret' => 'SAVANNAH_SHARED_SECRET',
                        'X-Savannah-App-Id' => @org.savannah_app_id})
        .to_return(status: 200, body: '{"CONTENT": "#json_request"}', headers: {})

    # Resource group

    status, data = SavannahService::Base.new(@resource_group).json_request('/ABCD', :get)

    assert_equal '#json_request', data['CONTENT']
    assert_equal true, status
  end

  test '#user_json_request' do
    # Resource group and user
    initialize_resource_group
    user = create :user, email: 'abcd@mail.com'

    # Request stub
    stub_request(:get, Settings.savannah_base_location + '/ABCD')
        .with(headers: {'Content-Type' => 'application/json', 'Accept' => 'application/json',
                        'X-Api-Key' => 'SAVANNAH_API_KEY', 'X-Shared-Secret' => 'SAVANNAH_SHARED_SECRET',
                        'X-Client-Name' => @resource_group.app_name, 'X-User-Email' => user.email,
                        'X-Savannah-App-Id' => @org.savannah_app_id})
        .to_return(status: 200, body: '{"CONTENT": "#user_json_request"}', headers: {})

    status, data = SavannahService::Base.new(@resource_group).user_json_request('/ABCD', :get, user)

    assert_equal '#user_json_request', data['CONTENT']
    assert_equal true, status
  end

  private

  def initialize_resource_group
    credential = create :credential, api_key: 'SAVANNAH_API_KEY', shared_secret: 'SAVANNAH_SHARED_SECRET'
    client = create :client, credentials: [credential], name: 'DESTINY_CLIENT'
    @org = create(:organization, client: client)
    @resource_group = create :resource_group, organization: @org
  end
end
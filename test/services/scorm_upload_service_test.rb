require 'test_helper'

class ScromUploadServiceTest < ActiveSupport::TestCase
  setup do
    @organization = create(:organization)
    @user = create(:user, organization: @organization)

    stub_request(:get, "https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv").
      to_return(:status => 200, :body => "", :headers => {})

    @scorm_card = create(:card, author: @user, organization: @organization, state: 'processing',
                    filestack: [scorm_course: true, size: 30000000,
                    url: 'https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv'])
  end

  test '#create_scorm_resource should not publish card if publish flag is off' do
    ScormUploadService.new.create_scorm_resource({title: 'title'}, @scorm_card, 'somecourse_id', publish=false)
    @scorm_card.reload
    assert_not_equal 'published', @scorm_card.state
  end

  test '#create_scorm_resource should publish card if publish flag is true' do
    ScormUploadService.new.create_scorm_resource({title: 'title'}, @scorm_card, 'somecourse_id', publish=true)
    @scorm_card.reload
    assert_equal 'published', @scorm_card.state
  end

  test 'upload to scorm service calls #async_import for zips more than 15MB' do
    ScormUploadService.any_instance.stubs(:get_upload_path)
    ScormUploadService.any_instance.expects(:async_import).once
    ScormUploadService.new.run(@scorm_card.id, 'somecourseid')
  end

  test 'upload to scorm service calls #sync_import for zips less than 15MB' do
    scorm_card = create(:card, author: @user, organization: @organization,
                    filestack: [scorm_course: true, size: 1000000,
                    url: 'https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv'])
    ScormUploadService.any_instance.stubs(:get_upload_path)
    ScormUploadService.any_instance.expects(:synchronous_import).once
    ScormUploadService.new.run(scorm_card.id, 'somecourseid')
  end

  test 'upload to scorm service calls #convert_into_temp_file if the file is in kb' do
    output = StringIO.new
    output.write('First line.\n')
    
    stub_request(:get, /.*cloud.scorm.com.*/).to_return(status: 200)
    stub_request(:post, /.*cloud.scorm.com.*/).to_return(status: 200)

    temp = Tempfile.new.tap do |file|
      file.binmode
      IO.copy_stream(output, file)
      output.close
      file.rewind
    end
    ScormUploadService.any_instance.stubs(:open).returns(output)
    ScormUploadService.any_instance.stubs(:get_upload_token).returns("7e514dbf-e565-42ba-b0e8-3ddb063b8b0b")
    ScormUploadService.any_instance.stubs(:convert_into_temp_file).returns(temp)
    ScormUploadService.any_instance.stubs(:synchronous_import)
    ScormUploadService.any_instance.stubs(:upload_file)
    ScormUploadService.any_instance.expects(:convert_into_temp_file).returns(temp)
    scorm_card = create(:card, author: @user, organization: @organization,
                    filestack: [scorm_course: true, size: 10,
                    url: 'https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv'])
    
    ScormUploadService.new.run(scorm_card.id, 'somecourseid')
  end
end
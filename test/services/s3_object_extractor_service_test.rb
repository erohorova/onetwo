require 'test_helper'

class S3ObjectExtractorServiceTest < ActiveSupport::TestCase
  setup do
    @client = Aws::S3::Client.new(stub_responses: true)
    @client.stub_responses(
      :list_buckets,
      {buckets: [{ name: 'my-bucket' }, {name: 'bucket-name'}]}
    )
    @client.stub_responses(
      :list_objects,
      Aws::S3::Types::ListObjectsOutput.new(
        is_truncated: false,
        contents: [
          {'key': 'image.jpeg'},
          {'key': 'image2.jpeg'},
          {'key': 'image3.jpeg'}
        ]
      )
    )
    @client.stub_responses(
      :head_object,
      {content_type: 'image/jpeg'}
    )
  end

  class BucketInfoExtractionTest < S3ObjectExtractorServiceTest
    setup do
      # console link
      @console_link_region = 'https://s3.console.aws.amazon.com/s3/buckets/bucket-name/?region=eu-central-1'
      @console_link_no_region = 'https://s3.console.aws.amazon.com/s3/buckets/bucket-name'
      #
      # virtual-hosted-style URL:
      #
      @vh_without_region = 'http://bucket-name.s3.amazonaws.com'
      @vh_with_region = 'http://bucket-name.s3-aws-region.amazonaws.com'
      #
      # path-style URL:
      #
      @ph_without_region = 'http://s3.amazonaws.com/bucket-name'
      @ph_with_region = 'http://s3-aws-region.amazonaws.com/bucket-name'

      S3ObjectExtractorService.stubs(:s3_client_default).returns(@client)
    end

    test 'should return bucket data from virtual hosted links' do
      with_region = S3ObjectExtractorService.extract_bucket_info(
        link: @vh_with_region
      )
      without_region = S3ObjectExtractorService.extract_bucket_info(
        link: @vh_without_region
      )

      assert_equal 'aws-region', with_region[:region]
      assert_equal 'bucket-name', with_region[:bucket]
      assert_equal 'bucket-name', without_region[:bucket]
      refute without_region[:region]
    end

    test 'should return bucket data from path-style links' do
      with_region = S3ObjectExtractorService.extract_bucket_info(
        link: @ph_with_region
      )
      without_region = S3ObjectExtractorService.extract_bucket_info(
        link: @ph_without_region
      )

      assert_equal 'aws-region', with_region[:region]
      assert_equal 'bucket-name', with_region[:bucket]
      assert_equal 'bucket-name', without_region[:bucket]
      refute without_region[:region]
    end

    test 'should return bucket data from console copied links' do
      with_region = S3ObjectExtractorService.extract_bucket_info(
        link: @console_link_region
      )
      without_region = S3ObjectExtractorService.extract_bucket_info(
        link: @console_link_no_region
      )

      assert_equal 'eu-central-1', with_region[:region]
      assert_equal 'bucket-name', with_region[:bucket]
      assert_equal 'bucket-name', without_region[:bucket]
      refute without_region[:region]
    end
  end

  class ImageUrlsExtractionTest < S3ObjectExtractorServiceTest

    setup do
      @urls = %w[
        https://bucket.s3.amazonaws.com/image.jpeg
        https://bucket.s3.amazonaws.com/image2.jpeg
        https://bucket.s3.amazonaws.com/image3.jpeg
      ]
    end

    test 'should return list of public urls' do
      image_extractor = S3ObjectExtractorService.new(
        bucket: 'bucket', region: 'region'
      )
      image_extractor.stubs(:s3_client).returns(@client)

      assert_same_elements @urls, image_extractor.extract_image_objects
    end

    test 'should not return not image objects' do
      @client.stub_responses(
        :head_object,
        {content_type: 'video/mp4'}
      )
      image_extractor = S3ObjectExtractorService.new(
        bucket: 'bucket', region: 'region'
      )
      image_extractor.stubs(:s3_client).returns(@client)

      assert_same_elements [], image_extractor.extract_image_objects
    end

    test 'should write exception info in log' do
      @client.stub_responses(:head_object, ['NoSuchKey'])
      @client.stub_responses(
        :list_objects,
        Aws::S3::Types::ListObjectsOutput.new(
          is_truncated: false,
          contents: [
            {'key': 'image.jpeg'}
          ]
        )
      )
      image_extractor = S3ObjectExtractorService.new(
        bucket: 'bucket', region: 'region'
      )
      msg = 'S3ObjectExtractorService #extract_image_objects error for image.jpeg.'
      msg_addition = ' Code:  NoSuchKey. Message: stubbed-response-error-message'
      image_extractor.stubs(:s3_client).returns(@client)
      log.expects(:warn).with(msg + msg_addition).once
      assert_empty image_extractor.extract_image_objects
      assert_equal ['image.jpeg'], image_extractor.failures[:object_errors]
    end

    test 'should properly handle InvalidAccessKeyId exception' do
      @client.stub_responses(
        :list_objects, ['InvalidAccessKeyId']
      )
      image_extractor = S3ObjectExtractorService.new(
        bucket: 'bucket', region: 'region'
      )
      image_extractor.stubs(:s3_client).returns(@client)

      expected_failures = <<-TXT.strip_heredoc.gsub(/^*\n/, ' ').chop
        S3ObjectExtractorService #extract_image_objects error.
        #<Aws::S3::Errors::InvalidAccessKeyId: stubbed-response-error-message>
      TXT

      log.expects(:warn).with(expected_failures).once

      res = image_extractor.extract_image_objects
      assert res.nil?

      message = 'AWS access error. Invalid keys'
      assert_equal message, image_extractor.failures[:aws_access]
    end
  end
end

require 'test_helper'

class Lrs::UserCoursesTest < ActiveSupport::TestCase
  setup do
    initialize_org
    @user = create :user, email: 'abcd@mail.com', organization: @org
  end

  test '#get_course_progress' do
    # Request stub
    expected_response = [{"_id"=>"123", "course"=>"test"},{"_id"=>"1234", "course"=>"test1"}]
    Lrs::UserCourses.any_instance.stubs(:user_courses_pipline).returns("/abcs&email=test@example.com")

    stub_request(:get, "https://lrs.edcast.com/abcs&email=test@example.com").
      to_return(:status => 200, :body => expected_response.to_json, :headers => {})

    resp = Lrs::UserCourses.new(org_id: @org.id).get_courses_status(@user)

    assert_equal expected_response, resp[:data]
    assert resp[:success]
  end

  private

  def initialize_org
    @org = create :organization
    cred = create :xapi_credential, organization: @org
  end
end

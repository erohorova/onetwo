require 'test_helper'

class Lrs::OrgCoursesTest < ActiveSupport::TestCase
  setup do
    @org = create :organization
    cred = create :xapi_credential, organization: @org
    @user = create :user, email: 'abcd@mail.com', organization: @org
    @card = create(:card, organization: @org, author_id: nil)
  end

  test 'should get LRS data and call create assignment for matched course' do
    expected_response = [
      {"_id"=>"123", "actor" => "mailto:#{@user.email}", "course"=>"test", "id" => 123, "url" => "test url"},
      {"_id"=>"1234", "actor" => "mailto:#{@user.email}", "course"=>"test1", "id" => 123, "url" => "test url"}
    ]
    Lrs::OrgCourses.any_instance.stubs(:courses_pipline).returns("/abcs&email=test@example.com")

    stub_request(:get, "https://lrs.edcast.com/abcs&email=test@example.com").
      to_return(:status => 200, :body => expected_response.to_json, :headers => {})

    Lrs::OrgCourses.any_instance.stubs(:get_card).returns(@card)
    LrsAssignmentService.any_instance.expects(:run).twice
    Lrs::OrgCourses.new(org_id: @org.id).run
  end


  test "should not hit assignment service if user is not present" do
    expected_response = [
      {"_id"=>"123", "actor" => "mailto:test@test.com", "course"=>"test", "id" => 123, "url" => "test url"},
      {"_id"=>"1234", "actor" => "mailto:#{@user.email}", "course"=>"test1", "id" => 123, "url" => "test url"}
    ]
    Lrs::OrgCourses.any_instance.stubs(:courses_pipline).returns("/abcs&email=test@example.com")

    stub_request(:get, "https://lrs.edcast.com/abcs&email=test@example.com").
      to_return(:status => 200, :body => expected_response.to_json, :headers => {})

    Lrs::OrgCourses.any_instance.stubs(:get_card).returns(@card)
    LrsAssignmentService.any_instance.expects(:run).once
    resp = Lrs::OrgCourses.new(org_id: @org.id).run
  end

  test "should not hit assignment service if ecl content item is not present" do
    expected_response = [
      {"_id"=>"123", "actor" => "mailto:test@test.com", "course"=>"test", "id" => 123, "url" => "test url"},
      {"_id"=>"1234", "actor" => "mailto:#{@user.email}", "course"=>"test1", "id" => 123, "url" => "test url"}
    ]

    Lrs::OrgCourses.any_instance.stubs(:courses_pipline).returns("/abcs&email=test@example.com")

    stub_request(:get, "https://lrs.edcast.com/abcs&email=test@example.com").
      to_return(:status => 200, :body => expected_response.to_json, :headers => {})

    Lrs::OrgCourses.any_instance.stubs(:get_card).returns(nil)
    LrsAssignmentService.any_instance.expects(:run).never
    resp = Lrs::OrgCourses.new(org_id: @org.id).run
  end
end

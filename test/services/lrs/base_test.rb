require 'test_helper'

class Lrs::BaseTest < ActiveSupport::TestCase
  test 'success json_request' do
    initialize_org

    #Stub LRS request
    stub_request(:get, "https://lrs.edcast.com/ABCD").
      to_return(status: 200, body: '{"CONTENT": "#json_request"}', headers: {})

    resp = Lrs::Base.new(org_id: @org.id).json_request('/ABCD', :get)

    assert_equal "#json_request", resp[:data]["CONTENT"]
    assert resp[:success]
  end

   test 'failure json_request' do
    initialize_org

    #Stub LRS request
    stub_request(:get, "https://lrs.edcast.com/ABCD").
      to_return(status: 500, body: "", headers: {})

    resp = Lrs::Base.new(org_id: @org.id).json_request('/ABCD', :get)

    refute resp[:success]
  end

  test 'split ecl_id from url' do
    initialize_org

    courses = [
      {"_id"=>"123", "actor" => "mailto:test@test.com", "course"=>"test", "url" => "http://test.com/insights/what-are?ecl_id=fdff1507-6cab"},
    ]
    user = create(:user, organization: @org)
    Card.expects(:find_by_resource_url).with("http://test.com/insights/what-are", @org.id).once
    card = Lrs::Base.new(org_id: @org.id).get_card(user, courses[0])
  end

  def initialize_org
    @org = create :organization
    cred = create :xapi_credential, organization: @org
  end
end
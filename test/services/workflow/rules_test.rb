require 'test_helper'

module WorkflowService
  class RulesTest < ActiveSupport::TestCase
    setup do
      @org = create(:organization)
      @creator = create :user, organization: @org
    end

    test 'get unique identifier if output is `create_dynamic`' do
      workflow = create(
        :workflow,
        user_id: @creator.id,
        organization: @org,
        is_enabled: false,
        wf_rules: {
          and: [
            { equals: {country: 'US'} },
            { not_equals: {location: 'SF'} },
            { must_exist: 'location' }
          ]
        },
        wf_action: {
          entity: 'Team',
          type: 'create_dynamic',
          values: [
            {
              column_name: 'name',
              wf_column_name: 'location',
              prefix: '',
              suffix: ''
            }
          ],
          metadata: {
            is_private: true,
            description: 'Group created from workflow',
            is_mandatory: true
          }
        }
      )
      columns = WorkflowService::Rules.new(rules: workflow.wf_rules).fetch_columns
      assert_instance_of Array, columns
      assert_same_elements %w[country location], columns
    end

    test 'filter input data according to the given rules AND' do
      workflow = @workflow = create(
        :workflow,
        user_id: @creator.id,
        organization: @org
      )

      custom_field_country = create :custom_field, organization: @org, abbreviation: 'country', display_name: 'country'
      custom_field_location = create :custom_field, organization: @org, abbreviation: 'location', display_name: 'location'

      user2 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      create :user_custom_field, user: user2, custom_field: custom_field_country, value: 'India'

      user3 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      create :user_custom_field, user: user3, custom_field: custom_field_location, value: 'Mumbai'
      create :user_custom_field, user: user3, custom_field: custom_field_country, value: 'India'

      user4 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      create :user_custom_field, user: user4, custom_field: custom_field_location, value: 'Pune'
      create :user_custom_field, user: user4, custom_field: custom_field_country, value: 'India'

      user5 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      create :user_custom_field, user: user5, custom_field: custom_field_location, value: 'Bangalore'
      create :user_custom_field, user: user5, custom_field: custom_field_country, value: 'India'

      user6 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      create :user_custom_field, user: user6, custom_field: custom_field_location, value: nil
      create :user_custom_field, user: user6, custom_field: custom_field_country, value: 'India'

      user7 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      create :user_custom_field, user: user7, custom_field: custom_field_location, value: ''
      create :user_custom_field, user: user7, custom_field: custom_field_country, value: 'India'

      rules_service = WorkflowService::Rules.new(rules: workflow.wf_rules)
      users = User.where(id: [user2.id, user3.id, user4.id, user5.id, user6.id, user7.id])

      filtered_input = rules_service.filter(data: users, input_type: 'User', org: @org)

      assert_equal [user4.id, user5.id], filtered_input.map(&:id).compact
    end

    test 'filter input data according to the given rules OR' do
      workflow = @workflow = create(
        :workflow,
        user_id: @creator.id,
        organization: @org
      )

      workflow.wf_rules = {
        or:[
          { equals: { religion: 'Hindu' } },
          { must_exist: 'day' },
          { lte: { age: '70' } }
        ]
      }

      workflow.save!

      custom_field_religion = create :custom_field, organization: @org, abbreviation: 'religion', display_name: 'religion'
      custom_field_day = create :custom_field, organization: @org, abbreviation: 'day', display_name: 'day'
      custom_field_age = create :custom_field, organization: @org, abbreviation: 'age', display_name: 'Age'
      custom_field_color = create :custom_field, organization: @org, abbreviation: 'color', display_name: 'color'

      user2 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      create :user_custom_field, user: user2, custom_field: custom_field_religion, value: 'Hindu'
      create :user_custom_field, user: user2, custom_field: custom_field_day, value: 'Monday'

      user3 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active',
                     is_anonymized: false, first_name: nil
      create :user_custom_field, user: user3, custom_field: custom_field_religion, value: 'Hindu'
      create :user_custom_field, user: user3, custom_field: custom_field_day, value: 'Tuesday'

      user4 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      create :user_custom_field, user: user4, custom_field: custom_field_age, value: '50'
      create :user_custom_field, user: user4, custom_field: custom_field_day, value: 'Wednesday'

      user5 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      create :user_custom_field, user: user5, custom_field: custom_field_age, value: '80'

      rules_service = WorkflowService::Rules.new(rules: workflow.wf_rules)
      input_data = User.where(id: [user2.id, user3.id, user4.id, user5.id])
      assert_equal [user2.id, user3.id, user4.id], rules_service.filter(data: input_data, input_type: 'User', org: @org).map(&:id)
    end

    # null check - nil value - positive
    test 'when user is having NULL as value in custom field consider it as must_not_exist' do
      workflow = @workflow = create(
        :workflow,
        user_id: @creator.id,
        organization: @org
      )
      workflow.wf_rules = {
        and:[
          { must_not_exist: 'religion' }
        ]
      }
      workflow.save!
      custom_field_religion = create :custom_field, organization: @org, abbreviation: 'religion', display_name: 'religion'
      user = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      create :user_custom_field, user: user, custom_field: custom_field_religion, value: nil

      rules_service = WorkflowService::Rules.new(rules: workflow.wf_rules)
      input_data = User.where(id: user.id)
      assert_equal user.id, rules_service.filter(data: input_data, input_type: 'User', org: @org)[0].id
    end

    # null check - nil value - negative
    test 'when user is having NULL as value in custom field do not consider it as must_exist' do
      workflow = @workflow = create(
          :workflow,
          user_id: @creator.id,
          organization: @org
      )
      workflow.wf_rules = {
        and:[
          { must_exist: 'religion' }
        ]
      }
      workflow.save!
      custom_field_religion = create :custom_field, organization: @org, abbreviation: 'religion', display_name: 'religion'
      user = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      create :user_custom_field, user: user, custom_field: custom_field_religion, value: nil

      rules_service = WorkflowService::Rules.new(rules: workflow.wf_rules)
      input_data = User.where(id: user.id)
      assert_nil rules_service.filter(data: input_data, input_type: 'User', org: @org)[0]&.id
    end

    # null check - no associated record - positive
    test 'when user is not having user custom field record consider it as must_not_exist' do
      workflow = @workflow = create(
        :workflow,
        user_id: @creator.id,
        organization: @org
      )
      workflow.wf_rules = {
        and: [
          { must_not_exist: 'religion' }
        ]
      }
      workflow.save!
      create :custom_field, organization: @org, abbreviation: 'religion', display_name: 'religion'
      user = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false

      rules_service = WorkflowService::Rules.new(rules: workflow.wf_rules)
      input_data = User.where(id: user.id)
      assert_equal user.id, rules_service.filter(data: input_data, input_type: 'User', org: @org)[0]&.id
    end

    # null check - no associated record - negative
    test 'when user is not having user custom field record do not consider it as must_exist' do
      workflow = @workflow = create(
          :workflow,
          user_id: @creator.id,
          organization: @org
      )
      workflow.wf_rules = {
        and: [
          { must_exist: 'religion' }
        ]
      }
      workflow.save!
      create :custom_field, organization: @org, abbreviation: 'religion', display_name: 'religion'
      user = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false

      rules_service = WorkflowService::Rules.new(rules: workflow.wf_rules)
      input_data = User.where(id: user.id)
      assert_nil rules_service.filter(data: input_data, input_type: 'User', org: @org)[0]&.id
    end

    # must exist - ''
    test 'when user is having empty string as value in custom field consider it as must_exist' do
      workflow = @workflow = create(
        :workflow,
        user_id: @creator.id,
        organization: @org
      )
      workflow.wf_rules = {
        and: [
          { must_exist: 'religion' }
        ]
      }
      workflow.save!
      custom_field_religion = create :custom_field, organization: @org, abbreviation: 'religion', display_name: 'religion'
      user = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      create :user_custom_field, user: user, custom_field: custom_field_religion, value: ''

      rules_service = WorkflowService::Rules.new(rules: workflow.wf_rules)
      input_data = User.where(id: user.id)
      assert_nil rules_service.filter(data: input_data, input_type: 'User', org: @org)[0]&.id
    end

    # must not exist - ''
    test 'when user is having empty string as value in custom field do not consider it as must_not_exist' do
      workflow = @workflow = create(
        :workflow,
        user_id: @creator.id,
        organization: @org
      )
      workflow.wf_rules = {
        and: [
          { must_not_exist: 'religion' }
        ]
      }
      workflow.save!
      custom_field_religion = create :custom_field, organization: @org, abbreviation: 'religion', display_name: 'religion'
      user = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      create :user_custom_field, user: user, custom_field: custom_field_religion, value: ''

      rules_service = WorkflowService::Rules.new(rules: workflow.wf_rules)
      input_data = User.where(id: user.id)
      assert_equal user.id, rules_service.filter(data: input_data, input_type: 'User', org: @org)[0]&.id
    end

    # comparison
    test 'should consider user below 30 years of age' do
      skip 'removed support for less than comparison, hence skipping!'
      workflow = @workflow = create(
        :workflow,
        user_id: @creator.id,
        organization: @org
      )
      workflow.wf_rules = {
        and:[
          lt: { age: '30' }
        ]
      }
      workflow.save!
      custom_field_age = create :custom_field, organization: @org, abbreviation: 'age', display_name: 'age'
      user = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      create :user_custom_field, user: user, custom_field: custom_field_age, value: '29'
      user2 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      create :user_custom_field, user: user2, custom_field: custom_field_age, value: '31'

      rules_service = WorkflowService::Rules.new(rules: workflow.wf_rules)
      input_data = User.where(id: [user.id, user2.id])
      assert_same_elements [user.id], rules_service.filter(data: input_data, input_type: 'User', org: @org).map(&:id)
    end

    # comparison
    test 'should not consider user who does not have user custom field record of age' do
      skip 'removed support for less than comparison, hence skipping!'
      workflow = @workflow = create(
        :workflow,
        user_id: @creator.id,
        organization: @org
      )
      workflow.wf_rules = {
        and:[
          lt: { age: '30' }
        ]
      }
      workflow.save!
      create :custom_field, organization: @org, abbreviation: 'age', display_name: 'age'
      user = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false

      rules_service = WorkflowService::Rules.new(rules: workflow.wf_rules)
      input_data = User.where(id: user.id)
      assert_nil rules_service.filter(data: input_data, input_type: 'User', org: @org)[0]&.id
    end

    # comparison
    test 'should not consider user who have empty string age as user custom field' do
      skip 'removed support for less than comparison, hence skipping!'
      workflow = @workflow = create(
        :workflow,
        user_id: @creator.id,
        organization: @org
      )
      workflow.wf_rules = {
        and:[
          lt: { age: '30' }
        ]
      }
      workflow.save!
      custom_field_age = create :custom_field, organization: @org, abbreviation: 'age', display_name: 'age'
      user = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      create :user_custom_field, user: user, custom_field: custom_field_age, value: ''

      rules_service = WorkflowService::Rules.new(rules: workflow.wf_rules)
      input_data = User.where(id: user.id)
      assert_nil rules_service.filter(data: input_data, input_type: 'User', org: @org)[0]&.id
    end
  end
end
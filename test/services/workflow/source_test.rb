require 'test_helper'

module WorkflowService
  class SourceTest < ActiveSupport::TestCase
    setup do
      @org = create(:organization)
      Role.create_standard_roles @org
      u1, u2, u3, u4, u5 = create_list(:user, 5, organization: @org)
      @users = [u1, u2, u3, u4, u5]
      @u_profiles = [u1, u2, u3, u4, u5].map do |usr|
        Profile.create(
          user_id: usr.id, organization_id: @org.id,
          external_id: "ext_id-#{usr.id}", uid: "uid-#{usr.id}"
        )
      end
    end

    test 'should return custom_fields in input source if present' do
      cf = create :custom_field, organization: @org, display_name: 'custom', abbreviation: 'custom'
      expected_columns_keys = %w[first_name last_name]
      last_p = Profile.last
      user = User.find_by(id: last_p.user_id)
      create :user_custom_field, user_id: user.id, custom_field_id: cf.id, value: 'last_user_value'
      expected_columns_keys.push(cf.abbreviation)
      @columns = WorkflowService::Source.new(type: 'user', organization: user.organization).fields
      columns_real_keys = @columns.map { |el| el[:column_name] }
      assert_same_elements expected_columns_keys, columns_real_keys
    end

    test 'should return custom_fields in input source only once' do
      cf = create :custom_field, organization: @org, display_name: 'custom', abbreviation: 'custom'
      expected_columns_keys = %w[first_name last_name]
      last_p = Profile.last
      user = User.find_by(id: last_p.user_id)
      create :user_custom_field, user_id: user.id, custom_field_id: cf.id, value: 'last_user_value'
      expected_columns_keys.push(cf.abbreviation)
      @columns = WorkflowService::Source.new(type: 'user', organization: user.organization).fields
      columns_real_keys = @columns.map { |el| el[:column_name] }
      assert_same_elements expected_columns_keys, columns_real_keys
      assert_equal 3, columns_real_keys.length

      @columns1 = WorkflowService::Source.new(type: 'user', organization: user.organization).fields
      columns_real_keys1 = @columns1.map { |el| el[:column_name] }
      assert_same_elements expected_columns_keys, columns_real_keys1
      assert_equal 3, columns_real_keys1.length
    end

    test 'should return blank array in input source if type is channel or team' do
      cf = create :custom_field, organization: @org, display_name: 'custom', abbreviation: 'custom'
      last_p = Profile.last
      user = User.find_by(id: last_p.user_id)
      create :user_custom_field, user_id: user.id, custom_field_id: cf.id, value: 'last_user_value'
      @columns = WorkflowService::Source.new(type: 'channel', organization: user.organization).fields
      columns_real_keys = @columns.map { |el| el[:column_name] }
      assert_same_elements [], columns_real_keys
    end

    test 'should return sample data when source type as user is provided' do
      user = @users.first
      preview_data = WorkflowService::Source.new(type: 'user', organization: user.organization).preview_data
      assert_equal 3, preview_data.length
    end

    test 'should return empty array as sample data when not valid source type is provided' do
      user = @users.first
      preview_data = WorkflowService::Source.new(type: 'xyz', organization: user.organization).preview_data
      assert_empty preview_data
    end
  end
end
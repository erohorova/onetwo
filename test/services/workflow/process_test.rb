require 'test_helper'

module WorkflowService
  class ProcessTest < ActiveSupport::TestCase
    setup do
      @org = create :organization
      @creator = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      @workflow = @workflow = create :workflow, user_id: @creator.id, organization: @org

      custom_field_country = create :custom_field, organization: @org, abbreviation: 'country', display_name: 'country'
      custom_field_location = create :custom_field, organization: @org, abbreviation: 'location', display_name: 'location'

      @user2 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      create :user_custom_field, user: @user2, custom_field: custom_field_country, value: 'India'

      @user3 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active',
                     is_anonymized: false, first_name: nil
      create :user_custom_field, user: @user3, custom_field: custom_field_country, value: 'India'

      @user4 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      create :user_custom_field, user: @user4, custom_field: custom_field_location, value: 'Mumbai'
      create :user_custom_field, user: @user4, custom_field: custom_field_country, value: 'India'

      @user5 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      create :user_custom_field, user: @user5, custom_field: custom_field_location, value: 'Pune'
      create :user_custom_field, user: @user5, custom_field: custom_field_country, value: 'India'

      @user6 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      create :user_custom_field, user: @user6, custom_field: custom_field_location, value: 'Mumbai'
      create :user_custom_field, user: @user6, custom_field: custom_field_country, value: 'USA'
    end

    test 'test workflow is executed properly' do
      WorkflowService::Process.new(workflow: @workflow).run
      assert_equal 1, Team.all.length

      team = Team.first

      assert_equal 'Pune', team.name

      # Out of 2 selected, one is skipped as that user is not having +location+ field
      assert_equal 1, team.teams_users.length

      # assert that user5 is added to the group
      assert_equal @user5.id, team.users.first.id

      # assert metadata of group
      assert team.is_private
      assert team.is_mandatory
      assert_equal 'Group created from workflow', team.description

      # assert wf_output_result
      assert_equal 'Team', @workflow.reload.wf_output_result['type']
      assert_same_elements [team.id], @workflow.reload.wf_output_result['ids']
    end

    # Iterate all the users and find or create Team if there is value for that field of the user
    test 'test workflow is executed properly when rules are not present' do
      @workflow.wf_rules = nil
      @workflow.save!

      assert_nil @workflow.reload.wf_rules

      WorkflowService::Process.new(workflow: @workflow).run

      assert_equal 2, Team.all.length
      assert_same_elements %w[Mumbai Pune], Team.pluck(:name)
    end

    test 'workflow state should change properly' do
      assert_equal Workflow::NEW, @workflow.status
      WorkflowService::Process.new(workflow: @workflow).run
      assert_equal Workflow::COMPLETED, @workflow.status
    end

    # CREATE DYNAMIC
    test 'user removal from group on custom field change when workflow type is dynamic' do
      assert_equal WorkflowService::Output::CREATE_DYNAMIC, @workflow.wf_action['type']

      # user added to team that will be created by workflow
      team = create :team, name: 'Pune', description: 'Group created from workflow', organization: @org
      team.add_user(@user5)
      assert_same_elements [@user5.id], team.users.ids

      # Update workflow output result
      @workflow.wf_output_result = {type: 'Team', ids: [team.id]}
      @workflow.save!

      user5_location_custom_field = @user5.assigned_custom_fields.find_by(value: 'Pune')
      user5_location_custom_field.update_attributes!(value: 'Chennai')
      assert_equal user5_location_custom_field.value, 'Chennai'

      previous_changes = {'location' => ['Pune', 'Chennai'] }

      input_data = @org.users.where(id: @user5.id)
      WorkflowService::Process.new(workflow: @workflow, input_data: input_data, changes: previous_changes).run

      assert_empty team.users.ids
      assert_equal 2, Team.all.size
      team2 = Team.last
      assert_equal 'Chennai', team2.name
      assert_same_elements [@user5.id], team2.users.ids
    end

    test 'dynamic workflow with no rules on attr change' do
      @workflow.wf_rules = nil
      @workflow.save!

      assert_nil @workflow.wf_rules
      assert_equal WorkflowService::Output::CREATE_DYNAMIC, @workflow.wf_action['type']

      # user added to team that will be created by workflow
      team = create :team, name: 'Pune', description: 'Group created from workflow', organization: @org
      team.add_user(@user5)
      assert_same_elements [@user5.id], team.users.ids

      # Update workflow output result
      @workflow.wf_output_result = {type: 'Team', ids: [team.id]}
      @workflow.save!

      user5_location_custom_field = @user5.assigned_custom_fields.find_by(value: 'Pune')
      user5_location_custom_field.update_attributes!(value: 'Chennai')
      assert_equal user5_location_custom_field.value, 'Chennai'

      previous_changes = {'location' => ['Pune', 'Chennai'] }

      input_data = @org.users.where(id: @user5.id)
      WorkflowService::Process.new(workflow: @workflow, input_data: input_data, changes: previous_changes).run

      assert_empty team.users.ids
      assert_equal 2, Team.all.size
      team2 = Team.last
      assert_equal 'Chennai', team2.name
      assert_same_elements [@user5.id], team2.users.ids
    end

    test 'should not remove user from group on custom field change if workflow type is dynamic and changes are not in wf_action' do
      assert_equal WorkflowService::Output::CREATE_DYNAMIC, @workflow.wf_action['type']

      # change workflow action from `location` to `country`
      @workflow.wf_action = {
        entity: 'Team',
        type: 'create_dynamic',
        value: {
          column_name: 'name',
          wf_column_names: ['country'],
          prefix: '',
          suffix: ''
        },
        metadata: {
          is_private: true,
          description: 'Group created from workflow',
          is_mandatory: true
        }
      }
      @workflow.save!
      @workflow = @workflow.reload
      assert_same_elements ['country'], @workflow.wf_action['value']['wf_column_names']


      # user added to team that will be created by workflow i.e. on `country`
      team = create :team, name: 'India', description: 'Group created from workflow', organization: @org
      team.add_user(@user5)
      assert_same_elements [@user5.id], team.users.ids

      # Update workflow output result
      @workflow.wf_output_result = {type: 'Team', ids: [team.id]}
      @workflow.save!

      # user5 changes location, this will not impact his/her country and hence should not be removed from team
      user5_location_custom_field = @user5.assigned_custom_fields.find_by(value: 'Pune')
      user5_location_custom_field.update_attributes!(value: 'Chennai')
      assert_equal user5_location_custom_field.value, 'Chennai'
      previous_changes = {'location' => ['Pune', 'Chennai'] }
      input_data = @org.users.where(id: @user5.id)

      user5_team_user_id = team.teams_users.first.id
      WorkflowService::Process.new(workflow: @workflow, input_data: input_data, changes: previous_changes).run

      assert_same_elements [@user5.id], team.users.ids
      # assert that user is not removed or removed and added back
      assert_equal user5_team_user_id, team.teams_users.first.id
    end

    # CREATE CUSTOM
    test 'user removal for workflow type custom when user passes the rule on attr change' do
      # user was initially in the group created by workflow
      expected_team_name = 'Engg team'
      @workflow.wf_action = {
        entity: 'Team',
        type: 'create_custom',
        value: {
          column_name: 'name',
          custom_column_name: expected_team_name,
        },
        metadata: {
          is_private: true,
          description: 'Group created from workflow',
          is_mandatory: true
        }
      }
      @workflow.save!
      @workflow = @workflow.reload

      assert_equal WorkflowService::Output::CREATE_CUSTOM, @workflow.wf_action['type']

      # user added to team that will be created by workflow i.e. on `country`
      team = create :team, name: expected_team_name, description: 'Group created from workflow', organization: @org
      team.add_user(@user5)
      assert_same_elements [@user5.id], team.users.ids

      # Update workflow output result
      @workflow.wf_output_result = {type: 'Team', ids: [team.id]}
      @workflow.save!

      @workflow = @workflow.reload

      # user5 changes location, this will not impact his/her country and hence should not be removed from team
      user5_location_custom_field = @user5.assigned_custom_fields.find_by(value: 'Pune')
      user5_location_custom_field.update_attributes!(value: 'Chennai')
      assert_equal user5_location_custom_field.value, 'Chennai'
      previous_changes = {'location' => ['Pune', 'Chennai'] }
      input_data = @org.users.where(id: @user5.id)

      user5_team_user_id = team.teams_users.first.id
      WorkflowService::Process.new(workflow: @workflow, input_data: input_data, changes: previous_changes).run

      assert_same_elements [@user5.id], team.users.ids
      # assert that user is not removed or removed and added back
      assert_equal user5_team_user_id, team.teams_users.first.id
    end

    test 'user removal for workflow type custom when user does NOT pass the rule on attr change' do
      # user was initially in the group created by workflow
      expected_team_name = 'Engg team'
      @workflow.wf_action = {
          entity: 'Team',
          type: 'create_custom',
          value: {
              column_name: 'name',
              custom_column_name: expected_team_name,
          },
          metadata: {
              is_private: true,
              description: 'Group created from workflow',
              is_mandatory: true
          }
      }
      @workflow.save!
      @workflow = @workflow.reload

      assert_equal WorkflowService::Output::CREATE_CUSTOM, @workflow.wf_action['type']

      # user added to team that will be created by workflow i.e. on `country`
      team = create :team, name: expected_team_name, description: 'Group created from workflow', organization: @org
      team.add_user(@user5)
      assert_same_elements [@user5.id], team.users.ids

      # Update workflow output result
      @workflow.wf_output_result = {type: 'Team', ids: [team.id]}
      @workflow.save!

      @workflow = @workflow.reload

      # user5 changes location, this will not impact his/her country and hence should not be removed from team
      user5_location_custom_field = @user5.assigned_custom_fields.find_by(value: 'Pune')
      user5_location_custom_field.update_attributes!(value: 'Mumbai')
      assert_equal user5_location_custom_field.value, 'Mumbai'
      previous_changes = {'location' => ['Pune', 'Mumbai'] }
      input_data = @org.users.where(id: @user5.id)

      WorkflowService::Process.new(workflow: @workflow, input_data: input_data, changes: previous_changes).run

      # user removed as it does not satisfy the rule and hence cannot be a part of the team (create_custom)
      assert_empty  team.users.ids
    end

    # EXISTING
    test 'user removal for workflow type existing when user passes the rule on attr change' do
      # existing team
      existing_team = create :team, name: 'An existing group', description: 'Group created from workflow', organization: @org

      # user was initially in the group created by workflow
      @workflow.wf_action = {
        entity: 'Team',
        type: 'existing',
        value: {
          column_name: 'id',
          id: existing_team.id,
        },
        metadata: {
          is_private: true,
          description: 'Group created from workflow',
          is_mandatory: true
        }
      }
      @workflow.save!
      @workflow = @workflow.reload

      assert_equal WorkflowService::Output::EXISTING, @workflow.wf_action['type']

      # user added to existing team
      existing_team.add_user(@user5)
      assert_same_elements [@user5.id], existing_team.users.ids

      # user5 changes location, this will not impact his/her country and hence should not be removed from team
      user5_location_custom_field = @user5.assigned_custom_fields.find_by(value: 'Pune')
      user5_location_custom_field.update_attributes!(value: 'Chennai')
      assert_equal user5_location_custom_field.value, 'Chennai'
      previous_changes = {'location' => ['Pune', 'Chennai'] }
      input_data = @org.users.where(id: @user5.id)

      user5_team_user_id = existing_team.teams_users.first.id
      WorkflowService::Process.new(workflow: @workflow, input_data: input_data, changes: previous_changes).run

      assert_same_elements [@user5.id], existing_team.users.ids
      # assert that user is not removed or removed and added back
      assert_equal user5_team_user_id, existing_team.teams_users.first.id
    end

    test 'user removal for workflow type existing when user does NOT pass the rule on attr change' do
      # existing team
      existing_team = create :team, name: 'An existing group', description: 'Group created from workflow', organization: @org

      # user was initially in the group created by workflow
      @workflow.wf_action = {
        entity: 'Team',
        type: 'existing',
        value: {
          column_name: 'id',
          id: existing_team.id,
        },
        metadata: {
          is_private: true,
          description: 'Group created from workflow',
          is_mandatory: true
        }
      }
      @workflow.save!
      @workflow = @workflow.reload

      assert_equal WorkflowService::Output::EXISTING, @workflow.wf_action['type']

      # user added to existing team
      existing_team.add_user(@user5)
      assert_same_elements [@user5.id], existing_team.users.ids

      # user5 changes location, this will not impact his/her country and hence should not be removed from team
      user5_location_custom_field = @user5.assigned_custom_fields.find_by(value: 'Pune')
      user5_location_custom_field.update_attributes!(value: 'Mumbai')
      assert_equal user5_location_custom_field.value, 'Mumbai'
      previous_changes = {'location' => ['Pune', 'Mumbai'] }
      input_data = @org.users.where(id: @user5.id)

      WorkflowService::Process.new(workflow: @workflow, input_data: input_data, changes: previous_changes).run

      # user removed as it does not satisfy the rule and hence cannot be a part of the team (create_existing)
      assert_empty  existing_team.users.ids
    end
  end
end
require 'test_helper'

module WorkflowService
  class ObserverTest < ActiveSupport::TestCase
    include ActiveJob::TestHelper
    setup do
      @org = create(:organization)
      @creator = create :user, organization: @org
      @workflow1 = create(
        :workflow,
        name: 'wf1',
        user_id: @creator.id,
        organization: @org,
        is_enabled: false,
        wf_action: {
          entity: 'Team',
          type: 'create_dynamic',
          values: [
            {
              column_name: 'name',
              wf_column_name: 'location',
              prefix: '',
              suffix: ''
            }
          ],
          metadata: {
            is_private: true,
            description: 'Group created from workflow',
            is_mandatory: true
          }
        }
      )

      @workflow2 = create(
        :workflow,
        name: 'wf2',
        user_id: @creator.id,
        organization: @org,
        is_enabled: true,
        wf_actionable_columns: [
          {
            column_name: 'location', is_identifier: false, csv_name: nil, type: 'string', is_dynamic: true, formula: nil
          },
          {
            column_name: 'first_name', is_identifier: false, csv_name: nil, type: 'string', is_dynamic: true, formula: nil
          },
          {
            column_name: 'last_name', is_identifier: false, csv_name: nil, type: 'string', is_dynamic: true, formula: nil
          },
          {
            column_name: 'status', is_identifier: false, csv_name: nil, type: 'string', is_dynamic: true, formula: nil
          }
        ],
        wf_action: {
          entity: 'Team',
          type: 'create_dynamic',
          values: [
            {
              column_name: 'name',
              wf_column_name: 'location',
              prefix: '',
              suffix: ''
            }
          ],
          metadata: {
            is_private: true,
            description: 'Group created from workflow',
            is_mandatory: true
          }
        }
      )
      stub_request(:get, 'http://breelio.com/wp-content/uploads/2014/06/sample1.jpg').with(
        headers: {
          'Accept' => '*/*',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'User-Agent' => 'Ruby'
        }
      ).to_return(status: 200, body: '', headers: {})
    end

    teardown do
    end

    test 'should trigger UserBulkUploadWorkflowJob to process workflows on user bulk upload v2' do
      # UserBulkUploadWorkflowJob.unstub(:perform_later)
      # EdcastActiveJob.unstub(:perform_later)
      # user_csv = File.open(Rails.root.join('test', 'fixtures', 'files', 'users_with_custom_fields.csv')).read
      # sender = create :user, organization: @org
      # invitation_file = create :invitation_file, sender: sender, invitable: @org, data: user_csv, roles: 'member', created_at: (Time.now - 3.weeks)
    end

    test 'should trigger only eligible workflows on user record change' do

    end
  end
end
require 'test_helper'

module WorkflowService
  class OutputTest < ActiveSupport::TestCase
    setup do
      @org = create(:organization)
      @creator = create :user, organization: @org
    end

    test 'get unique identifier if output is `create_dynamic`' do
      workflow = create(:workflow, user_id: @creator.id, organization: @org)
      unique_identifier = WorkflowService::Output.new(workflow: workflow).unique_identifier
      assert_instance_of Array, unique_identifier
      assert_equal 'location', unique_identifier[0]
    end

    test 'should create team as output with suffix' do
      workflow = create(:workflow, user_id: @creator.id, organization: @org)
      workflow.wf_action['value']['suffix'] = 'this is a suffix'
      workflow.save

      # country is India, first_name is not null, location is not Mumbai => selected
      user5 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      custom_field_location = create :custom_field, organization: @org, abbreviation: 'location', display_name: 'location'
      custom_field_country = create :custom_field, organization: @org, abbreviation: 'country', display_name: 'country'
      create :user_custom_field, user: user5, custom_field: custom_field_location, value: 'Pune'
      create :user_custom_field, user: user5, custom_field: custom_field_country, value: 'India'

      user4 = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      create :user_custom_field, user: user4, custom_field: custom_field_location, value: 'Thane'
      create :user_custom_field, user: user4, custom_field: custom_field_country, value: 'India'

      WorkflowService::Process.new(workflow: workflow).run
      assert_equal 2, Team.all.length
      assert_equal 'Pune this is a suffix', Team.first.name
      assert_same_elements Team.all.map(&:id), workflow.wf_output_result['ids']
    end

    test 'should create add users into existing team' do
      workflow = create(:workflow, user_id: @creator.id, organization: @org)
      existing_team = create :team, organization: @org, name: 'Existing team', description: 'An existing team'
      workflow.wf_action = {entity: 'Team', type: 'existing', value: {column_name: 'id', id: existing_team.id}}
      workflow.save!
      user = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      custom_field_location = create :custom_field, organization: @org, abbreviation: 'location', display_name: 'location'
      custom_field_country = create :custom_field, organization: @org, abbreviation: 'country', display_name: 'country'
      create :user_custom_field, user: user, custom_field: custom_field_location, value: 'Pune'
      create :user_custom_field, user: user, custom_field: custom_field_country, value: 'India'

      WorkflowService::Process.new(workflow: workflow).run
      assert_equal 1, existing_team.users.length
      assert_equal user.id, existing_team.users.first.id
    end

    test 'after output is created then initiate WorkflowAfterProcessJob' do
      EdcastActiveJob.unstub(:perform_later)
      WorkflowAfterProcessJob.unstub(:perform_later)
      WorkflowAfterProcessJob.expects(:perform_later).once
      workflow = create(:workflow, user_id: @creator.id, organization: @org)
      user = create :user, organization: @org, is_active: true, is_suspended: false, status: 'active', is_anonymized: false
      custom_field_location = create :custom_field, organization: @org, abbreviation: 'location', display_name: 'location'
      create :user_custom_field, user: user, custom_field: custom_field_location, value: 'Pune'
      custom_field_country = create :custom_field, organization: @org, abbreviation: 'country', display_name: 'country'
      create :user_custom_field, user: user, custom_field: custom_field_country, value: 'India'
      process_service = WorkflowService::Process.new(workflow: workflow)
      process_service.run
      assert_same_elements Team.ids, process_service.instance_variable_get(:@output_service).output_ids
    end
  end
end
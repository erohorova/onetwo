require 'test_helper'

class TopicUpdateServiceTest < ActiveSupport::TestCase

  setup do
    @learning_topics = {
                          "topic_name" => 'technology.software_development.c#',
                          "name" => 'technology.software_development.c#',
                          "topic_id" =>  SecureRandom.hex,
                          "topic_label" =>  'c#',
                          "domain_name" =>  'Technology',
                          "domain_id" =>  SecureRandom.hex,
                          "domain_label" =>  'technology'
                        }
    @taxonomy_topic = {
        "labs.technology.software_development.c#" => {
            "name" => "labs.technology.software_development.c#",
            "domain_name" => "labs.Technology",
            "taxo_id" => "5017549307456121674",
            "topic_id" => "4561360800805781504",
            "domain_id" => "4561360797140012203"

        }
    }
    @topic_response =  {
        "topics" => [
            {
                "description"=> nil,
                "domain"=> {
                    "id"=> "4561360797140012203",
                    "label"=> "technology",
                    "name"=> "labs.Technology"
                },
                "id"=> "4561360800805781504",
                "image_url"=> "https://s3.amazonaws.com/edc-dev-web/assets/computer_icon.png",
                "label"=> "c #",
                "name"=> "labs.technology.software_development.c#",
                "taxo_id"=> "5017549307456121674"
            }]}

  end

  test 'add org prefix to user topic' do

    # Execute
    learning_topics = TopicUpdateService.new("user_profile").convert_topics(@taxonomy_topic, @learning_topics, "labs", "prepend", "edcast")
    assert_equal @topic_response["topics"][0]["name"], learning_topics["topic_name"]
    assert_equal @topic_response["topics"][0]["domain"]["name"], learning_topics["domain_name"]
  end

  test 'add org prefix to  card topic' do

    # Execute
    learning_topics = TopicUpdateService.new("card").convert_topics(@taxonomy_topic, @learning_topics["topic_name"], "labs", "prepend", "edcast")
    assert_equal @topic_response["topics"][0]["name"], learning_topics
  end

  test 'add org prefix to channel topic' do

    # Execute
    learning_topics = TopicUpdateService.new("channel").convert_topics(@taxonomy_topic, @learning_topics, "labs", "prepend", "edcast")
    assert_equal @topic_response["topics"][0]["name"], learning_topics["name"]
  end

  test 'topic update should not update if prefix exists' do
    @learning_topics["topic_name"] = "labs.technology.software_development.c#"

    # Execute
    learning_topics = TopicUpdateService.new("user_profile").convert_topics(@taxonomy_topic, @learning_topics, "labs", "prepend", "edcast")
    assert_equal @topic_response["topics"][0]["name"], learning_topics["topic_name"]

  end

  test 'replace root node with org prefix' do
    @learning_topics["topic_name"] = "edcast.technology.software_development.c#"
    @topic_response["topics"][0]["domain"]["name"] = "labs.technology.software_development.c#"

    # Execute
    learning_topics = TopicUpdateService.new("user_profile").convert_topics(@taxonomy_topic, @learning_topics, "labs", "replace", "edcast")
    assert_equal @topic_response["topics"][0]["name"], learning_topics["topic_name"]
  end

end

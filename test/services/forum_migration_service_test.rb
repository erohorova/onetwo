require 'test_helper'

class ForumMigrationServiceTest < ActiveSupport::TestCase
  def setup_data_for_org(org)
    group_set11, group_set12, group_set13 = create_list(:resource_group, 3, organization: org, client: create(:client, organization: org))
    [@user1, @user2, @user3].each do |user|
      [group_set11, group_set12, group_set13].each do |grp|
        grp.add_member user
      end
    end
    # post [postable_id, postable_type=Group]
    # comment [commentable_id, commentable_type=Post]
    # approval [approvable_id, approvable_type=Post,Comment]
    # vote [votable_id, votable_type=Post,Comment]
    # stream [item_id, item_type=Post,Comment, Group]
    # group_user [user_id]
    # follow [followable_id, followable_type=Post]
    # report [reportable_id, reportable_id=Post,Comment]
    # notification [notifiable_id, notifiable_type=Post,Comment]
    # file_resource [attachable_id, attachable_type=Post,Comment]
    # activity_stream [streamable_id, streamable_type=Comment]
    # score [user_id]
    # event [user_id]
    # event_user [user_id]
    # group [creator_id]

    # post
    posts111 = create(:post, postable: group_set11, user_id: @user1.id)
    posts122 = create(:post, postable: group_set12, user_id: @user2.id)
    posts133 = create(:post, postable: group_set13, user_id: @user3.id)

    # comment
    comment1 = create(:comment, commentable: posts111, user: @user2)
    comment2 = create(:comment, commentable: posts122, user: @user1)

    # approval
    approval1 = create(:approval, approvable: posts111, user_id: @user3.id)
    approval2 = create(:approval, approvable: comment1, user_id: @user1.id)

    #vote
    post_upvotes = create(:vote, votable: posts111, user_id: @user3.id)
    comment_upvotes = create(:vote, votable: comment2, user_id: @user3.id)

    #stream
    stream1 = create(:stream, item: posts111, user_id: @user3.id)
    stream2 = create(:stream, item: comment1, user_id: @user1.id)
    stream3 = create(:stream, item: group_set11, user_id: @user1.id)

    # group_user
    groups_users = GroupsUser.joins(:group).where("groups.organization_id= #{org.id}")

    #follow
    follow = create(:follow, followable: posts111, user_id: @user3.id)

    #report
    report1 = create(:report, reportable: posts111, user_id: @user3.id)
    report2 = create(:report, reportable: comment1, user_id: @user1.id)

    #notification
    notification1 = create(:notification, notifiable: posts111, user_id: @user3.id)
    notification2 = create(:notification, notifiable: comment1, user_id: @user1.id)

    # file_resource
    fr1 = create(:file_resource, attachable: posts111, user_id: @user1.id)
    fr2 = create(:file_resource, attachable: comment1, user_id: @user2.id)

    # activity_stream

    # score
    scores =  Score.joins(:group).where("organization_id = #{org.id}")

    #event
    event1 = create(:event, group: group_set11, user_id: @user1.id)
    event1 = create(:event, group: group_set11, user_id: @user2.id)

    # event_user
    events_users = EventsUser.joins(event: :group).where("groups.organization_id = #{org.id}")

    # private_group
    private_group1 = create(:private_group, parent_id: group_set11.id, creator_id: @user3.id, organization_id: org.id)
    private_group2 = create(:private_group, parent_id: group_set12.id, creator_id: @user1.id, organization_id: org.id)
    pref1 = create(:user_preference, preferenceable: group_set11, user_id: @user1.id, key: 'notification.post.opt_out', value: true)
    pref2 = create(:user_preference, preferenceable: group_set12, user_id: @user2.id, key: 'notification.post.opt_out', value: true)

    event1 = create(:event, user_id: @user1.id, group_id: group_set11.id)
    event2 = create(:event, user_id: @user2.id, group_id: group_set12.id)

    {
      posts: [posts111, posts122, posts133],
      comments: [comment1, comment2],
      votes: [post_upvotes, comment_upvotes],
      approvals: [approval1, approval2],
      streams: [stream1, stream2, stream3],
      groups_users: groups_users,
      follows: [follow],
      reports: [report1, report2],
      notifications: [notification1, notification2],
      preferences: [pref1, pref2],
      events: [event1, event2],
      events_users: events_users,
      private_groups: [private_group1, private_group2],
      file_resources: [fr1, fr2],
      scores: scores
    }
  end
  setup do
    skip #UNCOMMENT THIS AFTER CONSULATION WITH SAVANNAH TEAM
    create_default_org
    Approval.any_instance.stubs(:check_approval_rights).returns(true)
    Event.any_instance.stubs(:user_authorized).returns(true)
    @default_org = Organization.default_org
    @user1, @user2, @user3 = create_list(:user, 3, organization: @default_org)
    @org1, @org2, @org3 = create_list(:organization, 3)
  end

  test 'forum data should update www_user to org user' do
    org1_data = setup_data_for_org(@org1)
    org2_data = setup_data_for_org(@org2)
    assert_equal [@default_org], UserPreference.all.map(&:user).map(&:organization).uniq
    updater = ForumMigrationService.new
    # @orgs1 = Organization.where(id: @org1.id)
    # @orgs2 = Organization.where(id: @org2.id)

    #####################BEFORE MIGRATION#####################
    before_migration_assert(org1_data)
    before_migration_assert(org2_data)
    #####################  Migration Started #####################



    run_migration("PrivateGroup")

    run_migration("GroupsUser")

    run_migration("Score")

    run_migration("Event")

    run_migration("EventsUser")

    run_migration("Vote")

    run_migration("Approval")

    run_migration("Stream")

    run_migration("Follow")

    run_migration("Report")

    run_migration("Notification")

    run_migration("FileResource")

    run_migration("ActivityStream")

    run_migration("Comment")

    run_migration("Post")

    run_migration("UserPreference")

    updater.run
    #####################  Migration Finished #####################
    after_migration_assert(org1_data, @org1)
    after_migration_assert(org2_data, @org2)

    ################## User preferences ###################
    assert_not_equal [@default_org], UserPreference.all.map(&:user).map(&:organization).uniq
    assert_equal [@org1, @org2], UserPreference.all.map(&:user).map(&:organization).uniq
  end

  def run_migration(model)
    updater = ForumMigrationService.new
    updater.backfill_forum_user_mappings(model)
  end

  def before_migration_assert(org_data)
    #----Posts
    posts_org = org_data[:posts].map(&:user).map(&:organization).uniq
    assert_equal [@default_org], posts_org

    #----Comments
    comments_org = org_data[:comments].flatten.map(&:user).map(&:organization).uniq
    assert_equal [@default_org], comments_org

    #----Votes
    voters_org = org_data[:votes].flatten.map(&:voter).map(&:organization).uniq
    assert_equal [@default_org], voters_org

    #----Approvals
    assert_equal [@default_org], org_data[:approvals].map(&:approver).map(&:organization).uniq

    #----Streams
    assert_equal [@default_org], org_data[:streams].map(&:user).map(&:organization).uniq

    #----GroupsUsers
    assert_equal [@default_org], org_data[:groups_users].map(&:user).map(&:organization).uniq

    #----Score
    assert_equal [@default_org], org_data[:scores].map(&:user).map(&:organization).uniq

    #----FileResources
    assert_equal [@default_org], org_data[:file_resources].map(&:user).map(&:organization).uniq

    #----Follows
    assert_equal [@default_org], org_data[:follows].map(&:user).map(&:organization).uniq

    #----Reports
    assert_equal [@default_org], org_data[:reports].map(&:reporter).map(&:organization).uniq

    #----notifications
    assert_equal [@default_org], org_data[:notifications].map(&:user).map(&:organization).uniq

    #----Events
    assert_equal [@default_org], org_data[:events].map(&:user).map(&:organization).uniq

    #----EventUsers
    assert_equal [@default_org], org_data[:events_users].map(&:user).map(&:organization).uniq

    #----privategroups
    assert_equal [@default_org], org_data[:private_groups].map(&:creator).map(&:organization).uniq
  end

  def after_migration_assert(org_data, org)

    posts = org_data[:posts].map(&:reload)
    assert_equal [org], posts.map(&:user).map(&:organization).uniq

    #----Comments
    comments = org_data[:comments].map(&:reload)
    assert_equal [org], comments.map(&:user).map(&:organization).uniq

    #----Likes
    votes = org_data[:votes].map(&:reload)
    assert_equal [org], votes.map(&:voter).map(&:organization).uniq

    #----Approvals
    approvals = org_data[:approvals].map(&:reload)
    assert_equal [org], approvals.map(&:approver).map(&:organization).uniq

    #----Streams
    streams = org_data[:streams].map(&:reload)
    assert_equal [org], streams.map(&:user).map(&:organization).uniq

    #----GroupsUsers
    assert_equal [org], org_data[:groups_users].map(&:reload).map(&:user).map(&:organization).uniq

    #----Score
    assert_equal [org], org_data[:scores].map(&:reload).map(&:user).map(&:organization).uniq

    #----FileResources
    assert_equal [org], org_data[:file_resources].map(&:reload).map(&:user).map(&:organization).uniq

    #----Follow
    follows = org_data[:follows].map(&:reload)
    assert_equal [org], follows.map(&:user).map(&:organization).uniq

    #----Reports
    reports = org_data[:reports].map(&:reload)
    assert_equal [org], reports.map(&:reporter).map(&:organization).uniq

    #----Notifications
    notifications = org_data[:notifications].map(&:reload)
    assert_equal [org], notifications.map(&:user).map(&:organization).uniq

    #----Event
    events = org_data[:events].map(&:reload)
    assert_equal [org], events.map(&:user).map(&:organization).uniq

    assert_equal [org], org_data[:events_users].map(&:reload).map(&:user).map(&:organization).uniq

    #----PrivateGroups
    assert_equal [org], org_data[:private_groups].map(&:reload).map(&:creator).map(&:organization).uniq

  end
end
require 'test_helper'

class CardImageCleanupServiceTest < ActiveSupport::TestCase
    setup do
      WebMock.enable!
      Card.any_instance.stubs(:reindex_async).returns(true)
      @image_url_expired = 'http://abc.xyz.com/expired-url'
      @image_url_active = 'http://abc.xyz.com/active-url'
    end
    teardown do
      WebMock.disable!
    end

  test '#check_and_update_card_images must update filestack object of card if image expired' do
    stub_request(:get, @image_url_expired).to_return(status: 404)

    filestack = [
                 {
                    "filename": "Strix_nebulosaRB.jpg",
                    "mimetype": "image/jpeg",
                    "size": 106176,
                    "source": "local_file_system",
                    "url": @image_url_expired,
                    "handle": "abcdefgh",
                    "status": "Stored",
                    "key": "VYgpc6fnTOKJkFWIkzti_Strix_nebulosaRB.jpg",
                    "container": "your_bucket"
                 }.with_indifferent_access
              ]
    card = create(:card, filestack: filestack)
    CardImageCleanupService.new(card: card).check_and_update_card_images
    assert_not_equal @image_url_expired, card.reload.filestack[0][:url]
  end

  test '#check_and_update_card_images must not update filestack object of card if image is active' do
    stub_request(:get, @image_url_active).to_return(status: 200)

    filestack = [
                 {
                    "filename": "Strix_nebulosaRB.jpg",
                    "mimetype": "image/jpeg",
                    "size": 106176,
                    "source": "local_file_system",
                    "url": @image_url_active,
                    "handle": "abcdefgh",
                    "status": "Stored",
                    "key": "VYgpc6fnTOKJkFWIkzti_Strix_nebulosaRB.jpg",
                    "container": "your_bucket"
                 }.with_indifferent_access
                ]
    card = create(:card, filestack: filestack)
    CardImageCleanupService.new(card: card).check_and_update_card_images
    assert_equal @image_url_active, card.reload.filestack[0][:url]
  end

  test '#update_filestack_image_if_expired should not update filestack if it is not an image' do
    filestack = [
                 {
                    "filename": "Strix_nebulosaRB.zip",
                    "mimetype": "application/zip",
                    "size": 106176,
                    "source": "local_file_system",
                    "url": 'http://abc.xyz.com/cdn-zip-file',
                    "handle": "abcdefgh",
                    "status": "Stored",
                    "key": "VYgpc6fnTOKJkFWIkzti_Strix_nebulosaRB.zip",
                    "container": "your_bucket"
                 }.with_indifferent_access
                ]
    card = create(:card, filestack: filestack)
    CardImageCleanupService.new(card: card).check_and_update_card_images
    assert_equal 'http://abc.xyz.com/cdn-zip-file', card.reload.filestack[0][:url]
  end

  test '#check_and_update_card_images must call reindex_async if expired image was updated for filestack' do
    Card.any_instance.unstub(:reindex_async)
    stub_request(:get, @image_url_expired).to_return(status: 404)

    filestack = [
                 {
                    "filename": "Strix_nebulosaRB.jpg",
                    "mimetype": "image/jpeg",
                    "size": 106176,
                    "source": "local_file_system",
                    "url": @image_url_expired,
                    "handle": "abcdefgh",
                    "status": "Stored",
                    "key": "VYgpc6fnTOKJkFWIkzti_Strix_nebulosaRB.jpg",
                    "container": "your_bucket"
                 }.with_indifferent_access
              ]
    card = create(:card, filestack: filestack)
    card.expects(:reindex_async).once
    CardImageCleanupService.new(card: card).check_and_update_card_images
  end

  test '#check_and_update_card_images must call reindex_async only once if expired image was updated for filestack and resource' do
    Card.any_instance.unstub(:reindex_async)
    stub_request(:get, @image_url_expired).to_return(status: 404)

    filestack = [
                 {
                    "filename": "Strix_nebulosaRB.jpg",
                    "mimetype": "image/jpeg",
                    "size": 106176,
                    "source": "local_file_system",
                    "url": @image_url_expired,
                    "handle": "abcdefgh",
                    "status": "Stored",
                    "key": "VYgpc6fnTOKJkFWIkzti_Strix_nebulosaRB.jpg",
                    "container": "your_bucket"
                 }.with_indifferent_access
              ]
    resource = create(:resource, image_url: @image_url_expired)
    card = create(:card, filestack: filestack, resource: resource)
    card.expects(:reindex_async).once
    CardImageCleanupService.new(card: card).check_and_update_card_images
  end

  test '#check_and_update_card_images should update expired image_url of resource with an image from the array of stock images' do
    stub_request(:get, @image_url_expired).to_return(status: 404)
    resource = create(:resource, image_url: @image_url_expired)
    card = create(:card, resource: resource)
    CardImageCleanupService.new(card: card).check_and_update_card_images

    assert_not_equal @image_url_expired, resource.image_url
  end

  test '#check_and_update_card_images should not update active image_url of resource' do
    stub_request(:get, @image_url_active).to_return(status: 200)
    resource = create(:resource, image_url: @image_url_active)
    card = create(:card, resource: resource)
    CardImageCleanupService.new(card: card).check_and_update_card_images

    assert_equal @image_url_active, resource.image_url
  end

  test '#check_and_update_card_images should not update image_url of resource if url is nil' do
    resource = create(:resource, image_url: nil)
    card = create(:card, resource: resource)
    CardImageCleanupService.new(card: card).check_and_update_card_images

    assert_nil resource.image_url
  end

  test 'url_expired? must return true if url gives 404' do
    stub_request(:get, @image_url_expired).to_return(status: 404)
    assert CardImageCleanupService.new(card: nil).url_expired?(@image_url_expired)
  end

  test 'url_expired? must return false if url gives 200' do
    stub_request(:get, @image_url_active).to_return(status: 200)
    refute CardImageCleanupService.new(card: nil).url_expired?(@image_url_active)
  end

  test 'url_expired? must return false if url is nil' do
    refute CardImageCleanupService.new(card: nil).url_expired?(nil)
  end

end

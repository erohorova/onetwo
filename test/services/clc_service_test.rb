require 'test_helper'

class ClcServiceTest < ActiveSupport::TestCase
  setup do
    @org1 = create(:organization)
    @org2 = create(:organization)

    @channel1, @channel2, @channel3 = create_list(:channel, 3, organization: @org1)
    @team1, @team2, @team3 = create_list(:team, 3, organization: @org1)
    @user1= create(:user, organization: @org1)

    @user1.channels << [@channel1, @channel2, @channel3]
    [@team1, @team2, @team3].each {|team| team.add_member @user1}

    @org1_clc = create(:clc, name: 'test clc', entity: @org1, organization: @org1)
    [@channel1, @channel2, @team1, @team2].each do |entity|
      create(:clc, name: 'test clc', entity: entity, organization: @org1)
    end

    @author = create(:user, organization: @org1)
    @author2 = create(:user, organization: @org2)
    ecl_metadata = { "duration_metadata":
                      { "calculated_duration":300,
                        "calculated_duration_display":"5 minutes"
                      }
                    }
    @card = create(:card, author: @author, organization: @org1, ecl_metadata: ecl_metadata)
    @card1 = create(:card, author: @author, organization: @org1)
    @card2 = create(:card, author: @author2, organization: @org2, ecl_metadata: ecl_metadata)
    @service = ClcService.new
  end

  test "should not populate anything if duration is not present" do
    @service.expects(:poulate_clc_records).never
    ClcService.new.initiate_clc_records(@card1, @user1)
  end

  test "populate clc record for organization enabled for CLC" do
    assert_difference -> {ClcOrganizationsRecord.count} do
      ClcService.new.initiate_clc_records(@card, @user1)
      assert_equal 1, ClcOrganizationsRecord.where(organization_id: @org1.id).count
    end
  end

  test "should not populate clc record for non clc organization" do
    @service.expects(:populate).with(@org2, @author2, @card2).never
    @service.initiate_clc_records(@card2, @author2)
  end

  # Commenting this becasuse on prod we are not using Channel clc and team clc.
  # it is taking to much of time in background jobs
  # test "populate clc record for channels enabled for CLC and also part of card channels" do
  #   @card.channels << @channel1
  #   assert_difference -> {ClcChannelsRecord.count}, 1 do
  #     ClcService.new.initiate_clc_records(@card, @user1)
  #     assert_equal 1, ClcChannelsRecord.where(channel_id: @channel1.id).count
  #   end
  # end

  test "should not populate clc record for channels enabled for CLC but card is not part of channel" do
    @service.expects(:populate).with(@org1, @user1, @card).once
    @service.expects(:populate).with(@channel2, @user1, @card).never
    @service.initiate_clc_records(@card, @user1)
  end

  test "should not populate clc record if clc is not created for channel" do
    @card.channels << @channel3
    @service.initiate_clc_records(@card, @user1)
    assert_equal 0, ClcChannelsRecord.where(channel_id: @channel3.id).count
  end

  # test "populate clc record for teams enabled for CLC and card assign to those teams" do
  #   assignment = create(:assignment, assignable: @card, assignee: @author)
  #   create(:team_assignment, team: @team1, assignment: assignment, assignor: @author)

  #   assert_difference -> {ClcTeamsRecord.count}, 1 do
  #     @service.initiate_clc_records(@card, @user1)
  #     assert_equal 1, ClcTeamsRecord.where(team_id: @team1.id).count
  #   end
  # end

  test "should not populate clc record for teams enabled for CLC but card is not assigned to that team" do
    @service.expects(:populate).with(@org1, @user1, @card).once
    @service.expects(:populate).with(@team2, @user1, @card).never
    @service.initiate_clc_records(@card, @user1)
  end

  test "clc should not populate if CLC is not created for team" do
    @service.expects(:populate).with(@org1, @user1, @card).once
    @service.expects(:populate).with(@team3, @user1, @card).never

    assignment = create(:assignment, assignable: @card, assignee: @author)
    create(:team_assignment, team: @team3, assignment: assignment, assignor: @author)

    @service.initiate_clc_records(@card, @user1)
  end


  #compute_progress_of_user
  test "should compute clc_progress for each user per clc based on clc_records one at a time" do
    users = create_list(:user, 5, organization: @org1)

    #clc record for each user
    users.each_with_index do |user, index|
      create(:clc_organizations_record, user_id: user.id, organization_id: @org1.id, score: (5 + index), card_id: @card.id)
    end

    #more clc record for users[0]
    create(:clc_organizations_record, user_id: users[0].id, organization_id: @org1.id, score: 5, card_id: @card1.id)

    users.each do |user|
      @service.compute_progress_of_user(@org1_clc,user)
    end

    assert_equal 5, ClcProgress.count
    assert_equal 10, ClcProgress.find_by(user_id: users[0].id, clc_id: @org1_clc.id).clc_score
    users[1..5].each_with_index do |user, index|
      assert_equal (5 + (index + 1)), ClcProgress.find_by(user_id: user.id, clc_id: @org1_clc.id).clc_score
    end
  end

  test "should not create mutliple record for user for same clc when using #compute_progress_of_user" do
    user = create(:user, organization: @org1)
    cards = create_list(:card, 2, organization: @org1, author: @user1)
    create(:clc_organizations_record, user_id: @user1.id, organization_id: @org1.id, score: 5, card_id: @card.id)
    create(:clc_organizations_record, user_id: @user1.id, organization_id: @org1.id, score: 5, card_id: @card2.id)

    @service.compute_progress_of_user(@org1_clc, @user1)
    clc_progresses = ClcProgress.where(user_id: @user1.id, clc_id: @org1_clc.id)
    assert_equal 1, clc_progresses.length
    assert_equal 10, clc_progresses[0].clc_score
  end

  test "should update existing clc_progress for same user when using #compute_progress_of_user" do
    cards = create_list(:card, 2, organization: @org1, author: @user1)
    create(:clc_organizations_record, user_id: @user1.id, organization_id: @org1.id, score: 5, card_id: @card1.id)

    @service.compute_progress_of_user(@org1_clc,@user1)
    clc_progresses = ClcProgress.where(user_id: @user1.id, clc_id: @org1_clc.id)
    assert_equal 1, clc_progresses.length
    assert_equal 5, clc_progresses[0].clc_score

    create(:clc_organizations_record, user_id: @user1.id, organization_id: @org1.id, score: 5, card_id: cards[0].id)
    @service.compute_progress_of_user(@org1_clc,@user1)
    clc_progresses1 = ClcProgress.where(user_id: @user1.id, clc_id: @org1_clc.id)
    assert_equal 1, clc_progresses1.length
    assert_equal 10, clc_progresses1[0].clc_score
  end

  test "should sum only the records between clc time frame when using #compute_progress_of_user" do
    user = create(:user, organization: @org1)
    from_date = Date.today
    org_clc2 = create(:clc, name: 'test clc', entity: @org1, organization: @org1,from_date: from_date,to_date: (from_date + 1.months))
    cards = create_list(:card, 5, organization: @org1, author: @user1)

    #invlaid CLC record
    create(:clc_organizations_record, user_id: user.id, organization_id: @org1.id, score: 5, card_id: cards[0].id, created_at: from_date - 1.days)
    create(:clc_organizations_record, user_id: user.id, organization_id: @org1.id, score: 5, card_id: cards[1].id, created_at: from_date - 2.days)
    create(:clc_organizations_record, user_id: user.id, organization_id: @org1.id, score: 5, card_id: cards[2].id, created_at: from_date + (1.months + 1.day))

    #valid clc records
    create(:clc_organizations_record, user_id: user.id, organization_id: @org1.id, score: 5, card_id: cards[3].id,created_at: from_date + 1.day)
    create(:clc_organizations_record, user_id: user.id, organization_id: @org1.id, score: 5, card_id: cards[4].id,created_at: from_date + 1.month)


    @service.compute_progress_of_user(org_clc2, user)
    clc_progresses = ClcProgress.where(user_id: user.id, clc_id: org_clc2.id)
    assert_equal 1, clc_progresses.length
    assert_equal 10, clc_progresses[0].clc_score
  end

  #compute_progress
  test "should compute clc_progress for each user per clc based on clc_records for org clc" do
    users = create_list(:user, 5, organization: @org1)

    #clc record for each user
    users.each_with_index do |user, index|
      create(:clc_organizations_record, user_id: user.id, organization_id: @org1.id, score: (5 + index), card_id: @card.id)
    end

    #more clc record for users[0]
    create(:clc_organizations_record, user_id: users[0].id, organization_id: @org1.id, score: 5, card_id: @card1.id)

    assert_difference -> {ClcProgress.count}, 5 do
      @service.compute_progress(@org1_clc)
    end

    assert_equal 10, ClcProgress.find_by(user_id: users[0].id, clc_id: @org1_clc.id).clc_score
    users[1..5].each_with_index do |user, index|
      assert_equal (5 + (index + 1)), ClcProgress.find_by(user_id: user.id, clc_id: @org1_clc.id).clc_score
    end
  end

  test "it should not create mutliple record for user for same clc" do
    user = create(:user, organization: @org1)
    cards = create_list(:card, 2, organization: @org1, author: @user1)
    create(:clc_organizations_record, user_id: user.id, organization_id: @org1.id, score: 5, card_id: @card1.id)
    create(:clc_organizations_record, user_id: user.id, organization_id: @org1.id, score: 5, card_id: cards[0].id)
    create(:clc_organizations_record, user_id: user.id, organization_id: @org1.id, score: 5, card_id: cards[1].id)

    @service.compute_progress(@org1_clc)
    clc_progresses = ClcProgress.where(user_id: user.id, clc_id: @org1_clc.id)
    assert_equal 1, clc_progresses.length
    assert_equal 15, clc_progresses[0].clc_score
  end

  test "should update existing clc_progress for same user" do
    user = create(:user, organization: @org1)
    cards = create_list(:card, 2, organization: @org1, author: @user1)
    create(:clc_organizations_record, user_id: user.id, organization_id: @org1.id, score: 5, card_id: @card1.id)

    @service.compute_progress(@org1_clc)
    clc_progresses = ClcProgress.where(user_id: user.id, clc_id: @org1_clc.id)
    assert_equal 1, clc_progresses.length
    assert_equal 5, clc_progresses[0].clc_score

    create(:clc_organizations_record, user_id: user.id, organization_id: @org1.id, score: 5, card_id: cards[0].id)
    @service.compute_progress(@org1_clc)
    clc_progresses1 = ClcProgress.where(user_id: user.id, clc_id: @org1_clc.id)
    assert_equal 1, clc_progresses1.length
    assert_equal 10, clc_progresses1[0].clc_score
  end

  test "should sum only the records between clc time frame" do
    user = create(:user, organization: @org1)
    from_date = Date.today
    org_clc2 = create(:clc, name: 'test clc', entity: @org1, organization: @org1,from_date: from_date,to_date: (from_date + 1.months))
    cards = create_list(:card, 5, organization: @org1, author: @user1)

    #invlaid CLC record
    create(:clc_organizations_record, user_id: user.id, organization_id: @org1.id, score: 5, card_id: cards[0].id, created_at: from_date - 1.days)
    create(:clc_organizations_record, user_id: user.id, organization_id: @org1.id, score: 5, card_id: cards[1].id, created_at: from_date - 2.days)
    create(:clc_organizations_record, user_id: user.id, organization_id: @org1.id, score: 5, card_id: cards[2].id, created_at: from_date + (1.months + 1.day))

    #valid clc records
    create(:clc_organizations_record, user_id: user.id, organization_id: @org1.id, score: 5, card_id: cards[3].id,created_at: from_date + 1.day)
    create(:clc_organizations_record, user_id: user.id, organization_id: @org1.id, score: 5, card_id: cards[4].id,created_at: from_date + 1.month)


    @service.compute_progress(org_clc2)
    clc_progresses = ClcProgress.where(user_id: user.id, clc_id: org_clc2.id)
    assert_equal 1, clc_progresses.length
    assert_equal 10, clc_progresses[0].clc_score
  end

  test "should call #update_clc_score_of_user" do
    @service.update_clc_score_of_user(@card.id, @user1.id, @org1.id, "Organization")
    clc_progresses = ClcProgress.where(user_id: @user1.id, clc_id: @org1_clc.id)
    assert_equal 1, clc_progresses.length
    assert_equal 5, clc_progresses[0].clc_score
  end
end

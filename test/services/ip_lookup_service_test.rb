require 'test_helper'

class IpLookupServiceTest < ActiveSupport::TestCase
  setup do
    response_body = {
      "country_code" => "JP",
      "country_name" => "Japan",
      "time_zone"    => "Asia/Tokyo"
    }

    stub_ip_lookup(ip_address: '127.0.0.1', response_body: response_body)
    stub_ip_lookup(ip_address: '127.0.0.2')
  end

  test 'gets timezone using ip address' do
    assert_equal "Asia/Tokyo", IpLookupService.new("127.0.0.1").get_time_zone
    assert_nil   IpLookupService.new("127.0.0.2").get_time_zone
  end
end

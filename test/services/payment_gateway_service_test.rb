require 'test_helper'

class PaymentGatewayServiceTest < ActiveSupport::TestCase
  setup do
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @card = create :card, organization: @organization, author: @user
    @price = create(:price, card: @card)
  end

  test '#prepare payment must return transaction details for wallet' do
    result = PaymentGatewayService.new(
      orderable_type: 'Card',
      orderable: @card,
      price: @price,
      user_id: @user.id,
      organization_id: @organization.id,
      gateway: 'wallet').prepare_payment!

    assert_not_nil result[:transaction]
    assert_not_nil result[:order]
    assert_not_nil result[:token]
  end

  test '#prepare payment must return transaction details with client token for braintree' do
    Braintree::ClientToken.stubs(:generate).returns('A1B2C3D4E5F6H7')

    result = PaymentGatewayService.new(
      orderable_type: 'Card',
      orderable: @card,
      price: @price,
      user_id: @user.id,
      organization_id: @organization.id,
      gateway: 'braintree').prepare_payment!
    
    assert_not_nil result[:transaction]
    assert_not_nil result[:order]
    assert_not_nil result[:token]
    assert_not_nil result[:client_token]
  end

  test '#prepare payment must return transaction details for paypal' do
    payment_object = PayPal::SDK::REST::Payment.new(id: 'PAY-A1B2C3D4E5F7G8H9')
    payment_object.stubs(:approval_url).returns('http://sandbox-paypal-approval.com')
    Card.any_instance.stubs(:redirect_page_url).returns('http://qa.lvh.me/insights/card')
    PaypalPaymentGateway.any_instance.stubs(:create_payment_object).returns(payment_object)
    result = PaymentGatewayService.new(
      orderable_type: 'Card',
      orderable: @card,
      price: @price,
      user_id: @user.id,
      organization_id: @organization.id,
      gateway: 'paypal').prepare_payment!
    
    assert_not_nil result[:transaction]
    assert_not_nil result[:order]
    assert_not_nil result[:token]
    assert_not_nil result[:redirect_url]
  end

  test '#process_payment must return transaction object' do
    braintree_object = Braintree::SuccessfulResult.new
    braintree_object.stubs(:success?).returns(true)
    Transaction.any_instance.stubs(:set_gateway_information)
    BraintreePaymentGateway.any_instance.stubs(:authorize_payment).returns(braintree_object)
    Transaction.any_instance.stubs(:confirm_order).returns(true)
    BraintreePaymentGateway.any_instance.expects(:capture_payment).returns(true)

    order = create(:order, orderable: @card)
    transaction = create(:transaction, order: order)
    token = JWT.encode({payment_id: transaction.id}, Edcast::Application.config.secret_key_base, 'HS256')
    result_transaction = PaymentGatewayService.process_payment!(token: token,
                                                                gateway: 'braintree',
                                                                nonce: 'qwertyuiop')

    assert_not_nil result_transaction
  end

  test '#process_payment must raise error if token is incorrect' do
    token = 'dummy-wrong-token'
    exception = assert_raises(Exception) { PaymentGatewayService.process_payment!(token: token,
                                                                                  gateway: 'braintree',
                                                                                  nonce: 'qwertyuiop')
                                             }
    assert_equal IllFormattedTransactionIdError, exception.class
  end

end

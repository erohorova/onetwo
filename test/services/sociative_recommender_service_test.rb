require 'test_helper'

class SociativeRecmmenderServiceTest < ActiveSupport::TestCase

  test 'should have constants set right' do
    assert_equal 'CC', SociativeRecommenderService::STRATEGY_CC
    assert_equal 'CU', SociativeRecommenderService::STRATEGY_CU
    assert_equal 'CUC', SociativeRecommenderService::STRATEGY_CUC

    assert_not_nil SociativeRecommenderService::SOCIATIVE_ENDPOINT
  end

  test 'should call sociative service with post params #CC' do
    user = create(:user)
    content = create(:card, author: user, ecl_id: 'abc-1235')

    stub_request(:post, Settings.sociative.recommender_api).with(
      body: get_tag_req_body('CC', user, content)
    ).to_return(body: sample_sociative_content_resp.to_json, status: 200)

    stub_ecl_req

    status, results = SociativeRecommenderService.new(
      user_id: user.id,
      content_id: content.ecl_id,
      tag: 'CC'
    ).recommendations

    assert_requested(:post, Settings.sociative.recommender_api, times: 1)
    assert_requested(:post, "#{Settings.ecl.api_endpoint}/api/#{Settings.ecl.api_version}/content_items/search", times: 1)

    assert_equal 3, results[:cards].size
  end

  test 'should be able to get content from ECL for uuids' do
    user = create(:user)
    content = create(:card, author: user, ecl_id: 'abc-1235')

    # just a connection to emulate virtual cards
    ecl_conn = EclApi::EclSearch.new(user.organization.id, user.id)

    # stub request to get content ids
    stub_request(:post, Settings.sociative.recommender_api).with(
      body: get_tag_req_body('CC', user, content, size: 2)
    ).to_return(body: sample_sociative_content_resp.to_json, status: 200)

    # expecting these returned ids to be forwarded to ecl
    content_uuids = sample_sociative_content_resp[:data][:nodes]
      .collect {|n| n[:id] if n[:soc] && n[:kind] == 'content'}
      .compact
    content_uuids.delete content.ecl_id

    stub_ecl_req

    # These are the cards returned to caller of recommender service
    virtual_cards = ecl_conn.send(:_format_data_as_cards, ecl_resp['data'])

    # expect to be get called once
    ecl_search_mock = mock()
    EclApi::EclSearch.expects(:new).with(user.organization_id, user.id)
    .returns(ecl_search_mock)

    ecl_search_mock
    .expects(:search)
    .with(
      has_entries(
        query: '',
        limit: 2,
        offset: 0,
        filter_params: {ecl_card_ids: content_uuids}
      )
    ).returns(OpenStruct.new(results: virtual_cards, aggregations: []))

    # Execute recommender service
    SociativeRecommenderService.new(
      user_id: user.id,
      content_id: content.ecl_id,
      size: 2
    ).recommendations
  end

  test 'should call sociative service with post params #CUC' do
    user = create(:user)
    content = create(:card, author: user, ecl_id: 'abc-1235')

    stub_request(:post, Settings.sociative.recommender_api).with(
      body: get_tag_req_body('CUC', user, content)
    ).to_return(body: sample_sociative_content_resp.to_json, status: 200)

    stub_ecl_req

    SociativeRecommenderService.new(
      user_id: user.id,
      content_id: content.ecl_id,
      tag: 'CUC'
    ).recommendations

    assert_requested(:post, Settings.sociative.recommender_api, times: 1)
    assert_requested(:post, "#{Settings.ecl.api_endpoint}/api/#{Settings.ecl.api_version}/content_items/search", times: 1)
  end

  test 'should call sociative service with post params #CT' do
    user = create(:user)
    content = create(:card, author: user, ecl_id: 'abc-1235')

    stub_request(:post, Settings.sociative.recommender_api).with(
      body: get_tag_req_body('CT', user, content).to_json
    ).to_return(body: sample_sociative_content_resp.to_json, status: 200)

    SociativeRecommenderService.any_instance.stubs(:load_cards_from_content_nodes)

    status, results = SociativeRecommenderService.new(
      user_id: user.id,
      content_id: content.ecl_id,
      tag: 'CT'
    ).recommendations

    expected_topics = sample_sociative_content_resp.with_indifferent_access['data']["nodes"]
      .each_with_object([]) do |n, m|
        if n['kind'] == 'topic'
          m << {name: n['id'], label: n['id'].split('.').last.humanize}
        end
      end

    assert_requested(:post, Settings.sociative.recommender_api, times: 1)
    assert_same_elements expected_topics, results[:topics]
  end

  test 'should call sociative service with post params #CU' do
    org = create(:organization)
    user = create(:user, organization: org)
    recommended_user = create(:user, organization: org)
    content = create(:card, author: user, ecl_id: 'abc-1235')

    stub_request(:post, Settings.sociative.recommender_api).with(
      body: get_tag_req_body('CU', user, content).to_json
    ).to_return(body:
      {
        data: {
          nodes: [
            {
              id: recommended_user.id,
              kind: 'user',
              soc: false
            },
            {
              id: user.id,
              kind: 'user',
              soc: false
            }
          ]
        }
      }.to_json, status: 200
    )

    SociativeRecommenderService.any_instance.stubs(:load_cards_from_content_nodes)

    status, results = SociativeRecommenderService.new(
      user_id: user.id,
      content_id: content.ecl_id,
      tag: 'CU'
    ).recommendations

    assert_requested(:post, Settings.sociative.recommender_api, times: 1)
    assert_equal 1, results[:users].size
    assert_same_elements [recommended_user], results[:users]
  end

  test 'should return error msg for content not found #200' do
    org = create(:organization)
    user = create(:user, organization: org)
    recommended_user = create(:user, organization: org)
    content = create(:card, author: user, ecl_id: 'abc-1235')
    error_msg = 'no tau_set for key tau_set_ws at node_id abc'

    stub_request(:post, Settings.sociative.recommender_api).with(
      body: get_tag_req_body('CU', user, content).to_json
    ).to_return(body:
      {
        data: nil,
        error: error_msg
      }.to_json,
      status: 200
    )

    SociativeRecommenderService.any_instance.stubs(:load_cards_from_content_nodes)
    Bugsnag.expects(:notify).with(
      error_msg, {severity: 'error'}
    ).once

    status, results = SociativeRecommenderService.new(
      user_id: user.id,
      content_id: content.ecl_id,
      tag: 'CU'
    ).recommendations

    assert_requested(:post, Settings.sociative.recommender_api, times: 1)
    assert_not_nil results[:error].size

    assert_nil results[:cards]
    assert_nil results[:users]
    assert_nil results[:topics]
  end

  test 'should return error message service status_400/500' do
    org = create(:organization)
    user = create(:user, organization: org)
    recommended_user = create(:user, organization: org)
    content = create(:card, author: user, ecl_id: 'abc-1235')
    error_msg = 'Internal server error'

    stub_request(:post, Settings.sociative.recommender_api).with(
      body: get_tag_req_body('CU', user, content).to_json
    ).to_return(body:
      {
        data: nil,
        error: error_msg
      }.to_json,
      status: 500
    )

    SociativeRecommenderService.any_instance.stubs(:load_cards_from_content_nodes)
    Bugsnag.expects(:notify).with(
      error_msg, {severity: 'error'}
    ).once

    status, results = SociativeRecommenderService.new(
      user_id: user.id,
      content_id: content.ecl_id,
      tag: 'CU'
    ).recommendations

    assert_requested(:post, Settings.sociative.recommender_api, times: 1)
    assert_not_nil results[:error].size

    assert_nil results[:cards]
    assert_nil results[:users]
    assert_nil results[:topics]
  end

  test 'should parse content weights from sco response' do
    user = create(:user)
    content = create(:card, author: user, ecl_id: 'abc-1235')

    stub_request(:post, Settings.sociative.recommender_api).with(
      body: get_tag_req_body('CC', user, content)
    ).to_return(body: sample_sociative_content_resp.to_json, status: 200)

    stub_ecl_req

    status, results = SociativeRecommenderService.new(
      user_id: user.id,
      content_id: content.ecl_id,
      tag: 'CC'
    ).recommendations

    assert_requested(:post, Settings.sociative.recommender_api, times: 1)
    assert_requested(:post, "#{Settings.ecl.api_endpoint}/api/#{Settings.ecl.api_version}/content_items/search", times: 1)

    assert_equal 2, results[:content_weights].size
  end

  test 'should respect limit passed to #CU' do
    org = create(:organization)
    user = create(:user, organization: org)
    recommended_users = create_list(:user, 3, organization: org)
    content = create(:card, author: user, ecl_id: 'abc-1235')
    resp_user_nodes = []
    recommended_users.each do |user|
      resp_user_nodes << {
        id: user.id,
        kind: 'user',
        soc: false
      }
    end

    stub_request(:post, Settings.sociative.recommender_api).with(
      body: get_tag_req_body('CU', user, content, size: 2).to_json
    ).to_return(body:
      {
        data: {
          nodes: resp_user_nodes
        }
      }.to_json, status: 200
    )

    SociativeRecommenderService.any_instance.stubs(:load_cards_from_content_nodes)

    status, results = SociativeRecommenderService.new(
      user_id: user.id,
      content_id: content.ecl_id,
      tag: 'CU',
      size: 2
    ).recommendations

    assert_requested(:post, Settings.sociative.recommender_api, times: 1)
    assert_equal 2, results[:users].size
    assert_same_elements recommended_users.first(2), results[:users]
  end

  test 'should respect limit passed to #CC' do
    user = create(:user)
    content = create(:card, author: user, ecl_id: 'abc-1235')

    stub_request(:post, Settings.sociative.recommender_api).with(
      body: get_tag_req_body('CC', user, content, size: 2)
    ).to_return(body: sample_sociative_content_resp.to_json, status: 200)
    stub_ecl_req

    ecl_search_mock = mock()
    EclApi::EclSearch.expects(:new).returns ecl_search_mock
    # expect to have only two ids in ecl search.
    ecl_search_mock.expects(:search).with(
      query: '',
      limit: 2,
      offset: 0,
      filter_params: {
        ecl_card_ids: ['kDkJlNSe1BNZus8XPJ2uOg==', 'kDkJlNSe1BNZus8XPJ1uOg==']
      },
      load_tags: false
    ).returns(OpenStruct.new(results: []))

    status, results = SociativeRecommenderService.new(
      user_id: user.id,
      content_id: content.ecl_id,
      tag: 'CC',
      size: 2
    ).recommendations

    assert_requested(:post, Settings.sociative.recommender_api, times: 1)
  end

  # This will be updated as we will make progress
  def sample_sociative_content_resp
    {
      'data': {
        'nodes': [  # list of nodes
            {
                'id': 'kDkJlNSe1BNZus8XPJ2uOg==',
                'kind': 'content',
                'soc': true
            },
            {
                'id': 'abc-1235',
                'kind': 'content',
                'soc': true
            },
            {
                'id': 'kDkJlNSe1BNZus8XPJ1uOg==',
                'kind': 'content',
                'soc': true
            },          {
                'id': 'edcast.technology.game_design_and_development.gaming_technology',
                'kind': 'topic',
                'soc': true
            },          {
                'id': 'edcast.health.health_and_fitness.mental_health',
                'kind': 'topic',
                'soc': true
            },
            {
                'id': 'some-diff-id',
                'kind': 'content',
                'soc': false
            }
        ],
        'edges': [  # list of edges
          {
            "src" => {
              "kind" => "content",
              "id" => "i5HvzWIJSagwidZeReULoQ=="
            },
            "dst" => {
              "kind" => "content",
              "id" => "kDkJlNSe1BNZus8XPJ2uOg=="
            },
            "data" => {},
            "soc" => true,
            "weight" => 0.5169246196746826
          }, {
            "src" => {
              "kind" => "content",
              "id" => "i5HvzWIJSagwidZeReULoQ=="
            },
            "dst" => {
              "kind" => "content",
              "id" => "kDkJlNSe1BNZus8XPJ1uOg=="
            },
            "data" => {},
            "soc" => true,
            "weight" => 0.45973101258277893
          }
        ]
      }
    }
  end

  def get_tag_req_body(tag, user, content, size: 5)
    {
      tag: tag,
      context: {
        user_id: user.id,
        org_id: user.organization.id
      }
    }.merge!(entity_and_related_req_params(tag, content, size))
  end

  def entity_and_related_req_params(tag, content, size)
    entity_releated_hash = case tag
    when 'CC', 'CUC'
      {
        entity: {id: content.ecl_id, kind: 'content'},
        related: {size: size, kind: 'content'}
      }
    when 'CU'
      {
        entity: {id: content.ecl_id, kind: 'content'},
        related: {size: size, kind: 'user'}
      }
    when 'CT'
      {
        entity: {id: content.ecl_id, kind: 'content'},
        related: {size: size, kind: 'topic'}
      }
    end
    entity_releated_hash || {}
  end

  def stub_ecl_req
    # ecl endpoint gets hit with two snow flake ids and returns virtual cards
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/#{Settings.ecl.api_version}/content_items/search")
      .to_return(body: ecl_resp.to_json, status: 200)
  end

  def ecl_resp
    resp ||= File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json")
    JSON.parse(resp)["/api/#{Settings.ecl.api_version}/content_items"]
  end
end
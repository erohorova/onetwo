require 'test_helper'

class UserTransitions::InactiveTest < ActiveSupport::TestCase
  setup do
    @org  = create :organization
    @user = create :user, organization: @org
    @other_user = create :user, organization: @org

    # Entities with the interactions by the @user.
    # Step 1: Card created by @other_user and tracks the activity streams.
    @card = create :card, author: @other_user, organization: @org
    @card_stream = create :activity_stream, streamable: @card, user: @other_user,
      action: ActivityStream::ACTION_CREATED_CARD, organization: @org, card_id: @card.id

    @assignable_card = create :card, author: @other_user, organization: @org
    @assignable_stream = create :activity_stream, streamable: @assignable_card, user: @other_user,
      action: ActivityStream::ACTION_CREATED_CARD, organization: @org, card_id: @assignable_card.id

    @bookmarkable_card = create :card, author: @other_user, organization: @org
    @bookmarkable_stream =create :activity_stream, streamable: @bookmarkable_card, user: @other_user,
      action: ActivityStream::ACTION_CREATED_CARD, organization: @org, card_id: @bookmarkable_card.id

    @completable_card = create :card, author: @other_user, organization: @org
    @completable_stream = create :activity_stream, streamable: @completable_card, user: @other_user,
      action: ActivityStream::ACTION_CREATED_CARD, organization: @org, card_id: @completable_card.id

    # Step 2: @card upvoted by the @user and @other_user.
    @vote = create :vote, votable: @card, voter: @user
    create :vote, votable: @card, voter: @other_user

    # Step 3: @card commented by the @user.
    @comment = create :comment, commentable: @card, user: @user

    # Step 4: @card commented by the @other_user and @user is tagged.
    @tagged_comment = create :comment, commentable: @card, user: @other_user, message: "Hello #{@user.handle}"
    @mention = create :mention, mentionable: @comment, user: @user

    # Step 5: Channel authored by the @other_user and is followed by @user.
    @channel = create :channel, organization: @org
    @channel.add_authors([@other_user])
    @channel.add_followers([@user])

    # Step 6: @user follows the @other_user.
    @user_user_follow = Follow.create(followable: @other_user, user: @user)

    # Entities owned by the @user.
    # Step 7: Card authored by @user.
    @card2 = create :card, author_id: @user, organization: @org
    @authored_card_stream = create :activity_stream, streamable: @card2,
      user: @user, action: ActivityStream::ACTION_CREATED_CARD, organization: @org, card_id: @card2.id
    # Step 8: Channel authored by @user.
    @channel2 = create :channel, organization: @org
    @channel2.add_authors([@user])
    # Step 9: Bookmarked by @user.
    @bookmarked = create :bookmark, bookmarkable: @bookmarkable_card, user: @user
    # Step 10: Assigned to @user.
    @assigned = create :assignment, assignable: @assignable_card, assignee: @user
    @completed = create :user_content_completion, completable: @completable_card, user: @user

    # Step 11: Commented on activity stream.
    # commentable_stream = ActivityStream.find_by(streamable: @card)
    @commented_stream = create :comment, user: @user, commentable: @assignable_stream

    # Step 12: Voted the activity stream.
    @voted_stream = create :vote, user_id: @user.id, votable: @assignable_stream

    # Step 13: @user tags @other_user.
    other_user_tagged_stream = create :comment, user: @user, commentable: @commented_stream
    other_user_mention = create :mention, mentionable: other_user_tagged_stream, user: @other_user

    # Step 14: @other_user tags @user.
    self_tagged_stream = create :comment, user: @other_user, commentable: @commented_stream
    @self_user_mention = create :mention, mentionable: self_tagged_stream, user: @user

    # Step 15: suspend the user. Initial state.
    @user.suspend!

    @user.reload
  end

  # When the user is marked as `inactive` from `suspended` state
  # START #
  test 'marks the user as `inactive` if the user was `suspended`' do
    refute @user.is_active
    assert @user.is_suspended
    assert_equal 'suspended', @user.status

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    assert @user.is_active
    refute @user.is_suspended
    assert_equal 'inactive', @user.status
  end

  test 'reindex the user when user is marked as `inactive`' do
    refute @user.is_active
    assert @user.is_suspended
    @user.stubs(:reindex_async)

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark
  end

  test 'restore votes given by the @user to the card if the user is marked from `suspended` state' do
    assert @vote.reload.deleted?
    assert @user.is_suspended
    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    @vote.reload
    refute @vote.deleted?
  end

  test 're-calculate votes_count for the cards if the user is marked from `suspended` state' do
    assert_equal 1, @card.reload.votes_count
    assert @user.is_suspended

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    assert_equal 2, @card.reload.votes_count
  end

  test 'restore comments added by the @user to the card if the user is marked from `suspended` state' do
    assert @comment.reload.deleted?
    assert @user.is_suspended
    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    @comment.reload
    refute @comment.deleted?
  end

  test 're-calculate comments_count for the cards if the user is marked from `suspended` state' do
    assert_equal 1, @card.reload.votes_count
    assert @user.is_suspended
    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    assert_equal 2, @card.reload.votes_count
  end

  test 'restore mentions that tags @user to the card by @other_user if the user is marked from `suspended` state' do
    assert @mention.reload.deleted?
    assert @user.is_suspended
    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    @mention.reload
    refute @mention.deleted?
  end

  test 'restore user-channel follows association if the user is marked from `suspended` state' do
    assert @user.is_suspended
    follow = Follow.only_deleted.find_by(followable: @channel, user: @user)
    assert follow.reload.deleted?

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    follow.reload
    refute follow.deleted?
  end

  test 'restore user-user follows association if the user is marked from `suspended` state' do
    assert @user_user_follow.reload.deleted?

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    @user_user_follow.reload
    refute @user_user_follow.deleted?
  end

  test 'restore the activity streams if the user is marked from `suspended` state' do
    assert @authored_card_stream.reload.deleted?

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    @authored_card_stream.reload
    refute @authored_card_stream.deleted?
  end

  test 'restore the votes for an activity stream if the user is marked from `suspended` state' do
    assert @voted_stream.reload.deleted?

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    @voted_stream.reload
    refute @voted_stream.deleted?
  end

  test 're-calculate votes_count of a activity stream if the user is marked from `suspended` state' do
    assert_equal 0, @assignable_stream.reload.votes_count

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    assert_equal 1, @assignable_stream.reload.votes_count
  end

  test 'restore the comments for an activity stream if the user is marked from `suspended` state' do
    assert @commented_stream.reload.deleted?

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    @commented_stream.reload
    refute @commented_stream.deleted?
  end

  test 're-calculate comments_count of a activity stream if the user is marked from `suspended` state' do
    assert_equal 0, @assignable_stream.reload.comments_count

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    assert_equal 1, @assignable_stream.reload.comments_count
  end

  test 'restore the mentions that tags @user in an activity stream if the user is marked from `suspended` state' do
    assert @self_user_mention.reload.deleted?

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    @self_user_mention.reload
    refute @self_user_mention.deleted?
  end

  test 'check the integrity of the bookmarked cards' do
    assert_includes @user.reload.bookmarked_card_ids, @bookmarked.bookmarkable_id

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    assert_includes @user.reload.bookmarked_card_ids, @bookmarked.bookmarkable_id
  end

  test 'check the integrity of the assigned cards' do
    assert_equal @assignable_card.id, @user.assignment_for(@assignable_card).assignable_id

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    assert_equal @assignable_card.id, @user.assignment_for(@assignable_card).assignable_id
  end

  test 'check the integrity of the completed cards' do
    assert_includes @user.reload.completed_card_ids, @completable_card.id

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    assert_includes @user.reload.completed_card_ids, @completable_card.id
  end

  test 'record the event as `user_unsuspended` to Influx' do
    Analytics::MetricsRecorderJob.expects(:perform_later).with(includes(
      :recorder, :event, :actor, :org, :suspended_user, :additional_data
    ))

    UserTransitions::Active.new(users: [@user], organization: @org).mark
  end
  # END #

  # When the user is marked as `inactive` from `active` state
  # START #
  test 'marks the user as `inactive` if the user was `active`' do
    # initial state as `active`
    UserTransitions::Active.new(users: [@other_user], organization: @org).mark

    assert @other_user.is_active
    refute @other_user.is_suspended

    UserTransitions::Inactive.new(users: [@other_user], organization: @org).mark

    assert @other_user.is_active
    refute @other_user.is_suspended
    assert_equal 'inactive', @other_user.status
  end

  test 'do not soft-delete/restore votes given by the @user to the card if the user is marked from `active` state' do
    # initial state as `active`
    UserTransitions::Active.new(users: [@user], organization: @org).mark

    assert @user.is_active
    refute @vote.reload.deleted?

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    @vote.reload
    refute @vote.deleted?
  end

  test 'do not re-calculate votes_count for the cards if the user is marked from `active` state' do
    # initial state as `active`
    UserTransitions::Active.new(users: [@user], organization: @org).mark

    assert_equal 2, @card.reload.votes_count
    assert @user.is_active

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    assert_equal 2, @card.reload.votes_count
  end

  test 'do not soft-delete/restore comments added by the @user to the card if the user is marked from `active` state' do
    # initial state as `active`
    UserTransitions::Active.new(users: [@user], organization: @org).mark

    refute @comment.reload.deleted?
    assert @user.is_active
    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    @comment.reload
    refute @comment.deleted?
  end

  test 'do not re-calculate comments_count for the cards if the user is marked from `active` state' do
    # initial state as `active`
    UserTransitions::Active.new(users: [@user], organization: @org).mark

    assert_equal 2, @card.reload.votes_count
    assert @user.is_active
    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    assert_equal 2, @card.reload.votes_count
  end

  test 'do not soft-delete/restore mentions that tags @user to the card by @other_user if the user is marked from `active` state' do
    # initial state as `active`
    UserTransitions::Active.new(users: [@user], organization: @org).mark

    refute @mention.reload.deleted?
    assert @user.is_active
    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    @mention.reload
    refute @mention.deleted?
  end

  test 'do not soft-delete/restore user-channel follows association if the user is marked from `active` state' do
    # initial state as `active`
    UserTransitions::Active.new(users: [@user], organization: @org).mark

    assert @user.is_active
    follow = Follow.with_deleted.find_by(followable: @channel, user: @user)
    assert follow

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    follow.reload
    refute follow.deleted?
  end

  test 'do not soft-delete/restore user-user follows association if the user is marked from `active` state' do
    # initial state as `active`
    UserTransitions::Active.new(users: [@user], organization: @org).mark

    refute @user_user_follow.reload.deleted?

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    @user_user_follow.reload
    refute @user_user_follow.deleted?
  end

  test 'do not soft-delete/restore the activity streams if the user is marked from `active` state' do
    # initial state as `active`
    UserTransitions::Active.new(users: [@user], organization: @org).mark

    refute @authored_card_stream.reload.deleted?

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    @authored_card_stream.reload
    refute @authored_card_stream.deleted?
  end

  test 'do not soft-delete/restore the votes for an activity stream if the user is marked from `active` state' do
    # initial state as `active`
    UserTransitions::Active.new(users: [@user], organization: @org).mark

    refute @voted_stream.reload.deleted?

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    @voted_stream.reload
    refute @voted_stream.deleted?
  end

  test 'do not re-calculate votes_count of a activity stream if the user is marked from `active` state' do
    # initial state as `active`
    UserTransitions::Active.new(users: [@user], organization: @org).mark

    assert_equal 1, @assignable_stream.reload.votes_count

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    assert_equal 1, @assignable_stream.reload.votes_count
  end

  test 'do not soft-delete/restore the comments for an activity stream if the user is marked from `active` state' do
    # initial state as `active`
    UserTransitions::Active.new(users: [@user], organization: @org).mark

    refute @commented_stream.reload.deleted?

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    @commented_stream.reload
    refute @commented_stream.deleted?
  end

  test 'do not re-calculate comments_count of a activity stream if the user is marked from `active` state' do
    # initial state as `active`
    UserTransitions::Active.new(users: [@user], organization: @org).mark

    assert_equal 1, @assignable_stream.reload.comments_count

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    assert_equal 1, @assignable_stream.reload.comments_count
  end

  test 'do not soft-delete/restore the mentions that tags @user in an activity stream if the user is marked from `active` state' do
    # initial state as `active`
    UserTransitions::Active.new(users: [@user], organization: @org).mark

    refute @self_user_mention.reload.deleted?

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    @self_user_mention.reload
    refute @self_user_mention.deleted?
  end

  test 'record the event as `user_edited` to Influx when status is changed from `active` to `inactive`' do
    # initial state as `active`
    UserTransitions::Active.new(users: [@user], organization: @org).mark

    Analytics::MetricsRecorderJob.expects(:perform_later).with(includes(
      :recorder, :event, :actor, :org, :user, :timestamp,
      :additional_data, :changed_column, :old_val, :new_val
    ))

    UserTransitions::Inactive.new(users: [@user], organization: @org).mark
  end
  # END #
end

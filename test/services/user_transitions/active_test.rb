require 'test_helper'

class UserTransitions::ActiveTest < ActiveSupport::TestCase
  setup do
    @org  = create :organization
    @user = create :user, organization: @org
    @other_user = create :user, organization: @org

    # Entities with the interactions by the @user.
    # Step 1: Card created by @other_user and tracks the activity streams.
    @card = create :card, author: @other_user, organization: @org
    @card_stream = create :activity_stream, streamable: @card, user: @other_user,
      action: ActivityStream::ACTION_CREATED_CARD, organization: @org, card_id: @card.id

    @assignable_card = create :card, author: @other_user, organization: @org
    @assignable_stream = create :activity_stream, streamable: @assignable_card, user: @other_user,
      action: ActivityStream::ACTION_CREATED_CARD, organization: @org, card_id: @assignable_card.id

    @bookmarkable_card = create :card, author: @other_user, organization: @org
    @bookmarkable_stream =create :activity_stream, streamable: @bookmarkable_card, user: @other_user,
      action: ActivityStream::ACTION_CREATED_CARD, organization: @org, card_id: @bookmarkable_card.id

    @completable_card = create :card, author: @other_user, organization: @org
    @completable_stream = create :activity_stream, streamable: @completable_card, user: @other_user,
      action: ActivityStream::ACTION_CREATED_CARD, organization: @org, card_id: @completable_card.id

    # Step 2: @card upvoted by the @user and @other_user.
    @vote = create :vote, votable: @card, voter: @user
    create :vote, votable: @card, voter: @other_user

    # Step 3: @card commented by the @user.
    @comment = create :comment, commentable: @card, user: @user

    # Step 4: @card commented by the @other_user and @user is tagged.
    @tagged_comment = create :comment, commentable: @card, user: @other_user, message: "Hello #{@user.handle}"
    @mention = create :mention, mentionable: @comment, user: @user

    # Step 5: Channel authored by the @other_user and is followed by @user.
    @channel = create :channel, organization: @org
    @channel.add_authors([@other_user])
    @channel.add_followers([@user])

    #Make team follow a channel
    @team_user = create :user, organization: @org
    @team = create :team, organization: @org
    @team.add_user(@team_user)
    @channel.followed_team_ids = [@team.id]


    # Step 6: @user follows the @other_user.
    @user_user_follow = Follow.create(followable: @other_user, user: @user)

    # Entities owned by the @user.
    # Step 7: Card authored by @user.
    @card2 = create :card, author_id: @user, organization: @org
    @authored_card_stream = create :activity_stream, streamable: @card2,
      user: @user, action: ActivityStream::ACTION_CREATED_CARD, organization: @org, card_id: @card2.id
    # Step 8: Channel authored by @user.
    @channel2 = create :channel, organization: @org
    @channel2.add_authors([@user])
    # Step 9: Bookmarked by @user.
    @bookmarked = create :bookmark, bookmarkable: @bookmarkable_card, user: @user
    # Step 10: Assigned to @user.
    @assigned = create :assignment, assignable: @assignable_card, assignee: @user
    @completed = create :user_content_completion, completable: @completable_card, user: @user

    # Step 11: Commented on activity stream.
    # commentable_stream = ActivityStream.find_by(streamable: @card)
    @commented_stream = create :comment, user: @user, commentable: @assignable_stream

    # Step 12: Voted the activity stream.
    @voted_stream = create :vote, user_id: @user.id, votable: @assignable_stream

    # Step 13: @user tags @other_user.
    other_user_tagged_stream = create :comment, user: @user, commentable: @commented_stream
    other_user_mention = create :mention, mentionable: other_user_tagged_stream, user: @other_user

    # Step 14: @other_user tags @user.
    self_tagged_stream = create :comment, user: @other_user, commentable: @commented_stream
    @self_user_mention = create :mention, mentionable: self_tagged_stream, user: @user

    # Step 15: Suspend the user.
    @user.suspend!

    @user.reload
  end

  # When the user is marked as `active` from `suspended` state
  # START #
  test 'marks the user as `active` if the user was suspended' do
    refute @user.is_active
    assert @user.is_suspended
    assert_equal 'suspended', @user.status

    UserTransitions::Active.new(users: [@user], organization: @org).mark

    assert @user.is_active
    refute @user.is_suspended
    assert_equal 'active', @user.status
  end

  test 'reindex the user' do
    @user.stubs(:reindex_async)

    UserTransitions::Active.new(users: [@user], organization: @org).mark
  end

  test 'restore votes given by the @user to the card if the user was suspended' do
    assert @vote.reload.deleted?

    UserTransitions::Active.new(users: [@user], organization: @org).mark

    @vote.reload
    refute @vote.deleted?
  end

  test 're-calculate votes_count for the cards if the user was suspended' do
    assert_equal 1, @card.reload.votes_count

    UserTransitions::Active.new(users: [@user], organization: @org).mark

    assert_equal 2, @card.reload.votes_count
  end

  test 'restore comments added by the @user to the card if the user was suspended' do
    assert @comment.reload.deleted?

    UserTransitions::Active.new(users: [@user], organization: @org).mark

    @comment.reload
    refute @comment.deleted?
  end

  test 're-calculate comments_count for the cards if the user was suspended' do
    assert_equal 1, @card.reload.votes_count

    UserTransitions::Active.new(users: [@user], organization: @org).mark

    assert_equal 2, @card.reload.votes_count
  end

  test 'restore mentions that tags @user to the card by @other_user if the user was suspended' do
    assert @mention.reload.deleted?

    UserTransitions::Active.new(users: [@user], organization: @org).mark

    @mention.reload
    refute @mention.deleted?
  end

  test 'restore user-channel follows association if the user was suspended' do
    follow = Follow.only_deleted.find_by(followable: @channel, user: @user)
    assert follow.reload.deleted?

    UserTransitions::Active.new(users: [@user], organization: @org).mark

    follow.reload
    refute follow.deleted?
  end

  test 'restore user-user follows association if the user was suspended' do
    assert @user_user_follow.reload.deleted?

    UserTransitions::Active.new(users: [@user], organization: @org).mark

    @user_user_follow.reload
    refute @user_user_follow.deleted?
  end

  test 'restore the activity streams if the user was suspended' do
    assert @authored_card_stream.reload.deleted?

    UserTransitions::Active.new(users: [@user], organization: @org).mark

    @authored_card_stream.reload
    refute @authored_card_stream.deleted?
  end

  test 'restore the votes for an activity stream if the user was suspended' do
    assert @voted_stream.reload.deleted?

    UserTransitions::Active.new(users: [@user], organization: @org).mark

    @voted_stream.reload
    refute @voted_stream.deleted?
  end

  test 're-calculate votes_count of a activity stream if the user was suspended' do
    assert_equal 0, @assignable_stream.reload.votes_count

    UserTransitions::Active.new(users: [@user], organization: @org).mark

    assert_equal 1, @assignable_stream.reload.votes_count
  end

  test 'restore the comments for an activity stream if the user was suspended' do
    assert @commented_stream.reload.deleted?

    UserTransitions::Active.new(users: [@user], organization: @org).mark

    @commented_stream.reload
    refute @commented_stream.deleted?
  end

  test 're-calculate comments_count of a activity stream if the user was suspended' do
    assert_equal 0, @assignable_stream.reload.comments_count

    UserTransitions::Active.new(users: [@user], organization: @org).mark

    assert_equal 1, @assignable_stream.reload.comments_count
  end

  test 'restore the mentions that tags @user in an activity stream if the user was suspended' do
    assert @self_user_mention.reload.deleted?

    UserTransitions::Active.new(users: [@user], organization: @org).mark

    @self_user_mention.reload
    refute @self_user_mention.deleted?
  end

  test 'check the integrity of the channels authored by the @user if the user was suspended' do
    refute_includes @channel2.reload.authors.pluck(:id), @user.id

    UserTransitions::Active.new(users: [@user], organization: @org).mark

    assert_includes @channel2.reload.authors.pluck(:id), @user.id
  end

  test 'check the integrity of the bookmarked cards' do
    assert_includes @user.reload.bookmarked_card_ids, @bookmarked.bookmarkable_id

    UserTransitions::Active.new(users: [@user], organization: @org).mark

    assert_includes @user.reload.bookmarked_card_ids, @bookmarked.bookmarkable_id
  end

  test 'check the integrity of the assigned cards' do
    assert_equal @assignable_card.id, @user.assignment_for(@assignable_card).assignable_id

    UserTransitions::Active.new(users: [@user], organization: @org).mark

    assert_equal @assignable_card.id, @user.assignment_for(@assignable_card).assignable_id
  end

  test 'check the integrity of the completed cards' do
    assert_includes @user.reload.completed_card_ids, @completable_card.id

    UserTransitions::Active.new(users: [@user], organization: @org).mark

    assert_includes @user.reload.completed_card_ids, @completable_card.id
  end

  test 'record the event as `user_unsuspended` to Influx' do
    Analytics::MetricsRecorderJob.expects(:perform_later).with(includes(
      :recorder, :event, :actor, :org, :suspended_user, :additional_data
    ))

    UserTransitions::Active.new(users: [@user], organization: @org).mark
  end
  # END #

  # When the user is marked as `active` from `inactive` state
  # START #
  test 'marks the user as `active` if the user was inactive' do
    # initial state as `inactive`
    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    assert @user.reload.inactive?
    assert_equal 'inactive', @user.status

    UserTransitions::Active.new(users: [@user], organization: @org).mark

    assert @user.reload.active?
    assert_equal 'active', @user.status
  end

  test 'record the event as `user_edited` to Influx when status is changed from `active` to `inactive`' do
    # initial state as `active`
    UserTransitions::Inactive.new(users: [@user], organization: @org).mark

    Analytics::MetricsRecorderJob.expects(:perform_later).with(includes(
      :recorder, :event, :actor, :org, :user, :timestamp,
      :additional_data, :changed_column, :old_val, :new_val
    ))

    UserTransitions::Active.new(users: [@user], organization: @org).mark
  end
  # END #

  test 'deletes the rails cache of channels followers_count' do
    Rails.cache = ActiveSupport::Cache::MemoryStore.new
    Rails.cache.fetch("channel_#{@channel.id}_followers_count".to_sym) {3}
    assert_equal 3, Rails.cache.read("channel_#{@channel.id}_followers_count".to_sym)
    UserTransitions::Active.new(users: [@user], organization: @org).mark
    assert_nil Rails.cache.read("channel_#{@channel.id}_followers_count".to_sym)
  end

  test 'deletes the rails cache of channels followers_count, when user suspend from team' do
    @team_user.suspend!
    @team_user.reload

    Rails.cache = ActiveSupport::Cache::MemoryStore.new
    channel = ChannelBridge.new([@channel], user: @user, fields: "followers_count").attributes
    assert_equal channel[0][:followersCount], Rails.cache.read("channel_#{@channel.id}_followers_count".to_sym)
    UserTransitions::Active.new(users: [@team_user], organization: @org).mark
    assert_nil Rails.cache.read("channel_#{@channel.id}_followers_count".to_sym)
  end
end

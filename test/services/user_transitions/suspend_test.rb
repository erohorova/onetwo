require 'test_helper'

class UserTransitions::SuspendTest < ActiveSupport::TestCase
  setup do 

    @org  = create :organization
    @user = create :user, organization: @org, is_active: true, status: 'active', is_suspended: false
    @other_user = create :user, organization: @org

    # Entities with the interactions by the @user.
    # Step 1: Card created by @other_user and tracks the activity streams.
    @card = create :card, author: @other_user, organization: @org
    @card_stream = create :activity_stream, streamable: @card, user: @other_user,
      action: ActivityStream::ACTION_CREATED_CARD, organization: @org, card_id: @card.id

    @assignable_card = create :card, author: @other_user, organization: @org
    @assignable_stream = create :activity_stream, streamable: @assignable_card, user: @other_user,
      action: ActivityStream::ACTION_CREATED_CARD, organization: @org, card_id: @assignable_card.id

    @bookmarkable_card = create :card, author: @other_user, organization: @org
    @bookmarkable_stream =create :activity_stream, streamable: @bookmarkable_card, user: @other_user,
      action: ActivityStream::ACTION_CREATED_CARD, organization: @org, card_id: @bookmarkable_card.id

    @completable_card = create :card, author: @other_user, organization: @org
    @completable_stream = create :activity_stream, streamable: @completable_card, user: @other_user,
      action: ActivityStream::ACTION_CREATED_CARD, organization: @org, card_id: @completable_card.id

    # Step 2: @card upvoted by the @user.
    @vote = create :vote, votable: @card, voter: @user

    # Step 3: @card commented by the @user.
    @comment = create :comment, commentable: @card, user: @user

    # Step 4: @card commented by the @other_user and @user is tagged.
    @tagged_comment = create :comment, commentable: @card, user: @other_user, message: "Hello #{@user.handle}"
    @mention = create :mention, mentionable: @comment, user: @user

    # Step 5: Channel authored by the @other_user and is followed by @user.
    @channel = create :channel, organization: @org
    @channel.add_authors([@other_user])
    @channel.add_followers([@user])

    #Make team follow a channel
    @team_user = create :user, organization: @org
    @team = create :team, organization: @org
    @team.add_user(@team_user)
    @channel.followed_team_ids = [@team.id]

    # Step 6: @user follows the @other_user.
    @user_user_follow = Follow.create(followable: @other_user, user: @user)

    # Entities owned by the @user.
    # Step 7: Card authored by @user.
    @card2 = create :card, author_id: @user, organization: @org
    @authored_card_stream = create :activity_stream, streamable: @card2,
      user: @user, action: ActivityStream::ACTION_CREATED_CARD, organization: @org, card_id: @card2.id
    # Step 8: Channel authored by @user.
    @channel2 = create :channel, organization: @org
    @channel2.add_authors([@user])
    # Step 9: Bookmarked by @user.
    @bookmarked = create :bookmark, bookmarkable: @bookmarkable_card, user: @user
    # Step 10: Assigned to @user.
    @assigned = create :assignment, assignable: @assignable_card, assignee: @user
    @completed = create :user_content_completion, completable: @completable_card, user: @user

    # Step 11: Commented on activity stream.
    # commentable_stream = ActivityStream.find_by(streamable: @card)
    @commented_stream = create :comment, user: @user, commentable: @assignable_stream

    # Step 12: Voted the activity stream.
    @voted_stream = create :vote, user_id: @user.id, votable: @assignable_stream

    # Step 13: @user tags @other_user.
    other_user_tagged_stream = create :comment, user: @user, commentable: @commented_stream
    other_user_mention = create :mention, mentionable: other_user_tagged_stream, user: @other_user

    # Step 14: @other_user tags @user.
    self_tagged_stream = create :comment, user: @other_user, commentable: @commented_stream
    @self_user_mention = create :mention, mentionable: self_tagged_stream, user: @user
  end

  test 'marks the user as `suspend`' do
    assert @user.is_active
    refute @user.is_suspended
    assert_equal 'active', @user.status

    UserTransitions::Suspend.new(users: [@user], organization: @org).mark

    refute @user.is_active
    assert @user.is_suspended
    assert_equal 'suspended', @user.status
  end

  test 'soft delete votes given by the @user to the card' do
    refute @vote.reload.deleted?
    UserTransitions::Suspend.new(users: [@user], organization: @org).mark
    assert @vote.reload.deleted?
  end

  test 'soft delete comments added by @user to card' do 
    refute @comment.reload.deleted?
    UserTransitions::Suspend.new(users: [@user], organization: @org).mark
    assert @comment.reload.deleted?
  end

  test 'soft delete mentions that tags @user to the card by @other_user' do
    refute @mention.reload.deleted?

    UserTransitions::Suspend.new(users: [@user], organization: @org).mark
    assert @mention.reload.deleted?
  end

  test 'soft delete user-channel follows association' do
    follow = Follow.find_by(followable: @channel, user: @user)
    UserTransitions::Suspend.new(users: [@user], organization: @org).mark
    assert follow.reload.deleted?
  end

  test 'soft delete user-user follows association' do
    refute @user_user_follow.reload.deleted?
    UserTransitions::Suspend.new(users: [@user], organization: @org).mark
    assert @user_user_follow.reload.deleted?
  end

  test 'soft delete the activity streams' do
    refute @authored_card_stream.reload.deleted?

    UserTransitions::Suspend.new(users: [@user], organization: @org).mark

    assert @authored_card_stream.reload.deleted?
  end

  test 'soft delete the votes for an activity stream' do
    refute @voted_stream.reload.deleted?

    UserTransitions::Suspend.new(users: [@user], organization: @org).mark
    assert @voted_stream.reload.deleted?
  end

  test 'soft delete the comments for an activity stream' do
    refute @commented_stream.reload.deleted?
    UserTransitions::Suspend.new(users: [@user], organization: @org).mark
    assert @commented_stream.reload.deleted?
  end


  test 'soft delete the mentions that tags @user in an activity stream' do
    refute @self_user_mention.reload.deleted?
    UserTransitions::Suspend.new(users: [@user], organization: @org).mark
    assert @self_user_mention.reload.deleted?
  end

  test 'should not delete from people carousal in discover.' do
    structure = create(:structure, current_context: 'channel', current_slug: 'users', creator: @other_user)
    structure_item = create(:structured_item, structure: structure, entity: @user)
    UserTransitions::Suspend.new(users: [@user], organization: @org).mark
    assert StructuredItem.find_by_id(structure_item.id)
  end

  test 'should reset the votes_count in activity stream' do 
    before_count = @assignable_stream.votes_count
    
    UserTransitions::Suspend.new(users: [@user], organization: @org).mark

    @assignable_stream = @assignable_stream.reload
    after_count = @assignable_stream.votes_count
    assert_not_equal before_count,after_count 
    assert_equal @assignable_stream.votes.size, after_count
  end

  test 'should reset the votes_count in cards ' do 
    before_count = @card.votes_count
    
    UserTransitions::Suspend.new(users: [@user], organization: @org).mark

    @card = @card.reload
    after_count = @card.votes_count
    assert_not_equal before_count,after_count 
    assert_equal @card.votes.size, after_count
  end

  test 'should reset the comments_count in activity stream' do 
    before_count = @assignable_stream.comments_count
    
    UserTransitions::Suspend.new(users: [@user], organization: @org).mark

    @assignable_stream = @assignable_stream.reload
    after_count = @assignable_stream.comments_count
    assert_not_equal before_count,after_count 
    assert_equal @assignable_stream.comments.size, after_count
  end

  test 'deletes the rails cache of channels followers_count' do 
    Rails.cache = ActiveSupport::Cache::MemoryStore.new
    Rails.cache.fetch("channel_#{@channel.id}_followers_count".to_sym) {1}
    assert_equal 1, Rails.cache.read("channel_#{@channel.id}_followers_count".to_sym)

    UserTransitions::Suspend.new(users: [@user], organization: @org).mark

    assert_nil Rails.cache.read("channel_#{@channel.id}_followers_count".to_sym)
  end

  test 'deletes the rails cache of channels followers_count, when user suspend from team' do
    Rails.cache = ActiveSupport::Cache::MemoryStore.new
    channel = ChannelBridge.new([@channel], user: @user, fields: "followers_count").attributes
    assert_equal channel[0][:followersCount], Rails.cache.read("channel_#{@channel.id}_followers_count".to_sym)
    UserTransitions::Suspend.new(users: [@team_user], organization: @org).mark
    assert_nil Rails.cache.read("channel_#{@channel.id}_followers_count".to_sym)
  end

  test 'record the event as `user_suspended` to Influx' do
    Analytics::MetricsRecorderJob.expects(:perform_later).with(includes(
      :recorder, :event, :actor, :org, :suspended_user, :additional_data
    ))

    UserTransitions::Suspend.new(users: [@user], organization: @org).mark
  end

  test 'record the event as `card_comment_deleted` to Influx' do 
    Analytics::MetricsRecorderJob.expects(:perform_later).with(includes(
      :recorder, :comment,:card,:org, :event, :actor, :org, :timestamp, :additional_data
    ))
    UserTransitions::Suspend.new(users: [@user], organization: @org).mark
  end

  test 'record the event as `card_unliked` to Influx' do 
    Analytics::MetricsRecorderJob.expects(:perform_later).with(includes(
      :recorder,:card,:org, :event, :actor, :org, :timestamp, :additional_data
    ))
    UserTransitions::Suspend.new(users: [@user], organization: @org).mark
  end

  test 'record the event as `user_unfollowed` & `channel_unfollowed` to Influx' do

    Analytics::MetricsRecorderJob.expects(:perform_later).with(includes(
      :recorder,:followed,:follower,:org, :event, :actor, :timestamp, :additional_data
    ))
    UserTransitions::Suspend.new(users: [@user], organization: @org).mark  
  end

  test 'reindex the user' do
    @user.stubs(:reindex_async)

    UserTransitions::Suspend.new(users: [@user], organization: @org).mark
  end
end

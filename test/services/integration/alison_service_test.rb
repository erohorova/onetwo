require 'test_helper'
class Integrations::AlisonServiceTest < ActiveSupport::TestCase
  setup do
    @user = create :user
    @integration = create :integration, name: "Alison"
  end

  test "alison service creates courses" do
    courses_html = '<div class="panel-body body-certifications">
                      <div class="col-sm-4">
                        <a class="education-title" href="/courses/Customer-Support-Training"></a>
                      </div>
                      <div class="col-sm-4">
                        <a class="education-title" href="/courses/Customer-Support-Training12"></a>
                      </div>
                    </div>'
    courses = Nokogiri::HTML(courses_html)
    Integrations::AlisonService.any_instance.stubs(:get_courses_in_html).returns(courses)
    Resource.stubs(:get_resource_params).returns({title: "test1", description: "", image_url: ""})
    assert_difference ->{ExternalCourse.count}, 2 do
      Integrations::AlisonService.new(user_id: @user.id, external_uid: "3924199").scrape
      assert_equal @user.external_courses.count, 2
      assert_equal @user.users_integrations.count, 1
    end
    Integrations::AlisonService.any_instance.unstub(:get_courses_in_html)
  end

  test "Alison service with invalid user_id" do
    ExternalCourse.delete_all
    UsersIntegration.delete_all
    assert_equal ExternalCourse.count, 0
    courses_html = '<ul></ul>'
    courses = Nokogiri::HTML(courses_html)
    Integrations::AlisonService.any_instance.stubs(:get_courses_in_html).returns(courses)
    assert_no_difference ->{ExternalCourse.count} do
      courses = Integrations::AlisonService.new(user_id: @user.id, external_uid: "invalid").scrape
      assert_equal ExternalCourse.count, 0
      assert_equal @user.external_courses.count, 0
    end

    Integrations::AlisonService.any_instance.expects(:get_courses_in_html).raises(ArgumentError.new)
    assert_no_difference ->{ExternalCourse.count} do
      refute Integrations::AlisonService.new(user_id: @user.id, external_uid: "invalid").scrape
    end
  end
end

require 'test_helper'
class Integrations::SkillShareServiceTest < ActiveSupport::TestCase
  setup do
    @user = create :user
    @integration = create :integration, name: "Skill Share"
  end

  #hitting skill share public pages from tests for for test user
  test "Skill share service creates courses" do
    courses_html = '<div class="user-profile-section">
                <div class="enrolled-section">
                  <div>
                    <li>
                      <div class="class-row-inner-wrapper">
                        <div class="background-image-holder" style="background-image: url(https://static.skillshare.com/uploads/video/thumbnails/631b384aa5db94c8e7cf8f55faab3fca/448-252);">
                        </div>
                        <div class="class-info">
                          <p class="title-link"><a href="https://www.skillshare.com/classes/business/Getting-Started-with-Twitter-for-Business/501538509?via=user-profile">Getting Started with Twitter for Business</a></p>
                          <div class="user-information">
                            <p class="title">
                              <a href="https://www.skillshare.com/sandravee">Sandra Vega</a>
                            </p>
                          </div>
                        </div>
                      </div>
                    </li>
                  </div>
                </div>
              </div>'
    courses = Nokogiri::HTML(courses_html)
    Integrations::SkillShareService.any_instance.stubs(:get_courses_in_html).returns(courses)
    ExternalCourse.delete_all
    UsersIntegration.delete_all
    assert_equal ExternalCourse.count, 0
    assert_difference ->{ExternalCourse.count}, 1 do
      courses = Integrations::SkillShareService.new(user_id: @user.id, external_uid: "sunilsharma").scrape
      assert_equal ExternalCourse.count, 1
      assert_equal @user.external_courses.count, 1
    end
    Integrations::SkillShareService.any_instance.unstub(:get_courses_in_html)
  end

  test "skill share service with invalid public_id" do
    courses_html = '<div class="user-profile-section">
              </div>'
    courses = Nokogiri::HTML(courses_html)
    Integrations::SkillShareService.any_instance.stubs(:get_courses_in_html).returns(courses)
    ExternalCourse.delete_all
    UsersIntegration.delete_all
    assert_equal ExternalCourse.count, 0
    assert_no_difference ->{ExternalCourse.count} do
      courses = Integrations::SkillShareService.new(user_id: @user.id, external_uid: "invalid").scrape
      assert_equal ExternalCourse.count, 0
      assert_equal @user.external_courses.count, 0
    end
    Integrations::SkillShareService.any_instance.unstub(:get_courses_in_html)

    #exception
    Integrations::SkillShareService.any_instance.expects(:get_courses_in_html).raises(ArgumentError.new)
    assert_no_difference ->{ExternalCourse.count} do
      refute Integrations::SkillShareService.new(user_id: @user.id, external_uid: "invalid").create_courses
    end
  end
end

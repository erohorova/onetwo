require 'test_helper'
class Integrations::CodeSchoolServiceTest < ActiveSupport::TestCase
  setup do
    @user = create :user
    @integration = create :integration, name: "Code School"
  end

  test "code school service creates courses" do
    courses = {
                "completed" =>[
                  {
                    "title"=>"Rails for Zombies",
                    "url"=>"http://www.codeschool.com/courses/rails-for-zombies",
                  },
                  {
                    "title"=>"Ruby Bits",
                    "url"=>"http://www.codeschool.com/courses/ruby-bits",
                  },
                ],
                "in_progress"=>[
                  {
                    "title"=>"Discover DevTools",
                    "url"=>"http://www.codeschool.com/courses/discover-devtools",
                  }
                ]
              }
    Resource.stubs(:get_resource_params).returns({description: "test", image_url: nil})
    Integrations::CodeSchoolService.any_instance.stubs(:get_courses).returns(courses)
    ExternalCourse.delete_all
    assert_equal ExternalCourse.count, 0

    assert_difference ->{ExternalCourse.count}, 3 do
      Integrations::CodeSchoolService.new(user_id: @user.id, external_uid: "3924199").scrape
      assert_equal ExternalCourse.count, 3
      assert @user.external_courses.count, 3
    end
  end

  test "code service service with invalid username" do
    Integrations::CodeSchoolService.any_instance.expects(:get_courses).raises(ArgumentError.new)
    ExternalCourse.delete_all
    assert_equal ExternalCourse.count, 0
    assert_no_difference ->{ExternalCourse.count} do
      refute Integrations::CodeSchoolService.new(user_id: @user.id, external_uid: "invalid").create_courses
      assert ExternalCourse.count, 0
      assert @user.external_courses.count, 0
      assert @user.users_integrations.count, 0
    end
  end

  test 'should create or update users_integration' do
    scraper = Integrations::CodeSchoolService.new(user_id: @user.id, external_uid: "3924199")
    values = {public_user_name: "test"}.to_json
    assert_difference ->{UsersIntegration.count}, 1 do
      scraper.update_users_integration(@user.id, values)
      assert_equal UsersIntegration.last.field_values, values
    end

    #it won't create duplicate record
    values1 = {public_user_name: "test1"}.to_json
    assert_no_difference ->{UsersIntegration.count} do
      scraper.update_users_integration(@user.id, values1)
    end
  end
end

require 'test_helper'
class Integrations::PluralSightServiceTest < ActiveSupport::TestCase
  setup do
    @user = create :user
    @integration = create :integration, name: "Plural Sight"
  end

  test "Plural Sight service creates courses" do
    courses = [{"Course"=>{
                  "Id"=>"skype-business-server-core-admin",
                  "Title"=>"Core Administration - Skype for Business Server",
                  "Authors"=>[{"FirstName"=>"Desmond", "LastName"=>"Lee", "Handle"=>"desmond-lee"}]
                },
                "Progress"=>{"CourseId"=>"skype-business-server-core-admin", "PercentComplete"=>0.4969464734762304}
              }]
    Integrations::PluralSightService.any_instance.stubs(:get_courses).returns(courses)
    ExternalCourse.delete_all
    assert_equal ExternalCourse.count, 0

    assert_difference ->{ExternalCourse.count}, 1 do
      Integrations::PluralSightService.new(user_id: @user.id, external_uid: "3924199").scrape
      assert_equal ExternalCourse.count, 1
      assert @user.external_courses.count, 1
    end
  end

  test "Plural Sight service with invalid public_id" do
    Integrations::PluralSightService.any_instance.expects(:get_courses).raises(ArgumentError.new)
    ExternalCourse.delete_all
    assert_equal ExternalCourse.count, 0
    assert_no_difference ->{UsersIntegration.count} do
      refute Integrations::PluralSightService.new(user_id: @user.id, external_uid: "invalid").create_courses
      assert ExternalCourse.count, 0
      assert @user.external_courses.count, 0
      assert @user.users_integrations.count, 0
    end
  end

  test 'should create or update users_integration' do
    scraper = Integrations::PluralSightService.new(user_id: @user.id, external_uid: "3924199")
    values = {public_user_name: "test"}.to_json
    assert_difference ->{UsersIntegration.count}, 1 do
      scraper.update_users_integration(@user.id, values)
      assert_equal UsersIntegration.last.field_values, values
    end

    #it won't create duplicate record
    values1 = {public_user_name: "test1"}.to_json
    assert_no_difference ->{UsersIntegration.count} do
      scraper.update_users_integration(@user.id, values1)
    end
  end
end

require 'test_helper'
class Integrations::TreeHouseServiceTest < ActiveSupport::TestCase
  setup do
    @user = create :user
    @integration = create :integration, name: "Tree House"
  end

  test "tree_house service creates courses" do
    courses = [{
                "name"=>"Newbie",
                "url"=>"https://teamtreehouse.com/sunilsharma3",
                "icon_url"=>"https://achievement-images.teamtreehouse.com/Generic_Newbie.png",
                "earned_date"=>"2016-07-18T05:37:54.000Z", "courses"=>[]
                },
                {
                  "name"=>"Python For Beginners",
                  "url"=>"https://teamtreehouse.com/library/python-basics/python-for-beginners",
                  "icon_url"=>"https://achievement-images.teamtreehouse.com/badges_python_basics_stage01.png",
                  "courses"=>[
                    {
                      "title"=>"Python Basics",
                      "url"=>"https://teamtreehouse.com/library/python-basics"
                    },
                    {
                      "title"=>"Python for Beginners",
                      "url"=>"https://teamtreehouse.com/library/python-basics/python-for-beginners"
                    }
                  ]
                },
                {
                  "name"=>"Getting the REST You Need",
                  "url"=>"https://teamtreehouse.com/library/rest-api-basics/getting-the-rest-you-need",
                  "icon_url"=>"https://achievement-images.teamtreehouse.com/badges_development-tools_rest-api-basics_stage01.png",
                  "courses"=>[
                    {
                      "title"=>"REST API Basics",
                      "url"=>"https://teamtreehouse.com/library/rest-api-basics",
                    },
                    {
                      "title"=>"Getting the REST You Need",
                      "url"=>"https://teamtreehouse.com/library/rest-api-basics/getting-the-rest-you-need"
                    }
                  ]
                }
              ]

    Integrations::TreeHouseService.any_instance.stubs(:get_courses).returns(courses)
    ExternalCourse.delete_all
    assert_equal ExternalCourse.count, 0
    assert_difference ->{ExternalCourse.count}, 4 do
      Integrations::TreeHouseService.new(user_id: @user.id, external_uid: "sunilsharma").scrape
      assert @user.external_courses.count, 1
    end

  end

  test "tree_house service with invalid public_id" do
    Integrations::TreeHouseService.any_instance.expects(:get_courses).raises(ArgumentError.new)
    ExternalCourse.delete_all
    assert_equal ExternalCourse.count, 0
    assert_no_difference -> {ExternalCourse.count} do
      refute Integrations::TreeHouseService.new(user_id: @user.id, external_uid: "invalid").create_courses
      assert @user.external_courses.count, 0
      assert @user.users_integrations.count, 0
    end
  end

  test 'should create or update users_integration' do
    scraper = Integrations::TreeHouseService.new(user_id: @user.id, external_uid: "3924199")
    values = {public_user_name: "test"}.to_json
    assert_difference ->{UsersIntegration.count}, 1 do
      scraper.update_users_integration(@user.id, values)
      assert_equal UsersIntegration.last.field_values, values
    end

    #it won't create duplicate record
    values1 = {public_user_name: "test1"}.to_json
    assert_no_difference ->{UsersIntegration.count} do
      scraper.update_users_integration(@user.id, values1)
    end
  end
end

require 'test_helper'
class Integrations::FutureLearnServiceTest < ActiveSupport::TestCase
  setup do
    @user = create :user
    @integration = create :integration, name: "Future Learn"
  end


  #hitting future learn public pages from tests for for test user
  test "future learn service creates courses" do
    courses_html = '<ul>
                  <li class="o-profile-courses__run">
                    <div>
                      <a class="o-profile-courses__run-title" href="/courses/big-data-visualisation">Big Data: Data Visualisation</a>
                    </div>
                  </li>
                  <li class="o-profile-courses__run">
                    <div>
                      <a class="o-profile-courses__run-title" href="/courses/royal-food">A History of Royal Food and Feasting</a>
                    </div>
                  </li>
                </ul>'
    courses = Nokogiri::HTML(courses_html)
    Integrations::FutureLearnService.any_instance.stubs(:get_courses_in_html).returns(courses)
    Resource.stubs(:get_resource_params).returns({title: "test1", description: "", image_url: ""})
    ExternalCourse.delete_all
    UsersIntegration.delete_all
    assert_equal ExternalCourse.count, 0
    assert_difference ->{ExternalCourse.count}, 2 do
      Integrations::FutureLearnService.new(user_id: @user.id, external_uid: "3924199").scrape
      assert_equal ExternalCourse.count, 2
      assert_equal @user.external_courses.count, 2
      assert_equal @user.users_integrations.count, 1
    end
    Integrations::FutureLearnService.any_instance.unstub(:get_courses_in_html)
  end

  test "future learn service with invalid public_id" do
    ExternalCourse.delete_all
    UsersIntegration.delete_all
    assert_equal ExternalCourse.count, 0
    courses_html = '<ul></ul>'
    courses = Nokogiri::HTML(courses_html)
    Integrations::FutureLearnService.any_instance.stubs(:get_courses_in_html).returns(courses)
    assert_no_difference ->{ExternalCourse.count} do
      courses = Integrations::FutureLearnService.new(user_id: @user.id, external_uid: "invalid").scrape
      assert_equal ExternalCourse.count, 0
      assert_equal @user.external_courses.count, 0
    end

    Integrations::FutureLearnService.any_instance.expects(:get_courses_in_html).raises(ArgumentError.new)
    assert_no_difference ->{ExternalCourse.count} do
      refute Integrations::FutureLearnService.new(user_id: @user.id, external_uid: "invalid").scrape
    end
  end
end

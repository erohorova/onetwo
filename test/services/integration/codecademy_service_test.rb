require 'test_helper'
class Integrations::CodecademyServiceTest < ActiveSupport::TestCase
  setup do
    @user = create :user
    @integration = create :integration, name: "Codecademy"
  end

  test "should create user interests" do
    skills_html = '<div id="completed-body">
                <div class="completed">
                  <a class="link--target" href="test">Test1</a>
                  <a class="link--target" href="test2">Test1</a>
                </div>
              </div>'
    skills = Nokogiri::HTML(skills_html)
    Integrations::CodecademyService.any_instance.stubs(:get_skills_in_html).returns(skills)
    Resource.stubs(:get_resource_params).returns({title: "test1", description: "", image_url: ""})
    # Interest.delete_all
    # UsersIntegration.delete_all
    # assert_equal Interest.count, 0
    assert_difference ->{ExternalCourse.count}, 2 do
      Integrations::CodecademyService.new(user_id: @user.id, external_uid: "sunilsharma").scrape
      assert_equal @user.external_courses.count, 2
      assert_equal @user.users_integrations.count, 1
    end
    Integrations::CodecademyService.any_instance.unstub(:get_skills_in_html)
  end

  test "should not create interest with invalid public_id" do
    skills_html = '<div>
              </div>'
    skiils = Nokogiri::HTML(skills_html)
    Integrations::CodecademyService.any_instance.stubs(:get_skills_in_html).returns(skiils)
    ExternalCourse.delete_all
    UsersIntegration.delete_all
    assert_equal ExternalCourse.count, 0
    assert_no_difference ->{ExternalCourse.count} do
      Integrations::CodecademyService.new(user_id: @user.id, external_uid: "invalid").scrape
      assert_equal ExternalCourse.count, 0
      assert_equal @user.external_courses.count, 0
    end
    Integrations::CodecademyService.any_instance.unstub(:get_skills_in_html)

    #exception
    Integrations::CodecademyService.any_instance.expects(:get_skills_in_html).raises(ArgumentError.new)
    assert_no_difference ->{ExternalCourse.count} do
      refute Integrations::CodecademyService.new(user_id: @user.id, external_uid: "invalid").scrape
    end
  end
end

require 'test_helper'
class Integrations::UdemyServiceTest < ActiveSupport::TestCase
  self.use_transactional_fixtures = false
  setup do
    WebMock.disable!
    @user = create :user
    @integration = create :integration, name: "Udemy"
  end

  teardown do
    WebMock.enable!
  end

  #hitting udemy public pages from tests for for test user
  test "udemy service creates courses" do
    courses = {"results" => [{"title"=>"Adobe Illustrator: Mastering the Fundamentals",
                "url"=>"/adobe-illustrator-training/",
                "visible_instructors"=>[{
                  "title"=>"School of Game Design .", "name"=>"School of Game Design",
                }],
                "image_240x135"=>"https://udemy-images.udemy.com/course/240x135/65306_a9c7_4.jpg",
                "published_title"=>"adobe-illustrator-training"
              }]}
    Integrations::UdemyService.any_instance.stubs(:get_user_id).returns(123)
    Integrations::UdemyService.any_instance.stubs(:get_courses).returns(courses)
    assert_difference ->{ExternalCourse.count}, 1 do
      Integrations::UdemyService.new(user_id: @user.id, external_uid: "shemal").scrape
      assert @user.external_courses.count > 0
      assert @user.users_integrations.count > 0
    end
  end

  test "udemy service with invalid public_id" do
    # Integrations::UdemyService.any_instance.stubs(:get_user_id).returns(123)
    Integrations::UdemyService.any_instance.stubs(:get_user_id).returns(nil)
    assert_no_difference ->{ExternalCourse.count} do
      courses = Integrations::UdemyService.new(user_id: @user.id, external_uid: "invalid").scrape
    end
  end
end

require 'test_helper'
class Integrations::CourseraServiceTest < ActiveSupport::TestCase
  setup do
    WebMock.disable!
    @user = create :user
    @integration = create :integration, name: "Coursera"
    ExternalCourse.delete_all
  end

  teardown do
    WebMock.enable!
  end

  test "coursera service creates courses" do
    assert_equal ExternalCourse.count, 0
    courses = [{"courses" =>[{"status"=> 1}], "name" => "test", "short_name" => "test", "short_description" => "test dsc" }]
    Integrations::CourseraService.any_instance.stubs(:get_user_coursera_user_id).returns(18450707)
    Integrations::CourseraService.any_instance.stubs(:get_courses).returns(courses)

    assert_difference -> {ExternalCourse.count} do
      Integrations::CourseraService.new(user_id: @user.id, external_uid: "ac7e10e2a8cf5db6c0ecf3a5ea60ad87").scrape
      assert @user.external_courses.count, 1
      assert @user.users_integrations.count, 1
    end
  end

  test "#get_user_coursera_user_id" do
    Integrations::CourseraService.any_instance.stubs(:get_user_coursera_user_id).returns(18450707)
    Integrations::CourseraService.any_instance.stubs(:get_courses).raises(ArgumentError.new)
    assert_equal ExternalCourse.count, 0
    assert_no_difference ->{UsersIntegration.count} do
      coursera_id = Integrations::CourseraService.new(user_id: @user.id, external_uid: "ac7e10e2a8cf5db6c0ecf3a5ea60ad87").get_user_coursera_user_id
      assert_equal coursera_id, 18450707 #test account
    end
  end
end

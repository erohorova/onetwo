require 'test_helper'
require 'json'

class CardConnectorTest < ActiveSupport::TestCase
  include RequestHelperMethods
  include CardHelperMethods

  setup do
    @org = create(:organization)
    @user_id = create(:user, organization_id: @org.id).id
    @title = "some title"
    @message = "some message"
    @payload = {
      "name" => @title,
      "description" => @message,
      "user_id" => @user_id,
      "user_taxonomies" => []
    }
  end

  test 'propagation to ecl' do
    tags = create_list(:tag, 2)
    url = "http://someurl.com"
    image_url = "http://someurl.png"
    resource = create(:resource, url: url, image_url: image_url)

    card = create(:card, title: @title, message: @message, author_id: @user_id, organization_id: @org.id, resource: resource, card_type: 'media', card_subtype: 'link')
    card.tags <<  tags

    card_rating = create(:cards_rating, user_id: @user_id, rating: 3, card: card, level: 'advanced')

    card_metadata = create :card_metadatum, card: card, level: 'advanced', average_rating: 3, plan: 'paid'

    payload = @payload.merge({
      "url" => url,
      "source_id" => Settings.ecl.ugc_source_id,
      "content_type" => "article",
      "tags" => tags.map{|tag| {"tag" => tag.name, "tag_type" => "keyword", "source" => "ugc", "strength" => 1.0}},
      "external_id" => card.id,
      "level" => "advanced",
      "rating" => 3,
      "resource_metadata" => {"title" => resource.title, "description" => resource.description, "embed_html" => resource.embed_html, "images" => [{"url" => resource.image_url}]},
      "status" => "active",
      "is_paid" => true,
      "plan" => "paid"
    })

    ecl_id = "rerf-rfde"
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items").
      with(:body => hash_including(payload)).to_return(:status => 200, :body => {"data" => {"id" => ecl_id, "level" => 'advanced', "rating" => 3}}.to_json)
    EclApi::CardConnector.new.propagate_card_to_ecl(card.id)
    # Ensure card is updated with ecl id
    assert_equal ecl_id, card.reload.ecl_id
    assert_equal 'advanced', card_rating.level
    assert_equal 3, card_rating.rating
  end

  test 'propagation to ecl when is_paid is true but card_metadatum is not present' do
    tags = create_list(:tag, 2)
    url = "http://someurl.com"
    image_url = "http://someurl.png"
    resource = create(:resource, url: url, image_url: image_url)

    card = create(:card, title: @title, message: @message, author_id: @user_id, organization_id: @org.id, resource: resource,
             card_type: 'media', card_subtype: 'link', is_paid: true)
    card.tags <<  tags

    payload = @payload.merge({
      "url" => url,
      "source_id" => Settings.ecl.ugc_source_id,
      "content_type" => "article",
      "tags" => tags.map{|tag| {"tag" => tag.name, "tag_type" => "keyword", "source" => "ugc", "strength" => 1.0}},
      "external_id" => card.id,
      "resource_metadata" => {"title" => resource.title, "description" => resource.description, "embed_html" => resource.embed_html, "images" => [{"url" => resource.image_url}]},
      "status" => "active",
      "plan" => "paid",
      "is_paid" => true
    })

    ecl_id = "rerf-rfde"
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items").
      with(:body => hash_including(payload)).to_return(:status => 200, :body => {"data" => {"id" => ecl_id}}.to_json)
    EclApi::CardConnector.new.propagate_card_to_ecl(card.id)
    # Ensure card is updated with ecl id
    assert_equal ecl_id, card.reload.ecl_id
  end

  test 'propagation to ecl when cards_rating is null' do
    tags = create_list(:tag, 2)
    url = "http://someurl.com"
    image_url = "http://someurl.png"
    resource = create(:resource, url: url, image_url: image_url)

    card = create(:card, title: @title, message: @message, author_id: @user_id, organization_id: @org.id, resource: resource, card_type: 'media', card_subtype: 'link')
    card.tags <<  tags

    card_rating = create(:cards_rating, user_id: @user_id, rating: nil, card: card, level: 'advanced')

    card_metadata = create :card_metadatum, card: card, level: 'advanced'

    payload = @payload.merge({
      "url" => url,
      "source_id" => Settings.ecl.ugc_source_id,
      "content_type" => "article",
      "tags" => tags.map{|tag| {"tag" => tag.name, "tag_type" => "keyword", "source" => "ugc", "strength" => 1.0}},
      "external_id" => card.id,
      "level" => "advanced",
      "rating" => 0,
      "resource_metadata" => {"title" => resource.title, "description" => resource.description, "embed_html" => resource.embed_html, "images" => [{"url" => resource.image_url}]}
    })

    ecl_id = "rerf-rfde"
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items").
      with(:body => hash_including(payload)).to_return(:status => 200, :body => {"data" => {"id" => ecl_id, "level" => 'advanced', "rating" => 0}}.to_json)
    EclApi::CardConnector.new.propagate_card_to_ecl(card.id)
    # Ensure card is updated with ecl id
    assert_equal ecl_id, card.reload.ecl_id
    assert_equal 'advanced', card_rating.level
    assert_equal 0, payload['rating']
  end

  test "propagate pathway with cards data" do
    url = "http://someurl.com"
    image_url = "http://someurl.png"
    resource = create(:resource, url: url, image_url: image_url)
    pack_card = create(:card, title: @title, message: @message, card_type: "pack", organization_id: @org.id, author_id: @user_id)
    card = create(:card, title: @title, message: @message, author_id: @user_id, organization_id: @org.id, resource: resource, card_type: 'media', card_subtype: 'link')

    CardPackRelation.add(cover_id: pack_card.id, add_id: card.id, add_type: 'Card')

    pack_card.publish!

    payload = {"name"=>"some title",
     "description"=>"some message",
     "url"=>nil,
     "user_id"=>@user_id,
     "source_id" => Settings.ecl.ugc_source_id,
     "tags"=>[],
     "user_taxonomies"=>[],
     "hidden"=>false,
     "external_id"=> pack_card.id,
     "content_type"=>"pathway",
     "pack_items"=>["some message", "some title"],
     "access_restricted_in_orgs"=>[],
     "groups"=>[],
     "channels"=>[],
     "users_with_access"=>[]
   }
    ecl_id = "rerf-rfde"
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items").
      with(:body => hash_including(payload)).to_return(:status => 200, :body => {"data" => {"id" => ecl_id}}.to_json)

    EclApi::CardConnector.new.propagate_card_to_ecl(pack_card.id)
    # Ensure card is updated with ecl id
    assert_equal ecl_id, pack_card.reload.ecl_id
  end

  test 'ecl_id should be updated on publishing a pathway which is free and paid' do
    ecl_id = "ecl-card"
    pack_card = create(:card, message: 'pathway cover', card_type: 'pack', author_id: @user_id, title: 'pack cover')
    card1 = create(:card,  organization_id: @org.id, author_id: @user_id, card_type: 'media', message: 'card1', is_paid: true)
    card2 = create(:card,  organization_id: @org.id, author_id: @user_id, card_type: 'media', message: 'card2', is_paid: false)
    create(:card_metadatum, plan: 'paid', card_id: card1.id)
    [card1, card2].each do |card|
      CardPackRelation.add(cover_id: pack_card.id, add_id: card.id, add_type: 'Card')
    end
    pack_card.publish
    pack_card.find_pack_journey_and_update

    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items").
      to_return(status: 200, body: get_ecl_card_response(ecl_id).to_json)

    EclApi::CardConnector.new.propagate_card_to_ecl(pack_card.id)

    pack_card.reload
    assert_equal ecl_id, pack_card.ecl_id
  end

  test 'propogate non-ugc card on update with tags' do
    tags = create_list(:tag, 2)
    user = create(:user, organization: @org)
    url = "http://someurl.com"
    image_url = "http://someurl.png"
    ecl_id = "rerf-rfde"

    resource = create(:resource, url: url, image_url: image_url)
    ecl_metadata = {'source_id' => "abcdef"}

    card = create(:card, title: @title, message: @message, author_id: nil, organization_id: @org.id, resource: resource, card_type: 'media', card_subtype: 'link', ecl_id: ecl_id, ecl_metadata: ecl_metadata)
    card.tags <<  tags
    card.save

    payload = @payload.merge({
      "url" => url,
      "user_id" => nil,
      "source_id" => ecl_metadata["source_id"],
      "tags" => tags.map{|tag| {"tag" => tag.name, "tag_type" => "keyword", "source" => "ugc", "strength" => 1.0}},
      "ecl_id" => ecl_id,
      "resource_metadata" => {"title" => resource.title, "description" => resource.description, "embed_html" => resource.embed_html, "images" => [{"url" => resource.image_url}]}
    })

    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items").
      with(:body => hash_including(payload)).to_return(:status => 200, :body => {"data" => {"id" => ecl_id}}.to_json)
    EclApi::CardConnector.new.propagate_card_to_ecl(card.id)
    # Ensure card is updated with ecl id
    assert_equal ecl_id, card.reload.ecl_id
  end

  test 'update card' do
    ecl_id = "001ac01d-b272-4a93-8c2d-f61bc93c7125"

    existing_card = create(:card, author_id: nil, organization_id: @org.id, ecl_id: ecl_id)

    ecl_content_item_data = {"id"=>"001ac01d-b272-4a93-8c2d-f61bc93c7125", "name"=>"Travel Photography: Costa Rica",
                              "description"=>"Join Rich Harrington and family", "summary"=>nil,
                              "url"=>"https://www.lynda.com/Photography-Cameras-Gear-tutorials/Travel-Photography-Costa-Rica/163608-2.html",
                              "external_id"=>"163608", "author"=>"Richard Harrington", "source_id"=>"22364fac-d40a-4915-a2da-1bd7a0fe2b1d",
                              "source_type_id"=>"2cdc12d5-75b4-4e2d-a10a-94e936ca1dc7", "source_type_name"=>"lynda", "content_type"=>"article",
                              "organization_id"=>15, "duration_metadata"=>nil, "resource_metadata"=>nil, "additional_metadata"=>nil,
                              "created_at"=>"2017-01-06T15:31:18.286Z", "updated_at"=>"2017-01-06T15:31:18.286Z", "user_id"=>nil,
                              "source_logo_url" => "http://www.abc.com/xyz.png",
                              "source_display_name"=>nil, "taxonomies_list"=>nil, "taxonomies"=>nil, "nlp_metadata"=>nil,
                              "tags"=>[{"tag"=>"Photography", "source"=>"native", "tag_type"=>"keyword"}], "score"=>nil,
                              "path"=>"api/web_hook/v1/cards/refetch", "application"=>{"id"=>"001ac01d-b272-4a93-8c2d-f61bc93c7125",
                                "name"=>"Travel Photography: Costa Rica", "description"=>"Join Rich ", "summary"=>nil,
                                "url"=>"https://www.lynda.com/Photography-Cameras-Gear-tutorials/Travel-Photography-Costa-Rica/163608-2.html",
                                "external_id"=>"163608", "author"=>"Richard Harrington", "source_id"=>"22364fac-d40a-4915-a2da-1bd7a0fe2b1d",
                                "source_type_id"=>"2cdc12d5-75b4-4e2d-a10a-94e936ca1dc7", "source_type_name"=>"lynda", "content_type"=>"article",
                                "organization_id"=>15, "duration_metadata"=>nil, "resource_metadata"=>nil, "additional_metadata"=>nil,
                                "created_at"=>"2017-01-06T15:31:18.286Z", "updated_at"=>"2017-01-06T15:31:18.286Z", "user_id"=>nil, "source_display_name"=>nil,
                                "taxonomies_list"=>nil, "taxonomies"=>nil, "nlp_metadata"=>nil,
                                "tags"=>[{"tag"=>"Photography", "source"=>"native", "tag_type"=>"keyword"}], "score"=>nil
                              }
                            }

    EclApi::CardConnector.new(ecl_id: ecl_id, organization_id: @org.id).update_card(existing_card, ecl_content_item_data)
    existing_card.reload

    assert_equal ecl_content_item_data["source_id"], existing_card.ecl_metadata['source_id']
    assert_equal ecl_content_item_data["source_logo_url"], existing_card.ecl_metadata['source_logo_url']
  end


  test 'should update the cpe subject area and cpe credits to cards ecl_metadata column' do
    ecl_id = "001ac01d-b272-4a93-8c2d-f61bc93c7125"

    existing_card = create(:card, author_id: nil, organization_id: @org.id, ecl_id: ecl_id)

    ecl_content_item_data = {"id"=>"001ac01d-b272-4a93-8c2d-f61bc93c7125", "name"=>"Travel Photography: Costa Rica",
                              "description"=>"Join Rich Harrington and family", "summary"=>nil,
                              "url"=>"https://www.lynda.com/Photography-Cameras-Gear-tutorials/Travel-Photography-Costa-Rica/163608-2.html",
                              "external_id"=>"163608", "author"=>"Richard Harrington", "source_id"=>"22364fac-d40a-4915-a2da-1bd7a0fe2b1d",
                              "source_type_id"=>"2cdc12d5-75b4-4e2d-a10a-94e936ca1dc7", "source_type_name"=>"lynda", "content_type"=>"article",
                              "organization_id"=>15, "duration_metadata"=>nil, "resource_metadata"=>nil, "additional_metadata"=>nil,
                              "created_at"=>"2017-01-06T15:31:18.286Z", "updated_at"=>"2017-01-06T15:31:18.286Z", "user_id"=>nil,
                              "source_logo_url" => "http://www.abc.com/xyz.png",
                              "source_display_name"=>nil, "taxonomies_list"=>nil, "taxonomies"=>nil, "nlp_metadata"=>nil,
                              "tags"=>[{"tag"=>"Photography", "source"=>"native", "tag_type"=>"keyword"}], "score"=>nil,
                              "additional_metadata" => {
                                "cpe_credits": "100",
                                "cpe_subject": "Work Ethics"
                              }
                            }

    EclApi::CardConnector.new(ecl_id: ecl_id, organization_id: @org.id).update_card(existing_card, ecl_content_item_data)
    existing_card.reload

    assert_equal ecl_content_item_data["additional_metadata"]["cpe_subject"], existing_card.ecl_metadata['cpe_subject']
    assert_equal ecl_content_item_data["additional_metadata"]["cpe_credits"], existing_card.ecl_metadata['cpe_credits']

  end

  test 'update_card_change_from_ecl' do
    url = "http://someurl.com"
    image_url = "http://someurl.png"
    ecl_id = "001ac01d-b272-4a93-8c2d-f61bc93c7129"
    amount, currency = '2', 'INR'

    resource = create(:resource, url: url, image_url: image_url)
    existing_card = create(:card, author_id: nil, organization_id: @org.id, ecl_id: ecl_id, resource: resource, language: 'en')

    create(:price, card: existing_card)
    ecl_content_item_data = {
      'ecl_id' => ecl_id,
      'name'=>'Travel Photography: Costa Rica',
      'description' => 'Join Rich Harrington and family',
      'resource_metadata' => {
        'title'=>'Travel Photography: Costa Rica',
        'description' => 'Join Rich Harrington and family',
      },
      'url' => 'http://www.abc.com',
      'source_display_name' => 'rss',
      'prices_data' => [{'amount' => amount, 'currency' => currency}],
      'readable_card_type' => 'document',
      'published_at' => "Thu, 02 Aug 2018 11:43:18 UTC +00:00",
      additional_metadata: {languages: [{Name: "ItaLIAN"}]},
      'provider_name' => 'Provider Test'
    }
    ecl_content_item_data = ActionController::Parameters.new(ecl_content_item_data)
    EclApi::CardConnector.new(ecl_id: ecl_id, organization_id: @org.id).update_card_change_from_ecl(existing_card, ecl_content_item_data)
    existing_card.reload

    assert_equal DateTime.parse(ecl_content_item_data['published_at']), existing_card.published_at
    assert_equal Card.readable_card_type('document'), existing_card.readable_card_type
    assert_equal ecl_content_item_data['source_display_name'], existing_card.ecl_source_name
    assert_equal ecl_content_item_data['name'], existing_card.message
    assert_equal 'http://www.abc.com', resource.reload.url
    assert_equal ecl_content_item_data['name'], resource.title
    assert_equal ecl_content_item_data['description'], resource.description
    assert_equal amount.to_f, existing_card.prices.find_by(currency: currency).amount
    assert_equal 'it',existing_card.language
    assert_equal 'Provider Test', existing_card.provider
    assert existing_card.is_paid

    ecl_content_item_data[:prices_data].first[:amount] = nil
    EclApi::CardConnector.new(ecl_id: ecl_id, organization_id: @org.id).update_card_change_from_ecl(existing_card, ecl_content_item_data)
    existing_card.reload
    assert_equal amount.to_f, existing_card.prices.find_by(currency: currency).amount
  end

  test 'update_card_change_from_ecl to make team associations and privacy changes' do
    team = create(:team, organization_id: @org.id)

    url = "http://someurl.com"
    image_url = "http://someurl.png"
    ecl_id = "001ac01d-b272-4a93-8c2d-f61bc93c7129"
    amount, currency = '2', 'INR'

    resource = create(:resource, url: url, image_url: image_url)
    existing_card = create(:card, author_id: nil, organization_id: @org.id, ecl_id: ecl_id, resource: resource)

    ecl_content_item_data = {
      'ecl_id' => ecl_id,
      'groups' => [{'group_id' => team.id, 'org_id' => @org.id}],
      'access_restricted_in_orgs' => [@org.id]
    }

    EclApi::CardConnector.new(ecl_id: ecl_id, organization_id: @org.id).update_card_change_from_ecl(existing_card, ecl_content_item_data)
    existing_card.reload

    refute existing_card.is_public
    assert_includes team.shared_card_ids, existing_card.id
  end

  test 'retrieve_existing_card_based_on_ecl_id' do
    ecl_id = "0001e8d0-a6f1-4332-b52d-3f9e6a03ec58"
    existing_card = create(:card, author_id: nil, organization_id: @org.id, ecl_id: ecl_id)

    ecl_response = get_ecl_card_response(ecl_id)
    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id}").
      to_return(:status => 200, :body => ecl_response.to_json)

    card = EclApi::CardConnector.new(ecl_id: ecl_id, organization_id: @org.id).card_from_ecl_id
    assert_equal card, existing_card
  end

  test 'create_card_from_ecl' do
    ecl_id = "0001e8d0-a6f1-4332-b52d-3f9e6a03ec59"
    org = create(:organization)
    content_html = %Q{
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" >
<head>
    <title>Listening to music while working is terrible for productivity - Business Insider</title>
</head>
<body class="responsive ">This is a test content body</body>
</html>
    }

    ecl_response = get_ecl_card_response(ecl_id)
    ecl_response["data"]["published_at"] = "2018-04-21 00:00:00"

    content_url = ecl_response["data"]["url"]

    stub_request(:get, content_url).
      to_return(:status => 200, :body => content_html)

    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id}").
      to_return(:status => 200, :body => ecl_response.to_json)

    assert_difference 'Card.count', +1 do
      card = EclApi::CardConnector.new(ecl_id: ecl_id, organization_id: @org.id).card_from_ecl_id
      assert_equal ecl_id, card.ecl_id
      assert_equal "en", card.language
      assert_equal @org.id, card.organization_id
      assert_equal ecl_response["data"]["name"], card.message
      assert_equal Set.new(ecl_response["data"]["tags"].map{|t| t['tag']}), Set.new(card.tags.map(&:name))
      assert_equal ecl_response["data"]["url"], card.resource.url
      assert_equal ecl_response["data"]["taxonomies_list"], card.taxonomy_topics
      assert_equal ecl_response["data"]["external_id"], card.ecl_metadata['external_id']
      assert_equal ecl_response["data"]["content_type"], Card.readable_card_type_name(card.readable_card_type)
      assert_equal DateTime.parse(ecl_response["data"]["published_at"]), card.published_at
      assert_equal "Provider Test", card.provider
    end
  end

  test 'create_card_from_ecl_v3_with_groups' do
    ecl_id = "3087222c-28bd-4536-94ba-011843f78c6a"
    org = create(:organization)
    content_html = %Q{
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" >
<head>
    <title>Listening to music while working is terrible for productivity - Business Insider</title>
</head>
<body class="responsive ">This is a test content body</body>
</html>
    }

    ecl_response = get_ecl_card_response_v3(ecl_id)
    ecl_response["data"]["published_at"] = "2018-04-21 00:00:00"

    team = create(:team, organization_id: org.id)
    ecl_response["data"]["groups"] = [team.id]

    content_url = ecl_response["data"]["url"]

    stub_request(:get, content_url).
      to_return(:status => 200, :body => content_html)

    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id}").
      to_return(:status => 200, :body => ecl_response.to_json)

    assert_difference 'Card.count', +1 do
      card = EclApi::CardConnector.new(ecl_id: ecl_id, organization_id: @org.id).card_from_ecl_id
      assert_equal ecl_id, card.ecl_id
      assert_equal "en", card.language
      assert_equal @org.id, card.organization_id
      assert_equal ecl_response["data"]["name"], card.message
      assert_equal Set.new(ecl_response["data"]["tags"].map{|t| t['tag']}), Set.new(card.tags.map(&:name))
      assert_equal ecl_response["data"]["url"], card.resource.url
      assert_equal DateTime.parse(ecl_response["data"]["published_at"]), card.published_at
      assert_equal card.shared_with_team_ids, ecl_response["data"]["groups"]
      assert_equal "Provider Test", card.provider
    end
  end

  test 'create card with privacy and group associations from ecl' do
    ecl_id = "0001e8d0-a6f1-4332-b52d-3f9e6a03ec59"

    org = create(:organization)

    content_html = %Q{
    <!DOCTYPE html>
    <html lang="en" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" >
    <head>
        <title>Listening to music while working is terrible for productivity - Business Insider</title>
    </head>
    <body class="responsive ">This is a test content body</body>
    </html>
        }

    team = create(:team, organization_id: org.id)
    ecl_response = get_ecl_card_response(ecl_id)
    ecl_response["data"]["access_restricted_in_orgs"] = [org.id]
    ecl_response["data"]["groups"] = [{"org_id": org.id, "group_id": team.id}]

    content_url = ecl_response["data"]["url"]

    stub_request(:get, content_url).
      to_return(:status => 200, :body => content_html)

    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id}").
      to_return(:status => 200, :body => ecl_response.to_json)

    card = EclApi::CardConnector.new(ecl_id: ecl_id, organization_id: org.id).card_from_ecl_id
    assert_equal ecl_id, card.ecl_id
    assert_equal "en", card.language
    assert_equal org.id, card.organization_id
    refute card.is_public
    assert_includes team.shared_card_ids, card.id
  end

  test 'create card with privacy and group associations from ecl if private_content is true and access_restricted_in_orgs is missing' do
    ecl_id = "0001e8d0-a6f1-4332-b52d-3f9e6a03ec59"

    org = create(:organization)

    content_html = %Q{
    <!DOCTYPE html>
    <html lang="en" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" >
    <head>
        <title>Listening to music while working is terrible for productivity - Business Insider</title>
    </head>
    <body class="responsive ">This is a test content body</body>
    </html>
        }

    team = create(:team, organization_id: org.id)
    ecl_response = get_ecl_card_response(ecl_id)
    ecl_response["data"]["private_content"] = true
    ecl_response["data"]["groups"] = [{"org_id": org.id, "group_id": team.id}]

    content_url = ecl_response["data"]["url"]

    stub_request(:get, content_url).
      to_return(:status => 200, :body => content_html)

    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id}").
      to_return(:status => 200, :body => ecl_response.to_json)

    card = EclApi::CardConnector.new(ecl_id: ecl_id, organization_id: org.id).card_from_ecl_id
    assert_equal ecl_id, card.ecl_id
    assert_equal "en", card.language
    assert_equal org.id, card.organization_id
    refute card.is_public
    assert_includes team.shared_card_ids, card.id
  end

  test 'create card as public from ecl if private_content is false and access_restricted_in_orgs is empty' do
    ecl_id = "0001e8d0-a6f1-4332-b52d-3f9e6a03ec59"

    org = create(:organization)

    content_html = %Q{
    <!DOCTYPE html>
    <html lang="en" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" >
    <head>
        <title>Listening to music while working is terrible for productivity - Business Insider</title>
    </head>
    <body class="responsive ">This is a test content body</body>
    </html>
        }

    team = create(:team, organization_id: org.id)
    ecl_response = get_ecl_card_response(ecl_id)
    ecl_response["data"]["private_content"] = false
    ecl_response["data"]["access_restricted_in_orgs"] = []
    ecl_response["data"]["groups"] = [{"org_id": org.id, "group_id": team.id}]

    content_url = ecl_response["data"]["url"]

    stub_request(:get, content_url).
      to_return(:status => 200, :body => content_html)

    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id}").
      to_return(:status => 200, :body => ecl_response.to_json)

    card = EclApi::CardConnector.new(ecl_id: ecl_id, organization_id: org.id).card_from_ecl_id
    assert_equal ecl_id, card.ecl_id
    assert_equal "en", card.language
    assert_equal org.id, card.organization_id
    assert card.is_public
  end

  test 'create card from ecl v3 with restricted teams' do
    ecl_id = "3087222c-28bd-4536-94ba-011843f78c6a"
    org = create(:organization)
    content_html = %Q{
      <!DOCTYPE html>
      <html lang="en" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" >
      <head>
          <title>Listening to music while working is terrible for productivity - Business Insider</title>
      </head>
      <body class="responsive ">This is a test content body</body>
      </html>
    }

    ecl_response = get_ecl_card_response_v3(ecl_id)
    ecl_response["data"]["published_at"] = "2018-11-21 00:00:00"

    team = create(:team, organization_id: org.id)
    ecl_response["data"]["access_restricted_in_orgs"] = [org.id]
    ecl_response["data"]["restricted_groups"] = [{org_id: org.id, group_id: team.id}]

    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id}").
      to_return(:status => 200, :body => ecl_response.to_json)

    content_url = ecl_response["data"]["url"]

    stub_request(:get, content_url).
      to_return(:status => 200, :body => content_html)

    card = EclApi::CardConnector.new(ecl_id: ecl_id, organization_id: org.id).card_from_ecl_id
    assert_equal ecl_id, card.ecl_id
    assert_equal "en", card.language
    assert_equal org.id, card.organization_id
    refute card.is_public
    assert_equal card.teams_with_permission_ids, [ecl_response["data"]["restricted_groups"][0][:group_id]]
  end

  test 'create card from ecl v3 with restricted users' do
    ecl_id = "3087222c-28bd-4536-94ba-011843f78c6a"
    org = create(:organization)
    content_html = %Q{
      <!DOCTYPE html>
      <html lang="en" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" >
      <head>
          <title>Listening to music while working is terrible for productivity - Business Insider</title>
      </head>
      <body class="responsive ">This is a test content body</body>
      </html>
    }

    ecl_response = get_ecl_card_response_v3(ecl_id)
    ecl_response["data"]["published_at"] = "2018-11-21 00:00:00"

    user = create(:user, organization_id: org.id)
    ecl_response["data"]["access_restricted_in_orgs"] = [org.id]
    ecl_response["data"]["restricted_users"] = [{org_id: org.id, user_id: user.id}]

    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id}").
      to_return(:status => 200, :body => ecl_response.to_json)

    content_url = ecl_response["data"]["url"]

    stub_request(:get, content_url).
      to_return(:status => 200, :body => content_html)

    card = EclApi::CardConnector.new(ecl_id: ecl_id, organization_id: org.id).card_from_ecl_id
    assert_equal ecl_id, card.ecl_id
    assert_equal "en", card.language
    assert_equal org.id, card.organization_id
    refute card.is_public
    assert_equal card.users_with_permission_ids, [ecl_response["data"]["restricted_users"][0][:user_id]]
  end

  test 'create card with privacy and restricted-users/groups associations from ecl' do
    ecl_id = "0001e8d0-a6f1-4332-b52d-3f9e6a03ec59"

    org = create(:organization)

    content_html = %Q{
    <!DOCTYPE html>
    <html lang="en" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" >
    <head>
        <title>Listening to music while working is terrible for productivity - Business Insider</title>
    </head>
    <body class="responsive ">This is a test content body</body>
    </html>
        }

    ecl_response = get_ecl_card_response_v3(ecl_id)
    ecl_response["data"]["published_at"] = "2018-11-21 00:00:00"

    user = create(:user, organization_id: org.id)
    team = create(:team, organization_id: org.id)
    ecl_response["data"]["access_restricted_in_orgs"] = [org.id]
    ecl_response["data"]["restricted_users"] = [{org_id: org.id, user_id: user.id}]
    ecl_response["data"]["restricted_groups"] = [{org_id: org.id, group_id: team.id}]

    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{ecl_id}").
      to_return(:status => 200, :body => ecl_response.to_json)

    content_url = ecl_response["data"]["url"]

    stub_request(:get, content_url).
      to_return(:status => 200, :body => content_html)

    card = EclApi::CardConnector.new(ecl_id: ecl_id, organization_id: org.id).card_from_ecl_id
    assert_equal ecl_id, card.ecl_id
    assert_equal "en", card.language
    assert_equal org.id, card.organization_id
    refute card.is_public
    assert_equal card.users_with_permission_ids, [ecl_response["data"]["restricted_users"][0][:user_id]]
    assert_equal card.teams_with_permission_ids, [ecl_response["data"]["restricted_groups"][0][:group_id]]
  end

  test 'create_video_card_from_ecl' do

    video_ecl_id = "0001e8d0-a6f1-4332-b52d-3f9e6a03ec58"
    org = create(:organization)
    org_id = org.id
    content_html = %Q{
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" >
<head>
    <title>Listening to music while working is terrible for productivity - Business Insider</title>
</head>
<body class="responsive ">This is a test content body</body>
</html>
    }

    ecl_response = get_ecl_card_response_for_video_card(video_ecl_id)

    content_url = ecl_response["data"]["url"]

    stub_request(:get, content_url).
      to_return(:status => 200, :body => content_html)

    stub_request(:get, "#{Settings.ecl.api_endpoint}/api/v1/content_items/#{video_ecl_id}").
      to_return(:status => 200, :body => ecl_response.to_json)

    assert_difference -> {Card.count}, 1 do
      card = EclApi::CardConnector.new(ecl_id: video_ecl_id, organization_id: org_id).card_from_ecl_id
      assert_equal video_ecl_id, card.ecl_id
      assert_equal org_id, card.organization_id
      assert_equal 'video', card.card_subtype
      assert_equal ecl_response["data"]["name"], card.message
      assert_equal Set.new(ecl_response["data"]["tags"].map{|t| t['tag']}), Set.new(card.tags.map(&:name))
      assert_equal ecl_response["data"]["url"], card.resource.url
      assert_equal ecl_response["data"]["taxonomies_list"], card.taxonomy_topics
      assert_equal ecl_response["data"]["external_id"], card.ecl_metadata['external_id']
    end
  end

  test 'lrs enabled should reflect in card' do
    ecl_id = "rerf-rfde"

    card = create(:card, title: @title, message: @message, author_id: 12, organization_id: @org.id, card_type: 'media', card_subtype: 'text')

    payload = @payload.merge({
      "user_id" => 12,
      "source_id" => Settings.ecl.ugc_source_id,
      "content_type" => "insight",
    })

    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items")
      .with(:body => hash_including(payload))
      .to_return(
        :status => 200,
        :body => {"data" => {"id" => ecl_id, "lrs_enabled" => true}}.to_json
      )
    EclApi::CardConnector.new.propagate_card_to_ecl(card.id)

    assert card.reload.ecl_metadata["lrs_enabled"]
  end

  test 'mark feature disabled should reflect in card' do
    skip#https://jira.edcastcloud.com/browse/EP-12476
    ecl_id = "rerf-rfde"
    card = create(:card, title: @title, message: @message, author_id: 12, organization_id: @org.id, card_type: 'media', card_subtype: 'text')

    payload = @payload.merge({
      "user_id" => 12,
      "source_id" => Settings.ecl.ugc_source_id,
      "content_type" => "insight",
    })

    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items").
      with(:body => hash_including(payload)).to_return(:status => 200, :body => {"data" => {"id" => ecl_id, "mark_feature_disabled" => true}}.to_json)
    EclApi::CardConnector.new.propagate_card_to_ecl(card.id)

    assert card.reload.ecl_metadata["mark_feature_disabled"]
  end

  test 'card duration metadata should be populated in payload to ecl' do
    card = create(:card, title: @title, message: @message, author_id: 12, organization_id: @org.id, card_type: 'media', card_subtype: 'text', duration: 3600)

    payload = @payload.merge({
      "user_id" => 12,
      "source_id" => Settings.ecl.ugc_source_id,
      "duration_metadata" => {
        "calculated_duration" => 3600,
        "calculated_duration_display" => "about 1 hour"
      }
    })

    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items").
      with(:body => hash_including(payload)).to_return(:status => 200, :body => {"data" => {"id" => "ECL-1234"}}.to_json)
    EclApi::CardConnector.new.propagate_card_to_ecl(card.id)
  end

  test 'archive card from ecl for physical card' do
    card = create(:card, organization_id: @org.id, author_id: nil, ecl_id: "test-ecl-id")

    stub_request(:put, "#{Settings.ecl.api_endpoint}/api/v1/content_items/test-ecl-id").
      with(body: {'status' => 'archived'}).to_return(:status => 200)
    EclApi::CardConnector.new.archive_card_from_ecl(card.ecl_id, @org.id)
  end

  test 'archive card from ecl for virtual card' do
    card = build(:card, organization_id: @org.id, author_id: nil, ecl_id: "test-ecl-id")

    stub_request(:put, "#{Settings.ecl.api_endpoint}/api/v1/content_items/test-ecl-id").
      with(body: {'status' => 'archived'}).to_return(:status => 200)
    EclApi::CardConnector.new.archive_card_from_ecl(card.ecl_id, @org.id)
  end

  test 'trash card from ecl' do
    card = create(:card, organization_id: @org.id, author_id: nil, ecl_id: "test-ecl-id")

    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_item_states").
      with(body: { 'ecl_id' => card.ecl_id }).to_return(:status => 200)
    EclApi::CardConnector.new(ecl_id: card.ecl_id, organization_id: @org.id).trash_card_from_ecl
  end

  test 'recover card from ecl' do
    card = create(:card, organization_id: @org.id, author_id: nil, ecl_id: "test-ecl-id")

    stub_request(:put, "#{Settings.ecl.api_endpoint}/api/v1/content_item_states/update_state").
      with(body: { 'ecl_id' => card.ecl_id, 'state' => 'active' }).to_return(:status => 200)
    EclApi::CardConnector.new(ecl_id: card.ecl_id, organization_id: @org.id).recover_card_from_ecl
  end

  test 'notify ecl to delete the s3 object' do
    source_id = 'ecl-test-source-123'
    stub_request(:delete,"#{Settings.ecl.api_endpoint}/api/v1/sources/#{source_id}/delete_s3_object_source").
      to_return(:status => 200)

    EclApi::CardConnector.new(organization_id: @org.id).notify_card_deletion_job_completion_to_ecl(source_id)
  end

  # Note: EP-13195
  # Fix Propagation of title and message to ECL content item
  # Do not update description if card title and card message same
  class TitleMessagePropagationTest < ActiveSupport::TestCase
    setup do
      @title = 'some title'
      @message = 'some description'
      @org = create :organization
    end

    test 'card should propagate title, message to ecl if they are different' do
      card = create(:card, title: @title, message: @message, author_id: nil, organization_id: @org.id, card_type: 'media', ecl_metadata: {source_id: 'source-123'})

      payload = {
        "name" => @title,
        "description" => @message,
        "source_id" => 'source-123'
      }

      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items").
        with(:body => hash_including(payload)).to_return(:status => 200, :body => {"data" => {"id" => "ECL-1234"}}.to_json)
      EclApi::CardConnector.new.propagate_card_to_ecl(card.id)
    end

    test 'card should propagate message only to ecl if title and message are same' do
      card = create(:card, title: @title, message: @title, author_id: nil, organization_id: @org.id, card_type: 'media', ecl_metadata: {source_id: 'source-123'})

      payload = {
        "name" => @title,
        "source_id" => 'source-123'
      }

      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items").
        with(:body => hash_including(payload)).to_return(:status => 200, :body => {"data" => {"id" => "ECL-1234"}}.to_json)
      EclApi::CardConnector.new.propagate_card_to_ecl(card.id)
    end

    test 'card should propagate message only to ecl if title nil' do
      card = create(:card, title: nil, message: @title, author_id: nil, organization_id: @org.id, card_type: 'media', ecl_metadata: {source_id: 'source-123'})

      payload = {
        "name" => @title,
        "source_id" => 'source-123'
      }

      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items").
        with(:body => hash_including(payload)).to_return(:status => 200, :body => {"data" => {"id" => "ECL-1234"}}.to_json)
      EclApi::CardConnector.new.propagate_card_to_ecl(card.id)
    end
  end

  # Note: EP-13889
  # Fix Resync of title and message to LXP card
  class TitleMessageResyncTest < ActiveSupport::TestCase
    setup do
      @org = create :organization
    end

    test 'sync from ecl when card title and message are same' do
      ecl_id = "001ac01d-b272-4a93-8c2d-f61bc93c7129"

      resource = create(:resource, url: "http://someurl.com", image_url: "http://someurl.png")
      existing_card = create(:card,
        author_id: nil,
        title: 'message check',
        message: 'message check',
        organization_id: @org.id,
        ecl_id: ecl_id,
        resource: resource
      )

      ecl_content_item_data = {
        'ecl_id' => '001ac01d-b272-4a93-8c2d-f61bc93c7129',
        'name'=>'Travel Photography: Costa Rica',
        'description' => 'Join Rich Harrington and family',
        'resource_metadata' => {
          'title'=>'Travel Photography: Costa Rica',
          'description' => 'Join Rich Harrington and family',
        },
        'url' => 'http://www.abc.com',
        'source_display_name' => 'rss'
      }

      EclApi::CardConnector.new(ecl_id: ecl_id, organization_id: @org.id).update_card_change_from_ecl(existing_card, ecl_content_item_data)
      existing_card.reload

      assert_equal ecl_content_item_data['source_display_name'], existing_card.ecl_source_name
      assert_equal ecl_content_item_data['name'], existing_card.title
      assert_equal ecl_content_item_data['name'], existing_card.message
      assert_equal 'http://www.abc.com', resource.reload.url
      assert_equal ecl_content_item_data['name'], resource.title
      assert_equal ecl_content_item_data['description'], resource.description
    end

    test 'sync from ecl  when card title and message are different' do
      ecl_id = "001ac01d-b272-4a93-8c2d-f61bc93c7129"

      resource = create(:resource, url: "http://someurl.com", image_url: "http://someurl.png")
      existing_card = create(:card,
        author_id: nil,
        title: 'title check',
        message: 'message check',
        organization_id: @org.id,
        ecl_id: ecl_id,
        resource: resource
      )

      ecl_content_item_data = {
        'ecl_id' => '001ac01d-b272-4a93-8c2d-f61bc93c7129',
        'name'=>'Travel Photography: Costa Rica',
        'description' => 'Join Rich Harrington and family',
        'resource_metadata' => {
          'title'=>'Travel Photography: Costa Rica',
          'description' => 'Join Rich Harrington and family',
        },
        'url' => 'http://www.abc.com',
        'source_display_name' => 'rss'
      }

      EclApi::CardConnector.new(ecl_id: ecl_id, organization_id: @org.id).update_card_change_from_ecl(existing_card, ecl_content_item_data)
      existing_card.reload

      assert_equal ecl_content_item_data['source_display_name'], existing_card.ecl_source_name
      assert_equal ecl_content_item_data['name'], existing_card.title
      assert_equal ecl_content_item_data['description'], existing_card.message
      assert_equal 'http://www.abc.com', resource.reload.url
      assert_equal ecl_content_item_data['name'], resource.title
      assert_equal ecl_content_item_data['description'], resource.description
    end
  end

  class AccessControlPropagationTest < ActiveSupport::TestCase
    setup do
      @organization = create :organization
    end

    test 'propagate access control to ecl for ugc' do
      user = create :user, organization: @organization
      card = create(:card, author: user, organization: @organization, ecl_id: 'abcd',
                    ecl_metadata: {source_id: 'source'})

      expected_params = {
        access_restricted_in_orgs: [],
        groups: [],
        channels: [],
        users_with_access: [],
      }

      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items")
        .with(:body => hash_including(expected_params))
        .to_return(:status => 200, :body => {"data" => {"id" => 'abcd'}}.to_json)

      EclApi::CardConnector.new.propagate_card_to_ecl(card.id)

      # Ensure card is updated with ecl id
      assert_equal 'abcd', card.reload.ecl_id
    end

    test 'propagate access control to ecl for non-ugc' do
      card = create(:card, author_id: nil, organization: @organization, ecl_id: 'abcd',
                    ecl_metadata: {source_id: 'source'})

      expected_params = {
        access_restricted_in_orgs: [],
        groups: [],
        channels: [],
        users_with_access: [],
      }

      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items")
        .with(:body => hash_including(expected_params))
        .to_return(:status => 200, :body => {"data" => {"id" => 'abcd'}}.to_json)

      EclApi::CardConnector.new.propagate_card_to_ecl(card.id)

      # Ensure card is updated with ecl id
      assert_equal 'abcd', card.reload.ecl_id
    end
  end

  class AccessRestrictedInOrgsTest < ActiveSupport::TestCase
    test 'public card should not be restricted' do
      organization = create :organization
      card = create(:card, author: nil, organization: organization, ecl_id: 'abcd',
                    ecl_metadata: {source_id: 'source'})

      expected_params = {
        access_restricted_in_orgs: [],
      }

      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items")
        .with(:body => hash_including(expected_params))
        .to_return(:status => 200, :body => {"data" => {"id" => 'abcd'}}.to_json)

      EclApi::CardConnector.new.propagate_card_to_ecl(card.id)
    end

    test 'private card should be restricted' do
      organization = create :organization
      card = create(:card, author: nil, organization: organization, ecl_id: 'abcd',
                    ecl_metadata: {source_id: 'source'}, is_public: false)

      expected_params = {
        access_restricted_in_orgs: [organization.id],
      }

      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items")
        .with(:body => hash_including(expected_params))
        .to_return(:status => 200, :body => {"data" => {"id" => 'abcd'}}.to_json)

      EclApi::CardConnector.new.propagate_card_to_ecl(card.id)
    end

    test 'cards from multiple organizations should be included' do
      organization1 = create :organization
      public_card = create(:card, author: nil, organization: organization1, ecl_id: 'abcd',
                           ecl_metadata: {source_id: 'source'}, is_public: true)

      organization2 = create :organization
      private_card1 = create(:card, author: nil, organization: organization2, ecl_id: 'abcd',
                             ecl_metadata: {source_id: 'source'}, is_public: false)

      organization3 = create :organization
      private_card2 = create(:card, author: nil, organization: organization3, ecl_id: 'abcd',
                             ecl_metadata: {source_id: 'source'}, is_public: false)

      expected_params = {
        access_restricted_in_orgs: [organization2.id, organization3.id],
      }

      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items")
        .with(:body => hash_including(expected_params))
        .to_return(:status => 200, :body => {"data" => {"id" => 'abcd'}}.to_json)

      # Test propagation on all cards
      EclApi::CardConnector.new.propagate_card_to_ecl(public_card.id)
      EclApi::CardConnector.new.propagate_card_to_ecl(private_card1.id)
      EclApi::CardConnector.new.propagate_card_to_ecl(private_card2.id)
    end
  end

  class GroupsPropagationTest < ActiveSupport::TestCase
    test 'groups of public cards should be propagated' do
      organization = create :organization
      groups = create_list :team, 2, organization: organization

      card = create(:card, author: nil, organization: organization, ecl_id: 'abcd',
                    ecl_metadata: {source_id: 'source'}, is_public: true)

      groups.each do |group|
        SharedCard.find_or_create_by(team_id: group.id, card_id: card.id, user_id: nil)
      end


      expected_params = {
        groups: groups.map{|x| {org_id: x.organization_id, group_id: x.id}}
      }

      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items")
        .with(:body => hash_including(expected_params))
        .to_return(:status => 200, :body => {"data" => {"id" => 'abcd'}}.to_json)

      EclApi::CardConnector.new.propagate_card_to_ecl(card.id)
    end

    test 'groups of private cards should be propagated' do
      organization = create :organization
      groups = create_list :team, 2, organization: organization

      card = create(:card, author: nil, organization: organization, ecl_id: 'abcd',
                    ecl_metadata: {source_id: 'source'}, is_public: false)

      groups.each do |group|
        SharedCard.find_or_create_by(team_id: group.id, card_id: card.id, user_id: nil)
      end


      expected_params = {
        groups: groups.map{|x| {org_id: x.organization_id, group_id: x.id}}
      }

      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items")
        .with(:body => hash_including(expected_params))
        .to_return(:status => 200, :body => {"data" => {"id" => 'abcd'}}.to_json)

      EclApi::CardConnector.new.propagate_card_to_ecl(card.id)
    end

    test 'groups of cards with the same ecl_id from all orgs should be propagated' do
      organization1 = create :organization
      groups1 = create_list :team, 2, organization: organization1
      card1 = create(:card, author: nil, organization: organization1, ecl_id: 'abcd',
                     ecl_metadata: {source_id: 'source'}, is_public: false)
      groups1.each do |group|
        SharedCard.find_or_create_by(team_id: group.id, card_id: card1.id, user_id: nil)
      end

      organization2 = create :organization
      groups2 = create_list :team, 2, organization: organization2
      card2 = create(:card, author: nil, organization: organization2, ecl_id: 'abcd',
                     ecl_metadata: {source_id: 'source'}, is_public: true)
      groups2.each do |group|
        SharedCard.find_or_create_by(team_id: group.id, card_id: card2.id, user_id: nil)
      end


      expected_params = {
        groups: (groups1 + groups2).map{|x| {org_id: x.organization_id, group_id: x.id}}
      }

      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items")
        .with(:body => hash_including(expected_params))
        .to_return(:status => 200, :body => {"data" => {"id" => 'abcd'}}.to_json)

      # Test propagation of both cards
      EclApi::CardConnector.new.propagate_card_to_ecl(card1.id)
      EclApi::CardConnector.new.propagate_card_to_ecl(card2.id)
    end
  end

  class ChannelsPropagationTest < ActiveSupport::TestCase
    test 'channels of public cards should be propagated' do
      organization = create :organization
      channels = create_list :channel, 2, organization: organization

      card = create(:card, author: nil, organization: organization, ecl_id: 'abcd',
                    ecl_metadata: {source_id: 'source'}, is_public: true)

      channels.each do |channel|
        card.channels << channel
      end


      expected_params = {
        channels: channels.map{|x| {org_id: x.organization_id, channel_id: x.id}}
      }

      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items")
        .with(:body => hash_including(expected_params))
        .to_return(:status => 200, :body => {"data" => {"id" => 'abcd'}}.to_json)

      EclApi::CardConnector.new.propagate_card_to_ecl(card.id)
    end

    test 'channels of private cards should be propagated' do
      organization = create :organization
      channels = create_list :channel, 2, organization: organization

      card = create(:card, author: nil, organization: organization, ecl_id: 'abcd',
                    ecl_metadata: {source_id: 'source'}, is_public: false)

      channels.each do |channel|
        card.channels << channel
      end


      expected_params = {
        channels: channels.map{|x| {org_id: x.organization_id, channel_id: x.id}}
      }

      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items")
        .with(:body => hash_including(expected_params))
        .to_return(:status => 200, :body => {"data" => {"id" => 'abcd'}}.to_json)

      EclApi::CardConnector.new.propagate_card_to_ecl(card.id)
    end

    test 'channels of cards with the same ecl_id from all orgs should be propagated' do
      organization1 = create :organization
      channels1 = create_list :channel, 2, organization: organization1
      card1 = create(:card, author: nil, organization: organization1, ecl_id: 'abcd',
                     ecl_metadata: {source_id: 'source'}, is_public: false)
      channels1.each do |channel|
        card1.channels << channel
      end

      organization2 = create :organization
      channels2 = create_list :channel, 2, organization: organization2
      card2 = create(:card, author: nil, organization: organization2, ecl_id: 'abcd',
                     ecl_metadata: {source_id: 'source'}, is_public: true)
      channels2.each do |channel|
        card2.channels << channel
      end


      expected_params = {
        channels: (channels1 + channels2).map{|x| {org_id: x.organization_id, channel_id: x.id}}
      }

      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items")
        .with(:body => hash_including(expected_params))
        .to_return(:status => 200, :body => {"data" => {"id" => 'abcd'}}.to_json)

      # Test propagation of both cards
      EclApi::CardConnector.new.propagate_card_to_ecl(card1.id)
      EclApi::CardConnector.new.propagate_card_to_ecl(card2.id)
    end
  end

  class UserAccessPropagationTest < ActiveSupport::TestCase
    test 'user access of public cards should be propagated' do
      organization = create :organization
      users = create_list :user, 2, organization: organization

      card = create(:card, author: nil, organization: organization, ecl_id: 'abcd',
                    ecl_metadata: {source_id: 'source'}, is_public: true)
      card.users_with_access << users


      expected_params = {
        users_with_access: users.map{|x| {org_id: x.organization_id, user_id: x.id}}
      }

      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items")
        .with(:body => hash_including(expected_params))
        .to_return(:status => 200, :body => {"data" => {"id" => 'abcd'}}.to_json)

      EclApi::CardConnector.new.propagate_card_to_ecl(card.id)
    end

    test 'user access of private cards should be propagated' do
      organization = create :organization
      users = create_list :user, 2, organization: organization

      card = create(:card, author: nil, organization: organization, ecl_id: 'abcd',
                    ecl_metadata: {source_id: 'source'}, is_public: false)

      card.users_with_access << users


      expected_params = {
        users_with_access: users.map{|x| {org_id: x.organization_id, user_id: x.id}}
      }

      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items")
        .with(:body => hash_including(expected_params))
        .to_return(:status => 200, :body => {"data" => {"id" => 'abcd'}}.to_json)

      EclApi::CardConnector.new.propagate_card_to_ecl(card.id)
    end

    test 'user access of cards with the same ecl_id from all orgs should be propagated' do
      organization1 = create :organization
      users1 = create_list :user, 2, organization: organization1
      card1 = create(:card, author: nil, organization: organization1, ecl_id: 'abcd',
                     ecl_metadata: {source_id: 'source'}, is_public: false)
      card1.users_with_access << users1

      organization2 = create :organization
      users2 = create_list :user, 2, organization: organization2
      card2 = create(:card, author: nil, organization: organization2, ecl_id: 'abcd',
                     ecl_metadata: {source_id: 'source'}, is_public: true)
      card2.users_with_access << users2


      expected_params = {
        users_with_access: (users1 + users2).map{|x| {org_id: x.organization_id, user_id: x.id}}
      }

      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items")
        .with(:body => hash_including(expected_params))
        .to_return(:status => 200, :body => {"data" => {"id" => 'abcd'}}.to_json)

      # Test propagation of both cards
      EclApi::CardConnector.new.propagate_card_to_ecl(card1.id)
      EclApi::CardConnector.new.propagate_card_to_ecl(card2.id)
    end
  end

  class CardRestrictionOnUserTeamPropagationTest < ActiveSupport::TestCase
    test 'card user permission of private cards should be propagated' do
      organization = create :organization
      users = create_list :user, 2, organization: organization

      card = create(:card, author: nil, organization: organization, ecl_id: 'abcd',
                    ecl_metadata: {source_id: 'source'}, is_public: false)

      card.users_with_permission << users


      expected_params = {
        restricted_users: users.map{|x| {org_id: x.organization_id, user_id: x.id}}
      }

      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items")
        .with(:body => hash_including(expected_params))
        .to_return(:status => 200, :body => {"data" => {"id" => 'abcd'}}.to_json)

      EclApi::CardConnector.new.propagate_card_to_ecl(card.id)
    end

    test 'card team permission of private cards should be propagated' do
      organization = create :organization
      groups = create_list :team, 2, organization: organization

      card = create(:card, author: nil, organization: organization, ecl_id: 'abcd',
                    ecl_metadata: {source_id: 'source'}, is_public: false)

      groups.each do |group|
        CardTeamPermission.find_or_create_by(team_id: group.id, card_id: card.id)
      end


      expected_params = {
        restricted_groups: groups.map{|x| {org_id: x.organization_id, group_id: x.id}}
      }

      stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items")
        .with(:body => hash_including(expected_params))
        .to_return(:status => 200, :body => {"data" => {"id" => 'abcd'}}.to_json)

      EclApi::CardConnector.new.propagate_card_to_ecl(card.id)
    end
  end
end

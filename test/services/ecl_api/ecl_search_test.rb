require 'test_helper'

class EclApi::EclSearchTest < ActiveSupport::TestCase

  setup do
    @organization = create :organization
    @user = create :user, organization: @organization
    @connection = ::EclApi::EclSearch.new(@organization.id, @user.id)
    @ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items"]
    @query = "q"
    @limit = 10
    @offset = 0
  end

  test 'payload passed in request-header' do
    payload = {organization_id:  @organization.id, name: @organization.name, user_id: @user.id}
    assert_equal payload, @connection.payload
  end

  test 'should be able to parse ecl search response' do
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items/search").with(body: {query: @query, limit: @limit, offset: @offset, type: 'search',load_hidden: false}).to_return(body: @ecl_response.to_json, status: 200)
    search_results = @connection.search(query: @query, limit: @limit, offset: @offset)['results']
    assert_equal 3, search_results.length
    assert_equal @ecl_response["data"].map{ |t| DateTime.parse(t['published_at'])}, search_results.map(&:published_at)
    assert_equal @ecl_response["data"].map{|r| r["id"]}, search_results.map(&:ecl_id)
    assert_equal @ecl_response["data"].map{|r| r["tags"].map{|t| t["tag"]}}, search_results.map{|card| card.tags.map(&:name)}
    assert_equal @ecl_response["data"].map{|r| r["url"]}, search_results.map{|card| card.resource.url}
    assert_equal @ecl_response["data"].map{|r| r["resource_metadata"].try(:[], "images").try(:first).try(:[], "url")}, search_results.map{|card| card.resource.image_url}
    search_results.each do |c|
      card = @ecl_response["data"].detect{|x| x["id"] == c.ecl_id}
      assert_equal card["private_content"], !c.is_public
    end
    embed_html = [nil, nil, @ecl_response["data"].last["resource_metadata"]["embed_html"]]
    assert_equal embed_html, search_results.map{|card| card.resource.embed_html}
    assert_equal ['media', 'media', 'media'], search_results.map{|card| card.card_type}
    assert_equal ['link', 'link', 'link'], search_results.map{|card| card.card_subtype}
  end

  test 'should load tags by default' do
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items/search").with(body: {query: @query, limit: @limit, offset: @offset, type: 'search', load_hidden: false}).to_return(body: @ecl_response.to_json, status: 200)
    search_results = @connection.search(query: @query, limit: @limit, offset: @offset)['results']

    assert_equal @ecl_response["data"].map{|r| r["tags"].map{|t| t["tag"]}}, search_results.map{|card| card.tags.map(&:name)}
  end

  test 'should load tags if enabled' do
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items/search").with(body: {query: @query, limit: @limit, offset: @offset, type: 'search', load_hidden: false}).to_return(body: @ecl_response.to_json, status: 200)
    search_results = @connection.search(query: @query, limit: @limit, offset: @offset, load_tags: true)['results']

    assert_equal @ecl_response["data"].map{|r| r["tags"].map{|t| t["tag"]}}, search_results.map{|card| card.tags.map(&:name)}
  end

  test 'should not load tags if disabled' do
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items/search").with(body: {query: @query, limit: @limit, offset: @offset, type: 'search', load_hidden: false}).to_return(body: @ecl_response.to_json, status: 200)
    search_results = @connection.search(query: @query, limit: @limit, offset: @offset, load_tags: false)['results']

    assert_equal [[], [], []], search_results.map{|card| card.tags.map(&:name)}
  end

  test 'should not persist virtual cards' do
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items/search").with(body: {query: @query, limit: @limit, offset: @offset, type: 'search', load_hidden: false}).to_return(body: @ecl_response.to_json, status: 200)
    assert_no_difference ->{Card.count} do
      @connection.search(query: @query, limit: @limit, offset: @offset)
    end
  end

  test 'should look up existing cards before creating a virtual card' do
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items/search").with(body: {query: @query, limit: @limit, offset: @offset, type: 'search', load_hidden: false}).to_return(body: @ecl_response.to_json, status: 200)
    c = create(:card, organization: @organization, author: nil, ecl_id: @ecl_response["data"][0]["id"])
    search_results = @connection.search(query: @query, limit: @limit, offset: @offset)['results']
    assert_equal @ecl_response["data"].map{|r| r["id"]}, search_results.map(&:ecl_id)
  end

  test 'should support filter params' do
    filter_params = {  :content_type => ['article'], :source_id => ["addd", "adds"], load_hidden: false, :topic => ['topic1']}
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items/search").with(body: filter_params.merge({query: @query, limit: @limit, offset: @offset, type: 'search', load_hidden: false})).to_return(body: @ecl_response.to_json, status: 200)
    @connection.search(query: @query, limit: @limit, offset: @offset, filter_params: filter_params)

    filter_params = {  only_default_sources: true, topic: ['topic1'], load_hidden: false}
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items/search").with(body: filter_params.merge({query: @query, limit: @limit, offset: @offset, type: 'search', load_hidden: false})).to_return(body: @ecl_response.to_json, status: 200)
    @connection.search(query: @query, limit: @limit, offset: @offset, filter_params: filter_params)

    filter_params = {  only_default_sources: true, topic: ['topic1'], load_hidden: false, rank: true, rank_params: { user_ids: [1,2,3] }}
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items/search").with(body: filter_params.merge({query: @query, limit: @limit, offset: @offset, type: 'search', load_hidden: false, rank: true, rank_params: { user_ids: [1,2,3] } })).to_return(body: @ecl_response.to_json, status: 200)
    @connection.search(query: @query, limit: @limit, offset: @offset, filter_params: filter_params)

    filter_params = {  only_default_sources: true, exclude_ecl_card_id: [1, 2, 4, 5], load_hidden: false, rank: true, rank_params: { user_ids: [1,2,3] }}
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items/search").with(body: filter_params.merge({query: @query, limit: @limit, offset: @offset, type: 'search', load_hidden: false, rank: true, rank_params: { user_ids: [1,2,3] } })).to_return(body: @ecl_response.to_json, status: 200)
    @connection.search(query: @query, limit: @limit, offset: @offset, filter_params: filter_params)

    filter_params = { :level => ['intermediate'], load_hidden: false, :rating => ['3', '4']}
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items/search").with(body: filter_params.merge({query: @query, limit: @limit, offset: @offset, type: 'search'})).to_return(body: @ecl_response.to_json, status: 200)
    @connection.search(query: @query, limit: @limit, offset: @offset, filter_params: filter_params)
  end

  test 'should parse aggregations' do
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items/search").with(body: {query: @query, limit: @limit, offset: @offset, type: 'search', load_hidden: false}).to_return(body: @ecl_response.to_json, status: 200)
    aggs = @connection.search(query: @query, limit: @limit, offset: @offset)['aggregations']
    assert_equal 8, aggs.count
    assert_equal ["Articles", "Document", "Books", "Lynda", "Coursera", "Gemba", "English" , "Business Management"], aggs.map{|agg| agg["display_name"]}
    assert_equal ["content_type", "content_type", "content_type", "source_id", "source_id", "provider_name", "languages", "subjects"], aggs.map{|agg| agg["type"]}
    assert_equal [8, 1, 20, 4, 90, 1, 1 , 1], aggs.map{|agg| agg["count"]}
  end

  test 'should handle unsuccessful response from ecl api' do
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items/search").with(body: {query: @query, limit: @limit, offset: @offset, type: 'search', load_hidden: false}).to_return(body: "", status: 404)
    data =  @connection.search(query: @query, limit: @limit, offset: @offset)
    assert_empty data["results"]
    assert_empty data["aggregations"]
  end

  test 'should filter out cards based on level' do
    filter_params = { level: ['intermediate'], load_hidden: false}
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items/search").with(body: filter_params.merge({query: @query, limit: @limit, offset: @offset, type: 'search'})).to_return(body: @ecl_response.to_json, status: 200)
    search_results = @connection.search(query: @query, limit: @limit, offset: @offset, filter_params: filter_params)['results']
    assert_equal @ecl_response["data"][0]["level"], 'intermediate'
  end

  test 'should filter out cards based on rating' do
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items/search").with(body: {query: @query, limit: @limit, offset: @offset, type: 'search', rating: ['5'], load_hidden: false}).to_return(body: @ecl_response.to_json, status: 200)
    filter_params = {  rating: ['5']}
    search_results = @connection.search(query: @query, limit: @limit, offset: @offset, filter_params: filter_params)['results']
    assert_equal @ecl_response["data"][0]["rating"], 2
  end

  test 'should filter out cards based on price and currency' do
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items/search").with(body: hash_including({query: @query, limit: @limit, offset: @offset, type: 'search', currency: 'INR', amount_range: ["[5.0, 10.0]"]})).to_return(body: @ecl_response.to_json, status: 200)
    c = create(:card, hidden: true, organization: @organization, author: @user, ecl_id: @ecl_response["data"][0]["id"])
    filter_params = {  currency: 'INR', amount_range: ["[5.0, 10.0]"] }
    search_results = @connection.search(query: @query, limit: @limit, offset: @offset, filter_params: filter_params)['results']
    assert_equal @ecl_response["data"][0]["rating"], 2
    assert_equal @ecl_response["data"][0]["prices_data"][0]['amount'], "5.0"
  end

  test 'should filter out cards based on free or paid status' do
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items/search").with(body: {query: @query, limit: @limit, offset: @offset, type: 'search', is_paid: true, load_hidden: false}).to_return(body: @ecl_response.to_json, status: 200)
    filter_params = { is_paid: true }
    search_results = @connection.search(query: @query, limit: @limit, offset: @offset, filter_params: filter_params)['results']
    assert_equal true, @ecl_response["data"][0]["is_paid"]
  end

  test 'should filter out cards based on price plans' do
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items/search").with(body: {query: @query, limit: @limit, offset: @offset, type: 'search', plan: ["paid"], load_hidden: false}).to_return(body: @ecl_response.to_json, status: 200)
    filter_params = { "plan": ["paid"] }
    search_results = @connection.search(query: @query, limit: @limit, offset: @offset, filter_params: filter_params)['results']
    assert_equal "paid", @ecl_response["data"][0]["plan"]
  end

  test 'should support users language param in search' do
   filter_params = { language: "en", load_hidden: false}
   stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items/search")
       .with(body: hash_including({query: @query, limit: @limit, offset: @offset, type: 'search'}))
       .to_return(body: @ecl_response.to_json, status: 200)
   @connection.search(query: @query, limit: @limit, offset: @offset, filter_params: filter_params)['results']
 end

  test 'should support user interest param' do
    filter_params = {user_interests: ['interest1', 'interest2', 'interest3'], load_hidden: false}
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items/search")
        .with(body: hash_including({query: @query, limit: @limit, offset: @offset, type: 'search'}))
        .to_return(body: @ecl_response.to_json, status: 200)
    @connection.search(query: @query, limit: @limit, offset: @offset, filter_params: filter_params)['results']
  end

  test 'should support is_admin param' do
    filter_params = { is_admin: true, load_hidden: false }
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items/search")
        .with(body: hash_including({query: @query, limit: @limit, offset: @offset, type: 'search'}))
        .to_return(body: @ecl_response.to_json, status: 200)
    @connection.search(query: @query, limit: @limit, offset: @offset, filter_params: filter_params)['results']
  end

  test 'should support topic_ids param' do
    filter_params = { topic_ids: ["9371e5d2-7013-41cd-acab-5a20a657f5c7"], load_hidden: false }
    stub_request(:post, "#{Settings.ecl.api_endpoint}/api/v1/content_items/search")
        .with(body: hash_including({query: @query, limit: @limit, offset: @offset, type: 'search'}))
        .to_return(body: @ecl_response.to_json, status: 200)
    @connection.search(query: @query, limit: @limit, offset: @offset, filter_params: filter_params)['results']
  end

  test "default should get media link" do
    assert_equal ["media", "link"], EclApi::EclSearch.new(@organization.id).get_card_type_and_subtype("xyz")
    assert_equal ["media", "link"], EclApi::EclSearch.new(@organization.id).get_card_type_and_subtype("article")
    assert_equal ["course", "link"], EclApi::EclSearch.new(@organization.id).get_card_type_and_subtype("course")
  end
  
end

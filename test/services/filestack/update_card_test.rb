require 'test_helper'

class Filestack::UpdateCardTest < ActiveSupport::TestCase
  test '#save doesnt do anything if urls are same' do
    card = build(:card)
    url = "http://cdn.filestackcontent.com/066CQGnQR0mNyPhU1jG4"
    resized_image_options = {"url" => url}

    Card.any_instance.expects(:update).never

    Filestack::UpdateCard.new(card: card, url: url, resized_image_options: resized_image_options).save
  end

  test '#save updates filestack' do
    filestack = [
      {
        filename: "SampleVideo_1280x720_1mb.mp4",
        handle: "5Gw6KbtrSUaAl9SokjBJ",
        mimetype: "video/mp4",
        size:156333,
        source: "local_file_system",
        status: "Stored",
        url: "https://cdn.filestackcontent.com/5Gw6KbtrSUaAl9SokjBJ"
      },
      {
        filename: "audio.mp3",
        handle: "5Gw6KbtrSUaAl9SokjBJ",
        mimetype: "audio/mp3",
        size:156333,
        source: "local_file_system",
        status: "Stored",
        url: "https://cdn.filestackcontent.com/5Gw6KbtrSUaAl9SokjBJ"
      },
      {
        filename: 'Screen Shot 2018-06-05 at 11.51.06 AM.png',
        mimetype: 'image/png',
        size: '1946518',
        source: 'local_file_system',
        url: 'https://cdn.filestackcontent.com/d2sjlUgWT1WRJwldER6P',
        handle: 'd2sjlUgWT1WRJwldER6P',
        status: 'Stored'
      }
    ]
    card = create(:card, filestack: filestack)
    url = "http://cdn.filestackcontent.com/066CQGnQR0mNyPhU1jG4"
    resized_image_options = {"url" => url, "handle" => '066CQGnQR0mNyPhU1jG4', "mimetype" => 'image/png', "size" => 1234}

    Filestack::UpdateCard.new(card: card, url: "https://cdn.filestackcontent.com/d2sjlUgWT1WRJwldER6P" , resized_image_options: resized_image_options).save

    assert_equal filestack[0], card.reload.filestack[0]
    assert_equal filestack[1], card.reload.filestack[1]
    assert_equal url, card.reload.filestack[2][:url]
  end
end

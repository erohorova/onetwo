require 'test_helper'

class Filestack::ProcessImageTest < ActiveSupport::TestCase
  test '#resize returns url if url is not image' do
    url = "http://cdn.filestackcontent.com/066CQGnQR0mNyPhU1jG4"
    stub_request(:get, /066CQGnQR0mNyPhU1jG4/).to_return(status: 200, body: File.read('test/fixtures/files/test.csv'))

    assert_equal url, Filestack::ProcessImage.new(url: url).resize["url"]
  end

  test '#resize returns url if url is one of the stock image' do
    url = "http://cdn.filestackcontent.com/M9k3ydtRMeVs5TzCSUBN"
    stub_request(:get, /M9k3ydtRMeVs5TzCSUBN/).to_return(status: 200, body: File.read('test/fixtures/images/test.png'))

    assert_equal url, Filestack::ProcessImage.new(url: url).resize["url"]
  end

  test "#resize returns url if height is less than 400px" do
    url = "http://cdn.filestackcontent.com/066CQGnQR0mNyPhU1jG4"
    stub_request(:get, /066CQGnQR0mNyPhU1jG4/).to_return(status: 200, body: File.read('test/fixtures/images/test.png'))

    assert_equal url, Filestack::ProcessImage.new(url: url).resize["url"]
  end

  test "#resize returns resized url if height is greater than 400px" do
    url = "http://cdn.filestackcontent.com/066CQGnQR0mNyPhU1jG4"
    process_url = "#{Settings.filestack.api}/api/store/S3?key=#{Settings.filestack.api_key}&url=https://process.filestackapi.com/#{Settings.filestack.api_key}/resize=height:400/#{url}"
    process_response = {"url" => "https://cdn.filestackcontent.com/BpdZjMERIGAGOgLfQlRI"}

    stub_request(:get, /066CQGnQR0mNyPhU1jG4/).to_return(status: 200, body: File.read('test/fixtures/images/technology.jpeg'))
    stub_request(:post, /\/api\/store\/S3/).to_return(status: 200, body: process_response.to_json, headers: {})

    assert_equal process_response["url"], Filestack::ProcessImage.new(url: url).resize["url"]
  end
end

require 'test_helper'

class Filestack::SecurityTest < ActiveSupport::TestCase
  test "generates policy for read action expirable after default 15 minutes" do
    time = Time.now
    Timecop.freeze(time) do
      obj = Filestack::Security.new
      expected_policy = Base64.urlsafe_encode64({
        call: ["read"],
        expiry: (time + 15.minutes).to_i
      }.to_json)
      assert_equal expected_policy, obj.policy
    end
  end

  test "generates policy for upload action expirable after 1 hour" do
    time = Time.now
    Timecop.freeze(time) do
      obj = Filestack::Security.new(call: ["pick"], expire_after_seconds: 1.hour)
      expected_policy = Base64.urlsafe_encode64({
        call: ["pick"],
        expiry: (time + 1.hour).to_i
      }.to_json)
      assert_equal expected_policy, obj.policy
    end
  end

  test "returns signed_url with policy and signature" do
    handle = "handle"
    obj = Filestack::Security.new
    signed_url = obj.signed_url(handle)
    assert_equal "https://cdn.filestackcontent.com/security=p:#{obj.policy},s:#{obj.signature}/handle", signed_url
  end
end

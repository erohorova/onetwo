require 'test_helper'

class DiscoveryService::ChannelsTest < ActiveSupport::TestCase
  setup do
    #TODO
    # @results = DiscoveryService::Channels.new(user: @user).discover
    # is called twice some of the tests
    # Entire file needs to be stubbed ChannelSearchResponse
    @org   = create :organization
    @owner = create :user, organization: @org
    @user  = create :user, organization: @org
    @user_profile = create(:user_profile, user: @user, learning_topics: Taxonomies.domain_topics)

    @general_channel = @org.channels.where(label: "General").first
    @channel_1 = create :channel, organization: @org, user_id: @owner.id

    #### creating promoted channels with matching user's learning topics ####
    @promoted_channel_1 = create :channel_with_topics, :promoted, organization: @org, user: @owner, label: 'Promoted channel one'
    @promoted_channel_2 = create :channel_with_topics, :promoted, organization: @org, user: @owner, label: 'Promoted channel two'
    @promoted_channel_3 = create :channel_with_topics, :promoted, organization: @org, user: @owner, label: 'Promoted channel three'

    @promoted_channels = [@promoted_channel_1, @promoted_channel_3, @promoted_channel_2]

    #### creating regular channels with matching user's learning topics ####
    @channel_2 = create :channel_with_topics, organization: @org, user: @owner, label: 'TechKnow one'
    @channel_3 = create :channel_with_topics, organization: @org, user: @owner, label: 'TechKnow four'
    @channel_4 = create :channel_with_topics, organization: @org, user: @owner, label: 'TechKnow two'
    @channel_5 = create :channel_with_topics, organization: @org, user: @owner, label: 'TechKnow three'

    ### Create channels with followers ###
    @channel_6 = create :channel, organization: @org, user_id: @owner.id
    15.times do
      user = (create :user, organization: @org)
      create(:follow, followable: @channel_6, user: user )
    end

    @channel_with_followers = [@channel_6]

    @channel_7 = create :channel, organization: @org, user_id: @owner.id
    @channel_8 = create :channel_with_topics, organization: @org, user: @user, label: 'TechKnow eight'

    @channel_with_topics = [@channel_8 , @channel_3, @channel_2, @channel_5, @channel_4 ]

    12.times do
      user = (create :user, organization: @org)
      create(:follow, followable: @channel_7, user: user )
    end

    @channel_4.authors << @user
    @promoted_channel_1.add_followers([@user])
    @channel_1.add_followers([@user])

    @user.reload
  end

  test 'should fetch only channels as limit count' do
    Search::ChannelSearch.any_instance
      .expects(:search)
      .with(
        q: nil,
        viewer: @user,
        filter: {
          exclude_ids: [],
          by_topics: ['edcast.technology.software_development.c#', 'edcast.technology.software_development.robots', 'edcast.technology.software_development.robots']
        },
        limit: 3,
        offset: 0,
        sort: [
          {key: :is_promoted, order: :desc},{key: :label, order: :asc}
        ],
        load: true).
        returns(
          Search::ChannelSearchResponse.new(results: (@channel_with_topics), total: 3)
          )
        .once

    @results = DiscoveryService::Channels.new(user: @user, params: { limit: 3, offset: 0 }).discover
    assert_equal 3, @results.count
  end

  test 'dont exclude channels that are followed by user' do
    Search::ChannelSearch.any_instance
      .expects(:search)
      .with(
        q: nil,
        viewer: @user,
        filter: {
          exclude_ids: [],
          by_topics: ['edcast.technology.software_development.c#', 'edcast.technology.software_development.robots', 'edcast.technology.software_development.robots']
        },
        limit: 5,
        offset: 0,
        sort: [
          {key: :is_promoted, order: :desc},{key: :label, order: :asc}
        ],
        load: true
        )
      .returns(
          Search::ChannelSearchResponse.new(results: ([@channel_1]), total: 1)
          )
      .once
    Search::ChannelSearch.any_instance
      .expects(:search)
      .with(
        q: nil,
        viewer: @user,
        filter: {
          exclude_ids: [],
          is_promoted: true
        },
        limit: 5,
        offset: 0,
        sort: [
          {key: :is_promoted, order: :desc},{key: :label, order: :asc}
        ],
        load: true
        )
      .returns(Search::ChannelSearchResponse.new(results: ([@channel_1]), total: 1))
      .once

    Search::ChannelSearch.any_instance
      .expects(:search)
      .with(
        q: nil,
        viewer: @user,
        filter: {
          exclude_ids: [],
          by_topics: ['edcast.technology.software_development.c#', 'edcast.technology.software_development.robots', 'edcast.technology.software_development.robots']
        },
        limit: 5,
        offset: 0,
        sort: [
          {key: :is_promoted, order: :desc}, {key: :label, order: :asc}
        ],
        load: true
        )
      .returns(
        Search::ChannelSearchResponse.new(results: (@promoted_channels), total: 3)
        )
      .once

    @results = DiscoveryService::Channels.new(user: @user, params: { limit: 5, offset: 0 }).discover
    channel_ids = @results.map(&:id)

    assert_includes channel_ids, @promoted_channel_1.id
    assert_includes channel_ids, @channel_1.id
  end

  test 'dont exclude channels if user is an AUTHOR' do
    Search::ChannelSearch.any_instance
    .expects(:search)
    .with(
      q: nil,
      viewer: @user,
      filter: {
        exclude_ids: [],
        by_topics: ['edcast.technology.software_development.c#', 'edcast.technology.software_development.robots', 'edcast.technology.software_development.robots']
      },
      limit: 5, offset: 0,
      sort: [
        {key: :is_promoted, order: :desc}, {key: :label, order: :asc}],
        load: true
        )
    .returns(
      Search::ChannelSearchResponse.new(results: (@channel_with_topics), total: 3)
      )
    .once

    @results = DiscoveryService::Channels.new(user: @user, params: { limit: 5, offset: 0 }).discover
    assert_includes @results.map(&:id), @channel_4.id
  end

  test 'include own channels' do
    Search::ChannelSearch.any_instance
    .expects(:search)
    .with(
      q: nil,
      viewer: @user,
      filter: {
        exclude_ids: [],
        by_topics: ['edcast.technology.software_development.c#', 'edcast.technology.software_development.robots', 'edcast.technology.software_development.robots']
      },
      limit: 3, offset: 0,
      sort: [{key: :is_promoted, order: :desc}, {key: :label, order: :asc}],
      load: true)
    .returns(
      Search::ChannelSearchResponse.new(results: ([@channel_1,@channel_6,@channel_8]), total: 3)
      )
    .once

    @results = DiscoveryService::Channels.new(user: @user, params: { limit: 3, offset: 0 }).discover
    assert_includes @results.map(&:id), @channel_8.id
  end

  test 'includes channels if user is not an AUTHOR as per his topics' do
    Search::ChannelSearch.any_instance
      .expects(:search)
      .with(
        q: nil,
        viewer: @user,
        filter: {exclude_ids: [], by_topics: ['edcast.technology.software_development.c#', 'edcast.technology.software_development.robots', 'edcast.technology.software_development.robots']
        },
        limit: 4,
        offset: 0,
      sort: [{key: :is_promoted, order: :desc}, {key: :label, order: :asc}], load: true)
      .returns(
        Search::ChannelSearchResponse.new(results: (@channel_with_topics), total: 3)
        )
      .once

    @results = DiscoveryService::Channels.new(user: @user, params: { limit: 4, offset: 0 }).discover
    assert_includes @results.map(&:id), @channel_5.id
  end

  test 'includes promoted channels if user does not follow' do
    Search::ChannelSearch.any_instance
      .expects(:search)
      .with(
        q: nil,
        viewer: @user,
        filter: {
          exclude_ids: [],
          by_topics: ['edcast.technology.software_development.c#', 'edcast.technology.software_development.robots', 'edcast.technology.software_development.robots']
        },
        limit: 6,
        offset: 0,
        sort: [
          {key: :is_promoted, order: :desc}, {key: :label, order: :asc}
        ],
        load: true)
      .returns(Search::ChannelSearchResponse.new(results: (@channel_with_topics), total: 3)
        )
      .once
    Search::ChannelSearch.any_instance
      .expects(:search)
      .with(
        q: nil,
        viewer: @user,
        filter: {
          exclude_ids: [],
          by_topics: ['edcast.technology.software_development.c#', 'edcast.technology.software_development.robots', 'edcast.technology.software_development.robots']
        },
        limit: 6,
        offset: 0,
        sort: [{key: :is_promoted, order: :desc}, {key: :label, order: :asc}], load: true)
      .returns(
        Search::ChannelSearchResponse.new(results: (@promoted_channels), total: 3)
        )
      .once
    @results = DiscoveryService::Channels.new(user: @user, params: { limit: 6, offset: 0 }).discover
    assert_includes @results.map(&:id), @promoted_channel_2.id
    assert_includes @results.map(&:id), @promoted_channel_3.id
  end

  test 'includes non-promoted channels' do
    Search::ChannelSearch.any_instance
      .expects(:search)
      .with(
        q: nil,
        viewer: @user,
        filter: {
          exclude_ids: [],
          by_topics: ['edcast.technology.software_development.c#', 'edcast.technology.software_development.robots', 'edcast.technology.software_development.robots']
        },
        limit: 4,
        offset: 0,
      sort: [
        {key: :is_promoted, order: :desc}, {key: :label, order: :asc}
      ],
      load: true
      )
      .returns(
        Search::ChannelSearchResponse.new(results: (@channel_with_topics), total: 2)
        )
      .once

    @results = DiscoveryService::Channels.new(user: @user, params: { limit: 4, offset: 0 }).discover
    channel_ids = @results.map(&:id)

    assert_includes channel_ids, @channel_2.id
    assert_includes channel_ids, @channel_3.id
  end

  test 'sort results by is_promoted(:desc) and label(:asc)' do
    Search::ChannelSearch.any_instance
    .expects(:search)
    .with(
      q: nil,
      viewer: @user,
      filter: {
        exclude_ids: [],
        by_topics: ['edcast.technology.software_development.c#', 'edcast.technology.software_development.robots', 'edcast.technology.software_development.robots']
      },
      limit: 25,
      offset: 0,
      sort: [
        {key: :is_promoted, order: :desc}, {key: :label, order: :desc}
      ],
      load: true)
    .returns(
      Search::ChannelSearchResponse.new(results: (@channel_with_topics), total: 5)
      )
    .once

    Search::ChannelSearch.any_instance
      .expects(:search)
      .with(
        q: nil,
        viewer: @user,
        filter: {
          exclude_ids: [],
          by_topics: ['edcast.technology.software_development.c#', 'edcast.technology.software_development.robots', 'edcast.technology.software_development.robots']
        },
        limit: 25,
        offset: 0,
        sort: [
          {key: :is_promoted, order: :desc}, {key: :label, order: :desc}
        ],
        load: true)
      .returns(
        Search::ChannelSearchResponse.new(results: (@promoted_channels), total: 3)
        )
      .once

    Search::ChannelSearch.any_instance
      .expects(:search)
      .with(
        q: nil,
        viewer: @user,
        filter: {
          exclude_ids: [],
          is_promoted: true
        },
        limit: 25,
        offset: 0,
        sort: [
          {key: :is_promoted, order: :desc}, {key: :label, order: :desc}
        ],
        load: true)
      .returns(
        Search::ChannelSearchResponse.new(results: (@promoted_channels), total: 3)
        )
      .once

    @results = DiscoveryService::Channels.new(user: @user, params: { order_label: :desc }).discover

    assert_equal @results.map(&:id), [@promoted_channel_1.id, @promoted_channel_3.id, @promoted_channel_2.id, @channel_8.id, @channel_3.id, @channel_2.id, @channel_5.id, @channel_4.id, @channel_6.id, @channel_7.id, @channel_1.id]
  end

  test 'returns other promoted, most followed channels with authoring channels and own channels if no match found for expert/learning topics' do
    User.any_instance.stubs(:expert_topics_name).returns([])
    User.any_instance.stubs(:learning_topics_name).returns([])

    Search::ChannelSearch.any_instance
      .expects(:search)
      .with(
        q: nil,
        viewer: @user,
        filter: {
          exclude_ids: [],
          is_promoted: true
        },
        limit: 9,
        offset: 0,
        sort: [{key: :is_promoted, order: :desc}, {key: :label, order: :asc}
        ],
        load: true
        )
      .returns(
        Search::ChannelSearchResponse.new(results: (@promoted_channels), total: 3)
        )
      .once

    Search::ChannelSearch.any_instance
      .expects(:search)
      .with(
        q: nil,
        viewer: @user,
        filter: {
          exclude_ids: [],
          by_topics: []},
          limit: 9,
          offset: 0,
          sort: [
            {key: :is_promoted, order: :desc}, {key: :label, order: :asc}
          ],
          load: true
          )
      .returns(
        Search::ChannelSearchResponse.new(results: ([@channel_6, @channel_7, @channel_1, @channel_4, @channel_8]), total: 3)
        )
      .once
    Search::ChannelSearch.any_instance
      .expects(:search)
      .with(
        q: nil,
        viewer: @user,
        filter: {
          exclude_ids: [],
          by_topics: []
        },
        limit: 9,
        offset: 0,
        sort: [
          {key: :is_promoted, order: :desc}, {key: :label, order: :asc}
        ],
        load: true
        )
      .returns(
        Search::ChannelSearchResponse.new(results: ([]), total: 3)
        )
      .once
    results = DiscoveryService::Channels.new(user: @user, params: { limit: 9, offset: 0 }).discover
    assert_equal [@channel_6.id, @channel_7.id, @channel_1.id, @channel_4.id, @channel_8.id, @promoted_channel_1.id, @promoted_channel_3.id, @promoted_channel_2.id], results.map(&:id)
  end

  test 'returns public channels and skip private channel' do
    User.any_instance.stubs(:expert_topics_name).returns([])
    User.any_instance.stubs(:learning_topics_name).returns([])

    public_channel = create :channel, organization: @org, user_id: @owner.id
    private_channel = create :channel, organization: @org, user_id: @owner.id, is_private: true
    2.times do
      user = (create :user, organization: @org)
      create(:follow, followable: public_channel, user: user )
      create(:follow, followable: private_channel, user: user )
    end

    Search::ChannelSearch.any_instance
      .expects(:search)
      .with(
        q: nil,
        viewer: @user,
        filter: {
          exclude_ids: [],
          is_promoted: true
        },
        limit: 9,
        offset: 0,
        sort: [
          {key: :is_promoted, order: :desc}, {key: :label, order: :asc}
        ],
        load: true
        )
      .returns(Search::ChannelSearchResponse.new(results: (@promoted_channels), total: 3)
        )
      .once
    Search::ChannelSearch.any_instance
      .expects(:search)
      .with(
        q: nil,
        viewer: @user,
        filter: {
          exclude_ids: [],
          by_topics: []
        },
        limit: 9,
        offset: 0,
        sort: [
          {key: :is_promoted, order: :desc}, {key: :label, order: :asc}
        ],
        load: true
        )
      .returns(
        Search::ChannelSearchResponse.new(results: ([@channel_6, @channel_7, @channel_1, @channel_4, @channel_8]), total: 3)
        )
      .once
    Search::ChannelSearch.any_instance
      .expects(:search)
      .with(
        q: nil,
        viewer: @user,
        filter: {
          exclude_ids: [],
          by_topics: []
        },
        limit: 9,
        offset: 0,
        sort: [
          {key: :is_promoted, order: :desc}, {key: :label, order: :asc}
        ],
        load: true
        )
      .returns(
        Search::ChannelSearchResponse.new(results: ([]), total: 3)
        )
      .once

    results = DiscoveryService::Channels.new(user: @user,params: { limit: 9, offset: 0 }).discover

    assert_equal [@channel_6.id, @channel_7.id, @channel_1.id, @channel_4.id, @channel_8.id, @promoted_channel_1.id, @promoted_channel_3.id, @promoted_channel_2.id, public_channel.id], results.map(&:id)
    refute results.map(&:id).include?(private_channel.id)
  end

  test "should return private channel followed by user" do
    User.any_instance.stubs(:expert_topics_name).returns([])
    User.any_instance.stubs(:learning_topics_name).returns([])

    private_channel = create :channel, organization: @org, user_id: @owner.id, is_private: true

    2.times do
      user = (create :user, organization: @org)
      create(:follow, followable: private_channel, user: user )
    end
    create(:follow, followable: private_channel, user: @user )

    Search::ChannelSearch.any_instance
      .expects(:search)
      .with(
        q: nil,
        viewer: @user,
        filter: {
          exclude_ids: [],
          is_promoted: true
        },
        limit: 9,
        offset: 0,
        sort: [
          {key: :is_promoted, order: :desc}, {key: :label, order: :asc}
        ],
        load: true
        )
      .returns(
        Search::ChannelSearchResponse.new(results: (@promoted_channels), total: 3)
        )
      .once
    Search::ChannelSearch.any_instance
      .expects(:search)
      .with(
        q: nil,
        viewer: @user,
        filter: {
          exclude_ids: [],
          by_topics: []
        },
        limit: 9,
        offset: 0,
        sort: [
          {key: :is_promoted, order: :desc}, {key: :label, order: :asc}
        ],
        load: true
        )
      .returns(
        Search::ChannelSearchResponse.new(results: ([@channel_6, @channel_7, @channel_1, @channel_4, @channel_8]), total: 3)
        )
      .once
    Search::ChannelSearch.any_instance
      .expects(:search)
      .with(
        q: nil,
        viewer: @user,
        filter: {
          exclude_ids: [],
          by_topics: []
        },
        limit: 9,
        offset: 0,
        sort: [
          {key: :is_promoted, order: :desc}, {key: :label, order: :asc}
        ],
        load: true
        )
      .returns(
        Search::ChannelSearchResponse.new(results: ([]), total: 3)
        )
      .once


    results = DiscoveryService::Channels.new(user: @user,params: { limit: 9, offset: 0 }).discover
    assert_equal [@channel_6.id, @channel_7.id, @channel_1.id, @channel_4.id, @channel_8.id,@promoted_channel_1.id, @promoted_channel_3.id, @promoted_channel_2.id, private_channel.id], results.map(&:id)
    assert_includes results.map(&:id), private_channel.id
  end
end

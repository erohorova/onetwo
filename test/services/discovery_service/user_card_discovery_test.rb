require 'test_helper'

class DiscoveryService::UserCardDicsoveryTest < ActiveSupport::TestCase

  class TrendingCardsTest < ActionController::TestCase
    setup do
      @org   = create :organization
      Role.create_master_roles @org

      @owner = create :user, organization: @org
      @user  = create :user, organization: @org
      @user_profile = create(:user_profile, user: @user, learning_topics: Taxonomies.domain_topics)
      @latest_cards = []
      @old_cards = []
      @private_cards = []
      @latest_pathways = []
      @old_pathways = []
      topics = Taxonomies.domain_topics.collect{|k| k["topic_name"] }
      @sme = create :user, organization: @org
      sme = @org.sme_role
      sme.add_user(@sme)

      ##### CARDS #####
      # TRENDING CARDS
      now = Time.now.utc
      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

      5.times do |n|
        @latest_cards << (card = create :card, author: @owner, taxonomy_topics: topics)
        applicable_offsets.each do |k, v|
          create(:content_level_metric, content_id: card.id, content_type: "card", views_count: n*2, comments_count: n, likes_count: n, completes_count: n, period: k, offset: v)
        end
      end

      # BOOKMARKED CARDS
      @bookmarked_card = create :card, author: @owner
      applicable_offsets.each do |k, v|
        create(:content_level_metric, content_id: @bookmarked_card.id, content_type: "card", views_count: 10*2, comments_count: 10, likes_count: 10, completes_count: 10, period: k, offset: v)
      end
      Bookmark.create(bookmarkable: @bookmarked_card, user: @user)

      # COMPLETED CARDS
      @completed_card = create :card, author: @owner
      applicable_offsets.each do |k, v|
        create(:content_level_metric, content_id: @completed_card.id, content_type: "card", views_count: 10*2, comments_count: 10, likes_count: 10, completes_count: 10, period: k, offset: v)
      end
      create(:user_content_completion, :completed, completable: @completed_card, user: @user)

      # OLD CARDS
      now = 2.months.ago.utc
      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)
      5.times do |n|
        @old_cards << (card = create :card, author: @owner, created_at: 2.months.ago)
        applicable_offsets.each do |k, v|
          create(:content_level_metric, content_id: card.id, content_type: "card", views_count: n*2, comments_count: n, likes_count: n, completes_count: n, period: k, offset: v)
        end
      end

      # PRIVATE CARDS
      2.times do
        @private_cards << (card = create :card, author: @sme, taxonomy_topics: topics, is_public: false)
        applicable_offsets.each do |k, v|
          create(:content_level_metric, content_id: card.id, content_type: "card",
                 views_count: 10*2, comments_count: 20, likes_count: 30,
                 completes_count: 5, period: k, offset: v)
        end
      end

      # SME CARDS
      @sme_cards = create_list :card, 5, author: @sme

      @user.reload
   end

    ## CARDS ##
    test 'Fetch latest trending cards' do
      Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
                    results: @latest_cards,
                    total: 5,
                ))

      @cards = DiscoveryService::UserCardDiscovery.new(user_id: @user.id, limit: 5).trending_cards

      assert_same_elements @latest_cards.map(&:id), @cards.map(&:id)
      assert_equal 5, @cards.count
    end

    test 'should not include private content if no access for user' do
      Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
                    results: (@latest_cards - @private_cards),
                    total: 5,
                ))

      @cards = DiscoveryService::UserCardDiscovery.new(user_id: @user.id, limit: 100).trending_cards
      assert @cards.map(&:id).exclude? @private_cards.first.id
      assert @cards.map(&:id).exclude? @private_cards.last.id
    end

    test "Should not fetch user's bookmarked card" do
      Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
                    results: @latest_cards,
                    total: 5,
                ))

      @cards = DiscoveryService::UserCardDiscovery.new(user_id: @user.id, limit: 5).trending_cards

      refute_includes @cards.map(&:id), @bookmarked_card
    end

    test "Should not fetch user's completed card" do
      Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
                    results: @latest_cards,
                    total: 5,
                ))

      @cards = DiscoveryService::UserCardDiscovery.new(user_id: @user.id, limit: 5).trending_cards

      refute_includes @cards.map(&:id), @completed_card
    end

    test 'Should not include old cards' do
       Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
                    results: @latest_cards,
                    total: 5,
                ))

      @cards = DiscoveryService::UserCardDiscovery.new(user_id: @user.id, limit: 5).trending_cards

      refute_includes @cards.map(&:id), @old_cards.map(&:id)
    end

    test 'Fetch latest trending cards and SME cards' do

      Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
                    results: @latest_cards + @old_cards + @sme_cards,
                    total: 15,
                ))

      @cards = DiscoveryService::UserCardDiscovery.new(user_id: @user.id, limit: 15).trending_cards
      assert_same_elements (@latest_cards.map(&:id) + @sme_cards.map(&:id) + @old_cards.map(&:id)), @cards.map(&:id)

      # Should not include bookmarked and completed cards of user
      refute_includes @cards.map(&:id), [@completed_card.id, @bookmarked_card.id]
    end
  end

  class TrendingPathwaysTest < ActionController::TestCase

    setup do
      @org   = create :organization
      Role.create_master_roles @org

      @owner = create :user, organization: @org
      @user  = create :user, organization: @org
      @user_profile = create(:user_profile, user: @user, learning_topics: Taxonomies.domain_topics)
      @latest_cards = []
      @old_cards = []
      @latest_pathways = []
      @old_pathways = []
      @private_pathways = []
      topics = Taxonomies.domain_topics.collect { |k| k['topic_name'] }
      @sme = create :user, organization: @org
      sme = Role.find_by(name: 'sme', organization: @org)
      sme.add_user(@sme)

      now = Time.now.utc
      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

      ##### PATHWAYS #####
      # TRENDING PATHWAYS
      now = Time.now.utc
      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

      5.times do |n|
        cover = create :card, author: @owner, taxonomy_topics: topics, card_type: 'pack'
        CardPackRelation.add(cover_id: cover.id, add_id: create(:card, author: @owner).id)
        cover.publish!

        @latest_pathways << cover
        applicable_offsets.each do |k, v|
          create(:content_level_metric, content_id: cover.id, content_type: "card", views_count: n*2, comments_count: n, likes_count: n, completes_count: n, period: k, offset: v)
        end
      end

      2.times do |n|
        cover = create :card, author: @owner, taxonomy_topics: topics, card_type: 'pack', is_public: false
        CardPackRelation.add(cover_id: cover.id, add_id: create(:card, author: @owner).id)
        cover.publish!

        @private_pathways << cover
        applicable_offsets.each do |k, v|
          create(:content_level_metric, content_id: cover.id, content_type: "card", views_count: n*2,
            comments_count: n, likes_count: n, completes_count: n, period: k, offset: v)
        end
      end

      # BOOKMARKED PATHWAYS
      @bookmarked_pathway = create :card, author: @owner, card_type: 'pack'
      CardPackRelation.add(cover_id: @bookmarked_pathway.id, add_id: create(:card, author: @owner).id)
      @bookmarked_pathway.publish!

      applicable_offsets.each do |k, v|
        create(:content_level_metric, content_id: @bookmarked_pathway.id, content_type: "card", views_count: 10*2, comments_count: 10, likes_count: 10, completes_count: 10, period: k, offset: v)
      end
      Bookmark.create(bookmarkable: @bookmarked_pathway, user: @user)

      # COMPLETED PATHWAYS
      @completed_pathway = create :card, author: @owner, card_type: 'pack'
      CardPackRelation.add(cover_id: @completed_pathway.id, add_id: create(:card, author: @owner).id)
      @completed_pathway.publish!

      applicable_offsets.each do |k, v|
        create(:content_level_metric, content_id: @completed_pathway.id, content_type: "card", views_count: 10*2, comments_count: 10, likes_count: 10, completes_count: 10, period: k, offset: v)
      end
      create(:user_content_completion, :completed, completable: @completed_pathway, user: @user)

      # OLD PATHWAYS
      now = 2.months.ago.utc
      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)
      5.times do |n|
        cover = create :card, author: @owner, taxonomy_topics: topics, card_type: 'pack'
        CardPackRelation.add(cover_id: cover.id, add_id: create(:card, author: @owner).id)
        cover.publish!

        @old_pathways << cover
        applicable_offsets.each do |k, v|
          create(:content_level_metric, content_id: cover.id, content_type: "card", views_count: n*2, comments_count: n, likes_count: n, completes_count: n, period: k, offset: v)
        end
      end

      # SME PATHWAYS
      @sme_pathways = create_list :card, 5, author: @sme, card_type: 'pack'
      @sme_pathways.each do |cover|
        CardPackRelation.add(cover_id: cover.id, add_id: create(:card, author: @sme).id)
        cover.publish!
      end

      @user.reload
    end

    ## PATHWAYS ##
    test 'Fetch latest trending pathways' do
      Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
                    results: @latest_pathways,
                    total: 5,
                ))

      @pathways = DiscoveryService::UserCardDiscovery.new(
        user_id: @user.id, limit: 5, card_types: ["pack"]
      ).personalized_trending_pathway

      assert_same_elements @latest_pathways.map(&:id), @pathways.map(&:id)
      assert_equal 5, @pathways.count
    end

    test 'should include private pathways if user has access to it' do
      create(:card_user_share, user: @user, card: @private_pathways[0])
      Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
          results: @latest_pathways + @private_pathways,
          total: 7,
      ))
      visible_pathway_ids = @latest_pathways.map(&:id) + [@private_pathways[0].id]
      pathways = DiscoveryService::UserCardDiscovery.new(
        user_id: @user.id, limit: 10, card_types: ["pack"]
      ).personalized_trending_pathway

      assert_equal visible_pathway_ids, pathways.map(&:id)
    end

    test 'should not include private pathways if user has no access to it' do
      Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
                    results: @latest_pathways + @private_pathways,
                    total: 7,
                ))

      pathways = DiscoveryService::UserCardDiscovery.new(
        user_id: @user.id, limit: 10, card_types: ["pack"]
      ).personalized_trending_pathway
      assert pathways.map(&:id).exclude? @private_pathways.first.id
      assert pathways.map(&:id).exclude? @private_pathways.last.id
    end

    test "Should not fetch user's bookmarked pathway" do
      Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
                    results: @latest_pathways,
                    total: 5,
                ))

      @pathways = DiscoveryService::UserCardDiscovery.new(
        user_id: @user.id, limit: 5, card_types: ["pack"]
      ).personalized_trending_pathway

      refute_includes @pathways.map(&:id), @bookmarked_pathway
    end

    test "Should not fetch user's completed pathway" do
      Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
                    results: @latest_pathways,
                    total: 5,
                ))

      @pathways = DiscoveryService::UserCardDiscovery.new(
        user_id: @user.id, limit: 5, card_types: ["pack"]
      ).personalized_trending_pathway
      refute_includes @pathways.map(&:id), @completed_pathway
    end

    test 'Should not include old pathways' do
      Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
                    results: @latest_pathways,
                    total: 5,
                ))

      @pathways = DiscoveryService::UserCardDiscovery.new(
        user_id: @user.id, limit: 5, card_types: ["pack"]
      ).personalized_trending_pathway
      refute_includes @pathways.map(&:id), @old_pathways.map(&:id)
    end

    test 'Fetch latest trending pathways and SME pathways' do
      Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
                    results: @latest_pathways + @sme_pathways + @old_pathways,
                    total: 5,
                ))

      @pathways = DiscoveryService::UserCardDiscovery.new(
        user_id: @user.id, limit: 15, card_types: ["pack"]
      ).personalized_trending_pathway
      assert_same_elements (@latest_pathways.map(&:id) + @sme_pathways.map(&:id) + @old_pathways.map(&:id)), @pathways.map(&:id)

      # Should not include bookmarked and completed pathways of user
      refute_includes @pathways.map(&:id), [@completed_pathway.id, @bookmarked_pathway.id]
    end
  end

  class TrendingOrgPathwayTest < ActionController::TestCase
    setup do
      org = create(:organization)
      @user = create(:user, organization: org)
      create(:config, name: 'enable_trending_org_pathway', value: 'true', configable: org)
      @pathways = create_list(:card, 5, card_type: 'pack', author: @user)
      @pathways.each do |pathway|
        pathway.update_attributes(state: 'published')
      end
      @private_pathways = create_list(:card, 2, card_type: 'pack', author: create(:user, organization: org) ,is_public: false)
      @private_pathways.each do |pathway|
        pathway.update_attributes(state: 'published')
      end
      @draft_pathway = create(:card, card_type: 'pack', author: @user, state: 'draft')

      now = Time.now.utc
      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

      applicable_offsets.each do |k, v|
        create(:content_level_metric, content_id: @pathways[0].id, content_type: "collection", views_count: 20, comments_count: 10, likes_count: 1, completes_count: 1, period: k, offset: v) #440
        create(:content_level_metric, content_id: @pathways[1].id, content_type: "collection", views_count: 30, comments_count: 20, likes_count: 2, completes_count: 2, bookmarks_count: 9, period: k, offset: v) #1050
        create(:content_level_metric, content_id: @pathways[2].id, content_type: "collection", views_count: 40, comments_count: 30, likes_count: 3, completes_count: 4, bookmarks_count: 4, period: k, offset: v) #1265
        create(:content_level_metric, content_id: @pathways[3].id, content_type: "collection", views_count: 20, comments_count: 10, likes_count: 1, completes_count: 3, bookmarks_count: 2, period: k, offset: v) #550
        create(:content_level_metric, content_id: @pathways[4].id, content_type: "collection", views_count: 0, comments_count: 0, likes_count: 0, completes_count: 0, period: k, offset: v) #0
        create(:content_level_metric, content_id: @private_pathways[0].id, content_type: "collection", views_count: 10, comments_count: 5, likes_count: 1, completes_count: 3, period: k, offset: v)
        create(:content_level_metric, content_id: @private_pathways[1].id, content_type: "collection", views_count: 0, comments_count: 0, likes_count: 0, completes_count: 0, period: k, offset: v)
        create(:content_level_metric, content_id: @draft_pathway.id, content_type: 'collection', views_count: 5,
          likes_count: 0, completes_count: 0, period: k, offset: v)
      end
    end

    test 'should return collections in descending order of trending-ness quotient' do
      content_metrics = DiscoveryService::UserCardDiscovery.new(user_id: @user.id,
        limit: 3,
        offset: 0,
        duration: 30.days.ago).trending_organization_pathways
      assert_equal 3, content_metrics.length
      assert_not_includes content_metrics.map(&:content_id), @pathways[4].id
      assert_not_includes content_metrics.map(&:content_id), @pathways[0].id
      assert_equal content_metrics.map(&:content_id), [@pathways[2].id, @pathways[1].id, @pathways[3].id]
    end

    test 'trending_pathways method should return private pathways which user have access to' do
      create(:card_user_share, user: @user, card: @private_pathways[0])
      visible_pathway_ids = @pathways.map(&:id) + [@private_pathways[0].id]
      pathways = DiscoveryService::UserCardDiscovery.new(user_id: @user.id,
        limit: 10,
        offset: 0,
        duration: 30.days.ago).trending_pathways
      assert_equal visible_pathway_ids.sort, pathways.map(&:id).sort
    end

    test 'trending_pathways method should not return pathways which user do not have access to' do
      pathways = DiscoveryService::UserCardDiscovery.new(user_id: @user.id,
        limit: 10,
        offset: 0,
        duration: 30.days.ago).trending_pathways
      assert pathways.map(&:id).exclude? @private_pathways.first.id
      assert pathways.map(&:id).exclude? @private_pathways.last.id
    end

    test 'trending_pathways method should return cards in the exact same order of the content level metric records' do
      cards = DiscoveryService::UserCardDiscovery.new(user_id: @user.id,
        limit: 3,
        offset: 0,
        duration: 30.days.ago).trending_pathways
      assert_equal cards.map(&:id), [@pathways[2].id, @pathways[1].id, @pathways[3].id]
      assert_equal 3, cards.count
    end

    test 'trending_pathways method should not return draft pathways' do
      cards = DiscoveryService::UserCardDiscovery.new(user_id: @user.id,
        limit: 3,
        offset: 0,
        duration: 30.days.ago).trending_pathways
      assert_not_includes cards, @draft_pathway
    end
  end

  class TrendingJourneysTest < ActionController::TestCase
    setup do
      org = create(:organization)
      @user = create(:user, organization: org)
      @card = create(:card, card_type: 'media', author: @user)
      @journeys = create_list(:card, 5, card_type: 'journey', card_subtype: 'self_paced', author: @user)
      @journeys.each do |journey|
        journey.update_attributes(state: 'published')
      end
      @private_journeys = create_list(:card, 2, card_type: 'journey', card_subtype: 'self_paced', author: create(:user, organization: org) ,is_public: false)
      @private_journeys.each do |journey|
        journey.update_attributes(state: 'published')
      end
      @draft_journey = create(:card, card_type: 'journey', card_subtype: 'self_paced', author: @user, state: 'draft')

      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(Time.now.utc)

      applicable_offsets.each do |k, v|
        create(:content_level_metric, content_id: @card.id, content_type: "card", views_count: 100, comments_count: 100, likes_count: 10, completes_count: 10, period: k, offset: v)
        create(:content_level_metric, content_id: @journeys[0].id, content_type: "card", views_count: 20, comments_count: 10, likes_count: 1, completes_count: 1, period: k, offset: v) #440
        create(:content_level_metric, content_id: @journeys[1].id, content_type: "card", views_count: 30, comments_count: 20, likes_count: 2, completes_count: 2, bookmarks_count: 9, period: k, offset: v) #1050
        create(:content_level_metric, content_id: @journeys[2].id, content_type: "card", views_count: 40, comments_count: 30, likes_count: 3, completes_count: 4, bookmarks_count: 4, period: k, offset: v) #1265
        create(:content_level_metric, content_id: @journeys[3].id, content_type: "card", views_count: 20, comments_count: 10, likes_count: 1, completes_count: 3, bookmarks_count: 2, period: k, offset: v) #550
        create(:content_level_metric, content_id: @journeys[4].id, content_type: "card", views_count: 0, comments_count: 0, likes_count: 0, completes_count: 0, period: k, offset: v) #0
        create(:content_level_metric, content_id: @private_journeys[0].id, content_type: "card", views_count: 10, comments_count: 5, likes_count: 1, completes_count: 3, period: k, offset: v)
        create(:content_level_metric, content_id: @private_journeys[1].id, content_type: "card", views_count: 0, comments_count: 0, likes_count: 0, completes_count: 0, period: k, offset: v)
        create(:content_level_metric, content_id: @draft_journey.id, content_type: 'card', views_count: 5,
          likes_count: 0, completes_count: 0, period: k, offset: v)
      end
    end

    test 'trending_journeys method should return private journeys which user have access to' do
      create(:card_user_share, user: @user, card: @private_journeys[0])
      visible_journey_ids = @journeys.map(&:id) + [@private_journeys[0].id]
      journeys = DiscoveryService::UserCardDiscovery.new(user_id: @user.id,
        limit: 10,
        offset: 0,
        duration: 30.days.ago).trending_journeys
      assert_equal visible_journey_ids.sort, journeys.map(&:id).sort
    end

    test 'trending_journeys method should not return journeys which user do not have access to' do
      journeys = DiscoveryService::UserCardDiscovery.new(user_id: @user.id,
        limit: 10,
        offset: 0,
        duration: 30.days.ago).trending_journeys
      assert journeys.map(&:id).exclude? @private_journeys.first.id
      assert journeys.map(&:id).exclude? @private_journeys.last.id
    end

    test 'trending_journeys method should return cards in the exact same order of the content level metric records' do
      cards = DiscoveryService::UserCardDiscovery.new(user_id: @user.id,
        limit: 3,
        offset: 0,
        duration: 30.days.ago).trending_journeys
      assert_equal cards.map(&:id), [@journeys[2].id, @journeys[1].id, @journeys[3].id]
      assert_equal 3, cards.count
    end

    test 'trending_journeys method should not return draft journeys' do
      cards = DiscoveryService::UserCardDiscovery.new(user_id: @user.id,
        limit: 3,
        offset: 0,
        duration: 30.days.ago).trending_journeys
      assert_not_includes cards, @draft_journey
    end

    test 'trending_journeys should return journey when limit is 1 and card has greater total_score than the journey' do
      cards = DiscoveryService::UserCardDiscovery.new(user_id: @user.id,
        limit: 1,
        offset: 0,
        duration: 30.days.ago).trending_journeys
      assert_equal @journeys[2].id, cards[0].id
    end
  end

  class PersonalizedTrendingJourneysTest < ActionController::TestCase

    setup do
      @org   = create :organization
      Role.create_master_roles @org

      @owner = create :user, organization: @org
      @user  = create :user, organization: @org
      @user_profile = create(:user_profile, user: @user, learning_topics: Taxonomies.domain_topics)
      @latest_journeys = []
      @old_journeys = []
      @private_journeys = []
      topics = Taxonomies.domain_topics.collect { |k| k['topic_name'] }
      @sme = create :user, organization: @org
      sme = Role.find_by(name: 'sme', organization: @org)
      sme.add_user(@sme)

      ##### JOURNEYS #####
      # TRENDING JOURNEYS
      now = Time.now.utc
      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)

      5.times do |n|
        cover = create :card, author: @owner, taxonomy_topics: topics, card_type: 'journey', card_subtype: 'self_paced'
        cover.publish!

        @latest_journeys << cover
        applicable_offsets.each do |k, v|
          create(:content_level_metric, content_id: cover.id, content_type: "card", views_count: n*2, comments_count: n, likes_count: n, completes_count: n, period: k, offset: v)
        end
      end

      2.times do |n|
        cover = create :card, author: @owner, taxonomy_topics: topics, card_type: 'journey',
          card_subtype: 'self_paced', is_public: false
        cover.publish!

        @private_journeys << cover
        applicable_offsets.each do |k, v|
          create(:content_level_metric, content_id: cover.id, content_type: "card", views_count: n*2,
            comments_count: n, likes_count: n, completes_count: n, period: k, offset: v)
        end
      end

      # BOOKMARKED JOURNEYS
      @bookmarked_journey = create :card, author: @owner, card_type: 'journey', card_subtype: 'self_paced'
      @bookmarked_journey.publish!

      applicable_offsets.each do |k, v|
        create(:content_level_metric, content_id: @bookmarked_journey.id, content_type: "card", views_count: 10*2, comments_count: 10, likes_count: 10, completes_count: 10, period: k, offset: v)
      end
      Bookmark.create(bookmarkable: @bookmarked_journey, user: @user)

      # COMPLETED JOURNEYS
      @completed_journey = create :card, author: @owner, card_type: 'journey', card_subtype: 'self_paced'
      @completed_journey.publish!

      applicable_offsets.each do |k, v|
        create(:content_level_metric, content_id: @completed_journey.id, content_type: "card", views_count: 10*2, comments_count: 10, likes_count: 10, completes_count: 10, period: k, offset: v)
      end
      create(:user_content_completion, :completed, completable: @completed_journey, user: @user)

      # OLD JOURNEYS
      now = 2.months.ago.utc
      applicable_offsets = PeriodOffsetCalculator.applicable_offsets(now)
      5.times do |n|
        cover = create :card, author: @owner, taxonomy_topics: topics, card_type: 'journey', card_subtype: 'self_paced'
        cover.publish!

        @old_journeys << cover
        applicable_offsets.each do |k, v|
          create(:content_level_metric, content_id: cover.id, content_type: "card", views_count: n*2, comments_count: n, likes_count: n, completes_count: n, period: k, offset: v)
        end
      end

      # SME JOURNEYS
      @sme_journeys = create_list :card, 5, author: @sme, card_type: 'journey', card_subtype: 'self_paced'
      @sme_journeys.each do |cover|
        CardPackRelation.add(cover_id: cover.id, add_id: create(:card, author: @sme).id)
        cover.publish!
      end

      @user.reload
    end

    ## JOURNEYS ##
    test 'Fetch latest trending journeys' do
      Search::CardSearch.any_instance.stubs(:search).returns(
        Search::CardSearchResponse.new(results: @latest_journeys, total: 5)
      )

      @journeys = DiscoveryService::UserCardDiscovery.new(
        user_id: @user.id, limit: 5, card_types: ["journey"]
      ).personalized_trending_journey

      assert_same_elements @latest_journeys.map(&:id), @journeys.map(&:id)
      assert_equal 5, @journeys.count
    end

    test 'should include private journeys if user has access to it' do
      create(:card_user_share, user: @user, card: @private_journeys[0])
      Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
        results: @latest_journeys + @private_journeys,
        total: 7
      ))
      visible_journey_ids = @latest_journeys.map(&:id) + [@private_journeys[0].id]
      journeys = DiscoveryService::UserCardDiscovery.new(
        user_id: @user.id, limit: 10, card_types: ["pack"]
      ).personalized_trending_journey

      assert_equal visible_journey_ids, journeys.map(&:id)
    end

    test 'should not include private journeys if user has no access to it' do
      Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
        results: @latest_journeys + @private_journeys,
        total: 7,
      ))

      journeys = DiscoveryService::UserCardDiscovery.new(
        user_id: @user.id, limit: 10, card_types: ["journey"]
      ).personalized_trending_journey
      assert journeys.map(&:id).exclude? @private_journeys.first.id
      assert journeys.map(&:id).exclude? @private_journeys.last.id
    end

    test "Should not fetch user's bookmarked journey" do
      Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
        results: @latest_journeys,
        total: 5,
      ))

      @journeys = DiscoveryService::UserCardDiscovery.new(
        user_id: @user.id, limit: 5, card_types: ["journey"]
      ).personalized_trending_journey

      refute_includes @journeys.map(&:id), @bookmarked_journey
    end

    test "Should not fetch user's completed journey" do
      Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
        results: @latest_journeys,
        total: 5,
      ))

      @journeys = DiscoveryService::UserCardDiscovery.new(
        user_id: @user.id, limit: 5, card_types: ["journey"]
      ).personalized_trending_journey
      refute_includes @journeys.map(&:id), @completed_journey
    end

    test 'Should not include old journeys' do
      Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
        results: @latest_journeys,
        total: 5,
      ))

      @journeys = DiscoveryService::UserCardDiscovery.new(
        user_id: @user.id, limit: 5, card_types: ["journey"]
      ).personalized_trending_journey
      refute_includes @journeys.map(&:id), @old_journeys.map(&:id)
    end

    test 'Fetch latest trending journeys and SME journeys' do
      Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
        results: @latest_journeys + @sme_journeys + @old_journeys,
        total: 5,
      ))

      @journeys = DiscoveryService::UserCardDiscovery.new(
        user_id: @user.id, limit: 15, card_types: ["journey"]
      ).personalized_trending_journey
      assert_same_elements (@latest_journeys.map(&:id) + @sme_journeys.map(&:id) + @old_journeys.map(&:id)), @journeys.map(&:id)

      # Should not include bookmarked and completed journeys of user
      refute_includes @journeys.map(&:id), [@completed_journey.id, @bookmarked_journey.id]
    end
  end

  class RelevantInterestedCards < ActionController::TestCase
    test 'Should return relevant interested cards' do
      @org   = create :organization
      @owner = create :user, organization: @org
      @user  = create :user, organization: @org
      topics = Taxonomies.domain_topics.collect{|k| k["topic_name"] }
      @cards = []
      @not_interested_cards = []
      @channel_ids = @owner.followed_channel_ids
      @cards = create_list :card, 2, author: @owner, taxonomy_topics: topics, state: 'published'
      @not_interested_cards = create_list :card, 2, author: @owner, state: 'published'

      @interested_cards = [@cards].flatten
      @user.reload

      Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
        results: @interested_cards,
        total: 5,
      ))

      @result = DiscoveryService::UserCardDiscovery.new(user_id: @owner.id,
      channel_ids: @channel_ids,
      limit: @org.number_of_learning_items).relevent_interested_cards

      assert_equal @interested_cards, @result
    end
  end
end

require 'test_helper'

class DiscoveryService::UsersTest < ActiveSupport::TestCase

  def create_team_members
    @teams = []
    (1..4).each do |i|
      @teams << create(:team, organization: @org, created_at: i.minutes.ago)
    end

    @team1, @team2, @team3, @team4 = *@teams[0..3]
    @team1.add_admin @admin1
    @team2.add_admin @admin2
    @team3.add_admin @admin3  # @admin3 is suspended user
    @team4.add_admin @admin4  # @team4 contains only admin user, no member

    #add current_user to teams except @team4
    [@team1, @team2, @team3].each {|team| team.add_member @user}

    # add more members to each team except @team4
    @team1_user, @team2_user, @team3_user, @team4_user = create_list :user, 4, organization: @org
    @team1.add_member(@team1_user)
    @team2.add_member(@team2_user)
    @team3.add_member(@team3_user)
  end

  def create_influencers
    @influencer_role = create :role, organization: @org, name: 'influencer'
    @influencer1, @influencer2, @influencer3, @influencer4, @influencer5 = create_list :user, 5, organization: @org

    @influencer_role.add_user(@influencer1)
    @influencer_role.add_user(@influencer2)
    @influencer_role.add_user(@influencer3)
    @influencer_role.add_user(@influencer4)
    @influencer_role.add_user(@influencer5)
    @follower1, @follower2, @follower3, @follower4, @follower5, @follower6 = create_list :user, 6, organization: @org

    #follow @admin2
    create :follow, user: @user, followable: @admin2

    create :follow, user: @user, followable: @influencer1
    create :follow, user: @user, followable: @influencer2
    create :follow, user: @user, followable: @influencer3

    create :follow, user: @influencer1, followable: @follower1
    create :follow, user: @influencer1, followable: @follower2
    create :follow, user: @influencer1, followable: @follower3
    create :follow, user: @influencer2, followable: @follower4
    create :follow, user: @influencer2, followable: @follower5
    create :follow, user: @influencer3, followable: @follower6
    create :follow, user: @influencer4, followable: @follower1
    create :follow, user: @influencer4, followable: @follower2
    create :follow, user: @influencer4, followable: @follower3
    create :follow, user: @influencer5, followable: @follower4
    create :follow, user: @influencer5, followable: @follower5
  end

  def create_org_data
    @org = create :organization
    Role.create_master_roles @org
    @sme = @org.sme_role

    # SMEs
    @normal_sme = create(:user, organization: @org)
    @sme.add_user(@normal_sme)

    # Current User
    @user = create(:user, organization: @org)
    create(:user_profile, user: @user, learning_topics: Taxonomies.domain_topics)

    # Admin user
    @edcast_admin = create(:user, is_edcast_admin: true)
    create(:user_profile, user: @edcast_admin, expert_topics: Taxonomies.domain_topics.first(1))

    # other expert
    @other_user = create(:user, organization: @org)
    create(:user_profile, user: @other_user, expert_topics: Taxonomies.domain_topics.first(1))

    # to be used in create team admins
    @admin1, @admin2, @admin3, @admin4 = create_list :user, 4, organization: @org

    # Suspended User
    @admin3.update_column(:is_suspended, true)
  end

  def create_follower_data(number)
    @following_users = create_list(:user, number, organization: @org)
    @following_users.each do |u|
      create(:user_profile, user: u, learning_topics: Taxonomies.domain_topics.first(2))
      create :follow, user: @user, followable: u
    end
  end

  def create_channel_followers(number)
    @channel_users = create_list(:user, number, organization: @org)
    @channel = create(:channel, organization: @org)
    @channel.add_followers @channel_users
    @channel.add_followers [@user]
  end

  def create_recommended_users(number)
    @recommended_users = create_list(:user, number, organization: @org)
    @recommended_users.each {|u| create(:user_profile, user: u, expert_topics: Taxonomies.domain_topics.first(2)) }
  end

  def create_peer_learners(number)
    @peer_learners = create_list(:user, number, organization: @org)
    @peer_learners.each {|u| create(:user_profile, user: u, learning_topics: Taxonomies.domain_topics.first(2)) }
  end

  setup do
    create_org_data
  end

  test 'should return experts' do
    create_recommended_users(2)
    Search::UserSearch.any_instance.expects(:search).with(
      q: nil,
      viewer: @user,
      filter: {search_expertise: true, role: "sme"},
      allow_blank_q: true,
      limit: User::MAX_SEARCH,
      sort: :score,
      load: false
      ).returns(
        Search::UserSearchResponse.new(
          results: (@recommended_users + [@other_user]),
          total: 5
      )).once

    @results = DiscoveryService::Users.new(@user.id, 3, 0, false).recommendations
    assert_includes @results.map {|u| u['id']}, @recommended_users.first.id
    assert_includes @results.map {|u| u['id']}, @recommended_users.last.id
  end

  test 'should return experts with role name instead of default name' do
    create_recommended_users(2)
    @sme.name = "new_name"
    @sme.save
    Search::UserSearch.any_instance.expects(:search).with(
      q: nil,
      viewer: @user,
      filter: {search_expertise: true, role: @sme.name },
      allow_blank_q: true,
      limit: User::MAX_SEARCH,
      sort: :score,
      load: false
      ).returns(
        Search::UserSearchResponse.new(
          results: (@recommended_users + [@other_user]),
          total: 5
      )).once

    @results = DiscoveryService::Users.new(@user.id, 3, 0, false).recommendations
    assert_includes @results.map {|u| u['id']}, @recommended_users.first.id
    assert_includes @results.map {|u| u['id']}, @recommended_users.last.id
  end

  test 'should return experts with only_team_users flag true' do
    create_recommended_users(2)
    create_team_members
    @team4.add_member(@team4_user)
    Search::UserSearch.any_instance.expects(:search).with(
      q: nil,
      viewer: @user,
      filter: {search_expertise: true, role: "sme"},
      allow_blank_q: true,
      limit: User::MAX_SEARCH,
      sort: :score,
      load: false
      ).returns(
        Search::UserSearchResponse.new(
          results: (@recommended_users + [@other_user]),
          total: 5
      )).once

    @results = DiscoveryService::Users.new(@user.id, 5, 0, true).recommendations
    assert_includes @results.map {|u| u['id']}, @team2_user.id
    assert_includes @results.map {|u| u['id']}, @team3_user.id
    assert_not_includes @results.map {|u| u['id']}, @team4_user.id
  end

  test 'should return experts when only_sme_carousel is TRUE' do
    create_recommended_users(1)
    create(:config, name: 'enable_only_sme_on_people_carousel', value: true, configable: @org)
    @results = DiscoveryService::Users.new(@user.id, 2, 0, false).recommendations
    assert_includes @results.map {|u| u['id']}, @normal_sme.id
    assert_not_includes @results.map {|u| u['id']}, @other_user.id
  end

  test 'should return peer learners too if number of users to follow is less than limit' do
    create_recommended_users(1)
    create_influencers
    create_team_members
    create_peer_learners(2)
    Search::UserSearch.any_instance.expects(:search).with(
      q: nil,
      viewer: @user,
      filter: {search_expertise: true, role: "sme"},
      allow_blank_q: true,
      limit: User::MAX_SEARCH,
      sort: :score,
      load: false
      ).returns(
        Search::UserSearchResponse.new(
          results: (@recommended_users + [@other_user]),
          total: 5
      )).once

    Search::UserSearch.any_instance.expects(:search).with(
      q: nil,
      viewer: @user,
      filter: {search_peer_learners: true},
      allow_blank_q: true,
      limit: User::MAX_SEARCH,
      sort: :score,
      load: false
      ).returns(
        Search::UserSearchResponse.new(
          results: (@peer_learners),
          total: 4
      )).once

    @results = DiscoveryService::Users.new(@user.id, 11, 0, false).recommendations
    assert_includes @results.map {|u| u['id']}, @peer_learners.first.id
    assert_includes @results.map {|u| u['id']}, @peer_learners.last.id
  end

  test 'should include users from teams I am following and channels too' do
    create_recommended_users(1) # 2
    create_influencers # 2 + 1 SME
    create_team_members # 4
    create_channel_followers(2)

    Search::UserSearch.any_instance.expects(:search).with(
      q: nil,
      viewer: @user,
      filter: {search_expertise: true, role: "sme"},
      allow_blank_q: true,
      limit: User::MAX_SEARCH,
      sort: :score,
      load: false
      ).returns(
        Search::UserSearchResponse.new(
          results: (@recommended_users + [@other_user]),
          total: 5
      )).once

    @results = DiscoveryService::Users.new(@user.id, 11, 0, false).recommendations
    assert_includes @results.map {|u| u['id']}, @channel_users.first.id
    assert_includes @results.map {|u| u['id']}, @channel_users.last.id
    assert_includes @results.map {|u| u['id']}, @team1_user.id
    assert_includes @results.map {|u| u['id']}, @team2_user.id
  end

  test "should return normal experts too" do
    create_recommended_users(1)

    Search::UserSearch.any_instance.expects(:search).with(
      q: nil,
      viewer: @user,
      filter: {search_expertise: true, role: "sme"},
      allow_blank_q: true,
      limit: User::MAX_SEARCH,
      sort: :score,
      load: false
      ).returns(
        Search::UserSearchResponse.new(
          results: (@recommended_users + [@other_user]),
          total: 2
      )).once

    @results = DiscoveryService::Users.new(@user.id, 3, 0, false).recommendations
    assert_includes @results.map {|u| u['id']}, @other_user.id
  end

  test "should return already following users" do
    create_recommended_users(1)
    create_influencers
    create_team_members
    create_peer_learners(1)
    create_follower_data(2)

    Search::UserSearch.any_instance.expects(:search).with(
      q: nil,
      viewer: @user,
      filter: {search_expertise: true, role: "sme"},
      allow_blank_q: true,
      limit: User::MAX_SEARCH,
      sort: :score,
      load: false
      ).returns(
        Search::UserSearchResponse.new(
          results: (@recommended_users + [@other_user]),
          total: 2
      )).once

    Search::UserSearch.any_instance.expects(:search).with(
      q: nil,
      viewer: @user,
      filter: {search_expertise: true, following: true, role: "sme"},
      allow_blank_q: true,
      limit: User::MAX_SEARCH,
      sort: :score,
      load: false
      ).returns(
        Search::UserSearchResponse.new(
          results: (@following_users),
          total: 5
      )).once

    Search::UserSearch.any_instance.expects(:search).with(
      q: nil,
      viewer: @user,
      filter: {search_peer_learners: true},
      allow_blank_q: true,
      limit: User::MAX_SEARCH,
      sort: :score,
      load: false
      ).returns(
        Search::UserSearchResponse.new(
          results: (@peer_learners),
          total: 5
      )).once

    @results = DiscoveryService::Users.new(@user.id, 12, 0, false).recommendations
    assert_includes @results.map {|u| u['id']}, @following_users.first.id
  end

  # will only include influencers which current user is not following
  test "should return influencers by number of followers in desc order" do
    create_recommended_users(1)
    create_influencers
    Search::UserSearch.any_instance.expects(:search).with(
      q: nil,
      viewer: @user,
      filter: {search_expertise: true, role: "sme"},
      allow_blank_q: true,
      limit: User::MAX_SEARCH,
      sort: :score,
      load: false
      ).returns(
        Search::UserSearchResponse.new(
          results: (@recommended_users + [@other_user]),
          total: 2
      )).once

    @results = DiscoveryService::Users.new(@user.id, 5, 0, false).recommendations
    assert_includes @results.map {|u| u['id']}, @influencer4.id
    assert_includes @results.map {|u| u['id']}, @influencer5.id
  end

  # will also include current user already following influencers
  test "should return following influencers by number of followers in desc order" do
    create_recommended_users(1)
    create_influencers
    create_team_members
    create_channel_followers(1)
    create_follower_data(1)
    create_peer_learners(1)
    Search::UserSearch.any_instance.expects(:search).with(
      q: nil,
      viewer: @user,
      filter: {search_expertise: true, role: "sme"},
      allow_blank_q: true,
      limit: User::MAX_SEARCH,
      sort: :score,
      load: false
      ).returns(
        Search::UserSearchResponse.new(
          results: (@recommended_users + [@other_user]),
          total: 2
      )).once

    Search::UserSearch.any_instance.expects(:search).with(
      q: nil,
      viewer: @user,
      filter: {search_peer_learners: true},
      allow_blank_q: true,
      limit: User::MAX_SEARCH,
      sort: :score,
      load: false
      ).returns(
        Search::UserSearchResponse.new(
          results: (@peer_learners),
          total: 5
      )).once

    Search::UserSearch.any_instance.expects(:search).with(
      q: nil,
      viewer: @user,
      filter: {search_expertise: true, following: true, role: "sme"},
      allow_blank_q: true,
      limit: User::MAX_SEARCH,
      sort: :score,
      load: false
      ).returns(
        Search::UserSearchResponse.new(
          results: (@following_users),
          total: 5
      )).once

    @results = DiscoveryService::Users.new(@user.id, 16, 0, false).recommendations
    assert_includes @results.map {|u| u['id']}, @influencer1.id
    assert_includes @results.map {|u| u['id']}, @influencer2.id
    assert_includes @results.map {|u| u['id']}, @influencer3.id
    assert_includes @results.map {|u| u['id']}, @admin2.id
  end

  test "should return group leaders" do
    create_recommended_users(1)
    create_team_members

    Search::UserSearch.any_instance.expects(:search).with(
      q: nil,
      viewer: @user,
      filter: {search_expertise: true, role: "sme"},
      allow_blank_q: true,
      limit: User::MAX_SEARCH,
      sort: :score,
      load: false
      ).returns(
        Search::UserSearchResponse.new(
          results: (@recommended_users + [@other_user]),
          total: 2
      )).once

    @results = DiscoveryService::Users.new(@user.id, 4, 0, false).recommendations
    assert_includes @results, @admin1
    assert_not_includes @results, @admin4
  end

  test "should return smes" do
    create_recommended_users(1)
    Search::UserSearch.any_instance.expects(:search).with(
      q: nil,
      viewer: @user,
      filter: {search_expertise: true, role: "sme"},
      allow_blank_q: true,
      limit: User::MAX_SEARCH,
      sort: :score,
      load: false
      ).returns(
        Search::UserSearchResponse.new(
          results: (@recommended_users + [@other_user]),
          total: 2
      )).once

    @results = DiscoveryService::Users.new(@user.id, 3, 0, false).recommendations
    assert_includes @results, @normal_sme
  end

  test "should not return edcast admin" do
    create_recommended_users(1)
    Search::UserSearch.any_instance.expects(:search).with(
      q: nil,
      viewer: @user,
      filter: {search_expertise: true, role: "sme"},
      allow_blank_q: true,
      limit: User::MAX_SEARCH,
      sort: :score,
      load: false
      ).returns(
        Search::UserSearchResponse.new(
          results: (@recommended_users + [@other_user]),
          total: 2
      )).once

    @results = DiscoveryService::Users.new(@user.id, 2, 0, false).recommendations
    assert_not_includes @results, @edcast_admin
  end

  test "should not return suspended users" do
    create_recommended_users(1)
    Search::UserSearch.any_instance.expects(:search).with(
      q: nil,
      viewer: @user,
      filter: {search_expertise: true, role: "sme"},
      allow_blank_q: true,
      limit: User::MAX_SEARCH,
      sort: :score,
      load: false
      ).returns(
        Search::UserSearchResponse.new(
          results: (@recommended_users + [@other_user]),
          total: 2
      )).once

    @results = DiscoveryService::Users.new(@user.id, 2, 0, false).recommendations
    assert_not_includes @results, @admin3
  end

  test "should not return self user" do
    create_recommended_users(1)
    Search::UserSearch.any_instance.expects(:search).with(
      q: nil,
      viewer: @user,
      filter: {search_expertise: true, role: "sme"},
      allow_blank_q: true,
      limit: User::MAX_SEARCH,
      sort: :score,
      load: false
      ).returns(
        Search::UserSearchResponse.new(
          results: (@recommended_users + [@other_user]),
          total: 2
      )).once

    @results = DiscoveryService::Users.new(@user.id, 2, 0, false).recommendations
    assert_not_includes  @results, @user
  end
end

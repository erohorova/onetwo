require 'test_helper'

class DiscoveryService::CarouselContentTest < ActiveSupport::TestCase
  setup do
    @org    = create :organization
    owner = create :user, organization: @org
    @user  = create :user, organization: @org

    @cards = (create_list :card, 2, author: owner)
    card_hash = []

    @cards.each_with_index do |card, index|
      card_hash << {"card_id" => card.id, "index" => index}
    end

    @carousel_item = {
      "defaultLabel" => "User given label",
      "visible" => true,
      "index" => 1,
      "cards" => card_hash
    }
  end

  test '#get_content' do
    DiscoveryService::CarouselContent.any_instance.stubs(:get_carousel).returns(@carousel_item)
    assert_same_elements DiscoveryService::CarouselContent.new(@user.id, "uuid-1234").get_content, @cards

    DiscoveryService::CarouselContent.any_instance.stubs(:get_carousel).returns(nil)
    assert_nil DiscoveryService::CarouselContent.new(@user.id, "uuid-1234").get_content
  end

  test 'should fetch #get_carousel' do
    config_discover = {
        "version":"4.7",
        "discover":{
          "discover/carousel/1bpro9o06":{"defaultLabel":"EdCast Instances","visible":true,"index":18,"cards":[{"card_id": @cards[0].id,"index":0},
                        {"card_id": @cards[1].id,"index":0}]
        }}}.to_json

    config = create(:config, name: "OrgCustomizationConfig", value: config_discover, data_type: 'json', configable_id: @org.id, configable_type: 'Organization')

    org_customization_config = @org.configs.find_by(name: "OrgCustomizationConfig")
    parsed_value = org_customization_config&.parsed_value
    _key = parsed_value["discover"].keys.first
    DiscoveryService::CarouselContent.new(@user.id,"1bpro9o06")

    assert_equal org_customization_config.parsed_value["discover"]["discover/carousel/1bpro9o06"] , parsed_value["discover"][_key]
  end
end

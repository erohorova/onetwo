require 'test_helper'

class DiscoveryService::VideosTest < ActiveSupport::TestCase
  include CardHelperMethods
  setup do
    @org   = create :organization
    @owner = create :user, organization: @org
    @user  = create :user, organization: @org
    @user_profile = create(:user_profile, user: @user, learning_topics: Taxonomies.domain_topics)
    @latest_cards = []
    @old_cards = []

    # TRENDING CARDS
    @latest_cards << (create_list :video_card, 3, author: @owner, is_public: true)
    @latest_cards << (create_list :video_stream_card, 2, author: @owner, is_public: true)
    @latest_cards.flatten!

    # BOOKMARKED CARDS
    @bookmarked_card = create :video_card, author: @owner, is_public: true
    Bookmark.create(bookmarkable: @bookmarked_card, user: @user)

    # PROMOTED CARDS
    @promoted_card = create :video_card, author: @owner, title: 'Promoted', is_official: true, is_public: true

    # COMPLETED CARDS
    @completed_card = create :video_card, author: @owner, is_public: true
    create(:user_content_completion, :completed, completable: @completed_card, user: @user)

    # SME CARDS
    @sme = create :user, organization: @org
    sme = create :role, organization: @org, name: 'SME'
    sme.add_user(@sme)
    @sme_cards = create_list :video_card, 5, author: @sme, taxonomy_topics: nil, is_public: true

    @user.reload
  end

  test 'Fetch latest trending video cards' do
    Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
                  results: search_results(@latest_cards),
                  total: 5,
              ))
    @results = DiscoveryService::Videos.new(@user.id, 5).video_cards

    assert_same_elements @latest_cards.map(&:id), @results.map(&:id)
    assert_equal 5, @results.count
  end

  test "Should not fetch user's bookmarked video card" do
    Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
                  results: search_results(@latest_cards),
                  total: 5,
              ))
    @results = DiscoveryService::Videos.new(@user.id, 5).video_cards

    assert_same_elements @latest_cards.map(&:id), @results.map(&:id)
    refute_includes @results.map(&:id), @bookmarked_card
  end

  test "Should not fetch user's completed video card" do
    Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
                  results: search_results(@latest_cards),
                  total: 5,
              ))
    @results = DiscoveryService::Videos.new(@user.id, 5).video_cards

    assert_same_elements @latest_cards.map(&:id), @results.map(&:id)
    refute_includes @results.map(&:id), @completed_card
  end

  test 'Should not include old video cards' do
    Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
                  results: search_results(@latest_cards),
                  total: 5,
              ))
    @results = DiscoveryService::Videos.new(@user.id, 5).video_cards

    assert_same_elements @latest_cards.map(&:id), @results.map(&:id)
    refute_includes @results.map(&:id), @old_cards.map(&:id)
  end

  test 'Fetch latest trending video cards, promoted and SME video cards' do
    Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
                  results: search_results(@latest_cards + [@promoted_card] + @sme_cards + @old_cards),
                  total: 5,
              ))

    @results = DiscoveryService::Videos.new(@user.id, 12).video_cards
    assert_equal (@latest_cards.map(&:id) + [@promoted_card.id] + @sme_cards.map(&:id) + @old_cards.map(&:id)), @results.map(&:id)

    # Should not include bookmarked and completed video cards of user
    refute_includes @results.map(&:id), [@completed_card.id, @bookmarked_card.id]
  end

  test 'Fetch video cards using search term' do
    Search::CardSearch.any_instance.stubs(:search).returns(Search::CardSearchResponse.new(
                  results: search_results([@promoted_card]),
                  total: 5,
              ))

    @results = DiscoveryService::Videos.new(@user.id, 5, 0, 'Promote').video_cards
    assert_equal [@promoted_card.id], @results.map(&:id)
  end
end

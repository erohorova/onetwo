require 'test_helper'

class UserBadgeServiceTest < ActiveSupport::TestCase
  setup do
    @org = create(:organization)
    @user = create(:user, organization: @org)
  end

  def prepare_pack_data
    @cover = create :card, author: @user, organization_id: @org.id, card_type: 'pack'
    @member = create :user, organization: @org

    card_badge    = create :card_badge, organization: @org
    @card_badging = @cover.create_card_badging(badge_id: card_badge.id, title: 'Badge title', all_quizzes_answered: true)

    @card1 = create(:card, card_type: 'poll', author: @user, organization_id: @org.id)
    @option1 = create(:quiz_question_option, label: 'a', quiz_question: @card1, is_correct: true)
    @option2 = create(:quiz_question_option, label: 'b', quiz_question: @card1)
    @option3 = create(:quiz_question_option, label: 'c', quiz_question: @card1)

    @card2 = create(:card, card_type: 'poll', author: @user, organization_id: @org.id)
    @option4 = create(:quiz_question_option, label: 'a', quiz_question: @card2)
    @option5 = create(:quiz_question_option, label: 'b', quiz_question: @card2, is_correct: true)
    @option6 = create(:quiz_question_option, label: 'c', quiz_question: @card2)

    CardPackRelation.add(cover_id: @cover.id, add_id: @card1.id, add_type: 'Card')
    CardPackRelation.add(cover_id: @cover.id, add_id: @card2.id, add_type: 'Card')
  end

  test "should allocate card badge" do
    @card = create(:card, author: @user, organization: @org)
    @card_badge = create(:card_badge, organization: @user.organization)
    @card_badging = @card.create_card_badging(badge_id: @card_badge.id, title: "dummy title")

    @service = UserBadgeService.new("Card", @card, @user.id).assign_badge
    assert_equal 1, UserBadge.where(badging_id: @card_badging.id, user_id: @user.id).count
  end

  test "should allocate clc badge" do
    @clc_badge1 = create(:clc_badge, organization: @user.organization)
    @clc_badge2 = create(:clc_badge, organization: @user.organization)
    @clc_badge3 = create(:clc_badge, organization: @user.organization)
    @clc_badge4 = create(:clc_badge, organization: @user.organization)
    @clc = create(:clc, name: 'test clc', entity: @org, organization: @org,
                  clc_badgings_attributes: [{badge_id: @clc_badge1.id, title: "clc-25", "target_steps":25},
                                            {badge_id: @clc_badge2.id, title: "clc-50", "target_steps":50},
                                            {badge_id: @clc_badge3.id, title: "clc-75", "target_steps":75},
                                            {badge_id: @clc_badge4.id, title: "clc-100", "target_steps":100}
                                           ])
    @service = UserBadgeService.new("Clc", @clc, @user.id, 100).assign_badge
    assert_equal 1, UserBadge.where(user_id: @user.id).count
  end

  test "should allocate clc badge sequentially (25 -> 50 -> 75 -> 100)" do
    @clc_badge1 = create(:clc_badge, organization: @user.organization)
    @clc_badge2 = create(:clc_badge, organization: @user.organization)
    @clc_badge3 = create(:clc_badge, organization: @user.organization)
    @clc_badge4 = create(:clc_badge, organization: @user.organization)
    @clc = create(:clc, name: 'test clc', entity: @org, organization: @org,
                  clc_badgings_attributes: [{badge_id: @clc_badge1.id, title: "clc-25", "target_steps":25},
                                            {badge_id: @clc_badge2.id, title: "clc-50", "target_steps":50},
                                            {badge_id: @clc_badge3.id, title: "clc-75", "target_steps":75},
                                            {badge_id: @clc_badge4.id, title: "clc-100", "target_steps":100}
                                           ])

    @clc_badging1 = @clc.clc_badgings.where(target_steps: 25, badgeable_id: @clc.id).first
    @service = UserBadgeService.new("Clc", @clc, @user.id, @clc_badging1.target_steps).assign_badge
    assert_equal 1, UserBadge.where(user_id: @user.id).count
    assert_equal @clc_badging1.id, UserBadge.where(user_id: @user.id).last.badging_id

    @clc_badging2 = @clc.clc_badgings.where(target_steps: 50, badgeable_id: @clc.id).first
    @service = UserBadgeService.new("Clc", @clc, @user.id, @clc_badging2.target_steps).assign_badge
    assert_equal 1, UserBadge.where(user_id: @user.id).count
    assert_equal @clc_badging2.id, UserBadge.where(user_id: @user.id).last.badging_id

    @clc_badging3 = @clc.clc_badgings.where(target_steps: 75, badgeable_id: @clc.id).first
    @service = UserBadgeService.new("Clc", @clc, @user.id, @clc_badging3.target_steps).assign_badge
    assert_equal 1, UserBadge.where(user_id: @user.id).count
    assert_equal @clc_badging3.id, UserBadge.where(user_id: @user.id).last.badging_id

    @clc_badging4 = @clc.clc_badgings.where(target_steps: 100, badgeable_id: @clc.id).first
    @service = UserBadgeService.new("Clc", @clc, @user.id, @clc_badging4.target_steps).assign_badge
    assert_equal 1, UserBadge.where(user_id: @user.id).count
    assert_equal @clc_badging4.id, UserBadge.where(user_id: @user.id).last.badging_id
  end

  test 'grants badge when all quizzes in a pathway are attempted correctly' do
    prepare_pack_data

    attempt1 = create(:quiz_question_attempt, user: @member, quiz_question: @card1, selections: [@option1.id])
    attempt2 = create(:quiz_question_attempt, user: @member, quiz_question: @card2, selections: [@option5.id])

    UserBadgeService.new('Card', @cover, @member.id).assign_badge
    assert_equal 1, UserBadge.where(badging_id: @card_badging.id, user_id: @member.id).count
  end

  test 'do not grants badge when quizzes in a pathway are not attempted completely' do
    prepare_pack_data

    attempt1 = create(:quiz_question_attempt, user: @member, quiz_question: @card1, selections: [@option1.id])

    UserBadgeService.new('Card', @cover, @member.id).assign_badge
    assert_empty UserBadge.where(badging_id: @card_badging.id, user_id: @member.id)
  end

  test 'grants badge irrespective of quizzes in a pathway if `all_quizzes_answered` is set to FALSE' do
    prepare_pack_data

    @card_badging.update(all_quizzes_answered: false)

    attempt1 = create(:quiz_question_attempt, user: @member, quiz_question: @card1, selections: [@option1.id])

    UserBadgeService.new('Card', @cover, @member.id).assign_badge
    assert_equal 1, UserBadge.where(badging_id: @card_badging.id, user_id: @member.id).count
  end
end

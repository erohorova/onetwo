require 'test_helper'

class GroupLeaderboardQueryServiceTest < ActiveSupport::TestCase

  setup do
    @class = GroupLeaderboardQueryService
  end

  test 'MAX_LIMIT' do
    assert_equal 9000, @class::MAX_LIMIT
  end

  test 'DEFAULT_LIMIT' do
    assert_equal 500, @class::DEFAULT_LIMIT
  end

  test '.query with strategy' do
    opts = {foo: "bar"}
    cursor = "CURSOR"
    result = "RESULT"
    stub_strategy = Class.new(@class)
    stub_strategy.expects(:prepare_query).with(opts).returns cursor
    stub_strategy.expects(:evaluate_query).with(cursor).returns result
    assert_equal result, @class.query(stub_strategy, opts)
  end

  test '.query with strategy that fails to implement interface methods' do
    stub_strategy = Class.new(@class)
    error = assert_raises RuntimeError do
      @class.query(stub_strategy, {})
    end
    assert_equal "unimplemented", error.message
  end

end
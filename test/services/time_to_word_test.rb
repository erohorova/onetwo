require 'test_helper'

class TimeToWordTest < ActiveSupport::TestCase

  test "return sec in humanize format" do
    org = create(:organization)
    duration_format = org.get_settings_for('duration_format').present?
    assert_equal "less than a minute", TimeToWord.convert(time_in_sec: 36,duration_format: duration_format)
    assert_equal "6 Mins", TimeToWord.convert(time_in_sec: 360,duration_format: duration_format)
    assert_equal "1 Hour", TimeToWord.convert(time_in_sec: 3600,duration_format: duration_format)
    assert_equal "9 Hours 28 Mins", TimeToWord.convert(time_in_sec: 34128,duration_format: duration_format)
    assert_equal "3 Days 22 Hours", TimeToWord.convert(time_in_sec: 341280,duration_format: duration_format)
    assert_equal "11 Months", TimeToWord.convert(time_in_sec: 28512000,duration_format: duration_format)
    assert_equal "1 Year", TimeToWord.convert(time_in_sec: 31104000,duration_format: duration_format)
    assert_equal "1 Year 1 Month", TimeToWord.convert(time_in_sec: 34128000,duration_format: duration_format)
    assert_equal "5 Mins 41 Secs", TimeToWord.convert(time_in_sec: 341,duration_format: duration_format)
  end

  test "return sec in duration format for org" do
    org = create(:organization)
    create(:config, configable: org, name: 'duration_format', value: "hh:mm:ss")
    duration_format = org.get_settings_for('duration_format').present?
    assert_equal "6m", TimeToWord.convert(time_in_sec: 360,duration_format: duration_format)
    assert_equal "1h", TimeToWord.convert(time_in_sec: 3600,duration_format: duration_format)
    assert_equal "9h 28m", TimeToWord.convert(time_in_sec: 34128,duration_format: duration_format)
    assert_equal "94h 48m", TimeToWord.convert(time_in_sec: 341280,duration_format: duration_format)
    assert_equal "7920h", TimeToWord.convert(time_in_sec: 28512000,duration_format: duration_format)
    assert_equal "8640h", TimeToWord.convert(time_in_sec: 31104000,duration_format: duration_format)
    assert_equal "9480h", TimeToWord.convert(time_in_sec: 34128000,duration_format: duration_format)
    assert_equal "5m", TimeToWord.convert(time_in_sec: 341,duration_format: duration_format)
  end
end

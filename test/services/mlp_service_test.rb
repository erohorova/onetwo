require 'test_helper'

class MlpServiceTest < ActiveSupport::TestCase
  setup do
    @user = create(:user, organization: create(:organization))
    @service = MlpService.new(user_id: @user.id)
  end

  test 'get learning status' do
    course_card = create(:card, organization_id: @user.organization_id,
      author_id: nil, card_type: 'course', card_subtype: 'link')
    pathway_card = create(:card, organization_id: @user.organization_id,
      author_id: nil, card_type: 'pack', card_subtype: 'simple')

    course_assignment = create(:assignment, assignable: course_card, assignee: @user)
    pathway_assignment = create(:assignment, assignable: pathway_card, assignee: @user, state: 'completed', completed_at: 2.days.ago)

    assert_same_elements [course_assignment.id, pathway_assignment.id], @service.latest_assignments.pluck(:id)
  end

  test 'should not return dismissed assginment' do
    course_card = create(:card, organization_id: @user.organization_id,
      author_id: nil, card_type: 'course', card_subtype: 'link')
    pathway_card = create(:card, organization_id: @user.organization_id,
      author_id: nil, card_type: 'pack', card_subtype: 'simple')

    course_assignment = create(:assignment, assignable: course_card, assignee: @user)
    pathway_assignment = create(:assignment, assignable: pathway_card, assignee: @user, state: 'completed', completed_at: 2.days.ago)
    course_assignment.dismiss!
    assert_same_elements [pathway_assignment.id], @service.latest_assignments.pluck(:id)
  end
end

require 'test_helper'

class PushNotificationServiceTest < ActiveSupport::TestCase
  setup do
    @org  = create(:organization)
    @card = create(:card, organization: @org, author: nil)
  end

  test '#payload_has_required_keys?' do
    payload = { deep_link_type: 'course', deep_link_id: 23 }

    push_notification = PushNotificationService.new(@org, payload)
    refute push_notification.payload_has_required_keys?

    push_notification.payload[:content] = 'Test message'
    assert push_notification.payload_has_required_keys?
  end

  test '#record_exists?' do
    payload = {
      deep_link_type: 'card',
      deep_link_id: @card.id,
      content: 'Test message'
    }

    push_notification = PushNotificationService.new(@org, payload)
    assert push_notification.record_exists?

    push_notification.payload[:deep_link_id] = -1
    refute push_notification.record_exists?
  end

  test '#fetch_channel_name_for_payload' do
    user    = create(:user, organization: @org)
    channel = create(:channel, organization: @org)

    payload = {
      deep_link_type: 'card',
      deep_link_id: @card.id,
      content: 'Test message',
    }

    payload[:user_id] = user.id
    push_notification = PushNotificationService.new(@org, payload)
    assert_equal user.private_pubnub_channel_name, push_notification.fetch_channel_name_for_payload

    payload.delete(:user_id)
    payload[:channel_id] = channel.id
    push_notification = PushNotificationService.new(@org, payload)
    assert_equal channel.public_pubnub_channel_name, push_notification.fetch_channel_name_for_payload

    payload.delete(:channel_id)
    push_notification = PushNotificationService.new(@org, payload)
    assert_equal @org.public_pubnub_channel_name, push_notification.fetch_channel_name_for_payload
  end

  test '#payload_valid?' do
    payload = {
      deep_link_type: 'card',
      deep_link_id: @card.id,
      content: 'Test message'
    }

    push_notification = PushNotificationService.new(@org, payload)
    assert push_notification.payload_valid?
    assert_includes push_notification.payload[:channel_names], @org.public_pubnub_channel_name

    push_notification.payload[:deep_link_id] = -1
    refute push_notification.payload_valid?
  end

  test '#notify' do
    payload = {
      deep_link_type: 'card',
      deep_link_id: @card.id,
      content: 'Test message'
    }

    PubnubNotifier.expects(:send_payload).with(
      {
        pn_gcm: {
          data: {
            summary: 'Test message',
            deep_link_id: @card.id,
            deep_link_type: 'card',
            notification_type: 'AdminGenerated',
            host_name: @org.host
          }
        },
        pn_apns: {
          aps: {
            alert: 'Test message',
            sound: 'default',
            badge: 1
          },
          deep_link_id: @card.id,
          deep_link_type: 'card',
          notification_type: 'AdminGenerated',
          host_name: @org.host
        }
      },

      [@org.public_pubnub_channel_name],
      organization: @org
    ).once

    push_notification = PushNotificationService.new(@org, payload)
    push_notification.payload_valid?

    PushyNotifier.expects(:notify).with(payload).once
    push_notification.notify
  end

  test 'record_custom_notification_event' do
    # some stubs for dependable testing of the event
    actor = create(:user, organization: @org)
    metadata = { user_id: actor.id }
    RequestStore.store[:request_metadata] = metadata
    time = Time.now
    Time.stubs(:now).returns time
    Analytics::MetricsRecorderJob.unstub(:perform_later)
    EdcastActiveJob.unstub(:perform_later)


    # Build the notification object
    payload = {
      payload_key: 'payload_val',
      content:     'the content',
    }
    push_notification = PushNotificationService.new(@org, payload)
    
    # expect that this event was recorded
    expected_params = {
      recorder: "Analytics::OrgMetricsRecorder",
      event: "org_custom_notification_sent",
      org: Analytics::MetricsRecorder.org_attributes(@org),
      timestamp: time.to_i,
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      notification: {
        'json_payload' => payload.except(:content).to_json,
        'message' => payload[:content]
      },
      additional_data: metadata
    }
    Analytics::MetricsRecorderJob.expects(:perform_later).with(expected_params)

    # trigger the event recording
    push_notification.record_custom_notification_event
  end
end

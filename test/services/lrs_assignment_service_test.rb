require 'test_helper'

class LrsAssignmentServiceTest < ActiveSupport::TestCase
  setup do
    @org = create(:organization)
    @admin = create(:user, organization_role: 'admin', organization: @org)
    @user = create(:user, organization_id: @org.id)
    @card = create(:card, organization_id: @org.id, ecl_id: "test_ecl_id", author_id: nil)
    EclApi::CardConnector.any_instance.stubs(:card_from_ecl_id).returns(@card)
  end

  test "should create assignment and set status" do
    assert_difference ->{Assignment.count} do
      assignment = LrsAssignmentService.new(user_id: @user.id, ecl_id: "test_ecl_id", status: "started").run
      assert_equal "started", assignment.state
    end
  end

  test "should create user_content_completion and set status" do
    TestAfterCommit.enabled = true
    assert_difference ->{UserContentCompletion.count} do
      LrsAssignmentService.new(user_id: @user.id, ecl_id: "test_ecl_id", status: "started").run
    end

    assert_equal 'started', UserContentCompletion.last.state
    assert_not_nil UserContentCompletion.last.started_at
  end

  test "should not create duplicate assignment" do
    assignment = create(:assignment, assignable: @card, assignee: @user, due_at: Date.current + 10.days)

    assert_no_difference ->{Assignment.count} do
      assignment = LrsAssignmentService.new(user_id: @user.id, ecl_id: "test_ecl_id", status: "completed").run
      assert_equal "completed", assignment.state
    end
  end

  test 'should send skip_notification as true if status of the assignment to be created is either started/completed' do
    assignment = create(:assignment, assignable: @card, assignee: @user, state: 'completed')
    
    @card.expects(:create_assignment)
    .with(user: @user, assignor: @admin, opts: {skip_mail: true, completed_at: nil, started_at: nil, 
          skip_notifications: true})
    .returns(assignment)
    
    LrsAssignmentService.new(user_id: @user.id, ecl_id: "test_ecl_id", card: @card, status: "completed", assignor_id: @admin.id).run
  end

  test "should not create duplicate user_content_completion" do
    TestAfterCommit.enabled = true
    user_content_completion = create(:user_content_completion, completable: @card, user: @user, state: 'started', started_at: Time.now)

    assert_no_difference ->{UserContentCompletion.count} do
      assignment = LrsAssignmentService.new(user_id: @user.id, ecl_id: "test_ecl_id", status: "completed").run
    end

    assert_equal 'completed', user_content_completion.reload.state
    assert_not_nil user_content_completion.completed_at
  end

  test "shold not hit assignment create if assignment_id is present" do
    assignment = create(:assignment, assignable: @card, assignee: @user, due_at: Date.current + 10.days)
    @card.expects(:create_assignment).never

    assignment = LrsAssignmentService.new(user_id: @user.id, ecl_id: "test_ecl_id", status: "completed", assignment_id: assignment.id).run
    assert "completed", assignment.state
  end

  test "it should complete a completed assignment" do
    assignment = create(:assignment, assignable: @card, assignee: @user, due_at: Date.current + 10.days, state: "completed", completed_at: Time.now)
    assignment.expects(:complete).never

    LrsAssignmentService.new(user_id: @user.id, ecl_id: "test_ecl_id", status: "completed", assignment_id: assignment.id).run
  end

  test "it should track completed event" do
    create(:assignment, assignable: @card, assignee: @user, due_at: Date.current + 10.days)

    Tracking.expects(:track_act).with(has_entries(
          user_id: @user.id,
          object: "card",
          object_id: @card.id,
          action: 'mark_completed_assigned',
          organization_id: @org.id,
          platform: nil,
          device_id: nil)).once

    LrsAssignmentService.new(user_id: @user.id, ecl_id: "test_ecl_id", status: "completed").run
  end

  test "it should not hit ecl if card is passed" do
    EclApi::CardConnector.any_instance.stubs(:card_from_ecl_id).never
    assignment = LrsAssignmentService.new(user_id: @user.id, card: @card, status: "started").run
  end

  test 'should create content completion record' do
    create(:assignment, assignable: @card, assignee: @user)
    assert_difference -> {UserContentCompletion.count} do
      LrsAssignmentService.new(
        user_id: @user.id,
        ecl_id: "test_ecl_id",
        status: "completed"
      ).run
    end
  end
end

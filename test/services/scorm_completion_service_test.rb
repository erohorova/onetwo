require 'test_helper'

class ScromCompletionServiceTest < ActiveSupport::TestCase
  class Objectifyy #converts hash into an object
    def initialize(hash)
      hash.each do |k,v|
        self.instance_variable_set("@#{k}", v.is_a?(Hash) ? Hashit.new(v) : v)
        self.class.send(:define_method, k, proc{self.instance_variable_get("@#{k}")})
        self.class.send(:define_method, "#{k}=", proc{|v| self.instance_variable_set("@#{k}", v)})
      end
    end
  end
  setup do
    @org = create(:organization)
    @user = create(:user, organization: @org)
    stub_request(:get, "https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv").
      to_return(:status => 200, :body => "", :headers => {})
    @resource = create(:resource, url: "#{@org.home_page}/api/scorm/launch?course_id=set-fire-totherain-**100100")
    @scorm_card = create(:card, author: @user, organization: @org, resource_id: @resource.id,
                    filestack: [scorm_course: true,
                    url: 'https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv'])
    @service = mock()
    scorm_hash =  {
      app_id: "app_id",
      completed_date: "2017-10-11T13:03:42.000+0000",
      course_id: "#{@org.host_name}#{@scorm_card.id}",
      course_title: "Golf Explained - Simple Remediation",
      courseid: "#{@org.host_name}#{@scorm_card.id}",
      create_date: "2017-07-27T09:24:02.000+0000",
      email: nil,
      first_access_date: "2017-07-27T09:24:05.000+0000",
      id: "#{@user.id}-#{@scorm_card.id}",
      instances: nil,
      last_access_date: "2017-08-11T13:03:42.000+0000",
      last_course_version_launched: "0",
      learner_first_name: "shemal",
      learner_id: "#{@user.id}",
      learner_last_name: "joshi",
      registration_id: "#{@user.id}-#{@org.host_name}#{@scorm_card.id}"
     }
     @all_registrations = []
     @all_registrations << Objectifyy.new(scorm_hash)
    @result = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n<rsp stat=\"ok\">
    <registrationreport format=\"course\" regid=\"#{@all_registrations.first.registration_id}\" instanceid=\"0\"><complete>complete</complete><success>unknown</success>
    <totaltime>6</totaltime><score>unknown</score></registrationreport></rsp>"
  end

  # we split user and course_id in the returning reg_id by '-'

  test '#extract_user_id should return user_id' do
    ScormCloud::ScormCloud.any_instance.stubs(:initiate_scorm).returns(@service)
    ScormCompletionService.any_instance.stubs(:get_registrations).returns(@all_registrations)

    service = ScormCompletionService.new
    reg_id = "#{@user.id}-#{@org.host_name}#{@scorm_card.id}"
    index_of_split = reg_id.index('-')
    assert_equal service.extract_user_id(reg_id, index_of_split), @user.id.to_s
  end

  test '#extract_course_id should return course_id' do
    ScormCloud::ScormCloud.any_instance.stubs(:initiate_scorm).returns(@service)
    ScormCompletionService.any_instance.stubs(:get_registrations).returns(@all_registrations)

    service = ScormCompletionService.new
    reg_id = "#{@user.id}-#{@org.host_name}#{@scorm_card.id}"
    index_of_split = reg_id.index('-')
    assert_equal service.extract_course_id(reg_id, index_of_split), "#{@org.host_name}#{@scorm_card.id}"
  end

  test '#hasify_xml should return hash' do
    ScormCloud::ScormCloud.any_instance.stubs(:initiate_scorm).returns(@service)
    ScormCompletionService.any_instance.stubs(:get_registrations).returns(@all_registrations)

    service = ScormCompletionService.new
    hashed_result = service.hasify_xml(@result)
    assert_equal 'Hash', hashed_result.class.to_s
  end

  test '#get_response_status should return response status' do
    ScormCloud::ScormCloud.any_instance.stubs(:initiate_scorm).returns(@service)
    ScormCompletionService.any_instance.stubs(:get_registrations).returns(@all_registrations)

    service = ScormCompletionService.new
    hashed_result = service.hasify_xml(@result)
    assert_equal "ok", service.get_response_status(hashed_result)
  end

  test '#course_completion_status should return course completion status' do
    ScormCloud::ScormCloud.any_instance.stubs(:initiate_scorm).returns(@service)
    ScormCompletionService.any_instance.stubs(:get_registrations).returns(@all_registrations)

    service = ScormCompletionService.new
    hashed_result = service.hasify_xml(@result)
    assert_equal "complete", service.course_completion_status(hashed_result)
  end

  test 'should find card by course_id in resource' do
    ScormCloud::ScormCloud.any_instance.stubs(:initiate_scorm).returns(@service)
    ScormCompletionService.any_instance.stubs(:get_registrations).returns(@all_registrations)

    service = ScormCompletionService.new
    course_id = 'set-fire-totherain-**100100'
    resource = service.get_resource(@org, course_id)
    assert_equal resource.id, @resource.id
    assert_equal resource.url, @resource.url
    assert_equal resource.cards.ids, [@scorm_card.id]
  end

  test 'should complete card if scorm course completed' do
    create(:config, name: 'auto_mark_as_complete_scorm_cards', value: true, configable: @org)
    ScormCloud::ScormCloud.any_instance.stubs(:initiate_scorm).returns(@service)
    ScormCompletionService.any_instance.stubs(:get_registrations).returns(@all_registrations)
    assert_difference -> {UserContentCompletion.count}, 1 do
      RecurringJobs::PullScormResultsJob.perform_now
      assert_equal 1, UserContentCompletion.count
      ucc = UserContentCompletion.last
      assert_equal ucc.completable_id, @scorm_card.id
      assert_equal ucc.user_id, @user.id
    end
  end

  test 'should not complete card if scorm course is not completed' do
    org = create(:organization)
    user = create(:user, organization: org)

    stub_request(:get, "https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv").
      to_return(:status => 200, :body => "", :headers => {})
    scorm_card_one = create(:card, author: user, organization: org,
                    filestack: [scorm_course: true,
                    url: 'https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv'])
    scorm_hash =  {
      app_id: "app_id",
      completed_date: nil,
      course_id: "#{org.host_name}#{scorm_card_one.id}",
      course_title: "Golf Explained - Simple Remediation",
      courseid: "#{org.host_name}#{scorm_card_one.id}",
      create_date: "2017-07-27T09:24:02.000+0000",
      email: nil,
      first_access_date: "2017-07-27T09:24:05.000+0000",
      id: "#{user.id}-#{scorm_card_one.id}",
      instances: nil,
      last_access_date: "2017-08-11T13:03:42.000+0000",
      last_course_version_launched: "0",
      learner_first_name: "shemal",
      learner_id: "#{user.id}",
      learner_last_name: "joshi",
      registration_id: "#{user.id}-#{org.host_name}#{scorm_card_one.id}"
     }

    all_registrations = []
    all_registrations << Objectifyy.new(scorm_hash)
    create(:config, name: 'auto_mark_as_complete_scorm_cards', value: true, configable: @org)

    ScormCloud::ScormCloud.any_instance.stubs(:initiate_scorm).returns(@service)
    ScormCompletionService.any_instance.stubs(:get_registrations).returns(all_registrations)
    assert_no_difference -> {UserContentCompletion.count} do
      RecurringJobs::PullScormResultsJob.perform_now
    end
  end

  test 'should do nothing if org config not set for auto_mark_as_complete' do
    ScormCloud::ScormCloud.any_instance.stubs(:initiate_scorm).returns(@service)
    ScormCompletionService.any_instance.stubs(:get_registrations).returns(@all_registrations)
    assert_no_difference -> {UserContentCompletion.count} do
      RecurringJobs::PullScormResultsJob.perform_now
    end
  end

  test 'should do nothing if user and card have different orgs' do
    org = create(:organization)
    org2 = create(:organization)
    user = create(:user, organization: org)
    user2 = create(:user, organization: org2)

    stub_request(:get, "https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv").
      to_return(:status => 200, :body => "", :headers => {})
    scorm_card_one = create(:card, author: user2, organization: org2,
                    filestack: [scorm_course: true,
                    url: 'https://cdn.filestackcontent.com/rAsQFWotRbeyofqTMYNv'])
    scorm_hash =  {
      app_id: "app_id",
      completed_date: nil,
      course_id: "#{org2.host_name}#{scorm_card_one.id}",
      course_title: "Golf Explained - Simple Remediation",
      courseid: "#{org2.host_name}#{scorm_card_one.id}",
      create_date: "2017-07-27T09:24:02.000+0000",
      email: nil,
      first_access_date: "2017-07-27T09:24:05.000+0000",
      id: "#{user.id}-#{scorm_card_one.id}",
      instances: nil,
      last_access_date: "2017-08-11T13:03:42.000+0000",
      last_course_version_launched: "0",
      learner_first_name: "shemal",
      learner_id: "#{user.id}",
      learner_last_name: "joshi",
      registration_id: "#{user.id}-#{org2.host_name}#{scorm_card_one.id}"
     }

    all_registrations = []
    all_registrations << Objectifyy.new(scorm_hash)
    result = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n<rsp stat=\"ok\">
    <registrationreport format=\"course\" regid=\"#{all_registrations.first.registration_id}\" instanceid=\"0\"><complete>complete</complete><success>unknown</success>
    <totaltime>6</totaltime><score>unknown</score></registrationreport></rsp>"

    ScormCloud::ScormCloud.any_instance.stubs(:initiate_scorm).returns(@service)
    ScormCompletionService.any_instance.stubs(:get_registrations).returns(all_registrations)
    assert_no_difference -> {UserContentCompletion.count} do
      RecurringJobs::PullScormResultsJob.perform_now
    end
  end
end
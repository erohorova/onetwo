require 'test_helper'

class NiitServiceTest < ActiveSupport::TestCase
  setup do
    organization = create(:organization)
    @user = create(:user, organization: organization)
    @service = NiitService.new(
      user: @user,
      course_id: 'course1234',
      course_type: '3',
      domain: 'xyz'
    )
  end

  test 'check headers' do
    assert_equal(
      {
        "accept"=>"application/json",
        "authorization-domain" => "xyz",
        "authorization-username"=>"pmi_sso",
        "authorization-password"=>"bmlpdF9wbSFfNTUw",
        "Content-Type"=>"application/json"
      },
      @service.headers
    )
  end

  test 'register user' do
    register_user_stub_success
    @service.register_user
  end

  test 'subscribe user' do
    subscribe_user_stub_success
    @service.subscribe_user
  end

  test 'subscribe user fail should log error' do
    subscribe_user_stub_fail
    response_json = {
      "soapenv:Fault" => {
        "faultstring" => "Invalid subscription pool",
        }
    }.to_json

    log.expects(:error).with("Niit course subscription failed for course_id: course1234, course_type: 3, remoteUserIdentifier: #{@user.id}, response: #{response_json}")
    @service.subscribe_user
  end

  test 'fetch deeplink url for user registration and subscrption success' do
    register_user_stub_success
    subscribe_user_stub_success
    assert_not_nil @service.fetch_deeplink_url
  end

  test 'fetch deeplink url for already registered user' do
    register_user_stub_fail
    subscribe_user_stub_success
    assert_not_nil @service.fetch_deeplink_url
  end

  test 'should rescue exception for fetch deeplink url' do
    log.expects(:error).once
    assert_nil @service.fetch_deeplink_url
  end

  test 'get deeplink url for classroom course' do
    register_user_stub_success
    subscribe_user_stub_success
    assert_includes @service.fetch_deeplink_url, CGI.escape(NiitService::CLASSROOM_REDIRECT_URL)
  end

  test 'get deeplink url for elearning course' do
    @service = NiitService.new(
      user: @user,
      course_id: 'course1234',
      course_type: '14',
      domain: 'xyz'
    )

    register_user_stub_success
    subscribe_user_stub_success

    deeplink_url = @service.fetch_deeplink_url
    assert_includes deeplink_url, CGI.escape(NiitService::ELEARNING_REDIRECT_URL)
  end

  test 'deeplink url should contain course language if provided' do
    register_user_stub_success
    subscribe_user_stub_success
    assert_not_includes @service.fetch_deeplink_url, CGI.escape('language=')

     @service = NiitService.new(
      user: @user,
      course_id: 'course1234',
      course_type: '14',
      domain: 'xyz',
      language: 'es'
    )

    register_user_stub_success
    subscribe_user_stub_success
    assert_includes @service.fetch_deeplink_url, CGI.escape('language=es')
  end
end

private

def register_user_stub_success
  stub_request(:post, NiitService::BASE_URL + NiitService::CREATE_STUDENT_URL).
    with("body" => {
      "identification" => {
        "remoteUserIdentifier"=> @user.id,
        "username"=> @user.email
      },

      "studentProfile" => {
        "emailAddress" => @user.email,
        "name" => {
          "firstName" => @user.first_name,
          "lastName" => @user.last_name
        },
        "organization" => 'xyz',
        "studentSettings" => []
      }
    },
    "headers" => @service.headers.merge(
      'Accept'=> 'application/json',
      'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
      'User-Agent' => 'Ruby')
  ).to_return(
    status: 200,
    body: {
      "ns1:assignSubscriptionResponse" => {
        "return" => {
          "subscriptionPoolIdentifier" => 1234,
          "remoteUserIdentifier" => @user.id
        }
      }
    }.to_json,
    headers: {}
  )
end

def register_user_stub_fail
  stub_request(:post, NiitService::BASE_URL + NiitService::CREATE_STUDENT_URL).
    with("body" => {
      "identification" => {
        "remoteUserIdentifier"=> @user.id,
        "username"=> @user.email
      },

      "studentProfile" => {
        "emailAddress" => @user.email,
        "name" => {
          "firstName" => @user.first_name,
          "lastName" => @user.last_name
        },
        "organization" => 'xyz',
        "studentSettings" => []
      }
    },
    "headers" => @service.headers.merge(
      'Accept'=> 'application/json',
      'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
      'User-Agent' => 'Ruby')
  ).to_return(
    status: 500,
    body: {
      "soapenv:Fault" => {
        "faultstring" => "Remote user identifier is already in use in the domain",
        }
      }.to_json,
    headers: {}
  )
end

def subscribe_user_stub_success
  stub_request(:post, NiitService::BASE_URL + NiitService::SUBSCRIPTION_URL).
    with("body" => {
      "remoteUserIdentifier" => @user.id,
      "subscriptionPoolIdentifier" => Settings.niit.subscription_pool_identifier
    },
    "headers" => @service.headers.merge(
      'Accept'=> 'application/json',
      'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
      'User-Agent' => 'Ruby')
  ).to_return(
    status: 200,
    body: {
      "ns1:assignSubscriptionResponse": {
        "return": {
          "remoteUserIdentifier": @user.id,
          "subscriptionIdentifier": 909090
        }
      }
    }.to_json,
    headers: {}
  )
end

def subscribe_user_stub_fail
  stub_request(:post, NiitService::BASE_URL + NiitService::SUBSCRIPTION_URL).
    with("body" => {
      "remoteUserIdentifier" => @user.id,
      "subscriptionPoolIdentifier" => Settings.niit.subscription_pool_identifier
    },
    "headers" => @service.headers.merge(
      'Accept'=> 'application/json',
      'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
      'User-Agent' => 'Ruby')
  ).to_return(
    status: 500,
    body:  {
      "soapenv:Fault" => {
        "faultstring" => "Invalid subscription pool",
        }
      }.to_json,
    headers: {}
  )
end

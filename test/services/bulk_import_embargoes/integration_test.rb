require 'test_helper'

class BulkImportEmbargo::IntegrationTest < ActiveSupport::TestCase

  def setup_csv_for_creation
    stub_request(:post, 'https://api.branch.io/v1/url').to_return(status: 200, body: '', headers: {})
    stub_request(:post, 'http://localhost:9200/_bulk')

    @klass = BulkImportEmbargo.new(
      file_id: @embargo_file.id,
    )
    @klass.call
  end

  setup do
    Config.any_instance.stubs :create_okta_group_config

    User.any_instance.stubs(:update_details_to_okta).returns true
    stub_request(:get, 'http://breelio.com/wp-content/uploads/2014/06/sample1.jpg').
        with(headers: { 'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Ruby' }).
        to_return(status: 200, body: '', headers: {})

    @organization  = create :organization
    @admin_user    = create :user, organization: @organization, organization_role: 'admin'

    csv_rows = File.open("#{Rails.root}/test/fixtures/files/embargo.csv").read

    @embargo_file = create(:embargo_file,
                           user_id: @admin_user.id,
                           organization_id: @organization.id,
                           data: csv_rows)

    stub_request(:post, "https://api.branch.io/v1/url").to_return(status: 200, body: "{\"url\":\"http://bitly.co}", headers: {})
  end

  test 'rejects invalid urls' do
    setup_csv_for_creation
    assert_equal CSV.parse(@embargo_file.data, headers: true,
                           header_converters: lambda{ |h| h.underscorize }).to_a.last[0], 'ggg+hhh'
    assert_nil @organization.embargos.find_by(value: 'ggg+hhh')
  end

  test 'sends import to admin' do
    setup_csv_for_creation

    mailer_mock = mock()
    mailer_mock.expects(:deliver_now).returns(nil).once
    BulkImportEmbargoDetailsMailer.expects(:notify_admin).returns(mailer_mock).once

    BulkImportEmbargo.new(file_id: @embargo_file.id).call
  end

end

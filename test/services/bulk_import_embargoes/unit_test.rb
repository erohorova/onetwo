require 'test_helper'

class BulkImportEmbargo::UnitTest < ActiveSupport::TestCase
  setup do
    csv_rows = File.read("#{Rails.root}/test/fixtures/files/embargo.csv")
    @organization = create(:organization)
    @admin_user = create(
      :user, organization: @organization, organization_role: 'admin'
    )
    @embargo_file = create(
      :embargo_file, user_id: @admin_user.id,
      organization_id: @organization.id,
      data: csv_rows
    )
  end

  test '#preview_csv resolves fields and values for quick preview' do
    data, total = BulkImportEmbargo.new(file_id: @embargo_file.id).preview_csv(7)
    assert_equal 7, data.length
    assert_equal 8, total
    assert_equal %w[value category], data.first.keys
  end

  test '.csv_headers_validation validates mandatory headers in the CSV' do
    # empty csv
    csv_data_1 = "xyz,pqr,email,group\n"
    expected_msg_1 = 'Invalid or empty csv'
    assert_equal expected_msg_1, BulkImportEmbargo.csv_headers_validation(csv_data_1)

    # invalid headers
    csv_data_2 = "xyz,pqr\naaa,iii"
    expect_msg_2 = 'Invalid CSV headers. Use format value, category'
    assert_equal expect_msg_2, BulkImportEmbargo.csv_headers_validation(csv_data_2)

    # valid headers
    csv_data_3 = "value,category\nword,unapproved_words"
    assert_nil BulkImportEmbargo.csv_headers_validation(csv_data_3)
  end
end

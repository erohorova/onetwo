require 'test_helper'

class Notify::EngineTest < ActiveSupport::TestCase
  test '#trigger_event' do
    user = create :user, first_name: 'First', last_name: 'Last', email: 'first@last.com'
    engine = Notify::Engine.new
    notifier = Notify::NewSmartbiteCommentNotifier.new

    engine.register_notifier(notifier, [Notify::EVENT_NEW_COMMENT])

    notifier.expects(:users_to_notify).yields([user.id])
    # engine.expects(:send_single_notification_async)
    engine.expects(:set_web_notification_content).never

    engine.trigger_event(Notify::EVENT_NEW_COMMENT, user)
  end

  test '#set_web_notification_content even if org notification enabled config is off' do
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(false)
    org = create(:organization)
    user = create :user, organization: org
    channel = create :channel, user: user, organization: org, curate_only: true
    curator = create :user, organization: org
    channel.curators << curator
    channel.followers << user
    card = create(:card, author: user)
    channels_card = create(:channels_card, channel_id: channel.id, card_id: card.id, state: ChannelsCard::STATE_NEW)
    channels_card.skip

    notification_entry = NotificationEntry.where(event_name: 'skip_card')
    notification = Notification.where(notification_type: 'skipped_card')
    assert_equal 0, notification_entry.count
    assert_equal 1, notification.count
    assert_equal 'skipped_card', notification.first.notification_type
    assert_equal channels_card.card.author_id, notification.first.user_id
  end

  test '#send_single_notification' do
    org = create :organization, name: 'Org'
    user = create :user, first_name: 'First', last_name: 'Last', email: 'first@last.com', organization: org
    author = create :user, organization: org

    card = create :card, organization: org, author: user
    comment = create :comment, user: author, commentable: card
    note_entry = create :notification_entry, user: user, sourceable: comment,
                        date_in_timezone: Time.zone.today, sending_time_in_timezone: Time.zone.now
    note_entry.set_enabled(:email, :single)
    note_entry.set_enabled(:push, :single)
    note_entry.save

    engine = Notify::Engine.new
    notifier = Notify::NewSmartbiteCommentNotifier.new

    engine.register_notifier(notifier, [Notify::EVENT_NEW_COMMENT])

    # Set expectations
    expected_params = {
      "user"=> {
        "first_name" => "First",
        "last_name" => "Last",
        "email" => "first@last.com",
        "org_name" => "Org"
      },
      "org" => {"logo"=>nil}
    }

    engine.stubs(:encapsulated_params_for_notification).returns(expected_params)

    engine.expects(:send_email).with(encapsulated_params:expected_params,
                                     template_name: Notify::NOTE_NEW_SMARTBITE_COMMENT, layout_name: 'default_layout')

    engine.expects(:send_push).with(encapsulated_params:expected_params, template_name: Notify::NOTE_NEW_SMARTBITE_COMMENT)

    engine.send_single_notification(note_entry)

    note_entry.reload
    assert_equal Notify::STATUS_SENT, note_entry.get_status(:email, :single)
    assert_equal Notify::STATUS_SENT, note_entry.get_status(:push, :single)
  end

  test 'notification params should return correct response' do
    org = create :organization
    user = create :user, first_name: 'First', last_name: 'Last', email: 'first@last.com', organization: org
    assignment = create :assignment, assignee: user
    create :team_assignment, assignment_id: assignment.id, assignor: user
    note_entry = create :notification_entry, user: user, sourceable: assignment
    notifier = Notify::AssignedContentNotifier.new
    assert notifier.notification_params(note_entry)
    assert_equal 'First Last', notifier.notification_params(note_entry)[:user_name]
  end

  test 'notification params should return false if notificaion_entry is nil' do
    notifier = Notify::AssignedContentNotifier.new
    refute notifier.notification_params(nil)
  end

  test 'notification params should return truncated title from 100 symbols' do
    org = create :organization
    user = create :user, organization: org
    card = create :card, title: 'a' * 200, author: user
    assignment = create :assignment, assignee: user, assignable: card
    create :team_assignment, assignment_id: assignment.id, assignor: user
    note_entry = create :notification_entry, user: user, sourceable: assignment
    notifier = Notify::AssignedContentNotifier.new
    assert notifier.notification_params(note_entry)
    assert_equal 100, notifier.notification_params(note_entry)[:assignment][:title].length
  end

  test '#send_digests' do
    # Initialize stuff
    user = create :user
    engine = Notify::Engine.new

    today = Time.zone.now
    out_of_range = today - Notify::LOOKBACK_DURATIONS[:digest_daily]
    tomorrow = today + 1.day

    # Create notification entries
    user1 = create :user
    note1 = create :notification_entry, user: user1, date_in_timezone: today,
                                        sending_time_in_timezone: today, sourceable: user
    note1.set_enabled(:email, :digest_daily)
    note1.set_enabled(:push, :digest_daily)
    note1.save

    user2 = create :user
    note2 = create :notification_entry, user: user2, date_in_timezone: today,
                                        sending_time_in_timezone: today, sourceable: user
    note2.set_enabled(:email, :digest_daily)
    note2.save

    user3 = create :user
    note3 = create :notification_entry, user: user3, date_in_timezone: today,
                                        sending_time_in_timezone: today, sourceable: user
    note3.set_enabled(:push, :digest_daily)
    note3.save

    user4 = create :user
    note4 = create :notification_entry, user: user4, date_in_timezone: out_of_range,
                                        sending_time_in_timezone: out_of_range, sourceable: user
    note4.set_enabled(:email, :digest_daily)
    note4.set_enabled(:push, :digest_daily)
    note4.save

    user5 = create :user
    note5 = create :notification_entry, user: user5, date_in_timezone: tomorrow,
                                        sending_time_in_timezone: tomorrow, sourceable: user
    note5.set_enabled(:email, :digest_daily)
    note5.set_enabled(:push, :digest_daily)
    note5.save

    user6 = create :user
    note6 = create :notification_entry, user: user6, date_in_timezone: today,
                                        sending_time_in_timezone: today, sourceable: user
    note6.set_enabled(:push, :digest_daily)
    note6.set_status(:push, :digest_daily, Notify::STATUS_SENT)
    note6.save

    user7 = create :user
    note7 = create :notification_entry, user: user7, date_in_timezone: today,
                                        sending_time_in_timezone: today, sourceable: user
    note7.set_enabled(:email, :digest_daily)
    note7.set_status(:email, :digest_daily, Notify::STATUS_SENT)
    note7.save

    # Set expectations
    engine.expects(:send_user_digest_async).with(today, user1.id, :digest_daily)
    engine.expects(:send_user_digest_async).with(today, user2.id, :digest_daily)
    engine.expects(:send_user_digest_async).with(today, user3.id, :digest_daily)

    engine.expects(:send_user_digest_async).with(today, user4.id, :digest_daily).never
    engine.expects(:send_user_digest_async).with(today, user5.id, :digest_daily).never
    engine.expects(:send_user_digest_async).with(today, user6.id, :digest_daily).never
    engine.expects(:send_user_digest_async).with(today, user7.id, :digest_daily).never

    # Run engine
    engine.send_digests(today, :digest_daily)
  end

  test '#send_user_digest' do
    # Initialize stuff
    org = create :organization, name: 'Org'
    user = create :user
    user1 = create :user, first_name: 'First', last_name: 'Last', email: 'first@last.com', organization: org
    user2 = create :user
    engine = Notify::Engine.new
    notifier = Notify::NewSmartbiteCommentNotifier.new
    engine.register_notifier(notifier, [Notify::EVENT_NEW_COMMENT])

    today = Time.zone.now
    out_of_range = today - Notify::LOOKBACK_DURATIONS[:digest_daily]
    tomorrow = today + 1.day

    # Create notification entries
    note11 = create :notification_entry, user: user1, date_in_timezone: today,
                                         sending_time_in_timezone: today, sourceable: user
    note11.set_enabled(:email, :digest_daily)
    note11.set_enabled(:push, :digest_daily)
    note11.save

    note12 = create :notification_entry, user: user1, date_in_timezone: today,
                                         sending_time_in_timezone: today, sourceable: user

    note13 = create :notification_entry, user: user1, date_in_timezone: out_of_range,
                                         sending_time_in_timezone: out_of_range, sourceable: user
    note13.set_enabled(:email, :digest_daily)
    note13.set_enabled(:push, :digest_daily)
    note13.save

    note14 = create :notification_entry, user: user1, date_in_timezone: tomorrow,
                                         sending_time_in_timezone: tomorrow, sourceable: user
    note14.set_enabled(:email, :digest_daily)
    note14.set_enabled(:push, :digest_daily)
    note14.save

    note15 = create :notification_entry, user: user1, date_in_timezone: today,
                                         sending_time_in_timezone: today, sourceable: user
    note15.set_enabled(:email, :digest_daily)
    note15.set_status(:email, :digest_daily, Notify::STATUS_SENT)
    note15.save

    note16 = create :notification_entry, user: user1, date_in_timezone: today,
                                         sending_time_in_timezone: today, sourceable: user
    note16.set_enabled(:push, :digest_daily)
    note16.set_status(:push, :digest_daily, Notify::STATUS_SENT)
    note16.save

    note17 = create :notification_entry, user: user1, date_in_timezone: today,
                                         sending_time_in_timezone: today, sourceable: user
    note17.set_enabled(:email, :digest_daily)
    note17.save

    note21 = create :notification_entry, user: user2, date_in_timezone: today,
                                         sending_time_in_timezone: today, sourceable: user
    note21.set_enabled(:email, :digest_daily)
    note21.set_enabled(:push, :digest_daily)
    note21.save

    note22 = create :notification_entry, user: user2, date_in_timezone: today,
                                         sending_time_in_timezone: today, sourceable: user
    note22.set_enabled(:push, :digest_daily)
    note22.save

    note23 = create :notification_entry, user: user2, date_in_timezone: out_of_range,
                                         sending_time_in_timezone: out_of_range, sourceable: user
    note23.set_enabled(:email, :digest_daily)
    note23.set_enabled(:push, :digest_daily)
    note23.save

    note24 = create :notification_entry, user: user2, date_in_timezone: tomorrow,
                                         sending_time_in_timezone: tomorrow, sourceable: user
    note24.set_enabled(:email, :digest_daily)
    note24.set_enabled(:push, :digest_daily)
    note24.save

    # Set expectations
    notifier.define_singleton_method(:notification_params) { |note|
      {note_id: note.id}
    }

    expected_params = {
      user: {
        first_name: 'First',
        last_name: 'Last',
        email: 'first@last.com',
        org_name: 'Org'
      }
    }

    expected_email_params = {}.merge(expected_params)
    expected_email_params[Notify::NOTE_NEW_SMARTBITE_COMMENT] = [
      {note_id: note11.id},
      {note_id: note17.id}
    ]

    expected_push_params = {}.merge(expected_params)
    expected_push_params[Notify::NOTE_NEW_SMARTBITE_COMMENT] = [
      {note_id: note11.id}
    ]
    engine.stubs(:global_params).returns(expected_params)

    engine.expects(:send_email).with(encapsulated_params:expected_email_params, template_name: :digest_daily, layout_name: 'default_layout')

    # engine.expects(:send_push).with(encapsulated_params:expected_push_params, template_name: :digest_daily)

    # Execute sending
    engine.send_user_digest(today, user1.id, :digest_daily)

    # Check status
    note11.reload
    assert_equal Notify::STATUS_SENT, note11.get_status(:email, :digest_daily)
    # assert_equal Notify::STATUS_SENT, note11.get_status(:push, :digest_daily)

    note12.reload
    assert_equal Notify::STATUS_UNSENT, note12.get_status(:email, :digest_daily)
    # assert_equal Notify::STATUS_UNSENT, note12.get_status(:push, :digest_daily)

    note13.reload
    assert_equal Notify::STATUS_UNSENT, note13.get_status(:email, :digest_daily)
    # assert_equal Notify::STATUS_UNSENT, note13.get_status(:push, :digest_daily)

    note14.reload
    assert_equal Notify::STATUS_UNSENT, note14.get_status(:email, :digest_daily)
    # assert_equal Notify::STATUS_UNSENT, note14.get_status(:push, :digest_daily)

    note15.reload
    assert_equal Notify::STATUS_SENT, note15.get_status(:email, :digest_daily)
    # assert_equal Notify::STATUS_UNSENT, note15.get_status(:push, :digest_daily)

    note16.reload
    assert_equal Notify::STATUS_UNSENT, note16.get_status(:email, :digest_daily)
    # assert_equal Notify::STATUS_SENT, note16.get_status(:push, :digest_daily)

    note17.reload
    assert_equal Notify::STATUS_SENT, note17.get_status(:email, :digest_daily)
    # assert_equal Notify::STATUS_UNSENT, note17.get_status(:push, :digest_daily)
  end

  test '#send_user_digest should not include private content if not accessible' do
    org = create :organization, name: 'Org'
    engine = Notify::Engine.new
    notifier = Notify::NewContentByFollowedUserNotifier.new
    engine.register_notifier(notifier, [Notify::EVENT_NEW_CARD])

    author = create :user, organization: org
    user1 = create(:user, first_name: "First", last_name: "Last", email: "first@last.com", organization: org)
    user2 = create(:user, organization: org)
    private_channel = create :channel, organization: org, is_private: true
    create(:follow, followable: private_channel, user_id: user1.id)

    today = Time.zone.now

    card = create :card, author_id: author.id

    note1 = create :notification_entry, user: user1, date_in_timezone: today, sending_time_in_timezone: today,
      sourceable: card, event_name: Notify::EVENT_NEW_CARD, notification_name: Notify::NOTE_NEW_CONTENT_BY_FOLLOWED_USER
    note1.set_enabled(:email, :digest_daily)
    note1.save

    note2 = create :notification_entry, user: user2, date_in_timezone: today, sending_time_in_timezone: today,
      sourceable: card, event_name: Notify::EVENT_NEW_CARD, notification_name: Notify::NOTE_NEW_CONTENT_BY_FOLLOWED_USER
    note2.set_enabled(:email, :digest_daily)
    note2.save

    create(:channels_card, card_id: card.id, channel_id: private_channel.id)

    engine.send_user_digest(today, user1.id, :digest_daily)

    note1.reload
    assert_equal Notify::STATUS_SENT, note1.get_status(:email, :digest_daily)

    note2.reload
    assert_not_equal Notify::STATUS_SENT, note2.get_status(:email, :digest_daily)
  end

  test '#send_email' do

  end

  test '#send_push' do

  end

  class NotifyTemplateRenderTest < ActiveSupport::TestCase
    setup do
      @org = create :organization
      @engine = Notify::Engine.new
      @test_params = { org: { instance: @org }}
    end

    test 'return default template if org specific template not present' do
      @engine.expects(:render_template_to_string).with(template_name: 'email_subject/test_subject', vars: @test_params).once
      @engine.expects(:render_template_to_string).with(template_name: 'email_body/test_body', vars: @test_params,
        layout_name: 'default_layout').once

      @engine.get_org_specific_template(encapsulated_params: @test_params, template_name: 'test', layout_name: 'default_layout')
    end

    test 'return org specific template if present' do
      File.stubs(:exists?).returns(true)
      org_folder = "#{@org.host_name.downcase}_#{@org.id}"
      @engine.expects(:render_template_to_string).with(template_name: "#{org_folder}/email_subject/test_subject", vars: @test_params).once
      @engine.expects(:render_template_to_string).with(template_name: "#{org_folder}/email_body/test_body", vars: @test_params,
        layout_name: "#{org_folder}_default_layout").once

      @engine.get_org_specific_template(encapsulated_params: @test_params, template_name: 'test', layout_name: 'default_layout')
      File.unstub(:exists?)
    end
  end

  test '#render_template_to_string' do
    engine = Notify::Engine.new

    output = engine.render_template_to_string(template_name:'test', vars:{name: 'world'}, layout_name: 'test_layout')

    assert_equal '!!Hello world.!!', output
  end

  test '#user_params' do
    engine = Notify::Engine.new
    org = create :organization, name: 'Org'
    user = create :user, first_name: 'First', last_name: 'Last', email: 'first@last.com', organization: org

    result = engine.user_params(user)
    assert_equal 'First', result[:first_name]
    assert_equal 'Last', result[:last_name]
    assert_equal 'first@last.com', result[:email]
    assert_equal 'Org', result[:org_name]
  end

  test 'return defaulte org params' do
    engine = Notify::Engine.new
    org = create :organization, name: 'Org'

    result = engine.org_params(org)
    assert_equal 'EdCast', result[:name]
    assert_equal Settings.ios_app_location, result[:appstore_link]
    assert_equal Settings.android_app_location, result[:playstore_link]
    assert_equal Settings.support_email, result[:support_email]
  end

  test 'return custom org params' do
    engine = Notify::Engine.new
    org = create :organization, name: 'CustomOrg'
    config_hash = {
        "appStore" => 'customappstoreapplink',
        "playStore" => 'customplaystoreapplink',
        "supportEmail" => 'support@custom.com',
        "promotions" => true
    }
    config = create :config, name: 'footer_options', configable_id: org.id, configable_type: 'Organization',
        value: config_hash.to_json, data_type: 'json'

    result = engine.org_params(org)
    assert_equal 'CustomOrg', result[:name]
    assert_equal 'customappstoreapplink', result[:appstore_link]
    assert_equal 'customplaystoreapplink', result[:playstore_link]
    assert_equal 'support@custom.com', result[:support_email]
    assert_equal true, result[:promotions]
  end

  test 'return defaulte css params' do
    engine = Notify::Engine.new
    org = create :organization, name: 'Org'

    result = engine.params_custom_css(org)
    assert_equal "##{Settings.mail_css.header_background_color}", result[:header_background_color]
    assert_equal "##{Settings.mail_css.text_color}", result[:text_color]
    assert_equal "##{Settings.mail_css.highlight_text_color}", result[:highlight_text_color]
    assert_equal "##{Settings.mail_css.section_text_color}", result[:section_text_color]
    assert_equal "##{Settings.mail_css.card_text_color}", result[:card_text_color]
    assert_equal "##{Settings.mail_css.button_background_color}", result[:button_background_color]
  end

  test 'return defaulte css params if custom css enabled off' do
    engine = Notify::Engine.new
    org = create :organization, name: 'CustomOrg'
    config_hash = { "enableCustomCss" => false, "headerBackgroundColor" => '#e3452d',
        "textColor" => '#000000', "buttonBackgroundColor" => '#45e324' }

    config = create :config, name: 'mail_custom_css', configable_id: org.id, configable_type: 'Organization',
        value: config_hash.to_json, data_type: 'json'

    result = engine.params_custom_css(org)
    assert_equal "##{Settings.mail_css.header_background_color}", result[:header_background_color]
    assert_equal "##{Settings.mail_css.text_color}", result[:text_color]
    assert_equal "##{Settings.mail_css.highlight_text_color}", result[:highlight_text_color]
    assert_equal "##{Settings.mail_css.section_text_color}", result[:section_text_color]
    assert_equal "##{Settings.mail_css.card_text_color}", result[:card_text_color]
    assert_equal "##{Settings.mail_css.button_background_color}", result[:button_background_color]
  end

  test 'return custom org css params' do
    engine = Notify::Engine.new
    org = create :organization, name: 'CustomOrg'
    config_hash = { "enableCustomCss" => true, "headerBackgroundColor" => '#e3452d',
        "textColor" => '#000000', "buttonBackgroundColor" => '#45e324' }

    config = create :config, name: 'mail_custom_css', configable_id: org.id, configable_type: 'Organization',
        value: config_hash.to_json, data_type: 'json'

    result = engine.params_custom_css(org)
    assert_equal config_hash["headerBackgroundColor"], result[:header_background_color]
    assert_equal config_hash["textColor"], result[:text_color]
    assert_equal config_hash["textColor"], result[:highlight_text_color]
    assert_equal config_hash["textColor"], result[:section_text_color]
    assert_equal config_hash["textColor"], result[:card_text_color]
    assert_equal config_hash["buttonBackgroundColor"], result[:button_background_color]
  end

  test 'show edcast logo if enabled by org' do
    engine = Notify::Engine.new
    org = create :organization, name: 'CustomOrg'
    user = create :user, organization: org
    config = create :config, name: 'enable_edcast_logo', configable: org, value: true, data_type: 'boolean'

    result = engine.global_params(user)

    assert_equal true, result[:show_ed_logo]
  end

  test 'not show edcast logo if disabled by org' do
    engine = Notify::Engine.new
    org = create :organization, name: 'CustomOrg'
    user = create :user, organization: org
    config = create :config, name: 'enable_edcast_logo', configable: org, value: false, data_type: 'boolean'

    result = engine.global_params(user)

    assert_equal false, result[:show_ed_logo]
  end
  test 'show edcast promotions if enabled by org' do
    engine = Notify::Engine.new
    org = create :organization, name: 'CustomOrg'
    config_hash = {"promotions" => true}
    config = create :config, name: 'footer_options', configable_id: org.id, configable_type: 'Organization',
        value: config_hash.to_json, data_type: 'json'

    result = engine.org_params(org)

    assert_equal true, result[:promotions]
  end

  test 'hide edcast promotions if disabled by org' do
     engine = Notify::Engine.new
    org = create :organization, name: 'CustomOrg'
    config_hash = {"promotions" => false}
    config = create :config, name: 'footer_options', configable_id: org.id, configable_type: 'Organization',
        value: config_hash.to_json, data_type: 'json'

    result = engine.org_params(org)

    assert_equal false, result[:promotions]
  end

  test 'return org url' do
    engine = Notify::Engine.new
    org = create :organization, name: 'CustomOrg', host_name: "custom"
    result = engine.org_params(org)
    assert_not_empty result[:home_page]
  end

  test '#notification_params' do
    engine = Notify::Engine.new
    notifier = Notify::NewSmartbiteCommentNotifier.new
    engine.register_notifier(notifier, [Notify::EVENT_NEW_COMMENT])

    notification_entry = mock
    notification_entry.expects(:notification_name).returns(notifier.notification_name)

    notifier.expects(:notification_params)
            .with(notification_entry).returns('HELLO')

    assert_equal 'HELLO', engine.notification_params(notification_entry)
  end

  test '#register_notifier' do
    engine = Notify::Engine.new
    notifier = Notify::NewSmartbiteCommentNotifier.new
    engine.register_notifier(notifier, [Notify::EVENT_NEW_COMMENT])

    assert_same_elements engine.events_to_notifiers[Notify::EVENT_NEW_COMMENT], [notifier]
    assert_equal engine.notification_name_to_notifiers[Notify::NOTE_NEW_SMARTBITE_COMMENT], notifier
  end

  class SendingCustomTemplateTest < ActiveSupport::TestCase
    setup do
      @org = create :organization, name: 'Org'
      @user = create(
        :user, first_name: 'First', last_name: 'Last',
        email: 'first@last.com', organization: @org
      )
      @author = create :user, organization: @org

      @card = create :card, organization: @org, author: @user
      @comment = create :comment, user: @author, commentable: @card
      @note_entry = create(
        :notification_entry, user: @user, sourceable: @comment,
        date_in_timezone: Time.zone.today,
        sending_time_in_timezone: Time.zone.now
      )

      @note_entry.set_enabled(:email, :single)
      @note_entry.save

      @engine = Notify::Engine.new

      content = <<-TXT.strip_heredoc
        <!DOCTYPE html>
        <html>
          <body>
            <p> User first_name: {{user.first_name}}. {{user.email}}</p>
            <p> User last_name: {{user.last_name}}</p>
            <p> Commenter first_name: {{commenter.first_name}}</p>
            <p> Commenter last_name: {{commenter.last_name}}</p>
            <p> Content: {{content}}</p>
            <p> Assignment url: {{card.assignment_url}}</p>
            <p> Org support email: {{org.support_email}}</p>
            <p> Org logo: {{org.logo}}</p>
          </body>
        </html>
      TXT

      # default template
      @default_template = EmailTemplate.create(
        is_active: true, title: 'new_smartbite_comment', content: 'content',
        organization: @org, design: 'design', language: 'en', state: 'published'
      )
      # custom active template
      @custom_template = EmailTemplate.create(
        is_active: true, title: 'title', content: content,
        organization: @org, design: 'design', language: 'en',
        default_id: @default_template.id, creator_id: @user.id,
        state: 'published'
      )
    end

    test 'email notification should send custom template if it present and customization enabled' do
      create(:config, configable: @org, name: 'emailTemplatesEnabled', value: true)
      notifier = Notify::NewSmartbiteCommentNotifier.new
      @engine.register_notifier(notifier, [Notify::EVENT_NEW_COMMENT])
      encapsulated_params = @engine.encapsulated_params_for_notification(@note_entry)
      full_name = "#{@author.first_name} #{@author.last_name}"
      subject = "#{full_name} commented on your SmartCard \"#{@card.message}\"\n"
      user_info = {
        first_name: @user.first_name,
        last_name: @user.last_name,
        email: @user.email,
        org_name: @org.name
      }.with_indifferent_access

      specific_params = encapsulated_params[:new_smartbite_comment].first
      expected_vars = [
        {name: 'user', content: encapsulated_params[:user]},
        {name: 'commenter', content: specific_params[:commenter]},
        {name: 'content', content: specific_params[:content]},
        {name: 'card', content: specific_params[:card]},
        {name: 'org', content: encapsulated_params[:org]}
      ]

      # expected params for #send_email_mandrill
      @engine.expects(:send_email_mandrill).with(
        @org, user_info, subject, @custom_template.content, expected_vars
      )

      #sending email
      ne_name = @note_entry.notification_name
      @engine.send_email(
        encapsulated_params: encapsulated_params,
        template_name: ne_name,
        layout_name: @engine.notification_name_to_notifiers[ne_name].layout_name
      )
    end

    test 'email notification should send default template if customization disabled' do
      notifier = Notify::NewSmartbiteCommentNotifier.new
      @engine.register_notifier(notifier, [Notify::EVENT_NEW_COMMENT])
      encapsulated_params = @engine.encapsulated_params_for_notification(@note_entry)

      email_body = ''
      global_merge_vars = {}
      exp_org = nil

      # expected params for #send_email_mandrill
      @engine.expects(:send_email_mandrill).with(){|org, usr, subj, body, vars|
        email_body = body
        global_merge_vars = vars
        exp_org = org
      }

      #sending email
      ne_name = @note_entry.notification_name
      @engine.send_email(
        encapsulated_params: encapsulated_params,
        template_name: ne_name,
        layout_name: @engine.notification_name_to_notifiers[ne_name].layout_name
      )

      assert_not_equal @custom_template.content, email_body
      assert_equal nil, global_merge_vars
      assert_equal exp_org, @org
    end

    test '#custom_content should return proper values' do
      create(:config, configable: @org, name: 'emailTemplatesEnabled', value: true)
      notifier = Notify::NewSmartbiteCommentNotifier.new
      @engine.register_notifier(notifier, [Notify::EVENT_NEW_COMMENT])

      encapsulated_params = @engine.encapsulated_params_for_notification(@note_entry)

      custom_email_content = @engine.custom_content(
        template_name: @note_entry.notification_name,
        encapsulated_params: encapsulated_params
      )
      specific_params = encapsulated_params[:new_smartbite_comment].first
      expected_vars = [
        {name: 'user', content: encapsulated_params[:user]},
        {name: 'commenter', content: specific_params[:commenter]},
        {name: 'content', content: specific_params[:content]},
        {name: 'card', content: specific_params[:card]},
        {name: 'org', content: encapsulated_params[:org]}
      ]
      full_name = "#{@author.first_name} #{@author.last_name}"
      subject = "#{full_name} commented on your SmartCard \"#{@card.message}\"\n"

      # to return default subject since subject is not customized
      expectation = {
        subject: subject, body: @custom_template.content, variables: expected_vars
      }
      assert_equal expectation, custom_email_content
    end

    test '#custom_content should return custom subject of published custom template' do
      user = create(:user, organization: @org)
      create(:config, configable: @org, name: 'emailTemplatesEnabled', value: true)
      notifier = Notify::NewSmartbiteCommentNotifier.new
      @engine.register_notifier(notifier, [Notify::EVENT_NEW_COMMENT])

      encapsulated_params = @engine.encapsulated_params_for_notification(@note_entry)

      @custom_template.update(
        subject: '{{commenter.first_name}} commented on your SmartCard {{card.title_simplified}}'
      )

      custom_email_content = @engine.custom_content(
        template_name: @note_entry.notification_name,
        encapsulated_params: encapsulated_params
      )
      assert_equal custom_email_content[:subject], @custom_template.subject
    end

    test '#custom_content should return default subject if custom template(with subject) is not published' do
      user = create(:user, organization: @org)
      create(:config, configable: @org, name: 'emailTemplatesEnabled', value: true)
      notifier = Notify::NewSmartbiteCommentNotifier.new
      @engine.register_notifier(notifier, [Notify::EVENT_NEW_COMMENT])

      encapsulated_params = @engine.encapsulated_params_for_notification(@note_entry)

      @custom_template.update(
        state: 'draft',
        subject: '{{commenter.first_name}} commented on your SmartCard {{card.title_simplified}}'
      )

      subject = "#{@author.first_name} #{@author.last_name} commented on your SmartCard \"#{@card.message}\"\n"

      custom_email_content = @engine.custom_content(
        template_name: @note_entry.notification_name,
        encapsulated_params: encapsulated_params
      )

      default_email_content = @engine.get_email_content(
        encapsulated_params: encapsulated_params,
        template_name: @note_entry.notification_name,
        layout_name: 'default_layout'
      )

      assert_nil custom_email_content
      assert_equal subject, default_email_content[:email_subject]
    end

    test '#to_html_safe should properly set dynamic content syntax for mandrill' do
      variable_names_contain_tag = ['html']
      string_content = 'some regular variable {{variable}} and html {{html}}'
      res = @engine.to_htm_safe(string_content, variable_names_contain_tag)

      expected_result = 'some regular variable {{variable}} and html {{{html}}}'
      assert_equal expected_result, res
    end

    test '#extract_tags should properly take variable names which contain html tags' do
      key = 'two'
      data_under_key = {:three=>"<p>tag</p>", :four=>{:five=>"<p>tag</p>", six: 'text'}}
      res = @engine.extract_tags(data_under_key.with_indifferent_access, key)
      expected = %w[two.three two.four.five]

      assert_equal expected, res
    end

    test '#sanitized_variables should close all unclosed tag in string values in the object' do
      params = {
        a: 'string', b: 1, c: ['string', '<p>tag...'],
        d: {
          e: '<div>tag...',
          z: '<p>closed</p>',
          f: {
            g: ['<a>tag...'], h: 'string', j: '<p>tag...'}
        }
      }.with_indifferent_access

      res = @engine.sanitized_variables(params)

      expected = {
        a: 'string', b: 1, c: ['string', '<p>tag...</p>'],
        d: {
          e: '<div>tag...</div>',
          z: '<p>closed</p>',
          f: {
            g: ['<a>tag...</a>'], h: 'string', j: '<p>tag...</p>'}
        }
      }.with_indifferent_access

      assert_equal expected, res
    end

    test 'should properly get custom template based on language settings' do
      create(:config, configable: @org, name: 'emailTemplatesEnabled', value: true)
      notifier = Notify::NewSmartbiteCommentNotifier.new
      @engine.register_notifier(notifier, [Notify::EVENT_NEW_COMMENT])
      encapsulated_params = @engine.encapsulated_params_for_notification(@note_entry)

      content_fr = <<-TXT.strip_heredoc
        <!DOCTYPE html>
        <html>
          <body>
            <p>texte en français</p>
            <p>Dans le premier utilisateur nom: {{user.first_name}}</p>
          </body>
        </html>
      TXT

      content_de = <<-TXT.strip_heredoc
        <!DOCTYPE html>
        <html>
          <body>
            <p>Text in Deutsch</p>
            <p>Vorname des Benutzers: {{user.first_name}}</p>
          </body>
        </html>
      TXT

      custom_template_fr = EmailTemplate.create(
        is_active: true, title: 'title_fr', content: content_fr,
        organization: @org, design: 'design', language: 'fr',
        default_id: @default_template.id, creator_id: @user.id,
        state: 'published'
      )

      custom_template_de = EmailTemplate.create(
        is_active: true, title: 'title_de', content: content_de,
        organization: @org, design: 'design', language: 'de',
        default_id: @default_template.id, creator_id: @user.id,
        state: 'published'
      )

      args = {template_name: 'new_smartbite_comment', encapsulated_params: encapsulated_params}
      content = @engine.send(:custom_content, args)[:body]
      # base language 'en'
      #   lowest priority
      assert_equal @custom_template.content, content

      create(:config, configable: @org, name: 'DefaultOrgLanguage', value: 'fr')
      content = @engine.send(:custom_content, args)[:body]
      # default organization language if it present
      assert_equal custom_template_fr.content, content

      # user preferred language if it present
      #   highest priority
      create(:user_profile, user: @user, language: 'de')
      content = @engine.send(:custom_content, args)[:body]

      assert_equal custom_template_de.content, content
    end
  end
end

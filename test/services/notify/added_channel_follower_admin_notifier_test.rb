require 'test_helper'

class Notify::AddedChannelFollowerAdminNotifierTest < ActiveSupport::TestCase
  setup do
    @notifier = Notify::AddedChannelFollowerAdminNotifier.new
    @org = create :organization
    @user = create :user, organization: @org
    @admin = create :user, organization: @org, organization_role: 'admin'
    @follower = create :user, organization: @org
    @channel = create :channel, user: @user, organization: @org
  end

  test 'notify admin user added to channel as follower' do
    @channel.followers << @follower
    Follow.where(user_id: @follower.id, followable_id: @channel.id, followable_type: 'Channel').first
    batches = []
    custom_hash = {sender_id: @user.id, channels: [@channel.label], followers_emails: [@follower.email], teams_names: [] }

    @notifier.users_to_notify(Notify::EVENT_ADD_CHANNEL_FOLLOWER, @org, custom_hash) { |x| batches << x }

    assert_equal 1, batches.count
    assert_same_elements [@admin.id], batches[0]
  end

  test 'notify admin group added to channel as follower' do
    team = create :team, name: 'Group 1', organization: @org
    team.add_member @follower

    @channel.followed_teams << team
    batches = []
    custom_hash = {sender_id: @user.id, channels: [@channel.label], followers_emails: [], teams_names: ['Group 1'] }

    @notifier.users_to_notify(Notify::EVENT_ADD_CHANNEL_FOLLOWER, @org, custom_hash) { |x| batches << x }

    assert_equal 1, batches.count
    assert_same_elements [@admin.id], batches[0]
  end

  test 'verify subject of email when name is present' do
    @channel.followers << @follower
    custom_hash = {sender_id: @user.id, channels: [@channel.label], followers_emails: [@follower.email] }

    config = build :notification_config, organization: @org
    config.set_enabled(Notify::NOTE_ADDED_CHANNEL_FOLLOWER_ADMIN, :email, :single)
    config.save
    mail_args = {}

    EDCAST_NOTIFY.expects(:send_email_mandrill).with(){|organization, user_info, subject, body|
      mail_args[:subject] = subject
    }.at_least_once

    EDCAST_NOTIFY.trigger_event(Notify::EVENT_ADD_CHANNEL_FOLLOWER_ADMIN, @org, custom_hash)
    EDCAST_NOTIFY.send_single_notification(NotificationEntry.last)

    assert_equal  "#{@user.first_name} #{@user.last_name} has added the below follower to the channel", mail_args[:subject]
  end

  test 'verify subject of email when name is not present' do
    user = create :user, organization: @org, first_name: ""
    @channel.followers << @follower
    custom_hash = {sender_id: user.id, channels: [@channel.label], followers_emails: [@follower.email] }

    config = build :notification_config, organization: @org
    config.set_enabled(Notify::NOTE_ADDED_CHANNEL_FOLLOWER_ADMIN, :email, :single)
    config.save
    mail_args = {}

    EDCAST_NOTIFY.expects(:send_email_mandrill).with(){|organization, user_info, subject, body|
      mail_args[:subject] = subject
    }.at_least_once

    EDCAST_NOTIFY.trigger_event(Notify::EVENT_ADD_CHANNEL_FOLLOWER_ADMIN, @org, custom_hash)
    EDCAST_NOTIFY.send_single_notification(NotificationEntry.last)

    assert_equal  "Admin has added the below follower to the channel", mail_args[:subject]
  end
end

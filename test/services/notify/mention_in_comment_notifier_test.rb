require 'test_helper'

class Notify::MentionInCommentNotifierTest < ActiveSupport::TestCase
  setup do
    @notifier = Notify::MentionInCommentNotifier.new
  end

  test '#users to notify' do
    org = create :organization
    user = create :user, organization: org, handle: '@apple'
    mentioned_user = create :user, organization: org, handle: '@newton'
    mentioned_user1 = create :user, organization: org, handle: '@newton1'
    card = create(:card, organization: org, author: user)
    comment = create(:comment, message: 'Hey @newton, @newton1 How does the apple tasstes', commentable_id: card.id, commentable_type: 'Card', user: user)
    batches = []
    @notifier.users_to_notify(Notify::EVENT_NEW_COMMENT, comment) { |x| batches = x }
    assert_equal 2, batches.count
    assert_same_elements [mentioned_user.id, mentioned_user1.id], batches
  end
end
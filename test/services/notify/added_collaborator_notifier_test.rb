require 'test_helper'

class Notify::AddedCollaboratorNotifierTest < ActiveSupport::TestCase
  setup do
    @notifier = Notify::AddedCollaboratorNotifier.new
  end

  test '#users to notify' do
    org = create(:organization)
    user = create :user, organization: org
    inviter = create :user, organization: org
    channel = create :channel, curate_only: true, organization: org
    channel.authors << user
    # channel.send_curator_invites tos: user, from: inviter
    batches = []
    channel_author = ChannelsUser.where(channel_id: channel.id, user_id: user.id).first

    @notifier.users_to_notify(Notify::EVENT_ADD_CURATOR, channel_author) { |x| batches << x }
    assert_equal 1, batches.count
    assert_equal [[channel_author.user.id]], batches
  end
end
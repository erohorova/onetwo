require 'test_helper'

class Notify::NewSmartbiteCommentNotifierTest < ActiveSupport::TestCase
  setup do
    @notifier = Notify::NewSmartbiteCommentNotifier.new
  end

  test '#users_to_notify' do
    org = create(:organization)
    card_author = create :user, organization: org
    comment_author = create :user, organization: org

    card = create :card, author_id: card_author.id
    comment = create :comment, commentable: card, user: comment_author

    batches = []
    @notifier.users_to_notify(Notify::EVENT_NEW_COMMENT, comment) { |x| batches << x }

    assert_equal 1, batches.count
    assert_same_elements [card_author.id], batches[0]
  end

  test '#notification_params' do

  end
end

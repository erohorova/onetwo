require 'test_helper'

class Notify::ContentSharedNotifierTest < ActiveSupport::TestCase
  setup do
    @notifier = Notify::ContentSharedNotifier.new
    @org = create(:organization)
    @author = create :user, organization: @org
    @card = create :card, author_id: @author.id
  end

  test 'notify group members when content shared with group' do
    team = create :team, organization: @org
    team_members = (0..2).map{|x| u = create(:user, organization: @org); create :teams_user, user: u, team: team; u}
    create :teams_card, card: @card, team: team, type: 'SharedCard'

    batches = []
    @notifier.users_to_notify(Notify::EVENT_CONTENT_SHARE, @card, {event: Card::EVENT_CARD_SHARE, team_ids: [team.id]}) { |x| batches << x }

    assert_equal 1, batches.count
    assert_same_elements team_members.map(&:id), batches[0]
  end

  test 'notify shared users when content shared with users' do
    shared_users = (0..2).map{|x| u = create(:user, organization: @org); create :card_user_share, user: u, card: @card; u}

    batches = []
    @notifier.users_to_notify(Notify::EVENT_CONTENT_SHARE, @card, {event: Card::EVENT_CARD_SHARE, user_ids: shared_users.map(&:id)}) { |x| batches << x }

    assert_equal 1, batches.count
    assert_same_elements shared_users.map(&:id), batches[0]
  end

  test 'notify followers who are also in shared users and other shared users when content is shared' do 
    user1 = create(:user, organization: @org)
    create :follow, user: user1, followable: @author
    user2 = create(:user, organization: @org)
    create :follow, user: user2, followable: @author
    shared_users = (0..2).map{|x| u = create(:user, organization: @org); create :card_user_share, user: u, card: @card; u}
    create :card_user_share, user: user1, card: @card

    user_ids = [user1.id] + shared_users.map(&:id)
    batches = []
    @notifier.users_to_notify(Notify::EVENT_CONTENT_SHARE, @card, {event: Card::EVENT_CARD_SHARE, user_ids: user_ids}) { |x| batches << x }

    assert_equal 1, batches.count
    assert_same_elements user_ids, batches[0]
  end

  test 'notify shared users if card is private' do
    @card.update(is_public: 0)
    shared_users = (0..2).map{|x| u = create(:user, organization: @org); create :card_user_share, user: u, card: @card; u}

    batches = []
    @notifier.users_to_notify(Notify::EVENT_CONTENT_SHARE, @card, {event: Card::EVENT_CARD_SHARE, user_ids: shared_users.map(&:id)}) { |x| batches << x }

    assert_equal 1, batches.count
    assert_same_elements shared_users.map(&:id), batches[0]
  end

  test 'notify followers only once when they are also in shared users' do
    team = create :team, organization: @org
    users_following_author_and_team_member = (0..2).map do |x| 
      u = create(:user, organization: @org)
      create :teams_user, user: u, team: team
      create :follow, user: u, followable: @author
      u
    end
    create :teams_card, card: @card, team: team, type: 'SharedCard'
    follower_notifier = Notify::NewContentByFollowedUserNotifier.new

    batch1, batch2 = [], []
    @notifier.users_to_notify(Notify::EVENT_CONTENT_SHARE, @card, {event: Card::EVENT_NEW_CARD_SHARE, team_ids: [team.id]}) { |x| batch1 << x }
    follower_notifier.users_to_notify(Notify::EVENT_NEW_CARD, @card, {event: Card::EVENT_NEW_CARD_SHARE, team_ids: [team.id]}) { |x| batch2 << x }
    assert_equal 1, batch1.count
    assert_equal 0, batch2.flatten.count
    assert_same_elements users_following_author_and_team_member.map(&:id), batch1[0]
  end

  test 'notify only restricted users if card is restricted on some users along with sharing' do
    team = create :team, organization: @org
    user = create :user, organization: @org
    user1 = create(:user, organization: @org)
    
    team.add_member(user1)
    create :teams_card, card: @card, team: team, type: 'SharedCard'
    create :card_user_permission, card_id: @card.id, user_id: user.id
    create :card_team_permission, card_id: @card.id, team_id: team.id

    batches = []
    @notifier.users_to_notify(Notify::EVENT_CONTENT_SHARE, @card, {event: Card::EVENT_CARD_SHARE, permitted_user_ids: [user.id],
      permitted_team_ids: [team.id]}) { |x| batches << x }
    assert_same_elements batches[0], [user.id, user1.id]
  end
end

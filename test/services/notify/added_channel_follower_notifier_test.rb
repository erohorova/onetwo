require 'test_helper'

class Notify::AddedChannelFollowerNotifierTest < ActiveSupport::TestCase
  setup do
    @notifier = Notify::AddedChannelFollowerNotifier.new
  end

  test '#notify user added to channel as follower' do
    org = create :organization
    user = create :user, organization: org
    follower = create :user, organization: org
    channel = create :channel, user: user, organization: org
    channel.followers << follower
    channel_follower = Follow.where(user_id: follower.id, followable_id: channel.id, followable_type: 'Channel').first
    batches = []

    @notifier.users_to_notify(Notify::EVENT_ADD_CHANNEL_FOLLOWER, channel_follower) { |x| batches << x }

    assert_equal 1, batches.count
    assert_same_elements [follower.id], batches[0]
  end
end
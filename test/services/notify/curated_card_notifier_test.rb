require 'test_helper'

class Notify::CuratedCardNotifierTest < ActiveSupport::TestCase
  setup do
    notifier = Notify::CuratedCardNotifier.new
    @user = create :user
    channel = create :channel, user: @user, organization: @user.organization, curate_only: true
    curator = create :user, organization: @user.organization
    channel.curators << curator
    channel.followers << @user
    @channels_cards = []
    create_list(:card, 2, author: @user).each do |card|
      @channels_cards << create(:channels_card, channel_id: channel.id, card_id: card.id, state: ChannelsCard::STATE_NEW)
    end
  end

  test '#notify author when a card posted to a curatable channel is published' do
    @channels_cards[0].curate
    notification = NotificationEntry.where(event_name: 'curate_card')
    assert_equal 1, notification.count
    assert_equal 'curated_card', notification.first.notification_name
    assert_equal @channels_cards[0].card.author_id, notification.first.user_id
  end

  test 'generate web notification when a card posted to a curatable channel is skipped' do
    @channels_cards[0].curate
    message = "Your card has been successfully published to channel: #{@channels_cards[0].channel.label}"
    notification = Notification.where(notification_type: 'curated_card')
    assert_equal 1, notification.count
    assert_equal 'ChannelsCard', notification.first.notifiable_type
    assert_equal @channels_cards[0].card.author_id, notification.first.user_id
    assert_equal message, notification.first.custom_message
  end

  test "#notify author when a card posted to a curatable channel is published via email" do
    config = build :notification_config, organization: @user.organization
    config.set_enabled(Notify::NOTE_CURATED_CARD, :email, :single)
    config.save

    EDCAST_NOTIFY.expects(:send_email_mandrill).once

    @channels_cards[0].curate
    EDCAST_NOTIFY.send_single_notification(NotificationEntry.last)
  end

  test "#notify author when a card posted to a curatable channel is published via daily digest" do
    config = build :notification_config, organization: @user.organization
    config.set_enabled(Notify::NOTE_CURATED_CARD, :email, :digest_daily)
    config.save

    @channels_cards[0].curate
    assert_equal true, NotificationEntry.last.config_email_digest_daily
  end
end
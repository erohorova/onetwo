require 'test_helper'

class Notify::NewLivestreamFromFollowedUserNotifierTest < ActiveSupport::TestCase
  setup do
    @notifier = Notify::NewLivestreamFromFollowedUserNotifier.new
  end

  test '#users_to_notify' do
    org = create :organization
    card_author = create :user, organization: org
    followers = create_list(:user, 3, organization: org)
    followers.each do |f|
      Follow.create(user_id: f.id, followable_type: 'User', followable_id: card_author.id )
    end

    livestream = create :card, card_type: 'video_stream', author_id: card_author.id

    batches = []
    @notifier.users_to_notify(Notify::EVENT_NEW_LIVESTREAM, livestream) { |x| batches = x }
    assert_equal 3, batches.count
    assert_same_elements followers.map(&:id), batches
  end

  test 'for private cards dont notify anybody' do
    org = create(:organization)
    author = create :user, organization: org
    user_list = create_list(:user, 2, organization: org)

    livestream = create :card, card_type: 'video_stream', author_id: author.id, is_public: 0

    batches = []
    @notifier.users_to_notify(Notify::EVENT_NEW_LIVESTREAM, livestream) { |x| batches << x }
    
    assert_equal 1, batches.count
    assert batches.first.empty?
  end
end

require 'test_helper'

class Notify::GroupUserPromotedToAdminNotifierTest < ActiveSupport::TestCase
  setup do
    @notifier = Notify::GroupUserPromotedToAdminNotifier.new
    @org = create(:organization)
    @admin = create(:user, organization: @org, organization_role: 'admin')
    @team = create :team, organization: @org
    @team.add_admin(@admin)
    @users = create_list(:user, 3, organization: @org)
    @users.each { |user| @team.add_member(user) }
  end

  test 'should create web notification when group member is promoted to group leader' do
    team_user_member = @users.first
    @team.add_admin(team_user_member, actor_name: @admin.name)

    batches = []
    @notifier.users_to_notify(Notify::EVENT_GROUP_USER_PROMOTED_TO_ADMIN, @team, { user_id: team_user_member.id, actor_name: @admin.name }) { |x| batches << x }

    notification = Notification.where(notification_type: 'group_user_promoted_to_admin')
    message = "#{@admin.name} added you as a Group Leader to #{@team.name}"

    assert_equal 1, batches.count
    assert_same_elements [team_user_member.id], batches[0]
    assert_equal 1, notification.count
    assert_equal team_user_member.id, notification.first.user_id
    assert_equal message, notification.first.custom_message
  end

  test 'should create a notification when group admin is promoted to group leader' do
    group_sub_admin_user = create(:user, organization: @org, organization_role: 'member')
    @team.add_sub_admin(group_sub_admin_user)
    @team.add_admin(group_sub_admin_user, actor_name: @admin.name)

    batches = []
    @notifier.users_to_notify(Notify::EVENT_GROUP_USER_PROMOTED_TO_ADMIN, @team, { user_id: group_sub_admin_user.id, actor_name: @admin.name }) { |x| batches << x }

    notification = Notification.where(notification_type: 'group_user_promoted_to_admin')
    message = "#{@admin.name} added you as a Group Leader to #{@team.name}"

    assert_equal 1, batches.count
    assert_same_elements [group_sub_admin_user.id], batches[0]
    assert_equal 1, notification.count
    assert_equal group_sub_admin_user.id, notification.first.user_id
    assert_equal message, notification.first.custom_message
  end

  test 'should not create notification when group leader is de-promoted to group admin' do
    group_sub_admin_user = create(:user, organization: @org, organization_role: 'member')
    @team.add_admin(group_sub_admin_user, actor_name: @admin.name)
    @team.add_sub_admin(group_sub_admin_user)

    notification = Notification.where(notification_type: 'group_user_promoted_to_admin')

    assert_equal 0, notification.count
  end


  test 'should not create notification when group leader is de-promoted to group member' do
    group_sub_admin_user = create(:user, organization: @org, organization_role: 'member')
    @team.add_admin(group_sub_admin_user, actor_name: @admin.name)
    @team.add_member(group_sub_admin_user)

    notification = Notification.where(notification_type: 'group_user_promoted_to_admin')

    assert_equal 0, notification.count
  end

end

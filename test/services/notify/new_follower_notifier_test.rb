require 'test_helper'

class Notify::NewFollowerNotifierTest < ActiveSupport::TestCase
  setup do
    @notifier = Notify::NewFollowerNotifier.new
  end

  test '#users to notify' do
    org = create :organization
    user1 = create :user, organization: org
    user2 = create :user, organization: org

    follow = Follow.create(user_id: user1.id, followable_id: user2.id, followable_type: 'User')
    batches = []
    @notifier.users_to_notify(Notify::EVENT_NEW_FOLLOW, follow) { |x| batches << x }

    assert_equal 1, batches.count
    assert_same_elements [user2.id], batches[0]
  end
end
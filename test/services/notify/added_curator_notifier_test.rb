require 'test_helper'

class Notify::AddedCuratorNotifierTest < ActiveSupport::TestCase
  setup do
    @notifier = Notify::AddedCuratorNotifier.new
  end

  test '#users to notify' do
    org = create(:organization)
    user = create :user, organization: org
    inviter = create :user, organization: org
    channel = create :channel, curate_only: true, organization: org
    channel.curators << user
    # channel.send_curator_invites tos: user, from: inviter
    batches = []
    channel_curator = ChannelsCurator.where(channel_id: channel.id, user_id: user.id).first

    @notifier.users_to_notify(Notify::EVENT_ADD_CURATOR, channel_curator) { |x| batches << x }
    assert_equal 1, batches.count
    assert_equal [[channel_curator.user.id]], batches
  end
end
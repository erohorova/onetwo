require 'test_helper'

class Notify::IntegrationTest < ActiveSupport::TestCase
  setup do
    Comment.any_instance.stubs(:record_comment_event)
    ActiveJob::Base.queue_adapter = :inline

    @card_author = create :user, first_name: 'First', last_name: 'Last', email: 'first@last.com'
    @org = @card_author.organization
    @card = create :card, author: @card_author, message: 'Message', title: 'Card title'
    @card.reload

    @comment_author = create :user, first_name: 'Comm', last_name: 'Ent', email: 'comm@ent.com', organization: @org
    @comment = create :comment, commentable: @card, user: @comment_author
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(false)
  end

  teardown do
    Organization.any_instance.unstub(:notifications_and_triggers_enabled?)
  end

  test 'ondemand integration' do
    # Setup config
    config = build :notification_config, user: @card_author
    config.set_enabled(Notify::NOTE_NEW_SMARTBITE_COMMENT, :email, :single)
    config.save

    # Set boundary expectations
    EDCAST_NOTIFY.expects(:send_email_mandrill).never
    TestAfterCommit.enabled = true
    # Trigger the event
    EDCAST_NOTIFY.trigger_event(Notify::EVENT_NEW_COMMENT, @comment)
  end


  test 'should not trigger notifications if disabled' do
    TestAfterCommit.enabled = true
    Organization.any_instance.unstub(:notifications_and_triggers_enabled?)
    # Setup config
    config = build :notification_config, user: @card_author
    config.set_enabled(Notify::NOTE_NEW_SMARTBITE_COMMENT, :email, :single)
    config.save
    # Set boundary expectations(new_smartbite_Comment + new_activity)
    EDCAST_NOTIFY.expects(:send_email_mandrill).never

    # Trigger the event
    EDCAST_NOTIFY.trigger_event(Notify::EVENT_NEW_COMMENT, @comment)
  end

  test 'should not send any mails if send_no_mail config is set to true' do
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(true)
    Organization.any_instance.stubs(:send_no_mails?).returns(true)
    # Set boundary expectations(new_smartbite_Comment + new_activity)
    EDCAST_NOTIFY.expects(:send_email_mandrill).never

    # Trigger the event
    EDCAST_NOTIFY.trigger_event(Notify::EVENT_NEW_COMMENT, @comment)
  end

  test 'should not send any mails if send_no_mail config is set to true and triggers and notifications is not enabled' do
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(false)
    Organization.any_instance.stubs(:send_no_mails?).returns(true)
    # Set boundary expectations(new_smartbite_Comment + new_activity)
    MandrillMailer.expects(:send_api_email).never

    # Trigger the event
    UserMailer.follower_batch_notification_email(@card_author, create_list(:user, 2))
  end
  # notifications_and_triggers_enabled?
end

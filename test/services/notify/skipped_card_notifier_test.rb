require 'test_helper'

class Notify::SkippedCardNotifierTest < ActiveSupport::TestCase
  setup do
    @org = create(:organization)
    user = create :user, organization: @org
    channel = create :channel, user: user, organization: @org, curate_only: true
    curator = create :user, organization: @org
    channel.curators << curator
    channel.followers << user
    @channels_cards = []
    create_list(:card, 2, author: user).each do |card|
      @channels_cards << create(:channels_card, channel_id: channel.id, card_id: card.id, state: ChannelsCard::STATE_NEW)
    end
  end

  test '#notify author when a card posted to a curatable channel is skipped' do
    @channels_cards[0].skip
    notification_entry = NotificationEntry.where(event_name: 'skip_card')
    assert_equal 1, notification_entry.count
    assert_equal 'skipped_card', notification_entry.first.notification_name
    assert_equal @channels_cards[0].card.author_id, notification_entry.first.user_id
  end

  test 'generate web notification when a card posted to a curatable channel is skipped' do
    @channels_cards[0].skip
    message = "Your card was rejected by the curator of the channel: #{@channels_cards[0].channel.label}"
    notification = Notification.where(notification_type: 'skipped_card')
    assert_equal 1, notification.count
    assert_equal 'ChannelsCard', notification.first.notifiable_type
    assert_equal @channels_cards[0].card.author_id, notification.first.user_id
    assert_equal message, notification.first.custom_message
  end

  test "#notify author when a card posted to a curatable channel is skipped via email" do
    config = build :notification_config, organization: @org
    config.set_enabled(Notify::NOTE_SKIPPED_CARD, :email, :single)
    config.save

    EDCAST_NOTIFY.expects(:send_email_mandrill).once

    @channels_cards[0].skip
    EDCAST_NOTIFY.send_single_notification(NotificationEntry.last)
  end

  test "#notify author when a card posted to a curatable channel is skipped via daily digest" do
    config = build :notification_config, organization: @org
    config.set_enabled(Notify::NOTE_SKIPPED_CARD, :email, :digest_daily)
    config.save

    @channels_cards[0].skip
    assert_equal true, NotificationEntry.last.config_email_digest_daily
  end
end
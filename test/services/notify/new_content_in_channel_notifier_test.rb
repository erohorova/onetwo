require 'test_helper'

class Notify::NewContentInChannelNotifierTest < ActiveSupport::TestCase
  setup do
    @notifier = Notify::NewContentInChannelNotifier.new
  end

  test '#notify followers when stream is posted to channel' do
    org = create :organization
    user = create :user, organization: org
    follower = create :user, organization: org
    channel = create :channel, user: user, organization: org
    channel.followers << follower

    card = create :card, organization: org, author_id: user.id
    channel_card = create :channels_card, card_id: card.id, channel_id: channel.id
    video_stream = create(:wowza_video_stream, organization_id: org.id, creator: user, card: card)

    batches = []

    @notifier.users_to_notify(Notify::EVENT_NEW_CONTENT_IN_CHANNEL, channel_card) { |x| batches << x }

    assert_equal 1, batches.count
    assert_same_elements [follower.id], batches[0]
  end

  test 'verify user params' do
    org = create :organization
    user = create :user, organization: org
    follower = create :user, organization: org
    channel = create :channel, user: user, organization: org
    channel.followers << follower

    card = create :card, organization: org, author_id: user.id
    channel_card = create :channels_card, card_id: card.id, channel_id: channel.id
    video_stream = create(:wowza_video_stream, organization_id: org.id, creator: user, card: card)
    mail_args = {}

    EDCAST_NOTIFY.trigger_event(Notify::EVENT_NEW_CONTENT_IN_CHANNEL, channel_card)

    notification_params = @notifier.notification_params(NotificationEntry.last)[:user]

    assert_equal notification_params[:first_name], user.first_name
    assert_equal notification_params[:last_name], user.last_name
  end


  test '#notify followers once when following both creator and channel' do
    org = create :organization
    user = create :user, organization: org
    follower = create :user, organization: org
    channel = create :channel, user: user, organization: org
    create(:follow, followable: channel, user: follower)
    create(:follow, followable: user, user: follower)

    card = create :card, organization: org, author_id: user.id
    channel_card = create :channels_card, card_id: card.id, channel_id: channel.id
    video_stream = create(:wowza_video_stream, organization_id: org.id, creator: user, card: card)

    EDCAST_NOTIFY.trigger_event(Notify::EVENT_NEW_LIVESTREAM, card)
    assert_equal 1, NotificationEntry.count
    assert_equal follower.id, NotificationEntry.find_by(event_name: Notify::EVENT_NEW_LIVESTREAM, sourceable_id: card.id).user_id

    assert_no_difference -> {NotificationEntry.count} do
      EDCAST_NOTIFY.trigger_event(Notify::EVENT_NEW_CONTENT_IN_CHANNEL, channel_card)
    end
  end

  test "#notify channel team user of live stream" do
    org = create :organization
    user = create :user, organization: org
    team_user = create :user, organization: org
    channel = create :channel, user: user, organization: org
    team = create(:team, organization: org)
    team.add_user team_user
    team.teams_channels_follows.create(channel: channel)
    follower = create :user, organization: org
    channel.followers << follower

    card = create :card, organization: org, author_id: user.id
    channel_card = create :channels_card, card_id: card.id, channel_id: channel.id
    video_stream = create(:wowza_video_stream, organization_id: org.id, creator: user, card: card)

    EDCAST_NOTIFY.trigger_event(Notify::EVENT_NEW_CONTENT_IN_CHANNEL, channel_card)

    assert_equal 2, NotificationEntry.count
    assert_equal NotificationEntry.where(user_id: team_user.id).count, 1
    assert_equal NotificationEntry.where(user_id: follower.id).count, 1
  end

  test "#should sent notification once when the user follows the channel and is also in team which is in channel" do
    org = create :organization
    user = create :user, organization: org
    team_user = create :user, organization: org
    channel = create :channel, user: user, organization: org
    team = create(:team, organization: org)
    team.add_user team_user
    team.teams_channels_follows.create(channel: channel)
    follower = create :user, organization: org
    channel.followers << follower
    channel.followers << team_user

    card = create :card, organization: org, author_id: user.id
    channel_card = create :channels_card, card_id: card.id, channel_id: channel.id
    video_stream = create(:wowza_video_stream, organization_id: org.id, creator: user, card: card)

    EDCAST_NOTIFY.trigger_event(Notify::EVENT_NEW_CONTENT_IN_CHANNEL, channel_card)

    assert_equal 2, NotificationEntry.count
    assert_equal NotificationEntry.where(user_id: team_user.id).count, 1
    assert_equal NotificationEntry.where(user_id: follower.id).count, 1
  end
end

require 'test_helper'

class Notify::NewContentByFollowedUserNotifierTest < ActiveSupport::TestCase
  setup do
    @notifier = Notify::NewContentByFollowedUserNotifier.new
  end

  test '#users_to_notify' do
    org = create(:organization)
    author = create :user, organization: org
    users_following = (0..5).map{|x| u = create(:user, organization: org); create :follow, user: u, followable: author; u}
    users_not_following = (0..5).map{|x| create :user, organization: org}

    card = create :card, author_id: author.id

    batches = []
    @notifier.users_to_notify(Notify::EVENT_NEW_CARD, card, {}) { |x| batches << x }

    assert_equal 1, batches.count
    assert_same_elements users_following.map(&:id), batches[0]
  end

  test 'notify only followers when card shared with followers and other users' do
    org = create(:organization)
    author = create :user, organization: org
    users_following = (0..3).map{|x| u = create(:user, organization: org); create :follow, user: u, followable: author; u}
    card = create :card, author_id: author.id
    team = create :team, organization: org
    team_members = (0..2).map{|x| u = create(:user, organization: org); create :teams_user, user: u, team: team; u}
    create :teams_card, card: card, team: team, type: 'SharedCard'

    batches = []
    @notifier.users_to_notify(Notify::EVENT_NEW_CARD, card, {event: Card::EVENT_CARD_SHARE, team_ids: [team.id]}) { |x| batches << x }

    assert_equal 1, batches.count
    assert_same_elements users_following.map(&:id), batches[0]
  end

  test 'for private cards dont notify anybody' do
    org = create(:organization)
    author = create :user, organization: org
    user_list = create_list(:user, 2, organization: org)

    card = create :card, author_id: author.id, is_public: 0

    batches = []
    @notifier.users_to_notify(Notify::EVENT_NEW_CARD, card, {}) { |x| batches << x }
    
    assert_equal 1, batches.count
    assert batches.first.empty?
  end

  test 'for cards shared to private channel dont notify if user cant access the card' do
    org = create(:organization)
    author = create :user, organization: org
    user_list = create_list(:user, 2, organization: org)
    followers = user_list.map {|user| create :follow, user: user, followable: author}
    private_channel = create :channel, organization: org, is_private: true
    create(:follow, followable: private_channel, user_id: user_list.first.id)

    card = create :card, author_id: author.id, channel_ids: private_channel.id

    batches = []
    @notifier.users_to_notify(Notify::EVENT_NEW_CARD, card, {}) { |x| batches << x }
    
    assert_equal 1, batches.count
    assert_equal batches.first[0], user_list.first.id
  end

  test '#notification_params' do

  end
end

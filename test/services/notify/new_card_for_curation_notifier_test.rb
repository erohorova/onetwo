require 'test_helper'

class Notify::NewCardForCurationNotifierTest < ActiveSupport::TestCase
  setup do
    @notifier = Notify::NewCardForCurationNotifier.new
  end

  test '#notify curator when a new card has been added to curation required channel' do
    org = create :organization
    user = create :user, organization: org
    curator = create :user, organization: org
    curator1 = create :user, organization: org
    follower = create :user, organization: org
    channel = create :channel, user: user, organization: org, curate_only: true
    channel.curators << curator1
    channel.curators << curator
    channel.followers << follower
    # channel_follower = Follow.where(user_id: follower.id, followable_id: channel.id, followable_type: 'Channel').first
    card = create(:card, author_id: follower.id, organization: org)
    channel.cards << card

    assert_equal 2, NotificationEntry.count
  end

  test 'should not notify curator when a new card has been added to non curation required channel' do
    org = create :organization
    user = create :user, organization: org
    curator = create :user, organization: org
    curator1 = create :user, organization: org
    follower = create :user, organization: org
    channel = create :channel, user: user, organization: org
    channel.curators << curator1
    channel.curators << curator
    channel.followers << follower
    # channel_follower = Follow.where(user_id: follower.id, followable_id: channel.id, followable_type: 'Channel').first
    card = create(:card, author_id: follower.id, organization: org)
    channel.cards << card

    assert_equal 0, NotificationEntry.count
  end
end
require 'test_helper'

class Notify::NotifierTest < ActiveSupport::TestCase
  test '#execute_for_notification_web_only' do
    org = create :organization
    notifier = Notify::Notifier.new
    notifier.notification_name = Notify::NOTE_NEW_SMARTBITE_COMMENT
    notifier.expects(:get_organization).returns(org)
    user = create :user, organization: org
    notifier.expects(:users_to_notify).yields([user.id])

    # Assert creation of proper notifications
    notifications = []
    notifier.execute_for_notification_web_only(Notify::EVENT_NEW_COMMENT, user) do |m|
      notifications << m
    end
    notification = notifications.first
    assert notification.new_record?
    assert_equal Notify::EVENT_NEW_COMMENT, notification.event_name
    assert_equal Notify::NOTE_NEW_SMARTBITE_COMMENT, notification.notification_name
  end

  test '#execute_for_event' do
    # Create the service
    org = create :organization
    notifier = Notify::Notifier.new
    notifier.notification_name = Notify::NOTE_NEW_SMARTBITE_COMMENT
    notifier.set_enabled(:email, :single)
    notifier.set_enabled(:push, :single)

    notifier.expects(:get_organization).returns(org)

    # Create a user and notification preferences
    user = create :user, organization: org
    config = build :notification_config, user_id: user.id
    # commenting out below test as we suppressed all user preffereces for notifications EP-6329
    # config.set_enabled(Notify::NOTE_NEW_SMARTBITE_COMMENT, :web, :single)
    # config.set_enabled(Notify::NOTE_NEW_SMARTBITE_COMMENT, :email, :single)
    # config.set_enabled(Notify::NOTE_NEW_SMARTBITE_COMMENT, :push, :single)
    # config.save

    # Create mocks
    notifier.expects(:users_to_notify).yields([user.id])

    # Assert creation of proper notifications
    notifications = []
    notifier.execute_for_event(Notify::EVENT_NEW_COMMENT, user) do |m|
      notifications << m
    end

    notification = notifications.first
    assert_equal 1, notifications.count
    assert_equal Notify::EVENT_NEW_COMMENT, notification.event_name
    assert_equal Notify::NOTE_NEW_SMARTBITE_COMMENT, notification.notification_name

    # Configs
    assert_not notification.enabled?(:web, :single)

    assert_not notification.enabled?(:email, :single)
    assert_not notification.enabled?(:email, :digest_daily)
    assert_not notification.enabled?(:email, :digest_weekly)

    assert_not notification.enabled?(:push, :single)
    assert_not notification.enabled?(:push, :digest_daily)
    assert_not notification.enabled?(:push, :digest_weekly)

    # Statuses
    assert_equal Notify::STATUS_UNSENT, notification.get_status(:web, :single)

    assert_equal Notify::STATUS_UNSENT, notification.get_status(:email, :single)
    assert_equal Notify::STATUS_UNSENT, notification.get_status(:email, :digest_daily)
    assert_equal Notify::STATUS_UNSENT, notification.get_status(:email, :digest_weekly)

    assert_equal Notify::STATUS_UNSENT, notification.get_status(:push, :single)
    assert_equal Notify::STATUS_UNSENT, notification.get_status(:push, :digest_daily)
    assert_equal Notify::STATUS_UNSENT, notification.get_status(:push, :digest_weekly)
  end

  # commenting out below test as we suppressed all user preffereces for notifications EP-6329
  # test '#resolve_config returns user config' do
  #   notifier = Notify::Notifier.new
  #   notifier.notification_name = Notify::NOTE_NEW_SMARTBITE_COMMENT

  #   user_config = build :notification_config
  #   user_config.set_enabled(Notify::NOTE_NEW_SMARTBITE_COMMENT, :email, :digest_daily)
  #   user_config.save

  #   org_config = build :notification_config
  #   org_config.save

  #   assert notifier.resolve_config(:email, :digest_daily, org_config, user_config)
  # end

  test '#resolve_config returns org config' do
    notifier = Notify::Notifier.new
    notifier.notification_name = Notify::NOTE_NEW_SMARTBITE_COMMENT

    user_config = nil

    org_config = build :notification_config
    org_config.set_enabled(Notify::NOTE_NEW_SMARTBITE_COMMENT, :email, :digest_daily)
    org_config.save

    assert notifier.resolve_config(:email, :digest_daily, org_config, user_config)
  end

  test '#resolve_config returns default config' do
    notifier = Notify::Notifier.new
    notifier.notification_name = Notify::NOTE_NEW_SMARTBITE_COMMENT
    notifier.set_enabled(:email, :digest_daily)
    notifier.set_default(:email, :digest_daily, true)

    user_config = nil
    org_config = nil

    assert notifier.resolve_config(:email, :digest_daily, org_config, user_config)
  end

  test '#get_users_with_settings' do
    # Create the service
    org = create :organization
    notifier = Notify::Notifier.new
    notifier.notification_name = Notify::NOTE_NEW_SMARTBITE_COMMENT
    notifier.set_enabled(:email, :single)
    notifier.set_enabled(:push, :single)

    # Create a user and notification preferences
    user = create :user, organization: org
    config = build :notification_config, user_id: user.id
    # config.set_enabled(Notify::NOTE_NEW_SMARTBITE_COMMENT, :web, :single)
    # config.set_enabled(Notify::NOTE_NEW_SMARTBITE_COMMENT, :email, :single)
    # config.set_enabled(Notify::NOTE_NEW_SMARTBITE_COMMENT, :push, :single)
    # config.save

    result = notifier.get_users_with_settings([user.id], org)
    expected_results = [{user_id: user.id,
                         configs: [
                           {medium: :email, interval: :single, enabled: false},
                           {medium: :email, interval: :digest_daily, enabled: false},
                           {medium: :email, interval: :digest_weekly, enabled: false},

                           {medium: :push, interval: :single, enabled: false},
                           {medium: :push, interval: :digest_daily, enabled: false},
                           {medium: :push, interval: :digest_weekly, enabled: false},
                           {medium: :web, interval: :single, enabled: false},
                         ]}]

    assert_equal 1, result.count
    assert_equal expected_results[0][:user_id], result[0][:user_id]
    assert_same_elements expected_results[0][:configs], result[0][:configs]
  end

  test '#users_to_notify' do
    notifier = Notify::Notifier.new

    assert_raise NotImplementedError do
      notifier.users_to_notify(nil, nil)
    end
  end

  test '#notification_params' do
    notifier = Notify::Notifier.new

    assert_raise NotImplementedError do
      notifier.notification_params(nil)
    end
  end
end

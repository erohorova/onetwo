require 'test_helper'

class Notify::LiveStreamInGroupNotifierTest < ActiveSupport::TestCase
  setup do
    @group_notifier = Notify::LiveStreamInGroupNotifier.new
  end

  test "#notification should not be sent to author of the card who initiates live stream" do
    org = create :organization
    user = create :user, organization: org
    team_user = create :user, organization: org
    team = create(:team, organization: org)
    team.add_user team_user
    card = create :card, organization: org, author_id: user.id
    team_card = create(:teams_card, team_id: team.id, card_id: card.id, type: 'SharedCard')

    video_stream = create(:wowza_video_stream, organization_id: org.id, creator: user, card: card)
    EDCAST_NOTIFY.trigger_event(Notify::EVENT_LIVE_STREAM_IN_GROUP, team_card)
    notification_params = @group_notifier.notification_params(NotificationEntry.last)[:user]

    assert_equal 1, NotificationEntry.count
    assert_not_equal NotificationEntry.last.user_id, user.id
  end

  test "#notify team_users when stream is posted to team" do
    org = create :organization
    user = create :user, organization: org
    team_user = create :user, organization: org
    team = create(:team, organization: org)
    team.add_user team_user
    card = create :card, organization: org, author_id: user.id
    team_card = create(:teams_card, team_id: team.id, card_id: card.id, type: 'SharedCard')

    video_stream = create(:wowza_video_stream, organization_id: org.id, creator: user, card: card)
    EDCAST_NOTIFY.trigger_event(Notify::EVENT_LIVE_STREAM_IN_GROUP, team_card)

    assert_equal 1, NotificationEntry.count
  end

  test "#notify user params" do
    org = create :organization
    user = create :user, organization: org
    team_user = create :user, organization: org
    team = create(:team, organization: org)
    team.add_user team_user
    card = create :card, organization: org, author_id: user.id
    team_card = create(:teams_card, team_id: team.id, card_id: card.id, type: 'SharedCard')

    video_stream = create(:wowza_video_stream, organization_id: org.id, creator: user, card: card)
    EDCAST_NOTIFY.trigger_event(Notify::EVENT_LIVE_STREAM_IN_GROUP, team_card)
    notification_params = @group_notifier.notification_params(NotificationEntry.last)[:user]

    assert_equal notification_params[:first_name], user.first_name
    assert_equal notification_params[:last_name], user.last_name
  end

  test "#notification should be sent once to the user if user is in team and is also the direct follower of card author" do
    org = create :organization
    user = create :user, organization: org
    team_user = create :user, organization: org
    team = create(:team, organization: org)
    team.add_user team_user

    create(:follow, user: team_user, followable: user)

    card = create :card, organization: org, author_id: user.id
    video_stream = create(:wowza_video_stream, organization_id: org.id, creator: user, card: card)

    EDCAST_NOTIFY.trigger_event(Notify::EVENT_NEW_LIVESTREAM, card)
    assert_equal 1, NotificationEntry.count
    
    team_card = create(:teams_card, team_id: team.id, card_id: card.id, type: 'SharedCard')
    assert_no_difference -> {NotificationEntry.count} do
      EDCAST_NOTIFY.trigger_event(Notify::EVENT_LIVE_STREAM_IN_GROUP, team_card)
    end
  end


  test "#notification when live stream is added to channel and group together" do
    org = create :organization
    user = create :user, organization: org
    
    follower_user = create :user, organization: org
    channel = create :channel, user: user, organization: org
    
    team = create(:team, organization: org)
    team.add_member follower_user
    
    # team.teams_channels_follows.create(channel: channel)
    channel.followers << follower_user
    
    card = create :card, organization: org, author_id: user.id
    video_stream = create(:wowza_video_stream, organization_id: org.id, creator: user, card: card)

    team_card = create(:teams_card, team_id: team.id, card_id: card.id, type: 'SharedCard')
    EDCAST_NOTIFY.trigger_event(Notify::EVENT_LIVE_STREAM_IN_GROUP, team_card)
    
    assert_equal 1, NotificationEntry.count
    assert_equal NotificationEntry.where(user_id: follower_user.id, event_name: Notify::EVENT_LIVE_STREAM_IN_GROUP).count, 1

    channel_card = create :channels_card, card_id: card.id, channel_id: channel.id
    
    assert_no_difference -> {NotificationEntry.count} do
      EDCAST_NOTIFY.trigger_event(Notify::EVENT_NEW_CONTENT_IN_CHANNEL, channel_card, {team_ids_notified: [team.id]})
    end
  end

end

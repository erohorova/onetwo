require 'test_helper'

class Notify::RemindedCuratorNotifierTest < ActiveSupport::TestCase
  setup do
    @notifier = Notify::RemindedCuratorNotifier.new
  end

  test 'remind curator of channel to curate content' do
    organization = create :organization
    user = create(:user, organization: organization)
    curator1 = create(:user, organization: organization)
    channel1 = create(:channel, curate_ugc: true, curate_only: true, organization: organization, created_at: 5.days.ago)
    create(:channels_user, user_id: user.id, channel_id: channel1.id)
    create(:channels_curator, user_id: curator1.id, channel_id: channel1.id)
    curator2 = create(:user, organization: organization)
    channel2 = create(:channel, curate_ugc: true, curate_only: true, organization: organization)
    create(:channels_curator, user_id: curator2.id, channel_id: channel2.id)
    card = create(:card, organization: organization, author_id: user.id, created_at: 5.days.ago)
    create(:channels_card, card_id: card.id, channel_id: channel1.id, created_at: 5.days.ago)
    batches = []
    channel_curators = ChannelsCurator.where(channel_id: [channel1.id, channel2.id]).pluck(:user_id)
    [curator1, curator2].each do |curator|
      if curator.channels_to_curate.present? #will check curation queue present or not for curator
        @notifier.users_to_notify(Notify::EVENT_REMIND_CURATOR, curator) { |x| batches << x }
      end
    end
    assert_equal 1, batches.count
    assert_equal [channel_curators.first], batches.first
  end
end
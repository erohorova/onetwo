require 'test_helper'

class Notify::NotifySettingsTest < ActiveSupport::TestCase
  setup do
    @org = create :organization
    @user = create :user, organization: @org

    @org_config = build :notification_config, organization: @org
    @org_config.save
    EDCAST_NOTIFY.notification_name_to_notifiers.values.each {|n| n.setup}
  end

  test '#get_settings' do
    user_config = build :notification_config, user: @user
    user_config.set_enabled(Notify::NOTE_NEW_SMARTBITE_COMMENT, :email, :single)
    user_config.set_disabled(Notify::NOTE_NEW_SMARTBITE_COMMENT, :push, :single)
    user_config.save

    expected = {
      "new_smartbite_comment" => {
        push: [
          {label: "Off", id: :off, selected: true},
          {label: "Realtime", id: :single, selected: false}
        ],
        email: [
          {label: "Off", id: :off, selected: true},
          {label: "Realtime", id: :single, selected: false},
          {label: "Daily", id: :digest_daily, selected: false}
        ],
        user_configurable: nil
      },
      "mention_in_comment" => {
        push: [
          {label: "Off", id: :off, selected: true},
          {label: "Realtime", id: :single, selected: false}
        ],
        email: [
          {label: "Off", id: :off, selected: true},
          {label: "Realtime", id: :single, selected: false},
          {label: "Daily", id: :digest_daily, selected: false}
        ],
        user_configurable: nil
        },
        "new_activity_commented_smartbite" => {
          push: [
            {label: "Off", id: :off, selected: true},
            {label: "Realtime", id: :single, selected: false}
          ],
          email: [
            {label: "Off", id: :off, selected: true},
            {label: "Realtime", id: :single, selected: false},
            {label: "Daily", id: :digest_daily, selected: false}
          ],
        user_configurable: nil
        },
        "new_livestream_from_followed_user" => {
          push: [
            {label: "Off", id: :off, selected: true},
            {label: "Realtime", id: :single, selected: false}
          ],
          email: [
            {label: "Off", id: :off, selected: true},
            {label: "Realtime", id: :single, selected: false}
            ],
        user_configurable: nil
          },
          "new_follower" => {
            push: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false}
            ],
            email: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false},
              {label: "Daily", id: :digest_daily, selected: false}
            ],
        user_configurable: nil
          },
          "assigned_content" => {
            push: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false}
            ],
            email: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false},
              {label: "Daily", id: :digest_daily, selected: false}
            ],
        user_configurable: nil
          },
          "new_content_by_followed_user" => {
            push: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false}
            ],
            email: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false},
              {label: "Daily", id: :digest_daily, selected: false}
            ],
        user_configurable: nil
          },
          "start_scheduledstream" => {
            :push => [
              {:label=>"Off", :id=>:off, :selected=>true},
              {:label=>"Realtime", :id=>:single, :selected=>false}
            ],
            :email => [
              {:label=>"Off", :id=>:off, :selected=>true},
              {:label=>"Realtime", :id=>:single, :selected=>false}
            ],
        :user_configurable => nil
          },
          "completed_assignment" => {
            push: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false},
              {label: "Daily", id: :digest_daily, selected: false}
            ],
            email: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false},
              {label: "Daily", id: :digest_daily, selected: false}
            ],
        user_configurable: nil
          },
          "added_curator" => {
            push: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false},
              {label: "Daily", id: :digest_daily, selected: false}
            ],
            email: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false},
              {label: "Daily", id: :digest_daily, selected: false}
            ],
        user_configurable: nil
          },
          "added_collaborator" => {
            push: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false},
              {label: "Daily", id: :digest_daily, selected: false}
            ],
            email: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false},
              {label: "Daily", id: :digest_daily, selected: false}
            ],
        user_configurable: nil
          },
          "added_channel_follower_admin" => {
            :push=>[
              {:label=>"Off", :id=>:off, :selected=>true}
            ],
            :email=>[
              {:label=>"Off", :id=>:off, :selected=>true},
              {:label=>"Realtime", :id=>:single, :selected=>false}
            ],
        user_configurable: nil
          },
          "curated_card" => {
            push: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false}
            ],
            email: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false},
              {label: "Daily", id: :digest_daily, selected: false}
            ],
        user_configurable: nil
          },
          "skipped_card" => {
            push: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false}
            ],
            email: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false},
              {label: "Daily", id: :digest_daily, selected: false}
            ],
        user_configurable: nil
          },
          "new_card_for_curation" => {
            push: [
              {label: "Off", id: :off, selected: true}
            ],
            email: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false},
              {label: "Daily", id: :digest_daily, selected: false}
            ],
        user_configurable: nil
          },
          "reminded_curator" => {
            push: [
              {label: "Off", id: :off, selected: true}
            ],
            email: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false}
            ],
        user_configurable: nil
          },
          "content_shared" => {
            push: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false}
            ],
            email: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false},
              {label: "Daily", id: :digest_daily, selected: false}
            ],
        user_configurable: nil
          },
          "new_content_in_channel" => {
            push: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false}
            ],
            email: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false},
            ],
          user_configurable: nil
          },
          "live_stream_in_group" => {
            push: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false}
            ],
            email: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false},
            ],
        user_configurable: nil
          }
        }


    output = EDCAST_NOTIFY.settings.get_settings(@org.id, @user.id)

    assert_equal expected, output[:options]
  end

  test '#save_settings' do
    # org = create :organization
    # user = create :user, organization: org

    user_config = create :notification_config, user: @user

    settings_to_save = {}
    settings_to_save[Notify::NOTE_NEW_SMARTBITE_COMMENT] = {
      push: [
        {label: 'Off', id: :off, selected: true},
        {label: 'Realtime', id: :single, selected: false},
        {label: 'Daily', id: :digest_daily, selected: false},
      ],
      email: [
        {label: 'Off', id: :off, selected: true},
        {label: 'Realtime', id: :single, selected: false},
        {label: 'Daily', id: :digest_daily, selected: false},
      ],
      user_configurable: nil
    }

    EDCAST_NOTIFY.settings.save_settings(user_config, settings_to_save)

    # Assertions
    user_config.reload
    assert_not user_config.enabled?(Notify::NOTE_NEW_SMARTBITE_COMMENT, :push, :single)
    assert_not user_config.enabled?(Notify::NOTE_NEW_SMARTBITE_COMMENT, :push, :digest_daily)
    assert_not user_config.enabled?(Notify::NOTE_NEW_SMARTBITE_COMMENT, :push, :digest_weekly)

    assert_not user_config.enabled?(Notify::NOTE_NEW_SMARTBITE_COMMENT, :email, :single)
    assert_not user_config.enabled?(Notify::NOTE_NEW_SMARTBITE_COMMENT, :email, :digest_daily)
    assert_not user_config.enabled?(Notify::NOTE_NEW_SMARTBITE_COMMENT, :email, :digest_weekly)

    # Assertions for output
    # expected = {}
    # expected[Notify::NOTE_NEW_SMARTBITE_COMMENT] = {
    #   push: [
    #     {label: 'Off', id: :off, selected: true},
    #     {label: 'Realtime', id: :single, selected: false},
    #     {label: 'Daily', id: :digest_daily, selected: false},
    #   ],
    #   email: [
    #     {label: 'Off', id: :off, selected: false},
    #     {label: 'Realtime', id: :single, selected: false},
    #     {label: 'Daily', id: :digest_daily, selected: true},
    #   ]
    # }


    expected = {"new_smartbite_comment" => {
      push: [
        {label: "Off", id: :off, selected: true},
        {label: "Realtime", id: :single, selected: false}
      ],
      email: [
        {label: "Off", id: :off, selected: true},
        {label: "Realtime", id: :single, selected: false},
        {label: "Daily", id: :digest_daily, selected: false}
      ],
      user_configurable: nil
    },
    "mention_in_comment" => {
      push: [
        {label: "Off", id: :off, selected: true},
        {label: "Realtime", id: :single, selected: false}
      ],
      email: [
        {label: "Off", id: :off, selected: true},
        {label: "Realtime", id: :single, selected: false},
        {label: "Daily", id: :digest_daily, selected: false}
      ],
      user_configurable: nil
      },
      "new_activity_commented_smartbite" => {
        push: [
          {label: "Off", id: :off, selected: true},
          {label: "Realtime", id: :single, selected: false}
        ],
        email: [
          {label: "Off", id: :off, selected: true},
          {label: "Realtime", id: :single, selected: false},
          {label: "Daily", id: :digest_daily, selected: false}
        ],
      user_configurable: nil
      },
      "new_livestream_from_followed_user" => {
        push: [
          {label: "Off", id: :off, selected: true},
          {label: "Realtime", id: :single, selected: false}
        ],
        email: [
          {label: "Off", id: :off, selected: true},
          {label: "Realtime", id: :single, selected: false}
          ],
      user_configurable: nil
        },
        "new_follower" => {
          push: [
            {label: "Off", id: :off, selected: true},
            {label: "Realtime", id: :single, selected: false}
          ],
          email: [
            {label: "Off", id: :off, selected: true},
            {label: "Realtime", id: :single, selected: false},
            {label: "Daily", id: :digest_daily, selected: false}
          ],
      user_configurable: nil
        },
        "assigned_content" => {
          push: [
            {label: "Off", id: :off, selected: true},
            {label: "Realtime", id: :single, selected: false}
          ],
          email: [
            {label: "Off", id: :off, selected: true},
            {label: "Realtime", id: :single, selected: false},
            {label: "Daily", id: :digest_daily, selected: false}
          ],
      user_configurable: nil
        },
        "new_content_by_followed_user" => {
          push: [
            {label: "Off", id: :off, selected: true},
            {label: "Realtime", id: :single, selected: false}
          ],
          email: [
            {label: "Off", id: :off, selected: true},
            {label: "Realtime", id: :single, selected: false},
            {label: "Daily", id: :digest_daily, selected: false}
          ],
      user_configurable: nil
        },
        "start_scheduledstream" => {
          :push => [
            {:label=>"Off", :id=>:off, :selected=>true},
            {:label=>"Realtime", :id=>:single, :selected=>false}
          ],
          :email => [
            {:label=>"Off", :id=>:off, :selected=>true},
            {:label=>"Realtime", :id=>:single, :selected=>false}
          ],
      :user_configurable => nil
        },
        "completed_assignment" => {
          push: [
            {label: "Off", id: :off, selected: true},
            {label: "Realtime", id: :single, selected: false},
            {label: "Daily", id: :digest_daily, selected: false}
          ],
          email: [
            {label: "Off", id: :off, selected: true},
            {label: "Realtime", id: :single, selected: false},
            {label: "Daily", id: :digest_daily, selected: false}
          ],
      user_configurable: nil
        },
        "added_curator" => {
          push: [
            {label: "Off", id: :off, selected: true},
            {label: "Realtime", id: :single, selected: false},
            {label: "Daily", id: :digest_daily, selected: false}
          ],
          email: [
            {label: "Off", id: :off, selected: true},
            {label: "Realtime", id: :single, selected: false},
            {label: "Daily", id: :digest_daily, selected: false}
          ],
      user_configurable: nil
        },
        "added_collaborator" => {
          push: [
            {label: "Off", id: :off, selected: true},
            {label: "Realtime", id: :single, selected: false},
            {label: "Daily", id: :digest_daily, selected: false}
          ],
          email: [
            {label: "Off", id: :off, selected: true},
            {label: "Realtime", id: :single, selected: false},
            {label: "Daily", id: :digest_daily, selected: false}
          ],
      user_configurable: nil
        },
        "added_channel_follower_admin" => {
          :push => [
            {:label=>"Off", :id=>:off, :selected=>true}
          ],
          :email => [
            {:label=>"Off", :id=>:off, :selected=>true},
            {:label=>"Realtime", :id=>:single, :selected=>false}
          ],
      user_configurable: nil
        },
          "curated_card" => {
            push: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false}
            ],
            email: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false},
              {label: "Daily", id: :digest_daily, selected: false}
            ],
      user_configurable: nil
          },
          "skipped_card" => {
            push: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false}
            ],
            email: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false},
              {label: "Daily", id: :digest_daily, selected: false}
            ],
      user_configurable: nil
          },
          "new_card_for_curation" => {
            push: [
              {label: "Off", id: :off, selected: true}
            ],
            email: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false},
              {label: "Daily", id: :digest_daily, selected: false}
            ],
      user_configurable: nil
          },
          "reminded_curator" => {
            push: [
              {label: "Off", id: :off, selected: true}
            ],
            email: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false}
            ],
      user_configurable: nil
          },
          "content_shared" => {
            push: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false}
            ],
            email: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false},
              {label: "Daily", id: :digest_daily, selected: false}
            ],
      user_configurable: nil
          },
          "new_content_in_channel" => {
            push: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false}
            ],
            email: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false},
            ],
      user_configurable: nil
          },
          "live_stream_in_group" => {
            push: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false}
            ],
            email: [
              {label: "Off", id: :off, selected: true},
              {label: "Realtime", id: :single, selected: false},
            ],
        user_configurable: nil
          }
      }

    output = EDCAST_NOTIFY.settings.get_settings(@org.id, @user.id)

    assert_equal expected, output[:options]
  end
end

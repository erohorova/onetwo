require 'test_helper'

class Notify::StartScheduledstreamNotifierTest < ActiveSupport::TestCase
  setup do
    @notifier = Notify::StartScheduledstreamNotifier.new
  end

  test '#notify creator of scheduled_stream' do
    org = create :organization
    scheduledstream_author = create :user, organization: org

    scheduledstream = create :video_stream_card, author_id: scheduledstream_author.id

    batches = []
    @notifier.users_to_notify(Notify::EVENT_TIME_TO_START_SCHEDULEDSTREAM, scheduledstream) { |x| batches << x }

    assert_equal 1, batches.count
    assert_same_elements [scheduledstream_author.id], batches[0]
  end
end
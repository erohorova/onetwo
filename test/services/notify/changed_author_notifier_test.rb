require 'test_helper'

class Notify::ChangedAuthorNotifierTest < ActiveSupport::TestCase
  setup do
    @org = create(:organization)
    @logged_in_user = create :user, organization: @org
    @old_author = create :user, organization: @org
    @new_author = create :user, organization: @org
    @card = create :card, author: @old_author, organization: @org, title: "Hello card"
  end

  test '#generate web notification for new author when a @card author is changed' do
    @card.current_user = @logged_in_user
    @card.update(author_id: @new_author.id)
    TransferOwnershipJob.perform_now(@card.id, {old_author_id: @old_author.id})

    notification = Notification.where(notification_type: 'changed_author')
    message = "Hello card Ownership has been transferred to you."

    assert_equal 1, notification.count
    assert_equal @card.author_id, notification.first.user_id
    assert_equal message, notification.first.custom_message
  end
end
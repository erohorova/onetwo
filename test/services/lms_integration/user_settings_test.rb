require 'test_helper'
class LmsIntegration::UserSettingsTest < ActiveSupport::TestCase

  setup do
    @organization = create :organization
    @user = create :user, organization: @organization

    @saml_provider = create :identity_provider, :saml, user: @user, auth_info: {user_id: "test_01" }
    @custom_field = create :custom_field, abbreviation: "userid", display_name: "user_id"
    @user_custom_field = create :user_custom_field, user_id: @user.id, custom_field_id: @custom_field.id, value: "7500177"
  end

  test "sync_user_settings" do

    stub_request(:post, "http://test.localhost/api/v1/user_settings").
      with(:body => {
        "name"=> @user.name,
        "email"=> @user.email,
        "organization_id"=> @organization.id,
        "organization_host_name" => @organization.host_name,
        "user_id"=> @user.id,
        "additional_info"=> {
          "providers" => {
            "SamlProvider"=>{
              "uid"=> @saml_provider.uid,
              "token"=>@saml_provider.token,
              "secret"=>@saml_provider.secret,
              "auth_info"=> @saml_provider.auth_info }
          },
          "custom_fields" => {
            @custom_field.abbreviation => @user_custom_field.value
          }
        }} ).
      to_return(:status => 200, :body => "", :headers => {})

    LmsIntegration::UserSettings.new(@user.id).setup_user
  end

end

require 'test_helper'
class LmsIntegration::XapiCredentialTest < ActiveSupport::TestCase

  setup do
    @organization = create :organization
    @xapi_credential = create :xapi_credential, organization: @organization
  end

  test "sync_xapi_credentials" do
    stub_request(:post, "http://test.localhost/api/v1/sources/sync_xapi_credentials").
      with(:body => {
        "organization_id"  => @xapi_credential.organization_id,
        "lrs_end_point"    => @xapi_credential.end_point,
        "lrs_login_key"    => @xapi_credential.lrs_login_key,
        "lrs_password_key" => @xapi_credential.lrs_password_key,
        "lrs_api_version"  => @xapi_credential.lrs_api_version
        }).
      to_return(:status => 200, :body => "", :headers => {})

    LmsIntegration::XapiCredential.new(@xapi_credential.id).sync_xapi_credentials
  end
end

require 'test_helper'

class ApiTest < ActionDispatch::IntegrationTest
  #feed
  test 'api feed routes' do
    assert_routing({path: 'api/feed_promos/channels',  method: 'get' }, controller: 'api/feed_promotions', action: 'channels')
    assert_routing({path: 'api/feed_promos/courses',   method: 'get' }, controller: 'api/feed_promotions', action: 'courses')
  end

  #suggestions
  test 'api suggestions routes' do
    skip # NOT WORKING, SHOULD ADDRESS
    assert_routing({path: 'api/suggest/initial_state',   method: 'get' }, controller: 'api/suggestions',  action: 'initial_state')
    assert_routing({path: 'api/suggest',   method: 'get' }, controller: 'api/suggestions',  action: 'suggest')
    assert_routing({path: 'api/suggest_interests',   method: 'get' }, controller: 'api/suggestions',  action: 'suggest_interests')
    assert_routing({path: 'api/suggest_tags',   method: 'get' }, controller: 'api/suggestions',  action: 'suggest_tags')
  end

  # card
  test 'api cards routes' do
    # comments: card
    assert_routing({ path: 'api/cards/1/comments', method: 'post' }, controller: 'comments', action: 'create', card_id: '1')
    assert_routing({ path: 'api/cards/1/comments', method: 'get'  }, controller: 'comments', action: 'index',  card_id: '1')
    # Assert routing is not working with routes generated for both
    # namespace and without namespace urls for same controller
    # using assert_recognizes
    assert_recognizes({controller: 'comments', action: 'destroy', card_id: '1', id: '2'}, {path: 'api/cards/1/comments/2', method: 'delete'})
  end

  # user
  test 'api users routes' do
    assert_routing({path: 'api/users',                method: 'get' }, controller: 'api/users', action: 'index')
    assert_routing({path: 'api/users/1/follow',   method: 'post' }, controller: 'follows',  action: 'create', user_id: "1")
    assert_routing({path: 'api/users/1/unfollow', method: 'post' }, controller: 'follows',  action: 'destroy', user_id: "1")
    assert_routing({path: 'api/users/1/change_role',  method: 'put' }, controller: 'api/users', action: 'toggle_role', user_id: '1')

    assert_routing({path: 'api/users/groups',         method: 'get' }, controller: 'users', action: 'groups')
    assert_routing({path: 'api/users/info',           method: 'get' }, controller: 'users', action: 'info')
    assert_routing({path: 'api/users/info/following', method: 'get' }, controller: 'users', action: 'following_users')
    assert_routing({path: 'api/users/1/basic_info',   method: 'get' }, controller: 'users', action: 'basic_info', id: '1')
    assert_routing({path: 'api/users/1/cards',        method: 'get' }, controller: 'users', action: 'cards', id: '1')
    assert_routing({path: 'api/users/1/streams',      method: 'get' }, controller: 'users', action: 'streams', id: '1')
    assert_routing({path: 'api/users/confirmed',      method: 'post'}, controller: 'users', action: 'confirmed')
    assert_routing({path: 'api/users/set_config',     method: 'post'}, controller: 'users', action: 'set_config')
    assert_routing({path: 'api/user/1/followers',     method: 'get' }, controller: 'users', action: 'followers', id: '1')

    assert_recognizes({controller: 'users', action: 'suggested_users'}, {path: 'api/suggested_users',   method: 'get'})
    assert_recognizes({controller: 'users', action: 'suggested_users'}, {path: 'api/users/recommended', method: 'get'})
  end

  # post
  test 'api posts routes' do
    # comments: post
    assert_recognizes({controller: 'comments', action: 'create', post_id: '1'}, {path: 'api/posts/1/comments', method: 'post'})
    assert_recognizes({controller: 'comments', action: 'create', post_id: '1'}, {path: 'api/posts/1/comments', method: 'post'})

    assert_routing({path: 'api/posts/1/follow',   method: 'post' }, controller: 'follows',  action: 'create', post_id: "1")
    assert_routing({path: 'api/posts/1/unfollow', method: 'post' }, controller: 'follows',  action: 'destroy', post_id: "1")
  end

  # courses
  test 'api courses data' do
    assert_routing({path: 'api/courses', method: 'get'}, controller: 'api/courses', action: 'index')
    assert_routing({path: 'api/courses/1/enroll', method: 'post'}, controller: 'api/courses', action: 'enroll', id: '1')
  end

  # api webex
  test 'api webex routes' do
    assert_routing({path: 'api/webex_configs', method: 'post'}, controller: 'api/webex_configs', action: 'create')
    assert_routing({path: 'api/webex_configs/get_webex_credentials', method: 'get'}, controller: 'api/webex_configs', action: 'get_webex_credentials')
  end

  # user interests
  test 'api user interesets' do
    assert_routing({path: 'api/user_interests', method: 'post'}, controller: 'api/user_interests', action: 'create')
    assert_routing({path: 'api/user_interests', method: 'get'}, controller: 'api/user_interests', action: 'index')
    assert_routing({path: 'api/user_interests/suggested', method: 'get'}, controller: 'api/user_interests', action: 'suggested')
    assert_routing({path: 'api/user_interests', method: 'delete'}, controller: 'api/user_interests', action: 'destroy')
  end

  # org settings
  test 'api org settings' do
    assert_routing({path: 'api/org_details', method: 'get'}, controller: 'api/organizations', action: 'show')
    assert_routing({path: 'api/org_settings', method: 'put'}, controller: 'api/organizations', action: 'set_org_settings')
    assert_routing({path: 'api/org_settings', method: 'get'}, controller: 'api/organizations', action: 'read_org_settings')

    assert_routing({path: 'api/organizations/host_name_check', method: 'get'}, controller: 'api/organizations', action: 'host_name_check')
    assert_routing({path: 'api/organizations/settings', method: 'get'}, controller: 'api/organizations', action: 'settings')
    assert_routing({path: 'api/organizations/settings', method: 'put'}, controller: 'api/organizations', action: 'update')

    assert_routing({path: 'api/organizations/activate', method: 'post'}, controller: 'api/organizations', action: 'activate_org')
  end

  # teams routes
  test 'api teams' do
    assert_routing({path: 'api/teams', method: 'get'}, controller: 'api/teams', action: 'index')
    assert_routing({path: 'api/teams/assigned_groups', method: 'get'}, controller: 'api/teams', action: 'assigned_groups')
    assert_routing({path: 'api/teams/1/assignments', method: 'get'}, controller: 'api/teams', action: 'assignments', id: "1")
    assert_routing({path: 'api/teams/1/users/2/action', method: 'patch'}, controller: 'api/teams', action: 'user_action', id: "1", user_id: '2')
    assert_routing({path: 'api/teams/1/users/2/action', method: 'delete'}, controller: 'api/teams', action: 'user_action', id: "1", user_id: '2')
    assert_routing({path: 'api/teams/1/users', method: 'get'}, controller: 'api/teams', action: 'team_users', id: "1")
    assert_routing({path: 'api/teams/1', method: 'get'}, controller: 'api/teams', action: 'show', id: "1")
  end

  # other routes
  test 'api all other routes' do
    assert_routing({path: 'api/s3_uploads/configuration', method: 'get'}, controller: 'api/s3_uploads', action: 'configuration')

    assert_routing({path: 'api/groups/1/course_data', method: 'get' }, controller: 'groups',          action: 'course_data', id: '1')
    assert_routing({path: 'api/notifications',        method: 'get' }, format: 'json', controller: 'notifications', action: 'index')
  end
end

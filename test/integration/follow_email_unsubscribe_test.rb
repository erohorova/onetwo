require 'test_helper'

class FollowEmailUnsubscribeTest < ActionDispatch::IntegrationTest
  self.use_transactional_fixtures = false

  setup do
    FollowEmailNotificationGeneratorJob.unstub :perform_later
    NotificationGeneratorJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    # WebMock.disable!
    create(:batch_job, :follow_email)
    # UserContentsQueue.create_index! force: true
    # UserContentsQueue.gateway.refresh_index!
    Organization.any_instance.stubs(:notifications_and_triggers_enabled?).returns(false)
  end

  teardown do
    # WebMock.enable!
    Organization.any_instance.unstub(:notifications_and_triggers_enabled?)
  end

  test 'follow unsubscribe integration test' do
    now = Time.now.utc
    u = create(:user)
    followers = create_list(:user, 2, organization: u.organization)

    followers.each do |f|
      create(:follow, user: f, followable: u, created_at: now - 1.hour)
    end

    unsubscribe_url = ''
    MandrillMailer.any_instance.expects(:send_api_email).with(){|message, async, ipool, send_at, args|
      unsubscribe_url = message[:message][:global_merge_vars][15]['content']
      }.at_least(1)
    FollowBatchEmailGenerator.perform

    unsubscribe_url = unsubscribe_url

    assert_difference ->{UserPreference.count} do
      get unsubscribe_url
    end

    more_followers = create_list(:user, 2, organization: u.organization)
    now = Time.now.utc
    Time.stubs(:now).returns(now + 4.hours)

    more_followers.each do |f|
      create(:follow, user: f, followable: u, created_at: now)
    end

    FollowBatchEmailGenerator.perform
  end

end

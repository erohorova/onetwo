require 'test_helper'
class SignInIntegrationTest < ActionDispatch::IntegrationTest
  setup do
    Organization.any_instance.stubs(:show_onboarding_on_admin_invite_if_applicable)

    @organization = create(:organization)
    @admin = create(:user, organization: @organization, organization_role: 'admin')
    @user  = create(:user, organization: @organization, email: 'x-men@wolverine.com')
    @headers = { 'Content-Type' => 'application/json', 'Accept' => 'application/json', 'X-Edcast-JWT' => 1 }
    @sso = create(:sso, name: 'linkedin')
    @org_sso = create(:org_sso, organization: @organization, sso: @sso, position: 0)

    stub_request(:get, "https://api.linkedin.com/v1/people/~:(id,email-address,first-name,last-name,headline,industry,picture-url;secure=true,location,public-profile-url)?format=json&oauth2_access_token=xxx123").
      with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>/.*/}).
      to_return(:status => 200,
        :body => {
          "emailAddress" => "x-men@wolverine.com",
          "firstName" =>  "X-Men",
          "headline" =>  "Machine Learning Engineer at Diffbot",
          "id" =>  "YXEgmZb0iw",
          "industry" =>  "Computer Software",
          "lastName" =>  "Wolverine",
          "location" =>  {
            "country" =>  {"code" =>  "us"},
            "name" =>  "San Francisco Bay Area"
          },
          "pictureUrl" => "https://media.licdn.com/mpr/mprx/0_yHCbiqdc_gXu5pTZymPZinWJixT85VhZOfn4in4dpyvKsYc4ratwS9uFDc3x6j3NgI8M2AE4AeyE",
          "publicProfileUrl" =>  "http://www.linkedin.com/pub/bharath-bhat/15/500/329"
        }.to_json,
        :headers => {"Content-Type" => "application/json"})
    Search::UserSearch.any_instance.stubs(:search_by_id)
  end

  test 'should restrict access to any API after sign out using same JWT token' do
    card = create(:card, organization: @organization, author: @user)
    post "#{@organization.home_page}/auth/users/sign_in", user: { email: @user.email, password: @user.password }
    jwt_token = @user.jwt_token
    get "#{@organization.home_page}/api/v2/cards", {}, @headers.merge!('X-API-TOKEN' => jwt_token)
    resp = get_json_response(response)
    assert_response :ok
    assert_equal [card.id], resp['cards'].map {|card| card['id'].to_i}

    # Sign Out
    get "#{@organization.home_page}/sign_out"
    get "#{@organization.home_page}/api/v2/cards", {}, @headers.merge!('X-API-TOKEN' => jwt_token)
    resp = get_json_response(response)
    assert_response :unauthorized
    assert_equal "JWTToken is expired or invalid", resp['message']
  end

  test 'active users must be able to log in' do
    assert_equal 'active', @user.status
    assert @user.is_active?
    refute @user.is_suspended?

    host! @organization.host
    post '/auth/linkedin_access_token/callback', {access_token: 'xxx123', provider: :linkedin}, {'User-Agent' => 'EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0', 'X-Build-Number' => '42'}

    assert_response :success
  end

  test 'inactive users should be able to log in' do
    assert_equal 'active', @user.status
    assert @user.is_active?
    refute @user.is_suspended?

    host! @organization.host

    # Step 1: Mark the user as `inactive`.
    post "/api/v2/users/#{@user.id}/user_status.json", { status: 'inactive' }, { 'X-API-TOKEN' => @admin.jwt_token}
    assert_response :success

    @user.reload
    assert_equal 'inactive', @user.status
    assert @user.is_active?
    refute @user.is_suspended?

    # Step 2: User still should be able to login.
    post '/auth/linkedin_access_token/callback', {access_token: 'xxx123', provider: :linkedin}, {'User-Agent' => 'EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0', 'X-Build-Number' => '42'}
    assert_response :success
  end

  test 'suspended users should not be able to log in' do
    assert_equal 'active', @user.status
    assert @user.is_active?
    refute @user.is_suspended?

    host! @organization.host

    # Step 1: Mark the user as `suspended`.
    post "/api/v2/users/#{@user.id}/user_status.json", { status: 'suspended' }, { 'X-API-TOKEN' => @admin.jwt_token}
    assert_response :success

    @user.reload
    assert_equal 'suspended', @user.status
    refute @user.is_active?
    assert @user.is_suspended?

    # Step 2: User should not be able to login.
    post '/auth/linkedin_access_token/callback', {access_token: 'xxx123', provider: :linkedin}, {'User-Agent' => 'EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0', 'X-Build-Number' => '42'}

    assert_response :forbidden
    assert_match /Invalid sign on credentials/, response.body
  end

  test 'deleted users should not be able to log in' do
    assert_equal 'active', @user.status
    assert @user.is_active?
    refute @user.is_suspended?

    host! @organization.host

    # Step 1: Delete this user. This user is marked as suspended though.
    post "/api/v2/users/#{@user.id}/admin_delete_account.json", {}, { 'X-API-TOKEN' => @admin.jwt_token}
    assert_response :success

    @user.reload
    assert_equal 'suspended', @user.status
    refute @user.is_active?
    assert @user.is_suspended?

    # Step 2: User should not be able to login.
    post '/auth/linkedin_access_token/callback', {access_token: 'xxx123', provider: :linkedin}, {'User-Agent' => 'EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0', 'X-Build-Number' => '42'}

    assert_response :forbidden
    assert_match /Invalid sign on credentials/, response.body
  end

  test 'reactivated deleted users should be able to log in' do
    assert_equal 'active', @user.status
    assert @user.is_active?
    refute @user.is_suspended?

    host! @organization.host

    # Step 1: Delete the user being an admin.
    post "/api/v2/users/#{@user.id}/admin_delete_account.json", {}, { 'X-API-TOKEN' => @admin.jwt_token}
    assert_response :success

    @user.reload
    # Step 2: Mark the user as `active`.
    post "/api/v2/users/#{@user.id}/user_status.json", { status: 'active' }, { 'X-API-TOKEN' => @admin.jwt_token}
    assert_response :success

    # Step 3: User should be able to login.
    post '/auth/linkedin_access_token/callback', {access_token: 'xxx123', provider: :linkedin}, {'User-Agent' => 'EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0', 'X-Build-Number' => '42'}

    assert_response :success
  end
end

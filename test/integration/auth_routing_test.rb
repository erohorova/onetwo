require 'test_helper'

class AuthRoutingTest < ActionDispatch::IntegrationTest

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'

    stub_request(:get, "#{Settings.edcast_wordpress_url}/corp/").
      with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.9.2'}).
      to_return(:status => 200, :body => "", :headers => {})
    stub_ip_lookup(ip_address: "127.0.0.1")
  end

  test 'redirect user on default org to homepage on sign out' do
    create_default_org
    org = Organization.default_org
    user = create(:user, organization: org)

    open_session do |sess|
      Promotion.stubs(:promotions_for).returns([])
      UserContentsQueue.stubs(:get_queue_for_user).returns([])
      UserContentsQueue.stubs(:feed_content_for).returns([[], 'feed-request-id'])
      sess.post '/auth/users/sign_in', user: {email: user.email, password: user.password}
      sess.get '/sign_out'
      sess.assert_redirected_to '/'
      Promotion.unstub(:promotions_for)
      UserContentsQueue.unstub(:get_queue_for_user)
      UserContentsQueue.unstub(:feed_content_for)
    end
  end

  test 'redirect user on customer org to sign in page when he signs out' do
    org = create(:organization)
    user = create(:user, organization: org)

    open_session do |sess|
      Promotion.stubs(:promotions_for).returns([])
      UserContentsQueue.stubs(:get_queue_for_user).returns([])
      UserContentsQueue.stubs(:feed_content_for).returns([[], 'feed-request-id'])
      sess.post "#{org.home_page}/auth/users/sign_in", user: {email: user.email, password: user.password}
      sess.get "#{org.home_page}/sign_out"
      assert_redirected_to new_user_session_path
      Promotion.unstub(:promotions_for)
      UserContentsQueue.unstub(:get_queue_for_user)
      UserContentsQueue.unstub(:feed_content_for)
    end


    ENV['USE_EDCAST_DOMAIN'] = @old_use_edcast_domain
  end

  test "sign out user after 24 hours" do
    org =  create(:organization)
    user =  create(:user,organization_id: org.id)
    open_session do |sess|
      sess.post "#{org.home_page}/auth/users/sign_in", user: {email: user.email, password: user.password}
      Timecop.freeze(Date.today + 24.hours) do
        sess.get "#{org.home_page}"
        assert_redirected_to new_user_session_path({from_root: true})
      end
    end
  end

end

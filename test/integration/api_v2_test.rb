require 'test_helper'

class ApiV2Test < ActionDispatch::IntegrationTest
  test 'api roles routes' do
    assert_routing({path: 'api/v2/roles/permission',   method: 'put' }, controller: 'api/v2/roles', action: 'role_permission')
  end

  test 'Project card creation and deletion' do
    organization = create :organization
    user = create :user, organization: organization
    headers = { 'Content-Type' => 'application/json', 'Accept' => 'application/json', 'X-Edcast-JWT' => 1}
    assert_difference ['Card.count', 'Project.count'] do
      post "#{organization.home_page}/api/v2/cards", {card: {message: 'Start writing here', users_with_access_ids: nil,
       channel_ids: [], team_ids: nil, is_public: true, prices_attributes: nil,
       title: 'Project name', project_attributes: {reviewer_id: ''}, card_type: 'project',
       topics: [], duration: 0, hidden: true}}.to_json, headers.merge!('X-API-TOKEN' => user.jwt_token)
     end

    card =  OpenStruct.new get_json_response(response)
    assert_nil card.deleted_at

    delete "#{organization.home_page}/api/v2/cards/#{card.id}", {}, headers.merge!('X-API-TOKEN' => user.jwt_token)
    assert_equal Project.count, 0
    assert_equal Card.count, 0
  end
end
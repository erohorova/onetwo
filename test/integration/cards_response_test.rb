require 'test_helper'

class CardsResponseTest < ActionDispatch::IntegrationTest

  def html_string
    string = 'This is a sample string'
    string += '<body><a style="left:0;position:absolute;" href="http://www.google.com">Happy?</a></body>'
    string += '<script="text/javascript">alert("Hello")</script>'
    string += 'End of string'

    string
  end

  setup do
    skip
    @organization = create :organization
    @admin = create :user, organization: @organization, organization_role: 'admin'
    @token = @admin.jwt_token
    @organization.reload

    @headers = { 'Content-Type' => 'application/json', 'Accept' => 'application/json', 'X-Edcast-JWT' => 1, 'X-API-TOKEN' => @token }
    WebMock.allow_net_connect!
  end

  # teardown do
  #   WebMock.disable_net_connect!
  # end

  test 'Cards v2 #index strips all html tags' do
    resource = create :resource, user_id: @admin.id, description: html_string
    card1 = create :public_media_card, author_id: @admin.id, organization_id: @organization.id, title: html_string, resource_id: resource.id
    card2 = create :card, author_id: @admin.id, organization_id: @organization.id

    open_session do |sess|
      sess.get "#{@organization.home_page}/api/v2/cards", { offset: 0 }, @headers

      cards = get_json_response(response)['cards']

      card1_response = cards.select { |c| c['id'] == card1.id.to_s }.first
      card2_response = cards.select { |c| c['id'] == card2.id.to_s }.first
      assert_equal "This is a sample string<a href=\"http://www.google.com\">Happy?</a>alert(\"Hello\")End of string", card1_response['title']
      assert_equal "This is a sample string<a href=\"http://www.google.com\">Happy?</a>alert(\"Hello\")End of string", card1_response['resource']['description']

      assert_nil card2_response['title']
      refute card2_response['resource']
    end
  end

  test 'Cards V2 #show strips all html tags' do
    resource = create :resource, user_id: @admin.id, description: html_string
    card = create :public_media_card, author_id: @admin.id, organization_id: @organization.id, title: html_string, resource_id: resource.id

    open_session do |sess|
      sess.get "#{@organization.home_page}/api/v2/cards/#{card.id}", {}, @headers

      card_response = get_json_response(response)

      assert_equal "This is a sample string<a href=\"http://www.google.com\">Happy?</a>alert(\"Hello\")End of string", card_response['title']
      assert_equal "This is a sample string<a href=\"http://www.google.com\">Happy?</a>alert(\"Hello\")End of string", card_response['resource']['description']
    end
  end

  test 'Assignments V2 #index strips all html tags' do
    resource = create :resource, user_id: @admin.id, description: html_string
    card     = create :public_media_card, author_id: @admin.id, organization_id: @organization.id, title: html_string, resource_id: resource.id
    assignee = create :user, organization: @organization, organization_role: 'member'
    token    = assignee.jwt_token

    assignment1 = create(:assignment, assignable: card, assignee: assignee, state: Assignment::COMPLETED)

    open_session do |sess|
      sess.get "#{@organization.home_page}/api/v2/assignments", {}, @headers.merge!('X-API-TOKEN' => assignee.jwt_token)

      cards = get_json_response(response)['assignments'].map { |a| a['assignable'] }
      assignee_card = cards.select { |c| c['id'] == card.id.to_s }.first

      assert_equal "This is a sample string<a href=\"http://www.google.com\">Happy?</a>alert(\"Hello\")End of string", assignee_card['title']
      assert_equal "This is a sample string<a href=\"http://www.google.com\">Happy?</a>alert(\"Hello\")End of string", assignee_card['resource']['description']
    end
  end

  test 'Feed V2 #index strips all html tags' do
    resource = create :resource, user_id: @admin.id, description: html_string
    card     = create :public_media_card, author_id: @admin.id, organization_id: @organization.id, title: html_string, resource_id: resource.id

    expected_response = [{ 'rationale' => 'rat 1', 'item' => card }]

    UserContentsQueue.expects(:feed_content_for_v2).with(user: @admin, limit: 10, offset: 0).returns(expected_response).once

    open_session do |sess|
      sess.get "#{@organization.home_page}/api/v2/feed", {}, { "Content-Type" => "application/json", "Accept" => "application/json", "X-Edcast-JWT" => 1, "X-API-TOKEN" => @token }

      cards = get_json_response(response)['cards']
      feed_card = cards.select { |c| c['id'] == card.id.to_s }.first

      assert_equal "This is a sample string<a href=\"http://www.google.com\">Happy?</a>alert(\"Hello\")End of string", feed_card['title']
      assert_equal "This is a sample string<a href=\"http://www.google.com\">Happy?</a>alert(\"Hello\")End of string", feed_card['resource']['description']
    end
  end

  test 'Learning Queue Items V2 #index strips all html tags' do
    user = create :user, organization: @organization

    resource = create :resource, user_id: @admin.id, description: html_string
    card     = create :public_media_card, author_id: @admin.id, organization_id: @organization.id, title: html_string, resource_id: resource.id

    @user_queue = []
    @user_queue << create(:learning_queue_item, :from_assignments, queueable: card, user: user)

    open_session do |sess|
      sess.get "#{@organization.home_page}/api/v2/learning_queue_items", { include_queueable_details: true }, @headers.merge!('X-API-TOKEN' => user.jwt_token)

      cards = get_json_response(response)['learning_queue_items'].map { |lqi| lqi['queueable'] }

      lq_card = cards.select { |c| c['id'] == card.id.to_s }.first
      assert_equal "This is a sample string<a href=\"http://www.google.com\">Happy?</a>alert(\"Hello\")End of string", lq_card['title']
      assert_equal "This is a sample string<a href=\"http://www.google.com\">Happy?</a>alert(\"Hello\")End of string", lq_card['resource']['description']
    end
  end

  test 'Search V2 #index strips all html tags' do
    user = create :user, organization: @organization

    resource = create :resource, user_id: @admin.id, description: html_string
    card     = create :public_media_card, author_id: @admin.id, organization_id: @organization.id, title: html_string, resource_id: resource.id

    media_card = create :card, title: 'search card 1', card_type: 'media', card_subtype: 'link', organization: @organization, author_id: @admin.id
    poll_card  = create :card, title: 'search card 2', card_type: 'poll', card_subtype: 'basic', organization: @organization, author_id: @admin.id
    pack_card  = create :card, title: 'search card 3', card_type: 'pack', card_subtype: 'simple', organization: @organization, author_id: @admin.id

    cards = [card]

    query = "query"
    limit = 10
    offset = 0
    Search::UserSearch.any_instance.expects(:search).returns(Search::UserSearchResponse.new(results: [], total: 20)).once
    Search::ChannelSearch.any_instance.expects(:search).returns(Search::ChannelSearchResponse.new(results: [], total: 10)).once

    ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items"]
    ecl_search = mock()
    EclApi::EclSearch.expects(:new).with(@organization.id, user.id).returns(ecl_search).once
    ecl_search.expects(:search).returns(OpenStruct.new(results: cards, aggregations: ecl_response['aggregations'])).once

    open_session do |sess|
      sess.get "#{@organization.home_page}/api/v2/search", { q: 'search' }, @headers.merge!('X-API-TOKEN' => user.jwt_token)

      cards = get_json_response(response)['cards']

      search_card = cards.select { |c| c['id'] == card.id.to_s }.first
      assert_equal "This is a sample string<a href=\"http://www.google.com\">Happy?</a>alert(\"Hello\")End of string", search_card['title']
      assert_equal "This is a sample string<a href=\"http://www.google.com\">Happy?</a>alert(\"Hello\")End of string", search_card['resource']['description']
    end
  end

  test 'Search V2 #index must include `resource_group` as a course detail' do
    user = create :user, organization: @organization

    # 1. Create few groups/courses
    resource_group_1 = create :resource_group, organization: @organization, client_resource_id: '123'
    resource_group_2 = create :resource_group, organization: @organization, client_resource_id: '456'
    resource_group_3 = create :resource_group, organization: @organization, client_resource_id: '789'
    ecl_data = {
      "source_id" => "a36643fb-ed6c-443b-b473-5362c1cacf18",
      "source_display_name" => "Marketing Land - Internet Marketing News, Strategies & Tips",
      "source_type_name" => "edcast_cloud",
      "duration_metadata" => nil
    }

    # 2. Prepare ECL cards with resource group
    ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items"]
    ecl_content_1 = ecl_response['data'][0].merge!('external_id' => resource_group_1.client_resource_id.to_s, 'source_type_name' => 'edcast_cloud')
    ecl_content_2 = ecl_response['data'][1].merge!('external_id' => resource_group_2.client_resource_id.to_s, 'source_type_name' => 'edcast_cloud')
    ecl_content_3 = ecl_response['data'][2].merge!('external_id' => resource_group_3.client_resource_id.to_s, 'source_type_name' => 'edcast_cloud')

    # 3. Link physical (but ECL) cards to resource_groups
    ecl_card_1 = create :card, organization: @organization, ecl_id: ecl_content_1['id'], ecl_metadata: ecl_data.merge('external_id' => resource_group_1.client_resource_id.to_s), author_id: nil, is_public: true, is_manual: false
    ecl_card_2 = create :card, organization: @organization, ecl_id: ecl_content_2['id'], ecl_metadata: ecl_data.merge('external_id' => resource_group_2.client_resource_id.to_s), author_id: nil, is_public: true, is_manual: false

    # 4. Make `user` to be a valid user in one of the groups
    resource_group_1.add_member(user)
    resource_group_1.reload

    Search::UserSearch.any_instance.expects(:search).returns(Search::UserSearchResponse.new(results: [], total: 20)).once
    Search::ChannelSearch.any_instance.expects(:search).returns(Search::ChannelSearchResponse.new(results: [], total: 10)).once

    EclApi::EclSearch.any_instance.expects(:_search).returns(ecl_response).once

    open_session do |sess|
      sess.get "#{@organization.home_page}/api/v2/search", { q: 'search' }, @headers.merge!('X-API-TOKEN' => user.jwt_token)

      cards = get_json_response(response)['cards']

      card_1 = cards.select { |c| c['id'] == ecl_card_1.id.to_s }[0]
      card_2 = cards.select { |c| c['id'] == ecl_card_2.id.to_s }[0]
      card_3 = cards.select { |c| c['id'] == ("ECL-" + ecl_content_3['id']) }[0]

      assert_equal resource_group_1.id, card_1['resource_group']['id']
      assert_equal true, card_1['resource_group']['is_user_enrolled']

      assert_equal resource_group_2.id, card_2['resource_group']['id']
      assert_equal false, card_2['resource_group']['is_user_enrolled']

      assert_equal resource_group_3.id, card_3['resource_group']['id']
      assert_equal false, card_3['resource_group']['is_user_enrolled']
    end
  end
end

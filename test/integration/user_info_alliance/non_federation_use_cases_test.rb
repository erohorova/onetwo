require 'test_helper'

module UserInfoAlliance
  class NonFederationUseCasesTest < ActionDispatch::IntegrationTest
    # NOTE: Organization here do not have config named `OktaApiToken`.
    setup do
      @organization = create :organization

      @params = {
        email: 'okta@edcast.com',
        first_name: 'Okta',
        last_name: 'User',
        password: 'Password@123'
      }

      @klass = UserInfo::Alliance.new(organization: @organization, user_params: @params, opts: { platform: 'web' })
    end

    test '#account returns user matching email' do
      existing_user = create :user, organization: @organization, email: 'okta@edcast.com'

      assert_equal existing_user.id, @klass.account.id
    end

    test '#valid? invalidates if password is blank' do
      @params[:password] = ''

      @klass = UserInfo::Alliance.new(organization: @organization, user_params: @params, opts: { platform: 'web' })

      refute @klass.valid?
      assert_match /Password can't be blank/, @klass.message
    end

    test '#valid? invalidates if password is invalid' do
      @params[:password] = 'password'

      klass = UserInfo::Alliance.new(organization: @organization, user_params: @params, opts: { platform: 'web' })

      refute klass.valid?
      assert_match /Password is invalid/, klass.message
    end

    test '#valid? invalidates if email already exists' do
      existing_user = create :user, organization: @organization, email: 'okta@edcast.com'

      refute @klass.valid?
      assert_match /#{I18n.t("activerecord.errors.models.user.attributes.email.taken")}/, @klass.message
    end

    test 'does not creates identity provider' do
      @klass.connect

      user = @organization.reload.users.find_by(email: 'okta@edcast.com')
      provider = IdentityProvider.find_by(user: user)

      assert_nil provider
    end
  end
end

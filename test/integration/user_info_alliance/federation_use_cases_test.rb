require 'test_helper'

module UserInfoAlliance
  class FederationUseCasesTest < ActionDispatch::IntegrationTest
    setup do
      @organization = create :organization

      @params = {
        email: 'okta@edcast.com',
        first_name: 'Okta',
        last_name: 'User',
        password: 'Password@123'
      }

      stub_okta_group_creation
      stub_okta_app_creation
      stub_okta_group_app_link
      # add a config that represents Okta is enabled in an organization.
      create :config, configable: @organization, name: 'OktaApiToken', value: SecureRandom.hex
      create :config, :boolean, configable: @organization, name: 'app.config.okta.enabled', value: 'true'

      @klass = UserInfo::Alliance.new(organization: @organization, user_params: @params, opts: { platform: 'web' })
    end

    test '#account returns user matching email primarily' do
      stub_okta_user
      @klass.instance_variable_set(:@federated_user, @mocked_federated_user)

      existing_user = create :user, organization: @organization, email: 'okta@edcast.com', federated_identifier: nil

      assert_equal existing_user.id, @klass.account.id
    end

    test '#account returns user matching email secondarily' do
      stub_okta_user
      @klass.instance_variable_set(:@federated_user, @mocked_federated_user)

      existing_user = create :user, organization: @organization, email: nil, federated_identifier: '2-okta@edcast.com'

      assert_equal existing_user.id, @klass.account.id
    end

    test '#valid? invalidates if password is blank' do
      @params[:password] = ''

      @klass = UserInfo::Alliance.new(organization: @organization, user_params: @params, opts: { platform: 'web' })

      refute @klass.valid?
      assert_match /Password can't be blank/, @klass.message
    end

    test '#valid? invalidates if password is invalid' do
      @params[:password] = 'password'

      klass = UserInfo::Alliance.new(organization: @organization, user_params: @params, opts: { platform: 'web' })

      refute klass.valid?
      assert_match /Password is invalid/, klass.message
    end

    test '#valid? invalidates if email already exists' do
      existing_user = create :user, organization: @organization, email: 'okta@edcast.com', federated_identifier: nil

      refute @klass.valid?
      assert_match /#{I18n.t("activerecord.errors.models.user.attributes.email.taken")}/, @klass.message
    end

    test 'any error from Okta must not create a user in EdCast' do
      stub_failed_user_creation

      assert_nil @klass.connect

      assert_nil @organization.reload.users.find_by(email: 'okta@edcast.com')
    end

    test 'creates user from the details returned from Okta' do
      stub_okta_user
      @klass.instance_variable_set(:@federated_user, @mocked_federated_user)

      @klass.connect

      user = @organization.reload.users.find_by(email: 'okta@edcast.com')
      assert user
      assert_equal 'Okta', user.first_name
      assert_equal 'User', user.last_name
      assert_equal '2-okta@edcast.com', user.federated_identifier
    end

    test 'should not create identity provider' do
      stub_okta_user
      @klass.instance_variable_set(:@federated_user, @mocked_federated_user)

      @klass.connect

      user = @organization.reload.users.find_by(email: 'okta@edcast.com')
      provider = IdentityProvider.find_by(user: user)

      assert_nil provider
    end
  end
end

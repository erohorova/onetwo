require 'test_helper'

class CurrentOrgsTest < ActionDispatch::IntegrationTest

  setup do
    create_default_org
    @org1, @org2 = create_list(:organization, 2)
    @user1 = create(:user, organization: @org1)
    @user2 = create(:user, organization: @org2)
  end

  test 'get currently signed in orgs' do
    post "#{@org1.home_page}/auth/users/sign_in", user: {email: @user1.email, password: @user1.password}
    post "#{@org2.home_page}/auth/users/sign_in", user: {email: @user2.email, password: @user2.password}

    get "#{Organization.default_org.home_page}/api/current_orgs", {"Content-Type" => "application/json", "Accept" => "application/json"}
    assert_response :success
    resp = ActiveSupport::JSON.decode(response.body)
    assert_equal [@org1, @org2].map{|org| org.as_json(only: [:id, :name, :host_name])}, resp
  end
end

require 'test_helper'

# holy mother of all integration tests
class CardCommentsTest < ActionDispatch::IntegrationTest

  self.use_transactional_fixtures = false

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'

    StreamCreateJob.unstub :perform_later
    EdcastActiveJob.unstub :perform_later

    User.any_instance.stubs(:reindex_async).returns(true)
    @org = create(:organization)
    @user = create(:user, organization: @org)
  end

  test 'do not add card activity to stream' do
    Comment.any_instance.stubs(:record_comment_event)
    stub_request(:put, %r{http://localhost:9200/cards_test/card/.*})
    card = create(:card, card_type: 'media', card_subtype: 'text', author: create(:user, organization: @org))

    user = create(:user, organization: @org)

    another_user = create(:user, organization: @org)
    card_comment = create(:comment, commentable: card, user: user)
    assert_no_difference ->{Stream.count} do
      vote = create(:vote, votable: card_comment, voter: another_user)
      vote.run_callbacks(:commit)
      card.run_callbacks(:commit)
    end
  end

  test 'should log the cookie, session, user agent, path when authorized' do
    open_session do |sess|
      sess.post "/comments/1/votes", {}.to_json,
                {'Content-Type' => 'application/json', 'Accept' => 'application/json', 'User-Agent' => 'MyTestAgent', 'Cookie' => 'm=3247:t|34e2:|18c3:t|2c69:t|4a01:t|ca3:t|5cf4:t|745a:t|1d98:t|2a03:t|54e1:t|77cb:t|79d4:chart|640c:small|678e:600%7C5|47ba:t; csrftoken=8rRX7x5dy9Hoin8g3wz7HbDA7PfdhZGm; _csrf=vbvO3K95SGgKCS1lrlrZEQAQ; _savannah_session=a1l2bzFJZDFPUVc3Q0ZCanh5TmdaWVNlR0V0K2VvSjcyeHM4NmN3SCtkSm5IZmNxT2VzMkx6Z0dNL3JqMjJ3bmlBemNOQldNVm5Eamp4SkNuQ1kxMXVna2JEWFlHbnNMRllaQW1DYWhHQU1mOHNzeWJzc1VQKzNKa3pEVE1lb1dGRWVrQ3RPb0IyblkrTDBvNlVzZVlEa1FnZkg0Q0VEM2pyMVM3QkpnTk5iOGdRNXRGaFZrUVBUM0lMeFc3bXRFKzJ3cnhMOHJZanRuSThpL3dBR1FCU1JnTzkraTQ1Tzg4Q1BiZ1BGSVp6a0NadFhIWmNPZVpWL3ZnUTFJNFUrVWdWcHB2QzVFQXBsMzNicWZNdW9Fc29hZHFpWWQvOUptVkJZak8vbWhmMDlKQlFwajh2b2Y3Y2w1dk1BTlNTQ1ctLXdlaFBvcjJxMzVtUXcwWDNOL3lLWWc9PQ%3D%3D--d3cbc24822d041e418ee5a5e995854694cd58689; _ga=GA1.1.1691755882.1394836332'}
    end
  end
end

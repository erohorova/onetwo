require 'test_helper'

class FollowRoutingTest < ActionDispatch::IntegrationTest
  setup do
    skip
  end
  test 'alternate routes for [un]follow work' do
    Follow.any_instance.stubs(:record_user_follow_action)

    org = create(:organization)
    user, follower = create_list(:user, 2, organization: org)
    channel = create(:channel, :robotics, organization: org)
    token = follower.jwt_token

    open_session do |sess|

      [user].each do |followee|
        assert_difference ->{Follow.count} do
          sess.post "#{org.home_page}/api/#{followee.class.name.tableize}/#{followee.id}/follow", {}, {"Content-Type" => "application/json", "Accept" => "application/json", "X-Edcast-JWT" => 1, "X-API-TOKEN" => token}
        end
        last_follow = Follow.last
        assert_equal followee, last_follow.followable
        assert_equal follower, last_follow.user

        assert_difference ->{Follow.count}, -1 do
          sess.post "#{org.home_page}/api/#{followee.class.name.tableize}/#{followee.id}/unfollow", {}, {"Content-Type" => "application/json", "Accept" => "application/json", "X-Edcast-JWT" => 1, "X-API-TOKEN" => token}
        end
        assert_not Follow.where(user: follower, followable: followee).any?
      end
    end
  end
end

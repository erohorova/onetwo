require 'test_helper'

module RefinedResponse
  class AssignmentsApiTest < ActionDispatch::IntegrationTest
    setup do
      @org = create :organization
      @admin = create :user, organization: @org, organization_role: 'admin'

      resource = create :resource, user_id: @admin.id
      @card     = create :public_media_card, author_id: @admin.id, organization_id: @org.id, resource_id: resource.id
      @assignee = create :user, organization: @org, organization_role: 'member'

      @headers = { 'Content-Type' => 'application/json', 'Accept' => 'application/json', 'X-Edcast-JWT' => 1 }
      Organization.any_instance.stubs(:reporting_content_enabled?).returns(true)
    end

    test 'assignments#index v2 returns all data in the response if the request is originated from EdCast app' do
      ActionDispatch::Request.any_instance.stubs(:user_agent).returns('EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0')

      assignment1 = create(:assignment, assignable: @card, assignee: @assignee, state: Assignment::COMPLETED)

      open_session do |sess|
        sess.get "#{@org.secure_home_page}/api/v2/assignments", {}, @headers.merge!('X-API-TOKEN' => @assignee.jwt_token)

        # sess.get "#{@o.secure_home_page}/api/v2/assignments.json", {}, headers: @headers
        json_details = get_json_response(sess.response)
        response_keys = json_details['assignments'][0].keys

        # Returns expensive details
        assert_same_elements ['id', 'title', 'due_at', 'start_date', 'assignable_id', 'assignable_type', 'started_at', 'completed_at',
         'state', 'deep_link_id', 'deep_link_type', 'assignable', 'assign_url', 'image_url', 'teams_count', 'teams'], response_keys
      end
    end

    test 'assignments#index v1 returns specific data in the response if the request is originated from web' do
      create(
        :assignment, assignable: @card,
        assignee: @assignee, state: Assignment::COMPLETED
      )

      open_session do |sess|
        keys = %w[id title message card_type card_subtype slug state is_official provider
          provider_image readable_card_type share_url average_rating skill_level filestack
          votes_count comments_count published_at prices is_assigned is_bookmarked hidden
          is_public is_paid mark_feature_disabled_for_source completion_state is_upvoted
          all_ratings is_reported author resource is_clone created_at paid_by_user due_at
          subscription_end_date]
        card_fields = keys.join(',')
        fields = "id,assignable,card(#{card_fields})"
        sess.get "#{@org.secure_home_page}/api/v2/assignments", {fields: fields}, @headers.merge!('X-API-TOKEN' => @assignee.jwt_token)

        json_details = get_json_response(sess.response)
        expected_keys = %w[id assignable]

        assert_same_elements expected_keys, json_details['assignments'][0].keys
        assert_same_elements keys, json_details['assignments'][0]['assignable'].keys
      end
    end
  end
end

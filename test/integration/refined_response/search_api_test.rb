# TODO:
#   This file is temporary to address differences in response for mobile and web separately.
#   Once the response is unified, this file can be deleted. And related tests can be moved to respective test suite.
require 'test_helper'

module RefinedResponse
  class SearchApiTest < ActionDispatch::IntegrationTest
    setup do
      @o = create :organization
      @u = create :user, organization: @o
      @a = create :user, organization: @o, organization_role: 'admin'

      @c = create :channel, organization: @o
      @c.add_authors([@a])
      @c.add_followers([@u])

      @headers = { 'Content-Type' => 'application/json', 'Accept' => 'application/json', 'X-Edcast-JWT' => 1, 'X-API-TOKEN' => @u.jwt_token }

      Card.any_instance.stubs(:reindex).returns true
      User.stubs(:pubnub_channels_to_subscribe_to_for_id).returns([])

      media_link_card = create(:card, author_id: @a.id, card_type: 'media', card_subtype: 'link', resource: create(:resource))
      create(:assignment, assignable: media_link_card, assignee: @u, due_at: Time.now)
      create(:card_subscription, card_id: media_link_card.id, user_id: @u.id, end_date: Time.now)
      media_video_card = create(:card, author_id: @a.id, card_type: 'media', card_subtype: 'video', resource: create(:resource))
      cards = [media_link_card, media_video_card]
      pathway_card = create(:card, card_type: 'pack', card_subtype: 'simple', author_id: @a.id)
      hidden_cards = create_list(:card, 2, hidden: true, author_id: @a.id)
      hidden_cards.each do |card|
        CardPackRelation.add(cover_id: pathway_card.id, add_id: card.id, add_type: card.class.name)
      end

      query = "query"
      limit = 10
      offset = 0
      Search::UserSearch.any_instance.expects(:search)
        .with(limit: limit, offset: offset, filter: {}, load: true, q: query, viewer: @u)
        .returns(Search::UserSearchResponse.new(results: [], total: 20)).once

      Search::ChannelSearch.any_instance.expects(:search)
        .with(limit: limit, offset: offset, :filter => {:include_private => true, skip_aggs: true}, load: true, q: query,
          viewer: @u, sort: [{:key => '_score', :order => :desc}])
        .returns(Search::ChannelSearchResponse.new(results: [], total: 10)).once

      ecl_response = JSON.parse(File.read("#{Rails.application.root}/test/fixtures/api/ecl/ecl_api.json"))["/api/v1/content_items"]

      EclApi::EclSearch.any_instance.stubs(:search).returns(OpenStruct.new(results: cards, aggregations: ecl_response['aggregations'])).once

      Organization.any_instance.stubs(:reporting_content_enabled?).returns(true)
    end

    test 'search v2 returns all data in the cards response if request is originated from EdCast app' do
      ActionDispatch::Request.any_instance.stubs(:user_agent).returns('EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0')

      get "#{@o.secure_home_page}/api/v2/search.json", { q: 'query' }, @headers

      json_details = get_json_response(response)
      response_keys = json_details['cards'][0].keys

      # Returns expensive details
      assert_includes response_keys, 'completed_percentage'
      assert_includes response_keys, 'is_official'
      assert_includes response_keys, 'is_public'
      assert_includes response_keys, 'voters'
      assert_includes response_keys, 'channel_ids'
      assert_includes response_keys, 'non_curated_channel_ids'
      assert_includes response_keys, 'user_rating'
      assert_includes response_keys, 'all_ratings'
      assert_includes response_keys, 'is_bookmarked'
      assert_includes response_keys, 'is_reported'
      assert_includes response_keys, 'is_assigned'
      assert_includes response_keys, 'completion_state'
      assert_includes response_keys, 'resource'
      assert_includes response_keys, 'mentions'
      assert_includes response_keys, 'tags'
      assert_includes response_keys, 'channels'
      assert_includes response_keys, 'users_with_access'
      assert_includes response_keys, 'user_taxonomy_topics'

      # expected when it is a PATHWAY
      # assert_includes response_keys, 'pack_cards'

      # expected when it is a JOURNEY
      # assert_includes response_keys, 'journey_section'

      # expected when it is an ASSIGNMENT
      # assert_includes response_keys, 'assignment'

      # expected if LEAPS enabled
      # assert_includes response_keys, 'leaps'
    end

    test 'search v2 returns specific data in the response if the request is originated from web' do
      expected_keys = %w[
        id title message card_type card_subtype slug state is_official provider
        average_rating provider_image readable_card_type share_url comments_count
        skill_level filestack author votes_count resource published_at prices
        is_assigned is_bookmarked is_reported is_public hidden is_paid
        mark_feature_disabled_for_source completion_state all_ratings
        is_upvoted is_clone created_at paid_by_user subscription_end_date due_at
      ]
      fields = expected_keys.join(',')

      get "#{@o.secure_home_page}/api/v2/search.json", { q: 'query', fields: fields }, @headers

      json_details = get_json_response(response)
      search_response = json_details['cards'][0]

      # Returns default details

      # includes card details
      assert_same_elements expected_keys, search_response.keys

      # includes author details
      assert_same_elements ['id', 'handle', 'avatarimages', 'full_name', 'profile'], search_response['author'].keys
    end
  end
end

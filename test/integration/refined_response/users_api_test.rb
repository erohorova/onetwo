require 'test_helper'

module RefinedResponse
  class UsersApiTest < ActionDispatch::IntegrationTest
    setup do
      create_default_ssos
      Organization.any_instance.unstub :default_sso
      @org = create :organization
      @user = create :user, organization: @org
      @admin = create :user, organization: @org, organization_role: 'admin'

      @channel = create :channel, organization: @org
      @channel.add_authors([@admin])
      @channel.add_followers([@user])

      @headers = { 'Content-Type' => 'application/json', 'Accept' => 'application/json', 'X-Edcast-JWT' => 1 }

      User.stubs(:pubnub_channels_to_subscribe_to_for_id).returns([])
    end

    test 'users#info v1 returns all data in the response if the request is originated from EdCast app' do
      ActionDispatch::Request.any_instance.stubs(:user_agent).returns('EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0')

      standard_sign_in(@user)

      get "#{@org.secure_home_page}/api/users/info.json", {}, headers: @headers

      json_details = get_json_response(response)
      response_keys = json_details.keys

      # Returns expensive details
      assert_includes response_keys, 'followers_count'
      assert_includes response_keys, 'following_channels'
      assert_includes response_keys, 'following_users'
      assert_includes response_keys, 'pubnub_channels'
      assert_includes response_keys, 'user_subscriptions'
      assert_includes response_keys, 'writable_channels'
    end

    test 'users#info v1 returns specific data in the response if the request is originated from web' do
      standard_sign_in(@user)

      get "#{@org.secure_home_page}/api/users/info.json", {}, headers: @headers

      json_details = get_json_response(response)

      assert_same_elements ['id', 'email', 'full_name', 'is_admin',
        'is_super_admin', 'handle', 'org_annual_subscription_paid',
        'picture', 'organization', 'permissions', 'profile', 'active_orgs',
        'csrf_param', 'csrf_token', 'jwt_token', 'onboarding_completed', 'step',
        'country_code'], json_details.keys

    end
  end
end

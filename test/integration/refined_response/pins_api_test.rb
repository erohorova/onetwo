require 'test_helper'

module RefinedResponse
  class PinsApiTest < ActionDispatch::IntegrationTest
    setup do
      @user = create(:user)
      @org = @user.organization
      author = create(:user, organization: @org)
      cards = create_list(:card, 10, organization: @org, author: author)
      @channel = create(:channel, organization: @org)
      @channel.curators << @user
      cards.each { |card| Pin.create(pinnable: @channel, object: card, user: @user) }

      @headers = { 'Content-Type' => 'application/json', 'Accept' => 'application/json', 'X-Edcast-JWT' => 1 }
      Organization.any_instance.stubs(:reporting_content_enabled?).returns(true)
    end

    test 'pins#index v2 returns all data in the response if the request is originated from EdCast app' do
      ActionDispatch::Request.any_instance.stubs(:user_agent).returns('EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0')

      open_session do |sess|
        sess.get "#{@org.secure_home_page}/api/v2/pins", {pin: {pinnable_id: @channel.id, pinnable_type: 'Channel'}}, @headers.merge!('X-API-TOKEN' => @user.jwt_token)

        json_details = get_json_response(sess.response)
        response_keys = json_details['pins'][0].keys

        # Returns expensive details
        assert_same_elements ['id', 'card_type', 'card_subtype', 'slug', 'state', 'is_official', 'is_public', 'created_at',
          'updated_at', 'published_at', 'hidden', 'ecl_id', 'provider', 'comments_count', 'votes_count',
          'readable_card_type', 'title', 'message', 'views_count', 'share_url', 'completed_percentage', 'mark_feature_disabled_for_source',
          'ecl_duration_metadata', 'voters', 'channel_ids', 'non_curated_channel_ids', 'user_rating', 'all_ratings', 'average_rating',
          'skill_level', 'is_upvoted', 'auto_complete', 'is_bookmarked', 'is_reported', 'is_assigned', 'completion_state', 'filestack',
          'mentions', 'author', 'tags', 'file_resources', 'channels', 'teams', 'users_with_access', 'user_taxonomy_topics', 'paid_by_user',
          'is_paid', 'payment_enabled', 'prices', 'language'], response_keys
      end
    end

    test 'pins#index v2 returns specific data in the response if the request is originated from web' do

      open_session do |sess|
        sess.get "#{@org.secure_home_page}/api/v2/pins", {pin: {pinnable_id: @channel.id, pinnable_type: 'Channel'}}, @headers.merge!('X-API-TOKEN' => @user.jwt_token)

        json_details = get_json_response(sess.response)
        assert_same_elements ['id', 'title', 'message', 'card_type', 'card_subtype', 'slug', 'state', 'is_official', 'provider',
          'provider_image', 'readable_card_type', 'share_url', 'average_rating', 'skill_level', 'filestack', 'votes_count',
          'comments_count', 'published_at', 'prices', 'is_assigned', 'is_bookmarked', 'hidden', 'is_public', 'is_paid',
          'mark_feature_disabled_for_source', 'completion_state', 'is_upvoted', 'all_ratings', 'is_reported', 'author',
          'is_clone', 'created_at'], json_details['pins'][0].keys
      end
    end
  end
end

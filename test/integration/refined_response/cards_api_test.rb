require 'test_helper'

module RefinedResponse
  class CardsApiTest < ActionDispatch::IntegrationTest
    setup do
      @filestack = [{
        'filename' => 'Screen Shot 2018-06-05 at 11.51.06 AM.png',
        'mimetype' => 'image/png',
        'size' => '1946518',
        'source' => 'local_file_system',
        'url' => 'https://cdn.filestackcontent.com/d2sjlUgWT1WRJwldER6P',
        'handle' => 'd2sjlUgWT1WRJwldER6P',
        'status' => 'Stored',
      }]

      @org = create :organization
      @a = create :user, organization: @org, organization_role: 'admin'

      resource = create :resource, user_id: @a.id
      @card = create :public_media_card, author_id: @a.id, organization_id: @org.id, resource_id: resource.id, filestack: @filestack

      @headers = { 'Content-Type' => 'application/json', 'Accept' => 'application/json', 'X-Edcast-JWT' => 1 }
      Organization.any_instance.stubs(:reporting_content_enabled?).returns(true)
    end

    test 'cards#index v2 returns all data in the response if the request is originated from EdCast app' do
      ActionDispatch::Request.any_instance.stubs(:user_agent).returns('EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0')
      org = create :organization
      user = create :user, organization: org, organization_role: 'admin'
      resource = create :resource, user_id: user.id
      card = create :public_media_card, author_id: user.id, organization_id: org.id, resource_id: resource.id, filestack: @filestack
      create(
        :assignment,
        assignable: card,
        assignee: user,
        due_at: Time.now
      )
      create(
        :card_subscription,
        card_id: card.id,
        user_id: user.id,
        end_date: Time.now + 1.days
      )

      get "#{org.secure_home_page}/api/v2/cards", {}, @headers.merge!('X-API-TOKEN' => user.jwt_token)

      json_details = get_json_response(response)
      response_keys = json_details['cards'].first.keys
      assert_same_elements ['id', 'card_type', 'card_subtype', 'slug', 'state', 'is_official', 'is_public', 'created_at',
        'updated_at', 'published_at', 'hidden', 'ecl_id', 'provider', 'comments_count', 'votes_count',
        'readable_card_type', 'title', 'message', 'views_count', 'share_url', 'completed_percentage', 'mark_feature_disabled_for_source',
        'ecl_duration_metadata', 'voters', 'channel_ids', 'non_curated_channel_ids', 'user_rating', 'all_ratings', 'average_rating',
        'skill_level', 'is_upvoted', 'auto_complete', 'is_bookmarked','is_reported', 'is_assigned', 'completion_state', 'filestack', 'resource',
        'mentions', 'author', 'tags', 'file_resources', 'channels', 'teams', 'users_with_access', 'user_taxonomy_topics',
        'paid_by_user', 'is_paid', 'payment_enabled', 'prices', 'language', 'assignment'], response_keys
      
      # assert for specific information
      assert_equal 'https://cdn.filestackcontent.com/d2sjlUgWT1WRJwldER6P', json_details['cards'][0]['filestack'][0]['url']
    end

    test 'cards#index v2 returns all data in the response if the request is originated from web cms for content type pack' do
      skip('Yet to be optimised')
      2.times do |n|
        cover = create :card, author: @a, card_type: 'pack'
        CardPackRelation.add(cover_id: cover.id, add_id: create(:card, author: @a).id)
        cover.publish!
      end

      get "#{@o.secure_home_page}/api/v2/cards", {full_response: true, card_type: ['pack'], state: ['draft', 'published']}, @headers.merge!('X-API-TOKEN' => @a.jwt_token)

      json_details = get_json_response(response)

      response_keys = json_details['cards'][0].keys
      assert_same_elements ['id', 'card_type', 'card_subtype', 'slug', 'state', 'is_official', 'is_public', 'created_at',
        'updated_at', 'published_at', 'hidden', 'ecl_id', 'provider', 'provider_image', 'comments_count', 'votes_count',
        'readable_card_type', 'title', 'message', 'views_count', 'share_url', 'completed_percentage', 'mark_feature_disabled_for_source',
        'ecl_duration_metadata', 'voters', 'channel_ids', 'non_curated_channel_ids', 'user_rating', 'all_ratings', 'average_rating',
        'skill_level', 'is_upvoted', 'auto_complete', 'is_bookmarked', 'is_reported', 'is_assigned', 'completion_state', 'filestack',
        'mentions', 'author', 'tags', 'file_resources', 'channels', 'teams', 'users_with_access', 'pack_cards',
        'user_taxonomy_topics', 'paid_by_user', 'is_paid', 'payment_enabled', 'prices'], response_keys

    end

    test 'cards#index v2 returns all data in the response if the request is originated from web cms for content type journey' do
      skip('Yet to be optimised')
      2.times do |n|
        cover = create :card, author: @a, card_type: 'journey', card_subtype: 'self_paced'
        CardPackRelation.add(cover_id: cover.id, add_id: create(:card, author: @a).id)
        cover.publish!
      end

      get "#{@o.secure_home_page}/api/v2/cards", {full_response: true, card_type: ['journey'], state: ['draft', 'published']}, @headers.merge!('X-API-TOKEN' => @a.jwt_token)

      json_details = get_json_response(response)

      response_keys = json_details['cards'][0].keys
      assert_same_elements ['id', 'card_type', 'card_subtype', 'slug', 'state', 'is_official', 'is_public', 'created_at',
        'updated_at', 'published_at', 'hidden', 'ecl_id', 'provider', 'provider_image', 'comments_count', 'votes_count',
        'readable_card_type', 'title', 'message', 'views_count', 'share_url', 'completed_percentage', 'mark_feature_disabled_for_source',
        'ecl_duration_metadata', 'voters', 'channel_ids', 'non_curated_channel_ids', 'user_rating', 'all_ratings', 'average_rating',
        'skill_level', 'is_upvoted', 'auto_complete', 'is_bookmarked', 'is_reported', 'is_assigned', 'completion_state', 'filestack',
        'mentions', 'author', 'tags', 'file_resources', 'channels', 'teams', 'users_with_access', 'journey_section',
        'user_taxonomy_topics', 'paid_by_user', 'is_paid', 'payment_enabled', 'prices'], response_keys
    end

    test 'cards#index v2 returns subscription_end_date and due_at in the full response for content type pack' do
      ActionDispatch::Request.any_instance.stubs(:user_agent).returns('EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0')
      org = create :organization
      user = create :user, organization: org, organization_role: 'admin'
      resource = create :resource, user_id: user.id
      card = create(:card, author_id: user.id, organization_id: org.id, resource_id: resource.id, card_type: 'pack', card_subtype: 'simple')
      card.publish!
      create(
        :assignment,
        assignable: card,
        assignee: user,
        due_at: Time.now
      )
      create(
        :card_subscription,
        card_id: card.id,
        user_id: user.id,
        end_date: Time.now + 1.days
      )
      get "#{org.secure_home_page}/api/v2/cards",
          {full_response: true, card_type: ['pack'], state: ['draft', 'published']},
          @headers.merge!('X-API-TOKEN' => user.jwt_token)
      json_details = get_json_response(response)
      assert json_details['cards'].first.key?('subscription_end_date' && 'due_at')
    end

    test 'cards#index v2 returns on demand response if the request is originated from web cms for content type media, course, poll' do
      keys = %w[id card_type card_subtype created_at ecl_duration_metadata message title state hidden
             slug channels author tags filestack resource is_paid due_at subscription_end_date paid_by_user]
      fields = keys.join(',')
      get "#{@org.secure_home_page}/api/v2/cards",
          { is_cms: true, card_type: ['media', 'course', 'poll'], fields: fields },
          @headers.merge!('X-API-TOKEN' => @a.jwt_token)

      json_details = get_json_response(response)

      response_keys = json_details['cards'][0].keys
      assert_same_elements keys, response_keys
    end

    test 'cards#index v2 specific data in the response if the request is originated from web' do
      Settings.stubs(:serve_authenticated_images).returns(true)

      get "#{@org.secure_home_page}/api/v2/cards", {}, @headers.merge!('X-API-TOKEN' => @a.jwt_token)

      json_details = get_json_response(response)
      card_response = json_details['cards'].select { |card| card['id'] == @card.id.to_s }[0]
      response_keys = card_response.keys
      expected_keys = %w[
        id title message card_type card_subtype slug state is_official provider
        provider_image readable_card_type share_url average_rating skill_level
        filestack votes_count comments_count published_at prices is_assigned
        is_bookmarked is_reported hidden is_public is_paid
        mark_feature_disabled_for_source author resource completion_state
        all_ratings is_upvoted is_clone created_at
      ]

      assert_same_elements expected_keys, response_keys
      assert_match /https:\/\/#{@org.host_name}.test.host\/uploads/, card_response['filestack'][0]['url']
    end

    test 'cards#index v2 return default fields when fields are not provided' do
      2.times do
        cover = create :card, author: @a, card_type: 'pack'
        CardPackRelation.add(cover_id: cover.id, add_id: create(:card, author: @a).id)
        cover.publish!
      end

      url = "#{@org.secure_home_page}/api/v2/cards"
      headers = @headers.merge!('X-API-TOKEN' => @a.jwt_token)

      get url, {card_type: ['pack']}, headers
      resp = get_json_response response
      expected_keys = %w[
        id title message card_type card_subtype slug state is_official provider
        provider_image readable_card_type share_url average_rating skill_level
        filestack votes_count comments_count published_at prices is_assigned
        is_bookmarked is_reported hidden is_public is_paid completion_state
        mark_feature_disabled_for_source author all_ratings is_upvoted is_clone
        completed_percentage created_at
      ]
      assert_equal 2, resp['cards'].count
      assert_same_elements expected_keys, resp['cards'][0].keys
    end

    test 'cards#index v2 should return on demand fields when fields param is present' do
      2.times do |n|
        cover = create :card, author: @a, card_type: 'pack'
        CardPackRelation.add(cover_id: cover.id, add_id: create(:card, author: @a).id)
        cover.publish!
      end

      get "#{@org.secure_home_page}/api/v2/cards", {card_type: ['pack'], fields: 'id,title,message,slug,filestack,pack_cards_count'}, @headers.merge!('X-API-TOKEN' => @a.jwt_token)
      resp = get_json_response response

      assert_equal 2, resp['cards'].count
      assert_same_elements ["id", "title", "message", "slug", "filestack", "pack_cards_count"], resp['cards'][0].keys
    end

    test 'cards#purchased v2 returns all data in the response if the request is originated from EdCast app' do
      skip("INTERMITTENT FAILURE ON TRAVIS")
      ActionDispatch::Request.any_instance.stubs(:user_agent).returns('EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0')

      user_card_subscription1 = create(:card_subscription, user_id: @a.id, card_id: @card.id, transaction_id: 2)
      get "#{@org.secure_home_page}/api/v2/cards/purchased", {}, @headers.merge!('X-API-TOKEN' => @a.jwt_token)

      json_details = get_json_response(response)

      response_keys = json_details['cards'][0].keys
      assert_same_elements ['id', 'card_type', 'card_subtype', 'slug', 'state', 'is_official', 'is_public', 'created_at',
        'updated_at', 'published_at', 'hidden', 'ecl_id', 'provider', 'provider_image', 'comments_count', 'votes_count',
        'readable_card_type', 'title', 'message', 'views_count', 'share_url', 'completed_percentage', 'mark_feature_disabled_for_source',
        'ecl_duration_metadata', 'voters', 'channel_ids', 'non_curated_channel_ids', 'user_rating', 'all_ratings', 'average_rating',
        'skill_level', 'is_upvoted', 'auto_complete', 'is_bookmarked', 'is_reported', 'is_assigned', 'completion_state', 'filestack',
        'mentions', 'author', 'tags', 'file_resources', 'channels', 'teams', 'users_with_access', 'user_taxonomy_topics', 'paid_by_user',
        'is_paid', 'payment_enabled', 'prices', 'resource'], response_keys
    end

    test ':api show should return language in response for journey_card' do
      Analytics::CardViewMetricsRecorder.stubs(:get_initial_card_view_count).returns(0)
      journey = create(:card, author: @a, card_type: 'journey', card_subtype: 'self_paced', language: 'en')

      get "#{@org.secure_home_page}/api/v2/cards/#{journey.id}", {}, @headers.merge!('X-API-TOKEN' => @a.jwt_token)
      json_details = get_json_response(response)
      assert_equal journey['language'], json_details['language']
    end

    test 'cards#purchased v2 returns on demand response if the request is originated from web' do
      skip("INTERMITTENT FAILURE ON TRAVIS")
      user_card_subscription1 = create(:card_subscription, user_id: @a.id, card_id: @card.id, transaction_id: 2)
      get "#{@org.secure_home_page}/api/v2/cards/purchased", {}, @headers.merge!('X-API-TOKEN' => @a.jwt_token)

      json_details = get_json_response(response)

      response_keys = json_details['cards'][0].keys
      assert_same_elements ['id', 'title', 'message', 'card_type', 'card_subtype', 'slug', 'state', 'is_official', 'provider',
        'provider_image', 'readable_card_type', 'share_url', 'average_rating', 'skill_level', 'filestack', 'votes_count',
        'comments_count', 'published_at', 'prices', 'is_assigned', 'is_bookmarked', 'hidden', 'is_public', 'is_paid',
        'mark_feature_disabled_for_source', 'completion_state', 'is_upvoted', 'all_ratings', 'is_reported', 'author',
        'resource', 'is_clone'], response_keys
    end

    test ':api show should return assigner field in response' do
      Analytics::CardViewMetricsRecorder.stubs(:get_initial_card_view_count)
        .returns(0)
      card = create(:card, author: @a)
      assignment = create(:assignment, assignable: card, assignee: @a, due_at: Date.parse("#{Date.current.year}/06/01"))
      create(:team_assignment,assignment: assignment, assignor: @a, team_id: nil)
      keys = %w(id handle full_name first_name last_name avatarimages)

      get "#{@org.secure_home_page}/api/v2/cards/#{card.id}", {is_standalone_page: true, fields: 'assigner'},
          @headers.merge!('X-API-TOKEN' => @a.jwt_token)
      json_details = get_json_response(response)
      assert_same_elements keys, json_details['assigner'].keys
    end
  end
end

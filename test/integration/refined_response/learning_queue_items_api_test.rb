require 'test_helper'

module RefinedResponse
  class LearningQueueItemsApiTest < ActionDispatch::IntegrationTest
    setup do
      @org = create :organization
      @user = create :user, organization: @org
      card = create :card, author: @user
      @completed_item = create :learning_queue_item, :from_assignments, queueable: card, user: @user, state: "completed"
      @headers = { 'Content-Type' => 'application/json', 'Accept' => 'application/json', 'X-Edcast-JWT' => 1 }
      Organization.any_instance.stubs(:reporting_content_enabled?).returns(true)
    end

    test 'learning_queue_items#index completed v2 returns all data in the response if the request is originated from EdCast app' do
      ActionDispatch::Request.any_instance.stubs(:user_agent).returns('EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0')

      get "#{@org.secure_home_page}/api/v2/learning_queue_items", {state: ['completed'], include_queueable_details: true}, @headers.merge!('X-API-TOKEN' => @user.jwt_token)
      json_response = get_json_response(response)

      expected_keys = %w(id display_type deep_link_id deep_link_type source updated_at snippet queueable)
      assert_same_elements expected_keys, json_response['learning_queue_items'][0].keys

      # Returns expensive queueable details
      expected_queueable_keys = %w(id card_type card_subtype slug state is_official is_public created_at updated_at
        published_at hidden ecl_id provider comments_count votes_count readable_card_type title message
        views_count share_url completed_percentage mark_feature_disabled_for_source ecl_duration_metadata voters
        channel_ids non_curated_channel_ids user_rating all_ratings average_rating skill_level is_upvoted auto_complete
        is_bookmarked is_reported is_assigned completion_state filestack mentions author tags file_resources channels teams
        users_with_access user_taxonomy_topics paid_by_user is_paid payment_enabled prices language)

      assert_same_elements expected_queueable_keys, json_response['learning_queue_items'][0]['queueable'].keys

      assert_equal expected_queueable_keys.length, 50
    end

    test 'learning_queue_items#index completed v2 return specific data in the response if the request is originated from web' do
      get "#{@org.secure_home_page}/api/v2/learning_queue_items", {state: ['completed'], include_queueable_details: true}, @headers.merge!('X-API-TOKEN' => @user.jwt_token)
      json_response = get_json_response(response)

      # Returns minimal queueable details
      expected_queueable_keys = %w[
        id title message card_type card_subtype slug state is_official
        provider provider_image readable_card_type share_url average_rating
        filestack votes_count comments_count published_at prices skill_level
        is_assigned is_bookmarked is_reported hidden is_public is_paid
        mark_feature_disabled_for_source author completion_state all_ratings
        is_upvoted is_clone created_at
      ]

      assert_same_elements expected_queueable_keys, json_response['learning_queue_items'][0]['queueable'].keys
      assert_equal expected_queueable_keys.length, 32
    end
  end
end

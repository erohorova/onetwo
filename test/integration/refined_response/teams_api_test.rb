require 'test_helper'

module RefinedResponse
  class TeamsApiTest < ActionDispatch::IntegrationTest
    setup do
      @org = create :organization
      @admin = create :user, organization: @org, organization_role: 'admin'
      @user = create :user, organization: @org

      # @user is an admin in @team1
      @team1 = create :team, organization: @org
      @team1.add_admin(@user)

      # @user is a sub-admin in @team2 and @admin is an admin in @team2
      @team2 = create :team, organization: @org
      @team2.add_admin(@admin)
      @team2.add_sub_admin(@user)

      # @user is a member in @team3 and @admin is an admin in @team3
      @team3 = create :team, organization: @org
      @team3.add_member(@user)

      # @u is invited in @team4 and @admin is an admin in @team4
      @team4 = create :team, organization: @org
      @team4.add_admin(@admin)
      create :invitation, invitable: @team4, recipient: @user, accepted_at: nil

      @headers = { 'Content-Type' => 'application/json', 'Accept' => 'application/json', 'X-Edcast-JWT' => 1 }
      @all_fields = %w[
        id name description slug image_urls members_count is_private is_team_admin
        is_team_sub_admin is_member is_pending created_at created_by organization_id
        pending_members_count is_everyone_team auto_assign_content roles is_dynamic
        is_mandatory domains only_admin_can_post is_dynamic_selected carousels
      ]
    end

    test 'teams#index v2 returns all data in the response if the request is originated from EdCast app' do
      ActionDispatch::Request.any_instance.stubs(:user_agent).returns('EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0')

      open_session do |sess|
        sess.get "#{@org.secure_home_page}/api/v2/teams", {}, @headers.merge!('X-API-TOKEN' => @admin.jwt_token)

        json_details = get_json_response(sess.response)
        response_keys = json_details['teams'][0].keys

        assert_same_elements @all_fields, response_keys
      end
    end

    test 'teams#index v2 returns all data in the response if the request is originated from CMS' do
      open_session do |sess|
        sess.get "#{@org.secure_home_page}/api/v2/teams", { full_response: 'true' }, @headers.merge!('X-API-TOKEN' => @admin.jwt_token)

        json_details = get_json_response(sess.response)
        response_keys = json_details['teams'][0].keys

        assert_same_elements @all_fields, response_keys
      end
    end

    test 'teams#index v2 returns minimal data in the response if the request is originated from web' do
      open_session do |sess|
        sess.get "#{@org.secure_home_page}/api/v2/teams", {}, @headers.merge!('X-API-TOKEN' => @admin.jwt_token)

        json_details = get_json_response(sess.response)
        response_keys = json_details['teams'][0].keys
        expected_keys = %w[
          id name description slug image_urls members_count
          is_private is_team_admin is_team_sub_admin is_member
          is_pending is_everyone_team is_mandatory only_admin_can_post
          created_by
        ]
        assert_same_elements expected_keys, response_keys
      end
    end

    test 'teams#search v2 returns all data in the response if the request is originated from EdCast app' do
      ActionDispatch::Request.any_instance.stubs(:user_agent).returns('EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0')

      open_session do |sess|
        sess.get "#{@org.secure_home_page}/api/v2/teams", {}, @headers.merge!('X-API-TOKEN' => @admin.jwt_token)

        json_details = get_json_response(sess.response)
        response_keys = json_details['teams'][0].keys

        assert_same_elements @all_fields, response_keys
      end
    end

    test 'teams#search v2 returns all data in the response if the request is originated from CMS' do
      ActionDispatch::Request.any_instance.stubs(:user_agent).returns('EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0')

      open_session do |sess|
        sess.get "#{@org.secure_home_page}/api/v2/teams", { full_response: 'true' }, @headers.merge!('X-API-TOKEN' => @admin.jwt_token)

        json_details = get_json_response(sess.response)
        response_keys = json_details['teams'][0].keys

        assert_same_elements @all_fields, response_keys
      end
    end

    test 'teams#search v2 returns all data in the response if the request is originated from web' do
      open_session do |sess|
        sess.get "#{@org.secure_home_page}/api/v2/teams", {}, @headers.merge!('X-API-TOKEN' => @admin.jwt_token)

        json_details = get_json_response(sess.response)
        response_keys = json_details['teams'][0].keys

        expected_keys = %w[
          id name description slug image_urls members_count is_private
          is_team_admin is_team_sub_admin is_member is_pending is_everyone_team
          is_mandatory only_admin_can_post created_by
        ]
        assert_same_elements expected_keys, response_keys
      end
    end

    test ':api show channels followed by the group with requested fields' do
      user_list = create_list(:user, 3, organization: @org)
      @team1 = create(:team, organization: @org)

      @team1.add_member user_list[0]
      @team1.add_member user_list[1]
      @team1.add_member user_list[2]

      channel_list = create_list(:channel, 2, organization: @org)
      channel = create(:channel, organization: @org)

      @team1.users.each do |user|
        create(:follow, followable: channel_list[0], user: user)
        create(:follow, followable: channel_list[1], user: user)
      end
      create(:teams_channels_follow, team: @team1, channel: channel_list[0])
      create(:teams_channels_follow, team: @team1, channel: channel_list[1])

      get "#{@org.secure_home_page}/api/v2/teams/#{@team1.id}/channels", {fields: 'id,allow_follow,is_following,is_private,slug,label'},  @headers.merge!('X-API-TOKEN' => @admin.jwt_token)

      assert_response :ok
      resp = get_json_response(response)
      assert_equal channel_list.map{|d| d.id}, resp['channels'].map{|d| d['id']}
      assert_equal %w(id allow_follow is_following is_private slug label), resp['channels'].first.keys
    end
  end
end

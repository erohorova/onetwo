require 'test_helper'

module RefinedResponse
  class ChannelsApiTest < ActionDispatch::IntegrationTest
    setup do
      @org = create :organization
      @user = create :user, organization: @org
      @channel = create :channel, organization: @org
      @card = create :card, author: @user
      # Add card to the channel
      @card.post_to_channel(@channel.id)
      @headers = { 'Content-Type' => 'application/json', 'Accept' => 'application/json', 'X-Edcast-JWT' => 1 }
      Organization.any_instance.stubs(:reporting_content_enabled?).returns(true)
    end

    test 'channels#index v2 returns all data in the response if the request is originated from EdCast app' do
      ActionDispatch::Request.any_instance.stubs(:user_agent).returns('EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0')

      get "#{@org.secure_home_page}/api/v2/channels", {}, @headers.merge!('X-API-TOKEN' => @user.jwt_token)
      json_response = get_json_response(response)

      single_channel_response = json_response['channels'].first

      # Returns expensive details
      %w(auto_follow keywords only_authors_can_post ecl_enabled curate_only curate_ugc is_promoted is_open provider
      banner_image_url followers_count is_owner authors_count created_at author_name curators
      carousels video_streams_count courses_count smartbites_count published_pathways_count published_journeys_count
      is_curator teams).each do |field|
        assert_includes single_channel_response.keys, field
      end
    end

    test 'channels#index v2 returns all data in the response if the request is originated from admin console' do
      get "#{@org.secure_home_page}/api/v2/channels", {is_cms: true}, @headers.merge!('X-API-TOKEN' => @user.jwt_token)
      json_response = get_json_response(response)

      single_channel_response = json_response['channels'].first

      # Returns expensive details
      %w(auto_follow keywords only_authors_can_post ecl_enabled curate_only curate_ugc is_promoted is_open provider
      banner_image_url followers_count is_owner authors_count created_at author_name curators
      carousels video_streams_count courses_count smartbites_count published_pathways_count published_journeys_count
      is_curator teams).each do |field|
        assert_includes single_channel_response.keys, field
      end
    end

    test 'channels#index v2 return specific data in the response if the request is originated from web' do
      get "#{@org.secure_home_page}/api/v2/channels", {}, @headers.merge!('X-API-TOKEN' => @user.jwt_token)
      json_response = get_json_response(response)

      single_channel_response = json_response['channels'].first

      assert_same_elements %w(id label description is_private allow_follow banner_image_urls profile_image_url profile_image_urls
        is_following updated_at slug), single_channel_response.keys
    end

    test 'channels#cards v2 return all data in the response if the request is originated from EdCast app' do
      ActionDispatch::Request.any_instance.stubs(:user_agent).returns('EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0')

      channel = create(:channel, curate_only: true, organization: @org)
      another_channel = create(:channel, curate_only: false, organization: @org)
      cards = create_list(:card, 2, organization_id: @org.id, author: @user)
      cards.each {|c| c.channels << [channel, another_channel]}

      channel.add_curators([@user])
      another_channel.add_curators([@user])

      curated_channels_card = ChannelsCard.where(channel_id: channel.id).first

      get "#{@org.secure_home_page}/api/v2/channels/#{channel.id}/cards", {channel_card_state: 'new', ugc: false}, @headers.merge!('X-API-TOKEN' => @user.jwt_token)
      resp = get_json_response(response)

      assert_same_elements ['id', 'card_type', 'card_subtype', 'slug', 'state', 'is_official', 'is_public',
        'created_at', 'updated_at', 'published_at', 'hidden', 'ecl_id', 'provider',
        'comments_count', 'votes_count', 'readable_card_type', 'title', 'message', 'views_count', 'share_url',
        'completed_percentage', 'mark_feature_disabled_for_source', 'ecl_duration_metadata', 'voters',
        'channel_ids', 'non_curated_channel_ids', 'user_rating', 'all_ratings', 'average_rating', 'skill_level',
        'is_upvoted', 'auto_complete', 'is_bookmarked', 'is_assigned', 'completion_state', 'filestack', 'mentions',
        'author', 'tags', 'file_resources', 'channels', 'teams', 'users_with_access', 'user_taxonomy_topics',
        'paid_by_user', 'is_paid', 'payment_enabled', 'prices', 'is_reported', 'language'], resp['cards'][0].keys
    end

    test 'channels#cards v2 return specific data in the response if the request is originated from web' do
      Organization.any_instance.stubs(:reporting_content_enabled?).returns(true)

      channel = create(:channel, curate_only: true, organization: @org)
      another_channel = create(:channel, curate_only: false, organization: @org)
      cards = create_list(:card, 2, organization_id: @org.id, author: @user)
      cards.each {|c| c.channels << [channel, another_channel]}
      channel.add_curators([@user])
      another_channel.add_curators([@user])

      get "#{@org.secure_home_page}/api/v2/channels/#{channel.id}/cards", {channel_card_state: 'new', ugc: false}, @headers.merge!('X-API-TOKEN' => @user.jwt_token)
      resp = get_json_response(response)

      expected_keys = %w[
        id title message card_type card_subtype slug state is_official provider
        provider_image readable_card_type share_url average_rating skill_level
        filestack votes_count comments_count published_at prices is_assigned
        is_bookmarked hidden is_public is_paid mark_feature_disabled_for_source
        completion_state is_upvoted all_ratings author is_reported is_clone created_at
      ]
      assert_same_elements expected_keys, resp['cards'][0].keys
    end

    test ':api should return subscription_end_date and due_at fields in response' do
      channel = create(:channel, organization: @org, curate_only: true)
      card = create(:card, organization_id: @org.id, author: @user)
      is_paid = card.is_paid_card && card.paid_by_user?(@user)
      channel.add_curators([@user])
      assignment = create(
        :assignment,
        assignable: card,
        assignee: @user,
        due_at: Date.parse("#{Date.current.year}/06/01")
      )
      card_subscription = create(
        :card_subscription,
        card_id: card.id,
        user_id: @user.id,
        end_date: Date.parse("#{Date.current.year}/06/01")
      )
      create :channels_card, channel: channel, card: card, state: 'curated'
      get "#{@org.secure_home_page}/api/v2/channels/#{channel.id}/cards",
          {fields: 'due_at,subscription_end_date,paid_by_user'},
          @headers.merge!('X-API-TOKEN' => @user.jwt_token)
      assert_response :ok
      resp = get_json_response(response)['cards'].first
      assert_equal assignment['due_at'], resp['due_at']
      assert_equal card_subscription['end_date'], resp['subscription_end_date']
      assert_equal is_paid, resp['paid_by_user']
    end
  end
end

require 'test_helper'

module RefinedResponse
  class TeamCardsTest < ActionDispatch::IntegrationTest
    setup do
      @org = create :organization
      @user = create :user, organization: @org
      @team = create :team, organization: @org
      @team.add_member @user
      @card = create :card, author: @user

      # Share a card to the group
      @shared_card = create :shared_card, team: @team, card: @card

      # Assign a card to the group
      @assignment = create(:assignment, assignee: @user, assignable: @card)
      create(:team_assignment, team_id: @team.id, assignment_id: @assignment.id)

      @headers = { 'Content-Type' => 'application/json', 'Accept' => 'application/json',
        'X-Edcast-JWT' => 1 }

      Organization.any_instance.stubs(:reporting_content_enabled?).returns(true)
    end

    # Tests for shared cards
    test 'teams#cards v2 shared cards returns all data in the response if the request is originated from EdCast app' do
      ActionDispatch::Request.any_instance.stubs(:user_agent).returns('EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0')

      get "#{@org.secure_home_page}/api/v2/teams/#{@team.id}/cards.json", {type: 'shared'}, @headers.merge!('X-API-TOKEN' => @user.jwt_token)
      json_response = get_json_response(response)
      response_keys = json_response['cards'].first.keys

      # Return expensive details
      %w(all_ratings auto_complete channel_ids channels completed_percentage completion_state created_at
        ecl_duration_metadata ecl_id file_resources is_official is_upvoted mentions
        non_curated_channel_ids paid_by_user payment_enabled tags teams updated_at user_rating
        user_taxonomy_topics users_with_access views_count voters).each do |field|
          assert_includes response_keys, field
      end
    end

    test 'teams#cards v2 shared cards returns all data in the response if the request is originated from web' do
      get "#{@org.secure_home_page}/api/v2/teams/#{@team.id}/cards.json", {type: 'shared'}, @headers.merge!('X-API-TOKEN' => @user.jwt_token)
      json_response = get_json_response(response)
      single_card_response = json_response['cards'].first

      # Returns default fields
      expected_keys = %w[
        id title message card_type card_subtype slug state is_official provider
        provider_image readable_card_type share_url average_rating skill_level
        filestack votes_count comments_count published_at prices is_assigned
        is_bookmarked is_reported hidden mark_feature_disabled_for_source author
        is_public is_paid  completion_state all_ratings is_upvoted is_clone created_at
      ]
      assert_same_elements expected_keys, single_card_response.keys

      # Includes author details
      assert_same_elements %w(id handle avatarimages full_name profile), single_card_response['author'].keys
    end

    # Tests for assigned cards
    test 'teams#cards v2 assigned returns all data in the response if the request is originated from Edcast app' do
      ActionDispatch::Request.any_instance.stubs(:user_agent).returns('EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0')

      get "#{@org.secure_home_page}/api/v2/teams/#{@team.id}/cards.json", {type: 'assigned'}, @headers.merge!('X-API-TOKEN' => @user.jwt_token)
      json_response = get_json_response(response)
      response_keys = json_response['cards'].first.keys

      # Return expensive details
      %w(all_ratings auto_complete channel_ids channels completed_percentage completion_state created_at
        ecl_duration_metadata ecl_id file_resources is_official is_upvoted mentions
        non_curated_channel_ids paid_by_user payment_enabled tags teams updated_at user_rating
        user_taxonomy_topics users_with_access views_count voters).each do |field|
          assert_includes response_keys, field
      end
    end

    test 'teams#cards v2 assigned cards returns all data in the response if the request is originated from web' do
      get "#{@org.secure_home_page}/api/v2/teams/#{@team.id}/cards.json", {type: 'assigned'}, @headers.merge!('X-API-TOKEN' => @user.jwt_token)
      json_response = get_json_response(response)
      single_card_response = json_response['cards'].first

      # Returns default fields
      expected_keys = %w[
        id title message card_type card_subtype slug state is_official provider
        provider_image readable_card_type share_url average_rating skill_level
        votes_count comments_count published_at prices is_assigned is_bookmarked
        is_public is_paid mark_feature_disabled_for_source author completion_state
        is_upvoted is_clone filestack is_reported hidden all_ratings created_at
      ]
      assert_same_elements expected_keys, single_card_response.keys

      # Includes author details
      assert_same_elements %w(id handle avatarimages full_name profile), single_card_response['author'].keys
    end

  end
end

require 'test_helper'

module RefinedResponse
  class UserDailyLearningsApiTest < ActionDispatch::IntegrationTest
    setup do
      Organization.any_instance.stubs(:get_settings_for).with('NumberOfDailyLearningItems').returns 5
      Organization.any_instance.stubs(:get_settings_for)
      .with('auto_mark_as_complete_scorm_cards').returns false
      Organization.any_instance.stubs(:get_settings_for)
      .with('reporting_content').returns false
      Organization.any_instance.stubs(:get_settings_for)
      .with('feature/shared_channels').returns false
      Organization.any_instance.stubs(:get_settings_for)
      .with('duration_format').returns nil

      @org = create(:organization)
      create(:config, name:'web_session_timeout', configable: @org, value: 10, data_type: 'integer', category: nil)
      @user = create(:user, organization: @org)
      @headers = { 'Content-Type' => 'application/json', 'Accept' => 'application/json', 'X-Edcast-JWT' => 1 }

      Organization.any_instance.stubs(:reporting_content_enabled?).returns(true)

      setup_search_user_daily_learnings
      Organization.any_instance.stubs(:current_todays_learning).returns('v2')
      Organization.any_instance.stubs(:web_session_timeout).returns(10)
      Organization.any_instance.stubs(:inactive_web_session_timeout).returns(10)
    end

    test 'user_daily_learnings#search_user_daily_learnings v2 returns all data in the response if the request is originated from EdCast app' do
      ActionDispatch::Request.any_instance.stubs(:user_agent).returns('EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0')
      Taxonomies.domain_topics.each_with_index do |topic, index|
        TodaysLearning::V2.any_instance.expects(:do_search).with(HashWithIndifferentAccess.new({q: "#{topic['topic_label']}",
                                             exclude_ecl_card_id: [], rank: false, load_tags: false,
                                             content_type: %w(pathway poll article video insight video_stream course)}))
                                       .returns([@cfd[index], nil])
      end
      get "#{@org.secure_home_page}/api/v2/user_daily_learnings/search_user_daily_learnings", {}, @headers.merge!('X-API-TOKEN' => @user.jwt_token)

      json_details = get_json_response(response)

      response_keys = json_details['cards'][0].keys
      assert_same_elements ['id', 'card_type', 'card_subtype', 'slug', 'state', 'is_official', 'is_public', 'created_at',
        'updated_at', 'published_at', 'hidden', 'ecl_id', 'provider', 'comments_count', 'votes_count',
        'readable_card_type', 'title', 'message', 'views_count', 'share_url', 'completed_percentage', 'mark_feature_disabled_for_source',
        'ecl_duration_metadata', 'voters', 'channel_ids', 'non_curated_channel_ids', 'user_rating', 'all_ratings',
        'average_rating', 'skill_level', 'is_upvoted', 'auto_complete', 'is_bookmarked', 'is_reported', 'is_assigned',
        'completion_state', 'filestack', 'mentions', 'author', 'tags', 'file_resources', 'channels', 'teams',
        'users_with_access', 'user_taxonomy_topics', 'paid_by_user', 'is_paid', 'payment_enabled', 'prices', 'language'], response_keys
    end

    test 'user_daily_learnings#search_user_daily_learnings v2 returns on-Demand data in the response if the request is originated from edc-web' do
      Taxonomies.domain_topics.each_with_index do |topic, index|
        TodaysLearning::V2.any_instance.expects(:do_search).with(HashWithIndifferentAccess.new({q: "#{topic['topic_label']}",
                                             exclude_ecl_card_id: [], rank: false, load_tags: false,
                                             content_type: %w(pathway poll article video insight video_stream course)}))
                                       .returns([@cfd[index], nil])
      end
      expected_keys = %w[
        id title message card_type card_subtype slug state provider provider_image
        readable_card_type share_url average_rating skill_level filestack
        is_official votes_count comments_count published_at prices is_assigned
        is_bookmarked hidden is_public mark_feature_disabled_for_source is_paid
        completion_state is_upvoted all_ratings is_reported author is_clone created_at
        paid_by_user due_at subscription_end_date
      ]
      fields = expected_keys.join(',')

      get "#{@org.secure_home_page}/api/v2/user_daily_learnings/search_user_daily_learnings", {fields: fields}, @headers.merge!('X-API-TOKEN' => @user.jwt_token)

      json_details = get_json_response(response)
      response_keys = json_details['cards'][0].keys
      assert_same_elements expected_keys, response_keys
    end

    private

    def setup_search_user_daily_learnings
      @profile = create(:user_profile, user: @user, learning_topics: Taxonomies.domain_topics)

      # Create cards for each of the domain
      @author = create(:user, organization: @org)
      @cfd = Taxonomies.domain_topics.map do |topic|
        (1..5).map do
          create(:card, author: @author, ecl_id: SecureRandom.hex)
        end
      end
    end
  end
end

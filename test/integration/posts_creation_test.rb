require 'test_helper'

class PostsCreationTest < ActionDispatch::IntegrationTest

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'

    create_default_ssos
    Organization.any_instance.unstub :default_sso
    create_default_org
    stub_request(:get, "https://graph.facebook.com/app").
        with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Bearer xxx123', 'User-Agent'=>/.*/}).
        to_return(:status => 200,
                  :headers => {"Content-Type" => "application/json"},
                  :body => {"category" => "Education",
                            "daily_active_users" => "0",
                            "daily_active_users_rank" => 945908,
                            "icon_url" => "https:\/\/fbstatic-a.akamaihd.net\/rsrc.php\/v2\/yE\/r\/7Sq7wKJHi_5.png",
                            "link" => "https:\/\/www.facebook.com\/appcenter\/?app_id=627617180664755",
                            "logo_url" => "https:\/\/fbcdn-photos-c-a.akamaihd.net\/hphotos-ak-xpa1\/t39.2081-0\/p75x75\/851578_455087414601994_1601110696_n.png",
                            "monthly_active_users" => "5",
                            "monthly_active_users_rank" => 540079,
                            "name" => "edCast",
                            "weekly_active_users" => "1",
                            "id" => "627617180664755"}.to_json)

    stub_request(:get, "https://graph.facebook.com/me").
        with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Bearer xxx123', 'User-Agent'=>/.*/}).
        to_return(:status => 200,
                  :headers => {"Content-Type" => "application/json"},
                  :body => {"id" => "649815170",
                            "email" => "bharathpbhat41@gmail.com",
                            "first_name" => "Bharath",
                            "gender" => "male",
                            "hometown" => {
                              "id" => "110015012354282",
                              "name" => "Shimoga, Karnataka"
                            },
                            "last_name" => "Bhat",
                            "link" => "https:\/\/www.facebook.com\/bharathpbhat",
                            "location" => {
                              "id" => "103701963002297",
                              "name" => "Stanford, California"
                            },
                            "locale" => "en_US",
                            "name" => "Bharath Bhat",
                            "timezone" => -7,
                            "updated_time" => "2014-04-23T18:24:30+0000",
                            "username" => "bharathpbhat",
                            "verified" => true}.to_json)
    ActionController::Base.allow_forgery_protection = true
    WebMock.allow_net_connect!
  end

  teardown do
    WebMock.disable_net_connect!
    ActionController::Base.allow_forgery_protection = false
  end

  test 'existing user should just be logged in' do
    user = create(:user, email: 'bharathpbhat41@gmail.com', organization: Organization.default_org)
    idp  = create :identity_provider, :facebook, uid: '649815170', token: 'xxx123', user: user
    group = create(:group)
    group.add_member user
    # log the user in
    post '/auth/facebook_access_token/callback', {access_token: 'xxx123', provider: :facebook}, {'User-Agent' => 'EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0', 'X-Build-Number' => '42'}
    resp = ActiveSupport::JSON.decode(response.body)
    assert_response :success

    csrf_param = resp['csrfParam']
    csrf_token = resp['csrfToken']

    pars = {
      title: 'title',
      message: 'message',
      type: 'Question'
    }

    # non edcast user agent
    assert_no_difference -> {Post.count} do
      post "/groups/#{group.id}/posts", pars.to_json,
        {'Content-Type' => 'application/json', 'Accept' => 'application/json', 'User-Agent' => 'Mozilla/5.0'}
      assert_response :unauthorized
    end

    # ios
    assert_difference -> {Post.count} do
      post "/groups/#{group.id}/posts", pars.to_json,
        {'Content-Type' => 'application/json', 'Accept' => 'application/json', 'User-Agent' => 'EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0', 'X-Build-Number' => '42'}
      assert_response :created
    end

    # android
    assert_difference -> {Post.count} do
      post "/groups/#{group.id}/posts", pars.to_json,
        {'Content-Type' => 'application/json', 'Accept' => 'application/json', 'User-Agent' => 'Edcast-Android', 'X-Build-Number' => '31'}
      assert_response :created
    end

    # non edcast user agent with csrf param
    assert_difference -> {Post.count} do
      post "/groups/#{group.id}/posts", pars.merge({csrf_param => csrf_token}).to_json,
        {'Content-Type' => 'application/json', 'Accept' => 'application/json', 'User-Agent' => 'Mozilla/5.0'}
      assert_response :created
    end
  end
end

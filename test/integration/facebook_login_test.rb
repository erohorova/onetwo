require 'test_helper'

class FacebookLoginTest < ActionDispatch::IntegrationTest

  setup do
    Search::UserSearch.any_instance.stubs :search_by_id
    create_default_ssos
    Organization.any_instance.unstub :default_sso
    create_default_org
    @uid = "1234"
    stub_request(:get, "https://graph.facebook.com/app").
        with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Bearer xxx123', 'User-Agent'=>/.*/}).
        to_return(:status => 200,
                  :headers => {"Content-Type" => "application/json"},
                  :body => {"category" => "Education",
                            "daily_active_users" => "0",
                            "daily_active_users_rank" => 945908,
                            "icon_url" => "https:\/\/fbstatic-a.akamaihd.net\/rsrc.php\/v2\/yE\/r\/7Sq7wKJHi_5.png",
                            "link" => "https:\/\/www.facebook.com\/appcenter\/?app_id=627617180664755",
                            "logo_url" => "https:\/\/fbcdn-photos-c-a.akamaihd.net\/hphotos-ak-xpa1\/t39.2081-0\/p75x75\/851578_455087414601994_1601110696_n.png",
                            "monthly_active_users" => "5",
                            "monthly_active_users_rank" => 540079,
                            "name" => "edCast",
                            "weekly_active_users" => "1",
                            "id" => "627617180664755"}.to_json)

    stub_request(:get, "https://graph.facebook.com/me?fields=name,email,first_name,last_name").
        with(:headers => {
            'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Bearer xxx123', 'User-Agent'=>/.*/}).
        to_return(:status => 200,
                  :headers => {"Content-Type" => "application/json"},
                  :body => {"id" => @uid,
                            "email" => "bharathpbhat41@gmail.com",
                            "first_name" => "Bharath",
                            "gender" => "male",
                            "hometown" => {
                              "id" => "110015012354282",
                              "name" => "Shimoga, Karnataka"
                            },
                            "last_name" => "Bhat",
                            "link" => "https:\/\/www.facebook.com\/bharathpbhat",
                            "location" => {
                              "id" => "103701963002297",
                              "name" => "Stanford, California"
                            },
                            "locale" => "en_US",
                            "name" => "Bharath Bhat",
                            "timezone" => -7,
                            "updated_time" => "2014-04-23T18:24:30+0000",
                            "username" => "bharathpbhat",
                            "verified" => true}.to_json)
    @expected_response = {
        'email' => 'bharathpbhat41@gmail.com',
        'name' => 'Bharath Bhat',
        'firstName' => 'Bharath',
        'lastName' => 'Bhat',
        'bio' => nil,
        'followingUsers' => {'count' => 0, 'users' => []},
        'followingChannels' => [],
        'picture' => "http://graph.facebook.com/#{@uid}/picture?width=512",
        "coverimages" => {
            "banner_url"=>"http://cdn-test-host.com/assets/default_banner_user_image.png",
            "profile_url"=>"http://cdn-test-host.com/assets/default_banner_user_image.png",
            "medium_url"=>"http://cdn-test-host.com/assets/default_banner_user_image.png",
            "large_url"=>"http://cdn-test-host.com/assets/default_banner_user_image.png"
        },
        'newsletterOptIn' => false,
        'writableChannels' => [],
        'followingCount' => 0,
        'followersCount' => 0,
        'fullName' => 'Bharath Bhat',
        'roles' => ['member'],
        'rolesDefaultNames' => ['member'],
        "permissions"=>[
            "GET_LEADERBOARD_INFORMATION", "USER_OPT_OUT_OF_LEADERBOARD", "MANAGE_CARD", "VIEW_CARD_ANALYTICS", "UPLOAD", 
            "CREATE_LIVESTREAM", "PUBLISH_LINKS", "CHANGE_AUTHOR", "UPLOAD_CONTENT_COVER_IMAGES", "MARK_AS_PRIVATE", "CREATE_TEXT_CARD", "CREATE_COMMENT", "LIKE_CONTENT", 
            "BOOKMARK_CONTENT", "MARK_AS_COMPLETE", "CREATE_LEAP", "UPDATE_LEAP", "DELETE_LEAP", "ASSIGN_CONTENT", "DISMISS_CONTENT",
            "ADD_TO_PATHWAY", "SHARE", "ENABLE_EDGRAPH", "USE_DYNAMIC_SELECTION"
        ],
        'isAdmin' => false,
        'isSuperAdmin'=>false,
        'signInCount' => 1,
        'ssoInfo' => nil,
        'passwordResetRequired' => false,
        'onboardingOptions' => { 'profile_setup' => 1 },
        'onboardingStepsNames' => ['profile_setup'],
        'userSubscriptions' => [],
        'orgAnnualSubscriptionPaid' => false,
        'defaultTeamId' => nil,
        'hideFromLeaderboard' => nil
    }
  end

  test 'create new user on facebook login' do
    Role.any_instance.unstub :create_role_permissions
    Role.create_master_roles(Organization.default_org)
    User.any_instance.unstub :update_role
    assert_difference [-> {User.count}, -> {FacebookProvider.count}] do
      post '/auth/facebook_access_token/callback', {access_token: 'xxx123', token_provider: :facebook}, {'User-Agent' => 'EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0', 'X-Build-Number' => '42'}
      assert_response :success
      resp = ActiveSupport::JSON.decode(response.body)
      assert_equal @expected_response.merge({'id' => User.last.id, 'isSuspended' => false, 'groups' => [],
                                             'profile' => nil, 'currentSessionProvider' => 'signup.facebook',
                                             'userSubscriptions' => [], 'orgAnnualSubscriptionPaid' => false
                                            }), resp.except('csrfParam', 'csrfToken', 'handle', 'pubnubChannels', 'organization', 'activeOrgs', 'jwtToken', 'countryCode')
      assert_equal User.last.organization_id, resp['organization']['id']
    end

    user = User.find_by(email: 'bharathpbhat41@gmail.com')
    assert_not_nil user
    assert_equal @controller.current_user, user

    fb = FacebookProvider.last
    assert_equal fb.token, 'xxx123'
    assert_equal fb.user, user
  end

  test 'facebook login to custom orgs' do
    org = create(:organization, host_name: 'ge', is_open: true)
    assert_difference [-> {User.count}, -> {FacebookProvider.count}] do
      post "#{org.home_page}/auth/facebook_access_token/callback", {access_token: 'xxx123', token_provider: :facebook}, {'User-Agent' => 'EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0', 'X-Build-Number' => '42'}
      assert_response :success
      resp = ActiveSupport::JSON.decode(response.body)

      assert_equal User.last.id, resp['id']
      assert_equal org.id, resp['organization']['id']
    end

    user = User.find_by(email: 'bharathpbhat41@gmail.com')
    assert_not_nil user
    assert_equal org, user.organization
    assert_equal @controller.current_user, user

    fb = FacebookProvider.last
    assert_equal fb.token, 'xxx123'
    assert_equal fb.user, user
  end

  test 'existing user should just be logged in' do
    user = create(:user, email: 'bharathpbhat41@gmail.com', organization: Organization.default_org)
    idp  = create :identity_provider, :facebook, uid: @uid, token: 'xxx123', user: user

    assert_no_difference [ -> {User.count} ] do
      post '/auth/facebook_access_token/callback', {access_token: 'xxx123', provider: :facebook}, {'User-Agent' => 'EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0', 'X-Build-Number' => '42'}
      assert_response :success
    end

    fb = FacebookProvider.where(user: user).first
    assert_equal fb.token, 'xxx123'

    assert_equal @controller.current_user, user
  end

  test 'suspended user should not be logged in when email present' do
    user = create(:user, email: 'bharathpbhat41@gmail.com', is_suspended: true, organization: Organization.default_org)
    create :identity_provider, :facebook, user: user, token: 'xxx123', auth: true, uid: @uid

    assert_no_difference [->{User.count}, ->{FacebookProvider.count}] do
      post '/auth/facebook_access_token/callback', {access_token: 'xxx123', provider: :facebook}, {'User-Agent' => 'EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0', 'X-Build-Number' => '42'}
      assert_response :forbidden
    end
  end

  test 'suspended user should not be logged in when id provider already present' do
    user = create(:user, email: 'bharathpbhat41@gmail.com', is_suspended: true, organization: Organization.default_org)
    create(:identity_provider, type: "FacebookProvider", user: user, token: 'xxx123', auth: true, uid: @uid)

    assert_no_difference [->{User.count}, ->{FacebookProvider.count}] do
      post '/auth/facebook_access_token/callback', {access_token: 'xxx123', provider: :facebook}, {'User-Agent' => 'EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0', 'X-Build-Number' => '42'}
      assert_response :forbidden
    end
  end
end

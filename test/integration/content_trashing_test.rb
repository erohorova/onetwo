require 'test_helper'
class ContentTrashingTest < ActionDispatch::IntegrationTest
  setup do
    @organization = create(:organization)
    @user = create(:user, organization: @organization, organization_role: 'admin')
    @author = create(:user, organization: @organization)
    @card = create(:card, author: @author)
    @header = { 'Content-Type' => 'application/json', 'Accept' => 'application/json', 'X-Edcast-JWT' => 1 }
  end

  test 'should not return trashed bookmarks' do
    create(:bookmark, user: @user, bookmarkable: @card)
    create(:learning_queue_item, :from_bookmarks, queueable: @card, user: @user)

    # Before trashing
    get "#{@organization.home_page}/api/v2/learning_queue_items", { include_queueable_details: 'true', source: ['bookmarks']}, @header.merge!('X-API-TOKEN' => @user.jwt_token)
    resp = OpenStruct.new get_json_response(response)
    assert_includes resp['learning_queue_items'].map{|d| d['queueable']['id'].to_i}, @card.id

    # Trash bookmarked card
    post "#{@organization.home_page}/api/v2/cards/#{@card.id}/trash", {}, @header.merge!('X-API-TOKEN' => @user.jwt_token)

    # Request for bookmarks
    get "#{@organization.home_page}/api/v2/learning_queue_items", { include_queueable_details: 'true', source: ['bookmarks']}, @header.merge!('X-API-TOKEN' => @user.jwt_token)
    resp = OpenStruct.new get_json_response(response)
    assert_equal [nil], resp['learning_queue_items'].map{|d| d['queueable']}.uniq
  end

  test 'should not return trashed assignments' do
    create(:assignment, assignable: @card, assignee: @user)
    # Before trashing
    get "#{@organization.home_page}/api/v2/assignments", {}, @header.merge!('X-API-TOKEN' => @user.jwt_token)
    resp = OpenStruct.new get_json_response(response)
    assert_equal 1, resp['assignments'].size

    # Trash bookmarked card
    post "#{@organization.home_page}/api/v2/cards/#{@card.id}/trash", {}, @header.merge!('X-API-TOKEN' => @user.jwt_token)

    # Request for bookmarks
    get "#{@organization.home_page}/api/v2/assignments", {}, @header.merge!('X-API-TOKEN' => @user.jwt_token)
    resp = OpenStruct.new get_json_response(response)
    assert_equal 0, resp['assignments'].size
  end
end

require 'test_helper'

class UserUnwrappedParamsPasswordUpdateTest < ActionDispatch::IntegrationTest
  # setup do
  #   ActionController::Base.allow_forgery_protection = true
  #   WebMock.disable!
  # end

  # teardown do
  #   WebMock.disable_net_connect!
  #   ActionController::Base.allow_forgery_protection = false
  # end

  test 'should update password when called with unwrapped params' do
    skip
    create_default_org
    User.any_instance.expects(:reindex_async).returns(nil).once
    user = create(:user, handle: '@edcast-ish', organization: Organization.default_org)
    password = User.random_password
    patch "/users/#{user.id}", {
        first_name: 'ed',
        last_name: 'castro',
        handle: '@edcast-ish-2',
        password: password
      }.to_json , {
        'Content-Type' => 'application/json',
        'Accept' => 'application/json',
        'User-Agent' => 'EdCast Android',
        'X-Build-Number' => '79',
        'X-Edcast-JWT' => 1,
         'X-API-TOKEN' => user.jwt_token
      }
    assert_response :success
    resp = get_json_response(response)
    user.reload
    assert_equal 'ed', user.first_name
    assert_equal 'castro', user.last_name
    assert_equal '@edcast-ish-2', user.handle
    assert user.valid_password?(password)
  end
end

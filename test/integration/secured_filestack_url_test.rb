require 'test_helper'

class SecuredFilestackUrltest < ActionDispatch::IntegrationTest
  setup do
    # MANDATORY:
    Settings.stubs(:serve_authenticated_images).returns('true')

    @org   = create :organization
    @admin = create :user, organization: @org, organization_role: 'admin'
    @member = create :user, organization: @org, organization_role: 'member'

    @headers = { 'Content-Type' => 'application/json', 'Accept' => 'application/json', 'X-Edcast-JWT' => 1 }

    @filestack = [
      {
        'name'     => "Presentation1.pptx",
        'mimetype' => "application/vnd.openxmlformats-officedocument.presentationml.presentation",
        'size'     => 701380,
        'source'   => "local_file_system",
        'url'      => "https://cdn.filestackcontent.com/Ct0DbtZ0TtmzI5AHv8Yh",
        'handle'   => "Ct0DbtZ0TtmzI5AHv8Yh",
        'status'   => "Stored"
      }
    ]
  end

  test 'returns authenticated url when a card is created' do
    open_session do |sess|
      sess.post "#{@org.secure_home_page}/api/v2/cards", { card: { message: Faker::Lorem.sentence, filestack: @filestack } }.to_json, @headers.merge!('X-API-TOKEN' => @admin.jwt_token)

      card_response = get_json_response(sess.response)

      assert_includes card_response['filestack'][0]['url'], '/uploads'
    end
  end

  test 'returns authenticated url when a card is updated' do
    card = create :card, organization: @org, author_id: @admin, filestack: @filestack

    @filestack[0].merge!(url: "https://cdn.filestackcontent.com/Ct0DbtZ0T")

    open_session do |sess|
      sess.put "#{@org.secure_home_page}/api/v2/cards/#{card.id}.json", { card: { message: Faker::Lorem.sentence, filestack: @filestack } }.to_json, @headers.merge!('X-API-TOKEN' => @admin.jwt_token)

      card_response = get_json_response(sess.response)

      assert_includes card_response['filestack'][0]['url'], '/uploads'
    end
  end

  test 'returns authenticated url when an image of an article is updated' do
    resource = create :resource, url: 'http://example.com/this-is-a-dummy-url'
    card = create :card, organization: @org, author_id: @admin, card_type: 'media', card_subtype: 'link', resource_id: resource.id

    open_session do |sess|
      sess.put "#{@org.secure_home_page}/api/v2/cards/#{card.id}.json", { card: { filestack: @filestack } }.to_json, @headers.merge!('X-API-TOKEN' => @admin.jwt_token)

      card_response = get_json_response(sess.response)

      assert_includes card_response['filestack'][0]['url'], '/uploads'
    end
  end

  test 'returns authenticated url when an image is added to poll card' do
    open_session do |sess|
      sess.post "#{@org.secure_home_page}/api/v2/cards",
        { card: { message: 'Is this a poll?', options: [ { label: 'yes' }, { label: 'no' } ], filestack: @filestack } }.to_json,
        @headers.merge!('X-API-TOKEN' => @admin.jwt_token)

      card_response = get_json_response(sess.response)

      assert_equal 'poll', card_response['card_type']
      assert_equal 'text', card_response['card_subtype']
      assert_includes card_response['filestack'][0]['url'], '/uploads'
    end
  end

  test 'returns authenticated url when an image is added to text card' do
    open_session do |sess|
      sess.post "#{@org.secure_home_page}/api/v2/cards",
        { card: { message: 'this is a text card', filestack: @filestack } }.to_json,
        @headers.merge!('X-API-TOKEN' => @admin.jwt_token)

      card_response = get_json_response(sess.response)

      assert_equal 'media', card_response['card_type']
      assert_equal 'text', card_response['card_subtype']
      assert_includes card_response['filestack'][0]['url'], '/uploads'
    end
  end

  test 'bookmarked cards return authenticated url' do
    # article card with thumbnail
    resource = create :resource, url: 'http://example.com/this-is-a-dummy-url'
    article = create :card, organization: @org, author_id: @admin, card_type: 'media', card_subtype: 'link', resource_id: resource.id, filestack: @filestack

    # quiz card with image
    quiz = create :card, message: 'quiz card', filestack: @filestack, card_type: 'poll', card_subtype: 'text', author_id: @admin.id
    quiz_option1 = create(:quiz_question_option, label: 'opt 1', is_correct: false, quiz_question: quiz)
    quiz_option2 = create(:quiz_question_option, label: 'opt 2', is_correct: true, quiz_question: quiz)

    # text card with no image
    text = create :card, message: 'this is a text card', card_type: 'media', card_subtype: 'text', author_id: @admin.id

    create :learning_queue_item, user_id: @member.id, queueable: article, source: 'bookmarks'
    create :learning_queue_item, user_id: @member.id, queueable: quiz, source: 'bookmarks'
    create :learning_queue_item, user_id: @member.id, queueable: text, source: 'bookmarks'

    create :bookmark, user_id: @member.id, bookmarkable: article
    create :bookmark, user_id: @member.id, bookmarkable: quiz
    create :bookmark, user_id: @member.id, bookmarkable: text

    open_session do |sess|
      sess.get "#{@org.secure_home_page}/api/v2/learning_queue_items",
        { source: ['bookmarks'], include_queueable_details: 'true' }.to_json,
        @headers.merge!('X-API-TOKEN' => @member.jwt_token)

      card_response = get_json_response(sess.response)['learning_queue_items']
      article_response = card_response.select { |c| c['queueable']['id'] == article.id.to_s }[0]['queueable']
      quiz_response = card_response.select { |c| c['queueable']['id'] == quiz.id.to_s }[0]['queueable']
      text_response = card_response.select { |c| c['queueable']['id'] == text.id.to_s }[0]['queueable']

      assert_includes article_response['filestack'][0]['url'], '/uploads'
      assert_includes quiz_response['filestack'][0]['url'], '/uploads'
      assert_empty text_response['filestack']
    end
  end

  test 'assigned cards return authenticated url' do
    # article card with thumbnail
    resource = create :resource, url: 'http://example.com/this-is-a-dummy-url'
    article = create :card, organization: @org, author_id: @admin, card_type: 'media', card_subtype: 'link', resource_id: resource.id, filestack: @filestack

    # quiz card with image
    quiz = create :card, message: 'quiz card', filestack: @filestack, card_type: 'poll', card_subtype: 'text', author_id: @admin.id
    quiz_option1 = create(:quiz_question_option, label: 'opt 1', is_correct: false, quiz_question: quiz)
    quiz_option2 = create(:quiz_question_option, label: 'opt 2', is_correct: true, quiz_question: quiz)

    # text card with no image
    text = create :card, message: 'this is a text card', card_type: 'media', card_subtype: 'text', author_id: @admin.id

    create :learning_queue_item, user_id: @member.id, queueable: article, source: 'assignments'
    create :learning_queue_item, user_id: @member.id, queueable: quiz, source: 'assignments'
    create :learning_queue_item, user_id: @member.id, queueable: text, source: 'assignments'

    create :assignment, user_id: @member.id, assignable: article
    create :assignment, user_id: @member.id, assignable: quiz
    create :assignment, user_id: @member.id, assignable: text

    open_session do |sess|
      sess.get "#{@org.secure_home_page}/api/v2/learning_queue_items",
        { source: ['assignments'], include_queueable_details: 'true' }.to_json,
        @headers.merge!('X-API-TOKEN' => @member.jwt_token)

      card_response = get_json_response(sess.response)['learning_queue_items']
      article_response = card_response.select { |c| c['queueable']['id'] == article.id.to_s }[0]['queueable']
      quiz_response = card_response.select { |c| c['queueable']['id'] == quiz.id.to_s }[0]['queueable']
      text_response = card_response.select { |c| c['queueable']['id'] == text.id.to_s }[0]['queueable']

      assert_includes article_response['filestack'][0]['url'], '/uploads'
      assert_includes quiz_response['filestack'][0]['url'], '/uploads'
      assert_empty text_response['filestack']
    end
  end

  test 'cards #index return authenticated url' do
    # article card with thumbnail
    resource = create :resource, url: 'http://example.com/this-is-a-dummy-url'
    article = create :card, organization: @org, author_id: @admin, card_type: 'media', card_subtype: 'link', resource_id: resource.id, filestack: @filestack

    # quiz card with image
    quiz = create :card, message: 'quiz card', filestack: @filestack, card_type: 'poll', card_subtype: 'text', author_id: @admin.id
    quiz_option1 = create(:quiz_question_option, label: 'opt 1', is_correct: false, quiz_question: quiz)
    quiz_option2 = create(:quiz_question_option, label: 'opt 2', is_correct: true, quiz_question: quiz)

    # text card with no image
    text = create :card, message: 'this is a text card', card_type: 'media', card_subtype: 'text', author_id: @admin.id

    open_session do |sess|
      sess.get "#{@org.secure_home_page}/api/v2/cards",
        { state: 'active' }.to_json,
        @headers.merge!('X-API-TOKEN' => @admin.jwt_token)

      card_response = get_json_response(sess.response)['cards']
      article_response = card_response.select { |c| c['id'] == article.id.to_s }[0]
      quiz_response = card_response.select { |c| c['id'] == quiz.id.to_s }[0]
      text_response = card_response.select { |c| c['id'] == text.id.to_s }[0]

      assert_includes article_response['filestack'][0]['url'], '/uploads'
      assert_includes quiz_response['filestack'][0]['url'], '/uploads'
      assert_empty text_response['filestack']
    end
  end

  test 'filestack_security#signed_url returns authenticated URL' do
    open_session do |sess|
      sess.get "#{@org.secure_home_page}/api/v2/filestack/signed_url?url=#{@filestack[0]['url']}",
        {}.to_json,
        @headers.merge!('X-API-TOKEN' => @admin.jwt_token)

      resp = JSON.parse(sess.response.body)
      url = resp["signed_url"]

      assert_includes url, "/uploads"
    end
  end
end

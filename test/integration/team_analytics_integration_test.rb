require 'test_helper'

class TeamAnalyticsIntegrationTest < ActionDispatch::IntegrationTest
  setup do
    skip
    applicable_offsets = PeriodOffsetCalculator.applicable_offsets(2.days.ago)
    @org = create(:organization)
    @team = create(:team, organization: @org)

    admin = create(:user, organization: @org, organization_role: 'admin')

    card =  create(:card, author: admin)
    create(:user_level_metric, user_id: admin.id, smartbites_score: 50, smartbites_consumed: 1, smartbites_created: 10, time_spent: 2, period: :year, offset: applicable_offsets[:year])
    create(:clc_organizations_record, user_id: admin.id, organization_id: @org.id, score: 3, card_id: card.id, created_at: 3.days.ago)

    @team.add_admin(admin)

    # total smartbites_consumed = 10, smartbites_created = 5, time_spent = 100, smartbite_score = 50, clc_score = 25
    create_list(:user, 5, organization: @org).each do |user|
      @team.add_member(user)
      create(:user_level_metric, user_id: user.id, smartbites_score: 10, smartbites_consumed: 2, smartbites_created: 1, time_spent: 20, period: :year, offset: applicable_offsets[:year])
      create(:clc_organizations_record, user_id: user.id, organization_id: @org.id, score: 5, card_id: card.id, created_at: 2.days.ago)
    end

    @headers = { 'Content-Type' => 'application/json', 'Accept' => 'application/json', 'X-Edcast-JWT' => 1, 'X-API-TOKEN' => admin.jwt_token }

  end

  test 'Only team or org admin will be able to access the metric api' do
    from_date = (Time.now.utc - 5.days).strftime("%m/%d/%Y")
    to_date = DateTime.tomorrow.strftime("%m/%d/%Y")

    get "#{@org.home_page}/api/v2/organizations/metrics", {organization_id: @org.id, team_id: @team.id, from_date: from_date, to_date: to_date }, @headers
    assert_response :success


    # non team admins can't access this api
    get "#{@org.home_page}/api/v2/organizations/metrics", {organization_id: @org.id, team_id: @team.id, from_date: from_date, to_date: to_date },
        { 'Content-Type' => 'application/json', 'Accept' => 'application/json', 'X-Edcast-JWT' => 1, 'X-API-TOKEN' => @team.members.first.jwt_token }
    assert_response :unauthorized
  end

  test 'metric api response' do
    from_date = (1.year.ago).strftime("%m/%d/%Y")
    to_date = DateTime.tomorrow.strftime("%m/%d/%Y")
    get "#{@org.home_page}/api/v2/organizations/metrics", {organization_id: @org.id, team_id: @team.id, from_date: from_date, to_date: to_date, period: 'year'}, @headers

    resp =  get_json_response(response)
    assert_response :ok
    assert_equal ["active_users", "average_session", "clc_progress", "engagement_index", "new_users", "smartbites_consumed", "smartbites_created", "total_users"].sort, resp.keys.sort
    assert_equal 6, resp['new_users']['value'] # 5 members + 1 admin
    assert_equal 6, resp['active_users']['value'] # 5 members + 1 admin
    assert_equal 6, resp['total_users']['value'] # 5 members + 1 admin
    assert_equal 11, resp['smartbites_consumed']['value'] # 2*5(members) + 1(admin)
    assert_equal 15, resp['smartbites_created']['value'] # 1*5(members) + 10(admin)
    assert_equal 17, resp['average_session']['value'] # (100(members) + 2(admin))/6
    assert_equal 100, resp['engagement_index']['value'] # all 6 users are active, so 100% engagement
    assert_equal 13, resp['clc_progress']['value'] # clc calculated from 5.days.ago to one day after -> 7 days
  end
end
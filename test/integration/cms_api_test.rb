require 'test_helper'

class CmsApiTest < ActionDispatch::IntegrationTest
  test 'cms api #card routes' do
    assert_routing({path: 'cms/api/cards',     method: 'get'   }, controller: 'cms/api/cards', action: 'index', format: :json)
    assert_routing({path: 'cms/api/cards/1',   method: 'put'   }, controller: 'cms/api/cards', action: 'update', id: '1', format: :json)

    assert_routing({path: 'cms/api/cards/publish',   method: 'post'  }, controller: 'cms/api/cards', action: 'publish', format: :json)
    assert_routing({path: 'cms/api/cards/archive',   method: 'post'  }, controller: 'cms/api/cards', action: 'archive', format: :json)
    assert_routing({path: 'cms/api/cards/delete',    method: 'post'  }, controller: 'cms/api/cards', action: 'delete', format: :json)
  end

  test 'cms api suggest' do
    assert_routing({path: 'cms/api/topics/suggest',   method: 'get'   }, controller: 'cms/api/topics',    action: 'suggest', format: :json)
  end

  test 'cms api miscellaneous' do
    assert_routing({path: 'cms/unknown',   method: 'get'   }, controller: 'cms/base', action: 'home', other: 'unknown')
  end

  test 'cms pathways api' do
    assert_routing({path: 'cms/api/collection', method: 'get' }, controller: 'cms/api/card_packs',  action: 'index', format: :json)
    assert_routing({path: 'cms/api/collection/2', method: 'put' }, controller: 'cms/api/card_packs',  action: 'update', id: '2', format: :json)
    assert_routing({path: 'cms/api/collection/publish', method: 'post' }, controller: 'cms/api/card_packs',  action: 'publish', format: :json)
    assert_routing({path: 'cms/api/collection/delete', method: 'post' }, controller: 'cms/api/card_packs',  action: 'delete', format: :json)
  end
end

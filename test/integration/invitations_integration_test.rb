require 'test_helper'

class IntegrationLoginTest < ActionDispatch::IntegrationTest
  setup do
    stub_request(:post, "http://localhost:9200/_bulk")
      .to_return(status: 200, body: '', headers: {})
    stub_request(:post, 'https://api.branch.io/v1/url')
      .to_return(status: 200, body: '', headers: {})

    @org     = create :organization
    @admin_user = create :user, organization: @org, organization_role: 'admin'
    @channel = create :channel, organization: @org
    @team    = create :team, organization: @org

    @invitation = create :invitation,
      recipient_email: 'xmen@wolverine.com', first_name: 'Xmen',
      last_name: 'Wolverine', invitable: @org, sender: @admin_user,
      authenticate_recipient: true,
      parameters: { channel_ids: [@channel.id], team_ids: [@team.id] }

    @saml = create(:sso, name: 'saml')
    @org_sso = create(:org_sso, organization: @org, sso: @saml,
                                position: 0,
                                saml_issuer: 'edcast',
                                saml_allowed_clock_drift: 10.years.to_i,
                                saml_idp_sso_target_url: 'http://example.com/login',
                                saml_idp_cert: '-----BEGIN CERTIFICATE-----
MIIDSjCCAjKgAwIBAgIGAVJrq3O0MA0GCSqGSIb3DQEBCwUAMGYxCzAJBgNVBAYTAlVTMQswCQYD
VQQIEwJDTzEPMA0GA1UEBxMGRGVudmVyMRYwFAYDVQQKEw1QaW5nIElkZW50aXR5MSEwHwYDVQQD
ExhodHRwc3BpbmdvbmVjb21pZHBlZGNhc3QwHhcNMTYwMTIyMjMyOTA0WhcNMTkwMTIxMjMyOTA0
WjBmMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ08xDzANBgNVBAcTBkRlbnZlcjEWMBQGA1UEChMN
UGluZyBJZGVudGl0eTEhMB8GA1UEAxMYaHR0cHNwaW5nb25lY29taWRwZWRjYXN0MIIBIjANBgkq
hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkVv6Zn7fybsUp9IZXHshkOIrnrdVyB36mvuXwypLJDJq
zNC3sxf8Eh6/OuOJr/x2MFylB78m1SRjR3gnDyPByptBOrBSNvJOqlx9RqJUODPSKDpfKmSDwjWw
DwfDhJMZSzKKdM1UIjljiY8iNYLIZk/YIO/ifh/8+yRtk+fk2Hxstg4sycuj0Zj8YV6HGgOLkjkj
WrbCvardEf9Q12SuyAAj5BdZuggczq0JgSwAee1TFndHAMt8sdNWwb8bAoRp9q+WhQlPFlCTBm/g
PX8dclv0iw2rths0+0V4zIczBh7coPEQmrnnWLhRmmsInhdaooNYczpKiYxnM0pWHqBweQIDAQAB
MA0GCSqGSIb3DQEBCwUAA4IBAQAeaPjNMkb2n/iV5mzbmNA/rXUoPmJHdWifi8fGBhbYtTLIdhsH
9v1HA7Ku1ihhWvL73xyXg77QcT+gLZtcxBc/ZMMW/k5Xd8PVpr21dUIHdX++8kwIDKLNKT/PEg5k
WpQWTF6EbOjMEuWNiV8CB14GxaUQGGFpovuWO66bof8dik1+94GHNpIdDHUQOJgyidpEIEJ+q6Th
oD8mcvSWaHpaAsnRw/84sG0FrhZeIrN5cciE2aEZ92BKtG7bB6or0yVwzy8LIH/UO2WJVdCSvLk5
S4gWvDaRO1vwiLS3PTQtM/RzTDN/MlaPRFuLiCvrl1BOfTRQ3HqFNAQ0n1OWq1ls
-----END CERTIFICATE-----')

    stub_ip_lookup(ip_address: "127.0.0.1")
  end

  test 'redirects to sign in page when `authenticate_recipient` is set to TRUE' do
    open_session do |s|
      post "#{@org.home_page}/join/#{@invitation.token}"

      assert_redirected_to root_path(host: @org.home_page)
    end
  end

  test 'accepts invitation when `authenticate_recipient` is set to FALSE' do
    @invitation.update(authenticate_recipient: false)

    open_session do |s|
      post "#{@org.home_page}/join/#{@invitation.token}"

      assert_redirected_to root_path
      assert @invitation.reload.accepted_at
      assert User.find_by(email: 'xmen@wolverine.com', organization: @org).id, controller.current_user.id
    end
  end

  test 'accepts invitation by authenticating recipient with different `recipient_email` for existing user' do
    TestAfterCommit.enabled = true

    user = create :user, organization: @org, email: 'shirish@edcast.com'
    create :identity_provider, user: user, uid: 'shirish@edcast.com'

    open_session do |s|
      post "#{@org.home_page}/join/#{@invitation.token}"

      assert_match (/#{@invitation.id}/), session[:invitation_id].to_s

      # not sure validate_signature is failing. Only change is extra IdentityType attribute to XML
      XMLSecurity::SignedDocument.any_instance.stubs(:validate_signature).returns(true)
      file = File.read('test/fixtures/saml_response.txt').squish
      encoded_form = Base64.encode64(file)

      host! @org.host
      post '/auth/saml/callback', 'SAMLResponse' => encoded_form,
       'RelayState' => 'http://test.lvh.me:4000/',
       'token_provider' => 'saml'

      assert_equal user.id, controller.current_user.id

      @invitation.reload
      assert_equal user.id, @invitation.recipient.id
      assert @invitation.accepted_at?

      assert_includes user.followed_channels.pluck(:id), @channel.id
      assert_includes user.teams.pluck(:id), @team.id
    end
  end

  test 'accepts invitation by authenticating recipient with same recipient_email for non-existent user' do
    TestAfterCommit.enabled = true
    EdcastActiveJob.unstub :perform_later
    Users::AfterCommitOnCreateJob.unstub :perform_later

    User.any_instance.unstub :cleanup_invitation
    open_session do |s|
      post "#{@org.home_page}/join/#{@invitation.token}"

      assert_match (/#{@invitation.id}/), session[:invitation_id].to_s

      # not sure validate_signature is failing. Only change is extra IdentityType attribute to XML
      XMLSecurity::SignedDocument.any_instance.stubs(:validate_signature).returns(true)
      file = File.read('test/fixtures/saml_response.txt').squish
      file.gsub!(/shirish@edcast.com/, @invitation.recipient_email)
      encoded_form = Base64.encode64(file)
      stub_okta_user_update

      host! @org.host
      post '/auth/saml/callback', 'SAMLResponse' => encoded_form,
       'RelayState' => 'http://test.lvh.me:4000/',
       'token_provider' => 'saml'

      user = User.find_by(email: @invitation.recipient_email, organization: @org)
      user.run_callbacks :commit

      assert_equal user.id, controller.current_user.id

      @invitation.reload
      user.run_callbacks :commit
      assert_equal user.id, @invitation.recipient.id
      assert @invitation.accepted_at?
      assert_includes user.followed_channels.pluck(:id), @channel.id
      assert_equal [@team.id], user.teams.pluck(:id)
    end
  end

  test 'logs out user if recipient_email do not match with recipient_email' do
    user = create :user, organization: @org, email: 'shirish@edcast.com'
    create :identity_provider, user: user, uid: 'shirish@edcast.com'

    open_session do |s|
      # not sure validate_signature is failing. Only change is extra IdentityType attribute to XML
      XMLSecurity::SignedDocument.any_instance.stubs(:validate_signature).returns(true)
      file = File.read('test/fixtures/saml_response.txt').squish
      encoded_form = Base64.encode64(file)

      host! @org.host
      post '/auth/saml/callback', 'SAMLResponse' => encoded_form,
       'RelayState' => 'http://test.lvh.me:4000/',
       'token_provider' => 'saml'

      assert_equal user.id, controller.current_user.id

      post "#{@org.home_page}/join/#{@invitation.token}"

      # user signed out due to safety reasons.
      refute controller.current_user
      # sets `:invitation_url`
      assert_match (/#{@invitation.id}/), session[:invitation_id].to_s
    end
  end
end

require 'test_helper'

class BatchApiTest < ActionDispatch::IntegrationTest
  self.use_transactional_fixtures = false

  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    WebMock.disable!
    @org = create(:organization)
    @user = create(:user, organization: @org)
    @group = create(:group, organization: @org)
    @group.add_member(@user)
    @card = create(:card, card_type: 'media', card_subtype: 'video', organization: @org, author: @user)

    @editable_post = create(:post, user: @user, group: @group, title: 'original title', message: 'original message')
    @post = create(:post, user: @user, group: @group)
    @comment = create(:comment, commentable: @post, message: 'message', user: @user)
    @saml_provider = create :identity_provider, :saml, user: @user, auth_info: sso_info(user: @user)
    UserContentsQueue.create_index! force: true
  end

  teardown do
    WebMock.enable!
  end

  test 'batch api request should work' do
    srand(0)
    expected_statuses = [
      200,
      200,
      404,
      201,
      422,
      200,
      204
    ]

    reqs = { ops: [
      # get user info
      {
        method: 'get',
        url: "/api/users/info"
      },
      # get posts in a group. should return 200
      {
        method: 'get',
        url: "/groups/#{@group.id}/posts"
      },
      # get posts in a non existant group. return 404
      {
        method: 'get',
        url: "/groups/9999/posts"
      },
      # create a new post. should return 201
      {
        method: 'post',
        url: "/groups/#{@group.id}/posts",
        params: {
          title: 'second post title',
          message: 'second post message',
          type: 'Question'
        }
      },
      # bad create request. Missing title
      {
        method: 'post',
        url: "/groups/#{@group.id}/posts",
        params: {
          title: '',
          message: 'failed post',
          type: 'Question'
        }
      },
      # update existing post
      {
        method: 'put',
        url: "/groups/#{@group.id}/posts/#{@editable_post.id}",
        params: {
          title: 'edited title',
          message: 'edited message'
        }
      },
      # delete existing comment
      {
        method: 'delete',
        url: "/posts/#{@post.id}/comments/#{@comment.id}",
      }
    ],
    sequential: true
    }

    token = @user.jwt_token
    open_session do |sess|

      # also testing that comment got deleted, and new post got created
      assert_differences [[->{Comment.count}, -1], [->{Post.count}, 1]]  do
        sess.post "#{@org.home_page}/api/batch", reqs.to_json, {'Content-Type' => 'application/json',
                                                                'X-Edcast-JWT' => 1,
                                                                'X-API-TOKEN' => token,
                                                                'Accept' => 'application/json'}
      end
      assert_equal "200", sess.response.code
      resp = get_json_response(sess.response)
      assert_equal reqs[:ops].count, resp['results'].count
      assert_equal expected_statuses, resp['results'].map{|r| r['status']}, resp['results'].map{|r| r['body']}

      # test users/info body
      _raw_info = @saml_provider.auth_info['raw_info']
      _raw_info = Hash[_raw_info.map { |k, v| [k.underscore, v]}]
      saml      = @saml_provider.as_json
      saml['auth_info'].merge!('raw_info' => _raw_info)
      expected = {
          'name' => @user.name,
          'first_name' => @user.first_name,
          'email' => @user.email,
          'last_name' => @user.last_name,
          'handle' => @user.handle,
          'picture' => @user.photo(:medium),
          'coverimages' => @user.fetch_coverimage_urls,
          'id' => @user.id,
          'is_suspended' => false,
          'bio' => @user.bio,
          'newsletter_opt_in' => @user.pref_newsletter_opt_in?,
          'following_users' => {'count': 0, 'users': []}.as_json,
          'following_channels' => [],
          'writable_channels'=> [],
          'groups' => @user.groups.as_json(only: ['id', 'name']),
          'following_count' => 0,
          'followers_count' => 0,
          'full_name' => @user.full_name,
          'permissions'=>[],
          'is_admin' => false,
          'sso_info' => saml.merge!(
            'created_at' => @saml_provider.created_at.strftime("%Y-%m-%dT%H:%M:%S.000Z"),
            'updated_at' => @saml_provider.updated_at.strftime("%Y-%m-%dT%H:%M:%S.000Z"))
      }

      assert_equal @user.id, resp['results'][0]['body']['id']

      # test post was edited
      @editable_post.reload
      assert_equal 'edited title', @editable_post.title
      assert_equal 'edited message', @editable_post.message
    end
    srand()
  end
end

require 'test_helper'

class LinkedInLoginTest < ActionDispatch::IntegrationTest

  setup do
    Organization.any_instance.unstub :default_sso
    create_default_ssos
    create_default_org

    stub_request(:get, "https://api.linkedin.com/v1/people/~:(id,email-address,first-name,last-name,headline,industry,picture-url;secure=true,location,public-profile-url)?format=json&oauth2_access_token=xxx123").
      with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>/.*/}).
      to_return(:status => 200,
                :body => {
                  "emailAddress" =>  "bharathpbhat@gmail.com",
                  "firstName" =>  "Bharath",
                  "headline" =>  "Machine Learning Engineer at Diffbot",
                  "id" =>  "YXEgmZb0iw",
                  "industry" =>  "Computer Software",
                  "lastName" =>  "Bhat",
                  "location" =>  {
                    "country" =>  {"code" =>  "us"},
                    "name" =>  "San Francisco Bay Area"
                  },
                  "pictureUrl" => "https://media.licdn.com/mpr/mprx/0_yHCbiqdc_gXu5pTZymPZinWJixT85VhZOfn4in4dpyvKsYc4ratwS9uFDc3x6j3NgI8M2AE4AeyE",
                  "publicProfileUrl" =>  "http://www.linkedin.com/pub/bharath-bhat/15/500/329"
                }.to_json,
                :headers => {"Content-Type" => "application/json"})
      Search::UserSearch.any_instance.stubs(:search_by_id)
  end

  test 'create new user on linkedin login' do
    assert_difference [-> {User.count}, -> {LinkedinProvider.count}] do
      post '/auth/linkedin_access_token/callback', {access_token: 'xxx123', provider: :linkedin}, {'User-Agent' => 'EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0', 'X-Build-Number' => '42'}
      assert_response :success
      resp = ActiveSupport::JSON.decode(response.body)
      assert_equal User.last.id, resp['id']
      assert_equal User.last.organization_id, resp['organization']['id']
    end

    user = User.find_by(email: 'bharathpbhat@gmail.com')
    assert_not_nil user
    assert_equal @controller.current_user, user

    lp = LinkedinProvider.last
    assert_equal lp.token, 'xxx123'
    assert_equal lp.user, user
  end

  test 'existing user should just be logged in' do
    user = create(:user, email: 'bharathpbhat@gmail.com', organization: Organization.default_org)
    idp  = create :identity_provider, :linkedin, uid: 'YXEgmZb0iw', token: 'xxx123', user: user, auth: true

    assert_no_difference [ -> {User.count} ] do
      post '/auth/linkedin_access_token/callback', {access_token: 'xxx123', provider: :linkedin}, {'User-Agent' => 'EdCast/39 CFNetwork/672.1.15 Darwin/14.0.0', 'X-Build-Number' => '42'}
      assert_response :success
    end

    lp = LinkedinProvider.where(user: user).first
    assert_equal lp.token, 'xxx123'

    assert_equal @controller.current_user, user
  end
end

require 'test_helper'

class UserInfoAggregatorTest < ActionDispatch::IntegrationTest
  include HelperMethods

  setup do
    @org = create :organization
    @default_org = Organization.default_org
    stub_okta_user_update
    User.any_instance.stubs(:reindex_async)
  end

  test 'revoke activities if user is suspended' do
    omniauth = setup_fb_omniauth
    omniauth.merge!(current_platform: 'web')

    user     = create :user, organization: @org, is_suspended: true, email: omniauth[:info][:email]
    provider = create :identity_provider, :facebook, user: user, uid: omniauth[:uid]

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate

    assert_match /Invalid sign on credentials/, user_aggregator.error
  end

  test 'creates new User' do
    omniauth = setup_fb_omniauth
    omniauth.merge!(current_platform: 'web')

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate

    user = User.find_by(email: omniauth[:info][:email], organization: @org)

    assert_not_nil user
    assert_match /#{user.first_name}/, omniauth[:info][:first_name]
    assert_equal user.picture_url, omniauth[:info][:image]
    assert_match /#{user.last_name}/, omniauth[:info][:last_name]
  end

  test 'creates Facebook IdentityProvider' do
    omniauth = setup_fb_omniauth
    omniauth.merge!(current_platform: 'web')

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate

    user = User.find_by(email: omniauth[:info][:email], organization: @org)
    provider = FacebookProvider.find_by(user: user, uid: omniauth[:uid])

    assert provider
    assert provider.expires_at
    assert_equal provider.token, omniauth[:credentials][:token]
  end

  test 'creates Google identity_provider' do
    omniauth = setup_google_omniauth
    omniauth.merge!(current_platform: 'web')

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate

    user = User.find_by(email: omniauth[:info][:email], organization: @org)
    provider = GoogleProvider.find_by(user: user, uid: omniauth[:uid])

    assert provider
    assert provider.expires_at
    assert_equal provider.token, omniauth[:credentials][:token]
  end

  test 'creates Linkedin identity_provider' do
    omniauth = setup_linkedin_omniauth
    omniauth.merge!(current_platform: 'web')

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate

    user = User.find_by(email: omniauth[:info][:email], organization: @org)
    provider = LinkedinProvider.find_by(user: user, uid: omniauth[:uid])

    assert provider
    assert provider.expires_at
    assert_equal provider.token, omniauth[:credentials][:token]
  end

  test 'creates OrgOauth identity_provider' do
    omniauth = setup_org_oauth_omniauth
    omniauth.merge!(current_platform: 'web')

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate

    user = User.find_by(email: omniauth[:info][:email], organization: @org)
    provider = OrgOauthProvider.find_by(user: user, uid: omniauth[:uid])

    assert provider
    assert provider.auth_info
    assert_equal provider.token, omniauth[:credentials][:token]
    assert_equal provider.secret, omniauth[:credentials][:secret]
  end

  test 'creates Office365 identity_provider' do
    omniauth = setup_office365_omniauth
    omniauth.merge!(current_platform: 'web')

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate

    user = User.find_by(email: omniauth[:info][:email], organization: @org)
    provider = Office365Provider.find_by(user: user, uid: omniauth[:uid])

    assert provider
    assert provider.auth_info
    assert_equal provider.token, omniauth[:credentials][:token]
    assert_equal provider.secret, omniauth[:credentials][:secret]
  end

  test 'creates Salesforce identity_provider' do
    omniauth = setup_salesforce_omniauth
    omniauth.deep_symbolize_keys!
    omniauth.merge!(organization_id: @org.id, current_platform: 'web', provider: 'salesforce')

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate

    user = User.find_by(email: omniauth[:info][:email], organization: @org)
    provider = SalesforceProvider.find_by(user: user, uid: omniauth[:uid])

    assert provider
    assert provider.auth_info
    assert_equal provider.token, omniauth[:credentials][:token]
    assert_equal provider.secret, omniauth[:credentials][:secret]
  end

  test 'creates Salesforceusers identity_provider' do
    omniauth = setup_salesforce_omniauth
    omniauth.deep_symbolize_keys!
    omniauth.merge!(organization_id: @org.id, current_platform: 'web', provider: 'salesforceusers')

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate

    user = User.find_by(email: omniauth[:info][:email], organization: @org)
    provider = SalesforceuserProvider.find_by(user: user, uid: omniauth[:uid])

    assert provider
    assert provider.auth_info
    assert_equal provider.token, omniauth[:credentials][:token]
    assert_equal provider.secret, omniauth[:credentials][:secret]
  end

  test 'creates Salesforcedevelopers identity_provider' do
    omniauth = setup_salesforce_omniauth
    omniauth.deep_symbolize_keys!
    omniauth.merge!(organization_id: @org.id, current_platform: 'web', provider: 'salesforcedevelopers')

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate

    user = User.find_by(email: omniauth[:info][:email], organization: @org)
    provider = SalesforcedeveloperProvider.find_by(user: user, uid: omniauth[:uid])

    assert provider
    assert provider.auth_info
    assert_equal provider.token, omniauth[:credentials][:token]
    assert_equal provider.secret, omniauth[:credentials][:secret]
  end

  test 'creates Salesforcepartners identity_provider' do
    omniauth = setup_salesforce_omniauth
    omniauth.deep_symbolize_keys!
    omniauth.merge!(organization_id: @org.id, current_platform: 'web', provider: 'salesforcepartners')

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate

    user = User.find_by(email: omniauth[:info][:email], organization: @org)
    provider = SalesforcepartnerProvider.find_by(user: user, uid: omniauth[:uid])

    assert provider
    assert provider.auth_info
    assert_equal provider.token, omniauth[:credentials][:token]
    assert_equal provider.secret, omniauth[:credentials][:secret]
  end

  test 'creates LxpOauth identity_provider' do
    omniauth = setup_lxp_oauth_omniauth
    omniauth.deep_symbolize_keys!
    omniauth.merge!(organization_id: @org.id, current_platform: 'web', provider: 'lxp_oauth')

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate

    user = User.find_by(email: omniauth[:info][:email], organization: @org)
    provider = LxpOauthProvider.find_by(user: user, uid: omniauth[:uid])

    assert provider
    assert provider.auth_info
    assert_equal provider.token, omniauth[:credentials][:token]
    assert_equal provider.secret, omniauth[:credentials][:secret]
  end

  test 'creates Email record' do
    omniauth = setup_fb_omniauth
    omniauth.merge!(current_platform: 'web')

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate

    user  = User.find_by(email: omniauth[:info][:email], organization: @org)
    email = user.emails.first

    assert email
    assert email.confirmed
  end

  test 'update first & last name existing user' do
    omniauth = setup_google_omniauth
    omniauth.merge!(current_platform: 'web')

    user     = create :user, email: omniauth[:info][:email], first_name: nil, last_name: nil, organization: @org
    provider = create :identity_provider, :google, user: user, uid: '123456789', auth: true

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate

    user.reload

    assert_match /#{user.first_name}/, omniauth[:info][:first_name]
    assert_match /#{user.last_name}/, omniauth[:info][:last_name]
    assert_equal user.identity_providers.count, 1
    assert_equal user.identity_providers.first.id, provider.id
  end

  test 'update email of an existing user if it does not match with the existing email' do
    omniauth = setup_lxp_oauth_omniauth
    omniauth.merge!(current_platform: 'web')

    user     = create :user, email: omniauth[:info][:email], first_name: nil, last_name: nil, organization: @org
    user.skip_confirmation!
    provider = create :identity_provider, :lxp_oauth, user: user, uid: '00ud29p6s62vb5NWq0h7', auth: true

    omniauth[:info].merge!(email: "okta-user@gmail.in")

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate

    assert_equal 'okta-user@gmail.in', user.reload.email
    assert_equal 'Okta', user.first_name
    assert_equal 'User', user.last_name
  end

  test 'do no update email if the email is blank in the IdP response' do
    omniauth = setup_lxp_oauth_omniauth
    omniauth.merge!(current_platform: 'web')

    user     = create :user, email: omniauth[:info][:email], first_name: nil, last_name: nil, organization: @org
    provider = create :identity_provider, :lxp_oauth, user: user, uid: '00ud29p6s62vb5NWq0h7', auth: true

    omniauth[:info].merge!(email: '')

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate
    
    assert_equal 'okta-user@gmail.com', user.reload.email
  end

  test 'do not update email if the email of existing user in the IdP response exists in the system' do
    omniauth = setup_lxp_oauth_omniauth
    omniauth.merge!(current_platform: 'web')

    # Step 1: SSO user exists already in the system and is linked to the identity provider
    user     = create :user, email: omniauth[:info][:email], first_name: nil, last_name: nil, organization: @org
    provider = create :identity_provider, :lxp_oauth, user: user, uid: '00ud29p6s62vb5NWq0h7', auth: true

    # Step 2: Admin imports the same user but with different email as directed by the client.
    #   Since we do not support importing users along with the SSO ID, system will create a new user.
    user2    = create :user, email: 'okta-user@gmail.in', first_name: 'Okta', last_name: 'User', organization: @org

    omniauth[:info].merge!(email: 'okta-user@gmail.in')

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate

    assert_equal 'okta-user@gmail.com', user.reload.email
  end

  test 'track email change event to Influx' do
    omniauth = setup_lxp_oauth_omniauth
    omniauth.merge!(current_platform: 'web')

    user     = create :user, email: omniauth[:info][:email], first_name: 'Okta', last_name: 'User', organization: @org
    provider = create :identity_provider, :lxp_oauth, user: user, uid: '00ud29p6s62vb5NWq0h7', auth: true

    omniauth[:info].merge!(email: 'okta-user@gmail.in')

    Analytics::MetricsRecorderJob.expects(:perform_later).with(includes(
      :recorder, :event, :actor, :org, :user, :timestamp,
      :additional_data, :changed_column, :old_val, :new_val
    ))

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate
  end

  test 'change #federated_identifier of a user if the provider is lxp_oauth' do
    omniauth = setup_lxp_oauth_omniauth
    omniauth.merge!(current_platform: 'web')

    user     = create :user, email: omniauth[:info][:email], first_name: nil, last_name: nil, organization: @org
    provider = create :identity_provider, :lxp_oauth, user: user, uid: '00ud29p6s62vb5NWq0h7', auth: true

    omniauth[:info].merge!(email: 'okta-user@gmail.in', 'preferred_username' => "1589184311142599-okta_user@gmail.in", 'provider' => 'lxp_oauth')
    omniauth[:info].delete(:provider)

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate

    assert_equal '1589184311142599-okta_user@gmail.in', user.reload.federated_identifier
  end

  test 'change email (if already blank) to the email in the IdP response' do
    omniauth = setup_fb_omniauth
    omniauth.merge!(current_platform: 'web')

    user     = create :user, email: nil, first_name: nil, last_name: nil, organization: @org
    provider = create :identity_provider, :facebook, user: user, uid: '1234567', auth: true

    omniauth[:info].merge!(email: 'joe@bloggs.inc')

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate

    assert_equal 'joe@bloggs.inc', user.reload.email
    assert_equal 'Joe', user.first_name
    assert_equal 'Bloggs', user.last_name
  end

  test 'do not change #federated_identifier of a user if the provider is other than lxp_oauth' do
    omniauth = setup_fb_omniauth
    omniauth.merge!(current_platform: 'web')

    user     = create :user, email: nil, first_name: nil, last_name: nil, organization: @org
    provider = create :identity_provider, :facebook, user: user, uid: '1234567', auth: true

    omniauth[:info].merge!(email: 'joe@bloggs.inc')

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate

    assert_nil user.reload.federated_identifier
  end

  test 'updating email should send no confirmation email' do
    omniauth = setup_lxp_oauth_omniauth
    omniauth.merge!(current_platform: 'web')

    user     = create :user, email: omniauth[:info][:email], first_name: nil, last_name: nil, organization: @org
    provider = create :identity_provider, :lxp_oauth, user: user, uid: '00ud29p6s62vb5NWq0h7', auth: true

    omniauth[:info].merge!(email: 'okta-user@gmail.in')

    DeviseMandrillMailer.expects(:confirmation_instructions).never

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate
  end

  test 'create a user if the former user had never signed in with IdP information' do
    omniauth = setup_lxp_oauth_omniauth
    omniauth.merge!(current_platform: 'web')

    user     = create :user, email: omniauth[:info][:email], first_name: nil, last_name: nil, organization: @org

    omniauth[:info].merge!(email: 'okta-user@gmail.in')

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate

    user2 = @org.reload.users.find_by(email: 'okta-user@gmail.in')
    identity_provider = IdentityProvider.find_by(user: user2)

    assert user2
    assert identity_provider
    assert_equal '00ud29p6s62vb5NWq0h7', identity_provider.uid

    assert_empty user.reload.identity_providers
    assert_equal 'okta-user@gmail.com', user.reload.email
  end

  test 'email is changed only if account is confirmed on social applications' do
    omniauth = setup_fb_omniauth
    omniauth.merge!(current_platform: 'web')

    user     = create :user, email: omniauth[:info][:email], first_name: nil, last_name: nil, organization: @org
    provider = create :identity_provider, :facebook, user: user, uid: '1234567', auth: nil

    omniauth[:info].delete(:verified)

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate

    assert_match /Your account is not yet confirmed/, user_aggregator.error
  end

  test 'create Email record for change in email' do
    omniauth = setup_lxp_oauth_omniauth
    omniauth.merge!(current_platform: 'web')

    user     = create :user, email: omniauth[:info][:email], first_name: nil, last_name: nil, organization: @org
    provider = create :identity_provider, :lxp_oauth, user: user, uid: '00ud29p6s62vb5NWq0h7', auth: true

    omniauth[:info].merge!(email: 'okta-user@gmail.in')

    user_aggregator = UserInfo::Aggregator.new(
      organization: @org,
      user_info:    omniauth )
    user_aggregator.aggregate

    emails = Email.where(user_id: user.id)
    assert_equal ['okta-user@gmail.in'], emails.map(&:email)
  end
end

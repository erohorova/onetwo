require 'test_helper'

class LtiIntegrationTest < ActionDispatch::IntegrationTest
  setup do
    skip if ENV['EXCLUDE_FORUM_SAVANNAH_TESTS'] == 'true'
    WebMock.disable!
    Searchkick.enable_callbacks
  end
  teardown do
    Searchkick.disable_callbacks
    WebMock.enable!
  end

  def events_setup
    @tc = IMS::LTI::ToolConfig.new(:title => 'EdCast', :launch_url => 'https://localhost:4000/lti/events')
    common_setup
  end

  def common_setup
    host! 'localhost:4000'
    https!

    @test_client = create(:client, name: 'test_client')
    @org = create(:organization, client: @test_client)
    @test_credential = @test_client.valid_credential

    @consumer = IMS::LTI::ToolConsumer.new(@test_credential.api_key,
                                           @test_credential.shared_secret)
    @consumer.set_config(@tc)

    group = create(:group, client_resource_id: 'test_resource_id', name: 'name1', description: 'test description')
    @consumer.resource_link_id = group.client_resource_id
    @consumer.resource_link_title = group.name
    @consumer.resource_link_description = group.description
    cm_admin = create :user, email: 'admin@course-master.com'
    user = create(:user, email: 'b@b.com')
    @consumer.user_id = user.id

    pic_url = URI.parse(user.photo)
    if pic_url.scheme == 'http'
      @consumer.user_image = user.photo
    end

    @consumer.roles = 'student'
    @consumer.lis_person_contact_email_primary = user.email
    @consumer.lis_person_name_full = user.name
    @consumer.lis_person_name_given = user.first_name
    @consumer.lis_person_name_family = user.last_name
  end
# -----------------------------------------------------------------------------
# COMMENTED OUT UNTIL WE FIX BROWSERIFY IN TEST ENVIRONMENT
# -----------------------------------------------------------------------------

=begin

  test 'events launch with student role should work' do
    events_setup
    post '/lti/events', @consumer.generate_launch_data
    assert_response :success
    assert_template 'events'

    o_role = assigns(:original_user_role)
    assert_equal 'student', o_role
    u_role = assigns(:user_role)
    assert_equal 'member', u_role
    assert_equal 'student', GroupsUser.last.role_label
  end

  test 'events launch with instructor role should work' do
    events_setup
    @consumer.roles = 'instructor'
    post '/lti/events', @consumer.generate_launch_data
    assert_response :success
    assert_template 'events'

    o_role = assigns(:original_user_role)
    assert_equal 'instructor', o_role
    u_role = assigns(:user_role)
    assert_equal 'admin', u_role
    assert_equal 'instructor', GroupsUser.last.role_label
  end

  test 'events launch with assistant role should work' do
    events_setup
    @consumer.roles = 'assistant'
    post '/lti/events', @consumer.generate_launch_data
    assert_response :success
    assert_template 'events'

    o_role = assigns(:original_user_role)
    assert_equal 'assistant', o_role
    u_role = assigns(:user_role)
    assert_equal 'admin', u_role
    assert_equal 'assistant', GroupsUser.last.role_label
  end

=end

  test 'bad role should send out bad request for events' do
    events_setup
    @consumer.roles = 'badRole'
    post '/lti/events', @consumer.generate_launch_data
    assert_response :bad_request
    assert_template 'error'
  end

end

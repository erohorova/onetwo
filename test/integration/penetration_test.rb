require 'test_helper'

class PenetrationTest < ActionDispatch::IntegrationTest
  setup do
    @organization = create :organization
    @admin = create :user, organization: @organization, organization_role: 'admin'
    @token = @admin.jwt_token
    @organization.reload

    @headers = { 'Content-Type' => 'application/json', 'Accept' => 'application/json', 'X-Edcast-JWT' => 1, 'X-API-TOKEN' => @token }
  end

  test 'any HTTPS request must have `Strict-Transport-Security` in the response headers' do
    card1 = create :public_media_card, author_id: @admin.id, organization_id: @organization.id
    get "#{@organization.home_page.gsub('http', 'https')}/api/v2/cards", { offset: 0 }, @headers

    assert_equal 200, response.status
    assert_equal "max-age=31536000; includeSubDomains", response.headers['Strict-Transport-Security']
  end
end

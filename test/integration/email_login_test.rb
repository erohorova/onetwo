require 'test_helper'

class EmailLoginTest < ActionDispatch::IntegrationTest
  self.use_transactional_fixtures = false
  test 'should be able to log in from mobile' do
    skip("INTERMITTENT FAILURE ON TRAVIS")
    create_default_org
    MandrillMailer.any_instance.expects(:send_api_email).twice
    post '/auth/users', {user: {email: 'ios@edcast.com'}}.to_json, {'Content-Type' => 'application/json', 'Accept' => 'application/json', 'User-Agent' => 'EdCast ios', 'X-Build-Number' => '42'}
    assert_response :ok

    post '/auth/users', {user: {email: 'android@edcast.com'}}.to_json, {'Content-Type' => 'application/json', 'Accept' => 'application/json', 'User-Agent' => 'EdCast android', 'X-Build-Number' => '42'}
    assert_response :ok
  end
end

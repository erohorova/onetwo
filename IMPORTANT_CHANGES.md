```
8-8-2017
Add ENV key SECURE_IMAGES_PREFIX_URL=https://d23od3i110ssao.cloudfront.net/
(Make sure we have the trailing slash)
```

```
27-07-2017
Add ENV Key ECL_API_VERSION=v2 for Destiny to use the new version of ECL (with Sociative)
```

```
07-06-2017
Add ENV Key "ENV['LAUNCHDARKLY_CLIENT_KEY']" for launch darkly client access key.
Add ENV Key "ENV['report_bucket']" to store reports going forward.
```

```
18-05-2017
Add workers for the following queue: edcast_notifications
```

```
03-05-2017
rake one_time_task:change_users_emails 'host_name' (EP-4033).
```

```
21-03-2017
Save secret token as ENV variable
SECRET_KEY_BASE = to some value.
```

```
21-02-2017
New Queues added for jobs
- batch
```
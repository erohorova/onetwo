class Settings < Settingslogic
  unless (Rails.env.development? || Rails.env.test?)
    begin
      require "dotenv"
      Dotenv::S3.load(bucket: ENV['S3_BUCKET_NAME'], filename: "environment_variables", base64_encoded: false, kms_key_id: nil) do |tempfile|
        Dotenv.load(tempfile.path)
      end
    rescue => e
      puts "Inside Rescue - Error in loading Dotenv::S3 #{e.backtrace}"
    end
  end
  source File.expand_path('../settings.yml', __FILE__)
  namespace Rails.env
end
# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Edcast::Application.initialize!

#camilize all json responses
Jbuilder.key_format camelize: :lower

if ENV['ALLOCATION'] == 'true'
  require 'objspace'

  ObjectSpace.trace_object_allocations_start
end

$skip_on_before_fork = true

workers Integer(ENV['PUMA_WORKERS'] || 3)
threads Integer(ENV['MIN_THREADS']  || 16), Integer(ENV['MAX_THREADS'] || 16)

preload_app!
rackup      DefaultRackup
port        ENV['PORT']     || 3000
environment ENV['RACK_ENV'] || 'development'

before_fork do
  ActiveRecord::Base.connection_pool.disconnect!
end

on_worker_boot do
  # worker specific setup
  ActiveRecord::Base.establish_connection

  #reconnect Rails.cache
  Rails.cache.reset if Rails.cache.respond_to?(:reset)


  $skip_on_before_fork = false #set this to false in order to actually load the initializers below
  require("#{Rails.root}/config/initializers/elastic_search")
  # require("#{Rails.root}/config/initializers/sneakers_job_queue")
  require("#{Rails.root}/config/initializers/cache")

end

class EclSettings < Settingslogic
  source File.expand_path('../ecl_settings.yml', __FILE__)
  namespace 'providers'
end
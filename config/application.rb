require File.expand_path('../boot', __FILE__)

## Code Unbundled from "railties/lib/rails/all.rb". Note: require 'rails/all' is commented out
# This is done so we don't try to connect to mysql when we build the docker image
frameworks = ["action_controller", "action_view", "action_mailer", "rails/test_unit", "sprockets"]
unless ENV["PRECOMPILE_MODE"] == "true"
  frameworks << "active_record"
end

frameworks.each do |framework|
  begin
    require "#{framework}/railtie"
  rescue LoadError
  end
end
## END require 'rails/all'

# require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

# Require settings
require File.expand_path('../settings', __FILE__)
require File.expand_path('../ecl_settings', __FILE__)
require File.expand_path('../organization_default_configs', __FILE__)

# Monkey patch AWS SES library
require File.expand_path('../../lib/aws_ses_mailer', __FILE__)

# #enable elasticsearch lograge
# require 'elasticsearch/rails/lograge'

module Edcast
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de
    config.i18n.enforce_available_locales = true
    config.paper_trail.enabled = false
    #force IE to use the latest rendering engine
    config.action_dispatch.default_headers = {'X-UA-Compatible' => 'IE=edge'}
    Koala.config.api_version = "v2.0"
    config.middleware.insert_before ActionDispatch::ParamsParser, "CatchJsonParseErrors"
    config.middleware.use BatchApi::RackMiddleware do |batch_config|
      batch_config.endpoint = "/api/batch"
      batch_config.batch_middleware = Proc.new {}
    end

    config.middleware.insert_before 0, "Rack::Cors", logger: (-> {Rails.logger} ) do
      allow do
        origins '*'
        resource '/api/v2/*',
          :headers => :any,
          :methods => [],
          :credentials => false, # does not let cookes go through,
          :max_age => 3600 # time period to cache preflight request, in seconds
      end
      allow do
        origins '*'
        resource '/public/*',
          :headers => :any,
          :methods => [],
          :credentials => false, # does not let cookes go through,
          :max_age => 3600 # time period to cache preflight request, in seconds
      end
      allow do
        origins '*'
        resource '*',
          :headers => :any,
          :expose => ['X-Cache-Id', 'X-UA-Compatible', 'X-Last-Fetch-Timestamp'],
          :methods => [:get, :post, :delete, :put, :options, :head],
          :credentials => false, # does not let cookes go through,
          :max_age => 3600 # time period to cache preflight request, in seconds
      end
    end
    config.autoload_paths += ["#{Rails.root}/app/cache/concerns"]
    config.browserify_rails.commandline_options = %q{-t [ babelify --presets [ es2015 react ] ] --transform [ babelify ] --extension=".js.jsx"}

    config.react.react_js = lambda { '' } #tell rails-react gem to not load the reactjs library that came with the gem. instead we use the CommonJS npm version.
    config.react.server_renderer_options = {
      files: ["react-components-server.js"]
    }
    require "#{Rails.application.root}/lib/react-rails/lib/react/rails/edcast_component_mount.rb"
    require "#{Rails.application.root}/lib/react-rails/lib/react/server_rendering/edcast_renderer.rb"
    config.react.server_renderer = React::ServerRendering::EdcastRenderer
    config.react.view_helper_implementation = React::Rails::EdcastComponentMount

=begin
    config.react.jsx_transform_options = {
      harmony: true,
      strip_types: true
    }
=end

    config.to_prepare do
      # Only Authorization endpoint
      Doorkeeper::AuthorizationsController.layout Proc.new { |controller|  'popup' }
      Doorkeeper::AuthorizationsController.send(:include, 'NativeAppDetection'.constantize)
      Doorkeeper::AuthorizationsController.send(:include, 'DetectPlatform'.constantize)
    end

    # Allow iframing on same origin
    # Reducing MIME type security risks
    config.action_dispatch.default_headers.merge!({'X-Frame-Options' => 'SAMEORIGIN', "X-Content-Type-Options" => 'nosniff'})

    config.exceptions_app = self.routes
    config.assets.paths << Rails.root.join("#{Rails.root}/app/assets/images/about_us/") << Rails.root.join("#{Rails.root}/app/assets/images/partners/")

    unless ENV["PRECOMPILE_MODE"] == "true"
      config.active_record.raise_in_transactional_callbacks = true
    end

    # Enable gzip compression of controller responses if the client requests it.
    # See https://stackoverflow.com/a/19936985/2981429
    config.middleware.use Rack::Deflater

    # To implement throttling
    config.middleware.use Rack::Attack

  end
end

Edcast::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # DON'T SPAM THE CONSOLE WITH DATABASE LOGS!
  # CAN REMOVE THIS WHILE WORKING IF YOU WANT TO SEE ALL THOSE LOGS
  # config.log_level = ENV.fetch("LOG_LEVEL", "error").to_sym

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations
  config.active_record.migration_error = :page_load

  config.action_mailer.default_url_options = {
    :host => "localhost",
    :port => 4000
  }
  Paperclip::Attachment.default_options.merge!({
    # actual path of the file stored by paperclip
    #:path => ':rails_root/app/assets/:class/:attachment/:id_partition/:style/:filename'
  })
  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true
  config.react.variant = :development
  config.action_mailer.asset_host = 'http://localhost:4000'
  config.action_dispatch.tld_length = 0

  config.cache_store = :memory_store
  config.after_initialize do
    Bullet.enable = true
    Bullet.bullet_logger = true
    Bullet.console = true
    Bullet.rails_logger = true
  end
end

#uncomment this line below to send background job to sneakers instead of the inline queue.
#ActiveJob::Base.queue_adapter = :sneakers

module LogQuerySource
  def debug(*args, &block)
    return unless super

    backtrace = Rails.backtrace_cleaner.clean caller

    relevant_caller_line = backtrace.detect do |caller_line|
      !caller_line.include?('/initializers/')
    end

    if relevant_caller_line
      logger.debug("  ↳ #{ relevant_caller_line.sub("#{ Rails.root }/", '') }")
    end
  end
end
ActiveRecord::LogSubscriber.send :prepend, LogQuerySource

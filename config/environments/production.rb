require Rails.root.join('config/environments/production_shared')

Edcast::Application.configure do
  config.action_mailer.default_url_options = {
    :host => ENV['DEFAULT_EMAIL_HOST'] || 'www.edcast.com',
    :protocol => 'https'
  }

  config.log_level = ENV['LOG_LEVEL'] ? ENV['LOG_LEVEL'].to_sym : :info
end

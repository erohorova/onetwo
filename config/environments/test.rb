Edcast::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # The test environment is used exclusively to run your application's
  # test suite. You never need to work with it otherwise. Remember that
  # your test database is "scratch space" for the test suite and is wiped
  # and recreated between test runs. Don't rely on the data there!
  config.cache_classes = true

  # Do not eager load code on boot. This avoids loading your whole application
  # just for the purpose of running a single test. If you are using a tool that
  # preloads Rails for running tests, you may have to set it to true.
  config.eager_load = false

  # Configure static asset server for tests with Cache-Control for performance.
  config.serve_static_files = true
  config.static_cache_control = "public, max-age=3600"

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Raise exceptions instead of rendering exception templates.
  config.action_dispatch.show_exceptions = false

  # Disable request forgery protection in test environment.
  config.action_controller.allow_forgery_protection = false

  # Tell Action Mailer not to deliver emails to the real world.
  # The :test delivery method accumulates sent emails in the
  # ActionMailer::Base.deliveries array.
  config.action_mailer.delivery_method = :test

  # Print deprecation notices to the stderr.
  config.active_support.deprecation = :stderr

  Paperclip::Attachment.default_options.merge!({
    # actual path of the file stored by paperclip
    :path => ':rails_root/test_assets/:class/:attachment/:id_partition/:style/:filename',
    :url => ':rails_root/test_assets/:class/:attachment/:id_partition/:style/:filename'
  })

  if File.exist?(Rails.root+'test/fixtures/s3_test_keys.yaml')
    config.paperclip_defaults = {
        :storage => :s3,
        :s3_credentials => HashWithIndifferentAccess.new(YAML.load_file(Rails.root+'test/fixtures/s3_test_keys.yaml'))
    }
  end

  # ENV variable for savannah provisioning
  ENV['SAVANNAH_TOKEN'] = 'token'

  config.action_controller.asset_host = 'http://cdn-test-host.com'
  config.action_mailer.asset_host = 'http://cdn-test-host.com'
  config.action_mailer.default_url_options = {
    :protocol => 'https',
    :host => Settings.platform_host
  }

  config.active_support.test_order = :random # will be default in Rails 5.0

  config.cache_store = :null_store
end

module Authority
  module Controller
    def authority_forbidden(error)
      head :forbidden
    end
  end
end

module LogQuerySource
  def debug(*args, &block)
    return unless super

    backtrace = Rails.backtrace_cleaner.clean caller

    relevant_caller_line = backtrace.detect do |caller_line|
      !caller_line.include?('/initializers/') || !caller_line.include?('/test_after_commit/')
    end

    if relevant_caller_line
      logger.debug("  ↳ #{ relevant_caller_line.sub("#{ Rails.root }/", '') }")
    end
  end
end

ActiveRecord::LogSubscriber.send :prepend, LogQuerySource
Edcast::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # Code is not reloaded between requests.
  config.cache_classes = true

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both thread web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # Full error reports are disabled and caching is turned on.
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Enable Rack::Cache to put a simple HTTP cache in front of your application
  # Add `rack-cache` to your Gemfile before enabling this.
  # For large-scale production use, consider using a caching reverse proxy like nginx, varnish or squid.
  # config.action_dispatch.rack_cache = true

  # Disable Rails's static asset server (Apache or nginx will already do this).
  config.serve_static_files = true #need to turn this one because of heroku

  # Compress JavaScripts and CSS.
  config.assets.js_compressor = :uglifier_with_source_maps
  config.assets.uglifier = {mangle: false, compress: {keep_fargs: true}}
  # config.assets.css_compressor = :sass

  # Do not fallback to assets pipeline if a precompiled asset is missed.
  #config.assets.compile = false
  config.assets.compile = true #need this for heroku to work

  # Generate digests for assets URLs.
  config.assets.digest = true

  # Version of your assets, change this if you want to expire all your assets.
  config.assets.version = '1.0'

  # Specifies the header that your server uses for sending files.
  # config.action_dispatch.x_sendfile_header = "X-Sendfile" # for apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for nginx

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  config.force_ssl = Settings.platform_force_ssl

  # Prepend all log lines with the following tags.
  # config.log_tags = [ :subdomain, :uuid ]

  # Use a different logger for distributed setups
  config.lograge.keep_original_rails_log = true
  config.lograge.logger = ActiveSupport::Logger.new "#{Rails.root}/log/#{Rails.env}.log"

  # Use a different cache store in production.
  # config.cache_store = :mem_cache_store

  # Enable serving of images, stylesheets, and JavaScripts from an asset server.
  config.action_controller.asset_host = ENV['AWS_CDN_HOST'] if ENV['AWS_CDN_HOST']
  config.action_mailer.asset_host = "https://#{ENV['AWS_CDN_HOST']}" if ENV['AWS_CDN_HOST']

  # Precompile additional assets.
  # application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
  config.assets.precompile += %w( dist/main.js events/bootstrap.js events.css ui/bootstrap.js ui.css dev.css dev/bootstrap.js tinymce-4.0.20/js/tinymce/tinymce.jquery.min.js tinymce-4.0.20/js/tinymce/plugins/mention/plugin.min.js tinymce-4.0.20/js/tinymce/plugins/link/plugin.min.js tinymce-4.0.20/js/tinymce/themes/modern/theme.min.js react-components-client.js widget/sdk.js application_v2.css infinite-scroll.js application_v3.js follows.js autosuggest.js authentication.js card.js localdate.js link_preview.js scheduled_stream_handler.js jquery-ui.js jquery.gridder.js jquery.tagsinput.min.js jquery.text_limiter.js user_feed_tour.js add_to_pack.js issue_collector.js metric_recorder.js moment.en.js react-components-server.js lengthValidator.js)

  # Ignore bad email addresses and do not raise email delivery errors.
  # Set this to true and configure the email server for immediate delivery to raise delivery errors.
  # config.action_mailer.raise_delivery_errors = false

  AWS::Rails.add_action_mailer_delivery_method(:aws_ses, {config: AWS::Core::Configuration.new(http_handler: AWS::Core::Http::Handler.new(AWS::Core::Http::CurbHandler.new) { |req, resp|
    super(req, resp)
    unless 200 == resp.status
      email_param = req.params.find{ |param| param.name == 'Destinations.member.1' }
      if email_param
        log.error("AWS SES ERROR: email=#{email_param.value} response=#{resp.body}")
      else
        log.error("AWS SES ERROR: request=#{req.body} response=#{resp.body}")
      end
    end
  }),
                                                          async: true})
  config.action_mailer.delivery_method = (ENV['EMAIL_DELIVERY_METHOD'] ? ENV['EMAIL_DELIVERY_METHOD'].to_sym : :smtp) #possible values: smtp, aws_ses. Default is to use smtp.
  config.action_mailer.perform_deliveries = true
  config.action_mailer.raise_delivery_errors = true

  # paperclip for storing user profile pictures
  config.paperclip_defaults = {
      :storage => :s3,
      :s3_credentials => {
          :bucket => Settings.s3_bucket_name,
          :access_key_id => Settings.aws_access_key_id,
            :secret_access_key => Settings.aws_secret_access_key
      },
      :s3_host_alias => Settings.aws_cdn_host,
      :s3_headers => { 'Cache-Control' => 'max-age=315576000' },
      :url => ':s3_alias_url',
      :path => '/:class/:attachment/:id_partition/:style/:filename',
      :s3_protocol => '',
      s3_server_side_encryption: 'AES256'
  }
  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation can not be found).
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify

  # Disable automatic flushing of the log to improve performance.
  # config.autoflush_log = false

  config.action_controller.default_asset_host_protocol = :relative

  config.action_mailer.smtp_settings = {
    :address => ENV['AWS_SMTP_SERVER'],
    :authentication => (ENV['AWS_SMTP_AUTHENTICATION'] ? ENV['AWS_SMTP_AUTHENTICATION'].to_sym : :login),
    :user_name => ENV['AWS_SMTP_LOGIN'],
    :password => ENV['AWS_SMTP_PASSWORD'],
    :enable_starttls_auto => (ENV['AWS_SMTP_TLS'] ? ENV['AWS_SMTP_TLS'] == 'true' : true),
    :port => ENV['AWS_SMTP_PORT'] || 465
  }

  # Show referrer and user_agent in request logs; depends on custom ApplicationController.append_info_to_payload
  config.lograge.enabled = true
  config.lograge.custom_options = lambda do |event|
    event.payload.slice(:host, :referrer, :build_number, :user_agent).merge({
      elasticsearch_runtime: event.payload[:elasticsearch_runtime].to_f.round(2),
      searchkick_runtime: event.payload[:searchkick_runtime].to_f.round(2),
      time: event.time,
      params: (event.payload[:params] || {}).except(*['controller', 'action', 'format']),
      request_host: event.payload[:request_host],
      request_ip: event.payload[:request_ip]
    })
  end

  # Tag all log entries with UUID
  config.log_tags = [:uuid]
  config.react.variant = :production #minified reactjs code

# Use sneakers job queue in production
  unless ENV['INLINE_BACKGROUND_JOBS'] == 'true'
    config.active_job.queue_adapter = :sidekiq
  end

  if Settings.aws_cache_endpoint.present? # allows the task server to default to FileSystem cache store
    #caching setup. development/test will default to FileSystem cache store.
    elasticache = Dalli::ElastiCache.new(Settings.aws_cache_endpoint)
    config.cache_store = :dalli_store, elasticache.servers, {:compress => true, namespace: Settings.aws_cache_namespace, :pool_size => Integer(ENV['MAX_THREADS'] || 16)}
  end
end

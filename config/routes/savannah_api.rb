namespace :savannah_api do
  resource :mailer_config, only: [:destroy] do
    post :save
  end

  resource :app_config, only: [:destroy] do
    post :save
  end
end

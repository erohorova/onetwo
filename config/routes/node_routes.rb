# Dummy routes for all routes handled by the node server. These are here so that we can continue to use rails routes helpers
get 'video_streams/:id' => 'dev#null', as: :video_stream

namespace :cms do
  namespace :api do
    resources :cards, only: [:index, :update], defaults: {format: :json} do
      collection do
        post 'publish', to: 'cards#publish'
        post 'archive', to: 'cards#archive'
        post 'delete', to: 'cards#delete'
        get ':id/history', to: 'cards#history'
      end
    end

    resources :video_streams, only: [:index, :update], defaults: {format: :json} do
      collection do
        post 'publish', to: 'video_streams#publish'
        post 'delete', to: 'video_streams#delete'
      end
    end

    resources :courses, only: [:index, :update, :show], defaults: {format: :json} do
      collection do
        post 'promote', to: 'courses#promote'
        post 'unpromote', to: 'courses#unpromote'
        post 'hide', to: 'courses#hide'
        post 'unhide', to: 'courses#unhide'
      end
    end

    # card packs
    get 'collection', to: 'card_packs#index', defaults: {format: :json}
    put 'collection/:id', to: 'card_packs#update', defaults: {format: :json}
    post 'collection/publish', to: 'card_packs#publish', defaults: {format: :json}
    post 'collection/delete', to: 'card_packs#delete', defaults: {format: :json}

    get 'topics/suggest', to: 'topics#suggest', defaults: {format: :json}

    # users
    put 'users/bulk_upload', to: 'users#bulk_upload', defaults: {format: :json}
    put 'users/bulk_suspend_upload', to: 'users#bulk_suspend_upload', defaults: {format: :json}
    put 'users/:id', to: 'users#update', defaults: {format: :json}
    put 'users/:id/assign_roles', to: 'users#assign_roles', defaults: {format: :json}
    post 'users/preview_bulk_import', to: 'users#preview_bulk_import', defaults: {format: :json}

    resources :roles, only: [:index, :create, :show, :destroy, :update] , defaults: {format: :json}do
      member do
        post "add_users"
        post "remove_users"
      end
      member do
        post "assign_permissions"
      end
    end
  end
  get '(/*other)', to: 'base#home'
end

namespace :api do
  namespace :developer do
    namespace :v1 do
      resources :users do
        get 'auth', on: :collection
        post 'suspend', on: :collection
      end
      resources :channels, only: [:index, :show]

      resources :cards, only: [:index, :create, :update, :show, :destroy] do
        resources :comments, only: [:index, :show, :create, :destroy]
      end
      resources :search, only: [:index]
      post 'votes', to: 'votes#create'
      delete 'votes', to: 'votes#destroy'
      get :events, to: 'events#index'
      get :transactions, to: 'settlements#transactions'

      resources :organizations do
        get 'lrs_credentials', on: :collection
      end
    end
    namespace :v5 do
      get 'auth', to: 'external_authentication#auth', format: :json
      resources :profiles, only: [:index,:create, :show, :update, :destroy], format: :json do
        collection do
          get 'custom_fields', to: 'profiles#index_custom_fields'
          post 'custom_fields', to: 'profiles#create_custom_fields'
        end
      end
      resources :search, only: [:index]
      resources :mdp_cards, only: [:index], format: :json
      resources :feed do
        get 'index', on: :collection
      end
      resources :users do
        get 'clc', on: :collection
      end
      
      resources :assignments do
        get 'index', on: :collection
      end
    end
  end
end

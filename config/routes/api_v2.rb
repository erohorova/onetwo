namespace :api do
  namespace :v2 do

    namespace :admin_events do
      post 'integration_ecl_content_access_enabled'
      post 'integration_ecl_content_access_disabled'
      post 'integration_added'
      post 'integration_removed'
      post 'integration_activated'
      post 'integration_deactivated'
      post 'integration_featured'
      post 'integration_unfeatured'
      post 'integration_edited'
      post 'integration_content_fetched'

      post 'lrs_integration_added'
      post 'lrs_integration_activated'
      post 'lrs_integration_deactivated'
      post 'lrs_integration_edited'
      post 'lrs_integration_removed'
    end

    get 'knowledge_graph', to: 'knowledge_graph#knowledge_graph'

    namespace :superadmin do
      get 'platform_metrics', to: "platform_metrics#index"
      resources :users, only: :create, defaults: {format: :json}
    end

    resources :org_ssos, only: [:create, :update] do
      put :toggle, on: :member
      put :batch_update, on: :collection
    end

    resources :structures, only: [:index, :create, :update, :destroy, :show], defaults: {format: :json}  do
      resources :structured_items, only: [:index, :create], defaults: {format: :json} do
        collection do
          get :all_items
          post :bulk_create
          delete :bulk_remove
        end
      end
    end
    get 'structured_items/entities_data', to: 'structured_items#entities_data'
    put 'structures/:id/reorder', to: 'structures#reorder'
    delete 'structures/:structure_id/remove_item' , to: 'structured_items#remove'

    get 'verify_badge/:identifier', to: 'users#verify_badge'
    get 'filestack/signed_url', to: 'filestack_security#signed_url'
    get 'filestack/upload_policy_and_signature', to: 'filestack_security#upload_policy_and_signature'

    resources :teams, only: [:index, :create, :destroy, :update, :show], defaults: {format: :json} do
      resources :comments, only: [:index, :show, :create, :destroy]
      member do
        get 'users'
        get 'channels'
        get 'addable_channels'
        get 'cards'
        get :activity_streams
        post 'invite'
        post :add, to: 'teams#add_users'
        get 'analytics'
        get 'top_contributors'
        get 'top_content'
        delete 'remove_shared', to: 'teams#remove_shared_card'
        delete 'users', to: 'teams#remove_users'
        put 'users/:user_id', to: 'teams#change_user_role'
        post 'accept_invite', to: 'teams#accept_invite'
        delete 'remove_teams_type', to: 'teams#remove_teams_type'
        post 'decline_invite', to: 'teams#decline_invite'
        post 'join', to: 'teams#join'
        post 'announcement'
        delete 'leave', to: 'teams#leave'
        get 'analytics_v2'
        put :carousels
      end
      collection do
        get 'metrics_v2'
        get :metrics
        get :search
      end
    end

    resources :project_submissions, only: [:index, :create], defaults: {format: :json} do
      resources :project_submission_reviews, only: [:index, :create], defaults: {format: :json}
    end

    get 'cards/user_metric', to: 'cards#cards_user_metric'
    get 'cards/new_content_count', to: 'cards#new_content_count'
    put 'cards/course_status', to: 'cards#course_status'

    resources :cards, only: [:index, :create, :edit, :update, :show, :destroy], defaults: {format: :json} do
      resources :comments, only: [:index, :show, :create, :destroy]
      resources :leaps, only: [:create, :update, :destroy]
      member do
        post :add
        post :journey_add
        post :journey_batch_add
        post :add_to_journeys
        post :share
        post :post_to_channel
        post :batch_add
        post :add_to_pathways
        post :remove
        post :journey_remove
        post :publish
        post :reorder
        post :responses
        put  :rate
        post :complete
        post :uncomplete
        put :start
        get :report
        post :dismiss
        get :addable_pathways
        get :addable_to_journeys
        get :addable_journeys
        get :metric
        get :metric_users
        get :time_range_analytics
        post :set_lock
        post :journey_reorder
        post :custom_order
        put :promote
        put :unpromote
        put :transfer_ownership
        post :trash
        put :unreport
        get :card_reportings
        get :assignable
        get :pathway, to: 'pathways#show'
        get :secured_urls
        delete :remove_assignments
        post :add_to_mdp
        get :mdp_card_detail
      end
      collection do
        post :publish, to: 'cards#publish_cards'
        post :archive, to: 'cards#archive_cards'
        post :delete, to: 'cards#destroy_cards'
        get :purchased, to: 'cards#purchased'
        get :card_by_ids, to: 'cards#card_by_ids'
        get :deleted_cards, to: 'cards#deleted_cards'
      end
    end

    get :duplicate_cards, to: 'cards#duplicate_cards', defaults: {format: :json}

    resources :cards, defaults: {format: :json}

    post :batch_remove, path: '/pathways/:id/cards/batch_remove', controller: 'cards'

    resources :cards, path: 'journey', defaults: {format: :json} do
      member do
        post :journey_batch_remove, path: 'cards/journey_batch_remove'
      end
    end

    resources :channels, only: [:index, :create, :update, :show, :destroy], defaults: {format: :json} do
      resources :teams, controller: 'channels/teams' do
        delete '/', to: 'channels/teams#destroy', on: :collection
      end
      resources :cards, only: [] do
        member do
          put :curate
          put :skip
          put :archive
        end
      end
      member do
        put :add_collaborators
        put :remove_collaborators
        put :add_curators
        put :remove_curators
        get :curators
        get :not_curators
        get :followers
        put :remove_followers
        get :contributors
        get :not_contributors
        post :follow
        post :unfollow
        get :versions
        put :carousels

        delete :remove_cards

        get :ecl_sources, to: 'channels#list_ecl_sources'
        put :ecl_sources, to: 'channels#add_ecl_source'
        put :add_ecl_source_list, to: 'channels#add_ecl_source_list'
        put :programming, to: 'channels#programming'
        delete :ecl_sources, to: 'channels#remove_ecl_source'
        delete :remove_ecl_source_list, to: 'channels#remove_ecl_source_list'

        get :topics, to: 'channels#list_topics'
        put :topics, to: 'channels#add_topic'
        put :add_topic_list, to: 'channels#add_topic_list'
        delete :topics, to: 'channels#remove_topic'
        delete :remove_topic_list, to: 'channels#remove_topic_list'
        get :cards
        put :promote
        put :demote
        get :aggregated_metrics
        get :top_cards
        get :top_users
        get :trusted_collaborators
        get :search_cards
      end
      collection do
        get :as_curator
        get :suggestions
        post :batch_promote
        post :batch_demote
        get :search
      end
    end

    put 'channels/:id/cards/:card_id/reorder', to: 'channels#reorder_card'
    post 'purchase/new', to: 'transactions#initiate_transaction'
    post 'purchase/complete', to: 'transactions#process_payment'
    post 'purchase/cancel', to: 'transactions#cancel'
    post 'wallet_purchase/new', to: 'wallet_transactions#initiate_transaction'
    post 'wallet_purchase/complete', to: 'wallet_transactions#process_payment'

    get 'wallet/recharges', to: 'recharges#index'

    get 'users/profile', to: 'users#user_profile'
    get 'users/projects', to: 'users#projects'
    get 'users/wallet_balance', to: 'users#skillcoin'
    get 'public_profile/:handle', to: 'users#public_profile'
    resources :users, only: [:index, :update, :show], defaults: {format: :json} do
      collection do
        get :accessible_candidates
        get :following
        get :followers
        get :suggested
        put :toggle_role
        get :courses
        get :integrations
        put :update_password
        get :magic_link
        get :validate_password_reset_token
        get :login_landing
        get :join_url_mobile
        get :info
        get :onboarding_status
        put :onboarding
        post :reset_onboarding
        get :recommended_users
        get :magic_link_login
        get :pubnub_channels
        get :notification_config, to: 'users#get_notification_config'
        put :notification_config, to: 'users#update_notification_config'
        get :dev_api_token
        get :enrolled_courses
        get :wallet_api_token
        get :clc
        get :assignments_by_assignor
        post :providers_assignment
        post :email_signup
        post :bulk_suspend
        get :managed_cards
        get :activity_streams
        get :wallet_transactions, to: 'users#transactions'
        get :shared_cards, to: 'users#shared_cards'
        post :start_data_export
        get :data_export
        post :delete_account
        post :clear_search_history
        get :paid_users
        post :data_download_request
        get 'fields', to: 'users#users_fields'
        get 'fields/:name/values', to: 'users#field_values'
        get :transcript, to: 'users#user_transcript'
        get :profiles
        get :last_viewed
      end

      member do
        post :suspend
        post :anonymize
        post :unlock
        post :unsuspend
        post :user_status
        get :integrations
        get :courses
        get :skills
        delete :remove_skill
        get :learning_topics
        get :pwc_records
        get :expert_topics
        get :terms_and_conditions
        post :follow
        post :unfollow
        get :badges
        put :invitation
        post :exclude_from_leaderboard_status
        post :admin_delete_account
        get :peer_learning
      end
      collection do
        get :search
      end
    end

    resources :user_import_statuses, only: [:index, :show]

    resources :notifications, only:[:index], defaults: {format: :json} do
      member do
        post :read
      end
      collection do
        post :seen_at
        post :push_to_org
      end
    end

    resources :user_content_completions, only: [:index]
    resources :pronouncements, except: [:new, :edit]

    resources :integrations, only: [] do
      member do
        put :toggle, to: 'integrations#toggle_integration'
      end
    end

    resources :courses, only: [] do
      collection do
        post :refresh_external_courses
        get  :export
      end
    end

    resources :assignments, only: [:index, :create], defaults: {format: :json} do
      collection do
        get :metrics
        get :metric_users
        get :notice
        post :dismiss_notice
        post :notify
        post :dismiss
        get :assigned
      end
    end

    resources :search, only: [:index] do
      collection do
        get :initial_state
        get :related
        get :private_content
      end
    end

    resources :suggestions, only: [:index]

    put 'roles/permission', to: "roles#role_permission"
    resources :roles, only: [:create, :update, :show, :index, :destroy], defaults: {format: :json}

    resources :file_resources, only: [:create, :show, :destroy]


    resources :clcs, only: [:create, :index]

    resources :pins, only: [:index]
    post 'pin', to: 'pins#pin'
    delete 'unpin', to: 'pins#unpin'

    get 'feed', to: 'feed#index', defaults: {format: :json}
    get 'feed/items_count', to: 'feed#feed_items_count', defaults: {format: :json}

    # Bookmarks
    get 'bookmarks', to: 'bookmarks#index'
    post 'bookmarks', to: 'bookmarks#create'
    delete 'bookmarks', to: 'bookmarks#destroy'

    # Votes
    post 'votes', to: 'votes#create'
    delete 'votes', to: 'votes#destroy'

    # Analytics
    resources :analytics, only: [:create] do
      collection do
        post :record_event
        get :group_performance
        post :visit
      end
    end

    resources :credentials, only: [:index, :create, :destroy]
    resources :sources_credentials, only: [:index, :create, :destroy, :update, :show]

    # Organizations
    resources :organizations, only: [:index] do
      resources :comments, only: [:index, :show, :create, :destroy]
      collection do
        get :details, to: 'organizations#show'
        put :details, to: 'organizations#update'
        get :leaderboard
        get :leaderboard_v2
        get :settings
        get :teams
        get :channels
        get :metrics
        get :shareable_channels
        get :shared_channels
        get :cloned_channels
        get :content_metrics
        get :assessments
        get :channel_performance
        get :ssos, to: 'organizations#list_ssos'
        put :add_lms_users
        post :push_ecl_content_to_channels
        get :notification_config, to: 'organizations#get_notification_config'
        put :notification_config, to: 'organizations#update_notification_config'
        get :badges, to: 'organizations#badges'
        post :badges, to: 'organizations#create_badges'
        get :restrictable_users_or_teams, to: 'organizations#restrictable_users_or_teams'
      end
    end

    post 'organizations/settings', to: 'organizations#create_setting'
    put 'organizations/settings/:id', to: 'organizations#update_setting'
    put 'organizations/ssos/:sso_name/enable', to: 'organizations#enable_sso'
    put 'organizations/ssos/:sso_name/disable', to: 'organizations#disable_sso'

    resources :discover, only: [] do
      collection do
        get 'items_count'
        get 'trending_cards'
        get 'trending_pathways'
        get 'trending_journeys'
        get 'cards_by_channels'
        get 'interested_cards'
        get 'user_interested_cards'
        get 'channels'
        get 'videos'
        get 'carousel_content'
      end
    end

    # learning queue items
    resources :learning_queue_items, only: [:index], defaults: {format: :json}

    resources :aws_notifications, only: [:create]

    resources :activity_streams, only: [:index, :destroy] do
      resources :comments, only: [:index, :show, :create, :destroy]
    end

    resources :pathways, only: [:show, :edit] do
      post :add_cards, on: :collection
    end

    resources :journeys, only: [:show] do
      member do
        get :pathway_cards
      end
    end

    resources :user_level_metrics, only: [:index], defaults: {format: :json} do
      collection do
        get :trend
      end
    end

    resources :video_streams, only: [:show], defaults: {format: :json} do
      collection do
        get :uuid
      end

      member do
        post :start
        post :stop
        post :join
        post :leave
        post :ping
        post :recordings, to: 'video_streams#create_recording'
        get :recordings
        get :viewers
        put :retranscode
        get :presigned_post_url
        post :record_download_metric
        post :record_preview_metric
      end
    end

    resource :user_profile, only: [:create]

    resources :resources, only: [:update, :create]
    resources :skills, only: [:index, :create, :update]
    resources :user_daily_learnings, only: [:index] do
      collection do
        get :available
        get :search_user_daily_learnings
      end
    end

    resources :mlp, only: [] do
      collection do
        get :latest_assignments
      end
    end

    resources :custom_fields, only: [:create, :destroy, :index, :update] do
      member do
        post 'config', to: 'custom_fields#custom_field_config'
      end
    end
    resources :user_custom_fields, only: [:index, :create, :destroy]
    get :user_custom_fields_data, to: 'user_custom_fields#user_custom_fields_data', format: :json
    post 'user_custom_fields/update_all', to: 'user_custom_fields#update_all'
    resources :partner_centers
    get 'career_advisor', to: 'career_advisors#show', format: :json
    put 'career_advisor', to: 'career_advisors#update', format: :json

    resources :recommendations, only: :index
    resources :embargo, only: [:index, :show, :update] do
      collection do
        post :batch_create
        post :batch_remove
        get :preview_csv
        post :bulk_upload
      end
    end

    resources :card_reportings, only: [:create, :index] do
      collection  do
        get :search
      end
    end

    resources :comment_reportings, only: [:create, :index], defaults: {format: :json}
    delete 'comments/:id/trash', to: 'comments#trash' , format: :json
    put 'comments/:id/unreport', to: 'comments#unreport', format: :json

    ########ecl webhook
    resources :ecl, only: [:destroy] do
      collection do
        post :sync_card
        post :sync_team_sources
        delete :delete_cards_of_source
        post :sync_source_cards
      end
    end

    resources :email_templates, only: %i[create update destroy show index] do
      member do
        get :activate
        get :disable
      end
      collection { post :send_custom_template }
    end

    resources :manage_email_senders, only: %i[create update destroy show index], defaults: {format: :json} do
      member do
        get :verify_domain
      end
    end

    resources :curriculums, only: [:create]

    resources :topic_requests, only: [:create, :index], defaults: {format: :json} do
      member do
        put :approve
        put :reject
        put :publish
      end
       collection do
        get :aggregate_data
        post :batch_create
      end
    end

    resources :versions, only: [:index]

    resources :workflows, defaults: {format: :json} do
      member do
        put :toggle
        post :run
      end
      collection do
        get 'sources/:id', to: 'workflows#list'
        get 'sources/profile/columns', to: 'workflows#profile_columns'
        get :input_source
        post 'handle/patches', to: 'workflows#handle_patches'
        post 'dynamic-selections', to: 'workflows#dynamic_selections'
      end
    end

    resources :translations, only: [:create]
    resources :translation_languages, only: [:index]

    resources :widgets, only: [:index, :create, :update, :show, :destroy], defaults: { format: :json }
  end
end

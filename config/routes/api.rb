namespace :api do
	# Feed routes
  get 'feed_promos/channels', to: 'feed_promotions#channels'
  get 'feed_promos/courses', to: 'feed_promotions#courses'

  resources :user_onboardings, only:[:index, :update] do
    collection do
      get :websites
    end
  end

  get 'suggest/initial_state', to: 'suggestions#initial_state'
  get 'suggest', to: 'suggestions#suggest'
  get 'suggest_interests', to: 'suggestions#suggest_interests'
  get 'suggest_tags', to: 'suggestions#suggest_tags'

  get 'scorm/launch', to: 'scorm#launch'
  get 'scorm/close', to: 'scorm#close'
  post 'scorm/completion', to: 'scorm#completion'
  get '/pwc_callback', to: 'user_interests#pwc_callback'
  get '/profile_pwc_callback', to: 'user_interests#profile_pwc_callback'

  get 'niit/launch', to: "niit#launch"

  post 'cards/:card_id/comments', to: '/comments#create'
  get 'cards/:card_id/comments', to: '/comments#index'
  delete 'cards/:card_id/comments/:id', to: '/comments#destroy'
  put 'cards/:card_id/comments/:id', to: '/comments#update'

  get '/search', to: 'card_search#search'

  # User routes
  resources :users, only: [:index] do
    post 'follow', to: '/follows#create'
    post 'unfollow', to: '/follows#destroy'
    put 'change_role', action: :toggle_role
    member do
      post 'suspend'
      post 'unsuspend'
    end
    collection do
      get 'token'
    end
    get 'integrations', action: :integrations
    put 'toggle_integration', action: :toggle_integration
    post 'fetch_external_courses', action: :fetch_external_courses
    get 'my_courses', action: :my_courses
    put 'toggle_profile_pref', as: :toggle_profile_pref
    get 'interests', action: :interests, as: :competencies
  end
  get 'users/nav', to: 'users#top_nav'

  get 'users/groups', to: '/users#groups'
  get 'users/info', to: '/users#info'
  get 'users/info/following', to: '/users#following_users'
  get 'suggested_users', to: '/users#suggested_users'
  get 'users/permissions', to: '/users#permissions'
  # Alias to suggested users, below API endpoints will be used for future releases
  get 'users/recommended', to: '/users#suggested_users'
  get 'users/:id/basic_info', to: '/users#basic_info'
  get 'users/:id/cards', to: '/users#cards'
  get 'users/:id/streams', to: '/users#streams'
  post 'users/confirmed', to: '/users#confirmed'
  post 'users/set_config', to: '/users#set_config'
  get 'user/:id/followers', to: '/users#followers'

  # Post routes
  resources :posts, only: [] do
    post 'follow', to: '/follows#create'
    post 'unfollow', to: '/follows#destroy'
  end
  post 'posts/:post_id/comments', to: '/comments#create'
  get 'posts/:post_id/comments', to: '/comments#index'

  # Course routes
  resources :courses, only: [:index] do
    member do
      get  :course_structure
      get 'last_accessed', to: 'courses#get_last_accessed'
      put 'last_accessed', to: 'courses#set_last_accessed'
      get 'course_progress', to: 'courses#course_progress'
      post :enroll
    end
  end

  # Webex routes
  resources :webex_configs, only: [:create] do
    collection do
      get 'get_webex_credentials'
    end
  end

  resources :dismissed_contents, only:[:create]

  # Set and delete interests for a user
  get 'user_interests', to: 'user_interests#index'
  post 'user_interests', to: 'user_interests#create'
  delete 'user_interests', to: 'user_interests#destroy'
  get 'user_interests/suggested', to: 'user_interests#suggested'

  # org settings
  resources :organizations, only: [:index]

  get :org_details,       to: 'organizations#show', as: :org_details
  put :org_settings,      to: 'organizations#set_org_settings'
  get :org_settings,      to: 'organizations#read_org_settings'

  get 'organizations/host_name_check', to: 'organizations#host_name_check', as: :org_host_name_check
  get 'organizations/settings', to: 'organizations#settings'
  put 'organizations/settings', to: 'organizations#update'
  post 'organizations/create_from_savannah', to: 'organizations#create_from_savannah'

  post 'organizations/setup', to: 'organizations#create_org_with_admin'
  post 'organizations/activate', to: 'organizations#activate_org'

  # team
  resources :teams, except: [:edit, :new] do
    member do
      get 'users',                    to: 'teams#team_users'
      post 'follow_all',               to: 'teams#follow_all'
      get 'assignments',              to: "teams#assignments"
      patch 'users/:user_id/action',  to: 'teams#user_action'
      delete 'users/:user_id/action', to: 'teams#user_action'
    end
    collection do
      get 'assigned_groups' , to: 'teams#assigned_groups'
    end
  end

  # Miscellaneous routes
  get 'settings/version', to: "settings#version"
  get 'groups/:id/course_data', to: '/groups#course_data'

  get 'notifications', to: '/notifications#index',:defaults => { :format => 'json' }

  get 's3_uploads/configuration', to: 's3_uploads#configuration'
  post 's3_bucket/get_content', to: 's3_uploads#get_content'


  get 'current_orgs', to: 'users#current_orgs'

  # School - Users Education Section on Profile Routes
  get 'users/:user_id/schools', to: 'schools#index', as: :users_schools
  post 'users/:user_id/schools', to: 'schools#create'
  get 'users/:user_id/schools/:id', to: 'schools#show', as: :users_school
  put 'users/:user_id/schools/:id', to: 'schools#update'
  delete 'users/:user_id/schools/:id', to: 'schools#destroy'

  ## Analytics APIs
  get 'user_content_level_metrics', to: 'user_content_level_metrics#index', as: :user_content_level_metrics
  get 'content_level_metrics', to: 'content_level_metrics#index', as: :content_level_metrics
  get 'user_level_metrics', to: 'user_level_metrics#index', as: :user_level_metrics
  get 'user_level_metrics/trend', to: 'user_level_metrics#trend', as: :user_level_metrics_trend
  get 'organization_metrics', to: 'user_level_metrics#organization_metrics', as: :organization_metrics

  get 'knowledge_map', to: 'user_topic_level_metrics#knowledge_map'
  get 'topics/:topic_id/knowledge_map', to: 'user_topic_level_metrics#knowledge_map'
  get 'users/:user_id/knowledge_map', to: 'user_topic_level_metrics#knowledge_map'
  get 'discover_items/counts', to: 'discover_items#counts'

  post :team_notifications, to: 'team_notifications#create'
  resources :configs, only: [:index]

  resources :xapi_credentials, only: [:index, :create, :update] do
    collection do
      put :activate
      put :deactivate
    end
  end
  resources :xapi_credentials, only: [:index, :create, :update]

  get 'email_test/weekly_activity', to: 'email_test#weekly_activity'

end

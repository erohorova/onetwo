require 'sidekiq/web'
class ActionDispatch::Routing::Mapper
  def draw(routes_name)
    instance_eval(File.read(Rails.root.join("config/routes/#{routes_name}.rb")))
  end
end

Edcast::Application.routes.draw do

  if Rails.env.development?
    mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/graphql"
  end

  post "/graphql", to: "graphql#execute"
  use_doorkeeper do
    # it accepts :authorizations, :tokens, :applications and :authorized_applications
    skip_controllers :applications, :authorized_applications
  end

  ## WEBAPP ENDPOINTS

  devise_for :users, path_prefix: 'auth', controllers: { sessions: "users/sessions", confirmations: 'confirmations', registrations: 'registrations', passwords: 'passwords' }
  apipie

  controller 'user_pages' do
    get 'settings(/:tab)', action: 'settings', as: 'settings'
    get 'notifications'
    get 'channels(/:tab)', action: 'channels', as: :user_channels_page
    put 'edit_settings'
    patch 'handle'
    get '/users/:handle', to: 'user_pages#check_handle', as: 'check_handle'
    # get 'about'
  end

  get 'video_streams/:id/signed_playback_url', to: 'video_streams#signed_playback_url'
  post 'video_streams/aws_notifications', to: 'aws_notifications#create'

  get '401', :to => 'errors#unauthorized', as: 'unauthorized'
  get '403', :to => 'errors#forbidden', as: 'forbidden'
  get '404', :to => 'errors#not_found', as: 'not_found'

  get 'join/:token', to: 'referrals#join', as: :join
  post 'join/:token', to: 'referrals#join'

  devise_scope :user do
    get 'sign_out', to: 'users/sessions#destroy'
    get 'choose_organization', to: 'users/sessions#choose_organization'
    post 'switch_organization', to: 'users/sessions#switch_organization'
    get 'find_organization', to: 'users/sessions#find_organization'
    post 'find_organization', to: 'users/sessions#find_organization'
    get 'log_in', to: 'users/sessions#new', as: 'new_user_session', override_helpers: true
    get 'sign_up', to: 'registrations#new', as: 'new_user_registration', override_helpers: true
    get 'forgot_password', to: 'passwords#new', as: 'new_user_password', override_helpers: true
    get '/auth/users/establish_handshake', to: 'users/sessions#establish_handshake'
  end
  get 'confirmation', to: 'users/profile#continue', as: :proceed_confirmation
  scope :profile do
    get '', to: 'users/profile#show', as: :user_profile
    get 'edit', to: 'users/profile#edit', as: :edit_user_profile
    patch '', to: 'users/profile#update'
    get 'sign_up/confirmation', to: redirect('/confirmation')
  end
  root 'welcome#index'

  get 'about', to: 'welcome#about'
  get 'careers', to: 'welcome#jobs', as: 'jobs'
  get 'contact', to: 'welcome#contact'
  get 'tos', to: 'welcome#tos'
  get 'privacy', to: 'welcome#privacy'
  get 'ferpa', to: 'welcome#ferpa'
  get 'partners', to: 'welcome#partners'
  get 'eula', to: 'welcome#eula'

  get '/users/groups/:group_id/score', to: 'users#score'
  get 'users/:user_id/user_profile', to: 'users#user_profile'

  get 'developer',                to: 'developers#dev_redir'
  get 'developers',               to: 'developers#index',        as: :dev_home
  get 'developers/manage',        to: 'clients#manage',          as: :dev_manage
  get 'developers/features',      to: 'developers#features',     as: :dev_features
  get 'developers/docs',          to: 'developers#docs',         as: :dev_docs
  get 'developers/invitation',    to: 'developers#invitation',   as: :dev_invitation
  get 'developers/invite',        to: 'developers#invite',       as: :dev_invite
  get 'developers/join/:token',   to: 'developers#join',         as: :dev_join
  post 'developers/admin_create', to: 'developers#admin_create', as: :dev_admin_create
  scope '/developers' do
    resources :background_jobs, only: [:index] do
      member do
        post 'reenqueue', to: 'background_jobs#reenqueue'
      end
    end
  end

  get 'dashboard_stream', to: 'cards#dashboard_stream'
  resources :posts, except: [:new]
  resources :groups, only: [] do
    resources :activity_records, only: [:create]
    resources :notifications, only: [:index]
    resources :events do
      member do
        get 'watch', to: 'events#watch'
      end
    end
    resources :posts, except: [:new] do
      get :filters, on: :collection

      resources :reports, only: [:create]
      get 'reports/stats', to: 'reports#stats'
    end

    member do
      get :preferences
      put :change_preference
      resources :private_groups, only: [:index, :create] do
        collection do
          get :search
          get :enrolled_groups
          get :open_groups
        end
      end
    end

    member do
      resources :users, only: [:index]
    end

    resources :users, only: [] do
      member do
        get 'stats' #show stats for the user belonging to this group
      end
    end

    resources :streams, only: [:index]
  end

  resources :private_groups, except: [:index, :create, :search] do
    member do
      get 'users'
      post 'add_user'
      delete 'remove_user'
      post 'ban_user'
      delete 'unban_user'
    end
  end

  post 'groups/:group_id/posts/:id/restore', to: 'posts#restore'
  get '/groups/:id/stats', to: 'groups#stats'
  get '/groups/:id/right_rail_stats', to: 'groups#right_rail_stats'
  get '/groups/:id/contexts', to: 'groups#contexts'
  get '/groups/:id/topic_stats', to: 'groups#topic_stats'
  post '/groups/upload_s3_urls', to: 'groups#upload_s3_urls'
  get '/groups/channels', to: 'groups#channels'
  get '/groups/forum_channels', to: 'groups#forum_channels'

  get 'course_offerings/:client_resource_id/posts', to: 'posts#index'

  get '/forum_data', to: 'groups#forum_data'
  get '/addon_stats', to: 'groups#addon_stats'
  post '/add_user', to: 'groups#add_user'
  post '/remove_user', to: 'groups#remove_user'
  post '/close_group', to: 'groups#close'
  post '/sync_course_data', to: 'groups#sync_course_data'
  get '/users/:id/org_details', to: 'groups#user_org_details'
  get '/verify_badge/:identifier', to: 'users#verify_badge'

  get 'current_user', to: 'users#current'
  resources :users, only: [:update] do
    member do
      get 'addable_private_groups'
    end

  end

  resources :posts, only: [:destroy, :show] do
    resources :comments, except: [:new] do
      resources :reports, only: [:create]
      get 'reports/stats', to: 'reports#stats'
    end
    resources :votes, only: [:create]
    delete 'votes', to: 'votes#destroy'
    resources :approvals, only: [:create]
    delete 'approvals', to: 'approvals#destroy'
    resources :reports, only: [:create]
    get 'reports/stats', to: 'reports#stats'
    post 'read', to: 'post_reads#create'
    member do
      post 'pin'
      post 'restore'
      post 'convert_to_card'
    end
  end

  resources :comments, only: [:destroy] do
    resources :comments, only: [:index, :show, :create, :update, :destroy]
    resources :votes, only: [:create]
    delete 'votes', to: 'votes#destroy'
    resources :approvals, only: [:create]
    delete 'approvals', to: 'approvals#destroy'
    resources :reports, only: [:create]
    get 'reports/stats', to: 'reports#stats'
    member do
      post 'restore'
    end
  end

  resources :events, only: [] do
    resources :events_users, only: [:create, :update]
    resources :comments, except: [:new] do
      resources :reports, only: [:create]
      get 'reports/stats', to: 'reports#stats'
    end
    resources :votes, only: [:create]
    delete 'votes', to: 'votes#destroy'
    resources :approvals, only: [:create]
    delete 'approvals', to: 'approvals#destroy'
    resources :reports, only: [:create]
    get 'reports/stats', to: 'reports#stats'
    post 'read', to: 'post_reads#create'
  end

  post 'events/:event_id/comments/:id/restore', to: 'comments#restore'
  get 'groups/:group_id/plot/events', to: 'events#plot'
  get 'groups/:group_id/webex_enabled/events', to: 'events#webex_enabled'
  post 'posts/:post_id/comments/:id/restore', to: 'comments#restore'

  resources :clients, only: [:index, :show, :new, :create, :destroy, :update] do
    resources :groups, only: [:index, :update] do
      member do
        post 'clear_posts'
        get 'data_dump'
      end
    end
  end

  resources :channels, only: [] do
    resources :posts, only: [:index, :create, :update, :destroy]
  end

  resources :channels, path: '/manage/channels', only: [:index]

  get '/manage/globalinfluencers', to: 'influencers#show'
  post '/manage/addinfluencers', to: 'influencers#create'
  patch '/manage/updateinfluencersvisibility', to: 'influencers#update_visibility'
  delete '/manage/removeinfluencers', to: 'influencers#destroy'

  get 'home(/:tab)', to: 'organizations#show', as: :organization_home
  get 'organizations/host_name_check', to: 'organizations#host_name_check', as: :org_host_name_check
  post 'organizations/add_user', to: 'organizations#add_user', as: :org_add_user

  resources :organizations, only: [:new, :create]

  get 'ui/qa', to: 'ui#qa'
  get 'ui/events', to: 'ui#events'
  get 'ui/cards', to: 'ui#cards'
  get 'ui/qa_v1', to: 'ui#qa_v1'

  scope module: 'api' do
    resources :cards, path: 'manage/insights', as: 'insights_manage', except: %i[show new]
    get 'create', to: 'cards#new', as: 'new_insights_manage'
  end

  get 'insights/:id/visit', to: 'insights#visit', as: :visit_insight
  get 'api/insights/:id/visit_url', to: 'insights#visit_url'

  post 'lti/forum/v1', to: 'lti#forum_v1'
  post 'lti/events', to: 'lti#events'

  get 'unsubscribe', to: 'follows#unsubscribe'

  get 'preferences', to: 'user_preferences#set_from_token'

  resources :user_preferences, only: [:index] do
    collection do
      patch :update #special route to update by key name rather than by id
      put :update #special route to update by key name rather than by id
    end
  end

  post 'file_resources', to: 'file_resources#create'
  post 'file_resources/v1', to: 'file_resources#create_v1'
  get 'file_resources/:id', to: 'file_resources#show'

  get '/auth/:provider/callback', to: 'sessions#create'
  get '/auth/facebook/setup', to: 'sessions#setup'
  get '/auth/:provider/finish', to: 'sessions#finish', as: :auth_finish #org use case, redirect to the org's domain name then sign the user in
  get '/auth/native_scheme_redirect', to: 'sessions#native_scheme_redirect', as: :native_scheme_redirect #forward the sso to mobile app

  get 'confirm_email/:token', to: 'emails#confirm', as: :confirm_email_address
  post 'blacklist_email', to: 'mandrill#blacklist', as: :blacklist_email_address
  match 'blacklist_email', to: 'mandrill#get_blacklist', as: :get_blacklist_email_address, via: :head

  post 'hooks/events/hangout', to: 'events#hangout'

  get '/er/:id' => "shortener/shortened_urls#show"
  get '/~/:id' => "shortener/shortened_urls#show"

  get 'vimeo/:id', to: 'vimeo_container#show'
  get 'learn/:id', to: redirect {|_, request| request.fullpath.sub('/learn/', '/channel/') }

  get 'channel/:id', to: 'channels#show', as: :learn

  get 'dialog/share', to: 'widget#share', as: :share_dialog
  get 'dialog/share/preview', to: 'widget#preview', as: :share_preview
  get 'dialog/share/demo', to: 'widget#share_demo'

  resources :promotions, only: :show

  resources :teams, except: %i[index show edit]
  get 'teams/:id/settings/(:tab)', to: 'teams#settings', as: 'team_settings'
  get 'teams/:id/(:tab)', to: 'teams#show'

  # CMS ENDPOINTS
  draw :cms

  ## MOBILE ENDPOINTS
  draw :api
  draw :api_v2
  draw :node_routes

  ## SAVANNAH ENDPOINTS
  draw :savannah_api

  ## Public APIs
  draw :developer

  post '/auth/:token_provider/callback', to: 'sessions#create_mobile_session'

  ## SHARED MOBILE & WEBAPP ENDPOINTS

  post 'notifications/:id/read', to: 'notifications#read'
  post 'notifications/see_upto', to: 'notifications#see_upto'
  # /groups/:id/posts/[:index, :create]
  # posts comments
  # posts votes
  # comments votes

  get '/groups/:id/tags', to: 'groups#tags'
  get '/groups/:id/top_tags', to: 'groups#top_tags'
  get '/groups/search', to: 'groups#search', as: :groups_search

  post 'file_resources/v1', to: 'file_resources#create_v1'
  patch 'file_resources/v1', to: 'file_resources#create_v1'

  get 'widget/learn', to: 'widget#learn_button'
  get 'widget/share', to: 'widget#share_button'

  post 'video_streams/:video_stream_id/votes', to: 'votes#create'
  delete 'video_streams/:video_stream_id/votes', to: 'votes#destroy'

  get 'redirect', to: 'mobile_redirect#redirect', as: :mobile_redirect

  resources :follows, only: [:create]
  delete '/follows', to: 'follows#destroy'

  post 'metric_recorder', to: 'metric_recorder#create'

  resources :invitations do
    member do
      post 'resend'
    end
    collection do
      post 'upload'
    end
  end

  resources :courses do
    member do
      get 'link'
    end
  end

  resources :savannah do
    collection do
      get 'list_ssos'
      post 'enable_sso'
      post 'disable_sso'
    end
  end

  # Savannah Email Sso
  get  "sso_login",to: "savannah#sso_login"

  resources :org_ssos, only: [:update, :index] do
    member do
      post 'disable'
      post 'enable'
    end
    collection do
      post 'enable'
    end
  end

  get 'brightcove_videos', to: 'brightcove_videos#show'

  get 'superadmin', to: 'super_admin_session#new'
  post 'superadmin/submit', to: 'super_admin_session#submit'
  post 'superadmin', to: 'super_admin_session#create'
  get  "/courses/:course_id/redirect", to: "courses#redirect", as: :redirect_course

  # Notifications testing
  resources :notifications_explorer, only: [] do
    get :note_entry, on: :collection
  end

  resources :deeplink, only: [:index]

  # route for favicon.ico - request made by browser
  get '/favicon.ico', to: proc { [200, {}, ['']] }

  ## WEBAPP ENDPOINTS that should appear last
  mount Sidekiq::Web => '/sidekiq/jobs'
  get ':handle(/:tab)', to: 'user_pages#public', as: :public_user_page

  ## CATCH-ALL
  match '*path', to: 'application#unknown_route', via: :all
end

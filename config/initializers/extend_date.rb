class Date
  class << self
    # convert string to date with required format
    def strptime_with_format(date, format='%m/%d/%Y')
      Date.strptime(date,format) rescue nil
    end
  end
end

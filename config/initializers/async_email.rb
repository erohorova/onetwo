#Remove this after upgrading to rails 4.2
#Removed / commented
# require "#{Rails.root}/lib/action_mailer_4_2/delivery_job"
# require "#{Rails.root}/lib/action_mailer_4_2/message_delivery"


class ActionMailer::Base

  class << self
    def method_missing(method_name, *args) # :nodoc:
      if action_methods.include?(method_name.to_s)
        ::ActionMailer::MessageDelivery.new(self, method_name, *args)
      else
        super
      end
    end
  end

end

=begin
unless ENV['PRECOMPILE_MODE']
  (Proc.new do
    # Enable web mocks
    if Rails.env.test?
      WebMock.disable!
    end

    # List of fields to ensure proper typing
    fields_to_ensure = [
      {
        index_name: Card.searchkick_index.name, type_name: 'card',
        body: {
          _all: {
            analyzer: 'default_index'
          },
          properties: {
            ecl: {
              properties: {
                origin: {
                  type: 'string',
                  index: 'not_analyzed'
                }
              }
            }
          }
        }
      }
    ]

    # Establish connection
    conn = Faraday.new(url: "#{ENV['ELASTICSEARCH_URL'] || 'http://localhost:9200'}")
    conn.basic_auth(ENV['ELASTICSEARCH_CARDS_UID'] || '', ENV['ELASTICSEARCH_CARDS_PWD'] || '')

    fields_to_ensure.each do |field|
      conn.put do |req|
        req.url "#{field[:index_name]}/_mappings/#{field[:type_name]}"
        req.headers['Content-Type'] = 'application/json'
        req.body = field[:body].to_json
      end
    end

    # Disable web mocks
    if Rails.env.test?
      WebMock.disable_net_connect!
    end
  end).call
end
=end

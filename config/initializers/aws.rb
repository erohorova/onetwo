AWS.config(access_key_id:     Settings.aws_access_key_id,
           secret_access_key: Settings.aws_secret_access_key )

AWS_S3_TEMP_UPLOAD_BUCKET = AWS::S3.new.buckets[Settings.s3_bucket_name]
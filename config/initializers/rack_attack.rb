class Rack::Attack
  # Throttle Developer APIs
  limit = (ENV['THROTTLE_LIMIT'] || 10).to_i
  period = (ENV['THROTTLE_PERIOD'] || 10).to_i
  throttle('limit Developer APIs per subdomain', limit: limit, period: period) do |req|
    if req.path.include?("/api/developer")
      req.host
    end
  end
end

class String
  def clean
    Sanitize.fragment(self, elements: Sanitize::Config::RESTRICTED[:elements] + ['a'], attributes: {'a' => ['href']})
  end

  def compare_with(string)
    current = gsub(/\D/, '')
    default = string.gsub(/\D/, '')

    max_version = [current.to_i, default.to_i].max

    [self, string].select { |s| s.include?(max_version.to_s) }[0]
  end

  def to_bool
    return true if self == true || self =~ /(true|t|yes|y|1|on)$/i
    return false if self == false || blank? || self =~ /(false|f|no|n|nil|0|off)$/i
    raise ArgumentError, "invalid value for Boolean: \"#{self}\""
  end

  def to_camelcase_sym
    camelcase(:lower).to_sym
  end

  def underscorize
    underscore.parameterize('_')
  end
end
# frozen_string_literal: true

# See the end of this file for INFLUXDB_CLIENT definition

class InfluxdbClientWrapper

  def initialize
    reload_client!
  end

  def reload_client!
    self.client = InfluxDB::Client.new(Settings.influxdb.database,
      host: Settings.influxdb.host,
      username: Settings.influxdb.username,
      password: Settings.influxdb.password,
      use_ssl: Rails.env.production?,
      time_precision: 'ns',
      chunk_size: 10_000
    )
  end

  def client=(client)
    self.class.const_set :CLIENT, client
  end

  # =========================================
  # Note, since we implement influx-service (golang microservice),
  # LXP doesn't write directly to Influx DB anymore.
  # Instead, it pushes the points to a Redis queue.
  #
  # In order to REALLY write some points directly to Influx from LXP,
  # use the following methods
  # =========================================
  def write_point(measurement, data_point)
    validate_timestamps!([data_point])
    self.class::CLIENT.write_point(measurement, data_point)
  end

  def write_points(data_points)
    validate_timestamps!(data_points)
    self.class::CLIENT.write_points(data_points)
  end

  # =========================================
  # These methods push points to a Redis queue
  # (where they are picked up by influx-service (golang microservice)
  # rather than being pushed directly to Influx here.
  # =========================================

  def write_point_to_queue(measurement, data_point)
    data_point.merge!(name: measurement)
    data_points = [data_point]
    validate_timestamps! data_points
    change_timestamps_to_rfc3339! data_points
    push_to_redis_input_queue data_points
  end

  def write_points_to_queue(data_points)
    validate_timestamps! data_points
    change_timestamps_to_rfc3339! data_points
    # Change the 'series' key in each of the points to 'name'. This is to
    # account for an inconsistency between the Ruby and Golang Influx APIs.
    points = data_points.map do |point|
      point[:name] = point.delete(:series) || point.delete("series")
      point
    end
    push_to_redis_input_queue(points)
  end

  # Pushes the given JSON to a Redis input queue
  def push_to_redis_input_queue(points_list)
    queue_name = Settings.redis["influx_recording_input_queue"]
    REDIS_CLIENT.rpush(queue_name, points_list.to_json)
    # Return 204 so MetricsRecorder knows it was successful
    OpenStruct.new(code: '204')
  end

  def query(query, params: {})
    results = self.class::CLIENT.query(query, params: params)
    # Usually Influx returns all results sharing a measurement / tag set in the
    # same list. However, if there are very many results, it may split them up.
    # We want to prevent this. All records sharing a measurement / tag set
    # should be put into the same list.
    results.group_by do |result|
      [result["tags"], result["name"]]
    end.transform_values do |vals|
      {
        "name" => vals[0]["name"],
        "tags" => vals[0]["tags"],
        "values" => vals.flat_map { |val| val["values"] }
      }
    end.values
  end

  def delete_continuous_query(*args)
    self.class::CLIENT.delete_continuous_query(*args)
  end

  def create_continuous_query(*args)
    self.class::CLIENT.create_continuous_query(*args)
  end

  private

  def validate_timestamps!(data_points)
    timestamps = data_points.map { |point| point[:timestamp] || point["timestamp"] }
    if !timestamps.all? || timestamps.any? { |timestamp| timestamp&.to_s&.length != 19 }
      raise ArgumentError, "must provide timestamps with nanosecond precision"
    end
  end

  def change_timestamps_to_rfc3339!(data_points)
    data_points.each do |point|
      point_timestamp_ns = point.with_indifferent_access[:timestamp].to_s
      point_seconds = point_timestamp_ns[0..9]
      point_nsec = point_timestamp_ns[10..-1].rjust(9, '0')
      point_time = Time.at(point_seconds.to_i).utc

      # here's where we actually build the RFC3339 string
      # for meaning of these codes, see:
      # ruby-doc.org/stdlib-2.3.1/libdoc/date/rdoc/DateTime.html#method-i-strftime
      point[:timestamp] = point_time.strftime(
        "%Y-%m-%dT%H:%M:%S.#{point_nsec}Z"
      )
    end
  end

end

INFLUXDB_CLIENT = InfluxdbClientWrapper.new

# Heroku example from https://devcenter.heroku.com/articles/distributing-reads-to-followers-with-octopus
# modifications: none
=begin
module Octopus
  def self.shards_in(group=nil)
    config[Rails.env].try(:[], group.to_s).try(:keys)
  end
  def self.followers
    shards_in(:followers)
  end
  class << self
    alias_method :followers_in, :shards_in
    alias_method :slaves_in, :shards_in
  end
end
if Octopus.enabled?
  count = case (Octopus.config[Rails.env].values[0].values[0] rescue nil)
  when Hash
    Octopus.config[Rails.env].map{|group, configs| configs.count}.sum rescue 0
  else
    Octopus.config[Rails.env].keys.count rescue 0
  end

  puts "=> #{count} #{'database'.pluralize(count)} enabled as read-only #{'slave'.pluralize(count)}"
  if Octopus.followers.count == count
    Octopus.followers.each{ |f| puts "  * #{f.split('_')[0].upcase} #{f.split('_')[1]}" }
  end
end
=end

if Octopus.enabled?
  Octopus.config[Rails.env.to_s]['master'] = ActiveRecord::Base.connection.config
  ActiveRecord::Base.connection.initialize_shards(Octopus.config)

  ActiveRecord::Base.class_eval do
    # WORKAROUND:
    #   Octopus sets the #current_shard (with the replica details) by default.
    #   This causes immediate WRITE/READ operations on replica databases due the current_shard being already set.
    #   As a solution, prevent setting the #current_shard at an instance level.
    #

    # NOTE:
    #   This method is used by Octopus to hijack that object initialisation around #after_initialize.
    #   Defined here: https://github.com/thiagopradi/octopus/blob/master/lib/octopus/model.rb#L40
    #   Used here: https://github.com/thiagopradi/octopus/blob/master/lib/octopus/model.rb#L111
    def set_current_shard
      self.current_shard = nil
    end

    # NOTE: Used by rails to initialise an object.
    #   Used here: https://github.com/rails/rails/blob/3bcdd09ed47778b928d6d51e621e33d75682827d/activerecord/lib/active_record/core.rb#L329
    def init_with(coder)
      obj = super
      obj.current_shard = nil
      obj
    end
  end
end

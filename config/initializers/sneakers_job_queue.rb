# module Sneakers
#   module Handlers
#     class CustomHandler < Sneakers::Handlers::Oneshot
#       def error(hdr, props, msg, err)
#         Sneakers::logger.error err
#         Sneakers::logger.error "ActiveJob error: msg=#{msg}"
#         Bugsnag.notify err, { severity: 'error' }
#         super
#       end
#       def timeout(hdr, props, msg)
#         Sneakers::logger.error "ActiveJob timeout: msg=#{msg}"
#         super
#       end
#     end
#   end
# end
#
# unless defined?($skip_on_before_fork) && $skip_on_before_fork
#   #this config is shared by both the publisher and consumer
#   Sneakers.configure amqp: (ENV['RABBITMQ_URL'] || 'amqp://guest:guest@localhost:5672'),
#                      heartbeat: 10,
#                      vhost: (ENV['RABBITMQ_VHOST'] || '/'),
#                      workers: (ENV['JOB_QUEUE_WORKERS'] ? ENV['JOB_QUEUE_WORKERS'].to_i : 1),
#                      threads: (ENV['JOB_QUEUE_THREADS'] ? ENV['JOB_QUEUE_THREADS'].to_i : 1),
#                      log: log, #reuse the rails logger. Sneaker will honor the log level setting
#                      handler: Sneakers::Handlers::OneshotBugsnag,
#                      hooks: {
#                         before_fork: -> {
#                           log.info('Worker: Disconnect from the database')
#                           ActiveRecord::Base.connection_pool.disconnect!
#                           if defined?(Octopus)
#                             ActiveRecord::Base.connection_proxy.instance_variable_get(:@shards).each {|shard, pool| pool.disconnect! }
#                           end
#                         },
#                         after_fork: -> {
#                           ActiveRecord::Base.establish_connection
#                           ActiveRecord::Base.connection_proxy.instance_variable_get(:@shards).each {|shard, pool| pool.clear_reloadable_connections! }
#                           log.info('Worker: Reconnect to the database')
#                         }
#                      },
#                      timeout_job_after: (ENV['JOB_QUEUE_TIMEOUT'].to_i || 1800) #half an hour time out
#   ActiveJob::Base.logger = log
# end

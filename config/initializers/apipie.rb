Apipie.configure do |config|
  config.app_name                             = 'EdCast'
  config.api_base_url['v2']                   = '/'
  config.doc_base_url                         = '/apipie'
  config.api_controllers_matcher              = File.join(Rails.root, 'app', 'controllers', '**','*_controller.rb')
  config.default_version                      = 'v2'
  config.validate                             = false
  config.authenticate = Proc.new do
    unless user_signed_in? && current_user.email.ends_with?('@edcast.com')
      render text: 'Unauthorized', status: :forbidden
    end
  end
end

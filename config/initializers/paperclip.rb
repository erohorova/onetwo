#require "#{Rails.application.root}/lib/paperclip_processors/asset_aware_url_generator.rb"

# Paperclip::Attachment.default_options.merge!({
#   url_generator: AssetAwareUrlGenerator,
# })

class DelayedPaperclip::UrlGenerator
  def for_with_asset_path(*args)
    ActionController::Base.helpers.asset_path(for_without_asset_path(*args))
  end
  alias_method_chain :for, :asset_path
end

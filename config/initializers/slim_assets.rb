# For Slim, remember also to add gem to Gemfile
#set_default_options has been deprecated, use set_options
Slim::Engine.set_options :attr_list_delims => {'(' => ')', '[' => ']'}
#angular-rails-template is already including slim
#Rails.application.assets.register_engine('.slim', Slim::Template)

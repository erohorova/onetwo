require 'active_support/cache/dalli_store'


# =========================================
# NOTE: WE ARE DEPRECATING NEAR_CACHE
#
# IT HASN'T ACTUALLY BEEN FUNCTIONAL IN PRODUCTION ENVIRONMENT
# SINCE LATE 2016.
#
# SEE THE FAULTY LOGIC IN THE BELOW CODE.
# PARTICULARLY LINES 22-27
# (HINT: THE NEAR_CACHE CONSTANT IS A NULL STORE NO MATTER WHAT!)
# =========================================

unless defined?($skip_on_before_fork) && $skip_on_before_fork
  #initialize cache for this process
  if Rails.env == 'test'
    NEAR_CACHE = ActiveSupport::Cache::NullStore.new
  elsif Rails.env == 'development'
     NEAR_CACHE = ActiveSupport::Cache::MemoryStore.new(size: 32.megabytes) #default to 32 mb
  else

    if Settings.aws_cache_endpoint.present? 
      elasticache = Dalli::ElastiCache.new(Settings.aws_cache_endpoint)
      NEAR_CACHE = ActiveSupport::Cache::DalliStore.new(elasticache.servers,{:compress => true, namespace: Settings.aws_cache_namespace, :pool_size => Integer(ENV['MAX_THREADS'] || 16)}) 
    
    end
    NEAR_CACHE = ActiveSupport::Cache::NullStore.new
    

    
  end
  Rails.cache.logger = Rails.logger #log cache hit and miss for log_level = debug
end

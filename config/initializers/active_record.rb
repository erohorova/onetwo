require "#{Rails.application.root}/lib/active_record_configuration.rb"
ActiveRecord::Base.extend(ActiveRecordConfiguration::ClassMethods)
ActiveRecord::Base.include(ActiveRecordConfiguration::InstanceMethods)

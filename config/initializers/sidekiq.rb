require 'sidekiq'
require 'sidekiq/web'

Sidekiq::Web.use(Rack::Auth::Basic) do |user, password|
  [user, password] == ["admin", "L3@rning"]
end

if Rails.env == 'production' || Rails.env == 'staging' ||  Rails.env == 'qa'
  host_name = ENV['REDIS_HOST']
  port_number = ENV['REDIS_PORT']
  password = ENV['REDIS_PASSWORD']
  redis_url = "redis://#{host_name}:#{port_number}"

  Sidekiq.configure_client do |config|
    config.redis = { :url => redis_url, :password=>password}
    config.error_handlers << Proc.new {|exception, ctx_hash| Bugsnag.notify(exception) }
  end

  Sidekiq.configure_server do |config|
    config.redis = { :url => redis_url,:password=>password }
    config.error_handlers << Proc.new {|exception, ctx_hash| Bugsnag.notify(exception) }
  end

  Sidekiq.default_worker_options = { backtrace: 20, retry: 0 }
end

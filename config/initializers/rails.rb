# patches for Rails 4.2.2

########## override_helpers
#
# adds :override_helpers option for route definitions
# if true, then helpers specified via :as will replace any previously defined helpers of the same name,
#   instead of raising an exception
#
# only the lines here that include "override_helpers" have been modified from Rails 4.2.2
# RouteSet#named_routes.[]= removes the old helpers: https://github.com/rails/rails/blob/5d101c33fa19deca00e251152d25090cc152998f/actionpack/lib/action_dispatch/routing/route_set.rb#L138-L139

module ActionDispatch::Routing
  class Mapper
    module Resources
      def add_route(action, options) # :nodoc:
        override_helpers = options.delete(:override_helpers) # PATCHED

        path = path_for_action(action, options.delete(:path))
        raise ArgumentError, "path is required" if path.blank?

        action = action.to_s.dup

        if action =~ /^[\w\-\/]+$/
          options[:action] ||= action.tr('-', '_') unless action.include?("/")
        else
          action = nil
        end

        as = if !options.fetch(:as, true) # if it's set to nil or false
               options.delete(:as)
             else
               name_for_action(options.delete(:as), action)
             end

        mapping = Mapping.build(@scope, @set, URI.parser.escape(path), as, options)
        app, conditions, requirements, defaults, as, anchor = mapping.to_route
        @set.add_route(app, conditions, requirements, defaults, as, anchor, override_helpers) # PATCHED
      end
    end
  end

  class RouteSet
    def add_route(app, conditions = {}, requirements = {}, defaults = {}, name = nil, anchor = true, override_helpers = false)
      raise ArgumentError, "Invalid route name: '#{name}'" unless name.blank? || name.to_s.match(/^[_a-z]\w*$/i)

      if name && named_routes[name] && !override_helpers # PATCHED
        raise ArgumentError, "Invalid route name, already in use: '#{name}' \n" \
            "You may have defined two routes with the same name using the `:as` option, or " \
            "you may be overriding a route already defined by a resource with the same naming. " \
            "For the latter, you can restrict the routes created with `resources` as explained here: \n" \
            "http://guides.rubyonrails.org/routing.html#restricting-the-routes-created"
      end

      path = conditions.delete :path_info
      ast  = conditions.delete :parsed_path_info
      path = build_path(path, ast, requirements, anchor)
      conditions = build_conditions(conditions, path.names.map { |x| x.to_sym })

      @set.routes.find{|route| route.name == name}.instance_variable_set(:@name, nil) if override_helpers # PATCHED

      route = @set.add_route(app, path, conditions, defaults, name)
      named_routes[name] = route if name # PATCHED for override_helpers
      route
    end
  end
end

module ActionDispatch
  module Journey
    class Routes
      def add_route(app, path, conditions, defaults, name = nil)
        route = Route.new(name, app, path, conditions, defaults)

        route.precedence = routes.length
        routes << route
        named_routes[name] = route if name # PATCHED for override_helpers; this is not the same 'named_routes' as above, for some reason
        clear_cache!
        route
      end
    end
  end
end

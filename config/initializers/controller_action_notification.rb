ActionController::Instrumentation.class_eval do
  def process_action(*args)
    raw_payload = {
      controller:     self.class.name,
      action:         self.action_name,
      params:         request.filtered_parameters,
      format:         request.format.try(:ref),
      method:         request.method,
      path:           (request.fullpath rescue "unknown"),
      user_id:        defined?(current_user) ? current_user.try(:id) : nil,
      platform:       defined?(get_platform) ? get_platform : '',
      request_ip:     request.ip,
      request_host:   request.headers.env['HTTP_HOST'],
      build_number:   request.headers.env['HTTP_X_BUILD_NUMBER'],
      platform_version_number: request.headers.env['HTTP_X_VERSION_NUMBER'],
      user_agent:     request.headers.env['HTTP_USER_AGENT']
    }
    ActiveSupport::Notifications.instrument("start_processing.action_controller", raw_payload.dup)

    ActiveSupport::Notifications.instrument("process_action.action_controller", raw_payload) do |payload|
      result = super
      payload[:status] = response.status

      # try if user id has updated during the request
      if payload[:user_id].nil?
        payload[:user_id] = defined?(current_user) ? current_user.try(:id) : nil
      end

      payload[:org_id] = defined?(current_org) ? current_org.try(:id) : nil

      append_info_to_payload(payload)
      result
    end
  end
end

ActiveSupport::Notifications.subscribe "start_processing.action_controller" do |name, start, finish, id, payload|
  # Requeststore is not being set for certain requests. Will revisit this later
  # RequestStore.store[:request_metadata] = payload.slice(:platform, :platform_version_number, :user_id, :user_agent)
end
# There was an issue introduced related to key-length due to change in openssl
# It has been fixed in rails 5.0.1. With ruby 2.5.1 and rails 4.2.10
# secret key length has to be reduced to 32 bytes
# Making changes as done here https://github.com/rails/rails/pull/25758
# REF
  # https://github.com/rails/rails/issues/25448
  # https://github.com/ruby/openssl/commit/f8eec6b558675eb8cedccdd904e833ed77151b42
# Remove this code once upgraded to rails 5.0.1

module ActiveSupport
  class MessageEncryptor
    def initialize(secret, *signature_key_or_options)
      options = signature_key_or_options.extract_options!
      sign_secret = signature_key_or_options.first
      @secret = secret[0, ActiveSupport::MessageEncryptor.key_len]
      @sign_secret = sign_secret
      @cipher = options[:cipher] || 'aes-256-cbc'
      @verifier = MessageVerifier.new(@sign_secret || @secret, digest: options[:digest] || 'SHA1', serializer: NullSerializer)
      @serializer = options[:serializer] || Marshal
    end
  end
end
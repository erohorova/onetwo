# Installs the custom elasticsearch ranking script if not found
# Score = P  / ((now - enqueue_time) in hours + 2))
# Derived from the hacker news ranking algorithm
# http://amix.dk/blog/post/19574
unless ENV['PRECOMPILE_MODE']
  (Proc.new do
    if Rails.env.test?
      WebMock.disable!
    end
    conn = Faraday.new(url: "#{ENV['ELASTICSEARCH_URL'] || 'http://localhost:9200'}")
    conn.basic_auth(ENV['ELASTICSEARCH_CARDS_UID'] || '', ENV['ELASTICSEARCH_CARDS_PWD'] || '')
    unless (conn.get do |req|
      req.url '_scripts/groovy/indexedFeedRankingScript'
      end).status == 200
      conn.post do |req|
        req.url "_scripts/groovy/indexedFeedRankingScript"
        req.headers['Content-Type'] = 'application/json'
        req.body = {"script" => "doc['priority'].value / pow(((DateTime.now().getMillis() - doc['last_enqueue_event_time'].date.getMillis())/(1000*3600) + 2), 1)"}.to_json
      end
    end
    if Rails.env.test?
      WebMock.disable_net_connect!
    end
  end).call
end

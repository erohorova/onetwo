require 'slim'

module SlimHelper
  def self.include_partial(name, options = {}, &block)
	Slim::Template.new(Rails.root.join("public/templates/#{name}.html.slim").to_s).render(Object.new, :options => options).html_safe
  end
end
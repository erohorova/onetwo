# Be sure to restart your server when you modify this file.
Edcast::Application.config.session_store :cookie_store, key: '_edcast_session',
                                         secure: Settings.secure_cookies

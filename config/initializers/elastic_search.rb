unless defined?($skip_on_before_fork) && $skip_on_before_fork
  ES_LOGGER = (Settings.elasticsearch.logging ? log : nil)
  ES_TRACER = (Settings.elasticsearch.tracing ? log : nil)
  ES_SEARCH_CLIENT = Elasticsearch::Client.new host: ENV['ELASTICSEARCH_URL'], logger: ES_LOGGER, tracer: ES_TRACER, adapter: :net_http_persistent
  Elasticsearch::Persistence.client ES_SEARCH_CLIENT
  Searchkick.client = ES_SEARCH_CLIENT

  module Searchkick
    def self.env
      if Rails.env.test? && ENV['TEST_ENV_NUMBER'].present?
        @env = "test-#{ENV['TEST_ENV_NUMBER']}"
      else
        @env ||= ENV["RAILS_ENV"] || ENV["RACK_ENV"] || "development"
      end
    end
  end
end

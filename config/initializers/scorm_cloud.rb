module ScormCloud
  class CourseService < BaseService
    def import_course_async(course_id, path)
      xml = connection.call("rustici.course.importCourseAsync", :courseid => course_id, :path => path)
			if xml.elements['//rsp/'].attributes["stat"] == "ok"
				token = xml.elements['//rsp/token/id'].text
				{ :token => token, :warnings => [] }
			else
				nil
			end
    end

    def get_async_import_result(token)
      xml = connection.call("rustici.course.getAsyncImportResult", :token => token)
      if xml.elements['//rsp/status'].text == 'finished'
				title = xml.elements['//rsp/importresult/title'].text
        message = xml.elements['//rsp/importresult/message'].text
        { :status => "finished", :title => title, :message => message }
			elsif xml.elements['//rsp/status'].text == "error" #xml.elements['//rsp/status'].text == 'running'
        { :status => "error", :message => xml.elements['//rsp/error'].text }
      elsif xml.elements['//rsp/status'].text == "running"
        { :status => "running"}
      else
        nil
			end
    end
  end
end
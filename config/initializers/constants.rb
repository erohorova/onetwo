LOCALE_TO_LANGUAGE = {
  "ar"    => "Arabic",
  "en"    => "English",
  "es-es" => "Spanish",
  "fr"    => "French",
  "hi"    => "Hindi",
  "pt-br" => "Portuguese",
  "zh-cn" => "Chinese"
}

if Rails.env.test?
  CARD_STOCK_IMAGE = JSON.parse(
    File.read('test/fixtures/file_resources/card_stock_images.json')
  ).map(&:with_indifferent_access)
else
  default_image_url = ENV['FILESTACK_DEFAULT_IMAGES_URL'] || 'https://qa.cmnetwork.co/default_images/non_prod_default_images.json'
  response = Faraday.get(default_image_url)
  CARD_STOCK_IMAGE = JSON.parse(response.body).map(&:with_indifferent_access)
end

LANGUAGES = {
  "Arabic": "ar",
  "Chinese (Traditional)": "zh-Hans-CN",
  "Chinese": "zh-CN",
  "Czech": "cs-CZ",
  "Dutch (Belgium)": "nl-BE",
  "English": "en",
  "French (Canada)": "fr-CA",
  "French": "fr",
  "German": "de",
  "Greek": "el",
  "Gujarati": "gu",
  "Hebrew": "he",
  "Hindi": "hi",
  "Hungarian": "hu-HU",
  "Indonesian (Indonesia)": "id-ID",
  "Italian": "it",
  "Japanese": "ja-JP",
  "Lithuanian": "lt-LT",
  "Malay (Malaysia)": "ms-Latn-MY",
  "Marathi (India)": "mr-IN",
  "Polish": "pl",
  "Portuguese (Brazil)": "pt-BR",
  "Portuguese": "pt",
  "Romanian (Romania)": "ro-RO",
  "Russian": "ru",
  "Serbian (Serbia)": "sr-Latn-RS",
  "Slovak (Slovakia)": "sk-SK",
  "Spanish": "es",
  "Swedish": "sv",
  "Tamil (India)": "ta-IN",
  "Telugu": "te",
  "Turkish": "tr-TR",
  "Ukrainian": "uk-UA",
  "Urdu": "ur",
}

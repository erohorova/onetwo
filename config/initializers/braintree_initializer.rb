Braintree::Configuration.environment = Settings.braintree.fetch 'environment'
Braintree::Configuration.merchant_id = Settings.braintree.fetch 'merchant_id'
Braintree::Configuration.public_key = Settings.braintree.fetch 'public_key'
Braintree::Configuration.private_key = Settings.braintree.fetch 'private_key'

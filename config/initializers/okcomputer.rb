# config/initializers/okcomputer.rb
require 'sidekiq/api'
OkComputer.mount_at = 'health_checks'    # mounts at /health_checks

class MyDatabaseCheck < OkComputer::Check
  def check
    @health = HealthCheck.new(:data => "testing")

    begin
      @health.save!
      @health.destroy!
      mark_message "Database Write"
    rescue => e
      mark_message "Database Failure"
      log.error "Error with saving to the database: #{e.message}"
    end

  end
end

class MyElasticSearchCheck < OkComputer::Check
  def check
    url = ENV['ELASTICSEARCH_URL']
    if url
      conn = Faraday.new(url: url) do |faraday|
        faraday.response :logger
        faraday.adapter Faraday.default_adapter
      end

      begin
        response = conn.get '/_cluster/health'
        health = JSON.parse(response.body)['status']
      rescue => e
        log.error "Error connecting to ElasticSearch: #{e.message}"
        health = 'Error'
      end

    else
      health = 'BadURL'
    end
    mark_message "#{health} Elasticsearch Query"
  end
end

class JobCheck < OkComputer::Check

  QUEUE_THRESHOLDS = {
      batch: 10000,
      user_import: 10000,
      feed: 10000,
      indexer: 10000,
      reindex_urgent: 10000,
      mailers: 10000,
      forum: 10000,
      notifications: 10000,
      versioning: 10000,
      analytics: 10000,
      bulk: 10000,
      channel: 10000,
      cards: 10000,
      clc: 10000,
      content_source: 10000,
      default: 10000,
      image_upload: 10000,
      lms_integration: 10000,
      lrs_integration: 10000,
      okta: 10000,
      scorm: 10000,
      team: 10000,
      users: 10000,
      video_stream: 10000,
      paperclip: 10000,
      daily_learning: 10000

  }.stringify_keys

  def check
   result = Sidekiq::Queue.all

    responses = []
    QUEUE_THRESHOLDS.each do |name, limit|
      queue = result.find{|r| r.name == name}
      if queue.blank?
        responses << "queue '#{name}' missing"
        next
      end

      message_count = queue.size
      if message_count.nil?
        ## getting sporadic nil result from rabbitmq; ignoring for now
        # responses << "queue '#{name}' is missing message count"
        next
      end

     
      if message_count > limit
        responses << "queue '#{name}' at #{message_count}, over limit #{limit}"
        next
      end
    end
    if responses.empty?
      mark_message 'OK'
    else
      mark_failure
      mark_message 'Queues in bad state: ' + responses.join('; ')
    end
    log.info 'health_check for job queues: ' + message
  end
end

OkComputer::Registry.register "database", MyDatabaseCheck.new
OkComputer::Registry.register "elasticsearch", MyElasticSearchCheck.new
OkComputer::Registry.register "jobs", JobCheck.new

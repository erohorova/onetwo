# frozen_string_literal: true

password = ENV.fetch('REDIS_PASSWORD', nil)
host     = ENV.fetch('REDIS_HOST', 'localhost')
port     = ENV.fetch('REDIS_PORT', '6379')

REDIS_CLIENT = if Rails.env.test?
  MockRedis.new
else
  Redis.new(
   host: host,
   port: port.to_i,
   password: password
  )
end

# =========================================
# The following configures Redlock, which is a library for locking Redis resources.
#
# Some background on this ...
#
# In duplicate_checker.rb we have a need for a lock on Redis.
# 
# The purpose of the duplicate checker is to prevent us from pushing the
# same attribute set to Influx multiple times within a short time frame.
# So, what we do is make a hash key from the attributes, push it to Redis
# along with the record's timestamp, and then before pushing make sure that
# Redis doesn't know of a recent record with that same hash code.
#
# This system works in most cases. We even added a Mutex so make sure that
# multiple threads don't try and write/read the same key to Redis at the same
# time (which would make it non-threadsafe).
#
# The problem is that we use multiple sidekiq workers to process our event
# recording. Each worker can handle 25 threads or so. If a couple duplicate
# events get processed by different workers, they won't share the same Mutex,
# so there is a race condition.
#
# The solution is to use a distributed lock. The Redlock system works with
# one Redis server, or multiple. In our case we only use one Redis host for now,
# but we might want to increase that in the future.
#
# For more info, see:
# - http://redis.io/topics/distlock
# - https://github.com/leandromoreira/redlock-rb
# =========================================

REDIS_LOCK_MANAGER = Redlock::Client.new([REDIS_CLIENT])

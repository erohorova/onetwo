
OmniAuth::Strategies::Salesforce.class_eval do
  default_options[:client_options][:site] = Settings.salesforce.site
end

OmniAuth::Strategies::Salesforcepartners.class_eval do
  default_options[:client_options][:site] = Settings.salesforcepartners.site
end

OmniAuth::Strategies::Salesforcedevelopers.class_eval do
  default_options[:client_options][:site] = Settings.salesforcedevelopers.site
end

OmniAuth::Strategies::Salesforceusers.class_eval do
  default_options[:client_options][:site] = Settings.salesforceusers.site
end

Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook_access_token, Settings.facebook.api_key, Settings.facebook.secret, secure_image_url: true,  image_size: { width: 512 }, info_fields: 'name,email,first_name,last_name'
  provider :facebook, Settings.facebook.api_key, Settings.facebook.secret, scope: Settings.facebook.scope, info_fields: 'name,email,first_name,last_name', display: 'popup', auth_type: 'rerequest', image_size: {width: 512}, secure_image_url: true, setup: true, client_options: {
                        :site => 'https://graph.facebook.com',
                        :authorize_url => 'https://www.facebook.com/v2.0/dialog/oauth',
                        :token_url => '/oauth/access_token'
                    }, token_params: {parse: :json}


  provider :linkedin_access_token, Settings.linkedin.api_key, Settings.linkedin.secret, scope: Settings.linkedin.scope, secure_image_url: true
  provider :linkedin, Settings.linkedin.api_key, Settings.linkedin.secret, scope: Settings.linkedin.scope, secure_image_url: true

  provider :google_oauth2, Settings.google.client_id, Settings.google.secret, provider_ignores_state: true, name: 'google', scope: Settings.google.scope, display: 'popup', secure_image_url: true, image_size: {width: 1024}

  provider :twitter, Settings.twitter.api_key, Settings.twitter.secret, image_size: 'original', secure_image_url: true

  #https://github.com/omniauth/omniauth-saml/blob/master/lib/omniauth/strategies/saml.rb#L20
  provider :org_saml, name: 'saml', attribute_statements: {
      name: ['name', 'Name'],
      email: ['email', 'mail', 'Email', 'emailaddress', 'EmailAddress', 'Email Address', 'email address', 'Email address', 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress', 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailAddress'],
      first_name: ['first_name', 'firstname', 'firstName', 'Given name'],
      last_name: ['last_name', 'lastname', 'lastName', 'Family name']
  }

  on_failure { |env|
    SessionsController.action(:failure).call(env)
  }

  provider :microsoft_office365, Settings.office365.api_key, Settings.office365.secret, name: 'office365', provider_ignores_state: true, redirect_uri: Settings.office365.redirect_uri

  provider :salesforce, Settings.salesforce.api_key, Settings.salesforce.secret
  provider :salesforcepartners, Settings.salesforce.api_key, Settings.salesforce.secret
  provider :salesforcedevelopers, Settings.salesforce.api_key, Settings.salesforce.secret
  provider :salesforceusers, Settings.salesforceusers.api_key, Settings.salesforceusers.secret
  provider :org_oauth, nil, nil, setup: OrgOmniauthSetup

  provider :lxp_oauth, nil, nil, setup: LxpOauthSetup
  provider :internal_content, name: 'internal_content', attribute_statements: {
      name: ['name', 'Name'],
      email: ['email', 'mail', 'Email', 'emailaddress', 'EmailAddress', 'Email Address', 'email address', 'Email address', 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress', 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailAddress'],
      first_name: ['first_name', 'firstname', 'firstName', 'Given name'],
      last_name: ['last_name', 'lastname', 'lastName', 'Family name'],
      source_id: ['source_id'],
      user_id: ['user_id']
  }


  OmniAuth.config.logger = Rails.logger
end


#monkey patch google oauth library to use to the correct redirect_uri for mobile app
OmniAuth::Strategies::GoogleOauth2.class_eval do
  def callback_url
    if request.params['native']
      'urn:ietf:wg:oauth:2.0:oob'
    else
      super
    end
  end
end

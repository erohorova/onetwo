#!/bin/bash

cd /var/app

#automatically run db:migrate on the leader
#check to see if this is a leader instance. see ./.ebextensions/21-db-migrate.config
if [ "$SKIP_POST_DEPLOY_TASKS" = "true" ]; then
  echo "Skipping post deploy tasks because ENV variable RUN_POST_DEPLOY_TASKS is set to false"
else
  if test -e /var/host/tmp/is_leader; then
      echo "Running db:migrate on the leader node"
      bundle exec rake db:migrate
      bundle exec rake after_party:run
  else
    echo "Skipping post deploy tasks because not a leader node"
  fi
fi
#replacing AWS_CDN_HOST place holder
#echo "Replace AWS_CDN_HOST=${AWS_CDN_HOST}"
#handle assets:precomple files
#find /var/app/public/assets -name *.css -type f -print0 | xargs -0 sed -i "s/cdn.placeholder.com/${AWS_CDN_HOST}/g"
#find /var/app/public/assets -name *.js -type f -print0 | xargs -0 sed -i "s/cdn.placeholder.com/${AWS_CDN_HOST}/g"

#handle the assets manifest file generated by webpack
#find /var/app/public/assets -name webpack-assets.json -type f -print0 | xargs -0 sed -i "s/cdn.placeholder.com/${AWS_CDN_HOST}/g"

#remove the gzip files because it still contains placeholder host name in it. Let CloudFront handle the compression instead
#rm -f /var/app/public/assets/*.css.gz
#rm -f /var/app/public/assets/*.js.gz

#start the rails server
echo "Starting web server using RAILS_ENV=$RAILS_ENV"
bundle exec puma -C config/puma.rb

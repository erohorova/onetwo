- [![Build Status](https://travis-ci.com/Course-Master/edcast.svg?token=NRYbbVgukYua6WGAsYKU&branch=elk-for-destiny)](https://travis-ci.com/Course-Master/edcast) 


## Migration of integration database into edcast

```console
rake one_time_tasks:populate_integrations
```
Add environment variable INTEGRATION_DATABASE_URL and then run
```console
rake one_time_tasks:migrate_data_from_integration_database
```

## Setup

see the [setting-up-the-system](https://github.com/Course-Master/edcast/wiki/Setting-up-the-System) page in the wiki.

## Setting up one time tasks after deployment

We use the [after_party](https://github.com/theSteveMitchell/after_party) gem for handling one time tasks after deployment. Instructions below

```console
rails generate after_party:task task_name --description=optional_description_of_the_task
```

This creates a new rake task for you, that includes a description and timestamp:
```console
create lib/tasks/deployment/20130130215258_task_name.rake
```

after_party deploy tasks are run with
```console
rake after_party:run
```
## QA deployment

See the [deploying to QA](https://confluence.edcastcloud.com/display/MOB/Deploying+to+QA?flashId=1419978248) page on Confluence

## Production deployment

See the [deployment procedure](https://confluence.edcastcloud.com/display/DEV/Edcast+-+Deployment+Procedure) page on Confluence

## Other stuff

Please see [the wiki](https://github.com/Course-Master/edcast/wiki) of this repo as well as the documents in the [engineering-handbook](https://github.com/Course-Master/engineering-handbook) repo.



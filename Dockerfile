FROM edcastinc/edcast:seed-v6-deb-jemalloc

WORKDIR /var/app

COPY . /var/app

RUN npm install

RUN bundle install --jobs 5 --retry 5 --without development test

EXPOSE 3000

CMD        ["./script/docker_start.sh"]

require 'clockwork'
require './config/boot'
require './config/environment'
Edcast::Application.load_tasks

module Clockwork
  handler do |job, time|
    puts "Enqueueing #{job} at #{time}"
    job.perform_later
  end

  # Wrapper method exists for the sake of easier testing.
  def self.schedule_job(interval, job, **opts)
    every interval, job, **opts
  end

  IsFirstOfMonth = ->(t) { t == 1 }

  def self.start_jobs

    run_at_time_zone = 'America/Los_Angeles' #should only matter for daily job.
    #setting the run at time prevent the job from running immediately when we restart the scheduler.

    # ------ Commenting out for now ------ #
    # every(1.hour, RecurringJobs::ExportAmplitudeJob, at: '**:15')
    # ------ ------ ------ ------ -------- #

    #check if today a new clc started
    schedule_job 1.day,
                RecurringJobs::SendNewClcNotification,
                at: '00:30',
                tz: run_at_time_zone

    schedule_job 10.minutes,
                 RecurringJobs::RemoveStaleVideoStreamJob

    schedule_job 3.hour,
                 RecurringJobs::GroupStatsBatchJob,
                 at: '**:01'

    schedule_job 1.hour,
                 RecurringJobs::UpdateVideoStreamScoreJob,
                 tz: run_at_time_zone

    schedule_job 1.week,
                 RecurringJobs::WeeklyUserContentAnalyticsJob,
                 at: 'Monday 9:00',
                 tz: run_at_time_zone

    schedule_job 1.week,
                 RecurringJobs::WeeklySocialAnalyticsReportJob,
                 at: 'Monday 9:00',
                 tz: run_at_time_zone

    schedule_job 1.day,
                 RecurringJobs::DailyOrgAnalyticsReportsJob,
                 at: '9:00',
                 tz: run_at_time_zone

    # pulls results of scorm card courses and marks those cards as complete
    schedule_job 1.day,
                 RecurringJobs::PullScormResultsJob,
                 at: '05:00',
                 tz: run_at_time_zone

    # Batch jobs
    schedule_job 1.day,
                 RecurringJobs::FollowBatchEmailGeneratorJob,
                 at: '02:00',
                 tz: run_at_time_zone

    schedule_job 1.day,
                 DailyDigestEmailGeneratorJob,
                 at: '21:00',
                 tz: run_at_time_zone

    schedule_job 1.day,
                 InstructorMailerJob,
                 at: '8:00',
                 tz: run_at_time_zone

    schedule_job 1.day,
                 InstructorDailyDigestBatchReportJob,
                 at: '8:00',
                 tz: run_at_time_zone

    schedule_job 1.day,
                 RecurringJobs::PromoteStreamsJob,
                 at: '05:00',
                 tz: run_at_time_zone

    #organization
    schedule_job 1.day,
                 RecurringJobs::SetupInviteReminderJob,
                 at: '9:00',
                 tz: run_at_time_zone

    schedule_job 1.day,
                 RecurringJobs::RemindCuratorDailyJob,
                 at: '9:00',
                 tz: run_at_time_zone

    # clean up background jobs logs
    schedule_job 1.day,
                 RecurringJobs::RemoveOldBackgroundJobsLogsJob,
                 at: '13:00',
                 tz: run_at_time_zone

    # clean up feed content
    schedule_job 1.week,
                 RecurringJobs::RemoveOldFeedContentJob,
                 at: 'Wednesday 13:00',
                 tz: run_at_time_zone

    # Recalculate percentile daily
    schedule_job 1.day,
                 RecurringJobs::PercentileCalculatorJob,
                 at: '3:00',
                 tz: run_at_time_zone

    # Articles throwing 404 shall be archived
    schedule_job 1.day,
                 RecurringJobs::ArchiveExpiredCardsJob,
                 at: '20:00',
                 tz: run_at_time_zone,
                 if: IsFirstOfMonth

    # Image urls throwing 404 shall be updated
    schedule_job 1.week,
                 RecurringJobs::UpdateExpiredImagesJob,
                 at: '10:00',
                 tz: run_at_time_zone

    schedule_job 1.day,
                 RecurringJobs::AssignmentPerformanceMetricsJob,
                 at: '2:30',
                 tz: run_at_time_zone

    # Notifications
    schedule_job 2.hour,
                 SendDailyDigestJob

    # ECL source fetch runner job
    schedule_job 6.hours,
                 EclSourceFetchRunnerJob

    # Channel ECL source fetch job
    schedule_job 6.hours,
             ChannelEclSourcesFetchBatchJob

    #channel programming v2.0
    schedule_job 1.day,
                 ChannelProgrammingBatchJob,
                 at: '00:00',
                 tz: run_at_time_zone

    # Channel pins updated
    schedule_job 1.hour,
                  RecurringJobs::ChannelPinUpdateJob

    # LRS Pull Job - Pull LRS data and create assignment for MLP
    schedule_job 15.minutes,
                  RecurringJobs::LrsPullJob

    # Aggregates user_scores_daily data on InfluxDB to group-level
    schedule_job 1.day,
                  Analytics::GroupScoreAggregationJob,
                  at: '2:00',
                  tz: run_at_time_zone

    # Per-org aggregation of user engagement
    schedule_job 1.day,
                  Analytics::ContentEngagementMetricsJob,
                  at: '3:00',
                  tz: run_at_time_zone

    # Per-channel aggregation of channel activity
    schedule_job 1.day,
                  Analytics::ChannelMetricsAggregationRecorderJob,
                  at: '4:00',
                  tz: run_at_time_zone

    # Per-card aggregation of activity
    schedule_job 1.day,
                  Analytics::CardMetricsAggregationRecorderJob,
                  at: '5:00',
                  tz: run_at_time_zone


    # Copies user_scores_daily influx measurement to SQL
    schedule_job 1.day,
                  Analytics::UserMetricsAggregationRecorderJob,
                  at: '6:00',
                  tz: run_at_time_zone


    # Records UsersTopicsActivity and TopicsUsersActivity records to SQL
    schedule_job 1.day,
                  Analytics::KnowledgeGraphRecorderJob,
                  at: '7:00',
                  tz: run_at_time_zone

    # Builds daily aggregations for each team, and persists them to SQL.
    schedule_job 1.day,
                  Analytics::GroupMetricsAggregationRecorderJob,
                  at: '8:00',
                  tz: run_at_time_zone

    schedule_job 2.weeks,
                  RecurringJobs::CleanImportStatusJob,
                  at: '10:00',
                  tz: run_at_time_zone

    # Check for new untranslated strings and add them to CSV on S3.
    # Runs at 1am PST/2:30pm IST daily
    schedule_job 1.day,
                  RecurringJobs::AddUntranslatedStringsJob,
                  at: '1:00',
                  tz: run_at_time_zone

    # Initiates sending data to Workflow every 5 min
    schedule_job Settings.workflows.patcher_wait_time || 5.minutes,
                 WorkflowsPatcherJob

  end
end

Clockwork.start_jobs unless Rails.env.test?

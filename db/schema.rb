# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190317170632) do

  create_table "access_logs", force: :cascade do |t|
    t.integer  "user_id",                    limit: 4
    t.string   "platform",                   limit: 191
    t.string   "access_uri",                 limit: 4096
    t.date     "access_at"
    t.datetime "created_at"
    t.string   "annotation",                 limit: 255
    t.boolean  "is_signup_event",                          default: false
    t.boolean  "insider_follow_processed",                 default: false
    t.text     "insiders_already_following", limit: 65535
  end

  add_index "access_logs", ["user_id", "platform", "access_at"], name: "index_access_logs_on_user_id_and_platform_and_access_at", unique: true, using: :btree

  create_table "activity_streams", force: :cascade do |t|
    t.integer  "user_id",         limit: 4
    t.integer  "organization_id", limit: 4
    t.string   "action",          limit: 191
    t.string   "streamable_type", limit: 191
    t.integer  "streamable_id",   limit: 4
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "votes_count",     limit: 4,   default: 0
    t.integer  "comments_count",  limit: 4,   default: 0
    t.integer  "card_id",         limit: 4
    t.datetime "deleted_at"
  end

  add_index "activity_streams", ["card_id"], name: "index_activity_streams_on_card_id", using: :btree
  add_index "activity_streams", ["deleted_at"], name: "index_activity_streams_on_deleted_at", using: :btree
  add_index "activity_streams", ["organization_id"], name: "index_activity_streams_on_organization_id", using: :btree
  add_index "activity_streams", ["streamable_type", "streamable_id"], name: "index_activity_streams_on_streamable_type_and_streamable_id", using: :btree
  add_index "activity_streams", ["user_id"], name: "index_activity_streams_on_user_id", using: :btree

  create_table "amplitude_exports", force: :cascade do |t|
    t.string   "date",         limit: 255
    t.integer  "hour",         limit: 4
    t.string   "file_name",    limit: 255
    t.string   "status",       limit: 255
    t.integer  "insert_count", limit: 4,   default: 0
    t.integer  "error_count",  limit: 4,   default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "approvals", force: :cascade do |t|
    t.integer  "user_id",         limit: 4,   null: false
    t.integer  "approvable_id",   limit: 4,   null: false
    t.string   "approvable_type", limit: 191, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "approvals", ["approvable_type", "approvable_id"], name: "index_approvals_on_approvable_type_and_approvable_id", using: :btree

  create_table "assignment_notice_statuses", force: :cascade do |t|
    t.integer  "user_id",             limit: 4
    t.datetime "notice_dismissed_at"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "assignment_notice_statuses", ["user_id"], name: "index_assignment_notice_statuses_on_user_id", using: :btree

  create_table "assignments", force: :cascade do |t|
    t.integer  "user_id",         limit: 4
    t.string   "title",           limit: 255
    t.string   "assignable_type", limit: 191
    t.integer  "assignable_id",   limit: 4
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.string   "state",           limit: 255, default: "assigned"
    t.datetime "started_at"
    t.datetime "completed_at"
    t.datetime "due_at"
    t.datetime "start_date"
  end

  add_index "assignments", ["assignable_type", "assignable_id"], name: "index_assignments_on_assignable_type_and_assignable_id", using: :btree
  add_index "assignments", ["user_id", "assignable_id", "assignable_type"], name: "indx_assignments_on_user_assignable", unique: true, using: :btree
  add_index "assignments", ["user_id"], name: "index_assignments_on_user_id", using: :btree

  create_table "background_jobs", force: :cascade do |t|
    t.string   "job_id",        limit: 191
    t.string   "job_class",     limit: 191
    t.string   "job_queue",     limit: 191
    t.string   "status",        limit: 191
    t.datetime "enqueued_at"
    t.datetime "started_at"
    t.datetime "finished_at"
    t.datetime "scheduled_at"
    t.integer  "time_taken",    limit: 4
    t.text     "job_arguments", limit: 65535
    t.boolean  "truncated",                   default: false
  end

  add_index "background_jobs", ["job_class"], name: "index_background_jobs_on_job_class", using: :btree
  add_index "background_jobs", ["job_id"], name: "index_background_jobs_on_job_id", using: :btree
  add_index "background_jobs", ["job_queue"], name: "index_background_jobs_on_job_queue", using: :btree
  add_index "background_jobs", ["status"], name: "index_background_jobs_on_status", using: :btree

  create_table "background_workers", force: :cascade do |t|
    t.string   "name",       limit: 191
    t.string   "command",    limit: 255
    t.string   "state",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "background_workers", ["name"], name: "index_background_workers_on_name", unique: true, using: :btree

  create_table "badges", force: :cascade do |t|
    t.integer  "organization_id",           limit: 4
    t.string   "type",                      limit: 20
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.string   "image_file_name",           limit: 255
    t.string   "image_content_type",        limit: 255
    t.integer  "image_file_size",           limit: 4
    t.datetime "image_updated_at"
    t.string   "image_social_file_name",    limit: 255
    t.string   "image_social_content_type", limit: 255
    t.integer  "image_social_file_size",    limit: 4
    t.datetime "image_social_updated_at"
    t.boolean  "is_default",                            default: false
    t.string   "image_original_url",        limit: 255
  end

  add_index "badges", ["id", "type"], name: "index_badges_on_id_and_type", using: :btree
  add_index "badges", ["organization_id", "type"], name: "index_badges_on_organization_id_and_type", using: :btree

  create_table "badgings", force: :cascade do |t|
    t.string   "title",                limit: 255
    t.integer  "badge_id",             limit: 4
    t.integer  "badgeable_id",         limit: 4
    t.string   "type",                 limit: 20
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.integer  "target_steps",         limit: 4
    t.boolean  "all_quizzes_answered",             default: false
  end

  add_index "badgings", ["id", "type"], name: "index_badgings_on_id_and_type", using: :btree

  create_table "batch_jobs", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.datetime "last_run_time"
  end

  create_table "bookmarks", force: :cascade do |t|
    t.integer  "user_id",           limit: 4
    t.integer  "bookmarkable_id",   limit: 4
    t.string   "bookmarkable_type", limit: 191
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "bookmarks", ["bookmarkable_id", "bookmarkable_type"], name: "index_bookmarks_on_bookmarkable_id_and_bookmarkable_type", using: :btree
  add_index "bookmarks", ["user_id", "bookmarkable_type"], name: "index_bookmarks_on_user_id_and_bookmarkable_type", using: :btree
  add_index "bookmarks", ["user_id"], name: "index_bookmarks_on_user_id", using: :btree

  create_table "branchmetrics_invitations", force: :cascade do |t|
    t.string   "recipient_email",   limit: 255
    t.string   "token",             limit: 191
    t.datetime "sent_at"
    t.integer  "recipient_id",      limit: 4
    t.datetime "accepted_at"
    t.string   "first_name",        limit: 255
    t.string   "last_name",         limit: 255
    t.text     "branchmetrics_url", limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "channel",           limit: 255
  end

  add_index "branchmetrics_invitations", ["token"], name: "index_branchmetrics_invitations_on_token", unique: true, using: :btree

  create_table "card_metadata", force: :cascade do |t|
    t.integer  "card_id",        limit: 4,                                            null: false
    t.string   "plan",           limit: 255,                         default: "free"
    t.datetime "created_at",                                                          null: false
    t.datetime "updated_at",                                                          null: false
    t.string   "level",          limit: 20
    t.decimal  "average_rating",             precision: 3, scale: 2, default: 0.0,    null: false
  end

  add_index "card_metadata", ["card_id"], name: "index_card_metadata_on_card_id", using: :btree

  create_table "card_metrics_aggregations", force: :cascade do |t|
    t.integer  "organization_id", limit: 4, null: false
    t.integer  "card_id",         limit: 4, null: false
    t.integer  "num_likes",       limit: 4, null: false
    t.integer  "num_views",       limit: 4, null: false
    t.integer  "num_completions", limit: 4, null: false
    t.integer  "num_bookmarks",   limit: 4, null: false
    t.integer  "num_comments",    limit: 4, null: false
    t.integer  "start_time",      limit: 4, null: false
    t.integer  "end_time",        limit: 4, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "num_assignments", limit: 4, null: false
  end

  add_index "card_metrics_aggregations", ["card_id"], name: "index_card_metrics_aggregations_on_card_id", using: :btree
  add_index "card_metrics_aggregations", ["organization_id"], name: "index_card_metrics_aggregations_on_organization_id", using: :btree

  create_table "card_pack_relations", force: :cascade do |t|
    t.integer  "cover_id",   limit: 4
    t.integer  "from_id",    limit: 4
    t.integer  "to_id",      limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "title",      limit: 65535
    t.string   "from_type",  limit: 191
    t.boolean  "deleted",                  default: false
    t.boolean  "locked"
  end

  add_index "card_pack_relations", ["cover_id", "from_id", "from_type"], name: "index_card_pack_relations_on_cover_id_and_from_id_and_from_type", unique: true, using: :btree
  add_index "card_pack_relations", ["cover_id", "to_id"], name: "index_card_pack_relations_on_cover_id_and_to_id", using: :btree
  add_index "card_pack_relations", ["cover_id"], name: "index_card_pack_relations_on_cover_id", using: :btree
  add_index "card_pack_relations", ["from_id"], name: "index_card_pack_relations_on_from_id", using: :btree
  add_index "card_pack_relations", ["to_id"], name: "index_card_pack_relations_on_to_id", using: :btree

  create_table "card_queries", force: :cascade do |t|
    t.string   "q",            limit: 191, null: false
    t.string   "normalized_q", limit: 191, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "card_queries", ["normalized_q"], name: "index_card_queries_on_normalized_q", unique: true, using: :btree
  add_index "card_queries", ["q"], name: "index_card_queries_on_q", unique: true, using: :btree

  create_table "card_reportings", force: :cascade do |t|
    t.integer  "card_id",    limit: 4
    t.integer  "user_id",    limit: 4
    t.string   "reason",     limit: 255
    t.datetime "deleted_at"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "card_reportings", ["card_id"], name: "index_card_reportings_on_card_id", using: :btree
  add_index "card_reportings", ["deleted_at"], name: "index_card_reportings_on_deleted_at", using: :btree
  add_index "card_reportings", ["user_id"], name: "index_card_reportings_on_user_id", using: :btree

  create_table "card_subscriptions", force: :cascade do |t|
    t.datetime "start_date",                null: false
    t.datetime "end_date"
    t.integer  "organization_id", limit: 4, null: false
    t.integer  "card_id",         limit: 4, null: false
    t.integer  "user_id",         limit: 4, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "transaction_id",  limit: 4
  end

  add_index "card_subscriptions", ["card_id"], name: "index_card_subscriptions_on_card_id", using: :btree
  add_index "card_subscriptions", ["organization_id"], name: "index_card_subscriptions_on_organization_id", using: :btree
  add_index "card_subscriptions", ["transaction_id"], name: "index_card_subscriptions_on_transaction_id", using: :btree
  add_index "card_subscriptions", ["user_id"], name: "index_card_subscriptions_on_user_id", using: :btree

  create_table "card_team_permissions", force: :cascade do |t|
    t.integer  "team_id",    limit: 4,                null: false
    t.integer  "card_id",    limit: 4,                null: false
    t.boolean  "show",                 default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "card_team_permissions", ["card_id"], name: "index_card_team_permissions_on_card_id", using: :btree
  add_index "card_team_permissions", ["team_id"], name: "index_card_team_permissions_on_team_id", using: :btree

  create_table "card_usage_data", force: :cascade do |t|
    t.integer  "card_id",    limit: 4,     null: false
    t.integer  "user_id",    limit: 4,     null: false
    t.text     "data",       limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "card_usage_data", ["card_id", "user_id"], name: "index_card_usage_data_on_card_id_and_user_id", unique: true, using: :btree

  create_table "card_user_accesses", force: :cascade do |t|
    t.integer  "user_id",    limit: 4, null: false
    t.integer  "card_id",    limit: 4, null: false
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "card_user_accesses", ["card_id"], name: "index_card_user_accesses_on_card_id", using: :btree
  add_index "card_user_accesses", ["user_id", "card_id"], name: "index_card_user_accesses_on_user_id_and_card_id", unique: true, using: :btree
  add_index "card_user_accesses", ["user_id"], name: "index_card_user_accesses_on_user_id", using: :btree

  create_table "card_user_permissions", force: :cascade do |t|
    t.integer  "user_id",    limit: 4,                null: false
    t.integer  "card_id",    limit: 4,                null: false
    t.boolean  "show",                 default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "card_user_permissions", ["card_id"], name: "index_card_user_permissions_on_card_id", using: :btree
  add_index "card_user_permissions", ["user_id"], name: "index_card_user_permissions_on_user_id", using: :btree

  create_table "card_user_shares", force: :cascade do |t|
    t.integer  "user_id",      limit: 4, null: false
    t.integer  "card_id",      limit: 4, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "shared_by_id", limit: 4
  end

  add_index "card_user_shares", ["card_id"], name: "index_card_user_shares_on_card_id", using: :btree
  add_index "card_user_shares", ["shared_by_id"], name: "index_card_user_shares_on_shared_by_id", using: :btree
  add_index "card_user_shares", ["user_id"], name: "index_card_user_shares_on_user_id", using: :btree

  create_table "cards", force: :cascade do |t|
    t.integer  "cards_config_id",                    limit: 4
    t.integer  "group_id",                           limit: 4
    t.string   "author_first_name",                  limit: 255
    t.string   "author_last_name",                   limit: 255
    t.text     "author_picture_url",                 limit: 65535
    t.integer  "super_card_id",                      limit: 4
    t.integer  "author_id",                          limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_public",                                        default: false
    t.boolean  "is_manual",                                        default: false
    t.text     "features",                           limit: 65535
    t.integer  "client_id",                          limit: 4
    t.string   "client_item_id",                     limit: 191
    t.integer  "comments_count",                     limit: 4,     default: 0
    t.integer  "order_index",                        limit: 4,     default: 0
    t.string   "slug",                               limit: 191
    t.integer  "impressions_count",                  limit: 4,     default: 0
    t.integer  "outbounds_count",                    limit: 4,     default: 0
    t.integer  "shares_count",                       limit: 4,     default: 0
    t.integer  "answers_count",                      limit: 4,     default: 0
    t.integer  "expansions_count",                   limit: 4,     default: 0
    t.integer  "plays_count",                        limit: 4,     default: 0
    t.boolean  "hidden",                                           default: false
    t.integer  "resource_id",                        limit: 4
    t.integer  "organization_id",                    limit: 4
    t.string   "state",                              limit: 191
    t.text     "title",                              limit: 65535
    t.text     "message",                            limit: 65535
    t.datetime "published_at"
    t.boolean  "is_official",                                      default: false
    t.string   "card_type",                          limit: 20
    t.string   "card_subtype",                       limit: 255
    t.integer  "votes_count",                        limit: 4,     default: 0
    t.datetime "promoted_at"
    t.string   "ecl_id",                             limit: 191
    t.text     "ecl_metadata",                       limit: 65535
    t.text     "taxonomy_topics",                    limit: 65535
    t.text     "filestack",                          limit: 65535
    t.integer  "bookmarks_count",                    limit: 4
    t.string   "ecl_source_name",                    limit: 255
    t.datetime "deleted_at"
    t.integer  "readable_card_type",                 limit: 4
    t.text     "user_taxonomy_topics",               limit: 65535
    t.boolean  "auto_complete"
    t.string   "provider",                           limit: 255
    t.string   "provider_image",                     limit: 255
    t.integer  "duration",                           limit: 4
    t.boolean  "is_paid",                                          default: false
    t.boolean  "can_be_reanswered"
    t.string   "language",                           limit: 255
    t.integer  "authoritative_all_time_views_count", limit: 4
  end

  add_index "cards", ["author_id"], name: "index_cards_on_author_id", using: :btree
  add_index "cards", ["client_id", "client_item_id"], name: "index_cards_on_client_id_and_client_item_id", using: :btree
  add_index "cards", ["deleted_at"], name: "index_cards_on_deleted_at", using: :btree
  add_index "cards", ["ecl_id"], name: "index_cards_on_ecl_id", using: :btree
  add_index "cards", ["ecl_source_name"], name: "index_cards_on_ecl_source_name", length: {"ecl_source_name"=>125}, using: :btree
  add_index "cards", ["is_manual"], name: "index_cards_on_is_manual", using: :btree
  add_index "cards", ["language"], name: "index_cards_on_language", using: :btree
  add_index "cards", ["organization_id", "card_type"], name: "index_cards_on_org_id_and_card_type", using: :btree
  add_index "cards", ["organization_id", "ecl_id"], name: "index_cards_on_organization_id_and_ecl_id", unique: true, using: :btree
  add_index "cards", ["organization_id", "language"], name: "index_cards_on_organization_id_and_language", using: :btree
  add_index "cards", ["organization_id", "slug"], name: "index_cards_on_organization_id_and_slug", using: :btree
  add_index "cards", ["resource_id"], name: "index_cards_on_resource_id", using: :btree
  add_index "cards", ["state"], name: "index_cards_on_state", using: :btree
  add_index "cards", ["super_card_id"], name: "index_cards_on_super_card_id", using: :btree

  create_table "cards_actions", force: :cascade do |t|
    t.integer "cards_config_id",   limit: 4
    t.string  "display_text",      limit: 255
    t.text    "post_url",          limit: 65535
    t.text    "function_name",     limit: 65535
    t.boolean "is_internal"
    t.boolean "text_postback"
    t.string  "name",              limit: 255
    t.string  "user_role",         limit: 255
    t.string  "action_type",       limit: 255
    t.float   "signature_weight",  limit: 24
    t.text    "feedback_message",  limit: 65535
    t.string  "input_type",        limit: 255
    t.integer "reverse_action_id", limit: 4
    t.boolean "feedback_enabled",                default: false
  end

  add_index "cards_actions", ["cards_config_id"], name: "index_cards_actions_on_cards_config_id", using: :btree

  create_table "cards_configs", force: :cascade do |t|
    t.string   "card_type",          limit: 191
    t.string   "action_layout_type", limit: 255
    t.string   "ui_layout_type",     limit: 255
    t.integer  "user_id",            limit: 4
    t.boolean  "shareable"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "roles",              limit: 65535
  end

  add_index "cards_configs", ["card_type"], name: "index_cards_configs_on_card_type", using: :btree

  create_table "cards_items", force: :cascade do |t|
    t.string  "card_id",        limit: 191
    t.string  "item_type",      limit: 255, null: false
    t.integer "item_id",        limit: 4,   null: false
    t.string  "client_item_id", limit: 191
  end

  add_index "cards_items", ["card_id"], name: "index_cards_items_on_card_id", unique: true, using: :btree
  add_index "cards_items", ["client_item_id"], name: "index_cards_items_on_client_item_id", unique: true, using: :btree

  create_table "cards_ratings", force: :cascade do |t|
    t.integer  "card_id",    limit: 4
    t.integer  "user_id",    limit: 4
    t.integer  "rating",     limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "level",      limit: 255
  end

  add_index "cards_ratings", ["card_id"], name: "index_cards_ratings_on_card_id", using: :btree
  add_index "cards_ratings", ["user_id"], name: "index_cards_ratings_on_user_id", using: :btree

  create_table "cards_users", force: :cascade do |t|
    t.integer  "card_id",            limit: 4
    t.integer  "user_id",            limit: 4
    t.string   "state",              limit: 191
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "response_available",               default: false
    t.text     "response_data",      limit: 65535
  end

  add_index "cards_users", ["card_id", "user_id"], name: "index_cards_users_on_card_id_and_user_id", using: :btree
  add_index "cards_users", ["user_id", "state"], name: "index_cards_users_on_user_id_and_state", using: :btree

  create_table "career_advisor_cards", force: :cascade do |t|
    t.integer  "card_id",           limit: 4,   null: false
    t.integer  "career_advisor_id", limit: 4,   null: false
    t.string   "level",             limit: 255, null: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "career_advisor_cards", ["card_id"], name: "index_career_advisor_cards_on_card_id", using: :btree
  add_index "career_advisor_cards", ["career_advisor_id"], name: "index_career_advisor_cards_on_career_advisor_id", using: :btree

  create_table "career_advisors", force: :cascade do |t|
    t.string   "skill",           limit: 255,   null: false
    t.integer  "organization_id", limit: 4,     null: false
    t.text     "links",           limit: 65535
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "career_advisors", ["organization_id"], name: "index_career_advisors_on_organization_id", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "channel_content_sources", force: :cascade do |t|
    t.boolean  "enabled",                     default: true
    t.integer  "channel_id",        limit: 4
    t.integer  "content_source_id", limit: 4
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  create_table "channels", force: :cascade do |t|
    t.string   "label",                     limit: 255
    t.string   "description",               limit: 2000
    t.integer  "card_query_id",             limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name",           limit: 255
    t.string   "image_content_type",        limit: 255
    t.integer  "image_file_size",           limit: 4
    t.datetime "image_updated_at"
    t.string   "slug",                      limit: 191
    t.boolean  "visible",                                 default: true,  null: false
    t.string   "mobile_image_file_name",    limit: 255
    t.string   "mobile_image_content_type", limit: 255
    t.integer  "mobile_image_file_size",    limit: 4
    t.datetime "mobile_image_updated_at"
    t.boolean  "is_private",                              default: false
    t.integer  "client_id",                 limit: 4
    t.string   "keywords",                  limit: 1024
    t.boolean  "autopull_content",                        default: false
    t.boolean  "show_authors",                            default: false
    t.boolean  "visible_during_onboarding",               default: false
    t.boolean  "only_authors_can_post",                   default: true
    t.integer  "group_id",                  limit: 4
    t.integer  "organization_id",           limit: 4
    t.string   "content_discovery_freq",    limit: 255
    t.datetime "last_discovered_at"
    t.boolean  "is_general",                              default: false
    t.boolean  "is_promoted",                             default: false
    t.boolean  "auto_follow",                             default: false
    t.integer  "user_id",                   limit: 4
    t.boolean  "curate_only",                             default: false
    t.text     "topics",                    limit: 65535
    t.boolean  "ecl_enabled",                             default: false
    t.boolean  "is_open",                                 default: false
    t.string   "provider",                  limit: 255
    t.string   "provider_image",            limit: 255
    t.boolean  "curate_ugc",                              default: false
    t.boolean  "allow_follow",                            default: true
    t.boolean  "shareable"
    t.integer  "parent_id",                 limit: 4
    t.boolean  "auto_pin_cards",                          default: false
    t.text     "language",                  limit: 65535
    t.text     "content_type",              limit: 65535
    t.integer  "card_fetch_limit",          limit: 4
  end

  add_index "channels", ["group_id"], name: "index_channels_on_group_id", using: :btree
  add_index "channels", ["organization_id", "parent_id"], name: "index_channels_on_organization_id_and_parent_id", unique: true, using: :btree
  add_index "channels", ["organization_id"], name: "index_channels_on_organization_id", using: :btree
  add_index "channels", ["slug", "organization_id"], name: "index_channels_on_slug_and_organization_id", unique: true, using: :btree

  create_table "channels_cards", force: :cascade do |t|
    t.integer  "channel_id", limit: 4,   null: false
    t.integer  "card_id",    limit: 4,   null: false
    t.string   "state",      limit: 191
    t.datetime "curated_at"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "rank",       limit: 4
  end

  add_index "channels_cards", ["card_id", "channel_id"], name: "index_channels_cards_on_card_id_and_channel_id", unique: true, using: :btree
  add_index "channels_cards", ["channel_id", "card_id"], name: "index_channels_cards_on_channel_id_and_card_id", unique: true, using: :btree

  create_table "channels_curators", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "channel_id", limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "channels_curators", ["channel_id"], name: "index_channels_curators_on_channel_id", using: :btree
  add_index "channels_curators", ["user_id"], name: "index_channels_curators_on_user_id", using: :btree

  create_table "channels_groups", force: :cascade do |t|
    t.integer "channel_id", limit: 4
    t.integer "group_id",   limit: 4
  end

  add_index "channels_groups", ["channel_id", "group_id"], name: "index_channels_groups_on_channel_id_and_group_id", unique: true, using: :btree
  add_index "channels_groups", ["group_id", "channel_id"], name: "index_channels_groups_on_group_id_and_channel_id", unique: true, using: :btree

  create_table "channels_users", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "channel_id", limit: 4
    t.string   "as_type",    limit: 191, default: "author"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "channels_users", ["channel_id", "user_id", "as_type"], name: "index_channels_users_on_channel_id_and_user_id_and_as_type", using: :btree
  add_index "channels_users", ["user_id"], name: "index_channels_users_on_user_id", using: :btree

  create_table "channels_video_streams", force: :cascade do |t|
    t.integer  "video_stream_id", limit: 4, null: false
    t.integer  "channel_id",      limit: 4, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "channels_video_streams", ["channel_id", "video_stream_id"], name: "index_channels_video_streams_on_channel_id_and_video_stream_id", using: :btree
  add_index "channels_video_streams", ["video_stream_id", "channel_id"], name: "index_channels_video_streams_on_video_stream_id_and_channel_id", using: :btree

  create_table "clc_channels_records", id: false, force: :cascade do |t|
    t.datetime "created_at"
    t.integer  "user_id",    limit: 4
    t.integer  "score",      limit: 4
    t.integer  "card_id",    limit: 4
    t.integer  "channel_id", limit: 4
  end

  add_index "clc_channels_records", ["card_id"], name: "index_clc_channels_records_on_card_id", using: :btree
  add_index "clc_channels_records", ["channel_id"], name: "index_clc_channels_records_on_channel_id", using: :btree
  add_index "clc_channels_records", ["user_id", "card_id", "channel_id"], name: "index_clc_channels_records_on_user_id_and_card_id_and_channel_id", unique: true, using: :btree
  add_index "clc_channels_records", ["user_id"], name: "index_clc_channels_records_on_user_id", using: :btree

  create_table "clc_organizations_records", id: false, force: :cascade do |t|
    t.datetime "created_at"
    t.integer  "user_id",         limit: 4
    t.integer  "score",           limit: 4
    t.integer  "card_id",         limit: 4
    t.integer  "organization_id", limit: 4
  end

  add_index "clc_organizations_records", ["card_id"], name: "index_clc_organizations_records_on_card_id", using: :btree
  add_index "clc_organizations_records", ["organization_id"], name: "index_clc_organizations_records_on_organization_id", using: :btree
  add_index "clc_organizations_records", ["user_id", "card_id"], name: "index_clc_organizations_records_on_user_id_and_card_id", unique: true, using: :btree
  add_index "clc_organizations_records", ["user_id"], name: "index_clc_organizations_records_on_user_id", using: :btree

  create_table "clc_progresses", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "clc_id",     limit: 4
    t.integer  "clc_score",  limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "clc_progresses", ["clc_id"], name: "index_clc_progresses_on_clc_id", using: :btree
  add_index "clc_progresses", ["user_id", "clc_id"], name: "index_clc_progresses_on_user_id_and_clc_id", unique: true, using: :btree
  add_index "clc_progresses", ["user_id"], name: "index_clc_progresses_on_user_id", using: :btree

  create_table "clc_teams_records", id: false, force: :cascade do |t|
    t.datetime "created_at"
    t.integer  "user_id",    limit: 4
    t.integer  "score",      limit: 4
    t.integer  "card_id",    limit: 4
    t.integer  "team_id",    limit: 4
  end

  add_index "clc_teams_records", ["card_id"], name: "index_clc_teams_records_on_card_id", using: :btree
  add_index "clc_teams_records", ["team_id"], name: "index_clc_teams_records_on_team_id", using: :btree
  add_index "clc_teams_records", ["user_id", "card_id", "team_id"], name: "index_clc_teams_records_on_user_id_and_card_id_and_team_id", unique: true, using: :btree
  add_index "clc_teams_records", ["user_id"], name: "index_clc_teams_records_on_user_id", using: :btree

  create_table "clcs", force: :cascade do |t|
    t.integer  "entity_id",       limit: 4
    t.string   "entity_type",     limit: 20
    t.date     "from_date"
    t.date     "to_date"
    t.integer  "target_score",    limit: 4
    t.integer  "organization_id", limit: 4
    t.string   "target_steps",    limit: 100
    t.datetime "deleted_at"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "name",            limit: 255
  end

  add_index "clcs", ["entity_id", "entity_type"], name: "index_clcs_on_entity_id_and_entity_type", using: :btree
  add_index "clcs", ["organization_id"], name: "index_clcs_on_organization_id", using: :btree

  create_table "clients", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.integer  "user_id",        limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "redirect_uri",   limit: 65535
    t.string   "scopes",         limit: 255,   default: "",   null: false
    t.boolean  "social_enabled",               default: true
  end

  create_table "clients_admins", force: :cascade do |t|
    t.integer "client_id", limit: 4
    t.integer "admin_id",  limit: 4
  end

  add_index "clients_admins", ["admin_id", "client_id"], name: "index_clients_admins_on_admin_id_and_client_id", using: :btree

  create_table "clients_users", force: :cascade do |t|
    t.integer  "user_id",        limit: 4
    t.integer  "client_id",      limit: 4
    t.string   "client_user_id", limit: 191
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "clients_users", ["client_user_id", "client_id"], name: "index_clients_users_on_client_user_id_and_client_id", unique: true, using: :btree

  create_table "cms_ecr_content_items_organizations", force: :cascade do |t|
    t.integer "ecr_content_item_id",   limit: 4
    t.string  "ecr_content_item_type", limit: 191
    t.integer "organization_id",       limit: 4
    t.string  "state",                 limit: 191
  end

  add_index "cms_ecr_content_items_organizations", ["ecr_content_item_id", "ecr_content_item_type", "organization_id"], name: "ecr_ci_orgs_ci_orgs", using: :btree

  create_table "cms_judgements", force: :cascade do |t|
    t.integer  "user_id",           limit: 4
    t.integer  "content_item_id",   limit: 4
    t.string   "content_item_type", limit: 191
    t.string   "action",            limit: 191
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.text     "additional_info",   limit: 65535
  end

  add_index "cms_judgements", ["content_item_id", "content_item_type", "action"], name: "index_cms_judgements_on_content_item_action", using: :btree
  add_index "cms_judgements", ["user_id"], name: "index_cms_judgements_on_user_id", using: :btree

  create_table "comment_reportings", force: :cascade do |t|
    t.integer  "comment_id", limit: 4
    t.integer  "user_id",    limit: 4
    t.string   "reason",     limit: 255
    t.datetime "deleted_at"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "comment_reportings", ["comment_id"], name: "index_comment_reportings_on_comment_id", using: :btree
  add_index "comment_reportings", ["deleted_at"], name: "index_comment_reportings_on_deleted_at", using: :btree
  add_index "comment_reportings", ["user_id"], name: "index_comment_reportings_on_user_id", using: :btree

  create_table "comments", force: :cascade do |t|
    t.text     "message",          limit: 65535,                 null: false
    t.string   "type",             limit: 255
    t.integer  "commentable_id",   limit: 4
    t.string   "commentable_type", limit: 191
    t.integer  "user_id",          limit: 4,                     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "votes_count",      limit: 4,     default: 0
    t.integer  "resource_id",      limit: 4
    t.boolean  "hidden",                         default: false
    t.boolean  "anonymous",                      default: false
    t.boolean  "is_mobile",                      default: false
    t.string   "client_item_id",   limit: 191
    t.integer  "comments_count",   limit: 4,     default: 0
    t.datetime "deleted_at"
  end

  add_index "comments", ["commentable_id", "commentable_type", "client_item_id"], name: "index_comments_on_client_item_id", using: :btree
  add_index "comments", ["commentable_id", "commentable_type", "created_at"], name: "commentable_with_create_at", using: :btree
  add_index "comments", ["deleted_at"], name: "index_comments_on_deleted_at", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "competencies", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.integer  "job_role_id", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "competencies", ["job_role_id"], name: "index_competencies_on_job_role_id", using: :btree

  create_table "configs", force: :cascade do |t|
    t.string   "category",        limit: 191
    t.string   "name",            limit: 191
    t.string   "data_type",       limit: 255,   default: "string"
    t.text     "value",           limit: 65535
    t.string   "configable_type", limit: 191
    t.integer  "configable_id",   limit: 4
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.string   "description",     limit: 255
  end

  add_index "configs", ["configable_type", "configable_id", "category", "name"], name: "config_index", unique: true, using: :btree

  create_table "configurations", force: :cascade do |t|
    t.string   "name",       limit: 191, null: false
    t.string   "value",      limit: 255, null: false
    t.string   "type",       limit: 255, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "configurations", ["name"], name: "index_configurations_on_name", unique: true, using: :btree

  create_table "content_level_metrics", force: :cascade do |t|
    t.integer "views_count",     limit: 4,   default: 0
    t.integer "comments_count",  limit: 4,   default: 0
    t.integer "likes_count",     limit: 4,   default: 0
    t.string  "content_type",    limit: 191
    t.integer "content_id",      limit: 4,               null: false
    t.integer "organization_id", limit: 4,               null: false
    t.integer "offset",          limit: 4
    t.string  "period",          limit: 191
    t.integer "completes_count", limit: 4,   default: 0
    t.integer "bookmarks_count", limit: 4,   default: 0
  end

  add_index "content_level_metrics", ["content_id", "content_type", "period", "offset"], name: "indx_content_level_metrics_content", unique: true, using: :btree
  add_index "content_level_metrics", ["organization_id", "period", "offset"], name: "indx_content_level_metrics_org_id", using: :btree

  create_table "content_sources", force: :cascade do |t|
    t.text     "source",     limit: 65535, null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "contexts", force: :cascade do |t|
    t.text "context_set", limit: 65535, null: false
    t.text "styles",      limit: 65535, null: false
  end

  create_table "credentials", force: :cascade do |t|
    t.string   "api_key",       limit: 191
    t.string   "shared_secret", limit: 255
    t.string   "state",         limit: 255, default: "active"
    t.integer  "client_id",     limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "label",         limit: 255
  end

  add_index "credentials", ["api_key"], name: "index_credentials_on_api_key", unique: true, using: :btree

  create_table "crop_image_data", force: :cascade do |t|
    t.integer  "croppable_id",   limit: 4
    t.string   "croppable_type", limit: 255
    t.string   "crop_column",    limit: 255
    t.string   "file_signature", limit: 255
    t.integer  "x1",             limit: 4
    t.integer  "y1",             limit: 4
    t.integer  "x2",             limit: 4
    t.integer  "y2",             limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "custom_fields", force: :cascade do |t|
    t.integer  "organization_id",      limit: 4,  null: false
    t.string   "abbreviation",         limit: 64
    t.string   "display_name",         limit: 64
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.boolean  "enable_people_search"
  end

  add_index "custom_fields", ["organization_id"], name: "index_custom_fields_on_organization_id", using: :btree

  create_table "daily_card_stats", force: :cascade do |t|
    t.integer  "card_id",     limit: 4,             null: false
    t.date     "date"
    t.integer  "impressions", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "daily_card_stats", ["card_id", "date"], name: "index_daily_card_stats_on_card_id_and_date", unique: true, using: :btree

  create_table "daily_group_stats", force: :cascade do |t|
    t.integer  "group_id",                        limit: 4,                 null: false
    t.date     "date"
    t.integer  "total_users_with_posts",          limit: 4,     default: 0
    t.integer  "total_users_with_comments",       limit: 4,     default: 0
    t.integer  "total_active_forum_users",        limit: 4,     default: 0
    t.integer  "total_users_with_events",         limit: 4,     default: 0
    t.integer  "total_users_with_event_response", limit: 4,     default: 0
    t.integer  "total_active_events_users",       limit: 4,     default: 0
    t.integer  "total_posts",                     limit: 4,     default: 0
    t.integer  "total_unanswered_posts",          limit: 4,     default: 0
    t.integer  "total_answered_posts",            limit: 4,     default: 0
    t.integer  "total_new_events",                limit: 4,     default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "trending_tags",                   limit: 65535
    t.integer  "total_comments",                  limit: 4,     default: 0
    t.integer  "total_active_mobile_forum_users", limit: 4,     default: 0
    t.text     "trending_contexts",               limit: 65535
  end

  add_index "daily_group_stats", ["group_id", "date"], name: "index_daily_group_stats_on_group_id_and_date", unique: true, using: :btree

  create_table "delayable_joins", force: :cascade do |t|
    t.integer "delayable_id",   limit: 4
    t.string  "delayable_type", limit: 191
    t.integer "delayed_job_id", limit: 4
  end

  add_index "delayable_joins", ["delayable_type", "delayable_id", "delayed_job_id"], name: "delayable_join_index", using: :btree

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   limit: 4,     default: 0, null: false
    t.integer  "attempts",   limit: 4,     default: 0, null: false
    t.text     "handler",    limit: 65535,             null: false
    t.text     "last_error", limit: 65535
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by",  limit: 255
    t.string   "queue",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "job_id",     limit: 191
    t.string   "job_class",  limit: 191
  end

  add_index "delayed_jobs", ["job_class", "job_id"], name: "index_delayed_jobs_on_job_class_and_job_id", using: :btree
  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "developer_admin_invitations", force: :cascade do |t|
    t.integer  "sender_id",       limit: 4,   null: false
    t.integer  "client_id",       limit: 4,   null: false
    t.string   "recipient_email", limit: 255, null: false
    t.string   "token",           limit: 255, null: false
    t.datetime "sent_at"
    t.integer  "recipient_id",    limit: 4
    t.datetime "accepted_at"
    t.string   "first_name",      limit: 255
    t.string   "last_name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "developer_api_credentials", force: :cascade do |t|
    t.string   "api_key",         limit: 255
    t.string   "shared_secret",   limit: 255
    t.integer  "organization_id", limit: 4
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id",         limit: 4
  end

  add_index "developer_api_credentials", ["organization_id"], name: "index_developer_api_credentials_on_organization_id", using: :btree

  create_table "dismissed_contents", force: :cascade do |t|
    t.integer  "content_id",   limit: 4
    t.string   "content_type", limit: 191
    t.integer  "user_id",      limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "dismissed_contents", ["content_id", "content_type"], name: "index_dismissed_contents_on_content_id_and_content_type", using: :btree
  add_index "dismissed_contents", ["user_id"], name: "index_dismissed_contents_on_user_id", using: :btree

  create_table "domains", force: :cascade do |t|
    t.string   "name",        limit: 191
    t.integer  "category_id", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "domains", ["category_id"], name: "index_domains_on_category_id", using: :btree

  create_table "domains_organizations", force: :cascade do |t|
    t.integer  "domain_id",       limit: 4
    t.integer  "organization_id", limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "domains_organizations", ["domain_id"], name: "index_domains_organizations_on_domain_id", using: :btree
  add_index "domains_organizations", ["organization_id"], name: "index_domains_organizations_on_organization_id", using: :btree

  create_table "domains_skills", force: :cascade do |t|
    t.integer  "domain_id",  limit: 4
    t.integer  "skill_id",   limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "domains_skills", ["domain_id"], name: "index_domains_skills_on_domain_id", using: :btree
  add_index "domains_skills", ["skill_id"], name: "index_domains_skills_on_skill_id", using: :btree

  create_table "dynamic_group_revisions", force: :cascade do |t|
    t.integer  "external_id",     limit: 4
    t.string   "uid",             limit: 191
    t.string   "title",           limit: 191
    t.integer  "user_id",         limit: 4
    t.integer  "organization_id", limit: 4
    t.integer  "group_id",        limit: 4
    t.integer  "item_id",         limit: 4
    t.string   "item_type",       limit: 191
    t.string   "action",          limit: 191
    t.string   "message",         limit: 191
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "dynamic_group_revisions", ["external_id"], name: "index_dynamic_group_revisions_on_external_id", using: :btree
  add_index "dynamic_group_revisions", ["group_id"], name: "index_dynamic_group_revisions_on_group_id", using: :btree
  add_index "dynamic_group_revisions", ["title", "organization_id"], name: "index_dynamic_group_revisions_on_title_and_organization_id", unique: true, using: :btree
  add_index "dynamic_group_revisions", ["uid"], name: "index_dynamic_group_revisions_on_uid", using: :btree
  add_index "dynamic_group_revisions", ["user_id"], name: "index_dynamic_group_revisions_on_user_id", using: :btree

  create_table "ecl_sources", force: :cascade do |t|
    t.integer  "channel_id",      limit: 4
    t.string   "ecl_source_id",   limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "rate_interval",   limit: 4
    t.string   "rate_unit",       limit: 255
    t.integer  "number_of_items", limit: 4
    t.datetime "next_run_at"
    t.boolean  "private_content"
  end

  add_index "ecl_sources", ["channel_id", "ecl_source_id"], name: "index_ecl_sources_on_channel_id_and_ecl_source_id", unique: true, length: {"channel_id"=>nil, "ecl_source_id"=>191}, using: :btree
  add_index "ecl_sources", ["next_run_at"], name: "index_ecl_sources_on_next_run_at", using: :btree

  create_table "edcast_settings", force: :cascade do |t|
    t.boolean  "get",        default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "email_templates", force: :cascade do |t|
    t.text     "content",         limit: 65535
    t.integer  "creator_id",      limit: 4
    t.integer  "organization_id", limit: 4,                       null: false
    t.string   "state",           limit: 255,   default: "draft", null: false
    t.string   "language",        limit: 255,                     null: false
    t.string   "title",           limit: 191,                     null: false
    t.boolean  "is_active",                     default: false
    t.integer  "default_id",      limit: 4
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.text     "design",          limit: 65535,                   null: false
    t.string   "subject",         limit: 255
  end

  add_index "email_templates", ["creator_id"], name: "index_email_templates_on_creator_id", using: :btree
  add_index "email_templates", ["default_id"], name: "index_email_templates_on_default_id", using: :btree
  add_index "email_templates", ["organization_id"], name: "index_email_templates_on_organization_id", using: :btree
  add_index "email_templates", ["title"], name: "index_email_templates_on_title", using: :btree

  create_table "emails", force: :cascade do |t|
    t.integer  "user_id",            limit: 4
    t.string   "email",              limit: 191
    t.string   "confirmation_token", limit: 255
    t.boolean  "confirmed"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "contextable_id",     limit: 4
    t.string   "contextable_type",   limit: 255
    t.datetime "bounced_at"
  end

  add_index "emails", ["user_id", "email"], name: "index_emails_on_user_id_and_email", unique: true, using: :btree
  add_index "emails", ["user_id"], name: "index_emails_on_user_id", using: :btree

  create_table "embargo_files", force: :cascade do |t|
    t.integer  "user_id",         limit: 4,          null: false
    t.integer  "organization_id", limit: 4,          null: false
    t.text     "data",            limit: 4294967295
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  add_index "embargo_files", ["organization_id"], name: "index_embargo_files_on_organization_id", using: :btree
  add_index "embargo_files", ["user_id"], name: "index_embargo_files_on_user_id", using: :btree

  create_table "embargos", force: :cascade do |t|
    t.string   "value",           limit: 191, null: false
    t.string   "category",        limit: 255, null: false
    t.integer  "user_id",         limit: 4,   null: false
    t.integer  "organization_id", limit: 4,   null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "embargos", ["organization_id", "value"], name: "index_embargos_on_organization_id_and_value", unique: true, using: :btree
  add_index "embargos", ["organization_id"], name: "index_embargos_on_organization_id", using: :btree
  add_index "embargos", ["user_id"], name: "index_embargos_on_user_id", using: :btree

  create_table "events", force: :cascade do |t|
    t.string   "name",                       limit: 255,               null: false
    t.string   "address",                    limit: 255
    t.text     "description",                limit: 65535
    t.float    "latitude",                   limit: 24
    t.float    "longitude",                  limit: 24
    t.string   "state",                      limit: 255,               null: false
    t.string   "privacy",                    limit: 255,               null: false
    t.integer  "comments_count",             limit: 4,     default: 0
    t.integer  "group_id",                   limit: 4
    t.integer  "user_id",                    limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "event_start"
    t.datetime "event_end"
    t.string   "ical_file_name",             limit: 255
    t.string   "ical_content_type",          limit: 255
    t.integer  "ical_file_size",             limit: 4
    t.datetime "ical_updated_at"
    t.string   "conference_type",            limit: 255
    t.boolean  "hangout_discussion_enabled"
    t.text     "conference_url",             limit: 65535
    t.string   "youtube_url",                limit: 255
    t.boolean  "context_enabled"
    t.string   "webex_conference_key",       limit: 255
    t.string   "host_url",                   limit: 255
    t.string   "conference_email",           limit: 255
  end

  add_index "events", ["group_id"], name: "index_events_on_group_id", using: :btree
  add_index "events", ["user_id"], name: "index_events_on_user_id", using: :btree

  create_table "events_users", force: :cascade do |t|
    t.integer  "event_id",    limit: 4
    t.integer  "user_id",     limit: 4
    t.string   "rsvp",        limit: 255
    t.datetime "accepted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "events_users", ["event_id", "user_id"], name: "index_events_users_on_event_id_and_user_id", unique: true, using: :btree

  create_table "external_courses", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.text     "description",    limit: 65535
    t.string   "image_url",      limit: 255
    t.string   "course_url",     limit: 255
    t.integer  "integration_id", limit: 4
    t.string   "course_key",     limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "file_resources", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "attachment_file_name",    limit: 255
    t.string   "attachment_content_type", limit: 255
    t.integer  "attachment_file_size",    limit: 4
    t.datetime "attachment_updated_at"
    t.integer  "attachable_id",           limit: 4
    t.string   "attachable_type",         limit: 255
    t.text     "url",                     limit: 65535
    t.integer  "user_id",                 limit: 4
    t.text     "filestack",               limit: 65535
    t.string   "filestack_handle",        limit: 255
  end

  add_index "file_resources", ["attachable_id"], name: "index_file_resources_on_attachable_id", using: :btree

  create_table "follows", force: :cascade do |t|
    t.integer  "user_id",           limit: 4
    t.integer  "followable_id",     limit: 4
    t.string   "followable_type",   limit: 191
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.boolean  "skip_notification",             default: false
  end

  add_index "follows", ["followable_id", "followable_type"], name: "index_follows_on_followable_id_and_followable_type", using: :btree
  add_index "follows", ["followable_type", "followable_id"], name: "index_follows_on_followable_type_and_followable_id", using: :btree
  add_index "follows", ["skip_notification"], name: "index_follows_on_skip_notification", using: :btree
  add_index "follows", ["user_id"], name: "index_follows_on_user_id", using: :btree

  create_table "forum_user_mappings", force: :cascade do |t|
    t.string  "item_type",       limit: 191
    t.integer "item_id",         limit: 4
    t.integer "www_user_id",     limit: 4
    t.integer "org_user_id",     limit: 4
    t.integer "organization_id", limit: 4
    t.string  "table_name",      limit: 255
  end

  add_index "forum_user_mappings", ["item_type", "item_id"], name: "index_forum_user_mappings_on_item_type_and_item_id", using: :btree

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",           limit: 191, null: false
    t.integer  "sluggable_id",   limit: 4,   null: false
    t.string   "sluggable_type", limit: 191
    t.string   "scope",          limit: 191
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "global_influencers", force: :cascade do |t|
    t.integer  "user_id",                   limit: 4,                 null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "visible_during_onboarding",           default: false
  end

  add_index "global_influencers", ["user_id"], name: "index_global_influencers_on_user_id", using: :btree
  add_index "global_influencers", ["visible_during_onboarding"], name: "index_global_influencers_on_visible_during_onboarding", using: :btree

  create_table "goal_events", force: :cascade do |t|
    t.integer  "goal_id",    limit: 4
    t.text     "data",       limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "goal_events", ["goal_id"], name: "index_goal_events_on_goal_id", using: :btree

  create_table "goals", force: :cascade do |t|
    t.string   "title",           limit: 255
    t.integer  "user_id",         limit: 4
    t.string   "achievable_type", limit: 191
    t.integer  "achievable_id",   limit: 4
    t.string   "time_limit",      limit: 255
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.string   "type",            limit: 255
    t.integer  "team_id",         limit: 4
    t.string   "state",           limit: 191
    t.float    "progress",        limit: 24,  default: 0.0
  end

  add_index "goals", ["state", "team_id"], name: "index_goals_on_state_and_team_id", using: :btree
  add_index "goals", ["team_id"], name: "index_goals_on_team_id", using: :btree
  add_index "goals", ["user_id", "team_id"], name: "index_goals_on_user_id_and_team_id", using: :btree
  add_index "goals", ["user_id"], name: "index_goals_on_user_id", using: :btree

  create_table "group_metrics_aggregations", force: :cascade do |t|
    t.integer  "smartbites_created",  limit: 4, null: false
    t.integer  "team_id",             limit: 4, null: false
    t.integer  "organization_id",     limit: 4, null: false
    t.integer  "smartbites_consumed", limit: 4, null: false
    t.integer  "total_users",         limit: 4, null: false
    t.integer  "active_users",        limit: 4, null: false
    t.integer  "new_users",           limit: 4, null: false
    t.integer  "engagement_index",    limit: 4, null: false
    t.integer  "num_comments",        limit: 4, null: false
    t.integer  "num_likes",           limit: 4, null: false
    t.integer  "start_time",          limit: 4, null: false
    t.integer  "end_time",            limit: 4, null: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "time_spent_minutes",  limit: 4
  end

  add_index "group_metrics_aggregations", ["organization_id"], name: "index_group_metrics_aggregations_on_organization_id", using: :btree
  add_index "group_metrics_aggregations", ["team_id"], name: "index_group_metrics_aggregations_on_team_id", using: :btree

  create_table "groups", force: :cascade do |t|
    t.string   "name",                               limit: 255,                   null: false
    t.text     "description",                        limit: 65535
    t.string   "website_url",                        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "client_id",                          limit: 4
    t.string   "client_resource_id",                 limit: 191
    t.string   "type",                               limit: 255
    t.string   "access",                             limit: 255
    t.integer  "users_count",                        limit: 4,     default: 0
    t.text     "course_data_url",                    limit: 65535
    t.text     "image_url",                          limit: 65535
    t.boolean  "details_locked",                                   default: false
    t.integer  "creator_id",                         limit: 4
    t.integer  "parent_id",                          limit: 4
    t.boolean  "close_access",                                     default: false
    t.integer  "topic_id",                           limit: 4
    t.string   "picture_file_name",                  limit: 255
    t.string   "picture_content_type",               limit: 255
    t.integer  "picture_file_size",                  limit: 4
    t.datetime "picture_updated_at"
    t.boolean  "is_private",                                       default: false
    t.datetime "start_date"
    t.string   "course_term",                        limit: 255
    t.datetime "end_date"
    t.boolean  "connect_to_social_mediums",                        default: true
    t.boolean  "enable_mobile_app_download_buttons",               default: true
    t.boolean  "share_to_social_mediums",                          default: true
    t.boolean  "daily_digest_enabled",                             default: false
    t.boolean  "forum_notifications_enabled",                      default: true
    t.string   "language",                           limit: 255,   default: "en"
    t.boolean  "is_promoted",                                      default: false
    t.boolean  "is_hidden",                                        default: false
    t.string   "course_code",                        limit: 255
    t.integer  "organization_id",                    limit: 4
  end

  add_index "groups", ["client_id", "client_resource_id"], name: "index_groups_on_client_id_and_client_resource_id", unique: true, using: :btree
  add_index "groups", ["parent_id"], name: "index_groups_on_parent_id", using: :btree
  add_index "groups", ["topic_id"], name: "index_groups_on_topic_id", using: :btree

  create_table "groups_users", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "group_id",   limit: 4
    t.string   "as_type",    limit: 191
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "banned",                 default: false
    t.string   "role_label", limit: 255
  end

  add_index "groups_users", ["as_type"], name: "index_groups_users_on_as_type", using: :btree
  add_index "groups_users", ["group_id"], name: "index_groups_users_on_group_id", using: :btree
  add_index "groups_users", ["user_id", "group_id", "as_type", "banned"], name: "user_in_group", using: :btree
  add_index "groups_users", ["user_id", "group_id"], name: "index_groups_users_on_user_id_and_group_id", unique: true, using: :btree

  create_table "health_checks", force: :cascade do |t|
    t.text "data", limit: 65535
  end

  create_table "hr_job_roles", force: :cascade do |t|
    t.integer  "organization_id", limit: 4,   null: false
    t.string   "name",            limit: 255
    t.string   "description",     limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "identity_providers", force: :cascade do |t|
    t.string   "type",          limit: 191
    t.string   "uid",           limit: 191
    t.integer  "user_id",       limit: 4
    t.string   "token",         limit: 4096
    t.string   "secret",        limit: 255
    t.datetime "expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "token_scope",   limit: 255
    t.boolean  "auth"
    t.text     "auth_info",     limit: 65535
    t.string   "refresh_token", limit: 255
    t.integer  "source_id",     limit: 4
  end

  add_index "identity_providers", ["uid", "type", "user_id"], name: "index_identity_providers_on_uid_and_type_and_user_id", unique: true, using: :btree
  add_index "identity_providers", ["user_id"], name: "index_identity_providers_on_user_id", using: :btree

  create_table "impersonation_logs", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "target_id",  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "industries", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "industries_organizations", force: :cascade do |t|
    t.integer  "organization_id", limit: 4
    t.integer  "industry_id",     limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "industries_organizations", ["industry_id"], name: "index_industries_organizations_on_industry_id", using: :btree
  add_index "industries_organizations", ["organization_id"], name: "index_industries_organizations_on_organization_id", using: :btree

  create_table "integrations", force: :cascade do |t|
    t.string   "name",              limit: 255
    t.text     "description",       limit: 65535
    t.string   "logo_file_name",    limit: 255
    t.string   "logo_content_type", limit: 255
    t.integer  "logo_file_size",    limit: 4
    t.datetime "logo_updated_at"
    t.boolean  "enabled"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "callback_url",      limit: 255
    t.text     "fields",            limit: 65535
  end

  create_table "invitation_files", force: :cascade do |t|
    t.integer  "sender_id",         limit: 4,                         null: false
    t.text     "data",              limit: 4294967295
    t.string   "roles",             limit: 255
    t.integer  "invitable_id",      limit: 4
    t.string   "invitable_type",    limit: 191
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "send_invite_email",                    default: true
    t.text     "parameter",         limit: 65535
  end

  add_index "invitation_files", ["invitable_id", "invitable_type"], name: "index_invitation_files_on_invitable_id_and_invitable_type", using: :btree

  create_table "invitations", force: :cascade do |t|
    t.integer  "sender_id",              limit: 4
    t.string   "recipient_email",        limit: 255,                   null: false
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.string   "token",                  limit: 255
    t.datetime "sent_at"
    t.string   "roles",                  limit: 255
    t.integer  "recipient_id",           limit: 4
    t.datetime "accepted_at"
    t.integer  "invitable_id",           limit: 4
    t.string   "invitable_type",         limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "authenticate_recipient",               default: false
    t.text     "parameters",             limit: 65535
  end

  add_index "invitations", ["invitable_id"], name: "index_invitations_on_invitable_id", using: :btree
  add_index "invitations", ["recipient_id"], name: "index_invitations_on_recipient_id", using: :btree

  create_table "job_roles", force: :cascade do |t|
    t.string   "title",           limit: 255
    t.integer  "industry_id",     limit: 4
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.boolean  "user_created",                default: false
    t.boolean  "is_job_function",             default: false
  end

  add_index "job_roles", ["industry_id"], name: "index_job_roles_on_industry_id", using: :btree

  create_table "job_roles_organizations", force: :cascade do |t|
    t.integer  "job_role_id",     limit: 4
    t.integer  "organization_id", limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "job_roles_organizations", ["job_role_id"], name: "index_job_roles_organizations_on_job_role_id", using: :btree
  add_index "job_roles_organizations", ["organization_id"], name: "index_job_roles_organizations_on_organization_id", using: :btree

  create_table "journey_pack_relations", force: :cascade do |t|
    t.integer  "cover_id",   limit: 4
    t.integer  "from_id",    limit: 4
    t.integer  "to_id",      limit: 4
    t.datetime "start_date"
    t.boolean  "deleted",              default: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "journey_pack_relations", ["cover_id", "from_id"], name: "index_journey_pack_relations_on_cover_id_and_from_id", using: :btree
  add_index "journey_pack_relations", ["cover_id", "to_id"], name: "index_journey_pack_relations_on_cover_id_and_to_id", using: :btree
  add_index "journey_pack_relations", ["cover_id"], name: "index_journey_pack_relations_on_cover_id", using: :btree
  add_index "journey_pack_relations", ["from_id"], name: "index_journey_pack_relations_on_from_id", using: :btree
  add_index "journey_pack_relations", ["to_id"], name: "index_journey_pack_relations_on_to_id", using: :btree

  create_table "leaps", force: :cascade do |t|
    t.integer  "card_id",    limit: 4, null: false
    t.integer  "pathway_id", limit: 4
    t.integer  "correct_id", limit: 4, null: false
    t.integer  "wrong_id",   limit: 4, null: false
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "leaps", ["card_id", "pathway_id"], name: "index_leaps_on_card_id_and_pathway_id", using: :btree
  add_index "leaps", ["card_id"], name: "index_leaps_on_card_id", using: :btree
  add_index "leaps", ["pathway_id"], name: "index_leaps_on_pathway_id", using: :btree

  create_table "learning_queue_items", force: :cascade do |t|
    t.string   "display_type",   limit: 255
    t.string   "state",          limit: 191
    t.string   "queueable_type", limit: 191
    t.integer  "queueable_id",   limit: 4
    t.integer  "user_id",        limit: 4,   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "source",         limit: 255
    t.string   "deep_link_type", limit: 255
    t.integer  "deep_link_id",   limit: 4
  end

  add_index "learning_queue_items", ["queueable_type", "queueable_id"], name: "index_learning_queue_items_on_queueable_type_and_queueable_id", using: :btree
  add_index "learning_queue_items", ["user_id", "state"], name: "index_learning_queue_items_on_user_id_and_state", using: :btree

  create_table "mailer_configs", force: :cascade do |t|
    t.integer  "client_id",            limit: 4
    t.string   "address",              limit: 255
    t.integer  "port",                 limit: 4
    t.string   "user_name",            limit: 255
    t.string   "password",             limit: 255
    t.string   "domain",               limit: 255
    t.string   "webhook_keys",         limit: 255
    t.boolean  "enable_starttls_auto",               default: true
    t.string   "authentication",       limit: 255
    t.string   "from_address",         limit: 255
    t.string   "from_name",            limit: 255
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.integer  "organization_id",      limit: 4,                     null: false
    t.boolean  "from_admin",                         default: false
    t.boolean  "is_active",                          default: false
    t.text     "verification",         limit: 65535
  end

  add_index "mailer_configs", ["client_id"], name: "index_mailer_configs_on_client_id", using: :btree

  create_table "mdp_user_cards", force: :cascade do |t|
    t.integer  "card_id",      limit: 4,     null: false
    t.integer  "user_id",      limit: 4,     null: false
    t.text     "form_details", limit: 65535
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "mdp_user_cards", ["card_id", "user_id"], name: "index_mdp_user_cards_on_card_id_and_user_id", unique: true, using: :btree

  create_table "mentions", force: :cascade do |t|
    t.integer  "user_id",          limit: 4
    t.integer  "mentionable_id",   limit: 4
    t.string   "mentionable_type", limit: 191
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "mentions", ["deleted_at"], name: "index_mentions_on_deleted_at", using: :btree
  add_index "mentions", ["mentionable_id", "mentionable_type"], name: "index_mentions_on_mentionable_id_and_mentionable_type", using: :btree
  add_index "mentions", ["user_id"], name: "index_mentions_on_user_id", using: :btree

  create_table "mobile_invitations", force: :cascade do |t|
    t.string   "recipient_email", limit: 255, null: false
    t.string   "token",           limit: 255
    t.datetime "sent_at"
    t.integer  "recipient_id",    limit: 4
    t.datetime "accepted_at"
    t.string   "first_name",      limit: 255
    t.string   "last_name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "monthly_top_video_streams", force: :cascade do |t|
    t.integer  "video_stream_id",  limit: 4
    t.string   "month_identifier", limit: 191
    t.integer  "votes_count",      limit: 4,   default: 0
    t.boolean  "winner",                       default: false
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
  end

  add_index "monthly_top_video_streams", ["video_stream_id"], name: "index_monthly_top_video_streams_on_video_stream_id", using: :btree

  create_table "notification_configs", force: :cascade do |t|
    t.integer  "organization_id", limit: 4
    t.integer  "user_id",         limit: 4
    t.text     "configuration",   limit: 65535
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "notification_configs", ["organization_id"], name: "index_notification_configs_on_organization_id", unique: true, using: :btree
  add_index "notification_configs", ["user_id"], name: "index_notification_configs_on_user_id", unique: true, using: :btree

  create_table "notification_entries", force: :cascade do |t|
    t.integer  "user_id",                    limit: 4,     null: false
    t.string   "event_name",                 limit: 191,   null: false
    t.string   "notification_name",          limit: 191,   null: false
    t.string   "sourceable_type",            limit: 191,   null: false
    t.integer  "sourceable_id",              limit: 4,     null: false
    t.boolean  "config_web_single",                        null: false
    t.integer  "status_web_single",          limit: 1,     null: false
    t.string   "web_text",                   limit: 512
    t.boolean  "config_email_single",                      null: false
    t.integer  "status_email_single",        limit: 1,     null: false
    t.boolean  "config_email_digest_daily",                null: false
    t.integer  "status_email_digest_daily",  limit: 1,     null: false
    t.boolean  "config_email_digest_weekly",               null: false
    t.integer  "status_email_digest_weekly", limit: 1,     null: false
    t.boolean  "config_push_single",                       null: false
    t.integer  "status_push_single",         limit: 1,     null: false
    t.boolean  "config_push_digest_daily",                 null: false
    t.integer  "status_push_digest_daily",   limit: 1,     null: false
    t.boolean  "config_push_digest_weekly",                null: false
    t.integer  "status_push_digest_weekly",  limit: 1,     null: false
    t.date     "date_in_timezone",                         null: false
    t.datetime "sending_time_in_timezone",                 null: false
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.text     "additional_content",         limit: 65535
  end

  add_index "notification_entries", ["event_name"], name: "index_notification_entries_on_event_name", using: :btree
  add_index "notification_entries", ["notification_name"], name: "index_notification_entries_on_notification_name", using: :btree
  add_index "notification_entries", ["sourceable_id"], name: "index_notification_entries_on_sourceable_id", using: :btree
  add_index "notification_entries", ["sourceable_type"], name: "index_notification_entries_on_sourceable_type", using: :btree
  add_index "notification_entries", ["user_id"], name: "fk_rails_8bdf73a997", using: :btree

  create_table "notifications", force: :cascade do |t|
    t.integer  "notifiable_id",         limit: 4
    t.string   "notifiable_type",       limit: 191
    t.integer  "group_id",              limit: 4
    t.integer  "user_id",               limit: 4
    t.string   "notification_type",     limit: 255
    t.boolean  "unseen",                            default: true
    t.datetime "accepted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "widget_deep_link_id",   limit: 4
    t.integer  "card_id",               limit: 4
    t.string   "widget_deep_link_type", limit: 255
    t.integer  "app_deep_link_id",      limit: 4
    t.string   "app_deep_link_type",    limit: 255
    t.string   "custom_message",        limit: 255
  end

  add_index "notifications", ["group_id", "user_id"], name: "index_notifications_on_group_id_and_user_id", using: :btree
  add_index "notifications", ["notifiable_id", "notifiable_type"], name: "index_notifications_on_notifiable_id_and_notifiable_type", using: :btree
  add_index "notifications", ["user_id", "group_id"], name: "index_notifications_on_user_id_and_group_id", using: :btree

  create_table "notifications_causal_users", force: :cascade do |t|
    t.integer "notification_id", limit: 4,                 null: false
    t.integer "user_id",         limit: 4,                 null: false
    t.boolean "anonymous",                 default: false
  end

  add_index "notifications_causal_users", ["notification_id"], name: "index_notifications_causal_users_on_notification_id", using: :btree

  create_table "oauth_access_grants", force: :cascade do |t|
    t.integer  "resource_owner_id", limit: 4,     null: false
    t.integer  "application_id",    limit: 4,     null: false
    t.string   "token",             limit: 191,   null: false
    t.integer  "expires_in",        limit: 4,     null: false
    t.text     "redirect_uri",      limit: 65535, null: false
    t.datetime "created_at",                      null: false
    t.datetime "revoked_at"
    t.string   "scopes",            limit: 255
  end

  add_index "oauth_access_grants", ["token"], name: "index_oauth_access_grants_on_token", unique: true, using: :btree

  create_table "oauth_access_tokens", force: :cascade do |t|
    t.integer  "resource_owner_id", limit: 4
    t.integer  "application_id",    limit: 4
    t.string   "token",             limit: 191, null: false
    t.string   "refresh_token",     limit: 191
    t.integer  "expires_in",        limit: 4
    t.datetime "revoked_at"
    t.datetime "created_at",                    null: false
    t.string   "scopes",            limit: 255
  end

  add_index "oauth_access_tokens", ["refresh_token"], name: "index_oauth_access_tokens_on_refresh_token", unique: true, using: :btree
  add_index "oauth_access_tokens", ["resource_owner_id"], name: "index_oauth_access_tokens_on_resource_owner_id", using: :btree
  add_index "oauth_access_tokens", ["token"], name: "index_oauth_access_tokens_on_token", unique: true, using: :btree

  create_table "orders", force: :cascade do |t|
    t.integer  "orderable_id",    limit: 4,              null: false
    t.string   "orderable_type",  limit: 25,             null: false
    t.integer  "user_id",         limit: 4,              null: false
    t.integer  "organization_id", limit: 4,              null: false
    t.integer  "state",           limit: 4,  default: 0, null: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  add_index "orders", ["orderable_id"], name: "index_orders_on_orderable_id", using: :btree
  add_index "orders", ["orderable_type"], name: "index_orders_on_orderable_type", using: :btree

  create_table "org_mappings", force: :cascade do |t|
    t.integer "child_organization_id",  limit: 4
    t.integer "parent_organization_id", limit: 4
  end

  create_table "org_ssos", force: :cascade do |t|
    t.integer  "organization_id",                     limit: 4
    t.integer  "sso_id",                              limit: 4
    t.integer  "position",                            limit: 4,     default: 1
    t.datetime "created_at",                                                       null: false
    t.datetime "updated_at",                                                       null: false
    t.boolean  "is_enabled",                                        default: true
    t.string   "saml_idp_sso_target_url",             limit: 1024
    t.string   "saml_issuer",                         limit: 255
    t.string   "saml_idp_cert",                       limit: 4096
    t.string   "saml_assertion_consumer_service_url", limit: 1024
    t.integer  "saml_allowed_clock_drift",            limit: 4
    t.string   "oauth_login_url",                     limit: 255
    t.string   "oauth_authorize_url",                 limit: 255
    t.string   "oauth_token_url",                     limit: 255
    t.string   "oauth_user_info_url",                 limit: 255
    t.string   "label_name",                          limit: 255
    t.string   "oauth_client_id",                     limit: 255
    t.string   "oauth_client_secret",                 limit: 255
    t.string   "oauth_scopes",                        limit: 255
    t.text     "parameters",                          limit: 65535
    t.boolean  "is_visible",                                        default: true
  end

  create_table "org_subscriptions", force: :cascade do |t|
    t.string   "name",            limit: 255,   null: false
    t.text     "description",     limit: 65535, null: false
    t.date     "start_date",                    null: false
    t.date     "end_date",                      null: false
    t.integer  "organization_id", limit: 4,     null: false
    t.string   "sku",             limit: 200,   null: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "org_subscriptions", ["organization_id"], name: "index_org_subscriptions_on_organization_id", using: :btree

  create_table "organization_email_domains", force: :cascade do |t|
    t.integer  "organization_id", limit: 4
    t.string   "url",             limit: 191
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "organization_email_domains", ["organization_id"], name: "index_organization_email_domains_on_organization_id", using: :btree

  create_table "organization_forms", force: :cascade do |t|
    t.string   "first_name",        limit: 255
    t.string   "last_name",         limit: 255
    t.string   "email",             limit: 255
    t.string   "organization_name", limit: 255
    t.string   "host_name",         limit: 255
    t.string   "organization_size", limit: 255
    t.string   "interests",         limit: 1024
    t.string   "phone_number",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "organization_integrations", force: :cascade do |t|
    t.integer  "organization_id", limit: 4
    t.integer  "integration_id",  limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "organization_level_metrics", force: :cascade do |t|
    t.integer "organization_id", limit: 4
    t.integer "user_id",         limit: 4
    t.string  "action",          limit: 191
    t.string  "content_type",    limit: 191
    t.integer "events_count",    limit: 4,   default: 0
    t.string  "period",          limit: 191
    t.integer "offset",          limit: 4
  end

  add_index "organization_level_metrics", ["organization_id", "period", "offset"], name: "indx_org_level_metrics_org_id_period_offset", using: :btree
  add_index "organization_level_metrics", ["user_id"], name: "index_organization_level_metrics_on_user_id", using: :btree

  create_table "organization_prices", force: :cascade do |t|
    t.string   "currency",            limit: 10,                         default: "USD"
    t.decimal  "amount",                         precision: 8, scale: 2,                 null: false
    t.datetime "created_at",                                                             null: false
    t.datetime "updated_at",                                                             null: false
    t.integer  "org_subscription_id", limit: 4
  end

  add_index "organization_prices", ["org_subscription_id"], name: "index_organization_prices_on_org_subscription_id", using: :btree

  create_table "organization_profiles", force: :cascade do |t|
    t.integer  "organization_id", limit: 4
    t.boolean  "member_pay",                default: false
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "organization_profiles", ["organization_id"], name: "index_organization_profiles_on_organization_id", using: :btree

  create_table "organizations", force: :cascade do |t|
    t.string   "name",                                 limit: 255
    t.string   "description",                          limit: 1024
    t.string   "slug",                                 limit: 191
    t.string   "image_file_name",                      limit: 255
    t.string   "image_content_type",                   limit: 255
    t.integer  "image_file_size",                      limit: 4
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "mobile_image_file_name",               limit: 255
    t.string   "mobile_image_content_type",            limit: 255
    t.integer  "mobile_image_file_size",               limit: 4
    t.datetime "mobile_image_updated_at"
    t.string   "host_name",                            limit: 191
    t.integer  "client_id",                            limit: 4
    t.boolean  "is_open",                                           default: false
    t.string   "co_branding_logo_file_name",           limit: 255
    t.string   "co_branding_logo_content_type",        limit: 255
    t.integer  "co_branding_logo_file_size",           limit: 4
    t.datetime "co_branding_logo_updated_at"
    t.string   "co_branding_mobile_logo_file_name",    limit: 255
    t.string   "co_branding_mobile_logo_content_type", limit: 255
    t.integer  "co_branding_mobile_logo_file_size",    limit: 4
    t.datetime "co_branding_mobile_logo_updated_at"
    t.boolean  "is_registrable",                                    default: true
    t.string   "status",                               limit: 255,  default: "active"
    t.boolean  "send_weekly_activity",                              default: false
    t.boolean  "show_sub_brand_on_login",                           default: true
    t.integer  "savannah_app_id",                      limit: 4
    t.string   "redirect_uri",                         limit: 255
    t.boolean  "social_enabled"
    t.boolean  "xapi_enabled",                                      default: false
    t.integer  "users_count",                          limit: 4,    default: 0
    t.boolean  "show_onboarding",                                   default: true
    t.boolean  "enable_role_based_authorization",                   default: false
    t.string   "splash_image_file_name",               limit: 255
    t.string   "splash_image_content_type",            limit: 255
    t.integer  "splash_image_file_size",               limit: 4
    t.datetime "splash_image_updated_at"
    t.string   "favicon_file_name",                    limit: 255
    t.string   "favicon_content_type",                 limit: 255
    t.integer  "favicon_file_size",                    limit: 4
    t.datetime "favicon_updated_at"
    t.string   "custom_domain",                        limit: 191
    t.boolean  "enable_analytics_reporting",                        default: false
    t.integer  "comments_count",                       limit: 4,    default: 0
    t.boolean  "whitelabel_build_enabled",                          default: false
  end

  add_index "organizations", ["client_id"], name: "index_organizations_on_client_id", using: :btree
  add_index "organizations", ["custom_domain"], name: "index_organizations_on_custom_domain", using: :btree
  add_index "organizations", ["host_name"], name: "index_organizations_on_host_name", using: :btree
  add_index "organizations", ["slug"], name: "index_organizations_on_slug", using: :btree

  create_table "partner_centers", force: :cascade do |t|
    t.integer  "organization_id", limit: 4
    t.integer  "user_id",         limit: 4
    t.boolean  "enabled",                     default: false
    t.string   "description",     limit: 255
    t.string   "embed_link",      limit: 255
    t.string   "integration",     limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "partner_centers", ["organization_id"], name: "index_partner_centers_on_organization_id", using: :btree
  add_index "partner_centers", ["user_id"], name: "fk_rails_caad8ca248", using: :btree

  create_table "permissions", force: :cascade do |t|
    t.string   "name",       limit: 191
    t.string   "rule_name",  limit: 191
    t.string   "rule_type",  limit: 191
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "pins", force: :cascade do |t|
    t.integer  "pinnable_id",   limit: 4
    t.string   "pinnable_type", limit: 20
    t.integer  "object_id",     limit: 4
    t.string   "object_type",   limit: 255
    t.integer  "user_id",       limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "pins", ["pinnable_id", "object_id"], name: "index_pins_on_pinnable_id_and_object_id", unique: true, using: :btree
  add_index "pins", ["pinnable_id", "pinnable_type"], name: "index_pins_on_pinnable_id_and_pinnable_type", using: :btree

  create_table "post_reads", force: :cascade do |t|
    t.integer  "user_id",    limit: 4, null: false
    t.integer  "post_id",    limit: 4, null: false
    t.integer  "group_id",   limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "post_reads", ["group_id", "user_id", "post_id"], name: "index_post_reads_on_group_id_and_user_id_and_post_id", unique: true, using: :btree
  add_index "post_reads", ["user_id", "post_id"], name: "index_post_reads_on_user_id_and_post_id", unique: true, using: :btree

  create_table "posts", force: :cascade do |t|
    t.string   "title",          limit: 255
    t.text     "message",        limit: 65535
    t.string   "type",           limit: 255
    t.integer  "user_id",        limit: 4,                     null: false
    t.integer  "group_id",       limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "votes_count",    limit: 4,     default: 0
    t.integer  "resource_id",    limit: 4
    t.integer  "comments_count", limit: 4,     default: 0
    t.boolean  "hidden",                       default: false
    t.boolean  "anonymous",                    default: false
    t.boolean  "pinned",                       default: false
    t.string   "context_id",     limit: 255
    t.string   "context_label",  limit: 255
    t.boolean  "is_mobile",                    default: false
    t.integer  "postable_id",    limit: 4
    t.string   "postable_type",  limit: 191
    t.datetime "pinned_at"
  end

  add_index "posts", ["group_id"], name: "index_posts_on_group_id", using: :btree
  add_index "posts", ["postable_id", "postable_type", "created_at"], name: "index_post_created_at_postable", using: :btree
  add_index "posts", ["postable_id", "postable_type"], name: "index_posts_on_postable_id_and_postable_type", using: :btree
  add_index "posts", ["user_id"], name: "index_posts_on_user_id", using: :btree

  create_table "prices", force: :cascade do |t|
    t.string   "ecl_id",     limit: 255
    t.integer  "card_id",    limit: 4
    t.string   "currency",   limit: 10,                          default: "USD"
    t.decimal  "amount",                 precision: 8, scale: 2
    t.datetime "created_at",                                                     null: false
    t.datetime "updated_at",                                                     null: false
  end

  add_index "prices", ["card_id"], name: "index_prices_on_card_id", using: :btree

  create_table "profiles", force: :cascade do |t|
    t.integer  "user_id",         limit: 4
    t.integer  "organization_id", limit: 4
    t.string   "external_id",     limit: 255
    t.string   "uid",             limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "profiles", ["organization_id"], name: "index_profiles_on_organization_id", using: :btree
  add_index "profiles", ["user_id", "organization_id"], name: "profiles_constraint_by_user_org", unique: true, using: :btree
  add_index "profiles", ["user_id"], name: "index_profiles_on_user_id", using: :btree

  create_table "project_submission_reviews", force: :cascade do |t|
    t.text     "description",           limit: 65535
    t.integer  "status",                limit: 4,     default: 0, null: false
    t.integer  "reviewer_id",           limit: 4,                 null: false
    t.integer  "project_submission_id", limit: 4,                 null: false
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "project_submission_reviews", ["project_submission_id"], name: "index_project_submission_reviews_on_project_submission_id", using: :btree
  add_index "project_submission_reviews", ["reviewer_id"], name: "index_project_submission_reviews_on_reviewer_id", using: :btree

  create_table "project_submissions", force: :cascade do |t|
    t.text     "description",  limit: 65535
    t.integer  "submitter_id", limit: 4,     null: false
    t.integer  "project_id",   limit: 4,     null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.text     "filestack",    limit: 65535
  end

  add_index "project_submissions", ["project_id"], name: "index_project_submissions_on_project_id", using: :btree
  add_index "project_submissions", ["submitter_id"], name: "index_project_submissions_on_submitter_id", using: :btree

  create_table "projects", force: :cascade do |t|
    t.integer  "reviewer_id", limit: 4, null: false
    t.integer  "card_id",     limit: 4, null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "projects", ["card_id"], name: "index_projects_on_card_id", using: :btree
  add_index "projects", ["reviewer_id"], name: "index_projects_on_reviewer_id", using: :btree

  create_table "promotions", force: :cascade do |t|
    t.string   "url",             limit: 255
    t.integer  "promotable_id",   limit: 4
    t.string   "promotable_type", limit: 191
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.datetime "deleted_at"
    t.text     "data",            limit: 65535
  end

  add_index "promotions", ["deleted_at"], name: "index_promotions_on_deleted_at", using: :btree
  add_index "promotions", ["promotable_type", "promotable_id"], name: "index_promotions_on_promotable_type_and_promotable_id", using: :btree

  create_table "pronouncements", force: :cascade do |t|
    t.text     "label",      limit: 65535,                null: false
    t.integer  "scope_id",   limit: 8,                    null: false
    t.string   "scope_type", limit: 20,                   null: false
    t.datetime "start_date",                              null: false
    t.datetime "end_date"
    t.boolean  "is_active",                default: true
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "pronouncements", ["scope_id", "scope_type"], name: "index_pronouncements_on_scope_id_and_scope_type", using: :btree

  create_table "pubnub_channels", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "item_id",    limit: 4
    t.string   "item_type",  limit: 191
    t.string   "as_type",    limit: 191
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pubnub_channels", ["item_id", "item_type", "as_type"], name: "index_pubnub_channels_on_item_id_and_item_type_and_as_type", unique: true, using: :btree

  create_table "pwc_records", force: :cascade do |t|
    t.text     "skill",      limit: 65535
    t.integer  "level",      limit: 4
    t.integer  "user_id",    limit: 4
    t.string   "abbr",       limit: 5
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "pwc_records", ["user_id"], name: "index_pwc_records_on_user_id", using: :btree

  create_table "quarters", force: :cascade do |t|
    t.integer  "organization_id", limit: 4,   null: false
    t.date     "start_date",                  null: false
    t.date     "end_date",                    null: false
    t.string   "name",            limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "quiz_question_attempts", force: :cascade do |t|
    t.integer  "user_id",            limit: 4
    t.integer  "quiz_question_id",   limit: 4
    t.string   "quiz_question_type", limit: 191
    t.text     "response",           limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "quiz_question_attempts", ["quiz_question_id", "quiz_question_type"], name: "index_question_attempts_on_quiz_question", using: :btree
  add_index "quiz_question_attempts", ["user_id", "quiz_question_id", "quiz_question_type"], name: "index_question_attempts_on_user_quiz_question", using: :btree
  add_index "quiz_question_attempts", ["user_id"], name: "index_quiz_question_attempts_on_user_id", using: :btree

  create_table "quiz_question_options", force: :cascade do |t|
    t.integer  "quiz_question_id",   limit: 4
    t.string   "quiz_question_type", limit: 191
    t.text     "label",              limit: 65535
    t.boolean  "is_correct"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "quiz_question_options", ["quiz_question_id", "quiz_question_type"], name: "index_options_on_quiz_question", using: :btree

  create_table "quiz_question_stats", force: :cascade do |t|
    t.integer "quiz_question_id",   limit: 4
    t.string  "quiz_question_type", limit: 191
    t.text    "stats",              limit: 65535
    t.integer "attempt_count",      limit: 4,     default: 0
    t.integer "lock_version",       limit: 4,     default: 0
  end

  add_index "quiz_question_stats", ["quiz_question_id", "quiz_question_type"], name: "index_stats_on_quiz_question", using: :btree

  create_table "recharge_prices", force: :cascade do |t|
    t.integer  "recharge_id", limit: 4
    t.string   "currency",    limit: 10,                         default: "USD"
    t.decimal  "amount",                 precision: 8, scale: 2
    t.datetime "created_at",                                                     null: false
    t.datetime "updated_at",                                                     null: false
  end

  add_index "recharge_prices", ["recharge_id"], name: "index_recharge_prices_on_recharge_id", using: :btree

  create_table "recharges", force: :cascade do |t|
    t.decimal  "skillcoin",                 precision: 8, scale: 2,                null: false
    t.boolean  "active",                                            default: true
    t.text     "description", limit: 65535
    t.datetime "created_at",                                                       null: false
    t.datetime "updated_at",                                                       null: false
    t.string   "sku",         limit: 200,                                          null: false
  end

  create_table "recordings", force: :cascade do |t|
    t.string   "bucket",                       limit: 255
    t.string   "location",                     limit: 1024
    t.string   "key",                          limit: 255
    t.string   "checksum",                     limit: 255
    t.integer  "video_stream_id",              limit: 4
    t.integer  "sequence_number",              limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "transcoding_status",           limit: 255,  default: "pending"
    t.string   "hls_location",                 limit: 1024
    t.string   "mp4_location",                 limit: 1024
    t.string   "last_aws_transcoding_message", limit: 4096
    t.string   "last_aws_transcoding_job_id",  limit: 255
    t.string   "source",                       limit: 255,  default: "client"
    t.boolean  "default",                                   default: false
  end

  add_index "recordings", ["video_stream_id"], name: "index_recordings_on_video_stream_id", using: :btree

  create_table "releases", force: :cascade do |t|
    t.string   "state",       limit: 255,   default: "inactive"
    t.string   "platform",    limit: 255
    t.string   "form_factor", limit: 255,   default: "phone"
    t.string   "version",     limit: 255
    t.string   "notes",       limit: 10240
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "build",       limit: 4
  end

  create_table "reports", force: :cascade do |t|
    t.integer  "user_id",         limit: 4,                  null: false
    t.integer  "reportable_id",   limit: 4
    t.string   "reportable_type", limit: 191
    t.string   "type",            limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "validity",                    default: true
  end

  add_index "reports", ["reportable_id", "reportable_type", "user_id"], name: "index_reports_on_reportable_id_and_reportable_type_and_user_id", using: :btree

  create_table "resources", force: :cascade do |t|
    t.text     "url",         limit: 65535, null: false
    t.text     "description", limit: 65535
    t.text     "title",       limit: 65535
    t.string   "type",        limit: 255
    t.string   "site_name",   limit: 255
    t.text     "image_url",   limit: 65535
    t.text     "video_url",   limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "embed_html",  limit: 65535
    t.string   "url_hash",    limit: 64
    t.integer  "user_id",     limit: 4
  end

  add_index "resources", ["url_hash"], name: "index_resources_on_url_hash", using: :btree
  add_index "resources", ["user_id"], name: "index_resources_on_user_id", using: :btree

  create_table "role_permissions", force: :cascade do |t|
    t.integer  "role_id",       limit: 4
    t.integer  "permission_id", limit: 4
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "name",          limit: 30
    t.boolean  "enabled",                   default: true
    t.string   "description",   limit: 255
  end

  add_index "role_permissions", ["name"], name: "index_role_permissions_on_name", using: :btree
  add_index "role_permissions", ["role_id"], name: "index_role_permissions_on_role_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name",            limit: 191
    t.integer  "organization_id", limit: 4
    t.boolean  "master_role",                 default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_url",       limit: 255
    t.integer  "threshold",       limit: 4
    t.string   "default_name",    limit: 255
  end

  add_index "roles", ["name", "organization_id"], name: "index_roles_on_name_and_organization_id", using: :btree

  create_table "schools", force: :cascade do |t|
    t.string   "name",                 limit: 255
    t.string   "specialization",       limit: 255
    t.integer  "user_id",              limit: 4
    t.string   "image_file_name",      limit: 255
    t.string   "image_content_type",   limit: 255
    t.integer  "image_file_size",      limit: 4
    t.datetime "image_updated_at"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "from_year",            limit: 255
    t.string   "to_year",              limit: 255
    t.string   "institution_type",     limit: 255
    t.string   "degree_name",          limit: 255
    t.string   "institution_location", limit: 255
  end

  add_index "schools", ["user_id"], name: "index_schools_on_user_id", using: :btree

  create_table "scores", force: :cascade do |t|
    t.integer "user_id",      limit: 4,             null: false
    t.integer "group_id",     limit: 4,             null: false
    t.integer "score",        limit: 4, default: 0
    t.integer "stars",        limit: 4, default: 0
    t.integer "lock_version", limit: 4
  end

  add_index "scores", ["user_id", "group_id"], name: "index_scores_on_user_id_and_group_id", unique: true, using: :btree

  create_table "scorm_details", force: :cascade do |t|
    t.integer  "card_id",    limit: 4
    t.string   "token",      limit: 255
    t.string   "status",     limit: 255
    t.string   "message",    limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "scorm_details", ["card_id"], name: "index_scorm_details_on_card_id", using: :btree

  create_table "shortened_urls", force: :cascade do |t|
    t.integer  "owner_id",   limit: 4
    t.string   "owner_type", limit: 20
    t.string   "url",        limit: 191,             null: false
    t.string   "unique_key", limit: 10,              null: false
    t.integer  "use_count",  limit: 4,   default: 0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "shortened_urls", ["owner_id", "owner_type"], name: "index_shortened_urls_on_owner_id_and_owner_type", using: :btree
  add_index "shortened_urls", ["unique_key"], name: "index_shortened_urls_on_unique_key", unique: true, using: :btree
  add_index "shortened_urls", ["url"], name: "index_shortened_urls_on_url", using: :btree

  create_table "skills", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "skills", ["name"], name: "index_skills_on_name", unique: true, length: {"name"=>191}, using: :btree

  create_table "skills_tags", force: :cascade do |t|
    t.integer  "skill_id",   limit: 4
    t.integer  "tag_id",     limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "skills_tags", ["skill_id"], name: "index_skills_tags_on_skill_id", using: :btree
  add_index "skills_tags", ["tag_id"], name: "index_skills_tags_on_tag_id", using: :btree

  create_table "skills_users", force: :cascade do |t|
    t.integer  "user_id",         limit: 4
    t.integer  "skill_id",        limit: 4
    t.string   "description",     limit: 255
    t.text     "credential",      limit: 65535
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "experience",      limit: 255
    t.string   "skill_level",     limit: 255
    t.string   "credential_name", limit: 255
    t.string   "credential_url",  limit: 255
    t.datetime "expiry_date"
  end

  add_index "skills_users", ["skill_id", "user_id"], name: "index_skills_users_on_skill_id_and_user_id", unique: true, using: :btree
  add_index "skills_users", ["skill_id"], name: "index_skills_users_on_skill_id", using: :btree
  add_index "skills_users", ["user_id"], name: "index_skills_users_on_user_id", using: :btree

  create_table "sources_credentials", force: :cascade do |t|
    t.string   "source_id",       limit: 50,    null: false
    t.string   "shared_secret",   limit: 50
    t.text     "auth_api_info",   limit: 65535
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "organization_id", limit: 4
    t.string   "source_name",     limit: 30
  end

  create_table "ssos", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "description", limit: 255
  end

  create_table "streams", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "group_id",   limit: 4
    t.string   "action",     limit: 255
    t.integer  "item_id",    limit: 4
    t.string   "item_type",  limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "streams", ["group_id", "created_at"], name: "index_streams_on_group_id_and_created_at", using: :btree

  create_table "structured_items", force: :cascade do |t|
    t.integer  "structure_id", limit: 4,                  null: false
    t.integer  "entity_id",    limit: 4,                  null: false
    t.string   "entity_type",  limit: 20,                 null: false
    t.boolean  "enabled",                 default: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "position",     limit: 4
  end

  add_index "structured_items", ["structure_id", "entity_id", "entity_type"], name: "index_sitems_on_structure_parent", using: :btree
  add_index "structured_items", ["structure_id"], name: "index_structured_items_on_structure_id", using: :btree

  create_table "structures", force: :cascade do |t|
    t.integer  "creator_id",      limit: 4,                   null: false
    t.integer  "organization_id", limit: 4,                   null: false
    t.integer  "parent_id",       limit: 4,                   null: false
    t.string   "parent_type",     limit: 20,                  null: false
    t.string   "display_name",    limit: 100
    t.integer  "context",         limit: 4,                   null: false
    t.string   "slug",            limit: 255,                 null: false
    t.boolean  "enabled",                     default: false
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
  end

  add_index "structures", ["creator_id"], name: "fk_rails_5094890e30", using: :btree
  add_index "structures", ["organization_id", "parent_id", "parent_type"], name: "index_structures_on_org_parent", using: :btree
  add_index "structures", ["organization_id"], name: "index_structures_on_organization_id", using: :btree

  create_table "super_admin_audit_logs", force: :cascade do |t|
    t.integer  "user_id",         limit: 4
    t.integer  "organization_id", limit: 4
    t.string   "ip",              limit: 255
    t.string   "user_agent",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "super_admin_users", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id",        limit: 4,                  null: false
    t.integer  "taggable_id",   limit: 4,                  null: false
    t.string   "taggable_type", limit: 191,                null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "visible",                   default: true
  end

  add_index "taggings", ["taggable_type", "taggable_id"], name: "index_taggings_on_taggable_type_and_taggable_id", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string   "name",       limit: 191
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "type",       limit: 191
    t.boolean  "mapped",                 default: false
  end

  add_index "tags", ["name"], name: "index_tags_on_name", using: :btree

  create_table "task_records", id: false, force: :cascade do |t|
    t.string "version", limit: 255, null: false
  end

  create_table "team_assignment_metrics", force: :cascade do |t|
    t.integer  "team_id",         limit: 4,             null: false
    t.integer  "assignor_id",     limit: 4,             null: false
    t.integer  "card_id",         limit: 4,             null: false
    t.datetime "created_at",                            null: false
    t.integer  "assigned_count",  limit: 4, default: 0
    t.integer  "started_count",   limit: 4, default: 0
    t.integer  "completed_count", limit: 4, default: 0
    t.datetime "updated_at"
    t.datetime "assigned_at"
  end

  add_index "team_assignment_metrics", ["team_id", "assignor_id", "card_id"], name: "index_team_assignment_metrics_on_assignor", unique: true, using: :btree
  add_index "team_assignment_metrics", ["team_id"], name: "index_team_assignment_metrics_on_team_id", using: :btree

  create_table "team_assignments", force: :cascade do |t|
    t.integer  "assignment_id", limit: 4
    t.integer  "team_id",       limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "assignor_id",   limit: 4
    t.string   "message",       limit: 255
  end

  add_index "team_assignments", ["assignment_id"], name: "index_team_assignments_on_assignment_id", using: :btree
  add_index "team_assignments", ["team_id"], name: "index_team_assignments_on_team_id", using: :btree

  create_table "team_level_metrics", force: :cascade do |t|
    t.integer "organization_id",           limit: 4,               null: false
    t.integer "team_id",                   limit: 4,               null: false
    t.integer "smartbites_consumed",       limit: 4,   default: 0
    t.integer "time_spent",                limit: 4,   default: 0
    t.integer "smartbites_liked",          limit: 4,   default: 0
    t.integer "smartbites_comments_count", limit: 4,   default: 0
    t.integer "smartbites_created",        limit: 4,   default: 0
    t.string  "period",                    limit: 191
    t.integer "offset",                    limit: 4
  end

  add_index "team_level_metrics", ["organization_id", "period", "offset"], name: "team_metrics_indx_on_org_id", using: :btree
  add_index "team_level_metrics", ["team_id", "period", "offset"], name: "team_metrics_indx_on_team_id", unique: true, using: :btree

  create_table "teams", force: :cascade do |t|
    t.string   "name",                limit: 191,                   null: false
    t.text     "description",         limit: 65535
    t.string   "slug",                limit: 191
    t.integer  "organization_id",     limit: 4
    t.string   "image_file_name",     limit: 255
    t.string   "image_content_type",  limit: 255
    t.integer  "image_file_size",     limit: 4
    t.datetime "image_updated_at"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.boolean  "is_everyone_team",                  default: false
    t.integer  "comments_count",      limit: 4,     default: 0
    t.boolean  "auto_assign_content",               default: false
    t.text     "source_ids",          limit: 65535
    t.boolean  "is_private",                        default: false, null: false
    t.boolean  "is_dynamic",                        default: false
    t.boolean  "is_mandatory",                      default: false
    t.text     "domains",             limit: 65535
    t.boolean  "only_admin_can_post",               default: false
  end

  add_index "teams", ["organization_id"], name: "index_teams_on_organization_id", using: :btree
  add_index "teams", ["slug"], name: "index_teams_on_slug", using: :btree

  create_table "teams_cards", force: :cascade do |t|
    t.integer  "team_id",    limit: 4
    t.integer  "card_id",    limit: 4
    t.string   "type",       limit: 10
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "user_id",    limit: 4
  end

  add_index "teams_cards", ["card_id"], name: "index_teams_cards_on_card_id", using: :btree
  add_index "teams_cards", ["team_id", "card_id"], name: "index_teams_cards_on_team_and_card", using: :btree
  add_index "teams_cards", ["team_id", "type"], name: "index_teams_cards_on_team_and_teams_cards_type", using: :btree
  add_index "teams_cards", ["user_id"], name: "index_teams_cards_on_user_id", using: :btree

  create_table "teams_channels_follows", force: :cascade do |t|
    t.integer  "team_id",    limit: 4
    t.integer  "channel_id", limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "teams_channels_follows", ["channel_id"], name: "index_teams_channels_follows_on_channel_id", using: :btree
  add_index "teams_channels_follows", ["team_id", "channel_id"], name: "index_teams_channels_follows_on_team_id_and_channel_id", unique: true, using: :btree
  add_index "teams_channels_follows", ["team_id"], name: "index_teams_channels_follows_on_team_id", using: :btree

  create_table "teams_users", force: :cascade do |t|
    t.integer  "team_id",    limit: 4
    t.integer  "user_id",    limit: 4
    t.string   "as_type",    limit: 191
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "teams_users", ["team_id", "as_type"], name: "index_teams_users_on_team_id_and_as_type", using: :btree
  add_index "teams_users", ["team_id", "user_id", "as_type"], name: "index_teams_users_on_team_id_and_user_id_and_as_type", using: :btree
  add_index "teams_users", ["user_id"], name: "index_teams_users_on_user_id", using: :btree

  create_table "thumbnails", force: :cascade do |t|
    t.integer  "recording_id",    limit: 4
    t.string   "uri",             limit: 255
    t.integer  "sequence_number", limit: 4
    t.boolean  "is_default",                  default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "topic_daily_learnings", force: :cascade do |t|
    t.string   "topic_id",          limit: 191,                  null: false
    t.integer  "organization_id",   limit: 4,                    null: false
    t.text     "ecl_ids",           limit: 65535
    t.string   "date",              limit: 11,                   null: false
    t.string   "processing_state",  limit: 255,                  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "topic_name",        limit: 255
    t.boolean  "requested_by_user",               default: true
  end

  add_index "topic_daily_learnings", ["organization_id"], name: "fk_rails_f2a61a2d66", using: :btree
  add_index "topic_daily_learnings", ["topic_id", "organization_id", "date"], name: "indx_topic_dl_topic_org_date", unique: true, using: :btree

  create_table "topic_requests", force: :cascade do |t|
    t.string   "label",           limit: 150
    t.integer  "requestor_id",    limit: 4
    t.integer  "approver_id",     limit: 4
    t.string   "state",           limit: 30
    t.integer  "organization_id", limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "publisher_id",    limit: 4
  end

  add_index "topic_requests", ["label", "organization_id"], name: "index_topic_requests_on_label_and_organization_id", unique: true, using: :btree
  add_index "topic_requests", ["organization_id"], name: "index_topic_requests_on_organization_id", using: :btree
  add_index "topic_requests", ["state"], name: "index_topic_requests_on_state", using: :btree

  create_table "transaction_audits", force: :cascade do |t|
    t.integer  "transaction_id",        limit: 4,     null: false
    t.text     "order_data",            limit: 65535, null: false
    t.text     "orderable_prices_data", limit: 65535, null: false
    t.text     "orderable_data",        limit: 65535, null: false
    t.text     "user_data",             limit: 65535, null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.text     "transaction_data",      limit: 65535, null: false
  end

  add_index "transaction_audits", ["transaction_id"], name: "index_transaction_audits_on_transaction_id", using: :btree

  create_table "transactions", force: :cascade do |t|
    t.integer  "order_id",      limit: 4,                                             null: false
    t.string   "gateway",       limit: 255
    t.string   "gateway_id",    limit: 255
    t.text     "gateway_data",  limit: 65535
    t.decimal  "amount",                      precision: 8, scale: 2,                 null: false
    t.string   "currency",      limit: 10,                            default: "USD", null: false
    t.integer  "payment_state", limit: 4,                             default: 0,     null: false
    t.integer  "user_id",       limit: 4,                                             null: false
    t.datetime "created_at",                                                          null: false
    t.datetime "updated_at",                                                          null: false
    t.string   "source_name",   limit: 50
    t.string   "order_type",    limit: 20
  end

  add_index "transactions", ["order_id"], name: "index_transactions_on_order_id", using: :btree
  add_index "transactions", ["user_id"], name: "index_transactions_on_user_id", using: :btree

  create_table "uploads", force: :cascade do |t|
    t.integer  "sender_id",         limit: 4
    t.text     "output_url",        limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "file_file_name",    limit: 255
    t.string   "file_content_type", limit: 255
    t.integer  "file_file_size",    limit: 4
    t.datetime "file_updated_at"
    t.string   "type",              limit: 255
    t.integer  "group_id",          limit: 4
  end

  create_table "upsell_requests", force: :cascade do |t|
    t.integer "user_id",     limit: 4
    t.text    "fields",      limit: 65535
    t.boolean "approved",                  default: false
    t.string  "upsell_type", limit: 255
  end

  add_index "upsell_requests", ["user_id"], name: "index_upsell_requests_on_user_id", using: :btree

  create_table "user_badges", force: :cascade do |t|
    t.integer  "badging_id", limit: 4
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "identifier", limit: 10
  end

  add_index "user_badges", ["badging_id", "user_id"], name: "index_user_badges_on_badging_id_and_user_id", unique: true, using: :btree
  add_index "user_badges", ["identifier"], name: "index_user_badges_on_identifier", using: :btree
  add_index "user_badges", ["user_id"], name: "index_user_badges_on_user_id", using: :btree

  create_table "user_completions", force: :cascade do |t|
    t.integer  "user_id",          limit: 4
    t.integer  "completable_id",   limit: 4
    t.string   "completable_type", limit: 191
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "goal_id",          limit: 4
  end

  add_index "user_completions", ["goal_id"], name: "index_user_completions_on_goal_id", using: :btree
  add_index "user_completions", ["user_id"], name: "index_user_completions_on_user_id", using: :btree

  create_table "user_connections", force: :cascade do |t|
    t.integer "user_id",       limit: 4
    t.integer "connection_id", limit: 4
    t.string  "network",       limit: 191
  end

  add_index "user_connections", ["connection_id", "user_id"], name: "index_user_connections_on_connection_id_and_user_id", using: :btree
  add_index "user_connections", ["user_id", "connection_id", "network"], name: "index_user_connections_on_user_id_and_connection_id_and_network", unique: true, using: :btree

  create_table "user_content_completions", force: :cascade do |t|
    t.integer  "user_id",              limit: 4
    t.integer  "completable_id",       limit: 4
    t.string   "completable_type",     limit: 191
    t.string   "topics",               limit: 255
    t.string   "state",                limit: 255
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.datetime "started_at"
    t.datetime "completed_at"
    t.integer  "completed_percentage", limit: 4,   default: 0, null: false
  end

  add_index "user_content_completions", ["completable_id", "completable_type"], name: "index_user_content_completions_on_completable_id_and_type", using: :btree
  add_index "user_content_completions", ["user_id", "completable_id"], name: "unique_user_content_completion", unique: true, using: :btree

  create_table "user_content_level_metrics", force: :cascade do |t|
    t.integer  "user_id",          limit: 4
    t.integer  "content_id",       limit: 4
    t.string   "content_type",     limit: 191
    t.integer  "smartbites_score", limit: 4,   default: 0
    t.string   "period",           limit: 191
    t.integer  "offset",           limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_content_level_metrics", ["user_id", "period", "offset"], name: "indx_usr_content_lvl_mtrcs_on_usr_period_off", using: :btree

  create_table "user_custom_fields", force: :cascade do |t|
    t.integer  "custom_field_id", limit: 4,     null: false
    t.integer  "user_id",         limit: 4,     null: false
    t.text     "value",           limit: 65535
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "user_custom_fields", ["custom_field_id", "user_id"], name: "index_user_custom_fields_on_custom_field_id_and_user_id", using: :btree
  add_index "user_custom_fields", ["user_id"], name: "index_user_custom_fields_on_user_id", using: :btree

  create_table "user_daily_learnings", force: :cascade do |t|
    t.integer  "user_id",          limit: 4,     null: false
    t.text     "card_ids",         limit: 65535
    t.string   "date",             limit: 11
    t.string   "processing_state", limit: 255,   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_daily_learnings", ["user_id", "date"], name: "index_user_daily_learnings_on_user_id_and_date", unique: true, using: :btree

  create_table "user_feed_caches", force: :cascade do |t|
    t.integer  "user_id",                       limit: 4,     null: false
    t.text     "queue_item_ids",                limit: 65535
    t.string   "feed_request_id",               limit: 191,   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "channel_recommendations_shown"
  end

  add_index "user_feed_caches", ["user_id", "feed_request_id"], name: "index_user_feed_caches_on_user_id_and_feed_request_id", unique: true, using: :btree

  create_table "user_import_statuses", force: :cascade do |t|
    t.integer  "invitation_file_id",     limit: 4
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.string   "email",                  limit: 255
    t.string   "status",                 limit: 255
    t.text     "channel_ids",            limit: 65535
    t.text     "team_ids",               limit: 65535
    t.boolean  "resent_email"
    t.boolean  "existing_user"
    t.text     "custom_fields",          limit: 65535
    t.text     "additional_information", limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_import_statuses", ["invitation_file_id"], name: "fk_rails_275cdf88b4", using: :btree

  create_table "user_level_metrics", force: :cascade do |t|
    t.integer "user_id",                   limit: 4
    t.integer "organization_id",           limit: 4
    t.string  "period",                    limit: 191
    t.integer "offset",                    limit: 4
    t.integer "smartbites_score",          limit: 4,   default: 0
    t.integer "smartbites_consumed",       limit: 4,   default: 0
    t.integer "smartbites_created",        limit: 4,   default: 0
    t.integer "time_spent",                limit: 4,   default: 0
    t.integer "percentile",                limit: 4,   default: 0
    t.integer "smartbites_liked",          limit: 4,   default: 0
    t.integer "smartbites_comments_count", limit: 4,   default: 0
    t.integer "smartbites_completed",      limit: 4,   default: 0
  end

  add_index "user_level_metrics", ["organization_id", "period", "offset"], name: "indx_user_level_metrics_org_id_period_offset", using: :btree
  add_index "user_level_metrics", ["user_id", "period", "offset"], name: "index_user_level_metrics_on_user_id_and_period_and_offset", unique: true, using: :btree

  create_table "user_metrics_aggregations", force: :cascade do |t|
    t.integer  "smartbites_commented_count", limit: 4, null: false
    t.integer  "smartbites_completed_count", limit: 4, null: false
    t.integer  "smartbites_consumed_count",  limit: 4, null: false
    t.integer  "smartbites_created_count",   limit: 4, null: false
    t.integer  "smartbites_liked_count",     limit: 4, null: false
    t.integer  "time_spent_minutes",         limit: 4, null: false
    t.integer  "total_smartbite_score",      limit: 4, null: false
    t.integer  "total_user_score",           limit: 4, null: false
    t.integer  "user_id",                    limit: 4, null: false
    t.integer  "organization_id",            limit: 4, null: false
    t.integer  "start_time",                 limit: 4, null: false
    t.integer  "end_time",                   limit: 4, null: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "user_metrics_aggregations", ["organization_id"], name: "index_user_metrics_aggregations_on_organization_id", using: :btree
  add_index "user_metrics_aggregations", ["user_id"], name: "index_user_metrics_aggregations_on_user_id", using: :btree

  create_table "user_notification_reads", force: :cascade do |t|
    t.integer  "user_id",    limit: 4, null: false
    t.datetime "seen_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_notification_reads", ["user_id"], name: "index_user_notification_reads_on_user_id", unique: true, using: :btree

  create_table "user_onboardings", force: :cascade do |t|
    t.integer  "user_id",      limit: 4
    t.integer  "current_step", limit: 4,   default: 1
    t.string   "status",       limit: 191
    t.datetime "completed_at"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "user_onboardings", ["user_id"], name: "index_user_onboardings_on_user_id", using: :btree

  create_table "user_preferences", force: :cascade do |t|
    t.integer  "user_id",             limit: 4
    t.string   "key",                 limit: 191
    t.string   "value",               limit: 191
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "preferenceable_id",   limit: 4
    t.string   "preferenceable_type", limit: 191
  end

  add_index "user_preferences", ["user_id", "key", "preferenceable_type", "preferenceable_id"], name: "user_preference_by_user", unique: true, using: :btree
  add_index "user_preferences", ["user_id", "key", "value"], name: "index_user_preferences_on_user_id_and_key_and_value", using: :btree

  create_table "user_profiles", force: :cascade do |t|
    t.integer  "user_id",            limit: 4
    t.text     "expert_topics",      limit: 65535
    t.text     "learning_topics",    limit: 65535
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "hr_job_role_id",     limit: 4
    t.integer  "hr_organization_id", limit: 4
    t.integer  "hr_location_id",     limit: 4
    t.integer  "hr_competency_id",   limit: 4
    t.integer  "hr_domain_id",       limit: 4
    t.string   "time_zone",          limit: 64
    t.text     "onboarding_options", limit: 65535
    t.boolean  "tac_accepted"
    t.string   "language",           limit: 10
    t.date     "dob"
    t.datetime "tac_accepted_at"
    t.string   "job_title",          limit: 80
    t.text     "dashboard_info",     limit: 65535
    t.string   "company",            limit: 150
    t.string   "job_role",           limit: 50
  end

  add_index "user_profiles", ["user_id"], name: "index_user_profiles_on_user_id", using: :btree

  create_table "user_purchases", force: :cascade do |t|
    t.integer  "transaction_id", limit: 4, null: false
    t.integer  "order_id",       limit: 4, null: false
    t.integer  "user_id",        limit: 4, null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "user_roles", force: :cascade do |t|
    t.integer "user_id", limit: 4
    t.integer "role_id", limit: 4
  end

  add_index "user_roles", ["user_id", "role_id"], name: "index_user_roles_on_user_id_and_role_id", using: :btree

  create_table "user_sessions", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.datetime "start_time"
    t.datetime "end_time"
    t.string   "uuid",       limit: 191
  end

  add_index "user_sessions", ["user_id"], name: "index_user_sessions_on_user_id", using: :btree
  add_index "user_sessions", ["uuid"], name: "index_user_sessions_on_uuid", using: :btree

  create_table "user_subscriptions", force: :cascade do |t|
    t.integer  "org_subscription_id", limit: 4,                                           null: false
    t.integer  "organization_id",     limit: 4,                                           null: false
    t.integer  "user_id",             limit: 4,                                           null: false
    t.integer  "order_id",            limit: 4,                                           null: false
    t.decimal  "amount",                          precision: 8, scale: 2,                 null: false
    t.string   "currency",            limit: 10,                          default: "USD", null: false
    t.date     "start_date",                                                              null: false
    t.date     "end_date",                                                                null: false
    t.datetime "created_at",                                                              null: false
    t.datetime "updated_at",                                                              null: false
    t.string   "gateway_id",          limit: 255
  end

  add_index "user_subscriptions", ["order_id"], name: "index_user_subscriptions_on_order_id", using: :btree
  add_index "user_subscriptions", ["user_id"], name: "index_user_subscriptions_on_user_id", using: :btree

  create_table "user_taxonomy_topic_level_metrics", force: :cascade do |t|
    t.integer  "user_id",             limit: 4
    t.integer  "organization_id",     limit: 4
    t.integer  "smartbites_score",    limit: 4,   default: 0
    t.integer  "smartbites_created",  limit: 4,   default: 0
    t.integer  "smartbites_consumed", limit: 4,   default: 0
    t.integer  "time_spent",          limit: 4,   default: 0
    t.string   "period",              limit: 255
    t.integer  "offset",              limit: 4
    t.string   "taxonomy_label",      limit: 255
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
  end

  add_index "user_taxonomy_topic_level_metrics", ["organization_id"], name: "index_user_taxonomy_topic_level_metrics_on_organization_id", using: :btree
  add_index "user_taxonomy_topic_level_metrics", ["user_id"], name: "index_user_taxonomy_topic_level_metrics_on_user_id", using: :btree

  create_table "user_team_assignments", force: :cascade do |t|
    t.integer  "user_id",         limit: 4
    t.integer  "team_id",         limit: 4
    t.integer  "card_id",         limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "title",           limit: 255
    t.string   "assignable_type", limit: 191
    t.integer  "assignable_id",   limit: 4
    t.integer  "assignor_id",     limit: 4
  end

  add_index "user_team_assignments", ["assignable_id"], name: "index_user_team_assignments_on_assignable_id", using: :btree
  add_index "user_team_assignments", ["assignable_type"], name: "index_user_team_assignments_on_assignable_type", using: :btree
  add_index "user_team_assignments", ["card_id"], name: "index_user_team_assignments_on_card_id", using: :btree
  add_index "user_team_assignments", ["team_id"], name: "index_user_team_assignments_on_team_id", using: :btree
  add_index "user_team_assignments", ["user_id"], name: "index_user_team_assignments_on_user_id", using: :btree

  create_table "user_topic_level_metrics", force: :cascade do |t|
    t.integer  "user_id",             limit: 4,               null: false
    t.integer  "tag_id",              limit: 4,               null: false
    t.integer  "smartbites_score",    limit: 4,   default: 0
    t.integer  "smartbites_created",  limit: 4,   default: 0
    t.integer  "smartbites_consumed", limit: 4,   default: 0
    t.integer  "time_spent",          limit: 4,   default: 0
    t.string   "period",              limit: 101
    t.integer  "offset",              limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "organization_id",     limit: 4
  end

  add_index "user_topic_level_metrics", ["organization_id"], name: "index_user_topic_level_metrics_on_organization_id", using: :btree
  add_index "user_topic_level_metrics", ["user_id", "tag_id"], name: "indx_user_topic_level_metrics_user_id_tag_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                    limit: 191
    t.string   "first_name",               limit: 255
    t.string   "last_name",                limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "encrypted_password",       limit: 255,   default: "",    null: false
    t.string   "reset_password_token",     limit: 191
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",            limit: 4,     default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",       limit: 255
    t.string   "last_sign_in_ip",          limit: 255
    t.string   "picture_url",              limit: 255
    t.string   "anonimage_file_name",      limit: 255
    t.string   "anonimage_content_type",   limit: 255
    t.integer  "anonimage_file_size",      limit: 4
    t.datetime "anonimage_updated_at"
    t.string   "avatar_file_name",         limit: 255
    t.string   "avatar_content_type",      limit: 255
    t.integer  "avatar_file_size",         limit: 4
    t.datetime "avatar_updated_at"
    t.integer  "location_id",              limit: 4
    t.string   "confirmation_token",       limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",        limit: 255
    t.string   "handle",                   limit: 191
    t.boolean  "is_brand",                               default: false
    t.text     "bio",                      limit: 65535
    t.string   "coverimage_file_name",     limit: 255
    t.string   "coverimage_content_type",  limit: 255
    t.integer  "coverimage_file_size",     limit: 4
    t.datetime "coverimage_updated_at"
    t.boolean  "password_reset_required",                default: false
    t.boolean  "is_complete",                            default: false
    t.string   "configs",                  limit: 2048
    t.integer  "organization_id",          limit: 4
    t.string   "organization_role",        limit: 191
    t.integer  "parent_user_id",           limit: 4
    t.integer  "job_role_id",              limit: 4
    t.string   "time_in_role",             limit: 255
    t.boolean  "is_active",                              default: true
    t.boolean  "showcase",                               default: false
    t.boolean  "is_suspended",                           default: false
    t.boolean  "is_edcast_admin",                        default: false
    t.string   "federated_identifier",     limit: 255
    t.integer  "failed_attempts",          limit: 4,     default: 0,     null: false
    t.datetime "locked_at"
    t.string   "status",                   limit: 255
    t.boolean  "exclude_from_leaderboard"
    t.integer  "default_team_id",          limit: 4
    t.integer  "data_export_status",       limit: 4
    t.datetime "last_data_export_time"
    t.boolean  "is_anonymized"
    t.boolean  "invitation_accepted"
    t.boolean  "hide_from_leaderboard"
    t.string   "welcome_token",            limit: 191
    t.datetime "sign_out_at"
  end

  add_index "users", ["email", "organization_id"], name: "index_users_on_email_and_organization_id", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", using: :btree
  add_index "users", ["handle"], name: "index_users_on_handle", using: :btree
  add_index "users", ["job_role_id"], name: "index_users_on_job_role_id", using: :btree
  add_index "users", ["organization_id", "handle"], name: "index_users_on_organization_id_and_handle", unique: true, using: :btree
  add_index "users", ["organization_id"], name: "index_users_on_organization_id", using: :btree
  add_index "users", ["organization_role"], name: "index_users_on_organization_role", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["welcome_token"], name: "index_users_on_welcome_token", unique: true, using: :btree

  create_table "users_cards_managements", force: :cascade do |t|
    t.integer  "user_id",    limit: 4,   null: false
    t.integer  "card_id",    limit: 4,   null: false
    t.string   "action",     limit: 255
    t.integer  "target_id",  limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.datetime "deleted_at"
  end

  add_index "users_cards_managements", ["card_id"], name: "index_users_cards_managements_on_card_id", using: :btree
  add_index "users_cards_managements", ["deleted_at"], name: "index_users_cards_managements_on_deleted_at", using: :btree
  add_index "users_cards_managements", ["user_id", "card_id", "target_id"], name: "users_cards_managements_entries", using: :btree
  add_index "users_cards_managements", ["user_id"], name: "index_users_cards_managements_on_user_id", using: :btree

  create_table "users_external_courses", force: :cascade do |t|
    t.integer  "user_id",            limit: 4
    t.integer  "external_course_id", limit: 4
    t.string   "status",             limit: 255
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.text     "raw_info",           limit: 65535
    t.date     "due_date"
    t.date     "assigned_date"
  end

  add_index "users_external_courses", ["user_id", "external_course_id"], name: "index_users_external_courses_on_user_id_and_external_course_id", using: :btree

  create_table "users_integrations", force: :cascade do |t|
    t.integer  "user_id",        limit: 4
    t.integer  "integration_id", limit: 4
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.boolean  "connected",                    default: true
    t.text     "field_values",   limit: 65535
  end

  add_index "users_integrations", ["user_id"], name: "index_users_integrations_on_user_id", using: :btree

  create_table "users_mentors", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.string   "email",      limit: 255
    t.integer  "mentor_id",  limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "users_mentors", ["mentor_id"], name: "index_users_mentors_on_mentor_id", using: :btree
  add_index "users_mentors", ["user_id"], name: "index_users_mentors_on_user_id", using: :btree

  create_table "users_topics_activities", force: :cascade do |t|
    t.integer "organization_id",        limit: 4,     null: false
    t.integer "user_id",                limit: 4,     null: false
    t.text    "topic_counts",           limit: 65535, null: false
    t.integer "start_time",             limit: 4,     null: false
    t.integer "end_time",               limit: 4,     null: false
    t.integer "total_activities_count", limit: 4
  end

  add_index "users_topics_activities", ["organization_id", "user_id"], name: "index_u_t_activities_on_org_and_user_id", using: :btree

  create_table "users_websites", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.string   "url",        limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "users_websites", ["user_id"], name: "index_users_websites_on_user_id", using: :btree

  create_table "version_associations", force: :cascade do |t|
    t.integer "version_id",       limit: 4
    t.string  "foreign_key_name", limit: 50, null: false
    t.integer "foreign_key_id",   limit: 4
  end

  add_index "version_associations", ["foreign_key_name", "foreign_key_id"], name: "index_version_associations_on_foreign_key", using: :btree
  add_index "version_associations", ["version_id"], name: "index_version_associations_on_version_id", using: :btree

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",      limit: 191,        null: false
    t.integer  "item_id",        limit: 4,          null: false
    t.string   "event",          limit: 255,        null: false
    t.string   "whodunnit",      limit: 255
    t.text     "object",         limit: 4294967295
    t.datetime "created_at"
    t.integer  "transaction_id", limit: 4
    t.text     "object_changes", limit: 4294967295
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree
  add_index "versions", ["transaction_id"], name: "index_versions_on_transaction_id", using: :btree

  create_table "video_streams", force: :cascade do |t|
    t.integer  "creator_id",                 limit: 4
    t.integer  "group_id",                   limit: 4
    t.datetime "start_time"
    t.string   "name",                       limit: 255
    t.string   "uuid",                       limit: 191,                  null: false
    t.string   "status",                     limit: 191
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "publish_url",                limit: 255
    t.string   "playback_url",               limit: 1024
    t.string   "password",                   limit: 255
    t.string   "x_id",                       limit: 255
    t.string   "type",                       limit: 255
    t.integer  "comments_count",             limit: 4,    default: 0
    t.integer  "votes_count",                limit: 4,    default: 0
    t.string   "slug",                       limit: 191
    t.datetime "last_stream_at"
    t.integer  "anon_watchers_count",        limit: 4,    default: 0
    t.integer  "thumbnail_file_resource_id", limit: 4
    t.integer  "organization_id",            limit: 4
    t.string   "state",                      limit: 191
    t.boolean  "is_official",                             default: false
    t.integer  "width",                      limit: 4
    t.integer  "height",                     limit: 4
    t.integer  "card_id",                    limit: 4
    t.datetime "deleted_at"
  end

  add_index "video_streams", ["card_id"], name: "index_video_streams_on_card_id", using: :btree
  add_index "video_streams", ["creator_id", "status"], name: "index_video_streams_on_creator_id_and_status", using: :btree
  add_index "video_streams", ["deleted_at"], name: "index_video_streams_on_deleted_at", using: :btree
  add_index "video_streams", ["group_id", "status"], name: "index_video_streams_on_group_id_and_status", using: :btree
  add_index "video_streams", ["last_stream_at"], name: "index_video_streams_on_last_stream_at", using: :btree
  add_index "video_streams", ["slug", "organization_id"], name: "index_video_streams_on_slug_and_organization_id", unique: true, using: :btree
  add_index "video_streams", ["uuid"], name: "index_video_streams_on_uuid", unique: true, using: :btree

  create_table "video_streams_users", force: :cascade do |t|
    t.integer  "video_stream_id", limit: 4, null: false
    t.integer  "user_id",         limit: 4, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "video_streams_users", ["user_id", "video_stream_id"], name: "index_video_streams_users_on_user_id_and_video_stream_id", using: :btree
  add_index "video_streams_users", ["video_stream_id", "user_id"], name: "index_video_streams_users_on_video_stream_id_and_user_id", using: :btree

  create_table "votes", force: :cascade do |t|
    t.integer  "user_id",      limit: 4,   null: false
    t.integer  "votable_id",   limit: 4,   null: false
    t.string   "votable_type", limit: 191, null: false
    t.integer  "weight",       limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "votes", ["deleted_at"], name: "index_votes_on_deleted_at", using: :btree
  add_index "votes", ["votable_id", "votable_type", "user_id"], name: "index_votes_on_votable_id_and_votable_type_and_user_id", using: :btree

  create_table "wallet_balances", force: :cascade do |t|
    t.decimal  "amount",               precision: 8, scale: 2
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
  end

  add_index "wallet_balances", ["user_id"], name: "index_wallet_balances_on_user_id", using: :btree

  create_table "wallet_transaction_audits", force: :cascade do |t|
    t.integer  "wallet_transaction_id",   limit: 4,     null: false
    t.text     "order_data",              limit: 65535, null: false
    t.text     "orderable_data",          limit: 65535, null: false
    t.text     "user_data",               limit: 65535, null: false
    t.text     "wallet_transaction_data", limit: 65535, null: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  add_index "wallet_transaction_audits", ["wallet_transaction_id"], name: "index_wallet_transaction_audits_on_wallet_transaction_id", using: :btree

  create_table "wallet_transactions", force: :cascade do |t|
    t.integer  "order_id",      limit: 4,                                           null: false
    t.integer  "user_id",       limit: 4,                                           null: false
    t.integer  "payment_type",  limit: 4,                             default: 0,   null: false
    t.text     "gateway_data",  limit: 65535
    t.decimal  "amount",                      precision: 8, scale: 2,               null: false
    t.integer  "payment_state", limit: 4,                             default: 0,   null: false
    t.datetime "created_at",                                                        null: false
    t.datetime "updated_at",                                                        null: false
    t.decimal  "balance",                     precision: 8, scale: 2, default: 0.0, null: false
  end

  add_index "wallet_transactions", ["order_id"], name: "index_wallet_transactions_on_order_id", using: :btree
  add_index "wallet_transactions", ["user_id"], name: "index_wallet_transactions_on_user_id", using: :btree

  create_table "webex_configs", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.string   "site_name",       limit: 255
    t.string   "site_id",         limit: 255
    t.string   "partner_id",      limit: 255
    t.integer  "client_id",       limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "organization_id", limit: 4,   null: false
  end

  add_index "webex_configs", ["client_id"], name: "index_webex_configs_on_client_id", using: :btree

  create_table "widgets", force: :cascade do |t|
    t.integer  "creator_id",      limit: 4,                          null: false
    t.integer  "organization_id", limit: 4,                          null: false
    t.integer  "parent_id",       limit: 4,                          null: false
    t.string   "parent_type",     limit: 20,                         null: false
    t.text     "code",            limit: 4294967295,                 null: false
    t.string   "context",         limit: 20,                         null: false
    t.boolean  "enabled",                            default: false
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
  end

  add_index "widgets", ["organization_id"], name: "index_widgets_on_organization_id", using: :btree

  create_table "workflow_patch_revisions", force: :cascade do |t|
    t.integer  "organization_id",  limit: 4
    t.string   "wuid",             limit: 191
    t.string   "data_id",          limit: 191
    t.integer  "workflow_id",      limit: 4
    t.integer  "output_source_id", limit: 4
    t.string   "workflow_type",    limit: 255
    t.string   "patch_id",         limit: 255
    t.string   "patch_type",       limit: 255
    t.string   "org_name",         limit: 255
    t.integer  "version",          limit: 4,     default: 0
    t.text     "data",             limit: 65535
    t.text     "params",           limit: 65535
    t.text     "meta",             limit: 65535
    t.boolean  "completed"
    t.boolean  "in_progress"
    t.boolean  "failed"
    t.string   "job_id",           limit: 191
    t.text     "processing_info",  limit: 65535
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  add_index "workflow_patch_revisions", ["data_id"], name: "index_workflow_patch_revisions_on_data_id", using: :btree
  add_index "workflow_patch_revisions", ["job_id"], name: "index_workflow_patch_revisions_on_job_id", using: :btree
  add_index "workflow_patch_revisions", ["organization_id", "version"], name: "index_on_org_version", using: :btree
  add_index "workflow_patch_revisions", ["organization_id", "workflow_id", "output_source_id"], name: "index_on_org_workflow_output_ids", using: :btree
  add_index "workflow_patch_revisions", ["organization_id"], name: "index_workflow_patch_revisions_on_organization_id", using: :btree
  add_index "workflow_patch_revisions", ["output_source_id"], name: "index_workflow_patch_revisions_on_output_source_id", using: :btree
  add_index "workflow_patch_revisions", ["version"], name: "index_workflow_patch_revisions_on_version", using: :btree
  add_index "workflow_patch_revisions", ["workflow_id"], name: "index_workflow_patch_revisions_on_workflow_id", using: :btree
  add_index "workflow_patch_revisions", ["wuid"], name: "index_workflow_patch_revisions_on_wuid", using: :btree

  create_table "workflows", force: :cascade do |t|
    t.integer  "organization_id",       limit: 4,                     null: false
    t.integer  "user_id",               limit: 4,                     null: false
    t.string   "name",                  limit: 255
    t.string   "status",                limit: 255,   default: "new"
    t.boolean  "is_enabled",                          default: false
    t.string   "wf_type",               limit: 255,                   null: false
    t.text     "wf_input_source",       limit: 65535,                 null: false
    t.text     "wf_actionable_columns", limit: 65535,                 null: false
    t.text     "wf_rules",              limit: 65535
    t.text     "wf_action",             limit: 65535,                 null: false
    t.text     "wf_output_result",      limit: 65535
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
  end

  add_index "workflows", ["organization_id"], name: "index_workflows_on_organization_id", using: :btree
  add_index "workflows", ["user_id"], name: "index_workflows_on_user_id", using: :btree

  create_table "xapi_activities", force: :cascade do |t|
    t.integer  "actor_id",          limit: 4
    t.integer  "object_id",         limit: 4
    t.string   "object_type",       limit: 255
    t.string   "object_definition", limit: 255
    t.integer  "organization_id",   limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "verb",              limit: 191
    t.string   "verb_identifier",   limit: 191
  end

  add_index "xapi_activities", ["actor_id"], name: "index_xapi_activities_on_actor_id", using: :btree
  add_index "xapi_activities", ["organization_id"], name: "index_xapi_activities_on_organization_id", using: :btree

  create_table "xapi_credentials", force: :cascade do |t|
    t.string   "end_point",        limit: 255
    t.string   "lrs_login_key",    limit: 255
    t.string   "lrs_password_key", limit: 255
    t.string   "lrs_api_version",  limit: 255
    t.integer  "organization_id",  limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "xapi_credentials", ["organization_id"], name: "index_xapi_credentials_on_organization_id", using: :btree

  add_foreign_key "activity_streams", "cards"
  add_foreign_key "activity_streams", "organizations"
  add_foreign_key "activity_streams", "users"
  add_foreign_key "assignment_notice_statuses", "users"
  add_foreign_key "bookmarks", "users"
  add_foreign_key "card_metadata", "cards"
  add_foreign_key "card_reportings", "cards"
  add_foreign_key "card_reportings", "users"
  add_foreign_key "card_team_permissions", "cards"
  add_foreign_key "card_team_permissions", "teams"
  add_foreign_key "card_user_accesses", "cards"
  add_foreign_key "card_user_accesses", "users"
  add_foreign_key "card_user_permissions", "cards"
  add_foreign_key "card_user_permissions", "users"
  add_foreign_key "card_user_shares", "cards"
  add_foreign_key "card_user_shares", "users"
  add_foreign_key "cards_ratings", "cards"
  add_foreign_key "cards_ratings", "users"
  add_foreign_key "channels_curators", "channels"
  add_foreign_key "channels_curators", "users"
  add_foreign_key "clc_channels_records", "cards"
  add_foreign_key "clc_channels_records", "channels"
  add_foreign_key "clc_channels_records", "users"
  add_foreign_key "clc_organizations_records", "cards"
  add_foreign_key "clc_organizations_records", "organizations"
  add_foreign_key "clc_organizations_records", "users"
  add_foreign_key "clc_progresses", "clcs"
  add_foreign_key "clc_progresses", "users"
  add_foreign_key "clc_teams_records", "cards"
  add_foreign_key "clc_teams_records", "teams"
  add_foreign_key "clc_teams_records", "users"
  add_foreign_key "clcs", "organizations"
  add_foreign_key "comment_reportings", "comments"
  add_foreign_key "comment_reportings", "users"
  add_foreign_key "competencies", "job_roles"
  add_foreign_key "custom_fields", "organizations"
  add_foreign_key "dismissed_contents", "users"
  add_foreign_key "domains", "categories"
  add_foreign_key "domains_organizations", "domains"
  add_foreign_key "domains_organizations", "organizations"
  add_foreign_key "domains_skills", "domains"
  add_foreign_key "domains_skills", "tags", column: "skill_id"
  add_foreign_key "email_templates", "email_templates", column: "default_id"
  add_foreign_key "email_templates", "organizations"
  add_foreign_key "email_templates", "users", column: "creator_id"
  add_foreign_key "goal_events", "goals"
  add_foreign_key "goals", "users"
  add_foreign_key "industries_organizations", "industries"
  add_foreign_key "industries_organizations", "organizations"
  add_foreign_key "job_roles", "industries"
  add_foreign_key "job_roles_organizations", "job_roles"
  add_foreign_key "job_roles_organizations", "organizations"
  add_foreign_key "leaps", "cards"
  add_foreign_key "monthly_top_video_streams", "video_streams"
  add_foreign_key "notification_configs", "organizations"
  add_foreign_key "notification_configs", "users"
  add_foreign_key "notification_entries", "users"
  add_foreign_key "organization_email_domains", "organizations"
  add_foreign_key "organization_prices", "org_subscriptions"
  add_foreign_key "organization_profiles", "organizations"
  add_foreign_key "partner_centers", "organizations"
  add_foreign_key "partner_centers", "users"
  add_foreign_key "recharge_prices", "recharges"
  add_foreign_key "scorm_details", "cards"
  add_foreign_key "skills_tags", "tags"
  add_foreign_key "skills_tags", "tags", column: "skill_id"
  add_foreign_key "skills_users", "skills"
  add_foreign_key "skills_users", "users"
  add_foreign_key "structured_items", "structures"
  add_foreign_key "structures", "organizations"
  add_foreign_key "structures", "users", column: "creator_id"
  add_foreign_key "teams_channels_follows", "channels"
  add_foreign_key "teams_channels_follows", "teams"
  add_foreign_key "topic_daily_learnings", "organizations"
  add_foreign_key "upsell_requests", "users"
  add_foreign_key "user_completions", "users"
  add_foreign_key "user_custom_fields", "custom_fields"
  add_foreign_key "user_custom_fields", "users"
  add_foreign_key "user_import_statuses", "invitation_files"
  add_foreign_key "user_onboardings", "users"
  add_foreign_key "user_taxonomy_topic_level_metrics", "organizations"
  add_foreign_key "user_taxonomy_topic_level_metrics", "users"
  add_foreign_key "users", "job_roles"
  add_foreign_key "users_cards_managements", "cards"
  add_foreign_key "users_cards_managements", "users"
  add_foreign_key "users_mentors", "users"
  add_foreign_key "users_websites", "users"
  add_foreign_key "wallet_balances", "users"
  add_foreign_key "wallet_transactions", "orders"
  add_foreign_key "wallet_transactions", "users"
  add_foreign_key "workflow_patch_revisions", "organizations"
  add_foreign_key "xapi_activities", "organizations"
  add_foreign_key "xapi_credentials", "organizations"
end

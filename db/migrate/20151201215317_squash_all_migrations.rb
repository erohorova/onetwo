class SquashAllMigrations < ActiveRecord::Migration
  def change
    create_table "access_logs", force: :cascade do |t|
      t.integer  "user_id",                    limit: 4
      t.string   "platform",                   limit: 191
      t.string   "access_uri",                 limit: 4096
      t.date     "access_at"
      t.datetime "created_at"
      t.string   "annotation",                 limit: 255
      t.boolean  "is_signup_event",            limit: 1,     default: false
      t.boolean  "insider_follow_processed",   limit: 1,     default: false
      t.text     "insiders_already_following", limit: 65535
    end

    add_index "access_logs", ["user_id", "platform", "access_at"], name: "index_access_logs_on_user_id_and_platform_and_access_at", unique: true, using: :btree

    create_table "approvals", force: :cascade do |t|
      t.integer  "user_id",         limit: 4,   null: false
      t.integer  "approvable_id",   limit: 4,   null: false
      t.string   "approvable_type", limit: 191, null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "approvals", ["approvable_type", "approvable_id"], name: "index_approvals_on_approvable_type_and_approvable_id", using: :btree

    create_table "background_workers", force: :cascade do |t|
      t.string   "name",       limit: 191
      t.string   "command",    limit: 255
      t.string   "state",      limit: 255
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "background_workers", ["name"], name: "index_background_workers_on_name", unique: true, using: :btree

    create_table "batch_jobs", force: :cascade do |t|
      t.string   "name",          limit: 255
      t.datetime "last_run_time"
    end

    create_table "beta_registrations", force: :cascade do |t|
      t.string   "email",        limit: 255,   null: false
      t.string   "first_name",   limit: 255
      t.string   "last_name",    limit: 255
      t.string   "organization", limit: 255
      t.text     "reason",       limit: 65535
      t.string   "status",       limit: 255
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "branchmetrics_invitations", force: :cascade do |t|
      t.string   "recipient_email",   limit: 255
      t.string   "token",             limit: 191
      t.datetime "sent_at"
      t.integer  "recipient_id",      limit: 4
      t.datetime "accepted_at"
      t.string   "first_name",        limit: 255
      t.string   "last_name",         limit: 255
      t.text     "branchmetrics_url", limit: 65535
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "channel",           limit: 255
    end

    add_index "branchmetrics_invitations", ["token"], name: "index_branchmetrics_invitations_on_token", unique: true, using: :btree

    create_table "card_pack_relations", force: :cascade do |t|
      t.integer  "cover_id",   limit: 4
      t.integer  "from_id",    limit: 4
      t.integer  "to_id",      limit: 4
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "title",      limit: 255
      t.string   "from_type",  limit: 191
    end

    add_index "card_pack_relations", ["cover_id", "from_id", "from_type"], name: "index_card_pack_relations_on_cover_id_and_from_id_and_from_type", unique: true, using: :btree
    add_index "card_pack_relations", ["cover_id", "to_id"], name: "index_card_pack_relations_on_cover_id_and_to_id", using: :btree
    add_index "card_pack_relations", ["cover_id"], name: "index_card_pack_relations_on_cover_id", using: :btree
    add_index "card_pack_relations", ["from_id"], name: "index_card_pack_relations_on_from_id", using: :btree
    add_index "card_pack_relations", ["to_id"], name: "index_card_pack_relations_on_to_id", using: :btree

    create_table "card_queries", force: :cascade do |t|
      t.string   "q",            limit: 191, null: false
      t.string   "normalized_q", limit: 191, null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "card_queries", ["normalized_q"], name: "index_card_queries_on_normalized_q", unique: true, using: :btree
    add_index "card_queries", ["q"], name: "index_card_queries_on_q", unique: true, using: :btree

    create_table "card_updates", force: :cascade do |t|
      t.integer  "card_id",     limit: 4
      t.integer  "user_id",     limit: 4
      t.boolean  "unseen",      limit: 1,   default: true
      t.string   "update_type", limit: 255
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "card_updates", ["user_id"], name: "index_card_updates_on_user_id", using: :btree

    create_table "card_updates_causal_users", force: :cascade do |t|
      t.integer  "user_id",        limit: 4
      t.integer  "card_update_id", limit: 4
      t.boolean  "anonymous",      limit: 1, default: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "card_updates_causal_users", ["card_update_id"], name: "index_card_updates_causal_users_on_card_update_id", using: :btree

    create_table "card_usage_data", force: :cascade do |t|
      t.integer  "card_id",    limit: 4,     null: false
      t.integer  "user_id",    limit: 4,     null: false
      t.text     "data",       limit: 65535
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "card_usage_data", ["card_id", "user_id"], name: "index_card_usage_data_on_card_id_and_user_id", unique: true, using: :btree

    create_table "cards", force: :cascade do |t|
      t.integer  "cards_config_id",    limit: 4,                     null: false
      t.integer  "group_id",           limit: 4
      t.string   "author_first_name",  limit: 255
      t.string   "author_last_name",   limit: 255
      t.text     "author_picture_url", limit: 65535
      t.integer  "super_card_id",      limit: 4
      t.integer  "author_id",          limit: 4
      t.datetime "created_at"
      t.datetime "updated_at"
      t.boolean  "is_public",          limit: 1,     default: false
      t.boolean  "is_manual",          limit: 1,     default: false
      t.text     "features",           limit: 65535
      t.integer  "client_id",          limit: 4
      t.string   "client_item_id",     limit: 191
      t.integer  "comments_count",     limit: 4,     default: 0
      t.integer  "order_index",        limit: 4,     default: 0
      t.string   "slug",               limit: 191
      t.integer  "impressions_count",  limit: 4,     default: 0
      t.integer  "outbounds_count",    limit: 4,     default: 0
      t.integer  "shares_count",       limit: 4,     default: 0
      t.integer  "answers_count",      limit: 4,     default: 0
      t.integer  "expansions_count",   limit: 4,     default: 0
      t.integer  "plays_count",        limit: 4,     default: 0
      t.boolean  "hidden",             limit: 1,     default: false
      t.integer  "resource_id",        limit: 4
    end

    add_index "cards", ["client_id", "client_item_id"], name: "index_cards_on_client_id_and_client_item_id", using: :btree
    add_index "cards", ["is_manual"], name: "index_cards_on_is_manual", using: :btree
    add_index "cards", ["slug"], name: "index_cards_on_slug", unique: true, using: :btree
    add_index "cards", ["super_card_id"], name: "index_cards_on_super_card_id", using: :btree

    create_table "cards_actions", force: :cascade do |t|
      t.integer "cards_config_id",   limit: 4,                     null: false
      t.string  "display_text",      limit: 255
      t.text    "post_url",          limit: 65535
      t.text    "function_name",     limit: 65535
      t.boolean "is_internal",       limit: 1
      t.boolean "text_postback",     limit: 1
      t.string  "name",              limit: 255
      t.string  "user_role",         limit: 255
      t.string  "action_type",       limit: 255
      t.float   "signature_weight",  limit: 24
      t.text    "feedback_message",  limit: 65535
      t.string  "input_type",        limit: 255
      t.integer "reverse_action_id", limit: 4
      t.boolean "feedback_enabled",  limit: 1,     default: false
    end

    add_index "cards_actions", ["cards_config_id"], name: "index_cards_actions_on_cards_config_id", using: :btree

    create_table "cards_configs", force: :cascade do |t|
      t.string   "card_type",          limit: 191
      t.string   "action_layout_type", limit: 255
      t.string   "ui_layout_type",     limit: 255
      t.integer  "user_id",            limit: 4
      t.boolean  "shareable",          limit: 1
      t.datetime "created_at"
      t.datetime "updated_at"
      t.text     "roles",              limit: 65535
    end

    add_index "cards_configs", ["card_type"], name: "index_cards_configs_on_card_type", using: :btree

    create_table "cards_contents", force: :cascade do |t|
      t.integer "card_id",      limit: 4,                 null: false
      t.string  "name",         limit: 255
      t.text    "value",        limit: 65535
      t.integer "lock_version", limit: 4,     default: 0
    end

    add_index "cards_contents", ["card_id"], name: "index_cards_contents_on_card_id", using: :btree

    create_table "cards_default_signatures", force: :cascade do |t|
      t.string   "role",                     limit: 191
      t.integer  "cards_metadata_config_id", limit: 4
      t.float    "value",                    limit: 24
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "cards_default_signatures", ["cards_metadata_config_id"], name: "index_cards_default_signatures_on_cards_metadata_config_id", using: :btree
    add_index "cards_default_signatures", ["role"], name: "index_cards_default_signatures_on_role", using: :btree

    create_table "cards_disallowed_actions", force: :cascade do |t|
      t.integer "card_id",         limit: 4, null: false
      t.integer "cards_action_id", limit: 4, null: false
    end

    add_index "cards_disallowed_actions", ["card_id", "cards_action_id"], name: "index_cards_disallowed_actions_on_card_id_and_cards_action_id", unique: true, using: :btree

    create_table "cards_items", force: :cascade do |t|
      t.string  "card_id",        limit: 191
      t.string  "item_type",      limit: 255, null: false
      t.integer "item_id",        limit: 4,   null: false
      t.string  "client_item_id", limit: 191
    end

    add_index "cards_items", ["card_id"], name: "index_cards_items_on_card_id", unique: true, using: :btree
    add_index "cards_items", ["client_item_id"], name: "index_cards_items_on_client_item_id", unique: true, using: :btree

    create_table "cards_metadata", force: :cascade do |t|
      t.integer "card_id",                  limit: 4,     null: false
      t.integer "cards_metadata_config_id", limit: 4,     null: false
      t.text    "value",                    limit: 65535
      t.integer "delegate_card_id",         limit: 4
    end

    add_index "cards_metadata", ["card_id", "cards_metadata_config_id"], name: "index_cards_metadata_on_card_id_and_cards_metadata_config_id", unique: true, using: :btree

    create_table "cards_metadata_configs", force: :cascade do |t|
      t.integer "cards_config_id", limit: 4,   null: false
      t.string  "name",            limit: 255
      t.string  "datatype",        limit: 255
      t.string  "category",        limit: 255
    end

    add_index "cards_metadata_configs", ["cards_config_id"], name: "index_cards_metadata_configs_on_cards_config_id", using: :btree

    create_table "cards_users", force: :cascade do |t|
      t.integer  "card_id",            limit: 4
      t.integer  "user_id",            limit: 4
      t.string   "state",              limit: 191
      t.datetime "created_at"
      t.datetime "updated_at"
      t.boolean  "response_available", limit: 1,     default: false
      t.text     "response_data",      limit: 65535
    end

    add_index "cards_users", ["card_id", "user_id"], name: "index_cards_users_on_card_id_and_user_id", using: :btree
    add_index "cards_users", ["user_id", "state"], name: "index_cards_users_on_user_id_and_state", using: :btree

    create_table "channel_content_sources", force: :cascade do |t|
      t.boolean  "enabled",           limit: 1, default: true
      t.integer  "channel_id",        limit: 4
      t.integer  "content_source_id", limit: 4
      t.datetime "created_at",                                 null: false
      t.datetime "updated_at",                                 null: false
    end

    create_table "channels", force: :cascade do |t|
      t.string   "label",                     limit: 255
      t.string   "description",               limit: 255
      t.integer  "card_query_id",             limit: 4
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "image_file_name",           limit: 255
      t.string   "image_content_type",        limit: 255
      t.integer  "image_file_size",           limit: 4
      t.datetime "image_updated_at"
      t.string   "slug",                      limit: 191
      t.boolean  "visible",                   limit: 1,    default: true,  null: false
      t.string   "mobile_image_file_name",    limit: 255
      t.string   "mobile_image_content_type", limit: 255
      t.integer  "mobile_image_file_size",    limit: 4
      t.datetime "mobile_image_updated_at"
      t.boolean  "is_private",                limit: 1,    default: false
      t.integer  "client_id",                 limit: 4
      t.string   "keywords",                  limit: 1024
      t.boolean  "autopull_content",          limit: 1,    default: false
      t.boolean  "show_authors",              limit: 1,    default: false
      t.boolean  "visible_during_onboarding", limit: 1,    default: false
      t.boolean  "only_authors_can_post",     limit: 1,    default: true
      t.integer  "group_id",                  limit: 4
      t.integer  "organization_id",           limit: 4
      t.string   "content_discovery_freq",    limit: 255
      t.datetime "last_discovered_at"
      t.boolean  "is_general",                limit: 1,    default: false
    end

    add_index "channels", ["organization_id"], name: "index_channels_on_organization_id", using: :btree
    add_index "channels", ["slug"], name: "index_channels_on_slug", unique: true, using: :btree

    create_table "channels_cards", force: :cascade do |t|
      t.integer "channel_id", limit: 4, null: false
      t.integer "card_id",    limit: 4, null: false
    end

    add_index "channels_cards", ["card_id", "channel_id"], name: "index_channels_cards_on_card_id_and_channel_id", using: :btree
    add_index "channels_cards", ["channel_id", "card_id"], name: "index_channels_cards_on_channel_id_and_card_id", using: :btree

    create_table "channels_users", force: :cascade do |t|
      t.integer  "user_id",    limit: 4
      t.integer  "channel_id", limit: 4
      t.string   "as_type",    limit: 191, default: "author"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "channels_users", ["channel_id", "user_id", "as_type"], name: "index_channels_users_on_channel_id_and_user_id_and_as_type", using: :btree

    create_table "channels_video_streams", force: :cascade do |t|
      t.integer  "video_stream_id", limit: 4, null: false
      t.integer  "channel_id",      limit: 4, null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "channels_video_streams", ["channel_id", "video_stream_id"], name: "index_channels_video_streams_on_channel_id_and_video_stream_id", using: :btree
    add_index "channels_video_streams", ["video_stream_id", "channel_id"], name: "index_channels_video_streams_on_video_stream_id_and_channel_id", using: :btree

    create_table "clients", force: :cascade do |t|
      t.string   "name",           limit: 255
      t.integer  "user_id",        limit: 4
      t.datetime "created_at"
      t.datetime "updated_at"
      t.text     "redirect_uri",   limit: 65535
      t.string   "scopes",         limit: 255,   default: "",   null: false
      t.boolean  "social_enabled", limit: 1,     default: true
    end

    create_table "clients_admins", force: :cascade do |t|
      t.integer "client_id", limit: 4
      t.integer "admin_id",  limit: 4
    end

    add_index "clients_admins", ["admin_id", "client_id"], name: "index_clients_admins_on_admin_id_and_client_id", using: :btree

    create_table "clients_users", force: :cascade do |t|
      t.integer  "user_id",        limit: 4
      t.integer  "client_id",      limit: 4
      t.string   "client_user_id", limit: 191
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "clients_users", ["client_user_id", "client_id"], name: "index_clients_users_on_client_user_id_and_client_id", unique: true, using: :btree

    create_table "comments", force: :cascade do |t|
      t.text     "message",          limit: 65535,                 null: false
      t.string   "type",             limit: 255
      t.integer  "commentable_id",   limit: 4
      t.string   "commentable_type", limit: 191
      t.integer  "user_id",          limit: 4,                     null: false
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "votes_count",      limit: 4,     default: 0
      t.integer  "resource_id",      limit: 4
      t.boolean  "hidden",           limit: 1,     default: false
      t.boolean  "anonymous",        limit: 1,     default: false
      t.boolean  "is_mobile",        limit: 1,     default: false
      t.string   "client_item_id",   limit: 191
    end

    add_index "comments", ["commentable_id", "commentable_type", "client_item_id"], name: "index_comments_on_client_item_id", using: :btree
    add_index "comments", ["commentable_id", "commentable_type", "created_at"], name: "commentable_with_create_at", using: :btree
    add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

    create_table "configurations", force: :cascade do |t|
      t.string   "name",       limit: 191, null: false
      t.string   "value",      limit: 255, null: false
      t.string   "type",       limit: 255, null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "configurations", ["name"], name: "index_configurations_on_name", unique: true, using: :btree

    create_table "content_sources", force: :cascade do |t|
      t.text     "source",     limit: 65535, null: false
      t.datetime "created_at",               null: false
      t.datetime "updated_at",               null: false
    end

    create_table "contexts", force: :cascade do |t|
      t.text "context_set", limit: 65535, null: false
      t.text "styles",      limit: 65535, null: false
    end

    create_table "conversations", force: :cascade do |t|
      t.string   "channel",         limit: 191
      t.integer  "interested_id",   limit: 4
      t.string   "interested_type", limit: 191
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "conversations", ["channel"], name: "index_conversations_on_channel", unique: true, using: :btree
    add_index "conversations", ["interested_type", "interested_id"], name: "index_conversations_on_interested_type_and_interested_id", using: :btree

    create_table "conversations_users", force: :cascade do |t|
      t.integer  "user_id",         limit: 4
      t.integer  "conversation_id", limit: 4
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "conversations_users", ["conversation_id"], name: "index_conversations_users_on_conversation_id", using: :btree
    add_index "conversations_users", ["user_id"], name: "index_conversations_users_on_user_id", using: :btree

    create_table "credentials", force: :cascade do |t|
      t.string   "api_key",       limit: 191
      t.string   "shared_secret", limit: 255
      t.string   "state",         limit: 255, default: "active"
      t.integer  "client_id",     limit: 4
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "label",         limit: 255
    end

    add_index "credentials", ["api_key"], name: "index_credentials_on_api_key", unique: true, using: :btree

    create_table "crop_image_data", force: :cascade do |t|
      t.integer  "croppable_id",   limit: 4
      t.string   "croppable_type", limit: 255
      t.string   "crop_column",    limit: 255
      t.string   "file_signature", limit: 255
      t.integer  "x1",             limit: 4
      t.integer  "y1",             limit: 4
      t.integer  "x2",             limit: 4
      t.integer  "y2",             limit: 4
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "daily_card_stats", force: :cascade do |t|
      t.integer  "card_id",     limit: 4,             null: false
      t.date     "date"
      t.integer  "impressions", limit: 4, default: 0
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "daily_card_stats", ["card_id", "date"], name: "index_daily_card_stats_on_card_id_and_date", unique: true, using: :btree

    create_table "daily_group_stats", force: :cascade do |t|
      t.integer  "group_id",                        limit: 4,                 null: false
      t.date     "date"
      t.integer  "total_users_with_posts",          limit: 4,     default: 0
      t.integer  "total_users_with_comments",       limit: 4,     default: 0
      t.integer  "total_active_forum_users",        limit: 4,     default: 0
      t.integer  "total_users_with_events",         limit: 4,     default: 0
      t.integer  "total_users_with_event_response", limit: 4,     default: 0
      t.integer  "total_active_events_users",       limit: 4,     default: 0
      t.integer  "total_posts",                     limit: 4,     default: 0
      t.integer  "total_unanswered_posts",          limit: 4,     default: 0
      t.integer  "total_answered_posts",            limit: 4,     default: 0
      t.integer  "total_new_events",                limit: 4,     default: 0
      t.datetime "created_at"
      t.datetime "updated_at"
      t.text     "trending_tags",                   limit: 65535
      t.integer  "total_comments",                  limit: 4,     default: 0
      t.integer  "total_active_mobile_forum_users", limit: 4,     default: 0
      t.text     "trending_contexts",               limit: 65535
    end

    add_index "daily_group_stats", ["group_id", "date"], name: "index_daily_group_stats_on_group_id_and_date", unique: true, using: :btree

    create_table "delayable_joins", force: :cascade do |t|
      t.integer "delayable_id",   limit: 4
      t.string  "delayable_type", limit: 191
      t.integer "delayed_job_id", limit: 4
    end

    add_index "delayable_joins", ["delayable_type", "delayable_id", "delayed_job_id"], name: "delayable_join_index", using: :btree

    create_table "delayed_jobs", force: :cascade do |t|
      t.integer  "priority",   limit: 4,     default: 0, null: false
      t.integer  "attempts",   limit: 4,     default: 0, null: false
      t.text     "handler",    limit: 65535,             null: false
      t.text     "last_error", limit: 65535
      t.datetime "run_at"
      t.datetime "locked_at"
      t.datetime "failed_at"
      t.string   "locked_by",  limit: 255
      t.string   "queue",      limit: 255
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "job_id",     limit: 191
      t.string   "job_class",  limit: 191
    end

    add_index "delayed_jobs", ["job_class", "job_id"], name: "index_delayed_jobs_on_job_class_and_job_id", using: :btree
    add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

    create_table "developer_admin_invitations", force: :cascade do |t|
      t.integer  "sender_id",       limit: 4,   null: false
      t.integer  "client_id",       limit: 4,   null: false
      t.string   "recipient_email", limit: 255, null: false
      t.string   "token",           limit: 255, null: false
      t.datetime "sent_at"
      t.integer  "recipient_id",    limit: 4
      t.datetime "accepted_at"
      t.string   "first_name",      limit: 255
      t.string   "last_name",       limit: 255
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "emails", force: :cascade do |t|
      t.integer  "user_id",            limit: 4
      t.string   "email",              limit: 191
      t.string   "confirmation_token", limit: 255
      t.boolean  "confirmed",          limit: 1
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "contextable_id",     limit: 4
      t.string   "contextable_type",   limit: 255
      t.datetime "bounced_at"
    end

    add_index "emails", ["user_id", "email"], name: "index_emails_on_user_id_and_email", unique: true, using: :btree
    add_index "emails", ["user_id"], name: "index_emails_on_user_id", using: :btree

    create_table "events", force: :cascade do |t|
      t.string   "name",                       limit: 255,               null: false
      t.string   "address",                    limit: 255
      t.text     "description",                limit: 65535
      t.float    "latitude",                   limit: 24
      t.float    "longitude",                  limit: 24
      t.string   "state",                      limit: 255,               null: false
      t.string   "privacy",                    limit: 255,               null: false
      t.integer  "comments_count",             limit: 4,     default: 0
      t.integer  "group_id",                   limit: 4
      t.integer  "user_id",                    limit: 4
      t.datetime "created_at"
      t.datetime "updated_at"
      t.datetime "event_start"
      t.datetime "event_end"
      t.string   "ical_file_name",             limit: 255
      t.string   "ical_content_type",          limit: 255
      t.integer  "ical_file_size",             limit: 4
      t.datetime "ical_updated_at"
      t.string   "hangout_type",               limit: 255
      t.boolean  "hangout_discussion_enabled", limit: 1
      t.string   "hangout_url",                limit: 255
      t.string   "youtube_url",                limit: 255
      t.boolean  "context_enabled",            limit: 1
    end

    add_index "events", ["group_id"], name: "index_events_on_group_id", using: :btree
    add_index "events", ["user_id"], name: "index_events_on_user_id", using: :btree

    create_table "events_users", force: :cascade do |t|
      t.integer  "event_id",    limit: 4
      t.integer  "user_id",     limit: 4
      t.string   "rsvp",        limit: 255
      t.datetime "accepted_at"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "events_users", ["event_id", "user_id"], name: "index_events_users_on_event_id_and_user_id", unique: true, using: :btree

    create_table "file_resources", force: :cascade do |t|
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "attachment_file_name",    limit: 255
      t.string   "attachment_content_type", limit: 255
      t.integer  "attachment_file_size",    limit: 4
      t.datetime "attachment_updated_at"
      t.integer  "attachable_id",           limit: 4
      t.string   "attachable_type",         limit: 255
      t.text     "url",                     limit: 65535
      t.integer  "user_id",                 limit: 4
    end

    create_table "follows", force: :cascade do |t|
      t.integer  "user_id",           limit: 4
      t.integer  "followable_id",     limit: 4
      t.string   "followable_type",   limit: 191
      t.datetime "created_at"
      t.datetime "updated_at"
      t.datetime "deleted_at"
      t.boolean  "skip_notification", limit: 1,   default: false
    end

    add_index "follows", ["deleted_at"], name: "index_follows_on_deleted_at", using: :btree
    add_index "follows", ["followable_type", "followable_id"], name: "index_follows_on_followable_type_and_followable_id", using: :btree
    add_index "follows", ["followable_type"], name: "index_follows_on_followable_type", using: :btree
    add_index "follows", ["skip_notification"], name: "index_follows_on_skip_notification", using: :btree
    add_index "follows", ["user_id"], name: "index_follows_on_user_id", using: :btree

    create_table "friendly_id_slugs", force: :cascade do |t|
      t.string   "slug",           limit: 191, null: false
      t.integer  "sluggable_id",   limit: 4,   null: false
      t.string   "sluggable_type", limit: 50
      t.string   "scope",          limit: 191
      t.datetime "created_at"
    end

    add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
    add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
    add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
    add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

    create_table "global_influencers", force: :cascade do |t|
      t.integer  "user_id",                   limit: 4,                 null: false
      t.datetime "created_at"
      t.datetime "updated_at"
      t.boolean  "visible_during_onboarding", limit: 1, default: false
    end

    add_index "global_influencers", ["visible_during_onboarding"], name: "index_global_influencers_on_visible_during_onboarding", using: :btree

    create_table "groups", force: :cascade do |t|
      t.string   "name",                 limit: 255,                   null: false
      t.text     "description",          limit: 65535
      t.string   "website_url",          limit: 255
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "client_id",            limit: 4
      t.string   "client_resource_id",   limit: 191
      t.string   "type",                 limit: 255
      t.string   "access",               limit: 255
      t.integer  "users_count",          limit: 4,     default: 0
      t.text     "course_data_url",      limit: 65535
      t.text     "image_url",            limit: 65535
      t.boolean  "details_locked",       limit: 1,     default: false
      t.integer  "creator_id",           limit: 4
      t.integer  "parent_id",            limit: 4
      t.boolean  "close_access",         limit: 1,     default: false
      t.integer  "topic_id",             limit: 4
      t.string   "picture_file_name",    limit: 255
      t.string   "picture_content_type", limit: 255
      t.integer  "picture_file_size",    limit: 4
      t.datetime "picture_updated_at"
      t.boolean  "is_private",           limit: 1,     default: false
      t.datetime "start_date"
      t.string   "course_term",          limit: 255
      t.datetime "end_date"
    end

    add_index "groups", ["client_id", "client_resource_id"], name: "index_groups_on_client_id_and_client_resource_id", unique: true, using: :btree
    add_index "groups", ["parent_id"], name: "index_groups_on_parent_id", using: :btree
    add_index "groups", ["topic_id"], name: "index_groups_on_topic_id", using: :btree

    create_table "groups_users", force: :cascade do |t|
      t.integer  "user_id",    limit: 4
      t.integer  "group_id",   limit: 4
      t.string   "as_type",    limit: 191
      t.datetime "created_at"
      t.datetime "updated_at"
      t.boolean  "banned",     limit: 1,   default: false
      t.string   "role_label", limit: 255
    end

    add_index "groups_users", ["as_type"], name: "index_groups_users_on_as_type", using: :btree
    add_index "groups_users", ["group_id"], name: "index_groups_users_on_group_id", using: :btree
    add_index "groups_users", ["user_id", "group_id", "as_type", "banned"], name: "user_in_group", using: :btree
    add_index "groups_users", ["user_id", "group_id"], name: "index_groups_users_on_user_id_and_group_id", unique: true, using: :btree

    create_table "health_checks", force: :cascade do |t|
      t.text "data", limit: 65535
    end

    create_table "identity_providers", force: :cascade do |t|
      t.string   "type",        limit: 191
      t.string   "uid",         limit: 255
      t.integer  "user_id",     limit: 4
      t.string   "token",       limit: 1024
      t.string   "secret",      limit: 255
      t.datetime "expires_at"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "token_scope", limit: 255
      t.boolean  "auth",        limit: 1
    end

    add_index "identity_providers", ["type", "user_id"], name: "index_identity_providers_on_type_and_user_id", using: :btree
    add_index "identity_providers", ["user_id"], name: "index_identity_providers_on_user_id", using: :btree

    create_table "impersonation_logs", force: :cascade do |t|
      t.integer  "user_id",    limit: 4
      t.integer  "target_id",  limit: 4
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "invitations", force: :cascade do |t|
      t.integer  "sender_id",       limit: 4
      t.string   "recipient_email", limit: 255, null: false
      t.string   "first_name",      limit: 255
      t.string   "last_name",       limit: 255
      t.string   "token",           limit: 255
      t.datetime "sent_at"
      t.string   "roles",           limit: 255
      t.integer  "recipient_id",    limit: 4
      t.datetime "accepted_at"
      t.integer  "invitable_id",    limit: 4
      t.string   "invitable_type",  limit: 255
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "locations", force: :cascade do |t|
      t.decimal  "latitude",               precision: 15, scale: 10
      t.decimal  "longitude",              precision: 15, scale: 10
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "name",       limit: 255
    end

    create_table "locations_locatables", force: :cascade do |t|
      t.integer "location_id",    limit: 4
      t.integer "locatable_id",   limit: 4
      t.string  "locatable_type", limit: 191
    end

    add_index "locations_locatables", ["locatable_id", "locatable_type"], name: "index_locations_locatables_on_locatable_id_and_locatable_type", unique: true, using: :btree
    add_index "locations_locatables", ["location_id"], name: "index_locations_locatables_on_location_id", using: :btree

    create_table "mentions", force: :cascade do |t|
      t.integer  "user_id",          limit: 4
      t.integer  "mentionable_id",   limit: 4
      t.string   "mentionable_type", limit: 191
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "mentions", ["mentionable_id", "mentionable_type"], name: "index_mentions_on_mentionable_id_and_mentionable_type", using: :btree
    add_index "mentions", ["user_id"], name: "index_mentions_on_user_id", using: :btree

    create_table "mobile_invitations", force: :cascade do |t|
      t.string   "recipient_email", limit: 255, null: false
      t.string   "token",           limit: 255
      t.datetime "sent_at"
      t.integer  "recipient_id",    limit: 4
      t.datetime "accepted_at"
      t.string   "first_name",      limit: 255
      t.string   "last_name",       limit: 255
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "notifications", force: :cascade do |t|
      t.integer  "notifiable_id",         limit: 4
      t.string   "notifiable_type",       limit: 191
      t.integer  "group_id",              limit: 4
      t.integer  "user_id",               limit: 4
      t.string   "notification_type",     limit: 255
      t.boolean  "unseen",                limit: 1,   default: true
      t.datetime "accepted_at"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "widget_deep_link_id",   limit: 4
      t.integer  "card_id",               limit: 4
      t.string   "widget_deep_link_type", limit: 255
      t.integer  "app_deep_link_id",      limit: 4
      t.string   "app_deep_link_type",    limit: 255
    end

    add_index "notifications", ["group_id", "user_id"], name: "index_notifications_on_group_id_and_user_id", using: :btree
    add_index "notifications", ["notifiable_id", "notifiable_type"], name: "index_notifications_on_notifiable_id_and_notifiable_type", using: :btree
    add_index "notifications", ["user_id", "group_id"], name: "index_notifications_on_user_id_and_group_id", using: :btree

    create_table "notifications_causal_users", force: :cascade do |t|
      t.integer "notification_id", limit: 4,                 null: false
      t.integer "user_id",         limit: 4,                 null: false
      t.boolean "anonymous",       limit: 1, default: false
    end

    add_index "notifications_causal_users", ["notification_id"], name: "index_notifications_causal_users_on_notification_id", using: :btree

    create_table "oauth_access_grants", force: :cascade do |t|
      t.integer  "resource_owner_id", limit: 4,     null: false
      t.integer  "application_id",    limit: 4,     null: false
      t.string   "token",             limit: 191,   null: false
      t.integer  "expires_in",        limit: 4,     null: false
      t.text     "redirect_uri",      limit: 65535, null: false
      t.datetime "created_at",                      null: false
      t.datetime "revoked_at"
      t.string   "scopes",            limit: 255
    end

    add_index "oauth_access_grants", ["token"], name: "index_oauth_access_grants_on_token", unique: true, using: :btree

    create_table "oauth_access_tokens", force: :cascade do |t|
      t.integer  "resource_owner_id", limit: 4
      t.integer  "application_id",    limit: 4
      t.string   "token",             limit: 191, null: false
      t.string   "refresh_token",     limit: 191
      t.integer  "expires_in",        limit: 4
      t.datetime "revoked_at"
      t.datetime "created_at",                    null: false
      t.string   "scopes",            limit: 255
    end

    add_index "oauth_access_tokens", ["refresh_token"], name: "index_oauth_access_tokens_on_refresh_token", unique: true, using: :btree
    add_index "oauth_access_tokens", ["resource_owner_id"], name: "index_oauth_access_tokens_on_resource_owner_id", using: :btree
    add_index "oauth_access_tokens", ["token"], name: "index_oauth_access_tokens_on_token", unique: true, using: :btree

    create_table "organizations", force: :cascade do |t|
      t.string   "name",                      limit: 255
      t.string   "description",               limit: 1024
      t.string   "slug",                      limit: 191
      t.string   "image_file_name",           limit: 255
      t.string   "image_content_type",        limit: 255
      t.integer  "image_file_size",           limit: 4
      t.datetime "image_updated_at"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "mobile_image_file_name",    limit: 255
      t.string   "mobile_image_content_type", limit: 255
      t.integer  "mobile_image_file_size",    limit: 4
      t.datetime "mobile_image_updated_at"
      t.string   "host_name",                 limit: 191
    end

    add_index "organizations", ["host_name"], name: "index_organizations_on_host_name", using: :btree
    add_index "organizations", ["slug"], name: "index_organizations_on_slug", using: :btree

    create_table "organizations_users", force: :cascade do |t|
      t.integer  "organization_id", limit: 4
      t.integer  "user_id",         limit: 4
      t.string   "as_type",         limit: 255
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "post_reads", force: :cascade do |t|
      t.integer  "user_id",    limit: 4, null: false
      t.integer  "post_id",    limit: 4, null: false
      t.integer  "group_id",   limit: 4
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "post_reads", ["group_id", "user_id", "post_id"], name: "index_post_reads_on_group_id_and_user_id_and_post_id", unique: true, using: :btree
    add_index "post_reads", ["user_id", "post_id"], name: "index_post_reads_on_user_id_and_post_id", unique: true, using: :btree

    create_table "posts", force: :cascade do |t|
      t.string   "title",          limit: 255
      t.text     "message",        limit: 65535
      t.string   "type",           limit: 255
      t.integer  "user_id",        limit: 4,                     null: false
      t.integer  "group_id",       limit: 4,                     null: false
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "votes_count",    limit: 4,     default: 0
      t.integer  "resource_id",    limit: 4
      t.integer  "comments_count", limit: 4,     default: 0
      t.boolean  "hidden",         limit: 1,     default: false
      t.boolean  "anonymous",      limit: 1,     default: false
      t.boolean  "pinned",         limit: 1,     default: false
      t.string   "context_id",     limit: 255
      t.string   "context_label",  limit: 255
      t.boolean  "is_mobile",      limit: 1,     default: false
    end

    add_index "posts", ["group_id"], name: "index_posts_on_group_id", using: :btree
    add_index "posts", ["user_id"], name: "index_posts_on_user_id", using: :btree

    create_table "promotions", force: :cascade do |t|
      t.string   "url",             limit: 255
      t.integer  "promotable_id",   limit: 4
      t.string   "promotable_type", limit: 191
      t.datetime "created_at",                    null: false
      t.datetime "updated_at",                    null: false
      t.datetime "deleted_at"
      t.text     "data",            limit: 65535
    end

    add_index "promotions", ["deleted_at"], name: "index_promotions_on_deleted_at", using: :btree
    add_index "promotions", ["promotable_type", "promotable_id"], name: "index_promotions_on_promotable_type_and_promotable_id", using: :btree

    create_table "pubnub_channels", force: :cascade do |t|
      t.string   "name",       limit: 255
      t.integer  "item_id",    limit: 4
      t.string   "item_type",  limit: 191
      t.string   "as_type",    limit: 191
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "pubnub_channels", ["item_id", "item_type", "as_type"], name: "index_pubnub_channels_on_item_id_and_item_type_and_as_type", unique: true, using: :btree

    create_table "quiz_question_attempts", force: :cascade do |t|
      t.integer  "user_id",            limit: 4
      t.integer  "quiz_question_id",   limit: 4
      t.string   "quiz_question_type", limit: 191
      t.text     "response",           limit: 65535
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "quiz_question_attempts", ["quiz_question_id", "quiz_question_type"], name: "index_question_attempts_on_quiz_question", using: :btree
    add_index "quiz_question_attempts", ["user_id", "quiz_question_id", "quiz_question_type"], name: "index_question_attempts_on_user_quiz_question", using: :btree
    add_index "quiz_question_attempts", ["user_id"], name: "index_quiz_question_attempts_on_user_id", using: :btree

    create_table "quiz_question_options", force: :cascade do |t|
      t.integer  "quiz_question_id",   limit: 4
      t.string   "quiz_question_type", limit: 191
      t.text     "label",              limit: 65535
      t.boolean  "is_correct",         limit: 1
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "quiz_question_options", ["quiz_question_id", "quiz_question_type"], name: "index_options_on_quiz_question", using: :btree

    create_table "quiz_question_stats", force: :cascade do |t|
      t.integer "quiz_question_id",   limit: 4
      t.string  "quiz_question_type", limit: 191
      t.text    "stats",              limit: 65535
      t.integer "attempt_count",      limit: 4,     default: 0
      t.integer "lock_version",       limit: 4,     default: 0
    end

    add_index "quiz_question_stats", ["quiz_question_id", "quiz_question_type"], name: "index_stats_on_quiz_question", using: :btree

    create_table "recordings", force: :cascade do |t|
      t.string   "bucket",                       limit: 255
      t.string   "location",                     limit: 1024
      t.string   "key",                          limit: 255
      t.string   "checksum",                     limit: 255
      t.integer  "video_stream_id",              limit: 4
      t.integer  "sequence_number",              limit: 4
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "transcoding_status",           limit: 255,  default: "pending"
      t.string   "hls_location",                 limit: 1024
      t.string   "mp4_location",                 limit: 1024
      t.string   "last_aws_transcoding_message", limit: 4096
      t.string   "last_aws_transcoding_job_id",  limit: 255
      t.string   "source",                       limit: 255,  default: "client"
    end

    create_table "releases", force: :cascade do |t|
      t.string   "state",       limit: 255,   default: "inactive"
      t.string   "platform",    limit: 255
      t.string   "form_factor", limit: 255,   default: "phone"
      t.string   "version",     limit: 255
      t.string   "notes",       limit: 10240
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "build",       limit: 4
    end

    create_table "reports", force: :cascade do |t|
      t.integer  "user_id",         limit: 4,                  null: false
      t.integer  "reportable_id",   limit: 4
      t.string   "reportable_type", limit: 191
      t.string   "type",            limit: 255
      t.datetime "created_at"
      t.datetime "updated_at"
      t.boolean  "validity",        limit: 1,   default: true
    end

    add_index "reports", ["reportable_id", "reportable_type", "user_id"], name: "index_reports_on_reportable_id_and_reportable_type_and_user_id", using: :btree

    create_table "resources", force: :cascade do |t|
      t.text     "url",         limit: 65535, null: false
      t.text     "description", limit: 65535
      t.string   "title",       limit: 255
      t.string   "type",        limit: 255
      t.string   "site_name",   limit: 255
      t.text     "image_url",   limit: 65535
      t.text     "video_url",   limit: 65535
      t.datetime "created_at"
      t.datetime "updated_at"
      t.text     "embed_html",  limit: 65535
    end

    create_table "scores", force: :cascade do |t|
      t.integer "user_id",      limit: 4,             null: false
      t.integer "group_id",     limit: 4,             null: false
      t.integer "score",        limit: 4, default: 0
      t.integer "stars",        limit: 4, default: 0
      t.integer "lock_version", limit: 4
    end

    add_index "scores", ["user_id", "group_id"], name: "index_scores_on_user_id_and_group_id", unique: true, using: :btree

    create_table "shortened_urls", force: :cascade do |t|
      t.integer  "owner_id",   limit: 4
      t.string   "owner_type", limit: 20
      t.string   "url",        limit: 191,             null: false
      t.string   "unique_key", limit: 10,              null: false
      t.integer  "use_count",  limit: 4,   default: 0, null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "shortened_urls", ["owner_id", "owner_type"], name: "index_shortened_urls_on_owner_id_and_owner_type", using: :btree
    add_index "shortened_urls", ["unique_key"], name: "index_shortened_urls_on_unique_key", unique: true, using: :btree
    add_index "shortened_urls", ["url"], name: "index_shortened_urls_on_url", using: :btree

    create_table "solicitation_cards", force: :cascade do |t|
      t.integer  "solicitable_id",   limit: 4
      t.string   "solicitable_type", limit: 255
      t.string   "template_type",    limit: 255
      t.text     "contents",         limit: 65535
      t.string   "type",             limit: 255
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "solicitation_results", force: :cascade do |t|
      t.integer  "user_id",          limit: 4
      t.integer  "solicitable_id",   limit: 4
      t.string   "solicitable_type", limit: 191
      t.string   "response",         limit: 255
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "solicitation_results", ["solicitable_id", "solicitable_type"], name: "solicitation_result_on_solicitable", using: :btree
    add_index "solicitation_results", ["user_id"], name: "index_solicitation_results_on_user_id", using: :btree

    create_table "streams", force: :cascade do |t|
      t.integer  "user_id",    limit: 4
      t.integer  "group_id",   limit: 4
      t.string   "action",     limit: 255
      t.integer  "item_id",    limit: 4
      t.string   "item_type",  limit: 255
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "streams", ["group_id", "created_at"], name: "index_streams_on_group_id_and_created_at", using: :btree

    create_table "taggings", force: :cascade do |t|
      t.integer  "tag_id",        limit: 4,   null: false
      t.integer  "taggable_id",   limit: 4,   null: false
      t.string   "taggable_type", limit: 191, null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "taggings", ["taggable_type", "taggable_id"], name: "index_taggings_on_taggable_type_and_taggable_id", using: :btree

    create_table "tags", force: :cascade do |t|
      t.string   "name",       limit: 191
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

    create_table "thumbnails", force: :cascade do |t|
      t.integer  "recording_id",    limit: 4
      t.string   "uri",             limit: 255
      t.integer  "sequence_number", limit: 4
      t.boolean  "is_default",      limit: 1,   default: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "topics", force: :cascade do |t|
      t.string   "name",                 limit: 255
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "picture_file_name",    limit: 255
      t.string   "picture_content_type", limit: 255
      t.integer  "picture_file_size",    limit: 4
      t.datetime "picture_updated_at"
    end

    create_table "topics_cards", force: :cascade do |t|
      t.integer  "topic_id",   limit: 4, null: false
      t.integer  "card_id",    limit: 4, null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "topics_cards", ["card_id"], name: "index_topics_cards_on_card_id", using: :btree
    add_index "topics_cards", ["topic_id"], name: "index_topics_cards_on_topic_id", using: :btree

    create_table "topics_groups", force: :cascade do |t|
      t.integer  "topic_id",   limit: 4, null: false
      t.integer  "group_id",   limit: 4, null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "topics_groups", ["group_id", "topic_id"], name: "index_topics_groups_on_group_id_and_topic_id", unique: true, using: :btree
    add_index "topics_groups", ["topic_id", "group_id"], name: "index_topics_groups_on_topic_id_and_group_id", unique: true, using: :btree

    create_table "topics_users", id: false, force: :cascade do |t|
      t.integer  "user_id",    limit: 4, null: false
      t.integer  "topic_id",   limit: 4, null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "topics_users", ["topic_id", "user_id"], name: "index_topics_users_on_topic_id_and_user_id", unique: true, using: :btree
    add_index "topics_users", ["user_id", "topic_id"], name: "index_topics_users_on_user_id_and_topic_id", unique: true, using: :btree

    create_table "uploads", force: :cascade do |t|
      t.integer  "sender_id",         limit: 4
      t.text     "output_url",        limit: 65535
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "file_file_name",    limit: 255
      t.string   "file_content_type", limit: 255
      t.integer  "file_file_size",    limit: 4
      t.datetime "file_updated_at"
      t.string   "type",              limit: 255
      t.integer  "group_id",          limit: 4
    end

    create_table "user_connections", force: :cascade do |t|
      t.integer "user_id",       limit: 4
      t.integer "connection_id", limit: 4
      t.string  "network",       limit: 191
    end

    add_index "user_connections", ["connection_id", "user_id"], name: "index_user_connections_on_connection_id_and_user_id", using: :btree
    add_index "user_connections", ["user_id", "connection_id", "network"], name: "index_user_connections_on_user_id_and_connection_id_and_network", unique: true, using: :btree

    create_table "user_feed_caches", force: :cascade do |t|
      t.integer  "user_id",                       limit: 4,     null: false
      t.text     "queue_item_ids",                limit: 65535
      t.string   "feed_request_id",               limit: 191,   null: false
      t.datetime "created_at"
      t.datetime "updated_at"
      t.boolean  "channel_recommendations_shown", limit: 1
    end

    add_index "user_feed_caches", ["user_id", "feed_request_id"], name: "index_user_feed_caches_on_user_id_and_feed_request_id", unique: true, using: :btree

    create_table "user_notification_reads", force: :cascade do |t|
      t.integer  "user_id",    limit: 4, null: false
      t.datetime "seen_at"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "user_notification_reads", ["user_id"], name: "index_user_notification_reads_on_user_id", unique: true, using: :btree

    create_table "user_preferences", force: :cascade do |t|
      t.integer  "user_id",             limit: 4
      t.string   "key",                 limit: 191
      t.string   "value",               limit: 191
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "preferenceable_id",   limit: 4
      t.string   "preferenceable_type", limit: 191
    end

    add_index "user_preferences", ["user_id", "key", "preferenceable_type", "preferenceable_id"], name: "user_preference_by_user", unique: true, using: :btree
    add_index "user_preferences", ["user_id", "key", "value"], name: "index_user_preferences_on_user_id_and_key_and_value", using: :btree

    create_table "user_signatures", force: :cascade do |t|
      t.integer "user_id",   limit: 4,     null: false
      t.text    "value",     limit: 65535
      t.string  "user_role", limit: 191
      t.string  "name",      limit: 191
    end

    add_index "user_signatures", ["name"], name: "index_user_signatures_on_name", using: :btree
    add_index "user_signatures", ["user_id", "user_role"], name: "index_user_signatures_on_user_id_and_user_role", using: :btree

    create_table "users", force: :cascade do |t|
      t.string   "email",                   limit: 191
      t.string   "first_name",              limit: 255
      t.string   "last_name",               limit: 255
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "encrypted_password",      limit: 255,   default: "",    null: false
      t.string   "reset_password_token",    limit: 191
      t.datetime "reset_password_sent_at"
      t.datetime "remember_created_at"
      t.integer  "sign_in_count",           limit: 4,     default: 0,     null: false
      t.datetime "current_sign_in_at"
      t.datetime "last_sign_in_at"
      t.string   "current_sign_in_ip",      limit: 255
      t.string   "last_sign_in_ip",         limit: 255
      t.string   "picture_url",             limit: 255
      t.string   "anonimage_file_name",     limit: 255
      t.string   "anonimage_content_type",  limit: 255
      t.integer  "anonimage_file_size",     limit: 4
      t.datetime "anonimage_updated_at"
      t.string   "avatar_file_name",        limit: 255
      t.string   "avatar_content_type",     limit: 255
      t.integer  "avatar_file_size",        limit: 4
      t.datetime "avatar_updated_at"
      t.integer  "location_id",             limit: 4
      t.string   "confirmation_token",      limit: 255
      t.datetime "confirmed_at"
      t.datetime "confirmation_sent_at"
      t.string   "unconfirmed_email",       limit: 255
      t.string   "handle",                  limit: 191
      t.boolean  "is_brand",                limit: 1,     default: false
      t.text     "bio",                     limit: 65535
      t.string   "coverimage_file_name",    limit: 255
      t.string   "coverimage_content_type", limit: 255
      t.integer  "coverimage_file_size",    limit: 4
      t.datetime "coverimage_updated_at"
      t.boolean  "password_reset_required", limit: 1,     default: false
      t.boolean  "is_complete",             limit: 1,     default: false
      t.string   "configs",                 limit: 2048
      t.integer  "organization_id",         limit: 4
      t.string   "organization_role",       limit: 191
    end

    add_index "users", ["email", "organization_id"], name: "index_users_on_email_and_organization_id", unique: true, using: :btree
    add_index "users", ["email"], name: "index_users_on_email", using: :btree
    add_index "users", ["handle"], name: "index_users_on_handle", using: :btree
    add_index "users", ["organization_id", "handle"], name: "index_users_on_organization_id_and_handle", using: :btree
    add_index "users", ["organization_id"], name: "index_users_on_organization_id", using: :btree
    add_index "users", ["organization_role"], name: "index_users_on_organization_role", using: :btree
    add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

    create_table "users_solicitation_cards", force: :cascade do |t|
      t.integer  "user_id",              limit: 4
      t.integer  "solicitation_card_id", limit: 4
      t.string   "status",               limit: 255
      t.datetime "created_at"
      t.datetime "updated_at"
      t.datetime "last_shown"
    end

    add_index "users_solicitation_cards", ["solicitation_card_id"], name: "index_users_solicitation_cards_on_solicitation_card_id", using: :btree
    add_index "users_solicitation_cards", ["user_id"], name: "index_users_solicitation_cards_on_user_id", using: :btree

    create_table "video_streams", force: :cascade do |t|
      t.integer  "creator_id",     limit: 4,               null: false
      t.integer  "group_id",       limit: 4
      t.datetime "start_time"
      t.string   "name",           limit: 255
      t.string   "uuid",           limit: 191,             null: false
      t.string   "status",         limit: 191
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "publish_url",    limit: 255
      t.string   "playback_url",   limit: 255
      t.string   "password",       limit: 255
      t.string   "x_id",           limit: 255
      t.string   "type",           limit: 255
      t.integer  "comments_count", limit: 4,   default: 0
      t.integer  "votes_count",    limit: 4,   default: 0
      t.string   "slug",           limit: 191
      t.datetime "last_stream_at"
    end

    add_index "video_streams", ["creator_id", "status"], name: "index_video_streams_on_creator_id_and_status", using: :btree
    add_index "video_streams", ["group_id", "status"], name: "index_video_streams_on_group_id_and_status", using: :btree
    add_index "video_streams", ["last_stream_at"], name: "index_video_streams_on_last_stream_at", using: :btree
    add_index "video_streams", ["slug"], name: "index_video_streams_on_slug", unique: true, using: :btree
    add_index "video_streams", ["uuid"], name: "index_video_streams_on_uuid", unique: true, using: :btree

    create_table "video_streams_users", force: :cascade do |t|
      t.integer  "video_stream_id", limit: 4, null: false
      t.integer  "user_id",         limit: 4, null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "video_streams_users", ["user_id", "video_stream_id"], name: "index_video_streams_users_on_user_id_and_video_stream_id", using: :btree
    add_index "video_streams_users", ["video_stream_id", "user_id"], name: "index_video_streams_users_on_video_stream_id_and_user_id", using: :btree

    create_table "votes", force: :cascade do |t|
      t.integer  "user_id",      limit: 4,   null: false
      t.integer  "votable_id",   limit: 4,   null: false
      t.string   "votable_type", limit: 191, null: false
      t.integer  "weight",       limit: 4
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "votes", ["votable_id", "votable_type", "user_id"], name: "index_votes_on_votable_id_and_votable_type_and_user_id", using: :btree

  end
end

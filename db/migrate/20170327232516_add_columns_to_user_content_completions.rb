class AddColumnsToUserContentCompletions < ActiveRecord::Migration
  def change
    add_column :user_content_completions, :started_at, :datetime
    add_column :user_content_completions, :completed_at, :datetime
  end
end

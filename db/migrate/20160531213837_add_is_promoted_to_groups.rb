class AddIsPromotedToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :is_promoted, :boolean, default: false
  end
end

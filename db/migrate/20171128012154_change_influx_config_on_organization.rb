class ChangeInfluxConfigOnOrganization < ActiveRecord::Migration
  def change
    change_column_default :organizations, :enable_influx_event_recorder, true
  end
end

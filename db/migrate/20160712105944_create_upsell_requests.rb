class CreateUpsellRequests < ActiveRecord::Migration
  def change
    create_table :upsell_requests do |t|
      t.references :user, index: true, foreign_key: true
      t.string :type
      t.text :fields
      t.boolean :approved, default: false
    end
  end
end

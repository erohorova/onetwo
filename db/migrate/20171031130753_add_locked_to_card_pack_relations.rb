class AddLockedToCardPackRelations < ActiveRecord::Migration
  def change
    add_column :card_pack_relations, :locked, :boolean
  end
end

class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.integer :reviewer_id, index: true, foreign_key: true, null: false
      t.integer :card_id, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end

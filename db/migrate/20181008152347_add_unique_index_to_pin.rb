class AddUniqueIndexToPin < ActiveRecord::Migration
  def change
  	add_index :pins, [:pinnable_id, :object_id], unique: true
  end
end
class CreateUserTeamAssignments < ActiveRecord::Migration
  def change
    create_table :user_team_assignments do |t|
      t.references :user, index: true
      t.references :team, index: true
      t.references :card, index: true

      t.timestamps null: false
    end
  end
end

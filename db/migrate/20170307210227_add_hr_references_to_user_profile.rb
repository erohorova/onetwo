class AddHrReferencesToUserProfile < ActiveRecord::Migration
  def change
    add_column :user_profiles, :hr_job_role_id, :integer
    add_column :user_profiles, :hr_organization_id, :integer
    add_column :user_profiles, :hr_location_id, :integer
    add_column :user_profiles, :hr_competency_id, :integer
    add_column :user_profiles, :hr_domain_id, :integer
  end
end

class AddAverageRatingToCardMetadata < ActiveRecord::Migration
  def change
    add_column :card_metadata, :average_rating, :decimal, precision: 3, scale: 2, null: false, default: 0.0
  end
end

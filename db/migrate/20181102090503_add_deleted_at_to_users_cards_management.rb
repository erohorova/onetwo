class AddDeletedAtToUsersCardsManagement < ActiveRecord::Migration
  def change
    add_column :users_cards_managements, :deleted_at, :datetime
    add_index :users_cards_managements, :deleted_at
  end
end

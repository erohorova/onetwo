class ChangeSizeInCardsCardType < ActiveRecord::Migration
  def change
    change_column :cards, :card_type, :string, limit: 20
  end
end

class MoveAssignorToTeamAssignment < ActiveRecord::Migration
  def change
    add_column :team_assignments, :assignor_id, :integer
    remove_column :assignments, :assignor_id, :integer
  end
end

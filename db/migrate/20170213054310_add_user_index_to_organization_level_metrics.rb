class AddUserIndexToOrganizationLevelMetrics < ActiveRecord::Migration
  def change
    add_index :organization_level_metrics, :user_id
  end
end

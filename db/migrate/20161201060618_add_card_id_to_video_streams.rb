class AddCardIdToVideoStreams < ActiveRecord::Migration
  def change
    add_column :video_streams, :card_id, :integer
    add_index :video_streams, :card_id
  end
end

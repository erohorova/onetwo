class CreateGoalEvents < ActiveRecord::Migration
  def change
    create_table :goal_events do |t|
      t.references :goal, index: true, foreign_key: true
      t.string :data

      t.timestamps null: false
    end
  end
end

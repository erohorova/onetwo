class AddCallbackUrlToIntegrations < ActiveRecord::Migration
  def change
    add_column :integrations, :callback_url, :string
  end
end

class CreateUsersMentors < ActiveRecord::Migration
  def change
    create_table :users_mentors do |t|
      t.references :user, index: true, foreign_key: true
      t.string :email
      t.integer :mentor_id, index: true
      t.timestamps null: false
    end
  end
end

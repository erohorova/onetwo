class CreatePersonalizedEmails < ActiveRecord::Migration
  def change
    create_table :personalized_emails do |t|
      t.references :user, index: true, foreign_key: true
      t.references :organization, index: true, foreign_key: true
      t.string :subject
      t.string :contribution_indicator_subject
      t.string :personal_performance_subject
      t.string :activity_indicator_subject
      t.text :user_performance
      t.text :contributions
      t.text :interesting_cards_for_user
      t.timestamp :from_time

      t.timestamps null: false
    end
  end
end

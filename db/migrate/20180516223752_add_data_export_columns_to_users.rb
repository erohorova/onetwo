class AddDataExportColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :data_export_status, :integer
  end
end

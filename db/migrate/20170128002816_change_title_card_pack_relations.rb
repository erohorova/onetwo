class ChangeTitleCardPackRelations < ActiveRecord::Migration
  def change
    change_column(:card_pack_relations, :title, :text)
  end
end

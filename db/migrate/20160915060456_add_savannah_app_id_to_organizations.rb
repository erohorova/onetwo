class AddSavannahAppIdToOrganizations < ActiveRecord::Migration
  #now organization will directly mapped to savannah app
  def change
    add_column :organizations, :savannah_app_id, :integer
    add_column :organizations, :redirect_uri, :string
  end
end

class FixColumnTypeForGoalEvents < ActiveRecord::Migration
  def change
    change_column :goal_events, :data, :text
  end
end

class AddEnableReportingFlagToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :enable_analytics_reporting, :boolean, default: false
  end
end

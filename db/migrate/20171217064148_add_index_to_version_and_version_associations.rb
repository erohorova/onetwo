class AddIndexToVersionAndVersionAssociations < ActiveRecord::Migration
  def self.up
    add_index :versions, %i(item_type item_id) unless index_exists?(:versions, [:item_type, :item_id])
    add_index :version_associations, [:version_id] unless index_exists?(:version_associations, [:version_id])

    unless index_exists?(:version_associations, [:foreign_key_name, :foreign_key_id],  name: "index_version_associations_on_foreign_key")
      add_index :version_associations,
        %i(foreign_key_name foreign_key_id),
        name: "index_version_associations_on_foreign_key"
    end
    add_index :versions, [:transaction_id] unless index_exists?(:versions, [:transaction_id])

  end

  def self.down
    remove_index :versions, %i(item_type item_id)
    remove_index :version_associations, [:version_id]
    remove_index :version_associations,
      name: "index_version_associations_on_foreign_key"
    remove_index :versions, [:transaction_id]
  end
end


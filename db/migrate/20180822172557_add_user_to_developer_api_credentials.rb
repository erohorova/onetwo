class AddUserToDeveloperApiCredentials < ActiveRecord::Migration
  def change
    add_column :developer_api_credentials, :user_id, :integer
  end
end

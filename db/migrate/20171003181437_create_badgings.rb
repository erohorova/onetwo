class CreateBadgings < ActiveRecord::Migration
  def change
    create_table :badgings do |t|
      t.string :title
      t.integer :badge_id
      t.integer :badgeable_id
      t.string :type

      t.timestamps null: false
    end
  end
end

class AddCompanyToProfile < ActiveRecord::Migration
  def change
    add_column :user_profiles, :company, :string, limit: 30
    #We won't use Job_title existing field as it can be edited by user. Job_roles will be predefine set from sociative.
    add_column :user_profiles, :job_role, :string, limit: 50
  end
end

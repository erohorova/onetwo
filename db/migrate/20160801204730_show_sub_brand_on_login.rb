class ShowSubBrandOnLogin < ActiveRecord::Migration
  def change
    add_column :organizations, :show_sub_brand_on_login, :boolean, default: true
  end
end

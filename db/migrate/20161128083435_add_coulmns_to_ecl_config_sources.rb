class AddCoulmnsToEclConfigSources < ActiveRecord::Migration
  def change
    add_column :ecl_config_sources, :auth_details, :text
    add_column :ecl_config_sources, :ecl_provider_id, :integer
    add_column :ecl_config_sources, :ecl_content_source_id, :integer
  end
end

class AddJobRoleRefToUsers < ActiveRecord::Migration
  def change
    add_reference :users, :job_role, index: true, foreign_key: true
  end
end

class AddIndexToCardsResourceId < ActiveRecord::Migration
  def change
    add_index :cards, :resource_id
  end
end

class CreateJourneyPackRelations < ActiveRecord::Migration
  def change
    create_table :journey_pack_relations do |t|
      t.integer  :cover_id, index: true
      t.integer  :from_id, index: true
      t.integer  :to_id, index: true
      t.datetime :start_date
      t.boolean :deleted, default: false

      t.timestamps null: false
    end
    add_index :journey_pack_relations, [:cover_id, :from_id]
    add_index :journey_pack_relations, [:cover_id, :to_id]
  end
end

class AddPhoneNumberToOrganizationForms < ActiveRecord::Migration
  def change
    add_column :organization_forms, :phone_number, :string
  end
end

class AddIndicesToCards < ActiveRecord::Migration
  def up
    execute("ALTER TABLE cards ADD INDEX index_cards_on_org_id_and_card_type (organization_id, card_type), ALGORITHM=INPLACE, LOCK=NONE;")
  end

  def down
    execute("DROP INDEX index_cards_on_org_id_and_card_type on cards;")
  end
end

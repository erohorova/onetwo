class AddColumnToEmailTemplates < ActiveRecord::Migration
  def change
    # add column which would content html in JSON
    add_column :email_templates, :design, :text, null: false
  end
end

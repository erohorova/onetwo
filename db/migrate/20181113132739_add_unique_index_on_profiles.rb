class AddUniqueIndexOnProfiles < ActiveRecord::Migration
  def change
    add_index(:profiles, [:user_id, :organization_id], unique: true, name: 'profiles_constraint_by_user_org')
  end
end

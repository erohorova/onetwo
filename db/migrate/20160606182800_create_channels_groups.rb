class CreateChannelsGroups < ActiveRecord::Migration
  def change
    create_table :channels_groups do |t|
      t.integer :channel_id
      t.integer :group_id
    end

    add_index :channels_groups, [:channel_id, :group_id], unique: true
    add_index :channels_groups, [:group_id, :channel_id], unique: true
  end
end

class AddPronouncements < ActiveRecord::Migration
  def change
    create_table :pronouncements do |t|
      t.text :label, null: false
      t.integer :announceable_id, null: false
      t.string :announceable_type, null: false
      t.datetime :start_date, null: false
      t.datetime :end_date
      t.boolean :is_active, default: true
      t.timestamps null: false
    end
  end
end

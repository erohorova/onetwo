class CreateInvitationFile < ActiveRecord::Migration
  def change
    create_table :invitation_files do |t|
      t.integer  "sender_id",         limit: 4,                         null: false
      t.text     "data",              limit: 4294967295
      t.string   "roles",             limit: 255
      t.integer  "invitable_id",      limit: 4
      t.string   "invitable_type",    limit: 191
      t.datetime "created_at"
      t.datetime "updated_at"
      t.boolean  "send_invite_email", limit: 1,          default: true
    end
    add_index "invitation_files", ["invitable_id", "invitable_type"]
  end
end

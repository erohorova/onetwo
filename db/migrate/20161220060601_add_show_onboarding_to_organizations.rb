class AddShowOnboardingToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :show_onboarding, :boolean, default: true
  end
end

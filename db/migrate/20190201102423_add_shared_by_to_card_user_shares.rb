class AddSharedByToCardUserShares < ActiveRecord::Migration
  def change
    add_column :card_user_shares, :shared_by_id, :integer, index: true
    add_index :card_user_shares, :shared_by_id
  end
end

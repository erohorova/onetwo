class AddIdentifierToBadges < ActiveRecord::Migration
  def change
    add_column :user_badges, :identifier, :string, limit: 10
  end
end

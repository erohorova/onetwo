class CreateWalletBalances < ActiveRecord::Migration
  def change
    create_table :wallet_balances do |t|
      t.decimal :amount, precision: 8, scale: 2
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

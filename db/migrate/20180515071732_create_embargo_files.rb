class CreateEmbargoFiles < ActiveRecord::Migration
  def change
    create_table :embargo_files do |t|
      t.references :user, index: true, null: false
      t.references :organization, index: true, null: false
      t.text :data, limit: 4294967295
      t.timestamps null: false
    end
  end
end

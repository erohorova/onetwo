class ChangeAbbreviationLimit < ActiveRecord::Migration
  def change
    # 1. SELECT COUNT(*) FROM `custom_fields` WHERE (length(abbreviation) > 60
    #                                         OR length(display_name) > 60)
    #    => 0
    # 2. SELECT COUNT(*) FROM `custom_fields`
    #    => 270
    reversible do |migration|
      migration.up do
        CustomField.find_each do |custom_field|
          old_abbr = custom_field.abbreviation
          old_display_name = custom_field.display_name
          next if old_abbr.size <= 64 && old_display_name.size <= 64
          custom_field.update_attributes(
            abbreviation: old_abbr.truncate(64, omission: ''),
            display_name: old_display_name.truncate(64, omission: '')
          )
        end
        change_column :custom_fields, :abbreviation, :string, limit: 64
        change_column :custom_fields, :display_name, :string, limit: 64
      end
      migration.down do
        change_column :custom_fields, :abbreviation, :string
        change_column :custom_fields, :display_name, :string
      end
    end
  end
end

class CreateAmplitudeExports < ActiveRecord::Migration
  def change
    create_table :amplitude_exports do |t|
      t.string :date
      t.integer :hour
      t.string :file_name
      t.string :status
      t.integer :insert_count, default: 0
      t.integer :error_count, default: 0
      t.timestamps
    end
  end
end

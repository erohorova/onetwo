class AddTimeStampsToOrganizationForm < ActiveRecord::Migration
  def change
    add_timestamps(:organization_forms)
  end
end

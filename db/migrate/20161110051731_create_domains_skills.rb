class CreateDomainsSkills < ActiveRecord::Migration
  def change
    create_table :domains_skills do |t|
      t.references :domain, index: true, foreign_key: true
      t.references :skill, index: true

      t.timestamps null: false
    end
    add_foreign_key :domains_skills, :tags, column: :skill_id
  end
end

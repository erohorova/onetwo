class ChangeUpsellType < ActiveRecord::Migration
  def change
    remove_column :upsell_requests, :type
    add_column :upsell_requests, :upsell_type, :string
  end
end

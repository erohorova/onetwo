class CreateOrganizationPrices < ActiveRecord::Migration
  def change
    create_table :organization_prices do |t|
      t.references :organization, index: true, foreign_key: true
      t.string :currency, default: 'USD', limit: 10
      t.decimal :amount, precision: 8, scale: 2
      t.string :sku

      t.timestamps null: false
    end
  end
end

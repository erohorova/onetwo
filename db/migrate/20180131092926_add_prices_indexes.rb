class AddPricesIndexes < ActiveRecord::Migration
  def change
    add_index :prices, :card_id
  end
end

class AddStateAttrToCard < ActiveRecord::Migration
  def change
    add_column :cards, :state, :string, limit: 191
    add_index :cards, :state
  end
end

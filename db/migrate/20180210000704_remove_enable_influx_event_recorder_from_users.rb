class RemoveEnableInfluxEventRecorderFromUsers < ActiveRecord::Migration
  def change
    remove_column :organizations, :enable_influx_event_recorder
  end
end

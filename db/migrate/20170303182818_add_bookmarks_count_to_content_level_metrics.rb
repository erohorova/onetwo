class AddBookmarksCountToContentLevelMetrics < ActiveRecord::Migration
  def change
    add_column :content_level_metrics, :bookmarks_count, :integer, default: 0
  end
end

class CreateJobRolesOrganizations < ActiveRecord::Migration
  def change
    create_table :job_roles_organizations do |t|
      t.references :job_role, index: true, foreign_key: true
      t.references :organization, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

class CreateCommentReporting < ActiveRecord::Migration
  def change
    create_table :comment_reportings do |t|
      t.references :comment, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.string :reason
      t.datetime :deleted_at, index: true

      t.timestamps null: false
    end
  end
end

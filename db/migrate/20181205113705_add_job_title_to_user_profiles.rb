class AddJobTitleToUserProfiles < ActiveRecord::Migration
  def change
    add_column :user_profiles, :job_title, :string, limit: 80
  end
end

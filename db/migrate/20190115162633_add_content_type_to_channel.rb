class AddContentTypeToChannel < ActiveRecord::Migration
  def change
    add_column :channels, :content_type, :text
  end
end

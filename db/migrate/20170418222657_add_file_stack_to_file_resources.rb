class AddFileStackToFileResources < ActiveRecord::Migration
  def change
    add_column :file_resources, :filestack, :text
    add_column :file_resources, :filestack_handle, :string

    remove_column :file_resources, :source
  end
end

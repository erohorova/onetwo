class AddAdditionalColumnsToSchools < ActiveRecord::Migration
  def change
    add_column :schools, :institution_type, :string
    add_column :schools, :degree_name, :string
    add_column :schools, :institution_location, :string
  end
end

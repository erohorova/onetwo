class AddEnabledToRolePermission < ActiveRecord::Migration
  def change
    add_column :role_permissions, :enabled, :boolean, default: true
  end
end

class AddEclSourceDetailsToCards < ActiveRecord::Migration
  def change
    add_column :cards, :ecl_metadata, :text
  end
end

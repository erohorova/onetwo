class LowerCaseStateInAssignment < ActiveRecord::Migration
  def change
    change_column :assignments, :state, :string, default: 'assigned'
  end
end

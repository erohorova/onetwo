class DropEcrTables < ActiveRecord::Migration
  def change

    drop_table :cms_content_objects if ActiveRecord::Base.connection.table_exists? :cms_content_objects
    drop_table :cms_content_sources if ActiveRecord::Base.connection.table_exists? :cms_content_sources
    drop_table :cms_content_sources_industries if ActiveRecord::Base.connection.table_exists? :cms_content_sources_industries
    drop_table :cms_content_sources_organizations if ActiveRecord::Base.connection.table_exists? :cms_content_sources_organizations
    drop_table :cms_content_sources_organizations_channels if ActiveRecord::Base.connection.table_exists? :cms_content_sources_organizations_channels
    drop_table :cms_ecr_content_items if ActiveRecord::Base.connection.table_exists? :cms_ecr_content_items
    if column_exists? :cards, :cms_content_sources_organization_id
      remove_column(:cards, :cms_content_sources_organization_id)
    end
  end
end

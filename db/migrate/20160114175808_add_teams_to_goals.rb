class AddTeamsToGoals < ActiveRecord::Migration
  def change
    add_reference :goals, :team, index: true
    add_column :goals, :state, :string, limit: 191
    add_index :goals, [:state, :team_id]
    add_index :goals, [:user_id, :team_id]
  end
end

class AddEclSourceNameToCards < ActiveRecord::Migration
  def change
    add_column :cards, :ecl_source_name, :string
    add_index :cards, :ecl_source_name, length: {ecl_source_name: 125}
  end
end

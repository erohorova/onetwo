class ExtendInvitations < ActiveRecord::Migration
  def change
    add_column :invitations, :authenticate_recipient, :boolean, default: false
    add_column :invitations, :parameters, :text
  end
end

class CardTeamPermission < ActiveRecord::Migration
  def change
    create_table :card_team_permissions do |t|
      t.references :team, index: true, null: false, foreign_key: true
      t.references :card, index: true, null: false, foreign_key: true
      t.boolean :show, default: true
 
      t.timestamps
    end
  end
end

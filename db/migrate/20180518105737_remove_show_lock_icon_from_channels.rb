class RemoveShowLockIconFromChannels < ActiveRecord::Migration
  def change
    remove_column :channels, :show_lock_icon
  end
end

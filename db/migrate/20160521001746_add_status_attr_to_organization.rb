class AddStatusAttrToOrganization < ActiveRecord::Migration
  def change
    add_column :organizations, :status, :string, default: 'active'
  end
end

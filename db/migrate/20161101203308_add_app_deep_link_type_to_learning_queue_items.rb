class AddAppDeepLinkTypeToLearningQueueItems < ActiveRecord::Migration
  def change
    add_column :learning_queue_items, :deep_link_type, :string
  end
end

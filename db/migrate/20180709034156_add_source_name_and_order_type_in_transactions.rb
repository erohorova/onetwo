class AddSourceNameAndOrderTypeInTransactions < ActiveRecord::Migration
  def change
    add_column :transactions, :source_name, :string, limit: 50, index: true
    add_column :transactions, :order_type, :string, limit: 20, index: true
  end
end

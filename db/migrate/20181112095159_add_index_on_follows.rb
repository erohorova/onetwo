class AddIndexOnFollows < ActiveRecord::Migration
  # EU DB is having index: 'index_follows_on_followable_type_and_followable_id'.

  # US DB is having index: 'index_follows_on_followable_id_and_followable_type'.

  # As we are using 'index_follows_on_followable_id_and_followable_type' in our query,
  # we have to add this index for EU DB too.
  def change
    unless Follow.connection.index_exists? :follows, [:followable_id, :followable_type]
      add_index :follows, [:followable_id, :followable_type], name: 'index_follows_on_followable_id_and_followable_type'
    end
  end
end

class AddIndexesToTables < ActiveRecord::Migration
  def change
    add_index :recordings, :video_stream_id
    add_index :file_resources, :attachable_id
    add_index :global_influencers, :user_id
    add_index :organizations, :client_id
    add_index :channels, :group_id    
    add_index :cms_content_sources_organizations_channels, :channel_id
    add_index :mailer_configs, :client_id
    add_index :webex_configs, :client_id
    add_index :invitations, :invitable_id
  end
end

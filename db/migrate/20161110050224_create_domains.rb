class CreateDomains < ActiveRecord::Migration
  def change
    create_table :domains do |t|
      t.string :name, limit: 191
      t.references :category, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

class CreateCardSubscriptions < ActiveRecord::Migration
  def change
    create_table :card_subscriptions do |t|
      t.date :start_date, null: false
      t.date :end_date
      t.integer :organization_id, index: true, foreign_key: true, null: false
      t.integer :card_id, index: true, foreign_key: true, null: false
      t.integer :user_id, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end

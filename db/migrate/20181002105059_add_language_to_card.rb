class AddLanguageToCard < ActiveRecord::Migration
  def change
    add_column :cards, :language, :string
  end
end

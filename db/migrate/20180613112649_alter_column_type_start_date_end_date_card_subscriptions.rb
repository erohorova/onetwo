class AlterColumnTypeStartDateEndDateCardSubscriptions < ActiveRecord::Migration
  def change
    change_column :card_subscriptions, :start_date, :datetime
    change_column :card_subscriptions, :end_date, :datetime
  end
end

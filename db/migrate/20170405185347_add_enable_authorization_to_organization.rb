class AddEnableAuthorizationToOrganization < ActiveRecord::Migration
  def change
    add_column :organizations, :enable_role_based_authorization, :boolean, default: false
  end
end

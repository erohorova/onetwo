class RemoveColumnFromXapiActivity < ActiveRecord::Migration
  def change
    remove_column :xapi_activities, :actor_email
  end
end

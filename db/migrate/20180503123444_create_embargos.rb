class CreateEmbargos < ActiveRecord::Migration
  def change
    create_table :embargos do |t|
      t.string :value, limit: 191, null: false
      t.string :category, null: false
      t.references :user, index: true, null: false
      t.references :organization, index: true, null: false
      t.index [:organization_id, :value], unique: true
      t.timestamps null: false
    end
  end
end

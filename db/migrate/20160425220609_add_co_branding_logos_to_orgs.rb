class AddCoBrandingLogosToOrgs < ActiveRecord::Migration
  def change
    add_attachment :organizations, :co_branding_logo
    add_attachment :organizations, :co_branding_mobile_logo
  end
end

class CreateCardsRatings < ActiveRecord::Migration
  def change
    create_table :cards_ratings do |t|
      t.references :card, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.integer :rating 

      t.timestamps null: false
    end
  end
end

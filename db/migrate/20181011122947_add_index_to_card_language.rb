class AddIndexToCardLanguage < ActiveRecord::Migration
  def change
    # changing column with limit,
    # error: Specified key was too long
    change_column :cards, :language, :string, limit: 10

    add_index :cards, :language
    add_index :cards, [:organization_id, :language]
  end
end

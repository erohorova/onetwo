class AddCustomDomainToOrganization < ActiveRecord::Migration
  def change
    add_column :organizations, :custom_domain, :string, limit: 191
    add_index :organizations, :custom_domain
  end
end

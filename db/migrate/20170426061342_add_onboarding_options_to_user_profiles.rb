class AddOnboardingOptionsToUserProfiles < ActiveRecord::Migration
  def change
    add_column :user_profiles, :onboarding_options, :text
  end
end

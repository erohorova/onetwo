class RemoveIsActiveFromClc < ActiveRecord::Migration
  def change
    remove_column :clcs, :is_active, :boolean
  end
end

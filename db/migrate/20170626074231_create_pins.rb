class CreatePins < ActiveRecord::Migration
  def change
    create_table :pins do |t|
      t.integer :pinnable_id
      t.string :pinnable_type, limit: 20
      t.integer :object_id
      t.string :object_type
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :pins, [:pinnable_id, :pinnable_type], using: :btree
  end
end

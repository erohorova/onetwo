class ChangeIsPrivateToNonNullInTeam < ActiveRecord::Migration
  def change
	
	# 15 such records on production 
  	Team.where("is_private IS NULL").each do |team|
    	team.is_private = false
    	team.save
    end
  end
end

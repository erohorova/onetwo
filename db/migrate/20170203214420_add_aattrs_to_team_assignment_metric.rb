class AddAattrsToTeamAssignmentMetric < ActiveRecord::Migration
  def change
    add_column :team_assignment_metrics, :assigned_at, :datetime
  end
end

class CreateTopicRequests < ActiveRecord::Migration
  def change
    create_table :topic_requests do |t|
      t.string :label, limit: 150
      t.integer :requestor_id
      t.integer :approver_id
      t.string :state, limit: 30, index: true
      t.integer :organization_id, index: true

      t.timestamps null: false
    end
  end
end

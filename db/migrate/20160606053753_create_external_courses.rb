class CreateExternalCourses < ActiveRecord::Migration
  def change
    create_table :external_courses do |t|
      t.string :name
      t.text :description
      t.string :image_url
      t.string :course_url
      t.integer :integration_id
      t.string :course_key

      t.timestamps null: false
    end
  end
end

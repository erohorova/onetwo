class AddIndexToPosts < ActiveRecord::Migration
  def change
  	add_index :posts, [:postable_id, :postable_type, :created_at], name: :index_post_created_at_postable
  end
end

class CreateCardUserAccess < ActiveRecord::Migration
  def change
    create_table :card_user_accesses do |t|
      t.references :user, index: true, null: false, foreign_key: true
      t.references :card, index: true, null: false, foreign_key: true

      t.timestamps null: false
    end

    add_index :card_user_accesses, [:user_id, :card_id], unique: true
  end
end

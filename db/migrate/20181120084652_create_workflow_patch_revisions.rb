class CreateWorkflowPatchRevisions < ActiveRecord::Migration
  def change
    create_table :workflow_patch_revisions do |t|
      t.references :organization, foreign_key: true, index: true
      t.string :wuid, limit: 191, index: true
      t.string :data_id, limit: 191, index: true
      t.integer :workflow_id, index: true
      t.integer :output_source_id, index: true
      t.string :workflow_type
      t.string :patch_id
      t.string :patch_type
      t.string :org_name
      t.integer :version, default: 0, index: true
      t.text :data
      t.text :params
      t.text :meta
      t.boolean :completed
      t.boolean :in_progress
      t.boolean :failed
      t.string :job_id, limit: 191, index: true
      t.text :processing_info

      t.timestamps null: false
    end
    add_index :workflow_patch_revisions, [:organization_id, :workflow_id, :output_source_id], name: 'index_on_org_workflow_output_ids'
    add_index :workflow_patch_revisions, [:organization_id, :version], name: 'index_on_org_version'
  end
end

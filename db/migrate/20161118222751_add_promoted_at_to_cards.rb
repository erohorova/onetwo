class AddPromotedAtToCards < ActiveRecord::Migration
  def change
    add_column :cards, :promoted_at, :datetime
  end
end

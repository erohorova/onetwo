class AddIsEdcastAdminToUsers < ActiveRecord::Migration
  def change
    add_column :users, :is_edcast_admin, :boolean, default: false
  end
end

class AddFetchAllContentToEclConfigSources < ActiveRecord::Migration
  def change
    add_column :ecl_config_sources, :fetch_all_content, :boolean, default: false
  end
end

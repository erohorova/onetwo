class AddTransactionIdToCardSubscriptions < ActiveRecord::Migration
  def change
    add_column :card_subscriptions, :transaction_id, :integer
    add_index :card_subscriptions, :transaction_id
  end
end

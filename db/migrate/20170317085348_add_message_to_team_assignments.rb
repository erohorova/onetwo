class AddMessageToTeamAssignments < ActiveRecord::Migration
  def change
    add_column :team_assignments, :message, :string, limit: 255
  end
end

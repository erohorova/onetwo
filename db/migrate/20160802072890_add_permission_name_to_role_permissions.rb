class AddPermissionNameToRolePermissions < ActiveRecord::Migration
  def up
    add_column :role_permissions, :name, :string, :limit => 30
  end
end

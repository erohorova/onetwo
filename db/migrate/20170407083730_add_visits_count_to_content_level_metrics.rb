class AddVisitsCountToContentLevelMetrics < ActiveRecord::Migration
  def change
    add_column :content_level_metrics, :visits_count, :integer, default: 0
  end
end

class AddTopicNameToTopicDailyLearning < ActiveRecord::Migration
  def change
    add_column :topic_daily_learnings, :topic_name, :string
  end
end

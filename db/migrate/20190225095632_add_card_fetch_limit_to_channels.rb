class AddCardFetchLimitToChannels < ActiveRecord::Migration
  def change
    add_column :channels, :card_fetch_limit, :integer
  end
end

class AddDefaultValueToDataTypeInConfig < ActiveRecord::Migration
  def change
    change_column :configs, :data_type, :string, default: :string
  end
end

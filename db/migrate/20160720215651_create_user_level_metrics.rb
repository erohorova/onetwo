class CreateUserLevelMetrics < ActiveRecord::Migration
  def change
    create_table :user_level_metrics do |t|
      t.integer :user_id
      t.integer :organization_id
      t.string :period, limit: 191
      t.integer :offset
      t.integer :smartbites_score, default: 0
      t.integer :smartbites_consumed, default: 0
      t.integer :smartbites_created, default: 0
      t.integer :time_spent, default: 0
      t.integer :percentile, default: 0
    end

    add_index :user_level_metrics, [:user_id, :period, :offset]
    add_index :user_level_metrics, [:organization_id, :period, :offset], name: :indx_user_level_metrics_org_id_period_offset
  end
end

class ExtendPersonalizedEmails < ActiveRecord::Migration
  def change
    add_column :personalized_emails, :extra_info, :text
  end
end

class CreateIntegrations < ActiveRecord::Migration
  def change
    create_table :integrations do |t|
      t.string :name
      t.text :description
      t.attachment :logo
      t.boolean :enabled

      t.timestamps null: false
    end
  end
end

class AddLevelInCardRatings < ActiveRecord::Migration
  def change
    add_column :cards_ratings, :level, :string
  end
end

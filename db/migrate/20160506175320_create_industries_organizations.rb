class CreateIndustriesOrganizations < ActiveRecord::Migration
  def change
    create_table :industries_organizations do |t|
      t.references :organization, index: true, foreign_key: true
      t.references :industry, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

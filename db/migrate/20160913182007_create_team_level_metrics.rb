class CreateTeamLevelMetrics < ActiveRecord::Migration
  def change
    create_table :team_level_metrics do |t|
      t.integer :organization_id, null: false
      t.integer :team_id, null: false
      t.integer :smartbites_consumed, default: 0
      t.integer :time_spent, default: 0
      t.integer :smartbites_liked, default: 0
      t.integer :smartbites_comments_count, default: 0
      t.integer :smartbites_created, default: 0

      t.string :period, limit: 191
      t.integer :offset
    end

    add_index :team_level_metrics, [:organization_id, :period, :offset], name: "team_metrics_indx_on_org_id"
    add_index :team_level_metrics, [:team_id, :period, :offset], name: "team_metrics_indx_on_team_id", unique: true
  end
end

class CreateUsersExternalCourses < ActiveRecord::Migration
  def change
    create_table :users_external_courses do |t|
      t.integer :user_id
      t.integer :external_course_id
      t.string :status

      t.timestamps null: false
    end
  end
end

class RemoveUniquenessContraintOnTagNames < ActiveRecord::Migration
  def change
    # Remove uniqueness constraint and add it back again
    remove_index :tags, :name
    add_index :tags, :name
  end
end

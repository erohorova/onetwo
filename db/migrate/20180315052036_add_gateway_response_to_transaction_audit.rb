class AddGatewayResponseToTransactionAudit < ActiveRecord::Migration
  def change
    add_column :transaction_audits, :transaction_data, :text, null: false
  end
end

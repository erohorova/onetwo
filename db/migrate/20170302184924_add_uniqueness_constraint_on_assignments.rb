class AddUniquenessConstraintOnAssignments < ActiveRecord::Migration
  def change
    Assignment.group([:user_id, :assignable_id, :assignable_type]).having("count(*) > 1").select("user_id, assignable_id, assignable_type, min(id) as min_id").each do |assignment|
      Assignment.where(user_id: assignment.user_id, assignable_id: assignment.assignable_id, assignable_type: assignment.assignable_type).where.not(id: assignment.min_id).destroy_all
    end

    add_index :assignments, [:user_id, :assignable_id, :assignable_type], unique: true, name: "indx_assignments_on_user_assignable"
  end
end

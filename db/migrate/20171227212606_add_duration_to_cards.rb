class AddDurationToCards < ActiveRecord::Migration
  def change
    add_column :cards, :duration, :integer
  end
end

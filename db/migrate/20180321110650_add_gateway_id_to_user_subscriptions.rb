class AddGatewayIdToUserSubscriptions < ActiveRecord::Migration
  def change
    add_column :user_subscriptions, :gateway_id, :string
  end
end

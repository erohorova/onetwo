class AddTacAcceptedToUserProfile < ActiveRecord::Migration
  def change
    add_column :user_profiles, :tac_accepted, :boolean
  end
end
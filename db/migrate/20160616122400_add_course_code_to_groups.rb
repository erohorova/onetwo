class AddCourseCodeToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :course_code, :string
  end
end

class CreateClcOrganizationsRecords < ActiveRecord::Migration
  def change
    create_table :clc_organizations_records, id: false do |t|
      t.timestamp :created_at
      t.references :user, index: true, foreign_key: true
      t.integer :score
      t.references :card, index: true, foreign_key: true
      t.references :organization, index: true, foreign_key: true
    end

    add_index :clc_organizations_records, [:user_id, :card_id], using: :btree, unique: true
  end
end

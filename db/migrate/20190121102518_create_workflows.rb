class CreateWorkflows < ActiveRecord::Migration
  def change
    create_table :workflows do |t|
      t.references :organization, index: true, null: false
      t.references :user, index: true, null: false
      t.string :name
      t.string :status, length: 20, default: 'new'
      t.boolean :is_enabled, default: false
      t.string :wf_type, length: 30, null: false
      t.text :wf_input_source, null: false
      t.text :wf_actionable_columns, null: false
      t.text :wf_rules
      t.text :wf_action, null: false
      t.text :wf_output_result

      t.timestamps null: false
    end
  end
end

class RenameCardIdsToEclIdsTdl < ActiveRecord::Migration
  def change
    rename_column :topic_daily_learnings, :card_ids, :ecl_ids
  end
end

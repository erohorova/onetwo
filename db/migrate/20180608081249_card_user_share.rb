class CardUserShare < ActiveRecord::Migration
  def change
    create_table :card_user_shares do |t|
      t.references :user, index: true, null: false, foreign_key: true
      t.references :card, index: true, null: false, foreign_key: true
 
      t.timestamps
    end
  end
end

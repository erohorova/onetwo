class IncreateIdentityProviderTokenLimit < ActiveRecord::Migration
  def change
    change_column :identity_providers, :token, :string, :limit => 4096
  end
end

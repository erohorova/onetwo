class AddIndexToUserContentCompeltions < ActiveRecord::Migration
  def change
  	change_column :user_content_completions, :completable_type, :string, limit: 191
  	add_index :user_content_completions, [:completable_id, :completable_type], name: 'index_user_content_completions_on_completable_id_and_type'
  end
end

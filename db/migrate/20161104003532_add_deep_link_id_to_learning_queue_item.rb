class AddDeepLinkIdToLearningQueueItem < ActiveRecord::Migration
  def change
    add_column :learning_queue_items, :deep_link_id, :integer
  end
end

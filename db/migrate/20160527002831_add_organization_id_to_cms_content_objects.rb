class AddOrganizationIdToCmsContentObjects < ActiveRecord::Migration
  def change
    add_column :cms_content_objects, :organization_id, :integer
  end
end

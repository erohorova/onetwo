class AddColumnRankOnChannelsCards < ActiveRecord::Migration
  def change
    add_column :channels_cards, :rank, :integer
  end
end

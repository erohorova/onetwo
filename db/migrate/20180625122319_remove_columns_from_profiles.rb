class RemoveColumnsFromProfiles < ActiveRecord::Migration
  def change
    remove_column :profiles, :business_unit, :string
    remove_column :profiles, :job_id, :integer
    remove_column :profiles, :job_title, :string
    remove_column :profiles, :department, :string
    remove_column :profiles, :country, :string
    remove_column :profiles, :location, :string
    remove_column :profiles, :manager_external_id, :string
    remove_column :profiles, :manager_name, :string
    remove_column :profiles, :manager_internal_id, :string
    remove_column :profiles, :employee_type, :string
  end
end

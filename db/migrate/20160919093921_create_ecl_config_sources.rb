class CreateEclConfigSources < ActiveRecord::Migration
  def change
    create_table :ecl_config_sources do |t|
      t.integer :ecl_content_config_id, null: false
      t.string :source, null: false
    end
    add_foreign_key :ecl_config_sources, :ecl_content_configs, column: :ecl_content_config_id
  end
end

class AddThumbnailFileResourceIdToVideoStream < ActiveRecord::Migration
  def change
    add_column :video_streams, :thumbnail_file_resource_id, :integer
  end
end

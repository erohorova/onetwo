class CreateSuperAdminAuditLogs < ActiveRecord::Migration
  def change
    create_table :super_admin_audit_logs do |t|
      t.integer :user_id
      t.integer :organization_id
      t.string :ip
      t.string :user_agent
      t.timestamps
    end
  end
end

class RenameTeamCardsToTeamsCards < ActiveRecord::Migration
  def change
    rename_table :team_cards, :teams_cards
  end
end

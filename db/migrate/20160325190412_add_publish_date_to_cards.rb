class AddPublishDateToCards < ActiveRecord::Migration
  def change
    add_column :cards, :published_at, :timestamp
  end
end

class UpdateUserStatusWithSuspended < ActiveRecord::Migration
  def change
    User.suspended.update_all(status: User::SUSPENDED_STATUS)
  end
end

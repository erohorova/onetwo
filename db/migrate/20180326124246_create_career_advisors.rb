class CreateCareerAdvisors < ActiveRecord::Migration
  def change
    create_table :career_advisors do |t|
      t.string :skill, null: false
      t.integer :organization_id, index: true, foreign_key: true, null: false
      t.text :links

      t.timestamps null: false
    end

    CareerAdvisor.create_default_skills
  end
end

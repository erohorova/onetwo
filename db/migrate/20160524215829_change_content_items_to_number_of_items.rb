class ChangeContentItemsToNumberOfItems < ActiveRecord::Migration
  def change
    rename_column :cms_content_sources_organizations, :content_items, :number_of_items
  end
end

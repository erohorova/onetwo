class AddLevelToCardMetadata < ActiveRecord::Migration
  def change
    add_column :card_metadata, :level, :string, limit: 20
  end
end

class AddInfluxConfigToOrganization < ActiveRecord::Migration
  def change
    add_column :organizations, :enable_influx_event_recorder, :boolean, default: false
  end
end

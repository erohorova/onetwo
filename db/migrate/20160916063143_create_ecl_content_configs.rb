class CreateEclContentConfigs < ActiveRecord::Migration
  def change
    create_table :ecl_content_configs do |t|
      t.integer :organization_id, null: false

      t.string :name, null: false
      t.datetime :last_fetched
    end
    add_foreign_key :ecl_content_configs, :organizations, column: :organization_id
  end
end

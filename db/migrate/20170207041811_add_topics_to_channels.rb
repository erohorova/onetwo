class AddTopicsToChannels < ActiveRecord::Migration
  def change
    add_column :channels, :topics, :text
  end
end

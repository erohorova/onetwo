class AddEnableDeeplinkingToOrganization < ActiveRecord::Migration
  def change
    add_column :organizations, :enable_deeplinking, :boolean, default: false
  end
end

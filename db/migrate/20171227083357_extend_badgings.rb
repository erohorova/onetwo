class ExtendBadgings < ActiveRecord::Migration
  def change
    add_column :badgings, :all_quizzes_answered, :boolean, default: false
  end
end

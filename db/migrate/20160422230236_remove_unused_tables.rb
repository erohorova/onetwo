class RemoveUnusedTables < ActiveRecord::Migration
  def change
    drop_table :beta_registrations
    drop_table :card_updates
    drop_table :card_updates_causal_users
    drop_table :cards_default_signatures
    drop_table :cards_contents
    drop_table :cards_disallowed_actions
    drop_table :cards_metadata
    drop_table :cards_metadata_configs
    drop_table :cms_metacards
    drop_table :cms_metacards_organizations
    drop_table :conversations
    drop_table :conversations_users
    drop_table :locations
    drop_table :locations_locatables
    drop_table :organizations_users
    drop_table :solicitation_cards
    drop_table :solicitation_results
    drop_table :topics
    drop_table :topics_cards
    drop_table :topics_users
    drop_table :topics_groups
    drop_table :user_signatures
    drop_table :users_solicitation_cards
  end
end

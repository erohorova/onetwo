class AddVisibleToTaggings < ActiveRecord::Migration
  def change
    add_column :taggings, :visible, :boolean, default: true
  end
end

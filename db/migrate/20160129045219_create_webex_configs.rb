class CreateWebexConfigs < ActiveRecord::Migration
  def change
    create_table :webex_configs do |t|
      t.string :name
      t.string :site_name
      t.string :site_id
      t.string :partner_id
      t.integer :client_id

      t.timestamps null: false
    end
  end
end

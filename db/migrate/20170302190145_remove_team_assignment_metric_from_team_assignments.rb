class RemoveTeamAssignmentMetricFromTeamAssignments < ActiveRecord::Migration
  def change
    remove_column :team_assignments, :team_assignment_metric
  end
end

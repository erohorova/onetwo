class AddClientIdToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :client_id, :integer
  end
end

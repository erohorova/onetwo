class ForumNotificationEnabledToGroups < ActiveRecord::Migration
  def change
  	add_column :groups, :forum_notifications_enabled, :boolean, default: true
  end
end

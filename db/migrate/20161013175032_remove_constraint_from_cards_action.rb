class RemoveConstraintFromCardsAction < ActiveRecord::Migration
  def change
  	change_column :cards_actions, :cards_config_id, :integer, null: true
  end
end

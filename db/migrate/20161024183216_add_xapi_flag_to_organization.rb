class AddXapiFlagToOrganization < ActiveRecord::Migration
  def change
    add_column :organizations, :xapi_enabled, :boolean, default: false
  end
end

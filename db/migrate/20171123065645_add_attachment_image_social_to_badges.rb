class AddAttachmentImageSocialToBadges < ActiveRecord::Migration
  def self.up
    change_table :badges do |t|
      t.attachment :image_social
    end
  end

  def self.down
    remove_attachment :badges, :image_social
  end
end

class FixVarcharSizeForUserCompletions < ActiveRecord::Migration
  def change
    change_column :user_completions, :completable_type, :string, limit:191
  end
end

class AddColumnsToRoles < ActiveRecord::Migration
  def change
    add_column :roles, :image_url, :string
    add_column :roles, :threshold, :integer
  end
end

class CreateSuperAdminUsers < ActiveRecord::Migration
  def change
    create_table :super_admin_users do |t|
      t.belongs_to :user
      t.timestamps
    end
  end
end

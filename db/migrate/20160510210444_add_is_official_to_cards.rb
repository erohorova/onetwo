class AddIsOfficialToCards < ActiveRecord::Migration
  def change
    add_column :cards, :is_official, :boolean, default: false
  end
end

class ChangeVideoStreamIndexOnSlugs < ActiveRecord::Migration
  def change
    remove_index :video_streams, :slug
    add_index :video_streams, [:slug, :organization_id], unique: true
  end
end

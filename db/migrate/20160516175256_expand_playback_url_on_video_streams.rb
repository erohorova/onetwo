class ExpandPlaybackUrlOnVideoStreams < ActiveRecord::Migration
  def change
    change_column :video_streams, :playback_url, :string, limit: 1024
  end
end

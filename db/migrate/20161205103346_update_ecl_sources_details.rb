class UpdateEclSourcesDetails < ActiveRecord::Migration
  def change
    add_column :ecl_providers, :is_featured, :boolean, default: false
    add_column :ecl_config_sources, :is_enabled, :boolean, default: false
  end
end

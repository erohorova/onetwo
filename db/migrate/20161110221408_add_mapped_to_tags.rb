class AddMappedToTags < ActiveRecord::Migration
  def change
    add_column :tags, :mapped, :boolean, default: false
  end
end

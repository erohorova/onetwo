class AddColumnsToChannelsCard < ActiveRecord::Migration
  def change
    add_column :channels_cards, :state, :string, limit: 191, index: true
    add_column :channels_cards, :curated_at, :timestamp
  end
end

class AddMandatoryToTeam < ActiveRecord::Migration
  def change
    add_column :teams, :is_mandatory, :boolean, default: false
  end
end

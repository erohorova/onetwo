class RemoveVisitsCountFromContentLevelMetrics < ActiveRecord::Migration
  def change
    remove_column :content_level_metrics, :visits_count
  end
end

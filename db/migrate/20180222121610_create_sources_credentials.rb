class CreateSourcesCredentials < ActiveRecord::Migration
  def change
    create_table :sources_credentials do |t|
      t.string :source_id, limit: 50, unique: true, null: false
      t.string :shared_secret, limit: 50
      t.text :auth_api_info

      t.timestamps null: false
    end
  end
end

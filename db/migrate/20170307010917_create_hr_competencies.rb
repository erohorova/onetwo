class CreateHrCompetencies < ActiveRecord::Migration
  def change
    create_table :hr_competencies do |t|
      t.integer :organization_id, null: false

      t.string :name
      t.string :description
      t.string :category

      t.timestamps
    end
  end
end

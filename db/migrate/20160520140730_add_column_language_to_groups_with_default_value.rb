class AddColumnLanguageToGroupsWithDefaultValue < ActiveRecord::Migration
  def change
    remove_column :groups, :language, :string
    add_column    :groups, :language, :string, default: 'en'
  end
end

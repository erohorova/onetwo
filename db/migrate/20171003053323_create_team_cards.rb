class CreateTeamCards < ActiveRecord::Migration
  def change
    create_table :team_cards do |t|
      t.integer :team_id
      t.integer :card_id
      t.string :type
      t.timestamps null: false
    end
  end
end

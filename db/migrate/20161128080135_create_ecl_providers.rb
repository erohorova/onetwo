class CreateEclProviders < ActiveRecord::Migration
  def change
    create_table :ecl_providers do |t|
      t.string :name
      t.string :description
      t.string :resource_class
      t.string :card_type
      t.string :card_layout
      t.text :auth_details
      t.boolean :premium, default: false

      t.timestamps null: false
    end
  end
end

class RedoIndexOnUsersTopicsActivities < ActiveRecord::Migration
  def change

    remove_index :users_topics_activities,
                 column: :organization_id,
                 name:  "index_u_t_activities_on_org_id"

    remove_index :users_topics_activities,
                 column: :organization_id,
                 name: "index_u_t_activities_on_user_id"

    add_index :users_topics_activities,
              [:organization_id, :user_id],
              name: "index_u_t_activities_on_org_and_user_id"
    
  end
end

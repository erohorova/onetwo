class ChangeFromAndToColumn < ActiveRecord::Migration
  def change
  	remove_column  :schools, :from
  	remove_column  :schools, :to
  	add_column :schools, :from_year, :string
  	add_column :schools, :to_year, :string
  end
end

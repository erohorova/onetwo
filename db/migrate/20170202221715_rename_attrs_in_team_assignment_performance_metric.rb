class RenameAttrsInTeamAssignmentPerformanceMetric < ActiveRecord::Migration
  def change
    rename_column :team_assignment_metrics, :not_started_count, :assigned_count
    add_column :team_assignment_metrics, :updated_at, :datetime
  end
end

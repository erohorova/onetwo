class CreateRecharges < ActiveRecord::Migration
  def change
    create_table :recharges do |t|
      t.decimal :amount, precision: 8, scale: 2, null: false
      t.string :currency, default: 'USD', limit: 10, null: false
      t.decimal :skillcoin, precision: 8, scale: 2, null: false
      t.boolean :active, default: true
      t.text :description

      t.timestamps null: false
    end
  end
end

class CreateUserContentLevelMetrics < ActiveRecord::Migration
  def change
    create_table :user_content_level_metrics do |t|
      t.integer :user_id
      t.integer :content_id
      t.string :content_type, limit: 191
      t.integer :smartbites_score, default: 0

      t.string :period, limit: 191
      t.integer :offset

      t.timestamps
    end

    add_index :user_content_level_metrics, [:user_id, :period, :offset], name: "indx_usr_content_lvl_mtrcs_on_usr_period_off"
  end
end

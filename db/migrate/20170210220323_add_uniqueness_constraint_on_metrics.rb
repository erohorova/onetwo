class AddUniquenessConstraintOnMetrics < ActiveRecord::Migration
  def change

    UserLevelMetric.group([:user_id, :period, :offset]).having("count(*) > 1").select("user_id, period, offset, min(id) as min_id").each do |um|
      UserLevelMetric.where(user_id: um.user_id, period: um.period, offset: um.offset).where.not(id: um.min_id).destroy_all
    end

    if index_exists?(:user_level_metrics, [:user_id, :period, :offset])
      remove_index :user_level_metrics, [:user_id, :period, :offset]
    end
    add_index :user_level_metrics, [:user_id, :period, :offset], unique: true
  end
end

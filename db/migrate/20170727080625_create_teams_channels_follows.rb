class CreateTeamsChannelsFollows < ActiveRecord::Migration
  def change
    create_table :teams_channels_follows do |t|
      t.references :team, index: true, foreign_key: true
      t.references :channel, index: true, foreign_key: true

      t.timestamps null: false
    end

    add_index :teams_channels_follows, [:team_id, :channel_id], unique: true, using: :btree
  end
end
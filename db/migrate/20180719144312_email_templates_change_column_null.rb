class EmailTemplatesChangeColumnNull < ActiveRecord::Migration
  def change
    change_column_null :email_templates, :content, true
  end
end

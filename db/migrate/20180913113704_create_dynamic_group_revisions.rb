class CreateDynamicGroupRevisions < ActiveRecord::Migration
  def change
    create_table :dynamic_group_revisions do |t|
      t.integer :external_id,     index: true
      t.string  :uid,             limit: 191
      t.string  :title,           limit: 191
      t.integer :user_id,         index: true
      t.integer :organization_id
      t.integer :group_id,        index: true
      t.integer :item_id
      t.string  :item_type,       limit: 191
      t.string  :action,          limit: 191
      t.string  :message,         limit: 191

      t.timestamps null: false
    end
    add_index :dynamic_group_revisions, [:title, :organization_id], unique: true
    add_index :dynamic_group_revisions, :uid
  end
end

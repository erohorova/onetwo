class AddVotesCountToActivityStreams < ActiveRecord::Migration
  def change
    add_column :activity_streams, :votes_count, :integer, default: 0
  end
end

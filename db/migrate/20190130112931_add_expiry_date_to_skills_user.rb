class AddExpiryDateToSkillsUser < ActiveRecord::Migration
  def change
    add_column :skills_users, :expiry_date, :datetime
  end
end

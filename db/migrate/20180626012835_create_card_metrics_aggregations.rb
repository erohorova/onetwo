class CreateCardMetricsAggregations < ActiveRecord::Migration
  def change
    create_table :card_metrics_aggregations do |t|
      # NOTE: foreign_key: false is set for these in a subsequent migration,
      # we don't want to prevent records from being created if their foreign
      # keys don't point to other records.
      # This is because Influx doesn't delete records at the same time as SQL.
      t.references :organization, index: true, foreign_key: true, null: false
      t.references :card, index: true, foreign_key: true, null: false
      t.integer :num_likes, null: false
      t.integer :num_views, null: false
      t.integer :num_completions, null: false
      t.integer :num_bookmarks, null: false
      t.integer :num_comments, null: false
      t.integer :start_time, null: false
      t.integer :end_time, null: false
      t.timestamps null: false
    end
  end
end

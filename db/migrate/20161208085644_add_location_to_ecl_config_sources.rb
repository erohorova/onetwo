class AddLocationToEclConfigSources < ActiveRecord::Migration
  def change
    add_column :ecl_config_sources, :location, :string
  end
end

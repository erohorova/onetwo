class AddAuthInfoToProviders < ActiveRecord::Migration
  def change
  	add_column :identity_providers, :auth_info, :text
  end
end

class ExtendCardsWithReadableCardType < ActiveRecord::Migration
  def change
    add_column :cards, :readable_card_type, :integer, length: 2
  end
end

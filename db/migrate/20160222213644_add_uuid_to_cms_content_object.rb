class AddUuidToCmsContentObject < ActiveRecord::Migration
  def change
    add_column :cms_content_objects, :uuid, :string, limit: 191, unique: true, null: false
  end
end

class AddWelcomeTokenToUser < ActiveRecord::Migration
  def change
    add_column :users, :welcome_token, :string, limit: 191
    add_index :users, :welcome_token, unique: true
  end
end

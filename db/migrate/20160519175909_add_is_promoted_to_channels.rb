class AddIsPromotedToChannels < ActiveRecord::Migration
  def change
    add_column :channels, :is_promoted, :boolean, :default => false
  end
end

class UpdateColumnsTeamAssignmentMetric < ActiveRecord::Migration
  def change
    remove_column :team_assignment_metrics, :content_type
    rename_column :team_assignment_metrics, :content_id, :card_id
  end
end

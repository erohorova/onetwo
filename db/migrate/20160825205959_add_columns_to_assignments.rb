class AddColumnsToAssignments < ActiveRecord::Migration
  def change
    add_column :assignments, :state, :string, default: Assignment::EVENT_ACTIONS[:assign]
    add_column :assignments, :started_at, :datetime
    add_column :assignments, :completed_at, :datetime
    add_column :assignments, :due_at, :datetime
  end
end

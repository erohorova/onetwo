class AddAllTimeViewsCountToCards < ActiveRecord::Migration
  def change
    # This column is only used internally by the application.
    # The values returned to clients are stored in Redis.
    #
    # Steps are as follows:
    #   1. We do an initial seed on the authoritative_all_time_views_count
    #      (this happens from an after_party task)
    #   2. The first time a read/increment happens for a key, we look it up in MySQL
    #      and store it to redis
    #   3. Further read/increment operations go to Redis
    #   4. Every hour, a background job copies the card all time views counts to MySQL.
    add_column :cards, :authoritative_all_time_views_count, :integer
  end
end

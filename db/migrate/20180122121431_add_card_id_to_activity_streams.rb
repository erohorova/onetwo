class AddCardIdToActivityStreams < ActiveRecord::Migration
  def change
    add_reference :activity_streams, :card, foreign_key: true, index: true
  end
end

class AddIndustryRefToOrganizations < ActiveRecord::Migration
  def change
    add_reference :organizations, :industry, index: true, foreign_key: true
  end
end

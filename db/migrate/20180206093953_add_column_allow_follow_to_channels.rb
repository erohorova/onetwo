class AddColumnAllowFollowToChannels < ActiveRecord::Migration
  def change
    add_column :channels, :allow_follow, :bool, default: true
  end
end

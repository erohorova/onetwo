class ChangeIdentityProviderIndex < ActiveRecord::Migration
  def change
    change_column :identity_providers, :uid, :string, limit: 191
    remove_index :identity_providers, [:type, :user_id]
    add_index :identity_providers, [:uid, :type, :user_id], unique: true
  end
end

class AddRequestedByUserToTopicDailyLearnings < ActiveRecord::Migration
  def change
    add_column :topic_daily_learnings, :requested_by_user, :boolean, default: true
  end
end

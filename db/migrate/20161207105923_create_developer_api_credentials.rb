class CreateDeveloperApiCredentials < ActiveRecord::Migration
  def change
    create_table :developer_api_credentials do |t|
      t.string :api_key, unique: true
      t.string :shared_secret
      t.references :organization, index: true
    end
  end
end
class AddFilestackColumnInProjectSubmissions < ActiveRecord::Migration
  def change
    add_column :project_submissions, :filestack, :text
  end
end

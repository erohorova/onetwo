class CreateOrganizationIntegrations < ActiveRecord::Migration
  def change
    create_table :organization_integrations do |t|
      t.integer :organization_id
      t.integer :integration_id

      t.timestamps null: false
    end
  end
end

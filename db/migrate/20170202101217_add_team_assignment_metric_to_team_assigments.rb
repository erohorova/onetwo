class AddTeamAssignmentMetricToTeamAssigments < ActiveRecord::Migration
  def change
    add_column :team_assignments, :team_assignment_metric, :integer
    add_index :team_assignments, :team_assignment_metric
  end
end

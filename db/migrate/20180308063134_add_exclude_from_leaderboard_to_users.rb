class AddExcludeFromLeaderboardToUsers < ActiveRecord::Migration
  def change
    add_column :users, :exclude_from_leaderboard, :boolean
  end
end

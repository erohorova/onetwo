class PopulateInvitationTableWithMissedRecipiendId < ActiveRecord::Migration
  def change
    invitations = Invitation.where(recipient_id: nil, invitable_type: 'Team').includes(invitable: :users)
    invitations.each do |inv|
      user_id = inv.invitable&.users&.find_by(email: inv.recipient_email)&.id
      inv.update_attributes(recipient_id: user_id)  unless user_id.nil?
    end
  end
end

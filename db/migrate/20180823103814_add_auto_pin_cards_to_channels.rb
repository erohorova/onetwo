class AddAutoPinCardsToChannels < ActiveRecord::Migration
  def change
  	add_column :channels, :auto_pin_cards, :boolean, default: false
  end
end

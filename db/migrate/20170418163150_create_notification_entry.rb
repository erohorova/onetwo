class CreateNotificationEntry < ActiveRecord::Migration
  def change
    create_table :notification_entries do |t|
      # User details
      t.integer :user_id, null: false

      # Event details
      t.string :event_name, null: false, limit: 191
      t.string :notification_name, null: false, limit: 191
      t.string :sourceable_type, null: false, limit: 191
      t.integer :sourceable_id, null: false

      # Intervals and mediums
      # Web
      t.boolean :config_web_single, null: false
      t.integer :status_web_single, limit: 1, null: false
      t.string :web_text, limit: 512

      # Email
      t.boolean :config_email_single, null: false
      t.integer :status_email_single, limit: 1, null: false

      t.boolean :config_email_digest_daily, null: false
      t.integer :status_email_digest_daily, limit: 1, null: false

      t.boolean :config_email_digest_weekly, null: false
      t.integer :status_email_digest_weekly, limit: 1, null: false

      # Push
      t.boolean :config_push_single, null: false
      t.integer :status_push_single, limit: 1, null: false

      t.boolean :config_push_digest_daily, null: false
      t.integer :status_push_digest_daily, limit: 1, null: false

      t.boolean :config_push_digest_weekly, null: false
      t.integer :status_push_digest_weekly, limit: 1, null: false

      # Timing details
      t.date :date_in_timezone, null: false
      t.datetime :sending_time_in_timezone, null: false

      t.timestamps null: false
    end
    add_foreign_key :notification_entries, :users, column: :user_id
    add_index :notification_entries, :event_name, unique: false
    add_index :notification_entries, :notification_name, unique: false
    add_index :notification_entries, :sourceable_type, unique: false
    add_index :notification_entries, :sourceable_id, unique: false

    create_table :notification_configs do |t|
      t.integer :organization_id
      t.integer :user_id

      t.text :configuration

      t.timestamps null: false
    end
    add_foreign_key :notification_configs, :organizations, column: :organization_id
    add_foreign_key :notification_configs, :users, column: :user_id
    add_index :notification_configs, :organization_id, unique: true
    add_index :notification_configs, :user_id, unique: true
  end
end

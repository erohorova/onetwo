class RemoveContraintFromCards < ActiveRecord::Migration
  def change
    change_column :cards, :card_type, :string, null: true
  end
end

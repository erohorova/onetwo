class AddNotNullConstraintToIsPrivateInTeam < ActiveRecord::Migration
  def change
  	change_column_null :teams, :is_private, false
  end
end

class DropHrTables < ActiveRecord::Migration
  def change
    drop_table :hr_competencies
    drop_table :hr_domains
    drop_table :hr_government_identifications
    drop_table :hr_government_licenses
    drop_table :hr_locations
    drop_table :hr_organizations
    drop_table :hr_user_statuses
  end
end

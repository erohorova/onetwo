class AddCommentsCountToActivityStreams < ActiveRecord::Migration
  def change
    add_column :activity_streams, :comments_count, :integer, default: 0
  end
end
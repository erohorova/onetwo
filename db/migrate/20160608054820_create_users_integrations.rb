class CreateUsersIntegrations < ActiveRecord::Migration
  def change
    create_table :users_integrations do |t|
      t.integer :user_id
      t.integer :integration_id

      t.timestamps null: false
    end
    add_index :users_integrations, :user_id
  	add_index :users_external_courses, [:user_id, :external_course_id]
  end
end

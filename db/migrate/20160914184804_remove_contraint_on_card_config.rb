class RemoveContraintOnCardConfig < ActiveRecord::Migration
  def change
    change_column :cards, :cards_config_id, :integer, null: true
  end
end

class AddExperienceToSkillsUsers < ActiveRecord::Migration
  def change
    add_column :skills_users, :experience, :string
  end
end

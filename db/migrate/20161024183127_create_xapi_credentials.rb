class CreateXapiCredentials < ActiveRecord::Migration
  def change
    create_table :xapi_credentials do |t|
      t.string :end_point
      t.string :lrs_login_key
      t.string :lrs_password_key
      t.string :lrs_api_version
      t.references :organization, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

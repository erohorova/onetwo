class CreateGovernmentLicenses < ActiveRecord::Migration
  def change
    create_table :hr_government_licenses do |t|
      t.integer :user_id, null: false, index: true

      t.string :license_id
      t.string :license_type
      t.string :country
      t.date :issued_date
      t.date :expiration_date
      t.date :verification_date
    end
  end
end

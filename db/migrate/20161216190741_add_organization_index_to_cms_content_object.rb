class AddOrganizationIndexToCmsContentObject < ActiveRecord::Migration
  def change
    add_index :cms_content_objects, :organization_id
  end
end

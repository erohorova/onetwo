class AddTimestampToChannelsCard < ActiveRecord::Migration
  def change
    add_column :channels_cards, :created_at, :datetime, null: false
    add_column :channels_cards, :updated_at, :datetime, null: false
  end
end

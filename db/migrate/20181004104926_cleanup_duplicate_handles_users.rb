class CleanupDuplicateHandlesUsers < ActiveRecord::Migration
  def change
    #Cleanup logic to update duplicate user handles 
    users = User.with_handle.group([:handle, :organization_id]).having("count(*) > 1")
    users.each do |user|
      users_array = User.where(handle: user.handle, organization_id: user.organization_id).to_a
      users_array.shift #remove first record from array
      users_array.each_with_index do |u|
        u.handle = nil
        u.assign_handle
        u.save
      end
    end
  end
end

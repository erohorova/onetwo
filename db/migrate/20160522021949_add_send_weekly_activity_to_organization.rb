class AddSendWeeklyActivityToOrganization < ActiveRecord::Migration
  def change
    add_column :organizations, :send_weekly_activity, :boolean, default: false
  end
end

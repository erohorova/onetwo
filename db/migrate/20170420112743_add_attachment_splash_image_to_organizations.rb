class AddAttachmentSplashImageToOrganizations < ActiveRecord::Migration
  def self.up
    change_table :organizations do |t|
      t.attachment :splash_image
    end
  end

  def self.down
    remove_attachment :organizations, :splash_image
  end
end

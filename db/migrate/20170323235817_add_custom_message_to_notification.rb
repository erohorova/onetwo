class AddCustomMessageToNotification < ActiveRecord::Migration
  def change
    add_column :notifications, :custom_message, :string
  end
end

class AddSourceToIdentityProviders < ActiveRecord::Migration
  def change
    add_column :identity_providers, :source_id, :integer
  end
end

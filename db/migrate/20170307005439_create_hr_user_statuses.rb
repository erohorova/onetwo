class CreateHrUserStatuses < ActiveRecord::Migration
  def change
    create_table :hr_user_statuses do |t|
      t.integer :user_id, null: false, index: true

      t.string :employee_type
      t.boolean :is_manager
      t.boolean :active
      t.date :hire_date
      t.date :termination_date
      t.string :cost_center
      t.string :business_title
    end
  end
end

class CreateCmsMetacardsOrganizations < ActiveRecord::Migration
  def change
    create_table :cms_metacards_organizations do |t|
      t.integer :cms_metacard_id
      t.integer :organization_id
      t.string :status, limit: 191, default: 'pending'

      t.timestamps null: false
    end

    add_index :cms_metacards_organizations, [:cms_metacard_id, :organization_id], name: 'metacards_org_metacard_org'
    add_index :cms_metacards_organizations, [:organization_id, :status], name: 'metacards_org_org_status'
  end
end

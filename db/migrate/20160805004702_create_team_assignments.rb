class CreateTeamAssignments < ActiveRecord::Migration
  def change
    create_table :team_assignments do |t|
      t.integer :assignment_id
      t.integer :team_id

      t.timestamps null: false
    end
  end
end

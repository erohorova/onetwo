class CreatePrices < ActiveRecord::Migration
  def change
    create_table :prices do |t|
      t.string :ecl_id
      t.integer :card_id
      t.string :currency, default: 'USD', limit: 10
      t.decimal :amount, precision: 8, scale: 2

      t.timestamps null: false
    end
  end
end

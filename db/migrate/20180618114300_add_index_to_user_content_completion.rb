class AddIndexToUserContentCompletion < ActiveRecord::Migration
  def change
    add_index :user_content_completions, [:user_id, :completable_id], name: "unique_user_content_completion", unique: true
  end
end

class AddAdditionalInfoToCmsJudgement < ActiveRecord::Migration
  def change
    add_column :cms_judgements, :additional_info, :text, default: nil
  end
end

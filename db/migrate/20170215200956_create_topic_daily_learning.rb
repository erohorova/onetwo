class CreateTopicDailyLearning < ActiveRecord::Migration
  def change
    create_table :topic_daily_learnings do |t|
      t.string :topic_id, null: false, limit: 191
      t.integer :organization_id, null: false
      t.text :card_ids
      t.string :date, limit: 11, null: false
      t.string :processing_state, null: false

      t.timestamps
    end

    add_foreign_key :topic_daily_learnings, :organizations, column: :organization_id
    add_index :topic_daily_learnings, [:topic_id, :organization_id, :date], unique: true, name: 'indx_topic_dl_topic_org_date'
  end
end

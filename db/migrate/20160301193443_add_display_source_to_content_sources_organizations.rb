class AddDisplaySourceToContentSourcesOrganizations < ActiveRecord::Migration
  def change
    add_column :cms_content_sources_organizations, :display_source, :text
  end
end

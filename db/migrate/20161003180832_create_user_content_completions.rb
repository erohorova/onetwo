class CreateUserContentCompletions < ActiveRecord::Migration
  def change
    create_table :user_content_completions do |t|
    	t.integer :user_id
    	t.integer :completable_id
    	t.string :completable_type
    	t.string :topics
    	t.string :state
      t.timestamps null: false
    end
  end
end

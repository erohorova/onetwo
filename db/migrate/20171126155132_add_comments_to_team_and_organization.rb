class AddCommentsToTeamAndOrganization < ActiveRecord::Migration
  def change
    add_column :teams, :comments_count, :integer, default: 0
    add_column :organizations, :comments_count, :integer, default: 0
  end
end

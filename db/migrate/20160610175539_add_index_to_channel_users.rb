class AddIndexToChannelUsers < ActiveRecord::Migration
  def change
    add_index :channels_users, :user_id
  end
end

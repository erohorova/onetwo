class CreateWidgets < ActiveRecord::Migration
  def change
    create_table :widgets do |t|
      t.integer     :creator_id,        limit: 4,            null: false
      t.integer     :organization_id,   limit: 4,            null: false,   index: true
      t.integer     :parent_id,         limit: 4,            null: false
      t.string      :parent_type,       limit: 20,           null: false
      t.text        :code,              limit: 4294967295,   null: false
      t.string      :context,           limit: 20,           null: false
      t.boolean     :enabled,           default: false

      t.timestamps null: false
    end
  end
end

class CreateOrgSsos < ActiveRecord::Migration
  def change
    create_table :org_ssos do |t|
      t.references :organization
      t.references :sso
      t.integer :position, default: 1

      t.timestamps null: false
    end
  end
end

class AddVotesCountToCard < ActiveRecord::Migration
  def change
    add_column :cards, :votes_count, :integer, default: 0
  end
end

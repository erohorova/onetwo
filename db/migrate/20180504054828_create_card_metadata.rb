class CreateCardMetadata < ActiveRecord::Migration
  def change
    create_table :card_metadata do |t|
      t.references :card, foreign_key: true, index: true, null: false
      t.string :plan, default: 'free'

      t.timestamps null: false
    end
  end
end

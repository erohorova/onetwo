class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :name, limit: 191, null: false
      t.text :description
      t.string :slug, limit: 191, index: true
      t.references :organization, index: true
      t.attachment :image

      t.timestamps null: false
    end

    create_table :teams_users do |t|
      t.references :team
      t.references :user, index: true
      t.string :as_type, limit: 191

      t.timestamps null: false
    end
    add_index :teams_users, [:team_id, :as_type]
    add_index :teams_users, [:team_id, :user_id, :as_type]
  end
end

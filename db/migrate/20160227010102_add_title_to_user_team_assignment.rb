class AddTitleToUserTeamAssignment < ActiveRecord::Migration
  def change
    add_column :user_team_assignments, :title, :string
  end
end

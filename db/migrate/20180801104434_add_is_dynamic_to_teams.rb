class AddIsDynamicToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :is_dynamic, :boolean, default: false
  end
end

class SupportCustomFields < ActiveRecord::Migration
  def change
    create_table :custom_fields do |t|
      t.references :organization, index: true, foreign_key: true, null: false

      t.string :abbreviation
      t.string :display_name

      t.timestamps null: false
    end

    create_table :user_custom_fields do |t|
      t.references :custom_field, foreign_key: true, null: false
      t.references :user, index: true, foreign_key: true, null: false

      t.text :value

      t.timestamps null: false
    end

    add_index :user_custom_fields, [:custom_field_id, :user_id]
  end
end

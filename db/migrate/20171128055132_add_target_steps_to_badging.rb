class AddTargetStepsToBadging < ActiveRecord::Migration
  def change
    add_column :badgings, :target_steps, :string
  end
end

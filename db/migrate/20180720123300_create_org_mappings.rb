class CreateOrgMappings < ActiveRecord::Migration
  def change
    create_table :org_mappings do |t|
      t.integer :child_organization_id
      t.integer :parent_organization_id
    end
  end
end

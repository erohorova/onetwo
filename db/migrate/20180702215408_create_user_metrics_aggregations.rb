class CreateUserMetricsAggregations < ActiveRecord::Migration
  def change
    create_table :user_metrics_aggregations do |t|
      t.integer :smartbites_commented_count, null: false
      t.integer :smartbites_completed_count, null: false
      t.integer :smartbites_consumed_count, null: false
      t.integer :smartbites_created_count, null: false
      t.integer :smartbites_liked_count, null: false
      t.integer :time_spent_minutes, null: false
      t.integer :total_smartbite_score, null: false
      t.integer :total_user_score, null: false
      t.references :user, index: true, foreign_key: false, null: false
      t.references :organization, index: true, foreign_key: false, null: false
      t.integer :start_time, null: false
      t.integer :end_time, null: false
      t.timestamps null: false
    end
  end
end
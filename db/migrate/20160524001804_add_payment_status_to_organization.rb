class AddPaymentStatusToOrganization < ActiveRecord::Migration
  def change
    add_column :organizations, :payment_status, :string, default: 'active'
  end
end

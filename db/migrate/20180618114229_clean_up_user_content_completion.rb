class CleanUpUserContentCompletion < ActiveRecord::Migration
  def change
    #Cleanup logic to delete duplicate records for user_content_completions. Don't want
    #lag in deletion of duplicates and addition of index.
    ucc = UserContentCompletion.group([:user_id, :completable_id]).having("count(*) > 1")
    ucc.each do |u|
      user_completion = UserContentCompletion.where(user_id: u.user_id, completable_id: u.completable_id).to_a
      keep_record = user_completion.pop
      user_completion.each{|r| r.destroy}
    end
  end
end

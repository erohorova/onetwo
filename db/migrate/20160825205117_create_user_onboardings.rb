class CreateUserOnboardings < ActiveRecord::Migration
  def change
    create_table :user_onboardings do |t|
      t.references :user, index: true, foreign_key: true
      t.integer :current_step, default: 1
      t.string :status
      t.timestamp :completed_at
      t.timestamps null: false
    end
  end
end

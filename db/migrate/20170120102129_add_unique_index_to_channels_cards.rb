class AddUniqueIndexToChannelsCards < ActiveRecord::Migration
  def up
    # delete duplicate channels cards entries
    ChannelsCard.group([:channel_id, :card_id]).having("count(*) > 1").select("channel_id, card_id, min(id) as min_id").each do |cc|
      ChannelsCard.where(channel_id: cc.channel_id, card_id: cc.card_id).where.not(id: cc.min_id).destroy_all
    end

    remove_index :channels_cards, column: [:channel_id, :card_id]
    remove_index :channels_cards, column: [:card_id, :channel_id]

    add_index :channels_cards, [:channel_id, :card_id], unique: true
    add_index :channels_cards, [:card_id, :channel_id], unique: true
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end

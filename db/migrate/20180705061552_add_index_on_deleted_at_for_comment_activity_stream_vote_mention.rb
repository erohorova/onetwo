class AddIndexOnDeletedAtForCommentActivityStreamVoteMention < ActiveRecord::Migration
  def change
    add_index :comments, :deleted_at
    add_index :activity_streams, :deleted_at
    add_index :votes, :deleted_at
    add_index :mentions, :deleted_at
  end
end

class FixVarcharLimitForGoals < ActiveRecord::Migration
  def change
    change_column :goals, :achievable_type, :string, limit: 191
  end
end

class AddIndexOnTopicRequests < ActiveRecord::Migration
  def change
    add_index :topic_requests, [:label, :organization_id], unique: true
  end
end

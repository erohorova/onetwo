class AddIsPrivateToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :is_private, :boolean, default: false
  end
end

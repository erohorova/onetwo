class RemovePaymentStatusFromOrganization < ActiveRecord::Migration
  def change
    remove_column :organizations, :payment_status
  end
end

class ChangeDefaultValueLanguageOnGroups < ActiveRecord::Migration
  def up
    change_column :groups, :language, :string, default: 'en'
  end

  def down
    change_column :groups, :language, :string, default: nil
  end
end

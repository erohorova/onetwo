class CreateWalletTransactions < ActiveRecord::Migration
  def change
    create_table :wallet_transactions do |t|
      t.references :order, index: true, foreign_key: true, null: false
      t.references :user, index: true, foreign_key: true, null: false
      t.integer :type, default: 0, null: false
      t.text :gateway_data
      t.decimal :amount, precision: 8, scale: 2, null: false
      t.integer :payment_state, default: 0, null: false

      t.timestamps null: false
    end
  end
end

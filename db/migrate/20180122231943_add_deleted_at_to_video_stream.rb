class AddDeletedAtToVideoStream < ActiveRecord::Migration
  def change
    add_column :video_streams, :deleted_at, :datetime
    add_index :video_streams, :deleted_at
  end
end

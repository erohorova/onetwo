class CreateOrgSubscriptions < ActiveRecord::Migration
  def change
    create_table :org_subscriptions do |t|
      t.string :name, null: false
      t.text :description, null: false
      t.date :start_date, null: false
      t.date :end_date, null: false
      t.integer :organization_id, index: true, foreign_key: true, null: false
      t.string :sku, limit: 200, null: false

      t.timestamps null: false
    end
  end
end

class AddOrganizationIdToUserTopicLevelMetrics < ActiveRecord::Migration
  def change
    add_column :user_topic_level_metrics, :organization_id, :integer

    add_index :user_topic_level_metrics, :organization_id
  end
end

class AddIsJobFunctionToJobRoles < ActiveRecord::Migration
  def change
    add_column :job_roles, :is_job_function, :boolean, default: false
  end
end

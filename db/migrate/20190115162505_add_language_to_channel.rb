class AddLanguageToChannel < ActiveRecord::Migration
  def change
    add_column :channels, :language, :text
  end
end

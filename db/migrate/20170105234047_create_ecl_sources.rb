class CreateEclSources < ActiveRecord::Migration
  def change
    create_table :ecl_sources do |t|
      t.integer :channel_id
      t.string :ecl_source_id

      t.timestamps null: false
    end
  end
end

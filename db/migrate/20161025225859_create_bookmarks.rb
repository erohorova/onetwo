class CreateBookmarks < ActiveRecord::Migration
  def change
    create_table :bookmarks do |t|
      t.references :user, index: true, foreign_key: true
      t.integer :bookmarkable_id
      t.string :bookmarkable_type, limit: 191

      t.timestamps null: false
    end
    add_index :bookmarks, [:bookmarkable_id, :bookmarkable_type]
    add_index :bookmarks, [:user_id, :bookmarkable_type] #For uniqueness constraint on bookmarks
  end
end

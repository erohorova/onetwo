class AddIndexToCardIdToTeamsCards < ActiveRecord::Migration
  def change
    add_index :teams_cards, :card_id
  end
end

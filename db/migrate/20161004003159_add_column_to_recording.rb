class AddColumnToRecording < ActiveRecord::Migration
  def change
    add_column :recordings, :default, :boolean, default: false
  end
end

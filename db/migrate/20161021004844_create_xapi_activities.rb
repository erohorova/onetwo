class CreateXapiActivities < ActiveRecord::Migration
  def change
    create_table :xapi_activities do |t|
      t.integer :actor_id, index: true
      t.string  :actor_email
      t.integer :object_id
      t.string :object_type
      t.string :object_definition
      t.references :organization, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end

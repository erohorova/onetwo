class CreateLearningQueueItems < ActiveRecord::Migration
  def change
    create_table :learning_queue_items do |t|
      t.string :display_type
      t.string :state, limit: 191

      t.string :queueable_type, limit: 191
      t.integer :queueable_id

      t.integer :user_id, null: false

      t.timestamps
    end

    add_index :learning_queue_items, [:user_id, :state]
    add_index :learning_queue_items, [:queueable_type, :queueable_id]
  end
end

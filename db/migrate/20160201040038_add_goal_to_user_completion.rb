class AddGoalToUserCompletion < ActiveRecord::Migration
  def change
    add_reference :user_completions, :goal, index: true
  end
end

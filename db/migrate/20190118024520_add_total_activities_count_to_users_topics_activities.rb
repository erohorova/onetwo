class AddTotalActivitiesCountToUsersTopicsActivities < ActiveRecord::Migration
  def change
    add_column :users_topics_activities, :total_activities_count, :integer
  end
end

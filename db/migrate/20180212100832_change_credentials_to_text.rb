class ChangeCredentialsToText < ActiveRecord::Migration
  def change
    change_column :skills_users, :credential, :text
  end
end

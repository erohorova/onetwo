class AddEclIdIndex < ActiveRecord::Migration
  def up
    execute("ALTER TABLE cards ADD INDEX index_cards_on_ecl_id (ecl_id), ALGORITHM=INPLACE, LOCK=NONE;")
  end

  def down
    execute("DROP INDEX index_cards_on_ecl_id on cards;")
  end
end

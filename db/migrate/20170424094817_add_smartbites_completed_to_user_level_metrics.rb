class AddSmartbitesCompletedToUserLevelMetrics < ActiveRecord::Migration
  def change
    add_column :user_level_metrics, :smartbites_completed, :integer, default: 0
  end
end

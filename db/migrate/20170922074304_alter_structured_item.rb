class AlterStructuredItem < ActiveRecord::Migration
  def change
    remove_column :structured_items, :parent
    remove_column :structured_items, :to_id

    add_column :structured_items, :position, :integer
  end
end

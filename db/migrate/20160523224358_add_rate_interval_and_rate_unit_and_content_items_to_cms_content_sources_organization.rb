class AddRateIntervalAndRateUnitAndContentItemsToCmsContentSourcesOrganization < ActiveRecord::Migration
  def change
    add_column :cms_content_sources_organizations, :rate_interval, :integer
    add_column :cms_content_sources_organizations, :rate_unit, :string
    add_column :cms_content_sources_organizations, :content_items, :integer
  end
end

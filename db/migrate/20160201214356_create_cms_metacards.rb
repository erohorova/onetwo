class CreateCmsMetacards < ActiveRecord::Migration
  def change
    create_table :cms_metacards do |t|
      t.integer :cms_content_source_id
      t.integer :resource_id

      t.timestamps null: false
    end
  end
end

class AddOnlyAdminCanPostToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :only_admin_can_post, :boolean, default: false
  end
end

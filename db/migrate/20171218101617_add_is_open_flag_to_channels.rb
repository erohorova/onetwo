class AddIsOpenFlagToChannels < ActiveRecord::Migration
  def change
    add_column :channels, :is_open, :boolean, default: false
  end
end

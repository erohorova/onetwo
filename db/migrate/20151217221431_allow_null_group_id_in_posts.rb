class AllowNullGroupIdInPosts < ActiveRecord::Migration
  def change
    change_column :posts, :group_id, :integer, :null => true
  end
end

class CreateQuarters < ActiveRecord::Migration
  def change
    create_table :quarters do |t|
      t.integer :organization_id, null: false
      t.date :start_date, null: false
      t.date :end_date, null: false
      t.string :name

      t.timestamps null: false
    end
  end
end

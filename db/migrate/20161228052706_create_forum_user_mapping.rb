class CreateForumUserMapping < ActiveRecord::Migration
  def change
    create_table :forum_user_mappings do |t|
      t.string :item_type, limit: 191
      t.integer :item_id
      t.integer :www_user_id
      t.integer :org_user_id
      t.integer :organization_id
      t.string :table_name
    end
    add_index :forum_user_mappings, [:item_type, :item_id]
  end
end

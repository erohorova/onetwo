class AddAttrsToUserTeamAssignment < ActiveRecord::Migration
  def change
    add_column :user_team_assignments, :assignable_type, :string, limit: 191
    add_column :user_team_assignments, :assignable_id, :integer
    add_column :user_team_assignments, :assignor_id, :integer

    add_index :user_team_assignments, :assignable_type
    add_index :user_team_assignments, :assignable_id
  end
end

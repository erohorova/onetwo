class AddFieldValuesToUsersIntegrations < ActiveRecord::Migration
  def change
    add_column :integrations, :fields, :text
    add_column :users_integrations, :field_values, :text
  end
end

class AddIndexToUserBadges < ActiveRecord::Migration
  def change
    add_index :user_badges, :identifier, using: :btree
  end
end

class AddVerbToXapiActivity < ActiveRecord::Migration
  def change
    add_column :xapi_activities, :verb, :string, limit: 191
    add_column :xapi_activities, :verb_identifier, :string, limit: 191
  end
end

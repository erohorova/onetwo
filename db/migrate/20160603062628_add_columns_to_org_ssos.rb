class AddColumnsToOrgSsos < ActiveRecord::Migration
  def change
    add_column :org_ssos, :oauth_login_url, :string
    add_column :org_ssos, :oauth_authorize_url, :string
    add_column :org_ssos, :oauth_token_url, :string
    add_column :org_ssos, :oauth_user_info_url, :string
    add_column :org_ssos, :label_name, :string
    add_column :org_ssos, :oauth_client_id, :string
    add_column :org_ssos, :oauth_client_secret, :string
  end
  
end

class CreateMonthlyTopVideoStreams < ActiveRecord::Migration
  def change
    create_table :monthly_top_video_streams do |t|
      t.references :video_stream, index: true, foreign_key: true
      t.string :month_identifier, limit: 191
      t.integer :votes_count, default: 0
      t.boolean :winner, default: false

      t.timestamps null: false
    end
  end
end

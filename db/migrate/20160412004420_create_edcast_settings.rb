class CreateEdcastSettings < ActiveRecord::Migration
  def change
    create_table :edcast_settings do |t|
      t.boolean :get, default: false
      t.timestamps null: false
    end
  end
end

class CardUserPermission < ActiveRecord::Migration
  def change
    create_table :card_user_permissions do |t|
      t.references :user, index: true, null: false, foreign_key: true
      t.references :card, index: true, null: false, foreign_key: true
      t.boolean :show, default: true
 
      t.timestamps
    end
  end
end

class AddSkillLevelToSkillsUser < ActiveRecord::Migration
  def change
    add_column :skills_users, :skill_level, :string
  end
end

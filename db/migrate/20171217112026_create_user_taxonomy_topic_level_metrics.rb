class CreateUserTaxonomyTopicLevelMetrics < ActiveRecord::Migration
  def change
    create_table :user_taxonomy_topic_level_metrics do |t|
      t.references :user, index: true, foreign_key: true
      t.references :organization, index: true, foreign_key: true
      t.integer :smartbites_score,default: 0
      t.integer :smartbites_created, default: 0
      t.integer :smartbites_consumed ,default: 0
      t.integer :time_spent, default: 0
      t.string :period
      t.integer :offset
      t.string :taxonomy_label

      t.timestamps null: false
    end
  end
end

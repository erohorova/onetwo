class AddProviderFieldsToCard < ActiveRecord::Migration
  def change
    add_column :cards, :provider, :string
    add_column :cards, :provider_image, :string
  end
end

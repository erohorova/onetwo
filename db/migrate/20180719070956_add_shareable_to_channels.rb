class AddShareableToChannels < ActiveRecord::Migration
  def change
    add_column :channels, :shareable, :boolean
  end
end

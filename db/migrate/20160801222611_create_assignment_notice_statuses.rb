class CreateAssignmentNoticeStatuses < ActiveRecord::Migration
  def change
    create_table :assignment_notice_statuses do |t|
      t.references :user, index: true, foreign_key: true
      t.timestamp :notice_dismissed_at, default: 0
      t.timestamps null: false
    end
  end
end

class ChannelSlugsScoped < ActiveRecord::Migration
  def change
    remove_index :channels, [:slug]
    add_index    :channels, [:slug, :organization_id], unique: true
  end
end

class AddTimestampsToUserImportStatus < ActiveRecord::Migration
  def change
    add_column :user_import_statuses, :created_at, :datetime
    add_column :user_import_statuses, :updated_at, :datetime
  end
end

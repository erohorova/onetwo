class ExtendUsersForUsername < ActiveRecord::Migration
  def change
    add_column :users, :federated_identifier, :string, length: 50, index: true
  end
end

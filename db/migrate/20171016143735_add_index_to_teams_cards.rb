class AddIndexToTeamsCards < ActiveRecord::Migration
  def change
    change_column :teams_cards, :type, :string, limit: 10
    unless index_exists?(:teams_cards, [:team_id, :card_id])
      add_index :teams_cards, [:team_id, :card_id], name: :index_teams_cards_on_team_and_card
    end
    unless index_exists?(:teams_cards, [:team_id, :type])
      add_index :teams_cards, [:team_id, :type], name: :index_teams_cards_on_team_and_teams_cards_type
    end
  end
end

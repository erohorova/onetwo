class AddTruncatedToBackgroundJobs < ActiveRecord::Migration
  def change
    add_column :background_jobs, :truncated, :boolean, default: false
  end
end

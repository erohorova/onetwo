class AddIsOfficialToVideoStreams < ActiveRecord::Migration
  def change
    add_column :video_streams, :is_official, :boolean, default: false
  end
end

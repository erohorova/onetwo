class AddRawInfoToUsersExternalCourses < ActiveRecord::Migration
  def change
    add_column :users_external_courses, :raw_info, :text
  end
end

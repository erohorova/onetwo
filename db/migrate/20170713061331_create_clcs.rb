class CreateClcs < ActiveRecord::Migration
  def change
    create_table :clcs do |t|
      t.integer :entity_id
      t.string :entity_type, limit: 20
      t.date :from_date
      t.date :to_date
      t.integer :target_score
      t.references :organization, index: true, foreign_key: true
      t.string :target_steps, limit: 100
      t.boolean :is_active, default: true
      t.datetime :deleted_at

      t.timestamps null: false
    end

    add_index :clcs, [:entity_id, :entity_type], using: :btree
  end
end

class AddAutoAssignContentToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :auto_assign_content, :boolean, default: false
  end
end

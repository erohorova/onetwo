class AddIndexesToAssignmentModels < ActiveRecord::Migration
  def change
    add_index :assignments, :user_id
    add_index :assignments, :assignor_id
    add_index :assignments, [:assignable_type, :assignable_id]

    add_index :team_assignments, :assignment_id
    add_index :team_assignments, :team_id
  end
end

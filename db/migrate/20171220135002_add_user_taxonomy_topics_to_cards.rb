class AddUserTaxonomyTopicsToCards < ActiveRecord::Migration
  def change
    add_column :cards, :user_taxonomy_topics, :text
  end
end

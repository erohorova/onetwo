class CreateEmailTemplates < ActiveRecord::Migration
  def change
    create_table :email_templates do |t|
      t.text :content, null: false
      t.integer :creator_id, index: true, null: true
      t.references :organization, index: true, foreign_key: true, null: false
      t.string :state, null: false, default: 'draft'
      t.string :language, null: false
      t.string :title, index: true, null: false, limit: 191
      t.boolean :is_active, default: false
      t.integer :default_id, index: true, null: true

      t.timestamps null: false
    end
    add_foreign_key :email_templates, :users, column: :creator_id
    add_foreign_key :email_templates, :email_templates, column: :default_id, primary_key: :id
  end
end

class AddDeletedAtToVotes < ActiveRecord::Migration
  def change
    add_column :votes, :deleted_at, :datetime
  end
end

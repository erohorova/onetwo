class AddFaviconToOrganizations < ActiveRecord::Migration
  def change
    add_attachment :organizations, :favicon
  end
end

class AddCurateUgcToChannels < ActiveRecord::Migration
  def change
    add_column :channels, :curate_ugc, :boolean, :default => false
  end
end

class UpdateLimitOfCompanyColumn < ActiveRecord::Migration
   def up
    change_column :user_profiles, :company, :string, limit: 150
  end

  def down
    change_column :user_profiles, :company, :string, limit: 30
  end
end

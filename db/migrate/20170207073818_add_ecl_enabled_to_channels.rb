class AddEclEnabledToChannels < ActiveRecord::Migration
  def change
    add_column :channels, :ecl_enabled, :boolean, default: false
  end
end

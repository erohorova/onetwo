class CreateCmsContentSourcesIndustries < ActiveRecord::Migration
  def change
    create_table :cms_content_sources_industries do |t|
      t.integer :cms_content_source_id, null: false
      t.integer :industry_id, null: false

      t.timestamps null: false
    end
  end
end

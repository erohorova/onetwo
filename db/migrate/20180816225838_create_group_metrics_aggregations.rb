class CreateGroupMetricsAggregations < ActiveRecord::Migration
  def change
    create_table :group_metrics_aggregations do |t|
      t.integer :smartbites_created, null: false
      t.references :team, index: true, foreign_key: false, null: false
      t.references :organization, index: true, foreign_key: false, null: false
      t.integer :smartbites_consumed, null: false
      t.decimal :average_session, null: false
      t.integer :total_users, null: false
      t.integer :active_users, null: false
      t.integer :new_users, null: false
      t.integer :engagement_index, null: false
      t.integer :num_comments, null: false
      t.integer :num_likes, null: false
      t.integer :start_time, null: false
      t.integer :end_time, null: false
      t.timestamps null: false
    end
  end
end

class CreateCmsJudgements < ActiveRecord::Migration
  def change
    create_table :cms_judgements do |t|
      t.references :user, index: true
      t.integer :content_item_id
      t.string :content_item_type, limit: 191
      t.string :action, limit: 191

      t.timestamps null: false
    end

    add_index :cms_judgements, [:content_item_id, :content_item_type, :action], name: "index_cms_judgements_on_content_item_action"
  end
end

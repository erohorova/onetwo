class CreateClcChannelsRecords < ActiveRecord::Migration
  def change
    create_table :clc_channels_records, id: false do |t|
      t.timestamp :created_at
      t.references :user, index: true, foreign_key: true
      t.integer :score
      t.references :card, index: true, foreign_key: true
      t.references :channel, index: true, foreign_key: true
    end
    add_index :clc_channels_records, [:user_id, :card_id, :channel_id], using: :btree, unique: true
  end
end

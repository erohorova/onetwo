class AddDescriptionToRolePermissions < ActiveRecord::Migration
  def change
    add_column :role_permissions, :description, :string
  end
end

class CreateDismissedContents < ActiveRecord::Migration
  def change
    create_table :dismissed_contents do |t|
      t.integer :content_id, index: true
      t.string :content_type, limit: 191, index: true
      t.references :user, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end

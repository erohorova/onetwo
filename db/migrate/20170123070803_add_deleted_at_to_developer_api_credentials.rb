class AddDeletedAtToDeveloperApiCredentials < ActiveRecord::Migration
  def change
    add_column :developer_api_credentials, :deleted_at, :datetime
    add_column :developer_api_credentials, :created_at, :datetime
    add_column :developer_api_credentials, :updated_at, :datetime
  end
end

class AddStructuredItems < ActiveRecord::Migration
  def change
    create_table :structured_items do |t|
      t.references :structure, foreign_key: true, index: true, null: false

      t.integer :entity_id, null: false
      t.string :entity_type, limit: 20, null: false

      t.boolean :enabled, default: false
      t.boolean :parent, default: false
      t.integer :to_id

      t.timestamps null: false
    end

    add_index :structured_items, [:structure_id, :entity_id, :entity_type], name: 'index_sitems_on_structure_parent'
  end
end

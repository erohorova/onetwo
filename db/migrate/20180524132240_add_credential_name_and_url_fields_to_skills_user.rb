class AddCredentialNameAndUrlFieldsToSkillsUser < ActiveRecord::Migration
  def change
    add_column :skills_users, :credential_name, :string
    add_column :skills_users, :credential_url, :string
  end
end

class ChangeStringCharacterLengthToSupportEmojis < ActiveRecord::Migration

  def utf8_mb4port table_name, column_name
    change_column table_name, column_name, :string, :limit => 191
  end

  def change
    to_be_migrated = {
      :access_logs => [:platform],
      :approvals => [:approvable_type],
      :background_workers => [:name],
      :branchmetrics_invitations => [:token],
      :card_pack_relations => [:from_type],
      :card_queries => [:q, :normalized_q],
      :cards => [:slug, :client_item_id],
      :cards_configs => [:card_type],
      :cards_default_signatures => [:role],
      :cards_items => [:card_id, :client_item_id],
      :cards_users => [:state],
      :channels => [:slug],
      :channels_users => [:as_type],
      :clients_users => [:client_user_id],
      :comments => [:commentable_type, :client_item_id],
      :configurations => [:name],
      :conversations => [:interested_type, :channel],
      :credentials => [:api_key],
      :delayable_joins => [:delayable_type],
      :delayed_jobs => [:job_class, :job_id],
      :emails => [:email],
      :follows => [:followable_type],
      :groups => [:client_resource_id],
      :groups_users => [:as_type],
      :identity_providers => [:type],
      :locations_locatables => [:locatable_type],
      :mentions => [:mentionable_type],
      :notifications => [:notifiable_type],
      :oauth_access_grants => [:token],
      :oauth_access_tokens => [:token, :refresh_token],
      :organizations => [:slug],
      :promotions => [:promotable_type],
      :pubnub_channels => [:as_type, :item_type],
      :quiz_question_attempts => [:quiz_question_type],
      :quiz_question_options => [:quiz_question_type],
      :quiz_question_stats => [:quiz_question_type],
      :reports => [:reportable_type],
      :shortened_urls => [:url],
      :solicitation_results => [:solicitable_type],
      :taggings => [:taggable_type],
      :tags => [:name],
      :user_connections => [:network],
      :user_feed_caches => [:feed_request_id],
      :user_preferences => [:key, :value, :preferenceable_type],
      :user_signatures => [:name, :user_role],
      :users => [:email, :handle, :reset_password_token, :organization_role],
      :video_streams => [:status, :slug, :uuid],
      :votes => [:votable_type],
      :friendly_id_slugs => [:slug, :sluggable_type, :scope]
    }
    to_be_migrated.each do |k,v|
      v.each do |vi|
        utf8_mb4port k, vi
      end
    end
  end
end

class ExtendIdentityProviders < ActiveRecord::Migration
  def change
    add_column :identity_providers, :refresh_token, :string, length: 100
  end
end

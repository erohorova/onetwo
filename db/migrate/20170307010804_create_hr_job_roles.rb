class CreateHrJobRoles < ActiveRecord::Migration
  def change
    create_table :hr_job_roles do |t|
      t.integer :organization_id, null: false

      t.string :name
      t.string :description

      t.timestamps
    end
  end
end

class RemoveIndicesFromFollowsTable < ActiveRecord::Migration
  def change
    remove_index :follows, :deleted_at
    remove_index :follows, :followable_type
  end
end

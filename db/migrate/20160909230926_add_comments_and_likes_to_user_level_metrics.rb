class AddCommentsAndLikesToUserLevelMetrics < ActiveRecord::Migration
  def change
    add_column :user_level_metrics, :smartbites_liked, :integer, default: 0
    add_column :user_level_metrics, :smartbites_comments_count, :integer, default: 0
  end
end

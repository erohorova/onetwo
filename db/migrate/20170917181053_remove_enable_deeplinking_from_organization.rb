class RemoveEnableDeeplinkingFromOrganization < ActiveRecord::Migration
  def change
    remove_column :organizations, :enable_deeplinking, :boolean
  end
end

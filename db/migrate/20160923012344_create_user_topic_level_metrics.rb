class CreateUserTopicLevelMetrics < ActiveRecord::Migration
  def change
    create_table :user_topic_level_metrics do |t|

      t.integer :user_id, null: false
      t.integer :tag_id, null: false

      t.integer :smartbites_score, default: 0
      t.integer :smartbites_created, default: 0
      t.integer :smartbites_consumed, default: 0
      t.integer :time_spent, default: 0

      t.string :period, limit: 101
      t.integer :offset

      t.timestamps
    end
  end
end

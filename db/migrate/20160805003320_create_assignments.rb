class CreateAssignments < ActiveRecord::Migration
  def change
    create_table :assignments do |t|
      t.integer :user_id
      t.string :title
      t.string :assignable_type, limit: 191
      t.integer :assignable_id
      t.integer :assignor_id

      t.timestamps null: false
    end
  end
end

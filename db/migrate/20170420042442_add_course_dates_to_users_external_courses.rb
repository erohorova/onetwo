class AddCourseDatesToUsersExternalCourses < ActiveRecord::Migration
  def change
    add_column :users_external_courses, :due_date, :date
    add_column :users_external_courses, :assigned_date, :date
  end
end

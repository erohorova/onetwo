class CreateUserCompletions < ActiveRecord::Migration
  def change
    create_table :user_completions do |t|
      t.references :user, index: true, foreign_key: true
      t.integer :completable_id
      t.string :completable_type
      t.timestamps null: false
    end
  end
end

class ChangeSlugIndexOnCards < ActiveRecord::Migration
  def change
    remove_index :cards, [:slug]
    add_index :cards, [:organization_id, :slug]
  end
end

class AddBookmarksCountToCards < ActiveRecord::Migration
  def change
    add_column :cards, :bookmarks_count, :integer
  end
end

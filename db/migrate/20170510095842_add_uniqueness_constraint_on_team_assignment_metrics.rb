class AddUniquenessConstraintOnTeamAssignmentMetrics < ActiveRecord::Migration
  def change
    TeamAssignmentMetric.group([:team_id, :assignor_id, :card_id]).having("count(*) > 1").select("team_id, assignor_id, card_id, min(id) as min_id").each do |tam|
      TeamAssignmentMetric.where(team_id: tam.team_id, assignor_id: tam.assignor_id, card_id: tam.card_id).where.not(id: tam.min_id).destroy_all
    end

    add_index :team_assignment_metrics, [:team_id, :assignor_id, :card_id], unique: true, name: "index_team_assignment_metrics_on_assignor"
  end
end

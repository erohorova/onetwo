class AddOauthScopesToOrgSsos < ActiveRecord::Migration
  def change
    add_column :org_ssos, :oauth_scopes, :string
  end
end

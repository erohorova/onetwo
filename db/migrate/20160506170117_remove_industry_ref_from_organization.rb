class RemoveIndustryRefFromOrganization < ActiveRecord::Migration
  def change
    remove_reference :organizations, :industry, index: true, foreign_key: true
  end
end

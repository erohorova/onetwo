class CreatePwcRecord < ActiveRecord::Migration
  def change
    create_table :pwc_records do |t|
      t.text  :skill
      t.integer  :level
      t.integer  :user_id, index: true
      t.string  :abbr, limit: 5

      t.timestamps null: false
    end
  end
end

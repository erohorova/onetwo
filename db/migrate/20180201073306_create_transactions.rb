class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.integer :order_id, index: true, foreign_key: true, null: false
      t.string :gateway
      t.string :gateway_id
      t.text :gateway_data
      t.decimal :amount, precision: 8, scale: 2, null: false
      t.string :currency, default: 'USD', limit: 10, null: false
      t.integer :payment_state, default: 0, null: false
      t.integer :user_id, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end

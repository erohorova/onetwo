class AddAutoFollowToChannels < ActiveRecord::Migration
  def change
    add_column :channels, :auto_follow, :boolean, default: false
  end
end

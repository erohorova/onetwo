class AddDashboardInfoToUserProfile < ActiveRecord::Migration
  def change
    add_column :user_profiles, :dashboard_info, :text
  end
end

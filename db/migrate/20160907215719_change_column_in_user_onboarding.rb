class ChangeColumnInUserOnboarding < ActiveRecord::Migration
  def change
    change_column :user_onboardings, :status, :string , limit: 191
  end
end

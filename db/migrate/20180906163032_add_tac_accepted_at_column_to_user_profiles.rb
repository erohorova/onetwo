class AddTacAcceptedAtColumnToUserProfiles < ActiveRecord::Migration
  def change
    add_column :user_profiles, :tac_accepted_at, :datetime
  end
end

class AddIndexToBadgeAndBadgingAndUserBadgeTables < ActiveRecord::Migration
  def change
    # changing column with limit,
    # error: Specified key was too long
    # this is an irreversible migration

    change_column :badges, :type, :string, limit: 20
    change_column :badgings, :type, :string, limit: 20
    add_index :badges, [:organization_id, :type]
    add_index :user_badges, :user_id
    add_index :user_badges, [:badging_id, :user_id], unique: true
    add_index :badgings, [:id, :type]
    add_index :badges, [:id, :type]
  end
end

class CreateBadges < ActiveRecord::Migration
  def change
    create_table :badges do |t|
      t.integer :organization_id
      t.string :type

      t.timestamps null: false
    end
  end
end

class CreateCmsContentObjects < ActiveRecord::Migration
  def change
    create_table :cms_content_objects do |t|
      t.references :cms_content_source, index: true, foreign_key: true
      t.string :link, null: false, limit:2100
      t.string :guid, null: false, limit:2100
      t.boolean :extracted, default: false
      t.timestamps null: false
    end
  end

end

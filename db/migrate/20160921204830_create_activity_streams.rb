class CreateActivityStreams < ActiveRecord::Migration
  def change
    create_table :activity_streams do |t|
      t.references :user, index: true, foreign_key: true
      t.references :organization, index: true, foreign_key: true
      t.string :action, limit: 191
      t.string :streamable_type, limit: 191
      t.integer :streamable_id
      t.timestamps null: false
    end
    add_index :activity_streams, [:streamable_type, :streamable_id ], using: :btree
  end
end

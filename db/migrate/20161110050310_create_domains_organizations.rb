class CreateDomainsOrganizations < ActiveRecord::Migration
  def change
    create_table :domains_organizations do |t|
      t.references :domain, index: true, foreign_key: true
      t.references :organization, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

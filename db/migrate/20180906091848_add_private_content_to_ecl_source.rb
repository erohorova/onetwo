class AddPrivateContentToEclSource < ActiveRecord::Migration
  def change
    add_column :ecl_sources, :private_content, :boolean
  end
end

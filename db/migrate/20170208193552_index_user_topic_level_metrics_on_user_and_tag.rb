class IndexUserTopicLevelMetricsOnUserAndTag < ActiveRecord::Migration
  def change
    add_index :user_topic_level_metrics, [:user_id, :tag_id], name: 'indx_user_topic_level_metrics_user_id_tag_id'
  end
end

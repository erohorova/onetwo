class CreateOrganizationProfiles < ActiveRecord::Migration
  def change
    create_table :organization_profiles do |t|
      t.references :organization, index: true, foreign_key: true
      t.boolean :member_pay, default: false

      t.timestamps null: false
    end
  end
end

# This migration and AddTransactionIdColumnToVersions provide the necessary
# schema for tracking associations.
class CreateVersionAssociations < ActiveRecord::Migration
  def self.up
    create_table :version_associations do |t|
      t.integer  :version_id
      t.string   :foreign_key_name, null: false, limit: 50
      t.integer  :foreign_key_id
    end

  end

  def self.down
    drop_table :version_associations
  end
end

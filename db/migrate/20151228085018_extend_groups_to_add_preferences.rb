class ExtendGroupsToAddPreferences < ActiveRecord::Migration
  def change
    [ :connect_to_social_mediums,
      :enable_mobile_app_download_buttons,
      :share_to_social_mediums].each do |column|
      add_column :groups, column, :boolean, default: true
    end
  end
end

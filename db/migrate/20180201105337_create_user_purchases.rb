class CreateUserPurchases < ActiveRecord::Migration
  def change
    create_table :user_purchases do |t|
      t.integer :transaction_id, foreign_key: true, null: false
      t.integer :order_id, foreign_key: true, null: false
      t.integer :user_id, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end

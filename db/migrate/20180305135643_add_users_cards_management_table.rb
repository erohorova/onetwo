class AddUsersCardsManagementTable < ActiveRecord::Migration
  def change
    create_table :users_cards_managements do |t|
      t.references :user, index: true, foreign_key: true, null: false
      t.references :card, index: true, foreign_key: true, null: false
      t.string :action
      t.integer :target_id

      t.timestamps null: false
    end
    add_index(:users_cards_managements, [:user_id, :card_id, :target_id], name: 'users_cards_managements_entries', unique: false)
  end
end

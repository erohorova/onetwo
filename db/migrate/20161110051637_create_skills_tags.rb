class CreateSkillsTags < ActiveRecord::Migration
  def change
    create_table :skills_tags do |t|
      t.references :skill, index: true
      t.references :tag, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_foreign_key :skills_tags, :tags, column: :skill_id
  end
end

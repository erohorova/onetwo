class CreateWalletTransactionAudits < ActiveRecord::Migration
  def change
    create_table :wallet_transaction_audits do |t|
      t.integer :wallet_transaction_id, index: true, foreign_key: true, null: false
      t.text :order_data, null: false
      t.text :orderable_data, null: false
      t.text :user_data, null: false
      t.text :wallet_transaction_data, null: false

      t.timestamps null: false
    end
  end
end

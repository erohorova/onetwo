class AddNextRunAtToEclSources < ActiveRecord::Migration
  def change
    add_column :ecl_sources, :next_run_at, :datetime
    add_index :ecl_sources, :next_run_at
  end
end

class FixVarcharLimitForChannels < ActiveRecord::Migration
  def change
    change_column :channels, :description, :string, limit: 2000
  end
end
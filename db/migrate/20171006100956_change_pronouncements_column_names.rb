class ChangePronouncementsColumnNames < ActiveRecord::Migration
  def change
    if column_exists? :pronouncements, :announceable_id
      rename_column :pronouncements, :announceable_id, :scope_id
      rename_column :pronouncements, :announceable_type, :scope_type
    end
  end
end

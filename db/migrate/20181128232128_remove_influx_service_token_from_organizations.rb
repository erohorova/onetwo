class RemoveInfluxServiceTokenFromOrganizations < ActiveRecord::Migration
  def change
    remove_column :organizations, :influx_service_token
  end
end

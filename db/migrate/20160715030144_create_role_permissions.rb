class CreateRolePermissions < ActiveRecord::Migration
  def change
    create_table :role_permissions do |t|
      t.references :role
      t.references :permission

      t.timestamps null: false
    end
  end
end

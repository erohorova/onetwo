class ChangeColumnClcBadging < ActiveRecord::Migration
  def change
    change_column :badgings, :target_steps, :integer
  end
end

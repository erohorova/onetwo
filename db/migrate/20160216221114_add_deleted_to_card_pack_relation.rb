class AddDeletedToCardPackRelation < ActiveRecord::Migration
  def change
    add_column :card_pack_relations, :deleted, :boolean, default: false
  end
end

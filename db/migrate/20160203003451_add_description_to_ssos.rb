class AddDescriptionToSsos < ActiveRecord::Migration
  def change
    add_column :ssos, :description, :string
  end
end

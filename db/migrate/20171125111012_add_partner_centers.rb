class AddPartnerCenters < ActiveRecord::Migration
  def change
    create_table :partner_centers do |t|
      t.references :organization, foreign_key: true, index: true
      t.references :user, foreign_key: true

      t.boolean :enabled, default: false

      t.string :description
      t.string :embed_link

      t.string :integration, length: 20

      t.timestamps
    end
  end
end

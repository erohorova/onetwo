class AddUserProfiles < ActiveRecord::Migration
  def change
    create_table :user_profiles do |t|
      t.references :user, index: true

      t.text :expert_topics
      t.text :learning_topics

      t.timestamps null: false
    end
  end
end

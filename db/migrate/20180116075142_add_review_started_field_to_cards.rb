class AddReviewStartedFieldToCards < ActiveRecord::Migration
  def change
    add_column :cards, :review_started, :boolean
  end
end

class AddIndexToPrononcements < ActiveRecord::Migration
  def change
    change_column :pronouncements, :scope_id, :integer, limit: 5
    change_column :pronouncements, :scope_type, :string, limit: 20
    unless index_exists? :pronouncements, [:scope_id, :scope_type]
      add_index :pronouncements, [:scope_id, :scope_type], using: :btree
    end
  end
end

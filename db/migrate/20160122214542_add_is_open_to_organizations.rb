class AddIsOpenToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :is_open, :boolean, default: false
  end
end

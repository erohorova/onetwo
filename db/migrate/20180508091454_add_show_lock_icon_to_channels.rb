class AddShowLockIconToChannels < ActiveRecord::Migration
  def change
    add_column :channels, :show_lock_icon, :boolean
  end
end

class AddTimeInRoleToUsers < ActiveRecord::Migration
  def change
    add_column :users, :time_in_role, :string
  end
end

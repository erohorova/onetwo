class AddIsEveryoneTeamToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :is_everyone_team, :boolean, default: false
  end
end

class CreateGoals < ActiveRecord::Migration
  def change
    create_table :goals do |t|
      t.string :title
      t.references :user, index: true, foreign_key: true
      t.string :achievable_type
      t.integer :achievable_id
      t.string :time_limit

      t.timestamps null: false
    end
  end
end

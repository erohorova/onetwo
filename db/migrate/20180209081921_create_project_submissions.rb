class CreateProjectSubmissions < ActiveRecord::Migration
  def change
    create_table :project_submissions do |t|
      t.text :description
      t.integer :submitter_id, index: true, foreign_key: true, null: false
      t.integer :project_id, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end

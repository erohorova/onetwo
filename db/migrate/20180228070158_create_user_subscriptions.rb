class CreateUserSubscriptions < ActiveRecord::Migration
  def change
    create_table :user_subscriptions do |t|
      t.integer :org_subscription_id, foreign_key: true, null: false
      t.integer :organization_id, foreign_key: true, null: false
      t.integer :user_id, index: true, foreign_key: true, null: false
      t.integer :order_id, index: true, foreign_key: true, null: false
      t.decimal :amount, precision: 8, scale: 2, null: false
      t.string :currency, default: 'USD', limit: 10, null: false
      t.date :start_date, null: false
      t.date :end_date, null: false

      t.timestamps null: false
    end
  end
end

class AddIsRegistrableToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :is_registrable, :boolean, default: true
  end
end

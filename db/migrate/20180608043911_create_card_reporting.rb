class CreateCardReporting < ActiveRecord::Migration
  def change
    create_table :card_reportings do |t|
      t.references :card, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.string :reason
      t.datetime :deleted_at

      t.timestamps null: false
    end
  end
end

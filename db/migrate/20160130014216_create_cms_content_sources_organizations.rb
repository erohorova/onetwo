class CreateCmsContentSourcesOrganizations < ActiveRecord::Migration
  def change
    create_table :cms_content_sources_organizations do |t|
      t.integer :cms_content_source_id, null: false
      t.integer :organization_id, null: false
      t.timestamps null: false
    end

    add_index :cms_content_sources_organizations, [:cms_content_source_id, :organization_id], name: 'cms_cs_org_cs_org'
    add_index :cms_content_sources_organizations, [:organization_id, :cms_content_source_id], name: 'cms_cs_org_org_cs'
  end
end

class AddUsersCountToOrganization < ActiveRecord::Migration
  def change
    add_column :organizations, :users_count, :integer, default: 0
    Organization.pluck(:id).each do |org_id|
      Organization.reset_counters org_id, :users
    end
  end

end

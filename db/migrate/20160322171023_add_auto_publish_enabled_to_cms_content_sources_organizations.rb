class AddAutoPublishEnabledToCmsContentSourcesOrganizations < ActiveRecord::Migration
  def change
    add_column :cms_content_sources_organizations, :autopublish_enabled, :boolean, default: false
  end
end

class AddSourceNameToContentSourceIndustry < ActiveRecord::Migration
  def change
    add_column :cms_content_sources_industries, :display_source, :string
    add_column :cms_content_sources_industries, :name, :string
  end
end

class CreateScormDetails < ActiveRecord::Migration
  def change
    create_table :scorm_details do |t|
      t.references :card, index: true, foreign_key: true
      t.string :token
      t.string :status
      t.string :message

      t.timestamps null: false
    end
  end
end

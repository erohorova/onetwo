class AddReportingOnlyToPersonalizedEmail < ActiveRecord::Migration
  def change
    add_column :personalized_emails, :reporting_only, :boolean
  end
end

class RemovePersonalizedEmailsTableAndColumns < ActiveRecord::Migration
  def change
  	drop_table :personalized_emails
  	remove_column :organizations, :enable_personalized_email
    remove_column :organizations, :enable_personalized_email_reporting
  end
end

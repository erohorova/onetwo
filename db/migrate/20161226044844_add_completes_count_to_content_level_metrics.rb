class AddCompletesCountToContentLevelMetrics < ActiveRecord::Migration
  def change
    add_column :content_level_metrics, :completes_count, :integer, default: 0
  end
end

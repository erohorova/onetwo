class AddLanguageToUserProfiles < ActiveRecord::Migration
  def change
    add_column :user_profiles, :language, :string, :limit => 10
  end
end

class RemoveDuplicatePins < ActiveRecord::Migration
  def change
  	#Cleanup logic to delete duplicate records for pins.
    duplicate_pins = Pin.group([:pinnable_id, :object_id]).having("count(*) > 1")
    duplicate_pins.each do |pn|
      pins = Pin.where(pinnable_id: pn.pinnable_id, object_id: pn.object_id).to_a
      keep_record = pins.pop
      pins.each{|r| r.destroy}
    end
  end
end

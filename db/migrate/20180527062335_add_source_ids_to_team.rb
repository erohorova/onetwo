class AddSourceIdsToTeam < ActiveRecord::Migration
  def change
    add_column :teams, :source_ids, :text
  end
end

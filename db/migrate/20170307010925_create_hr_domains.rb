class CreateHrDomains < ActiveRecord::Migration
  def change
    create_table :hr_domains do |t|
      t.integer :organization_id, null: false

      t.string :name
      t.string :description

      t.boolean :is_root
      t.integer :parent_id

      t.timestamps
    end
  end
end

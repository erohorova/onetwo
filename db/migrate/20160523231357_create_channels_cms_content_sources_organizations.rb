class CreateChannelsCmsContentSourcesOrganizations < ActiveRecord::Migration
  def change
    create_table :cms_content_sources_organizations_channels do |t|
      t.integer :channel_id
      t.integer :cms_content_sources_organization_id
    end
  end
end

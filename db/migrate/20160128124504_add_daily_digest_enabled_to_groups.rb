class AddDailyDigestEnabledToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :daily_digest_enabled, :boolean, default: false
  end
end

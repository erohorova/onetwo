class AddColumnToNotificationEntries < ActiveRecord::Migration
  def change
    add_column :notification_entries, :additional_content, :text
  end
end

class DropEclTable < ActiveRecord::Migration
  def change
    drop_table :ecl_providers
    drop_table :ecl_config_sources
    drop_table :ecl_config_channels
    drop_table :ecl_content_configs
    drop_table :card_ecl_data
  end
end

class AddSourceToFileResource < ActiveRecord::Migration
  def change
    add_column :file_resources, :source, :string, default: 'user'
  end
end

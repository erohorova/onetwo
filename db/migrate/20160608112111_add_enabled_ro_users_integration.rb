class AddEnabledRoUsersIntegration < ActiveRecord::Migration
  def change
  	add_column :users_integrations, :enabled, :boolean, default: true
  end
end

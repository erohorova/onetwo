class AddMissingIndexesOnSkills < ActiveRecord::Migration
  def change
    add_index :skills, :name, unique: true, using: :btree, length: {name: 191}
    add_index :skills_users, [:skill_id, :user_id], unique: true, using: :btree
  end
end

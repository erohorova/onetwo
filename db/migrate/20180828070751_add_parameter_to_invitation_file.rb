class AddParameterToInvitationFile < ActiveRecord::Migration
  def change
    add_column :invitation_files, :parameter, :text
  end
end

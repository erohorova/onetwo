class AddPostableToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :postable_id, :integer
    add_column :posts, :postable_type, :string, limit: 191
    
    add_index :posts, [:postable_id, :postable_type]
  end
end

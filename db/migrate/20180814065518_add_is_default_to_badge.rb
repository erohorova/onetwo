class AddIsDefaultToBadge < ActiveRecord::Migration
  def change
    add_column :badges, :is_default, :boolean, default: false
  end
end

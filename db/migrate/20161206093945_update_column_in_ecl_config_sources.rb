class UpdateColumnInEclConfigSources < ActiveRecord::Migration
  def up
    change_column :ecl_config_sources, :ecl_content_source_id, :string
  end

  def down
    change_column :ecl_config_sources, :ecl_content_source_id, :integer
  end
end

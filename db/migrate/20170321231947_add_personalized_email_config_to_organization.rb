class AddPersonalizedEmailConfigToOrganization < ActiveRecord::Migration
  def change
    add_column :organizations, :enable_personalized_email, :boolean, default: false
    add_column :organizations, :enable_personalized_email_reporting, :boolean, default: false
  end
end

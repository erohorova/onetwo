class AddProgressToGoal < ActiveRecord::Migration
  def change
    add_column :goals, :progress, :float, default: 0.00
  end
end

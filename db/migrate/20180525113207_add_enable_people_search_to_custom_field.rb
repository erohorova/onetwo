class AddEnablePeopleSearchToCustomField < ActiveRecord::Migration
  def change
    add_column :custom_fields, :enable_people_search, :boolean
  end
end

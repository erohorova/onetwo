class CreateSsoTable < ActiveRecord::Migration
  def change
    create_table :ssos do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end

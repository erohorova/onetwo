class CreateRechargePrices < ActiveRecord::Migration
  def change
    create_table :recharge_prices do |t|
      t.references :recharge, index: true, foreign_key: true
      t.string :currency, default: 'USD', limit: 10
      t.decimal :amount, precision: 8, scale: 2

      t.timestamps null: false
    end
  end
end

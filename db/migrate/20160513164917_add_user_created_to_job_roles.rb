class AddUserCreatedToJobRoles < ActiveRecord::Migration
  def change
    add_column :job_roles, :user_created, :boolean, :default => false
  end
end

class RemoveDuplicatesFromProfiles < ActiveRecord::Migration
  def change
    # user_ids which have more than 1 profile
    duplicates_ids = Profile.group('user_id').having('COUNT(user_id) > 1').pluck(:user_id)

    duplicates_ids.each do |user_id|
      # get recent profile
      recent_profile = Profile.where(user_id: user_id)
        .order(created_at: :desc).first
      # remove all duplicated profiles except latest
      Profile.where(user_id: user_id).where.not(id: recent_profile.id).destroy_all
    end
  end
end

class AddUserIdToTeamsCards < ActiveRecord::Migration
  def change
    add_column :teams_cards, :user_id, :integer
    add_index :teams_cards, :user_id
  end
end

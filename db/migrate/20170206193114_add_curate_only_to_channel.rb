class AddCurateOnlyToChannel < ActiveRecord::Migration
  def change
    add_column :channels, :curate_only, :boolean, default: false
  end
end

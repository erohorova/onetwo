class AddIndexOnDeletedAtInCardReportings < ActiveRecord::Migration
  def change
    add_index :card_reportings, :deleted_at
  end
end

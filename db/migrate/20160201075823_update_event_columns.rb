class UpdateEventColumns < ActiveRecord::Migration
  def change
  	change_column :events, :hangout_url, :text
    rename_column :events, :hangout_url, :conference_url
    rename_column :events, :hangout_type, :conference_type

    add_column :events, :webex_conference_key, :string
    add_column :events, :host_url, :string
    add_column :events, :conference_email, :string
  end
end

class AddSourceToLearningQueueItems < ActiveRecord::Migration
  def change
    add_column :learning_queue_items, :source, :string
  end
end

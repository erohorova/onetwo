class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :orderable_id, index: true, foreign_key: true, null: false
      t.string :orderable_type, index: true, limit: 25 , null: false
      t.integer :user_id, foreign_key: true, null: false
      t.integer :organization_id, foreign_key: true, null: false
      t.integer :state, default: 0, null: false

      t.timestamps null: false
    end
  end
end

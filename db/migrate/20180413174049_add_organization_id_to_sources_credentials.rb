class AddOrganizationIdToSourcesCredentials < ActiveRecord::Migration
  def change
    add_column :sources_credentials, :organization_id, :integer
  end
end

class AddPublisherIdToTopicRequest < ActiveRecord::Migration
  def change
    add_column :topic_requests, :publisher_id, :integer
  end
end

class CreateMailerConfigTable < ActiveRecord::Migration
  def change
    create_table :mailer_configs do |t|
      t.integer :client_id
      t.string :address
      t.integer :port
      t.string :user_name
      t.string :password
      t.string :domain
      t.string :webhook_keys
      t.boolean :enable_starttls_auto, default: true
      t.string :authentication
      t.string :from_address
      t.string :from_name

      t.timestamps null: false
    end
  end
end

class AddAttrsToChannelEclSources < ActiveRecord::Migration
  def change
    add_column :ecl_sources, :rate_interval, :integer
    add_column :ecl_sources, :rate_unit, :string
    add_column :ecl_sources, :number_of_items, :integer
  end
end

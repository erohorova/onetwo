class CreateContentLevelMetrics < ActiveRecord::Migration
  def change
    create_table :content_level_metrics do |t|
      t.integer :views_count, default: 0
      t.integer :comments_count, default: 0
      t.integer :likes_count, default: 0

      t.string :content_type, limit: 191
      t.integer :content_id, null: false

      t.integer :organization_id, null: false

      t.integer :offset
      t.string :period, limit: 191
    end

    add_index :content_level_metrics, [:organization_id, :period, :offset], name: "indx_content_level_metrics_org_id"
    add_index :content_level_metrics, [:content_id, :content_type, :period, :offset], name: "indx_content_level_metrics_content", unique: true
  end
end

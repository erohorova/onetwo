class CreateGovernmentIdentifications < ActiveRecord::Migration
  def change
    create_table :hr_government_identifications do |t|
      t.integer :user_id, null: false, index: true
      t.string :government_id
      t.string :government_id_type
      t.string :country
      t.date :issued_date
      t.date :expiration_date
      t.date :verification_date
    end
  end
end

class AddSkuToRecharges < ActiveRecord::Migration
  def change
    add_column :recharges, :sku, :string, limit: 200, null: false
  end
end

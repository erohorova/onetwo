class CreateUserImportStatus < ActiveRecord::Migration
  def change
    create_table :user_import_statuses do |t|
      t.references :invitation_file, foreign_key: true
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :status
      t.text :channel_ids
      t.text :team_ids
      t.boolean :resent_email
      t.boolean :existing_user
      t.text :custom_fields
      t.text :additional_information
    end
  end
end

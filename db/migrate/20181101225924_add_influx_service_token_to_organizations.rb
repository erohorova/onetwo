class AddInfluxServiceTokenToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :influx_service_token, :string
    add_index :organizations, :influx_service_token
  end
end

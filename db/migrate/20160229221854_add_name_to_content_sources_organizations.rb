class AddNameToContentSourcesOrganizations < ActiveRecord::Migration
  def change
    add_column :cms_content_sources_organizations, :name, :string
  end
end

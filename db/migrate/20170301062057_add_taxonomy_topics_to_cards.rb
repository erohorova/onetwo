class AddTaxonomyTopicsToCards < ActiveRecord::Migration
  def change
    add_column :cards, :taxonomy_topics, :text
  end
end

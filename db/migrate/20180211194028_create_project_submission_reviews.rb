class CreateProjectSubmissionReviews < ActiveRecord::Migration
  def change
    create_table :project_submission_reviews do |t|
      t.text :description
      t.integer :status, null: false, default: 0
      t.integer :reviewer_id, index: true, foreign_key: true, null: false
      t.integer :project_submission_id, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end

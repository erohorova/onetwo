class CreateUserSessions < ActiveRecord::Migration
  def change
    create_table :user_sessions do |t|
      t.integer :user_id
      t.datetime :start_time
      t.datetime :end_time
      t.string :uuid, limit: 191
    end

    add_index :user_sessions, :user_id
    add_index :user_sessions, :uuid
  end
end

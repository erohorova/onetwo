class FixIndexOnDismissedContent < ActiveRecord::Migration
  def change
    remove_index :dismissed_contents, :content_id
    remove_index :dismissed_contents, :content_type
    add_index :dismissed_contents, [:content_id, :content_type]
  end
end

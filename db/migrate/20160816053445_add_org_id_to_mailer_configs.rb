class AddOrgIdToMailerConfigs < ActiveRecord::Migration
  def change
    add_column :mailer_configs, :organization_id, :integer, unique: true, null: false
  end
end

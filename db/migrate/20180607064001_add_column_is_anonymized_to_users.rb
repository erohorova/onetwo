class AddColumnIsAnonymizedToUsers < ActiveRecord::Migration
  def change
    add_column :users, :is_anonymized, :boolean
  end
end

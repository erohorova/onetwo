class CreateLeaps < ActiveRecord::Migration
  def change
    create_table :leaps do |t|
      t.references :card, index: true, foreign_key: true, null: false
      t.integer :pathway_id, index: true, null: true
      t.integer :correct_id, null: false
      t.integer :wrong_id, null: false

      t.timestamps null: false
    end
    add_index :leaps, [:card_id, :pathway_id]
  end
end

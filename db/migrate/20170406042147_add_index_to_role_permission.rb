class AddIndexToRolePermission < ActiveRecord::Migration
  def change
    add_index :role_permissions,  :role_id
    add_index :role_permissions,  :name
  end
end

class AddStateToVideoStreams < ActiveRecord::Migration
  def change
    add_column :video_streams, :state, :string, limit: 191
  end
end

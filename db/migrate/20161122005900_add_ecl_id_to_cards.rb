class AddEclIdToCards < ActiveRecord::Migration
  def change
    add_column :cards, :ecl_id, :string, limit: 191
    add_index :cards, [:organization_id, :ecl_id], unique: true
  end
end

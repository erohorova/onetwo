class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.references :user, index: true
      t.references :organization, index: true
      t.string :external_id
      t.string :employee_type
      t.string :business_unit
      t.string :job_title
      t.integer :job_id
      t.string :department
      t.string :location
      t.string :country
      t.string :manager_name
      t.string :manager_external_id
      t.string :manager_internal_id
      t.string :uid

      t.timestamps null: false
    end
  end
end

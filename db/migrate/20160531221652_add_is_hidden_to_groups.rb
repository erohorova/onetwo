class AddIsHiddenToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :is_hidden, :boolean, default: false
  end
end

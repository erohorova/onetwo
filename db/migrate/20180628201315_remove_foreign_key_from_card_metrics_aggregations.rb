class RemoveForeignKeyFromCardMetricsAggregations < ActiveRecord::Migration
  def change
    remove_foreign_key :card_metrics_aggregations, :organization
    remove_foreign_key :card_metrics_aggregations, :card
  end
end

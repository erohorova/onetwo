class CreateMdpUserCards < ActiveRecord::Migration
  def change
    create_table :mdp_user_cards do |t|
      t.integer :card_id, null: false
      t.integer :user_id, null: false
      t.text :form_details

      t.timestamps null: false
    end
    add_index :mdp_user_cards, [:card_id, :user_id], unique: true
  end
end

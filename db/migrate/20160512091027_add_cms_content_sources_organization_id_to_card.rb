class AddCmsContentSourcesOrganizationIdToCard < ActiveRecord::Migration
  def change
    add_column :cards, :cms_content_sources_organization_id, :integer
    add_index :cards, :cms_content_sources_organization_id
  end
end

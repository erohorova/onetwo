class CreateClcProgresses < ActiveRecord::Migration
  def change
    create_table :clc_progresses do |t|
      t.references :user, index: true, foreign_key: true
      t.references :clc, index: true, foreign_key: true
      t.integer :clc_score

      t.timestamps null: false
    end
    add_index :clc_progresses, [:user_id, :clc_id], using: :btree, unique: true
  end
end

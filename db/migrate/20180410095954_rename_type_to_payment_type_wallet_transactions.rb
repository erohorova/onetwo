class RenameTypeToPaymentTypeWalletTransactions < ActiveRecord::Migration
  def change
    rename_column :wallet_transactions, :type, :payment_type
  end
end

class CreateUserBadges < ActiveRecord::Migration
  def change
    create_table :user_badges do |t|
      t.integer :badging_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end

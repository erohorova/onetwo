class RemoveAmountAndCurrencyColumnFromRecharges < ActiveRecord::Migration
  def change
    remove_column :recharges, :amount
    remove_column :recharges, :currency
  end
end

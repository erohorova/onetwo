class AddColumnIsVisibleToOrgSso < ActiveRecord::Migration
  def change
    add_column :org_ssos, :is_visible, :boolean, default: true
  end
end

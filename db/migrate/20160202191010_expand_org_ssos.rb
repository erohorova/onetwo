class ExpandOrgSsos < ActiveRecord::Migration
  def change
    add_column :org_ssos, :is_enabled, :boolean, default: true
    add_column :org_ssos, :saml_idp_sso_target_url, :string, limit: 1024
    add_column :org_ssos, :saml_issuer, :string
    add_column :org_ssos, :saml_idp_cert, :string, limit: 4096
    add_column :org_ssos, :saml_assertion_consumer_service_url, :string, limit: 1024
    add_column :org_ssos, :saml_allowed_clock_drift, :integer
  end
end

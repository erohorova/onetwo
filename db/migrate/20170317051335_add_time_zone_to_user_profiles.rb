class AddTimeZoneToUserProfiles < ActiveRecord::Migration
  def change
    add_column :user_profiles, :time_zone, :string, limit: 64
  end
end

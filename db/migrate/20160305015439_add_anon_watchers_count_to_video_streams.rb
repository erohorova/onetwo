class AddAnonWatchersCountToVideoStreams < ActiveRecord::Migration
  def change
    add_column :video_streams, :anon_watchers_count, :integer, default: 0
  end
end

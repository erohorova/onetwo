class CreateCardEclData < ActiveRecord::Migration
  def change
    create_table :card_ecl_data do |t|
      t.integer :card_id, null: false

      t.text :data, null: false
      t.integer :data_version, null: false, default: 1

      t.timestamps null: false
    end

    add_index :card_ecl_data, :card_id, unique: true
    add_foreign_key :card_ecl_data, :cards, foreign_key: :card_id
  end
end

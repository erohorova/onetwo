class AllowNullCreatorIdInVideoStreams < ActiveRecord::Migration
  def change
    change_column :video_streams, :creator_id, :integer, null: true
  end
end

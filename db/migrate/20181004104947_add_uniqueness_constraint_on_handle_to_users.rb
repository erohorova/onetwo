class AddUniquenessConstraintOnHandleToUsers < ActiveRecord::Migration
  def change
  
    remove_index :users, [:organization_id, :handle]
    add_index :users, [:organization_id, :handle], unique: true
    
  end
end

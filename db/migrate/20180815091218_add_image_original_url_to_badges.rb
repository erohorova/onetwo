class AddImageOriginalUrlToBadges < ActiveRecord::Migration
  def change
    add_column :badges, :image_original_url, :string
  end
end

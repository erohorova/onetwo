class AddTypeFieldToTags < ActiveRecord::Migration
  def change
    add_column :tags, :type, :string, limit: 191
  end
end

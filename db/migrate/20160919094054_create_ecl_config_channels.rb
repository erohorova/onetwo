class CreateEclConfigChannels < ActiveRecord::Migration
  def change
    create_table :ecl_config_channels do |t|
      t.integer :ecl_content_config_id, null: false
      t.integer :channel_id, null: false
    end
    add_foreign_key :ecl_config_channels, :ecl_content_configs, column: :ecl_content_config_id
    add_foreign_key :ecl_config_channels, :channels, column: :channel_id
  end
end

class CreateOrganizationForm < ActiveRecord::Migration
  def change
    create_table :organization_forms do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :organization_name
      t.string :host_name
      t.string :organization_size
      t.string :interests, limit: 1024

      t.timestamp
    end
  end
end

class CreateOrganizationLevelMetrics < ActiveRecord::Migration
  def change
    create_table :organization_level_metrics do |t|
      t.integer :organization_id
      t.integer :user_id
      t.string :action, limit: 191
      t.string :content_type, limit: 191
      t.integer :events_count, default: 0

      t.string :period, limit: 191
      t.integer :offset
    end

    add_index :organization_level_metrics, [:organization_id, :period, :offset], name: 'indx_org_level_metrics_org_id_period_offset'
  end
end

class AddSourceNameToSourcesCredentials < ActiveRecord::Migration
  def change
    add_column :sources_credentials, :source_name, :string, limit: 30
  end
end

class AddWidthHeightToVideoStreams < ActiveRecord::Migration
  def change
    add_column :video_streams,:width, :integer
    add_column :video_streams, :height, :integer
  end
end

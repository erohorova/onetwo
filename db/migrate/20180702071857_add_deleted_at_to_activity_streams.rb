class AddDeletedAtToActivityStreams < ActiveRecord::Migration
  def change
    add_column :activity_streams, :deleted_at, :datetime
  end
end

class AddIsPaidToCards < ActiveRecord::Migration
  def change
    add_column :cards, :is_paid, :bool, default: false
  end
end

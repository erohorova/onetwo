class AddNumAssignedToCardMetricsAggregations < ActiveRecord::Migration
  def change
    add_column :card_metrics_aggregations, :num_assignments, :integer, null: false
  end
end

class CreatePermissions < ActiveRecord::Migration
  def change
    create_table :permissions do |t|
      t.string :name,:limit=>191
      t.string :rule_name,:limit=>191
      t.string :rule_type,:limit=>191

      t.timestamps null: false
    end
  end
end

class RemoveReviewStartedFromCards < ActiveRecord::Migration
  def change
    remove_column :cards, :review_started, :boolean
  end
end

class AddBalanceToWalletTransaction < ActiveRecord::Migration
  def change
    add_column :wallet_transactions, :balance, :decimal, precision: 8, scale: 2, null: false, default: 0.0
  end
end

class CreateCmsEcrContentItemsOrganizations < ActiveRecord::Migration
  def change
    create_table :cms_ecr_content_items_organizations do |t|
      t.integer :ecr_content_item_id
      t.string :ecr_content_item_type, limit: 191
      t.integer :organization_id
      t.string :state, limit: 191
    end

    add_index :cms_ecr_content_items_organizations, [:ecr_content_item_id, :ecr_content_item_type, :organization_id], name: 'ecr_ci_orgs_ci_orgs'
  end
end

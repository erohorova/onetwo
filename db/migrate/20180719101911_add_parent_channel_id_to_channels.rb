class AddParentChannelIdToChannels < ActiveRecord::Migration
  def change
    add_column :channels, :parent_id, :integer
    add_index :channels, [:organization_id, :parent_id], unique: true
  end
end

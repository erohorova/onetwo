class RolifyCreateRoles < ActiveRecord::Migration
  def change
    create_table(:roles) do |t|
      t.string :name,:limit=>191
      t.integer :organization_id
      t.boolean :master_role,:default=>false
      t.timestamps
    end

    create_table(:user_roles) do |t|
      t.references :user
      t.references :role
    end

    add_index(:roles, [:name,:organization_id])
    add_index(:user_roles, [ :user_id, :role_id ])
  end
end

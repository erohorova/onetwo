class CreateBackgroundJobs < ActiveRecord::Migration
  def change
    create_table :background_jobs do |t|
      t.string :job_id, limit: 191
      t.string :job_class, limit: 191
      t.string :job_queue, limit: 191
      t.string :status, limit: 191
      
      t.datetime :enqueued_at
      t.datetime :started_at
      t.datetime :finished_at
      t.datetime :scheduled_at

      t.integer :time_taken
      t.text :job_arguments
    end

    add_index :background_jobs, [:job_id]
    add_index :background_jobs, [:status]
    add_index :background_jobs, [:job_queue]
    add_index :background_jobs, [:job_class]
  end
end

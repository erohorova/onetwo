class AddDefaultNameToRole < ActiveRecord::Migration
  def change
    add_column :roles, :default_name, :string, index: true
  end
end

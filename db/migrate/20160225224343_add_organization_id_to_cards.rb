class AddOrganizationIdToCards < ActiveRecord::Migration
  def change
    add_column :cards, :organization_id, :integer
  end
end

class CreateCmsContentSources < ActiveRecord::Migration
  def change
    create_table :cms_content_sources do |t|
      t.text :source
      t.timestamps null: false
    end
  end
end

class CreateOrganizationEmailDomains < ActiveRecord::Migration
  def change
    create_table :organization_email_domains do |t|
      t.references :organization, index: true, foreign_key: true
      t.string :url, limit: 191
      t.timestamps null: false
    end
  end
end

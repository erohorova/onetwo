class AddColumnDomainsToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :domains, :text
  end
end

class AddOrganizationIdToVideoStreams < ActiveRecord::Migration
  def change
    add_column :video_streams, :organization_id, :integer
  end
end

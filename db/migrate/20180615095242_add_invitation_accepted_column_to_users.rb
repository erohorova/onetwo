class AddInvitationAcceptedColumnToUsers < ActiveRecord::Migration
  def change
    add_column :users, :invitation_accepted, :boolean
  end
end

class AddSocialEnabledToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :social_enabled, :boolean
  end
end

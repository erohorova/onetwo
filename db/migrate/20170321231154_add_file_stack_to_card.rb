class AddFileStackToCard < ActiveRecord::Migration
  def change
    add_column :cards, :filestack, :text
  end
end

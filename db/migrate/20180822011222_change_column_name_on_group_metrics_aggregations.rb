class ChangeColumnNameOnGroupMetricsAggregations < ActiveRecord::Migration
  def change
    remove_column :group_metrics_aggregations, :average_session
    add_column :group_metrics_aggregations, :time_spent_minutes, :integer
  end
end

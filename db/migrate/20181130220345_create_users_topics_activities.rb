class CreateUsersTopicsActivities < ActiveRecord::Migration
  def change
    create_table :users_topics_activities do |t|

      t.references :organization, null: false, foreign_key: false
      t.references :user,         null: false, foreign_key: false
      t.text       :topic_counts, null: false
      t.integer    :start_time,   null: false
      t.integer    :end_time,     null: false

    end

    add_index :users_topics_activities, :organization_id, name: "index_u_t_activities_on_org_id"
    add_index :users_topics_activities, :organization_id, name: "index_u_t_activities_on_user_id"

  end
end

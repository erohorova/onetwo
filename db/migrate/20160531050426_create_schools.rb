class CreateSchools < ActiveRecord::Migration
  def change
    create_table :schools do |t|
      t.string :name
      t.string :specialization
      t.date :from
      t.date :to
      t.integer :user_id
      t.attachment :image

      t.timestamps null: false
    end
    add_index :schools, :user_id
  end
end

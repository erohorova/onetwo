class AddConnectedColumnToUsersIntegration < ActiveRecord::Migration
  def change
  	remove_column :users_integrations, :enabled
  	add_column :users_integrations, :connected, :boolean, default: true
  end
end

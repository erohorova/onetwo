class AddTitleAndMessageToCards < ActiveRecord::Migration
  def change
    add_column :cards, :title, :text
    add_column :cards, :message, :text
  end
end

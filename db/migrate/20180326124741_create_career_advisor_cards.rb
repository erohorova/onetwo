class CreateCareerAdvisorCards < ActiveRecord::Migration
  def change
    create_table :career_advisor_cards do |t|
      t.integer :card_id, index: true, foreign_key: true, null: false
      t.integer :career_advisor_id, index: true, foreign_key: true, null: false
      t.string :level, null: false

      t.timestamps null: false
    end
  end
end

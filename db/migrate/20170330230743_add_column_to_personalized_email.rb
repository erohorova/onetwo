class AddColumnToPersonalizedEmail < ActiveRecord::Migration
  def change
    add_column :personalized_emails, :remark, :string
  end
end

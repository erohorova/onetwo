class CreateHrLocations < ActiveRecord::Migration
  def change
    create_table :hr_locations do |t|
      t.integer :organization_id, null: false

      t.string :name
      t.string :description

      t.timestamps
    end
  end
end

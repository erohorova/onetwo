class AddStructures < ActiveRecord::Migration
  def change
    create_table :structures do |t|
      t.references :creator, references: :users, null: false
      t.references :organization, index: true, foreign_key: true, null: false

      t.integer :parent_id, null: false
      t.string :parent_type, limit: 20, null: false

      t.string :display_name, limit: 100
      t.integer :context, null: false
      t.string :slug, null: false

      t.boolean :enabled, default: false

      t.timestamps null: false
    end

    add_foreign_key :structures, :users, column: :creator_id
    add_index :structures, [:organization_id, :parent_id, :parent_type], name: 'index_structures_on_org_parent'
  end
end

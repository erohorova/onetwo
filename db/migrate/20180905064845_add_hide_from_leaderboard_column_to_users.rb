class AddHideFromLeaderboardColumnToUsers < ActiveRecord::Migration
  def change
    add_column :users, :hide_from_leaderboard, :boolean
  end
end

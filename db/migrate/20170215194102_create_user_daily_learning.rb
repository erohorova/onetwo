class CreateUserDailyLearning < ActiveRecord::Migration
  def change
    create_table :user_daily_learnings do |t|
      t.integer :user_id, null: false
      t.text :card_ids
      t.string :date, limit: 11
      t.string :processing_state, null: false
      t.timestamps
    end

    add_index :user_daily_learnings, [:user_id, :date], unique: true
  end
end

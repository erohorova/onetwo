class AddDataExportColumnsToUsers2 < ActiveRecord::Migration
  def change
    add_column :users, :last_data_export_time, :datetime
  end
end

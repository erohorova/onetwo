class AddCompletedPercentageToUserContentCompletion < ActiveRecord::Migration
  def change
    add_column :user_content_completions, :completed_percentage, :integer, :null => false, :default => 0
  end
end

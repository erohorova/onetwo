class AddProviderAndProviderImageToChannels < ActiveRecord::Migration
  def change
    add_column :channels, :provider, :string
    add_column :channels, :provider_image, :string
  end
end

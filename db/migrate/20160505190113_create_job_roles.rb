class CreateJobRoles < ActiveRecord::Migration
  def change
    create_table :job_roles do |t|
      t.string :title
      t.references :industry, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

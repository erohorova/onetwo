class AddColumnsToMailerConfigs < ActiveRecord::Migration
  def change
    add_column :mailer_configs, :from_admin, :boolean, default: false
    add_column :mailer_configs, :is_active, :boolean, default: false
    add_column :mailer_configs, :verification, :text
  end
end

class ChangesInOrganisationPrices < ActiveRecord::Migration
  def change
    remove_reference :organization_prices, :organization, index: true, foreign_key: true
    add_reference :organization_prices, :org_subscription, index: true, foreign_key: true
    change_column :organization_prices, :amount, :decimal, precision: 8, scale: 2, null: false
    remove_column :organization_prices, :sku
  end
end

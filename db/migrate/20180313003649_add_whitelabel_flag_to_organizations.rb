class AddWhitelabelFlagToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :whitelabel_build_enabled, :boolean, default: false
  end
end

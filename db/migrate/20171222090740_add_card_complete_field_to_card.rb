class AddCardCompleteFieldToCard < ActiveRecord::Migration
  def up
    add_column :cards, :auto_complete, :boolean
  end

  def down
    remove_column :cards, :auto_complete
  end
end


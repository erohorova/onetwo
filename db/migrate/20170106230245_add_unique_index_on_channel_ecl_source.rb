class AddUniqueIndexOnChannelEclSource < ActiveRecord::Migration
  def change
    add_index :ecl_sources, [:channel_id, :ecl_source_id], unique: true, length: {ecl_source_id: 191}
  end
end

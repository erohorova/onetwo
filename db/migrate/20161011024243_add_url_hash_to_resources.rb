class AddUrlHashToResources < ActiveRecord::Migration
  def change
    add_column :resources, :url_hash, :string, limit: 64
    add_index :resources, :url_hash
  end
end

class CreateTeamAssignmentMetrics < ActiveRecord::Migration
  def change
    create_table :team_assignment_metrics do |t|
      t.integer  :team_id, null: false
      t.integer  :assignor_id, null: false
      t.integer  :content_id, null: false
      t.string   :content_type, null: false
      t.datetime :created_at, null: false
      t.integer  :not_started_count, default: 0
      t.integer  :started_count, default: 0
      t.integer  :completed_count, default: 0
    end

    add_index :team_assignment_metrics, :team_id
  end
end

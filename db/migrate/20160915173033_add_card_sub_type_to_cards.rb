class AddCardSubTypeToCards < ActiveRecord::Migration
  def change
    add_column :cards, :card_subtype, :string
  end
end

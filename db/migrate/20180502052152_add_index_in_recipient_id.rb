class AddIndexInRecipientId < ActiveRecord::Migration
  def change
    add_index :invitations, :recipient_id
  end
end

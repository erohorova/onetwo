class AddOrgIdToWebexConfigs < ActiveRecord::Migration
  def up
    add_column :webex_configs, :organization_id, :integer, unique: true, null: false
  end

  def down 
    remove_column :webex_configs, :organization_id
  end
end

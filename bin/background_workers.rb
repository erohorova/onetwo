#!/usr/bin/env ruby

# Using standard puts instead of default logger because logger is not available in this standalone ruby script. Heroku sends standard output to logentries anyway.
# Avoid using the rake task because do not want to load the entire Rails program, plus there is some threading issue with active record.

can_run = true

Signal.trap('INT') do
  puts '[Background workers] received INT...'
  can_run = false
end

Signal.trap('TERM') do
  puts '[Background workers] received TERM...'
  can_run = false
end

require 'active_record'
require 'yaml'
require 'erb'

database_config = YAML.load(ERB.new(File.open('config/database.yml').read).result) || {}

ActiveRecord::Base.establish_connection(database_config[ENV['RAILS_ENV']||'development'])

require_relative '../app/models/background_worker'


loop do
  begin
    BackgroundWorker.all.reload.each do |worker|
      if worker.state == 'run'
        worker.run unless worker.is_running?
      elsif worker.state == 'stop'
        worker.stop unless !worker.is_running?
      end
    end
  rescue => e
    puts "[Background workers] Silently skipping over temporary error: #{e}" #could be some temporary network exception
  end

  sleep 60
  puts '[Background workers] refreshing workers state...'
  break unless can_run
end

Process.waitall #wait for all the child processes to terminate...

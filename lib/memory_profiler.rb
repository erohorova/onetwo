require 'memory_profiler'

report = MemoryProfiler.report do
  require_relative '../config/environment'
  # Add specific code here if you want to profile deeper
end

report.pretty_print

module PeriodOffsetCalculator
  BEGINNING_OF_TIME = Time.parse("2015-01-05T00:00+00:00") #this is the starting time of all the time period offset
  DRILL_DOWN_PERIOD = {:day => :day, :week => :day, :month => :day, :year => :month}

  class << self

    def applicable_offsets timestamp
      {
        all_time: 0,
        day: day_offset(timestamp),
        week: week_offset(timestamp),
        month: month_offset(timestamp),
        year: year_offset(timestamp)
      }
    end

    def day_offset timestamp
      ((timestamp - BEGINNING_OF_TIME) / 1.day).floor
    end

    def week_offset timestamp
      ((timestamp - BEGINNING_OF_TIME) / 1.week).floor
    end

    def month_offset timestamp
      year_offset(timestamp) * 12 + (timestamp.month - 1)
    end

    def year_offset timestamp
      timestamp.year - BEGINNING_OF_TIME.year
    end

    # return timerange for period with offset
    # for eg: period: :week, offset: 72
    # [2016-05-23 00:00:00 +0000, 2016-05-30 00:00:00 +0000]
    def timerange(period: , offset: )
      if [:day, :week].include?(period.to_sym)
        [BEGINNING_OF_TIME + offset.send(period.to_sym), BEGINNING_OF_TIME + (offset+1).send(period.to_sym)]
      elsif period.to_sym == :year
        [Time.new(BEGINNING_OF_TIME.year + offset, 01, 01, 0, 0, 0, "+00:00"), Time.new(BEGINNING_OF_TIME.year + offset+1, 01, 01, 0, 0, 0, "+00:00")]
      elsif period.to_sym == :month
        [Time.new(BEGINNING_OF_TIME.year + (offset/12), 01 + (offset % 12), 01, 0, 0, 0, "+00:00"), Time.new(BEGINNING_OF_TIME.year + ((offset+1)/12), 01 + ((offset + 1)%12), 01, 0, 0, 0, "+00:00")]
      end
    end

    # returns offsets for current period
    # for eg: period: :week, offset: 72
    # [:day, [504, 505, 506, 507, 508, 509, 510]]
    def drill_down_period_and_offsets(period: , offset: )
      _timerange = timerange(period: period, offset: offset)
      drill_down_period_and_offsets_for_timerange(period: period, timerange: _timerange)
    end

    # returns timeranges for current period
    # for eg: period: :week, offset: 72
    # output format:
    # [
    #   [2016-05-23 00:00:00 UTC, 2016-05-24 00:00:00 UTC],
    #   [2016-05-24 00:00:00 UTC, 2016-05-25 00:00:00 UTC],
    #   [2016-05-25 00:00:00 UTC, 2016-05-26 00:00:00 UTC],
    #   [2016-05-26 00:00:00 UTC, 2016-05-27 00:00:00 UTC],
    #   [2016-05-27 00:00:00 UTC, 2016-05-28 00:00:00 UTC],
    #   [2016-05-28 00:00:00 UTC, 2016-05-29 00:00:00 UTC],
    #   [2016-05-29 00:00:00 UTC, 2016-05-30 00:00:00 UTC]
    # ]
    def drill_down_timeranges(period: , offset: )
      _timerange = timerange(period: period, offset: offset)
      drill_down_timeranges_for_timerange(period: period, timerange: _timerange)
    end

    # period: :day
    # timerange: [2015-03-16 00:00:00 +0000, 2015-03-23 00:00:00 +0000]
    def drill_down_timeranges_for_timerange(period: , timerange:)
      if timerange.first.beginning_of_day == timerange.last.beginning_of_day
        timerange = [timerange.first.beginning_of_day, (timerange.last + 1.day).beginning_of_day]
      end

      timestamps = (timerange.first.to_i..timerange.last.to_i).step(1.send(DRILL_DOWN_PERIOD[period.to_sym])).map{|t| Time.at(t).utc}
      timestamps.take(timestamps.length - 1).map.with_index{|item, index| [item, timestamps[index+1]]}
    end

    # period: :day
    # timerange: [2015-03-16 00:00:00 +0000, 2015-03-23 00:00:00 +0000]
    def drill_down_period_and_offsets_for_timerange(period: , timerange:)
      drill_down_period = DRILL_DOWN_PERIOD[period.to_sym]
      drill_down_offsets = ((self.send("#{drill_down_period}_offset", timerange.first))..(self.send("#{drill_down_period}_offset", timerange.last) - 1)).to_a
      [drill_down_period, drill_down_offsets]
    end

    def timestamp_to_readable_period(timestamp:, period: :day)
      case period
      when :day
        timestamp.strftime("%m/%d/%Y")
      when :week
        "#{timestamp.beginning_of_week.strftime("%m/%d/%Y")} - #{timestamp.end_of_week.strftime("%m/%d/%Y")}"
      when :month
        timestamp.strftime("%B %Y")
      when :year
        "Year of #{timestamp.try(:year)}"
      when :all_time
        "#{BEGINNING_OF_TIME.strftime("%m/%d/%Y")} - #{timestamp.strftime("%m/%d/%Y")}"
      end
    end
  end
end

class Gdpr
  #ActivityStream - delete respective votes, comments
  #Assignment - also deletes team_assignments
  #Bookmark - removes from learning_queue_item if not completed, read bookmark destroy callback
  #Event - deleted respective webex_meeting
  #Notification - destroys casual users
  #Pin -- dont remove pins - gayatri's suggestion
  #Follow - Delete all that user is following- channel, circle, event, post, user, card_query
  #Assignment - Should make a call to LMS also do delete it as they show it for schneider
=begin - poll cards kept as leaps/locks of pathways depends on it. It can be kept
      #Poll Cards
      polls = Card.where({author_id: user_id, card_type: 'poll'})
      quiz_questions = QuizQuestionOptions.where({quiz_question_id: polls.map(&:id), quiz_question_type: 'Card'})
      quiz_questions_attempts = QuizQuestionAttempt.where({quiz_question_id: polls.map(&:id), quiz_question_type: 'Card'})
      quiz_questions_stats = QuizQuestionOptions.where({quiz_question_id: polls.map(&:id), quiz_question_type: 'Card'})

      quiz_questions.destroy_all
      quiz_questions_attempts.destroy_all
      quiz_questions_stats.destroy_all
      polls.update_all({message: message, card_type: 'media', card_subtype: 'text', resource_id: nil, filestack: nil})
=end

  def self.delete_user(user_id, admin_id)
    user = User.find(user_id)
    admin = User.find(admin_id)
    if user and admin

  	#**************************Destiny Scope (LXP)**************************#
      #Data to be deleted using user_id
      delete_classes = [AccessLog, ActivityStream, Assignment, AssignmentNoticeStatus, Bookmark, CardUserAccess, 
                        ClcProgress, Comment, Email, Event, EventsUser, IdentityProvider, LearningQueueItem, Mention, NotificationConfig,
                        NotificationEntry, Notification, Post, PwcRecord, QuizQuestionAttempt, Score, SkillsUser, SuperAdminAuditLog,
                        UserBadge, UserConnection, UserContentCompletion, UserContentLevelMetric, UserCustomField, UserDailyLearning,
                        UserLevelMetric, UserNotificationRead, UserOnboarding, UserPreference, UserProfile, UserRole, UserSession,
                        UserTaxonomyTopicLevelMetric, UserTopicLevelMetric, UsersCardsManagement, UsersExternalCourse, UsersIntegration,
                        Follow]

      delete_classes.each do |klass|
        begin
          klass.where({user_id: user_id}).destroy_all
        rescue Exception => e
          log.error("Gdpr: Error deletinng data for #{klass} | #{e.message}")
        end
      end
      begin
        invitations = Invitation.where({recipient_id: user_id}).destroy_all
      rescue Exception => e
        log.error("Gdpr: Error deleting Invitation | #{e.message}")
      end

      begin
        pubnub_channels = PubnubChannel.where({item_type: 'User', item_id: user_id}).destroy_all
      rescue Exception => e
        log.error("Gdpr: Error deleting PubnubChannel | #{e.message}")
      end

      begin
        taggings = Tagging.where({taggable_type: 'User', taggable_id: user_id}).destroy_all
      rescue Exception => e
        log.error("Gdpr: Error deleting Tagging | #{e.message}")
      end

      begin
        #This is handled with delete bcoz destroy_all fails for table without primary key
        ClcOrganizationsRecord.delete_all({user_id: user_id})
      rescue Exception => e
        log.error("Gdpr: Error deleting clc organization record | #{e.message}")
      end


      ###Handling Channels###

      #Replace curator with admin user.
      channels_curator = ChannelsCurator.where({user_id: user_id}).update_all({user_id: admin.id})
      #Replace collaborator with admin user.
      channels_collaborator = ChannelsUser.where({user_id: user_id}).update_all({user_id: admin.id})

      ######Handling Groups(Teams)#######
      #Remove user from teams
      #teams = TeamsUser.where({user_id: user_id}).destroy_all
      user_teams = TeamsUser.where({user_id: user_id})

      #If user is admin and there are no other admins in a team, assign admin randomly
      user_teams.each do |user_team|
        begin
          if user_team.as_type == 'admin' && !TeamsUser.where("team_id = ? and user_id != ? and as_type = 'admin'", user_team.team_id, user_id).exists?
            others_role = TeamsUser.where("team_id = ? and user_id != ?", user_team.team_id, user_id).limit(1)
            others_role.first.update({as_type: 'admin'})
          end
        rescue Exception => e
          log.error("Gdpr: Error assigning admin to team #{user_team.team_id} | #{e.message}")
        end
      end

      user_teams.destroy_all

      ######Groups(savannah)######
      user_groups = GroupsUser.where({user_id: user_id})

      user_groups.each do |user_group|
        begin
          if user_group.as_type == 'admin' && !GroupsUser.where("group_id = ? and user_id != ? and as_type = 'admin'", user_group.group_id, user_id)
            others_role = GroupsUser.where("group_id = ? and user_id != ?", user_group.group_id, user_id).limit(1)
            others_role.first.update({as_type: 'admin'})
          end
        rescue Exception => e
          log.error("Gdpr: Error assigning admin to group | #{e.message}")
        end
      end

      user_groups.destroy_all

      ###Handling Projects###
      #remove all project submissions of user and its corresponding reviews
      submissions = ProjectSubmission.where({submitter_id: user_id})
      reviews = ProjectSubmissionReview.where(project_submission_id: submissions.map(&:id))
      submissions.destroy_all
      reviews.destroy_all

      #replace reviewer as admin
      projects = Project.where({reviewer_id: user_id})
      project_reviews = ProjectSubmissionReview.where({reviewer_id: user_id})
      projects.update_all({reviewer_id: admin.id})
      project_reviews.update_all({reviewer_id: admin.id})

      ###Handling user cards###
      #Should we handle deleted cards also?
      message = "This card is deleted by author" #"Author of this card does not exist anymore"
      #Media Card
      media_card = Card.where({author_id: user_id, card_type: 'media'})
      media_card.update_all({message: message, card_type: 'media', card_subtype: 'text', resource_id: nil, filestack: nil})

      #Course Cards
      #course link(scorm cards, removing resource)
      course_link = Card.where({author_id: user_id, card_type: 'course', card_subtype: 'text'})
      course_link.update_all({message: message, card_type: 'media', card_subtype: 'text', resource_id: nil, filestack: nil})

      #course other
      course_other = Card.where({author_id: user_id, card_type: 'course'})
      course_other.update_all({message: message, card_type: 'media', card_subtype: 'text', resource_id: nil, filestack: nil})

      #Video Stream Cards
      video_stream_cards = Card.where({author_id: user_id, card_type: 'video_stream'})
      streams = VideoStream.where({card_id: video_stream_cards.map(&:id)})
      recordings = Recording.where({video_stream_id: streams.map(&:id)})

      video_stream_cards.update_all({message: message, card_type: 'media', card_subtype: 'text', resource_id: nil, filestack: nil})
      streams.destroy_all
      recordings.destroy_all #also destroys thumbnails

      #Finally delete user
      #this is done to avoid callbacks as it dosent all to update email(sends confirmation mail)
      user.update_column('email', "#{user_id}suspended@edcast.com")
      user.first_name = 'UnknownFname'
      user.last_name = 'UnknownLname'
      user.handle = "@#{user_id}suspended"
      user.picture_url = nil
      user.organization_role = 'member'
      user.save
      user.suspend!
    end
  end
end
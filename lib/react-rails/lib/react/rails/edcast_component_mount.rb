module React
  module Rails
    # This is the default view helper implementation.
    # It just inserts HTML into the DOM (see {#react_component}).
    #
    # You can extend this class or provide your own implementation
    # by assigning it to `config.react.view_helper_implementation`.
    class EdcastComponentMount < ComponentMount

      def setup(env)
        @__controller_instance = env
      end

      def react_component(name, props = {}, options = {}, &block)
        if (options[:prerender])
          if @__controller_instance.current_org
            options[:prerender] = {cobrand_settings: @__controller_instance.current_org.fetch_cobrand_configs_with_defaults}
          end
        end
        super(name, props, options, &block)
      end
    end
  end
end

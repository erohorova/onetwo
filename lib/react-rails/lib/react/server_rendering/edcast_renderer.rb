module React
  module ServerRendering
    class EdcastRenderer < SprocketsRenderer

      def before_render(component_name, props, prerender_options)
        "window.cobrands = #{ActiveSupport::JSON.encode(prerender_options[:cobrand_settings])}"
      end

      def render(component_name, props, prerender_options)
        # pass prerender: :static to use renderToStaticMarkup
        react_render_method = if prerender_options == :static
                                "renderToStaticMarkup"
                              else
                                "renderToString"
                              end

        if !props.is_a?(String)
          props = props.to_json
        end

        ExecJSRenderer.instance_method(:render).bind(self).call(component_name, props, {render_function: react_render_method, cobrand_settings: prerender_options[:cobrand_settings]})
      end

    end
  end
end

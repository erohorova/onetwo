module EsAnalyzers

  # Tokenizers
  STANDARD_TOKENIZER = :standard
  KEYWORD_TOKENIZER = :keyword

  # Filters
  STANDARD_FILTER = :standard
  LOWERCASE_FILTER = :lowercase
  ASCIIFOLDING_FILTER = :asciifolding

  WORD_DELIMITER_FILTER_NAME = :edcast_word_delimiter
  WORD_DELIMITER_FILTER = { WORD_DELIMITER_FILTER_NAME => { type: :word_delimiter, preserve_original: true } }

  EDGE_NGRAM_FILTER_NAME = :edcast_edge_ngram_filter
  EDGE_NGRAM_FILTER = { EDGE_NGRAM_FILTER_NAME => { type: :edgeNGram, min_gram: 2, max_gram: 30 } }
  STOP_FILTER = :stop
  STEMMER_FILTER = :stemmer

  SORT_ANALYZER_NAME = :edcast_sort_analyzer
  SORT_ANALYZER = { SORT_ANALYZER_NAME => { tokenizer: KEYWORD_TOKENIZER, filter: [LOWERCASE_FILTER] } }

  EXACT_MATCH_ANALYZER_NAME = :edcast_exact_match_analyzer
  EXACT_MATCH_ANALYZER = { EXACT_MATCH_ANALYZER_NAME => { tokenizer: KEYWORD_TOKENIZER, filter: [LOWERCASE_FILTER] } }

  SEARCH_ANALYZER_NAME = :edcast_search_analyzer
  SEARCH_ANALYZER = { SEARCH_ANALYZER_NAME => { tokenizer: STANDARD_TOKENIZER, filter: [LOWERCASE_FILTER, WORD_DELIMITER_FILTER_NAME, STOP_FILTER, STEMMER_FILTER] } }

  PREFIX_SEARCH_ANALYZER_NAME = :edcast_prefix_search_analyzer
  PREFIX_SEARCH_ANALYZER = { PREFIX_SEARCH_ANALYZER_NAME => { tokenizer: STANDARD_TOKENIZER, filter: [LOWERCASE_FILTER, WORD_DELIMITER_FILTER_NAME, EDGE_NGRAM_FILTER_NAME] } }

  SEARCHABLE_FIELD_MAPPING = { 
    type: :string, 
    analyzer: SEARCH_ANALYZER_NAME , 
    search_analyzer: SEARCH_ANALYZER_NAME 
  }

  SORTABLE_FIELD_MAPPING = { 
    type: :string, 
    analyzer: SORT_ANALYZER_NAME
  }

  PREFIX_SEARCHABLE_FIELD_MAPPING = {
    type: :string,
    analyzer: PREFIX_SEARCH_ANALYZER_NAME,
    search_analyzer: :standard
  }

  EXACT_MATCH_FIELD_MAPPING = { 
    type: :string, 
    analyzer: EXACT_MATCH_ANALYZER_NAME
  }

  UNANALYZED_STRING_MAPPING = { 
    type: :string, 
    index: :not_analyzed
  }

  DEFAULT_STRING_TEMPLATE = { 
    edcast_string_template: { 
      match: "*", 
      match_mapping_type: "string", 
      mapping: UNANALYZED_STRING_MAPPING
    }
  }

  # All string fields are by default unanalyzed. This sets up our custom
  # analyzers to be available
  DEFAULT_SEARCHKICK_OPTS = {
        callbacks: false,
        merge_mappings: true,
        mappings: {
          _default_: {
            dynamic_templates: [EsAnalyzers::DEFAULT_STRING_TEMPLATE],
          }
        },
        settings: {
          analysis: {
            analyzer: EsAnalyzers::SEARCH_ANALYZER.
              merge(EsAnalyzers::SORT_ANALYZER).
              merge(EsAnalyzers::PREFIX_SEARCH_ANALYZER).
              merge(EsAnalyzers::EXACT_MATCH_ANALYZER),
            filter: EsAnalyzers::WORD_DELIMITER_FILTER.merge(EsAnalyzers::EDGE_NGRAM_FILTER)
          }
        }
  }

  # Splits up nested fields such as "tags.name" to generate the appropriate 
  # elasticsearch mapping.
  def self.format_nested(property, mapping)
    nested_list = property.split(".")
    if nested_list.length == 1
      {nested_list.first => mapping}
    else
      {
        nested_list.first => {
          properties: {
            nested_list.last => mapping
          }
        }
      }
    end
  end
end

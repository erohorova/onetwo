module BranchmetricsApi

  # generate a deep link for a particular user with
  # some payload
  # Organization argument is added to support whitelabel builds
  def self.generate_link(payload, organization: nil)
    end_point = Settings.branchmetrics.endpoint
    api_key = fetch_branchmetrics_api_key(organization)

    data = {
      'branch_key' => api_key,
      'data' => payload,
    }

    conn = Faraday.new(url: end_point)

    result = conn.post do |req|
      req.url '/v1/url'
      req.headers['Content-Type'] = 'application/json'
      req.body = data.to_json
    end

    unless result.success?
      log.error "Branchmetrics returned status: #{result.status} for payload: #{payload}"
      return nil
    end

    begin
      json_result = JSON.parse(result.body)
      return json_result['url']
    rescue
      log.error "Branchmetrics returned bad json for payload: #{payload}. Response was: #{result.body}"
      return nil
    end
  end

  # Get branchmetrics api key
  def self.fetch_branchmetrics_api_key(organization)
    whitelabel_build_branchmetrics_key = nil
    api_key = Settings.branchmetrics.api_key

    if organization&.whitelabel_build_enabled?
      whitelabel_build_branchmetrics_key = organization.get_settings_for('whitelabel_branchmetrics_api_key')
      if whitelabel_build_branchmetrics_key.blank?
        log.warn "Custom Branchmetrics key not found"
      end
    end

    whitelabel_build_branchmetrics_key || api_key
  end

end

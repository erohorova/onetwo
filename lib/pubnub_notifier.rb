require 'pubnub'
class PubnubNotifier

  class << self
    #here host_name is like ge.edcast.com and host_name will always available
    def skip_send_notification?(host)
      org = Organization.find_by_request_host(host)
      org && org.notifications_and_triggers_enabled?
    end
    
    def notify(opts = {}, skip_from_old = true)
      return if skip_send_notification?(opts[:host_name]) && skip_from_old
      org = Organization.find_by_request_host(opts[:host_name])

      log.info "Pubnub notify called with opts: #{opts}"
      item = opts[:item]
      excluded_user_ids = opts[:excluded_user_ids]
      deep_link_id = opts[:deep_link_id]
      deep_link_type = opts[:deep_link_type]
      content = opts[:content]
      notification_type = opts[:notification_type]
      channel_names = []
      if notification_type == Notification::TYPE_MENTION_IN_CARD || notification_type == Notification::TYPE_MENTION_IN_COMMENT
        user = User.find_by(id: opts[:user_id])
        channel_names << user.private_pubnub_channel_name
      elsif item.instance_of? Announcement
        # Should have ideally used the group channel here, but then the creator, who is subscribed to this group channel will also get the push notification
        item.group.users.where.not(id: excluded_user_ids).find_each do |u|
          channel_names << u.private_pubnub_channel_name
        end
      elsif (item.is_a?(Card) && item.card_type == Card::TYPE_VIDEO_STREAM)
        video_stream = item.video_stream
        if notification_type == 'scheduled_stream_reminder'
          channel_names << video_stream.creator.private_pubnub_channel_name
        else
          if item.publicly_visible?
            channel_names << video_stream.creator.public_pubnub_channel_name
            added_ids = video_stream.creator.followers.pluck(:id)
            video_stream.relevant_channels.each do |ch|
              # exclude those already covered under following_user rationale
              recipients = ch.followed_by_users.where.not(id: added_ids)
              recipients.each do |recipient|
                channel_names << recipient.private_pubnub_channel_name
                added_ids << recipient.id
              end
            end

          else
            added_ids = []
            # no way out but to send a message on each individual user's channel.
            # Can't use channel.public_pubnub_channel_name because then a single user
            # will get multiple notifications if he is subsribed to multiple channels
            video_stream.relevant_channels.each do |ch|
              # exclude those already covered under following_user rationale
              recipients = ch.followed_by_users.where.not(id: added_ids)
              recipients.each do |recipient|
                channel_names << recipient.private_pubnub_channel_name
                added_ids << recipient.id
              end
            end
          end
        end
      elsif item.kind_of? Comment
        creator_id = nil
        if item.commentable_type == 'Card'
          # continue for media cards only
          if item.commentable.card_type != 'media'
            return
          end
          creator_id = item.commentable.author_id
        elsif item.commentable_type == 'Post'
          creator_id = item.commentable.user_id
        end

        if creator_id && !excluded_user_ids.include?(creator_id)
          creator = User.find_by(id: creator_id)
          channel_names << creator.private_pubnub_channel_name
          excluded_user_ids << creator_id
        end

        item.commentable.comments.where.not(user_id: excluded_user_ids).each do |c|
          channel_names << c.user.private_pubnub_channel_name
        end
      else
        user = User.find_by(id: opts[:user_id])
        channel_names << user.private_pubnub_channel_name
      end

      payload = {
        pn_apns: {
          aps: {
            alert: content,
            sound: 'default',
            badge: 1
          },
          deep_link_id: deep_link_id,
          deep_link_type: deep_link_type,
          notification_type: notification_type,
          host_name: opts[:host_name]
        },
        pn_gcm: {
          data: {
            summary: content,
            deep_link_id: deep_link_id,
            deep_link_type: deep_link_type,
            notification_type: notification_type,
            host_name: opts[:host_name]
          }
        }
      }
      send_payload(payload, channel_names, organization: org)
    end

    # White labelled apps need separate keys.
    # Based on org settings, keys will be fetched.
    # organization is defaulted to nil to support backward compatibility
    def send_payload(payload, channel_names, organization: nil)
      pubnub_keys = fetch_pubnub_keys(organization)
      log.info "Pubnub send payload called with payload: #{payload} for channels: #{channel_names}"

      pubnub_options = pubnub_keys.merge(logger: log)
      pubnub = Pubnub.new(pubnub_options)

      channel_names.each do |ch_name|
        pubnub.publish(
          channel: ch_name,
          message: payload,
          http_sync: true
        ) do |envelope|
          resp = envelope.parsed_response
          if resp[0] == 1
            # Success
            log.info "Successfully notified pubnub channel: #{ch_name} with payload: #{payload}. response: #{resp}"
          else
            log.error "Error notifying pubnub channel: #{ch_name} with payload: #{payload}. response: #{resp}"
          end
        end
      end
    end

    def notify_v2(opts = {})
      log.info "Pubnub notify v2 called with opts: #{opts}"
      org = Organization.find_by_request_host(opts[:host_name])

      payload = form_payload(opts)
      send_payload(payload, opts[:channel_names], organization: org)
    end

    def form_payload(opts = {})
      {
        pn_gcm: {
          data: {
            summary: opts[:content],
            deep_link_id: opts[:deep_link_id],
            deep_link_type: opts[:deep_link_type],
            notification_type: opts[:notification_type],
            host_name: opts[:host_name]
          }
        },
        pn_apns: {
          aps: {
            alert: opts[:content],
            sound: 'default',
            badge: 1
          },
          deep_link_id: opts[:deep_link_id],
          deep_link_type: opts[:deep_link_type],
          notification_type: opts[:notification_type],
          host_name: opts[:host_name]
        }
      }
    end

    def fetch_pubnub_keys(organization)
      whitelabel_build_pubnub_keys = nil
      pubnub_keys = {
        subscribe_key: Settings.pubnub.subscribe_key,
        publish_key: Settings.pubnub.publish_key
      }

      if organization&.whitelabel_build_enabled?
        whitelabel_build_pubnub_keys = organization.get_settings_for('whitelabel_pubnub_keys')
        if whitelabel_build_pubnub_keys.blank?
          log.warn "Custom pubnub keys are not found"
        end
      end

      whitelabel_build_pubnub_keys || pubnub_keys
    end
  end
end

# This is written to provide developer APIs Authentication.

# AUTHENTICATION:
#   Every organization will have credential created. Each credential will have api-key and shared_secret keys.
#   api-key and shared-secret keys from hexdigest code created randomly
#   Clients will have original api-key and shared-secret keys

# AUTH IMPLEMENTATION:
#   api-key is passed in headers named as `X-DEVELOPER-API-KEY`
#   X-DEVELOPER-AUTH-TOKEN is passed as encrypted from shared_secret to request JWT token for session
#   X-JWT-TOKEN is passed for subsequent API calls

require 'jwt'
class DeveloperApiAuthentication

  attr_accessor :jwt_token, :api_key, :api_credential, :organization, :user

  def initialize(options)
    self.api_key = options[:api_key]
    self.jwt_token = options[:auth_token] || options[:jwt_token]
    self.api_credential, self.organization = get_api_credential
    self.user = nil
  end

  def get_api_credential
    DeveloperApiCredential.fetch_api_credential_and_org(api_key: api_key)
  end

  #Request for JWT token
  def authenticate_jwt_request
    return nil unless organization
    decode_jwt_token_request
  end

  #API Access with JWT token
  def authenticate_api_request
    return nil unless organization

    if decode_jwt_token && decode_jwt_token["email"]
      user = User.find_by(email: decode_jwt_token["email"], organization_id: organization.id)
    end
    [user, organization, decode_jwt_token]
  end

  def authenticate_external_api_request
    return nil unless organization

    if decode_jwt_token && decode_jwt_token["email"]
      self.user = User.find_by(email: decode_jwt_token["email"], organization_id: organization.id)
      return nil unless user.authorize?(Permissions::DEVELOPER)
    end
    [user, organization, decode_jwt_token]
  end

  def decode_jwt_token_request
    find_or_create_user(decode_jwt_token) if decode_jwt_token
    [user, organization, decode_jwt_token]
  end

  #If user is not present for that org then we will create new
  def find_or_create_user(payload)
    payload['auth_hash'] ||= {}
    payload['auth_hash']['organization_id'] = organization.id
    auth_hash = payload['auth_hash'].with_indifferent_access
    auth_hash['email'] = payload["email"] if payload["email"].present?

    self.user = User.get_or_create_with_email(auth_hash: auth_hash)
    if auth_hash['provider'] && (auth_hash['provider'] != "email")
      set_provider(auth_hash['provider'], auth_hash: auth_hash)
    end
  end

  #Get or create IdentityProvider
  def set_provider(provider, auth_hash: nil)
    identity_provider_class = IdentityProvider.class_for(auth_hash[:provider])
    provider = identity_provider_class.find_or_initialize_by(user: user, uid: auth_hash[:uid])
    provider.token = auth_hash[:token]
    provider.secret = auth_hash[:secret]
    provider.expires_at = Time.parse(auth_hash[:expires_at]) if auth_hash[:expires_at]
    provider.save!
  end

  #Generate jwt token for subsequents APIs
  def generate_jwt
    user.api_jwt_token(api_credential.shared_secret)
  end

  def decode_jwt_token
    jwt_payload, jwt_header = JWT.decode(jwt_token, api_credential.shared_secret)
    jwt_payload
  end
end
module DailyLearningProcessingStates

  INITIALIZED = 'initialized'.freeze
  STARTED = 'started'.freeze
  COMPLETED = 'completed'.freeze
  ERROR = 'errored'.freeze
  ALL_STATES = [INITIALIZED, STARTED, COMPLETED, ERROR].freeze

end

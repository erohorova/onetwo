# frozen_string_literal: true

module ObjectRefinements

  module YieldSelf
    refine(Object) { def yield_self; yield self; end }
  end

  # Applies group_by and transform_values using each of the given keys.
  # For example, given a list of records which each has a 'org_id', 'team_id',
  # and 'start_time' attribute, you could use the call:
  #
  # group_by_sequence(list, ['org_id', 'team_id', 'start_time'])
  #
  # To produce a structure like so:
  #
  # { <org_id> => { <team_id> => { <start_time> => [records] } } }
  #
  # It's sort of like a multilayered group_by, and works using recursion.
  #
  module GroupBySequence
    # Need to make a static wrapper for the method.
    # Otherwise, recursion doesn't work with refinements.
    def self.group_by_sequence(list, attrs, &blk)
      blk ||= -> (x) { x} 
      return blk.call(list) unless attrs&.any?
      grouping_attr, *remaining_attrs = attrs
      list.group_by do |record|
        record[grouping_attr]
      end.transform_values do |values|
        group_by_sequence(values, remaining_attrs, &blk)
      end
    end
    refine(Object) do
      def group_by_sequence(*args, &blk)
        ObjectRefinements::GroupBySequence.group_by_sequence(*args, &blk)
      end
    end
  end

  # The opposite of #dig. Buries a value at a nested key in a hash,
  # creating the inner hashes along the way if needed.
  module Bury
    # Need to make a static wrapper for the method.
    # Otherwise, recursion doesn't work with refinements.
    def self.bury!(obj, keys, val, &blk)
      # By default it sets the val to the final key in the list.
      # However, a custom block can be provided if you want to do something else,
      # e.g. merge
      blk ||= -> (_obj, _current_key, _val) do
        _obj[_current_key] = val
      end
      current_key, *remaining_keys = keys
      if remaining_keys.empty?
        blk.call(obj, current_key, val)
        return
      end
      obj[current_key] ||= {}
      bury!(obj[current_key], remaining_keys, val, &blk)
    end
    refine(Object) do
      def bury!(*args, &blk)
        ObjectRefinements::Bury.bury!(*args, &blk)
      end
    end
  end

end

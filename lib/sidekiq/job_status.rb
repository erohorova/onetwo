# IDEA: Maintain a status of each job in redis so that it won't enqueue same entity over and over again.
#   1. Expose an interface to track the jobs only once.
#   2. This means a card won't be enqueued if the same card is either enqueued or processing.
#   3. Once the job is finished processing, remove the entry from redis. ActiveJob will start accepting the job for the same entity.

module Sidekiq
  class JobStatus
    UNIQ_KEY = 'sidekiq_unique_job'

    def initialize(redis_pool = nil)
      @redis_pool = redis_pool
    end

    def self.key(*args)
      _key = args.join('-').downcase

      UNIQ_KEY + ':' + Digest::MD5.hexdigest(_key)
    end

    def delete(args)
      self.redis do |connection|
        current_key = JobStatus.key(args)
        keys = connection.keys "#{current_key}*"

        keys.each{ |key| connection.del(key) }
      end
    end

    def enqueuable?(args)
      status = nil

      self.redis do |connection|
        status = connection.get(JobStatus.key(args))
      end

      ['enqueued', 'processing'].exclude?(status)
    end

    def redis
      if @redis_pool
        @redis_pool.with do |connection|
          yield connection
        end
      else
        Sidekiq.redis do |connection|
          yield connection
        end
      end
    end

    def set_status(args, status)
      self.redis do |connection|
        current_key = JobStatus.key(args)

        connection.multi do
          connection.set current_key, status
          connection.expire current_key, 30.minutes
        end
      end
    end
  end
end

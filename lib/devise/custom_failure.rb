class CustomFailure < Devise::FailureApp

  def respond
    if request_format.to_s == 'json'
      log.warn "path=#{request.original_fullpath} cookies=#{request.cookies.to_json} raw_cookies=#{request.headers['Cookie']} user_agent=#{request.user_agent} device_id=#{request.headers['X-Device-Id']}"
    end
    super
  end
end
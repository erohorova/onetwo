require 'devise/strategies/authenticatable'


module Devise
  module Strategies
    class OrganizationAuthenticatable < Authenticatable

      def valid?
        if params[:action] == 'update' && params[:controller] == 'passwords' && params[:user].present? && params[:user][:password].present?
          organization = Organization.find_by_request_host(request.host)
          if organization && organization.encryption_payload? && ENV['JS_PRIVATE_KEY'].present?
            begin
              private_key = OpenSSL::PKey::RSA.new(ENV['JS_PRIVATE_KEY'].gsub("\\n", "\n"))
              params[:password][:user].merge!(params[:password][:user].inject({}){ |hash, (k, v)| hash.merge( k.to_sym => private_key.private_decrypt(Base64.decode64(v)) ) })
            rescue OpenSSL::PKey::RSAError
              params[:password][:user].merge!(params[:password][:user])
            end
          end          
        end

        if params[:action] == 'create' && params[:user].present? && params[:user][:_password].present?
          organization = Organization.find_by_request_host(request.host)
          if organization && organization.encryption_payload? && ENV['JS_PRIVATE_KEY'].present?
            begin
              private_key = OpenSSL::PKey::RSA.new(ENV['JS_PRIVATE_KEY'].gsub("\\n", "\n"))
              params[:session][:user][:password] = private_key.private_decrypt(Base64.decode64(params[:user][:_password]))  
            rescue OpenSSL::PKey::RSAError
              params[:session][:user][:password] = params[:user][:_password]
            end
          else
            params[:session][:user][:password] = params[:user][:_password]
          end      
        end

        ((params.has_key?(:token) && params.has_key?(:api_key)) ||
          (params.has_key?(:auth_token) && params.has_key?(:savannah_app_id))) &&
          params[:controller] == 'users/sessions' && params[:action] == 'create'
      end

      def authenticate!
        organization, payload, header = PartnerAuthentication.get_org(request: request, params: params)
        if organization.nil?
          if params[:api_key]
            log.info("Unable to look up client and credential using api_key=#{params[:api_key]}") 
          else
            log.info("Unable to look up organization using savannah_app_id=#{params[:savannah_app_id]}") 
          end
          fail!("Unable to authenticate via organization")
          return
        end

        begin
          #ensure loggin into the right organization host so cookie will work
          if request.host != organization.host
            log.warn "Host name does not match. expected=#{organization.host}, actual=#{request.host}"
            fail!("Invalid organization host name")
            return
          end

          # user = get_user(payload, organization) #global org user
          # return unless user
          destiny_user_id = payload["destinyUserId"] || payload[:destiny_user_id]
          org_user = User.where(organization_id: organization.id, id: destiny_user_id).first

          unless org_user
            log.error "Unable to look up organization user where parent_user_id=#{user.id} organization_id=#{organization.id}"
            fail!("Unable to look up organization user")
            return
          end
          remember_me(org_user)  
          success!(org_user)
        rescue => e
          log.error e
          fail!("Unable to authenticate via organization")
        end
      end

    end
  end
end

# frozen_string_literal: true

module AggregationQueryUtil

  def add_null_records_for_empty_days!(results:, start_time:, end_time:, &blk)
    num_seconds_per_day = 86400
    # Find the number of days contained in the range.
    num_days_in_range = (end_time - start_time) / 60 / 60 / 24
    # Make sure records are set for each day in the range
    (0..num_days_in_range).each do |day_increment|
      time = start_time + (num_seconds_per_day * day_increment)
      # Proceed to the next iteration if there's a record at this time
      next if results[time]
      # Delegate to the block to actually set the value in the case that
      # there's a missing value
      end_time = time + (num_seconds_per_day - 1)
      blk.call results, { start_time: time, end_time: end_time }
    end
  end

end
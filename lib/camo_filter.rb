module CamoFilter

  def self.filter(url)
    pattern = /^((http):\/\/)/
    host = Settings.camo_host
    if pattern.match(url)
      query = {url: url}.to_query
      return "#{host}/ec/?#{query}"
    end
    return url
  end
end

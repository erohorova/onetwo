class CustomCipher
  def initialize(secret_key: nil)
    @secret_key = secret_key || Edcast::Application.config.secret_key_base
  end

  def decrypt(context:)
    ActiveSupport::MessageEncryptor.new(@secret_key).decrypt_and_verify(context)
  end

  def encrypt(context:)
    ActiveSupport::MessageEncryptor.new(@secret_key).encrypt_and_sign(context)
  end
end

require 'aws-sdk'

class S3UpdateInterface

  attr_accessor :bucket

  def initialize(options={})
    aws_access_key_id = options[:aws_access_key_id] || Settings.aws_access_key_id
    aws_secret_access_key =  options[:aws_secret_access_key] || Settings.aws_secret_access_key
    s3_bucket_name = options[:s3_bucket_name] || Settings.s3_bucket_name
    AWS.config(access_key_id: aws_access_key_id, secret_access_key: aws_secret_access_key)
    @s3 = AWS::S3.new
    @bucket = @s3.buckets[s3_bucket_name]
  end

  def update_cache_control_header
    @bucket.objects.each do |obj|
      obj.copy_from(obj.key, content_type: obj.content_type, cache_control: 'max-age=315576000',  acl: :public_read)
    end
  end
end

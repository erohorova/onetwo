class OmniauthReader
  attr_accessor :app_data, :auth, :params, :redirect_uri, :strategy

  def initialize(environment:)
    oauth = environment['omniauth.auth']
    self.auth     = oauth.is_a?(Hash) ? Hashie::Mash.new(environment['omniauth.auth']) : oauth
    self.params   = Hashie::Mash.new(environment['omniauth.params'])
    self.strategy = environment['omniauth.strategy']
  end

  def read
    self.app_data = app_data_info
    self.params.redirect_uri = redirect_uri_info
  end

  private
    def app_data_info
      self.params['app_data']
    end

    def redirect_uri_info
      self.params.consumer_redirect_uri || self.params.redirect_uri
    end
end

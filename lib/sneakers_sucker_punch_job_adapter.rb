require 'active_support/core_ext/module/aliasing'

module JobWrapperWithRescue
  def work(msg)
    begin
      super
    rescue ActiveJob::DeserializationError => e
      log.error e
      ack!
    end
  end
end

ActiveJob::QueueAdapters::SidekiqAdapter::JobWrapper.send(:prepend, JobWrapperWithRescue)


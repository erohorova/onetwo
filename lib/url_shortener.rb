class UrlShortener
  SHORTENER_API_URL = "https://www.googleapis.com/urlshortener/v1/url?key="

  def self.shorten!(url)
    google_server_url =  "#{SHORTENER_API_URL}#{Settings.url_shortener_api_key}"
    begin
      conn = Faraday.new(google_server_url)

      resp = conn.post do |req|
        req.headers['Content-Type'] = 'application/json'
        req.body = "{ 'longUrl': '#{url}' }"
      end

      if resp.success?
        json_response = JSON.parse(resp.body)
        return json_response['id']
      else
        return url
      end
    rescue => e
      log.error "Error while shortening url #{e.messasge}"
      return url
    end
  end

end

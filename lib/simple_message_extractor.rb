module SimpleMessageExtractor

  require 'nokogiri'
  
  # checks a text for urls and returns the first one if found
  def self.simple_text html
    Nokogiri::HTML(html).text
  end

  def self.link_urls text
    return text if text.blank?
    text = text.gsub(/<a .*?>/,'').gsub("</a>",'')
    URI.extract(text, schemes = ['http','https']).each do |url|
      text.gsub!(url, "<a href='#{url}' target='_blank'>#{url}</a>")
    end
    text
  end
end

module OmniAuth
  module Strategies
    class OrgSAML < SAML

      def set_options_for_organization(organization)
        saml_sso = Sso.find_by_name('saml')
        if saml_sso
          org_sso = organization.org_ssos.where(sso_id: saml_sso.id).first
          if org_sso
            saml_config_keys = OrgSso.columns.map(&:name).select {|column| column.start_with?('saml')}
            saml_config_keys.each do |k|
              value = org_sso.send(k.to_sym)
              name = k.sub(/^saml_/, '')
              options[name] = value unless value.blank?
            end
          else
            fail!("Saml SSO configuration not found for organization_id=#{organization.id}")
          end
        else
          fail!("Saml SSO does not exist in the database")
        end
      end

      #override this method to retrieve the configuration at run time by using the host name
      def call!(env)

        unless env['rack.session']
          error = OmniAuth::NoSessionError.new('You must provide a session to use OmniAuth.')
          fail(error)
        end

        @env = env

        if on_auth_path? || on_path?("#{request_path}/metadata")
          current_org = Organization.find_by_request_host(request.host)
          if current_org
            set_options_for_organization(current_org)
          else
            fail!("Cannot look up organization by host name")
          end
        end

        super
      end
    end
  end
end
OmniAuth.config.add_camelization 'org_saml', 'OrgSAML'

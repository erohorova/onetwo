module OmniAuth
  module Strategies
    class LxpOAuth < OmniAuth::Strategies::OAuth2
      option :name, 'lxp_oauth'
      option :provider_ignores_state, true

      uid { [raw_info['user_id'], raw_info['sub'], raw_info['id']].detect(&:presence) }

      credentials do
        hash = {'token' => access_token.token}
        hash.merge!('refresh_token' => access_token.refresh_token) if access_token.expires? && access_token.refresh_token
        hash.merge!('id_token' => access_token.params['id_token']) if access_token.params && access_token.params['id_token']
        hash.merge!('expires_at' => access_token.expires_at) if access_token.expires?
        hash.merge!('expires' => access_token.expires?)
        hash
      end

      info do
        {
          email: raw_info['email'],
          first_name: raw_info['first_name'] || raw_info['firstName'] || raw_info['given_name'],
          image: raw_info['image'] || raw_info['picture_url'],
          last_name: raw_info['last_name'] || raw_info['lastName'] || raw_info['family_name'],
          name: raw_info['name'] || raw_info['Name'],
          preferred_username: raw_info['preferred_username']
        }
      end

      extra do
        {
          raw_info: raw_info
        }
      end

      def raw_info
        @raw_info ||= access_token.get(options['user_info_url']).parsed
      end

      # DESC:
      # Overriden because vanity URL receives port `80` in the base URL.
      # This causes SSL issue even we run on `https`.
      # However, this is overriden for both legacy and Okta-enabled SSOs.
      # Receives:
      # {
      #   'setup' => LxpOauthSetup,
      #   'skip_info' => false,
      #   'client_id' => "pavbLKVH6079!",
      #   'client_secret' => "31YUE338A5auR3jyQmuDlb7wECwIY-JyVB7j4PN4",
      #   'client_options' => {'site' => "https://dev-562233.oktapreview.com/", 'authorize_url' => "/oauth2/default/v1/authorize", 'token_url' => "/oauth2/default/v1/token"},
      #   'authorize_params' => {'response_mode' => "query", 'nonce' => "YsG76jo", 'idp' => "0oaevx1dcjm6rLsqt0h7"},
      #   'authorize_options' => [:scope],
      #   'token_params' => {},
      #   'token_options' => [],
      #   'auth_token_params' => {},
      #   'provider_ignores_state' => true,
      #   'name' => "lxp_oauth",
      #   'user_info_url' => "/oauth2/default/v1/userinfo",
      #   'redirect_uri' => "http://test-app.lvh.me:5000/auth/lxp_oauth/callback",
      #   'response_type' => "id_token",
      #   'state' => "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.Ijci.-EUaRDLlxBlFZSZpG4WSTB9135-IXzJ0kAGSaqGxGfI",
      #   'scope' => "openid email profile offline_access"
      # }
      def callback_url
        options['redirect_uri']
      end
    end
  end

end

OmniAuth.config.add_camelization 'lxp_oauth', 'LxpOAuth'

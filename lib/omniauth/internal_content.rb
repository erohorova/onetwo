module OmniAuth
  module Strategies
    class InternalContent < SAML

      def set_options_for_organization(organization)
        sso = Sso.find_by(name: 'internal_content')
        organization_sso = organization.org_ssos.find_by(sso_id: sso.id)
        if organization_sso
          saml_config_keys = OrgSso.columns.map(&:name).select {|column| column.start_with?('saml')}

          saml_config_keys.each do |k|
            value = organization_sso.send(k.to_sym) 
            name = k.sub(/^saml_/, '')
            options[name] = value unless value.blank?
          end
          options['skip_subject_confirmation'] = true
        else
          fail!("Saml SSO does not exist in the database")
        end
      end

      def callback_phase
        redirect_uri = [request.params['redirect_uri'], request.params['RelayState']].detect(&:presence)
        if redirect_uri
          request.session[:user_return_to] = redirect_uri
        end
        super
      end

      #override this method to retrieve the configuration at run time by using the host name
      def call!(env)
        unless env['rack.session']
          error = OmniAuth::NoSessionError.new('You must provide a session to use OmniAuth.')
          fail(error)
        end

        @env = env
        if on_auth_path? || on_path?("#{request_path}/metadata")
          current_organization = Organization.find_by_request_host(request.host)
          if current_organization
            set_options_for_organization(current_organization)
          else
            fail!("Cannot look up organization by host name")
          end
        end

        super
      end
    end
  end
end

OmniAuth.config.add_camelization 'internal_content', 'InternalContent'

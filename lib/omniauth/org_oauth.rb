module OmniAuth
  module Strategies
    class OrgOAuth < OmniAuth::Strategies::OAuth2
      option :name, 'org_oauth'
      option :user_info_url, nil

      uid { raw_info['id'] || raw_info['uid'] || raw_info['sub'] }

      info do
        {
          email: raw_info['email'],
          first_name: raw_info['first_name'] || raw_info['firstName'],
          image: raw_info['image'] || raw_info['picture_url'],
          last_name: raw_info['last_name'] || raw_info['lastName'],
          name: raw_info['name'] || raw_info['Name']
          # and anything else you want to return to your API consumers
        }
      end

      def raw_info
        @raw_info ||= access_token.get(options['user_info_url']).parsed
      end
     
    end
  end

end
OmniAuth.config.add_camelization 'org_oauth', 'OrgOAuth'



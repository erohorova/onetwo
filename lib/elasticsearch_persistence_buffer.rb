# allows buffering of elastic-persisted object saves; include with 'extend'
# flush must be called after creates/saves are done
# barebones; no save options, response, return value etc.
# extending classes maintain distinct buffers, so calls can be interleaved safely

module ElasticsearchPersistenceBuffer

  ENQUEUE_BUFFER_SIZE = 1000

  def buffer_save(obj, limit: ENQUEUE_BUFFER_SIZE)
    Thread.current[:elastic_bulk_buffer] ||= []
    Thread.current[:elastic_bulk_buffer] << {index: {
        _id: obj.id,
        data: obj.to_hash.except(:id, 'id').merge(updated_at: Time.now.utc)
    }}
    bulk_submit if Thread.current[:elastic_bulk_buffer].length >= limit
  end

  def buffer_create(pars, limit: nil)
    obj = self.new(pars)
    opts = {}
    opts.update limit: limit if limit
    buffer_save obj, opts
  end

  def buffer_delete(obj, limit: ENQUEUE_BUFFER_SIZE)
    Thread.current[:elastic_bulk_buffer] ||= []
    Thread.current[:elastic_bulk_buffer] << {
      delete: {_index: index_name, _type: document_type, _id: obj.id}
    }
    bulk_submit if Thread.current[:elastic_bulk_buffer].length >= limit
  end

  def buffer_flush
    bulk_submit
    refresh_index!
  end

  private

  def bulk_submit
    return if Thread.current[:elastic_bulk_buffer].blank?
    gateway.client.bulk(
        body: Thread.current[:elastic_bulk_buffer],
        index: index_name,
        type: document_type,
    )
  rescue => e
    log.error e
  ensure
    Thread.current[:elastic_bulk_buffer] = nil
  end
end

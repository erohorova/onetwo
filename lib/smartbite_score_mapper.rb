module SmartbiteScoreMapper

  SCORE_CONTRIBUTION = {
    'playback' => {
      'card' => {
        'owner' => 20,
        'actor' => 10
      },
      'video_stream' => {
        'owner' => 20,
        'actor' => 10
      }
    },
    'visit' => {
      'card' => {
        'owner' => 20,
        'actor' => 10
      },
    },
    'like' => {
      'card' => {
        'owner' => 10,
        'actor' => 4
      },
      'video_stream' => {
        'owner' => 10,
        'actor' => 4
      },
      'collection' => {
          'owner' => 10,
          'actor' => 4
      }
    },
    'create' => {
      'card' => {
        'owner' => 0,
        'actor' => 40
      },
      'video_stream' => {
        'owner' => 0,
        'actor' => 80
      },
      'channel' => {
        'owner' => 0,
        'actor' => 10
      },
      'team' => {
        'owner' => 0,
        'actor' => 10
      }
    },
    'publish' => {
      'collection' => {
        'owner' => 0,
        'actor' => 60
      },
    },
    'comment' => {
      'card' => {
        'owner' => 20,
        'actor' => 10
      },
      'collection' => {
          'owner' => 20,
          'actor' => 10
      },
      'video_stream' => {
        'owner' => 20,
        'actor' => 10
      }
    },
    'poll_create' => {
      'card' => {
        'owner' => 0,
        'actor' => 20
      }
    },
    'videostream_create' => {
      'card' => {
        'owner' => 0,
        'actor' => 80
      }
    },
    'poll_respond' => {
      'card' => {
        'owner' => 10,
        'actor' => 4
      }
    },
    'mark_completed_assigned' => {
      'card' => {
        'owner' => 10,
        'actor' => 40
      },
      'collection' => {
          'owner' => 10,
          'actor' => 40
      },
      'video_stream' => {
        'owner' => 10,
        'actor' => 40
      }
    },
    'mark_completed_self' => {
      'card' => {
        'owner' => 10,
        'actor' => 4
      },
      'collection' => {
        'owner' => 10,
        'actor' => 4
      },
      'video_stream' => {
        'owner' => 10,
        'actor' => 4
      }
    },
    'follow' => {
      'channel' => {
        'owner' => 10,
        'actor' => 4
      },
      'user' =>{
        'owner' => 10,
        'actor' => 4
      }
    },
    'assign' => {
      'card' => {
        'owner' => 10,
        'actor' => 0
      },
      'video_stream' => {
        'owner' => 10,
        'actor' => 0
      }
    },
    'content_publish' => {
      'card' => {
        'owner' => 10,
        'actor' => 0
      }
    },
    'join' => {
      'team' => {
        'owner' => 0, # owner of team is not saved
        'actor' => 4
      }
    }
  }

  def self.score_map(action, object_type, user_type)
    SCORE_CONTRIBUTION.try(:[], action).try(:[], object_type).try(:[], user_type) || 0
  end

  def self.smartbite_score_increments(attributes)
    ret = []
    unless attributes[:actor_id].nil?
      score_contribution_to_actor = score_map(attributes[:action], attributes[:object_type], "actor")
      ret << [attributes[:actor_id], score_contribution_to_actor]
    end

    if !attributes[:owner_id].nil? && attributes[:owner_id] != attributes[:actor_id]
      score_contribution_to_owner = score_map(attributes[:action], attributes[:object_type], "owner")
      ret << [attributes[:owner_id], score_contribution_to_owner]
    end
    ret
  end
end

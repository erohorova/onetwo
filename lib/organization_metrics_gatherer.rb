module OrganizationMetricsGatherer

  METRICS = [:smartbites_consumed, :smartbites_created, :average_session, :total_users, :new_users, :active_users, :engagement_index]

  class << self
    # output format:
    # {
    #   smartbites_created: 0,
    #   smartbites_consumed: 0,
    #   active_users: 0,
    #   average_session: 0,
    #   total_users: 0,
    #   new_users: 0,
    #   engagement_index: 0
    # }
    def current_period_data(organization: , period: , offset: )
      # return timerange for period with offset
      # for eg: period: :week, offset: 72
      # [2016-05-23 00:00:00 +0000, 2016-05-30 00:00:00 +0000]
      timerange = PeriodOffsetCalculator.timerange(period: period, offset: offset)

      # return total number of users before particular time
      total_users_count = organization.real_users_count_before(timerange.last)

      # returns metrics for an organization with an offset
      # output format:
      # {smartbites_created: 0, smartbites_consumed: 0, active_users: 0, average_session: 0}
      data = UserLevelMetric.get_organization_metrics(organization_id: organization.id, period: period, offset: offset)

      # returns number of new users created for particular period
      new_users_count = organization.get_new_users_count(period: period, offset: offset)

      data.merge({total_users: total_users_count, new_users: new_users_count, engagement_index: calculate_engagement_index(data[:active_users], total_users_count)})
    end

    # output format:
    # {
    #   smartbites_created: 0,
    #   smartbites_consumed: 0,
    #   active_users: 0,
    #   average_session: 0,
    #   total_users: 0,
    #   new_users: 0,
    #   engagement_index: 0
    # }
    def metrics_over_span(organization: , team_id: , period: , offsets: , timeranges: )
      # returns metrics for an organization with offset range
      # output format:
      # {smartbites_created: 0, smartbites_consumed: 0, active_users: 0, average_session: 0}
      data = UserLevelMetric.get_organization_metrics_over_span(
        organization_id: organization.id,
        team_id: team_id,
        period: period,
        begin_offset: offsets.first,
        end_offset: offsets.last
      )
      
      # returns number if new users created over a timerange
      new_users_count = if team_id.present?
        Team.find(team_id).get_new_users_count_over_span(
          begin_time: timeranges.first.first,
          end_time: timeranges.last.last
        )
      else
        organization.get_new_users_count_over_span(
          begin_time: timeranges.first.first,
          end_time: timeranges.last.last
        )
      end

      # returns number of total users before particular time
      total_users_count = if team_id.present?
        Team.find(team_id).real_users_count_before(timeranges.last.last)
      else
        organization.real_users_count_before(timeranges.last.last)
      end

      data.merge({total_users: total_users_count, new_users: new_users_count, engagement_index: calculate_engagement_index(data[:active_users], total_users_count)})
    end

    # output format:
    # {
    #   smartbites_created: 0,
    #   smartbites_consumed: 0,
    #   active_users: 0,
    #   average_session: 0,
    #   total_users: 0,
    #   new_users: 0,
    #   engagement_index: 0
    # }
    def comparison_data(organization: , period: , offset: )
      # returns timeranges for current period
      # for eg: period: :day, offset: 786
      # output format:
      # [[2017-03-01 00:00:00 UTC, 2017-03-02 00:00:00 UTC]]
      drill_down_timeranges_current_period = PeriodOffsetCalculator.drill_down_timeranges(period: period, offset: offset)

      # returns offsets for current period
      # for eg: period: :week, offset: 72
      # [:day, [504, 505, 506, 507, 508, 509, 510]]
      drill_down_offsets_current_period = PeriodOffsetCalculator.drill_down_period_and_offsets(period: period, offset: offset)


      # returns timesrange upto current date
      drill_down_timeranges_upto_now = drill_down_timeranges_current_period.select{|tr| tr.first <= Time.now.utc}
      # returns offset upto current date
      drill_down_offsets_upto_now = drill_down_offsets_current_period.last.take(drill_down_timeranges_upto_now.count)

      # no time ranges to compare
      if drill_down_timeranges_upto_now.length == 0
        return Hash[METRICS.map{|metric| [metric, 0]}]
      end

      # returns offsets for previous period (ie before current period)
      drill_down_offsets_previous_period = PeriodOffsetCalculator.drill_down_period_and_offsets(period: period,
                                                                                                offset: offset-1)
      # returns timeranges for previous period (ie before current period)
      drill_down_timeranges_previous_period = PeriodOffsetCalculator.drill_down_timeranges(period: period,
                                                                                           offset: offset-1)

      # return period offsets (to compare previous aganist current)
      previous_comparable_period_offsets = drill_down_offsets_previous_period.
        last.
        take(drill_down_timeranges_upto_now.count)

      # return timeranges (to compare previous aganist current)
      previous_comparable_timeranges = drill_down_timeranges_previous_period.
        take(drill_down_timeranges_upto_now.count)

      _previous_comparable_period_data = metrics_over_span(organization: organization, team_id: nil, period: :day, offsets: previous_comparable_period_offsets, timeranges: previous_comparable_timeranges)
      _current_comparable_period_data = metrics_over_span(organization: organization, team_id: nil, period: :day, offsets: drill_down_offsets_upto_now, timeranges: drill_down_timeranges_upto_now)

      # calucates percentage changes for current with respect to previous data
      Hash[METRICS.map{|metric| [metric, calculate_change(_current_comparable_period_data[metric], _previous_comparable_period_data[metric])]}]
    end

    # output format (for week period):
    # {
    #   smartbites_created: [0, 0, 0, 0, 0, 0, 0],
    #   smartbites_consumed: [0, 0, 0, 0, 0, 0, 0],
    #   active_users: [0, 0, 0, 0, 0, 0, 0],
    #   average_session: [0, 0, 0, 0, 0, 0, 0],
    #   total_users: [0, 0, 0, 0, 0, 0, 0],
    #   engagement_index: [0, 0, 0, 0, 0, 0, 0],
    #   new_users: [0, 0, 0, 0, 0, 0, 0]
    # }
    def drill_down_data(organization: , period: , offset: )

      data = UserLevelMetric.get_organization_drill_down_data(organization_id: organization.id, period: period, offset: offset)

      drill_down_timeranges = PeriodOffsetCalculator.drill_down_timeranges(period: period, offset: offset)
      drill_down_total_users_count = drill_down_timeranges.map{|tr| organization.real_users_count_before(tr.last)}

      data[:total_users] = drill_down_total_users_count

      data[:engagement_index] = drill_down_timeranges.map.with_index{|tr, index| calculate_engagement_index(data[:active_users][index], drill_down_total_users_count[index])}
      data[:new_users] = organization.get_new_users_drill_down_data(period: period, offset: offset)
      data
    end

    # Returns MAU (Monthly Active Users) and DAU (Daily Active Users) for a month
    def platform_metrics_report_data(organization: , year: , month: )
      timestamp = Time.utc(year, month)
      month_offset = PeriodOffsetCalculator.month_offset(timestamp)
      month_data = current_period_data(organization: organization, period: :month, offset: month_offset)
      daily_drilldown = drill_down_data(organization: organization, period: :month, offset: month_offset)

      # total users count for month
      users_count = month_data[:total_users]

      # active users count for month
      mau = month_data[:active_users]

      # This is to ensure we don't count future 0 entries for average DAU
      number_of_drilldown_days = PeriodOffsetCalculator.drill_down_timeranges(period: :month, offset: month_offset).select{|tr| tr.first <= Time.now.utc}.length

      applicable_dau_numbers = daily_drilldown[:active_users].take(number_of_drilldown_days)
      dau = if applicable_dau_numbers.length > 0
              applicable_dau_numbers.sum / applicable_dau_numbers.length.to_f
            else
              0
            end

      {team: organization.host_name, year: year, month: month, mau: mau, users_count: users_count, dau: dau}
    end

    # percentage change of current data with respect to previous data
    def calculate_change(current, previous)
      if current == 0 && previous == 0
        0
      elsif previous == 0
        "Inf"
      else
        ((current - previous) * 100)/ previous
      end
    end

    # returns percentage of active users against total users
    # active users: users with (time_spent > 0, is_edcast_admin: false, is_complete: true, is_suspended: false)
    def calculate_engagement_index(active_users_count, total_users_count)
      total_users_count > 0 ? (active_users_count * 100 / total_users_count) : 0
    end

    def drill_down_data_for_custom_period(organization: , team_id: , timerange: ,period: :day)
      data = UserLevelMetric.get_organization_drill_down_data_for_timerange(organization_id: organization.id, team_id: team_id, period: period, timerange: timerange)

      drill_down_timeranges = PeriodOffsetCalculator.drill_down_timeranges_for_timerange(period: period, timerange: timerange)
      drill_down_total_users_count = if team_id.present?
        drill_down_timeranges.map{|tr| Team.find(team_id).real_users_count_before(tr.last)}
      else
        drill_down_timeranges.map{|tr| organization.real_users_count_before(tr.last)}
      end

      data[:total_users] = drill_down_total_users_count

      data[:engagement_index] = drill_down_timeranges.map.with_index{|tr, index| calculate_engagement_index(data[:active_users][index], drill_down_total_users_count[index])}
      data[:new_users] = if team_id.present?
        Team.find(team_id).get_new_users_drill_down_data_for_timeranges(timeranges: drill_down_timeranges)
      else
        organization.get_new_users_drill_down_data_for_timeranges(timeranges: drill_down_timeranges)
      end
      data
    end
  end
end

#Send push notifications in china region via pushy api
#apps in china region will also listen to pubnub channels
#only for android users
#EP-16763
class PushyNotifier
  CHINA_TIMEZONES = ['Asia/Shanghai', 'Asia/Urumqi', 'Asia/Hong_Kong', 'Asia/Macau']
  def self.notify(opts)
    return false if opts[:user_id] and user = User.find(opts[:user_id]) and !CHINA_TIMEZONES.include?(user.profile.time_zone)

    organization = Organization.find_by_request_host(opts[:host_name])
    api_keys = [organization.get_settings_for('whitelabel_pushy_api_key'), Settings.pushy.api_key].compact.uniq
  	payload = {
  		       summary: opts[:content],
             deep_link_id: opts[:deep_link_id].to_s,
  		       deep_link_type: opts[:deep_link_type],
  		       notification_type: opts[:notification_type],
  		       host_name: opts[:host_name]
  		      }

    opts[:channel_names].each do |channel_name|
      api_keys.each do |api_key|
        send_push(api_key, channel_name, payload)
      end
    end
  end

  def self.send_push(api_key, topic, payload)
    conn = Faraday.new(url: "#{Settings.pushy.url}?api_key=#{api_key}")
  	result = conn.post do |req|
      req.headers['Content-Type'] = 'application/json'
      req.body = {
                  to: "/topics/#{topic}",
                  data: payload
                }.to_json
    end
    JSON.parse(result.body)
  end
end

__END__
#Smaple Payload
#{user_id: 1749, deep_link_id: 39753, deep_link_type: "card", notification_type: "assigned_content", host_name: "stark.edcasting.co", content: "Mansi Panchmia has assigned you: New Journey 15th June...", channel_names: ["User-1749-private-e22801a0a987a4954c5ea8086c67004b"]}

Sample Request
conn = Faraday.new(url: "https://api.pushy.me/push?api_key=f2cfbcf68b6fcf4c3008e055b3cc70f19de09344153bcfaa6dd5b3d239cef657")

result = conn.post do |req|
  req.headers['Content-Type'] = 'application/json'
  #device token payload - success
  req.body = {"to" => 'ed2471141eb53923c9a837', "data" => {"message" => "This message is from web"}}.to_json
  #topic(pubnub_channels) payload - not working
  #req.body = {"to" => '/topics/User-194-private-649520a31adc7aeabf5d3ecdf2bf5bdd', "data" => {"message" => "This message is from web"}}.to_json
end

JSON.parse(result.body)
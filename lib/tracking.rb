module Tracking

  class << self

    def track(*args)
      #send to elastic search metric record
      MetricRecorderJob.perform_later(*args)
    end

    def track_act(opts)
      action_type = {
          'upvote' => 'like',
          'unupvote' => 'unlike',
      }[opts[:action]] || opts[:action]
      unless action_type && opts[:user_id]
        log.error opts and return
      end

      track(opts[:user_id],
            'act',
            opts.except(:user_id, :action).merge(type: action_type),
      )
    end

    def track_entry(opts)
      return unless @tracker

      type, provider, reserved = opts[:access_uri].split('.')
      cookie_value = { type: type, provider: provider }
      cookie = nil

      if type == 'signup'
        cookie_value.merge! reserved: (reserved == 'reserved').to_s
        data = {
            '$id' => opts[:user_id],
            '$signup_provider' => provider,
            '$signup_date' => opts[:access_at_date],
            '$signup_platform' => opts[:platform],
            '$signup_reserved_handle' => cookie_value[:reserved],
        }

        if opts[:platform] == 'web'
          cookie_value[:data] = data
        else
          set(opts[:user_id], data)
        end
      end

      if opts[:platform] == 'web'
        cookie = { value: cookie_value.to_json, expires: 1.hour.since }
      else
        track(opts[:user_id], 'entry', cookie_value)
      end

      return cookie
    end

    def track_daily(opts)
      return unless @tracker
      access_uri = opts[:access_uri]
      if access_uri.starts_with?('login.')
        event_name = 'daily_login'
        provider = access_uri.split('.')[1]
        uri = nil
      elsif access_uri.starts_with?('signup.')
        event_name = 'daily_signup'
        provider = access_uri.split('.')[1]
        uri = nil
      else
        event_name = 'daily_access'
        provider = nil
        uri = opts[:access_uri]
      end
      # this method is called from a job; don't invoke another job
      track_now(opts[:user_id], event_name, {
                              provider: provider,
                              uri: uri, platform: opts[:platform],
                              access_at_date: opts[:access_at_date], annotation: opts[:annotation]
                          }.compact)
    end
  end

  module Visit

    def tracking_params(target)
      case target
        when User
          {
              object: 'user',
              user: target.handle,
          }
        when Channel
          {
              object: 'channel',
              channel: target.label,
          }
        else
          {}
      end
    end

  end

end

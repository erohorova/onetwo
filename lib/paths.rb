module Paths
  class << self
    def first_level
      routes = Rails.application.routes.routes
      # only worry about the get path and route dirs like /assets, /dbadmin etc
      paths = routes.select {|r| ((r.verb == //) || (r.verb == /^GET$/))}.
        collect {|r| r.path.spec.to_s}

      handles_from_routes = paths.collect {|path| match_path_segment(path)}.compact.uniq
      handles_from_routes.map {|x| "@#{x}"}
    end

    def match_path_segment(path)
      if path.count('/') == 1 # removes the / at the beginning
        path[1..-1].gsub(/\(.*\)/, '') # removes (.format) from the url path
      end
    end
  end
end

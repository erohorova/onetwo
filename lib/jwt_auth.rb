require 'jwt'
class JwtAuth
  ALGORITHM = 'HS256'

  #Issue JWT for user.
  #payload, user id, client user id, and timestamp
  def self.encode(user)
    #TODO: Do we need client_user_id?
    client_user_id = user.clients_users.last.try(:client_user_id)
    payload = { user_id: user.id, client_user_id: client_user_id, timestamp: Time.now }
    JWT.encode(payload, Settings.features.integrations.secret, ALGORITHM)
  end

  def self.decode(token)
    jwt_payload, jwt_header = JWT.decode(token, Settings.features.integrations.secret)
    jwt_payload
  rescue
    nil
  end

  def self.renew(user)
    user.jwt_token({is_web: true})
  end
end

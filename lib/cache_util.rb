# frozen_string_literal: true

module CacheUtil
  # Checks the cache to see if the given key is present.
  # If so, returns the value. If not, evaluates the block and sets the cache
  # key to that value.
  def cache_get_set(cache_key, **cache_set_opts, &blk)
    # Return early if the results are found in the cache.
    cached_results = Rails.cache.fetch(cache_key)
    return cached_results if cached_results

    blk.call.tap do |results|
      # Store the results in the cache before returning
      Rails.cache.write cache_key, results, **cache_set_opts
    end
  end
end
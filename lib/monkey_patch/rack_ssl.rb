module ActionDispatch
  SSL.class_eval do
    def call_with_ssl_by_pass(env)
      if env['HTTP_CLOUDFRONT_FORWARDED_PROTO'] == 'https' || env['PATH_INFO'] == '/health_checks' || env['PATH_INFO'].start_with?('/assets/')
        @app.call(env)
      else
        call_without_ssl_by_pass(env)
      end
    end
    alias_method_chain :call, :ssl_by_pass
  end
end
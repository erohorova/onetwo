module ActionDispatch
  Request.class_eval do
    def scheme
      if @env['HTTP_CLOUDFRONT_FORWARDED_PROTO'] == 'https'
        'https'
      else
        super
      end
    end
  end
end

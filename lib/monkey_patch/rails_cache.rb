class ActiveSupport::Cache::Store
  def save_block_result_to_cache(name, options)
    result = instrument(:generate, name, options) do |payload|
      yield(name)
    end

    write(name, result, options) unless result.nil? && options[:unless_nil]
    result
  end

  def fetch_with_cacheable(name, options = nil, &block)
    if name.is_a?(Cacheable)
      fetch_without_cacheable(name.key, options) do
        name.value
      end
    else
      fetch_without_cacheable(name, options, &block)
    end
  end
  alias_method_chain :fetch, :cacheable
  
  def fetch_multi_with_cacheable(*names)
    options = names.extract_options!
    options = merged_options(options)

    _key_map = {}
    _name_strings = names.map do |name|
      if name.is_a?(Cacheable)
        key = name.key
        _key_map[key] = name
        key
      else
        name
      end
    end
    
    results = read_multi(_name_strings, options)

    _name_strings.map do |name|
      results.fetch(name) do
        if _key_map[name]
          value = _key_map[name].value
        else
          value = yield name
        end
        write(name, value, options)
        value
      end
    end
  end
  alias_method_chain :fetch_multi, :cacheable
end
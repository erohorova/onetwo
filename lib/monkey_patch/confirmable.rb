module Devise
  module Models
    module Confirmable

      protected
      def pending_any_confirmation
        yield
      end
    end
  end
end

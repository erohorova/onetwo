# methods to supply rationales for retrieved cards

class CardsRationales

  # card with rationale: {"id"=>3, "rationale"=>{"text"=>"John Doe posted this", "image_url"=>"[url]"}, ... }

  # words to use in rationale (some are values of CardsUser.state)
  ACTION_TERMS = {
      'upvote' => 'liked',
      'bookmark' => 'bookmarked',
      'comment' => 'commented on',
      'announcement' => 'announced',
      'creation' => 'created',
      'comment_upvote_update' => 'liked',
      'card_upvote_update' => 'liked',
      'post_upvote_update' => 'liked',
      'comment_update' => 'commented on',
      'social_upvote' => 'liked',
      'social_comment' => 'commented on',
      'social_post' => 'created',
      'social_card' => 'created'
  }

  # message to be displayed top right of a card on the landing page
  # currently defaults to just a set of people who liked the card
  def self.supply_destination_page_message(card)
    cus = CardsUser.where(card_id: card['id'], state: 'upvote')
    total_upvotes = cus.count
    us = User.where(id: cus.pluck(:user_id)).limit(2)
    message = ''
    case total_upvotes
    when 0
      message = 'Be the first to like this!'
    when 1
      message = "<a href='/#{us.first.handle_no_at}'>#{us.first.name}</a> liked this!"
    when 2
      message = "<a href='/#{us[0].handle_no_at}'>#{us[0].name}</a> and <a href='/#{us[1].handle_no_at}'>#{us[1].name}</a> liked this!"
    when 3
      message = "<a href='/#{us[0].handle_no_at}'>#{us[0].name}</a> and #{total_upvotes - 1} others liked this!"
    else
      message = "<a href='/#{us[0].handle_no_at}'>#{us[0].name}</a>, <a href='/#{us[1].handle_no_at}'>#{us[1].name}</a> and #{total_upvotes - 2} others liked this!"
    end
    return message
  end

  def self.supply_rationales(cards)
    cards.each do |card|
      if card.nil?
        msg = 'Nil card returned'
        msg += " in request: #{request.try(:env)}" if defined?(request)
        log.error msg
        next
      end

      next unless queue_data = card['queue_data']
      next unless event = queue_data.first
      next unless rationale = event['rationale']
      next unless event_type = event['event']

      user_ids = []
      case event_type
        when 'announcement'
          text = "Important announcement from #{event['rationale']['group_name']}"
        when 'creation'
          text = nil
        when 'comment_upvote_update'
          text = "#{event['rationale']['user_name']} likes your comment!"
          user_ids = [event['rationale']['user_id']]
        when 'post_upvote_update', 'card_upvote_update'
          text = "#{event['rationale']['user_name']} liked your post!"
          user_ids = [event['rationale']['user_id']]
        when 'comment_update'
          text = "New comment by #{event['rationale']['user_name']}"
          user_ids = [event['rationale']['user_id']]
        when 'social_card', 'social_post'
          # this is when a friend created something. The creator's face is prominently displayed anyway. Don't need
          # specific text
          text = nil
        when 'social_upvote', 'social_comment'
          # friend action
          text = "#{event['rationale']['user_name']} #{ACTION_TERMS[event_type]} this post"
          user_ids = [event['rationale']['user_id']]
      end

      act = ACTION_TERMS[event_type].dup
      act.slice!(' on') # 'acted on' -> 'acted'
      rationale['update_act'] = act
      rationale['user_ids'] = user_ids
      rationale.merge!({'text' => text}) unless text.nil?
      card['rationale'] = rationale
    end
  end

end

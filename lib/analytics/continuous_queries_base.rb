module Analytics
  class ContinuousQueriesBase

    # Launches a CQ which runs asynchronously on influx.
    def self.run_continuous_query
      INFLUXDB_CLIENT.create_continuous_query(
        cq_name, db, build_backfill_query,
        resample_every: self::RESAMPLE_EVERY,
        resample_for: self::RESAMPLE_FOR
      )
    end

    # The name of the CQ
    def self.cq_name
      "fetch_#{self::MEASUREMENT}"
    end

    # The database name
    def self.db
      Settings.influxdb.database
    end

    # Command to drop the CQ's measurement
    def self.drop_measurement
      INFLUXDB_CLIENT.query "DROP MEASUREMENT #{self::MEASUREMENT}"
    end

    # Command to drop the CQ
    def self.drop_continuous_query
      INFLUXDB_CLIENT.delete_continuous_query(cq_name, db)
    rescue InfluxDB::QueryError => e
      raise unless e.message == "continuous query not found"
    end

    # Builds a SELECT INTO given keys, measurement, and FROM clause
    def self.select_into(selections, into_measurement, from_clause)
      <<-TXT
        SELECT #{selections} \
        INTO #{into_measurement} \
        FROM #{from_clause}
      TXT
    end

    # conditions is a list of strings
    # joins them into a "WHERE <condition> AND <condition> ..." string
    def self.where_clause(conditions=[])
      return "" unless conditions.any?
      "WHERE " + conditions.join(" AND ")
    end

  end
end
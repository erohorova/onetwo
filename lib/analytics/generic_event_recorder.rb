# frozen_string_literal: true

module Analytics
  class GenericEventRecorder

    # A whitelist for valid events and their measurements
    # Events must be added to this file to record from this class.
    MEASUREMENTS_BY_EVENT = YAML.load(
      File.read("config/analytics/valid_generic_events.yml")
    ).each_with_object({}) do |(measurement, events), memo|
      events.each { |event| memo[event] = measurement }
    end

    # Key = measurement, val = attr
    # Whenever the 'measurement' param has this value, this param
    # is also required.
    REQUIRED_ATTRS = {
      "cards"         => "card_id",
      "groups"        => "group_id",
      "channels"      => "channel_id"
    }

    # =====================================s====
    # Entry point methods
    # =========================================

    def initialize(user_id:, org_id:, params:)
      @user_id           = user_id
      @org_id            = org_id
      @metadata          = RequestStore.read(:request_metadata)
      @event             = params['event']
      @measurement       = self.class::MEASUREMENTS_BY_EVENT[@event]
      @attributes        = params['attributes']
      @required_attr     = find_required_attr(@measurement)
      @required_attr_val = params[@required_attr]
    end

    # Returns [<event hash>, <error string>]
    # One of them will be nil, depending on if there was an error or not.
    # This event hash is not recorded immediately, but is enqueued to sidekiq.
    def enqueue_recording_job
      err = run_validations
      return [nil, err] if err
      event_obj = build_event_obj

      Analytics::MetricsRecorderJob.perform_later(
        recorder:    self.class.name,
        measurement: @measurement,
        event_obj:   event_obj
      )
      [event_obj, nil]
    end

    # This is the callback which gets hit by Sidekiq.
    # It actually records the event to Influx.
    def self.record(measurement:, event_obj:)
      Analytics::MetricsRecorder.influxdb_record(
        measurement,
        event_obj,
      )
    end

    # =========================================
    # Helper methods
    # =========================================

    # Returns error from the first validation to fail, or nil
    def run_validations
      %i{
        validate_event_present
        validate_known_event
        validate_required_attrs
      }.reduce(nil) do |err, fn|
        err = send(fn)
        break err if err
      end
    end

    # Builds an influx event object that will be recorded
    def build_event_obj
      event_obj             = {}
      event_obj[:tags]      = extract_tags
      event_obj[:values]    = extract_values
      event_obj[:timestamp] = build_timestamp

      Analytics::MetricsRecorder.merge_additional_data(event_obj, @metadata)
      event_obj
    end

    def extract_tags
      tags = {
        event:    @event,
        _user_id: @user_id,
        org_id:   @org_id
      }
      # The required_attr is prepended with an underscore and added to tags
      # e.g. _card_id or _user_id
      tags.merge!(:"_#{@required_attr}" => @required_attr_val) if @required_attr
      tags
    end

    def extract_values
      values = {
        user_id:    @user_id,
        attributes: @attributes&.to_json
      }
      # The required_attr is prepended with an underscore and added to tags
      # e.g. _card_id or _user_id
      values.merge!(:"#{@required_attr}" => @required_attr_val) if @required_attr
      values
    end

    def build_timestamp
      InfluxdbRecorder.to_ns(@measurement, Time.now.to_i)
    end

    # =========================================
    # Validations
    # =========================================

    # One or zero attributes may be required, depending on the measurement
    def find_required_attr(measurement)
      self.class::REQUIRED_ATTRS[measurement]
    end

    # Returns error or nil
    def validate_required_attrs
      return unless @required_attr
      return if @required_attr_val.is_a?(String)
      (
        "Param '#{@required_attr}' is required " +
        "for measurement '#{@measurement}'. " +
        "It is either missing or not a string."
      )
    end

    # Returns error or nil
    def validate_event_present
      return if @event.is_a?(String)
      (
        "Param 'event' is missing or not a string."
      )
    end

    # Returns error or nil
    def validate_known_event
      return if self.class::MEASUREMENTS_BY_EVENT[@event]
      (
        "Param 'event' is not registered. " +
        "Valid events: " +
        "#{self.class::MEASUREMENTS_BY_EVENT.keys.join(",")}"
      )
    end

  end
end

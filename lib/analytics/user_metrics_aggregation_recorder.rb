# frozen_string_literal: true

# =========================================
# THIS CLASS SAVED PER-USER, PER-DAY AGGREGATIONS TO MYSQL
# IT SAVES TO MYSQL SO IT CAN EASILY BE QUERIED USING WHERE..IN
#
# NOTE: THIS DOES NOT CALCULATE THE METRICS FROM SCRATCH!
# IT JUST COPIES THE DATA OVER FROM INFLUX FOR EASIER QUERYING
# =========================================

module Analytics
  class UserMetricsAggregationRecorder < Analytics::MetricsRecorder

    attr_reader :org_ids, :start_time, :end_time

    # This is the Influx measurement that data gets taken from.
    SOURCE_MEASUREMENT = "user_scores_daily"

    # The SQL table that data gets pushed to.
    DESTINATION_TABLE = UserMetricsAggregation

    # These are columns present on both the Influx measurement and SQL table
    # If all of these are null or zero, a SQL record won't get created.
    COUNTS = %w{
      smartbites_commented_count
      smartbites_completed_count
      smartbites_consumed_count
      smartbites_created_count
      smartbites_liked_count
      time_spent_minutes
      total_smartbite_score
      total_user_score
    }

    # Opts are org_ids, start_time, and end_time. All are optional.
    # If org_ids is given the recorder will only run for those ids.
    # Otherwise it will run for all orgs.
    def initialize(**opts)
      base_time = Time.now.utc
      defaults = {
        org_ids: nil,
        start_time: (base_time.beginning_of_day - 1.days).to_i,
        end_time: (base_time.end_of_day - 1.days).to_i
      }
      defaults.merge(opts).each do |key, val|
        instance_variable_set "@#{key}", val
      end
    end

    def run
      query = build_query_for_orgs
      result_sets = query.resolve
      insertion_records = build_insertion_records(result_sets)
      push_results(insertion_records)
      "#{insertion_records.length} records created"
    end

    # Builds an InfluxQuery object to query all the given orgs
    def build_query_for_orgs
      query = InfluxQuery.new(SOURCE_MEASUREMENT)
      if org_ids.is_a?(Array)
        query.add_where_in_filter!(:org_id, "org_id", org_ids.map(&:to_s))
      end
      query.add_time_filters!(
        start_date: start_time,
        end_time: end_time
      )
    end

    # Converts the Influx response to a flat array of records
    def build_insertion_records(result_sets)
      result_sets.flat_map do |result_set|
        result_set["values"].map do |val|
          start_time = Time.parse(val["time"])
          obj = {
            start_time:                 start_time.to_i,
            end_time:                   start_time.end_of_day.to_i,
            organization_id:            val["org_id"],
            user_id:                    val["user_id"]
          }
          COUNTS.each do |col_name|
            obj[col_name.to_sym] = val[col_name] || 0
          end
          obj
        end
      end
    end

    # We have a problem with duplicate UserAggregationRecord records sometimes.
    # We only want one record for each user_id/start_time/organization_id set
    # We cannot simply add a uniqueness validation to the database, because
    # then a single invalid record would cause the whole bulk insertion to fail.
    # Instead, we can query SQL for records that have the start_time we're
    # inserting for, and remove the insertion records that already have matching
    # user_id/organization_id in SQL for these times.
    #
    # Returns a filtered version of insertion_records
    def prevent_duplicates(insertion_records)
      # First get a list of unique start times for the insertion records
      distinct_start_times = insertion_records.map do |record|
        record[:start_time]
      end.uniq

      # Do the same thing for organization_id
      distinct_org_ids = insertion_records.map do |record|
        record[:organization_id]
      end.uniq

      # Then group all the insertion records by their attribute sets.
      indexed_records = insertion_records.group_by do |record|
        record.
          slice(:start_time, :user_id, :organization_id).
          stringify_keys.
          transform_values(&:to_i)
      end

      # Now query SQL for records with the unique start times and org ids
      existing_aggs = UserMetricsAggregation.where(
        start_time:      distinct_start_times,
        organization_id: distinct_org_ids
      )

      # Find the attribute sets which already have records in SQL
      existing_agg_attr_sets = existing_aggs.index_by do |record|
        record.attributes.slice("start_time", "user_id", "organization_id")
      end

      # Now we filter the insertion records by the attributes found in SQL
      return indexed_records.reject do |attr_set, records|
        existing_agg_attr_sets.key?(attr_set)
      end.values.flatten(1)
    end

    # Pushes records to SQL in a bulk insertion.
    # This means that model-level validations are not run.
    def push_results(insertion_records)
      insertion_records = prevent_duplicates(insertion_records)
      insertion_records.in_groups_of(10_000, false).each do |record_group|
        log.debug("creating #{record_group.length} UserMetricsAggregation records")
        UserMetricsAggregation.bulk_insert do |worker|
          record_group.each do |record|
            # in_groups_of can use nil elements as placeholders
            next unless record
            # Skip any records that don't have at least one positive numeric column.
            next unless COUNTS.any? do |column_name|
              attr = record[column_name.to_sym]
              attr && attr > 0
            end
            worker.add(record)
          end
        end
      end
    end

  end
end

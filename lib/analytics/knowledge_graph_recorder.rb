# frozen_string_literal: true


=begin

testing:

Analytics::KnowledgeGraphRecorder.run([15])
Analytics::KnowledgeGraphRecorder.run_backfill([15], num_days: 20)

=end

module Analytics
  class KnowledgeGraphRecorder

    CARD_EVENTS = %w{
      card_viewed
      card_liked
      card_comment_added
      card_shared
      card_bookmarked
      card_marked_as_complete
      card_created
    }

    def self.run(org_ids=Organization.pluck(:id), start_time: nil, end_time: nil)
      # Each day, we make a record for the previous day.
      beginning_of_day = Time.now.utc.beginning_of_day
      start_time     ||= (beginning_of_day - 1.days).to_i
      end_time       ||= (beginning_of_day.end_of_day - 1.days).to_i

      records = org_ids.in_groups_of(50).flat_map do |org_ids_group|
        org_ids_group.compact!
        fetch_card_events(org_ids_group, start_time, end_time)
      end

      user_topics_map = map_users_to_topics(records)

      persist_data(start_time, end_time, user_topics_map)
    end

    def self.run_backfill(org_ids=Organization.pluck(:id), num_days: 20)
      beginning_of_day = Time.now.utc.beginning_of_day
      num_days.times do |i|
        puts "day #{i}/#{num_days}"
        start_time = (beginning_of_day - (2 + i).days).to_i
        end_time = (beginning_of_day.end_of_day - (2 + i).days).to_i
        run(org_ids, start_time: start_time, end_time: end_time)
      end
    end

    def self.fetch_card_events(org_ids_group, start_time, end_time)
      query = InfluxQuery
        .new("cards")
        .add_where_in_filter!(:org_id, "org_id", org_ids_group.map(&:to_s))
        .add_where_in_filter!(:event, "event", CARD_EVENTS)
        .group_by!("org_id,_user_id,_card_id")
        .add_time_filters!(start_date: start_time, end_date: end_time)
        .select!("COUNT(card_id)")

      query.resolve
    end

    def self.fetch_card_topics_map(records)
      # Retrieve topics for each of the cards included in these results
      card_ids = records.map { |record| record["tags"]["_card_id"] }
      card_ids.in_groups_of(200).flat_map do |card_ids_group|
        card_ids_group.compact!
        Card.where(
          id: card_ids_group
        ).where.not(
          taxonomy_topics: nil
        ).select(
          :id, :taxonomy_topics
        )
      end.index_by(&:id).transform_values(&:taxonomy_topics)
    end

    def self.map_users_to_topics(records)
      card_topics_map = fetch_card_topics_map(records)

      user_topics_map = Hash.new { |hash, key| hash[key] = Hash.new(0) }

      records.each do |record, memo|
        org_id, user_id, card_id = record["tags"].values_at(
          *%w{org_id _user_id _card_id}
        ).map(&:to_i)
        count = record["values"][0]["count"]
        taxonomies = card_topics_map[card_id]
        next unless taxonomies.is_a?(Array) && taxonomies.any?(&:present?)
        taxonomies.each do |taxonomy|
          user_topics_map[[org_id, user_id]][taxonomy] += count
        end
      end
      user_topics_map
    end

    def self.persist_data(start_time, end_time, user_topics_map)
      user_topics_map.to_a.in_groups_of(5000) do |records|
        records.compact!
        records.each do |(org_id, user_id), topic_counts|
          total_activities_count = topic_counts.values.sum
          UsersTopicsActivity.bulk_insert do |worker|
            worker.add(
              organization_id:        org_id,
              user_id:                user_id,
              start_time:             start_time,
              end_time:               end_time,
              topic_counts:           topic_counts.to_json,
              total_activities_count: total_activities_count
            )
          end
        end
      end
    end

  end
end

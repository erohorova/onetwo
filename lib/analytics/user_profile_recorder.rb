# frozen_string_literal: true

module Analytics
  class UserProfileRecorder < UserMetricsRecorder

    AddEvents = {
      "expert_topics" => "user_expertise_topic_added",
      "learning_topics" => "user_interest_topic_added"
    }

    RemoveEvents = {
      "expert_topics" => "user_expertise_topic_removed",
      "learning_topics" => "user_interest_topic_removed"
    }

    WHITELISTED_TOPIC_KEYS = [
      :topic_id, :topic_name, :topic_label,
      :domain_id, :domain_name, :domain_label
    ]

    def self.get_topic_attributes(topic)
      topic.
        with_indifferent_access.
        slice(*WHITELISTED_TOPIC_KEYS).
        transform_values(&:to_s)
    end

  # changed_profile_keys is a hash which maps key (changed column) to [old_topics, new_topics]
  # old_topics and new_topics should each be arrays of topic objects,
  # e.g. representing expert_topics or learning_topics
    def self.record(org:, actor:, user_profile:, changed_profile_keys:, timestamp:, metadata: {})
      changed_profile_keys.each do |key, (old_topics, new_topics)|
        added_topics = (new_topics - old_topics).map &method(:get_topic_attributes)
        removed_topics = (old_topics - new_topics).map &method(:get_topic_attributes)
        added_topics.each do |added_topic|
          push_added_topic org, actor, user_profile, key, added_topic, timestamp, metadata
        end
        removed_topics.each do |removed_topic|
          push_removed_topic org, actor, user_profile, key, removed_topic, timestamp, metadata
        end
      end
    end

    def self.push_added_topic(org, actor, user_profile, key, added_topic, timestamp, metadata)
      user_profile = RecursiveOpenStruct.new(user_profile)
      added_topic = added_topic.with_indifferent_access
      Analytics::UserMetricsRecorder.record(
        org: org,
        event: AddEvents.fetch(key),
        timestamp: timestamp,
        actor: actor,
        additional_data: metadata
      ) do |data|
        data[:values].merge!(
          member_user_id: user_profile.user_id.to_s
        )
        data[:values].merge!(added_topic.symbolize_keys)
      end
    end

    def self.push_removed_topic(org, actor, user_profile, key, removed_topic, timestamp, metadata)
      user_profile = RecursiveOpenStruct.new user_profile
      removed_topic = removed_topic.with_indifferent_access
      Analytics::UserMetricsRecorder.record(
        org: org,
        event: RemoveEvents.fetch(key),
        timestamp: timestamp,
        actor: actor,
        additional_data: metadata
      ) do |data|
        data[:values].merge!(
          member_user_id: user_profile.user_id.to_s
        )
        data[:values].merge!(removed_topic.symbolize_keys)
      end
    end

  end
end
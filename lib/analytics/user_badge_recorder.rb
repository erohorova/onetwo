# frozen_string_literal: true

module Analytics
  class UserBadgeRecorder < UserMetricsRecorder
    USER_BADGE_EVENTS = ['user_badge_completed'].freeze
    USER_BADGE_EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self
      def record(org:, timestamp:, user_badge:, actor:, event:, additional_data: {})
        validate_user_event(event)
        user_badge = RecursiveOpenStruct.new(user_badge)
        super(
          org: org,
          timestamp: timestamp,
          actor: actor,
          event: event,
          additional_data: additional_data
        ) do |data|
          merge_extra_information(data, user_badge)
        end
      end

      def merge_extra_information(data, user_badge)
        data[:values].merge!(
          badge_id:  user_badge.id.to_s,
          badge_title: user_badge.badge_title,
          badge_type: user_badge.badge_type,
        )
      end

      private

      def validate_user_event(event)
        unless USER_BADGE_EVENTS.include?(event)
          raise "Invalid event: #{event}. Valid Events: #{USER_BADGE_EVENTS.join(",")}"
        end
      end
    end
  end
end

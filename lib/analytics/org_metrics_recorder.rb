# frozen_string_literal: true
module Analytics
  class OrgMetricsRecorder < Analytics::MetricsRecorder
    MEASUREMENT = 'organizations'.freeze
    ORG_METRICS_EVENTS = [
      'org_developer_api_credentials_created',
      'org_developer_api_credentials_deleted',
      "org_created",
      "org_announcement_created",
      "org_announcement_deleted",
      "org_custom_notification_sent",
      "org_taxonomy_created",
      "org_taxonomy_deleted",
      "org_embargo_added",
      "org_embargo_removed",
      "org_partner_center_created",
      "org_partner_center_deleted",
      "org_email_template_created",
      "org_email_template_deleted",
      'org_email_sender_created',
      'org_email_sender_deleted',
      "org_sources_credential_created",
      "org_sources_credential_deleted"
    ]

    ORG_METRICS_EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self

      def record(
        announcement: nil,taxonomy: nil, embargo: nil, partner_center: nil, sources_credential: nil,
        email_template: nil, mailer_config: nil,
        org:, event:, actor:, timestamp:, notification: nil, additional_data: {}
      )
        actor, org = [actor, org].map &RecursiveOpenStruct.method(:new)
        announcement &&= RecursiveOpenStruct.new(announcement)
        taxonomy &&= RecursiveOpenStruct.new(taxonomy)
        embargo &&= RecursiveOpenStruct.new(embargo)
        partner_center &&= RecursiveOpenStruct.new(partner_center)
        email_template &&= RecursiveOpenStruct.new(email_template)
        mailer_config &&= RecursiveOpenStruct.new(mailer_config)
        sources_credential &&= RecursiveOpenStruct.new(sources_credential)

        if event && !get_recorder_for_event(event)
          raise StandardError, 'Event is not registered'
        end

        data = {
          tags: {
            org_id: org.id.to_s,
            event: event,
            _user_id: actor.id&.to_s
          },
          values: {
            org_host_name: org.host_name,
            org_name: org.name,
            org_slug: org.slug,
            org_custom_domain: org.custom_domain,
            org_client_id: org.client_id.to_s,
            org_is_open: org.is_open ? '1' : '0',
            org_is_registrable: org.is_registrable ? '1' : '0',
            org_status: org.status,
            org_send_weekly_activity: org.send_weekly_activity ? '1' : '0',
            org_show_sub_brand_on_login: org.show_sub_brand_on_login ? '1' : '0',
            org_savannah_app_id: org.savannah_app_id.to_s,
            org_redirect_uri: org.redirect_uri,
            org_social_enabled: org.social_enabled ? '1' : '0',
            org_xapi_enabled: org.xapi_enabled ? '1' : '0',
            org_show_onboarding: org.show_onboarding ? '1' : '0',
            org_enable_role_based_authorization: org.enable_role_based_authorization ? '1' : '0',
            user_id: actor.id&.to_s
          },
          timestamp: timestamp
        }

        if announcement
          data[:values][:announcement_name]       = announcement.label
          data[:values][:announcement_start_date] = announcement.start_date
          data[:values][:announcement_end_date]   = announcement.end_date
          data[:values][:announcement_id]         = announcement.id.to_s
        end
        
        if notification.is_a?(Hash)
          notification_struct = RecursiveOpenStruct.new(notification)
          data[:values].merge!(
            notification_json_payload: notification_struct.json_payload,
            notification_message: notification_struct.message,
          )
        end

        if taxonomy
           data[:values][:taxonomy_id]            = taxonomy.id.to_s
           data[:values][:taxonomy_label]         = taxonomy.label
           data[:values][:taxonomy_requestor_id]  = taxonomy.requestor_id.to_s
           data[:values][:taxonomy_approver_id]   = taxonomy.approver_id.to_s
           data[:values][:taxonomy_publisher_id]  = taxonomy.publisher_id.to_s 
           data[:values][:taxonomy_state]         = taxonomy.state
        end

        if embargo
          data[:values][:embargo_id]              = embargo.id.to_s
          data[:values][:embargo_type]            = embargo.category
          data[:values][:embargo_content]         = embargo.value
        end        

        if sources_credential
          data[:values][:sources_credential_id]              = sources_credential.id.to_s
          data[:values][:sources_credential_source_id]       = sources_credential.source_id
          data[:values][:sources_credential_shared_secret]   = sources_credential.shared_secret
          data[:values][:sources_credential_auth_api_info]   = sources_credential.auth_api_info&.to_json
          data[:values][:sources_credential_source_name]     = sources_credential.source_name
        end

        if partner_center
          data[:values][:partner_center_id]          = partner_center.id.to_s
          data[:values][:partner_center_enabled]     = partner_center.enabled ? '1' : '0'
          data[:values][:partner_center_description] = partner_center.description
          data[:values][:partner_center_embed_link]  = partner_center.embed_link
          data[:values][:partner_center_integration] = partner_center.integration
        end
        
        if email_template
          data[:values].merge!(
            email_template_content:    email_template.content,
            email_template_creator_id: email_template.creator_id.to_s,
            email_template_state:      email_template.state,
            email_template_language:   email_template.language,
            email_template_title:      email_template.title,
            email_template_is_active:  email_template.is_active ? '1' : '0',
            email_template_default_id: email_template.default_id.to_s,
            email_template_design:     email_template.design,
            email_template_id:         email_template.id.to_s,
          )
        end

        # Used for email sender events
        if mailer_config
          data[:values][:email_sender_from_address] = mailer_config.email_sender_from_address
          data[:values][:email_sender_from_name]    = mailer_config.email_sender_from_name
          data[:values][:is_sender_verified]        = mailer_config.is_sender_verified
          data[:values][:mailer_config_id]          = mailer_config.mailer_config_id
          data[:values][:is_current_sender]         = mailer_config.is_current_sender
        end

        merge_additional_data(data, additional_data)
        yield(data) if block_given?

        influxdb_record(MEASUREMENT, data)
      end

      def merge_additional_data(data, additional_data)
        whitelisted_attrs = [:developer_api_key]
        whitelisted_attrs.each do |key|
          data[:values][key] = additional_data[key]&.to_s
        end
        super
      end

    end

  end
end

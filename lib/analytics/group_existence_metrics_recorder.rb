# frozen_string_literal: true
class Analytics::GroupExistenceMetricsRecorder < Analytics::GroupMetricsRecorder

  VALID_EVENTS = %w{
    group_created group_deleted
  }

  VALID_EVENTS.each do |event_name|
    register_event_and_recorder(event_name, self)
    const_set("EVENT_#{event_name.upcase}", event_name).freeze
  end

  def self.record(
    actor:, group:, org:,
    timestamp:, event:,
    additional_data: {}
  )

    if event && !get_recorder_for_event(event)
      raise StandardError, 'Event is not registered'
    end

    super(
      actor: actor,
      group: group,
      org: org,
      timestamp: timestamp,
      event: event,
      additional_data: additional_data
    )
  end

end

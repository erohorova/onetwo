# frozen_string_literal: true
module Analytics
  class CardViewMetricsRecorder < CardMetricsRecorder

    VALID_EVENTS = [
      "card_viewed",
      "card_source_visited"
    ].index_by(&:itself).transform_values { true }

    VALID_EVENTS.keys.each do |event_name|
      register_event_and_recorder(event_name, self)
    end

    def self.record(*args, card:, **opts)
      card_struct = RecursiveOpenStruct.new(card)
      write_response = super(*args, card: card, **opts) do
        raise(ArgumentError, "no card id found") unless card_struct.id
      end
      if write_response.try(:code) == "204"
        increment_card_view_count(card_struct)
      end
    end

    def self.increment_card_view_count(card)
      key = card_view_count_cache_key(card)
      view_count = Rails.cache.read(key)
      if !view_count
        set_initial_card_view_count(card, key)
      end
      Rails.cache.increment(key)
    end

    def self.card_view_count_cache_key(card)
      "card-#{card.id}/card_view_count"
    end

    # Returns the new count
    def self.set_initial_card_view_count(card, key)
      count = get_initial_card_view_count(card)
      Rails.cache.write(key, count, raw: true)
      count
    end

    def self.get_initial_card_view_count(card)
      query = InfluxQuery.new("cards").
        select!("count(user_id)").
        filter!(:card_id, "_card_id", "=", card.id.to_s).
        filter!(:event, "event", "=", "card_viewed").
        add_time_filters!(
          start_date: card.created_at.to_i,
          end_date: Time.now.utc.to_i
        )
      results = query.resolve
      results.any? ? results[0]["values"][0]["count"] : 0
    end

  end
end
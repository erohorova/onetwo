# frozen_string_literal: true
module Analytics
  class CardPinRecorder < CardMetricsRecorder
    CARD_PIN_EVENTS = ['card_pinned', 'card_unpinned']
    CARD_PIN_EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self
      def record(org:, pin_id:, timestamp:, event:, actor:, channel:, card:, additional_data:)
        # NOTE: only pin object of type Card and
        # pinnable object type of Channel are supported
        validate_card_event(event)
        channel = RecursiveOpenStruct.new(channel)
        super(
          org: org,
          card: card,
          event: event,
          actor: actor,
          timestamp: timestamp,
          additional_data: additional_data
        ) do |data|
          data[:values][:pin_id] = pin_id.to_s

          # store pinnnable information
          data[:values][:channel_id] = channel.id.to_s
          data[:values][:is_channel_featured] = channel.is_promoted && '1' || '0'
          data[:values][:is_channel_curated] = channel.curate_only && '1' || '0'
          data[:values][:is_channel_public] = !channel.is_private && '1' || '0'
          data[:values][:channel_name] = channel.label
        end
      end

      private

      def validate_card_event(event)
        raise 'Valid events: card_pin and card_unpin' unless CARD_PIN_EVENTS.include?(event)
      end
    end
  end
end

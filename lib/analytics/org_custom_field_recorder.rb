# frozen_string_literal: true
module Analytics
  class OrgCustomFieldRecorder < Analytics::OrgMetricsRecorder
    ORG_CUSTOM_FIELD_EVENTS = [
      'org_custom_field_added',
      'org_custom_field_removed',
      'org_custom_field_edited'
    ]

    ORG_CUSTOM_FIELD_EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    # We only care about updates to these attributes
    SIGNIFICANT_EDIT_ATTRIBUTES = Set.new(%w{
      display_name
      enable_people_search
      value
    })

    class << self

      def record(
        # Mandatory stuff
        custom_field:, org:, event:, actor:, timestamp:,
        # Optional - edit events only
        changed_column: nil, old_val: nil, new_val: nil, config: nil,
        # Extra metadata
        additional_data: {}
      )
        custom_field = RecursiveOpenStruct.new(custom_field)
        config = RecursiveOpenStruct.new(config)
        super(
          org: org,
          event: event,
          actor: actor,
          timestamp: timestamp,
          additional_data: additional_data
        ) do |data|
          data[:values].merge!(
            custom_field_display_name: custom_field.display_name,
            custom_field_id: custom_field.id
          )
          if config
            data[:values].merge! config_id: config.id.to_s
            data[:values].merge! config_name: config.name
          end
          if changed_column
            data[:values].merge!(changed_column: changed_column)
            data[:values].merge!(
              old_val: old_val.to_s,
              new_val: new_val.to_s
            )
          end
        end
      end

      def is_ignored_event?(changed_column)
        !SIGNIFICANT_EDIT_ATTRIBUTES.member?(changed_column.to_s)
      end

    end

  end
end

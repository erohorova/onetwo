# frozen_string_literal: true

module Analytics
  class CardCommentRecorder < CardMetricsRecorder
    CARD_COMMENT_EVENTS = ['card_comment_created', 'card_comment_deleted'].freeze
    CARD_COMMENT_EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self
      def record(org:, comment:, actor:, card:, timestamp:, event:, additional_data: {})
        comment = RecursiveOpenStruct.new(comment)
        validate_card_event(event)
        super(
          org: org,
          card: card,
          event: event,
          actor: actor,
          timestamp: timestamp,
          additional_data: additional_data
        ) do |data|
          data[:values][:comment_type] = comment.type  || 'StandardComment'
          data[:values][:comment_message] = comment.message
          data[:values][:comment_id] = comment.id.to_s
        end
      end

      private

      def validate_card_event(event)
        unless CARD_COMMENT_EVENTS.include?(event)
          raise 'Valid events: card_comment and card_uncomment'
        end
      end

    end
  end
end
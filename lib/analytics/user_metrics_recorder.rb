# frozen_string_literal: true
module Analytics
  class UserMetricsRecorder < Analytics::MetricsRecorder
    MEASUREMENT = 'users'.freeze
    VALID_EVENTS = {
      "user_created"                 => true,
      "user_followed"                => true, 
      "user_unfollowed"              => true,
      "user_logged_in"               => true,
      "user_expertise_topic_added"   => true,
      "user_expertise_topic_removed" => true,
      "user_interest_topic_added"    => true,
      "user_interest_topic_removed"  => true,
      "user_edited"                  => true,
      "user_role_added"              => true,
      "user_role_removed"            => true
    }.freeze

    VALID_EVENTS.keys.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self

      def record(
        # REQUIRED ARGS
        org:, timestamp:, actor:, event:,
        # OPTIONAL CHANGE-RELATED ARGS
        # NOTE: user is only used for user edited/created/role events.
        # actor is used for all other purposes.
        changed_column: nil, old_val: nil, new_val: nil, user: nil,
        # OPTIONAL ROLE-RELATED ARGS
        added_role: nil, removed_role: nil,
        # METADATA
        additional_data: {}
      )
        user, actor, org = [user, actor, org].map &RecursiveOpenStruct.method(:new)
        return if is_ignored_event?(changed_column)
        raise StandardError, 'Event cannot be empty' if event.nil?

        if event && !(VALID_EVENTS.has_key?(event) || get_recorder_for_event(event))
          raise StandardError, 'Event is not registered'
        end

        data = {}

        data[:values] = extract_fields(org, user, actor, event)
        data[:tags] = extract_tags(org, user, actor, event)
        data[:timestamp] = timestamp

        merge_role_data(data, added_role, removed_role)
        merge_additional_data(data, additional_data)
        merge_changes(data, changed_column, old_val, new_val)
        yield(data) if block_given?
        influxdb_record(MEASUREMENT, data)
      end

      def extract_fields(org, user, actor, event)
        field_set = {}
        field_set[:sign_in_ip] = actor.last_sign_in_ip
        field_set[:sign_in_at] = actor.last_sign_in_at.to_i.to_s
        field_set[:user_id] = actor.id.to_s
        field_set[:user_role] = actor.organization_role
        field_set[:user_org_uid] = actor.user_org_uid.to_s
        field_set[:org_hostname] = org.host_name
        field_set[:onboarding_status] = actor && (actor.onboarded? && '1' || '0')
        ###NOTE: PII
        field_set[:full_name] = actor.full_name
        field_set[:user_handle] = actor.handle
        ###
        if event == "user_edited"
          field_set[:edited_user_id] = user.id.to_s
        end

        if event == "user_created" && user
          field_set[:created_user_id] = user.id.to_s
          field_set[:upload_mode] = user.upload_mode
        end

        if event.in? %w{user_role_added user_role_removed}
          field_set[:member_user_id] = user.id.to_s
        end

        field_set
      end

      def merge_changes(data, changed_column, old_val, new_val)
        data[:values][:changed_column] = changed_column
        data[:values][:old_val] = old_val&.to_s
        data[:values][:new_val] = new_val&.to_s
      end

      def extract_tags(org, user, actor, event)
        tag_set = {}
        tag_set[:_user_id] = actor.id.to_s
        tag_set[:org_id] = org.id.to_s
        tag_set[:event] = event
        tag_set
      end

      def merge_role_data(data, added_role, removed_role)
        data[:values].merge!(added_role: added_role)
        data[:values].merge!(removed_role: removed_role)
      end

      def is_ignored_event?(changed_column)
        return false unless changed_column
        [
          whitelisted_change_attrs,
          whitelisted_profile_change_attrs
        ].all? do |attrs_list|
          attrs_list.exclude?(changed_column)
        end
      end

      def whitelisted_change_attrs
        %w{
          email
          first_name
          last_name
          handle
          bio
          is_complete
          organization_role
          parent_user_id
          locked_at
          is_active
          hide_from_leaderboard
          exclude_from_leaderboard
          default_team_id
        }
      end

      # These attributes are stored on the user profile, but still tracked
      # through the user_edited event.
      def whitelisted_profile_change_attrs
        %w{
          hr_job_role_id
          hr_organization_id
          hr_location_id
          hr_competency_id
          hr_domain_id
          time_zone
          tac_accepted
          language
          dob
          tac_accepted_at
          job_title
        }
      end

    end

  end
end

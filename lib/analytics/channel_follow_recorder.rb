# frozen_string_literal: true

require 'analytics/channel_metrics_recorder'
module Analytics
  class ChannelFollowRecorder < ChannelMetricsRecorder
    CHANNEL_FOLLOW_EVENTS = ['channel_followed', 'channel_unfollowed'].freeze
    CHANNEL_FOLLOW_EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self
      def record(timestamp:, actor:, follower:, followed:, event:, additional_data: {})
        follower = RecursiveOpenStruct.new(follower)
        validate_follow_event(event)
        super(
          channel: followed,
          event: event,
          actor: actor,
          additional_data: additional_data,
          timestamp: timestamp
        ) do |data|
          data[:values][:follower_id] = follower.id.to_s
        end
      end

      private

      def validate_follow_event(event)
        unless CHANNEL_FOLLOW_EVENTS.include?(event)
          raise "Valid events: #{ChannelMetricsRecorder::EVENT_CHANNEL_FOLLOWED} and #{ChannelMetricsRecorder::EVENT_CHANNEL_UNFOLLOWED}"
        end
      end
    end
  end
end

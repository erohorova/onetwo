# frozen_string_literal: true
module Analytics
  class CardModerationMetricsRecorder < CardMetricsRecorder
    CARD_MODERATION_EVENTS = [
      'card_approved_for_channel',
      'card_rejected_for_channel'
    ]

    CARD_MODERATION_EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    def self.record(org:, actor:, event:, channel:, card:, timestamp:, additional_data:)
      channel = RecursiveOpenStruct.new(channel)
      super(
        org: org,
        card: card,
        actor: actor,
        event: event,
        timestamp: timestamp,
        additional_data: additional_data
      ) do |data|
        data[:values].merge!(
          channel_id: channel.id.to_s
        )
      end
    end

  end
end
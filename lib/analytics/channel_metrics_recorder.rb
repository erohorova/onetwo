# frozen_string_literal: true
module Analytics

  class ChannelMetricsRecorder < Analytics::MetricsRecorder

    MEASUREMENT = 'channels'.freeze

    VALID_EVENTS = %w{
      channel_created
      channel_deleted
      channel_visited
      channel_followed  
      channel_unfollowed
      channel_edited
      channel_promoted
      channel_unpromoted
      channel_course_added
      channel_course_removed
    }.index_by(&:itself).transform_values { true }

    VALID_EVENTS.keys.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self

      def record(
        channel:, event:, actor:, timestamp:,
        course: nil,
        changed_column: nil, old_val: nil, new_val: nil,
        additional_data: {}
      )
        return if is_ignored_event?(changed_column)
        channel, actor = [channel, actor].map &RecursiveOpenStruct.method(:new)
        course &&= RecursiveOpenStruct.new course
        raise StandardError, 'Event cannot be empty' if event.nil?
        if event && !(VALID_EVENTS.has_key?(event) || get_recorder_for_event(event))
          raise StandardError, 'Event is not registered'
        end

        data = {
          values: extract_fields(channel, actor),
          tags: extract_tags(channel, event, actor),
          timestamp: timestamp
        }
        merge_additional_data(data, additional_data)
        merge_changes(data, changed_column, old_val, new_val)
        yield(data) if block_given?

        if course
          data[:values][:course_id]   = course.id.to_s
          data[:values][:course_name] = course.name.to_s
        end

        influxdb_record(self::MEASUREMENT, data)
      end

      def extract_fields(channel, actor)
        {
          channel_id: channel.id.to_s,
          user_id: actor&.id.to_s,
          channel_name: channel.label,
          #### NOTE: PII
          user_full_name: actor&.full_name,
          user_handle: actor&.handle,
          ####
          is_channel_featured: channel.is_promoted ? '1' : '0',
          is_channel_curated: channel.curate_only ? '1' : '0',
          is_channel_public: channel.is_private ? '0' : '1',
          is_ecl_enabled: channel.ecl_enabled ? '1' : '0',
        }
      end

      def extract_tags(channel, event, actor)
        {
          _channel_id: channel.id.to_s,
          _user_id: actor.id.to_s,
          org_id: channel.organization_id.to_s,
          event: event
        }
      end

      def merge_changes(data, changed_column, old_val, new_val)
        data[:values][:changed_column] = changed_column
        data[:values][:old_val] = old_val&.to_s
        data[:values][:new_val] = new_val&.to_s
      end

      def is_ignored_event?(changed_column)
        return false unless changed_column
        whitelisted_change_attrs.exclude?(changed_column)
      end

      def whitelisted_change_attrs
        %w{
          label
          description
          is_private
          curate_only
          ecl_enabled
          curate_ugc
          allow_follow
          is_open
          only_authors_can_post
        }
      end

    end

  end

end
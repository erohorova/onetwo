# frozen_string_literal: true

module Analytics

  class UserScoreContinuousQueries < ContinuousQueriesBase

    # the CQ takes user_scores, creates daily aggregations, and pushes here:
    MEASUREMENT = "user_scores_daily"

    # The length of time to aggregate data by.
    TIME_LENGTH = "1d"

    # the interval on which the CQ runs
    RESAMPLE_EVERY = "1d"

    # how much data it sees at once.
    RESAMPLE_FOR = "2d"

    # This method backfills the user_score_daily aggregation for previous data.
    # It should only be used for user_score records pushed BEFORE the
    # continuous query was launched.
    def self.build_backfill_query

      # This query is composed of many subqueries.
      evaluated_subqueries = evaluate_subqueries

      # The results are grouped by user and time. Influx requires all queries
      # using GROUP BY to only use aggregation methods in the SELECT clause.
      # The MEAN here are just used for that purpose. Mathematically speaking,
      # they are totally redundant and have no impact on the score, because
      # the same aggregations already happened with the same GROUP BY in the
      # subqueries.
      #
      # TL;DR - the MEAN() in the following block has no effect on the score
      # but is required because of Influx rules.
      result_keys = evaluated_subqueries.keys.map do |key|
        "MEAN(#{key}) AS #{key}"
      end.join(",")

      # Join all the subqueries into a single FROM clause
      from_clause = evaluated_subqueries.values.join(",")

      # build a final query
      final_query = <<-TXT.chomp.strip_heredoc
        #{select_into(result_keys, self::MEASUREMENT, from_clause)} \
        GROUP BY org_id,user_id,time(#{self::TIME_LENGTH})
      TXT

      # UNCOMMENT TO PRINT FINAL QUERY:
      # puts final_query.gsub(/\s+/) { |x| x.length > 0 ? "\n#{x}" : x }\

      final_query
    end

    # Returns a hash mapping name to query string.
    #
    # For example,
    #   { total_user_score: "SELECT FROM ...." }
    #
    # The key names correspond to methods
    # e.g. total_user_score calls the total_user_score_subquery method.
    #
    # They also correspond to keys in the aggregation records
    def self.evaluate_subqueries
      query_names
      .index_by(&:itself)
      .transform_values { |name| send(:"#{name}_subquery") }
    end

    def self.query_names
      %w{
        total_user_score
        smartbites_commented_count
        smartbites_completed_count
        smartbites_created_count
        smartbites_consumed_count
        smartbites_liked_count
        total_smartbite_score
        time_spent_minutes
      }
    end

    # A WHERE condition used by a number of subqueries.
    def self.role_filter
      "(role='actor' OR role='actor_and_owner')"
    end

    class << self

      private

      # The sum of all scores for a user
      def total_user_score_subquery
        <<-TXT.chomp.strip_heredoc
          ( \
            SELECT SUM(score_value) AS total_user_score \
            FROM user_scores \
            GROUP BY org_id,user_id,time(#{self::TIME_LENGTH})
          )
        TXT
      end

      # The number of times a user commented on a card
      def smartbites_commented_count_subquery
        event_filter = "event='card_comment_created'"
        <<-TXT.chomp.strip_heredoc
          ( \
            SELECT COUNT(score_value) AS smartbites_commented_count \
            FROM user_scores \
            #{where_clause([event_filter, role_filter])} \
            GROUP BY org_id,user_id,time(#{self::TIME_LENGTH})
          )
        TXT
      end

      # The number of times a user marked a card as complete
      def smartbites_completed_count_subquery
        event_filter = "event='card_marked_as_complete'"
        <<-TXT.chomp.strip_heredoc
          ( \
            SELECT COUNT(score_value) AS smartbites_completed_count \
            FROM user_scores \
            #{where_clause([event_filter, role_filter])} \
            GROUP BY org_id,user_id,time(#{self::TIME_LENGTH})
          )
        TXT
      end

      # The number of times a user viewed a card
      def smartbites_consumed_count_subquery
        event_filter = "event='card_viewed'"
        <<-TXT.chomp.strip_heredoc
          ( \
            SELECT COUNT(score_value) AS smartbites_consumed_count \
            FROM user_scores \
            #{where_clause([event_filter, role_filter])} \
            GROUP BY org_id,user_id,time(#{self::TIME_LENGTH})
          )
        TXT
      end

      # The number of times a user created a card.
      def smartbites_created_count_subquery
        event_filter = "event='card_created'"
        <<-TXT.chomp.strip_heredoc
          ( \
            SELECT COUNT(score_value) AS smartbites_created_count \
            FROM user_scores \
            #{where_clause([event_filter, role_filter])} \
            GROUP BY org_id,user_id,time(#{self::TIME_LENGTH})
          )
        TXT
      end

      # The number of times a user liked a card.
      def smartbites_liked_count_subquery
        event_filter = "event='card_liked'"
        <<-TXT.chomp.strip_heredoc
          ( \
            SELECT COUNT(score_value) AS smartbites_liked_count \
            FROM user_scores \
            #{where_clause([event_filter, role_filter])} \
            GROUP BY org_id,user_id,time(#{self::TIME_LENGTH})
          )
        TXT
      end

      # The sum of all scores for a user relating to card actions
      def total_smartbite_score_subquery
        card_events = Analytics::CardMetricsRecorder.card_events
        event_filter = [
          "(",
          card_events.map { |event| "event='#{event}'" }.join(" OR "),
          ")"
        ].join
        <<-TXT.chomp.strip_heredoc
          ( \
            SELECT SUM(score_value) AS total_smartbite_score \
            FROM user_scores \
            #{where_clause([event_filter, role_filter])} \
            GROUP BY org_id,user_id,time(#{self::TIME_LENGTH})
          )
        TXT
      end

      # The number of minutes a user spent on the platform.
      # This is calculated like so:
      # - inner query: gets the number of events per minute for the user
      # - outer query: returns the # of minutes where there was at least 1 event
      def time_spent_minutes_subquery
        <<-TXT.chomp.strip_heredoc
          ( \
            SELECT COUNT(time_spent_minute) AS time_spent_minutes \
            FROM ( \
              SELECT COUNT(score_value) AS time_spent_minute \
              FROM user_scores \
              #{where_clause([role_filter])} \
              GROUP BY org_id,user_id,time(1m)
            ) \
            WHERE time_spent_minute > 0 \
            GROUP BY org_id,user_id,time(#{self::TIME_LENGTH})
          )
        TXT
      end

    end

  end

end

# frozen_string_literal: true
module Analytics
  class GroupChannelsMetricsRecorder < Analytics::GroupMetricsRecorder

    VALID_EVENTS  = %w{
      group_channel_followed group_channel_unfollowed
    }.index_by(&:itself).transform_values { true }

    VALID_EVENTS.keys.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self
      def record(
          org:, actor:, group:, channel:,
          event:, timestamp:,
          additional_data: {}
        )

        channel, _ = [ channel ].map &RecursiveOpenStruct.method(:new)

        raise StandardError, 'Group or channel can not be blank' unless group || channel
        if event && !get_recorder_for_event(event)
          raise StandardError, 'Event is not registered'
        end

        super(
          org: org,
          actor: actor,
          group: group,
          event: event,
          timestamp: timestamp,
          additional_data: additional_data
        ) do |data|
            data[:values].merge! channel_fields(channel)
        end
      end

      def channel_fields(channel)
        { channel_id: channel.id.to_s, channel_name: channel.label}
      end
    end
  end
end

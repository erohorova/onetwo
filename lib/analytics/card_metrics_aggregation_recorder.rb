# frozen_string_literal: true

# =========================================
# THIS CLASS SAVED PER-CARD, PER-DAY AGGREGATIONS TO MYSQL
# IT SAVES TO MYSQL SO IT CAN EASILY BE QUERIED USING WHERE..IN
# =========================================

module Analytics
  class CardMetricsAggregationRecorder < Analytics::MetricsRecorder

    attr_reader :org_ids, :start_time, :end_time

    COLUMN_NAMES = {
      likes:       "num_likes",
      views:       "num_views",
      completions: "num_completions",
      bookmarks:   "num_bookmarks",
      comments:    "num_comments",
      assignments: "num_assignments",
    }

    def initialize(**opts)
      base_time = Time.now.utc
      defaults = {
        org_ids: Organization.pluck(:id),
        start_time: base_time.beginning_of_day - 1.day,
        end_time: base_time.end_of_day - 1.day
      }
      defaults.merge(opts).each do |key, val|
        instance_variable_set "@#{key}", val
      end
    end

    def run
      org_ids.map(&:to_s).each { |org_id| run_for_org(org_id) }
    end

    private

    def run_for_org(org_id)
      query = query_for_org(org_id)
      results = query.resolve
      # Influx returns the results grouped by org/card
      # We don't actually want this - since we queried for a single org only,
      # the org grouping is pointless, and since we want to bulk-create records
      # for cards, we need to un-group by card (put everything all in one list)
      results.each do |tag_set|
        tag_set["values"].each { |val| val.merge!(tag_set["tags"] || {}) }
      end
      records = results.map { |tag_set| tag_set["values"] }.flatten
      log.debug("processing #{records.length} records for org #{org_id} ")
      save_results(records)
    end

    def save_results(records)
      # see https://github.com/jamis/bulk_insert
      records.in_groups_of(10_000).each do |record_group|
        attr_sets = record_group.compact.reduce([]) do |memo, record|
           # in_groups_of can use nil elements as placeholders
          next memo unless record
          time = Time.parse(record["time"]).utc
          start_time = time.beginning_of_day.to_i
          end_time = time.end_of_day.to_i
          record_attrs = {
            organization_id: record["org_id"],
            card_id:         record["_card_id"],
            start_time:      start_time,
            end_time:        end_time
          }
          COLUMN_NAMES.values.each do |column_name|
            record_attrs[column_name.to_sym] = record[column_name] || 0
          end
          next memo unless COLUMN_NAMES.values.any? do |column_name|
            attr = record_attrs[column_name.to_sym]
            attr && attr > 0
          end
          memo + [record_attrs]
        end
        CardMetricsAggregation.bulk_insert do |worker|
          attr_sets.each { |record_attrs| worker.add(record_attrs) }
        end
        log.debug("creating #{attr_sets.length} CardMetricsAggregation records")
      end
    end

    def query_for_org(org_id)
      subqueries_str = subqueries_for_org(org_id)
      # apply a filter that requires any of the counts to be nonzero.
      # This check also happens once the records are fetched, but applying
      # it here prevents unnecessary records from being loaded into memory.
      min_values_filter = COLUMN_NAMES.values.map do |column_name|
        { key: column_name, operator: ">", value: 0 }
      end
      main_subquery = InfluxQuery
        .new(subqueries_str, has_subquery: true)
        .group_by!("_card_id,time(1d),org_id")
        .select!(select_columns)
      main_subquery_str = main_subquery.to_plain_query(as_subquery: true)
      query = InfluxQuery.new(main_subquery_str, has_subquery: true)
      query.add_conditions!(min_values_filter, operator: "OR", wrap_clause: true)
    end

    def select_columns
      COLUMN_NAMES.values.map do |col|
        "MEAN(#{col}) AS #{col}"
      end.join(",")
    end

    def subqueries_for_org(org_id)
      subqueries = COLUMN_NAMES.keys.map do |column_name|
        send(:"#{column_name}_count_subquery", org_id)
      end.map do |query|
        query.to_plain_query(as_subquery: true)
      end.join(",")
    end

    def comments_count_subquery(org_id)
      generic_cards_subquery(
        org_id: org_id,
        event_name: "card_comment_created",
        result_name: COLUMN_NAMES[:comments]
      )
    end

    def likes_count_subquery(org_id)
      generic_cards_subquery(
        org_id: org_id,
        event_name: "card_liked",
        result_name: COLUMN_NAMES[:likes]
      )
    end

    def views_count_subquery(org_id)
      generic_cards_subquery(
        org_id: org_id,
        event_name: "card_viewed",
        result_name: COLUMN_NAMES[:views]
      )
    end

    def completions_count_subquery(org_id)
      generic_cards_subquery(
        org_id: org_id,
        event_name: "card_marked_as_complete",
        result_name: COLUMN_NAMES[:completions]
      )
    end

    def bookmarks_count_subquery(org_id)
      generic_cards_subquery(
        org_id: org_id,
        event_name: "card_bookmarked",
        result_name: COLUMN_NAMES[:bookmarks]
      )
    end

    def assignments_count_subquery(org_id)
      generic_cards_subquery(
        org_id: org_id,
        event_name: "card_assigned",
        result_name: COLUMN_NAMES[:assignments]
      )
    end

    def generic_cards_subquery(org_id:, event_name:, result_name:)
      InfluxQuery
        .new("cards")
        .filter!(:org_id, "org_id", "=", org_id)
        .filter!(:event, "event", "=", event_name)
        .select!("COUNT(card_id) AS #{result_name}")
        .group_by!("_card_id,time(1d),org_id")
        .add_time_filters!(
          start_date: start_time.to_i,
          end_date: end_time.to_i
        )
    end

  end
end

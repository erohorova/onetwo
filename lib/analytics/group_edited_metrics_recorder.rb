# frozen_string_literal: true
class Analytics::GroupEditedMetricsRecorder < Analytics::GroupMetricsRecorder
  GROUP_EDITED_EVENTS = ['group_edited']

  GROUP_EDITED_EVENTS.each do |event_name|
    register_event_and_recorder(event_name, self)
    const_set("EVENT_#{event_name.upcase}", event_name).freeze
  end

  def self.record(
    changed_column:, old_val:, new_val:,
    actor:, group:, org:, timestamp:,
    event:, additional_data:
  )
    super(
      actor: actor,
      group: group,
      org: org,
      timestamp: timestamp,
      event: event,
      additional_data: additional_data
    ) do |data|
      data[:values].merge! changed_column: changed_column
      data[:values].merge! old_val: old_val&.to_s
      data[:values].merge! new_val: new_val&.to_s
    end
  end

  def self.is_ignored_event?(changed_column)
    return false unless changed_column
    whitelisted_change_attrs.exclude?(changed_column)
  end

  def self.whitelisted_change_attrs
    %w{
      name
      description
      auto_assign_content
      only_admin_can_post
      is_private
      is_mandatory
      image_file_name
      image_content_type
      domains
      value
    }
  end

end

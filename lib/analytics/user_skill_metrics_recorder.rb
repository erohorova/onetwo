# frozen_string_literal: true

module Analytics
  class UserSkillMetricsRecorder < UserMetricsRecorder
    USER_SKILL_EVENT = [
      'user_skill_created',
      'user_skill_edited',
      'user_skill_deleted'
    ]

    USER_SKILL_EVENT.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    def self.record(org:, skill:, user_skill:, timestamp:,
      changed_column: nil, old_val: nil, new_val: nil,
      actor:, event:, additional_data: {}
    )
      skill = RecursiveOpenStruct.new(skill)
      user_skill = RecursiveOpenStruct.new(user_skill)

      super(
        org: org,
        timestamp: timestamp,
        actor: actor,
        event: event,
        additional_data: additional_data
      ) do |data|
        data[:values].merge! user_skill_attributes(skill, user_skill)
        if event == EVENT_USER_SKILL_EDITED
          merge_changes(data, changed_column, old_val, new_val)
        end
      end
    end

    def self.user_skill_attributes(skill, user_skill)
      {
        skill_name:            skill.name,
        skill_id:              skill.id&.to_s,
        skill_description:     user_skill.description,
        skill_experience:      user_skill.experience,
        skill_level:           user_skill.skill_level,
        skill_credential_name: user_skill.credential_name,
        skill_credential_url:  user_skill.credential_url,
        skill_credential:      user_skill.credential.to_json #this is a serialized column in db
      }
    end

      def self.is_ignored_event?(changed_column)
        return false unless changed_column
        whitelisted_change_attrs.exclude?(changed_column)
      end

      def self.whitelisted_change_attrs
        %w{
          skill_id
          description
          credential
          experience
          skill_level
          credential_name
          credential_url
        }
      end

  end
end

# frozen_string_literal: true

module Analytics

  class OrgScoreContinuousQueries < ContinuousQueriesBase

    # user_scores_daily records are aggregated by org into this measurement.
    MEASUREMENT = "org_scores_daily"

    # The length of time to aggregate data by.
    TIME_LENGTH = "1d"

    # the interval on which the CQ runs
    RESAMPLE_EVERY = "1d"

    # how much data it sees at once.
    RESAMPLE_FOR = "2d"

    # This backfill reads the user_scores_daily records
    # and aggregates based on the org_id included there.
    def self.build_backfill_query
      user_scores_cq = Analytics::UserScoreContinuousQueries
      user_scores_measurement = user_scores_cq::MEASUREMENT
      result_keys = user_scores_cq.query_names.map do |key|
        "SUM(#{key}) AS #{key}"
      end.join(",")
      final_query = <<-TXT
        #{select_into(result_keys, self::MEASUREMENT, user_scores_measurement)} \
        GROUP BY org_id,time(#{self::TIME_LENGTH})
      TXT
      final_query.chomp.strip_heredoc.strip.gsub(/\s+/, ' ')
    end

  end

end

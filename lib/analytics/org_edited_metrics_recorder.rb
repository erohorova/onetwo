# frozen_string_literal: true
module Analytics
  class OrgEditedMetricsRecorder < Analytics::OrgMetricsRecorder
    ORG_CONFIG_EVENTS = %w{
      org_config_edited
      org_edited
      org_announcement_edited
      org_notification_settings_edited
      org_taxonomy_approved
      org_taxonomy_rejected
      org_taxonomy_published
      org_embargo_edited
      org_sources_credential_edited
    }
    ORG_CONFIG_EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self

      def record(
        config: nil, announcement: nil, taxonomy: nil,
        embargo: nil, partner_center: nil, sources_credential: nil,
        mailer_config: nil, email_template: nil, 
        changed_column:, old_val:, new_val:,
        org:, event:, actor:,
        timestamp:, additional_data: {}
      )
        config &&= RecursiveOpenStruct.new(config)
        super(
          org: org,
          event: event,
          actor: actor,
          announcement: announcement,
          taxonomy: taxonomy,
          partner_center: partner_center,
          embargo: embargo,
          email_template: email_template,
          mailer_config: mailer_config,
          sources_credential: sources_credential,
          timestamp: timestamp,
          additional_data: additional_data
        ) do |data|
          data[:values].merge! changed_column: changed_column
          data[:values].merge! old_val: old_val&.to_s
          data[:values].merge! new_val: new_val&.to_s
          if config
            data[:values].merge! config_id: config.id.to_s
            data[:values].merge! config_name: config.name
          end
        end
      end

      def is_ignored_event?(changed_column)

        # NOTE:
        # The following terms are used on the admin/settings page:
        # -----------------------------------------
        # image_file_name            = "banner image"
        # mobile_image_file_name     = "team image"
        # co_branding_logo_file_name = "sub brand image"
        # favicon_file_name          = "favicon"
        # splash_image_file_name     = DEPRECATED
        #
        # For "login splash image" we use Config table instead.

        @allowed_events ||= Set.new(%w{
          name
          description
          host_name
          is_open
          status
          send_weekly_activity
          xapi_enabled
          enable_role_based_authorization
          custom_domain
          value
          image_file_name
          mobile_image_file_name
          co_branding_logo_file_name
          favicon_file_name
          label
          start_date
          end_date
          is_active
          state
          requestor_id
          approver_id
          publisher_id
          source_id
          shared_secret
          auth_api_info
          source_name
        })

        ! @allowed_events.member?(changed_column.to_s)
      end

    end

  end
end

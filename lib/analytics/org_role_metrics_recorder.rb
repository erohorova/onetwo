# frozen_string_literal: true
module Analytics
  class OrgRoleMetricsRecorder < Analytics::OrgMetricsRecorder
    RoleEvents = %w{
      org_role_permission_enabled
      org_role_permission_disabled
      org_role_created
      org_role_deleted
    }
    RoleEvents.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self

      # Note, we don't need to override 'record' in this class,
      # it will go to OrgMetricsRecorder, and then come here when it's time
      # to merge additional data.

      # The additional data is added by the models to the metadata hash
      # via "event_opts" (called "extra_info" here).

      def merge_additional_data(data, extra_info)
        super
        whitelisted_attrs = %i{
          role_id
          role_name
          role_permission
        }
          
        data[:values].merge!(
          extra_info
          .transform_values(&:to_s)
          .slice(*whitelisted_attrs)
        )
      end
    end

  end
end

class InfluxQuery

  include InfluxQuery::EntityStrategies

  class InfluxQueryError < StandardError
  end

  def self.fetch(query, params)
    INFLUXDB_CLIENT.query query, params: params
  end

  # returns [<query string>, <params hash>]
  # These can be passed to INFLUXDB_CLIENT.query
  #
  # ABSOLUTELY NO USER GENERATED CONTENT SHOULD BE UNESCAPED IN
  # THE OUTPUT OF THIS METHOD.
  #
  # We use parameterized queries for this. %{param_name} in a query string
  # will have its values inserted when .fetch is called with
  # params: {param_name: "val"}
  def self.get_query_for_entity(entity:, organization_id:, filters: {})
    filters = filters.
      merge(organization_id: organization_id.to_s).
      symbolize_keys
    raise(InfluxQueryError, "missing start date") unless filters[:start_date]
    unless self::EntityStrategies::Entities.member?(entity)
      raise InfluxQuery::InfluxQueryError.new("unknown entity")
    end
    query_obj = new(entity).
      add_default_filters!(**filters).
      send(:"prepare_#{entity}_query!", **filters).
      group_by!(filters[:group_by]).
      limit!(filters[:limit]).
      offset!(filters[:offset]).
      order!("ASC")
    [query_obj.finalize_query, query_obj.params]
  end

  attr_reader :params, :conditions, :select_columns, :source, :has_subquery
  attr_accessor :query_method

  def initialize(source, has_subquery: false)
    @has_subquery = has_subquery

    # This is akin to entity, e.g. "cards",
    # but can actually be any string, e.g. subqueries,
    # or a regex.
    source = "/#{source.source}/" if source.is_a?(Regexp)
    @source = source.to_s

    check_args!(source, has_subquery: has_subquery)

    @conditions = []
    @params = {}
    @query_method = "select"
    @select_columns = ["*"]
  end

  def check_args!(source, has_subquery:)
     # If measurement starts with quotations raise an error.
    if source =~ /^["']/
      raise(ArgumentError, "don't include quotes in measurement name")
    end
    if has_subquery
      unless source =~ /^\(/
        raise(ArgumentError,
          "Given subquery doesn't start with parenthesis"
        )
      end
    else
      if source =~ /^\(/
        raise(ArgumentError,
          "pass has_subquery: true if using subquery as measurement"
        )
      end
    end
  end

  # Can be converted to a string, which returns a query that can be
  # pasted into Influx
  def to_plain_query(as_subquery: false)
    escaped_params = params.transform_values do |val|
      val.is_a?(String) ? "'#{val}'" : val
    end
    result = finalize_subquery % escaped_params
    as_subquery ? wrap(result) : result
  end

  # CHANGES QUERY TO DELETE INSTEAD OF SELECT
  # doesn't accept limit/offset/order/group by
  def delete_all!
    if %i{limit offset group_by}.any? &params.method(:key?)
      raise ArgumentError, "can't use limit/offset/group_by in delete queries"
    end
    @query_method = "delete"
    @select_columns = [""]
    @params.delete :order
    self
  end

  # Returns a new instance with the same config
  def clone
    self.class.new(@source).tap do |instance|
      instance_variables.each do |attr|
        attr_val = instance_variable_get(attr).clone
        instance.instance_variable_set("#{attr}", attr_val)
      end
    end
  end

  # evaluates the query
  def resolve
    self.class.fetch(finalize_query, params)
  end

  # Some queries are too big to resolve all at once.
  # In these cases, this method can be used to fire multiple requests
  # and combine them before returning.
  # Requires that start_date already be defined on the query.
  # end_date defaults to the end of the current day.
  # 'interval' arg is a number of seconds to split time range into.
  # 'limit' is max num records to process at once
  # blk is optional and will be passed each group of records as they are available.
  # Note that using limit makes the query slower, which is why it defaults to nil
  def resolve_with_loop(interval:, limit: nil, &blk)
    unless params[:start_date].is_a?(Integer)
      raise ArgumentError, "must set start_date as timestamp to use looping query"
    end
    params[:end_date] ||= Time.now.utc.end_of_day.to_i
    orig_query = self
    interval_start = params[:start_date]
    interval_end = interval_start + interval
    results = []
    idx = 0
    loop do
      offset = 0
      interval_end = [params[:end_date], interval_end].min
      break if interval_start >= interval_end
      loop do
        idx += 1
        start_str, end_str = [interval_start, interval_end].map { |time| Time.at(time).to_date }
        puts "measurement - #{@source}"
        puts "limit=#{limit} offset=#{offset} start=#{start_str} end=#{end_str}"
        interval_query = orig_query.
          clone.
          add_time_filters!(start_date: interval_start, end_date: interval_end).
          limit!(limit).
          offset!(offset).
          order!("ASC")
        interval_result = interval_query.resolve
        if interval_result.any?
          results.concat interval_result[0]["values"]
        end
        puts "results: #{interval_result[0]&.fetch("values")&.length || 0}"
        puts "total: #{results.length}"
        blk&.call(interval_result, idx)
        is_over_limit = limit && interval_result.any? do |measurement_result|
          measurement_result["values"].length >= limit
        end
        if is_over_limit
          offset += limit
        else
          break
        end
      end
      interval_start = interval_end
      interval_end += interval
    end
    results
  end

  # Total overwrites the select_columns ivar
  def select!(*cols)
    @select_columns = cols
    self
  end

  def finalize_query
    # Just add a semicolon
    "#{finalize_subquery} ;"
  end

  # Note, this intentionally doesn't add () around the result.
  # This is so this method can be re-used by finalize_query
  def finalize_subquery
    source = has_subquery ? @source : "\"#{@source}\""
    (
      "#{query_method} #{@select_columns.join(",")} " +
      "from #{source}" +
      "#{finalize_conditions}"
    ).tap do |str|
      # Need to directly interpolate order and group.
      # parameterization isn't supported by InfluxDB::Client here.
      str << " GROUP BY #{params[:group_by]}" if @params[:group_by]
      str << " ORDER BY #{params[:order]}" if @params[:order]
      str << " LIMIT %{limit}" if @params[:limit]
      str << " OFFSET %{offset}" if @params[:offset]
    end
  end

  def finalize_conditions
    return "" unless @conditions.any?
    " where #{@conditions.join(" AND ")}"
  end

  def order!(order)
    (params[:order] = order) if order
    self
  end

  def limit!(limit)
    return self unless limit
    limit = limit.to_i
    raise(InfluxQueryError, "Cannot add a limit of 0") if limit.zero?
    @params.merge! limit: limit
    self
  end

  def offset!(offset)
    return self unless offset
    offset = offset.to_i
    @params.merge! offset: offset
    self
  end

  def group_by!(group_by)
    return self unless group_by
    @params.merge! group_by: group_by
    self
  end

  def add_conditions!(properties, operator: "AND", wrap_clause: false)
    clause = build_query_constraints(
      properties: properties,
      operator: operator
    )
    clause = wrap(clause) if wrap_clause
    @conditions.push clause
    self
  end

  def add_where_in_filter!(param_key_base, col_name, vals)
    return self unless vals.any?
    properties = vals.map.with_index do |val, idx|
      param_key = :"#{param_key_base}_#{idx}"
      @params.merge! param_key => val
      {
        key: col_name,
        operator: "=",
        value: "%{#{param_key}}"
      }
    end
    add_conditions!(properties, operator: "OR", wrap_clause: true)
  end

  def add_default_filters!(
    organization_id: nil,
    start_date: nil,
    end_date: nil,
    event: nil,
    **_opts
  )
    filter! :organization_id, "org_id", "=", organization_id
    filter! :event, "event", "=", event
    add_time_filters!(start_date: start_date, end_date: end_date)
    add_version_filter!
  end

  # Most of the filters have the same structure.
  def filter!(param_name, col_name, operator, val)
    return self unless val
    @params.merge!(param_name => val)
    add_conditions! [{
      key: col_name,
      operator: operator,
      value: "%{#{param_name}}"
    }]
  end

  def add_version_filter!
    skipped_entities = %w{
      user_scores_daily
      org_scores_daily
    }
    return self if @source.in? skipped_entities
    version = Settings.influxdb.event_tracking_version
    filter!(:version, 'analytics_version', '=', version)
  end

  def add_time_filters!(start_date: nil, end_date: nil, **_opts)
    if %i{start_time end_time}.any? &_opts.method(:key)
      raise ArgumentError, "use start_date and end_date"
    end
    # Cannot use filter! here because it requires unique interpolation rules.
    # Using the default param injection like we do in filter!, 12345s could not
    # be included in the output. If it were a string, parameterization would
    # wrap quotes around it (not valid).
    {
      start_date: { value: start_date, operator: ">=" },
      end_date: { value: end_date, operator: "<=" }
    }.each do |key, val|
      next unless val[:value]
      @params.merge! key => val[:value]
      add_conditions! [{
        key: 'time', operator: val[:operator], value: "%{#{key}}s"
      }]
    end
    self
  end

  def wrap(clause)
    "(#{clause})"
  end

  def build_query_constraints(properties: [], operator: 'AND')
    conditions = []
    properties.each do |property|
      value = property[:value]
      conditions << "#{property[:key]} #{property[:operator]} #{value}"
    end
    conditions.join(" #{operator} ")
  end

end

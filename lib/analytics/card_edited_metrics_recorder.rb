# frozen_string_literal: true
module Analytics
  class CardEditedMetricsRecorder < CardMetricsRecorder
    VALID_EVENTS = %w{
      card_edited card_edited_from_career_advisor
    }.index_by(&:itself).transform_values { true }.freeze

    VALID_EVENTS.keys.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    def self.record(
      org:, card:, changed_column:, old_val:, new_val:, timestamp:, event:,
      actor:, additional_data:,
      # Optional 
      career_advisor_card: nil
    )
      return if is_ignored_event?(changed_column)
      super(
        org: org,
        card: card,
        timestamp: timestamp,
        event: event,
        actor: actor,
        career_advisor_card: career_advisor_card,
        additional_data: additional_data
      ) do |data|
        data[:values][:changed_column] = changed_column
        data[:values][:old_val] = old_val&.to_s
        data[:values][:new_val] = new_val&.to_s
      end
    end

    def self.is_ignored_event?(changed_column)
      return false unless changed_column
      whitelisted_change_attrs.exclude?(changed_column)
    end

    def self.whitelisted_change_attrs
      %w{
        author_id
        hidden
        published_at
        resource_id
        title
        state
        message
        is_official
        card_type
        card_subtype
        ecl_id
        provider
        duration
        is_paid
        filestack
        readable_card_type
        is_public
        language
        level
      }
    end

  end
end

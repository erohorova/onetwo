class Analytics::UserDataDeletion

  class InfluxStrategy

    # Deletes a user's data from influx
    def self.run!(user)
      queries(user).each_with_object({}) do |(name, query), memo|
        # NOTE - cannot use resolve_with_loop here
        # it uses limit/offset which are not accepted for DELETE queries.
        memo[name] = query.resolve
      end
    end

    # A hash mapping query name (e.g. :cards) to InfluxQuery object
    # The queries are borrowed from UserDataExport::InfluxStrategy,
    # and modified to delete instead of select.
    def self.queries(user)
      data_export_strategy = Analytics::UserDataExport::InfluxStrategy
      select_queries = data_export_strategy.queries(user)
      select_queries.transform_values(&:delete_all!)
    end

  end

end
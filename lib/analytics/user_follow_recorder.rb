# frozen_string_literal: true

module Analytics
  class UserFollowRecorder < UserMetricsRecorder
    USER_FOLLOW_EVENTS = ['user_followed', 'user_unfollowed'].freeze
    USER_FOLLOW_EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self
      def record(org:, timestamp:, followed:, follower:, actor:, event:, additional_data: {})
        validate_user_event(event)
        followed, follower = [
          followed, follower
        ].map &RecursiveOpenStruct.method(:new)
        super(
          org: org,
          timestamp: timestamp,
          actor: actor,
          event: event,
          additional_data: additional_data
        ) do |data|
          merge_extra_information(data, followed, follower)
        end
      end

      def merge_extra_information(data, followed, follower)
        data[:values].merge!(
          followed_user_id: followed.id.to_s,
          follower_id: follower.id.to_s,
          ###NOTE: PII
          followed_user_full_name: followed.full_name,
          followed_user_handle: followed.handle
          ###
        )
      end

      private

      def validate_user_event(event)
        unless USER_FOLLOW_EVENTS.include?(event)
          raise "Valid events: #{UserMetricsRecorder::EVENT_USER_FOLLOWED} and #{UserMetricsRecorder::EVENT_USER_UNFOLLOWED}"
        end
      end
    end
  end
end

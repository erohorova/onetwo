# frozen_string_literal: true

module Analytics
  class CardBookmarkRecorder < CardMetricsRecorder
    CARD_BOOKMARK_EVENTS = ['card_bookmarked', 'card_unbookmarked'].freeze
    CARD_BOOKMARK_EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self
      def record(bookmark_id:, org:, card:, actor:, timestamp:, event:, additional_data: {})
        validate_card_event(event)

        super(
          org: org,
          card: card,
          event: event,
          actor: actor,
          additional_data: additional_data,
          timestamp: timestamp
        ) do |data|
          data[:values][:card_bookmark_id] = bookmark_id.to_s
        end
      end

      private

      def validate_card_event(event)
        raise 'Valid events: card_bookmark and card_unbookmark' unless CARD_BOOKMARK_EVENTS.include?(event)
      end
    end
  end
end

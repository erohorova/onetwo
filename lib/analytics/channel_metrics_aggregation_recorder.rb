# frozen_string_literal: true

=begin

  REQUIREMENTS

        (√) 1. num monthly active users by platform
        (√) 2. num channel visits
        (√) 3. num channel follows
        (√) 4. num cards added to channel
        (√) 5. num cards removed from channel
        (√) 6. num cards liked in channel
        (√) 7. num cards commented in channel
        (√) 8. num cards completed in channel
        (√) 9. backfill ability for 1-8
        (√) 10. views,assigned,completed, likes count for each card in channel
        (√) 11. leaderboard & top contributors

  TESTING

        require 'byebug'

        klass = Analytics::ChannelMetricsAggregationRecorder
        klass::TIME_RANGE = 1.days

        users_channels_map = klass.send :channel_followers_by_org; "ok"

        res = klass.get_num_monthly_active_users(
          users_channels_map: users_channels_map
        ); "ok"

        cards_channels_map = klass.send :cards_by_channel_and_org; "ok"

        res = klass.get_num_cards_completed_in_channel(
          cards_channels_map: cards_channels_map
        ); "ok"

        res = klass.get_num_channel_visits; "ok"
        res = klass.get_num_cards_added_to_channel; "ok"
        res = klass.get_num_cards_removed_from_channel; "ok"
        res = klass.get_num_cards_liked_in_channel(
          cards_channels_map: cards_channels_map
        ); "ok"
        res = klass.get_num_cards_commented_in_channel(
          cards_channels_map: cards_channels_map
        ); "ok"

        data = klass.fetch_data; "ok"
        # to debug timestamps
        puts data.map { |k,v| [k, v.values.flat_map(&:values).flat_map(&:keys).uniq.sort].flatten.join(" - ") + "\n\n" }
=end

module Analytics
  class ChannelMetricsAggregationRecorder < Analytics::MetricsRecorder

    # MAKES Object#yield_self AVAILABLE IN THIS CLASS
    using ObjectRefinements::YieldSelf

    # THE NUMBER OF DAYS TO COVER IN QUERIES
    # INCREASE THIS NUMBER IF BACKFILLING
    TIME_RANGE = 1.days

    # THE MEASUREMENT STUFF WILL GET PUSHED TO
    MEASUREMENT = "channel_aggregation_daily"

    class << self

      def run(orgs: Organization.all)
        # Some commonly-used times are stored in methods and memoized
        # for consistency. This is cleared whenever the recorder is run.
        clear_memoization!
        # We need to run each method only for a single org at a time
        # This is to prevent the memory usage from getting huge
        iterator = orgs.is_a?(Array) ? :each : :find_each
        orgs.send iterator do |org|
          # Fetch data for each org, channel, and day in the time range,
          # combining them into a big dictionary.
          merged_data = merge_data(fetch_data([org]))
          # flatten out the dictionary and format for influx
          insertion_records = build_insertion_records(merged_data)
          # bulk insert all the records to influx
          push_records(insertion_records)
        end
      end

      # bulk records records to influx
      def push_records(insertion_records)
        Analytics::MetricsRecorder.influxdb_bulk_record(
          self::MEASUREMENT,
          insertion_records
        )
      end

      # Merges all data per org/channel/timestamp
      # input: result of fetch_data
      # output: { <org_id> => { <channel_id> => { <time> => <record> } } }
      def merge_data(data)
        data.values.each_with_object({}) do |org_sections, memo|
          org_sections.each do |(org_id, org_data)|
            org_data.each do |channel_id, channel_data|
              channel_data.each do |timestamp, time_data|
                buried_merge!(memo, org_id, channel_id, timestamp, time_data)
              end
            end
          end
        end
      end

      # input: result of merge_data
      # output: [<record>...]
      # flattens the input hash to a list of records, formatted for influx
      def build_insertion_records(merged_data)
        # we keep track of the unique keys so we can set defaults later
        unique_keys = Set.new
        final = merged_data.each_with_object([]) do |(org_id, org_data), results|
          org_data.each do |channel_id, channel_data|
            channel_data.each do |timestamp, record|
              record.each_key &unique_keys.method(:add)
              results.push influx_record(org_id, channel_id, timestamp, record)
            end
          end
        end
        # Make sure every key is set for every record (defaulting to zero)
        set_default_values_for_records(unique_keys, final)
        final
      end

      # loads all the data, keyed by type, org, channel, and time
      # returns: { <type> => { <org_id> => { <channel_id> => { <time> => <record> } } } }
      def fetch_data(orgs)
        # Fetch preliminary data, used to map associations between records
        # when they must be joined across measurements
        opts = {
          users_channels_map: channel_followers_by_org(orgs),
          cards_channels_map: cards_by_channel_and_org(orgs),
          orgs: orgs
        }
        # Fetch all the data needed for all orgs, channels, and times within range.
        %i{
          num_monthly_active_users
          num_channel_visits
          num_channel_follows
          num_cards_added_to_channel
          num_cards_removed_from_channel
          num_cards_liked_in_channel
          num_cards_commented_in_channel
          num_cards_completed_in_channel
        }.each_with_object({}) do |key, memo|
          memo[key] = send(:"get_#{key}", **opts)
        end
      end

      # This is very similar to the monthly_active_users count in
      # ContentEngagementInfluxQuery .. but here we need to group by platform
      #
      # returns:
      #   { <org_id> => { <channel_id> => { <time> => { <platform> => <record> } } } }
      def get_num_monthly_active_users(users_channels_map:, orgs:, **opts)
        content_engagement = Analytics::ContentEngagementInfluxQuery
        events = content_engagement::ActiveUserEvents.sort_by(&:first)
        # e.g. if num_days_in_range = 3, will run for 0,1,2 days_in_past
        # this is somewhat confusing since "0 days in the past" is actually
        # data for yesterday (we use yesterday as a starting point).
        (0...num_days_in_range).each_with_object({}) do |days_in_past, memo|
          # We only increment the total count for each user once.
          # Also, we only increment the per-platform count for each user once.
          # e.g., a single user can increment two per-platform counts,
          # but does not increment the total count more than once.
          seen_user_channel_combos = Set.new
          seen_user_channel_platform_combos = Set.new
          events.each do |measurement, event_names|
            # start_date is the bound for the earliest data
            start_date = (end_of_yesterday - 30.days - days_in_past.days).to_i
            # end_date is the bound for the most recent data
            end_date = (end_of_yesterday - days_in_past.days).to_i
            # timestamp is the start of the date contained by the query,
            # e.g. if a timestamp is the start of june 15,
            # the query concerns data from the start to the end of that day
            timestamp = (start_of_yesterday - days_in_past.days).to_i
            query = content_engagement.query_measurement_for_active_users(
              measurement: measurement,
              start_date: start_date,
              end_date: end_date,
              event_names: event_names,
              org_ids: orgs.map(&:id).map(&:to_s)
            ).select!("user_id,platform").group_by!("org_id")
            debug_log(
              "querying #{measurement} for monthly active users (starting #{days_in_past} day(s) ago)",
              query.to_plain_query
            )
            results = query.resolve
            org_groups = results
            .group_by { |record| record["tags"]["org_id"] }
            org_groups.each do |org_id, record_groups|
              memo[org_id] ||= {}
              record_groups.each do |record_group|
                record_group["values"].each do |record|
                  user_id, platform = record.values_at *%w{ user_id platform }
                  channel_ids = users_channels_map[org_id.to_i].select do |channel_id, user_ids|
                    user_ids.member? user_id.to_i
                  end.keys.map(&:to_s)
                  channel_ids.each do |channel_id|
                    memo[org_id][channel_id] ||= {}
                    memo[org_id][channel_id][timestamp] ||= {}
                    unless seen_user_channel_combos.member?([user_id, channel_id])
                      memo[org_id][channel_id][timestamp]["num_monthly_active_users_total"] ||= 0
                      memo[org_id][channel_id][timestamp]["num_monthly_active_users_total"] += 1
                      seen_user_channel_combos.add([user_id, channel_id])
                    end
                    unless seen_user_channel_platform_combos.member?(
                      [user_id, channel_id, platform]
                    )
                      platform_key = build_platform_key(
                        "num_monthly_active_users_in_",
                        platform
                      )
                      memo[org_id][channel_id][timestamp][platform_key] ||= 0
                      memo[org_id][channel_id][timestamp][platform_key] += 1
                      seen_user_channel_platform_combos.add([user_id, channel_id, platform])
                    end
                  end # channel_ids.each
                end # record_group["values"].each
              end # records.each
            end # org_groups.each
          end # events.each
        end # (1..num_days_in_range).each_with_object
      end

      # returns:
      #   { <org_id> => { <channel_id> => { <time> => { <record> } } }
      def get_num_channel_visits(orgs:, **opts)
        query = InfluxQuery.new("channels")
          .select!("COUNT(channel_id)")
          .filter!(:event, "event", "=", "channel_visited")
          .add_where_in_filter!(:org_id, "org_id", orgs.map(&:id).map(&:to_s))
          .group_by!("org_id,_channel_id,time(1d)")
          .add_time_filters!(
            # start_date is the bound for the earliest data.
            # it refers to the start of a day
            start_date: (end_of_yesterday - self::TIME_RANGE + 1.days).beginning_of_day.to_i,
            # end_date is the bound for the most recent data
            # it refers to the end of that day
            end_date: end_of_yesterday.to_i
          )
        debug_log(
         "querying for num channel visits",
          query.to_plain_query
        )
        query_results = query.resolve
        final_results = {}
        query_results.each do |query_result|
          org_id, channel_id = query_result["tags"].values_at "org_id", "_channel_id"
          final_results[org_id] ||= {}
          final_results[org_id][channel_id] ||= {}
          query_result["values"].each do |val|
            time, count = val.values_at "time", "count"
            timestamp = Time.parse(time).to_i
            final_results[org_id][channel_id][timestamp] = {
              "num_channel_visits" => count
            }
          end
        end
        final_results
      end

      # returns:
      #   { <org_id> => { <channel_id> => { <time> => { <record> } } }
      def get_num_channel_follows(orgs:, **opts)
        query = InfluxQuery.new("channels")
          .select!("COUNT(channel_id)")
          .filter!(:event, "event", "=", "channel_followed")
          .add_where_in_filter!(:org_id, "org_id", orgs.map(&:id).map(&:to_s))
          .group_by!("org_id,_channel_id,time(1d)")
          .add_time_filters!(
            # start_date is the bound for the earliest data.
            # it refers to the start of a day
            start_date: (end_of_yesterday - self::TIME_RANGE + 1.days).beginning_of_day.to_i,
            # end_date is the bound for the most recent data
            # it refers to the end of that day
            end_date: end_of_yesterday.to_i
          )
        debug_log(
          "querying for num channel follows",
          query.to_plain_query
        )
        query_results = query.resolve
        final_results = {}
        query_results.each do |query_result|
          org_id, channel_id = query_result["tags"].values_at "org_id", "_channel_id"
          final_results[org_id] ||= {}
          final_results[org_id][channel_id] ||= {}
          query_result["values"].each do |val|
            time, count = val.values_at "time", "count"
            timestamp = Time.parse(time).to_i
            final_results[org_id][channel_id][timestamp] = {
              "num_channel_follows" => count
            }
          end
        end
        final_results
      end

      # returns:
      #   { <org_id> => { <channel_id> => { <time> => { <record> } } }
      def get_num_cards_added_to_channel(orgs:, **opts)
        query = InfluxQuery.new("cards")
          .select!("card_id,channel_id,org_id")
          .filter!(:event, "event", "=", "card_added_to_channel")
          .add_where_in_filter!(:org_id, "org_id", orgs.map(&:id).map(&:to_s))
          .add_time_filters!(
            # start_date is the bound for the earliest data.
            # it refers to the start of a day
            start_date: (end_of_yesterday - self::TIME_RANGE + 1.days).beginning_of_day.to_i,
            # end_date is the bound for the most recent data
            # it refers to the end of that day
            end_date: end_of_yesterday.to_i
          )
        debug_log(
          "querying for num cards added to channel",
          query.to_plain_query
        )
        query_results = (query.resolve.first || {}).fetch("values", [])
        final_results = {}
        query_results.each do |val|
          org_id, card_id, channel_id, time = val.values_at *%w{
            org_id card_id channel_id time
          }
          timestamp = (Time.parse(time).utc.beginning_of_day).to_i
          final_results[org_id] ||= {}
          final_results[org_id][channel_id] ||= {}
          final_results[org_id][channel_id][timestamp] ||= {
            "num_cards_added_to_channel" => 0
          }
          final_results[org_id][channel_id][timestamp]["num_cards_added_to_channel"] += 1
        end
        final_results
      end

      # returns:
      #   { <org_id> => { <channel_id> => { <time> => { <record> } } }
      def get_num_cards_removed_from_channel(orgs:, **opts)
        query = InfluxQuery.new("cards")
          .select!("card_id,channel_id,org_id")
          .filter!(:event, "event", "=", "card_removed_from_channel")
          .add_where_in_filter!(:org_id, "org_id", orgs.map(&:id).map(&:to_s))
          .add_time_filters!(
            # start_date is the bound for the earliest data.
            # it refers to the start of a day
            start_date: (end_of_yesterday - self::TIME_RANGE + 1.days).beginning_of_day.to_i,
            # end_date is the bound for the most recent data
            # it refers to the end of that day
            end_date: end_of_yesterday.to_i
          )
        debug_log(
          "querying for num cards removed from channel",
          query.to_plain_query
        )
        query_results = (query.resolve.first || {}).fetch("values", [])
        final_results = {}
        query_results.each do |val|
          org_id, card_id, channel_id, time = val.values_at *%w{
            org_id card_id channel_id time
          }
          timestamp = (Time.parse(time).utc.beginning_of_day).to_i
          final_results[org_id] ||= {}
          final_results[org_id][channel_id] ||= {}
          final_results[org_id][channel_id][timestamp] ||= {
            "num_cards_removed_from_channel" => 0
          }
          final_results[org_id][channel_id][timestamp]["num_cards_removed_from_channel"] += 1
        end
        final_results
      end

      # We get per-card like counts for ALL CARDS over the past day,
      # then map this according to channel membership.
      # returns:
      #   { <org_id> => { <channel_id> => { <time> => { <record> } } }
      def get_num_cards_liked_in_channel(cards_channels_map:, orgs:, **opts)
        query = InfluxQuery.new("cards")
          .select!("COUNT(card_id)")
          .filter!(:event, "event", "=", "card_liked")
          .add_where_in_filter!(:org_id, "org_id", orgs.map(&:id).map(&:to_s))
          .add_time_filters!(
            # start_date is the bound for the earliest data.
            # it refers to the start of a day
            start_date: (end_of_yesterday - self::TIME_RANGE + 1.days).beginning_of_day.to_i,
            # end_date is the bound for the most recent data
            # it refers to the end of that day
            end_date: end_of_yesterday.to_i
          )
          .group_by!("org_id,_card_id,time(1d)")
        debug_log(
          "querying for num cards liked in channel",
          query.to_plain_query
        )
        query_results = query.resolve
        final_results = {}
        query_results.each do |query_result|
          org_id, card_id = query_result["tags"].values_at "org_id", "_card_id"
          final_results[org_id] ||= {}
          next unless cards_channels_map[org_id.to_i]
          channel_ids = cards_channels_map[org_id.to_i].select do |channel_id, card_ids|
            card_ids.member? card_id.to_i
          end.keys.map(&:to_s)
          channel_ids.each do |channel_id|
            final_results[org_id][channel_id] ||= {}
            query_result["values"].each do |val|
              timestamp = Time.parse(val["time"]).to_i
              final_results[org_id][channel_id][timestamp] ||= {
                "num_cards_liked_in_channel" => 0
              }
              final_results[org_id][channel_id][timestamp]["num_cards_liked_in_channel"] += val["count"]
            end
          end
        end
        remove_empties(final_results)
      end

      # We get per-card comment counts for ALL CARDS over the past day,
      # then map this according to channel membership.
      # returns:
      #   { <org_id> => { <channel_id> => { <time> => { <record> } } }
      def get_num_cards_commented_in_channel(cards_channels_map:, orgs:, **opts)
        query = InfluxQuery.new("cards")
          .select!("COUNT(card_id)")
          .filter!(:event, "event", "=", "card_comment_created")
          .add_where_in_filter!(:org_id, "org_id", orgs.map(&:id).map(&:to_s))
          .add_time_filters!(
            # start_date is the bound for the earliest data.
            # it refers to the start of a day
            start_date: (end_of_yesterday - self::TIME_RANGE + 1.days).beginning_of_day.to_i,
            # end_date is the bound for the most recent data
            # it refers to the end of that day
            end_date: end_of_yesterday.to_i
          )
          .group_by!("org_id,_card_id,time(1d)")
        debug_log(
          "querying for num cards commented in channel",
          query.to_plain_query
        )
        query_results = query.resolve
        final_results = {}
        query_results.each do |query_result|
          org_id, card_id = query_result["tags"].values_at "org_id", "_card_id"
          final_results[org_id] ||= {}
          next unless cards_channels_map[org_id.to_i]
          channel_ids = cards_channels_map[org_id.to_i].select do |channel_id, card_ids|
            card_ids.member? card_id.to_i
          end.keys.map(&:to_s)
          channel_ids.each do |channel_id|
            final_results[org_id][channel_id] ||= {}
            query_result["values"].each do |val|
              timestamp = Time.parse(val["time"]).to_i
              final_results[org_id][channel_id][timestamp] ||= {
                "num_cards_commented_in_channel" => 0
              }
              final_results[org_id][channel_id][timestamp]["num_cards_commented_in_channel"] += val["count"]
            end
          end
        end
        remove_empties(final_results)
      end

      # We get per-card completion counts for ALL CARDS over the past day,
      # then map this according to channel membership.
      # returns:
      #   { <org_id> => { <channel_id> => { <time> => { <record> } } }
      def get_num_cards_completed_in_channel(cards_channels_map:, orgs:, **opts)
        query = InfluxQuery.new("cards")
          .select!("COUNT(card_id)")
          .filter!(:event, "event", "=", "card_marked_as_complete")
          .add_where_in_filter!(:org_id, "org_id", orgs.map(&:id).map(&:to_s))
          .add_time_filters!(
            # start_date is the bound for the earliest data.
            # it refers to the start of a day
            start_date: (end_of_yesterday - self::TIME_RANGE + 1.days).beginning_of_day.to_i,
            # end_date is the bound for the most recent data
            # it refers to the end of that day
            end_date: end_of_yesterday.to_i
          )
          .group_by!("org_id,_card_id,time(1d)")
        debug_log(
          "querying for num cards completed in channel",
          query.to_plain_query
        )
        query_results = query.resolve
        final_results = {}
        query_results.each do |query_result|
          org_id, card_id = query_result["tags"].values_at "org_id", "_card_id"
          final_results[org_id] ||= {}
          next unless cards_channels_map[org_id.to_i]
          channel_ids = cards_channels_map[org_id.to_i].select do |channel_id, card_ids|
            card_ids.member? card_id.to_i
          end.keys.map(&:to_s)
          channel_ids.each do |channel_id|
            final_results[org_id][channel_id] ||= {}
            query_result["values"].each do |val|
              timestamp = Time.parse(val["time"]).to_i
              final_results[org_id][channel_id][timestamp] ||= {
                "num_cards_completed_in_channel" => 0
              }
              final_results[org_id][channel_id][timestamp]["num_cards_completed_in_channel"] += val["count"]
            end
          end
        end
        remove_empties(final_results)
      end

      # Recursively removes blank elements
      def remove_empties(obj)
        obj.each_with_object({}) do |(key, val), memo|
          next if val.blank?
          if val.is_a?(Hash)
            next_val = remove_empties(val)
            next if next_val.blank?
            memo[key] = next_val
          else
            memo[key] = val
          end
        end
      end

      # NOTE: need to optimize for prod
      def channel_followers_by_org(orgs)
        Follow
          .all
          .joins(:user)
          .where(followable_type: "Channel")
          .where(
            users: {
              organization_id: orgs.map(&:id)
            }
          )
          .select([
            "follows.id",
            "users.organization_id",
            "follows.user_id",
            "follows.followable_id AS channel_id"
          ].join(","))
          .find_in_batches(batch_size: 50_000)
          .reduce({}) do |memo, results|
            debug_log "#{results.length} Follow records found"
            memo.deep_merge(
              results
                .group_by(&:organization_id)
                .transform_values do |records|
                  records.group_by(&:channel_id).transform_values do |val|
                    Set.new(val.map(&:user_id))
                  end
                end
            )
          end
      end

      # NOTE: need to optimize for prod
      def cards_by_channel_and_org(orgs)
        ChannelsCard
          .all
          .joins(:card)
          .where(
            cards: {
              organization_id: orgs.map(&:id)
            }
          )
          .select([
            "channels_cards.id",
            "cards.organization_id",
            "channel_id",
            "cards.id AS card_id"
          ].join(","))
          .find_in_batches(batch_size: 50_000)
          .reduce({}) do |memo, results|
            debug_log "#{results.length} ChannelsCard records found"
            memo.deep_merge(
              results
                .group_by(&:organization_id)
                .transform_values do |records|
                  records.group_by(&:channel_id).transform_values do |val|
                    Set.new(val.map(&:card_id))
                  end
                end
            )
          end
      end

      def build_platform_key(prefix, platform)
        platform_name = platform.downcase[/[a-z]+/]
        platform_name = "null" if platform_name.blank?
        "#{prefix}#{platform_name}_platform"
      end

      def end_of_yesterday
        @end_of_yesterday ||= (Time.now.utc - 1.day).end_of_day
      end

      def start_of_yesterday
        @start_of_yesterday ||= end_of_yesterday.beginning_of_day
      end

      def clear_memoization!
        @end_of_yesterday = nil
        @start_of_yesterday = nil
      end

      def num_days_in_range
        (
          end_of_yesterday.to_date - (
            end_of_yesterday - self::TIME_RANGE
          ).to_date
        ).to_i
      end

      # merges a value to a hash using nested keys.
      def buried_merge!(obj, *keys, val_to_merge)
        keys.reduce(obj) do |memo, key|
          memo[key] ||= {}
        end.merge!(val_to_merge)
        nil
      end

      # The final data structure that is recorded to influx
      def influx_record(org_id, channel_id, timestamp, record)
        {
          timestamp: timestamp,
          tags: {
            org_id: org_id,
            channel_id: channel_id
          },
          values: record
        }
      end

      # Ensures that every needed key in every record has a value.
      # Values default to zero.
      def set_default_values_for_records(unique_keys, records)
        records.each do |record|
          unique_keys.each do |key|
            record[:values][key] ||= 0
          end
        end
      end

      def debug_log(*msgs)
        puts "------" * 4
          msgs.each do |msg|
            puts msg, ""
          end
        puts "------" * 4, ""
      end

    end
  end
end

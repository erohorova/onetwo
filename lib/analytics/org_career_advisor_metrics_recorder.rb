module Analytics
  class OrgCareerAdvisorMetricsRecorder < Analytics::OrgMetricsRecorder
    ORG_CAREER_ADVISOR_EVENTS = [
      'org_career_advisor_created',
      'org_career_advisor_deleted',
      'org_career_advisor_edited'
    ].freeze

    ORG_CAREER_ADVISOR_EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end
        # We only care about updates to these attributes
    SIGNIFICANT_EDIT_ATTRIBUTES = Set.new(%w{
      links
      level
    })

    class << self

      def record(
        # Mandatory stuff
        org:, event:, actor:, timestamp:,
        # Optional 
        career_advisor: nil, changed_column: nil, old_val: nil, new_val: nil,
        # Extra metadata
        additional_data: {}
      )
        validate_career_advisor_event(event)
        career_advisor &&= RecursiveOpenStruct.new(career_advisor)
        super(
          org: org,
          event: event,
          actor: actor,
          timestamp: timestamp,
          additional_data: additional_data
        ) do |data|
          
          merge_career_advisor_information(data, career_advisor) if career_advisor
          
          if changed_column
            data[:values].merge!(changed_column: changed_column)
            data[:values].merge!(
              old_val: old_val.to_s,
              new_val: new_val.to_s
            )
          end
          
        end
      end

      def merge_career_advisor_information(data, career_advisor)
        data[:values].merge!(
          career_advisor_id:    career_advisor.id.to_s,
          career_advisor_skill: career_advisor.skill,
          career_advisor_links: career_advisor.links
        )
      end

      def is_ignored_event?(changed_column)
        !SIGNIFICANT_EDIT_ATTRIBUTES.member?(changed_column.to_s)
      end

      private

      def validate_career_advisor_event(event)
        unless ORG_CAREER_ADVISOR_EVENTS.include?(event)
          raise "Invalid event: #{event}. Valid events: #{ORG_CAREER_ADVISOR_EVENTS.join(",")}"
        end
      end
    end
  end
end
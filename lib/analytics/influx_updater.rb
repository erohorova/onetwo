class Analytics::InfluxUpdater

# =============================================================================
# USAGE EXAMPLE
#
#   InfluxUpdater.process_records(
#     measurement: "foobars",
#     in_batches_of: 10
#   ) do |records|
#     records.each { |record| record["tags"]["_foo"] = "asd" }
#   end
#
# IMPORTANT NOTE:
#
# This class can only be used to update FIELDS, not TAGS
# It is very easy to update fields because a "record" is defined as a unique
# combination of timestamp, and tags.
#
# If we change only fields, then we can push the record with the same
# timestamp and tag set, and it will OVERWRITE the existing record on influx.
# =============================================================================

  class << self

    def analytics_version
      InfluxdbRecorder::ANALYTICS_VERSION
    end

    # updater_blk is passed each batch of records sequentially, and should
    # mutate the records. After the block is called, the mutated records are pushed
    # to influx.
    # Note that the timestamp cannot be altered - otherwise a new record will be added
    # instead of an update.
    def process_records(
      measurement:, # String
      in_batches_of: 1000, # max num records to process at once
      date_chunk_range: 30, # max number of days to process at once
      start_date:, # Date object
      end_date:, # Date object
      from_version:, # String
      write_client:, # InfluxDB::Client instance
      query_blk: nil, # Optional proc which can be used to change the query
      **query_params, # Accepts org_id and event, to filter.
                      # Additional filters can be applied using query_blk
      &updater_blk # block to call for each record group before inserting
    )
      updated_count = 0
      raise("no updater block given") unless updater_blk.is_a?(Proc)
      time_chunks = get_time_chunks(start_date, end_date, date_chunk_range)
      tag_keys = get_tag_keys(measurement)
      time_chunks.each do |(start_time, end_time)|
        offset = 0
        loop do

          puts [
            "getting #{measurement}",
            "offset=#{offset}",
            "start_time=#{Time.at(start_time).to_date}",
            "end_date=#{Time.at(end_time).to_date}"
          ].join(", ")

          records = get_records(query_params.merge(
            from_version: from_version,
            measurement:  measurement,
            limit:        in_batches_of,
            tag_keys:     tag_keys,
            start_time:   start_time,
            end_time:     end_time,
            offset:       offset,
            query_blk:    query_blk,
          ))
          updater_blk.call(records)
          puts "pushing #{records.length} records to #{measurement}"
          push_updated_records(measurement, records, write_client)
          updated_count += records.length
          break if records.length < in_batches_of
          offset += in_batches_of
        end
      end
      { updated_count: updated_count }
    end

    private

    # Given start/end dates and # of days to chunk by,
    # Splits up the range into start/end for chunks.
    # Returns nested array: [ [<chunk_start>, <chunk_end>]... ]
    def get_time_chunks(start_date, end_date, date_chunk_range)
      unless [start_date, end_date].all? { |date| date.is_a?(Date) }
        raise ArgumentError, "invalid date(s)"
      end
      chunks = []
      loop do
        next_date = [(start_date + date_chunk_range.days), end_date].min
        chunk_start = start_date.to_time.to_i
        chunk_end = next_date.to_time.to_i
        chunks.push([chunk_start, chunk_end])
        start_date = next_date
        break if start_date == end_date
      end
      chunks
    end

    def push_updated_records(measurement, records, write_client)
      records.map! do |record|
        record = record.symbolize_keys
        record[:tags] = record.delete(:tags).symbolize_keys
        record[:values] = record.delete(:values).symbolize_keys
        record
      end
      Analytics::MetricsRecorder.influxdb_bulk_record(
        measurement,
        records,
        write_client: write_client
      )
    end

    def get_tag_keys(measurement)
      results = INFLUXDB_CLIENT.query(
        %{show tag keys from "#{measurement}"}
      ).shift&.fetch("values", nil)
      raise("Can't find measurement!") unless results.is_a?(Array)
      results.map { |result| result["tagKey"] }
    end

    def get_records(
      measurement:, limit:, offset:, tag_keys:, from_version:,
      start_time:, end_time:, org_id: nil, event: nil, query_blk: nil
    )
      query = InfluxQuery.new("#{measurement}")
        .filter!(:version, "analytics_version", "=", from_version)
        .order!("ASC")
        .limit!(limit)
        .offset!(offset)
        .add_time_filters!(start_date: start_time, end_date: end_time)
      if org_id
        query.filter!(:org_id, "org_id", "=", org_id)
      end
      if event
        query.filter!(:event, "event", "=", event)
      end
      if query_blk
        query = query_blk.call(query)
      end
      results = query.resolve.shift&.fetch("values", nil)
      return [] unless results.is_a?(Array)
      # Unless a group by is given (which it is not, here),
      # influx doesn't separate tags and values in the results.
      # So, we need to do that manually.
      # NOTE: we could maybe fix this by adding "GROUP BY *" to the query.
      results.map do |result|
        new_result = {
          "values" => result,
          "tags" => {}
        }.with_indifferent_access
        time = new_result["values"].delete("time")
        time_obj = Time.parse(time)
        timestamp = time_obj.to_i.to_s.rjust(10, '0') + time_obj.nsec.to_s.rjust(9, '0')
        new_result["timestamp"] = timestamp.to_s
        tag_keys.each do |tag_key|
          new_result["tags"][tag_key] = new_result["values"].delete tag_key
        end
        new_result["tags"].compact!
        new_result["values"].compact!
        new_result
      end
    end

  end
end

# frozen_string_literal: true
module Analytics
  class MetricsRecorder

    # =========================================
    # THESE ARE METHODS WHICH CONVERT RECORDS TO PLAIN RUBY OBJECTS (AKA POROS)
    # THESE POROS ARE SENT AS ARGUMENTS TO BACKGROUND JOBS
    # =========================================

    module Serializers

      # The typical info we need for card measurements.
      # Converts a card into a PORO nested object
      def card_attributes(card)
        with_timestamps(
          card.attributes.merge(
            "resource" => card.resource&.attributes || {},
            "author" => user_attributes(card.author || OpenStruct.new(attributes: {})),
            "video_stream" => card.video_stream&.attributes || {},
            "snippet" => card.snippet,
            "readable_card_type" => card.card_type_display_name,
            "average_rating" => card.card_metadatum&.average_rating
          )
        )
      end

      def partner_center_attributes(partner_center)
        partner_center.attributes.slice *%w{
          id enabled description embed_link integration
        }
      end
      
      def email_template_attributes(email_template)
        email_template.attributes.slice *%w{
          content    creator_id state
          language   title      is_active
          default_id design     id
        }
      end

      # The typical info we need for xapi_credential measurements.
      # Converts a xapi_credential into a PORO nested object
      def xapi_credential_attributes(xapi_credentials)
        with_timestamps(
          xapi_credentials.attributes.except *%w{
            lrs_password_key
          }
        )
      end

      def resource_group_attributes(resource_group)
        resource_group.attributes.slice *%w{
          id name
        }
      end
      
      def role_attributes(role)
        {
          role_id:        role.id.to_s,
          role_name:      role.name,
        }
      end

      def mailer_config_attributes(mailer_config)
        {
          'email_sender_from_address' => mailer_config.from_address,
          'email_sender_from_name'    => mailer_config.from_name,
          'is_current_sender'         => (mailer_config.is_active ? '1' : '0'),
          'is_sender_verified'        => (mailer_config.verification.dig(:domain) ? '1' : '0'),
          'mailer_config_id'          => mailer_config.id.to_s
        }
      end

      # turns a User record into a PORO with all required info for recorder
      def user_attributes(user)
        with_timestamps(
          user.attributes.merge(
            "full_name" => user.full_name,
            "onboarded?" => user.onboarded?,
            "user_org_uid" => user.identity_providers&.first&.uid,
            "upload_mode" => user.upload_mode || "direct_sign_up"
          ).except("email", "created_at")
        )
      end

      # turns a Structure record into a PORO with all required info for recorder
      def structure_attributes(structure)
        with_timestamps(structure.attributes)
      end

      # turns a quiz option record into a PORO with all required info for recorder
      def quiz_question_option_attributes(quiz_question_option)
        with_timestamps(quiz_question_option.attributes)
      end

      # Turns a channel record into a PORO with all required data for metric
      def channel_attributes(channel)
        # may add more in future ..
        with_timestamps(
          channel.attributes.tap do |attrs|
            attrs.merge!(Channel.get_channel_info_for_event(attrs).stringify_keys)
          end.except(
            "image_file_name", "image_content_type", "image_file_size",
            "image_updated_at", "created_at", "updated_at"
          )
        )
      end

      # Turns a organization record into a PORO with all required data for metric
      # TODO: DEPRECATED. THIS METHOD WILL BE REMOVED.
      # Use organization_attributes
      def org_attributes(org)
        with_timestamps(org.attributes).except(
          "users_count"
        )
      end

      # Turns a config record into a PORO with all required data for metric
      def config_attributes(config)
        with_timestamps(config.attributes).slice(
          "name", "id"
        )
      end

      def organization_attributes(org)
        org_attributes(org)
      end

      # Turns a group record into a PORO with all required data for metric
      def team_attributes(group)
        with_timestamps(group.attributes)
      end

      # Turns a cards_rating record into a PORO with all required data for metric
      def cards_rating_attributes(cards_rating)
        with_timestamps(cards_rating.attributes.except("id"))
      end

      # Turns a comment record into a PORO with all required data for metric
      def comment_attributes(comment)
        with_timestamps(comment.attributes)
      end

      # Turns a custom_field record into a PORO with all required data for metric
      def custom_field_attributes(custom_field)
        fields = %w{display_name id}
        with_timestamps(custom_field.attributes.slice(*fields))
      end

      def user_custom_field_attributes(user_custom_field)
        fields = %w{value id}
        with_timestamps(user_custom_field.attributes.slice(*fields))
      end

      # Turns a user profile record into a PORO with all required data for metric
      def user_profile_attributes(user_profile)
        with_timestamps(user_profile.attributes).except(
          "learning_topics", "expert_topics"
        )
      end

      # Turns a sso record into a PORO with all required data for metric
      def sso_attributes(sso)
        with_timestamps(sso.attributes.except("id"))
      end

      def skill_attributes(skill)
        with_timestamps(skill.attributes).except('created_at', 'updated_at')
      end

      def skills_user_attributes(skills_user)
        with_timestamps(skills_user.attributes).except('created_at', 'updated_at')
      end

      def pronouncement_attributes(pronouncement)
        with_timestamps(pronouncement.attributes).slice(
          'label', 'start_date', 'end_date', 'id', 'is_active'
        )
      end

      def topic_request_attributes(topic_request)
        with_timestamps(topic_request.attributes).slice(
          'id', 'label', 'requestor_id', 'approver_id', 'state', 'publisher_id'
        )
      end

      def embargo_attributes(embargo)
        with_timestamps(embargo.attributes).slice(
          'id','category', 'value'
        )
      end

      def user_badge_attributes(user_badge)
        with_timestamps(
          user_badge.attributes.merge(
            badge_id:     user_badge.id,
            badge_title:  user_badge.badging.title,
            badge_type:   user_badge.badging.type,
          )
        )
      end

      def user_onboarding_attributes(user_onboarding)
        with_timestamps(user_onboarding.attributes).slice(
          'id','current_step', 'status'
        )
      end
      
      def sources_credential_attributes(sources_credential)
        with_timestamps(sources_credential.attributes).slice(
          'id','source_id', 'shared_secret', 'auth_api_info', 'source_name'
        )
      end

      def clc_badging_attributes(badging)
        with_timestamps(badging.attributes).slice(
          'id','title', 'badge_id', 'badgeable_id', 'type', 'target_steps', 'all_quizzes_answered'
        )
      end

      def card_badging_attributes(badging)
        with_timestamps(badging.attributes).slice(
          'id','title', 'badge_id', 'badgeable_id', 'type', 'target_steps', 'all_quizzes_answered'
        )
      end

      def clc_attributes(clc)
        with_timestamps(clc.attributes).slice(
          'id','entity_id', 'entity_type', 'from_date', 'to_date', 'target_score', 'target_steps', 'name'
        )
      end

      def career_advisor_attributes(career_advisor)
        with_timestamps(career_advisor.attributes).slice(
          'id','skill', 'links'
        )
        
      end

      def career_advisor_card_attributes(career_advisor_card)
        with_timestamps(career_advisor_card.attributes).slice(
          'id','card_id', 'career_advisor_id', 'level'
        )
      end

      def live_comment_attributes(comment)
        with_timestamps(comment.attributes)
      end
    end
  end
end

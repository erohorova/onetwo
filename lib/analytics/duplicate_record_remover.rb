# frozen_string_literal: true

module Analytics
  class DuplicateRecordRemover

    # The columns used to detect duplicates;
    # array of strings e.g. ["_card_id", "_user_id"]
    attr_accessor :grouping_columns

    # Conditions to apply to query;
    # Hash e.g. { event: "card_viewed" }
    attr_accessor :conditions

    # Time window to detect duplicates;
    # String e.g. "1s"
    attr_accessor :time_frame

    # Must provide a field to use with COUNT
    # String e.g. "user_id", should be set on 100% of records
    attr_accessor :field

    # The lower time bounds to check for duplicates
    # Integer, e.g. Time.now.to_i
    attr_writer :min_time
    def min_time
      @min_time || raise(RuntimeError, "min_time has not been set")
    end

    # The upper time bounds to check for duplicates
    # Integer, e.g. Time.now.to_i
    attr_writer :max_time
    def max_time
      @max_time || raise(RuntimeError, "max_time has not been set")
    end

    # measurement to query
    # String e.g. "cards", has no default value and must be set.
    attr_writer :measurement
    def measurement
      @measurement || raise(RuntimeError, "measurement has not been set")
    end

    # Each org gets run individually. To run only some, pass the
    # list as an argument to #initialize
    attr_reader :org_ids

    def initialize(**opts)
      # Except min_time, all these config options are optional
      defaults = {
        grouping_columns: [],
        conditions:       {},
        time_frame:       "1s",
        field:            "user_id",
        max_time:         Time.now.to_i,
        org_ids:          Organization.all.pluck(:id)
      }
      opts = defaults.merge(opts)
      # Mass-assign all passed options to instance variables
      opts.each { |key, val| instance_variable_set("@#{key}", val) }
    end

    def run
      org_ids.map(&:to_s).each do |org_id|
        # First find the attribute sets which have duplicates
        dups = find_duplicate_records(org_id)
        raise(RuntimeError, "no dups found") if dups.none?
        log.info "#{dups[0]["values"].length} attribute sets detected with duplicate records"
        # Pass the attributes to the remove_duplicates method
        dups.first["values"].each { |attrs| remove_duplicates org_id, attrs }
      end
    end

    # Removes duplicates of a single attribute set
    def remove_duplicates(org_id, attrs)
      # Select the significant attributes for the duplicate
      filter_attrs = attrs
        .except("time", "_duplicate_count")
        .merge("org_id" => org_id)

      # Create start/end bounds for our query.
      # We will get data for all records with the same attr set
      # within this time frame.
      time = Time.parse(attrs["time"]).to_i
      start_date = time - 1
      end_date = time + 1

      # Create a query to find the records with these attrs
      query = InfluxQuery.new(measurement)

      apply_filters query, filter_attrs
      query.add_time_filters!(start_date: start_date, end_date: end_date)

      # Fire off the query, get the records with these attrs
      results = (query.resolve.first || {}).fetch "values", []
      log.info "#{results.length} records detected with attrs #{filter_attrs.merge(time: time)}"

      # Create the queries to delete the records.
      # this block is run for all the records except 1
      results.tap(&:shift).each do |record|
        delete_record_with_attrs(record, filter_attrs)
      end
    end

    def delete_record_with_attrs(record, filter_attrs)
      time = Time.parse record["time"]
      time_ns = (time.to_i.to_s + time.nsec.to_s).to_i
      delete_query = InfluxQuery.new(measurement)
      apply_filters delete_query, filter_attrs
      # We query on the exact valiue of time, in nanoseconds
      delete_query.filter!(:time, "time", "=", time_ns)
      # We log the records to create before deleting them
      log_records(delete_query)
      # Delete the records for real
      delete_query.delete_all!.resolve
    end

    # Fires off the passed query and log the records' attributes
    def log_records(query)
      records = query.resolve.first.try(:[], "values") || []
      log.info "will delete #{records.length} records"
      return unless records
      File.open("./log/duplicate_cleanup_log.txt", "a") do |f|
        f.puts records.to_json
      end
    end

    private


    # Given a query and a hash of filters, applies the filters
    def apply_filters(query, filters)
      filters.each do |key, val|
        query.filter!(key.to_sym, key, "=", val)
      end
    end

    # Finds attribute sets that have duplicates
    def find_duplicate_records(org_id)
      query = duplicate_records_query(org_id)
      return query.resolve
    end

    # Query for #find_duplicate_records
    def duplicate_records_query(org_id)
      subquery_groupings = [
        *grouping_columns,
        "event",
        "org_id",
        "time(#{time_frame})"
      ]

      # We are just selecting the count of records here.
      # However, more data is available to the outer query because of GROUP BY
      subquery_obj = InfluxQuery.new(measurement)
        .select!("COUNT(#{field}) AS _duplicate_count")
        .add_time_filters!(
          start_date: min_time,
          end_date: max_time
        )
        .filter!(:org_id, "org_id", "=", org_id)
        .group_by!(subquery_groupings.join(","))

      apply_filters(subquery_obj, conditions)

      # We have to un-parameterize the subquery, e.g. remove the %{param}
      # variables and interpolate the values.
      subquery_str = subquery_obj.to_plain_query as_subquery: true

      query = InfluxQuery.new(subquery_str, has_subquery: true)
        .select!([*grouping_columns, "event", "org_id", "_duplicate_count"].join(","))
        .filter!(:_duplicate_count, "_duplicate_count", ">", 1)

      return query
    end

  end
end
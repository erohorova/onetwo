# frozen_string_literal: true

module Analytics
  class DeleteUserSearchHistory

    def self.run!(user_id:, org_id:)
      InfluxQuery.new("searches").
        filter!(:user_id, "_user_id", "=", user_id.to_s).
        filter!(:org_id, "org_id", "=", org_id.to_s).
        delete_all!.
        resolve
    end

  end
end
# frozen_string_literal: true
module Analytics
  class UserSuspensionMetricsRecorder < UserMetricsRecorder
    USER_SUSPENDED_EVENTS = ['user_suspended', 'user_unsuspended']
    USER_SUSPENDED_EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    def self.record(org:, suspended_user:, timestamp:, actor:, event:, additional_data:)
      suspended_user = RecursiveOpenStruct.new(suspended_user)
      super(
        org: org,
        timestamp: timestamp,
        actor: actor,
        event: event,
        additional_data: additional_data
      ) do |data|
        data[:values].merge!(suspended_user_id: suspended_user.id.to_s)
      end
    end
  end
end
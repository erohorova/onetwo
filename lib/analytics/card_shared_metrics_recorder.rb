# frozen_string_literal: true
module Analytics
  class CardSharedMetricsRecorder < CardMetricsRecorder
    CARD_SHARED_EVENT =['card_shared']
    CARD_SHARED_EVENT.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end


    def self.record(
      org:, card:, timestamp:, event:, team_id: nil, actor:,
      shared_to_user: nil, additional_data:
    )
      super(
        org: org,
        card: card,
        timestamp: timestamp,
        event: event,
        actor: actor,
        additional_data: additional_data
      ) do |data|
        shared_to_user = RecursiveOpenStruct.new(shared_to_user) if shared_to_user
        if team_id
          data[:values].merge! team_id: team_id.to_s
        end
        if shared_to_user
          data[:values].merge! shared_to_user_id: shared_to_user.id.to_s
        end
      end
    end

  end
end

# frozen_string_literal: true

module Analytics
  class ViewEventMetricRecorder

    def self.record_view_event(entity:, viewer:)
      if entity.respond_to?(:push_view_event)
        entity.push_view_event(viewer)
      else
        log.info(
          "No method implemented for #{entity.class}#push_view_event"
        )
      end
    end

    def self.record_click_through_event(entity:, viewer:)
      if entity.respond_to?(:push_click_through_event)
        entity.push_click_through_event(viewer)
      else
        log.info(
          "No method implemented for #{entity.class}#push_click_through_event"
        )
      end
    end

  end
end

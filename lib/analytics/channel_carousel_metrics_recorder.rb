# frozen_string_literal: true

module Analytics

  class ChannelCarouselMetricsRecorder < ChannelMetricsRecorder
    CHANNEL_CAROUSEL_EVENTS = %w{
      channel_carousel_created
      channel_carousel_enabled
      channel_carousel_disabled
      channel_carousel_edited
      channel_carousel_deleted
      channel_added_to_channel_carousel
      channel_removed_from_channel_carousel
    }

    CHANNEL_CAROUSEL_EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self

      def record(
        # REQUIRED ARGS
        structure:, channel: {}, actor:, event:, timestamp:,
        # OPTIONAL CHANGE-RELATED ARGS
        changed_column: nil, old_val: nil, new_val: nil,
        # METADATA
        additional_data:
      )
        return if is_ignored_event?(changed_column)
        actor_struct, structure, channel = [
          actor, structure, channel
        ].map &RecursiveOpenStruct.method(:new)
        channel.organization_id ||= actor_struct.organization_id

        # special case, Name of the event is different when
        # channel carousel is enabled/disabled
        if changed_column && changed_column == 'enabled'
          event = new_val&.to_s&.to_bool ? 'channel_carousel_enabled' : 'channel_carousel_disabled'
        end

        super(
          actor: actor,
          channel: channel.to_h,
          event: event,
          additional_data: additional_data,
          timestamp: timestamp
        ) do |data|
          merge_structure_info(data, structure)
          data[:values][:changed_column] = changed_column
          data[:values][:new_val] = new_val&.to_s
          data[:values][:old_val] = old_val&.to_s
        end
      end

      def merge_structure_info(data, structure)
        data[:values][:channel_carousel_creator_id] = structure.creator_id.to_s
        data[:values][:channel_carousel_id] = structure.id.to_s
        data[:values][:channel_carousel_display_name] = structure.display_name
        data[:values][:is_channel_carousel_enabled] = structure.enabled ? '1' : '0'
        data[:values][:channel_carousel_slug] = structure.slug
      end

      def is_ignored_event?(changed_column)
        return false unless changed_column
        whitelisted_change_attrs.exclude?(changed_column)
      end

      def whitelisted_change_attrs
        %w{
          enabled
          context
          display_name
        }
      end

    end

  end

end
class Analytics::InfluxCardinalityMigration

  def self.process_all(**opts)
    start_time = Time.now.to_i

    # =========================================
    # NOTE: UNCOMMENT THESE AS NEEDED!
    # =========================================

    # puts "PROCESSING cards"
    # process_cards(opts.merge(start_date: Date.new(2017, 05, 14)))

    # puts "PROCESSING card assigned"
    # process_card_assigned(opts.merge(start_date: Date.new(2016, 9)))

    # puts "PROCESSING orgs"
    # process_orgs(opts.merge(start_date: Date.new(2018,2)))

    # puts "PROCESSING channels"
    # process_channels(opts.merge(start_date: Date.new(2015, 12, 1)))

    # puts "PROCESSING users"
    # process_users(opts.merge(start_date: Date.new(2015, 12, 3)))

    # puts "PROCESSING groups"
    # process_groups(opts.merge(start_date: Date.new(2016, 4, 1)))

    # puts "PROCESSING searches"
    # process_searches(opts.merge(start_date: Date.new(2018, 1, 17)))

    # puts "PROCESSING user_scores"
    # process_user_scores(opts.merge(start_date: Date.new()))

    end_time = Time.now.to_i
    puts "DONE. took #{end_time - start_time} seconds"
  end

  def self.process_card_assigned(**opts)
    log.warn "should delete all card_assigned events before running this!"
    sleep 3
    migrate_generic("cards", opts.merge(ProcessCardsConfig).merge(
      query_blk: Proc.new { |query|
        query.filter! :event, "event", "=", "card_assigned"
      }
    )) do |records|
      failures = 0
      card_ids = records.map { |record| record["tags"]["_card_id"] }
      user_ids = records.map { |record| record["values"]["assigned_to_user_id"] }
      assignments = Assignment.
        where(assignable_type: "Card").
        where(assignable_id: card_ids, user_id: user_ids).
        includes(:team_assignments).
        group_by(&:assignable_id).
        stringify_keys
      records.each do |record|
        card_id = record["tags"]["_card_id"]
        assigned_to_user_id = record["values"]["assigned_to_user_id"]
        record_assignment = assignments[card_id]&.find do |assignment|
          assignment.user_id.to_s == assigned_to_user_id
        end
        unless record_assignment
          failures += 1
          next
        end
        assignor_id = record_assignment.team_assignments.first&.assignor_id&.to_s
        if assignor_id
          record["tags"]["_user_id"] = assignor_id
          record["values"]["user_id"] = assignor_id
        end
      end
      puts "CANT FIND ASSIGNOR ID: #{failures}"
    end
  end

  def self.process_cards(**opts)
    migrate_generic("cards", opts.merge(ProcessCardsConfig).merge(
      query_blk: Proc.new { |query|
        query.filter!(:event, "event", "!=", "card_assigned")
      }
    ))
  end

  def self.process_channels(**opts)
    migrate_generic("channels", opts.merge(ProcessChannelsConfig))
  end

  def self.process_orgs(**opts)
    migrate_generic("organizations", opts.merge(ProcessOrgsConfig)) do |records|
      records.each do |record|
        record["tags"]["org_id"] = record["tags"].delete("_org_id")
        record["values"].delete("org_id")
        record["values"].delete("_org_id")
      end
      # We are missing actor id on the org events.
      # Backfill them through a database lookup.
      org_ids = records.map { |record| record["tags"]["org_id"] }
      admin_ids = Organization.
        where(id: org_ids).
        joins(:users).
        where("users.organization_role = 'admin'").
        select("organizations.id, users.id AS admin_id").
        group_by(&:id).
        stringify_keys.
        transform_values { |org_data| org_data.first.admin_id }
      records.each do |record|
        org_id = record["tags"]["org_id"]
        admin_id = admin_ids[org_id]
        record["tags"]["_user_id"] = admin_id
        record["values"]["user_id"] = admin_id
      end
    end
  end

  def self.process_users(**opts)
    migrate_generic("users", opts.merge(ProcessUsersConfig))
  end

  def self.process_groups(**opts)
    migrate_generic("groups", opts.merge(ProcessGroupsConfig))
  end

  def self.process_searches(**opts)
    migrate_generic("searches", opts.merge(ProcessSearchesConfig))
  end

  def self.process_user_scores(**opts)
    migrate_generic("user_scores", opts.merge(ProcessUserScoresConfig))
  end

  def self.recorder_client=(client)
    @recorder_client = client
  end

  class << self

    def self.recorder_client
      @recorder_client || raise("need to set recorder_client first")
    end

    private

    def analytics_version
      InfluxdbRecorder::ANALYTICS_VERSION
    end


    def recorder_client
      @recorder_client
    end

    def migrate_generic(
      measurement,
      tags_to_become_fields: [],
      tags_to_delete: [],
      fields_to_stringify: [],
      fields_to_delete: [],
      start_date: nil,
      end_date: nil,
      org_id: nil,
      query_blk: nil,
      &blk
    )
      start_date ||= Date.new(1970, 1, 1)
      end_date ||= Date.today + 5.days
      Analytics::InfluxUpdater.process_records(
        measurement: measurement,
        in_batches_of: 1_000,
        from_version: analytics_version,
        write_client: recorder_client,
        start_date: start_date,
        end_date: end_date,
        org_id: org_id,
        query_blk: query_blk
      ) do |records|
        records.each do |record|
          tags_to_become_fields.each do |col|
            record["values"][col] = record["tags"].delete(col).to_s
          end
          tags_to_delete.each do |col|
            record["tags"].delete(col)
          end
          fields_to_stringify.each do |col|
            record["values"][col] &&= record["values"][col].to_s
          end
          fields_to_delete.each do |col|
            record["values"].delete(col)
          end
          set_is_system_generated(record)
        end
        blk&.call(records)
      end
    end

    def set_is_system_generated(record)
      is_system_generated = record["values"]["platform"].blank? ? "1" : "0"
      record["tags"]["is_system_generated"] ||= is_system_generated
    end

  end

  ProcessCardsConfig = {
    tags_to_become_fields: %w{
      platform
      is_admin_request
      card_author_id
      card_state
      card_type
      org_hostname
      ecl_source_name
      is_user_generated
      is_live_stream
      card_bookmark_id
      is_card_promoted
      channel_id
      is_channel_featured
      is_channel_curated
      is_channel_public
      comment_id
      changed_column
      card_rating
      card_level
      is_correct
      pin_id
      user_handle
    },
    tags_to_delete: %w{
      _channel_id
      _assigned_to_user_id
      _team_id
      _poll_option_id
      _quiz_option_id
    },
    fields_to_stringify: %w{
      assigned_to_user_id
      card_id
      pathway_id
      poll_option_id
      quiz_option_id
      team_id
      user_id
    },
    fields_to_delete: %w{
      changes
      new_value
      old_value
      channel_id_1
    }
  }

  ProcessChannelsConfig = {
    tags_to_become_fields: %w{
      platform
      is_admin_request
      is_channel_featured
      is_channel_curated
      is_channel_public
      is_ecl_enabled
      changed_column
      channel_carousel_creator_id
      is_channel_carousel_enabled
      channel_carousel_slug
    },
    tags_to_delete: %w{
      _collaborator_id
      _curator_id
      _ecl_source_id
      _follower_id
    },
    fields_to_stringify: %w{
      channel_id
      collaborator_id
      curator_id
      follower_id
      user_id
    },
    fields_to_delete: %w{
      channel_id_1
      new_value
      old_value
    }
  }

  ProcessOrgsConfig = {
    tags_to_become_fields: %w{
      platform
      is_admin_request
      org_host_name
      changed_column
    },
    tags_to_delete: %w{
    },
    fields_to_stringify: %w{
      org_client_id
      org_enable_role_based_authorization
      org_is_open
      org_is_registrable
      org_savannah_app_id
      org_send_weekly_activity
      org_show_onboarding
      org_show_sub_brand_on_login
      org_social_enabled
      org_xapi_enabled
    },
    fields_to_delete: %w{
      new_value
      old_value
    }
  }

  ProcessUsersConfig = {
    tags_to_become_fields: %w{
      platform
      is_admin_request
      user_org_uid
      org_hostname
      onboarding_status
      changed_column
      added_role
      removed_role
      followed_user_id
      controller_action
      followed_user_handle
      followed_user_full_name
      full_name
      user_handle
    },
    tags_to_delete: %w{
      _user_role
      _follower_id
      _member_user_id
      _topic_id
      _visited_profile_user_id
      _suspended_user_id
    },
    fields_to_stringify: %w{
      follower_id
      level
      member_user_id
      suspended_user_id
      user_id
      visited_profile_user_id
      sign_in_at
    },
    fields_to_delete: %w{
      new_value
      old_value
    }
  }

  ProcessGroupsConfig = {
    tags_to_become_fields: %w{
      platform
      is_admin_request
      org_hostname
      changed_column
      user_handle
    },
    tags_to_delete: %w{
      _group_user_id
    },
    fields_to_stringify: %w{
      group_id
      group_user_id
      user_id
    },
    fields_to_delete: %w{
      old_value
      new_value
    }
  }

  ProcessSearchesConfig = {
    tags_to_become_fields: %w{
      platform
      is_admin_request
      org_hostname
    },
    tags_to_delete: %w{
    },
    fields_to_stringify: %w{
      user_id
    }
  }

  ProcessUserScoresConfig = {
    tags_to_become_fields: %w{
      platform
      actor_id
      owner_id
      is_admin_request
    }
  }

end

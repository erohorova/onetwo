module Analytics
  class UserLoginMetricsRecorder < UserMetricsRecorder

    def self.record(org:, actor:, metadata:, timestamp:)
      return unless actor
      super(
        org: org,
        timestamp: timestamp,
        event: "user_logged_in",
        actor: actor,
        additional_data: metadata
      ) do |data|
        data[:values].merge!(
          controller_action: "#{metadata[:controller]}##{metadata[:action]}"
        )
      end
    end

  end
end

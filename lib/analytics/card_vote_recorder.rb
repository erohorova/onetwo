# frozen_string_literal: true

module Analytics
  class CardVoteRecorder < CardMetricsRecorder
    CARD_LIKE_EVENTS = ['card_liked', 'card_unliked'].freeze
    CARD_LIKE_EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self

      def record(org:, card:, actor:, timestamp:, event:, additional_data: {})
        validate_card_event(event)
        super(
          org: org,
          card: card,
          event: event,
          actor: actor,
          timestamp: timestamp,
          additional_data: additional_data
        )
      end

      private

      def validate_card_event(event)
        raise 'Valid events: card_liked and card_unliked' unless CARD_LIKE_EVENTS.include?(event)
      end
    end
  end
end

module Analytics
  class UserOnboardingRecorder < UserMetricsRecorder
    USER_ONBOARDING_EVENTS = [
      'user_onboarding_created',
      'user_onboarding_started',
      'user_onboarding_completed',
      'user_onboarding_deleted'
    ].freeze
    
    USER_ONBOARDING_EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self
      def record(
        org:, timestamp:, user_onboarding:, actor:, event:, additional_data: {},
        changed_column: nil, old_val: nil, new_val: nil
      )
        validate_user_event(event)
        user_onboarding = RecursiveOpenStruct.new(user_onboarding)

        super(
          org: org,
          timestamp: timestamp,
          actor: actor,
          event: event,
          additional_data: additional_data
        ) do |data|
          merge_extra_information(data, user_onboarding)
        end
      end

      def is_ignored_event?(changed_column)
        return false unless changed_column
        changed_column != "status"
      end

      def merge_extra_information(data, user_onboarding)
        data[:values].merge!(
          user_onboarding_id: user_onboarding.id.to_s,
          user_onboarding_current_step: user_onboarding.current_step.to_s,
          user_onboarding_status: user_onboarding.status
        )
      end

      private

      def validate_user_event(event)
        unless USER_ONBOARDING_EVENTS.include?(event)
          raise "Invalid event: #{event}. Valid Events: #{USER_ONBOARDING_EVENTS.join(",")}"
        end
      end
    end
  end
end

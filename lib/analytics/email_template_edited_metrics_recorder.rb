# frozen_string_literal: true
module Analytics
  class EmailTemplateEditedMetricsRecorder < Analytics::OrgEditedMetricsRecorder
    EVENTS = %w{
      org_email_template_edited
    }
    EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self

      def is_ignored_event?(changed_column)
        @allowed_events ||= Set.new(%w{
          content state     language
          title   is_active design
        })
        ! @allowed_events.member?(changed_column.to_s)
      end

    end

  end
end
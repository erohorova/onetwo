# frozen_string_literal: true

module Analytics

  class SearchRecorder < MetricsRecorder
    VALID_EVENTS = ['homepage_searched']

    MEASUREMENT= 'searches'
    VALID_EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self

      def record(actor:, org:, search_query:,
        results_count:, timestamp:, event:, additional_data:
      )
        actor, organization = [
          actor, org
        ].map &RecursiveOpenStruct.method(:new)
        data = {
          tags: {
            _user_id: actor.id.to_s,
            org_id: organization.id.to_s,
            event: event
          },
          values: {
            search_query: search_query,
            org_hostname: organization.host_name,
            results_count: results_count.to_i,
            ###NOTE: PII
            user_full_name: actor.full_name,
            ###
            user_id: actor.id.to_s
          },
          timestamp: timestamp
        }
        merge_additional_data(data, additional_data)
        influxdb_record MEASUREMENT, data
      end

    end

  end

end
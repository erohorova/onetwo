# frozen_string_literal: true
module Analytics
  class CardMetricsRecorder < Analytics::MetricsRecorder
    MEASUREMENT = 'cards'.freeze
    VALID_EVENTS = %w{
      card_created
      card_created_virtual
      card_deleted
      card_promoted
      card_unpromoted
      card_marked_as_complete
      card_marked_as_uncomplete
      card_video_stream_stopped_viewing
      card_video_stream_started_viewing
      card_video_stream_downloaded
      card_video_stream_previewed
      card_added_to_pathway
      card_removed_from_pathway
      card_published
      card_dismissed
      card_poll_response_created
      card_quiz_response_created
      card_added_to_journey
      card_removed_from_journey
      card_added_to_career_advisor
      card_removed_from_career_advisor
    }.index_by(&:itself).transform_values { true }
    # NOTE: card_added_to_journey is same as section has been added to Jouney
    # Card actually gets added to section(pathway). You can add only section(card)
    # to journey(card). This case analogous to card_removed_from_journey

    VALID_EVENTS.keys.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self

      def record(
        org:, card: nil, event:, actor:, timestamp:, additional_data: {},
        # Optional 
        career_advisor_card: nil
      )
        card, actor, org = [
          card, actor, org
        ].map &RecursiveOpenStruct.method(:new)

        career_advisor_card &&= RecursiveOpenStruct.new(career_advisor_card)

        # If the card has a blank author_id, then we change the event to
        # 'card_created_virtual'.
        if card.author_id.blank? && event == 'card_created'
          event = 'card_created_virtual'
        end

        if event && !(self::VALID_EVENTS.has_key?(event) || get_recorder_for_event(event))
          raise StandardError, 'Event is not registered'
        end

        data = {}

        data[:values]     = extract_fields(org, card, actor)
        data[:tags]       = extract_tags(org, card, event, actor)
        data[:timestamp]  = timestamp

        if career_advisor_card
          data[:values][:career_advisor_card_id]    = career_advisor_card.id.to_s
          data[:values][:career_advisor_id]         = career_advisor_card.career_advisor_id.to_s
          data[:values][:career_advisor_card_level] = career_advisor_card.level
        end

        merge_additional_data(data, additional_data)
        yield(data) if block_given?
        
        if card.organization_id != actor.organization_id
          msg = (
            "ORG ID MISMATCH: " +
            "card id = #{card.id}, " +
            "card org id = #{card.organization_id}, " +
            "actor org id = #{actor.organization_id}" +
            "event = #{event}"
          ) 
          log.error msg
          return
        end

        influxdb_record(MEASUREMENT, data)
      end

      def extract_fields(org, card, actor)
        field_set = {ecl_id: card.ecl_id.to_s}
        field_set[:card_id] =  card.id.to_s
        field_set[:card_title] = card.snippet
        field_set[:card_subtype] = card.card_subtype
        field_set[:card_resource_url] = card.resource&.url
        field_set[:is_public] = card.is_public ? '1' : '0'

        #### NOTE: PII
        field_set[:card_author_full_name] = card.author.try(:full_name)
        field_set[:user_full_name] = actor.try(:full_name)
        field_set[:user_handle] = actor.try(:handle)
        ####

        field_set[:card_duration] = card.duration&.to_i
        field_set[:average_rating] = card.average_rating&.to_f
        field_set[:readable_card_type] = card.readable_card_type.to_s
        field_set[:card_author_id] = card.author_id.to_s
        field_set[:user_id] = actor.try(:id).to_s
        field_set[:org_name] = org.name
        field_set[:card_state] = card.state
        field_set[:card_type] = card.card_type
        field_set[:org_hostname] = org.host_name
        field_set[:is_card_promoted] = card.is_official ? '1' : '0'
        field_set[:ecl_source_name] = card.ecl_source_name
        field_set[:is_user_generated] = card.author_id ? '1' : '0'
        field_set[:is_live_stream] = card.video_stream&.status.eql?("live") ? '1' : '0'
        field_set
      end

      def extract_tags(org, card, event, actor)
        tag_set = {_user_id: actor.try(:id).to_s, _card_id: card.id.to_s}
        tag_set[:org_id] = org.id.to_s
        tag_set[:event] = event
        tag_set
      end

      # A list of events that qualify for total_smartbite_score
      def card_events
        Analytics::MetricsRecorder
          .load_event_recorder_mapping
          .keys
          .grep(/^card_/)
      end

      private

      def merge_additional_data(data, additional_data)
        super
        %i{pathway_id journey_id pathway_name journey_name}.each do |key|
          data[:values][key] = additional_data[key].to_s if additional_data[key]
        end
      end
    end

  end
end

# frozen_string_literal: true
require 'tempfile'
require_relative './user_data_export/influx_strategy.rb'
require_relative './user_data_export/sql_strategy.rb'

module Analytics
  class UserDataExport

    # The bucket where this data gets stored, on AWS S3
    Bucket = AWS::S3.new.buckets[Settings.user_data_export.s3_bucket]

    # All the available sources of data.
    # Strategy classes are required to respond to respond to .get_data(user)
    Strategies = {
      # sql: SqlStrategy,
      influxdb: InfluxStrategy,
    }

    # Gets run for all strategies by default,
    # but can be customized by passing an array of symbols (Strategies keys)
    # e.g. run(user, strategies: [:influxdb, :some_other_strategy])
    def self.run(user, strategies: Strategies.keys)
      data = load_data_from_strategies(user, strategies: strategies)
      zipfile = write_to_zipfile(user, data)
      s3_key = upload_data(user, zipfile)
      # delete the file, it's no longer needed
      zipfile.delete
    end

    # Returns { <source>: { <table_name>: [<rows>...] } }
    def self.load_data_from_strategies(user, strategies: Strategies.keys)
      strategies.each_with_object({}) do |name, memo|
        klass = Strategies[name]
        memo[name] = klass.get_data(user)
      end
    end

    # Generates a temporary link for the s3 key. Expires shortly.
    def self.generate_link_for(s3_key)
      s3_obj = Bucket.objects[s3_key]
      expires_in = 600 # 10 minutes
      s3_obj.url_for(:read, expires: expires_in).to_s
    end

    # Checks if the user has a data export file on S3.
    # Used for verification at the end of the process.
    # Credit: https://gist.github.com/hartfordfive/19097441d3803d9aa75ffe5ecf0696da
    def self.data_export_exists?(user)
      Bucket.objects[build_s3_zipfile_key(user)].exists?
    end

    def self.write_to_zipfile(user, data)
      # Create a tempfile for the zip
      zip_tempfile = Tempfile.new
      # A list keeping track of per-table tempfiles created
      table_tempfiles = []
      # Create a tempfile for each table and add it to the zipfile.
      Zip::File.open(zip_tempfile.path, Zip::File::CREATE) do |zipfile|
        data.each do |source_name, source_data|
          source_data.each do |table_name, table_data|
            # Skip tables with zero records.
            next unless table_data.any?
            Rails.logger.debug "dumping #{table_data.length} records from #{table_name}"
            table_tempfile = dump_table(table_data)
            table_tempfiles << table_tempfile
            table_filename = build_table_filename(user, source_name, table_name)
            zipfile.add(table_filename, table_tempfile.path)
          end
        end
      end
      # delete all the per-table tempfile; they're not needed
      table_tempfiles.each &:delete
      # Return the zip tempfile.
      zip_tempfile
    end

    # uploads data as zip.
    def self.upload_data(user, file)
      # Upload the tempfile to s3
      s3_key = build_s3_zipfile_key(user)
      # Must convert file.path to a Pathname, won't work otherwise
      result = Bucket.objects[s3_key].write(Pathname.new(file.path))
      Rails.logger.debug result
      nil
    end

    def self.delete_from_s3!(s3_key)
      Bucket.objects[s3_key].delete
    end

    # Creates a tempfile for the table and returns the file path.
    def self.dump_table(table_data)
      # Write the data to a tempfile
      Tempfile.new.tap do |file|
        # Not using to_csv, it doesn't work for arrays of hashes.
        CSV.open(file.path, "wb") do |csv|
          csv << table_data[0].keys
          table_data.each do |obj|
            csv << obj.values
          end
        end
        file.close
      end
    end

    # org-<org_id>/user-<user_id>/<source_name>/<table_name>.<ext>
    def self.build_table_filename(user, source_name, table_name, ext='csv')
      "org-#{user.organization_id}/user-#{user.id}/#{source_name}/#{table_name}.#{ext}"
    end

    # the url to download all the user's data.
    # the data should have been previously uploaded
    # org-<org_id>/user-<user_id>/data_export.zip
    def self.build_s3_zipfile_key(user)
      "org-#{user.organization_id}/user-#{user.id}/data_export.zip"
    end

  end
end
# frozen_string_literal: true
module Analytics
  class GroupMembershipMetricsRecorder < Analytics::GroupMetricsRecorder

    VALID_EVENTS  = %w{
      group_user_added group_user_removed group_user_edited
    }.index_by(&:itself).transform_values { true }

    VALID_EVENTS.keys.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self
      def record(
          org:, actor:, group:, member:,
          role:, event:, timestamp:,
          changed_column: nil, old_val: nil, new_val: nil,
          additional_data: {}
        )

        member, _ = [ member ].map &RecursiveOpenStruct.method(:new)

        raise StandardError, 'Group or member can not be blank' unless group || member
        if event && !get_recorder_for_event(event)
          raise StandardError, 'Event is not registered'
        end

        super(
          org: org,
          actor: actor,
          group: group,
          event: event,
          timestamp: timestamp,
          additional_data: additional_data
        ) do |data|
            data[:values].merge! member_fields(member, role)
            if changed_column
              data[:values].merge! changed_column: changed_column
              data[:values].merge! old_val: old_val&.to_s
              data[:values].merge! new_val: new_val&.to_s
            end
        end
      end

      def member_fields(member, role)
        { group_user_role: role, group_user_id: member.id.to_s}
      end

      def is_ignored_event?(changed_column)
        changed_column != "as_type"
      end
    end
  end
end

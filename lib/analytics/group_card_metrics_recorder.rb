# frozen_string_literal: true

class Analytics::GroupCardMetricsRecorder < Analytics::GroupMetricsRecorder
  GROUP_CARD_EVENTS = %w(group_card_assigned)

  GROUP_CARD_EVENTS.each do |event_name|
    register_event_and_recorder(event_name, self)
    const_set("EVENT_#{event_name.upcase}", event_name).freeze
  end

  class << self
    def record(
      org:, actor:, group:,
      card:, event:, timestamp:,
      additional_data: {}
    )

      card, _ = [ card ].map &RecursiveOpenStruct.method(:new)

      raise StandardError, 'Group or card can not be blank' unless group || card
      if event && !get_recorder_for_event(event)
        raise StandardError, 'Event is not registered'
      end

      super(
        org: org,
        actor: actor,
        group: group,
        event: event,
        timestamp: timestamp,
        additional_data: additional_data
      ) do |data|
          data[:values].merge! card_fields(card)
      end
    end

    def card_fields(card)
      { card_id: card.id.to_s }
    end
  end
end
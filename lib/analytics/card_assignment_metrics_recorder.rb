# frozen_string_literal: true

module Analytics

  class CardAssignmentMetricsRecorder < CardMetricsRecorder
    VALID_EVENTS = [
      'card_assigned',
      'card_assignment_dismissed'
    ].index_by(&:itself).transform_values { true }

    VALID_EVENTS.keys.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    def self.record(org:, card:, timestamp:, event:, actor:, assignee:, additional_data:)
      assignee = RecursiveOpenStruct.new(assignee)
      super(
        org: org,
        card: card,
        timestamp: timestamp,
        event: event,
        actor: actor,
        additional_data: additional_data
      ) do |data|
        data[:values][:assigned_to_user_id] = assignee.id.to_s
      end
    end

    def self.is_ignored_event?(changed_column)
      changed_column != "state"
    end

  end

end

# frozen_string_literal: true
module Analytics
  class CardRatedMetricsRecorder < CardMetricsRecorder
    CARD_RATED_EVENT = ['card_relevance_rated']
    CARD_RATED_EVENT.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    def self.record(org:, actor:, cards_rating:, event:, card:, timestamp:, additional_data:)
      cards_rating = RecursiveOpenStruct.new(cards_rating)
      super(
        org: org,
        card: card,
        actor: actor,
        event: event,
        timestamp: timestamp,
        additional_data: additional_data
      ) do |data|
        data[:values].merge! card_rating: cards_rating.rating.to_s
        data[:values].merge! card_level: cards_rating.level
      end
    end

  end
end
module Analytics
  class MigrateUserTopic
    def initialize(org_id:)
      @org = Organization.find(org_id)
      unless @org
        puts "Org not found, pls enter valid id"
        return
      end

      @migrate_topics       = []
      @influx_topic_events  = {}
    end

    def run
      migrate_user_profiles_topics_for_org
    end

    def is_present_in_influx?(user_id:, event_type:, topic:)
      @influx_topic_events.has_key?([user_id&.to_s, topic['topic_id'], event_type])
    end

    def process_topics(profile:, topic_type:, event_type:)
      profile.send("#{topic_type}s".to_sym).each do |topic|
        unless is_present_in_influx?(
          user_id: profile.user_id, event_type: event_type, topic: topic
        )
          migrate_hash = { profile: profile, updated_at: profile.updated_at }
          migrate_hash[:topic_type] = "#{topic_type}s"
          migrate_hash[:topic] = topic
          @migrate_topics << migrate_hash
        end
      end
    end

    # event_type: One of `expert_topics` or `learning_topics`
    def migrate_topic(topic_obj)
      org = Analytics::MetricsRecorder.org_attributes(@org)
      actor = Analytics::MetricsRecorder.user_attributes(topic_obj[:profile]&.user)
      user_profile = Analytics::MetricsRecorder.user_profile_attributes(topic_obj[:profile])
      added_topic = Analytics::UserProfileRecorder.get_topic_attributes(topic_obj[:topic])
      timestamp = topic_obj[:updated_at]&.to_i
      key = topic_obj[:topic_type]
      metadata = {}

      Analytics::UserProfileRecorder.push_added_topic(
        org, actor, user_profile, key, added_topic, timestamp, metadata
      )
    end

    def migrate_user_profiles_topics_for_org
      user_profiles = UserProfile.joins(:user).includes(:user)
        .where(users: {organization_id: @org.id})
        .select(:user_id, :expert_topics, :learning_topics)
        .index_by(&:user_id)

      puts "-----FOUND #{user_profiles.count} user profiles"
      puts "-----FETCHING INFLUX DATA"

      query = InfluxQuery.new("users")
        .select!("user_id, topic_id, event")
        .filter!(:org_id, "org_id", "=", @org.id.to_s)
        .add_time_filters!(start_date: @org.created_at.to_i, end_date: Time.now.to_i)
        .add_conditions!(
          [
            {key: 'event', value: "'user_interest_topic_added'", operator: '=' },
            {key: 'event', value: "'user_expertise_topic_added'", operator: '='}
          ], operator: 'OR', wrap_clause: true
        )

      puts "PLAIN QUERY: #{query.to_plain_query}"
      @influx_topic_events = query.resolve
        .first['values']
        .index_by {|event| [event['user_id'], event['topic_id'], event['event']]}

      user_profiles.each do |user_id, profile|
        process_topics(profile: profile, topic_type: 'expert_topic', event_type: 'user_expertise_topic_added')
        process_topics(profile: profile, topic_type: 'learning_topic', event_type: 'user_interest_topic_added')
      end

      puts "TOTAL TOPICS FOUND #{@migrate_topics.count}"
      @migrate_topics.each do |topic_obj|
        migrate_topic(topic_obj)
      end
    end
  end
end
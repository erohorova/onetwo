module Analytics
  class MigrateAddToPathwayEvent
    def initialize(org_id:)
      @org = Organization.find org_id
      unless @org
        puts "No Org found"
        return
      end

      @card_pack_relations = {}
      @add_to_pathway_events = {}
      @migrate_card_pack_relations = []
    end

    def run
      @card_pack_relations = get_card_pack_relations
      @add_to_pathway_events = get_add_to_pathway_events
      process_card_pack_relations
    end

    def get_card_pack_relations
      cpr_in_batches = []
      CardPackRelation.joins(:card)
        .includes(:card, cover: :author)
        .where(
          cards: { organization_id: @org.id }
        )
        .where(deleted: false)
        .find_in_batches(batch_size: 1000)
        .flatten!
        .index_by { |cpr| [cpr.cover_id&.to_s, cpr.from_id&.to_s] }
    end

    def get_add_to_pathway_events
      query = InfluxQuery.new("cards")
        .select!("card_id, pathway_id, user_id")
        .filter!(:org_id, "org_id", "=", @org.id.to_s)
        .filter!(:event, "event", "=", 'card_added_to_pathway')
        .add_time_filters!(start_date: @org.created_at.to_i, end_date: Time.now.to_i)

      puts "PLAIN QUERY: #{query.to_plain_query}"
      query.resolve
        .first['values']
        .index_by { |event| [event['pathway_id'], event['card_id']] }
    end

    def process_card_pack_relations
      @card_pack_relations.each do |key, val|
        unless @add_to_pathway_events.has_key?(key)
          @migrate_card_pack_relations << val
        end
      end
      migrate_card_added_to_pathway
    end

    def get_migration_cprs
      @migrate_card_pack_relations
    end

    def migrate_card_added_to_pathway
      puts "TOTAL CPR FOUND #{@migrate_card_pack_relations.count}"
      additional_data = {}

      @migrate_card_pack_relations.each do |cpr|
        next unless cpr.cover&.author
        next unless cpr&.card

        begin
          puts "Performing pathway #{cpr.cover_id} and card #{cpr.from_id}"
            Analytics::MetricsRecorderJob.perform_later(
              recorder: "Analytics::CardMetricsRecorder",
              org: Analytics::MetricsRecorder.org_attributes(@org),
              card: Analytics::MetricsRecorder.card_attributes(cpr&.card),
              event: 'card_added_to_pathway',
              actor: Analytics::MetricsRecorder.user_attributes(cpr.cover&.author),
              timestamp: cpr&.created_at.to_i,
              additional_data: additional_data.merge!(pathway_id: cpr.cover&.id)
            )
          rescue => e
            puts "Exception occured: #{e.message}"
            next
          end
      end
    end
  end
end
# frozen_string_literal: true
module Analytics
  class GroupScoreAggregationRecorder < Analytics::MetricsRecorder

    # We set a lower time bounds for all queries,
    # since it makes influx faster. This specific value is
    # January 1, 2016, a few months before the first group_user_added event.
    MinTime = 1451606400

    # The group aggregations are pushed to this measurement on Influx
    Measurement = "group_scores_daily"

    # The leaderboard info is pushed to this measurement on Influx
    LeaderboardMeasurement = "group_user_scores_daily"

    # The data is cached in Mongo at this cache key.
    LeaderboardMongoKey = "group_user_scores_daily"

    # The amount of days to look in the past for user_scores_daily records.
    SearchPeriod = "2d"

    class << self

      def ref
        @ref ||= MONGODB_CLIENT[LeaderboardMongoKey]
      end

      # This is the method fired by the background job.
      # It pushes data to influx and caches the leaderboard data.
      # Returns the number of records created.
      def run!
        group_user_scores_daily, group_score_aggs = fetch_data
        insertion_records = build_insertion_records(group_score_aggs)
        if insertion_records.empty?
          log.warn "no user_scores_daily found for last #{self::SearchPeriod}"
        else
          save_leaderboard!(group_user_scores_daily)
          influxdb_bulk_record(self::Measurement, insertion_records)
        end
        insertion_records.length
      end

      # Returns an array of two objects.
      # First obj is 'group_user_scores_daily', a list of all scores per group
      #   { <time> => { <group_id> => <group_score> } }
      # Second obj is 'group_score_aggs', the total score for the group.
      def fetch_data
        # Get a map of { <user_id> => [<group_id>...] }
        users_groups = get_user_group_mappings

        # Get a map of { <time> => [<user_score>...] }
        user_scores = get_user_scores_aggregations

        # first, map the scores to their groups
        group_user_scores_daily = build_group_user_scores_daily(users_groups, user_scores)

        # Combine the maps into { <time> => { <group_id> => <group_score> } }
        # This sums the scores into a single per-group/day aggregation
        group_score_aggs = build_scores_by_group(group_user_scores_daily)
        # Return both sets of data
        [group_user_scores_daily, group_score_aggs]
      end

      # Writes the group_user_scores_daily.
      # This can either be stored in influx or Mongo.
      # Currently we're disabling Mongo storage and testing Influx
      # is able to handle this load.
      def save_leaderboard!(group_user_scores_daily)
        # write_leaderboard_to_mongo_cache!(group_user_scores_daily)
        write_leaderboard_to_influx!(group_user_scores_daily)
        nil
      end

      # NOTE: THIS METHOD IS NOT CURRENTLY USED
      def write_leaderboard_to_mongo_cache!(group_user_scores_daily)
        # Convert the insertion records to the format expected by mongo
        # ( no tag/field nested hashes are needed )
        records = build_insertion_records(group_user_scores_daily).clone.map do |record|
          record[:tags].
            merge(record[:values]).
            merge("time" => record[:timestamp])
        end
        write_in_chunks(records, &ref.method(:insert_many))
        nil
      end

      def write_leaderboard_to_influx!(group_user_scores_daily)
        insertion_records = build_insertion_records(group_user_scores_daily, user_id: true)
        write_in_chunks insertion_records do |records|
          influxdb_bulk_record(self::LeaderboardMeasurement, records)
        end
        nil
      end

      # Flattens out a nested hash to produce an array of insertion records.
      # This can handle both aggregation records (a single record per group)
      # or leaderboard (an array of records per group).
      # If given the param user_id=true, will include user_id as a tag in the result.
      def build_insertion_records(group_aggs, user_id: false)
        # cache the timestamps to avoid unnecessary Time.parse
        timestamps = {}
        # denormalize the data into a list of hashes that can be sent to influx
        group_aggs.each_with_object([]) do |(time_str, groups_hash), results|
          timestamps[time_str] ||= Time.parse(time_str).to_i
          groups_hash.each do |group_id, record_or_records|
            records = if record_or_records.is_a?(Array) then record_or_records
            else [record_or_records]
            end
            records.each do |record|
              record = record.with_indifferent_access
              results.push({
                timestamp: timestamps[time_str],
                values: record.except("org_id", "user_id"),
                tags: {
                  "group_id" => group_id,
                  "org_id" => record["org_id"]
                }.merge(user_id ? { "user_id" => record["user_id"] } : {})
              })
            end
          end
        end
      end

      # Combines group-level score records to get the total values
      # Return value is a nested hash:
      #   { <time> => { <group_id> => <group_score> } }
      def build_scores_by_group(group_user_scores_daily)
        # get a list of the user_scores_daily keys
        keys = Analytics::UserScoreContinuousQueries.query_names
        # The scores are already keyed by time, we just transform the values
        result = group_user_scores_daily.transform_values do |groups_hash|
          # We change the values assigned to group ids in the hashes
          groups_hash.transform_values do |records|
            # The value is set to a new hash with the sum of the desired keys set.
            keys.each_with_object({}) do |col_name, memo|
              memo[col_name] = records.sum { |record| record[col_name] || 0 }
            end.merge("org_id" => records[0]["org_id"])
          end
        end
        result
      end

      # Combines user scores by group.
      # Returns a nested hash:
      #   { <time> => { <group_id> => [<user_score>...] } }
      # The sum of these scores is computed elsewhere to get the 'group score'.
      def build_group_user_scores_daily(users_groups, user_scores)
        # user scores are already grouped by time,
        # so we just want to change the top-level values
        user_scores.transform_values do |records|
          # We index the user scores records by groups
          records.each_with_object({}) do |user_score, memo|
            group_ids = users_groups[user_score["user_id"]]
            group_ids&.each do |group_id|
              group_id = group_id.to_s
              memo[group_id] ||= []
              memo[group_id].push user_score
            end
          end
        end
      end

      # fetches user_scores_daily records.
      # Returns:
      #  { <time> => [<user_score>...] }
      def get_user_scores_aggregations
        send_query(user_scores_aggregations_query).
          shift&.fetch("values")&.
          group_by { |record| record&.fetch("time") } || {}
      end

      # This calculates the last day's worth of user_scores_daily data
      def user_scores_aggregations_query
        <<-INFLUX
          SELECT
            *
          FROM
            user_scores_daily
          WHERE
            time > now() - #{self::SearchPeriod}
        INFLUX
      end

      # Queries influx for user => group associations.
      # Reads group_user_added events on "groups" measurement
      # Returns:
      #  { <user_id> => [<group_id>...] }
      def get_user_group_mappings
        mappings = {}
        send_query(user_group_mappings_query).
          each_with_object({}) do |record_group, memo|
            user_id = record_group["tags"]["_user_id"]
            group_ids = record_group["values"].map { |val| val["group_id"] }
            memo[user_id] = group_ids
          end
      end

      # Queries the distincnt groups per user
      def user_group_mappings_query
        <<-INFLUX
          SELECT
            DISTINCT(group_id) AS group_id
          FROM
            "groups"
          WHERE
            event='group_user_added'
          AND
            time > #{self::MinTime}
          GROUP BY
            _user_id
        INFLUX
      end

    end

  end
end

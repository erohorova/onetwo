# frozen_string_literal: true

require_relative './user_data_deletion/influx_strategy.rb'

module Analytics
  class UserDataDeletion

    Strategies = {
      influxdb: InfluxStrategy
    }

    # Runs the user data deletion for the given strategy(s)
    # Defaults to all strategies
    def self.run!(user, strategies: Strategies.keys)
      strategies.each_with_object({}) do |name, memo|
        memo[name] = Strategies.fetch(name).run!(user)
      end
    end

  end
end
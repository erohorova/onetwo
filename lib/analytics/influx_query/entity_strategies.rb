class InfluxQuery
  module EntityStrategies

    Entities = Set.new %w{
      cards
      users
      groups
      channels
      searches
      organizations
      user_scores
      user_scores_daily
      org_scores_daily
      group_scores_daily
      group_user_scores_daily
      content_engagement_metrics
      channel_aggregation_daily
      xapi_credentials
      integrations
    }

    def prepare_searches_query!(user_id: nil, **_opts)
      filter!(:user_id, "_user_id", "=", user_id)
    end

    def prepare_content_engagement_metrics_query!(**_opts)
      self
    end

    def prepare_cards_query!(
      user_id: nil, card_id: nil, card_author_id: nil, **_opts
    )
      filter!(:user_id, "_user_id", "=", user_id)
      filter!(:card_id, "_card_id", "=", card_id)
      filter!(:card_author_id, "card_author_id", "=", card_author_id)
    end

    def prepare_users_query!(user_id: nil, **_opts)
      filter!(:user_id, "_user_id", "=", user_id)
    end

    def prepare_groups_query!(group_id: nil, **_opts)
      filter!(:group_id, "_group_id", "=", group_id)
    end

    def prepare_channels_query!(channel_id: nil, **_opts)
      filter!(:channel_id, "_channel_id", "=", channel_id)
    end

    def prepare_user_scores_query!(user_id: nil, role: nil, **_opts)
      filter!(:user_id, "user_id", "=", user_id)
      filter!(:role, "role", "=", role)
    end

    def prepare_user_scores_daily_query!(user_id: nil, **_opts)
      filter!(:user_id, "user_id", "=", user_id)
    end

    def prepare_organizations_query!(user_id: nil, **_opts)
      filter!(:user_id, "_user_id", "=", user_id)
    end

    def prepare_org_scores_daily_query!(**_opts)
      # Intentionally empty, nothing needs to happen here
      # since the WHERE org_id= clause is added by #add_default_filters!
      self
    end

    def prepare_group_scores_daily_query!(group_id: nil, **_opts)
      filter!(:group_id, "group_id", "=", group_id)
    end

    def prepare_group_user_scores_daily_query!(group_ids: [], user_id: nil, **_opts)
      group_ids = [*group_ids] # Force it to an array.
      filter!(:user_id, "user_id", "=", user_id)
      add_where_in_filter!("group_id", "group_id", group_ids)
    end

    def prepare_channel_aggregation_daily_query!(channel_id: nil, **_opts)
      filter!(:channel_id, "channel_id", "=", channel_id)
    end

    def prepare_xapi_credentials_query!(**_opts)
      # Intentionally empty, nothing needs to happen here
      # since the WHERE org_id= clause is added by #add_default_filters!
      self
    end

    def prepare_integrations_query!(**_opts)
      # Intentionally empty, nothing needs to happen here
      # since the WHERE org_id= clause is added by #add_default_filters!
      self
    end

  end
end
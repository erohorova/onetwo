# frozen_string_literal: true

module Analytics

  class UserProfileVisitMetricsRecorder < UserMetricsRecorder
    USER_PROFILE_EVENTS = ['user_profile_visited']

    USER_PROFILE_EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    def self.record(org:, timestamp:, actor:, visited_profile_user:, additional_data:)
      visited_profile_user = RecursiveOpenStruct.new(visited_profile_user)
      super(
        org: org,
        timestamp: timestamp,
        actor: actor,
        event: "user_profile_visited",
        additional_data: additional_data
      ) do |data|
        data[:values].merge!(visited_profile_user_id: visited_profile_user.id.to_s)
      end
    end

  end

end
# frozen_string_literal: true

class Analytics::UserDataExport
  class InfluxStrategy

    # Returns plain data from db in form:
    # { <table_name> => [<record>...] }
    def self.get_data(user)
      queries(user).each_with_object({}) do |(measurement, query), memo|
        results = query.resolve_with_loop(interval: 30.days.to_i)
        memo[measurement] = results
      end
    end

    # A hash mapping query name (e.g. :cards) to InfluxQuery instance
    # This is reused by UserDataDeletion::InfluxStrategy,
    # which will DELETE instead of SELECT the result of these queries,
    # so be careful when changing them.
    def self.queries(user)
      user_id = user.id.to_s
      InfluxUserPointers.each_with_object({}) do |(measurement, tags), memo|
        conditions = tags.map do |tag|
          { key: tag, operator: "=", value: "'#{user_id}'" }
        end
        start_date = user.created_at
        query = InfluxQuery.new(measurement).
          add_version_filter!.
          add_time_filters!(start_date: start_date.to_time.to_i).
          add_conditions!(conditions, operator: "OR", wrap_clause: true).
          order!("ASC")
        memo[measurement] = query
      end
    end

    # These are user id pointers for influx data
    InfluxUserPointers = {
      cards: %w{
        _user_id
        _assigned_to_user_id
        card_author_id
      },
      channels: %w{
        _user_id
        _collaborator_id
        _curator_id
        _follower_id
      },
      groups: %w{
        _group_user_id
      },
      users: %w{
        _user_id
        _visited_profile_user_id
        followed_user_id
        _follower_id
        _member_user_id
        _suspended_user_id
      },
      searches: %w{
        _user_id
      },
      organizations: %w{
        _user_id
      },
    }

  end
end

# frozen_string_literal: true

module Analytics

  class ChannelEclSourceMetricsRecorder < ChannelMetricsRecorder
    CHANNEL_ECL_SOURCE_EVENTS = [
      'channel_ecl_source_added',
      'channel_ecl_source_removed'
    ]

    CHANNEL_ECL_SOURCE_EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    def self.record(ecl_source_id:, channel:, actor:, additional_data: {}, timestamp:, event:)
      super(
        channel: channel,
        event: event,
        actor: actor,
        timestamp: timestamp.to_i,
        additional_data: additional_data
      ) do |data|
        data[:values][:ecl_source_id] = ecl_source_id.to_s
      end
    end

  end

end

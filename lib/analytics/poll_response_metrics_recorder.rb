# frozen_string_literal: true
module Analytics
  class PollResponseMetricsRecorder < CardMetricsRecorder

    def self.record(org:, timestamp:, actor:, card:, quiz_question_options:, additional_data:)
      quiz_question_options.each do |option|
        option = RecursiveOpenStruct.new option
        event = if option.is_correct.nil?
          # nil value means it's a poll
          "card_poll_response_created"
        else
          # true or false value means it's a quiz
          "card_quiz_response_created"
        end
        super(
          org: org,
          timestamp: timestamp,
          actor: actor,
          card: card,
          event: event,
          additional_data: additional_data
        ) do |data|
          case event
          when "card_poll_response_created"
            data[:values].merge! poll_option_label: option.label
            data[:values].merge! poll_option_id: option.id.to_s
          when "card_quiz_response_created"
            data[:values].merge! quiz_option_label: option.label
            data[:values].merge! quiz_option_id: option.id.to_s
            data[:values].merge! is_correct: option.is_correct.to_s
          end
        end
      end
    end

  end
end

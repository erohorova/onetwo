# frozen_string_literal: true
module Analytics
  class MetricsRecorder
    attr_accessor :event_recorder_mapping
    extend Serializers

    class << self

      # Influx query results use a different format than the one used to insert records.
      # This method builds an insertion record from a response record.
      # This is used to backfill user scores.
      #
      # Can optionally provide an array of tag names, if you don't want to
      # look them up from Influx.
      def response_to_insertion(measurement, record, tag_keys: nil)
        # The response from influx doesn't group tags and values.
        # The tags need to be looked up, and manually separated.
        tags = if tag_keys
          tag_keys.map(&:to_s).index_by(&:itself).transform_values { true }
        else
          @tags ||= {}
          @tags[measurement] ||= get_tags(measurement)
        end
        parsed_data = {}
        parsed_data["tags"] = {}
        parsed_data["values"] = {}
        # rename the "time" key to "timestamp"
        time = Time.parse(record.fetch("time"))
        timestamp = time.to_i.to_s.rjust(10, '0') + time.nsec.to_s.rjust(9, '0')
        parsed_data["timestamp"] = timestamp
        record.delete "time"
        record.each_key do |key|
          if tags[key]
            parsed_data["tags"][key] = record[key]
          else
            parsed_data["values"][key] = record[key]
          end
        end
        parsed_data
      end

      # Turns multi-spaces to a single space and removes newlines
      def format_query(query)
        query.strip.chomp.gsub(/\s{2,}/, ' ').gsub("\n", '')
      end

      # Formats a query and sends it to influx.
      # If using this method with hand-build Influx query strings, make sure
      # to not introduce an injection risk. See the influxdb-ruby gem's readme
      # on parameterized query.
      def send_query(query, params={})
        # puts query
        INFLUXDB_CLIENT.query format_query(query), params
      end

      # queries influx for the tags keys on a measurement.
      # formats the response to a hash mapping key to `true`
      def get_tags(measurement)
        measurement = "\"#{measurement}\"" if measurement.in? %w{users groups}
        INFLUXDB_CLIENT
        .query("SHOW TAG KEYS FROM #{measurement}")
        .shift
        .fetch("values")
        .map { |val| val.fetch("tagKey") }
        .index_by(&:itself)
        .transform_values { true }
      end

      # queries influx for the field keys on a measurement.
      # formats the response to a hash mapping key to `true`
      def get_fields(measurement)
        measurement = "\"#{measurement}\"" if measurement.in? %w{users groups}
        INFLUXDB_CLIENT
        .query("SHOW FIELD KEYS FROM #{measurement}")
        .shift
        .fetch("values")
        .map { |val| val.fetch("fieldKey") }
        .index_by(&:itself)
        .transform_values { true }
      end

      # These classes cannot be included in arguments passed to Sidekiq jobs.
      def unserializable_time_classes
        [Date, Time, DateTime, ActiveSupport::TimeWithZone]
      end

      # Record a record to influx. Centralized method used used by recorders.
      # Can optionally be passed a custom Influx client, but uses the default one otherwise.
      def influxdb_record(measurement, data, write_client: nil, duplicate_check: true)
        response = InfluxdbRecorder.record(
          measurement,
          compact_data(data),
          {
            write_client:   write_client,
            duplicate_check: duplicate_check
          }
        )

        if response.try(:code) == "204" # Success status code
          log.info(
            "Writing data to influxdb. " +
            "measurement = #{measurement}, data = #{data}"
          )
          push_auxiliary_metrics(measurement, data)
        elsif response.try(:message) != "duplicate"
          log.error(
            "Received non-successful status code writing to Influx. " +
            "code = #{response.try(:code)}, measurement = #{measurement}, " +
            "data = #{data}"
          )
        end
        response
      end

      # Records many records to influx
      # We don't push auxiliary metrics in this case.
      # Can optionally be passed a custom Influx client, but uses the default one otherwise.
      def influxdb_bulk_record(measurement, data_points, write_client: nil)
        compacted_points = data_points.map do |record|
          record = record.to_h
          record.symbolize_keys!
          record[:values] = record[:values]&.symbolize_keys if record[:values]
          record[:tags] = record[:tags]&.symbolize_keys if record[:tags]
          compact_data(record)
        end
        # The use of splat (*) and compact is only to prevent many existing
        # tests from needing to be changed.
        InfluxdbRecorder.bulk_record(*[
          measurement,
          compacted_points,
          (write_client ? { write_client: write_client } : nil)
        ].compact)
      end

      # Some data is denormalized into multiple measurements.
      # This method is called whenever a metric is successfully recorded,
      # and triggers other metric recordings.
      # It is not called for bulk recordings.
      def push_auxiliary_metrics(measurement, data)
        return if measurement == "user_scores" # avoid infinite loop
        data_struct = RecursiveOpenStruct.new(data)
        # puts "aux metric: #{measurement}/#{data_struct[:tags][:event]}"
        is_admin_request = data_struct.dig(:values, :is_admin_request) == 1
        unless is_admin_request
          push_user_score_metric(measurement, data_struct)
        end
      end

      # If the data has a [:tags][:event] key that is present in
      # config/analytics/event_scores.yml, a 'user score' metric is pushed.
      def push_user_score_metric(measurement, data_struct)
        recorder = Analytics::UserScoreRecorder
        event = data_struct.dig :tags, :event
        score = recorder.scores_table[event]
        return unless score
        actor_id, owner_id = recorder.find_actor_and_owner_ids(
          event, data_struct
        )
        return unless [actor_id, owner_id].any?
        recorder.recording_targets(
          event: event,
          actor_id: actor_id,
          owner_id: owner_id
        ).each do |target|
          recorder.record(target.merge(
            event: event,
            timestamp: data_struct.fetch(:timestamp),
            org_id: data_struct.dig(:tags, :org_id)
          ))
        end
      end

      # Calls the writer_blk on chunks of records in sequence
      def write_in_chunks(records, &writer_blk)
        group_size = [300, records.length].min
        return if records.length.zero?
        records.in_groups_of(group_size, false).each do |records_group|
          writer_blk.call(records_group.compact)
        end
        nil
      end

      # resursively goes through hash, changing all Time objects to integers.
      # Time objects cannot be serialized through Sidekiq.
      def with_timestamps(hash)
        hash.each_with_object({}) do |(key, val), memo|
          memo[key] = if val.is_a?(Hash)
            with_timestamps(val)
          else
            unserializable_time_classes.include?(val.class) ? val.to_time.to_i : val
          end
        end
      end

      # Converts times to integers in an ActiveRecord::Dirty#changes object
      # Also removes updated_at, which is redundant
      def sanitize_times_in_changes(changes)
        changes.reject do |key, _val|
          key.to_s.in? %w{
            updated_at
            comments_count
            users_count
          }
        end.transform_values do |val_pair|
          if val_pair.any? { |val| val.class.in? unserializable_time_classes }
            val_pair.map { |val| val ? val.to_i : val }
          else
            val_pair
          end
        end
      end

      def get_recorder_for_event(event_name)
        @@event_recorder_mapping[event_name]
      end

      def load_event_recorder_mapping
        @@event_recorder_mapping
      end

      def register_event_and_recorder(event_name, klass)
        @@event_recorder_mapping ||= {}
        @@event_recorder_mapping[event_name] = klass
      end

      def compact_data(data)
        data[:values] = data.fetch(:values).reject { |k,v| v.blank? }
        data[:tags] = data.fetch(:tags).reject { |k,v| v.blank? }
        data.fetch(:values).compact!
        data.fetch(:tags).compact!
        data.compact
      end

      def merge_additional_data(data, additional_data)
        additional_data ||= {}
        additional_data = additional_data.with_indifferent_access
        platform = additional_data[:platform]
        is_system_generated = platform.blank? ? "1" : "0"
        data[:values][:platform] = platform
        data[:tags][:is_system_generated] = is_system_generated
        data[:values][:platform_version_number] = additional_data[:platform_version_number]
        data[:values][:user_agent] = additional_data[:user_agent]
        data[:values][:is_admin_request] = additional_data[:is_admin_request].to_s
      end

    end
  end
end

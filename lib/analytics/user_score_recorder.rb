# frozen_string_literal: true
module Analytics
  class UserScoreRecorder < Analytics::MetricsRecorder

    # User scores get pushed to this measurement
    MEASUREMENT = "user_scores"

    # SCORES_TABLE is a hash with string keys.
    # It is populated from the config/analytics/event_scores.yml file.
    SCORES_TABLE = YAML.load File.read File.join(
      Rails.root, "config", "analytics", "event_scores.yml"
    )

    # put this in a method for easier stubbing from tests
    def self.scores_table
      SCORES_TABLE
    end

    # @param event string
    # @param data_struct a hash such as that returned by
    #    MetricsRecorder.response_to_insertion
    #
    # Looks up user_id/owner_id refs from the scores table and matches
    # them to values in data_struct
    #
    def self.find_actor_and_owner_ids(event, data_struct)
      actor_keys, owner_keys = scores_table
        .fetch(event)
        .fetch("refs")
        .values_at("actor_id", "owner_id")
      actor_id = data_struct.dig(*actor_keys) if actor_keys.any?
      owner_id = data_struct.dig(*owner_keys) if owner_keys.any?
      [actor_id, owner_id]
    end

    # Returns array of hashes with keys:
    #   user_id, actor_id, owner_id, role, score_value
    # At least one of ids passed as arguments must be non-nil
    # These hashes are used to invoke UserScoreRecorder.recorder
    # in MetricsRecorder.push_user_score_metric
    def self.recording_targets(event:, actor_id:, owner_id:)
      raise ArgumentError unless [actor_id, owner_id].any?
      base_attrs = {
        actor_id: actor_id,
        owner_id: owner_id
      }
      targets = []
      # If the actor and owner are the same, only one target is returned
      if actor_id == owner_id
        target = base_attrs.merge(
          user_id: actor_id,
          role: "actor_and_owner",
          score_value: determine_score(event: event, role: "actor_and_owner")
        )
        targets.push target
      else
        # If the actor and owner are distinct, up to 2 targets are returned
        if actor_id
          target = base_attrs.merge(
            user_id: actor_id,
            role: "actor",
            score_value: determine_score(event: event, role: "actor")
          )
          targets.push target
        end
        if owner_id
          target = base_attrs.merge(
            user_id: owner_id,
            role: "owner",
            score_value: determine_score(event: event, role: "owner")
          )
          targets.push target
        end
      end
      targets
    end

    # Finds the score given an event and role
    def self.determine_score(event:, role:)
      scores_table.fetch(event).fetch("scores").fetch(role)
    end

    def self.record(
      user_id:, actor_id:, owner_id:, org_id:, event:, timestamp:,
      score_value:, role:
    )
      data = {
        timestamp: timestamp,
        values: {
          score_value: score_value,
          actor_id: actor_id.to_s,
          owner_id: owner_id.to_s,
        },
        tags: {
          event: event,
          role: role,
          user_id: user_id.to_s,
          org_id: org_id.to_s
        }
      }

      # =========================================
      # NOTE: uncomment this next line if inheriting from this class,
      # and want to modify the data sent through a block
      # =========================================
      # yield(data) if block_given?

      influxdb_record(MEASUREMENT, data)
    end

  end
end
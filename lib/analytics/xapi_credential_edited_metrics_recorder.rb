# frozen_string_literal: true
module Analytics
  class XapiCredentialEditedMetricsRecorder < Analytics::XapiCredentialMetricsRecorder
    VALID_EVENTS = %w{
      xapi_credential_edited
    }.index_by(&:itself).transform_values { true }.freeze

    VALID_EVENTS.keys.each do |event_name|
      register_event_and_recorder(event_name, self)
    end

    class << self

      def record(
        org:, xapi_credential:, changed_column:, old_val:, new_val:, timestamp:,
        event:, actor:, additional_data:
      )
        return if is_ignored_event?(changed_column)
        super(
          org: org,
          xapi_credential: xapi_credential,
          timestamp: timestamp,
          event: event,
          actor: actor,
          additional_data: additional_data
        ) do |data|
          data[:values][:changed_column] = changed_column
          data[:values][:old_val] = old_val&.to_s
          data[:values][:new_val] = new_val&.to_s
        end
      end

      def sensitive_info_keys
        %w{
          lrs_password_key
        }
      end

      def is_ignored_event?(column)
        return true unless column
        %w{
          end_point
          lrs_login_key
          lrs_password_key
          lrs_api_version
        }.exclude?(column)
      end

    end

  end
end

# frozen_string_literal: true

module Analytics

  # =========================================
  # NOTE: THIS CLASS IS NOT BEING USED.
  # We are using CardMetricsAggregationRecorder instead,
  # which persists to MySQL.
  # =========================================

  class CardMetricsAggregationContinuousQueries < ContinuousQueriesBase

    # the CQ takes user_scores, creates daily aggregations, and pushes here:
    MEASUREMENT = "content_level_metrics_daily"

    # The length of time to aggregate data by.
    TIME_LENGTH = "1d"

    # the interval on which the CQ runs
    RESAMPLE_EVERY = "1d"

    # how much data it sees at once.
    RESAMPLE_FOR = "2d"

    COLUMN_NAMES = {
      likes:       "num_likes",
      views:       "num_views",
      completions: "num_completions",
      bookmarks:   "num_bookmarks",
      comments:    "num_comments",
      assignments: "num_assignments",
    }

    def self.build_backfill_query
      subqueries_str = build_subqueries_str
      InfluxQuery
        .new(subqueries_str, has_subquery: true)
        .select!("#{select_cols} INTO #{MEASUREMENT}")
        .group_by!("_card_id,time(#{TIME_LENGTH}),org_id")
        .to_plain_query
    end

    def self.select_cols
      COLUMN_NAMES.values.map do |col|
        "MEAN(#{col}) AS #{col}"
      end.join(",")
    end

    def self.build_subqueries_str
      COLUMN_NAMES.keys.map do |column_name|
        send(:"#{column_name}_count_subquery")
      end.map do |query|
        query.to_plain_query(as_subquery: true)
      end.join(",")
    end

    def self.comments_count_subquery
      generic_cards_subquery(
        event_name: "card_comment_created",
        result_name: COLUMN_NAMES[:comments]
      )
    end

    def self.likes_count_subquery
      generic_cards_subquery(
        event_name: "card_liked",
        result_name: COLUMN_NAMES[:likes]
      )
    end

    def self.views_count_subquery
      generic_cards_subquery(
        event_name: "card_viewed",
        result_name: COLUMN_NAMES[:views]
      )
    end

    def self.completions_count_subquery
      generic_cards_subquery(
        event_name: "card_marked_as_complete",
        result_name: COLUMN_NAMES[:completions]
      )
    end

    def self.bookmarks_count_subquery
      generic_cards_subquery(
        event_name: "card_bookmarked",
        result_name: COLUMN_NAMES[:bookmarks]
      )
    end

    def self.assignments_count_subquery
      generic_cards_subquery(
        event_name: "card_assigned",
        result_name: COLUMN_NAMES[:assignments]
      )
    end

    def self.generic_cards_subquery(event_name:, result_name:)
      InfluxQuery
        .new("cards")
        .filter!(:event, "event", "=", event_name)
        .select!("COUNT(card_id) AS #{result_name}")
        .group_by!("_card_id,time(#{TIME_LENGTH}),org_id")
    end

  end

end

# frozen_string_literal: true
module Analytics
  class OrgMailerConfigEditedMetricsRecorder < Analytics::OrgEditedMetricsRecorder
    EVENTS = %w{
      org_email_sender_edited
    }
    EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self

      def record(changed_column:, old_val:, new_val:, **opts)
        case changed_column
        when 'is_active'
          changed_column = "is_current_sender"
          old_val = old_val == "true" ? '1' : '0'
          new_val = new_val == "true" ? '1' : '0'
        when 'verification'
          changed_column = "is_sender_verified"
          old_val = old_val.stringify_keys.dig('domain') ? '1' : '0'
          new_val = new_val.stringify_keys.dig('domain') ? '1' : '0'
        when "from_address"
          changed_column = "email_sender_from_address"
        when "from_name"
          changed_column = "email_sender_from_name"
        end

        super(
          changed_column: changed_column,
          old_val: old_val,
          new_val: new_val,
          **opts
        )
      end

      def is_ignored_event?(changed_column)
        @allowed_events ||= Set.new(%w{
          from_address from_name is_active verification
        })

        ! @allowed_events.member?(changed_column.to_s)
      end

    end

  end
end

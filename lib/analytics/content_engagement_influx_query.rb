=begin

Define "Total users" as "All the users invited/added/unsuspended to the org
minus any suspended/deleted users"

With this definition, add:

1 Total Users

2. Total Users who completed Onboarded

3  Total Users who Logged In

4. monthly - where has 1 action in month

5. daily - where has 1 action in day

6. total active - has 1 action all time

=end

module Analytics
  class ContentEngagementInfluxQuery

    # All analytics metrics have the version key.
    AnalyticsVersion = Settings.influxdb.event_tracking_version

    # Metrics are stored to this location on influx.
    Measurement = "org_level_user_engagement_metrics"

    # These are the events that determine whether a user is 'active'.
    ActiveUserEvents = {
      cards: %w{
        card_added_to_channel
        card_approved_for_channel
        card_bookmarked
        card_comment_created
        card_comment_removed
        card_created
        card_deleted
        card_dismissed
        card_edited
        card_liked
        card_marked_as_complete
        card_marked_as_uncomplete
        card_pinned
        card_poll_response_created
        card_quiz_response_creaated
        card_rejected_for_channel
        card_viewed
        card_shared
        card_unbookmarked
        card_unpinned
        card_promoted
        card_unpromoted
        card_marked_as_complete
      },
      channels: %w{
        channel_visited
        channel_visited
        channel_followed
        channel_unfollowed
        channel_promoted
        channel_created
      },
      users: %w{
        user_expertise_topic_added
        user_expertise_topic_removed
        user_interest_topic_added
        user_interest_topic_removed
        user_followed
        user_unfollowed
        user_logged_in
        user_profile_visited
        user_expertise_added
      }
    }

    # NOTE: we run orgs in groups of 50.
    # When testing, running for one group at a time was far slower, but we can't
    # run for all orgs at once because the query gets too large.
    # 50 is a middle ground.
    def self.run(org_ids: default_org_ids)
      org_ids.in_groups_of(50) do |org_ids|
        org_ids = org_ids.compact
        data = fetch_data(org_ids: org_ids)
        filtered_data = filter_data(data)
        aggregated_data = aggregate_data(filtered_data)
        insertion_records = build_insertion_records(aggregated_data)
        insert_records(insertion_records)
      end
    end

    class << self

      def fetch_data(org_ids:)
        total_user_ids = get_total_user_ids(org_ids: org_ids)
        total_onboarded_user_ids = get_total_onboarded_user_ids(org_ids: org_ids)
        total_logged_in_user_ids = get_total_logged_in_user_ids(org_ids: org_ids)
        daily_active_user_ids = get_daily_active_user_ids(org_ids: org_ids)
        monthly_active_user_ids = get_monthly_active_user_ids(org_ids: org_ids)
        {
          total_user_ids: total_user_ids,
          total_onboarded_user_ids: total_onboarded_user_ids,
          total_logged_in_user_ids: total_logged_in_user_ids,
          monthly_active_user_ids: monthly_active_user_ids,
          daily_active_user_ids: daily_active_user_ids,
        }
      end

      def filter_data(data)
        # Filter out any ids from these keys where the id is not
        # present in the 'total users' list.
        %i{
          total_logged_in_user_ids
          monthly_active_user_ids
          daily_active_user_ids
        }.each do |key|
          data[key] = data[key].each_with_object({}) do |(org_id, user_ids), memo|
            memo[org_id] = (user_ids & data[:total_user_ids][org_id]).to_a
          end
        end
        data
      end

      def aggregate_data(data)
        data.each_with_object({}) do |(key, orgs_data), memo|
          orgs_data.each do |org_id, user_ids|
            result_key = key.to_s.gsub("_ids", "s").to_sym
            memo[org_id] ||= {}
            memo[org_id][result_key] = user_ids.length
          end
        end
      end

      def insert_records(insertion_records)
        InfluxdbRecorder.bulk_record(Measurement, insertion_records)
      end

      def build_insertion_records(aggregated_data)
        timestamp = Time.now.utc.beginning_of_day.to_i
        aggregated_data.each_with_object([]) do |(org_id, org_data), memo|
          memo.push(
            timestamp: timestamp,
            tags: {
              org_id: org_id
            },
            values: {
              total_users: org_data[:total_users] || 0,
              total_onboarded_users: org_data[:total_onboarded_users] || 0,
              total_logged_in_users: org_data[:total_logged_in_users] || 0,
              monthly_active_users: org_data[:monthly_active_users] || 0,
              daily_active_users: org_data[:daily_active_users] || 0,
              daily_engagement: get_daily_engagement(org_data),
              monthly_engagement: get_monthly_engagement(org_data),
            }
          )
        end
      end

      def get_daily_engagement(org_data)
        daily_pct = (org_data[:daily_active_users] || 0) / (org_data[:total_onboarded_users] || 1.0).to_f
        (daily_pct * 100).round(2)
      end

      def get_monthly_engagement(org_data)
        monthly_pct = (org_data[:monthly_active_users] || 0) / (org_data[:total_onboarded_users] || 1.0).to_f
        (monthly_pct * 100).round(2)
      end

      def get_total_user_ids(org_ids:, &blk)
        query = User
          .where(organization_id: org_ids, is_suspended: false)
          .select(:id, :organization_id)
        query = blk.call(query) if blk
        query
          .group_by(&:organization_id)
          .stringify_keys
          .transform_values do |user_infos|
            Set.new(user_infos.map(&:id).map(&:to_s))
          end
      end

      def get_total_onboarded_user_ids(org_ids:)
        get_total_user_ids(org_ids: org_ids) do |query|
          query
            .joins(:user_onboarding)
            .where("user_onboardings.status = 'completed'")
        end
      end

      def get_total_logged_in_user_ids(org_ids:)
        org_ids_set = Set.new(org_ids.map(&:to_s))
        query = InfluxQuery.new("users").
          select!("DISTINCT(user_id) AS user_id").
          filter!(:event, "event", "=", "user_logged_in").
          add_version_filter!.
          group_by!("org_id")
        result = normalize_influx_response(query.resolve)
        result.each_with_object({}) do |(org_id, users_info), memo|
          if org_ids_set.member?(org_id)
            user_ids = Set.new(users_info.map { |user| user["user_id"].to_s })
            memo[org_id] = user_ids
          end
        end
      end

      def get_monthly_active_user_ids(org_ids:)
        get_active_user_ids_over_period(
          org_ids: org_ids,
          start_date: (Time.now.utc - 30.days).to_i
        )
      end

      def get_daily_active_user_ids(org_ids:)
        get_active_user_ids_over_period(
          org_ids: org_ids,
          start_date: (Time.now.utc - 1.days).to_i
        )
      end

      def get_active_user_ids_over_period(org_ids:, start_date:, end_date: nil)
        end_date ||= Time.now.utc.end_of_day.to_i
        org_ids_set = Set.new(org_ids.map(&:to_s))
        results = ActiveUserEvents.each_with_object({}) do |(measurement, event_names), memo|
          query = query_measurement_for_active_users(
            measurement: measurement,
            org_ids: org_ids,
            start_date: start_date,
            end_date: end_date,
            event_names: event_names
          )
          result = normalize_influx_response(query.resolve)
          filtered_result = filter_result(
            org_ids_set: org_ids_set,
            result: result
          )
          filtered_result.each do |org_id, users_info|
            memo[org_id] ||= Set.new
            users_info.each do |user_obj|
              memo[org_id].add user_obj["user_id"].to_s
            end
          end
        end
        results
      end

      def filter_result(org_ids_set:, result:)
        result.reject do |org_id, _users_info|
          !org_ids_set.member?(org_id)
        end
      end

      def query_measurement_for_active_users(measurement:, org_ids:, start_date:, end_date:, event_names:)
        InfluxQuery.new(measurement)
          .select!("DISTINCT(user_id) AS user_id")
          .add_version_filter!
          .add_time_filters!(start_date: start_date, end_date: end_date)
          .add_where_in_filter!(:event, "event", event_names)
          .add_where_in_filter!(:org_id, "org_id", org_ids.map(&:to_s))
          .group_by!("org_id")
          .filter!(:is_system_generated, "is_system_generated", "=", "0")
      end

      def format_query(heredoc)
        heredoc.strip_heredoc.chomp.gsub "\n", " "
      end

      def normalize_influx_response(data)
        data.each_with_object({}) do |record, memo|
          memo[record["tags"]["org_id"]] = record["values"]
        end
      end

      def default_org_ids
        Organization.pluck :id
      end

    end

  end
end

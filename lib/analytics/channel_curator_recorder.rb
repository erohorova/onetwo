# frozen_string_literal: true

require 'analytics/channel_metrics_recorder'
module Analytics
  class ChannelCuratorRecorder < ChannelMetricsRecorder
    CHANNEL_CURATOR_EVENTS = ['channel_curator_added', 'channel_curator_removed'].freeze
    CHANNEL_CURATOR_EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self
      def record(channel:, curator:, actor:, event:, timestamp:, additional_data: {})
        curator = RecursiveOpenStruct.new(curator)
        super(
          channel: channel,
          timestamp: timestamp,
          event: event,
          actor: actor,
          additional_data: additional_data
        ) do |data|
          merge_tags(data[:tags], curator)
          merge_fields data[:values], curator
        end
      end

      def merge_tags(tags, curator)
      end

      def merge_fields(fields, curator)
        fields.merge!(
          curator_id: curator.id.to_s,
          ### NOTE: PII
          curator_full_name: curator.full_name
          ##
        )
      end

    end
  end
end

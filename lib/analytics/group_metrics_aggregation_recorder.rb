# frozen_string_literal: true
module Analytics
  class GroupMetricsAggregationRecorder < Analytics::MetricsRecorder

    # THE NUMBER OF EXTRA DAYS TO COVER IN QUERIES
    # (in addition to the previous day)
    # INCREASE THIS NUMBER IF BACKFILLING
    TIME_RANGE = 0.days

    # THE MEASUREMENT STUFF WILL GET PUSHED TO
    MEASUREMENT = "group_aggregation_daily"

    class << self

      def run(orgs: Organization.all)
        # Some commonly-used times are stored in methods and memoized
        # for consistency. This is cleared whenever the recorder is run.
        clear_memoization!
        # We need to run each method only for a single org at a time
        # This is to prevent the memory usage from getting huge
        iterator = orgs.is_a?(Array) ? :each : :find_each
        orgs.send iterator, &method(:run_for_org)
      end

      def run_for_org(org)
        # Fetch data for each org, team, and day in the time range,
        # combining them into a big dictionary.
        merged_data = merge_data(fetch_data([org]))
        # Merge in engagement
        add_user_engagement!(merged_data)
        # flatten out the dictionary and format for sql
        insertion_records = build_insertion_records(merged_data)
        # bulk insert all the records to sql
        push_records(insertion_records)
      end

      # bulk records records to sql
      def push_records(insertion_records)
        # raise # NEED TO CHECK THAT ALL RECORDS FALL WITHIN TIME_RANGE
        insertion_records.in_groups_of(10_000).each do |record_group|
          log.debug("creating #{record_group.length} GroupMetricsAggregation records")
          GroupMetricsAggregation.bulk_insert do |worker|
            record_group.each do |record|
              # in_groups_of can use nil elements as placeholders
              next unless record
              worker.add(record)
            end
          end
        end
        nil # dont spam console with output
      end

      # Merges all data per org/team/timestamp
      # input: result of fetch_data
      # output: { <org_id> => { <team_id> => { <time> => <record> } } }
      def merge_data(data)
        data.values.each_with_object({}) do |org_sections, memo|
          org_sections.each do |(org_id, org_data)|
            org_data.each do |team_id, team_data|
              team_data.each do |timestamp, time_data|
                buried_merge!(memo, org_id, team_id, timestamp, time_data)
              end
            end
          end
        end
      end

      # input: result of merge_data
      # output: [<record>...]
      # flattens the input hash to a list of records, formatted for sql
      def build_insertion_records(merged_data)
        merged_data.each_with_object([]) do |(org_id, org_data), results|
          org_data.each do |team_id, team_data|
            team_data.each do |timestamp, record|
              results.push attrs_for_sql(org_id, team_id, timestamp, record)
            end
          end
        end
      end

      # loads all the data, keyed by type, org, team, and time
      # returns: { <type> => { <org_id> => { <team_id> => { <time> => <record> } } } }
      def fetch_data(orgs)
        # Fetch smartbites_created, smartbites_consumed, and time_spent_minutes
        base_data = fetch_data_with_standard_filters(orgs)
        # Merge in active_users
        base_data.merge!(fetch_active_users(orgs))
        # Merge in new_users
        base_data.merge!(fetch_new_users(orgs))
        # Merge in total users
        base_data.merge!(fetch_total_users(orgs))
        base_data
      end

      # Fetches data per team, per day.
      # As much as possible is requested here, but some data needs to be
      # requested in separate queries.
      def fetch_data_with_standard_filters(orgs)
        query = <<-SQL.strip_heredoc
          SELECT
            SUM(smartbites_created_count) AS smartbites_created,
            SUM(smartbites_commented_count) AS num_comments,
            SUM(smartbites_liked_count) AS num_likes,
            SUM(smartbites_consumed_count) AS smartbites_consumed,
            SUM(user_metrics_aggregations.time_spent_minutes) AS time_spent_minutes,
            teams_users.team_id AS team_id,
            user_metrics_aggregations.start_time,
            user_metrics_aggregations.end_time,
            user_metrics_aggregations.organization_id
          FROM user_metrics_aggregations
          INNER JOIN teams_users ON teams_users.user_id = user_metrics_aggregations.user_id
          WHERE user_metrics_aggregations.organization_id IN (:org_ids)
          AND user_metrics_aggregations.start_time >= :from_date_int
          AND user_metrics_aggregations.end_time <= :to_date_int
          GROUP BY team_id,start_time
        SQL
        params = {
          org_ids:       orgs.map(&:id),
          from_date_int: (start_of_yesterday - TIME_RANGE).to_i,
          to_date_int:   end_of_yesterday.to_i
        }
        records = UserMetricsAggregation.find_by_sql([query, params])
        attr_sets = records.map(&:attributes)
        results = {
          "smartbites_created"  => {},
          "smartbites_consumed" => {},
          "time_spent_minutes"  => {},
          "num_comments"        => {},
          "num_likes"           => {}
        }
        attr_sets.each do |attr_set|
          team_id, org_id, start_time, end_time = attr_set.values_at *%w{
            team_id organization_id start_time end_time
          }
          results.each_key do |key|
            buried_merge!(
              results,
              key, org_id, team_id, start_time,
              { key => attr_set[key] }
            )
          end
        end
        results
      end

      # Requests the number of active users per day, per team
      def fetch_active_users(orgs)
        query = <<-SQL.strip_heredoc
          SELECT
            COUNT(DISTINCT(user_metrics_aggregations.user_id)) AS active_users,
            teams_users.team_id AS team_id,
            user_metrics_aggregations.start_time,
            user_metrics_aggregations.end_time,
            user_metrics_aggregations.organization_id
          FROM user_metrics_aggregations
          INNER JOIN teams_users ON teams_users.user_id = user_metrics_aggregations.user_id
          WHERE user_metrics_aggregations.organization_id IN (:org_ids)
          AND user_metrics_aggregations.start_time >= :from_date_int
          AND user_metrics_aggregations.end_time <= :to_date_int
          AND user_metrics_aggregations.time_spent_minutes > 0
          GROUP BY team_id,start_time
        SQL
        params = {
          org_ids:       orgs.map(&:id),
          from_date_int: (start_of_yesterday - TIME_RANGE).to_i,
          to_date_int:   end_of_yesterday.to_i
        }
        records = UserMetricsAggregation.find_by_sql([query, params])
        attr_sets = records.map(&:attributes)
        results = {
          "active_users" => {}
        }
        attr_sets.each do |attr_set|
          team_id, org_id, start_time, end_time = attr_set.values_at *%w{
            team_id organization_id start_time end_time
          }
          results.each_key do |key|
            buried_merge!(
              results,
              key, org_id, team_id, start_time,
              { key => attr_set[key] }
            )
          end
        end
        results
      end

      # Fetches the number of new users per day, per team.
      def fetch_new_users(orgs)
        from_date = (start_of_yesterday - TIME_RANGE)
        to_date = end_of_yesterday
        from_date_components = {
          year: from_date.year, month: from_date.month, day: from_date.day
        }
        to_date_components = {
          year: to_date.year, month: to_date.month, day: to_date.day
        }
        query = <<-SQL.strip_heredoc
          SELECT
            COUNT(DISTINCT(users.id)) AS new_users,
            YEAR(users.created_at) AS year,
            MONTH(users.created_at) AS month,
            DAY(users.created_at) AS day,
            teams_users.team_id AS team_id,
            users.organization_id
          FROM users
          INNER JOIN teams_users ON teams_users.user_id = users.id
          WHERE users.organization_id IN (:org_ids)
          AND YEAR(users.created_at)  >= :from_date_year
          AND MONTH(users.created_at) >= :from_date_month
          AND DAY(users.created_at)   >= :from_date_day
          AND YEAR(users.created_at)  <= :to_date_year
          AND MONTH(users.created_at) <= :to_date_month
          AND DAY(users.created_at)   <= :to_date_day
          GROUP BY
            team_id,YEAR(users.created_at),MONTH(users.created_at),
            DAY(users.created_at)
        SQL
        params = {
          org_ids:         orgs.map(&:id),
          from_date_year:  from_date_components[:year],
          from_date_month: from_date_components[:month],
          from_date_day:   from_date_components[:day],
          to_date_year:    to_date_components[:year],
          to_date_month:   to_date_components[:month],
          to_date_day:     to_date_components[:day]
        }
        records = UserMetricsAggregation.find_by_sql([query, params])
        attr_sets = records.map(&:attributes)
        results = {
          "new_users" => {}
        }
        attr_sets.each do |attr_set|
          team_id, org_id, year, month, day = attr_set.values_at *%w{
            team_id organization_id year month day
          }
          start_time = Date.new(year, month, day).beginning_of_day.to_i
          results.each_key do |key|
            buried_merge!(
              results,
              key, org_id, team_id, start_time,
              { key => attr_set[key] }
            )
          end
        end
        results
      end

      # Fetches the number of total users per day, per team.
      # We cannot backfill this with a single query. Need to issue a separate
      # query for each day in the backfill range.
      def fetch_total_users(orgs)
        num_days = TIME_RANGE / 60 / 60 / 24
        cutoffs = [
          end_of_yesterday,
          *num_days.times.map { |i| end_of_yesterday - i.days }
        ]
        results = {
          "total_users" => {}
        }
        cutoffs.each do |cutoff|
          records = TeamsUser
            .joins(:team) # need to join team to get organization_id
            .select("COUNT(DISTINCT(user_id)) AS total_users,team_id,organization_id")
            .group("team_id")
            .where("teams_users.created_at <= ?", cutoff)
            .where("teams.organization_id IN (?)", orgs.map(&:id))
            .map(&:attributes)
          records.each do |record|
            org_id, team_id = record.values_at *%w{
              organization_id team_id
            }
            results.each_key do |key|
              buried_merge!(
                results,
                key, org_id, team_id, cutoff.beginning_of_day.to_i,
                { key => record[key] }
              )
            end
          end
        end
        results
      end

      # Manually adds user engagement to each team/day in the results.
      # This is defined as active_users divided by total_users
      def add_user_engagement!(base_data)
        base_data.each do |org_id, org_vals|
          org_vals.each do |team_id, team_vals|
            team_vals.each do |time, record|
              active_users = record["active_users"] || 0
              total_users = record["total_users"] || 0
              engagement = if total_users.zero?
                0.0
              else
                active_users.to_f / total_users.to_f
              end
              record["engagement_index"] = (engagement * 100).to_i
            end
          end
        end
      end

      private

      # Cache the end of yesterday as a time object
      def end_of_yesterday
        @end_of_yesterday ||= (Time.now.utc - 1.day).end_of_day
      end

      # Cache the start of yesterday as a time object
      def start_of_yesterday
        @start_of_yesterday ||= end_of_yesterday.beginning_of_day
      end

      # Nullify memoized variables.
      # Is run at the start of the recorder
      def clear_memoization!
        @end_of_yesterday = nil
        @start_of_yesterday = nil
      end

      # merges a value to a hash using nested keys.
      def buried_merge!(obj, *keys, val_to_merge)
        keys.reduce(obj) do |memo, key|
          memo[key] ||= {}
        end.merge!(val_to_merge)
        nil
      end

      # The final data structure that is recorded to sql
      def attrs_for_sql(org_id, team_id, timestamp, record)
        end_time = Time.at(timestamp).utc.end_of_day.to_i
        {
          start_time:          timestamp,
          end_time:            end_time,
          organization_id:     org_id.to_i,
          team_id:             team_id.to_i,
          smartbites_created:  record["smartbites_created"]  || 0,
          smartbites_consumed: record["smartbites_consumed"] || 0,
          time_spent_minutes:  record["time_spent_minutes"]  || 0,
          total_users:         record["total_users"]         || 0,
          active_users:        record["active_users"]        || 0,
          new_users:           record["new_users"]           || 0,
          engagement_index:    record["engagement_index"]    || 0,
          num_comments:        record["num_comments"]        || 0,
          num_likes:           record["num_likes"]           || 0,
          timestamp:           timestamp,
        }
      end

    end
  end
end

module Analytics
  class LastActivityRecorder

    VALID_EVENTS = %w{
      channel_visited user_profile_visited card_source_visited card_viewed
    }.index_by(&:itself).transform_values { true }.freeze

    def initialize(user_id:)
      @user_id = user_id
    end

    # public methods
    def add_data_to_cache(event:, value:) 
      return unless is_valid_event?(event)
      Rails.cache.write(
        cache_key_for_last_activity, cache_attrs(event, value), 
        expires_in: 30.days
      )
    end

    def get_data_from_cache
      Rails.cache.read(cache_key_for_last_activity) || {}
    end

    private
    def is_valid_event?(event)
      VALID_EVENTS[event]
    end

    def cache_key_for_last_activity
      "user_last_activity_#{@user_id}"      
    end

    def cache_attrs(event, value)
      {
        event: event,
        entity: Analytics::MetricsRecorder.get_recorder_for_event(event)::MEASUREMENT.singularize,
        last_viewed_at: value[:timestamp],
        event_attributes: value.except(*[:timestamp])
      }
    end
  end
end
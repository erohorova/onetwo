# frozen_string_literal: true

module Analytics
  class CardChannelRecorder < CardMetricsRecorder
    CARD_CHANNEL_EVENTS = ['card_added_to_channel', 'card_removed_from_channel'].freeze
    CARD_CHANNEL_EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self

      def record(org:, channel:, card:, actor:, timestamp:, event:, additional_data: {})
        channel = RecursiveOpenStruct.new channel
        super(
          org: org,
          card: card,
          event: event,
          timestamp: timestamp,
          actor: actor,
          additional_data: additional_data
        ) do |data|
          data[:values][:channel_id] = channel.id.to_s
          data[:values][:is_channel_featured] = channel.is_promoted && '1' || '0'
          data[:values][:is_channel_curated] = channel.curate_only && '1' || '0'
          data[:values][:is_channel_public] = !channel.is_private && '1' || '0'
          data[:values][:channel_name] = channel.label
          data[:timestamp] = timestamp.to_i
        end
      end

      private

      def validate_card_event(event)
        raise 'Valid events: add_to_card and remove_from_channel' unless CARD_CHANNEL_EVENTS.include?(event)
      end
    end
  end
end
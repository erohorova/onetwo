# frozen_string_literal: true

class Analytics::GroupMetricsRecorder < Analytics::MetricsRecorder
  MEASUREMENT = 'groups'.freeze
  VALID_EVENTS = %w{
    group_visited
  }.index_by(&:itself).transform_values { true }

  VALID_EVENTS.keys.each do |event_name|
    register_event_and_recorder(event_name, self)
    const_set("EVENT_#{event_name.upcase}", event_name).freeze
  end

  class << self
    def record(org:, actor:, group:, event:, timestamp:, additional_data: {})
      actor, group, org = [
        actor, group, org
      ].map &RecursiveOpenStruct.method(:new)

      data = {}
      data[:values] = extract_fields(org, actor, group)
      data[:tags] = extract_tags(actor, group, event)
      data[:timestamp] = timestamp

      merge_additional_data(data, additional_data)
      yield(data) if block_given?

      influxdb_record(MEASUREMENT, data)
    end

    def extract_fields(org, actor, group)
      field_set = { group_name: group.name }
      field_set[:org_name] = org.name
      field_set[:user_id] = actor.id.to_s
      field_set[:group_id] = group.id.to_s
      field_set[:org_hostname] = org.host_name
      ### NOTE: PII
      field_set[:user_full_name] = actor.try(:full_name)
      field_set[:user_handle] = actor&.handle
      ###
      field_set
    end

    def extract_tags(actor, group, event)
      tag_set = {_user_id: actor.id.to_s, _group_id: group.id.to_s}
      tag_set[:org_id] = group.organization_id.to_s
      tag_set[:event] = event
      tag_set
    end
  end
end

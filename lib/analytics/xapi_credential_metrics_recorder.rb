# frozen_string_literal: true
module Analytics
  class XapiCredentialMetricsRecorder < Analytics::MetricsRecorder
    MEASUREMENT = 'xapi_credentials'.freeze
    VALID_EVENTS = %w{
      xapi_credential_created
      xapi_credential_deleted
    }.index_by(&:itself).transform_values { true }

    VALID_EVENTS.keys.each do |event_name|
      register_event_and_recorder(event_name, self)
    end

    class << self

      def record(org:, xapi_credential:, event:, actor:, timestamp:, additional_data: {})
        xapi_credential, actor, org = [
          xapi_credential, actor, org
        ].map &RecursiveOpenStruct.method(:new)
        if event && !(self::VALID_EVENTS.has_key?(event) || get_recorder_for_event(event))
          raise StandardError, 'Event is not registered'
        end

        data = {}

        data[:values] = extract_fields(org, xapi_credential, actor)
        data[:tags] = extract_tags(org, xapi_credential, event, actor)
        data[:timestamp] = timestamp

        merge_additional_data(data, additional_data)
        yield(data) if block_given?

        influxdb_record(MEASUREMENT, data)
      end

      def extract_fields(org, xapi_credential, actor)
        {
          xapi_credential_id: xapi_credential.id.to_s,
          user_full_name:     actor.try(:full_name),
          user_handle:        actor.try(:handle),
          user_id:            actor.try(:id).to_s,
          org_name:           org.name,
          org_hostname:       org.host_name,
          endpoint:           xapi_credential.end_point,
          lrs_login_key:      xapi_credential.lrs_login_key,
          lrs_api_version:    xapi_credential.lrs_api_version
        }
      end

      def extract_tags(org, xapi_credential, event, actor)
        {
          _user_id: actor.try(:id).to_s,
          org_id:   org.id.to_s,
          event:    event
        }
      end

      private

      def merge_additional_data(data, additional_data)
        super
      end

    end

  end
end

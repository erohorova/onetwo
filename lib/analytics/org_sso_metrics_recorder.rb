# frozen_string_literal: true
module Analytics
  class OrgSsoMetricsRecorder < Analytics::OrgMetricsRecorder
    ORG_SSO_EVENTS = [
      'org_sso_added',
      'org_sso_removed',
      'org_sso_edited'
    ]

    ORG_SSO_EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self

      def record(
        # Mandatory stuff
        sso:, org:, event:, actor:, timestamp:,
        # Optional - edit events only
        changed_column: nil, old_val: nil, new_val: nil,
        # Extra metadata
        additional_data: {}
      )
        sso = RecursiveOpenStruct.new(sso)

        super(
          org: org,
          event: event,
          actor: actor,
          timestamp: timestamp,
          additional_data: additional_data
        ) do |data|
          data[:values].merge!(
            org_sso_name: sso.name
          )

          # push only whitelested attributes
          if is_ignored_event?(changed_column)
            return
          end

          data[:values].merge!(
            changed_column: changed_column,
            old_val: old_val.to_s,
            new_val: new_val.to_s
          )


        end
      end

      def is_ignored_event?(changed_column)
        return false unless changed_column
        whitelisted_change_attrs.exclude?(changed_column)
      end

      def whitelisted_change_attrs
        %w{
          is_enabled
          label_name
        } | saml_attributes
      end

      def saml_attributes
        %w{
          saml_idp_sso_target_url
          saml_issuer
          saml_idp_cert
          saml_assertion_consumer_service_url
        }
      end

      def sensitive_info_keys
        %w{
          saml_idp_cert
        }
      end

    end

  end
end

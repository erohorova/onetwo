# frozen_string_literal: true

require 'analytics/channel_metrics_recorder'

module Analytics
  class ChannelCollaboratorRecorder < ChannelMetricsRecorder
    VALID_CHANNEL_COLLABORATOR_EVENTS = [
      'channel_collaborator_added',
      'channel_collaborator_removed'
    ]

    VALID_CHANNEL_COLLABORATOR_EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    def self.record(user_id:, channel:, actor:, additional_data: {}, timestamp:, event:)
      super(
        channel: channel,
        event: event,
        actor: actor,
        timestamp: timestamp.to_i,
        additional_data: additional_data
      ) do |data|
        data[:values][:collaborator_id] = user_id.to_s
      end
    end

  end

end

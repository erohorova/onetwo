module Analytics
  class OrgBadgingMetricsRecorder < Analytics::OrgMetricsRecorder
    ORG_BADGING_EVENTS = [
      'org_clc_created',
      'org_clc_deleted',
      'org_clc_edited',
      'org_badging_created',
      'org_badging_deleted',
      'org_badging_edited' 
    ].freeze

    ORG_BADGING_EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end
    
    # We only care about updates to these attributes
    SIGNIFICANT_EDIT_ATTRIBUTES = Set.new(%w{
      name
      links
      level
    })

    class << self

      def record(
        # Mandatory stuff
        org:, event:, actor:, timestamp:,
        # Optional 
        clc: nil, badging: nil, 
        changed_column: nil, old_val: nil, new_val: nil,
        # Extra metadata
        additional_data: {}
      )
        validate_badging_event(event)
        clc &&= RecursiveOpenStruct.new(clc)
        badging &&= RecursiveOpenStruct.new(badging)
        super(
          org: org,
          event: event,
          actor: actor,
          timestamp: timestamp,
          additional_data: additional_data
        ) do |data|
          merge_clc_information(data, clc) if clc
          merge_badging_information(data, badging) if badging
        end

    end

    def merge_badging_information(data, badging)
      data[:values].merge!(
        badging_id:                      badging.id.to_s,
        badging_title:                   badging.title,
        badging_badge_id:                badging.badge_id.to_s,
        badging_badgeable_id:            badging.badgeable_id.to_s,
        badging_type:                    badging.type,
        badging_target_steps:            badging.target_steps,
        badging_all_quizzes_answered:    badging.all_quizzes_answered
      )
    end


    def merge_clc_information(data, clc)
      data[:values].merge!(
        clc_id:              clc.id.to_s,
        clc_entity_id:       clc.entity_id.to_s,
        clc_entity_type:     clc.entity_type,
        clc_from_date:       clc.from_date,
        clc_to_date:         clc.to_date,
        clc_target_score:    clc.target_score.to_s,
        clc_target_steps:    clc.target_steps.to_s,
        clc_name:            clc.name
      )
    end

    def is_ignored_event?(changed_column)
      !SIGNIFICANT_EDIT_ATTRIBUTES.member?(changed_column.to_s)
    end

    private

    def validate_badging_event(event)
      unless ORG_BADGING_EVENTS.include?(event)
        raise "Invalid event: #{event}. Valid events: #{ORG_BADGING_EVENTS.join(",")}"
      end
    end
  end

  end
end
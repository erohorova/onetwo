# frozen_string_literal: true
module Analytics
  class OrgPartnerCenterEditedMetricsRecorder < Analytics::OrgEditedMetricsRecorder
    EVENTS = %w{
      org_partner_center_edited
    }
    EVENTS.each do |event_name|
      register_event_and_recorder(event_name, self)
      const_set("EVENT_#{event_name.upcase}", event_name).freeze
    end

    class << self

      def record(
        changed_column:, old_val:,   new_val:,
        org:,            event:,     actor:,
        partner_center:, timestamp:, additional_data: {}
      )
        super(
          org:             org,
          event:           event,
          actor:           actor,
          partner_center:  partner_center,
          timestamp:       timestamp,
          additional_data: additional_data,
          changed_column:  changed_column,
          old_val:         old_val,
          new_val:         new_val
        )
      end

      def is_ignored_event?(changed_column)
        return false unless changed_column
        !changed_column.in? %w{
          enabled
          description
          embed_link
          integration
        }
      end

    end

  end
end

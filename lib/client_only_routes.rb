# -----------------------------------------------------------------------------
# These routes are only used by the client.
# There are no matching server/API routes.
# -----------------------------------------------------------------------------

class ClientOnlyRoutes

  def self.show_insight_path(id:)
    "/insights/#{id}"
  end

  def self.show_insight_url(card)
    show_insight_url_with_org(
      card.organization, id: card.slug
    )
  end

  def self.assignment_url(card)
    if card.card_type == 'pack'
      "#{card.organization.home_page}/pathways/#{card.slug}"
    elsif card.card_type == 'journey'
      "#{card.organization.home_page}/journey/#{card.slug}"
    else
      "#{card.organization.home_page}/insights/#{card.slug}"
    end
  end

  # NOTE: Not made any changes in below method as it is used in many different places.
  # to make it work need to make changes in all the place where this method is used.
  def self.show_insight_url_with_org(organization, id:)
    "#{organization.home_page}/insights/#{id}"
  end

  def self.collection_url(params)
    card = Card.friendly.find(params[:id])
    organization = card.organization
    "#{organization.home_page}/#{collection_path(id: params[:id])}"
  end

  def self.collection_path(id:)
    "/collections/#{id}"
  end

  def self.show_insight_link(insight_id, pars={})
    card_type = pars.delete(:card_type)
    params = pars.merge(id: insight_id)
    return collection_url(params) if card_type.in? %w[pack]
    show_insight_url(Card.find params[:id])
  end

end



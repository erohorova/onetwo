# frozen_string_literal: true
class InfluxdbRecorder

  ANALYTICS_VERSION = Settings.influxdb.event_tracking_version

  # By default, organizations use the legacy system for writing to Influx
  # (writing to Influx directly from background jobs).
  # However, if there's a Config for the org with name='use_influx_service'
  # and value=true, then it will push to the Redis queue instead.
  #
  # NOTE: must restart the tasks server after adding/removing these configs.
  ORGS_USING_INFLUX_SERVICE = Set.new

  org_ids = if ActiveRecord::Base.connection.table_exists?(:configs)
    Config.where(
      configable_type: "Organization",
      name:            "use_influx_service",
      value:           "true"
    ).pluck(:configable_id)
  else
    []
  end

  org_ids.each do |org_id|
    ORGS_USING_INFLUX_SERVICE.add org_id.to_s
  end

  # Code to prevent duplicate records being pushed.
  # See lib/influxdb_recorder/duplicate_checker.rb
  extend DuplicateChecker

  # Record a single data point to influx
  def self.record(measurement, data, write_client: nil, duplicate_check: true)
    # See DuplicateChecker for `is_duplicate?` method
    if duplicate_check && is_duplicate?(measurement, data)
      return OpenStruct.new(message: "duplicate")
    end
    write_client ||= INFLUXDB_CLIENT
    merge_common_tags!(data)
    use_nanosecond_precision!(measurement, [data])
    if use_influx_service?(data)
      write_client.write_point_to_queue(measurement, data)
    else
      write_client.write_point(measurement, data)
    end
  end

  # Bulk-record data points to influx. Much faster if there are many points.
  def self.bulk_record(measurement, data_points, write_client: nil)
    write_client ||= INFLUXDB_CLIENT
    data_points.each do |record|
      merge_common_tags!(record, aggregation: true)
    end
    merge_series_for_bulk_record!(measurement, data_points)
    use_nanosecond_precision!(measurement, data_points)

    # When bulk_writing points, it is possible for multiple orgs' data to
    # be processed at once.
    # Since we configure destination on a per-org basis, we first partition it
    # depending on whether the individual points' orgs are configured to
    # use influx-service or not.
    data_points.
      partition { |point| use_influx_service?(point) }.
      tap do |(with_influx_service, without_influx_service)|
        if with_influx_service.length > 0
          write_client.write_points_to_queue(with_influx_service)
        end
        if without_influx_service.length > 0
          write_client.write_points(without_influx_service)
        end
      end
  end

  # All data merged to influx will have the analytics version set
  # Can pass aggregation: true to skip adding some common tags
  def self.merge_common_tags!(data, aggregation: false)
    unless data[:values]
      raise(ArgumentError, "must include values key with influx metric")
    end
    data[:tags]&.merge!(analytics_version: ANALYTICS_VERSION)
    unless aggregation
      is_admin_request = data[:values]&.
        with_indifferent_access&.
        try(:[], "is_admin_request")
      data[:values].merge!(is_admin_request: "0") unless is_admin_request
      data[:values][:is_admin_request] &&= data[:values][:is_admin_request].to_s
    end
    data
  end

  # For bulk recording, all data points must have a :series key pointing
  # to the measurement.
  def self.merge_series_for_bulk_record!(measurement, data_points)
    data_points.map { |record| record.merge!(series: measurement) }
  end

  # Some background explanation:
  #   In the beginning we stored A LOT of data on Influx as tags instead of fields.
  #
  #   We realize that this was growing expensive because of high cardinality,
  #   so we tried to migrate most of these over to fields.
  #
  #   we encountered a problem where we lost some data during migration.
  #   the reason is that multiple metrics with the same timestamp, tag set,
  #   and measurement get combined into one, even if they have different fields.
  #
  #   As a workaround, we add nanosecond level precision to our timestamps
  def self.use_nanosecond_precision!(measurement, data_points)
    data_points.each.with_index do |point, idx|
      # Require timestamp to be given
      point[:timestamp] = (point[:timestamp] || point.delete("timestamp"))&.to_s
      raise ArgumentError, "no timestamp given" unless point[:timestamp]
      unless point[:timestamp].to_s.length == 19
        # random nanosecond variation is added.
        point[:timestamp] = to_ns(measurement, point[:timestamp])
      end
    end
  end

  def self.to_ns(measurement, time_in_seconds)
    nanoseconds = current_ns(measurement)
    time_in_seconds.to_s.rjust(10, "0") + nanoseconds.rjust(9, "0")
  end

  def self.current_ns(measurement)
    # We don't want to give variation for aggregation events.
    return "0" if measurement.in? %w{
      user_scores_daily
      org_scores_daily
      group_scores_daily
      group_user_scores_daily
      content_engagement_metrics
      channel_aggregation_daily
      group_aggregation_daily
    }
    Time.now.nsec.to_s
  end

  def self.use_influx_service?(point)
    org_id = point[:tags][:org_id]
    org_id && ORGS_USING_INFLUX_SERVICE.member?(org_id)
  end

end
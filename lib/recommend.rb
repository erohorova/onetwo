module Recommend

  USER_RATIONALE_DISPLAY_MAP = {
    global_influencer: {
        title: 'Influencers on EdCast',
        description: "Follow the top influencers to learn from their insights"
    },
    social_connection: {
        title: 'Your friends are EdCasting',
        description: "Follow your connections to check out what they're sharing and learning",
    }
  }

  RATIONALE_AGGREGATION_MAP = {
    global_influencer: :global_influencer,
    facebook_connection: :social_connection,
    linkedin_connection: :social_connection,
    google_connection: :social_connection
  }

  class << self

    def categorize_channel_recommendations(items, options = {})
      return [], [] if items.blank?

      rationale_type = 'channel_association'
      filtered_items = items.select{|i| !i.metadata.select{|m| m.try(:[], 'rationale').try(:[], 'type') == rationale_type}.blank?}

      categorized_by_channels = {}
      cids = []
      filtered_items.each do |i|
        next unless i.metadata.kind_of?(Array)
        channel_ids = i.metadata.map{|m| m.try(:[], 'rationale').try(:[], 'channel_id')}.compact
        channel_ids.each do |cid|
          unless categorized_by_channels.has_key?(cid)
            cids << cid
            categorized_by_channels[cid] = []
          end
          categorized_by_channels[cid] << i
        end
      end

      channels = Channel.where(id: cids).index_by(&:id)
      categorized_by_channels.each do |cid, _items|
        # Return first group of 3 we can gather
        if _items.length >= 3
          return [{"title" => "Popular channels among people interested in #{channels[cid].label}", "description" => "", "items" => _items.sample(3)}], [] unless channels[cid].nil?
        end
      end

      # Now try user connection rationale
      rationale_type = 'user_association'
      filtered_items = items.select{|i| !i.metadata.select{|m| m.try(:[], 'rationale').try(:[], 'type') == rationale_type}.blank?}
      if filtered_items.length >= 3
        return [{"title" => "Popular channels in your network", "description" => "", "items" => filtered_items.sample(3)}], []
      end

      return [{"title" => "Recommended channels", "description" => "", "items" => items.sample(3)}], []
    end

    # 3 kinds of rationales exist now. To be searched in order. To be bundled up in case there isnt enough content from one user or one channel
    # A. following_user
    # B. following_channel
    def categorize_card_recommendations(items, options = {})
      permanent_exclusion_list = []
      items_dup = items.clone

      cards_by_id = Card.published.where(id: items_dup.map{|i| i.content_id}).index_by(&:id)
      permanent_exclusion_list = items_dup.select do |item|
        card = cards_by_id[item.content_id]
        card.nil? || (options[:author_must] && card.author_id.nil?) || (options[:pathway_author_must] && card.author_id.nil? && card.card_type == "pack")
      end

      items_dup.reject!{|i| permanent_exclusion_list.include?(i)}

      ret = []
      rationales_list = items.map{|i| i.metadata.kind_of?(Array) ? (i.metadata[0].try(:[], 'rationale').try(:[], 'type')) : nil}.compact
      [['following_user', 'user_id', 'User'], ['following_channel', 'channel_id', 'Channel'], ['user_interest', 'interest_id', 'Interest']].sort_by{|x| rationales_list.index(x[0]) || -1}.each do |tup|
        ret += process_following_rationale(items_dup, tup[0], tup[1], tup[2])
        items_dup -= ret.map{|r| r['items']}.flatten
      end

      unless items_dup.length < 2
        ret << {"items" => items_dup, "title" => "Content you might be interested in", "description" => ""}
      end
      return ret, permanent_exclusion_list
    end

    # Do simple categorization.
    # All live streams go together
    # All scheduled streams go together
    # Past streams get categorized based on rationale
    def categorize_stream_recommendations(items, options = {})
      items_dup = items.clone
      ret = []
      options[:show_scheduled_streams] = options.has_key?(:show_scheduled_streams) ? options[:show_scheduled_streams] : true
      streams_by_id = VideoStream.published.where(id: items_dup.map{|i| i.content_id}).index_by(&:id)

      # These streams shouldn't ever be considered
      permanent_exclusion_list = items_dup.select do |i|
        stream = streams_by_id[i.content_id.to_i]
        stream.nil? ||
          (options[:creator_must] && stream.creator.nil?) ||
          (stream.status == 'past' && !stream.past_stream_available?) ||
          (stream.status.eql?('upcoming') && !options[:show_scheduled_streams]) ||
          (stream.created_at < 1.week.ago) # Dont show video streams that are older than 1 week
      end

      items_dup.reject!{|i| permanent_exclusion_list.include?(i)}

      live = items_dup.select{|i| streams_by_id.has_key?(i.content_id.to_i) && streams_by_id[i.content_id.to_i].status == 'live'}

      unless live.blank?
        ret << {'title' => "Live right now!", 'description' => '', 'items' => live}
        items_dup -= live
      end

      scheduled = items_dup.select{|i| streams_by_id.has_key?(i.content_id.to_i) && streams_by_id[i.content_id.to_i].status == 'upcoming'}
      unless scheduled.blank?
        # sort scheduled streams by start time, overriding any kind of priority
        scheduled.sort_by!{|s| streams_by_id[s.content_id.to_i].start_time}
        ret << {'title' => "Scheduled streams you may be interested in", 'description' => '', 'items' => scheduled}
        items_dup -= scheduled
      end

      items_dup.select! do |i|
        stream = streams_by_id[i.content_id.to_i]
        stream && stream.past_stream_available?
      end

      influencer_streams = items_dup.select do |i|
        !i.metadata.select{|m| m.try(:[], 'rationale').try(:[], 'type') == 'influencer_stream'}.blank?
      end

      if influencer_streams.length >= 2
        ret << {'title' => "Streams from influencers on Edcast", 'description' => '', 'items' => influencer_streams}
        items_dup -= influencer_streams
      end

      following_user_streams = items_dup.select do |i|
        !i.metadata.select{|m| m.try(:[], 'rationale').try(:[], 'type') == 'following_user'}.blank?
      end

      if following_user_streams.length >= 2
        ret << {'title' => "Streams from people you are following", 'description' => '', 'items' => following_user_streams}
        items_dup -= following_user_streams
      end

      following_channel_streams = items_dup.select do |i|
        !i.metadata.select{|m| m.try(:[], 'rationale').try(:[], 'type') == 'following_channel'}.blank?
      end

      if following_channel_streams.length >= 2
        ret << {'title' => "Streams from channels you are following", 'description' => '', 'items' => following_channel_streams}
        items_dup -= following_channel_streams
      end

      # This is the default rationale
      unless items_dup.blank?
        ret << {'title' => "Streams you may be interested in", 'description' => '', 'items' => items_dup}
      end
      return ret, permanent_exclusion_list
    end

    def categorize_user_recommendations(rec_items, options = {})
      content_type = 'User'
      permanent_exclusion_list = []

      ## create hash 'recs' that maps rationale types (symbols) to arrays of item ids
      # recs = {rationale1: [item_id1, item_id2, ...], ...}
      recs = Hash.new{|h,k| h[k] = []}
      rec_items.each do |item|
        # assume only one metadata, with a 'rationales' key
        # the queue will return strings in hashes, but we will work with symbols
        rationales = item.metadata.try(:first).try(:[], 'rationales')
        rationales ||= [DEFAULT_RATIONALE_TYPES[content_type]]
        rationales.each { |rat| recs[rat.to_sym] << item }
      end

      aggregated = {global_influencer: [], social_connection: []}
      recs.each do |rationale, items|
        aggregated[RATIONALE_AGGREGATION_MAP[rationale]] += items
      end

      ret = []
      aggregated.each do |aggregated_rationale, items|
        if items.length > 0
          ret << USER_RATIONALE_DISPLAY_MAP[aggregated_rationale].merge({'items' => items.sample(3)}).with_indifferent_access
        end
      end

      if ret.length > 0
        return [ret.sample], permanent_exclusion_list
      else
        return nil, permanent_exclustion_list
      end
    end

    # returns one set of recommended items as {items: [...], title:, description(optional):}
    # TODO: return a group of such results for display in various feed locations
    def get_recommendations(user, content_type:)
      # get all the pertinent items
      rec_items = UserContentsQueue.get_queue_for_user(
          user_id: user.id,
          content_type: content_type,
      )
      return if rec_items.empty?
      categorized, permanent_exclusion_list = categorize_user_recommendations(rec_items)
      return categorized.map{|ct| ct.merge({items: User.where(id: ct['items'].map(&:content_id))})}.first
    end

    private

    def following_rationale_for(rationale_type, followable)
      if rationale_type == 'following_user'
        return {"title" => "Because you are following #{followable.name}", "description" => ""}
      elsif rationale_type == 'following_channel'
        return {"title" => "Because you are following #{followable.label}", "description" => ""}
      elsif rationale_type == 'user_interest'
        return {"title" => "Because you are interested in #{followable.name}", "description" => ""}
      else
        return nil
      end
    end

    def leftover_following_rationale_for(rationale_type)
      if rationale_type == 'following_user'
        return {"title" => "Content from people you are following", "description" => ""}
      elsif rationale_type == 'following_channel'
        return {"title" => "Content from channels you are following", "description" => ""}
      elsif rationale_type == 'user_interest'
        return {"title" => "Content related to your interests", "description" => ""}
      else
        return nil
      end
    end

    def process_following_rationale(items, rationale_type, followable_key, followable_class)
      ret = []
      filtered_items = items.select{|i| !i.metadata.select{|m| m.try(:[], 'rationale').try(:[], 'type') == rationale_type}.blank?}

      categorized_by_followables = {}
      fids = []
      filtered_items.each do |i|
        next unless i.metadata.kind_of?(Array)
        followable_ids = i.metadata.map{|m| m.try(:[], 'rationale').try(:[], followable_key)}.compact
        followable_ids.each do |fid|
          unless categorized_by_followables.has_key?(fid)
            fids << fid
            categorized_by_followables[fid] = []
          end
          categorized_by_followables[fid] << i
        end
      end

      followables = followable_class.constantize.where(id: fids).index_by(&:id)
      not_big_enough_to_be_on_their_own_set = []
      categorized_by_followables.each do |fid, items|
        if items.length >= 2
          ret << following_rationale_for(rationale_type, followables[fid]).merge({'items' => items}) unless followables[fid].nil?
        else
          not_big_enough_to_be_on_their_own_set += items
        end
      end
      unless not_big_enough_to_be_on_their_own_set.length < 1
        ret << leftover_following_rationale_for(rationale_type).merge({"items" => not_big_enough_to_be_on_their_own_set})
      end
      ret
    end
  end

end

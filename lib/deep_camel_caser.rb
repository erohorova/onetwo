module DeepCamelCaser

  # convert to snake case
  def self.deep_camel_case!(val)
    case val
      when Array
        val.map { |v| deep_camel_case! v }
      when Hash
        val.keys.each do |k, v|
          val[k.camelize(:lower)] = deep_camel_case!(val.delete(k))
        end
        val
      else
        val
    end
  end
end


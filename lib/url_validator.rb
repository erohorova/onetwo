class UrlValidator < ActiveModel::EachValidator
  require 'uri'
  def validate_each record, attribute, value
    begin
      uri = URI.parse(value)
      resp = true
    rescue URI::InvalidURIError
      # let relative URLs be
      resp = value.start_with?('//')
    end
    unless resp == true
      record.errors[attribute] << (options[:message] || "is not an url")
    end
  end
end

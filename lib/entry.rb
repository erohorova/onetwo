module Entry

  def perform_api_auth!
    @current_org, @current_user, @auth_params, @widget_request = PartnerAuthentication.api_auth!(params: params)
    head :forbidden and return  unless @current_org
  end

  def check_credentials!
    if is_api_client_request?(params)
      perform_api_auth!
    elsif is_oauth2_request?(params)
      doorkeeper_authorize!
      @current_user = current_resource_owner
      if @current_user.nil?
        head :unauthorized and return
      end
      @client = current_oauth_client
      @current_org = @client.organization
    else
      authenticate_user!
    end
  end

  def cm_auth_user
    @auth_params = nil
    if !(Rails.env.test? && user_signed_in?)
      return if visible_without_auth?
      check_credentials!
    end

  end

  def is_oauth2_request?(pars)
    !pars[:access_token].blank?
  end

  def is_api_client_request?(pars)
    (pars[:api_key].present? && pars[:token].present?) || (pars[:savannah_app_id].present? && pars[:auth_token].present?)
  end

  def render_with_status_and_message(template: 'ui/error', message: '', status: 401)
    respond_to do |f|
      f.html { render template, layout: false, status: status and return }
      f.json { render json: {msg: message}, status: status and return }
    end
  end

  def prepare_data(pars)
    pars = pars.with_indifferent_access
    @current_org = PartnerAuthentication.fetch_org_from_cache(pars)
    if @current_org.nil?
      return render_with_status_and_message(status: :forbidden)
    end

    #TODO Deprecated will remove it\
    @user_id = pars[:user_id]
    pars[:credential] = CacheFactory.fetch_credential_and_client(api_key: pars[:api_key]).try(:[], 0) if pars[:api_key]
    #TODO Deprecated used in json builder, will remove it
    @client = @current_org.client
    unless PartnerAuthentication.is_api_client_request_valid?(pars)
      return render_with_status_and_message(status: :forbidden)
    end
    user_email = pars[:user_email]
    @original_user_role = pars[:user_role]
    @user_role = pars[:user_role].blank? ? 'member' : pars[:user_role]
    user_image = pars[:user_image]
    user_name = pars[:user_name]
    user_first_name = pars[:user_first_name]
    user_last_name = pars[:user_last_name]
    parent_url = pars[:parent_url]

    if @user_id.nil?
      log.error('Widget Launch request missing user id')
      flash[:error] = 'Please verify your integration'
      return render_with_status_and_message(status: :bad_request)
    end

    # get parent url. Required for drill down
    host_url = nil
    @ref_id = nil

    if parent_url
      begin
        u = URI.parse(parent_url)
        u_params = Rack::Utils.parse_nested_query(u.query)
        @ref_id = u_params['edcast_ref']
        host_url = URI::Generic.build({:scheme => u.scheme,
                                    :host => u.host,
                                    :path => u.path,
                                    :query => u_params.except!('edcast_ref').to_query}).to_s

      rescue URI::InvalidURIError
        host_url = nil
        @ref_id = nil
      end
    end

    # get group or create one
    @group = CacheFactory.fetch_group(organization_id: @current_org.id, client_resource_id: pars[:resource_id])

    course_term = pars[:course_term] || pars[:custom_course_term]
    if @group.nil?
      @group = ResourceGroup.new(creator_id: User.cm_admin.id,
                         organization: @current_org,
                         client_resource_id: pars[:resource_id],
                         name: pars[:resource_name] || pars[:resource_id] , #need this to pass validation
                         description: pars[:resource_description] || pars[:resource_id], #need this to pass validation
                         website_url: host_url,
                         is_private: !!pars[:resource_is_private],
                         course_term: course_term
                        )
      @group.save!

    elsif !@group.details_locked? &&
      (@group.name != pars[:resource_name] || @group.description != pars[:resource_description] || @group.website_url != host_url || @group.is_private != !!pars[:resource_is_private])

      @group.website_url = host_url unless host_url.blank?
      @group.name = pars[:resource_name] unless pars[:resource_name].blank?
      @group.description = pars[:resource_description] unless pars[:resource_description].blank?
      @group.creator = User.cm_admin if @group.creator.nil?
      @group.is_private = !!pars[:resource_is_private]
      @group.course_term = course_term
      @group.save!
    end

    # look up user by id
    tries = 0
    begin

      if(pars[:event_request])
        pars[:savannah_app_id] = @current_org.savannah_app_id
        pars[:destiny_user_id] = User.find_by(email: user_email, organization: @current_org).try(:id)
      end
      @user = PartnerAuthentication.get_or_create_user(pars, @current_org)
    rescue => e
      tries += 1
      if tries <= 5
        # reset cache and retry
        ActiveRecord::Base.connection.clear_query_cache
        retry
      else
        raise e
      end
    end

    #if user is not enrolled in this group then automatically enroll the user
    @user_role = 'member' if %w(student learner member).include? @user_role

    @user_role = 'admin' if %w(administrator mentor ta teachingassistant instructor faculty staff teacher assistant).include? @user_role

    unless %w(member admin).include? @user_role
      log.error "#{@user_role} is an invalid role in params #{pars}"
      flash[:error] = 'Please verify your integration.'
      return render_with_status_and_message(message: "#{@user_role} is an invalid.", status: :bad_request)
    end

    @group.add_user(user: @user, role: @user_role, label: @original_user_role)

    unless user_image.nil?
      @user.picture_url = user_image
    end

    if user_first_name || user_last_name
      @user.first_name = user_first_name || ''
      @user.last_name = user_last_name || ''
    elsif user_name
      @user.first_name = user_name
      @user.last_name = ''
    end

    @user.save if @user.changed?

    # Try to map forum user to org user if exists
    if @current_org
      org_user = User.where(organization: @current_org, email: @user.email).first
      if org_user && org_user.parent_user_id.nil?
        org_user.update_with_reindex_async(parent_user_id: @user.id)
      end
    end

    @api_key = pars[:api_key]
    @token = pars[:token]

    @user_email = pars[:user_email]
    @resource_id = pars[:resource_id]
    @timestamp = pars[:timestamp]
    #for event app
    parts = [pars[:savannah_app_id], pars[:timestamp], pars[:user_id], pars[:user_email],
      pars[:user_role], pars[:resource_id], pars[:destiny_user_id]].compact
    @auth_token = Digest::SHA256.hexdigest(parts.join('|'))

    @context_label = pars[:context_label]
    @context_id = pars[:context_id]
    @display_context = pars[:display_context] ? ['true', '1'].include?(pars[:display_context]) : true #default to true

    unless @user.valid?
      @user.reload
    end

    @current_user = @user
    @score_entry = @user.score(@group)
    @user.score_entry = @score_entry
  end

  # Find the user that owns the access token
  def current_resource_owner
    User.where(id: doorkeeper_token.resource_owner_id, is_suspended: false).first if doorkeeper_token
  end

  def current_oauth_client
    Client.find(doorkeeper_token.application_id) if doorkeeper_token
  end
end

module UrlBuilder

  def self.build(url, params)

    begin
      u = URI.parse(url)
      u_params = Rack::Utils.parse_nested_query(u.query)
      new_url = URI::Generic.build({:scheme => u.scheme,
                                 :host => u.host,
                                 :path => u.path,
                                 :query => u_params.merge!(params).to_query}).to_s
    rescue URI::InvalidURIError
      new_url
    end
    new_url
  end

end

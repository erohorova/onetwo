require 'aws/core/http/curb_handler'
class AWS::SimpleEmailService
  attr_accessor :settings


  def initialize(options = {})
    self.settings = options.except(:config)
    super
  end

    # Sends a raw email (email message, with header and content specified).
    # Useful for sending multipart MIME emails.  The raw text of the message
    # must comply with Internet email standards; otherwise, the message
    # cannot be sent.
    #
    #     raw = <<-EMAIL
    #     Date: Wed, 1 Jun 2011 09:13:07 -0700
    #     Subject: A Sample Email
    #     From: "John Doe" <johndoe@domain.com>
    #     To: "Jane Doe" <janedoe@domain.com>
    #     Accept-Language: en-US
    #     Content-Language: en-US
    #     Content-Type: text/plain; charset="utf-8"
    #     Content-Transfer-Encoding: base64
    #     MIME-Version: 1.0
    #
    #     c2FtcGxlIHRleHQNCg==
    #     EMAIL
    #
    #     ses.send_raw_email(raw)
    #
    # Amazon SES has a limit on the total number of recipients per
    # message: The combined number of To:, CC: and BCC: email addresses
    # cannot exceed 50. If you need to send an email message to a larger
    # audience, you can divide your recipient list into groups of 50 or
    # fewer, and then call Amazon SES repeatedly to send the message to
    # each group.
    #
    # @param [required, String] raw_message The raw text of the message.
    #   You can pass in any object whos #to_s returns a valid formatted
    #   email (e.g. ruby Mail gem).  The raw message should:
    #   * Contain a header and a body, separated by a blank line
    #   * Contain all required internet email headers
    #   * Each part of a multipart MIME message must be formatted properly
    #   * MIME content types must be among those supported by Amazon SES.
    #     Refer to the Amazon SES Developer Guide for more details.
    #   * Use content that is base64-encoded, if MIME requires it
    # @option options [String,Array] :to One or more email addresses to
    #   send the email to.
    # @option options [String] :from The sender's email address.
    #   If you specify the :from option, then bounce notifications and
    #   complaints will be sent to this email address. This takes
    #   precedence over any Return-Path header that you might include in
    #   the `raw_message`.
    # @return [Core::Response] the SendRawEmail response
    def send_raw_email raw_message, options = {}

      send_opts = {}
      send_opts[:async] = settings[:async] if settings #merge the async flag down
      send_opts[:raw_message] = {}
      send_opts[:raw_message][:data] = raw_message.to_s
      send_opts[:source] = options[:from] if options[:from]

      if raw_message.respond_to?(:destinations)
        send_opts[:destinations] = raw_message.destinations
      end
      send_opts[:destinations] = [options[:to]].flatten if options[:to]

      response = client.send_raw_email(send_opts)

      if raw_message.respond_to?(:message_id=)
        raw_message.message_id = "#{response.data[:message_id]}@email.amazonses.com"
      end

      response
    end

    # for compatability with ActionMailer
    alias_method :deliver, :send_raw_email
    alias_method :deliver!, :send_raw_email
end
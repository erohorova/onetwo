# monkey patch paperclip to fix a security issue in paperclip
# we cannot use this fix right now because of some content spoofing
# restrictions new versions of paperclip has.
# https://github.com/thoughtbot/paperclip/pull/1514/files

module Paperclip
  class GeometryDetector

    private
    def geometry_string
      begin
        Paperclip.run("identify", "-format '%wx%h,1' :file", :file => "#{path}[0]")
      rescue Cocaine::ExitStatusError
        ""
      rescue Cocaine::CommandNotFoundError => e
        raise_because_imagemagick_missing
      end
    end
  end
end

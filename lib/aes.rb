class Aes
  # SECTION 1: Starts
  # DESC:
  #   1. Encryption exposes the `iv` value in the response. So, Decryption is able to grab the plain value from the encrypted value being received.
  #   2. Encrypted response is Base64 encoded value.
  #   3. Key is 16-bytes in length.
  def self.decrypt(encrypted_data, key)
    decoded_text = Base64.decode64(encrypted_data)

    ciphertext, iv = decoded_text.split('||/')

    cipher = OpenSSL::Cipher::Cipher.new('aes-128-cfb')
    cipher.decrypt
    cipher.key = key[0..15]
    cipher.iv = iv
    cipher.update(ciphertext)
  end

  def self.encrypt(text, key)
    cipher = OpenSSL::Cipher::Cipher.new('aes-128-cfb')
    cipher.encrypt
    cipher.key = key[0..15]
    iv = cipher.random_iv
    data = (cipher.update(text) + cipher.final) + "||/" + iv
    Base64.encode64(data)
  end
  # SECTION 1: Ends

  ############ DO NOT CHANGE: Starts ############
  # SECTION 2: Starts
  # CAUTION: Used in LAMBDA function in AWS.
  # DESC:
  #   1. Encryption encrypts the `iv` value in the response. So, decryption is able to grab the encrypted iv and decrypt it.
  # CONVENTION:
  #   text = { filestack_url: 'https://cdn.filestackcontent.com/Q31hcvi9TiKHZuFOwzxi', user_id: 11 }.to_json
  #   encrypted_data = Aes.safe_encrypt(text, 'xyz')
  #   response = Aes.safe_decrypt(encrypted_data, 'xyz')
  def self.safe_decrypt(encrypted_data, key)
    packed_iv, packed_ciphertext = encrypted_data.split(':')

    iv         = [packed_iv].pack('H*')
    ciphertext = [packed_ciphertext].pack("H*")
    secret_key =  [Digest::MD5.hexdigest(key)].pack("H*")

    cipher = OpenSSL::Cipher.new('aes-128-cbc')
    cipher.decrypt
    cipher.key = secret_key
    cipher.iv  = iv

    (cipher.update(ciphertext) + cipher.final).gsub(/\0+$/, '')
  end

  def self.safe_encrypt(text, key)
    secret_key =  [Digest::MD5.hexdigest(key)].pack("H*")
    cipher = OpenSSL::Cipher.new('aes-128-cbc')
    iv = cipher.random_iv
    cipher.encrypt
    cipher.key = secret_key
    cipher.iv  = iv
    encrypted_text = cipher.update(text) + cipher.final

    [iv.unpack("H*"), encrypted_text.unpack("H*")].join(":")
  end
  # SECTION 2: Ends
  ############ DO NOT CHANGE: Ends ############
end

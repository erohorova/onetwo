module ActiveRecordConfiguration
  module ClassMethods
    def cfg(name)
      Configuration.send(:get_cfg, name, self)
    end
  end

  module InstanceMethods
    def full_errors
      self.errors.messages.values.flatten.to_sentence
    end
  end
end

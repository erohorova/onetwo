namespace :organization_configs do
  desc 'Migrate user acceptance prompt messages & links to new structure'
  task migrate_organization_prompt_links: :environment do
    STDOUT.puts "Started migration ..."
    configs = Config.where(name: 'acceptancePromptMessage', configable_type: 'Organization')
    if configs.present?
      configs.each do |config|
          org_id = config.configable_id
          link = Config.where(configable_id: org_id, name: 'acceptancePromptLink', configable_type: 'Organization').first
          old_msg = config.value
          if link
            new_msg = "#{old_msg} <a target='_blank' href='#{link.value}'>Terms & Conditions</a>"
            config.value = new_msg
            config.save!
            Config.destroy(link)
          else
            STDOUT.puts "No link for organization id: #{org_id} where prompt message is: #{old_msg}"
          end
      end
    end
    STDOUT.puts "Done migration ..."
  end
end
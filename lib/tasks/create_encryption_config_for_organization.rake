namespace :create_encryption_config_for_organization do 
	desc 'create a config encryption for organizations'
	task :encryption_config, [:org_id] => :environment do |_task, args|
    Config.find_or_create_by(configable_id: args.org_id, configable_type: 'Organization', name: 'encryption_payload', data_type: 'boolean', value: true)
    puts 'Encryption Payload config created successfully'
  end
end



namespace :migrate_user_images do

  # this task converts http://graph.facebook.com/picture/123
  # to the https version. We have http urls for some early users
  # when we were not asking Facebook for the secure url
  # Filtering out users who might have avatar images set manually
  # One time run
  desc 'migrate user images'
  task go: :environment do
    User.where("avatar_file_name IS NULL AND picture_url LIKE '%http://graph.facebook.com%'").find_each do |user|
      user.picture_url = user.picture_url.gsub(/^http:\/\//, 'https://') 
      user.save
    end
  end
end


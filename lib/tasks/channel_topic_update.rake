namespace :update_channel_topic do
  # This rake will update topic, node_id and taxo_id in channel table
  # org id and taxonomy topic prefix will be taken from environment variable

  desc "This will update topic, node_id in channel table"

  task :channel_topic_update, [:organization_id, :taxo_prefix, :taxo_root_flag, :old_root] => [:environment] do |t, args|
    @org_id = args[:organization_id]

    # Create token using org_id and sociative secret key
    token = JWT.encode({ organization_id: @org_id }, Settings.sociative.sociative_auth_secret, 'HS256')

    # Get taxonomy topics and create a hash having 'key' as node_path and value as name, domain_id, topic_id, taxo_id
    # and domain_name
    taxonomy_topics = Adapter::Sociative.new({ organization_id: @org_id }).get_taxonomy_topics(token)

    # Get all channels for given organization
    channel_obj = Channel.where(organization_id: @org_id).where.not(topics:nil)
    if channel_obj.present?
      channel_obj.each do |channel|

        # Get topic of channel and call convert_topic to update topic and node_id
        channel_topic = channel.topics
        if channel_topic.present?
          channels_topics =channel_topic.map do |topic|
            TopicUpdateService.new("channel").convert_topics(taxonomy_topics, topic, args[:taxo_prefix], args[:taxo_root_flag], args[:old_root])
          end

          # Update topic in channel db
          if channels_topics.present?
            channel.update_attribute(:topics, channels_topics)
            log.info "Updated topic, node id for channel #{channel.id}"
          end
        end
      end
    end
  end
end
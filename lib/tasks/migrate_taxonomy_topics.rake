namespace :migrate_taxonomy_topics do

  desc "updating learing interest of users from two level to one level"
  task migrate_user_learning_topics: :environment do
    org_id = ENV["ORG_ID"]
    taxo_url = ENV["TAXO_URL"]
    if org_id.blank? || taxo_url.blank?
      puts "Please provide organization id/ taxonomy url"
      return
    end

    # Get taxonomy topics response from sociative
    raw_response = Faraday.get taxo_url
    taxonomy_response = JSON.parse(raw_response.body).first

    # level 1 to level 2 mapping of taxonomy topics
    taxonomy_mappings = {}
    taxonomy_response['data'].each do |level1|
      taxonomy_mappings[level1['id']] = {
        name: level1['path'],
        label: level1['label'],
        level2_ids: level1['data']&.map{|level2| level2['id']}
      }
    end

    # Nasscom has ~4k users with learning topics
    user_ids = []
    UserProfile.joins(:user).where(users: {organization_id: org_id})
      .where('user_profiles.learning_topics is not null')
      .find_each do |user_profile|
        old_learning_topics = user_profile.learning_topics
        new_learning_topics = []
        old_learning_topics.each do |learning_topic|
          level1_topic = taxonomy_mappings.select{|k,v| v[:level2_ids]&.include?(learning_topic['topic_id'].to_s) }
          next if level1_topic.blank? || new_learning_topics.map{|tp| tp['topic_id']}.include?(level1_topic.keys.first)
          new_topic = {}
          new_topic['topic_id'] = level1_topic.keys.first
          new_topic['topic_label'] = level1_topic[new_topic['topic_id']][:label]
          new_topic['topic_name'] = level1_topic[new_topic['topic_id']][:name]
          new_topic['domain_id'] = taxonomy_response['id']
          new_topic['domain_label'] = taxonomy_response['label']
          new_topic['domain_name'] = taxonomy_response['path']
          new_learning_topics << new_topic
          user_ids << user_profile.user_id
        end
        user_ids.uniq!
        if user_ids.include?(user_profile.user_id)
          user_profile.learning_topics = new_learning_topics
          user_profile.save
        end
    end
    puts "successfully migrated user learning topics for #{user_ids} users, users count : #{user_ids.uniq.count}"
  end

  desc "updating channel topics from two level to one level"
  task migrate_channel_topics: :environment do
    org_id = ENV["ORG_ID"]
    taxo_url = ENV["TAXO_URL"]
    if org_id.blank? || taxo_url.blank?
      puts "Please provide organization id/ taxonomy url"
      return
    end

    # Get taxonomy topics response from sociative
    raw_response = Faraday.get taxo_url
    taxonomy_response = JSON.parse(raw_response.body).first
    
    # level 1 to level 2 mapping of taxonomy topics
    taxonomy_mappings = {}
    taxonomy_response['data'].each do |level1|
      taxonomy_mappings[level1['id']] = {
        name: level1['name'],
        label: level1['label'],
        level2_ids: level1['data']&.map{|level2| level2['id'] }
      }
    end

    # Nasscom has 59 such channels
    channel_ids = []
    Channel.where('topics is not null').where(organization_id: org_id).find_each do |channel|
      old_channel_topics = channel.topics
      new_channel_topics = []
      old_channel_topics.each do |topic|
        level1_topic = taxonomy_mappings.select{|k,v| v[:level2_ids]&.include?(topic['id'].to_s) }
        next if level1_topic.blank? || new_channel_topics.map{|tp| tp['id']}.include?(level1_topic.keys.first)
        new_topic = {}
        new_topic['id'] = level1_topic.keys.first
        new_topic['label'] = level1_topic[new_topic['id']][:label]
        new_topic['name'] = level1_topic[new_topic['id']][:name]
        new_channel_topics << new_topic
        channel_ids << channel.id
      end
      if channel_ids.include?(channel.id)
        channel.topics = new_channel_topics
        channel.save
      end
    end
    puts "successfully migrated channel topics for #{channel_ids.uniq} channels"
  end

end
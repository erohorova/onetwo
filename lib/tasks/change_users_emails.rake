namespace :one_time_tasks do
  desc 'to change the users email address'
  task change_users_emails: :environment do
    require 'open-uri'
    host_name = ARGV[1].strip
    org = Organization.where(host_name: host_name).first
    csv = CSV.parse(open('https://s3.amazonaws.com/ed-general/change_email.csv'))
             .each  do |line|
                      if org.nil?
                        STDOUT.puts "Bad org hostname: #{host_name}"
                      else
                        user = User.where(email: line[0].strip, organization_id: org.id).first
                        if user.nil?
                          STDOUT.puts "User not found for email: #{line[0].strip} and org #{org.host_name}"
                        else
                          user.update_column(:email, line[1].strip)
                          user.reindex
                        end
                      end
                    end
  STDOUT.puts "Ending change users email rake task for #{org.host_name}"
  end
end
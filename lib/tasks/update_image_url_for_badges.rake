namespace :badges do
  desc 'Update image url for badges(ClcBadge and CardBadge)'
  task update_image_url_for_custom_badges: :environment do

    badges = Badge.where(is_default: false)
    badges.each do |badge|
      if badge.image.url
        badge.image = open(badge.image.url.gsub(/^[\/]*/, ''))
        badge.save
      end
    end
  end
end

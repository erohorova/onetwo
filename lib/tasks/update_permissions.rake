namespace :update_permissions do
  desc "update permissions for admin role"
  task admin_role_permissions: :environment do
    Role.admins.find_each{|r| r.create_role_permissions }
    puts "Role permissions for admin was successfully updated"
  end

  desc "update permissions for member role"
  task member_role_permissions: :environment do
    Role.members.find_each{|r| r.create_role_permissions }
    puts "Role permissions for member was successfully updated"
  end

  desc "update permissions for curator role"
  task curator_role_permissions: :environment do
    Role.curators.find_each{|r| r.create_role_permissions }
    puts "Role permissions for curator was successfully updated"
  end

  desc "update permissions for collaborator role"
  task collaborator_role_permissions: :environment do
    Role.collaborators.find_each{|r| r.create_role_permissions }
    puts "Role permissions for collaborator was successfully updated"
  end

  desc "update permissions for sme role"
  task sme_role_permissions: :environment do
    Role.sme.find_each{|r| r.create_role_permissions }
    puts "Role permissions for sme was successfully updated"
  end
end

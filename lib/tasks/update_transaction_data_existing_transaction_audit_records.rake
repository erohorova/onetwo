namespace :transaction_audit do
  desc "Update transaction_data column of transaction_audits table with a hash to maintain consistency"
  task update_transaction_data_existing_records: :environment do

    puts "Updating transaction_audits"

    # Around 633 records exist in the transaction_audits table in production. Might take around 20-25 seconds
    TransactionAudit.all.group_by(&:transaction_id).each do |transaction_id, grouped_transaction_audit|
      latest_transaction_audit = grouped_transaction_audit.last
      grouped_transaction_audit.each do |transaction_audit|
        # Update payment_state only for the latest transaction audit record for a transaction.
        # We only have information about the final state and no reference to the previous states. So keeping them as nil
        payment_state = (latest_transaction_audit.id == transaction_audit.id) ? transaction_audit.user_transaction&.payment_state : nil
        transaction_audit.update(transaction_data: { gateway_data: transaction_audit.transaction_data, payment_state: payment_state })
      end
    end

    puts "Finished Updating transaction_audits"
  end
end
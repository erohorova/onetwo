namespace :ecl do
  desc 'add url_hashes to all resources'
  task update_resource_url_hashes: :environment do
    start = 0

    puts "Total count of resources: #{Resource.where(url_hash: nil).count}"
    Resource.where(url_hash: nil).find_in_batches do |group|
      group.each do |resource|
        resource.update_columns(url_hash: Resource.get_url_hash(resource.url))
      end

      start += group.size
      puts "Completed updating #{start} resources."
    end
  end

  desc 'check ecl source fetch job status'
  task check_ecl_source_fetch_job_status: :environment do
    nonenqueued_ecl_sources = []

    EclSource.find_each do |ecl_source|
      record = BackgroundJob.where(job_class: 'EclSourceFetchJob', job_arguments: "[#{ecl_source.id}]").order("enqueued_at desc").first

      # if job is not enqueued or not error then affected
      if ["enqueued", "error"].exclude?(record.try(:status))
        nonenqueued_ecl_sources << [ecl_source.id, ecl_source.channel_id]
      end
    end

    puts "-------------------------------------------"
    # affected ecl sources
    affected_ecl_source_ids = nonenqueued_ecl_sources.map(&:first)
    puts "Affected ecl sources : #{affected_ecl_source_ids}"
    puts "-------------------------------------------"
    # affected channels
    affected_channel_ids = nonenqueued_ecl_sources.map(&:second).compact.uniq
    puts "Affected channels : #{affected_channel_ids}"
    puts "-------------------------------------------"
    # affected {organization_id => channels_count}
    affected_data = Channel.where(id: affected_channel_ids).group(:organization_id).count(:id)
    puts "Affected Data: {organization_id => channel_count} \n#{affected_data}"
    puts "-------------------------------------------"
    # affected organizations
    affected_organization_ids = affected_data.keys
    puts "Affected organizations : #{affected_organization_ids}"
    puts "-------------------------------------------"
    affected_organization_host_names = Organization.where(id: affected_organization_ids).pluck(:host_name)
    puts "Affected organizations host names : #{affected_organization_host_names}"
  end
end

namespace :card_metadata do
  desc 'Update average_rating of cards having cards_ratings'
  task update_average_rating: :environment do
    card_ids = Card.joins(:cards_ratings).where.not(cards_ratings: {id: nil}).pluck(:id).uniq
    
    Card.where(id: card_ids).find_each do |card|
      average_rating = card.cards_ratings.average(:rating)
      if average_rating
        card_metadata = card.card_metadatum || card.build_card_metadatum
        card_metadata.average_rating = average_rating
        card_metadata.save
      end
    end
  end
end


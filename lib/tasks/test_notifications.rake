namespace :notifications do
  #not making any DB writes with this rake task
  #the purpose is to test the notifications only
  desc 'test emails'

  def quit
    STDOUT.puts "Ending this rake BYE!!"
  end

  def testable_options
    @testable_options = [
      {prompt: 'New Follower Notifier'},
      {prompt: 'New Livestream From Followed User Notifier'},
      {prompt: 'New Smartbite Comment Notifier'},
      {prompt: 'New Content By Followed User Notifier'},
      {prompt: 'Completed Assignment Notifier'},
      {prompt: 'Assigned Content Notifier'},
      {prompt: 'New Content In Channel Notifier'},
      {prompt: 'New Activity Commented Smartbite Notifier - New Comment'},
      {prompt: 'New Content By Followed User Notifier'},
      {prompt: 'Mention In Comment Notifier'}
    ]
  end

  def make_notification_entry
    case @choice
    when 0 #event_new_follow
      follow = Follow.where(user_id: @user.id).first
      event_name = 'new_follow'
      notification_name = 'new_follower'
      sourceable_type = 'Follow'
      sourceable_id = follow.id
      {web_text = :text=>"Web for new follow"}
    when 1 #event_new_livestream
      livestream = Card.where(card_type: 'video_stream', author_id: @user.id).first || Card.where(card_type: 'video_stream', organization_id: @org.id).first
      event_name = 'new_livestream'
      notification_name = 'new_livestream_from_followed_user'
      sourceable_type = 'Card'
      sourceable_id = livestream.id
      {web_text = :text=>"#{@user.name} is starting a Livestream now"}
    when 2 #event_new_smartbite_comment
      comment = Comment.where(user_id: @user.id).first
      event_name = 'new_comment'
      notification_name = 'new_smartbite_comment'
      sourceable_type = 'Comment'
      sourceable_id = comment.id
      {web_text = :text=>"#{@user.name} commented on your smartbite."}
    when 3 #event_new_content_by_followed_user
      follow = Follow.where(user_id: @user.id).first
      event_name = 'new_follow'
      notification_name = 'new_follower'
      sourceable_type = 'Follow'
      sourceable_id = follow.id
      {web_text = :text=>"Web for new follow"}
    when 4 #event_completed assignment
      assignment = Assignment.where(user_id: @user.id).where.not(completed_at: nil).last
      event_name = 'complete_assignment'
      notification_name = 'completed_assignment'
      sourceable_type = 'Assignment'
      sourceable_id = assignment.id
      {web_text = :text=>"text"}
    when 5 #event_assigned_content
      assignment = Assignment.where(user_id: @user.id).first
      event_name = 'new_assignment'
      notification_name = 'assigned_content'
      sourceable_type = 'Assignment'
      sourceable_id = assignment.id
      {web_text = :text=>"text"}
    when 6 #event_new_content_in_channel
      channel = Channel.where(organization_id: @org.id, label: :'General').first
      channel_card = ChannelsCard.where(channel_id: channel.id).first
      event_name = 'notify_new_channel_cards'
      notification_name = 'new_content_in_channel'
      sourceable_type = 'ChannelsCard'
      sourceable_id = channel_card.id
      {web_text = :text=>"#{channel.label} has new posts"}
    when 7 #new_comment_on_smartbite ?? done in 2
      comment = Comment.where(user_id: @user.id).first
      card = comment.commentable
      last_comment = card.comments.last
      event_name = 'new_comment'
      notification_name = 'new_activity_commented_smartbite'
      sourceable_type = 'Comment'
      sourceable_id = last_comment.id
      {web_text = :text=>"text"}
    when 8 #event_new_content_by_followed_user
      card = Card.where(author_id: @user.id).last
      event_name = 'new_card'
      notification_name = 'new_content_by_followed_user'
      sourceable_type = 'Card'
      sourceable_id = card.id
      {web_text = :text=>"#{card.author.name} posted a new SmartBite"}
    when 9 #event_mention_in_comment - New Comment
      mention = Mention.where(user_id: @user.id).first
      comment = mention.mentionable
      event_name = 'new_comment'
      notification_name = 'mention_in_comment'
      sourceable_type = 'Comment'
      sourceable_id = comment.id
      {web_text = :text => "#{@user.name} mentioned you in a comment."}
    end
    NotificationEntry.new(id: 1,
                   user_id: @user.id,
                   event_name: event_name,
                   notification_name: notification_name,
                   sourceable_type: sourceable_type,
                   sourceable_id: sourceable_id,
                   config_web_single: 1,
                   status_web_single: 1,
                   web_text: web_text,
                   config_email_single: 1,
                   status_email_single: 1,
                   config_email_digest_daily: 0,
                   status_email_digest_daily: 0,
                   config_email_digest_weekly:0,
                   status_email_digest_weekly: 0,
                   config_push_single: 1,
                   status_push_single: 0,
                   config_push_digest_daily: 0,
                   status_push_digest_daily: 0,
                   config_push_digest_weekly: 0,
                   status_push_digest_weekly: 0,
                   date_in_timezone: Date.today,
                   sending_time_in_timezone: Time.now,
                   created_at: Time.now,
                   updated_at: Time.now)
  end

  def send_notifications
    ne = make_notification_entry
    def ne.save
      STDOUT.puts 'fake save!'
    end
    #send_notifications
    EDCAST_NOTIFY.send_single_notification(ne)
  end

  def send_notifi
    if @testable_options[@choice]
      self.send(:send_notifications)
    else
      STDOUT.puts "Wrong number entered"
      quit
    end
  end

  task :notifications_and_triggers =>:environment do
    host_name = ARGV[1].strip
    email = ARGV[2].strip
    @org = Organization.where(host_name: host_name).first
    @user = User.where(email: email, organization_id: @org.id).first
    STDOUT.puts "Running rake task for org: #{@org.home_page} & user: with name: #{@user.name} and email: #{@user.email} #{@user.id}"
    STDOUT.puts "If the above info is correct enter '1' or anything else to quit"
    input = STDIN.gets.chomp
    if input == '1'
      testable_options
      STDOUT.puts "\n\n"
        @testable_options.each_with_index do |v,index|
          STDOUT.puts "#{(index + 1).to_s.rjust(2)}  ==>  #{v[:prompt]}"
        end
        STDOUT.puts "\n\n"
        STDOUT.puts "Enter the number of the notification you want to test or anything else to Quit"
        @choice = (STDIN.gets.chomp).to_i - 1
        if !@testable_options[@choice].present?
          STDOUT.puts "Wrong number entered"
          quit
        else
          send_notifi
        end
    else
      quit
    end
  end
end

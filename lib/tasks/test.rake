namespace :test do

  desc "run tests marked with :api to generate APIPIE docs"
  task doc: :environment do
    def generate_doc filepath, controller
      # we don't care about the output of tests. send it to nowhereland!
      sh "APIPIE_RECORD=examples ruby -I test #{filepath} >> /dev/null"

      generated_message = "echo \"$(tput setaf 2)#{controller} " +
        "Controller doc generated.$(tput sgr0)\""
      sh generated_message
    end

    def test_finished n
      finish_message = "echo \"$(tput setaf 2)All tests executed." +
        " #{n} controllers searched$(tput sgr0)\""

      sh finish_message
    end

    def show_all_examples!
      text = File.read('doc/apipie_examples.json')
      new_text = text.gsub(/"show_in_doc": 0/, '"show_in_doc": 1')
      File.open('doc/apipie_examples.json', 'w') { |file| file.puts new_text}
    end

    path = Rails.root.join("test/controllers/**/*_test.rb").to_s

    filter = if ENV['test']
               ENV['test'].split(',')
             else
               nil
             end
    n = 0
    verbose(false) do
      FileList[path].map(&:to_s).each do |testfile|
        filepath = testfile + " -n " + "\"/:api/\""
        controller = testfile.split('/').last.split('_').first.capitalize

        Rake::Task['db:test:prepare'].execute
        if filter.present?
          if filter.select { |f| testfile.include? f }.present?
            generate_doc filepath, controller
            n += 1
          end
        else
          generate_doc filepath, controller
          n += 1
        end
      end
      show_all_examples!
      Rake::Task['apipie:static'].invoke
      test_finished n
    end
    n
  end

  task js: :environment do
    system("cd redux/ && npm run-script coverage")
    exit_status = $?.exitstatus
    exit(exit_status)
  end

  desc "Test all tests expects the ones in slow_tests and controlleres"
  Rails::TestTask.new(the_rest: 'test:prepare') do |t|
    t.libs << 'test'
    t.test_files = FileList['test/**/*_test.rb'].exclude('test/controllers/**/*_test.rb').exclude('test/slow_tests/**/**/*_test.rb')
    t.verbose = true
  end

  desc "Test all tests controllers except apiv2"
  Rails::TestTask.new(controllers_pre_v2_only: 'test:prepare') do |t|
    t.libs << 'test'
    t.test_files = FileList['test/controllers/**/*_test.rb'].exclude('test/controllers/api/v2/**/*_test.rb')
    t.verbose = true
  end

  desc "Test all api v2 actions"
  Rails::TestTask.new(controllers_v2: 'test:prepare') do |t|
    t.libs << 'test'
    t.test_files = FileList['test/controllers/api/v2/**/*_test.rb']
    t.verbose = true
  end

  desc "Test all developers api actions"
  Rails::TestTask.new(developers_api: 'test:prepare') do |t|
    t.libs << 'test'
    t.test_files = FileList['test/controllers/api/developer/**/*_test.rb']
    t.verbose = true
  end

  desc "Test all event recorder classes"
  Rails::TestTask.new("analytics_recorders" => "test:prepare") do |t|
    t.libs << 'test'
    t.pattern = 'test/lib/analytics/**/*_test.rb'
    t.verbose = true
  end

  desc "Test all bridge classes"
  Rails::TestTask.new("bridges" => "test:prepare") do |t|
    t.libs << 'test'
    t.pattern = 'test/bridges/**/*_test.rb'
    t.verbose = true
  end

  desc "Test all mailer classes"
  Rails::TestTask.new("mailers" => "test:prepare") do |t|
    t.libs << 'test'
    t.pattern = 'test/mailers/**/*_test.rb'
    t.verbose = true
  end

  desc "Test all service classes"
  Rails::TestTask.new("services" => "test:prepare") do |t|
    t.libs << 'test'
    t.pattern = 'test/services/**/*_test.rb'
    t.verbose = true
  end
end

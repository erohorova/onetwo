namespace :create_genpact_header_icon_config do 
  desc 'create a config to add header icon on genpact header only'
  # EP-20457: To add json config for genpact header icon :
  # rake create_genpact_header_icon_config:genpact_header_icon["org_id=6&image_url=https://s3.amazonaws.com/ed-general/Genpact/Education%40Work+-+new+logo.png&header_icon=true&channel_url=dsg"]
  
  # Note: To turn of header icon config , pass 'header_icon' to false 
  # e.g. rake create_genpact_header_icon_config:genpact_header_icon["org_id=6&image_url=https://s3.amazonaws.com/ed-general/Genpact/Education%40Work+-+new+logo.png&header_icon=false&channel_url=dsg"]
  task :genpact_header_icon, [:args_expr] => :environment do |_task, args|
    if args[:args_expr].present?
      options = Rack::Utils.parse_nested_query(args[:args_expr])
      Config.find_or_create_by( 
        name: 'genpact_header_icon',
        configable_id: options['org_id'],
        configable_type: 'Organization',
        data_type: 'json'
      ).update_attributes(
        value: {
          "channel_url" => options['channel_url'],
          "image_url" => options['image_url'],
          "header_icon" => options['header_icon'].to_bool
        }.to_json
      )
    end
  end
end

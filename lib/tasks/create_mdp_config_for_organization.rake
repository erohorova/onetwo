namespace :create_mdp_config_for_organization do 

  desc 'create a config add to MDP for organizations'
  task :add_to_mdp_config, [:org_id] => :environment do |_task, args|

    Config.find_or_create_by(configable_id: args.org_id, configable_type: 'Organization', 
      name: 'addToMDP', data_type: 'boolean', value: 'true')
    puts "MDP config successfully created for org #{args.org_id}"

  end
end
namespace :update_configs_org_customization_config do
  desc 'Update default team label config for organizations'
  task default_label_team: :environment do
    Config.where(name: 'OrgCustomizationConfig').each do |config|
      parsed_value = config.parsed_value
      teams_config = parsed_value.dig 'web', 'profile', 'web/profile/teams'
      next unless teams_config
      teams_config['defaultLabel'] = 'Team'
      config.value = parsed_value.to_json
      config.save
    end
    puts 'DefaultLabel: team was successfully updated'
  end
  desc 'Update default people label config for organizations'
  task default_label_people: :environment do
    Config.where(name: 'OrgCustomizationConfig').each do |config|
      parsed_value = config.parsed_value
      teams_config = parsed_value.dig 'web', 'profile', 'web/profile/teams'
      next unless teams_config
      teams_config['defaultLabel'] = 'People'
      config.value = parsed_value.to_json
      config.save
    end
    puts 'DefaultLabel: people was successfully updated'
  end

  desc "Update organization discover page config featured providers carousel"
  task :add_provider_carousel, [:org_name, :carousel_name, :label, :position, :category] => :environment do |task, args|
    # Run task when we need to add sources/featured provider carousel on discover page
    # params organization name, carousel name, label to display on carousel heading, 
    # position of carousel on disover page, source category
    if args.org_name.present? && args.carousel_name.present? && args.label.present? && args.position.present? && 
      (org = Organization.find_by_name(args.org_name)).present?
        Config.where(name: "OrgCustomizationConfig", configable: org).each do |config|
          parsed_value = JSON.parse(config.value)
          if parsed_value["discover"].present?
            parsed_value["discover"].each do |key,val|
              val["index"] += 1 if val["index"] >= args.position.to_i
            end
            parsed_value["discover"].merge!({"discover/featuredProviders/#{args.carousel_name}"=>{"defaultLabel"=>"#{args.label}", 
              "label"=> "#{args.label}", "visible"=>false, "index"=>args.position.to_i, "category"=>args.category}}) 
            parsed_value["discover"] = parsed_value["discover"].sort_by{ |k,v| v["index"] }.to_h
            config.update(value: parsed_value.to_json)
            puts 'Discover provider carousel config was successfully updated'
          end
        end
    end
  end

  desc "Update featured providers category config"
  task :update_provider_category, [:org_name, :carousel_key, :category] => :environment do |task, args|
    # Run task when we need to update sources/featured provider carousel category
    # params organization name, carousel key which needs category update, new source category
    if args.org_name.present? && args.carousel_key.present? && args.category.present? &&
      (org = Organization.find_by_name(args.org_name)).present?
        Config.where(name: "OrgCustomizationConfig", configable: org).each do |config|
          parsed_value = JSON.parse(config.value)
          if parsed_value["discover"].present? && parsed_value["discover"][args.carousel_key].present?
            parsed_value["discover"][args.carousel_key]["category"] = args.category
            config.update(value: parsed_value.to_json)
            puts 'Provider carousel category was successfully updated'
          end
        end
    end
  end

  desc "Remove newMobileMeTab, newMeTabContent, newMeTabSections, newMeTabStats from existing configs for mobile and reformat json response to include meTabV2"
  task update_me_tab_config_for_mobile: :environment do
    # E.g mobile/meTabStatsV2/careerAdvisor --> mobile/meTabStats/careerAdvisor
    replace_v2_keys = lambda do |v2_hash, parent_key_name, desired_key_name|
      if v2_hash
        v2_hash.keys.each do |key_name|
          if key_name.include?(parent_key_name)
            new_key_name = key_name.dup #Else it gives Frozen string error while modifying keyname
            new_key_name.gsub!("#{parent_key_name}", desired_key_name)
            v2_hash[new_key_name] = v2_hash.delete(key_name)
          end
        end
        v2_hash
      end
    end

    # 255 records in production for this query
    Config.where(name: "OrgCustomizationConfig").each do |config|
      value = config.parsed_value
      value.except!("newMobileMeTab")
      value.dig("mobile").except!("newMeTabContent", "newMeTabSections", "newMeTabStats")

      me_tab_content_v2 = value.dig("mobile","meTabContentV2")
      if me_tab_content_v2
        ["profile", "groups", "myContent"].each do |subkey|
          if me_tab_content_v2.has_key?(subkey)
            me_tab_content_v2[subkey] = replace_v2_keys.call(me_tab_content_v2.dig(subkey), "meTabContentV2", "meTabV2/meTabContent")
          end
        end
      end
      me_tab_sections_v2  = replace_v2_keys.call(value.dig("mobile","meTabSectionsV2"), "meTabSectionsV2", "meTabV2/meTabSections")
      me_tab_stats_v2     = replace_v2_keys.call(value.dig("mobile","meTabStatsV2"), "meTabStatsV2", "meTabV2/meTabStats")

      if me_tab_content_v2.present? && me_tab_sections_v2.present? && me_tab_stats_v2.present?
        value.dig("mobile")["meTabV2"] = { meTabContent: me_tab_content_v2,
                                           meTabSections: me_tab_sections_v2,
                                           meTabStats: me_tab_stats_v2 }
        value.dig("mobile").except!("meTabContentV2", "meTabSectionsV2", "meTabStatsV2")
      end
      config.update(value: value.to_json)
    end
  end


  desc "Replace My-Shared-Cards key with Shared-With-Me and Shared-With-Team keys"
  task update_my_shared_cards_for_mobile: :environment do

    # 255 records in production for this query
    Config.where(name: "OrgCustomizationConfig").each do |config|
      value = config.parsed_value
      new_value = value.dig("mobile", "meTabV2", "meTabContent", "myContent")
      my_shared_hash = new_value["mobile/meTabV2/meTabContent/myContent/myShared"] if new_value.present?
      if my_shared_hash.present?

        # ---------------------------------------------------
        #   'mobile/meTabV2/meTabContent/myContent/myShared': {
        #     defaultLabel: 'Shared With Me',
        #     visible: false,
        #     index: 6
        #   }
        # Replaced the above json hash with the following hash in edc-web-sdk
        # ---------------------------------------------------
        #   'mobile/meTabV2/meTabContent/myContent/sharedWithMe': {
        #     defaultLabel: 'Shared With Me',
        #     visible: true,
        #     index: 6
        #   },
        #   'mobile/meTabV2/meTabContent/myContent/sharedWithTeam': {
        #     defaultLabel: 'Shared With Team',
        #     visible: true,
        #     index: 7
        #   }
        # ---------------------------------------------------
        # Need to do the same for already saved configuration
        ["Shared With Me", "Shared With Team"].each do |label|
          sub_key = label.gsub(/\s+/, "")
          sub_key[0] = sub_key[0].downcase
          new_value["mobile/meTabV2/meTabContent/myContent/#{sub_key}"] = my_shared_hash.dup
          new_value["mobile/meTabV2/meTabContent/myContent/#{sub_key}"]["defaultLabel"] = label
        end

        # Setting visible: true for all newly added tab configs and adjusting the index
        index_val = new_value.dig("mobile/meTabV2/meTabContent/myContent/myShared", "index") || 6
        ["sharedWithMe", "sharedWithTeam", "myPrivate", "myCompleted"].each do |sub_key|
          new_value["mobile/meTabV2/meTabContent/myContent/#{sub_key}"]["visible"] = true
          new_value["mobile/meTabV2/meTabContent/myContent/#{sub_key}"]["index"] = index_val
          index_val+=1
        end
        new_value.except!("mobile/meTabV2/meTabContent/myContent/myShared")
      end
      # ---------------------------------------------------
      # header: {
      #   headerBackgroundColor: {
      #     'mobile/header/headerBackgroundColor': {
      #       defaultLabel: 'headerBackgroundColor',
      #       visible: true,
      #       value: #454560,
      #       index: 0
      #   },
      # }
      # Removed 'headerBackgroundColor' key from the above json hash
      # ---------------------------------------------------
      # header: {
      #   'mobile/header/headerBackgroundColor': {
      #     defaultLabel: 'Header Background Color',
      #     visible: true,
      #     value: '#454560',
      #     index: 0
      #   }
      # }
      # ---------------------------------------------------
      header = value.dig("mobile", "header")
      if header.present? && header["headerBackgroundColor"].present?
        header['mobile/header/headerBackgroundColor'] = header["headerBackgroundColor"]['mobile/header/headerBackgroundColor']
        header.except!("headerBackgroundColor")
      end
      config.update(value: value.to_json)
    end
  end

  # meTabV2: {
  #   meTabStats : null
  #   meTabContent : null
  #   meTabSections : null
  # }
  # Removing such values which are corrupted data. Production corrupted data contains only those, where all 3 are null together
  desc "Remove meTabV2 if its subkeys have null values"
  task remove_meTabV2_if_null: :environment do
    #266 records on production
    Config.where(name: "OrgCustomizationConfig").each do |config|
      value = config.parsed_value
      mobile_meTabV2 = value.dig("mobile","meTabV2")
      # If all values are null, remove meTabV2 entirely
      if mobile_meTabV2.present? && mobile_meTabV2["meTabContent"].nil? && mobile_meTabV2["meTabSections"].nil? && mobile_meTabV2["meTabStats"].nil?
        value.dig("mobile").except!("meTabV2")
      end
      config.update(value: value.to_json)
    end
  end
end

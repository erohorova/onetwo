namespace :private_channel_ecl_source do
  desc "ecl_source belonging to private channel will be marked as private"
  task :update_source_visibility, [:organization_id] => [:environment] do |t, args|
    channels = Channel.where(is_private: true, organization_id: args[:organization_id])
    channels.each do |channel|
      ecl_source = EclSource.where(channel_id: channel.id)
      ecl_source.each do |source|
        source.private_content = true
        source.save        
      end
    end
  end #task end
end #namespace end

namespace :payment_notification do
  desc 'send_mails_to_users_who_have_already_subscribed_to_org'
  task send_mails_to_already_subscribed_users: :environment do

    UserSubscription.all.each do |user_subscription|
      PaymentNotificationJob.perform_later(user_subscription.id)
    end
    
  end  # task :send_mails_to_already_subscribed_users
end

namespace :add_permission do
  desc "This will add mark as private permission to role"
  task :mark_as_private_to_role, [:organization_id, :role_name, :permission_name, :description] => [:environment] do |t, args|
    role = Role.find_by({organization_id: args[:organization_id], default_name: args[:role_name]})
    role.role_permissions.find_or_create_by(name: args[:permission_name], description: args[:description]).enable!
  end #task end
end #namespace end
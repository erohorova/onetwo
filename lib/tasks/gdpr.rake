namespace :gdpr do
  desc "This will anonymize and soft delete a user and remove all its references"
  task :delete_user, [:destiny_user_id] => [:environment] do |t, args|
    user = User.find(args[:destiny_user_id])
    user_id = args[:destiny_user_id]

    #Admin users
    admins = User.where("organization_id=#{user.organization_id} and organization_role='admin' and id != #{user_id} and is_suspended = false and status = 'active'")
    #puts "Admin Users of Organization are"
    #admins.each do |admin|
    #  puts "#{admin.email} : #{admin.first_name} #{admin.last_name}(#{admin.id})"
    #end

    if admins.length == 0
      puts "No admins other than user. It may break some flow. Task will abort"
      abort()
    end

    puts "***********select one of following admins to replace user. enter id**********"
    admins.each do |admin|
      puts "#{admin.email} : #{admin.first_name} #{admin.last_name}(#{admin.id})"
    end
    answer = STDIN.gets.chomp.to_i
    admin = User.find(answer)
    puts "**********Selected admin is #{admin.first_name} #{admin.last_name}*********"

    if user and admin
      Gdpr.delete_user(user_id, admin.id)
    end #if user and admin end
  end #task end
end #namespace end

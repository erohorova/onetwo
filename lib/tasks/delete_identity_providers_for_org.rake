# can be used as follow

# To destroy IdentityProviders of given org's user than use following syntax.
# rake delete_identity_providers_for_org host_name=org_host_name
# example:
# rake delete_identity_providers_for_org host_name=stark

# To destroy IdentityProviders for number of users than use following syntax.
# rake delete_identity_providers_for_org user_id=user_ids
# example:
# rake delete_identity_providers_for_org user_id=184,57,138
task delete_identity_providers_for_org: :environment do
  desc 'removes identity_providers for a given orgs users or given users'
  host_name = ENV["host_name"]
  user_id   = ENV["user_id"]

  # finding org from host_name if host_name is present.
  organization = Organization.where(host_name: host_name).first if host_name.present?

  # fetching user ids of org if ENV["host_name"] present.
  user_ids = if organization.present?
    User.where(organization_id: organization.id).map(&:id)
  else
    user_id.split(',')
  end

  IdentityProvider.where(user_id: user_ids).each do |identity_provider|
    identity_provider.destroy
  end
end


namespace :one_time_tasks do
  desc 'change card type details for brainshark and slideshare contents'
  task change_card_type_for_brainshark_contents: :environment do
    # total number of cards : 1647
    # Put your task implementation HERE.
    Resource.joins(:cards).where("cards.ecl_metadata like '%brainshark%'").each do |resource|
      resource.update_columns(type: "Video")

      resource.cards.each do |card|
        card.update_columns(card_type: 'media', card_subtype: 'video')
        card.reindex
      end
    end
  end
end
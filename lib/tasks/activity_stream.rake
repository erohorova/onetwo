namespace :activity_stream do

	task :remove_hidden_completed_cards, [:organization_id] => [:environment] do |t, args|
  	# Run as rake activity_stream:remove_hidden_completed_cards[6]
  	puts "Deleting activity_streams created for hidden cards on organization_id ----> #{args[:organization_id]}"
  	ActivityStream.joins(:card)
  		.where(cards: {hidden: true, organization_id: args[:organization_id]}).delete_all
  		
  	puts "Deleted activity_streams created for hidden cards of organization_id ----> #{args[:organization_id]}"
  end

end
namespace :profile do
  desc 'Remove duplicates for external profile'
  task remove_duplicates_for_external_profile: :environment do
     # user_ids which have more than 1 profile
     duplicates_ids = Profile.group('user_id').having('COUNT(user_id) > 1').pluck(:user_id)
     duplicates_ids.each do |user_id|
      all_user_profiles = Profile.where(user_id: user_id).order(created_at: :desc)
      # get organization context for the user profiles
      organizations = all_user_profiles.pluck(:organization_id).uniq
      organizations.each do |org_id|
        # found latest created profile in scope of organization and user
        latest_profile = Profile.where(
          user_id: user_id,
          organization_id: org_id
        ).order(created_at: :desc).first
        # destroy all profiles except latest created
        Profile.where(
          user_id: user_id,
          organization_id: org_id
        ).where.not(id: latest_profile.id).destroy_all
      end
    end
  end
end

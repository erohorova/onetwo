namespace :migrate_user_avatars do
  desc 'move user profile pics to paperclip'
  task copy: :environment do

    User.find_each do |user|
      if !user.avatar_file_name and user.picture_url
        user.avatar = user.picture_url.gsub("http","https") # http links from mobile auth (cause redirect failures)

        begin
          user.save!
        rescue => e
          log.error("Unable to create avatar image: user_id=#{user.id} errors=#{user.errors.to_json} exception=#{e}")
        end
      end
    end

  end
end
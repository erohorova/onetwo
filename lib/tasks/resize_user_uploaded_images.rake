namespace :resize_user_upload_images do
  desc 'resize card images'
  task resize_filestack_images: :environment do
    # 18_722 Records with filestack NOT NULL
    cards = Card.where.not(filestack: nil)
    cards.find_in_batches(batch_size: 2000) do |cards|
      cards.each do |card|
        ResizeExistingFilestackImagesJob.perform_later(card_id: card.id)
      end
    end
  end

  desc 'resize resources which have user uploaded image'
  task resize_resource_whose_image_changed: :environment do
    # 22_039
    resources = Resource.joins(:cards).where("image_url LIKE 'https://cdn.filestackcontent.com%'")
    resources.find_in_batches(batch_size: 1000) do |resources|
      resources.each do |resource|
        ResizeExistingResourceImagesJob.perform_later(resource_id: resource.id)
      end
    end
  end
end

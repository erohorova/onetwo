namespace :one_time_tasks do
  # This rake will dismiss assignments with passed `cards_ids` for given `team_id`
  # and `organization_id`
  # Sample : organization_id = 1813, team_id = 3802, card_ids = 2500635 2602132 2627436 2627453
  # How to run: rake "one_time_tasks:dismiss_assignment[1813,3802,2500635 2602132 2627436 2627453]"
  desc "Dismiss an assignment"
  task :dismiss_assignment, [:organization_id, :team_id, :card_ids] => :environment do |_task, args|
    organization = Organization.find_by_id(args[:organization_id])
    team     = organization.teams.find_by_id(args[:team_id])
    card_ids = organization.cards.where(id: args[:card_ids].split(' ').map(&:to_i)).pluck(:id)

    TeamsUser.where(team_id: team.id)
    .find_in_batches(batch_size: 1000) do |teams_users|
      user_ids = teams_users.map(&:user_id)
      assignment_ids = Assignment.where(
        user_id: user_ids,
        assignable_id: card_ids,
        assignable_type: 'Card'
      ).where.not(
        state: 'dismissed'
      ).pluck(:id)

      # Note: This will not trigger any callbacks.
      # Make sure you check the callbacks before re-using this.
      Assignment.where(id: assignment_ids).update_all(
        state: 'dismissed',
        completed_at: nil,
        started_at: nil
      )
      puts "Dismissed #{assignment_ids.count}"

      # Register dismiss events
      puts "Registering dismiss events"
      record_card_assignment_dismissed_event(
        assignment_ids: assignment_ids,
        organization: organization
      )
      puts "Done registering dismiss events"
    end
  end

  def record_card_assignment_dismissed_event(
    assignment_ids: [], organization: nil
  )
    return if organization.nil? || assignment_ids.blank?

    Assignment.where(id: assignment_ids)
    .includes(:assignee, :assignable).each do |assignment|
      begin
        Analytics::MetricsRecorderJob.perform_later(
          recorder:        "Analytics::CardAssignmentMetricsRecorder",
          event:           "card_assignment_dismissed",
          actor:           Analytics::MetricsRecorder.user_attributes(assignment.assignee),
          org:             Analytics::MetricsRecorder.organization_attributes(organization),
          timestamp:       assignment.updated_at.to_i,
          assignee:        Analytics::MetricsRecorder.user_attributes(assignment.assignee),
          card:            Analytics::MetricsRecorder.card_attributes(assignment.assignable),
          additional_data: {}
        )
      rescue exception => e
        log.debug "Failed to push dismiss event for Assignment #{assignment.id}"
        log.debug "Error message: #{e.message}"
      end
    end
  end
end

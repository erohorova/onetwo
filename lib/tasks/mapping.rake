require 'csv'

namespace :competency_mapping do
  def parse_name_from_source(url)
    begin
      url_splits = URI.parse(url).host.split('.')
      url_splits.each do |key|
        next if key.downcase === 'www'
        return key.downcase
      end
    rescue => ex
      log.info "Exception occurred while parsing url: #{url}"
    end
  end

  task :seed => :environment do
    desc "map CSV on S3 to database"

    # Create new S3 client
    s3 = AWS::S3::Client.new(
      region: "us-east-1",
      access_key_id: "#{Settings.aws_access_key_id}",
      secret_access_key: "#{Settings.aws_secret_access_key}"
    )

    # Get all objects
    log.info "Getting list of CSV files..."
    resp = s3.list_objects(
      bucket_name: "#{Settings.aws_competency_mapping_bucket}"
    )

    # Loop through each object
    resp[:contents].each do |map|

      # Get the contents of the object
      log.info "Mapping " + map[:key] + " file..."
      data = s3.get_object(
        bucket_name: "#{Settings.aws_competency_mapping_bucket}",
        key: map[:key]
      )

      # Read and parse the CSV
      res = CSV.parse(data[:data], :headers => true)
      headers = res.headers.compact

      @ind = nil
      @jr = nil
      @comp = nil

      # Read each row mapped to headers
      res.each_with_index do |row, i|
        # hash = row.to_hash
        (0...headers.length).each do |j|
          column_header = headers[j]
          column_value = row[j]
          #log.info "Row #{i}: Header #{column_header} is #{column_value}"

          # Add content sources
          if column_header.strip == "Source" && column_value
            log.info "Adding source: #{column_value}"
            if @ind
              begin
                source = Cms::ContentSource.rss_link_from_source(column_value)
                log.error "RSS LINK NOT FOUND: #{column_value}" unless source

                content_source = Cms::ContentSource.find_or_create_by(source: source)
                if !@ind.cms_content_sources.include?(content_source) && content_source && content_source.id
                  log.info "CMS content source #{content_source.inspect}"
                  name_column_value = row[j+1] #get name of the source
                  #Try to parse it
                  name_column_value = unless name_column_value
                    parse_name_from_source(column_value)
                  end
                  name_column_value = source unless name_column_value

                  csi = Cms::ContentSourcesIndustry.new(industry: @ind, cms_content_source: content_source,
                                                            name: name_column_value, display_source: column_value)

                  unless csi.save
                    log.error "Could not save content source for: #{source}"
                  end
                end
              rescue => ex
                log.error "Exception occurred while save industry content source #{column_value}"
              end
            end
          end

          # Add topics/interests/tags
          if column_header.strip == "Keywords/Topics" && column_value
            # log.info "Topic -> " + column_value
            @comp.add_topics([column_value])
          end

        end
      end

      log.info "Finished Mapping: " + map[:key]
    end

    log.info "Finished."
  end
end
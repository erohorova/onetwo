namespace :after_party do
  desc 'Deployment task: fill_up_organization_ids_for_search_query'
  task fill_up_organization_ids_for_search_query: :environment do
    puts "Running deploy task 'fill_up_organization_ids_for_search_query'"

    # Put your task implementation HERE.
    SearchQuery.all.each do |search_query|
      user_organization_id = User.find_by(id: search_query.user_id).try(:organization_id)
      search_query.update(organization_id: user_organization_id)
    end
    SearchQuery.gateway.refresh_index!

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20161129090201'
  end  # task :fill_up_organization_ids_for_search_query
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: everyone_group_to_bool'
  task everyone_group_to_bool: :environment do
    puts "Running deploy task 'everyone_group_to_bool'"

    # Put your task implementation HERE.
    Team.where(name: 'Everyone').update_all(is_everyone_team: true)

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160922000452'
  end  # task :everyone_group_to_bool
end  # namespace :after_party

require "open-uri"
namespace :after_party do
  desc 'Deployment task: populate badges for each org -onetime only'
  task badges_population: :environment do
    puts "Running deploy task 'badges_population'"

    # Put your task implementation HERE.
    unless Badge.any? #one time process only
      image_urls = ['https://d1iwkfmdo6oqxx.cloudfront.net/badges/Badge25.png', 'https://d1iwkfmdo6oqxx.cloudfront.net/badges/Badge50.png', 'https://d1iwkfmdo6oqxx.cloudfront.net/badges/Badge75.png', 'https://d1iwkfmdo6oqxx.cloudfront.net/badges/Badge100.png'] #default image urls here
      image_urls.each do |image|
        orgs = Organization.all
        orgs.each do |org|
          org.card_badges.create(image: open(image))
        end
      end
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20171006074019'
  end  # task :badges_population
end  # namespace :after_party

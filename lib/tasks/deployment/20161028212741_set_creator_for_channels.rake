namespace :after_party do
  desc 'Deployment task: set_creator_for_channels'
  task set_creator_for_channels: :environment do
    puts "Running deploy task 'set_creator_for_channels'"

    # Put your task implementation HERE.
    Organization.all.find_in_batches(batch_size: 10).each do |orgs|
      orgs.each do |org|
        #fetch org admin
        admin = org.admins.first
        org.channels.each do |channel|
          # fetch first collaborator
          first_collaborator = channel.channels_authors.first.try(:user)
          creator = first_collaborator || admin
          channel.update(creator: creator)
        end
      end
    end


    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20161028212741'
  end  # task :set_creator_for_channels
end  # namespace :after_party

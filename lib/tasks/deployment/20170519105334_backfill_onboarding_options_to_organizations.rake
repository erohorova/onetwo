namespace :after_party do
  desc 'Deployment task: backfill_onboarding_options_to_organizations'
  task backfill_onboarding_options_to_organizations: :environment do
    puts "Running deploy task 'backfill_onboarding_options_to_organizations'"

    # Put your task implementation HERE.
    Organization.where("host_name != 'www'").find_each do |organization|
      organization.send(:add_onboarding_options)
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170519105334'
  end  # task :backfill_onboarding_options_to_organizations
end  # namespace :after_party

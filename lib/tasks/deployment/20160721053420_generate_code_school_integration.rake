namespace :after_party do
  desc 'Deployment task: generate_code_school_integration'
  task generate_code_school_integration: :environment do
    puts "Running deploy task 'generate_code_integration'"

    CODE_SCHOOL = {
      "name"=>"Code School",
      "description"=>"<p>Import completed courses from Code School.<br/><a href='https://www.codeschool.com/'>https://www.codeschool.com/users/<b>code_school_username</b>.json</a><br/>Note: Requires a public Treehouse profile.</p>",
      "enabled"=>true,
      "logo"=> File.open(Rails.root.join("app/assets/images", "codeschool_logo.jpg")),
      "fields" => [{"name":"code_school_user_name","label":"Code School Username","placeholder":"Code school username"}].to_json
    }
    Integration.create(CODE_SCHOOL)
    AfterParty::TaskRecord.create version: '20160721053420'
  end  # task :generate_code_integration
end  # namespace :after_party

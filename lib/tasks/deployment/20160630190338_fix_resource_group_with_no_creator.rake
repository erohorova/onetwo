namespace :after_party do
  desc 'Deployment task: set_client_user_as_creator_of_resource_group_if_doesnt_exist'
  task fix_resource_group_with_no_creator: :environment do
    puts "Running deploy task 'fix_resource_group_with_no_creator'"

    # Put your task implementation HERE.
    groups = ResourceGroup.where creator_id: nil
    admin_user = User.where(email: 'admin@course-master.com').first

    groups.each do |group|
      creator = group.client.try(:user) || admin_user
      group.update creator: creator
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160630190338'
  end  # task :fix_resource_group_with_no_creator
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: update_resource_image_url'
  task update_resource_image_url: :environment do
    puts "Running deploy task 'update_resource_image_url'"

    # Put your task implementation HERE.

    # number of records: 7550
    Resource.joins(:cards).where(cards: {card_type: 'course'},resources:{image_url: nil}).each do |resource|
      image_index = (resource.created_at.to_i) % 100 || 0
      stock_image = CARD_STOCK_IMAGE[image_index][:url] || CARD_STOCK_IMAGE[0][:url]
      resource.update_attribute(:image_url, stock_image)
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180821071504'
  end  # task :update_resource_image_url
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: reindex_users_custom_fields_data'
  task reindex_users_custom_fields_data: :environment do
    puts "Running deploy task 'reindex_users_custom_fields_data'"

    # Put your task implementation HERE.
    User.not_suspended.joins(:assigned_custom_fields).distinct.find_in_batches do |users|
      users.each do |user|
        begin
          user.reindex
        rescue => e
          log.error "Unable to reindex user with id: #{user.id} with Exception: #{e.message}"
        end
      end
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180904125531'
  end  # task :reindex_users_custom_fields_data
end  # namespace :after_party

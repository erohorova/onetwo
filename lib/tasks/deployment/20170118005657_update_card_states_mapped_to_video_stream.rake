namespace :after_party do
  desc 'Deployment task: update_card_states_mapped_to_video_stream'
  task update_card_states_mapped_to_video_stream: :environment do
    puts "Running deploy task 'update_card_states_mapped_to_video_stream'"

    # Put your task implementation HERE.
    VideoStream.find_in_batches(batch_size: 100) do |group|
    	group.each do |video_stream|
    		video_stream.send(:update_card_state) if video_stream.card
    	end
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170118005657'
  end  # task :update_card_states_mapped_to_video_stream
end  # namespace :after_party

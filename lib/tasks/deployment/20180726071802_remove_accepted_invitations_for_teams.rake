namespace :after_party do
  desc 'Deployment task: remove_accepted_invitations_for_teams'
  task remove_accepted_invitations_for_teams: :environment do
    puts "Running deploy task 'remove_accepted_invitations_for_teams'"

    invitations = Invitation.where(invitable_type: 'Team').where.not(accepted_at: nil)

    invitations.find_in_batches do |batch|

      team_ids = batch.map(&:invitable_id).uniq
      user_ids = batch.map(&:recipient_id).uniq
      user_ids.map! do |id|
        id unless TeamsUser.where(team_id: team_ids).pluck(:user_id).include?(id)
      end
      invitations.where(recipient_id: user_ids.compact).destroy_all
    end


    AfterParty::TaskRecord.create version: '20180726071802'
  end  # task :remove_accepted_invitations_for_teams
end  # namespace :after_party

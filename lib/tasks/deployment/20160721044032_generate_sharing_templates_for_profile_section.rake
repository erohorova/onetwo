namespace :after_party do
  desc 'Deployment task: generate_sharing_templates_for_profile_section'
  task generate_sharing_templates_for_profile_section: :environment do
    puts "Running deploy task 'generate_sharing_templates_for_profile_section'"

    Config.where(category: "profile").delete_all
    templates = {
        "section/continous_learning/course_taken" => "{{user_first_name}} has taken {{course_name1}} and {{course_name2}} as online courses. Check out his profile to learn more.",
        "section/continous_learning/smartbite_score_increased" => "{{user_first_name}}'s Smartbite Score has increased by x %. Check out his profile to see what is he learning",
        "section/continous_learning/added_new_integration" => "{{user_first_name}} is taking an online course on {{provider_name}}. View his profile for more info.",
        "section/continous_learning/added_new_integrations" => "{{user_first_name}} is learning online on {{providers_name}}. View his profile for more info.",
        "section/continous_learning/added_new_course" => "{{user_first_name}} is taking an online course on {{course_name}}. Check out his profile.",
        "section/continous_learning/added_new_courses" => "{{user_first_name}} is taking courses on {{courses_name}}. Check out his profile.",
      
        "section/continous_learning/summary_30" => "{{user_first_name}} is stepping up his/her game with Continuous Learning by taking {{course_count}} courses in the last month. Check out his profile to learn more.",
        "section/continous_learning/summary_60" => "{{user_first_name}} is stepping up his/her game with Continuous Learning by taking {{course_count}} courses in the 2 months. Check out his profile to learn more.",
        "section/continous_learning/summary_90" => "{{user_first_name}} is stepping up his/her game with Continuous Learning by taking {{course_count}} courses in the last 3 months. Check out his profile to learn more.",
      
        "section/competencies/added_new_competency" => "{{user_first_name}} picked up a new skill on {{competency}}. Check out his profile to learn more.",
        "section/competencies/added_new_competencies" => "{{user_first_name}} picked up new skills on  {{competencies}}. Check out his profile to learn more.",
        "section/competencies/summary_30" => "{{user_first_name}} picked up {{competencies_count}} new skills including {{competencies}}. Check out his profile.",
        "section/competencies/summary_60" => "{{user_first_name}} picked up {{competencies_count}} new skills including {{competencies}}. Check out his profile.",
        "section/competencies/summary_90" => "{{user_first_name}} picked up {{competencies_count}} new skills including {{competencies}}. Check out his profile.",

        "section/education/summary" => "{{user_first_name}} updated his Education history. Checkout his profile for more info."
      }

    templates.each do |k, v|
      Config.create(category: "profile", name: k, value: v, configable: Organization.default_org)
    end
    AfterParty::TaskRecord.create version: '20160721044032'
  end  # task :generate_sharing_templates_for_profile_section
end  # namespace :after_party

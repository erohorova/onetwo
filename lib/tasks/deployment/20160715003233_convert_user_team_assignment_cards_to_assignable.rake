namespace :after_party do
  desc 'Deployment task: convert_pathway_to_assignable'
  task convert_user_team_assignment_cards_to_assignable: :environment do
    puts "Running deploy task 'convert_user_team_assignment_cards_to_assignable'"

    # Put your task implementation HERE.
    # UserTeamAssignment.all.each do |uta|
    #   card = Card.where(id: uta.card_id).first
    #   if card.nil?
    #     puts "Card with #{uta.card_id} was not found"
    #   else
    #     uta.update assignable: card, assignor: card.organization.admins.first
    #   end
    # end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160715003233'
  end  # task :convert_user_team_assignment_cards_to_assignable
end  # namespace :after_party

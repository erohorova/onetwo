namespace :after_party do
  desc 'Deployment task: reindex_cards'
  task reindex_cards: :environment do
    puts "Running deploy task 'reindex_cards'"

    # Put your task implementation HERE.
    Card.reindex

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160909005409'
  end  # task :reindex_cards
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: add_author_id_to_pathway'
  task add_author_id_to_pathway: :environment do
    puts "Running deploy task 'add_author_id_to_pathway'"

    # Put your task implementation HERE.
    Card.where(author_id: nil, card_type: 'pack').each do |card|
      admin_user = User.where(organization_id: card.organization_id, is_edcast_admin: true, organization_role: 'admin').first!
      card.update(author_id: admin_user.id)
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180406073739'
  end  # task :add_author_id_to_pathway
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: remove_permissions_manage_channels_and_related_permissions_for_sub_admin_and_group_sub_dmin'
  task remove_permissions_with_channels_for_sub_admin: :environment do
    puts "Running deploy task 'remove_permissions_with_channels_for_sub_admin'"

    permissions = %w[MANAGE_CHANNELS MANAGE_GROUP_CHANNELS MANAGE_CHANNEL_ANALYTICS]

    RolePermission.where(name: permissions).find_in_batches do |ids|
      RolePermission.where(id: ids).destroy_all
    end

    AfterParty::TaskRecord.create version: '20180816100807'
  end  # task :remove_permissions_with_channels_for_sub_admin
end  # namespace :after_party

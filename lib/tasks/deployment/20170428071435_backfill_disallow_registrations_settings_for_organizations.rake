namespace :after_party do
  desc 'Deployment task: backfill_disallow_registrations_settings_for_organizations'
  task backfill_disallow_registrations_settings_for_organizations: :environment do
    puts "Running deploy task 'backfill_disallow_registrations_settings_for_organizations'"

    # Put your task implementation HERE.

    # We are deprecating is_registrable column in organization table
    # Now onwards we will use AllowedEmailDomains Organization settings to check
    # 
    # possible values for this setting
    #
    # 'none' => disallow all registrations
    # 'all'  => allow all domain registrations
    # 'edcast.com,spark.com' => whitelisted domains

    Organization.where(is_registrable: false).each do |org|
      config = Config.find_or_initialize_by(
        configable: org,
        name: 'AllowedEmailDomains',
        description: "Allowed email domains for registration",
        data_type: "string"
      )

      config.update(value: 'none')
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170428071435'
  end  # task :backfill_disallow_registrations_settings_for_organizations
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: populate_enable_people_search'
  task set_default_enable_people_search: :environment do
    puts "Running deploy task 'set_default_enable_people_search'"

    # Put your task implementation HERE.
    CustomField.where(enable_people_search: nil).update_all(enable_people_search: false)

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180525132303'
  end  # task :set_default_enable_people_search
end  # namespace :after_party

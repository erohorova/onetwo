namespace :after_party do
  desc 'Deployment task: make_team_admins_leaders_of_everyone_group'
  task make_team_admins_leaders_of_everyone_group: :environment do
    puts "Running deploy task 'make_team_admins_leaders_of_everyone_group'"
    # Put your task implementation HERE.
    Organization.all.each do |org|
      everyone_team = org.everyone_team
      unless everyone_team.nil?
        org.admins.each { |admin| everyone_team.add_admin(admin) }
      end
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20161227191856'
  end  # task :make_team_admins_leaders_of_everyone_group
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: learning_queue_deletion_cleanup'
  task learning_queue_deletion_cleanup: :environment do
    puts "Running deploy task 'learning_queue_deletion_cleanup'"

    # Put your task implementation HERE.
    LearningQueueItem.active.find_each do |lqi|
      queueable = lqi.queueable
      if queueable.nil? || queueable.state != "published"
        lqi.mark_as_deleted
      end
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20161103182437'
  end  # task :learning_queue_deletion_cleanup
end  # namespace :after_party

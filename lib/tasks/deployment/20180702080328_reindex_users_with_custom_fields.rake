namespace :after_party do
  desc 'Deployment task: reindex_users_with_custom_fields'
  task reindex_users_with_custom_fields: :environment do
    puts "Running deploy task 'reindex_users_with_custom_fields'"

    # Put your task implementation HERE.
    User.not_suspended.completed_profile.joins(:assigned_custom_fields).distinct.each(&:reindex)

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180702080328'
  end  # task :reindex_users_with_custom_fields
end  # namespace :after_party

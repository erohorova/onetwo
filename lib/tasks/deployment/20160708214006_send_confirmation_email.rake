namespace :after_party do
  desc 'Deployment task: latest_eft_admins_send_confirmation_email'
  task send_confirmation_email: :environment do
    puts "Running deploy task 'send_confirmation_email'"

    # Put your task implementation HERE.

    orgs = Organization.where(created_at: 6.weeks.ago..Time.zone.now)
    orgs.each do |org|
      org.admins.each do |admin|
        if org.status.eql?('active') && !admin.confirmed? && !admin.email.nil?
          admin.send_confirmation_instructions
        end
      end
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160708214006'
  end  # task :send_confirmation_email
end  # namespace :after_party

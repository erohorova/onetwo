namespace :after_party do
  desc 'Deployment task: At present some cards have created_at as nil this task will update the created_at with the value in updated_at'
  task update_cards_created_at_date_where_it_is_nil: :environment do
    puts "Running deploy task 'update_cards_created_at_date_where_it_is_nil'"

    # Put your task implementation HERE.
    cards_without_created_at = Card.where(created_at: nil)
    puts "Total no.of cards without created_at date are #{cards_without_created_at.count}"

    #updating all such card's 'created_at' dat with their 'updated_at' date.
    cards_without_created_at.each do |card|
      card.update_column(:created_at, card.updated_at)
      card.reindex
    end

    #checking after running task if any such cards still exists.
    cards_without_created_at_after_task = Card.where(created_at: nil)
    puts "Total no.of cards without created_at date after running task is #{cards_without_created_at_after_task.count}"

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160830051848'
  end  # task :update_cards_created_at_date_where_it_is_nil
end  # namespace :after_party

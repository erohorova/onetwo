namespace :after_party do
  desc 'Deployment task: clean_team_invitation_of_already_accepted_users'
  task clean_team_invitation_of_already_accepted_users: :environment do
    puts "Running deploy task 'clean_team_invitation_of_already_accepted_users'"

    # Put your task implementation HERE.
    # production database has around 3k team invitation records which are accepted by user.
    Invitation.where(invitable_type: 'Team').where('accepted_at is NOT NULL').destroy_all

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180904134124'
  end  # task :clean_team_invitation_of_already_accepted_users
end  # namespace :after_party

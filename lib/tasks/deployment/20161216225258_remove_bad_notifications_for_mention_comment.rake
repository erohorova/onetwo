namespace :after_party do
  desc 'Deployment task: remove_bad_notifications_for_mention_comment'
  task remove_bad_notifications_for_mention_comment: :environment do
    puts "Running deploy task 'remove_bad_notifications_for_mention_comment'"

    # Put your task implementation HERE.
    to_be_deleted = []
    Notification.where(notifiable_type: "mention_in_comment").find_each do |notif|
      unless Comment.where(id: notif.notifiable_id).present?
        to_be_deleted << notif.id
      end
    end
    Notification.where(id: to_be_deleted).destroy_all
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20161216225258'
  end  # task :remove_bad_notifications_for_mention_comment
end  # namespace :after_party

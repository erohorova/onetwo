namespace :after_party do
  desc 'Deployment task: propagate_cards_to_ecl'
  task propagate_cards_to_ecl: :environment do
    puts "Running deploy task 'propagate_cards_to_ecl'"

    # Put your task implementation HERE.

    default_org = Organization.default_org

    card_ids = Card.where("state = 'published' AND ecl_id IS NULL AND organization_id <> ? and ecl_id is null", default_org.id).where("author_id is not null").select("id").pluck(:id)
    # card_ids.map {|card_id| EclCardPropagationJob.perform_later(card_id) }
    default_card_ids = Card.where("organization_id = ?",default_org.id).pluck(:id)

    votable_card_ids = Vote.where("votable_type = ?",'Card').where("votable_id not in (?)",card_ids+default_card_ids).select("distinct votable_id").pluck(:votable_id)
    # votable_card_ids.map {|card_id| EclCardPropagationJob.perform_later(card_id)}
    card_ids += votable_card_ids 
    # card_ids.uniq!

    comment_card_ids  = Comment.where("commentable_type = ?",'Card').where("commentable_id not in (?)",card_ids+default_card_ids).select("distinct commentable_id").pluck(:commentable_id)
    # comment_card_ids.map {|card_id| EclCardPropagationJob.perform_later(card_id)}
    card_ids += comment_card_ids
    # card_ids.uniq!

    assignment_card_ids = Assignment.where("assignable_type = ?",'Card').where("assignable_id not in (?)",card_ids+default_card_ids).select("distinct assignable_id").pluck(:assignable_id)
    # assignment_card_ids.map {|card_id| EclCardPropagationJob.perform_later(card_id)}
    card_ids += assignment_card_ids
    # card_ids.uniq!

    bookmark_card_ids = Bookmark.where("bookmarkable_type = ?",'Card').where("bookmarkable_id not in (?)",card_ids+default_card_ids).select("distinct bookmarkable_id").pluck(:bookmarkable_id)
    # bookmark_card_ids.map {|card_id| EclCardPropagationJob.perform_later(card_id)}
    card_ids += bookmark_card_ids
    Card.where("id in (?)",card_ids).where("ecl_id is null").pluck(:id).map {|card_id|EclCardPropagationJob.perform_later(card_id)}
    

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20161229084631'
  end  # task :propagate_cards_to_ecl
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: create_config_for_leaderboard_for_all_orgs'
  task create_config_for_leaderboard_for_all_orgs: :environment do
    puts "Running deploy task 'create_config_for_leaderboard_for_all_orgs'"

    # Put your task implementation HERE.
    # 1847 organizations in production
    Organization.find_in_batches do |batch|
      batch.each do |organization|
        Config.find_or_create_by(name: 'leaderboard', data_type: 'boolean', value: 'true', configable: organization)
      end
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180821083654'
  end  # task :create_config_for_leaderboard_for_all_orgs
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: fix_bad_pathway_covers'
  task fix_bad_pathway_covers: :environment do
    puts "Running deploy task 'fix_bad_pathway_covers'"

    # Put your task implementation HERE.
    Card.where(card_type: 'pack').where.not(card_subtype: 'simple').update_all(card_subtype: 'simple')


    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170324212905'
  end  # task :fix_bad_pathway_covers
end  # namespace :after_party

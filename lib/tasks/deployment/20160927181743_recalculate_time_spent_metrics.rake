namespace :after_party do
  desc 'Deployment task: recalculate_time_spent_metrics'
  task recalculate_time_spent_metrics: :environment do
    puts "Running deploy task 'recalculate_time_spent_metrics'"

    # Put your task implementation HERE.
    UserSession.delete_all
    User.where(is_complete: true).where.not(organization_id: Organization.default_org.id).find_each do |user|
      MetricRecord.reaggregate_time_spent_metrics(user.id)
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160927181743'
  end  # task :recalculate_time_spent_metrics
end  # namespace :after_party

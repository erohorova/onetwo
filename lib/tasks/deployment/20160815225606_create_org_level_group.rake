namespace :after_party do
  desc 'Deployment task: create_org_level_group'
  task create_org_level_group: :environment do
    puts "Running deploy task 'create_org_level_group'"

    # Put your task implementation HERE.
    Organization.where.not(id: Organization.default_org.id).each do |org|
      org.send(:create_org_level_team)
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160815225606'
  end  # task :create_org_level_group
end  # namespace :after_party

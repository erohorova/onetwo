namespace :after_party do
  desc 'Deployment task: add_state_to_video_streams_fixed'
  task add_state_to_video_streams_fixed: :environment do
    puts "Running deploy task 'add_state_to_video_streams_fixed'"

    # Put your task implementation HERE.
    VideoStream.where(state: nil).find_each do |v|
      begin
        v.update_attributes(state: 'published')
      rescue => e
        log.error e.inspect
        v.update_columns(state: 'published')
        v.reindex
      end
    end


    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160505185951'
  end  # task :add_state_to_video_streams_fixed
end  # namespace :after_party

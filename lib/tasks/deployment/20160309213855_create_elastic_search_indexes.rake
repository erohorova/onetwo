namespace :after_party do
  desc 'Deployment task: create_elastic_search_indexes'
  task create_elastic_search_indexes: :environment do
    puts "Running deploy task 'create_elastic_search_indexes'"

    # Put your task implementation HERE.
    # Create all required ES indexes
    ActivityRecord.create_index!
    Group.searchkick_create_index
    MetricRecord.create_index!
    PostRead.create_index!
    Post.searchkick_create_index
    SearchQuery.create_index!
    UserContentsQueue.create_index!

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160309213855'
  end  # task :create_elastic_search_indexes
end  # namespace :after_party

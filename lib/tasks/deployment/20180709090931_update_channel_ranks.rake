namespace :after_party do
  desc 'Deployment task: update_channel_ranks'
  task update_channel_ranks: :environment do
    puts "Running deploy task 'update_channel_ranks'"

    # we are not reseting channels_cards rank upto 20 as they may be ordered by curator.
    ActiveRecord::Base.connection.execute("UPDATE channels_cards SET rank = NULL where rank > 20;")

    # Put your task implementation HERE.

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180709090931'
  end  # task :update_channel_ranks
end  # namespace :after_party

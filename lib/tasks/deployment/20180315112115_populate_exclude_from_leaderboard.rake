namespace :after_party do
  desc 'Deployment task: populate_exclude_from_leaderboard'
  task populate_exclude_from_leaderboard: :environment do
    puts "Running deploy task 'populate_exclude_from_leaderboard'"

    User.where(exclude_from_leaderboard: nil).update_all(exclude_from_leaderboard: false)

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180315112115'
  end  # task :populate_exclude_from_leaderboard
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: Fetch users with &amp; and fix it'
  task unescape_html_for_users: :environment do
    puts "Running deploy task 'unescape_html_for_users'"

    # Fetch users having &amp; in their first_name, last_name or bio.
    # Total 269 users are there on production.
    users = User.where("first_name like '%&amp;%' or last_name like '%&amp;%' or bio like '%&amp;%'")
    users.each do |user|
      user.send :strip_all_tags_from_user_input
      user.save
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20181126164422'
  end  # task :unescape_html_for_users
end  # namespace :after_party

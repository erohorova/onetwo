namespace :after_party do
  desc 'Deployment task: populate_empty_message_for_ecl_card'
  task populate_empty_message_for_ecl_card: :environment do
    puts "Running deploy task 'populate_empty_message_for_ecl_card'"

    Card.where("message IS NULL or message = ''").
      where("title IS NOT NULL and ecl_id IS NOT NULL").
      where.not(organization_id: Organization.default_org.id).
      find_in_batches do |cards|
        cards.each do |card|
          card.update_attributes(message: card.title)
        end  
      end

    AfterParty::TaskRecord.create version: '20170301063101'
  end  # task :populate_empty_message_for_ecl_card
end  # namespace :after_party

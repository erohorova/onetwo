namespace :after_party do
  desc 'Deployment task: reindex_teams'
  task reindex_teams: :environment do
    puts "Running deploy task 'reindex_teams'"

    # Put your task implementation HERE.
    Team.where.not(organization_id: Organization.default_org.id).each do |team|
      team.reindex
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180607070248'
  end  # task :reindex_teams
end  # namespace :after_party

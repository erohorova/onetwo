namespace :after_party do
  desc 'Deployment task: add_org_members_to_everyone_group'
  task add_org_members_to_everyone_group: :environment do
    puts "Running deploy task 'add_org_members_to_everyone_group'"

    # Put your task implementation HERE.
    Organization.where.not(host_name: 'www').each do |org|
      group = org.everyone_team
      if group.nil?
        org.send(:create_org_level_team)
        group = org.everyone_team
      end

      log.info "Begin adding group members for org: #{org.host_name}"
      org.members.find_in_batches(batch_size: 500) do |users|
        users.each {|u| group.add_user(u, as_type: 'member') unless group.members.include?(u)}
      end
      log.info "Done adding group members for #{org.host_name}"
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160819184212'
  end  # task :add_org_members_to_everyone_group
end  # namespace :after_party

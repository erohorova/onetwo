namespace :after_party do
  desc 'Deployment task: backfill_bookmarks_count'
  task backfill_bookmarks_count: :environment do
    puts "Running deploy task 'backfill_bookmarks_count'"

    # Put your task implementation HERE.
    Bookmark.where(bookmarkable_type: 'Card').
      select("DISTINCT(bookmarkable_id), bookmarkable_type, id").
      find_each do |bookmark|
        card = bookmark.bookmarkable
        card && Card.reset_counters(card.id, :bookmarks)
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170428105556'
  end  # task :backfill_bookmarks_count
end  # namespace :after_party

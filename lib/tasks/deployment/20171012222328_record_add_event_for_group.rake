namespace :after_party do
  desc 'Deployment task: record_add_event_for_group'
  task record_add_event_for_group: :environment do
    puts "Running deploy task 'record_add_event_for_group'"

    # Put your task implementation HERE.
    # 65,332 records
    TeamsUser.joins(team: :organization)
      .where("organization_id != ?", Organization.default_org.id)
      .where(teams: {is_everyone_team: false})
      .find_in_batches(batch_size: 1000) do |teams_users|
        teams_users.each {|tu| tu.send(:record_group_event, {event: 'group_user_added', timestamp: tu.created_at})}
      end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20171012222328'
  end  # task :record_add_event_for_group
end  # namespace :after_party

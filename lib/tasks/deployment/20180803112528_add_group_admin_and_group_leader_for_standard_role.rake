namespace :after_party do
  desc 'Deployment task: create_group_admin_and_group_leader_roles'
  task add_group_admin_and_group_leader_for_standard_role: :environment do
    puts "Running deploy task 'add_group_admin_and_group_leader_for_standard_role'"

    Organization.all.each do |org|
      role_group_leader = Role.create(
          organization: org, default_name: 'group_leader',
          name: 'group_leader', master_role: true
      )
      role_group_leader.create_role_permissions

      role_group_admin = Role.create(
          organization: org, default_name: 'group_admin',
          name: 'group_admin', master_role: true
      )
      role_group_admin.create_role_permissions
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180803112528'
  end  # task :add_group_admin_and_group_leader_for_standard_role
end  # namespace :after_party

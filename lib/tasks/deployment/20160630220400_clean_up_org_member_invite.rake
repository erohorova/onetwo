namespace :after_party do
  desc 'Deployment task: remove_duplicate_invites'
  task clean_up_org_member_invite: :environment do
    puts "Running deploy task 'clean_up_org_member_invite'"

    # Put your task implementation HERE.
    orgs = Organization.find_in_batches(batch_size: 10) do |orgs|
      orgs.each do |org|
        invitations = Invitation.where(invitable: org, accepted_at: nil)
        user_emails = org.users.where(email: invitations.pluck(:recipient_email)).map(&:email)
        invitations.where(recipient_email: user_emails).update_all(accepted_at: Time.now)
      end

    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160630220400'
  end  # task :clean_up_org_member_invite
end  # namespace :after_party

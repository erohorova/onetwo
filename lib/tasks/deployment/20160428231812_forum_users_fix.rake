namespace :after_party do
  desc 'Deployment task: forum_users_fix'
  task forum_users_fix: :environment do
    puts "Running deploy task 'forum_users_fix'"

    # Put your task implementation HERE.

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    User.where.not(organization_id: Organization.default_org.id).
      where.not(email: nil).
      where(parent_user_id: nil).
      find_each do |user|
      unless user.organization.client.nil?
        forum_user = ClientsUser.joins(:user).where(users: {email: user.email}, 
                                                    client: user.organization.client).first.try(:user)
        if forum_user
          user.update_with_reindex_async(parent_user_id: forum_user.id)
        end
      end
    end
    AfterParty::TaskRecord.create version: '20160428231812'
  end  # task :forum_users_fix
end  # namespace :after_party

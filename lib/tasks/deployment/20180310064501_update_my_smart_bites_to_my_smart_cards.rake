namespace :after_party do
  desc 'Deployment task: update_my_smart_bites_to_my_smart_cards'
  task update_my_smart_bites_to_my_smart_cards: :environment do
    puts "Running deploy task 'update_my_smart_bites_to_my_smart_cards'"

    # Put your task implementation HERE.
    Config.where(name: "OrgCustomizationConfig").each do |config|
      value = ActiveSupport::JSON.decode(config.value)
      if value["mobile"]["meTabContent"].present? && value["mobile"]["meTabContent"]["mobile/meTabContent/mySmartbites"].present?
        value["mobile"]["meTabContent"]["mobile/meTabContent/mySmartbites"]["defaultLabel"] = "My SmartCards" 
        config.update_attributes(value: value.to_json)
      end  
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180310064501'
  end  # task :update_my_smart_bites_to_my_smart_cards
end  # namespace :after_party
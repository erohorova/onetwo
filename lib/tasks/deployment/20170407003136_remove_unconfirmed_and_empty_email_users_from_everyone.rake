namespace :after_party do
  desc 'Deployment task: remove_unconfirmed_and_empty_email_users_from_everyone'
  task remove_unconfirmed_and_empty_email_users_from_everyone: :environment do
    puts "Running deploy task 'remove_unconfirmed_and_empty_email_users_from_everyone'"

    # Put your task implementation HERE.
    # About 59305 records
    TeamsUser.joins(:user, :team).where("users.confirmed_at IS NULL OR users.email IS NULL AND is_everyone_team = ? AND users.organization_role != 'admin'", true).destroy_all

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170407003136'
  end  # task :remove_unconfirmed_and_empty_email_users_from_everyone
end  # namespace :after_party

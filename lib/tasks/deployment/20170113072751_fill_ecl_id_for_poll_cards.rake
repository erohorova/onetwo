namespace :after_party do
  desc 'Deployment task: fill_ecl_id_for_poll_cards'
  task fill_ecl_id_for_poll_cards: :environment do
    puts "Running deploy task 'fill_ecl_id_for_poll_cards'"
    default_org = Organization.default_org
    cards = Card.where("state = 'published' AND ecl_id IS NULL AND author_id is not null AND organization_id <> ? and card_type = 'poll'", default_org.id)

    cards.each do |card|
      result = EclApi::CardConnector.new.propagate_card_to_ecl(card.id)
      ecl_id = result[1].try(:[], "ecl_id")

      if ecl_id
        card.update_column(:ecl_id, ecl_id)
        card.reindex
      end
    end
    AfterParty::TaskRecord.create version: '20170113072751'
  end  # task :fill_ecl_id_for_poll_cards
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: remove'
  task remove_stale_card_pack_relation: :environment do
    puts "Running deploy task 'remove stale_card_pack_relation'"
    # About 259 records that are stale
    cprs = CardPackRelation.joins("LEFT JOIN cards on card_pack_relations.from_id = cards.id").where("cards.id IS NULL AND card_pack_relations.`deleted`=false")
    cprs.update_all(deleted: true)
    cprs.map{|cpr| cpr.send(:reindex_cover)}

  AfterParty::TaskRecord.create version: '20170331100942'
  end  # task :remove
end  # namespace :after_party

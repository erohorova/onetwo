namespace :after_party do
  desc 'Deployment task: enqueue_cards_with_video_stream'
  task enqueue_cards_with_video_stream: :environment do
    puts "Running deploy task 'enqueue_cards_with_video_stream'"

    # Put your task implementation HERE.
    VideoStream.published.where("card_id IS NOT NULL AND status != ?", 'upcoming').find_in_batches(batch_size: 100) do |video_streams|
      video_streams.each do |vs|
        if vs.card.published?
          FollowedCardEnqueueJob.perform_later(card_id: vs.card_id)
        end
      end
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170120005416'
  end  # task :enqueue_cards_with_video_stream
end  # namespace :after_party

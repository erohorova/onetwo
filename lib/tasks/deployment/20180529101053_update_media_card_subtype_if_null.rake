namespace :after_party do
  desc 'Deployment task: update_media_card_subtype_if_null'
  task update_media_card_subtype_if_null: :environment do
    puts "Running deploy task 'update_media_card_subtype_if_null'"

    # Put your task implementation HERE.
    Card.where(:card_subtype=>nil, :card_type=>"media").update_all(card_subtype: "file")

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180529101053'
  end  # task :update_media_card_subtype_if_null
end  # namespace :after_party

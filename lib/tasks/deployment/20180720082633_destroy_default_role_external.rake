namespace :after_party do
  desc 'Deployment task: destroy_default_role_external'
  task destroy_default_role_external: :environment do
    puts "Running deploy task 'destroy_default_role_external'"

    Role.where(default_name: 'external').destroy_all

    AfterParty::TaskRecord.create version: '20180720082633'
  end  # task :destroy_default_role_external
end  # namespace :after_party

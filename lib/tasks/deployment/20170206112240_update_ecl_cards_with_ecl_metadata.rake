namespace :after_party do
  desc 'Deployment task: update_ecl_cards_with_ecl_metadata'
  task update_ecl_cards_with_ecl_metadata: :environment do
    puts "Running deploy task 'update_ecl_cards_with_ecl_metadata'"

    # Put your task implementation HERE.

    # Update ecl_source_id, ecl_source_display_name of ecl_cards
    Card.where('ecl_id IS NOT NULL').find_each do |card|
      EclCardPropagationJob.perform_later(card.id)
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170206112240'
  end  # task :update_ecl_cards_with_ecl_metadata
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: backfill_streamable_for_votes'
  task backfill_streamable_for_votes: :environment do
    puts "Running deploy task 'backfill_streamable_for_votes'"

    # Put your task implementation HERE.
    CardsUser.where(state: "upvote").find_each do |cu|
      vote_object = Vote.where(votable: cu.card, voter: cu.user).first
      if vote_object
        ActivityStream.where(streamable: cu).find_each do |as|
          as.update_columns(streamable_type: "Vote", streamable_id: vote_object.id)
        end
      else
        log.error "Vote object does not exist for cards user : #{cu.id}"
      end
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20161118220805'
  end  # task :backfill_streamable_for_votes
end  # namespace :after_party

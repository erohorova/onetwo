namespace :after_party do
  desc 'Deployment task: backfill_assignment_metric_data'
  task backfill_assignment_metric_data: :environment do
    puts "Running deploy task 'backfill_assignment_metric_data'"

    # Put your task implementation HERE.
    Assignment.find_in_batches(batch_size: 100) do |batch|
      batch.each do |assignment|
        assignment.update_performance_metrics
      end
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170204201123'
  end  # task :backfill_assignment_metric_data
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: rss_blue_image_fix'
  task rss_blue_image_fix: :environment do
    puts "Running deploy task 'rss_blue_image_fix'"

    # Put your task implementation HERE.
    # Replacing blue image with stock_image
    # 8572 Records
    resources = Resource.joins(:cards).where(image_url: 'https://s3.amazonaws.com/edcast-s3-video-store/2017-07-27.png').order('created_at desc')
    resources.find_each do |resource|
      image_index = resource.created_at.to_i % 100
      stock_image = CARD_STOCK_IMAGE[image_index]
      resource.update_column(:image_url, stock_image[:url])
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20181119060509'
  end  # task :rss_blue_image_fix
end  # namespace :after_party

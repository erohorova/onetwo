namespace :after_party do
  desc 'Deployment task: populate_organizations_with_default_templates'
  task seed_default_templates: :environment do
    puts "Running deploy task 'seed_default_templates'"
    # rake seed_default_templates:populate[argument]

    # list of default templates titles
    #  must match existed file names without extension
    notify_templates_titles = EmailTemplate::DEFAULT_TITLES[:notify]
    mailer_templates_titles = EmailTemplate::DEFAULT_TITLES[:mailer]

    templates_titles = notify_templates_titles + mailer_templates_titles

    # folder should contain JSON files named as existed templates
    # JSON is generated using `unlayer` library
    working_dir = Rails.root.join(
      'public', 'templates', 'default_templates_design'
    )
    # get all organizations under deloitte host names
    Organization.exclude_default_org.find_each do |org|
      templates_titles.each do |title|
        next if org.email_templates.pluck(:title).include?(title)
        file_content = read_file(working_dir + "#{title}.json")
        next unless file_content
        EmailTemplate.create(
          title: title, design: file_content, language: 'en',
          state: 'published', is_active: true, organization_id: org.id
        )
      end
    end

    AfterParty::TaskRecord.create version: '20180802100404'
  end # task :seed_default_templates

  def read_file(file_path)
    file = File.open(file_path)
    file_data = file.read
    file.close
    # check validness of JSON
    JSON.parse(file_data)
    return file_data
  rescue Errno::ENOENT, JSON::ParserError => e
    puts "File read error: #{e.message}"
    nil
  end

end # namespace :after_party

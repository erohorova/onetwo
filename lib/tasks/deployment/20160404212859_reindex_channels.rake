namespace :after_party do
  desc 'Deployment task: re-index_all_channels'
  task reindex_channels: :environment do
    puts "Running deploy task 'reindex_channels'"

    # Put your task implementation HERE.

    Channel.all.each do |ch|
      ch.reindex_async
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160404212859'
  end  # task :reindex_channels
end  # namespace :after_party

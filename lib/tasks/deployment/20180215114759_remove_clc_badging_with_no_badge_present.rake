namespace :after_party do
  desc 'Deployment task: remove_clc_badging_with_no_badge_present'
  task remove_clc_badging_with_no_badge_present: :environment do
    puts "Running deploy task 'remove_clc_badging_with_no_badge_present'"

    # Put your task implementation HERE.

    ClcBadging.where('badge_id NOT IN (SELECT id from badges)').destroy_all

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180215114759'
  end  # task :remove_clc_badging_with_no_badge_present
end  # namespace :after_party

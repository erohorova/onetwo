namespace :after_party do
  desc 'Deployment task: add_org_oauth_sso'
  task add_org_oauth_sso: :environment do
    puts "Running deploy task 'add_org_oauth_sso'"

    Sso.find_or_create_by(name: 'org_oauth')

    AfterParty::TaskRecord.create version: '20160608101012'
  end  # task :add_org_oauth_sso
end  # namespace :after_party

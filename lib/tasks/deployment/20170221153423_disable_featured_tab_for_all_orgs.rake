namespace :after_party do
  desc 'Deployment task: disable_featured_tab_for_all_orgs'
  task disable_featured_tab_for_all_orgs: :environment do
    puts "Running deploy task 'disable_featured_tab_for_all_orgs'"

    # Put your task implementation HERE.

    Organization.where.not(id: Organization.default_org.id).find_each do |org|
      desktop_config = Config.find_or_initialize_by(
        configable: org,
        category: 'settings',
        name: 'tabs/featured/desktop',
      )
      desktop_config.update(value: 'false')

      mobile_config = Config.find_or_initialize_by(
        configable: org,
        category: 'settings',
        name: 'tabs/featured/mobile',
      )
      mobile_config.update(value: 'false')
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170221153423'
  end  # task :disable_featured_tab_for_all_orgs
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: add_user_content_completion_records_to_learning_queue'
  task add_user_content_completion_records_to_learning_queue: :environment do
    puts "Running deploy task 'add_user_content_completion_records_to_learning_queue'"

    # Put your task implementation HERE.
    UserContentCompletion.where(state: 'COMPLETED').find_each do |ucc|
      # add only user org cards to learning queue
      if ucc.user.organization_id == ucc.completable.organization_id
         ucc.completable.add_to_learning_queue(
          user_id: ucc.user_id,
          source: "assignments",
          state: 'completed'
        )
      end
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20161206064445'
  end  # task :add_user_content_completion_records_to_learning_queue
end  # namespace :after_party

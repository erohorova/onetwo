namespace :after_party do
  desc 'Deployment task: update_messaging_on_integrations'
  task update_messaging_on_integrations: :environment do
    puts "Running deploy task 'update_messaging_on_integrations'"

    # Put your task implementation HERE.
    coursera = Integration.find_by_name('Coursera')
    if coursera
      coursera.description = '<p>Import all courses from my Coursera profile.<br/><br/>You can determine your "coursera_user_id" from your Coursera Public Profile.<br/>Eg: http://www.coursera.org/user/i/coursera_user_id<br/><br/><span class="mute-font">Note: Requires a public Coursera profile</span></p>'
      coursera.fields = '[{"name":"coursera_public_page_id","label":"Coursera User Id","placeholder":"coursera_user_id"}]'
      coursera.save!
    end

    udemy = Integration.find_by_name('Udemy')
    if udemy
      udemy.description = '<p>Import all courses from my Udemy profile.<br/><br/>You can determine your "udemy_user_id" from your Udemy public profile url.<br/>Eg: https://www.udemy.com/u/udemy_user_id<br/><br/><span class="mute-font">Note: Requires a public Udemy profile with visible courses</span></p>'
      udemy.fields = '[{"name":"udemy_public_page_id","label":"Udemy User Id","placeholder":"udemy_user_id"}]'
      udemy.save!
    end

    skill_share = Integration.find_by_name('Skill Share')
    if skill_share
      skill_share.description = '<p>Import all courses from my Skillshare profile.<br/><br/>You can determine your "skillshare_user_name" from your Skillshare public profile url.<br/>Eg: https://www.skillshare.com/skillshare_user_name<br/><br/><span class="mute-font">Note: Requires a public Skillshare profile.</span></p>'
      skill_share.fields = '[{"name":"skill_share_user_name","label":"Skillshare Username","placeholder":"skillshare_user_name"}]'
      skill_share.save!
    end

    future_learn = Integration.find_by_name('Future Learn')
    if future_learn
      future_learn.description = '<p>Import all courses from my Future Learn profile.<br/><br/>You can determine your "future_learn_id" from your Future Learn public profile url.<br/>Eg: https://www.futurelearn.com/profiles/future_learn_id<br/><br/><span class="mute-font">Note: Requires a public Future Learn profile.</span></p>'
      future_learn.fields = '[{"name":"future_learn_profile_id","label":"Future Learn Id","placeholder":"future_learn_id"}]'
      future_learn.save!
    end

    code_school = Integration.find_by_name('Code School')
    if code_school
      code_school.description = '<p>Import all courses from my Code School profile.<br/><br/>You can determine your "codeschool_user_name" from your Code School public profile url.<br/>Eg: https://www.codeschool.com/users/codeschool_user_name<br/><br/><span class="mute-font">Note: Requires a public Code School profile.</span></p>'
      code_school.fields = '[{"name":"code_school_user_name","label":"Code School Username","placeholder":"codeschool_user_name"}]'
      code_school.save!
    end

    tree_house = Integration.find_by_name('Tree House')
    if tree_house
      tree_house.description = '<p>Import all courses from my Treehouse profile.<br/><br/>You can determine your "treehouse_username" from your Treehouse public profile url.<br/>Eg: http://www.teamtreehouse.com/treehouse_username.json<br/><br/><span class="mute-font">Note: Requires a public Treehouse profile.</span></p>'
      tree_house.fields = '[{"name":"tree_house_user_name","label":"Treehouse Username","placeholder":"treehouse_username"}]'
      tree_house.save!
    end

    plural_sight = Integration.find_by_name('Plural Sight')
    if plural_sight
      plural_sight.description = '<p>Import all courses from my Pluralsight profile.<br/><br/>You can determine your "pluralsight_username" from your Pluralsight public profile url.<br/>Eg: http://app.pluralsight.com/profile/pluralsight_username<br/><br/><span class="mute-font">Note: Requires a public Pluralsight profile.</span></p>'
      plural_sight.fields = '[{"name":"plural_sight_user_name","label":"Plural Sight Username","placeholder":"pluralsight_username"}]'
      plural_sight.save!
    end

    alison = Integration.find_by_name('Alison')
    if alison
      alison.description = '<p>Import all courses from my Alison profile.<br/><br/>You can determine your "alison_id/alison_username" from your Alison public profile url.<br/>Eg: http://alison.com/profile/alison_user_id/alison_user_name<br/><br/><span class="mute-font">Note: Requires a public Alison profile.</span></p>'
      alison.fields = '[{"name":"alison_user_id","label":"Alison Id/Alison Username","placeholder":"alison_id/alison_username"}]'
      alison.save!
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160726210141'
  end  # task :update_messaging_on_integrations
end  # namespace :after_party

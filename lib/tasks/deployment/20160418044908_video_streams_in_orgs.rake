namespace :after_party do
  desc 'Deployment task: video_streams_in_orgs'
  task video_streams_in_orgs: :environment do
    puts "Running deploy task 'video_streams_in_orgs'"

    # Put your task implementation HERE.
    VideoStream.find_each do |v|
      v.update_columns(organization_id: v.creator.try(:organization_id))
    end

    VideoStream.reindex

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160418044908'
  end  # task :video_streams_in_orgs
end  # namespace :after_party

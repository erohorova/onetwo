namespace :after_party do
  desc 'Deployment task: update_duplicate_teams'
  task update_duplicate_teams: :environment do
    puts "Running deploy task 'update_duplicate_teams'"

    # Put your task implementation HERE.

    duplicate_teams = Team.select('*, COUNT(*) as team_count').group('name, organization_id').having('team_count > 1')

    duplicate_teams.each do |record|
      teams = record.organization.teams.where(name: record.name).order(:created_at)
      teams.to_a.shift
      teams.each_with_index do |team, index|
        team.update(name: "#{team.name}_#{index+2}")
      end
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170124044626'
  end  # task :update_duplicate_teams
end  # namespace :after_party

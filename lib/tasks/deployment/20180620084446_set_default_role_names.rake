namespace :after_party do
  desc 'Deployment task: set_default_name_for_roles'
  task set_default_role_names: :environment do

    puts "Running deploy task 'set_default_role_names'"
    Role.find_each do |role|
      role_name = role.name.downcase
      def_name = role_name == 'smr' ? 'sme' : role_name
      role.update_attributes(default_name: def_name)
    end
    puts "Deploy task finished: 'set_default_role_names'"

    AfterParty::TaskRecord.create version: '20180620084446'
  end # task :set_default_role_names
end # namespace :after_party

namespace :after_party do
  desc 'Deployment task: update_channels_cards'
  task update_channels_cards: :environment do
    puts "Running deploy task 'update_channels_cards'"

    # Put your task implementation HERE.
    
    ChannelsCard.where(state: nil).find_each do |cc|
      cc.update_columns(state: 'curated', curated_at: cc.card.published_at)
    end


    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170215183610'
  end  # task :update_channels_cards
end  # namespace :after_party

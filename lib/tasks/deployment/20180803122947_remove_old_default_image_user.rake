namespace :after_party do
  desc 'Deployment task: remove_old_default_image_user'
  task remove_old_default_image_user: :environment do
    puts "Running deploy task 'remove_old_default_image_user'"

    # Put your task implementation HERE.
    # count = 33 records
    # the filestack url for the previous user image : https://cdn.filestackcontent.com/q0x8mlmQKs2phY7xUqCg
    User.where(avatar_file_name: 'q0x8mlmQKs2phY7xUqCg').each do |user|
      user.avatar = 'https://cdn.filestackcontent.com/FX31w7pFQFQMgb2RE2DT'
      user.save
      user.reindex_async
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180803122947'
  end  # task :remove_old_default_image_user
end  # namespace :after_party

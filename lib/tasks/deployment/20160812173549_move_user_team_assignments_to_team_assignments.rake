namespace :after_party do
  desc 'Deployment task: move_user_team_assignments_to_team_assignments'
  task move_user_team_assignments_to_team_assignments: :environment do
    puts "Running deploy task 'move_user_team_assignments_to_team_assignments'"

    # Put your task implementation HERE.

    # there are about 100 uta's
    # UserTeamAssignment.all.each do |uta|
    #   next if uta.assignable.nil? || uta.assignor.nil? || uta.team.nil?
    #   uta.team.create_team_assignments_for(assignee_ids: [uta.assignee.id], assignor_id: uta.assignor.id, assignable_type: uta.assignable.class.name, assignable_id: uta.assignable.id, opts: {self_assign: true})
    # end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160812173549'
  end  # task :move_user_team_assignments_to_team_assignments
end  # namespace :after_party

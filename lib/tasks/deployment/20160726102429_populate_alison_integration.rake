namespace :after_party do
  desc 'Deployment task: populate_alison_integration'
  task populate_alison_integration: :environment do
    puts "Running deploy task 'populate_alison_integration'"
    
    ALISON = {
      "name"=>"Alison",
      "description"=>"<p>Import all courses from my Alison profile daily.<br/><a href='http://alison.com/'>http://alison.com/profile/<b><Alison User Id/Alison User Name></b></a><br/>Note: Requires a public Alison profile.</p>",
      "enabled"=>true,
      "logo"=> File.open(Rails.root.join("app/assets/images", "alison_logo.jpg")),
      "fields" => [{"name":"alison_user_id","label":"Alison User Id","placeholder":"Alison User Id/Alison User Name"}].to_json
    }
    Integration.create(ALISON)
    AfterParty::TaskRecord.create version: '20160726102429'
  end  # task :populate_alison_integration
end  # namespace :after_party

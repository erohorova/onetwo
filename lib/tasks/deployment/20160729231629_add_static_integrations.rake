namespace :after_party do
  desc 'Deployment task: add_static_integrations'
  task add_static_integrations: :environment do
    puts "Running deploy task 'add_static_integrations'"

    # Put your task implementation HERE.
    SABA = {
        'name'=> 'SABA',
        'description'=> '<p>Please contact your Administrator to enable this integration.</p>',
        'enabled'=>true,
        'logo'=> File.open(Rails.root.join('app/assets/images', 'logo-saba.png')),
        'callback_url'=>'static'
    }

    Integration.create(SABA)

    SF = {
        'name'=> 'SuccessFactors',
        'description'=> '<p>Please contact your Administrator to enable this integration.</p>',
        'enabled'=>true,
        'logo'=> File.open(Rails.root.join('app/assets/images', 'successfactors-logo.jpg')),
        'callback_url'=>'static'
    }

    Integration.create(SF)

    CORNERSTONE = {
        'name'=> 'Cornerstone',
        'description'=> '<p>Please contact your Administrator to enable this integration.</p>',
        'enabled'=>true,
        'logo'=> File.open(Rails.root.join('app/assets/images', 'cornerstone_logo.jpg')),
        'callback_url'=>'static'
    }

    Integration.create(CORNERSTONE)

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160729231629'
  end  # task :add_static_integrations
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: clean_up_deleted_video_stream_notifications'
  task clean_up_deleted_video_stream_notifications: :environment do
    puts "Running deploy task 'clean_up_deleted_video_stream_notifications'"

    # Put your task implementation HERE.
    VideoStream.where(state: 'deleted').find_in_batches(batch_size: 200) do |streams|
      Notification.where(notifiable: streams).destroy_all
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20161118002255'
  end  # task :clean_up_deleted_video_stream_notifications
end  # namespace :after_party

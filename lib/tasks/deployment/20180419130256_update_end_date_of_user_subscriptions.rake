namespace :after_party do
  desc 'Deployment task: update_end_date_with_a_day_less_than_current_value'
  task update_end_date_of_user_subscriptions: :environment do
    puts "Running deploy task 'update_end_date_of_user_subscriptions'"

    # Put your task implementation HERE.
    UserSubscription.find_each do |user_subscription|
      user_subscription.update_column('end_date', user_subscription.end_date - 1.day)
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180419130256'
  end  # task :update_end_date_of_user_subscriptions
end  # namespace :after_party

require "open-uri"
namespace :after_party do
  desc 'Deployment task: populate_social_images_of_badges for each org -onetime only'
  task social_badge_image_population: :environment do
    puts "Running deploy task 'social_badge_image_population'"

    # Put your task implementation HERE.
    image_urls = ['https://s3.amazonaws.com/edcast-qa-paperclip-store/Badge01_Variant1.png', #100%, 4
                  'https://s3.amazonaws.com/edcast-qa-paperclip-store/Badge02_Variant1.png', #75% , 3
                  'https://s3.amazonaws.com/edcast-qa-paperclip-store/Badge03_Variant1.png', #50% , 2
                  'https://s3.amazonaws.com/edcast-qa-paperclip-store/Badge04_Variant1.png'] #25% , 1

    orgs = Organization.all
    orgs.each do |org|
      org.card_badges.order("created_at desc").each_with_index do |badge, index|
        badge.image_social = image_urls[index]
        badge.save
      end
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20171123070440'
  end  # task :badges_population
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: update_my_smart_bites_to_my_smart_cards'
  task update_my_smart_bites_to_my_smart_cards: :environment do
    puts "Running deploy task 'update_my_smart_bites_to_my_smart_cards'"

    # Put your task implementation HERE.
    # Total 183 Records in prod.
    Config.where(name: "OrgCustomizationConfig").each do |config|
      value = ActiveSupport::JSON.decode(config.value)
      value["mobile"]["meTabContent"]["mobile/meTabContent/mySmartbites"]["defaultLabel"] = "My SmartCards"
      config.update_attributes(value: value.to_json)
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180213113924'
  end  # task :update_my_smart_bites_to_my_smart_cards
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: update_assignment_state_from_assigned_to_completed'
  task update_assignment_state_from_assigned_to_completed: :environment do
    puts "Running deploy task 'update_assignment_state_from_assigned_to_completed'"

    # Put your task implementation HERE.
    query="select ass.*, ucc.completed_at as 'complete_date'  from user_content_completions ucc join assignments ass on ucc.completable_id = ass.assignable_id where ucc.state = 'completed' and ass.state in ('started', 'assigned') and ucc.user_id = ass.user_id  group by ucc.id;"
    Assignment.find_by_sql(query).each do |assignment|
      assignment.update(state: "completed", completed_at: assignment.complete_date)
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180423072156'
  end  # task :update_assignment_state_from_assigned_to_completed
end  # namespace :after_party

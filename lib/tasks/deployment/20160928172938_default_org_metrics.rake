namespace :after_party do
  desc 'Deployment task: default_org_metrics'
  task default_org_metrics: :environment do
    puts "Running deploy task 'default_org_metrics'"

    # Put your task implementation HERE.
    User.where(is_complete: true).where(organization_id: Organization.default_org.id).find_each do |user|
      MetricRecord.reaggregate_user_level_metrics(user.id)
      MetricRecord.reaggregate_time_spent_metrics(user.id)
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160928172938'
  end  # task :default_org_metrics
end  # namespace :after_party

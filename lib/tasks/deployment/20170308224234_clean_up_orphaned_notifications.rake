namespace :after_party do
  desc 'Deployment task: clean_up_orphaned_notifications'
  task clean_up_orphaned_notifications: :environment do
    puts "Running deploy task 'clean_up_orphaned_notifications'"

    # Put your task implementation HERE.
    # About 15.7K records
    Notification.joins("LEFT JOIN cards ON notifications.notifiable_id = cards.id where cards.id IS NULL AND notifications.notifiable_type='Card'").destroy_all

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170308224234'
  end  # task :clean_up_orphaned_notifications
end  # namespace :after_party
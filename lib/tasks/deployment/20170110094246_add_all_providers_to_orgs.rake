namespace :after_party do
  desc 'Deployment task: add_all_providers_to_orgs'
  task add_all_providers_to_orgs: :environment do
    puts "Running deploy task 'add_all_providers_to_orgs'"

    payload = {is_superadmin: true}
    token = JWT.encode(payload, Settings.features.integrations.secret, 'HS256')
    
    source_types = get_source_types(payload, token)
    if source_types
      org_ids = Organization.pluck(:id)
      non_default_providers = ["box", "brainshark", "cross_knowledge", "lynda", "get_abstract", "intuition"]
      sources_to_add = source_types.select{ |data| non_default_providers.include?(data["name"]) }
      sources_to_add.each do |source_type|
        add_orgs_to_source_types(payload, token, org_ids, source_type)
      end
    end
    AfterParty::TaskRecord.create version: '20170110094246'
  end  # task :add_all_providers_to_orgs

  def add_orgs_to_source_types(payload, token, org_ids, source_type) 
    conn = Faraday.new(url: "#{Settings.ecl.api_endpoint}/api/v1/source_types/#{source_type["id"]}/add_to_orgs")

    begin
      result = conn.put do |req|
        req.headers['Accept'] = 'application/json'
        req.headers['X-Api-Token'] = token
        req.body = {orgs: org_ids}
      end
    rescue Faraday::Error => e
      log.error "Error communicating with ECL; while adding providers for #{source_type["name"]} with Exception => #{e}"
      return
    end
  end

  def get_source_types(payload, token)
    conn = Faraday.new(url: "#{Settings.ecl.api_endpoint}/api/v1/source_types")
    log.debug "Requesting url api/v1/source_types from ECL"

    begin
      response = conn.get do |f|
        f.headers['X-Api-Token'] = token
        f.headers['Accept'] = 'application/json'
      end
    rescue Faraday::Error => e
      log.error "Error communicating with ECL; #{e}"
      return
    end

    unless response.success?
      log.error "Invalid response code from ECL API; response_code=#{response.status}"
      return
    end

    begin
      data = ActiveSupport::JSON.decode(response.body)["data"]
      if data.blank?
        log.error "ECL API response body returned no data;"
        return
      end
    rescue Exception => e
      log.error "Could not get source_types from ECL"
      return
    end
    data
  end
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: channels_reindex'
  task channels_reindex: :environment do
    puts "Running deploy task 'channels_reindex'"

    # Put your task implementation HERE.
    Channel.find_each { |c| c.reindex }
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180710070944'
  end  # task :channels_reindex
end  # namespace :after_party

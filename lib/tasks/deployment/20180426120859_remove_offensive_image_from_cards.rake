namespace :after_party do
  desc 'Deployment task: remove_offensive_image_from_cards'
  task remove_offensive_image_from_cards: :environment do
    puts "Running deploy task 'remove_offensive_image_from_cards'"

    # Put your task implementation HERE.
  	Card.where("filestack like ?", '%https://cdn.filestackcontent.com/qlLgeCGETcuNGf7Brqlu%').each do |card|
      card.filestack.each do |fs|
        if fs["url"]=="https://cdn.filestackcontent.com/qlLgeCGETcuNGf7Brqlu"
          fs["url"]="https://cdn.filestackcontent.com/sbJZdBJT5O3JJSIbs5dJ"
        end
  	  end
      card.save
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180426120859'
  end  # task :remove_offensive_image_from_cards
end  # namespace :after_party

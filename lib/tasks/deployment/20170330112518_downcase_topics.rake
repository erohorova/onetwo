namespace :after_party do
  desc 'Deployment task: downcase_topics'
  task downcase_topics: :environment do
    puts "Running deploy task 'downcase_topics'"

    # 1. Find user_profiles with atleast expert and learning topics set.
    # 2. Downcase topic_name and domain_name.
    # 3. Save downcased-data in respective fields.
    profiles = UserProfile.where('expert_topics is not null or learning_topics is not null')
    profiles.each do |profile|
      expert_topics   = profile.expert_topics.is_a?(Array) ? profile.expert_topics : []
      learning_topics = profile.learning_topics.is_a?(Array) ? profile.learning_topics : []

      expert_topics.each do |t|
        t['topic_name'].try(:downcase!)
        t['domain_name'].try(:downcase!)
      end

      learning_topics.each do |t|
        t['topic_name'].try(:downcase!)
        t['domain_name'].try(:downcase!)
      end

      profile.expert_topics = expert_topics
      profile.learning_topics = learning_topics

      profile.save
      profile.user.reindex
    end

    # 1. Find default_onboarding configs.
    # 2. Downcase topic_name and domain_name.
    # 3. Save downcased-data in respective fields.
    default_onboarding_configs = Config.where(name: 'default_topics', category: 'settings')
    default_onboarding_configs.each do |config|
      if config.parsed_value.present? && defaults = config.parsed_value['defaults']
        defaults.each do |topic|
          topic['topic_name'].try(:downcase!)
          topic['domain_name'].try(:downcase!)
        end

        config.value =  ActiveSupport::JSON.encode({ 'defaults' => defaults })
      end
      config.save
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170330112518'
  end  # task :downcase_topics
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: populate_organization_id'
  task populate_organization_id: :environment do
    puts "Running deploy task 'populate_organization_id' for webex_configs"
    #in production we are having only one records
    WebexConfig.find_each do |wc|
      wc.organization_id = wc.client.organization.id
      wc.save
    end

    puts "Populating organization_id for mailer_configs"
    #in production we are having only few records
    MailerConfig.find_each do |config|
      config.organization_id = config.client.organization.id
      config.save
    end

    puts "Populating organization_id for groups"
    Group.find_each do |group|
     #On production db we are having a single record with client_id NULL
     if group.client && group.client.organization
       group.organization_id = group.client.organization.id
       group.save!
     end
   end

    AfterParty::TaskRecord.create version: '20160913103905'
  end  # task :populate_organization_id
end  # namespace :after_party

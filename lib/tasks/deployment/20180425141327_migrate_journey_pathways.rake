namespace :after_party do
  desc 'Deployment task: migrating_existing_journey_pathways'
  task migrate_journey_pathways: :environment do
    puts "Running deploy task 'migrate_journey_pathways'"
    cards = Card.where(card_type: 'pack', card_subtype: 'journey')
    cards.update_all(card_subtype: 'simple', hidden: true)
    cards.map(&:reindex)
    puts "'migrate_journey_pathways' Task ended"
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180425141327'
  end  # task :migrate_journey_pathways
end  # namespace :after_party




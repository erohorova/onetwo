namespace :after_party do
  desc 'Deployment task: set_for_old_badges_is_default_true'
  task set_for_old_badges_is_default_true: :environment do
    puts "Running deploy task 'set_for_old_badges_is_default_true'"

    Badge.update_all(is_default: true)

    AfterParty::TaskRecord.create version: '20180815095347'
  end  # task :set_for_old_badges_is_default_true
end  # namespace :after_party

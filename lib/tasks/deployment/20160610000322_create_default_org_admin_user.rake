namespace :after_party do
  desc 'Deployment task: create_default_org_admin_user'
  task create_default_org_admin_user: :environment do

    Organization.where.not(id: Organization.default_org.id).each do |organization|
      begin
        User.create!(first_name: 'Admin',
                     last_name: 'User',
                     handle: '@adminuser',
                     organization_id: organization.id,
                     organization_role: 'admin',
                     password: User.random_password)
      rescue => e
        log.error e
        log.error "Problem with creating default admin user for org=#{organization.id}. errors=#{e.message}"
      end
    end

    # Put your task implementation HERE.

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160610000322'
  end  # task :create_default_org_admin_user
end  # namespace :after_party

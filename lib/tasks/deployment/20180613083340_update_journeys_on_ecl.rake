namespace :after_party do
  desc 'Deployment task: update_content_type_for_existed_journeys_on_ecl'
  task update_journeys_on_ecl: :environment do
    puts "Running deploy task 'update_journeys_on_ecl'"

    # Put your task implementation HERE.
    # Needs to update content_type for journeys on ecl

    journey_ids = Card.journeys.pluck(:id)
    journey_ids.each do |journey_id|
      begin
        EclApi::CardConnector.new.propagate_card_to_ecl(journey_id)
      rescue Exception => e
        log.error("Propagating card to ecl failed for card_id: #{journey_id}, error: #{e.message}")
      end
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180613083340'
  end  # task :update_journeys_on_ecl
end  # namespace :after_party

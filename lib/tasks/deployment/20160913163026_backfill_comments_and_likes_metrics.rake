namespace :after_party do
  desc 'Deployment task: backfill_comments_and_likes_metrics'
  task backfill_comments_and_likes_metrics: :environment do
    puts "Running deploy task 'backfill_comments_and_likes_metrics'"

    # Put your task implementation HERE.

    MetricRecord.find_each(query: {filtered: {filter: {and: [{terms: {action: ['like', 'comment']}}]}}}, sort: [{created_at: :asc}]) do |mr|
      if mr.should_perform_aggregations?
        _attrs = mr.attributes_for_activejob
        increments = []
        if MetricRecord.is_comment_event?(_attrs)
          increments << [:smartbites_comments_count, 1]
        end

        if MetricRecord.is_like_event?(_attrs)
          increments << [:smartbites_liked, 1]
        end
        UserLevelMetric.update_from_increments(opts: {user_id: _attrs[:actor_id]}, increments: increments, timestamp: Time.at(_attrs[:created_at_to_i]))
      end
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160913163026'
  end  # task :backfill_comments_and_likes_metrics
end  # namespace :after_party

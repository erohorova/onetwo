namespace :after_party do
  desc 'Deployment task: mark_complete_to_onboarded_users'
  task mark_complete_to_onboarded_users: :environment do
    puts "Running deploy task 'mark_complete_to_onboarded_users'"

    # Put your task implementation HERE.
    users = User
      .joins(:organization)
      .joins(:user_onboarding)
      .where("user_onboardings.status = 'completed' and users.is_complete = false and organizations.host_name != 'www'")

    p "Affecting #{users.count} users ... "
    users.find_each do |user|
      user.onboard!
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170519104743'
  end  # task :mark_complete_to_onboarded_users
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: update_app_configs'
  task update_app_configs: :environment do
    puts "Running deploy task 'update_app_configs'"

    Config.where(configable_type: "Client").each do |cfg|
      client = cfg.configable
      if client && client.organization
        cfg.configable = client.organization
        cfg.save
      end
    end
    AfterParty::TaskRecord.create version: '20161111183217'
  end  # task :update_app_configs
end  # namespace :after_party

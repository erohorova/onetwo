namespace :after_party do
  desc 'Deployment task: moving_readable_card_types_from_constant_to_configs_table'
  task populate_readable_card_types_configs_default_org: :environment do
    puts "Running deploy task 'populate_readable_card_types_configs_default_org'"

    # Put your task implementation HERE.
    default_org = Organization.default_org
    config_params = { 
                      name: 'readable_card_types',
                      value: { 
                               course: 1, webinar: 2, classroom: 3, video: 4, article: 5, book: 6,
                               endorsement: 7, compressed_file: 8, excel: 9, image: 10, music: 11,
                               pdf: 12, powerpoint: 13, podcast: 14, livefeed: 15, assessment: 16,
                               blog_post: 17, mission: 18, text: 19, project: 20, webcast: 21,
                               wiki: 22, quiz: 23, curricula: 24, audio_books: 25, books: 26,
                               diagnostics: 27, jobs: 28, simulation: 29, vlabs: 30,
                               introduction: 31, action_step: 32, connect_block: 33,
                               connect_item: 34, connect_topic: 35, curriculum: 36, discussion: 37,
                               document: 38, evaluation_test: 39, event: 40,
                               external_training: 41, form: 42, library: 43, material: 44,
                               note: 45, objective: 46, observation_checklist: 47,
                               online_class: 48, online_resource: 49, q_a: 50,
                               quick_course: 51, section: 52, session: 53, test: 54,
                               topic: 55, sco: 56, social_learning_program: 57
                             }.to_json,
                      category:'settings',
                      data_type: 'json',
                      description: 'Default readable card types' 
                    }
	  default_org.configs.create(config_params)
    
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180125123434'
  end  # task :populate_readable_card_types_configs_default_org
end  # namespace :after_party

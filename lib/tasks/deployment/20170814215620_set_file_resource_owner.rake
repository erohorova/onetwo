# Attachable types
# Post
# NULL
# Comment
# VideoStream
# Card
# Resource

#1. 11407 which does not have attachable type
namespace :after_party do
  desc 'Deployment task: set_file_resource_owner'
  task set_file_resource_owner: :environment do
    puts "Running deploy task 'set_file_resource_owner'"

    # Put your task implementation HERE.
    # 14008
    FileResource.where("attachable_type IN ('Card', 'Comment') AND user_id IS NULL").find_in_batches do |file_resources|
      file_resources.each do |fr|
        fr.send :set_file_resource_author
        unless fr.save
          log.error "Unable to set file resource author for #{fr.id}"
        end
      end
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170814215620'
  end  # task :set_file_resource_owner
end  # namespace :after_party

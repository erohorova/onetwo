namespace :after_party do
  desc 'Deployment task: set_is_private_true_for_old_teams_except_team_everyone'
  task set_is_private_true_for_old_teams: :environment do
    puts "Running deploy task 'set_is_private_true_for_old_teams'"

    Team.where.not(is_everyone_team: true).find_in_batches do |ids|
      Team.where(id: ids).update_all(is_private: true)
    end

    AfterParty::TaskRecord.create version: '20180704081840'
  end  # task :set_is_private_true_for_old_teams
end  # namespace :after_party

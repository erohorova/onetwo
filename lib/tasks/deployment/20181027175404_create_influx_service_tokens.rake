namespace :after_party do
  desc 'Deployment task: create_influx_service_tokens'
  task create_influx_service_tokens: :environment do
    puts "Running deploy task 'create_influx_service_tokens'"

    Organization.all.each { |org| org.send :set_influx_service_token }

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20181027175404'
  end  # task :create_influx_service_tokens
end  # namespace :after_party

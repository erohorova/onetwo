namespace :after_party do
  desc 'Deployment task: added_group_permissions_sub_admins_for_admin_role'
  task add_group_permissions_sub_admins_for_admin_role: :environment do
    puts "Running deploy task 'add_group_permissions_sub_admins_for_admin_role'"

    Role.admins.find_each{|r| r.create_role_permissions }

    AfterParty::TaskRecord.create version: '20180516093611'
  end  # task :add_group_permissions_sub_admins_for_admin_role
end  # namespace :after_party

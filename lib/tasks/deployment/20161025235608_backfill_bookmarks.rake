namespace :after_party do
  desc 'Deployment task: backfill_bookmarks'
  task backfill_bookmarks: :environment do
    puts "Running deploy task 'backfill_bookmarks'"

    Bookmark.skip_callback(:create, :after, :add_to_learning_queue)

    CardsUser.where(state:'bookmark').each do |cu|
      Bookmark.create(bookmarkable: cu.card, user: cu.user, created_at: cu.created_at)
    end

    Bookmark.set_callback(:create, :after, :add_to_learning_queue)

    # Put your task implementation HERE.

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20161025235608'
  end  # task :backfill_bookmarks
end  # namespace :after_party

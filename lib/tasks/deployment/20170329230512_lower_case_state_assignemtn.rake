namespace :after_party do
  desc 'Deployment task: lower_case_state_assignemtn'
  task lower_case_state_assignemtn: :environment do
    puts "Running deploy task 'lower_case_state_assignemtn'"

    # Put your task implementation HERE.
    Assignment.where(state: 'ASSIGNED').update_all(state: 'assigned')
    Assignment.where(state: 'STARTED').update_all(state: 'started')
    Assignment.where(state: 'COMPLETED').update_all(state: 'completed')

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170329230512'
  end  # task :lower_case_state_assignemtn
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: backfill_assignments_to_learning_queue'
  task backfill_assignments_to_learning_queue: :environment do
    puts "Running deploy task 'backfill_assignments_to_learning_queue'"

    # Put your task implementation HERE.
    Assignment.where.not(state: "COMPLETED").find_each do |assignment|
      assignment.add_to_learning_queue
    end


    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20161003232850'
  end  # task :backfill_assignments_to_learning_queue
end  # namespace :after_party

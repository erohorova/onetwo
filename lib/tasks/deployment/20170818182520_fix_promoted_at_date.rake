namespace :after_party do
  desc 'Deployment task: fix_promoted_at_date'
  task fix_promoted_at_date: :environment do
    puts "Running deploy task 'fix_promoted_at_date'"

    # Put your task implementation HERE.
    # 1486 records
    Card.where(is_official: true, promoted_at: nil).find_in_batches do |cards|
      cards.each {|card| card.update(promoted_at: card.updated_at)}
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170818182520'
  end  # task :fix_promoted_at_date
end  # namespace :after_party

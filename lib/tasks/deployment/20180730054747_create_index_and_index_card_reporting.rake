namespace :after_party do
  desc 'Deployment task: create_index_and_index_card_reporting'
  task create_index_and_index_card_reporting: :environment do
    puts "Running deploy task 'create_index_and_index_card_reporting'"

    # Put your task implementation HERE.
    FlagContent::CardReportingSearchService.create_index!

    card_ids = CardReporting.with_deleted.pluck(:card_id).uniq
    card_ids.map do |card|
      CardReportingIndexingJob.perform_later(card_id: card)
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180730054747'
  end  # task :create_index_and_index_card_reporting
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: update_ecl_cards_with_resource_id'
  task update_ecl_cards_with_resource_id: :environment do
    puts "Running deploy task 'update_ecl_cards_with_resource_id'"

    # Put your task implementation HERE.
    # Affected cards are of type media and course. We will try to fetch the data from ecl again and update resource_id if possible
    Card.where.not(ecl_id: nil).
      where(author_id: nil).
      where(resource_id: nil).
      where(hidden: false).
      where(card_type: ['media', 'course']).
      find_each do |card|
        connector = EclApi::CardConnector.new(ecl_id: card.ecl_id, organization_id: card.organization_id)
        ecl_data = connector.content_item_by_id
        resource = connector.get_resource(ecl_data)
        if resource && resource.persisted?
          card.update_attributes(resource_id: resource.id)
        else
          log.error "Error saving resource for card id: #{card.id}"
        end
      end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170127012742'
  end  # task :update_ecl_cards_with_resource_id
end  # namespace :after_party

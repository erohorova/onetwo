namespace :after_party do
  desc 'Deployment task: update_auto_complete_for_pathways_and_journeys'
  task update_auto_complete_for_pathways_and_journeys: :environment do
    puts "Running deploy task 'update_auto_complete_for_pathways_and_journeys'"

    # Put your task implementation HERE.
    Card.where(card_type: ['pack', 'journey']).each do |card|
      card.update_attributes(auto_complete: true)
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180102072317'
  end  # task :update_auto_complete_for_pathways_and_journeys
end  # namespace :after_party

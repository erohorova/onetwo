namespace :after_party do
  desc 'Deployment task: add_state_to_video_streams'
  task add_state_to_video_streams: :environment do
    puts "Running deploy task 'add_state_to_video_streams'"

    # Put your task implementation HERE.
    VideoStream.find_each do |v|
      v.update_attributes(state: 'published')
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160422004830'
  end  # task :add_state_to_video_streams
end  # namespace :after_party

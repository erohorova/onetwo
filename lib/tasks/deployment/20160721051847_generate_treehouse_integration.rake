namespace :after_party do
  desc 'Deployment task: generate_treehouse_integration'
  task generate_treehouse_integration: :environment do
    puts "Running deploy task 'generate_treehouse_integration'"

    TREE_HOUSE = {
      "name"=>"Tree House",
      "description"=>"<p>Import completed courses from Treehouse daily.<br/><a href='http://www.teamtreehouse.com/'>http://www.teamtreehouse.com/<b>Treehouse username</b>.json</a><br/>Note: Requires a public Treehouse profile.</p>",
      "enabled"=>true,
      "logo"=> File.open(Rails.root.join("app/assets/images", "treehouse_logo.jpg")),
      "fields" => [{"name":"tree_house_user_name","label":"Tree House Username","placeholder":"Tree house username"}].to_json
    }
    Integration.create(TREE_HOUSE)
    AfterParty::TaskRecord.create version: '20160721051847'
  end  # task :generate_treehouse_integration
end  # namespace :after_party

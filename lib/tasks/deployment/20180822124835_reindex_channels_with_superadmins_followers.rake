namespace :after_party do
  desc 'Deployment task: reindex_channels_with_superadmins_followers'
  task reindex_channels_with_superadmins_followers: :environment do
    puts "Running deploy task 'reindex_channels_with_superadmins_followers'"

    # Put your task implementation HERE.
    Channel.joins(:user).where(users: {email: nil}).each(&:reindex)
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180822124835'
  end  # task :reindex_channels_with_superadmins_followers
end  # namespace :after_party

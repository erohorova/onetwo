namespace :after_party do
  desc 'Deployment task: clean_up_duplicate_metrics'
  task clean_up_duplicate_metrics: :environment do
    puts "Running deploy task 'clean_up_duplicate_metrics'"

    # Put your task implementation HERE.
    UserLevelMetric.group([:user_id, :period, :offset]).having("count(*) > 1").select("user_id, period, offset, min(id) as min_id").each do |um|
      UserLevelMetric.where(user_id: um.user_id, period: um.period, offset: um.offset).where.not(id: um.min_id).destroy_all
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170123192008'
  end  # task :clean_up_duplicate_metrics
end  # namespace :after_party

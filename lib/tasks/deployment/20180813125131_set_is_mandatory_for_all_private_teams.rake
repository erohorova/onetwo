namespace :after_party do
  desc 'Deployment task: set_is_mandatory_for_all_private_and_dynamic_teams'
  task set_is_mandatory_for_all_private_teams: :environment do
    puts "Running deploy task 'set_is_mandatory_for_all_private_teams'"

    # Put your task implementation HERE.
    Team.where('is_private IS TRUE or is_dynamic IS TRUE').update_all(is_mandatory: true)
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180813125131'
  end  # task :set_is_mandatory_for_all_private_teams
end  # namespace :after_party

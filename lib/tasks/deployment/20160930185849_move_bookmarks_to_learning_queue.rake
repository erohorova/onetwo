namespace :after_party do
  desc 'Deployment task: move_bookmarks_to_learning_queue'
  task move_bookmarks_to_learning_queue: :environment do
    puts "Running deploy task 'move_bookmarks_to_learning_queue'"

    # Put your task implementation HERE.
    CardsUser.where(state: "bookmark").find_each do |cu|
      if cu.card
        cu.card.add_to_learning_queue(user_id: cu.user_id, source: "bookmarks")
      end
    end


    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160930185849'
  end  # task :move_bookmarks_to_learning_queue
end  # namespace :after_party

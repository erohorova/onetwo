namespace :after_party do
  desc 'Deployment task: add_custom_fields_and_joined_date_mapping_to_users'
  task add_mapping_to_users: :environment do
    puts "Running deploy task 'add_mapping_to_users'"

    # Put your task implementation HERE.
    index_name = "#{User.searchkick_index.name}/_mapping/user"
    ES_SEARCH_CLIENT.perform_request('PUT', index_name, {}, {
      properties: {
        joined_at: {
          type: 'date'
        },
        custom_fields: {
          type: :nested
        }
      }
    })

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180702075929'
  end  # task :add_mapping_to_users
end  # namespace :after_party
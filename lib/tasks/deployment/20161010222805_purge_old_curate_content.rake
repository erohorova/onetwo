namespace :after_party do
  desc 'Deployment task: purge_old_curate_content'
  task purge_old_curate_content: :environment do
    puts "Running deploy task 'purge_old_curate_content'"

    # Put your task implementation HERE.
    Card.where(state: "new").where("created_at <= (:day_ago)", day_ago: 1.day.ago).find_each do |card|
      card.destroy
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20161010222805'
  end  # task :purge_old_curate_content
end  # namespace :after_party

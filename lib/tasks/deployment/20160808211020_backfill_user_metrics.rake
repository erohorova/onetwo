namespace :after_party do
  desc 'Deployment task: backfill_user_metrics'
  task backfill_user_metrics: :environment do
    puts "Running deploy task 'backfill_user_metrics'"

    # Put your task implementation HERE.
    User.where(is_complete: true).where.not(organization_id: Organization.default_org.id).find_each do |user|
      MetricRecord.reaggregate_user_level_metrics(user.id)
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160808211020'
  end  # task :backfill_user_metrics
end  # namespace :after_party

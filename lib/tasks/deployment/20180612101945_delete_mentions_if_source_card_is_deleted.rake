namespace :after_party do
  desc 'Deployment task: delete_mentions_if_source_card_is_deleted'
  task delete_mentions_if_source_card_is_deleted: :environment do
    puts "Running deploy task 'delete_mentions_if_source_card_is_deleted'"

    # Put your task implementation HERE.
    Mention.joins("LEFT JOIN cards on mentions.mentionable_id = cards.id where cards.deleted_at IS NOT NULL AND mentions.mentionable_type = 'Card'").delete_all
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180612101945'
  end  # task :delete_mentions_if_source_card_is_deleted
end  # namespace :after_party

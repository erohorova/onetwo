namespace :after_party do
  desc 'Deployment task: fill_up_deep_link_type_for_learning_queue_items'
  task fill_up_deep_link_type_for_learning_queue_items: :environment do
    puts "Running deploy task 'fill_up_deep_link_type_for_learning_queue_items'"

    # Put your task implementation HERE.
    LearningQueueItem.find_in_batches(batch_size: 100) do |items|
      items.each do |item|
        begin
          id, type = item.get_app_deep_link_id_and_type
          item.update(deep_link_id: id, deep_link_type: type)
        rescue => e
          log.error "Error occurred while updating learning_queue_item for #{item.id} with #{e.message}"
          next
        end
      end
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20161101204609'
  end  # task :fill_up_deep_link_type_for_learning_queue_items
end  # namespace :after_party

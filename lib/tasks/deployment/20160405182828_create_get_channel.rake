namespace :after_party do
  desc 'Deployment task: create_get_channel'
  task create_get_channel: :environment do
    channel = Channel.new(label:'get', organization: Organization.default_org)
    channel.only_authors_can_post = false
    channel.save
    AfterParty::TaskRecord.create version: '20160405182828'
  end  # task :create_get_channel
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: remove_duplicate_user_metrics_aggregations'
  task remove_duplicate_user_metrics_aggregations: :environment do
    puts "Running deploy task 'remove_duplicate_user_metrics_aggregations'"

    # TASK DEFINITION HERE:
    # =====================================================================

    group_by_attrs = %w{organization_id start_time user_id}

    ids_to_delete = UserMetricsAggregation.
      all.
      group_by { |agg| agg.attributes.slice(*group_by_attrs) }.
      values.
      select { |aggs| aggs.length > 1}.
      flat_map { |x| x[1..-1] }.
      map(&:id)

    UserMetricsAggregation.where(id: ids_to_delete).delete_all

    # =====================================================================


    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20181031220816'
  end  # task :remove_duplicate_user_metrics_aggregations
end  # namespace :after_party

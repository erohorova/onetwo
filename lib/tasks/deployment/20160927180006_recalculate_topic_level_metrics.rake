namespace :after_party do
  desc 'Deployment task: recalculate_topic_level_metrics'
  task recalculate_topic_level_metrics: :environment do
    puts "Running deploy task 'recalculate_topic_level_metrics'"

    # Put your task implementation HERE.
    UserTopicLevelMetric.delete_all
    Interest.find_each do |interest|
      MetricRecord.reaggregate_user_topic_level_metrics(interest)
    end


    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160927180006'
  end  # task :recalculate_topic_level_metrics
end  # namespace :after_party

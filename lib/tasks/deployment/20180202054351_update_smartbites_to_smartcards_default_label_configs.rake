namespace :after_party do
  desc 'Deployment task: updating_default_value_from_smart_cards_to_smart_bites_OrgCustomizationConfig'
  task update_smartbites_to_smartcards_default_label_configs: :environment do
    puts "Running deploy task 'update_smartbites_to_smartcards_default_label_configs'"

    # Put your task implementation HERE.
    Config.where(name: "OrgCustomizationConfig").each do |config|
      value = config.parsed_value
      smartbite_score = value.dig("mobile","meTabStats","mobile/meTabStats/smartbiteScore")
      if smartbite_score.present? && smartbite_score[:defaultLabel] == "SmartBites Score"
        smartbite_score[:defaultLabel] = "SmartCards Score" 
      end
      my_smartbites = value.dig("mobile","meTabContent","mobile/meTabContent/mySmartbites")
      if my_smartbites.present? && my_smartbites[:defaultLabel] == "My SmartBites"
        my_smartbites[:defaultLabel] = "My SmartCards" 
      end
      create_smartbite = value.dig("mobile","create","mobile/create/createSmartbite")
      if create_smartbite.present? && create_smartbite[:defaultLabel] == "Create Smartbite"
        create_smartbite[:defaultLabel] = "Create SmartCard" 
      end
      config.update(value: value.to_json)
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180202054351'
  end  # task :update_smartbites_to_smartcards_default_label_configs
end  # namespace :after_party

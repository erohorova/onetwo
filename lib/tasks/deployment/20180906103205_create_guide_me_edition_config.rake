namespace :after_party do
  desc 'Deployment task: create_guide_me_edition_config'
  task create_guide_me_edition_config: :environment do
    puts "Running deploy task 'create_guide_me_edition_config'"

    # Put your task implementation HERE.
    # production db has ~200 enable_guide_me records.
    Config.where(name: 'enable_guide_me').find_each do |config|
      guide_me_edition = Config.new(configable: config.configable, 
        name: 'guide_me_config',
        data_type: 'json',
        value: {edition: 'pro', guide_me_enabled: false}.to_json
      )
      guide_me_edition.value = {edition: 'pro', guide_me_enabled: true}.to_json if config.value.to_bool
      guide_me_edition.save!
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180906103205'
  end  # task :create_guide_me_edition_config
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: fix_course_subtype'
  task fix_course_subtype: :environment do
    puts "Running deploy task 'fix_course_subtype'"

    # Put your task implementation HERE.
    # About 28335 cards
    course_text_cards = Card.where.not(organization: Organization.default_org).
      where(card_type: 'course', card_subtype: 'text').
      where.not(resource_id: nil)

    course_text_cards.update_all(card_subtype: 'link')

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170928180748'
  end  # task :fix_course_subtype
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: sync_to_ecl_and_reindex_cards_having_bia_and_rating'
  task sync_to_ecl_and_reindex_cards_having_bia_and_rating: :environment do
    puts "Running deploy task 'sync_to_ecl_and_reindex_cards_having_bia_and_rating'"

    # NOTE: Please make sure that ECL is deployed before running this task.
    # There are around 15000 records of CardsRating on prod.

    CardsRating.find_in_batches do |ratings|
      card_ids = ratings.map(&:card_id)
      cards_having_ratings_and_bia = Card.where(id: card_ids)
        cards_having_ratings_and_bia.find_each do |card|
          IndexJob.perform_later('Card', card.id)
          EclCardPropagationJob.perform_later(card.id)
        end 
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180506155921'
  end  # task :sync_to_ecl_and_reindex_cards_having_bia_and_rating
end  # namespace :after_party

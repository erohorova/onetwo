namespace :after_party do
  desc 'Deployment task: backfill_video_streams_with_default_recordings'
  task backfill_video_streams_with_default_recordings: :environment do
    VideoStream.find_in_batches do |video_streams|
      video_streams.each do |video_stream|
        recording = video_stream.recordings.first
        unless recording.nil?
          recording.update(default: true)
          video_stream.reindex
        end
      end
    end
    # Put your task implementation HERE.
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20161004220943'
  end  # task :backfill_video_streams_with_default_recordings
end  # namespace :after_party

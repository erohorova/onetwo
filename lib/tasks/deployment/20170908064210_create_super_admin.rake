namespace :after_party do
  desc 'Deployment task: create_super_admin'
  task create_super_admin: :environment do
    puts "Running deploy task 'create_super_admin'"

    emails = ["sagar@edcast.com", "ramin@edcast.com", "bharani@edcast.com", "pd@edcast.com", "michael@edcast.com","deepika@edcast.com", "kavita@edcast.com", "kapil@edcast.com", "neil@edcast.com", "raahul.seshadri@edcast.com”, jigar@edcast.com"]

    SuperAdminUser.delete_all
    emails.each do |email|
      SuperAdminUser.add_by_email(email)
    end

    AfterParty::TaskRecord.create version: '20170908064210'
  end  # task :create_super_admin
end  # namespace :after_party

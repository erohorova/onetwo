namespace :after_party do
  desc 'Deployment task: add_basic_level_to_learning_topics'
  task add_basic_level_to_learning_topics: :environment do
    puts "Running deploy task 'add_basic_level_to_learning_topics'"

    # selecting profiles of users who are not suspended and whose profiles have non null values
    # for learning topics 
    UserProfile.where("learning_topics is not null")
      .find_in_batches do |batch|
      batch.each do |user_profile|
        user_profile.learning_topics.map{|topic| topic[:level] = 'beginner'}
        user_profile.save
      end
    end
    # Put your task implementation HERE.

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20171228084538'
  end  # task :add_basic_level_to_learning_topics
end  # namespace :after_party

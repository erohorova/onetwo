namespace :after_party do
  desc 'Deployment task: add_keys_for_whitelabelled_apps'
  task add_keys_for_whitelabelled_apps: :environment do
    puts "Running deploy task 'add_keys_for_whitelabelled_apps'"

    # Put your task implementation HERE.

    values = [
      {
        host_name: 'abg',
        uri_scheme: 'abgedcastapp',
        branchmetric_api_key: 'key_live_ahQbVvjxh9kV8YUaKjL1fhoeuBfwUHI1'
      },
      {
        host_name: 'dell',
        uri_scheme: 'delledcastapp',
        branchmetric_api_key: 'key_live_jeRoSymvjWxkejcVXXKfEoodytfxWvVn'
      },
      {
        host_name: 'deloitte',
        uri_scheme: 'deloitteedcastapp',
        branchmetric_api_key: 'key_live_ahKcLAetj2BicTP8FTP0gliixsnvYOni'
      },
      {
        host_name: 'futureskillsnasscom',
        uri_scheme: 'fsnasscomedcastapp',
        branchmetric_api_key: 'key_live_maLdKtdEl7qpwd9U1Qi2nmemsqbAYSXf'
      },
      {
        host_name: 'salesu',
        uri_scheme: 'salesuedcastapp',
        branchmetric_api_key: 'key_live_kmSlNrowgYtdvGXoSdURZcilBzht87Bo'
      },
      {
        host_name: 'selearn',
        uri_scheme: 'selearnedcastapp',
        branchmetric_api_key: 'key_live_lirM20l6seBCL8kyd3exKdhlsxcwIF8n'
      },
      {
        host_name: 'skillset',
        uri_scheme: 'skillsetedcastapp',
        branchmetric_api_key: 'key_live_hjMiLthtg6wcPXjM5PYX5ciiwrpw1431'
      },
      {
        host_name: 'naluat',
        uri_scheme: 'nseedcastapp',
        branchmetric_api_key: 'key_live_kmHhPBmtp2vh9suW9wLwNadosrmPdkyF'
      }
    ]


    values.each do |args|
      org = Organization.find_by(host_name: args[:host_name])
      if org.nil?
        puts "======= Org not found with host_name: #{args[:host_name]}"
        next
      end

      org.update_attribute(:whitelabel_build_enabled, true)

      # Add branchmetrics_api_key
      config = org.configs.find_or_initialize_by(name: "whitelabel_branchmetrics_api_key")
      config.value = args[:branchmetric_api_key]
      config.description = "Branchmetric API key for white label app"
      config.save
      puts "========== Added config ##{config.id} for org ##{org.id}"

      # Add uri_scheme config
      config = org.configs.find_or_initialize_by(name: "mobile_uri_scheme")
      config.value = args[:uri_scheme]
      config.description = "URI scheme for white label app"
      config.save
      puts "========== Added config ##{config.id} for org ##{org.id}"
    end


    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20190314115105'
  end  # task :add_keys_for_whitelabelled_apps
end  # namespace :after_party

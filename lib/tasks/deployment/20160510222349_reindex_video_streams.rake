namespace :after_party do
  desc 'Deployment task: reindex_video_streams'
  task reindex_video_streams: :environment do
    puts "Running deploy task 'reindex_video_streams'"

    # Put your task implementation HERE.
    VideoStream.find_each do |v|
      v.partial_update = true
      v.update_hash = {is_official: false}
      v.update_index
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160510222349'
  end  # task :reindex_video_streams
end  # namespace :after_party

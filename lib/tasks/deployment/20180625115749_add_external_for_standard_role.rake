namespace :after_party do
  desc 'Deployment task: find_or_create_external_role'
  task add_external_for_standard_role: :environment do
    puts "Running deploy task 'add_external_for_standard_role'"

    Organization.all.each do |org|
      role = Role.where(
        organization: org, default_name: 'external',
        name: 'external', master_role: true
      ).first_or_create
      role.create_role_permissions
    end

    AfterParty::TaskRecord.create version: '20180625115749'
  end  # task :added_external_for_standard_role
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: clean_up_notification_entries_for_nil_source'
  task clean_up_notification_entries_for_nil_source: :environment do
    puts "Running deploy task 'clean_up_notification_entries_for_nil_source'"

    # Put your task implementation HERE.
    NotificationEntry.joins("Left join assignments on notification_entries.sourceable_id = assignments.id where 
      assignments.id is null and notification_entries.sourceable_type = 'Assignment'").destroy_all

    NotificationEntry.joins("Left join cards on notification_entries.sourceable_id = cards.id where 
      cards.id is null and notification_entries.sourceable_type = 'Card'").destroy_all

    NotificationEntry.joins("Left join comments on notification_entries.sourceable_id = comments.id where 
      comments.id is null and notification_entries.sourceable_type = 'Comment'").destroy_all
    
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180514060916'
  end  # task :clean_up_notification_entries_for_nil_source
end  # namespace :after_party

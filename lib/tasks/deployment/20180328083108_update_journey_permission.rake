namespace :after_party do
  desc 'Deployment task: change_add_to_journey_permission_to_common_create_journey'
  task update_journey_permission: :environment do
    puts "Running deploy task 'update_journey_permission'"

    # Put your task implementation HERE.

    #clear unnecessary ADD_TO_JOURNEY permission and create new CREATE_JOURNEY for 'admin', 'curator', 'collaborator'

    RolePermission.where(name: 'ADD_TO_JOURNEY').delete_all
    Role.where(name: ['admin', 'curator', 'collaborator']).find_each do |role|
      role.create_role_permissions
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180328083108'
  end  # task :update_journey_permission
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: create_edcast_settings'
  task create_edcast_settings: :environment do
    puts "Running deploy task 'create_edcast_settings'"

    # Put your task implementation HERE.
    EdcastSetting.create(get:false)
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160412010759'
  end  # task :create_edcast_settings
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: clean_up_orphaned_assignments'
  task clean_up_orphaned_assignments: :environment do
    puts "Running deploy task 'clean_up_orphaned_assignments'"

    # Put your task implementation HERE.
    Assignment.all.each do |assignment|
    	assignment.destroy if assignment.assignable.nil?
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160923231732'
  end  # task :clean_up_orphaned_assignments
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: backfill_bookmarks_count_in_content_level_metrics'
  task backfill_bookmarks_count_in_content_level_metrics: :environment do
    puts "Running deploy task 'backfill_bookmarks_count_in_content_level_metrics'"

    ContentLevelMetric.transaction do
      Bookmark.where(bookmarkable_type: "Card").find_each do |record|
        begin
          ContentLevelMetric.increment_bookmarks_count({content_id: record.bookmarkable_id, content_type: 'card'}, 1, record.created_at)
        rescue => e
          log.info("Unable to add bookmarks count for Card id: #{record.bookmarkable_id}")
        end
      end
    end

    # Put your task implementation HERE.

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170303130845'
  end  # task :backfill_bookmarks_count_in_content_level_metrics
end  # namespace :after_party

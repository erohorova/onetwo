namespace :after_party do
  desc 'Deployment task: backfill_team_level_metrics'
  task backfill_team_level_metrics: :environment do
    puts "Running deploy task 'backfill_team_level_metrics'"

    # Put your task implementation HERE.
    Team.where.not(organization_id: Organization.default_org.id).find_each do |team|
      MetricRecord.reaggregate_team_level_metrics(team)
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160914011721'
  end  # task :backfill_team_level_metrics
end  # namespace :after_party

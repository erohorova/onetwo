namespace :after_party do
  desc 'Deployment task: generate_default_roles_for_organization'
  task generate_default_roles_for_organization: :environment do
    puts "Running deploy task 'generate_default_roles_for_organization'"

    # Put your task implementation HERE.
    Organization.all.each do |org|
      org.send :create_default_roles
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20161007220929'
  end  # task :generate_default_roles_for_organization
end  # namespace :after_party

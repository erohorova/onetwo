namespace :after_party do
  desc 'Deployment task: port_follow_preferences'
  task port_follow_preferences: :environment do
    puts "Running deploy task 'port_follow_preferences'"

    # Put your task implementation HERE.
    UserPreference.where(key: "notification.follower.opt_out").find_each do |up|
      begin
        up.update_attributes!(key: "notification.follow", value: (up.value == "true" ? true : false))
      rescue => e
        log.error "Failed to update user preference for user_preference id: #{up.id}"
        next
      end
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160523234024'
  end  # task :port_follow_preferences
end  # namespace :after_party

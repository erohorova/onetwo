namespace :after_party do
  desc 'Deployment task: replacing_smartbite_to_smartcard_in_emailtemplates'
  task replacing_smartbite_to_smartcard_in_emailtemplates: :environment do
    puts "Running deploy task 'replacing_smartbite_to_smartcard_in_emailtemplates'"

    Organization.exclude_default_org.find_each do |org|
      new_activity_commented_smartbite = org.email_templates.find_by(
        title: 'new_activity_commented_smartbite'
      )
      unless new_activity_commented_smartbite.blank?
        new_activity_commented_smartbite.update(
          design: new_activity_commented_smartbite.design.gsub('SmartBite', 'SmartCard')
        )
      end

      new_smartbite_comment = org.email_templates.find_by(
        title: 'new_smartbite_comment'
      )
      unless new_smartbite_comment.blank?
        new_smartbite_comment.update(
          design: new_smartbite_comment.design.gsub('smartbite', 'SmartCard')
        )
      end
    end

    AfterParty::TaskRecord.create version: '20180917084131'
  end  # task :replacing_smartbite_to_smartcard_in_emailtemplates
end  # namespace :after_party

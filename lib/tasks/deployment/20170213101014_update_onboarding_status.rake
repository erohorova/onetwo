namespace :after_party do
  desc 'Deployment task: update_onboarding_status'
  task update_onboarding_status: :environment do
    puts "Running deploy task 'update_onboarding_status'"

    # Put your task implementation HERE.

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    UserOnboarding.where(status: "started").update_all(current_step: 2)
    AfterParty::TaskRecord.create version: '20170213101014'
  end  # task :update_onboarding_status
end  # namespace :after_party

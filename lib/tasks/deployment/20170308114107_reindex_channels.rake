namespace :after_party do
  desc 'Deployment task: reindex_channels'
  task reindex_channels: :environment do
    puts "Running deploy task 'reindex_channels'"

    # Put your task implementation HERE.

    # Reindex all channels for new search-field topics.
    Channel.reindex

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170308114107'
  end  # task :reindex_channels
end  # namespace :after_party

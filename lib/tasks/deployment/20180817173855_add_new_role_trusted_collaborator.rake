namespace :after_party do
  desc 'Deployment task: add_new_role_trusted_collaborator'
  task add_new_role_trusted_collaborator: :environment do
    puts "Running deploy task 'add_new_role_trusted_collaborator'"

    Organization.all.each do |org|
      role = Role.where(
        organization: org, default_name: 'trusted_collaborator',
        name: 'trusted_collaborator', master_role: true
      ).first_or_create
      role.create_role_permissions
    end

    AfterParty::TaskRecord.create version: '20180817173855'
  end  # task :add_new_role_trusted_collaborator
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: migrate_card_user_access_to_card_user_share'
  task migrate_card_user_access_to_card_user_share: :environment do
    puts "Running deploy task 'migrate_card_user_access_to_card_user_share'"

    # Put your task implementation HERE.
    CardUserAccess.all.each do |card_user_access|
      if card_user_access.card.present? && card_user_access.user.present?
        CardUserShare.find_or_create_by(card_id: card_user_access.card_id, user_id: card_user_access.user_id)
      end
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180608085612'
  end  # task :migrate_card_user_access_to_card_user_share
end  # namespace :after_party

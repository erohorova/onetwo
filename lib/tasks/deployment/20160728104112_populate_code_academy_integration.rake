namespace :after_party do
  desc 'Deployment task: populate_code_academy_integration'
  task populate_code_academy_integration: :environment do
    puts "Running deploy task 'populate_code_academy_integration'"

    description = '<p>Import completed skills from Codecademy daily.<br/><br/>You can determine your "codecademy_user_name" from your Codecademy public profile url.<br/>Eg: https://www.codecademy.com/codecademy_user_name<br/><br/><span class="mute-font">Note: Requires a public Codecademy profile.</span></p>'
    fields = '[{"name":"codecademy_username","label":"Codecademy Username","placeholder":"Codecademy username"}]'
    CODECADEMY = {
      "name"=>"Codecademy",
      "description"=>description,
      "enabled"=>true,
      "logo"=> File.open(Rails.root.join("app/assets/images", "codecademy_logo.jpg")),
      "fields" => fields
    }

    Integration.create(CODECADEMY)
    AfterParty::TaskRecord.create version: '20160728104112'
  end  # task :populate_code_academy_integration
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: lower_case_state_user_content_completion'
  task lower_case_state_user_content_completion: :environment do
    puts "Running deploy task 'lower_case_state_user_content_completion'"

    # Put your task implementation HERE.
    UserContentCompletion.where(state: 'STARTED').update_all(state: 'started')
    UserContentCompletion.where(state: 'COMPLETED').update_all(state: 'completed')

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170330005451'
  end  # task :lower_case_state_user_content_completion
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: generate_clc_badge_and_social_badges'
  task clc_badge_population: :environment do
    puts "Running deploy task 'clc_badge_population'"

    # Put your task implementation HERE.

    clc_badge_image_urls = [['https://d1iwkfmdo6oqxx.cloudfront.net/badges/Badge25.png', 'https://s3.amazonaws.com/edcast-qa-paperclip-store/Badge04_Variant1.png'],
                            ['https://d1iwkfmdo6oqxx.cloudfront.net/badges/Badge50.png', 'https://s3.amazonaws.com/edcast-qa-paperclip-store/Badge03_Variant1.png'],
                            ['https://d1iwkfmdo6oqxx.cloudfront.net/badges/Badge75.png', 'https://s3.amazonaws.com/edcast-qa-paperclip-store/Badge02_Variant1.png'],
                            ['https://d1iwkfmdo6oqxx.cloudfront.net/badges/Badge100.png', 'https://s3.amazonaws.com/edcast-qa-paperclip-store/Badge01_Variant1.png']
                           ] #default image urls here

    clc_badge_image_urls.each do |image_urls|
      orgs = Organization.all
      orgs.each do |org|
        org.clc_badges.create(image: open(image_urls[0]),
                               image_social: open(image_urls[1])
                              )
      end
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20171125105200'
  end  # task :clc_badge_population
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: generate_pluralsight_integration'
  task generate_pluralsight_integration: :environment do
    puts "Running deploy task 'generate_pluralsight_integration'"

    PLURAL_SIGHT = {
      "name"=>"Plural Sight",
      "description"=>"<p>Import completed courses from Pluralsight. <a href='https://www.pluralsight.com/'>http://app.pluralsight.com/profile/<b>Pluralsight username</b></a><br><u>Note</u>: Requires a public Pluralsight profile.</p>",
      "enabled"=>true,
      "logo"=> File.open(Rails.root.join("app/assets/images", "pluralsight-logo.png")),
      "fields" => [{"name":"plural_sight_user_name","label":"Plural Sight Username","placeholder":"Plural Sight Username"}].to_json

    }
    Integration.create(PLURAL_SIGHT)
    AfterParty::TaskRecord.create version: '20160721050421'
  end  # task :generate_pluralsight_integration
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: update_assignment_status_for_user_completed_card'
  task update_assignment_status_for_user_completed_card: :environment do
    puts "Running deploy task 'update_assignment_status_for_user_completed_card'"

    def fetch_assignment_for_content(user_content)
      Assignment
        .where(assignable: user_content.completable)
        .where.not(state: Assignment::COMPLETED)
        .where(assignee: user_content.user).first
    end

    # Put your task implementation HERE.
    UserContentCompletion
      .where(state: Assignment::COMPLETED)
      .where(completable_type: 'Card').find_each do |user_content|
        assignemnt = fetch_assignment_for_content user_content
        assignemnt&.set_status_user_completed_card
      end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180118063751'
  end  # task :update_assignment_status_for_user_completed_card
end  # namespace :after_party

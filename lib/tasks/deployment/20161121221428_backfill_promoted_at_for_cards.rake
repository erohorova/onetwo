namespace :after_party do
  desc 'Deployment task: backfill_promoted_at_for_cards'
  task backfill_promoted_at_for_cards: :environment do
    puts "Running deploy task 'backfill_promoted_at_for_cards'"

    # Put your task implementation HERE.
    Card.where(is_official: true, promoted_at: nil).find_in_batches(batch_size: 500) do |cards|
      cards.each do |card|
        begin
          card.update(promoted_at: card.created_at)
        rescue Exception => e
          log.error "Error while promoting card with id #{card.id}. Message: #{e.message}"
        end
      end
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20161121221428'
  end  # task :backfill_promoted_at_for_cards
end  # namespace :after_party

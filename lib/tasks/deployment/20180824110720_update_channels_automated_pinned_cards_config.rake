namespace :after_party do
  desc 'Deployment task: update_channels_automated_pinned_cards_config'
  task update_channels_automated_pinned_cards_config: :environment do
    puts "Running deploy task 'update_channels_automated_pinned_cards_config'"

    # Put your task implementation HERE.
    # 9 organizations where automated pinned cards feature is enabled
    Organization.automated_pinned_carousel_enabled.each do |org|
      org.channels.update_all(auto_pin_cards: true)
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180824110720'
  end  # task :update_channels_automated_pinned_cards_config
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: remove_bad_notifications'
  task remove_bad_notifications: :environment do
    puts "Running deploy task 'remove_bad_notifications'"

    # Put your task implementation HERE.
    to_be_deleted = []
    Notification.where(notifiable_type: "comment").find_each do |notif|
      unless Comment.where(id: notif.notifiable_id).present?
        to_be_deleted << notif.id
      end
    end
    Notification.where(id: to_be_deleted).destroy_all

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160804173803'
  end  # task :remove_bad_notifications
end  # namespace :after_party

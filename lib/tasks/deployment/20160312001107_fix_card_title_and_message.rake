namespace :after_party do
  desc 'Deployment task: Fix empty card title and message'
  task fix_card_title_and_message: :environment do
    puts "Running deploy task 'fix_card_title_and_message'"

    def get_content_map card
      contents = {}
      card.cards_contents.each do |content|
        contents[content.get_key] = content.get_value
      end
      contents
    end

    # We couldnt trace why cards were created without title and message
    # on March 10, 2016 between 7 PM to March 11, 1 AM PST (hackathon day)
    Card.where("title is NULL or message is NULL").find_each do |card|
      begin
        contents = get_content_map card
        if !contents.blank? && (contents['title'].blank? || contents['message'].blank? || contents['description'].blank?)
          log.debug "Card title or message is blank for #{card.id}"
        end
        card.update_columns(title: contents['title'], message: contents['message'] || contents['description'])
      rescue => e
        log.error "error saving title and message for card id: #{card.id}. exception: #{e.message}"
      end
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160312001107'
  end  # task :fix_card_title_and_message
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: add_admin_permission_to_be_able_create_channel'
  task update_admin_permissions: :environment do
    puts "Running deploy task 'update_admin_permissions'"

    # Put your task implementation HERE.
    Role.admins.find_each{|r| r.create_role_permissions }
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180330092317'
  end  # task :update_admin_permissions
end  # namespace :after_party

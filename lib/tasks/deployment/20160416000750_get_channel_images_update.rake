namespace :after_party do
  desc 'Deployment task: get_channel_images_update'
  task get_channel_images_update: :environment do
    puts "Running deploy task 'get_channel_images_update'"

    channel = Channel.find_by_slug('get')
    channel.mobile_image = "https://www.edcast.com/assets/GET_logo.jpeg"
    channel.image = "https://www.edcast.com/assets/GET_Banner.jpg"
    channel.save(validate: false)

    AfterParty::TaskRecord.create version: '20160416000750'
  end  # task :get_channel_images_update
end  # namespace :after_party

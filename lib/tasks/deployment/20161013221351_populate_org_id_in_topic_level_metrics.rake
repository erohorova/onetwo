namespace :after_party do
  desc 'Deployment task: populate_org_id_in_topic_level_metrics'
  task populate_org_id_in_topic_level_metrics: :environment do
    puts "Running deploy task 'populate_org_id_in_topic_level_metrics'"

    # Put your task implementation HERE.
    UserTopicLevelMetric.find_each do |metric|
      metric.update_attributes(organization_id: metric.user.organization_id)
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20161013221351'
  end  # task :populate_org_id_in_topic_level_metrics
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: create_default_config_for_orgs'
  task create_default_config_for_orgs: :environment do
    puts "Running deploy task 'create_default_config_for_orgs'"

    # Put your task implementation HERE.
    orgs = Organization.all
    orgs.each do |org|
      org.send :create_default_config
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160323002527'
  end  # task :create_default_config_for_orgs
end  # namespace :after_party

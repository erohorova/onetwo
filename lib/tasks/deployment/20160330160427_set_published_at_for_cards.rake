namespace :after_party do
  desc 'Deployment task: set_published_at_for_cards'
  task set_published_at_for_cards: :environment do
    puts "Running deploy task 'set_published_at_for_cards'"

    # Put your task implementation HERE.
    Card.published.find_each {|c| c.update_columns(published_at: c.created_at)}
    Card.reindex
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160330160427'
  end  # task :set_published_at_for_cards
end  # namespace :after_party

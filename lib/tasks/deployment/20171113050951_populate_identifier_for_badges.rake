namespace :after_party do
  desc 'Deployment task: populate_identifier_for_badges'
  task populate_identifier_for_badges: :environment do
    puts "Running deploy task 'populate_identifier_for_badges'"

    UserBadge.all.each do |user_badge|
      user_badge.generate_identifier
      user_badge.save
    end

    AfterParty::TaskRecord.create version: '20171113050951'
  end  # task :populate_identifier_for_badges
end  # namespace :after_party

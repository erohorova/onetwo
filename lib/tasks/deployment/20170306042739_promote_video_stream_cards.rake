namespace :after_party do
  desc 'Deployment task: promote_video_stream_cards'
  task promote_video_stream_cards: :environment do
    puts "Running deploy task 'promote_video_stream_cards'"

    card_ids = VideoStream.promoted.pluck(:card_id).compact
    cards = Card.where(id: card_ids).where(is_official: false)

    if cards
      cards.update_all(is_official: true)
      cards.each do |card|
        begin
          card.reindex 
        rescue Exception
          logs.error "Unable to reindex card with id: #{card.id}"
        end
      end
    end

    

    AfterParty::TaskRecord.create version: '20170306042739'
  end  # task :promote_video_stream_cards
end  # namespace :after_party

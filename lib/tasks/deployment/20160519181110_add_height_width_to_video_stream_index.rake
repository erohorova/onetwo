namespace :after_party do
  desc 'Deployment task: add_height_width_to_video_stream_index'
  task add_height_width_to_video_stream_index: :environment do
    VideoStream.find_in_batches.each do |batch|
      batch.each do |video_stream|
        video_stream.partial_update = true
        video_stream.update_hash = { width: video_stream.width, height: video_stream.height }
        video_stream.update_index
      end
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160519181110'
  end  # task :add_height_width_to_video_stream_index
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: backfill_comment_type_for_announcement'
  task backfill_comment_type_for_announcement: :environment do
    puts "Running deploy task 'backfill_comment_type_for_announcement'"
    announcement_ids = Post.where(type: 'Announcement').pluck(:id)
    Comment.where(commentable_id: announcement_ids, commentable_type: "Post").update_all(type: 'Answer')
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160726084302'
  end  # task :backfill_comment_type_for_announcement
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: add_permission_create_livestream_for_all_roles'
  task add_permission_create_livestream_for_all_roles: :environment do
    puts "Running deploy task 'add_permission_create_livestream_for_all_roles'"

    RolePermission.where(name: 'DISABLE_LIVESTREAM_CREATION').destroy_all
    Role.where(name: ['admin', 'member', 'collaborator']).each do |role|
        role.role_permissions.create(name: 'CREATE_LIVESTREAM', description: 'Create Livestream')
    end
    # Put your task implementation HERE.

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180308063341'
  end  # task :add_permission_create_livestream_for_all_roles
end  # namespace :after_party

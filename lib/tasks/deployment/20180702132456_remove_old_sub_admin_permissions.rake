namespace :after_party do
  desc 'Deployment task: remove_old_sub_admin_permissions'
  task remove_old_sub_admin_permissions: :environment do
    puts "Running deploy task 'remove_old_sub_admin_permissions'"

    permissions = %w[
      CAN_MANAGE_CONTENT CAN_MANAGE_CONTENT_LIMITED CAN_MANAGE_CHANNELS
      CAN_MANAGE_CHANNELS_LIMITED CAN_MANAGE_ACCOUNTS CAN_MANAGE_ACCOUNTS_LIMITED
      CAN_MANAGE_ANALYTICS CAN_MANAGE_ANALYTICS_LIMITED
    ]

    RolePermission.where(name: permissions).find_in_batches do |ids|
      RolePermission.where(id: ids).destroy_all
    end

    AfterParty::TaskRecord.create version: '20180702132456'
  end  # task :remove_old_sub_admin_permissions
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: remove_duplicate_users_from_teams'
  task remove_duplicate_users_from_teams: :environment do
    puts "Running deploy task 'remove_duplicate_users_from_teams'"

    # Put your task implementation HERE.
    Team.all.each do |team|
      team.teams_users.group_by {|tu| [tu.team_id, tu.user_id, tu.as_type]}.values.each do |duplicates|
        first_rec = duplicates.shift #pop first record
        duplicates.each {|rec| rec.destroy}
      end
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160923224221'
  end  # task :remove_duplicate_users_from_teams
end  # namespace :after_party

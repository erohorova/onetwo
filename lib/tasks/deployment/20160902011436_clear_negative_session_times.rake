namespace :after_party do
  desc 'Deployment task: clear_negative_session_times'
  task clear_negative_session_times: :environment do
    puts "Running deploy task 'clear_negative_session_times'"

    # Put your task implementation HERE.
    UserLevelMetric.where("time_spent < 0").update_all(time_spent: 0)

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160902011436'
  end  # task :clear_negative_session_times
end  # namespace :after_party

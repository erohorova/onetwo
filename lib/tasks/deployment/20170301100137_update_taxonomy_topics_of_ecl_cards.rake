namespace :after_party do
  desc 'Deployment task: update_taxonomy_topics_of_ecl_cards'
  task update_taxonomy_topics_of_ecl_cards: :environment do
    puts "Running deploy task 'update_taxonomy_topics_of_ecl_cards'"

    # Put your task implementation HERE.

    Card.where('ecl_id IS NOT NULL').find_each do |card|
      EclCardSyncJob.perform_later(card.id)
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170301100137'
  end  # task :update_taxonomy_topics_of_ecl_cards
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: update_create_channel_config_for_default_org'
  task update_create_channel_config_for_default_org: :environment do
    puts "Running deploy task 'update_create_channel_config_for_default_org'"

    # Put your task implementation HERE.
    org = Organization.default_org
    org.configs.find_by_name('permissions/channel/member_can_create_channel').update(value: 'false')

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160517044054'
  end  # task :update_create_channel_config_for_default_org
end  # namespace :after_party

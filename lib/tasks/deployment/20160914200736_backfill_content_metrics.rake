namespace :after_party do
  desc 'Deployment task: backfill_content_metrics'
  task backfill_content_metrics: :environment do
    puts "Running deploy task 'backfill_content_metrics'"

    # Put your task implementation HERE.
    default_org_id = Organization.default_org.id
    Card.where.not(organization_id: default_org_id).find_each do |card|
      MetricRecord.reaggregate_content_level_metrics(card.id, 'card')
      if card.card_type == 'pack'
        MetricRecord.reaggregate_content_level_metrics(card.id, 'collection')
      end
    end

    VideoStream.where.not(organization_id: default_org_id).find_each do |video_stream|
      MetricRecord.reaggregate_content_level_metrics(video_stream.id, video_stream.class.name.underscore)
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160914200736'
  end  # task :backfill_content_metrics
end  # namespace :after_party

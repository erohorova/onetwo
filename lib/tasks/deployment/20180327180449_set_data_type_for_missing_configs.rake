namespace :after_party do
  desc 'Deployment task: set_data_type_for_missing_configs'
  task set_data_type_for_missing_configs: :environment do
    puts "Running deploy task 'set_data_type_for_missing_configs'"

    # Put your task implementation HERE.
    # ABOUT 3270 with boolean value
    Config.where(value: ['true', 'false'], data_type: nil).update_all(data_type: 'boolean')

    # ABOUT 178 String values
    # Rest of them are string/integer. JSON values are already set as `json`
    string_configs_ids = []
    integer_configs_ids = []
    integer_regex = /\A\d+\z/
    Config.where(data_type: nil).each do |config|
      if integer_regex.match config.value
        integer_configs_ids << config.id
      else
        string_configs_ids << config.id
      end
    end

    Config.where(id: string_configs_ids).update_all data_type: 'string'
    Config.where(id: integer_configs_ids).update_all data_type: 'integer'

    # print remaining config ids
    ids = Config.where(data_type: nil).pluck :id
    log.info "REMAINING CONFIG IDS: #{ids.join(', ')}"

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180327180449'
  end  # task :set_data_type_for_missing_configs
end  # namespace :after_party

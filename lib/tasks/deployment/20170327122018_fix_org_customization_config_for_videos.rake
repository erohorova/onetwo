namespace :after_party do
  desc 'Deployment task: fix_org_customization_config_for_videos'
  task fix_org_customization_config_for_videos: :environment do
    puts "Running deploy task 'fix_org_customization_config_for_videos'"

    # Put your task implementation HERE.

    Config.where(name: "OrgCustomizationConfig").find_each do |config|
      value = config.value
      value.gsub!(/livestreams/, 'videos')
      value.gsub!(/Livestreams/, 'Videos')
      config.update(value: value)
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170327122018'
  end  # task :fix_org_customization_config_for_videos
end  # namespace :after_party

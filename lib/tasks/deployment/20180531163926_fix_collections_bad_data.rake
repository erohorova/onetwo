namespace :after_party do
  desc 'Deployment task: fix_collections_bad_data'
  task fix_collections_bad_data: :environment do
    puts "Running deploy task 'fix_collections_bad_data'"

    limit = 1000
    offset = 0
    # getting max times we should processed deleted card with 1k batch
    rounds = (Card.with_deleted.where.not(deleted_at: nil).count.to_f / limit).ceil
    total = 0
    cprs = []
    jprs = []

    rounds.times do
      card_ids = Card.with_deleted.where.not(deleted_at: nil)
        .limit(limit)
        .offset(offset)
        .pluck(:id)

      processed = card_ids.count
      cpr = CardPackRelation.where(
        from_id: card_ids,
        deleted: false
      )
      jpr = JourneyPackRelation.where(
        from_id: card_ids,
        deleted: false
      )
      if cpr.present?
        cpr.update_all(deleted: true)
        cprs.push(cpr.pluck(:id))
      end
      if jpr.present?
        jpr.update_all(deleted: true)
        jprs.push(jpr.pluck(:id))
      end
      offset += 1000
      total += processed
    end

    addition = " Card processed: #{total},
       CPR ids fixed: #{cprs.join(', ')},
       JPR ids fixed: #{cprs.join(', ')}"

    puts "Deploy task finished: 'fix_collections_bad_data'. " + addition

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180531163926'
  end  # task :fix_collections_bad_data
end  # namespace :after_party

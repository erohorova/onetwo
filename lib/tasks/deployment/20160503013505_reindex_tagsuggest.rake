namespace :after_party do
  desc 'Deployment task: reindex_tagsuggest'
  task reindex_tagsuggest: :environment do
    puts "Running deploy task 'reindex_tagsuggest'"

    # Put your task implementation HERE.
    TagSuggest.reindex

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160503013505'
  end  # task :reindex_tagsuggest
end  # namespace :after_party

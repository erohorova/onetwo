namespace :after_party do
  desc 'Deployment task: EP-11992'
  task reset_readable_card_type: :environment do
    puts "Running deploy task 'reset_readable_card_type'"

    cards = Card.joins("inner JOIN resources ON resources.id = cards.resource_id")
                .where("cards.card_subtype = 'link' and resources.url like 'https://www.youtube.com/watch%'
                 and cards.readable_card_type is null and resources.type is null")
 	  
    cards.update_all(readable_card_type: Card.readable_card_type('video'))	
    
    cards.map(&:index)
    
   
    # Put your task implementation HERE.

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180403064222'
  end  # task :reset_readable_card_type
end  # namespace :after_party

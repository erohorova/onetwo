namespace :after_party do
  desc 'Deployment task: update_pack_draft_pathways'
  task update_pack_draft_pathways: :environment do
    puts "Running deploy task 'update_pack_draft_pathways'"

    # Put your task implementation HERE.
    Organization.where.not(host_name: 'www').each do |org|
      org.cards.where(card_type: 'pack_draft').find_in_batches(batch_size: 100).each do |cards|
        cards.each do |card|
          if !card.update_attributes(card_type: 'pack', state: 'draft')
            log.error "Error while updating card with Id: #{card.id}"
          end
        end
      end
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170207212910'
  end  # task :update_pack_draft_pathways
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: migrate_video_streams_to_cards'
  task migrate_video_streams_to_cards: :environment do
    puts "Running deploy task 'migrate_video_streams_to_cards'"

    VideoStream.find_in_batches(batch_size: 5) do |video_streams|
      video_streams.each do |video_stream|
        card = video_stream.card || video_stream.create_card
        card.comments_count = video_stream.comments_count
        card.votes_count = video_stream.votes_count
        if card.save
          video_stream.update_column(:card_id, card.id)
          begin
            update_video_stream_data(video_stream)
          rescue Exception => e
            log.error "Unable to migrate video_stream with id: #{video_stream.id} with Exception: #{e.message}"
          end
          video_stream.reindex
        end
        puts "Completed with video_stream : #{video_stream.id}"
      end
    end
    AfterParty::TaskRecord.create version: '20161207035506'
  end  # task :migrate_video_streams_to_cards

  def update_video_stream_data(video_stream)
    # {
    #   channels
    #   tags 
    #   file_resource - attachable
    #   votes - votable
    #   comments - commentable
    #   xapi_activities - object
    #   activity_streams - streamable, action will remains same
    #   learning_queue_items - queueable. Also display_type, deep_link will remains same
    #   Assignment - assignable, title
    #   bookmarks
    #   Notification
    # }
    
    card = video_stream.card

    #tags
    card.tags += video_stream.tags

    #votes
    up_votes = Vote.where(votable: video_stream)
    up_votes.update_all(votable_id: card.id, votable_type: "Card")

    #comments
    comments = Comment.where(commentable: video_stream)
    comments.update_all(commentable_id: card.id, commentable_type: "Card")

    #xapi_activities
    xapi_activities = XapiActivity.where(object: video_stream)
    xapi_activities.update_all(object_id: card.id, object_type: "Card")

    #activity_streams
    activity_streams = ActivityStream.where(streamable: video_stream)
    activity_streams.update_all(streamable_id: card.id, streamable_type: "Card")

    #learning_queue_items
    learning_queue_items = LearningQueueItem.where(queueable: video_stream)
    learning_queue_items.update_all(queueable_id: card.id, queueable_type: "Card")

    assignments = Assignment.where(assignable: video_stream)
    assignments.update_all(assignable_id: card.id, assignable_type: "Card")

    #bookmarks
    bookmarks = Bookmark.where(bookmarkable: video_stream)
    bookmarks.update_all(bookmarkable_id: card.id, bookmarkable_type: "Card")

    #notifications
    notifications = Notification.where(notifiable: video_stream)
    notifications.update_all(notifiable_id: card.id, notifiable_type: "Card")
    
    #file_resource
    file_resources = FileResource.where(attachable: video_stream)
    file_resources.update_all(attachable_id: card.id, attachable_type: "Card")

    #channels
    channels = ChannelsVideoStream.where(video_stream_id: video_stream.id).map(&:channel)
    card.channels << channels
  end
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: update_card_pack_relation_mapping_of_deleted_cards'
  task update_card_pack_relation_mapping_of_deleted_cards: :environment do
    puts "Running deploy task 'update_card_pack_relation_mapping_of_deleted_cards'"

    # Put your task implementation HERE.
    # Production database has 248 records of pathways which have wrong card_pack relation.
    cpr_cover_ids = CardPackRelation.find_by_sql('SELECT DISTINCT(`card_pack_relations`.`cover_id`) from `card_pack_relations` where 
      `card_pack_relations`.to_id NOT IN (SELECT `card_pack_relations`.id from card_pack_relations)')
    cpr_cover_ids.each do |cpr|
      pathway_cards = CardPackRelation.where(cover_id: cpr.cover_id).order(:to_id)
      cpr_ids, cpr_to_ids = pathway_cards.map(&:id), pathway_cards.map(&:to_id).compact
      cpr_having_wrong_relation = pathway_cards.select do |cpr_wrong|
        cpr_ids.exclude?(cpr_wrong.to_id) if cpr_wrong.to_id.present?
      end
      cpr_ids_not_in_relation = pathway_cards.collect do |cpr_right|
        cpr_right.id if (cpr_right.cover_id != cpr_right.from_id) && cpr_to_ids.exclude?(cpr_right.id) 
      end.compact
      begin
        cpr_having_wrong_relation.each do |cpr_map|
          cpr_map.update_columns(to_id: cpr_ids_not_in_relation.shift)
        end
      rescue Exception => e
        puts "Unable to save correct pack_card_relation for cover_id: #{cpr.cover_id}"
      end
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180629103018'
  end  # task :update_card_pack_relation_mapping_of_deleted_cards
end  # namespace :after_party

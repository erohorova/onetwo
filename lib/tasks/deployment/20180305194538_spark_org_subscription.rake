namespace :after_party do
  desc 'Deployment task: create_spark_org_subscription_and_prices'
  task spark_org_subscription: :environment do
    puts "Running deploy task 'spark_org_subscription'"

    org = Organization.find_by(slug: 'spark')
    if org.present?
      # set member_pay true for paid organizations.
      organization_profile = org.create_organization_profile!(member_pay: true)

      # create organization_subscriptions for paid organizations.
      org_subscription = org.org_subscriptions.create!(name: 'EdCast Spark - Annual Subscription Fees',
                                                       description: 'Your Spark subscription fee will be reimbursed automatically by your employer, EdCast upon achieving the 2018 continuous learning goals of 50 hours and earning 4 badges over the 4 quarters.',
                                                       start_date: Date.today,
                                                       end_date: Date.today + 1.year,
                                                       sku: "EdCast Instance name: Spark - Annual Subscription Fees")

      # Price: $60/ user/year
      org_subscription_prices = org_subscription.prices.create!(currency: 'USD',
                                                                amount: BigDecimal.new("60"))
      # Price: INR 3,500/ user/year
      org_subscription_prices = org_subscription.prices.create!(currency: 'INR',
                                                                amount: BigDecimal.new("3500"))
    end

    AfterParty::TaskRecord.create version: '20180305194538'
  end  # task :spark_org_subscription
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: update_default_label_courses_in_channel_configs'
  task update_default_label_courses_in_channel_configs: :environment do
    puts "Running deploy task 'update_default_label_courses_in_channel_configs'"

    configs = Config.where(configable_type:'Channel', name: 'channel_carousels')
    configs.find_each do |config|
      value = config.value.gsub(/Сourses/,'Courses')
      config.update(value: value)
    end

    AfterParty::TaskRecord.create version: '20180921085032'
  end  # task :update_default_label_courses_in_channel_configs
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: reindex_users'
  task reindex_users: :environment do
    puts "Running deploy task 'reindex_users'"

    # Put your task implementation HERE.
    User.reindex

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160909005439'
  end  # task :reindex_users
end  # namespace :after_party

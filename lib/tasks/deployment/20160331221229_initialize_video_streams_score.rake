namespace :after_party do
  desc 'Deployment task: initialize_video_streams_score'
  task initialize_video_streams_score: :environment do
    puts "Running deploy task 'initialize_video_streams_score'"
    InitializeScoreInVideoStreamsJob.perform_later
    AfterParty::TaskRecord.create version: '20160331221229'
  end  # task :initialize_video_streams_score
end  # namespace :after_party

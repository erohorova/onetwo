namespace :after_party do
  desc 'Deployment task: add_permission_publish_links_for_all_roles'
  task add_permission_publish_links_for_all_roles: :environment do
    puts "Running deploy task 'add_permission_publish_links_for_all_roles'"

    # Put your task implementation HERE.
    Role.where(name: ['admin', 'member', 'collaborator']).each do |role|
      role.role_permissions.create(name: 'PUBLISH_LINKS', description: 'Publish Links')
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180515120151'
  end  # task :add_permission_publish_links_for_all_roles
end  # namespace :after_party

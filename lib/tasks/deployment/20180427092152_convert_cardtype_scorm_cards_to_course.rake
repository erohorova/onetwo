namespace :after_party do
  desc 'Deployment task: convert_cardtype_scorm_cards_to_course'
  task convert_cardtype_scorm_cards_to_course: :environment do
    puts "Running deploy task 'convert_cardtype_scorm_cards_to_course'"

    # Put your task implementation HERE.
    scorm_resources_ids  = Resource.where("url LIKE :query", query: "%/api/scorm/launch?course_id%").pluck(:id)
    puts "Running for total no of #{scorm_resources_ids.size} resources "
    cards = Card.where(resource_id: scorm_resources_ids, card_type: 'media', card_subtype: 'link')
    puts "Running for total no of #{cards.size} cards "
    puts "Updating cards"
    cards.each { |card| card.update(card_type: 'course') }
    puts "Finished updation of cards"

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180427092152'
  end  # task :convert_cardtype_scorm_cards_to_course
end  # namespace :after_party

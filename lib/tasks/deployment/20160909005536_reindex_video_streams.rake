namespace :after_party do
  desc 'Deployment task: reindex_video_streams'
  task reindex_video_streams: :environment do
    puts "Running deploy task 'reindex_video_streams'"

    # Put your task implementation HERE.
    VideoStream.reindex
    VideoStream.find_each do |vs|
      VideoStreamUpdateScoreJob.perform_later(vs)
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160909005536'
  end  # task :reindex_video_streams
end  # namespace :after_party

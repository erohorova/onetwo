namespace :after_party do
  desc 'Deployment task: populate_gateway_id_user_subscriptions'
  task populate_gateway_id_user_subscriptions: :environment do
    puts "Running deploy task 'populate_gateway_id_user_subscriptions'"

    # Put your task implementation HERE.
    UserSubscription.all.each do |user_subscription|
    	order = Order.find_by(id: user_subscription.order_id)
       gateway_id = order.order_transaction.gateway_id
    	user_subscription.update_column('gateway_id', gateway_id)
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180321114946'
  end  # task :populate_gateway_id_user_subscriptions
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: add_ecl_source_name_to_cards'
  task add_ecl_source_name_to_cards: :environment do
    puts "Running deploy task 'add_ecl_source_name_to_cards'"
    #41330 rows going to affect

    Card.where.not(organization_id: 1).where(card_type: 'course').find_in_batches do |group|
      group.each do |card|
        if card.ecl_metadata.present? && card.ecl_metadata['source_display_name'].present?
          card.update_attributes(ecl_source_name: card.ecl_metadata['source_display_name'])
        end
      end
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170901115036'
  end  # task :add_ecl_source_name_to_cards
end  # namespace :after_party

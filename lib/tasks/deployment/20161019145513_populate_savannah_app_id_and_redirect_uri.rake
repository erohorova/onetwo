namespace :after_party do
  desc 'Deployment task: populate_savannah_app_id_and_redirect_uri'
  task populate_savannah_app_id_and_redirect_uri: :environment do
    puts "Running deploy task 'populate_savannah_app_id_and_redirect_uri'"
 	module Savannah
      class SavannahDatabase < ActiveRecord::Base
        db_url = ENV['SAVANNAH_DATABASE_URL']
        establish_connection(db_url)
        self.abstract_class = true
      end
      class App < SavannahDatabase
        acts_as_paranoid
      end
    end

    begin
      organizations = Organization.where("client_id IS NOT NULL")
      apps = Savannah::App.default_scoped.as_json
      organizations.each do |org|
        client = org.client
        next unless client
        app_id = apps.detect {|a| [a["name"], a["host_name"]].include?(client.name)}.try(:[], "id")
        next unless app_id
        updated = org.update(savannah_app_id: app_id, redirect_uri: client.redirect_uri, social_enabled: client.social_enabled)
        if updated
          msg = "Updated organization with id: #{org.id} with app_id #{app_id}"
          puts msg
          log.info msg
        else
          error = "Failed to update organization with id: #{org.id}"
          puts error
          log.error error
        end
      end
    rescue Exception => e
      log.error "Faild to populate savannah_app_id due to exception #{e}"
    end

    AfterParty::TaskRecord.create version: '20161019145513'
  end  # task :populate_savannah_app_id_and_redirect_uri
end  # namespace :after_party

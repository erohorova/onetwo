namespace :after_party do
  desc 'Deployment task: backfill_org_metrics'
  task backfill_org_metrics: :environment do
    puts "Running deploy task 'backfill_org_metrics'"

    # Put your task implementation HERE.
    Organization.where.not(id: Organization.default_org.id).find_each do |org|
      MetricRecord.reaggregate_org_level_metrics(org.id)
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160808211025'
  end  # task :backfill_org_metrics
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: teams_reindex'
  task teams_reindex: :environment do
    puts "Running deploy task 'teams_reindex'"

    # Put your task implementation HERE.
    Team.find_each { |t| t.reindex }
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180711103054'
  end  # task :teams_reindex
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: delete_entry_from_associated_tables_for_deleted_cards'
  task delete_entry_from_associated_tables_for_deleted_cards: :environment do
    puts "Running deploy task 'delete_entry_from_associated_tables_for_deleted_cards'"

    # Put your task implementation HERE.
    # 410 entries
    card_user_shares_records = CardUserShare.find_by_sql("SELECT * FROM card_user_shares INNER JOIN cards ON
      cards.id = card_user_shares.card_id WHERE (cards.deleted_at IS NOT NULL)")
    card_user_shares_records.each do |record|
      record.destroy
    end

    # 1 entry
    card_user_permissions_records = CardUserPermission.find_by_sql("SELECT * FROM card_user_permissions INNER JOIN cards ON
      cards.id = card_user_permissions.card_id WHERE (cards.deleted_at IS NOT NULL)")
    card_team_permissions_records.each do |record|
      record.destroy
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20181205123623'
  end  # task :delete_entry_from_associated_tables_for_deleted_cards
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: change_the_not_suspended_users_statuses_to_active'
  task change_users_status: :environment do
    puts "Running deploy task 'change_users_status'"

    # Put your task implementation HERE.
    User.where(status: nil).update_all(status: User::ACTIVE_STATUS)
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180315105336'
  end  # task :change_users_status
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: update_average_rating_in_card_metadata'
  task update_average_rating_in_card_metadata: :environment do
    puts "Running deploy task 'update_average_rating_in_card_metadata'"

    # Put your task implementation HERE.
    # There are around 21k records of CardsRating on prod.

    # We can run this task manually through EB console
    # Dont want to call ECL through callback update of 21k CardsRating records

    # Card.joins(:cards_ratings).where.not(cards_ratings: {id: nil}).find_each do |card|
    #   average_rating = card.cards_ratings.average(:rating)
    #   if average_rating
    #     card_metadata = card.card_metadatum || card.build_card_metadatum
    #     card_metadata.average_rating = average_rating
    #     card_metadata.save
    #   end
    # end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180529070606'
  end  # task :update_average_rating_in_card_metadata
end  # namespace :after_party

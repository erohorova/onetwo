namespace :after_party do
  desc 'Deployment task: fix_deleted_group_assignments'
  task fix_deleted_group_assignments: :environment do
    puts "Running deploy task 'fix_deleted_group_assignments'"

    # Put your task implementation HERE.
    deleted_team_ids = TeamAssignment.pluck(:team_id).uniq - Team.pluck(:id)
    TeamAssignment.where(team_id: deleted_team_ids).destroy_all

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20161012182819'
  end  # task :fix_deleted_group_assignments
end  # namespace :after_party

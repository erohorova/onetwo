namespace :after_party do
  desc 'Deployment task: delete_structured_items_if_source_entity_is_nil'
  task delete_structured_items_if_source_entity_is_nil: :environment do
    puts "Running deploy task 'delete_structured_items_if_source_entity_is_nil'"
    Structure.find_each do |structure|
      if structure.parent.present?
        if structure.parent_type=="Organization"
          structure.structured_items.each do |si|
            if (si.entity.present? && si.entity_type=="User" && si.entity.is_suspended?) || (si.entity.nil?)
              si.destroy
            end
          end
        elsif structure.parent_type=="Channel"
          channel_card_ids=structure.parent.channels_cards.pluck(:card_id)
          structured_items_entity_ids=structure.structured_items.pluck(:entity_id)
          deleted_channel_card_id=structured_items_entity_ids-channel_card_ids
          StructuredItem.where(entity_type: "Card", entity_id: deleted_channel_card_id).destroy_all
        end
      else
        structure.destroy
      end
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180611072011'
  end  # task :delete_structured_items_if_source_entity_is_nil
end  # namespace :after_party

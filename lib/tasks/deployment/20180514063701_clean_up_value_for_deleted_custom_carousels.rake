namespace :after_party do
desc 'Deployment task: clean_up_value_for_deleted_custom_carousels'
  task clean_up_value_for_deleted_custom_carousels: :environment do
    puts "Running deploy task 'clean_up_value_for_deleted_custom_carousels'"

    # Put your task implementation HERE.
    Config.where(name: 'OrgCustomizationConfig', configable_type: 'Organization').each do |config|
      if config.configable.present?
        value = ActiveSupport::JSON.decode(config.value)
        discover = value['discover']
        if discover.present? && discover.keys.detect { |key| key.to_s =~ /\A(discover\/carousel\/customCarousel\/channel\/)/ }.present?
          custom_carousels = discover.keys.select { |key| key.to_s =~ /\A(discover\/carousel\/customCarousel\/channel\/)/ }
          custom_carousel_ids = custom_carousels.map { |key| key.split('discover/carousel/customCarousel/channel/').last.to_i  }
          del_cust_carousel_ids = custom_carousel_ids - Structure.where(id: custom_carousel_ids).map(&:id)
          del_cust_carousel_ids.each do |structure_id|
            discover.except!("discover/carousel/customCarousel/channel/#{structure_id}")
          end
        end
        value['discover'] = discover
        puts "Updating config for organization id: #{config.configable_id}, configable_type: #{config.configable_type}, config: #{config.id}"
        config.update_attributes(value: value.to_json)
      end
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180514063701'
  end  # task :clean_up_value_for_deleted_custom_carousels
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: generate_new_integrations'
  task generate_new_integrations: :environment do
    puts "Running deploy task 'generate_new_integrations'"

    puts "creating Future Learn intgration"
    FUTURE_LEARN = {
      "name"=>"Future Learn",
      "description"=>"<p>FutureLearn is a massive open online course (MOOC) learning platform focused on offering courses from leading universities and cultural institutions from around the world.<br>You can determine your Future Learn ProfileId from your Future Learn Profile:<a href=\"https://www.futurelearn.com\"> https://www.futurelearn.com/profiles/futureLearnId</a><br><u>Note</u>: You have to make your profile page public from Edit profile.</p>",
      "enabled"=>true,
      "logo"=> File.open(Rails.root.join("app/assets/images", "future_learn.jpeg")),
      "fields" => [{"name":"future_learn_profile_id","label":"Future Learn Profile Id","placeholder":"Future Learn"}].to_json

    }
    Integration.create(FUTURE_LEARN)

    puts "creating Skill Share intgration"
    SKILL_SHARE = {
      "name"=>"Skill Share",
      "description"=>"<p>Skillshare is an online learning community offering courses from leading universities and cultural institutions from around the world.</p>",
      "enabled"=>true,
      "logo"=> File.open(Rails.root.join("app/assets/images", "skill_share.png")),
      "fields" => [{"name":"skill_share_user_name","label":"Skill Share Username","placeholder":"Skill share username"}].to_json

    }
    Integration.create(SKILL_SHARE)

    AfterParty::TaskRecord.create version: '20160721043740'
  end  # task :generate_new_integrations
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: Store flag which will choose braintree OR paypal based on organization'
  task populate_braintree_paypal_configuration_configs: :environment do
    puts "Running deploy task 'populate_braintree_paypal_configuration_configs'"

    # Put your task implementation HERE.
    org = Organization.find_by(slug: 'spark')
    config_params = { 
                  name: 'payment_gateway',
                  value: { braintree: 1 }.to_json,
                  category:'settings',
                  data_type: 'json',
                  description: 'Store gateway used by the organization' 
                }
    org.configs.create(config_params) if org.present?

    org = Organization.find_by(slug: 'futureskillsnasscom')
    config_params = { 
                  name: 'payment_gateway',
                  value: { paypal: 1 }.to_json,
                  category:'settings',
                  data_type: 'json',
                  description: 'Store gateway used by the organization' 
                }
    org.configs.create(config_params) if org.present?

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180514143944'
  end  # task :populate_braintree_paypal_configuration_configs
end  # namespace :after_party

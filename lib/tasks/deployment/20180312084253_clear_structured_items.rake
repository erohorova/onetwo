namespace :after_party do
  desc 'Deployment task: clear_structured_items_where_entity_is_not_present'
  task clear_structured_items: :environment do
    puts "Running deploy task 'clear_structured_items'"

    # Put your task implementation HERE.
    StructuredItem.where(entity_type: "Channel").includes(:entity).find_each do |item|
      item.destroy if item.entity.nil?
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180312084253'
  end  # task :clear_structured_items
end  # namespace :after_party

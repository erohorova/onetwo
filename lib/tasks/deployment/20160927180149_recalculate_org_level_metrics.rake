namespace :after_party do
  desc 'Deployment task: recalculate_org_level_metrics'
  task recalculate_org_level_metrics: :environment do
    puts "Running deploy task 'recalculate_org_level_metrics'"

    # Put your task implementation HERE.
    OrganizationLevelMetric.delete_all
    Organization.where.not(id: Organization.default_org.id).find_each do |org|
      MetricRecord.reaggregate_org_level_metrics(org.id)
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160927180149'
  end  # task :recalculate_org_level_metrics
end  # namespace :after_party

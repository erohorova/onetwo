namespace :after_party do
  desc 'Deployment task: create_es_indexes_for_versioning'
  task create_es_indexes_for_versioning: :environment do
    puts "Running deploy task 'create_es_indexes_for_versioning'"

    CardHistory.create_index!
    CardResourceHistory.create_index!

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20180801151515'
  end  # task :create_es_indexes_for_versioning
end  # namespace :after_party

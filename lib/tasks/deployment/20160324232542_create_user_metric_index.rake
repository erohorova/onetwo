namespace :after_party do
  desc 'Deployment task: create an empty user metric. will be populated by another AWS offline task'
  task create_user_metric_index: :environment do
    log.info "Running deploy task 'create_user_metric_index'"
    UserMetric.create_index!
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160324232542'
  end  # task :seed_user_metric
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: clear_unused_permissions_from_config'
  task clear_org_permissions: :environment do
    puts "Running deploy task 'clear_org_permissions'"

    config1 = 'permissions/channel/collaborator_can_create_channel'
    config2 = 'permissions/channel/promote_subscriber_channel'

    # Put your task implementation HERE.
    Organization.all.each do |org|
      org.configs.where(name: [config1, config2]).destroy_all
      org.invalidate_config_cache
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160421222750'
  end  # task :clear_org_permissions
end  # namespace :after_party

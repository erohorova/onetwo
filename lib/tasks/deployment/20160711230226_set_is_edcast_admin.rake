namespace :after_party do
  desc 'Deployment task: set_is_edcast_admin'
  task set_is_edcast_admin: :environment do
    puts "Running deploy task 'set_is_edcast_admin'"

    # Put your task implementation HERE.
    Organization.where.not(id: Organization.default_org.id).find_each do |org|
      edcast_admin = org.admins.where(email: nil, handle: "@adminuser").first
      begin
        if edcast_admin
          edcast_admin.update!(is_edcast_admin: true)
        else
          user = User.new(
                       first_name: 'Admin',
                       last_name: 'User',
                       organization: org,
                       organization_role: 'admin',
                       is_edcast_admin: true,
                       password: User.random_password)
          user.skip_indexing = true
          user.skip_generate_images = true
          user.save!
        end
      rescue ActiveRecord::ActiveRecordError
        log.error "Is set admin failed for organization id #{org.id}"
      end
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160711230226'
  end  # task :set_is_edcast_admin
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: backfill_card_type_to_cards'
  task backfill_card_type_to_cards: :environment do
    puts "Running deploy task 'backfill_card_type_to_cards'"

    # Put your task implementation HERE.
    Organization.all.each do |org|
      org.cards.find_in_batches do |cards|
        cards.each do |card|
          next if card.cards_config.nil?
          card.update(card_type: card.cards_config.card_type, card_subtype: card.cards_config.ui_layout_type)
        end
      end
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20161004210134'
  end  # task :backfill_card_type_to_cards
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: fix_existing_picture_urls'
  task fix_existing_picture_urls: :environment do
    puts "Running deploy task 'fix_existing_picture_urls'"
    User.where("picture_url like '%graph.facebook%'").find_each do |user|
      if user.picture_url.index('width').nil? && user.picture_url.index('?').nil?
        user.picture_url = "#{user.picture_url}?width=512"
        user.save
      end
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20160310213855'
  end  # task :fix_existing_picture_urls
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: create_roles_for_all_orgs'
  task create_roles_for_all_orgs: :environment do
    puts "Running deploy task 'create_roles_for_all_orgs'"
    Role.find_each do |role|
      role.create_role_permissions
    end

    Organization.where(enable_role_based_authorization: false).find_each do |organization|
      Role.create_master_roles(organization)
      SetUserRolesOnRbacEnableJob.perform_later(organization_id: organization.id)
    end
    # Put your task implementation HERE.

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170428004559'
  end  # task :create_roles_for_all_orgs
end  # namespace :after_party

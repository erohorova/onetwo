namespace :after_party do
  desc 'Deployment task: remove_stale_user_profile'
  task remove_stale_user_profile: :environment do
    puts "Running deploy task 'remove_stale_user_profile'"

    UserProfile.where("user_id IS NULL").delete_all
    AfterParty::TaskRecord.create version: '20170316073250'
  end  # task :remove_stale_user_profile
end  # namespace :after_party

namespace :after_party do
  desc 'Deployment task: reindex_channels_for_teams_channels_follows_record'
  task reindex_channels_for_teams_channels_follows_record: :environment do
    puts "Running deploy task 'reindex_channels_for_teams_channels_follows_record'"

    # Put your task implementation HERE.
    # Channel IDs -> US Prod: 1050 , EU Prod: 56
    channel_ids = TeamsChannelsFollow.pluck(:channel_id).uniq
    puts "Total channels to be reindexed : #{channel_ids.size}"
    count = 0
    Channel.where(id: channel_ids).find_in_batches(batch_size: 100) do |batch|
      puts "---------------- Batch : #{count += 1} ------------------------"
      batch.each { |channel| channel.reindex_async }
    end
    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20190220110005'
  end  # task :reindex_channels_for_teams_channels_follows_record
end  # namespace :after_party

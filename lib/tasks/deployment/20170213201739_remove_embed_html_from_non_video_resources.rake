namespace :after_party do
  desc 'Deployment task: remove_embed_html_from_non_video_resources'
  task remove_embed_html_from_non_video_resources: :environment do
    puts "Running deploy task 'remove_embed_html_from_non_video_resources'"

    # Put your task implementation HERE.
    Resource.where.not(embed_html: nil).where.not(type: 'Video').find_each do |res|
      res.update_attributes(embed_html: nil)
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord.create version: '20170213201739'
  end  # task :remove_embed_html_from_non_video_resources
end  # namespace :after_party

# can be used as follow
# To Add `enableJourneyType` config to a give org.
# rake add_enable_journey_type_config

# On prod 1913 records are present.
task add_enable_journey_type_config: :environment do
  desc 'Adds a config to given organization'

  #Create a config for that org
  Organization.find_each do |organization|
    config = Config.create(data_type: 'boolean', name: 'enableJourneyType', value: true, configable: organization)
    if config.present?
      puts "Config enableJourneyType is added for organization_id: #{organization.id}"
    else
      puts "Config enableJourneyType is not added for organization_id: #{organization.id}"
    end
  end
end
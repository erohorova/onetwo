# Rake task to set card metadata for pathway and journeys
namespace :migrate_card_metadata_for_pack_journey do
  desc 'card plan'
  task set_metadata: :environment do
    # Run as rake change_pathway_author["url-slug",authorId]
    #14K+ card_metadata records will get created
    Card.where({card_type: 'pack'}).find_in_batches.each do |pathways|
      pathways.each do |pathway|
        plan = 'free'
        plan = 'paid' if pathway.pack_cards.map(&:is_paid_or_metadata).any?
        rating = pathway.cards_ratings.average(:rating)
        rating=0.00 if rating==nil
        CardMetadatum.find_or_create_by({plan: plan, card_id: pathway.id, average_rating: rating})
      end
    end

    Card.where({card_type: 'journey'}).find_in_batches.each do |journeys|
      journeys.each do |journey|
        plan = 'free'
        plan = 'paid' if journey.journey_pathways.map(&:is_paid_or_metadata).any?
        rating = journey.cards_ratings.average(:rating) 
        rating=0.00 if rating==nil
        CardMetadatum.find_or_create_by({plan: plan, card_id: journey.id, average_rating: rating})
      end
    end
  end
end 
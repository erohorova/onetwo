# can be used as follow

# To Add `sessionExpireOnBrowserClose` config to a give org.
# rake add_session_expire_config host_name=org_host_name
# example:
# rake add_session_expire_config host_name=stark
task add_session_expire_config: :environment do
  desc 'Adds a config to given organization'
  host_name = ENV["host_name"]

  # finding org from host_name if host_name is present.
  organization = Organization.where(host_name: host_name).first if host_name.present?
  
  #Create a config for that org

  Config.create(data_type: 'boolean', name: 'sessionExpireOnBrowserClose', value: true, configable: organization)
end


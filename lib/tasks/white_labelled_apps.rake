namespace :white_labelled_apps do

  desc 'Add branchmetric keys for white-labelled apps'
  # Run as rake white_labelled_apps:add_keys['skillset','key_live_xxx','skillsetedcastapp']
  task :add_keys, [:host_name, :branchmetric_api_key, :uri_scheme] => [:environment] do |t, args|
    orgs = Organization.find_by(host_name: args[:host_name])
    org.update_attribute(whitelabel_build_enabled: true)

    # Add branchmetrics_api_key
    config = org.configs.find_or_initialize_by(name: "whitelabel_branchmetrics_api_key")
    config.value = args[:branchmetric_api_key]
    config.description = "Branchmetric API key for white label app"
    config.save

    # Add uri_scheme config
    config = org.configs.find_or_initialize_by(name: "mobile_uri_scheme")
    config.value = args[:uri_schema]
    config.description = "URI scheme for white label app"
    config.save
  end

end

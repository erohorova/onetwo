namespace :assignment do
  desc "Recomputes assignment performance metrics for a content or for an org"
  # To recompute assignment metrics for:
  # A content: rake assignment:recompute_assignment_performance_metric[content_id=2]
  # Multiple content: rake assignment:recompute_assignment_performance_metric["content_id[]=1&content_id[]=2"]
  # An org: rake assignment:recompute_assignment_performance_metric[org_id=2]
  # Multiple org: rake assignment:recompute_assignment_performance_metric["org_id[]=1&org_id[]=2"]
  #
  # NOTE: This task can only process for a content or org. Both options cannot be passed together.
  task :recompute_assignment_performance_metric, [:args_expr] => [:environment] do |t, args|
    if args[:args_expr].present?
      options = Rack::Utils.parse_nested_query(args[:args_expr])
      content_ids = []

      if options["content_id"]
        content_ids = [options["content_id"]].flatten
      else options["org_id"]
        org_ids = [options["org_id"]].flatten
        content_ids = Assignment
          .joins(:card)
          .where("cards.organization_id IN (?)", org_ids)
          .pluck(:assignable_id).uniq
      end

      AssignmentPerformanceMetricJob.perform_now(content_ids) if content_ids.any?
    end
  end

  desc "Set assigned_at date for team_assignment_metric records that have assigned_at as nil"
  task set_assigned_at_in_performance_metric: :environment do 
    TeamAssignmentMetric.where("assigned_at IS NULL").each do |team_assignment_metric|
      team_assignment_metric.assigned_at = TeamAssignment.joins(:assignment)
        .where(assignments: {assignable_id: team_assignment_metric.card_id},
        team_assignments: {assignor_id: team_assignment_metric.assignor_id, team_id: team_assignment_metric.team_id})
        .limit(1).first.created_at
      team_assignment_metric.save
    end 
  end
end

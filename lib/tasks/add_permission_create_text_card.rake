# This will add 'CREATE_TEXT_CARD' permission to all roles present in org.
# On Production roles present for all organizations are 11,978

# rake add_permission_create_text_card
task add_permission_create_text_card: :environment do
  Role.where.not(organization_id: Organization.default_org.id).find_in_batches do |roles|
    roles.each do |role|
      role.role_permissions.find_or_create_by(name: 'CREATE_TEXT_CARD', description: 'Create Text Card').enable!
      puts "permission CREATE_TEXT_CARD is added for role: #{role.name} for organization_id: #{role.organization_id}"
    end
  end
end #task end

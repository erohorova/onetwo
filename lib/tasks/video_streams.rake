namespace :video_streams do

  desc 'Add VideoStreams'
  task :add_streams, [:type, :count, :user] => :environment do |t, args|
    # creates 5 past streams for admin user by default
    args.with_defaults(type: 'past_stream', count: 5, user: 1)

    start_index = VideoStream.last.nil? ? 0 : VideoStream.last.id
    end_index = start_index + args[:count].to_i
    user = User.find(args[:user].to_i)

    # Overriding Class behaviour to set transcoding_status correctly in dev
    Recording.class_eval do
      def submit_aws_transcoder_job
      end
    end

    (start_index..end_index).each_with_index do |stream, index|
      index = start_index + index
      if args[:type] == 'live_stream'
        puts "Adding new Live Stream"
        name_prefix = 'Livestream'
        uuid_prefix = 'ls'
        slug_prefix = 'livestream'
        status = 'live'

        vs = WowzaVideoStream.create(name: "#{name_prefix} #{index}", uuid: "#{uuid_prefix}#{index}", slug: "#{slug_prefix}-#{index}", creator: user, start_time: Time.now.utc(), status: "#{status}")
        EnqueueStreamsJob.perform_now({id: vs.id})
      elsif args[:type] == 'scheduled_stream'
        puts "Adding new Scheduled Stream"
        name_prefix = 'Scheduled Stream'
        uuid_prefix = 'ss'
        slug_prefix = 'scheduledstream'
        status = 'upcoming'

        vs = WowzaVideoStream.create(name: "#{name_prefix} #{index}", uuid: "#{uuid_prefix}#{index}", slug: "#{slug_prefix}-#{index}", creator: user, start_time: (Time.now.utc() + 1.month), status: "#{status}")
        EnqueueStreamsJob.perform_now({id: vs.id})

      elsif args[:type] == 'past_stream'
        puts "Adding new Past Stream"
        name_prefix = 'Past Stream'
        uuid_prefix = 'ps'
        slug_prefix = 'paststream'
        status = 'past'

        vs = WowzaVideoStream.create(name: "#{name_prefix} #{index}", uuid: "#{uuid_prefix}#{index}", slug: "#{slug_prefix}-#{index}", creator: user, start_time: (Time.now.utc() - 1.day), status: "#{status}")
        rec = Recording.create(sequence_number: 0, location: 'https://sombewhere/bucket/123/x.mp4', bucket: 'bucket', key: '/bucket/123/x.mp4', checksum: 'abcxyz', hls_location: 'something.m3u8', mp4_location: 'something.mp4', transcoding_status: 'available', source: 'client', video_stream_id: vs.id)
        EnqueueStreamsJob.perform_now({id: vs.id})
      end
    end
  end

  desc 'Add Streams to GET Channel by a continuous range of Video Stream ids'
  task :add_streams_to_get_by_range, [:from_id, :to_id] => :environment do |t, args|
    start_index = args[:from_id].to_i
    end_index = args[:to_id].to_i
    get_channel = Channel.find_by_slug('get')

    if !get_channel.nil? && !args[:from_id].blank? && args[:to_id].blank?
      (start_index..end_index).each_with_index do |stream, index|
        video_id = index + start_index
        video = VideoStream.find(video_id)
        video.channels << get_channel
        video.update_index
      end
    end
  end

  desc 'Add Streams to GET Channel by an array of ids'
  task :add_streams_to_get_by_array, [:video_ids] => :environment do |t, args|
    get_channel = Channel.find_by_slug('get')
    video_ids = args[:video_ids].split(",").map(&:to_i)

    if !get_channel.nil? && !video_ids.blank?
      video_ids.each do |video_id|
        video = VideoStream.find(video_id)
        video.channels << get_channel
        video.update_index
      end
    end
  end

  desc 'Re-enqueue and reindex video streams in user content queue'
  task reenqueue_video_Streams: :environment do
    VideoStream.published.each do |video_stream|
      EnqueueStreamsJob.perform_later({ id: video_stream.id, use_created_at_as_timestamp: true })
    end
  end

end

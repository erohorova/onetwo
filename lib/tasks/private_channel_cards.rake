namespace :private_channel_cards do
  desc "Cards which are in private channel will be update with is_public:false"
  task :update_visibility, [:organization_id] => [:environment] do |t, args|
    channels = Channel.where(is_private: true, organization_id: args[:organization_id])
    channels.each do |channel|
      channel.cards.where(is_public: true).find_in_batches do |cards|
        cards.each do |card|
          card.ecl_update_required = true
          card.is_public = false
          card.save
        end
      end
    end
  end #task end
end #namespace end

namespace :cards do
  desc 'Deployment task: fix_cards_duration'
  task :fix_cards_duration, [:organization_id] => [:environment] do |t, args|
    org_id = args[:organization_id]
    puts "Running deploy task fix_cards_duration for organization #{org_id}"
    total_cards_count = Card.where(duration: nil, organization_id: org_id).where.not(ecl_metadata: nil).count
    puts "total cards fetched #{total_cards_count}"
    updated_card_count = 0
    Card.where(duration: nil, organization_id: org_id).where.not(ecl_metadata: nil).find_each(batch_size: 1000) do |card|
      duration = card&.ecl_metadata&.dig("duration_metadata","calculated_duration")
      if duration && duration > 0
        puts "updating card  #{card.id}"
        card.duration = duration
        card.save
        updated_card_count += 1
      end
    end
    puts "total cards #{total_cards_count}"
    puts "total cards updated #{updated_card_count}"

    # Update task as completed.  If you remove the line below, the task will
    AfterParty::TaskRecord.create version: '20190307113157'
  end  # task :fix_cards_duration
end  # namespace :cards

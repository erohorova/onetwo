namespace :cards_test do

  desc 'Replace default db with duplicate of another database; supply source=dbname'
  task init: :environment do
    source = ENV['source']
    abort "run task with source=dbname" if source.nil?

    connect source

    begin
      sql_connection = ActiveRecord::Base.connection
    rescue Mysql2::Error
      abort "Couldn't connect to database #{source}; run task with source=dbname"
    end

    destination = Rails.application.config.database_configuration[Rails.env]['database']

    sql_connection.execute("DROP DATABASE IF EXISTS #{destination};")
    sql_connection.execute("CREATE DATABASE #{destination} CHARACTER SET utf8 COLLATE utf8_unicode_ci;")

    sql_connection.select_all("Show Tables").map(&:values).reduce(&:+).each { |table_name|
      sql_connection.execute("CREATE TABLE #{destination}.#{table_name} LIKE #{source}.#{table_name}")
      sql_connection.execute("INSERT INTO #{destination}.#{table_name} SELECT * FROM #{source}.#{table_name}")
    }

    connect

    Rake::Task['db:migrate'].invoke
  end

  def connect(db=nil)
    ActiveRecord::Base.connection_pool.disconnect!
    config = Rails.application.config.database_configuration[Rails.env]
    config['reaping_frequency'] = ENV['DB_REAP_FREQ'] || 10 # seconds
    config['pool']              = ENV['DB_POOL']      || 5
    config[:database] = db unless db.nil?
    ActiveRecord::Base.establish_connection(config)
  end

  desc 'Set all updated_at data to created_at'
  task fix_updated: :environment do
    [Post, Comment].each do |klass|
      ActiveRecord::Base.connection.execute("UPDATE `#{klass.table_name}` SET `updated_at` = `created_at`")
    end
  end

  # disregarding impossible situations in irrelevant data; e.g. post upvoted before it is created
  desc 'Shift forum timestamp data; supply group=groupname'
  task shift: :environment do
    groupname = ENV['group']
    if groupname.nil?
      abort "run task with group=groupname"
    else
      group = Group.find_by_name(groupname)
    end
    abort "Group #{groupname} not found" if group.nil?

    latest_q = Post.where(group: group).order(created_at: :desc).first.created_at
    # TODO only works for answers on posts (not e.g. on events)
    latest_a = Comment.joins("INNER JOIN posts ON posts.id = comments.commentable_id AND comments.commentable_type = 'Post' AND posts.group_id=#{group.id}").order(created_at: :desc).first.created_at
    diff = Time.zone.now - [latest_q, latest_a].max

    Post.where(group: group).find_each do |post|
      new_stamp = post.created_at + diff
      post.update(created_at: new_stamp, updated_at: new_stamp)
    end
    Comment.find_each do |comment|
      if comment.group == group
        new_stamp = comment.created_at + diff
        comment.update(created_at: new_stamp, updated_at: new_stamp)
      end
    end
  end

  desc 'Initialize time-shifted test db with scored cards; supply source=dbname, group=groupname|all'
  task spinup: :environment do
    abort "supply source=dbname, group=groupname|all" if ENV['group'].nil? || ENV['source'].nil?

    Rake::Task['cards_test:init'].invoke

    if ENV['group'] == 'all'
      Group.pluck(:name).each do |group|
        ENV['group'] = group
        Rake::Task['cards_test:shift'].invoke
      end
    else
      Rake::Task['cards_test:shift'].invoke
    end

    Rake::Task['cards:generate:all'].invoke

    Rake::Task['cards:update:all'].invoke
  end

end

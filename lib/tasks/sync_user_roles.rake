namespace :roles do
  desc 'Sync organization_role column of user with RBAC'
  # JIRA: EP-16676
  # Problem: The user could go to admin console but cannot see tabs that used to be on left-side of the page.
  # Cause: 
  # To access `/admin` page we check either organization_role column on users OR 'Admin' role in RBAC. 
  # So, the user can go to admin console page. 
  # But to display tabs on left-side we check for 'Admin' role in RBAC, this was done on 21st may as part of sub-admin implementation.
  # Hence, the user cannot see any tabs on left-side.
  # Solution:
  # Syncing, organization_role and RBAC.
  task sync_user_role: :environment do

    # whose organization_role is `member` and RBAC is `admin`
    # SQL query:
    # select roles.id,roles.name as role_name, users.id, users.email, users.organization_role 
    # from users
    # join user_roles on user_roles.user_id=users.id 
    # join roles on roles.id = user_roles.role_id 
    # where users.organization_role='member' and roles.name like 'admin';
    # returns 19 rows takes ~677 ms on prod.

    select_columns = "select users.id as user_id"
    joined_tables = " from user_roles join roles on roles.id = user_roles.role_id join users on user_roles.user_id = users.id" 

    # organization_role = admin and RBAC having role admin (using like query to be case sensitive)
    where_condition = " where users.organization_role='member' and roles.name like 'admin'"

    # sql_query = "select roles.id, roles.name as role_name, users.id, users.email, users.organization_role, users.organization_id from user_roles join roles on roles.id = user_roles.role_id join users on user_roles.user_id=users.id where users.organization_role='member' and roles.name='admin'"
    sql_query = select_columns + joined_tables + where_condition
    
    users = User.find_by_sql(sql_query)
    user_ids = users.map(&:user_id)

    # looping users
    User.includes(:roles).where(id: user_ids).each do |user|
      p "===SYNCING ROLES FOR USER ID #{user.id} STARTED==="
      p "PREVIOUS USER's ROLES RECORD: #{user.roles.pluck(:name)}"
      p "PREVIOUS USER's organization_role COLUMN: #{user.organization_role}"
      
      # Updating users organization_role column as 'admin'
      p 'UPDATING organization_role COLUMN...'
      user.update_columns(organization_role: 'admin') unless user.organization_role == 'admin'

      p "NEW USER's ROLES RECORD: #{user.roles.pluck(:name)}"
      p "NEW USER's organization_role COLUMN: #{user.organization_role}"
      p "===SYNCING ROLES FOR USER ID #{user.id} FINISHED==="
      puts ' '
    end

    # users whose organization_role is `admin` and there is no permission of `ADMIN_ONLY`
    # select distinct(user_id)
    # from user_roles 
    # join roles on roles.id = user_roles.role_id 
    # join users on user_roles.user_id=users.id 
    # join role_permissions on role_permissions.role_id = roles.id
    # where users.id not in ( 
    # select u.id from user_roles ur join roles r on r.id = ur.role_id join users u on ur.user_id=u.id join role_permissions rp on rp.role_id = r.id where u.organization_role='admin' and rp.name ='ADMIN_ONLY'
    # ) and users.organization_role='admin' and role_permissions.name !='ADMIN_ONLY';
    # returns 22 rows takes ~367 ms on prod.

    # select all the users' id whose data is perfect i.e. organization_role = 'admin' and having permission 'admin'
    sub_query = "select u.id from user_roles ur join roles r on r.id = ur.role_id join users u on ur.user_id=u.id join role_permissions rp on rp.role_id = r.id where u.organization_role='admin' and rp.name ='ADMIN_ONLY'"


    select_columns = "select distinct(users.id)"
    joined_tables = " from user_roles join roles on roles.id = user_roles.role_id join users on user_roles.user_id = users.id join role_permissions on role_permissions.role_id = roles.id" 

    # organization_role is admin and permission NOT having `ADMIN_ROLE` AND
    # users should not be part of sub_query i.e. perfect users
    where_condition = " where users.id not in (" + sub_query + ") and users.organization_role = 'admin' and role_permissions.name !='ADMIN_ONLY'"

    # sql_query = "select roles.id, roles.name as role_name, users.id, users.email, users.organization_role, users.organization_id from user_roles join roles on roles.id = user_roles.role_id join users on user_roles.user_id=users.id where users.organization_role='member' and roles.name='admin'"
    sql_query = select_columns + joined_tables + where_condition

    users = User.find_by_sql(sql_query)

    User.includes(:roles, organization: :roles).where(id: users).each do |user|
      p "===SYNCING ROLES FOR USER ID #{user.id} STARTED==="
      p "PREVIOUS USER's ROLES RECORD: #{user.roles.pluck(:name)}"
      p "PREVIOUS USER's organization_role COLUMN: #{user.organization_role}"
      
      # Updating users RBAC with role 'admin'
      p 'UPDATING RBAC WITH ADMIN ROLE..'
      role_with_admin_only_permission =  user.organization.roles.joins(:role_permissions).where(role_permissions: {name: 'ADMIN_ONLY'}).first
      p role_with_admin_only_permission.inspect
      user.roles << role_with_admin_only_permission unless user.get_user_permissions.include? 'ADMIN_ONLY'

      p "NEW USER's ROLES RECORD: #{user.roles.pluck(:name)}"
      p "NEW USER's organization_role COLUMN: #{user.organization_role}"
      p "===SYNCING ROLES FOR USER ID #{user.id} FINISHED==="
      puts ' '
    end
  end
end


namespace :update_mappings do
  desc "Update existing pathways in ecl with pack items and also cards with hidden attribute"
  task put_searchable_skills: :environment do
    puts "Running task 'update_searchable_skills'"
    index_name = "#{User.searchkick_index.name}/_mapping/user"
    ES_SEARCH_CLIENT.perform_request('PUT', index_name, {}, {
      properties: {
        expert_topics: {
          properties: {
            searchable_domain_label: {
              type: 'string',
              analyzer: 'edcast_search_analyzer'
            },
            searchable_topic_label: {
              type: 'string',
              analyzer: 'edcast_search_analyzer'
            },
          }
        }
      }
    })

    puts 'COMPLETED'
  end
end

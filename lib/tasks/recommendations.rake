namespace :recommendations do
  # DEPRECATED
  # desc 'add recommended users to all user queues'
  # task recommend_users: :environment do
  #   Recommend.recommend_all_users
  # end

  desc 'remove all user recommendations'
  task unrecommend_users: :environment do
    UserContentsQueue.gateway.client.delete_by_query(
        index: UserContentsQueue.index_name,
        body: {query: {filtered: {query: {match_all: {}},
                                  filter: {term: {content_type: 'User'}}}}})
  end
  # DEPRECATED
  # desc 'add recommended channels for all user queues'
  # task recommend_channels: :environment do
  #   Recommend.recommend_channels_for_all_users
  # end

  desc 'remove all channel recommendations'
  task unrecommend_channels: :environment do
    UserContentsQueue.gateway.client.delete_by_query(
        index: UserContentsQueue.index_name,
        body: {query: {filtered: {query: {match_all: {}},
                                  filter: {term: {content_type: 'Channel'}}}}})
  end
end

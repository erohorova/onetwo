namespace :update_default_templates do
  desc 'update/create default templates design fields with proper JSON content'
  task :design, [:host_name] => :environment do |t, args|
    # Will update JSON content for each default template or create if it not found in org
    #   Should be run after changing files in `public/templates/default_templates_design`
    #
    # Usage:
    # - specified org by host name:
    #   rake update_default_templates:design['www']
    #                                :design[www]
    # - all orgs exclude default:
    #   rake update_default_templates:design
    host_name = args[:host_name]
    organizations = if host_name.present?
      Organization.where(host_name: host_name)
    else
      Organization.exclude_default_org
    end
    # after changing the files under public/templates/default_templates_design
    # need to run this task to update all default templates with proper design
    notify_templates_titles = EmailTemplate::DEFAULT_TITLES[:notify]
    mailer_templates_titles = EmailTemplate::DEFAULT_TITLES[:mailer]
    templates_titles = notify_templates_titles + mailer_templates_titles

    working_dir = Rails.root.join(
      'public', 'templates', 'default_templates_design'
    )

    organizations.find_each do |org|
      templates_titles.each do |title|
        file_content = read_file(working_dir + "#{title}.json")
        next unless file_content
        template = org.email_templates.find_by(title: title, default_id: nil)
        if template
          template.update_attributes(design: file_content)
        else
          EmailTemplate.create(
            title: title, design: file_content, language: 'en',
            state: 'published', is_active: true, organization_id: org.id
          )
        end
      end
    end
  end

  desc 'update custom email templates to remove invite email to join mobile app'
  task :remove_custom_email_type_for_all, [:default_email_title] => :environment do |t, args|
    
    email_type = args[:default_email_title]
    puts "Finding custom templates for #{email_type}"
    
    default_template_for_type = EmailTemplate.where(title: email_type, default_id: nil)
    unless default_template_for_type.present?
      puts "No custom templates for #{email_type}" and return
    end
    
    puts "Destroying customized templates for #{email_type}"
    default_template_for_type.pluck(:id).each do |default_id|
      custom_templates = EmailTemplate.where(default_id: default_id)
      custom_templates.destroy_all if custom_templates.present?
    end
    puts "Destroying defaults for #{email_type}"
    default_template_for_type.destroy_all
  end
  

  def read_file(file_path)
    file = File.open(file_path)
    file_data = file.read
    file.close
    # check validness of JSON
    JSON.parse(file_data)
    return file_data
  rescue Errno::ENOENT, JSON::ParserError => e
    puts "File read error: #{e.message}"
    nil
  end
end

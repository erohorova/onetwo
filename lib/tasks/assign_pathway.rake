# Rake task to assing pathway to another author

task :change_pathway_author, [:pathway, :authorId] => [:environment] do |t, args|
  # Run as rake change_pathway_author["url-slug",authorId]

  @pathway = Card.friendly.published.find(args[:pathway])
  @cards = CardPackRelation.pack(cover_id: @pathway.id)

  @cards.map do |card|
    Card.find(card.item.id).update(author_id: args[:authorId])
  end
end
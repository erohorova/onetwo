namespace :one_time_tasks do
  desc 'cleanup team invitation'
  # when a user is already invited to join the team but the user does not accept that invitation and
  # admin add the same user from admin console to the same team than the same user appears in members list
  # as well as in pending list
  # Recods going to affect 8356
  task cleanup_team_invitation: :environment do
    # Put your task implementation HERE.
    Invitation.pending.where.not(recipient_id: nil).where(invitable_type: 'Team').find_in_batches do |invitations|
      invitations.each do |invitation|
        teams_user = TeamsUser.where(as_type: invitation.roles, team_id: invitation.invitable_id, user_id: invitation.recipient_id).first
        invitation.update_columns(accepted_at: Time.now) if teams_user.present?
      end
    end
  end
end
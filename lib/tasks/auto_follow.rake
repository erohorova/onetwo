namespace :auto_follow do

  desc 'edcast insiders to follow new users'
  task daily_follow: :environment do
    insiders = ENV['insiders']
    perday = ENV['perday'].blank? ? 1 : ENV['perday'].to_i

    if insiders.blank?
      log.error "missing 'insiders' input"
    else
      default_org_id = Organization.default_org.id
      insider_ids = User.where(organization_id: default_org_id, email: insiders.split(',')).map(&:id)

      signups = AccessLog.joins(:user).where(users: {organization_id: default_org_id}).where(is_signup_event: true).where.not(insider_follow_processed: true)
      signups.find_each do |event|
        insiders_already_following = []
        unless event.insiders_already_following.nil?
          begin
            insiders_already_following = JSON.parse(event.insiders_already_following)
          rescue => e
            log.error "Error while reading insiders yet to follow: #{e.inspect}"
          end
        end
        insiders_yet_to_follow = insider_ids - insiders_already_following
        if insiders_yet_to_follow.empty?
          event.update_columns(insider_follow_processed: true)
        else
          sampled = insiders_yet_to_follow.sample(perday)
          sampled.each do |fid|
            Follow.create(user_id: fid, followable_type: 'User', followable_id: event.user_id)
            insiders_already_following << fid
          end
          event.update_columns(insiders_already_following: insiders_already_following.to_json)
        end
      end
    end
  end
end

namespace :update_default_role do
  desc 'Update default_role column where Default role is NULL'
  task default_role_update: :environment do
    # 2 Records on production
    roles = Role.where("default_name IS NULL OR default_name=''")
    roles.each do |role|
      role.update(default_name: role.name)
    end
  end
end

namespace :content_item do
  desc "Update existing pathways in ecl with pack items and also cards with hidden attribute"
  task update_content_item_for_pathways_and_hidden_cards: :environment do
    puts "Updating pathways content item"
    Card.where(card_type: "pack", state: "published").find_in_batches do |pathway_cards|
      pathway_cards.each do |card|
        log.info("Updating pathway with id: #{card.id}")
        puts("Updating pathway with id: #{card.id}")
        EclCardPropagationJob.perform_later(card.id)
      end
    end

    puts "Updating hidden cards content item"
    Card.where(hidden: true).find_in_batches do |cards|
      cards.each do |card|
        log.info("Updating card with id: #{card.id}")
        puts("Updating card with id: #{card.id}")
        EclCardPropagationJob.perform_later(card.id)
      end
    end
  end

  desc 'Update content_items in ecl with the value stored in plan column of card_metadata'
  task sync_to_ecl_cards_having_paid_plans: :environment do
    puts "Updating plans for content_item"

    # Note: Please ensure plan column is present in Ecl before running the rake
    # Around 600 records are present in production for this query - select * from card_metadata where plan != 'free';
    # Not syncing the free plans, because content_items with null value in plan column would anyway be considered as free
    CardMetadatum.where.not(plan: 'free').find_in_batches do |card_metadata|
      Card.where(id: card_metadata.map(&:card_id)).find_each do |card|
        EclCardPropagationJob.perform_later(card.id)
      end
    end
  end

  desc 'Update plan column of content_items in ecl with -paid- for cards having is_paid as true'
  task sync_to_ecl_cards_having_is_paid_true: :environment do
    puts "Updating plans for content_item"

    # Note: Please ensure plan column is present in Ecl before running the rake
    # Around 562 records are present in production for this query - select * from cards c left join card_metadata cm on cm.card_id =c.id where c.is_paid = true and cm.id is null and c.deleted_at is null;
    # Not syncing cards with is_paid false, because content_items with null value in plan column would anyway be considered as free
    cards = Card.joins("LEFT JOIN card_metadata ON cards.id = card_metadata.card_id").where("cards.is_paid = 1 AND card_metadata.id IS NULL")
    cards.each do |card|
      EclCardPropagationJob.perform_later(card.id)
    end
  end

end
namespace :notifications do
  desc "Updates notification config for new_card_for_curation"
  task set_new_card_for_curation_push_to_off: :environment do 
    NotificationConfig.all.each do |nc| 
      config = if nc.user_id
        EDCAST_NOTIFY.settings.get_settings(nc.user.organization_id, nc.user_id)
      elsif nc.organization_id
        EDCAST_NOTIFY.settings.get_settings(nc.organization_id)
      end
      config[:options]["new_card_for_curation"][:push] = [{:label=>"Off", :id=>:off, :selected=>true}]
      puts "Updating notification config #{nc.id}"
      EDCAST_NOTIFY.settings.save_settings(nc, config[:options])
    end
  end
end
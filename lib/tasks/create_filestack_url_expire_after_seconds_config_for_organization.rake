namespace :create_filestack_url_expire_after_seconds_for_organization do

  desc 'create a config filestack_url_expire_after_seconds for organizations'
  task :add_filestack_url_expire_after_seconds, [:org_id, :value] => :environment do |_task, args|

    Config.find_or_create_by(
      configable_id: args.org_id,
      configable_type: 'Organization',
      name: 'filestack_url_expire_after_seconds',
      data_type: 'integer'
    ).update_attribute(:value, args.value)

    puts "Config successfully created for org #{args.org_id}"
  end
end

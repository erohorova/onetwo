# INVOKED as `rake cleanup_records_and_add_indices:run` collectively.
# It removes the duplicate records excluding soft-deleted records.
# Add unique indices to teams_users, user_profiles, user_onboardings, user_roles, follows.
# TODO: `rake cleanup_records_and_add_indices:add_indices` will be moved to db/migration/.

namespace :cleanup_records_and_add_indices do
  task cleanup: :environment do
    ### TeamsUser: Retrieve the duplicate entries. IDs are comma-separated. ###
    duplicate_teams_users = TeamsUser.group('team_id, user_id').having('COUNT(*) > 1').select("COUNT(*), GROUP_CONCAT(id SEPARATOR ',') as ids")
    duplicate_teams_users.each do |tu|
      # Step 1: Split the IDs.
      ids = tu['ids'].split(',')
      # Step 2: Reset the array to start the element from the second position. We want the first ID to be the original record.
      ids.shift
      p "Deleting TeamsUser: #{ids.join(",")}"
      # Step 3: Delete all
      TeamsUser.where(id: ids).delete_all
    end

    ### UserProfile: Retrieve the duplicate entries. IDs are comma-separated. ###
    duplicate_user_profiles = UserProfile.group('user_id').having('COUNT(*) > 1').select("COUNT(*), GROUP_CONCAT(id SEPARATOR ',') as ids")
    duplicate_user_profiles.each do |tu|
      # Step 1: Split the IDs.
      ids = tu['ids'].split(',')
      # Step 2: Reset the array to start the element from the second position. We want the first ID to be the original record.
      ids.shift
      p "Deleting UserProfile: #{ids.join(",")}"
      # Step 3: Delete all
      UserProfile.where(id: ids).delete_all
    end

    ### UserOnboarding: Retrieve the duplicate entries. IDs are comma-separated. ###
    duplicate_onboardings = UserOnboarding.group('user_id').having('COUNT(*) > 1').select("COUNT(*), GROUP_CONCAT(id SEPARATOR ',') as ids")
    duplicate_onboardings.each do |tu|
      # Step 1: Split the IDs.
      ids = tu['ids'].split(',')
      # Step 2: Reset the array to start the element from the second position. We want the first ID to be the original record.
      ids.shift
      p "Deleting UserOnboarding: #{ids.join(",")}"
      # Step 3: Delete all
      UserOnboarding.where(id: ids).delete_all
    end

    ### Follow: Retrieve the duplicate entries. IDs are comma-separated. Independent of soft-deleted records. ###
    duplicate_follows = Follow.unscoped.group('user_id, followable_id, followable_type')
      .having('COUNT(*) > 1')
      .select("COUNT(*) as counts, GROUP_CONCAT(id SEPARATOR ',') as ids, GROUP_CONCAT(deleted_at SEPARATOR ',') as deleted_ats")
    duplicate_follows.each do |f|
      # Step 1: Split the IDs.
      ids = f['ids'].split(',')
      deleted_records = (f['deleted_ats'] || '').split(',').length
      count = f['counts']

      # Case 1: Duplicate records such that few of them are soft-deleted and one is the active record.
      if deleted_records != 0 && deleted_records < count
        # Find the records that are soft-deleted + live records.
        follows = Follow.unscoped.where(id: ids)

        # Select the soft-deleted ones.
        soft_deleted_records = follows.select { |follow| follow.deleted? }.map(&:id)
        p "Deleting Follow: #{soft_deleted_records.join(",")}"

        # Delete only the soft-deleted ones.
        Follow.unscoped.where(id: soft_deleted_records).delete_all
      end

      # Case 2: Duplicate records where all of the records are soft-deleted.
      if deleted_records != 0 && deleted_records == count
        # Temporarily creates a copy of the ids.
        partial_ids = ids
        # Reset the array to start the element from the second position. We want the first ID to be the original record.
        partial_ids.shift
        p "Deleting Follow: #{partial_ids.join(",")}"
        # Delete the duplicate ones.
        Follow.unscoped.where(id: partial_ids).delete_all
      end

      # Case 3: Duplicate records where none of them are soft-deleted
      if deleted_records == 0
        # Reset the array to start the element from the second position. We want the first ID to be the original record.
        ids.shift
        # Delete the remaining records
        Follow.unscoped.where(id: ids).delete_all
      end
    end


    ### UserRole: Retrieve the duplicate entries. IDs are comma-separated. ###
    duplicate_user_roles = UserRole.group('user_id, role_id').having('COUNT(*) > 1').select("COUNT(*), GROUP_CONCAT(id SEPARATOR ',') as ids")
    duplicate_user_roles.each do |tu|
      # Step 1: Split the IDs.
      ids = tu['ids'].split(',')
      # Step 2: Reset the array to start the element from the second position. We want the first ID to be the original record.
      ids.shift
      p "Deleting UserRole: #{ids.join(",")}"
      # Step 3: Delete all
      UserRole.where(id: ids).delete_all
    end
  end

  task add_indices: :environment do
    if ActiveRecord::Base.connection.indexes(:teams_users).select { |s| s.name == "unique_tid_uid" && s.unique == true }.empty?
      p "Add unique index (team_id, user_id) to teams_users..."
      ActiveRecord::Base.connection.execute("ALTER TABLE teams_users ADD CONSTRAINT unique_tid_uid UNIQUE (team_id, user_id);")
    end

    if ActiveRecord::Base.connection.indexes(:user_profiles).select { |s| s.name == "unique_user_id" && s.unique == true }.empty?
      p "Add unique index (user_id) to user_profiles..."
      ActiveRecord::Base.connection.execute("ALTER TABLE user_profiles ADD CONSTRAINT unique_user_id UNIQUE (user_id);")
    end

    if ActiveRecord::Base.connection.indexes(:user_onboardings).select { |s| s.name == "unique_user_id" && s.unique == true }.empty?
      p "Add unique index (user_id) to user_onboardings..."
      ActiveRecord::Base.connection.execute("ALTER TABLE user_onboardings ADD CONSTRAINT unique_user_id UNIQUE (user_id);")
    end

    if ActiveRecord::Base.connection.indexes(:user_roles).select { |s| s.name == "unique_uid_rid" && s.unique == true }.empty?
      p "Add unique index (user_id, role_id) to user_roles..."
      ActiveRecord::Base.connection.execute("ALTER TABLE user_roles ADD CONSTRAINT unique_uid_rid UNIQUE (user_id, role_id);")
    end

    if ActiveRecord::Base.connection.indexes(:follows).select { |s| s.name == "unique_uid_fid_ftype" && s.unique == true }.empty?
      p "Add unique index (user_id, followable_id, followable_type) to follows..."
      ActiveRecord::Base.connection.execute("ALTER TABLE follows ADD CONSTRAINT unique_uid_fid_ftype UNIQUE (user_id, followable_id, followable_type);")
    end
  end

  task run: :environment do
    Rake::Task['cleanup_records_and_add_indices:cleanup'].invoke
    Rake::Task['cleanup_records_and_add_indices:add_indices'].invoke
  end
end

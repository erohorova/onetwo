namespace :reindex_cards do
  desc 'We are adding access restrictions to cards index. Hence, this rake is to backfill cards index.'
  task backfill_access_restrictions_for_cards: :environment do
    ids_to_be_reindexed = []

    ids_to_be_reindexed << Card.joins(:card_user_shares).pluck(:id)       # Shared with users
    ids_to_be_reindexed << Card.joins(:card_user_permissions).pluck(:id)  # Restricted to users
    ids_to_be_reindexed << Card.joins(:card_team_permissions).pluck(:id)  # Restricted to teams
    ids_to_be_reindexed << Card.joins(:shared_with_teams).pluck(:id)      # Shared with teams

    ids_to_be_reindexed = ids_to_be_reindexed.flatten

    puts "--------------Total cards to be reindexed: #{ids_to_be_reindexed.uniq.count}---------------"
    count = 0
    Card.where(id: ids_to_be_reindexed).find_in_batches do |batch|
      puts "********** Reindexing batch number: #{count += 1} ****************"
      batch.each { |card| card.reindex_async }
    end
  end
end

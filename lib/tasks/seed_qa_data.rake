require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'retriable'

namespace :seed do

  # category=subdir of data/forum
  # group=id of group to add content
  # mode=new if new users are to be created; otherwise existing users are used randomly
  # limit=N for upper limit on number of questions
  # example: rake seed:forum category=aviation.stackexchange.com mode=new group=1
  #  populates Group id 1 with all data from aviation dir, creating new users
  desc 'populate a q&a forum'
  task forum: :environment do
    category = ENV['category']
    group = Group.find_by(id: ENV['group'])
    abort "Group id #{ENV['group']} not found" if group.nil?

    # create users
    users = {} # key: stackexchange id, value: User object
    instructor = nil
    user_nodes = get_nodes(category, 'Users', [:Id, :CreationDate, :DisplayName, :Reputation]).reject {|n| n[:Id]=='-1'}
    if ENV['mode'].present?
      instructor_id = user_nodes.max_by { |u| u[:Reputation].to_i }[:Id]
    else
      existing_users = User.where(group: group)
    end
    user_nodes.each do |u|
      id = u[:Id]
      if ENV['mode'].present?
        name = u[:DisplayName]
        email = "#{name.downcase.gsub(/\W+/,'')}_#{id}@course-master.com"
        try_a_few do
          users[id] = User.where(email: email).first_or_create do |user|
            user.password = User.random_password
            user.first_name = name
            user.last_name = 'User'
            user.created_at = u[:CreationDate]
          end
        end
        if id == instructor_id
          instructor = users[id]
          instructor.update last_name: 'The Instructor'
          group.add_user user: instructor, role: 'instructor'
        end
      else
        users[id] = existing_users.sample
      end
    end

    # add users to group if new
    if ENV['mode'].present?
      users.values.each { |u| try_a_few { group.add_user user: u, role: 'student' } unless u == instructor }
    end

    post_nodes = get_nodes(category, 'Posts', [:Id, :PostTypeId, :AcceptedAnswerId, :ParentId, :CreationDate,
                                               :Body, :OwnerUserId, :Title, :Tags])

    # create questions
    questions = {} # key: stackexchange id, value: hash (question: Question object, accepted_answer: accepted answer stackexchange id)
    question_nodes = post_nodes.select { |n| n[:PostTypeId] == '1'}
    question_nodes = question_nodes.take(ENV['limit'].to_i) if ENV['limit'].present?
    question_nodes.each do |q|
      next if (title = q[:Title]).empty?
      next if (owner_id = q[:OwnerUserId]).nil?

      title = title.strip.truncate 250
      user = get_or_create_student owner_id, users, group, ENV['mode'].present?
      message = q[:Body]

      tags = q[:Tags].gsub('-','').split(/[<>]+/).delete_if(&:blank?)
      tags.each { |tag| message += " ##{tag}" }
      # (tags - Tag.pluck(:name)).each { |tag| Tag.create!(name: tag) } # this happens automatically

      try_a_few do
        question = Question.where(title: title).first_or_create do |new_q|
          new_q.message = message
          new_q.user = user
          new_q.group = group
          new_q.created_at = q[:CreationDate]
        end
        questions[q[:Id]] = {question: question, accepted_answer: q[:AcceptedAnswerId]}
      end
    end

    # create answers and approvals (stackexchange answers don't have tags)
    answers = {} # key: stackexchange id, value: Answer object
    post_nodes.select { |n| n[:PostTypeId] == '2'}.each do |a|
      next if (message = a[:Body]).empty?
      next if (owner_id = a[:OwnerUserId]).nil?
      next if (question_hash = questions[a[:ParentId]]).nil?

      message.strip!
      user = get_or_create_student owner_id, users, group, ENV['mode'].present?
      id = a[:Id]
      try_a_few do
        answers[id] = Answer.where(message: message).first_or_create do |new_a|
          new_a.commentable = question_hash[:question]
          new_a.user = user
          new_a.created_at = a[:CreationDate]
        end
      end
      try_a_few do
        if (id == question_hash[:accepted_answer]) && !Approval.exists?(approvable: answers[id])
            Approval.create!(approver: instructor, approvable: answers[id])
        end
      end
    end

    # create votes and reports (stackexchange doesn't indicate users, so pick users pseduorandomly)
    vote_nodes = get_nodes(category, 'Votes', [:Id, :PostId, :VoteTypeId, :CreationDate])

    [{ vote_type_id: '2', klass: Vote, referent: :votable, extra: { weight: 1 } },
     { vote_type_id: '12', klass: SpamReport, referent: :reportable, extra: { } },
     { vote_type_id: '4', klass: InappropriateReport, referent: :reportable, extra: { } }].each do |vote_type|
      vote_nodes.select { |n| n[:VoteTypeId] == vote_type[:vote_type_id]}.each do |v|
        next if (referent = get_question_or_answer(v[:PostId], questions, answers)).nil?

        rng = Random.new(v[:Id].to_i)
        next if (user = users.values.reject { |u| referent.user == u }.sample(random: rng)).nil?

        next if vote_type[:klass].exists?(vote_type[:referent] => referent, user_id: user.id)

        attrs = { vote_type[:referent] => referent, user_id: user.id, created_at: v[:CreationDate] }
        try_a_few { vote_type[:klass].create!(attrs.merge(vote_type[:extra])) }
      end
    end
  end

  def try_a_few
    Retriable.retriable tries: 3, interval: 1 do
      yield
    end
  rescue SystemExit, Interrupt
    raise
  rescue Exception
    puts 'RESCUED ERROR', $!.inspect, $@
  end

  def get_question_or_answer(id, questions, answers)
    if questions.has_key? id
      questions[id][:question]
    elsif answers.has_key? id
      answers[id]
    else
      nil
    end
  end

  def get_or_create_student(id, users, group, create)
    return users[id] unless users[id].nil?
    if create
      users[id] = User.create!(password: 'password', email: "user_#{id}@course-master.com", first_name: "Pat #{id}")
      group.add_user user: users[id], role: 'student'
    else
      users.values.sample
    end
  end

  def get_nodes(source, file, fields)
    filepath = Rails.root.join("data/forum/#{source}/#{file}.xml")
    if File.exist?(filepath)
      access = File.open filepath
    elsif File.exist?("#{filepath}.gz")
      access = Zlib::GzipReader.open "#{filepath}.gz"
    else
      abort "File #{filepath} not found"
    end
    Nokogiri::XML(access).css('row').map(&:attributes).map(&:with_indifferent_access) \
                  .map { |node| Hash[node.slice(*fields).map { |k,v| [k, v.value] }].with_indifferent_access }
  end

  namespace :forum do

  # TODO: create separate groups for forum dirs
    desc 'populate a q&a forum with all data in data/forum'
    task all: :environment do
      list = Dir.entries(Rails.root.join 'data','forum').select {|entry| !(entry == '.' || entry == '..') }
      list.each do |category|
        ENV['category']=category
        Rake::Task['seed:forum'].execute
      end
    end
  end
end

=begin
used attributes:

Users
Id
Reputation
CreationDate
DisplayName

Posts
Id
PostTypeId
1 Question
2 Answer
AcceptedAnswerId (only present if PostTypeId is 1)
ParentId (only present if PostTypeId is 2)
CreationDate
Body
OwnerUserId (present only if user has not been deleted; always -1 for tag wiki entries (i.e., the community user owns them))
Title
Tags

Votes
Id
PostId
VoteTypeId
2 - UpMod
3 - DownMod
4 - Offensive
12 - Spam
CreationDate

full attributes:

Id
PostTypeId
1 Question
2 Answer
3 Orphaned tag wiki
4 Tag wiki excerpt
5 Tag wiki
6 Moderator nomination
7 "Wiki placeholder" (seems to only be the election description)
8 Privilege wiki
AcceptedAnswerId (only present if PostTypeId is 1)
ParentId (only present if PostTypeId is 2)
CreationDate
Score
ViewCount
Body
OwnerUserId (present only if user has not been deleted; always -1 for tag wiki entries (i.e., the community user owns them))
OwnerDisplayName
LastEditorUserId
LastEditorDisplayName="Rich B"
LastEditDate="2009-03-05T22:28:34.823" - the date and time of the most recent edit to the post
LastActivityDate="2009-03-11T12:51:01.480" - the date and time of the most recent activity on the post. For a question, this could be the post being edited, a new answer was posted, a bounty was started, etc.
Title
Tags
AnswerCount
CommentCount
FavoriteCount
ClosedDate (present only if the post is closed)
CommunityOwnedDate (present only if post is community wikied)

Users
Id
Reputation
CreationDate
DisplayName
LastAccessDate
WebsiteUrl
Location
AboutMe
Views
UpVotes
DownVotes
EmailHash (now always blank)
AccountId
Age

Votes
Id
PostId
VoteTypeId
1 - AcceptedByOriginator
2 - UpMod
3 - DownMod
4 - Offensive
5 - Favorite (if VoteTypeId = 5, UserId will be populated)
6 - Close
7 - Reopen
8 - BountyStart (if VoteTypeId = 8, UserId will be populated)
9 - BountyClose
10 - Deletion
11 - Undeletion
12 - Spam
15 - ModeratorReview
16 - ApproveEditSuggestion
UserId (only present if VoteTypeId is 5 or 8)
CreationDate
BountyAmount (only present if VoteTypeId is 8 or 9)

source: http://meta.stackexchange.com/questions/2677/database-schema-documentation-for-the-public-data-dump-and-sede
=end

namespace :ecl do
  desc 'removes draft pathways from ecl'
  task delete_draft_pathways_from_ecl: :environment do
    draft_pathways_to_delete = Card.where(card_type: 'pack', state: 'draft')
                                   .where.not(ecl_id: nil)
    draft_pathways_to_delete.each do |draft_pathway|
      EclApi::CardConnector.new.destroy_card_from_ecl(draft_pathway.ecl_id,
                                                      draft_pathway.organization_id)
    end
  end
end

namespace :channel_tasks do
  desc "To add or switch ON channel_programming_v2 config for org"
  task :switch_on_channel_programming_v2, [:organization_id] => [:environment] do |t, args|
    config = Config.find_or_create_by(configable_id: args[:organization_id], 
                                      configable_type: 'Organization',
                                      category: 'settings',
                                      name: 'channel_programming_v2',
                                      data_type: 'boolean')
    config.value = 'true'
    config.save
  end

  desc "To add or switch OFF channel_programming_v2 config for org"
  task :switch_off_channel_programming_v2, [:organization_id] => [:environment] do |t, args|
    config = Config.find_or_create_by(configable_id: args[:organization_id], 
                                      configable_type: 'Organization',
                                      category: 'settings',
                                      name: 'channel_programming_v2',
                                      data_type: 'boolean')
    config.value = 'false'
    config.save
  end

  desc "This will migrate channels data for cards to ECL"
  #Total no. of cards propagated: 1029899
  task migrate_channels_cards_to_ecl: :environment do 
    limit = 1000
    offset = 0
    loop do
      puts "Batch : #{offset}"
      card_ids = ChannelsCard.order(:card_id).limit(limit).offset(offset).pluck('distinct card_id')
      cards = Card.where(id: card_ids)
      cards.each do |card|
        EclCardPropagationJob.perform_later(card.id)
      end
      break if card_ids.length < limit
      offset += limit
    end
  end#task end

  #Below task is to be run when we switch ON channel_programming_v2 config for organization
  desc "This will add sociative source to ecl_sources table for channels where ecl_enabled = true"
  #Total no. of channels with ecl_enables: 541
  task :ecl_enabled_to_sociative_source, [:sociative_id, :organization_id] => [:environment] do |t, args|
  	args.with_defaults(:sociative_id => '', :organization_id => nil)
  	if args[:organization_id]
      organization_id = args[:organization_id].split('|')
  	  channels = Channel.where({organization_id: organization_id, ecl_enabled: true})
  	else
  	  channels = Channel.where({ecl_enabled: true})
  	end

  	#this will create records in ecl_source against channel for sociative source
  	channels.find_in_batches.each do |batch|
      batch.each do |ch|
  	    a = EclSource.find_or_create_by(channel: ch, ecl_source_id: args[:sociative_id])
      end
  	end
  end #task end

  #Below task is for reverse migration i.e when we switch OFF channel_programming_v2 config for organization
  #we should migrate all open source and make ecl_enabled true
  desc "This is to make ecl_enabled true where sociative source is present in ecl_sources table"
  task :sociative_source_to_ecl_enabled, [:sociative_id, :organization_id] => [:environment] do |t, args|
    args.with_defaults(:sociative_id => '', :organization_id => nil)
    if args[:organization_id]
      organization_id = args[:organization_id].split(',')
      channels = Channel.joins(:ecl_sources).where(organization_id: organization_id, ecl_sources: {ecl_source_id: args[:sociative_id]})
      channels_to_switch_off = Channel.where(organization: organization_id, ecl_enabled: true).where.not(id: channels.pluck(:id))
    else
      channels = Channel.joins(:ecl_sources).where(ecl_sources: {ecl_source_id: args[:sociative_id]})
      channels_to_switch_off = Channel.where(ecl_enabled: true).where.not(id: channels.pluck(:id))
    end

    #this will make ecl_enabled = true where source is present.
    channels.update_all(ecl_enabled: true)
    #this will make ecl_enabled = false where source may be deleted with new UI.
    channels_to_switch_off.update_all(ecl_enabled: false)
    #Destroy all ecl_sources with sociative as a source
    EclSource.where(ecl_source_id: args[:sociative_id], channel_id: channels.pluck(:id)).destroy_all
  end #task end
end #namespace end

require 'rubygems'
require 'rake'

namespace :test do
  desc "Test cache directory"
  Rails::TestTask.new(cache: 'test:prepare') do |t|
    t.libs << 'test'
    t.pattern = 'test/cache/**/*_test.rb'
    t.verbose = true
  end
end

Rake::Task['test:run'].enhance ['test:cache']
namespace :migrate do
  desc "Backfill mappings for groups"
  task "groups" => :environment do
    run_migration("PrivateGroup", ENV["FROM_DATE"])
  end

  desc "Backfill mappings for groups_users"
  task "groups_users" => :environment do
    run_migration("GroupsUser", ENV["FROM_DATE"])
  end

  desc "Backfill mappings for scores"
  task "scores"  => :environment do
    run_migration("Score", ENV["FROM_DATE"])
  end

  desc "Backfill mappings for events"
  task "events"  => :environment do
    run_migration("Event", ENV["FROM_DATE"])
  end

  desc "Backfill mappings for events_users"
  task "events_users"  => :environment do
    run_migration("EventsUser", ENV["FROM_DATE"])
  end

  desc "Backfill mappings for votes"
  task "votes"  => :environment do
    run_migration("Vote", ENV["FROM_DATE"])
  end

  desc "Backfill mappings for approvals"
  task "approvals"  => :environment do
    run_migration("Approval", ENV["FROM_DATE"])
  end

  desc "Backfill mappings for streams"
  task "streams"  => :environment do
    run_migration("Stream", ENV["FROM_DATE"])
  end

  desc "Backfill mappings for follows"
  task "follows"  => :environment do
    run_migration("Follow", ENV["FROM_DATE"])
  end

  desc "Backfill mappings for reports"
  task "reports"  => :environment do
    run_migration("Report", ENV["FROM_DATE"])
  end

  desc "Backfill mappings for notifications"
  task "notifications"  => :environment do
    run_migration("Notification", ENV["FROM_DATE"])
  end

  desc "Backfill mappings for file_resources"
  task "file_resources"  => :environment do
    run_migration("FileResource", ENV["FROM_DATE"])
  end

  desc "Backfill mappings for activity_streams"
  task "activity_streams"  => :environment do
    run_migration("ActivityStream", ENV["FROM_DATE"])
  end

  desc "Backfill mappings for comments"
  task "comments"  => :environment do
    run_migration("Comment", ENV["FROM_DATE"])
  end

  desc "Backfill mappings for posts"
  task "posts"  => :environment do
    run_migration("Post", ENV["FROM_DATE"])
  end

  desc "Backfill mappings for user_preferences"
  task "user_preferences"  => :environment do
    run_migration("UserPreference", ENV["FROM_DATE"])
  end

  def run_migration(model, from)
    puts "Running for #{model}"
    updater = ForumMigrationService.new
    updater.backfill_forum_user_mappings(model, from: from)
    puts "Finished #{model}"
  end

  desc "Migrate user_id of backfilled data"
  task update_user_ids: :environment do
    puts "Updating user_id #{Time.now}"
    ForumMigrationService.new.run
    puts "Finished updating user_id #{Time.now}"
  end
end
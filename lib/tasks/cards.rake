namespace :cards do
  desc "create a list of admins"
  task update_card_type: :environment do
    org_id = ENV["ORG_ID"]
    organization = Organization.find_by(id: org_id)
    if organization.present?
      # select count(*) from cards where resource_id in (select id from resources where type="video" and video_url is null)
      # and card_subtype="video" and deleted_at is null and organization_id != 1;
      query = organization.cards.joins(:resource).where("resources.type = 'video' and video_url is null").where(card_subtype: "video")
      puts "Total cards with wrong type: #{query.count}"
      query.find_in_batches do |cards|
        cards.each do |card|
          begin
            resource = card.resource
            params =  Resource.get_resource_params(resource.url)
            resource.update(params)
            card.send(:set_card_subtype)
            card.save
          rescue => e
            log.error("Failed to update card with id: #{card.id} with error #{e.message}")
          end
        end
      end
      puts "Remaining cards with wrong type: #{query.count}"

    else
      puts "Organization not found with id: #{org_id}"
    end
  end

  task update_card_resource_title: :environment do
    org_id = ENV["ORG_ID"]
    if org_id.blank?
      puts "Please provide organization id"
      return
    end

    puts "Performing update_card_resource_title for org_id: #{org_id}"
    resource_ids = []
    # disabling STI as it is aborting rake task for type which are not inherited from Resource like 'video',
    # we don't require STI for this rake task hence disabling
    Resource.inheritance_column = nil
    Resource.joins(:cards).where(cards: {organization_id: org_id.to_i}).find_each do |resource|
      if resource.url&.truncate(35) == resource.title&.truncate(35)
        resource_ids << resource.id
        resource.update_columns(title: resource.title&.truncate(35))
        if (card = resource.cards.first).present? && (card.message&.truncate(35) == resource.url&.truncate(35))
          card.update_columns(message: card.message&.truncate(35))
        end
      end
    end
    puts "updated junk resource title for following resource_ids: #{resource_ids}"
  end

  task update_card_language: :environment do
    host_name = ENV['HOST_NAME']
    language = ENV['LANGUAGE']

    unless host_name.present? && language.present?
      raise 'Please provide organization host_name and language'
    end

    org = Organization.find_by(host_name: host_name)

    unless org.present?
      raise "Could not find organization with host_name: #{host_name}"
    end

    if UserProfile::VALID_LANGUAGES.values.include?(language)
      org.cards.user_created.where(language: nil).update_all(language: language)
    else
      raise "#{language} is not valid language"
    end

    puts "Language for cards' with organization: #{org.host_name} was successfully updated"
  end
end

namespace :test_emails do

  #not making any DB writes with this rake task
  #the purpose is to test the templates
  #without following needed steps to get the mail in inbox.
  desc 'test emails'
  task :test_templates =>:environment do
    if(Rails.env == 'qa' || Rails.env == 'staging' || Rails.env == 'development')
      STDOUT.puts "\n\n"
      STDOUT.puts "Enter email address to recieve the test emails on"
      @email = STDIN.gets.chomp

      def mail_css_default_variables(collection)
        global_merge_vars = [
          {'name' => 'header_background_color', 'content' => "##{Settings.mail_css.header_background_color}" },
          {'name' => 'text_color', 'content' => "##{Settings.mail_css.text_color}" },
          {'name' => 'section_text_color', 'content' => "##{Settings.mail_css.section_text_color}" },
          {'name' => 'highlight_text_color', 'content' => "##{Settings.mail_css.highlight_text_color}" },
          {'name' => 'button_blue_color', 'content' => "##{Settings.mail_css.button_blue_color}" },
          {'name' => 'button_background_color', 'content' => "##{Settings.mail_css.button_background_color}" },
          {'name' => 'show_edcast_logo', 'content' => true }
        ]
        global_merge_vars += collection
        return global_merge_vars
      end

      def mail_css_custom_variables(collection)
        global_merge_vars = [
          {'name' => 'header_background_color', 'content' => '#e3452d' },
          {'name' => 'text_color', 'content' => '#000000' },
          {'name' => 'section_text_color', 'content' => '#000000' },
          {'name' => 'highlight_text_color', 'content' => '#000000' },
          {'name' => 'button_blue_color', 'content' => '#45e324' },
          {'name' => 'button_background_color', 'content' => '#45e324' },
          {'name' => 'show_edcast_logo', 'content' => false }
        ]
        global_merge_vars += collection
        return global_merge_vars
      end

      def org_promotion_variable(collection)
        global_merge_vars = [
          {'name' => 'promotions', 'content' => @promotions},
          {'name' => 'home_page', 'content' => @home_page}
        ]
        global_merge_vars += collection
        return global_merge_vars
      end

      def send_mail(template, subject, global_merge_vars, from_name='EdCast')
        global_merge_vars = @use_custom_css ? mail_css_custom_variables(global_merge_vars) : mail_css_default_variables(global_merge_vars)
        global_merge_vars = org_promotion_variable(global_merge_vars)
        message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: template,
              subject: subject,
              from_email: 'admin@edcast.com',
              from_name: from_name,
              to: [MandrillMailer.send(:new).send(:format_email_address_for_api, @email,'')],
              global_merge_vars: global_merge_vars)
        MandrillMailer.send_api_email(message: message).deliver_now
        STDOUT.puts "Go check your inbox.."
      end

      def set_it_up
        @email_directory = [
          {prompt: 'Activity notification email', func: 'send_activity_email'},
          {prompt: 'Send annoucement mail', func: 'send_annoucement_mailer'},
          {prompt: 'bulk import user details email - notify admin', func: ''},
          {prompt: 'developer admin invitation email', func: 'send_dev_admin_invite'},
          {prompt: 'confirmations instruction email', func: 'send_confirmation_instruction'},
          {prompt: 'reset password instructions email', func: 'send_forgot_password_mail'},
          {prompt: 'after signup - upon succesful signup email', func: ''},
          {prompt: 'import users welcome email', func: ''},
          {prompt: 'event reminder email', func: 'send_event_reminder_email'},
          {prompt: 'CSV: import confirmation email - confirmation for CSV uploaded users', func: 'send_import_confirmation'},
          {prompt: 'invitation emails', func: 'send_invitation_emails'},
          {prompt: 'mobile invitation email', func: 'send_mobile_invitation'},
          {prompt: 'new channel request email', func: ''},
          {prompt: 'new team request to staff email', func: 'send_new_team_request'},
          {prompt: 'Org Admin: reminder mail to add users emai', func: 'send_org_admin_reminder_email'},
          {prompt: 'self serve email', func: 'send_self_serve_email'},
          {prompt: 'team users - admin group annoucements email', func: 'send_team_notification_mailer'},
          {prompt: 'User Mailer: confirm-email-address', func: 'send_user_mailer_confirm_email'},
          {prompt: 'data dump sync email', func: ''},
          {prompt: 'follower batch notification email', func: 'send_follow_batch_email'},
          {prompt: 'invite collaborator email', func: 'send_invite_collaborator'},
          {prompt: 'invite follower email', func: 'send_invite_follower_email'},
          {prompt: 'daily digest email', func: ''},
          {prompt: 'instructor daily digest email', func: 'send_instructor_daily'},
          {prompt: 'daily instructor email', func: ''},
          {prompt: 'weekly activity email', func: ''},
          {prompt: 'assignment notification email', func: 'send_assignment_notification'},
          {prompt: 'welcome group member email', func: 'send_welcome_group_email'},
          {prompt: 'magic link email', func: 'send_magic_link_email'}
      ]
        @org_logo = "https://s3-us-west-2.amazonaws.com/edcast-email-assets/ed-new.png"
        @qa_org_logo = "https://dp598loym07sk.cloudfront.net/organizations/co_branding_logos/000/000/015/original/temp_uploads_2Ff3c62262-1088-4323-a19c-d1e24dc90673_2FQA-Logo-copy.png?1478848586"
        @show_edcast_logo = true
        @use_custom_css = false
        @promotions=true;
        @home_page="http://www.lvh.me:4000"
      end

      def choose_email_to_send
        STDOUT.puts "\n\n"
        @email_directory.each_with_index do |v,index|
          STDOUT.puts "#{(index + 1).to_s.rjust(2)}  ==>  #{v[:prompt]}"
        end
        STDOUT.puts "\n\n"
        STDOUT.puts "Enter the number of the email you want to test or anything else to Quit"
        choice = (STDIN.gets.chomp).to_i - 1
        if !@email_directory[choice].present?
          exit_task
        elsif !@email_directory[choice][:func].present?
          dont_support(choice)
        else
          send_email(choice)
        end
        # exit_task unless @email_directory[choice].present? 
        #  unless @email_directory[choice][:func].present?
        
      end

      def dont_support(choice)
        STDOUT.puts "Dont yet support sending #{@email_directory[choice][:prompt]} from this rake task"
        choose_email_to_send
      end

      def send_forgot_password_mail
        global_merge_vars = [
            { "name" => "link_url", "content" =>  "http://qa.cmnetwork.co/log_in" },
            { "name" => "org_logo", "content" => @org_logo },
            { "name" => "first_name", "content" => "Forgetful" },
            { "name" => "show_edcast_name", "content" => @show_edcast_logo }
        ]
        send_mail('edcast-reset-password-request.html.slim',"[EdCast] Reset your password",global_merge_vars)
      end

      def send_confirmation_instruction
        global_merge_vars = [
          { "name" => "link_url", "content" =>  "http://qa.cmnetwork.co/log_in" },
          { "name" => "org_logo", "content" => @org_logo },
          { "name" => "first_name", "content" => "Forgetful" },
          { "name" => "show_edcast_name", "content" => @show_edcast_logo }
      ]
        send_mail("edcast-confirmation-instruction-web.html.slim","[EdCast] Welcome - just one more step!",global_merge_vars)
      end

      def send_org_admin_reminder_email
        global_merge_vars = [
          {"name" => "user_name", "content" => 'Email tester'},
          {"name" => "team_url", "content" => 'http://www.lvh.me'},
          {"name" => "forgot_password_url", "content" => 'http://www.lvh.me:4000/forgot_password'},
          {"name" => "show_edcast_name", "content" => @show_edcast_logo }
        ]
        template = 'admin-reminder-mail.html.slim'
        subject = "Invite team members to join EdCast"
        send_mail(template, subject, global_merge_vars)
      end

      def send_follow_batch_email
        global_merge_vars = [
          {"name" => "first_name", "content" => 'EmailTester'},
          {"name" => "unsubscribe_url", "content" => "http://www.lvh.me:4000/"},
          {"name" => "email", "content" => @email},
          {"name" => "followers", "content" =>[{'name' => "followerName", 'picture' => "https:d1iwkfmdo6oqxx.cloudfront.net/users/avatars/000/628/335/small/photo.jpg?1462516132", 'url' => "https://qa.cmnetwork.co/#{@sagarbhute}"}]},
          {"name" => "user_name", "content" => "followeeName"},
          {"name" => "org_logo", "content" => @org_logo},
          {"name" => "show_edcast_name", "content" => @show_edcast_logo }
        ]
        template = 'follow-batch-email.html.slim'
        subject = "[EdCast] You have new followers"
        send_mail(template, subject, global_merge_vars)
      end

      def send_instructor_daily
        url = "https://s3.amazonaws.com/edcast-instructor-daily-digest-report-staging/1097_English_17-01-17-16-00-05.xls"
        language = 'en'
        global_merge_vars = [
          {"name" => "first_name", "content" => 'User FirstName'},
          {"name" => "email", "content" => @email},
          {"name" => "org_logo", "content" => @org_logo},
          {"name" => "link_url", "content" => url},
          {"name" => "language", "content" => language},
          {"name" => "show_edcast_name", "content" => @show_edcast_logo }
        ]
        send_mail("instructor-daily-digest-report.html.slim", "[EdCast] Your daily instructor digest report for {{language}} courses", global_merge_vars)
      end

      def send_assignment_notification
        global_merge_vars = [
          {"name" => "user_name", "content" => "EmailTester"},
          {"name" => "assignment_title", "content" => "Assignment snippet goes here"},
          {"name" => "assignor_name", "content" => "Assignor Name"},
          {"name" => "team_name", "content" => "Team Name"},
          {"name" => "assignment_url", "content" => "http://qa.cmnetwork.co/insights/knowledge-is-power-pathway"},
          {"name" => "org_logo", "content" => @org_logo},
          {"name" => "unsubscribe_url", "content" => "unsubscribe_url"}, #not supported in new template
          {"name" => "show_edcast_name", "content" => @show_edcast_logo }
        ]
        send_mail("user-team-assignment-email-revised.html.slim", "[EdCast] {{assignor_name}} assigned you content", global_merge_vars)
      end

      def send_welcome_group_email
        global_merge_vars = [
            {"name" => "group_name", "content" => "Your Group Name"},
            {"name" => "parent_group_name", "content" => "parent groupname"},
            {"name" => "from_user_name", "content" => "From user name"},
            {"name"=>"parent_group_url",
              "content"=>"http://www.lvh.me/redirect?url=http%3A%2F%2Fexample.com%3Fsomekey%3Dsomeval"},
            {"name" => "unsubscribe_url", "content" => "unsubscribe_url"},
            {"name" => "show_edcast_name", "content" => @show_edcast_logo }
          ]
        STDOUT.puts "Enter 'a' for *join-group-member-notification* or 'b' *welcome-group-member-notification* "
        user_choice = STDIN.gets.chomp.downcase
        if user_choice == 'a'
          template = 'join-group-member-notification.html.slim'
        else
          template = 'welcome-group-member-notification.html.slim'
        end
        send_mail(template, "Welcome to {{GROUP_NAME}}", global_merge_vars, "EdCast Forum")
      end

      def send_invite_collaborator
        global_merge_vars = [
            {"name" => "first_name", "content" => "EmailTester"},
            {"name" => "invite_url", "content" => "http://qa.cmnetwork.co/channel/general"},
            {"name" => "channel_name", "content" => "General"},
            {"name" => "inviter_name", "content" => "Sagar Bhute"},
            {"name" => "show_edcast_name", "content" => @show_edcast_logo}
        ]
        send_mail('invite_collaborator.html.slim', "{{inviter_name}} has invited you to collaborate on an EdCast Channel.", global_merge_vars)
      end

      def send_invite_follower_email
        global_merge_vars = [
            {"name" => "first_name", "content" => "EmailTester"},
            {"name" => "invite_url", "content" => "http://qa.cmnetwork.co/channel/general"},
            {"name" => "channel_name", "content" => "General"},
            {"name" => "inviter_name", "content" => "Sagar Bhute"},
            { "name" => "show_edcast_name", "content" => @show_edcast_logo }
        ]
        send_mail('invite_follower.html.slim', "{{inviter_name}} has invited you to follow an EdCast Channel.", global_merge_vars)
      end

      def send_annoucement_mailer
        global_merge_vars = [
                  {"name" => "post_title", "content" => "Announcement Post"},
                  {"name" => "post_message", "content" => "<p>Announcement Post</p>"},
                  { "name" => "show_edcast_name", "content" => @show_edcast_logo }
        ]
        global_merge_vars += [
          {"name" => "post_url", "content" => "http://www.lvh.me/redirect?url=https%3A%2F%2Ftest-qa.edcast.me%2Fcourses%2F5%2Flearn%3Fedcast_ref%3D59"},
          {"name" => "post_group_name" , "content" => "QA TEST" }
        ]
        global_merge_vars += [
        {"name" => "post_user", "content" => "Sagar Bhute" },
        {"name" => "org_logo", "content" => "http:/system/organizations/co_branding_logos/000/000/060/original/dp598loym07sk.cloudfront.net/organizations/co_branding_logos/000/000/015/original/temp_uploads_2F1fa28efd-da9a-424b-bc17-167b3bcccafe_2FHewlett_Packard_Enterprise_logo.svg.png%253F1475919454?1472130694" }
        ]
        template = 'group-announcement.html.slim'
        subject = "New Forum Announcement from Sagar Bhute"
        send_mail(template, subject, global_merge_vars)
      end

      def send_activity_email
        common_template = 'new-comment-activity-notification'
        template = common_template +'.html.slim'
        STDOUT.puts "Enter Q for Question, A for annoucement & E for event"
        input = STDIN.gets.chomp.downcase
        if input == 'q'
          activity = 'Answer'
          STDOUT.puts "Enter 'locale' if any"
          input_locale = STDIN.gets.chomp.downcase

          if input_locale == 'es-es'
            subject = "Nueva respuesta a la pregunta Title of the Question"
            template = "localized/localized-" + common_template +"-"+ input_locale + ".html.slim"
            follow_class_name = "pregunta"
          elsif input_locale == 'pt-br'
            subject = "Nova resposta para pergunta Title of the Question"
            template = "localized/localized-" + common_template +"-"+ input_locale + ".html.slim"
            follow_class_name =  "perguntas"
          elsif input_locale == 'ar'
            subject = "Title of the post جواب جديد على السؤال"
            template = "localized/localized-" + common_template +"-"+ input_locale + ".html.slim"
            follow_class_name = "الأسئلة"
          elsif input_locale == 'fr'
            subject = "Nouvelle réponse à la question Title of the Question"
            template = "localized/localized-" + common_template +"-"+ input_locale + ".html.slim"
            follow_class_name = "questions"
          elsif input_locale == 'hi'
            subject = "प्रश्न Title of the Question का नया उत्तर"
            template = "localized/localized-" + common_template +"-"+ input_locale + ".html.slim"
            follow_class_name = "प्रश्नों"
          else
            subject = "New answer to Question Title of the Question"
            follow_class_name = "questions"
          end
        elsif input == 'a'
          activity = 'Comment'
          subject = "New comment on Post Title of the Post"
          follow_class_name = 'Announcement'
        else
          activity = 'Comment'
          subject = "New comment on Event Title of the event"
          follow_class_name = 'Event'
        end
        snippet = "<p>This is a first time Post for the QA environment. This environment looks great and is working fine.</p>"
        global_merge_vars = [
          {"name" => "follow_class_name", "content" => follow_class_name },
          {"name" => "followable_group_name", "content" => "QA group" },
          {"name" => "followable_group_client_name", "content" => "savannah-qa"},
          {"name" => "activity_snippet", "content" => snippet},
          {"name" => "activity", "content" => activity},
          {"name" => "link_url", "content" => "https://newlife.edcast.me/courses/783/learn#/posts"},
          {"name" => "unsubscribe_url", "content" => "unsubscribe_url"},
          { "name" => "show_edcast_name", "content" => @show_edcast_logo }
        ]
        send_mail(template, subject, global_merge_vars)
      end

      def send_event_reminder_email
        global_merge_vars = [
          {"name" => "event_name", "content" => "Testing Event" },
          {"name" => "event_description", "content" => "testing event email" },
          {"name" => "group_website_url", "content" => "https://savannah-qa.edcast.me/courses/17/learn?" },
          {"name" => "group_description" , "content" => "The Age of Sustainable Development" },
          { "name" => "show_edcast_name", "content" => @show_edcast_logo}
        ]
        send_mail('events-start-notification.html.slim', "Testing event starts soon", global_merge_vars)
      end

      def send_team_notification_mailer
        global_merge_vars = [
          {"name" => "post_group_name", "content" => "My Test team"},
          {"name" => "org_logo", "content" => @org_logo},
          {"name" => "post_message", "content" => "This is a test email from console"},
          {"name" => "post_user", "content" => "Sagar Bhute"},
          {"name" => "first_name", "content" => "FirstName"},
          {"name" => "unsubscribe_url", "content" => "unsubscribe_ur"},
          { "name" => "show_edcast_name", "content" => @show_edcast_logo}
        ]
        send_mail('admin-group-announcements.html.slim',"Welcome to your EdCast team!",global_merge_vars)
      end

      def send_import_confirmation
        global_merge_vars = [
          {"name" => "org_logo", "content" => @org_logo},
          {"name" => "first_name", "content" => "FirstName"},
          {"name" => "message", "content" => "Confirmation email of uploaded CSV is processed or failed"},
          {"name" => "show_edcast_name", "content" => @show_edcast_logo}
        ]
        send_mail("import_confirmation.html.slim", 'Uploaded CSV processed' , global_merge_vars)
      end

      def send_invitation_emails
        global_merge_vars = [
          {"name" => "host", "content" => "qa.cmnetwork.co" },
          {"name" => "invite_url", "content" => "https://95jo.test-app.link/HqlOP8mHqA"},
          {"name" => "org_logo", "content" => @org_logo},
          {"name" => "first_name", "content" => 'FirstName' },
          {"name" => "recipient_email", "content" => @email},
          {"name" => "inviter_name", "content" => "Sagar Bhute"},
          {"name" => "invitable_name", "content" => "QA "},
          {"name" => "invitable_photo", "content" => "https://dp598loym07sk.cloudfront.net/organizations/co_branding_logos/000/000/015/original/temp_uploads_2Ff3c62262-1088-4323-a19c-d1e24dc90673_2FQA-Logo-copy.png?1478848586"},
          {"name" => "org_logo", "content" => @org_logo},
          {"name" => "show_edcast_name", "content" => @show_edcast_logo}
        ]
        STDOUT.puts('Enter "O" for organization invite, "G" for group invite or "T" for team invite.')
        inv_type = STDIN.gets.chomp.downcase
        if inv_type == 'o'
            STDOUT.puts "Enter A to test 'admin' role or M to test 'member' role template"
            role = STDIN.gets.chomp.downcase
            if role == 'a'
              template = "edcast-organization-invite-admin.html.slim"
              subject = "[EdCast] Welcome to EdCast"
            else
              template = "edcast-organization-invite-member.html.slim"
              subject = "Invitation to join QA on EdCast"
            end
        elsif inv_type == 'g'
          template = 'edcast-topic-admin-invite.html.slim'
          subject = "[EdCast] Start sharing your Insights with your audience"
        else
          STDOUT.puts "Enter A to test 'admin' role or M to test 'member' role template"
          role = STDIN.gets.chomp.downcase
          if role == 'a'
            template = "edcast-team-invite-admin.html.slim"
            subject = "Invitation to join QA on EdCast"
          else
            template = "edcast-team-invite-member.html.slim"
            subject = "Invitation to join QA on EdCast"
          end
        end
        send_mail(template, subject, global_merge_vars)
      end

      def send_new_team_request
        global_merge_vars = [
          {"name"=>"first_name", "content"=>"Sagar"},
          {"name"=>"last_name", "content"=>"Bhute"},
          {"name"=>"customer_email", "content"=>@email},
          { "name" => "show_edcast_name", "content" => @show_edcast_logo}
        ]
        send_mail('edcast-new-team-request-to-staff.html.slim', "New Team Request from Sagar Bhute", global_merge_vars)
      end

      def send_dev_admin_invite
        global_merge_vars = [
          {"name"=>"first_name", "content"=>" anjan"},
          {"name"=>"invite_url", "content"=>"http://localhost:4000/developers/join/token"},
          {"name"=>"sender_name", "content"=>"Admin User"},
          {"name"=>"client_name", "content"=>"savannah-qa"},
          { "name" => "show_edcast_name", "content" => @show_edcast_logo}
        ]
        send_mail('dev_invite_email.html.slim','Invitation to Manage savannah-qa on EdCast', global_merge_vars)
      end

      def send_mobile_invitation
        global_merge_vars = [
          {"name" => "org_logo", "content" => @org_logo},
          {"name" => "first_name", "content" =>"Anjan"},
          {"name" => "invite_url", "content" => "http://localhost:4000/go/029eb71e72dedb85984f24738e9630ed"},
          { "name" => "show_edcast_name", "content" => @show_edcast_logo}
        ]
        send_mail('invite-email.html.slim','[EdCast] Sign in and get started',global_merge_vars)
      end

      def send_self_serve_email
        STDOUT.puts "Enter 's' to notify staff or 'a' to notify admin"
        notify_to = STDIN.gets.chomp.downcase
        global_merge_vars =
            [{"name"=>"first_name", "content"=>"Sagar"},
            {"name"=>"last_name", "content"=>"Bhute"},
            {"name"=>"organization_name", "content"=>"EdCast"},
            {"name"=>"industry", "content"=>""},
            {"name"=>"host_name", "content"=>"www"},
            {"name"=>"team_url", "content"=>"http://www.lvh.me"},
            {"name"=>"customer_email", "content"=>"sagar@edcast.com"},
            {"name" => "show_edcast_name", "content" => @show_edcast_logo}]
        if notify_to == 's'
          global_merge_vars +=
            [{"name"=>"customer_phone_number", "content"=>"9876543210"}]
          send_mail('edcast-self-serve-to-staff-v2.html.slim', "EFT request: Sagar Bhute for Edcast org", global_merge_vars)
        else
          send_mail('edcast-self-serve-to-customer-v3.html.slim', "Welcome to your EdCast team!", global_merge_vars)
        end
      end

      def send_user_mailer_confirm_email
        global_merge_vars = [
          {"name" => "first_name", "content" => "Anjan"},
          {"name" => "link_url", "content" => "http://qa.cmnetwork.co"},
          {"name" => "email", "content" => @email},
          {"name" => "show_edcast_name", "content" => @show_edcast_logo}
        ]
        send_mail('confirm-email-address.html.slim','Confirm your e-mail address',global_merge_vars)
      end

      def send_magic_link_email
        global_merge_vars = [
          {"name" => "first_name", "content" => "FirstName"},
          {"name" => "org_name", "content" => "QA org"},
          {"name" => "org_logo", "content" => @qa_org_logo},
          {"name" => "magic_link", "content" => "http://magiclink"},
          {"name" => "show_edcast_name", "content" => @show_edcast_logo}
        ]
        send_mail('user-magic-link.html.slim',"[Edcast] Magic Link",global_merge_vars)
      end

      def send_email(choice)
        if @email_directory[choice]
          self.send(@email_directory[choice][:func])
        else
          STDOUT.puts "Wrong number entered or you choose to QUIT, Byeee"
        end
      end


      def exit_task
        STDOUT.puts "You choose to quit BYE!! "
      end

      set_it_up
      choose_email_to_send

    else
      STDOUT.puts "Wont run on production, please run me on Dev, staging or QA"
    end
  end
end

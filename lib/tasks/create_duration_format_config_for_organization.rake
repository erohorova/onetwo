namespace :create_duration_format_config_for_organization do 
	desc 'create a config encryption for organizations'
	task :duration_config, [:org_id,:duration_format] => :environment do |_task, args|
    Config.find_or_create_by(configable_id: args.org_id, configable_type: 'Organization', name: 'duration_format', data_type: 'string', value: args.duration_format)
    puts 'Duration Format config created successfully'
  end
end
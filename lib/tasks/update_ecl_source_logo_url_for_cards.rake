namespace :cards do
  desc "Update existing card ecl source logo url, USE ENV['ORG_ID']"
  task update_ecl_source_logo_url_for_cards: :environment do
    counter = 0
    organization_ids = ENV.fetch("ORG_ID").split(",").map(&:to_i)

    organization_ids.present? && Organization.where(id: organization_ids).each do |org|
      source_logo_for = {}
      token = JWT.encode({ organization_id: org.id }, Settings.features.integrations.secret, 'HS256')
      headers = {
        "X-Api-Token": token,
        "Content-Type": "application/json",
        "Accept": "application/json",
      }

      # get sources for organization
      result = Faraday.new(url: Settings.ecl.api_endpoint).get do |req|
        req.url "/api/v1/sources?limit=2000&offset=0"
        req.headers = headers
      end

      if result.success?
        source_data = JSON.parse(result.body)['data']
        source_data.each do |source|
          source_logo_for[source['id']] = source["logo_url"]
        end
      end

      if source_logo_for.present?
        Card.where(organization_id: org.id).where.not(ecl_metadata: nil, ecl_id: nil).find_each do |card|
          if card.ecl_metadata["source_id"] && card.ecl_metadata["source_logo_url"].nil?
            source_id = card.ecl_metadata["source_id"]
            if source_logo_for[source_id].present?
              card_ecl_metadata = card.ecl_metadata
              card_ecl_metadata["source_logo_url"] = source_logo_for[source_id]
              card.update_columns(ecl_metadata: card_ecl_metadata)
              card.reindex_async
              counter += 1
              puts "Updated Card ID: #{card.id}, Organization ID: #{card.organization_id}"
            end
          end
        end
      end
    end

    puts "Total cards updated #{counter}"
  end
end

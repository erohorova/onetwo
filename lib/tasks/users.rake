namespace :users do
  desc 'save users to regenerate cover image...'
  task save: :environment do
    User.find_in_batches do |batch|
      puts "saving users batch from id #{batch.first.id}"
      batch.each &:save
    end
  end

  desc 'remove all stale users and associated data'
  task remove_stale: :environment do
    host_name = ENV["HOST_NAME"]

    if(host_name == "all")
      orgs = Organization.where.not(host_name: "www")
      orgs.each {|org| remove_stale_users(org) }
    else
      org = Organization.find_by(host_name: host_name)
      if org && host_name != "www"
        remove_stale_users(org)
      else
        log.error("Could not find organization with host_name: #{host_name} or default org")
      end
    end
  end

  desc 'save users to regenerate cover image...'
  task destiny_users_fix: :environment do
    wrong_destiny_uids = []
    Organization.where.not(id: Organization.default_org.id).find_each do |org|
      puts "--> Processing org: #{org.host_name}"
      User.where(organization_id: org.id, is_complete: true).find_each do |user|
        unless MetricRecord.search(query: {filtered: {filter: {and: [{term: {actor_id: user.id}}, {not: {term: {platform: 'forum'}}}]}}}).results.count > 0
          wrong_destiny_uids << user.id
        end
      end
    end

    puts "--> All orgs done. Found #{wrong_destiny_uids.count} number of bad users"
    wrong_destiny_uids.each do |uid|
      user = User.where(id: uid).first
      begin
        user.update!(is_complete: false)
        user.reindex
      rescue => e
        log.error "Updating user id: #{user.id} failed with exception: #{e}"
      end
    end
  end

  def remove_stale_users(org)
    stale_users = org.users.joins(:access_logs)
                      .where("access_logs.access_uri LIKE '%addon_stats%' and users.email IS NULL and is_complete = false")
    user_ids = stale_users.pluck(:id)

    if user_ids.present?
      begin
        UserContentLevelMetric.where(user_id: user_ids).delete_all
        UserLevelMetric.where(user_id: user_ids).delete_all
        stale_users.destroy_all
      rescue Exception => e
        log.error("Failed to delete stale users for organization: #{org.id}. Exception: #{e.message}")
      end
    end
  end


  task prefill_time_zone_for_users: :environment do
    class UserProfileVirtual < ActiveRecord::Base
      self.table_name = 'user_profiles'
    end

    Organization.where.not(id: Organization.default_org.id).find_each do |org|
      org.real_users.find_each do |user|
        user_profile = UserProfileVirtual.find_or_initialize_by(user_id: user.id)

        if user_profile.time_zone.nil?
          time_zone = IpLookupService.new(user.current_sign_in_ip).get_time_zone

          if time_zone.present?
            user_profile.time_zone = time_zone
            user_profile.save
          end
        end
      end
    end
  end

  desc 'suspend all users with empty email, first_name, last_name and identity_providers'
  task suspend_users: :environment do
    host_name = ENV["host_name"]
    user_id   = ENV["user_id"]

    if user_id.present?
      user = User.find(user_id)
      if !(user.identity_providers.present? && user.first_name.present? && user.last_name.present? && user.email.present?)
        user.suspend!
      end
    else
      if host_name.present?
        org = Organization.find_by(host_name: host_name)
        if org && host_name != "www"
          users = org
            .users
            .joins("LEFT JOIN identity_providers on users.id = identity_providers.user_id")
            .where("identity_providers.id IS NULL")
            .where("users.first_name IS NULL and users.last_name IS NULL and users.email IS NULL and users.is_suspended is false")

          p "Iterating over #{users.count} for an organization: #{host_name}"
          users.each do |user|
            begin
              p "Suspending user: #{user.id}"
              user.suspend!
            rescue => e
              log.error("Error while suspending user: #{user.id} with message: #{e.message}")
            end
          end
        else
          log.error("Could not find organization with host_name: #{host_name} or default org")
        end
      end
    end
  end

  desc 'create handle for all users with empty handle'
  task create_handle: :environment do
    host_name = ENV["host_name"]
    user_id   = ENV["user_id"]

    if user_id.present?
      user = User.find(user_id)
      if (!user.handle.present? && user.email.present? && !user.is_suspended && user.is_complete)
        user.assign_handle
        user.save!
        user.reindex_async
      end
    else
      if host_name.present?
        org = Organization.find_by(host_name: host_name)
        if org && host_name != "www"
          users = org
            .users
            .where("handle IS NULL and email IS NOT NULL and is_suspended is false and is_complete is true")

          p "Iterating over #{users.count} for an organization: #{host_name}"
          users.each do |user|
            begin
              p "Assigning handle to user: #{user.id}"
              user.assign_handle
              user.save!
              user.reindex_async
            rescue => e
              log.error("Error while suspending user: #{user.id} with message: #{e.message}")
            end
          end
        else
          log.error("Could not find organization with host_name: #{host_name} or default org")
        end
      end
    end
  end

  desc 'fix existing data for pubnub channels in ES'
  task reindex_users_for_pubnub: :environment do
    # https://app.bugsnag.com/edcast/1-lxp-production-tasks/errors/5bb5c6ae503ec5001839ef57?filters[event.since][0][type]=eq&filters[event.since][0][value]=30d&filters[search][0][type]=eq&filters[search][0][value]=pubnub
    # Started this bugsnag from 4 Oct, So we need to reindex data after 4th oct
    # Total users -  ~47k
    puts Time.now
    User.where("created_at > '2018-10-04 00:00:00'").find_in_batches(batch_size: 500) do |users|
      users.map(&:reindex_async)
    end
    puts Time.now
    puts "Finished reindexing"
  end

  desc 'Fix users and their statuses'
  task fix_statuses: :environment do
    # Total active records with `is_active = FALSE` & `is_suspended = FALSE` in production = 982.
    default_org = Organization.default_org
    User
      .where(status: 'active', is_active: false, is_suspended: false)
      .where('organization_id != ?', default_org.id)
      .find_each do |user|
        user.update_attribute(:is_active, true)
        user.reindex_async_urgent
    end

    # Total active records with `is_active = TRUE` & `is_suspended = TRUE` in production = 133.
    User
      .where(status: 'active', is_active: true, is_suspended: true)
      .where('organization_id != ?', default_org.id)
      .find_each do |user|
        user.update_attribute(:is_suspended, false)
        user.reindex_async_urgent
    end

    # Total suspended records with `is_active = TRUE` & `is_suspended = TRUE` in production = 12541.
    User
      .where(status: 'suspended', is_active: true, is_suspended: true)
      .where('organization_id != ?', default_org.id)
      .find_each do |user|
        user.update_attribute(:is_active, false)
        user.reindex_async_urgent
    end

    # Total suspended records with `is_active = TRUE` & `is_suspended = FALSE` in production = 12541.
    User
      .where(status: 'suspended', is_active: true, is_suspended: false)
      .where('organization_id != ?', default_org.id)
      .find_each do |user|
        user.update_attributes(is_active: false, is_suspended: true)
        user.reindex_async_urgent
    end
  end
end

namespace :migrate_post_reads do
  desc 'move post reads from database to elastic search'
  task copy: :environment do

    class PostReadDB < ActiveRecord::Base
      self.table_name = 'post_reads'
    end

    PostReadDB.find_in_batches(start: 0, batch_size: 100) do |post_reads|

      post_reads.each do |pr|
        read = PostRead.new(id: "post-id-#{pr.post_id}-user-id-#{pr.user_id}", #prevent duplicate read by manually constructing the primary key
                        group_id: pr.group_id,
                        user_id: pr.user_id,
                        post_id: pr.post_id,
                        created_at: pr.created_at,
                        updated_at: pr.updated_at)
        unless read.save
          log.warn("Not able to migrate post read elastic search #{read.as_json}")
        end

      end
    end
  end
end
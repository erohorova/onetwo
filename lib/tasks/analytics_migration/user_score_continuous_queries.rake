# frozen_string_literal: true

namespace :analytics do
  task run_user_score_continuous_queries: :environment do
    Analytics::UserScoreContinuousQueries.run_continuous_query
  end

  task backfill_user_score_continuous_queries: :environment do
    query = Analytics::UserScoreContinuousQueries.build_backfill_query
    INFLUXDB_CLIENT.query query
  end
end
# frozen_string_literal: true

# =========================================
# NOTE: THIS IS NOT CURRENTLY BEING USED.
# We are using CardMetricsAggregationRecorder instead,
# which persists to MySQL.
# =========================================

namespace :analytics do
  task run_card_metrics_aggregation_continuous_queries: :environment do
    Analytics::CardMetricsAggregationContinuousQueries.run_continuous_query
  end

  task backfill_card_metrics_aggregation_continuous_queries: :environment do
    query = Analytics::CardMetricsAggregationContinuousQueries.build_backfill_query
    INFLUXDB_CLIENT.query query
  end
end
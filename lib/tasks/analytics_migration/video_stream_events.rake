namespace :analytics_migration do

  desc "Migration Task: Run videostream create event for given org"
  task :migrate_videostream_create_event, [:org_id] => :environment do |_task, args|
    log_task "migrate_videostream_create_event", args.org_id
    migrate_vs_events args.org_id, "videostream_create", Analytics::CardMetricsRecorder::EVENT_CARD_CREATED
  end

  def videostream_metrics_record_query(org, action, start_date, end_date)
    ands = [
      {term: {action: action}},
      {term: {organization_id: org.id}},
      {range: {created_at: {gte: start_date, lte: end_date}}}
    ]

    {
      query: {
        filtered: {
          filter: {
            and: ands
          }
        }
      },
      sort: {
        created_at: {
          order: 'desc',
          ignore_unmapped: 'true'
        }
      },
      size: 5000
    }
  end

  def process_video_stream_results(results, event)
    puts "Processing #{results.size} results"

    results.each do |es_metric|
      card, actor = get_card_and_actor(es_metric)

      unless card
        puts "Card was not found #{es_metric.object_id}"
        next
      end

      additional_data = {
        platform: es_metric.platform,
        is_admin_request: 0
      }

      send_vs_metrics_to_influx(
        card, actor, event, es_metric.created_at,
        additional_data
      )
    end
  end

  def send_vs_metrics_to_influx(card, actor, action, timestamp, opts={})
    Analytics::MetricsRecorderJob.perform_later(
      recorder: "CardMetricsRecorder",
      org: Analytics::MetricsRecorder.org_attributes(card.organization),
      card: Analytics::MetricsRecorder.card_attributes(card),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      event: action,
      timestamp: timestamp.to_i,
      additional_data: opts
    )
  end

  def get_card_and_actor(es_metric)
    actor = nil
    card = Card.with_deleted.find_by(id: es_metric.object_id)
    if es_metric.actor_type == 'user'
      actor = User.find_by(id: es_metric.actor_id)
    end
    [card, actor]
  end

  def fetch_dates_from_env
    start_date = Date.strptime(ENV.fetch('START_DATE'), "%m/%d/%Y")
    end_date = Date.strptime(ENV.fetch('END_DATE'), "%m/%d/%Y")
    [start_date, end_date]
  end

  def migrate_vs_events(org_id, es_event, influx_event)
    start_date, end_date = fetch_dates_from_env
    org = Organization.find org_id
    (start_date..end_date).each do |date|
      query = videostream_metrics_record_query(org, es_event, date.beginning_of_day, date.end_of_day)

      results = MetricRecord.search(query).results
      puts "--------DATE #{date}----------------------------"
      puts "Results found #{results.count}"
      process_video_stream_results(results, influx_event)
    end
  end
end
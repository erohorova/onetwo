namespace :analytics_migration do
  desc "Migration Task: Run add to group event for given org"
  task :migrate_add_to_group_event, [:org_id] => :environment do |_task, args|
    log_task "migrate_add_to_group_event", args.org_id
    migrate_group_events args.org_id
  end

  def process_group_event_results(teams_users)
    puts "Processing #{teams_users.size} teams_users"

    teams_users.each do |tu|
      send_group_metrics_to_influx(tu)
    end
  end

  def send_group_metrics_to_influx(teams_user, opts={})
    team = teams_user.team
    actor = team.organization.admins.where(is_edcast_admin: false).first
    timestamp = teams_user.created_at.to_i
    user = teams_user.user
    as_type = teams_user.as_type

    Analytics::MetricsRecorderJob.perform_later(
      recorder: "GroupMembershipMetricsRecorder",
      org: Analytics::MetricsRecorder.org_attributes(actor.organization),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      group: Analytics::MetricsRecorder.group_attributes(team),
      member: Analytics::MetricsRecorder.user_attributes(user),
      role: as_type,
      event: Analytics::GroupMembershipMetricsRecorder::EVENT_GROUP_USER_ADDED,
      timestamp: timestamp,
      additional_data: {is_admin_request: 0}
    )
  end

  def migrate_group_events(org_id)
    org = Organization.find org_id
    teams_users = TeamsUser.where(team: org.teams.where(is_everyone_team: false))

    puts "Results found #{teams_users.count}"
    process_group_event_results(teams_users)
  end
end
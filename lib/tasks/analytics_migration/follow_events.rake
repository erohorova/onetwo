# This command will migrate data from db to influx for
# follow and unfollow events of user and channel models.

namespace :analytics_migration do
  desc 'migrate follow events for user'
  task :migrate_user_follow_db_events, [:org_id] => :environment do |_task, args|
    log_task "migrate_user_follow_db_events", args.org_id
    migrate_follow_events(args.org_id, 'user', 'follow', Analytics::UserMetricsRecorder::EVENT_USER_FOLLOWED)
  end

  desc 'migrate un-follow events for user'
  task :migrate_user_unfollow_db_events, [:org_id] => :environment do |_task, args|
    log_task "migrate_user_unfollow_db_events", args.org_id
    migrate_follow_events(args.org_id, 'user', 'unfollow', Analytics::UserMetricsRecorder::EVENT_USER_UNFOLLOWED)
  end

  desc 'migrate follow channel event'
  task :migrate_channel_follow_db_events, [:org_id] => :environment do |_task, args|
    log_task "migrate_channel_follow_db_events", args.org_id
    migrate_follow_events(args.org_id, 'channel', 'follow', Analytics::ChannelMetricsRecorder::EVENT_CHANNEL_FOLLOWED)
  end

  desc 'migrate un-follow channel event'
  task :migrate_channel_unfollow_db_events, [:org_id] => :environment do |_task, args|
    log_task "migrate_channel_unfollow_db_events", args.org_id
    migrate_follow_events(args.org_id, 'channel', 'unfollow', Analytics::ChannelMetricsRecorder::EVENT_CHANNEL_UNFOLLOWED)
  end

  desc "Migration Task: Run all follows migrations for an org"
  task :migrate_all_follows_for_org, [:org_id] => :environment do |_task, args|
    log_task "migrate_add_to_group_event", args.org_id

    task_names = %w{
      analytics_migration:migrate_user_follow_db_events
      analytics_migration:migrate_user_unfollow_db_events
      analytics_migration:migrate_channel_follow_db_events
      analytics_migration:migrate_channel_unfollow_db_events
    }
    tasks = task_names.map(&Rake::Task.method(:[]))
    tasks.each(&:reenable).each { |task| task.invoke args.org_id }
  end

  desc "Migration Task: Run all follows migration for all orgs"
  task migrate_all_follows_for_all_org: :environment do |_task, args|
    Organization.
    exclude_default_org.
    order(id: :asc).
    pluck(:id, :host_name).each do |org_id, host_name|
      sleep 2
      task = Rake::Task["analytics_migration:migrate_all_follows_for_org"]
      task.tap(&:reenable).invoke org_id
    end
  end

  # migrate follow event for a given org

  def migrate_follow_events(org_id, followable_type, es_event, influx_event)
    org = Organization.find org_id
    query = follow_metrics_record_query(org, es_event, followable_type)
    puts "Results found #{query.count}"
    process_follow_event_results(query, es_event, influx_event, followable_type)
  end

  #  ----common methods
  def follow_metrics_record_query(org, es_event, followable_type)
    query = nil
    if es_event == 'follow' && followable_type == 'user'
      query = Follow.with_deleted
        .joins(
          "INNER JOIN users ON user_id = users.id
            AND followable_type = 'User'
            AND users.organization_id = #{org.id}"
        )
    elsif es_event == 'unfollow' && followable_type == 'user'
      query = Follow.with_deleted
        .joins(
          "INNER JOIN users ON user_id = users.id
            AND followable_type = 'User'
            AND deleted_at IS NOT NULL
            AND users.organization_id = #{org.id}"
        )
    elsif es_event == 'follow' && followable_type == 'channel'
      query = Follow.with_deleted
        .joins(
          "INNER JOIN users ON user_id = users.id
            AND followable_type = 'Channel'
            AND users.organization_id = #{org.id}"
        )
    elsif es_event == 'unfollow' && followable_type == 'channel'
      query = Follow.with_deleted
        .joins(
          "INNER JOIN users ON user_id = users.id
            AND followable_type = 'Channel'
            AND deleted_at IS NOT NULL
            AND users.organization_id = #{org.id}"
        )
    end
    query
  end

  def process_follow_event_results(results, es_event, influx_event, followable_type)
    puts "Processing #{results.size} results"

    results.find_in_batches(batch_size: 100).each do |follows|
      follows.each do |follow|

        followable, actor = follow.followable, follow.user

        unless followable
          puts "followed #{follow.followable_type} was not found #{follow.followable_id}"
          next
        end

        timestamp = nil
        if es_event == 'follow'
          timestamp = follow.created_at
        else
          timestamp = follow.deleted_at
        end

        if timestamp > (Time.now.utc - 3.hours)
          puts "Warning: event occurred in past 3 hour window"
        end

        send_follow_metrics_to_influx(
          followable, actor, influx_event,
          timestamp.to_i,
          {is_admin_request: 0}
        )

      end
    end
  end

  def send_follow_metrics_to_influx(followable, actor, event, timestamp, metadata)
    case followable
    when Channel
      Analytics::MetricsRecorderJob.perform_later(
        recorder: "ChannelFollowRecorder",
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        follower: Analytics::MetricsRecorder.user_attributes(actor),
        followed: Analytics::MetricsRecorder.channel_attributes(followable),
        event: event,
        additional_data: metadata,
        timestamp: timestamp
      )
    when User
      Analytics::MetricsRecorderJob.perform_later(
        recorder: "UserFollowRecorder",
        org: Analytics::MetricsRecorder.org_attributes(actor.organization),
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        follower: Analytics::MetricsRecorder.user_attributes(actor),
        followed: Analytics::MetricsRecorder.user_attributes(followable),
        event: event,
        additional_data: metadata,
        timestamp: timestamp
      )
    end
  end

end
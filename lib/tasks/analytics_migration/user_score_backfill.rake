
namespace :analytics do

  desc "Backfill user score records in Influx"
  task backfill_user_scores: :environment do

    measurements = [
      # "\"users\"",
      "\"groups\"",
      "channels",
      "cards",
      # "searches"
    ]

    measurements.each do |measurement|

      offset = 0
      limit = 100

      loop do

        records = INFLUXDB_CLIENT.query(<<-TXT).shift["values"]
          SELECT * FROM #{measurement} LIMIT #{limit} OFFSET #{offset}
        TXT

        # separate tags/values in records
        records.each do |record|
          # push the record through the UserScoreRecorder
          insertion_record = Analytics::MetricsRecorder.response_to_insertion(
            measurement,
            record
          )
          Analytics::MetricsRecorder.push_auxiliary_metrics(
            measurement,
            insertion_record
          )
        end

        break if records.length < limit
        offset += limit

      end

    end

  end

end
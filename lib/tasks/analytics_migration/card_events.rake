namespace :analytics_migration do

  def log_task(task_name, org_id)
    puts "RUNNING #{task_name} FOR ORG: #{org_id}"
  end

  desc "Migration Task: Run all migrations"
  task :migrate_all_for_org, [:org_id] => :environment do |_task, args|
    task_names = %w{
      analytics_migration:migrate_card_create
      analytics_migration:migrate_card_like
      analytics_migration:migrate_card_comment
      analytics_migration:migrate_card_bookmark
      analytics_migration:migrate_card_mark_completed_self
      analytics_migration:migrate_card_mark_completed_assigned
      analytics_migration:migrate_card_view
      analytics_migration:migrate_card_impression
    }
    tasks = task_names.map(&Rake::Task.method(:[]))
    tasks.each(&:reenable).each { |task| task.invoke args.org_id }
  end

  desc 'Migration Task: Move card create events to influx'
  task :migrate_card_create, [:org_id] => :environment do |_task, args|
    # =========================================
    # NOTE: THIS HAS NOT BEEN UPDATED SINCE THE EVENT card_created_virtual
    # WAS ADDED.
    #
    # IT MAY NEED TO BE UPDATED BEFORE IT IS RUN.
    # =========================================
    log_task "migrate_card_create", args.org_id
    migrate_events args.org_id, "create", "card_created"
  end

  desc 'Migration Task: Move card like events to influx'
  task :migrate_card_like, [:org_id] => :environment do |_task, args|
   log_task "migrate_card_like", args.org_id
    migrate_events args.org_id, "like", "card_liked"
  end

  desc 'Migration Task: Move card comment events to influx'
  task :migrate_card_comment, [:org_id] => :environment do |_task, args|
    log_task "migrate_card_comment", args.org_id
    migrate_events args.org_id, "comment", "card_comment_created"
  end

  desc 'Migration Task: Move card bookmark events to influx'
  task :migrate_card_bookmark, [:org_id] => :environment do |_task, args|
    log_task "migrate_card_bookmark", args.org_id
    migrate_events args.org_id, "bookmark", "card_bookmarked"
  end

  desc 'Migration Task: Move card mark_completed_self events to influx'
  task :migrate_card_mark_completed_self, [:org_id] => :environment do |_task, args|
    log_task "migrate_card_mark_completed_self", args.org_id
    migrate_events args.org_id, "mark_completed_self", "card_marked_as_complete"
  end

  desc 'Migration Task: Move card mark_completed_assigned events to influx'
  task :migrate_card_mark_completed_assigned, [:org_id] => :environment do |_task, args|
    log_task "migrate_card_completed_assigned", args.org_id
    migrate_events args.org_id, "mark_completed_assigned", "card_marked_as_complete"
  end

  desc 'Migration Task: Move card view events to influx'
  task :migrate_card_view, [:org_id] => :environment do |_task, args|
    log_task "migrate_card_view", args.org_id
    migrate_events args.org_id, "visit", "card_viewed"
  end

  desc 'Migration Task: Move card view events to influx'
  task :migrate_card_impression, [:org_id] => :environment do |_task, args|
    log_task "migrate_card_view", args.org_id
    migrate_events args.org_id, "impression", "card_viewed"
  end

  def card_metrics_record_query(org, action, start_date, end_date)
    ands = [
      {term: {action: action}},
      {term: {organization_id: org.id}},
      {term: {object_type: 'card'}},
      {range: {created_at: {gte: start_date, lte: end_date}}}
    ]

    ands << {term: {"properties.view"  => "full"}} if action == 'impression'
    {
      query: {
        filtered: {
          filter: {
            and: ands
          }
        }
      },
      sort: {
        created_at: {
          order: 'desc',
          ignore_unmapped: 'true'
        }
      },
      size: 5000
    }
  end

  def process_card_event_results(results, event)
    puts "Processing #{results.size} results"

    results.each do |card_event|
      card, actor = get_card_and_actor(card_event)

      unless card
        puts "Card was not found #{card_event.object_id}"
        next
      end

      unless actor
        puts "actor was not found #{card_event.actor_id}"
        next
      end

      additional_data = {
        platform: card_event.platform,
        is_admin_request: 0
      }

      send_card_metrics_to_influx(
        card, actor, event, card_event.created_at,
        additional_data
      )
    end
  end

  def send_card_metrics_to_influx(card, actor, action, timestamp, opts={})
    Analytics::MetricsRecorderJob.perform_later(
      recorder: "CardMetricsRecorder",
      org: Analytics::MetricsRecorder.org_attributes(card.organization),
      card: Analytics::MetricsRecorder.card_attributes(card),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      event: action,
      timestamp: timestamp.to_i,
      additional_data: opts
    )
  end

  def get_card_and_actor(card_event)
    actor = nil
    card = Card.with_deleted.find_by(id: card_event.object_id)
    if card_event.actor_type == 'user'
      actor = User.find_by(id: card_event.actor_id)
    end
    [card, actor]
  end

  def fetch_dates_from_env
    start_date = Date.strptime(ENV.fetch('START_DATE'), "%m/%d/%Y")
    end_date = Date.strptime(ENV.fetch('END_DATE'), "%m/%d/%Y")
    [start_date, end_date]
  end

  def migrate_events(org_id, es_event, influx_event)
    start_date, end_date = fetch_dates_from_env
    org = Organization.find org_id
    (start_date..end_date).each do |date|
      query = card_metrics_record_query(org, es_event, date.beginning_of_day, date.end_of_day)

      results = MetricRecord.search(query).results
      puts "--------DATE #{date}----------------------------"
      puts "Results found #{results.count}"
      process_card_event_results(results, influx_event)
    end
  end
end

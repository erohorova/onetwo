namespace :analytics_migration do

  desc "Migration Task: Run card assigned event for given org"
  task migrate_card_assigned: :environment do |_task, args|
    log_task "migrate_card_assigned_event", "all orgs"
    migrate_assigned_events "assign", Analytics::CardMetricsRecorder::EVENT_CARD_ASSIGNED
  end

  def process_assign_event_results(results, influx_event)
    puts "Processing #{results.size} results"

    results.each do |es_metric|
      card, assignee = get_card_and_assignee(es_metric)

      unless card && assignee
        puts "Card or assgnee was not found #{es_metric.object_id}"
        next
      end

      additional_data = {
        platform: es_metric.platform,
        is_admin_request: 0
      }

      send_assigned_metrics_to_influx(
        card, assignee, influx_event, es_metric.created_at.to_i,
        additional_data
      )
    end
  end

  def send_assigned_metrics_to_influx(card, assignee, influx_event, timestamp, opts={})
    # Try to find actor for the assignment i.e. assignor
    # if nil or not found set as self assigned
    actor = find_actor(card, assignee) || assignee
    Analytics::MetricsRecorderJob.perform_later(
      recorder: "CardAssignmentMetricsRecorder",
      card: Analytics::MetricsRecorder.card_attributes(card),
      org: Analytics::MetricsRecorder.org_attributes(card.organization),
      actor: Analytics::MetricsRecorder.user_attributes(actor),
      assignee: Analytics::MetricsRecorder.user_attributes(assignee),
      event: "card_assigned",
      timestamp: timestamp,
      additional_data: opts
    )
  end

  def find_actor(card, assignee)
    assignment = Assignment.find_by(assignable: card, assignee: assignee)
    TeamAssignment.find_by(assignment_id: assignment.id)&.assignor if assignment
  end

  def get_card_and_assignee(es_metric)
    # actor in ES is the assignee of the assignment in DB
    assignee = nil
    card = nil
    if es_metric.object_type == 'video_stream'
      vs = VideoStream.find_by(id: es_metric.object_id)
      card = vs.card
    elsif es_metric.object_type == 'card'
      card = Card.with_deleted.find_by(id: es_metric.object_id)
    end

    if es_metric.actor_type == 'user'
      assignee = User.find_by(id: es_metric.actor_id)
    end
    [card, assignee]
  end

  def fetch_dates_from_env
    start_date = Date.strptime(ENV.fetch('START_DATE'), "%m/%d/%Y")
    end_date = Date.strptime(ENV.fetch('END_DATE'), "%m/%d/%Y")
    [start_date, end_date]
  end

  def migrate_assigned_events(es_event, influx_event)
    # Open the "view" of the index
    metric_records = []
    ands = [
      {term: { action: 'assign' }},
    ]

    response = MetricRecord.gateway.client.search(
      index: MetricRecord.index_name,
      body: {
        query: {
          filtered: {
            filter: {
              and: ands
            }
          }
        },
        sort: {
          created_at: {
            order: 'desc',
            ignore_unmapped: 'true'
          }
        }
      },
      search_type: 'scan', scroll: '2m',
      size: 1000
    )

    # Call `scroll` until results are empty
    while response = MetricRecord.gateway.client.scroll(scroll_id: response['_scroll_id'], scroll: '2m') and not response['hits']['hits'].empty? do
      sleep 0.1
      response['hits']['hits'].each { |r| metric_records << MetricRecord.new(r['_source']) }
    end
    puts "Metrics found #{metric_records.count}------"
    process_assign_event_results(metric_records, influx_event)
  end
end
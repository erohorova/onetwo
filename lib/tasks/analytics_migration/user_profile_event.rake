namespace :analytics_migration do

  desc "Migration Task: Run add to group event for given org"
  task :migrate_user_profile_event, [:org_id] => :environment do |_task, args|
    log_task "migrate_user_profile_event", args.org_id
    migrate_user_expert_topic_added args.org_id
    migrate_user_interest_topic_added args.org_id
  end

  def migrate_user_expert_topic_added(org_id)
    user_profiles = UserProfile.joins(:user)
      .where("users.organization_id = ? AND user_profiles.expert_topics IS NOT NULL", org_id)
    user_profiles.each do |user_profile|
      puts "USER PROFILE ID #{user_profile.id}"
      user_profile.expert_topics.each do |topic_hash|
        record_metric(user_profile, 'expert_topics', topic_hash)
      end
    end
  end

  def migrate_user_interest_topic_added(org_id)
    user_profiles = UserProfile.joins(:user)
      .where("users.organization_id = ? AND user_profiles.learning_topics IS NOT NULL", org_id)

    user_profiles.each do |user_profile|
      puts "USER PROFILE ID #{user_profile.id}"
      user_profile.learning_topics.each do |topic_hash|
        record_metric(user_profile, 'learning_topics', topic_hash)
      end
    end
  end

  def record_metric(user_profile, topic_type, topic_hash)
    user = user_profile.user
    Analytics::UserProfileRecorder.push_added_topic(
      Analytics::MetricsRecorder.org_attributes(user.organization),
      Analytics::MetricsRecorder.user_attributes(user),
      Analytics::MetricsRecorder.user_profile_attributes(user_profile),
      topic_type,
      topic_hash.with_indifferent_access.slice(*Analytics::UserProfileRecorder::WHITELISTED_TOPIC_KEYS),
      user_profile.updated_at.to_i,
      {is_admin_request: 0}
    )
  end
end
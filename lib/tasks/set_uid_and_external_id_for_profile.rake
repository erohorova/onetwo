namespace :profile do
  desc 'Set uid and external_id for old external profile'
  task set_uid_and_external_id: :environment do
    User.where.not(
      organization: Organization.default_org
    ).find_in_batches(batch_size: 5000) do |users|
      profiles = []
      users.each do |user|
        profile = Profile.find_or_initialize_by(
          user: user,
          organization: user.organization
        )
        profile.uid = SecureRandom.uuid if profile.uid.blank?
        profile.external_id = profile.uid if profile.external_id.blank?
        profiles.push(profile)
      end
      Profile.import profiles, validate: false, on_duplicate_key_update: [:uid, :external_id]
    end
  end
end
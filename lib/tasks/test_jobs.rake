require 'rubygems'
require 'rake'

namespace :test do
  desc "Test jobs directory"
  Rails::TestTask.new(jobs: 'test:prepare') do |t|
    t.libs << 'test'
    t.pattern = 'test/jobs/**/*_test.rb'
    t.verbose = true
  end
end

Rake::Task['test:run'].enhance ['test:jobs']

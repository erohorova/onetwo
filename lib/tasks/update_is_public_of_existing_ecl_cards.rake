namespace :cards do
  desc "Update existing card is_public"
  task :update_is_public_of_existing_ecl_cards, [:organization_id, :source_id, :created_at] => :environment do |_task, args|
    counter = 0
    condition = <<~HEREDOC.strip_heredoc.chomp.gsub("\n", " ")
          organization_id = #{args.organization_id}
          and ecl_metadata like '{"source_id":"#{args.source_id}"%'
          and created_at >= "#{args.created_at}"
        HEREDOC
    Card.where(condition).find_each do |card|
      counter += 1
      EclApi::CardConnector.new.propagate_card_to_ecl(card.id)
      puts "Updated card id: #{card.id}"
    end
    puts "Total cards updated #{counter}"
  end
end
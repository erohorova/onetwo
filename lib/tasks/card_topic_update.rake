namespace :update_card_topic do
  # This rake will update topic, node_id and taxo_id in card table
  # org id and taxonomy topic prefix will be taken from environment variable

  desc "This will update topic, node_id in user_profile table"

  task :card_topic_update, [:organization_id, :taxo_prefix, :taxo_root_flag, :old_root] => [:environment] do |t, args|
    @org_id = args[:organization_id]

    # Create token using org_id and sociative secret key
    token = JWT.encode({organization_id: @org_id}, Settings.sociative.sociative_auth_secret, 'HS256')

    # Get taxonomy topics and create a hash having 'key' as node_path and value as name, domain_id, topic_id, taxo_id
    # and domain_name
    taxonomy_topics = Adapter::Sociative.new({ organization_id: @org_id }).get_taxonomy_topics(token)

    # Get all cards for that organization
    cards = Card.where(organization_id: @org_id).where.not(taxonomy_topics: nil)
    cards.each do |card|
      # Get taxonomy topics and call convert_topic function
      topic = card.taxonomy_topics
      if topic.present?
        # card_topic = []
        card_topic = topic.map do |learning_topic|
          TopicUpdateService.new("card").convert_topics(taxonomy_topics, learning_topic, args[:taxo_prefix], args[:taxo_root_flag], args[:old_root])
        end

        # Update card topic in db
        if card_topic.present?
          card.update_attribute(:taxonomy_topics, card_topic)
          log.info "Updated topic, node id for card #{card.id}"
        end
      end
    end
    end
end
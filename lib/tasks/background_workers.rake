namespace :background_workers do

  def workers
    background_workers = BackgroundWorker.all
    if !ENV['WORKER_NAME'].blank?
      background_workers.where!(name: ENV['WORKER_NAME'])
    end
    background_workers
  end

  desc 'Pause background worker process. Set WORKER_NAME=process_name, leave empty to pause all processes'
  task pause: :environment do
    workers.each do |worker|
      log.info "Pausing background worker: #{worker.name}"
      worker.state = 'stop'
      worker.save!
    end
  end

  desc 'Resume background worker process. Set WORKER_NAME=process_name, leave empty to resume all processes'
  task resume: :environment do
    workers.each do |worker|
      log.info "Resuming background worker: #{worker.name}"
      worker.state = 'run'
      worker.save!
    end
  end

  desc 'List all the registered workers'
  task list: :environment do
    BackgroundWorker.all.each do |worker|
      puts "Worker: #{worker.to_json}"
    end
  end
end

# frozen_string_literal: true

namespace :analytics do
  task run_group_score_aggregation: :environment do
    Analytics::GroupScoreAggregationJob.new.perform
  end
end
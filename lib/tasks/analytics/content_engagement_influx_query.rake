# frozen_string_literal: true

namespace :analytics do
  task run_content_engagement_influx_query: :environment do
    Analytics::ContentEngagementMetricsJob.new.perform
  end
end
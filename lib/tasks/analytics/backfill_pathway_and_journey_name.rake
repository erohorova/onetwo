# frozen_string_literal: true

# =========================================
# NOTE: In EP-19049 we added pathway_name and journey_name
# to the card_added_to_pathway and card_added_to_journey events
#
# We need to update the existing data to have these extra fields.
# =========================================

namespace :analytics do

  task backfill_pathway_name: :environment do

    require 'byebug'
    load './scripts/analytics/backfill_pathway_and_journey_name.rb'
    Analytics::BackfillPathwayAndJourneyName.run_for_pathway_events!

  end

  task backfill_journey_name: :environment do

    require 'byebug'
    load './scripts/analytics/backfill_pathway_and_journey_name.rb'
    Analytics::BackfillPathwayAndJourneyName.run_for_journey_events!

  end

end

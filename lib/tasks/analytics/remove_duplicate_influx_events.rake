# frozen_string_literal: true

namespace :analytics do

  desc "remove duplicate card view events"
  task remove_duplicate_card_view_events: :environment do
    duplicate_remover = Analytics::DuplicateRecordRemover.new(
      grouping_columns: %w{_card_id _user_id},
      conditions: { event: "card_viewed" },
      min_time: (Time.now.end_of_day - 1.days - 4.hours).to_i,
      max_time: (Time.now.end_of_day - 1.days).to_i,
      org_ids: [1454],
      measurement: "cards",
      time_frame: "1s"
    )
    duplicate_remover.run
  end

  desc "remove duplicate card completion events"
  task remove_duplicate_card_completion_events: :environment do
    duplicate_remover = Analytics::DuplicateRecordRemover.new(
      grouping_columns: %w{_card_id _user_id},
      conditions: { event: "card_marked_as_complete" },
      min_time: (Time.now - 500.days).to_i,
      org_ids: [1454],
      measurement: "cards",
      time_frame: "1s"
    )
    duplicate_remover.run
  end


end



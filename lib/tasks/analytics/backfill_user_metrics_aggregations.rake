# frozen_string_literal: true

namespace :analytics do
  task backfill_user_metrics_aggregations: :environment do
    Organization.all.each do |org|
      Analytics::UserMetricsAggregationRecorder.new(
        # -----------------------------------------
        # CUSTOMIZE BELOW OPTS ON AN AS-NEEDED BASIS
        # CAN REMOVE org_ids OPT TO RUN FOR ALL ORGS
        # -----------------------------------------
        # org_ids: [org.id],
        # org_ids: [1690],
        start_time: 0
        # end_time: (Time.now.end_of_day - 1.day).to_i,
        # start_time: (Time.now.beginning_of_day - 7.months).to_i
      ).run
    end
  end
end

=begin

to verify the accuracy of results, after creating records for all orgs
and all time:

1. Get count from SQL

  UserMetricsAggregation.count
  # => 30098

2. Get count from Influx:

  INFLUXDB_CLIENT.query(
    "SELECT COUNT(total_user_score) FROM user_scores_daily"
  )[0]["values"][0]["count"]
  # => 30098

=end
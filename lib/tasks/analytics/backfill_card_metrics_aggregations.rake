# frozen_string_literal: true

namespace :analytics do
  task backfill_card_metrics_aggregations: :environment do
    Analytics::CardMetricsAggregationRecorder.new(
      # -----------------------------------------
      # CUSTOMIZE BELOW OPTS ON AN AS-NEEDED BASIS
      # CAN REMOVE org_ids OPT TO RUN FOR ALL ORGS
      # -----------------------------------------
      org_ids: Organization.pluck(:id) - [1],
      start_time: 0,
      # end_time: Time.now.end_of_day - 1.day,
      # start_time: Time.now.beginning_of_day - 1400.day
    ).run
  end
end
  
=begin

  =========================================
  To verify the accuracy of results, after creating records for all orgs
  and all time:
  =========================================

  1.  Get unique count of card ids in SQL:

      CardMetricsAggregation.select(:card_id).distinct.count
      # => 5259

  2. Get last end_time of SQL records

      end_time = CardMetricsAggregation.order("end_time DESC").first.end_time
      # => 1531267199

  3.  Get unique count of card ids in INFLUX:

      %w{
        card_viewed
        card_liked
        card_marked_as_complete
        card_bookmarked
        card_comment_created
        card_assigned
      }.flat_map do |event|
        INFLUXDB_CLIENT.query(
          "SELECT DISTINCT(card_id) FROM cards " +
          "WHERE event='#{event}' " +
          "AND time <= #{end_time}s"
        )[0]["values"].map { |val| val["distinct"] }
      end.uniq.count
      # => 5259

  4.  Get the total count of card_viewed in SQL:

      CardMetricsAggregation.pluck(:num_views).sum
      # => 18046

  5.  Get total count of card_viewed in Influx:

      INFLUXDB_CLIENT.query(
        "SELECT COUNT(card_id) FROM cards " +
        "WHERE event='card_viewed' " +
        "AND time <= #{end_time}s"
      )[0]["values"][0]["count"]
      # => 18046

=end

# frozen_string_literal: true

# =========================================
#
# INSTRUCTIONS FOR PERFORMING THIS BACKFILL:
#
#   1. DO NOT SIMPLY RUN THE RAKE TASK FROM BASH
#
#   2. OPEN RAILS CONSOLE AND RUN THE (COMMENTED OUT) COMMANDS BELOW
#      TO CREATE AN EXPORT FOR ONE OF THE EVENTS
#
#   3. CLOSE RAILS CONSOLE AND OPEN IT AGAIN WITH INFLUX CONFIGURED TO POINT
#      AT A NEW DATABASE
#
#   4. COPY-PASTE THE COMMANDS IN THE RAKE TASK INTO RAILS CONSOLE
#      THIS WILL POPULATE THE NEW DATABASE WITH UPDATED EVENTS
#
#   5. DELETE THE EVENTS IN THE ORIGINAL DATABASE AND COPY THEM OVER FROM
#      THE NEW DATABASE
#
# =========================================

dont_run_directly = -> {
  puts <<-TXT
    DONT RUN THIS RAKE TASK DIRECTLY.
    FOLLOW THE INSTRUCTIONS IN backfill_user_ids_for_user_events.rake

    (EXITING)
  TXT
  exit
}

namespace :analytics do

# NOTE: to create backup:
=begin
    load './scripts/analytics/backfill_user_ids_for_user_events.rb'
    min_date = Date.new(2018,5,10).to_time.utc.beginning_of_day.to_i
    Analytics::BackfillUserIdsForUserEvents.build_backup("users", "user_created", start_date: min_date)
=end
  task backfill_user_ids_for_user_created: :environment do
    dont_run_directly.call

    require 'byebug'
    load './scripts/analytics/backfill_user_ids_for_user_events.rb'
    min_date = Date.new(2018,5,10).to_time.utc.beginning_of_day.to_i
    Analytics::BackfillUserIdsForUserEvents.run_for_user_created_events!(min_date)

  end

# NOTE: to create backup:
=begin
    load './scripts/analytics/backfill_user_ids_for_user_events.rb'
    Analytics::BackfillUserIdsForUserEvents.build_backup("users", "user_edited")
=end
  task backfill_user_ids_for_user_edited: :environment do
    dont_run_directly.call

    require 'byebug'
    load './scripts/analytics/backfill_user_ids_for_user_events.rb'
    Analytics::BackfillUserIdsForUserEvents.run_for_user_edited_events!
  end

end

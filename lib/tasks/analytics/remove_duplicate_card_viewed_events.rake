# frozen_string_literal: true

namespace :analytics do

  desc "remove duplicate card view events"
  task remove_duplicate_card_view_events: :environment do
    require 'byebug'
    duplicate_remover = Analytics::DuplicateRecordRemover.new(
      grouping_columns: %w{_card_id _user_id},
      conditions: { event: "card_viewed" },
      min_time: (Time.now - 5.days).to_i,
      measurement: "cards",
      time_frame: "1s"
    )
    duplicate_remover.run
  end

end



# frozen_string_literal: true

namespace :analytics do
  task run_org_score_continuous_queries: :environment do
    Analytics::OrgScoreContinuousQueries.run_continuous_query
  end

  task backfill_org_score_continuous_queries: :environment do
    query = Analytics::OrgScoreContinuousQueries.build_backfill_query
    INFLUXDB_CLIENT.query query
  end
end
# This rake is to index existing cards which are added ecl enabled channels
# ChannelEclSourcesFetchJob uses channel_ids of card ES query to exclude unwanted card ids
# Card#add_ecl_card_to_channel doesnt reindex card
# added callback to reindex card as soon as it is added to channel (Commit: c7dabb9)
# 
namespace :ecl do
  desc 'index cards for ecl enabled channels'
  task index_ecl_enabled_channel_cards: :environment do
    counter = 0

    ChannelsCard.joins(:channel).where("channels.ecl_enabled = ? ", true).select("DISTINCT channels_cards.card_id").each do |record|
       IndexJob.perform_later("Card", record.card_id)
       puts "Card Id #{record.card_id} indexed"
       counter +=1
    end

    puts "Total cards indexed: #{counter}"
  end
end

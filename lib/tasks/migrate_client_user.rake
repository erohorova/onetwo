namespace :migrate_client_user do
  desc 'create missing thread cards'
  task copy: :environment do
    groups_users = GroupsUser.all.each do |gu|
      user = gu.user
      puts "GroupsUser: #{gu.to_json}"
      ClientsUser.find_or_create_by(client_id: gu.group.client_id,
                         user_id: user.id,
                         client_user_id: gu.client_user_id
                        )
    end

  end
end
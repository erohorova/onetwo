require 'yaml'

namespace :deploy do

  desc "Deploy to AWS Elastic Beanstalk"
  task :update do
    output = %x[eb list]
    raise "Error! Check AWS console." unless $?.success?
    environment_list = output.split("\n").map do |name|
      if name_match = /([a-z-]+)-v(\d+)/.match(name.gsub('*', '').strip)
        {full_name: name_match[0],name: name_match[1], version: name_match[2].to_i}
      elsif name_match = /([a-z-]+)/.match(name.gsub('*', '').strip)
        {full_name: name_match[0],name: name_match[0], version: 0}
      end
    end.compact.sort do |x,y|
      #sort by name first then version number
      if x[:name] == y[:name]
        y[:version] <=> x[:version]
      else
        x[:name] <=> y[:name]
      end
    end

    puts "Which environment do you want to update?"
    puts "#{environment_list.join("\n")}"
    environment = nil
    until environment
    print "Type in the name string(#{environment_list.first[:name]}): "
      environment_name = STDIN.gets.chomp.strip
      environment = environment_list.find do |item|
        item[:name] == environment_name
      end
    end

    puts 'Which docker image do you want to use?'
    docker_image = nil
    until docker_image && docker_image.length > 0
      print "Type in the docker image name(master, or some-other-branch): "
      docker_image = STDIN.gets.chomp.strip
    end
    %x[git fetch --quiet]
    old_environment_name = environment[:full_name]

    cfg_name = old_environment_name
    cfg_name += '-v0' if environment[:version] == 0
    puts 'Saving current configuration...'
    %x[eb config delete #{environment[:full_name]} --cfg #{cfg_name}]
    %x[eb config save #{environment[:full_name]} --cfg #{cfg_name}]
    raise "Error! Check AWS console." unless $?.success?

    #override the DOCKER_IMAGE env variable
    config_file_path = "./.elasticbeanstalk/saved_configs/#{cfg_name}.cfg.yml"
    old_cfg_yaml_file = YAML.load_file config_file_path
    old_cfg_yaml_file['OptionSettings']['aws:elasticbeanstalk:application:environment']['DOCKER_IMAGE'] = docker_image
    File.open(config_file_path, 'w') { |f| YAML.dump(old_cfg_yaml_file, f) }

    #create a minimal package needed to Elastic Beanstalk
    zip_output = %x[zip .elasticbeanstalk/deploy.zip Dockerrun.aws.json .ebextensions/*]
    puts "Creating the source package for Elastic Beanstalk"
    puts zip_output
    eb_config_file_path = './.elasticbeanstalk/config.yml'
    eb_config_yaml_file = YAML.load_file eb_config_file_path
    eb_config_yaml_file['deploy'] = {'artifact' => './.elasticbeanstalk/deploy.zip'}
    File.open(eb_config_file_path, 'w') { |f| YAML.dump(eb_config_yaml_file, f) }


    new_environment_name = "#{environment[:name]}-v#{environment[:version]+1} "
    puts "Launching new environment: #{new_environment_name}"
    puts %x[eb create #{new_environment_name} --cfg #{cfg_name} --nohang]
    raise "Error! Check AWS console." unless $?.success?

    puts 'This step will take a while. Please check AWS console for the progress'

    ready = false
    until ready
      print '.'
      output = %x[eb status #{new_environment_name}]
      ready = output.include?('Status: Ready') && output.include?('Health: Green')
      sleep 10 unless ready
    end

    puts "\n"
    puts output

    puts 'Please manually verify that the new deployment is working before accepting live traffic.'

    swap_environment = false
    until swap_environment
      print "Ready to accept live traffic?(type swap): "
      swap_environment = STDIN.gets.chomp.strip == 'swap'
    end


    puts 'Swapping environment...'
    %x[eb swap #{new_environment_name} --destination_name #{old_environment_name}]
    raise "Error! Check AWS console. `eb swap` command is currently broken. Please use AWS Elastic Beanstalk instead. Don't forget to terminate the old environment!" unless $?.success?
    puts 'New environment is now accepting live traffic'

    terminate_old_environment = false
    until terminate_old_environment
      print "Terminate the old #{old_environment_name} environment?(type terminate): "
      terminate_old_environment = STDIN.gets.chomp.strip == 'terminate'
    end
    puts "Terminating #{old_environment_name} environment..."
    %x[eb terminate #{old_environment_name} --force]
    puts "All is done!!!"

    ENV['deployname'] = new_environment_name
    Rake::Task['deploy:notify'].execute
  end

  desc 'Notify slack that deploy has been performed'
  task :notify do
    url = 'https://hooks.slack.com/services/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'
    pars = {
        text: "#{ENV['deployname'] || 'EdCast'} has been deployed on AWS.",
        channel: '#deploy',
        username: 'Elastic Beanstalk',
        icon_emoji: ':edcast:',
    }
    Curl.post(url, pars.to_json)
  end
end

task deploy: ['deploy:update']

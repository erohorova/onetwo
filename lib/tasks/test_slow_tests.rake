require 'rubygems'
require 'rake'

namespace :test do
  desc "Test slow tests that require full database commit"
  Rails::TestTask.new(slow_tests: 'test:prepare') do |t|
    t.libs << 'test'
    t.pattern = 'test/slow_tests/**/*_test.rb'
    t.verbose = true
  end

end

Rake::Task['test:run'].enhance ['test:slow_tests']

# This will add 'CAN_RATE' permission to all the users who has that role.
# On Production roles present for all organizations are 11,000

# rake add_permission_can_rate
task add_permission_can_rate: :environment do
  Role.where.not(organization_id: Organization.default_org.id).find_in_batches do |roles|
    roles.each do |role|
      role.role_permissions.find_or_create_by(name: 'CAN_RATE', description: 'Can Rate').enable!
      puts "permission CAN_RATE is added for role: #{role.name} for organization_id: #{role.organization_id}"
    end
  end
end #task end

namespace :fetch_topic do
  # Example ORG_HOST=xyz URL='ur1, ur2' rake fetch_topic:seed
  task :seed => :environment do
    return unless ENV['ORG_HOST'] || ENV['URL']

    urls = ENV['URL'].split(", ")
    urls.each do |url|
      puts "Fetching url: #{url}"
      begin
        org = Organization.where(host_name: ENV['ORG_HOST']).first_or_create do |org|
                                            org.name = 'test org',
                                            org.status = 'active',
                                            org.description = "test org"
                                    end

        puts "ORG ERROR: #{org.errors.full_messages}" if !org.errors.empty?
        source_url = url
        rss_link = Cms::ContentSource.rss_link_from_source(source_url)
        puts "RSS LINK: #{rss_link}"
        content_source = Cms::ContentSource.find_or_create_by(source: rss_link)
        cso = nil
        if content_source
          cso = Cms::ContentSourcesOrganization.where(organization: org, cms_content_source: content_source, name: source_url, display_source: source_url).first_or_create
          content_source.fetch
        else
          puts "Error occurred while creating content source #{source_url}"
        end

        puts "NO OF CARDS: #{org.cards.count}"
        topics = org.cards.map(&:tags).flatten.map(&:name).uniq.join(", ")
        org.cards.destroy_all
        puts "TOPICS for URL: #{url} -> #{topics}"
      rescue Exception => e
        puts "Exception occurred: #{e.message}"
      end
    end
  end
end

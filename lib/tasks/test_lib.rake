require 'rubygems'
require 'rake'

namespace :test do
  desc "Test lib modules"
  Rails::TestTask.new(lib: 'test:prepare') do |t|
    t.libs << 'test'
    t.pattern = 'test/lib/**/*_test.rb'
    t.verbose = true
  end
end

Rake::Task['test:run'].enhance ['test:lib']

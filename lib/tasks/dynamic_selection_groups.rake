namespace :dynamic_selection_groups do
  desc 'remove non-existent dynamic selections groups'
  task :remove, [:host_name] => :environment do |t, args|
    # Usage:
    # - specified org by host name:
    #   rake dynamic_selection_groups:remove['www']
    #                                :remove[www]
    # - all orgs exclude default:
    #   rake dynamic_selection_groups:remove
    host_name = args[:host_name]
    organizations = if host_name.present?
      Organization.where(host_name: host_name)
    else
      Organization.exclude_default_org
    end

    organizations.find_each do |org|
      org.dynamic_group_revisions.where.not(external_id: nil).find_each do |ds_rev|
        # destroy dynamic selection if it not present on workflow side
        ds_rev.destroy unless get_dynamic_group(ds_rev.external_id, org.host_name)
      end
    end
  end

  def get_dynamic_group(ext_id, host_name)
    url = "api/dynamic-selections/#{ext_id}"
    conn = Faraday.new(:url => Settings.workflows.host_url) do |faraday|
      faraday.adapter Faraday.default_adapter
      faraday.options[:open_timeout] = 2
      faraday.options[:timeout] = 15
      faraday.headers['x-api-token'] = Settings.workflows.edcast_master_token || ''
      faraday.headers['org-name'] = host_name
      faraday.headers['Content-Type'] = 'application/json'
    end

    begin
      res = conn.get { |req| req.url url}
      # do not remove if entry obtained
      res.status.to_i.in?(200..299)
    rescue Faraday::ConnectionFailed, Faraday::TimeoutError => e
       log.warn('Connection to Workflow failed')
       true # do not remove if connection failed
    end
  end
end

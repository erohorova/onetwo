namespace :sync_dynamic_groups do
  desc 'sync valid groups between LXP & Workflows. Not valid groups will be non dynamic'
  task :update, [:host_name] => :environment do |t, args|
    host_name = args[:host_name]
    org = Organization.find_by(host_name: host_name)
    unless org
      puts(<<-TXT)
        Organization not found or host_name blank. Host name: #{host_name}
      TXT
      return
    end
    output_source_ids, workflow_ids = extract_workflow_objects(org)

    valid_data_ids = WorkflowPatchRevision.where(
      organization_id: org.id,
      workflow_type: 'groups',
      output_source_id: output_source_ids,
      workflow_id: workflow_ids
    ).pluck(:data_id)

    dynamic_groups = DynamicGroupRevision.where(uid: valid_data_ids)
    valid_team_ids = dynamic_groups.pluck(:group_id).compact
    invalid_teams = org.teams.where(is_dynamic: true).where.not(id: valid_team_ids)

    puts(<<-TXT)
      Not valid dynamic groups ids: #{invalid_teams.pluck(:id)}
    TXT

    invalid_teams.update_all(is_dynamic: false)
    DynamicGroupRevision.where.not(group_id: valid_team_ids).delete_all
  end

  # return array of existed workflows in provided organization
  def extract_workflow_objects(org)
    updated_url = Settings.workflows.host_url&.gsub(/w{3}/, org.host_name)
    conn = Faraday.new(:url => updated_url) do |faraday|
      faraday.adapter Faraday.default_adapter
      faraday.options[:open_timeout] = 2
      faraday.options[:timeout] = 15
      faraday.headers['x-api-token'] = Settings.workflows.edcast_master_token
      faraday.headers['org-name'] = org.host_name
      faraday.headers['Content-Type'] = 'application/json'
    end

    begin
      res = conn.get { |req| req.url('api/workflows') }
      workflows = JSON.parse(res.body)
      unless workflows.is_a?(Array)
        puts(<<-TXT)
          Extracted info is not valid for processing: #{workflows}
        TXT
        return
      end
      w_objects = workflows.select { |i| i['graphXMLSchema'].present? }
      return [w_objects.map { |i| i['outputSourceId']}, w_objects.map { |i| i['id']}]
    rescue => e
      puts(<<-TXT)
        Could not obtain Workflow instance information. Exception: #{e.inspect}
      TXT
    end
  end
end

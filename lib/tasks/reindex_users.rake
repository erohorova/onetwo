namespace :reindex_users do
  desc "reindex visible users not from default org with expert topics"
  task reindex_users_with_expert_topics: :environment do
    User.not_suspended.where.not(organization_id: Organization.default_org.id)
      .joins(:profile).where.not(user_profiles: {expert_topics: nil})
      .find_in_batches do |users|
        users.each do |user|
          begin
            user.reindex
          rescue => e
            log.error "Unable to reindex user with id: #{user.id} with Exception: #{e.message}"
          end
        end
    end
    puts "Reindexing was successfully completed"
  end

  # Command: ` rake 'reindex_users:whose_role_names_differ_from_default_name[100010 100005]' `
  # Seperate org_ids with spaces and enclose the rake command in single quotes to consider all the arguments
  desc "reindex users whose roles are changed"
  task :whose_role_names_differ_from_default_name,  [:org_id] => [:environment] do |t, args|
    org_ids = args[:org_id].split(" ").map {|arg| arg.to_i}
    org_ids.each do |org_id|
      puts "Running for Org_id--->", org_id
      User.not_suspended.where(organization_id: org_id)
        .joins(user_roles: :role).where(
          "roles.name != roles.default_name AND
          users.organization_id = roles.organization_id")
        .select(:id)
        .find_in_batches do |batch|
          ids = batch.map {|user| user.id}
          BulkPartialReindexJob.perform_now(ids: ids, klass_name: 'User', method_name: :roles_data)
        end
      puts "Completed Enqueing for Org_id--->", org_id
    end
  end
end

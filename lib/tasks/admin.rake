namespace :admin do

  desc "create a list of admins"
  task create_admin: :environment do
    u = User.new('email' => 'admin@edcast.com', 'password' => 'password', 'first_name' => 'Admin', 'last_name' => 'User')
    if u.save
      SuperAdminUser.create!(user: u)
      puts "saved new user with email: #{u.email}"
    else
      puts "failed to save user: #{u.email}"
    end
  end

  desc 'Reset the db and create admin user'
  task :reset => :environment do
    puts 'admin:reset - Resetting existing database'
    Rake::Task['db:migrate:reset'].invoke # does db:drop, db:create, db:migrate

    # reset table info for any models that might have been changed during migration
    Dir[Rails.root.join('app/models/*.rb').to_s].each do |filename|
      klass = File.basename(filename, '.rb').camelize.constantize
      next unless klass.ancestors.include?(ActiveRecord::Base)
      klass.reset_column_information
    end

    class Organization; def create_general_channel(*_); end; end
    default_org = Organization.create!(name: "EdCast", host_name: "www", is_open: true, description: "Edcast open channel")
    au = User.new(email: 'admin@edcast.com', first_name: 'Admin', last_name: 'User', 'password' => 'Password1!', organization: default_org, organization_role: 'admin')
    au.skip_confirmation!
    au.save!
    SuperAdminUser.create!(user: au)
  end

  desc 'initialize all elastic indices'
  task elastic_reset: :environment do
    def elastic_persisted?(k)
      k.try(:included_modules).try(:include?, Elasticsearch::Persistence::Model)
    end

    Object.constants.each do |c|
      klass = Object.const_get(c)
      if elastic_persisted?(klass) && !elastic_persisted?(klass.superclass)
        puts "initializing elastic index for persisted model #{c}"
        klass.create_index! force: true
      elsif klass.respond_to?(:searchkick_index) && !klass.superclass.respond_to?(:searchkick_index)
        puts "initializing elastic index for searchkick model #{c}"
        klass.searchkick_index.delete if klass.searchkick_index.exists?
        klass.reindex
      end
    end
  end

  desc 'Populate db with sample data'
  task sample: [:reset, :elastic_reset] do

    return unless (Rails.env.development? || Rails.env.test?)
    class PubnubNotifier; def self.notify(*_); end; end
    puts 'admin:sample - Populating database'
    au = User.cm_admin
    default_org = Organization.default_org
    fu1 = User.create!(email: 'facultyuserone@edcast.com', first_name: 'Facultyuser', last_name: 'One', 'password' => 'Password1!', organization: default_org, organization_role: 'admin')
    fu2 = User.create!(email: 'facultyusertwo@edcast.com', first_name: 'Facultyuser', last_name: 'Two', 'password' => 'Password1!', organization: default_org, organization_role: 'admin')
    su1 = User.create!(email: 'firststudentuser@edcast.com', first_name: 'First', last_name: 'Studentuser', 'password' => 'Password1!', organization: default_org, organization_role: 'member')
    su2 = User.create!(email: 'secondstudentuser@edcast.com', first_name: 'Second', last_name: 'Studentuser', 'password' => 'Password1!', organization: default_org, organization_role: 'member')

    #generate test client and api key for development
    client = Client.create!(user: au, name: 'test_client')
    if Rails.env.development?
      website_url_first = 'http://localhost:3000/ui/demo?group=first&mode=wide'
      website_url_second = 'http://localhost:3000/ui/demo?group=second&mode=wide'
    else
      website_url_first = "http://#{ENV['API_SERVER_LOCATION']}/ui/demo?group=first&mode=wide"
      website_url_second = "http://#{ENV['API_SERVER_LOCATION']}/ui/demo?group=second&mode=wide"
    end

    resource1 = ResourceGroup.create(name: 'test group 1',
                             description: 'first test group',
                             client_resource_id: 'test_resource',
                             client: client,
                             website_url: website_url_first,
                             creator: au)
    resource2 = ResourceGroup.create(name: 'test group 2',
                             description: 'second test group',
                             client_resource_id: 'test_resource_2',
                             client: client,
                             website_url: website_url_second,
                             creator: au)

    ClientsUser.create(user: su1, client: client, client_user_id: su1.id)
    ClientsUser.create(user: su2, client: client, client_user_id: su2.id)
    ClientsUser.create(user: fu1, client: client, client_user_id: fu1.id)
    ClientsUser.create(user: fu2, client: client, client_user_id: fu2.id)
    ClientsUser.create(user: au, client: client, client_user_id: au.id)

    resource1.add_member su1
    resource1.add_admin fu1

    resource2.add_member su2
    resource2.add_admin fu2

    tag_snippets = ['#hw1 #hw2 #hw3', '#lecture1 #lecture2 #lecture3', '#midterm #final', '#lab1 #lab2 #lab3', '']

    students = 4.times.map do |i|
      student = User.create!(email: "student#{i}@edcast.com", first_name: 'Stu', last_name: "Dent#{i}", 'password' => 'password', organization: Organization.default_org)
      ClientsUser.create!(user: student, client: client, client_user_id: student.id)
      resource1.add_member student
      student
    end

    students.last(3).each &:make_influencer
    students.last(2).each {|u| u.global_influencer.update visible_during_onboarding: true }

    10.times do |i|
      student = students[i % students.count]
      question = Question.create!(
          user: student,
          postable: resource1,
          title: "question #{i+1}",
          message: "<p>I have a question #tag#{i} #{tag_snippets[i % tag_snippets.count]} </p>",
          context_id: 'eee1',
          context_label: 'sample-context')
      Answer.create!(user: fu1, commentable: question, message: "some comment")
      Answer.create!(user: su1, commentable: question, message: "other comment")
    end

    #confirm all the user accounts
    User.find_each do |user|
      user.confirm!
    end

    User.update_all configs: '{"user-feeds-tour":"1","chrome-extension-tour":"1","card-tour":"1"}'

    team = Team.create!(name: 'Team 1', description: 'First team.', organization: default_org)
    team.add_admin students[-1]
    team.add_member students[-2]
    team.add_member students[-3]

    %w[
    cards:v1:configure:media_cards
    cards:v1:configure:poll_cards
    cards:v1:configure:pack_cards
    admin:add_channels
    admin:add_interests
    admin:create_email_login
    ].each do |rake_task|
      puts "admin:sample - invoking '#{rake_task}'"
      Rake::Task[rake_task].execute
    end
  end

  task create_email_login: :environment do
    Sso.create(name: 'email')
  end

  task add_channels: :environment do
    default_org = Organization.default_org
    fu1 = User.find_by(email: 'facultyuserone@edcast.com', organization: default_org)
    fu2 = User.find_by(email: 'facultyusertwo@edcast.com', organization: default_org)

    arch = Channel.create(
      description: 'Learn everything new about Architecture from this Channel',
      image:       File.new("#{Rails.root}/test/fixtures/images/architecture.jpg"),
      label:       'Architecture',
      autopull_content: true,
      only_authors_can_post: false,
      visible_during_onboarding: true,
      organization: default_org
    )

    Channel.create(
      description: 'Learn everything new about Fitness from this Channel',
      image:       File.new("#{Rails.root}/test/fixtures/images/fitness.jpeg"),
      label:       'Fitness',
      autopull_content: true,
      only_authors_can_post: false,
      visible_during_onboarding: true,
      organization: default_org
    )

    Channel.create(
      description: 'Learn everything new about Food Science from this Channel',
      image:       File.new("#{Rails.root}/test/fixtures/images/food_science.jpg"),
      label:       'Food Science',
      autopull_content: true,
      only_authors_can_post: false,
      visible_during_onboarding: true,
      organization: default_org
    )

    Channel.create(
      description: 'Learn everything new about Sustainability from this Channel',
      image:       File.new("#{Rails.root}/test/fixtures/images/sustainability.jpg"),
      label:       'Sustainability',
      autopull_content: true,
      only_authors_can_post: false,
      visible_during_onboarding: true,
      organization: default_org
    )

    Channel.create(
      description: 'Learn everything new about Business from this Channel',
      image:       File.new("#{Rails.root}/test/fixtures/images/business.jpeg"),
      label:       'Business',
      autopull_content: true,
      only_authors_can_post: false,
      visible_during_onboarding: true,
      organization: default_org
    )

    Channel.create(
      description: 'Learn everything new about Technology from this Channel',
      image:       File.new("#{Rails.root}/test/fixtures/images/technology.jpeg"),
      label:       'Technology',
      autopull_content: true,
      only_authors_can_post: false,
      visible_during_onboarding: true,
      organization: default_org
    )

    Channel.create(
      description: 'Learn everything new about Music from this Channel',
      image:       File.new("#{Rails.root}/test/fixtures/images/music.jpeg"),
      label:       'Music',
      autopull_content: true,
      only_authors_can_post: false,
      visible_during_onboarding: true,
      organization: default_org
    )

    Channel.create(
      description: 'Learn everything new about Entrepreneurship from this Channel',
      image:       File.new("#{Rails.root}/test/fixtures/images/entrepreneurship.jpg"),
      label:       'Entrepreneurship',
      autopull_content: true,
      only_authors_can_post: false,
      visible_during_onboarding: true,
      organization: default_org
    )

    Channel.create(
      description: 'Learn everything new about Nuclear Security from this Channel',
      image:       File.new("#{Rails.root}/test/fixtures/images/nuclear_security.jpg"),
      label:       'Nuclear Security',
      autopull_content: true,
      only_authors_can_post: false,
      visible_during_onboarding: true,
      organization: default_org
    )
    Channel.create(
      description: 'Learn everything new about EdTech from this Channel',
      image:       File.new("#{Rails.root}/test/fixtures/images/edtech.jpeg"),
      label:       'EdTech',
      autopull_content: true,
      only_authors_can_post: false,
      visible_during_onboarding: true,
      organization: default_org
    )

    class ResourceGroup; def promotion_eligible?; true; end; end
    ResourceGroup.last(2).each do |group|
      group.update image_url: 'favicons/android-chrome-192x192.png'
      Promotion.promote(
          group,
          url: Rails.application.routes.url_helpers.learn_url(
              id: arch.slug,
              host: 'lvh.me',
              port: '4000',
          ),
          tags: 'architecture|promotion'
      )
    end
  end

  desc 'delete users from the system'
  task delete_users: :environment do
    emails = ENV['EMAILS'].try(:split, ',')
    if emails.blank?
      puts "--> Usage: 'EMAILS=to-be-deleted-1@domain.com,to-be-deleted-2@domain.com' rake admin:delete_users'"
      exit
    end

    now = Time.now.utc
    default_org_id = Organization.default_org.id
    emails.each do |email|
      u = User.find_by(organization_id: default_org_id, email: email)
      if u.nil?
        puts "--> User with email: #{email} not found. Moving on"
        next
      end
      deleted_email = "del-#{email.gsub('@', 'at').gsub('.', 'dot')}-on-#{now.strftime("%F")}-admin"[0..63] + "@edcast.com"
      followed_users_count = u.followed_users.count
      cards_count = Card.where(author_id: u.id).count
      STDOUT.puts "Deleting user with email: #{email}. handle: #{u.handle}. This user is following #{followed_users_count} users and has created #{cards_count} cards. Are you sure (Y/N)?"
      input = STDIN.gets.strip
      if input == 'Y'
        email_entry = Email.find_or_initialize_by(email: email, user: u)
        email_entry.email = deleted_email
        email_entry.bounced_at = now
        email_entry.save!
        u.update_columns(email: deleted_email, handle: nil)
        puts "--> Done"
      else
        puts "--> Okay. Moving on then"
      end
    end
  end

  desc 'create sample destiny content; run after admin:sample'
  task sample_destiny: :environment do
    Card.searchkick_index.delete if Card.searchkick_index.exists?

    default_org = Organization.default_org || Organization.create!(name: "EdCast", host_name: "www", description: "Edcast open channel", is_open: true)

    Channel.find_or_create_by(
        description: 'All about Homework 1, Lecture 1, and Lab 1',
        label:       'The First Week',
        organization_id: default_org.id
    ) do |ch|
      ch.image = File.new("#{Rails.root}/test/fixtures/images/default_circle.jpg")
      ch.visible_during_onboarding = true
    end
    Channel.find_or_create_by(
        description: 'All about the final exam',
        label:       'Final Exam',
        organization_id: default_org.id
    ) do |ch|
      ch.image = File.new("#{Rails.root}/test/fixtures/images/default_circle.jpg")
      ch.visible_during_onboarding = true
    end
    Channel.find_or_create_by(
        description: 'All about the midterm',
        label:       'Midterm Tips',
        organization_id: default_org.id
    ) do |ch|
      ch.image = File.new("#{Rails.root}/test/fixtures/images/default_circle.jpg")
      ch.visible_during_onboarding = true
    end

    PostsController.skip_before_action(:cm_auth_user, :verify_authenticity_token)
    jawohl = ->(*_){ true }
    PostsController.send(:define_method, :authenticate_user!, &jawohl)
    Group.send(:define_method, :is_active_super_user?, &jawohl)
    session = ActionDispatch::Integration::Session.new(Rails.application)
    Post.all.each do |p|
      session.post Rails.application.routes.url_helpers.convert_to_card_post_path(p), format: :json
      puts JSON.parse(session.response.body)
    end

    pack_users = User.last(2)
    ENV['author_id'] = pack_users.last.id.to_s
    ENV['other_id'] = pack_users.first.id.to_s
    Rake::Task['admin:sample_card_packs'].execute
    Rake::Task['admin:sample_video_streams'].execute
    User.last(2).permutation.each do |u1, u2|
      Follow.create!(user: u1, followable: u2)
      Follow.create!(user: u1, followable: Channel.first)
    end
  end

  desc 'create sample video streams content'
  task sample_video_streams: :environment do

    VideoStream.reset_callbacks(:commit)

    creator = User.where(organization: Organization.default_org).first
    2.times do |i|
      WowzaVideoStream.create!(
          creator: creator,
          name: "#{creator.name} upcoming stream #{i+1}",
          status: 'upcoming',
          start_time: (i+1).weeks.since,
      )
      WowzaVideoStream.create!(
          creator: creator,
          name: "#{creator.name} past stream #{i+1}",
          status: 'past',
          start_time: (i+1).weeks.ago,
      )
      WowzaVideoStream.create!(
          creator: creator,
          name: "#{creator.name} live stream #{i+1}",
          status: 'live',
          start_time: Time.now,
      )
    end

    Recording.reset_callbacks(:commit)
    creator.video_streams.where(status: 'past').each do |str|
      next if str.past_stream_available?
      Recording.create!(
          transcoding_status: 'available',
          video_stream: str,
          sequence_number: 1,
          hls_location: 'hls_location',
          mp4_location: 'mp4_location'
      )
    end
  end

  # requires db with admin:sample or cards:v1:configure:pack_cards
  # uses last 4 cards by author and last 2 cards by other (as available)
  # run automatically by admin:sample_destiny
  desc 'create sample card pack content; required: author_id=x, other_id=y'
  task sample_card_packs: :environment do
    author = User.find_by(id: ENV['author_id'])
    other = User.find_by(id: ENV['other_id'])
    abort 'author and other not found' unless author && other

    card_params = {
        is_public: true,
        author_id: author.id
    }

    author_cards = Card.where(author_id: author.id).last(4)
    other_cards = Card.where(author_id: other.id).last(2)

    %w[pack pack_draft].each do |type|
      _, result = CardsApi::cards_create(card_params.merge(
          {
              type: type,
              ui_layout_type: 'simple',
              contents: {
                  title: "my #{type}",
                  description: "this is the #{type} by #{author.name}",
                  image_url: Rails.root.join('app/assets/images/events-image.png')
              },
          }
      ))
      CardPackRelation.create!(
          cover_id: result['id'],
          from_id: result['id'],
          from_type: 'Card',
      )
      author_card = author_cards.pop
      CardPackRelation.add(
          cover_id: result['id'],
          add_id: author_card.id,
          add_type: 'Card',
      ) if author_card
      other_card = other_cards.pop
      CardPackRelation.add(
          cover_id: result['id'],
          add_id: other_card.id,
          add_type: 'Card',
      ) if other_card
      author_card = author_cards.pop
      CardPackRelation.add(
          cover_id: result['id'],
          add_id: author_card.id,
          add_type: 'Card',
      ) if author_card
    end
  end

  desc 'fetch content'
  task fetch_content: :environment do
    Cms::ContentSource.find_each do |cs|
      ContentSourceFetchJob.perform_now(cs.id)
    end
  end

  desc 'adding interests to channels'
  task add_interests: :environment do
    arch = Channel.find_by_label('Architecture')
    ['Facade','Vault','Skyscrapers','Hacienda','Gentrification'].each do |interest|
      arch.interests << Interest.create(name: interest)
    end

    fitness = Channel.find_by_label('Fitness')
    ['Protein Diet','Crunches','Workout Routines','Body Workouts','Yoga'].each do |interest|
      fitness.interests << Interest.create(name: interest)
    end

    foodsci = Channel.find_by_label('Food Science')
    ['Atkins Diet','Gastronomy','Braising','Sauté','SousVide'].each do |interest|
      foodsci.interests << Interest.create(name: interest)
    end

    sustainability = Channel.find_by_label('Sustainability')
    ['Sustainability','Sustainable Development','Jeffry Sachs','UN SDSN','Carbon Footprint'].each do |interest|
      sustainability.interests << Interest.create(name: interest)
    end

    business = Channel.find_by_label('Business')
    ['Sales','Marketing','Revenue','Lead Generation','Social Media Marketing'].each do |interest|
      business.interests << Interest.create(name: interest)
    end

    tech = Channel.find_by_label('Technology')
    ['Ruby','Ruby on Rails','Artificial Intelligence','Algorithms','Data Structures'].each do |interest|
      tech.interests << Interest.create(name: interest)
    end

    music = Channel.find_by_label('Music')
    ['Guitar','Bass','Piano','AC/DC','Taylor Swift'].each do |interest|
      music.interests << Interest.create(name: interest)
    end

    entrepreneurship = Channel.find_by_label('Entrepreneurship')
    ['Startups','Valuations','Growth Hacks','Unicorns','Startup Equity'].each do |interest|
      entrepreneurship.interests << Interest.create(name: interest)
    end

    nuclearsec = Channel.find_by_label('Nuclear Security')
    ['Nuclear','Security','Nuclear War','Warheards','Cold War'].each do |interest|
      nuclearsec.interests << Interest.create(name: interest)
    end

    edtech = Channel.find_by_label('EdTech')
    ['Online Learning','LMS','Online Certifications','Micro Learning','Self Education'].each do |interest|
      edtech.interests << Interest.create(name: interest)
    end
  end

  desc 'remove channels from default org'
  task channels_clean_up: :environment do
    puts "Running deploy task 'channels_clean_up'"

    ActiveRecord::Base.transaction do
      default_org_channels = Organization.default_org.channels

      slugs = ENV['CHANNELS'].try(:split, ',')
      if slugs.blank?
        puts "--> Usage: 'CHANNELS=to-be-deleted-1,to-be-deleted-2' rake after_party:channels_clean_up'"
        exit
      end

      org_channels_delete = default_org_channels.where(slug: slugs)

      Rails.logger.info "DELETING CHANNELS: #{org_channels_delete.map(&:id)}"

      Follow.where(followable: org_channels_delete).destroy_all
      ChannelsUser.where(channel: org_channels_delete).destroy_all
      PubnubChannel.where(item: org_channels_delete).destroy_all
      ChannelsVideoStream.where(channel: org_channels_delete).destroy_all

      # card
      channels_cards = ChannelsCard.where(channel: org_channels_delete)
      card_ids = channels_cards.map(&:card_id)
      Rails.logger.info "REINDEX CARDS: #{card_ids}"

      channels_cards.destroy_all
      Card.where(id: card_ids).map(&:reindex_card)

      # video stream
      channels_video_streams = ChannelsVideoStream.where(channel: org_channels_delete)
      video_stream_ids = channels_video_streams.map(&:video_stream_id)
      Rails.logger.info "REINDEX VIDEO STREAMS: #{video_stream_ids}"

      channels_video_streams.destroy_all
      VideoStream.where(id: video_stream_ids).map(&:reindex_async)

      org_channels_delete.destroy_all
    end

  end  # task :channels_clean_up

  desc 'set up brightcove account id to access brightcove videos'
  task set_up_brightcove_account: :environment do
    if ENV['ORG_HOST_NAME'].blank? || ENV['ACCOUNT_ID'].blank?
      puts "--> Usage: 'ORG_HOST_NAME=www ACCOUNT_ID=123456' rake admin:set_up_brightcove_account'"
      exit
    end
    org = Organization.find_by_host_name(ENV['ORG_HOST_NAME'])
    exit unless org
    puts "Setting brightcove for #{org.host_name} with account id #{ENV['ACCOUNT_ID']}"
    config = Config.where(category: 'brightcove', name: 'account_id', configable: org).first_or_create!
    config.data_type = 'string'
    config.value = ENV['ACCOUNT_ID']
    config.save!
  end

  desc 'create data for analytics dashboard'
  task sample_v2: [:reset, :elastic_reset, :create_email_login] do

    def random_number
      1 + rand(100)
    end

    rand_num = random_number

    puts "--> Creating Organization #{rand_num}"

    org = Organization.create!(host_name: "org#{rand_num}", is_open: true, name: "Organization #{rand_num}", description: "Organization Description")

    channels = []

    admin = User.new(email: "admin1@edcast.com", password: "Password1!", organization: org, organization_role: 'admin', is_complete: true)
    admin.skip_confirmation!
    admin.save!

    # Set onboarding complete
    uo = UserOnboarding.where(user_id: admin.id).first
    uo.current_step = 3
    uo.status = "completed"
    uo.completed_at = Date.today
    uo.save!


    members = []
    (0..10).each do |index|
      member = User.new(email: "user#{index}@edcast.com", password: "Password1!", organization: org, organization_role: 'member', is_complete: true)
      member.skip_confirmation!
      member.save!
      members << member
    end

    # Create teams
    teams = []
    (0..10).each do |index|
      team = Team.new(name: "team#{index}", organization_id: org.id, description: "team #{index} description")
      team.save!
      teams << team
    end

    content_items = []
    # cards_config = CardsConfig.find_by(card_type: "media", ui_layout_type: "link") || CardsConfig.new(user_id: User.first.id, card_type: "media", ui_layout_type: "link").save!

    # Create cards
    (0..5).each do |index|
      card = Card.new(organization_id: org.id, card_type: 'media', card_subtype: 'link', title: "card#{index}", message: "card #{index} message")
      card.save!
      content_items << [card, 'card']
    end

    # Create live streams
    (0..5).each do |index|
      video_stream = IrisVideoStream.new(start_time: Time.now.utc, organization_id: org.id, creator_id: members.first.id, status: "past", name: "videostream#{index}")
      video_stream.save!
      content_items << [video_stream, 'video_stream']
    end

    # Create topics
    topics = (0..5).map{|i| Interest.find_or_create_by!(name: "interest #{i}")}

    applicable_offsets = PeriodOffsetCalculator.applicable_offsets(Time.now.utc)

    # User Level Analytics Seed
    (members + [admin]).each_with_index do |member, index|
      # all time data
      UserLevelMetric.new(user_id: member.id, smartbites_score: random_number, smartbites_consumed: random_number, smartbites_created: random_number, time_spent: random_number, period: :all_time, offset: 0).save!

      # Some data in last year
      UserLevelMetric.new(user_id: member.id, smartbites_score: random_number, smartbites_consumed: random_number, smartbites_created: random_number, time_spent: random_number, period: :year, offset: applicable_offsets[:year] - 1).save!

      # Some data every 45 days in the past
      (applicable_offsets[:day]-45..applicable_offsets[:day]).each do |offset|
        UserLevelMetric.new(user_id: member.id, smartbites_score: random_number, smartbites_consumed: random_number, smartbites_created: random_number, time_spent: random_number, period: :day, offset: offset).save!
      end

      # Some data for the last 6 weeks
      (applicable_offsets[:week]-4..applicable_offsets[:week]).each do |offset|
        UserLevelMetric.new(user_id: member.id, smartbites_score: random_number, smartbites_consumed: random_number, smartbites_created: random_number, time_spent: random_number, period: :week, offset: offset).save!
      end

      # Some data for the last 2 months
      (applicable_offsets[:month]-1..applicable_offsets[:month]).each do |offset|
        UserLevelMetric.new(user_id: member.id, smartbites_score: random_number, smartbites_consumed: random_number, smartbites_created: random_number, time_spent: random_number, period: :month, offset: offset).save!
      end
    end

    # Team Level Analytics Seed
    teams.each_with_index do |team, index|
      # Some data in last year
      TeamLevelMetric.new(team_id: team.id, smartbites_consumed: random_number, smartbites_created: random_number, time_spent: random_number, smartbites_liked: random_number, smartbites_comments_count: random_number, period: :year, offset: applicable_offsets[:year] - 1).save!

      # Some data every 45 days in the past
      (applicable_offsets[:day]-45..applicable_offsets[:day]).each do |offset|
        TeamLevelMetric.new(team_id: team.id, smartbites_consumed: random_number, smartbites_created: random_number, time_spent: random_number, smartbites_liked: random_number, smartbites_comments_count: random_number, period: :day, offset: offset).save!
      end

      # Some data for the last 6 weeks
      (applicable_offsets[:week]-4..applicable_offsets[:week]).each do |offset|
        TeamLevelMetric.new(team_id: team.id, smartbites_consumed: random_number, smartbites_created: random_number, time_spent: random_number, smartbites_liked: random_number, smartbites_comments_count: random_number, period: :week, offset: offset).save!
      end

      # Some data for the last 2 months
      (applicable_offsets[:month]-1..applicable_offsets[:month]).each do |offset|
        TeamLevelMetric.new(team_id: team.id, smartbites_consumed: random_number, smartbites_created: random_number, time_spent: random_number, smartbites_liked: random_number, smartbites_comments_count: random_number, period: :month, offset: offset).save!
      end
    end

    # Content Level Analytics Seed
    content_items.each_with_index do |content_item, index|
      # Some data in last year
      ContentLevelMetric.new(content_id: content_item[0].id, content_type: content_item[1], views_count: random_number, comments_count: random_number, likes_count: random_number, period: :year, offset: applicable_offsets[:year] - 1).save!

      # Some data every 45 days in the past
      (applicable_offsets[:day]-45..applicable_offsets[:day]).each do |offset|
        ContentLevelMetric.new(content_id: content_item[0].id, content_type: content_item[1], views_count: random_number, comments_count: random_number, likes_count: random_number, period: :day, offset: offset).save!
      end

      # Some data for the last 6 weeks
      (applicable_offsets[:week]-4..applicable_offsets[:week]).each do |offset|
        ContentLevelMetric.new(content_id: content_item[0].id, content_type: content_item[1], views_count: random_number, comments_count: random_number, likes_count: random_number, period: :week, offset: offset).save!
      end

      # Some data for the last 2 months
      (applicable_offsets[:month]-1..applicable_offsets[:month]).each do |offset|
        ContentLevelMetric.new(content_id: content_item[0].id, content_type: content_item[1], views_count: random_number, comments_count: random_number, likes_count: random_number, period: :month, offset: offset).save!
      end
    end

    # Topic Level Analytics Seed
    topics.each_with_index do |topic, index|
      (members + [admin]).each do |user|
        # All time
        UserTopicLevelMetric.new(tag_id: topic.id, user_id: user.id, smartbites_score: random_number, smartbites_consumed: random_number, smartbites_created: random_number, time_spent: random_number, period: :all_time, offset: 0).save!

        # Some data in last year
        UserTopicLevelMetric.new(tag_id: topic.id, user_id: user.id, smartbites_score: random_number, smartbites_consumed: random_number, smartbites_created: random_number, time_spent: random_number, period: :year, offset: applicable_offsets[:year] - 1).save!

        # Some data every 45 days in the past
        (applicable_offsets[:day]-45..applicable_offsets[:day]).each do |offset|
          UserTopicLevelMetric.new(tag_id: topic.id, user_id: user.id, smartbites_score: random_number,smartbites_consumed: random_number, smartbites_created: random_number, time_spent: random_number, period: :day, offset: offset).save!
        end

        # Some data for the last 6 weeks
        (applicable_offsets[:week]-4..applicable_offsets[:week]).each do |offset|
          UserTopicLevelMetric.new(tag_id: topic.id, user_id: user.id, smartbites_score: random_number,smartbites_consumed: random_number, smartbites_created: random_number, time_spent: random_number, period: :week, offset: offset).save!
        end

        # Some data for the last 2 months
        (applicable_offsets[:month]-1..applicable_offsets[:month]).each do |offset|
          UserTopicLevelMetric.new(tag_id: topic.id, user_id: user.id, smartbites_consumed: random_number, smartbites_score: random_number,smartbites_created: random_number, time_spent: random_number, period: :month, offset: offset).save!
        end
      end
    end

    todays_learning_card_ids = content_items.select{|content_item| content_item[1] == 'card'}.first(5).map{|content_item| content_item[0].id}
    UserDailyLearning.create!(user: admin, processing_state: DailyLearningProcessingStates::COMPLETED, card_ids: todays_learning_card_ids, date: Time.now.utc.to_date.to_s)

    puts "--> Created #{org.host_name} Team with seed data. Log in with email: #{admin.email} and password: 'Password1!'"
  end

  desc 'setup influxdb'
  task setup_influxdb: :environment do
    puts "Logging in with root"
    influxdb = InfluxDB::Client.new(host: Settings.influxdb.host, username: 'root', password: 'root')
    databases = influxdb.list_databases.collect {|d| d['name']}

    # NOTE: Temporarily commented
    if databases.include?(Settings.influxdb.database)
      puts "DATABASE EXISTS"
      # puts "Dropping database"

      # influxdb.delete_database(Settings.influxdb.database)
    else
      puts "Creating influx database"
      influxdb.create_database(Settings.influxdb.database)
    end

    if Rails.env.production?
      puts "Creating a new user"
      if ENV['INFLUXDB_USERNAME'].blank? || ENV['INFLUXDB_PASSWORD'].blank?
        puts "Setup influxdb username and password in ENV"
        return
      end

      influxdb.create_database_user(Settings.influxdb.database, Settings.influxdb.username, Settings.influxdb.password)

      if !ENV['INFLUXDB_ROOT_PASSWORD'].blank?
        puts "Reset root password"
        influxdb.update_user_password('root', ENV['INFLUXDB_ROOT_PASSWORD'])
      else
        puts "Root password reset failed: Set up ENV['INFLUXDB_ROOT_PASSWORD'] and run this task again"
      end

    end
  end
end

namespace :handle_swap do

  desc "test replacing email address"
  task test: :environment do
    email1 = ENV['email1']
    email2 = ENV['email2']
    org_host_name = ENV['org']

    if (email1.blank? || email2.blank? || org_host_name.blank?)
      puts "error: need two emails (test email1=<email> email2=<email> and org=<organization host name>)"
    else
      org = Organization.find_by_host_name(org_host_name)
      u1 = User.where(email:email1, organization_id: org.id).first
      u2 = User.where(email:email2, organization_id: org.id).first

      if org.blank?
        puts "error: cannot find organization where host name = #{org_host_name}"
      elsif (u1.nil?)
        puts "error: user with #{email1} doesn't exist - (#{email2} exists? #{!u2.nil?})"
      elsif (u2.nil?)
        puts "it's safe to change #{email1} (id: #{u1.id}, email: #{u1.email}, handle: #{u1.handle}) to #{email2}"
      else
        puts "::: user1: {email: #{u1.email}, id: #{u1.id}, handle: #{u1.handle}"
        puts "    cards: #{Card.where(author_id: u1.id).count}"
        puts "    followers: #{Follow.where(followable_id: u1.id, followable_type: 'User').count}"
        channels = Channel.all.select{|c| c.card_query.q.split(' ').include?(u1.handle)}
        puts "    channels: #{channels.collect(&:label).join(", ")}"

        puts "::: user2: {email: #{u2.email}, id: #{u2.id}, handle: #{u2.handle}"
        puts "    cards: #{Card.where(author_id: u2.id).count}"
        puts "    followers: #{Follow.where(followable_id: u2.id, followable_type: 'User').count}"
        channels = Channel.all.select{|c| c.card_query.q.split(' ').include?(u2.handle)}
        puts "    channels: #{channels.collect(&:label).join(", ")}"
      end

    end

  end

  desc "reset user emails"
  task move: :environment do
    email1 = ENV['email1']
    email2 = ENV['email2']
    org_host_name = ENV['org']
    if (email1.blank? || email2.blank? || org_host_name.blank?)
      puts "error: need two emails (test email1=<email> email2=<email> and org=<organization host name>)"

    else
      org = Organization.find_by_host_name(org_host_name)
      u1 = User.where(email:email1, organization_id: org.id).first
      u2 = User.where(email:email2, organization_id: org.id).first

      if org.nil?
        puts "error: cannot find organization where host name = #{org_host_name}"
      elsif (u1.nil?)
        puts "error: user with #{email1} doesn't exist - (#{email2} exists? #{!u2.nil?})"
      elsif (u2.nil?)
        puts "setting user1's email (#{u1.email}) to #{email2}"
        u1.update_columns(email: email2)
      else
        u1_handle = u1.handle
        u1.update_columns(handle: "@catfish#{u1.id}_#{u2.id}")
        u2.update_columns(handle: u1_handle)

        cards = Card.where(author_id: u1.id)
        cards_count = cards.count
        cards.each{|c|
          c.update_columns(author_id: u2.id,
                           author_first_name: u2.first_name,
                           author_last_name: u2.last_name,
                           author_picture_url: u2.picture_url)
          c.reindex
        }
        puts "#{cards_count} cards moved from #{email1} to #{email2}"

        streams = VideoStream.where(creator_id: u1.id)
        streams_count = streams.count
        streams.each do |s|
          s.update_columns(creator_id: u2.id)
        end
        puts "#{streams_count} video streams moved from #{email1} to #{email2}"

        followers = Follow.where(followable_id: u1.id, followable_type: 'User')
        followers_count = followers.count
        followers.each{|f|
          f.update_columns(followable_id: u2.id)
        }
        puts "#{followers_count} follows moved from #{email1} to #{email2}"

        puts "verify https://#{org_host_name}.edcast.com/#{u1.handle.sub('@','')}"
        puts "verify https://#{org.host_name}.edcast.com/#{u2.handle.sub('@','')}"
      end

    end

  end

end

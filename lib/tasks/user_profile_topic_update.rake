namespace :update_node_path do
  # This rake will update topic, node_id and taxo_id in user profile
  # org id and taxonomy topic prefix will be taken from environment variable

  desc "This will update topic, node_id in user_profile table"

  task :user_profile_topic_update, [:organization_id, :taxo_prefix, :taxo_root_flag, :old_root] => [:environment] do |t, args|
    @org_id = args[:organization_id]

    # Create token using organization id and sociative secret key
    token = JWT.encode({ organization_id: @org_id }, Settings.sociative.sociative_auth_secret, 'HS256')

    # Get taxonomy topics and create a hash having 'key' as node_path and value as name, domain_id, topic_id
    # taxo_id and domain_name
    taxonomy_topics = Adapter::Sociative.new({ organization_id: @org_id }).get_taxonomy_topics(token)
    # Get user for that organization
    users = User.where(organization_id: @org_id)
    users.each do |user|
      # Get user profile for each user, and find expert topic and learning topic from user profile
      user_profile = UserProfile.find_by(user_id: user.id)
      if user_profile.present?

        # Get user expert topics and learning topics
        user_expert_topic = user_profile.expert_topics
        user_learning_topic = user_profile.learning_topics

        if user_learning_topic.present?

          # For each learning topic add prefix or update root in topic name
          user_learning_topic.each do |learning_topic|
            TopicUpdateService.new("user_profile").convert_topics(taxonomy_topics, learning_topic, args[:taxo_prefix], args[:taxo_root_flag], args[:old_root])
          end
        end
        if user_expert_topic.present?

          # For each expert topic add prefix or update root in topic name
          user_expert_topic.each do |learning_topic|
            TopicUpdateService.new("user_profile").convert_topics(taxonomy_topics, learning_topic, args[:taxo_prefix], args[:taxo_root_flag], args[:old_root])
          end
        end
        user_profile.save
        log.info "Updated topic, node id for user #{user_profile.user_id}"
      end
    end
  end

end
namespace :cards do
  desc "Update existing card message with card title, USE ENV['ORG_ID']"
  task update_card_message_for_ecl_cards: :environment do
    counter = 0
    Card.where(organization_id: ENV.fetch('ORG_ID')).where("cards.ecl_id is not null and cards.author_id is null").find_each do |card|
      counter += 1
      card.message = card.try(:title) || (card.resource && card.resource.title)
      card.title = nil
      card.save

      puts "Updated card id: #{card.id}"
    end
    puts "Total cards updated #{counter}"
  end

  desc "Update broken card images"
  task update_broken_card_images: :environment do
    deleted_files_list=["OMBAbkoHRNOZgjbtqtNJ" ,"23THjA9pQ12UTD16xebg" ,"CLRtQ05bSRibJqeBW3b5" ,"haFE4XJdS12lHPfJEB6D" ,"QljML7ewR6i33wd53auY" ,"VMWUkIhlSm2CqNIgy9rr" ,"TMkibwA1SuaQff9oC9Nd" ,"mPEHzeSiQEal4L9tgF0z" ,"E81ePvsnSdCLteDGegsX" ,"jFRMaY8mQHCgF11TBw1a" ,"zfulrh7ASmOTwLQ57MZ9" ,"z11FIwaBSTa5hGXARGtA" ,"Kvy9uCzTxiNjWBwJwZxg" ,"xJ73PwrSECX9JnfQskyo" ,"rlHLSX8hRVCUZDM6IX00" ,"VIEcaIpOQn28fUQxQ4fR" ,"cPLlqIkDQOG1wjtNn0l8" ,"Mhd5iPePT5yP6xqBwNhK" ,"b7VP0GzmT3O21MMgIQ8E" ,"wT7sKaNR6u3Nb9DXGZ7E" ,"SUQTTtB2TDufLX0jWUCK" ,"HL7nC56IRgac74LliPud" ,"TMRRN8r3QJq5UPydeuGr" ,"gwaCmERDR02ZWssGl5fd" ,"45p2d7jGQPumPkPPfwlL" ,"UziUPRwREurRUZVXyk7o" ,"k4Rk7MYQMiuGJcLNuC9j" ,"tUv06M3nT9OgTTdCQBmV" ,"AjowIG8xSYuBkEKaYr0O" ,"dZhngzexSCGGONvqjUMs" ,"gB10YG6CSJqmnR4GiFyB" ,"iJLpB1YQQhujxDKAiX95" ,"IgaADY7kThGPgkQ1b4jv" ,"CQ6z7o7US2e83B3P3tc0" ,"c6ydaYwTLy74U7FrYuQ4" ,"UwB7APf9Rka3a2bUdhK8" ,"FOAnQvYnRaqHfsnyNVP8" ,"UcG4GldrR4uXVfmtRguA" ,"QzLKC3auROKgnOV7OgEQ" ,"IrfltLRziuV54uJMnqrg" ,"mAjjySCdTA2nlw3SaFj6" ,"qXiyYqjGQOCHWqer6qQg" ,"V7kegwFrTbS2eWli516D" ,"5flTSsTwsnvOJfAqidQI" ,"3TI9IUTkTjC6fudaUbSi" ,"TKuoXzCSw23GNsYy1nxw" ,"3TI9IUTkTjC6fudaUbSi" ,"YnDDIWhMSZ6uuHI2DVNW" ,"YnDDIWhMSZ6uuHI2DVNW" ,"wHDuYWjbR227CyNOF1k4" ,"YZy8DIUbQCSJreo9ZaWs" ,"julVKcVDQ1SkLWsXxagw" ,"Kjb03xHVTvG8pINeiquB" ,"JZLwnZ1WS8epmXX84kDg" ,"4ssDXRxrTqOuQMQExu2n" ,"5fL8OGf7SEqQ1aR1hbWo" ,"xGbsQfxFQCqVSK16xLK5" ,"TRW4zaI6RcC2Y3IW4vTi" ,"Wdnr3ey4Tuug6gXBF1T7" ,"r4e1H6xiSbtpFcNrsghU" ,"LZeZOWKQoiMNTn58Gch0" ,"5Aw6rsxtSQ6EDHjilicO" ,"5whcYXKSAOP9NUXWpN6D" ,"VrYXsGHIRDmv5daLtdZn" ,"shhRTWxBQNu6Mmr9DsPu" ,"AVMwjLDdTkqP6n8tYKq5" ,"3GzEsBAZQKGARJgvORAZ" ,"NqCglKQVTquVTxkYEIMg" ,"pGLXsH8FTMSOnb94NXQZ" ,"MsQVwJnTvm63aSJpZY6H" ,"denByqBTQuugcVLWnKfK" ,"Jgwd2ttDSA6tskcjH3mX" ,"RqbGtk5JTbuvsHUA3oVH" ,"oLuZx7xHQNyPLH8K9OH5" ,"5rhxRCauQBuAbJ42axB5" ,"ThkdJ3lWRDS6yqKqTiVn" ,"JD99QYSTSx6xnG7R0mV8" ,"93MlQNjsTvWkR8HFIJJm" ,"eRIp9MtkR2qYOJIT5ijS" ,"I3FdJVJQTW0pEtdl7C7p" ,"wc3rQRmGRjGMleqQO5Vo" ,"bCp66T0cQaYXGwZfquBu" ,"3r2X9iYuTOxeEHUk3P8I" ,"sbJZdBJT5O3JJSIbs5dJ" ,"7McLeCYRTKM2gCz8i9Cj" ,"Y2kdgeB2Q1SMldfRvRex" ,"sFHJvwZfRzmtca5MoaRH" ,"07HC5nBRS4fmCNVrb5cM" ,"ohozUaKQwOCOzsTXCtpw" ,"zfulrh7ASmOTwLQ57MZ9" ,"hga4uKYRQkCoBjVfJwlO" ,"Kvy9uCzTxiNjWBwJwZxg" ,"xJ73PwrSECX9JnfQskyo" ,"WFbSFImQGKfFy0JXlN6W" ,"kTVWnU35T1qS6W5jgQYS" ,"bUkzJc4SS9asB499NEKV" ,"ICvOAemxQCyP4MddhTv4" ,"jwFmYuPNTdKVGoDWowL1" ,"jxgMkVFIRaiAnagqI50U" ,"Kpg1s6cRyWY2FIU43pmL" ,"CoJCC4QnSkeHJvt2azAv" ,"pMnuyXq1RjqqH8VagiC2" ,"denByqBTQuugcVLWnKfK"]
    card_ids=FileResource.where(filestack_handle: deleted_files_list, attachable_type: "Card").pluck(:attachable_id)
    Card.where(id: card_ids).each do |card|
      image_file=CARD_STOCK_IMAGE[rand(0...90)]
      card.filestack.each do |fs|
        if (deleted_files_list.include? fs["handle"]) || (deleted_files_list.include? fs["url"].sub("https://cdn.filestackcontent.com/", ""))
          fs["url"]=image_file[:url]
          fs["handle"]=image_file[:handle]
          fs["size"]=image_file[:size]
          fs["mimetype"]=image_file[:mimetype]
        end
      end
      card.save if card.changed?
    end
  end
end

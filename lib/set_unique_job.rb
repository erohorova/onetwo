module SetUniqueJob
  def unique
    true
  end
end

########################################################################
# REFERENCE: https://groups.google.com/forum/#!topic/rubyonrails-core/mhD4T90g0G4
# IDEA:
# 1. Do not enqueue a job for an entity if it is already processing.
# 2. If we come across this state, raise an exception clearly.
# 3. Handle that exception here.
########################################################################
module ActiveJob
  module Enqueuing
    def enqueue(options={})
      self.scheduled_at = options[:wait].seconds.from_now.to_f if options[:wait]
      self.scheduled_at = options[:wait_until].to_f if options[:wait_until]
      self.queue_name   = self.class.queue_name_from_part(options[:queue]) if options[:queue]

      begin
        run_callbacks :enqueue do
          if self.scheduled_at
            self.class.queue_adapter.enqueue_at self, self.scheduled_at
          else
            self.class.queue_adapter.enqueue self
          end
        end
        self
      rescue Exception => e
        # TODO: Once everything is confirmed working, remove warning.
        # This will prevent more bugsnags.
        # ... DO SOMETHING ...
      end
    end
  end
end

# frozen_string_literal: true
class InfluxdbRecorder
  module DuplicateChecker

    # We want to prevent duplicate points from being pushed to Influx.
    # To do this, we store a list of the "main attributes" for each event -
    # this is in the config/analytics/unique_event_attributes.yml file.
    # Whenever an event is pushed, we ensure that another event with those
    # same attributes has not been pushed recently.
    UNIQUE_EVENT_ATTRIBUTES = YAML.load File.read Rails.root.join(
      "config", "analytics", "unique_event_attributes.yml"
    )

    # The min seconds that must pass between records with the same attributes
    DUPLICATE_CACHE_TIME_FRAME = 1

    # The amount of seconds before each cache key expires.
    DUPLICATE_CACHE_EXPIRES_IN = 10.minutes.to_i

    # The key used to lock Redis.
    # See http://redis.io/topics/distlock
    REDIS_LOCK_KEY = "influx_duplicate_checker_lock"

    # The timeout for locking Redis (in ms). Prevents deadlock.
    REDIS_LOCK_TIMEOUT = 2.seconds.to_i * 1000

    # Returns false/true, representing if this record is a duplicate.
    # Caches the record's attributes for a short period.
    def is_duplicate?(measurement, record)
      # Turn record into a recursive open struct for easier reading
      record_struct = RecursiveOpenStruct.new record
      # Find the event tag, which is the top-level key for detecting duplicates
      # We special case the user_scores measurement, in which case we use
      # a pseudo event name "user_score"
      # Find the list of attributes which determine 'uniqueness' for this event
      event = if measurement == "user_scores"
        "user_score"
      else
        record_struct.tags&.event
      end
      org_id = record_struct.tags&.org_id
      # If there is no event, abort
      return false unless event
      # If there is no timestamp, abort
      new_record_time = record_struct.timestamp
      return false unless new_record_time
      # Cancel dup-checking entirely if the event isn't found in the config file
      attr_keys = UNIQUE_EVENT_ATTRIBUTES[event.to_s]
      unless attr_keys
        raise "unique attributes for #{event} not registered"
      end
      # Find the values of the record corresponding to the 'uniqueness' attrs
      record_attrs = attr_keys.map do |attr_set|
        record_struct.dig *attr_set
      end
      # Apply a cryptographic hash function to turn the attrs into an int
      attrs_digest = record_attrs.hash
      # build a key which is used to lookup the attr set in the cache
      cache_key = build_cache_key(event, attrs_digest)
      # Synchronize with Redis lock
      begin
        with_redis_lock(org_id, event, attrs_digest) do
          # see if there's a cached record with this attribute set
          existing_record_time = duplicate_cache_get(cache_key)
          has_dup = if existing_record_time
            cutoff = DUPLICATE_CACHE_TIME_FRAME
            if new_record_time.to_i - existing_record_time.to_i <= cutoff
              true
            end
          end
          log_duplicate_found(measurement, record, cache_key) if has_dup
          update_cache_key_unless_dup(has_dup, cache_key, new_record_time)
          return !!has_dup
        end
      rescue Redis::CommandError => e
        Bugsnag.notify e, { severity: 'error' }
        return false
      end
    end

    # Attempts to get a lock on Redis.
    # If one's not available, then it waits via looping.
    # The block is only called when the lock is available.
    # After the block is called, the lock is released.
    def with_redis_lock(org_id, event, attrs_digest, &blk)
      key = "#{REDIS_LOCK_KEY}_#{org_id}_#{event}_#{attrs_digest}"
      attempts = 25
      lock = loop do
        lock_obj = REDIS_LOCK_MANAGER.lock(key, REDIS_LOCK_TIMEOUT)
        break lock_obj if lock_obj
        break false if attempts == 0
        sleep 0.1
        attempts -= 1
      end
      blk.call 
      if lock
        REDIS_LOCK_MANAGER.unlock(lock)
      end
    end

    def log_duplicate_found(measurement, record, cache_key=nil)
      log.debug([
        "DUPLICATE FOUND",
        "measurement=#{measurement}",
        "record=#{record.to_json}",
        ("cache_key=#{cache_key}" if cache_key),
      ].compact.join(", "))
    end

    def update_cache_key_unless_dup(has_dup, cache_key, new_record_time)
      # Don't want to update the timestamp of the cache key if there's a dup.
      # Want to let the old value expire instead.
      #
      # Consider the following example:
      #
      # 1. Hypothetically, we set the window for duplicate detection at 5 sec
      # 2. At second 0 we push a record "A"
      # 3. At second 3 we push a record "B" with the same attr set as "A"
      # 4. At second 6 we push a record "C" with the same attr set as "A"
      #
      # We want only record "B" to be marked as a duplicate.
      # If, at the time that record "B" was pushed, we updated the timestamp
      # at the cache key, then record "C" would also be marked as a duplicate
      # even though it had been more than 5 seconds since a record was pushed.
      return if has_dup
      duplicate_cache_set(cache_key, new_record_time)
    end

    def duplicate_cache_get(key)
      Rails.cache.read(key)
    end

    def duplicate_cache_set(key, val)
      expires_in = DUPLICATE_CACHE_EXPIRES_IN
      Rails.cache.write key, val, expires_in: expires_in
    end

    def build_cache_key(event, attrs_digest)
      [
        "duplicate_checker",
        Rails.env,
        event,
        attrs_digest.to_s
      ].join("/")
    end

  end

end
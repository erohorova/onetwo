module UrlMetadataExtractor

  require 'uri'
  require 'nokogiri'
  require 'open-uri'
  require 'cgi'
  require 'faraday_middleware'

  SET_OF_ENCODING_NAME_LIST = Set.new(Encoding.name_list.map(&:downcase))

  # checks a text for urls and returns the first one if found
  # failing url: http://someurl, http:, http://
  def self.return_url text
    row_url = text[%r{
                  ((http|https):\/{2})?               # zero or one of http/https
                  [a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9] # any single character in the range a-z or A-Z or 0-9 and zero or more of -
                  \.[^\s^<^>]{1,}([a-zA-Z0-9/])                      # 2 or more characters after the dot without space
                  }x]
    # ^[^(http|https):\/{2}] check that there is no http or https before the link
    #[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9] check that at the beginning of the letter / number between can be a hyphen
    #\.[^\s]{2,} check that there are no spaces after the point, and there were more than 2 characters
    consecutive_dot = row_url[%r{\.{2}}x] if row_url.present?
    url = consecutive_dot.present? ? nil : row_url
    regexp = %r{
                (((^[^(http|https):\/{2}]?)          # start of line any character except: http/https
                [a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9] # any single character in the range a-z or A-Z or 0-9 and zero or more of -
                \.[^\s]{1,})([a-zA-Z0-9/]))                        # 2 or more characters after the dot without space
                }x

    if url =~ regexp
      url.gsub!(regexp, 'http://\1')
    else
      url
    end
  end

  # Gets all metadata from a url. Returns hash of the form:
  # hash['title'] => title
  # hash['og:description'] = og description
  def self.get_metadata url
    log.info "Fetching url metadata: #{url.to_s}"
    attribute_map = {}

    uri = URI(url)
    begin
      # allow http <-> https redirections
      connection = Faraday.new do |conn|
        conn.use FaradayMiddleware::FollowRedirects
        conn.adapter Faraday.default_adapter
      end

      response = connection.get do |req|
        req.url uri
        req.options.timeout = 5
      end
      if response.success?

        attribute_map = get_metadata_attributes(response, url)
      elsif Settings.splash_server.enabled
        log.info "Falling back to splash for url: #{url}. Standard extraction returned status: #{response.status}"
        # fallback: scrape html using splash server
        # TODO -> This also needs to be a fallback for when pages are rendered entirely through javascript. In these cases, we get a success response, but get_metadata_attributes will not return meaningful information. How do we reliably identify such cases?
        response = self.get_html_data_using_splash(url)
        if response.success?
          attribute_map = get_metadata_attributes(response, url)
        else
          log.warn "Metadata extraction from splash failed for Url: #{url}. Status: #{response.status}. Headers: #{response.headers}. Body: #{response.body}"
        end
      else
        log.warn "Metadata extraction failed for Url: #{url}. Status: #{response.status}. Headers: #{response.headers}. Body: #{response.body}. Splash is disabled"
      end
    rescue StandardError => e
      log.warn "Metadata extraction failed for url due to Exception: #{e.inspect}"
    end
    attribute_map
  end

  def self.get_metadata_attributes response, url
    attribute_map = {}
    content = response.body
    attribute_map['content_type'] = response.headers['Content-Type']

    #force encode to given encoding
    page_encoding = content.scan(/<meta.*?charset=([^"']+)/)&.first&.first&.downcase
    @format_encoding_name = content.encoding.name.downcase
    if(
      page_encoding && (SET_OF_ENCODING_NAME_LIST.member?(page_encoding)) &&
      content.force_encoding(page_encoding) && content.valid_encoding?
      )
        @format_encoding_name = page_encoding
    end
    content.force_encoding(@format_encoding_name)
    doc = Nokogiri::HTML(content, nil, @format_encoding_name)

    # get head title node
    title_node = doc.xpath('//head/title').first
    attribute_map['title'] = title_node.content if title_node

    # get oembed code
    head_link_nodes = doc.xpath('//head/link')
    oembed_json_node = head_link_nodes.detect { |h| h.attributes['type']&.value == 'application/json+oembed' }
    if oembed_json_node
      oembed_url = oembed_json_node.attributes['href'].value
      prefix_match = oembed_url[/(https:\/\/|http:\/\/)/]
      # fix for this type of oembed_links -> www.foo.com?url=https://foo.com/bar,
      # will add lead prefix if it exist in the link
      oembed_url.prepend(prefix_match) if oembed_url.starts_with?('www') && prefix_match

      #EP-19290: fix for youtube list urls
      if oembed_url.match(/.*youtube.com.*watch.*list.*/)
        oembed_url.gsub(/(.*youtube.com.*watch.*)(list.*)/) {|match| oembed_url = $1 }
      end

      begin
      # handle exceptions to log, on error level
      # redirections or bad connection etc.
        res = Faraday.get oembed_url
        if res.success?
          begin
            js_res = JSON.parse(res.body)
            oembed_type = js_res['type']
            attribute_map['oembed_type'] = oembed_type
            # Update -> Extending to 'rich' fails on wordpress pages
            # that send am embed html that does not display properly. See EP-2675
            if oembed_type == 'video' #|| oembed_type == 'rich'.
              embed_html = js_res['html']
              # this is to make the vide autoplay
              ifr_node = Nokogiri::HTML(embed_html).xpath('//iframe').first
              if ifr_node
                ifr_src = ifr_node.attributes['src'].value
                ifr_src_autoplay = UrlBuilder.build(ifr_src, {autoplay: 1}).to_s
                embed_html.gsub!(ifr_src, ifr_src_autoplay)
              end
              attribute_map['embed_html'] = embed_html
            end
          rescue => e
            log.error "Exception while getting oembed url for url: #{url}. Response body: #{res.body} Exception: #{e.inspect}"
          end
        end
      rescue => e
        log.error "oembed_url extraction failed due to exception: #{e.inspect}"
      end
    end
    # get meta tags
    doc.xpath('//head/meta').each do |node|
      if node.attributes.key?('content')
        node_value = node.attributes['content'].value

        if node.attributes.key?('name') && !node_value.start_with?('{{') # skip handlebars if any
          attribute_map[node.attributes['name'].value] = node_value
        elsif node.attributes.key?('property') && !node_value.start_with?('{{') # skip handlebars if any
          attribute_map[node.attributes['property'].value] ||= node_value
        end
      end
    end

    attribute_map
  end

  def self.get_html_data_using_splash url
    uri = URI(url)
    conn = Faraday.new(url: Settings.splash_server.endpoint)

    # Use a header that spoofes  a browser
    payload = {:url => uri.to_s, :headers => {"User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"}}

    resp = conn.post do |req|
      req.url '/render.html'
      req.body = payload.to_json
      req.headers['Content-Type'] = 'application/json'
    end
  end

  def self.valid_video_url url
    valid_url = url[%r{
            ((http|https):\/{2})                # must be http or https
            [a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9] # any single character in the range a-z or A-Z or 0-9 and zero or more of -
            \.[^\s^<^>]{2,}([a-zA-Z0-9])        # 2 or more characters after the dot without space
            }x]
    valid_url.gsub(/^http:|^https:/, '') unless valid_url.nil?
  end

  # extract video link from embed html and set autoplay option
  def self.get_src_from_embed(embed)
    ifr_node = Nokogiri::HTML(embed).xpath('//iframe').first
    return unless ifr_node
    ifr_src = ifr_node.attributes['src'].value
    params = ifr_src.include?('autoplay=1') ? {} : {autoplay: 1}
    UrlBuilder.build(ifr_src, params).to_s
  end
end

class DelayedPaperclip::UrlGenerator
  def for_with_asset_path(*args)
    ActionController::Base.helpers.asset_path(for_without_asset_path(args))
  end
  alias_method_chain :for, :with_asset_path
end

module Paperclip
  # Crop the original image before passing it to the thumbnail processor
  class Cropper < Thumbnail

    def make
      croppable = @attachment.instance
      file_signature = Digest::MD5.hexdigest("#{croppable.send("#{@attachment.name}_file_name".to_sym)}#{croppable.send("#{@attachment.name}_file_size".to_sym)}")
      @crop_image_data = CropImageData.where(file_signature: file_signature,
                                            croppable: croppable,
                                            crop_column: @attachment.name.to_s
                                            ).last
      if @crop_image_data
        super
      else
        File.new(@file.path) #if there is no crop data then just return the original image
      end
    end

    # Returns the command ImageMagick's +convert+ needs to transform the image
    # into the thumbnail.
    def transformation_command
      width = @crop_image_data.x2 - @crop_image_data.x1
      height = @crop_image_data.y2 - @crop_image_data.y1

      trans = []
      trans << "-coalesce" if animated?
      trans << "-auto-orient" if auto_orient
      trans << "-crop" << %["#{width}x#{height}+#{@crop_image_data.x1}+#{@crop_image_data.y1}"] << "+repage"
      trans << '-layers "optimize"' if animated?
      trans
    end

  end
end

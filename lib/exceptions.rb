module Exceptions
  class IdentityTaken < StandardError; end
  class CourseCreateError < StandardError; end
end

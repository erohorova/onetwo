require 'open-uri'

class BranchMetricUrlGenerator

  def self.generator_url(url, deep_link_id, deep_link_type)
   uri =  URI.parse(url)
   uri.query = [ uri.query, "deep_link_id=#{deep_link_id}", "deep_link_type=#{deep_link_type}"].compact.join('&')

   original_url = uri.to_s
   final_url = Settings.branch_url + "3p?%243p=e_md&%24original_url=" + URI.escape(original_url, Regexp.new("[^#{URI::PATTERN::UNRESERVED}]")) + "&%24desktop_url=" + URI.escape(original_url, Regexp.new("[^#{URI::PATTERN::UNRESERVED}]"))
   return final_url
  end

end

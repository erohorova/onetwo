module Api::V2::VersionsHelper
  def updated_fields_with_signed_filestack_urls(fields, user_id, org, is_native_app)
    fields = fields[0]
    if fields.keys.include?("filestack")
      filestack = fields["filestack"]
      before = filestack[0]
      after = filestack[1]

      if before.present?
        before[0].url = is_native_app ? before[0].url : secured_url(before[0].url, user_id, org)
      end

      if after.present?
        after[0].url = is_native_app ? after[0].url : secured_url(after[0].url, user_id, org)
      end
    end
    [fields]
  end
end

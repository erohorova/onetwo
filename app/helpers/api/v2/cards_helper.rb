module Api::V2::CardsHelper
  def card_type_subtype(card)
    type = card.try(:card_type)
    subtype = card.try(:card_subtype)

    "#{type}_#{subtype}"
  end

  def should_send_default_image?(card)
    return false if card&.filestack.present?
    Card::COMBINATIONS_FOR_STOCK_IMAGE.include?(card_type_subtype(card))
  end

  def default_image(entity, type) # return hash or url
    image_index = (entity&.created_at.to_i) % 100 || 0
    stock_image = CARD_STOCK_IMAGE[image_index] || CARD_STOCK_IMAGE[0]
    type == 'filestack' ? [stock_image] : stock_image[:url]
  end

  # return image hash or default filestack object for project card/pathways/journeys
  def card_filestack(card, user_id, org, is_native_app = false)
    if should_send_default_image?(card)
      fs = default_image(card, 'filestack')[0]
      fs[:url] = is_native_app ? fs[:url] : secured_url(fs[:url], user_id, org)
      [fs]
    else
      card = update_thumbnail(card)
      card.filestack.map do |filestack|
        filestack["url"] = is_native_app ? filestack["url"] : secured_url(filestack["url"], user_id, org) if filestack["url"]
        filestack["thumbnail"] = is_native_app ? filestack["thumbnail"] : secured_url(filestack["thumbnail"], user_id, org) if filestack["thumbnail"]
        filestack
      end
    end
  end

  def update_thumbnail card
    if card.filestack.size == 2 && card.filestack[0][:thumbnail] != card.filestack[1][:url]
      card.filestack[0][:thumbnail] = card.filestack[1][:url]
    end
    card
  end

  # entity can be card or resource object
  def card_resource(entity)
    default_image(entity, 'resource')
  end

  # DESC:
  #   Prevents exposing Filestack's raw URL either by securing it with signature and policy or authenticating with CDN/Lambda.
  #
  #   Serving assets with authentication works only if `ENV['SECURE_AUTHENTICATED_IMAGES']` is enabled. This uses LXP's session.
  #   O/P: https://stark.edcasting.co/uploads/515b4c0451576e5270f7877b29b9018b:5bb3952...
  #
  #   Securing assets with signature and policy is the default implementation though. And a fallback plan.
  #   O/P: https://stark.edcasting.co//security=p:eyJjYWxsIjpbImNvbnZlcnQiXSwiZXhwaXJ5IjoxNTUxMTc3NDYwfQ==,s:e1a25fa1.../:handle
  def secured_url(url, user_id, org)
    if (url && url.include?("cdn.filestackcontent.com"))
      secured_url = Filestack::Security.new(call: ["convert"], org: org).signed_url(url)

      if Settings.serve_authenticated_images.try(&:to_bool)
        code = { filestack_url: secured_url, user_id: user_id }.to_json
        encrypted_data = Aes.safe_encrypt(code, Settings.features.integrations.secret)
        [org.host_url, 'uploads/' + encrypted_data].join('/')
      else
        secured_url
      end
    else
      url
    end
  end

  def secured_video_stream(path, user_id, org)
    if Settings.serve_authenticated_images.try(&:to_bool) && path.include?("video-streams-output-v2/")
      code = { video_path: path, user_id: user_id }.to_json
      encrypted_data = Aes.safe_encrypt(code, Settings.features.integrations.secret)
      [org.host_url, 'past_stream/' + encrypted_data].join('/')
    else
      "https://#{Settings.aws_cdn_host}/#{path}"
    end
  end
end

module Api::V2::UsersHelper
  def get_http_url(url)
    url.present? ? url.gsub('https', 'http') : url
  end
end

module Api::V2::BaseHelper
  V2_DEEP_LINK_ENABLED = [Card, Assignment, Comment, Team, Organization, Channel, ChannelsCurator, ChannelsUser]
  def get_app_deep_link_id_and_type(model_object)
    if model_object.class.in?(V2_DEEP_LINK_ENABLED)
      deep_link_id, deep_link_type = model_object.try(:get_app_deep_link_id_and_type_v2) || [nil, nil]
    else #TODO TECHDEBT: REMOVE THIS AFTER WE SUNSET V1
      deep_link_id, deep_link_type = model_object.try(:get_app_deep_link_id_and_type) || [nil, nil]
    end
    [deep_link_id, deep_link_type]
  end

  #TODO - COPY OF Notification#app_deep_link_id_and_type
  def notification_app_deep_link_id_and_type(notification)
    _id, _type = nil, nil
    if notification.notification_type == 'new_follow'
      # dummy id. There isn't really a deep link business object corresponding to a follow notification
      _id, _type = 0, 'follow'
      # NOTE: everything below is notifiable_type, only the top one is notification_type
    elsif notification.notification_type == 'following'
      _id, _type = notifiable_id, 'channel'
    elsif notification.notifiable_type == 'Clc'
      _id, _type = notification.user_id, 'user'
    elsif notification.notifiable_type == 'TopicRequest'
      _id, _type = notification.notifiable_id, 'topic_request'
    elsif notification.notifiable_type == 'ChannelsCard'
       _id, _type = notification.app_deep_link_id, notification.app_deep_link_type
    else
      # Ex. notifiable = Comment, Post, Card. returns [id, 'comment/post/card']
      _id, _type = get_app_deep_link_id_and_type(notification.notifiable)
    end
    return _id, _type
  end

  def check_status_external_link(link)
    response = Faraday.get link
    response.status
  end

  def humanized_action(event)
    case event
    when 'create'
      'added'
    when 'update'
      "updated"
    when 'destroy'
      'removed'
    end
  end
end

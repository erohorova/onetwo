module ClcsHelper

  def get_earned_badge(clc,user)
    UserBadge.where(user_id: user.id, badging_id: clc.clc_badgings.pluck(:id)).order("badging_id DESC").first
  end

end
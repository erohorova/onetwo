module ApplicationHelper
  include Tracking::Visit

  HOST_PROVIDERS = ['org_oauth', 'saml', 'lxp_oauth']

  def show_dismiss_content_button?
    (@on_feed_page && (Settings.features.dismiss_content || !params[:fdc].nil?))
  end

  def webpack_asset_url(bundle, type='js')
    webpack_assets = Rails.cache.fetch('webpack-assets-manifest', expires_in: 10.minutes) do
      path = Rails.root.join('public', 'assets', 'webpack-assets.json')
      file = File.read(path)
      json = JSON.parse(file)
      json
    end
    webpack_assets[bundle]['js']
  end

  def is_leader?(item, user)
    if user && item.is_a?(Organization)
      item.has_admin_access?(user)
    else
      false
    end
  end

  def is_edcast_admin?
    @current_org.admins.include?(current_user) && current_user.email == Settings.self_serve_mailing_list
  end

  def home_page_banner
    (@current_org == Organization.default_org) ? image_path('homepage_banner.jpg') : @current_org.photo
  end

  def home_page_pitch
    (@current_org == Organization.default_org) ? "Our Mission: Empowering Individuals and Organizations to Build their Knowledge Network" : @current_org.description
  end

  # Render a partial into a script tag so Angular
  # sticks it into $templateCache
  def load_ng_template(partial)
    template_name = partial.sub("#{Rails.root}/app/assets/templates/", '').sub('.slim', '')
    template_name = asset_path(template_name).sub('http:', '').sub('https:', '')
    content_tag :script, type: 'text/ng-template', id: template_name do
      render file: partial
    end
  end
  # See more at: http://www.localytics.com/blog/2014/a-year-on-angular-on-rails-a-retrospective/#sthash.iPqDVWA5.dpuf

  def page_title(content: , user: nil)
    user && collection = [user.name.gsub(/(on|On) (EdCast|Edcast)/, ''), 'on EdCast:']
    ((collection ||= []) + [content.truncate(63)]).join(' ').squish
  end

  def hyperlink_url(card)
    case card['card_template']
    when 'link'
      return card['content'] && card['content']['link_url']
    when 'video'
      return card['content'] && card['content']['video_url']
    end
  end

  def get_unseen_notifications_count
    return 0 if current_user.nil?
    return @unseen_count unless @unseen_count.nil?
    query = "widget_deep_link_id IS NULL AND app_deep_link_id IS NOT NULL"

    seen_upto = UserNotificationRead.where(user: current_user).first

    if seen_upto
      return current_user.unseen_notifications.where(query).where("created_at >= (:seen_upto)", seen_upto: seen_upto.seen_at).count
    else
      return current_user.unseen_notifications.where(query).count
    end
  end

  def profile_page_url(user)
    if user.handle.present?
      public_user_page_url(handle: user.handle_no_at, host: user.fetch_organization.host)
    else
      "#"
    end
  end

  def handle_no_at(handle)
    handle
  end

  def camo_filter(url)
    CamoFilter.filter(url)
  end

  def get_og_image_url(url)
    begin
      uri = URI.parse(url)
      url.match(/http/) || uri.scheme = Settings.platform_force_ssl ? 'https' : 'http'
      return uri.to_s
    rescue => e
      log.warn "og image extraction failed for url: #{url}. Exception: #{e.inspect}"
      return nil
    end
  end

  def get_local_copy(url:, size: :original)
    res = Resource.where(image_url: url).last

    if res.present?
      fr = res.file_resources.last
      return fr.attachment.url(size) if fr.present?
    end

    url
  end

  def encrypted_host
    ActiveSupport::MessageEncryptor.
      new(Edcast::Application.config.secret_key_base).
      encrypt_and_sign(request.host_with_port)
  end

  def social_connect_url provider
    host = nil
    if HOST_PROVIDERS.include?(provider)
      host = "#{request.protocol}#{request.host_with_port}"
    else
      host = "#{request.protocol}#{Settings.platform_host}"
    end

    "#{host}#{request.port_string}/auth/#{provider}?current_host=#{encrypted_host}"
  end

  def sso_url(org_sso)
    provider    = org_sso.sso.name
    connect_url = social_connect_url(provider)

    if provider == 'lxp_oauth'
      token = JWT.encode(org_sso.id.to_s, Settings.features.integrations.secret, 'HS256')
      connect_url + '&connector=' + token + '&provider=' + org_sso.provider_name.to_s
    else
      connect_url
    end
  end

  alias_method :http_url, :get_og_image_url

  def external_courses_url(user_id)
    "#{Settings.external_courses_url}/?user_id=#{user_id}"
  end

  def get_state_for_nav(path)
    return 'active' if request.url == path
    ''
  end

  def get_co_branding_image
     @current_org.co_branding_logo.url if @current_org && @current_org.co_branding_logo.exists?
  end

  def is_search_page?
    false
  end

  def team_role(as_type)
    key = as_type.to_s
    {
        'admin' => 'leader',
        'member' => 'member',
    }[key] || key
  end

  def right_rail_widgets(extra_widgets: nil)
    return [] if @right_rail_disabled
    widgets = {}
    unless @default_widgets_disabled
      widgets.merge!('PeopleWidget' => {"title" => "Follow Team Members", "path" => organization_home_path(tab: 'members')},
        'ChannelsWidget' => {"title" => "Featured Channels", "path" => organization_home_path(tab: 'channels')},
        'CoursesPromotionWidget' => {"title" => "New Courses You Might Like", "path" => organization_home_path(tab: 'courses')}
      )
      if @current_org && @current_org.host == Organization.default_org.host
        widgets = {'TeamsUpsellBanner' => {"path" => "#"}}.merge!(widgets)
      end
    end
    widgets = extra_widgets.merge(widgets) if extra_widgets
    widgets
  end

  def org_sso_configs
    columns = OrgSso.columns.map(&:name).select { |column| column.start_with?('oauth', 'saml')}.map(&:to_sym)
    columns << 'label_name'
  end

  # overriding ActionView::Helpers::SanitizeHelper#strip_tags
  #  undesired escaping behavior was added in Rails 4.2
  #  see https://github.com/rails/rails-html-sanitizer/issues/31
  def strip_tags(string)
    str = Rails::Html::WhiteListSanitizer.new.sanitize(string, tags: [], encode_special_chars: false)
    Nokogiri::HTML.fragment(str).text
  end

  def simplify_encoded_string(text)
    markdown = Redcarpet::Markdown.new(Redcarpet::Render::StripDown)
    removed_markdown_text = markdown.render(text)
    sanitized_text = HTMLEntities.new.decode(ActionView::Base.full_sanitizer.sanitize(removed_markdown_text))
    sanitized_text.gsub("\n","").gsub("*","").gsub("~","").gsub("_", "").html_safe
  end

  def organization_footer_options(org = nil)
    options = {
      privacy_policy:     'http://www.edcast.com/corp/privacy-policy/',
      promotions_enabled: true,
      terms_of_service:   'http://www.edcast.com/corp/terms-of-service/'
    }

    return options unless org

    custom_options = org.custom_footer_options.to_hash.symbolize_keys!

    [:privacy_policy, :terms_of_service].each do |attribute|
      options[attribute] = custom_options[attribute] if custom_options[attribute].present?
    end

    options[:promotions_enabled] = custom_options[:promotions].to_s.try(:to_bool) unless custom_options[:promotions].to_s.blank?

    options
  end

  def configured_ssos(organization)
    # Giving only those ssos in response which are is_enabled and is_visible
    org_ssos = organization.org_ssos.active.visible

    # Only one will be active at a time either legacy or okta
    if organization.okta_enabled?
      org_ssos = org_ssos.revised_lxp_oauth
    else
      org_ssos = org_ssos.legacy_ssos
    end

    org_ssos.includes(:sso)
  end
end

module CardsHelper
  def extract_files(card)
    files = []
    begin
      files = JSON.parse(card['content']['files_info'])
    rescue => e
      log.warn "Parsing file data failed for card: #{card['id']}. Content: #{card['content']}. Error: #{e.message}"
    end
    files
  end

  def render_tag_list(tags = [])
    any_tags = tags.present?
    prompt   = content_tag :div, class: "tags-pitch #{any_tags && 'hide' }" do
      content_tag :p, class: 'small-font' do
        prompt  = "Use hashtags so that other EdCasters can discover your card easily."
        prompt += " Just use the “#” symbol before some keywords in your card.  For example: #education #technology"
      end
    end

    list   = content_tag :div, class: "tags-list #{any_tags || 'hide'}" do
      text = content_tag :div, class: 'small-font', style: 'font-weight: 400' do
        "Tags You've Used"
      end
      text + content_tag(:p, tags.join(' '), class: 'small-font tags')
    end
    (prompt + list).html_safe
  end

  def updated_timestamp(card)
    timestamp = card['rationale'] && card['rationale']['timestamp']
    return card['created_at'] unless timestamp
    return timestamp.to_s
  end

  def card_display_type(card)
    if card['card_type'] == 'media' && card['card_template'] == 'text'
      return 'question','text-card'
    elsif card['card_type'] == 'media' && card['card_template'] == 'link'
      return 'article','link-card'
    elsif card['card_type'] == 'media' && card['card_template'] == 'video'
      return 'video','video-card'
    elsif card['card_type'] == 'media' && card['card_template'] == 'image'
      return 'photo','image-card'
    elsif card['card_type'] == 'poll' && card['card_template'].in?(['basic', 'link', 'image', 'video'])
      return 'poll','poll-card'
    elsif card['card_type'] == 'pack' && card['card_template'] == 'simple'
      return 'pathway',  'card-pack'
    elsif card['card_type'] == Card::TYPE_PACK && card['state'] == Card::STATE_DRAFT && card['card_template'] == 'simple'
      return 'draft pathway', 'card-pack'
    else
      return 'link','link-card'
    end
  end

  def video_stream_display_image(stream)
    #else case handle ElasticSearch results for video-streams
    if stream.is_a?(VideoStream)
        return stream.image(:medium)
    else
      return stream.image
    end
  end

  def display_image(card)
    if card.is_a?(VideoStream)
      if card.image.present?
        return card.image(:large)
      else
        return image_url('card_default_image_min.jpg')
      end
    end
    if card['medium_image_url']
      return card['medium_image_url']
    end
    #VideoStream ElasticSearch returns hashes not objects
    case card['card_type']
      when 'media'
        case card['card_template']
          when 'link'
            return camo_filter((card['content'] && card['content']['link_image_url'])) || image_url('card_default_image_min.jpg')
          when 'video'
            return camo_filter((card['content'] && card['content']['video_image_url'])) || image_url('card_default_image_min.jpg')
          when 'image'
            return camo_filter((card['content'] && card['content']['image_url'])) || image_url('card_default_image_min.jpg')
          else
            return image_url('card_default_image_min.jpg')
        end
      when 'pack_draft'
        if card['content'] && !card['content']['image_url'].blank?
          return camo_filter(card['content']['image_url'])
        else
          return image_url('card_default_image_min.jpg')
        end
      when 'pack'
        if card['content'] && !card['content']['image_url'].blank?
          return camo_filter(card['content']['image_url'])
        else
          return image_url('card_default_image_min.jpg')
        end
      else
        return image_url('card_default_image_min.jpg')
    end
  end

  def channel_options(channels)
    channels.map{|ch| [ch.label, ch.id]}
  end

  def err_image_url
    asset_path('anonymous-user.png')
  end

  def user_image_avatar(src, options)
    options.merge!(onerror: "this.src='"+err_image_url+"'")
    image_tag src, options
  end

  def answered?(poll_card_id, user)
    user && QuizQuestionAttempt.has_user_attempted_quiz_question_card?(poll_card_id, user.id)
  end

  def liked?(card, user)
    user && card['liked_by'].map(&:id).include?(user.id)
  end

  def calculate_percentages(count, attemptCount)
    count * 100 / attemptCount
  end

  def course_image_url(course)
    course.image || image_url("card_default_image_min.jpg")
  end

  def polymorphic_card_url(entity_type, entity_id)
    case entity_type
    when 'Card'
      assignable = entity_type.constantize.find(entity_id)
      if assignable.card_type == 'pack'
        ClientOnlyRoutes.collection_path(id: assignable.slug)
      # Support backward compatibility for video stream card type
      # Commenting this out because this does not work throwing `This content does not exist or has been deleted`
      # elsif assignable.card_type == 'video_stream'
      #   video_stream_path(id: assignable.video_stream.slug)
      else
        ClientOnlyRoutes.show_insight_path(id: assignable.slug)
      end
    when 'VideoStream'
      video_stream = VideoStream.find_by(id: entity_id)
      video_stream_path(id: video_stream.slug)
    end
  end

  def formatted_duration_metadata(card)
    if card.time_to_read
      {
        "calculated_duration" => card.time_to_read,
        "calculated_duration_display" => TimeToWord.convert(time_in_sec: card.time_to_read,duration_format: card.organization.get_settings_for('duration_format').present?)
      }
    end
  end
end

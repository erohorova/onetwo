module NotificationsHelper
  def following_message(notification)
    case notification.causal_users.size
      when 1 then "is following you"
      when 2 then "and #{user_link(notification.causal_users.last)} are following you"
      else "and #{notification.causal_users.size - 1} other people are following you"
    end
  end

  def post_message(notification)
    item = Post.find_by(id: notification.notifiable_id)
    first_user = notification.causal_user
    case notification.notification_type
      when 'new_post_upvote' then causify_messages('liked your post -', notification)
      when 'new_announcement' then "#{user_link(first_user)} made a new announcement -"
      when 'new_comment' then causify_messages('commented on post -', notification)
      when 'new_approval' then "#{user_link(first_user)} approved your post -"
    end
  end

  def comment_message(notification)
    case notification.notification_type
      when 'new_comment_upvote' then causify_messages('liked your comment on -', notification)
      when 'new_approval'then "#{user_link(notification.causal_user)} approved your comment on -"
      when 'mention_in_comment'then causify_messages("mentioned you in a comment on #{notification.resolve_mention_in_comment_message} -", notification)
    end
  end

  def card_message(notification)
    case notification.notification_type
      when 'new_comment' then causify_messages('commented on the insight -', notification)
      when 'new_card_upvote' then causify_messages('liked your insight -', notification)
      when 'new_poll' then causify_messages('shared a poll with you -', notification)
      when 'poll_answer' then causify_messages('answered your poll -', notification)
      when 'mention_in_card'then causify_messages('mentioned you in the insight -', notification)
      when 'assigned' then causify_messages('assigned you the smartcard -', notification)
    end
  end

  def video_stream_message(notification)
    n_message = ''
    message = ''
    case notification.notification_type
    when 'stream_scheduled'
      n_message = "scheduled a live stream -"
    when 'stream_started'
      if notification.notifiable && notification.notifiable.status == 'live'
        n_message = "is now live -"
      else
        n_message = "was live -"
      end
    when 'scheduled_stream_reminder'
      message = "Time to start your stream -"
    end
    n_message.blank? ? message : "#{user_link(notification.causal_user)} #{n_message}"
  end

  def channel_message(notification)
     case notification.notification_type
      when 'following' then "You are following -"
      when 'added_author' then causify_messages('added you as a collaborator of the channel -', notification)
      when 'added_follower' then causify_messages('added you as a follower of the channel -', notification)
    end
  end

  def assignment_message(notification)
    message = case notification.notification_type
    when 'assigned'
      causify_messages("assigned you the #{notification.notifiable.assignable.card_type.eql?('pack') ? 'Pathway' : 'SmartCard'}", notification)
    when 'assignment_due_soon'
      "#{notification.get_assingment_due_message}:"
    when 'assignment_notice'
      "#{custom_message}"
    end
    message
  end

  def notification_message(notification)
    if notification.notification_type == 'new_follow'
      "#{user_link(notification.causal_user)} #{following_message(notification)}".html_safe()
    elsif notification.notification_type == 'team_card_share'
      card_name = notification.notifiable.message.presence || "SmartCard"
      group_name = notification.custom_message.presence || ""
      causal_user_name = notification.causal_users.first.name.presence || ""
      message = "#{causal_user_name} shared #{card_name} with #{group_name} group."
      "#{message}".html_safe
    elsif notification.notification_type == 'user_card_share'
      card_name = notification.notifiable.message.presence || "SmartCard"
      causal_user_name = notification.causal_users&.first&.name || "User"
      message = "#{causal_user_name} shared #{card_name} with you."
      "#{message}".html_safe
    else
      message = ''
      case notification.notifiable_type
      when 'Post'
        message = post_message(notification)
      when 'Comment'
        message = comment_message(notification)
      when 'Card'
        message = card_message(notification)
      when 'VideoStream'
        message = video_stream_message(notification)
      when 'Channel'
        message = channel_message(notification)
      when 'Assignment'
        message = assignment_message(notification)
      end
      "#{message} #{snippet_link(notification)}".html_safe()
    end
  end

  def snippet_link(notification)
    snippet = nil
    if @preloaded_snippets.nil?
      snippet = notification.snippet
    elsif notification.notification_type == "mention_in_comment"
      snippet = notification.notifiable.commentable.snippet
    else
      snippet = @preloaded_snippets[notification.id]
    end
    link_to snippet, target_path(notification), class: "snippet" if snippet
  end

  def user_link(user)
    handle_no_at = user.handle_no_at
    handle_no_at ? (link_to user.name, public_user_page_path(handle: handle_no_at), class: "noti_user_link") : user.name
  end

  def action_link(notification)
    action_params = case notification.notification_type
    when "stream_started"
      ["Watch", video_stream_path(notification.app_deep_link_id)]
    when "new_poll"
      ["Answer Poll", show_insight_path(notification.app_deep_link_id)]
    when "poll_answer"
      ["View Poll", show_insight_path(notification.app_deep_link_id)]
    when "mention_in_comment", "mention_in_card"
      ["View Post", target_path(notification)]
    when "following", "added_follower", "added_author"
      ["View Channel", learn_path(notification.app_deep_link_id)]
    when "assignment_due_soon"
      ["View Assignment", target_path(notification)]
    else
      handle_no_at = handle_no_at(notification.causal_user.handle)
      if handle_no_at
        ["View Profile", "/#{handle_no_at(notification.causal_user.handle)}"]
      else
        nil
      end
    end

    action_params ? (link_to action_params[0], action_params[1], class: "view_link") : nil
  end

  def causify_messages(_message, notification)
    causal_users = notification.causal_users
    first_user = causal_users.first
    msg = case causal_users.size
      when 1 then "#{_message}"
      when 2 then "and 1 other person #{_message}"
      else "and #{causal_users.size - 1} other people #{_message}"
      end
    "#{user_link(first_user)} #{msg}"
  end

  def target_path(notification)
    case notification.app_deep_link_type
      when 'card' then ClientOnlyRoutes.show_insight_path(id: notification.app_deep_link_id)
      when 'collection' then ClientOnlyRoutes.collection_path(id: notification.app_deep_link_id)
      when 'video_stream' then video_stream_path(notification.app_deep_link_id)
      when 'post' then post_path(notification.app_deep_link_id)
      when 'channel' then learn_path(notification.app_deep_link_id)
      else "#"
    end
  end

  def notification_image(notification)
    if(notification.causal_user.nil?)
      # using try so that it doesn't error out in the future, eg:- for new-follow the notifiable is nil. Worst case it will default to the default card image.
      mobile_photo = notification.try(:notifiable).try(:mobile_photo) || image_url('card_default_image_min.jpg')
      [ mobile_photo, learn_path(notification.app_deep_link_id)]
    else
      causal_user = notification.causal_user
      [causal_user.photo, handle_no_at(causal_user.handle)]
    end
  end
end

module ChannelsHelper
  def content_sources_frequencies
    Channel::FREQUENCIES.collect { |v| [v.titleize, v] }
  end
  #Channel or Organization
  def profile_photo(object)
    object.mobile_photo(:small) || object.photo
  end

  def simple_form_opts(channel)
    if channel.new_record?
      {url: channels_path, method: :post}
    else
      {url: channel_path(id: channel.slug), method: :patch}
    end
  end

  def is_discussion_request?
    request.path == channel_forum_path(@channel)
  end

end

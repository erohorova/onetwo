module VideoStreamsHelper
  def description_meta_tag(stream)

    # Return title if stream creator is nil
    if stream.creator.nil?
      return stream.name
    end

    start_time = stream.start_time.in_time_zone("Pacific Time (US & Canada)").strftime('%d %b %y %I:%m %p')
    user_name  = stream.creator.name

    case stream.status
    when 'live'
      "#{user_name} is live streaming on EdCast"
    when 'past'
      "#{user_name} was live streaming on EdCast. Follow them to catch their next live stream"
    when 'upcoming'
      "#{user_name} will live stream on EdCast at #{start_time} PST"
    end
  end

  def voted_by?(video_stream,user)
    if video_stream.is_a?(VideoStream)
      video_stream.voted_by?(user)
    else
      begin
        VideoStream.find(video_stream.id).voted_by?(user)
      rescue ActiveRecord::RecordNotFound
        log.error "VideoStream #{video_stream.id} not found in database but present in Elasticsearch"
        return false
      end
    end
  end

  def stream_end_time(stream)
    stream.start_time.is_a?(String) ? Time.parse(stream.start_time) : stream.start_time + 15.minutes
  end

  def streams_add_to_pack_enabled?
    Settings.features.add_to_pack_stream.enabled
  end

  def theatre_mode_enabled?
    Settings.features.theatre_view.enabled
  end

  #TODO: depricate this. use the jbuilder version
  def data_for_video_stream_container
    ret = {}
    ret['user'] = @user.nil? ? nil : @user.capsule_data
    ret['comments'] = @comments.map{|c| comment_data_from_search(c).merge({'up_voted' => (@votes_by_user && @votes_by_user.include?(c['id'])) })}
    if (@video_stream.status == 'live')
      ret['viewers'] = []
    else
      ret['viewers'] = @video_stream.watchers.map{|w| w.mini_profile_data}
      ret['anon_watchers_count'] = @video_stream.anon_watchers_count
    end
    if (params[:action] == 'theatre_mode')
      ret['layout'] = 'theatre'
    end
    ret['recording'] = @recording.nil? ? nil : {'id' => @recording.id, 'mp4_location' => "https://#{Settings.aws_cdn_host}/#{@recording.mp4_location}", 'url' => @recording.location, 'hls_location' => "https://#{Settings.aws_cdn_host}/#{@recording.hls_location}"}
    ret['video_stream'] = @video_stream.as_json(only: [:id, :slug, :uuid, :name, :status, :votes_count, :comments_count, :height, :width])
    ret['video_stream']['creator'] = @video_stream.creator.try(:capsule_data)
    ret['video_stream']['image_url'] = @video_stream.image || image_url('card_default_image_min.jpg')
    ret['video_stream']['playback_url'] = @video_stream.playback_url
    ret['video_stream']['embed_playback_url'] = @video_stream.embed_playback_url if @video_stream.is_a?(IrisVideoStream)
    ret['video_stream']['rtmp_playback_url'] = @video_stream.rtmp_playback_url
    ret['video_stream']['start_time'] = @video_stream.start_time.strftime('%FT%T.%LZ')
    ret['video_stream']['tags'] = @video_stream.tags.map{|t| {'id' => t.id, 'name' => t.name}}
    ret['video_stream']['user_owns_stream'] = (@user && @user == @video_stream.creator)
    ret['video_stream']['upvoted'] = @user && @video_stream.voted_by?(@user)
    ret['video_stream']['share_url'] = video_stream_url(@video_stream.slug)
    ret['video_stream']['has_uploaded_image'] = @video_stream.file_resource.present?
    ret['video_stream']['channels'] = @video_stream.channels.map{|ch| {'id' => ch.id, 'label' => ch.label}}
    ret['video_stream']['provider'] = @video_stream.provider_name
    ret['video_stream']['cluster_api_endpoint'] = @video_stream.cluster_api_endpoint if @video_stream.is_a?(Red5ProVideoStream)
    ret
  end
end

module FollowsHelper

  # in opts supply either :followable, or :followable_id and :followable_type
  def configure_follow_link(opts: {}, show_image: true)
    if opts[:followable]
      followable_id = opts[:followable].id
      followable_type = opts[:followable].class.name
    else
      followable_id = opts[:followable_id]
      followable_type = opts[:followable_type]
    end
    following = opts[:user] && opts[:user].following_id_and_type?(id: followable_id, type: followable_type)

    follow_link_text = following ? 'Following' : 'Follow'

    opts[:css_class] = '' if opts[:css_class].nil?
    opts[:css_class] += ' selected' if following

    followable_type.downcase!
    link_params      = {"#{followable_type}_id" => followable_id}
    link_params.merge!(mode: opts[:mode]) if opts.key?(:mode)
    request_link     = follows_path link_params

    content_tag :a,
      class:                 opts[:css_class],
      href:                  'javascript:false;',
        'data-ec' => followable_type,
        'data-ea' => (following ? 'unfollow.attempt' : 'follow.attempt'),
        'data-el' => 'follow.button',
      'data-following'    => following,
      'data-followable' => '',
      'data-request-link' => request_link do
        image = content_tag :span, nil, class: 'follow-image' if show_image
        text  = content_tag :span, class: show_image ? 'follow-text' : 'follow-text no-image' do
          follow_link_text
        end
        show_image ? (image + text) : text
    end
  end
end

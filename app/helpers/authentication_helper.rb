require 'jwt_auth'
module AuthenticationHelper
  def current_org
    @current_org
  end

  def source_id
    @source_id
  end

  def set_current_org_using_host_name
    @current_org = Organization.find_by_request_host(request.host)
  end

  def authenticate_user_from_token!
    replica_database do
      if claims && user = User.find(claims['user_id'])
        check_user_validity(user)
        add_to_response_headers(user) if get_platform == 'web' && request.headers['REFRESH-TOKEN']
      else
        render_unauthorized('JWTToken is expired or invalid') and return
      end
    end
  end

  def authenticate_user_from_token_for_lms!
    replica_database do
      if claims && user = User.find(claims['user_id'])
        check_user_validity(user,is_lms: true)
        add_to_response_headers(user) if get_platform == 'web' && request.headers['REFRESH-TOKEN']
        set_request_store
      else
        render_unauthorized('JWTToken is expired or invalid') and return
      end
    end
  end
  alias_method :authenticate_superadmin_from_token!, :authenticate_user_from_token_for_lms!

  def authenticate_user_using_welcome_token!
    replica_database do
      raw_token = params[:welcome_token]
      encrypted_token = Devise.token_generator.digest(self, :welcome_token, raw_token)
      user = User.find_by(welcome_token: encrypted_token)
      if user
        check_user_validity(user)
        add_to_response_headers(user) if get_platform == 'web' && request.headers['REFRESH-TOKEN']
      else
        render_unauthorized('Welcome token is expired or invalid') and return
      end
    end
  end

  def require_superadmin
    return render_unauthorized unless current_user.is_edcast_dot_com_user?
  end

  def claims
    JwtAuth.decode(token)
  end

  def token
    request.headers['X-API-TOKEN'] || params[:token]
  end

  def is_using_jwt_token?
    !request.headers['X-Edcast-JWT'].nil? || !params[:token].nil?
  end

  def check_user_validity(user, is_lms: false)
    if get_platform == 'web' && user.sign_out_at && !is_lms
      render_unauthorized('JWTToken is expired or invalid') and return
    end
    organization = Organization.find_by_request_host(request.host)
    unless (organization == user.organization) && user.active_for_authentication?
      Octopus.using(:master) { sign_out user }
      return render_unauthorized('JWTToken is expired or invalid')
    else
      @current_user = user
      @current_org = user.organization
    end
  end

  def require_organization_admin
    unless current_org.has_admin_access?(current_user)
      render_unauthorized('unauthorized action')
    end
  end

  def require_org_or_team_admin
    is_team_admin = Team.find_by(id: params[:team_id])&.is_admin?(current_user)
    unless current_org.has_admin_access?(current_user) || is_team_admin
      render_unauthorized('unauthorized action')
    end
  end

  def authenticate_from_provider
    if request.headers['X-API-KEY'].blank?
      return render_unprocessable_entity("API KEY is not present.")
    end

    @current_org = get_source_organization
    if @current_org.present? && request.headers['X-EMAIL'].present?
      check_valid_user(@current_org)
    else
      return render_bad_request("Invalid request")
    end
  end

  def check_valid_user(org)
    unless @current_user = org.users.find_by(email: request.headers['X-EMAIL'])
      return render_unauthorized
    end
  end

  def get_source_organization
    source_credential = SourcesCredential.find_by(shared_secret: request.headers['X-API-KEY'])
    if source_credential.present?
      @source_id = source_credential.source_id
      source_credential.organization
    end
  end

  def check_exclude_group_permission(permission_name)
    if current_user.exclude_group_permission?(permission_name)
      message = 'You are unauthorized to view this page. Please contact your organization administrator'
      render_unauthorized(message)
    end
  end

  # Error code renders
  def render_unauthorized(message = nil)
    render_with_status_and_message(message, status: :unauthorized)
  end

  def render_unprocessable_entity(message = nil)
    render_with_status_and_message(message, status: :unprocessable_entity)
  end

  def render_bad_request(message = nil)
    render_with_status_and_message(message, status: :bad_request)
  end

  def render_not_found(message = nil)
    render_with_status_and_message(message, status: :not_found)
  end

  def render_with_status_and_message(message = nil, status:)
    if message
      render json: {message: message}, status: status
    else
      render json: :no_content, status: status
    end
  end

  def require_array_param_values(params_keys: [])
    params_keys.each do |key|
      unless params[key].is_a?(Array)
        render_unprocessable_entity('key param is not an array') and return
      end

      params_key_values = params[key].reject(&:blank?)
      if params_key_values.blank?
        render_unprocessable_entity('Missing required params') and return
      end
    end
  end

  def add_to_response_headers(user)
    response.headers['X_API_TOKEN'] = JwtAuth.renew(user)
    response.headers['EXPIRE_AT'] = JwtAuth.decode(response.headers['X_API_TOKEN'])['exp']
  end
end

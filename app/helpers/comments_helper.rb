module CommentsHelper
  def comment_data_from_search comment
    comment.slice('id', 'message', 'resource', 'tags', 'file_resources', 'user', 'votes_count', 'created_at', 'updated_at').merge({'mentions' => comment['mentioned_users']})
  end
end

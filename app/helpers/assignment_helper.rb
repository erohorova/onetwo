module AssignmentHelper
  def get_assign_image(assignable_type, assignable_id)
    assignable_type.constantize.find(assignable_id).try(:image_attachment)
  end
end

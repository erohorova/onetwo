module UserPagesHelper
  def active_link_class(tab)
    case tab
      when :feed
        action_name == 'feed_v1'
      when :my_content
        action_name == 'my_content'
      when :my_profile
        controller_name == 'profile_data'
      when :channels
        action_name == 'channels'  
      when :settings
        action_name == 'settings'
      when :notifications
        action_name == 'notifications'   
      when :packs
        controller_name == 'card_packs'
    end ? 'active' : ''
  end
end

module ProfileDataHelper
  def encoded_user_id(user)
    payload = {user_id: user.id, timestamp: Time.now.to_s}.to_json
    JWT.encode(payload, Settings.features.integrations.secret, 'HS256')
  end
end


module ApiHelper

  def camo_filter(url)
    CamoFilter.filter(url)
  end

  def current_user_following_user_id?(id)
    return false unless current_user && id.present?
    current_user.following_user_id? id
  end

end

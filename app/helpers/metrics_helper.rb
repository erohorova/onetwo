module MetricsHelper

  def url_for_user_content_level_metric(user_content_level_metric)
    slug = user_content_level_metric.content_object.try(:slug) || user_content_level_metric.content_id
    case user_content_level_metric.content_type
    when 'card'
      if user_content_level_metric.content_object.try(:card_type) == "video_stream"
        video_stream_path(id: slug)
      else
        ClientOnlyRoutes.show_insight_path(id: slug)
      end
    when 'collection'
      ClientOnlyRoutes.collection_path(id: slug)
    when 'video_stream'
      video_stream_path(id: slug)
    else
      nil
    end
  end

  def content_owned_by_user?(user_content_level_metric)
    case user_content_level_metric.content_type
      when 'video_stream'
        user_content_level_metric.content_object.try(:creator).try(:id) == current_user.id
      when 'card', 'collection'
        user_content_level_metric.content_object.try(:author).try(:id) == current_user.id
      else
        false
    end
  end

  def content_nested_type(user_content_level_metric)
    user_content_level_metric.content_type == 'card' ? user_content_level_metric.content_object.try(:card_subtype) : nil
  end
end

module PromotionsHelper
  def duration(course)
    course.end_date ? (distance_of_time_in_words(course.start_date, course.end_date)) : "self-paced"
  end
end

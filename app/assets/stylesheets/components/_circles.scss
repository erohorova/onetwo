// Circles - App Component
// - - - - - - - - - - - - - - - - - - - - - - - - -
// Reusable circle elements can be used anywhere in the app.
// Circles here indicate visual representation, not be be confused with
// database model or system object of any type.

// TODO: Modularize as mixin to render based on size
// TODO: Modularize to render as a list or a grid (2,3,5)

.circles-list {
  margin: 1rem 0;
}

.circle {
  height: rem-calc(150);
  width: rem-calc(150);
  @include background-size(cover);
  background-position: center center;
  @include border-radius(50%);
  background-color: rgba(0, 0, 0, 0.5);
  cursor: pointer;

  .circle-name {
    @include element-center;
    padding: rem-calc(3);
    background-color: rgba(0, 0, 0, 0.7);
    text-align: center;
    word-wrap: break-word;
    color: $white;
    font-size: rem-calc(14);
    display: inline-block;
    line-height: rem-calc(14);
    max-width: rem-calc(120);
  }

  @media  only screen and (max-width: 350px) {
    height: rem-calc(100);
    width: rem-calc(100);

    .circle-name {
      font-size: rem-calc(12);
      max-width: rem-calc(90);
    }
  }
}

.medium-circle {
  @extend .circle;
  height: rem-calc(100);
  width: rem-calc(100);
}

.semi-large-circle {
  @extend .circle;
  height: rem-calc(120);
  width: rem-calc(120);

  .circle-name {
    font-size: rem-calc(12);
    max-width: rem-calc(97);
  }
}

.small-circle {
  @extend .medium-circle;
  height: rem-calc(70);
  width: rem-calc(70);
}

// Border
.circle-white-border {
  border: 2px solid $white;
}
require 'active_support/concern'

module Cacheable
  class UnSerializableKeyError < StandardError; end
  extend ActiveSupport::Concern

  def make_key(arg)
    key = ''
    if arg.is_a?(Hash)
      key += '{' +
      arg.map do |key, value|
        "#{key}:#{make_key(value)}"
      end.join(',') + '}'
    elsif arg.is_a?(Array)
      key += '[' + arg.map do |item|
        make_key(item)
      end.join(',') + ']'
    elsif arg.is_a?(String) || arg.is_a?(Fixnum) || arg.is_a?(Float)
      key += "#{arg}"
    elsif arg.respond_to?(:id)
      key += "#{arg.id}"
    else
      raise UnSerializableKeyError.new("Cannot serialize #{arg.class.name} class: #{arg}")
    end
    key
  end
  
  included do
    def initialize(args)
      raise ArgumentError.new('Must be a Hash') unless args.is_a?(Hash)
      @key = make_key(args)
      @args = args
    end
    
    def key
      "#{self.class.name}-#{self.class.class_variable_defined?(:@@version) ? self.class.class_variable_get(:@@version) : 1}-#{@key}"
    end
    
    def value
      raise NotImplementedError.new('value method must be implemented on the child class')
    end

    #dynamically define methods passed into the constructor
    def method_missing(method, *args, &block)
      @args[method]
    end
  end
  
end

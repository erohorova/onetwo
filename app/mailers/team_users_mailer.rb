class TeamUsersMailer < MandrillMailer

  def send_notification(opts = {})
    sender = User.find_by(id: opts[:sender_id])
    user = User.find_by(id: opts[:user_id])
    team = Team.find_by(id: opts[:team_id])
    return if team.nil? || team.organization.nil? || team.organization.send_no_mails?
    if user && sender && opts[:message].present?

      org_logo = get_co_branding_logo(user.organization)
      user = valid_users_email(users: [user], key: 'notification.admin.team').first

      if user

        to = "\"#{[user.first_name, user.last_name].join(' ').squish}\" <#{user.email}>"
        preference_token = ActiveSupport::MessageEncryptor.
          new(Edcast::Application.config.secret_key_base).
          encrypt_and_sign(ActiveSupport::JSON.encode({user_id: user.id,
                                                       preferenceable_type: 'Team',
                                                       preferenceable_id: team.id,
                                                       key: UserPreference::TEAM_NOTIFICATION_KEY,
                                                       value: 'true'})).gsub('=', '_')

        unsubscribe_url = preferences_url(host: Settings.platform_host, token: preference_token)

        global_merge_vars = [
          {"name" => "post_group_name", "content" => team.name},
          {"name" => "org_logo", "content" => org_logo},
          {"name" => "post_message", "content" => opts[:message]},
          {"name" => "post_user", "content" => [sender.first_name, sender.last_name].join(' ').squish},
          {"name" => "first_name", "content" => user.first_name.try(:titleize)},
          {"name" => "show_app_download", "content" => user.organization.get_download_app},
          {"name" => "unsubscribe_url", "content" => unsubscribe_url}
        ]

        subject = "Group Announcement from {{post_user}}"
        global_merge_vars, subject = get_org_custom_config(user.organization, global_merge_vars, subject)

        message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'admin-group-announcements.html.slim',
                                    current_user: user,
                                    subject: subject,
                                    from_email: 'admin@edcast.com',
                                    from_name: 'EdCast {{POST_GROUP_NAME}} Announcement',
                                    to: user_formatted_email_addresses_per_api(user),
                                    mailer: user.organization.mailer_config,
                                    global_merge_vars: global_merge_vars)

        send_api_email(message: message)
      end
    end
  end
end

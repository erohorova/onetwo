class BulkImportEmbargoDetailsMailer < MandrillMailer

  def notify_admin(opts = {})

    admin = opts[:admin]

    if admin
      global_merge_vars = [
        {
            'name' => 'upload_time',
            'content' => opts[:upload_time] && opts[:upload_time].strftime('%b %e, %l:%M %p')
        },
        {
            'name' => 'first_name',
            'content' => admin.first_name.try(:titleize)
        },
        {
            'name' => 'org_logo',
            'content' => get_co_branding_logo(admin.organization)
        }
      ]

      template = 'edcast-bulk-import-embargo-admin' + '.html.slim'
      subject = 'Bulk Import Embargo Details'
      global_merge_vars, subject = get_org_custom_config(admin.organization, global_merge_vars, subject)
      message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: template,
                from_email: 'admin@edcast.com',
                from_name: 'EdCast Admin',
                to: user_formatted_email_addresses_per_api(admin),
                subject: subject,
                global_merge_vars: global_merge_vars,
                mailer: admin.organization.mailer_config)
      send_api_email(message: message)
    end
  end
end

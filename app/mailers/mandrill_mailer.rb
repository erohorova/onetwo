require 'mandrill'
require "base64"

class MandrillMailer < ActionMailer::Base
  include ActionView::Helpers::AssetUrlHelper

  self.smtp_settings = Settings.mandrill.symbolize_keys

  def send_email(*args)

    if @mailer.present?
      sender_email = @mailer.from_address
      sender_name = @mailer.from_name
      args.first[:delivery_method_options] = @mailer.get_config
      args.first[:from] = "\"#{sender_name}\" <#{sender_email}>"
    end
    mail(*args)
  end

  #generate attachments for mandrill api
  def generate_attachments_object(files)
    attachments = []
    files.each do |file|
      tmp_file = Tempfile.new(file.attachment_file_name)
      file.attachment.copy_to_local_file(:original, tmp_file.path)

      obj = {
        "type": file.attachment_content_type,
        "name": file.attachment_file_name,
        "content": Base64.encode64(tmp_file.read)
      }

      attachments << obj

      tmp_file.close
      tmp_file.unlink
    end
    attachments
  end

   # Parameters:
  # message(Struct): Information to be sent
  # async(boolean):  enable a background sending mode that is optimized for bulk sending
  #        Defaults  to false for messages with no more than 10 recipients
  #        messages  with more than 10 recipients are always sent asynchronously, regardless of the value of async
  # ip_pool(String): the name of the dedicated ip pool that should be used to send the message
  #                  If you do not have any dedicated IPs, this parameter has no effect
  #                  If you specify a pool that does not exist, your default pool will be used instead
  # send_at(String): when this message should be sent as a UTC timestamp in YYYY-MM-DD HH:MM:SS format
  #                  If you specify a time in the past, the message will be sent immediately
  #                  An additional fee applies for scheduled email, and this feature is only available to accounts with a positive balance
  def send_api_email(message: ,
                     async: true,
                     ip_pool: nil,
                     send_at: nil,
                     mailer: nil)
    begin
      key = mailer.present? ? mailer.password : Settings.mandrill.password
      mandrill = Mandrill::API.new key
      result = mandrill.messages.send message, async, ip_pool, send_at
      log.info "Mandrill's response #{result}"
    rescue => e
      log.error "An error occurred while sending mail: #{e.class} - #{e.message}"
    end
  end

  # added three parameters
  # @option args [String] :template Mandrill email template
  # @option args [Hash] :common_vars shared variables, eg: {foo: 'bar'}
  # @option args [Hash] :recipient_vars recipient specific variables with email address as the key, eg, {"user1@example.com" => {"var1": "val1"}, "user2@example.com" => {"var1": "val1"}}
  def mail(*args)
    template_slug = args.first.delete(:template)
    _attachments = args.first.delete(:attachments)
    send_without_unsubscribe_url = args.first.delete(:send_without_unsubscribe_url)

    raise ArgumentError.new('Missing template parameter') unless template_slug

    set_template_slug(template_slug)

    common_vars = args.first.delete(:common_vars)

    if common_vars
      set_common_vars(common_vars)
    end

    use_handlebar_lang = args.first.delete(:use_handlebar_lang)
    if use_handlebar_lang
      set_handlebar
    end

    recipient_vars = args.first.delete(:recipient_vars)
    if recipient_vars.is_a?(Hash)
      recipient_vars.each do |email, vars|
        set_recipient_vars(email, vars)
      end
    end

    all_recipients_have_unsubscribe_link = (common_vars.is_a?(Hash) && common_vars.has_key?(:unsubscribe_url)) || (recipient_vars.is_a?(Hash) && recipient_vars.values.map{|v| v[:unsubscribe_url]}.all?)

    raise ArgumentError.new('Missing Unsubscribe Url') unless (send_without_unsubscribe_url || all_recipients_have_unsubscribe_link)

    mail = super

    if _attachments.present?
      _attachments.each do |name, file|
        mail.attachments[name] = file
      end
      mail.text_part = Mail::Part.new
      mail.html_part = Mail::Part.new
    end

    mail.header[:subject] = nil
    mail
  end

  private

  #append http to org_logo url
  def append_http(org_logo = nil)
      uri = URI.parse(org_logo)
      uri.scheme = 'http'
      org_logo = uri.to_s
  end

  def get_co_branding_logo(org = nil)
    if org
      org_logo = org.co_branding_logo.present? ? org.co_branding_logo.url : nil
      org_logo = append_http(org_logo) if org_logo
    end
  end

  def skip_email_notifcations?(org)
    return true if org.nil?
    return true if org.send_no_mails?
    return true if org.notifications_and_triggers_enabled?
    false
  end

  # suppress local template
  def collect_responses(*args)
    []
  end

  def set_handlebar
    headers['X-MC-MergeLanguage'] = 'Handlebars'
  end

  def set_recipient_vars(email, vars)
    headers['X-MC-MergeVars'] = ActiveSupport::JSON.encode({_rcpt: email}.merge(vars))
  end

  def set_common_vars(vars)
    headers['X-MC-MergeVars'] = ActiveSupport::JSON.encode(vars)
  end

  def set_template_slug(template_slug)
    headers['X-MC-Template'] = template_slug
  end

  def formatted_email_address_from_email_and_name(email, name)
    _address = Mail::Address.new
    _address.address = email
    _address.display_name = name if name
    _address.format #doing this will escape weird characters in the name field
  end

  def format_email_address_for_api(email, name)
    {
      "email": email,
      "name": name || '',
      "type": "to"
    }
  end

  def user_formatted_email_addresses_per_api(users)
    if users.kind_of?(Array)
      users.map do |user|
        format_email_address_for_api(user.mailable_email, user.name)
      end
    else
      [format_email_address_for_api(users.mailable_email, users.name)]
    end
  end

  def user_formatted_email_addresses(users)
    if users.kind_of?(Array)
      users.map do |user|
        formatted_email_address_from_email_and_name(user.mailable_email, user.name)
      end
    else
      formatted_email_address_from_email_and_name(users.mailable_email, users.name)
    end
  end

  def valid_users_email(users: [], group: nil, key: '', omit_user_id: nil)
    _users = users.select do |user|
        user.is_suspended == false && !user.mailable_email.blank? && user.id != omit_user_id &&
            (group.nil? ? !user.pref_email_opt_out?(key) : !user.pref_group_opt_out?(group, key))
    end
  end

  def self.stripped_snippet(text, len=15, sep=' ')
    text.nil? ? nil : Nokogiri::HTML.fragment(text).inner_text.truncate(len, separator: sep)
  end

  protected

    def global_vars(collection)
      global_merge_vars = [
        { 'name' => 'first_name', 'content' => (@user.first_name || '') },
        { 'name' => 'email', 'content' => @user.mailable_email },
        { 'name' => 'org_logo', 'content' => get_co_branding_logo(@user.organization) }
      ]
      global_merge_vars += collection

      global_merge_vars.compact
    end

    def get_org_custom_css(org, collection)
      custom_css = org.mail_custom_css_options
      custom_css = custom_css["enableCustomCss"] ? custom_css : {}
      global_merge_vars = [
        {'name' => 'header_background_color', 'content' => custom_css["headerBackgroundColor"] || "##{Settings.mail_css.header_background_color}" },
        {'name' => 'text_color', 'content' => custom_css["textColor"] || "##{Settings.mail_css.text_color}" },
        {'name' => 'section_text_color', 'content' => custom_css["textColor"] || "##{Settings.mail_css.section_text_color}" },
        {'name' => 'highlight_text_color', 'content' => custom_css["textColor"] || "##{Settings.mail_css.highlight_text_color}" },
        {'name' => 'button_blue_color', 'content' => custom_css["buttonBackgroundColor"] || "##{Settings.mail_css.button_blue_color}" },
        {'name' => 'button_background_color', 'content' => custom_css["buttonBackgroundColor"] || "##{Settings.mail_css.button_background_color}" }
      ]
      global_merge_vars += collection
      return global_merge_vars
    end

    def get_org_custom_config(org, collection, subject)
      custom_options = org.custom_footer_options
      use_custom_name = custom_options["supportEmail"] && (custom_options["supportEmail"] != Settings.support_email)
      global_merge_vars = [
        {'name' => 'org_display_name', 'content' => use_custom_name ? org.name : 'EdCast'},
        {'name' => 'appstore_link', 'content' => custom_options["appStore"] || Settings.ios_app_location},
        {'name' => 'playstore_link', 'content' => custom_options["playStore"] || Settings.android_app_location},
        {'name' => 'support_email', 'content' => custom_options["supportEmail"] || Settings.support_email},
        {'name' => 'show_edcast_name', 'content' => !use_custom_name },
        {'name' => 'show_edcast_logo', 'content' => org.enable_edcast_logo },
        {'name' => 'promotions', 'content' => custom_options["promotions"]},
        {'name' => 'home_page', 'content' => org.home_page.split('//').last}
      ]
      global_merge_vars = get_org_custom_css(org, global_merge_vars)
      subject = subject.gsub(/edcast/i, org.name) if use_custom_name
      global_merge_vars += collection
      return [global_merge_vars.compact, subject]
    end

    def recipient_name(user)
      user.first_name || user.last_name ?
        "\"#{[user.first_name, user.last_name].compact.join(' ')}\" <#{user.mailable_email}>" :
        user.mailable_email
    end

    def load_static_assets
      {
        arrow: asset_url('assets/arrows.png'),
        edcast_banner: asset_url('assets/mailer_header.png'),
        facebook: asset_url('assets/facebook.png'),
        google_plus: asset_url('assets/gmail.png'),
        linkedin: asset_url('assets/linkedin.png'),
        twitter: asset_url('assets/twitter.png')
      }
    end

    # mandrill api send_template method to handle multibyte characters issue in mandrill headers
    # ref: https://github.com/mikel/mail/issues/1016
    def send_mandrill_template(template:, args:, use_handlebar_lang: false)
      begin
        api_key = Settings.mandrill.password
        if @mailer.present?
          api_key           = @mailer.password
          args[:from_name]  = "\"#{@mailer.from_name}\""
          args[:from_email] = "#{@mailer.from_address}"
        end

        mandrill = Mandrill::API.new(api_key) # USE mandrill api key
        args[:merge_language] = 'handlebars' if use_handlebar_lang
        mandrill.messages.send_template(template, [], args)
      rescue Mandrill::Error => e
        # Mandrill errors are thrown as exceptions
        log.error "A mandrill error occurred: #{e.class} - #{e.message}"
      end
    end
end

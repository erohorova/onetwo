# coding: utf-8
require 'uri'
require 'rack/utils'
include ActionView::Helpers::NumberHelper
#require 'slim/smart'

class UserMailer < MandrillMailer

  def confirm_email_address(email_record_id)
    @email_record = Email.find(email_record_id)
    @user = @email_record.user
    first_name = @user.first_name ? @user.first_name : ''

    if @email_record.confirmation_token.nil?
      log.warn "Trying to send confirmation email to email: #{@email_record.email} without confirmation token" and return
    end

    url = confirm_email_address_url(token: @email_record.confirmation_token)
    email = @email_record.email
    to_name = "#{[@user.first_name, @user.last_name].compact.join(' ')}"
    subject = 'Confirm your e-mail address'
    org_logo = get_co_branding_logo(@user.organization)

    global_merge_vars = [
      {"name" => "first_name", "content" => first_name},
      {"name" => "link_url", "content" => url},
      {"name" => "email", "content" => email},
      {"name" => "org_logo", "content" => org_logo}
    ]

    global_merge_vars, subject = get_org_custom_config(@user.organization, global_merge_vars, subject)
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'confirm-email-address.html.slim',
                                    subject: subject,
                                    from_email: "admin@edcast.com",
                                    from_name: %Q{"#{(@email_record.contextable && @email_record.contextable.name) || 'Course'} Discussion"},
                                    mailer: @user.organization.mailer_config,
                                    to: [{"email" => @email_record.email, "name" => to_name, "type"=> 'to'}],
                                    global_merge_vars: global_merge_vars)

    send_api_email(message: message)
  end

  def data_dump(group_id, requester_id, url)
    data_dump_sync(Group.find(group_id), User.find(requester_id), url)
  end

  def data_dump_sync(group, user, url)

    url = url
    first_name = user.first_name ? user.first_name : ''
    email = user.email
    subject = 'Your data dump request'
    org_logo = get_co_branding_logo(user.organization)

    global_merge_vars = [
      {"name" => "first_name", "content" => first_name},
      {"name" => "link_url", "content" => url},
      {"name" => "email", "content" => email},
      {"name" => "org_logo", "content" => org_logo}
    ]

    global_merge_vars, subject = get_org_custom_config(user.organization, global_merge_vars, subject)
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'data-dump.html.slim',
                subject: subject,
                from_email: 'admin@edcast.com',
                from_name: 'EdCast Forum Dump',
                to: user_formatted_email_addresses_per_api(user),
                mailer: user.organization.mailer_config,
                global_merge_vars: global_merge_vars)

    send_api_email(message: message)
  end

   def send_user_data_export_request(user_id)
    user = User.find_by(id: user_id)
    return unless user.present?

    org_logo = get_co_branding_logo(user.organization)

    global_merge_vars = [
      {"name" => "user_email", "content" => user.email},
      {"name" => "name", "content" => user.name},
      {"name" => "organization", "content" => user.organization.host},
      {"name" => "org_logo", "content" => org_logo}
    ]

    subject = "GDPR: Data Export Request from #{user.organization.host}"
    to = [{"email": Settings.support_email, "name": "" || '',"type": "to"}]
    global_merge_vars, subject = get_org_custom_config(user.organization, global_merge_vars, subject)
    
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'gdpr-data-download.html.slim',
                subject: subject,
                from_email: 'admin@edcast.com',
                from_name: 'Admin User',
                to: to,
                mailer: user.organization.mailer_config,
                global_merge_vars: global_merge_vars)
    
    send_api_email(message: message)
  end


  def follower_batch_notification_email(followee, followers_list)
    to_email = followee.mailable_email
    org = followee.organization
    return if skip_email_notifcations?(org)

    org_logo = get_co_branding_logo(org)

    followee = valid_users_email(users: [followee]).first
    return if to_email.nil? || followee.nil?

    preference_token = ActiveSupport::MessageEncryptor.
        new(Edcast::Application.config.secret_key_base).
        encrypt_and_sign(ActiveSupport::JSON.encode({user_id: followee.id,
                                                     key: 'notification.follow',
                                                     value: false})).gsub('=', '_')

    unsubscribe_url = preferences_url(host: Settings.platform_host, token: preference_token)

    global_merge_vars = [
      {"name" => "first_name", "content" => followee.first_name},
      {"name" => "unsubscribe_url", "content" => unsubscribe_url},
      {"name" => "email", "content" => followee.email},
      {"name" => "followers", "content" => followers_list.map{|f| {'name' => f.name, 'picture' => "#{f.photo}", 'url' => "#{(Settings.platform_force_ssl ||
                        Rails.env.production? ||
                        Rails.env.staging?) ? 'https' : 'http'}://#{followee.organization.host}/#{f.handle_no_at}"}}},
      {"name" => "user_name", "content" => followee.name},
      {"name" => "org_name", "content" => org.name},
      {"name" => "org_logo", "content" => org_logo},
      {"name" => "followers_count", "content" => followers_list.count}
    ]

    subject = "[EdCast] Congratulations! You have new followers."
    user = org.users.find_by(email: to_email)
    global_merge_vars, subject = get_org_custom_config(org, global_merge_vars, subject)
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'follow-batch-email.html.slim',
                current_user: user,
                subject: subject,
                from_email: 'admin@edcast.com',
                from_name: 'EdCast',
                to: user_formatted_email_addresses_per_api(followee),
                mailer: org.mailer_config,
                global_merge_vars: global_merge_vars)

    send_api_email(message: message)
  end

  def invite_collaborator(inviter ,user, channel)
    to_email = user.mailable_email
    org = channel.organization
    return if to_email.nil? || user.is_suspended || skip_email_notifcations?(org)
    invite_url = learn_url(id: channel.slug, host: channel.organization.host)
    first_name = user.first_name ? user.first_name : ''
    inviter_name = inviter.name
    org_logo = get_co_branding_logo(org)

    global_merge_vars = [
        {"name" => "first_name", "content" => first_name},
        {"name" => "invite_url", "content" => invite_url},
        {"name" => "channel_name", "content" => channel.label},
        {"name" => "inviter_name", "content" => inviter_name},
        {"name" => "show_app_download", "content" => org.get_download_app},
        {"name" => "org_logo", "content" => org_logo}
    ]

    subject = "{{inviter_name}} has invited you to collaborate on an EdCast Channel."

    global_merge_vars, subject = get_org_custom_config(org, global_merge_vars, subject)
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'invite_collaborator.html.slim',
                                    subject: subject,
                                    from_email: 'admin@edcast.com',
                                    from_name: 'EdCast',
                                    to: user_formatted_email_addresses_per_api(user),
                                    mailer: org.mailer_config,
                                    global_merge_vars: global_merge_vars)

    send_api_email(message: message)
  end

  # admin only notification
  def channel_follows_notification_to_admin(inviter, channel_names, followers_emails, teams_names)
    organization = inviter.organization
    admin_users = valid_users_email(users: organization.users.where(organization_role: 'admin').where.not(id: inviter.id).to_a)
    return if (admin_users.blank? || skip_email_notifcations?(organization))
    inviter_name = inviter.name
    to_channel_string = "to the #{'channel'.pluralize(channel_names.size)}"
    org_logo = get_co_branding_logo(organization)

    global_merge_vars = [
      {"name" => "channel_name", "content" => channel_names.join(',')},
      {"name" => "inviter_name", "content" => inviter_name},
      {"name" => "show_app_download", "content" => organization.get_download_app},
      {"name" => "org_logo", "content" => org_logo},
      {"name" => "to_channel_string", "content" => to_channel_string},
      {"name" => "followers_emails", "content"=> followers_emails},
      {"name" => "followers_emails_present", "content"=> followers_emails.any?},
      {"name" => "teams_names_present", "content"=> teams_names.any?},
      {"name" => "teams_names", "content"=> teams_names},
      {"name" => "users_string", "content" => 'user'.pluralize(followers_emails.size) },
      {"name" => "teams_string", "content" => 'group'.pluralize(teams_names.size) },
      {"name" => "bulk_users", "content"=> followers_emails.size >= ChannelsUser::BULK_USER_START}
    ]

    subject = "#{inviter_name} has added #{'follower'.pluralize(followers_emails.size + teams_names.size)} #{to_channel_string}"

    global_merge_vars, subject = get_org_custom_config(organization, global_merge_vars, subject)

    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'channel_follows_notification_to_admin.html.slim',
                                    subject: subject,
                                    from_email: 'admin@edcast.com',
                                    from_name: 'EdCast',
                                    to: user_formatted_email_addresses_per_api(admin_users),
                                    mailer: organization.mailer_config,
                                    merge_vars: admin_vars(admin_users),
                                    global_merge_vars: global_merge_vars)

    send_api_email(message: message)
  end

  def invite_curator(inviter ,user, channel)
    to_email = user.mailable_email
    org = channel.organization
    return if to_email.nil? || user.is_suspended || skip_email_notifcations?(org)
    invite_url = channel.organization.host + '/curate'
    first_name = user.first_name ? user.first_name : ''
    inviter_name = inviter.name
    org_logo = get_co_branding_logo(org)

    global_merge_vars = [
        {"name" => "first_name", "content" => first_name},
        {"name" => "invite_url", "content" => invite_url},
        {"name" => "channel_name", "content" => channel.label},
        {"name" => "inviter_name", "content" => inviter_name},
        {"name" => "show_app_download", "content" => org.get_download_app},
        {"name" => "org_logo", "content" => org_logo}
    ]

    subject = "{{inviter_name}} has invited you to curate the content on an EdCast Channel."

    global_merge_vars, subject = get_org_custom_config(org, global_merge_vars, subject)
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'invite_curator.html.slim',
                                    subject: subject,
                                    from_email: 'admin@edcast.com',
                                    from_name: 'EdCast',
                                    to: user_formatted_email_addresses_per_api(user),
                                    mailer: org.mailer_config,
                                    global_merge_vars: global_merge_vars)

    send_api_email(message: message)
  end

  def invite_follower(inviter, user, channel)
    to_email = user.mailable_email
    org = channel.organization
    return if to_email.nil? || user.is_suspended || skip_email_notifcations?(org)
    invite_url = learn_url(id: channel.slug, host: channel.organization.host)
    first_name = user.first_name ? user.first_name : ''
    inviter_name = inviter.name
    org_logo = get_co_branding_logo(org)

    global_merge_vars = [
        {"name" => "first_name", "content" =>first_name},
        {"name" => "invite_url", "content" =>invite_url},
        {"name" => "channel_name", "content" =>channel.label},
        {"name" => "inviter_name", "content" =>inviter_name},
        {"name" => "show_app_download", "content" => org.get_download_app},
        {"name" => "org_logo", "content" => org_logo}
    ]

    subject = "{{inviter_name}} has invited you to follow an EdCast Channel."

    global_merge_vars, subject = get_org_custom_config(org, global_merge_vars, subject)
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'invite_follower.html.slim',
                                    subject: subject,
                                    from_email: 'admin@edcast.com',
                                    from_name: 'EdCast',
                                    to: user_formatted_email_addresses_per_api(user),
                                    mailer: org.mailer_config,
                                    global_merge_vars: global_merge_vars)

    send_api_email(message: message)
  end

  def send_daily_digest(group, user, posts)
    group['mailer_config_id'] && @mailer = MailerConfig.find(group['mailer_config_id'])
    to_email = user.mailable_email
    org = user.organization
    return if to_email.nil? || user.is_suspended || org.send_no_mails?

    org_logo = get_co_branding_logo(org)
    extension = @mailer ? '.html' : '.html.slim'

    user_name = user.name ? user.name : ''



    preference_token = ActiveSupport::MessageEncryptor.
        new(Edcast::Application.config.secret_key_base).
        encrypt_and_sign(ActiveSupport::JSON.encode({user_id: user.id,
                                                     preferenceable_type: 'Group',
                                                     preferenceable_id: group['group_id'],
                                                     key: UserPreference::FORUM_DAILY_DIGEST_KEY,
                                                     value: 'true'})).gsub('=', '_')

    unsubscribe_url = preferences_url(host: Settings.platform_host, token: preference_token)

    # handlebar variables name: variable name, content: data to populate
    global_merge_vars = [
      { "name" => "user_name", "content" => user_name },
      { "name" => "group", "content" => group },
      { "name" => "posts", "content" => posts },
      { "name" => "org_logo", "content" => org_logo },
      {"name" => "show_app_download", "content" => org.get_download_app},
      { "name" => "unsubscribe_url", "content" => unsubscribe_url }
    ]

    subject = "Your personalized daily digest from {{group.group_name}}"
    global_merge_vars, subject = get_org_custom_config(org, global_merge_vars, subject)
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'daily-digest'+extension,
                                    subject: subject,
                                    from_email: 'admin@edcast.com',
                                    from_name: 'EdCast',
                                    to: user_formatted_email_addresses_per_api(user),
                                    global_merge_vars: global_merge_vars,
                                    mailer: @mailer)

    send_api_email(message: message, mailer: @mailer)
  end

  def send_instructor_daily_digest_report(user_id, url, language)
    user = User.find_by(id: user_id)
    return if user.nil?

    to_email = user.mailable_email
    return if to_email.nil? || user.is_suspended || user.organization.send_no_mails?

    org_logo = get_co_branding_logo(user.organization)

    global_merge_vars = [
      {"name" => "first_name", "content" => user.first_name || ''},
      {"name" => "email", "content" => to_email},
      {"name" => "org_logo", "content" => org_logo},
      {"name" => "link_url", "content" => url},
      {"name" => "language", "content" => language}
    ]

    subject = "[EdCast] Your daily instructor digest report for {{language}} courses"
    global_merge_vars, subject = get_org_custom_config(user.organization, global_merge_vars, subject)

    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'instructor-daily-digest-report.html.slim',
                                    subject: subject,
                                    from_email: 'admin@edcast.com',
                                    from_name: 'EdCast',
                                    to: user_formatted_email_addresses_per_api(user),
                                    mailer: user.organization.mailer_config,
                                    global_merge_vars: global_merge_vars)

    send_api_email(message: message)
  end

  def send_assessment_report(s3_url:, user_id:, card_title:)
    @user = User.not_suspended.find(user_id)

    if @user.mailable_email.nil?
      log.info "User does not have mailable email"
      return
    end

    vars = [
      { 'name' => 'link_url', 'content' => s3_url },
      { 'name' => 'card_title', 'content' => card_title }
    ]
    global_merge_vars = global_vars(vars)

    subject = "[EdCast] Your assessment report for {{card_title}}..."
    global_merge_vars, subject = get_org_custom_config(@user.organization, global_merge_vars, subject)

    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'assessment-report.html.slim',
                                    subject: subject,
                                    from_email: 'admin@edcast.com',
                                    from_name: 'EdCast',
                                    to: user_formatted_email_addresses_per_api(@user),
                                    mailer: @user.organization.mailer_config,
                                    global_merge_vars: global_merge_vars)

    send_api_email(message: message)
  end

  def send_organization_report(s3_url:, user_ids:, email_title_snippet:)
    users = User.where(id: user_ids)

    organization = users.first.organization
    users.each do |user|
      @user = user
      vars = [
        { 'name' => 'link_url', 'content' => s3_url }
      ]
      global_merge_vars = global_vars(vars)

      subject = "[EdCast] Organization analytics report: #{email_title_snippet}"
      global_merge_vars, subject = get_org_custom_config(organization, global_merge_vars, subject)

      message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'organization-analytics-report.html.slim',
                                      subject: subject,
                                      from_email: 'admin@edcast.com',
                                      from_name: 'EdCast',
                                      to: user_formatted_email_addresses_per_api(@user),
                                      mailer: @user.organization.mailer_config,
                                      global_merge_vars: global_merge_vars)

      send_api_email(message: message)
    end
  end

  def send_daily_instructor_mails(user, data)
    to_email = user.mailable_email
    return if to_email.nil? || user.is_suspended || user.organization.send_no_mails?
    user_name = user.name ? user.name : ''
    org_logo = get_co_branding_logo(user.organization)

    global_merge_vars = [
        { "name" => "user_name", "content" => user_name },
        {"name" => "show_app_download", "content" => user.organization.get_download_app},
        { "name" => 'data', "content" => data },
        { "name" => 'org_logo',"content" => org_logo }
      ]

    subject = "Daily updates for Instructors"
    global_merge_vars, subject = get_org_custom_config(user.organization, global_merge_vars, subject)
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'instructor-daily.html.slim',
                                    subject: subject,
                                    from_email: 'admin@edcast.com',
                                    from_name: 'EdCast',
                                    to: user_formatted_email_addresses_per_api(user),
                                    mailer: user.organization.mailer_config,
                                    global_merge_vars: global_merge_vars)

    send_api_email(message: message)
  end

  def send_courses_report(s3_url:, user:)
    valid_users = valid_users_email(users: [user])
    return if valid_users.empty?

    @user = valid_users.first

    vars = [{ 'name' => 'link_url', 'content' => s3_url }]

    global_merge_vars = global_vars(vars)
    subject = "[EdCast] Your courses report is ready..."
    global_merge_vars, subject = get_org_custom_config(@user.organization, global_merge_vars, subject)

    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'courses-report.html.slim',
                                    subject: subject,
                                    from_email: 'admin@edcast.com',
                                    from_name: 'EdCast',
                                    to: user_formatted_email_addresses_per_api(user),
                                    mailer: user.organization.mailer_config,
                                    global_merge_vars: global_merge_vars)

    send_api_email(message: message)
  end

  def weekly_activity(org, users, activity, score_stats)
    valid_users = valid_users_email(users: users.to_a)
    return if valid_users.empty? || skip_email_notifcations?(org)
    org_logo = get_co_branding_logo(org)
    global_merge_vars = [
      {"name" => "org", "content" => org.slice(:name)},
      {"name" => "org_logo", "content" => org_logo},
      {"name" => "org_url", "content" => org.home_page},
      {"name" => "insights", "content" => activity[:insights]},
      {"name" => "video_streams", "content" => activity[:video_streams]},
      {"name" => "pathways", "content" => activity[:pathways]},
      {"name" => "points_earned", "content" => number_with_delimiter(score_stats[:points_earned])},
      {"name" => "team_standing", "content" => score_stats[:team_standing]},
      {"name" => "show_app_download", "content" => org.get_download_app},
      {"name" => "avg_points_earned_by_team", "content" => number_with_delimiter(score_stats[:avg_points_earned_by_team])}
    ]


    activity_length = activity[:insights].length + activity[:video_streams].length + activity[:pathways].length
    template_name = activity_length < 2 ? 'weekly-activity-fall-back' : 'weekly-activity'

    subject = "[EdCast] This week’s activity in your EdCast team"
    global_merge_vars, subject = get_org_custom_config(org, global_merge_vars, subject)
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: "#{template_name}.html.slim",
                                  subject: subject,
                                  from_email: 'admin@edcast.com',
                                  from_name: 'EdCast',
                                  to: user_formatted_email_addresses_per_api(valid_users),
                                  merge_vars: recipient_vars(users, opt_out_key: UserPreference::WEEKLY_ACTIVITY_KEY, opt_out_val: false),
                                  mailer: org.mailer_config,
                                  global_merge_vars: global_merge_vars)
    send_api_email(message: message)
  end

  def assignment_notification(team, assignment, assignor, assignment_message=nil)
    user = assignment.assignee
    return if skip_email_notifcations?(user.organization)

    team_name = team.try(:name)
    assignment_message = assignment_message
    to_email = user.mailable_email
    org_logo = get_co_branding_logo(user.organization)
    user_name = user.name ? user.name : ''
    assignment_due_at = assignment.due_at&.strftime("%B %e, %Y")
    assignment_start_date = assignment.start_date&.strftime("%B %e, %Y")
    # Assignor name should always be there
    assignor_name = assignor.name

    # To support assignments of all kinds, i.e. Smartbites, livestreams, pathways.
    assignment_url = assignment.deeplinked_assignment_url

    from = "\"EdCast\" <admin@edcast.com>"

    preference_token = ActiveSupport::MessageEncryptor.
        new(Edcast::Application.config.secret_key_base).
        encrypt_and_sign(ActiveSupport::JSON.encode({user_id: user.id,
                                                     key: UserPreference::ASSIGNMENT_NOTIFICATION_KEY,
                                                     value: false})).gsub('=', '_')

    unsubscribe_url = preferences_url(host: Settings.platform_host, token: preference_token)

    global_merge_vars = [
      {"name" => "user_name", "content" => user_name},
      {"name" => "assignment_message", "content" => assignment_message},
      {"name" => "assignment_title", "content" => assignment.email_snippet},
      {"name" => "assignor_name", "content" => assignor_name},
      {"name" => "team_name", "content" => team_name},
      {"name" => "assignment_url", "content" => assignment_url},
      {"name" => "org_logo", "content" => org_logo},
      {"name" => "unsubscribe_url", "content" => unsubscribe_url},
      {"name" => "assignment_due_at", "content" => assignment_due_at},
      {"name" => "assignment_start_date", "content" => assignment_start_date}
    ]

    subject = "[EdCast] {{assignor_name}} assigned you content"
    global_merge_vars, subject = get_org_custom_config(user.organization, global_merge_vars, subject)

    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: "user-team-assignment-email-revised.html.slim",
                                  subject: subject,
                                  from_email: 'admin@edcast.com',
                                  from_name: 'EdCast',
                                  to: user_formatted_email_addresses_per_api(user),
                                  mailer: user.organization.mailer_config,
                                  global_merge_vars: global_merge_vars)

    send_api_email(message: message)
  end

  def magic_link(user, token)
    branchmetrics_payload = {
      '$deeplink_path': 'login_user',
      # mobile will request join_url after app launch/install via branch
      '$desktop_url': new_user_session_url(token: token) ,
      'user': {id: user.id, name: user.name, handle: user.handle, jwt_token: token }
    }

    magic_link_url = magic_link_login_api_v2_users_url(token: token, host: user.organization.host)
    global_merge_vars = [
      {"name" => "first_name", "content" => user.first_name},
      {"name" => "magic_link", "content" => magic_link_url},
      {"name" => "org_name", "content" => user.organization.name},
      {"name" => "org_logo", "content" => get_co_branding_logo(user.organization)},
    ]

    subject = '[Edcast] Magic Link'
    global_merge_vars, subject = get_org_custom_config(user.organization, global_merge_vars, subject)

    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: "user-magic-link.html.slim",
                                  current_user: user,
                                  subject: subject,
                                  from_email: 'admin@edcast.com',
                                  from_name: 'EdCast',
                                  to: user_formatted_email_addresses_per_api(user),
                                  mailer: user.organization.mailer_config,
                                  global_merge_vars: global_merge_vars)

    send_api_email(message: message)
  end

  protected

  def add_deeplink(content_array, deep_link_type)
    content_array.map do |content|
      content[:url] = BranchMetricUrlGenerator.generator_url(content[:url], content[:id], deep_link_type)
      content
    end
  end

  def default_from
    "\"EdCast\" <admin@edcast.com>"
  end

  def recipient_vars(users, opt_out_key:, opt_out_val: )
    merge_vars = []
    users.each do |user|
      preference_token = ActiveSupport::MessageEncryptor.
          new(Edcast::Application.config.secret_key_base).
          encrypt_and_sign(ActiveSupport::JSON.encode({user_id: user.id,
                                                     key: opt_out_key,
                                                     value: opt_out_val})).gsub('=', '_')

      unsubscribe_url = preferences_url(host: Settings.platform_host, token: preference_token)
      merge_vars << {"rcpt"=> user.mailable_email,
            "vars"=>[
              {"name" => "first_name", "content" => user.first_name},
              {"name" => "last_name", "content" => user.last_name},
              {"name" => "name", "content" => user.name},
              {"name" => "unsubscribe_url", "content" => unsubscribe_url}
            ]
      }
    end
    merge_vars
  end

  def calculate_time(total_seconds) # in seconds
      mm, ss = total_seconds.divmod(60)
      hh, mm = mm.divmod(60)
      dd, hh = hh.divmod(24)

      if dd > 0
        [dd, dd == 1 ? 'day' : 'days', hh, hh == 1 ? 'hr' : 'hrs']
      elsif hh > 0
        [hh, hh == 1 ? 'hr' : 'hrs', mm, mm == 1 ? 'min' : 'mins']
      elsif mm > 0
        mm = ss > 30 ? mm+1 : mm
        [mm, mm == 1 ? 'min' : 'mins']
      else
        [0, 'min']
      end
  end

  private

  def admin_vars(users)
    merge_vars = []
    users.each do |user|
      user_vars = {
        "rcpt": user.email,
        "vars": [
          {
            "name": "first_name",
            "content": user.first_name
          },
          {
            "name": "last_name",
            "content": user.last_name
          },
          {
            "name": "name",
            "content": user.name
          }
        ]
      }
    merge_vars << user_vars
    end
    merge_vars
  end
end

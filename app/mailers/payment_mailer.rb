require 'uri'
require 'rack/utils'

class PaymentMailer < MandrillMailer

  def user_subscription_notification(user_subscription_id)
    user_subscription = UserSubscription.find_by(id: user_subscription_id)
    if user_subscription.nil?
      log.warn "Trying to send mail for bad user_subscription_id : #{user_subscription_id}" and return
    end
    user = User.find_by(id: user_subscription.user_id)
    global_merge_vars = [
      {"name" => "first_name", "content" => user.first_name.capitalize},
      {"name" => "amount", "content" => user_subscription.amount},
      {"name" => "currency", "content" => user_subscription.currency},
      {"name" => "org_name", "content" => user.organization.name},
      {"name" => "org_logo", "content" => get_co_branding_logo(user.organization)},
      {"name" => "gateway_id", "content" => user_subscription.gateway_id.upcase},
      {"name" => "subscription_start_date", "content" => user_subscription.start_date&.strftime("%B %e, %Y")},
      {"name" => "subscription_end_date", "content" => user_subscription.end_date&.strftime("%B %e, %Y")}
    ]
    subject = "Payment Confirmation for {{org_name}}"
    global_merge_vars, subject = get_org_custom_config(user.organization, global_merge_vars, subject)
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: "payment_notification.html.slim",
                                  subject: subject,
                                  from_email: 'admin@edcast.com',
                                  from_name: 'EdCast',
                                  to: user_formatted_email_addresses_per_api(user),
                                  mailer: user.organization.mailer_config,
                                  global_merge_vars: global_merge_vars)

    send_api_email(message: message)
  end

  def user_card_subscription_notification(card_subscription_id)
    user_card_subscription = CardSubscription.find_by(id: card_subscription_id)
    if user_card_subscription.nil?
      log.warn "Trying to send mail for bad card_subscription_id : #{card_subscription_id}" and return
    end
    transaction = Transaction.find_by(id: user_card_subscription.transaction_id)
    card = user_card_subscription.card
    user = user_card_subscription.user
    course_name  =  (card.resource&.title || card.message.to_s)
    currency = transaction.currency == "SKILLCOIN" ? "Skillcoins" : transaction.currency
    global_merge_vars = [
      {"name" => "first_name", "content" => user.first_name.capitalize},
      {"name" => "amount", "content" => transaction.amount.to_f},
      {"name" => "currency", "content" => currency},
      {"name" => "org_name", "content" => user.organization.name},
      {"name" => "org_logo", "content" => get_co_branding_logo(user.organization)},
      {"name" => "course_provider_logo", "content" => card.provider_image},
      {"name" => "course_provider_name", "content" => card.provider},
      {"name" => "course_name", "content" => course_name&.truncate(70)},
      {"name" => "course_image_url", "content" => card.resource&.image_url}
    ]
    subject = "Your Receipt for purchase on {{org_name}}"
    global_merge_vars, subject = get_org_custom_config(user.organization, global_merge_vars, subject)
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: "card_payment_notification.html.slim",
                                  subject: subject,
                                  from_email: 'admin@edcast.com',
                                  from_name: 'EdCast',
                                  to: user_formatted_email_addresses_per_api(user),
                                  mailer: user.organization.mailer_config,
                                  global_merge_vars: global_merge_vars)

    send_api_email(message: message)
  end

  def wallet_recharge_notification(wallet_transaction_id)
    wallet_transaction = WalletTransaction.find_by(id: wallet_transaction_id)
    if wallet_transaction.nil?
      log.warn "Trying to send mail for bad wallet_transaction_id : #{wallet_transaction_id}" and return
    end
    transaction = wallet_transaction.get_transaction_for_recharge
    user = wallet_transaction.user
    balance = user.wallet_balance
    wallet_url = user.organization.home_page + '/me/skill-coins'
    skillcoin_amount = wallet_transaction.amount.round
    global_merge_vars = [
      {"name" => "first_name", "content" => user.first_name.capitalize},
      {"name" => "skillcoins", "content" => skillcoin_amount},
      {"name" => "amount", "content" => transaction.amount.to_f},
      {"name" => "currency", "content" => transaction.currency},
      {"name" => "balance", "content" => balance.amount.round},
      {"name" => "wallet_link", "content" => wallet_url},
      {"name" => "org_name", "content" => user.organization.name},
      {"name" => "org_logo", "content" => get_co_branding_logo(user.organization)},
    ]
    subject = "#{skillcoin_amount} Skillcoins successfully added to your wallet"
    global_merge_vars, subject = get_org_custom_config(user.organization, global_merge_vars, subject)
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: "wallet_recharge_notification.html.slim",
                                  subject: subject,
                                  from_email: 'admin@edcast.com',
                                  from_name: 'EdCast',
                                  to: user_formatted_email_addresses_per_api(user),
                                  mailer: user.organization.mailer_config,
                                  global_merge_vars: global_merge_vars)

    send_api_email(message: message)
  end

end
class ActivityNotificationMailer < MandrillMailer
  default "X-MC-PreserveRecipients" => "false"
  def activity_notification_email(followable: nil, users: [], content: nil, omit_user_id: nil, activity_id: nil)
    follow_class_name = followable.class.name.downcase
    return if followable.group && followable.group.organization.send_no_mails?
    unless followable.nil?
      group= followable.group
      snippet = nil
      snippet = content.html_safe if content
      _users = valid_users_email(users: users, group: group, key: 'notification.post', omit_user_id: omit_user_id)
      unless _users.empty?
        @url = nil
        if followable.group
          @url = followable.class.url(followable.attributes)
        end
        activity = 'Invitation'
        subject = nil
        @mailer = followable.group.mailer_config
        extension = @mailer ? '.html' : '.html.slim'
        # declaring default template name
        common_template = 'new-comment-activity-notification'
        template_name = 'new-comment-activity-notification'+ extension
        if followable.class.name == 'Question'
          activity = 'Answer'
          subject = "New answer to Question \"#{ActivityNotificationMailer.stripped_snippet(followable.title, 40)}\""

          # to localize this email getting the language information from the group
          # if the group type is resource group then getting it language otherwise 
          # if it is a private group getting the parent's language
          if group.present?

            locale = group.type == "PrivateGroup" ? group.resource_group.language : group.language

            # if locale is not blank or chinese adding that to the template name and setting subject
            if locale.present? && ['en', 'zh-cn'].exclude?(locale)

                template_name = "localized/localized-" + common_template +"-"+ locale + extension

                # subject depending on locale
                case locale 
                when "ar"   
                  subject = "\"#{ActivityNotificationMailer.stripped_snippet(followable.title, 40)}\" جواب جديد على السؤال"
                  follow_class_name = "الأسئلة"
                when "es-es"   
                  subject = "Nueva respuesta a la pregunta \"#{ActivityNotificationMailer.stripped_snippet(followable.title, 40)}\""
                  follow_class_name = "pregunta"
                when "fr"   
                  subject = "Nouvelle réponse à la question \"#{ActivityNotificationMailer.stripped_snippet(followable.title, 40)}\""
                  follow_class_name = "questions"
                when "hi"   
                  subject = "प्रश्न \"#{ActivityNotificationMailer.stripped_snippet(followable.title, 40)}\" का नया उत्तर"
                  follow_class_name = "प्रश्नों"
                when "pt-br"   
                  subject = "Nova resposta para pergunta \"#{ActivityNotificationMailer.stripped_snippet(followable.title, 40)}\""
                  follow_class_name =  "perguntas"
                else
                  subject = "New answer to Question \"#
                  {ActivityNotificationMailer.stripped_snippet(followable.title, 40)}\""
                  follow_class_name = "questions"
                end
            end
          end

        elsif followable.class.name == 'Event'
          activity = 'Comment'
          subject = "New comment on Event \"#{ActivityNotificationMailer.stripped_snippet(followable.name, 40)}\""
        elsif followable.class.name == 'Announcement'
          activity = 'Comment'
          subject = "New comment on Post \"#{ActivityNotificationMailer.stripped_snippet(followable.title, 40)}\""
        end

        if followable.group.instance_of?(PrivateGroup)
          client_name = followable.group.resource_group.app_name
        else
          client_name = followable.group.app_name
        end

        object = activity.constantize.find_by(id: activity_id)
        attachments = []
        if object
          files = object.file_resources
          unless files.empty?
            attachments = generate_attachments_object(files)
          end
        end

        global_merge_vars = [
          {"name" => "follow_class_name", "content" => follow_class_name },
          {"name" => "followable_group_name", "content" => followable.group.name },
          {"name" => "followable_group_client_name", "content" => client_name},
          {"name" => "activity_snippet", "content" => snippet},
          {"name" => "activity", "content" => activity},
          {"name" => "link_url", "content" => @url},
          {"name" => "show_app_download", "content" => followable.group.organization.get_download_app},
          {"name" => "org_logo", "content" => get_co_branding_logo(followable.group.organization) }
        ]

        global_merge_vars, subject = get_org_custom_config(followable.group.organization, global_merge_vars, subject)
        message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: template_name,
                                      subject: subject,
                                      from_email: "admin@edcast.com",
                                      from_name: 'EdCast Notification',
                                      to: user_formatted_email_addresses_per_api(_users),
                                      attachments: attachments,
                                      mailer: @mailer,
                                      merge_vars: recipient_vars(followable, _users),
                                      global_merge_vars: global_merge_vars)

        send_api_email(message: message, mailer: @mailer)
      end
    end
  end


  private

  def recipient_vars(followable, users)
      merge_vars = []

      users.each do |user|
        follow_id = Follow.where(followable: followable, user: user).first.id
        unsubscribe_token = CGI::escape(ActiveSupport::MessageEncryptor.
                                            new(Edcast::Application.config.secret_key_base).
                                            encrypt_and_sign(follow_id))

        # platform host is set to localhost:3000 in the config/settings.yml
        # file.
        host = Settings.platform_host

        @unsubscribe_url = unsubscribe_url(host: host, token: unsubscribe_token)
        user_vars = {
          "rcpt": user.email,
          "vars": [
            {
              "name": "first_name",
              "content": user.first_name
            },
            {
              "name": "last_name",
              "content": user.last_name
            },
            {
              "name": "name",
              "content": user.name
            },
            {
              "name": "unsubscribe_url",
              "content": @unsubscribe_url
            }
          ]
        }

        merge_vars << user_vars
      end
      merge_vars
  end
end

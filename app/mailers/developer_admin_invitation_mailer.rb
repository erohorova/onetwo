class DeveloperAdminInvitationMailer < MandrillMailer

  def invite(invitation_id)
    invite_sync(DeveloperAdminInvitation.find(invitation_id))
  end

  def invite_sync(invitation)
    @invitation = invitation
    first_name = @invitation.first_name ? @invitation.first_name : ''
    invite_url = dev_join_url(token: @invitation.token)
    to_name = "#{[@invitation.first_name, @invitation.last_name].compact.join(' ')}"

    @invitation.sent_at = Time.now
    @invitation.save

    global_merge_vars =  [
        { "name" => "first_name", "content" => first_name },
        { "name" => "invite_url", "content" => invite_url },
        { "name" => "sender_name", "content" => @invitation.sender.name },
        { "name" => "client_name", "content" => @invitation.client.name }
    ]

    subject = "Invitation to Manage #{@invitation.client.name} on EdCast"
    global_merge_vars, subject = get_org_custom_config(@invitation.sender.organization, global_merge_vars, subject)
    template = 'dev_invite_email.html.slim'
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: template,
                                        to: [{"email" => @invitation.recipient_email, "name" => to_name, "type"=> 'to'}],
                                        from_email: "admin@edcast.com",
                                        from_name: "EdCast Dev Team",
                                        subject: subject,
                                        global_merge_vars: global_merge_vars,
                                        mailer: @invitation.sender.organization.mailer_config
                                        )
    send_api_email(message: message)
  end
end


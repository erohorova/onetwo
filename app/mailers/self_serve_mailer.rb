class SelfServeMailer < MandrillMailer

  def notify_staff(org, admin_user, phone_number)
    global_merge_vars = get_email_vars(org, admin_user, phone_number)
    subject = "EFT request: {{first_name}} {{last_name}} for {{organization_name}}"
    global_merge_vars, subject = get_org_custom_config(org, global_merge_vars, subject)
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'edcast-self-serve-to-staff-v2.html.slim',
                                    subject: subject,
                                    from_email: 'admin@edcast.com',
                                    from_name: 'EdCast',
                                    to: [{"email" => Settings.self_serve_mailing_list, "type"=> 'to'}],
                                    mailer: org.mailer_config,
                                    global_merge_vars: global_merge_vars)

    send_api_email(message: message)
  end

  def notify_org_admin(organization, admin_user)
    global_merge_vars = get_email_vars(organization, admin_user, nil)
    subject = "Welcome to your EdCast team!"
    global_merge_vars, subject = get_org_custom_config(organization, global_merge_vars, subject)
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'edcast-self-serve-to-customer-v3.html.slim',
                                    subject: subject,
                                    from_email: 'admin@edcast.com',
                                    from_name: 'EdCast',
                                    to: [{"email" => Settings.self_serve_mailing_list, "type"=> 'to'}],
                                    mailer: organization.mailer_config,
                                    global_merge_vars: global_merge_vars)

    send_api_email(message: message)
  end

  private
  def get_email_vars(org, admin_user, phone_number)
    [
      {"name" => "first_name", "content" => admin_user.first_name},
      {"name" => "last_name", "content" => admin_user.last_name},
      {"name" => "organization_name", "content" => org.name},
      {"name" => "industry", "content" => org.industries.map(&:title).join("|")},
      {"name" => "host_name", "content" => org.host_name},
      {"name" => "team_url", "content" => org.home_page},
      {"name" => "customer_phone_number", "content" => phone_number},
      {"name" => "show_app_download", "content" => org.get_download_app},
      {"name" => "customer_email", "content" => admin_user.email} #send over work email as customer_email. can't use email since mandrill treats it as a reserved variable
    ]
  end
end
# frozen_string_literal: true

require 'nokogiri'

class TestTemplateMailer < MandrillMailer
  def send_custom_template(user_id:, template_content:, title: '')
    template, dynamic_variables = custom_template_data(template_content)
    return unless template

    user = User.find_by(id: user_id)
    return unless user&.email

    global_merge_vars = dynamic_variables.map do |name|
      followers_ary = [
        {name: 'first_user', picture: 'picture_url', url: 'first_user_url'},
        {name: 'second_user', picture: 'picture_url2', url: 'second_user_url'}
      ]
      {name: name.to_s, content: name == 'followers' ? followers_ary : name.to_s}
    end

    subject = "Test email template #{title}"

    message = MandrillMessageGenerationService.new.merge_custom_attrs(
      test_email: true,
      html_file: template,
      subject: subject,
      from_email: 'admin@edcast.com',
      from_name: 'EdCast template preview',
      to: user_formatted_email_addresses_per_api(user),
      mailer: user.organization.mailer_config,
      global_merge_vars: global_merge_vars
    )

    send_api_email(message: message, mailer: user.organization.mailer_config)
  end

  private

  def custom_template_data(content)
    # find template
    return unless content
    doc = Nokogiri::HTML::DocumentFragment.parse(content)
    extracted_variables = []
    # extract dynamic variables from text nodes
    # since variables will be only present in text nodes for custom templates
    doc.traverse do |node|
      next unless node.text?
      # extract text in double curly brackets from text node
      match = node.content[/{{(.*?)}}/, 1]
      extracted_variables.push(match) if match
    end

    # sanitize iterators names
    #  e.g {{#followers}} <...> {{/followers}}
    extracted_variables.map!{ |i| i.gsub(/^[#|\/]/, '')}.uniq
    # return template and extracted variables from html
    [content, extracted_variables]
  end
end

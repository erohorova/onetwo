class OrganizationMailer < MandrillMailer
  def reminder_mail_to_add_users(org, admin)
    user_name = admin.name ? admin.name : ''

    global_merge_vars = [
          {"name" => "user_name", "content" => user_name},
          {"name" => "team_url", "content" => org.home_page},
          {"name" => "forgot_password_url", "content" => new_user_password_url(host: org.host)}
        ]

    subject = "Invite team members to join EdCast"
    global_merge_vars, subject = get_org_custom_config(org, global_merge_vars, subject)

    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'admin-reminder-mail.html.slim',
              subject: subject,
              from_email: 'admin@edcast.com',
              from_name: 'EdCast',
              to: user_formatted_email_addresses_per_api(admin),
              mailer: org.mailer_config,
              global_merge_vars: global_merge_vars)

    send_api_email(message: message) 
  end
end
class NewTeamRequestMailer < MandrillMailer

  def notify_staff(user)    
    global_merge_vars = get_email_vars(user)

    subject = "New Team Request from #{user.first_name} #{user.last_name}"
    global_merge_vars, subject = get_org_custom_config(user.organization, global_merge_vars, subject)

    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'edcast-new-team-request-to-staff.html.slim',
              subject: subject,
              from_email: 'admin@edcast.com',
              from_name: 'EdCast',
              to: [{"email" =>  Settings.new_team_request_mailing_list, "type"=> 'to'}],
              mailer: user.organization.mailer_config,
              global_merge_vars: global_merge_vars)

    send_api_email(message: message)
  end

  private
  def get_email_vars(user)
    [
      {"name" => "first_name", "content" => user.first_name},
      {"name" => "last_name", "content" => user.last_name},
      {"name" => "show_app_download", "content" => user.organization.get_download_app},
      {"name" => "customer_email", "content" => user.email}
    ]
  end
end
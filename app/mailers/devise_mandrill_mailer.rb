class DeviseMandrillMailer < MandrillMailer

  def confirmation_instructions(user_data, token, opts={})
    to_email = user_data['email']
    return if to_email.nil?
    if token.nil?
      log.warn "Trying to send devise confirmation email to email: #{to_email} without confirmation token" and return
    end
    org = Organization.find(user_data['organization_id'])
    org_logo = get_co_branding_logo(org)

    url_params = {confirmation_token: token, host: org.host, format: 'html'}

    if user_data['platform'] == 'ios' || user_data['platform'] == 'android'
      url_params.merge!({app_redirect: true})
    end

    url = user_confirmation_url(url_params)
    if org.okta_enabled?
      details = user_data.merge!('redirect_url' => url)
      url     = User.url_to_federated_service(details: details, organization: org)
    end

    first_name = user_data['first_name'] ? (user_data['first_name']) : ''
    template = "edcast-confirmation-instruction-#{user_data['platform']}.html.slim"
    subject = "[EdCast] Welcome - just one more step!"


    global_merge_vars = [
        { "name" => "link_url", "content" =>  url },
        { "name" => "org_logo", "content" => org_logo },
        {"name" => "show_app_download", "content" => org.get_download_app},
        { "name" => "first_name", "content" => first_name }
    ]
    user = org.users.find_by(email: user_data['email'])
    global_merge_vars, subject = get_org_custom_config(org, global_merge_vars, subject)
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: template,
                      current_user: user,
                      to: [format_email_address_for_api(user_data['email'], user_data['name'])],
                      subject: subject,
                      global_merge_vars: global_merge_vars,
                      from_email: "admin@edcast.com",
                      from_name: '[EdCast]',
                      mailer: org.mailer_config)

    send_api_email(message: message)
  end

  def reset_password_instructions(user_data, token, opts={})
    org = Organization.find_by(id: user_data['organization_id'])
    org_logo = get_co_branding_logo(org)

    url = "https://#{org.host}/forgot_password/confirm/#{token}"
    first_name = user_data['first_name'] || ''
    subject = "[EdCast] Reset your password"
    template = 'edcast-reset-password-request.html.slim'

    global_merge_vars = [
      { "name" => "link_url", "content" => url },
      { "name" => "org_logo", "content" => org_logo },
      { "name" => "first_name", "content" => first_name }
    ]
    user = org.users.find_by(email: user_data['email'])
    global_merge_vars, subject = get_org_custom_config(org, global_merge_vars, subject)
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: template,
                      current_user: user,
                      to: [format_email_address_for_api(user_data['email'], user_data['name'])],
                      subject: subject,
                      global_merge_vars: global_merge_vars,
                      from_email: "admin@edcast.com",
                      from_name: '[EdCast]',
                      mailer: org.mailer_config)
    send_api_email(message: message)
  end

  #user_id
  #provider is email, facebook, google, and linkedin
  #platform is web, ios, or android
  def send_after_signup_email(user_id, platform, provider)
    user = User.find(user_id)
    org = user.organization
    org_logo = get_co_branding_logo(org)

    return if user.mailable_email.nil?

    first_name = user.first_name ? user.first_name : ''
    subject = 'Welcome to EdCast!'
    template = "edcast-sign-up-successful-#{platform}.html" #edcast-sign-up-successful-web

    global_merge_vars = [
      { "name" => "first_name", "content" => first_name },
      { "name" => "org_logo", "content" => org_logo }
    ]

    global_merge_vars, subject = get_org_custom_config(org, global_merge_vars, subject)
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: template,
                      to: [format_email_address_for_api(user.email, '')],
                      subject: subject,
                      global_merge_vars: global_merge_vars,
                      from_email: "admin@edcast.com",
                      from_name: '[EdCast]',
                      mailer: org.mailer_config)

    send_api_email(message: message)
  end

  def send_import_user_welcome_email(user_id:, redirect_url: nil)
    user = User.find(user_id)
    org = user.organization
    org_logo = get_co_branding_logo(org)

    return if user.mailable_email.nil?

    first_name = user.first_name ? user.first_name : ''
    subject = "Welcome to #{org.name} on EdCast!"
    template = "admin-bulk-import-welcome.html.slim"

    global_merge_vars = [
        { "name" => "org_logo", "content" => org_logo },
        { "name" => "first_name", "content" => first_name },
        { "name" => "subject", "content" => subject },
        { "name" => "team_name", "content" => org.name },
        { "name" => "org_login_link", "content" => redirect_url }
    ]

    global_merge_vars, subject = get_org_custom_config(org, global_merge_vars, subject)
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: template,
                      current_user: user,
                      subject: subject,
                      to: user_formatted_email_addresses_per_api(user),
                      global_merge_vars: global_merge_vars,
                      from_email: "admin@edcast.com",
                      from_name: "EdCast",
                      mailer: org.mailer_config)

    send_api_email(message: message)
  end
end

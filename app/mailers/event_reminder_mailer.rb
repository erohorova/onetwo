class EventReminderMailer < MandrillMailer
  default "X-MC-PreserveRecipients" => "false"
  def remind_users(event:nil, users:nil)
    global_merge_vars = [
      {"name" => "event_name", "content" => event.name },
      {"name" => "event_description", "content" => event.description }
    ]
    if event.group
      global_merge_vars += [
          {"name" => "group_website_url", "content" => event.group.website_url },
          {"name" => "group_description" , "content" => event.group.description }
      ]
    end

    valid_users = valid_users_email(users: users.to_a)
    return if valid_users.empty? || skip_email_notifcations?(event.group.organization)

    org_logo = get_co_branding_logo(event.group.organization)

    global_merge_vars += [
      { "name" => "org_logo", "content" => org_logo }
    ]
    @mailer = event.group.mailer_config
    extension = @mailer ? '.html' : '.html.slim'
    template = 'events-start-notification' + extension
    subject = "#{event.name} event starts soon"
    global_merge_vars, subject = get_org_custom_config(event.group.organization, global_merge_vars, subject)
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: template,
                                            to: user_formatted_email_addresses_per_api(valid_users),
                                            subject: subject,
                                            merge_vars: merge_vars(event, users),
                                            from_email: "admin@edcast.com",
                                            from_name: '[EdCast]',
                                            global_merge_vars: global_merge_vars,
                                            mailer: @mailer)

    send_api_email(message: message, mailer: @mailer)
  end

  private
    def user_formatted_email_addresses(users)
      users.map do |user|
        "#{user.name} <#{user.mailable_email}>"
      end
    end

    def merge_vars(event, users)
      merge_vars = []
      users.each do |user|

        user_vars = {
          "rcpt": user.email,
          "vars": [
            {
              "name": "first_name",
              "content": user.first_name
            },
            {
              "name": "last_name",
              "content": user.last_name
            },
            {
              "name": "name",
              "content": user.name
            },
            {
              "name": "rsvp_status",
              "content": event.user_rsvp(user).rsvp
            }
          ]
        }
      merge_vars << user_vars
      end
      merge_vars
    end
end

class BulkImportUserDetailsMailer < MandrillMailer

  def notify_admin(opts = {})

    admin = opts[:admin]

    if admin
      global_merge_vars = [
          { 'name' => 'total_users_count', 'content' => opts[:count][:total_users_count] },
          { 'name' => 'new_users_count', 'content' => opts[:count][:new_users_count] },
          { 'name' => 'old_users_count', 'content' => opts[:count][:old_users_count] },
          { 'name' => 'resent_emails_count', 'content' => opts[:count][:resent_emails_count] },
          { 'name' => 'invitees_count', 'content' => opts[:count][:invitees_count] },
          { 'name' => 'failed_users_count', 'content' => opts[:count][:failed_users_count] },
          { 'name' => 'channels_names_list', 'content' => opts[:channels_names_list] },
          { 'name' => 'new_groups_count', 'content' => opts[:new_groups].count },
          { 'name' => 'new_groups', 'content' => opts[:new_groups] },
          { 'name' => 'old_groups', 'content' => opts[:old_groups] },
          { 'name' => 'upload_time', 'content' => opts[:upload_time] && opts[:upload_time].strftime('%b %e, %l:%M %p') },
          { 'name' => 'first_name', 'content' => admin.first_name.try(:titleize) },
          { 'name' => 'show_app_download', 'content' => admin.organization.get_download_app},
          { 'name' => 'org_logo', 'content' => get_co_branding_logo(admin.organization) }
      ]

      template = 'edcast-bulk-import-admin' + '.html.slim'
      subject = 'Bulk Import User Details'
      global_merge_vars, subject = get_org_custom_config(admin.organization, global_merge_vars, subject)
      # current_user: used to determine proper custom template and language preference
      message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: template,
                                          current_user: admin,
                                          from_email: 'admin@edcast.com',
                                          from_name: 'EdCast Admin',
                                          to: user_formatted_email_addresses_per_api(admin),
                                          subject: subject,
                                          global_merge_vars: global_merge_vars,
                                          mailer: admin.organization.mailer_config)

      send_api_email(message: message)
    end
  end
end

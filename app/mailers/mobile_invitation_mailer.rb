class MobileInvitationMailer < MandrillMailer

  def invite(invitation_id)
    invite_sync(MobileInvitation.find(invitation_id))
  end

  def invite_sync(invitation)
    @invitation = invitation
    user = User.find_by(email: @invitation.recipient_email)
    org = user&.organization
    org_logo = org.co_branding_logo.present? ? org.co_branding_logo.url : nil
    first_name = @invitation.first_name ? @invitation.first_name : ''
    invite_url = ""

    to_name = "#{[@invitation.first_name, @invitation.last_name].compact.join(' ')}"

    @invitation.sent_at = Time.now
    @invitation.save

    global_merge_vars = [
          {"name" => "org_logo", "content" => org_logo},
          {"name" => "first_name", "content" => first_name},
          {"name" => "show_app_download", "content" => org.get_download_app},
          {"name" => "invite_url", "content" => invite_url}
        ]

    subject = "[EdCast] Sign in and get started"
    global_merge_vars, subject = get_org_custom_config(org, global_merge_vars, subject)
    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'invite-email.html.slim',
              current_user: user,
              subject: subject,
              from_email: 'admin@edcast.com',
              from_name: 'EdCast',
              to: [{"email" => @invitation.recipient_email, "name"=> to_name, "type"=> 'to'}],
              mailer: org.mailer_config,
              global_merge_vars: global_merge_vars)

    send_api_email(message: message)
  end
end

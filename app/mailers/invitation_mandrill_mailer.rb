class InvitationMandrillMailer < MandrillMailer

  #Confirmation email of uploaded CSV is processed or failed
  def import_confirmation(user, message, invitable)
    first_name = user.first_name || ' '

    org = invitable.is_a?(Organization) ? invitable : invitable.organization
    org_logo = get_co_branding_logo(org)

    global_merge_vars = [
          {"name" => "org_logo", "content" => org_logo},
          {"name" => "first_name", "content" => first_name},
          {"name" => "show_app_download", "content" => org.get_download_app},
          {"name" => "message", "content" => message}
        ]

    subject = "Uploaded CSV processed"
    global_merge_vars, subject = get_org_custom_config(org, global_merge_vars, subject)

    mandrill_message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: 'import_confirmation.html.slim',
              subject: subject,
              from_email: 'admin@edcast.com',
              from_name: 'CSV Processed',
              to: user_formatted_email_addresses_per_api(user),
              mailer: org.mailer_config,
              global_merge_vars: global_merge_vars)

    send_api_email(message: mandrill_message)
  end

  def invite(invitation_data)
    inv = Invitation.find(invitation_data['id'])
    host = nil
    organization = inv.invitable.is_a?(Organization) ? inv.invitable : inv.invitable.organization
    user = organization.users.find_by(email: invitation_data['email'])
    # Below #if is deprecated and dont think so if this is used now.
    if inv.invitable.is_a?(Organization)
      join_pars = {token: invitation_data['token'], host: inv.invitable.host}
      web_join_url = join_url(join_pars)
      host = inv.invitable.host
      org_logo = get_co_branding_logo(inv.invitable)
      show_app_download = inv.invitable.get_download_app

      branchmetrics_payload = {
        '$deeplink_path': 'join_organization',
        # mobile will request join_url after app launch/install via branch
        'join_url': join_url(join_pars.merge(format: :json)),
        '$desktop_url': web_join_url,
        'organization': {id: inv.invitable.id, name: inv.invitable.name, host_name: inv.invitable.host_name,
                          hostName: inv.invitable.host_name, photo: inv.invitable.mobile_photo},
        'sender': {id: inv.sender.id, name: inv.sender.name, photo: inv.sender.photo}
      }

      # with fallback in case branch fails for whatever reason

      invite_url = BranchmetricsApi.generate_link(branchmetrics_payload, organization: organization) || web_join_url
    elsif inv.invitable.is_a?(Team)
      return if organization.send_no_mails?
      org_logo = get_co_branding_logo(inv.invitable.organization)
      show_app_download = inv.invitable.organization.get_download_app
      join_pars = {token: invitation_data['token'], host: inv.invitable.organization.host}
      web_join_url = join_url(join_pars)
      branchmetrics_payload = {
        '$deeplink_path': 'join_team',
        # mobile will request join_url after app launch/install via branch
        'join_url': join_url(join_pars.merge(format: :json)),
        '$desktop_url': web_join_url,
        'team': {id: inv.invitable.id, name: inv.invitable.name},
        'sender': {id: inv.sender.id, name: inv.sender.name, photo: inv.sender.photo}
      }
      invite_url = BranchmetricsApi.generate_link(branchmetrics_payload, organization: organization) || web_join_url
      auto_assign_content = inv.invitable.auto_assign_content
      team_assignments_present = inv.invitable.assignments.where('(due_at IS NULL or due_at >= ?)', Date.today).exists?
      show_assignment_msg = auto_assign_content && team_assignments_present
    else
      show_app_download = inv.invitable.organization.get_download_app
      invite_url = join_url(token: invitation_data['token'])
    end

    mandrill_template_name = nil
    subject = nil
    global_merge_vars = [
          {"name" => "host", "content" => host },
          {"name" => "invite_url", "content" => invite_url},
          {"name" => "org_logo", "content" => org_logo},
          {"name" => "first_name", "content" => invitation_data['first_name'] || invitation_data['recipient_name'] || '' },
          {"name" => "recipient_email", "content" => invitation_data['email']},
          {"name" => "inviter_name", "content" => invitation_data['sender_name']},
          {"name" => "invitable_name", "content" => invitation_data['invitable_name']},
          {"name" => "show_app_download", "content" => show_app_download},
          {"name" => "invitable_photo", "content" => invitation_data['invitable_photo']},
          {"name" => "show_assignment_msg", "content" => show_assignment_msg}
        ]

    case invitation_data['invitable_type']
      when 'Group'
        mandrill_template_name = 'edcast-topic-admin-invite.html.slim'
        subject = "[EdCast] Start sharing your Insights with your audience"
      when 'Organization'
        mandrill_template_name = "edcast-organization-invite-#{invitation_data['roles']}.html.slim"
        if invitation_data['roles'] == 'admin'
          subject = "[EdCast] Welcome to EdCast"
        elsif invitation_data['roles'] == 'member'
          subject = "Invitation to join #{invitation_data['invitable_name']} on EdCast"
        end
      when 'Team'
        global_merge_vars << {"name" => "sender_role", "content" => get_sender_role(invitation_data['roles'])}
        mandrill_template_name = "edcast-team-invite.html.slim"
        subject = "Invitation to join #{invitation_data['invitable_name']} on EdCast"
    end

    global_merge_vars, subject = get_org_custom_config(organization, global_merge_vars, subject)

    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: mandrill_template_name,
              organization: organization,
              current_user: user,
              subject: subject,
              from_email: "admin@edcast.com",
              from_name: "EdCast Team",
              mailer: organization.mailer_config,
              to: [{"email" => invitation_data['email'], "name"=> invitation_data['first_name'], "type"=> 'to'}],
              global_merge_vars: global_merge_vars)

    send_api_email(message: message)
  end

  def get_sender_role(role)
    case role
    when 'admin'
      'leader'
    when 'sub_admin'
      'admin'
    else
      role.gsub(/([_@#!%()\-=;><,{}\~\[\]\.\/\?\"\*\^\$\+\-]+)/, ' ')
    end
  end

  def send_welcome_email(invitation_id)
    invitation = Invitation.find(invitation_id)

    organization = invitation.invitable
    org_logo = get_co_branding_logo(organization)

    invite_url = invitation.branchmetrics_url

    first_name = invitation.recipient_name
    subject = "Welcome to #{organization.name} on EdCast!"
    template = 'admin-bulk-import-welcome.html.slim'

    global_merge_vars = [
        { 'name' => 'org_logo', 'content' => org_logo },
        { 'name' => 'first_name', 'content' => first_name },
        { 'name' => 'subject', 'content' => subject },
        { 'name' => 'team_name', 'content' => organization.name },
        { 'name' => 'org_login_link', 'content' => invite_url }
    ]

    global_merge_vars, subject = get_org_custom_config(organization, global_merge_vars, subject)
    message = MandrillMessageGenerationService.new.merge_custom_attrs(
      organization: organization,
      html_file: template,
      subject:   subject,
      to:        [format_email_address_for_api(invitation.recipient_email, first_name)],
      global_merge_vars: global_merge_vars,
      from_email: 'admin@edcast.com',
      from_name: 'EdCast',
      mailer: organization.mailer_config
    )

    send_api_email(message: message)
  end
end

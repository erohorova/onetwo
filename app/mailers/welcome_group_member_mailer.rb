class WelcomeGroupMemberMailer < MandrillMailer
  #group_id: nil, to_user_id: nil, from_user_id: nil
  def welcome_group_member_email(opts ={})
    group = PrivateGroup.find(opts[:group_id])
    return if group && group.organization && skip_email_notifcations?(group.organization)
    to_user = User.find(opts[:to_user_id])
    from_user = User.find(opts[:from_user_id])
    parent_group = group.resource_group
    org_logo = get_co_branding_logo(group.organization)
    global_merge_vars = [
      {"name" => "group_name", "content" => group.name},
      {"name" => "parent_group_name", "content" => parent_group.name},
      {"name" => "from_user_name", "content" => from_user.name},
      {"name" => "org_logo", "content" => org_logo}
    ]

    if !to_user.mailable_email.blank? && !to_user.pref_group_opt_out?(parent_group, 'notification.group_welcome')
      redirect_url = UrlBuilder::build(parent_group.website_url, {})
      @url = UrlBuilder::build((Settings.platform_force_ssl == "true") ? 'https://' : 'http://' + Settings.platform_host + '/redirect', {'url' => redirect_url})
      global_merge_vars << {"name" => "parent_group_url", "content" => @url}

      subject = "Welcome to #{group.name}"
      global_merge_vars, subject = get_org_custom_config(group.organization, global_merge_vars, subject)

      @mailer = group.mailer_config || parent_group.mailer_config
      extension = @mailer ? '.html' : '.html.slim'
      template = to_user == from_user ? 'join-group-member-notification'+ extension : 'welcome-group-member-notification' + extension

      message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: template,
                                    subject: subject,
                                    from_email: 'admin@edcast.com',
                                    from_name: 'EdCast Forum',
                                    to: user_formatted_email_addresses_per_api(to_user),
                                    merge_vars: recipient_vars(to_user,parent_group),
                                    global_merge_vars: global_merge_vars,
                                    mailer: @mailer)

      send_api_email(message: message, mailer: @mailer)
    end
  end

  private

    def recipient_vars(user,parent_group)
      vars = []
      preference_token = ActiveSupport::MessageEncryptor.
                                         new(Edcast::Application.config.secret_key_base).
                                         encrypt_and_sign(ActiveSupport::JSON.encode({user_id: user.id,
                                                                                      key: 'notification.group_welcome.opt_out',
                                                                                      value: 'true',
                                                                                      preferenceable_type: 'Group',
                                                                                      preferenceable_id: parent_group.id})).gsub('=', '_')

      # platform host is set to localhost:3000 in the config/settings.yml
      # file.
      host = Settings.platform_host

      @unsubscribe_url = preferences_url(host: host, token: preference_token)
      vars << {"rcpt"=> user.mailable_email,
            "vars"=>[
              {"name" => "first_name", "content" => user.first_name},
              {"name" => "last_name", "content" => user.last_name},
              {"name" => "name", "content" => user.name},
              {"name" => "unsubscribe_url", "content" => @unsubscribe_url}
            ]
      }
      vars
    end
end
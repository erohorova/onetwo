class AnnouncementMailer < MandrillMailer
  default "X-MC-PreserveRecipients" => "false"
  def announcement_email(post: nil, users: [])
    #reject people without email address or who opted out
    _users = valid_users_email(users: users, group: post.group, key: 'notification.announcement')

    if _users.blank? || post.group.organization.nil? || post.group.organization.send_no_mails?
      return
    end

    global_merge_vars = [
                  {"name" => "post_title", "content" => post.title},
                  {"name" => "post_message", "content" => post.message}
    ]
    if post.group
      global_merge_vars += [
          {"name" => "post_url", "content" => Post.url(post.attributes)},
          {"name" => "post_group_name" , "content" => post.group.name },
          {"name" => "show_app_download", "content" => post.group.organization.get_download_app}
      ]
    end

    if post.user
      global_merge_vars += [
        {"name" => "post_user", "content" => post.user.name },
        {"name" => "org_logo", "content" => get_co_branding_logo(post.postable.organization) }
      ]
    end

    attachments = []
    if post
      files = post.file_resources
      unless files.empty?
        attachments = generate_attachments_object(files)
      end
    end

    @mailer = post.group.mailer_config
    extension = @mailer ? '.html' : '.html.slim'
    template = 'group-announcement' + extension
    subject = "New Forum Announcement from #{post.user.name}"

    global_merge_vars, subject = get_org_custom_config(post.group.organization, global_merge_vars, subject)

    message = MandrillMessageGenerationService.new.merge_custom_attrs(html_file: template,
                                    subject: subject,
                                    from_email: "admin@edcast.com",
                                    from_name: "EdCast Announcement",
                                    to: user_formatted_email_addresses_per_api(_users),
                                    merge_vars: recipient_vars(_users,post),
                                    attachments: attachments,
                                    mailer: @mailer,
                                    global_merge_vars: global_merge_vars)

    send_api_email(message: message, mailer: @mailer)

  end


  private

    def recipient_vars(users,post)
      merge_vars = []
      users.each do |user|
        preference_token = ActiveSupport::MessageEncryptor.
                                             new(Edcast::Application.config.secret_key_base).
                                             encrypt_and_sign(ActiveSupport::JSON.encode({user_id: user.id,
                                                                                          key: 'notification.announcement.opt_out',
                                                                                          value: 'true', preferenceable_type: 'Group',
                                                                                          preferenceable_id: post.group.id})).gsub('=', '_')

        # platform host is set to localhost:3000 in the config/settings.yml
        # file.
        host = Settings.platform_host

        unsubscribe_url = preferences_url(host: host, token: preference_token)

        user_vars = {
          "rcpt": user.email,
          "vars": [
            {
              "name": "first_name",
              "content": user.first_name
            },
            {
              "name": "last_name",
              "content": user.last_name
            },
            {
              "name": "name",
              "content": user.name
            },
            {
              "name": "unsubscribe_url",
              "content": unsubscribe_url
            }
          ]
        }

        merge_vars << user_vars
      end
      merge_vars
    end
end
 
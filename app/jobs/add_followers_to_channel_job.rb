class AddFollowersToChannelJob < EdcastActiveJob
  queue_as :channel

  # Adds all users in the org to channel
  def perform(channel)
    channel.organization.users.visible_members.find_each do |follower|
      unless channel.is_follower?(follower)
        begin
          channel.followers << follower
        rescue ActiveRecord::RecordInvalid
          next
        end
      end
    end
  end
end

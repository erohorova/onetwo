class TranscoderCompleteJob < EdcastActiveJob
  queue_as :video_stream

  def perform(recording, raw_sns_message)
    message_payload = ActiveSupport::JSON.decode(raw_sns_message)
    recording.transcoding_status = 'available'
    recording.last_aws_transcoding_message = raw_sns_message
    recording.hls_location = message_payload['outputKeyPrefix'] + message_payload['outputs'][0]['key'] + '.m3u8'
    recording.mp4_location = message_payload['outputKeyPrefix'] + message_payload['outputs'][1]['key']

    #check to see if the master playlist exists
    if message_payload['playlists'].present? && message_payload['playlists'].first
      recording.hls_location = message_payload['outputKeyPrefix'] + message_payload['playlists'].first['name'] + '.m3u8'
    else #fallback to the old hls output
      recording.hls_location = message_payload['outputKeyPrefix'] + message_payload['outputs'][0]['key'] + '.m3u8'
    end


    thumbnail_pattern = message_payload['outputs'].first['thumbnailPattern']
    has_thumbnails = thumbnail_pattern.present?
    if has_thumbnails #older notification message won't have this field. backward compatible.
      region = Settings.transcoder_region
      if region.blank?
        raise 'AWS region is missing'
      end
      s3_client = AWS::S3::Client.new(region: region)
      prefix = message_payload['outputKeyPrefix'] + thumbnail_pattern.split('{count}').first
      response = s3_client.list_objects(bucket_name: recording.bucket, prefix:prefix)
      thumbnails_count = response.data[:contents].length
      if thumbnails_count > 0
        #delete all the existing thumbnail
        Thumbnail.delete_all(recording_id: recording.id)
      end
      response.data[:contents].each_with_index do |content, i|
        is_default = thumbnails_count/2 == i
        recording.thumbnails.build(uri: "https://#{Settings.wowza.s3_bucket}.s3.amazonaws.com/#{content[:key]}",
                                   sequence_number: i, is_default: is_default)
      end
    end

    recording.save!
    recording.mark_default!
    recording.video_stream.reindex
  end
end

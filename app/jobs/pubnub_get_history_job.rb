class PubnubGetHistoryJob < EdcastActiveJob
  queue_as :analytics

  def perform(stream_id)

    stream = VideoStream.find_by(id: stream_id)
    return if stream.nil?

    channel = stream.uuid
    pubnub = Pubnub.new(subscribe_key: Settings.pubnub.subscribe_key,
                        publish_key: Settings.pubnub.publish_key)

    anon_device_ids = {}
    stream.anon_watchers_count = 0

    current_end = nil
    while(1) do
      history = pubnub.history(reverse: true, count: 100, start: current_end, channel: channel, http_sync: true)
      # no more messages
      break if history.empty?

      if history.first.status != 200
        log.error "Pubnub history returned bad response: #{history} for stream: #{stream.id}"
        break
      end

      current_end = history.first.history_end
      
      deletedComment=[]
      history.each do |history_item|
        message = history_item.message
        if message["a"] == 'deletedComment'
          deletedComment = message["dc"]
        end
      end

      history.each do |history_item|
        message = history_item.message
        if (!deletedComment.include? message["msgId"]) && (message["type"] == 'videoStream' && message["a"] == 'comment')
          lc = LiveComment.new(commentable: stream.card, user_id: message["uid"], message: message["m"])
          if lc.save
            MetricRecorderJob.perform_later(lc.user_id, "comment", {object: 'video_stream', object_id: stream.id, platform: message['platform']})
          else
            log.error "Saving live stream comment failed for message: #{message} for video stream id: #{stream.id}"
          end
        elsif message["type"] == 'videoStream' && message["a"] == 'join'
          VideoStreamsUser.create(video_stream: stream, user_id: message['uid'])
        elsif message["type"] == 'videoStream' && message["a"] == 'anonymous_join'
          if message['deviceId'].present? && !anon_device_ids[message['deviceId']]
            stream.anon_watchers_count += 1
            anon_device_ids[message['deviceId']] = true
          elsif message['deviceId'].blank?
            stream.anon_watchers_count += 1
          end
        elsif message["type"] == 'videoStream' && message["a"] == 'like'
          MetricRecorderJob.perform_later(message['uid'], "like", {object: 'video_stream', object_id: stream.id, platform: message['platform']})
        end
      end
      unless stream.save
        log.warn("Fail to save video_stream_id=#{stream.id} error=#{stream.errors.full_messages}")
      end
    end
  end
end

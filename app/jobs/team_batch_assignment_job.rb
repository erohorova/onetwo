class TeamBatchAssignmentJob < EdcastActiveJob
  queue_as :team

  def perform(team_id:, assignor_id:, assignee_ids:, assignable_id:,
    assignable_type:, opts: {})

    team = Team.find_by(id: team_id)
    team.create_team_assignments_for(
      assignee_ids: assignee_ids,
      assignable_type: assignable_type,
      assignable_id: assignable_id,
      assignor_id: assignor_id,
      opts: opts
    )
  end

end



class UserTeamAssignmentNotificationJob < EdcastActiveJob
  queue_as :notifications

  def perform(opts = {})
    team_assignment = TeamAssignment.where(id: opts[:team_assignment_id]).first
    assignment = team_assignment.assignment

    if assignment.nil?
      log.error "Assignment Notification failed for: #{opts[:team_assignment_id]}"
    else
      causal_user_id = opts[:causal_user_id] || team_assignment.assignor.id
      notification = assignment.create_notification(causal_user_id: causal_user_id, notification_type: opts[:notification_type], message: opts[:message])
      if notification.valid?
        assignment.create_push_notification(notification_type: opts[:notification_type], message: (opts[:message] || notification.message))
      else
        #remove invalid notification
        notification.destroy if notification.persisted?
      end
    end
  end
end

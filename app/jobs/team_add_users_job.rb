class TeamAddUsersJob < EdcastActiveJob
  queue_as :team

  def perform(team_id: , user_ids: [], as_type: 'member', actor_name:)
    team = Team.find_by(id: team_id)
    unless team
      log.warn "Team with id: #{team_id} was not found"
      return
    end
    team.add_to_team(user_ids, as_type: as_type, actor_name: actor_name)
  end

end

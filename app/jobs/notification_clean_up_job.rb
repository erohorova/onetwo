class NotificationCleanUpJob < EdcastActiveJob
  queue_as :notifications

  def perform(opts = {})
    begin
      Notification.where(notifiable_type: opts[:type], notifiable_id: opts[:id]).destroy_all
      NotificationEntry.where(sourceable_type: opts[:type], sourceable_id: opts[:id]).destroy_all
      log.info "Successfully deleted #{opts[:type]} notifications for #{opts[:type]} id: #{opts[:id]}"
    rescue => e
      log.error "Failed to delete #{opts[:type]} notifications, #{opts[:type]} id: #{opts[:id]}, exception: #{e}"
    end
  end
end
class VerifySenderDomainJob < EdcastActiveJob
  queue_as :default
  
  def perform(opts={})
    mailer_config = MailerConfig.find_by(id: opts[:config_id])
    return unless mailer_config.present?

    mandrill = Mandrill::API.new Settings.mandrill.password
    log.info "Checking domain verification on mandrill for domain: #{mailer_config.domain}"
    result = mandrill.senders.check_domain mailer_config.domain
    log.info "Domain verification status on mandrill for domain: #{mailer_config.domain}, result: #{result}"

    if result['valid_signing'] == true
      mailer_config.verification[:domain] = true
      mailer_config.save!
    end
  end
end
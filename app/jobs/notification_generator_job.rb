class NotificationGeneratorJob < EdcastActiveJob
  queue_as :notifications

  #NOTE:
  #item_type should be the model name for which item_id is associated.
  #item_type should not be equal to notification_type
  #adding another tech-debt ticket to refactor it https://jira.edcastcloud.com/browse/EP-9506
  def perform(opts = {})
    item_id = opts[:item_id]
    item_type = opts[:item_type]
    notification_type = opts[:notification_type]
    if item_id.nil? || item_type.nil?
      log.error "notification generator job called with bad opts: #{opts}"
      return
    end
    case item_type
    when 'add_authors'
      channel_invite_notifications(opts, "added_author")
    when 'add_followers'
      channel_invite_notifications(opts, "added_follower")
    when 'group_invite'
      group_invite_notifications(opts)
    when 'clc'
      generate_clc_notification(opts)
    when 'new_card_for_curation'
      generate_new_card_curation_notifications(opts)
    when 'team_card_share'
      generate_team_card_share_notifications(opts)
    when 'user_card_share'
      generate_user_card_share_notifications(opts)
    when "topic_request_approval"
      generate_topic_request_approval_notifications(opts)
    when 'mention'
      mention = Mention.find_by(id: item_id)
      if mention.nil?
        log.warn "Trying to generate mention notification for bad id: #{item_id}"
        return
      end
      mentionable = mention.mentionable

      if(mentionable)
        notification_type, causal_user_id = (mentionable.class == Card) ? [Notification::TYPE_MENTION_IN_CARD, mentionable.author_id]
          : [Notification::TYPE_MENTION_IN_COMMENT, mentionable.user_id]
        user_id = mention.user_id
        unless user_id == causal_user_id
          notification, newly_created = find_or_new_notification(notifiable: mention.mentionable, user_id: mention.user_id, notification_type: notification_type, causal_user_id: causal_user_id)

          if newly_created
            opts = get_pubnub_opts(notification)
            PubnubNotifier.notify(opts)
          end
        end
      end

    when 'new_poll'
      conn = ActiveRecord::Base.connection
      card = Card.find_by(id: item_id)
      if card.nil?
        log.warn "Trying to generate poll notification for bad id: #{item_id}"
        return
      end
      author = User.find_by(id: card.author_id)
      now = Time.now.utc.strftime("%Y-%m-%d %H:%M:%S")
      Follow.where(followable: author).find_in_batches do |follows|
        follows = Follow.array_to_relation(follows)
        follows = follows.includes(:user).select{|follow| card.visible_to_user(follow.user)}
        inserts = []
        follows.each do |follow|
          inserts.push("(#{card.id}, 'Card', #{follow.user_id}, 'new_poll', TRUE, '#{now}', '#{now}', #{card.id}, 'card', #{card.id})");
        end
        unless inserts.empty?
          sql = "INSERT INTO `notifications` (`notifiable_id`, `notifiable_type`, `user_id`, `notification_type`, `unseen`, `created_at`, `updated_at`, `app_deep_link_id`, `app_deep_link_type`, `card_id`) VALUES #{inserts.join(',')}"
          conn.execute sql
        end
      end
      inserted_notification_ids = Notification.where(notifiable: card, notification_type: "new_poll").pluck(:id)
      unless inserted_notification_ids.empty?
        inserts = inserted_notification_ids.map{|i| "(#{i}, #{card.author_id}, FALSE)"}
        sql = "INSERT INTO `notifications_causal_users` (`notification_id`, `user_id`, `anonymous`) VALUES #{inserts.join(',')}"
        conn.execute sql
      end

    when 'poll_answer'
      answer = QuizQuestionAttempt.find_by(id: item_id)
      if answer.nil?
        log.warn "Trying to generate poll answer notification for bad id: #{item_id}"
        return
      end
      card = answer.quiz_question
      unless answer.user.id == card.author_id
        find_or_new_notification(notifiable: card, user_id: card.author_id, notification_type: "poll_answer", causal_user_id: answer.user.id)
      end
    when 'follow'
      follow = Follow.find_by(id: item_id)
      if follow.nil?
        log.warn "Trying to generate follow notification for bad id: #{item_id}"
        return
      elsif follow.skip_notification
        return
      end
      notification_type = 'new_follow'
      n = Notification.where(notifiable: nil, user: follow.followable, notification_type: notification_type, unseen: true).last

      if !n || n.created_at <= (Time.now.utc - 1.day) || n.notifications_causal_users.count >= 10
        n = Notification.new(notifiable: nil,
                             user: follow.followable,
                             notification_type: notification_type)
        unless n.save
          log.error "Follow notification save failed for follow id: #{item_id}"
          return
        end
      end
      unless n.causal_users.include?(follow.user)
        NotificationsCausalUser.create!(notification: n, user: follow.user, anonymous: false)
      end
    when 'video_stream'
      unless ['stream_scheduled', 'stream_started', 'scheduled_stream_reminder'].include?(notification_type)
        log.error "bad notification type for stream id: #{item_id}"
        return
      end
      stream = VideoStream.find_by(id: item_id)
      if stream.nil?
        log.warn "Trying to generate stream notification for bad id: #{item_id}"
        return
      end

      # if already started then dont send any notification for reminder
      if notification_type == 'scheduled_stream_reminder' && (stream.status == "live" || stream.status == "past")
        return
      end

      fk_notif = Notification.new(notifiable: stream.card, notification_type: notification_type)
      fk_notif.causal_users << stream.creator
      opts = {deep_link_id: stream.card.id, deep_link_type: 'card', content: fk_notif.message, item: stream.card, notification_type: notification_type, host_name: stream.creator.organization.host }
      if notification_type != 'scheduled_stream_reminder'
        opts[:excluded_user_ids] = [stream.creator_id]
      end
      PubnubNotifier.notify(opts)

      if notification_type == 'scheduled_stream_reminder'
        VideoStreamBatchNotificationGeneratorJob.perform_later(user_ids: [stream.creator_id], video_stream: stream, notification_type: notification_type)
      else
        # People who are following the stream creator
        if stream.publicly_visible?
          Follow.where(followable: stream.creator).where.not(user_id: stream.creator_id).select(:id, :user_id).find_in_batches do |follows|
            VideoStreamBatchNotificationGeneratorJob.perform_later(user_ids: follows.map(&:user_id), video_stream: stream, notification_type: notification_type)
          end
        end

        # People who are following the channel the stream is a part of
        stream.relevant_channels.each do |ch|
          Follow.where(followable: ch).where.not(user_id: stream.creator_id).select(:id, :user_id).find_in_batches do |follows|
            VideoStreamBatchNotificationGeneratorJob.perform_later(user_ids: follows.map(&:user_id), video_stream: stream, notification_type: notification_type)
          end
        end
      end

    when 'approval'
      item = Approval.find_by(id: item_id)
      if item.nil?
        log.warn "Trying to generate approval notification for bad id: #{item_id}"
        return
      end
      n = Notification.new(notifiable: item.approvable, group: item.approvable.group, user: item.approvable.user, notification_type: 'new_approval')
      unless n.save
        log.error "Approval notification save failed for id: #{item_id}"
        return
      end
      NotificationsCausalUser.create!(notification: n, user: item.approver, anonymous: false)
    when 'vote'
      item = Vote.find_by(id: item_id)
      if item.nil?
        log.warn "Trying to generate upvote notification for bad id: #{item_id}"
        return
      end
      if item.votable_type == 'Comment'
        notification_type = 'new_comment_upvote'
      elsif item.votable_type == 'Card'
        notification_type = 'new_card_upvote'
      else
        notification_type = 'new_post_upvote'
      end
      user_id = item.votable.try(:user_id) || item.votable.try(:author_id)
      response = find_or_new_notification(notifiable: item.votable,
          user_id: user_id, notification_type: notification_type,
          causal_user_id: item.voter.id, group_id: item.votable.group_id)
    when 'curated_card'
      # Deprecated
      channels_card =  ChannelsCard.find_by(id: opts[:item_id])
      opts = {item_id: item_id, notification_type: 'curated_card', notifiable: channels_card.channel, user: channels_card.card.author}
      generate_curation_notifications(opts)
    when 'skipped_card'
      # Deprecated
      channels_card =  ChannelsCard.find_by(id: opts[:item_id])
      # this is temporary fix, as we are going to move web notifcations to new framework
      custom_msg = "The curator of the channel #{channels_card.channel.label}, has rejected your post #{channels_card.card.snippet_html}"
      opts = {item_id: item_id, notification_type: 'skipped_card', notifiable: channels_card.card, user: channels_card.card.author, custom_msg: custom_msg}
      generate_curation_notifications(opts)
    when 'added_to_team'
      add_to_team_notifications(opts)
    when 'announcement'
      item = Announcement.find_by(id: item_id)
      if item.nil?
        log.warn "Trying to generate announcement notification for bad id: #{item_id}"
        return
      end

      notification_type = 'new_announcement'
      app_deep_link_id, app_deep_link_type        = item.get_app_deep_link_id_and_type
      widget_deep_link_id, widget_deep_link_type  = Notification.widget_deep_link_id_and_type(notification_type, 'Post', item.id)

      fk_notif = Notification.new(notifiable: item, notification_type: notification_type)
      fk_notif.causal_users << item.user
      opts = {deep_link_id: item.id, deep_link_type: 'post', content: fk_notif.message, item: item, notification_type: notification_type, excluded_user_ids: [item.user_id], host_name: item.user.organization.host }
      PubnubNotifier.notify(opts)

      now = Time.now.utc.strftime("%Y-%m-%d %H:%M:%S")
      conn = ActiveRecord::Base.connection
      inserts = []
      item.group.users.where.not(id: item.user_id).find_in_batches do |users|
        inserts = []
        users.each do |user|
          inserts.push("(#{item.id}, 'Post', #{item.group_id}, #{user.id}, '#{notification_type}', TRUE, '#{now}', '#{now}', '#{now}', #{widget_deep_link_id.nil? ? 'NULL' : widget_deep_link_id}, #{widget_deep_link_type.nil? ? 'NULL' : "'#{widget_deep_link_type}'"}, #{app_deep_link_id.nil? ? 'NULL' : app_deep_link_id}, #{app_deep_link_type.nil? ? 'NULL' : "'#{app_deep_link_type}'"}, NULL)");
        end
        unless inserts.empty?
          sql = "INSERT INTO `notifications` (`notifiable_id`, `notifiable_type`, `group_id`, `user_id`, `notification_type`, `unseen`, `accepted_at`, `created_at`, `updated_at`, `widget_deep_link_id`, `widget_deep_link_type`, `app_deep_link_id`, `app_deep_link_type`, `card_id`) VALUES #{inserts.join(',')}"
          conn.execute sql
        end
      end
      inserted_notification_ids = Notification.where(notifiable: item, notification_type: notification_type).pluck(:id)
      unless inserted_notification_ids.empty?
        inserts = inserted_notification_ids.map{|i| "(#{i}, #{item.user_id}, FALSE)"}
        sql = "INSERT INTO `notifications_causal_users` (`notification_id`, `user_id`, `anonymous`) VALUES #{inserts.join(',')}"
        conn.execute sql
      end

    when 'added_to', 'removed_from', 'card_updated'
      card = Card.find(opts[:item_id])
      Notification.create({user_id: opts[:user_id], notifiable: card, notification_type: opts[:notification_type], custom_message: opts[:custom_message]})
    when 'comment'
      item = Comment.find_by(id: item_id)
      if item.nil?
        log.warn "Trying to generate comment notification for bad id: #{item_id}"
        return
      end

      notification_type = 'new_comment'

      if item.commentable_type == 'Card' || item.commentable_type == 'Post'
        fk_notif = Notification.new(notifiable: item.commentable, notification_type: notification_type)
        fk_notif.causal_users << item.user
        opts = {deep_link_id: item.commentable_id, deep_link_type: (item.commentable_type == 'Card' ? 'card' : 'post'), content: fk_notif.message, item: item, notification_type: notification_type, excluded_user_ids: [item.user_id], host_name: item.user.organization.host }
        PubnubNotifier.notify(opts)
      end

      # for every follower, see if they have an unread notification for this thread.
      # If so, then add the new comment creator as a causal user. Otherwise create a new one
      recipients = []
      if item.commentable_type == 'Card'
        exclusions = [item.user_id]
        creator = User.where(id: item.commentable.author_id).where.not(id: exclusions).first
        if creator
          recipients = [creator]
          exclusions << creator.id
        end
        item.commentable.comments.where.not(user_id: exclusions).each do |c|
          recipients << c.user
        end
      else
        recipients = item.commentable.followers.where.not(id: item.user_id)
      end

      recipients.each do |user|
        find_or_new_notification(notifiable: item.commentable,
          user_id: user.id, notification_type: notification_type, group_id: item.group_id,
          causal_user_id: item.user_id, anonymous: item.anonymous)
      end
    when 'join_group', 'leave_group', 'accept_group_invite', 'decline_group_invite'
      action_to_team_notification(opts, action: item_type)
    end
  end

  private

  def generate_new_card_curation_notifications(opts)
    notification_type = opts[:notification_type]
    channel = Channel.find_by(id: opts[:item_id])
      if channel.present?
        channel.curators.each do |curator|
          notification = Notification.new(user: curator,notification_type: notification_type, notifiable: channel, widget_deep_link_id: channel.id, widget_deep_link_type: "Channel")
          unless notification.save
            log.error "New card for curation notification generation failed for channel id: #{item_id} and Curator id: #{curator.id}"
            return
          end
        end
      end
  end

  def generate_curation_notifications(opts)
      if opts[:user].present?
        notification = Notification.new(
          user: opts[:user],
          notification_type: opts[:notification_type],
          notifiable: opts[:notifiable],
          widget_deep_link_id: opts[:deep_link_id],
          widget_deep_link_type: opts[:deep_link_type],
          custom_message: opts[:custom_msg]
        )
        unless notification.save
          log.error "#{opts[:notification_type]} notification save failed for channels_card id: #{opts[:item_id]}"
          return
        end
      end
  end

  def generate_clc_notification(opts)
    notification_type = opts[:notification_type]
    case notification_type
      when 'clc_started'
        clc = Clc.find(opts[:item_id])
        clc.entity.users.not_suspended.each do |user|
          message = "A new Continuous Learning exercise, #{clc.name}, has been started for #{clc.target_score} HOURS"
          notification = Notification.new(user: user,notification_type: notification_type, notifiable: clc, custom_message: message)
          unless notification.save
            log.error "Follow notification save failed for follow id: #{item_id} #{notification.errors.full_messages}"
            return
          end
          push_clc_notification(notification)
        end
      when 'won_badge'
        user_badge = UserBadge.find(opts[:item_id])
        clc_badging = user_badge.clc_badging
        clc = user_badge.clc_badging.clc
        message = "Congratulations! You have successfully earned a badge for completing #{clc_badging.target_steps}% of Continuous Learning, #{clc.name}."
        notification = Notification.new(user: User.find(opts[:user_id]),notification_type: notification_type, notifiable: clc, custom_message: message)
        unless notification.save
          log.error "Follow notification save failed for follow id: #{item_id} with errors: #{notification.errors.full_messages}"
          return
        end
        push_clc_notification(notification)
    end
  end

  def push_clc_notification(notification)
    opts = { deep_link_id: notification.user_id, deep_link_type: 'user' , content: notification.message, notification_type: notification.notification_type, host_name: notification.user.organization.host, user_id: notification.user_id }
    PubnubNotifier.notify(opts)
  end

  # (notifiable_id, notifiable_type, group_id, user_id, notification_type, unseen, accepted_at, created_at, updated_at, widget_deep_link_id, widget_deep_link_type, app_deep_link_id, app_deep_link_type, card_id)
  def bulk_insert_video_stream_notifications(conn: , user_ids: , stream:, notification_type: , now: , widget_deep_link_params: , app_deep_link_params:)
    inserts = []
    user_ids.each do |user_id|
      next if user_id == stream.creator_id
      inserts.push("(#{stream.id}, 'VideoStream', NULL, #{user_id}, '#{notification_type}', TRUE, '#{now}', '#{now}', '#{now}', #{widget_deep_link_params[0].nil? ? 'NULL' : widget_deep_link_params[0]}, #{widget_deep_link_params[1].nil? ? 'NULL' : "'#{widget_deep_link_params[1]}'"}, #{app_deep_link_params[0].nil? ? 'NULL' : app_deep_link_params[0]}, #{app_deep_link_params[1].nil? ? 'NULL' : "'#{app_deep_link_params[1]}'"}, NULL)");
    end
    unless inserts.empty?
      sql = "INSERT INTO `notifications` (`notifiable_id`, `notifiable_type`, `group_id`, `user_id`, `notification_type`, `unseen`, `accepted_at`, `created_at`, `updated_at`, `widget_deep_link_id`, `widget_deep_link_type`, `app_deep_link_id`, `app_deep_link_type`, `card_id`) VALUES #{inserts.join(',')}"
      conn.execute sql
    end
  end

  def find_or_new_notification(notifiable: nil,
                               user_id: nil,
                               notification_type: nil,
                               causal_user_id: nil,
                               group_id: nil,
                               anonymous: false,
                               notification_message: nil,
                               app_deep_link_id: nil,
                               app_deep_link_type: nil)

    return [nil, nil] if (user_id == causal_user_id)
    newly_created = false
    notification = Notification.where(notifiable: notifiable,
                                      user_id: user_id,
                                      notification_type: notification_type,
                                      group_id: group_id,
                                      unseen: true).first

    unless notification
      newly_created = true
      notification = Notification.new(notifiable: notifiable,
                                      user_id: user_id,
                                      group_id: group_id,
                                      notification_type: notification_type,
                                      app_deep_link_id: app_deep_link_id,
                                      app_deep_link_type: app_deep_link_type,
                                      custom_message: notification_message,
                                      card_id: app_deep_link_id)

      if notification.access_notification_type_for_team || notification_type == "user_card_share"
        notification.custom_message = notification_message
      end
      unless notification.save
        log.error "#{notification_type} notification save failed for
        #{notifiable.class} with id: #{notifiable.id} and user_id: #{user_id}"
        return
      end
    end
    unless notification.causal_users.pluck(:id).include?(causal_user_id)
      NotificationsCausalUser.create!(notification: notification, user_id: causal_user_id, anonymous: anonymous)
    end
    [notification, newly_created]
  end

  def group_invite_notifications(opts)
    invite = Invitation.find_by(id: opts[:item_id])
    team = invite&.invitable
    unless invite || team
      return log.warn "Trying to generate group invite notification for bad id: #{opts[:item_id]}"
    end
    user = User.find_by(email: invite.recipient_email,
                        organization_id: team.organization_id)
    notification_message = "You've been invited by #{invite.sender.name} to join\nthe #{invite.invitable.name} Team"
    app_deep_link_id, app_deep_link_type = team.get_app_deep_link_id_and_type_v2
    notification, newly_created = find_or_new_notification(notifiable: team,
                                  user_id: user.id,
                                  notification_type: opts[:item_type],
                                  causal_user_id: invite.sender_id,
                                  notification_message: notification_message)

    return unless newly_created
    opts = get_pubnub_opts(notification)
    PubnubNotifier.notify(opts)
  end

  def add_to_team_notifications(opts)
    item = Team.find_by(id: opts[:item_id])
    notification = Notification.new(notifiable: item, notification_type: opts[:notification_type], user_id: opts[:user_id])
    unless notification.save
      log.error "Add team team notification save failed for team id: #{item_id}, user_id: #{opts[:user_id]}"
      return
    end
    opts = { deep_link_id: item.id, deep_link_type: 'Team', content: notification.message,
             item: item, notification_type: opts[:notification_type], host_name: item.organization.host }
    PubnubNotifier.notify(opts)
  end

  def action_to_team_notification(opts, action: nil)
    team = Team.find_by(id: opts[:item_id])
    join_user = User.find_by(id: opts[:team_user_id])
    unless team
      return log.warn "Trying to generate group notification for bad id: #{opts[:item_id]}"
    end
    notification_message = case action
      when 'join_group'
        "#{join_user.full_name} joined the team #{team.name}"
      when 'leave_group'
        "#{join_user.full_name} left the team #{team.name}"
      when 'accept_group_invite'
        "#{join_user.full_name} accepted the invitation to the team #{team.name}"
      when 'decline_group_invite'
        "#{join_user.full_name} declined an invitation to the team #{team.name}"
    end
    users = team.sub_admins.presence || team.admins

    return if users.blank?
    users.each do |user|
      notification, newly_created = find_or_new_notification(
        notifiable: team,
        user_id: user.id,
        notification_type: action,
        causal_user_id: join_user.id,
        notification_message: notification_message,
        app_deep_link_id: team.id,
        app_deep_link_type: 'team'
      )
      return unless newly_created
      opts = get_pubnub_opts(notification)
      PubnubNotifier.notify(opts)
    end
  end


  def channel_invite_notifications(opts, type)
    channel = Channel.find_by(id: opts[:item_id])
    if channel.nil?
      log.warn "Trying to generate #{type} notifications for bad channel id: #{opts[:item_id]}"
      return
    end

    users_ids = opts[:users_ids]
    inviter_id = opts[:inviter]
    users = User.where(id: users_ids)
    users.each do |user|
      unless user.id == inviter_id
        n, newly_created = find_or_new_notification(notifiable: channel, user_id: user.id, notification_type: type, causal_user_id: inviter_id)
        if newly_created
          opts = {deep_link_id: channel.id, deep_link_type: 'channel', content: n.message, item: channel, notification_type: n.notification_type, user_id: user.id, host_name: user.organization.host }
          PubnubNotifier.notify(opts)
        end
      end
    end
  end

  def get_pubnub_opts(notification)
    item = notification.notifiable
    item = notification.notifiable.commentable if notification.notifiable.is_a?(Comment)
    deep_link_id, deep_link_type = item.get_app_deep_link_id_and_type_v2
    opts = { deep_link_id: deep_link_id, deep_link_type: deep_link_type, item: item , content: notification.message, notification_type: notification.notification_type, host_name: notification.user.organization.host, user_id: notification.user_id }
    opts
  end

  def generate_team_card_share_notifications(opts)
    team_card = TeamsCard.find_by(id: opts[:item_id])
    if team_card.nil?
      log.warn "Trying to generate team_card_share notification for bad id: #{opts[:item_id]}"
      return
    end
    user = team_card.user
    card = team_card.card
    team = team_card.team
    notification_type = opts[:item_type]
    app_deep_link_id, app_deep_link_type = card.get_app_deep_link_id_and_type_v2
    team.teams_users.each do |team_user|
      # to not send bell for share if card is not accessible to teams user(because of restriction)
      next if (!card.is_public && !card.visible_to_user(team_user.user))
      notification, newly_created = find_or_new_notification(notifiable: card,
                                                             user_id: team_user.user_id,
                                                             notification_type: notification_type,
                                                             causal_user_id: user.id,
                                                             group_id: nil,
                                                             anonymous: false,
                                                             app_deep_link_id: app_deep_link_id,
                                                             app_deep_link_type: app_deep_link_type)
      if newly_created
       opts = {deep_link_id: app_deep_link_id,
               deep_link_type: app_deep_link_type,
               content: notification.message,
               item: card,
               notification_type: notification.notification_type,
               user_id: notification.user_id,
               host_name: user.organization.host }
       PubnubNotifier.notify(opts)
      end
    end
  end

  def generate_user_card_share_notifications(opts)
    card_user_share = CardUserShare.find_by(id: opts[:item_id])
    if card_user_share.nil?
      log.warn "Trying to generate user_card_share notification for bad id: #{opts[:item_id]}"
      return
    end
    user = card_user_share.user
    card = card_user_share.card
    app_deep_link_id, app_deep_link_type = card.get_app_deep_link_id_and_type_v2
    notification, newly_created = find_or_new_notification(notifiable: card,
                                                           user_id: user.id,
                                                           notification_type: opts[:item_type],
                                                           causal_user_id: opts[:item_shared_by],
                                                           group_id: nil,
                                                           anonymous: false,
                                                           app_deep_link_id: app_deep_link_id,
                                                           app_deep_link_type: app_deep_link_type)
    if newly_created
      opts = {deep_link_id: app_deep_link_id,
              deep_link_type: app_deep_link_type,
              content: notification.message,
              item: card,
              notification_type: notification.notification_type,
              user_id: notification.user_id,
              host_name: user.organization.host }

      PubnubNotifier.notify(opts)
    end
  end

  def generate_topic_request_approval_notifications(opts)
    topic_request = TopicRequest.find_by(id: opts[:item_id])
    if topic_request.nil?
      log.warn "Trying to generate topic_request_approval notification for bad id: #{opts[:item_id]}"
      return
    end
    requestor = topic_request.requestor
    topic_request_label = topic_request.label.presence || ""
    event_type = (opts[:item_state] == "topic_request_published") ? "approved and published" : "rejected"
    custom_message = "Your learning topic '#{topic_request_label}' has been #{event_type}."
    notification, newly_created = find_or_new_notification(notifiable: topic_request,
                                                           user_id: requestor.id,
                                                           notification_type: opts[:item_state],
                                                           anonymous: false,
                                                           notification_message: custom_message,
                                                           app_deep_link_id: topic_request.id,
                                                           app_deep_link_type: "topic_request")

    return unless newly_created
    opts = get_pubnub_opts(notification)
    PubnubNotifier.notify(opts)
  end
end

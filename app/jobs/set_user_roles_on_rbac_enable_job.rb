class SetUserRolesOnRbacEnableJob < ActiveJob::Base
  queue_as :default

  def perform(organization_id:)
    org = Organization.find(organization_id)
    org.admins.each do |admin|
      admin.add_role(Role::TYPE_ADMIN)
    end
    org.members.each do |member|
      member.add_role(Role::TYPE_MEMBER)
    end
  end
end

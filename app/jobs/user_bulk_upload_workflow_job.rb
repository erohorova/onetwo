# This job will pass the input data of User for processing all eligible Workflows.
# These input data will be in +user_metadata+ parameter.
# The input data will be passed to this Job will be a batch of 500.

class UserBulkUploadWorkflowJob < EdcastActiveJob
  queue_as :workflow

  def perform(organization_id:, user_metadata: {})
    org = Organization.find_by(id: organization_id)
    user_ids = []

    user_ids = org.users.where(id: user_metadata[:ids]).ids if user_metadata[:ids].present?
    user_ids = org.users.where(email: user_metadata[:emails]).ids if user_metadata[:emails].present?

    # To be on safer side, using find_in_batches.
    input_batch_size = org.workflow_input_batch_size
    user_ids.each_slice(input_batch_size) do |user_id_batch|
      WorkflowService::Observer.new(
        input_data_ids: user_id_batch,
        model_name: :User,
        organization: org
      ).perform
    end
  end
end
class LmsAddOrganizationUsersJob < ActiveJob::Base
  queue_as :lms_integration

  def perform(organization_id)

    organization = Organization.select('id').find(organization_id)

    if organization
      organization.users.select('users.id').find_each do |user|
        LmsIntegration::UserSettings.new(user.id).setup_user
      end
    end
  end
end

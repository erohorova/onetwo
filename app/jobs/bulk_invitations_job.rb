class BulkInvitationsJob < EdcastActiveJob
  queue_as :batch

  def perform(invitation_file_id:)
    #turn the file into csv
    invitation_file = InvitationFile.find(invitation_file_id)

    success_count = 0
    csv_rows = CSV.parse(invitation_file.data)
    csv_rows.each do |row|

      if row.size == 3 #have first name, last name, and email
        email = row[2]
        last_name = row[1]
        first_name = row[0]
      else #only have email
        email = row[0]
        first_name = nil
        last_name = nil
      end

      invitation = Invitation.new(:sender_id => invitation_file.sender_id,
                                  :invitable => invitation_file.invitable,
                                  :recipient_email => email,
                                  :first_name => first_name,
                                  :last_name => last_name,
                                  :roles => invitation_file.roles)
      invitation.send_invite_email = invitation_file.send_invite_email

      if invitation.save
        success_count += 1
      else
        log.warn "Unable to create invitation for cvs row=#{row.to_json}"
      end
    end
    if success_count > 0
      log.info "Successfully sent out #{success_count} of #{csv_rows.length} invitations for invitable=#{invitation_file.invitable_type}, invitable_id=#{invitation_file.invitable_id}"
      msg = "We have processed your import file successfully and sent out #{success_count} of #{csv_rows.length} invitations for #{invitation_file.invitable.name} #{invitation_file.invitable_type == "Team" ? "Group" : "Organization"} "
      #invitation_file.destroy
    else
      log.error "Unable to create any invitation from invitation_file=#{invitation_file_id}"
      msg = "We were unable to process your import file. Please contact support@edcast.com." 
    end

    InvitationMandrillMailer.import_confirmation(invitation_file.sender, msg, invitation_file.invitable).deliver_now
  end
end

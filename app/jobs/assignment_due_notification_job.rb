class AssignmentDueNotificationJob < EdcastActiveJob
  queue_as :notifications

  def perform(assignment_id, due_at = nil)
    assignment = Assignment.where(id: assignment_id).first!
    assignor = assignment.team_assignments.last.try(:assignor)
    unless assignment.assignee.is_suspended?
      if due_at
        if due_at == assignment.due_at.to_i
          assignment.create_notification(causal_user_id: assignor.try(:id), notification_type: 'assignment_due_soon')
        end
      else
        assignment.create_notification(causal_user_id: assignor.try(:id), notification_type: 'assignment_due_soon')
      end
    end
  end
end

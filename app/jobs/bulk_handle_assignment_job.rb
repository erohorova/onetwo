class BulkHandleAssignmentJob < ActiveJob::Base
  queue_as :default

  def perform(min_id:, max_id:)
    users = User.where("id >= #{min_id} and id <= #{max_id}")
    users.find_each do |user|
      user.assign_handle
      user.save(validate: false)
    end
  end
end

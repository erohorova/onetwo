class SendUserDigestJob < EdcastActiveJob
  queue_as :forum

  def perform(date_str, user_id, interval='digest_daily')
    EDCAST_NOTIFY.send_user_digest(Date.parse(date_str), user_id, interval.to_sym)
  end
end

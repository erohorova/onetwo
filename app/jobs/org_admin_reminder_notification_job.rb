class OrgAdminReminderNotificationJob < EdcastActiveJob
  queue_as :mailers

  def perform(org, admin)
    OrganizationMailer.reminder_mail_to_add_users(org, admin)
  end
end

class EnqueueStreamsBatchJob < EdcastActiveJob
  queue_as :feed

  # {stream_id: @stream.id, user_ids: user_ids, rationale: rationale, timestamp: @timestamp, buffer_flush: buffer_flush}
  def perform(opts={})
    stream = VideoStream.find opts[:stream_id]
    timestamp = Time.at(opts[:timestamp])

    return if stream.nil? || opts[:user_ids].blank?

    # skip default org
    return if stream.organization_id == Organization.default_org&.id

    User.where(id: opts[:user_ids]).select(:id).find_each{ |user| stream.enqueue(user, opts[:rationale], timestamp, opts[:buffer_flush]) }
    UserContentsQueue.buffer_flush
  end
end

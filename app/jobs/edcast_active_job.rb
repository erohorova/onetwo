# AJ does not provide to prevent job execution.
# It merely provides callbacks similar to ActiveRecord but does not halt execution if NIL/FALSe is returned.
# Refer:
#  1. `https://groups.google.com/forum/#!topic/rubyonrails-core/mhD4T90g0G4`
#  2. `http://stackoverflow.com/questions/26857845/how-to-read-rabbitmq-unacknowledged-messages-rabbitmq-cycle`

class EdcastActiveJob < ActiveJob::Base

  ActiveJob::Base.logger.level = :error
  WHITELISTED_JOB_CLASS = %w(RecurringJobs::LrsPullJob)

  def serialize_and_truncate_args job
    args = ActiveJob::Arguments.serialize(job.arguments).to_json
    truncated = false
    if args.length > 65000
      args = args.truncate(65000)
      truncated = true
    end
    return args, truncated
  end

  # This method is called from both perform_now and perform_later.
  # Possibly we could use before_enqueue for this,
  # but I don't know how to trigger it from tests
  def self.job_or_instantiate(*args)
    opts = { request_store_data: RequestStore.read(:request_metadata) || {} }
    # add the request store opts as the last hash.
    # In, before_perform, this gets assigned back to RequestStore
    # and the arg is deleted.
    args << opts
    super(*args)
  end

  before_enqueue do |job|
    # Run the iteration for only those jobs that are markked unique.
    if job.class.respond_to?(:unique)
      args    = job.class.unique_args(job.arguments)
      tracker = Sidekiq::JobStatus.new

      if tracker.enqueuable?(args)
        tracker.set_status(args, 'enqueued')
      else
        # Refer: ./lib/set_unique_jobs.rb
        # REFERENCE: https://groups.google.com/forum/#!topic/rubyonrails-core/mhD4T90g0G4
        raise StandardError
      end
    end

    if WHITELISTED_JOB_CLASS.include?(job.class.name)
      args, truncated = serialize_and_truncate_args job
      Octopus.using(:master) do
        BackgroundJob.new(
          job_id: job.job_id,
          job_class: job.class.name,
          job_queue: job.queue_name,
          enqueued_at: Time.now.utc,
          scheduled_at: job.try(:scheduled_at),
          status: "enqueued",
          job_arguments: args,
          truncated: truncated
        ).save
      end
    end
  end

  before_perform do |job|
    # Remove the last argument (which should have been set by .job_or_instantiate)
    # and assign properties to RequestStore.
    last_argument = job.arguments[-1]
    if last_argument.is_a?(Hash) && last_argument.key?(:request_store_data)
      RequestStore.store[:request_metadata] = job.arguments.pop[:request_store_data]
    elsif job.is_a?(Analytics::MetricsRecorderJob)
      log.warn "EDCAST ACTIVE JOB: Job ID: #{job.job_id} Date stored in request
        store is missing"
    end

    if WHITELISTED_JOB_CLASS.include?(job.class.name)
      args, truncated = serialize_and_truncate_args job
      Octopus.using(:master) do
        bk_job = BackgroundJob.find_by_job_id(job.job_id)
        if bk_job
          # ISSUE: RabbitMQ doesnot receives job acknowledgement but job is already done.
          #   So, consumers pulls same job over and over again despite of status being marked as `finished` OR `errored`.
          # Hence, raising an exception will call #handle_error and acknowledge same message in rabbitmq.
          if bk_job.finished? || bk_job.errored?
            raise 'Job already finished'
          end

          bk_job.started_at = Time.now.utc
          bk_job.status = "started"
          bk_job.save
        end
      end

      if job.class.respond_to?(:unique)
        args = job.class.unique_args(job.arguments)
        Sidekiq::JobStatus.new.set_status(args, 'processing')
      end
    end
  end

  after_perform do |job|
    # Wipe the thread variable, this will prevent the wrong user id
    # from being recorded
    RequestStore.delete(:request_metadata)

    if WHITELISTED_JOB_CLASS.include?(job.class.name)
      bk_job = BackgroundJob.find_by_job_id(job.job_id)
      if bk_job
        args, truncated = serialize_and_truncate_args job

        Octopus.using(:master) do
          now = Time.now.utc
          bk_job.finished_at = now
          if bk_job.started_at
            bk_job.time_taken = (now - bk_job.started_at).to_i
          end
          bk_job.status = "finished"
          bk_job.save
        end
      end
    end
    job.class.respond_to?(:unique) && Sidekiq::JobStatus.new.delete(job.class.unique_args(job.arguments))
  end

  def handle_error exception
    # You can override this class, but be sure to call super
    log.error exception

    Octopus.using(:master) do
      bk_job = BackgroundJob.find_by_job_id(job_id)

      return if bk_job&.finished? || bk_job&.errored?

      if bk_job
        bk_job.status = "error"
        now = Time.now.utc
        bk_job.finished_at = now
        if bk_job.started_at
          bk_job.time_taken = (now - bk_job.started_at).to_i
        end
        bk_job.save

        current_job = Object.const_get(bk_job.job_class)
        if current_job.respond_to?(:unique)
          args = current_job.unique_args(JSON.parse(bk_job.job_arguments))
          Sidekiq::JobStatus.new.delete(args)
        end
      end
    end
  end

  rescue_from(StandardError) do |exception|
    handle_error exception
  end
end

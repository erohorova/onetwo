class MultiInvitationsJob < EdcastActiveJob
  queue_as :notifications

  def perform(args)
    args[:emails].each_with_index do |email, index|
      recipient_id = args[:recipient_ids]&.fetch(index, nil)
      invitation = Invitation.new(sender_id: args[:sender_id],
                                  recipient_id: recipient_id,
                                  invitable_id: args[:invitable_id],
                                  invitable_type: args[:invitable_type],
                                  recipient_email: email,
                                  roles: args[:roles])

      unless invitation.save
        log.warn "Unable to create invitation for email=#{email} with error: #{invitation.errors.full_messages}"
      end
    end
  end
end

class TrackJob < EdcastActiveJob
  queue_as :analytics

  def perform(payload)
    tries ||= 3
    Mixpanel::Consumer.new.send!(*JSON.parse(payload))
  rescue => e
    tries -= 1
    if tries > 0
      sleep 1
      retry
    else
      log.error e
    end
  end
end

class UpdateCardMetadataJob < EdcastActiveJob
  queue_as :cards

  def perform(card_id)
    card = Card.find_by(id: card_id)
    unless card.nil?
      card.find_pack_journey_and_update
    else
      log.error "Card with #{card_id} does not exist"
    end
  end
end
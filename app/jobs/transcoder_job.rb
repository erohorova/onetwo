class TranscoderJob < EdcastActiveJob
  queue_as :video_stream

  def perform(opts)
    recording_id = opts[:recording_id]
    overwrite = opts[:overwrite] == true
    recording = Recording.find(recording_id)
    if overwrite
      recording.retranscode!
    else
      recording.start_transcoding!
    end
  end
end

class EclCardRecoverJob < EdcastActiveJob
  # NOTE:
  #   Executes the job maximum 10 times.
  #   In the last iteration, it does not execute the #callback proc.
  include ActiveJob::Retry.new(
    strategy: :exponential, limit: 10,
    callback: proc do |exception, delay|
      # will be run before each retry
      Rails.logger.debug "Retrying EclCardRecoverJob in #{delay.to_s} seconds with job_id: #{self.job_id}"
    end
  )

  def perform(ecl_id, organization_id)
    EclApi::CardConnector.new(ecl_id: ecl_id, organization_id: organization_id).recover_card_from_ecl
  end
end
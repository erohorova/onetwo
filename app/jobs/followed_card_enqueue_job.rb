# enqueue cards from followed users, cardqueries, or channels
# Enqueue cards for teams that follow those channels in which the card is posted.
class FollowedCardEnqueueJob < CardsJob
  queue_as :cards

  def perform(opts = {})
    Octopus.using(:replica1) do
      card = Card.excluding_default_org.visible_cards.published.find_by(id: opts[:card_id])
      unless card
        log.warn "no card for #{opts}"
        return
      end

      author = card.author
      if author
        channel_ids = ChannelsCard.where(card_id: card.id).pluck(:channel_id)
        Follow.followers_to_enqueue(
          followable_id: author.id,
          followable_type: 'User',
          card_id: card.id,
          channel_ids: channel_ids
        )
      end
    end
  end
end

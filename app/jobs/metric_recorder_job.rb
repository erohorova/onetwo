class MetricRecorderJob < EdcastActiveJob
  queue_as :analytics

  # a string value indicates a proxy model represented by some other key in the hash
  OWNER_MAPPING = {
      'card': {type: 'user', id: 'author_id'},
      collection: 'card',
      'post': {type: 'user', id: 'user_id'},
      'question': {type: 'user', id: 'user_id'},
      'comment': {type: 'user', id: 'user_id'},
      'answer': {type: 'user', id: 'user_id'},
      'channel': {type: 'user', id: 'user_id'},
      'video_stream': {type: 'user', id: 'creator_id'},
      'wowza_video_stream': {type: 'user', id: 'creator_id'},
      'user': {type: 'user', id: 'id'}
  }

  def perform(user_id, type, payload) #type is always 'act' because we're trying to conform with mix panel interface; useless
    action = type == 'act' ? payload[:type] : type #03-28-2016 someone changed the mixpanel tracking interface where type is no longer always 'act', causing card creation tracking to fail
    object_type = payload[:object]
    object_id   = payload[:object_id]

    owner_type  = nil
    owner_id    = nil

    if object_type
      mapping = OWNER_MAPPING[object_type.to_sym]
      if String === mapping
        object_type = mapping
        mapping = OWNER_MAPPING[mapping.to_sym]
      end

      if object_type && object_id
        object = object_type.classify.constantize.find_by_id(object_id)

        #indexing the owner ahead of time makes querying for how many likes a given user received across his insights, comments easy to query
        owner_type, owner_id = get_owner(object: object, mapping: mapping)
      end
    end

    MetricRecord.create(actor_type: 'user', actor_id: user_id,
                        object_type: object_type, object_id: object_id,
                        owner_type: owner_type, owner_id: owner_id,
                        action: action, platform: payload[:platform],
                        device_id: payload[:device_id], properties: payload[:properties],
                        organization_id: payload[:organization_id], referer: payload[:referer],
                        ecl_id: object.try(:ecl_id)
    )
  end

  def get_owner(object:, mapping:)
    if object && mapping
      return mapping[:type], object.send(mapping[:id].to_sym)
    else
      return nil, nil
    end
  end
end

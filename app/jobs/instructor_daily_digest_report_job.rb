class InstructorDailyDigestReportJob < EdcastActiveJob
  queue_as :forum

  def perform(instructor_data:, from:, to:)
    instructor = User.where(id: instructor_data['id']).first
    return if instructor.blank?

    from = Time.at(from).utc
    to   = Time.at(to).utc
    log.info("Generating report for instructor data: #{instructor_data}")
    InstructorDigestReportService.new.generate_report(instructor_data: instructor_data, from: from, to: to)
  end
end

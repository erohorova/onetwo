class EclSourceFetchJob < EdcastActiveJob
  extend SetUniqueJob

  queue_as :content_source

  def self.unique_args(args)
    [args[0]]
  end

  def perform(ecl_source_id)
    ecl_source = EclSource.find_by(id: ecl_source_id)
    if ecl_source.nil?
      log.warn("ecl source id #{ecl_source_id} not present")
      return
    end

    org = ecl_source.channel.organization
    limit = org.get_settings_for('channel_fetch_job_limit') || 500
    # Find number of cards already in the channel from that ECL source
    card_search_response = Search::CardSearch.new.search(
      ecl_source.channel,
      org,
      viewer: nil,

      # The "ecl_source_id" key has to be a symbol, not a string, since
      # the CardSearch class needs a symbol.
      filter_params: {ecl_source_id: ecl_source.ecl_source_id},
      source: ["ecl_id"],
      offset: 0,
      limit: limit
    )

    exclude_ecl_ids = card_search_response.results.map{|card|card.ecl_id}.compact
    cards = ecl_source.search_ecl({ exclude_ecl_card_id: exclude_ecl_ids })

    cards.each {|card| card.add_card_to_channel(ecl_source.channel, ecl_source&.private_content)}
  end
end

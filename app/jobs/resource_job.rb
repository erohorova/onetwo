class ResourceJob < EdcastActiveJob
  require 'open-uri'
  require 'open_uri_redirections'

  queue_as :cards

  def perform resource
    if resource.image_url.present?
      begin
        fr = FileResource.find_or_initialize_by(attachable: resource)
        url_str = resource.image_url
        if url_str.start_with?('//')
          url_str = 'http:' + url_str
        end

        if url_str.include?("cdn.filestackcontent.com")
          url_str = Filestack::Security.new.signed_url(url_str)
        end

        fr.attachment = open(url_str, allow_redirections: :safe)

        unless fr.save
          log.error "Failed to localize image URL for resource: #{resource.id} errors: #{fr.errors.full_messages}"
        else
          resource.callback_asset_download_completed if resource.respond_to?(:callback_asset_download_completed)
        end
      rescue => e
        log.error "Failed downloading and saving assets for resource: #{resource.id} img_url: #{url_str} error: #{e.to_s}"
        # Set image url to nil. We failed to download image
        resource.update_attributes(image_url: nil)
        resource.callback_asset_download_completed if resource.respond_to?(:callback_asset_download_completed)
      end
    end
  end
end

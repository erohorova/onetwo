class BulkImportUsersV3Job < EdcastActiveJob
  queue_as :batch

  def perform(file_id:, options:, organization_id:)
    file = InvitationFile.find_by(id: file_id)
    opts = { channels: options[:channels], teams: options[:teams] }

    klass = BulkImport::Runner.new(data: file.data, organization_id: organization_id, opts: opts)
    klass.execute

    file.update(parameter: klass.status)
  end
end

class ChannelEclSourcesFetchJob < EdcastActiveJob
  extend SetUniqueJob

  queue_as :content_source

  def self.unique_args(args)
    [args[0]]
  end

  def perform(channel_id, filter_params= {})
    channel = Channel.find_by(id: channel_id)
    return if channel.blank?

    topics  = channel.try(:topics)
    return if topics.blank?

    filter_params[:topic] = topics.map {|topic| topic['name']}
    filter_params[:topic_ids] = topics.map {|topic| topic['id']}

    org = channel.organization
    limit = org.get_settings_for('channel_fetch_job_limit') || 500
    card_search_response = Search::CardSearch.new.search(
      channel,
      channel.organization,
      viewer: nil,
      filter_params: {"taxonomy_topics" => filter_params[:topic] },
      source: ["ecl_id"],
      offset: 0,
      limit: limit
    )
    filter_params[:exclude_ecl_card_id] = card_search_response.results.map{|card|card.ecl_id}.compact
    filter_params[:sort_by_recency] =  true
    filter_params[:type] = "channel_programming"

    cards = EclApi::EclSearch.new(channel.organization_id).
      search(
        query: '',
        limit: EclSource::DEFAULT_NUMBER_OF_ITEMS,
        offset: 0,
        filter_params: filter_params
      ).results

    # add ecl cards to channels
    cards.each {|card| card.add_card_to_channel(channel)}
    # IF Taxonomy Tpic isnot mapped
    if cards.blank?
      topics.each do |topic|
        if topic["label"].present?
          cards = search_by_topic(channel, topic["label"], filter_params)
          cards.each {|card| card.add_card_to_channel(channel)}
          filter_params[:exclude_ecl_card_id] = filter_params[:exclude_ecl_card_id] + cards.map(&:ecl_id)
        end
      end
    end
  end

  def search_by_topic(channel,topic,filter_params={})
   EclApi::EclSearch.new(channel.organization_id).
      search(
        query: topic,
        limit: EclSource::DEFAULT_NUMBER_OF_ITEMS,
        filter_params: filter_params.except(:topic),
        offset: 0,
      ).results
  end
end

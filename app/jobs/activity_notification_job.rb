class ActivityNotificationJob < EdcastActiveJob
  queue_as :forum

  def perform(opts = {})
    @followable = opts[:followable_class_name].constantize.find(opts[:followable_id])
      batch_size = opts[:batch_size] || 100
      @followable.followers.find_in_batches(batch_size:batch_size) do |users|
        begin
          ActivityNotificationMailer.activity_notification_email(followable: @followable, users: users, content: opts[:content], omit_user_id: opts[:omit_user_id], activity_id: opts[:activity_id]).deliver_now
        rescue => e
          log.warn("Batch email failed. Trying to send each email one at a time: exception=#{e} followable_id=#{@followable.id} users=#{users.map{|u| u.as_json(only: [], methods: [:name, :mailable_email])}}")
          users.each do |user|
            begin
              ActivityNotificationMailer.activity_notification_email(followable: @followable, content: opts[:content], omit_user_id: opts[:omit_user_id], activity_id: opts[:activity_id]).deliver_now
            rescue => e
              log.error("Failed announcement email. exception=#{e} name=#{user.name} email=#{user.mailable_email} followable_id=#{@followable.id}")
            end
          end
        end
      end
  end
end

class ChannelEclSourcesFetchBatchJob < EdcastActiveJob
  extend SetUniqueJob

  queue_as :content_source

  def self.unique_args(args)
    [self.name]
  end

  def perform
    #We will get organizations where channel_programming_v2=true.
    #Then will exclude channels with organization from this Job(not condition)
    organizations_v2 = Organization.get_config_enabled_orgs('channel_programming_v2', 'true').pluck(:configable_id)
    Channel.where(ecl_enabled: true).where.not(organization_id: organizations_v2).find_each do |channel|
      ChannelEclSourcesFetchJob.set(wait: Random.rand(30).minutes).perform_later(channel.try(:id), { only_default_sources: true })
    end
  end
end


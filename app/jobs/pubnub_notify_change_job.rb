class PubnubNotifyChangeJob < EdcastActiveJob
  queue_as :notifications

  def perform(action, to, channel_name)

    payload = {
      pn_apns: {
        aps: {
          "content-available" => 1
        },
        action: action,
        channel_name: channel_name
      },
      pn_gcm: {
        data: {
          action: action,
          channel_name: channel_name
        }
      }
    }

    PubnubNotifier.send_payload(
      payload, [to.private_pubnub_channel_name],
      organization: to.try(:organization)
    )
  end
end

class VideoStreamInvalidateScoreJob < EdcastActiveJob
  queue_as :video_stream

  def perform(video_stream)
    video_stream.invalidate_score
  end
end

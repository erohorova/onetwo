class UserTopicLevelMetricsAggregatorJob < EdcastActiveJob
  queue_as :analytics

  def perform(attributes)

    topics = MetricRecord.get_topics_for_object(attributes)
    
    return if topics.blank?

    increments = {}
    [:actor_id, :owner_id].each {|user_type| increments[attributes[user_type]] = [] unless attributes[user_type].nil?}

    if MetricRecord.should_increment_smartbite_score?(attributes)
      SmartbiteScoreMapper.smartbite_score_increments(attributes).each do |user_id, increment|
        increments[user_id] << [:smartbites_score, increment]
      end
    end

    unless attributes[:actor_id].nil?
      # smartbites created
      if MetricRecord.is_creation_event?(attributes)
        increments[attributes[:actor_id]] << [:smartbites_created, 1]
      # smartbites consumed
      elsif MetricRecord.is_consumption_event?(attributes)
        increments[attributes[:actor_id]] << [:smartbites_consumed, 1]
      end
    end

    increments.each do |user_id, _increments|
      topics.each do |topic|
        UserTopicLevelMetric.update_from_increments(opts: {user_id: user_id, tag_id: topic.id}, increments: _increments, timestamp: Time.at(attributes[:created_at_to_i]))
      end
    end
  end
end

class RecurringJobs::AssignmentPerformanceMetricsJob < ActiveJob::Base
  queue_as :default

  def perform
    time = (Time.now - 1.days).beginning_of_day
    content_ids = Assignment.where(
      "created_at >= :time OR updated_at >= :time",
      time: time
    ).pluck(:assignable_id).uniq

    AssignmentPerformanceMetricJob.perform_later(content_ids)
  end
end

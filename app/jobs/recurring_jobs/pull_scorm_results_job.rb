class RecurringJobs::PullScormResultsJob < EdcastActiveJob
  queue_as :scorm

  def perform
    ScormCompletionService.new.run
  end
end

class RecurringJobs::RemoveOldBackgroundJobsLogsJob < EdcastActiveJob
  queue_as :default

  def perform(*args)
    BackgroundJob.where("finished_at < (:week_ago)", {week_ago: 1.week.ago}).delete_all
  end
end

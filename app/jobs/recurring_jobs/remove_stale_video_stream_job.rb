class RecurringJobs::RemoveStaleVideoStreamJob < EdcastActiveJob
  queue_as :video_stream

  def perform(*args)
    log.info "Remove stale live streams - starting"
    count = 0
    ids = []
    VideoStream.where.not(last_stream_at: nil).where(status: 'live').where('last_stream_at < ?', 5.minute.ago).find_each do |v|
      unless v.update_attributes(status: 'past')
        log.error "Unable to turn stale live stream to past stream: video_stream_id=#{v.id}"
      end
      count += 1
      ids << v.id
    end

    log.info "Finished removing stale live streams. Found #{count} such streams. Here are the ids: #{ids}"
    log.info "Remove stale scheduled streams - starting"

    count = 0
    ids = []
    VideoStream.where.not(state: 'deleted').where(status: 'upcoming').where("start_time <= (:one_hour_ago)", one_hour_ago: Time.now.utc - 1.hour).find_each do |v|
      v.delete_from_cms!
      count += 1
      ids << v.id
    end

    log.info "Finished removing stale scheduled streams. Found #{count} such streams. Here are the ids: #{ids}"

    #data correction, set all the video streams without last_stream_at time to now. which then will be set to past on the next run.
    VideoStream.where(last_stream_at: nil).where(status: 'live').where('created_at < ?', 5.minute.ago).find_each do |v|
      unless v.update_attributes(last_stream_at: Time.now)
        log.error "Unable to last_stream_at to now: video_stream_id=#{v.id}"
      end
    end
  end
end

class RecurringJobs::PercentileCalculatorJob < EdcastActiveJob
  queue_as :analytics

  def perform
    now = Time.now.utc
    periods_with_offsets = PeriodOffsetCalculator.applicable_offsets(now)

    Organization.exclude_default_org.find_each do |org|
      UpdatePercentileForOrgJob.perform_later(org.id, periods_with_offsets)
    end
  end
end

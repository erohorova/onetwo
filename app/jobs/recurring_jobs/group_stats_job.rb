class RecurringJobs::GroupStatsJob < EdcastActiveJob
  queue_as :forum


  def perform(opts = {})
    group = ResourceGroup.where(id: opts[:group_id]).first!
    # start date or end date is time.to_i format other wise it will give
    #  serialized error
    start_date = opts[:start_date].present? ? Time.at(opts[:start_date]).utc : nil
    end_date = opts[:end_date].present? ? Time.at(opts[:end_date]).utc : nil

    return unless group

    group_stats = group.daily_group_stats

    # forum posts data
    update_posts_stats(group_stats, group, start_date, end_date)

    # forum comments/answers data
    update_comments_stats(group_stats, group, start_date, end_date)

    # unanswered forum posts
    update_unanswered_posts_stats(group_stats, group)

    # active users from search index
    update_active_users(group_stats, group, start_date, end_date)

    # trending tags for last 7 days - no backfill logic yet
    update_trending_tags(group_stats, group)

    # trending context for last 7 days - no backfill
    update_trending_contexts(group_stats, group)

    group_stats.each{|s|
      s.save! if s.persisted? || s.active_day?
    }
  end

  private
  def update_daily_stat_record(group_stats, group, date, attrs)
    stat = group_stats.select{|s| date == s.date}.first

    if stat.nil?
      stat = DailyGroupStat.new(group_id: group.id, date: date)
      group_stats << stat
    end
    attrs.keys.each do |k|
      stat.send("#{k.to_s}=", attrs[k])
    end
  end

  def update_posts_stats(group_stats, group, start_date, end_date)
    sql = 'select date(created_at), count(distinct(user_id)), count(*) '
    sql = sql + "from posts where postable_id = #{group.id} and postable_type = 'Group' "
    sql = sql + "and created_at >= '#{start_date}' " unless start_date.nil?
    sql = sql + "and created_at <= '#{end_date}' " unless end_date.nil?
    sql = sql + 'group by date(created_at)'
    ActiveRecord::Base.connection.execute(sql).each do |r|
      attrs = {}
      (date, attrs[:total_users_with_posts], attrs[:total_posts]) = r
      update_daily_stat_record(group_stats, group, date, attrs)
    end
  end

  def update_comments_stats(group_stats, group, start_date, end_date)
    sql = 'select date(c.created_at), count(distinct(c.user_id)), count(distinct(p.id)), count(*)  from comments as c, posts as p '
    sql = sql + "where c.commentable_type = 'Post' and c.commentable_id = p.id and p.postable_id = #{group.id} and p.postable_type = 'Group' "
    sql = sql + "and c.created_at >= '#{start_date}' " unless start_date.nil?
    sql = sql + "and c.created_at <= '#{end_date}' " unless end_date.nil?
    sql = sql + 'group by date(c.created_at)'
    ActiveRecord::Base.connection.execute(sql).each do |r|
      attrs = {}
      (date, attrs[:total_users_with_comments], attrs[:total_answered_posts], attrs[:total_comments]) = r
      update_daily_stat_record(group_stats, group, date, attrs)
    end
  end

  def update_unanswered_posts_stats(group_stats, group)
    sql = "select date(created_at), count(*) from posts where comments_count = 0 and postable_id = #{group.id} and postable_type = 'Group' group by date(created_at)"
    ActiveRecord::Base.connection.execute(sql).each do |r|
      attrs = {}
      (date, attrs[:total_unanswered_posts]) = r
      update_daily_stat_record(group_stats, group, date, attrs)
    end
  end

  def update_active_users(group_stats, group, start_date, end_date)
    if start_date.nil?
      range = {date: {to: end_date.nil? ? 'now' : end_date}}
    else
      range = {date: {from: start_date, to: end_date.nil? ? 'now' : end_date}}
    end

    ['forum','events', 'mobile_forum'].each do |type|

      must = [{term: {type: type}}, {term: {group_id: group.id}},{range: range}]
      query = {bool: {must: must}}
      aggs = {dates: {terms: { field: 'date', size: 500 }}}
      results = ActivityRecord.search(query: query, aggs: aggs)


      results.response.aggregations.dates.buckets.each {|b|
        attrs = {}
        date = Date.strptime((b.key_as_string.to_f / 1000).to_s, '%s')
        if type == 'forum'
          attrs[:total_active_forum_users] = b.doc_count
        elsif type == 'mobile_forum'
          attrs[:total_active_mobile_forum_users] = b.doc_count
        else
          attrs[:total_active_events_users] = b.doc_count
        end
        update_daily_stat_record(group_stats, group, date, attrs)
      }

    end
  end

  def update_trending_tags(group_stats, group)
    # Todo: backfill logic

    # running two searchs
    #   1) to find the total tag count of all tags in a forum
    where = {postable_id: group.id, postable_type: 'Group'}
    facets = {tag_names: {limit: 200}}
    results = Post.search('*', where: where, aggs: facets, load: false, limit: 0)
    tags = {}
    results.aggs['tag_names']['buckets'].each do |filter|
      tags[filter['key']] = filter['doc_count']
    end

    #   2) to find trending tags for last 7 days.
    date = (Time.now.utc-7.days).strftime('%Y-%m-%d')
    where = {updated_at: {gt: date}, postable_id: group.id, postable_type: 'Group'}
    facets = {tag_names: {limit: 10}}
    results = Post.search('*', where: where, aggs: facets, load: false, limit: 0)

    trending_tags = []
    results.aggs['tag_names']['buckets'].each do |filter|
      trending_tags << "#{filter['term']}##{tags[filter['term']]}"
    end

    unless trending_tags.empty?
      date = Date.parse(Time.now.utc.strftime('%Y-%m-%d'))
      update_daily_stat_record(group_stats, group, date, {:trending_tags => trending_tags.join(',')})
    end
  end

  def update_trending_contexts(group_stats, group)

    # running two searchs
    #   1) to find the total count of all contexts in a forum
    where = {postable_id: group.id, postable_type: 'Group'}
    facets = {context_id: {limit: 200}}
    results = Post.search('*', where: where, aggs: facets, load: false, limit: 0)
    context_global_counts = {}
    results.aggs['context_id']['buckets'].each do |filter|
      context_global_counts[filter['key']] = filter['doc_count']
    end

    #   2) to find trending contexts for last 7 days.
    date = (Time.now.utc-7.days).strftime('%Y-%m-%d')
    where = {updated_at: {gt: date}, postable_id: group.id, postable_type: "Group"}
    facets = {context_id: {limit: 10}}
    results = Post.search('*', where: where, aggs: facets, load: false, limit: 0)

    trending_contexts = []
    results.aggs['context_id']['buckets'].each do |filter|
      # get the first matching label for this context
      # this is hacky, we might need to make contexts separate
      # business objects at some point
      # 25 Jan 2015 edit: hacky indeed, filter out posts without a context label.
      # Not sure why posts were created with a context id, but nil label
      post = Post.where(context_id: filter['key'], postable: group).where.not(context_label: nil).first
      unless (post.nil?)
        label = post.context_label
        trending_contexts << {'id' => filter['key'], 'label' => label, 'count' => context_global_counts[filter['key']]}
      end
    end

    unless trending_contexts.empty?
      date = Date.parse(Time.now.utc.strftime('%Y-%m-%d'))
      update_daily_stat_record(group_stats, group, date, {:trending_contexts => trending_contexts.to_json})
    end
  end
end
class RecurringJobs::SetupWeeklyActivityJob < EdcastActiveJob
  queue_as :weekly_activity

  def perform(*args)
    Organization.setup_weekly_activity_jobs
  end
end

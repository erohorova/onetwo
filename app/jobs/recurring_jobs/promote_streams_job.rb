class RecurringJobs::PromoteStreamsJob < EdcastActiveJob
  queue_as :video_stream

  def perform(*args)
    streams = VideoStream.upcoming.where("start_time > (:now) AND start_time <= (:week_from_now)", now: Time.now.utc, week_from_now: Time.now.utc + 2.weeks)
    streams.find_each do |stream|
      start_time = Time.now
      EnqueueStreamsJob.perform_later({id: stream.id})
    end
  end
end

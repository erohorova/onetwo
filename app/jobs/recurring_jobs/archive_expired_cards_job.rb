# Aim: Cards with expired URLs must be archived
# Note: Cards can be in any state i.e new, published, deleted.
#   Considering these states, cards can transit to archive state, any time.
#   Job will be running under dedicated queue

class RecurringJobs::ArchiveExpiredCardsJob < EdcastActiveJob
  queue_as :archive

  def perform(*args)
    Card
      .system_generated
      .joins(:resource)
      .find_each do |card|
        resource = card.resource

        next unless resource.expired?

        card.archive!
      end
  end
end
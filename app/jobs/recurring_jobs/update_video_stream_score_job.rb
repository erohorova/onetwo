class RecurringJobs::UpdateVideoStreamScoreJob < EdcastActiveJob
  queue_as :video_stream

  def perform(*args)
    results = Search::VideoStreamSearch.new.find_objects_with_invalid_score.results
    results.each do |video_stream_elastic_search_object|
      video_stream = VideoStream.find(video_stream_elastic_search_object.id)
      VideoStreamInvalidateScoreJob.perform_later(video_stream)
    end
  end
end

class RecurringJobs::FollowBatchEmailGeneratorJob < EdcastActiveJob
  queue_as :notifications

  def perform(*args)
    FollowBatchEmailGenerator.perform
  end
end

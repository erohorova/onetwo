class RecurringJobs::SendNewClcNotification < EdcastActiveJob
  queue_as :default

  def perform(*args)
    Clc.create_notification_for_started_clc
  end
end

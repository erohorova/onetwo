class RecurringJobs::RemindCuratorDailyJob < EdcastActiveJob
  queue_as :default

  def perform
    Organization.exclude_default_org.get_config_enabled_orgs('feature/notifications_and_triggers_enabled', 'true').each do |org|
      if NotificationConfig.find_by(organization_id: org.id).try(:enabled?, "reminded_curator", "email", "single")
        RemindCuratorToCurateJob.perform_later(channel_ids: org.channels.pluck(:id))
      end
    end
  end
end
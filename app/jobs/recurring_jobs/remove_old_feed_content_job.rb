class RecurringJobs::RemoveOldFeedContentJob < EdcastActiveJob
  queue_as :default

  def perform(*args)
    UserContentsQueue.clear_docs_older_than(6.month.ago.to_i) 
  end
end

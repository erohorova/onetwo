# DESC:
#   UserImportStatus that are older than 2 weeks must be removed from the DB
#     and we should capture the statuses of each InvitationFile from UserImportStatus.
# EXPLANATION:
#   In Bulk Import v2, every CSV row is transformed into userImportStatus.
#   This captures the status of import at an individual level. Initial is PENDING.
#   If a row is imported into actual user and their details successfully, the each record is marked as SUCCESS.
#   If a row being imported fails, the each record is marked as error.
#  Works only for Bulk Import User v3.

class RecurringJobs::CleanImportStatusJob < ActiveJob::Base
  queue_as :bulk

  def perform
    invitation_file_ids = UserImportStatus
      .where('created_at < ?', (Time.now - 2.weeks))
      .pluck(:invitation_file_id)
      .uniq

    invitation_files = InvitationFile.where(id: invitation_file_ids).uniq

    invitation_files.find_each do |invitation_file|
      if invitation_file.record_status
        invitation_file.user_import_statuses.destroy_all
      end
    end
  end
end

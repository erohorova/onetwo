class RecurringJobs::GroupStatsBatchJob < EdcastActiveJob
  queue_as :forum

  def perform(*args)

    now = Time.now.utc
    batch_job = BatchJob.find_by(name: self)

    # pick existing OR create new one
    if batch_job
      last_run_time = batch_job.last_run_time
      batch_job.update_attributes!(last_run_time: now)
    else
      BatchJob.create(last_run_time: now, name: self)
      last_run_time = now - 3.hour
    end

    if ENV['start_date'] == '0'
      start_date = nil
    else
      start_date = ENV['start_date'].blank? ? (last_run_time).to_i : Time.parse(ENV['start_date']).utc.to_i
    end
    end_date = ENV['end_date'].blank? ? nil : Time.parse(ENV['end_date']).utc.to_i
    groups = ENV['groups'].blank? ? ResourceGroup.not_closed.select(:id) : Group.where(id: ENV['groups'].split(',')).select(:id)

    log.info "Calculating groups stats with arguments (groups: #{ENV['groups']}, start_date: #{ENV['start_date']}, end_date: #{ENV['end_date']})"
    groups.find_each(batch_size: 100) do |group|
      RecurringJobs::GroupStatsJob.set(wait: 1.minutes).perform_later({group_id: group.id, start_date: start_date, end_date: end_date})      
    end
  end

end

class RecurringJobs::SetupInviteReminderJob < EdcastActiveJob
  queue_as :notifications

  def perform(*args)
    Organization.remind_admin_to_invite_users
  end
end

class RecurringJobs::ChannelPinUpdateJob < EdcastActiveJob
  queue_as :default

  def perform
    Channel.auto_pin_cards_enabled.find_each do |channel|
      Pin.update_pinned_for_channel(channel.id)
    end
  end
end
# Aim: Check for new untranslated strings and add them to CSV on S3

class RecurringJobs::AddUntranslatedStringsJob < EdcastActiveJob
  queue_as :default

  def perform
    key = (Time.now.utc - 1.day).strftime("%Y%m%d")
    Translation::ExportUntranslatedStrings.new(key: key).export
  end
end

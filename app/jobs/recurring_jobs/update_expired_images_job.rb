# Aim: Cards with expired image URLs must be updated

class RecurringJobs::UpdateExpiredImagesJob < EdcastActiveJob
  queue_as :image_upload

  def perform(*args)
    hostnames = Settings.org_hostnames_for_url_cleanup
    organization_ids = Organization.where(host_name: hostnames).pluck(:id)
    Card.where(organization_id: organization_ids)
      .system_generated
      .includes(:resource)
      .find_each do |card|
        CardImageCleanupService.new(card: card).check_and_update_card_images
      end
  end
end
class RecurringJobs::WeeklyUserContentAnalyticsJob < EdcastActiveJob
  queue_as :analytics

  def perform(*args)
    Organization.exclude_default_org.analytics_reporting_enabled.each do |organization|
      settings = organization.get_settings_for('AnalyticsReportingConfig') || {}
      next unless settings[:content_activity_feed]

      UserContentAnalyticsJob.perform_now(organization.id)
    end
  end
end

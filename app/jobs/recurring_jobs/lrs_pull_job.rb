class RecurringJobs::LrsPullJob < EdcastActiveJob
  queue_as :lrs_integration

  def perform
    last_run_at = BackgroundJob.where(job_class: "RecurringJobs::LrsPullJob").order(finished_at: :desc).first.try(:started_at)

    # Run for only those orgs for which our LRS is enabled
    lrs_org_ids = XapiCredential.where("end_point like 'https://lrs.edcast.com/%'").pluck(:organization_id)
    lrs_org_ids.each do |org_id|
      Lrs::OrgCourses.new(org_id: org_id, last_run_at: last_run_at).run
    end
  end
end

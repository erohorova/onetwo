class RemindCuratorToCurateJob < EdcastActiveJob
  def perform(opts = {})
    User.joins(:channels_curators).where(channels_curators: {channel_id: opts[:channel_ids]}).
    not_is_edcast_admin.
    not_suspended.
    distinct.each do |curator|
      if curator.channels_to_curate.present?
      	EDCAST_NOTIFY.trigger_event(Notify::EVENT_REMIND_CURATOR, curator)
      end
    end
  end
end
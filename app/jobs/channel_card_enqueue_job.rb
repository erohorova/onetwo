class ChannelCardEnqueueJob < ActiveJob::Base
  queue_as :feed

  def perform(channel_id, card_id)
    Octopus.using(:replica1) do
      card = Card.excluding_default_org.find_by(id: card_id)

      unless card
        log.warn "no card for #{card_id}"
        return
      end

      channel_ids = ChannelsCard.where(card_id: card.id).pluck(:channel_id)
      Follow.followers_to_enqueue(
        followable_id: channel_id,
        followable_type: 'Channel',
        card_id: card.id,
        channel_ids: channel_ids
      )
    end
  end
end

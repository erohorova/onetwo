class AddUserToWhitelistedTeamsJob < EdcastActiveJob
  queue_as :team

  def perform(user_id:)
    user = User.find_by(id: user_id)
    domain = user&.email&.split("@")&.last

    if domain.present?
      query = Team.search body: {query: {term: {domains: domain}}}
      teams = query.results

      if teams.present?
        teams.each do |team|
          team = Team.find_by(id: team.id)
          unless team
            log.warn "Team with id: #{team.id} was not found"
            return
          end
          team.add_to_team([user.id], as_type: "member")
        end
      end
    end
  end
end

class EclCardSyncJob < EdcastActiveJob
  queue_as :cards

  # NOTE:
  #   Executes the job maximum 10 times.
  #   In the last iteration, it does not execute the #callback proc.
  include ActiveJob::Retry.new(
    strategy: :exponential, limit: 10,
    callback: proc do |exception, delay|
      # will be run before each retry
      Rails.logger.debug "Retrying EclCardSyncJob in #{delay.to_s} seconds with job_id: #{self.job_id}"
    end
  )

  def perform(card_id)
    EclApi::CardConnector.new.sync_ecl_card(card_id)
  end
end

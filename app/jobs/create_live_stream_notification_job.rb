class CreateLiveStreamNotificationJob < EdcastActiveJob
  queue_as :notifications

  def perform(video_stream_id:)
    video_stream = VideoStream.find_by(id: video_stream_id)

    # Precedence of notifications in the following order:
    # 1. If user is the follower of the author
    # 2. If the user is in the group the livestream is posted to
    # 3. If the user is in the channel the livestream is posted to

    # 1. If user is the follower of the author
    EDCAST_NOTIFY.trigger_event(Notify::EVENT_NEW_LIVESTREAM, video_stream.card)

    # 2. If the user is in the group the livestream is posted to
    video_stream.teams_cards.each do |teams_card|
      EDCAST_NOTIFY.trigger_event(Notify::EVENT_LIVE_STREAM_IN_GROUP, teams_card)
    end

    # 3. If the user is in the channel the livestream is posted to
    video_stream.channels_cards.each do |channels_card|
      custom_hash = {}
      team_ids = video_stream.card.shared_with_team_ids if video_stream.teams_cards.present?
      if team_ids.present?
        custom_hash[:team_ids_notified] = team_ids
      end
      EDCAST_NOTIFY.trigger_event(Notify::EVENT_NEW_CONTENT_IN_CHANNEL, channels_card, custom_hash)
    end
  end
end

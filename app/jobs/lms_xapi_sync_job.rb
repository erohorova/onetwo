class LmsXapiSyncJob < ActiveJob::Base
  queue_as :lms_integration

  def perform(record_id)
    LmsIntegration::XapiCredential.new(record_id).sync_xapi_credentials
  end
end


class LogHistoryJob < EdcastActiveJob
  queue_as :versioning

  def perform(opts = {})
    VersioningService.new(
      entity_type: opts["entity_type"],
      entity_id: opts["entity_id"],
      changes: opts["changes"],
      association_changes: opts["association_changes"],
      user_id: opts["user_id"]
    ).create_version
  end
end

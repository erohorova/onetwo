class TeamUsersMailerJob < EdcastActiveJob
  queue_as :mailers

  def perform(message: nil, team_id: nil, sender_id: nil)
    team = Team.find_by(id: team_id)

    if team && message.present?
      team_users = team.users 

      team_users.each do |user|
        TeamUsersMailer.send_notification({ 
          message: message,
          user_id: user.id,
          sender_id: sender_id,
          team_id: team.id
        }).deliver_now
      end 
    end
  end
end

class TeamUsersCleanupJob < ActiveJob::Base
  queue_as :team

  # should be passed a PLAIN RUBY OBJECT to ensure access to the team
  # attributes, even if it's been previously deleted!
  def perform(team:)
    team = RecursiveOpenStruct.new team
    teams_users = TeamsUser.where(team_id: team.id)
    teams_users.find_each do |teams_user|
      # Here we create a team record from the PORO and assign it to the team user.
      teams_user.team = Team.new(team.to_h)
      teams_user.destroy
    end
  end
end

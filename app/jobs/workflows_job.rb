class WorkflowsJob < EdcastActiveJob
  queue_as :bulk
  
  def perform(patch_id:)
    patch = WorkflowPatchRevision.find_by(id: patch_id)
    return unless patch
    patch.set_processing(self.job_id)

    org = Organization.find_by(id: patch.organization_id)
    unless org
      error = {
        organization: "Can't found organization. ID: #{patch.organization_id}, Host name: #{patch.org_name}"
      }
      patch.update_errors(error)
      patch.update_attributes(in_progress: false, completed: true)
      return
    end

    send("proceed_#{patch.workflow_type}", patch, org)

    patch.update_attributes(completed: true, in_progress: false)
    patch.update_info({finished_at: Time.current})
  end

  def proceed_groups(patch, org)
    return unless org
    external_data = patch.data
    dynamic_group = org.teams.find_by(name: external_data['group_name'])
    dynamic_group_revision = DynamicGroupRevision.find_by(uid: external_data['id'])

    case patch.patch_type
    when 'insert'
      # no changes
      return if dynamic_group && dynamic_group_revision
      default_description = 'Group created using Workflow'
      # create dynamic group if it not present
      unless dynamic_group&.name == external_data['group_name']
      # race condition fix EP-20905
        begin
          dynamic_group = Team.create(
            organization_id: org.id,
            name: external_data['group_name'],
            description: default_description,
            is_private: true,
            is_dynamic: true,
            is_mandatory: true,
            auto_assign_content: true
          )
        rescue ActiveRecord::RecordNotUnique => e
          fail_info = {
            exception: {
              inspect: e.inspect,
              backtrace: e&.backtrace&.first(10) || []
            }
          }
          dynamic_group = org.teams.find_by(name: external_data['group_name'])
          unless dynamic_group
            patch.update_errors(fail_info)
            patch.update_attributes(in_progress: false)
            return
          end
        end
        #end race condition fix EP-20905
      end
      # in this case for patch identified as regular workflow
      #   and need to add to the team a user with ID: external_data['id']
      if dynamic_group.present? && !dynamic_group_revision
        usr = org.users.find_by(id: external_data['id'])
        if usr
          dynamic_group.add_to_team([usr.id])
        else
          err = { user: "User not found. ID: #{external_data['id']}" }
          patch.update_errors(err)
        end

        return
      end

      if dynamic_group_revision.nil? || dynamic_group.id.nil?
       patch.update_errors({ group: 'Creation failed'})
       return
      end

      dynamic_group_revision.update_attributes(group_id: dynamic_group.id)
      # insert creator of dynamic group as leader(BE -> admin)
      dynamic_group.add_to_team(
        [dynamic_group_revision.user_id],
        as_type: 'admin'
      )
      proceed_dynamic_group_action(dynamic_group_revision, org, dynamic_group)
    when 'update'
      # add user to new team - regular workflow
      unless dynamic_group&.name == external_data['group_name']
        dynamic_group = Team.create(
          organization_id: org.id,
          name: external_data['group_name'],
          description: 'Group created using Workflow',
          is_private: true,
          is_dynamic: true,
          is_mandatory: true,
          auto_assign_content: true
        )
      end

      # --------fix for EP-20905 start
      if dynamic_group.present? && !dynamic_group_revision
        usr = org.users.find_by(id: external_data['id'])
        if usr
          patches_related_to_user = WorkflowPatchRevision.where(
            workflow_id: patch.workflow_id,
            data_id: usr.id.to_s,
            version: 0
          )
          group_names = patches_related_to_user.map do |previous_patch|
            name = previous_patch.data['group_name']
            next if external_data['group_name'] == name
            name
          end

          dynamic_groups = Team.where(name: group_names.compact.uniq)
          if dynamic_groups.empty?
            dynamic_group.add_to_team([usr.id])
          else
            team_users = TeamsUser.where(
             team_id: dynamic_groups.pluck(:id), user_id: usr.id
            )
            team_users.each{ |team_user| team_user.is_workflow = true }
            team_users.destroy_all
            dynamic_group.add_to_team([usr.id])
          end
        else
          err = { user: "User not found. ID: #{external_data['id']}" }
          patch.update_errors(err)
        end
      end
      # --------fix for EP-20905 end
    when 'delete'
      return unless dynamic_group
      # if dynamic group && dynamic group revision
      #    destroy both
      # else
      #    make dynamic group only private
      if dynamic_group.id == dynamic_group_revision&.group_id
        dynamic_group.destroy
        dynamic_group_revision.destroy
      end
      # --------fix for EP-20905 start
      unless dynamic_group_revision
        # all groups in context of workflow
        dynamic_groups = WorkflowPatchRevision.where(
          workflow_id: patch.workflow_id, version: 0
        )
        dynamic_group_names = dynamic_groups.map do |group|
          group.data['group_name']
        end
        org.teams.where(name: dynamic_group_names).each do |team|
          if team.is_mandatory && team.is_dynamic
            team.update_attributes(
              is_mandatory: false,
              is_dynamic: false,
              is_private: true
            )
          end
        end
      end
      # --------fix for EP-20905 end
    end
  end

  def proceed_users_groups(patch, org)
    return unless org
    external_data = patch.data
    dynamic_group_revision = DynamicGroupRevision.find_by(uid: external_data['group_id'])
    return unless dynamic_group_revision
    dynamic_group = org.teams.find_by(id: dynamic_group_revision.group_id)
    return unless dynamic_group
    user = org.users.find_by(id: external_data['id'])
    unless user
      err = { user: "User not found. ID: #{external_data['id']}" }
      patch.update_errors(err)
      return
    end
    team_user = dynamic_group.teams_users.find_by(user_id: user.id)
    case patch.patch_type
    when 'insert'
      return if team_user
      dynamic_group.add_to_team([user.id])
    when 'update'
      # TODO: plan for next phase
    when 'delete'
      return unless team_user
      team_user.is_workflow = true
      team_user.destroy
    end
  end

  def proceed_dynamic_group_action(dynamic_group_rev, org, dynamic_group)
    return unless dynamic_group_rev.action.in?(%w[assign share])
    card = org.cards.find_by(id: dynamic_group_rev.item_id)
    return unless card
    user = org.users.find_by(id: dynamic_group_rev.user_id)
    case dynamic_group_rev.action
    when 'assign'
      date = dynamic_group_rev.created_at.strftime("%m/%d/%Y")
      opts = {}
      opts[:due_at] = date
      opts[:start_date] = date
      opts[:self_assign] = true
      opts[:message] = dynamic_group_rev.message
      card.create_assignment_for_teams(
        team_ids: [dynamic_group.id],
        assignor: user,
        opts: opts
      )
    when 'share'
      card.share_card(
        team_ids: [dynamic_group.id],
        user: user
      )
    end
  end

  def handle_error exception
    fail_info = {
      exception: {
        inspect: exception.inspect,
        backtrace: exception&.backtrace&.first(10) || []
      }
    }
    patch = WorkflowPatchRevision.find_by(job_id: self.job_id)
    if patch
      patch.update_errors(fail_info)
      patch.update_attributes(in_progress: false)
    end

    super
  end
end

class DailyDigestEmailGeneratorJob < EdcastActiveJob
  queue_as :forum

  def perform
    # set default Time
    now       = Time.now.utc
    batch_job = BatchJob.where(name: self.class.name)

    # pick existing OR create new one
    if batch_job.any?
      record = batch_job.first
      last_run_time = record.last_run_time
      record.update_attributes!(last_run_time: now)
    else
      batch_job.create(last_run_time: now, name: self.class.name)
      last_run_time = now - 1.day
    end

    # Collect group with preference values
    groups = ResourceGroup
      .not_closed
      .with_digest_enabled
      .joins("inner join user_preferences up on up.preferenceable_id = groups.id and up.preferenceable_type='Group'")
      .where("groups.end_date > (:last_run_time) or groups.end_date is null",
        last_run_time: last_run_time)

    users_that_have_daily_digest_turned_off = {}
    groups
      .where("up.key = 'notification.daily_digest.opt_out' and up.value = 'true'")
      .select('groups.id, up.user_id')
      .each { |g| (users_that_have_daily_digest_turned_off[g.id] ||= []) << g.user_id }
    ResourceGroup
      .not_closed
      .with_digest_enabled
      .joins(organization: :client)
      .where("groups.end_date > (:last_run_time) OR groups.end_date IS NULL", last_run_time: last_run_time)
      .uniq
      .find_in_batches(batch_size: 100) do |batch|
      batch.flatten.each do |group|
        starttime = Time.now
        log.info "Sending daily digest for group id: #{group.id}"
        posts = Post.where(postable_id: group.id, postable_type: 'Group')
        # posts from a group
        pinned_posts = posts.where("pinned = true AND pinned_at >= (:last_run_time)", last_run_time: last_run_time)

        commented_posts = posts
          .joins("inner join comments c on c.commentable_type = 'Post' and c.commentable_id = posts.id")

        # most replied posts from a group
        most_replied_posts = commented_posts
          .select("COUNT(c.id) as comments_count, posts.*")
          .where("c.created_at >= (:last_run_time)", last_run_time: last_run_time)
          .group('posts.id')
          .having('comments_count > 0')
          .order('comments_count desc')

        comment_ids = commented_posts.select('c.id').pluck(:id).uniq
        # posts from a group with approved comments
        approved_posts = commented_posts.joins("inner join approvals a on a.approvable_id = c.id and a.approvable_type = 'Comment'")
          .where("a.created_at >= (:last_run_time)",
            last_run_time: last_run_time)

        # most upvoted posts from a group
        upvoted_posts = posts
          .joins("inner join votes v on v.votable_id = posts.id and v.votable_type = 'Post'")
          .where("v.created_at >= (:last_run_time)", last_run_time: last_run_time)

        aggregated_posts = pinned_posts | most_replied_posts | approved_posts | upvoted_posts
        default_posts = posts.where("created_at >= (:last_run_time)", last_run_time: last_run_time).order('id desc')

        posts_users_hash = User.where(id: (aggregated_posts.map(&:user_id) + default_posts.map(&:user_id))).index_by(&:id)
        posts_resources_hash = Resource.where(id: (aggregated_posts.map(&:resource_id) + default_posts.map(&:resource_id))).index_by(&:id)

        begin
          group_hash = { 'group_name' => group.name,
            "group_id" => group.id,
            "group_image" => "https:#{group.image || ActionController::Base.helpers.asset_path('card_default_image_min.jpg')}",
            "group_term" => group.course_term,
            "group_url" => group.website_url,
            "group_organization" => group.app_name,
            'mailer_config_id' => group.mailer_config.try(:id) }
          urls_hash = Hash[(aggregated_posts + default_posts).map{|p| [p.id, Post.url(p, group)]}]

          emails_count = 0
          group.users.where.not(id: users_that_have_daily_digest_turned_off[group.id]).each do |user|
            filtered_posts = (aggregated_posts.reject{|p| p.user_id == user.id} | default_posts.reject{|p| p.user_id == user.id}).take(5)

            if filtered_posts.size >= 3
              posts_collection = []
              filtered_posts.each do |post|
                post_user = posts_users_hash[post.user_id]
                post_resource = posts_resources_hash[post.resource_id]
                post_hash = post.as_json
                post_hash.merge!("creator_name" => post_user.name, "creator_avatar" => "#{post_user.photo}", "deep_url" => urls_hash[post.id])
                if post_resource.present?
                  post_hash.merge!("resource_site" => post_resource.site_name, "resource_title" => post_resource.title, "resource_desc" => post_resource.description, "resource_img" => post_resource.image_url.present? ? post_resource.image_url : "https:#{ActionController::Base.helpers.asset_path('card_default_image_min.jpg')}")
                else
                  post_hash.merge!("resource_site" => '', "resource_title" => '', "resource_desc" => '', "resource_img" => '')
                end
                posts_collection << post_hash
              end

              emails_count += 1
              UserMailer.send_daily_digest(group_hash, user, posts_collection).deliver_now
            end
          end
        rescue StandardError => e
          log.error "Error: #{e.message} for group id: #{group.id}"
        end

        endtime = Time.now
        log.info "Done sending daily digest for group id: #{group.id}. Sent emails to #{emails_count} users. Time taken: #{endtime-starttime} second"
      end
    end
  end
end

class OrgUserSendWeeklyActivityEmailBatchJob < EdcastActiveJob
  queue_as :batch

  def perform(organization_id:, user_ids:)
    User.select(:id).where(id: user_ids).each do |user|
      OrgUserSendWeeklyActivityEmailJob.perform_later(organization_id, user.id) if user.pref_notification_weekly_activity
    end
  end
end

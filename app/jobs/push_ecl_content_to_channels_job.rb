class PushECLContentToChannelsJob < ActiveJob::Base
  queue_as :cards

  def perform(organization_id:, channel_ids: [], ecl_ids: [])
    ecl_ids.each do |ecl_id|
      service = EclApi::CardConnector.new(ecl_id: ecl_id, organization_id: organization_id)
      card = service.card_from_ecl_id
      organization = Organization.find_by(id: organization_id)

      if card && organization
        channel_ids = organization.channels.not_private.where(id: channel_ids).pluck :id
        card.channel_ids = card.channel_ids | channel_ids
        card.reindex_async if card.save
      end
    end
  end
end

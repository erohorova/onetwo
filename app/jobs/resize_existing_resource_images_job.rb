class ResizeExistingResourceImagesJob < EdcastActiveJob
  queue_as :existing_resource_images

  def perform(resource_id: )
    array_of_resources = []
    resource = Resource.find_by(id: resource_id)
    resource_url = resource&.image_url
    @original_resource_handle = resource_url&.split("/")[-1]
    resized_image = Filestack::ProcessImage.new(url: resource_url).resize
    if resized_image["status"] == 'failed'
      resource_id = find_large_image_resources(@original_resource_handle, resource.id)
      array_of_resources << resource_id
    else
      new_resource = resource_update(resource: resource, url:resource_url, resized_image_options:resized_image)
      if new_resource.present?
        @new_resource_handle = resized_image["url"]&.split("/")[-1]
        Filestack::ProcessImage.delete_original_images(@original_resource_handle) if (@original_resource_handle != @new_resource_handle)
      end
    end
    log.warn "large image resources:: #{array_of_resources.compact}" if array_of_resources.compact.length > 0
  end

  def resource_update(resource:, url:, resized_image_options:)
    @resized_image_options = resized_image_options.with_indifferent_access
    return if url == @resized_image_options[:url]
    resource_image = @resized_image_options[:url]
    resource.update_attribute(:image_url, resource_image)
  end

  def find_large_image_resources(handle, resource_id)
    size_info = Filestack::ProcessImage.image_size(handle)
    if size_info.present? && size_info.has_key?('height')
      image_in_pixels = size_info["height"] * size_info["width"]
      image_size_filestack_limit = 100_000_000
      if image_in_pixels > image_size_filestack_limit
        resource_id
      end
    end
  end
end

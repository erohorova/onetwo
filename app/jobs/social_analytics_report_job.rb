class SocialAnalyticsReportJob < ActiveJob::Base
  queue_as :analytics

  def perform(organization_id)
    organization = Organization.find(organization_id)
    organization.send_user_content_analytics(report_type: 'social')
  end
end

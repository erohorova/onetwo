class ChannelProgrammingBatchJob < EdcastActiveJob
  extend SetUniqueJob
  queue_as :content_source

  def self.unique_args(args)
    [self.name]
  end

  def perform
    #We will get organizations where channel_programming_v2=true.
    #Then will execute this job for  channels with organization.
    organizations_v2 = Organization.get_config_enabled_orgs('channel_programming_v2', 'true').pluck(:configable_id)
    Channel.where(organization_id: organizations_v2).find_in_batches.each do |batch|
      batch.each do |channel|
        ChannelProgrammingJob.perform_later(channel.id)
      end
    end
  end	
end
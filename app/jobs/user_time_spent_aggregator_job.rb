class UserTimeSpentAggregatorJob < EdcastActiveJob
  queue_as :analytics

  def perform(attributes)
    unless attributes[:actor_id].nil?
      time_increment = UserSession.update_session(attributes[:actor_id], Time.at(attributes[:created_at_to_i]))
      UserLevelMetric.increment_time_spent({user_id: attributes[:actor_id]}, time_increment, Time.at(attributes[:created_at_to_i]))

      topics = MetricRecord.get_topics_for_object(attributes)

      topics.each do |topic|
        UserTopicLevelMetric.increment_time_spent({user_id: attributes[:actor_id], tag_id: topic.id}, time_increment, Time.at(attributes[:created_at_to_i]))
      end
    end
  end
end

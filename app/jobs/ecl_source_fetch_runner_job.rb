class EclSourceFetchRunnerJob < EdcastActiveJob
  extend SetUniqueJob

  queue_as :content_source

  def self.unique_args(args)
    [self.name]
  end

  def perform
    # Can't use Rails' find_in_batches here because it paginates. When we process the first 100
    # results, we update them so that they no longer show up in the next query. In this case,
    # pagination is not required. Rails' way would simply skip these records instead of processing
    # them.
    while true
      sources = runnable_ecl_sources
      return if sources.blank?

      sources.each do |source|
        # Should update fail, we raise and exception and stop execution, so that we
        # don't enter an infinite loop
        source.update!(next_run_at: Time.zone.now + source.rate)
        EclSourceFetchJob.set(wait: Random.rand(30).minutes).perform_later(source.id)
      end
    end
  end

  def runnable_ecl_sources
    #We will get organizations where channel_programming_v2=true.
    #Then will exclude channels with organization from this Job(not condition)
    organizations_v2 = Organization.get_config_enabled_orgs('channel_programming_v2', 'true').pluck(:configable_id)
    sources = EclSource.joins(:channel).where('next_run_at is NULL OR next_run_at < ?', Time.zone.now).where.not(channels: {organization_id: organizations_v2}).limit(100)
    sources
  end
end

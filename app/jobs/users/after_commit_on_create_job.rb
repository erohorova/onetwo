class Users::AfterCommitOnCreateJob < EdcastActiveJob
  queue_as :default

  def perform(user_id:)
    user = User.find_by_id user_id
    return unless user

    # protected methods
    user.send :add_to_org_general_channel
    user.send :cleanup_invitation

  end
end

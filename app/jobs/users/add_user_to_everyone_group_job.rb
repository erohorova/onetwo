class Users::AddUserToEveryoneGroupJob < EdcastActiveJob
  queue_as :users

  def perform(user_id:)
    user = User.find_by_id user_id
    return unless user

    team = user.organization.everyone_team
    if !team.nil? && user.confirmed? && !user.email.blank?
      team.add_user(user, as_type: user.organization_role)
    end
  end
end

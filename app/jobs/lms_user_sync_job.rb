class LmsUserSyncJob < ActiveJob::Base
  queue_as :lms_integration

  def perform(user_id)
    LmsIntegration::UserSettings.new(user_id).setup_user
  end
end

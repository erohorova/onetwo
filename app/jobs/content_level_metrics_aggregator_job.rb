class ContentLevelMetricsAggregatorJob < EdcastActiveJob
  queue_as :analytics

  def perform(attributes)

    return if (attributes[:object_id].nil? || attributes[:object_type].nil?)

    increments = []

    # Views
    if MetricRecord.is_consumption_event?(attributes)
      increments << [:views_count, 1]
    # Comments
    elsif MetricRecord.is_comment_event?(attributes)
      increments << [:comments_count, 1]
    elsif MetricRecord.is_uncomment_event?(attributes)
      increments << [:comments_count, -1]
    # Likes
    elsif MetricRecord.is_like_event?(attributes)
      increments << [:likes_count, 1]
    elsif MetricRecord.is_unlike_event?(attributes)
      increments << [:likes_count, -1]
    # Mark Completed
    elsif MetricRecord.is_mark_completed_event?(attributes)
      increments << [:completes_count, 1]
    # Mark Uncompleted
    elsif MetricRecord.is_mark_uncompleted_event?(attributes)
      increments << [:completes_count, -1]
    # Bookmarks
    elsif MetricRecord.is_bookmark_event?(attributes)
      increments << [:bookmarks_count, 1]
    end

    if increments.present?
      ContentLevelMetric.update_from_increments(opts: {content_id: attributes[:object_id], content_type: attributes[:object_type]}, increments: increments, timestamp: Time.at(attributes[:created_at_to_i]))
    end
  end
end

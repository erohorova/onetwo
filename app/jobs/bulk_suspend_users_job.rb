class BulkSuspendUsersJob < EdcastActiveJob
  queue_as :batch

  def perform(file_id:, admin_id:)
    BulkSuspendUsers.new(
      file_id: file_id,
      admin_id: admin_id
    ).import
  end
end
class AccessLogJob < EdcastActiveJob
  queue_as :analytics

  def perform(opts = {})
    begin
      is_signup_event = opts[:access_uri].try(:start_with?, 'signup') && opts[:annotation] != "repeat_signup"
      query = ActiveRecord::Base.send(:sanitize_sql_array, ['INSERT INTO access_logs (user_id, platform, access_at, access_uri, annotation, created_at, is_signup_event) VALUES(?, ?, ?, ?, ?, ?, ?)',
                                                            opts[:user_id], opts[:platform], opts[:access_at_date], opts[:access_uri], opts[:annotation], opts[:created_at], is_signup_event])
      ActiveRecord::Base.connection.raw_connection.query(query)
      Tracking::track_daily(opts)
    rescue ActiveRecord::RecordNotUnique => e
    rescue Mysql2::Error => e
      if !(e.to_s.include?('index_access_logs_on_user_id_and_platform_and_access_at'))
        log.error e
      end
    rescue Exception => e
      log.error e
    end
  end
end

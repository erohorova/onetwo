class BulkImportEmbargoesJob < EdcastActiveJob
  queue_as :bulk

  def perform(file_id:)
    BulkImportEmbargo.new(file_id: file_id).call
  end
end

class CardDeletionJob < EdcastActiveJob
  queue_as :cards

  def perform(card_id)

    # Old approach, where we used to set deleted = true
    # pack_cards = CardPackRelation.where(from_id: card_id)
    # pack_cards.update_all(deleted: true)
    # journey_packs = JourneyPackRelation.where(from_id: card_id)
    # cover_ids = pack_cards.uniq.pluck(:cover_id)
    # Card.where(id: cover_ids).find_each { |card| card.reindex_card }
    # if journey_packs.any?
    #   journey_packs.update_all(deleted: true)
    #   journey_packs_ids = journey_packs.uniq.pluck(:cover_id)
    #   Card.where(id: journey_packs_ids).find_each { |card| card.reindex_card }
    # end

    # deleting the card from all pathways and journeys
    pack_cards = CardPackRelation.where(from_id: card_id)
    cover_ids = pack_cards.pluck(:cover_id)
    cover_ids.each do |cover_id|
      CardPackRelation.remove_from_pack(
        cover_id: cover_id,
        remove_id: card_id,
        remove_type: 'Card',
        destroy_hidden: false
      )
    end
  end

end

class OrgUserSendWeeklyActivityEmailJob < EdcastActiveJob
  queue_as :weekly_activity

  def perform(org_id, user_id)
    org  = Organization.find(org_id)
    user = User.find(user_id)

    activity = org.weekly_activity_mail_attributes
    score_stats = UserLevelMetric.fetch_stats_for_weekly_activity_email(organization_id: org.id, user_id: user.id)

    UserMailer.weekly_activity(org, [user], activity, score_stats).deliver_now
  end
end

class OrgLevelMetricsAggregatorJob < EdcastActiveJob
  queue_as :analytics

  def perform(attributes)
    if !attributes[:actor_id].nil? && MetricRecord.should_log_org_level_metric?(attributes)
      user = User.find(attributes[:actor_id])
      OrganizationLevelMetric.increment_events_count({
        organization_id: user.organization_id,
        user_id: user.id,
        content_type: attributes[:object_type],
        action: attributes[:action]},
      1,
      Time.at(attributes[:created_at_to_i]))
    end
  end
end

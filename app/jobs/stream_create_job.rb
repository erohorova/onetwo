class StreamCreateJob < EdcastActiveJob
  queue_as :forum

  def perform(opts)
    item_type = opts[:item_type]
    item_id = opts[:item_id]
    user_id = opts[:user_id]
    group_id = opts[:group_id]
    action = opts[:action]
    created_at = opts[:created_at]
    item = item_type.constantize.find_by(id: item_id)
    if item.nil?
      log.warn "Stream creation failed for item type: #{item_type}, id: #{item_id}" and return
    end
    stream = Stream.new(action: action, item: item, user_id: user_id, group_id: group_id, created_at: created_at)
    unless stream.save
      log.warn("Unable to create stream: stream=#{stream.to_json} errors=#{stream.errors.to_json}")
    end
  end
end

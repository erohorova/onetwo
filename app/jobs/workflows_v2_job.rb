class WorkflowsV2Job < EdcastActiveJob
  queue_as :bulk

  # TODO: move `support` methods to class/module

  def perform(patch_ids:)
     patches = WorkflowPatchRevision.where(id: patch_ids)
     return if patches.empty?
     patches.each do |patch|
       patch.set_processing(self.job_id)

       org = Organization.find_by(id: patch.organization_id)
       unless org
         error = {
           organization: "Can't found organization. ID: #{patch.organization_id}, Host name: #{patch.org_name}"
         }
         patch.update_errors(error)
         patch.update_attributes(in_progress: false, completed: true)
         return
       end

       send("proceed_#{patch.workflow_type}", patch, org)

       patch.update_attributes(completed: true, in_progress: false)
       patch.update_info({finished_at: Time.current})
     end
  end

  def proceed_groups(patch, org)
    return unless org
    external_data = patch.data
    case patch.patch_type
    when 'insert'
      create_dynamic_group(
        org: org,
        patch: patch,
      )
    when 'update'
      # when group uid mapping changes will be applied insert/delete patches
      # in other cases update changing only options of existed dynamic groups
      dynamic_group = org.dynamic_group_revisions
        .find_by(uid: external_data['id'])&.dynamic_group
      unless dynamic_group
        patch.update_errors({dynamic_group: 'Not found'})
        return
      end
      dynamic_group.update_attributes(identify_group_rules(external_data))
    when 'delete'
      dynamic_group_revision = org.dynamic_group_revisions.find_by(
        uid: external_data['id']
      )
      unless dynamic_group_revision
        patch.update_errors({group: 'Dynamic group does not exist'})
        return
      end
      dynamic_group_revision.destroy
    end
  end

  def proceed_users_groups(patch, org)
    write_error = proc do |msg|
      patch.update_errors(msg)
      return
    end
    write_error.call({organization: 'Not found'}) unless org
    external_data = patch.data
    dynamic_group_revision = org.dynamic_group_revisions.find_by(
      uid: external_data['group_id']
    )
    write_error.call({dynamic_group_revision: 'Not found'}) unless dynamic_group_revision
    dynamic_group = dynamic_group_revision.dynamic_group
    write_error.call({dynamic_group: 'Not found'})  unless dynamic_group
    user = identify_user(
      id: external_data['user_id'],
      id_type: external_data['id_type'],
      org: org
    )
    write_error.call(
      { user: "User not found. ID: #{external_data['user_id']}" }
    ) unless user
    team_user = dynamic_group.teams_users.find_by(user_id: user.id)

    case patch.patch_type
    when 'insert'
      return if team_user
      user_type = identify_user_type(external_data['member_type'])
      dynamic_group.add_to_team([user.id], as_type: user_type)
    when 'update'
      # update only user type
      user_type = identify_user_type(external_data['member_type'])
      dynamic_group.add_to_team([user.id], as_type: user_type)
    when 'delete'
      return unless team_user
      team_user.is_workflow = true
      team_user.destroy
    end
  end

  def proceed_dynamic_group_action(dynamic_group_rev, org, dynamic_group)
    return unless dynamic_group_rev.action.in?(%w[assign share])
    dynamic_group_rev.update_attributes(group_id: dynamic_group.id)
    # insert creator of dynamic group as leader(BE -> admin)
    dynamic_group.add_to_team([dynamic_group_rev.user_id], as_type: 'admin')
    card = org.cards.find_by(id: dynamic_group_rev.item_id)
    return unless card
    user = org.users.find_by(id: dynamic_group_rev.user_id)
    case dynamic_group_rev.action
    when 'assign'
      date = dynamic_group_rev.created_at.strftime("%m/%d/%Y")
      opts = {}
      opts[:due_at] = date
      opts[:start_date] = date
      opts[:self_assign] = true
      opts[:message] = dynamic_group_rev.message
      card.create_assignment_for_teams(
        team_ids: [dynamic_group.id],
        assignor: user,
        opts: opts
      )
    when 'share'
      card.share_card(
        team_ids: [dynamic_group.id],
        user: user
      )
    end
  end

  def create_dynamic_group(org:, patch:)
    group_data = patch.data.dup
    existed_group = org.teams.find_by(name: group_data['id'])
    existed_dynamic_group_revision = org.dynamic_group_revisions.find_by(
      uid: group_data['id']
    )

    # fix occasion when dynamic group not bonded to existed dynamic group revision
    if existed_group&.is_dynamic && existed_dynamic_group_revision
      existed_dynamic_group_revision.update_attributes(group_id: existed_group.id)
      patch.update_info({group: 'dynamic group is bind to dynamic group revision'})
      return
    end

    dynamic_group = existed_dynamic_group_revision&.dynamic_group

    unless dynamic_group.present?
      safe_name = safe_name_definition(existed_group&.name, org) || group_data['id']

      params = {
        organization_id: org.id,
        name: safe_name,
        description: 'Group created using Workflow',
        auto_assign_content: true
      }

      params.merge!(identify_group_rules(group_data))
      dynamic_group = Team.create(params)
    end

    dynamic_group_rev = identify_group_revision(
      group: dynamic_group,
      uid: group_data['id']
    )

    # write errors
    if dynamic_group_rev.nil? || dynamic_group.nil?
     patch.update_errors({ group: 'creation failed'})
     return
    end

    # processed dynamic-selection action
    proceed_dynamic_group_action(dynamic_group_rev, org, dynamic_group)
  end

  def safe_name_definition(name, org)
    # TODO: refactor name definition
    return unless name
    valid_recent_name = org.teams
      .where("name REGEXP '^#{name}[0-9][[:>:]]'")
      .order(name: :desc)
      .limit(1).first
    name_pattern = valid_recent_name&.name || name
    # will increment last character if it int or concat `1`
    if !!(name_pattern =~ /\d+$/)
      return name_pattern.gsub(/\d+$/) { |match| match.to_i + 1 } 
    end  
    name_pattern.concat('1')
  end

  def identify_group_rules(external_data)
    {
      is_private: external_data['group_type'] || true,
      is_mandatory: external_data['mandatory_group'] || true,
      is_dynamic: true
    }
  end

  def identify_group_revision(group:, uid:)
    return unless group
    org = group.organization

    dynamic_group_revision = org.dynamic_group_revisions.find_by(title: group.name)
    if dynamic_group_revision
      if dynamic_group_revision.group_id != group.id
        dynamic_group_revision.update_attributes(group_id: group.id)
      end
    else
      dynamic_group_revision = org.dynamic_group_revisions.create(
        title: group.name, uid: uid, group_id: group.id
      )
    end
    dynamic_group_revision
  end

  def identify_user_type(as_type)
    types_map = {leader: 'admin', admin: 'sub_admin'}.with_indifferent_access
    types_map[as_type.try(:downcase)] || 'member'
  end

  def identify_user(id:, id_type:, org:)
    case id_type
      when 'uid'
        org.profiles.find_by(uid: id)&.user
      when 'email'
        org.users.find_by(email: id)
      else
        org.users.find_by(id: id)
    end
  end

  def handle_error(exception)
    fail_info = {
      exception: {
        inspect: exception.inspect,
        backtrace: exception&.backtrace&.first(10) || []
      }
    }
    patch = WorkflowPatchRevision.find_by(job_id: self.job_id)
    if patch
      patch.update_errors(fail_info)
      patch.update_attributes(in_progress: false)
    end

    super
  end
end

class RecalculateAssignmentPerformanceMetricJob < ActiveJob::Base
  queue_as :team

  def perform(team_id, assignor_id, assignable_id)
    team_assignment_metric = TeamAssignmentMetric.where(
      team_id:      team_id,
      assignor_id:  assignor_id,
      card_id:      assignable_id
    ).first

    return if team_assignment_metric.nil?
    
    status_count = TeamAssignment.joins(:assignment).
      where(team_id: team_id, assignor_id: assignor_id).
      where("assignments.assignable_id = ?", assignable_id).
      group("assignments.state").count

    team_assignment_metric.assigned_count   = status_count[Assignment::ASSIGNED]  || 0
    team_assignment_metric.started_count    = status_count[Assignment::STARTED]   || 0
    team_assignment_metric.completed_count  = status_count[Assignment::COMPLETED] || 0
    team_assignment_metric.save
  end
end

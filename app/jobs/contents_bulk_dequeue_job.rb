class ContentsBulkDequeueJob < EdcastActiveJob
  queue_as :feed

  def perform(opts = {})
    Octopus.using(:replica1) do
      content_ids = opts[:content_ids]
      content_type = opts[:content_type]
      user_ids = opts[:user_ids]

      if content_ids.blank? || content_type.blank?
        log.error "Content Dequeue Job called with bad opts: #{opts}"
        return
      end

      begin
        log.info "Running with opts(contents_ids) -> #{content_ids}"
        and_filters = [{terms: { content_id: content_ids }}, {term: { content_type: content_type }}]
        and_filters << {terms: { user_id: user_ids }} if user_ids.present?
        UserContentsQueue.delete_by_query({
            query: {
              filtered: {
                query: {match_all: {}},
                filter: {
                  and: and_filters}
              }
            }
        })
        if opts[:reenqueue]
          content_type_klass = opts[:content_type].constantize
          content_type_klass.where(id: content_ids).each {|content| content.try(:enqueue_card) }
        end
      rescue => e
        log.error "Bulk dequeue failed for opts: #{opts}. Error: #{e.message}"
      end
    end
  end
end

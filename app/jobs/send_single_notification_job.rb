class SendSingleNotificationJob < EdcastActiveJob
  queue_as :notifications

  def perform(notification_entry_id)
    notification_entry = NotificationEntry.find_by(id: notification_entry_id)
    
    if notification_entry.present? 
      EDCAST_NOTIFY.send_single_notification(notification_entry)
    end
  end
end

class UpdatePercentileForOrgJob < EdcastActiveJob
  queue_as :analytics

  def perform(organization_id, periods_with_offsets)
    user_ids = User.where(
      organization_id: organization_id,
      is_edcast_admin: false,
      is_suspended: false,
      is_active: true).pluck(:id)

    suspended_user_ids = User.where(organization_id: organization_id, is_suspended: true).pluck(:id)
    users_count = user_ids.count
    return if users_count.zero?

    periods_with_offsets.each do |period, offset|
      active_users = UserLevelMetric.where(organization_id: organization_id, period: period, offset: offset).where("time_spent > 0").where.not(user_id: suspended_user_ids).
        select(:id, :smartbites_score).order(smartbites_score: :desc)

      inactive_user_ids = user_ids - active_users.pluck(:id)
      inactive_users_percentile = inactive_user_ids.count * 100 / users_count

      # Bulk update the percentile for all the users with time_spent set to 0
      UserLevelMetric.where(organization_id: organization_id, period: period, offset: offset, time_spent: 0).update_all(percentile: inactive_users_percentile)

      # Handle active users
      scores_to_count = {}
      active_users.each_with_index do |active_user, indx|
        below_or_equal_count = users_count - indx + scores_to_count[active_user.smartbites_score].to_i
        scores_to_count[active_user.smartbites_score] = scores_to_count[active_user.smartbites_score].to_i + 1
        active_user.update_column(:percentile, below_or_equal_count * 100 / users_count)
      end
    end
  end
end

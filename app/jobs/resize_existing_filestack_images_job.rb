class ResizeExistingFilestackImagesJob < EdcastActiveJob
  queue_as :existing_filestack_images

  def perform(card_id: )
    array_of_cards = []
    card = Card.find_by(id: card_id)
    filestacks = card.filestack
    filestacks.each do |filestack|
      @original_handle = filestack["handle"]
      if filestack["mimetype"].present? && filestack_is_image?(mimetype: filestack["mimetype"])
        resized_image = Filestack::ProcessImage.new(url: filestack["url"]).resize
        if resized_image["status"] == 'failed'
          card_id = find_large_imaged_cards(@original_handle, card.id)
          array_of_cards << card_id
        else
          new_filestack = Filestack::UpdateCard.new(card:card, url:filestack["url"], resized_image_options:resized_image).save
          if new_filestack.present?
            @new_handle = resized_image["url"]&.split("/")[-1]
            Filestack::ProcessImage.delete_original_images(@original_handle) if (@original_handle != @new_handle)
          end
        end
      end
    end
    log.warn "large_imaged_cards:  #{array_of_cards.compact}" if array_of_cards.compact.length > 0
  end

  def filestack_is_image?(mimetype:)
    (/^image\/(png|svg\+xml|webp|jpeg|jpg|pjpeg|x-png|gif|tiff)$/.match(mimetype).present?)
  end

  def find_large_imaged_cards(handle, card_id)
    size_info = Filestack::ProcessImage.image_size(handle)
    if size_info.present? && size_info.has_key?('height')
      image_in_pixels = size_info["height"] * size_info["width"]
      image_size_filestack_limit = 100_000_000
      if image_in_pixels > image_size_filestack_limit
        card_id
      end
    end
  end
end

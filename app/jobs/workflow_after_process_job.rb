class WorkflowAfterProcessJob < EdcastActiveJob
  queue_as :workflow

  def perform(model_name: ,ids:)
    data = model_name.safe_constantize.where(id: ids)
    if model_name == 'Team'
      data.find_each do |team|
        # invalidate cache
        TeamsChannelsFollow.where(team_id: team.id).pluck(:channel_id).uniq.each do |channel_id|
          Channel.invalidate_followers_cache(channel_id)
        end
        # reindex team
        IndexJob.perform_later(model_name, team.id)
      end
    end
  end
end
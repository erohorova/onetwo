class QuizQuestionStatsRecorderJob < EdcastActiveJob
  queue_as :analytics

  def perform(opts = {})

    quiz_question_attempt_id = opts[:quiz_question_attempt_id]

    if quiz_question_attempt_id.nil?
      log.error "Quiz Question Stats Recorder Job called with bad opts: #{opts}"
      return
    end

    attempt = QuizQuestionAttempt.find_by(id: quiz_question_attempt_id)

    if attempt.nil?
      log.warn "Bad id: #{quiz_question_attempt_id}"
      return
    end

    tries = 0
    begin
      stats = QuizQuestionStats.find_or_initialize_by(quiz_question: attempt.quiz_question)
      attempts_count = QuizQuestionAttempt.where(quiz_question_id: attempt.quiz_question_id).count
      return if attempts_count == stats.attempt_count
      if (stats.new_record? && attempts_count > 1) || (attempts_count - stats.attempt_count) > 1
        set_stats(attempt.quiz_question_id, stats, attempts_count)
      else
        stats.attempt_count = attempts_count
        stats_data = stats.options_stats || {}
        user_selections = attempt.selections || []
        user_selections.each do |option_id|
          stats_data[option_id.to_s] = (stats_data[option_id.to_s] || 0) + 1
        end
        stats.options_stats = stats_data
      end

      stats.save
    rescue ActiveRecord::StaleObjectError
      if tries < 4
        tries += 1
        retry
      else
        log.error "Couldn't Record stats for attempt: #{quiz_question_attempt_id} after 5 tries. Aborting"
        return
      end
    end
  end

  def set_stats(quiz_question_id, stats, attempts_count)
      attempts = QuizQuestionAttempt.where(quiz_question_id: quiz_question_id)
      attempts_selections = attempts.flat_map(&:selections)
      options_stats = Hash[
        attempts_selections.uniq.map { |t| [t.to_s,attempts_selections.count(t)] }
      ]
      stats.attempt_count = attempts_count
      stats.options_stats = options_stats
      stats
  end
end

class UnsyncCloneChannelJob < EdcastActiveJob
  queue_as :channel

  # opts
  # parent_channel_id: id of parent channel,  
  # action: add/remove
  def perform(opts= {})
    if (parent_channel = Channel.find_by_id(opts[:parent_channel_id])).present? && opts[:action] == 'remove'
      parent_channel.cards.find_each do |parent_card|
        parent_card.remove_cloned_cards
      end
      parent_channel.cloned_channels.destroy_all
    end
  end
end
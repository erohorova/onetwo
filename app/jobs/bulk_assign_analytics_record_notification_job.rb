class BulkAssignAnalyticsRecordNotificationJob < EdcastActiveJob
  queue_as :notifications

  # opts :
  # assignment_ids
  # team_id
  # organization_id
  # team_assignment_ids
  def perform(opts = {})
    organization = Organization.find_by_id(opts[:organization_id])
    assignable = nil
    Assignment.includes(:team_assignments)
      .where(id: opts[:assignment_ids])
      .where(team_assignments: {team_id: opts[:team_id]})
      .find_each(batch_size: 200) do |assignment|

      # card is same for all assignments
      assignable ||= assignment.assignable

      # trigger new assignment notification
      EDCAST_NOTIFY.trigger_event(Notify::EVENT_NEW_ASSIGNMENT, assignment)

      # trigger analytics metric record job for assignment
      Analytics::MetricsRecorderJob.perform_later(
        recorder:        "Analytics::CardAssignmentMetricsRecorder",
        event:           Analytics::CardAssignmentMetricsRecorder::EVENT_CARD_ASSIGNED,
        actor:           Analytics::MetricsRecorder.user_attributes(assignment.assignee),
        org:             Analytics::MetricsRecorder.organization_attributes(organization),
        timestamp:       assignment.updated_at.to_i,
        assignee:        Analytics::MetricsRecorder.user_attributes(assignment.assignee),
        card:            Analytics::MetricsRecorder.card_attributes(assignable),
        additional_data: {}
      )

      # trigger email notification job for team assignments
      team_assignment = assignment.team_assignments.first
      if opts[:team_assignment_ids].include?(team_assignment.id)
        team_assignment.send(:generate_email_notification)
        team_assignment.send(:generate_client_notification)
      end
    end
  end
end
class NewTeamUserAutoFollowJob < EdcastActiveJob
  queue_as :team

  def perform(channel_id, follower_id)
    channel = Channel.find_by(id: channel_id)

    # Since this job is specifically for teams_users, we are sure that the follower is a User.
    follower = User.find_by(id: follower_id)

    TeamsChannelsAutofollowJob.new.after_followed(channel, follower) if channel.present? && follower.present?
  end
end

class DailyLearningForUserJob < ActiveJob::Base
  queue_as :daily_learning

  # Does 
  def perform(user_daily_learning_id)
    user_daily_learning = UserDailyLearning.find(user_daily_learning_id)
    if user_daily_learning.processing_state == DailyLearningProcessingStates::INITIALIZED
      user_daily_learning.process!
    else
      log.warn "User Daily Learning Job for id: #{user_daily_learning_id} called, but no processing done because current state is #{user_daily_learning.processing_state}"
    end
  end
end

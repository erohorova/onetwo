class NotifyUserJob < EdcastActiveJob
  queue_as :notifications

  def perform(opts={})
    action = opts[:action]
    card_ids = opts[:card_ids]
    cover_ids = opts[:cover_ids]
    actor_name = User.find_by(id: opts[:actor_id])&.first_name
    actor_details = "by #{actor_name}" if actor_name.present?
    cover_ids.each do |c|
      cover = Card.find(c)
      # to handle the scenarios when not all cards from the list get added successfully to the pathway
      added_card_ids = CardPackRelation.joins(:card).where("cards.id in (?)", card_ids).pluck(:from_id)
      case action
      when 'added_to'
        message = "View updates #{actor_details} on your completed pathway, '#{cover.title}'"
        notifier(cover, message, action, added_card_ids, true)
      when 'removed_from'
        message = "A SmartCard has been removed #{actor_details} from the completed pathway, '#{cover.title}'"
        notifier(cover, message, action)
      when 'card_updated'
        message = "View updates #{actor_details} on your completed pathway, '#{cover.title}'"
        notifier(cover, message, action, added_card_ids, true)
      end
    end
  end

  def notifier(cover, message, event, added_card_ids=[], visiblity_check=false)
    if cover.published?
      user_ids = UserContentCompletion.where(completable: cover, state: UserContentCompletion::COMPLETED).pluck(:user_id)
      user_ids.each do |user_id|
        user = User.find(user_id)
        if !visiblity_check || (visiblity_check && (user.is_org_admin? || Card.where(id: added_card_ids).visible_to_userV2(user).count > 0))
          NotificationGeneratorJob.perform_later({user_id: user_id, item_id: cover.id, item_type: event, notification_type: event, custom_message: message})
        end
      end
    end
  end
end
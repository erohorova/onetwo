class Red5ProVideoStreamServerUploadJob < EdcastActiveJob
  queue_as :video_stream

  def perform(opts)
    video_stream = Red5ProVideoStream.find(opts[:video_stream_id])
    ssh_key_file_name = File.join(Rails.root, 'tmp', "red5_pro_#{video_stream.uuid}")
    File.write(ssh_key_file_name, ENV['RED5_PRO_SSH_PRIVATE_KEY'])
    File.chmod(0600, ssh_key_file_name)
    video_file_found = false
    original_video_file = "#{Settings.red5_pro.content_dir}/#{video_stream.uuid}.flv"
    _log = log
    SSHKit::Coordinator.new(Settings.red5_pro.servers.split(',').map(&:strip)).each do |host|
      host.ssh_options = {
          keys: [ssh_key_file_name],
          user: 'ec2-user',
          forward_agent: false,
          auth_methods: %w(publickey)
      }
      as user: 'root' do
        key = "#{Settings.red5_pro.s3_upload_prefix}/#{video_stream.id}/server/#{video_stream.uuid}.flv"
        if test("sudo -u root -- sh -c '[ -f #{original_video_file} ]'")
          video_file_found = true
          begin

            file_to_upload = original_video_file


            s3_result_raw = capture(:aws, :s3api, 'put-object', '--bucket', Settings.red5_pro.s3_bucket,
                                    '--key', key, '--body', file_to_upload)
            s3_result_json = ActiveSupport::JSON.decode(s3_result_raw)

            #this will alllow us to rereun the job without unique constraint error
            recording = Recording.find_or_initialize_by(video_stream_id: video_stream.id, sequence_number: 0, source: 'server')

            recording.bucket = Settings.red5_pro.s3_bucket
            recording.key = key
            recording.location = "https://#{Settings.red5_pro.s3_bucket}.s3.amazonaws.com/#{key}"
            recording.checksum = s3_result_json['ETag']

            recording.save!


            #TODO: remove the file from the disk now that it has been uploaded to s3
            #TODO: commenting this out for now. renable this once we're comfortable of not losing data.
            #   execute(:rm, '-f', original_video_file)


          rescue => e
            #log is overwritten by SSHKit
            _log.error "Problem uploading past stream from red5_pro server to s3: video_stream_id=#{video_stream.id} error=#{e.message} backtrace=#{e.backtrace}"
            _log.error e
          end
        end
      end
    end

    unless video_file_found
      log.error "Problem uploading past stream from red5_pro server to s3: video_stream_id=#{video_stream.id} error=FileNotFound file_name=#{original_video_file}"
    end

    File.delete(ssh_key_file_name)
  end
end

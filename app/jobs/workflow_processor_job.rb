class WorkflowProcessorJob < EdcastActiveJob
  queue_as :workflow

  def perform(workflow_id:, model_name:, input_data_ids: nil, changes: nil)
    workflow = Workflow.find_by(id: workflow_id)
    input_data = model_name.safe_constantize.where(id: input_data_ids)
    WorkflowService::Process.new(workflow: workflow, input_data: input_data, changes: changes).run
  end
end
class FollowTeamUsersJob < EdcastActiveJob
  queue_as :team

  def perform(opts = {})
    team_id   = opts.delete(:team_id)
    user_id   = opts.delete(:user_id)
    device_id = opts.delete(:device_id)
    platform  = opts.delete(:platform)

    team = Team.find(team_id)
    user = User.find(user_id)
    return if team.nil? || user.nil?

    ##Find all users of team
    team.users.each do |team_user|
      if !user.following?(team_user) && user != team_user
        follow = Follow.new(user: user, followable: team_user, skip_notification: false)
        unless follow.save
          log.warn("User id=#{user.id} has not autofollowed errors=#{follow.full_errors.to_json}")
        else
          Tracking::track_act(
            user_id: user.id,
            object: 'user',
            object_id: team_user.id,
            action: 'follow',
            platform: platform,
            device_id: device_id,
            organization_id: user.organization_id
          )
        end
      end
    end
  end
end

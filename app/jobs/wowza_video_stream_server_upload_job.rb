class WowzaVideoStreamServerUploadJob < EdcastActiveJob
  queue_as :video_stream

  def perform(opts)
    video_stream = WowzaVideoStream.find(opts[:video_stream_id])
    ssh_key_file_name = File.join(Rails.root, 'tmp', "wowza_#{video_stream.uuid}")
    File.write(ssh_key_file_name, ENV['WOWZA_SSH_PRIVATE_KEY'])
    File.chmod(0600, ssh_key_file_name)
    video_file_found = false
    original_video_file = "#{Settings.wowza.content_dir}/#{video_stream.uuid}.mp4"
    _log = log
    SSHKit::Coordinator.new(Settings.wowza.servers.split(',').map(&:strip)).each do |host|
      host.ssh_options = {
          keys: [ssh_key_file_name],
          user: 'ec2-user',
          forward_agent: false,
          auth_methods: %w(publickey)
      }
      as user: 'root' do
        key = "#{Settings.wowza.s3_upload_prefix}/#{video_stream.id}/server/#{video_stream.uuid}.mp4"
        if test("sudo -u root -- sh -c '[ -f #{original_video_file} ]'")
          video_file_found = true
          begin
            #combine multiple files if necessary
            ls_result = capture(:ls, '-m', "#{Settings.wowza.content_dir}/#{video_stream.uuid}*.mp4")
            files_list = ls_result.split(",\n").compact.select do |file|
              #select only the main file + the other additional files. exclude _source.mp4, _1080p.mp4, etc
              file =~ %r{#{video_stream.uuid}_\d+\.mp4$} || file.end_with?("#{video_stream.uuid}.mp4")
            end.sort do |x,y|
              x_matches = /_(\d+)\.mp4$/.match(x)
              y_matches = /_(\d+)\.mp4$/.match(y)
              if x_matches.nil?
                -1
              elsif y_matches.nil?
                1
              else
                x_matches[1].to_i <=> y_matches[1].to_i
              end
            end

            if files_list.length > 1
              combined_file_list_local = File.join(Rails.root, 'tmp', "combined_#{video_stream.uuid}.txt")
              combined_file_list_remote = "#{Settings.wowza.content_dir}/combined_#{video_stream.uuid}.txt"
              combined_file_name = "#{Settings.wowza.content_dir}/combined_#{video_stream.uuid}.mp4"
              File.write(combined_file_list_local, files_list.map{|f| "file '#{f}'" }.join("\n"))
              upload! combined_file_list_local, combined_file_list_remote
              execute(:ffmpeg, '-y','-f', 'concat', '-i', combined_file_list_remote, '-c', 'copy', combined_file_name)
              file_to_upload = combined_file_name
            else
              file_to_upload = original_video_file
            end
            #end combining files

            s3_result_raw = capture(:aws, :s3api, 'put-object', '--bucket', Settings.wowza.s3_bucket,
                    '--key', key, '--body', file_to_upload)
            s3_result_json = ActiveSupport::JSON.decode(s3_result_raw)

            #this will alllow us to rereun the job without unique constraint error
            recording = Recording.find_or_initialize_by(video_stream_id: video_stream.id, sequence_number: 0, source: 'server')

            recording.bucket = Settings.wowza.s3_bucket
            recording.key = key
            recording.location = "https://#{Settings.wowza.s3_bucket}.s3.amazonaws.com/#{key}"
            recording.checksum = s3_result_json['ETag']

            recording.save!


            #safe to remove the file from the disk now that it has been uploaded to s3
            #TODO: commenting this out for now. renable this once we're comfortable of not losing data.
            # files_list.each do |f|
            #   execute(:rm, '-f', f)
            # end

            if files_list.length > 1
              execute(:rm, '-f', combined_file_list_remote)
              execute(:rm, '-f', combined_file_name) #safe to remove the combined file.
            end

          rescue => e
            #log is overwritten by SSHKit
            _log.error "Problem uploading past stream from wowza server to s3: video_stream_id=#{video_stream.id} error=#{e.message} backtrace=#{e.backtrace}"

            # Explicitly raise exception to retry 3 times
            raise StandardError
          end
        end
      end
    end

    unless video_file_found
      log.error "Problem uploading past stream from wowza server to s3: video_stream_id=#{video_stream.id} error=FileNotFound file_name=#{original_video_file}"
    end

    File.delete(ssh_key_file_name)
  end

  #Retry job 3 more times
  def handle_error exception
    opts = arguments[0]

    opts[:retry] = opts[:retry] ? opts[:retry].to_i + 1 : 1

    if opts[:retry] < 4
      opts[:wait] = (opts[:retry] * opts[:retry] * 10).seconds #exponential backoff
      self.class.set(wait: wait_time).perform_later(opts) # Deliberately creating a new job id for tracking instead of using retry_job
    end
    super
  end
end

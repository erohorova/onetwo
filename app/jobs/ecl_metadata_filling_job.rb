class EclMetadataFillingJob
  include Sidekiq::Worker

  # NOTE:
  #   Executes the job maximum 10 times.
  #   In the last iteration, it does not execute the #callback proc.
  sidekiq_options :queue => :ecl_metadata_filling_jobs, :retry => 10, :backtrace => true

  sidekiq_retry_in do |count|
    delay = (count**4 + 15 + (rand(30) * (count + 1))).seconds
    Rails.logger.debug "Retrying EclMetadataFillingJob in #{delay.to_s} seconds"
  end

  def perform(id)
    card = Card.find_by(id: id)
    if card.present?
      EclApi::CardConnector.new.sync_ecl_card(card.id)
    end
  end
end

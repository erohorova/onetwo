class AssignmentPerformanceMetricJob < EdcastActiveJob
  queue_as :default

  # content_id would be card_id/assignable_id
  def perform(content_ids)
    # results = {[team_id, assignor_id, created_at, 'assigned', content_id] => 2}
    results = TeamAssignment.joins(:assignment)
      .where(assignments: { assignable_id: content_ids, assignable_type: 'Card' })
      .where.not(team_id: nil)
      .group([:team_id, :assignor_id, :created_at, "assignments.state", "assignments.assignable_id"])
      .count("assignments.id")

    # restructure hash with {[team_id, assignor_id, created_at, content_id] = {state_count: value}}
    team_assignor_data = results.each_with_object({}) do |((team_id, assignor_id, created_at, state, content_id), value), memo|
      memo[[team_id, assignor_id, created_at, content_id]] ||= { assigned_count: 0, completed_count: 0, started_count: 0 }
      if ['assigned', 'completed', 'started'].include?(state)
        memo[[team_id, assignor_id, created_at, content_id]]["#{state}_count"&.to_sym] = value
      end
    end

    # Ex. [team_id, assignor_id, assigned_at, content_id]=>{:assigned_count => 4, :completed_count => 2, :started_count => 0}
    team_assignor_data.each do |(team_id, assignor_id, created_at, content_id), val|
      team_assignment_metric = TeamAssignmentMetric
        .where(team_id: team_id, assignor_id: assignor_id, assigned_at: created_at, card_id: content_id)
        .first_or_initialize
      team_assignment_metric.assign_attributes(val)

      # when assigned_at is not present.
      if team_assignment_metric.assigned_at.nil?
        team_assignment_metric.assigned_at = TeamAssignment.joins(:assignment)
          .where(assignments: {assignable_id: team_assignment_metric.card_id},
            team_assignments: {assignor_id: team_assignment_metric.assignor_id, team_id: team_assignment_metric.team_id})
            .limit(1).first.created_at
      end

      unless team_assignment_metric.save
        log.error "Error while saving team assignment metric"
        log.error "#{team_assignment_metric.errors.full_messages}"
      end
    end
  end
end

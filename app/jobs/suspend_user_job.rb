class SuspendUserJob < EdcastActiveJob
  queue_as :users

  def perform(user_id, org_id)
    user = User.find_by(id: user_id, organization_id: org_id)
    return unless user.present? && !user.is_suspended?
    user.suspend!
    #Adding below update to suspend! method
    #user.update_attributes(status: User::SUSPENDED_STATUS)
  end
end

class FlipmodeActivityRecorderJob < EdcastActiveJob
  queue_as :default

  def perform(email, client_resource_id)

    payload = {
      'edcast_mobile_user_email' => email,
      'EDCAST_TOKEN' => Settings.savannah_token
    }

    conn = Faraday.new(url: Settings.savannah_base_location)

    result = conn.post do |req|
      req.url "/courses/#{client_resource_id}/register_mobile_activity"
      req.headers['Content-Type'] = 'application/json'
      req.headers['Accept'] = 'application/json'
      req.body = payload.to_json
    end

    unless result.success?
      log.error "Sending flip mode activity to savannah failed for \
      email: #{email}, group: #{client_resource_id}. Status: #{result.status} \
      Response: #{result.body}"
    end
  end
end

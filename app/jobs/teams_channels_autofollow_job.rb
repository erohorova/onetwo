class TeamsChannelsAutofollowJob < EdcastActiveJob
  queue_as :team

  def perform(team_ids: [], channel_id: nil, followers_ids: [], sender_id: nil)
    channel = Channel.find_by(id: channel_id)
    return false unless channel.present?
    added = []
    added += add_followers(followers_ids.flatten, channel) if(followers_ids.present?)
    if team_ids.present?
      team_ids.each do |team_id|
        user_ids = TeamsUser.where(team_id: team_id).pluck(:user_id)
        user_ids.each do |user_id|
          user = User.visible_members.find_by(id: user_id)
          after_followed(channel, user)
        end
      end
    end
    notify_to_admins(channel, sender_id, followers_ids, team_ids) if sender_id.present?
  end

  def add_followers(user_ids, channel)
    users = User.visible_members.where(id: user_ids, organization_id: channel.organization_id )
    channel.add_followers(users, true)
  end

  def notify_to_admins(channel, sender_id, followers_ids, team_ids)
    sender_user = User.find_by(id: sender_id)
    return unless sender_user&.is_admin_user?

    followers_emails = User.where("id in (?)", followers_ids).pluck(:email)
    teams_names = Team.where("id in (?)", team_ids).pluck(:name)
    if sender_user.organization.notifications_and_triggers_enabled?
      EDCAST_NOTIFY.trigger_event(Notify::EVENT_ADD_CHANNEL_FOLLOWER_ADMIN, sender_user.organization,
        {sender_id: sender_user.id, channels: [channel.label], followers_emails: followers_emails.first(ChannelsUser::BULK_USER_START + 5), teams_names: teams_names}
      )
    else
      UserMailer.channel_follows_notification_to_admin(sender_user, [channel.label], followers_emails, teams_names).deliver_later
    end
  end

  def after_followed(channel, follower, restore: nil)
    PubnubNotifyChangeJob.new.perform('subscribe', follower, channel.public_pubnub_channel_name)
    IndexJob.new.perform(follower.class.name, follower.id)
    OnFollowEnqueueJob.new.perform(
        id: channel.id,
        type: "Channel",
        follower_id: follower.id
    )
  end
end

class UserImportJob < EdcastActiveJob
  queue_as :user_import

  def perform(user_import_id:, admin_id:, user_import_metadata:)
    UserImport.new(user_import_id: user_import_id, admin_id: admin_id, user_import_metadata: user_import_metadata).import
  end
end
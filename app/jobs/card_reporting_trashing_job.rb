class CardReportingTrashingJob < EdcastActiveJob
  queue_as :cards

  def perform(card_id:)
    card = Card.with_deleted.find_by(id: card_id)
    FlagContent::CardReportingSearchService.trash_record(card: card)
  end
end

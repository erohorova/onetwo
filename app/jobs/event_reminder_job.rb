class EventReminderJob < EdcastActiveJob
  queue_as :forum

  def perform(opts)
    event_id = opts[:event_id]
    event_start = opts[:event_start]

    event = Event.find_by(id: event_id)
    if(event_start && event_start != event.event_start.to_i)
      return
    end
    return if event.nil?
    event.attending_users.find_in_batches(batch_size: 100) do |users|
      EventReminderMailer.remind_users(event: event, users: users).deliver_now
    end
  end
end

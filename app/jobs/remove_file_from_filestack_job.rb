# WARNING:
# Please ensure the changes from https://github.com/edcast/edcast/pull/8631
#   since we deprecated the file resources.
# Also, this job is not invoked for an unknown issue. Check L#60 in file_resource.rb.

class RemoveFileFromFilestackJob < EdcastActiveJob
  queue_as :default

  def perform(opts)
    # remove file from filestack
    if CARD_STOCK_IMAGE.any? {|h| h[:handle] == opts[:handle] }
      log.warn "Cannot delete default image from filestack with handle: #{opts[:handle]} for card_id: #{opts[:card_id]}"
    else
      begin
        conn = Faraday.new(url: Settings.filestack.api)
        conn.basic_auth 'app', Settings.filestack.app_secret
        result = conn.delete do |req|
          req.url "/api/file/#{opts[:handle]}"
          req.params['key']           = Settings.filestack.api_key
          req.headers['Content-Type'] = 'application/json'
          req.headers['Accept']       = 'application/json'
        end
        
        if result.success?
          log.warn "Successfully deleted file from filestack with handle: #{opts[:handle]} for card_id: #{opts[:card_id]}"
        else
          log.warn "Failed to delete file from filestack with handle: #{opts[:handle]} for card_id: #{opts[:card_id]}, Status: #{result.status}, Response: #{result.body}"
        end
      rescue StandardError => e
        log.warn "Cannot delete file from filestack with handle: #{opts[:handle]} for card_id: #{opts[:card_id]}"
      end
    end
  end
end

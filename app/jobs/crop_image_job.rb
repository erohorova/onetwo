class CropImageJob < EdcastActiveJob
  queue_as :image_upload

  def perform(crop_image_data:)
    #does not really do much but trigger the attachment paperclip reprocess code
    crop_image_data.croppable.send(crop_image_data.crop_column).reprocess!
  end
end

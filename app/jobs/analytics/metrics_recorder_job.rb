class Analytics::MetricsRecorderJob < EdcastActiveJob
  queue_as :lxp_event_recorder

  # These are general-purpose recorders.
  # Each of them refers to a class in lib/analytics
  # Reference those classes to see the unique arguments.

  # If recording fails due to an InfluxDB::Error, we retry every 30 seconds,
  # up to 10 minutes.

  def perform(recorder:, retries_left: 20, **args)
    recorder_class = if Object.const_defined?(recorder)
      Object.const_get(recorder)
    end
    raise(ArgumentError, "unknown recorder") unless recorder_class
    begin
      recorder_class.record(**args)
    rescue InfluxDB::Error => e
      if retries_left > 0
        self.class.enqueue_clone(
          wait_time:    30.seconds,
          recorder:     recorder,
          retries_left: (retries_left - 1),
          **args
        )
      else
        log.error(
          "failed to write data to influx due to InfluxDB::Error. " + 
          "recorder = #{recorder}, args = #{args}"
        )
        raise # re-raises the InfluxDB::Error with message
      end
    end
  end

  def self.enqueue_clone(wait_time:, **args)
    new(**args).enqueue(wait: wait_time)
  end

end

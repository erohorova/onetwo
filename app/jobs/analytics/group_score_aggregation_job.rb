# frozen_string_literal: true

# =========================================
# Aggregates user_scores_daily records by group/day.
# Populates the group_scores_daily measurement
# and group_user_scores_daily measurement.
# Unlike some of the other Influx aggregation queries, this cannot
# be done in Influx SQL-like API, because it lacks the IN clause.
# In the future we may adopt IFQL and rewrite it there.
# =========================================

class Analytics::GroupScoreAggregationJob < EdcastActiveJob
  queue_as :lxp_event_aggregation

  def perform
    Analytics::GroupScoreAggregationRecorder.run!
  end

end
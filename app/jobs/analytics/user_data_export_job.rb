# frozen_string_literal: true

class Analytics::UserDataExportJob < EdcastActiveJob
  queue_as :lxp_event_aggregation

  def perform(user:)
    Analytics::UserDataExport.run user
    is_done = Analytics::UserDataExport.data_export_exists?(user)
    status =  is_done ? "done" : "error"
    user.update(
      data_export_status: status,
      last_data_export_time: (DateTime.now if is_done)
    )
  rescue => e
    Rails.logger.debug e
    user.update data_export_status: "error"
    raise e
  end

end

# frozen_string_literal: true

class Analytics::UserDataDeletionJob < EdcastActiveJob
  queue_as :lxp_event_aggregation

  def perform(user:)
    Analytics::UserDataDeletion.run! user
  end

end
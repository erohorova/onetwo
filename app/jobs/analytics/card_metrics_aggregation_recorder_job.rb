# frozen_string_literal: true

# =========================================
# Produces aggregated data per card and day.
# Populates the content_level_metrics_daily measurement
# Stores the results to MySQL so we can use WHERE..IN queries
# =========================================

class Analytics::CardMetricsAggregationRecorderJob < EdcastActiveJob
  queue_as :lxp_event_aggregation

  def perform
    Analytics::CardMetricsAggregationRecorder.new.run
  end

end

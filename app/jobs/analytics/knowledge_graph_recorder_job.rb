# frozen_string_literal: true

class Analytics::KnowledgeGraphRecorderJob < EdcastActiveJob
  queue_as :lxp_event_aggregation

  def perform
    Analytics::KnowledgeGraphRecorder.run
  end

end
# frozen_string_literal: true

class Analytics::GroupMetricsAggregationRecorderJob < EdcastActiveJob
  queue_as :lxp_event_aggregation

  def perform
    Analytics::GroupMetricsAggregationRecorder.run
  end

end

# =========================================
# For testing purposes, or backfilling
=begin
require 'byebug'
Analytics::GroupMetricsAggregationRecorder.const_set "TIME_RANGE", 5.days
Analytics::GroupMetricsAggregationRecorder.run(
  orgs: Organization.where(id: 15)
)
=end
# =========================================
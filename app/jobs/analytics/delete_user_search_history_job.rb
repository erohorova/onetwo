# frozen_string_literal: true

class Analytics::DeleteUserSearchHistoryJob < EdcastActiveJob
  queue_as :lxp_event_aggregation

  def perform(user_id:, org_id:)
    Analytics::DeleteUserSearchHistory.run!(
      user_id: user_id,
      org_id: org_id
    )
  end

end
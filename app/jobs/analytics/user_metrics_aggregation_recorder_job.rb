# frozen_string_literal: true

# =========================================
# Produces aggregated data per user and day.
# Copies the user_scores_daily measurement from Influx
# to the UserMetricsAggregation SQL table.
# Stores the results to MySQL so we can use WHERE..IN queries
# =========================================

class Analytics::UserMetricsAggregationRecorderJob < EdcastActiveJob
  queue_as :lxp_event_aggregation

  def perform
    Analytics::UserMetricsAggregationRecorder.new.run
  end

end

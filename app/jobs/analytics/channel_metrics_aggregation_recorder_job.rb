# frozen_string_literal: true

class Analytics::ChannelMetricsAggregationRecorderJob < EdcastActiveJob
  queue_as :lxp_event_aggregation

  def perform
    Analytics::ChannelMetricsAggregationRecorder.run
  end

end
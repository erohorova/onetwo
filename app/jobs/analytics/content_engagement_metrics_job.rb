# frozen_string_literal: true

class Analytics::ContentEngagementMetricsJob < EdcastActiveJob
  queue_as :lxp_event_aggregation

  def perform
    Analytics::ContentEngagementInfluxQuery.run
  end

end
class EclCardPropagationJob < EdcastActiveJob
  queue_as :cards

  # NOTE:
  #   Executes the job maximum 10 times.
  #   In the last iteration, it does not execute the #callback proc.
  include ActiveJob::Retry.new(
    strategy: :exponential, limit: 10,
    callback: proc do |exception, delay|
      # will be run before each retry
      Rails.logger.debug "Retrying EclCardPropagationJob in #{delay.to_s} seconds with job_id: #{self.job_id}"
    end
  )

  def perform(card_id)
    begin
      EclApi::CardConnector.new.propagate_card_to_ecl(card_id)
    rescue Exception => e
      log.error("Propagating card to ecl failed for card_id: #{card_id}, error: #{e.message}")
    end
  end
end

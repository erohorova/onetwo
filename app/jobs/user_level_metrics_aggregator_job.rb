class UserLevelMetricsAggregatorJob < EdcastActiveJob
  queue_as :analytics

  def perform(attributes)

    increments = {}
    [:actor_id, :owner_id].each {|user_type| increments[attributes[user_type]] = [] unless attributes[user_type].nil?}

    # Update smartbite score
    if MetricRecord.should_increment_smartbite_score?(attributes)
      SmartbiteScoreMapper.smartbite_score_increments(attributes).each do |user_id, increment|
        increments[user_id] << [:smartbites_score, increment]
      end
    end

    unless attributes[:actor_id].nil?
      # smartbites created
      if MetricRecord.is_creation_event?(attributes)
        increments[attributes[:actor_id]] << [:smartbites_created, 1]
      # smartbites consumed
      elsif MetricRecord.is_consumption_event?(attributes)
        increments[attributes[:actor_id]] << [:smartbites_consumed, 1]
      # Comments
      elsif MetricRecord.is_comment_event?(attributes)
        increments[attributes[:actor_id]] << [:smartbites_comments_count, 1]
      # Likes
      elsif MetricRecord.is_like_event?(attributes)
        increments[attributes[:actor_id]] << [:smartbites_liked, 1]
      # smartbite completed
      elsif MetricRecord.is_mark_completed_event?(attributes)
        increments[attributes[:actor_id]] << [:smartbites_completed, 1]
      elsif MetricRecord.is_mark_uncompleted_event?(attributes)
        increments[attributes[:actor_id]] << [:smartbites_completed, -1]
      end
    end

    increments.each do |user_id, increments|
      UserLevelMetric.update_from_increments(opts: {user_id: user_id}, increments: increments, timestamp: Time.at(attributes[:created_at_to_i]))
    end
  end
end

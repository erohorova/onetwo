class OnFollowEnqueueJob < EdcastActiveJob
  queue_as :feed

  def perform(opts = {})
    Octopus.using(:replica1) do
      klass = opts[:type].constantize
      followee = klass.find_by(id: opts[:id])

      if [followee, opts[:follower_id]].any? &:nil?
        # possible due to followee or follower object is deleted or not exists
        log.warn "bad opts #{opts}"
        return
      end

      # skip default org
      return if followee.organization_id == Organization.default_org&.id
      follower = User.find_by(id: opts[:follower_id])

      # remove followee from recommendation queue
      UserContentsQueue.dequeue_for_user(
          user_id: opts[:follower_id],
          content_id: opts[:id],
          content_type: opts[:type],
      )

      enqueue_cards followee, follower
      enqueue_streams followee, follower
    end
  end

  private
  def enqueue_cards followee, follower
    metadata = {}
    case followee.class.name
    when 'User'
      #TODO Card::EXPOSED_TYPES should switch to v2
      response = Search::CardSearch.new.search(
          followee,
          followee.organization,
          viewer: follower,
          filter_params: {card_type: Card::EXPOSED_TYPES, state: 'published'},
          limit: 10,
          sort: :updated,
          load: true #load AR object
      )
      metadata = {rationale: {type: 'following_user', user_id: followee.id}}
      return if response.nil? || response.results.empty?
      cards = response.results

      cards = Card.where(id: cards.map{|d| d['id']})
    when 'Channel'
      # Cards which are not created by the follower should only be enqueued for him.
      cards = Card.curated_cards_for_channel_id(followee.id)
        .includes(:channels)
        .where('author_id != ? OR author_id IS NULL', follower.id)
        .select("cards.*, channels_cards.created_at")
        .order("channels_cards.created_at DESC").limit(10)

      return if cards.empty?
      metadata = {rationale: {type: 'following_channel', channel_id: followee.id}}
    end

    # To not call the visibility filter in case the logged in user is an org admin
    unless follower.is_org_admin? || cards.empty?
      cards = cards.visible_to_userV2(follower)
    end

    items = []
    card_channel_ids = ChannelsCard
                           .select(:card_id, :channel_id)
                           .where(card_id: cards.ids)
                           .group_by(&:card_id)
                           .each {|k,v| v.map!(&:channel_id)}

    cards.each do |card|
      enqueued_at = if followee.class.name == 'User'
        card['published_at'] || card.published_at
      elsif followee.class.name == 'Channel'
        DateTime.now.in_time_zone('UTC')
      end
      items << {
        content_id: card.id,
        channel_ids: card_channel_ids[card.id],
        queue_timestamp: enqueued_at,
        metadata: metadata
      }
    end

    UserContentsQueue.enqueue_bulk_for_user(
      user_id: follower.id,
      content_type: 'Card',
      items: items,
      flush: true,
      merge_metadata: false
    )
  end

  def enqueue_streams followee, follower
    streams = []
    rationale_type = nil
    case followee.class.name
    when 'User'
      streams = followee.my_video_streams_visible_to(viewer: follower).first(10)
      rationale_type = "following_user"
    when 'Channel'
      streams = Channel.video_stream_feed([followee])
      rationale_type = "following_channel"
    end
    items = []
    streams_channel_ids = ChannelsCard
                              .select(:card_id, :channel_id)
                              .where(card_id: streams.map(&:card_id))
                              .group_by(&:card_id)
                              .each{|k,v| v.map!(&:channel_id)}
    streams.each do |stream|
      items << {
        content_id: stream.id,
        channel_ids: streams_channel_ids[stream.card_id],
        queue_timestamp: stream.created_at,
        metadata: {'rationale' => {'type' => rationale_type, 'user_id' => stream.creator_id}}
      }
    end

    UserContentsQueue.enqueue_bulk_for_user(
      user_id: follower.id,
      content_type: 'VideoStream',
      items: items,
      merge_metadata: false,
      flush: true
    )
  end
end

class TeamPushNotificationJob < EdcastActiveJob
  queue_as :notifications

  def perform(content: nil, team_id: nil)
    team = Team.find_by(id: team_id)
    
    if team && content && team.users.any?
      team.users.each do |user|
        opts = {
          host_name: team.organization.host,
          user_id: user.id,
          notification_type: 'AdminGenerated',
          content: content
        }
        PubnubNotifier.notify(opts, false)
      end
    end
  end
end

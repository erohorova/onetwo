class AvatarImageJob < ImageUploadJob
  queue_as :image_upload

  def perform(opts)
    user_id = opts[:user_id]
    picture_url = opts[:picture_url]

    begin
      user = User.find(user_id)
      user.avatar = picture_url
      user.skip_generate_images = true #need to do this other wise the after save callback will get trigger again
      user.save!
      user.reindex_async
    rescue => e
      log.error("Unable to create avatar image: user_id=#{user.id} errors=#{user.errors.to_json} picture_url=#{user.picture_url} exception=#{e}")
    end

  end
end

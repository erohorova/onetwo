class CoursesScraperJob < EdcastActiveJob
  queue_as :default

  def perform(user_id, external_uid, provider)
    scraper =  case provider
      when "Coursera"
        Integrations::CourseraService.new(external_uid: external_uid, user_id: user_id)
      when "Udemy"
        Integrations::UdemyService.new(external_uid: external_uid, user_id: user_id)
      when "Future Learn"
        Integrations::FutureLearnService.new(external_uid: external_uid, user_id: user_id)
      when "Skill Share"
        Integrations::SkillShareService.new(external_uid: external_uid, user_id: user_id)
      when "Plural Sight"
        Integrations::PluralSightService.new(external_uid: external_uid, user_id: user_id)
      when "Tree House"
        Integrations::TreeHouseService.new(external_uid: external_uid, user_id: user_id)
      when "Code School"
        Integrations::CodeSchoolService.new(external_uid: external_uid, user_id: user_id)
      when "Alison"
        Integrations::AlisonService.new(external_uid: external_uid, user_id: user_id)
      when "Codecademy"
        Integrations::CodecademyService.new(external_uid: external_uid, user_id: user_id)
      when "Shire"
        Integrations::ShireService.new(user_id: user_id)
      end
      scraper.scrape if scraper
  end
end

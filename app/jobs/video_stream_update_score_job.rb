class VideoStreamUpdateScoreJob < EdcastActiveJob
  queue_as :video_stream

  def perform(video_stream)
    video_stream.update_score
  end
end

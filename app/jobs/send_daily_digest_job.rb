class SendDailyDigestJob < EdcastActiveJob
  queue_as :forum

  def perform
    EDCAST_NOTIFY.send_digests(Date.today, :digest_daily)
  end
end

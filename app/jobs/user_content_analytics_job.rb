class UserContentAnalyticsJob < EdcastActiveJob
  queue_as :analytics

  def perform(organization_id)
    organization = Organization.find(organization_id)
    organization.send_user_content_analytics
  end
end

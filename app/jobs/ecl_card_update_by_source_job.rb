class EclCardUpdateBySource < CardsJob
  # NOTE:
  #   Executes the job maximum 10 times.
  #   In the last iteration, it does not execute the #callback proc.
  include ActiveJob::Retry.new(
    strategy: :exponential, limit: 10,
    callback: proc do |exception, delay|
      # will be run before each retry
      Rails.logger.debug "Retrying EclCardUpdateBySource in #{delay.to_s} seconds with job_id: #{self.job_id}"
    end
  )

  def perform(params)
    cards = Card.where(organization_id: params[:organization_id]).where("ecl_metadata LIKE ?", "%#{params[:source_id]}%")
    cards.find_each do |card|
      ecl_card_connector = EclApi::CardConnector.new(ecl_id: card.ecl_id)
      ecl_card_connector.update_cards_on_source_update(card, params)
    end
  end

end

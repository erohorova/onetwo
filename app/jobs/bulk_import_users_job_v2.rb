class BulkImportUsersJobV2 < EdcastActiveJob
  queue_as :batch

  def perform(file_id:, options:, admin_id:)
    BulkImportUsersV2.new(
      file_id: file_id,
      options: options,
      admin_id: admin_id
    ).import
  end
end
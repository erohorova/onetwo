class OrganizationSendWeeklyActivityJob < EdcastActiveJob
  queue_as :mailers

  def perform(organization_id)
    organization = Organization.find(organization_id)
    return if organization.nil?

    organization.send_weekly_activity_email
  end
end

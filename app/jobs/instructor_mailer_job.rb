class InstructorMailerJob < EdcastActiveJob
  queue_as :forum

  MAX_POST = 5
  MAX_GROUP = 5
  def perform
    log.info "Started instructor daily digest email"
    # set default Time
    now       = Time.now.utc
    batch_job = BatchJob.find_by(name: self.class.name)

    # pick existing OR create new one
    if batch_job
      last_run_time = batch_job.last_run_time
      batch_job.update_attributes!(last_run_time: now)
    else
      BatchJob.create(last_run_time: now, name: self.class.name)
      last_run_time = now - 1.day
    end
    eligible_org_ids = Config.where(name: 'app.config.instructor_daily_digest_email_v1', configable_type: 'Organization', value: 'true').pluck(:configable_id)
    eligible_groups = ResourceGroup.not_closed.where(organization_id: eligible_org_ids)

    eligible_posts = Post.includes(:user, :resource).where(:created_at => last_run_time...now, postable_type: 'Group', postable_id: eligible_groups.map(&:id))
    groups_to_eligible_posts_hash = eligible_posts.group_by(&:postable_id)

    eligible_groups_by_id = eligible_groups.index_by(&:id)

    eligible_users = User.includes(:groups_users => :group).joins(:groups_users => :group)  
      .where("groups.close_access != 1 and groups_users.as_type = 'admin' and groups.type='ResourceGroup' and groups.organization_id in (?) ",eligible_org_ids)
      .uniq
      .find_in_batches(batch_size: 100) do |batch|
        batch.each do |user|

          starttime = Time.now
          user_groups = GroupsUser.where(user_id: user.id, as_type: 'admin', group_id: eligible_groups_by_id.keys)
          groups_to_eligible_posts_hash_for_this_user = groups_to_eligible_posts_hash.slice(*user_groups.map(&:group_id))
         
          aggregated_posts = []
          groups_sorted_by_post = groups_to_eligible_posts_hash_for_this_user.sort_by{|k,v| v.length}.reverse.to_h
          groups_sorted_by_post.each { |group_id, posts| 
             aggregated_posts << {group_id: group_id, posts: posts}
          }

          # To add groups without any posts in the data hash as well.
          user_groups.map(&:group_id).each{ |group|
           aggregated_posts <<{group_id: group, posts: []} unless groups_sorted_by_post.include? group
          }
          begin
            aggregated_data = aggregated_posts.first(MAX_GROUP).map { |gp|
              post_set = gp[:posts].first(MAX_POST).map { |each_post|
                post_user = each_post.user
                post_hash = each_post.as_json
                post_hash.merge!("creator_name" => post_user.name, "deep_url" => Post.url(each_post, eligible_groups_by_id[gp[:group_id]]))
                post_hash
              }
              group_hash = {'group_name' => eligible_groups_by_id[gp[:group_id]].name, 'group_url' => eligible_groups_by_id[gp[:group_id]].website_url, 'posts' => post_set, 'posts_empty' => post_set.empty?, 'show_more' => (gp[:posts].count > MAX_POST)}
            }
            UserMailer.send_daily_instructor_mails(user, aggregated_data).deliver_now
            log.info "Sent mail to instructor with id : #{user.id}"
          rescue StandardError => e
            log.error "Error: #{e.message}"
          end
          endtime = Time.now
          log.info "Done sending daily digest for user id: #{user.id}. Time taken: #{endtime-starttime} second"
        end
      end  
  end
end


      

class UserTaxonomyLevelMetricsAggregatorJob < EdcastActiveJob
  queue_as :analytics

  def perform(attributes)

    attributes = HashWithIndifferentAccess.new(attributes) # Used in migration 
    topics = MetricRecord.get_taxonomy_topics_for_object(attributes[:object_id])
  

    return if topics.blank?

    increments = {}
    [:actor_id, :owner_id].each {|user_type| increments[attributes[user_type]] = [] unless attributes[user_type].nil?}
    if MetricRecord.should_increment_smartbite_score?(attributes)
      SmartbiteScoreMapper.smartbite_score_increments(attributes).each do |user_id, increment|
        increments[user_id] << [:smartbites_score, increment]
      end
    end

    unless attributes[:actor_id].nil?
      # smartbites created
      if MetricRecord.is_creation_event?(attributes)
        increments[attributes[:actor_id]] << [:smartbites_created, 1]
      # smartbites consumed
      elsif MetricRecord.is_consumption_event?(attributes)
        increments[attributes[:actor_id]] << [:smartbites_consumed, 1]
      end
    end

    increments.each do |user_id, _increments|
      topics.each do |topic|
        UserTaxonomyTopicLevelMetric.update_from_increments(opts: {user_id: user_id, taxonomy_label: topic}, increments: _increments, timestamp: Time.at(attributes[:created_at_to_i]))
      end
    end
  end
end

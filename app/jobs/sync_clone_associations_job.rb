class SyncCloneAssociationsJob < EdcastActiveJob
  extend SetUniqueJob

  queue_as :channel

  def self.unique_args(args)
    args = args[0].with_indifferent_access
    [args[:parent_card_id], args[:sync_association_type]]
  end

  # opts
  # parent_card_id: card id of parent card, 
  # sync_association_type: parent card association against which needs sync
  def perform(opts={})
    if (parent_card = Card.find_by_id(opts[:parent_card_id])).present?
      sync_assoc_service = SyncCloneAssociationsService.new(opts)
      parent_card.cloned_cards.find_each do |cloned_card|
        sync_assoc_service.sync_card_association(cloned_card)
      end
    end
  end
end
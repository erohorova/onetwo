class AutoAssignPathwaysForLearningJob < EdcastActiveJob
  def perform(user_id)
    service = AutoRecommendCardService.new(user_id, card_types: ['pack'])
    service.auto_assign_recommended_cards_to_user
  end
end

require 'open-uri'
class UploadToScormJob < EdcastActiveJob
  queue_as :scorm

  def perform(card_id, course_id)
    ScormUploadService.new.run(card_id, course_id)
  end
end

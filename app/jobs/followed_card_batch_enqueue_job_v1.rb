# This job is performs bulk insertion of feeds in `UserContentsQueue` for 1000 contents.
class FollowedCardBatchEnqueueJobV1 < CardsJob
  queue_as :feed

  def perform(options = {})
    card_id, follow_ids, channel_ids = options.values_at(:card_id, :follow_ids, :channel_ids)
    card = Card.find_by(id: card_id)
    unless card
      log.warn "no card for #{card_id}"
      return
    end

    users = {}

    # prepare user data for insertion
    Follow.includes(:user).where(id: follow_ids).each do |follow|
      enqueue_for_follower = follow.enqueue_for_follower(card: card)
      users[follow.user&.id.to_s] = enqueue_for_follower if enqueue_for_follower
    end

    # insert user data in bulk
    UserContentsQueue.enqueue_content_for_users(
      selected_users: users,
      content_id: card.id,
      content_type: 'Card',
      channel_ids: channel_ids,
      overwrite_timestamp: false
    )
  end
end

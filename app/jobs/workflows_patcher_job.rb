class WorkflowsPatcherJob < EdcastActiveJob
  queue_as :bulk

  def perform
    ids_to_process = []
    WorkflowPatchRevision.outgoing_patches
      .find_in_batches(batch_size: 5000) do |patches|
        next if patches.empty?
        ids_to_process.concat(patches.map(&:id))
      end

    log.warn(<<-TXT)
      TOTAL_PATCHES: #{ids_to_process.count}
      PATCHES_IDS: #{ids_to_process}
    TXT

    counter = (ids_to_process.count.to_f / WorkflowPatchRevision::BATCH_LIMIT).ceil
    counter.times do
      return if ids_to_process.empty?
      data_sender(ids_to_process.pop(WorkflowPatchRevision::BATCH_LIMIT))
    end

    # enqueue processing of failed patches if they present
    WorkflowPatchRevision.failed_patches.find_in_batches do |batch|
      WorkflowsV2Job.perform_later(
        patch_ids: batch.map(&:id)
      )
    end
  end

  def data_sender(patch_ids)
    return if patch_ids.empty?
    patches = WorkflowPatchRevision.where(id: patch_ids)
    data_to_sent = patches.pluck(:data).to_json

    patches.update_all(
      in_progress: true,
      job_id: self.job_id
    )
    conn = Faraday.new(:url => Settings.workflows.host_url) do |faraday|
      faraday.adapter :net_http_persistent do |http|
        http.pool_size = ENV['HTTP_PERSISTENT_POOL_SIZE'] || 15
      end
      faraday.options[:open_timeout] = 2
      faraday.options[:timeout] = 15
      faraday.headers['x-api-token'] = Settings.workflows.edcast_master_token
      faraday.headers['org-name'] = 'www' # default org is used
      faraday.headers['Content-Type'] = 'application/json'
    end

    res = conn.post do |req|
      req.url('api/edcast/handle-patches')
      req.body = data_to_sent
    end

    if res.status.to_i.between?(200, 299)
      WorkflowPatchRevision.where(id: patch_ids)
        .update_all(completed: true, in_progress: false)
    else
      log.warn(<<-TXT)
        WorkflowsPatcherJob error.
        Response: #{res.body}.
        Status: #{res.status}.
        Headers: #{conn.headers}
        Patch ids: #{patch_ids}
      TXT

      WorkflowPatchRevision.where(id: patch_ids)
        .update_all(
          in_progress: false,
          failed: true
        )
    end
  end

  def handle_error(exception)
    fail_info = {
      exception: {
        inspect: exception.inspect,
        backtrace: exception&.backtrace&.first(10) || []
      }
    }
    patches = WorkflowPatchRevision.where(job_id: self.job_id)
    patches.update_all(
      in_progress: false,
      failed: true,
    )

    log.warn(<<-TXT)
      WorkflowsPatcherJob exception: #{fail_info};
      Job_id: #{self.job_id}
    TXT

    super
  end
end

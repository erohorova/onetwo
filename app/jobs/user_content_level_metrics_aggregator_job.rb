class UserContentLevelMetricsAggregatorJob < EdcastActiveJob
  queue_as :analytics

  def perform(attributes)

    if MetricRecord.should_increment_smartbite_score?(attributes)
      SmartbiteScoreMapper.smartbite_score_increments(attributes).each do |user_id, increment|
        UserContentLevelMetric.increment_smartbites_score({
          user_id: user_id, 
          content_id: attributes[:object_id], 
          content_type: attributes[:object_type]
        },
        increment,
        Time.at(attributes[:created_at_to_i]))
      end
    end
  end
end

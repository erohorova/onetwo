class CreateCustomEmailTemplateJob < ActiveJob::Base
  queue_as :mailers
  
  def perform(organization_id)
    organization = Organization.find_by(id: organization_id)
    if organization
      working_dir = Rails.root.join('public', 'templates', 'default_templates_design')

      notify_templates_titles = EmailTemplate::DEFAULT_TITLES[:notify]

      mailer_templates_titles = EmailTemplate::DEFAULT_TITLES[:mailer]
      templates_titles = notify_templates_titles + mailer_templates_titles

      templates_titles.each do |title|
        next if organization.email_templates.pluck(:title).include?(title)
        
        file_content = read_file(working_dir + "#{title}.json")
        next unless file_content
        
        EmailTemplate.create(
          title: title, design: file_content, language: 'en',
          state: 'published', is_active: true, organization_id: organization.id
        )
      end
    end
  end

  def read_file(file_path)
    file = File.open(file_path)
    file_data = file.read
    file.close
    # check validness of JSON
    JSON.parse(file_data)
    return file_data
  rescue Errno::ENOENT, JSON::ParserError => e
    puts "File read error: #{e.message}"
    nil
  end
end
class CardDeletionByEclSourceJob < EdcastActiveJob
  require 'open-uri'
  require 'csv'

  queue_as :cards

  def perform(source_id, org_id, file_url)
    csv_data = open(file_url).read
    ecl_ids = CSV.parse(csv_data).flatten

    ecl_ids.in_groups_of(100, false) do |ids|
      cards = Card.where(ecl_id: ids, organization_id: org_id)
      cards.each do |card|
        card.update_request_from_ecl = true
      end
      cards.destroy_all
    end

    EclApi::CardConnector.new(organization_id: org_id).notify_card_deletion_job_completion_to_ecl(source_id)
  end

end

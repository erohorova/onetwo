class ActivityStreamCreateJob < ActiveJob::Base
  queue_as :forum

  def perform(streamable_id:,streamable_type:,user_id:,action:,card_id: nil, attempts: 5)
    user = User.find_by(id: user_id)
    return unless user
    streamable = streamable_type.safe_constantize&.find_by(id: streamable_id)
    return unless streamable

    activity_stream = ActivityStream.new(
      streamable: streamable,
      user: user,
      action: action,
      organization: user.organization,
      card_id: card_id
    )
    begin
      activity_stream.save
    rescue ActiveRecord::StatementInvalid => exception
      is_deadlock = exception.message =~ /Deadlock found when trying to get lock/
      if is_deadlock && attempts > 0
        self.class.new(
          self.arguments.first.merge({attempts: (attempts - 1)}
          )
        ).enqueue(wait_time: 3.seconds)
      else
        raise exception
      end
    end
  end
end

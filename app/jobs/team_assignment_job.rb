class TeamAssignmentJob < EdcastActiveJob
  queue_as :team
  include EventTracker

  def perform(team_id:, assignor_id:, assignee_ids:, assignable_id:,
    assignable_type:, opts: { self_assign: false }
  )
    team = Team.find_by(id: team_id)
    assignor = team.organization.users.where(id: assignor_id).first
    assignable = assignable_type.classify.constantize.find assignable_id

    if !assignable || (assignable.organization_id != team.organization_id)
      log.error "Failed to load assignable for #{assignable_type} with
        Id: #{assignable_id}"
      return
    end

    # if assignee_ids are blank then assign it to all team members
    # else pick not suspended users
    assignees = team.users.not_suspended

    unless assignee_ids.blank?
      assignees = assignees.where(id: assignee_ids)
    end

    assignees.find_in_batches(batch_size: 200).each do |users|
      TeamBatchAssignmentJob.perform_later(
        team_id: team.id,
        assignor_id: assignor_id,
        assignee_ids: users.map(&:id),
        assignable_id: assignable_id,
        assignable_type: assignable_type,
        opts: opts
      )
    end

    # create event for card assigned to group/team
    if assignable_type == 'Card'
      record_group_card_assigned_event(
        team: team,
        card_id: assignable_id,
        assignor: assignor
      )
    end

  end

  def record_group_card_assigned_event(team:, card_id:, assignor:)
    card = Card.with_deleted.find_by_id(card_id)

    unless card
      log.error "Event was not recorded due to card not found"
      return
    end

    record_custom_event(
      event: 'group_card_assigned',
      job_args: [ {
        org: team.organization,
        group: team,
        card: card,
        actor: assignor
      } ]
    )

  end

end
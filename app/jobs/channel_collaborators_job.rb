class ChannelCollaboratorsJob < EdcastActiveJob
  queue_as :channel

  def perform(channel_id:, role_ids:, from_id:, as_trusted: false)
    channel = Channel.where(id: channel_id).first
    from_user = User.where(id: from_id).first

    channel.organization.roles.where(id: role_ids).map(&:user_ids).each_slice(1000) do |batch_of_user_ids|
      users = User.where(id: batch_of_user_ids)
      channel.add_collaborators(from: from_user, users: users, as_trusted: as_trusted)
    end
  end
end

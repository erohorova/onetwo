class CloneChannelCardsJob < EdcastActiveJob
  queue_as :channel

  def perform(parent_channel_id, clone_channel_id)
    if ( clone_channel = Channel.find_by_id(clone_channel_id) ).present?
      clone_record_service = CloneRecordsService.new(clone_channel.organization.id)
      Card.joins(:channels_cards)
        .where(is_public: true)
        .where.not(card_type: 'project')
        .where(channels_cards: {channel_id: parent_channel_id, state: 'curated'})
        .find_each do |card|
          # Run clone record service to clone card
          clone_card = clone_record_service.run(card)
          # Add clone card in clone channel
          if clone_card.present? && clone_card.is_a?(Card)
            clone_card.channels << clone_channel unless clone_channel.channels_cards.where(card_id: clone_card.id).exists?
          else
            log.warn "Clonning card #{card.id} of channel id #{parent_channel_id} to channel id #{clone_channel_id} failed"
          end
        end
    else
      log.warn "Clonning cards of channel id #{parent_channel_id} to channel id #{clone_channel_id} failed"
    end
  end
end
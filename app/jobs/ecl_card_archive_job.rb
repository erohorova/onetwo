class EclCardArchiveJob < EdcastActiveJob
  # NOTE:
  #   Executes the job maximum 10 times.
  #   In the last iteration, it does not execute the #callback proc.
  include ActiveJob::Retry.new(
    strategy: :exponential, limit: 10,
    callback: proc do |exception, delay|
      # will be run before each retry
      Rails.logger.debug "Retrying EclCardArchiveJob in #{delay.to_s} seconds with job_id: #{self.job_id}"
    end
  )

  def perform(ecl_id, organization_id)
    EclApi::CardConnector.new.archive_card_from_ecl(ecl_id, organization_id)
  end
end

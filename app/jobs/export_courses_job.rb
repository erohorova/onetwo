class ExportCoursesJob < EdcastActiveJob
  queue_as :default

  def perform(group_ids: [], user_id:)
    user = User.find_by(id: user_id)

    return if user.nil?

    groups = ResourceGroup.where(id: group_ids)
    ExportResourceGroups.new(groups: groups, user: user).run
  end
end

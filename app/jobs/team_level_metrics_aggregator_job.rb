class TeamLevelMetricsAggregatorJob < EdcastActiveJob
  queue_as :analytics

  def perform(attributes)

    return if attributes[:actor_id].nil?

    team_ids = User.find_by(id: attributes[:actor_id]).teams.excluding_everyone_team.pluck(:id)

    unless team_ids.blank?
      increments = []

      # smartbites created
      if MetricRecord.is_creation_event?(attributes)
        increments << [:smartbites_created, 1]
      # smartbites consumed
      elsif MetricRecord.is_consumption_event?(attributes)
        increments << [:smartbites_consumed, 1]
      # Comments
      elsif MetricRecord.is_comment_event?(attributes)
        increments << [:smartbites_comments_count, 1]
      # Likes
      elsif MetricRecord.is_like_event?(attributes)
        increments << [:smartbites_liked, 1]
      end

      team_ids.each do |team_id|
        TeamLevelMetric.update_from_increments(opts: {team_id: team_id}, increments: increments, timestamp: Time.at(attributes[:created_at_to_i]))
      end
    end
  end
end

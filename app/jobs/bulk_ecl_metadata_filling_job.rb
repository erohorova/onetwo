class BulkEclMetadataFillingJob
  include Sidekiq::Worker
  sidekiq_options :queue => :bulk_ecl_metadata_filling_jobs, :retry => 1, :backtrace => true
  
  def perform()
    excluded_default_org_ids = Organization.exclude_default_org.pluck(:id)
    Card.where("ecl_id IS NOT NULL and organization_id IN (?)", excluded_default_org_ids).find_each do |card|
      EclMetadataFillingJob.perform_async(card.id)
    end
  end
end

class AnnouncementJob < EdcastActiveJob
  queue_as :forum

  def perform(opts = {})
    post = Post.find_by(id: opts[:post_id])
    if post.nil?
      log.warn "Announcement id: #{post.id} does not exist"
      return
    end
    batch_size = opts[:batch_size] || 100
    post.group.users.find_in_batches(batch_size: batch_size) do |users|
      begin
        AnnouncementMailer.announcement_email(post: post, users: users).deliver_now
      rescue => e
        log.warn("Batch email failed. Trying to send each email one at a time: exception=#{e} post_id=#{post.id} users=#{users.map{|u| u.as_json(only: [], methods: [:name, :mailable_email])}}")
        users.each do |user|
          begin
            AnnouncementMailer.announcement_email(post: post, users: [user]).deliver_now
          rescue => e
            log.error("Failed announcement email. exception=#{e} name=#{user.name} email=#{user.mailable_email} post_id=#{post.id}")
          end
        end
      end
    end
  end
end

class InstructorDailyDigestBatchReportJob < EdcastActiveJob
  queue_as :forum

  def perform
    InstructorDigestReportService.new.send_report_to_instructors(get_duration)
  end

  def get_duration
    batch_job = BatchJob.where(name: self.class.name)
    now = Time.now.utc
    if batch_job.any?
      record = batch_job.first
      last_run_time = record.last_run_time
      record.update_attributes!(last_run_time: now)
    else
      batch_job.create(last_run_time: now, name: self.class.name)
      last_run_time = now - 1.day
    end

    { from: last_run_time.to_i, to: now.to_i }
  end
end

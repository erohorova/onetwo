class AnonImageJob < ImageUploadJob
  queue_as :image_upload

  def perform(opts)
    user_id = opts[:user_id]
    user = User.find(user_id)
    if user.email
      color = "##{Digest::SHA1.hexdigest(user.email)[0..5]}"
    else
      color = "##{SecureRandom.hex[0..5]}"
    end

    img = ::Magick::ImageList.new Rails.root.join("app/assets/images/anonymous-user.png")
    newimg = img.color_floodfill(10, 10, color)
    newimg = newimg.modulate(saturate=0.5, hue=0.5)
    temp_file = Tempfile.new(["user-#{user.id}-anon", '.png'])
    newimg.write temp_file.path
    temp_file.close
    user.anonimage = File.new(temp_file.path)
    user.skip_generate_images = true #need to do this other wise the after save callback will get trigger again

    begin
      user.save!
      user.reindex_async
    rescue => e
      log.error("Unable to create anon image: user_id=#{user.id} errors=#{user.errors.to_json} exception=#{e}")
    end
  end
end

class AssessmentReportGenerationJob < EdcastActiveJob
  def perform(card_id:, user_id:)
    user = User.find_by(id: user_id)
    return if user.blank?

    log.info("Generating report for assessment data for: #{card_id}")
    aws_url = AnalyticsReport::AssessmentReport.new(card_id: card_id, user_id: user_id).generate_report

    user.send_assessment_report_email_for(card_id: card_id, aws_url: aws_url)
  end
end

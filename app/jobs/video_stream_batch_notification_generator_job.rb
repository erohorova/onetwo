# This is called from NotificationGeneratorJob. Tests are in NotificationGeneratorJobTest
class VideoStreamBatchNotificationGeneratorJob < EdcastActiveJob
  queue_as :notifications

  def perform(user_ids: [], video_stream: , notification_type: )
    inserts = []
    now = Time.now.utc.strftime("%Y-%m-%d %H:%M:%S")
    user_ids.each do |user_id|
      begin
        ActiveRecord::Base.transaction do
          n = Notification.where(notifiable: video_stream.card, user_id: user_id, notification_type: notification_type)
          unless n.present?
            n = Notification.new(notifiable: video_stream.card, user_id: user_id, notification_type: notification_type)
            n.save!
            NotificationsCausalUser.create!(notification: n, user_id: video_stream.creator_id, anonymous: false)
          end
        end
      rescue ActiveRecord::RecordInvalid
        log.error "video stream notification on stream id: #{video_stream.id} save failed for user id: #{user_id}"
      end
    end
  end
end

class TransferOwnershipJob < EdcastActiveJob
  queue_as :cards

  def perform(card_id, opts={})
    card = Card.find_by(id: card_id)

    if card
      if card.pack_card?
        update_pack_cards(card, opts)
        #update reviewer_id for project where submissions not presnt or aren't approved
        update_projects(card)
      elsif card.journey_card?
        pathways = card.journey_pathways.where(hidden: true, author_id: opts[:old_author_id])
        if pathways
          pathways.each do |pathway|
            pathway.update_column(:author_id, card.author_id)
            LogHistoryJob.perform_later("entity_type" => 'card', "entity_id" => pathway.id,
              "changes" => {"author_id" => [opts[:old_author_id], card.author_id]}, "user_id" => opts[:actor_id]
            )
            update_pack_cards(pathway, opts)
          end
        end
      end

      EDCAST_NOTIFY.trigger_event(Notify::EVENT_CHANGE_AUTHOR, card) if card.author_id != opts[:actor_id]
      EDCAST_NOTIFY.trigger_event(Notify::EVENT_NOTIFY_OLD_AUTHOR, card, {old_author_id: opts[:old_author_id]}) if opts[:old_author_id] != opts[:actor_id]
    else
      log.info("Card not found: #{card_id}")
    end
  end

  private

  def update_pack_cards(pathway, opts)
    pack_cards = pathway.pack_cards.where(hidden: true, author_id: opts[:old_author_id], is_paid: false)
    pack_cards.each do |pc|
      pc.update_column(:author_id, pathway.author_id)
      LogHistoryJob.perform_later("entity_type" => 'card', "entity_id" => pc.id,
        "changes" => {"author_id" => [opts[:old_author_id], pathway.author_id]}, "user_id" => opts[:actor_id]
      )
    end
  end

  def update_projects(card)
    project_cards = card.pack_cards.where(hidden: true, card_type: "project")
    if project_cards
      project_cards.each do |project_card|
        project = project_card.project
        update_project = project.project_submissions.empty? || project.project_submissions.awaiting_reviews.present?
        project.update(reviewer_id: card.author_id) if update_project
      end
    end
  end
end

class IndexJob < EdcastActiveJob
  extend SetUniqueJob

  # Copied from https://github.com/ankane/searchkick/blob/master/lib/searchkick/reindex_v2_job.rb
  queue_as :indexer

  def self.unique_args(args)
    [args[0], args[1]]
  end

  def perform(klass, id)
    Octopus.using(:replica1) do
      model = klass.constantize
      record = model.find(id) rescue nil # TODO fix lazy coding
      index = model.searchkick_index
      if !record || !record.should_index?
        # hacky
        record ||= model.new
        record.id = id
        begin
          index.remove record
        rescue Elasticsearch::Transport::Transport::Errors::NotFound
          # do nothing
        end
      else
        index.store record
      end
    end
  end
end

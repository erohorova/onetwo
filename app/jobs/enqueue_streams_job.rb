class EnqueueStreamsJob < EdcastActiveJob
  queue_as :video_stream

  def perform(opts = {})
    @stream = VideoStream.find_by(id: opts[:id])
    @timestamp =  opts[:use_created_at_as_timestamp] ? @stream.created_at.to_i : Time.now.utc.to_i
    @exclude_users = [@stream.creator_id]

    return if @stream.nil?

    # skip default org
    return if @stream.organization_id == Organization.default_org&.id

    if @stream.creator_id && @stream.publicly_visible?
      enqueue_stream_for_bulk_followers
    end
    enqueue_stream_for_bulk_channel_followers
  end

  private

  def enqueue_stream_for_bulk_followers
    @exclude_users = @exclude_users.nil? ? [] : @exclude_users
    rationale = {'type' => "following_user", "user_id" => @stream.creator_id}
    @stream.creator.followed_by_users.where(is_complete: true).select(:id).find_in_batches(batch_size: 200) do |batch|
      user_ids = batch.map(&:id)
      @exclude_users += user_ids
      enqueue_stream_for_bulk_users(user_ids, rationale)
    end
  end

  def enqueue_stream_for_bulk_channel_followers
    @exclude_users = @exclude_users.nil? ? [] : @exclude_users

    @stream.relevant_channels.each do |ch|
      # exclude those already covered under following_user rationale
      rationale = {'type' => "following_channel", "channel_id" => ch.id}
      ch.followed_by_users.where.not(id: @exclude_users).select(:id).find_in_batches(batch_size: 200) do |batch|
        user_ids = batch.map(&:id)
        @exclude_users += user_ids
        enqueue_stream_for_bulk_users(batch.map(&:id), rationale)
      end
    end
  end

  def enqueue_stream_for_bulk_users(user_ids, rationale, buffer_flush: false)
    EnqueueStreamsBatchJob.perform_later({stream_id: @stream.id, user_ids: user_ids, rationale: rationale, timestamp: @timestamp, buffer_flush: buffer_flush})
  end
end

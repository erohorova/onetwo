require 'csv'
class DataDumpJob < EdcastActiveJob
  queue_as :forum

  def perform(format, requester_id, group_id)
    group = ::Group.find(group_id)
    dump = Hash.new
    dump[:course_name] = group.name
    dump[:posts] = Array.new

    group_list = [group.id]
    group_list << group.private_group_ids if group.private_groups.exists?
    group_list.flatten!

    Post.where(postable_type: 'Group', postable_id: group_list).each do |p|
      post_json = json_for_post(p, group)
      post_json['comments'] = Array.new
      Comment.where(commentable: p).each do |c|
        comment_json = json_for_comment(c, group)
        post_json['comments'] << comment_json
      end
      dump[:posts] << post_json
    end
    # puts "dump: #{dump.inspect}"
    s3 = AWS::S3.new
    bucket = s3.buckets["edcast-forum-data-dump-#{Rails.env}"]
    obj = bucket.objects["#{group.id}-dump-#{DateTime.now.to_s}.#{format}"]
    if format == 'JSON'
      obj.write(JSON.pretty_generate(dump))
    elsif format == 'XML'
      obj.write(dump.to_xml)
    elsif format == 'CSV'
      column_names = ['id', 'type', 'title', 'message', 'client_user_id', 'group_id', 'group_name', 'is_private_group', 'created_at', 'updated_at', 'votes_count', 'comments_count', 'views_count', 'reports_count', 'has_approval', 'anonymous', 'pinned', 'context_id', 'context_label', 'is_mobile', 'commentable_id']
      column_names_hash = Hash[column_names.map.with_index {|x, i| [x, i]}]

      s = CSV.generate do |csv|
        csv << column_names
        dump[:posts].each do |post|
          post_csv = Array.new
          column_names_hash.keys.each do |k|
            post_csv[column_names_hash[k]] = post[k]
          end
          csv << post_csv
          post['comments'].each do |comment|
            comment_csv = Array.new
            column_names_hash.keys.each do |k|
              comment_csv[column_names_hash[k]] = comment[k]
            end
            csv << comment_csv
          end
        end
      end
      obj.write(s)
    end
    UserMailer.data_dump(group.id, requester_id, obj.url_for(:read).to_s).deliver_later
  end

  private

  def json_for_post(p, group)
    post_json = p.as_json(only: [:id, :type, :title, :message, :group_id, :created_at, :updated_at, :votes_count, :comments_count, :anonymous, :pinned, :context_id, :context_label, :is_mobile])
    post_json['group_name'] = p.group.name
    post_json['is_private_group'] = p.group.parent_id.present?
    post_json['client_user_id'] = PartnerAuthentication.get_cu_by_user_id(p.user.forum_user.id, group.organization).client_user_id
    post_json['has_approval'] = p.has_approval?
    post_json['views_count'] = p.reads_count
    post_json['reports_count'] = p.reports.count
    post_json
  end

  def json_for_comment(c, group)
    comment_json = c.as_json(only: [:id, :message, :created_at, :updated_at, :votes_count, :anonymous, :is_mobile, :commentable_id])
    comment_json['client_user_id'] = PartnerAuthentication.get_cu_by_user_id(c.user.forum_user.id, group.organization).client_user_id
    comment_json['has_approval'] = c.has_approval?
    comment_json['views_count'] = nil
    comment_json['reports_count'] = c.reports.count
    comment_json['group_id'] = c.group.id
    comment_json['type'] = 'Comment'
    comment_json
  end
end

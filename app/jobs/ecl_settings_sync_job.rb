class EclSettingsSyncJob < EdcastActiveJob

  def perform(organization_id)
    ecl_settings_params = {default_source_enabled: false, sociative_integration: true}
    EclApi::EclSettings.new(organization_id).create_ecl_settings(ecl_settings_params)
  end
end
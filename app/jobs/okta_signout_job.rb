class OktaSignoutJob < ActiveJob::Base
  queue_as :okta

  def perform(user_id:, organization_id:)
    Octopus.using(:replica1) do
      user = User.find(user_id)
      if user.federated_identifier.present?
        current_org = Organization.find(organization_id)
        klass = Okta::InternalFederation.new(organization: current_org)
        response = klass.federated_user(username: user.federated_identifier)
        klass.sign_out_user(response["id"]) if response.present?
      end
    end
  end
end

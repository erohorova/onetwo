class UsersCardsManagementJob < EdcastActiveJob
  queue_as :users

  def perform(user_id, card_id, action, target_id, opts: 'create')
    return false unless [user_id, card_id, action, target_id].all?
    case opts
    when 'destroy'
      umc = UsersCardsManagement.find_by(user_id: user_id, card_id: card_id, action: action, target_id: target_id)
      umc.delete if umc.present?
    when 'create'
      UsersCardsManagement.create(user_id: user_id, card_id: card_id, action: action, target_id: target_id)
    end
  end
end
class GetScormUploadStatusJob < EdcastActiveJob
  queue_as :scorm

  def perform(card_id)
    log.info "Getting upload status for card: #{card_id}"
    ScormUploadStatusService.new.run(card_id)
  end
end
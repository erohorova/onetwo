class WorkflowPatchRevisionsJob < EdcastActiveJob
  queue_as :bulk

  def perform(patches:, version: 0)
    return if patches.blank?
    patches.each do |patch|
      params = {
        organization_id: Organization.find_by(host_name: patch['orgName'])&.id,
        # workflow unique identifier(wuid) of the patch
        wuid: get_wuid(patch),
        data_id: patch['data']['id'],
        workflow_id: patch.dig('models', 'workflowId'),
        output_source_id: patch.dig('models', 'outputSourceId'),
        workflow_type: patch['workflowType'],
        patch_id: patch['patchId'],
        patch_type: patch['patchType'],
        org_name: patch['orgName'],
        data: patch['data'],
        params: patch['params'],
        meta: patch['meta'],
        completed: false,
        in_progress: false,
        processing_info: {info: [], errors: []},
        version: version,
        job_id: nil,
        failed: false
      }
      schema_validation = WorkflowConcern::validate_schema(patch, 'handle_patches')
      org = params[:organization_id]

      errors = []
      unless org
        err = { organization: "Can't identify provided host name: #{patch['orgName']}" }
        errors.push(err)
      end

      unless schema_validation.empty?
        err = { schema_validation: schema_validation }
        errors.push(err)
      end

      ### provide more complex schema validator to remove these checks
      if version == 0 && patch['workflowType'] == 'groups' && patch['data']['group_name'].nil?
        err = { group_name: "Can't be blank" }
        errors.push(err)
      end

      if patch['workflowType'] == 'users_groups' && patch['data']['group_id'].nil?
        err = { group_id: "Can't be blank" }
        errors.push(err)
      end
      ###end

      unless errors.empty?
        params[:processing_info][:errors].push(*errors)
        params[:completed] = false
        params[:failed] = true
      end

      WorkflowPatchRevision.create(params)
    end
  end

  def get_wuid(patch)
    [
      patch.dig('data', 'id').to_s,
      patch.dig('models', 'workflowId').to_s,
      patch.dig('models', 'outputSourceId').to_s
    ].reduce(:+)
  end
end

class AutoAssignOldContentToNewTeamUserJob < EdcastActiveJob
  queue_as :users

  def perform(user_id:, team_id:)
    if user_id.present? and team_id.present?
      team = Team.find_by_id(team_id)
      user = User.find_by_id(user_id)

      if team.present? and team.auto_assign_content == true and user.present?
        assignment_card_ids = team.assignments.where('(due_at IS NULL or due_at >= ?)', Date.today).distinct.pluck(:assignable_id)

        assignment_card_ids.each do |assignment_card_id|
          assignor_id = TeamAssignment.joins(:assignment)
            .find_by("team_assignments.team_id = ? and assignments.assignable_id = ?", team.id, assignment_card_id)
            .assignor_id

          # EP-18036 not sending notification of assignment of previous contents, as it spams the user.
          team.create_team_assignments_for(
            assignee_ids: [user.id],
            assignor_id: assignor_id,
            assignable_type: 'Card',
            assignable_id: assignment_card_id,
            opts: { skip_notification: true }
          )
        end
      end
    end
  end
end

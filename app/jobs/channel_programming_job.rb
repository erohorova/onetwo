class ChannelProgrammingJob < EdcastActiveJob
  extend SetUniqueJob
  queue_as :content_source

  def self.unique_args(args)
    [args[0]]
  end

  def perform(channel_id)
    #Actual channel programming v2 logic
    channel = Channel.find(channel_id)
    sources_ids = EclSource.where(channel_id: channel.id).pluck(:ecl_source_id)
    #only if sources are added call channel programming api
    if sources_ids.length > 0
      opts = { node_paths: channel.topics.map{|topic| topic["name"]},
               topic_ids: channel.topics.map {|topic| topic["id"]},
               source_ids: sources_ids,
               types: channel.content_type,
               organization_id: channel.organization_id,
               size: channel.card_fetch_limit || 500,
               scope: {channel_ids: [channel.id]},
               languages: channel.language
             }
      Adapter::Content.new({organization_id: channel.organization_id}).channel_programming(channel.id, opts)
    end
  end
end
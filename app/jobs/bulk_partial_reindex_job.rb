class BulkPartialReindexJob < ActiveJob::Base
  queue_as :bulk

  def perform(ids:, klass_name:, method_name:)
    klass   = klass_name.safe_constantize
    records = klass.where(id: ids)
    klass.searchkick_index.bulk_update(records, method_name)
  end
end

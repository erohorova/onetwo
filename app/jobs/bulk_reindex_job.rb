class BulkReindexJob < ActiveJob::Base
  queue_as :reindex_async

  def perform(ids:, klass_name:)
    klass   = klass_name.safe_constantize
    records = klass.where(id: ids)
    klass.searchkick_index.import(records)
  end
end

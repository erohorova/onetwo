class SyncCloneChannelCardsJob < EdcastActiveJob
  queue_as :channel

  # opts
  # parent_card_id: card id of parent card, 
  # parent_channel_id: id of parent channel,  
  # action: add/delete clone card action
  def perform(opts= {})
    if (parent_channel = Channel.find_by_id(opts[:parent_channel_id])).present?
      parent_channel.cloned_channels.find_each do |clone_channel|
        if (parent_card = Card.unscoped.find_by_id(opts[:parent_card_id])).present?
          clone_record_service = CloneRecordsService.new(clone_channel.organization_id)
          case opts[:action]
          when 'add'
            clone_card = clone_record_service.run(parent_card)
            if clone_card.present? && clone_card.is_a?(Card)
              clone_card.channels << clone_channel unless clone_channel.channels_cards.where(card_id: clone_card.id).exists?
            else
              log.warn "Clonning card #{parent_card.id} of channel id #{opts[:parent_channel_id]} to channel id #{clone_channel.id} failed"
            end
          when 'destroy'
            parent_card.remove_cloned_cards
          end
        else
          log.warn "Parent card #{opts[:parent_card_id]} does not exists for cloning/destroying from clone channel"
        end
      end
    end
  end
end
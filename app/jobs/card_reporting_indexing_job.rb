class CardReportingIndexingJob < EdcastActiveJob
  queue_as :cards

  def perform(card_id: )
    card = Card.with_deleted.find_by(id:card_id)
    card_reportings =  card&.card_reportings&.with_deleted&.order('created_at DESC')&.map(&:json_data)
    if card_reportings.present?
      FlagContent::CardReportingSearchService.new(
        id: card.id,
        reportings: card_reportings,
        card_message: card.title.presence || card.message,
        card_type: card.card_type,
        card_created_at: card.created_at,
        card_author_id: card.author_id,
        card_organization_id: card.organization_id,
        card_deleted_at: card.deleted_at,
        card_source: card.source_type_name).save
    end
  end
end

class BulkPartialReindexUserRolesJob < ActiveJob::Base
  queue_as :bulk

  def perform(org_id:, id: )
    User.not_suspended.where(organization_id: org_id).joins(:user_roles).where("user_roles.role_id= ?", id).select(:id).find_in_batches do |batch|
      ids = batch.map {|user| user.id}
      BulkPartialReindexJob.perform_later(ids: ids, klass_name: 'User', method_name: :roles_data)
    end
  end
end

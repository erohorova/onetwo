# notify card author's followers, shared users when card created or shared
class CreateShareCardNotificationJob < EdcastActiveJob
  queue_as :notifications

  def perform(opts = {})
    card = Card.find_by(id: opts[:card_id])
    unless card
      log.warn "no card for #{opts}"
      return
    end

    custom_hash = {
      event: opts[:event],
      team_ids: opts[:team_ids],
      user_ids: opts[:user_ids], 
      permitted_user_ids: opts[:permitted_user_ids],
      permitted_team_ids: opts[:permitted_team_ids]
    }

    custom_hash.merge!({share_by_id: opts[:share_by_id]}) if card.author_id != opts[:share_by_id]

    if (opts[:event] == Card::EVENT_NEW_CARD_SHARE)
      if (opts[:user_ids].present? || opts[:team_ids].present?)
        EDCAST_NOTIFY.trigger_event(Notify::EVENT_NEW_CARD, card, custom_hash)
      else
        card.send_notification
      end
    end
    if (opts[:user_ids].present? || opts[:team_ids].present?)
        EDCAST_NOTIFY.trigger_event(Notify::EVENT_CONTENT_SHARE, card, custom_hash)
    end
  end
end
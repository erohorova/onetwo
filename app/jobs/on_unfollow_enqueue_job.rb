class OnUnfollowEnqueueJob < EdcastActiveJob
  queue_as :feed
  DEQUEUE_BATCH_SIZE = 5000

  def perform(opts = {})
    Octopus.using(:replica1) do
      klass = opts[:type].constantize
      followee = klass.find_by(id: opts[:id])
      if [followee, opts[:follower_id]].any? &:nil?
        log.warn "bad opts #{opts}"
        return
      end

      remove_from_queue(user_id: opts[:follower_id], content_id: opts[:id], content_type: opts[:type])
      dequeue_cards followee, opts[:follower_id]
    end
  end

  private

  def remove_from_queue(user_id:, content_id:, content_type:)
    UserContentsQueue.dequeue_for_user(user_id: user_id, content_id: content_id, content_type: content_type)
  end

  def dequeue_cards(followee, follower_id)
    followee.cards.select(:id).find_in_batches(batch_size: DEQUEUE_BATCH_SIZE) do |cards|
      card_ids = cards.map(&:id)
      dequeue_card_batch(card_ids: card_ids, follower_id: follower_id)
    end
  end

  def dequeue_card_batch(card_ids:, follower_id:)
    contents = UserContentsQueue.get_contents_for_user(user_id: follower_id, content_ids: card_ids, content_type: 'Card')
    UserContentsQueue.bulk_delete(contents: contents)
  end
end

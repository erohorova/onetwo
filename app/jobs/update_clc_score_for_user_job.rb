class UpdateClcScoreForUserJob < EdcastActiveJob
  queue_as :clc

  def perform(card_id, user_id, entity_id, entity_type)
    clc_service = ClcService.new.update_clc_score_of_user(card_id, user_id, entity_id, entity_type)
  end
end

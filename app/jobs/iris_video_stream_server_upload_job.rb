class IrisVideoStreamServerUploadJob < EdcastActiveJob
  queue_as :video_stream

  def perform(opts = {})
    video_stream_id = opts[:video_stream_id]
    video_stream = VideoStream.find(video_stream_id)

    meta_data = video_stream.get_meta_data
    if meta_data
      recording_url = video_stream.get_meta_data['payload']['url']
      uri = URI(recording_url)

      file_extension = File.extname(uri.path)
      tempfile = Tempfile.new([video_stream_id.to_s, file_extension])

      begin
        #this code will avoid downloading the entire file into memory. rather download and flush it to disk in chunk
        Net::HTTP.start(uri.host, uri.port, use_ssl: uri.scheme == 'https') do |http|
          request = Net::HTTP::Get.new uri

          http.request request do |response|
            open tempfile.path, 'wb' do |io|
              response.read_body do |chunk|
                io.write chunk
              end
            end
          end
        end

        s3  = AWS::S3.new
        s3_key = "#{Settings.iris.s3_upload_prefix}/#{video_stream.id}/server/#{video_stream.uuid}#{file_extension}"
        iris_s3_bucket = Settings.iris.s3_bucket
        s3_object = s3.buckets[iris_s3_bucket].objects[s3_key]

        # refer http://docs.aws.amazon.com/AWSRubySDK/latest/AWS/S3/S3Object.html#write-instance_method
        # upload the file in parts if the total file size exceeds 100MB
        s3_response = s3_object.write(file: tempfile, multipart_threshold: 100.megabytes)
      rescue Exception => e
        raise StandardError, "Failed to upload IRIS video_stream_id=#{video_stream_id} to S3, Error: #{e.message}. Retrying..."
      end

      recording = Recording.find_or_initialize_by(video_stream_id: video_stream.id, sequence_number: video_stream.get_recording_sequence_number, source: 'server')
      recording.bucket = iris_s3_bucket
      recording.key = s3_key
      recording.location = "https://#{iris_s3_bucket}.s3.amazonaws.com/#{s3_key}"
      recording.checksum = s3_response.etag
      recording.save!
      tempfile.close
    else
      log.error("Cannot fetch meta data from iris video_stream_id=#{video_stream_id}")
      raise StandardError, "Failed to download IRIS video_stream_id=#{video_stream_id}. Retrying..."
    end
  end

  #Retry job 3 more times
  def handle_error exception
    opts[:retry] = opts[:retry] ? opts[:retry].to_i + 1 : 1

    if opts[:retry] < 4
      wait_time = (opts[:retry] * opts[:retry] * 10).seconds #exponential backoff
      self.class.set(wait: wait_time).perform_later(opts) # Deliberately creating a new job id for tracking instead of using retry_job
    end
    super
  end

end

class WorkflowPatchSaverJob < EdcastActiveJob
  queue_as :default

  def perform(data:, org_id:, bulk_import: false)
    org = Organization.find_by(id: org_id)
    unless org
      log.warn(<<-TXT)
        Organization does not exist by host_name: #{org_id}
      TXT
      return
    end

    if bulk_import
      data.each do |user_email|
        usr = org.users.find_by(email: user_email)
        unless usr
          log.warn(<<-TXT)
            WorkflowPatchSaverJob. User not found. Email: #{user_email}
          TXT
          next
        end
        usr.workflow_import = true
        create_patch_entry(org, usr.send(:detect_upsert, import: true))
      end
    else
      create_patch_entry(org, data)
    end
  end

  def create_patch_entry(org, data)
    WorkflowPatchRevision.create(
      organization_id: org.id,
      patch_type: data[:patchType],
      patch_id: data[:patchTimestamp],
      org_name: org.host_name,
      data: data,
      data_id: data[:data].is_a?(Array) ? nil : data.dig(:data, :user_email),
      in_progress: false,
      completed: false,
      version: 2
    )
  end
end

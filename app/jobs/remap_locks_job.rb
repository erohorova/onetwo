class RemapLocksJob < ActiveJob::Base
  queue_as :default

  def perform(card_id)
    # After deletion of locked card, move lock to next not deleted position in CardPackRelation, if next card exist
    CardPackRelation.where('from_id = ? AND locked = 1 AND to_id IS NOT NULL', card_id).each(&:remap_lock)
  end
end
class SetUserTimeZoneJob < EdcastActiveJob
  def perform(user_id:, ip_address:)
    user = User.find(user_id)
    time_zone = IpLookupService.new(ip_address).get_time_zone
    if user && time_zone
      profile = user.profile || user.build_profile
      profile.time_zone = time_zone
      unless profile.save
        log.warn "Failed to save time zone(value: #{profile.time_zone}) for ip #{ip_address} with error: #{profile.errors.full_messages}"
      end
    end
  end
end

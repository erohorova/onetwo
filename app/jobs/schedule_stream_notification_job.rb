class ScheduleStreamNotificationJob < EdcastActiveJob
  def perform(opts = {})
    card = Card.find_by(id: opts[:card_id])
    return unless card.present?
    EDCAST_NOTIFY.trigger_event(Notify::EVENT_TIME_TO_START_SCHEDULEDSTREAM, card)
  end
end
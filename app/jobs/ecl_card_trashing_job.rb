class EclCardTrashingJob < EdcastActiveJob
  def perform(ecl_id, organization_id)
    EclApi::CardConnector.new(ecl_id: ecl_id, organization_id: organization_id).trash_card_from_ecl
  end
end

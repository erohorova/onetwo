class ShareJob < EdcastActiveJob
  queue_as :forum

  def perform(sharable_class: nil, sharable_id: nil, user_id: nil, networks: nil, message: nil)
    links = []
    user = User.find(user_id)
    sharable = sharable_class.constantize.find(sharable_id)
    links = user.identity_providers.map do |provider|
      if networks.include?(provider.provider_name) && provider.scoped?(:share)

        {name: provider.provider_name.capitalize, url: provider.share(message: message, object: sharable) }

      end
    end.compact
    links
  end
end

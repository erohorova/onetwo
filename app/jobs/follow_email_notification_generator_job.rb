class FollowEmailNotificationGeneratorJob < EdcastActiveJob
  queue_as :notifications

  def perform(followable_id, user_ids)
    email_to = User.where(id: followable_id).first
    return unless email_to.pref_notification_follow
    new_followers = User.where(id: user_ids)
    followers_whose_notifs_are_unread = email_to.notifications.where(unseen: true, notification_type: 'new_follow').map(&:causal_users).flatten
    filtered_follower_list = new_followers & followers_whose_notifs_are_unread
    unless filtered_follower_list.blank?
      UserMailer.follower_batch_notification_email(email_to, filtered_follower_list).deliver_now
    end
  end
end

class OrganizationAnalyticsJob < EdcastActiveJob
  queue_as :analytics

  def perform(organization_id)
    Organization.find(organization_id).send_org_analytics
  end
end

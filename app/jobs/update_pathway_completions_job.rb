class UpdatePathwayCompletionsJob < EdcastActiveJob
  queue_as :default

  def perform(cover_id: , org_id:)
    org = Organization.find_by(id: org_id)
    return unless org
    cover = org.cards.find_by(id: cover_id)
    return unless cover
    uc_completions = UserContentCompletion.where(completable: cover)
    return if uc_completions.blank?
    uc_completions.each do |ucc|
      ucc.update_attributes(state: 'started', completed_at: nil, completed_percentage: 0)
      CompletionPercentageService.set_completed_percentage(ucc.user, ucc.completable)
    end
  end
end

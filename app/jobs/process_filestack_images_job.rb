class ProcessFilestackImagesJob < EdcastActiveJob
  queue_as :image_upload

  def perform(card_id)
    card = Card.find_by(id: card_id)
    if card
      filestacks = card.filestack
      filestacks.each do |filestack|
        url = filestack["url"]
        resized_image_options = Filestack::ProcessImage.new(url: url).resize
        Filestack::UpdateCard.new(
          card: card,
          url: url,
          resized_image_options: resized_image_options
        ).save
      end
    end
  end
end

class BulkImportUsersJob < EdcastActiveJob
  queue_as :batch

  def perform(file_id:, options:, admin_id:)
    BulkImportUsers.new(
      file_id: file_id,
      options: options,
      admin_id: admin_id
    ).import
  end
end
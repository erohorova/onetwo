class IndividualAssignmentJob < EdcastActiveJob
  queue_as :team

  include Assignable

  def perform(**args)
    assignor = User.not_suspended.find_by_id args[:assignor_id]

    unless assignor
      log.warn "Assignor not found"
      return
    end
    args[:opts] ||= {}

    org = assignor.organization
    assignable = org.cards.find_by_id(args[:assignable_id])

    unless assignable
      log.warn "Card not found"
      return
    end

    org.users.not_suspended.where(id: args[:assignee_ids]).each do |user|

      create_assignment(
        user: user,
        assignor: assignor,
        assignable: assignable,
        opts: args[:opts]
      )
    end
  end
end

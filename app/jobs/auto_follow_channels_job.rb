class AutoFollowChannelsJob < EdcastActiveJob
  def perform(opts = {})
    user_id = opts.delete(:user_id)
    user = User.find(user_id)

    ## These are the channels the user should be autofollowing
    auto_followed_channels = user.organization.channels.where(auto_follow: true)
    auto_followed_channels.find_each do |c|
      c.add_followers([user])
    end
  end
end

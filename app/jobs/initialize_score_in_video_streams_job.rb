class InitializeScoreInVideoStreamsJob < EdcastActiveJob
  queue_as :analytics

  def perform
    VideoStream.find_each do |video_stream|
      #adds handle for creator and empty score hash to videostream elasticsearch index
      video_stream.update_data_in_elastic_search(video_stream.empty_update_data.merge({creator: video_stream.creator_hash}))
    end
  end
end

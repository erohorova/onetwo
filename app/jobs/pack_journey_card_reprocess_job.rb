class PackJourneyCardReprocessJob < EdcastActiveJob
  queue_as :cards
  def perform(cover_id:)
    CompletionPercentageService.reset_completed_percentage(cover_id)
  end
end

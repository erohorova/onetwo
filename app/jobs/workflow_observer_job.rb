# This job will be called when the worklfow input is changed.
# This job will trigger Observer service of Workflow

class WorkflowObserverJob < EdcastActiveJob
  queue_as :workflow

  def perform(organization:, changes:, model_name:, input_data_ids: nil)
    WorkflowService::Observer.new(
      organization: organization,
      changes: changes,
      model_name: model_name,
      input_data_ids: input_data_ids
    ).perform
  end
end
class TagAutosuggestIndexerJob < EdcastActiveJob
  queue_as :tag_suggest

  def perform(tagging)
    TagSuggest.index_for_autosuggest(tagging)
  end

end

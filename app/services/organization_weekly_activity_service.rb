class OrganizationWeeklyActivityService
  attr_accessor :activity
  def initialize(org)
    @org = org
    @activity = {
      insights: top_insights,
      video_streams: top_video_streams,
      pathways: top_pathways
    }
  end

  def top_insights
    filter_params = { card_type: ['media', 'poll'], range: { created_at: { 'gte': Time.zone.now - 7.days }}, small_image_url: true }

    Search::CardSearch.new.search(
        nil,
        @org,
        filter_params: filter_params,
        viewer: nil,
        offset: 0,
        limit: 3,
        sort: :like_count
    ).results
  end

  def top_video_streams
    filter_params = { organization_id: @org.id, range: { created_at: { 'gte': Time.zone.now - 7.days }}}
    Search::VideoStreamSearch.new.search(q: nil, viewer: nil, offset: 0, limit: 3, filter_params: filter_params, sort_params: { votes_count: 'desc'}).results
  end

  def top_pathways
    filter_params = { card_type: ['pack'], range: { created_at: { 'gte': Time.zone.now - 7.days }}, small_image_url: true}
    Search::CardSearch.new.search(
        nil,
        @org,
        filter_params: filter_params,
        viewer: nil,
        offset: 0,
        limit: 3,
        sort: :like_count
    ).results
  end

end

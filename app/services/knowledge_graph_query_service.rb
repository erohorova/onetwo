# frozen_string_literal: true

class KnowledgeGraphQueryService

  # See .get_user_topics_map for other keyword params
  def self.run(**opts)
    query_results = get_user_topics_map(**opts)
 
    # Get the total count for each user/topic over the period
    user_topic_counts = add_total_count(query_results)

    # Filter the list, to include only the top N topics per user
    filter_topics!(user_topic_counts, opts[:num_top_topics])

    results = add_full_name_and_teams(user_topic_counts, opts[:team_ids])
    
    derive_topics_users_map!(results)
    derive_teams_users_map!(results)

    results
  end

  def self.get_user_topics_map(
    # Mandatory params
    start_time:, end_time:, org_id:,
    num_top_users:, num_top_topics:,
    current_user_id:, is_admin:,
    # Optional params
    user_ids: nil, team_ids: nil
  )
    # Applied as a where clause at the end
    user_filter = build_user_filter(user_ids: user_ids)

    # Applied by inner joining on a subquery.
    # team_filter (which uses the team_ids param) happens inside
    # the user_teams_filter (which applies the default role based filtering).
    team_filter = build_team_filter(team_ids: team_ids)
    user_teams_filter = build_user_teams_filter(
      is_admin: is_admin,
      team_filter: team_filter,
    )
    query = <<-SQL.strip_heredoc
      SELECT _activities.topic_counts,_activities.user_id
      FROM   users_topics_activities AS _activities
      INNER JOIN (
        SELECT   _activities_inner.user_id,SUM(total_activities_count) AS sum
        FROM     users_topics_activities AS _activities_inner
        #{user_teams_filter}
        WHERE    start_time >= :start_time
        AND      end_time <= :end_time
        AND      organization_id = :org_id
        #{user_filter}
        GROUP BY user_id
        ORDER BY sum DESC
        LIMIT    :num_top_users
      ) AS _top_users
      ON _activities.user_id = _top_users.user_id
      WHERE _activities.start_time >= :start_time
      AND   _activities.end_time <= :end_time
      AND   _activities.organization_id = :org_id
    SQL

    query_params = {
      start_time:      start_time,
      end_time:        end_time,
      org_id:          org_id.to_i,
      num_top_users:   num_top_users,
      current_user_id: current_user_id,
      team_ids:        team_ids,
      user_ids:        user_ids
    }
    UsersTopicsActivity.find_by_sql([query, query_params])
  end

  def self.add_total_count(query_results)
    user_topic_counts = Hash.new do |hash, key|
      hash[key] = Hash.new(0)
    end
    query_results.each do |record|
      record.topic_counts.each do |(topic, count)|
        user_topic_counts[record.user_id][topic] += count
      end
    end
    user_topic_counts
  end

  # Topic names are period-delimited, but we only care about the final
  # topic in the list. So, we can dedupe by combining the counts for all topics
  # that are the same after the final period.
  def self.dedupe_topic_counts(topic_counts)
    topic_counts.group_by do |topic, count|
      topic.split(".").last
    end.reject do |key, val|
      key.blank?
    end.transform_values do |topics_with_same_name|
      topics_with_same_name.sum { |topic, count| count }
    end
  end

  def self.filter_topics!(user_topic_counts, num_top_topics)
    user_topic_counts.each do |user, user_results|
      deduped_topic_counts = dedupe_topic_counts(user_results)
      sorted_topic_counts = deduped_topic_counts.sort_by do |topic, count|
        count * -1
      end
      top_topic_counts = sorted_topic_counts.first(num_top_topics)
      user_topic_counts[user] = top_topic_counts.map do |topic_name, count|
        {
          name:  topic_name,
          count: count
        }
      end
    end
  end

  # Determine the topics => users map using the available users => topics info
  def self.derive_topics_users_map!(results)
    topics_users_map = Hash.new { |hash, key| hash[key] = { users: [] } }

    results[:users].each do |user_id, user_data|
      user_data[:topics].each do |topic_obj|
        topics_users_map[topic_obj[:name]][:users] << {
          id:    user_id,
          count: topic_obj[:count]
        }
      end
    end
    # apply explicit sorting by topic count here
    results[:topics] = topics_users_map.transform_values do |val|
      val.merge(users: val[:users].sort_by { |u| u[:count] }.reverse)
    end
  end

  def self.derive_teams_users_map!(results)
    teams_users_map = Hash.new { |hash, key| hash[key] = { users: [] } }

    results[:users].each do |user_id, user_data|
      user_data[:groups].each do |team|
        teams_users_map[team[:id]][:users].push(id: user_id)
      end
    end
    # apply explicit sorting by user id here
    results[:groups] = teams_users_map.transform_values do |val|
      val.merge(users: val[:users].sort_by { |u| u[:id] })
    end
  end

  def self.add_full_name_and_teams(user_topic_counts, team_ids)
    user_ids       = user_topic_counts.keys
    user_names_map = build_user_names_map(user_ids)
    teams_map      = build_user_teams_map(user_ids, team_ids)
    memo = { users: {} }
    results = user_topic_counts.reduce(memo) do |memo, (user_id, topics)|
      user = user_names_map[user_id]
      username, handle = user.values_at :full_name, :handle
      teams = teams_map[user_id] || []
      memo[:users][user_id] = {
        name:   username,
        handle: handle,
        groups: teams || [],
        topics: topics
      }
      memo
    end
    results
  end

  def self.build_user_teams_map(user_ids, team_ids)
    user_ids.in_groups_of(250, false).map do |user_ids_group|
      query = TeamsUser.
        where(user_id: user_ids_group).
        joins(:team).
        where.not("teams.is_everyone_team = true").
        select("teams_users.team_id, teams_users.user_id, teams.name")

      if team_ids&.present?
        query = query.where(team_id: team_ids)
      end

      query.group_by(&:user_id).transform_values do |user_teams|
        user_teams.map do |user_team|
          {
            id:   user_team.team_id,
            name: user_team.name,
          }
        end
      end
    end.reduce(&:merge)
  end

  def self.build_user_names_map(user_ids)
    user_ids.in_groups_of(250, false).map do |user_ids_group|
      User.
        where(id: user_ids_group).
        includes(:saml_providers).
        select("id, first_name, last_name, email, handle").
        index_by(&:id).
        transform_values do |user|
          {
            full_name: user.full_name,
            handle:    user.handle
          }
        end
    end.reduce(&:merge)
  end

  # Builds a SQL query fragment to filter result, if team_ids param given.
  def self.build_team_filter(team_ids:)
    return unless team_ids&.any?
    <<-SQL.strip_heredoc
      WHERE teams_users.team_id IN (:team_ids)
    SQL
  end

  # Builds a SQL query fragment to filter result, if user_ids param given
  def self.build_user_filter(user_ids:)
    return unless user_ids&.any?
    <<-SQL.strip_heredoc
      AND _activities_inner.user_id IN (:user_ids)
    SQL
  end

  # If current user is admin, only applies the team filter if it was
  # explicitly requested in the params (via "team_ids")
  #
  # If the current user is member, only shows users in the current user's
  # non-'everyone' teams (applies "team_ids" filter from params as well)
  #
  def self.build_user_teams_filter(is_admin:, team_filter:)
    # If the user is admin, we only filter on the "team_ids" param
    return if is_admin && !team_filter
    if is_admin && team_filter
      <<-SQL
      INNER JOIN (
        SELECT DISTINCT(user_id) FROM teams_users
        #{team_filter}
      ) AS _team_users
      ON _team_users.user_id = _activities_inner.user_id
      SQL
    # If the user is member, we apply defalt filtering as well.
    # We must join with teams here to exclude the 'everyone' team.
    elsif !is_admin
      <<-SQL.strip_heredoc
      INNER JOIN (
        SELECT DISTINCT(user_id) FROM teams_users
        INNER JOIN (
          SELECT DISTINCT(id) FROM teams
          INNER JOIN (
            SELECT DISTINCT(team_id) FROM teams_users
            WHERE teams_users.user_id = :current_user_id
          ) AS _current_user_teams
          ON _current_user_teams.team_id = teams.id
          WHERE teams.is_everyone_team = false
        ) AS _user_teams
        ON _user_teams.id = teams_users.team_id
        #{team_filter}
      ) AS _team_users
      ON _team_users.user_id = _activities_inner.user_id
      SQL
    end
  end

end
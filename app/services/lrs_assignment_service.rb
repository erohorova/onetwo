class LrsAssignmentService
  include CompoundIdReader
  STATUSES = ["assigned", "started", "completed"].freeze

  def initialize(user_id:, ecl_id: nil, assignment_id: nil, status:, start_date: nil, complete_date: nil, card: nil, assignor_id: nil, skip_notifications: nil)
    @status = status
    users = User.where(id: [user_id, assignor_id])
    @user = users.find{|user| user.id == user_id}
    @organization = @user.organization
    @ecl_id = ecl_id
    @card = card
    @assignment_id = assignment_id
    @start_date = start_date
    @complete_date = complete_date
    @assignor = users.find{|user| user.id == assignor_id} || @organization.real_admins.first
    @skip_notifications = skip_notifications
  end

  def run
    @assignment = Assignment.find_by(id: @assignment_id) if @assignment_id.present?

    if @assignment.blank?
      if @card.blank?
        @card = create_card_from_ecl
      end
      
      # To allow skipping email for provider assignments, when
      # 1. Notifications are skipped explicitly by passing skip_notifications as true
      # 2. If the status passed is either started/completed
      skip_notifications = @skip_notifications || (@status != 'assigned')

      @assignment = @card.create_assignment(
        user: @user, assignor: @assignor, opts: {skip_mail: true, completed_at: @complete_date, started_at: @start_date, 
          skip_notifications: skip_notifications}
      )

      @user.user_content_completions
        .where(completable: @card)
        .first_or_create
    elsif @assignment.assignable_type == "Card"
      @card = @assignment.assignable
      @assignment = @card.update_assignment(
        user: @user, assignor: @assignor, opts: {skip_mail: true, completed_at: @complete_date, started_at: @start_date }
      )
    end

    update_status
    @assignment
  end

  def update_status
    begin
      case @status
        when "started"
          @assignment.start!
        when "completed"
          # If it is assigned, start and then complete
          if @assignment.assigned?
            @assignment.start!
          end

          @assignment.complete!
          track_assignment
          UpdateClcScoreForUserJob.perform_later(@card.id, @user.id, @user.organization_id, "Organization") if @card.present?
      end
    rescue AASM::InvalidTransition => e
      log.info "Invalid state transition: #{e.message} for assignment with id: #{@assignment.id}"
    end
  end

  def track_assignment
    Tracking.track_act(
      user_id: @user.id,
      object: @assignment.assignable_type.underscore,
      object_id: @assignment.assignable_id,
      action: 'mark_completed_assigned',
      organization_id: @organization.id,
      platform: nil,
      device_id: nil
    )
  end

  def create_card_from_ecl
    EclApi::CardConnector.new(
      ecl_id: @ecl_id,
      organization_id: @organization.id
    ).card_from_ecl_id
  end
end
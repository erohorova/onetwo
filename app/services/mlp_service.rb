class MlpService
  attr_accessor :user_id, :card_types, :limit

  def initialize(opts = {})
    @user_id    = opts[:user_id]
    @limit      = opts[:limit] || 5
    @card_types = opts[:card_types]
  end

  def latest_assignments
    # Get user assignments that are not dismissed
    base_query = Assignment.not_dismissed
      .joins(:card)
      .where(user_id: user_id)

    #filter assignments by card type
    if card_types
      base_query = base_query.where(cards: { card_type: card_types })
    end

    @assignments = base_query.order(updated_at: :desc).limit(limit)
  end
end

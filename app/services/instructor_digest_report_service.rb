# Generate and mail XLS report of courses per language for v2 client instructors.
# Aggregates all data for users present in www as well as in specific orgs.
# Aggregates data for a user in specific org and is not present in www org.
# Sends single email to user for all the data from www and a specific org.

class InstructorDigestReportService < ReportService
  HEADERS = ['Course Name', 'Course Code', 'Course URL', 'Post Creator', 'Post Title', 'Post Message', 'Post URL','Comment Creator', 'Comment Message']

  def eligible_v2_org_ids
    Config.where(name: 'app.config.instructor_daily_digest_email_v2', configable_type: 'Organization', value: 'true').pluck(:configable_id)
  end

  # Returns instructor groups ids based on language
  #
  # Example:
  #   # instructors_with_resource_groups(org_id)
  #   AR collection -> [{user.id, user.email, group_ids, language}, ...]
  #                    [{id: 1, email:"abc@edcast.com", group_ids: "3,4,5,6,7", language: "en"}, ...]
  #
  def instructors_with_resource_groups(org_id)
    User.joins(groups_users: :group).where(
      "groups.close_access != 1 and groups.type = ? and groups.organization_id = ? and
       groups_users.as_type = ? and groups_users.banned = 0", 'ResourceGroup', org_id, 'admin').
      group("users.id, groups.language").
      select("users.id, users.email, GROUP_CONCAT(groups.id) as group_ids, groups.language as language")
  end

  # Returns collection of {language of group, instructor's id, ids of groups}
  # Handles www org instructors using email of instructor
  #
  # Example:
  #   # instructors(org_id)
  #   [
  #     {'language' => 'en', 'id' => 1, group_ids => ['1','2','3','4','5']},
  #     {'language' => 'fr', 'id' => 2, group_ids => ['6','7','8','9','10']},
  #     ....
  #   ]
  #
  def instructors(org_id)
    data = {}
    instructor_collection = []

    # Collect www org instructors using common email id
    #
    # {
    #   "abc@edcast.com" => {
    #      "id" => "1",
    #      "language_groups" => { "ar" => "1,2,3,", "en"=>"4,5,6," }
    #    },
    #   "xyz@edcast.com" => {
    #.     "id" => "2",
    #      "language_groups" => { "es-es" => "1,2,3,", "en"=>"4,5,6," }
    #    },
    #    ...
    # }
    #
    instructors_with_resource_groups(org_id).each do |record|
      data[record["email"]] ||= {}
      data[record['email']]['id'] = record['id'] # Gets organization specific instructor id
      data[record['email']]['language_groups'] ||= {}
      data[record['email']]['language_groups'][record['language']] ||= ''
      data[record['email']]['language_groups'][record['language']] << record['group_ids'] + ','
    end

    data.each do |key, value|
      value['language_groups'].each do |language, group_ids|
        instructor_collection << {
          "id" => value['id'],
          "language" => language,
          "group_ids" => group_ids.split(',').uniq
        }
      end
    end

    instructor_collection
  end

  # Returns posts for groups
  #
  # Course with posts give:
  # row_data = ['Course Name', 'Course Code', 'Course URL', 'Post Creator', 'Post Title', 'Post Message', 'Post URL']
  #
  # Course with no posts give:
  # row_data = ['Course Name', 'Course Code', 'Course URL', 'No new posts']
  #
  def posts_rows_for(group_ids:, from:, to:)
    post_rows = []

    Group.where(id: group_ids).includes(:posts).each do |resource_group|
      posts = posts_for(resource_group: resource_group, from: from, to: to)
      comments = comments_for(resource_group: resource_group, from: from, to: to)
      
      posts.each do |post|
        post_comments = comments.select{|comment| comment.commentable_id == post.id}
        comments -= post_comments

        if( post_comments.length > 0 )
          post_comments.each do |comment|
            post_rows << resource_group.digest_attrs +
              [post.user.name, post.title, Sanitize.clean(post.message), Post.url(post, resource_group),
                comment.user.name, Sanitize.clean(comment.message)]
          end
        else
          post_rows << resource_group.digest_attrs +
            [post.user.name, post.title, Sanitize.clean(post.message), Post.url(post, resource_group),
              " - ", "No new comments"]
        end
      end
      
      comments.each do |comment|
        log.info "comment: #{comment}"
        post_rows << resource_group.digest_attrs +
              [comment.commentable.user.name, comment.commentable.title, Sanitize.clean(comment.commentable.message), Post.url(comment.commentable, resource_group),
                comment.user.name, Sanitize.clean(comment.message)]
      end

      posts.empty? && comments.empty? && post_rows << resource_group.digest_attrs + ["No new posts"]
    end

    post_rows
  end

  # Returns posts for a resource group for duration
  #
  def posts_for(resource_group: , from: , to:)
    resource_group.posts.where(created_at: from...to)
  end

  # Returns comments for a resource group for duration
  #
  def comments_for(resource_group:, from: , to:)
    group_post_ids = resource_group.posts.pluck(:id)
    Comment.includes(commentable: [:user]).where(commentable_type: "Post", commentable_id: group_post_ids, created_at: from...to)
  end

  # Triggers report job for instructors in each org
  #
  def send_report_to_instructors(from:, to:)
    eligible_v2_org_ids.each do |org_id|
      instructors(org_id).each do |instructor_data|
        InstructorDailyDigestReportJob.perform_later(instructor_data: instructor_data, from: from, to: to)
      end
    end
  end

  # Generates xls and sends report to instructor
  #
  # Example:
  #   instructor_data = {"language" => 'en', 'id' => 1, 'group_ids' => [1,2,3,4,5]}
  #   from            = 1.day.ago.utc
  #   to              = Time.now.utc
  #
  #   generate_report(instructor_data:, from:, to:)
  #
  def generate_report(instructor_data:, from:, to:)
    instructor_id   = instructor_data['id']
    language        = LOCALE_TO_LANGUAGE.fetch(instructor_data['language'], 'English')
    group_ids       = instructor_data['group_ids']

    posts           = posts_rows_for(group_ids: group_ids, from: from, to: to)
    files           = generate_xls_parts([HEADERS] + posts) # HEADERS as first row

    file = files.length > 1 ? format_tar_gz(files) : files[0]
    aws_url = upload_report_to_s3(file: file,
                      filename: "#{instructor_id}_#{language}_#{Time.now.strftime("%d-%m-%y-%H-%M-%S")}",
                      bucket_name: "edcast-instructor-daily-digest-report-#{Rails.env}")
    close_file file
    #log.info "aws_url: #{aws_url}"
    UserMailer.send_instructor_daily_digest_report(instructor_id, aws_url, language).deliver_later
  end
end

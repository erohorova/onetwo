class CardImageCleanupService

  def initialize(card:)
    @card = card
  end

  def update_filestack_image_if_expired
    update_filestack = false
    @card.filestack.each do |fs|
      if filestack_is_image?(mimetype: fs["mimetype"]) && url_expired?(fs["url"])
        # Replaces expired filestack images with the Stock Images. 
        # In case, if filestack image is deleted from the FS, then stock image will be used.
        image_file = get_default_image
        fs["url"] = image_file[:url]
        fs["handle"] = image_file[:handle]
        fs["size"] = image_file[:size]
        fs["mimetype"] = image_file[:mimetype]
        update_filestack = true
      end
    end
    @card.update_column(:filestack, @card.filestack) if update_filestack
  end

  def url_expired?(url)
    return false if url.nil? # Skipping check for empty urls
    connection = Faraday.new do |conn|
      conn.use FaradayMiddleware::FollowRedirects
      conn.adapter Faraday.default_adapter
    end
    # Consider url to be invalid if the farday connection throws an exception
    begin
      response = connection.get(url)
    rescue
      return true
    end
    !response.success?
  end

  def check_and_update_card_images
    image_updated = update_filestack_image_if_expired

    resource = @card.resource
    if resource&.image_url && url_expired?(resource&.image_url)
      # Replaces expired image_urls with stock images
      image_updated = resource.update_column(:image_url, get_default_image[:url])
    end
    if image_updated
      @card.reindex_async
    end
  end

  def get_default_image
    image_index = rand(0..90)
    CARD_STOCK_IMAGE[image_index]
  end

  def filestack_is_image?(mimetype:)
    (/^image\/(png|svg\+xml|webp|jpeg|jpg|pjpeg|x-png|gif|tiff)$/.match(mimetype).present?)
  end
end

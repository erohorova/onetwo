require 'scorm_cloud'

class ScormCompletionService

  def initialize
    @scorm_cloud = initiate_scorm
  end

  def get_registrations
    @scorm_cloud.registration.get_registration_list
  end

  def get_completion_data(reg_id)
    @scorm_cloud.registration.get_registration_result(reg_id)
  end

  def initiate_scorm
    ScormCloud::ScormCloud.new(Settings.scorm.app_id, Settings.scorm.secret)
  end

  def run
    get_registrations.each do |registration|
      complete_with_registration(registration)
    end
  end

  def get_user_course_ids(reg_id)
    index_of_split = reg_id.index('-')
    user_id = extract_user_id(reg_id, index_of_split)
    course_id = extract_course_id(reg_id, index_of_split)
    [User.find_by(id: user_id), course_id]
  end

  def complete_course(result, course_id, user, reg_id)
    org = user.organization
    unless org.auto_mark_as_complete_enabled?
      log.info("Organization: #{org.host_name} has disabled auto mark as compelte") and return
    end
    if get_response_status(result) == 'ok'
      completion_status = course_completion_status(result)
      if completion_status == 'complete'
        card = get_card(org, course_id)
        if card.present?
          complete!(card, user)
        else
          log.info("No card found for course_id: #{course_id}")
        end
      else
        log.info("course: #{course_id} not completed by user: #{user.id} for registration: #{reg_id}")
      end
    else
      log.error("course: #{course_id} for registration: #{reg_id} & err: #{result['rsp']['err']}")
    end
  end

  def complete_course_by_date(completed_date, course_id, user, reg_id)
    org = user.organization
    unless org.auto_mark_as_complete_enabled?
      log.info("Organization: #{org.host_name} has disabled auto mark as compelte") and return
    end

    completed_date = DateTime.parse(completed_date) rescue nil
    if completed_date.present?
      card = get_card(org, course_id)
      if card.present?
        complete!(card, user)
      else
        log.info("No card found for course_id: #{course_id}")
      end
    else
      log.info("course: #{course_id} for registration: #{reg_id} not completed")
    end
  end

  def extract_reg_id(result_hash)
    (result_hash['rsp'] && result_hash['rsp']['registrationreport']['regid']) || result_hash['registrationreport']['regid']
  end


  def complete_with_result(scorm_result)
    result = hasify_xml(scorm_result)
    reg_id = extract_reg_id(result)
    user, course_id = get_user_course_ids(reg_id)
    if user.present?
      complete_course(result, course_id, user, reg_id)
    end
  end

  def complete_with_registration(registration)
    reg_id = registration.registration_id
    user, course_id = get_user_course_ids(reg_id)
    complete_course_by_date(registration.completed_date, course_id, user, reg_id) if user.present?
  end


  def extract_user_id(reg_id, index)
    reg_id[0...index]
  end

  def extract_course_id(reg_id, index)
    reg_id[(index+1)..-1]
  end

  def hasify_xml(xml)
    Hash.from_xml(xml)
  end

  def get_response_status(response)
    return 'ok' if (response['rsp'] && response['rsp']['stat'] == 'ok') || response['registrationreport']
  end

  def course_completion_status(response)
    (response['rsp'] && response['rsp']['registrationreport']['complete']) || response['registrationreport']['complete']
  end

  def complete!(card, user)
    if user.organization_id == card.organization_id
      user.complete_card(card)
    else
      log.info("User: #{user.id} and card: #{card.id} have different orgs")
    end
  end

  def get_resource(org, course_id)
    Resource.where(url_hash: Resource.get_url_hash("#{org.home_page}/api/scorm/launch?course_id=#{course_id}")).first
  end

  def get_card(org, course_id)
    card_id = course_id.split(org.host_name).try(:[], 1)
    card = if card_id.present?
      Card.where(id: card_id).first
    else
      get_resource(org, course_id)&.cards&.first
    end
    card
  end
end
module Okta
  class Provision
    CREATE_GROUP_PATH     = '/api/v1/groups'
    CREATE_IDP_PATH       = '/api/v1/idps'
    CREATE_APP_PATH       = '/api/v1/apps'
    LINK_APP_GROUP        = '/api/v1/apps/app_id/groups/group_id'

    def initialize(organization:)
      @organization = organization
      @okta_domain = @organization.current_okta_domain
    end

    def create_group(opts:)
      post_params = {"profile" => {"name" => opts["name"], "description" => opts['description']}}
      BaseCommunicator.post_connection(path: CREATE_GROUP_PATH,
                                      payload: post_params,
                                      token: @organization.okta_api_token,
                                      domain: @okta_domain)
    end

    def create_facebook_idp
      parameters = {"type" => "FACEBOOK","name"=> "#{@organization.name} Facebook",
                    "protocol"=> {
                      "type"=> "OAUTH2","scopes"=> Settings.facebook.scope.split(','),
                      "credentials"=> {
                        "client"=> {"client_id"=> Settings.facebook.api_key,"client_secret"=> Settings.facebook.secret}
                      }
                    },
                    "policy"=> {
                      "provisioning"=> {
                        "action"=> "AUTO","profileMaster"=> true,
                        "groups"=> {"action": "ASSIGN","assignments": [@organization.linked_okta_groups]},
                        "conditions"=> {"deprovisioned"=> {"action"=> "NONE"},"suspended"=> {"action"=> "NONE"}}
                      },
                      "accountLink"=> {"action"=> "AUTO"},
                      "subject"=> {"userNameTemplate"=> {"template"=> "idpuser.email"},"matchType"=> "USERNAME"},
                      "maxClockSkew"=> 0
                    }
                  }
      create_idp(parameters)
    end

    def create_google_idp
      parameters = {"type": "GOOGLE","name": "#{@organization.name} Google",
                    "protocol": {"type": "OAUTH2","scopes": Settings.google.scope.split(','),
                      "credentials": {
                        "client": {"client_id": Settings.google.client_id,"client_secret": Settings.google.secret}
                      }
                    },
                    "policy": {
                      "provisioning": {"action": "AUTO","profileMaster": true,
                        "groups": {"action": "ASSIGN","assignments": [@organization.linked_okta_groups]},
                        "conditions": {"deprovisioned": {"action": "NONE"},"suspended": {"action": "NONE"}}
                      },
                      "accountLink": {"action": "AUTO"},
                      "subject": {"userNameTemplate": {"template": "idpuser.email",},"matchType": "USERNAME"},
                      "maxClockSkew": 0
                    }
                  }
      create_idp(parameters)
    end

    def create_linkedin_idp
      parameters = {"type": "LINKEDIN","name": "#{@organization.name} LinkedIn",
                    "protocol": {"type": "OAUTH2","scopes": Settings.linkedin.scope.split(' '),
                      "credentials": {
                        "client": {"client_id": Settings.linkedin.api_key,"client_secret": Settings.linkedin.secret}
                      }
                    },
                    "policy": {
                      "provisioning": {"action": "AUTO","profileMaster": true,
                        "groups": {"action": "ASSIGN","assignments": [@organization.linked_okta_groups]}
                      },
                      "accountLink": {"action": "AUTO"},
                      "subject": {"userNameTemplate": {"template": "idpuser.email",},"matchType": "USERNAME"},
                      "maxClockSkew": 0
                    }
                  }
      create_idp(parameters)
    end

    def create_idp(post_params)
      BaseCommunicator.post_connection(path: CREATE_IDP_PATH,
                                      payload: post_params,
                                      token: @organization.okta_api_token,
                                      domain: @okta_domain)
    end

    def create_app(opts)
      client_id = opts[:client_id]
      name = "INSTANCE - #{@organization.name}"
      parameters = {"name": "oidc_client","label": name,"signOnMode": "OPENID_CONNECT",
                    "credentials": {
                      "oauthClient": {
                        "client_id": client_id, 
                        "autoKeyRotation": true,
                        "token_endpoint_auth_method": "client_secret_post"
                      }
                    },
                    "settings": {
                      "oauthClient": {
                        "client_uri": "https://example.com",
                        "redirect_uris": ["#{opts[:app_url]}/auth/lxp_oauth/callback"],
                        "response_types": ["token","id_token","code"],
                        "grant_types": ["implicit","authorization_code", "refresh_token", "client_credentials"],
                        "application_type": "web"
                      }
                    }
                  }
      BaseCommunicator.post_connection(path: CREATE_APP_PATH,
                                      payload: parameters,
                                      token: @organization.okta_api_token,
                                      domain: @okta_domain)
    end

    def link_app_with_group(opts)
      LINK_APP_GROUP.gsub!('app_id',opts[:app_id]).gsub!('group_id',opts[:group_id])
      BaseCommunicator.put_connection(path: LINK_APP_GROUP,
                                      token: @organization.okta_api_token,
                                      domain: @okta_domain)
    end
  end
end
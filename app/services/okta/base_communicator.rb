module Okta
  class BaseCommunicator

    class << self
      def get_connection(path:, token:, domain:)
        channel = Faraday.new(url: domain)
        response = channel.get do |request|
          request.url path
          request.headers['Content-Type'] = 'application/json'
          request.headers['Accept'] = 'application/json'
          request.headers['Authorization'] = "SSWS #{token}"
        end
        if response.success?
          JSON.parse(response.body)
        else
          log.warn("Response: #{response.inspect}")
          return nil
        end
      end

      def post_connection(path:, payload: {}, token:, domain:)
        channel = Faraday.new(url: domain)
        response = channel.post do |request|
          request.url path
          request.headers['Content-Type'] = 'application/json'
          request.headers['Accept'] = 'application/json'
          request.headers['Authorization'] = "SSWS #{token}"
          request.body = payload.to_json
        end
        if response.success?
          JSON.parse(response.body)
        else
          log.warn("Response: #{response.inspect}")
          return response
        end
      end

      def put_connection(path:, payload: {}, token:, domain:)
        channel = Faraday.new(url: domain)
        response = channel.put do |request|
          request.url path
          request.headers['Content-Type'] = 'application/json'
          request.headers['Accept'] = 'application/json'
          request.headers['Authorization'] = "SSWS #{token}"
          request.body = payload.to_json
        end
        if response.success?
          JSON.parse(response.body)
        else
          log.warn("Response: #{response.inspect}")
          return response
        end
      end

      def delete_connection(path:, id: , token:, domain:)
        okta_connection = Faraday.new(url: domain)
        begin
          response = okta_connection.delete do |request|
            request.url (path.gsub('uid', id))
            request.headers['Content-Type'] = 'application/json'
            request.headers['Accept'] = 'application/json'
            request.headers['Authorization'] = "SSWS #{token}"
          end
        rescue Exception => e
          log.warn("#{e.inspect}")
          return response
        end
        if response.success?
          true
        else
          log.warn("Resource Not found")
          nil
        end
      end
    end
  end
end
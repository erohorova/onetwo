module Okta
  class InternalFederation
    # Error codes:
    #   1. E0000001 : Already exists
    #   ...

    # Paths:
    AUTHN_USER_PATH       = '/api/v1/authn'
    AUTHORIZE_PATH        = '/oauth2/v1/authorize'
    CREATE_USER_PATH      = '/api/v1/users?activate=true'
    FIND_USER_PATH        = '/api/v1/users/'
    SESSION_PATH          = '/login/sessionCookieRedirect'
    RESET_PASSWORD_PATH   = '/api/v1/users'
    SIGN_OUT_SESSION      = '/api/v1/users/uid/sessions'

    def initialize(organization:)
      @organization = organization
      @okta_domain = organization.current_okta_domain
    end

    # Class methods - Starts here ##
    # ...
    # ...
    # Class methods - Ends here ##

    # Instance methods - Starts here ##

    # Refer: https://developer.okta.com/docs/api/resources/oidc.html#response-parameters-4
    # Includes refresh_token in the request
    # TODO:
    #   Need to fix this method. Not used anywhere.
    def authorize_uri(user:, redirect_url:, opts: {})
      email_sso = @organization.federated_email_sso
      response = session_token(opts: opts)

      if response.nil?
        return redirect_url
      end

      token = response['sessionToken']
      uri = URI.join(@okta_domain, AUTHORIZE_PATH)
      uri.query = URI.encode_www_form({
        sessionToken: response['sessionToken'],
        redirect_uri: redirect_url,
        client_id: email_sso.oauth_client_id,
        response_type: 'code',
        scope: email_sso.oauth_scopes,
        prompt: 'none',
        state: 'Af0ifjslDkj',
        nonce: 'YsG76jo'
      })

      uri.to_s
    end

    def federated_username(opts)
      [opts[:username], "#{@organization.id}-#{opts[:email]}"].detect(&:presence)
    end

    # Okta requires First + Last name, Email and Password to create a user.
    # Old registration API and CSV supports singular email field currently too.
    # First and Last name will be pre-filled to some values and will be updated during onboarding.
    def create_user(opts = {})
      post_params = {
        profile: {
          firstName: [opts[:first_name], 'First name'].detect(&:present?),
          lastName: [opts[:last_name], 'Last name'].detect(&:present?),
          email: opts[:email],
          login: opts[:username],
          employeeNumber: opts[:username],
          organization: @organization.id
        },
        credentials: {
          password: { value: opts[:password] }
        }
      }

      group_ids = @organization.linked_okta_groups

      if group_ids.present?
        group_ids = group_ids.split(',')
        post_params.merge!(groupIds: group_ids)
      end

      BaseCommunicator.post_connection(path: CREATE_USER_PATH, 
                                      payload: post_params, 
                                      token: @organization.okta_api_token,
                                      domain: @okta_domain)
    end

    def federated_user(username:)
      url = FIND_USER_PATH + CGI.escape(username)
      BaseCommunicator.get_connection(path: url, token: @organization.okta_api_token, domain: @okta_domain)
    end

    def session_redirect_uri(redirect_url:, opts: {})
      response = session_token(opts: opts)

      if response.nil?
        return redirect_url
      end

      token = response['sessionToken']
      uri = URI.join(@okta_domain, SESSION_PATH)
      uri.query = URI.encode_www_form({ token: response['sessionToken'], redirectUrl: redirect_url })

      uri.to_s
    end

    def sign_out_user(id)
      BaseCommunicator.delete_connection(path: SIGN_OUT_SESSION, 
                                        id: id, 
                                        token: @organization.okta_api_token,
                                        domain: @okta_domain)
    end

    def session_token(opts:)
      decoded_value = Base64.decode64(opts[:password])
      post_params = {
        username: opts[:federated_identifier],
        password: decoded_value,
        options: {
          multiOptionalFactorEnroll: false,
          warnBeforePasswordExpired: false
        }
      }

      BaseCommunicator.post_connection(path: AUTHN_USER_PATH, 
                                      payload: post_params, 
                                      token: @organization.okta_api_token,
                                      domain: @okta_domain)
    end

    # TODO:
    def suspend_user
    end

    # TODO:
    def unsuspend_user
    end

    def update_user(user:, opts:)
      post_params = {
        profile: {
          firstName: opts[:first_name],
          lastName:  opts[:last_name],
          employeeNumber: user.id,
          organization: @organization.id
        }
      }

      if opts[:password].present?
        post_params.merge!(credentials: { password: { value: opts[:password] }})
      end

      BaseCommunicator.post_connection(path: RESET_PASSWORD_PATH + "/#{user.federated_identifier}", 
                                      payload: post_params, 
                                      token: @organization.okta_api_token,
                                      domain: @okta_domain)
    end

    def user(opts = {})

      username = federated_username(opts)
      opts.merge!(username: username)
      _user = federated_user(username: username)

      if _user.nil?
        _user = create_user(opts)
      end

      _user
    end

  end
end
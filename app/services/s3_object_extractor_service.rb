# frozen_string_literal: true

require 'aws-sdk'
class S3ObjectExtractorService

  def initialize(bucket:, region: nil)
    @errors = {}
    @bucket = bucket
    # if region nil -> set default
    @region = region.presence || 'us-east-1'
  end

  class << self
    # console link
    # https://s3.console.aws.amazon.com/s3/buckets/<!bucket_name!>/?region=<!region!>
    #
    # virtual-hosted-style URL:
    #
    # http://bucket.s3.amazonaws.com
    # http://bucket.s3-aws-region.amazonaws.com
    #
    # path-style URL:
    #
    # http://s3.amazonaws.com/bucket
    # http://s3-aws-region.amazonaws.com/bucket
    def extract_bucket_info(link:)
      return unless link
      # ensure the link is valid url
      url = UrlMetadataExtractor.return_url(link)
      return unless url
      # take list of all buckets for provided aws creds
      bucket_name_list = s3_client_default.list_buckets[:buckets].map(&:name)

      # for console links try to extract region if it present
      query_region = Rack::Utils.parse_query(URI(link).query)['region']
      # extract path names which could be possible bucket names
      path_style = URI(url).path.split('/')
      # extract bucket name for virtual-hosted links
      vh_style = url.gsub(/^(http|https):\/{2}/, '').split('.')

      possible_regions = [vh_style.second, vh_style.first]
      possible_buckets = [path_style.first, path_style.last, vh_style.first]

      # detect if possible bucket names in our bucket list
      bucket_name = possible_buckets.compact.detect do |bucket|
        bucket.in?(bucket_name_list)
      end

      # region is needed in sdk to correctly detect target bucket
      raw_region = possible_regions.compact.detect do |region|
        region.start_with?('s3-')
      end

      region_name = raw_region.nil? ? query_region : raw_region.gsub(/^(s3-)/, '')
      # return info extracted from link
      {bucket: bucket_name, region: region_name}
    end

    def s3_client_default(region: 'us-east-1')
      # set Aws client version 2+ with default region
      Aws::S3::Client.new(
        access_key_id: Settings.aws_access_key_id,
        secret_access_key: Settings.aws_secret_access_key,
        region: region
      )
    end
  end

  def extract_image_objects
    sample_public_url = "https://#{@bucket}.s3.amazonaws.com/"
    return unless @bucket
    begin
      object_key_list = s3_client
        .list_objects(bucket: @bucket)
        .contents
        .map { |obj| obj[:key] }
    rescue Aws::S3::Errors::InvalidAccessKeyId => e
      log.warn(
        "S3ObjectExtractorService #extract_image_objects error. #{e.inspect}"
      )
      @errors[:aws_access] = 'AWS access error. Invalid keys'
      return
    end

    return if object_key_list.blank?

    public_objects_urls = []
    errors_key_objects = []

    object_key_list.each do |key|
      begin
        content_type = s3_client.head_object(
          {bucket: @bucket, key: key}
        )[:content_type]
      rescue => e
        errors_key_objects.push(key)
        msg = "S3ObjectExtractorService #extract_image_objects error for #{key}."
        msg_addition = " Code:  #{e.code}. Message: #{e.message}"
        log.warn(
          msg + msg_addition
        )
      end
      next unless content_type&.start_with?('image/')
      public_objects_urls.push(sample_public_url + key)
    end

    @errors[:object_errors] = errors_key_objects
    public_objects_urls
  end

  def failures
    @errors
  end

  private

  def s3_client
    self.class.s3_client_default(region: @region)
  end
end

# coding: utf-8
# frozen_string_literal: true

# This service creates assignment, team assignments, learning queue items records in bulk
# Approach :
# Checked existing assignments, team assignments, user content completion, 
#   learning queue items, notification entries of assignee in batches of 200
#   if existing records found update records
#   else make new record object and bulk insert in database
#   made new job for analytics record and bulk sending notifications of newly created assignments and team assignments
# All callbacks of assignment and team assignments handled in this service

class BulkAssignService

  BULK_BATCH_SIZE = 200

  def initialize(team: , assignor_id: , assignable_type: , assignable_id: , opts: {})
    @team           = team
    @assignor       = User.where(id: assignor_id, organization_id: @team.organization_id).first
    @assignable     = assignable_type.classify.constantize.find_by_id(assignable_id)
    @opts           = opts
  end

  def run(assignee_ids: [], batch_size: BULK_BATCH_SIZE)
    assignee_ids.each_slice(batch_size) do |assignee_ids_batch|
      prefetch_existing_records(assignee_ids_batch)
      new_assignments, new_team_assignments, new_learning_queues = [], [], []
      existing_assignments, existing_lq_ids = [], []
      @team.users.where(id: assignee_ids_batch).find_each(batch_size: batch_size) do |user|
        next if user == @assignor && !@opts[:self_assign]

        if (existing_assignment = @existing_assignments[user.id]).present?
          existing_assignments << updated_assignment(existing_assignment)
          unless @existing_team_assignment_ids.include?(existing_assignment.id)
            new_team_assignments << new_team_assignment_object(existing_assignment)
          end
        elsif valid_assignee?(user)
          new_assignments << new_assignment_object(user)
        end

        if (existing_lq = @existing_learning_queue[user.id]).present?
          existing_lq_ids << existing_lq[0] if existing_lq[1].nil?
        else
          new_learning_queues << new_learning_queue_object(user.id)
        end
      end
 
      import_assignment_records(new_assignments, new_team_assignments, new_learning_queues)
      update_assignment_records(existing_assignments, existing_lq_ids)
    end
  end

  # Fetch existing assignments, team assignments, user content completion, 
  # learning queue items, notification entries of assignee
  def prefetch_existing_records(assignee_ids)
    @existing_assignments = Assignment.where(
                              user_id: assignee_ids, 
                              assignable: @assignable, 
                              title: @assignable.try(:title) || @assignable.try(:message)
                            ).index_by(&:user_id)
    
    @existing_team_assignment_ids  = TeamAssignment.where(
                                      assignment_id: @existing_assignments.values.map(&:id),
                                      team_id: @team.id
                                    ).pluck(:assignment_id)

    @existing_ucc_user_ids = UserContentCompletion.where(
                              user_id: assignee_ids,
                              completable: @assignable, 
                              state: UserContentCompletion::COMPLETED
                            ).pluck(:user_id)

    existing_learning_queue = LearningQueueItem.where(
                                user_id: assignee_ids,
                                queueable: @assignable
                              ).pluck(:user_id, :id, :state)

    # {user_id1: [lq_id1, lq_state1], user_id2: [lq_id2, lq_state2]}
    @existing_learning_queue = existing_learning_queue.each_with_object({}) do |lq, hash|
      hash[lq[0]] = [lq[1], lq[2]]
    end

    @existing_notification_user_ids = NotificationEntry.where(
                                      user_id: assignee_ids,
                                      sourceable_type: Assignment.name,
                                      sourceable_id: @existing_assignments.values.map(&:id),
                                      notification_name: Notify::NOTE_ASSIGNED_CONTENT
                                    ).distinct.pluck(:user_id)
  end

  # If existing assignment is present for assignee update assignment attributes
  def updated_assignment(existing_assignment)
    existing_assignment.re_assign if existing_assignment.dismissed?
    check_ucc_record(existing_assignment)
  end

  # Make new assignment if assignee doesn't have assignment for assignable
  def new_assignment_object(user)
    new_assigment = Assignment.new(
                      assignee: user,
                      assignable: @assignable,
                      title: @assignable.try(:title) || @assignable.try(:message)
                    )
    check_ucc_record(new_assigment)
  end

  # Make new learning queue item if assignee doesn't have learning queue item for card
  def new_learning_queue_object(user_id)
    new_learning_queue = LearningQueueItem.new(
                          user_id: user_id,
                          queueable: @assignable,
                          source: "assignments",
                          state: LearningQueueItem::ACTIVE_STATE
                        )

    new_learning_queue.set_display_type
    new_learning_queue.set_deep_link_id_and_type
    new_learning_queue
  end

  # Make new team assignment if assignee doesn't have team assignment associated with current team
  def new_team_assignment_object(assignment)
    TeamAssignment.new(
      assignment: assignment,
      assignor: @assignor,
      team: @team,
      message: @opts[:message]
    )
  end

  # Update new assignment or existing assignment if assignee has user content completion record
  def check_ucc_record(assignment)
    assignment.due_at = Date.strptime(@opts[:due_at], "%m/%d/%Y").end_of_day if @opts[:due_at]
    if @existing_ucc_user_ids.include?(assignment.user_id)
      assignment.state        = Assignment::COMPLETED
      assignment.started_at   =  assignment.started_at.presence || Time.current
      assignment.completed_at =  assignment.completed_at.presence || Time.current
    end
    assignment
  end

  # Before making new assignment check assignee is vaild user
  def valid_assignee?(assignee)
    (assignee.is_active? || !assignee.is_suspended? || assignee.status == User::ACTIVE_STATUS) && 
      (assignee.organization_id == @assignable.organization_id)
  end

  # Insert assignment record in bulk
  # Make new team assignment object for newly created assignments
  def import_assignment_records(new_assignments, new_team_assignments, new_learning_queues)
    notify_assignment_ids = []
    assignment_import = Assignment.import(new_assignments)
    failed_user_ids = assignment_import.failed_instances.map(&:user_id)
    if failed_user_ids.present?
      log.info "Bulk Assignment creation failed for users #{failed_user_ids}, assignable #{@assignable.class.name} #{@assignable.id}"
    end

    Assignment.where(user_id: (new_assignments.map(&:user_id) - failed_user_ids), assignable: @assignable).each do |assignment|
      new_team_assignments << new_team_assignment_object(assignment)
      if !@opts[:skip_notification] && @existing_notification_user_ids.exclude?(assignment.user_id) && 
        (assignment.user_id != @assignor.id)
        notify_assignment_ids << assignment.id
      end
      assignment.send(:schedule_all_due_notifications)
    end
    
    notify_team_assignment_ids = import_team_assignment_records(new_team_assignments)
    import_learning_queue_records(new_learning_queues, failed_user_ids)

    BulkAssignAnalyticsRecordNotificationJob.perform_later({assignment_ids: notify_assignment_ids, team_id: @team.id, 
      organization_id: @team.organization_id, team_assignment_ids: notify_team_assignment_ids})
  end

  # Insert team assignment in bulk
  def import_team_assignment_records(new_team_assignments)
    team_assignment_import = TeamAssignment.import(new_team_assignments)
    failed_assignment_ids = team_assignment_import.failed_instances.map(&:assignment_id)
    team_assignment_ids = new_team_assignments.map(&:assignment_id) - failed_assignment_ids

    if failed_assignment_ids.present?
      log.info "Bulk Team Assignment creation failed for assignment_ids : #{failed_assignment_ids}"
    end

    TeamAssignment.where(assignment_id: team_assignment_ids, team_id: @team.id).pluck(:id)
  end

  # Insert new learning queue items in bulk
  def import_learning_queue_records(new_learning_queues, failed_user_ids)
    new_learning_queues.reject! { |lq| failed_user_ids.include?(lq.user_id) }
    lq_import = LearningQueueItem.import(new_learning_queues)
    failed_lq_ids = lq_import.failed_instances.map(&:user_id)

    if failed_lq_ids.present?
      log.info "Bulk learning queue item creation failed for learning_queue_item_ids : #{failed_lq_ids}"
    end
  end

  # Update existing assignment, learning queue items if they are changed
  def update_assignment_records(existing_assignments, existing_lq_ids)
    existing_assignments.each do |existing_assignment|
      existing_assignment.save if existing_assignment.changed?
    end
    LearningQueueItem.where(id: existing_lq_ids).update_all(state: LearningQueueItem::ACTIVE_STATE, updated_at: Time.current)
  end
end
# 1. DESC:
#   Users can be inactivated either from `suspended` or `active`.
#   In either case, suspended users associated data should not be hard-deleted from the system. They should be restored when inactivated.
#
# 2. ASSOCIATED DATA:
#   Let's assume U1 is user given an org.
#   2.1  U1 following a channel.
#   2.2  U1 following a user.
#   2.3  Other users following U1.
#   2.4  Cards commented by the U1.
#   2.5  Cards voted by the U1.
#   2.6  Comments were U1 is tagged.
#   2.7  Comments were U1 has tagged some other users. This is the comment itself. Taken care with #2.4
#   2.8  Activity streams generated for the cards authored by U1.
#   2.9  Activity streams commented by U1.
#   2.10 Activity streams liked by U1.
#   2.11 Activity streams were U1 is tagged. Taken care with #2.6.
#   2.12 Activity streams were U1 has tagged some other users. Taken care with #2.9.

# 3. REINDEXABLE DATA:
#   3.1  Channels that U1 follows.
#   3.2  Users that U1 follows.

# 4. COUNTER-CACHED COLUMNS:
#   4.1  Cards commented by U1.
#   4.2  Cards voted by U1.
#   4.3  Activity streams commented by U1.
#   4.4  Activity streams voted by U1.

# 5. Marks the user as `inactive`

# 6. Push the event `user_unsuspended` to Influx.

# 7. Reindex the user.

class UserTransitions::Inactive < UserTransitions::Base
  def mark

    # 1. Grab the suspended users so that their data can be restored, if any.
    suspended_ids = @users.map { |user| user.id if user.suspended? }.compact

    # 2. ASSOCIATED DATA
    # 2.1 restore channel followees.
    if suspended_ids.present?

      followees = Follow.only_deleted.where(user_id: suspended_ids)
      followed_channels = followees.where(followable_type: 'Channel')
      followed_channels_ids = followed_channels.map(&:followable_id)
      followed_channels.present? && followed_channels.update_all(deleted_at: nil)

      # 2.2 restore user followees.
      followed_users = followees.where(followable_type: 'User')
      followed_users.present? && followed_users.update_all(deleted_at: nil)

      # 2.3 restore user followers.
      followers = Follow.only_deleted.where(followable_id: suspended_ids, followable_type: 'User')
      follower_ids = followers.map(&:id)
      followers.present? && followers.update_all(deleted_at: nil)

      # 2.4 restore comments owned by the users.
      card_comments = Comment.only_deleted.where(user_id: suspended_ids, commentable_type: 'Card')
      commented_card_ids = card_comments.map(&:commentable_id) # pick the card IDs to calculate the comments_count.
      card_comments.present? && card_comments.update_all(deleted_at: nil)

      # 2.5 restore votes/likes.
      card_votes = Vote.only_deleted.where(user_id: suspended_ids, votable_type: 'Card')
      voted_card_ids = card_votes.map(&:votable_id) # pick the card IDs to calculate the votes_count.
      card_votes.present? && card_votes.update_all(deleted_at: nil)

      # 2.6 restore mentions that tags the users.
      tagged_user_mentions = Mention.only_deleted.where(user_id: suspended_ids, mentionable_type: ['Comment', 'ActivityStream'])
      tagged_user_mentions.present? && tagged_user_mentions.update_all(deleted_at: nil)

      # 2.8 restore activity streams pointing to the users.
      activity_streams = ActivityStream.only_deleted.where(user_id: suspended_ids)
      activity_streams.present? && activity_streams.update_all(deleted_at: nil)

      # 2.10 restore comments on activity streams.
      stream_comments = Comment.only_deleted.where(user_id: suspended_ids, commentable_type: 'ActivityStream')
      stream_commentable_ids = stream_comments.map(&:commentable_id)
      stream_comments.present? && stream_comments.update_all(deleted_at: nil)

      # 2.11 restore votes on activity streams.
      stream_votes = Vote.only_deleted.where(user_id: suspended_ids, votable_type: 'ActivityStream')
      stream_votable_ids = stream_votes.map(&:votable_id)
      stream_votes.present? && stream_votes.update_all(deleted_at: nil)

      # 3. REINDEXABLE DATA
      # 3.1 reindex channels the followees follows i.e `followed_channels`.
      channels = @organization.channels.where(id: followed_channels_ids)
      channels.each(&:reindex_async_urgent)

      # 3.2 reindex users the followees follows i.e `followers`.
      users = @organization.users.where(id: follower_ids)
      users.each(&:reindex_async_urgent)

      # 4. COUNTER-CACHED COLUMNS
      # 4.1 cards commented by the U1.
      cards = @organization.cards
      commented_card_ids.in_groups_of(10, false) do |card_ids|
        _cards = cards.where(id: card_ids)
        _cards.each do |card|
          Card.reset_counters(card.id, :comments)
        end
      end

      # 4.2 cards voted by the U1.
      voted_card_ids.in_groups_of(10, false) do |card_ids|
        _cards = cards.where(id: card_ids)
        _cards.each do |card|
          Card.reset_counters(card.id, :votes)
        end
      end

      # 4.3 activity streams commented by the U1.
      # 4.4 activity streams voted by the U1.
      (stream_commentable_ids + stream_votable_ids).in_groups_of(10, false) do |streams|
        _streams = ActivityStream.where(id: streams)
        _streams.each do |stream|
          ActivityStream.reset_counters(stream.id, :comments)
          ActivityStream.reset_counters(stream.id, :votes)
        end
      end
    end

    # 5. Mark user as inactive
    @users.each do |user|
      current_time = Time.now

      # 6. Mark user as inactive & record the event as `user_unsuspended` in Influx.
      user_unsuspended = user.suspended?

      user.update_columns(status: User::INACTIVE_STATUS, is_suspended: false, is_active: true, updated_at: current_time)

      if user_unsuspended
        # INFO: when user is in suspended state and will now be marked as `inactive`.
        # When the user is unsuspended/re-activated, `user_unsuspended` event is triggered.
        # Events keys: [:event, :actor, :org, :timestamp, :suspended_user, :additional_data]
        user.send(:push_user_unsuspended_metric)
      else
        # INFO: when user is in `active` state and will now be marked as `inactive`. We want to forcefully capture this event.
        user.record_custom_event(
          event: 'user_edited',
          actor: user,
          job_args: [{
            user: user,
            changed_column: 'status',
            old_val: 'active',
            new_val: 'inactive'
          }]
        )
      end

      # 7. Reindex the the user
      user.reindex_async
    end
  end
end

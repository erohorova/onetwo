# 1. Desc 
# User can be suspended either from 'active' or 'inactive' state.
# Associated data be should be soft deleted after suspending users.

# 2. ASSOCIATED DATA:
#   Let's assume user1 is user given an org.
#   2.1  user1 following a channel,user1 following a user and other users following user1.
#   2.2  Cards voted by the user1,Activity Stream Voted by user.
#   2.3  Cards,ActivityStream commented by the user1.
#   2.4  Comments were user1 is tagged.
#   2.5  Comments were user1 has tagged some other users. This is comment itself. Taken care with #2.3
#   2.6  Activity streams generated for the cards authored by the user1.
#   2.7  Activity streams commented by the user1.


# 3. COUNTER-CACHED COLUMNS:
#   3.1  Cards commented by the user1,Activity streams commented by the user1.
#   3.2  Activity streams voted by the user1,Cards voted by the user1.
#   3.3  Reset Channels followers Cache count
#   3.4  Reset Channels team followers Cache count for user in team

# 4. Marks the user as `suspend`

# 5. Push the event `user_suspended` to Influx.

# 6. Send user `delete` patch to Workflow

class UserTransitions::Suspend < UserTransitions::Base
  def mark
    user_ids = @users.map(&:id)

    # Following deletion are reversible
    ActiveRecord::Base.transaction do
      @current_time = Time.now
      # 2. ASSOCIATED DATA
      # 2.1 soft delete users followees, user followers and channel followees.
      follows = Follow.where("(followable_type = 'User' AND followable_id IN (:user_ids)) OR (user_id IN (:user_ids))", user_ids: user_ids)
      
      followables = follows.pluck(:followable_id,:followable_type)
      follows.update_all(deleted_at: @current_time)
      
      # Reload the deleted follows records and track the events as `user_unfollowed` & `channel_unfollowed`.
      follows = follows.with_deleted
      follows.each {|follow| follow.send(:record_unfollow_event)}

      # 2.2 soft delete votes associated with users
      votes =  Vote.where(voter: user_ids)
      votables = votes.pluck(:votable_id,:votable_type)
      votes.update_all(deleted_at: @current_time)

      # Reload the deleted votes records and track the events as `card_unliked`.
      votes = votes.with_deleted
      votes.each {|vote| vote.send(:record_vote_event,'card_unliked')}
      
      # 2.3 soft delete users comments
      comments = Comment.where(user: user_ids)
      commentables = comments.pluck(:commentable_id,:commentable_type)
      # Record card_comment_deleted event to influx db
      comments.each {|channel| channel.send(:record_card_uncomment_event)}
      comments.update_all(deleted_at: @current_time)

      # 2.4 soft delete users mentions 
      Mention.where(user: user_ids).update_all(deleted_at: @current_time)
      #2.6 soft delete users activity streams
      ActivityStream.where(user: user_ids).update_all(deleted_at: @current_time)

      # 3. COUNTER-CACHED COLUMNS
      # 3.1 Reset votes_count counter cache 
      votables.in_groups_of(10,false) do |votable_array| 
        votable_array.each do |votable|
          if ['ActivityStream','Card'].include? votable[1]
            klass = votable[1].constantize
            klass.with_deleted.reset_counters(votable[0], :votes)
          end
        end
      end

      # 3.2 Reset comments_count counter cache
      commentables.in_groups_of(10,false) do |commentable_array| 
        commentable_array.each do |commentable|
          # Reset only counter cache of ActivityStream and Card.
          if ['ActivityStream','Card'].include? commentable[1]
            klass = commentable[1].constantize
            klass.with_deleted.reset_counters(commentable[0], :comments)
          end
        end
      end

      # 3.3 Reset Channels followers Cache count
      followables.in_groups_of(10,false) do |followable_array|
        followable_array.each do |followable|
          if followable[1]== 'Channel'
            Channel.invalidate_followers_cache(followable[0])
          end
        end
      end

      # 3.4 Reset Channels team followers Cache count for user in team
      teams_channels_follows = TeamsChannelsFollow
        .joins('INNER JOIN teams_users ON teams_channels_follows.team_id = teams_users.team_id')
        .where(teams_users: {user_id: user_ids})
        .pluck(:channel_id).uniq

      teams_channels_follows.each do |channel_id|
        Channel.invalidate_followers_cache(channel_id)
      end

      # 4. Update User status and record the event as 'suspended' in Influx
      @users.each do |user|
        user.update_columns(is_suspended: true,status: 'suspended',is_active: false,updated_at: @current_time)
        user.reindex_async
        
        # 5. Mark user as suspend & record the event as `suspend` in Influx. 
        # When the user is suspended, `user_suspended` event is triggered.
        # Events keys: [:event, :actor, :org, :timestamp, :suspended_user, :additional_data]
        user.send(:push_user_suspended_metric)

        # 6. When the user is suspended, should be triggered `delete` patch sending to Workflow
        user.send(:detect_upsert)
      end
    end
  end
end

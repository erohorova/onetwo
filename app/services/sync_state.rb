class SyncState
  def initialize(card:, object:)
    @card = card
    @object = object
  end

  def assignment_update
    return false unless @object.is_a?(UserContentCompletion)

    assignment = Assignment.where(assignable: @card, user_id: @object.user_id)
      .where.not(state: 'dismissed').first

    return false if assignment.blank?

    assignment.start! if (@object.started? || @object.completed?) && assignment.assigned?
    assignment.complete! if @object.completed? && assignment.started?
    assignment.uncomplete! if @object.initialized? && assignment.completed?
  end

  def user_content_completion_update
    return false unless @object.is_a?(Assignment)

    ucc = UserContentCompletion.where(completable: @card, user_id: @object.user_id)
      .first_or_create

    ucc.start! if @object.started? && ucc.initialized?
    ucc.complete! if @object.completed? && (ucc.initialized? || ucc.started?)
    ucc.uncomplete! if @object.assigned? && ucc.completed?
  end
end

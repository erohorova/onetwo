class CloneRecordsService

  def initialize(organization_id)
    @organization_id = organization_id
  end

  def run(parent_object)
    clone_card(parent_object)
  end

  def clone_card(parent_card)
    card_type = parent_card.card_type
  
    case card_type
    when Card::TYPE_PACK
      clone_card_pack_relations(parent_card)
    when Card::TYPE_JOURNEY
      clone_journey_pack_relations(parent_card)
    else
      if (['poll', 'media', 'video_stream', 'course'].include? card_type)
        new_card = deep_clone_card(parent_card, card_type)
        begin
          if (new_card.is_a?(Card) && new_card.save!)
            return new_card
          else
            return false
          end
        rescue Exception => e
          log.error "unable to clone parent card #{parent_card.id}, error message #{e}"
        end
      else
        false
      end
    end
  end

  # clone pathway with its associated cards
  def clone_card_pack_relations(parent_card)
    card_pack_relations = CardPackRelation.pack_relations(cover_id: parent_card.id)
    card_ids = card_pack_relations.map(&:from_id)
    card_pack_relations.each do |cpr|
      if cpr.card.journey_card? # For curriculum
        card_ids << get_journey_pack_card_ids(cpr.card)
      end
    end
    all_cards = Card.where(id: card_ids.flatten.uniq.compact)
    # return false if pathway has private cards or project cards
    return false if journey_pack_unclonable?(all_cards)

    new_cover_card = deep_clone_card(parent_card, parent_card.card_type)
    begin
      if (new_cover_card.is_a?(Card) && new_cover_card.save!)
        new_pack_cards = []
        card_pack_relations.each do |cpr|
          next if cpr.cover_id == cpr.from_id
          card = all_cards.detect{ |card| card.id == cpr.from_id }
          if card.journey_card?
            new_card = clone_journey_pack_relations(card)
          else
            new_card = deep_clone_card(card, card.card_type)
          end
          if new_card.save!
            if new_cover_card.add_card_to_pathway(new_card.id, 'Card') && cpr.locked
              CardPackRelation.where(cover_id: new_cover_card.id, from_id: new_card.id).first.update_columns(locked: true)
            end
            new_pack_cards << new_card
          end
        end
        Leap.where(pathway_id: parent_card.id).each do |leap|
          clone_leap(leap, new_cover_card.id, new_pack_cards)
        end
      end
    rescue Exception => e
      log.error "unable to clone parent pathway card #{parent_card.id}, error message #{e}"
    end
    new_cover_card
  end

  # clone journey with its associated section and cards
  def clone_journey_pack_relations(parent_card)
    card_ids = get_journey_pack_card_ids(parent_card)
    all_cards = Card.where(id: card_ids.flatten.uniq.compact)
    # return false if journey has private cards or project cards
    return false if journey_pack_unclonable?(all_cards)
    
    new_journey_cover = deep_clone_card(parent_card, parent_card.card_type)
    begin
      if (new_journey_cover.is_a?(Card) && new_journey_cover.save!)
        parent_card.journey_relns.each do |jpr|
          next if jpr.cover_id == jpr.from_id
          pack_card = all_cards.detect{ |card| card.id == jpr.from_id }
          new_pack_card = clone_card_pack_relations(pack_card)
          if new_pack_card.present?
            new_journey_cover.add_card_to_journey(new_pack_card.id)
          end
        end
      end
    rescue Exception => e
      log.error "unable to clone parent journey card #{parent_card.id}, error message #{e}"
    end
    new_journey_cover
  end

  def get_journey_pack_card_ids(parent_card)
    journey_pack_relations = JourneyPackRelation.journey_relations(cover_id: parent_card.id)
    card_ids = journey_pack_relations.map(&:from_id) 
    journey_pack_relations.each do |jpr|
      next if jpr.cover_id == jpr.from_id
      card_ids << CardPackRelation.pack_relations(cover_id: jpr.from_id).map(&:from_id)
    end
    card_ids
  end

  # clone leap of parent pathway if any
  def clone_leap(parent_leap, new_cover_id, new_pack_cards)
    begin
      Leap.create!(
        card_id: new_pack_cards.detect{|card| card.super_card_id == parent_leap.card_id }&.id,
        pathway_id: new_cover_id,
        correct_id: new_pack_cards.detect{|card| card.super_card_id == parent_leap.correct_id }&.id,
        wrong_id: new_pack_cards.detect{|card| card.super_card_id == parent_leap.wrong_id }&.id
      )
    rescue Exception => e
      log.error "unable to clone leap #{parent_leap.id} for new pathway_id #{new_cover_id}, error message #{e}"
    end
  end

  private

  # checks all section/cards of journey/pathway are public and not 'project' card_type
  def journey_pack_unclonable?(cards)
    cards.any? {|card| !card.is_public? || ['project'].include?(card.card_type)}
  end

  # returns clone of parent card with clone parent card associations
  def deep_clone_card(parent_card, card_type)
    return false if !parent_card.is_public?

    # return already cloned card if present in organization
    if (card = Card.where(super_card_id: parent_card.id, organization_id: @organization_id).first).present?
      return card
    end

    if ['journey', 'pack'].include? card_type
      new_card = parent_card.deep_clone(
        include: [:prices, :card_metadatum, :file_resources],
        except: [[Card::EXCLUDED_ATTRIBUTES_FROM_CLONNING], {file_resources: [:user_id]}],
        skip_missing_associations: true
      )
    else
      new_card = parent_card.deep_clone(
        include: [:prices, :card_metadatum, :file_resources, :resource, :quiz_question_options, {video_stream: :recordings}],
        except: [ 
          [Card::EXCLUDED_ATTRIBUTES_FROM_CLONNING],
          { resource: [:user_id], file_resources: [:user_id], video_stream: VideoStream::EXCLUDED_ATTRIBUTES_FROM_CLONNING }
        ],
        skip_missing_associations: true
      )
      new_card.video_stream.organization_id = @organization_id  if new_card.video_stream.present?

    end
    new_card.super_card_id = parent_card.id
    new_card.organization_id = @organization_id
    new_card.ecl_update_required = true
    new_card
  end
end
class DiscoveryService::CardBaseService
  attr_accessor :user, :organization, :limit, :offset, :options

  def intialize(user_id:, limit: 10, offset: 0, options: {})
    self.user    = User.find_by(id: user)
    self.organization = self.user.organization
    self.limit   = limit
    self.offset  = offset
    self.options = options
  end

  def exclude_cards_ids
    @exclude_cards_ids ||= (@user.completed_card_ids | @user.bookmarked_card_ids | user_cards_ids)
  end

  def user_cards_ids
    @user.cards.pluck('id')
  end

  def omit_private_content(cards)
    # to filter out inaccessible content only for non admin users
    unless (user.is_org_admin? || cards.length == 0)
      card_ids = cards.map(&:id)
      # to ensure that cards are returned in the original order of trendingness
      cards = Card.where(id: card_ids).visible_to_userV2(user)
        .order("field(cards.id, #{card_ids.join(',')})")   
    end 
    cards
  end
end

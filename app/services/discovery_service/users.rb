class DiscoveryService::Users

  attr_accessor :user, :organization, :limit, :offset

  def initialize(user_id, limit = 25, offset = 0, only_teams_users = false)
    @user         = User.find(user_id)
    @organization = @user.organization
    @limit        = limit
    @offset       = offset
    @only_teams_users  = only_teams_users
  end

  def recommendations
    if @only_teams_users
      org_active_users = fetch_non_following_team_users
    else
      org_active_users = fetch_non_following_org_users
    end

    #fetch only smes
    if @organization.only_sme_on_people_carousel?
      smes = org_active_users.sme_by_scores.group("users.id").limit(@limit).offset(@offset)
      return smes
    end

    #fetch smes by score
    role = @organization.roles.where(default_name: "sme").pluck(:name).first
    user_ids = @user.recommended_users(user_type: "expert", filter_params: {role: role }).map(&:id)
    @users = org_active_users.where(id: user_ids).group("users.id").limit(@limit).offset(@offset)

    #influencers_by_followers
    @users.push(*exclude_and_limit_users(org_active_users.influencers_by_followers))
    return @users if @users.length == @limit

    #smes by score
    @users.push(*exclude_and_limit_users(org_active_users.sme_by_scores))
    return @users if @users.length == @limit

    #users from my team
    @users.push(*exclude_and_limit_users(fetch_non_following_team_users))
    return @users if @users.length == @limit

    unless @only_teams_users
      #user from my channels
      channel_ids = @user.followed_channels.not_general.ids
      if channel_ids.any?
        @users.push(*fetch_my_channel_users(channel_ids))
        return @users if @users.length == @limit
      end

      #user type peer
      peeruser_ids = @user.recommended_users(user_type: "peer").map(&:id)
      @users.push(*exclude_and_limit_users(org_active_users.where(id: peeruser_ids)))
      return @users if @users.length == @limit
    end

    #following users
    following_expert_ids = @user.recommended_users(user_type: "expert", filter_params: {following: true, role: "sme"}).map(&:id)
    @users.push(*exclude_and_limit_users(fetch_following_users.where(id: following_expert_ids)))
    return @users if @users.length == @limit

    @users.push(*exclude_and_limit_users(fetch_following_users.following_influencers_by_followers))
    return @users if @users.length == @limit

    @users.push(*exclude_and_limit_users(fetch_following_users.sme_by_scores))
    return @users if @users.length == @limit

    @users.push(*exclude_and_limit_users(fetch_following_users))
  end

  def fetch_following_users
    @user.following.with_handle
  end

  def fetch_non_following_team_users
    team_ids = @user.teams.excluding_everyone_team.ids
    fetch_non_following_org_users.user_team_members(team_ids)
  end

  def fetch_non_following_org_users
    fetch_non_following_org_users =@organization.users.active(exclude_ids: [@user.id]).not_following_users(@user.id)
  end

  def fetch_my_channel_users(channel_ids)
    users = fetch_non_following_org_users
    channel_users = users.joins("INNER join follows as channel_follows on channel_follows.user_id = users.id
                                  AND channel_follows.followable_type = 'Channel'
                                  AND channel_follows.followable_id IN (#{channel_ids.join(",")})
                                  ")
    return exclude_and_limit_users(channel_users)
  end

  def exclude_and_limit_users(users)
    users.where.not(id: @users.map(&:id)).group("users.id").limit(@limit-@users.length).offset(@offset)
  end

end

class DiscoveryService::UserCardDiscovery < DiscoveryService::CardBaseService

  attr_accessor :user, :organization, :limit, :card_types, :channel_ids

  TRENDING_FROM = 36.hours.ago
  RANK_FACTOR = {views_count: 10, likes_count: 15, comments_count: 20, completes_count: 25, bookmarks_count: 30}

  def initialize(user_id: , limit: 25, offset: 0, duration: TRENDING_FROM,
                 card_types: [], channel_ids: [], filter_params: {})
    @user               = User.find(user_id)
    @organization       = @user.organization
    @user_language      = @user.language
    @filter_by_language = filter_params[:filter_by_language] || false
    @duration           = duration || TRENDING_FROM
    @limit              = limit || 25
    @offset             = offset || 0
    @card_types         = card_types || []
    @result_limit       = (@limit * (@offset+1))
    @result_offset      = 0
    @channel_ids        = channel_ids
  end

  # Find trending_organization_cards that matches with User's learning interest
  # if result is less than limit find trending_organization_cards that matches with User's expertise
  # if result is less than limit find cards added by SMEs.
  def trending_cards
    @card_types = %w(media poll course) if @card_types.blank?
    cards = cards_by_interest.results
    cards = (cards + cards_by_expertise.results).uniq if cards.flatten.length < @result_limit
    cards = (cards + sme_cards.results).uniq if cards.flatten.length < @result_limit
    cards = (cards + card_search_query.results).uniq if cards.flatten.length < @result_limit
    cards = omit_private_content(cards)
    cards[@offset, @limit]
  end

  def personalized_trending_pathway
    cards = cards_by_interest.results
    cards = (cards + cards_by_expertise.results).uniq if cards.flatten.length < @result_limit
    cards = (cards + promoted_cards.results).uniq if cards.flatten.length < @result_limit
    cards = (cards + sme_cards.results).uniq if cards.flatten.length < @result_limit
    cards = (cards + card_search_query.results).uniq if cards.flatten.length < @result_limit
    cards = omit_private_content(cards)
    cards[@offset, @limit]
  end
  alias :personalized_trending_journey :personalized_trending_pathway

  def trending_pathways
    trending_content(card_type: "pack", content_type: "collection")
  end

  def trending_journeys
    trending_content(card_type: "journey", content_type: "card")
  end

  def trending_content (card_type: , content_type: )
    initial_offset = PeriodOffsetCalculator.day_offset(@duration)
    final_offset = PeriodOffsetCalculator.day_offset(Time.now)
    cards = Card.joins("INNER JOIN content_level_metrics on cards.id = content_level_metrics.content_id")
              .select("cards.*,
                content_id,
                (sum(#{RANK_FACTOR[:views_count]}*content_level_metrics.views_count +
                  #{RANK_FACTOR[:likes_count]}*content_level_metrics.likes_count +
                  #{RANK_FACTOR[:comments_count]}*content_level_metrics.comments_count +
                  #{RANK_FACTOR[:completes_count]}*content_level_metrics.completes_count +
                  #{RANK_FACTOR[:bookmarks_count]}*content_level_metrics.bookmarks_count
                )) AS total_score")
              .where("content_level_metrics.period = 'day'AND content_level_metrics.content_type = :content_type AND content_level_metrics.organization_id = :organization_id", content_type: content_type, organization_id: @organization.id)
              .where("cards.card_type = :card_type AND cards.state = :state", card_type: card_type, state: 'published')
              .where('content_level_metrics.offset >= ? AND content_level_metrics.offset <= ?', initial_offset, final_offset)
    cards = cards.filtered_by_language(@user_language) if @filter_by_language
    cards = cards.group("content_level_metrics.content_type, content_level_metrics.content_id")
              .limit(@result_limit)
              .order ("total_score desc")
    omit_private_content(cards)
  end

  def interested_cards
    @card_types = ["pack", "journey", "course"]
    cards = cards_by_interest_and_channel.results
    cards = omit_private_content(cards)
    cards[@offset, @limit]
  end

  def relevent_interested_cards
    @card_types = @card_types.empty? ? Card::EXPOSED_TYPES_V2 : @card_types
    filter_params = {
      taxonomy_topics: @user.learning_topics_name,
      channel_ids: @channel_ids,
      exclude_ids: exclude_cards_ids | @user.dismissed_card_ids,
      relevent_cards: true
    }
    cards = card_search_query(filter_params).results
    cards = omit_private_content(cards)
  end

  # This will fetch most liked + viewed + commented + completed count of
  # cards in last trending_duration (default 30 days) within organization
  def trending_organization_cards
    initial_offset = PeriodOffsetCalculator.day_offset(@duration)
    final_offset = PeriodOffsetCalculator.day_offset(Time.now)

    results = ContentLevelMetric.
      select('content_id, (sum(views_count + comments_count + likes_count + completes_count)) AS totalScore').
      where(period: 'day', content_type: 'card', organization_id: @organization.id).
      where('offset >= ? AND offset <= ?', initial_offset, final_offset).
      group(:content_type, :content_id).
      order("totalScore desc").limit(@result_limit)
  end

  # This will fetch most liked + viewed + commented + completed count of
  # pathways in last trending_duration (default 30 hours) within organization
  def trending_organization_pathways
    initial_offset = PeriodOffsetCalculator.day_offset(@duration)
    final_offset = PeriodOffsetCalculator.day_offset(Time.now)

    ContentLevelMetric.
      select("
        content_id,
        (sum(#{RANK_FACTOR[:views_count]}*views_count +
          #{RANK_FACTOR[:likes_count]}*likes_count +
          #{RANK_FACTOR[:comments_count]}*comments_count +
          #{RANK_FACTOR[:completes_count]}*completes_count +
          #{RANK_FACTOR[:bookmarks_count]}*bookmarks_count
        )) AS total_score"
      )
      .where(period: 'day', content_type: "collection", organization_id: @organization.id)
      .where('offset >= ? AND offset <= ?', initial_offset, final_offset)
      .group(:content_type, :content_id)
      .order("total_score desc")
      .limit(@result_limit)
  end

  # This will fetch most liked + viewed + commented + completed count of
  # cards in last trending_duration (default 36 hours) within organization
  # which are not completed or bookmarked by user.
  def trending_cards_ids_for_user
    @trending_cards_ids_for_user ||= trending_organization_cards.map(&:content_id)
  end

  def cards_by_interest
    cards_by_topics(@user.learning_topics_name)
  end

  def cards_by_interest_and_channel
    filter_params = {
      taxonomy_topics: @user.learning_topics_name,
      channel_ids: @channel_ids
    }
    card_search_query(filter_params)
  end

  def cards_by_expertise
    cards_by_topics(@user.expert_topics_name)
  end

  def sme_cards
    filter_params = {
      exclude_ids: trending_cards_ids_for_user | exclude_cards_ids,
      author_id: @organization.sme_ids
    }
    card_search_query(filter_params)
  end

  def promoted_cards
    filter_params = {
      exclude_ids: trending_cards_ids_for_user | exclude_cards_ids,
      is_official: true
    }
    card_search_query(filter_params)
  end

  def cards_by_topics(taxonomy_topics=nil)
    filter_params = {
      taxonomy_topics: taxonomy_topics,
      ids: trending_cards_ids_for_user
    }
    card_search_query(filter_params)
  end

  def card_search_query(filter_params = {}, sort = nil)
    filter = {
      card_type: @card_types,
      state: 'published',
      exclude_ids: (filter_params[:exclude_ids] || []) | exclude_cards_ids
    }.merge(filter_params)

    filter = filter.merge({language: @user_language}) if @filter_by_language

    Search::CardSearch.new.search(
      nil,
      @organization,
      viewer: @user,
      filter_params: filter,
      load: true,
      offset: @result_offset,
      sort: sort,
      limit: @result_limit
    )
  end
end

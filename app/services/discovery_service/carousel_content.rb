class DiscoveryService::CarouselContent

  attr_accessor :user, :organization, :limit, :offset

  def initialize(user_id, uuid, limit=25, offset=0, filter_params: {})
    @user               = User.find_by(id: user_id)
    @organization       = @user.organization
    @user_language      = @user.language
    @filter_by_language = filter_params[:filter_by_language] || false
    @carousel           = get_carousel(uuid)
    @limit              = limit || 25
    @offset             = offset || 0
  end

  def get_carousel(uuid)
    org_customization_config = @organization.configs.find_by(name: "OrgCustomizationConfig")

    if org_customization_config.present?
      if org_customization_config.parsed_value["discover"] #if orgcustomization config has the key `discover`
        org_customization_config.parsed_value["discover"]["discover/carousel/#{uuid}"]
      else
        org_customization_config.parsed_value["web"]["discover"]["web/discover/carousel/#{uuid}"]
      end
    end
  end

  def get_content
    if @carousel && @carousel["visible"] && (cards_hash = @carousel["cards"])
      sorted_card_ids = cards_hash.sort_by{|k| k["index"]}.map{|h| h["card_id"]}
      if sorted_card_ids.present?
        @cards = @organization.cards.where(id: sorted_card_ids)
        @cards = @cards.filtered_by_language(@user_language) if @filter_by_language
        @cards = @cards.visible_to_userV2(@user) unless (@user.is_org_admin? || @cards.length == 0)
        @cards = @cards.order("field(cards.id, #{sorted_card_ids.join(',')})").limit(@limit).offset(@offset)
      end
      @cards
    end
  end
end

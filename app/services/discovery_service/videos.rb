class DiscoveryService::Videos < DiscoveryService::CardBaseService

  attr_accessor :user, :organization, :limit, :offset, :query

  def initialize(user_id, limit=25, offset=0, query=nil, filter_params: {})
    @user               = User.find(user_id)
    @organization       = @user.organization
    @user_language      = @user.language
    @filter_by_language = filter_params[:filter_by_language] || false
    @limit              = limit || 25
    @offset             = offset || 0
    @query              = query
    @result_limit       = (@limit * (@offset+1))
    @result_offset      = 0
  end

  # Find video_cards that matches with User's learning interest
  # if result is less than limit find video_cards that matches with User's experties
  # if result is less than limit find video_cards added by SMEs
  # if result is less than limit find video_cards added within organization.
  def video_cards
    cards = cards_by_interest.results
    cards = (cards + cards_by_experties.results).uniq if cards.flatten.length < @result_limit
    cards = (cards + promoted_video_cards.results).uniq if cards.flatten.length < @result_limit
    cards = (cards + sme_video_cards.results).uniq if cards.flatten.length < @result_limit
    cards = (cards + video_cards_search_query.results).uniq if cards.flatten.length < @result_limit
    cards = omit_private_content(cards)
    cards[@offset, @limit]
  end

  def cards_by_interest
    video_cards_by_topics(@user.learning_topics_name)
  end

  def cards_by_experties
    video_cards_by_topics(@user.expert_topics_name)
  end

  def video_cards_by_topics(taxonomy_topics=nil)
    filter_params = {
      taxonomy_topics: taxonomy_topics
    }
    video_cards_search_query(filter_params)
  end

  def sme_video_cards
    filter_params = {
      author_id: @organization.sme_ids
    }
    video_cards_search_query(filter_params)
  end

  def promoted_video_cards
    filter_params = {
      is_official: true
    }
    video_cards_search_query(filter_params)
  end

  # Video cards added within organization
  def video_cards_search_query(filter_params = {})
    filter = {
      state: 'published',
      exclude_ids: exclude_cards_ids,
      video_cards: true,
    }.merge(filter_params)

    filter = filter.merge({language: @user_language}) if @filter_by_language

    Search::CardSearch.new.search(
      @query,
      @organization,
      viewer: @user,
      filter_params: filter,
      offset: @result_offset,
      limit: @result_limit,
      load: true,
      sort:  { 'created_at' => { order: :desc } }
    )
  end
end

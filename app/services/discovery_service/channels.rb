# AIM:
#  Recommended channels for Personalized Discovery
# WORKFLOW:
#  1. Fetch #{limit} unfollowed channels matching user's learning-topics.
#  2. If channels are less than limit from #1, retrieve remaining unfollowed channels matching user's expert-topics.
#  3. If channels are less than limit from #2, retrieve promoted channels.
#  4. If channels are less than limit from #3, retrieve most followed channels.

module DiscoveryService
  class Channels
    attr_accessor :limit, :offset, :organization, :params, :user

    def initialize(params: {}, user:)
      @params       = params
      @limit        = params[:limit] || 25
      @offset       = params[:offset] || 0
      @user         = user
      @organization = user.organization
      @channels     = []
      @result_limit = (@limit * (@offset+1))
      @result_offset = 0
    end

    def all_channels
      @organization.channels
    end

    def discover
      @channels = channels_by_learning_topics
      @channels = (@channels + channels_by_expert_topics).uniq if @channels.length < @result_limit
      @channels = (@channels + promoted_channels).uniq if @channels.length < @result_limit
      @channels = (@channels + most_followed_channel).uniq if @channels.length < @result_limit
      @channels = (@channels + readable_channels).uniq if @channels.length < @result_limit

      @channels[@offset, @limit]
    end

    def most_followed_channel
      all_channels.
        joins(:follows).not_private.
        select('count(follows.user_id) as followers_count, channels.*').
        group('channels.id').
        order('followers_count desc').
        limit(@result_limit)
    end

    def readable_channels
      Channel.where(id: managed_channels)
    end

    def managed_channels
      @user.followed_channels.pluck(:id) | @user.authoring_channels.pluck(:id) | @channels.map(&:id) | @user.channels.pluck(:id)
    end

    private
      def channels_by_expert_topics
        expert_topics = user_expert_topics
        search_channels({by_topics: expert_topics})
      end

      def channels_by_learning_topics
        learning_topics = user_learning_topics
        search_channels({by_topics: learning_topics})
      end

      def promoted_channels
        search_channels({is_promoted: true})
      end

      def search_channels(filter_params = {})
        default_filters = {
          exclude_ids: []
        }

        Search::ChannelSearch.new.search(
          q:      nil,
          viewer: @user,
          filter: default_filters.merge(filter_params),
          limit:  @result_limit,
          offset: @result_offset,
          sort:   sort,
          load:   true).results
      end

      def sort
        [
          {
            key: :is_promoted,
            order: :desc
          },
          {
            key: :label,
            order: params[:order_label] || :asc
          }
        ]
      end

      def user_expert_topics
        @user.expert_topics_name
      end

      def user_learning_topics
        @user.learning_topics_name
      end
  end
end

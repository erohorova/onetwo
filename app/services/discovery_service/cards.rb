class DiscoveryService::Cards
  def query(search_term: '', card_types: nil, origins: nil, card_templates: nil, limit: 20, offset: 0, organization_id:)
    keywords = process_search_term(search_term)

    # Construct clauses
    where = {organization_id: organization_id}
    where[:card_type] = card_types if card_types.present?
    where['ecl.origin'] = origins if origins.present?
    where[:card_template] = card_templates if card_templates.present?

    aggs = [:card_type, 'ecl.origin', :card_template]
    options = {where: where, limit: limit, offset: offset, aggs: aggs, load: false}

    # Construct a dummy query, and then we'll replace it with our keywords
    searchkick_query = Searchkick::Query.new(Card, 'XYZZYSPOON', options)
    searchkick_query_body = searchkick_query.body

    # Get the list of queries
    if search_term == '*'
      searchkick_query_body[:query] = {match_all: {}}
    else
      queries_list = searchkick_query_body[:query][:dis_max][:queries]
      searchkick_query_body[:query][:dis_max][:queries] = keywords.flat_map{|x|
        get_queries_for_keyword(x, queries_list)
      }
    end

    # Remove fields: []
    searchkick_query_body.delete(:fields)

    searchkick_query.execute
  end

  private

  def process_search_term(term)
    term.split(' OR ')
  end

  def get_queries_for_keyword(keyword, queries_list)
    queries = queries_list.deep_dup
    queries.map {|x|
      x[:match]['_all'][:query] = keyword
    }

    queries
  end
end

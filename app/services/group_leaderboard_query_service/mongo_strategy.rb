# frozen_string_literal: true

class GroupLeaderboardQueryService
  class MongoStrategy < self

# =========================================
# IF ENABLING MONGO, UNCOMMENT FOLLOWING LINES

    # CacheKey = Analytics::GroupScoreAggregationRecorder::LeaderboardMongoKey
    # Ref = MONGODB_CLIENT[CacheKey]

# THIS FILE WON'T WORK OTHERWISE
# =========================================

    # Adds indexes to Mongo so queries happen faster
    # This is called from the after_party deploy task
    # add_indexes_to_mongo_for_group_leaderboard.
    # It doesn't really matter if it's called multiple times,
    # but there's no reason to.
    class Indexer < self
      def self.run!
        self::Ref.indexes.create_many([
          { key: { user_id: 1 } },
          { key: { group_id: 1 } },
          { key: { org_id: 1 } }
        ])
      end
    end

    def self.evaluate_query(cursor)
      cursor.to_a
    end

    def self.prepare_query(
      group_ids:, org_ids:, start_date:, end_date:, **opts
    )
      filters = [
        # filter_by_user(opts.fetch :user_ids, []),
        filter_by_group(group_ids),
        filter_by_org(org_ids),
        filter_by_date(start_date, end_date)
      ]
      paginate(filters.reduce(&:merge), opts)
    end

    def self.paginate(filters, opts)
      cursor = self::Ref.find(filters)
      with_limit(with_offset(cursor, opts), opts)
    end

    # Dates should be in ISO format, e.g. "2010-04-29T00:00:00.000Z"
    def self.filter_by_date(start_date, end_date)
      filter = {}
      return filter unless start_date || end_date
      filter.merge!("$gte" => start_date ) if start_date
      filter.merge!("$lte" => end_date) if end_date
      { time: filter }
    end

    # def self.filter_by_user(user_ids)
    #   return {} if user_ids.empty?
    #   { user_id: { "$in" => user_ids } }
    # end

    def self.filter_by_group(group_ids)
      return {} if group_ids.empty?
      { group_id: { "$in" => group_ids } }
    end

    def self.filter_by_org(org_ids)
      return {} if org_ids.empty?
      { org_id: { "$in" => org_ids } }
    end

    def self.with_limit(cursor, opts)
      # NOTE THAT MONGO IGNORES LIMIT IF SET TO 0
      limit = opts[:limit]
      unless limit.is_a?(Integer) && limit.between?(1, self::MAX_LIMIT)
        limit = self::DEFAULT_LIMIT
      end
      cursor.limit limit
    end

    def self.with_offset(cursor, opts)
      offset = opts[:offset]
      return cursor unless offset.is_a?(Integer) && offset > 0
      cursor.skip offset
    end

  end
end
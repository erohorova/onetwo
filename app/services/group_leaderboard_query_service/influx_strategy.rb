# frozen_string_literal: true

class GroupLeaderboardQueryService
  class InfluxStrategy < self

    Measurement = "group_user_scores_daily"

    def self.evaluate_query((query_str, query_params))
      InfluxQuery.fetch(
        query_str,
        query_params
      ).shift.try(:[], "values") || []
    end

    # Returns query object
    def self.prepare_query(
      start_date:, end_date:, group_ids:, org_ids:,
      limit:, offset:, **_opts
    )
      InfluxQuery.get_query_for_entity(
        entity: self::Measurement,
        organization_id: org_ids[0],
        filters: {
          start_date: start_date,
          end_date: end_date,
          limit: limit,
          offset: offset,
          group_ids: group_ids
        }
      )
    end

  end
end
class UserImport

  def initialize(user_import_id:, admin_id:, user_import_metadata: nil)
    @user_import_status = UserImportStatus.find_by(id: user_import_id)
    @file         = InvitationFile.find_by(id: @user_import_status.invitation_file_id)
    @organization = @file.invitable
    @admin        = @organization.users.find_by(id: admin_id) if admin_id
    @channels     = addable_channels(@user_import_status)
    @custom_fields = @organization.custom_fields
    initialize_user_metadata(user_import_metadata)
    @team_ids = []
  end

  def import
    user = addable_user(@user_import_status)

    if user
      assign_groups(@user_import_status.team_ids, user)
      set_custom_fields(user, @user_import_status.custom_fields)
      set_profile(
        user: user,
        profile: {
          language: @user_import_status.additional_information['language'],
          job_title: @user_import_status.additional_information['job_title']
        }
      )
      add_profile_picture(user: user, tuple: @user_import_status)

      if @channels.present?
        follow_channels(user)
        user_email = user.email

        if @admin.present? && user_email.present?
          if @organization.notifications_and_triggers_enabled?
            EDCAST_NOTIFY.trigger_event(Notify::EVENT_ADD_CHANNEL_FOLLOWER_ADMIN, @organization,
              {sender_id: @admin.id, channels: @channels.pluck(:label), followers_emails: [user_email]}
            )
          else
            UserMailer.channel_follows_notification_to_admin(@admin, @channels.pluck(:label), [user_email], [] ).deliver_now
          end
        end
      end
      # Clearing the cached cached count of followers count of channel that is part of given teams
      # making changes for this issue EP-21404
      log.info "Clearing channels followers count cache for teams_channels_follower of team_ids: #{@team_ids.flatten.uniq}"
      TeamsUser.clear_cached(@team_ids.flatten.uniq)

      # Check whether bulk import for all the users of this file is completed
      # If completed, then initiate Workflow processing
      if (@organization.simplified_workflow_enabled? || @organization.workflow_service_enabled?) && (@total_csv_users == @user_import_counter)
        uniq_user_import_status = @file.user_import_statuses.select(:status).distinct.map(&:status)
        if uniq_user_import_status.exclude?(UserImportStatus::PENDING)
          input_batch_size = @organization.workflow_input_batch_size
          @file.user_import_statuses
            .where(status: UserImportStatus::SUCCESS)
            .find_in_batches(batch_size: input_batch_size) do |user_import_status_batch|
              emails = user_import_status_batch.map(&:email)
              if @organization.simplified_workflow_enabled?
                UserBulkUploadWorkflowJob.perform_later(organization_id: @organization.id, user_metadata: {emails: emails})
              end
              if @organization.workflow_service_enabled?
                WorkflowPatchSaverJob.perform_later(data: emails, org_id: @organization.id, bulk_import: true)
              end
          end
        end
      end
    end
  end

  private

  def add_profile_picture(user:, tuple:)

    return if tuple.additional_information['picture_url'].blank?

    user.avatar = tuple.additional_information['picture_url']
    log.error "Could not save the user: #{tuple.email}" unless user.save
  end

  def addable_channels(tuple)
    @organization.channels.where(id: tuple.channel_ids || [])
  end


    def addable_user(tuple)
      user = @organization.users.find_by(email: tuple.email)
      # at this point we should already have proper default group - #addable_groups
      default_group = @organization.teams.find_by(name: tuple.additional_information['default_group']) if tuple.additional_information.present? && tuple.additional_information['default_group'].present?

      # If user exists with matching email, mark this user as `complete`.
      # Resend welcome email if Send Welcome Email is enabled while bulk importing user.
      if user
        user_params = {is_complete: true, first_name: tuple.first_name,
                    last_name: tuple.last_name, default_team_id: default_group&.id}
        # avoid nil, false or blank parameters
        filteredUserParams = user_params.select { |k,v| v.present? }
        user.update(filteredUserParams)

        tuple.update(status: UserImportStatus::SUCCESS, existing_user: true)
        if tuple.invitation_file.send_invite_email && resend_welcome_email?(user)
          federated_url = remote_provisioning(tuple, user)
          user.update_columns(invitation_accepted: false)
          DeviseMandrillMailer.send_import_user_welcome_email(user_id: user.id, redirect_url: federated_url).deliver_now
          tuple.update(resent_email: true)
        end

        return user
      end

      # Non-enterprise users can be added in here for some business reasons.
      # No need them to invite them, just create users and send welcome email
      # This email will logs them in.
      add_user(tuple: tuple, default_group: default_group)
    end

  def add_user(tuple:, default_group: nil)
    password = tuple.additional_information['password'].try(:strip)
    tuple.additional_information['password'] = password || User.random_password

    user = User.new(
      first_name:  tuple.first_name,
      last_name:   tuple.last_name,
      email:       tuple.email,
      password:    tuple.additional_information['password'],
      bulk_import: true,
      is_complete: true,
      password_reset_required: password.nil?,
      organization: @organization,
      organization_role: 'member',
      default_team_id: default_group&.id,
      invitation_accepted: false
    )
    user.upload_mode = "bulk_upload"
    user.skip_confirmation!
    user.skip_indexing = true #skip reindexing.
    user.save

    if user.persisted?
      tuple.update(status: UserImportStatus::SUCCESS, existing_user: false)
      federated_url = remote_provisioning(tuple, user)
      if tuple.invitation_file.send_invite_email
        DeviseMandrillMailer.send_import_user_welcome_email(user_id: user.id, redirect_url: federated_url).deliver_now
      end
      user
    else
      tuple.update(status: UserImportStatus::ERROR)
      nil
    end
  end

  def assign_groups(team_ids, user)
    groups = Team.where(id: team_ids)
    @team_ids << team_ids
    groups.each do |group|
      if group.users.include?(user)
        next
      end

      # The idea here is, if there is one more file import status record
      # exists for the same file, then skip group reindex.
      # only reindex for the last user creation for the same file.
      if should_skip_group_reindex?(group.id)
        group.skip_indexing #this will skip reindex
      end

      group.add_member(user, skip_flush_cache: true)
    end
  end

  def follow_channels(user)
    @channels.each { |channel| channel.add_followers([user]) }
  end

  def remote_provisioning(tuple, user)
    redirect_url = nil
    args = tuple.additional_information
    args.merge!(first_name: tuple.first_name, last_name: tuple.last_name, email: tuple.email)
    if @organization.okta_enabled?
      redirect_url = user.provision_and_sign_in(args: args, redirect_url: user.auth_landing_url)
    end

    redirect_url ||= user.auth_landing_url
  end

  def resend_welcome_email?(user)
    # There are plenty of users whose onboarding records isn't created. Such users must be considered for import.
    (user.user_onboarding.present? && !user.user_onboarding.completed?) || user.user_onboarding.blank?
  end

  def set_custom_fields(user, attributes)
    @custom_fields.wrap(user: user, attributes: attributes) if @custom_fields.present?
  end

  def set_profile(user:, profile:)
    UserProfile.set_profile(user: user, attributes: profile)
  end

  def should_skip_group_reindex?(group_id)
    UserImportStatus.exists?(
      ["id > ? AND invitation_file_id = ? and team_ids LIKE ?",
        @user_import_status.id, @user_import_status.invitation_file_id, group_id
      ]
    )
  end

  def initialize_user_metadata(user_import_metadata)
    if user_import_metadata
      @total_csv_users = user_import_metadata[:total_csv_users].to_i
      @user_import_counter = user_import_metadata[:user_import_counter].to_i
    end
  end
end

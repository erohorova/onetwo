class Integrations::CourseraService < Integrations::BaseIntegration
  #ac7e10e2a8cf5db6c0ecf3a5ea60ad87
  COURSERA_CONN = Faraday.new(url: Settings.features.integrations.coursera.host)
  def initialize(args = {})
    @external_uid = args[:external_uid]
    @user_id = args[:user_id]
  end

  #for coursera we have to get coursera_user_id then using that Id we will get courses
  def get_user_coursera_user_id
    result = COURSERA_CONN.get Settings.features.integrations.coursera.get_user_api, { "user-id" => @external_uid }
    if(result.status == 200)
      JSON.parse(result.body)['id'] #18450707
    else
      log.error "User not found with external_uid: #{@external_uid}"
      false
    end
  end

  def get_courses(coursera_user_id)
    result = COURSERA_CONN.get Settings.features.integrations.coursera.get_courses_api, { "user_id" => coursera_user_id }
    if(result.status == 200)
      JSON.parse(result.body)
    else
      log.warn "Unable to get courses with external_uid: #{@external_uid}"
    end
  end

  def source
    Integration.where(name: "Coursera").first
  end

  def create_courses(external_courses)
    begin
      if external_courses
        update_users_integration(@user_id, {coursera_public_page_id: @external_uid}.to_json)
        external_courses.each do |external_course|
          course_url = "https://www.coursera.org/course/#{external_course['short_name']}"
          status = (external_course["courses"][0]["status"] == 1) ? "enrolled" : "completed"
          course_params = { name: external_course["name"], description: external_course["short_description"],
            image_url: external_course["photo"], course_url: course_url, integration_id: source.id,
            course_key: external_course["short_name"]}
          create_course(course_params, status)
        end
      end
    rescue Exception => e
      log.warn "Error while getting html response from external source: #{source.name} for user with id: #{@user_id} with - #{e.message}"
      false
    end
  end

  def scrape
    coursera_user_id = get_user_coursera_user_id
    if coursera_user_id.present?
      courses = get_courses(coursera_user_id)
      create_courses(courses) if courses.present?
    end
  end
end
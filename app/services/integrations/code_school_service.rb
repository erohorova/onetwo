class Integrations::CodeSchoolService < Integrations::BaseIntegration
  def initialize(args = {})
    @external_uid = args[:external_uid]
    @user_id = args[:user_id]
  end

  def get_courses
    resp = Faraday.get(Settings.features.integrations.code_school.host + "users/" + @external_uid + ".json")
    JSON.parse(resp.body)["courses"]
  end

  def create_courses
    begin
      courses = get_courses
      if courses.length > 0
        update_users_integration(@user_id, {code_school_user_name: @external_uid}.to_json)

        courses["completed"].each do |course|
          create_course(course_params(course), "completed")
        end

        courses["in_progress"].each do |course|
          create_course(course_params(course), "enrolled")
        end
      end
    rescue Exception => e
      log.warn "Error while getting html response from external source: #{source.name} for user with id: #{@user_id} with - #{e.message}"
      false
    end
  end

  def course_params(course)
    params = Resource.get_resource_params(course["url"])
    { name: course["title"], description: params[:description], image_url: params[:image_url],
      course_url: course["url"], integration_id: source.id, course_key: course["url"] }
  end

  def source
    Integration.where(name: "Code School").first
  end

  def scrape
    create_courses if @external_uid.present?
  end
end
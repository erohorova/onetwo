class Integrations::UdemyService < Integrations::BaseIntegration
  UDEMY_CONN = Faraday.new(url: "https://www.udemy.com")
  def initialize(args = {})
    @external_uid = args[:external_uid]
    @user_id = args[:user_id]
  end

  #read user_id from public page
  def get_user_id
    begin
      url = "https://www.udemy.com/user/#{@external_uid}"
      doc = Nokogiri::HTML(open(url, ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE, 'User-Agent' => 'Opera'))
      JSON.parse(doc.css('[data-module-id="user-profile"]')[0].get_attribute("data-module-args"))["user"]["id"]
    rescue Exception => e
      log.warn "Integrations::UdemyService: Error while scraping html to get udemy user id with id: #{@user_id} with - #{e.message}"
      false
    end
  end

  #get courses using udemy user_id
  def get_courses(udemy_user_id)
    url = "https://www.udemy.com/api-2.0/users/#{udemy_user_id}/subscribed-profile-courses/"
    result = UDEMY_CONN.get url
    if(result.status == 200)
      JSON.parse(result.body)
    else
      log.warn "Unable to get courses with external_uid: #{@external_uid}"
      false
    end
  end

  def create_courses(external_courses)
    begin
      if external_courses
        external_courses.each do |external_course|
          course_url = "https://www.udemy.com#{external_course['url']}"
          status = "enrolled"
          course_params = { name: external_course["title"], description: external_course["visible_instructors"] && external_course["visible_instructors"][0]["title"],
            image_url: external_course["image_240x135"], course_url: course_url, integration_id: source.id,
            course_key: external_course["url"]}
          if(external_course["visible_instructors"].present?)
            details = external_course["visible_instructors"][0]
            course_params.merge!(description: details["title"])
          end
          create_course(course_params, status)
        end
      end
    rescue Exception => e
      log.warn "Error while getting html response from external source: #{source.name} for user with id: #{@user_id} with - #{e.message}"
      false
    end
  end

  def source
    Integration.where(name: "Udemy").first
  end

  def scrape
    udemy_user_id = get_user_id
    if udemy_user_id.present?
      update_users_integration(@user_id, {udemy_public_page_id: @external_uid}.to_json)
      courses = get_courses(udemy_user_id)
      create_courses(courses["results"]) if courses.present?
    end
  end
end
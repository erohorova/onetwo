class Integrations::AlisonService < Integrations::BaseIntegration
  def initialize(args = {})
    @external_uid = args[:external_uid]
    @user_id = args[:user_id]
  end

  def get_courses_in_html
    Nokogiri::HTML(open(Settings.features.integrations.alison.host + "profile/public/"+ @external_uid + "/"))
  end

  def create_courses
    begin
      courses = get_courses_in_html
      if courses
        update_users_integration(@user_id, {alison_user_id: @external_uid}.to_json)
        courses.css(".col-sm-4 .education-title").each do |course_row|
          course_url = course_row.get_attribute("href")
          params = Resource.get_resource_params(course_url)
          if params.present?
            course_key = course_url
            integration_id = source.id
            course_params = { name: params[:title], description: params[:description],
              image_url: params[:image_url], course_url: course_url, integration_id: source.id,
              course_key: course_key }
            create_course(course_params, "completed")
          end
        end
      end
    rescue Exception => e
      log.warn "Error while getting html response from external source :#{source.name} for user with id: #{@user_id} with - #{e.message}"
      false
    end
  end

  def source
    Integration.where(name: "Alison").first
  end

  def scrape
    create_courses if @external_uid.present?
  end
end
#this service will pull skills from code academy
class Integrations::CodecademyService < Integrations::BaseIntegration
  def initialize(args = {})
    @external_uid = args[:external_uid]
    @user_id = args[:user_id]
  end

  def get_skills_in_html
    Nokogiri::HTML(open(Settings.features.integrations.codecademy.host + @external_uid))
  end

  def create_skills
    begin
      skills = get_skills_in_html
      if skills
        update_users_integration(@user_id, {codecademy_username: @external_uid}.to_json)
        skills.css('#completed-body .completed .link--target').each do |skill|
          course_url = skill.get_attribute("href")
          params = Resource.get_resource_params(course_url)
          if params.present?
            course_key = course_url
            integration_id = source.id
            course_params = { name: params[:title], description: params[:description],
              image_url: params[:image_url], course_url: course_url, integration_id: source.id,
              course_key: course_key }
            create_course(course_params, "completed")
          end
        end
      end
    rescue Exception => e
      log.warn "Error while getting html response from external source :#{source.name} for user with id: #{@user_id} with - #{e.message}"
      false
    end
  end

  def source
    Integration.where(name: "Codecademy").first
  end

  def scrape
    create_skills if @external_uid.present?
  end
end
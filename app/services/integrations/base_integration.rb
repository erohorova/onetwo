class Integrations::BaseIntegration

  attr_accessor :course, :user_course

  def create_course(params, status, user_course_params={})
    begin
      @course = create_external_course(params)
      @user_course = get_user_external_course(params)
      update_user_external_course(status, user_course_params)
    rescue Exception => e
      log.error "Error while creating courses from external sources for user with id: #{@user_id} with - #{e.message}"
    end
  end

  def update_users_integration(user_id, field_values)
    begin
      user_integration = UsersIntegration.where(user_id: user_id, integration_id: source.id).first_or_initialize
      user_integration.field_values = field_values
      user_integration.connected = true
      user_integration.save!
    rescue Exception => e
      log.warn "Error while trying to update the integration for the user with id: #{user_id} with - #{e.message}"
    end
  end

  def create_external_course(params)
    ExternalCourse.where(integration_id: source.id, course_key: params[:course_key]).first || ExternalCourse.create(params)
  end

  def get_user_external_course(params)
    UsersExternalCourse.where(user_id: @user_id, external_course_id: @course.id).first_or_initialize
  end

  def update_user_external_course(status, user_course_params)
    @user_course.update(user_course_params.merge({ status: status }))
  end
end

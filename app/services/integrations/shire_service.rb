# COURSE STRUCTURE:
#
# {
#   "discontinued-from": "",
#   "certification-status": "INEFFECT",
#   "grace-period": 0,
#   "target-days": 13,
#   "available-from": "05/20/2016",
#   "parent-certification-id": "",
#   "allow-weights": 0,
#   "allow-self-registration": true,
#   "display-to-learner": true,
#   "version": "1.0",
#   "domain": {
#       "id": "domin000000000001021",
#       "name": "Shire"
#   },
#   "paths": [
#       {
#           "name": "TO SOP-0440",
#           "id": "track000000000013301"
#       }
#   ],
#   "description": "",
#   "name": "TO SOP-0440 Policy on Reporting Product Quality Complaints and Safety Information During Shire/Baxalta Integration",
#   "id": "crtfy000000000012491",
#   "customs": [
#       {
#           "name": "custom0",
#           "value": "Compliance",
#           "type": "18"
#       },
#       {
#           "name": "custom1",
#           "value": "Corporate",
#           "type": "18"
#       }
#   ]
# }

class Integrations::ShireService < Integrations::BaseIntegration

  attr_accessor :user_id, :certificate, :employee_username, :cookie

  LOGIN_URL = '/Saba/api/platform/authentication/login/'

  def initialize(args = {})
    @user_id = args[:user_id]
    get_employee_username
    get_certificate if @employee_username.present?
  end

  def get_employee_username
    user = User.find_by(id: @user_id)
    provider = user.saml_providers.first if user.present?
    @employee_username = provider.auth_info['user_id'][0] if provider.present?
  end

  def get_certificate
    response = establish_connection.get do |f|
      f.url LOGIN_URL
      f.headers = login_headers
    end

    if response.success?
      body = ActiveSupport::JSON.decode(response.body)
      @certificate = body['SabaCertificateWrapper']['certificate']
      @cookie = response.headers["set-cookie"]
    end
  end

  def scrape
    courses = get_courses || []
    courses.each do |course|
      course_details = get_course_detail(course['id'])
      create_course(course_params(course_details), course_details['certification-status'], user_course_details(course_details))
    end
  end

  def get_courses
    response = establish_connection.get do |f|
      f.url '/Saba/api/person/profile/'
      f.headers = headers
      f.params = { 'EmpUserName' => @employee_username }
    end

    if response.success?
      body = ActiveSupport::JSON.decode(response.body)
      body[0]['certifications']
    end
  end

  def get_course_detail(id)
    response = establish_connection.get do |f|
      f.url "/Saba/api/certification/#{id}"
      f.headers = headers
    end

    if response.success?
      ActiveSupport::JSON.decode(response.body)
    end
  end

  private

  def user_course_details(course)
    assigned_date = course['available-from'].present? ? Date.strptime(course['available-from'], "%m/%d/%Y") : nil
    due_date = (assigned_date.present? || course['target-days'].present?) ? assigned_date + course['target-days'].days : nil

    {
      raw_info: course,
      assigned_date: assigned_date,
      due_date: due_date
    }
  end

  def shire_api_base_url
    Settings.shire.api_endpoint || 'https://shiretrg.sabahosted.com'
  end

  def login_headers
    {
      'cache-control' => 'no-cache',
      'site'          => 'SabaSite',
      'Content-Type'  => 'application/json',
      'password'      => Settings.shire.password,
      'user'          => Settings.shire.username
    }
  end

  def headers
    {
      "cache-control" => "no-cache",
      "certificate"   => @certificate,
      "Content-Type"  => "application/json",
      "Cookie"        => @cookie
    }
  end

  def establish_connection
    Faraday.new(url: shire_api_base_url)
  end

  def course_url(id)
    "#{shire_api_base_url}/Saba/Web/Cloud/goto/CertificationDetailDeeplinkURL?certificationId=#{id}&baseType=0&pageMode=GuestLogin"
  end

  def course_params(course)
    {
      name:           course['name'],
      description:    course['description'],
      course_url:     course_url(course['id']),
      integration_id: source.id,
      course_key:     course['id']
    }
  end

  def source
    Integration.find_by(name: "Shire")
  end

end

class Integrations::SkillShareService < Integrations::BaseIntegration
  def initialize(args = {})
    @external_uid = args[:external_uid]
    @user_id = args[:user_id]
  end

  def get_courses_in_html
    Nokogiri::HTML(open(Settings.features.integrations.skill_share.host + @external_uid + "/enrolled"))
  end

  def create_courses

    begin
      courses = get_courses_in_html
      if courses
        update_users_integration(@user_id, {skill_share_user_name: @external_uid}.to_json)

        courses.css('.enrolled-section li .class-row-inner-wrapper').each do |course_div|
          image_url = course_div.search('.background-image-holder').map{ |n| n['style'][/url\((.+)\)/, 1] }[0]
          title_div = course_div.search('.class-info .title-link a')
          title = title_div.text
          course_url = title_div[0].get_attribute("href")
          description = "By: " + course_div.search(".user-information .title").text.squish

          course_params = { name: title, description: description, image_url: image_url, course_url: course_url,
            integration_id: source.id, course_key: course_url }
          create_course(course_params, "enrolled")
        end
      end
    rescue Exception => e
      log.warn "Error while getting html response from external source: #{source.name} for user with id: #{@user_id} with - #{e.message}"
      false
    end
  end

  def source
    Integration.where(name: "Skill Share").first
  end

  def scrape
    create_courses if @external_uid.present?
  end
end
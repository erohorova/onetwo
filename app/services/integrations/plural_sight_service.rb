class Integrations::PluralSightService < Integrations::BaseIntegration
  def initialize(args = {})
    @external_uid = args[:external_uid]
    @user_id = args[:user_id]
  end

  def get_courses
    #here /1 is hardcoded, its required any random number
    resp = Faraday.get(Settings.features.integrations.plural_sight.host + "id/data/activity/"+ @external_uid + "/1")
    courses = JSON.parse(resp.body)["CurrentlyLearning"]
  end

  def create_courses
    begin
      courses = get_courses
      if courses.length > 0
        update_users_integration(@user_id, {plural_sight_user_name: @external_uid}.to_json)

        courses.each do |course|
          href = course["Course"]["Id"]
          course_url = Settings.features.integrations.plural_sight.host + "library/courses/" + href
          author = course["Course"]["Authors"][0]
          description = "Taught by: #{author['FirstName']} #{author['LastName']}"
          image_url = "https://s3.amazonaws.com/profile-integration-prod/default_pluralsight.png"
          course_params = { name: course["Course"]["Title"], description: description,
              image_url: image_url, course_url: course_url, integration_id: source.id,
              course_key: href }
          status = (course["Progress"]["PercentComplete"] == 100) ? "completed" : "enrolled"
          create_course(course_params, status)
        end
      end
    rescue Exception => e
      log.warn "Error while getting html response from external source: #{source.name} for user with id: #{@user_id} with - #{e.message}"
      false
    end
  end

  def source
    Integration.where(name: "Plural Sight").first
  end

  def scrape
    create_courses if @external_uid.present?
  end
end
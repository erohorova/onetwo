class Integrations::FetchCourses

  attr_accessor :fields, :user, :provider, :external_uid

  def initialize(user_id:)
    @user = User.find_by(id: user_id)
  end

  def scrape
    return if @user.blank?

    users_integrations = @user.users_integrations

    users_integrations.each do |user_integration|

      fields = user_integration.field_values
      provider = user_integration.integration.name

      if fields && fields.all? { |k,v| v.present? }

        external_uid =  case provider
          when "Coursera"
            fields[:coursera_public_page_id]
          when "Udemy"
            fields[:udemy_public_page_id]
          when "Future Learn"
            fields[:future_learn_profile_id]
          when "Skill Share"
            fields[:skill_share_user_name]
          when "Plural Sight"
            fields[:plural_sight_user_name]
          when "Tree House"
            fields[:tree_house_user_name]
          when "Code School"
            fields[:code_school_user_name]
          when "Alison"
            fields[:alison_user_id]
          when "Codecademy"
            fields[:codecademy_username]
          end
      end

      CoursesScraperJob.perform_later(@user.id, external_uid, provider)
    end
  end

end

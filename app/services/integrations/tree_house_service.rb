class Integrations::TreeHouseService < Integrations::BaseIntegration
  def initialize(args = {})
    @external_uid = args[:external_uid]
    @user_id = args[:user_id]
  end

  def get_courses
    resp = Faraday.get(Settings.features.integrations.tree_house.host + @external_uid + ".json")
    JSON.parse(resp.body)["badges"]
  end

  def create_courses
    begin
      subjects = get_courses
      if subjects.length > 0
        update_users_integration(@user_id, {tree_house_user_name: @external_uid}.to_json)

        #subject[0] contains basic newbie info that is not valid course
        subjects[1..-1].each do |subject|
          image_url = subject["icon_url"]
          subject["courses"].each do |course|
            title = course["title"]
            course_url = course["url"]
            course_params = { name: title, description: "", image_url: image_url,
              course_url: course_url, integration_id: source.id, course_key: course_url }
            create_course(course_params, "completed")
          end
        end
      end
    rescue Exception => e
      log.warn "Error while getting html response from external source: #{source.name} for user with id: #{@user_id} with - #{e.message}"
      false
    end
  end

  def source
    Integration.where(name: "Tree House").first
  end

  def scrape
    create_courses if @external_uid.present?
  end
end
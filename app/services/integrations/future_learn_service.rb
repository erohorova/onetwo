class Integrations::FutureLearnService < Integrations::BaseIntegration
  def initialize(args = {})
    @external_uid = args[:external_uid]
    @user_id = args[:user_id]
  end

  def get_courses_in_html
    Nokogiri::HTML(open(Settings.features.integrations.future_learn.host + "/profiles/"+ @external_uid))
  end

  def create_courses
    begin
      courses = get_courses_in_html
      if courses
        update_users_integration(@user_id, {future_learn_profile_id: @external_uid}.to_json)

        courses.css('a.o-profile-courses__run-title').each do |course|
          href = course.get_attribute('href')
          course_url = Settings.features.integrations.future_learn.host + href
          params = Resource.get_resource_params(course_url)
          if params.present?
            course_key = href.split("/courses/")[1]
            integration_id = source.id
            course_params = { name: params[:title], description: params[:description],
              image_url: params[:image_url], course_url: course_url, integration_id: source.id,
              course_key: course_key }
            create_course(course_params, "enrolled")
          end
        end
      end
    rescue Exception => e
      log.warn "Error while getting html response from external source :#{source.name} for user with id: #{@user_id} with - #{e.message}"
      false
    end
  end

  def source
    Integration.where(name: "Future Learn").first
  end

  def scrape
    create_courses if @external_uid.present?
  end
end
require 'csv'

class BulkImportEmbargo
  WHITELISTED_HEADERS = %w[value category].freeze

  def initialize(file_id:)
    @file = EmbargoFile.find_by(id: file_id)
    @admin = @file.user
    @organization = @file.organization
    @headers = WHITELISTED_HEADERS
  end

  def call
    records = []
    rows.each do |row|
      tuple = row.to_hash
      details = tuple.slice(*@headers)
      next if invalid_data(details)
      records << build_new_record(tuple)
    end
    import_records(records)

    email_bulk_upload_details
  end

  def preview_csv(display_row_count)
    embargoes = []
    count = 0
    rows.each do |row|
      tuple = row.to_hash
      details = tuple.slice(*@headers)
      next if invalid_data(details)
      embargoes << details

      break if embargoes.length >= display_row_count # display first n rows
    end

    @rows.each do |row|
      count += 1 unless invalid_data(row.to_hash.slice(*@headers))
    end
    [embargoes.compact, count]
  end

  def paginate_csv_data(range)
    data = CSV.parse(
      @file.data, headers: true,
      header_converters: lambda { |h| h.underscorize }
    )
    valid_data = data.map do |row|
      valid_row = row.to_hash.slice(*@headers)
      valid_row unless invalid_data(valid_row)
    end
    valid_data.compact!
    rows = valid_data[range] || []

    [rows, valid_data.count]
  end

  class << self
    def csv_headers_validation(csv_data)
      csv_rows = CSV.parse(csv_data)
      return 'Invalid or empty csv' if csv_rows.length <= 1
      header = csv_rows.first

      # HEADERS Check, should contain "Value, Category"
      valid_headers = (header.first(2).map(&:downcase) == ['value', 'category'])
      'Invalid CSV headers. Use format value, category' unless valid_headers
    end
  end

  private


  def rows
    @rows ||= CSV.parse(@file.data, headers: true, header_converters: lambda{ |h| h.underscorize })
  end

  def build_new_record(row)
    row = row.merge({organization_id: @organization.id, user_id: @admin.id})
    Embargo.new(row)
  end

  def import_records(records)
    Embargo.import(records)
  end

  def email_bulk_upload_details
    parameters = { admin: @admin, upload_time: @file.created_at}
    BulkImportEmbargoDetailsMailer.notify_admin(parameters).deliver_now
  end

  def valid_url?(url_string)
    valid_url_regex = %r{((http|https):\/{2})?(^[*]\.)?[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9]\.[^\s^<^>]{1,}([a-zA-Z0-9])}x
    (url_string =~ valid_url_regex) == 0
  end

  def invalid_data(details)
    details['category'].in?(Embargo::SITES) && !valid_url?(details['value']) ||
        Embargo::VALID_TYPES.exclude?(details['category'])
  end
end

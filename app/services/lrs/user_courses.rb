module Lrs
  class UserCourses < Base

    # Output:
    # {
    #   :success=>true,
    #   :data=>[
    #     {"_id"=>"123", "course"=>"http://example.co/insights/brainee", "status"=>"completed"},
    #     {"_id"=>"122", "course"=>"http://example.co/insights/brainee1", "status"=>"enrolled"},
    #     {"_id"=>"121", "course"=>"http://example.co/insights/brainee2", "status"=>"cretaed"}
    #   ]
    # }
    def get_courses_status(user)
      json_request(user_courses_pipline(user.email), :get)
    end

    # Output:
    # {
    #   :success=>true,
    #   :data=>[
    #     {"_id"=>"123", "course"=>"http://example.co/insights/brainee", "status"=>"completed"},
    #     {"_id"=>"122", "course"=>"http://example.co/insights/brainee1", "status"=>"completed"},
    #     {"_id"=>"121", "course"=>"http://example.co/insights/brainee2", "status"=>"completed"}
    #   ]
    # }
    def get_completed_courses(user)
      json_request(user_completed_courses_pipline(user.email), :get)
    end

    private

    def user_courses_pipline(email)
      BASE_AGGREGATE_API_URL + '
      [
        {"$match": {
            "statement.actor.mbox": "mailto:'+email+'"
          }
        },
        {"$project": {
            "course": "$statement.object.id",
            "verb": "$statement.verb.id",
            "status": "$statement.verb.display.en-US"
          }
        }
      ]'
    end

    def user_completed_courses_pipline(email)
      BASE_AGGREGATE_API_URL + '
      [
        {"$match": {
            "statement.actor.mbox": "mailto:'+email+'",
            "statement.verb.id": "http://adlnet.gov/expapi/verbs/completed"
          }
        },
        {"$project": {
            "course": "$statement.object.id",
            "status": "$statement.verb.display.en-US"
          }
        }
      ]'
    end
  end
end



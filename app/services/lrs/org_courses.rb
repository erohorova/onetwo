module Lrs
  class OrgCourses < Base

    # Output:
    # {
    #   :success=>true,
    #   :data=>[
    #     {"_id"=>"123", "id" => "123", "url" => "course url", "course"=>"http://example.co/insights/brainee", "status"=>"completed"},
    #     {"_id"=>"122", "id" => "123", "url" => "course url", "course"=>"http://example.co/insights/brainee1", "status"=>"enrolled"},
    #     {"_id"=>"121", "id" => "123", "url" => "course url", "course"=>"http://example.co/insights/brainee2", "status"=>"cretaed"}
    #   ]
    # }
    def get_courses
      json_request(courses_pipline, :get)
    end

    # Output:
    # {
    #   :success=>true,
    #   :data=>[
    #     {"_id"=>"123", "id" => "123", "url" => "course url", "course"=>"http://example.co/insights/brainee", "status"=>"completed"},
    #     {"_id"=>"122", "id" => "123", "url" => "course url", "course"=>"http://example.co/insights/brainee1", "status"=>"completed"},
    #     {"_id"=>"121", "id" => "123", "url" => "course url", "course"=>"http://example.co/insights/brainee2", "status"=>"completed"}
    #   ]
    # }
    def get_completed_courses
      json_request(completed_courses_pipline, :get)
    end

    def run
      lrs_courses = get_courses[:data]
      lrs_courses = lrs_courses.select{|a| a["id"].present? || a["url"].present?}

      lrs_courses.each do |course|
        email = course["actor"].gsub("mailto:", "")
        user = @organization.users.find_by(email: email)
        if user.present?
          create_mlp_assignment(user, course)
        end
      end
    end

    def create_mlp_assignment(user, course)
      card = get_card(user, course)
      if card.present?
        status = status(course)

        resp = LrsAssignmentService.new( user_id: user.id,
                                  card: card,
                                  status: status).run

        unless resp.present?
          log.info "Could not create assignment for user with id #{user.id}"
        end
      else
        log.info "Could not find ecl card with course details: #{course} with user email: #{user.email}"
      end
    end

    def status(course)
      if COMELETED_STATE.include?(course["status"])
        "completed"
      elsif STARTED_STATE.include?(course["verb"])
        "started"
      else
        "assigned"
      end
    end

    def courses_pipline
      BASE_AGGREGATE_API_URL + '
      [ '+match_query(from: @last_run_at)+',
        {"$project": {
            "actor": "$statement.actor.mbox",
            "url": "$statement.object.id",
            "name": "$statement.object.defination.name.en-US",
            "descritpion": "$statement.object.defination.descritpion.en-US",
            "verb": "$statement.verb.id",
            "status": "$statement.verb.display.en-US",
            "id": "$statement.context.registration"
          }
        }
      ]'
    end

    def completed_courses_pipline
      BASE_AGGREGATE_API_URL + '
      [
        {"$project": {
            "url": "$statement.object.id",
            "name": "$statement.object.defination.name.en-US",
            "descritpion": "$statement.object.defination.descritpion.en-US",
            "verb": "$statement.verb.id",
            "status": "$statement.verb.display.en-US",
            "id": "$statement.context.registration"
          }
        }
      ]'
    end
  end
end


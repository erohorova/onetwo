module Lrs
  class Base
    BASE_AGGREGATE_API_URL = "/api/statements/aggregate?cache=false&maxTimeMS=5000&maxScan=100&pipeline="

    COMELETED_STATE = %w[completed approved closed passed].freeze
    STARTED_STATE = %w[started answered attempted assigned progressed consumed joined].freeze

    def initialize(org_id: organization_id, last_run_at: nil)
      @organization = Organization.find(org_id)
      @lrs_credential = XapiCredential.find_by(organization_id: org_id)
      @last_run_at = last_run_at || (Time.now - 2.days) #if last_run_at not present then run for last 2 days
    end

    def json_request(url, method, &block)
      if @lrs_credential
        conn = Faraday.new(Settings.lrs_host)
        conn.basic_auth @lrs_credential.lrs_login_key, @lrs_credential.lrs_password_key
        has_block = block_given?

        result = conn.send(method) do |req|
          req.url url
          req.headers['Content-Type'] = 'application/json'
          req.headers['Accept'] = 'application/json'
          yield req if has_block
        end

        data = nil
        unless result.success?
          log.error "Request to LRS failed with status: #{result.status} and body: #{result.body}"
          data = []
        end

        {success: result.success?, data: data || (ActiveSupport::JSON.decode(result.body))}
      end
    end

    # {
    #    "$match":  {
    #      "statement.actor.mbox" :  "mailto:sunil@edcast.com"
    #      "timestamp": {
    #       "$gte": {
    #         "$dte": "2018-01-19T07:38:36.556Z"
    #       },
    #       "$lt": {
    #          "$dte": "'+to.utc.iso8601+'"
    #       }
    #     }
    #    }
    #  }

    def match_query(email: nil, from: Time.parse("01-01-2017"), to: Time.now)
      email_query = '"statement.actor.mbox": "mailto:'+email+'"' if email.present?
      query = '"timestamp": {
                "$gte": {
                  "$dte": "'+from.utc.iso8601+'"
                },
                "$lt": {
                  "$dte": "'+to.utc.iso8601+'"
                }
              }'

      query = (email_query +','+ query) if email_query.present?

      '{ "$match": {'+query+'} }'
    end

    def get_card(user, course)
      url = course["url"]&.split('?')&.first
      Card.find_by_resource_url(url, user.organization_id)
    end
  end
end

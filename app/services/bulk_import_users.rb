# coding: utf-8
# frozen_string_literal: true
# WORKFLOW:
#   SSO users are identified if `@options[:invitable]` is set to TRUE.
#   When `@options[:invitable]` is TRUE:
#     1. Find if user exists with an email.
#     2. If #1 returns true, then attach groups, channels, custom_fields to this user.
#     3. If #1 returns FALSE:
#        a. Find pending invitation with matching email.
#        b. If #3.a returns false, then create an invitation for this email.
#        c. Attach groups, channels, custom_fields to this invitation.
#        d. Enabling Invitation#authenticate_recipient asks enterprise user to sign in.
#   When `@options[:invitable]` is FALSE:
#     1. Find if user exists with an email.
#     2. If #1 returns true, then attach groups, channels, custom_fields to this user.
#     3. If #1 returns false, then create user and attach groups, channels, custom_fields to this invitation.

# FEATURES:
#   1. Groups (creates new groups if matching group name is not found)
#   2. Channels (links channels to users)
#   3. Custom fields (create/update custom fields)
#   4. Password
#   5. Picture Url
#   6. Admin receives an email with import status:
#      a. Count of total users imported.
#      b. Count of new users created.
#      c. Count of old users considered.
#      d. Count of users failed to be imported.
#      e. Count of total invitations created.
#      f. List of all channels considered in this import.
#      g. Count of new groups created.
#      h. Count of old groups considered.
#      i. Count of emails resent to existing users.

require 'csv'

class BulkImportUsers
  # MANDATORY: Sequence of attributes
  WHITELISTED_ATTRS = %w(first_name last_name email groups picture_url password language job_title)

  def initialize(file_id:, admin_id: nil, options: {})
    @file         = InvitationFile.find_by(id: file_id)
    @organization = @file.invitable
    @options      = options
    @admin        = @organization.users.find_by(id: admin_id) if admin_id

    prepare_attributes
    initialize_status
    @team_ids = []
    @persisted_user_ids = []
  end

  def prepare_attributes
    @channels      = addable_channels
    @default_teams = teams_names(ids: @options[:team_ids])

    @custom_fields = @organization.custom_fields
    @abbreviations = @custom_fields.pluck(:abbreviation)
    @headers       = WHITELISTED_ATTRS + @abbreviations
  end

  def initialize_status
    @count = {
      failed_users_count:  0,
      invitees_count:      0,
      new_users_count:     0,
      old_users_count:     0,
      resent_emails_count: 0,
      total_users_count:   0
    }

    @channels_names_list = nil

    @new_groups = {}
    @old_groups = {}
  end

  def preview_csv
    users = []

    csv_rows = CSV.parse(@file.data, headers: true, header_converters: lambda{ |h| h.underscorize })
    csv_rows.each do |row|
      tuple   = row.to_hash
      details = tuple.slice(*@headers)

      next if !valid_email?(details['email'])
      users << details

      break if users.length >= 10 # display first 10 rows
    end
    users
  end

  def import
    users = []

    CSV.parse(@file.data, headers: true, header_converters: ->(h) { h.underscorize }) do |row|
      tuple = row.to_hash

      next unless valid_email?(tuple['email'].try(:strip))

      user_groups = addable_groups(tuple)
      @team_ids << user_groups.values.map(&:id)
      user = addable_user(tuple: tuple, user_groups: user_groups)

      # Add invited users to the list but still ignore the flow further.
      users << user

      next if user.blank?

      set_attributes(user: user, tuple: tuple, groups: user_groups, custom_fields: tuple.slice(*@abbreviations))
    end

    @count[:total_users_count] = users.length
    if @organization.simplified_workflow_enabled?
      input_batch_size = @organization.workflow_input_batch_size
      @persisted_user_ids.each_slice(input_batch_size) do |user_ids|
        UserBulkUploadWorkflowJob.perform_later(organization_id: @organization.id, user_metadata: {ids: user_ids})
      end
    end

    if @channels.present?
      follow_channels(users.compact)
      user_emails = users.compact.map{|u| u.email}

      if @admin.present? && user_emails.present?
        if @organization.notifications_and_triggers_enabled?
          EDCAST_NOTIFY.trigger_event(Notify::EVENT_ADD_CHANNEL_FOLLOWER_ADMIN, @organization,
            {sender_id: @admin.id, channels: @channels.pluck(:label), followers_emails: user_emails.first(ChannelsUser::BULK_USER_START + 5)}
          )
        else
          UserMailer.channel_follows_notification_to_admin(@admin, @channels.pluck(:label), user_emails, [] ).deliver_later
        end
      end
    end
    email_bulk_upload_details
    # Clearing the cached cached count of followers count of channel that is part of given teams
    # making changes for this issue EP-21404
    log.info "Clearing channels folllowers count cache for teams_channels_follower of team_ids: #{@team_ids.flatten.uniq}"
    TeamsUser.clear_cached(@team_ids.flatten.uniq)
  end

  class << self
    def csv_headers_validation(csv_data)
      csv_data = csv_data.encode!("UTF-8", invalid: :replace, undef: :replace)

      #https://stackoverflow.com/questions/19164254/how-can-i-replace-utf-8-errors-in-ruby-without-converting-to-a-different-encodin
      csv_data.scrub unless csv_data.valid_encoding?
      csv_rows = CSV.parse(csv_data)

      return "Invalid or empty csv" if csv_rows.length <= 1
      header = csv_rows.first

      # HEADERS Check, should contain "First, Last, Email, Group"
      valid_headers = (header[0] =~ /first/i &&
                       header[1] =~ /last/i &&
                       header[2] =~ /e[-]*mail/i)

      if valid_headers.nil?
        csv_error = "Invalid CSV headers. Use format first_name, last_name, email, groups (optional), picture_url (optional), password (optional)"
      end

      csv_error
    end

    def csv_formatted_user_emails(emails, groups)
      csv_string = CSV.generate do |csv|
        csv << %w(first_name last_name email groups)
        emails.each do |email|
          csv << ['', '', email, groups]
        end
      end
      csv_string
    end

    def csv_formatted_user_details(details, groups)
      csv_string = CSV.generate do |csv|
        csv << %w(first_name last_name email groups)
        csv << [details['first_name'], details['last_name'], details['email'], groups]
      end
      csv_string
    end
  end

  private
    def add_profile_picture(user:, tuple:)
      return if tuple['picture_url'].blank?

      user.avatar = tuple['picture_url']

      log.error "Could not save the user: #{tuple}" unless user.save
    end

    def addable_channels
      @organization.channels.where(id: @options[:channel_ids] || [])
    end

    # Combines groups from CSV row and when added from configuration when uploading CSV.
    def addable_groups(user_row)
      new_groups_for_user = []
      groups = @default_teams.clone
      group_names = (user_row['groups'] || '').strip.split(',')
      # combine groups and default_group if it present
      group_names |= [user_row['default_group']] if user_row['default_group']
      existing_groups = Team.where(name: group_names, organization_id: @organization.id).select('id, name, organization_id')

      new_group_names = group_names - existing_groups.pluck(:name)

      new_group_names.each do |group_name|
        new_group = Team.create(name: group_name, organization: @organization, description: group_name)
        new_group.add_admin(@admin) if @admin.present?

        @new_groups[group_name] = 0

        new_groups_for_user << new_group
      end

      (new_groups_for_user + existing_groups).each { |group| groups[group.name] = group }

      groups
    end

    def addable_user(tuple:, user_groups:)
      user = @organization.users.find_by(email: tuple['email'])
      # at this point we should already have proper default group - #addable_groups
      default_group = @organization.teams.find_by(name: tuple['default_group'])

      # If user exists with matching email, mark this user as `complete`.
      # Resend welcome email if Send Welcome Email is enabled while bulk importing user.
      if user
        @count[:old_users_count] += 1

        user_params = {is_complete: true, first_name: tuple['first_name'],
                    last_name: tuple['last_name'], default_team_id: default_group&.id}
        # avoid nil, false or blank parameters
        filteredUserParams = user_params.select { |k,v| v.present? } 
        user.update(filteredUserParams)

        if @options[:send_welcome_email] && resend_welcome_email?(user)
          federated_url = remote_provisioning(tuple, user)

          user.update(invitation_accepted: false)
          DeviseMandrillMailer.send_import_user_welcome_email(user_id: user.id, redirect_url: federated_url).deliver_later
          @count[:resent_emails_count] += 1
        end

        return user
      end
      # `:invitable` will be always be set when admin invites enterprise users.
      # Setting `:invitable` creates invitation, sets `authenticate_recipient` to be TRUE.
      # This flag asks user to sign in with their Enterprise Credential.
      if @options[:invitable]
        invite_user(tuple: tuple, user_groups: user_groups)
      else
        # Non-enterprise users can be added in here for some business reasons.
        # No need them to invite them, just create users and send welcome email
        # This email will logs them in.
        add_user(tuple: tuple, default_group: default_group)
      end
    end

    def add_user(tuple: {}, default_group: nil)
      password = tuple['password'].try(:strip)
      tuple['password'] = password || User.random_password

      user = User.new(
        first_name:  tuple['first_name'],
        last_name:   tuple['last_name'],
        email:       tuple['email'],
        password:    tuple['password'],
        bulk_import: true,
        is_complete: true,
        password_reset_required: password.nil?,
        organization: @organization,
        organization_role: 'member',
        default_team_id: default_group&.id,
        invitation_accepted: false
      )
      user.upload_mode = "bulk_upload"
      user.skip_confirmation!

      user.save
      if user.persisted?
        federated_url = remote_provisioning(tuple, user)

        if @options[:send_welcome_email]
          DeviseMandrillMailer.send_import_user_welcome_email(user_id: user.id, redirect_url: federated_url).deliver_later
        end

        @count[:new_users_count] += 1
        @persisted_user_ids.concat([user.id])
        user
      else
        @count[:failed_users_count] += 1
        nil
      end
    end

    def assign_groups(groups, user)
      groups.values.each do |group|
        if group.users.include?(user)
          next
        end

        group.add_member(user, skip_flush_cache: true)
        track_groups(groups: [group])
      end
    end

    def email_bulk_upload_details
      parameters = {
        admin:               @admin,
        channels_names_list: @channels_names_list,
        count:               @count,
        new_groups:          @new_groups,
        old_groups:          @old_groups,
        upload_time:         @file.created_at
      }

      BulkImportUserDetailsMailer.notify_admin(parameters).deliver_now
    end

    def follow_channels(users)
      @channels.each { |channel| channel.add_followers users }
      @channels_names_list = @channels.pluck('label').join(', ')
    end

    def invite_user(tuple:, user_groups:)
      email = tuple['email'].try(:strip)
      invitation = Invitation.pending.find_by(recipient_email: email, invitable: @organization)

      invitation ||= Invitation.new(
        sender:          @admin,
        recipient_email: email,
        roles:           'member',
        invitable:       @organization,
        send_invite_email: false,
        authenticate_recipient: true
      )

      channel_ids = @options[:channel_ids] || []
      team_ids    = user_groups.values.map(&:id)

      if invitation
        channel_ids += (invitation.parameters['channel_ids'] || [])
        team_ids += (invitation.parameters['team_ids'] || [])
      end

      parameters = invitation.parameters
      parameters.merge!({ 'team_ids' => team_ids.uniq, 'channel_ids' => channel_ids.compact.uniq, 'language' => tuple['language'] })
      parameters.merge!(tuple.slice(*@abbreviations))

      invitation.update(
        parameters: parameters,
        first_name: tuple['first_name'],
        last_name:  tuple['last_name']
      )

      @count[:invitees_count] += 1
      track_groups(groups: user_groups.values)
      InvitationMandrillMailer.send_welcome_email(invitation.id).deliver_later

      nil
    end

    def remote_provisioning(tuple, user)
      redirect_url = nil

      if @organization.okta_enabled?
        redirect_url = user.provision_and_sign_in(args: tuple, redirect_url: user.auth_landing_url)
      end

      redirect_url ||= user.auth_landing_url
    end

    def resend_welcome_email?(user)
      # There are plenty of users whose onboarding records isn't created. Such users must be considered for import.
      (user.user_onboarding.present? && !user.user_onboarding.completed?) || user.user_onboarding.blank?
    end

    def set_attributes(user:, tuple:, groups:, custom_fields:)
      add_profile_picture(user: user, tuple: tuple)
      assign_groups(groups, user)
      set_custom_fields(user, custom_fields)
      set_profile(user: user, profile: { language: tuple['language'], job_title: tuple['job_title'] })
    end

    def set_custom_fields(user, attributes)
      @custom_fields.wrap(user: user, attributes: attributes)
    end

    def set_profile(user:, profile:)
      UserProfile.set_profile(user: user, attributes: profile)
    end

    def teams_names(ids: [])
      @organization
        .teams
        .where(id: ids)
        .select('id, name, organization_id')
        .inject({}) do |hash, value|
          hash[value.name] = value
          hash
      end
    end

    def track_groups(groups: [])
      groups.each do |current_group|
        if @new_groups[current_group.name].present?
          @new_groups[current_group.name] += 1
        else
          @old_groups[current_group.name] = (@old_groups[current_group.name] || 0) + 1
        end
      end
    end

    def valid_email?(email)
      valid_email_regex = /\A[\w+\-'çÇéÉâêîôûÂÊÎÔÛàèùÀÈÙëïüËÏÜ.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
      (email =~ valid_email_regex) == 0
    end
end

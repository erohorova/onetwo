class EclApi::EclSource < EclApi::BaseCommunicator
  # api/v1/sources
  # Check ECL source params
  def get_sources(ecl_source_params={})
    connection = establish_connection
    default_params = {
      limit: 1000,
      is_default: false,
      is_enabled: true
    }

    resp = connection.get do |f|
      f.url '/api/v1/sources'
      f.headers = headers
      f.params = default_params.merge(ecl_source_params).as_json
    end

    sources = []
    if resp.success?
      begin
        sources = ActiveSupport::JSON.decode(resp.body)['data']
      rescue JSON::ParserError => e
        log.error "Response from ECL for sources is badly formatted: #{resp.body}. Exception :#{e}"
      end
    end

    sources
  end

  def payload
    super.merge(is_org_admin: true)
  end
end

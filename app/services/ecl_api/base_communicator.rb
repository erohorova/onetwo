class EclApi::BaseCommunicator
  attr_accessor :response, :response_data, :error, :user_id, :user

  def initialize(organization_id, user_id=nil)
    @organization = Organization.find(organization_id)
    @user_id = user_id
    @user = User.find(user_id) if user_id
  end

  def establish_connection
    Faraday.new(url: ecl_api_base_url)
  end

  def headers
    {
      "X-Api-Token": token,
      "Content-Type": "application/json",
      "Accept": "application/json",
    }
  end

  def token
    JWT.encode(payload, jwt_shared_key, 'HS256')
  end

  def payload
    {
      organization_id: @organization.id,
      name: @organization.name,
      user_id: @user_id
    }
  end

  def jwt_shared_key
    Settings.features.integrations.secret
  end

  def ecl_api_base_url
    Settings.ecl.api_endpoint
  end

  def success?
    @response.success?
  end

  def error
    @response_data = ActiveSupport::JSON.decode(@response.body)
    (@response_data["error"] || @response_data["message"]).try(:humanize)
  end

end

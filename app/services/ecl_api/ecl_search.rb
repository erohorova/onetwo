class EclApi::EclSearch < EclApi::BaseCommunicator
  CARD_CONTENT_TYPE = ['article', 'course', 'insight', 'video', 'video_stream', 'poll', 'document', 'book', 'project'].freeze
  PATHWAY_CONTENT_TYPE = "pathway"
  JOURNEY_CONTENT_TYPE = "journey"

  def search(query: , limit: 10, offset: 0, query_type: 'and', load_tags: true, filter_params: {}, load_hidden:  false)
    results_from_ecl = _search(query, limit, offset, query_type, filter_params)
    OpenStruct.new(results: _format_data_as_cards(results_from_ecl["data"], load_tags: load_tags, load_hidden: load_hidden),
                   aggregations: results_from_ecl["aggregations"])
  end

  def todays_learning(filters: {})
    connection = establish_connection

    response = connection.post do |f|
      f.url '/api/v2/content_items/todays_learning'
      f.body = filters.to_json
      f.headers = headers
    end

    if response.success?
      JSON.parse(response.body)
    else
      {}
    end
  end

  def _format_data_as_cards(ecl_results, load_tags: true, load_hidden: false)
    existing_cards = Card.where(organization_id: @organization.id, ecl_id: ecl_results.map{|r| r['id']}).includes(:card_metadatum, :channels_cards).index_by(&:ecl_id)

    cards = ecl_results.map do |ecl_result|
      ecl_id = ecl_result['id']
      additional_metadata = ecl_result['additional_metadata']
      cpe_credits = additional_metadata && additional_metadata['cpe_credits'] || ""
      cpe_subject = additional_metadata && additional_metadata['cpe_subject'] || ""

      if existing_cards.has_key?(ecl_id)
        card = existing_cards[ecl_id]
        card.ecl_metadata ||= {}
        card.ecl_metadata.merge!({
          source_display_name: ecl_result["source_display_name"],
          source_type_name: ecl_result["source_type_name"],
          duration_metadata: ecl_result["duration_metadata"],
          source_logo_url: ecl_result["source_logo_url"],
          mark_feature_disabled: ecl_result['mark_feature_disabled'],
          cpe_credits: cpe_credits,
          cpe_subject: cpe_subject
        })
        card
      else
        # EP:8233 Do not update card.title, card.message if it's a UGC content item or clone cards
        if [ Settings.ecl.ugc_source_id, Settings.ecl.shared_channel_source_id ].include?(ecl_result["source_id"])
          title = ecl_result["name"]
          message = ecl_result["description"]
        else
          title = nil
          message = ecl_result["name"].presence || ecl_result["description"]
        end
        published_date = if ecl_result['published_at'].present?
          ecl_result['published_at']
        else
          ecl_result['created_at']
        end
        c = Card.new(ecl_id: ecl_id,
                     title: title,
                     slug: Card.card_ecl_id(ecl_id),
                     message: message,
                     created_at: ecl_result["created_at"],
                     published_at: published_date,
                     updated_at: ecl_result["updated_at"],
                     organization_id: @organization.id,
                     is_public: (ecl_result["private_content"] ? !ecl_result["private_content"] : true),
                     ecl_metadata: {
                        :source_display_name=>ecl_result["source_display_name"],
                        :source_type_name=>ecl_result["source_type_name"],
                        :duration_metadata => ecl_result["duration_metadata"],
                        source_logo_url: ecl_result["source_logo_url"],
                        cpe_credits: cpe_credits,
                        cpe_subject: cpe_subject,
                        external_id: ecl_result['external_id'],
                        mark_feature_disabled: ecl_result['mark_feature_disabled']
                      },
                     readable_card_type_name: ecl_result['readable_card_type'] || ecl_result['content_type']
                    )

        c.taxonomy_topics = ecl_result["taxonomies_list"] if ecl_result["taxonomies_list"].present?
        c.cards_ratings = [CardsRating.new(level: ecl_result['level'], rating: ecl_result['rating'])]
        c.card_type, c.card_subtype = get_card_type_and_subtype(ecl_result["content_type"])
        c.prices = ecl_result["prices_data"] && ecl_result["prices_data"].map do|price|
          Price.new(price)
        end || []

        c.is_paid = true if ecl_result["prices_data"] .present?

        if load_tags
          c.tags = (ecl_result["tags"] || []).map{|t| Tag.find_or_initialize_by(name: t["tag"].try(:strip))}
        else
          c.tags = []
        end

        resource_metadata = ecl_result["resource_metadata"]
        c.resource = Resource.new(url: ecl_result["url"],
                                  image_url: ((resource_metadata.try(:[], "images") || [])[0].try(:[], 'url')),
                                  title: resource_metadata.try(:[], "title"),
                                  description: resource_metadata.try(:[], "description"),
                                  video_url: resource_metadata.try(:[], "video_url"),
                                  embed_html: resource_metadata.try(:[], "embed_html"))

        c
      end
    end
    cards
  end

  def get_card_type_and_subtype(ecl_content_type)
    case ecl_content_type
    when 'document'
      return 'media', 'link'
    when 'article'
      return 'media', 'link'
    when 'video'
      return 'media', 'video'
    when 'course'
      return 'course', 'link'
    else
      return 'media', 'link'
    end
  end

  def _search(q, limit, offset, query_type, filter_params)
    connection = establish_connection

    _params = {
      'limit' => limit,
      'offset' => offset,
      'query' => q,
    }
    if filter_params[:sort_attr].present? && filter_params[:sort_order].present?
      _params.merge!({'sort_attr' => filter_params[:sort_attr], 'sort_order' => filter_params[:sort_order] })
    end
    _params.merge!({'source_id' => filter_params[:source_id]}) unless filter_params[:source_id].nil?
    _params.merge!({'provider_name' => filter_params[:provider_name]}) unless filter_params[:provider_name].nil?
    _params.merge!({'languages' => filter_params[:languages]}) unless filter_params[:languages].nil?
    _params.merge!({'subjects' => filter_params[:subjects]}) unless filter_params[:subjects].nil?
    _params.merge!({'groups' => filter_params[:group_id]}) unless filter_params[:group_id].nil?
    _params.merge!({'channels' => filter_params[:channel_id]}) unless filter_params[:channel_id].nil?
    _params.merge!({'type' => filter_params[:type] || 'search'})
    _params.merge!({'source_type_name' => filter_params[:source_type_name]}) unless filter_params[:source_type_name].nil?
    _params.merge!({'content_type' => filter_params[:content_type]}) unless filter_params[:content_type].nil?
    _params.merge!({'only_default_sources' => filter_params[:only_default_sources]}) unless filter_params[:only_default_sources].nil?
    _params.merge!({'topic' => filter_params[:topic]}) unless filter_params[:topic].blank?
    _params.merge!({'topic_ids' => filter_params[:topic_ids]}) unless filter_params[:topic_ids].blank?
    _params.merge!({'exclude_ecl_card_id' => filter_params[:exclude_ecl_card_id]}) unless filter_params[:exclude_ecl_card_id].blank?
    _params.merge!({'operator' => 'or'}) if query_type == 'or'
    _params.merge!({'ecl_card_id' => filter_params[:ecl_card_ids]}) unless filter_params[:ecl_card_ids].blank?
    _params.merge!({'from_date' => filter_params[:from_date]}) unless filter_params[:from_date].blank?
    _params.merge!({'till_date' => filter_params[:till_date]}) unless filter_params[:till_date].blank?
    _params.merge!({'external_id' => filter_params[:external_id]}) if filter_params[:external_id].present?
    _params.merge!({'url' => filter_params[:url]}) if filter_params[:url].present?
    _params.merge!({'level' => filter_params[:level]}) if filter_params[:level].present?
    _params.merge!({'rating' => filter_params[:rating]}) if filter_params[:rating].present?
    _params.merge!({'currency' => filter_params[:currency]}) if filter_params[:currency].present?
    _params.merge!({'amount_range' => filter_params[:amount_range]}) if filter_params[:amount_range].present?
    _params.merge!({'is_paid' => filter_params[:is_paid]}) if filter_params[:is_paid].present?
    _params.merge!({'plan' => filter_params[:plan]}) if filter_params[:plan].present?

    # ecl prioritization params
    _params.merge!({'rank' => filter_params[:rank]}) if filter_params[:rank].present?
    _params.merge!({'rank_params' => filter_params[:rank_params]}) if filter_params[:rank_params].present?
    _params.merge!({'exclude_ecl_card_id' => filter_params[:exclude_ecl_card_id]}) unless filter_params[:exclude_ecl_card_id].blank?
    _params.merge!({'sort_by_recency' => filter_params[:sort_by_recency]}) if filter_params[:sort_by_recency].present?
    _params.merge!({'sociative' => filter_params[:sociative]}) if filter_params[:sociative].present?

    # access control
    _params.merge!({'users_with_access' => filter_params[:users_with_access]}) if filter_params[:users_with_access].present?
    _params.merge!({'groups_with_access' => filter_params[:groups_with_access]}) if filter_params[:groups_with_access].present?
    _params.merge!({'channels_with_access' => filter_params[:channels_with_access]}) if filter_params[:channels_with_access].present?
    _params.merge!({'only_private' => filter_params[:only_private]}) if filter_params[:only_private].present?
    _params.merge!({'load_hidden' => !!filter_params[:load_hidden]})  # !! so that null parameter is also sent as false
    _params.merge!({'is_admin' => filter_params[:is_admin]}) if filter_params[:is_admin].present?

    if @user.present?
      # User language
      _params.merge!({'language' => @user.language}) if @user.language.present?

      # user interests
      _params.merge!({'user_interests' => @user.learning_topics_name}) if @user.learning_topics_name.present?

      # user interests topics
      _params.merge!({'topic_ids' => @user.learning_topic_ids}) if @user.learning_topic_ids.present?
    end
    resp = connection.post do |f|
      f.url "/api/#{Settings.ecl.api_version}/content_items/search"
      f.body = _params.to_json
      f.headers = headers
    end

    search_results = {"data" => [], "aggregations" => []}
    if resp.success?
      begin
        search_results = ActiveSupport::JSON.decode(resp.body)
      rescue JSON::ParserError => e
        log.error "Response from ECL for search query: #{q} is badly formatted: #{resp.body}. Exception :#{e}"
      end
    else
      log.error "Response from ECL for search query: #{q} is badly formatted: #{resp.body}"
    end
    return search_results
  end
end

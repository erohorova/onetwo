class EclApi::EclSettings < EclApi::BaseCommunicator
# api/v1/sources
# Check ECL setiings param
#
  def create_ecl_settings(ecl_settings_params = {})
    default_params = {
        organization_id: @organization.id
    }
    response = establish_connection.post do |f|
      f.url '/api/v1/ecl_settings'
      f.headers = headers
      f.body = default_params.merge(ecl_settings_params).to_json
    end

    unless response.success?
      log.warn "Ecl settings creation failed. #{response.status}, #{response.body}"
    end
  end

  def payload
    super.merge(is_org_admin: true)
  end
end
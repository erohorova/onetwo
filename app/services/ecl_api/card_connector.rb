class EclApi::CardConnector
  include ActionView::Helpers::DateHelper

  attr_accessor :organization_id, :ecl_id

  def initialize(organization_id: nil, ecl_id: nil)
    @organization_id = organization_id
    @ecl_id = ecl_id
  end

  def headers(organization_id)
    payload = { organization_id: organization_id }
    token = JWT.encode(payload, Settings.features.integrations.secret, 'HS256')

    {
      "X-Api-Token": token,
      "Content-Type": "application/json",
      "Accept": "application/json",
    }
  end

  def establish_connection
    Faraday.new(url: Settings.ecl.api_endpoint)
  end

  def card_from_ecl_id(data = nil)
    existing_card = Card.where(ecl_id: @ecl_id, organization_id: @organization_id).first

    if existing_card.present?
      existing_card
    else
      card_data = data || content_item_by_id
      create_card(card_data) if card_data.present?
    end
  end

  def content_item_by_id
    log.debug "Requesting ECL content item with ecl_id=#{@ecl_id} for org_id=#{@organization_id}"
    begin
      response = establish_connection.get do |f|
        f.url "/api/#{Settings.ecl.api_version}/content_items/#{CGI.escape(@ecl_id)}"
        f.headers = headers(@organization_id)
      end
    rescue Faraday::Error => e
      log.error "Error communicating with ECL; ecl_id=#{@ecl_id}: #{e}"
      return
    end

    if response.success?
      begin
        data = ActiveSupport::JSON.decode(response.body)["data"]
        if data.blank?
          log.error "ECL API response body returned no data; ecl_id=#{@ecl_id}"
          return
        elsif !data.is_a?(Hash)
          log.error "ECL API response body data has invalid type; ecl_id=#{@ecl_id}"
          return
        end
      rescue JSON::ParserError
        log.error "ECL API response could not be parsed as JSON; ecl_id=#{@ecl_id}"
        return
      rescue NoMethodError => e
        log.error "ECL API response body is invalid; ecl_id=#{@ecl_id}: #{e}"
        return
      end
    else
      log.error "Invalid response code from ECL API; ecl_id=#{@ecl_id}: response_code=#{response.status}"
      return
    end

    data
  end

  def get_card_type(data)
    if data["content_type"].try(:downcase) == 'course'
      'course'
    else
      'media'
    end
  end

  def get_card_subtype(data)
    if ['video', 'image'].include? data["content_type"]
      data["content_type"]
    else
      'link'
    end
  end

  def get_privacy_value(data)
    # is_private key for sociative and access_restricted_in_orgs for ecl_app
    ## method definition in ecl for private_content
    # def private_content
    #   return self.object.is_private if self.object.respond_to?(:is_private) and self.object.is_private
    #   return true if self.object.access_restricted_in_orgs.present?
    #   return false
    # end
    return false if data['access_restricted_in_orgs'].present?
    return !data['private_content'] if data['private_content'].present?
    return !data['is_private']
  end

  def get_teams_with_access(data)
    return [] unless data['groups'].present?

    data['groups'].map do |group_data|
      if group_data.is_a?(Hash)
        group_data['group_id']
      else
        group_data
      end
    end
  end

  def resource_attributes(resource_metadata, url)
    resource_metadata ||= {}
    image_url = ((resource_metadata.try(:[], "images") || [])[0] || {}).fetch('url', nil)

    {
      url: url,
      image_url: image_url,
      title: resource_metadata.fetch("title", nil).to_s.truncate(250),
      description: resource_metadata.fetch("description", nil).to_s.truncate(500),
      video_url: resource_metadata.fetch("video_url", nil),
      embed_html: resource_metadata.fetch("embed_html", nil)
    }.reject{|_, value| value.blank? }
  end

  def get_resource(data)
    resource_metadata = data["resource_metadata"]
    url = (resource_metadata && resource_metadata["url"]) || data["url"]

    if url.present? && resource_metadata && resource_metadata["title"].present?
      resource = Resource.new(resource_attributes(resource_metadata, url))
      resource.type = "Video" if data["content_type"] == "video"
      resource.save
    else
     resource = Resource.extract_resource data["url"]
    end

    resource
  end

  def get_restricted_users_data(data)
    return [] unless data['restricted_users'].present?
    data['restricted_users'].map do |restricted|
      if restricted.is_a?(Hash)
        restricted['user_id']
      else
        restricted
      end
    end
  end

  def get_restricted_groups_data(data)
    return [] unless data['restricted_groups'].present?
    data['restricted_groups'].map do |restricted|
      if restricted.is_a?(Hash)
        restricted['group_id']
      else
        restricted
      end
    end
  end

  def extract_languages(additional_metadata)
    languages = [(additional_metadata || {})["language"]] || []
    if (item_languages = (additional_metadata || {})["languages"]) && item_languages.is_a?(Array)
      languages << item_languages.collect{|item_language| item_language["Name"]}
    end
    languages = languages.flatten.compact.uniq.map(&:downcase)
    languages[0] if languages.present? && languages.size == 1
  end

  def create_card(data)
    # EP-8223: deprecating use of card.title, using card.message instead using data["name"] || data["description"]
    published_date = data['published_at'].present? ? DateTime.parse(data['published_at']) : Time.now.utc
    card = Card.new(
      card_type:       get_card_type(data),
      card_subtype:    get_card_subtype(data),
      title:           nil,
      message:         data["name"].presence || data["description"],
      resource:        get_resource(data),
      organization_id: @organization_id,
      state:           'published',
      published_at:    published_date,
      is_public: get_privacy_value(data),
      readable_card_type_name: data['readable_card_type'] || data['content_type'],
      language: UserProfile::VALID_LANGUAGES_LOOKUP[extract_languages(data['additional_metadata'])],
      provider: data['provider_name']
    )

    topics = (data["tags"] || []).map{|t| t["tag"].strip}.uniq.join(",")
    card.topics = topics if topics.present?
    card.taxonomy_topics = data["taxonomies_list"] if data["taxonomies_list"].present?
    card.ecl_id = data["id"]
    if data["prices_data"].present?
      card.prices_attributes = data["prices_data"]
      card.is_paid = true
    end

    card.ecl_metadata = ecl_metadata(data)
    card.ecl_source_name = data['source_display_name']

    teams_with_access = get_teams_with_access(data)
    if teams_with_access
      card.shared_with_team_ids = teams_with_access
    end

    restricted_users = get_restricted_users_data(data)
    if restricted_users
      card.users_with_permission_ids = restricted_users
    end

    restricted_groups = get_restricted_groups_data(data)
    if restricted_groups
      card.teams_with_permission_ids = restricted_groups
    end

    if card.save
      card
    else
      log.error "Error saving card to the database; ecl_id=#{card.ecl_id} #{card.errors.full_messages}"
      nil
    end
  end

  def ecl_metadata(data)
    additional_metadata = data['additional_metadata']
    cpe_credits = additional_metadata && additional_metadata['cpe_credits'] || ""
    cpe_subject = additional_metadata && additional_metadata['cpe_subject'] || ""

    {
      source_id: data["source_id"],
      source_display_name: data["source_display_name"],
      source_type_name: data["source_type_name"],
      duration_metadata: data["duration_metadata"],
      source_logo_url: data["source_logo_url"],
      external_id: data['external_id'],
      lrs_enabled: data['lrs_enabled'],
      mark_feature_disabled: data['mark_feature_disabled'],
      cpe_credits: cpe_credits,
      cpe_subject: cpe_subject
    }.reject{|key, value| value.blank?}
  end

  def destroy_card_from_ecl(ecl_id, organization_id)
    result = establish_connection.delete do |req|
      req.url "/api/#{Settings.ecl.api_version}/content_items/#{ecl_id}"
      req.headers = headers(organization_id)
    end

    unless result.success?
      log.warn "Error deleting card from ecl; ecl_id=#{ecl_id} #{JSON.parse(result.body)} for current organization"
    end
  end

  def archive_card_from_ecl(ecl_id, organization_id)
    result = establish_connection.put do |req|
      req.url "/api/#{Settings.ecl.api_version}/content_items/#{ecl_id}"
      req.headers = headers(organization_id)
      req.body = {'status' => 'archived' }.to_json
    end

    unless result.success?
      log.warn "Error archiving card from ecl; ecl_id=#{ecl_id} #{JSON.parse(result.body)} for current organization"
    end
  end

  def trash_card_from_ecl
    result = establish_connection.post do |req|
      req.url "/api/#{Settings.ecl.api_version}/content_item_states"
      req.headers = headers(organization_id)
      req.body = { 'ecl_id' => ecl_id }.to_json
    end

    unless result.success?
      log.warn "Error trashing card from ecl; ecl_id=#{ecl_id} #{JSON.parse(result.body)} for current organization"
    end
  end

  def recover_card_from_ecl
    result = establish_connection.put do |req|
      req.url "/api/#{Settings.ecl.api_version}/content_item_states/update_state"
      req.headers = headers(organization_id)
      req.body = { 'ecl_id' => ecl_id, 'state' => 'active' }.to_json
    end

    unless result.success?
      log.warn "Error recovering card from ecl; ecl_id=#{ecl_id} #{JSON.parse(result.body)} for current organization"
    end
  end

  def header_with_user(organization_id, user_id)
    payload = { organization_id: organization_id, user_id: user_id }
    token = JWT.encode(payload, Settings.features.integrations.secret, 'HS256')

    {
      "X-Api-Token": token,
      "Content-Type": "application/json",
      "Accept": "application/json",
    }
  end

  def get_private_content_from_ecl(user_id, limit, offset)
    user = User.find_by(id: user_id)
    group_ids = user.team_ids
    channel_ids = user.followed_channel_ids

    result = establish_connection.post do |req|
      req.url "/api/#{Settings.ecl.api_version}/content_items/private_contents"
      req.body = { 'groups' => group_ids, 'channels' => channel_ids, 'limit' => limit, 'offset' => offset}.to_json
      req.headers = header_with_user(user.organization_id, user_id)
    end

    if result.success?
      begin
        result_body = ActiveSupport::JSON.decode(result.body)["data"]
        data = Card.where(organization_id: user.organization_id, ecl_id: result_body.map{|r| r['id']})
          .order("created_at DESC")

      rescue JSON::ParserError
        log.error "ECL API response could not be parsed as JSON; ecl_id=#{@ecl_id}"
        return
      end
    else
      log.error "Invalid response code from ECL API; response_code=#{response.status}"
      return
    end

    data
  end

  def sync_ecl_card(card_id)
    card = Card.find_by(id: card_id)
    if card.nil?
      return false, {"error" => "Card not found"}
    end

    result = establish_connection.get do |req|
      req.url "/api/#{Settings.ecl.api_version}/content_items/#{card.ecl_id}"
      req.headers = headers(card.organization_id)
    end

    if result.success?
      data = JSON.parse(result.body)['data']
      taxonomy_topics = data["taxonomies_list"].present? ? data["taxonomies_list"] : nil

      card.update_attributes({
        taxonomy_topics: taxonomy_topics,
        ecl_id: data["id"],
        ecl_metadata: ecl_metadata(data)
      })
      return true, {"ecl_id" => data["id"]}
    else
      return false, {"error" => JSON.parse(result.body)}
    end
  end

  def propagate_card_to_ecl(card_id)
    card = Card.find_by(id: card_id)
    if card.nil?
      log.warn("Propagating card to ecl failed, card not found: #{card_id}")
      return
    end
    source_id = card.ugc? ? Settings.ecl.ugc_source_id : (
      card.cloned_card? ? Settings.ecl.shared_channel_source_id : card.ecl_metadata["source_id"] )

    payload = {
      "url" => card.resource.try(:url),
      "user_id" => card.author_id,
      "source_id" => source_id,
      "user_taxonomies" => card.user_taxonomy_topics,
      "hidden" => card.hidden,
      "update_request_from_lxp" => true,
      "status" => card.ecl_state
    }

    payload["tags"] = if card.cloned_card?
        card.tags.map{|tag| {"tag" => tag.name, "tag_type" => "keyword", "source" => "shared_channel_content", "strength" => 1.0}}
      else
        card.tags.map{|tag| {"tag" => tag.name, "tag_type" => "keyword", "source" => "ugc", "strength" => 1.0}}
      end

    # Note: EP-13195
    # Fix Propagation of title and message to ECL content item
    # Do not update description if card title and card message same
    if card.title.present? && (card.title != card.message)
      payload["name"] = card.title
      payload["description"] = card.message
    else
      payload["name"] = card.message
    end

    # Note: EP-9504
    # ECL App checks uniqueness of cards based on external id.
    # Do not propagate external_id for Non UGC cards
    # Propagate external_id only for UGC cards
    if card.ugc? || card.cloned_card?
      payload.merge!(
        "external_id" => card.id,
        "content_type" => translate_card_type_to_content_type(card)
      )
    end

    if card.pack_card? && card.pack_cards.present?
      pack_items = card.pack_cards.pluck(:message, :title).flatten.compact
      payload.merge!("pack_items" => pack_items)
    end

    if card.journey_card? && card.journey_relns.present?
      journey_items = card.journey_inner_cards_data
      payload.merge!("pack_items" => journey_items)
    end

    if card.prices.present?
      prices_data = card.prices.map{|price| {"amount" => price.amount, "currency" => price.currency}}
      payload.merge!("prices_data" => prices_data)
    end

    if card.card_metadatum
      level = card.card_metadatum&.level
      payload.merge!("level" => level)
      rating = card.card_metadatum.average_rating.to_i
      payload.merge!("rating" => rating)
    end

    paid_plans = CardMetadatum::PAID_PLANS + ['free/paid'] # Adding 'free/paid' to the existing plans for Pathways/Journeys
    is_paid = (card.is_paid || paid_plans.compact.include?(card.card_metadatum&.plan))
    payload.merge!("is_paid" => is_paid)

    plan = card.card_metadatum&.plan
    plan ||= (card.is_paid ? 'paid' : 'free') # not all cards with is_paid true have been migrated to card_metadata's plan
    payload.merge!("plan" => plan)

    # course clone card duration metadata need to sync from parent course card
    if card.cloned_card?
      time = card.parent_card.time_to_read
      payload.merge!(
        "duration_metadata" => {
          "calculated_duration" => time.to_i,
          "calculated_duration_display" =>
            distance_of_time_in_words(Time.now, Time.now + time.to_i.seconds)
        }
      )
    end

    # if card duration is explicitly specified from user
    # pass info into ecl payload for duration_metadata
    if card.duration.present?
      payload.merge!(
        "duration_metadata" => {
          "calculated_duration" => card.duration.to_i,
          "calculated_duration_display" =>
            distance_of_time_in_words(Time.now, Time.now + card.duration.to_i.seconds)
        }
      )
    end

    if card.ecl_id.present?
      payload.merge!('ecl_id' => card.ecl_id)
    end

    # We will propagate access details only if
    # 1. A UGC is being CREATED or UPDATED
    # 2. A card WITH ecl_id is being only UPDATED
    if card.ecl_id.present? || card.ugc?
      if card.ecl_id.present?
        condition = {ecl_id: card.ecl_id}
      else
        condition = {id: card.id}
      end

      payload.merge!(
        "access_restricted_in_orgs" => get_access_restricted_in_orgs(condition),
        "groups" => get_groups(condition),
        "channels" => get_channels(condition),
        "users_with_access" => get_users_with_access(condition),
        "restricted_users" => get_restricted_users(condition, card.organization_id),
        "restricted_groups" => get_restricted_groups(condition, card.organization_id)
      )
    end

    if card.resource
      payload.merge!({"resource_metadata" => {"title" => card.resource.title, "description" => card.resource.description, "embed_html" => card.resource.embed_html, "images" => [{"url" => card.resource.image_url}].compact}})
    end

    result = establish_connection.post do |req|
      req.url "/api/#{Settings.ecl.api_version}/content_items"
      req.headers = headers(card.organization_id)
      req.body = payload.to_json
    end

    if result.success?
      data = JSON.parse(result.body)['data']
      update_card(card, data)
    else
      log.error("Propagating card to ecl failed for card_id: #{card_id}, error: #{JSON.parse(result.body)} with payload #{payload.to_json} ")
    end
  end

  def update_card(card, data)
    taxonomy_topics = data["taxonomies_list"].present? ? data["taxonomies_list"] : nil
    card.update_attributes({
      taxonomy_topics: taxonomy_topics,
      ecl_id: data['id'],
      ecl_metadata: ecl_metadata(data),
      ecl_source_name: data['source_display_name']
    })
  end

  # This method is used by api/v2/ecl/sync_card
  def update_card_change_from_ecl(card, data)
    update_card_fields = {
      ecl_id:          data['id'],
      ecl_metadata:    ecl_metadata(data),
      ecl_source_name: data['source_display_name'],
      taxonomy_topics: data['taxonomies_list'],
      topics:          (data['tags'] || []).map{|t| t['tag'].strip}.uniq.join(','),
      update_request_from_ecl: true,
      published_at:    data['published_at'],
      language: UserProfile::VALID_LANGUAGES_LOOKUP[extract_languages(data['additional_metadata'])],
      provider: data['provider_name']
    }

    # Fix: EP-13889
    # ecl card sync should verify whether existing title and message are different
    if card.title.present? && (card.title != card.message)
      update_card_fields["title"]   = data["name"]
      update_card_fields["message"] = data["description"]
    else
      update_card_fields["message"] = data["name"].presence || data["description"]
      update_card_fields["title"]   = update_card_fields["message"]
    end

    if data['readable_card_type'].present?
      update_card_fields['readable_card_type_name'] = data['readable_card_type']
    end

    if data["prices_data"].present?
      prices = []
      data["prices_data"].each do |price_data|
        permitted_price_attrs = price_data.permit(:currency, :amount)
        currency, amount = permitted_price_attrs[:currency], permitted_price_attrs[:amount]
        price = Price.find_or_initialize_by(card_id: card.id, currency: currency)
        price.amount = amount
        prices << price if price.valid?
      end
      if prices.present?
        card.prices = prices
        update_card_fields["is_paid"] = true
      end
    end

    teams_with_access = get_teams_with_access(data)
    if teams_with_access
      update_card_fields['shared_with_team_ids'] = teams_with_access
    end

    restricted_users = get_restricted_users_data(data)
    if restricted_users
      update_card_fields['users_with_permission_ids'] = restricted_users
    end

    restricted_groups = get_restricted_groups_data(data)
    if restricted_groups
      update_card_fields['teams_with_permission_ids'] = restricted_groups
    end

    # Excluding blank values
    update_card_fields.reject!{ |_, value| value.blank? }

    # Pushing boolean values to the attribute set.
    if data["access_restricted_in_orgs"].present?
      update_card_fields[:is_public] = get_privacy_value(data)
    end

    card.update_attributes(update_card_fields)

    if card_resource = card.resource
      card_resource.update_attributes(
        resource_attributes(data['resource_metadata'], data['url'])
      )
    end
  end

  def update_cards_on_source_update(ecl_card, payload)
    if payload[:ecl_metadata].present?
      ecl_metadata = ecl_card.ecl_metadata.merge(payload[:ecl_metadata])
      ecl_card.update_column(:ecl_metadata, ecl_metadata)
      ecl_card.reindex
    end
  end

  def translate_card_type_to_content_type(card)
    case card.card_type
      when 'media'
        case card.card_subtype
          when 'video' then 'video'
          when 'link' then 'article'
          else 'insight'
          end
      when 'poll' then 'poll'
      when 'course' then 'course'
      when 'pack' then 'pathway'
      when 'journey' then 'journey'
      when 'video_stream' then 'video_stream'
      else 'insight'
      end
  end

  def get_access_restricted_in_orgs(condition)
    Card.where({is_public: false}.merge(condition)).pluck(:organization_id).uniq
  end

  def get_groups(condition)
    SharedCard.includes(:card).joins(:card).where(cards: condition)
      .map{|shared_card| {org_id: shared_card.card.organization_id, group_id: shared_card.team_id}}
  end

  def get_channels(condition)
    ChannelsCard.includes(:card).joins(:card).where(cards: condition)
      .map{|channel_card| {org_id: channel_card.card.organization_id, channel_id: channel_card.channel_id}}
  end

  def get_users_with_access(condition)
    CardUserShare.includes(:card).joins(:card).where(cards: condition)
      .map{|user_access| {org_id: user_access.card.organization_id, user_id: user_access.user_id}}
  end

  def get_restricted_users(condition, org_id = nil)
    CardUserPermission.joins(:card).where(cards: condition)
      .map{|restricted_user| {org_id: org_id, user_id: restricted_user.user_id}}
  end

  def get_restricted_groups(condition, org_id = nil)
    CardTeamPermission.joins(:card).where(cards: condition)
      .map{|restricted_team| {org_id: org_id, group_id: restricted_team.team_id}}
  end

  # This will notify Ecl that Lxp has finished using the aws s3 object where
  # the deleted ecl_ids(content item ids) of the deleted source were stored.
  # Ecl can then delete this particular s3 object.
  def notify_card_deletion_job_completion_to_ecl(source_id)
    response = establish_connection.delete do |f|
      f.url "/api/v1/sources/#{source_id}/delete_s3_object_source"
      f.headers = headers(@organization_id)
    end
    if !response.success?
      log.error("Error in notifying Ecl about card deletion job completion #{JSON.parse(response.body)} ")
    end
  end

  def fetch_source_by_slug(source_slug)
    response = establish_connection.get do |f|
      f.url "/api/v1/sources?display_name=#{source_slug.gsub('-', '_')}&limit=1"
      f.headers = headers(@organization_id)
    end
    if response.success?
      data = JSON.parse(response.body)['data'][0]
      {"source" => data}
    else
      {"error" => response.status}
    end
  end

end

class MandrillMessageGenerationService
  def initialize
    @message = {
      "inline_css" => true,
      "merge_language" => "handlebars",
      "preserve_recipients" => false,
      "important" => false,
      "url_strip_qs" => false,
      "track_clicks" => true,
      "track_opens" => true
    }
  end

  # Parameters:
  # html_file(String):         Template file name with extension
  # subject(String):           Subject line for email
  # from_email(String):        Sender's email address
  # from_name(String):         Sender's name
  # to(Array):                 an array of recipient information
  # attachments(Array):        an array of supported attachments to add to the message
  # global_merge_vars(Array):  global merge variables to use for all recipients
  # merge_vars(Array):         per-recipient merge variables, which override global merge variables with the same name
  # recipient_metadata(Array): Per-recipient metadata that will override the global values specified in the metadata parameter
  def merge_custom_attrs(*args)
    html_file = args.first[:html_file]
    # if current_user is present and custom template exist
    # return custom template from EmailTemplate model
    # else #read_from_file
    html = if args.first[:test_email]
      html_file
    else
      get_template(
        user: args.first[:current_user],
        file_name: html_file,
        mailer: args.first[:mailer],
        organization: args.first[:organization]
      )
    end
    
    subject = get_subject(
      user: args.first[:current_user],
      file_name: html_file,
      organization: args.first[:organization],
      default: args.first[:subject])

    @message[:html] = html
    @message[:subject] = subject
    @message[:to] = args.first[:to]

    if args.first[:mailer].present?
      mailer = args.first[:mailer]
      @message[:from_name] = mailer.from_name
      @message[:from_email] = mailer.from_address
    else
      from_name_exist = args.first[:from_name].present?
      @message[:from_name] =  args.first[:from_name] if from_name_exist
      @message[:from_email] = args.first[:from_email]
    end

    attachments = args.first[:attachments]
    global_merge_vars = args.first[:global_merge_vars]
    recipient_metadata = args.first[:recipient_metadata]
    merge_vars = args.first[:merge_vars]
    tags = args.first[:tags]

    if global_merge_vars.present?
      @message[:global_merge_vars] = global_merge_vars
    end

    if recipient_metadata.present?
      @message[:recipient_metadata] = recipient_metadata
    end

    @message[:attachments] = attachments if attachments.present?
    @message[:merge_vars] = merge_vars if merge_vars.present?
    @message[:tags] = tags if tags.present?
    @message
  end

  private

  def get_template_directory(mailer = nil)
    if mailer.present?
      return 'cma_test' unless Rails.env.production?
      "#{mailer.organization.host_name.downcase}_#{mailer.organization.id}"
    else
      'default'
    end
  end

  # TODO: Add logic to fetch file from subfolders too
  def read_from_file(filename, mailer = nil)
    # check if slim template
    is_slim = filename.ends_with?('.slim')
    directory = get_template_directory(mailer)
    # only default directory contains .slim files
    file_path = Rails.root.join('public', 'templates', 'default', filename)

    unless is_slim
      file_path = Rails.root.join(
        'public', 'templates', directory, filename
      )
    end
    # get file content
    file_data = safely_read_file(file_path: file_path, is_slim: is_slim)
    # need_fallback `true` if html and file_data is nil
    need_fallback = [is_slim, file_data].none?

    return file_data unless need_fallback
    # fallback to templates/default folder with extra .slim extension
    fallback_path = Rails.root.join(
      'public', 'templates', 'default', filename + '.slim'
    )

    safely_read_file(file_path: fallback_path, is_slim: is_slim)
  end

  def get_subject(user: nil, file_name:, organization: nil, default: nil)
    org = user.present? ? user&.organization : organization
    if org.nil? || !org.get_settings_for('emailTemplatesEnabled')
      return default
    end

    # return file name without extensions *.html or *.html.slim
    proper_title = file_name[/^[^.]+/]

    default_template = org.email_templates.find_by(title: proper_title)

    custom_template = default_template&.custom_templates&.find_by(
      language: user.try(:language) || 'en', is_active: true, state: 'published'
    )

    if custom_template.present? && custom_template.subject.present?
      return custom_template.subject
    else
      return default
    end
  end

  def get_template(user: nil, file_name:, mailer: nil, organization: nil)
    org = user.present? ? user&.organization : organization
    if org.nil? || !org.get_settings_for('emailTemplatesEnabled')
      return read_from_file(file_name, mailer)
    end
    # return file name without extensions *.html or *.html.slim
    proper_title = file_name[/^[^.]+/]

    # drop user agent ending
    user_agent_ending = proper_title.split('-').last.in?(%w[ios android web])
    if user_agent_ending
      proper_title = proper_title.split('-')[0...-1].join('-')
    end

    # should exist only one default template in org by title
    default_template = org.email_templates.find_by(title: proper_title)
    # default language
    default_language = org.get_settings_for('DefaultOrgLanguage') || 'en'
    # found `is_active` custom template with preferred user language
    custom_template = default_template&.custom_templates&.find_by(
      language: user.try(:language) || default_language,
      is_active: true,
      state: 'published'
    )
    # return custom_template if template found
    # else return #read_from_file
    if custom_template.present?
      custom_template.content
    else
      read_from_file(file_name, mailer)
    end
  end

  # under `public/templates` directory could be existed two file types: .html, .slim
  def safely_read_file(file_path:, is_slim:)
    return Slim::Template.new(Rails.root.join(file_path).to_s).render if is_slim

    file = File.open(file_path)
    file_data = file.read
    file.close
    file_data
  rescue Errno::ENOENT, Slim::Parser::SyntaxError => e
    log.error("MandrillMessageGenerationService file read error: #{e.message}")
    nil
  end
end

require 'csv'

class ExportResourceGroups < ReportService
  HEADERS = ['id', 'title', 'description', 'image_url', 'deeplink_url',  'keywords', 'duration', 'metadata']

  def initialize(groups: [], user:)
    @groups = groups
    @user   = user
    @organization = user.organization
    @s3_url = nil
  end

  def run
    generate_and_upload
    send_email
  end

  private
    def generate_and_upload
      rows = prepare_rows

      file = generate_csv([HEADERS] + rows)

      @s3_url = upload_to_s3(
        file: file,
        filename: "export-courses-#{Time.now.utc.strftime("%d-%m-%y-%H-%M-%S")}.csv",
        bucket_name: "edcast-reports-#{Rails.env}",
        content_type: 'application/csv' )

      log.info "Uploaded Courses report to S3 URL: #{@s3_url}"

      file.close
      file.unlink
    end

    def prepare_rows
      rows = []

      @groups.each do |group|
        group_url = Rails.application.routes.url_helpers.redirect_course_url(group.client_resource_id, host: @organization.home_page)
        rows << [group.client_resource_id, group.name, group.description, group.absolute_image_url, group_url, 'deeplink', '', '']
      end

      rows
    end

    def send_email
      UserMailer.send_courses_report(s3_url: @s3_url, user: @user).deliver_later
    end
end

#   This class responsible for all operations of CLC like
#   Popolate CLC score, processing CLC progress, return CLC progress etc.
class ClcService
  attr_accessor :organization, :user, :duration

  def initiate_clc_records(card, user)

    @organization = card.organization
    @card = card
    @user = user
    #Duration in minutes 
    @duration = ClcCalculateDurationService.get_duration(organization: @organization,card: @card)
    #Populate records only if duration present
    poulate_clc_records if @duration.present?
  end

  def update_clc_score_of_user(card_id, user_id, entity_id, entity_type)
    card = Card.find_by(id: card_id)
    user = User.find_by(id: user_id)
    initiate_clc_records(card, user)
    clc = Clc.active.find_by(entity_id: entity_id, entity_type: entity_type)
    compute_progress_of_user(clc, user) if clc.present?
  end

  # deprecated
  def compute_progress(clc)
    clc_records_group_by_user(clc).each do |clc_score_record|
      create_or_update_clc_progress(clc, clc_score_record)
    end
  end

  def compute_progress_of_user(clc,user)
    clc_score_record = clc_records_of_user(clc, user)
    create_or_update_clc_progress(clc,clc_score_record) if clc_score_record.present?
  end

  # intentionally performing delete in runtime to avoid "Cannot delete or update a parent row: a foreign key constraint fails" error
  # delete called instead of destroyed for no callbacks calling
  def remove(card_id)
    ClcOrganizationsRecord.where(card_id: card_id).delete_all
    ClcChannelsRecord.where(card_id: card_id).delete_all
    ClcTeamsRecord.where(card_id: card_id).delete_all
  end

  #get the sumation of clc_progress for the organization for particular time period day wise.
  # all the paramters are required to get the progress.
  def sumation_clc_progress(from_date,to_date,organization_id)
    clc_progresses = ClcOrganizationsRecord
                      .select("sum(score) clc_score, DATE_FORMAT(created_at,'%d/%m/%Y') as clc_date")
                      .where("organization_id = :organization_id and created_at BETWEEN :from_date and :to_date", from_date: from_date, to_date: to_date, organization_id: organization_id)
                      .group("DATE(created_at)")
                      .order("created_at ASC")
    (from_date..to_date).to_a.collect do |date|
      ((clc_progresses.select{|clc_progress| clc_progress.clc_date == date.strftime('%d/%m/%Y')}.first.clc_score.to_f) rescue 0)
    end
  end

  #get the sumation of clc_progress for the team of the organization for particular time period month wise.
  def team_clc_progress(from_date, to_date, organization_id, team_id: nil)
    clc_progresses = ClcOrganizationsRecord
                      .where("organization_id = :organization_id and created_at BETWEEN :from_date and :to_date",
                        from_date: from_date, to_date: to_date, organization_id: organization_id)
                      .where("user_id in (?) and card_id in (?)", TeamsUser.where(team_id: team_id).pluck(:user_id),
                        Team.find_by(id: team_id).distinct_cards)
                      .select("sum(score) as clc_score, Month(created_at) AS created_month, Year(created_at) as created_year")
                      .group("created_month, created_year")
                      .order("created_at ASC")

    (from_date.to_date..to_date.to_date).to_a.map{|date| [date.month, date.year]}.uniq.collect do |date_set|
      ((clc_progresses.select{|clc_progress| clc_progress.created_month == date_set[0] &&  clc_progress.created_year == date_set[1]}.first.clc_score.to_f) rescue 0)
    end
  end

  private
  def create_or_update_clc_progress(clc, clc_score_record)
    ClcProgress.where(user_id: clc_score_record.user_id, clc_id: clc.id).first_or_initialize.tap do |clc_progress|
      clc_progress.clc_score = clc_score_record.clc_score
      if clc_progress.save
        score_in_hours = (clc_progress.clc_score.to_f/60.to_f)
        score_percentage = ((score_in_hours/clc.target_score) * 100).to_i
        UserBadgeService.new("Clc", clc, clc_score_record.user_id, score_percentage).assign_badge # allocate clc badge
      else
        log.warn "Error while computing clc_progress for user_id #{clc_score_record.user_id} for clc id #{clc.id}: #{clc_progress.full_errors}"
      end
    end
  end

  #object can have Organization, channel, team
  def clc_for(object)
    Clc.active.find_by(entity: object)
  end

  def channels_with_clc_for(user)
    Channel.readable_channels_for(user, _organization:  user.organization,onboarding:false).joins(:clcs)
  end

  def teams_with_clc_for(user)
    user.teams.joins(:clcs)
  end

  def clc_organization_records(organization)
    ClcOrganizationsRecord.where(organization: organization)
  end

  def clc_channel_records(channel)
    ClcChannelsRecord.where(channel: channel)
  end

  def clc_team_records(team)
    ClcTeamsRecord.where(team: team)
  end

  def poulate_clc_records
    populate(@organization, @user, @card) if clc_for(@organization)
    # Right now we only use ClcOrganizationsRecord to calculate clc_progress.
    # we don't have any ui to set clc for channel and team
    # than also we are fetching clc for channel and team.
    # channels = channels_with_clc_for(@user) & @card.channels
    # teams = teams_with_clc_for(@user) & @card.teams
    # if channels.present?
    #   channels.each{|channel| populate(channel, @user, @card)}
    # end

    # if teams.present?
    #   teams.each{|team| populate(team, @user, @card)}
    # end
  end

  #object can have Organization, channel, team
  def populate(object, user, card)
    unless find_record_for_user(object, user, card)
      unless clc_records_for(object).create(user_id: user.id, card_id: card.id, score: @duration)
        log.error "Error while creating clc_#{object.class.name.downcase}_records for user_id #{user.id}, card id #{card.id} and #{object.class.name.downcase} id #{object.id}"
      end
    end
  end

  def clc_records_group_by_user(clc)
    clc_records_for(clc.entity)
      .select("sum(score) as clc_score, user_id")
      .where(created_at: clc.from_date..clc.to_date)
      .group(:user_id)
  end

  def clc_records_of_user(clc, user)
    clc_records_for(clc.entity)
      .select("sum(score) as clc_score, user_id")
      .find_by(created_at: (clc.from_date..clc.to_date),user_id: user.id)
  end

  def find_record_for_user(object, user, card)
    clc_records_for(object).find_by(user_id: user.id, card_id: card.id)
  end

  def clc_records_for(object)
    send("clc_#{object.class.name.downcase}_records", object)
  end
end

class LeaderboardService
  include SortOrderExtractor

  attr_reader :params, :organization, :current_user

  # PARAMS :-
  # :active, String, desc: "Boolean, whether to show only active users or not"
  # :order_attr, String, desc: "Attribute to sort on. One of \"smartbites_consumed\", \"smartbites_created\", \"smartbites_completed\", \"smartbites_score\", \"smartbites_comments_count\", \"smartbites_liked\", \"time_spent\", default: \"smartbites_score\""
  # :order, String, desc: "Either \"asc\" or \"desc\", default: \"desc\""
  # :group_ids, String, desc: "Array of group ids. Show only users who are members of selected groups"
  # :roles, String, desc: "Array of role names. Show only users with selected roles"
  # :expertise_names, String, desc: "Array of expertise names. Show only users with selected expertises"
  # :show_my_result, String, desc: "Boolean. Return current user metrics also if true"
  # :q, String, desc: "Show only users that match the search term"

  def initialize(organization: , params: , current_user: ,
    metric_period: , metric_offset: , limit: , offset: )
    @organization = organization
    @params = params
    @current_user = current_user
    @metric_period = metric_period
    @metric_offset = metric_offset
    @limit = limit
    @offset = offset
  end

  def call
    # Prepare sort order. By default, it's smartbites_score.
    order_attr, order = sorting_order

    # Calculate metrics only for filtered users
    user_level_metrics = UserLevelMetric.joins(:user)
                        .where(
                            period: @metric_period,
                            offset: @metric_offset,
                            organization_id: organization,
                            users: { is_suspended: 0, exclude_from_leaderboard: false, hide_from_leaderboard: [false, nil] }
                        )

    if params[:q].present? || params[:expertise_names].present?
      search_result = User.search(body: construct_es_query)
      user_level_metrics = user_level_metrics.where(users: { id: search_result.results.map(&:id)}) if search_result.response.present?
    end

    if team_ids.present?
      filtered_by_team_ids = user_level_metrics.filter_by_team_ids(team_ids).pluck(:id)
      user_level_metrics = user_level_metrics.where(id: filtered_by_team_ids)
    end

    if role_ids.present?
      filtered_by_role_ids = user_level_metrics.filter_by_role_ids(role_ids).pluck(:id)
      user_level_metrics = user_level_metrics.where(id: filtered_by_role_ids)
    end


    user_level_metrics = user_level_metrics.group('user_id').select('
                            user_level_metrics.user_id as user_id,
                            sum(smartbites_created) as smartbites_created,
                            sum(smartbites_consumed) as smartbites_consumed,
                            sum(time_spent) as time_spent,
                            sum(smartbites_score) as smartbites_score,
                            sum(smartbites_comments_count) as smartbites_comments_count,
                            sum(smartbites_liked) as smartbites_liked,
                            sum(smartbites_completed) as smartbites_completed
                          ').order("#{order_attr} #{order}").having(eligible_metrics)

    # current_user_metrics is sent separately as its extra info that is being
    # displayed on leaderboard. It should not be part of pagination records
    # as it will create offset errors.
    current_user_metrics = {}
    current_user_metrics = find_current_user_metrics(user_level_metrics) if params[:show_my_result]&.to_bool

    # Added for backward compatibility as mobile uses limit 1 to show metrics of current_user
    metrics = if @limit == 1 && params[:show_my_result]&.to_bool
      [current_user_metrics]
    else
      user_level_metrics.limit(@limit).offset(@offset)
    end

    {
      total_count: user_level_metrics.length,
      metrics: metrics,
      current_user_metrics: current_user_metrics
    }
  end

  private

  # ----------------------SORTING ORDER CALCULATION-----------------------------
  def sorting_order
    extract_sort_order(
      valid_order_attrs: %w(smartbites_consumed smartbites_created
                            smartbites_completed smartbites_score
                            smartbites_comments_count smartbites_liked
                            time_spent),
      default_order_attr: 'smartbites_score'
    )
  end

  # ---------------------------FILTERS FOR USERS--------------------------------
  def construct_es_query
    result_q = {}
    result_q = string_search_q if params[:q].present?
    result_q.merge!(expertise_search_q) if params[:expertise_names].present?

    result_q.merge!({ size: 1000, sort: [ _score: 'desc'] })
  end

  def string_search_q
    User.string_search_query(q: params[:q], organization_id: @organization.id)
  end

  def expertise_search_q
    User.expertise_search_query(expertise_names: params[:expertise_names], organization_id: @organization.id)
  end

  def team_ids
    return @selected_team_ids if @selected_team_ids.present?
    @selected_team_ids = Team.where(organization_id: organization.id).pluck(:id)

    # If not admin, then only show details from users who are in the same team
    # as the current user
    unless current_user.is_admin_user?
      user_team_ids = if current_user.group_sub_admin_authorize?(Permissions::MANAGE_GROUP_ANALYTICS)
        current_user.team_ids_for_group_type(['sub_admin'])
      else
        current_user.teams.pluck(:id)
      end

      @selected_team_ids = @selected_team_ids && user_team_ids
    end

    # Selecting teams based on params
    @selected_team_ids = (@selected_team_ids && params[:group_ids].map(&:to_i)) if params[:group_ids].present?

    @selected_team_ids
  end

  def role_ids
    # Selecting roles based on params
    @role_ids ||= if params[:roles].present?
      Role.where(organization_id: organization.id, name: params[:roles]).pluck(:id)
    else
      Role.where(organization_id: organization.id).pluck(:id)
    end
  end

  # -------------------------SCORING CONDITIONS---------------------------------
  def eligible_metrics
    eligible_metrics_conditions = "sum(smartbites_score) >= 1"
    if params[:active].present?
      eligible_metrics_conditions += params[:active].to_bool ? " AND sum(time_spent) > 0" : " AND sum(time_spent) = 0"
    end

    eligible_metrics_conditions
  end

  # --------------------------CURRENT USER METRICS------------------------------
  def find_current_user_metrics(user_level_metrics)
    current_user_lvl_metric = user_level_metrics.find_by(user_id: current_user.id)
    if current_user_lvl_metric.present?
      current_user_lvl_order = user_level_metrics.pluck(:user_id).index(current_user.id)
      current_user_lvl_metric&.level_order = current_user_lvl_order + 1 # current_user rank
    else
      current_user_lvl_metric = OpenStruct.new(
        user_id: current_user.id,
        smartbites_created: 0 ,
        smartbites_consumed: 0 ,
        time_spent: 0 ,
        smartbites_score: 0 ,
        smartbites_comments_count: 0 ,
        smartbites_liked: 0 ,
        smartbites_completed: 0,
        level_order: '-'
      )
    end
    current_user_lvl_metric
  end
end

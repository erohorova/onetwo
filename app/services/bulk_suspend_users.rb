
require 'csv'

class BulkSuspendUsers
  # MANDATORY: Sequence of attributes
  WHITELISTED_ATTRS = ['email', 'status']

  def initialize(file_id:, admin_id: nil)
    @file         = InvitationFile.find_by(id: file_id)
    @organization = @file.invitable
    @admin        = @organization.users.find_by(id: admin_id) if admin_id

    @headers = WHITELISTED_ATTRS
  end

  def preview_csv
    users = []
    csv_rows = CSV.parse(@file.data, headers: true, header_converters: lambda{ |h| h.underscorize })
    csv_rows.each do |row|
      tuple   = row.to_hash
      details = tuple.slice(*@headers)

      next if !valid_email?(details['email'])
      users << details

      break if users.length >= 10 # display first 10 rows
    end
    users
  end

  def import
    CSV.parse(@file.data, headers: true, header_converters: ->(h) { h.underscorize }) do |row|
      tuple = row.to_hash

      next unless valid_email?(tuple['email'].try(:strip))
      update_status(tuple: tuple)
    end
  end

  class << self
    def csv_headers_validation(csv_data)
      csv_data = csv_data.encode!("UTF-8", invalid: :replace, undef: :replace)

      csv_data.scrub unless csv_data.valid_encoding?
      csv_rows = CSV.parse(csv_data)

      return "Invalid or empty csv" if csv_rows.length <= 1
      first_row = csv_rows.first

      header = first_row.compact.map(&:downcase)
      
      # HEADERS Check, should contain "Email, Status"
      valid_headers = (header[0] == "email" &&
                       header[1] == "status" )

      unless valid_headers
        csv_error = "Invalid CSV headers. Use format email, status"
      end

      csv_error
    end

  end

  private

    def update_status(tuple:)
      user = @organization.users.find_by(email: tuple['email'])
      if user
        case tuple['status'].downcase
        when 'suspend'
          user.suspend! unless user.is_suspended?
        when 'active'
          if user.is_suspended?
            user.unsuspend!
          else
            user.switch_to_active_status
          end
        when 'inactive'
          user.inactivate!
        end
        return user
      end

    end

    def valid_email?(email)
      valid_email_regex = /\A[\w+\-'çÇéÉâêîôûÂÊÎÔÛàèùÀÈÙëïüËÏÜ.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
      (email =~ valid_email_regex) == 0
    end
end

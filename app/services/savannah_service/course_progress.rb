module SavannahService
  class CourseProgress < Base
    def get_course_progress(student_user)
      success, data = user_json_request(course_progress_url(@resource_group.client_resource_id), :get, student_user)

      if success
        return :ok, data
      else
        return :service_unavailable, {error: 'Error accessing Savannah/Edx'}
      end
    end

    private

    def course_progress_url(offering_id)
      "api/offerings/#{offering_id}/course_progress"
    end
  end
end
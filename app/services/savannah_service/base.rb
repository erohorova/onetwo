module SavannahService
  class Base
    def initialize(resource_group)
      @resource_group = resource_group
      @organization = @resource_group.organization
      @credential = @resource_group.valid_credential
    end

    def user_json_request(url, method, user, &block)
      has_block = block_given?

      json_request url, method do |req|
        req.headers['X-User-Email'] = user.email
        req.headers['X-Client-Name'] = @resource_group.app_name

        yield req if has_block
      end
    end

    def json_request(url, method, &block)
      conn = Faraday.new(Settings.savannah_base_location)
      has_block = block_given?

      result = conn.send(method) do |req|
        req.url url

        req.headers['Content-Type'] = 'application/json'
        req.headers['Accept'] = 'application/json'
        req.headers['X-Api-Key'] = @credential.api_key
        req.headers['X-Shared-Secret'] = @credential.shared_secret
        req.headers['X-Savannah-App-Id'] = @organization.savannah_app_id.to_s

        yield req if has_block
      end

      if result.success?
        return true, ActiveSupport::JSON.decode(result.body)
      else
        log.error "Request failed with status: #{result.status} and body: #{result.body}"
        return false, {}
      end
    end
  end
end
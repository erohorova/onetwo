module SavannahService
  class LastAccessed < Base
    def get_last_accessed(student_user)
      success, data = user_json_request(last_accessed_url(@resource_group.client_resource_id), :get, student_user)

      if success
        return :ok, data
      else
        return :service_unavailable, {error: 'Error accessing Edx'}
      end
    end

    def set_last_accessed(student_user, last_visited_module_id)
      success, data = user_json_request(last_accessed_url(@resource_group.client_resource_id), :put, student_user) do |req|
        req.body = {last_module_id: last_visited_module_id}.to_json
      end

      if success
        return :ok, data
      else
        return :service_unavailable, {error: 'Error accessing Edx or bad request from Edx'}
      end
    end

    private

    def last_accessed_url(offering_id)
      "api/offerings/#{offering_id}/last_accessed"
    end
  end
end
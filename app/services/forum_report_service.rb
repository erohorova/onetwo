class ForumReportService

  # Returns posts for a resource group for duration
  #
  def posts_for(resource_group: , from: , to:)
    resource_group.posts.where("cast(created_at as date) between ? and ?", from, to)
  end

  # Returns comments for a resource group for duration
  #
  def comments_for(resource_group:, from: , to:)
    group_post_ids = resource_group.posts.pluck(:id)
    Comment.joins(:user).where("comments.commentable_type = 'Post' AND comments.commentable_id IN (?) AND cast(comments.created_at as date) between ? and ? ", group_post_ids, from, to)
  end

  # Returns subcomments for a resource group for duration
  #
  def sub_comments_for(comment_ids:, from: , to:)
    Comment.joins(:user).where("comments.commentable_type = 'Comment' AND comments.commentable_id IN (?) AND cast(comments.created_at as date) between ? and ? ", comment_ids, from, to)
  end

  def get_forum_data(group_id, from, to)
    post_rows = []

    resource_group = ResourceGroup.find_by(id: group_id)
    posts = posts_for(resource_group: resource_group, from: from, to: to)
    comments = comments_for(resource_group: resource_group, from: from, to: to)
    sub_comments = sub_comments_for(comment_ids: comments.pluck(:id), from: from, to: to)
    
    posts.each do |post|
      post_details = [post.user.name, post.title, Sanitize.clean(post.message), Post.url(post, resource_group)]
      post_comments = comments.select{|comment| comment.commentable_id == post.id}
      comments -= post_comments
      if( post_comments.length > 0 )
        post_comments.each do |comment|
          post_sub_comments = sub_comments.select{|sub_comment| sub_comment.commentable_id == comment.id}
          sub_comments -= post_sub_comments
          if( post_sub_comments.length > 0 )
            post_sub_comments.each do |sub_comment|
              post_rows << post_details + [comment.user.name, Sanitize.clean(comment.message), sub_comment.user.name, Sanitize.clean(sub_comment.message)]
            end
          else
            post_rows << post_details + [comment.user.name, Sanitize.clean(comment.message), '', '']
          end
        end
      else
        post_rows << post_details + ['', '', '', '']
      end  
    end

    post_rows
  end
end
class AnalyticsReport::UserContentActivityReport < ReportService
  DEFAULT_OBJECT_TYPES = ['card', 'video_stream', 'user', 'channel']
  REPORT_TYPES = ['SOCIAL', 'CONTENT']
  SOCIAL_REPORT_ACTIONS = ['like', 'unlike', 'playback', 'comment', 'follow', 'unfollow', 'assign', 'bookmark', 'mark_completed_self', 'mark_completed_assigned', 'mark_uncompleted_self', 'mark_uncompleted_assigned']
  CONTENT_REPORT_ACTIONS = ['impression', 'create', 'delete']
  APP_PLATFORMS = ['web', 'android', 'ios', 'extension']
  ACTIONT_TO_ACTIVITY_MAP = {
    impression: 'Viewed',
    create: 'Created',
    delete: 'Deleted',
    like: 'Liked',
    unlike: 'Unliked',
    playback: 'Played',
    comment: 'Commented',
    follow: 'Followed',
    unfollow: 'Unfollowed',
    assign: 'Assigned',
    bookmark: 'Bookmarked',
    mark_completed_self: 'Completed',
    mark_completed_assigned: 'Completed',
    mark_uncompleted_self: 'Uncompleted',
    mark_uncompleted_assigned: 'Uncompleted'
  }.with_indifferent_access.freeze

  REPORT_HEADERS = ["Date", "Timestamp", "UserEmail", "UserName", "ContentName", "ContentType", "ContentOwner", "ActivityType"]
  S3_BUCKET_NAME = "edcast-platform-metrics-report-#{Rails.env}"

  def initialize(organization:, timestamp: , period: , report_type: 'content')
    @organization  = organization
    @period = period || :week
    @timestamp = timestamp || Time.now.utc.prev_week
    @file_name = "#{@organization.host_name}/user_#{report_type}_activity_#{Time.now.strftime("%d-%m-%y-%H-%M-%S")}.csv"
    @report_type = (REPORT_TYPES & [report_type.upcase]).first
  end

  def generate_report
    csv_rows = report_details + [REPORT_HEADERS] + report_data
    file = generate_csv csv_rows

    aws_url   = upload_to_s3(
      file: file,
      filename: @file_name,
      bucket_name: S3_BUCKET_NAME,
      content_type: "application/csv"
    )

    log.info "Uploaded analytics report to S3 URL: #{aws_url}"
    file.close
    file.unlink

    aws_url
  end

  ######################## PRIVATE ######################
  private

  def report_data
    @metrics = MetricRecord.search(build_query).results
    process_metrics_to_reportable_data
  end

  def report_details
    details = []
    details << ["Organization: #{@organization.name}"]
    details << ["Period: #{PeriodOffsetCalculator.timestamp_to_readable_period(timestamp: @timestamp, period: @period)}"]
    details
  end

  def build_query
    range = get_timeranges_for_period
    and_filters = [
      { term: { organization_id: @organization.id } },
      { terms: { object_type: DEFAULT_OBJECT_TYPES } },
      { terms: {platform: APP_PLATFORMS }},
      { range: { created_at: {gte: range[0], lte: range[1] } } },
      { terms: { action: valid_actions_for_report } }
    ]

    and_filters << { term: { "properties.view" => "full" } } if @report_type == 'CONTENT'

    query =
    {
      query: {
        filtered: {
          filter: {
            and: and_filters
          }
        }
      },
      sort: {
        created_at: {
          order: 'desc',
          ignore_unmapped: true
        }
      }
    }
    query
  end

  def get_timeranges_for_period
    [@timestamp.beginning_of_week, @timestamp.end_of_week]
  end

  # data {Timestamp, UserName, User Email, Content Type (Smartbite, Pathway etc.), Content Name, Content owner, Activity Type (Viewed, Deleted) }
  def process_metrics_to_reportable_data
    data = []

    @metrics.each do |metric|
      event = {date: metric.created_at.strftime("%m/%d/%Y"), timestamp: metric.created_at.strftime("%H:%M:%S %z")}
      actor = @organization.users.find_by_id(metric.actor_id)
      content = load_content_from_metric(metric)

      event[:actor_email] = actor.try(:email) || ""
      event[:actor_full_name] = actor.try(:full_name) || ""
      event[:content_name] = content.try(:snippet) || ""
      event[:content_type] = get_content_type(content)

      owner = @organization.users.find_by_id(metric.owner_id)
      event[:owner_full_name] = owner.try(:full_name)
      event[:activity_type] = ACTIONT_TO_ACTIVITY_MAP[metric.action] || ""
      data << event.values
    end
    data
  end

  def load_content_from_metric(metric)
    content = case metric.object_type
    when 'card'
      @organization.cards.find(metric.object_id)
    when 'video_stream'
      @organization.video_streams.find(metric.object_id).card
    when 'channel'
      @organization.channels.find(metric.object_id)
    when 'user'
      @organization.users.find(metric.object_id)
    end
    content
  end

  def get_content_type(content)
    return "" unless content
    case content
    when Card
      content.pack_card? ? 'Pathway' : 'Smartbite'
    when Channel
      'Channel'
    when User
      'User'
    end
  end

  def valid_actions_for_report
    AnalyticsReport::UserContentActivityReport.const_get("#{@report_type}_REPORT_ACTIONS")
  end
end
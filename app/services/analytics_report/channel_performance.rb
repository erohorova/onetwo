class AnalyticsReport::ChannelPerformance
  def initialize(args)
    @organization_id = args[:organization_id]
    @channel_ids     = args[:channel_ids]
    @from_date       = args[:from_date]
    @to_date         = args[:to_date]
  end

  # Return format:
  # {
  #   "took"=>2,
  #   "timed_out"=>false,
  #   "_shards"=>{"total"=>5, "successful"=>5, "failed"=>0},
  #   "hits"=>{"total"=>11, "max_score"=>0.0, "hits"=>[]},
  #   "aggregations"=>
  #    { "actions"=>
  #       { "doc_count_error_upper_bound"=>0,
  #         "sum_other_doc_count"=>0,
  #         "buckets"=>
  #         [{ "key" => "impression",
  #             "doc_count"=>6,
  #             "contents"=>
  #               { "doc_count_error_upper_bound"=>0,
  #                 "sum_other_doc_count"=>0,
  #                 "buckets"=>
  #                   [{"key"=>6891, "doc_count"=>4},
  #                    {"key"=>6892, "doc_count"=>2} ]
  #               }
  #          }, ....]
  #       }
  #    }
  # }
  #
  def aggregation_query(opts= {})
    body = {
      size:  0,
      query: {
        bool: {
          must: [
            { term:  { organization_id:  @organization_id }},
            { terms: { object_type: %w(card collection video_stream) }},
            { range: { created_at: { gt: @from_date, lte: @to_date }}},
            { terms: { action: %w(like comment bookmark impression mark_completed_assigned mark_completed_self)}},
          ],
          must_not: [
            { terms: { action: %w(unlike uncomment unbookmark) }},
            { term:  { platform: 'forum' }},
            { term:  {"properties.view" => "summary" } }
          ]
        }
      },
      aggs: {
        actions: {
          terms: {
            field: "action"
          },
          aggs: {
            contents: {
              terms: { size: 9000, field: "object_id" }
            }
          }
        }
      }
    }

    MetricRecord.gateway.client.search(
      index: MetricRecord.index_name,
      body: body
    )
  end

  # Return format:
  # {
  #   "view" => {401898=>5, 391125=>3, 400985=>3, 401725=>3, 401245=>2},
  #   "like" => {401898=>3, 391125=>3, 400985=>1, 401725=>3},
  #   ...
  # }
  #
  def get_consumed_cards(query_result)
    data = {}

    query_result["aggregations"]["actions"]["buckets"].each do |a|
      a["contents"]["buckets"].each do |c|
        key = MetricRecord.get_action_key(a['key'])
        data[key]            ||= {}
        data[key][c["key"]]    = c["doc_count"]
      end
    end

    data
  end

  def fetch_channel_stats
    stats = {}
    channel_ids = @channels.map(&:first)

    @channels.each do |channel_record|
      stats[channel_record[0]] = {
        label: channel_record[1],
        is_private: channel_record[2],
        followers_count: 0,
        published_cards_count: 0,
        likes_count:     0,
        bookmarks_count: 0,
        comments_count:  0,
        views_count:     0,
        completes_count: 0
      }
    end

    followers_count_for_date_range(channel_ids, @from_date, @to_date).each do |channel_id, followers_count|
      stats[channel_id][:followers_count] = followers_count
    end

    published_cards_count_for_date_range(channel_ids, @from_date, @to_date).each do |channel_id, published_cards_count|
      stats[channel_id][:published_cards_count] = published_cards_count
    end

    ChannelsCard.where(channel_id: channel_ids).pluck(:channel_id, :card_id).group_by(&:first).each do |channel_id, channel_cards|
      card_ids           = channel_cards.map(&:second)

      likes_contents     = (@consumed_cards["like"] || {}).select{|k| card_ids.include?(k)}
      bookmarks_contents = (@consumed_cards["bookmark"] || {}).select{|k| card_ids.include?(k)}
      comments_contents  = (@consumed_cards["comment"] || {}).select{|k| card_ids.include?(k)}
      views_contents     = (@consumed_cards["view"] || {}).select{|k| card_ids.include?(k)}
      completes_contents = (@consumed_cards["complete"] || {}).select{|k| card_ids.include?(k)}

      stats[channel_id][:likes_count] = likes_contents.values.sum
      stats[channel_id][:bookmarks_count] = bookmarks_contents.values.sum
      stats[channel_id][:comments_count] = comments_contents.values.sum
      stats[channel_id][:views_count] = views_contents.values.sum
      stats[channel_id][:completes_count] = completes_contents.values.sum
    end

    stats
  end

  def get_records(search_term = nil)
    @channels = get_channels(search_term)
    @consumed_cards = get_consumed_cards(aggregation_query)
    fetch_channel_stats
  end

  def get_channels(search_term = nil)
    channels = if @channel_ids.present?
      Channel.where(organization_id: @organization_id, id: @channel_ids)
    else
      Channel.where(organization_id: @organization_id)
    end
    channels = channels.where('label like :q', q: "%#{search_term}%") unless search_term.nil?
    channels.pluck(:id, :label, :is_private)
  end

  def followers_count_for_date_range(channel_ids, from_date, to_date)
    followers_users = Channel.joins(:follows)
                          .where(id: channel_ids)
                          .where('follows.created_at between ? and ?', from_date, to_date)
                          .select('channels.id', 'follows.user_id as user_id')
                          .group_by(&:id)

    followers_teams = Channel.joins(followed_teams: :users)
                          .where(id: channel_ids)
                          .where('teams_channels_follows.created_at between ? and ?', from_date, to_date)
                          .select('channels.id', 'teams_users.user_id')
                          .group_by(&:id)

    follows = followers_users.merge(followers_teams) { |k,v1,v2| v1 + v2 }
    follows.transform_values! { |v| v.uniq(&:user_id).count }
  end

  def published_cards_count_for_date_range(channel_ids, from_date, to_date)
    ChannelsCard.joins("inner join channels on channels_cards.channel_id = channels.id").curated.where("channels_cards.channel_id IN (?)", channel_ids).where(curated_at: from_date..to_date).group('channels.id').count(:id)
  end
end

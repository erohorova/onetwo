class AnalyticsReport::AssessmentReport < ReportService
  S3_BUCKET_NAME = Settings.report_bucket

  def initialize(card_id:, user_id:)
    @card =  Card.find_by(id: card_id)
    @user = User.find_by(id: user_id)
    @quiz_question_cards = @card.associated_poll_cards
    @file_name ="assessment-report-#{card_id}_#{Time.now.strftime("%d-%m-%y-%H-%M-%S")}.csv"
  end

  def build_assessment_detail
    details = []
    details << [@card.snippet]

    @quiz_question_cards.each do |question|
      details << ["#{question.snippet}", "#{question.associated_correct_option}"]
    end

    details
  end

  def build_headers
    headers = ['email']
    (1..@quiz_question_cards.count).each{ |c| headers << "question #{c}"}
    headers << "score(%)"
    [headers]
  end

  def build_data
    # name: pathways name
    # question1: answer1
    # question2: answer2
    # question3: answer3

    # Headers user(email/name), question 1, question 2, question 3, %of correct answers
    data = []

    @card.attempted_by.each do |user_id|
      quiz_data = []
      attempted_user = User.find_by(id: user_id)
      quiz_data << (attempted_user.email || attempted_user.name)

      @quiz_question_cards.each do |quiz_question_card|
        quiz_data <<  (QuizQuestionAttempt.find_by(quiz_question: quiz_question_card, user_id: user_id).try(:selected_option).try(:label) || "-")
      end

      quiz_data << attempted_user.percentage_correct(@card.id)

      data << quiz_data
    end

    data
  end

  def generate_report
    csv_rows = build_assessment_detail + build_headers + build_data
    file = generate_csv csv_rows

    aws_url   = upload_to_s3(
      file: file,
      filename: @file_name,
      bucket_name: S3_BUCKET_NAME,
      content_type: "application/csv"
    )

    log.info "Uploaded assessment report to S3 URL: #{aws_url}"
    file.close
    file.unlink

    aws_url
  end
end

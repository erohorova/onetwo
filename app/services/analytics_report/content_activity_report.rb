class AnalyticsReport::ContentActivityReport < ReportService
  #card_id: Id of the card
  #from_date: Date with format mm/dd/YYYY. Default: a month in past from now
  #to_date: Date with format mm/dd/YYYY. Default: today
  #action_types: Array of actions. possible values
    # like, unlike, playback, comment, follow, unfollow, assign, bookmark,
    # mark_completed_self, mark_completed_assigned, add_to_channel, add_to_pathway

  def initialize(card_id:, from_date: , to_date:, action_types: [])
    from_date ||= (Date.today - 1.month).strftime("%m/%d/%Y")
    to_date ||= Date.today.strftime("%m/%d/%Y")

    @card  = Card.find card_id
    @timerange = set_timerange_for_period(from_date, to_date)
    @action_types = action_types
  end

  def generate_report
    report = {}
    report[:data] = []
    if @action_types.include?('add_to_channel')
      report[:data] += report_for_add_to_channel
      @action_types.delete 'add_to_channel'
    end

    if @action_types.include?('add_to_pathway')
      report[:data] += report_for_add_to_pathway
      @action_types.delete 'add_to_pathway'
    end

    report[:data] += report_for_other_actions
    report[:total] = report[:data].count
    report
  end

  private

  def set_timerange_for_period(from_date, to_date)
    [Date.strptime(from_date, "%m/%d/%Y").beginning_of_day.utc, Date.strptime(to_date, "%m/%d/%Y").end_of_day.utc]
  end

  # TODO: this event has to be part of MetricRecord
  def report_for_add_to_channel
    metrics = []
    _metrics = ChannelsCard.where(card_id: @card.id)
    _metrics.each do |metric|
      event = {}
      event[:created_at] = metric.created_at.strftime("%e %b %Y %H:%M:%S%p")
      event[:curated_at] = metric.curated_at.strftime("%e %b %Y %H:%M:%S%p")
      event[:channel_id] = metric.channel_id
      event[:channel_name] = metric.channel.label
      event[:action] = 'add_to_channel'
      metrics << event
    end
    metrics
  end

  # TODO: this event has to be part of MetricRecord
  def report_for_add_to_pathway
    metrics = []
    _metrics = CardPackRelation.where(from_id: @card.id)
    _metrics.each do |metric|
      event = {}
      event[:created_at] = metric.created_at.strftime("%e %b %Y %H:%M:%S%p")
      event[:cover_id] = metric.cover_id
      event[:title] = metric.cover.title || metric.cover.message
      event[:action] = 'add_to_pathway'
      metrics << event
    end
    metrics
  end

  def report_for_other_actions
    metrics = []
    _metrics = MetricRecord.search(build_query).results
    _metrics.each do |metric|
      event = {}
      event[:created_at] = metric.created_at.strftime("%e %b %Y %H:%M:%S%p")
      event[:actor_id] = metric.actor_id
      event[:actor_name] = User.find_by_id(metric.actor_id).full_name
      event[:action] = metric.action
      metrics << event
    end
    metrics
  end

  def build_query
    and_filters = [
        { term: { object_id: @card.id } },
        { terms: { object_type: ['card'] } },
        { range: { created_at: {gte: @timerange[0], lte: @timerange[1] } } },
        { terms: { action: @action_types } }
    ]

    query =
        {
            query: {
                filtered: {
                    filter: {
                        and: and_filters
                    }
                }
            },
            sort: {
                created_at: {
                    order: 'desc',
                    ignore_unmapped: true
                }
            }
        }
    query
  end
end
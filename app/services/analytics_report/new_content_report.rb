class AnalyticsReport::NewContentReport < ReportService

  def initialize(last_access:, content_types:, viewer:)
    @last_access = Time.at(last_access.to_i)
    @content_types = content_types
    @viewer = viewer
    @org = @viewer.organization
  end

  def generate_report
    report = {}

    if @content_types.include? 'featured'
      featured = get_featured
      report[:featured_count] = featured[:count]
      report[:featured_ids] = featured[:ids]
    end
    report[:required_count] = get_required_count if @content_types.include? 'required'
    report[:curated_count] = get_curated_count if @content_types.include? 'curated'

    report
  end

  private

  def get_featured
    filter_params = { card_type: Card::EXPOSED_TYPES, state: 'published', is_official: true, ranges: [{promoted_at: {gt: @last_access}}] }

    featured_count = Search::CardSearch.new.search(
        nil,
        @org,
        viewer: @viewer,
        filter_params: filter_params,
        limit: 0
    ).total

    { count: featured_count }
  end

  def get_required_count
    @viewer.assignments.where("created_at > ?", @last_access).count
  end

  def get_curated_count
    Search::CardSearch.new.search_for_cms(
        '',
        @org.id,
        filter_params: {state: ['new','archived','published'], ranges: [{created_at: {gt: @last_access}}]},
        limit: 0
    ).total
  end

end

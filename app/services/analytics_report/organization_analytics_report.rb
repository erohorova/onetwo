class AnalyticsReport::OrganizationAnalyticsReport < ReportService
  S3_BUCKET_NAME = "edcast-platform-metrics-report-#{Rails.env}"
  REPORT_HEADERS = ["Smartcards created", "Smartcards consumed", "Active users", "Average session", "Total users", "New users", "Engagement index"]

  # organization: Organization object for which report is being generated
  # timestamp: Timestamp, used to calculate offset, generate report for that period
  #     Ex. period: :month with timestamp:2017-06-14 22:36:51 UTC would generate report for Month of June, 2017
  # period: one of :day, :week, :month, :year, :all_time
  def initialize(organization:, timestamp: nil, period: nil)
    @organization = organization
    @timestamp = timestamp || Time.now.utc
    @period = period || :day
    @file_name = "#{@organization.host_name}/org_metrics_#{Time.now.strftime("%d-%m-%y-%H-%M-%S")}.csv"
  end

  def generate_report
    csv_rows = report_details + [REPORT_HEADERS] + report_data
    file = generate_csv csv_rows

    aws_url   = upload_to_s3(
      file: file,
      filename: @file_name,
      bucket_name: S3_BUCKET_NAME,
      content_type: "application/csv"
    )

    log.info "Uploaded org analytics report to S3 URL: #{aws_url}"
    file.close
    file.unlink

    aws_url
  end

  private
  def report_data
    data = []
    offset = PeriodOffsetCalculator.applicable_offsets(@timestamp)[@period]
    data << OrganizationMetricsGatherer.current_period_data(organization: @organization, period: @period, offset: offset).values
    data
  end

  def report_details
    details = []
    details << ["Organization: #{@organization.name}"]
    details << ["Period: #{PeriodOffsetCalculator::timestamp_to_readable_period(timestamp: @timestamp, period: @period)}"]
    details
  end
end
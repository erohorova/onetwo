class PaymentGatewayService

  def initialize(orderable_type:, orderable:, price:, user_id:, organization_id:, gateway:)
    @orderable_type = orderable_type.camelize
    @orderable = orderable
    @price = price
    @user_id = user_id
    @currency = @price.currency
    @organization_id = organization_id
    @gateway = gateway || PaymentGateway.gateway_for_currency(@currency)
  end

  # Create and return proper order & transaction objects and generate transaction token
  # Additionally will return client_token for braintree, and approval_url for Paypal
  def prepare_payment!
    ActiveRecord::Base.transaction do
      @order = create_order
      @transaction = create_transaction
      @token = generate_token
      resp_params = { transaction: @transaction, order: @order, token: @token }

      gateway_inst = PaymentGateway.gateway_cls(@gateway).new
      gateway_inst.merge_gateway_params(
                     params: {
                       resp_params: resp_params,
                       orderable: @orderable,
                       price: @price
                     }
                   )
    end
  end

  # Find the transaction object from the token and call process payment 
  def self.process_payment!(token:, **args)
    @transaction = find_transaction(token: token)
    @transaction.process_payment!(args)
  end

  # Find the transaction object from the token and call cancel event for transaction
  def self.cancel_payment!(token:)
    @transaction = find_transaction(token: token)
    @transaction.cancel_payment!
  end

  private

  def create_order
    args = {
      orderable: @orderable,
      user_id: @user_id,
      organization_id: @organization_id
    }
    
    Order.initialize_order!(args: args, orderable_type: @orderable_type)
  end

  def create_transaction
    Transaction.initialize_payment!(order_id: @order.id,
                                    gateway: @gateway,
                                    amount: @price.amount,
                                    currency: @currency,
                                    user_id: @user_id,
                                    source_name: get_source_name,
                                    order_type: @orderable_type)
  end

  # Used to store source name in transactions table for Settlement Reports
  def get_source_name
    if @order.orderable_type == "Card"
      @order.orderable&.ecl_metadata&.dig('source_type_name')
    else
      ""
    end
  end

  def self.get_secret_key
    Edcast::Application.config.secret_key_base
  end

  # Encode transaction id using JWT token
  def generate_token
    JWT.encode({payment_id: @transaction.id}, PaymentGatewayService.get_secret_key, 'HS256')
  end

  # Decode the transaction id which was encoded using JWT, and return transaction object
  def self.find_transaction(token:)
    begin
      data_decoded = JWT.decode token, get_secret_key, true, algorithm: 'HS256'
    rescue JWT::DecodeError
      raise IllFormattedTransactionIdError
    end

    transaction = Transaction.find_by(id: data_decoded[0]["payment_id"])
    raise WrongTransactionIdError unless transaction
    transaction
  end
end

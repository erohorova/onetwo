#   This class responsible for all calculations of CLC duration 
class ClcCalculateDurationService
  #Will return calculated duration in minutes
  def self.get_duration(organization: , card: )
    @organization = organization
    @card = card

    duration_display = @card.ecl_metadata&.dig("duration_metadata", "calculated_duration_display")

    if duration_display && conversion_required?(duration_display)
      duration = calculate_duration(duration_display)  
    else
      duration = @card.ecl_metadata&.dig("duration_metadata", "calculated_duration")
      (duration / 60).round if duration
    end
  end

  private
  #Will return false if @card.ecl_metadata["calculated_duration_display"] value contains second,seconds,minute,minutes,hour or hours.
  def self.conversion_required?(duration_display)
    !(duration_display.include?("second") || duration_display.include?("minute") || duration_display.include?("hour"))
  end

  #Will do calculation as per clc_minutes_for_day to get duration in minutes 
  def self.calculate_duration(duration)
    duration_number = duration.gsub(/\D/, "").to_i
    clc_minutes_for_day = @organization.clc_minutes_for_day

    calculated_duration = if duration.include?("day")
      duration_number * clc_minutes_for_day
    elsif duration.include?("week")
      duration_number * 7 * clc_minutes_for_day
    elsif duration.include?("month")
      duration_number * 30 * clc_minutes_for_day
    elsif duration.include?("year")
      duration_number * 365 * clc_minutes_for_day
    end
  end

end

class ContentProvider::Base
  def initialize(source_id)
    @source_detail = SourcesCredential.find_by(source_id: source_id)
  end
end
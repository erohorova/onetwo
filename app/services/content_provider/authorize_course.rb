class ContentProvider::AuthorizeCourse < ContentProvider::Base
  def initialize(args)
    super(args[:source_id])
    @user = args[:user]
    @card = args[:card]
    @payment = args[:payment]
  end

  def authorize
    return false unless @source_detail&.auth_api_info.present?

    auth_api_info = @source_detail.auth_api_info
    return true if auth_api_info["auth_required"] == false

    unless auth_api_info["host"].present?
      log.error "Host is not present for AUTH API for Source with id: #{@source_detail.id}"
      return
    end
    faraday_connection = Faraday.new(url: auth_api_info["host"])
    req_method = auth_api_info["method"].downcase || "post"
    params = {data: payload, auth_token: auth_token}
    process(faraday_connection, auth_api_info, params)
  end

  def establish_connection
    Faraday.new(url: @source_detail["host"])
  end

  def headers(auth_api_info)
    {
      "Content-Type": "application/json",
      "Accept": "application/json",
    }.merge(auth_api_info["headers"] || {})
  end

  def auth_token
    JWT.encode(payload, @source_detail.shared_secret, 'HS256')
  end

  def payload
    {
      user: @user.except(:id),
      card: @card.except(:id),
      payment: @payment.except(:id)
    }
  end

  def process(faraday_connection, auth_api_info, params)
    log.info "Hitting Auth API to unlock card at provider platform"
    req_method = auth_api_info["method"].downcase || "post"

    result = faraday_connection.send(req_method) do |req|
      req.url "#{auth_api_info['url']}"
      req.body    = params.to_json
      req.headers = headers(auth_api_info)
    end

    unlocked = result.success?

    if unlocked
      log.info "Authorization completed for user with id: #{@user.try(:[], :id)}
        for card: #{@card.try(:[], :id)} on source with source_id: #{@source_detail.id}"
    else
      log.error "Authorization Failed for user with id: #{@user.try(:[], :id)} for card: #{@card.try(:[], :id)}
        on source with source_id: #{@source_detail.id} with #{JSON.parse(result.body)}"
    end
    unlocked
  end
end

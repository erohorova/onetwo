class NiitService
  BASE_URL               = 'https://iv6-manage.niit-mts.com'
  CREATE_STUDENT_URL     = '/restservices/CreateStudent_v01.03/'
  SUBSCRIPTION_URL       = '/restservices/AssignSubscription_v01.01/'
  LOGIN_URL              = 'http://iv6-pmi.niit-mts.com/login/login.jsp'
  CLASSROOM_REDIRECT_URL = '/educate/pmi/catalog/UpcomingSession.jsp'
  ELEARNING_REDIRECT_URL = '/educate/pmi/catalog/overview.jsp'

  attr_reader :course_id, :course_type, :user, :domain, :language
  def initialize(opts= {})
    @user        = opts[:user]
    @course_id   = opts[:course_id]
    @course_type = opts[:course_type]
    @domain      = opts[:domain]
    @language    = opts[:language]
  end

  def headers
    {
      'accept'                 => 'application/json',
      'authorization-domain'   =>  domain,
      'authorization-username' =>  Settings.niit.api_username,
      'authorization-password' =>  niit_password,
      'Content-Type'           => 'application/json'
    }
  end

  def niit_username
    Base64.encode64(Settings.niit.api_username).gsub("\n", "")
  end

  def niit_password
    Base64.encode64(Settings.niit.api_password).gsub("\n", "")
  end

  def user_id_encoded
    Base64.encode64(user.id.to_s).gsub("\n", "")
  end

  def register_user
    conn = Faraday.new(BASE_URL)

    params = {
      "identification" => {
        "username" => user.email || user.first_name,
        "remoteUserIdentifier" => user.id
      },
      "studentProfile" => {
        "name" => {
           "firstName" => user.first_name,
           "lastName"  => user.last_name
        },
        "organization"    => domain,
        "emailAddress"    => user.email,
        "studentSettings" => []
      }
    }

    conn.post do |req|
      req.url CREATE_STUDENT_URL

      req.headers = headers
      req.body = params.to_json
    end
  end

  def subscribe_user
    conn = Faraday.new(BASE_URL)

    response = conn.post do |req|
      req.url SUBSCRIPTION_URL

      req.headers = headers
      req.body = {
        'remoteUserIdentifier' => user.id,
        'subscriptionPoolIdentifier' => Settings.niit.subscription_pool_identifier
      }.to_json
    end

    if !response.success?
      log.error("Niit course subscription failed for course_id: #{course_id}, course_type: #{course_type}, remoteUserIdentifier: #{user.id}, response: #{response.body}")
    end
  end

  def generate_course_deeplink_url
    uri = URI(LOGIN_URL)

    if @course_type.to_s == "3"
      redirect_url = CLASSROOM_REDIRECT_URL + "?key=#{course_id}&type=#{course_type}"
    elsif @course_type.to_s == "14"
      redirect_url = ELEARNING_REDIRECT_URL + "?key=#{course_id}&type=#{course_type}"
    end

    redirect_url << "&language=#{language}" if language.present?

    query = {
      'ruid'     => user_id_encoded,
      'domain'   => domain,
      'username' => niit_username,
      'password' => niit_password,
      'redirect' => redirect_url
    }.to_query

    uri.query = query
    uri.to_s
  end

  def fetch_deeplink_url
    proceed_to_subscription = true

    begin
      registration_response = register_user
      registration_json_response = JSON.parse(registration_response.body)

      if registration_response.success? || (registration_response.status == 500 && registration_json_response['soapenv:Fault']['faultstring'] == 'Remote user identifier is already in use in the domain')
        proceed_to_subscription = true
      end

      if proceed_to_subscription
        subscribe_user
        generate_course_deeplink_url
      else
        log.error("Niit user registration failed for course_id: #{course_id}, course_type: #{course_type}, domain: #{domain}, user_id: #{user.id}, response: #{registration_json_response}")
        nil
      end
    rescue Exception => e
      log.error("Niit Course deeplink generation failed for course_id: #{course_id}, course_type: #{course_type}, domain: #{domain}, user_id: #{user.id}, message: #{e.message}")
      nil
    end
  end
end

class ChannelCardService
  # PARAMS:-
  # param :channel_card_state, Array, desc: <<-TXT, required: false
  #   State of channel cards i.e curated, new, skipped or archived. Default: curated
  # TXT
  # param :card_type, Array, desc: <<-TXT, required: false
  #   Filter channel cards based on card types
  # TXT
  # param :ugc, String, desc: 'Get only UGC cards', required: false
  # param :filter_by_language, String, desc: 'Boolean. Filter cards based on the users language', required: false
  # param :from_date, String, desc: 'Filter to get cards created after a date, format: DD-MM-YYYY', required: false
  # param :till_date, String, desc: 'Filter to get cards create before date, format: DD-MM-YYYY', required: false
  # param :last_access_at, Integer, desc: 'Epoch timestamp', required: false
  # param :sort, String, desc: <<-TXT, required: false
  #   Sort criteria for channel cards viz. created_at, likes_count, views_count
  #   Default: rank of channel_card
  # TXT
  # param :order, String, 'Sort order(asc or desc). Default: desc'

  attr_reader :channel, :params, :viewer

  def initialize(channel:, params:, viewer:, limit:, offset:)
    @channel = channel
    @params = params
    @viewer = viewer
    @limit = limit
    @offset = offset
  end

  def call
    # ********************** Step 1 : Filter the cards *************************
    filter_cards

    # ******************* Step 2 : Sort and order the cards ********************
    sort_and_order_cards

    # *********** Step 3 : Return only limit & offset number of cards **********
    @total = @cards.count
    @cards = @cards.limit(@limit).offset(@offset)
      .select("cards.*, channels_cards.rank")
      .each { |card| card.channel_card_rank = card.rank }

    {
      cards: @cards,
      total: @total,
      new_cards_total: @new_cards_total || 0
    }
  end

  private

  def filter_cards
    # 1. Channel card state. Default: curated
    if params[:channel_card_state].present? && params[:channel_card_state] != 'curated'
      @cards = Card.cards_for_channel_id(channel.id).where(channels_cards: { state: params[:channel_card_state] }).published
    else
      @cards = Card.curated_cards_for_channel_id(channel.id).published
    end

    # 2. Exclude dismissed cards
    @cards = @cards.where.not(id: viewer.dismissed_card_ids)

    # If the viewer is non admin user
    unless viewer.is_org_admin?
      # 3. Check for access restrictions
      @cards = @cards.visible_to_userV2(viewer)
    end

    # 4. Card type and card subtype
    if params[:card_type]&.any?
      if params[:card_subtype]&.any?
        @cards = @cards.where(card_type: params[:card_type], card_subtype: params[:card_subtype])
      else
        @cards = @cards.where(card_type: params[:card_type])
      end
    end

    # 5. UGC
    if params[:ugc]&.to_bool
      @cards = @cards.user_created
    end

    # 6. Language
    if params[:language_filter]
      @cards = @cards.filtered_by_language(viewer.language)
    end

    # 7. Date range
    if params[:from_date].present? && params[:to_date].present?
      from_date = DateTime.parse(params[:from_date]).beginning_of_day
      to_date = DateTime.parse(params[:to_date]).end_of_day

      @cards = @cards.where("cards.created_at >= ? AND cards.created_at <= ?", from_date, to_date)
    end

    # 8. From time
    if params[:last_access_at].present?
      last_access_at = Time.at(params[:last_access_at].to_i).to_datetime.utc
      @new_cards_total = @cards.where('channels_cards.created_at > ?', last_access_at).count
    end

    @cards
  end

  def sort_and_order_cards
    # Default sorting is by `rank` of channel card
    sort_by = params[:sort].present? ? params[:sort].to_sym : :rank

    # Default order is 'DESC'
    order = params[:order].present? ? params[:order] : 'DESC'

    @cards =  case sort_by
              when :rank
                @cards.order("CASE WHEN channels_cards.rank IS NULL THEN 1 ELSE 0 END, channels_cards.rank asc, channels_cards.created_at desc")
              when :published_at
                @cards.order("cards.published_at #{order}")
              when :created_at
                @cards.order("cards.created_at #{order}")
              when :likes_count
                @cards.joins("LEFT JOIN content_level_metrics on cards.id = content_level_metrics.content_id")
                      .where(content_level_metrics: { period: ['all_time', nil], offset: [0, nil] })
                      .order("content_level_metrics.likes_count #{order}")
              when :views_count
                @cards.joins("LEFT JOIN content_level_metrics on cards.id = content_level_metrics.content_id")
                      .where(content_level_metrics: { period: ['all_time', nil], offset: [0, nil] })
                      .order("content_level_metrics.views_count #{order}")
              end
  end
end

# AIM:
#   Fetch content from Sociative matching learning_interests of user.
#   Limit of content is org-specific. Defaults to 5.
#
# WORKFLOW:
#   1. Picks contents from Sociative (in ECL) matching learning_interests.
#   2. Caches these contents.
#   3. Encapsulate contents if it has physical cards. Else, build a new card object for front-end.
#   4. If user bookmarks/completes/dismisses these contents, it will be rejected from final list.
#   5. If user changes their interests, then:
#      5.a new cache is created with new interests hash.
class TodaysLearning::V3 < TodaysLearning::Base
  attr_accessor :cards, :number_of_items, :offset, :options, :organization, :user

  def initialize(user:, opts: {})
    super(user: user, opts: opts)
  end

  def get_all_cards
    _cards = Rails.cache.fetch(cache_key)

    if _cards.blank? || (self.options[:force_cache] == 'true')
      # Delete the cache
      Rails.cache.delete(cache_key)

      # Try fetching cards again
      _cards = get_todays_learning_items

      # Cache if not blank
      if _cards.present?
        Rails.cache.write(cache_key, _cards, expires_in: 1.days)
      end
    end

    _cards
  end

  def get_todays_learning_items
    virtual_data = []
    filters = { learning_interests: learning_topics, exclude_ecl_ids: ecl_ids, offset: self.offset, limit: 100 }

    klass   = EclApi::EclSearch.new(self.organization.id, self.user.try(:id))
    response = klass.todays_learning(filters: filters)

    if response.present?
      virtual_data = response['data']
    end

    virtual_data
  end

  def process
    # Return blank if no learning keywords being set.
    return [] if learning_topics.empty?

    # Fetch contents from Sociative matching learning_topics.
    # Find/Create DailyLearningLog by learning topics.
    self.cards = get_all_cards

    # Do nothing if no contents returned.
    return [] if self.cards.empty?

    # Support pagination
    setup_pagination
    self.cards = self.cards.slice(self.offset, self.number_of_items)

    # Encapsulates contents as Card representing them as virtual and physical cards.
    _cards = format_results_as_cards

    # Rejects ecl cards if bookmarked/completed/dismissed by a user or are trashed.
    self.cards = _cards.reject { |card| self.ecl_ids.include?(card.ecl_id) || self.trashed_ecl_ids.include?(card.ecl_id) }

    # Pick physical cards appropriately.
    self.cards = reload_cards_from_db

    self.cards
  end

  def reload_cards_from_db
    ecl_card_ids = self.cards.map { |card| card.ecl_id }

    existing_cards = Card.where(organization_id: self.organization.id, ecl_id: ecl_card_ids).index_by(&:ecl_id)
    results = []

    self.cards.each do |card|
      if existing_cards[card.ecl_id].present?
        results << existing_cards[card.ecl_id]
      else
        results << card
      end
    end

    results
  end

  private
    def cache_key
      topic_ids = (self.user.learning_topics || []).map{ |x| x['topic_id'] }
      keywords  = topic_ids.collect { |s| s.split('-').first }.compact
      interests_hash = Digest::SHA256.hexdigest keywords.join('|')
      "search_user_daily_learnings/v3/#{self.user.id}/#{Date.today.to_s}/#{interests_hash}"
    end

    def format_results_as_cards
      klass = EclApi::EclSearch.new(self.organization.id, self.user.id)
      cards = klass._format_data_as_cards(self.cards, load_tags: false)

      cards
    end

    def learning_topics
      @learning_topics ||= self.user.learning_topics_name
    end
end

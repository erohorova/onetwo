# STEP 1: Get the list of learning keywords
# STEP 2: Get card ids to exclude
# STEP 3: Get ecl card ids to exclude
# STEP 4: Generate cache key
# STEP 5: Get content
# STEP 6: Depending on the day of the month, set random offsets
# STEP 7: Get cards only for the offset
# STEP 8: Remove excluded cards
# STEP 9: Reload cards from the database. This is necessary since we're caching the search
#   results, so the search routine doesn't get an opportunity to load cards from the DB.
class TodaysLearning::V2 < TodaysLearning::Base
  include SearchConcern

  attr_accessor :current_user, :current_org, :params

  def initialize(user:, opts: {})
    super(user: user, opts: opts)

    self.current_user = self.user
    self.current_org  = self.organization
    self.params       = self.options
  end

  def process
    self.cards = get_all_cards

    setup_pagination

    self.cards = self.cards.slice(self.offset, self.number_of_items)
    self.cards = self.cards.reject{ |card| ecl_ids.include?(card.ecl_id) || trashed_ecl_ids.include?(card.ecl_id) }

    self.cards = reload_cards_from_db

    self.cards
  end

  def get_all_cards
    _cards = Rails.cache.fetch(cache_key)
    if _cards.blank? || (params[:force_cache] == 'true')
      # Delete the cache
      Rails.cache.delete(cache_key)

      # Try fetching cards again
      _cards = get_todays_learning_items(learning_keywords, ecl_ids)

      # Cache if not blank
      if _cards.present?
        Rails.cache.write(cache_key, _cards, expires_in: 3.days)
      end
    end

    _cards
  end

  def get_todays_learning_items(learning_keywords, exclude_ecl_ids)
    # STEP 1: For each of the learning_keywords, get 5 results
    results = learning_keywords.map do |keyword|
      search_params = HashWithIndifferentAccess.new({
        q: keyword,
        exclude_ecl_card_id: exclude_ecl_ids,
        content_type: %w(pathway poll article video insight video_stream course),
        rank: false,
        load_tags: false
      })
      cards, aggs = do_search(search_params)

      cards
    end

    # STEP 2: Rotate and remove duplicates
    all_cards = results.map{|cards| process_results(cards)}
    max_len = all_cards.map{|cards| cards.count}.max || 0

    ([nil] * max_len).zip(*all_cards).flatten.reject{|x| x.nil?}.uniq{|x| x.ecl_id}
  end

  def process_results(cards)
    # 1. Check if we have cards
    cards ||= []

    # 2. Create the index
    card_types_to_cards = cards.group_by {|card| card.card_type }
    max_len = card_types_to_cards.values.map{ |cards| cards.count }.max || 0

    # 3. Create a staggered list
    ([nil] * max_len).zip(*card_types_to_cards.values).flatten.reject{ |card| card.nil? }
  end

  private
    def cache_key
      interests_hash = Digest::SHA256.hexdigest learning_keywords.join('|').slice(0,6)
      "search_user_daily_learnings/#{self.current_user.id}/#{interests_hash}"
    end
end

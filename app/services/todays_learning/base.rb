class TodaysLearning::Base
  attr_accessor :cards, :organization, :user, :number_of_items, :offset, :options, :limit

  def initialize(user:, opts: {})
    self.user    = user
    self.organization = user.organization

    self.options = opts
    self.limit   = opts[:limit]
    self.offset  = opts[:offset]
  end

  def ecl_ids
    @ecl_ids ||= Card.where(id: user_card_ids).where.not(ecl_id: nil).pluck(:ecl_id)
  end

  def trashed_ecl_ids
    @trashed_ecl_ids ||= self.organization.trashed_ecl_ids
  end

  def learning_keywords
    @learning_keywords ||= (self.user.learning_topics || []).map{ |x| x['topic_label'] }
  end

  def learning_nodes
    @learning_nodes ||= (self.user.learning_topics || []).map{ |x| x['topic_name'] }
  end

  def reload_cards_from_db
    existing_cards = Card.where(organization_id: self.organization.id, ecl_id: self.cards.map{ |card| card.ecl_id }).index_by(&:ecl_id)
    result = []

    self.cards.each do |card|
      if existing_cards[card.ecl_id].present?
        result << existing_cards[card.ecl_id]
      else
        result << card
      end
    end

    result
  end

  def setup_pagination
    number_of_items = self.organization.number_of_learning_items
    day_of_the_month = Time.zone.today.day
    number_of_batches = self.cards.count / number_of_items

    if number_of_batches > 0
      todays_offset = (((day_of_the_month - 1) % 10) % number_of_batches) * number_of_items
    else
      todays_offset = 0
    end

    self.offset = todays_offset
    self.number_of_items = number_of_items
  end

  def user_card_ids
    @user_card_ids ||= (self.user.dismissed_card_ids + self.user.bookmarked_card_ids + self.user.completed_card_ids).uniq
  end
end

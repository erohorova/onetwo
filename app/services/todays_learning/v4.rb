# STEP 1: Get the list of learning keywords
# STEP 2: Get card ids to exclude
# STEP 3: Get ecl card ids to exclude
# STEP 4: Generate cache key
# STEP 5: Get content
# STEP 6: Depending on the day of the month, set random offsets
# STEP 7: Get cards only for the offset
# STEP 8: Remove excluded cards
# STEP 9: Reload cards from the database. This is necessary since we're caching the search
#   results, so the search routine doesn't get an opportunity to load cards from the DB.
class TodaysLearning::V4 < TodaysLearning::Base
  include SearchConcern

  attr_accessor :current_user, :current_org, :params

  def initialize(user:, opts: {})
    super(user: user, opts: opts)

    self.current_user = self.user
    self.current_org  = self.organization
    self.params       = self.options
  end

  def process
    self.cards = get_todays_learning_items(learning_nodes, ecl_ids)

    setup_pagination

    self.cards = self.cards.slice(self.offset, self.number_of_items)
    self.cards = self.cards.reject{ |card| ecl_ids.include?(card.ecl_id) || trashed_ecl_ids.include?(card.ecl_id) }

    self.cards = reload_cards_from_db

    self.cards
  end

  def get_todays_learning_items(learning_nodes, exclude_ecl_ids)
    # STEP 1: Get results for all the nodes
    search_params = HashWithIndifferentAccess.new({
      topic: learning_nodes,
      exclude_ecl_card_id: exclude_ecl_ids,
      content_type: %w(pathway poll article video insight video_stream course),
      rank: false,
      type: "todays_learning",
      load_tags: false
    })

    cards, aggs = do_search(search_params)

    cards
  end
end

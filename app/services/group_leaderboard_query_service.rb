# frozen_string_literal: true

class GroupLeaderboardQueryService

  MAX_LIMIT = 9000
  DEFAULT_LIMIT = 500

  # returns an array of records
  #
  # strategy param is either GroupLeaderboardQueryService::MongoStrategy
  # or GroupLeaderboardQueryService::InfluxStrategy
  #
  # Valid opts are:
  #  start_date, end_date (ISO strings)
  #  user_ids, org_ids, group_ids (Arrays of ids)
  #  order, limit (Integers)
  # All opts are optional.
  def self.query(strategy, **opts)
    result = nil
    strategy.instance_exec do
      result = evaluate_query prepare_query(opts)
    end
    result
  end

  # Default methods when strategy doesn't implement methods
  def self.prepare_query(opts);         raise("unimplemented"); end
  def self.evaluate_query(query_input); raise("unimplemented"); end

end
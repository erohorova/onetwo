class ForumUserMapping < ActiveRecord::Base
  validates_uniqueness_of :item_type, scope: [:item_id]
  belongs_to :item, polymorphic: true
end

class ForumMigrationService

  #Migrate user_id of backfilled data
  def run
    conn = ActiveRecord::Base.connection

    #Fetch UserMapping and update mapping item user_id without select
    ForumUserMapping.find_in_batches(batch_size: 100000) do |mapping_batch|
      mapping_batch.each do |mapping|
        column = mapping.item_type == "PrivateGroup" ? "creator_id" : "user_id"
        sql = "UPDATE #{mapping.table_name} SET #{mapping.table_name}.#{column} = #{mapping.org_user_id} WHERE  #{mapping.table_name}.id = #{mapping.item_id}"

        begin
          conn.execute sql  
        rescue Exception => e
          msg = "Unable to update item with user_mappings #{mapping.to_json} with Exception: #{e}"
          log.error msg
          puts msg
        end
      end
    end
  end

  def backfill_forum_user_mappings(model_name, from: nil)
    default_org = Organization.default_org

    #Get list of models with mapping
    mapping_list = get_mapping_list[model_name]

    column = mapping_list[:columns][0]
    polymorphic_name = mapping_list[:polymorphic_name]
    table_name = mapping_list[:table_name]
    model = model_name.constantize
    if from
      model.where("created_at > '#{Date.parse(from)}'").find_in_batches(batch_size: 20000) do |batch|
        populate_mappings_in_batch(batch, column, polymorphic_name, table_name, model_name, default_org)
      end
    else
      model.find_in_batches(batch_size: 20000) do |batch|
        populate_mappings_in_batch(batch, column, polymorphic_name, table_name, model_name, default_org)
      end
    end
  end

  def populate_mappings_in_batch(batch, column, polymorphic_name, table_name, model_name, default_org)
    mappings = []
    batch.each do |item|
      #Create mapping for only forum related records
      if forum_item?(item, polymorphic_name)
        # debugger
        organization = model_name == "ActivityStream" ? item.org : item.organization
        
        #Skip for default org
        if organization && organization != default_org
          existing_user_id = item.send(column)

          # Find the www and org users for the same
          www_user, org_user = find_www_user_and_org_user(existing_user_id, organization)

          # We'll ignore if org_user.id == existing_user_id
          if org_user && org_user.persisted? && (org_user.id != www_user.id)
            attrs = { 
              item_id: item.id,
              item_type: model_name,
              www_user_id: www_user.id,
              org_user_id: org_user.id,
              organization_id: organization.id,
              table_name: table_name
            }

            mappings << ForumUserMapping.new(attrs)
          end
        end
      end
    end
    ForumUserMapping.import(mappings) if mappings.present?
  end

  def forum_item?(item, polymorphic_name)
    polymorphic_name.empty? || %w(Event Post Group Comment ResourceGroup).include?(item.send(polymorphic_name))
  end

  def get_mapping_list
    # post [postable_id, postable_type=Group]
    # comment [commentable_id, commentable_type=Post]
    # approval [approvable_id, approvable_type=Post,Comment]
    # vote [votable_id, votable_type=Post,Comment]
    # stream [item_id, item_type=Post,Comment, Group]
    # group_user [user_id]
    # follow [followable_id, followable_type=Post]
    # report [reportable_id, reportable_id=Post,Comment]
    # notification [notifiable_id, notifiable_type=Post,Comment]
    # file_resource [attachable_id, attachable_type=Post,Comment]
    # activity_stream [streamable_id, streamable_type=Comment]
    # score [user_id]
    # event [user_id]
    # event_user [user_id]
    # group [creator_id]
    # user_preferences

    {
      # Groups
      "PrivateGroup" => {
        table_name: "groups",
        polymorphic_name: "",
        columns: ['creator_id']
      },

      # Group users
      "GroupsUser" => {
        table_name: "groups_users",
        polymorphic_name: "",
        columns: ['user_id']
      },

      # Scores
      "Score" => {
        table_name: "scores",
        polymorphic_name: "",
        columns: ['user_id']
      },

      # Events
      "Event" => {
        table_name: "events",
        polymorphic_name: "",
        columns: ['user_id']
      },

      # Event users
      "EventsUser" => {
        table_name: "events_users",
        polymorphic_name: "",
        columns: ['user_id']
      },

      # Votes
      "Vote" => {
        table_name: "votes",
        polymorphic_name: "votable_type",
        columns: ['user_id']
      },

      # Approval
      "Approval" => {
        table_name: "approvals",
        polymorphic_name: "approvable_type",
        columns: ['user_id']
      },

      # Streams
      "Stream" => {
        table_name: "streams",
        polymorphic_name: "item_type",
        columns: ['user_id']
      },

      # Follows for posts
      "Follow" => {
        table_name: "follows",
        polymorphic_name: "followable_type",
        columns: ['user_id']
      },

      # Report
      "Report" => {
        table_name: "reports",
        polymorphic_name: "reportable_type",
        columns: ['user_id']
      },

      # Notifications
      "Notification" => {
        table_name: "notifications",
        polymorphic_name: "notifiable_type",
        columns: ['user_id']
      },

      # File resource
      "FileResource" => {
        table_name: "file_resources",
        polymorphic_name: "attachable_type",
        columns: ['user_id']
      },

      # Activity streams
      "ActivityStream" => {
        table_name: "activity_streams",
        polymorphic_name: "streamable_type",
        columns: ['user_id']
      },

      # Comment
      "Comment" => {
        table_name: "comments",
        polymorphic_name: "commentable_type",
        columns: ['user_id']
      },

      # Post
      "Post" => {
        table_name: "posts",
        polymorphic_name: "postable_type",
        columns: ['user_id']
      },

      # User Preference
      "UserPreference" => {
        table_name: "user_preferences",
        polymorphic_name: "preferenceable_type",
        columns: ['user_id']
      }
    }

  end

  # User by email or parent user id, will return existing one or create new one in org
  def find_www_user_and_org_user(www_user_id, org)
    www_user = User.find_by(id: www_user_id, organization: Organization.default_org)
    org_user = nil
    if www_user
      org_user = User.find_by(email: www_user.email, organization_id: org.id) if www_user.email
      org_user = User.find_by(parent_user_id: www_user_id, organization_id: org.id) if org_user.nil?
      unless org_user
        begin
          org_user = www_user.clone_for_org(org, 'member')  
        rescue Exception => e
          log.error "Unable to create user for org with id: #{org.id} and email: #{www_user.email}  Exception: #{e}"
        end
      end
    end
    [www_user, org_user]
  end
end
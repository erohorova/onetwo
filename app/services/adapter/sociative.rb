# This class will be responsible for every single API towards sociative.
class Adapter::Sociative
  BASE_URL = Settings.sociative.sociative_url

  def initialize(opt={})
    @organization = Organization.find(opt[:organization_id])
  end

  def establish_connection
    Faraday.new(url: BASE_URL)
  end

  def token
    JWT.encode({}, Settings.sociative.sociative_auth_secret, 'HS256')
  end

  #Channel Programming V2 By Sociative
  def channel_programming(opts)
    begin
      log.info("Channel Programming Sociative call for: #{opts[:scope]}")
      conn = establish_connection
      response = conn.post "/v1/content/set" do |req|
        req.headers['Content-Type'] = 'application/json'
        req.headers['X-Api-Token'] = token
        req.body = opts.to_json
      end
    rescue Exception => e
      log.error "Error communicating with Sociative for channel_programming for #{opts[:scope]} | #{e}"
      return
    end

    if response.success?
      cards = ActiveSupport::JSON.decode(response.body)["cards"]
      if cards.present?
        cards
      else
        return false
      end
    else
      log.error "Invalid response code from Sociative for #{opts[:scope]}: response_code=#{response.status}"
      return
    end
  end

  # todo: use token generated here and pass organization_id in token generation
  def get_taxonomy_topics(token)
    @taxonomy_topic = {}
    conn = establish_connection
    response = conn.get "/taxonomy_keywords/search_topics?q=" do |req|
      req.headers['Content-Type'] = 'application/json'
      req.headers['X-Api-Token'] = token
    end
    taxonomy_topics = ActiveSupport::JSON.decode(response.body)["topics"]
    taxonomy_topics.each do |taxo_topic|
      @taxonomy_topic.merge!(taxo_topic["name"]=>
                             {:name=> taxo_topic["name"],
                              :domain_name => taxo_topic["domain"]["name"],
                              :taxo_id => taxo_topic["taxo_id"],
                              :topic_id => taxo_topic["id"],
                              :domain_id => taxo_topic["domain"]["id"]
                             })

    end
    @taxonomy_topic
  end
end



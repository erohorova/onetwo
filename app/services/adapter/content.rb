# This class will be responsible for every single API call towards ECL / Sociative.
# Every feature fro content REPO will go through this class.
class Adapter::Content

  def initialize(opt={})
    @organization = Organization.find(opt[:organization_id])
  end

  #Channel Programming V2 will always call sociative
  #Main decision of V1/V2 version of programming is in respective Jobs
  #Below is called from ChannelProgrammingJob for V2. This will always serve V2
  def channel_programming(channel_id, opts={})
    channel = Channel.find(channel_id)
    ecl_sources = channel.ecl_sources

    adapter  = Adapter::Sociative.new({organization_id: @organization.id})
    data = adapter.channel_programming(opts)
    if data.present?
      data.each do |card|
        #Calling card connected as response from ecl/sociative is same
        #will pass card json from here and card connecter will not call ecl again
        lxp_card = EclApi::CardConnector.new(organization_id: @organization.id, ecl_id: card["id"]).card_from_ecl_id(card)
        private_source = ecl_sources.detect{|x| x.ecl_source_id == card["source_id"]}.private_content rescue false
        lxp_card.add_card_to_channel(channel, private_source, false) if lxp_card
      end
      return true
    end
    log.warn("No response from Sociative: #{opts[:scope]}")
  end
end
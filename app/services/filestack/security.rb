module Filestack
  class Security
    APP_SECRET = Settings.filestack.app_secret
    DEFAULT_EXPIRY = 15.minutes

    attr_accessor :policy, :signature

    def initialize(call: ["read"], expire_after_seconds: DEFAULT_EXPIRY, org: nil)
      expire_after_seconds = org.filestack_url_expire_after_seconds || expire_after_seconds if org
      generate(call, expire_after_seconds)
    end

    def signed_url(url)
      return nil if url.blank?
      handle = url_path(url)
      "https://cdn.filestackcontent.com/security=p:#{policy},s:#{signature}/#{handle}"
    end

    def self.strip_security_parameters(url)
      handle = URI.parse(url).path.split("/")[-1]
      "https://cdn.filestackcontent.com/#{handle}"
    end

    def self.decode_url(url)
      # STEP 1: decrypt the url
      encrypted_data = URI.parse(url).path.split("/")[-1]
      decrypted_data = Aes.safe_decrypt(encrypted_data, Settings.features.integrations.secret)
      # STEP 2: grab the secured filestack URL
      secure_url = JSON.parse(decrypted_data)['filestack_url']
      handle = URI.parse(secure_url).path.split("/")[-1]
      # STEP 3: Generate the FS's CDN url
      "https://cdn.filestackcontent.com/#{handle}"
    end

    # DESC:
    #   Ensure saving Filestack's raw URL in the table.
    #   If ENV['SECURE_AUTHENTICATED_IMAGES'] is disabled, LXP will receive secured URL with signature and policy.
    #   Example: https://stark.edcasting.co//security=p:eyJjYWxsIjpbImNvbnZlcnQiXSwiZXhwaXJ5IjoxNTUxMTc3NDYwfQ==,s:e1a25fa1.../:handle
    #   If ENV['SECURE_AUTHENTICATED_IMAGES'] is enabled, LXP will receive authenticated URL.
    #   Example: https://stark.edcasting.co/uploads/515b4c0451576e5270f7877b29b9018b:5bb3952...
    # OUTPUT:
    #   https://cdn.filestackcontent.com/:handle
    def self.read_filestack_url(url)
      if url.include?("/security=p:")
        Filestack::Security.strip_security_parameters(url)
      elsif url.include?("/uploads/")
        Filestack::Security.decode_url(url)
      else
        url
      end
    end

    private

    def generate(call, expire_after_seconds)
      policy_json = create_policy_string(call, expire_after_seconds)
      @policy = Base64.urlsafe_encode64(policy_json)
      @signature = OpenSSL::HMAC.hexdigest('sha256', APP_SECRET, policy)
    end

    def create_policy_string(call, expire_after_seconds)
      {
        call: call,
        expiry: Time.now.to_i + expire_after_seconds
      }.to_json
    end

    def url_path(url)
      uri = URI.parse(url)
      uri.path.split("/")[-1]
    end
  end
end

module Filestack
  class ProcessImage
    HEIGHT = 400
    PROCESS_IMAGE_FILESTACK_API = "https://process.filestackapi.com"

     def initialize(url:)
      @url = url
      @signed_url = Filestack::Security.new(expire_after_seconds: 5.minutes).signed_url(@url)
    end

    def resize
      should_resize? ? resize_image : { "url" => @url }
    end

    def self.delete_original_images(handle)
      begin
        conn = Faraday.new(url: Settings.filestack.api)
        conn.basic_auth 'app', Settings.filestack.app_secret
        result = conn.delete do |req|
          req.url "/api/file/#{handle}"
          req.params['key']          = Settings.filestack.api_key
          req.headers['Content-Type'] = 'application/json'
          req.headers['Accept']       = 'application/json'
        end
        if result.success?
          log.warn "Successfully deleted file from filestack with handle: #{handle}"
        else
          log.warn "Failed to delete file from filestack with handle: #{handle} Status: #{result.status}, Response: #{result.body}"
        end
      rescue StandardError => e
        log.warn "Cannot delete file from filestack with handle: #{handle}"
      end
    end

    def self.image_size(handle)
      obj = Filestack::Security.new(call: ["convert"])
      begin
        conn = Faraday.new(url: "https://cdn.filestackcontent.com/")
        result = conn.get do |req|
          req.url "/security=p:#{obj.policy},s:#{obj.signature}/imagesize/#{handle}"
        end
        if result.success?
          parsed_body = JSON.parse(result.body)
        else
          log.warn "Failed to fetch file from filestack with handle: #{handle}, status: #{result.status}, response: #{result.body}"
          false
        end
      rescue StandardError => e
        log.warn "Cannot connect to file from filestack with handle: #{handle}"
        false
      end
    end

    private

    def height_is_greater_than_limit?
      _width, height = FastImage.size(@signed_url)
      height.present? ? (height > HEIGHT) : false
    end

    def is_image?
      !!FastImage.type(@signed_url)
    end

    def is_not_stock_image?
      CARD_STOCK_IMAGE.select { |a| a[:url] == @url }.empty?
    end

    def resize_image
      obj = Filestack::Security.new(call: ["store", "convert"], expire_after_seconds: 5.minutes)
      begin
        conn = Faraday.new(url: "#{Settings.filestack.api}/api/store/S3?key=#{Settings.filestack.api_key}&policy=#{obj.policy}&signature=#{obj.signature}")
        result = conn.post do |req|
          req.params['url'] = PROCESS_IMAGE_FILESTACK_API + "/#{Settings.filestack.api_key}/security=p:#{obj.policy},s:#{obj.signature}/resize=height:#{HEIGHT}/#{@url}"
        end

        if result.success? && result.body !='Invalid Application'
          parsed_body = JSON.parse(result.body)
          parsed_body.slice!('filename', 'size', 'type', 'url')
          parsed_body["mimetype"] = parsed_body.delete("type")
          parsed_body
        else
          log.warn "Failed to process image #{@url}. Status: #{result.status}, Response: #{result.body}"
          { "url" => @url, "status" => 'failed' }
        end
      rescue StandardError => e
        log.warn "Invalid response processing image: #{@url}. Status: #{result.status}, Response: #{result.body}"
        { "url" => @url, "status" => 'failed' }
      end
    end

    def should_resize?
      is_image? && is_not_stock_image? && height_is_greater_than_limit?
    end
  end
end

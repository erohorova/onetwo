module Filestack
  class UpdateCard
    attr_accessor :card, :url, :resized_image_options

    def initialize(card:, url:, resized_image_options:)
      self.card = card
      self.url = url
      self.resized_image_options = resized_image_options.with_indifferent_access
    end

    def save
      return if url == resized_image_options[:url]
      filestack = card.filestack
      filestack_image = filestack.find { |a| a.with_indifferent_access[:url] == url }
      return unless filestack_image.present?
      item_index = filestack.index(filestack_image)
      filestack[item_index] = filestack_image.with_indifferent_access.merge(resized_image_options_with_handle)
      card.update_attribute(:filestack, filestack)
    end

    private

    def resized_image_options_with_handle
      resized_image_options.merge({ handle: resized_image_handle })
    end

    def resized_image_handle
      resized_image_options["url"].split("/")[-1]
    end
  end
end

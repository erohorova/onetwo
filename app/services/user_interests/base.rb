module UserInterests
  class Base
    def initialize(args, user, default_topics)
      @args           = args
      @curent_user    = user
      @default_topics = default_topics
    end

    def update_profile(interests)
      profile = @curent_user.profile
      unless profile
        profile = @curent_user.build_profile
      end

      profile.learning_topics = interests
      unless profile.save
        log.error "Unable to update interest from pwc PwcService for user #{@curent_user.id} with error #{profile.full_errors}"
      end
    end
  end
end
#   B - Business
#   T- Technical

#                                       Type of Skill   Rank
# Business Skills                           B               8
# Project Management and IT Operations      B               7
# Customer Management                       B               6
# Internet Of Things                        T               5
# Networking                                T               4
# Security                                  T               3
# Software and Application development      T               2
# Big Data and Analytics                    T               1


# Case 1  Select the skill for which user has given the minimum rating
# Case 2  If multiple skills have low rating then give priority to technical and then select based on the ranking above
# Case 3  If only business skills have the lowest level, then select the one based on the ranking
# Case 4  If all is advanced, select Big Data and analytics

#Format of PWC  callback http://qa.lvh.me:4000/api/pwc_callback?id=jwtToken&bs=3&cm=3&iot=3&cs=1&sd=1&bd=3&n=3&pm=3
module UserInterests
  class Pwc < Base
    SKILL_MAPPING = {
      "bs"=> {label: "Business Skills", rank: 8},
      "pm"=> {label: "Project Management", rank: 7},
      "cm"=> {label: "Customer Relationship Management (CRM)", rank: 6},
      "iot"=>{label: "Internet of Things", rank: 5},
      "n"=>  {label: "Computer Networking", rank: 4},
      "cs"=> {label: "Cybersecurity", rank: 3},
      "sd"=> {label: "Application Development", rank: 2},
      "bd"=> {label: "Big Data", rank: 1}
    }

    SKILL_LEVEL = [1, 2, 3]

    attr_accessor :args, :curent_user, :default_topics

    def initialize(args, user, default_topics)
      super(args, user, default_topics)
      save_pwc_response(args)
    end

    #Save PWC response before process Learning Interests
    def save_pwc_response(args)
      valid_abbrs.map do |abbr|
        skill = skill_hash(SKILL_MAPPING[abbr][:label])
        if(skill)
          record = @curent_user.pwc_records.where(abbr: abbr).first_or_initialize
          record.skill = skill
          record.level = args[abbr]
          unless record.save
            log.error "Failed to save PWC Record for user with id: #{@curent_user.id} and abbr: #{abbr}"
          end
        else
          log.error "Failed to save PWC Record for user with id: #{@curent_user.id} and abbr: #{abbr} because default topic is not present for this abbr."
        end
      end
    end

    #Get only 8 keys
    def valid_abbrs
       @args.keys & SKILL_MAPPING.keys
    end

    #Search Keys from PWC response to default topics of org
    def skill_hash(label)
      @default_topics["defaults"]&.detect {|skill| skill["topic_label"] == label}
    end

    #Process one learning interests as defined above in comments
    def process
      pwc_record = @curent_user.pwc_records.where(abbr: @args["topic"]).first
      if pwc_record
        skill_label = SKILL_MAPPING[pwc_record.abbr][:label]
        add_skill(skill_label, pwc_record)
      end
    end

    def add_skill(label, pwc_record)
      pwc_record.skill["level"] = pwc_record.level if @curent_user.organization.bia_enabled?
      update_profile([pwc_record.skill])
    end
  end
end

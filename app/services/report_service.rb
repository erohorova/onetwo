require 'rubygems'
require 'zlib'
require 'rubygems/package'
class ReportService
  def generate_csv(rows=[])
    file  = Tempfile.new(['', '.csv'])
    CSV.open(file, 'wb', { encoding: 'utf-8' })  do |csv|
      rows.each do |row|
        csv << row
      end
    end
    file
  end


  def generate_xls(rows=[])
    file = Tempfile.new(['', '.xls'])

    workbook  = WriteExcel.new(file.path)
    worksheet = workbook.add_worksheet

    format = workbook.add_format()
    format.set_bold()

    rows.each_with_index do |row, row_index|
      row.each_with_index do |data, data_index|
        begin
          if row_index.zero?
            worksheet.write_string(row_index, data_index, data, format)
          else
            worksheet.write_string(row_index, data_index, data)
          end
        rescue Exception => e
          log.error e.message
          log.error e.backtrace.inspect
        end
      end
    end

    workbook.close
    file
  end

  def upload_to_s3(file:, bucket_name:, filename:, content_type:)
    s3     = AWS::S3.new
    bucket = s3.buckets[bucket_name]
    s3_object = bucket.objects[filename]
      .write(file: file, acl: :authenticated_read)

    # expires in 30 days
    s3_url = s3_object.url_for(:read, expires: 30 * 24 * 60 * 60, response_content_type: content_type).to_s
    s3_url
  end

  def generate_xls_parts(rows=[])
      # Collection of temp files to be uploaded to s3
      files = []
      # Copy headers to separate variable
      # Will be used when we split data into batches to make separate file for it
      headers = rows[0]

      # Make group of 65535 rows since xls rows limit is 65536
      rows.each_slice(65535).with_index do |batch, index|

        # Ignore first batch which is alteady having header
        if index != 0
          # Add headers as a first row to batch
          batch.unshift(headers)
        end

        file = Tempfile.new(['', '.xls'])

        workbook  = WriteExcel.new(file.path)
        worksheet = workbook.add_worksheet

        row_format = workbook.add_format()
        row_format.set_bold()
        batch.each_with_index do |row, row_index|
          row.each_with_index do |data, data_index|
            if row_index.zero?
              worksheet.write(row_index, data_index, data, row_format)
            else
              worksheet.write(row_index, data_index, data)
            end
          end
        end

        workbook.close
        files << file
      end
      files
    end


  def format_tar_gz(files)
      temp_file = Tempfile.new(['', '.tar.gz'])

      # Open file from temp_file path and give wb file access mode
      File.open(temp_file.path, "wb") do |file|
        Zlib::GzipWriter.wrap(file) do |gz|
          Gem::Package::TarWriter.new(gz) do |tar|
            # Loop through each xls files
            files.each_with_index do |each_file, index|
              # Read file
              file_content = IO.read each_file

              # Add file to tar with max size limit of 50MB
              tar.add_file_simple("#{@file_name}/#{@file_name}_#{index+1}.xls",
                0444, 50.megabytes
              ) do |io|
                io.write(file_content)
              end
            end
          end
        end
      end
      temp_file
    end


  def close_file(file)
      #Close and delete the temp file
      file.close
      file.unlink
    end

  def upload_report_to_s3(file:, bucket_name:, filename:)
      file_format = file.path.split('.').last
      # Since we can' take extension from a multi-period file name we will have to manually assign it
      if file_format == 'gz'
        file_format = 'tar.gz'
      end

      content_type = case file_format
                     when 'xls'
                      'application/vnd.ms-excel'
                     when 'csv'
                      'application/csv'
                     when 'tar.gz'
                      "application/x-compressed"
                     end

      s3     = AWS::S3.new
      bucket = s3.buckets[bucket_name]
      s3_object = bucket.objects["#{filename}.#{file_format}"].write(file: file, acl: :authenticated_read)
      @s3_url = s3_object.url_for(:read,:expires => 15 * 24 * 60 * 60, :response_content_type => content_type ).to_s
      @s3_url
    end
end

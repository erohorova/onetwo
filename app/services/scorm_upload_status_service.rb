class ScormUploadStatusService
  def initialize
    @scorm_cloud = initiate_scorm
  end

  def initiate_scorm
    ScormCloud::ScormCloud.new(Settings.scorm.app_id, Settings.scorm.secret)
  end
  def get_status(scorm_detail)
    begin
      @scorm_cloud.course.get_async_import_result(scorm_detail.token)
    rescue
      scorm_detail.update_columns(status: 'error', message: 'Invalid token')
      log.error("Invalid token for scorm_detail: #{scorm_detail.id}") and return
    end
  end

  def run(card_id)
    card = Card.find_by(id: card_id)
    log.error("No card found for id: #{card_id} while getting scorm upload status") and return unless card.present?
    scorm_detail = card.scorm_detail

    log.info "Scorm upload status is #{scorm_detail.status} for card: #{card_id} and scorm_detail: #{scorm_detail.id}"

    upload_status = get_status(scorm_detail)
    if upload_status[:status] == 'finished'
      # create resource for card, update cards state to published and scrom details
      card = scorm_detail.card
      log.info("Card not found for id: #{card_id}") and return unless card.present?
      card.publish!
      resource = card.resource.update(title: upload_status[:title])
      scorm_detail.update(status: 'finished', message: upload_status[:message])
      log.info("Uploading succesfully completed for card: #{card.id} and scorm_detail: #{scorm_detail.id}")

    elsif upload_status[:status] == 'error'
      # log the error status of scorm_detail table
      scorm_detail.update(status: 'error', message: upload_status[:message] || 'error while uploading')
      card.error!
      log.info("Scorm upload error for card id: #{card.id} with scorm_detail id: #{scorm_detail.id} with message: #{upload_status[:message] || nil}")
    else
      # means upload in still running.
      log.info("Still uploading for scorm_detail: #{scorm_detail.id}")
      GetScormUploadStatusJob.set(wait: Random.rand(5).minutes).perform_later(card_id)
    end
  end
end
# coding: utf-8
# frozen_string_literal: true
# WORKFLOW:
#  
#   When `@options[:invitable]` is FALSE:
#     1. Find if user exists with an email.
#     2. If #1 returns true, then attach groups, channels, custom_fields to this user.
#     3. If #1 returns false, then create user and attach groups, channels, custom_fields to this invitation.

# FEATURES:
#   1. Groups (creates new groups if matching group name is not found)
#   2. Channels (links channels to users)
#   3. Custom fields (create/update custom fields)
#   4. Password
#   5. Picture Url
#

# TODO: Optimised very soon.
require 'csv'

class BulkImportUsersV2
  # MANDATORY: Sequence of attributes
  WHITELISTED_ATTRS = ['first_name', 'last_name', 'email', 'groups', 'picture_url', 'password', 'language']

  def initialize(file_id:, admin_id: nil, options: {})
    @file         = InvitationFile.find_by(id: file_id)
    @organization = @file.invitable
    @options      = options
    @admin        = @organization.users.find_by(id: admin_id) if admin_id

    prepare_attributes
    initialize_status
  end

  def prepare_attributes
    @default_teams = teams_names(ids: @options[:team_ids])

    @custom_fields = @organization.custom_fields
    @abbreviations = @custom_fields.pluck(:abbreviation)
    @headers       = WHITELISTED_ATTRS + @abbreviations
  end

  def initialize_status
    @all_organization_groups = []
    @new_groups = {}
    @old_groups = {}
    @old_groups_count = 0
    @new_groups_count = 0
  end

  def import
    total_csv_users = CSV.parse(@file.data).length - 1
    user_import_counter = 0
    CSV.parse(@file.data, headers: true, header_converters: ->(h) { h.underscorize }) do |row|
      tuple = row.to_hash

      unless valid_email?(tuple['email'].try(:strip))
        total_csv_users -= 1
        next
      end
      user_groups = addable_groups(tuple)
      user_import_status = create_user_import_status(tuple, user_groups)
      if user_import_status.present?
        user_import_counter += 1
        UserImportJob.perform_later(
          user_import_id: user_import_status.id,
          admin_id: @admin,
          user_import_metadata: {total_csv_users: total_csv_users, user_import_counter: user_import_counter}
        )
      end
    end
    track_groups
    parameter = {new_groups_count: @new_groups_count, old_groups_count: @old_groups_count}
    @file.update(parameter: parameter)
  end

  private
  def create_user_import_status(tuple, user_groups)
    # Set additional information of user in UserImportStatus like 'picture_url', 'password', 'language'.
    additional_information = set_additional_information_attributes(tuple)

    UserImportStatus.create(
      invitation_file_id: @file.id,
      first_name: tuple['first_name'],
      last_name: tuple['last_name'],
      email: tuple['email'],
      channel_ids: @options[:channel_ids],
      team_ids: user_groups.values.map(&:id),
      status: UserImportStatus::PENDING,
      custom_fields: tuple.slice(*@abbreviations),
      additional_information: additional_information
    )
  end

  def set_additional_information_attributes(tuple)
    additional_information = {}
    additional_information.merge!(picture_url: tuple['picture_url']) if tuple['picture_url'].present?
    additional_information.merge!(password: tuple['password']) if tuple['password'].present?
    additional_information.merge!(language: tuple['language']) if tuple['language'].present?
    additional_information.merge!(job_title: tuple['job_title']) if tuple['job_title'].present?
    additional_information.merge!(onboarding_options: @options[:onboarding_options]) if @options[:onboarding_options].present?
    additional_information.merge!(default_group: tuple['default_group']) if tuple['default_group'].present?
    additional_information
  end

  # Combines groups from each CSV row.
  # Captures the new and the existing groups and maintain a hash.
  def addable_groups(user_row)
    new_groups_for_user = []
    groups = @default_teams.clone
    group_names = (user_row['groups'] || '').strip.split(',')
    # combine groups and default_group if it present
    group_names |= [user_row['default_group']] if user_row['default_group']
    existing_groups = Team.where(name: group_names, organization_id: @organization.id).select('id, name, organization_id')

    @all_organization_groups += existing_groups.pluck(:name)
    new_group_names = group_names - existing_groups.pluck(:name)

    new_group_names.each do |group_name|
      new_group = Team.create(name: group_name, organization: @organization, description: group_name)
      new_group.add_admin(@admin) if @admin.present?

      @new_groups[group_name] = 0

      new_groups_for_user << new_group
      @all_organization_groups << group_name
    end

    (new_groups_for_user + existing_groups).each { |group| groups[group.name] = group }

    groups
  end

  def teams_names(ids: [])
    @organization
        .teams
        .where(id: ids)
        .select('id, name, organization_id')
        .inject({}) do |hash, value|
      hash[value.name] = value
      hash
    end
  end

  def valid_email?(email)
    valid_email_regex = /\A[\w+\-'çÇéÉâêîôûÂÊÎÔÛàèùÀÈÙëïüËÏÜ.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
    (email =~ valid_email_regex) == 0
  end

  def track_groups
    @all_organization_groups.uniq.each do |group_name|
      if @new_groups[group_name].present?
        @new_groups_count += 1
      else
        @old_groups_count += 1
      end
    end
  end
end

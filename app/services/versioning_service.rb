class VersioningService
  attr_accessor :association_changes, :entity_id, :entity_type,
                :changes, :user_id

  def self.get_klass(entity_type)
    if entity_type == 'card'
      CardHistory
    elsif entity_type == 'resource'
      CardResourceHistory
    else
      raise(ArgumentError, "Wrong entity passed")
    end
  end

  def initialize(entity_type:, entity_id:, changes: {}, association_changes: {}, user_id:)
    self.entity_type = entity_type
    self.entity_id = entity_id
    self.changes = changes
    self.association_changes = association_changes || {}
    self.user_id = user_id
  end

  def create_version
    attributes = updated_attributes.select { |k, v| !v.blank? }
    return if attributes.blank?
    klass.create_new_version(entity_id, user_id, attributes)
  rescue => e
    log.error(e.message)
  end

  private

  def klass
    self.class.get_klass(entity_type)
  end

  def model_field_changes
    changes.each_with_object({}) do |(key, value), h|
      h[key] = value if key.in?(klass::ATTRIBUTES_TO_RECORD)
    end
  end

  def updated_attributes
    attr_changes = model_field_changes.merge(association_changes)
    attr_changes.merge(klass.misc_changes(entity_id, attr_changes))
  end
end

class UserLeaderboardV2Service
  # Note: this class is tested through user_leaderboard_v2_test.rb

  extend SortOrderExtractor
  extend CacheUtil

  USE_CACHING = true
  PRINT_BENCHMARKS = false

  class ParameterError < StandardError; end

  class << self

    def with_benchmark(name, &blk)
      return blk.call unless PRINT_BENCHMARKS
      start_time = Time.now
      res = blk.call
      total_len = (Time.now - start_time).round(2).to_s.rjust(40 -  name.length, ' ')
      puts "#{name}: #{total_len}"
      res
    end

    # =========================================
    # The main method to get the leaderboard results.
    # Caches for 30 minutes based on the params and current user.
    # =========================================
    def get_leaderboard_results(
      current_org:, current_user:, params:, offset:, limit:
    )

      # Validate and set default values for the 'order' and 'order_attr' params
      # These must be directly interpolated into the SQL queries,
      # so this is essential.
      order_attr, order = determine_sort_order(
        order:      params[:order],
        order_attr: params[:order_attr]
      )
      params[:order] = order
      params[:order_attr] = order_attr

      # Build a unique cache key based on the user/org/params
      cache_key = build_cache_key(
        current_org: current_org,
        current_user: current_user,
        params: params
      )

      # Check if the cache key is set. If so, return that value.
      # If not, evaluate the results and store in cache.
      get_results_from_cache(cache_key, expires_in: 30.minutes) do

        filters, filter_params = build_filters(
          params:       params,
          current_user: current_user,
          current_org:  current_org,
          limit:        limit,
          offset:       offset
        )

        queries, my_result_only = build_queries(
          filters:       filters,
          filter_params: filter_params,
          current_user:  current_user,
          current_org:   current_org,
          limit:         limit,
          offset:        offset,
          order:         params[:order],
          order_attr:    params[:order_attr]
        )

        # Return a hash that gets converted to JSON and sent to clients.
        results = with_benchmark("MAIN QUERIES") do
          send_queries(queries, my_result_only)
        end
        finalize_results(
          results: results,
          current_user: current_user
        )
      end

    end

    def get_results_from_cache(key, **opts, &blk)
      return blk.call unless USE_CACHING
      cache_get_set key, **opts, &blk
    end

    # Builds an options hash that is used to construct the leaderboard queries.
    # Depending on the values found here, certain subqueries will be added or
    # not added.
    def build_filter_params(
      params:, current_user:, current_org:, limit:, offset:
    )

      is_manager = current_user.group_sub_admin_authorize?(
        Permissions::MANAGE_GROUP_ANALYTICS
      )

      default_from_date = Time.at(0)
      default_to_date = Time.now

      start_time = (
        parse_date(params[:from_date]) || default_from_date
      ).utc.beginning_of_day.to_i

      end_time = (
        parse_date(params[:to_date]) || default_to_date
      ).utc.end_of_day.to_i

      {
        order_attr:      params[:order_attr],
        order:           params[:order],
        org_id:          current_org.id,
        user_id:         current_user.id,
        is_admin:        current_user.is_admin_user?,
        is_manager:      is_manager,
        show_my_result:  !!params[:show_my_result]&.to_bool,
        start_time:      start_time,
        end_time:        end_time,
        role_names:      params[:roles],
        team_ids:        params[:group_ids],
        expertise_names: params[:expertise_names],
        q:               params[:q],
        active:          params[:active]&.to_bool,
        limit:           limit,
        offset:          offset,
        current_user:    current_user
      }
    end

    # Builds sub-filters.
    # Some might be blank, depending on the request params.
    def build_filters(params:, current_user:, current_org:, limit:, offset:)
      filter_params = build_filter_params(
        params:       params,
        current_user: current_user,
        current_org:  current_org,
        limit:        limit,
        offset:       offset
      )

      filters = {
        user_team_filter:                build_user_team_filter(filter_params),
        role_filter:                     build_role_filter(filter_params),
        team_filter:                     build_team_filter(filter_params),
        user_search_filter:              build_user_search_filter(filter_params),
        active_filter:                   build_active_filter(filter_params),
        current_user_filter:             build_current_user_filter(filter_params),
        positive_score_filter:           build_positive_score_filter(filter_params),
        exclude_from_leaderboard_filter: build_exclude_from_leaderboard_filter(filter_params),
        hide_from_leaderboard_filter:    build_hide_from_leaderboard_filter(filter_params),
      }

      [filters, filter_params]
    end

    # Builds multiple query strings along with their parameters.
    # These can be passed to find_by_sql.
    # Return queries for "total_count", "current_user", and "users"
    def build_queries(
      filters:, filter_params:, current_user:, current_org:, limit:, offset:,
      order:, order_attr:
    )

      query_builder_opts = filters.merge(
        order:      order,
        order_attr: order_attr,
        limit: limit,
        offset: offset
      )

      total_count_query_and_params = [
        build_query(
          "COUNT(id) AS count",
          query_builder_opts.except(:limit, :offset)
        ),
        filter_params
      ]

      current_user_query_and_params = [
        build_query(
          "*",
          query_builder_opts.merge(
            current_user_only:               true,
            active_filter:                   nil,
            user_search_filter:              nil,
            team_filter:                     nil,
            role_filter:                     nil,
            user_team_filter:                nil,
            exclude_from_leaderboard_filter: nil,
            hide_from_leaderboard_filter:    nil,
            positive_score_filter:           nil,
          )
        ),
        filter_params.merge(
          # Don't use offset or limit when querying for current_user data,
          # it will cause the results to be empty.
          offset: 0,
          limit: 1
        )
      ]

      users_query_and_params = [
        build_query(
          "*",
          query_builder_opts
        ),
        filter_params
      ]

      queries = {
        "total_count" => total_count_query_and_params,
        "current_user" => current_user_query_and_params,
        "users" => users_query_and_params
      }

      # If show_my_result=true and limit=1, it's a special case that we only
      # support for backward compatibility.
      # In this case, the "users" key only contains the current user info.
      # In order to accomplish this, we first delete the key before the queries
      # are run. Then we re-add the key later, taking the value from "current_user".
      show_my_result, limit = filter_params.values_at :show_my_result, :limit
      my_result_only = show_my_result && (limit == 1)

      if my_result_only
        queries.delete "users"
      end

      # If show_my_result is not true, don't include the "current_user" key
      if !show_my_result
        queries.delete "current_user"
      end
      [queries, my_result_only]
    end

    # Evaluates all the given query/param sets
    # Queries is a hash mapping name to [query_string, params] array.
    # If my_result_only is true, then we set the "users" key to have the
    # same value as "current_user"
    def send_queries(queries, my_result_only)
      results = queries.each_with_object({}) do |(key, (query, params)), memo|
        with_benchmark(key) do
          memo[key] = User.using(:replica1).find_by_sql([query, params])
        end
      end
      if my_result_only
        results["users"] = results["current_user"]
      end
      results
    end

    # Format results and add some additional data through secondary queries.
    def finalize_results(results:, current_user:)
      with_benchmark "FORMAT RESULTS" do
        format_results!(results)
      end
      with_benchmark "ALL ADDITIONAL DATA" do
        add_additional_user_data!(results, current_user.id)
      end
      results = camelize_results(results)
      # Remove the array wrapper from "current_user"
      if results["currentUser"]
        results["currentUser"] = results["currentUser"][0]
      end
      results
    end

    # Get a cryptographic hash of the params, current org, and current user
    def build_cache_key(current_org:, current_user:, params:)
      meaningful_params = params.slice *%i{
        limit offset from_date to_date active order_attr order
        group_ids roles expertise_names show_my_result q
      }
      [
        "organizations",
        current_org.id,
        "user-leaderboard",
        meaningful_params.merge("current_user_id" => current_user.id).hash
      ].join("/")
    end

    # Fires some other queries to get additional data for the user ids in <results>
    def add_additional_user_data!(results, current_user_id)
      user_ids = results
        .values_at(*%w{current_user users})
        .compact
        .flat_map do |records|
          next unless records
          records.flat_map { |record| record['user']['id'] }
        end
      extra_attrs = {
        channel_count:       -> { get_channel_count_data(user_ids) },
        following:           -> { get_following_data(user_ids, current_user_id) },
        group_count:         -> { get_group_count_data(user_ids) },
        followers_count:     -> { get_followers_count_data(user_ids) },
        expert_topics:       -> { get_expert_topics_data(user_ids) },
        roles:               -> { get_roles_data(user_ids) },
        roles_default_names: -> { get_roles_default_names_data(user_ids) },
      }.each_with_object({}) do |(key, _proc), memo|
        with_benchmark(key) do
          memo[key] = _proc.call
        end
      end
      %w{current_user users}.each do |key|
        results[key]&.each do |record|
          id = record["user"]["id"]
          record["user"].merge!(
            "channel_count"       => extra_attrs[:channel_count][id]       || 0,
            "following"           => extra_attrs[:following][id]           || 0,
            "group_count"         => extra_attrs[:group_count][id]         || 0,
            "followers_count"     => extra_attrs[:followers_count][id]     || 0,
            "expert_topics"       => extra_attrs[:expert_topics][id]       || [],
            "roles"               => extra_attrs[:roles][id]               || [],
            "roles_default_names" => extra_attrs[:roles_default_names][id] || [],
          )
        end
      end
    end

    # Get the list of roles belonging to each user in <user_ids>
    def get_roles_data(user_ids)
      user_ids = [0] if user_ids.empty?
      query = <<-SQL.strip_heredoc
        SELECT
          users.id AS user_id,
          roles.name AS role_name,
          users.organization_role
        FROM users
        LEFT JOIN user_roles ON user_roles.user_id = users.id
        LEFT JOIN roles ON roles.id = user_roles.role_id
        WHERE users.id IN (:user_ids)
      SQL
      query_params = { user_ids: user_ids }
      UserProfile
        .using(:replica1)
        .find_by_sql([query, query_params])
        .map(&:attributes)
        .group_by { |record| record["user_id"] }
        .transform_values do |records|
          roles = records.map { |record| record["role_name"] }
          org_roles = records.map { |record| record["organization_role"] }
          roles.any? ? roles : org_roles
        end
    end

    # Get the list of roles_default_names belonging to each user in <user_ids>
    def get_roles_default_names_data(user_ids)
      user_ids = [0] if user_ids.empty?
      query = <<-SQL.strip_heredoc
        SELECT
          users.id AS user_id,
          roles.default_name AS default_role_name,
          users.organization_role
        FROM users
        LEFT JOIN user_roles ON user_roles.user_id = users.id
        LEFT JOIN roles ON roles.id = user_roles.role_id
        WHERE users.id IN (:user_ids)
      SQL
      query_params = { user_ids: user_ids }
      UserProfile
        .using(:replica1)
        .find_by_sql([query, query_params])
        .map(&:attributes)
        .group_by { |record| record["user_id"] }
        .transform_values do |records|
          roles = records.map { |record| record["default_role_name"] }
          org_roles = records.map { |record| record["organization_role"] }
          roles.any? ? roles : org_roles
        end
    end

    # Get the list of expert_topics belonging to each user in <user_ids>
    def get_expert_topics_data(user_ids)
      query = <<-SQL.strip_heredoc
        SELECT
          user_profiles.user_id AS user_id,
          user_profiles.expert_topics AS expert_topics
        FROM
          user_profiles
        WHERE user_profiles.user_id IN (:user_ids)
      SQL
      query_params = { user_ids: user_ids }
      UserProfile
        .using(:replica1)
        .find_by_sql([query, query_params])
        .index_by(&:user_id)
        .transform_values do |val|
          val.attributes["expert_topics"]
        end
    end

    # Get the followers count for each user in <user_ids>
    # This is the number of users who follow each of the users
    def get_followers_count_data(user_ids)
      query = <<-SQL.strip_heredoc
        SELECT
          follows.followable_id AS user_id,
          COUNT(follows.user_id) AS followers_count
        FROM
          follows
          WHERE follows.followable_type = "User"
          AND follows.followable_id IN (:user_ids)
          AND follows.deleted_at IS NULL
          GROUP BY follows.followable_id
      SQL
      query_params = { user_ids: user_ids }
      Follow
        .using(:replica1)
        .find_by_sql([query, query_params])
        .index_by(&:user_id)
        .transform_values { |val| val.attributes["followers_count"] }
    end

    # Get the groups count for each user in <user_ids>
    # This is the number of groups each user belongs to.
    def get_group_count_data(user_ids)
      query = <<-SQL.strip_heredoc
        SELECT
          teams_users.user_id AS user_id,
          COUNT(teams_users.team_id) AS group_count
        FROM
          teams_users
          WHERE teams_users.user_id IN (:user_ids)
          GROUP BY teams_users.user_id
      SQL
      query_params = { user_ids: user_ids }
      TeamsUser
        .using(:replica1)
        .find_by_sql([query, query_params])
        .index_by(&:user_id)
        .transform_values { |val| val.attributes["group_count"] }
    end

    # Get a bool (as 0 or 1) for each user in <user_ids>
    # This represents whether each user follows the current user.
    def get_following_data(user_ids, current_user_id)
      query = <<-SQL.strip_heredoc
        SELECT
          follows.user_id AS user_id,
          COUNT(follows.followable_id) > 0 AS following
        FROM
          follows
          WHERE follows.followable_type = 'User'
          AND follows.user_id IN (:user_ids)
          AND follows.followable_id = :current_user_id
          AND follows.deleted_at IS NULL
          GROUP BY follows.user_id
      SQL
      query_params = {
        user_ids: user_ids,
        current_user_id: current_user_id
      }
      Follow
        .using(:replica1)
        .find_by_sql([query, query_params])
        .index_by(&:user_id)
        .transform_values { |val| val.attributes["following"] }
    end

    # Get the number of followed channels for each user in <user_ids>
    def get_channel_count_data(user_ids)
      query = <<-SQL.strip_heredoc
        SELECT
          follows.user_id AS user_id,
          COUNT(follows.followable_id) AS channel_count
        FROM
          follows
          WHERE follows.followable_type = 'Channel'
          AND follows.user_id IN (:user_ids)
          AND follows.deleted_at IS NULL
          GROUP BY follows.user_id
      SQL
      query_params = { user_ids: user_ids }
      Follow
        .using(:replica1)
        .find_by_sql([query, query_params])
        .index_by(&:user_id)
        .transform_values { |val| val.attributes["channel_count"] }
    end

    # Slightly reformats the data in the response.
    # Adds the "photo" key for each user and un-nests the "total count" key
    # at the top level.
    def format_results!(results)
      %w{current_user users}.each do |name|
        next unless results[name] # current_user might be absent
        results[name] = nest_user_data(
          results[name].map do |record|
            record
              .attributes
              .except(*%w{
                first_name last_name picture_url avatar_file_name
                avatar_content_type avatar_file_size avatar_updated_at
              })
              .merge("picture" => record.photo(:medium))
              .merge("name" => record.name)
          end
        )
      end
      add_order_by_score!(results)
      results["total_count"] = results["total_count"][0]['count']
    end

    # We add a key to each user which marks their position in the list.
    def add_order_by_score!(results)
      current_user = results["current_user"]&.first
      current_user_id = current_user&.dig("user", "id")
      results["users"].each.with_index do |user_record, idx|
        user_record['user']['order_by_score'] = idx + 1
        if current_user && user_record['user']['id'] == current_user_id
          current_user['user']['order_by_score'] = idx + 1
        end
      end
    end

    # Slightly reformats the user records in the response.
    # Some of the keys are moved from the top level to the nested key "user".
    def nest_user_data(records)
      records.map do |record|
        record['user'] = {}
        %w{
          email name bio handle picture id
        }.each do |attr|
          record['user'][attr] = record.delete attr
        end
        record
      end
    end

    # returns SQL query fragment, or nil if user is admin
    # NOTE: the logic in here is wierd, but I'm just trying to emulate the
    # functionality of the existing endpoint.
    def build_user_team_filter(is_admin:, is_manager:, **_opts)
      return if is_admin
      if is_manager
        # If the current user is a non-admin manager, we include only users who
        # belong to teams the current user is a sub_admin of.
        <<-SQL.strip_heredoc
          AND users.id IN (
            SELECT DISTINCT(teams_users.user_id) FROM teams_users
            WHERE teams_users.team_id IN (
              SELECT DISTINCT(_teams_users.team_id) FROM teams_users AS _teams_users
              INNER JOIN teams ON teams.id = _teams_users.team_id
              WHERE teams.is_everyone_team = false
              AND _teams_users.user_id = :user_id
              AND _teams_users.as_type = 'sub_admin'
            )
          )
        SQL
      else
        # If the current user is a non-admin non-manager, we only include users
        # who belong to teams the current user is a member of.
        <<-SQL.strip_heredoc
          AND users.id IN (
            SELECT DISTINCT(teams_users.user_id) FROM teams_users
            WHERE teams_users.team_id IN (
              SELECT DISTINCT(_teams_users.team_id) FROM teams_users AS _teams_users
              INNER JOIN teams ON teams.id = _teams_users.team_id
              WHERE teams.is_everyone_team = false
              AND _teams_users.user_id = :user_id
            )
          )
        SQL
      end
    end

    # returns SQL query fragment, or nil if no role filter given
    def build_role_filter(role_names:, **_opts)
      role_filter = if role_names&.is_a?(Array)
        <<-SQL.strip_heredoc
          AND roles.name IN (:role_names)
        SQL
      end
    end

    # returns SQL query fragment, or nil if no team ids given
    def build_team_filter(team_ids:, **_opts)
      team_filter = if team_ids&.is_a?(Array)
        team_ids = [0] if team_ids.empty?
        <<-SQL.strip_heredoc
          AND users.id IN (
            SELECT teams_users.user_id FROM teams_users
            WHERE teams_users.team_id IN (:team_ids)
          )
        SQL
      end
    end

    def build_exclude_from_leaderboard_filter(**_opts)
      <<-SQL.strip_heredoc
        AND (
          users.exclude_from_leaderboard = 0
          OR
          users.exclude_from_leaderboard IS NULL
        )
      SQL
    end

    def build_hide_from_leaderboard_filter(**_opts)
      <<-SQL.strip_heredoc
        AND (
          users.hide_from_leaderboard = 0
          OR
          users.hide_from_leaderboard IS NULL
        )
      SQL
    end

    # returns SQL query fragment, or nil if no expertise names / query given
    # queries elasticsearch for a list of user ids.
    def build_user_search_filter(current_user:, q:, expertise_names:, **_opts)
      return if q.blank? && !expertise_names&.is_a?(Array)
      conditions = {}
      if !q.blank?
        conditions.merge!({ q: q})
      end

      if expertise_names&.is_a?(Array)
        conditions.merge!({ filter: { expert_topic_names: expertise_names}, allow_blank_q: true})
      end
      return if conditions.blank?
      conditions.merge! viewer: current_user
      ids = Search::UserSearch.new.search(conditions).results&.map(&:id)
      # MySQL doesn't allow WHERE .. IN on an empty list.
      # WHERE users.id IN () will fail
      # So we add id=0 to the list if it's empty.
      ids = [0] if ids.none?
      <<-SQL.strip_heredoc
        AND users.id IN (#{ids.join(",")})
      SQL
    end

    # returns SQL query fragment, or nil if no 'active' param given
    def build_active_filter(active:, **_opts)
      active_filter = if !active.nil?
        if active
          <<-SQL.strip_heredoc
            WHERE time_spent > 0
          SQL
        else
          <<-SQL.strip_heredoc
            WHERE time_spent = 0
          SQL
        end
      end
    end

    # returns SQL query fragment, but is NOT always applied to final query
    def build_current_user_filter(**_opts)
      <<-SQL.strip_heredoc
        AND user_metrics_aggregations.user_id = :user_id
      SQL
    end

    # filters out users who don't have a total smartbite score > 0
    def build_positive_score_filter(**_opts)
      <<-SQL.strip_heredoc
        HAVING SUM(total_smartbite_score) > 0
      SQL
    end

    # This query is re-used with a couple different configurations
    def build_query(
      *select_columns, current_user_only: false, order:, order_attr:,
      user_team_filter:, role_filter:, team_filter:, positive_score_filter:,
      active_filter:, current_user_filter:, 
      exclude_from_leaderboard_filter:, hide_from_leaderboard_filter:,
      user_search_filter:, limit: nil, offset: nil, **_opts
    )
      # NOTE: cannot use parameterization for order and order_attr.
      #       they must be directly interpolated.
      # NOTE: This was originally written to INNER JOIN teams & teams_users,
      #       but this produced an issue where the result numbers were too high
      #       (they were multiplied by X where X is the number of teams per user).
      #       Instead of WHERE team_id IN (<subquery>),
      #       we're using WHERE user_id IN (<subquery>).
      <<-SQL.strip_heredoc
        SELECT #{select_columns.join(",")} FROM (
          SELECT

            user_metrics_aggregations.organization_id,
            user_metrics_aggregations.user_id AS id,

            users.email,
            users.first_name,
            users.last_name,
            users.bio,
            users.handle,
            users.avatar_file_name,
            users.avatar_content_type,
            users.avatar_file_size,
            users.avatar_updated_at,
            users.picture_url,

            SUM(smartbites_commented_count) AS smartbites_comments_count,
            SUM(smartbites_completed_count) AS smartbites_completed,
            SUM(smartbites_consumed_count)  AS smartbites_consumed,
            SUM(smartbites_created_count)   AS smartbites_created,
            SUM(smartbites_liked_count)     AS smartbites_liked,
            SUM(time_spent_minutes)         AS time_spent,
            SUM(total_smartbite_score)      AS smartbites_score,
            SUM(total_user_score)           AS total_user_score,
            MIN(start_time)                 AS start_time,
            MAX(end_time)                   AS end_time

          FROM user_metrics_aggregations
          INNER JOIN users
          ON users.id = user_metrics_aggregations.user_id
          #{
            <<-_SQL.strip_heredoc if role_filter
              INNER JOIN user_roles
              ON user_roles.user_id = users.id
              INNER JOIN roles
              ON roles.id = user_roles.role_id
            _SQL
          }
          WHERE user_metrics_aggregations.start_time >= :start_time
          AND user_metrics_aggregations.end_time <= :end_time
          AND user_metrics_aggregations.organization_id = :org_id
          AND users.is_suspended = 0
          #{exclude_from_leaderboard_filter}
          #{hide_from_leaderboard_filter}
          #{user_team_filter}
          #{role_filter}
          #{team_filter}
          #{user_search_filter}
          #{current_user_filter if current_user_only}
          GROUP BY id
          #{positive_score_filter}
          ORDER BY #{order_attr} #{order}
          #{"LIMIT :limit" if limit}
          #{"OFFSET :offset" if offset}
        ) AS inner_table #{active_filter}
      SQL
    end

    # Prepare sort order. By default, it's smartbites_score.
    def determine_sort_order(order:, order_attr:)
      extract_sort_order(
        valid_order_attrs: %w(
          smartbites_consumed
          smartbites_created
          smartbites_completed
          smartbites_score
          smartbites_comments_count
          smartbites_liked
          time_spent
        ),
        default_order_attr: 'smartbites_score',
        order: order,
        order_attr: order_attr
      )
    end

    # Returns the parsed date string, or nil if it's not given/parsable
    def parse_date(date_str)
      return unless date_str
      Time.strptime(date_str, "%m/%d/%Y")
    rescue ArgumentError, TypeError
      raise UserLeaderboardV2Service::ParameterError.new(
        "cant parse from_date or to_date param"
      )
    end

    # The client expects camel case results.
    def camelize_results(results)
      # Camelize the top level keys
      new_results = camelize_hash_keys(results)
      %w{currentUser users}.each do |key|
        next unless new_results[key]
        new_results[key] = new_results[key].map do |user|
          camelize_hash_keys(user).merge(
            "user" => camelize_hash_keys(user["user"])
          )
        end
      end
      new_results
    end

    def camelize_hash_keys(hash)
      hash.transform_keys do |key|
        key.camelize(:lower)
      end
    end

  end

end

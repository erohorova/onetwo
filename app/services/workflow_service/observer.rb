# The single responsibility of this service is, when input attrs change execute all eligible workflows against the
# given input data.
#
# Input data can be:
# 1. User::ActiveRecord_Relation
# 2. UserCustomField::ActiveRecord_Relation
# 3. Array of users (User bulk upload)

module WorkflowService
  class Observer
    MODEL_NAME_MAPPING = {
      UserCustomField: 'User',
      User: 'User'
    }.freeze

    def initialize(organization:, input_data_ids:, changes: nil, model_name:)
      @changes = changes # changes is required only to decide whether to execute a workflow or not.
      @model_name = MODEL_NAME_MAPPING[model_name.to_sym]
      @workflows  = organization.workflows.enabled
      @input_data_ids = input_data_ids
      @current_workflow = nil
    end

    def perform
      @workflows.each do |workflow|
        @current_workflow = workflow
        run_workflow if should_run_workflow?
      end
    end

    private

    def run_workflow
      WorkflowProcessorJob.perform_later(
        workflow_id: @current_workflow.id,
        model_name: @model_name,
        input_data_ids: @input_data_ids,
        changes: @changes
      )
    end

    # This method will decide whether to execute the workflow or not
    # First check is to compare wf_type with the model of the object changed
    # Secondly, we check if the changed columns is included either in rules OR output.
    # If First AND Second both conditions are satisfied then we execute the workflow.
    def should_run_workflow?
      return false unless @current_workflow.is_enabled
      #This will be the case when processing user bulk upload batch
      return true if @changes.nil? && @current_workflow.wf_type == @model_name
      validate_changes &&
        @current_workflow.wf_type == @model_name &&
        (changes_in_rules? || changes_in_output?)
    end

    def validate_changes
      @changes.is_a?(Hash)
    end

    def changes_in_rules?
      (@changes.keys & WorkflowService::Rules.new(rules: @current_workflow.wf_rules).fetch_columns).present?
    end

    def changes_in_output?
      (
        @changes.keys &
          WorkflowService::Output.new(workflow: @current_workflow).unique_identifier
      ).present?
    end
  end
end

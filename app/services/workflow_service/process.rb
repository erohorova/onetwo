# The single responsibility of this service is to Process a workflow.
# Will take input data from workflow record if not specified explicitly.
# Input data will be specified in case of source attr. change, user bulk import, etc.

module WorkflowService
  class Process

    def initialize(workflow:, input_data: nil, changes: nil)
      @workflow = workflow
      @changes = changes
      @org = @workflow.organization
      @source_service = WorkflowService::Source.new(organization: @org, input_source: @workflow.wf_input_source)
      @rules_service  = WorkflowService::Rules.new(rules: @workflow.wf_rules)
      @output_service = WorkflowService::Output.new(workflow: @workflow, changes: @changes)
      @input_data = input_data.present? ? input_data : fetch_input_data
    end

    # This will run one workflow against input_data.
    def run
      before_workflow_processing

      limit = @org.workflow_input_batch_size
      offset = 0

      while true do
        input_data_batch = @input_data.limit(limit).offset(offset)
        filtered_data = filter(input_data_batch) if input_data_batch.length > 0

        if filtered_data&.present?
          filtered_data.each do |data|
            remove_user_from_team(data: data, filtered: true) if @changes
            @output_service.execute(data)
          end
        else

          if @changes
            input_data_batch.each do |data|
              remove_user_from_team(data: data, filtered: false)
            end
          end
        end

        break if input_data_batch.length < limit
        offset = offset + limit
      end
      
      after_workflow_processing
    end

    private

    # Fetch input data depending upon +wf_input_source+.
    def fetch_input_data
      @source_service.data
    end

    def filter(input_data_batch)
      unless @workflow.wf_rules
        if @workflow.output_dynamic? && @workflow.wf_column_names.present? && @workflow.input_source == 'User'
          return input_data_batch
          .select(:id, :first_name, :last_name, :organization_id)
          .includes(assigned_custom_fields: :custom_field)
          .where(custom_fields: {abbreviation: @workflow.wf_column_names})
          .where.not(user_custom_fields: {value: [nil, '']})
        else
          return input_data_batch
        end
      end
      @rules_service.filter(data: input_data_batch, input_type: @workflow.wf_input_source[0][:source], org: @org)
    end

    def before_workflow_processing
      @workflow.update_attributes(status: Workflow::PROCESSING)
    end

    def after_workflow_processing
      @workflow.update_attributes(status: Workflow::COMPLETED)
      WorkflowAfterProcessJob.perform_later(
        model_name: @output_service.model_name,
        ids: @output_service.output_ids
      )
    end

    def changes_in_wf_column_names?
      (@changes.keys & @workflow.wf_action['value']['wf_column_names']).present?
    end

    def remove_user_from_team(data:, filtered:)
      type = @workflow.wf_action['type']
      if type == WorkflowService::Output::CREATE_DYNAMIC && @workflow.wf_output_result
        team_name = @output_service.fetch_dynamic_value(data: data, changes: @changes)

        team = @output_service.model_name.safe_constantize.find_by(id: @workflow.wf_output_result['ids'], name: team_name)
      end

      if type == WorkflowService::Output::CREATE_CUSTOM && @workflow.wf_output_result
        team_name = @workflow.wf_action['value']['custom_column_name']
        team = @output_service.model_name.safe_constantize.find_by(id: @workflow.wf_output_result['ids'], name: team_name)
      end

      if type == WorkflowService::Output::EXISTING
        team = @output_service.model_name.safe_constantize.find_by(id: @workflow.wf_action['value']['id'])
      end

      if team
        team_user = team.teams_users.find_by(user_id: data.id, as_type: 'member')
        if team_user
          if @workflow.output_dynamic?
            team_user.destroy unless filtered
            team_user.destroy if filtered && changes_in_wf_column_names?
          end
          if [WorkflowService::Output::CREATE_CUSTOM, WorkflowService::Output::EXISTING].include?(type)
            team_user.destroy unless filtered
          end
        end
      end

    end
  end
end

# frozen_string_literal: true

# This service is responsible for filtering the input_source according to the rules of the workflow.
module WorkflowService
  class Rules
    CONDITIONS = %w[and or equals not_equals lt gt lte gte must_exist must_not_exist like].freeze

    RULES_OPERATOR = {
      and: '&&',
      equals: '==',
      gt: '>',
      gte: '>=',
      lt: '<',
      lte: '<=',
      must_exist: '!=',
      must_not_exist: '==',
      not_equals: '!=',
      or: '||'
    }.freeze

    RULES_SQL_OPERATOR = {
      and: 'AND',
      equals: '=',
      must_exist: 'IS NOT NULL',
      must_not_exist: 'IS NULL',
      not_equals: '!=',
      or: 'OR'
    }.freeze

    def initialize(input_data: nil, rules:)
      @rules = rules
      @columns_involved = []
    end

    def sql_parser
      return '' unless @rules

      conditions = []
      @rules.values[0].each do |condition|
        workflow_rule = condition.keys.first
        sql_rule = RULES_SQL_OPERATOR[workflow_rule.to_sym]
        if %w[and or equals not_equals].include?(workflow_rule)
          conditions << "(" +
            "cf.abbreviation = '#{condition[workflow_rule].keys.first}' AND " +
            "(" +
              "ucf.value #{sql_rule} '#{condition[workflow_rule].values.first}' #{(sql_rule == '!=') ? "AND ucf.value != ''" : ""}" +
            ")" +
          ")"
        end
        if %w[must_exist must_not_exist].include?(workflow_rule)
          conditions << "(" +
            "cf.abbreviation = '#{condition[workflow_rule]}' AND " +
            "(" +
              "ucf.value #{sql_rule} #{(sql_rule == 'IS NOT NULL') ? "AND ucf.value != ''" : "OR ucf.value = ''"}" +
            ")" +
          ")"
        end
      end
      conditions.join(' OR ')
    end

    def fetch_columns(rules: @rules)
      return [] unless rules.is_a?(Hash)

      rules.each do |key, value|
        fetch_column(key, value)
      end
      @columns_involved.uniq
    end

    def filter(data:, input_type:, org:)
      return data unless @rules

      if input_type == 'User'
        data = data
          .joins("join custom_fields cf on cf.organization_id = users.organization_id and users.organization_id = #{org.id} and cf.organization_id = #{org.id}")
          .joins("left join user_custom_fields ucf on cf.id = ucf.custom_field_id and ucf.user_id = users.id and users.organization_id = #{org.id}")
          .where(sql_parser)
          .group('users.id')

        return data.having("count(users.id) = #{@rules.values.flatten.length}") if @rules.keys[0] == 'and'
        data if @rules.keys[0] == 'or'
      end
    end

    # def filter(data)
    #   if data.class.to_s == 'User'
    #     data.assigned_custom_fields.each do |user_custom_field|
    #       data.define_singleton_method(user_custom_field.custom_field.abbreviation) do
    #         user_custom_field.value
    #       end
    #     end
    #   end
    #
    #   return data if @rules.blank?
    #
    #   main_condition = RULES_OPERATOR[@rules.keys[0].to_sym]
    #   checks = []
    #
    #   @rules.values[0].each do |condition|
    #     condition.each do |rule, column|
    #       checks << equality_check(data, rule, column) if %w[equals not_equals].include?(rule)
    #       checks << null_check(data, rule, column) if %w[must_exist must_not_exist].include?(rule)
    #       checks << comparison_check(data, rule, column) if %w[lt gt lte gte].include?(rule)
    #     end
    #   end
    #
    #   return data if main_condition == '&&' && checks.all? { |check| check }
    #   data if main_condition == '||' && checks.any? { |check| check }
    # end

    private

    def fetch_column(key, value)
      value.each { |v| fetch_columns(rules: v) }  if value.class.to_s == 'Array'
      fetch_columns(rules: value)                 if value.class.to_s == 'Hash'
      @columns_involved << (CONDITIONS.include?(key.to_s) ? value.to_s : key.to_s) if value.class.to_s == 'String'
    end

    # # rule: equals(==), not_equals(!=)
    # # {country: India}
    # def equality_check(data, rule, column)
    #   column_value(data, column.keys[0]).send(RULES_OPERATOR[rule.to_sym], column.values[0])
    # end
    #
    # # rule: must_exist(!=), must_not_exist(==)
    # # column: first_name
    # def null_check(data, rule, column)
    #   column_value(data, column).send(RULES_OPERATOR[rule.to_sym], nil)
    # end
    #
    # # rule: less_than i.e. lt(<)
    # # column: {age: 40}
    # def comparison_check(data, rule, column)
    #   value = column_value(data, column.keys[0])
    #   value.present? && value.to_i.send(RULES_OPERATOR[rule.to_sym], column.values[0].to_i)
    # end
    #
    # def column_value(data, column)
    #   data.send(column) if data.respond_to?(column)
    # end
  end
end
#= Dumping all output screen logic here

# 1. There should be a relation between the chosen source and the output screen
#    E.g: If the input source is User than the possible output can be
#         i. group creation(Group)
#         ii. following a channel(Channel)
#         iii. assigning existing cards(Card) and many more
#   Similarly for Group as an input source there can corresponding set of possible outputs that are eligible/applicable to Group.

#= Roughly like below hash:
# {
#     User: ['Group', 'Channel', 'Card'],
#     Card: ['User', 'Group', 'Channel'],
#     Group: ['Channel']
# }

# 2. There should be a relation between the output selected and the +metadata(features)+ available from those.
#    E.g: If Group is selected as output then list features such as
#         i. is_private
#         ii. is_mandatory
#         iii. description
#         From the above +metadata(features)+ the user can select the behaviour of that +metadata(features)+.
#
#= Roughly like below hash:
# {
#     User: ['first_name', 'last_name', 'bio', 'handle'],
#     Card: ['title', 'description', 'card_type'],
#     Group: ['description', 'is_private', 'is_mandatory']
# }
#
# Also, this will perform output operations on LXP DB.

module WorkflowService
  class Output
    CREATE_DYNAMIC = 'create_dynamic'.freeze
    CREATE_CUSTOM = 'create_custom'.freeze
    EXISTING = 'existing'.freeze

    attr_reader :model_name, :output_ids

    def initialize(workflow:, changes: nil)
      @workflow = workflow
      @org = @workflow.organization
      initialize_wf_action(@workflow.wf_action)
      @changes = changes
      @output_ids = []
    end

    # This method is written only as per the given requirement i.e. group creation
    def execute(data)
      # Depending upon `type` use `value` find or create on the `entity`.
      # Also, process its metadata(features) if specified by end-user.
      type = @wf_action[:type]

      create_dynamic(data) if type == CREATE_DYNAMIC
      create_custom(data) if type == CREATE_CUSTOM
      existing(data) if type == EXISTING
    end

    def unique_identifier
      if @wf_action['type'] == CREATE_DYNAMIC
        [@wf_action['value']['wf_column_names']].flatten.reject(&:blank?)
      else
        []
      end
    end

    def create_dynamic(data)
      metadata = @wf_action[:metadata]
      create_by_value = fetch_dynamic_value(data: data)
      output = model_name.safe_constantize.find_or_initialize_by(@column_name => create_by_value, organization_id: @org.id)
      assign_metadata(output, metadata)
      output.save

      if output.persisted?
        output.add_user(data, skip_flush_cache: true, skip_team_indexing: true)
        output_ids.concat([output.id])
        save_output([output.id]) if output.previous_changes.keys.include?('id')
      end
    end

    def create_custom(data)
      custom_column_name = @value[:custom_column_name]
      metadata = @wf_action[:metadata]

      output = model_name.safe_constantize.find_or_initialize_by(@column_name => custom_column_name, organization_id: @org.id)
      assign_metadata(output, metadata)
      if output.save
        save_output([output.id]) if output.previous_changes.keys.include?('id')
        output.add_user(data, skip_flush_cache: true, skip_team_indexing: true)
        output_ids.concat([output.id])
      end
    end

    def existing(data)
      id = @value[:id]
      output = model_name.safe_constantize.find_by(@column_name => id, organization_id: @org.id)
      if output
        output.add_user(data, skip_flush_cache: true, skip_team_indexing: true)
        output_ids.concat([output.id])
      end
    end

    # returns old team name if changes is present else
    # returns team name
    def fetch_dynamic_value(data:, changes: nil)
      if @type == CREATE_DYNAMIC
        value = ''
        wf_column_names = @value[:wf_column_names]
        suffix = @value[:suffix]
        should_return = false

        wf_column_names.each do |wf_column_name|
          record = data.assigned_custom_fields&.find {|e| e.custom_field.abbreviation == wf_column_name}
          if (changes && changes[wf_column_name]&.first) || record&.value
            value += (changes && changes[wf_column_name]&.first) || record.value
            value += ' '
          else
            should_return = true
            break # we will return if the data doesn't have a value present for any of wf_column_names.
          end
        end

        return if should_return

        value += suffix
        value.strip
      end
    end

    private

    def assign_metadata(output, metadata)
      metadata.each do |attr, attr_value|
        output.send(attr.to_s + '=', attr_value)
      end
      output.description = 'Group created from workflow' if output.description.blank?
    end

    def save_output(output_ids)
      wf_output = @workflow.wf_output_result&.with_indifferent_access
      old_output_ids = (wf_output && wf_output[:ids]) || []
      output_ids.concat(old_output_ids)
      @workflow.update_attributes(
        wf_output_result: {
          type: model_name,
          ids: output_ids
        }
      )
    end

    def initialize_wf_action(wf_action)
      return unless wf_action
      @wf_action = @workflow.wf_action.with_indifferent_access
      @value = @wf_action[:value]
      @type  = @wf_action[:type]
      @model_name  = @wf_action[:entity]
      @column_name = @value[:column_name]
    end
  end
end
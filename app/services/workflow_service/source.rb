# This service will provide input fields for given source type. It can be DB or CSV.

module WorkflowService
  class Source
    INPUT_SOURCE = {
      user: {
        columns: [
          {column_name: 'first_name', type: 'string', label: 'First Name'},
          {column_name: 'last_name', type: 'string', label: 'Last Name'}
        ].freeze
      }
    }

    def initialize(type: nil, input_source: nil, organization:)
      @type = type
      @org  = organization
      @input_source = input_source
    end

    # This method gives all the fields for that source type
    def fields
      @type == 'user' ? user_input_fields : []
    end

    def preview_data
      data = []
      if @type == 'user'
          users = @org.users.includes(assigned_custom_fields: :custom_field).order(id: :desc).limit(3)
          users.each do |user|
            user_data = {id: user.id, first_name: user.first_name, last_name: user.last_name}
            custom_fields = []
            user.assigned_custom_fields.each do |ucf|
              custom_fields << {abbreviation: ucf.custom_field.abbreviation, display_name: ucf.custom_field.display_name, value: ucf.value}
            end
            user_data.merge!(custom_fields: custom_fields)
            data << user_data
          end
      end
      data
    end

    def user_input_fields
      custom_fields = []
      columns = custom_fields << INPUT_SOURCE[@type.to_sym][:columns]
      @org.custom_fields.each do |cf|
        abbr = cf.abbreviation.to_s
        custom_fields << { column_name: abbr, type: 'string', label: cf.display_name }
      end
      columns.flatten
    end

    def data
      # Get source data with the selected fields. It can be DB or CSV.
      if @input_source[0][:source] == 'User'
        @org
          .users
          .not_suspended
          .not_anonymized
          .select(:id, :first_name, :last_name, :organization_id)
          .includes(assigned_custom_fields: :custom_field)
      end
    end

  end
end
require 'csv'

module Translation
  class ExportUntranslatedStrings
    def initialize(key:)
      @redis_key = key
      @aws_credentials = Aws::Credentials.new(Settings.aws_access_key_id, Settings.aws_secret_access_key)
      @s3 = Aws::S3::Resource.new(region: 'us-east-1', credentials: @aws_credentials)
      @bucket = @s3.bucket(Settings.translation.s3_bucket) # load bucket name from env
      @hash_key = RedisHashKey.new(key: @redis_key)
    end

    def export
      values = []
      @languages = @hash_key.key_exists? ? @hash_key.fields : []
      @languages.each do |lang|
        values << RedisHashKey.new(key: @redis_key, field: lang).get_array_values
      end

      values = values.flatten.reject(&:blank?).uniq

      LANGUAGES.values.each do |language|
        obj = @bucket.object("translations/#{language}/edcast_translation.csv")
        begin
          csv_string = obj.get.body.string
          csv_body = append_to_csv_string(csv_string, values)
          obj.put(body: csv_body, acl: "public-read", content_type: "text/csv") if csv_body.present?
          @hash_key.clear
        rescue Aws::S3::Errors::NoSuchKey => e
          log.error "CSV file not present for language: #{language}, values: #{values}, error message: #{e}"
        rescue Exception => e
          log.error "Exception raised while exporting untranslated string for language: #{language}, values: #{values}, error message: #{e}"
        end
      end
    end

    private

    def append_to_csv_string(csv_string, values)
      arr = CSV.parse(csv_string)
      before_insert_count = arr.count
      eng_strings = arr.map { |a| a[0] }
      values.each do |a|
        arr << [a, ""] unless a.in?(eng_strings)
      end
      return nil if before_insert_count == arr.count
      CSV.generate { |csv| arr.each { |a| csv << a } }
    end
  end
end

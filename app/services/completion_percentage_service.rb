#This service is responsible for all calculations related to completed_percentage when user perform action "Mark As Complete" on card,pathway,or journey.
class CompletionPercentageService
  TOTAL_COMPLETED_PERCENTAGE = 100

  #When smartcard or pathway is completed, this will update completed percentage of all related pathways and journeys which are started.
  def self.set_completed_percentage(current_user, card, recalculate = false)
    cover_ids = get_cover_ids(card)
    if cover_ids.present?
      completions = get_user_content_completions(current_user, cover_ids, recalculate)
      completions.each do |user_content_completion|
        update_completed_percentage(user_content_completion, current_user)
      end
    end
  end

  #return cover_ids of pathways/journeys of which a card/pathway is a part of.
  def self.get_cover_ids(card)
    if card.pack_card? || !card.journey_card?
      card.pack_ids | card.journey_ids_for_card
    end
  end

  #return all completed or started pathways/journeys of current user.
  def self.get_user_content_completions(current_user, cover_ids, recalculate)
    return current_user.user_content_completions.where(completable_id: cover_ids).includes(:completable) if recalculate
    current_user.user_content_completions.where("completable_id in (?) and completed_percentage != (?)", cover_ids, TOTAL_COMPLETED_PERCENTAGE).includes(:completable)
  end 

  #Set updated completed_percentage of pathway/journeys related to completed card
  def self.update_completed_percentage(user_content_completion, current_user)
      card = user_content_completion.completable
      if card.pack_card?
        pack_card_ids = card.pack_cards.pluck(:id)
        user_content_completion.completed_percentage = calculate_pathway_percentage(card, pack_card_ids, current_user) if pack_card_ids.length > 0
      elsif card.journey_card?
        user_content_completion.completed_percentage = calculate_journey_percentage(card, current_user)
      end
      user_content_completion.save
  end

  #Calculation for Pathway completion percentage
  def self.calculate_pathway_percentage(card, pack_card_ids, current_user, round_off = false)
    completed_count = current_user.user_content_completions.where(completable_id: pack_card_ids, completed_percentage: 100).count
    calculate_completed_percent(completed_count, pack_card_ids.length, round_off)
  end

  #Calculation for Journey completion percentage
  def self.calculate_journey_percentage(card, current_user)
    completed_value = 0
    journey_pathways = card.journey_pathways
      journey_pathways.each do |pathway|
        pack_card_ids = pathway.pack_cards.pluck(:id)
        completed_value += calculate_pathway_percentage(pathway, pack_card_ids, current_user, true)
      end
      (calculate_completed_percent(completed_value.to_f/100, journey_pathways.length)).to_i
  end
  
  def self.calculate_completed_percent(completed_count, length, round_off = false)
    return 0 if completed_count.zero?
      value = (completed_count * 100 / length)
      value = 98 if(value == 100 && round_off == false)
      value
  end 

  #Reset completed_percentage for pathway/journey(which are started and not completed) when card is added/removed from pathway/journey
  def self.reset_completed_percentage(cover_id)
    completions = UserContentCompletion.where("completable_id in (?) and completed_percentage != (?)", cover_id, TOTAL_COMPLETED_PERCENTAGE).includes(:user , :completable)

    if completions.present?
      completions.each do |user_content_completion|
        user = user_content_completion.user
        update_completed_percentage(user_content_completion, user)
      end
    end
  end
end

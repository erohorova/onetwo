class AutoRecommendCardService
  attr_accessor :user, :options, :max_count, :topics, :card_types, :learning_topics, :learning_topics_cards

  #  Arguments for options:
  #  {
  #    max_count: 100,
  #    topics: [Array of topics] # if no topic then use user learning topc,
  #    card_types: ['pack'] # by default pack card,
  #    max_assign_count: 100
  #  }
  #
  def initialize(user_id, options = {})
    @user       = User.find_by(id: user_id)
    @options    = options
    @max_count  = @options[:max_count] || 100
    @topics     = @options[:topics] || @user.learning_topics_name
    @card_types = @options[:card_types] || ['pack']
    @max_assign_count =  @options[:max_assign_count] || 100

    # TODO:
    # Use min_assign_count for minimum assignments per topic in future
    # @min_assign_count = @topics&.length.to_i + 3
  end

  def recommended_cards
    if @user && @topics.present?
      filter_params = {
        card_type: @card_types,
        state: 'published',
        taxonomy_topics: @topics,
        exclude_ids: skip_card_ids_for_learning
      }

      search_data = Search::CardSearch.new.search(
        nil,
        @user.organization,
        filter_params: filter_params,
        viewer: @user,
        limit: @max_count,
        offset: 0,
        sort: :updated,
        load: true
      )

      search_data.results
    else
      []
    end
  end

  def skip_card_ids_for_learning
    @skip_card_ids_for_learning ||= @user.skip_card_ids_for_learning
  end

  # Description:
  #
  # Pick up cards by mapping taxonomy name and level of user learning interest with
  # card taxonomy topics and level, collect cards and assign to user
  #
  # @learning_topics = [
  #   { name: 'Taxonomy Path', level: 1, level_name: 'beginner' },
  #   { name: 'Taxonomy Path', level: 2, level_name: 'intermedoate' }
  # ]
  #
  # @learning_topics_cards = { topic_name_1: [Cards], topic_name_2: [Cards] }
  #
  def recommended_cards_based_on_user_interest_and_level
    @learning_topics = @user.learning_topics_name_with_level || []
    @learning_topics_cards = {}

    @user && @learning_topics.each do |topic|
      learning_topic_card_ids = []
      if @learning_topics_cards.values.present?
        learning_topic_card_ids = (@learning_topics_cards.values.flatten.uniq || []).map(&:id)
      end

      filter_params = {
        card_type: @card_types,
        state: 'published',
        taxonomy_topic_with_level: topic,
        exclude_ids: skip_card_ids_for_learning | learning_topic_card_ids
      }
      search_data = Search::CardSearch.new.search(
        nil,
        @user.organization,
        filter_params: filter_params,
        viewer: @user,
        offset: 0,
        limit: @max_count,
        sort: :updated,
        load: true
      )
      @learning_topics_cards[topic[:name]] = search_data.results
    end
  end

  def auto_assign_recommended_cards_based_on_user_interest_and_level
    recommended_cards_based_on_user_interest_and_level
    #
    #  TODO: (future implementation thought using @min_assign_count)
    #
    #  In favour of smart logic and smart assignment for each topic.
    #  Get minimum assginment cards for each topic
    #  and push the cards for each topic and assign to user
    #
    #  cards = []
    #  total_cards = @learning_topics_cards.values.flatten.compact.uniq.count || 0
    # 
    #  if total_cards > 0
    #    @learning_topics_cards.each do |topic, val|
    #      cards << val[0...@min_assign_count]
    #    end
    #  end
    #
    #  cards = cards.flatten.compact.uniq
    #
    #  if total_cards < @min_assign_count
    #    cards << recommended_cards
    #  end
    #
    cards = @learning_topics_cards.values.flatten.compact.uniq || []

    if cards.present?
      auto_assign_recommended_cards(cards.sample(@max_assign_count))
    end
  end

  def auto_assign_recommended_cards_to_user
    if @user.organization.bia_enabled?
      auto_assign_recommended_cards_based_on_user_interest_and_level    
    else
      auto_assign_recommended_cards
    end
  end

  def auto_assign_recommended_cards(cards = nil)
    cards = recommended_cards if cards.blank?

    if cards.present?
      organization = @user.organization
      assignor     = organization.admins.first # pick first admin as assignor
      cards        = Card.where(id: cards.map(&:id))

      # filter out cards that are not visible to user only for non admin users
      cards = cards.visible_to_userV2(@user) unless @user.is_org_admin? || cards.empty?

      # skip email notifications for these assignments
      # scheduled notifications are not eligible as due_at is nil
      cards.each do |card|
        assignment = Assignment.where(assignee: user, assignable: card).first_or_initialize
        if assignment.save
          team_assignment = TeamAssignment.where(assignor: assignor, assignment: assignment).first_or_initialize
          team_assignment.skip_email_notifications = true
          team_assignment.save
        end
      end
    end
  end
end

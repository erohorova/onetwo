class UserInfo::Provider
  PROVIDERS = %w(linkedin facebook twitter google office365 salesforce salesforceusers salesforcedevelopers salesforcepartners saml org_oauth lxp_oauth internal_content)

  def self.set(auth:, is_authenticable:, provider:, user:)
    if PROVIDERS.include?(provider)
      provider_klass = scope_for(provider: provider)
      provider_klass.new(auth: auth, user: user).create(is_authenticable: is_authenticable)

      return true
    else
      log.error "Unknown provider: #{provider}"
      return false
    end
  end

  private
    def self.scope_for(provider:)
      provider_string = (provider + '_identity_provider').classify
      UserInfo::IdentityProviders.const_get(provider_string)
    end
end

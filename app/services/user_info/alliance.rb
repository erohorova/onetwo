###################################################################################################
# AIM:
#   1. When Okta is enabled:
#     1.a. Users signing up with email-password must be served to Okta first.
#     1.b. Depending on the response, create user with identity provider in EdCast accordingly.
#   2. When Okta is not enabled:
#     2.a. Find or create a user by email.

# ATTRIBUTES:
#   1. `login` is the unique identifier in Okta for a user.
#   2. `email` is the user's email address
#   3. `id` is the primary key maintained by Okta

# WORKFLOW:
#   When Okta is enabled:
#     1. Finds a user in Okta.
#        1.a If user do not exists, create one.
#     2. Look up user in EdCast by:
#        2.a Okta's user `login` attribute.
#        2.b If 2.a do not return anything, look up user by `email` attribute.
#     3. Create identity provider with `id` returned by Okta.
#   When Okta is not enabled:
#     1. Find a user matching email.
#     2. If #1 returns a user, throw an error of `User already exists`.
#     3. If #1 do not return a user, create one.
#     4. No identity_provider is created.
###################################################################################################
class UserInfo::Alliance
  attr_accessor :errors
  attr_reader :organization, :user_params, :opts

  def initialize(organization:, user_params: {}, opts: {})
    @opts         = opts
    @organization = organization
    @user_params  = user_params
    @errors       = ActiveModel::Errors.new(base: Hashie::Mash.new(user_params))
    @federated_lookup = @organization.okta_enabled?
  end

  def account
    if @federated_lookup
      User.find_by(organization: self.organization, federated_identifier: @federated_user['profile']['login']) ||
        User.find_by(organization: self.organization, email: @federated_user['profile']['email'])
    else
      User.find_by(organization: self.organization, email: @user_params[:email])
    end
  end

  # Source of truth remains Okta.
  # Two-way communication.
  # 1. This finds or create a user in Okta.
  # 2. Accordingly, find or create a user in EdCast.
  # Fix: EP-13581
  # 3. Not creating Identity Provider here as this method is used only when user is signing up with email and password.
  #    And therefore, the user should be able to change his/her password.
  def connect
    return unless valid?

    if @federated_lookup
      @federated_user = federate

      if @federated_user.nil?
        self.errors.add(:remote_user, 'User creation failed in Okta')
        return
      end
    end

    prepare_account
    if @user.nil?
      self.errors.add(:user, "#{@user.full_errors}")
      return
    end

    @user
  end

  def federate
    Okta::InternalFederation.new(organization: self.organization).user(user_params)
  end

  def message
    self.errors.messages.values.flatten.to_sentence
  end

  def prepare_identity_provider
    provider = LxpOauthProvider.find_or_initialize_by(user: @user, uid: @federated_user['id'])
    provider.auth_info = @federated_user
    provider.save
  end

  def prepare_account
    user_params.except!('username')

    @federated_lookup && federated_identifier = @federated_user&.dig('profile', 'login')

    @user = account || self.organization.users.new(federated_identifier: federated_identifier)
    if @user.new_record?
      @user.password_reset_required = user_params['password_reset_required'] || false
      @user.is_complete = false
      @user.platform = self.opts[:platform]
      @user.is_active = self.organization.is_active?
      @user.organization_role =  'member'
      @user.assign_attributes(user_params)

      self.opts[:skip_confirmation_email] && @user.skip_confirmation!

      @user.save
    end
  end

  # Just provisions a user to Okta.
  # One-way communication.
  # 1. This finds or create a user in Okta.
  # 2. Link Identity Provider for this user.
  # 3. If EdCast's user is already present in Okta, return the user as it is. This assumes #NO-OP.
  def provision(user:)
    return user if user.federated_identifier.present?

    @user           = user
    @federated_user = federate

    if @federated_user.nil?
      self.errors.add(:remote_user, 'User creation failed in Okta')
      return
    end

    @user.update_column(:federated_identifier, @federated_user['profile']['login'])

    @user
  end

  def valid?
    user = User.new(organization: self.organization)
    user.assign_attributes(self.user_params)
    user.valid?

    if user.errors.any?
      self.errors.add(:user, "#{user.errors.messages.values.flatten.join(' and ')}")
      return false
    else
      true
    end
  end
end

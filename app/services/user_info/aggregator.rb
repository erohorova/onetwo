# Reusable class to create user, email and identity_provider at once

class UserInfo::Aggregator
  attr_accessor :authenticable, :complete_registration,
    :organization, :error, :user, :user_info, :user_id

  # Invoke this class with following parameters:
  #   organization = #<Organization id: 1...>
  #   user_info    = <#Omniauth ...> || {}
  #   user_id      = 1 | NIL

  # Explanation:
  # User is created using following attributes:
  # Refer method `#construct_user`
  # email           => user_info[:info][:email]
  # first_name      => user_info[:info][:first_name]
  # image           => user_info[:info][:image]
  # last_name       => user_info[:info][:last_name]
  # organization_id => user_info[:organization_id]

  # Identity Provider is created using following attributes:
  # Refer method `#set_provider`
  # expires_at => user_info[:credentials][:expires_at]
  # klass      => user_info[:provider]
  # secret     => user_info[:credentials][:secret]
  # token      => user_info[:credentials][:token]
  # uid        => user_info[:uid]

  # Activities:
  # 1. Find existing IdentityProvider and locate User.
  # 2. If no data from #1, create IdentityProvider and User.
  # 3. Mark new user as Inactive if organization is inactive.
  # 4. Prevent further activity if user is suspended or inactive.
  # 5. If new user registers on www.edcast.com, sends account confirmation email.
  # 6. Update identity_providers info respectively.

  def initialize(organization:, user_id: nil, user_info: {})
    self.organization = organization
    self.user_info    = user_info
    self.user_id      = user_id
  end

  def aggregate
    self.user = construct_user

    unless self.user
      self.error = "Your account is not yet confirmed"
      return
    end

    if self.user.is_suspended? || !self.user.is_active?
      self.error = 'Invalid sign on credentials'
      return
    end

    unless self.user.valid?
      self.error = "Errors while saving user: #{self.user.attributes} with #{self.user.full_errors}"
      return
    end

    self.user.fill_missing_data(auth: user_info)

    self.complete_registration = self.user.is_complete

    set_provider
  end

  private
    def construct_user
      if self.user_id
        _user = locate_existing_user
        self.authenticable = nil
      else
        _user = User.get_or_create_from_social(
          auth_hash: user_info.merge!(organization_id: organization.id))
        self.authenticable = true
      end

      _user
    end

    def locate_existing_user
      _uid = CustomCipher.new.decrypt(context: self.user_id)
      User.find(_uid)
    end

    def provider
      self.user_info[:provider].gsub('_access_token','')
    end

    def set_provider
      UserInfo::Provider.set(
        auth:             self.user_info,
        is_authenticable: self.authenticable,
        provider:         provider,
        user:             self.user )
    end
end

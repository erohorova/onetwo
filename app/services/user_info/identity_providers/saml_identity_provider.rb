module UserInfo::IdentityProviders
  class SamlIdentityProvider < BaseIdentityProvider
  	def provider_klass
      SamlProvider
    end
  end
end

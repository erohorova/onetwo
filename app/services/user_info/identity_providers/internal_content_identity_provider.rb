module UserInfo::IdentityProviders
  class InternalContentIdentityProvider < BaseIdentityProvider
    def provider_klass
      InternalContentProvider
    end
  end
end

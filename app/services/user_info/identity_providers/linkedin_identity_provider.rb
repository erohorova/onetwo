module UserInfo::IdentityProviders
  class LinkedinIdentityProvider < BaseIdentityProvider
    def provider_klass
      LinkedinProvider
    end

    def create(is_authenticable:)
      super do |provider|
        provider.token_scope = Settings.linkedin.scope
      end
    end
  end
end

module UserInfo::IdentityProviders
  class SalesforceIdentityProvider < BaseIdentityProvider
    def provider_klass
      SalesforceProvider
    end
  end
end

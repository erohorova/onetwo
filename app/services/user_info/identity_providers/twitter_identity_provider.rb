module UserInfo::IdentityProviders
  class TwitterIdentityProvider < BaseIdentityProvider
    def provider_klass
      TwitterProvider
    end
  end
end

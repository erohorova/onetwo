module UserInfo::IdentityProviders
  class SalesforcedevelopersIdentityProvider < BaseIdentityProvider
    def provider_klass
      SalesforcedeveloperProvider
    end
  end
end

module UserInfo::IdentityProviders
  class OrgOauthIdentityProvider < BaseIdentityProvider
    def provider_klass
      OrgOauthProvider
    end
  end
end

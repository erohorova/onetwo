module UserInfo::IdentityProviders
  class LxpOauthIdentityProvider < BaseIdentityProvider
    def provider_klass
      LxpOauthProvider
    end
  end
end

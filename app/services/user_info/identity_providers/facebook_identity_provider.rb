module UserInfo::IdentityProviders
  class FacebookIdentityProvider < BaseIdentityProvider
    def provider_klass
      FacebookProvider
    end
  end
end

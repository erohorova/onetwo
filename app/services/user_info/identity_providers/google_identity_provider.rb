module UserInfo::IdentityProviders
  class GoogleIdentityProvider < BaseIdentityProvider
    def provider_klass
      GoogleProvider
    end

    def create(is_authenticable:)
      super do |provider|
        provider.token_scope = Settings.google.scope
      end
    end
  end
end

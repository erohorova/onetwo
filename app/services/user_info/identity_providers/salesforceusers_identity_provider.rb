module UserInfo::IdentityProviders
  class SalesforceusersIdentityProvider < BaseIdentityProvider
    def provider_klass
      SalesforceuserProvider
    end
  end
end

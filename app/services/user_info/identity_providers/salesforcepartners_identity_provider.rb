module UserInfo::IdentityProviders
  class SalesforcepartnersIdentityProvider < BaseIdentityProvider
    def provider_klass
      SalesforcepartnerProvider
    end
  end
end

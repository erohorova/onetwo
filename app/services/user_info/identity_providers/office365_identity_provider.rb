module UserInfo::IdentityProviders
  class Office365IdentityProvider < BaseIdentityProvider
    def provider_klass
      Office365Provider
    end
  end
end

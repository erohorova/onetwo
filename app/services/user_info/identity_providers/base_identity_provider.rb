module UserInfo::IdentityProviders
  class BaseIdentityProvider
    attr_accessor :auth, :user

    def initialize(auth:, user:)
      self.auth = auth
      self.user = user
    end

    def create(is_authenticable:)
      credentials = self.auth[:credentials]
      provider = self.provider_klass.find_or_initialize_by(uid: self.auth[:uid], user: self.user)

      if (extra = self.auth[:extra]).present?
        extra_info = extra[:raw_info].try(:to_h) || {}
        provider.auth_info = extra_info.merge(credentials.slice(*['id_token']))
      end

      provider.auth  = is_authenticable if is_authenticable
      provider.token = credentials[:token]
      provider.secret = credentials[:secret]
      provider.refresh_token = credentials[:refresh_token]
      provider.source_id = self.auth.info.source_id rescue ''

      if expires_at = credentials[:expires_at]
        provider.expires_at = Time.at(expires_at)
      end

      yield(provider) if block_given?

      provider.save!
    end

    def provider_klass
      raise NotImplementedError
    end
  end
end

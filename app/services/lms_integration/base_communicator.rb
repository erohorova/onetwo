class LmsIntegration::BaseCommunicator
  attr_accessor :response, :response_data, :error

  def establish_connection
    Faraday.new(url: lms_api_base_url)
  end

  def headers
    {
      "X-Api-Token" => token,
      "Content-Type" => "application/json",
      "Accept" => "application/json",
    }
  end

  def token
    JWT.encode(payload, jwt_shared_key, 'HS256')
  end

  def jwt_shared_key
    Settings.features.integrations.secret
  end

  def lms_api_base_url
    Settings.lms.api_endpoint
  end

  def success?
    @response.success?
  end

  def error
    @response_data = ActiveSupport::JSON.decode(@response.body)
    (@response_data["error"] || @response_data["message"]).try(:humanize)
  end

end

class LmsIntegration::XapiCredential < LmsIntegration::BaseCommunicator
  attr_accessor :organization

  def initialize(record_id)
    @xapi_credential  = ::XapiCredential.find_by(id: record_id)
    @organization     = @xapi_credential.organization
  end

  def sync_xapi_credentials
    begin
      establish_connection.post do |f|
        f.url "/api/v1/sources/sync_xapi_credentials"
        f.headers = headers
        f.body = xapi_credentials_params.to_json
      end
    rescue => e
      log.error "LMS Xapi sync failed for Organization #{@organization.id}: #{e.message}"
    end
  end

  def xapi_credentials_params
    {
      organization_id:  @xapi_credential.organization_id,
      lrs_end_point:    @xapi_credential.end_point,
      lrs_login_key:    @xapi_credential.lrs_login_key,
      lrs_password_key: @xapi_credential.lrs_password_key,
      lrs_api_version:  @xapi_credential.lrs_api_version
    }
  end

  def payload
    {
      organization_id: @organization.id,
      name: @organization.name
    }
  end
end

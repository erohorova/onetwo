class LmsIntegration::UserSettings < LmsIntegration::BaseCommunicator
  attr_accessor :user, :organization

  def initialize(user_id)
    @user         = User.find_by(id: user_id)
    @organization = @user.organization
  end

  def setup_user
    begin
      establish_connection.post do |f|
        f.url "/api/v1/user_settings"
        f.headers = headers
        f.body = user_settings_params.to_json
      end
    rescue => e
      log.error "LMS User sync failed for User #{@user.id}: #{e.message}"
    end
  end

  def user_settings_params
    {
      name: @user.name,
      email: @user.email,
      organization_id: @organization.id,
      organization_host_name: @organization.host_name,
      user_id: @user.id,
      additional_info: {
        providers: user_providers,
        custom_fields: get_user_custom_fields
      }
    }
  end

  def get_user_custom_fields
    custom_fields = {}
    @user.assigned_custom_fields.joins(:custom_field).find_each do |user_custom_field|
      custom_fields[user_custom_field.custom_field.abbreviation] = user_custom_field.value
    end
    custom_fields
  end

  def user_providers
    providers_info = {}
    @user.identity_providers.each do |provider|
      providers_info[provider.type] = {
        uid: provider.uid,
        token: provider.token,
        secret: provider.secret,
        auth_info: provider.auth_info
      }
    end
    providers_info
  end

  def payload
    {
      organization_id: @organization.id,
      name: @organization.name,
      user_id: @user_id
    }
  end
end

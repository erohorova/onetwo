# Structure { key: { field: value } }
class RedisHashKey
  def initialize(key:, field: nil, value: nil)
    @key = key
    @field = field
    @value = value
  end

  def clear
    REDIS_CLIENT.del(@key)
  end

  def fields
    key_exists? ? REDIS_CLIENT.hkeys(@key) : []
  end

  def get_array_values
    val = get_value
    JSON.parse(val) rescue [val]
  end

  def key_exists?
    REDIS_CLIENT.hlen(@key) > 0
  end

  def set_as_array
    return set([@value].to_json) unless key_exists?
    vals = get_array_values || []
    unless (@value.in?(vals))
      vals << @value
      set(vals.to_json)
    end
  end

  private

  def set(val)
    REDIS_CLIENT.hset(@key, @field, val)
  end


  def get_value
    key_exists? ? REDIS_CLIENT.hget(@key, @field) : nil
  end
end

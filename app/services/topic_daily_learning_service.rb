class TopicDailyLearningService

  def get_ecl_ids(topic_name, org_id, date)
    ecl_search = EclApi::EclSearch.new(org_id)

    # Gather results
    search_results = ecl_search.search(query:'*', limit:100, offset:0, filter_params: {
      topic: [topic_name]
    })
    search_result_cards = search_results[:results]
    search_result_cards.map{|search_result_card| search_result_card['ecl_id']}
  end
end

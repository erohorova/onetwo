class TimeToWord
  def self.convert(time_in_sec:,duration_format: false)
    min, sec = time_in_sec.divmod(60)
    hour, min = min.divmod(60)
    if duration_format
      duration_in_words =[]
      duration_in_words << "#{hour}h" if hour.present? && hour > 0
      duration_in_words << "#{min}m" if min.present? && min > 0
      duration_in_words.join(" ")
    else
      day, hour = hour.divmod(24)
      mon, day = day.divmod(30)
      year, mon = mon.divmod(12)
      duration_in_words = ""
      if time_in_sec < 60
        duration_in_words = "less than a minute"
      elsif time_in_sec >=60 && time_in_sec < 3600
        duration_in_words = "#{min} Min".pluralize(min)
        duration_in_words += " #{sec} Sec".pluralize(sec) if sec > 0
      elsif time_in_sec >= 3600 && time_in_sec < 86400
        duration_in_words = "#{hour} Hour".pluralize(hour)
        duration_in_words += " #{min} Min".pluralize(min) if min > 0
      elsif time_in_sec >= 86400 && time_in_sec < 2592000
        duration_in_words = "#{day} Day".pluralize(day)
        duration_in_words +=" #{hour} Hour".pluralize(hour) if hour > 0
      elsif time_in_sec >= 2592000 && time_in_sec < 31104000
        duration_in_words = "#{mon} Month".pluralize(mon)
        duration_in_words += " #{day} Day".pluralize(day) if day > 0
      elsif time_in_sec >= 31104000
        duration_in_words = "#{year} Year".pluralize(year)
        duration_in_words += " #{mon} Month".pluralize(mon) if mon > 0
      end

      duration_in_words
    end
  end
end
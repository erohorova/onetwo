class Wallet::ContentFormatter
  def initialize(user:, results:, params: {})
    @service = Wallet::Communicator.new(user: user, params: params.merge!(content_id: results.map(&:id)))

    @results = results
  end

  def format
    products = @service.products

    hashed_products = Hash[products.map { |x| [x['content_id'], x] }]
    @results.each do |record|
      record.wallet = hashed_products[record.id]
    end

    @results
  end
end

class Wallet::Communicator
  APIS = { 'products' => '/api/products/subscriptions' }.freeze

  attr_accessor :params, :user

  def initialize(user:, params: {})
    self.params = params
    self.user   = user
  end

  def products
    results = http_post(api: APIS['products'])

    results['products'] || []
  end

  private
    def api_token
      self.user.wallet_api_token
    end

    def establish_connection
      Faraday.new(url: wallet_api_url)
    end

    def headers
      {
        'X-User-Api-Key' => api_token,
        'Content-Type'   => 'application/json',
        'Accept'         => 'application/json',
      }
    end

    def http_post(api:, params: {})
      response = establish_connection.post do |f|
        f.url api
        f.headers = headers
        f.params  = self.params
      end

      if response.success?
        JSON.parse(response.body)
      else
        log.error "Error communicating to Wallet service: api: #{api}, params: #{params}, user: #{self.user.id}"
        {}
      end
    end

    def wallet_api_url
      Settings.wallet.domain
    end
end

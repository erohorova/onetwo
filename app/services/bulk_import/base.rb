class BulkImport::Base
  def open_connection
    connection = Rails.configuration.database_configuration[Rails.env]
    Mysql2::Client.new(username: connection['username'], password: connection['password'], database: connection['database'], local_infile: true)
  end
end
#############################################################
# DESC:
#   The objective is to import users with miniml information.
# STEPS:
#   1. Create users.
#   2. Create user profiles.
#   3. Initialize onboarding.
#   4. Add users as followers to the channels.
#   5. Add users to the teams.
#   6. Configure users as member in the organization.
#   7. Reindex channels, teams, and users.
#   8. Generate handles.
#############################################################

class BulkImport::Runner < BulkImport::Base
  attr_accessor :status

  # ARGS:
  # {
  #   data: 'first_name,last_name,email,language\nHello,User,hello@user.com,en\n...',
  #   organization_id: 1,
  #   opts: { channels: [10, 11, 12], teams: [1, 2, 3] }
  # }
  #
  def initialize(data:, organization_id:, opts: {})
    @data = data
    @organization_id = organization_id
    @organization = Organization.find_by(id: organization_id)
    @channels = opts[:channels] || []
    @teams = opts[:teams] || []

    @status = { messages: [] }
  end

  def execute
    # Step 1: Collect unique channels that are marked as auto-follow + selected channels during the import.
    prepare_associations

    @status[:messages] << "[#{Time.now}] Start importing..."

    # Step 2: Provision users into the database.
    imported_users = BulkImport::Users.new(data: @data, organization_id: @organization_id)

    # FORMAT: Hash with email as a key and ID as a value.
    # EX: { "sheldon.cooper@example.com" => 1, "leonard@example.com" => 2 }
    @status[:messages] << "[#{Time.now}] #1 Pushing users..."
    imported_users.import
    @status[:messages] += imported_users.messages

    # Step 3: Retrieve all the users for better lookup.
    emails = CSV.parse(@data, headers: true)['email'].map(&:downcase)
    client = open_connection
    raw_entries = client.query("select * from users where email in (#{emails.map(&:inspect).join(', ')}) and organization_id = #{@organization_id}").entries
    users = raw_entries.inject({}){|acc, hash| acc.merge(hash['email'] => hash['id'])}

    # Step 4: Generate profile with the information provided in the CSV. Users are provisioned to the service.
    imported_profiles = BulkImport::Profiles.new(data: @data, organization_id: @organization_id, users: users)
    @status[:messages] << "[#{Time.now}] #2 Configuring profiles..."
    imported_profiles.import
    @status[:messages] += imported_profiles.messages

    # Step 5: Initialize onboarding with the current_step as `profile_setup` and state as `new`. Users are provisioned to the service.
    imported_onboarding = BulkImport::Onboarding.new(organization_id: @organization_id, users: users)
    @status[:messages] << "[#{Time.now}] #3 Initializing onboarding..."
    imported_onboarding.import
    @status[:messages] += imported_onboarding.messages

    # Step 6: Add users to the channels. Users are provisioned to the service.
    if @channels.present?
      imported_follows = BulkImport::Follows.new(organization_id: @organization_id, channels: @channels, users: users)
      @status[:messages] << "[#{Time.now}] #4 Pushing adding followers to channels..."
      imported_follows.import
      @status[:messages] += imported_follows.messages
    end

    # Step 7: Add users to the teams as `member` role. Users are provisioned to the service.
    if @teams.present?
      imported_teams_users = BulkImport::TeamsUsers.new(organization_id: @organization_id, teams: @teams, users: users)
      @status[:messages] << "[#{Time.now}] #5 Pushing to teams..."
      imported_teams_users.import
      @status[:messages] += imported_teams_users.messages
    end

    # Step 8: Configure all the users wit=h the role as `member`. Users are provisioned to the service.
    imported_roles = BulkImport::Roles.new(organization_id: @organization_id, users: users)
    @status[:messages] << "[#{Time.now}] #6 Assigning roles..."
    imported_roles.import
    @status[:messages] += imported_roles.messages

    @status[:messages] << "[#{Time.now}] Done importing..."

    # Step 9: Start generating the handles to each user.
    @status[:messages] << "[#{Time.now}] #7 Start to generate the handles..."
    BulkHandleAssignmentJob.perform_later(min_id: users.values.min, max_id: users.values.max)
    @status[:messages] << "[#{Time.now}] All handle generating jobs enqueued..."

    # Step 10: Reindex the channels, teams, and all the users.
    @status[:messages] << "[#{Time.now}] Starting to reindex..."

    reindex = BulkImport::Reindex.new(users: users.values.flatten.compact, channels: @channels, teams: @teams)
    reindex.execute
    @status[:messages] += reindex.messages
    @status[:messages] << "[#{Time.now}] All reindexing jobs enqueued..."

    self.status.merge!({ users_imported: users.length, channels_added: @channels, teams_added: @teams })
  end

  def prepare_associations
    # Pick auto-follow channels.
    @channels += @organization.channels.auto_followed.pluck(:id)
    @channels.uniq!

    # Pick `everyone` teams.
    @teams    += @organization.teams.everyone_teams.pluck(:id)
    @teams.uniq!
  end
end

require 'csv'

# DESC: This is responsible to bulk insert all the records to the users table.

class BulkImport::Users < BulkImport::Base
  attr_accessor :messages

  # ARGS:
  # {
  #   data: 'first_name,last_name,email,language\nHello,User,hello@user.com,en\n...',
  #   organization_id: 1,
  # }
  #
  def initialize(data: '', organization_id:)
    @data            = data
    @organization_id = organization_id
    @current_time    = Time.now
    @filename = "/tmp/#{organization_id}-#{@current_time.strftime("%Y%m%d%H%M%S%L")}-users.csv"

    @messages = []
  end

  # DESC:
  #   1. Prepare each tuple with the email, first_name, last_name, organization ID, organization_role, status with created_at, updated_at, and confirmed_at set as the current time.
  #   2. Prepare a temporary file.
  #   3. Pass this temporary file to MySQL.
  #   4. Immediately, delete the temporary file too.
  def import
    return nil if @data.blank?

    prepare

    insert

    File.delete(@filename)
  end

  def insert
    @messages << "[#{Time.now}] Inserting users..."

    client = open_connection
    client.query("LOAD DATA LOCAL INFILE '#{@filename}' IGNORE INTO TABLE users FIELDS TERMINATED BY ',' (email, first_name, last_name) SET organization_id = '#{@organization_id}', organization_role = 'member', status = 'active', created_at = '#{@current_time}', updated_at = '#{@current_time}', confirmed_at = '#{@current_time}'");
    client.close
  end

  # DESC: Prepares record into CSV.
  def prepare
    @messages << "[#{Time.now}] Preparing users..."
    rows = CSV.parse(@data, headers: true, header_converters: lambda{ |h| h.underscorize })

    # Write to temporary CSV: Open a file that remains unique by org ID and current time with milliseconds.
    CSV.open(@filename, 'wb') do |csv|
      rows.each do |row|
        tuple = row.to_hash

        # Validation: next if email is blank.
        next if tuple['email'].blank?

        # Select: pick attributes that will be a part of users table.
        entry = [tuple['email'].downcase, (tuple['first_name'].present? ? tuple['first_name'] : nil), (tuple['last_name'].present? ? tuple['last_name'] : nil)]

        csv << entry
      end
    end
  end
end

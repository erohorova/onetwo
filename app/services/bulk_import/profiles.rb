require 'csv'

# DESC: This is responsible to bulk insert all the records to the user_onboardings table.

class BulkImport::Profiles < BulkImport::Base
  attr_accessor :messages

  # ARGS:
  # {
  #   data: 'first_name,last_name,email,language\nHello,User,hello@user.com,en\n...',
  #   organization_id: 1,
  #   users: { 'xmen@wolverine.com' => 10, 'captain@americe.com' => 20 }
  # }
  #
  def initialize(data: '', organization_id:, users:)
    @data            = data
    @users           = users
    @current_time    = Time.now
    @filename = "/tmp/#{organization_id}-#{@current_time.strftime("%Y%m%d%H%M%S%L")}-onboarding.csv"

    @messages = []
  end

  # DESC:
  #   1. Prepare each tuple with the user ID, language with created_at & updated_at set as the current time.
  #   2. Prepare a temporary file.
  #   3. Pass this temporary file to MySQL.
  #   4. Immediately, delete the temporary file too.
  def import
    return nil if @users.blank?

    prepare

    insert

    File.delete(@filename)
  end

  def insert
    @messages << "[#{Time.now}] Inserting user_profiles..."

    client = open_connection
    client.query("LOAD DATA LOCAL INFILE '#{@filename}' IGNORE INTO TABLE user_profiles FIELDS TERMINATED BY ',' (user_id, language, created_at, updated_at)");
    client.close
  end

  # DESC: Prepares record into CSV.
  def prepare
    @messages << "[#{Time.now}] Initializing user_profiles..."

    # Step 1: Creates a hash with email as a key and language as a value.
    profile_data = CSV.parse(@data, headers: true).inject({}) { |_hash, data| _hash.merge!(data['email'].try(:downcase) => data['language'] )   }

    # Step 2: Rejects email with no `language`.
    profile_data.delete_if { |k, v| v.blank? }

    # Step 3: Write to temporary CSV: Open a file that remains unique by org ID and current time with milliseconds.
    CSV.open(@filename, 'wb') do |csv|
      profile_data.each do |email, language|
        # Configure:
        #   1. Set `current_step` to be `profile_setup` i.e `1`.
        #   2. Initialize `status` to be `new`.
        #   3. Track `created_at` and `updated_at`.
        entry = [@users[email], language, "#{@current_time}", "#{@current_time}"]

        csv << entry
      end
    end
  end
end

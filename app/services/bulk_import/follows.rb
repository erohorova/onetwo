require 'csv'

# DESC: This is responsible to bulk insert all the records to the follows table.

class BulkImport::Follows < BulkImport::Base
  attr_accessor :messages

  # ARGS:
  # {
  #   organization_id: 1,
  #   channels: [10, 11, 12],
  #   users: { 'xmen@wolverine.com' => 10, 'captain@americe.com' => 20 }
  # }
  #
  def initialize(organization_id:, channels: [], users:)
    @channels        = channels
    @users           = users
    @current_time    = Time.now
    @filename = "/tmp/#{organization_id}-#{@current_time.strftime("%Y%m%d%H%M%S%L")}-follows.csv"

    @messages = []
  end

  # DESC:
  #   1. Prepare each tuple with the user ID, channel ID with created_at & updated_at set as current time.
  #   2. Prepare a temporary file.
  #   3. Pass this temporary file to MySQL.
  #   4. Immediately, delete the temporary file too.
  def import
    return nil if @users.blank?

    prepare

    insert

    File.delete(@filename)
  end

  def insert
    @messages << "[#{Time.now}] Adding users as followers to channels..."

    client = open_connection
    client.query("LOAD DATA LOCAL INFILE '#{@filename}' IGNORE INTO TABLE follows FIELDS TERMINATED BY ',' (user_id, followable_id, followable_type, created_at, updated_at)");
    client.close
  end

  # DESC: Prepares record into CSV.
  def prepare
    @messages << "[#{Time.now}] Preparing users - channels associations..."

    user_ids = @users.values.flatten

    # Write to temporary CSV: Open a file that remains unique by org ID and current time with milliseconds.
    CSV.open(@filename, 'wb') do |csv|
      @channels.each do |channel|
        user_ids.each do |user_id|
          # Configure:
          #   1. Link `user` to a `channel`.
          #   2. Track `created_at` and `updated_at`.
          entry = [user_id, channel, 'Channel', "#{@current_time}", "#{@current_time}"]

          csv << entry
        end
      end
    end
  end
end

require 'csv'

class BulkImport::Reindex
  attr_accessor :messages

  def initialize(users:, channels:, teams:)
    @channels = channels
    @teams = teams
    @users = users

    @messages = []
  end

  def execute
    @messages << "[#{Time.now}] Reindexing users..."
    @users.in_groups_of(500, false) do |batch|
      BulkReindexJob.perform_later(ids: batch, klass_name: 'User')
    end

    @messages << "[#{Time.now}] Reindexing channels..."
    BulkReindexJob.perform_later(ids: @channels, klass_name: 'Channel')

    @messages << "[#{Time.now}] Reindexing teams..."
    BulkReindexJob.perform_later(ids: @teams, klass_name: 'Team')
  end
end

require 'csv'

# DESC: This is responsible to bulk insert all the records to the teams_users table.

class BulkImport::TeamsUsers < BulkImport::Base
  attr_accessor :messages

  # ARGS:
  # {
  #   organization_id: 1,
  #   teams: [10, 12, 13],
  #   users: { 'xmen@wolverine.com' => 10, 'captain@americe.com' => 20 }
  # }
  #
  def initialize(organization_id:, teams: [], users:)
    @teams           = teams
    @users           = users
    @current_time    = Time.now
    @filename = "/tmp/#{organization_id}-#{@current_time.strftime("%Y%m%d%H%M%S%L")}-teams_users.csv"

    @messages = []
  end

  # DESC:
  #   1. Prepare each tuple with the user ID, team ID, as_type with created_at & updated_at set as the current time.
  #   2. Prepare a temporary file.
  #   3. Pass this temporary file to MySQL.
  #   4. Immediately, delete the temporary file too.
  def import
    return nil if @users.blank?

    prepare

    insert

    File.delete(@filename)
  end

  def insert
    @messages << "[#{Time.now}] Add users to teams as member..."

    client = open_connection
    client.query("LOAD DATA LOCAL INFILE '#{@filename}' IGNORE INTO TABLE teams_users FIELDS TERMINATED BY ',' (team_id, user_id, as_type, created_at, updated_at)");
    client.close
  end

  # DESC: Prepares record into CSV.
  def prepare
    @messages << "[#{Time.now}] Preparing teams - users associations..."

    user_ids = @users.values.flatten

    # Write to temporary CSV: Open a file that remains unique by org ID and current time with milliseconds.
    CSV.open(@filename, 'wb') do |csv|
      @teams.each do |team|
        user_ids.each do |user_id|
          # Configure:
          #   1. Set `current_step` to be `profile_setup` i.e `1`.
          #   2. Initialize `status` to be `new`.
          #   3. Track `created_at` and `updated_at`.
          entry = [team, user_id, 'member', "#{@current_time}", "#{@current_time}"]

          csv << entry
        end
      end
    end
  end
end

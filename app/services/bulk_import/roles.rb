require 'csv'

# DESC: This is responsible to bulk insert all the records to the user_roles table.

class BulkImport::Roles < BulkImport::Base
  attr_accessor :messages

  # ARGS:
  # {
  #   organization_id: 1,
  #   users: { 'xmen@wolverine.com' => 10, 'captain@americe.com' => 20 }
  # }
  #
  def initialize(organization_id:, users:)
    @organization    = Organization.find_by(id: organization_id)
    @users           = users
    @current_time    = Time.now
    @filename = "/tmp/#{organization_id}-#{@current_time.strftime("%Y%m%d%H%M%S%L")}-roles.csv"

    @messages = []
  end

  # DESC:
  #   1. Prepare each tuple with the user ID, language with created_at & updated_at set as the current time.
  #   2. Prepare a temporary file.
  #   3. Pass this temporary file to MySQL.
  #   4. Immediately, delete the temporary file too.
  def import
    return nil if @users.blank?

    @member_id = @organization.member_role.id

    prepare

    insert

    File.delete(@filename)
  end

  def insert
    @messages << "[#{Time.now}] Configuring users as members..."

    client = open_connection
    client.query("LOAD DATA LOCAL INFILE '#{@filename}' IGNORE INTO TABLE user_roles FIELDS TERMINATED BY ',' (user_id, role_id)");
    client.close
  end

  # DESC: Prepares record into CSV.
  def prepare
    @messages << "[#{Time.now}] Preparing users - roles associations..."

    user_ids = @users.values.flatten

    # Write to temporary CSV: Open a file that remains unique by org ID and current time with milliseconds.
    CSV.open(@filename, 'wb') do |csv|
      user_ids.each do |user_id|
        entry = [user_id, @member_id]

        csv << entry
      end
    end
  end
end

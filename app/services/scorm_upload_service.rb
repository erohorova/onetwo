require 'open-uri'

class ScormUploadService
  def initialize
    @scorm_cloud = initiate_scorm
  end

  def initiate_scorm
    ScormCloud::ScormCloud.new(Settings.scorm.app_id, Settings.scorm.secret)
  end

  def run(card_id, course_id)
    log.info "Uploading to scorm for card id #{card_id}"
    card = Card.find_by(id: card_id)
    path = get_upload_path(card)
    if card.filestack.first[:size] < 16000000 # 15.258789 MB
      # do sync import
      synchronous_import(course_id, path, card)
    else
      # do async import
      async_import(course_id, path, card)
    end
  end

  def get_upload_path(card)
    secured_filestack_url = Filestack::Security.new(expire_after_seconds: 1.day).signed_url(card.filestack.first[:url])
    #NOTE:
    #open(url) methods returns TempFile class object if the download is large, it puts it into a file to save memory;
    #otherwise it keeps it in memory with a StringIO.
    stream = open(secured_filestack_url)

    if stream.instance_of? StringIO
      @file = convert_into_temp_file(stream)
    else
      @file = stream
    end
    upload_file(get_upload_token, @file)
  end

  def upload_file token, file
    @scorm_cloud.upload.upload_file(token, file)
  end

  def get_upload_token
    @scorm_cloud.upload.get_upload_token
  end

  def convert_into_temp_file string_io_obj
    Tempfile.new.tap do |file|
        file.binmode
        IO.copy_stream(string_io_obj, file)
        string_io_obj.close
        file.rewind
      end
  end

  def create_scorm_resource(import_response, card, course_id, publish=true)
    resource = card.build_resource(title: import_response[:title], type: 'Article',
                url: "#{card.organization.home_page}/api/scorm/launch?course_id=#{course_id}")
    card.publish if import_response[:warnings].blank? && publish
    if card.save
      log.info "Uploaded succesfully for resource #{resource.id}"
    else
      log.warn "Upload failed for card id #{card.id} with warnings #{import_response[:warnings]}"
    end
  end

  def synchronous_import(course_id, path, card)
    log.info("Doing synchronous upload for card: #{card.id}")
    import_response = @scorm_cloud.course.import_course(course_id, path)
    create_scorm_resource(import_response, card, course_id)
  end

  def async_import(course_id, path, card)
    log.info("Doing a-sync upload for card: #{card.id}")
    import_response = @scorm_cloud.course.import_course_async(course_id, path)
    create_scorm_resource(import_response, card, course_id, publish=false)
    add_upload_details(import_response, card)
  end

  def add_upload_details(import_response, card)
    scorm_detail = card.build_scorm_detail(token: import_response[:token], status: 'running', message: nil)
    if scorm_detail.save
      log.info "Scorm details created for card: #{card.id}"
      log.info "Triggering upload status job for card: #{card.id}"
      GetScormUploadStatusJob.perform_later(card.id)
    else
      log.error("Creation of scorm details failed for card: #{card.id}")
    end
  end
end

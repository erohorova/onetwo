class SyncCloneAssociationsService

  def initialize(opts)
    @parent_card = Card.find_by_id(opts[:parent_card_id])
    @opts = opts
  end

  def sync_card_association(cloned_card)
    @cloned_card = cloned_card
    send("sync_#{@opts[:sync_association_type].underscorize}")
  end

  private
  def sync_card
    changed_attributes = @parent_card.attributes.with_indifferent_access.slice(*Card::CLONE_SYNC_REQUIRED_ATTRIBUTES)
    @cloned_card.assign_attributes(changed_attributes)

    # check for resource change of parent card
    if (parent_resource = @parent_card.resource).present?
      if (cloned_resource = @cloned_card.resource).blank? || (parent_resource.url != cloned_resource.url)
        new_cloned_resource = parent_resource.deep_clone(except: :user_id).save
        @cloned_card.resource_id = new_cloned_resource.id
      end
    end
    @cloned_card.save
  end

  # Called when parent card quiz options are updated
  def sync_quiz_question_option
    # first check if clone card quiz has attempted or not, if yes then don't sync
    unless @cloned_card.quiz_question_attempts.exists?
      parent_card_options = @parent_card.quiz_question_options.to_a
      clone_card_options = @cloned_card.quiz_question_options.to_a
      begin
        ActiveRecord::Base.transaction do
          if parent_card_options.length >= clone_card_options.length # added options/updated options
            add_new_quiz_options(parent_card_options, clone_card_options)
          else # removed options from parent card
            remove_stale_quiz_options(parent_card_options, clone_card_options)
          end
        end
        reindex_cloned_card
      rescue ActiveRecord::RecordInvalid => e
        log.error "Error in syncing quiz options for card: #{@parent_card.id} and clone card: #{@cloned_card.id}. message: #{e.message}"
      end
    end
  end

  # Adds new quiz options in clone card
  def add_new_quiz_options(parent_card_options, clone_card_options)
    parent_card_options.each do |option|
      unless clone_card_options.empty?
        update_quiz_option(option, clone_card_options.shift)
      else
        QuizQuestionOption.create!(
          quiz_question_id: @cloned_card.id,
          quiz_question_type: 'Card',
          label: option.label,
          is_correct: option.is_correct
        )
      end
    end
  end

  # Removes stale quiz options from clone card
  def remove_stale_quiz_options(parent_card_options, clone_card_options)
    parent_card_options.each do |option|
      update_quiz_option(option, clone_card_options.shift)
    end
    clone_card_options.each{|stale_option| stale_option.destroy}
  end

  def update_quiz_option(parent_option, clone_option)
    clone_option.update_columns(
      label: parent_option.label, 
      is_correct: parent_option.is_correct
    )
  end

  # Called when parent card prices are updated
  def sync_price
    clone_card_prices = @cloned_card.prices.to_a
    parent_card_prices = @parent_card.prices.to_a
    if parent_card_prices.length >= clone_card_prices.length # added price/updated price of parent card
      parent_card_prices.each do |price|
        unless clone_card_prices.empty?
          update_price(price, clone_card_prices.shift)
        else
          Price.create!(card_id: @cloned_card.id, currency: price.currency, amount: price.amount)
        end
      end
    else # removed price of parent card
      parent_card_prices.each do |price|
        update_price(price, clone_card_prices.shift)
      end
      clone_card_prices.each{|stale_price| stale_price.destroy}
    end
    reindex_cloned_card
  end

  def update_price(parent_price, clone_price)
    clone_price.update_columns(
      currency: parent_price.currency,
      amount: parent_price.amount
    )
  end

  # Called when parent card metadatum is updated
  def sync_card_metadatum
    parent_card_metadata = @parent_card.card_metadatum
    card_metadata = CardMetadatum.where(card_id: @cloned_card.id).first_or_create
    card_metadata.update_attributes(
      plan: parent_card_metadata.plan,
      level: parent_card_metadata.level, 
      average_rating: parent_card_metadata.average_rating
    )
    reindex_cloned_card
  end

  # Called when parent journey card sections are updated
  def sync_journey_pack_relation
    journey_pack_relations, all_cards = get_journey_card_data(@parent_card.id)
    # return if journey has private cards or project cards
    if all_cards.any? {|card| !card.is_public? || ['project'].include?(card.card_type)}
      log.warn "Not able to sync journey_pack_relation for cover card #{@parent_card.id}"
    else
      begin
        ActiveRecord::Base.transaction do
          clone_jpr, all_clone_cards = get_journey_card_data(@cloned_card.id)
          if journey_pack_relations.length > clone_jpr.length # added pathway in parent journey
            add_new_journey_pack_cards(journey_pack_relations, all_clone_cards)
          elsif journey_pack_relations.length < clone_jpr.length # removed pathway from parent journey
            remove_stale_journey_pack_cards(clone_jpr, all_clone_cards, journey_pack_relations)
          end
          reorder_clone_journey_or_pathway(journey_pack_relations, clone_jpr)
        end
      rescue ActiveRecord::RecordInvalid => e
        log.error "Error in syncing jpr for card: #{@parent_card.id} and clone card: #{@cloned_card.id}. message: #{e.message}"
      end
    end
  end

  def get_journey_card_data(card_id)
    journey_pack_relations = JourneyPackRelation.journey_relations(cover_id: card_id)
    card_ids = journey_pack_relations.map(&:from_id)
    journey_pack_relations.each do |jpr|
      next if jpr.cover_id == jpr.from_id
      card_ids << CardPackRelation.pack_relations(cover_id: jpr.from_id).map(&:from_id)
    end
    cards = Card.where(id: card_ids.flatten.uniq.compact)
    [ journey_pack_relations, cards ]
  end

  # Adds new section in clone journey card
  def add_new_journey_pack_cards(journey_pack_relations, all_clone_cards)
    clone_record_service = CloneRecordsService.new(@cloned_card.organization_id)
    journey_pack_relations.each do |jpr|
      next if (jpr.cover_id == jpr.from_id) || (all_clone_cards.any?{|card| card.super_card_id == jpr.from_id })
      clone_pack_card = clone_record_service.run(jpr.card)
      @cloned_card.add_card_to_journey(clone_pack_card.id)
    end
  end

  # Removes section from clone journey card
  def remove_stale_journey_pack_cards(clone_jpr, all_clone_cards, parent_jpr)
    clone_jpr.each do |clone_jpr|
      next if clone_jpr.cover_id == clone_jpr.from_id
      clone_pack = all_clone_cards.detect{|card| card.id == clone_jpr.from_id}
      if parent_jpr.map(&:from_id).exclude?(clone_pack.super_card_id)
        @cloned_card.remove_card_from_journey(clone_pack.id, true)
        clone_pack.destroy
      end
    end
  end

  # Called when parent pathway card is updated
  def sync_card_pack_relation
    parent_card_packs, clone_card_packs, all_cards, all_clone_cards = get_pack_card_data
    
    # return if curriculum (pathway having journey) is not cloneable
    if (journey_cards = all_cards.select {|card| card.journey_card? }).present?
      return if curriculum_uncloneable?(journey_cards)
    end

    # return if pathway has private cards or project cards
    if all_cards.any? {|card| !card.is_public? || ['project'].include?(card.card_type)}
      log.warn "Not able to sync card_pack_relation for cover card #{@parent_card.id}"
    else
      begin
        ActiveRecord::Base.transaction do
          clone_record_service = CloneRecordsService.new(@cloned_card.organization_id)
          if parent_card_packs.length > clone_card_packs.length # added card in parent pathway
            add_new_pack_cards(parent_card_packs, all_clone_cards, all_cards, clone_record_service)
          elsif parent_card_packs.length < clone_card_packs.length # removed card from parent pathway
            remove_stale_pack_cards(parent_card_packs, all_clone_cards)
          end
          Leap.where(pathway_id:  @parent_card.id).each do |leap|
            clone_record_service.clone_leap(leap, @cloned_card.id, new_pack_cards)
          end
          reorder_clone_journey_or_pathway(parent_card_packs, clone_card_packs)
        end
      rescue ActiveRecord::RecordInvalid => e
        log.error "Error in syncing cpr for card: #{@parent_card.id} and clone card: #{@cloned_card.id}. message: #{e.message}"
      end
    end
  end

  def curriculum_uncloneable?(parent_journey_cards)
    uncloneable = false
    parent_journey_cards.each do |parent_journey_card|
      journey_pack_relations, all_cards = get_journey_card_data(parent_journey_card.id)
      if all_cards.any? {|card| !card.is_public? || ['project'].include?(card.card_type)}
        uncloneable = true
        break
      end
    end
    uncloneable
  end

  def get_pack_card_data
    parent_card_packs = @parent_card.pack_relns
    clone_card_packs = @cloned_card.pack_relns
    return [parent_card_packs,
      clone_card_packs,
      Card.where(id: parent_card_packs.map(&:from_id)),
      Card.where(id: clone_card_packs.map(&:from_id))
    ]
  end

  # Adds new cards in clone pathway card
  def add_new_pack_cards(parent_card_packs, all_clone_cards, all_cards, clone_record_service)
    parent_card_packs.each do |card_pack|
      next if all_clone_cards.any?{|card| card.super_card_id == card_pack.from_id } # already clone present
      card_to_clone = all_cards.detect{|card| card.id == card_pack.from_id}
      clone_card = clone_record_service.run(card_to_clone)
      @cloned_card.add_card_to_pathway(clone_card.id, 'Card')
    end
  end

  # Removes card from clone pathway card
  def remove_stale_pack_cards(parent_card_packs, all_clone_cards)
    removed_parent_packs = all_clone_cards.map(&:super_card_id) - parent_card_packs.map(&:from_id)
    removed_parent_packs.each do |stale_id|
      card_to_remove = all_clone_cards.detect{|card| card.super_card_id == stale_id}
      @cloned_card.remove_card_from_pathway(card_to_remove.id, 'Card', {delete_dependent: true})
      card_to_remove.destroy
    end
  end

  # Reorders clone journey/pathway
  def reorder_clone_journey_or_pathway(parent_card_relns, clone_card_relns)
    parent_ids = parent_card_relns.map(&:from_id)
    updated_reln_ids = Card.where(super_card_id: parent_ids).order("field(id, #{parent_ids.join ','})").pluck(:id).reverse
    # Reorder if any
    @cloned_card.relations_reorder(order: updated_reln_ids) if updated_reln_ids != clone_card_relns.map(&:from_id)
  end

  def reindex_cloned_card
    @cloned_card.ecl_update_required = true
    @cloned_card.touch
  end
end
class SociativeRecommenderService
  STRATEGY_CC = 'CC'.freeze #content-content
  STRATEGY_CU = 'CU'.freeze #content-user
  STRATEGY_CUC = 'CUC'.freeze #content-user-content
  STRATEGY_CT = 'CT'.freeze #content-topic

  VALID_TAGS = [STRATEGY_CUC, STRATEGY_CC, STRATEGY_CU, STRATEGY_CT]

  SOCIATIVE_ENDPOINT = Settings.sociative.recommender_api
  # **opts - size and other options
  def initialize(user_id:, content_id:, tag: STRATEGY_CC, **opts)
    @user         = User.find_by(id: user_id)
    @organization = @user&.organization
    @content_id   = content_id
    @tag          = VALID_TAGS.include?(tag) ? tag :STRATEGY_CC
    @results      = {content_weights: {}}
    @size         = opts.fetch(:size, 5)
    @opts         = opts
  end

  #Get recommendations for content/users based on initalization args
  # returns status
  def recommendations
    resp = get_sociative_recommendations
    log.debug "=========RESP RECEIVED FROM SOCIATIVE ENDPOINT=================="
    log.debug resp

    msg = "Recommendations for content id: #{@content_id} \
      failed with status: #{resp.status}"

    json_resp = JSON.parse(resp.body)&.with_indifferent_access

    unless resp.status == 200
      log_error_with(msg: json_resp[:error])
      return resp.status, { error: msg }
    end

    if json_resp[:error]
      log_error_with(msg: json_resp[:error])
      return resp.status, { error: msg }
    end

    process_recommended_entities(json_resp.fetch(:data, {}))
    return resp.status, @results
  end

  protected
  # Establish connection with Sociative
  def establish_connection
    Faraday.new(url: SOCIATIVE_ENDPOINT)
  end

  private
  # Get actual recommendations from sociative server.
  def get_sociative_recommendations
    connection = establish_connection
    connection.response :logger

    connection.post do |req|
      req.headers['Content-Type'] = 'application/json'
      req.body = prepare_req_body.to_json
    end
  end

  # Process the response received from sociative
  # This may have users/content ids or topics
  # SAMPLE RESP
  # {
  #     'nodes':[  # list of nodes
  #         {
  #             'id': '<some_entity>',
  #             'kind': 'content'
  #             'data':'<some_optional data>'
  #         },
  #         {},
  #     ],
  #     'edges':[  # list of edges
          # {
          #   "src" => {
          #     "kind" => "content",
          #     "id" => "i5HvzWIJSagwidZeReULoQ=="
          #   },
          #   "dst" => {
          #     "kind" => "content",
          #     "id" => "kDkJlNSe1BNZus8XPJ1uOg=="
          #   },
          #   "data" => {},
          #   "soc" => true,
          #   "weight" => 0.45973101258277893
          # }
  #         {},
  #     ]
  # }
  def process_recommended_entities(resp)
    nodes = resp.fetch :nodes, []
    edges = resp.fetch :edges, []

    content_weight_mapping(edges)
    load_entities_from_nodes(nodes)
  end

  #Parse root level objects from node.
  # it would be of kinds: user, content and topic
  def load_entities_from_nodes(nodes = [])
    return if nodes.empty?

    content_nodes, topic_nodes, user_nodes = [], [], []
    nodes_by_kind = nodes.group_by {|n| n[:kind]}.with_indifferent_access

    @results[:cards]  = load_cards_from_content_nodes(nodes_by_kind.fetch(:content, []))
    @results[:topics] = load_topics_in_results(nodes_by_kind.fetch(:topic, []))
    @results[:users]  = load_users_in_results(nodes_by_kind.fetch(:user, []))
  end

  # Content ids are of two types. Sociative may recommend
  # content that already exists in lxp or content that exists
  # in sociative cloud. Later has non-integer snow flake ids
  def load_cards_from_content_nodes(content_nodes)
    return [] if content_nodes.blank?
    content_uuids, lxp_card_ids = [], []
    content_nodes.each do |node|
      # Will do better if there is a key to differentiate between below two.
      (node[:soc] ? content_uuids : lxp_card_ids).push node[:id]
    end

    lxp_cards(lxp_card_ids.uniq) + cards_from_uuids(content_uuids.uniq)
  end

  # # Get cards from lxp db for the content ids returned by sociative
  def lxp_cards(ids)
    ids.delete @content_id
    @user.organization.cards.where(ecl_id: ids)
  end

  # # Build cards from sociative snow flake ids
  # # Eventually use ECL connector get cards from snow flake ids
  def cards_from_uuids(content_uuids)
    return [] if content_uuids.empty?
    log.debug content_uuids
    content_uuids.delete @content_id

    filter_params = {
      ecl_card_ids: content_uuids.take(@size)
    }

    cards = EclApi::EclSearch.new(@user.organization.id, @user.id).
      search(
        query: '',
        limit: content_uuids.size,
        offset: 0,
        filter_params: filter_params,
        load_tags: false
      ).results
    cards || []
  end

  # # re-format or forward same hash as recevied from sociative
  def load_topics_in_results(topic_nodes)
    return [] if topic_nodes.blank?
    topic_nodes.each_with_object([]) do |node, memo|
      next if node[:kind] != 'topic'
      memo << {name: node[:id], label: node[:id].split('.')&.last&.humanize}
    end
  end

  # load users from lxp db
  def load_users_in_results(user_nodes)
    return [] if user_nodes.blank?
    user_ids = user_nodes.collect {|user_node| user_node['id']}.compact
    # Remove if requested user is present in the response
    user_ids.delete(@user.id) if user_ids.include?(@user.id)
    # pick first top users
    @organization.users.where(id: user_ids.take(@size))
  end

  # A common params to all tag types
  def prepare_req_body
    {
      tag: @tag,
      context: {
        user_id: @user.id,
        org_id: @organization.id,
      }
    }.merge! entity_and_related_req_params(@tag)
  end

  # Get entity and related hash based on tag strategy
  def entity_and_related_req_params(tag)
    entity_releated_hash = case tag
    when STRATEGY_CC, STRATEGY_CUC
      {
        entity: {id: @content_id, kind: 'content'},
        related: {size: @size, kind: 'content'}
      }
    when STRATEGY_CU
      {
        entity: {id: @content_id, kind: 'content'},
        related: {size: @size, kind: 'user'}
      }
    when STRATEGY_CT
      {
        entity: {id: @content_id, kind: 'content'},
        related: {size: @size, kind: 'topic'}
      }
    end
    entity_releated_hash || {}
  end

  def log_error_with(msg:)
    Bugsnag.notify msg, { severity: 'error' }
  end

  def content_weight_mapping(edges = [])
    @results[:content_weights] = edges.each_with_object({}) do |edge, memo|
      memo[edge['dst']['id']] ||= edge['weight']&.round(2)
    end
  end

  # SAMPLE REQUEST TO SOCIATIVE
  # CC
  # related_request = {
  #     'tag':'CC', # until we have appropriate kinds
  #     'context': {
  #         'user_id':'<>', # these s/b perhaps entities
  #         'org':'<>',
  #     },
  #    'entity':{ # entity to find related_to
  #         "id":'1234',
  #         'kind':'content',
  #     },
  #     'related':{
  #         'kind':'content'
  #         'size': 10 # max related entities to return
  #     }
  # }

  # CU
  # related_request = {
  #     'tag':'CU', # until we have appropriate kinds
  #     'context': {
  #         'user_id':'<>', # these s/b perhaps entities
  #         'org':'<>',
  #     },
  #    'entity':{ # entity to find related_to
  #         "id":'1234',
  #         'kind':'content',
  #     },
  #     'related':{
  #         'kind':'user'
  #         'size': 10 # max related entities to return
  #     }
  # }

  # CUC
  # related_request = {
  #     'tag':'CUC', # until we have appropriate kinds
  #     'context': {
  #         'user_id':'<>', # these s/b perhaps entities
  #         'org':'<>',
  #     },
  #    'entity':{ # entity to find related_to
  #         "id":'1234',
  #         'kind':'content',
  #     },
  #     'related':{
  #         'kind':'content'
  #         'size': 10 # max related entities to return
  #     }
  # }
end
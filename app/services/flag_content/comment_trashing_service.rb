module FlagContent
  class CommentTrashingService
    include CommentConcern
    attr_reader :comment, :status

    def initialize(comment: )
      @comment = comment
      @status = false
    end

    def call
      if @comment.destroy
        klass = @comment.commentable.class
        # acts_as_paranoid does not trigger the after_destroy callback of counter_cache
        klass.decrement_counter(:comments_count, @comment.commentable_id)
        @status = true
      end
    end
  end
end

module FlagContent
  class CardReportingSearchService
    include Elasticsearch::Persistence::Model

    self.index_name = "card_reporting_#{Rails.env}"

    settings analysis: {
            analyzer: {
              card_reporting_analyzer: {
                type: "custom",
                tokenizer: :card_reporting_ngram_tokenizer,
                filter: [:lowercase]
            }
          },
          tokenizer: {
            card_reporting_ngram_tokenizer: {
              type: "edgeNGram",
              min_gram: 2,
              max_gram: 10,
              token_chars: [ "letter","digit"]
            }
          }
      }

    attribute :id, Integer
    attribute :card_type, String
    attribute :card_message, String, mapping: {index: 'analyzed', analyzer: 'card_reporting_analyzer' }
    attribute :card_created_at, DateTime
    attribute :card_author_id, Integer
    attribute :card_source, String, mapping: { type: 'string', index: 'not_analyzed' }
    attribute :card_organization_id, Integer
    attribute :card_deleted_at, DateTime, mapping: {type: :date}
    attribute :reportings, Array, mapping: {type:'object'}

    def self.find_record(id: )
      begin
        result = self.find(id)
      rescue => e
        log.error e
      end
      result
    end

    def self.trash_record(card: )
      existing = find_record(id: card.id)
      existing&.destroy
    end

    def self.search_match_query(query)
      if query.blank?
      {
        match_all: {}
      }
      else
      {
        match:{card_message: {query: query } }
      }
      end
    end

    def self.must_conditions(state: , organization_id: )
      musts = []
      musts << {term: {card_organization_id: organization_id}}
      if state == 'reported'
        musts << {missing: {field: "card_deleted_at"}}
        musts << {missing: {field: "reportings.deleted_at"}}
      elsif state == 'trashed'
        musts << {exists: {field: "card_deleted_at"}}
        musts << {exists: {field: "reportings.deleted_at"}}
      end
    end

    def self.search_for_card(organization: , query:, limit: 10, offset: 0, state: )
      search_query = {query: {
             filtered: {
               filter:{
                  bool:{
                   must: must_conditions(state:state, organization_id: organization.id)
                 }
               },
               query: search_match_query(query)
             }
           }
         }
      search_query.merge!(from: offset, size: limit)
      self.search(search_query).results
    end

    def self.extract_user_ids(results: )
      user_ids = results.each_with_object([]) do |item, arr|
        arr << item['card_author_id']
        arr << item['reportings'].map{|reporting| reporting['user_id']}
      end

     User.where(id: user_ids.flatten.uniq).each_with_object({}) {|user, hash| hash[user.id] = user.full_name}
    end
  end
end

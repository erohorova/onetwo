class UserBadgeService

  def initialize(type, entity, user_id, score=nil)
    @type = type
    @entity = entity
    @user_id = user_id
    @score = score
  end

  def assign_badge
    if @type == "Card"
      assign_card_badge
    elsif @type == "Clc"
      assign_clc_badge
    end
  end

  def assign_card_badge
    if (card_badge = @entity.card_badging)
      if card_badge.all_quizzes_answered?
        assign_badge_by_quiz_attempts(card_badge)
      else
        UserBadge.find_or_create_by(badging_id: card_badge.id, user_id: @user_id)
      end
    end
  end

  def assign_clc_badge
    clc_badging = ClcBadging.where("badgeable_id = ? and target_steps <= ?", @entity.id, @score).order("target_steps desc").first
    if clc_badging.present?
      clc_badgings = ClcBadging.where(badgeable_id: @entity.id)
      user_badge = UserBadge.where("badging_id in (?) and user_id = ?", clc_badgings.ids, @user_id).first || UserBadge.new(user_id: @user_id)
      user_badge.badging_id = clc_badging.id
      user_badge.save
    end
  end

  def assign_badge_by_quiz_attempts(badge)
    user = User.find_by(id: @user_id, organization_id: @entity.organization_id)
    @entity.pack_quizzes_attempts_correct?(user: user) && UserBadge.find_or_create_by(badging_id: badge.id, user_id: @user_id)
  end
end

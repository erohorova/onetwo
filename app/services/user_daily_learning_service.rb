require 'set'

# DEPRECATED
class UserDailyLearningService

  NUM_LEARNING_ITEMS = 5
  EXCLUSION_DAYS = 21
  TOPIC_CACHE_DAYS = 100

  def initialize(user)
    @user = user
    @organization = @user.organization
    @excluded_ecl_ids = Set.new([])
  end

  def pick_daily_learning_from_topics(topic_ids)
    if topic_ids.size == 0
      return []
    end
    ecl_ids = []
    from_date = Time.now.utc.to_date - EXCLUSION_DAYS

    # Exclude items already in daily learning from past 20 days
    excluded_card_ids = Set.new([])
    excluded_card_ids = excluded_card_ids.merge(UserDailyLearning.where("user_id = ? AND date >= ?", @user.id, from_date).map{ |x| x.card_ids}.flatten)

    # Exclude items that the user has dismissed from the feed
    excluded_card_ids = excluded_card_ids.merge(@user.dismissed_card_ids)

    # Exclude items that the user has marked as complete
    excluded_card_ids = excluded_card_ids.merge(@user.completed_card_ids)

    # Exclude items that the user has bookmarked
    excluded_card_ids = excluded_card_ids.merge(@user.bookmarked_card_ids)

    @excluded_ecl_ids = @excluded_ecl_ids.merge(Card.where(id: excluded_card_ids.to_a).pluck(:ecl_id))

    ecl_ids_per_topic = collect_items_for_topic_ids(topic_ids)

    # Cycles through the array of cards for each topic, picking out elements in order, until we have
    # enough number of items, or we have run out of items to pick
    # For example, if the input is [1,2,3], [4], [5,6,7,8,9,10], the algorithm picks out
    # [1,4,5,2,6]
    # 1st element is from list 1, 2nd element is from list 2, 3rd element is from list 3, and then 4th element is again from list 1 and so on
    while ecl_ids.length < NUM_LEARNING_ITEMS && ecl_ids_per_topic.length > 0 do
      ecl_ids_for_current_topic = ecl_ids_per_topic.shift

      top_ecl_id = ecl_ids_for_current_topic.shift
      while(top_ecl_id.present? && !item_ok_to_include?(top_ecl_id)) do
        top_ecl_id = ecl_ids_for_current_topic.shift
      end

      unless top_ecl_id.nil?
        ecl_ids << top_ecl_id
        @excluded_ecl_ids.add(top_ecl_id)
      end

      ecl_ids_per_topic << ecl_ids_for_current_topic unless ecl_ids_for_current_topic.blank?
    end

    ecl_ids.map{|ecl_id|
      EclApi::CardConnector.new(organization_id: @organization.id, ecl_id: ecl_id).card_from_ecl_id
    }.map{|card|
      card.id
    }
  end

  private
  ## Returns an array of (array of cards), where each element in the top level array is an array of cards corresponding to a topic
  # For example,
  # topic_ids = ['ml', 'cnn']
  # card_ids_per_topic = collect_items_for_topic_ids(topic_ids)
  # p card_ids_per_topic
  # [[1,2,3], [3,4,5]]
  # This means that card_ids 1,2 and 3 corresponding to topic 'ml', and 3,4 and 5 correspond to topic 'cnn'
  def collect_items_for_topic_ids(topic_ids)
    topic_ids.map do |topic_id|
      TopicDailyLearning.completed.where("organization_id = ? AND topic_id = ?", @organization.id, topic_id).order("date DESC").limit(TOPIC_CACHE_DAYS).map(&:ecl_ids).flatten
    end
  end

  def item_ok_to_include?(ecl_id)
    !@excluded_ecl_ids.include?(ecl_id)
  end
end

# frozen_string_literal: true

class FetchActivitiesService

  def initialize(user:, team_id: [], filter: {})
    @user = user
    @org = user.organization
    @team_ids = team_id.blank? ? @user.teams.pluck(:id) : [team_id]
    @only_conv = filter[:only_conversations]
    @limit = filter[:limit]
    @offset = filter[:offset]
    @only_user_activities = filter[:only_user_activities]
    @activity_actions = filter[:activity_actions].present? ? filter[:activity_actions] : ActivityStream::ACTIONS
    @current_user = filter[:current_user] || @user
  end

  def fetch_activities(filter_by_team: false)
    return ActivityStream.none if @team_ids.blank?
    query = teams_activities_query(filter_by_team: filter_by_team)

    query.includes(:user, :comments)
      .order(id: :desc).limit(@limit).offset(@offset).uniq
  end

  def fetch_org_activities
    return fetch_org_streams unless @only_conv

    base_query = <<-SQL.strip_heredoc
      SELECT activity_streams.id
      FROM activity_streams
      LEFT JOIN comments 
      JOIN organizations
      ON comments.commentable_id = organizations.id
      AND comments.commentable_type = 'Organization'
      ON activity_streams.streamable_id = comments.id
    SQL

    base_condition = <<-SQL.strip_heredoc
      WHERE organizations.id = :org_id
      AND action = 'comment'
    SQL

    query = base_query.concat base_condition

    query_params = {
      org_id: @org.id
    }

    fetch_streams(query, query_params)
  end

  private

  def teams_activities_query(filter_by_team: false)
    base_query = ActivityStream.joins("LEFT JOIN comments JOIN teams ON comments.commentable_id = teams.id "+
      "AND comments.commentable_type = 'Team' ON activity_streams.streamable_id = comments.id")
      .joins("INNER JOIN teams_users ON activity_streams.user_id = teams_users.user_id AND teams_users.team_id IN (#{@team_ids.first})")

    base_condition = <<-SQL.strip_heredoc
      activity_streams.organization_id = :org_id
    SQL

    query_params = {
      team_ids: @team_ids,
      org_id: @org.id
    }

    if @only_conv
      base_condition.concat <<-SQL.strip_heredoc
          AND activity_streams.action = 'comment'
      SQL

      if filter_by_team
        base_condition.concat <<-SQL.strip_heredoc
          AND teams.id IN (:team_ids)
        SQL
      end
    else

      base_query = content_accessibility_join(base_query)

      assignable_ids = Assignment.joins(:team_assignments)
        .where(team_assignments: { team_id: @team_ids })
        .pluck(:assignable_id)
        .uniq
      assignments = assignable_ids.empty? ? 0 : assignable_ids

      base_condition.concat <<-SQL.strip_heredoc
        AND (teams.id IN (:team_ids) OR teams_cards.team_id IN (:team_ids) OR activity_streams.card_id IN (:assignments))
        AND activity_streams.streamable_id IS NOT NULL
      SQL

      # The privacy checks should be added only for org members.
      # Org admins should be able to see everything in the stream
      unless @user.is_org_admin?
        base_condition = content_accessibility_condition(base_condition)
      end

      user_teams = @user.team_ids.presence
      user_channels = @user.followed_channel_ids.presence
      query_params = query_params.merge({assignments: assignments,
                                         current_user_id: @user.id,
                                         user_channels: user_channels,
                                         user_teams: user_teams})
    end

    base_query.where(base_condition, query_params)
  end

  def fetch_streams(query, params)
    ids = ActivityStream.find_by_sql([query, params])
    ActivityStream.where(id: ids).where.not(streamable: nil)
      .includes(:user, :comments)
      .order(id: :desc).limit(@limit).offset(@offset).uniq
  end

  def fetch_org_streams
    base_query = ActivityStream.joins("LEFT JOIN cards ON cards.id = activity_streams.card_id AND cards.organization_id = #{@org.id}")

    base_condition = <<-SQL.strip_heredoc
      activity_streams.organization_id = :org_id
    SQL

    query_params = {
      org_id: @org.id
    }

    if @only_user_activities
      base_condition.concat <<-SQL.strip_heredoc
        AND activity_streams.user_id = :user_id
      SQL
      query_params.merge!({user_id: @user.id})
    end

    if @activity_actions
      base_condition.concat <<-SQL.strip_heredoc
        AND activity_streams.action IN (:activity_actions)
      SQL
      query_params.merge!({activity_actions: @activity_actions})
    end

    # The privacy checks should be added only for org members.
    # Org admins should be able to see everything in the stream
    unless @current_user.is_org_admin?
      base_query = content_accessibility_join(base_query)
      base_condition  = content_accessibility_condition(base_condition)
      user_teams = @current_user.team_ids.presence
      user_channels = @current_user.followed_channel_ids.presence
      query_params.merge!({current_user_id: @current_user.id,
                      user_channels: user_channels,
                      user_teams: user_teams})

    end
    base_query = base_query.where(base_condition, query_params)
      .where.not(streamable: nil)
      .where.not("(activity_streams.action = 'upvote' AND (cards.hidden = true OR (cards.card_type = 'pack' AND cards.card_subtype = 'journey')))")

    base_query.includes(:comments, :user, :votes).uniq.order(id: :desc).limit(@limit).offset(@offset)
  end

  def content_accessibility_join(base_query)
    base_query.joins("LEFT JOIN cards ON cards.id = activity_streams.card_id AND cards.organization_id = #{@org.id}")
        .joins("LEFT JOIN teams_cards ON activity_streams.card_id = teams_cards.card_id AND teams_cards.type = 'SharedCard'")
        .joins("LEFT JOIN card_user_shares ON card_user_shares.card_id = cards.id AND card_user_shares.user_id = #{@current_user.id}")
        .joins("LEFT JOIN channels_cards cc ON cc.card_id = cards.id")
        .joins("LEFT JOIN card_team_permissions ON card_team_permissions.card_id = activity_streams.card_id")
        .joins("LEFT JOIN card_user_permissions ON card_user_permissions.card_id = activity_streams.card_id")
  end

  def content_accessibility_condition(base_condition)
    base_condition.concat <<-SQL.strip_heredoc
          AND cards.deleted_at IS NULL AND (cards.id IS NULL
            OR (cards.is_public = true || cards.author_id = :current_user_id || 
                (cards.is_public = false AND 
                  (
                    (card_user_permissions.id IS NOT NULL AND card_user_permissions.user_id = :current_user_id) ||
                    (card_team_permissions.id IS NOT NULL AND card_team_permissions.team_id IN (:user_teams)) ||
                    (
                      (
                        (cc.id IS NOT NULL && cc.channel_id IN (:user_channels)) || 
                        (teams_cards.id IS NOT NULL AND teams_cards.team_id IN (:user_teams)) || 
                        (card_user_shares.id IS NOT NULL)
                      ) 
                      && (card_user_permissions.id IS NULL AND card_team_permissions.id IS NULL)
                    )
                  )
                )
              )
            )
        SQL
  end
end

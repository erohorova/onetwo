module Notify
  class AddedChannelFollowerAdminNotifier < Notifier
    def setup
      self.notification_name = NOTE_ADDED_CHANNEL_FOLLOWER_ADMIN
      set_enabled(:email, :single)
      set_default(:email, :single, false)
      enable_additional_content
    end

    def users_to_notify(event_name, organization, custom_hash)
      if organization.present? && custom_hash.present?
        admin_users = organization.users.not_suspended.where(organization_role: 'admin', is_active: true)
        admin_ids = admin_users.where.not(id: custom_hash[:sender_id].to_i).pluck(:id)
        yield admin_ids
      else
        yield []
      end
    end

    def get_organization(organization)
      organization
    end

    def get_teams_hash(team_names)
      {
        names: team_names,
        count: team_names.size
      }
    end

    def get_followers_hash(followers_emails)
      {
        emails: followers_emails,
        count: followers_emails.size,
        bulk_users: followers_emails.size >= ChannelsUser::BULK_USER_START
      }
    end

    def get_channels_hash(channels)
      {
        channel_string: "to the #{'channel'.pluralize(channels.size)}",
        channel_names: channels.join(", ")
      }
    end

    def push_notification_params(notification_entry)
      organization = notification_entry.sourceable

      {
        user_id: notification_entry.user_id,
        deep_link_type: 'Organization',
        notification_type: self.notification_name,
        host_name: organization.home_page
      }
    end

    def notification_params(notification_entry)
      organization = notification_entry.sourceable
      additional_content = notification_entry.additional_content
      sender = User.find_by(id: additional_content[:sender_id])
      teams = additional_content[:teams_names].present? ? get_teams_hash(additional_content[:teams_names]) : {}
      followers = additional_content[:followers_emails].present? ? get_followers_hash(additional_content[:followers_emails]) : {}

      {
        first_name: sender.try(:first_name),
        last_name: sender.try(:last_name),
        channels: get_channels_hash(additional_content[:channels]),
        host_name: organization.home_page,
        teams: teams,
        followers: followers
      }
    end
  end
end
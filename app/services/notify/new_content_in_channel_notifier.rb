module Notify
  class NewContentInChannelNotifier < Notifier
    def setup
      self.notification_name = NOTE_NEW_CONTENT_IN_CHANNEL

      set_enabled(:email, :single)
      set_default(:email, :single, false)

      set_enabled(:push, :single)
      set_default(:push, :single, false)
      enable_additional_content
    end

    def get_organization(channel_card)
      channel_card.channel.organization
    end

    def users_to_notify(event_name, channel_card, custom_hash = {})
      channel = channel_card.channel
      card = channel_card.card

      if !channel || (channel.followers.empty? && channel.teams_channels_follows.empty?)
       yield []
       return
      end

      Notify.batchify(channel.followers, 1000) do |users|
        users = fetch_valid_users_to_notify(users, channel_card, custom_hash)
        yield users.map(&:id)
      end
      
      channel.followed_teams.each do |team|
        Notify.batchify(team.users, 1000) do |users|
          users = fetch_valid_users_to_notify(users, channel_card, custom_hash)
          yield users.map(&:id)
        end
      end

    end

    def fetch_valid_users_to_notify(users, channel_card, custom_hash)
      # To skip users who've been notified about the livestream by being followers of the author
      #  Or by being users of teams the livestream was shared with
      #  Or by being followers of a channel the livestream was posted to
      users_to_not_notify = NotificationEntry.where(user_id: users.pluck(:id),
        event_name: Notify::EVENT_NEW_LIVESTREAM,
        sourceable: channel_card.card).pluck(:user_id) |
      NotificationEntry.where(user_id: users.pluck(:id),
        event_name: Notify::EVENT_LIVE_STREAM_IN_GROUP,
        sourceable: SharedCard.where(team_id: custom_hash[:team_ids_notified], card_id: channel_card.card_id))
        .pluck(:user_id) |
      NotificationEntry.where(user_id: users.pluck(:id),
        event_name: Notify::EVENT_NEW_CONTENT_IN_CHANNEL,
        sourceable: channel_card).pluck(:user_id)

      #  To not notify the author of the livestream
      users_to_not_notify << channel_card.card.author_id

      users = users.where.not(id: users_to_not_notify)
      if !channel_card.card.is_public?
        users = users.select{|user| channel_card.card.visible_to_user(user)}
      end
      users
    end

     def push_notification_params(notification_entry)
      channel_card = notification_entry.sourceable

      {
        user_id: notification_entry.user_id,
        deep_link_id: channel_card.card.id,
        deep_link_type: 'card',
        notification_type: self.notification_name,
        host_name: get_organization(channel_card).home_page
      }
    end

    def notification_params(notification_entry)
      channel_card = notification_entry.sourceable
      channel = channel_card.channel
      card = channel_card.card
      user = card.author

      {
        user: {
          first_name: user.first_name,
          last_name: user.last_name,
        },
        channel: {
          name: channel.label,
          channel_url: channel.channel_url
        },
        card: {
          url: card.card_deeplink_url,
          image_url: card.fetch_image_attachment_url,
          author_image: card.author.try(:photo),
          author_name: card.author.try(:name),
          title: card.snippet,
          title_simplified: simplify_encoded_string(card.snippet).truncate(100, separator: ' ')
        },
        team_name: channel.organization.try(:name)
      }
    end
  end
end

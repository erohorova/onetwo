module Notify
  class CuratedCardNotifier < Notifier
    def setup
      self.notification_name = NOTE_CURATED_CARD

      set_enabled(:email, :single)
      set_default(:email, :single, false)
      set_enabled(:email, :digest_daily)

      set_enabled(:push, :single)
      set_default(:push, :single, false)
    end

    def users_to_notify(event_name, channels_card)
      unless channels_card.card.ugc?
        yield []
        return
      end

      # Return the card's author
      yield [channels_card.card.author_id]
    end

    def get_organization(channels_card)
      channels_card.channel.organization
    end

    def push_notification_params(notification_entry)
      channels_card = notification_entry.sourceable

      {
        user_id: notification_entry.user_id,
        deep_link_id: channels_card.channel_id,
        deep_link_type: 'channel',
        notification_type: self.notification_name,
        host_name: get_organization(channels_card).home_page
      }
    end

    def notification_params(notification_entry)
      channels_card = notification_entry.sourceable

      {
        channel: {
          name: channels_card.channel.label,
          channel_url: channels_card.channel.channel_url
        },
        card_title: channels_card.card.snippet_html
      }
    end
  end
end

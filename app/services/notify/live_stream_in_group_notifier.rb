module Notify
  class LiveStreamInGroupNotifier < Notifier
    def setup
      self.notification_name = NOTE_LIVE_STREAM_IN_GROUP

      set_enabled(:email, :single)
      set_default(:email, :single, false)

      set_enabled(:push, :single)
      set_default(:push, :single, false)
    end

    def users_to_notify(event_name, team_card)
      if (!team_card.team || team_card.team.users.empty?)
        yield []
        return
      end

      # Return users who have not been notified about this livestream already
      Notify.batchify(team_card.team.users, 1000) do |users|

        # To skip users who've been notified about the livestream by being followers of the author
        users_to_not_notify = NotificationEntry.where(user_id: users.pluck(:id),
          event_name: Notify::EVENT_NEW_LIVESTREAM,
          sourceable: team_card.card).pluck(:user_id)

        #  To not notify the author of the livestream
        users_to_not_notify << team_card.card.author_id

        users = users.where.not(id: users_to_not_notify)

        if !team_card.card.is_public?
          users = users.select{|user| team_card.card.visible_to_user(user)}
        end

        yield users.map(&:id)
      end
    end

    def get_organization(team_card)
      team_card.team.organization
    end


    def push_notification_params(notification_entry)
      team_card = notification_entry.sourceable

      {
        user_id: notification_entry.user_id,
        deep_link_id: team_card.card.id,
        deep_link_type: 'card',
        notification_type: self.notification_name,
        host_name: get_organization(team_card).home_page
      }
    end

    def notification_params(notification_entry)
      team_card = notification_entry.sourceable
      team = team_card.team
      card = team_card.card
      user = card.author

      {
        user: {
          first_name: user.first_name,
          last_name: user.last_name,
        },
        team: {
          name: team.name
        },
        card: {
          url: card.card_deeplink_url,
          image_url: card.fetch_image_attachment_url,
          author_image: card.author.try(:photo),
          author_name: card.author.try(:name),
          title: card.snippet,
          title_simplified: simplify_encoded_string(card.snippet).truncate(100, separator: ' ')
        },
        team_name: team.organization.try(:name)
      }
    end
  end
end

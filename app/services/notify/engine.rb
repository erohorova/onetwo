require 'mandrill'
include ApplicationHelper
module Notify
  class Engine
    attr_accessor :events_to_notifiers, :notification_name_to_notifiers, :settings
    def initialize
      self.events_to_notifiers = {}
      self.notification_name_to_notifiers = {}
      self.settings = NotifySettings.new(self)
    end

    def trigger_event(event_name, source_obj, custom_hash = {})
      notifiers = events_to_notifiers[event_name] || []
      notifiers.each do |notifier|
        args_to_pass = notifier.additional_content_enabled? ? [event_name, source_obj, custom_hash] : [event_name, source_obj]

        # Web notification don't need org  notifications_and_triggers_enabled config check,
        # and org send_no_mails config check hence even it fails we should create web notfication
        unless ( notifier.get_organization(source_obj).notifications_and_triggers_enabled? &&
          !notifier.get_organization(source_obj).send_no_mails?
        )
          notifier.execute_for_notification_web_only(*args_to_pass) do |ne|
            # temp. condition check until all web notifcations moved to new framework
            set_web_notification_content(ne) if Notify::WEB_NOTIFICATIONS_HANDLED.include?(event_name)
          end
          return
        end

        notifier.execute_for_event(*args_to_pass) do |ne|
          set_web_notification_content(ne) if Notify::WEB_NOTIFICATIONS_HANDLED.include?(event_name) #ne.enabled?(:web, :single)
        end
      end

      true
    end

    def encapsulated_params_for_notification(notification_entry)
      # Initialize params
      params_for_notif = notification_params(notification_entry).merge!({_push: push_notification_params(notification_entry)})
      notification_name = notification_entry.notification_name

      # Encapsulate them
      encapsulated_params = global_params(notification_entry.user)
      encapsulated_params[notification_name] = [params_for_notif]

      # Return
      HashWithIndifferentAccess.new(encapsulated_params)
    end

    def get_single_notification_content(notification_entry)
      # STEP 1: Initialize params
      encapsulated_params = encapsulated_params_for_notification(notification_entry)
      notification_name = notification_entry.notification_name

      {
        email: get_email_content(encapsulated_params:encapsulated_params, template_name:notification_name),
        push: get_push_content(encapsulated_params:encapsulated_params, template_name:notification_name)
      }
    end

    def set_web_notification_content(notification_entry)
      encapsulated_params = encapsulated_params_for_notification(notification_entry)
      notification_name = notification_entry.notification_name
      content = get_web_content(encapsulated_params: encapsulated_params,
                                template_name: notification_name)[:text]

      push_params = encapsulated_params[notification_name][0][:_push]

      # web_text = {
      #   text: content,
      #   deep_link_id: push_params[:deep_link_id],
      #   deep_link_type: push_params[:deep_link_type]
      # }

      find_or_new_web_notification(
        notification_name: notification_name,
        notification_entry: notification_entry,
        message: content,
        encapsulated_params: encapsulated_params,
        push_params: push_params
       )
      # notification_entry.web_text = web_text
      # notification_entry.set_status(:web, :single, STATUS_SENT)
      # notification_entry.save
    end

    def find_or_new_web_notification(opts = {})
      notification = Notification.where(notifiable: opts[:notification_entry].sourceable,
        user_id: opts[:notification_entry].user_id,
        notification_type: opts[:notification_name],
        unseen: true
      ).first

      unless notification
        notification = Notification.create(notifiable: opts[:notification_entry].sourceable,
          user_id: opts[:notification_entry].user_id,
          notification_type: opts[:notification_name],
          app_deep_link_id: opts[:push_params][:deep_link_id],
          app_deep_link_type: opts[:push_params][:deep_link_type],
          custom_message: opts[:message],
          card_id: (opts[:push_params][:deep_link_type] == 'Card' ? opts[:push_params][:deep_link_id] : nil)
        )
      end
    end

    def send_single_notification(notification_entry)
      # STEP 1: Initialize params
      encapsulated_params = encapsulated_params_for_notification(notification_entry)
      notification_name = notification_entry.notification_name
      layout_name = notification_name_to_notifiers[notification_name].layout_name

      # STEP 2: Send email if required
      if notification_entry.enabled?(:email, :single)
        begin
          send_email(encapsulated_params:encapsulated_params, template_name:notification_name,
                     layout_name:layout_name)
          notification_entry.set_status(:email, :single, STATUS_SENT)
          notification_entry.save
        rescue StandardError => e
          notification_entry.set_status(:email, :single, STATUS_ERROR)
          notification_entry.save
          log.error "send_single_notification EMAIL: #{e}"
        end
      end

      # STEP 3: Send push if required
      if notification_entry.enabled?(:push, :single)
        begin
          send_push(encapsulated_params:encapsulated_params, template_name:notification_name)
          notification_entry.set_status(:push, :single, STATUS_SENT)
          notification_entry.save
        rescue StandardError => e
          notification_entry.set_status(:push, :single, STATUS_ERROR)
          notification_entry.save
          log.error "send_single_notification PUSH: #{e}"
        end
      end
    end

    def send_digests(date, interval=:digest_daily)
      # STEP 1: Get all users for daily digest
      user_ids_query = NotificationEntry.select('DISTINCT user_id')
                                        .where('sending_time_in_timezone <= ?', date)
                                        .where('sending_time_in_timezone > ?', date - LOOKBACK_DURATIONS[interval])
                                        .where("#{NotificationEntry.unsent_for_sql(:email, interval)} OR
                                                #{NotificationEntry.unsent_for_sql(:push, interval)}")

      # All for efficiency. Can
      Notify.batchify(user_ids_query, 1000) do |users|
        user_ids = users.map(&:user_id)

        # STEP 2: For every such user, send daily digest
        user_ids.each do |user_id|
          send_user_digest_async(date, user_id, interval=interval)
        end
      end
    end

    def send_user_digest_async(date, user_id, interval=:digest_daily)
      SendUserDigestJob.perform_later(date.strftime, user_id, interval.to_s)
    end

    def send_user_digest(date, user_id, interval=:digest_daily)
      user = User.find(user_id)
      # STEP 1: Initialize base query
      base_notifications = NotificationEntry.where(user_id: user_id)
                                            .where('sending_time_in_timezone <= ?', date)
                                            .where('sending_time_in_timezone > ?', date - LOOKBACK_DURATIONS[interval])

      # STEP 2: Get all email notifications
      email_notifications = base_notifications.where(NotificationEntry.unsent_for_sql(:email, interval))
      email_notifications = email_notifications.reject { |n| !n.sourceable.visible_to_user(user) if n.sourceable_type == "Card" }
      email_notifications = NotificationEntry.where(id: email_notifications.map(&:id))
      # STEP 3: Get all push notifications
      # push_notifications = base_notifications.where(NotificationEntry.unsent_for_sql(:push, interval))

      # STEP 4: Get params for email notifications
      common_params = global_params(user)
      email_params = common_params.deep_dup
      email_notifications.each do |note|
        notification_name = note.notification_name
        email_params[notification_name] = [] if email_params[notification_name].nil?

        email_params[notification_name] << notification_params(note)
      end

      # STEP 5: Get params for push notifications
      # push_params = common_params.deep_dup
      # push_notifications.each do |note|
      #   notification_name = note.notification_name
      #   push_params[notification_name] = [] if push_params[notification_name].nil?

      #   push_params[notification_name] << notification_params(note)
      # end

      # STEP 6: Send email
      if email_notifications.exists?
        begin
          send_email(encapsulated_params: email_params, template_name: interval, layout_name: 'default_layout')
          email_notifications.update_all(NotificationEntry.status_hash(:email, interval, STATUS_SENT))
        rescue StandardError => e
          log.error "send_user_digest EMAIL: #{e}"
          email_notifications.update_all(NotificationEntry.status_hash(:email, interval, STATUS_ERROR))
        end
      end

      # STEP 7: Send push
      # if push_notifications.exists?
      #   begin
      #     send_push(encapsulated_params: push_params, template_name: interval)
      #     push_notifications.update_all(NotificationEntry.status_hash(:push, interval, STATUS_SENT))
      #   rescue StandardError => e
      #     log.error "send_user_digest PUSH: #{e}"
      #     push_notifications.update_all(NotificationEntry.status_hash(:push, interval, STATUS_ERROR))
      #   end
      # end
    end

    def get_web_content(encapsulated_params:, template_name:)
      text = render_template_to_string(template_name: "web/#{template_name}_web", vars: encapsulated_params)
      {
        text: text
      }
    end

    def file_exists?(path)
      return File.exists?(Rails.root.join('app', 'views', 'notify', path))
    end

    def get_org_specific_template(encapsulated_params:, template_name:, layout_name:)
      org_folder = "#{encapsulated_params[:org][:instance].host_name.downcase}_#{encapsulated_params[:org][:instance].id}"
      email_subject = if file_exists?("#{org_folder}/email_subject/#{template_name}_subject.html.erb")
        render_template_to_string(template_name: "#{org_folder}/email_subject/#{template_name}_subject", vars: encapsulated_params)
      else
        render_template_to_string(template_name: "email_subject/#{template_name}_subject", vars: encapsulated_params)
      end

      layout = file_exists?("layouts/#{org_folder}_#{layout_name}.html.erb") ? "#{org_folder}_#{layout_name}" : layout_name
      email_body = if file_exists?("#{org_folder}/email_body/#{template_name}_body.html.erb")
        render_template_to_string(template_name: "#{org_folder}/email_body/#{template_name}_body", vars: encapsulated_params, layout_name: layout)
      else
        render_template_to_string(template_name: "email_body/#{template_name}_body", vars: encapsulated_params, layout_name: layout)
      end

      return [email_subject, email_body]
    end

    def get_email_content(encapsulated_params:, template_name:, layout_name:)
      if encapsulated_params[:org][:instance].mailer_config.present?
        email_subject, email_body = get_org_specific_template(encapsulated_params: encapsulated_params, template_name: template_name,
                                     layout_name: layout_name)
      else
        email_subject = render_template_to_string(template_name: "email_subject/#{template_name}_subject", vars: encapsulated_params)
        email_body = render_template_to_string(template_name: "email_body/#{template_name}_body",
                                                vars: encapsulated_params, layout_name: layout_name)
      end

      {
        email_subject: email_subject,
        email_body: email_body
      }
    end

    def send_email(encapsulated_params:, template_name:, layout_name:)
      # get custom content object
      #   if custom templates not found returns nil
      # else
      # {subject: email subject, body: html content, vars: global_merge_vars}
      custom_email_content = custom_content(
        template_name: template_name,
        encapsulated_params: encapsulated_params
      )

      customs_enabled = encapsulated_params[:org][:instance].get_settings_for(
        'emailTemplatesEnabled'
      )&.to_bool
      
      if customs_enabled && custom_email_content
        email_subject = custom_email_content[:subject]
        email_body = custom_email_content[:body]
        global_merge_vars = custom_email_content[:variables]
      else
        content = get_email_content(
          encapsulated_params: encapsulated_params,
          template_name: template_name,
          layout_name: layout_name
        )
        email_subject = content[:email_subject]
        email_body = content[:email_body]
        global_merge_vars = nil
      end
      
      send_email_mandrill(
        encapsulated_params[:org][:instance],
        encapsulated_params[:user],
        email_subject,
        email_body,
        global_merge_vars
      )
    end

    def send_email_mandrill(organization, user, subject, body, global_merge_vars)
      sender_email = organization.mailer_config.present? ? organization.mailer_config.from_address : 'admin@edcast.com'
      sender_name = organization.mailer_config.present? ? organization.mailer_config.from_name : 'EdCast'

      # PRECHECK: return if email of receiver not found
      if user[:email].blank?
        log.info <<~HEREDOC
          Email with subject #{subject} from #{sender_email}
          has no receiver. Email notification was not sent.
        HEREDOC

        return
      end

      message = {
        inline_css: true,
        preserve_recipients: false,
        important: false,
        url_strip_qs: false,
        track_clicks: true,
        track_opens: true,
        html: body,
        subject: subject,
        to: [{email: user[:email], type: 'to', name: user[:first_name]}],
        from_name: sender_name,
        from_email: sender_email
      }

      if global_merge_vars
        message[:global_merge_vars] = global_merge_vars
        message[:merge_language] = 'handlebars'
      end

      begin
        key = Settings.mandrill.password
        mandrill = Mandrill::API.new key
        result = mandrill.messages.send message, true, nil, nil
        log.info "Mandrill's response #{result}"
      rescue => e
        log.error "An error occurred while sending mail: #{e.class} - #{e.message}"
      end
    end

    def get_push_content(template_name:, encapsulated_params: )
      text = render_template_to_string(template_name:"push/#{template_name}_push", vars:encapsulated_params)
      encapsulated_params[template_name][0][:_push].merge!({content: text})
      encapsulated_params[template_name][0][:_push]
    end

    def send_push(encapsulated_params:, template_name:)
      organization = encapsulated_params[:org][:instance]
      push = get_push_content(template_name:template_name, encapsulated_params:encapsulated_params)
      push_notification = PushNotificationService.new(organization, push)

      if push_notification.payload_valid?
        push_notification.notify
      else
        log.error("Invalid payload: #{push}")
      end
    end

    def render_template_to_string(template_name:, vars:, layout_name:nil)
      if layout_name.present?
        layout = "notify/layouts/#{layout_name}"
      else
        layout = nil
      end

      template_path = "notify/#{template_name}"
      ActionController::Base.new.render_to_string template: template_path, layout: layout,
                                                  locals: {vars: vars}
    end

    def user_params(user)
      {
        first_name: user.first_name,
        last_name: user.last_name,
        email: user.email,
        org_name: user.organization.name
      }
    end

    #append http to org_logo url
    def append_http(org_logo = nil)
      uri = URI.parse(org_logo)
      uri.scheme = 'http'
      org_logo = uri.to_s
    end

    def get_co_branding_logo(org = nil)
      if org
        org_logo = org.co_branding_logo.present? ? org.co_branding_logo.url : nil
        append_http(org_logo) if org_logo
      end
    end

    def org_params(org)
      custom_options = org.custom_footer_options
      custom_options = custom_options.present? ? custom_options : {}
      name = (custom_options["supportEmail"] && (custom_options["supportEmail"] != Settings.support_email)) ? org.name : 'EdCast'
      {
        logo: get_co_branding_logo(org),
        instance: org,
        home_page: org.home_page.split('//').last,
        name: name,
        promotions: custom_options["promotions"] ,
        appstore_link: custom_options["appStore"] || Settings.ios_app_location,
        playstore_link: custom_options["playStore"] || Settings.android_app_location,
        support_email: custom_options["supportEmail"] || Settings.support_email
      }
    end

    def params_custom_css(org)
      custom_css = org.mail_custom_css_options
      custom_css = custom_css["enableCustomCss"] ? custom_css : {}
      {
        header_background_color: custom_css["headerBackgroundColor"] || "##{Settings.mail_css.header_background_color}",
        text_color: custom_css["textColor"] || "##{Settings.mail_css.text_color}",
        highlight_text_color: custom_css["textColor"] || "##{Settings.mail_css.highlight_text_color}",
        section_text_color: custom_css["textColor"] || "##{Settings.mail_css.section_text_color}",
        card_text_color: custom_css["textColor"] || "##{Settings.mail_css.card_text_color}",
        button_background_color: custom_css["buttonBackgroundColor"] || "##{Settings.mail_css.button_background_color}"
      }
    end

    def global_params(user)
      params_for_user = user_params(user)
      params_for_org = org_params(user.organization)

      {
        user: params_for_user,
        org: params_for_org,
        custom_css: params_custom_css(user.organization),
        show_ed_logo: user.organization.enable_edcast_logo
      }
    end

    def notification_params(notification_entry)
      notifier = notification_name_to_notifiers[notification_entry.notification_name]
      notifier.notification_params(notification_entry)
    end

    def push_notification_params(notification_entry)
      notifier = notification_name_to_notifiers[notification_entry.notification_name]

      notifier.push_notification_params(notification_entry)
    end

    def register_notifier(notifier_obj, events)
      notification_name_to_notifiers[notifier_obj.notification_name] = notifier_obj

      events.each do |event|
        # Initialize if necessary
        events_to_notifiers[event] = [] if events_to_notifiers[event].nil?

        # Add to it
        events_to_notifiers[event] << notifier_obj
      end
    end

    def notification_enabled?(notification_name, medium, interval)
      notification_name_to_notifiers[notification_name].get_enabled(medium, interval)
    end

    def resolve_config(notification_name, medium, interval, org_config, user_config)
      notification_name_to_notifiers[notification_name].resolve_config(medium, interval, org_config, user_config)
    end

    def resolve_configurable(notification_name, org_config)
      notification_name_to_notifiers[notification_name].resolve_configurable(org_config)
    end

    # custom template methods below
    #   TODO: to refactor it in later requirements changes
    def custom_content(template_name:, encapsulated_params:)
      user_email = encapsulated_params[:user][:email]
      org = encapsulated_params[:org][:instance]
      user_language = org.users.find_by(email: user_email)&.language
      # get default organization language
      default_language = org.get_settings_for('DefaultOrgLanguage') || 'en'

      default_template = org.email_templates.find_by(title: template_name)
      custom_template = default_template&.custom_templates&.find_by(
        language: user_language || default_language,
        is_active: true,
        state: 'published'
      )
      return unless custom_template
      
      email_subject = if custom_template && custom_template.subject.present?
        custom_template.subject
      else
        render_template_to_string(
          template_name: "email_subject/#{template_name}_subject",
          vars: encapsulated_params
        )
      end
      
      dynamic_variables = EmailTemplate.extract_variables(
        template_content: custom_template.content
      )

      global_merge_vars, to_html_parse = merge_params_content(
        key_names: dynamic_variables,
        params: encapsulated_params,
        template_name: template_name
      )

      email_prepared_body = to_htm_safe(custom_template.content, to_html_parse)

      # prepared custom template and variables content
      {
        subject: email_subject,
        body: email_prepared_body,
        variables: global_merge_vars
      }
    end

    def merge_params_content(key_names:, params:, template_name:)
      # prepare params values
      data = sanitized_variables(params)
      sanitized_keys = key_names.map { |i| i.split('.').first }.uniq
      merge_params = []
      to_safe_html = []
      sanitized_keys.each do |key|
        content = data.dig(key).presence || data[template_name].first.dig(key)
        next unless content
        merge_params.push({name: key.to_s, content: content})
        # get array of variables keys name in template which might contain html tags
        to_safe_html.push(*extract_tags(content, key))
      end
      [merge_params.compact, to_safe_html]
    end

    # extract key map of values which may content html tag
    # e.g ["assignment.title"]
    # variables like that {{assignment.title}} could be placed in custom templates
    def extract_tags(data, key, res = [])
      return res.push(key) if data.is_a?(String) && is_tag?(data)
      data.each { |k, v| extract_tags(v, key + '.' + k, res) } if data.is_a?(Hash)
      res
    end

    # dynamic_variable -> content html tags
    # changed {{dynamic_variable}} -> {{{...}}}
    # {{{...}}} -> `handlebars` mandrill syntax, to handle html tags in variable
    def to_htm_safe(body, keys)
      body.gsub(/(?<outer>{{(?<inner>.*?)}})/) do |match|
        inner = Regexp.last_match[:inner] # variable name
        outer = Regexp.last_match[:outer] # all match with brackets
        inner.in?(keys) ? '{' + outer + '}' : match
      end
    end

    def is_tag?(str)
      return unless str.is_a?(String)
      Nokogiri::XML.parse(str).errors.empty?
    end

    # return hash with sanitized html tag string values
    # e.g of value "<p>text..." -> "<p>text...</p>"
    # goes through all level nesting in hash
    #   and changed only Strings with unclosed html tags
    def sanitized_variables(data_hash)
      return unless data_hash.is_a?(Hash)
      data_hash.each do |k, v|
        data_hash[k] = sanitize_values(v) if v.is_a?(String) || v.is_a?(Array)
        sanitized_variables(v)
      end
    end

    def sanitize_values(v)
      return close_tags(v) if v.is_a?(String)
      return [sanitized_variables(v.first)] if v.is_a?(Array) && v[0].is_a?(Hash)
      return v unless v.is_a?(Array)
      v.map do |el|
        el = close_tags(el) if el.is_a?(String)
        sanitized_variables(el) if el.is_a?(Hash)
        el
      end
    end

    def close_tags(v)
      return v unless v.is_a?(String)
      Nokogiri::HTML::DocumentFragment.parse(v).to_html
    end
  end
end

module Notify
  class AddedCollaboratorNotifier < Notifier
    def setup
      self.notification_name = NOTE_ADDED_COLLABORATOR
      
      set_enabled(:email, :single)
      set_enabled(:email, :digest_daily)
      set_default(:email, :single, false)

      set_enabled(:push, :single)
      set_enabled(:push, :digest_daily)
      set_default(:push, :single, false)
    end

    def users_to_notify(event_name, channel_author)
      
      # Only where there is a mention in a comment
      if channel_author.user.nil?
        yield []
        return
      end
      # Return the user who is mentioned in the comment.
      yield [channel_author.user.id]
    end

    def get_organization(channel_author)
      channel_author.channel.organization
    end

    def push_notification_params(notification_entry)
      channel_author = notification_entry.sourceable

      {
        user_id: channel_author.user.id,
        deep_link_id: channel_author.channel.id,
        channel_name: channel_author.channel.label,
        deep_link_type: 'Channel',
        notification_type: self.notification_name,
        host_name: get_organization(channel_author).home_page
      }
    end

    def notification_params(notification_entry)
      channel_author = notification_entry.sourceable
      channel = channel_author.channel
      user = channel_author.user
      {
        first_name: user.first_name,
        invite_url: channel.channel_url,
        channel_name: channel.label,
        host_name: get_organization(channel_author).home_page
      }
    end
  end
end

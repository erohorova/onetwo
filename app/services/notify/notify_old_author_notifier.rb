module Notify
  class NotifyOldAuthorNotifier < Notifier
    def setup
      self.notification_name = NOTE_NOTIFY_OLD_AUTHOR

      enable_additional_content
    end

    def users_to_notify(event_name, card, custom_hash)
      if custom_hash.present?
        yield [custom_hash[:old_author_id]]
      else
        yield []
      end
    end

    def get_organization(card)
      card.organization
    end

    def push_notification_params(notification_entry)
      card = notification_entry.sourceable

      {
        user_id: notification_entry.user_id,
        deep_link_id: card.id,
        deep_link_type: 'Card',
        notification_type: self.notification_name,
        host_name: get_organization(card).home_page
      }
    end

    def notification_params(notification_entry)
      card = notification_entry.sourceable

      {
        user_id: notification_entry.user_id,
        card_title: card.snippet_html,
        card_url: card.card_deeplink_url
      }
    end
  end
end

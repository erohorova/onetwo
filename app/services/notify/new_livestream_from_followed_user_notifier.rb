module Notify
  class NewLivestreamFromFollowedUserNotifier < Notifier
    def setup
      self.notification_name = NOTE_NEW_LIVESTREAM_FROM_FOLLOWED_USER

      set_enabled(:email, :single)

      set_enabled(:push, :single)
      set_default(:push, :single, false)
    end

    def users_to_notify(event_name, livestream)
      #here livestream is nothing but a card only
      
      # when author is present and followers is empty, notification will not be sent
      # if livestream created is private, no followers will be notified
      if !livestream.is_public?
        yield []
        return
      end 
      
      #find creators followers
      Notify.batchify(livestream.author.followers.where.not(id: livestream.author_id).select(:id), 1000) do |users|
        yield users.map(&:id)
      end
    end

    def get_organization(livestream)
      livestream.organization
    end

    def push_notification_params(notification_entry)
      livestream = notification_entry.sourceable
      {
        user_id: notification_entry.user_id,
        deep_link_id: livestream.id,
        deep_link_type: 'card',
        notification_type: self.notification_name,
        host_name: livestream.organization.home_page
      }
    end

    def notification_params(notification_entry)
      livestream = notification_entry.sourceable

      {
        user_name: livestream.author.try(:name),
        receiver_name: notification_entry.user.try(:name),
        title: livestream.snippet,
        title_simplified: simplify_encoded_string(livestream.snippet).truncate(100, separator: ' '),
        join_url: livestream.card_deeplink_url
      }
    end
  end
end

module Notify
  class AddedCuratorNotifier < Notifier
    def setup
      self.notification_name = NOTE_ADDED_CURATOR
      
      set_enabled(:email, :single)
      set_enabled(:email, :digest_daily)
      set_default(:email, :single, false)

      set_enabled(:push, :single)
      set_enabled(:push, :digest_daily)
      set_default(:push, :single, false)
    end

    def users_to_notify(event_name, channel_curator)
      if channel_curator.nil? || channel_curator.user.nil?
        yield []
        return
      end

      yield [channel_curator.user_id]
    end

    def get_organization(channel_curator)
      channel_curator.channel.organization
    end

    def push_notification_params(notification_entry)
      channel_curator = notification_entry.sourceable
      {
        user_id: channel_curator.user.id,
        deep_link_id: channel_curator.channel.id,
        channel_name: channel_curator.channel.label,
        deep_link_type: 'Channel',
        notification_type: self.notification_name,
        host_name: get_organization(channel_curator).home_page
      }
    end

    def notification_params(notification_entry)
      channel_curator = notification_entry.sourceable
      channel = channel_curator.channel
      user = channel_curator.user
      {
        first_name: user.first_name,
        invite_url: channel.organization.host + '/curate',
        channel_name: channel.label,
        host_name: get_organization(channel_curator).home_page
      }
    end
  end
end

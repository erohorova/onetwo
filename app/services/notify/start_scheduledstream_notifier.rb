module Notify
  class StartScheduledstreamNotifier < Notifier
    def setup
      self.notification_name = NOTE_START_SCHEDULEDSTREAM
      set_enabled(:email, :single)
      set_default(:email, :single, false)

      set_enabled(:push, :single)
      set_default(:push, :single, false)
    end

    def users_to_notify(event_name, scheduledstream)
      if scheduledstream.card_type.to_s != 'video_stream' || !scheduledstream.video_stream.status == 'upcoming'
        yield []
        return
      end
      #find creator of scheduledstream
      author = scheduledstream.author

      #send the creator notification
      yield [author.id]
    end

    def get_organization(scheduledstream)
      scheduledstream.author.organization
    end

    def push_notification_params(notification_entry)
      scheduledstream = notification_entry.sourceable
      {
        user_id: notification_entry.user_id,
        deep_link_id: scheduledstream.id,
        deep_link_type: 'card',
        notification_type: self.notification_name,
        host_name: scheduledstream.organization.home_page
      }
    end

    def notification_params(notification_entry)
      scheduledstream = notification_entry.sourceable
      scheduledstream_author = scheduledstream.author
      scheduledstream_link = "#{scheduledstream.organization.host_name}.edcast.com/insights/#{scheduledstream.id}"

      {
        user_name: scheduledstream_author.try(:name),
        title: scheduledstream.title,
        join_url: scheduledstream_link
      }
    end
  end
end
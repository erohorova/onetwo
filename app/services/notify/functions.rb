module Notify
  def self.batchify(query, batch_size)
    offset = 0

    loop do
      result = query.limit(batch_size).offset(offset)
      yield result

      break if result.size < batch_size
      offset += batch_size
    end
  end

  def self.field_name(medium, interval)
    "#{medium}_#{interval}"
  end

  def self.config_field_name(medium, interval)
    "config_#{field_name(medium, interval)}"
  end

  def self.status_field_name(medium, interval)
    "status_#{field_name(medium, interval)}"
  end

  def self.get_shared_user_ids(custom_hash, card_id)
    user_ids = []
    
    user_ids << custom_hash[:permitted_user_ids] if custom_hash[:permitted_user_ids].present?
    
    if custom_hash[:permitted_team_ids].present?
      user_ids << TeamsUser.where(team_id: custom_hash[:permitted_team_ids]).pluck(:user_id)
    end

    # if specific permissions are provided then these users will be notified
    return user_ids if user_ids.present?

    if custom_hash[:user_ids].present?
      user_ids << CardUserShare.where(card_id: card_id, user_id: custom_hash[:user_ids]).pluck(:user_id)
    end
    if custom_hash[:team_ids].present?
      user_ids << TeamsUser.joins(team: :teams_cards).where(teams_cards: {
        team_id: custom_hash[:team_ids], card_id: card_id, type: 'SharedCard'}).pluck(:user_id)
    end
    return user_ids.flatten.compact.uniq
  end
end

module Notify
  class ContentSharedNotifier < Notifier
    def setup
      self.notification_name = NOTE_CONTENT_SHARED

      set_enabled(:email, :single)
      set_enabled(:email, :digest_daily)
      set_default(:email, :single, false)

      set_enabled(:push, :single)
      set_default(:push, :single, false)
      enable_additional_content
    end

    def get_organization(card)
      card.organization
    end

    def users_to_notify(event_name, card, custom_hash)
      if custom_hash.present?
        user_ids = Notify.get_shared_user_ids(custom_hash, card.id)
        if custom_hash[:event] == Card::EVENT_NEW_CARD_SHARE
          # do not notify users who are only author's followers
          user_ids = user_ids - [card.author.try(:followers).try(:pluck, :id)]
        elsif custom_hash[:event] == Card::EVENT_CARD_SHARE
          # do not notify users who are already notified
          user_ids = user_ids - get_earlier_notified_users(card, custom_hash)
        end
        author_share_by_user_id = custom_hash[:share_by_id] ? custom_hash[:share_by_id] : card.author_id
        user_ids = user_ids.flatten.compact.uniq
        user_ids.delete(author_share_by_user_id)
        Notify.batchify(User.where(id: user_ids, organization_id: card.organization_id).not_suspended.select(:id), 1000) do |user|
          yield user.map(&:id)
        end
      else
        yield []
      end
    end

    def get_earlier_notified_users(card, custom_hash)
      custom_hash = {
        user_ids: card.users_with_access_ids -  custom_hash[:user_ids].to_a,
        channel_ids: card.channel_ids - custom_hash[:channel_ids].to_a,
        team_ids: card.shared_with_team_ids - custom_hash[:team_ids].to_a
      }
      return Notify.get_shared_user_ids(custom_hash, card.id) || []
    end

    def push_notification_params(notification_entry)
      card = notification_entry.sourceable
      {
        user_id: notification_entry.user_id,
        deep_link_id: card.id,
        deep_link_type: 'card',
        notification_type: self.notification_name,
        host_name: get_organization(card).home_page
      }
    end

    def notification_params(notification_entry)
      card = notification_entry.sourceable
      # if non-ugc card is shared or ugc card shared by someone else
      # then it will take id of user who shared card
      if notification_entry.additional_content[:share_by_id].present?
        shared_by = User.find_by_id(notification_entry.additional_content[:share_by_id])
      else
        shared_by = card.author
      end
      {
        card: {
          title: card.snippet_html,
          url: card.card_deeplink_url,
          image_url: card.fetch_image_attachment_url,
          shared_by_name: shared_by.try(:name),
          shared_by_image: shared_by.try(:photo),
          content_type: card.card_type
        }
      }
    end
  end
end

module Notify
  class NewFollowerNotifier < Notifier
    def setup
      self.notification_name = NOTE_NEW_FOLLOWER

      set_enabled(:email, :single)
      set_enabled(:email, :digest_daily)
      set_default(:email, :digest_daily, false)
 
      set_enabled(:push, :single)
      set_default(:push, :single, false)
    end

    def users_to_notify(event_name, follow)
      # Only where followable_type is User
      if follow.followable_type != 'User'
        yield []
        return
      end

      # Return the followed user id to be notified
      yield [follow.followable_id]
    end

    def get_organization(follow)
      follow.user.organization
    end

    def push_notification_params(notification_entry)
      follow = notification_entry.sourceable
      {
        user_id: notification_entry.user_id,
        deep_link_id: follow.user_id,
        deep_link_type: 'user',
        notification_type: self.notification_name,
        host_name: get_organization(follow).home_page
      }
    end

    def notification_params(notification_entry)
      follow = notification_entry.sourceable
      follower = follow.user

      {
        follower_name: follower.try(:name),
        image_url: follower.try(:photo),
        follower_link: URI.parse("#{get_organization(follow).home_page}/#{follower.handle}").to_s
      }
    end
  end
end

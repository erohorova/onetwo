module Notify
  class RemindedCuratorNotifier < Notifier
    def setup
      self.notification_name = NOTE_REMINDED_CURATOR
      
      set_enabled(:email, :single)
      set_default(:email, :single, false)
    end

    def users_to_notify(event_name, curator)
      yield [curator.id]
    end

    def get_organization(curator)
      curator.organization
    end

    def push_notification_params(notification_entry)
      curator = notification_entry.sourceable
      {
        user_id: notification_entry.user_id,
        deep_link_id: curator.id,
        deep_link_type: 'Channel',
        notification_type: self.notification_name,
        host_name: get_organization(curator).home_page
      }
    end

    def notification_params(notification_entry)
      curator = notification_entry.sourceable
      channels = []
      curator.channels_to_curate.pluck(:channel_id).first(3).each do |channel_id|
        channel = Channel.find_by_id(channel_id)
        channels << {name: channel.label, url:  channel.channel_url}
      end

      return {
        user_name: curator.name,
        channels: channels,
        curate_url: curator.organization.home_page + '/curate',
        team_name: curator.organization.try(:name)
      }
    end
  end
end

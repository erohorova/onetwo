module Notify
  class NewSmartbiteCommentNotifier < Notifier
    def setup
      self.notification_name = NOTE_NEW_SMARTBITE_COMMENT

      set_enabled(:email, :single)
      set_enabled(:email, :digest_daily)
      set_default(:email, :single, false)

      set_enabled(:push, :single)
      set_default(:push, :single, false)
    end

    def users_to_notify(event_name, comment)
      # Only support comments on cards
      if comment.commentable.class.to_s != 'Card' || (comment.commentable.author_id == comment.user_id) || !comment.commentable.ugc?
        yield []
        return
      end

      # Return the author of the card
      yield [comment.commentable.author_id]
    end

    def get_organization(comment)
      comment.organization
    end

    def push_notification_params(notification_entry)
      comment = notification_entry.sourceable
      
      {
        user_id: notification_entry.user_id,
        deep_link_id: comment.commentable_id,
        deep_link_type: 'card',
        item: comment.slice(:id, :message, :commentable_id, :commentable_type, :votes_count, :resource_id, :is_mobile, :hidden), 
        notification_type: self.notification_name,
        host_name: comment.organization.home_page
      }
    end

    def notification_params(notification_entry)
      comment = notification_entry.sourceable

      { 
        content: comment.message,
        commenter: {
          first_name: comment.user.first_name,
          last_name: comment.user.last_name
        },
        card: {
          title: comment.commentable.snippet_html,
          title_simplified: simplify_encoded_string(comment.commentable.snippet_html).truncate(100, separator: ' '),
          assignment_url: comment.commentable.card_deeplink_url,
          image_url: comment.commentable.fetch_image_attachment_url
        }
      }
    end
  end
end

module Notify
  class NewContentByFollowedUserNotifier < Notifier
    def setup
      self.notification_name = NOTE_NEW_CONTENT_BY_FOLLOWED_USER

      set_enabled(:email, :single)
      set_enabled(:email, :digest_daily)
      set_default(:email, :single, false)

      set_enabled(:push, :single)
      set_default(:push, :single, false)
      enable_additional_content
    end

    def get_organization(card)
      card.organization
    end

    def users_to_notify(event_name, card, custom_hash)
      # if new card is shared by author do not notify followers who are also
      # group members, channel members, explicitly shared users
      users_not_to_notify = [card.author_id]
      if custom_hash.present? && custom_hash[:event] == Card::EVENT_NEW_CARD_SHARE
        users_not_to_notify << Notify.get_shared_user_ids(custom_hash, card.id)
      end
      
      users_not_to_notify = users_not_to_notify.flatten.compact.uniq
      # Return the followers of the card's author if the card is accessible to them and excluding users_not_to_notify
      Notify.batchify(card.author.followers.where.not(id: users_not_to_notify), 1000) do |group|
        group = group.select{|user| card.visible_to_user(user)}
        yield group.map(&:id)
      end
    end

    def push_notification_params(notification_entry)
      card = notification_entry.sourceable
      {
        user_id: notification_entry.user_id,
        deep_link_id: card.id,
        deep_link_type: 'card',
        notification_type: self.notification_name,
        host_name: get_organization(card).home_page
      }
    end

    def notification_params(notification_entry)
      card = notification_entry.sourceable
      {
        card: {
          title: card.snippet_html,
          title_simplified: simplify_encoded_string(card.snippet_html).truncate(100, separator: ' '),
          url: card.card_deeplink_url,
          image_url: card.fetch_image_attachment_url,
          author_name: card.author.name,
          author_image: card.author.try(:photo),
          content_type: (card.card_type == 'pack' ? 'Pathway' : 'SmartCard')
        }
      }
    end
  end
end

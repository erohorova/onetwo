module Notify
  class ChangedAuthorNotifier < Notifier
    def setup
      self.notification_name = NOTE_CHANGED_AUTHOR
    end

    def users_to_notify(event_name, card)
      # Return the card's author
      yield [card.author_id]
    end

    def get_organization(card)
      card.organization
    end

    def push_notification_params(notification_entry)
      card = notification_entry.sourceable

      {
        user_id: notification_entry.user_id,
        deep_link_id: card.id,
        deep_link_type: 'Card',
        notification_type: self.notification_name,
        host_name: get_organization(card).home_page
      }
    end

    def notification_params(notification_entry)
      card = notification_entry.sourceable

      {
        user_id: notification_entry.user_id,
        card_title: card.snippet_html,
        card_url: card.card_deeplink_url
      }
    end
  end
end

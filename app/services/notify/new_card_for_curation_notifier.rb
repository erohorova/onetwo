module Notify
  class NewCardForCurationNotifier < Notifier
    def setup
      self.notification_name = NOTE_NEW_CARD_FOR_CURATION

      set_default(:push, :single, false)

      set_enabled(:email, :single)
      set_enabled(:email, :digest_daily)
      set_default(:email, :single, false)
    end

    def get_organization(channel)
      channel.organization
    end

    def users_to_notify(event_name, channel)
      if !channel || channel.curators.empty?
       yield []
       return
      end

      # Return the channel's curators
      Notify.batchify(channel.curators.select(:id), 1000) do |curators|
        yield curators.map(&:id)
      end
    end

    def push_notification_params(notification_entry)
      channel = notification_entry.sourceable
      {
        user_id: notification_entry.user_id,
        deep_link_id: channel.id,
        deep_link_type: 'channel', 
        notification_type: self.notification_name,
        host_name: get_organization(channel).home_page
      }
    end

    def notification_params(notification_entry)
      channel = notification_entry.sourceable

      {
        channel: {
          name: channel.label,
          channel_url: channel.channel_url
        },
        team_name: channel.organization.try(:name)
      }
    end
  end
end

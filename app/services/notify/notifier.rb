module Notify
  class Notifier
    attr_accessor :notification_name

    def initialize
      @enabled = {}
      @defaults = {}
      @additional_content = false

      set_enabled(:web, :single)
      set_default(:web, :single, false)

      setup
    end

    def setup
      # Override in the base class
    end

    def users_to_notify(event_name, source_obj)
      raise NotImplementedError
    end

    def get_organization(source_obj)
      raise NotImplementedError
    end

    def notification_params(notification_entry)
      raise NotImplementedError
    end

    def layout_name
      'default_layout'
    end

    # create only new NotificationEntry object and pass for creating web notification in Notification Table
    def execute_for_notification_web_only(event_name, source_obj, custom_hash = {})
      organization = get_organization(source_obj)
      args_to_pass = custom_hash.present? ? [event_name, source_obj, custom_hash] : [event_name, source_obj]
      users_to_notify(*args_to_pass) do |user_ids|
        user_ids.each do |user_id|
          notification = NotificationEntry.new(
            user_id: user_id,
            event_name: event_name,
            notification_name: notification_name,
            sourceable: source_obj
          )
          notification.additional_content = custom_hash if additional_content_enabled?
          yield notification if block_given?
        end
      end
    end

    def execute_for_event(event_name, source_obj, custom_hash = {})
      # STEP 1: Get users to notify about this event, if any. This is overridden
      # in the child class
      # A block is returned for efficiency
      organization = get_organization(source_obj)
      args_to_pass = custom_hash.present? ? [event_name, source_obj, custom_hash] : [event_name, source_obj]
      users_to_notify(*args_to_pass) do |user_ids|
        # STEP 2: Get notification configuration for all the concerned users
        user_ids_with_settings = get_users_with_settings user_ids, organization
        # STEP 3: For each record, create a new notification
        user_ids_with_settings.each do |record|
          # STEP A: Create a notification entry
          notification = NotificationEntry.new(
            user_id: record[:user_id],
            event_name: event_name,
            notification_name: notification_name,
            sourceable: source_obj,
            additional_content: additional_content_enabled? ? custom_hash : nil,

            date_in_timezone: record[:date_in_timezone],
            sending_time_in_timezone: record[:sending_time]
          )

          # STEP B: Update notification config with the configs
          record[:configs].each do |record_config|
            medium = record_config[:medium]
            interval = record_config[:interval]
            enabled = record_config[:enabled]

            notification.send("#{Notify.config_field_name(medium, interval)}=", enabled)
            notification.send("#{Notify.status_field_name(medium, interval)}=", Notify::STATUS_UNSENT)
          end

          # STEP C: Save the notification

          # STEP D: Yield the notification if required
          if notification.save
            yield notification if block_given?
          end
        end
      end

      nil
    end

    # Input is an array of user ids
    # [1, 2, 3]
    # Output is enhanced user ids
    # [
    #  {user_id: 1, enabled_configs: [{medium: '', interval: ''}, ...]},
    #  ...
    # ]
    def get_users_with_settings(user_ids, organization)
      # STEP 1: Get notification configs indexed by user_id
      notification_configs_indexed = NotificationConfig.where(user_id: user_ids)
                                                       .index_by(&:user_id)
      organization_config = NotificationConfig.find_by(organization: organization)
      user_timezones_indexed = UserProfile.where(user_id: user_ids).select(:user_id, :time_zone)
                                          .map{|x| [x[:user_id], x[:time_zone]]}.to_h

      # STEP 2: Loop through all the users and create configs for all mediums
      output = []
      user_ids.each do |user_id|
        user_sending_time = notification_time(
          organization_config.try(:notification_time),
          user_timezones_indexed[user_id]
        )
        user_output = {user_id: user_id,
                       date_in_timezone: Time.find_zone(user_timezones_indexed[user_id] || default_timezone).today,
                       sending_time: user_sending_time, configs: []}

        MEDIUMS.each do |medium|
          INTERVALS.each do |interval|
            # Only :single allowed for :web
            next if medium == :web && interval != :single

            # Get the enabled flag
            if get_enabled(medium, interval)
              enabled = resolve_config(medium, interval,
                                       organization_config,
                                       notification_configs_indexed[user_id])
            else
              enabled = false
            end

            # Add to the list
            user_output[:configs] << {medium: medium, interval: interval, enabled: enabled}
          end
        end

        output << user_output
      end

      # STEP 4: Return the output
      output
    end

    def resolve_config(medium, interval, org_config, user_config)
      # STEP 1: Try the user config
      # pick the user level config only if the org level notification is set to user_configurable
      # and the notification has not been switched off by the admin for the org
      
      if org_config.try(:configurable?, notification_name) && org_config.try(:enabled?, notification_name, medium, interval)
        config = user_config.try(:enabled?, notification_name, medium, interval) 
      end

      # STEP 2: Try the org config
      if config.nil?
        config = org_config.try(:enabled?, notification_name, medium, interval)
      end

      # STEP 3: Try the default config
      config = get_default(medium, interval) if config.nil?

      # STEP 4: Default to false
      config = false if config.nil?

      config
    end

    def resolve_configurable(org_config)
      configurable = org_config.try(:configurable?, notification_name)
    end

    def notification_time(org_notification_time=nil, user_timezone=nil)
      time = org_notification_time || default_time
      timezone = user_timezone || default_timezone

      today = Time.find_zone(timezone).today
      hour = time.split(':')[0]
      minute = time.split(':')[1]

      sending_time = Time.find_zone(timezone).local(today.year, today.month, today.day, hour, minute)

      # If the time has elapsed, move to the next day
      if sending_time < Time.find_zone(timezone).now
        sending_time += 1.day
      end

      sending_time
    end

    def set_enabled(medium, interval)
      @enabled[Notify.field_name(medium, interval)] = true
    end

    def set_disabled(medium, interval)
      @enabled[Notify.field_name(medium, interval)] = false
    end

    def get_enabled(medium, interval)
      @enabled[Notify.field_name(medium, interval)]
    end

    def set_default(medium, interval, value)
      @defaults[Notify.field_name(medium, interval)] = value
    end

    def get_default(medium, interval)
      @defaults[Notify.field_name(medium, interval)]
    end

    def enable_additional_content
      @additional_content = true
    end

    def additional_content_enabled?
      @additional_content
    end

    def default_timezone
      'America/Los_Angeles'
    end

    def default_time
      '17:00'
    end
  end
end

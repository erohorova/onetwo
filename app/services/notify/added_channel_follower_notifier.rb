module Notify
  class AddedChannelFollowerNotifier < Notifier
    def setup
      self.notification_name = NOTE_ADDED_CHANNEL_FOLLOWER
      set_enabled(:email, :single)
      set_enabled(:email, :digest_daily)
      set_default(:email, :single, false)

      set_enabled(:push, :single)
      set_enabled(:push, :digest_daily)
      set_default(:push, :single, false)
    end

    def users_to_notify(event_name, follow)
      if follow.user.nil?
        yield []
        return
      end

      #find channel follower
      follower = follow.user

      #send the follower notification
      yield [follower.id]
    end

    def get_organization(follow)
      follow.followable.organization
    end

    def push_notification_params(notification_entry)
      follow = notification_entry.sourceable
      channel = Channel.where(id: follow.followable_id).first

      {
        user_id: follow.user_id,
        channel_id: channel.id,
        channel_name: channel.label,
        deep_link_type: 'Channel',
        notification_type: self.notification_name,
        host_name: channel.organization.home_page
      }
    end

    def notification_params(notification_entry)
      follow = notification_entry.sourceable
      channel = Channel.where(id: follow.followable_id).first
      user = follow.user
      organization = get_organization(follow)

      {
        first_name: user.first_name,
        invite_url: channel.channel_url,
        channel_name: channel.label,
        host_name: get_organization(follow).home_page
      }
    end
  end
end
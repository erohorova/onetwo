module Notify
  class NotifySettings
    attr_accessor :engine

    def initialize(engine)
      self.engine = engine
    end

    def save_settings(notification_config, settings)
      # STEP 1: Loop through all notifications
      settings.each do |notification_name, options|
        # STEP 2: Now, loop through all mediums
        Notify::SETTINGS_MEDIUMS.each do |medium|
          # STEP 3: Retrieve medium options
          medium_id = medium[:id]
          medium_options = options[medium_id]

          # STEP 4: Disable all intervals for the medium
          INTERVALS.each do |interval|
            notification_config.set_disabled(notification_name, medium_id, interval)
          end

          # STEP 5: Loop through all medium options, and get the first selected one
          selected = :off
          medium_options.each do |medium_option|
            if medium_option[:selected] && INTERVALS.include?(medium_option[:id].to_sym)
              selected = medium_option[:id]
              break
            end
          end

          # STEP 6: Enable the selected option, unless it's :off
          if selected != :off
            notification_config.set_enabled(notification_name, medium_id, selected)
          end
        end
        unless options[:user_configurable].nil?
          notification_config.set_configurable(notification_name, options[:user_configurable]) 
        end
      end
      
      # STEP 7: Save the notification config
      notification_config.save
    end

    def get_settings(organization_id, user_id=nil)
      org_config = NotificationConfig.find_by(organization_id: organization_id)

      if user_id.present?
        user_config = NotificationConfig.find_by(user_id: user_id)
      else
        user_config = nil
      end
      settings_by_notification = {}

      # STEP 1: Loop through all the notification groups
      Notify::SETTINGS_GROUPS.each do |group|

        # STEP 2: Loop through each notification in the group
        group[:notifications].each do |notification|
          # STEP 3: Initialize options for the notification
          notification_options = {}

          # STEP 4: Loop through all the mediums
          Notify::SETTINGS_MEDIUMS.each do |medium|
            # STEP 5: Initialize the options array for the given medium
            medium_id = medium[:id]
            medium_options = []
            mapping = notification[:mapping].merge(off: :off)

            # STEP 6: Fill up medium options
            Notify::SETTINGS_OPTIONS.each do |option|
              db_id = mapping[option[:id]]
              next if db_id.nil?

              medium_options << {label: option[:label], id: db_id, selected: false}
            end

            # STEP 7: Remove options not available for the notification
            medium_options = medium_options.select do |medium_option|
              # Off is always visible
              if medium_option[:id] == :off
                true
              else
                engine.notification_enabled?(notification[:id], medium_id, medium_option[:id])
              end
            end

            # STEP 8: For every medium option, mark as selected. Mark off as a fallback
            off_selected = true
            medium_options.each do |medium_option|
              # Return Off as it is
              next if medium_option[:id] == :off

              enabled = engine.resolve_config(notification[:id], medium_id, medium_option[:id],
                                              org_config, user_config)
              if enabled
                medium_option[:selected] = true
                off_selected = false
                break
              end
            end
            medium_options[0][:selected] = true if off_selected

            # STEP 9: Add the medium options to the notification options
            notification_options[medium_id] = medium_options
          end
          configurable = engine.resolve_configurable(notification[:id], org_config) if org_config.present?
          notification_options[:user_configurable] = configurable
          # STEP 10: Add notification settings to the final settings
          settings_by_notification[notification[:id]] = notification_options
        end
      end
      # STEP 11: Return the final settings
      {
        groups: Notify::SETTINGS_GROUPS,
        mediums: Notify::SETTINGS_MEDIUMS,
        options: settings_by_notification
      }
    end
  end
end

module Notify
  # Status
  STATUS_UNSENT = 0
  STATUS_SENT = 1
  STATUS_ERROR = 2
  STATUS_READ = 3

  # Types of notifications
  NOTIFICATION_TYPES = [
    {name: :single, type: :immediate},
    {name: :digest_daily, type: :digest, duration: 1.day, lookback_duration: 3.day},
    {name: :digest_weekly, type: :digest, duration: 7.days, lookback_duration: 14.days}
  ].freeze

  INTERVALS = NOTIFICATION_TYPES.map {|x| x[:name]}
  DURATIONS = NOTIFICATION_TYPES.select{|x| x[:type] == :digest}.map{|x| [x[:name], x[:duration]]}.to_h
  LOOKBACK_DURATIONS = NOTIFICATION_TYPES.select{|x| x[:type] == :digest}.map{|x| [x[:name], x[:lookback_duration]]}.to_h

  # Mediums available
  MEDIUMS = [:email, :push, :web].freeze

  # Move web notification to new framework temp. array, Once we move all web notification remove array
  # and it's dependency in engine.rb:23, engine.rb:29

  WEB_NOTIFICATIONS_HANDLED = %w(curate_card
                                 skip_card
                                 change_author
                                 notify_old_author
                                 add_curator
                                 add_collaborator
                                 group_user_promoted_to_admin
                                 group_user_promoted_to_sub_admin).freeze

  # Valid events that can be raised
  VALID_EVENTS = %i(new_comment
                    new_bookmark
                    new_livestream
                    new_follow
                    new_card
                    new_assignment
                    new_content_in_channel
                    complete_assignment
                    remind_curator
                    time_to_start_scheduledstream
                    add_curator
                    add_collaborator
                    add_channel_follower
                    new_card_for_curation
                    curate_card
                    skip_card
                    add_channel_follower_admin
                    change_author
                    notify_old_author
                    content_share
                    group_user_promoted_to_admin
                    group_user_promoted_to_sub_admin
                    live_stream_in_group).freeze

  VALID_EVENTS.each do |event_name|
    const_set("EVENT_#{event_name.to_s.upcase}", event_name.to_s)
  end

  # Valid notifications that can be sent
  VALID_NOTIFICATIONS = %i(new_smartbite_comment
                           mention_in_comment
                           new_activity_commented_smartbite
                           new_livestream_from_followed_user
                           new_follower
                           assigned_content
                           new_content_in_channel
                           new_content_by_followed_user
                           new_smartbite_like
                           new_smartbite_bookmark
                           completed_assignment
                           reminded_curator
                           start_scheduledstream
                           added_curator
                           added_collaborator
                           added_channel_follower
                           new_card_for_curation
                           curated_card
                           skipped_card
                           added_channel_follower_admin
                           changed_author
                           notify_old_author
                           content_shared
                           group_user_promoted_to_admin
                           group_user_promoted_to_sub_admin
                           live_stream_in_group).freeze

  VALID_NOTIFICATIONS.each do |note_name|
    const_set("NOTE_#{note_name.to_s.upcase}", note_name.to_s)
  end

  # SETTINGS
  SETTINGS_OPTIONS = [
    {id: :off, label: 'Off'},
    {id: :realtime, label: 'Realtime'},
    {id: :daily, label: 'Daily'},
    {id: :weekly, label: 'Weekly'},
  ].freeze

  SETTINGS_MEDIUMS = [
    {id: :push, label: 'Mobile'},
    {id: :email, label: 'Email'}
  ].freeze

  SETTINGS_GROUPS = [
    {
      label: 'Activity related',
      notifications: [
        {
          label: 'When someone comments on a SmartCard I posted',
          id: Notify::NOTE_NEW_SMARTBITE_COMMENT,
          mapping: {realtime: :single, daily: :digest_daily, weekly: nil}
        },
        {
          label: 'When I am mentioned in a comment',
          id: Notify::NOTE_MENTION_IN_COMMENT,
          mapping: {realtime: :single, daily: :digest_daily, weekly: nil}
        },
        {
          label: 'When someone else comments after me',
          id: Notify::NOTE_NEW_ACTIVITY_COMMENTED_SMARTBITE,
          mapping: {realtime: :single, daily: :digest_daily, weekly: nil}
        },
        {
          label: 'Someone I am following starts a livestream',
          id: Notify::NOTE_NEW_LIVESTREAM_FROM_FOLLOWED_USER,
          mapping: {realtime: :single, daily: :nil, weekly: nil}
        },
        {
          label: 'When I have a new follower',
          id: Notify::NOTE_NEW_FOLLOWER,
          mapping: {realtime: :single, daily: :digest_daily, weekly: nil}
        },
        {
          label: 'When I am assigned something',
          id: Notify::NOTE_ASSIGNED_CONTENT,
          mapping: {realtime: :single, daily: :digest_daily, weekly: nil}
        },
        {
          label: 'When there is new content from a user that I am following',
          id: Notify::NOTE_NEW_CONTENT_BY_FOLLOWED_USER,
          mapping: {realtime: :single, daily: :digest_daily, weekly: nil}
        },
        {
          label: 'Time to start your schedule stream',
          id: Notify::NOTE_START_SCHEDULEDSTREAM,
          mapping: {realtime: :single, daily: :nil, weekly: nil}
        },
        {
          label: 'When someone completes an assignment assigned by me',
          id: Notify::NOTE_COMPLETED_ASSIGNMENT,
          mapping: {realtime: :single, daily: :digest_daily, weekly: nil}
        },
        {
          label: 'When a user is added as a curator to a channel',
          id: Notify::NOTE_ADDED_CURATOR,
          mapping: {realtime: :single, daily: :digest_daily, weekly: nil}
        },
        {
          label: 'When a user is added as a collaborator to a channel',
          id: Notify::NOTE_ADDED_COLLABORATOR,
          mapping: {realtime: :single, daily: :digest_daily, weekly: nil}
        },
        # {
        #   label: 'When a user is added as a follower to a channel',
        #   id: Notify::NOTE_ADDED_CHANNEL_FOLLOWER,
        #   mapping: {realtime: :single, daily: :digest_daily, weekly: nil}
        # },
        {
          label: 'To notify admin when there are new followers added to a channel.',
          id: Notify::NOTE_ADDED_CHANNEL_FOLLOWER_ADMIN,
          mapping: {realtime: :single, daily: :nil, weekly: nil}
        },
        {
          label: 'When a new livestream is added to a channel',
          id: Notify::NOTE_NEW_CONTENT_IN_CHANNEL,
          mapping: {realtime: :single, daily: :nil, weekly: nil}
        },
        {
          label: 'When a new livestream is added to a group',
          id: Notify::NOTE_LIVE_STREAM_IN_GROUP,
          mapping: {realtime: :single, daily: :nil, weekly: nil}
        },
        {
          label: 'When a card posted to a channel is curated',
          id: Notify::NOTE_CURATED_CARD,
          mapping: {realtime: :single, daily: :digest_daily, weekly: nil}
        },
        {
          label: 'When a card posted to a channel is skipped',
          id: Notify::NOTE_SKIPPED_CARD,
          mapping: {realtime: :single, daily: :digest_daily, weekly: nil}
        },
        {
          label: 'When a new card is added to a channel for curation',
          id: Notify::NOTE_NEW_CARD_FOR_CURATION,
          mapping: {realtime: :single, daily: :digest_daily, weekly: nil}
        },
        {
          label: 'To remind curator to curate cards',
          id: Notify::NOTE_REMINDED_CURATOR,
          mapping: {realtime: :single, daily: :nil, weekly: nil}

        },
        {
          label: 'To notify shared users when a card is shared with users, groups, channels',
          id: Notify::NOTE_CONTENT_SHARED,
          mapping: {realtime: :single, daily: :digest_daily, weekly: nil}
        }
      ]
    }
  ].freeze
end

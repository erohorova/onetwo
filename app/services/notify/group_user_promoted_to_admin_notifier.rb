module Notify
  class GroupUserPromotedToAdminNotifier < Notifier

    def setup
      self.notification_name = NOTE_GROUP_USER_PROMOTED_TO_ADMIN
      enable_additional_content
    end

    def users_to_notify(event_name, team, custom_hash = {})
      if custom_hash.present?
        yield [custom_hash[:user_id]]
      else
        yield []
      end
    end

    def get_organization(team)
      team.organization
    end

    def push_notification_params(notification_entry)
      team = notification_entry.sourceable
      {
        user_id: notification_entry.user_id,
        deep_link_id: team.id,
        deep_link_type: 'team',
        notification_type: self.notification_name,
        host_name: get_organization(team).home_page
      }
    end

    def notification_params(notification_entry)
      team = notification_entry.sourceable
      {
        user_id: notification_entry.user_id,
        team_name: team.name,
        actor_name: notification_entry.additional_content[:actor_name]
      }
    end

  end
end

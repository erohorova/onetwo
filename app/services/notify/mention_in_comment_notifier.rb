module Notify
  class MentionInCommentNotifier < Notifier
    def setup
      self.notification_name = NOTE_MENTION_IN_COMMENT
      set_enabled(:email, :single)
      set_enabled(:email, :digest_daily)
      set_default(:email, :single, false)

      set_enabled(:push, :single)
      set_default(:push, :single, false)
    end

    def users_to_notify(event_name, comment)
      # Only where there is a mention in a comment
    
      if comment.mentions.empty? || comment.commentable_type != 'Card'
        yield []
        return
      end
      # Return the user who is mentioned in the comment.
      yield comment.mentions.pluck(:user_id)
    end

    def get_organization(comment)
      comment.organization
    end

    def push_notification_params(notification_entry)
      comment = notification_entry.sourceable
      {
        user_id: notification_entry.user_id,
        deep_link_id: comment.commentable_id,
        deep_link_type: 'card',
        notification_type: self.notification_name,
        host_name: get_organization(comment).home_page
      }
    end

    def notification_params(notification_entry)
      comment = notification_entry.sourceable
      commenter = comment.user
      url = comment.commentable.card_deeplink_url
      {
        commenter_name: commenter.try(:name),
        url: url,
        message: comment.message
      }
    end
  end
end

module Notify
  class CompletedAssignmentNotifier < Notifier
    def setup
      self.notification_name = NOTE_COMPLETED_ASSIGNMENT

      set_enabled(:email, :single)
      set_enabled(:email, :digest_daily)
      set_default(:email, :digest_daily, false)

      set_enabled(:push, :single)
      set_enabled(:push, :digest_daily)
    end

    def get_organization(assignment)
      assignment.assignable.organization
    end

    def team_assignment(assignment)
      assignment.team_assignments.first
    end

    def users_to_notify(event_name, assignment)
      unless team_assignment(assignment)
        yield []
        return
      end

      # Return the assignment's assignor
      yield [team_assignment(assignment).assignor_id]
    end

    def push_notification_params(notification_entry)
      assignment = notification_entry.sourceable

      {
        user_id: notification_entry.user_id,
        deep_link_id: assignment.assignable_id,
        deep_link_type: 'card',
        notification_type: self.notification_name,
        host_name: get_organization(assignment).home_page
      }
    end

    def notification_params(notification_entry)
      assignment = notification_entry.sourceable
      
      {
        user_name: team_assignment(assignment).assignor.try(:name),
        assignment: {
          title: assignment.assignable.snippet_html,
          title_simplified: simplify_encoded_string(assignment.assignable.snippet_html).truncate(100, separator: ' '),
          assignment_url: assignment.assignable.assignment_url,
          image_url: assignment.assignable.fetch_image_attachment_url,
        },
        assignee: assignment.assignee.name,
        team_name: get_organization(assignment).try(:name)
      }
    end
  end
end

module Notify
  class AssignedContentNotifier < Notifier
    def setup
      self.notification_name = NOTE_ASSIGNED_CONTENT

      set_enabled(:email, :single)
      set_enabled(:email, :digest_daily)
      set_default(:email, :single, false)

      set_enabled(:push, :single)
      set_default(:push, :single, false)
    end

    def users_to_notify(event_name, assignment)
      yield [assignment.assignee.id]
    end

    def get_organization(assignment)
      assignment.assignable.organization
    end

    def push_notification_params(notification_entry)
      assignment = notification_entry.sourceable

      {
        user_id: notification_entry.user_id,
        deep_link_id: assignment.assignable_id,
        deep_link_type: 'card',
        notification_type: self.notification_name,
        host_name: get_organization(assignment).home_page
      }
    end

    def notification_params(notification_entry)
      return if notification_entry.blank? || notification_entry.sourceable.blank?
      assignment = notification_entry.sourceable
      assignable = assignment.assignable
      team_assignment = assignment.team_assignments.first
       {
        user_name: assignment.assignee.name,
        assignment: {
          title: assignable.snippet_html.truncate(100, separator: ' '),
          title_simplified: simplify_encoded_string(assignable.snippet_html).truncate(100, separator: ' '),
          assignment_url: assignment.deeplinked_assignment_url,
          assignment_message: team_assignment.message,
          image_url: assignable.fetch_image_attachment_url,
        },
        assignor: team_assignment.assignor.try(:name),
        team_name: get_organization(assignment).try(:name)
       }
    end
  end
end

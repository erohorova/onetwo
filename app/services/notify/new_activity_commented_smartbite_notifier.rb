module Notify
  class NewActivityCommentedSmartbiteNotifier < Notifier
    def setup
      self.notification_name = NOTE_NEW_ACTIVITY_COMMENTED_SMARTBITE

      set_enabled(:email, :single)
      set_enabled(:email, :digest_daily)
      set_default(:email, :single, false)
      
      set_enabled(:push, :single)
      set_default(:push, :single, false)
    end

    def users_to_notify(event_name, activity)
      # Only support comments on cards
      activity_source = activity_source(activity)
        
      if activity_source.class.to_s != 'Card'
       yield []
       return
      end

      ignores = [activity_source.author_id, activity.user_id]
 
      yield(activity_source.comments.pluck(:user_id) + activity_source.up_votes.pluck(:user_id) - ignores).uniq
    end

    def get_organization(activity)
      activity_source(activity).organization
    end

    def activity_source(activity)
      activity.try(:votable) || activity.try(:commentable)
    end

    def push_notification_params(notification_entry)
      activity = notification_entry.sourceable
      {
        user_id: notification_entry.user_id,
        deep_link_id: activity_source(activity).id,
        deep_link_type: 'card',
        notification_type: self.notification_name,
        host_name: get_organization(activity).home_page
      }
    end

    def notification_params(notification_entry)
      activity = notification_entry.sourceable
      activity_source = activity_source(activity)
      type = (activity.is_a?(Vote) ? 'Vote' : 'Comment')
      user = (type == 'Vote' ? activity.voter : activity.user)

       {  
        activity: {
          type: type,
          user_name: user.try(:name),
          content: activity.try(:message),
          url: activity_source.card_deeplink_url,
          title: activity_source.snippet_html,
          title_simplified: simplify_encoded_string(activity_source.snippet_html).truncate(100, separator: ' ')
        }
       }
    end

  end
end

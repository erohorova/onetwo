class IpLookupService
  attr_reader :ip_address

  def initialize(ip_address)
    @ip_address = ip_address
  end

  def ip_lookup_response
    begin
      Rails.cache.fetch("time-zone-lookup-for-#{@ip_address}", expires_in: 1.day) do
        response = Faraday.get("#{Settings.free_geo_ip}#{@ip_address}")

        if response.present? && response.success?
          JSON.parse(response.body)
        end
      end
    rescue => e
    end
  end

  def get_time_zone
    ip_lookup_response.try(:[], "time_zone")
  end
end

# convert topic i.e prepend prefix or replace root and
# create hash of updated topic

class TopicUpdateService

  def initialize(type)
    @type = type
  end

  def convert_topics(taxonomy_topics, learning_topic, topic_prefix, taxo_root_flag, old_root)
    # Add topic prefix to topic and domain.
    # Replace old root with new org level topic prefix
    # topic_prefix is org prefix i.e "labs","edcast"
    # taxo_root_flag is 'replace' to replace root node and 'prepend' to add prefix
    # old_root is old taxonomy root i.e "edcast"

    # Before update topic is : [{"topic_name"=>"corporate_learning_development.social_learning",
    # "topic_id"=>"743d2048-3f7a-4150-bb57-01731fda3574", "domain_name"=>"", "domain_id"=>"Social learning"}]

    # After update topic for org labs: [{"topic_name"=>"labs.corporate_learning_development.social_learning",
    # "topic_id"=>"4561360798311050569", "domain_name"=>"", "domain_id"=>"Social learning"}]

    # Get topic name and domain name from learning/expert topics or topic_name from cards or channels
    topic_name, domain_name = get_topic_domain(learning_topic)
    if topic_name.present?
      learning_topic_prefix = topic_name.split(".")[0]

      # Get root node from learning topic and check with topic prefix, if not same update with prefix
      if learning_topic_prefix != topic_prefix

        # If taxo_root_flay is 'replace' update root node with topic prefix i.e old_root 'edcast' with new prefix 'labs'
        if taxo_root_flag == 'replace'
          if learning_topic_prefix == old_root
            topic_name = topic_name.gsub!("#{old_root}", topic_prefix)
            if domain_name.present?
              domain_name = domain_name.gsub!("#{old_root}", topic_prefix)
            end
          end
        else
          # if taxo_root_flay is 'prepend' prepend topic prefix i.e labs.edcast.corporate_learning_development.social_learning
          topic_name = topic_prefix + "."+topic_name
          if domain_name.present?
            domain_name = topic_prefix + "."+domain_name
          end
        end
        learning_topic = create_topic_hash(taxonomy_topics, learning_topic, topic_name, domain_name)
      end
      learning_topic
    end
  end

  def create_topic_hash(taxonomy_topics, learning_topic, topic_name, domain_name)

    # Check if topic name exists in taxonomy topic response then only update learning/expert topics
    if taxonomy_topics.key?(topic_name)
      if @type == 'card'
        learning_topic = topic_name
        return learning_topic
      end
      if @type == 'channel'
        learning_topic["name"] = topic_name
        learning_topic["id"] = taxonomy_topics["#{topic_name}"][:topic_id]
        return learning_topic
      end
      if @type == 'user_profile'
        learning_topic["topic_name"] = topic_name
        learning_topic["domain_name"] = domain_name
        learning_topic["domain_id"] = taxonomy_topics["#{topic_name}"][:domain_id]
        learning_topic["topic_id"] = taxonomy_topics["#{topic_name}"][:topic_id]
        return learning_topic
      end
    end
  end

  def get_topic_domain(learning_topic)
    if @type == 'card'
      return learning_topic
    end
    if @type == 'channel'
      topic_name = learning_topic["name"]
      return topic_name
    end
    if @type == 'user_profile'
      topic_name = learning_topic["topic_name"]
      domain_name = learning_topic["domain_name"]
      return topic_name, domain_name
    end
  end

end
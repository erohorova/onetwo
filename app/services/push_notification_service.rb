class PushNotificationService
  attr_accessor :org, :payload

  include EventTracker

  def initialize(org, payload)
    payload[:host_name] = org.host
    payload[:notification_type] ||= "AdminGenerated"

    @org = org
    @payload = payload
  end

  def fetch_channel_name_for_payload
    if payload[:user_id]
      user = User.where(id: payload[:user_id], organization_id: org.id).first
      user.try(:private_pubnub_channel_name)
    elsif payload[:channel_id]
      channel = Channel.where(id: payload[:channel_id], organization_id: org.id).first
      channel.try(:public_pubnub_channel_name)
    else
      org.try(:public_pubnub_channel_name)
    end
  end

  # check if record exists for deep link data
  def record_exists?
    item_type = payload[:deep_link_type]
    item_id   = payload[:deep_link_id]

    # check for card exposed types if required
    item_type = 'group' if item_type.in?(['course'])
    item_type = 'card' if item_type.in?(Card::EXPOSED_TYPES_V2)

    begin
      record_class = item_type.classify.constantize
      record_class.where(id: item_id, organization_id: org.id).exists?
    rescue => e
      log.info("Invalid deep_link_type in payload: #{payload}")
      false
    end
  end

  def payload_valid?
    if payload_has_required_keys? && record_exists?
      payload[:channel_names] =  []
      payload[:channel_names] << fetch_channel_name_for_payload
      payload[:channel_names].compact.present?
    end
  end

  def notify
    #This will send notification via pubnub to (region outside china for ios/android, in china for ios)
    PubnubNotifier.notify_v2(payload)
    #This will send notification via pushy to (region china for android)
    PushyNotifier.notify(payload)
  end

  def record_custom_notification_event
    record_custom_event(
      event: "org_custom_notification_sent",
      job_args: [{
        notification: {
          'json_payload' => payload.except(:content).to_json,
          'message'      => payload[:content]
        },
        org: org
      }]
    )
  end

  def payload_has_required_keys?
    [:deep_link_type, :deep_link_id, :content].all? {|key| payload.key? key}
  end
end

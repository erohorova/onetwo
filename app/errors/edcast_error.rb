class EdcastError < StandardError
  class << self
    attr_accessor :user_error_message
  end
end

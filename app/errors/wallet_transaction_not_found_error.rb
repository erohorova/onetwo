class WalletTransactionNotFoundError < EdcastError
  self.user_error_message = 'Wallet transaction record not found'

  def http_status
    404
  end

  def code
    'wallet_transaction_not_found'
  end

  def message
    "Wallet transaction record not found."
  end

  def to_hash
    {
      message: message,
      code: code
    }
  end
end
class InsufficientWalletBalanceError < EdcastError
  self.user_error_message = 'Insufficient balance in wallet'

  def http_status
    422
  end

  def code
    'insufficient_balance'
  end

  def message
    "Insufficient balance in wallet"
  end

  def to_hash
    {
      message: message,
      code: code
    }
  end
end

class DuplicateOrderError < EdcastError
  self.user_error_message = 'Payment has already been initiated/confirmed.'

  def http_status
    409
  end

  def code
    'duplicate_order'
  end

  def message
    "Payment has already been initiated/confirmed."
  end

  def to_hash
    {
      message: message,
      code: code
    }
  end
end

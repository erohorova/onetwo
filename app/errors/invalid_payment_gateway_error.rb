class InvalidPaymentGatewayError < EdcastError
  self.user_error_message = 'Invalid payment gateway specified.'

  def http_status
    400
  end

  def code
    'invalid_gateway'
  end

  def message
    "Invalid payment gateway specified."
  end

  def to_hash
    {
      message: message,
      code: code
    }
  end
end

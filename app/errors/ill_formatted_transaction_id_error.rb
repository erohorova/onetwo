class IllFormattedTransactionIdError < EdcastError
  self.user_error_message = 'Ill-formatted payment id given.'

  def http_status
    406
  end

  def code
    'duplicate_order'
  end

  def message
    "Payment has already been initiated/confirmed."
  end

  def to_hash
    {
      message: message,
      code: code
    }
  end
end

class PaymentAlreadyProcessedError < EdcastError
  self.user_error_message = 'This payment has already been processed.'

  def http_status
    422
  end

  def code
    'payment_already_processed'
  end

  def message
    "This payment has already been processed."
  end

  def to_hash
    {
      message: message,
      code: code
    }
  end
end

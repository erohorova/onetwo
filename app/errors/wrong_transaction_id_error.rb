class WrongTransactionIdError < EdcastError
  self.user_error_message = 'Ill-formatted payment id given.'

  def http_status
    406
  end

  def code
    'wrong_transaction_id'
  end

  def message
    "Ill-formatted payment id given."
  end

  def to_hash
    {
      message: message,
      code: code
    }
  end
end

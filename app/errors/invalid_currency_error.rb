class InvalidCurrencyError < EdcastError
  self.user_error_message = 'Invalid currency specified.'

  def http_status
    400
  end

  def code
    'invalid_currency'
  end

  def message
    "Invalid currency specified."
  end

  def to_hash
    {
      message: message,
      code: code
    }
  end
end

class TransactionAlreadyCancelledError < EdcastError
  self.user_error_message = 'This transaction has already been cancelled'

  def http_status
    422
  end

  def code
    'transaction_already_canceled'
  end

  def message
    "This transaction has already been cancelled."
  end

  def to_hash
    {
      message: message,
      code: code
    }
  end
end

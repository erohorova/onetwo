require 'jwt_auth'

class LxpOauthSetup
  # OmniAuth expects the class passed to setup to respond to the #call method.
  # env - Rack environment
  def self.call(env)
    new(env).setup
  end

  # Assign variables and create a request object for use later.
  # env - Rack environment
  def initialize(env)
    @env = env
    @request = ActionDispatch::Request.new(env)
  end

  # The main purpose of this method is to set the consumer key and secret.
  def setup
    unless @env['rack.session']
      error = OmniAuth::NoSessionError.new('You must provide a session to use OmniAuth.')
      fail(error)
    end

    current_org = ::Organization.find_by_request_host(@request.host)
    if current_org
      set_options_for_organization(current_org)
    else
      fail("Cannot look up organization by host name")
    end
  end

  # Invokes when:
  #   1. Establishing a connection with Okta for the first time.
  #   2. Establishing a connection with EdCast when user is confirmed on Okta.
  # Use-case:
  #   Since we now shall have a single consumer with multiple connections, we need to identify what credentials it should pick up.
  #   We wrap current OrgSso#id by JWT token and pass along the requests and callbacks.
  def set_options_for_organization(organization)
    lxp_oauth_sso = ::Sso.find_by_name('lxp_oauth')

    # `@request.params['connector']` is received when initial request starts from EdCast and store it to session.
    # The reason we are storing this value in session as custom parameter are lost across the handshake.
    # `@request.session['connector_token']` is used when user is successfully authenticated from Okta.
    # Whatever token retrieved from `@request.params['connector']` and `@request.session['connector_token']` is then decoded and OrgSso#id is obtained.
    connector_token = @request.params['connector'] || @request.session['connector_token']
    native_scheme   = @request.params['native_scheme'] || @request.session['native_scheme']
    @request.session['connector_token'] = connector_token if @request.params['connector']
    @request.session['native_scheme']   = native_scheme if @request.params['native_scheme']
    org_sso_id = JwtAuth.decode(connector_token)

    if lxp_oauth_sso
      lxp_oauth = organization.org_ssos.where(sso_id: lxp_oauth_sso.id, id: org_sso_id).first
      if lxp_oauth
        @env['omniauth.strategy'].options[:client_options][:site] = lxp_oauth.oauth_login_url
        @env['omniauth.strategy'].options[:client_options][:authorize_url] = lxp_oauth.oauth_authorize_url
        @env['omniauth.strategy'].options[:client_options][:token_url] = lxp_oauth.oauth_token_url
        @env['omniauth.strategy'].options[:client_id] = lxp_oauth.oauth_client_id
        @env['omniauth.strategy'].options[:client_secret] = lxp_oauth.oauth_client_secret
        @env['omniauth.strategy'].options[:user_info_url] = lxp_oauth.oauth_user_info_url
        @env['omniauth.strategy'].options[:redirect_uri] = lxp_oauth.oauth_redirect_uri
        @env['omniauth.strategy'].options[:response_type] = 'id_token'
        @env['omniauth.strategy'].options[:state] = connector_token
        @env['omniauth.strategy'].options[:authorize_params] = { response_mode: 'query', nonce: 'YsG76jo' }

        if lxp_oauth.idp.present?
          @env['omniauth.strategy'].options[:authorize_params][:idp] = lxp_oauth.idp
        end
        
        if lxp_oauth.oauth_scopes.present?
          @env['omniauth.strategy'].options[:scope] = lxp_oauth.oauth_scopes
        end
      else
        fail("OAuth configuration not found for organization_id=#{organization.id}")
      end
    else
      fail("OAuth Sso does not exist in the database")
    end
  end
end

require 'jwt_auth'

class InternalContentSetup
  # OmniAuth expects the class passed to setup to respond to the #call method.
  # env - Rack environment
  def self.call(env)
    new(env).setup
  end

  # Assign variables and create a request object for use later.
  # env - Rack environment
  def initialize(env)
    @env = env
    @request = ActionDispatch::Request.new(env)
  end

  # The main purpose of this method is to set the consumer key and secret.
  def setup
    unless @env['rack.session']
      error = OmniAuth::NoSessionError.new('You must provide a session to use OmniAuth.')
      fail(error)
    end

    current_org = Organization.find_by_request_host(@request.host)
    if current_org
      set_options_for_organization(current_org)
    else
      fail("Cannot look up organization by host name")
    end
  end

  # Invokes when:
  #   1. Establishing a connection with Okta for the first time.
  #   2. Establishing a connection with EdCast when user is confirmed on Okta.
  # Use-case:
  #   Since we now shall have a single consumer with multiple connections, we need to identify what credentials it should pick up.
  #   We wrap current OrgSso#id by JWT token and pass along the requests and callbacks.
  def set_options_for_organization(organization)
    internal_content_sso = Sso.find_by_name('internal_content')

    # `@request.params['connector']` is received when initial request starts from EdCast and store it to session.
    # The reason we are storing this value in session as custom parameter are lost across the handshake.
    # `@request.session['connector_token']` is used when user is successfully authenticated from Okta.
    # Whatever token retrieved from `@request.params['connector']` and `@request.session['connector_token']` is then decoded and OrgSso#id is obtained.
    connector_token = @request.params['connector'] || @request.session['connector_token']
    native_scheme   = @request.params['native_scheme'] || @request.session['native_scheme']
    @request.session['connector_token'] = connector_token if @request.params['connector']
    @request.session['native_scheme']   = native_scheme if @request.params['native_scheme']
    org_sso_id = JwtAuth.decode(connector_token)

    if internal_content_sso
      internal_content = organization.org_ssos.find_by(sso_id: internal_content_sso.id, id: org_sso_id)
      if internal_content
        @env['omniauth.strategy'].options[:client_options][:site] = internal_content.oauth_login_url
        @env['omniauth.strategy'].options[:client_options][:authorize_url] = internal_content.oauth_authorize_url
        @env['omniauth.strategy'].options[:client_options][:token_url] = internal_content.oauth_token_url
        @env['omniauth.strategy'].options[:client_id] = internal_content.oauth_client_id
        @env['omniauth.strategy'].options[:client_secret] = internal_content.oauth_client_secret
        @env['omniauth.strategy'].options[:user_info_url] = internal_content.oauth_user_info_url
        @env['omniauth.strategy'].options[:redirect_uri] = internal_content.oauth_redirect_uri
        @env['omniauth.strategy'].options[:state] = connector_token

        if internal_content.idp.present?
          @env['omniauth.strategy'].options[:authorize_params][:idp] = internal_content.idp
        end
        
        if internal_content.oauth_scopes.present?
          @env['omniauth.strategy'].options[:scope] = internal_content.oauth_scopes
        end
      else
        fail("OAuth configuration not found for organization_id=#{organization.id}")
      end
    else
      fail("OAuth Sso does not exist in the database")
    end
  end
end
# lib/omniauth_setup.rb
class OrgOmniauthSetup
  # OmniAuth expects the class passed to setup to respond to the #call method.
  # env - Rack environment
  def self.call(env)
    new(env).setup
  end

  # Assign variables and create a request object for use later.
  # env - Rack environment
  def initialize(env)
    @env = env
    @request = ActionDispatch::Request.new(env)
  end

  # private

  # The main purpose of this method is to set the consumer key and secret.
  def setup
    unless @env['rack.session']
      error = OmniAuth::NoSessionError.new('You must provide a session to use OmniAuth.')
      fail(error)
    end

    current_org = ::Organization.find_by_request_host(@request.host)
    if current_org
      set_options_for_organization(current_org)
    else
      fail("Cannot look up organization by host name")
    end
  end

  # Use the subdomain in the request to find the account with credentials
  def set_options_for_organization(organization)
    org_oauth_sso = ::Sso.find_by_name('org_oauth')
    if org_oauth_sso
      org_oauth = organization.org_ssos.where(sso_id: org_oauth_sso.id).first
      if org_oauth
        @env['omniauth.strategy'].options[:client_options][:site] = org_oauth.oauth_login_url
        @env['omniauth.strategy'].options[:client_options][:authorize_url] = org_oauth.oauth_authorize_url
        @env['omniauth.strategy'].options[:client_options][:token_url] = org_oauth.oauth_token_url
        @env['omniauth.strategy'].options[:client_id] = org_oauth.oauth_client_id
        @env['omniauth.strategy'].options[:client_secret] = org_oauth.oauth_client_secret
        @env['omniauth.strategy'].options[:user_info_url] = org_oauth.oauth_user_info_url

        if org_oauth.oauth_scopes.present?
          @env['omniauth.strategy'].options[:scope] = org_oauth.oauth_scopes
        end
      else
        fail("Org Oauth configuration not found for organization_id=#{organization.id}")
      end
    else
      fail("Org Oauth Sso does not exist in the database")
    end
  end
end
# modified from http://robots.thoughtbot.com/catching-json-parse-errors-with-custom-middleware
class CatchJsonParseErrors
  def initialize(app)
    @app = app
  end

  def call(env)
    begin
      @app.call(env)    
    rescue ActionDispatch::ParamsParser::ParseError => error
      if env['HTTP_ACCEPT'] =~ /application\/json/
        log.warn("Invalid JSON submitted to app: #{error}")
        return [
            400,
            { "Content-Type" => "application/json" },
            [ { status: 400, error: "Error in submitted JSON: #{error}" }.to_json ]
        ]
      else
        raise error
      end
    end
  end
end

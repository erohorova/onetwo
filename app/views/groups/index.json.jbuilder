json.array! @groups do |group|
  json.name group.name
  json.description group.description
  json.created_at group.created_at
  json.website_url group.website_url
  json.user_count group.users.count
  json.post_count group.posts.count
  json.event_count group.events.count
  json.id group.id
  json.details_locked group.details_locked
  json.client_resource_id group.client_resource_id
  json.closed group.closed?
end

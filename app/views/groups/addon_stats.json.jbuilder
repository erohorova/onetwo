json.cache! [@group], expires_in: 10.minutes do
  json.labels do
    json.array! ['Date', 'Active forum users', 'Posts count', 'Comments count', 'Users posting', 'Users commenting']
  end
  json.data do
    json.(@stats) do |stat|
      json.array! [
                      stat.date,
                      stat.total_active_forum_users + stat.total_active_mobile_forum_users,
                      stat.total_posts,
                      stat.total_comments,
                      stat.total_users_with_posts,
                      stat.total_users_with_comments
                  ]
    end
  end
end

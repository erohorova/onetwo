json.array! @groups do |group|
  json.id group.id
  json.name group.name
  json.term group.course_term
end

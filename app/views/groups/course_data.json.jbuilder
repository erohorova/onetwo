json.id @group.id
json.start_date @group.start_date.try(:strftime, "%Y-%m-%d") || @start_date
json.course_title @group.name
json.course_description @group.description
json.course_image @group.image_url
json.course_website_url @savannah_data['website_url']
json.course_intro_link @savannah_data['intro_link']
json.course_organization @savannah_data['organization']
json.term @group.course_term
json.instructors do
  json.array! @savannah_data['instructors'] do |instructor|
    json.name instructor['name']
    json.image instructor['image']
    json.picture instructor['image']
  end
end
json.lectures do
  json.array! @lectures do |lecture|
    json.lecture_title lecture['chapter_title']
    json.videos do
      json.array! lecture['videos'] do |video|
        if @api_version == 0
          json.youtube_id video['youtube_id']
          json.youtube_title video['youtube_title']
        elsif @api_version == 1
          json.id video['video_id']
          json.title video['video_title']
          json.source video['video_source']
          json.thumbnail video['video_thumbnail']
        end
      end
    end
    json.statics do
      json.array! lecture['statics'] do |static|
        json.static_title static['static_title']
        json.static_url static['static_url']
      end
    end
  end
end
json.dogwood_release @is_dogwood_release

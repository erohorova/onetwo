json.weekly do
  json.num_threads @stats[:weekly][:num_threads]
  json.num_replies @stats[:weekly][:num_replies]
  json.num_threads_without_replies @stats[:weekly][:num_threads_without_replies]
  json.num_posts_unread @stats[:alltime][:num_posts_unread]
  json.trending_tags @stats[:weekly][:trending_tags]
  json.trending_contexts do
    json.array! @stats[:weekly][:trending_contexts] do |tc|
      json.context tc['label']
      json.count tc['count']
    end
  end
end

json.alltime do
  json.num_threads @stats[:alltime][:num_threads]
  json.num_replies @stats[:alltime][:num_replies]
  json.num_threads_without_replies @stats[:alltime][:num_threads_without_replies]
  json.num_posts_unread @stats[:alltime][:num_posts_unread]
end

if @stats[:daily_stats].nil?
  json.daily_stats nil
else
  json.daily_stats do
    json.array! @stats[:daily_stats] do |daily_stat|
      json.day daily_stat[:day]
      json.num_threads daily_stat[:num_threads]
      json.num_threads_with_replies daily_stat[:num_threads_with_replies]
    end
  end
end

if @stats[:daily_user_stats].nil?
  json.daily_user_stats nil
else
  json.daily_user_stats do
    json.array! @stats[:daily_user_stats] do |daily_user_stat|
      json.day daily_user_stat[:day]
      json.active_users daily_user_stat[:active_users]
      json.posted_users daily_user_stat[:posted_users]
      json.answered_users daily_user_stat[:answered_users]
    end
  end
end

json.score @stats[:score]

json.weekly do
  json.num_threads @stats[:weekly][:num_threads]
  json.trending_tags @stats[:weekly][:trending_tags]
  json.trending_contexts do
    json.array! @stats[:weekly][:trending_contexts] do |tc|
      json.context tc['label']
      json.count tc['count']
    end
  end
  json.num_replies @stats[:weekly][:num_replies]
end

json.alltime do
  json.num_threads @stats[:alltime][:num_threads]
  json.num_posts_unread @stats[:alltime][:num_posts_unread]
  json.num_replies @stats[:alltime][:num_replies]
end

json.score @stats[:score]
json.stars @stats[:stars]

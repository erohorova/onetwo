json.array! @trending_labels do |tl|
  json.type tl['type']
  json.id tl['id']
  json.label tl['label']
  json.count tl['count']
end

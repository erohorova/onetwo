json.posts_count @stats[:num_posts]
json.comments_count @stats[:num_comments]
json.user_posts_count @stats[:num_user_posts]
json.user_comments_count @stats[:num_user_comments]

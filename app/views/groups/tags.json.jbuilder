json.array! @tags do |tag|
  json.tag_id tag['tag_id']
  json.tag_name tag['tag_name']
  json.tag_count tag['tag_count']
end

if @group.is_active_super_user?(@user)
  json.partial! 'groups/instructor_stats', stats: @stats
else
  json.partial! 'groups/student_stats', stats: @stats
end

json.array! @channels do |channel|
  json.id channel.id
  json.label channel.label
  json.description channel.description
  json.image_url channel.mobile_photo
  json.url "channel/#{channel.slug}"
  json.followers_count channel.followers.length
end

json.cards do
  json.array! @cards do |card|
    json.slug card['slug']
    json.template_type 'simple'
    content_map = card['content']
    json.title content_map['title'] || ""
    json.description content_map['message']
    json.image_url content_map['link_image_url'] || content_map['video_image_url'] || content_map['image_url'] || image_url('card_default_image_min.jpg')
  end
end

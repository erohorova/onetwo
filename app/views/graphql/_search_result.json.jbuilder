json.cards do
  json.array! @cards do |card|
    json.partial! 'graphql/details', card: card
  end
end
json.users do
  json.array! @users do |user|
    json.partial! 'api/v2/users/capsule', user: user
    json.is_following @following.try(:[], 'User').try(:[], user.id) || false
    json.partial! 'users/role', user: user
    json.group_role 'group_leader' if user.is_group_leader?
    json.expert_skills user.expert_topics
  end
end
json.channels do
  json.array! @channels do |channel|
    json.partial! 'api/v2/channels/details', channel: channel
  end
end

json.offset @offset
json.limit @limit
json.cards_offset @cards_offset
json.cards_limit @cards_limit
json.channels_offset @channels_offset
json.channels_limit @channels_limit
json.users_offset @users_offset
json.users_limit @users_limit
json.query @query
json.aggregations @aggs

associated_group = card.resource_group

@fields['id'] && json.id(card.id.nil? ? Card.card_ecl_id(card.ecl_id) : card.id.to_s)

@fields['card_type'] && json.card_type(card.card_type)
@fields['card_subtype'] && json.card_type(card.card_subtype)
@fields['slug'] && json.card_type(card.slug)
@fields['state'] && json.card_type(card.state)
@fields['is_official'] && json.card_type(card.is_official)
@fields['created_at'] && json.card_type(card.created_at)
@fields['updated_at'] && json.card_type(card.updated_at)
@fields['published_at'] && json.card_type(card.published_at)

if @fields['wallet']
  card.wallet if card.wallet.present?
end

if @fields['title']
  json.title (card.message != card.title) ? card.title.try(:clean) : ""
end

@fields['message'] && card.sanitize_message
@fields['views_count'] && json.views_count(card.all_time_views_count)
@fields['share_url'] && json.share_url(
  ClientOnlyRoutes.show_insight_url_with_org(
    current_org,
    id: (card.id.nil? ? Card.card_ecl_id(card.ecl_id) : card.id.to_s)
  )
)

if @fields['ecl_metadata']
  if card.author_id.nil? && card.ecl_metadata.present?
    json.ecl_source_display_name card.ecl_metadata["source_display_name"]
    json.ecl_source_type_name card.ecl_metadata["source_type_name"]
    json.ecl_duration_metadata card.ecl_metadata["duration_metadata"]
  end
end

@fields['comments_count'] && json.comments_count(card.comments.count)
@fields['votes_count'] && json.votes_count(card.votes.count)
@fields['voters'] && json.voters(card.voters_names)
@fields['channel_ids'] && json.channel_ids(card.curated_channel_ids)

@fields['average_rating'] && json.average_rating(card.cards_ratings.average(:rating))
@fields['user_rating'] && json.user_rating(card.cards_ratings.where(user_id: current_user.id).first.try(:rating))
@fields['all_ratings'] && json.all_ratings(card.cards_ratings.group("rating").count)

@fields['is_upvoted'] && json.is_upvoted(current_user && card.voted_by?(current_user))
@fields['is_bookmarked'] && json.is_bookmarked(current_user && card.bookmarked_by?(current_user))
@fields['is_assigned'] && json.is_assigned(current_user.has_an_assignment?(card))
@fields['completion_state'] && json.completion_state(current_user && current_user.completion_state_for('Card', card['id']).try(&:upcase))
@fields['filestack'] && json.filestack(card.filestack)

if @fields['resource']
  json.resource do
    json.partial! 'api/v2/resources/details', resource: card.resource unless card.resource.blank?
  end
end

if @fields['mentions']
  json.mentions do
    json.array! card.mentions do |mention|
      json.partial! 'api/v2/users/capsule', user: mention.user unless mention.user.blank?
    end
  end
end

if @fields['author']
  json.author do
    json.partial! 'api/v2/users/capsule', user: card.author unless card.author.blank?
  end
end

if @fields['tags']
  json.tags do
    json.array! card.tags do |tag|
      json.partial! 'api/v2/tags/details', tag: tag
    end
  end
end

if @fields['file_resources']
  json.file_resources do
    json.array! card.file_resources do |file_resource|
      json.partial! 'api/v2/file_resources/details', file_resource: file_resource  unless file_resource.blank?
    end
  end
end

if @fields['channels']
  json.channels do
    json.array! card.curated_channels do |channel|
      json.id channel.id
      json.label channel.label
    end
  end
end

# pack
if @fields['pack_cards']
  if card.card_type == "pack"
    json.pack_cards card.pack_cards_index_data
  end
end

# poll
if @fields['poll']
  if card.card_type == "poll"
    json.quiz_question_options do
      json.array! card.quiz_question_options do |qqo|
        json.partial! 'api/v2/quiz_question_options/details', quiz_question_option: qqo
        json.count card.quiz_question_stats.try(:options_stats).try(:[], qqo['id'].to_s) || 0
        json.is_correct qqo.is_correct
      end
    end
    json.is_assessment card.is_a_poll_question?
    json.attempt_count card.quiz_question_stats.try(:attempt_count)
    json.has_attempted current_user && QuizQuestionAttempt.has_user_attempted_quiz_question_card?(card.id, current_user.id)
    json.attempted_option QuizQuestionAttempt.attempted_option(card_id: card.id, user: current_user)
  end
end

# video_stream
if @fields['video_stream']
  json.video_stream do
    json.partial! 'api/v2/video_streams/details', video_stream: card.video_stream, user: current_user if card.video_stream.present?
  end
end

if @fields['resource_group']
  json.resource_group do
    json.partial! 'api/v2/groups/details', group: associated_group, user: current_user if associated_group
  end
end

if @fields['assignment']
  if defined?(assignment)
    json.assignment do
      if assignment
        #include assignable details should be false. otherwise it might end up rendering card partial recursively
        json.partial! 'api/v2/assignments/details', assignment: assignment, include_assignable_details: false
      else
        nil
      end
    end
  end
end

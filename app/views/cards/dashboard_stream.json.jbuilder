json.array! @posts do |post|
  json.partial! 'posts/stream_details', post: post
  json.upvoted @votes_by_user.include?(post.id.to_i)
end

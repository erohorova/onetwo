json.array! @clients do |client|
  json.id client.id
  json.client_name client.name
  json.created_at client.created_at.strftime('%d/%m/%Y')
  json.social_enabled client.social_enabled
  json.owner_name client.owner.name
  json.redirect_uri client.redirect_uri
  json.credentials do
    json.array! client.credentials do |credential|
      json.api_key credential.api_key
      json.shared_secret credential.shared_secret
      json.credential_state credential.state
    end
  end
end
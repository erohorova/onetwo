json.id @client.id
json.client_name @client.name
json.created_at @client.created_at.strftime('%d/%m/%Y')
json.owner_name @client.owner.name
json.social_enabled @client.social_enabled
json.api_key @credential.api_key
json.shared_secret @credential.shared_secret
json.credential_state @credential.state

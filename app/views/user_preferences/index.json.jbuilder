json.array! @user_preferences do |preference|
  json.key preference.key
  json.value preference.value
end
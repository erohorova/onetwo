json.partial! 'api/teams/details', team: @team
if @assignable
  json.has_assignment @assigned_team_ids.include?(@team.id)
  json.unassigned_members_count @members_count[@team.id]
end
json.is_team_admin @team.is_admin?(current_user)

json.query @query
json.members do
  json.total @total
  json.users do
    json.array! @users do |user|
      json.extract! user, *%i[id name email handle photo role pending is_followed is_current_user]
    end
  end
end

json.pending do
  json.total @invitations_total
  json.users do
    json.array! @invitations do |invitation|
      json.id invitation.id
      json.name nil
      json.email invitation.recipient_email
      json.handle nil
      json.photo nil
      json.role invitation.roles
      json.pending true
    end
  end
end

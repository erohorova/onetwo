json.total @response.total
json.cards @cards do |card|
  json.partial! 'cards/details', card: card, user: current_user, user_action: nil
  json.is_assigned card['is_assigned']
end

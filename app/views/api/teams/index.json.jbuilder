json.teams @teams do |team|
  json.partial! 'api/teams/details', team: team
  json.role team.as_type

  if @assignable
    json.has_assignment @assigned_team_ids.include?(team.id)
    json.unassigned_members_count @members_count[team.id]
  end
end

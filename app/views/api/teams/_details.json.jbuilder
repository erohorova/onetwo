json.id team.id
json.name team.name
json.description team.description
json.slug team.slug
json.organization_id team.organization_id
json.created_at team.created_at
json.created_by team.admins.first.try(:name)
json.image_urls do
  json.small team.image.url(:small)
  json.medium team.image.url(:medium)
  json.large team.image.url(:large)
end
json.members_count team.active_users.count

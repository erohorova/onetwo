json.array! @configs do |setting|
  json.id setting.id
  json.name setting.name
  json.value setting.parsed_value
  json.description setting.description
end
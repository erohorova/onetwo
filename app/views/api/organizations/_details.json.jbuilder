is_native_app = is_native_app?

sso_names = []
json.id organization.id
json.name organization.name
json.description organization.description
json.is_open organization.is_open
json.is_registrable organization.is_registrable
json.image_url organization.mobile_photo || organization.photo
json.banner_url organization.photo
json.splash_image is_native_app ? Filestack::Security.new(call: ["convert"], org: organization).signed_url(organization.splash_image_url) : secured_url(organization.splash_image_url, current_user.id, organization)
json.co_branding_logo organization.co_branding_logo.present? ? organization.co_branding_logo.url : nil
json.host_name organization.host_name
# Need to support vanity url home page
json.home_page organization.edcast_home_page
json.show_sub_brand_on_login organization.show_sub_brand_on_login
json.show_onboarding organization.show_onboarding
json.favicons organization.favicons
json.custom_domain organization.custom_domain
json.ssos do
  json.array! organization.org_ssos.includes(:sso).active do |org_sso|
    sso_names << org_sso.sso.name
    json.name org_sso.customised_sso_name
    json.label_name org_sso.customised_label_name
    json.icon_url image_path("sso/#{org_sso.sso.name}.png")
    json.authentication_url "#{request.protocol}#{organization.host}/auth/#{org_sso.sso.name}"
    json.web_authentication_url sso_url(org_sso)
  end
end
json.cobrands do
  organization.fetch_cobrand_configs_with_defaults.each do |k, v|
    json.set! k, v
  end
end
json.configs do
  json.array! organization.configs do |config|
    json.id config.id
    json.name config.name
    json.value config.parsed_value
  end
end

json.quarters do
  json.array! organization.quarter_config.each do |quarter|
    json.id quarter.id
    json.name quarter.name
    json.start_date quarter.start_date
    json.end_date quarter.end_date
  end
end

json.is_onboarding_configurable sso_names.uniq != ['email']
json.enable_role_based_authorization organization.enable_role_based_authorization
json.errors organization.errors.full_messages unless organization.errors.blank?

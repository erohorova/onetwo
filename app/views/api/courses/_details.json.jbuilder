active_member = GroupsUser.is_active_user_in_group?(user_id: current_user.id, group_id: course.id)

json.id course.id
json.savannah_id course.client_resource_id
json.name course.name
json.enrolled_students course.enrolled_students.count
json.description course.description
json.website_url course.website_url
json.image_url asset_path(course.image_url || image_url('card_default_image_min.jpg'))
json.start_date course.start_date
json.end_date course.end_date
json.url link_course_url(course)
json.duration (course.end_date ? (distance_of_time_in_words(course.start_date, course.end_date)+ ",") : "self-paced")
json.is_user_enrolled active_member

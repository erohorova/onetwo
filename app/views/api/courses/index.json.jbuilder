json.courses @courses do |course|
  json.partial! 'api/courses/details', course: course
end
json.count @courses_count

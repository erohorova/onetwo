json.array! @monthly_winners.each do |k, v|
  json.partial! 'api/video_streams/details', video_stream: v, user: current_user
  json.views_count v.views_count
  json.month k
end

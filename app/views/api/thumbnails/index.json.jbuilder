json.array! @thumbnails do |thumbnail|
  json.id thumbnail.id
  json.uri thumbnail.uri
  json.sequence_number thumbnail.sequence_number
  json.is_default thumbnail.is_default
end
json.extract! channel, :id, :label, :description, :is_private, :slug
json.image_url channel.mobile_photo
json.image_url_small channel.mobile_photo(:small)
json.banner_url channel.photo
json.banner_urls channel.fetch_bannerimage_urls
json.smartbites_count channel.cards.count
json.videos_count channel.videos_count
json.followers_count channel.followers.count
json.allow_follow channel.allow_follow
json.organization do
  if channel.organization_id
    json.extract! channel.organization, :id, :name
  else
    json.null!
  end
end

start_time = video_stream.start_time.to_time.strftime('%FT%T.%LZ')

json.extract! video_stream, :id, :slug, :uuid, :name, :status, :state, :votes_count, :comments_count, :width, :height, :card_id
json.xid video_stream.x_id
json.creator do
  if video_stream.creator
    if video_stream.creator.is_a?(User)
      creator = video_stream.creator
    else
      creator = User.find(video_stream.creator.id)
    end
    json.partial! 'users/capsule', user: creator
    json.id video_stream.creator.id
    json.following user && user.following?(video_stream.creator, 'User')
  else
    json.null!
  end
end

json.image_url video_stream.image || image_url('card_default_image_min.jpg')
json.playback_url video_stream.playback_url
json.rtmp_playback_url video_stream.rtmp_playback_url

if video_stream.is_a?(IrisVideoStream)
  json.embed_playback_url video_stream.embed_playback_url
end

json.start_time start_time
json.published_at start_time

json.tags do
  json.array! video_stream.tags, partial: 'tags/details', as: :tag
end
if user && user == video_stream.creator
  json.user_owns_stream true
  json.publish_token video_stream.publish_token
  json.publish_url video_stream.publish_url
end

json.upvoted user && voted_by?(video_stream, user)
if video_stream.past_stream_available?
  json.recording do
    json.partial! 'api/video_streams/recording_details', recording: video_stream.default_recording
  end
end
json.channels do
  json.array! video_stream.channels do |ch|
    json.id ch.id
    json.label ch.label
  end
end
json.share_url video_stream_url(video_stream.slug)

json.provider video_stream.provider_name
if video_stream.is_a?(Red5ProVideoStream)
  json.cluster_api_endpoint video_stream.cluster_api_endpoint
end
json.completion_state (current_user && current_user.completion_state_for('VideoStream', video_stream.id).try(&:upcase))
if defined?(assignment)
  json.assignment assignment
else
  json.assignment nil
end
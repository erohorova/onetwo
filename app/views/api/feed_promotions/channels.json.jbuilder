json.array! @channels do |channel|
  json.followers_count channel.followers.length
  json.extract! channel, :id, :label, :description, :slug
  json.photo channel.mobile_photo
  json.photo_small channel.mobile_photo(:small)
end

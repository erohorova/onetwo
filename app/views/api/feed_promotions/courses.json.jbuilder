json.array! @promotions do |promotion|
  json.id promotion.promotable.id
  json.savannah_id promotion.promotable.try(:client_resource_id)
  json.data promotion.data
  json.name promotion.name
  json.image_url asset_path(promotion.image_url || image_url('card_default_image_min.jpg'))
  json.start_date promotion.start_date
  json.end_date promotion.end_date
  json.url promotion_path(promotion)
  json.duration (promotion.end_date ? (distance_of_time_in_words(promotion.start_date, promotion.end_date)+ ",") : "self-paced")
end

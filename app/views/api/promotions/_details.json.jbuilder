json.extract! promotion, :id, :name, :description, :image_url
json.url promotion_url(promotion)
json.merge! promotion.data
json.institution promotion.data[:organization]

json.bookmarks do
  json.cards CardBridge.new(@bookmark_cards, user: current_user, options: { is_native_app: is_native_app? }).attributes
end
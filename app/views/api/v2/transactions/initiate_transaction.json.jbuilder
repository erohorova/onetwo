if @response
  json.transaction_details do
    json.order @response[:order], :id, :orderable_id, :orderable_type, :state
    json.token @response[:token]
    json.client_token @response[:client_token] if @response[:client_token].present?
    json.url @response[:redirect_url] if @response[:redirect_url].present?
  end
end

json.message @message if @message
json.card_purchased @card_purchased if @card_purchased
json.cards do
  json.array! @cards do |card|
    json.partial! 'api/v2/cards/details', card: card, assignment: @assignments.try(:[], card.id)
  end
end
json.total @total

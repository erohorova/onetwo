json.extract! ucc, :completable_id, :completable_type, :topics, :state, :started_at, :completed_at
if @user_badge.present?
  json.user_badge do
    json.partial! 'api/v2/users/user_badges', user_badge: @user_badge
  end
end

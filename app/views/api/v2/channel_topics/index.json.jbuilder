json.topics do
  json.array! @channel_topics do |channel_topic|
    json.partial! 'api/v2/channel_topics/details', channel_topic: channel_topic
  end
end

json.comments do
  json.array! @comments do |comment|
    json.partial! 'api/v2/comments/details', comment: comment
    json.is_reported @reported_comment_ids.include?(comment.id)
  end
end

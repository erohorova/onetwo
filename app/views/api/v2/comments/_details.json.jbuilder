json.extract! comment, :id, :message, :votes_count, :is_mobile, :comments_count, :commentable_id, :commentable_type, :created_at, :updated_at
json.resource do
  json.partial! 'api/v2/resources/details', resource: comment.resource, object_type: comment if comment.resource
end
json.tags do
  json.array! comment.tags, partial: 'tags/details', as: :tag
end
json.mentions do
  json.array! comment.mentioned_users do |mu|
    json.partial! 'api/v2/users/capsule', user: mu
  end
end
json.user do
  json.partial! 'api/v2/users/capsule', user: comment.user, anonymous: comment.anonymous
  json.is_following current_user_following_user_id?(comment.user.id)
end

json.file_resources do
  json.array! comment.file_resources, partial: 'api/v2/file_resources/details', as: :file_resource
end
json.is_upvoted comment.voted_by?(current_user)

if(params[:load_voters] && params[:load_voters].to_bool)
  votes = comment.recent_votes
  json.voters  do
    json.array! votes do |vote|
      voter = vote.voter
      json.id voter.id
      json.name voter.full_name
      json.handle voter.handle
      json.avatarimages voter.fetch_avatar_urls_v2
    end
  end
end

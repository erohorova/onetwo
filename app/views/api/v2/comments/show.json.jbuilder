json.partial! 'api/v2/comments/details', comment: @comment
json.is_reported @comment.is_reported_by?(current_user)

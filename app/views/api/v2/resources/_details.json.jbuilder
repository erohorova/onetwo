is_native_app = is_native_app?
image_url = resource.image_url_secure || card_resource(object_type)
video_url = resource.video_url

json.id resource.id
json.url resource.url_with_context(current_user.try(:id))
json.title resource.title.try(:clean)
json.description resource.description.try(:clean)
json.type resource.type
json.site_name resource.site_name
json.video_url is_native_app ? video_url : secured_url(video_url, current_user.id, @current_org)
json.embed_html resource.embed_html
json.image_url is_native_app ? image_url : secured_url(image_url, current_user.id, @current_org)

json.file_resources do
  json.array! resource.file_resources do |file_resource|
    json.partial! 'api/v2/file_resources/details', file_resource: file_resource
  end
end

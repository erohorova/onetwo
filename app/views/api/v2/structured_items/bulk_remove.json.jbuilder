json.structure_id current_structure.id
json.removed_entity_ids @entities.uniq.map(&:id)
json.entity_errors_ids @entity_errors_ids
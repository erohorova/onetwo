# Minified response should be processed only for:
#   1. Channels on Discover.
#   2. Users on Discover.
#   3. Users on Channels.

if @current_slug == 'discover-channels'
  json.structured_items do
    minified_channels = ChannelBridge.new(@entities, user: current_user).attributes
    json.array! minified_channels do |channel|
      json.entity do
        json.merge! channel
      end
    end
  end
elsif ['discover-users', 'channel-users'].include?(@current_slug)
  json.structured_items do
    user_fields = params[:user_fields] || 'id,handle,avatarimages,roles,name,is_following,expert_skills,status,following_count,followers_count'
    minified_users = UserBridge.new(@entities, user: current_user, fields: user_fields).attributes
    json.array! minified_users do |user|
      json.entity do
        json.merge! user
      end
    end
  end
else
  json.structured_items @entities do |entity|
    json.entity do
      if entity.class.name == "Channel"
        json.partial! 'api/v2/channels/discover', channel: entity
      elsif entity.class.name == "User"
        json.partial! 'api/v2/users/details', user: entity
      elsif entity.class.name == "Card"
        json.partial! 'api/v2/cards/details', card: entity
      end
    end
  end
end

json.structure_id current_structure.id

json.extract! item, :id, :position, :created_at, :updated_at
json.entity do
  if item.entity_type == "Channel"
    json.partial! 'api/v2/channels/discover', channel: item.entity
  elsif item.entity_type == "User"
    json.partial! 'api/v2/users/details', user: item.entity
  elsif item.entity_type == "Card"
    json.partial! 'api/v2/cards/details', card: item.entity
  end
end

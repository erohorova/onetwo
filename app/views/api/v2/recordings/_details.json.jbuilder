is_native_app = is_native_app?

json.id recording.id
json.hls_location is_native_app ? "https://#{Settings.aws_cdn_host}/#{recording.hls_location}" : secured_video_stream(recording.hls_location, current_user.id, @current_org)
json.mp4_location is_native_app ? "https://#{Settings.aws_cdn_host}/#{recording.mp4_location}" : secured_video_stream(recording.mp4_location, current_user.id, @current_org)

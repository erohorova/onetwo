json.extract! career_advisor, *%i{
  skill
  links
}
json.career_advisor_cards do
  json.array! career_advisor.career_advisor_cards do |card|
    json.id card.id
    json.card_id card.card_id
    json.card_label card.card_label
    json.level card.level
  end
end

json.extract! mailer_config, :id, :from_name, :from_address, :is_active
json.domain_verified mailer_config.verification[:domain]&.to_bool || false
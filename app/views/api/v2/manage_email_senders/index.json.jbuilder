json.manage_senders @email_senders do |mailer_config|
  json.partial! 'api/v2/manage_email_senders/details', mailer_config: mailer_config
end
json.default_sender_address current_org.from_address
json.count @total
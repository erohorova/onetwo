json.content @results do |flagged_comment|
  json.comment_id flagged_comment.comment_id
  json.comment_title flagged_comment.comments_message
  json.card_title flagged_comment.title.presence || flagged_comment.message&.truncate(27 ,seperator: ' ')
  json.card_id flagged_comment.card_id
  if flagged_comment.ecl_metadata.present?
    json.ecl_source_type_name JSON.parse(flagged_comment.ecl_metadata)["source_type_name"]
  else
    json.ecl_source_type_name nil
  end
  json.card_type flagged_comment&.card_type
  json.comment_created_by [flagged_comment.comment_author_first_name, flagged_comment.comment_author_last_name].join(' ')
  json.comment_created_at flagged_comment&.comment_created_at
  json.content_reporter flagged_comment, partial: 'api/v2/comment_reportings/details', as: :reporter
  json.total_reporter_count flagged_comment&.comment_reporting_count
end
json.total @total

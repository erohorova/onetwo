json.reported_by [reporter.reported_by_first_name, reporter.reported_by_last_name].join(' ')
json.reported_at reporter&.created_at
json.reason reporter.reason

if @comment_id.present?
  json.comment_id @comment_id
  json.reported_comments @results do |reported_comment|
    json.partial! 'api/v2/comment_reportings/details', reporter: reported_comment
  end
else
  json.partial! 'api/v2/comment_reportings/comment_details', results: @results
end

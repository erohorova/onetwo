json.id file_resource.id
json.available file_resource.available?
json.file_url file_resource.file_url
json.file_type file_resource.file_type
json.file_name file_resource.file_name
json.file_urls file_resource.fetch_attachment_urls
json.filestack file_resource.filestack
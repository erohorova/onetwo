json.sources_credentials do
  json.array! @sources_credentials do |credential|
    json.partial! 'api/v2/sources_credentials/details', source_credential: credential
  end
end
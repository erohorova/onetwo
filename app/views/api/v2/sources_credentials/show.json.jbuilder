json.sources_credential do
  json.partial! 'api/v2/sources_credentials/details', source_credential: @source_credential
end
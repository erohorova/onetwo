json.followers do
  json.array! @followers do |user|
    json.partial! 'api/v2/users/capsule', user: user
  end
end
json.total @total

json.total_count @versions_count
json.versions do
  json.array! @versions.each do |version|
    json.id version.id
    json.item_id version.item_id
    json.action version.event
    case version.item_type
    when 'ChannelsCurator'
      json.item_type "Curator"
      json.humanized_action "Curator " + humanized_action(version.event)
      curator = version.item.user rescue nil
      json.item do
        json.partial! 'api/v2/users/capsule', user: curator if curator.present?
      end
    when 'Follow'
      json.item_type "Follower"
      json.humanized_action "Follower " + humanized_action(version.event)
      curator = version.item.user rescue nil
      json.item do
        json.partial! 'api/v2/users/capsule', user: curator if curator.present?
      end
    when 'ChannelsCard'
      json.item_type version.item_type
      json.humanized_action "Card " + humanized_action(version.event)
      card = version.item.card rescue nil
      json.item do
        json.partial! 'api/v2/cards/details', card: card if card.present?
      end
    else 
      json.item_type version.item_type
      json.humanized_action "Channel " + humanized_action(version.event)
      json.item do
        json.partial! 'api/v2/channels/details', channel: @channel
      end
    end
    actor = User.find_by(id: version.whodunnit)
    json.action_performed_at version.created_at
    json.actor do
      json.partial! 'api/v2/users/capsule', user: actor if actor.present?
    end
    if version.object_changes.present?
      json.changes PaperTrail.serializer.load(version.object_changes)
    end
  end
end
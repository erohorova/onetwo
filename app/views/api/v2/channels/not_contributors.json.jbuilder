json.not_authors do
  json.array! @not_authors do |user|
    json.partial! 'api/v2/users/capsule', user: user
  end
end
json.total @total
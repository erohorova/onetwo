json.teams do 
  json.array! @teams do |team|
    json.id team.id
    json.name team.name
  end
end
json.total @total

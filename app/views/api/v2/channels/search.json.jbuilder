json.channels do
  if @channels.present?
    fields = 'id,label,is_private,slug,allow_follow,followers_count,profile_image_url,is_following'
    json.merge! ChannelBridge.new(@channels, user: current_user, fields: fields).attributes
  else
    json.merge! []
  end
end
json.aggs @aggs if @aggs
json.curators do
  json.array! @curators do |user|
    json.partial! 'api/v2/users/capsule', user: user
  end
end
json.total @total

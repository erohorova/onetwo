json.query params[:q]
json.results do
  json.array! @channels do |ch|
    json.id ch.id
    json.label ch.label
  end
end
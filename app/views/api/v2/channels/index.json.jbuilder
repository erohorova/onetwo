json.channels do
  if params[:is_cms]&.to_bool || (is_native_app? && params[:fields].nil?)
    json.array! @channels do |channel|
      json.partial! 'api/v2/channels/details', channel: channel
    end
  else
    json.merge! ChannelBridge.new(@channels, user: current_user, fields: params[:fields]).attributes
  end
end
json.total @total

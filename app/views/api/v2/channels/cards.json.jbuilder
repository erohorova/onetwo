json.cards do
  if !is_native_app? || params[:fields]
    json.merge! CardBridge.new(@cards, user: current_user, fields: params[:fields], options: @options).attributes
  else
    json.array! @cards do |card|
      json.partial! 'api/v2/cards/details', card: card
    end
  end
end
json.total @total

if params[:last_access_at].present?
  json.new_cards_total @new_cards_total
end

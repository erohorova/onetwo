json.channels do
  json.array! @channels do |channel|

    json.extract! channel, *%i{
      id
  	  label
      description
      updated_at
      auto_follow
      keywords
      only_authors_can_post
      is_private
      slug
      ecl_enabled
      curate_only
      curate_ugc
      is_promoted
      is_open
      provider
      provider_image
      allow_follow
      auto_pin_cards
      shareable
    }
    json.banner_image_url channel.image #TODO: should be removed in future, this should be replaced by `banner_image_urls`
    json.banner_image_urls channel.fetch_bannerimage_urls
    json.profile_image_url channel.mobile_photo #TODO: should be removed in future, this should be replaced by `profile_image_urls`
    json.profile_image_urls channel.fetch_profile_image_urls
    json.followers_count channel.all_followers.count
    json.is_owner (current_user && channel.creator == current_user)
    json.is_following channel.is_follower?(current_user)

    all_authors_count = @all_authors_count[channel.id] || 0
    json.authors_count all_authors_count
    json.created_at channel.created_at
    json.author_name channel.creator_name
    json.parent_org @organization_name[channel.id].map(&:name).first
    json.cloned_channel_id @cloned_channel_ids[channel.id].try(:[],0).try(:id)

    if channel.shareable
      json.shared_with @shared_with[channel.id]&.map(&:name)
    end

    json.curators do
      json.array! channel.curators do |curator|
        json.partial! 'api/v2/users/capsule', user: curator
      end
    end

    json.carousels channel.config&.parsed_array_value || []
    json.video_streams_count channel.curated_streams.count
    json.courses_count channel.curated_courses.count
    json.smartbites_count channel.curated_smartbites.count
    json.published_pathways_count channel.curated_published_pathways.count
    json.published_journeys_count channel.curated_published_journeys.count
    json.is_curator channel.is_curator?(current_user)
    json.is_trusted_collaborator channel.is_trusted_author?(current_user)

    json.teams do
      json.array! channel.followed_teams do |team|
        json.name team.name
        json.id team.id
      end
    end

    if channel.creator
      json.creator do
        json.partial! 'api/v2/users/capsule', user: channel.creator
      end
    end
  end#Channel end
end#Channels end
json.total @total

json.extract! channel, *%i{
  id
  label
  is_private
  slug
  allow_follow
}

json.banner_image_url channel.image #TODO: should be removed in future, this should be replaced by `banner_image_urls`
json.banner_image_urls channel.fetch_bannerimage_urls
json.profile_image_url channel.mobile_photo #TODO: should be removed in future, this should be replaced by `profile_image_urls`
json.profile_image_urls channel.fetch_profile_image_urls

json.is_following channel.is_follower?(current_user)


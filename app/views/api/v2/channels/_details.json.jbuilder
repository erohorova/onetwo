# INTENTION:
#   1. Returns everything if request originated from mobile apps.
#   2. Returns minimal response if search triggered from web.
#   3. Returns everything from other web-originated requests.
minimal_view = !is_native_app? && (defined?(mini_view) ? mini_view : false)

json.extract! channel, *%i{
  id
  label
  description
  updated_at
  auto_follow
  keywords
  only_authors_can_post
  is_private
  slug
  ecl_enabled
  curate_only
  curate_ugc
  is_promoted
  is_open
  provider
  allow_follow
  auto_pin_cards
  shareable
  language
  content_type
}
json.provider_image secured_url(channel.provider_image, current_user.id, @current_org) if channel.provider_image
json.banner_image_url channel.image #TODO: should be removed in future, this should be replaced by `banner_image_urls`
json.banner_image_urls channel.fetch_bannerimage_urls
json.profile_image_url channel.mobile_photo #TODO: should be removed in future, this should be replaced by `profile_image_urls`
json.profile_image_urls channel.fetch_profile_image_urls
json.card_fetch_limit channel.card_fetch_limit
unless minimal_view
  json.followers_count ChannelBridge.new([channel], user: current_user, fields: "followers_count").attributes[0][:followersCount] || 0
  json.is_owner (current_user && channel.creator == current_user)
end

json.is_following channel.is_follower?(current_user)

unless minimal_view
  all_authors_count = @all_authors_count ? (@all_authors_count[channel.id] || 0) : 0
  json.authors_count all_authors_count
  json.created_at channel.created_at
  json.author_name channel.creator_name
end

unless minimal_view
  json.curators do
    json.array! channel.curators do |curator|
      json.partial! 'api/v2/users/capsule', user: curator
    end
  end
end

unless minimal_view
  json.carousels channel.config&.parsed_array_value || channel.get_default_carousels
  json.video_streams_count channel.curated_streams.count
  json.courses_count channel.curated_courses.count
  json.smartbites_count channel.curated_smartbites.count
  json.published_pathways_count channel.curated_published_pathways.count
  json.published_journeys_count channel.curated_published_journeys.count
  json.is_curator channel.is_curator?(current_user)
  json.is_trusted_collaborator channel.is_trusted_author?(current_user)

  json.teams do
    json.array! channel.followed_teams do |team|
      json.name team.name
      json.id team.id
    end
  end

  if channel.creator
    json.creator do
      json.partial! 'api/v2/users/capsule', user: channel.creator
    end
  end
end

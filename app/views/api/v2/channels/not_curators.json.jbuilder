json.not_curators do
  json.array! @not_curators do |user|
    json.partial! 'api/v2/users/capsule', user: user
  end
end
json.total @total
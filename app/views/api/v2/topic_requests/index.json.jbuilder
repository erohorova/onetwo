json.topic_requests do
  json.array! @topic_requests do |topic_request|
    json.partial! 'api/v2/topic_requests/details', topic_request: topic_request
  end
end
json.total @total


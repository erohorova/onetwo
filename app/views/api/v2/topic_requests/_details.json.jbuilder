json.extract! topic_request, *%i{id label state created_at updated_at}
json.requestor do
  json.id topic_request.requestor.id
  json.full_name topic_request.requestor.full_name
  json.email topic_request.requestor.email
end
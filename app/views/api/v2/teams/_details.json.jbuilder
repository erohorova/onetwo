# TODO: TeamBridge will be introduced asap. This will be removed accordingly.
roles = @existing_roles.blank? ? [] : @existing_roles[team.id].presence || []

json.id team.id
json.name team.name
json.description team.description
json.slug team.slug
json.organization_id team.organization_id
json.created_at team.created_at
json.created_by team.admins.first.try(:name)
json.domains team.domains
json.image_urls do
  json.small team.image.url(:small)
  json.medium team.image.url(:medium)
  json.large team.image.url(:large)
end

json.members_count team.users.visible_members.uniq.count
json.pending_members_count team.invitations.pending.count
json.is_everyone_team team.is_everyone_team
json.auto_assign_content team.auto_assign_content
json.is_private team.is_private
json.is_team_admin roles.include?('admin')
json.is_team_sub_admin roles.include?('sub_admin')
json.is_member roles.include?('member')
json.is_pending team.is_pending?(current_user)
json.is_dynamic team.is_dynamic
json.is_mandatory team.is_mandatory
json.is_dynamic_selected !!team.external_id
json.only_admin_can_post team.only_admin_can_post
json.carousels team.config&.parsed_array_value || team.get_default_carousels

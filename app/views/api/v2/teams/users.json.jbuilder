json.query @query
json.members do
  json.total @total
  json.users do
    json.array! @users do |user|
      json.extract! user, *%i[id name email handle photo role pending is_followed is_current_user]
    end
  end
end

json.pending do
  json.total @invitations_total
  json.users do
    json.array! @invitations do |invitation|
      json.id invitation.id
      json.user_id invitation.recipient_id
      json.email invitation.recipient_email
      json.role invitation.roles
      json.name invitation.recipient.try(:name)
      json.handle invitation.recipient.try(:handle)
      json.photo invitation.recipient.try(:photo)
      json.is_followed @current_user.following_user_id?(invitation.recipient.try(:id))
    end
  end
end

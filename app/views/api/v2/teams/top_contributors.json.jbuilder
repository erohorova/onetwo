json.top_contributors do
  json.array! @top_contributors do |contributor|
    json.id         contributor[:user].id
    json.email      contributor[:user].email
    json.first_name contributor[:user].first_name
    json.last_name  contributor[:user].last_name
    json.handle     contributor[:user].handle
    json.photo      contributor[:user].picture_url
    json.score      contributor[:score]
  end
end
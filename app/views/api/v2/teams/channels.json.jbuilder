json.team @team.id
json.total @total_channels
json.channels do
  json.array! @channels do |channel|
    json.partial! 'api/v2/channels/details', channel: channel
  end
end
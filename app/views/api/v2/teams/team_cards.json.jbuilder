json.team @team.id
json.total @total
json.cards do
  if is_native_app?
    json.array! @cards do |card|
      json.partial! 'api/v2/cards/details', card: card
    end
  else
    json.merge! CardBridge.new(@cards, user: current_user, options: { is_native_app: is_native_app? }).attributes
  end
end
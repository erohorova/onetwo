json.partial! 'api/v2/teams/details', team: @team
json.user do
  json.id @user.id
  json.name @user.name
  json.handle @user.handle
  json.role @role
end
json.teams do
  # For edc-cms and mobile will need old response
  if is_native_app? || params[:full_response]
    json.array! @teams do |team|
      json.partial! 'api/v2/teams/details', team: team
    end
  else
    json.merge! TeamBridge.new(@teams, user: current_user, fields: params[:fields]).attributes
  end
end

json.total @total
json.aggs @aggs if @aggs

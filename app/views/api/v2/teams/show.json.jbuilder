json.partial! 'api/v2/teams/details', team: @team
json.owners do
  json.array! @team.admins.not_suspended do |admin|
    json.extract! admin, :id, :handle, :first_name, :last_name, :email, :picture_url, :name
    json.avatarimages admin.fetch_avatar_urls_v2
  end
end
json.sub_admins do
  json.array! @team.sub_admins.not_suspended do |admin|
    json.extract! admin, :id, :handle, :first_name, :last_name, :email, :picture_url, :name
    json.avatarimages admin.fetch_avatar_urls_v2
  end
end

json.message @message if @message.present?


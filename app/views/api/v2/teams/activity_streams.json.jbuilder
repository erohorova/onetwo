json.activity_streams do
  json.array! @team_streams do |team_stream|
    json.extract! team_stream, :created_at, :action, :id, :updated_at, :user_id, :organization_id, :comments_count, :votes_count
    json.comments do
      json.array! team_stream.comments do |comment|
        json.partial! 'api/v2/comments/details', comment: comment
      end
    end
    json.voters do
      json.array! team_stream.votes do |vote|
        json.id vote.user_id
        json.name vote.voter.try(:full_name)
      end
    end

    json.snippet team_stream.linkable_item.snippet
    deep_link_id, deep_link_type = get_app_deep_link_id_and_type(team_stream.linkable_item)
    json.linkable do
      json.id deep_link_id
      json.type deep_link_type
    end

    json.streamable_id team_stream.streamable_id
    json.streamable_type team_stream.streamable_type

    if team_stream.linkable_item.class.in?([Team, Organization]) || team_stream.streamable_type == 'Comment'
      json.message do
        json.text team_stream.streamable.message
        json.mentions do
          json.array! team_stream.streamable.mentioned_users do |mu|
            json.partial! 'api/v2/users/capsule', user: mu
          end
        end
      end
    end

    json.user do
      json.id team_stream.user.id
      json.name team_stream.user.name
      json.avatarimages team_stream.user.fetch_avatar_urls_v2
      json.handle team_stream.user.handle
    end

    json.is_upvoted team_stream.voted_by?(current_user)
  end
end

json.teams do
  # For edc-cms and mobile will need old response
  if is_native_app? || params[:full_response]
    json.array! @teams do |team|
      json.partial! 'api/v2/teams/details', team: team
      json.roles team.teams_users.where(user: current_user).pluck(:as_type) if !@skip_roles

      if @assignable
        json.has_assignment @assigned_team_ids.include?(team.id)
        json.unassigned_members_count @members_count[team.id]
      end
    end
  else
    json.merge! TeamBridge.new(@teams, user: current_user, fields: params[:fields]).attributes
  end
end

json.total @total

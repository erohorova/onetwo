json.top_content do 
  json.array! @cards do |card|
    json.id card.id
    json.title card.message || card.title
    json.score card['totalScore']
  end
end
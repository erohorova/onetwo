active_member = GroupsUser.is_active_user_in_group?(user_id: user.id, group_id: group.id)

json.extract! group, :id, :name, :description, :course_code, :start_date, :end_date
json.learn_url group.website_url
json.logo_url group.image_url
json.course_id group.client_resource_id.try(:to_i)
json.is_user_enrolled active_member
json.students_count group.enrolled_students.count
json.featured group.is_promoted
json.instructors do
  json.array! group.admins do |user|
    json.id user.id
    json.first_name user.first_name
    json.last_name user.last_name
    json.name user.name
    json.email user.email
    json.picture user.picture_url
  end
end

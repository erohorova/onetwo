# DESC:
#   Mobile will still receive the old response for cards, pathways and channels.
#   #is_native_app? detects if the requests are mobile-originated.
legacy_response = is_native_app?

json.cards do
  if legacy_response
    json.array! @cards do |card|
      json.partial! 'api/v2/cards/details', card: card, mini_view: true
    end
  else
    json.merge! CardBridge.new(@cards, user: current_user, fields: params[:fields], options: @options).attributes
  end
end

if params['segregate_pathways'] == 'true'
  json.pathways do
    if legacy_response
      json.array! @pathways do |pathway|
        json.partial! 'api/v2/cards/details', card: pathway, mini_view: true
      end
    else
      json.merge! CardBridge.new(@pathways, user: current_user, fields: params[:fields], options: @options).attributes
    end
  end
  json.journeys do
    if legacy_response
      json.array! @journeys do |journey|
        json.partial! 'api/v2/cards/details', card: journey, mini_view: true
      end
    else
      json.merge! CardBridge.new(@journeys, user: current_user, fields: params[:fields], options: @options).attributes
    end
  end
end

json.users do
  if @users.present?
    json.merge! UserBridge.new(@users, user: current_user, fields: 'id,email,handle,avatarimages,coverimages,roles,name,is_following,expert_skills,job_title,followers_count,following_count').attributes
  else
    json.merge! []
  end
end
json.channels do
  if @channels.present?
    json.merge! ChannelBridge.new(@channels, user: current_user).attributes
  else
    json.merge! []
  end
end
json.teams do
  if @teams.present?
    json.merge! TeamBridge.new(@teams, user: current_user).attributes
  else
    json.merge! []
  end
end

json.offset @offset
json.limit @limit
json.cards_offset @cards_offset
json.cards_limit @cards_limit
json.channels_offset @channels_offset
json.channels_limit @channels_limit
json.teams_offset @teams_offset
json.teams_limit @teams_limit
json.users_offset @users_offset
json.users_limit @users_limit
json.query @query
json.aggregations @aggs

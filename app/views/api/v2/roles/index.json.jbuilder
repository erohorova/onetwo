# NOTE: this will be refactored. this is not the final version.
# Cannot handle roles from users table. Information passed in this
# response is going to be used in other API's
json.total @total_count
json.roles do
  json.array! @roles do |role|
    json.id role.id
    json.name role.name
    json.default_name role.default_name
    json.permissions do
      # TODO: use includes role_permissions in #index
      # supporting organization_role from users and includes won't work. Refactor this
      json.array! role.role_permissions do |role_permission|
        json.partial! 'api/v2/role_permissions/details', role_permission: role_permission
      end
    end
  end
end

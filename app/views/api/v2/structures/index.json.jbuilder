json.structures @structures do |structure|
  json.partial! 'api/v2/structures/structure', structure: structure
end

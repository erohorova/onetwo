json.extract! structure, :id, :display_name, :created_at, :updated_at, :enabled, :slug, :context
json.organization structure.organization
json.parent structure.parent
json.creator structure.creator


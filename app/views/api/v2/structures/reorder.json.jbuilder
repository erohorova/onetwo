json.structured_items @structured_items do |structured_item|
  json.partial! 'api/v2/structured_items/structured_item', item: structured_item
end
json.structure_id @structure.id

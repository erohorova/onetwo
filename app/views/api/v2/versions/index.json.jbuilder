json.versions do
  json.array! @results do |result|
    json.user @users[result.user_id]
    json.updated_fields updated_fields_with_signed_filestack_urls(result.updated_fields, current_user.id, @current_org, is_native_app?)
    json.created_at result.created_at
  end
end

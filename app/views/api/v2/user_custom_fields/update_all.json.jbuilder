json.user_custom_fields do
  json.array! @user_custom_fields do |user_custom_field|
    json.custom_field_id user_custom_field.custom_field_id
    json.value user_custom_field.value
  end
end

json.message @message

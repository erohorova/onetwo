 if params[:send_array]
   # {user => [user_custom_fields]}
   json.array! @users.each do |user|
     json.id user.id
     json.name user.name
     json.custom_fields do
       json.array! @custom_fields_hash[user.id] do |user_custom_field|
         json.abbreviation user_custom_field.custom_field.abbreviation
         json.display_name user_custom_field.custom_field.display_name
         json.id user_custom_field.id
         json.value user_custom_field.value
       end
     end
   end
 else
  # { user_id => { custom_field_id => value }  }
   @users.each do |user|
      json.set! user.id do |json|
        custom_fields = user.user_custom_fields_hash(@custom_fields_hash)
        custom_fields.each { |k,v| json.set! k, v } if custom_fields.present?
      end
    end
  end



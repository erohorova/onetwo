@custom_fields_hash.each do |user_id, user_custom_fields|
  json.set! user_id do |json|
    user_custom_fields.each  do |user_custom_field|
      next if @last_fetch_date.present? && (user_custom_field.updated_at < @last_fetch_date)
      json.set! user_custom_field.custom_field_id, user_custom_field.value
    end
  end
end
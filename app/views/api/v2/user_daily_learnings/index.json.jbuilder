json.state @state
json.cards do
  json.merge! CardBridge.new(@cards, user: current_user, fields: params[:fields], options: { is_native_app: is_native_app? }).attributes
end

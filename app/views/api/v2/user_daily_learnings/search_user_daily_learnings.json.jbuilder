json.state @state
json.cards do
  if is_native_app?
    json.array! @cards do |card|
      json.partial! 'api/v2/cards/details', card: card
    end
  else
    json.merge! CardBridge.new(@cards, user: current_user, options: @options, fields: params[:fields]).attributes
  end
end

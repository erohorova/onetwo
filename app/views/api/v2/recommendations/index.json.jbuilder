# cards
json.cards do
  json.array! @results[:cards] do |card|
    json.partial! 'api/v2/cards/details', card: card
    json.sim_wt @results[:content_weights][card.ecl_id]
  end
end

# topics
json.topics do
  json.array! @results[:topics]
end

# users
json.users do
  json.array! @results[:users] do |user|
    json.partial! 'api/v2/users/capsule', user: user
  end
end
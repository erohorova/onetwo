json.org_sso do
  json.partial! 'api/v2/org_ssos/details', org_sso: @org_sso
end

json.extract! org_sso, :id, :organization_id, :position, :is_enabled,
  :saml_idp_sso_target_url, :saml_issuer, :saml_idp_cert, :saml_assertion_consumer_service_url, :saml_allowed_clock_drift,
  :oauth_login_url, :oauth_authorize_url, :oauth_token_url, :oauth_user_info_url, :label_name, :oauth_client_id, :oauth_client_secret,
  :oauth_scopes

json.name org_sso.sso.name
json.description org_sso.sso.description
json.icon_url image_path("sso/#{org_sso.customised_sso_name}.png")


if org_sso.provider_name.present?
  json.logo image_path("sso/#{org_sso.provider_name}.png")
  json.provider_name org_sso.provider_name
end

if org_sso.idp.present?
  json.idp_id org_sso.idp
end

json.users do
  json.array! @users do |user|
    json.partial! 'api/v2/users/capsule', user: user
  end
end

json.total @following_count
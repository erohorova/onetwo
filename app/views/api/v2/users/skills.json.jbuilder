is_native_app = is_native_app?

json.skills do
  json.array! @user_skills do |user_skill|
    json.extract! user_skill, :id, :description, :credential, :credential_name, :credential_url, :experience, :skill_level, :expiry_date
    json.credential user_skill.credential do |credential|
      json.mimetype credential['mimetype']
      json.size credential['size']
      json.source credential['source']
      json.url is_native_app ? credential['url'] : secured_url(credential['url'], current_user.id, @current_org)
      json.handle credential['handle']
      json.status credential['status']
    end
    json.name user_skill.skill.name
  end
end

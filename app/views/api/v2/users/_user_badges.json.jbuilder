if user_badge.card_badging.present?
  json.extract! user_badge.card_badging, :id, :title
  json.identifier user_badge.identifier
  json.image_url user_badge.card_badging.card_badge_image.url(:large)
  json.type "CardBadge"
  json.created_at user_badge.created_at
elsif user_badge.clc_badging.present?
  json.extract! user_badge.clc_badging, :id, :title
  json.identifier user_badge.identifier
  json.image_url user_badge.clc_badging.clc_badge_image&.url(:large)
  json.type "ClcBadge"
  json.created_at user_badge.created_at
end

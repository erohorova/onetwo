json.projects do
  json.array! @projects do |project|
    json.project_id project.id
    json.card_id project.card_id
    json.card_title project.card.title
    json.card_message project.card.message
  end
end

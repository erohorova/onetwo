json.users do
  if params[:fields].present?
    fields = @is_admin_user ? "#{params[:fields]},current_sign_in_at" : params[:fields]
    json.merge! UserBridge.new(@users, user: current_user, fields: fields).attributes
  else
    json.array! @users do |user|
      json.partial! 'api/v2/users/details', user: user
      if @is_admin_user
        json.current_sign_in_at user.current_sign_in_at
      end
    end
  end
end
json.total @total

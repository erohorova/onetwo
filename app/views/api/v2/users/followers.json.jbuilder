json.users do
  json.array! @users do |user|
    json.partial! 'api/v2/users/capsule', user: user
    json.is_following @following.try(:[], 'User').try(:[], user.id) || false
  end
end

json.total @followers_count

if current_user.profile
  json.extract! current_user.profile, *%i{
    expert_topics
    learning_topics
    time_zone
    tac_accepted
    language
    company
    job_role
  }
else
  {}
end
is_native_app = is_native_app?

json.profile do
  json.id @user.id
  json.email @user.email
  json.first_name @user.first_name
  json.last_name @user.last_name
  json.avatarimages @user.fetch_avatar_urls_v2
  json.time_zone @user.time_zone
  json.language @user.language
  json.bio @user.bio
  json.coverimages @user.fetch_coverimage_urls
  json.is_following current_user && current_user.following?(@user)
  json.name @user.name
  json.roles @user.roles.map { |role| role.name }
  json.job_title @user.job_title
  json.dashboard_info @user.profile.try(:dashboard_info)
end

json.user_card_badges do
  json.total_count @user_card_badges_count
  json.array! @user_card_badges do |user_badge|
    json.partial! 'api/v2/users/user_badges', user_badge: user_badge
  end
end

json.user_clc_badges do
  json.total_count @user_clc_badges_count
  json.array! @user_clc_badges do |user_badge|
    json.partial! 'api/v2/users/user_badges', user_badge: user_badge
  end
end

json.skills do
  json.array! @user_skills do |user_skill|
    json.extract! user_skill, :id, :description, :credential, :credential_name, :credential_url, :experience, :skill_level, :expiry_date
    json.credential user_skill.credential do |credential|
      json.mimetype credential['mimetype']
      json.size credential['size']
      json.source credential['source']
      json.url is_native_app ? credential['url'] : secured_url(credential['url'], current_user.id, @current_org)
      json.handle credential['handle']
      json.status credential['status']
    end
    json.name user_skill.skill.name
  end
end

json.external_courses @external_courses do |course|
  json.extract! course, :id, :name, :description, :image_url, :course_url, :status, :raw_info, :due_date, :assigned_date
  json.integration_name course.integration.name
  json.integration_logo_url course.integration.logo(:medium)
end

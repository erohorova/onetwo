json.users do
  json.array! @users do |user|
    json.partial! 'api/v2/users/details', user: user
    json.current_sign_in_at user.current_sign_in_at
  end
end
json.total_count @total

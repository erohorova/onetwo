json.total_count @user_badges_count
json.user_badges do
  json.array! @user_badges do |user_badge|
    json.partial! 'api/v2/users/user_badges', user_badge: user_badge
    if user_badge.card_badging.present?
      json.card do
        card = Card.unscoped.find_by(organization: current_org, id:  user_badge.card_badging.badgeable_id)
        json.extract! card, :id, :title, :message
        json.image_url card.try(:image_attachment)
      end
    elsif user_badge.clc_badging.present?
      clc = Clc.unscoped.find_by(user_badge.clc_badging.badgeable_id)
      json.clc do
        json.partial! 'api/v2/clcs/progress', clc: clc
      end
    end
  end
end

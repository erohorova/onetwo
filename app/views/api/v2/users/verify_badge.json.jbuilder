user = @user_badge.user
organization = user.organization
json.organization do
  json.name organization.name
  json.image_url organization.mobile_photo || organization.photo
  json.banner_url organization.photo
  json.co_branding_logo organization.co_branding_logo.try(:url)
end
json.user do
  json.extract! user, :first_name, :last_name, :picture_url, :name
end
json.badge do
  json.title @user_badge.card_badging.title
  json.image_url @user_badge.card_badging.card_badge_image.url(:large)
  json.date @user_badge.created_at
end
json.users do
  if @users.present?
    fields = params[:fields] || 'id,handle,avatarimages,roles,roles_default_names,name,is_following,following_count,followers_count,expert_skills,company'
    json.merge! UserBridge.new(@users, user: current_user, fields: fields).attributes
  else
    json.merge! []
  end
end
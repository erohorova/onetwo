json.extract! user, :id, :handle, :last_name, :email, :bio,
                    :picture_url, :name, :is_complete, :failed_attempts, :locked_at,
                    :status, :exclude_from_leaderboard, :default_team_id, :hide_from_leaderboard
json.first_name user.first_name.present? ? user.first_name : user.name
json.sign_in_count user.sign_in_count
json.followers_count user.followers.count
json.following_count user.following_count
json.avatarimages user.fetch_avatar_urls_v2
json.coverimages user.fetch_coverimage_urls_v2
json.is_following current_user && current_user.following?(user)
json.full_name user.full_name
json.is_suspended user.suspended?

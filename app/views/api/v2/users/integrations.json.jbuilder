json.integrations do
  json.array! @integrations do |integration|
    json.extract! integration, :id, :name, :description
    json.logo_url integration.logo(:small)
    json.callback_url integration.callback_url
    users_integration = @user_integrations[integration.id]
    json.connected users_integration && users_integration.connected
    json.fields @user.fields_for(integration)
  end
end

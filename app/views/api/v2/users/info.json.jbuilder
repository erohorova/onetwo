json.extract! @user, :id, :email, :first_name, :last_name, :handle, :picture_url, :default_team_id, :bio, :is_suspended
json.token @user.jwt_token

json.coverimage_urls @user.fetch_coverimage_urls
json.avatarimage_urls @user.fetch_avatar_urls_v2

# Add join queries to do below things
json.following_count @user.following.count
json.followers_count @user.followers.count

json.roles @user.get_user_roles
json.roles_default_names @user.user_roles_default_names
json.permissions @user.get_all_user_permissions

# To avoid extra API for onboarding status
json.onboarding_completed @user.onboarding_completed?
json.step @user.user_onboarding.try(:current_step)
json.pwc_records do
  json.array! @pwc_records do |record|
    json.extract! record, :skill, :level, :user_id, :abbr, :created_at, :updated_at
  end
end
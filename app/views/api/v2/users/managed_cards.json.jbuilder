json.managed_card_actions do
  json.array! @cards_by_action do |(action, rows)|
    json.action action
    json.entries rows do |entry|
      json.card_id entry.card_id
      json.target_id entry.target_id
    end
  end
end
json.cards do
  json.array! @cards do |card|
    json.partial! 'api/v2/cards/details', card: card, assignment: @assignments.try(:[], card.id)
  end
end

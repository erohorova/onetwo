json.jwt_token current_user.jwt_token
json.step current_user.try(:user_onboarding).try(:current_step)
json.user current_user.onboarding_data
json.onboarding_completed current_user.onboarding_completed?

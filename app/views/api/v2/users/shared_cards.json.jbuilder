json.cards do
  if is_native_app? || params[:full_response]
    json.array! @cards do |card|
      json.partial! 'api/v2/cards/details', card: card, assignment: @assignments.try(:[], card.id)
    end
  else
    json.merge! CardBridge.new(@cards, user: current_user, fields: params[:fields], options: { is_native_app: is_native_app? }).attributes
  end
end
json.courses do
  json.array! current_user.not_banned_from_groups do |group|
    json.extract! group, :id, :name
    json.savannah_id group.client_resource_id
  end
end
json.partial! 'api/v2/users/details', user: @user
json.onboarding_completed_date @user.user_onboarding.try(:completed_at)
json.dashboard_info @user.profile.try(:dashboard_info)
json.password_changeable @user.password_changeable?

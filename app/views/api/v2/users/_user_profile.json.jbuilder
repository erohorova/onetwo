# DEPRECATED
if profile
  json.profile do
    json.id profile.id
    json.expert_topics profile.expert_topics
    json.learning_topics profile.learning_topics
    json.time_zone profile.time_zone
    json.tac_accepted profile.tac_accepted
    json.tac_accepted_at profile.tac_accepted_at
    json.language profile.language
    json.job_title profile.job_title
    json.job_role profile.job_role
    json.company profile.company
  end
else
  json.profile nil
end

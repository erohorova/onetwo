json.partial! 'api/v2/users/capsule', user: user
json.partial! 'api/v2/users/user_profile', profile: user.profile
json.is_following @following.try(:[], 'User').try(:[], user.id) || false
json.roles user.get_user_roles
json.roles_default_names user.user_roles_default_names
json.user_teams do
  json.array! user.teams.uniq do |team|
    json.team_id team.id
    json.name team.name
    json.is_dynamic team.is_dynamic
    json.is_mandatory team.is_mandatory
  end
end
json.pending_team_invitations do
  json.array! user.invitations do |invitation|
    if !invitation.accepted_at? && invitation.invitable_type == "Team"
      json.invitable_id invitation.invitable_id
      json.invitable_type invitation.invitable_type
    end
  end
end
json.created_at user.created_at

if @show_payment_info
  latest_active_org_subscription = user.active_org_subscriptions.last
  json.payment_date latest_active_org_subscription.try(:created_at)
  json.currency latest_active_org_subscription.try(:currency)
  json.payment_amount latest_active_org_subscription.try(:amount)
end

json.external_courses @external_courses do |course|
  json.extract! course, :id, :name, :description, :image_url, :course_url, :status, :raw_info, :due_date, :assigned_date
  json.integration_name course.integration.name
  json.integration_logo_url course.integration.logo(:medium)
end

json.internal_courses @internal_courses do |course|
  json.extract! course, :id, :name, :description
  json.image_url course.image
  json.course_url link_course_path(id: course.id)
  json.status (course.end_date.nil? || course.end_date > Date.current) ? "Enrolled" : "Completed"
  json.duration duration(course)
end

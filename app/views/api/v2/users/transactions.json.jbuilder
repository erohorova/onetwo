json.transactions do
  json.array! @transactions do |transaction|
    order = transaction.order
    json.id transaction.id
    json.order order.orderable_type
    json.mode transaction.via_wallet? ? 'Skillcoin' : 'Card Payment'

    # The following block of keys is used for showing information on whether skillcoins were credited or debited in a transaction
    # We need to show skillcoin information only for following 2 cases
    #   1. Course purchased using wallet(skillcoins) where type - debit
    #   2. Wallet recharged using any gateway where type - credit
    if (transaction.via_wallet? || order.wallet_recharge?)
      wallet_transaction = order.wallet_transaction
      json.type wallet_transaction&.payment_type
      json.skillcoins wallet_transaction&.amount
      json.balance wallet_transaction&.balance unless wallet_transaction&.failed?
    end

    # We need to skip the amount information (INR/USD)) when a course is purchased using skillcoins
    # But when a wallet is recharged, we cannot skip this, as user needs to know how much amount was spent 
    # in INR/USD in order to recharge.
    unless transaction.via_wallet?
      json.price_details do
        json.amount transaction.amount
        json.currency transaction.currency
        json.symbol (CURRENCY_TO_SYMBOL[transaction.currency] || "$")
      end
    end
    json.status transaction.payment_state
    json.date transaction.updated_at
  end
end
json.transactions_count @total_transactions_count

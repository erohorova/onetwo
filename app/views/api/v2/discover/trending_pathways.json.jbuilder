if !is_native_app? || params[:fields]
  json.cards do
    json.merge! CardBridge.new(@cards, user: current_user, options: @options, fields: @fields).attributes
  end
else
  json.cards do
    json.array! @cards do |card|
      json.partial! 'api/v2/cards/details', card: card
    end
  end
end

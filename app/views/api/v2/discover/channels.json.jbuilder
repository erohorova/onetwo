json.channels do
  json.merge! ChannelBridge.new(@channels, user: current_user).attributes
end

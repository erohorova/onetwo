# DESC:
#   Used by #cards_by_channels, #carousel_content endpoints.
json.cards do
  json.array! @cards do |card|
    json.partial! 'api/v2/cards/details', card: card
  end
end

json.badges do
  json.array! @badges do |badge|
    json.partial! 'api/v2/organizations/badge_details', badge: badge
  end
end

json.total @total

is_native_app = is_native_app?

sso_names = []
json.id organization.id
json.name organization.name
json.description organization.description
json.is_open organization.is_open
json.is_registrable organization.is_registrable
json.image_url organization.mobile_photo || organization.photo
json.banner_url organization.photo
json.co_branding_logo organization.co_branding_logo.present? ? organization.co_branding_logo.url : nil
json.host_name organization.host_name
# Need to support vanity url home page
json.home_page organization.edcast_home_page
json.show_sub_brand_on_login organization.show_sub_brand_on_login
json.show_onboarding organization.show_onboarding
json.favicons organization.favicons
json.custom_domain organization.custom_domain
json.ssos do
  json.array! configured_ssos(organization) do |org_sso|
    sso_names << org_sso.sso.name
    json.name org_sso.customised_sso_name
    json.label_name org_sso.customised_label_name
    json.icon_url image_path("sso/#{org_sso.sso.name}.png")
    json.authentication_url "#{request.protocol}#{organization.host}/auth/#{org_sso.sso.name}"
    json.web_authentication_url sso_url(org_sso)
    json.sso_name org_sso.sso.name
    json.position org_sso.position
  end
end
json.cobrands do
  organization.fetch_cobrand_configs_with_defaults.each do |k, v|
    json.set! k, v
  end
end
json.configs do
  json.array! organization.configs do |config|
    json.id config.id
    json.name config.name
    case config.name
    when 'guide_me_config'
      json.value config.get_guide_me_value
    when 'custom_splash_screen'
      json.value (current_user && !is_native_app) ? secured_url(organization.splash_image_url, current_user.id, organization) : Filestack::Security.new(call: ["convert"], org: organization).signed_url(organization.splash_image_url)
    else
      json.value config.parsed_value
    end
  end
end

json.quarters do
  json.array! organization.quarter_config.each do |quarter|
    json.id quarter.id
    json.name quarter.name
    json.start_date quarter.start_date
    json.end_date quarter.end_date
  end
end

json.is_onboarding_configurable sso_names.uniq != ['email']
json.enable_role_based_authorization organization.enable_role_based_authorization
json.share_channels_enabled organization.share_channels_enabled?
json.errors organization.errors.full_messages unless organization.errors.blank?
json.onboarding_options organization.onboarding_options
json.onboarding_steps_names organization.onboarding_options&.keys
json.enable_analytics_reporting organization.enable_analytics_reporting
json.current_todays_learning organization.current_todays_learning
json.readable_card_types Organization.default_org.fetch_readable_card_types_configs&.keys
json.from_address organization.from_address

member_pay = @org_profile&.member_pay || false
json.member_pay member_pay

if(member_pay)
  json.org_subscriptions do
    json.array! @org_subscriptions do |subscription|
      json.id subscription.id
      json.name subscription.name
      json.description subscription.description
      json.start_date subscription.start_date
      json.end_date subscription.end_date
      json.prices do
        json.array! subscription.prices do |price|
          json.id price.id
          json.amount price.amount
          json.currency price.currency
          json.symbol CURRENCY_TO_SYMBOL[price.currency] || "$"
        end
      end
    end
  end
end

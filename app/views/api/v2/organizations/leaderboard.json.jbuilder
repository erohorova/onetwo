json.users do
  json.array! @metrics do |metric|
    json.partial! 'leaderboard_metrics', metric: metric, user: @users[metric['user_id']]
  end
end
if @current_user_metrics.present?
  json.current_user do
    json.partial! 'leaderboard_metrics', metric: @current_user_metrics, user: current_user
  end
else
  # Ruby interprets empty hash as a block. Hence, json.set!
  json.set! :current_user, {}
end
json.total_count @total_count

json.smartbites_score metric.smartbites_score
json.smartbites_consumed metric.smartbites_consumed
json.smartbites_created metric.smartbites_created
json.time_spent metric.time_spent
json.smartbites_comments_count metric.smartbites_comments_count
json.smartbites_liked metric.smartbites_liked
json.smartbites_completed metric.smartbites_completed
json.user do  
  json.id user.id
  json.email user.email
  json.order_by_score metric.level_order
  json.name user.name
  json.bio user.bio
  json.handle user.handle
  json.picture user.photo(:medium)
  json.following current_user && current_user.following?(user)
  json.channel_count user.followed_channel_ids.count
  json.group_count user.teams.count
  json.followers_count user.followers.count
  json.expert_topics user.expert_topics || []
  json.roles user.get_user_roles
  json.roles_default_names user.user_roles_default_names
end
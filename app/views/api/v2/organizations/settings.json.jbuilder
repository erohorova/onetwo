json.configs do
  json.array! @organization.configs do |config|
    json.id config.id
    json.name config.name
    json.description config.description
    json.value config.parsed_value
  end
end
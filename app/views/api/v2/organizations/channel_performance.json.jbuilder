json.channel_performance do
  json.array! @channel_performance_data do |channel_id, data|
    json.channel_id channel_id
    json.channel_label data[:label]
    json.is_private data[:is_private]
    json.followers_count data[:followers_count]
    json.published_cards_count data[:published_cards_count]
    json.likes_count data[:likes_count]
    json.bookmarks_count data[:bookmarks_count]
    json.comments_count data[:comments_count]
    json.views_count data[:views_count]
    json.completes_count data[:completes_count]
  end
end

json.items do
  json.array! @metrics do |metric|
    json.item do
      json.id metric.content_id
      json.title metric.content_title || ""
      json.is_deleted metric.is_content_object_deleted?
      json.type metric.content_type
      json.url url_for_user_content_level_metric(metric)
      json.ui_layout_type content_nested_type(metric)

      if metric.content_ecl_metadata.present?
        json.ecl_source_display_name metric.content_ecl_metadata[:source_display_name]
        json.ecl_source_type_name metric.content_ecl_metadata[:source_type_name]
      end

    end
    json.views_count metric.views_count
    json.likes_count metric.likes_count
    json.comments_count metric.comments_count
    json.completes_count metric.completes_count
    json.bookmarks_count metric.bookmarks_count
  end
end

json.groups do
  json.array! @group_matches do |group|
    json.id group['id']
    json.name group['name']
    json.channel_ids group['channel_ids']
  end
end

json.users do
  json.array! @user_matches do |user|
    json.id user['id']
    json.name user['name']
    json.channel_ids user['followed_channels'].try(:map){|d| d['id']} || []
    json.team_ids user['teams'].try(:map){|d| d['id']} || []
  end
end

json.total @total
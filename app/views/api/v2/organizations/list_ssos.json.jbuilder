json.ssos do
  json.array! @ssos do |org_sso|
    # enabled sso's
    if org_sso.is_a? OrgSso
      json.partial! 'api/v2/org_ssos/details', org_sso: org_sso
    else
      # not enabled ssos
      sso = org_sso

      json.name sso.name
      json.description sso.description
      json.icon_url image_path("sso/#{sso.name}.png")
      json.is_enabled false
    end
  end
end

json.is_okta_enabled current_org.okta_enabled?

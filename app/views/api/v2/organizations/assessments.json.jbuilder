json.assessment_details do
  json.array! @assessment_pathways do |card|
    json.id card.id
    json.title card.snippet
    json.completion_count card.completion_count
    json.attempted_count card.attempted_by.count
    json.assignment_url card.assignment_url
    json.average_percentage card.percentage_of_correctness
  end
end

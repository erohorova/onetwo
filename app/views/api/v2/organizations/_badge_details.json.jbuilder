json.id badge.id
json.image_url badge.image.url(:small)
json.is_default badge.is_default?

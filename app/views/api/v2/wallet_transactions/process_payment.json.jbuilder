json.message @message
json.transaction_details do
  json.transaction_status @transaction.successful? ? 'Successful' : 'Failed'
  json.authorized (@auth_status || false) 
  json.redirect_url (@redirect_url || nil)
end
if @response
  json.transaction_details do
    json.order @response[:order], :id, :orderable_id, :orderable_type, :state
    json.token @response[:token]
  end
end

json.message @message if @message
json.card_purchased @card_purchased if @card_purchased

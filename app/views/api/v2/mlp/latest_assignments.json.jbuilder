json.items do
  json.array! @assignments do |assignment|
    card = assignment.assignable

    json.extract! assignment, :id, :due_at, :start_date, :started_at, :completed_at, :updated_at
    json.title (card.message != card.title) ? card.title.try(:clean) : ''
    json.progress card.completed_percent(current_user)
    json.card_id  card.id
    json.message card.message
    json.card_type card.card_type
    json.slug card.slug
    json.state assignment.state.upcase
    json.assign_url polymorphic_card_url(assignment.assignable_type, assignment.assignable_id)
    json.image_url get_assign_image(assignment.assignable_type, assignment.assignable_id)
  end
end

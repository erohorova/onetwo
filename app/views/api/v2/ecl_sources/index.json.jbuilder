json.ecl_sources do
  json.array! @ecl_sources do |ecl_source|
    json.partial! 'api/v2/ecl_sources/details', ecl_source: ecl_source
  end
end

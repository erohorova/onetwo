json.extract! quiz_question_option, :id, :label, :is_correct
json.file_resource do
  if quiz_question_option.file_resource
    json.partial! 'api/v2/file_resources/details', file_resource: quiz_question_option.file_resource
  else
    json.null!
  end
end

json.activity_streams do
  json.array! @activity_streams do |activity_stream|
    json.extract! activity_stream, :created_at, :action, :id, :updated_at, :user_id, :organization_id, :comments_count, :votes_count
    json.comments do
      json.array! activity_stream.comments do |comment|
        json.partial! 'api/v2/comments/details', comment: comment
      end
    end
    json.voters do
      json.array! activity_stream.votes do |vote|
        json.id vote.user_id
        json.name vote.voter.try(:full_name)
      end
    end

    json.snippet activity_stream.linkable_item&.snippet
    deep_link_id, deep_link_type = get_app_deep_link_id_and_type(activity_stream.linkable_item)
    json.linkable do
      json.id deep_link_id
      json.type deep_link_type

      if params[:linkable_card_required] && activity_stream.linkable_item.class.name.in?(['Card'])
        json.cards CardBridge.new([activity_stream.linkable_item], user: @user, options: { is_native_app: is_native_app? }).attributes[0]
      end
    end

    json.streamable_id activity_stream.streamable_id
    json.streamable_type activity_stream.streamable_type

    if activity_stream.linkable_item.class.in?([Team, Organization]) || activity_stream.streamable_type == 'Comment'
      json.message do
        json.text activity_stream.streamable.message
        json.mentions do
          json.array! activity_stream.streamable.mentioned_users do |mu|
            json.partial! 'api/v2/users/capsule', user: mu
          end
        end
      end
    end


    json.user do
      json.id activity_stream.user.id
      json.name activity_stream.user.name
      json.avatarimages activity_stream.user.fetch_avatar_urls_v2
      json.handle activity_stream.user.handle
    end
  end
end

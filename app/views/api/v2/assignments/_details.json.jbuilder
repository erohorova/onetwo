json.extract! assignment, :id, :title, :due_at, :start_date, :assignable_id, :assignable_type, :started_at, :completed_at
json.state assignment.state.upcase

deep_link_id, deep_link_type = get_app_deep_link_id_and_type(assignment.assignable)
json.deep_link_id deep_link_id
json.deep_link_type deep_link_type

if include_assignable_details
  json.assignable do
    json.partial! 'api/v2/cards/details', card: assignment.assignable
  end
else
  json.assignable nil
end

json.assign_url polymorphic_card_url(assignment.assignable_type, assignment.assignable_id)
json.image_url get_assign_image(assignment.assignable_type, assignment.assignable_id)

json.teams_count assignment.teams.count #assignment['groups'].try(:count).to_i
json.teams assignment.team_assignments do |team_assignment|
  json.id team_assignment.team_id
  json.name team_assignment.team.try(:name)
  json.message team_assignment.try(:message)
  json.assignment_id team_assignment.id
  json.assignor do
    json.partial! 'api/v2/users/capsule', user: team_assignment.assignor
  end
end

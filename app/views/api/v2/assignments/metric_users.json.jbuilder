json.users @metric_users do |metric_user|
  user = metric_user.assignee
  json.id             user.id
  json.first_name     user.first_name
  json.last_name      user.last_name
  json.name           user.name
  json.email          user.email
  json.assignment_id  metric_user.id
  json.avatarimages   user.fetch_avatar_urls_v2  
end

json.total @total
json.metrics @metrics_hash do |metric|
  json.extract! metric['metric'], :id, :card_id, :assigned_at, :assigned_count, :completed_count, :started_count
  json.team do
    json.id metric['metric'].team_id
    json.name metric['metric'].team_name
  end
  json.due_at metric['due_at']
  json.content_name metric['metric'].card.snippet #cards that doesnt have message but title
  json.content_title metric['metric'].card.title
  json.content_message metric['metric'].card.message
  json.card_type metric['metric'].card.card_type
  json.assignor do
    json.partial! 'api/v2/users/capsule', user: metric['metric'].assignor
  end
end
json.total @total_count

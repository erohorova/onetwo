if is_native_app?
  json.assignments @assignments do |assignment|
    json.partial! 'api/v2/assignments/details', assignment: assignment, include_assignable_details: true
  end
else
  json.assignments do
    json.merge! AssignmentBridge.new(
      @assignments,
      user: @user || current_user,
      fields: @fields,
      options: @options
    ).attributes
  end
end

json.assignment_count @assignments_count
json.mlp_period params[:mlp_period] if params[:mlp_period].present?
json.learning_topics_name params[:learning_topics_name] if params[:learning_topics_name].present?

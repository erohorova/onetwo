json.extract! custom_field, :id, :abbreviation, :display_name, :enable_people_search
json.enable_in_filters custom_field.get_config_enable('enable_in_filters')
json.show_in_profile custom_field.get_config_enable('show_in_profile')
json.editable_by_users custom_field.get_config_enable('editable_by_users')

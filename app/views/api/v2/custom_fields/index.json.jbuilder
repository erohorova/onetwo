json.custom_fields @custom_fields do |custom_field|
  json.partial! 'api/v2/custom_fields/details', custom_field: custom_field
end
json.is_super_admin current_user.is_edcast_dot_com_user?
json.total_count @custom_fields.count

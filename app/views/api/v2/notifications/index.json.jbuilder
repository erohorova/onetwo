json.unseen_notifications_count @unseen_count
json.notifications do
  json.array! @notifications do |notification|
    json.extract! notification, :id, :unseen, :created_at
    json.message strip_tags(notification.message(@preloaded_snippets))
    deep_link_id, deep_link_type = notification_app_deep_link_id_and_type(notification)

    if notification.notification_type == 'assigned'
      json.due_date notification.notifiable.try(:due_at)
      json.start_date notification.notifiable.try(:start_date)
    end

    if [Notify::NOTE_GROUP_USER_PROMOTED_TO_SUB_ADMIN,
        Notify::NOTE_GROUP_USER_PROMOTED_TO_ADMIN].include?(notification.notification_type)
      json.group_user_promoted true
    end

    json.deep_link_id deep_link_id
    json.deep_link_type deep_link_type
    # return card_id to proper navigation into child cards in
    # pathways and journeys
    json.card_id notification.card_id unless deep_link_type == 'card'
    json.users do
      json.array! notification.notifications_causal_users do |ncu|
        json.partial! 'api/v2/users/capsule', user: ncu.user
        json.is_following @following.try(:[], 'User').try(:[], ncu.user.id) || false
      end
    end
  end
end

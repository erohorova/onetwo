json.tags do
  json.array! @tags do |tag|
    json.name tag
  end
end
json.partial! 'api/v2/search/search_result'
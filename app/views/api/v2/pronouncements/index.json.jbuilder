json.pronouncements do
  json.array! @pronouncements do |pronouncement|
    json.partial! 'api/v2/pronouncements/details', pronouncement: pronouncement
  end
end
json.total @total

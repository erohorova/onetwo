json.project_submission_reviews_total_count reviews_count
json.project_submission_reviews do
  json.array! reviews do |submission_review|
    json.extract! submission_review, *submission_review.attributes.keys
  end
end

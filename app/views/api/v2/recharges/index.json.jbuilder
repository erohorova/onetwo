json.recharges do
  json.array! @recharges do |recharge|
    json.extract! recharge, *recharge.attributes.keys
    json.prices do
      json.array! recharge.prices do |price|
        json.id price.id
        json.amount price.amount
        json.currency price.currency
        json.symbol CURRENCY_TO_SYMBOL[price.currency] || "$"
      end
    end
  end
end

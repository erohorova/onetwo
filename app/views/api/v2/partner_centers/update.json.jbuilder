json.partner_center do
  json.partial! 'api/v2/partner_centers/partner_center', partner_center: @partner_center
end

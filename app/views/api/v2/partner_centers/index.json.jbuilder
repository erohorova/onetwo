json.partner_centers do
  json.array! @partner_centers do |partner_center|
    json.partial! 'api/v2/partner_centers/partner_center', partner_center: partner_center
  end
end

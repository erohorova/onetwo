json.project_submissions_total_count @submissions_count
json.project_submissions do
  json.array! @submissions do |project_submission|
    json.extract! project_submission, *project_submission.attributes.keys
    json.partial! 'api/v2/project_submission_reviews/details',
                  reviews: project_submission.project_submission_reviews,
                  reviews_count: project_submission.project_submission_reviews.count
    json.user do
      user = project_submission.submitter
      json.first_name user.first_name.present? ? user.first_name : user.name
      json.coverimages user.fetch_coverimage_urls_v2
    end
  end
end

json.extract! learning_queue_item, :id, :display_type, :deep_link_id, :deep_link_type, :source, :updated_at
json.snippet learning_queue_item.snippet

if include_queueable_details && queueable
  json.queueable do
    json.partial! 'api/v2/cards/details', card: queueable
    json.assignment do
      if assignment
        json.partial! 'api/v2/assignments/details', assignment: assignment, include_assignable_details: false
      else
        nil
      end
    end
  end
  if learning_queue_item.source == 'bookmarks'
    json.bookmark_id bookmarkable[queueable.id.to_i]
  end
else
  json.queueable nil
end

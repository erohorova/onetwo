json.learning_queue_items do
  if is_native_app?
    json.array! @learning_queue_items do |learning_queue_item|
      json.partial! 'api/v2/learning_queue_items/details',
        learning_queue_item: learning_queue_item,
        queueable: @queueables.try(:[], learning_queue_item.queueable_id),
        include_queueable_details: @include_queueable_details,
        assignment: @assignments.try(:[], learning_queue_item.queueable_id),
        bookmarkable: @bookmarkable || {}
    end
  else
    json.merge! LearningQueueItemBridge.new(
      @learning_queue_items,
      user: current_user,
      fields: params[:fields]
    ).attributes
  end
end

json.pinnable_card_id @pinnable_card_id

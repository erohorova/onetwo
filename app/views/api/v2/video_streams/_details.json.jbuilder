json.extract! video_stream, :id, :uuid, :status, :start_time, :width, :height
json.image_url video_stream.image
json.playback_url video_stream.playback_url

if video_stream.is_a?(IrisVideoStream)
  json.embed_playback_url video_stream.embed_playback_url
end

if video_stream.past_stream_available?
  json.recording do
    json.partial! 'api/v2/recordings/details', recording: video_stream.default_recording
  end
end

if video_stream.past_stream_available?
  json.watchers video_stream.watchers.count
end
json.recordings do
  json.array! @recordings do |recording|
    json.partial! 'api/v2/recordings/details', recording: recording
  end
end

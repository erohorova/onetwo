json.users do
  json.array! @viewers do |user|
    json.partial! 'api/v2/users/capsule', user: user
    json.is_following @following.try(:[], 'User').try(:[], user.id) || false
  end
end
json.total @total
# Currently, only 'Card' is objectable.
# TO DO: As and when we add new objectables, this json will have to be scaled.
json.pins do
  if is_native_app?
    json.array! @pin_objects do |card|
      json.partial! 'api/v2/cards/details', card: card
    end
  else
    json.merge! @pin_objects.present? ? CardBridge.new(@pin_objects, user: current_user, options: { is_native_app: is_native_app? }).attributes : []
  end
end

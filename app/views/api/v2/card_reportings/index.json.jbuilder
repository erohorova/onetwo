json.content @flagged do |flag_card|
  json.card_id flag_card.id
  json.share_url ClientOnlyRoutes.show_insight_url_with_org(
    @current_org,
    id: (flag_card.id.nil? ? Card.card_ecl_id(flag_card.ecl_id) : flag_card.id.to_s)
  )
  json.title flag_card.display_title(27)
  json.ecl_source_type_name flag_card&.ecl_metadata && flag_card&.ecl_metadata["source_type_name"]
  json.card_type flag_card&.card_type
  json.created_by flag_card&.author_name
  json.created_at flag_card&.card_created_at
  json.content_reporter flag_card , partial: 'api/v2/card_reportings/details', as: :reporter
  json.total_reporter_count flag_card&.card_reporting_count
end
json.total @total

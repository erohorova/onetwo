json.content @results do |result|
  json.card_id result.id
  json.share_url ClientOnlyRoutes.show_insight_url_with_org(
    @current_org,
    id: (result.id.nil? ? Card.card_ecl_id(result.id) : result.id.to_s)
  )
  json.title result.card_message
  json.card_type result.card_type
  json.ecl_source_type_name result.card_source
  json.created_by @users[result.card_author_id]
  json.created_at result.card_created_at
  json.content_reporter do
    json.reported_by @users[result.reportings.first['user_id']]
    json.reported_at result.reportings.first['created_at']
    json.reason result.reportings.first['reason']
  end
  json.total_reporter_count result.reportings.length
end
json.total @total

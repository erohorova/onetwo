# IF: This is constructed for mobile, Pathway, Journey.
# ELSE: This is constructed when we visit Standalone page (Card). Handled using `is_standalone_page` in the query parameter.
if is_native_app? || @card.pack_card? || @card.journey_card? || !params[:is_standalone_page]&.to_bool
  json.partial! 'api/v2/cards/details', card: @card, assignment: @assignment

  json.teams_permitted do
    json.array! @card.teams_with_permission.select(:id, :name) do |team|
      json.id team.id
      json.name team.name
      json.channel_ids team.following_channel_ids
    end
  end

  json.users_permitted do
    json.array! @card.users_with_permission.select(:id, :email, :first_name, :last_name) do |user|
      json.id user.id
      json.name user.name
      json.channel_ids user.direct_following_channel_ids
      json.team_ids user.team_ids
    end
  end

  json.show_restrict !(@card.assignments.any? || @card.bookmarks.any? || @card.user_content_completions.any?)
else
  json.merge! CardBridge.new([@card], user: current_user, options: { is_native_app: is_native_app? }).attributes[0]

  json.merge! CardBridge.new(
    [@card],
    user: current_user,
    fields: "ecl_id,auto_complete,duration,views_count,completed_percentage,assignment,channel_ids,users_with_access,voters,non_curated_channel_ids,file_resources,channels(id,label,is_private),teams(id,label),tags(id,name),tags,channels,teams,paid_by_user,payment_enabled,due_at,assigner,shared_by",
    options: { is_native_app: is_native_app? }
  ).attributes[0]
end

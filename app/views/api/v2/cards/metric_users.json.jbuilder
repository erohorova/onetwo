json.array! @users do |user|
  json.partial! 'api/v2/users/capsule', user: user
  json.roles user.get_user_roles
  json.roles_default_names user.user_roles_default_names
end

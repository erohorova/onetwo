json.cards do
  # For moblie and for edc-cms in pack and journey will need old response
  if is_native_app? || (params[:full_response] && (params[:card_type] == ['journey'] || params[:card_type] == ['pack']))
    json.array! @cards do |card|
      json.partial! 'api/v2/cards/details', card: card, assignment: @assignments.try(:[], card.id)
    end
  else
    # For edc-cms, we able to find this admin request from parameter params[:is_cms]
    if params[:is_cms] && params[:card_type] == ['media', 'course', 'poll'] && params[:fields].blank?
    # This gives minimal respose for smartcards
      fields = 'id,card_type,card_subtype,hidden,channels(id,label),tags(id,name),created_at,ecl_duration_metadata,message,title,state,slug,channels,author,tags,filestack,resource,is_paid'
      json.merge! CardBridge.new(
        @cards,
        user: current_user,
        fields: fields,
        options: { is_native_app: is_native_app? }
      ).attributes
    else
    # This gives minimal response in edc-web
      json.merge! CardBridge.new(
        @cards,
        user: current_user,
        fields: params[:fields],
        options: @options
      ).attributes
    end
  end
end
json.total @total

# Bridges are not used since there are more queries to be run.
# Extracting necessary fields right here.

json.assignable do
  json.teams do
    json.array! @teams do |team|
      json.id team.id
      json.name team.name
      json.assigned @assigned_teams.include?(team.id)
      json.image do
        json.small team.image.url(:small)
        json.medium team.image.url(:medium)
        json.large team.image.url(:large)
      end
    end
  end

  json.users do
    json.array! @users do |user|
      json.id user.id
      json.name user.name
      json.assigned @assigned_users.include?(user.id)
      json.image user.fetch_avatar_urls_v2
    end
  end
end
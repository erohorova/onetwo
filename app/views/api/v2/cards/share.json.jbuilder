json.card do
  json.partial! 'api/v2/cards/details', card: @card
end

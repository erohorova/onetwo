# INTENTION:
#   1. Returns everything if request originated from mobile apps.
#   2. Returns minimal response if search triggered from web.
#   3. Returns everything from other web-originated requests.
is_native_app = is_native_app?
minimal_view = !is_native_app && (defined?(mini_view) ? mini_view : false)

associated_group = card.resource_group

chosen_assignments = @chosen_assignments || {}
card_subscriptions = @card_subscriptions || {}
json.id (card.id.nil? ? Card.card_ecl_id(card.ecl_id) : card.id.to_s) # Using a string ID in anticipation of virtual cards compound ID implementation
json.extract! card, *%i{
  card_type
  card_subtype
  slug
  state
  is_official
  is_public
  created_at
  updated_at
  published_at
  hidden
  ecl_id
  provider
  comments_count
  votes_count
}
json.provider_image is_native_app ? card.provider_image : secured_url(card.provider_image, current_user.id, @current_org) if card.provider_image
json.can_be_reanswered !!card.can_be_reanswered if card.poll_card?
json.is_clone card.super_card_id.present? if @current_org.share_channels_enabled?
json.readable_card_type card.card_type_display_name
json.title (card.message != card.title) ? card.title.try(:clean) : ""
json.due_at chosen_assignments[card.id]&.due_at if chosen_assignments[card.id]&.due_at.present?
json.subscription_end_date card_subscriptions[card.id]&.end_date if card_subscriptions[card.id]&.end_date.present?
json.message card.sanitize_message
json.language card.language

unless minimal_view
  json.views_count card.all_time_views_count
end

json.share_url ClientOnlyRoutes.show_insight_url_with_org(
  @current_org,
  id: (card.id.nil? ? Card.card_ecl_id(card.ecl_id) : card.id.to_s)
)

unless minimal_view
  json.completed_percentage card.completed_percent(current_user)
end

json.project_id card.project.id if card.card_type == "project"
json.mark_feature_disabled_for_source card.mark_feature_disabled?

if card.ecl_metadata.present?
  json.ecl_source_display_name card.ecl_metadata["source_display_name"]
  json.ecl_source_type_name card.ecl_metadata["source_type_name"]
  json.ecl_source_logo_url card.ecl_metadata["source_logo_url"]
  json.ecl_source_cpe_credits card.ecl_metadata["cpe_credits"]
  json.ecl_source_cpe_sub_area card.ecl_metadata["cpe_subject"]
end

json.ecl_duration_metadata formatted_duration_metadata(card)

unless minimal_view
  json.voters do
    json.array! card.liked_by do |voter|
      json.id voter.id
      json.name voter.full_name
      json.handle voter.handle
      json.avatarimages voter.fetch_avatar_urls_v2
    end
  end
  json.channel_ids card.curated_channel_ids
  json.non_curated_channel_ids card.non_curated_channel_ids
  json.user_rating card.cards_ratings.find_by(user_id: current_user.id)&.rating
  json.all_ratings card.cards_ratings.group("rating").count
end

json.average_rating card.card_metadatum&.average_rating

# Showing global value of BIA of the card.
# NOTE:
# 1. User level update of BIA from top-right corner is regulated via flag 'allow-consumer-modify-level'.
# 2. This flag is turned off. Since end-user(who is neither author nor admin) cannot update the BIA, showing only the global value of BIA of the card. See EP-14189.
# 3. For existing cards if there is no card_metadata then showing author's BIA.
json.skill_level (card.card_metadatum&.level || card.cards_ratings.find_by(user_id: card.author_id)&.level)

json.is_upvoted (current_user && card.voted_by?(current_user))
json.auto_complete card.auto_complete
json.is_bookmarked (current_user && card.bookmarked_by?(current_user))

if @current_org.reporting_content_enabled?
  json.is_reported current_user && card.reported_by?(current_user)
end

json.is_assigned current_user.has_an_assignment?(card)

unless minimal_view
  json.completion_state (current_user && current_user.completion_state_for('Card', card['id']).try(&:upcase))
end

json.filestack card_filestack(card, current_user.id, @current_org, is_native_app)

json.resource do
  json.partial! 'api/v2/resources/details', resource: card.resource , object_type: card  unless card.resource.blank?
end

unless minimal_view
  json.mentions do
    json.array! card.mentions do |mention|
      json.partial! 'api/v2/users/capsule', user: mention.user unless mention.user.blank?
    end
  end
end

json.author do
  json.partial! 'api/v2/users/capsule', user: card.author unless card.author.blank?
  json.job_title card.author_profile.job_title if(card.author.present? && card.author_profile.present?)
end

unless minimal_view
  json.tags do
    json.array! card.tags do |tag|
      json.partial! 'api/v2/tags/details', tag: tag
    end
  end
end

json.file_resources do
  json.array! card.file_resources do |file_resource|
    json.partial! 'api/v2/file_resources/details', file_resource: file_resource  unless file_resource.blank?
  end
end

unless minimal_view
  json.channels do
    json.array! card.curated_channels do |channel|
      json.id channel.id
      json.label channel.label
      json.image_url channel.mobile_photo || channel.photo
      json.is_private channel.is_private
    end
  end
end

unless minimal_view
  json.teams do
    json.array! card.shared_with_teams do |team|
      json.id team.id
      json.label team.name
    end
  end
end

unless minimal_view
  json.users_with_access do
    json.array! card.users_with_access.not_anonymized do |user|
      json.id user.id
      json.full_name user.full_name
      json.email user.email
      json.handle user.handle
      json.avatarimages user.fetch_avatar_urls_v2
    end
  end
end

unless minimal_view
  # pack
  if card.card_type == "pack" && card.card_subtype.in?(%w(simple curriculum))
    json.pack_cards card.pack_cards_index_data(user: current_user)
  end

  #journey
  if card.card_type == "journey"
    json.journey_section card.journey_cards_index_data current_user
  end
end

# poll
if card.card_type == "poll"
  json.quiz_question_options do
    json.array! card.quiz_question_options do |qqo|
      json.partial! 'api/v2/quiz_question_options/details', quiz_question_option: qqo
      json.count card.quiz_question_stats.try(:options_stats).try(:[], qqo['id'].to_s) || 0
      json.is_correct qqo.is_correct
    end
  end
  json.is_assessment card.is_a_poll_question?
  json.attempt_count card.quiz_question_stats.try(:attempt_count)
  json.has_attempted current_user && QuizQuestionAttempt.has_user_attempted_quiz_question_card?(card.id, current_user.id)
  json.attempted_option QuizQuestionAttempt.attempted_option(card_id: card.id, user: current_user)
end

# video_stream
json.video_stream do
  json.partial! 'api/v2/video_streams/details', video_stream: card.video_stream, user: current_user if card.video_stream.present?
end

json.resource_group do
  json.partial! 'api/v2/groups/details', group: associated_group, user: current_user if associated_group
end

unless minimal_view
  if defined?(assignment)
    json.assignment do
      if assignment
        #include assignable details should be false. otherwise it might end up rendering card partial recursively
         json.partial! 'api/v2/assignments/details', assignment: assignment, include_assignable_details: false
      else
        nil
      end
    end
  end
end

card_badging = card.card_badging
json.badging do
  if card_badging.present?
    json.id card_badging.id
    json.title card_badging.title
    json.image_url card_badging.card_badge_image.url(:large)
    json.badge_id card_badging.badge_id
  end
end

json.user_taxonomy_topics card.user_taxonomy_topics || []

unless minimal_view
  json.leaps do
    json.standalone do
      json.partial! 'api/v2/cards/leaps', leap: card.standalone_leap
    end if card.standalone_leap

    json.in_pathways do
      json.array! card.pathway_leaps do |l|
        json.partial! 'api/v2/cards/leaps', leap: l
      end if card.pathway_leaps.any?
    end
  end if card.leaps.any?
end

json.paid_by_user (card.is_paid_card && card.paid_by_user?(current_user))
json.is_paid card.is_paid_card
json.payment_enabled card.lxp_payment_enabled?

if card.card_metadatum
  json.card_metadatum do
    json.id card.card_metadatum.id
    json.plan card.card_metadatum&.plan
  end
end
json.prices do
  json.array! card.prices do |price|
    json.id price.id
    json.amount price.amount&.round
    json.currency price.currency
    json.symbol CURRENCY_TO_SYMBOL[price.currency] || "$"
  end
end

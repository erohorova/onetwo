json.card do
  json.partial! 'api/v2/cards/details', card: @card
end

json.errors do
  json.pathways do
    json.array! @errors do |error|
      json.partial! 'api/v2/cards/details', card: error
    end
  end
  json.total @errors.count
end

json.successes do
  json.pathways do
    json.array! @successes do |success|
      json.partial! 'api/v2/cards/details', card: success
    end
  end
  json.total @successes.count
end
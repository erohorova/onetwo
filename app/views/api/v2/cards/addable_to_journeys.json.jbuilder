json.cards @cards do |card|
  json.id card.id
  json.message card.title
  json.updated_at card.updated_at
end
json.total @total
json.cards @cards do |card|
  json.id card.id
  json.message card.title
  json.journey_sections card.journey_pack_relations do |jpr|
    if (jpr_card = jpr.card).present?
      json.card_id jpr_card.id
      json.section_title jpr_card.message
    end
  end
  json.created_at card.created_at
  json.updated_at card.updated_at
end
json.total @total

json.card_reportings @card_reportings do | reported|
  json.reported_by reported.user.name
  json.reported_at reported&.created_at
  json.reason reported.reason
end

json.partial! 'api/v2/cards/details', card: @card
json.not_required_curation @not_required
json.required_curation @required

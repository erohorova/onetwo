json.cards do
  json.array! @cards do |card|
    if is_native_app?
      json.partial! 'api/v2/cards/details', card: card
    else
      json.id card.id
      json.title card.title
      json.message card.message
      json.author do |author|
        json.name card.author.name
        json.handle card.author.handle
      end
      json.updated_at card.updated_at
    end
  end
end

json.total @total

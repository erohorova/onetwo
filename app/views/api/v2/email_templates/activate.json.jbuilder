json.activated json.partial! 'api/v2/email_templates/details', template: @email_template

json.disabled do
  json.array! @info[:disabled] do |et|
    json.partial! 'api/v2/email_templates/details', template: et
  end
end

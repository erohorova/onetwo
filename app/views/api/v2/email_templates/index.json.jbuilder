json.email_templates do
  json.array! @email_templates do |et|
    json.partial! 'api/v2/email_templates/details', template: et
  end
end
json.total @total

json.extract! template, :id, :title, :default_id, :creator_id, :language,
  :organization_id, :is_active, :state, :design, :created_at, :updated_at
json.template_variables EmailTemplate.get_dynamic_variables(template.id)
json.default_template_title EmailTemplate.get_proper_title(template)
json.active_pair_title template.active_pair&.title if template.default_id
json.readable_default_title EmailTemplate.get_readable_title(template)
json.subject EmailTemplate.get_email_subject(template)

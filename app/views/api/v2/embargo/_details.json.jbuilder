json.extract! embargo, *%i{id value category user_id organization_id created_at updated_at}
json.added_by embargo.user&.full_name

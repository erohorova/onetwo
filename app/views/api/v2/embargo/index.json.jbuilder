json.embargoes do
  json.array! @embargoes do |embargo|
    json.partial! 'api/v2/embargo/details', embargo: embargo
  end
end
json.total @total

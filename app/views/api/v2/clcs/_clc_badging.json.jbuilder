json.badgings do
  json.array! badgings do |badge|
    json.partial! 'api/v2/clcs/badge', badge: badge
  end
end

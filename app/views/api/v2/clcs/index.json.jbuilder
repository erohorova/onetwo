json.clcs do
  json.array! @clcs do |clc|
    json.partial! 'api/v2/clcs/progress', clc: clc
    json.partial! 'api/v2/clcs/clc_badging', badgings: clc.clc_badgings if clc.clc_badgings.present?
  end
end
json.total @clcs_count
json.clc_id clc.id
json.name clc.name
json.target_score clc.target_score
json.from_date clc.from_date.strftime("%m/%d/%Y")
json.to_date clc.to_date.strftime("%m/%d/%Y")
json.status (clc.deleted_at.present? ? false : clc.active?)
json.score ((@clc_progress.present?) ? @clc_progress.clc_score : 0)

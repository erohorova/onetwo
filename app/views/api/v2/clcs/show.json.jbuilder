json.partial! 'api/v2/clcs/progress', clc: @clc
earned_badge = get_earned_badge(@clc,@user)
json.earned_badge do
  json.partial! 'api/v2/clcs/badge', badge: earned_badge.clc_badging if earned_badge.present?
end
json.partial! 'api/v2/clcs/clc_badging', badgings: @clc.clc_badgings if @clc.clc_badgings.present?

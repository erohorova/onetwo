json.id badge.id
json.title badge.title
json.image_url badge.clc_badge_image&.url(:large)
json.badge_id badge.badge_id
json.target_steps badge.target_steps

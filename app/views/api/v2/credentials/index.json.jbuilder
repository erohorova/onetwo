json.credentials do
  json.array! @credentials do |credential|
    json.partial! 'api/v2/credentials/details', credential: credential
  end
end
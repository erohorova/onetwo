json.array! @configs do |config|
  json.name config.name
  json.value config.parsed_value
end

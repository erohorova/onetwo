json.external_courses @external_courses do |course|
  json.id course.id
  json.name course.name
  json.description course.description
  json.image_url course.image_url
  json.course_url course.course_url
  json.status course.status
  json.integration_name course.integration.name
  json.integration_logo_url course.integration.logo(:medium)
end

json.internal_courses @internal_courses do |course|
  json.id course.id
  json.name course.name
  json.description course.description
  json.image_url course.image || image_url("card_default_image_min.jpg")
  json.course_url link_course_path(id: course.id)
  json.status (course.end_date.nil? || course.end_date > Date.current) ? "Enrolled" : "Completed"
  json.duration duration(course)
end

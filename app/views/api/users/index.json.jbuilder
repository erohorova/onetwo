json.users @users do |user|
  json.id user.id
  json.state 'active'
  json.role user.organization_role
  json.email user.email
  json.created_at user.created_at
  json.partial! 'users/capsule', user: user
  json.following current_user.following_user_id?(user.id)
  if @is_admin_user
    json.current_sign_in_at user.current_sign_in_at
  end
end

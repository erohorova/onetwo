json.array! @integrations do |integration|
  json.id integration.id
  json.name integration.name
  json.description integration.description
  json.logo_url integration.logo(:small)
  json.callback_url integration.callback_url
  users_integration = @user.integration_connected(integration.id)
  json.connected users_integration && users_integration.connected
  json.fields @user.fields_for(integration)
end

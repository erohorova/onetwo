json.items do
  json.array! @metrics do |metric|
    json.item do
      json.id metric.content_id
      json.title metric.content_title
      json.is_deleted metric.is_content_object_deleted?
      json.type metric.content_type
      json.url url_for_user_content_level_metric(metric)
      json.ui_layout_type content_nested_type(metric)
      json.owned content_owned_by_user?(metric)
    end
    json.score metric.smartbites_score
  end
end

json.offset @offset
json.limit @limit
json.query params[:q]
json.status @status
json.total_cards @total_cards
json.cards do
  json.total @total_cards
  cards_array = @cards
  json.array! cards_array do |card|
    user_action = @user_actions[card['id'].to_i]
    json.partial! 'cards/details', card: card, user: @user, user_role: 'student', user_action: user_action
    json.comments do
      json.array! @comments_hash[card['id'].to_i] do |comment|
        json.partial! 'comments/mobile_capsule', comment: comment
        json.is_upvoted @votes_by_user.include?(comment.id)
      end
    end
  end
end
if (@users)
  json.total_users @total_users
  json.users do
    json.array! @users do |user|
      json.partial! 'users/details', user: OpenStruct.new(user), anonymous: nil
      json.following current_user_following_user_id?(user['id'])
    end
  end
end

if (@video_streams)
  json.total_video_streams @total_video_streams
  json.video_streams do
    json.array! @video_streams do |video_stream|
      json.partial! 'api/video_streams/details', video_stream: video_stream, user: current_user
    end
  end
end

if @channels
  json.total_channels @total_channels
  json.channels do
    json.array! @channels do |channel|
      json.partial! 'api/channels/details', channel: channel
      json.following @following[channel.id]
    end
  end
end
json.schools @schools do |school|
  json.id school.id
  json.name school.name
  json.specialization school.specialization
  json.from_year school.from_year
  json.to_year school.to_year
  json.user_id school.user_id
end

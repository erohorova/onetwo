json.array! @promoted_feeds do |section|
  json.type section['type']
  json.items do
    json.array! section['items'] do |item|
      if section['type'] == 'Card'
        json.partial! 'cards/details', card: item, user: current_user, user_action: @user_actions[item['id'].to_i]
      elsif section['type'] == 'VideoStream'
        json.partial! 'api/video_streams/details', video_stream: item, user: current_user
      end
    end
  end
end

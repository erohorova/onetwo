json.cards do
  json.array! @cards do |card|
    user_action = @user_actions[card['id'].to_i]
    json.partial! 'cards/details', card: card, user: @user, user_role: 'student', user_action: user_action
    json.comments do
      json.array! @comments_hash[card['id'].to_i] do |comment|
        json.partial! 'comments/mobile_capsule', comment: comment
        json.is_upvoted @votes_by_user.include?(comment.id)
      end
    end
  end
end
if @webex_config.present?
  json.name @webex_config.name
  json.site_name @webex_config.site_name
  json.site_id @webex_config.site_id
  json.partner_id @webex_config.partner_id
end

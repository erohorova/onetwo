json.id @xapi_credential.try(:id)
json.end_point @xapi_credential.try(:end_point)
json.lrs_login_key @xapi_credential.try(:lrs_login_key)
json.lrs_api_version @xapi_credential.try(:lrs_api_version)
json.xapi_enabled @xapi_credential.try(:organization).try(:xapi_enabled) || false

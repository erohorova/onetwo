json.array! @tag_suggestions do |tag|
  json.id tag
  json.label "#{tag}"
  json.value "#{tag}"
end
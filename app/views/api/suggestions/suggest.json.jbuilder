unless @exclude_users
  json.users do
    json.total @results[:user_suggestions].try(:total) || 0
    json.results do
      results = @results[:user_suggestions].try(:results) || []
      json.array! results do |u|
        json.id u['id']
        json.partial! 'users/capsule', user: OpenStruct.new(u)
      end
    end
  end
end
unless @exclude_tags
  json.tags do
    json.array! @results[:tags] do |tag|
      json.name "##{tag}"
    end
  end
end

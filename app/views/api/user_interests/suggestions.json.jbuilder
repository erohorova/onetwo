json.users do
  json.array! @suggested_users do |user|
    json.id user.id
    json.partial! 'users/capsule', user: user
  end
end
json.channels do
  json.array! @suggested_channels do |channel|
    # Can add followers_count and insights_count here if needed
    json.partial! 'api/channels/details', channel: channel
  end
end

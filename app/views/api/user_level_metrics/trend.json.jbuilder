json.metrics do
  json.smartbites_score @offsets.map{|offset| @metrics_by_offset[offset].try(:smartbites_score) || 0}
  json.smartbites_created @offsets.map{|offset| @metrics_by_offset[offset].try(:smartbites_created) || 0}
  json.smartbites_consumed @offsets.map{|offset| @metrics_by_offset[offset].try(:smartbites_consumed) || 0}
  json.time_spent @offsets.map{|offset| @metrics_by_offset[offset].try(:time_spent) || 0}
  json.percentile @offsets.map{|offset| @metrics_by_offset[offset].try(:percentile) || @inactive_percentiles_by_offset[offset]}
end

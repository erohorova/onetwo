json.followings_count current_user.following_count
json.followers_count current_user.followers_count
json.smartbites_score @metric.try(:smartbites_score) || 0
json.smartbites_created @metric.try(:smartbites_created) || 0
json.smartbites_consumed @metric.try(:smartbites_consumed) || 0
json.time_spent @metric.try(:time_spent) || 0
json.percentile @percentile
json.topics do
  json.array! @topic_level_metrics do |topic_level_metric|
    json.id topic_level_metric.tag_id
    json.name topic_level_metric.tag_name
    json.score topic_level_metric.smartbites_score
  end
end

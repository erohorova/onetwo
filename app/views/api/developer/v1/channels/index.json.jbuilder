json.channels do
  json.array! @channels do |channel|
    json.partial! 'api/developer/v1/channels/details', channel: channel
  end
end
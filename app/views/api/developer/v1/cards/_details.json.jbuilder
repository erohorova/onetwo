json.id (card.id.nil? ? Card.card_ecl_id(card.ecl_id) : card.id.to_s) # Using a string ID in anticipation of virtual cards compound ID implementation
json.extract! card, :title, :message, :card_type, :card_subtype, :slug, :state, :created_at, :updated_at, :published_at
json.comments_count card.comments.count
json.votes_count card.votes_count
json.channel_ids card.channel_ids
json.resource_url card.resource.url unless card.resource.blank?

if params[:load_topics].try(:to_bool)
  json.topics do
    json.array! card.tags do |tag|
      json.name tag.name
    end
  end
end

json.resource do
  json.partial! 'api/v2/resources/details', resource: card.resource, object_type: card unless card.resource.blank?
end

json.author do
  json.partial! 'api/developer/v1/users/capsule', user: card.author unless card.author.blank?
end

json.file_resources do
  json.array! card.file_resources do |file_resource|
    json.partial! 'api/v2/file_resources/details', file_resource: file_resource  unless file_resource.blank?
  end
end

# pack
if card.card_type == "pack"
  json.pack_cards card.pack_cards_index_data
end

# poll
if card.card_type == "poll"
  json.quiz_question_options do
    json.array! card.quiz_question_options do |qqo|
      json.partial! 'api/v2/quiz_question_options/details', quiz_question_option: qqo
      json.count card.quiz_question_stats.try(:options_stats).try(:[], qqo['id'].to_s) || 0
    end
  end
  json.attempt_count card.quiz_question_stats.try(:attempt_count)
  json.has_attempted current_user && QuizQuestionAttempt.has_user_attempted_quiz_question_card?(card.id, current_user.id)
end

# video_stream
json.video_stream do
  json.partial! 'api/v2/video_streams/details', video_stream: card.video_stream, user: current_user if card.video_stream.present?
end

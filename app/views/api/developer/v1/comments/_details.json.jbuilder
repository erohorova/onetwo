json.extract! comment, :id, :message, :votes_count, :comments_count, :commentable_id, :commentable_type, :created_at, :updated_at
json.user do
  json.partial! 'api/v2/users/capsule', user: comment.user, anonymous: comment.anonymous
end
json.voted comment.voted_by?(current_user)

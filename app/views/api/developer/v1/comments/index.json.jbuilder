json.comments do
  json.array! @comments do |comment|
    json.partial! 'api/developer/v1/comments/details', comment: comment
  end
end

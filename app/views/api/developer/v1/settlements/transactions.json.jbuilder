json.transactions do
  json.array! @transactions do |transaction|
    json.date transaction.updated_at
    json.amount transaction.amount
    json.currency transaction.currency
    json.title (transaction&.order&.orderable&.display_title(100))
    json.id transaction.id
    json.source_name transaction.source_name
  end
end
json.total_count @total_count

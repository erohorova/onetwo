json.extract! user, :id, :handle, :email, :name, :bio
json.profile_image user.photo(:medium) || user.picture_url
json.user do
  json.partial! 'api/developer/v1/users/capsule', user: @current_user
  if current_user.profile
    json.extract! current_user.profile, :expert_topics, :learning_topics
  end
  json.jwt_token @jwt_token
end
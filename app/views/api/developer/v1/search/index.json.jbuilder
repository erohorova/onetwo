json.cards do
  json.array! @cards do |card|
    json.partial! 'api/developer/v1/cards/details', card: card
  end
end
json.aggregations @aggs

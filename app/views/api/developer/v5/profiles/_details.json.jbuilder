json.extract! profile, *%i{
  external_id
  uid
}
json.email profile.email
json.first_name profile.first_name
json.last_name profile.last_name
json.status profile.status
json.roles profile.user.roles.pluck(:name)
json.language profile.user.language
json.avatar profile.user.fetch_avatar_urls_v2
json.job_title profile.user.job_title

profile.assigned_custom_fields.each do |custom_field|
  json.set! custom_field.custom_field.abbreviation, custom_field.value
end

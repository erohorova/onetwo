json.custom_fields @custom_fields do |custom_field|
  json.partial! 'api/v2/custom_fields/details', custom_field: custom_field
end
json.total_count @custom_fields.count

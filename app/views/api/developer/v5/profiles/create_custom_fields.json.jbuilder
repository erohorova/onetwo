json.custom_fields do
  json.array! @created_custom_fields do |custom_field|
    json.partial! 'api/v2/custom_fields/details', custom_field: custom_field
  end
end

json.messages @messages if @messages.present?

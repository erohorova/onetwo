json.profiles do
  json.array! @profiles do |profile|
    json.partial! 'api/developer/v5/profiles/details', profile: profile
  end
end
json.total @total

json.profiles do
  json.array! @created_profiles do |profile|
    json.partial! 'api/developer/v5/profiles/details', profile: profile
  end
end
json.messages @all_messages
json.total_created_profiles @created_profiles.count

json.users do
  json.array! (@knowledge_map['users'] || []) do |item|
    json.id item['id']
    json.name item['name']
    json.smartbites_score item['smartbites_score']
  end
end
json.topics do
  json.array! (@knowledge_map['topics'] || []) do |item|
    json.id item['id']
    json.name item['name']
    json.smartbites_score item['smartbites_score']
    json.users_count item['users_count']
  end
end

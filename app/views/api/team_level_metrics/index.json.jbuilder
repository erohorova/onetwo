json.teams do
  json.array! @metrics do |metric|
    json.smartbites_consumed metric.smartbites_consumed
    json.smartbites_created metric.smartbites_created
    json.time_spent metric.time_spent
    json.smartbites_comments_count metric.smartbites_comments_count
    json.smartbites_liked metric.smartbites_liked
    json.team do
      team = @teams[metric.team_id]
      json.id team.id
      json.name team.name
      json.members_count team.active_users.count
    end
  end
end


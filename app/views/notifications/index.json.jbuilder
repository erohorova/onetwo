json.unseen_notifications_count @unseen_count
json.notifications do
  json.array! @notifications do |notification|
    json.id notification.id
    json.message strip_tags(notification.message(@preloaded_snippets))
    json.notification_text notification_message(notification)
    json.notification_image_url notification_image(notification)[0]
    json.action action_link(notification)

    json.notifiable_id notification.notification_card_id

    if @widget_request
      json.widget_deep_link_id notification.widget_deep_link_id
      json.widget_deep_link_type notification.widget_deep_link_type
    elsif @native_app
      # fallback to card_id, 'card' for things like video stream notifications
      json.deep_link_id notification.app_deep_link_id || notification.card_id
      json.deep_link_type notification.app_deep_link_type || 'card'
      if notification.app_deep_link_type == 'post'
        post = @posts_map[notification.app_deep_link_id]
        if post
          json.post do
            json.partial! 'posts/details', post: post
            json.up_voted @votes_by_user.include?(post.id.to_i)
          end
        else
          json.post nil
        end
      elsif notification.app_deep_link_type == 'video_stream'
        video_stream = @video_streams_map[notification.app_deep_link_id]
        if video_stream
          json.video_stream do
            json.partial! 'api/video_streams/details', video_stream: video_stream, user: @user
          end
        end
      end
      json.card_id notification.card_id
      card = @cards_map[notification.card_id]
      if card
        json.card do
          user_action = @user_actions.nil? ? nil : @user_actions[notification.card_id]
          json.partial! 'cards/details', card: @cards_map[notification.card_id], user: @user, user_action: user_action
        end
      else
        json.card nil
      end
    else
      json.deep_link_id notification.app_deep_link_id
      json.deep_link_type notification.app_deep_link_type
    end
    json.seen notification.seen?
    json.user do
      ncu = notification.notifications_causal_users.first
      json.partial! 'users/details', user: ncu.user, anonymous: ncu.anonymous
      json.following current_user_following_user_id?(ncu.user.id)
    end
    json.users do
      ncus = notification.notifications_causal_users
      json.array! ncus do |ncu|
        json.partial! 'users/details', user: ncu.user, anonymous: ncu.anonymous
        json.following current_user_following_user_id?(ncu.user.id)
      end
    end
    json.created_at notification.created_at
  end
end

sso_name = org_sso.sso.name
json.(org_sso, *org_sso_configs)

json.id org_sso.id
json.name sso_name
json.description org_sso.sso.description
json.icon_url image_path("sso/#{sso_name}.png")
json.enabled org_sso.is_enabled

json.id file.id
if file.kind_of?(ActiveRecord::Base)
  json.available file.available?
else
  json.available true
end
json.file_url file.file_url
json.file_type file.file_type
json.file_name file.file_name

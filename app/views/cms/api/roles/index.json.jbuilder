json.array! @roles do |role|
  json.partial! 'details', role: role
end

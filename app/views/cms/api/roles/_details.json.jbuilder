json.id role.id
json.name role.name
json.default_name role.default_name
json.master_role role.master_role
json.permissions do
  json.array! role.role_permissions do |permission|
    json.name permission.name
    json.label Permissions.permission_name(permission.name)
  end
end
json.users do
  json.array! role.users do |user|
    json.id user.id
    json.name user.name
  end
end
json.users_count role.users.count

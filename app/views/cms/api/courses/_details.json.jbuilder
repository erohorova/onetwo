json.id course.id
json.channels do
  json.array! course.channels do |ch|
    json.id ch.id
    json.label ch.label
  end
end
json.topics do
  json.array! course.tags do |tg|
    json.id tg.id
    json.name tg.name.gsub('#', '')
  end
end
json.name course.name
json.description course.description
json.is_promoted course.is_promoted
json.is_hidden course.is_hidden
json.website_url course.website_url
json.image_url course.image_url || image_url('card_default_image_min.jpg')
json.users_count course.users_count
json.created_at course.created_at
json.source 'Savannah'
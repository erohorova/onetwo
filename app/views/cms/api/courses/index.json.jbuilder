json.total_count @total_count
json.results do
  json.array! @courses, partial: 'cms/api/courses/details', as: :course
end
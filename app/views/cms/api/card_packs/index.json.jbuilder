json.total_count @total_count
json.results do
  json.array! @cards, partial: 'cms/api/cards/details', as: :card
end

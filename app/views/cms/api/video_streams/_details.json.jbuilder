json.id video_stream.id
json.channels do
  json.array! video_stream.channels do |ch|
    json.id ch.id
    json.label ch.label
  end
end
json.topics do
  json.array! video_stream.tags do |tg|
    json.id tg.id
    json.name tg.name.gsub('#', '')
  end
end
json.name video_stream.name
json.slug video_stream.slug
json.card_slug video_stream.card_slug
json.status video_stream.status
json.state video_stream.state
json.is_official video_stream.is_official
json.start_time video_stream.start_time
json.created_at video_stream.created_at
json.image_url video_stream.image || image_url('card_default_image_min.jpg')
if video_stream.past_stream_available?
  json.recording do
    json.transcoding_status video_stream.default_recording.transcoding_status
    json.mp4_location video_stream.default_recording.mp4_location
    json.hls_location video_stream.default_recording.hls_location
  end
end

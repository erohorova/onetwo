json.total_count @total_count
json.results do
  json.array! @video_streams, partial: 'cms/api/video_streams/details', as: :video_stream
end
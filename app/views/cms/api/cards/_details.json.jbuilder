json.id card.id
json.message card.sanitize_message
json.title (card.message != card.title) ? card.title.try(:clean) : ""
json.state card.state
json.is_official card.is_official
json.author_name card.author_name
json.channels do
  json.array! card.channels do |ch|
    json.id ch.id
    json.label ch.label
  end
end
json.topics do
  json.array! card.tags do |tg|
    json.id tg.id
    json.name tg.name.gsub('#', '')
  end
end
json.user_taxonomy_topics card.user_taxonomy_topics || []
json.created_at card.created_at
json.card_type card.card_type
json.card_template card.card_subtype
if card.kind_of?(ActiveRecord::Base)
  content_map = card.get_content_map
else # searchkick
  content_map = card.content
end
json.source_url content_map['link_url'] || content_map['video_url'] || content_map['image_url']
json.resource do
  json.title content_map['link_title'] || content_map['video_title']
  json.description content_map['link_description'] || content_map['video_description']
  json.site_name content_map['link_site'] || content_map['video_site']
  json.image_url content_map['link_image_url'] || content_map['video_image_url'] || content_map['image_url']
  json.video_url content_map['video_video_url']
  json.embed_html content_map['video_embed_html']
end

# poll
if card.poll_card?
  json.quiz_question_options do
    json.array! card.quiz_question_options do |qqo|
      json.partial! 'cms/api/quiz_question_options/details', quiz_question_option: qqo
      json.count card.quiz_question_stats.try(:options_stats).try(:[], qqo['id'].to_s) || 0
      json.is_correct qqo.is_correct
    end
  end
  json.is_assessment card.is_a_poll_question?
end
json.file_resources do
  json.array! card.file_resources do |file_resource|
    json.partial! 'api/v2/file_resources/details', file_resource: file_resource  unless file_resource.blank?
  end
end
json.filestack card.filestack
json.promoted_at card.promoted_at
json.deleted_at card.deleted_at

if card.ecl_metadata.present?
  json.ecl_source_display_name card.ecl_metadata["source_display_name"]
  json.ecl_source_type_name card.ecl_metadata["source_type_name"]
  json.ecl_duration_metadata formatted_duration_metadata(card)
  json.ecl_source_logo_url card.ecl_metadata["source_logo_url"]
end

json.is_paid card.is_paid
json.prices do
  json.array! card.prices do |price|
    json.id price.id
    json.amount price.amount
    json.currency price.currency
    json.symbol CURRENCY_TO_SYMBOL[price.currency] || "$"
  end
end

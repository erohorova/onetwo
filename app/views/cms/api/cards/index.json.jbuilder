json.total_count @total_count
json.cards do
  json.array! @cards, partial: 'cms/api/cards/details', as: :card
end

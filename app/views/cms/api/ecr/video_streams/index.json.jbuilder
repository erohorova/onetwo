json.array! @video_streams do |video_stream|
  json.partial! 'cms/api/video_streams/details', video_stream: video_stream
  json.is_cloned @cloned_streams[video_stream.id].present?
end

json.events do |json|
  json.partial! 'events/details', event: @event
end
json.rsvps do |json|
  json.array! @event.events_users do |events_user|
    json.events_user_id events_user.id
    json.partial! 'users/details', user: events_user.user, anonymous: nil, include_email: true
    json.rsvp events_user.rsvp

  end
end

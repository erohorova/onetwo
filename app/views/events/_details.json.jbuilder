json.id event.id
json.extract! event, :name, :description, :address, :event_start, :event_end,
:latitude, :longitude, :state, :privacy
json.ical event.ical.url
json.googlecal_url event.googlecal_url
json.rsvp_yes_count event.rsvp_yes_count
json.current_user do |json|
  json.extract! (event.user_rsvp current_user), :id, :rsvp
end
json.owner do
  json.partial! 'users/details', user: event.user, anonymous: nil
end
json.conference_type event.conference_type
json.conference_url event.conference_url
json.host_url event.host_url
json.conference_email event.conference_email
json.callback_token event.encrypt if current_user == event.user
json.youtube_url event.youtube_url
json.owner_id event.user_id
json.hangout_discussion_enabled event.hangout_discussion_enabled
json.context_enabled event.context_enabled

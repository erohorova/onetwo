json.array! @events do |event|
  json.partial! 'events/details', event: event
  json.users do |json|
    json.array! event.rsvp_top_users
  end
end

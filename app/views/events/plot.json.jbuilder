json.array! @coords do |coord|
  json.latitude coord[0]
  json.longitude coord[1]
end

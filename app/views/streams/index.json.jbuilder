json.array! @streams do |stream|
  json.id stream.id
  json.user do
    json.extract!(stream.user, :id, :first_name, :last_name, :name, :photo, :full_name)
  end
  json.snippet stream.snippet
  json.message stream.message
  json.is_mobile stream.is_mobile?
  json.action stream.action
  json.item_class stream.item.class.name
  json.item do
    json.type stream.item.class.name
    case stream.item
    when Answer, Comment
      json.id stream.item.id
      commentable = stream.item.commentable
      json.question do
        json.id (commentable.type == "Answer") ? commentable.commentable_id : stream.item.commentable_id
      end
      if stream.item.commentable.respond_to?(:snippet)
        json.snippet stream.item.commentable.snippet
      else
        json.snippet ''
      end
    when Question, Announcement
      json.id stream.item.id
      json.title stream.item.title
    when PrivateGroup
      json.id stream.item.id
      json.name stream.item.name
    end
  end
  json.indefinite_noun stream.item.class.name.humanize.downcase.indefinitize
  json.created_at stream.created_at
end

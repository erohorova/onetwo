json.id post.id
json.extract! post, :title, :message, :group_id,
  :comments_count, :votes_count, :created_at, :updated_at
json.anonymous post.anonymous

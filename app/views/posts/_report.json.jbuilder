json.reportingDisabled !reportingValidTuple[0]
json.reportingDisabledMessage reportingValidTuple[1]
if reportingValidTuple[1] == 'Reported'
  json.hidden true
end

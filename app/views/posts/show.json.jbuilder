json.partial! 'posts/details', post: @post
if current_user
  json.read PostRead.read_by?(user: current_user, post: @post) #should always be able to pull this out from elastic search

  if @post.kind_of?(ActiveRecord::Base)
    json.partial! 'posts/report', reportingValidTuple: @post.get_report_action_validity(current_user)
  else
    json.partial! 'posts/report', reportingValidTuple: Post.get_report_action_validity(current_user, @post)
  end

  if @post.kind_of?(ActiveRecord::Base)
    json.up_voted @post.voted_by?(current_user)
  else
    json.up_voted Post.voted_by?(@post, current_user)
  end
end

if @post.kind_of?(ActiveRecord::Base)
  json.is_faculty_approved @post.has_approval?
else
  json.is_faculty_approved Post.has_approval?(@post)
end

if @post.kind_of?(ActiveRecord::Base)
  json.post_url Post.url(@post.attributes)
else
  json.post_url Post.url(@post)
end

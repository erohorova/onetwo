json.id post.id
if post.kind_of?(ActiveRecord::Base)
  json.type post.class.name
else
  json.type post._type == 'announcement' ? 'Announcement' : 'Question'
end
json.extract! post, :title, :message, :group_id,
  :comments_count, :votes_count, :created_at, :updated_at
json.faculty_answer_count post.faculty_answer_count
json.student_answer_count post.student_answer_count
json.user do
  json.partial! 'users/details', user: post.user, anonymous: post.anonymous
  if !post.anonymous && !post.group_id.nil?
    json.partial! 'users/score', user: post.user, group_id: post.group_id
  end
  json.following current_user_following_user_id?(post.user.id)
end
json.resource do
  if post.resource.nil?
    json.null!
  else
    json.partial! 'resources/details', resource: post.resource
  end
end
json.tags do
  json.array! post.tags, partial: 'tags/details', as: :tag
end
json.anonymous post.anonymous
if post.kind_of?(ActiveRecord::Base)
  json.is_staff post.created_by_staff?
else
  json.is_staff Post.created_by_staff?(post)
end
json.creator_label post.creator_label
json.hidden post.hidden
json.pinned post.pinned
json.context_label post.context_label
json.context_id post.context_id
json.is_mobile post.is_mobile
json.file_resources do
  json.array! post.file_resources, partial: 'file_resources/details', as: :file
end
json.following (current_user && current_user.following_id_and_type?(id: post.id, type: 'Post'))

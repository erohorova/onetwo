json.id post.id
json.type post._type == 'announcement' ? 'Announcement' : 'Question'
json.extract! post, :title, :group_id, :anonymous, :pinned, :context_label, :is_mobile, :context_id,
  :comments_count, :votes_count, :created_at, :updated_at
json.message SimpleMessageExtractor.simple_text(post.message)
json.faculty_comments_count post.faculty_answer_count
json.student_comments_count post.student_answer_count
json.user do
  json.id post.user.id
  if post.anonymous
    json.name "Anonymous"
    json.picture post.user.anonimage.url
  else
    json.partial! 'users/capsule', user: post.user
  end
end
json.resource do
  if post.resource.nil?
    json.null!
  else
    json.partial! 'resources/details', resource: post.resource
  end
end
json.tags do
  json.array! post.tags, partial: 'tags/details', as: :tag
end
json.file_resources do
  json.array! post.file_resources, partial: 'file_resources/details', as: :file
end
json.group_name post['group_name']
json.website_url post['website_url']
json.deeplink do
  json.id post['deeplink_id']
  json.url post['deeplink_url']
end
rationale = post['rationale']
if rationale && rationale['timestamp']
  json.created_at rationale['timestamp']
end
json.update_act (rationale && rationale['update_act']) || 'created'
json.users []
json.action ''
json.stream_type 'post'
if rationale
  json.action rationale['text']
  json.stream_type rationale['stream_type']
  json.users do
    json.array! rationale['users'] do |u|
      json.partial! 'users/capsule', user: u
    end
  end
  comment = rationale['focus_comment']
  if comment
    json.comment do
      json.id comment['id']
      json.message SimpleMessageExtractor.simple_text(comment['message'])
      json.created_at comment['created_at']
      json.votes_count comment['votes_count']
      json.upvoted comment['upvoted']
      json.user do
        json.id comment['user_id']
        json.partial! 'users/capsule', user: comment['user']
      end
      resource = comment['resource']
      if resource
        json.resource do
          json.extract! comment['resource'], 'title', 'description', 'url', 'image_url', 'video_url', 'site_name'
        end
      end
    end
  end
end

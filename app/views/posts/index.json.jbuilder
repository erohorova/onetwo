json.array! @posts do |post|
  json.partial! 'posts/details', post: post
  json.read @reads.key?(post.id.to_i)
  if current_user
    json.partial! 'posts/report', reportingValidTuple: Post.get_report_action_validity(current_user, post)
  end
  json.up_voted @votes_by_user.include?(post.id.to_i)
  json.is_faculty_approved @approved_posts.include?(post.id.to_i)
  json.post_url Post.url(post)
end

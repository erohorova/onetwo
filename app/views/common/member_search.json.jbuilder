json.ignore_nil!
json.query @query
json.offset @offset
json.limit @limit
json.total @total
json.sort @sort
json.users do
  json.array! @users do |user|
    json.extract! user, *%i[id name email handle photo role following pending]
  end
end

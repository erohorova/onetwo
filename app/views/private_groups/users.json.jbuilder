json.array! @users do |user|
  json.name user.name
  json.full_name user.full_name
  json.id user.id
  json.first_name user.first_name
  json.last_name user.last_name
  json.handle user.handle
  json.photo user.photo
  json.roles Hash[*user.groups_users.map{ |p| [p.group_id, p.as_type] }.flatten]
  json.banned Hash[*user.groups_users.map{ |p| [p.group_id, p.banned] }.flatten]
end
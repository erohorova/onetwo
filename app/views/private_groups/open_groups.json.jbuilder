json.resource_group_id params[:id]
json.open_groups_count @open_groups_count
json.open_groups do
  json.array! @open_groups do |group|
    json.partial! 'private_groups/details', group: group, user_role: nil
  end
end


json.id group.id
json.name group.name
json.created_at group.created_at
json.users_count group.users_count
json.role user_role
json.is_admin @is_admin
json.access group.access
json.creator do
  json.partial! 'users/details', user: group.creator, anonymous: false
end


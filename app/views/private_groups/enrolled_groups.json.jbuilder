json.resource_group_id params[:id]
json.enrolled_groups_count @user_groups_count
json.enrolled_groups do
  json.array! @groups do |group|
    user_role = group.user_role(current_user)
    json.partial! 'private_groups/details', group: group, user_role: user_role
  end
end


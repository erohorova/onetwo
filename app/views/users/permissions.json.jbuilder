json.id @user.id
json.email @user.email
json.bio @user.bio
json.partial! 'users/capsule', user: @user
json.permissions @user.get_user_permissions
json.id user.id
json.first_name user.first_name
json.last_name user.last_name
json.name user.name
json.bio user.bio
json.partial! 'users/role', user: user
if user.is_a?(User) #if we pulled the user object out of the database
  json.picture user.photo(:medium)
  json.coverimages user.fetch_coverimage_urls
else #user is from search
  if user.respond_to?(:photo_medium) && user.photo_medium.present?
    json.picture user.photo_medium
  else
    json.picture user.photo
  end

  if user.respond_to?(:coverimages)
    json.coverimages user.coverimages
  end
end
json.is_suspended user.is_suspended
json.handle user.handle
if user.is_a?(User)
  json.followers_count user.followers_count
else
  # seach result object
  json.followers_count user['followers_count']
end
json.full_name user.full_name
json.email user.email

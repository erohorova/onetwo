json.array! @streams do |v|
  json.partial! 'api/video_streams/details', video_stream: v, user: current_user
end

json.organization do
  json.id organization.id
  json.name organization.name
  json.host_name organization.host_name
  json.description organization.description
  json.image_url organization.mobile_photo || organization.photo
  json.banner_url organization.photo
  json.splash_image organization.fetch_splash_image_url || organization.splash_image.url
  json.co_branding_logo organization.co_branding_logo.present? ? organization.co_branding_logo.url : nil
  json.home_page organization.edcast_home_page
  json.show_sub_brand_on_login organization.show_sub_brand_on_login
  json.show_onboarding organization.show_onboarding
  json.favicons organization.favicons
  json.custom_domain organization.custom_domain
  json.configs do
  json.array! organization.configs do |config|
    json.id config.id
    json.name config.name
    json.value config.parsed_value
  end
end
end

if user.is_a?(User)
  json.roles user.roles.pluck(:name).uniq
  json.roles_default_names user.roles.pluck(:default_name).uniq
else
  json.roles user.roles.to_a
  json.roles_default_names = user.roles.to_a
end


json.following_users do
  json.count users.count
  json.users do
    json.array! users.sample(6) do |user|
      json.id user.id
      json.partial! 'users/capsule', user: user
    end
  end
end
json.following_channels do
  json.array! channels do |ch|
    json.partial! 'api/channels/details', channel: ch
  end
end
json.writable_channels do
  json.array! writable_channels do |ch|
    json.partial! 'api/channels/details', channel: ch
  end
end

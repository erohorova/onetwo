if profile
  json.profile do
    json.id profile.id
    json.expert_topics profile.expert_topics
    json.learning_topics profile.learning_topics
    json.time_zone profile.time_zone
  end
else
  json.profile nil
end

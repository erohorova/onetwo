json.id user.id
json.email user.email
json.bio user.bio
json.default_team_id user.default_team_id
json.newsletter_opt_in user.pref_newsletter_opt_in
json.partial! 'users/capsule', user: user
json.groups do 
  json.array! user.not_banned_from_groups do |group|
    json.id group.id
    json.savannah_id group.client_resource_id
    json.name group.name
  end
end
json.partial! 'users/user_profile', profile: user.profile

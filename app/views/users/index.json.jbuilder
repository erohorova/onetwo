json.array! @users do |user|
  json.full_name user.full_name
  json.name user.name
  json.id user.id
  json.first_name user.first_name
  json.last_name user.last_name
  json.photo user.photo
  json.role @user_role_in_group[user.id] if @user_role_in_group[user.id]
end

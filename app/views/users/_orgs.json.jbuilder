json.active_orgs do
  json.array! @orgs do |org|
    json.id org.id
    json.name org.name
    json.host_name org.host_name
  end
end
json.offset @offset
json.users do
  json.array! @users do |user|
    json.id user.id
    json.partial! 'users/capsule', user: user
  end
end

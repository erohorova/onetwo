json.offset @offset
json.limit @limit
json.total @response.total
json.cards do
  json.array! @cards do |card|
    user_action = @user_actions[card['id'].to_i]
    json.partial! 'cards/details', card: card, user: @user, user_action: user_action
    json.channels card['channels']
    json.comments do
      json.array! @comments_hash[card['id'].to_i] do |comment|
        json.partial! 'comments/details', comment: comment
        json.is_upvoted @votes_by_user.include?(comment.id)
      end
    end
  end
end
json.user do
  json.id @user.id
  json.partial! 'capsule', user: @user
end

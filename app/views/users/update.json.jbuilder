json.partial! 'users/details', user: @user, anonymous: nil, include_email: true
json.organization do
  json.partial! 'api/organizations/details', organization: @user.organization
end

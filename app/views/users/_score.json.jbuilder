if user.score_entry.blank?
  group = Rails.cache.fetch("api/group/#{group_id}", expires_in: 1.hour) do
    Group.find(group_id)
  end

  score_entry = user.score(group)
else
  score_entry = user.score_entry
end

json.score score_entry[:score]
json.stars score_entry[:stars]

# DESC:
#   Default response must be transmitted over mobile-originated requests.
#   Mobile requests are identified as `Edcast/` in the User-Agent of the headers.
#   Default response must be transmitted for Onboarding and edc-cms also.
#   For web, optimised response must be returned.
if is_native_app? || params[:full_response]
  json.partial! 'users/user_data', user: @user
  # Not loading any users, there might be too many of them
  json.partial! 'users/following_data', users: [], channels: @user.followed_channels, writable_channels: @user.writable_channels
  json.csrf_param 'authenticity_token'
  json.csrf_token form_authenticity_token
  json.jwt_token @user.jwt_token({is_web: @is_web})
  json.is_admin @user.is_admin_user?
  json.is_super_admin @user.is_admin?
  json.sign_in_count @user.sign_in_count
  json.partial! 'users/pubnub_channels', channels: User.pubnub_channels_to_subscribe_to_for_id(@user.id)
  json.organization do
    json.partial! 'api/organizations/details', organization: @user.organization
  end
  json.followers_count @user.followers_count
  json.following_count @user.following_count

  json.partial! 'users/orgs'

  if @user.is_admin? || @user.is_edcast_admin
    json.can_impersonate true
  end

  if @impersonation
    json.impersonation @impersonation
  end

  json.partial! 'api/v2/users/user_profile', profile: @user.profile

  json.permissions @user.get_all_user_permissions
  json.sso_info @user.saml_providers.first
  json.current_session_provider session[:access_uri]

  if @user.is_edcast_admin && @user.organization.status == 'pending'
    json.needs_approval true
  end
  json.password_reset_required @user.password_reset_required
  json.onboarding_options @user.onboarding_options
  json.onboarding_steps_names @user.onboarding_options.keys
  json.hide_from_leaderboard @user.hide_from_leaderboard

  json.user_subscriptions do
    json.array! @user.user_subscriptions
  end
  json.org_annual_subscription_paid (@annual_org_subscription || false)
  json.country_code (request.headers['CloudFront-Viewer-Country'] || "US")
else
  json.merge! UserBridge.new([@user], user: @user, fields: params[:fields]).attributes[0]

  if params[:fields].blank?
    json.partial! 'users/orgs'
    json.csrf_param 'authenticity_token'
    json.csrf_token form_authenticity_token
    json.jwt_token @user.jwt_token({ is_web: @is_web })
    json.onboarding_completed @user.onboarding_completed?
    json.step @user.user_onboarding.try(:current_step)
    json.country_code (request.headers['CloudFront-Viewer-Country'] || "US")
  end
end

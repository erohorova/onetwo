json.array! @suggestions do |suggestion|
  json.id suggestion.id
  json.partial! 'users/capsule', user: suggestion
  json.interests suggestion.interests.limit(2).pluck(:name)
  json.following false
end

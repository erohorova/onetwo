json.user do
  json.id @user.id
  json.partial! 'users/capsule', user: @user
end

json.followers do
  json.array! @followers do |user|
    json.following current_user && current_user.following?(user)
    json.id user.id
    json.partial! 'users/capsule', user: user
  end
end

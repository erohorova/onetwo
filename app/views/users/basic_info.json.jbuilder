json.id @user.id
json.partial! 'users/capsule', user: @user
json.bio @user.bio
json.followers_count @user.followers_count
json.following_count @user.following_count
json.following current_user && current_user.following?(@user)

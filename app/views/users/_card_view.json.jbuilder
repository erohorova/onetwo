json.id user.id
json.partial! 'users/capsule', user: user
score_entry = user.score(group)
json.score score_entry[:score]
json.stars score_entry[:stars]
json.posts_count group.posts.where(:user_id => user.id).count
json.comment_count Comment.joins("inner join posts on posts.id = comments.commentable_id AND comments.commentable_type = 'Post'").where('posts.group_id = (:group_id) AND comments.user_id = (:user_id)', { :group_id => group.id, :user_id => user.id }).count

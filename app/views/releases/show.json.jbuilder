json.created_at @release.created_at.to_i
json.notes @release.notes
json.download_url @release.download_url
json.version @release.version
json.build @release.build
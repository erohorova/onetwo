json.user do
  json.partial! 'users/details', user: @user, include_score_entry: true, include_email: true, anonymous: nil
end
json.group do
  json.partial! 'groups/details', group: @group
end
if @ref_post && @ref_post.group
  json.launch_group do
    json.partial! 'groups/details', group: @ref_post.group
  end
end
json.social_enabled @group.social_enabled
json.user_role @user_role
json.encrypted_user_id ActiveSupport::MessageEncryptor.new(Edcast::Application.config.secret_key_base).encrypt_and_sign(@user.id)

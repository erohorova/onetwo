json.array! @users do |user|
  json.id user.id
  json.name user.name
  json.email user.email
  json.role user.organization_role
  json.handle user.handle
  json.photo user.photo
end
json.id comment.id
json.message comment.message.try(:tr, "\uFFFC", '') # remove OBJ character
json.resource do
  if comment.resource.nil?
    json.null!
  else
    json.partial! 'resources/details', resource: comment.resource
  end
end
json.user do
  json.partial! 'users/details', user: comment.user, include_score_entry: false, include_email: false, anonymous: comment.anonymous
end
json.anonymous comment.anonymous
json.votes_count comment.votes_count
json.created_at comment.created_at
json.updated_at comment.updated_at
json.hidden comment.hidden
json.is_mobile comment.is_mobile
json.file_resources do
  json.array! comment.file_resources, partial: 'file_resources/details', as: :file
end

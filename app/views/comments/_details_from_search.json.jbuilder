json.id comment['id']
json.message comment['message'].try(:tr, "\uFFFC", '') # remove OBJ character
json.resource do
  if comment['resource'].nil?
    json.null!
  else
    json.id comment['resource']['id']
    json.url comment['resource']['url']
    json.title comment['resource']['title']
    json.description comment['resource']['description']
    json.site_name comment['resource']['site_name']
    json.type comment['resource']['type']
    json.image_url comment['resource']['image_url']
    json.video_url comment['resource']['video_url']
    json.created_at comment['resource']['created_at']
    json.embed_html comment['resource']['embed_html']
  end
end
json.tags do
  json.array! comment['tags'] do |tag|
    json.id tag['id']
    json.name tag['name']
  end
end
json.mentions do
  json.array! comment['mentioned_users'] do |mu|
    json.id mu['id']
    json.partial! 'users/capsule', user: OpenStruct.new(mu)
  end
end
json.user do
  json.id comment['user']['id']
  json.first_name comment['user']['first_name']
  json.last_name comment['user']['last_name']
  json.name comment['user']['name']
  json.picture comment['user']['photo']
  json.handle comment['user']['handle']
end
json.votes_count comment['votes_count']
json.created_at comment['created_at']
json.updated_at comment['updated_at']
json.file_resources do
  json.array! comment['file_resources'] do |file_resource|
    json.id file_resource['id']
    json.file_url file_resource['file_url']
    json.file_name file_resource['file_name']
    json.file_type file_resource['file_type']
  end
end

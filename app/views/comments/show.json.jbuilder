json.partial! 'comments/details', comment: @comment
if current_user
  json.partial! 'comments/report', reportingValidTuple: @comment.get_report_action_validity(current_user)
  json.up_voted @comment.voted_by?(current_user)
end
json.is_faculty_approved @comment.has_approval?

json.id comment.id
json.extract! comment, :message, :votes_count, :created_at, :updated_at
json.anonymous comment.anonymous

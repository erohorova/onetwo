json.id comment.id
json.message comment.message.try(:tr, "\uFFFC", '') # remove OBJ character
json.resource do
  if comment.resource.nil?
    json.null!
  else
    json.partial! 'resources/details', resource: comment.resource
  end
end
json.tags do
  json.array! comment.tags, partial: 'tags/details', as: :tag
end
json.mentions do
  json.array! comment.mentioned_users.map(&:capsule_data) do |mu|
    json.id mu['id']
    json.partial! 'users/capsule', user: OpenStruct.new(mu)
  end
end
json.user do
  json.partial! 'users/details', user: comment.user, anonymous: comment.anonymous
  unless comment.anonymous || !comment.group || (defined?(exclude_user_score) && exclude_user_score)
    json.partial! 'users/score', user: comment.user, group_id: comment.group.id
  end
  json.following current_user_following_user_id?(comment.user.id)
end
json.anonymous comment.anonymous
json.votes_count comment.votes_count
json.is_staff comment.created_by_staff?
json.creator_label comment.creator_label
json.created_at comment.created_at
json.updated_at comment.updated_at
json.hidden comment.hidden
json.is_mobile comment.is_mobile
json.comments_count comment.comments_count
json.file_resources do
  json.array! comment.file_resources, partial: 'file_resources/details', as: :file
end

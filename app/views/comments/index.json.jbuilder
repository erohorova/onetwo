json.array! @comments do |comment|
  if defined?(exclude_user_score) && exclude_user_score
    json.partial! 'comments/details', comment: comment, exclude_user_score: true
  else
    json.partial! 'comments/details', comment: comment
  end
  if current_user
    unless defined?(exclude_flag) && exclude_flag
      json.partial! 'comments/report', reportingValidTuple: comment.get_report_action_validity(current_user, @reports_by_user)
    end
    unless defined?(exclude_upvotes) && exclude_upvotes
      json.up_voted @votes_by_user.include?(comment.id)
    end
    unless defined?(exclude_approvals) && exclude_approvals
      json.is_faculty_approved @approved_comments.include?(comment.id)
    end
  end
end

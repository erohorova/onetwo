json.array! comments do |comment|
  json.partial! 'comments/details_from_search', comment: comment
  json.up_voted (votes_by_user && votes_by_user.include?(comment['id']))
end

if report.type == 'InappropriateReport'
  json.type 'inappropriate'
elsif report.type == 'SpamReport'
  json.type 'spam'
else
  json.type 'compromised'
end

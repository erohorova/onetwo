json.spam @reports.where(:type => 'SpamReport').count
json.inappropriate @reports.where(:type => 'InappropriateReport').count
json.compromised @reports.where(:type => 'CompromisedReport').count

#encoding: UTF-8

xml.instruct! :xml, :version => "1.0"
xml.rss :version => "2.0" do
  xml.channel do
    xml.title "#{@channel.label} | EdCast"
    xml.description @channel.description
    xml.link learn_url(:id => @channel.slug)
    xml.language "en"
    
    @cards.each do |card|
      xml.item do
        xml.title card['content']['title']
        card_description = (card['content']['message'] && (strip_tags(card['content']['message'])).truncate(123)) || card['content']['title']
        card_image       = [card['content']['link_image_url'], card['content']['video_image_url'], card['content']['image_url'], asset_path('card_default_image_min.jpg')].detect(&:present?)
        xml.description "<div><p>#{card_description}</p><img src='#{card_image}' /></div>"
        xml.link show_insight_url(card['slug'])
      end
    end
  end
end

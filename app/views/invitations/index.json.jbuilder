json.invitations do
  json.array! @invitations do |invitation|
    json.partial! 'invitations/invitation', invitation: invitation
  end
end
json.total_count @total_count

json.id invitation.id
json.email invitation.recipient_email
json.created_at invitation.created_at
json.accepted_at invitation.accepted_at
json.roles invitation.roles
json.sender_id invitation.sender_id
json.first_name invitation.first_name
json.last_name invitation.last_name
json.id org_sso.id
json.name org_sso.sso.name
json.description org_sso.sso.description
json.icon_url image_path("#{org_sso.sso.name}.png")
json.enabled org_sso.is_enabled
json.label_name org_sso.label_name

json.array! @results do |result|
  if result.is_a? OrgSso
    json.partial! 'detail', org_sso: result
  else
    json.name result.name
    json.description result.description
    json.icon_url image_path("#{result.name}.png")
    json.enabled false
  end
end

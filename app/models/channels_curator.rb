# == Schema Information
#
# Table name: channels_curators
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  channel_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ChannelsCurator < ActiveRecord::Base
  include EventTracker

  has_paper_trail ignore: [:created_at, :updated_at]
  belongs_to :user
  belongs_to :channel

  validates_presence_of :user, :channel
  validates_uniqueness_of :user_id, scope: :channel_id
  validate :should_belong_to_same_org

  after_commit :reindex_channel

  def should_belong_to_same_org
    unless self.user.organization_id == self.channel.organization_id
      errors.add(:base, "User and Channel Organizations don't match")
    end
  end

  configure_metric_recorders(
    default_actor: lambda { |record| record.channel&.creator},
    trackers: {
      on_create: {
        event: 'channel_curator_added',
        job_args: [{ channel: :channel }, { curator: :user }],
        exclude_job_args: [:org]
      },
      on_delete: {
        event: 'channel_curator_removed',
        job_args: [{ channel: :channel }, {curator: :user }],
        exclude_job_args: [:org]
      }
    }
  )

  def get_app_deep_link_id_and_type_v2
    channel.try(:get_app_deep_link_id_and_type_v2)
  end

  private

  def reindex_channel
    channel.reindex
  end

end

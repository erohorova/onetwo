# == Schema Information
#
# Table name: user_level_metrics
#
#  id                        :integer          not null, primary key
#  user_id                   :integer
#  organization_id           :integer
#  period                    :string(191)
#  offset                    :integer
#  smartbites_score          :integer          default(0)
#  smartbites_consumed       :integer          default(0)
#  smartbites_created        :integer          default(0)
#  time_spent                :integer          default(0)
#  percentile                :integer          default(0)
#  smartbites_liked          :integer          default(0)
#  smartbites_comments_count :integer          default(0)
#  smartbites_completed      :integer          default(0)
#

class UserLevelMetric < ActiveRecord::Base
  include MetricsAggregator

  attr_accessor :level_order
  belongs_to :user
  belongs_to :organization

  validates :user, presence: true, uniqueness: {scope: [:period, :offset]}
  validates :organization, presence: true

  before_validation on: :create do
    if self.organization_id.nil?
      self.organization_id = user.try :organization_id
    end
  end

  create_increment_methods([:smartbites_score, :smartbites_created, :smartbites_consumed, :time_spent, :smartbites_comments_count, :smartbites_liked, :smartbites_completed])
  create_getter_methods([:smartbites_score, :smartbites_created, :smartbites_consumed, :time_spent, :smartbites_comments_count, :smartbites_liked, :smartbites_completed])

  class << self

    def get_percentile_for_inactive_users(organization_id: , period: , offset: )
      org = Organization.find_by(id: organization_id)
      users_count = org.real_users.count

      active_users_count = UserLevelMetric.where(organization_id: organization_id, period: period, offset: offset).
        where("time_spent > 0").
        where.not(user_id: Organization.find_by(id: organization_id).exclude_user_ids_for_analytics).
        count

      inactive_users_count = users_count - active_users_count
      inactive_users_count * 100 / users_count
    end

    def get_percentile(user_id: , period: , offset: )
      where(user_id: user_id, period: period, offset: offset).first.try(:percentile) || get_percentile_for_inactive_users(organization_id: User.find_by(id: user_id).organization_id, period: period, offset: offset)
    end

    def get_organization_metrics(organization_id: , period: , offset: )
      data = where(organization_id: organization_id, period: period, offset: offset).
        where("time_spent > 0").
        where.not(user_id: Organization.find_by(id: organization_id).exclude_user_ids_for_analytics).
        select("sum(smartbites_consumed) as smartbites_consumed, sum(smartbites_created) as smartbites_created, avg(time_spent) as average_session, count(user_id) as active_users").group(:organization_id).first
      Hash[[:smartbites_created, :smartbites_consumed, :active_users, :average_session].map{|k| [k, data.try(k).to_i]}]
    end

    def get_organization_metrics_over_span(organization_id: , team_id: , period: , begin_offset: , end_offset: )
      data = team_id.present? ? where(user_id: TeamsUser.where(team_id: team_id).pluck(:user_id), period: period) :
       where(organization_id: organization_id, period: period)

      data = data.where.not(user_id: Organization.find_by(id: organization_id).exclude_user_ids_for_analytics).
        where("offset >= (:begin_offset) AND offset <= (:end_offset)", begin_offset: begin_offset, end_offset: end_offset).
        where("time_spent > 0").
        select("sum(smartbites_consumed) as smartbites_consumed, sum(smartbites_created) as smartbites_created, avg(time_spent) as average_session, count(distinct user_id) as active_users").
        group(:organization_id).
        first
      Hash[[:smartbites_created, :smartbites_consumed, :active_users, :average_session].map{|k| [k, data.try(k).to_i]}]
    end

    def get_organization_drill_down_data(organization_id: , team_id: , period: , offset: )
      drill_down_period_and_offsets = PeriodOffsetCalculator.drill_down_period_and_offsets(period: period, offset: offset)
      data = get_organization_data_for(organization_id: organization_id, team_id: team_id, period: drill_down_period_and_offsets.first, offsets: drill_down_period_and_offsets.last)
      Hash[[:smartbites_created, :smartbites_consumed, :active_users, :average_session].map{|k| [k, drill_down_period_and_offsets.last.map{|offset| data[offset].try(k).to_i}]}]
    end

    def get_organization_drill_down_data_for_timerange(organization_id: , team_id: , period: , timerange: )
      drill_down_period_and_offsets = PeriodOffsetCalculator.drill_down_period_and_offsets_for_timerange(period: period, timerange: timerange)
      data = get_organization_data_for(organization_id: organization_id, team_id: team_id, period: drill_down_period_and_offsets.first, offsets: drill_down_period_and_offsets.last)
      Hash[[:smartbites_created, :smartbites_consumed, :active_users, :average_session].map{|k| [k, drill_down_period_and_offsets.last.map{|offset| data[offset].try(k).to_i}]}]
    end

    def fetch_stats_for_weekly_activity_email(organization_id: , user_id:)
      stats = {
        points_earned: 0,
        team_standing: 0,
        avg_points_earned_by_team: 0
      }
      offset = current_week_offset

      # User performance:
      points_earned = self.get_smartbites_score({user_id: user_id}, :week, offset)
      stats[:points_earned] = points_earned
      stats[:team_standing] = self.where(organization_id: organization_id, period: :week, offset: offset).where("smartbites_score > ? ", points_earned).count + 1

      # Team performance:
      team_performance = self.where(organization_id: organization_id, period: :week, offset: offset).group(:organization_id).select("organization_id, avg(smartbites_score) as avg_smartbites_score").first

      stats[:avg_points_earned_by_team] = team_performance.present? ? team_performance.avg_smartbites_score.to_int : 0
      stats
    end

    def current_week_offset
      PeriodOffsetCalculator.applicable_offsets(Time.now.utc)[:week]
    end

    def filter_by_team_ids(team_ids)
      joins(user: :teams).where(teams: { id: team_ids }).uniq
    end

    def filter_by_role_ids(role_ids)
      joins(user: :roles).where(roles: { id: role_ids }).uniq
    end

    def users_in_my_teams(teams_ids)
      joins(user: {teams: :teams_users}).where(teams_users: {team_id: teams_ids}).uniq
    end

    def get_organization_data_for(organization_id: , team_id: , period: , offsets:)
      data = team_id.present? ? where(user_id: TeamsUser.where(team_id: team_id).pluck(:user_id), period: period, offset: offsets) :
        where(organization_id: organization_id, period: period, offset: offsets)

      data = data.where("time_spent > 0").
        where.not(user_id: Organization.find_by(id: organization_id).exclude_user_ids_for_analytics).
        select("offset, sum(smartbites_consumed) as smartbites_consumed, sum(smartbites_created) as smartbites_created, avg(time_spent) as average_session, count(user_id) as active_users").
        group(:offset).
        index_by(&:offset)
      data
    end
  end

end

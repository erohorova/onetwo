# == Schema Information
#
# Table name: identity_providers
#
#  id            :integer          not null, primary key
#  type          :string(191)
#  uid           :string(191)
#  user_id       :integer
#  token         :string(4096)
#  secret        :string(255)
#  expires_at    :datetime
#  created_at    :datetime
#  updated_at    :datetime
#  token_scope   :string(255)
#  auth          :boolean
#  auth_info     :text(65535)
#  refresh_token :string(255)
#

class IdentityProvider < ActiveRecord::Base

  belongs_to :user

  validates :uid, :user_id, presence: true
  validates :uid, uniqueness: { scope: [:user_id, :type] }

  store :auth_info

  def provider_name
    self.type.sub('Provider', '').downcase
  end

  def is_active?
    expires_at.nil? || expires_at > Time.now
  end

  def self.is_verified?(auth_hash)
   raise StandardError.new 'Child class must implement the verified feature'
  end

  def share *args
    raise StandardError.new 'Child class must implement the "share" method'
  end

  # check scopes for particular social actions (:network, :share)
  # overriding methods may invoke social API calls
  def scoped?(_)
    true
  end

  def self.class_for(provider_name)
    "#{provider_name.classify}Provider".constantize
  end
end

class ScormDetail < ActiveRecord::Base
  belongs_to :card
  validates :card, :token, presence: true
  STATUSES = ['running', 'finished', 'error']

  STATUSES.each do |method|
    define_method "#{method}?" do
      status == "#{method}"
    end
  end
end

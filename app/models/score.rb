# == Schema Information
#
# Table name: scores
#
#  id           :integer          not null, primary key
#  user_id      :integer          not null
#  group_id     :integer          not null
#  score        :integer          default(0)
#  stars        :integer          default(0)
#  lock_version :integer
#

class Score < ActiveRecord::Base
  belongs_to :user
  belongs_to :group

  validates :user, presence: true
  validates :group, presence: true

  def increment_score_and_save
    tries = 0
    begin
      self.score += 1
      self.save
    rescue ActiveRecord::StaleObjectError
      if tries < 4
        tries += 1
        self.reload
        retry
      else
        log.warn "Couldn't increment score in 5 attempts for user: #{self.user_id} \
        and group: #{self.group_id}"
        return
      end
    end
  end

  def decrement_score_and_save
    tries = 0
    begin
      self.score -= 1
      self.save
    rescue ActiveRecord::StaleObjectError
      if tries < 4
        tries += 1
        self.reload
        retry
      else
        log.warn "Couldn't decrement score in 5 attempts for user: #{self.user_id} \
        and group: #{self.group_id}"
        return
      end
    end
  end

  def increment_stars_and_save
    tries = 0
    begin
      self.stars += 1
      self.save
    rescue ActiveRecord::StaleObjectError
      if tries < 4
        tries += 1
        self.reload
        retry
      else
        log.warn "Couldn't increment stars in 5 attempts for user: #{self.user_id} \
        and group: #{self.group_id}"
        return
      end
    end
  end

  def decrement_stars_and_save
    tries = 0
    begin
      self.stars -= 1
      self.save
    rescue ActiveRecord::StaleObjectError
      if tries < 4
        tries += 1
        self.reload
        retry
      else
        log.warn "Couldn't decrement stars in 5 attempts for user: #{self.user_id} \
        and group: #{self.group_id}"
        return
      end
    end
  end

  def organization
    group.try(:organization)
  end
end

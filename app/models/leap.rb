# == Schema Information
#
# Table name: leaps
#
#  id         :integer          not null, primary key
#  card_id    :integer          not null
#  pathway_id :integer
#  correct_id :integer          not null
#  wrong_id   :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Leap < ActiveRecord::Base
  belongs_to :card
  belongs_to :pathway, class_name: 'Card'
  belongs_to :wrong, class_name: 'Card'
  belongs_to :correct, class_name: 'Card'

  validates :card, presence: true
  validates :correct, presence: true
  validates :wrong, presence: true
  validates_uniqueness_of :card_id, scope: :pathway_id, message: 'already has a leap'
  after_validation :cards_valid?

  private

  def cards_valid?
    errors.add(:card, 'should be poll type' ) unless self.card&.card_type == 'poll'
    errors.add(:leap, 'cards should be different for correct and wrong answers' ) if self.wrong_id == self.correct_id
    if self.pathway.present?
      pack = self.pathway.pack_cards
      errors.add(:pathway, 'should be pack type' ) unless self.pathway.card_type == 'pack'
      errors.add(:quiz, 'card should be in this pathway' ) unless pack.include?(self.card)
      errors.add(:leap, "card not in this pathway #{self.wrong.id} for wrong answer") unless pack.include?(self.wrong)
      errors.add(:leap, "card not in this pathway #{self.correct.id} for correct answer") unless pack.include?(self.correct)
    end
  end

end

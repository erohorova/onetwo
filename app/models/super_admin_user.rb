# == Schema Information
#
# Table name: super_admin_users
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class SuperAdminUser < ActiveRecord::Base
  belongs_to :user
  validates_uniqueness_of :user_id

  class << self
    def add_by_email(email)
      user = User.where(email: email, organization: Organization.default_org).first
      if user
        create!(user: user)
        user
      else
        false
      end
    end

    def remove_by_email(email)
      user = User.where(email: email, organization: Organization.default_org).first
      if user && entry = where(user: user).first
        entry.destroy!
        true
      else
        false
      end
    end
  end  
end

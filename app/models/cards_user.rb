# == Schema Information
#
# Table name: cards_users
#
#  id                 :integer          not null, primary key
#  card_id            :integer
#  user_id            :integer
#  state              :string(191)
#  created_at         :datetime
#  updated_at         :datetime
#  response_available :boolean          default(FALSE)
#  response_data      :text(16777215)
#

class CardsUser < ActiveRecord::Base
  ACTIONS = ['upvote', 'unupvote', 'bookmark', 'unbookmark'].freeze

  include Streamable

  has_paper_trail ignore: [:created_at, :updated_at]

  belongs_to :card
  belongs_to :user

  validates_presence_of :card
  validates_presence_of :user
  validates_presence_of :state

  after_commit :reindex_card, on: [:create, :destroy]
  after_commit :generate_notification, on: :create
  after_create :add_to_learning_queue
  before_destroy :remove_from_learning_queue


  scope :upvote, ->{where(state: 'upvote')}

  acts_as_streamable on: :create, if: :upvote?

  def generate_notification
    if state == 'upvote'
      NotificationGeneratorJob.perform_later(item_id: id, item_type: 'card_upvote')
    end
  end

  def add_to_learning_queue
    if state == "bookmark"
      self.card.add_to_learning_queue(user_id: self.user_id, source: "bookmarks")
    end
  end

  def remove_from_learning_queue
    if state == "bookmark"
      self.card.learning_queue_items.where(user_id: self.user_id, source: "bookmarks").destroy_all
    end
  end

  def reindex_card
    IndexJob.perform_later(self.card.class.name, self.card.id)
  end

  def self.error_response
    {'message' => 'Sorry! Something went wrong'}.to_json
  end

  def self.sanitize_response(resp)
    begin
      JSON.parse(resp).slice('message').to_json
    rescue => e
      {}.to_json
    end
  end

  def self.get_user_actions(card_ids: [], user_id: nil)
    user_actions = Vote.where(votable_id: card_ids, votable_type: 'Card', user_id: user_id)
    ret = {}
    user_actions.each do |user_action|
      ret[user_action.votable_id] = [] unless ret.has_key?(user_action.votable_id)
      ret[user_action.votable_id] << "upvote"
    end
    bookmarks = Bookmark.where(bookmarkable_id: card_ids, bookmarkable_type: 'Card', user_id: user_id)
    bookmarks.each do |bookmark|
      ret[bookmark.bookmarkable_id] = [] unless ret.has_key?(bookmark.bookmarkable_id)
      ret[bookmark.bookmarkable_id] << "bookmark"
    end
    ret
  end

  def feedback
    if bad?
      'bad'
    elsif good?
      'good'
    elsif neutral?
      'neutral'
    else
      'none'
    end
  end

  def good?
    !bad? && !neutral?
  end

  def bad?
    self.state == 'dismiss'
  end

  def neutral?
    self.state == 'pass'
  end

  def upvote?
    self.state == 'upvote'
  end

  def activity_initiator
    self.user
  end

  def activity_stream_action
    ActivityStream::ACTION_UPVOTE
  end

  def is_stream_worthy?
    card.publicly_visible?
  end

  def self.get_cards_liked_by(user_ids)
    cu = CardsUser.where("user_id in (:user_ids) AND created_at > (:time_delimiter)",
                         {user_ids: user_ids, time_delimiter: 1.week.ago})
    good_ones = cu.select { |c| c.good? }
    good_ones.map{|c| c.card_id}
  end

  def response_available?
    response_available
  end

  def get_response
    if response_available?
      JSON.parse(response_data)
    end
  end

end

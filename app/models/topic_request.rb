class TopicRequest < ActiveRecord::Base
  include AASM
  include EventTracker

  belongs_to :requestor, class_name: 'User'
  belongs_to :approver, class_name: 'User'
  belongs_to :publisher, class_name: 'User'
  belongs_to :organization

  validates :label, :organization_id, :requestor_id, presence: true
  validates :label, uniqueness: { scope: [:organization_id] }

  after_destroy :remove_notification

  aasm column: 'state', :whiny_transitions => false do
    state :pending, initial: true
    state :approved
    state :rejected
    state :published

    event :approve do
      transitions from: [:pending, :rejected], to: :approved
    end

    event :reject do
      transitions from: :pending, to: :rejected
      after do
        generate_notification("topic_request_rejected")
      end
    end

    event :publish do
      transitions from: :approved, to: :published
      after do
        generate_notification("topic_request_published")
      end
    end
  end

  edited_events = -> (record) {
     return "org_taxonomy_approved" if record.state == "approved"
     return "org_taxonomy_published" if record.state == "published"
     return "org_taxonomy_rejected" if record.state == "rejected"
   }

  configure_metric_recorders(
     default_actor: lambda { |record| },
     trackers: {
       on_create: {
         event: 'org_taxonomy_created',
         job_args: [ {taxonomy: :itself} ],
       },
       on_delete: {
         event: 'org_taxonomy_deleted',
         job_args: [ {taxonomy: :itself} ],
       },
       on_edit: {
         event: edited_events,
         job_args: [ {taxonomy: :itself} ],
       }
     }
   )

  def update_requestor_learning_topic(taxonomy_topic)
    profile = self.requestor.profile || self.requestor.build_profile
    profile.learning_topics << taxonomy_topic
    profile.save
  end

  def generate_notification(topic_request_state)
    NotificationGeneratorJob.perform_later(item_id: id, 
                                         item_type: "topic_request_approval", 
                                         item_state: topic_request_state)
  end

  def remove_notification
    notification = Notification.find_by(notifiable_type: 'TopicRequest', 
                                        user_id: self.requestor_id, 
                                        app_deep_link_id: self.id)
    notification.destroy if notification
  end
end

# == Schema Information
#
# Table name: organization_email_domains
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  url             :string(191)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class OrganizationEmailDomain < ActiveRecord::Base
  belongs_to :organization
  validates_uniqueness_of :url, scope: :organization
  validates :url, format: { with: /\A[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}\z/ }

end

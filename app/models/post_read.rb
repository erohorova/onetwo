class PostRead

  include Elasticsearch::Persistence::Model

  self.index_name = "post_reads_#{Rails.env}_v1"

  attribute :post_id, Integer
  attribute :user_id, Integer
  attribute :group_id, Integer

  validate :reading_rights

  def reading_rights
    unless GroupsUser.where(group_id: self.group_id, user_id: self.user_id).exists? #doesnt matter what the role is, as long as there a role
      errors.add(:post_id, "You cannot read this item")
    end
  end

  def self.get_reads(post_ids = [], user)
    results = self.search query: {
      filtered: {
        filter: {
          bool: {must: [
            {term: {user_id: user.id}},
            terms: {post_id: post_ids}]
          }
        }
      }
  }, size: post_ids.length


    data = {}

    results.results.each do |pr|
      data[pr.post_id] = true
    end
    data
  end

  def self.reads_count(group_id:nil, user_id:nil, post_id:nil, range:nil)
    conditions = []
    conditions << {term: {group_id: group_id}} if group_id
    conditions << {term: {user_id: user_id}} if user_id
    conditions << {term: {post_id: post_id}} if post_id
    conditions << {range: range} if range
    PostRead.count(query: {
      filtered: {
        filter: {
          bool: {must: conditions}
        }
      }
    })
  end

  def self.read_by?(user:nil, post:nil)
    (self.reads_count(user_id: user.id, post_id: post.id) > 0)
  end

  def self.make_primary_key(post:nil, user:nil)
    "post-id-#{post.id}-user-id-#{user.id}"
  end
end

# == Schema Information
#
# Table name: taggings
#
#  id            :integer          not null, primary key
#  tag_id        :integer          not null
#  taggable_id   :integer          not null
#  taggable_type :string(191)      not null
#  created_at    :datetime
#  updated_at    :datetime
#  visible       :boolean          default(TRUE)
#

class Tagging < ActiveRecord::Base
  belongs_to :taggable, :polymorphic => true
  belongs_to :tag

  validates_uniqueness_of :tag_id, scope: [:taggable_type, :taggable_id]

  # after_commit(on: :create) do
  #   TagAutosuggestIndexerJob.perform_later(self)
  # end

end

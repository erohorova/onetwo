# == Schema Information
#
# Table name: invitations
#
#  id                     :integer          not null, primary key
#  sender_id              :integer
#  recipient_email        :string(255)      not null
#  first_name             :string(255)
#  last_name              :string(255)
#  token                  :string(255)
#  sent_at                :datetime
#  roles                  :string(255)
#  recipient_id           :integer
#  accepted_at            :datetime
#  invitable_id           :integer
#  invitable_type         :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  authenticate_recipient :boolean          default(FALSE)
#  parameters             :text(65535)
#

class Invitation < ActiveRecord::Base
  belongs_to :sender, :class_name => 'User'
  belongs_to :recipient, :class_name => 'User'
  belongs_to :invitable, polymorphic: true

  validates :recipient_email, presence: true, email_format: { message: 'is not a valid email address' },
                              uniqueness: {scope: [:invitable_type, :invitable_id, :roles]}
  validates_presence_of :invitable

  before_create :generate_token
  after_commit :send_mail, on: :create
  after_commit :generate_group_invite_notification, on: :create
  after_commit :reindex_team, if: :should_reindex_team?

  after_commit :attach_parameters_to_invitee, if: proc { previous_changes['recipient_id'].present? }

  store :parameters, accessors: %i(channel_ids team_ids)

  attr_accessor :send_invite_email
  scope :pending, -> {where accepted_at: nil}

  def generate_group_invite_notification
    NotificationGeneratorJob.perform_later({item_id: id, item_type: 'group_invite'}) if self.invitable_type == 'Team'
  end

  def attach_parameters_to_invitee
    return if parameters.blank?

    invited_user  = recipient
    custom_fields = invitable.custom_fields

    Channel.where(id: parameters['channel_ids']).each { |c| c.add_followers([invited_user]) }
    Team.where(id: parameters['team_ids']).each { |t| t.add_member(invited_user) }
    custom_fields.wrap(user: invited_user, attributes: parameters.except('channel_ids', 'team_ids'))

    UserProfile.set_profile(user: invited_user, attributes: { 'language' => parameters['language'] }) if parameters['language'].present?
  end

  def branchmetrics_url
    url_helpers  = Rails.application.routes.url_helpers
    organization = invitable.is_a?(Organization) ? invitable : invitable.organization

    join_pars    = { token: token, host: invitable.host }
    web_join_url = url_helpers.join_url(join_pars)

    branchmetrics_payload = {
      '$desktop_url'   => web_join_url,
      '$deeplink_path' => 'join_organization',
      'join_url'       => url_helpers.join_url(join_pars.merge(format: :json)),
      'organization'   => { id: invitable.id, name: invitable.name, host_name: invitable.host_name,
                            hostName: invitable.host_name, photo: invitable.mobile_photo },
      'sender'         => { id: sender_id, name: sender.name, photo: sender.photo },
      'invitation'     => { invitation_id: self.id, is_sso: true }
    }

    # with fallback in case branch fails for whatever reason
    BranchmetricsApi.generate_link(branchmetrics_payload, organization: organization) || web_join_url
  end

  def resend_mail
    do_send_mail
  end

  def recipient_name
    [
      first_name,
      recipient_email.split('@', 2).first
    ].detect(&:present?)
  end

  def pending?
    accepted_at.nil?
  end

  private

  def should_reindex_team?
    invitable_type == 'Team' && accepted_at.nil?
  end

  def reindex_team
    invitable.reindex_async_urgent if invitable.present?
  end

  def send_mail
    do_send_mail unless self.send_invite_email == false
  end

  def do_send_mail
    case invitable
      when Organization, Team
        invitable_name = nil
        if invitable.respond_to? :name
          invitable_name = invitable.name

        end
        invitable_photo = nil
        if invitable.respond_to? :photo
          invitable_photo = invitable.photo
        end

        InvitationMandrillMailer.invite({'first_name' => first_name, 'last_name' => last_name,
                                 'email' => recipient_email,
                                 'invitable_id' => invitable_id,
                                 'invitable_type' => invitable_type,
                                 'token' => token,
                                 'roles' => roles,
                                 'invitable_photo' => invitable_photo,
                                 'invitable_name' => invitable_name,
                                 'sender_name' => sender.try(:name),
                                 'recipient_name' => recipient.try(:first_name),
                                 'id' => self.id
                                }).deliver_later
    end
  end

  def generate_token
    self.token = SecureRandom.hex
  end

end

# == Schema Information
#
# Table name: video_streams_users
#
#  id              :integer          not null, primary key
#  video_stream_id :integer          not null
#  user_id         :integer          not null
#  created_at      :datetime
#  updated_at      :datetime
#

class VideoStreamsUser < ActiveRecord::Base
  include EventTracker

  belongs_to :video_stream
  belongs_to :user

  validates :user, presence: true
  validates :video_stream, presence: true
  validates :user_id, uniqueness: {scope: :video_stream_id}

  configure_metric_recorders(
    default_actor: :user,
    trackers: {
      on_create: {
        job_args: [{card: :video_stream_card}],
        event: 'card_video_stream_started_viewing'
      },
      on_delete: {
        job_args: [{card: :video_stream_card}],
        event: 'card_video_stream_stopped_viewing',
      }
    }
  )

  def video_stream_card
    video_stream.card
  end
end

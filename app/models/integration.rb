# == Schema Information
#
# Table name: integrations
#
#  id                :integer          not null, primary key
#  name              :string(255)
#  description       :text(65535)
#  logo_file_name    :string(255)
#  logo_content_type :string(255)
#  logo_file_size    :integer
#  logo_updated_at   :datetime
#  enabled           :boolean
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  callback_url      :string(255)
#  fields            :text(65535)
#

class Integration < ActiveRecord::Base
  has_many :external_courses
  
  scope :enabled, -> {where enabled: true}

  has_attached_file :logo, :styles => { small: '120x120', medium: '320x320' }
  validates_attachment_content_type :logo,  :content_type => /^image\/(png|jpeg|jpg|pjpeg|x-png|gif|tiff)$/,
                                    :message => 'Invalid file type'
  validates :name,  presence: true                             
end

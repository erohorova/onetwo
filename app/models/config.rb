# == Schema Information
#
# Table name: configs
#
#  id              :integer          not null, primary key
#  category        :string(191)
#  name            :string(191)
#  data_type       :string(255)      default("string")
#  value           :text(65535)
#  configable_type :string(191)
#  configable_id   :integer
#  created_at      :datetime
#  updated_at      :datetime
#  description     :string(255)
#

class Config < ActiveRecord::Base
  include IdentityCache
  include EventTracker

  belongs_to :configable, polymorphic: true, touch: true
  OKTA_GROUP = 'OktaDefaultGroups'.freeze
  OKTA_API_TOKEN = 'OktaApiToken'.freeze


  validates_uniqueness_of :name, scope: [:configable_type, :configable_id, :category]
  validates_presence_of :data_type, :value
  after_create :create_okta_group_config, if: :okta_api_token?
  before_save :check_guide_me_secret, if: Proc.new { |config|  config.name == 'guide_me_config' }

  configure_metric_recorders(
    # default_actor: lambda {|record| record.configable.try(:admins)&.first },
    default_actor: lambda {|record| },
    trackers: {
      on_create: {
        if_block: lambda { |record|
          ['Organization', 'CustomField'].include?(record.configable_type)
        },
        event: lambda { |record|
          case record.configable_type
          when "Organization" then "org_config_added"
          when "CustomField" then "org_custom_field_added"
          end
        },
        job_args: lambda { |record|
          case record.configable_type
          when "Organization"
            [ { org: :configable, config: :itself } ]
          when "CustomField"
            [ { custom_field: :configable, config: :itself } ]
          end
        }
      },
      on_edit: {
        if_block: lambda { |record|
          ['Organization', 'CustomField', 'Team'].include?(record.configable_type)
        },
        event: lambda { |record|
          case record.configable_type
          when "Organization" then "org_config_edited"
          when "CustomField" then "org_custom_field_edited"
          when "Team" then "group_edited"
          end
        },
        job_args: lambda { |record|
          case record.configable_type
          when "Organization"
            [ { org: :configable, config: :itself } ]
          when "CustomField"
            [ { custom_field: :configable, config: :itself } ]
          when "Team"
            [ { group: :configable, changed_column: record.name } ]
          end
        }
      }
    }
  )

  def create_okta_group_config
    if value.present?
      details = {"name" => "INSTANCE - #{self.configable.host_name.upcase}", 'description' => "All users in the organization #{self.configable.host_name}"}
      okta_provision = Okta::Provision.new(organization: self.configable)
      response = okta_provision.create_group(opts: details)
      if response && response['id'].present?
        begin
          self.configable.configs.create!(name: OKTA_GROUP, data_type: "string", value: response['id'])
        rescue Exception => e
          log.error e.inspect
        end
      else
        self.errors.add(:base, "Invalid Okta Api Token")
      end
      create_okta_app
    end
  end

  def create_okta_app
    client_id = generate_client_id
    app_url = "https://#{self.configable.host}"
    okta_app = Okta::Provision.new(organization: self.configable)
    org_name = self.configable.name
    response = okta_app.create_app({client_id: client_id, app_url: app_url})
    if response && response['id'].present?
      begin
        config_value = {
                        client_id: client_id,
                        client_secret: response['credentials']['oauthClient']['client_secret'],
                        }
        existing_config = self.configable.configs.find_by(name: 'OktaDefaultApplication')
        configured_value = existing_config.parsed_value
        configured_value.merge!(config_value)
        existing_config.update(value: configured_value.to_json)

        okta_app.link_app_with_group({app_id: response['id'], group_id: self.configable.linked_okta_groups})
      rescue Exception => e
        log.error e.inspect
      end
    else
      self.errors.add(:base, "Invalid Okta Api Token")
    end
  end

  def generate_client_id
    (('a'..'z').to_a.sample(4) +
      ('A'..'Z').to_a.sample(4) +
      (0..9).to_a.sample(4) +
      ['!'].sample(4))
    .join('')
  end

  # We do not pass guideme org secret in response because of security constraint.
  # Hence when frontend sends empty org secret in config update request,
  # this change is done to ensure org secret is not set blank.
  def check_guide_me_secret
    return if (prev_value = ActiveSupport::JSON.decode(value_was)&.with_indifferent_access).blank?
    if value_changed?
      value = ActiveSupport::JSON.decode(value_change[1])&.with_indifferent_access
      if value[:org_secret].blank?
        value[:org_secret] = prev_value[:org_secret]
      end
      self.value = ActiveSupport::JSON.encode(value)
    end
  end

  def okta_api_token?
    OKTA_API_TOKEN == self.name
  end

  def get_guide_me_value
    Rails.cache.fetch("#{cache_key}/guide_me_config", expires_in: 10.minutes) do
      data = ActiveSupport::JSON.decode(value).with_indifferent_access
      data[:encrypted_org_key] = get_encrypted_org_key(data) if data[:org_key] && data[:org_secret]
      data.except(:org_secret)
    end
  end

  def get_encrypted_org_key(data)
    payload = {
      org_key: data[:org_key],
      timestamp: Time.now.to_i
    }

    JWT.encode(payload, data[:org_secret], 'HS256')
  end

  def parsed_value
    case data_type
      when 'json'
        ActiveSupport::JSON.decode(value).with_indifferent_access
      when 'integer'
        value.to_i
      when 'boolean'
        value.to_bool
      else #default to string
        value
    end
  end

  def parsed_array_value
    ActiveSupport::JSON.decode(value)
  end

  def self.profile_templates
    where('category = :category AND name LIKE :learning OR name LIKE :competency OR name LIKE :education',
      {category: "Profile", learning: "%course_taken", competency: "%added_new_competencies", education: "%education/summary"}).pluck(:name, :value).to_h
  end
end

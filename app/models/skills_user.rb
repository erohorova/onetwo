class SkillsUser < ActiveRecord::Base
  include EventTracker

  belongs_to :user
  belongs_to :skill

  validates_uniqueness_of :skill_id, scope: :user_id
  validates :user_id, :skill_id, presence: :true

  serialize :credential, Array

  configure_metric_recorders(
    default_actor: :user,
    trackers: {
      on_create: {
        job_args: [{ skill: :skill, user_skill: :itself }],
        event: 'user_skill_created'
      },
      on_edit: {
        job_args: [{ skill: :skill, user_skill: :itself }],
        event: 'user_skill_edited',
      },
      on_delete: {
        job_args: [{ skill: :skill, user_skill: :itself }],
        event: 'user_skill_deleted',
      }
    }
  )

end

class Skill < ActiveRecord::Base
  has_many :skills_users, dependent: :destroy
  validates :name, uniqueness: true, presence: true
end

# == Schema Information
#
# Table name: channels_users
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  channel_id :integer
#  as_type    :string(191)      default("author")
#  created_at :datetime
#  updated_at :datetime
#

class ChannelsUser < ActiveRecord::Base
  include EventTracker

  BULK_USER_START = 5

  belongs_to :user
  belongs_to :channel

  validates :channel, presence: true
  validates :user, presence: true
  validates :as_type, inclusion: { in: %w[author trusted_author] }
  validate :author_has_access
  validate :should_belong_to_same_org

  after_commit :reindex_channel

  before_destroy :remove_notification
  scope :author, -> { where(as_type: 'author') }
  scope :trusted_author, -> { where(as_type: 'trusted_author') }
  scope :all_authors_types, -> { where(as_type: ['author', 'trusted_author' ]) }

  configure_metric_recorders(
    default_actor: lambda {|record| record.channel&.creator},
    trackers: {
      on_create: {
        event: 'channel_collaborator_added',
        exclude_job_args: [:org],
        job_args: [{channel: :channel}, {user_id: :user_id}]
      },
      on_delete: {
        event: 'channel_collaborator_removed',
        exclude_job_args: [:org],
        job_args: [{channel: :channel}, {user_id: :user_id}],
        if_block: :channel
      }
    }
  )

  def should_belong_to_same_org
    return if user.organization_id == channel.organization_id
    errors.add(:base, 'User and Channel organizations do not match')
  end

  def author_has_access
    has_permissions = user.organization.get_channel_permissions_for(user)
    errors.add(:channel, 'user does not have access') unless channel && has_permissions
  end

  def remove_notification
    Notification.where(notifiable: channel, user_id: user.id, notification_type: 'added_author').try(:destroy_all)
    true
  end

  def get_app_deep_link_id_and_type_v2
    channel.try(:get_app_deep_link_id_and_type_v2)
  end

  private

  def reindex_channel
    channel.reindex
  end
end

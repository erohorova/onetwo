class ProjectSubmission < ActiveRecord::Base
  belongs_to :submitter, foreign_key: "submitter_id", class_name: "User"
  belongs_to :project

  has_many :project_submission_reviews

  serialize :filestack, Array

  validates_presence_of :project, :submitter

  # this scope takes reviewers project_ids and returns project_submissions. (for the reviewer_user)
  scope :reviews_required, ->(project_ids) { where("project_id in (?)", project_ids).joins("
        LEFT JOIN project_submission_reviews ON project_submission_reviews.project_submission_id =
        project_submissions.id").where("project_submission_reviews.project_submission_id
        IS NULL").includes(:project_submission_reviews) }

  # this scope returns project_submissions which are awaiting reviews. (for the submitter_user)
  scope :awaiting_reviews, -> { joins("LEFT JOIN project_submission_reviews ON
        project_submission_reviews.project_submission_id = project_submissions.id").where("
        project_submission_reviews.project_submission_id IS NULL").includes(:project_submission_reviews) }
end

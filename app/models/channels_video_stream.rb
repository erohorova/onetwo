# == Schema Information
#
# Table name: channels_video_streams
#
#  id              :integer          not null, primary key
#  video_stream_id :integer          not null
#  channel_id      :integer          not null
#  created_at      :datetime
#  updated_at      :datetime
#

class ChannelsVideoStream < ActiveRecord::Base
  belongs_to :channel
  belongs_to :video_stream

  validates_presence_of :channel
  validates_presence_of :video_stream
  validate :should_belong_to_same_org
  
  after_create :touch_video_stream
  

  def should_belong_to_same_org
    unless self.card.organization_id == self.channel.organization_id
      errors.add(:base, "Card and Channel Organizations don't match")
    end
  end

  def touch_video_stream
    video_stream.touch(:updated_at)
  end

end

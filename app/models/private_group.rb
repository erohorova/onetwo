# == Schema Information
#
# Table name: groups
#
#  id                                 :integer          not null, primary key
#  name                               :string(255)      not null
#  description                        :text(16777215)
#  website_url                        :string(255)
#  created_at                         :datetime
#  updated_at                         :datetime
#  client_id                          :integer
#  client_resource_id                 :string(191)
#  type                               :string(255)
#  access                             :string(255)
#  users_count                        :integer          default(0)
#  course_data_url                    :text(16777215)
#  image_url                          :text(16777215)
#  details_locked                     :boolean          default(FALSE)
#  creator_id                         :integer
#  parent_id                          :integer
#  close_access                       :boolean          default(FALSE)
#  topic_id                           :integer
#  picture_file_name                  :string(255)
#  picture_content_type               :string(255)
#  picture_file_size                  :integer
#  picture_updated_at                 :datetime
#  is_private                         :boolean          default(FALSE)
#  start_date                         :datetime
#  course_term                        :string(255)
#  end_date                           :datetime
#  connect_to_social_mediums          :boolean          default(TRUE)
#  enable_mobile_app_download_buttons :boolean          default(TRUE)
#  share_to_social_mediums            :boolean          default(TRUE)
#  daily_digest_enabled               :boolean          default(FALSE)
#  forum_notifications_enabled        :boolean          default(TRUE)
#  language                           :string(255)      default("en")
#  is_promoted                        :boolean          default(FALSE)
#  is_hidden                          :boolean          default(FALSE)
#  course_code                        :string(255)
#  organization_id                    :integer
#

class PrivateGroup < Group

  belongs_to :resource_group, foreign_key: :parent_id

  validates_inclusion_of :access, in: ['open', 'invite']
  validates_presence_of :resource_group
  validates_presence_of :creator

  before_create do |group|
    group.admins << group.creator
  end

  def is_open?
    access == 'open'
  end

  def is_invite_only?
    access == 'invite'
  end

  def snippet
    name.truncate(60, separator: ' ')
  end

end

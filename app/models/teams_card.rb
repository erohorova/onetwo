# == Schema Information
#
# Table name: teams_cards
#
#  id         :integer          not null, primary key
#  team_id    :integer
#  card_id    :integer
#  type       :string(10)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#

class TeamsCard < ActiveRecord::Base

  include Manageable

  belongs_to :team
  belongs_to :card
  belongs_to :user, foreign_key: 'user_id', class_name: 'User'

  validates :team_id, :type, presence: true
  validates_uniqueness_of :card_id, scope: [:team_id, :type]

  scope :shared_cards, ->{where(type: 'SharedCard')}

  before_save :validate_card
  after_commit :generate_notification, on: :create
  after_destroy :remove_card_share_notification

  def validate_card
    errors.add(:card, 'Card cannot be blank') if card_id.nil?
  end

  acts_as_manageable on: :create

  # manageable methods start
  def is_manageable?
    # author could be nil e.g. RSS cards
    author = card&.author
    return false unless user
    user != author
  end

  def get_ucm_values
    # user, card, action, target_id
    action = UsersCardsManagement::ACTION_SHARE_TO_TEAM
    [user_id, card_id, action, team_id]
  end
  # manageable methods end

  def generate_notification
    NotificationGeneratorJob.perform_later(item_id: id, item_type: 'team_card_share') if type == "SharedCard"
  end

  def remove_card_share_notification
    team = Team.find_by(id: self.team_id)
    if team 
      team_users_ids = team.teams_users.pluck(:user_id)
      notifications = Notification.where("notification_type = 'team_card_share' AND user_id IN (?) AND app_deep_link_id = ? ", team_users_ids , self.card_id)
      notifications.destroy_all if notifications.present?
    end
  end
end

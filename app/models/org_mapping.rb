class OrgMapping < ActiveRecord::Base
  validates_presence_of :parent_organization_id
  validates_presence_of :child_organization_id
  validates_uniqueness_of :child_organization_id, scope: :parent_organization_id
end
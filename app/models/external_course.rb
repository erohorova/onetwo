# == Schema Information
#
# Table name: external_courses
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  description    :text(65535)
#  image_url      :string(255)
#  course_url     :string(255)
#  integration_id :integer
#  course_key     :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class ExternalCourse < ActiveRecord::Base
  belongs_to :integration

  has_many :users_external_courses
  has_many :users, through: :users_external_courses
  validates :name, :integration_id, :course_url, :course_key, presence: true
end

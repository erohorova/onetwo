# == Schema Information
#
# Table name: structures
#
#  id              :integer          not null, primary key
#  creator_id      :integer          not null
#  organization_id :integer          not null
#  parent_id       :integer          not null
#  parent_type     :string(20)       not null
#  display_name    :string(100)
#  context         :integer          not null
#  slug            :string(255)      not null
#  enabled         :boolean          default(FALSE)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Structure < ActiveRecord::Base
  include EventTracker

  # SLUG must one of ::
  #   1. When context is `discover`:
  #     a. discover-cards
  #     b. discover-videos
  #     c. discover-users
  #     d. discover-channels
  #     e. discover-custom-cards
  #   2. When context is `channel`:
  #     a. channel-cards
  #     b. channel-videos
  #     c. channel-users
  #     d. channel-pinned-cards
  #     e. channel-custom-cards

  belongs_to :creator, class_name: 'User'
  belongs_to :organization
  belongs_to :parent, polymorphic: true
  has_many   :structured_items, -> { order(position: :asc) }, dependent: :destroy

  scope :active, -> { where(enabled: true) }

  validates :context, :creator, :slug, presence: true

  SCOPES = { channel: 0, discover: 1 }.freeze
  DISCOVER_CAROUSELS =  {
                         'discover-cards': 'card',
                         'discover-channels': 'channel',
                         'discover-users': 'user'
                        }.with_indifferent_access.freeze

  after_commit :flush_cache
  before_destroy :remove_from_org_config, if: :org_carousel?
  after_commit :update_org_config, on: :update, if: :org_carousel?

  attr_accessor :current_context, :current_slug

  event_record_if_lambda = -> (record) { record.should_record_metric? }
  event_job_args_lambda = -> (record) {
    case record.parent_type
    when "Organization"
      [{structure: :itself}]
    when "Channel"
      [{structure: :itself, channel: :parent}]
    end
  }

  configure_metric_recorders(
    default_actor: :creator,
    trackers: {
      on_create: {
        if_block: event_record_if_lambda,
        job_args: event_job_args_lambda,
        exclude_job_args: [:org],
        event: 'channel_carousel_created'
      },
      on_edit: {
        if_block: event_record_if_lambda,
        job_args: event_job_args_lambda,
        exclude_job_args: [:org],
        event: 'channel_carousel_edited'
      },
      on_delete: {
        if_block: event_record_if_lambda,
        job_args: event_job_args_lambda,
        exclude_job_args: [:org],
        event: 'channel_carousel_deleted'
      }
    }
  )

  def current_context=(value)
    self.context = SCOPES[value.to_sym]
  end

  def current_slug=(value)
    _context = SCOPES.key(self.context)

    if self.context.present?
      self.slug = [_context, value].join('-')
    else
      self.errors.add(:slug, 'Context is required')
    end
  end

  def should_record_metric?
    parent_type.in? %w{Organization Channel}
  end

  def org_carousel?
    parent_type == "Organization"
  end

  def users
    User
      .joins("INNER JOIN structured_items on structured_items.entity_type = 'User' and structured_items.entity_id = users.id")
      .where("structured_items.structure_id = ?", self.id)
      .not_suspended
  end

  def update_entity_and_config(org: )
    update_attribute(:enabled, false)
    update_config(org, action: 'update') if org_carousel?
  end

  def update_org_config
    update_config(organization, action: 'update')
  end

  def remove_from_org_config
    update_config(organization, action: 'destroy')
  end

  def update_config(org, action: 'update')
    org_customization_config = org.configs.find_by(name: "OrgCustomizationConfig")
      parsed_value = org_customization_config&.parsed_value
      if parsed_value.present?
        _key = "discover/carousel/customCarousel/#{Structure::DISCOVER_CAROUSELS[self.slug]}/#{self.id}"

        if parsed_value["discover"][_key].present?
          if action == 'destroy'
            parsed_value["discover"].delete(_key)
          elsif action == 'update'
            parsed_value["discover"][_key]["visible"] = self.enabled
          end
        elsif action == 'update' # for new custom carousels
          parsed_value["discover"][_key] = {id: self.id, defaultLabel: self.display_name,
            index: 0, visible: self.enabled}
        end

        org_customization_config.value = ActiveSupport::JSON.encode(parsed_value)
        org_customization_config.save
    end
  end


  private

  def flush_cache
    Rails.cache.delete "org-#{organization_id}-structures"
    Rails.cache.delete "org-#{organization_id}-structure-entities"
  end
end

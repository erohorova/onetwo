# == Schema Information
#
# Table name: team_level_metrics
#
#  id                        :integer          not null, primary key
#  organization_id           :integer          not null
#  team_id                   :integer          not null
#  smartbites_consumed       :integer          default(0)
#  time_spent                :integer          default(0)
#  smartbites_liked          :integer          default(0)
#  smartbites_comments_count :integer          default(0)
#  smartbites_created        :integer          default(0)
#  period                    :string(191)
#  offset                    :integer
#

class TeamLevelMetric < ActiveRecord::Base
  include MetricsAggregator

  belongs_to :team
  belongs_to :organization

  validates :team, presence: true, uniqueness: {scope: [:period, :offset]}
  validates :organization, presence: true

  before_validation on: :create do
    if self.organization_id.nil?
      self.organization_id = team.try :organization_id
    end
  end

  create_increment_methods([:smartbites_created, :smartbites_consumed, :smartbites_liked, :smartbites_comments_count, :time_spent])
  create_getter_methods([:smartbites_created, :smartbites_consumed, :smartbites_liked, :smartbites_comments_count, :time_spent])
  class << self
    def get_team_metrics_over_span(team_id:, period: , begin_offset: , end_offset: )
      data = where(team_id: team_id, period: period)
      data = data
        .where("offset >= (:begin_offset) AND offset <= (:end_offset)", begin_offset: begin_offset, end_offset: end_offset)
        .select("sum(smartbites_consumed) as smartbites_consumed, sum(smartbites_created) as smartbites_created")
        .group(:team_id)
        .first
      Hash[[:smartbites_created, :smartbites_consumed].map{|k| [k, data.try(k).to_i]}]
    end

    def get_team_drill_down_data_for_timerange(organization_id: , team_id: , period: , timerange: )
      drill_down_period_and_offsets = PeriodOffsetCalculator.drill_down_period_and_offsets_for_timerange(period: period, timerange: timerange)
      data = get_team_data_for(organization_id: organization_id, team_id: team_id, period: drill_down_period_and_offsets.first, offsets: drill_down_period_and_offsets.last, timerange: timerange)
    end

    def get_team_data_for(organization_id: , team_id: , period: , offsets:, timerange: )
      # drill_down_period_and_offsets = PeriodOffsetCalculator.drill_down_period_and_offsets_for_timerange(period: period, timerange: timerange)
      data = where(team_id: team_id, period: period, offset: offsets)

      # smartbites created and smartbites consumed
      data = data
        .select("offset, sum(smartbites_consumed) as smartbites_consumed, sum(smartbites_created) as smartbites_created")
        .group(:offset)
        .index_by(&:offset)
      data = Hash[[:smartbites_created, :smartbites_consumed].map{|k| [k, offsets.map{|offset| data[offset].try(k).to_i}]}]

      # average session, active users and engagement index at the org level
      users_data = UserLevelMetric.get_organization_data_for(
        organization_id: organization_id,
        team_id: team_id,
        period: period,
        offsets: offsets)

      users_data = Hash[[:active_users, :average_session].map{|k| [k, offsets.map{|offset| users_data[offset].try(k).to_i}]}]
      data.merge({average_session: users_data[:average_session], active_users: users_data[:active_users]})
    end
  end
end

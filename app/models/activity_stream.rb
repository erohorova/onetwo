# frozen_string_literal: true

# == Schema Information
#
# Table name: activity_streams
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  organization_id :integer
#  action          :string(191)
#  streamable_type :string(191)
#  streamable_id   :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  comments_count  :integer          default(0)
#  votes_count     :integer          default(0)
#

class ActivityStream < ActiveRecord::Base
  acts_as_paranoid

  include Votable
  #Can't use symbols because the inclusion validation below errors out.
  ACTIONS = %w[
    created_card            created_pathway
    created_livestream      upvote
    smartbite_uncompleted   comment
    smartbite_completed     created_journey
    changed_author
  ].freeze
  ACTION_CREATED_CARD = 'created_card'
  ACTION_CREATED_PATHWAY = 'created_pathway'
  ACTION_CREATED_JOURNEY = 'created_journey'
  ACTION_CREATED_LIVESTREAM = 'created_livestream'
  ACTION_CHANGED_AUTHOR = 'changed_author'
  ACTION_UPVOTE = 'upvote'
  ACTION_COMMENT = 'comment'
  ACTION_SMARTBITE_COMPLETED = 'smartbite_completed'
  ACTION_SMARTBITE_UNCOMPLETED = 'smartbite_uncompleted'

  belongs_to :user
  belongs_to :organization
  belongs_to :card
  belongs_to :streamable, polymorphic: true #[Card, VideoStream, CardsUser(card upvote), Vote, Comment]
  has_many :votes, as: :votable, dependent: :destroy
  has_many :comments, as: :commentable, dependent: :destroy

  validates :action, inclusion: { in: ActivityStream::ACTIONS }
  validates_presence_of :streamable
  after_commit :create_xapi_activity, on: [:create, :update]

  def linkable_item
    return nil if streamable.nil?
    if action == ACTION_UPVOTE
      if streamable.votable.try(:hidden)
        Card.find_by(id: streamable.votable.pack_ids.first)
      else
        return streamable.votable
      end
    elsif action == ACTION_COMMENT
      return streamable.commentable
    elsif action.in? [ACTION_SMARTBITE_COMPLETED, ACTION_SMARTBITE_UNCOMPLETED]
      return streamable.completable
    else action
      return streamable
    end
  end

  def create_xapi_activity
    if self.organization.xapi_enabled
      XapiActivity.create_from_activity_stream(self)
    end
  end

  def org
    streamable.try(:organization)
  end

  def activity_card
    if action.in? ["assignment_completed", "assignment_uncompleted"]
      return linkable_item&.assignable
    elsif streamable_type == 'VideoStream'
      return linkable_item&.card
    elsif action == 'upvote' && linkable_item.try(:type) == 'IrisVideoStream'
      return linkable_item&.card
    else
      linkable_item
    end
  end

  def can_comment? user
    true
  end

  def remove_team_feed_entry
    is_comment = action == 'comment'
    is_feed_comment = streamable.try(:commentable).class.in?([Team, Organization])
    # check type of activity_stream
    # could be deleted only Team and Organization comments
    unless is_comment && is_feed_comment
      errors.add(:activity_stream, 'Incorrect activity type')
      return false
    end
    # return errors if streamable not destroyed
    unless streamable.destroy
      errors.add(
        :streamable,
        'Failed to destroy a comment'
      )
      return false
    end
    # destroy self
    # We do not want to change the hard delete feature by addition of paranoia
    # in activity stream. Hence, used destroy_without_paranoia
    destroy_without_paranoia
  end
end

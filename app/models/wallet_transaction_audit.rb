class WalletTransactionAudit < ActiveRecord::Base

  serialize :order_data, JSON
  serialize :orderable_data, JSON
  serialize :user_data, JSON
  serialize :wallet_transaction_data, JSON

  validates :wallet_transaction_id, presence: true

  def self.create_audit!(wallet_transaction_id:, order_data:, orderable_data:, user_data:, wallet_transaction_data:)
    WalletTransactionAudit.create!(wallet_transaction_id: wallet_transaction_id,
                                   order_data: order_data,
                                   orderable_data: orderable_data,
                                   user_data: user_data,
                                   wallet_transaction_data: wallet_transaction_data)
  end
end

# == Schema Information
#
# Table name: structured_items
#
#  id           :integer          not null, primary key
#  structure_id :integer          not null
#  entity_id    :integer          not null
#  entity_type  :string(20)       not null
#  enabled      :boolean          default(FALSE)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  position     :integer
#

class StructuredItem < ActiveRecord::Base
  include EventTracker

  belongs_to :structure
  belongs_to :entity, polymorphic: true

  acts_as_list scope: :structure

  validates :structure, :entity, presence: true
  validates :structure_id, uniqueness: { scope: [:entity_id, :entity_type] }

  after_commit :flush_cache

  configure_metric_recorders(
    default_actor: lambda {|record| record.structure&.creator },
    trackers: {
      on_create: {
        if_block: lambda {|record| record.should_push_metric?},
        job_args: [{channel: :entity}, {structure: :structure}],
        exclude_job_args: [:org],
        event: 'channel_added_to_channel_carousel'
      },
      on_delete: {
        if_block: lambda {|record| record.should_push_metric?},
        job_args: [{channel: :entity}, {structure: :structure}],
        exclude_job_args: [:org],
        event: 'channel_removed_from_channel_carousel'
      }
    }
  )

  def self.fetch_by_entity(entity_id,entity_type,structure)
    find_by(entity_id: entity_id, entity_type: entity_type, structure: structure)
  end

  def should_push_metric?
    structure.should_record_metric? && entity_type == "Channel" && entity.present?
  end

  private

  def flush_cache
    Rails.cache.delete "org-#{structure.organization_id}-structure-entities"
  end

  def self.reorder_and_destroy(structured_items)
    structured_items.map(&:remove_from_list)
    structured_items.destroy_all
  end
end

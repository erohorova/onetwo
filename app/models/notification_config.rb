# == Schema Information
#
# Table name: notification_configs
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  user_id         :integer
#  configuration   :text(65535)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class NotificationConfig < ActiveRecord::Base

  include EventTracker

  belongs_to :organization
  belongs_to :user

  serialize :configuration

  around_update :record_change_event_for_org_config

  def set_config(notification_name, key, value)
    ensure_config
    configuration["#{notification_name}|#{key}"] = value
  end

  def get_config(notification_name, key)
    ensure_config
    configuration["#{notification_name}|#{key}"]
  end

  def enabled?(notification_name, medium, interval)
    get_config(notification_name, Notify.config_field_name(medium, interval))
  end

  def configurable?(notification_name)
    get_config(notification_name, 'user_configurable')
  end

  def set_enabled(notification_name, medium, interval)
    set_config(notification_name, Notify.config_field_name(medium, interval), true)
  end

  def set_disabled(notification_name, medium, interval)
    set_config(notification_name, Notify.config_field_name(medium, interval), false)
  end

  def set_configurable(notification_name, value)
    ensure_config
    set_config(notification_name, "user_configurable", value)
  end

  private

  def ensure_config
    self.configuration = {} if configuration.nil?
  end

  # Records event(s) to influx when an org's notification config is changed.
  def record_change_event_for_org_config(&blk)
    # Runs the update
    blk&.call

    # We only record the event for org config, where user_id is nil
    return if user_id || !organization

    # Only record an event if configuration attribute was changed
    configuration_changes = changes['configuration']
    return unless configuration_changes

    # we determine the diff between the old and new configuration
    old_config, new_config = configuration_changes
    return unless old_config && new_config
    changed_attrs = {}
    old_config.each do |key, old_val|
      new_val = new_config[key]
      unless old_val == new_val
        changed_attrs[key] = [old_val, new_val]
      end
    end

    # record events for each of the changed configuration attributes
    changed_attrs.each do |(changed_column, (old_val, new_val))|
      record_custom_event(
        event: "org_notification_settings_edited",
        job_args: [
          {
            org:            organization,
            changed_column: changed_column,
            old_val:        old_val,
            new_val:        new_val
          }
        ]
      )
    end
  end

end

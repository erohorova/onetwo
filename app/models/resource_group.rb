# == Schema Information
#
# Table name: groups
#
#  id                                 :integer          not null, primary key
#  name                               :string(255)      not null
#  description                        :text(16777215)
#  website_url                        :string(255)
#  created_at                         :datetime
#  updated_at                         :datetime
#  client_id                          :integer
#  client_resource_id                 :string(191)
#  type                               :string(255)
#  access                             :string(255)
#  users_count                        :integer          default(0)
#  course_data_url                    :text(16777215)
#  image_url                          :text(16777215)
#  details_locked                     :boolean          default(FALSE)
#  creator_id                         :integer
#  parent_id                          :integer
#  close_access                       :boolean          default(FALSE)
#  topic_id                           :integer
#  picture_file_name                  :string(255)
#  picture_content_type               :string(255)
#  picture_file_size                  :integer
#  picture_updated_at                 :datetime
#  is_private                         :boolean          default(FALSE)
#  start_date                         :datetime
#  course_term                        :string(255)
#  end_date                           :datetime
#  connect_to_social_mediums          :boolean          default(TRUE)
#  enable_mobile_app_download_buttons :boolean          default(TRUE)
#  share_to_social_mediums            :boolean          default(TRUE)
#  daily_digest_enabled               :boolean          default(FALSE)
#  forum_notifications_enabled        :boolean          default(TRUE)
#  language                           :string(255)      default("en")
#  is_promoted                        :boolean          default(FALSE)
#  is_hidden                          :boolean          default(FALSE)
#  course_code                        :string(255)
#  organization_id                    :integer
#

class ResourceGroup < Group

  has_many :private_groups, foreign_key: :parent_id

  has_many :enrolled_students, -> { student }, class_name: 'GroupsUser', foreign_key: 'group_id'

  validates :description, presence: true
  validates :creator, presence: true
  scope :visible, -> { where(is_hidden: false)}

  # We use the new format only if a user is passed
  def get_cache_course_structure(options = {})
    user = options[:user]

    if user.present?
      cache_key = "savannah-course-simple-#{self.client_resource_id}-#{user.id}"
    else
      # The old caching key that was not user-wise and used the instructor's token.
      cache_key = "savannah-course-#{self.client_resource_id}"
    end

    Rails.cache.fetch(cache_key, expires_in: 1.hour, unless_nil: true) do
      credential = valid_credential

      conn = Faraday.new(url: Settings.savannah_base_location)
      result = conn.get do |req|
        req.url "/api/offerings/#{self.client_resource_id}/course_blocks"
        req.headers['Content-Type'] = 'application/json'
        req.headers['Accept'] = 'application/json'
        req.headers['X-Api-Key'] = credential.api_key
        req.headers['X-Shared-Secret'] = credential.shared_secret
        req.headers['X-Savannah-App-Id'] = organization.savannah_app_id.to_s

        if user.present?
          req.headers['X-User-Email'] = user.email
          req.headers['X-Client-Name'] = self.app_name
        end
      end

      if (result.success?)
        { status: 'success', course_structure: JSON.parse(result.body) }.merge!(course_hashed_data)
      else
        log.error "Savannah Course API failed for group: #{self.id}"
        nil
      end
    end
  end

  def promote!
    update!(is_promoted: true)
  end

  def unpromote!
    update!(is_promoted: false)
  end

  def hide!
    update!(is_hidden: true)
  end

  def unhide!
    update!(is_hidden: false)
  end

  # alias method
  def title
    name
  end

  def snippet
    name
  end

  def get_app_deep_link_id_and_type
    [id, 'group']
  end

  private
    #TODO will update it
    def ensure_creator
      self.creator = self.organization.client.try(:user) unless creator
    end

    def course_hashed_data
      g_admins = self.admins.collect do |a|
        { name: a.name, image: a.avatar.url(:medium), picture: a.avatar.url(:medium) }
      end

      {
        start_date: "#{self.start_date.try(:to_date)}",
        course_description: self.description,
        course_organization: self.app_name,
        course_title: self.name,
        course_website_url: self.website_url,
        course_image: self.image_url,
        instructors: g_admins
      }
    end
end

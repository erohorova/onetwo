# == Schema Information
#
# Table name: cards
#
#  id                   :integer          not null, primary key
#  cards_config_id      :integer
#  group_id             :integer
#  author_first_name    :string(255)
#  author_last_name     :string(255)
#  author_picture_url   :text(16777215)
#  super_card_id        :integer
#  author_id            :integer
#  created_at           :datetime
#  updated_at           :datetime
#  is_public            :boolean          default(FALSE)
#  is_manual            :boolean          default(FALSE)
#  features             :text(16777215)
#  client_id            :integer
#  client_item_id       :string(191)
#  comments_count       :integer          default(0)
#  order_index          :integer          default(0)
#  slug                 :string(191)
#  impressions_count    :integer          default(0)
#  outbounds_count      :integer          default(0)
#  shares_count         :integer          default(0)
#  answers_count        :integer          default(0)
#  expansions_count     :integer          default(0)
#  plays_count          :integer          default(0)
#  hidden               :boolean          default(FALSE)
#  resource_id          :integer
#  organization_id      :integer
#  state                :string(191)
#  title                :text(65535)
#  message              :text(65535)
#  published_at         :datetime
#  is_official          :boolean          default(FALSE)
#  card_type            :string(255)
#  card_subtype         :string(255)
#  votes_count          :integer          default(0)
#  promoted_at          :datetime
#  ecl_id               :string(191)
#  ecl_metadata         :text(65535)
#  taxonomy_topics      :text(65535)
#  filestack            :text(65535)
#  bookmarks_count      :integer
#  deleted_at           :datetime
#  ecl_source_name      :string(255)
#  readable_card_type   :integer
#  provider             :string(255)
#  provider_image       :string(255)
#  user_taxonomy_topics :text(65535)
#  auto_complete        :boolean
#  duration             :integer
#  can_be_reanswered    :boolean
#

class Card < ActiveRecord::Base
  include Votable
  include EdcastSearchkickWrapper
  include Taggable
  include Mentionable
  include AttachmentWorthy
  include CardPack
  include AASM
  include Streamable
  include Queueable
  include Assignable
  include Assessable
  include CustomReorder
  include NotificationCleanup
  include Actionable
  include EventTracker
  include CustomCarousel

  extend FriendlyId

  has_paper_trail ignore: [:created_at, :updated_at]

  TYPE_PACK = 'pack'.freeze
  TYPE_VIDEO_STREAM = 'video_stream'.freeze
  TYPE_JOURNEY = 'journey'.freeze

  EXCLUDED_ATTRIBUTES_FROM_CLONNING = [
    :author_first_name, :author_last_name, :author_picture_url, :super_card_id, :author_id, :comments_count, :impressions_count, :ecl_id,
    :outbounds_count, :shares_count, :answers_count, :expansions_count, :plays_count, :organization_id, :published_at, :votes_count,
    :promoted_at, :bookmarks_count, :deleted_at, :user_taxonomy_topics, :ecl_metadata, :ecl_source_name, :resource_id
  ].freeze

  STATE_DRAFT = 'draft'.freeze
  STATE_PUBLISHED = 'published'.freeze
  STATE_PROCESSING = 'processing'.freeze
  STATE_ERROR = 'error'.freeze

  CLONE_SYNC_REQUIRED_ATTRIBUTES = [
    :message, :title, :card_type, :can_be_reanswered, :card_subtype, :filestack, :duration, :readable_card_type, :is_paid, :resource_id
  ].freeze

  # card types that will be included in user feeds and search/query results
  EXPOSED_TYPES = %w[media poll pack course].freeze
  EXPOSED_TYPES_V2 = %w[media poll pack course video_stream journey project].freeze
  CONTENT_TYPES = %w[video poll insight pathway article course video_stream journey project].freeze

  AUTO_COMPLETE_CARDS = %w[pack journey].freeze

  # basic poll card type is for backward compatibility
  #TODO: remove text from course
  SUB_TYPES = {
    video_stream: ['simple', 'role_play'],
    # new pack sub_type is used for journey section of cards
    pack: %w[simple journey curriculum],
    media: ['video', 'image', 'link', 'text', 'file', 'audio'],
    poll: ['video', 'image', 'link', 'text', 'basic', 'file'],
    course: ['link'],
    journey: %w[self_paced weekly],
    project: ['text']
  }.with_indifferent_access.freeze

  COMBINATIONS_FOR_STOCK_IMAGE = %w[project_text pack_simple journey_weekly journey_self_paced].freeze

  ADMIN_ALLOWED_STATES = ['archived', 'draft', 'new', 'processing', 'error', 'published'].freeze
  AUTHOR_ALLOWED_STATES = ['draft', 'processing', 'error', 'published'].freeze
  MEMBER_ALLOWED_STATES = ['processing', 'error', 'published'].freeze

  EVENT_CARD_SHARE = 'card_share'.freeze
  EVENT_NEW_CARD_SHARE = 'new_card_share'.freeze

  attr_accessor :skip_indexing, :topics, :is_carousel, :wallet, :current_user, :old_author_id,
                :ecl_update_required, :readable_card_type_name, :update_request_from_ecl, :channel_card_rank
  attr_accessor :inline_indexing #index asap and wait until it's done

  serialize :ecl_metadata, JSON
  serialize :taxonomy_topics, Array
  serialize :user_taxonomy_topics, Array
  serialize :filestack, Array

  belongs_to :organization
  belongs_to :author, class_name: 'User'
  belongs_to :client
  belongs_to :resource
  has_many :card_reportings
  has_many :trashed_card_reportings, -> { only_deleted }, class_name: 'CardReporting', foreign_key: :card_id
  has_many :cards_users, dependent: :delete_all
  has_many :comments, as: :commentable, :dependent => :destroy
  has_many :notifications, as: :notifiable, :dependent => :destroy
  has_many :daily_card_stats, :dependent => :destroy
  has_many :user_content_completions, as: :completable, dependent: :destroy
  has_many :orders, as: :orderable
  has_many :mdp_user_cards, dependent: :destroy

  # this dependent destroy doesnt delete channel. Rather channels_cards records, :)
  # https://github.com/rails/rails/issues/27567
  has_many :channels_cards
  has_many :channels, through: :channels_cards, dependent: :destroy

  has_many :curated_channels, -> {where(channels_cards: {state: ChannelsCard::STATE_CURATED})}, source: 'channel', through: :channels_cards
  has_many :quiz_question_options, as: :quiz_question, inverse_of: :quiz_question, dependent: :destroy
  has_one :quiz_question_stats, as: :quiz_question, dependent: :destroy
  has_many :quiz_question_attempts, as: :quiz_question, dependent: :destroy
  has_many :votes, as: :votable, dependent: :destroy
  has_many :bookmarks, as: :bookmarkable, dependent: :destroy
  has_many :xapi_activities, as: :object, dependent: :destroy
  has_one :video_stream, inverse_of: :card
  has_many :cards_ratings, dependent: :destroy
  has_many :pins, as: :object, dependent: :destroy
  has_many :assignments, as: :assignable, dependent: :destroy
  has_many :teams_cards,  dependent: :destroy
  has_many :shared_cards, dependent: :destroy
  has_one :card_badging, foreign_key: :badgeable_id
  has_many :leaps, dependent: :destroy
  has_many :activity_streams, dependent: :destroy
  has_many :prices, dependent: :destroy, :inverse_of => :card
  has_many :card_subscriptions
  has_many :subscribed_users, source: 'user', through: :card_subscriptions

  has_many :shared_with_teams, through: :shared_cards, source: :team

  has_many :card_user_shares, dependent: :destroy
  has_many :users_with_access, through: :card_user_shares, source: :user

  has_many :card_user_permissions, dependent: :destroy
  has_many :users_with_permission, through: :card_user_permissions, source: :user

  has_many :card_team_permissions, dependent: :destroy
  has_many :teams_with_permission, through: :card_team_permissions, source: :team

  has_one :project, inverse_of: :card, dependent: :destroy
  has_many :journey_pack_relations, foreign_key: :cover_id
  has_many :career_advisor_cards, dependent: :destroy
  has_many :career_advisors, through: :career_advisor_cards

  has_many :users_cards_managements, dependent: :destroy
  has_many :managing_users, through: :users_cards_managements, source: :user
  has_one  :scorm_detail
  has_one :card_metadatum

  belongs_to :parent_card, class_name: 'Card', foreign_key: :super_card_id
  has_many :cloned_cards, class_name: 'Card', foreign_key: :super_card_id

  acts_as_streamable on: :create
  acts_as_paranoid

  # Validations ensure that
  # (a) Org is present
  # (b) Author belongs to the org
  #
  # Before validation hooks to
  # (a) associate card to org through the user if org not explicitly associated.
  # This is to avoid touching all parts of the sytem where cards are currently
  # created
  before_validation on: :create do
    if self.organization_id.nil?
      self.organization_id = author.try :organization_id
    end
  end

  # backward compatibility
  before_validation :set_message_from_title

  validates :organization, :message, presence: true
  validate :validate_poll_can_be_updated, if: proc { |attrs| attrs['card_type'] == 'poll' }
  validate :user_in_org
  validates :card_type,
            inclusion: {
              in: %w[media poll pack course video_stream journey project],
              message: "%{value} is not a valid card type"
            }
  validate :validate_card_subtype
  validates :language,
            inclusion: {
              in: UserProfile::VALID_LANGUAGES.values
            }, allow_blank: true

  after_save :update_topics
  after_commit :reindex_card, unless: :skip_indexing
  # Enqueue to followers when the card state changes to publish
  after_commit :enqueue_for_followers_on_publish
  after_commit :generate_poll_notification, on: :create, if: proc {|attrs|  attrs[:card_type] == 'poll' && !attrs[:hidden]}
  after_commit :update_mentions, on: [:create, :update]
  after_commit :remove_assignments, on: :destroy

  after_commit :update_video_stream_state, on: :update

  # ================================================
  # NOTE:
  #   Commented since we have a running bug that deletes files from filestack unknowingly.
  #   Uncomment when we figure and fix this issue.
  # after_commit :remove_from_filestack, on: :update
  # ================================================
  before_save :update_file_resources_from_filestack
  before_save :sanitize_title_description
  before_save :strip_emoji_title_message
  before_create :set_project_card

  after_commit :recalculate_card_metadata, on: :update
  before_update :set_promoted_timestamp
  after_update :record_promote_event

  before_destroy :remove_clc
  before_destroy :create_deleted_document
  before_destroy :remove_card_from_queue
  before_destroy :delete_ecl_card
  after_destroy :notify_user
  after_destroy :handle_card_deletion
  before_destroy :mark_learning_queue_items_as_deleted
  before_destroy :remap_locks, if: :remap_lock?

  after_initialize :set_card_type_and_subtype, if: :new_record?
  before_save :set_card_subtype
  before_save :set_autocomplete, if: :should_be_autocomplete?
  before_create :set_published_at, if: :cloned_card?

  before_create :set_draft_state, if: :should_be_draft?
  after_commit :create_relation_and_propagate_card_to_ecl, on: :create
  after_commit :propagate_card_to_ecl, on: :update
  after_commit :change_author, on: :update, if: :author_changed?
  after_commit :sync_clone_cards, on: :update

  friendly_id :slug_candidates, use: [:slugged, :history, :scoped], scope: :organization_id

  accepts_nested_attributes_for :tags, allow_destroy: true
  accepts_nested_attributes_for :quiz_question_options, allow_destroy: true#, reject_if: proc {|attrs| attrs[:label].blank? }
  accepts_nested_attributes_for :project, update_only: true
  accepts_nested_attributes_for :video_stream, update_only: true
  accepts_nested_attributes_for :card_badging, update_only: true
  accepts_nested_attributes_for :prices, update_only: true, allow_destroy: true
  accepts_nested_attributes_for :card_metadatum, allow_destroy: true
  accepts_nested_attributes_for :resource

  scope :published, -> {where state: 'published'}
  scope :system_generated, -> { where(author_id: nil) }

  scope :pathways, -> { where(card_type: 'pack') }
  scope :journeys, -> { where(card_type: 'journey') }
  scope :published_pathways, -> {where("cards.card_type = ? AND cards.state = ? AND cards.card_subtype in (?)", "pack", "published", ["simple", "curriculum"])}
  scope :published_journeys, -> {where(card_type: 'journey', state: 'published')}
  scope :polls, -> { where(card_type: 'poll') }
  scope :visible_cards, -> {where(hidden: false)}
  scope :cards_for_channel_id, ->(channel_id) { distinct.joins(:channels_cards).where(channels_cards: { channel_id: channel_id} ) }
  scope :curated_cards_for_channel_id, ->(channel_id) { cards_for_channel_id(channel_id).where(channels_cards: { state: ChannelsCard::STATE_CURATED})}
  scope :by_created_at, -> { order('created_at DESC') }
  scope :video_streams, -> { where(card_type: "video_stream")}
  scope :courses, -> { where(card_type: "course")}
  scope :scheduled_streams, -> {self.joins(:video_stream).where("video_streams.status = 'upcoming'")}
  scope :smartbites, -> {where(card_type: ['media', 'poll'])}
  scope :user_created, -> { where.not(author_id: nil) }
  scope :processing, -> { where(state: 'processing') }
  scope :error, -> { where(state: 'error') }
  scope :get_public_cards, -> { where(is_public: true) }

  # STATE -> one of started or completed. Not to get confused with cards
  # own state column
  scope :user_completed_cards_with_state,
          -> (state, card_type, user_id) {
             joins(:user_content_completions)
             .where(user_content_completions: {state: state, user_id: user_id}, card_type: card_type)}
  scope :search_by, -> (query) { where('cards.title like :q OR cards.message like :q', q: "%#{query}%") }
  scope :filtered_by_language, -> (language) { where('language = ? OR language IS NULL', language ) }
  scope :excluding_default_org, -> {where.not(organization_id: Organization.default_org.id)}

  # NOTE: Initial state for UGC is published
  # Use ! sign to actully persist state of the object
  # Caution: AASM provides a lot of callbacks, but these do
  # not ensure callbacks are run after a db commit.
  # for example, callbacks run even when you do card.publish, but database
  # record still has the old state, of course. card.publish! actually
  # performs a database update
  aasm column: 'state', :whiny_transitions => false do
    state :new, initial: true
    state :draft
    state :processing
    state :error
    state :published
    state :archived

    event :publish do
      before do
        self.published_at = Time.now.utc
        inline_indexing = true
      end

      after do
        push_card_published_influx_event
        add_to_activity_stream(self, self.activity_initiator, ActivityStream::ACTION_CREATED_PATHWAY) if pack_card?
        add_to_activity_stream(self, self.activity_initiator, ActivityStream::ACTION_CREATED_JOURNEY) if journey_card?
        add_to_activity_stream(self, self.activity_initiator, ActivityStream::ACTION_CREATED_CARD) if is_scorm_card?
      end

      transitions from: [:new, :draft, :processing, :archived], to: :published do
        guard do
          can_publish?
        end
      end
    end

    event :archive do
      after do
        activity_streams.destroy_all
        log.info "Calling ContentsBulkDequeue Job via (remove_card_from_queue) event archive for id #{self.id}"
        remove_card_from_queue
      end
      transitions from: [:new, :draft, :processing, :error, :published], to: :archived
    end

    event :error do
      after do
        log.info "Scorm upload failed for card: #{id}"
      end
      transitions from: [:new, :draft, :processing], to: :error
    end
  end

  configure_metric_recorders(
    default_actor: :author,
    trackers: {
      on_create: {
        event: 'card_created', # NOTE: THIS GETS CHANGED TO card_created_virtual
                               # BY THE RECORDER IF card_author_id IS NULL
      },
      on_edit: {
        event: 'card_edited',
      },
      on_delete: {
        event: 'card_deleted',
      }
    }
  )

  #============================================== UPDATE FILERESOURCE STARTS =====================

  # Skips file resources for:
  #   1. SCORM
  #   2. IMAGE (Poll and Image cards )
  #   3. AUDIO
  #   4. VIDEO (uploaded)
  #   5. FILES (json, html, rtf, ppt, pdf, xls, doc, csv, text)
  #   6. PATHWAY
  #   7. JOURNEY
  # .. In short, for every card.

  def skip_file_resources?
    self.has_scorm_filestack? || self.image_card? || self.audio_card? || self.video_card? || self.file_card?
  end

  def update_file_resources_from_filestack
    # begin
    #   return if self.filestack.blank? || skip_file_resources?
    #   log.info "Updating file resource for card: #{self.id}"

    #   self.filestack.each do |filestack_obj|
    #     filestack_obj = filestack_obj.with_indifferent_access
    #     file_resource = file_resources.where(attachable: self, filestack_handle: filestack_obj[:handle]).first_or_initialize
    #     file_resource.attachment = filestack_obj['url']
    #     file_resource.filestack_filename = filestack_obj[:filename] || filestack_obj[:name]
    #     file_resource.filestack = filestack_obj
    #     file_resource.user = self.author
    #     file_resource.save
    #   end
    #   # delete the previous unused file resource data of card on update
    #   file_resources.where.not(filestack_handle: self.filestack.map{ |file| file[:handle]}).destroy_all
    # rescue Exception => e
    #   log.warn("Updating file resource for card #{self.id} failed, message: #{e.message}")
    # end
  end

  # NOTE:
  #   Commented since we have a running bug that deletes files from filestack unknowingly.
  #   Uncomment when we figure and fix this issue.
  # NO-OP:
  def remove_from_filestack
    # if self.previous_changes.include?('filestack')
    #   old_filestack_contents = self.previous_changes['filestack'][0]
    #   old_handles = old_filestack_contents.map { |file| file[:handle] }
    #   stock_images = CARD_STOCK_IMAGE
    #   old_handles.each do |old_handle|
    #     unless stock_images.any? {|h| h[:handle] == old_handle }
    #       RemoveFileFromFilestackJob.perform_later(handle: old_handle, card_id: self.id)
    #     end
    #   end
    # end
  end
  #============================================== UPDATE FILERESOURCE ENDS =====================

  def self.merge_wallet_response(results:, user:)
    if user.organization.wallet_enabled?
      Wallet::ContentFormatter.new(user: user, results: results, params: { content_type: 'card' }).format
    end
  end

  def scorm_course_id
    "#{self.organization.host_name}#{self.id}"
  end

  def sanitize_message
    card_subtype == 'text' ? message : message.try(:clean)
  end

  def upload_to_scorm
    return if self.filestack.blank? || !self.filestack.first[:scorm_course]
    UploadToScormJob.perform_later(self.id, scorm_course_id)
  end

  def readable_card_type_name=(value)
    write_attribute(:readable_card_type, Card.readable_card_type(value))
  end

  def self.readable_card_type(readable_card_type_name)
    readable_card_types = Organization.default_org.fetch_readable_card_types_configs || {}
    readable_card_types[readable_card_type_name.try(:underscorize)]
  end

  def self.readable_card_type_name(readable_card_type)
    readable_card_types = Organization.default_org.fetch_readable_card_types_configs || {}
    readable_card_types.key(readable_card_type)
  end

  def card_type_display_name
    Card.readable_card_type_name(readable_card_type).humanize.titleize if readable_card_type
  end

  def voters_names
    User.where(id: self.votes.pluck(:user_id)).map {|u| u.full_name}
  end

  def share_card(user_ids: [], team_ids: [], user: )
    user_team_ids = if user.is_org_admin?
      Team.where(organization_id: user.organization_id, id: team_ids).pluck(:id)
    else
      user.teams.where(id: team_ids).pluck(:id)
    end

    new_team_ids = team_ids - self.shared_with_team_ids

    if user_team_ids.length < team_ids.length
      log.warn "The user is not a part of all groups: #{team_ids - user_team_ids.map(&:to_s)}"
    end
    user_team_ids.each do |team_id|
      shared_card = SharedCard.find_or_create_by(team_id: team_id, card_id: id, user_id: user.id)
      unless shared_card.persisted?
        log.error "Card #{id} could not be shared by user #{user.id} to #{team_id}: #{shared_card.errors.full_messages.join(',')}"
      end
    end

    user_ids.each do |user_id|
      card_user_share = CardUserShare.where(user_id: user_id, card_id: id).first_or_initialize
      card_user_share.shared_by_id = user.id
      card_user_share.save
      unless card_user_share.persisted?
        log.error "Card #{id} could not be shared to user #{user.id} : #{card_user_share.errors.full_messages.join(',')}"
      end
    end

    if new_team_ids.present?|| user_ids.present?
      send_card_create_or_share_notification(event: Card::EVENT_CARD_SHARE,
        team_ids: new_team_ids, share_by_id: user.id, user_ids: user_ids,
        permitted_user_ids: users_with_permission_ids,
        permitted_team_ids: teams_with_permission_ids)
    end
  end

  def unshare_card(user_ids: [], team_ids: [])
    shared_cards.where("team_id IN (?)",team_ids).destroy_all
    card_user_shares.where("user_id IN (?)",user_ids).destroy_all
  end

  def slug_candidates
    uuid = "card-#{SecureRandom.uuid}"

    current_title = title && title[0..59]
    candidates = [current_title]

    #NOTE: New org creation and creating feed based on job role goes through several transactions
    # Feed creation in above step fetches content from several sources and create card for the org
    # Create and modify tags(type) ending up in deadlocking the transaction.

    # if title is not unique, add all permutations of tags
    # tag_names = tags.pluck(:name)
    # tag_names.length.times do |i|
    #   tag_names.permutation(tag_names.length-i).each do |p|
    #     candidates << [current_title, *p]
    #   end
    # end

    # if still not unique, incrementally add some message words
    msg = ActionView::Base.full_sanitizer.sanitize message
    if msg
      msg_words = msg[0..50].split.take(10)
      msg_words.length.times do |i|
        candidates << [current_title, *msg_words.take(i+1)]
      end
    end

    # add uuid versions as last resorts
    candidates << [current_title, uuid]
    candidates << uuid

    return candidates
  end

  def update_channel_pins(old_channel_ids, new_channel_ids)
    deleted_channel_ids = (old_channel_ids - new_channel_ids) if old_channel_ids && new_channel_ids
    pins.where(pinnable_id: deleted_channel_ids, pinnable_type: "Channel").destroy_all if deleted_channel_ids.present?
  end

  def non_curated_channel_ids
    channels_cards.where(state: ChannelsCard::STATE_NEW).pluck(:channel_id)
  end

  def rejected_from_channels(channel_ids)
    channels_cards.where(channel_id: channel_ids, state: ChannelsCard::STATE_SKIPPED)
  end

  def repost_to_channel(channel_ids)
    rejected_from_channels(channel_ids).each do |channels_card|
      channels_card.repost
      unless channels_card.save
        log.info "Error while reposting card_id: #{channel_card.card_id} to channel_id: #{channels_card.channel_id}"
      end
    end
  end

  def fetch_card_reportings
    card_reportings.with_deleted.includes(:user)
  end

  def reported_by?(user)
    CardReporting.where(card: self, user: user).exists?
  end

  # attempt to replace temporary slug with a better one
  def should_generate_new_friendly_id?
    return true if super
    has_default = /card-([a-z0-9]+\-){4}[a-z0-9]+\z/ =~ slug
    return has_default && title.present?
  end

  # to pick cards from a channel with most comments and recent
  # 1. cards with the most conversation get featured
  # 2. If two or more cards have the same number of conversation all get featured
  # 3. If the limit is hit and the counts are the same, then the most recent ones should surface
  def self.trending_channel_cards(channel_id)
    curated_cards_for_channel_id(channel_id)
      .select("cards.*, channels_cards.created_at")
      .where("cards.comments_count > 0")
      .order("cards.comments_count DESC, channels_cards.created_at DESC")
  end

  def post_to_channel(channel_ids)
    channels << Channel.find(channel_ids)
  end

  def remove_from_channel(channel_ids)
    channels_cards.where(channel_id: channel_ids).destroy_all
  end

  # force a new slug
  def regenerate_slug
    update(slug: nil)
  end

  # prevent db column overrun
  def normalize_friendly_id(text)
    super[0..190]
  end

  def self.sortable_fields
    ['tags.name', 'title']
  end

  def self.exact_match_fields
    ['tags.name', 'state', 'card_type', 'card_template', 'author.handle']
  end

  def self.searchable_fields
    {
      'tags.name' => {
        boost: 15,
      },
      'author.name' => {
        boost: 5
      },
      'author.handle' => {
        boost: 5
      },
      'title' => {
        boost: 10,
      },
      'message' => {
        boost: 1
      }
    }
  end

  edcast_searchkick

  class << self
    def do_post_processing(results)
      if !results.nil? && !results.response.blank?
        #This is for backward compatibility. API V2 has load cards
        # earlier versions returns hashie mash object

        return results if results.first.instance_of?(Card)
        user_ids = results.map(&:author_id)
        users = User.where(id: user_ids).group_by(&:id)

        results.each { |result|
          if result.author_id.nil?
            next
          end
          user = users[result.author_id].try(:first)

          if user
            result.author!.id = user.id
            result.author!.name = user.name
            result.author!.first_name = user.first_name
            result.author!.last_name = user.last_name
            result.author!.photo = user.photo(:medium)
            result.author!.photo_medium = user.photo(:medium)
            result.author!.anonimage!.url = user.anonimage.url
            result.author!.handle = user.handle
          end

        }
      end
      results
    end

    def search_with_post_processing(*args)

      # override the default search method provided by searchkick
      # intercept the results here, which only has user_id and add
      # user to each result as a Hashie::Mash
      # this override in some ways breaks the default search method
      # since activerecord wont allow to write hashiemesh into it.
      # to keep search method working separately, always use load: false

      results = search_without_post_processing(*args)


      do_post_processing(results)
    end
    alias_method_chain :search, :post_processing

    def from_url_path(url_path, organization)
      # Assume that the url_path starts with a /,
      # so remove the leading /
      url_path = url_path.gsub(/^\//, '')
      url_path = url_path.gsub(/\/$/, '')
      # If the path does not start with either "insights", "pathways",
      # or "video_streams", boot
      parts = url_path.split('/')
      unless %w(insights pathways video_streams).include? parts[0]
        return nil
      end
      # The second segment contains the slug
      if parts.count < 2
        return nil
      end

      # Get the slug from the second segment
      slug = parts[1]

      # Check if it is ECL card
      if is_ecl_id?(slug)
        ecl_id = get_ecl_id(slug)
        EclApi::CardConnector.new(ecl_id: ecl_id, organization_id: organization.id).card_from_ecl_id
      else
        # If it's not an ECL card
        begin
          Card.where(organization: organization).friendly.find(slug)
        rescue ActiveRecord::RecordNotFound
          nil
        end
      end
    end
  end

  def _taggable_fields
    [title, message]
  end

  def video_stream?
    card_type == 'video_stream'
  end

  def text?
    card_subtype == 'text'
  end

  def reindex_card
    if inline_indexing
      IndexJob.perform_now(self.class.name, self.id)
      self.class.searchkick_index.refresh
    else
      reindex_async
    end
  end

  def reindex_async
    IndexJob.perform_later(self.class.name, self.id)
  end

  def snippet
    markdown = Redcarpet::Markdown.new(Redcarpet::Render::StripDown)
    markdown.render(title? ? title : message).gsub(/\n/,'').truncate(200, separator: ' ')
  end

  def snippet_html
    snippet_text = (title? ? title : message).gsub(/\n/,'').gsub('_', '*').gsub('++', '_')
    markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML, strikethrough: true, underline: true, escape_html: true)
    markdown.render(snippet_text).truncate(200, separator: ' ')
  end

  def self.index_credentials
    return ENV['ELASTICSEARCH_CARDS_UID'], ENV['ELASTICSEARCH_CARDS_PWD']
  end

  def self.index_name
    ENV['ELASTICSEARCH_CARDS_INDEXNAME'] || (Rails.env.development? ? 'cards_development' : 'cards_test')
  end

  def self.index_type
    'card'
  end

  def self.index_location
    ENV['ELASTICSEARCH_CARDS_URL'] || 'localhost:9200'
  end

  def author_name
    author.try(:name) || ''
  end

  def associated_correct_option
    if poll_card?
      QuizQuestionOption.find_by(quiz_question: self, is_correct: true).try(:label)
    end
  end

  def pack_quizzes_attempts_correct?(user:)
    return unless pack_card?

    poll_cards = self.associated_poll_cards
    poll_cards.all? { |card| card.associated_correct_option == QuizQuestionAttempt.attempted_option(card_id: card.id, user: user).try(:label) }
  end

  def teams
    TeamAssignment.where(assignment_id: assignments.pluck(:id)).map(&:team)
  end

  def file_card?
    card_type == 'media' && card_subtype == 'file'
  end

  def image_card?
    card_subtype == 'image'
  end

  def audio_card?
    card_type == 'media' && card_subtype == 'audio'
  end

  def video_card?
    card_type == 'media' && card_subtype == 'video'
  end

  def poll_card?
    card_type == "poll"
  end

  def course_card?
    card_type == "course"
  end

  def project_card?
    card_type == 'project'
  end

  def pack_card?
    card_type == 'pack'
  end

  def remap_lock?
    # will invoke for non pack/journey cards
    %w[pack journey].exclude?(card_type) && CardPackRelation.where('from_id = ? AND locked = 1 AND to_id IS NOT NULL', id).any?
  end

  def journey_card?
    card_type == 'journey'
  end

  def standalone_leap
    self.leaps.find_by(pathway_id: nil)
  end

  def pathway_leaps
    self.leaps.where.not(pathway_id: nil)
  end

  def should_be_draft?
    %w[pack journey].include?(card_type)
  end

  def media_card?
    card_type == 'media'
  end

  def promote
    update(is_official: true)
  end

  def unpromote
    update(is_official: false)
  end

  def author_changed?
    self.previous_changes.keys.include?("author_id")
  end

  def change_author
    TransferOwnershipJob.perform_later(self.id, {old_author_id: self.old_author_id, actor_id: self.current_user.id})
  end

  def ugc?
    author_id.present?
  end

  def cloned_card?
    super_card_id.present?
  end

  def ecl_state
    return "active" if published?
    return "archived" if archived? || deleted?
    "drafted"
  end

  def search_data
    as_json.except('filestack', 'deleted_at','auto_complete', 'is_paid', 'can_be_reanswered').merge(indexing_data)
  end

  # Deprecated, just hard coded for backward compatibility
  def action_data
    {
      'student' => {
        'standard' => {
          'upvote' => {
            'name' => 'upvote',
            'reverse' => { 'name' => 'unupvote' }
          },
          'bookmark' => {
            'name' => 'bookmark',
            'reverse' => { 'name' => 'unbookmark' }
          }
        }
      }
    }
  end

  def indexing_data
    ret = {}
    ret['actions'] = action_data # Deprecated
    ret['author_name'] = author_name
    ret['comments'] = comments.map(&:search_data)
    ret['mentioned_users'] = mentioned_users.map &:capsule_data
    ret['channel_ids'] = channel_ids
    ret['pack_ids'] = pack_ids
    ret['channels'] = channels.as_json(only:[:id, :label])
    ret['curated_channel_ids'] =  curated_channels.pluck(:id)
    ret['packs'] = pack_ids
    ret['pack_cards'] = pack_cards_index_data

    ret['in_private_channel'] = channels.any?(&:is_private?)
    ret['liked_by'] = liked_by.map(&:mini_profile_data)
    ret['video_stream_id'] = video_stream.try(:id)
    ret['author_skill_level'] = card_metadatum&.level
    ret['average_rating'] = card_metadatum&.average_rating.to_i || 0
    ret.merge!(display_data)
    ret.merge!(access_control_data)
  end

  def display_data
    ret = {}
    ret['content'] = get_content_map #Deprecated
    ret['resource'] = resource.as_json
    ret['card_template'] = card_subtype
    ret['tags'] = tags.map{|tag| {'id'=>tag.id, 'name'=>"#{tag.name}"}}
    ret['comments_count'] = comments.count
    ret['like_count'] = votes_count
    ret['slug'] = slug

    if author
      # TODO root-level 'handle' needed for Search::CardSearch; should remove in favor of author capsule
      ret['handle'] = author.handle
      ret['author'] = author.mini_profile_data
      ret['author']['influencer'] = false
      ret['author']['roles'] = author.roles.collect(&:name)
      ret['author']['organization_role'] = author.organization_role
    end
    ret['file_resources'] = file_resources.as_json
    ret['quiz_question_options'] = quiz_question_options.as_json
    ret['quiz_question_stats'] = quiz_question_stats.as_json
    ret['quiz_question_attempts'] = quiz_question_attempts.as_json

    #handle images
    if first_file_resource = file_resources.images.first
      ret['small_image_url'] = first_file_resource.attachment.url(:small)
      ret['medium_image_url'] = first_file_resource.attachment.url(:medium)
      ret['large_image_url'] = first_file_resource.attachment.url(:large)
    elsif first_file_resource = resource && resource.file_resources.images.first
      ret['small_image_url'] = first_file_resource.attachment.url(:small)
      ret['medium_image_url'] = first_file_resource.attachment.url(:medium)
      ret['large_image_url'] = first_file_resource.attachment.url(:large)
    else

    end
    ret
  end

  def access_control_data
    ret = {}
    ret['users_with_access_ids'] = users_with_access.pluck(:id)
    ret['users_with_permission_ids'] = users_with_permission.pluck(:id)
    ret['teams_with_permission_ids'] = teams_with_permission.pluck(:id)
    ret['shared_with_teams_ids'] = shared_with_teams.pluck(:id)
    ret
  end

  def topics_label
    tags.pluck(:name)
  end

  def mobile_photo
    if first_file_resource = file_resources.images.first
      first_file_resource.attachment.url(:medium)
    else
      nil
    end
  end

  def pack_cards
    Card.where(id: (pack_relns || []).map(&:from_id) - [self.id])
  end

  def journey_pathways
    Card.where(id: (journey_relns || []).map(&:from_id) - [self.id])
  end

  def pathway_all_cards
    Card.where(id: card_pack_relations.map(&:from_id) - [self.id])
  end

  def journey_inner_cards_data
    section_card_ids = journey_relns.map(&:from_id)
    cards = Card.where(id: section_card_ids)
    journey_items = cards.pluck(:message, :title)
    cards.each do |c|
      journey_items << c.pack_cards.pluck(:message, :title)
    end
    journey_items.flatten.compact
  end

  #All Journey_ids of which a card is a part of.
  def journey_ids_for_card
    JourneyPackRelation.where(from_id: pack_ids).uniq.pluck(:cover_id)
  end

  #All Journey_ids of which a pathway is a part of.
  def journey_ids_for_pack
    JourneyPackRelation.where(from_id: id).uniq.pluck(:cover_id)
  end

  def completed(user)
    UserContentCompletion.exists?(
      user_id: user.id, completable_id: id,
      completable_type: 'Card', state: 'completed'
    )
  end


  #==================DEPERECATED - Moved to CompletionPercentageService===============

  def completed_percent(user)
    return 100 if completed(user)
    return pathway_completed_percent(user) if pack_card?
    return journey_completed_percent(user) if journey_card?
    # else return zero
    0
  end

  def journey_completed_percent(user)
    card_ids = journey_pack_relations.map(&:from_id) - [self.id]
    return 0 if card_ids.empty?
    partition = CardPackRelation.where(cover_id: card_ids).not_deleted.group_by(&:cover_id)
    partition.each do |cover, cards|
      partition[cover] = batch_completed_percentage(
        user.id,
        cards.map(&:from_id) - [cover],
        true
      )
    end
    value = (partition.values.reduce(&:+) / partition.values.count)
    value.equal?(100) ? 98 : value
  end

  def batch_completed_percentage(user_id, cards, round_off)
    completed_count = UserContentCompletion.where(
      user_id: user_id,
      completable_id: cards,
      completable_type: 'Card'
    ).pluck(:state).compact.count('completed')
    return 0 if completed_count.zero?
    value = (completed_count.to_f * 100) / cards.count
    value.to_i.equal?(100) && !round_off ? 98 : value.round
  end

  def pathway_completed_percent(user, round_off = false)
    if card_subtype == 'journey'
      result = 0
      pathway_all_cards.each do |card|
        result += card.completed_percent(user)
      end
      return result.zero? ? 0 : (result / pathway_all_cards.count)
    end
    cards = pathway_all_cards
    return 0 if cards.blank?
    batch_completed_percentage(user.id, cards, round_off)
  end

  #======================================================================================

  def pack_cards_index_data(user: nil)
    return [] if card_type != 'pack'
    card_pack_relations = CardPackRelation.pack_relations(cover_id: id)
    return [] unless card_pack_relations
    card_pack_relations
      .select { |cpr| cpr.from_id != id && !cpr.deleted && cpr.card }
      .collect do |cpr|
        card = cpr.card
        hash = {
                 card_id: card.id,
                 locked: cpr.locked.nil? ? false : cpr.locked,
                 is_paid: card.is_paid,
                 card_type: card.card_type,
                 card_subtype: card.card_subtype
               }
        if !user.present? || (user.present? && cpr.card.visible_to_user(user))
          hash[:message] = cpr.card.message
          hash[:title]   = cpr.card.title
        end
        hash.with_indifferent_access
      end #title is deprecated
  end

  def journey_cards_index_data(user)
    return [] if card_type != 'journey'

    journey_pack_relations = JourneyPackRelation.journey_relations(cover_id: id)
    return [] unless journey_pack_relations

    journey_pack_relations
      .select { |cpr| cpr.from_id != id && !cpr.deleted && cpr.card }
      .collect do |cpr|
        hash = { card_id: cpr.card.id,
                 block_title: cpr.card.title.present? ? cpr.card.title : cpr.card.message,
                 block_message: cpr.card.message,
                 author_id: cpr.card.author_id.to_s,
                 author_name: cpr.card.author_name,
                 state: cpr.card.state,
                 hidden: cpr.card.hidden,
                 completed_percentage: cpr.card.pathway_completed_percent(user, true),
                 pack_cards: cpr.card.pack_cards_index_data(user: user) }
        hash[:start_date] = cpr.start_date unless cpr.start_date.nil?
        hash
      end
  end

  # All pathways of which a card is a part of.
  def pack_ids
    CardPackRelation.where(from_id: id).uniq.pluck(:cover_id)
  end

  def journeys_sections(pack: false)
    if pack
      {card_ids:addable_to_journey_ids}
    else
      in_packs = CardPackRelation.where(from_id: id).uniq.pluck(:cover_id)
      journeys_ids = JourneyPackRelation.where.not(id: JourneyPackRelation.where(from_id: in_packs).pluck(:id))
      ids = journeys_ids.pluck(:cover_id).uniq
      {card_ids: ids, jpr_ids: journeys_ids.pluck(:from_id) - ids}
    end
  end

  def addable_to_journey_ids
    JourneyPackRelation.where
            .not(cover_id: JourneyPackRelation.where(from_id: id).pluck(:cover_id))
            .pluck(:cover_id).uniq
  end


  #only for livestream and cards not for scheduled and past streams
  def send_notification
    return if pack_draft? || hidden?
    if EXPOSED_TYPES.include?(card_type) || (self.video_stream? && video_stream.status == "past")
      EDCAST_NOTIFY.trigger_event(Notify::EVENT_NEW_CARD, self, {event: Card::EVENT_NEW_CARD_SHARE})
    end
  end

  def pack_draft?
    self.card_type == Card::TYPE_PACK && self.state == Card::STATE_DRAFT
  end

  def processing?
    state == STATE_PROCESSING
  end

  def journey_draft?
    self.card_type == Card::TYPE_JOURNEY && self.state == Card::STATE_DRAFT
  end

  # DEPRECATED
  def get_content_map
    ret = {'title' => title, 'message' => message}
    if !file_resources.blank?
      file_resources.each do |fr|
        if fr.image?
          ret.merge!({'image_url' => fr.file_url, 'image_name' => fr.file_name})
          break
        end
      end
    elsif !resource.blank?
      if resource.type == 'Video'
        ret.merge!({'video_url' => resource.url, 'video_image_url' => resource.image_url, 'video_title' => resource.title, 'video_description' => resource.description, 'video_video_url' => resource.video_url, 'video_embed_html' => resource.embed_html, 'video_site' => resource.site_name})
      elsif resource.type == 'Image'
        ret.merge!({'image_url' => resource.image_url})
      else
        ret.merge!({'link_url' => resource.url, 'link_site' => resource.site_name, 'link_image_url' => resource.image_url, 'link_title' => resource.title, 'link_description' => resource.description, 'link_video_url' => resource.video_url})
      end
    end
    ret.with_indifferent_access
  end

  #DEPRECATED
  def get_content key
    content_map = get_content_map
    content_map[key]
  end

  def to_search_hashie_mash
    @hashie_map ||= Hashie::Mash.new(self.search_data)
  end

  #can comment on accessible_cards
  #accesibble_cards = public cards + private cards user has access to
  def can_comment? user
    organization_id == user.organization_id && visible_to_user(user)
  end

  def bookmarked_by? user
    Bookmark.where(bookmarkable: self, user: user).present?
  end

  def self.has_reading_rights?(card, user)
    # If user is nil, only cards in default org are visible
    user_org_id = user.try(:organization_id)
    _card_organization = Organization.find_by(id: card['organization_id'])
    (_card_organization.open? || card['organization_id'] == user_org_id) && (card['channel_ids'].empty? || (card['channel_ids'] & Channel.readable_channels_for(user, _organization: _card_organization, onboarding: false).map(&:id)).any?)
  end

  # proxy for card followers
  def follower_ids
    ret = []
    ret << self.author_id unless self.author_id.nil?
    CardsUser.where.not(state: 'dismiss').where(card_id: self.id).select(:user_id).distinct.each do |cu|
      ret << cu.user_id
    end
    Comment.where(commentable: self).select(:user_id).distinct.each do |comment|
      ret << comment.user_id unless ret.include?(comment.user_id)
    end
    ret
  end

  def assign_channels_to_card(channel_ids, user)
    writable_channel_ids = Channel.writable_channels_for(user).map(&:id)
    authorized_channel_ids = channel_ids.map(&:to_i) & writable_channel_ids

    valid_channel_ids = authorized_channel_ids - self.channel_ids
    self.channels << Channel.where(id: valid_channel_ids)
    self.reindex_async
  end

  def liked_by
    cu_ids = votes.pluck(:user_id)
    User.where(id: cu_ids)
  end

  def after_tagged
    # no op
  end

  def first_image
    @first_image ||= file_resources.images.first
  end

  def image_attachment
    file_resources.images.first.try(:attachment) || (resource && resource.file_resources.images.first.try(:attachment)) || (video_stream && video_stream.image_attachment)
  end

  def fetch_image_attachment_url
    url = image_attachment.try(:url) || (resource && resource.image_url)
    url = ApplicationController.helpers.asset_path('card_default_image_min.jpg') if url.nil?

    uri = URI.parse(url)
    uri.scheme = Settings.serve_images_on_ssl ? 'https' : 'http'
    uri.to_s
  end


  def card_deeplink_url
    deeplink_url = BranchMetricUrlGenerator.generator_url(self.assignment_url, self.id, "card")
    log.info "deeplinking url triggered #{deeplink_url}"
    return deeplink_url
  end

  # Notification class is using this method to generate deep link
  def get_app_deep_link_id_and_type
    # If a collection cover card, set type to collection.
    # Otherwise for an insight in a collection that does not have an independent existence (hidden = true), \
    # set deep_link_type to collection. Otherwise, the insight might belong to multiple pathways, so the \
    # best thing is to just deep link to the insight itself.

    _id, _type = case card_type
    when 'pack'
      [id, 'collection']
    when 'video_stream'
      [video_stream.id, 'video_stream']
    else
      if hidden
        [pack_ids.first, 'collection']
      else
        [id, 'card']
      end
    end
    [_id, _type]
  end

  def get_app_deep_link_id_and_type_v2
    # child cards for pathway and journey by default hidden /
    # sections for journey by default hidden and draft /
    # so in sum if card is hidden or/and draft that card could be exist
    # only in one of the collection (pathway/journey section)
    if hidden
      deep_link_for_hidden_card
    else
      case card_type
      when 'pack'
        [id, 'collection']
      when 'journey'
        [id, 'journey']
      else
        [id, 'card']
      end
    end
  end

  def valid_credential
    client.valid_credential
  end

  def is_author?(user_id)
    ugc? && author_id == user_id
  end

  def question_attempted?
    quiz_question_attempts.present?
  end

  def update_poll_options(options)
    # get removed question option ids for deletion
    removed_options_ids = quiz_question_options.map(&:id) - options.map{|c| c["id"].to_i }

    options.each do |option|
      if option[:id].blank?
        QuizQuestionOption.create!(quiz_question: self, label: option[:label], is_correct: option[:is_correct])
      else
        if quiz_question_option = QuizQuestionOption.where(id: option[:id], quiz_question: self).first
          quiz_question_option.assign_attributes(label: option[:label], is_correct: option[:is_correct])
          quiz_question_option.changed? && quiz_question_option.save!
        end
      end
    end

    if removed_options_ids.present?
      QuizQuestionOption.where(id: removed_options_ids).delete_all
    end
  end

  def delete_card(opts = {})
    reporting_present = card_reportings.present?
    card_id = id
    status = if card_type == 'pack'
      delete_pathway(opts)
    elsif card_type == 'journey'
      delete_journey(delete_dependent: opts[:delete_dependent])
    else
      destroy_card
    end
    CardReportingTrashingJob.perform_later(card_id: card_id) if reporting_present
    status
  end

  def delete_pathway(opts = {delete_dependent: false})
    CardPackRelation.remove(cover_id: id, destroy_hidden: opts[:delete_dependent], inline_indexing: true)
  end

  def delete_journey(delete_dependent:)
    JourneyPackRelation.remove(cover_id: id, inline_indexing: false, delete_hidden: delete_dependent)
  end

  def destroy_card
    inline_indexing = true
    self.destroy
  end

  def delete_ecl_card
    if ecl_id.present? && !update_request_from_ecl
      EclCardArchiveJob.perform_later(ecl_id.gsub('ECL-', ''), organization_id)
    end
  end

  def trash_from_ecl
    EclCardTrashingJob.perform_later(ecl_id, organization_id) if ecl_id.present?
  end

  def recover_content_from_ecl
    EclCardRecoverJob.perform_later(ecl_id, organization_id) if ecl_id.present?
  end

  # pathway
  def add_card_to_pathway(content_id, content_type)
    CardPackRelation.add(cover_id: id, add_id: content_id, add_type: content_type)
  end

  # journey
  def add_card_to_journey(content_id)
    JourneyPackRelation.add(cover_id: id, add_id: content_id)
  end

  def remove_card_from_pathway(content_id, content_type, opts = {delete_dependent: true})
    CardPackRelation.remove_from_pack(cover_id: id, remove_id: content_id, remove_type: content_type, destroy_hidden: opts[:delete_dependent])
  end

  def remove_card_from_journey(content_id, delete_dependent = true)
    # delete dependent card by default should be true
    JourneyPackRelation.remove_from_journey(cover_id: id, remove_id: content_id, destroy_hidden: delete_dependent)
  end

  def pack_relns
    CardPackRelation.pack_relations(cover_id: id)
  end

  def journey_relns
    JourneyPackRelation.journey_relations(cover_id: id)
  end

  def can_publish?
    message = 'Please add cards to the collection before publishing.'
    can_publish = (!pack_card? && !journey_card?) || (pack_card? && pack_relns && pack_relns.size > 1) || (journey_relns && journey_relns.size > 1)
    if card_subtype == 'journey'
      can_publish = false
      message = 'Not allowed publish pack with subtype journey'
    end
    self.errors.add(:state, message) unless can_publish
    can_publish
  end

  def reorder_pathway(from_pos, to_pos)
    CardPackRelation.reorder_pack_relations(cover_id: self.id, from_pos: from_pos, to_pos: to_pos)
  end

  # DEPRECATED
  # def record_card_action(action_name, user)
  #   case action_name
  #   when 'upvote', 'unupvote'
  #     result = cards_users.where(user: user, state: action_name).first_or_create if action_name == "upvote"
  #     result = cards_users.where(user: user, state: "upvote").first.destroy if action_name == "unupvote"
  #   when 'bookmark', 'unbookmark'
  #     result = cards_users.where(user: user, state: action_name).first_or_create if action_name == "bookmark"
  #     result = cards_users.where(user: user, state: "bookmark").first.destroy if action_name == "unbookmark"
  #   end
  #   result
  # end

  def reorder_journey(from_pos, to_pos)
    JourneyPackRelation.reorder_journey_relations(cover_id: self.id, from_pos: from_pos, to_pos: to_pos)
  end

  def record_card_action(action_name, user)
    case action_name
    when 'upvote', 'unupvote'
      result = cards_users.where(user: user, state: action_name).first_or_create if action_name == "upvote"
      result = cards_users.where(user: user, state: "upvote").first.destroy if action_name == "unupvote"
    when 'bookmark', 'unbookmark'
      result = cards_users.where(user: user, state: action_name).first_or_create if action_name == "bookmark"
      result = cards_users.where(user: user, state: "bookmark").first.destroy if action_name == "unbookmark"
    end
    result
  end

  def activity_initiator
    self.author
  end

  def activity_stream_action
    case card_type
    when 'pack'
      ActivityStream::ACTION_CREATED_PATHWAY
    when 'journey'
      ActivityStream::ACTION_CREATED_JOURNEY
    when 'video_stream'
      video_stream&.activity_stream_action
    else
      ActivityStream::ACTION_CREATED_CARD
    end
  end

  def is_stream_worthy?
    if(card_type == "video_stream")
      video_stream&.is_stream_worthy?
    else
      publicly_visible? && self.published? && !hidden && !author.nil?
    end
  end

  def publicly_visible?
    channels.empty? || channels.reject{|ch| ch.is_private}.any?
  end

  def shareable?(user)
    !hidden && published? && (is_public ||
      (!is_public &&
        (user.is_org_admin? || is_author?(user.id) || (explicit_permissions_present? && user.permission_to_access(id) ))
      )
    )
  end

  def explicit_permissions_present?
    users_with_permission.any? || teams_with_permission.any?
  end

  def visible_to_user(user, inaccessible_card_check: true)
    #Return true for public cards
    if is_public?
      return true
    else
      # Return true for authors/admins
      return true if self&.author_id == user.id ||
                    organization.is_admin?(user) ||
                    user.authorize?(Permissions::ADMIN_ONLY)

      # user will have access if they have been provided explicit permission
      return user.permission_to_access(id) if explicit_permissions_present?

      #user will have access if card is shared with the user directly
      return true if users_with_access.any? && users_with_access.where(id: user.id).exists?

      #user will have access if card is shared with their channel
      user_channels = user.followed_channels.pluck(:id)
      return true if channels.where(id: user_channels).exists?

      #user will have access if card is shared with their team
      user_groups = TeamsUser.where(user: user).pluck(:team_id)
      return true if shared_with_teams.where(id: user_groups).exists?

      false
    end
  end

  # all filters:
    # public || authored cards OR
    # for private cards -
    # check if card is restricted on users and one of it is the user himself
    # OR check if card is restricted on teams and it matches any of users teams
    # OR if there are no restrictions then
      # check if card is a part of users channels
      # OR if card is a part of users teams
      # OR if card is directly shared with the user
  def self.visible_to_userV2(user)

    user_teams = user.team_ids.any? ? user.team_ids : nil
    user_groups = user.team_ids.any? ? user.team_ids.join(',') : 'NULL'
    user_channels = user.followed_channel_ids.count > 0 ? user.followed_channel_ids.join(',') : 'NULL'

    query_params = {
      user_channels: user_channels,
      user_teams: user_teams,
      user_groups: user_groups,
      user_id: user.id
    }

    base_condition = <<-SQL.strip_heredoc
      (cards.is_public = true
      || cards.author_id = :user_id
      || (cards.is_public = false
          && ((card_user_permissions.id IS NOT NULL && card_user_permissions.user_id = :user_id)
            || (card_team_permissions.id IS NOT NULL && card_team_permissions.team_id IN (:user_teams))
            || ((cc.id IS NOT NULL || tc.id IS NOT NULL || card_user_shares.id IS NOT NULL) &&
                (card_user_permissions.id IS NULL && card_team_permissions.id IS NULL)))
          )
      )
      SQL

    self.joins("LEFT JOIN channels_cards cc ON cc.card_id = cards.id AND cc.channel_id in (#{user_channels})")
      .joins("LEFT JOIN teams_cards tc ON tc.card_id = cards.id AND tc.team_id in (#{user_groups})")
      .joins("LEFT JOIN card_user_shares ON card_user_shares.card_id = cards.id AND card_user_shares.user_id = (#{user.id})")
      .joins("LEFT JOIN `card_user_permissions` ON `card_user_permissions`.`card_id` = `cards`.`id`")
      .joins("LEFT JOIN `card_team_permissions` ON `card_team_permissions`.`card_id` = `cards`.`id`")
      .where(base_condition, query_params).uniq
  end

  def user_accessible_content(user)
    return true if ugc? || ecl_metadata.blank?
    return true if organization.team_accessible_source_ids.exclude?(ecl_metadata['source_id'])
    return user.accessible_source_ids.include?(ecl_metadata['source_id'])
  end

  def cover_card
    cpr = CardPackRelation.find_by(from_id: id)
    cpr&.cover
  end

  def display_title(length)
    title.presence || message&.truncate(length ,seperator: ' ')
  end

  def is_paid_card
    if pack_card? || journey_card?
      CardMetadatum::PAID_PLANS.include?(card_metadatum.try(:plan))
    else
      is_paid
    end
  end

  def is_paid_or_metadata
    return true if is_paid
    CardMetadatum::PAID_PLANS.include?(card_metadatum.try(:plan))
  end

  def recalculate_card_metadata(cover_card: false)
    if cover_card || (!pack_card? && !journey_card? && (card_metadatum.present? && card_metadatum.previous_changes.include?("plan")))
      UpdateCardMetadataJob.perform_later(id)
    end
  end

  def find_pack_journey_and_update
    if journey_card? || pack_card?
      create_or_update_metadata
      update_journey_metadata(id) if pack_card?
    else
      pack_ids = CardPackRelation.where(from_id: id).pluck(:cover_id)
      packs = Card.where(id: pack_ids)
      packs.each do |pack|
        pack.create_or_update_metadata
        update_journey_metadata(pack.id)
      end
    end
  end

  def create_or_update_metadata
    #Update Parhway/Journey metadata
    if pack_card?
      plan_types = pack_cards.map { |pack_card| pack_card.is_paid_or_metadata }.uniq
      plan = if (plan_types.include?(true) && plan_types.include?(false))
               "free/paid"
             elsif plan_types.include?(true)
               "paid"
             end
    elsif journey_card?
      plan_types = journey_pathways.map { |pack| pack&.card_metadatum&.plan }.uniq
      plan = if (plan_types.include?('free/paid'))
               "free/paid"
             elsif (CardMetadatum::PAID_PLANS & plan_types).present?
               "paid"
             end
    end
    plan ||= "free"
    card_metadata = CardMetadatum.find_or_create_by({card_id: id})
    card_metadata.update_column(:plan, plan) if card_metadata.plan != plan
  end
  # def pack_has_any_paid_card?
  #   return true if pack_cards.pluck(:is_paid).any?
  #   return true if pack_cards.map(&:card_metadatum).compact.map(&:plan).any?{|p| CardMetadatum::PAID_PLANS.include?(p)}
  #   return is_paid
  # end

  # def journey_has_any_paid_card?
  #   journey_pathways.each do |pack|
  #     return true if pack.pack_has_any_paid_card?
  #   end
  #   return is_paid
  # end
  def paid_by_user? user
    user.active_card_subscriptions.exists?(card_id: id)
  end

  def self.visible_cards_to_user(user)
    select{|card| card.visible_to_user(user)}
  end

  def self.public_cards
    select{|card| card.publicly_visible? }
  end


  def assignment_url
    ClientOnlyRoutes.assignment_url(self)
  end

  # To handle video stream published_at
  def published_at
    if self.card_type == 'video_stream'
      video_stream.try(:start_time)
    else
      super
    end
  end

  def self.card_ecl_id(ecl_id)
    "ECL-#{ecl_id}"
  end

  def self.is_ecl_id?(compound_id)
    /^ECL\-.+$/.match(compound_id)
  end

  def self.get_ecl_id(compound_id)
    /^ECL\-(.+$)/.match(compound_id).try(:[], 1)
  end

  def remove_from_queue_and_reenqueue
    log.info "Content Dequeue Job calling for id #{id} via remove_card_from_queue_reenqueue"
    ContentsBulkDequeueJob.perform_later(content_ids: [id], content_type: 'Card', reenqueue: true)
  end

  def enqueue_card
    if !hidden
      FollowedCardEnqueueJob.perform_later(card_id: id)
    end
  end

  def enqueue_for_followers_on_publish
    if !self.previous_changes['state'].blank? && self.previous_changes['state'][1] == 'published'
      enqueue_card
    end
  end

  def add_card_to_channel(channel, mark_content_private = false, call_card_connector = true)
    #call_card_connector is always true.
    #Its will be passed from sociative channel programming as false, bcoz we create card from sociative
    #and will not call ecl card connector again
    begin
      if call_card_connector
        service = EclApi::CardConnector.new(ecl_id: ecl_id, organization_id: channel.organization_id)
        card = service.card_from_ecl_id
      else
        card = self
      end
      if card && card.channels.exclude?(channel)
        card.ecl_update_required = true
        card.is_public = false if mark_content_private || !card.is_public
        card.channels << channel
        card.save
      end
    rescue => e
      log.warn("Adding ecl card #{ecl_id} to channel #{channel.id} failed, message: #{e.message}")
    end
  end

  def all_time_views_count
    # ECL card
    return 0 if id.blank? || Card.is_ecl_id?(id.to_s)
    key = Analytics::CardViewMetricsRecorder.card_view_count_cache_key(self)
    (
      Rails.cache.fetch(key) ||
      Analytics::CardViewMetricsRecorder.set_initial_card_view_count(self, key)
    ).to_i
  end

  def get_weekly_views_count(time = Time.now.utc )
    # TODO: Update all content type to card. Should stop supporting collection and video stream as content type
    content_id, content_type = self.get_app_deep_link_id_and_type
    offset = PeriodOffsetCalculator.applicable_offsets(time)[:week]
    ContentLevelMetric.get_views_count({content_id: content_id, content_type: content_type}, 'week', offset)
  end

  # We need to be precise to grab exact card type.
  # Course, Collection reflects card_type as per intention.
  # UGC cards are further categorized as TEXT, VIDEO, etc.
  def current_card_type
    if self.card_type == 'media'
      self.card_subtype
    else
      self.card_type
    end
  end

  def completion_count
    UserContentCompletion.where(completable: self).completed.count
  end

  def resource_group
    if ecl_metadata.present? && ecl_metadata['external_id'].present? && ecl_metadata['source_type_name'].present? && ecl_metadata['source_type_name'] == 'edcast_cloud'
      ResourceGroup.find_by(client_resource_id: ecl_metadata['external_id'], organization_id: self.organization_id)
    end
  end

  def send_card_create_or_share_notification(opts = {})
    # event: Card::EVENT_CARD_SHARE, opts: {}
    return if !organization.notifications_and_triggers_enabled? || pack_draft? || journey_draft?
    CreateShareCardNotificationJob.perform_later(card_id: id, event: opts[:event], share_by_id: opts[:share_by_id], team_ids: opts[:team_ids],
      user_ids: opts[:user_ids], permitted_user_ids: opts[:permitted_user_ids], permitted_team_ids: opts[:permitted_team_ids])
  end

  # EXP:
  #   This returns partner URL that is set in PartnerCenter.
  #   PartnerCenter deals with embed link configured in Okta so that user is provisioned into Partner's platform.
  #   Must cover all of the integrations, ignoring scorm integrations for now.
  def integrated_partner_link
    partner_link     = nil
    current_org      = self.organization
    integration_id = self.ecl_metadata.try(:[], 'source_id')
    source_type_name = self.ecl_metadata.try(:[], 'source_type_name')

    return if integration_id.blank? || source_type_name.blank?

    partners = current_org.partner_centers.enabled
    current_partner = partners.find_by(integration: integration_id)
    current_partner ||= partners.find_by(integration: source_type_name)
    return if current_partner.nil?

    case source_type_name
    when 'edcast_cloud'
      group = Group.find_by(client_resource_id: self.ecl_metadata['external_id'])
      group && partner_link = group.website_url
    else
      partner_link = self.resource.try(:url)
    end

    current_partner.embed_link + '?RelayState=' + partner_link.to_s
  end

  def time_to_read
    duration || (ecl_metadata.try(:[], "duration_metadata") && ecl_metadata["duration_metadata"]["calculated_duration"].try(:to_i))
  end

  def has_scorm_filestack?
    filestack.present? && filestack.first[:scorm_course].present?
  end

  def source_type_name
    ecl_metadata.present? ? ecl_metadata.dig('source_type_name') : nil
  end

  def is_scorm_card?
    has_scorm_filestack? ||
     resource.present? && resource.is_scorm_resource?
  end

  def disable_mark_feature_for_scorm_cards?
    organization.present? && organization.auto_mark_as_complete_enabled? && is_scorm_card?
  end

  # disable mark_as_complete for scorm course cards also if org_config enabled
  def mark_feature_disabled?
    ecl_metadata&.dig('mark_feature_disabled').to_s.to_bool || disable_mark_feature_for_scorm_cards?
  end

  def mark_as_complete(user:)
    user_content_completion = user.user_content_completions.
                                where(completable: self).first_or_create
    return true if user_content_completion.completed?

    if !user_content_completion.started? && !user_content_completion.completed?
      user_content_completion.start
    end
    if !user_content_completion.completed? && user_content_completion.started?
      user_content_completion.complete
    end
    if !user_content_completion.save
      errors.add(:base, "Card can not be marked as complete, #{user_content_completion.full_errors}")
    end
    if user_content_completion.completed?
      ClcService.new.initiate_clc_records(self, user)
      UserBadgeService.new("Card", self, user.id).assign_badge
    end
  end

  def self.find_by_resource_url(url, org_id)
    url_hash = Resource.get_url_hash(url)
    resource_ids = Resource.where(url_hash: url_hash).pluck(:id)
    if resource_ids.present?
      find_by(organization_id: org_id, resource_id: resource_ids)
    end
  end


  def self.find_in_source_with_message(message, source_id)
    where("message LIKE :key OR title LIKE :key and ecl_metadata like :source_id", key: message, source_id: "%#{@source_id}%").first
  end

  def lxp_payment_enabled?
    source_id = ecl_metadata&.dig('source_id')
    source_detail = SourcesCredential.find_by(source_id: source_id, organization_id: organization_id)
    return source_detail&.auth_api_info.present?
  end

  def savannah_course?
    ecl_metadata&.dig('source_type_name') == 'edcast_cloud'
  end

  def remove_cloned_cards
    case card_type
    when Card::TYPE_JOURNEY
      remove_cloned_journey_pack_association
    when Card::TYPE_PACK
      remove_cloned_pack_card_association
    else
      cloned_cards.destroy_all
    end
  end

  def remove_cloned_journey_pack_association
    Card.unscoped.joins("INNER JOIN journey_pack_relations ON cards.id = journey_pack_relations.from_id")
      .where(journey_pack_relations: {cover_id: id}).find_each do |card|
        if card.journey_card? # one card will be journey card in jpr
          card.cloned_cards.destroy_all
        else
          card.remove_cloned_pack_card_association
        end
    end
  end

  def remove_cloned_pack_card_association
    Card.unscoped.joins("INNER JOIN card_pack_relations ON cards.id = card_pack_relations.from_id")
      .where(card_pack_relations: {cover_id: id}).find_each do |card|
        if card.journey_card? # For curriculum
          card.remove_cloned_journey_pack_association
        else
          card.cloned_cards.destroy_all
        end
    end
  end

  def authorize_course(card_transaction: card_transaction_obj, user: user_obj)
    card_attributes = {
                       id: id,
                       url: resource&.url,
                       client_resource_id: resource&.parsed_client_resource_id,
                       external_id: ecl_metadata&.dig('external_id'),
                       app_id: organization&.savannah_app_id
                      }
    payment_attributes = {
                          id: card_transaction.id,
                          amount: card_transaction.amount,
                          currency: card_transaction.currency
                         }
    user_attributes = {
                        id: user.id,
                        email: user.email,
                        first_name: user.first_name,
                        last_name: user.last_name
                      }
    source_id = ecl_metadata&.dig('source_id')
    ContentProvider::AuthorizeCourse.new(source_id: source_id,
                                         user: user_attributes,
                                         card: card_attributes,
                                         payment: payment_attributes).authorize
  end

  def push_view_event(viewer)
    record_custom_event(
      event: 'card_viewed',
      actor: lambda { |record| viewer }
    )
  end

  def push_click_through_event(viewer)
    record_custom_event(
      event: 'card_source_visited',
      actor: lambda { |record| viewer }
    )
  end

  def redirect_page_url(org:)
    ClientOnlyRoutes.show_insight_url(self)
  end

  def split_by_curation(channel_ids)
    channel_ids.partition do |ch_id|
      ChannelsCard.new(channel_id: ch_id, card: self).curation_required?
    end
  end

  def author_profile
    UserProfile.find_by(user_id: self.author_id)
  end

  def reprocess_team_assignment_metric(team_ids, team_assignors)
    #Re-process metric
    return if team_ids.empty?
    team_ids.each do |team_id|
      RecalculateAssignmentPerformanceMetricJob.perform_later(team_id, team_assignors[team_id], id)
    end
  end

  #============================================== PRIVATE =====================

  private
  def notify_user
    changed_attributes = ['title', 'message', 'filestack', 'url', 'description', 'image_url', 'video_url']
    if !journey_card? and !pack_card? and !new_record?
      if destroyed? ||
         changes.keys.any?{|k| changed_attributes.include?(k)} ||
         (resource && resource.changes.keys.any?{|k| changed_attributes.include?(k)})

        action = destroyed? ? 'removed_from' : 'card_updated'
        pack_ids = CardPackRelation.where({from_id: id}).pluck(:cover_id)
        NotifyUserJob.perform_later(action: action, card_ids: [id], cover_ids: pack_ids, actor_id: self.current_user&.id)
      end
    end
  end

  def clone_without_transaction_to_org!(_organization, override_params)
    cloned_card = Card.new(card_type: self.card_type, card_subtype: self.card_subtype, title: self.title, message: self.message, organization_id: _organization.id, state: 'published', published_at: Time.now.utc)

    if self.resource
      cloned_card.resource = self.resource.dup
    end

    if self.file_resources.present?
      self.file_resources.each do |fr|
        cloned_card.file_resources << FileResource.new(attachment: fr.attachment)
      end
    end

    if override_params.has_key?(:hidden)
      cloned_card.hidden = override_params[:hidden]
    else
      cloned_card.hidden = self.hidden
    end

    if override_params.has_key?(:topics)
      tags = override_params[:topics].map{|topic| Tag.find_or_create_by(name: topic)}
    else
      tags = self.tags
    end

    if override_params.has_key?(:channel_ids)
      cloned_card.channels = _organization.channels.where(id: override_params[:channel_ids])
    end

    if self.quiz_question_options.present?
      self.quiz_question_options.each do |opt|
        cloned_card.quiz_question_options << QuizQuestionOption.new(label: opt.label, is_correct: opt.is_correct)
      end
    end

    cloned_card.save!
    # BUGSNAG: https://app.bugsnag.com/edcast/edcast-production-eb/errors/578373a15334c8b7bccabb20?event_id=579bc8cc303785377ed18a37
    # This somehow fixes `Tags is invalid`
    cloned_card.tags += tags
    cloned_card
  end

  # # Validation methods
  def validate_poll_can_be_updated
    if self.question_attempted? && (title_changed? || message_changed?)
      errors[:base] = "Edit functionality is disabled to maintain integrity of the poll."
    end
  end

  def sanitize_title_description
    self.title = self.title.try(:clean)
    self.message = sanitize_message
  end

  def user_in_org
    if author && self.organization_id
      errors.add(:organization_id, "User does not belong to organization") unless author.organization_id == self.organization_id
    end
  end

  def generate_poll_notification
    NotificationGeneratorJob.perform_later(item_id: id, item_type: 'new_poll')
  end

  def handle_card_deletion
    CardDeletionJob.perform_later self.id
  end

  def create_deleted_document
    Tracking::track(
      self.author_id,
      'deleted',
      object: 'card',
      object_id: self.id,
      type: 'deleted',
      organization_id: self.author.try(:organization_id)
    )
  end

  def remove_card_from_queue
    log.info "Content Dequeue Job called for id #{id} via remove_card_from_queue"
    ContentsBulkDequeueJob.perform_later(content_ids: [id], content_type: 'Card')
  end

  def remove_assignments
    Assignment.where(assignable: self).destroy_all
  end

  def remove_clc
    ClcService.new.remove(self.id)
  end

  def remap_locks
    RemapLocksJob.perform_later(id)
  end

  def set_promoted_timestamp
    if self.is_official_changed?
      if self.is_official?
        self.promoted_at = Time.current
      else
        self.promoted_at = nil
      end
    end
  end

  def record_promote_event
    if self.is_official_changed?
      self.is_official? ? record_card_promote_event : record_card_unpromote_event
    end
  end

  def propagate_card_to_ecl
    # Not optimal. We shouldn't make a call to ECL on every update. Only on some updates like title and message
    if !self.update_request_from_ecl && update_ecl_content_item?
      EclCardPropagationJob.perform_later(self.id)
    end
  end

  def sync_clone_cards
    if cloned_cards.exists? && is_public && clone_attributes_changed?
      SyncCloneAssociationsJob.perform_later(
        {
          parent_card_id:id,
          sync_association_type: 'Card'
        }
      )
    end
  end

  def clone_attributes_changed?
    (self.previous_changes.keys.map(&:to_sym) & Card::CLONE_SYNC_REQUIRED_ATTRIBUTES).any?
  end

  def create_relation_and_propagate_card_to_ecl
    create_card_pack_relation if card_type == 'pack'
    create_journey_pack_relation if card_type == 'journey'
    propagate_card_to_ecl
  end

  def update_ecl_content_item?
    self.ecl_update_required || ((self.ugc? || self.cloned_card?) && ecl_attributes_changed? && !pack_draft?) || self.previous_changes["duration"]
  end

  def ecl_attributes_changed?
    (self.previous_changes.keys & ['card_type', 'card_subtype', 'title', 'message', 'resource_id', 'state', 'user_taxonomy_topics',
      'author_id']).any?
  end

  # watch out on cyclic dependency.
  # see update_card_state method in models/video_stream.
  # or should use state column on card rather than on VS
  def update_video_stream_state
    return if self.card_type != "video_stream" || self.state == video_stream.state
    video_stream.update(state: self.state)
  end

  def create_card_pack_relation
    CardPackRelation.where(cover: self, from_id: self.id, from_type: 'Card').first_or_create
  end

  def create_journey_pack_relation
    JourneyPackRelation.where(cover: self, from_id: self.id).first_or_create
  end

  def ecl_metadata_present?
    ecl_metadata&.dig('source_display_name') != ecl_source_name
  end

  def set_project_card
    build_project if (card_type == "project")
  end

  # influx event recording methods
  def record_card_promote_event
    record_custom_event(
      event: 'card_promoted',
      actor: :author
    )
  end

  def record_card_unpromote_event
    record_custom_event(
      event: 'card_unpromoted',
      actor: :author
    )
  end

  def push_card_published_influx_event
    record_custom_event(
      event: 'card_published',
      actor: :author
    )
  end
  # influx event recording ends here

  def set_card_type_and_subtype
    self.card_type ||= 'media'
    self.card_subtype ||= set_card_subtype
  end

  # TODO/WARNING:
  #   This is beyond usual as far as implementation is concerned and unmanageable.
  #   Predicting the type of card should be moved to a class that takes care of this.
  #   Assumption is, CardConcern is already performing these many checks, but looks like it is repeated here.
  #   Card construction (CREATE & UPDATE) must be a separate CLASS that handles every complex iterations.
  def set_card_subtype
    # pack
    if self.pack_card?
      # default subtypes for pack and video_stream
      self.card_subtype ||= 'simple'
    elsif self.video_stream?
      self.card_subtype ||= 'simple'
    elsif self.journey_card?
      self.card_subtype ||= 'self_paced'
    elsif self.card_type == 'media' && self.card_subtype == 'text' && resource.blank?
      # in case when deleting resource we shouldn't change types
      # it keeps card type `text` when we updating it with image
      self.card_subtype
    elsif self.card_type == 'project' && self.card_subtype == 'text'
      self.card_subtype
    elsif self.resource.present?
      # based on resource
      self.card_subtype = card_subtype_from_resource
    elsif self.file_resources.present?
      # based on file_resource
      self.card_subtype = card_subtype_from_file_resource
    elsif self.card_type == 'course'
      self.card_subtype = 'link'
    # Image cards. Includes media, poll.
    # File cards. Includes JSON, HTML, RTF, PPT, PDF, XLS, DOC, CSV, TEXT.
    elsif self.card_subtype == 'image' || self.card_subtype == 'file'
      self.card_subtype
    # Video and Audio files.
    elsif self.card_type == 'media' && (self.card_subtype == 'audio' || self.card_subtype == 'video')
      self.card_subtype
    else
      # default is text
      self.card_subtype = 'text'
    end
  end

  def card_subtype_from_resource
    case resource.type
    when 'Video'
      'video'
    when 'Image'
      'image'
    else
      'link'
    end
  end

  def card_subtype_from_file_resource
    file_resources = self.file_resources.map(&:file_type).compact
    return "file" if file_resources.empty? && card_type=="media"
    begin
      if file_resources.include? 'video'
        'video'
      elsif file_resources.include? 'audio'
        'audio'
      elsif file_resources.include? 'file'
        'file'
      elsif self.file_resources.map(&:attachment_content_type).any? { |ftype| FileResource::FILE_CONTENT_TYPE_MAPPING[:zip].include? ftype }
        'link'
      else
        file_resources.last
      end
    rescue
      nil
    end
  end

  def set_draft_state
    # Not required for cloned card as it will inherit state from parent card
    self.state = STATE_DRAFT unless cloned_card?
  end

  def validate_card_subtype
    unless Card::SUB_TYPES[self.card_type]&.include?(self.card_subtype)
      errors.add(:card_subtype, "Supported subtypes for #{self.card_type} are: #{Card::SUB_TYPES[self.card_type]}")
    end
  end

  def set_message_from_title
    if !message? && title?
      self.message = title
    end
  end

  # for card_type pack or journey - set auto_complete to true if appropriate parameter nil
  def should_be_autocomplete?
    Card::AUTO_COMPLETE_CARDS.include?(card_type) && auto_complete.nil?
  end

  def set_autocomplete
    self.auto_complete = true
  end

  def set_published_at
    self.published_at = Time.now.utc if self.state == Card::STATE_PUBLISHED
  end

  def strip_emoji_title_message
    self.title = strip_emoji(title) if title.present?
    self.message = strip_emoji(message) if message.present?
  end

  def update_journey_metadata(pack_id)
    #Update journey's metadata
    journey_ids = JourneyPackRelation.where(from_id: pack_id).pluck(:cover_id)
    journeys = Card.where(id: journey_ids)
    journeys.each { |journey| journey.create_or_update_metadata }
  end

  def deep_link_for_hidden_card
    cpr = CardPackRelation.find_by(from_id: id)
    # if no CPR return self id and type == 'card'
    return [id, 'card'] unless cpr
    # if CPR hidden and draft - card should be the journey section
    # return journey id and type journey
    # else cpr could be part of journey or not
    # return CPR.cover_id, type = collection
    if cpr.cover.state == Card::STATE_DRAFT && cpr.cover.hidden
      jpr = JourneyPackRelation.find_by(from_id: cpr.cover_id)
      jpr ? [jpr.cover_id, 'journey'] : [cpr.cover_id, 'collection']
    else
      [cpr.cover_id, 'collection']
    end
  end
end

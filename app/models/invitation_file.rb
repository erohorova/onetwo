# == Schema Information
#
# Table name: invitation_files
#
#  id                :integer          not null, primary key
#  sender_id         :integer          not null
#  data              :text(4294967295)
#  roles             :string(255)
#  invitable_id      :integer
#  invitable_type    :string(191)
#  created_at        :datetime
#  updated_at        :datetime
#  send_invite_email :boolean          default(TRUE)
#

class InvitationFile < ActiveRecord::Base

  attr_accessor :skip_processing_invitations

  serialize :parameter, JSON

  belongs_to :sender, :class_name => 'User'
  belongs_to :invitable, polymorphic: true
  has_many :user_import_statuses

  validates_presence_of :invitable
  validates_presence_of :sender_id
  validates_presence_of :data

  after_commit :process_invitations, on: :create

  def process_invitations
    BulkInvitationsJob.perform_later(invitation_file_id: self.id) if !self.skip_processing_invitations
  end

  def record_status
    old_users_count     = self.user_import_statuses.old_users.count
    new_users_count     = self.user_import_statuses.new_users.count
    failed_users_count  = self.user_import_statuses.failed_users.count
    resent_emails_count = self.user_import_statuses.resent_emails.count
    total_users_count   = self.user_import_statuses.count

    parameters = self.parameter || {}
    parameters[:old_users_count]     = old_users_count
    parameters[:new_users_count]     = new_users_count
    parameters[:failed_users_count]  = failed_users_count
    parameters[:resent_emails_count] = resent_emails_count
    parameters[:total_users_count]   = total_users_count

    self.update(parameter: parameters)
  end
end

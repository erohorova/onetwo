class OrganizationsUser < ActiveRecord::Base
  
  belongs_to :user
  belongs_to :organization

  validates :organization, presence: true
  validates :user, presence: true
  validates_inclusion_of :as_type, in: %w(admin member)


  scope :admin, -> { where(:as_type => 'admin') }
  scope :member, -> { where(:as_type => 'member') }

end

# == Schema Information
#
# Table name: clients_users
#
#  id             :integer          not null, primary key
#  user_id        :integer
#  client_id      :integer
#  client_user_id :string(191)
#  created_at     :datetime
#  updated_at     :datetime
#

class ClientsUser < ActiveRecord::Base
  belongs_to :user
  belongs_to :client

  validates_presence_of :user
  validates_presence_of :client

  validates :client_user_id, uniqueness: { scope: :client_id }, if: :client_user_id

end

# == Schema Information
#
# Table name: user_content_completions
#
#  id               :integer          not null, primary key
#  user_id          :integer
#  completable_id   :integer
#  completable_type :string(191)
#  topics           :string(255)
#  state            :string(255)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  started_at       :datetime
#  completed_at     :datetime
#  completed_percentage :integer      not null

class UserContentCompletion < ActiveRecord::Base
  include Streamable
  include AASM
  include EventTracker

  serialize :topics, Array

  belongs_to :user
  belongs_to :completable, polymorphic: true
  belongs_to :card, class_name: 'Card', foreign_key: :completable_id

  validates :user, presence: true
  validates :completable, presence: true
  validates :completable_id, uniqueness: { scope: [:user, :completable_type] }
  validate :validate_user_and_content_organization

  STARTED   = 'started'.freeze
  COMPLETED = 'completed'.freeze

  # deprecated
  EVENT_ACTIONS = {start: "STARTED", complete: "COMPLETED"}.with_indifferent_access.freeze

  validate :validate_submission_completion

  after_commit :update_assignment_state, on: [:create, :update]

  scope :cards, ->{ where(completable_type: 'Card') }
  scope :completed, -> { where(state: 'completed') }

  aasm column: 'state' do
    state :initialized, initial: true #dummy state
    state :started
    state :completed

    event :start do
      transitions from: :initialized, to: :started
      before do
        self.started_at = Time.current
      end
    end

    event :complete do
      #user can complete piece of content by calling mark as complete
      transitions from: [:initialized, :started], to: :completed
      before do
        self.completed_at = Time.current
        self.completed_percentage = 100
      end

      after do
        UserContentsQueue.remove_from_queue(user, completable)
        add_to_activity_stream(self, user, ActivityStream::ACTION_SMARTBITE_COMPLETED)
        add_to_learning_queue
        if completable_type == 'Card'
          record_card_complete_event
          CompletionPercentageService.set_completed_percentage(user, completable)
        end
      end
    end

    event :uncomplete do
      #user can uncomplete piece of content by calling mark as incomplete. Should return state to initialized
      transitions from: :completed, to: :initialized

      before do
        self.completed_at = nil
        self.completed_percentage = 0
      end

      after do
        add_to_activity_stream(self, user, ActivityStream::ACTION_SMARTBITE_UNCOMPLETED)
        update_learning_queue
        if completable_type == 'Card'
          record_card_uncomplete_event
          CompletionPercentageService.set_completed_percentage(user, completable, true)
        end
      end
    end
  end

  # Do not mark as complete project_cards with a valid approval review.
  def validate_submission_completion
    if (completable_type == "Card" && completable.project_card?)
      valid_review = ProjectSubmissionReview.approved_review(completable.project.project_submissions.ids)
      errors.add(:base, "Can not complete project card without a Approval Review") if valid_review.blank?
    end
  end

  def has_assignment?
    user.has_an_assignment?(completable)
  end

  def is_stream_worthy?
    completable.is_a?(VideoStream) || (completable.is_a?(Card) && !completable.hidden?)
  end

  # add item to learning queue when user marks as complete (self)
  # unassigned content.
  def add_to_learning_queue
    if completable && self.completed?
      self.completable.add_to_learning_queue(
        user_id: self.user_id,
        source: "assignments",
        state: 'completed'
      )
    end
  end

  def update_learning_queue
    if completable && initialized?
      completable.uncomplete_learning_queue_item(
          user_id: user_id
      )
    end
  end

  def validate_user_and_content_organization
    if user && completable && user.organization != completable.try(:organization)
      errors.add "Content can not be completed"
    end
  end

  def update_assignment_state
    SyncState.new(card: completable, object: self).assignment_update
  end

  private

  def record_card_complete_event
    perform_card_metrics(
      Analytics::CardMetricsRecorder::EVENT_CARD_MARKED_AS_COMPLETE
    )
  end

  def record_card_uncomplete_event
    perform_card_metrics(
      Analytics::CardMetricsRecorder::EVENT_CARD_MARKED_AS_UNCOMPLETE
    )
  end

  def perform_card_metrics(event)
    record_custom_event(
      event: event,
      actor: :user,
      if_block: lambda { |record| record  },
      job_args: [
        { card: :card },
      ]
    )
  end
end

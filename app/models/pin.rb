# == Schema Information
#
# Table name: pins
#
#  id            :integer          not null, primary key
#  pinnable_id   :integer
#  pinnable_type :string(20)
#  object_id     :integer
#  object_type   :string(255)
#  user_id       :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Pin < ActiveRecord::Base
  include EventTracker

  has_paper_trail ignore: [:created_at, :updated_at]

  belongs_to :pinnable, polymorphic: true
  belongs_to :object, polymorphic: true
  belongs_to :user

  validate :should_belong_to_same_org, unless: :bookmark_pin
  validate :should_card_belong_to_bookmark, if: :bookmark_pin
  validates :pinnable_id, uniqueness: {scope: [:pinnable_id, :object_id, :object_type]}
  before_create :remove_bookmark_pin, if: :bookmark_pin

  #consider channel and card now
  def should_belong_to_same_org
    unless object.organization_id == pinnable.organization_id
      errors.add(:base, "Object and Pinnable Organizations don't match")
    end
  end

  def should_card_belong_to_bookmark
    if pinnable.bookmarkable_id != object_id
      errors.add(:base, "Object does not belong to the Pinnable")
    end
  end

  def self.update_pinned_for_channel(channel_id)
    trending_cards = Card.trending_channel_cards(channel_id)
    pins = where(pinnable_id: channel_id, pinnable_type: 'Channel')

    # deleting existing pins of cards that are not in the trending list anymore for this channel
    pins_to_delete = pins.where.not(object_id: trending_cards.pluck(:id)) if pins.present?
    pins_to_delete.destroy_all if pins_to_delete.present?

    # creating pins for new trending cards
    trending_cards.each do |card|
      pin = where(pinnable_id: channel_id, pinnable_type: 'Channel', object: card).first_or_initialize
      pin.save
    end
  end

  def self.fetch_featured_pins(pinnable, count: )
    # default pins count is 6 for now
    count = count || Settings.default_pins_count
    if pinnable.class == Channel
      # checks if there are any slots left to fill in the pin banner and update pins
      if pinnable.pins.count < count
        update_pinned_for_channel(pinnable.id)
      end
    else
      log.error "Invalid pinnable type"
      return
    end
    pinnable.pins.where(object_id: Card.trending_channel_cards(pinnable.id).pluck(:id))
      .includes(:object).limit(count)
  end

  configure_metric_recorders(
    default_actor: :user,
    trackers: {
      on_create: {
        event: 'card_pinned',
        if_block: lambda {|record| [Channel, Card].include?(record.pinnable.class)},
        job_args: [ { card: :object }, { channel: :pinnable }, {pin_id: :id} ]
      },
      on_delete: {
        event: 'card_unpinned',
        if_block: lambda {|record| [Channel, Card].include?(record.pinnable.class)},
        job_args: [ { card: :object }, { channel: :pinnable }, {pin_id: :id} ]
      }
    }
  )

  def bookmark_pin
    pinnable_type == 'Bookmark' && object_type == 'Card'
  end

  private

  def remove_bookmark_pin
    old_bookmark_pin = Pin.find_by(pinnable_type: 'Bookmark')
    old_bookmark_pin.destroy if old_bookmark_pin.present?
  end
end

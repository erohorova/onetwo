# == Schema Information
#
# Table name: credentials
#
#  id            :integer          not null, primary key
#  api_key       :string(191)
#  shared_secret :string(255)
#  state         :string(255)      default("active")
#  client_id     :integer
#  created_at    :datetime
#  updated_at    :datetime
#  label         :string(255)
#

class Credential < ActiveRecord::Base
  belongs_to :client
  
  before_validation on: :create do
    self.api_key = SecureRandom.hex if self.api_key.blank?
    self.shared_secret = SecureRandom.hex(32) if self.shared_secret.blank?
  end

  validates :client, presence: true
  validates :api_key, uniqueness: true

  scope :admin_list, ->(user) { joins(client: :admins).where(clients_admins: {admin_id: user.id}) }
  scope :active, -> { where(state: 'active') }

  delegate :name, to: :client
end

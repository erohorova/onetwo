# == Schema Information
#
# Table name: resources
#
#  id          :integer          not null, primary key
#  url         :text(16777215)   not null
#  description :text(16777215)
#  title       :string(255)
#  type        :string(255)
#  site_name   :string(255)
#  image_url   :text(16777215)
#  video_url   :text(16777215)
#  created_at  :datetime
#  updated_at  :datetime
#  embed_html  :text(16777215)
#  url_hash    :string(64)
#  user_id     :integer
#

# Deprecated
class Book < Resource
end

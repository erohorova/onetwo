# == Schema Information
#
# Table name: user_topic_level_metrics
#
#  id                  :integer          not null, primary key
#  user_id             :integer          not null
#  tag_id              :integer          not null
#  smartbites_score    :integer          default(0)
#  smartbites_created  :integer          default(0)
#  smartbites_consumed :integer          default(0)
#  time_spent          :integer          default(0)
#  period              :string(101)
#  offset              :integer
#  created_at          :datetime
#  updated_at          :datetime
#  organization_id     :integer
#

class UserTopicLevelMetric < ActiveRecord::Base
  include MetricsAggregator

  create_increment_methods([:smartbites_score, :smartbites_consumed, :smartbites_created, :time_spent])
  create_getter_methods([:smartbites_score, :smartbites_consumed, :smartbites_created, :time_spent])

  belongs_to :user
  belongs_to :tag
  belongs_to :organization

  validates :organization, presence: true
  validates :user, presence: true
  validates :tag, presence: true

  before_validation on: :create do
    if self.organization_id.nil?
      self.organization_id = user.try :organization_id
    end
  end

  def tag_name
    tag.name
  end

  class << self
    def top_topics_for_organization(organization_id, limit=10)
      where(organization_id: organization_id, period: :all_time, offset: 0).
        joins(:tag).
        group("tag_id").
        select("tag_id as tag_id, tags.name as tag_name, sum(smartbites_score) as smartbites_score, count(user_id) as users_count").
        order("smartbites_score DESC").limit(limit).
        map{|metric| {'id' => metric.tag_id, 'name' => metric.tag_name, 'smartbites_score' => metric.smartbites_score, 'users_count' => metric.users_count}}
    end

    def top_users_for_topic_in_organization(organization_id, tag_id, limit=10)
      where(organization_id: organization_id, tag_id: tag_id, period: :all_time, offset: 0).includes(:user).order(smartbites_score: :desc).limit(limit).map{|metric| {'id' => metric.user.id, 'name' => metric.user.name, 'smartbites_score' => metric.smartbites_score}}
    end

    def top_topics_for_user(user_id, limit=10)
      where(user_id: user_id, period: :all_time, offset: 0).includes(:tag).order(smartbites_score: :desc).limit(limit).map{|metric| {'id' => metric.tag_id, 'name' => metric.tag.name, 'smartbites_score' => metric.smartbites_score, 'users_count' => 0}}
    end

    def knowledge_map(organization, target=nil, limit=10)
      case target
      when User
        {"topics" => top_topics_for_user(target.id, limit)}
      when Tag
        {"users" => top_users_for_topic_in_organization(organization.id, target.id, limit)}
      else # Send for the organization by default
        {"topics" => top_topics_for_organization(organization.id, limit)}
      end
    end
  end
end

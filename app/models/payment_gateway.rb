class PaymentGateway
  GATEWAY_INTERNAL = 'internal'.freeze
  GATEWAY_BRAINTREE = 'braintree'.freeze

  STATUS_SUCCESSFUL = 'successful'.freeze
  STATUS_FAILED = 'failed'.freeze

  def self.gateway_for_currency(currency)
    if currency == Settings.payment_currencies['edu_currency']
      'wallet'
    elsif Settings.payment_currencies['external_currencies'].include?(currency)
      'braintree'
    else
      raise InvalidCurrencyError
    end
  end

  def self.gateway_cls(gateway)
    "#{gateway.classify}PaymentGateway".constantize
  end

  def authorize_payment(payment, data:{})
    raise NotImplementedError
  end

  def void_payment(gateway)
    raise NotImplementedError
  end

  def capture_payment(gateway)
    raise NotImplementedError
  end
end

# == Schema Information
#
# Table name: developer_admin_invitations
#
#  id              :integer          not null, primary key
#  sender_id       :integer          not null
#  client_id       :integer          not null
#  recipient_email :string(255)      not null
#  token           :string(255)      not null
#  sent_at         :datetime
#  recipient_id    :integer
#  accepted_at     :datetime
#  first_name      :string(255)
#  last_name       :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#

class DeveloperAdminInvitation < ActiveRecord::Base
  
  belongs_to :sender, :class_name => 'User'
  belongs_to :recipient, :class_name => 'User'
  belongs_to :client

  validates :recipient_email, presence: true

  before_create :generate_token_and_recipient_ref
  after_commit :send_mail, on: :create

  

  def resend_mail
    DeveloperAdminInvitationMailer.invite(self.id).deliver_later
  end

  def send_mail
    DeveloperAdminInvitationMailer.invite(self.id).deliver_later
  end

  def generate_token_and_recipient_ref
    generate_token
    generate_recipient_ref
  end

  def generate_recipient_ref
    self.recipient = User.find_by_email(self.recipient_email)
  end

  def generate_token
    self.token = SecureRandom.hex
  end

  def full_name
     [self.first_name, self.last_name].join(" ")
  end
end

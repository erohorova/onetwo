# == Schema Information
#
# Table name: follows
#
#  id                :integer          not null, primary key
#  user_id           :integer
#  followable_id     :integer
#  followable_type   :string(191)
#  created_at        :datetime
#  updated_at        :datetime
#  deleted_at        :datetime
#  skip_notification :boolean          default(FALSE)
#

class Follow < ActiveRecord::Base
  include EventTracker

  acts_as_paranoid
  has_paper_trail ignore: [:created_at, :updated_at]

  belongs_to :user
  belongs_to :followable, polymorphic: true

  # for joining followed channel
  belongs_to :channel,
             -> {where(follows: {followable_type: 'Channel'})},
             foreign_key: 'followable_id'

  validates :user, presence: true
  validates :followable_id, presence: true
  validates :followable_type, presence: true
  validates_uniqueness_of :user_id, :scope => [:followable_type, :followable_id]
  validate :can_be_followed

  after_commit :record_follow_event, on: :create
  after_commit :reindex_followable, if: :should_be_reindexed?
  after_commit :invalidate_channel_followers_count
  around_destroy :record_unfollow_event

  scope :array_to_relation, -> (array_collection) {where(id: array_collection.map(&:id))}

  def can_be_followed
    errors.add(:followable, "cannot be followed") if (followable && user && followable.respond_to?(:can_be_followed_by?) && !followable.can_be_followed_by?(user))
  end

  after_restore do
    followable.try(:after_followed, user, restore: true)
  end

  after_commit(on: [:create, :update]) do
    followable.try(:after_followed, user)
  end

  after_commit(on: [:destroy]) do
    followable.try(:after_unfollowed, user)
  end

  after_commit on: :create do
    if followable_type == 'User' && !skip_notification
      NotificationGeneratorJob.perform_later(item_id: id, item_type: 'follow')
    end
  end

  # list class methods
  def self.remove_followers(posts, user)
    Follow
        .where(
            followable_id: posts.pluck(:id),
            followable_type: 'Post',
            user_id: user.id)
        .destroy_all
  end

  def organization
    followable.try(:organization)
  end

  def self.followers_to_enqueue(options = nil)
    return if !options.is_a?(Hash) || options.blank?

    # Fetch followers and process in batch of 1000.
    # And, enqueue the card in their feed section.
    Follow
      .select(:id)
      .where(followable_id: options[:followable_id], followable_type: options[:followable_type])
      .find_in_batches do |follows|
        ids = follows.map(&:id)
        FollowedCardBatchEnqueueJobV1.perform_later(follow_ids: ids, card_id: options[:card_id], channel_ids: options[:channel_ids])
    end
  end

  def enqueue_for_follower(card:)
    return unless card.author_id != user&.id && card.visible_to_user(user)

    rationale = {type: "following_#{followable_type.downcase}"}
    rationale["#{followable_type.downcase}_id".to_sym] = followable_id

    enqueued_at = (card['published_at'] || card.published_at) if followable_type == 'User'
    enqueued_at ||= DateTime.now.in_time_zone('UTC') if followable_type == 'Channel'

    # Don't insert card from the author into the authors feed && Also takes care of the old private channel
    {rationale: rationale, enqueued_at: enqueued_at}
  end

  private

  def invalidate_channel_followers_count
    if self.followable_type == 'Channel'
      Channel.invalidate_followers_cache(followable_id)
    end
  end

  def should_be_reindexed?
    followable_type.in?(['User', 'Channel'])
  end

  def reindex_followable
    followable.reindex_async_urgent
  end

  def record_follow_event
    case followable
    when User
      record_follow_action(
          event: Analytics::UserMetricsRecorder::EVENT_USER_FOLLOWED
      )
    when Channel
      record_follow_action(
          event: Analytics::ChannelMetricsRecorder::EVENT_CHANNEL_FOLLOWED
      )
    end
  end

  def record_unfollow_event
    yield if block_given?
    return if persisted?
    case followable
    when User
      record_follow_action(
          event: Analytics::UserMetricsRecorder::EVENT_USER_UNFOLLOWED
      )
    when Channel
      record_follow_action(
          event: Analytics::ChannelMetricsRecorder::EVENT_CHANNEL_UNFOLLOWED
      )
    end
  end

  def record_follow_action(event:)
    event_args = {
        event: event,
        actor: :user,
        job_args: [{follower: :user}, {followed: :followable}]
    }

    if followable.class == Channel
      event_args.merge!(exclude_job_args: [:org])
    end

    record_custom_event(event_args)
  end
end

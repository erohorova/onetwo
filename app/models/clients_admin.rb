# == Schema Information
#
# Table name: clients_admins
#
#  id        :integer          not null, primary key
#  client_id :integer
#  admin_id  :integer
#

class ClientsAdmin< ActiveRecord::Base
  belongs_to :admin, class_name: "User"
  belongs_to :client

  validates_presence_of :admin
  validates_presence_of :client

  validates_uniqueness_of :client_id, :scope => :admin_id, :message => "can't have duplicate admins assigned"
  
  # validates :client_admin_id, uniqueness: { scope: :client_id }, if: :client_admin_id
end

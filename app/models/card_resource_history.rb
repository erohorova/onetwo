class CardResourceHistory
  include Elasticsearch::Persistence::Model

  ATTRIBUTES_TO_RECORD = %w(url image_url video_url embed_html)

  self.index_name = "card_resource_#{Rails.env}"

  attribute :updated_fields, Array, mapping: {type: 'object'}

  class << self
    def create_new_version(resource_id, _user_id, changes)
      new(
        id: resource_id,
        updated_fields: [changes]
      ).save
    end

    def get_changes(prev_changes)
      before = prev_changes[0].blank? ? nil : Resource.find_by(id: prev_changes[0])
      after = prev_changes[1].blank? ? nil : Resource.find_by(id: prev_changes[1])
      changes = [before, after].each_with_object([]) do |obj, arr|
        delta = ATTRIBUTES_TO_RECORD.each_with_object({}) do |attr, h|
          h[attr] = obj.try(attr)
        end
        arr << delta
      end
      changes[0] == changes[1] ? [] : changes
    end

    def fetch_record(id)
      find(id)
    rescue Elasticsearch::Persistence::Repository::DocumentNotFound
      nil
    end

    def misc_changes(_entity_id, _attr_changes)
      {}
    end
  end
end

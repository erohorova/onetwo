# == Schema Information
#
# Table name: reports
#
#  id              :integer          not null, primary key
#  user_id         :integer          not null
#  reportable_id   :integer
#  reportable_type :string(191)
#  type            :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#  validity        :boolean          default(TRUE)
#

class Report < ActiveRecord::Base

  belongs_to :reporter, class_name: 'User', foreign_key: :user_id
  belongs_to :reportable, polymorphic: true

  validates :reporter, presence: true, uniqueness: {:scope => [:reportable_id, :reportable_type]}
  validate :check_reporting_rights

  after_create :update_reported_item_visibility
  after_commit :reindex_question_async

  scope :valid, -> {where(:validity => true)}



  def check_reporting_rights
    if reportable.group && !reportable.group.is_active_user?(reporter)
      errors.add(:reporter, 'You cannot report this post')
    end

    if reportable.group && reportable.user && reportable.group.is_active_super_user?(reportable.user)
      errors.add(:reportable, 'You cannot report a faculty post')
    end
  end

  def update_reported_item_visibility

    # If has a report from a staff member or report count above threshold
    if reportable.group.is_active_super_user?(reporter) || reportable.valid_reports.count >= 2
      log.info "Report Threshold Crossed with report: #{attributes.to_s}"
      reportable.hidden = true
      reportable.save
      reindex_question_async
    end
  end

  def reindex_question_async
    if self.reportable.class.name == "Question"
      IndexJob.perform_later(self.reportable.class.name, self.reportable.id)
    end
  end
  
  def self.get_reports_by_user(user, reportable_ids: [], reportable_type: nil)
    if reportable_ids.blank?
      return []
    end

    Report.where(:reportable_id => reportable_ids,
                 :reportable_type => reportable_type,
                 :reporter => user).pluck(:reportable_id)
  end

  def organization
    reportable.try(:organization)
  end
end

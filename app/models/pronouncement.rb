# == Schema Information
#
# Table name: pronouncements
#
#  id         :integer          not null, primary key
#  label      :text(65535)      not null
#  scope_id   :integer          not null
#  scope_type :string(20)       not null
#  start_date :datetime         not null
#  end_date   :datetime
#  is_active  :boolean          default(TRUE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Pronouncement < ActiveRecord::Base
  validates :label, :scope_id, :scope_type, :start_date, presence: true
  belongs_to :scope, polymorphic: true

  include EventTracker

  scope :active_only, -> {where(is_active: true)}

  def self.live_pronouncement
    where("start_date <= :date and ( end_date is NULL OR end_date >= :date )", date: Time.now).active_only
  end

  # We only record events if this lambda returns true
  record_if_block = -> (record) {
    record.scope_type == "Organization" && record.scope
  }

  configure_metric_recorders(
    default_actor: lambda { |record| },
    trackers: {
      on_create: {
        event: 'org_announcement_created',
        if_block: record_if_block,
        job_args: [ { org: :scope, announcement: :itself} ],
      },
      on_delete: {
        event: 'org_announcement_deleted',
        if_block: record_if_block,
        job_args: [ { org: :scope, announcement: :itself} ],
      },
      on_edit: {
        event: "org_announcement_edited",
        if_block: record_if_block,
        job_args: [ { org: :scope, announcement: :itself} ],
      }
    }
  )
end

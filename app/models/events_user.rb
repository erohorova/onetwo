# == Schema Information
#
# Table name: events_users
#
#  id          :integer          not null, primary key
#  event_id    :integer
#  user_id     :integer
#  rsvp        :string(255)
#  accepted_at :datetime
#  created_at  :datetime
#  updated_at  :datetime
#

class EventsUser < ActiveRecord::Base

  belongs_to :event
  belongs_to :user

  validates_presence_of :user
  validates_presence_of :event
  validates_inclusion_of :rsvp, in: ['yes', 'no', 'maybe']
  
  after_save :update_webex_attendee

  scope :attending, -> { where(:rsvp => ['yes', 'no']) }
  
  def update_webex_attendee
    if event.webex_conference?
      if ['yes','maybe'].include? rsvp
        event.add_attendee(user)
      else  
        event.remove_attendee(user)
      end
    end
    
  end

  def organization
    event.try(:organization)
  end
end

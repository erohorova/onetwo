class CardMetadatum < ActiveRecord::Base
  include SyncCloneable

  PLAN_TYPES = %w[free paid premium subscription free/paid].freeze
  PAID_PLANS = %w[paid premium subscription].freeze
  ALLOWED_LEVELS = ["beginner", "intermediate", "advanced"].freeze

  belongs_to :card
  before_save :ensure_correct_plan
  validates_inclusion_of :plan, in: PLAN_TYPES
  validates :level, inclusion: {in: ALLOWED_LEVELS }, if: proc { |x| x.level.present? }

  after_commit :reindex_card, :push_to_ecl, if: :reindex_and_push_to_ecl?

  private

  def ensure_correct_plan
    if self.card&.is_paid
      self.plan = 'paid'
    end
  end

  def reindex_card
    card.reindex_async
  end

  def push_to_ecl
    EclCardPropagationJob.perform_later(card_id)
  end

  def reindex_and_push_to_ecl?
    is_average_rating_changed? || is_level_changed?
  end

  def is_level_changed?
    previous_changes.keys.include?('level')
  end

  def is_average_rating_changed?
    previous_changes.keys.include?('average_rating') &&
    previous_changes[:average_rating].map(&:to_i).uniq.length > 1
  end
end

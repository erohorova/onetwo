class WalletBalance < ActiveRecord::Base
  validates :user, presence: true
  validates :amount, presence: true

  belongs_to :user
end

class OrganizationPrice < ActiveRecord::Base
  belongs_to :org_subscription

  validates :amount, :currency, presence: true
  validates :amount, numericality: {greater_than: 0, less_than: 999999.99}
  validates_presence_of :org_subscription
  validates_uniqueness_of :org_subscription, scope: :currency
end

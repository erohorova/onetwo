# == Schema Information
#
# Table name: background_jobs
#
#  id            :integer          not null, primary key
#  job_id        :string(191)
#  job_class     :string(191)
#  job_queue     :string(191)
#  status        :string(191)
#  enqueued_at   :datetime
#  started_at    :datetime
#  finished_at   :datetime
#  scheduled_at  :datetime
#  time_taken    :integer
#  job_arguments :text(65535)
#  truncated     :boolean          default(FALSE)
#

class BackgroundJob < ActiveRecord::Base

  def re_enqueue
    unless truncated
      job_class.constantize.perform_later(*ActiveJob::Arguments.deserialize(JSON.parse(job_arguments)))
    end
  end

  def finished?
    status == 'finished' && finished_at.present?
  end

  def errored?
    status == 'error' && finished_at.present?
  end
end

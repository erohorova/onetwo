# == Schema Information
#
# Table name: badges
#
#  id                        :integer          not null, primary key
#  organization_id           :integer
#  type                      :string(20)
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  image_file_name           :string(255)
#  image_content_type        :string(255)
#  image_file_size           :integer
#  image_updated_at          :datetime
#  image_social_file_name    :string(255)
#  image_social_content_type :string(255)
#  image_social_file_size    :integer
#  image_social_updated_at   :datetime
#

class Badge < ActiveRecord::Base
  belongs_to :organization
  has_many :badgings
  validates :organization_id, presence: true
  validates :image, attachment_presence: true
  has_attached_file :image,
                    styles: {
                      small: '120x120',
                      medium: '320x320',
                      large: '512x512'
                    },
                    default_url: '' # update this with default image for badges
  validates_attachment_content_type :image,
                                    :content_type => /^image\/(png|jpeg|jpg|pjpeg|x-png|gif|tiff)$/,
                                    :message => 'Invalid file type'

  has_attached_file :image_social,
                    styles: {
                      linkedin: '522x368#'
                    },
                    default_url: '' # update this with default image_social for badges
  validates_attachment_content_type :image_social,
                                    :content_type => /^image\/(png|jpeg|jpg|pjpeg|x-png|gif|tiff)$/,
                                    :message => 'Invalid file type'
end

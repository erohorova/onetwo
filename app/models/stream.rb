# == Schema Information
#
# Table name: streams
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  group_id   :integer
#  action     :string(255)
#  item_id    :integer
#  item_type  :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Stream < ActiveRecord::Base
  belongs_to :user
  belongs_to :group

  # Following is the list of supported cases with the corresponding assignments for "item" at the moment:
  # (a) user makes a post/announcement in the forum (item -> post)
  # (b) user makes a comment on a post/announcement/comment (item -> comment)
  # (c) user likes a post/announcement/comment (item -> liked object)
  # (d) user approecs a post/comment (item -> approved object)
  # (e) user joins a private group (item -> private group)
  belongs_to :item, polymorphic: true

  scope :forum_activities, -> { where action: ['commented', 'asked', 'posted'] }

  def snippet
    item.respond_to?(:snippet) ? item.snippet : ''
  end

  def message
    class_name = item.class.name
    item_class_name = class_name == 'Question' ? 'Discussion' : (class_name == "PrivateGroup" ? "Group" : class_name)

    case action
    when 'commented'
      if item.commentable_type == 'Post'
        msg = "commented on discussion"
      else
        msg = "responded to comment"
      end
    when 'liked', 'approved'
      msg = "#{action} the #{item_class_name.humanize.downcase}"
    when 'joined'
      msg = "#{action} #{item_class_name.humanize.downcase.indefinitize}"
    when 'asked'
      msg = "started discussion"
    when 'posted'
      msg = "made #{item_class_name.humanize.downcase.indefinitize}"
    end

  end

  def is_mobile?
    item.respond_to?('is_mobile?') ? item.is_mobile? : false
  end

  def organization
    item.try(:organization)
  end
end

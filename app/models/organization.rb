# == Schema Information
#
# Table name: organizations
#
#  id                                   :integer          not null, primary key
#  name                                 :string(255)
#  description                          :string(1024)
#  slug                                 :string(191)
#  image_file_name                      :string(255)
#  image_content_type                   :string(255)
#  image_file_size                      :integer
#  image_updated_at                     :datetime
#  created_at                           :datetime
#  updated_at                           :datetime
#  mobile_image_file_name               :string(255)
#  mobile_image_content_type            :string(255)
#  mobile_image_file_size               :integer
#  mobile_image_updated_at              :datetime
#  host_name                            :string(191)
#  client_id                            :integer
#  is_open                              :boolean          default(FALSE)
#  co_branding_logo_file_name           :string(255)
#  co_branding_logo_content_type        :string(255)
#  co_branding_logo_file_size           :integer
#  co_branding_logo_updated_at          :datetime
#  co_branding_mobile_logo_file_name    :string(255)
#  co_branding_mobile_logo_content_type :string(255)
#  co_branding_mobile_logo_file_size    :integer
#  co_branding_mobile_logo_updated_at   :datetime
#  is_registrable                       :boolean          default(TRUE)
#  send_weekly_activity                 :boolean          default(FALSE)
#  status                               :string(255)      default("active")
#  show_sub_brand_on_login              :boolean          default(TRUE)
#  savannah_app_id                      :integer
#  redirect_uri                         :string(255)
#  social_enabled                       :boolean
#  xapi_enabled                         :boolean          default(FALSE)
#  users_count                          :integer          default(0)
#  show_onboarding                      :boolean          default(TRUE)
#  enable_role_based_authorization      :boolean          default(FALSE)
#  splash_image_file_name               :string(255)
#  splash_image_content_type            :string(255)
#  splash_image_file_size               :integer
#  splash_image_updated_at              :datetime
#  favicon_file_name                    :string(255)
#  favicon_content_type                 :string(255)
#  favicon_file_size                    :integer
#  favicon_updated_at                   :datetime
#  custom_domain                        :string(191)
#  enable_analytics_reporting           :boolean          default(FALSE)
#  comments_count                       :integer          default(0)
#  whitelabel_build_enabled             :boolean          default(FALSE)
#

class Organization < ActiveRecord::Base
  include IdentityCache
  include PubnubChannels
  extend FriendlyId
  include EventTracker

  HOSTNAME_FORMAT = /\A([A-Z]|[a-z]|[0-9])+\z/
  ORG_STATUS = ["active", "inactive", "pending"].freeze
  DEFAULT_ORG_HOSTNAME = 'www'

  friendly_id :name, use: [:slugged, :history]

  belongs_to :client
  has_many :users
  has_many :channels
  has_many :cards
  has_many :video_streams
  has_many :roles
  has_many :teams
  has_many :configs, as: :configable
  has_many :cobrand_configs, -> { where category: 'cobrand' }, class_name: 'Config', as: :configable
  cache_has_many :cobrand_configs, inverse_name: :configable
  has_many :org_ssos, dependent: :destroy
  has_many :ssos, through: :org_ssos
  has_many :organization_email_domains, dependent: :destroy

  has_many :org_subscriptions
  has_many :orders
  has_many :mailer_configs
  has_one :webex_config, dependent: :destroy
  has_one :organization_profile, dependent: :destroy
  has_many :organization_prices, dependent: :destroy
  has_many :groups
  has_many :resource_groups
  has_many :structures
  has_many :activity_streams, dependent: :destroy
  has_many :developer_api_credentials, dependent: :destroy
  has_many :sources_credentials, dependent: :destroy
  has_one :xapi_credential
  has_many :organization_integrations
  has_many :integrations, through: :organization_integrations
  has_many :custom_fields
  has_many :partner_centers, dependent: :destroy

  has_many :all_clcs, class_name: 'Clc', dependent: :destroy
  has_many :clcs, as: :entity, dependent: :destroy
  has_many :card_badges
  has_many :quarters
  has_many :comments, as: :commentable, dependent: :destroy
  has_many :clc_badges
  has_many :career_advisors, dependent: :destroy
  has_many :embargos, dependent: :destroy
  has_many :embargo_files, dependent: :destroy
  has_many :profiles, dependent: :destroy
  has_many :dynamic_group_revisions, dependent: :destroy

  has_many :pronouncements, as: :scope
  has_many :email_templates
  has_many :topic_requests
  has_many :workflows
  has_many :widgets, dependent: :destroy

  accepts_nested_attributes_for :configs, :quarters

  has_attached_file :image, default_url: 'default_org_image_:style.jpg', styles: { small: '120x120',medium: '320x320' }
  validates_attachment_content_type :image,  content_type: /^image\/(png|jpeg|jpg|pjpeg|x-png|gif|tiff)$/,
                                    message: 'Invalid file type'

  has_attached_file :mobile_image, default_url: 'default_org_image_:style.jpg', styles: { small: '120x120',medium: '320x320' }
  validates_attachment_content_type :mobile_image, content_type: /^image\/(png|jpeg|jpg|pjpeg|x-png|gif|tiff)$/,
                                    message: 'Invalid file type'

  has_attached_file :favicon, styles: { tiny: '16x16', small: '32x32', medium: '96x96', large: '192x192' }
  validates_attachment_content_type :favicon, content_type: /^image\/(png|jpeg|jpg|pjpeg|x-png|gif|tiff)$/,
                                    message:  'Invalid file type'

  has_attached_file :splash_image, styles:  { small: '120x120', medium: '320x320' }
  validates_attachment_content_type :splash_image, content_type: /^image\/(png|jpeg|jpg|pjpeg|x-png|gif|tiff)$/,
                                    message: 'Invalid file type'


  has_attached_file :co_branding_logo, default_url: 'default_org_image_:style.jpg'
  validates_attachment_content_type :co_branding_logo, content_type: /^image\/(png|jpeg|jpg|pjpeg|x-png|gif|tiff)$/,
                                    message: 'Invalid file type'

  validates_presence_of :name
  validates_presence_of :description
  validates :host_name, presence: true, uniqueness: true, format: { with: Organization::HOSTNAME_FORMAT }
  validate :ensure_valid_custom_domain, if: :custom_domain?
  validates :custom_domain, uniqueness: true, if: :custom_domain?
  validates :status, inclusion: ORG_STATUS

  before_create :default_sso

  after_create :create_general_channel
  after_create :create_default_config
  after_create :create_default_roles
  after_create :create_admin_user
  after_create :create_org_card_badges
  after_create :create_org_level_team
  after_create :create_default_skill_for_career_advisor

  after_commit :create_or_update_roles, :create_analytics_reporting_configs
  after_commit :add_onboarding_options, :enable_sociative, on: :create
  after_commit :create_custom_templates, on: :create

  # around_update :push_org_updated_influx_metric

  scope :exclude_default_org, -> {where.not(host_name: DEFAULT_ORG_HOSTNAME)}
  scope :analytics_reporting_enabled, -> { where(enable_analytics_reporting: true) }
  scope :automated_pinned_carousel_enabled, -> { joins(:configs)
    .where("configs.name = 'enable_automated_pinned_cards' and configs.value = 'true'") }

  configure_metric_recorders(
    # default_actor: lambda {|record| record.admins&.first},
    default_actor: lambda {|record| },
    trackers: {
      on_create: {
        event: "org_created",
        job_args: [ {org: :itself} ],
      },
      on_edit: {
        event: 'org_edited',
        job_args: [ {org: :itself}]
      }
    }
  )

  def enable_sociative
    EclSettingsSyncJob.perform_later(self.id)
  end

  def create_custom_templates
    CreateCustomEmailTemplateJob.perform_later(self.id)
  end

  def create_org_card_badges
    Settings.badge_image_urls.each do |image_urls|
      clc_badges.create(
        image: open(image_urls[0]),
        image_social: open(image_urls[1]),
        image_original_url: image_urls[0],
        is_default: true
      )
      card_badges.create(
        image: open(image_urls[0]),
        image_social: open(image_urls[1]),
        image_original_url: image_urls[0],
        is_default: true
      )
    end
  end

  def create_admin_user
    user = User.new(
                 first_name: 'Admin',
                 last_name: 'User',
                 organization: self,
                 organization_role: 'admin',
                 is_edcast_admin: true,
                 password: User.random_password)
    user.skip_indexing = true
    user.skip_generate_images = true
    user.skip_reconfirmation!
    user.skip_confirmation!
    user.save!
  end

  def create_general_channel
    Channel.create(label: "General", description: "For insights relevant to the entire organization",
                   only_authors_can_post: false,
                   organization_id: self.id, is_general: true)
  end

  def self.default_org
    Rails.cache.fetch('default-organization', expires_in: 1.hour) do
      where(host_name: 'www').first
    end
  end

  def everyone_team
    teams.where(is_everyone_team: true).first
  end

  def general_channel
    channels.find_by(is_general: true)
  end

  # TODO: remove join table after transition complete
  def has_access? _user
    _user && is_user?(_user)
  end

  def has_admin_access? user
    user.organization_id == id && user.is_admin_user?
  end

  def is_user? user
    user.organization_id == id
  end

  def is_member? user
    user.organization_id == id && user.organization_role == 'member'
  end

  def is_admin? user
    user.organization_role == 'admin' || (Organization.default_org&.id == id && user.is_edcast_dot_com_user?)
  end

  def is_active?
    status.downcase.eql?("active")
  end

  def open?
    is_open
  end

  def closed?
    !open?
  end

  # AIM:
  # Check whether user email (through email-signup or SSO) can be registered at organization
  # 1. true  if all domains are allowed
  # 2. false if all domains are not allowed
  # 3. false if whitelisted domains dont include email domain
  # 4. true if whitelisted domains includes email domain
  # 5. false if email domain is a part of disallowed_email_domains
  # 6. true if email domain is not a part of disallowed_email_domains
  def email_registrable?(email)
    domains = allowed_email_domains
    disallowed_emails = (domains.try(:index,"not:") == 0)

    allow_all = (domains.blank? || domains == 'all')
    allow_none = (domains == 'none')

    return true if allow_all
    return false if allow_none
    return false if email.blank?

    email_domain = email.split('@').last.downcase

    allow_user = if disallowed_emails
      !domains.split('not:')[1].split(',').select{|d| d.downcase.strip.ends_with?(email_domain)}.any?
    else
      domains.split(',').select{|d| d.downcase.strip.ends_with?(email_domain)}.any?
    end

    allow_user
  end

  def is_sign_up_allowed?
    allowed_email_domains != 'none'
  end

  def home_page
    @home_page ||= custom_domain? ? custom_domain_url : edcast_home_page
  end

  def secure_home_page
    "https://#{host_name}.#{Settings.platform_domain}"
  end

  def edcast_home_page
    @edcast_home_page ||=  "#{Settings.serve_images_on_ssl ? 'https' : 'http'}://#{host_name}.#{Settings.platform_domain}#{Settings['platform_port'] ? ":#{Settings['platform_port']}" : ''}"
  end

  def custom_domain_url
    @custom_domain_url ||= "https://#{custom_domain}"
  end

  def host
    @host ||= custom_domain? ? custom_domain : edcast_host
  end

  def host_url
    "https://#{host}"
  end

  def edcast_host
    @edcast_host ||= "#{host_name}.#{Settings.platform_domain}"
  end

  def photo(size = nil)
    return image.url(size) if size
    image.url
  end

  def mobile_photo(size = nil)
    return mobile_image.url(size) if size
    mobile_image.url
  end

  # NOTE:
  #   Only used by mobile to map Savannah course to EdCast group.
  #   Web completely uses Savannah APIs to list all courses in an organization.
  #   Mobile completely uses Savannah APIs to list all courses in an organization.
  #   Except: This is used to find out group and their related posts since forum posts do have post id.
  def featured_courses
    ResourceGroup
      .not_closed
      .where(organization_id: id)
      .where.not(website_url: nil)
      .where.not(is_private: true)
      .where(is_hidden: false)
      .recent
  end

  def self.cache_find_by_host_name(hostname)
    Rails.cache.fetch("org-by-hostname:#{hostname}", expires_in: 1.hour, unless_nil: true) do
      Organization.find_by_host_name(hostname)
    end
  end

  def available_ssos
    Rails.cache.fetch("ssos-by-org:#{self.id}", expires_in: 1.hour) do
      self.ssos.includes(:org_ssos).where(org_ssos: {is_enabled: true}).pluck(:name, 'org_ssos.label_name').uniq.to_h
    end
  end

  def favicons
    images = {}
    favicon.styles.map {|style, obj| images.merge!(style => favicon.url(style))} if favicon.file?
    images
  end

  def is_auth_enabled?(auth_name)
    available_ssos.keys.include?(auth_name)
  end

  def admins
    users.where(organization_role: 'admin')
  end

  def real_admins
    # the system generated admin user for every org
    admins.where.not(is_edcast_admin: true)
  end

  # AIM:
  #   Assessable pathways with atleast one question card with `is_correct` as `true` in quiz_question_options.
  #   Poll card will have atleast one correct option as TRUE. Such cards are question cards.
  def assessment_pathways
    Card
      .joins("INNER JOIN card_pack_relations on cards.id = card_pack_relations.cover_id and cards.organization_id = #{self.id}")
      .joins("INNER JOIN quiz_question_options on card_pack_relations.from_id = quiz_question_options.quiz_question_id and quiz_question_options.quiz_question_type = 'Card'")
      .where("quiz_question_options.is_correct = true")
      .uniq
  end

  def real_users
    # the system generated admin user for every org
    users.not_is_edcast_admin.not_suspended.completed_profile
  end

  def exclude_user_ids_for_analytics
    users.where("is_edcast_admin = 1 OR is_suspended = 1 OR is_complete = 0").pluck(:id)
  end

  def members
    users.where(organization_role: 'member')
  end

  def can_deep_link_to_social?
    _available_ssos = self.available_ssos.keys
    return _available_ssos.length == 1 && !_available_ssos.include?('email')
  end

  def self.get_config_enabled_orgs(config_name, value)
    joins(:configs).where(configs: {name: config_name, value: value, configable_type: 'Organization'})
  end

  def self.valid_hostname?(hostname)
    !HOSTNAME_FORMAT.match(hostname).nil?
  end

  def fetch_cobrand_configs_with_defaults
    #note: key should always be lower case. value can be anything
    response = {}.merge(Settings.cobrand_defaults)
    fetch_cobrand_configs.map do |config|
      response[config.name] = config.parsed_value
    end
    response
  end

  def invalidate_config_cache(config = nil)
    if config
      Rails.cache.delete("org:#{id}-configs-#{config.name}")
    else
      configs.each {|cnfig| Rails.cache.delete("org:#{id}-configs-#{cnfig.name}") }
    end
  end

  def self.remind_admin_to_invite_users
    orgs = self.where("created_at >= ? and created_at <= ?", 3.days.ago, 2.days.ago)
    orgs += self.where("created_at >= ? and created_at <= ?", 6.days.ago, 5.days.ago)
    orgs += self.where("created_at >= ? and created_at <= ?", 11.days.ago, 10.days.ago)

    orgs.each do |org|
      unless org.admins.blank?
        if org.members.count == 0
          org.real_admins.each { |admin| OrgAdminReminderNotificationJob.perform_later(org, admin) }
        end
      else
        log.warn "Org #{org.id} has no admin"
      end
    end
  end

  def self.setup_weekly_activity_jobs
    Organization.select(:id, :send_weekly_activity).where(send_weekly_activity: true).each do |organization|
      OrganizationSendWeeklyActivityJob.perform_later(organization.id)
    end
  end

  def send_weekly_activity_email
    self.users.select(:id, :email, :is_suspended).where.not(email: nil, is_suspended: true, is_complete: false).find_in_batches do |users|
      user_ids = users.collect(&:id)
      OrgUserSendWeeklyActivityEmailBatchJob.perform_later(organization_id: self.id, user_ids: user_ids)
    end
  end

  def weekly_activity
    Rails.cache.fetch("weekly_activity_in_org:#{self.id}",  expires_in: 1.hour) do
      OrganizationWeeklyActivityService.new(self).activity
    end
  end

  def quarter_config
    Rails.cache.fetch("organization_quarter_config_#{self.id}",  expires_in: 1.hour) do
      self.quarters.all
    end
  end

  def weekly_activity_mail_attributes
    weekly_activity_data = weekly_activity
    markdown = Redcarpet::Markdown.new(Redcarpet::Render::StripDown)
    [:insights, :pathways].each do |activities|
      weekly_activity_data[activities].map do |activity|
        activity.slice!(:author, :small_image_url, :message, :slug, :title)
        activity[:title] = markdown.render(activity[:title]).gsub(/\n/,'').try(:truncate, 200) unless activity[:title].blank?
        activity[:message] = markdown.render(activity[:message]).gsub(/\n/,'').try(:truncate, 200) unless activity[:message].blank?
        activity[:author].slice!(:picture, :handle, :name) if !activity[:author].nil?
      end
    end

    weekly_activity_data[:video_streams].map do |video_stream|
      video_stream.slice!(:creator, :image, :slug, :name)
      video_stream[:creator].slice!(:photo, :handle, :name) if !video_stream[:creator].nil?
    end

    weekly_activity_data
  end

  # After creating organization
  # Get default org master role and permission
  # And create smame role for organization and permissions
  def create_seed_roles
    Role.seed_roles.includes(:permissions).each do |seed_role|
      role = self.roles.where(:name=>seed_role.name,:master_role=>seed_role.master_role).present?
      if role.blank?
        role = self.roles.create(:name=>seed_role.name,:master_role=>seed_role.master_role)
      end
      role.permissions = seed_role.permissions
    end
  end

  def get_new_users_count(period: , offset: )
    timerange = PeriodOffsetCalculator.timerange(period: period, offset: offset)
    real_users.where("created_at >= (:begin_time_period) AND created_at < (:end_time_period)", begin_time_period: timerange.first, end_time_period: timerange.last).count
  end

  def get_new_users_count_over_span(begin_time: , end_time: )
    real_users.where("created_at >= (:begin_time) AND created_at < (:end_time)", begin_time: begin_time, end_time: end_time).count
  end

  def get_new_users_drill_down_data(period: , offset: )
    drill_down_timeranges = PeriodOffsetCalculator.drill_down_timeranges(period: period, offset: offset)
    get_new_users_drill_down_data_for_timeranges(timeranges: drill_down_timeranges)
  end

  # timeranges: -> PeriodOffsetCalculator#drill_down_timeranges_for_timerange
  # [[Time.now, Time.now + 1], [Time.now + 1, Time.now + 2]]
  def get_new_users_drill_down_data_for_timeranges(timeranges:)
    all_new_users = real_users.where("created_at >= (:begin_time_period) AND created_at < (:end_time_period)", begin_time_period: timeranges.first.first, end_time_period: timeranges.last.last).select(:id, :created_at)
    timeranges.map{|tr| all_new_users.select{|user| user.created_at >= tr.first && user.created_at < tr.last}.length}
  end

  def real_users_count_before(time)
    real_users.where("created_at <= (:time)", time: time).count
  end

  def sme_role
    roles.where(name: 'sme', master_role: true).first
  end

  def member_role
    roles.where(name: 'member', master_role: true).first
  end

  def sme_ids
    Rails.cache.fetch("organization_#{id}_sme_ids", expires_in: 5.minutes) do
      role = sme_role
      user_ids = role.users.pluck(:id) if role.present?
    end
  end

  def onboarding_hidden?
    !show_onboarding?
  end

  def only_email_sso_enabled?
    available_ssos.keys == ['email']
  end

  def team_level_metrics_for(q: nil, from_date:, to_date:, team_ids: [])
    period = :day
    time_period_offsets_from = PeriodOffsetCalculator.applicable_offsets(from_date)[period]
    time_period_offsets_to = PeriodOffsetCalculator.applicable_offsets(to_date)[period]

    filtered_query = TeamLevelMetric.where(period: :day, offset: (time_period_offsets_from..time_period_offsets_to), organization_id: self.id)
    filtered_query = filtered_query.where(team_id: team_ids) if team_ids.present?
    unless q.blank?
      filtered_query = filtered_query.joins(:team).where("teams.name like ?", "%#{q}%")
    end

    filtered_query.group("team_id").
      select("team_id, sum(smartbites_created) as smartbites_created, sum(smartbites_consumed) as smartbites_consumed, sum(time_spent) as time_spent, sum(smartbites_comments_count) as smartbites_comments_count, sum(smartbites_liked) as smartbites_liked")
  end

  def public_pubnub_channel_name
    self.pubnub_channel_name(as_type: 'public', item_type: 'Organization')
  end

  # Returns:
  #   1. Onboarding options configured for an org.
  #   2. If options unavailable for an org, defaults to `profile_setup`.
  #   Makes sure `profile_setup` is always a part of onboarding flow.
  def onboarding_options
    config_value = get_settings_for('onboarding_options')
    default_steps = UserOnboarding::DEFAULT_ONBOARDING_STEPS
    configured_steps = config_value.presence || {}
    configured_steps.merge(default_steps).sort_by { |k,v| v }.to_h
  end

  def content_level_metrics_for(q: nil, from_date:, to_date:, card_ids: [])
    period = :day
    time_period_offsets_from = PeriodOffsetCalculator.applicable_offsets(from_date)[period]
    time_period_offsets_to = PeriodOffsetCalculator.applicable_offsets(to_date)[period]
    filtered_query = ContentLevelMetric.where(period: period, offset: (time_period_offsets_from..time_period_offsets_to), organization_id: self.id)
    filtered_query = filtered_query.where(content_id: card_ids) if card_ids.present?

    if q.present?
      filtered_query = filtered_query
        .joins("left join cards on cards.id = content_level_metrics.content_id")
        .where("cards.title like :search_term OR cards.message like :search_term", search_term: "%#{q}%")
    end

    filtered_query.group("content_id, content_type").
      select("content_id, content_type, sum(views_count) as views_count, sum(content_level_metrics.comments_count) as comments_count, sum(likes_count) as likes_count, sum(completes_count) as completes_count, sum(content_level_metrics.bookmarks_count) as bookmarks_count")
  end

  def has_role?(role_name)
    roles.where(name: role_name).exists?
  end

  def send_user_content_analytics(report_type: 'content')
    timestamp = Time.now.prev_week.utc
    period = :week
    user_ids = real_users.where(organization_role: 'admin').pluck :id
    aws_url = AnalyticsReport::UserContentActivityReport.new(organization: self, timestamp: timestamp, period: period, report_type: report_type).generate_report
    snippet = PeriodOffsetCalculator::timestamp_to_readable_period(timestamp: timestamp, period: period)

    UserMailer.send_organization_report(s3_url: aws_url, user_ids: user_ids, email_title_snippet: snippet).deliver_later
  end

  # Find organization by host
  # case 1. spark.edcast.com
  # case 2. custom domain, learn.something.com
  def self.find_by_request_host(host)
    if host.ends_with?(Settings.platform_domain)
      find_by_host_name(host.split('.').first.try(:downcase))
    else
      find_by_custom_domain(host)
    end
  end

  def send_org_analytics
    user_ids = real_users.where(organization_role: 'admin').pluck :id
    timestamp = Time.now.utc - 1.day
    aws_url = AnalyticsReport::OrganizationAnalyticsReport.new(organization: self, timestamp: timestamp, period: :day).generate_report
    snippet = PeriodOffsetCalculator::timestamp_to_readable_period(timestamp: timestamp)

    UserMailer.send_organization_report(s3_url: aws_url, user_ids: user_ids, email_title_snippet: snippet).deliver_later
  end

  def current_todays_learning
    expected_version   = nil
    configured_version = get_settings_for('todays_learning_version')
    default_version    = Settings.todays_learning.default_version

    if configured_version.blank?
      expected_version = default_version
    else
      expected_version = configured_version.compare_with(default_version)
    end

    expected_version.downcase
  end

  def federated_email_sso
    self.org_ssos.select { |org_sso| org_sso.provider_name == 'email' }.first
  end

  # below methods will be used for filtering activity_streams
  def can_comment?(user)
    true
  end

  def visible_to_user(user)
    true
  end

  def snippet
    name
  end

  def get_app_deep_link_id_and_type_v2
    [id, 'organization']
  end

  #****************************************************************
  # ALL CONFIG RELATED METHODS FOLLOW HERE
  #****************************************************************

  def get_settings_for(config_name, expires_in: 6.hours)
    Rails.cache.fetch("#{cache_key}/#{config_name}", expires_in: expires_in) do
      configs.find_by_name(config_name).try(:parsed_value)
    end
  end

  def mobile_uri_scheme
    Rails.cache.fetch("#{self.id}_mobile_uri_scheme", expires_in: 1.day) do
      configs.find_by_name("mobile_uri_scheme").try(:parsed_value) || 'edcastapp'
    end
  end

  def okta_native_redirect
    "#{mobile_uri_scheme}:#{Settings.okta.native_platform}"
  end

  def get_my_assignments_default_value
    org_customization_config = self.configs.find_by(name: "OrgCustomizationConfig")

    if org_customization_config.present?
     org_customization_config.parsed_value["web"]["mlp"]["web/mlp/myAssignments"]["defaultValue"]
    end
  end

  def org_default_language
    get_settings_for('DefaultOrgLanguage')
  end

  def leaderboard_enabled?
    get_settings_for('leaderboard')
  end

  def reporting_content_enabled?
    get_settings_for('reporting_content') || false
  end

  def course_paywall_enabled?
    get_settings_for('course_paywall_enabled')
  end

  # This configuration is specific to WEF requirement REF: EP-8930
  def auto_assign_feature_enabled?
    get_settings_for('auto_assignment_feature_enabled')
  end

  def notification_limit_days
    get_settings_for('notificationLimitDays')
  end

  def auto_mark_as_complete_enabled?
    get_settings_for('auto_mark_as_complete_scorm_cards') || false
  end

  def okta_api_token
    get_settings_for('OktaApiToken')
  end

  def linked_okta_groups
    get_settings_for('OktaDefaultGroups')
  end

  def linked_okta_app
    get_settings_for('OktaDefaultApplication')
  end

  def account_lockout_enabled?
    get_settings_for('enable_account_lockout')
  end

  def number_of_learning_items
    get_settings_for('NumberOfDailyLearningItems') || 5
  end

  def clc_minutes_for_day
    get_settings_for('clc_minutes_for_day') || 60
  end

  def wallet_enabled?
    get_settings_for('WalletEnabled')
  end

  def fetch_splash_image_url
    get_settings_for('custom_splash_screen')
  end

  def has_dogwood_version?
    dogwood_release_enabled = get_settings_for('app.config.dogwood_release')
    dogwood_release_enabled || client.try(:has_dogwood_version?)
  end

  def are_filters_disabled?
    left_panel_enabled = get_settings_for('app.forum.left_panel.disable')
    left_panel_enabled || client.try(:are_filters_disabled?)
  end

  def okta_enabled?
    _okta_enabled = get_settings_for('app.config.okta.enabled')
    _okta_enabled.present? && okta_api_token.present?
  end

  def current_okta_domain
    config = get_settings_for('OktaDefaultApplication')

    if config
      config['okta_domain']
    end
  end

  def public_profile_enabled?
    get_settings_for('public_profile_enabled')
  end

  def allowed_email_domains
    get_settings_for('AllowedEmailDomains')
  end

  def notifications_and_triggers_enabled?
    get_settings_for('feature/notifications_and_triggers_enabled')
  end

  def send_no_mails?
    get_settings_for('feature/send_no_mails').try(:to_bool)
  end

  def only_sme_on_people_carousel?
    get_settings_for('enable_only_sme_on_people_carousel')
  end

  def trending_org_pathway?
    get_settings_for('enable_trending_org_pathway')
  end

  def trending_org_journey?
    get_settings_for('enable_trending_org_journey')
  end

  def automated_pinned_carousel?
    get_settings_for('enable_automated_pinned_cards')
  end

  def autofollow_based_on_topic?
    get_settings_for('enable_channel_autofollow_based_on_topic')
  end

  def search_with_sociative?
    get_settings_for('sociative/search')
  end

  def default_topics
    get_settings_for('default_topics')
  end

  def disable_user_search?
    get_settings_for('disable_user_search')
  end

  def get_channel_permissions_for(user)
    enabled = get_settings_for('permissions/channel/member_can_create_channel')
    # public pages dont have user signed in
    user ? (enabled ? true : (is_admin?(user) || user.can_post_to_channel)) : false
  end

  def get_sharing_permissions
    get_settings_for('permissions/social_sharing/can_share_channel_on_social_nw')
  end

  def web_session_timeout
    get_settings_for('web_session_timeout')
  end

  def inactive_web_session_timeout
    get_settings_for('inactive_web_session_timeout')
  end

  def expire_session_on_closing_browser
    get_settings_for('sessionExpireOnBrowserClose')
  end

  def fetch_readable_card_types_configs
    get_settings_for('readable_card_types', expires_in: 1.week)
  end

  def get_download_app
    get_settings_for('mailer/show_app_download') || true
  end

  def custom_footer_options
    get_settings_for('footer_options').presence || {}
  end

  def mail_custom_css_options
    get_settings_for('mail_custom_css').presence || {}
  end

  def encryption_payload?
    get_settings_for('encryption_payload')
  end

  def enable_edcast_logo
    value = get_settings_for('enable_edcast_logo')
    return value.nil? ? true : value
  end

  def bia_enabled?
    value = get_settings_for('enabled_bia')
    return value.nil? ? true : value
  end

  def custom_quarters_enabled?
    value = get_settings_for('enable_custom_quarters')
    return value.nil? ? false : value
  end

  def simplified_workflow_enabled?
    get_settings_for('simplified_workflow').presence || false
  end

  def workflow_service_enabled?
    get_settings_for('workflow_service')&.to_bool || false
  end

  def filestack_url_expire_after_seconds
    value = get_settings_for('filestack_url_expire_after_seconds')
    return value.nil? ? nil : value
  end

  def workflow_input_batch_size
    get_settings_for('workflow_input_batch_size').presence || 500
  end

  def from_address
    mailer_config.present? ? mailer_config.from_address : 'admin@edcast.com'
  end

  # Earlier organization was having only one mailer config
  # Now organization can have multiple mailer configs
  # This method return active mailer config for organization
  def mailer_config
    Rails.cache.fetch("active_mailer_config_for_#{id}", expires_in: 2.hours) do
      if ( custom_org_config = MailerConfig.where(organization_id: id, from_admin: true, is_active: true).first ).present?
        custom_org_config
      elsif (org_config = MailerConfig.where(organization_id: id, from_admin: false).first ).present?
        org_config
      else
        nil
      end
    end
  end

  def team_accessible_source_ids
    Rails.cache.fetch("organization_#{self.id}_team_private_source_ids", expires_in: 7.days) do
      teams.accessible_source_ids.flatten.uniq || []
    end.flatten
  end

  def get_channel_performance(from_date, to_date, channel_ids: [], search_term: nil, expires_in: 1.hour)
    org = "cp-#{self.id}"
    from = from_date.strftime('%m-%d-%Y')
    to = to_date.strftime('%m-%d-%Y')
    last_updated = Channel.maximum(:updated_at).to_time
    cache_key = [org, from, to, last_updated, search_term].join('-')
    Rails.cache.fetch(cache_key, expires_in: expires_in) do
      AnalyticsReport::ChannelPerformance.new(
          organization_id: self.id,
          from_date: from_date.beginning_of_day,
          to_date: to_date.end_of_day,
          channel_ids: channel_ids
      ).get_records(search_term)
    end
  end

  def org_subscription_enabled?
    organization_profile&.member_pay?
  end

  def shareable_channels
    mapped_org_ids = OrgMapping.where(child_organization_id: id).where.not(parent_organization_id: id)
      .pluck(:parent_organization_id)

    # to only fetch mappings from orgs that have the config to share channels enabled
    shared_org_ids = mapped_org_ids.select{|org_id|
      Organization.find_by(id: org_id).try(:share_channels_enabled?)
    }
    cloned_channels = Channel.where(organization_id: id)
      .where("parent_id is not null").select(:parent_id)

    # to fetch channels from orgs that shared with the org
    # but not channels that the org has already cloned.
    Channel.where(organization_id: shared_org_ids, shareable: true).order("updated_at DESC")
  end

  def share_channels_enabled?
    get_settings_for('feature/shared_channels')
  end

  def invalidate_trashed_ecl_ids
    Rails.cache.delete(trashed_ecl_ids_cache_key)
  end

  # TRASHED CARD IDS FOR ORGANIZATION
  def trashed_ecl_ids
    Rails.cache.fetch(trashed_ecl_ids_cache_key, expires_in: 30.days) do
      Card.where(organization: self).only_deleted.joins(
        'INNER JOIN card_reportings ON
        card_reportings.card_id = cards.id AND
        card_reportings.deleted_at IS NOT NULL').pluck(:ecl_id).compact
    end
  end

  def names_roles
    key = Role.maximum(:updated_at)
    Rails.cache.fetch("names-roles-#{id}-#{key}") do
      roles.pluck(:default_name)
    end
  end

  def trusted_collaborators_enabled?
    get_settings_for('trusted_collaborators_enabled')
  end

  def s3_bucket_name_and_region
    [
      get_settings_for('custom_images_bucket_name'),
      get_settings_for('custom_images_region')
    ]
  end

  def splash_image_url
    url = fetch_splash_image_url || splash_image.url
    if (url && (url.include?("cdn.filestackcontent.com") || url.include?("#{secure_home_page}/uploads/")))
      url = Filestack::Security.read_filestack_url(url)
    end
    url
  end

  private

  def trashed_ecl_ids_cache_key
    "organization_trashed_ecl_ids/#{self.id}"
  end

  def add_onboarding_options
    config = get_settings_for('onboarding_options')

    if config.nil?
      onboarding_options = {}
      onboarding_options.merge!(UserOnboarding::DEFAULT_ONBOARDING_STEPS)
      onboarding_options.merge!(UserOnboarding::OPTIONAL_ONBOARDING_STEPS)

      self.configs.create(
        category: 'settings',
        data_type: 'json',
        name: 'onboarding_options',
        value: onboarding_options.to_json )
    end
  end

  def default_sso
    begin
      self.ssos << Sso.where(name: Sso::DEFAULT_SSO_NAMES)
    rescue => e
      log.error "Could not add default SSO options. error: #{e}"
    end
  end

  def create_default_config
    OrganizationDefaultConfigs.default_configs.each do |config|
      Config.where(name: config['name'], configable: self).first_or_create do |s|
        s.value       = config['value']
        s.category    = config['category']
        s.description = config['description']
        s.data_type   = config['data_type']
      end
    end
  end

  def create_org_level_team
    return if self == Organization.default_org
    team = teams.where(is_everyone_team: true, name: 'Everyone', description: 'This is a default group. It has all the members in your organization.').first_or_initialize
    if team.save && !admins.blank?
      admins.each {|u| team.add_user(u, as_type: u.organization_role) unless team.admins.include?(u)}
    end
  end

  def create_default_roles
    Role.create_master_roles(self)
  end

  def create_or_update_roles
    # if this flag was set, previous_changes['enable_role_based_authorization'] would be [false, true]
    if self.previous_changes['enable_role_based_authorization'] && self.previous_changes['enable_role_based_authorization'][1]
      Role.create_standard_roles(self)
      SetUserRolesOnRbacEnableJob.perform_now(organization_id: self.id)
    end
  end

  def ensure_valid_custom_domain
    begin
      if URI.parse("https://#{custom_domain}").host != custom_domain
        errors.add(:custom_domain, "Invalid domain")
      end
    rescue Exception => e
      errors.add(:custom_domain, "Invalid domain")
    end
  end

  def create_analytics_reporting_configs
    if self.previous_changes['enable_analytics_reporting'] && self.previous_changes['enable_analytics_reporting'][1]
      config = configs.where(category: 'settings', name: 'AnalyticsReportingConfig').first_or_initialize
      config_value = config.parsed_value || {}

      config.description = "This configuration responsible for sending various analytics reports to org admins via email"
      config.data_type = 'json'
      config.value = {
        summary_feed: false,
        content_activity_feed: true,
        social_activity_feed: true
      }.merge(config_value).to_json

      unless config.save
        log.error "Error while saving config for organization: #{self.host_name}"
      end
    end
  end

  def create_default_skill_for_career_advisor
    CareerAdvisor::SKILLS.each { |skill| self.career_advisors.find_or_create_by(skill: skill) }
  end

end

class UserSubscription < ActiveRecord::Base

  validates :org_subscription_id, :organization_id, :user_id, :order_id,
            :amount, :currency, :start_date, :end_date, :gateway_id, presence: true

  belongs_to :org_subscription

  after_commit :send_email_notification, on: :create

  # TODO need to cache this query.
  def self.paid_annual_org_subscription?(org_id:, user_id:)
    org_subscription_id = OrgSubscription.find_by(organization_id:
                                                    org_id).try(:id)
    return false unless org_subscription_id

    UserSubscription.where("org_subscription_id = ?
    and user_id = ? and organization_id = ? and start_date <= (?)
    and end_date >= (?)",
                           org_subscription_id, user_id, org_id,
                           Date.today, Date.today).present?
  end

  def send_email_notification
    PaymentMailer.user_subscription_notification(id).deliver_later
  end
end

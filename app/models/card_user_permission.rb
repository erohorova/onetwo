class CardUserPermission < ActiveRecord::Base
  belongs_to :card
  belongs_to :user

  validates_presence_of :card, :user
  validates_uniqueness_of :card_id, scope: :user_id

  validate :should_belong_to_same_org

  def should_belong_to_same_org
    unless self.card.organization_id == self.user.organization_id
      errors.add(:base, "Card and User Organizations don't match")
    end
  end
end

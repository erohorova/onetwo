# == Schema Information
#
# Table name: promotions
#
#  id              :integer          not null, primary key
#  url             :string(255)
#  promotable_id   :integer
#  promotable_type :string(191)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  deleted_at      :datetime
#  data            :text(16777215)
#

class Promotion < ActiveRecord::Base
  include IdentityCache
  acts_as_paranoid
  serialize :data, Hash

  belongs_to :promotable, polymorphic: true
  has_many :taggings, as: :taggable, dependent: :destroy
  has_many :tags, through: :taggings

  delegate *%i[name description image_url start_date end_date], to: :promotable

  def update_tags(tag_string)
    return unless tag_string

    taggings.destroy_all
    tags << Tag.extract_tags(tag_string, :pipe_delimited)
  end

  class << self

    # data hash (except :url and :tags) will replace any current data
    def promote(promotable, data)
      return unless Hash === data && data[:url] && promotable.promotion_eligible?

      # assume one promo per promotable, even if deleted
      promo = with_deleted.find_or_initialize_by promotable: promotable
      update_pars = {url: data.delete(:url)}
      promo.update_tags data.delete(:tags)
      update_pars.merge! data: data
      promo.update update_pars
      promo.restore if promo.deleted?
      return promo
    rescue e
      log.error e
      return
    end

    def unpromote(promotable: )
      close(promotable: promotable)
    end

    def close(promotable:)
      where(promotable: promotable).destroy_all
    end

    def promotions_for(user:, limit: 3)
      # If user is in a custom org, prioritize courses that are in the org. TODO: tag filter should apply to these courses too, but not required until there is enough content
      # Then pick any courses that match the tags the user is following
      user_course_ids = user.courses.map(&:id)
      courses = ResourceGroup.visible.where(organization: user.fetch_organization).where.not(id: user_course_ids).where.not(client_resource_id: nil)
      Promotion.where(promotable: courses).sample(limit)
    end
  end



end

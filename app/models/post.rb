# == Schema Information
#
# Table name: posts
#
#  id             :integer          not null, primary key
#  title          :string(255)
#  message        :text(16777215)
#  type           :string(255)
#  user_id        :integer          not null
#  group_id       :integer
#  created_at     :datetime
#  updated_at     :datetime
#  votes_count    :integer          default(0)
#  resource_id    :integer
#  comments_count :integer          default(0)
#  hidden         :boolean          default(FALSE)
#  anonymous      :boolean          default(FALSE)
#  pinned         :boolean          default(FALSE)
#  context_id     :string(255)
#  context_label  :string(255)
#  is_mobile      :boolean          default(FALSE)
#  pinned_at      :datetime
#  postable_id    :integer
#  postable_type  :string(191)
#

class Post < ActiveRecord::Base
  include Votable
  include Approvable
  include Reportable
  include Taggable
  include ResourceWorthy
  include Followable
  include AttachmentWorthy
  include StreamWorthy
  before_save :remove_script_tags

  def remove_script_tags
    self.message.gsub!(/<script.*?>[\s\S]*<\/script>/i, "")
  end

  DEFAULT_POSTABLE_PER_PAGE_LIMIT = {:Channel => 10, :Group => 25}.with_indifferent_access
  DEFAULT_PER_PAGE = 25

  belongs_to :user
  belongs_to :postable, polymorphic: true

  has_many :comments, as: :commentable, :dependent => :destroy
  has_many :notifications, as: :notifiable, :dependent => :destroy

  validates :title, presence: true
  validates :postable, presence: true
  validates :user, presence: true
  validate :user_authorized

  after_create :p_read_by_creator
  after_create :add_tags
  after_commit :remove_post_reads, on: :destroy

  def group
    postable_type == 'Group' ? postable : nil
  end

  def organization
    group.try(:organization)
  end

  def group_id
    postable_type == 'Group' ? postable_id : nil
  end

  def self.url(post, group=nil)
    group ||= Group.where(id: post['postable_id']).first

    # TODO: Handle channel posts here
    return nil if group.nil?

    if group.instance_of?(PrivateGroup)
      website_url = group.resource_group.website_url
    else
      website_url = group.website_url
    end
    redirect_url = UrlBuilder::build(website_url, { 'edcast_ref' => post['id'] })
    UrlBuilder::build((Settings.platform_force_ssl == 'true') ? 'https://' : 'http://' + Settings.platform_host + '/redirect', {'url' => redirect_url})
  end

  def after_followed(record = nil, restore: false)
    IndexJob.perform_later(self.class.name, self.id)
  end

  def after_unfollowed(record = nil)
    IndexJob.perform_later(self.class.name, self.id)
  end

  def _taggable_fields
    [message, title]
  end

  def _resource_extraction_field
    message
  end

  def remove_post_reads
    begin
      PostRead.find_each(query: {filtered: {filter: {term: {post_id: self.id}}}}) do |doc|
        doc.delete
      end
    rescue => e
      log.error("Unable to remove post_reads for post_id=#{self.id} exception=#{e}")
    end
  end

  def snippet
    title.truncate(60, separator: ' ')
  end

  def p_read_by_creator
    # Suppress reads for channel posts. Need to figure out impact on group stats tasks before enabling this.
    unless self.group.nil?
      PostRead.create(id: PostRead.make_primary_key(post:self, user:self.user), user_id: self.user.id, post_id: self.id, group_id: self.group.id)
    end
  end

  # disable special string handling that searchkick does.
  # Note that this disables sorting by string fields
  searchkick callbacks: false, merge_mappings: true, mappings: {
    _default_: {
      dynamic_templates: [],
      properties: {
        postable_type: {
          type: 'string',
          index: 'not_analyzed'
        },
        context_id: {
          type: 'string',
          index: 'not_analyzed'
        },
        context: {
            type: 'string',
            index: 'not_analyzed'
        },
        tag_names: {
            type: 'string',
            index: 'not_analyzed'
        }
      }
    }
  }

  def search_data

    # created virtual index data for questions to prevent nested lookups
    # which is currently not allowed by searchkick.

    # reported are all the flagged posts
    # any post that is hidden or have answers that are hidden are reported
    reported = hidden || comments.map { |c| c.hidden }.reduce(:|) || false

    tag_names = tags.map { |t| t.name } # post tags

    commenttags = comments.joins(:tags).select("tags.name").distinct.map { |t| t.name }

    tag_names += commenttags

    faculty_post = created_by_staff?

    resource_with_type = nil

    if resource
      resource_with_type = resource.as_json.merge(type: resource.type)
    end

    # last_commented is a derived param
    # that keeps track of the timestamp on the
    # last answer to a question.
    # Default to question creation time when no answers.
    # Note: Can't user updated_at because it gets updated
    # even when there's an upvote or approval
    if self.comments.count > 0
      last_commented = self.comments.last.created_at
    else
      last_commented = self.created_at
    end

    additional_data = {
        comments: comments.as_json,
        resource: resource_with_type,
        reported: reported,
        tags: tags.as_json(only: [:id, :name]),
        tag_names: tag_names.uniq,
        faculty_post: faculty_post,
        reporters: reports.map(&:user_id),
        voters: up_votes.map { |x| x.user_id },
        created_by_staff: faculty_post,
        creator_label: creator_label,
        approver: (approval.user_id if approval),
        # list of authors for the question and all its answers
        participants: ([user.name] + comments.map { |a| a.user.name }).uniq,
        last_commented: last_commented,
        student_answer_count: student_answer_count,
        faculty_answer_count: faculty_answer_count,
        file_resources: file_resources.as_json,
        post_id: id,
        follower_ids: followers.pluck(:id),
        followers_count: followers.count
    }

    if context_id.present? && context_label.present?
      additional_data[:context] = ActiveSupport::JSON.encode({id: context_id, label: context_label})
    end

    post = as_json.merge(additional_data)
  end

  scope :search_import, -> { includes(:tags, :user) }

  after_commit :reindex_async #, :create_or_update_card_async

  def reindex_async
    IndexJob.perform_later(self.class.name, self.id)
  end

  def student_answer_count
    comments.count - faculty_answer_count
  end

  def faculty_answer_count
    return 0 if group.nil?
    super_users = group.groups_users.where(as_type: ['instructor', 'admin']).pluck(:user_id)
    comments.where(user_id: super_users).count
  end

  def user_authorized
    errors.add(:user, 'User is not authorized to create this post.') unless
        postable && user && postable.can_write_to_forum?(user)
  end


  def created_by_staff?
    !self.anonymous && self.postable && (self.postable.is_active_super_user? self.user)
  end

  # This function assumes argument to be returned from elasticsearch.
  # created_by_staff is an indexed field
  def self.created_by_staff? post
    post.created_by_staff
  end

  def read_by?(user)
    PostRead.read_by?(user: user, post: self)
  end

  def ref_url
    UrlBuilder.build(group.website_url,  {'edcast_ref' => id})
  end

  def message_text
    Nokogiri.HTML(self.message).text
  end

  def has_approved_comment?
    approved_comment = comments.where("NOT EXISTS (SELECT 1 from approvals where approvable_id = comments.id AND approvable_type = 'Comment')").first
    approved_comment.nil?
  end

  class << self
    def search_with_post_processing(*args)

      # override the default search method provided by searchkick
      # intercept the results here, which only has user_id and add
      # user to each result as a Hashie::Mash
      # this override in some ways breaks the default search method
      # since activerecord wont allow to write hashiemesh into it.
      # to keep search method working separately, always use load: false

      results = search_without_post_processing(*args)

      if !results.nil? && !results.response.blank? && !args[1][:skip_processing]
        user_ids = results.map(&:user_id)
        group_ids = results.map(&:group_id)
        users = User.where(id: user_ids).group_by(&:id)
        scores = Score.where(user_id: user_ids, group_id: group_ids)
        scores_hash = Hash[scores.map{|sc| ["#{sc.user_id}_#{sc.group_id}", sc]}]

        results.each { |result|
          user = users[result.user_id].first
          score_key = "#{result.user_id}_#{result.group_id}"
          score = scores_hash.has_key?(score_key) ? scores_hash[score_key] : nil

          score_entry = if score.nil?
                          {:score => 0, :stars => 0}
                        else
                          {:score => score.score, :stars => score.stars}
                        end

          result.user!.id = user.id
          result.user!.name = user.name
          result.user!.first_name = user.first_name
          result.user!.last_name = user.last_name
          result.user!.bio = user.bio
          result.user!.photo = user.photo
          result.user!.anonimage!.url = user.anonimage.url
          result.user!.score_entry = score_entry
          result.user!.handle = user.handle
          result.user!.full_name = user.full_name
          result.user!.is_suspended = !!user.is_suspended
        }
      end
      results
    end
    alias_method_chain :search, :post_processing
  end

  def self.retrieve(filter_params: {},
                  sort_params: {},
                  search_query: nil,
                  misspelling: 2,
                  limit: 25,
                  offset: 0)

    if search_query.nil?
      search_query = "*"
    end

    if filter_params.empty? && sort_params.empty?
      log.error "No search params passed"
      return nil
    end

    # backward compatibility, posts belonged to groups earlier, so a lot of tests still refer to group_id
    _group_id = filter_params.delete(:group_id)
    unless _group_id.nil?
      filter_params.merge!({postable_id: _group_id, postable_type: 'Group'})
    end

    search_filter = {}
    search_order = ActiveSupport::OrderedHash.new

    # set allowed filters and check for those values in the input
    # this will prevent sending bad input into elastic search as well
    # as having to if-else for presence of multiple filtering-ordering
    # params.

    allowed_filters = [:postable_id, :postable_type, :user_id, 'comments.user_id',
                       :reported, :tag_names, :hidden, :context_id,
                       :created_at, :pinned, :follower_ids]

    # the order of these matters
    allowed_orders = [:votes_count, :comments_count, :pinned, :last_commented, :followers_count]

    allowed_filters.each do |f|
      unless filter_params[f].nil?
        search_filter.merge!({ f => filter_params[f]})
      end
    end

    allowed_orders.each do |ord|
      if sort_params[ord]
        search_order.merge!({ ord => sort_params[ord]})
      end
    end

    # look separately for created at. add a descending created at sorting
    # if not explicitly specified. always treat created at as a secondary sort.
    if sort_params[:created_at]
      search_order.merge!(:created_at => sort_params[:created_at])
    else
      search_order.merge!({ :created_at => :desc })
    end

    type = nil
    if filter_params[:type] == 'Announcement'
      type = [Announcement]
    elsif filter_params[:type] == 'Question'
      type = [Question]
    end

    Post.search search_query, operator: "or", misspellings: { distance: misspelling },
      where: search_filter,
      type: type,
      order: search_order,
      limit: limit,
      offset: offset,
      load: false # do not load activerecord models
  end

  def self.retrieve_by_ids ids
    Post.search "*", where: { id: ids }, load: false
  end

  def self.search_by_id post_id, group_id
    Post.search "*", where: { id: post_id.to_s, group_id: group_id }, load: false
  end

  def reads_count
    PostRead.count(query: {filtered: {filter: {term: {post_id: self.id}}}})
  end

  def is_mobile?
    is_mobile
  end

  def creator_label
    gu = GroupsUser.where(user: user, group: group).first
    if gu.nil?
      return 'member'
    end
    if gu.role_label
      return gu.role_label
    else
      return gu.as_type
    end
  end

  def self.get_postable_default_page_limit(postable_type)
    return DEFAULT_POSTABLE_PER_PAGE_LIMIT[postable_type] if DEFAULT_POSTABLE_PER_PAGE_LIMIT[postable_type].present?
    return DEFAULT_PER_PAGE
  end

  def can_comment? user
    postable.can_write_to_forum?(user)
  end

  def visible_without_auth?
    !!postable.try(:visible_without_auth?)
  end

  def get_app_deep_link_id_and_type
    [id, 'post']
  end
end

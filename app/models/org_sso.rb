# == Schema Information
#
# Table name: org_ssos
#
#  id                                  :integer          not null, primary key
#  organization_id                     :integer
#  sso_id                              :integer
#  position                            :integer          default(1)
#  created_at                          :datetime
#  updated_at                          :datetime
#  is_enabled                          :boolean          default(TRUE)
#  saml_idp_sso_target_url             :string(1024)
#  saml_issuer                         :string(255)
#  saml_idp_cert                       :string(4096)
#  saml_assertion_consumer_service_url :string(1024)
#  saml_allowed_clock_drift            :integer
#  oauth_login_url                     :string(255)
#  oauth_authorize_url                 :string(255)
#  oauth_token_url                     :string(255)
#  oauth_user_info_url                 :string(255)
#  label_name                          :string(255)
#  oauth_client_id                     :string(255)
#  oauth_client_secret                 :string(255)
#  oauth_scopes                        :string(255)
#  parameters                          :text(65535)
#

class OrgSso < ActiveRecord::Base
  include EventTracker
  OKTA_SSO = "lxp_oauth"

  scope :active, -> { where(is_enabled: true) }
  scope :visible, -> { where(is_visible: true) }

  belongs_to :organization
  belongs_to :sso

  validates_presence_of :sso
  validates :sso, uniqueness: { scope: [:parameters, :organization_id], message: "cannot add same sso again for same organization" }

  store :parameters, accessors: [:idp, :provider_name]

  after_save :delete_ssos_cache, :show_onboarding_on_admin_invite_if_applicable
  after_commit :update_visibility
  validate :create_okta_idp, if: :okta_sso?, on: :create
  validate :set_client_id_client_sceret, if: :okta_sso?

  alias_attribute :oauth_redirect_uri, :saml_assertion_consumer_service_url

  configure_metric_recorders(
    default_actor: lambda { |record| record.organization.real_admins&.first },
    trackers: {
      on_create: {
        job_args: [ { sso: :sso, org: :organization } ],
        event: 'org_sso_added'
      },
      on_edit: {
        job_args: [ { sso: :sso, org: :organization } ],
        event: 'org_sso_edited'
      },
      on_delete: {
        job_args: [ { sso: :sso, org: :organization } ],
        event: 'org_sso_removed'
      }
    }
  )


  def self.revised_lxp_oauth
    self.joins(:sso).where(ssos: { name: ['lxp_oauth', 'email', 'org_oauth'] })
  end

  def self.legacy_ssos
    self.joins(:sso).where.not(ssos: { name: ['lxp_oauth', 'internal_content'] })
  end

  def self.by_name(name:)
    self.includes(:sso).where(ssos: { name: name })
  end

  def customised_label_name
    self.label_name || self.sso.name
  end

  def customised_sso_name
    self.provider_name || self.sso.name
  end

  def delete_ssos_cache
    Rails.cache.delete("ssos-by-org:#{organization_id}")
  end

  # user onboarding must for organization with only email as authentication
  def show_onboarding_on_admin_invite_if_applicable
    if organization.onboarding_hidden? && organization.only_email_sso_enabled?
      organization.update_column(:show_onboarding, true)
    end
  end

  def update_visibility
    update_columns(is_visible: previous_changes['is_enabled'][1]) if previous_changes['is_enabled']
  end

  def okta_sso?
    OKTA_SSO == sso.name
  end

  def set_client_id_client_sceret
    if Settings.okta.application_sso.include? provider_name
      application = organization.linked_okta_app
      self.oauth_client_id = application['client_id'] if oauth_client_id.blank?
      self.oauth_client_secret = application['client_secret'] if oauth_client_secret.blank?
      self.saml_assertion_consumer_service_url = "https://#{organization.host}/auth/lxp_oauth/callback" if saml_assertion_consumer_service_url.blank?
    end
  end

  def create_okta_idp
    Settings.okta.sso.each do |okta_sso|
      if okta_sso == provider_name
        okta_provision = Okta::Provision.new(organization: organization)
        response = okta_provision.send("create_#{okta_sso.downcase}_idp")
        if response && response['id'].present?
          begin
            parameter = parameters
            parameter['idp'] = response['id']
            parameters = parameter
            break
          rescue Exception => e
            self.errors.add(:base, "Unable to activate the #{okta_sso} SSO.")
            log.error e.inspect
          end
        else
          log.error response.inspect
          self.errors.add(:base, JSON.parse(response.env.body)['errorCauses'][0]['errorSummary'])
        end
      end
    end
  end

end

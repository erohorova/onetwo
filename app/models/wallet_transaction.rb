class WalletTransaction < ActiveRecord::Base
  include AASM

  belongs_to :order
  belongs_to :user

  serialize :gateway_data

  enum payment_state: {initiated: 0,
                       canceled: 4,
                       processing: 6,
                       failed: 2,
                       pending: 5,
                       successful: 1,
                       voided: 7,
                       unknown: 3}
  # payments life cycle : 
  #  initiated   => processing/canceled
  #  processing  => failed/pending
  #  pending     => successful/failed/void

  aasm :column => :payment_state, :enum => true do
    state :initiated, :initial => true    
    state :canceled   
    state :processing   
    state :failed  
    state :pending   
    state :successful  
    state :voided

    # Indicates that the payment is being processed and authorization will be done
    event :start_processing do
      transitions from: :initiated, to: :processing
    end

    # The transaction has been cancelled
    event :cancel do
      transitions from: :initiated, to: :canceled
    end

    # The payment has been processed but is not yet complete. Authorization has been done but not captured
    event :complete_processing do
      transitions from: :processing, to: :pending
    end

    # The payment i.e capture is successful
    event :success do
      transitions from: :pending, to: :successful
    end

    # There was a failure in the payment
    event :failure do
      transitions from: [:pending, :processing, :initiated, :successful], to: :failed
    end

    # The payment was not captured as the course was not unlocked
    event :void do
      transitions from: :pending, to: :voided
    end
  end

  enum payment_type: {credit: 0, debit: 1}

  validates :order, :amount, :payment_state, :user, presence: true
  validates :order, uniqueness: { scope: :user,
                                  message: "Duplicate WalletTransaction for the same Order." }

  scope :debit_transactions, -> (user_id) {
    where(payment_state: 1,
          user_id: user_id,
          payment_type: 1)}

  scope :credit_transactions, -> (user_id) {
    where(payment_state: 1,
          user_id: user_id,
          payment_type: 0)}

  after_save :update_wallet_balance, if: :successful?
  after_commit :create_transaction_audit
  after_commit :send_email_notification, on: :create, if: :credit?

  # This indicates that authorization was successful
  def success?
    pending?
  end

  def self.initiate(order_id:, user_id:, payment_type:, amount:)
    WalletTransaction.find_or_create_by!(order_id: order_id,
      user_id: user_id,
      payment_type: payment_type,
      amount: amount,
      payment_state: 0)
  end

  def capture
    success!
  end

  # this is to update the wallet balance for a user
  # after a successful credit or debit transaction.
  def update_wallet_balance
    balance = WalletTransaction.credit_transactions(user_id).sum(:amount) - WalletTransaction.debit_transactions(user_id).sum(:amount)
    wallet = (user.wallet_balance || user.build_wallet_balance)
    wallet.update(amount: balance)
    self.update_column(:balance, balance)
  end

  # record transaction_audit for every create/update of
  # transaction (successful/failure/error transaction).
  def create_transaction_audit
    WalletTransactionAudit.create_audit!(wallet_transaction_id: id,
                                         order_data: order,
                                         orderable_data: order.orderable,
                                         user_data: user,
                                         wallet_transaction_data: self)
  end

  def get_transaction_for_recharge
    if order&.orderable_type == 'Recharge'
      order.order_transactions.last  #Every recharge will have only one transaction per order
    end
  end

  def send_email_notification
    PaymentMailer.wallet_recharge_notification(id).deliver_later
  end
end
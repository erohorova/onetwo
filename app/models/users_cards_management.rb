# frozen_string_literal: true

class UsersCardsManagement < ActiveRecord::Base

  VALID_ACTIONS = %w[added_to_journey added_to_pathway posted_to_channel
                     self_assignment shared_to_team team_assignment user_assignment].freeze
  ACTION_ADDED_TO_JOURNEY = 'added_to_journey'
  ACTION_ADDED_TO_PATHWAY = 'added_to_pathway'
  ACTION_SHARE_TO_TEAM = 'shared_to_team'
  ACTION_POST_TO_CHANNEL = 'posted_to_channel'
  ACTION_USER_ASSIGNMENT = 'user_assignment'
  ACTION_SELF_ASSIGNMENT = 'self_assignment'
  ACTION_TEAM_ASSIGNMENT = 'team_assignment'

  belongs_to :user
  belongs_to :card

  acts_as_paranoid

  validates :card, presence: true
  validates :user, presence: true
  validates :card, uniqueness: { scope: %i[user target_id] }, if: :target_id
  validates :action, inclusion: { in: UsersCardsManagement::VALID_ACTIONS }


  scope :self_actions, -> { where(action: UsersCardsManagement::ACTION_SELF_ASSIGNMENT) }
end

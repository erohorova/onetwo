# == Schema Information
#
# Table name: daily_card_stats
#
#  id          :integer          not null, primary key
#  card_id     :integer          not null
#  date        :date
#  impressions :integer          default(0)
#  created_at  :datetime
#  updated_at  :datetime
#

# DEPRECATED
class DailyCardStat < ActiveRecord::Base
  
  belongs_to :card
  validates :date, :uniqueness => {:scope => :card_id}

  def self.get_card_daily_stats(card_id, date, num_days)
    from = (date-num_days.days).strftime('%Y-%m-%d')
    to = date.strftime('%Y-%m-%d')
    active_stats = where('card_id = :card_id and date > :from and date <= :to',
                         card_id: card_id, from: from, to: to).order(date: :asc).
        to_a.each_with_object({}){ |c,h| h[c.date] = c }
    (0..num_days-1).each do |i|
      d = (date-i.days)
      if active_stats[d].nil?
        active_stats[d] = DailyCardStat.new(card_id: card_id, date: (date-i.days))
      end
    end
    stats = []
    active_stats.sort.map do |date, value|
      stats << {date: date.to_s, impressions: value[:impressions]}
    end
    stats
  end

end

# == Schema Information
#
# Table name: notifications_causal_users
#
#  id              :integer          not null, primary key
#  notification_id :integer          not null
#  user_id         :integer          not null
#  anonymous       :boolean          default(FALSE)
#

class NotificationsCausalUser < ActiveRecord::Base

  belongs_to :notification
  belongs_to :user

  validates :user, :presence => true
  validates :notification, presence: true

  def causal_user_name
    if anonymous
      'Anonymous User'
    else
      user.name
    end
  end

  def causal_user_picture
    if anonymous
      user.anonimage.url
    else
      user.photo
    end
  end

end

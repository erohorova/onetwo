# == Schema Information
#
# Table name: configurations
#
#  id         :integer          not null, primary key
#  name       :string(191)      not null
#  value      :string(255)      not null
#  type       :string(255)      not null
#  created_at :datetime
#  updated_at :datetime
#

class Configuration < ActiveRecord::Base
  
  ConfigurationAccessError = Class.new(StandardError)

  validates :name, presence: true, uniqueness: true
  validates :value, presence: true
  validates :type, presence: true # ancestor class name of authorized classes

  
  def self.get_cfg(name, klass)
    cfg = find_by!(name: name)
    raise ConfigurationAccessError unless cfg.type.constantize >= klass
    return Context.property?(name) ? Context.property(name) : cfg.value
  end

  private_class_method :get_cfg # only ActiveRecordConfiguration should call this directly
end

class Recharge < ActiveRecord::Base

  has_many :prices, foreign_key: "recharge_id", class_name: "RechargePrice",
           dependent: :destroy

  validates :skillcoin, :sku, presence: true

  # get all active recharges
  scope :active, -> { where("active = ?", true) }

  def redirect_page_url(org:)
    "#{org&.home_page}/me/skill-coins"
  end

end

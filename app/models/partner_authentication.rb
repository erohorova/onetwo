require 'jwt'
class PartnerAuthentication
  #Find organization for api request based on headers for authentication
  def self.get_org(request: {}, params: {})
    savannah_app_id = request.headers['X-SAVANNAH-APP-ID'] || params["savannah_app_id"]
    savannah_app_id ? AppAuthentication.get_org(request: request, params: params) : ClientAuthentication.get_org(request: request, params: params)
  end

  #API authentication mostly used by forum and events
  def self.api_auth!(params: nil)
    savannah_app_id = params["savannah_app_id"]
    savannah_app_id ? AppAuthentication.api_auth!(params: params) : ClientAuthentication.api_auth!(params: params)
  end

  def self.is_api_client_request_valid?(params)
    savannah_app_id = params["savannah_app_id"]
    savannah_app_id ? AppAuthentication.is_api_client_request_valid?(params) : ClientAuthentication.is_api_client_request_valid?(params)
  end

  def self.get_client_user(client_user_id: nil, org: nil)
    ClientsUser.includes(:user).where(client_user_id: client_user_id, client_id: org.client_id).where('clients_users.user_id IS NOT NULL').references(:user).first
  end

  def self.get_cu_by_user_id(user_id, org)
    ClientsUser.where(user_id: user_id, client_id: org.client_id).first
  end

  #Get org for api from Cache
  def self.fetch_org_from_cache(pars)
    savannah_app_id = pars["savannah_app_id"]
    savannah_app_id ? CacheFactory.org_with_savannah_app_id(savannah_app_id: savannah_app_id) : CacheFactory.org_with_api_key(api_key: pars[:api_key])
  end

  #get current_user from the org or create forum user
  #Currently we are creating org in default org as forum user, once client_cleanup will be done
  #then we will map to current_org user and also create new user in specific org instead of default org
  def self.get_or_create_user(pars, org)
    destiny_user_id = pars[:destiny_user_id]
    if destiny_user_id
      AppAuthentication.get_or_create_user(user_id: pars[:destiny_user_id], email: pars[:user_email], org: org)
    else
      ClientAuthentication.get_or_create_user(user_id: pars[:user_id], email: pars[:user_email], org: org)
    end
  end
end

# == Schema Information
#
# Table name: user_notification_reads
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  seen_at    :datetime
#  created_at :datetime
#  updated_at :datetime
#

class UserNotificationRead < ActiveRecord::Base
  belongs_to :user

  validates :user, presence: true, uniqueness: true
  validates :seen_at, presence: true
  
end

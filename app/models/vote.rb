# == Schema Information
#
# Table name: votes
#
#  id           :integer          not null, primary key
#  user_id      :integer          not null
#  votable_id   :integer          not null
#  votable_type :string(191)      not null
#  weight       :integer
#  created_at   :datetime
#  updated_at   :datetime
#

class Vote < ActiveRecord::Base
  acts_as_paranoid

  include StreamWorthy
  include Streamable
  include EventTracker

  belongs_to :voter, class_name: 'User', foreign_key: :user_id
  belongs_to :votable, polymorphic: true, counter_cache: true

  validates :votable, presence: true
  validates :voter, presence: true

  validate :user_authorized
  validates_uniqueness_of :user_id, :scope => [:votable_type, :votable_id], message: "User has already upvoted this item"


  before_create :set_weight
  after_create :update_score, :touch_votable
  after_commit :update_video_stream_score, on: [:create, :destroy]
  after_commit :reindex_votable
  after_commit :generate_notification, on: :create
  before_destroy :reset_score
  after_destroy :touch_votable

  # an upvote is a 1 and a downvote is a 0.
  scope :up_votes, -> { where(:weight => 1) }

  configure_metric_recorders(
    default_actor: :voter,
    trackers: {
      on_create: {
        if_block: lambda {|record| record.votable.class == Card},
        job_args: [{card: :votable}],
        event: 'card_liked'
      },
      on_delete: {
        if_block: lambda {|record| record.votable.class == Card},
        job_args: [{card: :votable}],
        event: 'card_unliked',
      }
    }
  )

  def set_weight
    if self.weight.nil?
      self.weight = 1
    end
  end

  def generate_notification
    if votable_type == "Card" || votable_type == 'Post' || (votable_type == 'Comment' && (votable.commentable_type == 'Post' || votable.commentable_type == 'Card'))
      votable_creator_id = votable.try(:user_id) || votable.try(:author_id) #because card.user might mein something different from card.author
      NotificationGeneratorJob.perform_later(item_id: id, item_type: 'vote') if  votable_creator_id != user_id #dont generate notification on own comment/post
    end
  end

  acts_as_streamable on: :create

  add_to_stream action: 'liked',
                item: ->(record){record.votable},
                group_id: ->(record){
                  votable = record.votable
                  if votable.is_a?(Post)
                    votable.group_id
                  elsif votable.is_a?(Comment)
                    votable.commentable.group_id
                  end
                },
                if: ->(votable){
                  if votable.is_a?(Post) #Announment and Question
                    true
                  elsif votable.is_a?(Comment)
                    if votable.commentable.is_a?(Post)
                      true
                    else
                      false #exclude vote on card's comments and such
                    end
                  else
                    false
                  end
                }

  remove_from_stream(action: 'liked', item: ->(record){record.votable})

  def touch_votable
    votable.touch(:updated_at)
  end

  def self.get_votes_by_user(user, votable_ids: [], votable_type: nil)
    if votable_ids.blank?
      return []
    end

    Vote.where(:votable_id => votable_ids,
               :votable_type => votable_type,
               :voter => user).pluck(:votable_id)
  end

  def reset_score
    if votable_type == 'Post' || votable_type == 'Comment'
      if !votable.anonymous
        if votable.group && votable.user
          score_entry = Score.where("group_id = ? AND user_id = ?", votable.group, votable.user).first
          score_entry.decrement_score_and_save
        end
      end
    end
  end

  def update_score
    if votable_type == 'Post' || votable_type == 'Comment'
      if !votable.anonymous
        if votable.group && votable.user
          score_entry = Score.where("group_id = ? AND user_id = ?", votable.group, votable.user).first
          if score_entry.blank?
            score_entry = Score.new({group: votable.group, user: votable.user,
                                     score: 1, stars: 0})
            score_entry.save
          else
            score_entry.increment_score_and_save
          end
        end
      end
    end
  end

  def user_authorized
    if votable && votable.try(:group) && votable.try(:user)
      errors.add(:votable, 'You are not authorized to cast this vote.') unless votable.group.is_active_user?(voter)
    end
  end

  def reindex_votable
    if self.votable.kind_of?(Post) || self.votable.kind_of?(Card)
      IndexJob.perform_later(self.votable.class.name, self.votable.id)
    end
  end

  def update_video_stream_score
    if votable.kind_of?(Card) && votable.card_type == "video_stream"
      VideoStreamUpdateScoreJob.perform_later(votable.video_stream) #Updates Score for Trending / Popular
    end
  end

  def activity_initiator
    self.voter
  end

  def activity_stream_action
    ActivityStream::ACTION_UPVOTE
  end

  def is_stream_worthy?
    (votable.kind_of?(Card) || votable.kind_of?(VideoStream)) && votable.try(:publicly_visible?)
  end

  def organization
    votable.try(:organization)
  end

  private 

    def record_vote_event(event)
      return unless self.votable_type == 'Card'
      record_custom_event(
        event: event,
        actor: :voter,
        job_args: [
          { card: :votable}
        ]
      )
    end
end

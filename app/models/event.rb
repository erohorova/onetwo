# == Schema Information
#
# Table name: events
#
#  id                         :integer          not null, primary key
#  name                       :string(255)      not null
#  address                    :string(255)
#  description                :text(16777215)
#  latitude                   :float(24)
#  longitude                  :float(24)
#  state                      :string(255)      not null
#  privacy                    :string(255)      not null
#  comments_count             :integer          default(0)
#  group_id                   :integer
#  user_id                    :integer
#  created_at                 :datetime
#  updated_at                 :datetime
#  event_start                :datetime
#  event_end                  :datetime
#  ical_file_name             :string(255)
#  ical_content_type          :string(255)
#  ical_file_size             :integer
#  ical_updated_at            :datetime
#  conference_type            :string(255)
#  hangout_discussion_enabled :boolean
#  conference_url             :text(65535)
#  youtube_url                :string(255)
#  context_enabled            :boolean
#  webex_conference_key       :string(255)
#  host_url                   :string(255)
#  conference_email           :string(255)
#

require 'tempfile'
require 'icalendar/tzinfo'


class Event < ActiveRecord::Base
  include Followable
  include WebexConference

  attr_accessor :webex_id, :webex_password

  has_many :events_users, dependent: :destroy
  has_many :attending_events_users, -> { attending }, class_name: 'EventsUser', foreign_key: 'event_id'
  has_many :attending_users, :class_name => 'User', through: :attending_events_users, source: :user
  has_many :comments, as: :commentable, :dependent => :destroy
  belongs_to :group
  belongs_to :user

  validates_presence_of :name
  validates_presence_of :event_start
  validates_presence_of :event_end
  validates_presence_of :state
  validates :conference_email, presence: true, email_format: { message: 'is not a valid email address' }, on: :create, :if => :webex_conference?
  validates_presence_of :webex_id, :webex_password, :conference_email, on: :create, :if => :webex_conference?
  validates_presence_of :privacy
  validates_inclusion_of :state , in: ['open', 'cancelled']
  validates_inclusion_of :privacy, in: ['public', 'private']
  validate :user_authorized

  has_attached_file :ical
  validates_attachment_content_type :ical, :content_type => "text/calendar"
  validates_attachment_size :ical, :less_than => 4.megabytes, :message => 'ical too big.'

  before_create :create_webex_meeting ,:if=>:webex_conference?
  after_create :add_creator_rsvp, :generate_ical
  after_create :set_join_url, :set_host_url,:if=>:webex_conference?
  after_commit :schedule_reminder_email, on: :create
  after_commit :reschedule_reminder_email, on: :update
  before_destroy :delete_webex_meeting, :if=>:webex_conference?

  scope :upcoming, -> { where(["event_end >= ?", DateTime.now.utc])}
  scope :past, ->  { where(["event_end <= ?", DateTime.now.utc]) }

  def self.url(event)
    group = Group.find_by(id: event['group_id'])

    return nil if group.nil?

    if group.instance_of?(PrivateGroup)
      website_url = group.resource_group.website_url
    else
      website_url = group.website_url
    end
    redirect_url = UrlBuilder::build(website_url, { 'edcast_ref' => event['id'] })
    UrlBuilder::build((Settings.platform_force_ssl == 'true') ? 'https://' : 'http://' + Settings.platform_host + '/redirect', {'url' => redirect_url})
  end

  def user_authorized
    errors.add(:user, 'User is not authorized to create this event.') unless
        group && user && group.is_active_user?(user)
  end

  def rsvp_yes_count
    events_users.where(:rsvp => 'yes').count
  end

  def rsvp_top_users
    events_users.limit(7).map { |x| x.user.photo }
  end

  def user_rsvp user
    user_rsvp = events_users.where(:user_id => user.id).first
    if user_rsvp.nil?
      EventsUser.new
    else
      user_rsvp
    end
  end

  # see if user is going to an event
  def going? user
    events_users.where(:rsvp => 'yes', user: user).present?
  end

  def googlecal_url
     "https://www.google.com/calendar/render?action=TEMPLATE&text=#{name}&dates=#{event_start.strftime('%Y%m%dT%H%M00Z')}/#{event_end.strftime('%Y%m%dT%H%M00Z')}&details=#{description || '-' }&location=#{address}&sf=true&output=xml".gsub(/ /, "+")
  end

  def encrypt
    ActiveSupport::MessageEncryptor.
        new(Edcast::Application.config.secret_key_base).
        encrypt_and_sign(self.id)
  end

  def schedule_reminder_email
    if event_start
      #first time
      EventReminderJob.set(wait_until: event_start - 1.hour).perform_later(event_id: self.id, event_start: self.event_start.to_i)
    end
  end

  def reschedule_reminder_email
    if previous_changes['event_start']  #http://api.rubyonrails.org/classes/ActiveModel/Dirty.html#method-i-previous_changes
      #reschedule
      EventReminderJob.set(wait_until: event_start - 1.hour).perform_later(event_id: self.id, event_start: self.event_start.to_i)
    end
  end

  def can_comment? user
    group.is_active_user?(user)
  end

  def add_creator_rsvp
    self.events_users.create(:rsvp => 'yes', user: self.user)
  end

  def generate_ical
    file = Tempfile.new('ical.ics')

    begin
      cal = Icalendar::Calendar.new

      cal.event do |e|
        e.dtstart     = Icalendar::Values::DateTime.new(self.event_start, 'tzid' => 'UTC')
        e.dtend       = Icalendar::Values::DateTime.new(self.event_end, 'tzid' => 'UTC')
        e.summary     = self.name
        e.description = self.description
      end

      file.write(cal.to_ical)
      file.rewind

      self.ical = file
      self.ical_content_type = 'text/calendar'
      self.ical_file_name = 'ical.ics'
      self.save!
    ensure
      file.close
      file.unlink
    end
  end

  def org
    group.try(:organization)
  end
  alias_method :organization, :org
end

class DynamicGroupRevision < ActiveRecord::Base

  belongs_to :organization
  has_one :dynamic_group, foreign_key: :id, primary_key: :group_id, class_name: 'Team'

  after_commit :update_dynamic_group, on: :destroy

  private

  def update_dynamic_group
    return unless dynamic_group

    if self.external_id
      dynamic_group.destroy
    else
      dynamic_group.update_attributes(
        is_dynamic: false,
        is_mandatory: false
      )
      #Team #check_mandatory
    end
  end
end

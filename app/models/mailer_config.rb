# == Schema Information
#
# Table name: mailer_configs
#
#  id                   :integer          not null, primary key
#  client_id            :integer
#  address              :string(255)
#  port                 :integer
#  user_name            :string(255)
#  password             :string(255)
#  domain               :string(255)
#  webhook_keys         :string(255)
#  enable_starttls_auto :boolean          default(TRUE)
#  authentication       :string(255)
#  from_address         :string(255)
#  from_name            :string(255)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  organization_id      :integer          not null
#  from_admin           :boolean          default(FALSE)
#  is_active            :boolean          default(FALSE)
#  verification         :text(65535)
#

class MailerConfig < ActiveRecord::Base

  include EventTracker

  configure_metric_recorders(
    default_actor: -> (record) { },
    trackers: {
      on_create: {
        event: 'org_email_sender_created'
      },
      on_edit: {
        event: 'org_email_sender_edited',
        job_args: -> (record) {
          if record.changes.key? 'verification'
            [
              {
                old_val: record.changes["verification"][0],
                new_val: record.changes["verification"][1],
                mailer_config: record
              }
            ]
          else
            [{mailer_config: record}]
          end
        }
      },
      on_delete: {
        event: 'org_email_sender_deleted',
      }
    }
  )
  
  belongs_to :client
  belongs_to :organization

  validates :address, :domain, :port, :user_name, :password, :from_address, :from_name, :webhook_keys, :authentication, :organization_id, presence: true
  validates :port, length: { in: 2..3 }
  validates :address, :domain, format: { with: /\A[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}\z/,
    message: "invalid" }
  validates :from_address, email_format: { message: '' }
  validates_uniqueness_of :from_address, { scope: :organization_id, message: 'Email Id already taken' }

  scope :from_lxp_admin, -> { where(from_admin: true) }
  after_commit :invalidate_cache_config, if:  Proc.new { |config|  config.invalidate_cache || config.is_active }
  after_commit :verify_domain, on: :create
  before_destroy :check_is_acitve

  serialize :verification, Hash

  attr_accessor :invalidate_cache

  def get_config
    self.slice(
      :address,
      :authentication,
      :domain,
      :enable_starttls_auto,
      :password,
      :port,
      :user_name,
      :webhook_keys ).symbolize_keys
  end

  def self.disable_active_sender_email(organization_id)
    MailerConfig.where(organization_id: organization_id, is_active: true, from_admin: true).each do |config|
      config.update(is_active: false)
    end
  end

  def attach_default_values
    config_values = {
      address: Settings.mandrill.address,
      port: Settings.mandrill.port,
      user_name: Settings.mandrill.user_email,
      password: Settings.mandrill.password,
      domain: self.from_address.split('@').last,
      webhook_keys: Settings.mandrill.webhook_keys.first,
      enable_starttls_auto: Settings.mandrill.enable_starttls_auto,
      authentication: 'plain',
      from_admin: true,
      verification: {
        domain: domain_verified?(self.from_address.split('@').last)
      }
    }
    self.assign_attributes(config_values)
  end

  def verify_domain
    VerifySenderDomainJob.perform_later({config_id: id}) if (from_admin && !domain_verified?(domain))
  end

  private

  def check_is_acitve
    if is_active
      errors.add(:base, 'Current Sender cannot be deleted')
      false
    end
  end

  def domain_verified?(domain)
    verified =  MailerConfig.where(domain: domain).pluck(:verification).first
    return false unless verified

    verified[:domain]&.to_bool.present?
  end

  def invalidate_cache_config
    Rails.cache.delete("active_mailer_config_for_#{self.organization_id}")
  end

end

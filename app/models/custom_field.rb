# == Schema Information
#
# Table name: custom_fields
#
#  id              :integer          not null, primary key
#  organization_id :integer          not null
#  abbreviation    :string(255)
#  display_name    :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class CustomField < ActiveRecord::Base
  include EventTracker
  include WorkflowPatcher
  include WorkflowObserver

  belongs_to :organization
  has_many :user_fields, class_name: 'UserCustomField', dependent: :destroy
  has_many :users, through: :user_fields
  has_many :configs, as: :configable

  default_scope { order('custom_fields.created_at desc') }

  before_validation :assign_abbreviation
  validates :organization, presence: true
  validates :abbreviation, :display_name, presence: true, length: { maximum: 64 }
  validates :abbreviation, uniqueness: { scope: [:organization_id] }

  before_create :default_enable_people_search
  after_commit :reindex_users
  after_commit :detect_upsert, on: [:create, :update]
  after_commit :detect_delete, on: :destroy

  configure_metric_recorders(
    # default_actor: lambda {|record| record.organization&.admins&.first},
    default_actor: lambda {|record| },
    trackers: {
      on_create: {
        event: 'org_custom_field_added',
      },
      on_edit: {
        event: 'org_custom_field_edited',
      },
      on_delete: {
        event: 'org_custom_field_removed',
      }
    }
  )

  def self.wrap(user:, attributes:)
    attributes.each do |key, value|
      next if value.blank?

      field = find_by(abbreviation: key)
      next if field.nil?

      # Sending +upload_mode+ as 'bulk_upload' because this method is used only in user bulk upload process.
      user_field = UserCustomField.find_by(custom_field: field, user: user)
      user_field = UserCustomField.new(custom_field: field, user: user) if user_field.nil?
      user_field.update(value: value, upload_mode: 'bulk_upload')
    end
  end

  def reindex_users
    users.find_each(&:reindex_async_urgent)
  end

  def set_value_for_config(config_name, value)
    config = configs.find_or_initialize_by(
      category: 'custom_fields',
      data_type: 'string',
      name: config_name
    )
    if value.respond_to?('to_bool')
     config.attributes = { value: value.to_s }
     config.save
    end
  end

  def get_config_enable(config_name)
    !!configs.find_by(name: config_name)&.value&.to_bool
  end

  private

  def assign_abbreviation
    return if display_name.blank?
    self.abbreviation = display_name.underscorize
  end

  # returning false from any before_ callback in ActiveRecord will result
  # in halting of callback chain.
  def default_enable_people_search
    self.enable_people_search ||= false
    true # prevent halting of callback chain
  end
end

class BraintreePaymentGateway < PaymentGateway

  def get_merchant_account_id(currency: nil)
    Settings.braintree['accounts'][currency]
  end

  def merge_gateway_params(params:)
    params[:resp_params].merge!({ client_token: generate_client_token })
  end

  def generate_client_token
    Braintree::ClientToken.generate
  end

  def prepare_payment_params(transaction:, nonce:)
    {
      amount: transaction.amount.to_s,
      merchant_account_id:  get_merchant_account_id(currency: transaction.currency),
      payment_method_nonce: nonce,
      options: {
        submit_for_settlement: false
      }
    }
  end

  def authorize_payment(transaction, args)
    gateway = Braintree::Transaction.sale(prepare_payment_params(
                                                     transaction: transaction,
                                                     nonce: args[:nonce]))
  end

  def void_payment(gateway:)
    braintree_result = Braintree::Transaction.void(gateway.transaction.id)
    braintree_result.success?
  end

  def capture_payment(transaction:, gateway:)
    braintree_result = Braintree::Transaction.submit_for_settlement(gateway.transaction.id)
    braintree_result.success?
  end

  def typecast_object(gateway_object)
    {
      gateway_id: gateway_object&.transaction.try(:id),
      gateway_data: gateway_object
    }
  end

  def get_error(gateway_object)
    return gateway_object&.errors.first.message if gateway_object&.errors.any?

    braintree_transaction = gateway_object.transaction
    case braintree_transaction&.status
    when 'processor_declined'
      braintree_transaction&.processor_response_text + " Code: " + braintree_transaction&.processor_response_code
    when 'settlement_declined'
      braintree_transaction&.settlement_response_text + " Code: " + braintree_transaction&.settlement_response_code
    when 'gateway_rejected'
      braintree_transaction&.gateway_rejection_reason
    end
  end

end

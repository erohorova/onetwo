# == Schema Information
#
# Table name: content_level_metrics
#
#  id              :integer          not null, primary key
#  views_count     :integer          default(0)
#  comments_count  :integer          default(0)
#  likes_count     :integer          default(0)
#  content_type    :string(191)
#  content_id      :integer          not null
#  organization_id :integer          not null
#  offset          :integer
#  period          :string(191)
#  completes_count :integer          default(0)
#  bookmarks_count :integer          default(0)
#

class ContentLevelMetric < ActiveRecord::Base
  include MetricsAggregator

  belongs_to :organization

  before_validation on: :create do
    if self.organization_id.nil?
      self.organization_id = content_object.try :organization_id
    end
  end

  create_increment_methods([:comments_count, :likes_count, :views_count, :completes_count, :bookmarks_count])
  create_getter_methods([:comments_count, :likes_count, :views_count, :completes_count, :bookmarks_count])

  def content_object
    case content_type
    when 'card', 'collection'
      Card.find_by(id: content_id)
    when 'video_stream'
      VideoStream.find_by(id: content_id)
    else
      nil
    end
  end

  def is_content_object_deleted?
    content_object.nil?
  end

  def content_title
    content_object.try(:snippet)
  end

  def content_ecl_metadata
    content = content_object
    if content_object.is_a?(Card) && content.ecl_metadata.present?
      {
        source_display_name: content.ecl_metadata["source_display_name"],
        source_type_name: content.ecl_metadata["source_type_name"]
      }
    end
  end

end

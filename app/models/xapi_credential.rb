# == Schema Information
#
# Table name: xapi_credentials
#
#  id               :integer          not null, primary key
#  end_point        :string(255)
#  lrs_login_key    :string(255)
#  lrs_password_key :string(255)
#  lrs_api_version  :string(255)
#  organization_id  :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class XapiCredential < ActiveRecord::Base

  include EventTracker

  belongs_to :organization

  validates_presence_of *%i{
    end_point        lrs_login_key
    lrs_password_key organization_id
  }

  validates_uniqueness_of :organization_id

  after_commit ->(obj) { obj.push_details_to_lms }, on: [:create, :update]

  configure_metric_recorders(
    # Default actor is nil.
    # Recording will not happen unless one was found in RequestStore.
    default_actor: -> (record) {},
    trackers: {
      on_create: {
        event: 'xapi_credential_created',
      },
      on_edit: {
        event: 'xapi_credential_edited',
      },
      on_delete: {
        event: 'xapi_credential_deleted',
      }
    }
  )

  protected

  def push_details_to_lms
    LmsXapiSyncJob.perform_later(self.id)
  end
end

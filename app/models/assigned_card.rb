# == Schema Information
#
# Table name: teams_cards
#
#  id         :integer          not null, primary key
#  team_id    :integer
#  card_id    :integer
#  type       :string(10)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#

class AssignedCard < TeamsCard
end

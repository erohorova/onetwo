# == Schema Information
#
# Table name: user_content_level_metrics
#
#  id               :integer          not null, primary key
#  user_id          :integer
#  content_id       :integer
#  content_type     :string(191)
#  smartbites_score :integer          default(0)
#  period           :string(191)
#  offset           :integer
#  created_at       :datetime
#  updated_at       :datetime
#

class UserContentLevelMetric < ActiveRecord::Base
  include MetricsAggregator

  create_increment_methods([:smartbites_score])
  create_getter_methods([:smartbites_score])

  belongs_to :user

  def content_object
    case content_type
    when 'card', 'collection'
      Card.find_by(id: content_id)
    when 'video_stream'
      VideoStream.find_by(id: content_id)
    else
      nil
    end
  end

  def is_content_object_deleted?
    content_object.nil?
  end

  def content_title
    content_object.try(:snippet)
  end
end

require 'jwt'
class ClientAuthentication
  def self.get_org(request: {}, params: {})
    log.warn("ClientAuthentication with API key has been deprecated: get_org")
    api_key = request.headers['X-API-KEY'] || params["api_key"]
    token = request.headers['X-API-TOKEN'] || params["token"]

    credential = Credential.where(api_key: api_key).first
    current_org, jwt_payload, jwt_header = nil
    if credential.present?
      shared_secret = credential.shared_secret
      begin
        jwt_payload, jwt_header = JWT.decode(token,shared_secret)
        current_org = CacheFactory.org_with_api_key(api_key: api_key)
        jwt_payload = jwt_payload.with_indifferent_access
        jwt_header  = jwt_header.with_indifferent_access
      rescue => e
        log.error "Authentication failed due to this exception: #{e}"
      end
    end
    [current_org, jwt_payload, jwt_header]
  end

  def self.api_auth!(params: nil)
    log.warn("ClientAuthentication with API key has been deprecated: api_auth")
    unless params
      log.info("Invalid params") and return
    end
    credential, client, current_org = CacheFactory.fetch_credential_and_client(api_key: params[:api_key])

    unless current_org
      log.info("No client/org found for api key: #{params[:api_key]}")
      return
    end

    user_id = params[:user_id]
    user_email = params[:user_email]
    params[:credential] = credential
    if is_api_client_request_valid?(params)
      #setting the @current_user instance variable enables the current_user, user_signed_in? helper methods to work in the controller
      if user_id
        current_user = get_or_create_user(user_id: user_id, email: user_email, org: current_org)
      else
        log.warn('Unable to look up user') and return
      end
    else
      log.warn('invalid API request') and return
    end

    auth_params = params.slice(:api_key, :token, :resource_id, :user_id, :user_email, :user_role, :timestamp)
    widget_request = true
    return [current_org, current_user, auth_params, widget_request]
  end

  def self.is_api_client_request_valid?(pars)
    log.warn("ClientAuthentication with API key has been deprecated: is_api_client_request_valid")
    pars = pars.with_indifferent_access
    parts = [pars[:credential].shared_secret,
             pars[:timestamp],
             pars[:user_id],
             pars[:user_email],
             pars[:user_role],
             pars[:resource_id]].compact
    parts << pars[:destiny_user_id] if pars[:destiny_user_id]
    token = pars[:token]

    calculated_token = Digest::SHA256.hexdigest(parts.join('|'))

    if token == calculated_token
      return true
    else
      log.info("calculated token: #{calculated_token}, request token: #{token}")
      return false
    end
  end

  def self.get_or_create_user(user_id: nil, email: nil, org: nil)
    client_user = PartnerAuthentication.get_client_user(client_user_id: user_id, org: org)
    if client_user
      user = client_user.user
      #update the email address if the client changed it
      if(email && user.email != email)
        email_record = Email.find_by(email: email, user_id: user.id)
        unless email_record
          user.emails.create(email: email, confirmed: true)
        end
      end
      user
    else
      if email
        # Look up by default org for forum user
        # Stopgap until we figure out the org-inst association. We can then create
        # forum users in the correct organization.
        user = User.where(email: email, organization: org).first || User.new(email: email, password: User.random_password, is_complete: false, password_reset_required: true, organization: org, organization_role: 'member')
      else
        user = User.new(email: nil, password: User.random_password, is_complete: false, password_reset_required: true, organization: org, organization_role: 'member')
      end
      user.clients_users.build(client: org.client, client_user_id: user_id, user: user)
      user.skip_confirmation!
      user.save!
      user
    end
  end
end

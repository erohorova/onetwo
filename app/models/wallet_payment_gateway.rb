class WalletPaymentGateway < PaymentGateway

  def merge_gateway_params(params:)
    order = params.dig(:resp_params,:order)
    initiate_wallet_transaction(order: order, amount: params[:price].amount)
    params[:resp_params]
  end

  def initiate_wallet_transaction(order:, amount:)
    WalletTransaction.initiate(order_id: order.id,
      user_id: order.user_id,
      payment_type: 1,
      amount: amount)
  end

  def self.validate_amount(amount:, user_id:)
    return false if (amount < BigDecimal.new("0"))
    WalletBalance.find_by(user_id: user_id).amount >= amount
  end

  def authorize_payment(transaction, args)
     wallet_transaction = WalletTransaction.find_by(order_id: transaction.order_id,
       user_id: transaction.user_id,
       payment_type: 1,
       amount: transaction.amount,
       payment_state: 0)

    raise WalletTransactionNotFoundError if wallet_transaction.blank?

    wallet_transaction.start_processing!

    if self.class.validate_amount(amount: transaction.amount, user_id:transaction.user_id)
      wallet_transaction.complete_processing!
    else
      log.warn "InsufficientWalletBalanceError User #{transaction.user_id} Amount #{transaction.amount}"
      wallet_transaction.failure!
      wallet_transaction.errors.add(:base, :insufficient_balance, message: 'Insufficient balance in wallet')
    end
    wallet_transaction
  end

  def void_payment(gateway:)
    gateway.void!
  end

  def capture_payment(transaction:, gateway:)
    gateway.capture
  end

  def typecast_object(gateway_object)
    {
      gateway_id: gateway_object&.id,
      gateway_data: gateway_object&.attributes
    }
  end

  def get_error(gateway_object)
    return gateway_object&.errors.full_messages.to_sentence if gateway_object&.errors.any?
  end
end
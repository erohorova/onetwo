# == Schema Information
#
# Table name: mentions
#
#  id               :integer          not null, primary key
#  user_id          :integer
#  mentionable_id   :integer
#  mentionable_type :string(191)
#  created_at       :datetime
#  updated_at       :datetime
#

class Mention < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :user
  belongs_to :mentionable, polymorphic: true
  after_commit :generate_notification, on: :create, if: :notify?

  def self.extract_mentioned_users text, org_id
    html = Nokogiri::HTML(text)
    if html.errors.empty?
      plain_text = html.inner_text
    else
      plain_text = text
    end
    mentions = plain_text.scan(/\B(@[\.\-\w]*\w)/).map{|match| match[0]}
    mentions_users = User.where(handle: mentions, organization_id: org_id)
  end

  def generate_notification
    NotificationGeneratorJob.perform_later(item_id: id, item_type: 'mention')
  end

  def notify?
    card_not_hidden_and_published? || in_published_comment?
  end

  def card_not_hidden_and_published?
    mentionable.class == Card && mentionable.published? && !mentionable.hidden
  end

  def in_published_comment?
    (mentionable.class == Comment) && !mentionable.hidden && ['Card', 'VideoStream', 'Team', 'Organization'].include?(mentionable.commentable_type)
  end
end

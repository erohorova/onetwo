# == Schema Information
#
# Table name: quiz_question_options
#
#  id                 :integer          not null, primary key
#  quiz_question_id   :integer
#  quiz_question_type :string(191)
#  label              :text(16777215)
#  is_correct         :boolean
#  created_at         :datetime
#  updated_at         :datetime
#

class QuizQuestionOption < ActiveRecord::Base
  include SyncCloneable

  belongs_to :quiz_question, polymorphic: true
  has_one :file_resource, as: :attachable, dependent: :destroy

  validates :quiz_question, presence: true

  validates :label, presence: true, uniqueness: {scope: [:quiz_question_id, :quiz_question_type]}

  scope :correct_option, -> {where is_correct: true}
  #parent object for this model is card and file resource is two level deep to accept nested attributes.
  # on top of that, file resource is polymoprhic.
  # this is a workaround to set file resource object before you create QQO
  attr_accessor :file_resource_id

  before_create :set_file_resource
  before_update :set_file_resource
  after_destroy :sync_clone_card_association

  def as_json
    {
      'id' => id,
      'label' => label,
      'is_correct' => is_correct,
      'image_url' => file_resource && file_resource.file_url
    }
  end

  def set_file_resource
    self.file_resource = FileResource.find(self.file_resource_id) unless self.file_resource_id.nil?
  end
end

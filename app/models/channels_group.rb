# == Schema Information
#
# Table name: channels_groups
#
#  id         :integer          not null, primary key
#  channel_id :integer
#  group_id   :integer
#

class ChannelsGroup < ActiveRecord::Base

  # =========================================
  # EVENT RECORDING CONFIG
  
  include EventTracker

  if_block = -> (record) { record.group.course_code }
  job_args  = [{
    channel: :channel,
    course: :group
  }]

  configure_metric_recorders(
    default_actor: -> (record) { },
    trackers: {
      on_create: {
        event: 'channel_course_added',
        job_args: job_args,
        if_block: if_block
      },
      on_delete: {
        event: 'channel_course_removed',
        job_args: job_args,
        if_block: if_block
      }
    }
  )
  # =========================================

  belongs_to :group
  belongs_to :channel
  
  validates :channel, presence: true
  validates :group, presence: true
  validate :should_belong_to_same_org

  def should_belong_to_same_org
    unless self.group.try(:organization).try(:id) == self.channel.organization_id
      errors.add(:base, "Course and Channel Organizations don't match")
    end
  end
end

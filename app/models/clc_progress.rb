# == Schema Information
#
# Table name: clc_progresses
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  clc_id     :integer
#  clc_score  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

# We will save achieved score here for particular CLC by user
# clc_score is in minutes
class ClcProgress < ActiveRecord::Base
  belongs_to :user
  belongs_to :clc

  validates :clc_score, presence: true, numericality: { greater_than: 0 }
  validates :user_id, presence: true, uniqueness: {scope: :clc_id}

  def self.get_progress_for(clc_id, user_id)
    find_by(clc_id: clc_id, user_id: user_id)
  end
end

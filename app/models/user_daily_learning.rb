# == Schema Information
#
# Table name: user_daily_learnings
#
#  id               :integer          not null, primary key
#  user_id          :integer          not null
#  card_ids         :text(65535)
#  date             :string(11)
#  processing_state :string(255)      not null
#  created_at       :datetime
#  updated_at       :datetime
#

class UserDailyLearning < ActiveRecord::Base

  belongs_to :user

  serialize :card_ids, Array

  validates :user_id, presence: true, uniqueness: {scope: [:date]}
  validates :processing_state, presence: true, inclusion: {in: [DailyLearningProcessingStates::INITIALIZED, DailyLearningProcessingStates::STARTED, DailyLearningProcessingStates::ERROR, DailyLearningProcessingStates::COMPLETED]}

  before_validation :set_processing_state, on: :create

  def set_processing_state
    if self.processing_state.nil?
      self.processing_state = DailyLearningProcessingStates::INITIALIZED
    end
  end

  def available?
    processing_state == DailyLearningProcessingStates::COMPLETED
  end

  def self.daily_learning(user_id: , date: )
    where(user_id: user_id, date: date).first
  end

  def self.initialize_and_start(user_id: , date: )
    user_daily_learning = new(user_id: user_id, date: date)
    user_daily_learning.save!
    DailyLearningForUserJob.perform_later(user_daily_learning.id)
  end

  def process!
    begin
      start_processing!
      topic_ids = user.
        learning_topics.
        map{ |learning_topic|
          learning_topic["topic_id"] || learning_topic[:topic_id]
        }


      # Go through topics, set off jobs if data not available
      user.learning_topics.each do |topic|
        topic_id = topic['topic_id'] || topic[:topic_id]
        topic_name = topic['topic_name'] || topic[:topic_name]
        TopicDailyLearning.process_for_topic!(topic_id: topic_id, topic_name: topic_name, date: date,
                                              organization_id: user.organization_id, requested_by_user: true)
      end

      # card_ids = pick_daily_learning_from_topics(topic_ids)
      card_ids = UserDailyLearningService.new(user).pick_daily_learning_from_topics(topic_ids)
      update_attributes!(card_ids: card_ids)
      complete_processing!
    rescue => e
      log.error e
      mark_as_errored!
      raise e
    end
  end

  private
  def start_processing!
    update!(processing_state: DailyLearningProcessingStates::STARTED)
  end

  def complete_processing!
    update!(processing_state: DailyLearningProcessingStates::COMPLETED)
  end

  def mark_as_errored!
    update!(processing_state: DailyLearningProcessingStates::ERROR)
  end
end

class SourcesCredential < ActiveRecord::Base
  include EventTracker
  
  belongs_to :organization
  serialize :auth_api_info, JSON

  before_validation on: :create do
    self.shared_secret ||= SecureRandom.hex(16)
  end
  validates :organization_id, presence: true
  validates :source_id, presence: true, uniqueness: {scope: :organization_id}

  configure_metric_recorders(
     default_actor: lambda { |record| },
     trackers: {
       on_create: {
         event: 'org_sources_credential_created'
       },
       on_delete: {
         event: 'org_sources_credential_deleted'
       },
       on_edit: {
         event: 'org_sources_credential_edited'
       }
     }
   )
end


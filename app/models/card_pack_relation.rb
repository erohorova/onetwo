# == Schema Information
#
# Table name: card_pack_relations
#
#  id         :integer          not null, primary key
#  cover_id   :integer
#  from_id    :integer
#  to_id      :integer
#  created_at :datetime
#  updated_at :datetime
#  title      :text(65535)
#  from_type  :string(191)
#  deleted    :boolean          default(FALSE)
#  locked     :boolean
#  Use of `deleted` column is deprecated,
#  we now hard delete card_pack_relation when card is deleted, don't use `deleted` column

class CardPackRelation < ActiveRecord::Base
  include EventTracker
  include Manageable
  include CardMetadata
  include SyncCloneable

  # reference to the card representing the pack cover; same for all cards in a pack
  belongs_to :cover, -> { with_deleted }, class_name: 'Card'
  belongs_to :card, -> { with_deleted }, class_name: 'Card', foreign_key: :from_id

  validates :cover, presence: true
  # from_id and from_type represent pack content object (like AR STI, but sometimes with ancestor class names)
  # to_id is id of next relation object
  # not using activerecord association here because object may have been deleted
  validates :from_id, presence: true, uniqueness: { scope: [:cover_id, :from_type] }
  validates :from_type, presence: true
  validates :to_id, uniqueness: true, allow_nil: true
  validate :to_id_exists?

  after_commit :reindex_cover
  after_commit :record_add_to_pathway_event, on: :create
  after_destroy :record_remove_from_pathway_event
  after_destroy :destroy_management
  after_destroy :sync_clone_card_association
  after_commit  :reset_completed_percentage, on: [:create, :destroy]

  acts_as_manageable on: :create

  scope :not_deleted, -> { where(deleted: false) }

  # set from_id and from_type
  def from=(obj)
    return unless type = CardPackRelation.pack_item_add_type(obj)
    self.from_id = obj.id
    self.from_type = type
    self.title = CardPackRelation.item_title(obj)
  end

  # set to_id, indicating next relation object, which is created if necessary
  def to=(obj)
    return unless to_type = CardPackRelation.pack_item_add_type(obj)
    created = false
    to_reln = CardPackRelation.find_or_create_by(
        cover: cover,
        from_id: obj.id,
        from_type: to_type,
    ) do |reln|
      reln.title = CardPackRelation.item_title(obj)
      created = true
    end
    return unless to_reln && to_reln.errors.empty?
    self.to_id = to_reln.id
    CardPackRelation.reindex(obj) if created
  end

  # returns ordered array of structs representing items in a pack, starting with the cover
  #     {
  #         id: item id
  #         type: 'Card' or 'VideoStream'
  #         deleted: boolean
  #         title: title string
  #         slug: item slug
  #         item: object (e.g. elastic result for Card, VideoStream object)
  #     }
  # doesn't include item key if load is false; for TOC
  # nil on failure
  class << self
    def pack(cover_id:, load: true)
      cover_id = cover_id.to_i
      ordered = pack_relations(cover_id: cover_id)
      return if ordered.nil?

      # get a hash that groups all non-deleted pack contents by type
      items = {}
      ordered.group_by(&:from_type).each do |type, relns|
        items[type] = get_items(
            type: type,
            ids: relns.map(&:from_id),
        )
      end

      # populate the pack from the grouping hash
      return ordered.map do |reln|
        item = items[reln.from_type].find{|c| c.id == reln.from_id}
        metaitem = OpenStruct.new(
            id: reln.from_id,
            type: reln.from_type,
        )
        if item # item was found
          metaitem.deleted = false

          # make sure title stored in relation is up to date
          title = item_title(item)
          reln.update(title: title) unless reln.title == title

          metaitem.slug = item_slug(item)
          metaitem.item = item if load
        else # item was not found; has been deleted
          metaitem.deleted = true
          metaitem.slug = reln.from_id # use id as slug
        end
        metaitem.title = reln.title
        metaitem
      end
    end

    # adds an item (id and type) to the end of the pack designated by cover_id
    # returns nil or true
    def add(cover_id:, add_id:, add_type: 'Card')
      error = Proc.new do |msg|
        log.error "#{msg || 'Error'}: adding id #{add_id} type #{add_type} to pack cover card #{cover_id}"
        return
      end

      # find the ActiveRecord record for the added item
      item_klass = add_type.safe_constantize
      error.call('No such type') unless item_klass < ActiveRecord::Base
      added_item = item_klass.find_by(id: add_id)
      error.call('Item not found') unless pack_item_add_type(added_item)

      add_id = add_id.to_i
      cover_id = cover_id.to_i
      ordered = pack_relations(cover_id: cover_id)
      error.call('Pack not found') if ordered.nil?
      return true if ordered.map{|o| [o.from_id, o.from_type]}.include? [add_id, add_type]

      # add to end of pack
      linked = ordered.last
      error.call(linked.errors.full_messages) unless linked.update to: added_item
      return true
    end

    # returns array of relations ordered according to :from and :to, starting with cover
    # nil on failure
    def pack_relations(cover_id:)
      error = Proc.new do |msg|
        log.warn "#{msg} for pack cover card #{cover_id}"
        return
      end

      relations = where(cover_id: cover_id).to_a
      error.call('no pack') if relations.empty?

      first_relation = relations.find{|r| r.from_id == cover_id && r.from_type == 'Card'}
      error.call('missing cover relation') if first_relation.nil?

      ordered = [relations.delete(first_relation)]
      until relations.empty? do
        last_size = relations.size
        relations.delete_if {|reln| ordered << reln if reln.id == ordered.last.to_id }
        error.call('missing relation') unless relations.size < last_size
      end

      return ordered
    end

    # destroy all pack relations and cover card itself
    def remove(cover_id:, destroy_hidden: true, inline_indexing: false)
      error = Proc.new do |msg|
        log.error "#{msg} for pack cover card #{cover_id}"
        return
      end

      relations = where(cover_id: cover_id)
      error.call('no relations') unless relations.any?

      error.call('error destroying relations') unless relations.map(&:destroy).all?

      cover = Card.find_by(id: cover_id)
      cover_channels = !destroy_hidden && cover.channels.to_a
      if cover
        cover.inline_indexing = inline_indexing
        cover.destroy
      else
        log.error "can't find pack cover card #{cover_id}"
        return
      end

      # deal with hidden cards within pack
      pack_card_ids = relations.select{|r| r.from_type == 'Card'}.map(&:from_id) - [cover_id]
      hidden_cards, exposed_cards = Card.where(id: pack_card_ids).partition(&:hidden)

      if destroy_hidden
        hidden_cards.each &:destroy
      else
        hidden_cards.each {|c| c.update hidden: false, channels: cover_channels}
      end

      reindex exposed_cards, refresh: false
      Card.searchkick_index.refresh
      #destroy all leaps associated with this pathway
      Leap.where(pathway_id: cover_id).destroy_all
      return true
    end

    def remove_from_pack(cover_id:, remove_id:, remove_type: 'Card', destroy_hidden: false)
      cover_id  = cover_id.to_i
      remove_id = remove_id.to_i
      remove_node = find_by(
          cover_id: cover_id,
          from_id: remove_id,
          from_type: remove_type
      )
      return false unless remove_node

      before_node = find_by(cover_id: cover_id, to_id: remove_node.id)
      return false unless before_node

      ActiveRecord::Base.transaction do
        remove_node.remap_lock if remove_node.locked
        remove_node.destroy
        before_node.update(to_id: remove_node.to_id)
      end

      if remove_node.destroyed?
        if remove_type == 'Card'
          ids = [cover_id, remove_id]
          cover, removed = ids.map{|id| Card.where(id: ids).find{|c| c.id == id} }
          if removed && removed.hidden # else card is already deleted or is independent
            if destroy_hidden
              removed.destroy
            else
              removed.update hidden: false, channels: cover.channels
            end
          end
          reindex removed
          remove_leap(cover_id, remove_id)
        end
        return true
      end
    end

    # reorder items within a pack
    # TODO: assumes item is not moved to end; i.e. to_pos is in bounds of the existing sequence and not after it
    def reorder_pack_relations(cover_id:, from_pos:, to_pos:)
      error = Proc.new do |msg|
        log.error "#{msg} reordering pack #{cover_id}, from #{from_pos} to #{to_pos}"
        return
      end

      cover_id, from_pos, to_pos = [cover_id, from_pos, to_pos].map &:to_i

      error.call('zero index') if [from_pos, to_pos].any? &:zero?
      error.call('no op') if from_pos == to_pos

      relations = pack_relations(cover_id: cover_id)
      return unless relations
      error.call('out of bounds') if [from_pos, to_pos].any? {|i| i > relations.count - 1}

      purified_relations = relations.clone.delete_if {|cpr| cpr.deleted}
      purified_from_pos = relations.index purified_relations[from_pos]
      purified_to_pos = relations.index purified_relations[to_pos]

      from_relation = relations[purified_from_pos]
      to_relation = relations[purified_to_pos] # guaranteed to exist because to_pos is within sequence
      from_previous = relations[purified_from_pos-1]

      relations_to_update = [from_previous, from_relation]

      from_previous.to_id = from_relation.to_id

      if purified_from_pos < purified_to_pos #move down
        from_relation.to_id = to_relation.to_id
        to_relation.to_id = from_relation.id
        relations_to_update << to_relation
      else #move up
        to_previous = relations[purified_to_pos-1]
        to_previous.to_id = from_relation.id
        from_relation.to_id = to_relation.id
        relations_to_update << to_previous
      end

      # returns result of block; also rolls back transaction if result is falsey
      from_relation.with_transaction_returning_status do
        relations_to_update.each { |r| r.save validate: false }
        relations_to_update.map(&:valid?).all?
      end
    end

    def get_items(type:, ids:)
      klass = type.safe_constantize

      if klass == Card
        return Search::CardSearch.new.search_by_id(ids, state: ['draft', 'published'])
      end

      if klass < ActiveRecord::Base
        return klass.where(id: ids)
      end
    end

    # [DEPRECATED] for an array of cover ids, returns a hash of form {cover_id: count} wherever found; count INCLUDES cover
    # DESC:
    #   Pack counts now represents the children cards in the cover card.
    # EXAMPLE:
    #   If a pack has 3 children cards, CardParkRelation will have 4 in count since it maintains the relation of cover to itself too.
    #   #pack_counts now excludes self-referential relation.
    def pack_counts(cover_ids:)
      where(cover_id: cover_ids).where.not(from_id: cover_ids).group(:cover_id).count
    end

    # returns a count of items in a pack, INCLUDING cover; nil if not found
    def pack_count(cover_id:)
      pack_counts(cover_ids: [cover_id])[cover_id.to_i]
    end

    def pack_item_add_type(obj)
      ALLOWED_ANCESTOR_CLASSES.find { |aac| aac.name if aac === obj }
    end

    def pack_item_result_type(obj)
      case obj
        when Hashie::Mash # assume it's an elastic result
          obj._type.camelize
        when ActiveRecord::Base
          obj.class.name
      end
    end

    def item_title(obj)
      case obj
      when Card
        obj.title || obj.message
      when VideoStream
        obj.name
      when Hashie::Mash # elastic result of card object
        obj.content.title || obj.content.message
      end
    end

    def item_slug(obj)
      # AR Card, AR VideoStream, and elastic card result all respond to slug
      obj.slug
    end

    def reindex(objs, refresh: true)
      objs = [objs] unless Array === objs
      exemplar = objs.first
      if exemplar.respond_to?(:reindex) && exemplar.class.respond_to?(:searchkick_index)
        objs.each do |obj|
          obj.inline_indexing = true if obj.respond_to?(:inline_indexing=)
          obj.reindex
        end
      end
    end

    # destroy leaps if are used on removed card in pathway
    def remove_leap(cover_id, remove_id)
      leaps = Leap.where("pathway_id = ? AND ( wrong_id = ? OR correct_id = ? )", cover_id, remove_id, remove_id)
      return if leaps.blank?
      leaps.destroy_all
    end
  end

  def assignment_url
    ClientOnlyRoutes.collection_url(
      id: self.cover.slug,
      host: self.cover.organization.host
    )
  end

  def to_id_exists?
    return true if to_id.nil?
    errors.add(:to_id, "card_pack_relation with id #{to_id} doesn't exist") unless CardPackRelation.find_by(id: to_id)
  end

  def remap_lock
    pack = CardPackRelation.pack_relations(cover_id: cover_id)
    return if pack.blank?
    # pick appropriate not deleted next position
    next_position = pack[pack.index(self) + 1..-1].detect { |cpr| !cpr.deleted }
    return if next_position.nil? || next_position.locked
    update_attributes!(locked: false)
    # lock will be moved if lock doesn't exist already on this position
    next_position.update_attributes!(locked: true) unless next_position.nil? || next_position.locked
  end

  # manageable methods start
  def is_manageable?
    # author could be nil e.g. RSS cards
    author = card&.author
    user = cover&.author
    return false unless user
    author != user
  end

  def get_ucm_values
    action, target_id = [
                          UsersCardsManagement::ACTION_ADDED_TO_PATHWAY,
                          cover_id
                        ]
    # user, card, action, target_id
    [cover.author.id, from_id, action, target_id]
  end

  def get_journey_ucm_values
    jpr = JourneyPackRelation.find_by(from_id: cover.id)
    action, target_id = [
                          UsersCardsManagement::ACTION_ADDED_TO_JOURNEY,
                          jpr&.cover_id
                        ]
    [cover.author.id, from_id, action, target_id]
  end
  # manageable methods end

  private

  ALLOWED_ANCESTOR_CLASSES = [Card, VideoStream].freeze

  # provides a type string for obj, using allowed ancestor class

  def reindex_cover
    cover.reindex_card if cover.respond_to?(:reindex_card)
  end

  #Reset completed_percentage for pathway when card is added/removed from pathway
  def reset_completed_percentage
    PackJourneyCardReprocessJob.perform_later(cover_id: cover_id)
  end

  def record_add_to_pathway_event
    record_pathway_event('card_added_to_pathway')
  end

  def record_remove_from_pathway_event
    record_pathway_event('card_removed_from_pathway')
  end

  def record_pathway_event(event)
    record_custom_event(
      event: event,
      actor: lambda {|record| record&.cover&.author},
      job_args: [
        { card: :card },
      ],
      event_opts: lambda { |record|
        {
          pathway_id: record.cover_id,
          pathway_name: record.cover&.title
        }
      }
    )
  end
end

# == Schema Information
#
# Table name: user_feed_caches
#
#  id                            :integer          not null, primary key
#  user_id                       :integer          not null
#  queue_item_ids                :text(16777215)
#  feed_request_id               :string(191)      not null
#  created_at                    :datetime
#  updated_at                    :datetime
#  channel_recommendations_shown :boolean
#

class UserFeedCache < ActiveRecord::Base
  belongs_to :user
end

# == Schema Information
#
# Table name: invitation_files
#
#  id                     :integer
#  invitation_file_id     :integer
#  first_name             :string
#  last_name              :string
#  email                  :string
#  status                 :string
#  channel_ids            :text
#  team_ids               :text
#  send_email             :boolean
#  custom_fields          :text
#  additional_information :text

class UserImportStatus < ActiveRecord::Base
  SUCCESS = 'successful'.freeze
  ERROR = 'error'.freeze
  PENDING = 'pending'.freeze

  serialize :custom_fields, JSON
  serialize :channel_ids, Array
  serialize :team_ids, Array
  serialize :additional_information, JSON

  belongs_to :invitation_file

  scope :old_users, -> { where(existing_user: true) }
  scope :new_users, -> { where(existing_user: false) }
  scope :failed_users, -> { where(status: 'error') }
  scope :resent_emails, -> { where(resent_email: true) }
end

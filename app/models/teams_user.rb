# == Schema Information
#
# Table name: teams_users
#
#  id         :integer          not null, primary key
#  team_id    :integer
#  user_id    :integer
#  as_type    :string(191)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class TeamsUser < ActiveRecord::Base
  GROUP_ROLES = %w(admin member sub_admin)

  attr_accessor :is_workflow, :skip_flush_cache, :skip_team_indexing

  include EventTracker

  belongs_to :team
  belongs_to :user

  validates :team, presence: true
  validates :user, presence: true, uniqueness: {scope: [:team, :as_type]}
  validates :as_type, inclusion: GROUP_ROLES

  validate do |teams_user|
    if user && team
      unless user.organization_id == team.organization_id
        errors.add :base, "User organization must match team organization"
      end
    end
  end

  after_commit :run_channel_after_follow_jobs, :assign_previous_contents, on: :create
  after_commit :reindex_user
  after_commit :reindex_team
  after_commit :remove_user_team_assignments, on: :destroy

  before_destroy :remove_role_for_last_type, if: -> { as_type != 'member' }
  before_destroy :can_destroy?
  before_update :remove_role_for_last_type, if: :changes_as_type_for_not_member?
  after_destroy :run_channel_after_unfollow_jobs
  after_destroy :remove_default_team_value, if: [:team_default_for_user?]
  after_commit :invalidate_channel_followers_count

  scope :admin, -> { where(as_type: 'admin') }
  scope :member, -> { where(as_type: 'member') }
  scope :sub_admin, -> { where(as_type: 'sub_admin') }

  configure_metric_recorders(
    default_actor: lambda { |record| record.team&.admins&.first },
    trackers: {
      on_create: {
        if_block: lambda {|record| record.is_not_everyone_team?},
        job_args: [{group: :team}, {member: :user}, {role: :as_type} ],
        event: 'group_user_added'
      },
      on_edit: {
        if_block: lambda {|record| record.is_not_everyone_team?},
        job_args: [{group: :team}, {member: :user}, {role: :as_type} ],
        event: 'group_user_edited'
      },
      on_delete: {
        if_block: lambda {|record| record.is_not_everyone_team? },
        job_args: [{group: :team}, {member: :user}, {role: :as_type} ],
        event: 'group_user_removed'
      }
    }
  )

  def is_not_everyone_team?
    !team.is_everyone_team?
  end

  def changes_as_type_for_not_member?
    return false unless as_type_changed?
    as_type_change.first != 'member'
  end

  private

  def reindex_user
    user.reindex_async_urgent
  end

  def reindex_team
    return if skip_team_indexing
    team.reindex_async
  end

  def remove_user_team_assignments
    user.team_assignments.where(team: self.team).destroy_all
  end

  def assign_previous_contents
    if team.auto_assign_content?
      AutoAssignOldContentToNewTeamUserJob.perform_later(user_id: self.user_id, team_id: self.team_id)
    end
  end

  def run_channel_after_follow_jobs
    team.following_channels.pluck(:id).each do |following_channel_id|
      NewTeamUserAutoFollowJob.perform_later(following_channel_id, user_id)
    end
  end

  def run_channel_after_unfollow_jobs
    team.following_channels.each do |following_channel|
      following_channel.after_unfollowed(self.user)
    end
  end

  def remove_default_team_value
    user.update_attributes(default_team_id: nil)
  end

  def team_default_for_user?
    user.default_team_id == team.id
  end

  def remove_role_for_last_type
    user_type = as_type_changed? ? as_type_change.first : as_type

    unless user.teams_users.where(as_type: user_type).count > 1
      case user_type
      when 'admin'
        group_leader_role = user.roles.find_by(default_name: 'group_leader')
        group_leader_role.remove_user(user) if group_leader_role.present?
      when 'sub_admin'
        group_leader_role = user.roles.find_by(default_name: 'group_admin')
        group_leader_role.remove_user(user) if group_leader_role.present?
      end
    end
  end

  def can_destroy?
    return true if is_workflow || !team.is_dynamic
    errors.add(:base, "Can't remove a user from dynamic group")
    false
  end

  def self.clear_cached(team_ids)
    TeamsChannelsFollow.where(team_id: team_ids).pluck(:channel_id).uniq.each do |channel_id|
      Rails.cache.delete "channel_#{channel_id}_followers_count".to_sym
    end
  end

  def invalidate_channel_followers_count
    return if skip_flush_cache
    TeamsChannelsFollow.where(team_id: self.team_id).pluck(:channel_id).uniq.each do |channel_id|
      Channel.invalidate_followers_cache(channel_id)
    end
  end
end

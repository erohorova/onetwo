# coding: utf-8
# == Schema Information
#
# Table name: users
#
#  id                      :integer          not null, primary key
#  email                   :string(191)
#  first_name              :string(255)
#  last_name               :string(255)
#  created_at              :datetime
#  updated_at              :datetime
#  encrypted_password      :string(255)      default(""), not null
#  reset_password_token    :string(191)
#  reset_password_sent_at  :datetime
#  remember_created_at     :datetime
#  sign_in_count           :integer          default(0), not null
#  current_sign_in_at      :datetime
#  last_sign_in_at         :datetime
#  current_sign_in_ip      :string(255)
#  last_sign_in_ip         :string(255)
#  picture_url             :string(255)
#  anonimage_file_name     :string(255)
#  anonimage_content_type  :string(255)
#  anonimage_file_size     :integer
#  anonimage_updated_at    :datetime
#  avatar_file_name        :string(255)
#  avatar_content_type     :string(255)
#  avatar_file_size        :integer
#  avatar_updated_at       :datetime
#  location_id             :integer
#  confirmation_token      :string(255)
#  confirmed_at            :datetime
#  confirmation_sent_at    :datetime
#  unconfirmed_email       :string(255)
#  handle                  :string(191)
#  is_brand                :boolean          default(FALSE)
#  bio                     :text(16777215)
#  coverimage_file_name    :string(255)
#  coverimage_content_type :string(255)
#  coverimage_file_size    :integer
#  coverimage_updated_at   :datetime
#  password_reset_required :boolean          default(FALSE)
#  is_complete             :boolean          default(FALSE)
#  configs                 :string(2048)
#  organization_id         :integer
#  organization_role       :string(191)
#  parent_user_id          :integer
#  job_role_id             :integer
#  time_in_role            :string(255)
#  is_active               :boolean          default(TRUE)
#  showcase                :boolean          default(FALSE)
#  is_suspended            :boolean          default(FALSE)
#  is_edcast_admin         :boolean          default(FALSE)
#  failed_attempts         :integer          default(0), not null
#  unlock_token            :string(255)
#  locked_at               :datetime
#  federated_identifier    :string(255)
#

class User < ActiveRecord::Base

  ROLES = %w[admin member].freeze
  MAX_SEARCH = 500
  SUSPENDED_STATUS = 'suspended'.freeze
  ACTIVE_STATUS = 'active'.freeze
  INACTIVE_STATUS = 'inactive'.freeze
  PERMITED_STATUSES = [SUSPENDED_STATUS, ACTIVE_STATUS, INACTIVE_STATUS].freeze
  DEFAULT_FIELDS = %w[email first_name last_name handle]

  attr_accessor :user_interests, :bulk_import, :workflow_import
  attr_accessor :websites
  attr_accessor :mentor_emails
  attr_accessor :upload_mode # Used for user_created Influx events, Workflow
  attr_accessor :skip_generate_images, :skip_indexing
  attr_accessor :authentication_method, :score_entry, :platform
  attr_accessor :can_post_to_channel


  include IdentityCache, CustomLockable
  include SocialConnections
  include PubnubChannels
  include UserRollable
  include EdcastSearchkickWrapper
  include EventTracker
  include CustomCarousel
  include WorkflowPatcher
  include WorkflowObserver

  enum data_export_status: {
    started: 0,
    done:    1,
    error:   2
  }

  # ISSUE: Concurrent user creation in bulk import ensures Organization#users_count to be increment which consumes process in the DB.
  #  System now allows multiple users to be created concurrently. Post each creation, every transaction increments the same column value.
  #  Concurrent update to the same organization record piles up the increment SQL query.

  # NOTE: Total users_count is not useful as it is a collective count of users those are ACTIVE + SUSPENDED + INACTIVE.
  #  Whereas system tries to query active OR suspended OR inactive users separately.
  belongs_to :organization#, counter_cache: true
  belongs_to :parent_user, class_name: 'User', foreign_key: 'parent_user_id'

  has_many :card_user_shares
  has_many :cards_shared_with, through: :card_user_shares, source: :card
  has_many :card_user_permissions
  has_many :cards_permitted, through: :card_user_permissions, source: :card
  has_many :card_reportings
  has_many :comment_reportings
  has_many :wallet_transactions
  has_many :transactions
  has_many :processed_transactions, -> { where.not(payment_state: 0) }, class_name: 'Transaction', foreign_key: 'user_id'
  has_one  :wallet_balance, dependent: :destroy
  has_one  :user_notification_read, dependent: :destroy
  has_many :teams_users, dependent: :delete_all
  has_many :teams, through: :teams_users
  has_many :assignments, dependent: :destroy
  has_many :mdp_user_cards, dependent: :destroy
  has_many :team_assignments, through: :assignments
  has_many :groups_users, dependent: :delete_all
  has_many :not_banned_groups_users, -> { not_banned }, class_name: 'GroupsUser', foreign_key: 'user_id'
  has_many :groups, -> { not_closed }, class_name: 'Group', through: :not_banned_groups_users, source: :group
  has_many :not_banned_from_groups, class_name: 'Group', through: :not_banned_groups_users, source: :group
  has_many :all_groups, class_name: 'Group', through: :groups_users, source: :group
  has_many :private_groups, -> { where(type: 'PrivateGroup') }, class_name: 'Group', through: :groups_users, source: :group
  has_many :video_streams, foreign_key: 'creator_id', source: :video_stream
  has_many :interests_users, as: :taggable, class_name: 'Tagging', dependent: :destroy
  has_many :interests, through: :interests_users, class_name: 'Interest', foreign_key: 'tag_id', source: 'tag'

  has_many :skills_users, dependent: :destroy
  has_many :skills, through: :skills_users
  has_many :identity_providers, dependent: :destroy
  has_many :saml_providers, -> { where(type: 'SamlProvider') }, class_name: 'IdentityProvider'
  has_many :events_users
  has_many :events
  has_many :notification_entries
  has_many :channels
  has_many :channels_curators, dependent: :destroy
  has_many :channels_authors, -> { author }, class_name: 'ChannelsUser', foreign_key: 'user_id', dependent: :destroy
  has_many :trusted_authors, -> { trusted_author }, class_name: 'ChannelsUser', foreign_key: 'user_id', dependent: :destroy
  has_many :pwc_records, dependent: :destroy

  has_many :follows, dependent: :destroy
  has_many :user_follows, -> { where(followable_type: 'User') }, class_name: 'Follow'
  has_many :user_followers, through: :user_follows, source: 'followable', source_type: 'User'

  has_many :channel_follows, -> { where(followable_type: 'Channel') }, class_name: 'Follow'
  has_many :direct_following_channels, through: :channel_follows, source: 'followable', source_type: 'Channel'

  has_many :emails
  has_many :cards_users, dependent: :delete_all
  has_many :clients_users
  has_many :clients, through: :clients_users, source: :client
  has_many :clients_admins, foreign_key: :admin_id
  has_many :administered_clients, through: :clients_admins, source: :client
  has_many :unseen_notifications, -> { unseen }, class_name: 'Notification', source: :notification
  has_many :notifications, dependent: :destroy
  has_many :user_preferences, dependent: :destroy
  has_many :users_external_courses, dependent: :destroy
  has_many :external_courses, through: :users_external_courses
  has_many :users_integrations, dependent: :destroy
  has_many :integrations, through: :users_integrations
  has_many :cards, foreign_key: :author_id
  has_many :schools, dependent: :destroy
  has_one :pubnub_channel, as: :item, dependent: :destroy
  has_one :public_pubnub_channel, -> { public_channel }, class_name: 'PubnubChannel', as: :item
  has_one :private_pubnub_channel, -> { private_channel }, class_name: 'PubnubChannel', as: :item
  has_one :assignment_notice_status, dependent: :destroy
  has_many :dismissed_contents, dependent: :destroy
  has_one :user_onboarding, dependent: :destroy
  has_many :users_mentors, dependent: :destroy
  has_many :mentors, through: :users_mentors
  has_many :users_websites, dependent: :destroy
  has_many :learning_queue_items, dependent: :destroy
  has_many :user_content_completions, dependent: :destroy
  has_many :bookmarks, dependent: :destroy
  has_many :access_logs, dependent: :destroy
  has_one  :profile, class_name: 'UserProfile', dependent: :destroy, inverse_of: :user
  has_many :quiz_question_attempts
  has_many :assigned_custom_fields, class_name: 'UserCustomField'
  has_many :custom_fields, through: :assigned_custom_fields
  has_many :user_badges
  has_many :manage_cards, class_name: 'UserManageCard'
  has_many :orders
  has_many :user_purchases
  has_many :user_subscriptions
  has_many :card_subscriptions
  has_many :active_card_subscriptions, -> { where('start_date <= ? && (end_date >= ? || end_date IS NULL)', Time.now, Time.now) },
                                         foreign_key: "user_id", class_name: "CardSubscription"
  has_many :subscribed_cards, source: 'card', through: :card_subscriptions
  has_many :active_org_subscriptions, -> { where('start_date <= ? && end_date >= ?', Date.today, Date.today) },
                                         foreign_key: "user_id", class_name: "UserSubscription"

  has_many :my_followers, -> {where(followable_type: 'User').order("follows.created_at DESC")}, class_name: 'Follow', foreign_key: :followable_id
  has_many :my_following_users, through: :my_followers, source: :user

  has_many :users_cards_managements
  has_many :managed_cards, -> { distinct }, through: :users_cards_managements, source: :card

  before_validation :assign_handle
  validates :organization, presence: true
  validates :email, uniqueness: { scope: :organization_id, case_sensitive: false }, format: { with: Devise.email_regexp }, if: :email
  validate :picture_url_format
  validates :handle, uniqueness: { scope: :organization_id, case_sensitive: false }, if: :handle

  has_many :user_roles, dependent: :destroy
  has_many :roles, through: :user_roles
  has_many :permissions, through: :roles, source: :role_permissions

  has_many :posts
  has_many :comments
  has_many :votes
  has_many :projects, foreign_key: "reviewer_id"
  has_many :project_submissions, foreign_key: "submitter_id"
  has_many :project_submission_reviews, foreign_key: "reviewer_id"
  has_many :invitations, foreign_key: "recipient_id"
  has_many :embargos
  has_many :embargo_files
  has_many :email_templates, foreign_key: 'creator_id'
  has_one :external_profile, class_name: 'Profile', dependent: :destroy, inverse_of: :user
  has_many :topic_requests, class_name: 'TopicRequest'
  has_many :invitation_files, foreign_key: :sender_id

  validates_format_of :handle, with: /\A@[a-zA-Z0-9\-_]*\Z/, message: 'Should contain characters, numbers, - or _', allow_nil: true
  validate do
    errors.add(:handle, 'unavailable') if self.handle && User.reserved_handle?(self.handle)
    errors.add(:email, 'cant be blank') if (!self.email.nil? && self.email.strip == '')
  end

  validates_presence_of     :password, if: :password_required?
  validates_confirmation_of :password, if: :password_required?
  validates_length_of       :password, within: 8..72, allow_blank: true
# -----------------------------------------------------------------------------
  # USER PASSWORD REGEX:
  # 1 capital, 1 lower case, 1 special character, and 8+ total characters
  validates_format_of       :password, :with => %r{\A(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?["!#$%&'()*+,-.\/\\:;<=>?@\[\]^_`{|}~])(?=.{8,}).*\z}x, allow_blank: true, if: :password_required?
  #   \A                                             Start of string
  #   (?=.*?[A-Z])                                   lookahead - 1 of A-Z
  #   (?=.*?[a-z])                                   lookahead - 1 of a-z
  #   (?=.*?[0-9])                                   lookahead - 1 of 0-9
  #   (?=.*?["!#$%&'()*+,-.\/\\:;<=>?@\[\]^_`{|}~])  lookahead - 1 special char
  #   (?=.{8,})                                      lookahead  - 8 or more of total characters
  #   .*\z                                           End of string
  #    x                                             Ignore whitespace in regex
# -----------------------------------------------------------------------------
  validates_length_of       :bio, within: 0..1000, allow_blank: true
  validates :status, inclusion: { in: PERMITED_STATUSES }
  has_attached_file :coverimage,
    styles: {
      banner: '1440', #used for public page banner
      profile: 'x400', #used on my feed page
      medium: '256x256>',
      large: '512x512>'
    },
    default_url: 'default_banner_user_image.png', validate_media_type: false,
                    processors: [:cropper, :thumbnail]
  validates_attachment_content_type :coverimage, :content_type => /^image\/(png|jpeg|jpg|pjpeg|x-png|gif|tiff)$/, :message => 'Invalid file type'

  has_attached_file :anonimage, default_url: 'anonymous-user.png', validate_media_type: false
  validates_attachment :anonimage, :attachment_content_type => "image/png"

  has_attached_file :avatar, styles: {
    tiny: '64x64#', #forum uses 66x66 size
    small: 'x120',
    medium: '256x256>',
    large: '512x512>'
  },
  default_url: 'new-anonymous-user-:style.jpeg', validate_media_type: false,
                    processors: [:cropper, :thumbnail]

  process_in_background :coverimage
  process_in_background :anonimage
  process_in_background :avatar

  validates_attachment_content_type :avatar, :content_type => /^image\/(png|jpeg|jpg|pjpeg|x-png|gif|tiff)$/, :message => 'Invalid file type' unless Rails.env.test?
  validates_attachment_size :avatar, :less_than => 15.megabytes, :message => 'Picture too big'


  before_validation :set_user_status
  after_validation :update_reset_password_required, on: :update
  before_create :set_exclude_from_leaderboard
  before_create :associate_forum_user_if_present
  before_create :set_user_status
  before_save :strip_all_tags_from_user_input

  after_create :generate_user_onboarding

  after_create :update_role

  after_commit :prepare_user_profile, on: :create

  after_commit :update_details_to_okta, on: [:update]
  after_commit :update_interests, on: [:update]
  after_commit :update_websites, on: [:update]
  after_commit :update_mentors, on: [:update]
  after_commit :update_everyone_group_members, on: :update
  after_commit ->(obj) { obj.setup_lms_user }, on: [:create, :update]
  after_commit :add_user_to_everyone_team, if: Proc.new { |user| user.previous_changes.key?(:organization_role) }
  after_commit :add_user_to_whitelisted_teams, on: :create

  after_commit on: [:create, :update] do
    if self.is_complete && self.previous_changes['is_complete'] == [false, true]
      AutoFollowChannelsJob.perform_later(user_id: self.id)
    end
  end

  after_commit on: [:update] do
      perform_avatar_image_job if previous_changes['picture_url']
  end

  after_commit :reindex_async_urgent
  after_commit :detect_upsert, on: [:create, :update]
  after_commit :detect_delete, on: :destroy

  cache_belongs_to :organization

  scope :not_is_edcast_admin, -> { where(is_edcast_admin: false) }
  scope :suspended, -> { where(status: 'suspended', is_suspended: true)}
  scope :not_suspended, -> { where(status: 'active', is_suspended: false, is_active: true)}
  scope :completed_profile, -> { where(is_complete: true)}
  scope :search_by_name, ->(search_term) { where("first_name like :q OR last_name like :q OR email like :q OR handle like :q", q: "%#{search_term}%") }
  scope :by_created_at, -> { order('users.created_at DESC') }
  scope :visible_members, -> {
    where.not(email: nil).not_suspended
  }
  scope :not_anonymized, -> { where("is_anonymized = false OR is_anonymized is null") }
  scope :assigned_users_for_card, -> (card_id) { joins({assignments: [:team_assignments]}).where('assignments.assignable_id = ? AND team_assignments.team_id is NULL', card_id)}
  scope :with_handle, -> { where.not(handle: nil) }

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable,
         :confirmable
  accepts_nested_attributes_for :profile, update_only: true

  delegate :expert_topics, :learning_topics, :time_zone, :language, :job_title, to: :profile, allow_nil: true

  configure_metric_recorders(
    default_actor: :itself,
    trackers: {
      on_create: {
        job_args: [{user: :itself}],
        event: 'user_created'
      },
      on_edit: {
        job_args: [{user: :itself}],
        event: 'user_edited'
      }
    }
  )

  def self.sortable_fields
    ['name', 'first_name', 'last_name', 'handle', 'email']
  end

  def self.exact_match_fields
    ['expert_topics']
  end

  def self.searchable_fields
    {
      'name' => {
        boost: 15,
        prefix_search_enabled: true
      },
      'first_name' => {
        boost: 15,
        prefix_search_enabled: true
      },
      'last_name' => {
        boost: 15,
        prefix_search_enabled: true
      },
      'email' => {
        boost: 15,
      },
      'handle' => {
        boost: 10,
        prefix_search_enabled: true
      },
      'expert_topics.searchable_topic_label' => {
        boost: 1
      }
    }
  end

  edcast_searchkick({
    index_prefix: 'edcast',
    settings: {
      index: {
        analysis: {
          analyzer: {
            analyzer_case_insensitive: {
              tokenizer: 'keyword',
              filter: 'lowercase'
            }
          }
        }
      }
    },
    mappings: {
      _default_: {
        properties: {
          joined_at: {
            type: 'date'
          },
          teams: {
            type: :nested,
          },
          custom_fields: {
            type: :nested,
            properties: {
              value: {
                type: 'string',
                analyzer: 'analyzer_case_insensitive'
              }
            }
          },
          expert_topics: {
            properties: {
              domain_id: {
                index: 'not_analyzed',
                type: 'string'
              },
              domain_name: {
                index: 'not_analyzed',
                type: 'string'
              },
              topic_name: {
                index: 'not_analyzed',
                type: 'string'
              },
              topic_id: {
                index: 'not_analyzed',
                type: 'string'
              },
              topic_label: {
                index: 'not_analyzed',
                type: 'string'
              },
              domain_label: {
                index: 'not_analyzed',
                type: 'string'
              },
              searchable_topic_label: {
                analyzer: 'edcast_search_analyzer',
                type: 'string'
              },
              searchable_domain_label: {
                analyzer: 'edcast_search_analyzer',
                type: 'string'
              }
            }
          },
          learning_topics: {
            properties: {
              domain_id: {
                index: "not_analyzed",
                type: "string"
              },
              domain_name: {
                index: "not_analyzed",
                type: "string"
              },
              topic_name: {
                index: "not_analyzed",
                type: "string"
              },
              topic_id: {
                index: "not_analyzed",
                type: "string"
              },
              topic_label: {
                index: "not_analyzed",
                type: "string"
              },
              domain_label: {
                index: "not_analyzed",
                type: "string"
              }
            }
          }
        }
      }
    }
  })

  def self.string_search_query(q:, organization_id:)
    search_conditions = []
    # First: Exact match name, first_name, last_name
    search_conditions << { multi_match: { query: q, fields: ['name.searchable', 'first_name.searchable', 'last_name.searchable']}}

    # Second: Fuzzy match name, first_name, last_name
    search_conditions << { multi_match: { query: q, fields: ['name.searchable', 'first_name.searchable', 'last_name.searchable'], fuzziness: 'AUTO' }}

    # Exact match email
    search_conditions << { term: { "email.sortable": q }}

    { query: { bool: { should: search_conditions}}, filter: { bool: { must: { term: { organization_id: organization_id }}}}}
  end

  def self.expertise_search_query(expertise_names:, organization_id:)
    { filter: { bool: { should: { terms: { "expert_topics.topic_name": expertise_names }}, must: { term: { organization_id: organization_id}}}}}
  end

  # lists SMEs and Influencers for an organization
  # TODO: Remove recommended and active, instead use self.suggested_users_for
  def self.recommended(limit:, offset:)
    sme_and_influencers.limit(limit).offset(offset)
  end

  def self.sme_and_influencers
    joins(user_roles: :role)
      .where("LOWER(roles.default_name) in ('sme', 'influencer')")
      .distinct
  end

  def self.sme_by_scores
    joins(user_roles: :role)
      .where("LOWER(roles.default_name) = 'sme'")
      .joins("LEFT JOIN user_level_metrics url on users.id = url.user_id and url.period = 'all_time' ")
      .order("url.smartbites_score DESC")
      .distinct
  end

  def self.following_influencers_by_followers
    joins(user_roles: :role)
      .where("LOWER(roles.default_name) = 'influencer'")
      .joins("LEFT JOIN follows followers ON followers.followable_id = follows.followable_id and followers.followable_type = 'User' and followers.deleted_at is null")
      .group("users.id")
      .order("count(followers.followable_id) DESC")
      .distinct
  end

  def self.influencers_by_followers
    joins(user_roles: :role)
      .where("LOWER(roles.default_name) = 'influencer'")
      .joins("LEFT JOIN follows followers ON followers.followable_id = users.id and followers.followable_type = 'User' and followers.deleted_at is null")
      .group("users.id")
      .order("count(followers.followable_id) DESC")
      .distinct
  end

  def self.active(exclude_ids:)
    where.not(id: exclude_ids)
    .where.not(is_edcast_admin: true)
    .where.not(is_suspended: true)
    .where(is_complete: true)
    .where.not(handle: nil)
  end

  def self.user_team_members(team_ids)
    joins(:teams_users).where("teams_users.team_id IN (?)", team_ids).group("users.id")
  end

  def self.not_following_users(user_id)
    joins("left join follows on follows.followable_id = users.id and follows.user_id = #{user_id} AND follows.followable_type = 'User'").where("follows.id is null")
  end

  # AIM: To compute the percentage of correct answers of a pathway with poll questions
  def percentage_correct cover_id
    card = Card.find_by(id: cover_id, organization_id: self.organization_id)
    return unless card && card.pack_card?

    correct_count = 0
    user_attempts = card.poll_attempts.where(user_id: self.id).map { |c| [c.quiz_question_id, c.response] }.to_h

    correct_answers = card.pack_quiz_correct_options.map { |c| [c.quiz_question_id, c.id] }.to_h

    user_attempts.each do |key, value|
      if (correct_answers[key] == value['selections'].first.to_i)
        correct_count = correct_count + 1
      end
    end

    correct_answers_count = correct_answers.count
    if correct_answers_count > 0
      ((correct_count.to_f / correct_answers_count.to_f).round(2)) * 100
    else
      0.0
    end
  end

  def self.suggested_users_for(user:)
      # method1 - Less efficient
      # LEFT OUTER JOIN
      #   follows ON follows.user_id = #{user.id} AND follows.followable_type = 'User' AND follows.followable_id = users.id
      # AND follows.followable_id IS NULL

      # Method 2 - better when subquery returns large dataset
        # AND NOT EXISTS (SELECT * from follows WHERE follows.user_id = #{user.id} AND follows.followable_type = 'User' AND follows.followable_id = users.id)

      # Method 3 - better when subquery returns relatively smaller dataset
      # AND users.id NOT IN (select follows.followable_id from follows where follows.user_id = #{user.id} AND followable_type = 'User')

    User.joins("
      INNER JOIN
        user_roles ON user_roles.user_id = users.id
      INNER JOIN
        roles ON roles.id = user_roles.role_id
      WHERE roles.default_name IN ('sme', 'influencer')
        AND users.organization_id = #{user.organization.id}
        AND users.is_edcast_admin = false
        AND users.is_suspended = false
        AND users.is_complete = true
        AND users.handle IS NOT NULL
        AND users.id NOT IN (select follows.followable_id from follows where follows.user_id = #{user.id} AND follows.followable_type = 'User' AND follows.deleted_at IS NULL)
        AND users.id != #{user.id}")
  end

  def recommended_users(user_type: ,filter_params: {}, sort_by: :score, limit: MAX_SEARCH)
    key = user_type == "expert" ? :search_expertise : :search_peer_learners
    filter_params[key] = true
    search_response = Search::UserSearch.new.search(
      q:      nil,
      viewer: self,
      allow_blank_q: true,
      filter: filter_params,
      limit:  limit,
      sort:   sort_by,
      load:   false )
    search_response.results
  end


  ##################### only for search result processing #####################
  # Provide list of cards and it will retirn all cards visibe to that user
  # Based on readable channels for a user
  # We are using this method for following purposes:
  # 1. to post-process Search results from ECL
  def visible_cards(cards)
    return cards if is_admin_user?

    user_readable_channels = readable_channels.pluck(:id)
    cards_channel_ids = {}
    cards.map{|card| cards_channel_ids[card.id] = card.channel_ids }

    cards.select {|card| having_access_to_card(card, cards_channel_ids, user_readable_channels)}
  end

  def shared_cards
    Card.where(organization_id: organization_id, is_public: true)
      .joins("LEFT JOIN card_user_shares  ON cards.id = card_user_shares.card_id")
      .joins("LEFT JOIN (teams_cards
        INNER JOIN teams_users ON teams_cards.team_id = teams_users.team_id and"+
        " teams_cards.type = 'SharedCard'and teams_users.user_id = #{id})"+
        " ON cards.id = teams_cards.card_id ")
      .where("card_user_shares.user_id = #{id} OR teams_users.user_id = #{id}")
      .group('cards.id')
  end

  def team_shared_cards(sort, order)
    Card.where(organization_id: organization_id, is_public: true)
        .joins("INNER JOIN teams_cards ON cards.id = teams_cards.card_id AND teams_cards.type = 'SharedCard'")
        .joins("INNER JOIN teams_users ON teams_cards.team_id = teams_users.team_id AND teams_users.user_id = #{id}")
        .group('cards.id').order("teams_cards.#{sort} #{order}")
  end

  #If cards posted to only private channel and user is having access to that channel then only that card is accessible
  def having_access_to_card(card, cards_channel_ids, user_readable_channels)
    return true if card.author_id.present? && card.author_id == self.id
    return true if cards_channel_ids[card.id].empty?
    (cards_channel_ids[card.id] & user_readable_channels).present?
  end

  #Direct followed + Team_channel_follows + Public channels
  def readable_channels
    user_team_ids = self.teams_users.pluck(:team_id)
    Channel
      .where(organization_id: organization_id)
      .joins("LEFT JOIN follows on follows.followable_type = 'Channel' and follows.followable_id = channels.id and follows.user_id = #{self.id}")
      .joins("LEFT JOIN teams_channels_follows on teams_channels_follows.channel_id = channels.id")
      .where("(channels.is_private = 0) OR (channels.is_private = 1 and channels.id = follows.followable_id) OR (teams_channels_follows.team_id in (:team_ids))",
        team_ids: user_team_ids)
  end

  #############################################################################

  def my_team_leader_ids
    teams.map{|team| team.teams_admins.pluck(:user_id) }.flatten
  end

  def users_from_my_teams
    TeamsUser.where(team_id: teams.excluding_everyone_team).pluck(:user_id).uniq
  end

  def users_from_my_channels
    Follow.where(followable: followed_channels.not_general).pluck(:user_id).uniq
  end

  def is_group_leader?
    teams_users.admin.where(user_id: id).present?
  end

  # Deprecated not used anymore
  # def users_by_ids(ids:)
  #   User.where(id: ids).order("field(id, #{ids.join(',')})")
  # end

  def channels_to_curate
    channel_ids = ChannelsCurator.joins(:channel).where(user_id: id, channels: {curate_only: true}).pluck(:channel_id)
    ChannelsCard.where(state: ChannelsCard::STATE_NEW, channel_id: channel_ids)
      .where("created_at <= ?", 4.days.ago)
      .select("COUNT(card_id), channel_id")
      .group(:channel_id)
      .order("COUNT(card_id) DESC")
  end

  def search_data
    as_json(except: %i[configs exclude_from_leaderboard data_export_status])
      .merge(**additional_search_data, **search_expert_topics,**roles_data)
  end

  def additional_search_data
    {
      name: name,
      photo: photo(:small),
      pubnub_channels_to_subscribe_to: try(:pubnub_channels_to_subscribe_to),
      followed_channels: followed_channels.map { |ch| { id: ch.id, label: ch.label } },
      followers_count: followers_count,
      teams: teams_users.map { |tu| { id: tu.team_id, role: tu.as_type } },
      follower_ids: followers.map(&:id),
      following_ids: following_ids,
      coverimages: fetch_coverimage_urls,
      learning_topics: learning_topics,
      joined_at: created_at&.strftime("%F"),
      custom_fields: searchable_custom_fields
    }
  end

  def roles_data
    {
      roles: roles.map(&:name)
    }
  end

  def search_expert_topics
    {
      expert_topics: (expert_topics || []).flatten.map do |el|
        el.merge(
          {
            searchable_topic_label: el['topic_label'],
            searchable_domain_label: el['domain_label']
          }
        )
      end
    }
  end

  def expert_topics_name
    topics = expert_topics

    if topics.nil?
      []
    else
      topics.map {|topic| topic.stringify_keys['topic_name']}.compact
    end
  end

  def learning_topics_name
    topics = learning_topics

    if topics.blank?
      []
    else
      topics.map{|topic| topic.stringify_keys['topic_name']}.compact
    end
  end

  def learning_topic_ids
    topics = learning_topics

    if topics.blank?
      []
    else
      topics.map {|topic| topic.stringify_keys['topic_id']}.compact
    end
  end

  def learning_topics_name_with_level
    topics = learning_topics

    if topics.nil?
      []
    else
      topics.map{|topic| {name: topic.stringify_keys['topic_name'], level: topic.stringify_keys['level'], level_name: UserProfile::SKILL_LEVELS[topic.stringify_keys['level']]}}.compact
    end

  end

  def perform_avatar_image_job
    AvatarImageJob.perform_later(user_id: id, picture_url: picture_url)
  end

  def onboard!
    self.update(is_complete: true) unless self.onboarded?
  end

  def onboarded?
    self.is_complete && self.onboarding_completed?
  end

  def reset_onboarding!
    update(is_complete: false) if is_complete
  end

  def reindex_async
    unless self.skip_indexing
      IndexJob.perform_later(self.class.name, id)
    end
  end

  def reindex_async_urgent
    unless self.skip_indexing
      IndexJob.set(queue: :reindex_urgent).perform_later(self.class.name, id)
    end
  end

  def update_with_reindex_async! opts
    update!(opts)
    reindex_async
  end

  def update_with_reindex_async opts
    ret = update(opts)
    if ret
      reindex_async
    end
    ret
  end

  def send_devise_notification(notification, *args)
    user_data = {
      'id' => self.id,
      'email' => self.email,
      'first_name' => self.first_name,
      'last_name' => self.last_name,
      'name' => self.name,
      'platform' => self.platform || 'web',
      'organization_id' => self.organization_id,
      'federated_identifier' => self.federated_identifier }

    user_data.merge!('password' => Base64.encode64(self.password)) if self.password.present?

    devise_mailer.send(notification, user_data, *args).deliver_later
  end

  def add_to_org_general_channel
    if organization.general_channel
      if organization_role == 'admin'
        organization.general_channel.add_authors([self])
      else
        organization.general_channel.add_followers([self])
      end
    end
  end

  def associate_forum_user_if_present
    if self.organization_id != Organization.default_org&.id && !self.email.nil? && !self.organization.client.nil?
      forum_user = ClientsUser.joins(:user).where(users: {organization_id: Organization.default_org&.id, email: self.email}, client: self.organization.client).first.try(:user)
      if forum_user
        self.parent_user_id = forum_user.id
      end
    end
  end

  def can_be_followed_by? _user
    _user.organization_id == organization_id
  end

  def update_reset_password_required
    if password_reset_required && password.present? && !is_bulk_imported?
      self.password_reset_required = false
    end
  end

  def email_required?
    false
  end

  def mailable_email
    if email && !Email.where(user: self, email: email).where.not(bounced_at: nil).present?
      email
    else
      confirmed_email = emails.where(confirmed: true, bounced_at: nil).first
      confirmed_email && confirmed_email.email
    end
  end

  def picture_url_format
    unless self.picture_url.nil?
      begin
        pic = URI.parse(self.picture_url)
        if pic.scheme != 'http' && pic.scheme != 'https'
          errors.add(:picture_url, 'accept http and https only')
          return
        end
      rescue URI::InvalidURIError
        errors.add(:picture_url, 'invalid URI')
        return
      end
    end
  end

  def default_org_user
    default_org = Organization.default_org
    (organization_id == default_org&.id) ? self : User.find_by(email: email, organization_id: default_org&.id)
  end

  def is_equivalent_user(user)
    (self.id == user.id) || (self.default_org_user && self.default_org_user.id == user.id)
  end


  def pref_group_opt_out?(group, context)
    preferenceable = group.is_a?(PrivateGroup) ? [group, group.resource_group] : group
    user_preferences.where(key: "#{context}.opt_out", value: 'true', preferenceable: preferenceable).exists?
  end

  def pref_email_opt_out?(context)
    user_preferences.where(key: "#{context}.opt_out", value: 'true').exists?
  end

  def pref_newsletter_opt_in
    pref_get_bool('edcast_newsletter.opt_in', default: false)
  end
  alias_method :pref_newsletter_opt_in?, :pref_newsletter_opt_in

  def pref_newsletter_opt_in=(value)
    pref_set_bool('edcast_newsletter.opt_in', value)
  end

  def pref_set_bool(key, value)
    user_preferences.
        find_or_initialize_by(key: key).
        update(value: to_bool(value))
  end

  def pref_get_bool(key, default: false)
    pref = UserPreference.fetch_by_user_id_and_key_and_preferenceable_type_and_preferenceable_id(self.id, key, nil, nil)
    pref ? to_bool(pref.value) : default
  end

  def to_bool(value)
    ActiveRecord::Type::Boolean.new.type_cast_from_user value
  end

  def channel_ids_followed_via_teams
    TeamsUser
      .where(user_id: self.id)
      .joins('INNER JOIN teams_channels_follows on teams_channels_follows.team_id = teams_users.team_id')
      .pluck(:channel_id)
  end

  def add_skill(skill, args={})
    user_skill = skills_users.where(skill_id: skill.id).first_or_initialize
    user_skill.description = args[:description]
    user_skill.credential = args[:credential]
    user_skill.credential_name = args[:credential_name]
    user_skill.credential_url = args[:credential_url]
    user_skill.experience = args[:experience]
    user_skill.skill_level = args[:skill_level]
    user_skill.expiry_date = args[:expiry_date]
    user_skill.save
  end

  private :to_bool

  def pref_notification_content
    pref_get_bool('notification.content', default: true)
  end

  def pref_notification_content=(value)
    pref_set_bool('notification.content', value)
  end

  def pref_notification_follow
    pref_get_bool('notification.follow', default: true)
  end

  def pref_notification_follow=(value)
    pref_set_bool('notification.follow', value)
  end

  def pref_notification_weekly_activity
    pref_get_bool(UserPreference::WEEKLY_ACTIVITY_KEY, default: true)
  end

  def pref_notification_weekly_activity=(value)
    pref_set_bool(UserPreference::WEEKLY_ACTIVITY_KEY, value)
  end

  def pref_notification_activity_on_content
    pref_get_bool('notification.activity_on_content', default: true)
  end

  def pref_notification_activity_on_content=(value)
    pref_set_bool('notification.activity_on_content', value)
  end

  def pref_notification_mention
    pref_get_bool('notification.mention', default: true)
  end

  def pref_notification_mention=(value)
    pref_set_bool('notification.mention', value)
  end

  def pref_profile(key)
    pref_get_bool(key, default: true) if ["show_continuous_learning", "show_education", "show_competencies"].include?(key)
  end

  def pref_notification_assignment
    pref_get_bool(UserPreference::ASSIGNMENT_NOTIFICATION_KEY, default: true)
  end

  def pref_notification_assignment=(value)
    pref_set_bool(UserPreference::ASSIGNMENT_NOTIFICATION_KEY, value)
  end

  def score group
    score_entry = Score.where("group_id = ? AND user_id = ?", group.id, id).first
    if score_entry.nil?
      {:score => 0, :stars => 0}
    else
      {:score => score_entry.score, :stars => score_entry.stars}
    end
  end

  def photo(size = :small, send_default: true)
    url = nil

    if avatar.present?
      url = avatar.url(size)
    elsif picture_url.present? # Processed picture stored on s3 might not be present for some time
      url = picture_url
    elsif send_default # paperclip takes care of default. Send as is
      url = avatar.url(size)
    end

    return if url.nil?

    uri = URI.parse(url)
    uri.scheme = Settings.serve_images_on_ssl ? 'https' : 'http'
    uri.to_s
  end

  def cover(size: :banner, send_default: true)
    if coverimage.present? || send_default
      coverimage.url(size)
    else
      nil
    end
  end

  def full_name
    provider = saml_providers.first
    if provider && (array = provider.auth_info['Preferred Name'])
      preferred_name = array.join(',')
    end

    [preferred_name, name].detect(&:present?)
  end

  def name
    return "#{first_name.try(:strip)} #{last_name.try(:strip)}".strip if first_name.present? || last_name.present?
    return email.split('@', 2).first if email.present?
    return ''
  end

  def searchable_custom_fields
    assigned_custom_fields.map do |cf|
      {
        id: cf.custom_field_id,
        display_name: cf.custom_field.display_name,
        value: cf.value,
        enabled_in_search: cf.custom_field.enable_people_search
      }
    end
  end


  def has_full_name?
    first_name && last_name
  end

  #TODO: This is a confusing interface should be is_super_admin? or is_internal_admin?
  # Used for internal API's
  def is_admin?
    super_admin_user_ids = Rails.cache.fetch('super-admin-user-ids-v1', expires_in: 5.minutes) do
      SuperAdminUser.pluck(:user_id)
    end
    super_admin_user_ids.include?(self.id)
  end

  def is_edcast_dot_com_user?
    email.present? && (email.ends_with?("@course-master.com") || email.ends_with?("@edcast.com"))
  end

  def is_org_admin?
    organization.is_admin?(self) || self.authorize?(Permissions::ADMIN_ONLY)
  end

  # User role in an org
  def is_admin_user?
    organization.is_admin?(self)         ||
    self.authorize?(Permissions::ADMIN_ONLY)  ||
    self.is_sub_admin?                        ||
    self.is_group_sub_admin?
    #change when we handle curators
    #authorize_permission(Permissions::ADMIN_ONLY)
  end

  # user has permissions from the permissions list sub_admin
  def is_sub_admin?
    join_permission_in_role_group_name(['sub_admin']).present?
  end

  # user has permissions from the permissions list group_sub_admin
  # and should be in the group with type sub_admin
  def is_group_sub_admin?
    join_permission_in_role_group_name(['group_sub_admin']).present? &&
    self.teams_users.exists?(as_type: 'sub_admin')
  end

  # check user permissions in the permissions list by group_name
  def join_permission_in_role_group_name(role_group_names)
    user_permissions = self.get_user_permissions
    role_permissions = Permissions.get_permissions_for_groups(role_group_names).keys
    join_permissions = user_permissions & role_permissions
    join_permissions
  end

  def has_access_assign_content?
    Rails.cache.fetch(cache_key, expires_in: 1.hours) do
      sub_admin = is_sub_admin? && authorize?(Permissions::MANAGE_ANALYTICS)
      group_sub_admin = is_group_sub_admin? && authorize?(Permissions::MANAGE_GROUP_ANALYTICS)
      assign = authorize?(Permissions::ASSIGN_CONTENT)
      admin = authorize?(Permissions::ADMIN_ONLY)
      admin || assign || sub_admin || group_sub_admin
    end&.to_bool
  end

  def classmates group
    if group
      group.members.where('user_id <> ?',self.id)
    else
      []
    end
  end

  def self.cm_admin
    where(email: 'admin@course-master.com').first || where(email: 'admin@edcast.com').first
  end

  def generate_images
    unless skip_generate_images
      #submit these two jobs and forget about them
      AnonImageJob.perform_later(user_id: id) unless anonimage.present?
      AvatarImageJob.perform_later(user_id: id, picture_url: picture_url) if previous_changes['picture_url']
    end
  end

  # set user time zone using ip address
  def set_time_zone(ip_address:)
    SetUserTimeZoneJob.perform_later(user_id: self.id, ip_address: ip_address) if ip_address
  end

  def set_terms_accepted
    profile = self.profile || self.build_profile
    profile.tac_accepted = true
    profile.tac_accepted_at = Time.now
    unless profile.save
      log.warn "error while saving terms and conditions in user profile: #{profile.errors.full_messages}"
    end
  end

  def top_live_video_stream
    video_streams.published.where(status: 'live').last
  end

  def my_video_streams_visible_to(viewer: )
    query = "state = 'published'"
    if viewer&.id == id
      query += " OR state = 'draft'"
    end

    filtered = video_streams.where(query).includes(:recordings, card: :channels)

    status_order = {'live' => 1, 'upcoming' => 2, 'past' => 3}

    # make sure viewer has access to video streams - private channels privacy
    # short circuit when viewer is self
    filtered.select do |x|
      (viewer == self || x.has_access?(viewer)) && (x.status == 'live' || (x.status == 'upcoming' && x.start_time >= Time.now.utc - 1.hour) || (x.status == 'past' && x.past_stream_available?))
    end.sort do |x,y|
      if x.status != y.status
        status_order[x.status] <=> status_order[y.status]
      elsif x.status == y.status
        if x.status == 'past'
          y.start_time <=> x.start_time
        else
          x.start_time <=> y.start_time
        end
      end
    end
  end

  # TODO: Lots of logic (definitely complex). Needs revision.
  # 1. Identifies identity_provider irrespective if account is confirmed or not.
  # 2. If identity_provider is present, then:
  #    2.a Un-confirmed destiny user will be marked as authenticable once they are confirmed user of a provider.
  #    2.b Confirmed destiny user will be retrieved.
  #    2.c If user is not a confirmed user of a provider, do nothing. SKIP.
  # 3. If identity_provider is absent, then:
  #    3.a Either we do have an existing user matching email from omniauth (and confirmed from provider).
  #    3.b Or create a new user.
  def self.get_or_create_from_social(auth_hash:)
    identity_provider_class = IdentityProvider.class_for(auth_hash[:provider].gsub('_access_token',''))

    # STEP #1
    identities, identity = self.locate_identity_provider(auth: auth_hash, klass: identity_provider_class)

    matched_user = nil
    email = auth_hash[:info][:email]

    # STEP #2
    if !identity && email.present? && identity_provider_class.is_verified?(auth_hash)
      matched_user = where(email: email, organization_id: auth_hash[:organization_id]).first
      # if there is an identity that match a user's email address, then assume that it is also authenticable
      identity = identities.detect do |ip|
        ip.user == matched_user
      end
    end

    # STEP #3
    if identity
      if auth_hash[:info][:verified] || identity.auth
        identity.update!(auth: true) #indicate that this external account should be authenticable
        matched_user = identity.user
      else
        matched_user = nil
      end
    else
      matched_user ||= create_user_with_password(auth: auth_hash)
    end

    matched_user
  end

  def self.get_or_create_with_email(auth_hash:)
    User.where(email: auth_hash[:email], organization_id: auth_hash[:organization_id]).first_or_initialize do |user|
      user.password = User.random_password
      user.is_complete = false
      user.password_reset_required = true
      user.organization_role = 'member'
      user.skip_confirmation!
      user.save!
    end
  end

  def get_variations(first_name, last_name, limit=10)
    concat_chars = ['','_','-']

    # Generate all possible variations with the permitted concat chars
    variations_with_concat_chars = Array.new
    concat_chars.each do |cc|
      variations_with_concat_chars.push("@#{first_name}#{cc}#{last_name}")
    end

    # Generate variations by appending numbers
    variations_with_numbers = Array.new
    (1..limit).each do |i|
      concat_chars.each do |cc|
        variations_with_numbers.push("@#{first_name}#{cc}#{last_name}#{i}")
      end
    end

    # combine variations in order of preference for the given limit
    variations = variations_with_concat_chars + variations_with_numbers[0, (limit - variations_with_concat_chars.size)]

    # return all possible variations
    return variations
  end

  def get_email_variations(email, limit=10)

    # Generate variations by appending numbers
    variations_with_numbers = Array.new
    variations_with_numbers.push("@#{email}")
    (1..limit).each do |i|
      variations_with_numbers.push("@#{email}#{i}")
    end

    # combine variations in order of preference for the given limit
    variations = variations_with_numbers

    # return all possible variations
    return variations
  end

  def normalize_string(string)
    # replaces accented (and other special chars) in a name and converts them to plain alphabets
    # Test String: Adélaïde Éléonore-Françoise
    return string.mb_chars.normalize(:kd).gsub(/[^x00-\x7F]/, '').downcase.to_s
  end

  def assign_handle
    if [first_name, last_name].any?(&:present?) && handle.blank?
      first_n = first_name.blank? ? '' : normalize_string(first_name)
      last_n = last_name.blank? ? '' : normalize_string(last_name)
      return if first_n.blank? && last_n.blank? # normalize_string returns "" for non english characters
      variations = get_variations(first_n, last_n, 100)
      self.handle = get_uniq_handle_in_org(variants: variations, org: organization)
    elsif email.present? && handle.blank?
      email_norm = email.blank? ? '' : normalize_string(email.split('@', 2).first)
      return if email_norm.blank? # normalize_string returns "" for non english characters
      variations = get_email_variations(email_norm, 100)
      self.handle = get_uniq_handle_in_org(variants: variations, org: organization)
    elsif handle.present?
      self.handle = "@#{handle}" unless handle.starts_with?('@')
    end
    # `else` block do nothing, we dont have a first name or last name to
    # assign a reasonable handle
  end

  def get_uniq_handle_in_org(variants:, org:)
    variants.detect do |variant|
      !User.unscoped.exists?(handle: variant, organization: org)
    end
  end

  def self.reserved_handle?(handle)
    paths = Paths.first_level.map &:downcase
    (paths & [handle.downcase, "@#{handle.downcase}"]).present?
  end

  def following?(followable, followable_type = nil)
    followable_type = followable.class.name if followable_type.nil?
    following_id_and_type?(id: followable.id, type: followable_type)
  end

  def following_user_id?(id)
    following_id_and_type?(id: id, type: 'User')
  end

  CACHE_FOLLOW_TYPES = %w[User Channel Post]

  def following_id_and_type?(id:, type:)
    if CACHE_FOLLOW_TYPES.include? type
      # cache AR query for all followees of this type
      followees = follows.where(followable_type: type).pluck(:followable_id)
      return followees.include? id.to_i
    else
      return follows.where(followable_id: id, followable_type: type).any?
    end
  end

  #TODO:
  #Need to revisit this..
  #OKTA
  def self.update_password_to_okta(resource_params)
    user = devise_find_by_reset_password_token(resource_params[:reset_password_token])
    return true unless user.organization.present? and user.organization.okta_enabled? #check if okta is enabled.

    user.precheck_valid_password?(resource_params) #check valid password before updating it to okta
    user.register_existing_user_to_okta(resource_params) unless user.federated_identifier.present?
    return user if user.errors.present?
  end

  def register_existing_user_to_okta(resource_params)
    user_details = self.attributes.slice(*['email', 'first_name', 'last_name'])
    password = resource_params['password'].to_s
    username = nil

    user_details.merge!(password: password, username: username)
    user_details.symbolize_keys!

    alliance = UserInfo::Alliance.new(organization: organization, user_params: user_details)
    alliance.provision(user: self)
  end

  def precheck_valid_password?(resource_params)
    self.password = resource_params[:password]
    self.password_confirmation = resource_params[:password_confirmation]
    self.valid?
  end

  def self.devise_find_by_reset_password_token(token)
    reset_password_token = Devise.token_generator.digest(self, :reset_password_token, token)

    recoverable = find_or_initialize_with_error_by(:reset_password_token, reset_password_token)

    if recoverable.persisted?
      unless recoverable.reset_password_period_valid?
        recoverable.errors.add(:reset_password_token, :expired)
      end
    end
    return recoverable
  end

  def update_details_to_okta
    return true unless self.organization.okta_enabled?

    if(self.previous_changes.include?('first_name') ||
      self.previous_changes.include?('last_name') ||
        self.previous_changes.include?('encrypted_password'))

      internal_federation = Okta::InternalFederation.new(organization: self.organization)
      opts = { first_name: self.first_name, last_name: self.last_name, password: self.password }
      response = internal_federation.update_user(user: self, opts: opts)

      if response.nil?
        logger.warn("Error on okta")
      else
        return true
      end
    end
  end
  ##Okta end

  def password_changeable?
    identity_providers.blank?
  end

  def capsule_data
    as_json(only: [:id, :first_name, :last_name, :handle, :is_suspended, :bio]).merge('name' => name, 'photo' => photo, 'photo_medium' => photo(:medium), 'picture' => photo(:medium))
  end

  def onboarding_data
    capsule_data.merge(
      password_required: identity_providers.blank? && self.password_reset_required,
      password_changeable: password_changeable?,
      websites:          users_websites.map(&:url),
      user_interests:    interests.map(&:name),
      avatar:            photo(:medium, send_default: false),
      onboarding_options:     self.onboarding_options,
      onboarding_steps_names: self.onboarding_options.keys )
  end

  def mini_profile_data
    capsule_data.merge("followers_count" => followers_count)
  end

  def handle_no_at
    handle && handle.gsub('@', '')
  end

  def show_modal?
    handle.blank? || (first_name.blank? && last_name.blank?) || password_reset_required
  end

  def is_bulk_imported?
    bulk_import
  end

  # returns the list of users that the user is following(aka following)
  # Deprecated
  def followed_users
    User.where(id: followed_user_ids)
  end

  # Deprecated
  def followed_user_ids
    follows.where(followable_type: 'User').pluck(:followable_id)
  end

  # Deprecated
  def followed_by_users offset: nil, limit: 25
    followers = User.where(id: Follow.where(followable: self).pluck(:user_id))
    offset.present? ? followers.offset(offset).limit(limit) : followers
  end

  def following
    user_followers.not_suspended.completed_profile.uniq
  end

  def viewable_channels(channels_ids)
    return Channel.none if channels_ids.blank?

    user_team_ids = self.teams_users.pluck(:team_id)

    Channel
      .where(id: channels_ids, organization_id: organization_id)
      .joins("LEFT JOIN follows on follows.followable_type = 'Channel' and follows.followable_id = channels.id and follows.user_id = #{self.id} and follows.deleted_at IS NULL")
      .joins("LEFT JOIN teams_channels_follows on teams_channels_follows.channel_id = channels.id")
      .where("(channels.is_private = 0) OR (channels.is_private = 1 and channels.id = follows.followable_id) OR (teams_channels_follows.team_id in (:team_ids))",
        team_ids: user_team_ids)
      .select('channels.*')
      .uniq
      .order("field(channels.id, #{channels_ids.join(',')})")
  end

  def following_ids
    user_follows.pluck(:followable_id)
  end

  def following_count
    following.count
  end

  #get user followers
  def followers
    User.where(id: follower_ids).not_suspended.completed_profile
  end

  # return the number of followers
  def followers_count
    follower_ids.count
  end

  def follower_ids
    Follow.where(followable: self).pluck(:user_id).uniq
  end

  # returns a list of channels that the user is authoring
  def authoring_channels
    Channel.includes(:channels_users).where("channels_users.user_id = (:user_id) AND channels_users.as_type = 'author'", user_id: self.id).references(:channels_users)
  end

  ############ TODO: This could be has_many association directly. ############
  # returns a list of channels that the user is following
  def followed_channels
    Channel.where(id: followed_channel_ids).order('label ASC')
  end

  def followed_channel_ids
    follows.where(followable_type: 'Channel').pluck(:followable_id) | self.channel_ids_followed_via_teams
  end

  def writable_channels
    Channel.writable_channels_for(self)
  end

  # TODO: role model as add_user method that adds user to specific role
  # safe to delete this method
  def make_sme
    user_roles.where(user: self, role: self.organization.sme_role).first_or_create
  end

  def nonfollowed_onboarding_influencers
    User.suggested_users_for(user: self).limit(10).offset(0).reject {|u| Follow.where(followable: u, user: self).any?}
  end

  # invoked upon follow
  def after_followed(follower, restore: false)
    OnFollowEnqueueJob.perform_later(
        id: self.id,
        type: self.class.name,
        follower_id: follower.id
    )
    PubnubNotifyChangeJob.perform_later('subscribe',follower,self.public_pubnub_channel_name)
    IndexJob.perform_later(follower.class.name, follower.id)
  end

  def after_unfollowed(follower)
    PubnubNotifyChangeJob.perform_later('unsubscribe',follower,self.public_pubnub_channel_name)
    IndexJob.perform_later(follower.class.name, follower.id)
    OnUnfollowEnqueueJob.perform_later(
        id: self.id,
        type: self.class.name,
        follower_id: follower.id
    )
  end


  def set_config(key, value)
    raise 'key is too long' if key.to_s.length > 25
    raise 'value is too long' if value.to_s.length > 25
    _configs_json = configs.blank? ? {} : ActiveSupport::JSON.decode(configs)
    _configs_json[key] = value
    self.configs = ActiveSupport::JSON.encode(_configs_json)
  end

  def get_config(key)
    @_configs_json ||= configs.blank? ? {} : ActiveSupport::JSON.decode(configs)
    @_configs_json[key.to_s]
  end

  def private_pubnub_channel_name
    pubnub_channel_name(as_type: 'private', item_type: 'User')
  end

  def public_pubnub_channel_name
    pubnub_channel_name(as_type: 'public', item_type: 'User')
  end

  def self.pubnub_channels_to_subscribe_to_for_id(id)
    # fall back to AR if elasticsearch fails, may be user not yet indexed
    Search::UserSearch.new.search_by_id(id).try(:[], 'pubnub_channels_to_subscribe_to') || User.find(id).pubnub_channels_to_subscribe_to
  end

  def pubnub_channels_to_subscribe_to
    ret = [self.private_pubnub_channel_name, Settings.pubnub.global_channel_name]

    ret |= PubnubChannel.where(item_type: 'Channel', item_id: followed_channel_ids, as_type: 'public').select(:name).map(&:name)

    follows.where(followable_type: 'User').select(:id, :followable_id).find_in_batches do |batch|
      ret |= PubnubChannel.where(item_type: 'User', item_id: batch.map(&:followable_id), as_type: 'public').select(:name).map(&:name)
    end

    ret << organization.try(:public_pubnub_channel_name)
    ret.compact
  end

  def onboarding_candidates
    query = follows.where(followable_type: %w[Channel User])

    follows_count = query.count
    return if follows_count >= 2
    ret = {
        channels: Channel.readable_channels_for(self, _organization: self.organization).reject{|ch| Follow.where(followable: ch, user: self).any?},
        influencers: nonfollowed_onboarding_influencers,
    }
    return nil if follows_count >= (ret[:channels].length + ret[:influencers].length)
    return nil unless (ret[:channels].present? || ret[:influencers].present?)
    return ret
  end

  def clone_for_org(org, role)
    new_user = User.new(first_name: first_name, last_name: last_name, email: email, password: User.random_password, password_reset_required: true, is_complete: false, organization: org, organization_role: role, parent_user_id: self.id)
    new_user.avatar = avatar
    new_user.coverimage = coverimage
    new_user.skip_reconfirmation!
    new_user.skip_confirmation!

    if self.configs
      old_configs = ActiveSupport::JSON.decode(self.configs)
      cloned_configs = {}
      ['user-feeds-tour', 'chrome-extension-tour', 'card-tour'].each do |key|
        cloned_configs[key] = old_configs[key]
      end
      new_user.configs = ActiveSupport::JSON.encode(cloned_configs)
    end

    new_user.save

    # clone identity providers
    IdentityProvider.where(user: self).find_each do |ip|
      new_ip = ip.dup
      new_ip.user = new_user
      new_ip.save
    end

    new_user
  end

  def self.find_for_authentication(auth_options)
    org = Organization.find_by_request_host(auth_options[:host])
    where(:email => auth_options[:email], :organization_id => org.id).first
  end

  def self.serialize_from_session(key, salt)
    record = fetch_multi(key).first
    record if record && record.authenticatable_salt == salt
  end
  # courses that this user and the parent user from which this
  # user was cloned are enrolled in. Purpose: Forum users are in default org,
  # so people are enrolled in groups under their default org identity, but
  # they need to be able to see courses under GE when they are signed in
  # as the GE user
  def courses
    all_courses = (my_courses_only | (parent_user.try(:my_courses_only) || []))

    # if in default org context, return all courses
    if organization == Organization.default_org
      return all_courses
    else
      # If in specific org context, return only those courses that are in
      # that org
      return all_courses.select {|c| organization.id == c.organization_id}
    end
  end

  # courses that this user object is directly enrolled in
  def my_courses_only
    groups.where(type: 'ResourceGroup').select {|co| co.savannah?}
  end

  # Intermediate state - Forum users currently belong to default org.
  # Users in custom orgs are mapped to forum users through parent_user relationship
  def forum_user
    (organization_id == Organization.default_org&.id) ? self : ((parent_user.try(:organization_id) == Organization.default_org&.id) ? parent_user : self)
  end

  # returns hash of form {as_type => [user's teams for this as_type]}
  def teams_grouped
    grouped = teams.select('teams.*, teams_users.as_type').group_by(&:as_type)
    grouped.default = [].freeze
    return grouped.with_indifferent_access
  end

  def has_an_assignment?(assignable)
    assignments.where(assignable: assignable).where.not(state: "dismissed").present?
  end

  def assignment_for(assignable)
    assignments.where(assignable: assignable).first
  end

  def set_org_role(role)
    update!(organization_role: role, skip_indexing: true) if ROLES.include?(role)
  end

  # Override devise method to check if user account is active
  def active_for_authentication?
    super && is_active? && !is_suspended? && !account_locked? && (status != SUSPENDED_STATUS)
  end

  def switch_to_active_status
    return unless status == INACTIVE_STATUS
    update_attributes(status: ACTIVE_STATUS)
  end

  def anonymize_attributes
    {
      first_name: Settings.anonymized_user_values.first_name,
      last_name: Settings.anonymized_user_values.last_name,
      picture_url: Settings.anonymized_user_values.image_url,
      avatar: Settings.anonymized_user_values.image_url,
      is_anonymized: true
    }
  end

  def anonymize
    self.update(anonymize_attributes)
  end

  def suspend!
    UserTransitions::Suspend.new(users: [self], organization: self.organization).mark
  end

  def suspended?
    is_suspended && !is_active && status == 'suspended'
  end

  def unsuspend!
    UserTransitions::Active.new(users: [self], organization: self.organization).mark
  end

  def active?
    is_active && !is_suspended && status == 'active'
  end

  def inactivate!
    UserTransitions::Inactive.new(users: [self], organization: self.organization).mark
  end

  def inactive?
    is_active && !is_suspended && status == 'inactive'
  end

  def accessible_video_streams(viewer)
    query = "state = 'published'"
    if viewer.try(:id) == id
      query += " or state = 'draft'"
    end

    video_streams.where(query).includes(:creator, :recordings, card: [:file_resources, :tags]).select{|v| v.has_access?(viewer)}
  end

  # DEPRECATED
  def integration_connected(integration_id)
    users_integrations.where(integration_id: integration_id).first
  end

  def load_external_courses
    enabled_intgration_ids = users_integrations.connected.pluck(:integration_id)
    external_courses.where(integration_id: enabled_intgration_ids)
      .select("external_courses.*, users_external_courses.status as status, users_external_courses.raw_info as raw_info, users_external_courses.due_date as due_date, users_external_courses.assigned_date as assigned_date").includes(:integration)
  end


  def fields_for(integration)
    return nil unless integration.fields
    fields = JSON.parse(integration.fields)
    ui = users_integrations.where(integration: integration).first
    if(ui && ui.field_values)
      values = JSON.parse(ui.field_values)
      fields.each {|field| field["value"] = values[field["name"]]} if values
    end
    fields
  end

  def jwt_token(extra_params={})
    client_user_id = self.clients_users.last.try(:client_user_id)
    payload = {
      host_name: self.organization.client.try(:name),
      user_id: self.id,
      client_user_id: client_user_id,
      is_org_admin: is_admin_user?,
      is_superadmin: is_admin?,
      organization_id: organization_id,
      timestamp: Time.now }

    web_session_timeout = self.organization.web_session_timeout
    if extra_params[:is_web] && web_session_timeout.present? && web_session_timeout.to_i > 0
      payload[:exp] = (Time.now + web_session_timeout.minutes).to_i
    end

    if extra_params[:expire_after]
      payload[:exp] = (Time.now + extra_params[:expire_after]).to_i
    end

    payload.merge!(extra_params) unless extra_params.blank?
    token = JWT.encode(payload, Settings.features.integrations.secret, 'HS256')
  end

  #Keeping Expiration time 1 day for now
  def api_jwt_token(shared_secret)
    exp_hours = Settings.jwt_token_expire_hours.to_i
    payload = {
      host_name: self.organization.client.try(:name),
      user_id: self.id,
      email: self.email,
      timestamp: Time.now,
      exp: (Time.now + exp_hours.hours).to_i
    }
    token = JWT.encode(payload, shared_secret, 'HS256')
  end

  def dev_api_token(api_key)
    credential, org = DeveloperApiCredential.fetch_api_credential_and_org(api_key: api_key)
    api_jwt_token(credential.shared_secret) if credential && (org == self.organization)
  end

  def last_assignment_created_at
    assignments.order('created_at DESC').select('created_at').limit(1).first.try(:created_at)
  end

  def assignment_notice_dismissed_at
    assignment_notice_status.try(:notice_dismissed_at)
  end

  def has_dismissed_content?(content_id, content_type)
    dismissed_contents.where(content_id:content_id, content_type: content_type).exists?
  end

  def has_bookmarked_content?(content_id, content_type)
    bookmarks.where(bookmarkable_id: content_id, bookmarkable_type: content_type).exists?
  end

  def get_bookmarks
    Card
      .joins("inner join bookmarks on cards.id = bookmarks.bookmarkable_id")
      .where("bookmarks.bookmarkable_type = 'Card' AND bookmarks.user_id = #{self.id}")
  end

  def has_upvoted_content?(content_id, content_type)
    Vote.where(user_id: self, votable_type: content_type, votable_id: content_id).exists?
  end

  def fetch_coverimage_urls
    urls = {}
    coverimage.styles.keys.each do |style|
      urls[style.to_s + "_url"] = cover(size: style)
    end
    urls
  end

  def fill_missing_data(auth:)
    self.skip_reconfirmation!
    self.skip_confirmation!

    idp_email = auth[:info][:email]
    former_email = self.email

    if (idp_email.present? && self.email != idp_email)
      self.email = idp_email
      current_provider = [auth['provider'], auth[:provider]].detect(&:present?)
      # `federated_identifier` captures the login/Username attribute from Okta. This is helpful for Okta's API usability.
      # As far as, SSO ID is constructed with the IDP's user ID, `federated_identifier` will never change.
      self.federated_identifier = auth[:info]['preferred_username'] if current_provider == 'lxp_oauth'
    end

    logger.warn("Missing email address from provider=#{auth[:provider]} user_id=#{self.id} auth_hash=#{ActiveSupport::JSON.encode(auth)}") if idp_email.blank?

    self.picture_url = auth[:info][:image] if self.picture_url.blank?
    self.first_name  = auth[:info][:first_name] if self.first_name.blank?
    self.last_name   = auth[:info][:last_name] if self.last_name.blank?

    unless self.save
      if self.errors[:email].present?
        # Looks like there are chances with user.save throwing error only because of email-address being invalid
        # try updating the user again without the email address this time
        log.info(%Q{#{self.email} has been taken. Trying to save the user again without the email address.})
        # Revert to former email. It can either nil or an old email/
        self.email = former_email
        self.save
      end
    end

    email = Email.find_or_initialize_by(user_id: self.id, email: auth[:info][:email])
    been_confirmed = email.confirmed
    email.confirmed = true #auto confirm the user's email
    email.save unless been_confirmed

    self
  end

  # V2 does not send default image, V1 above does
  def fetch_coverimage_urls_v2
    urls = {}
    coverimage.styles.keys.each do |style|
      urls[style.to_s] = cover(size: style, send_default: false)
    end
    urls
  end

  def fetch_avatarimage_urls
    urls = {}
    avatar.styles.keys.each do |style|
      urls[style.to_s + "_url"] = cover(size: style)
    end
    urls
  end

  # V2 does not send default image, V1 above does
  def fetch_avatar_urls_v2
    urls = {}
    avatar.styles.keys.each do |style|
      urls[style.to_s] = photo(style)
    end
    urls
  end

  def generate_user_onboarding
    user_onboarding = self.build_user_onboarding
    user_onboarding.save
  end

  def onboarding_completed?
    return true unless user_onboarding.present?
    user_onboarding.completed?
  end

  def new_assignments
    assignments.where("created_at > '#{self.assignment_notice_dismissed_at}' AND state =?", 'ASSIGNED')
  end

  def follow_all_team_users(team, opts)
    FollowTeamUsersJob.perform_later(
      user_id: self.id,
      team_id: team.id,
      device_id: opts[:device_id],
      platform: opts[:platform])
  end

  def show_follow_all_for_team(team)
    !(team_user_ids(team) - followed_user_ids).empty?
  end

  #ToDO will update this query with JOIN, so it will fire a single aql query
  def team_user_ids(team)
    team.users.pluck(:id) - [self.id]
  end

  def accessible_source_ids
    teams.accessible_source_ids.flatten.uniq
  end

  def completion_state_for(completable_type, completable_id)
    user_content_completions.find_by(
      completable_type: completable_type,
      completable_id: completable_id
    )&.state
  end

  def skip_onboarding
    user_onboarding.start!
    user_onboarding.complete!
  end

  def dismissed_card_ids
    dismissed_contents.cards.pluck(:content_id)
  end

  def bookmarked_card_ids
    bookmarks.cards.pluck(:bookmarkable_id)
  end

  def completed_card_ids
    user_content_completions.cards.pluck(:completable_id)
  end

  def setup_lms_user
    LmsUserSyncJob.perform_later(self.id)
  end

  def skip_card_ids_for_learning
    (completed_card_ids + bookmarked_card_ids + cards.pluck('id')).uniq
  end

  # AIM:
  # Check if user already exists for following cases:
  # 1. user with args[:email] exists?
  # 2. user with invitation record exists?
  # 3. user with blank email but with identity provider record for org exists?
  #
  # Required arguments:
  # organization
  # email
  # uid
  # provider
  #
  def self.already_exists?(args)
    organization = args[:organization]

    # prevent users from accessing closed Organization unless invited or bulk uploaded from admin
    return true if (args[:email].present? &&
      (organization.users.where(email: args[:email]).exists? || Invitation.where(invitable: organization, recipient_email: args[:email]).exists?))

    # Existing user with sso sign in where email is not present should be checked using uid
    if args[:provider].present?
      provider_class = IdentityProvider.class_for(args[:provider].gsub('_access_token',''))
      identities, identity = self.locate_identity_provider(
        auth: { uid: args[:uid], organization_id: organization.id },
        klass: provider_class
      )

      identity.present?
    else
      false
    end
  end

  # Returns:
  #   1. Onboarding options configured for a user.
  #   2. If configs unavailable for a user, fallbacks to onboarding_options at an org level.
  #   3. If configs unavailable for an org, defaults to `profile_setup`.
  #   Makes sure `profile_setup` is always a part of onboarding flow.
  def onboarding_options
    steps = self.profile.try(:onboarding_options)

    if steps.blank?
      org   = self.organization
      steps = org.onboarding_options
    end

    steps.merge(UserOnboarding::DEFAULT_ONBOARDING_STEPS).sort_by { |k,v| v }.to_h
  end

  def auth_landing_url
    org = self.organization
    url_helpers = Rails.application.routes.url_helpers

    if org.is_auth_enabled?('email')
      raw_token, encrypted_token = Devise.token_generator.generate(self.class, :welcome_token)

      login_landing = url_helpers.login_landing_api_v2_users_url(host: "#{org.host_name}.#{Settings.platform_domain}", welcome_token: raw_token, invitation_link: true)
      join_url = url_helpers.join_url_mobile_api_v2_users_url(host: "#{org.host_name}.#{Settings.platform_domain}", welcome_token: raw_token)
      self.update_column(:welcome_token, encrypted_token)

      org_hash = {id: org.id, name: org.name, host_name: org.host_name, hostName: org.host_name, photo: org.mobile_photo}
      branchmetrics_payload = {
        '$deeplink_path': 'join_organization',
        'join_url': join_url,
        '$desktop_url': login_landing,
        'organization': org_hash
        }
      BranchmetricsApi.generate_link(branchmetrics_payload, organization: org) || login_landing
    else
      url_helpers.new_user_session_url(host: "#{org.host_name}.#{Settings.platform_domain}")
    end
  end

  def send_assessment_report_email_for(card_id: , aws_url:)
    card = self.organization.cards.find(card_id)
    UserMailer.send_assessment_report(s3_url: aws_url, user_id: self.id, card_title: card.snippet.slice(0,50)).deliver_later
  end

  def wallet_api_token
    payload = {
      account_identifier: self.id,
      organization_id: self.organization_id
    }

    JWT.encode(payload, Settings.features.integrations.secret, 'HS256')
  end

  def self.random_password
    (('a'..'z').to_a.sample(2) +
      ('A'..'Z').to_a.sample(2) +
      (0..9).to_a.sample(2) +
      ['"','!','#','$','%','&',"'",'(',')','*','+',',','-','.','/',':',';','<','=','>','?','@','[',']','^','_','`','{','|','}','~',']']
      .sample(2))
    .join('')
  end

  def accept_invitation(invitation_id:)
    @invitation = Invitation.find(invitation_id)

    if !@invitation.nil? && !@invitation.accepted_at.present?
      @invitation.update(recipient_id: self.id, accepted_at: Time.now)
    end
  end

  # TODO:
  #   Make this more efficient in order to handle edge-cases too
  #   Awaiting reply from Okta on this though.
  # user_details = {
  #   email: ...,
  #   first_name: ...,
  #   last_name: ...,
  #   sso_id: ...
  # }
  # redirect_url = Next URI after authentication in Okta.
  # Okta needs unique username (i.e login), so making password and login equal so as to make session successful.
  def provision_and_sign_in(args:, redirect_url:, is_sso_user: false)
    organization = self.organization
    user_details = self.attributes.slice(*['email', 'first_name', 'last_name'])

    if is_sso_user
      password = username = "#{organization.id}-#{args[:sso_id]}@edcast.com"
    else
      password = args['password'].to_s
      username = nil
    end

    user_details.merge!(password: password, username: username)
    user_details.symbolize_keys!

    alliance = UserInfo::Alliance.new(organization: organization, user_params: user_details)
    federated_user = alliance.provision(user: self)

    if federated_user && organization.okta_enabled?
      opts  = { 'password' => Base64.encode64(password), 'federated_identifier' => self.federated_identifier, 'redirect_url' => redirect_url }
      User.url_to_federated_service(details: opts, organization: organization)
    end
  end

  def self.url_to_federated_service(details:, organization:)
    klass = Okta::InternalFederation.new(organization: organization)
    opts  = { password: details['password'], federated_identifier: details['federated_identifier'] }
    klass.session_redirect_uri(redirect_url: details['redirect_url'], opts: opts)
  end

  def complete_card(card)
    user_content_completion = user_content_completions.where(completable: card).first_or_create
    if !user_content_completion.started? && !user_content_completion.completed?
      user_content_completion.start!
    end
    if !user_content_completion.completed?
      user_content_completion.complete!
    end
  end

  def all_subscriptions_expired?(card_id:)
    (!card_subscriptions.where(card_id: card_id).empty? && active_card_subscriptions.where(card_id: card_id).empty?)
  end

  def card_ids_for_team_users(types, group_ids: [], sub_admin:)
    user_ids = user_ids_for_teams(types, group_ids: group_ids)
    card_ids = organization.cards.where(author_id: user_ids).ids
    if sub_admin && group_ids.blank?
      card_ids += organization.cards.where(is_public: true).ids
    end
    card_ids
  end

  # team_ids in which the user has type sub_admin (admin or member if needs)
  def team_ids_for_group_type(types)
    self.teams_users.where(as_type: types).pluck(:team_id)
  end

  def permission_to_access(card_id)
    # to return the card if no permissions have been provided at all
    CardUserPermission.find_by(card_id: card_id, user_id: id, show: true) ||
    CardTeamPermission.where(team_id: team_ids, card_id: card_id, show: true).any?
  end

  # only private cards should be given to this method.
  # this method will return cards those are accessible to this user.
  def private_accessible_cards(card_ids)
    selected_cards = []
    cards = Card.where(id: card_ids)
    card_ids = cards.map(&:id)
    user_id = id

    user_followed_channel_ids = followed_channels.pluck(:id)
    user_followed_team_ids = teams_users.pluck(:team_id)

    # check whether the cards is restricted to any user
    cards_user_permissions = CardUserPermission.where(card_id: card_ids, show: true)&.group_by(&:card_id)
    # check whether the card is restricted to any team
    cards_team_permissions = CardTeamPermission.where(card_id: card_ids)&.group_by(&:card_id)

    # check if the cards are shared to this user via direct-share, channel or team.
    cards_user_access = CardUserShare.where(card_id: card_ids, user_id: user_id)&.index_by(&:card_id)
    cards_channels = ChannelsCard.where(channel_id: user_followed_channel_ids, card_id: card_ids)&.index_by(&:card_id)
    cards_teams = SharedCard.where(card_id: card_ids, team_id: user_followed_team_ids)&.index_by(&:card_id)

    cards.each do |card|
      card_id = card.id.to_i
      #check is_public
      if card.is_public
        selected_cards << card
        next
      end

      # check whether the card is restricted
      if cards_user_permissions&.[](card_id).present? || cards_team_permissions&.[](card_id).present?
        # if restricted to this user(directly or via team) then select that card
        if cards_user_permissions.values.flatten.map(&:user_id).include?(user_id) ||
          (
            cards_team_permissions&.[](card_id)&.any? &&
            cards_team_permissions[card_id].select { |ctp| user_followed_team_ids.include?(ctp.team_id) && ctp.show }.any?
          )
          selected_cards << card
        end
        next
      end

      if (!card.explicit_permissions_present?) && (cards_user_access&.[](card_id) || cards_channels&.[](card_id) || cards_teams&.[](card_id))
        selected_cards << card
      end
    end
    selected_cards
  end

  def expire_card_subscription(card:)
    card_subscription = active_card_subscriptions.find_by(card_id: card.id)
    card_subscription&.update(end_date: Time.now)
  end

  # To fetch all teams for a user ->
  # Teams user is a part of
  # Teams user is invited to but not accepted yet
  # Public teams of the org
  def all_user_teams
    Team.joins("LEFT JOIN teams_users tus ON tus.team_id = teams.id AND teams.organization_id = #{self.organization_id}")
      .joins("LEFT JOIN invitations ON invitations.invitable_id = teams.id AND invitations.invitable_type = 'Team' " +
        "AND invitations.accepted_at IS NULL")
      .where("tus.user_id = #{self.id} OR (teams.is_private = 0 AND teams.organization_id = #{self.organization_id}) "+
        "OR invitations.recipient_id = #{self.id}")
  end

  def pending_teams
    Team.joins("INNER JOIN invitations ON invitations.invitable_id = teams.id AND invitations.invitable_type = 'Team'")
      .where("invitations.accepted_at IS NULL AND invitations.recipient_id = ?",self.id)
  end

  def user_ids_for_teams(types, group_ids: [])
    team_ids = team_ids_for_group_type(types)
    team_ids &= group_ids.map(&:to_i) unless group_ids.blank?
    TeamsUser.where(team_id: team_ids).pluck(:user_id).uniq
  end

  # id teams in which the user is sub_admin
  # used as a subquery in get_user_ids_by_teams
  def get_team_ids_by_as_types(types, team_ids: [])
    teams = organization.teams.joins(:teams_users)
      .where(teams_users: { as_type: types, user_id: self.id })
    teams = teams.where(id: team_ids) if team_ids.present?
    teams.select(:id)
  end

  # id users who belong to teams in which the user is sub_admin
  # used as a subquery in cards_for_sub_admin
  def get_user_ids_by_teams(team_ids)
    organization.users.joins(:teams_users)
      .where(teams_users: { team_id: team_ids })
      .uniq
      .select(:id)
  end

  # all cards from users who belong to teams in which the user is sub_admin
  def cards_for_sub_admin(types: 'sub_admin', team_ids: [])
    team_ids = get_team_ids_by_as_types(types, team_ids: team_ids)
    users = get_user_ids_by_teams(team_ids)
    organization.cards.where(author_id: users).uniq
  end

  def self.users_fields(org)
    key = CustomField.maximum(:updated_at)
    Rails.cache.fetch("user-fields-#{org.id}-#{key}") do
      custom_fields = org.custom_fields.joins(:configs).where(
        configs: {
          value: 'true', name: 'enable_in_filters', category: 'custom_fields'
        }
      ).pluck(:display_name)
      custom_fields.push(*User::DEFAULT_FIELDS)
    end
  end

  def create_external_profile
    uid = SecureRandom.uuid
    profile = Profile.find_by(user_id: id)
    return profile if profile.present?
    Profile.create(
      user_id: id,
      external_id: uid,
      uid: uid,
      organization_id: organization_id,
      bulk_import: self.bulk_import # assign user accessor to profile too
    )
  end

  def quarter_wise_content
    quarter_wise = []
    Quarter::VALID_QUARTERS.each do |q|
      if organization.custom_quarters_enabled?
        quarter = Assignment.custom_quarter_dates(self, q)
      else
        quarter = Assignment.quarter_dates(q)
      end
      assignment_query = "SELECT assignments.* FROM assignments assignments
                          INNER JOIN cards
                          ON (assignments.assignable_id = cards.id AND
                          assignments.assignable_type = 'Card' AND
                          cards.state = '#{Card::STATE_PUBLISHED}')
                          WHERE assignments.user_id = #{id} AND
                          assignments.state != 'dismissed' AND
                          cards.deleted_at IS NULL AND
                          cards.card_type IN (#{get_assignments_card_type.to_sql_in}) AND
                          assignments.created_at between ('#{quarter[:begin_date]}') AND
                          ('#{quarter[:end_date]}')
                          ORDER BY assignments.created_at desc LIMIT 10 OFFSET 0"

      assignments = Assignment.find_by_sql(assignment_query)

      quarter_wise << AssignmentBridge.new(assignments,
                                          user: self,
                                          fields: 'id, state, completed_at, due_at, start_date, started_at,
                                            card(id, completed_percentage, card_subtype,
                                                card_type, slug, badging, skill_level,
                                                title, message, ecl_duration_metadata),
                                            assignor, assignable').attributes
    end
    quarter_wise
  end

  def get_assignments_card_type
    mlp_default_value = self.organization.get_my_assignments_default_value
    if mlp_default_value == "notAllContentType"
      card_type = ["pack", "course", "journey"]
    else
      card_type = Card::EXPOSED_TYPES_V2
    end
  end

  def earned_bagdes
    user_badges.joins("inner join badgings b on b.id = user_badges.badging_id")
  end

  def user_stats
    data = {}
    # continous learning count
    clc = Clc.active.get_clc_for_entity({entity_id: organization_id, entity_type: "Organization"}).first
    learning_hrs = ""
    if clc.present?
      clc_progress = ClcProgress.get_progress_for(clc.id, id)

      if clc_progress.present?
        hour, min = clc_progress.clc_score.divmod(60)

        if hour > 0
          learning_hrs += "#{hour}h"
        end
        learning_hrs += " #{min}m" if min > 0
      else
        learning_hrs += "0h"
      end

      data['Annual Leaning Goal'] = "#{clc.target_score}h"
      data['Continous Learning Hrs'] = learning_hrs

    end

    # published content count
    data['Published Content'] = Card.where(organization_id: organization_id, author_id: id, state: 'published', hidden: false).count

    # completed content count excluding courses
    data['Completed Content'] = Card.joins("INNER JOIN user_content_completions on user_content_completions.completable_id = cards.id")
                                  .where("cards.organization_id = #{organization_id}
                                   AND hidden = false
                                   AND cards.card_type != 'course' AND user_content_completions.user_id = #{id}
                                   AND user_content_completions.state = 'completed' ")
                                  .count

    # completed courses count
    data['Courses Completed'] = Card.joins("INNER JOIN user_content_completions on user_content_completions.completable_id = cards.id")
                                    .where("cards.organization_id = #{organization_id}
                                     AND hidden = false
                                     AND user_content_completions.state = 'completed'
                                     AND cards.card_type = 'course' AND user_content_completions.user_id = #{id}")
                                    .count

    # badges count
    data['Badges Earned'] = earned_bagdes.count

    data
  end

  def request_personal_data
    UserMailer.send_user_data_export_request(id).deliver_later
  end

  def user_custom_fields_hash(fields_hash = {})
    return {} if fields_hash.blank?
    user_custom_fields = fields_hash[id]
    return {} unless user_custom_fields
    user_custom_fields.map { |cf| [cf.custom_field_id, cf.value] }
  end

  def user_related_deleted_cards
    Card.with_deleted.joins(<<-SQL.strip_heredoc)
      LEFT JOIN users_cards_managements ON
        users_cards_managements.card_id = cards.id
      LEFT JOIN learning_queue_items ON
        learning_queue_items.queueable_id = cards.id AND
        learning_queue_items.queueable_type = 'Card' AND
        learning_queue_items.state = 'deleted'
      LEFT JOIN votes ON
        votes.votable_id = cards.id AND
        votable_type = 'Card'
      WHERE (users_cards_managements.user_id = '#{id}' OR
        learning_queue_items.user_id = '#{id}' OR
        votes.user_id = '#{id}' OR
        cards.author_id = '#{id}') AND
        cards.deleted_at IS NOT NULL AND
        organization_id = '#{organization_id}'
    SQL
  end

  def card_ids_grouped_by_bookmark_ids(card_ids)
    bookmarks.where(bookmarkable_id: card_ids)
      .select(:id, :bookmarkable_id).group_by(&:bookmarkable_id)
      .transform_values { |value| value.first&.id }
  end

  def allow_sign_in!
    self.sign_out_at.present? && self.update_column(:sign_out_at, nil)
  end

  private

    # Creates user with random password
    # For:
    #   a. Google/Facebook/LinkedIn
    #   b. OAuth
    #   c. SAML
    #   d. Okta SSOs including Social Providers + SAML + SFDC
    # Note:
    #   Federated Identifier must be present only if user is provisioned from Okta and Email/Password flow.
    #   In rest of the cases, it should be `nil`
    #   `nil` will help to identify users present on Okta.

    def self.create_user_with_password(auth:, role: 'member')
      provider_type = auth['provider_name']
      org = Organization.where(id: auth[:organization_id]).first
      _user = User.new(
        password:          User.random_password,
        is_complete:       false,
        email:             auth[:info][:email],
        organization_id:   auth[:organization_id],
        organization_role: role,
        is_active:         org.is_active?)

      if auth['provider'] == 'lxp_oauth' && !Settings.okta.provision.split(',').include?(provider_type)
        _user.federated_identifier = auth[:info]['preferred_username']
      end

      # Skips confirmation email for SSO
      _user.skip_confirmation!
      logger.error("Errors while saving user: #{_user.attributes} with #{_user.full_errors}") unless _user.valid?
      _user.save
      _user
    end

    def self.locate_identity_provider(auth:, klass:)
      identities = klass.where(uid: auth[:uid]).all
      identity   = identities.detect do |ip|
        ip.user.organization_id == auth[:organization_id]
      end

      [identities, identity]
    end

    def update_interests
      unless user_interests.blank?
        interests_users.destroy_all
        user_interests.each do |interest|
          tag = Tag.find_or_create_by(name: interest)
          tag.update_attributes(type: 'Interest')
          self.interests_users.create(tag: tag)
        end
      end
    end

    def update_websites
      unless websites.blank?
        websites.each do |website|
          UsersWebsite.create(user: self, url: website)
        end
      end
    end

    def update_mentors
      #TODO: Fix Gets called twice in Onboarding workflow.
      unless mentor_emails.blank?
        mentor_emails.each do |email|
          UsersMentor.create(user: self, email: email)
        end
      end
    end

    def update_role
      self.add_role(organization_role)
    end

    def strip_all_tags_from_user_input
      # ActionController::Base.helpers.strip_tags converts "<p> Hello & Hello</p>" to "Hello &amp; Hello"
      # CGI::unescapeHTML converts "Hello &amp; Hello" to "Hello & Hello"
      self.first_name = CGI::unescapeHTML(ActionController::Base.helpers.strip_tags(self.first_name) || '')
      self.last_name = CGI::unescapeHTML(ActionController::Base.helpers.strip_tags(self.last_name) || '')
      self.bio = CGI::unescapeHTML(ActionController::Base.helpers.strip_tags(self.bio) || '')
    end

    def push_user_suspended_metric
      record_custom_event(
        event: 'user_suspended',
        job_args: [{suspended_user: :itself}],
      )
    end

    def push_user_unsuspended_metric
      record_custom_event(
        event: 'user_unsuspended',
        job_args: [{suspended_user: :itself}],
      )
    end

  protected
    def password_required?
      !persisted? || !password.nil? || !password_confirmation.nil?
    end

    def add_user_to_everyone_team
      # No groups in default org
      return if organization == Organization.default_org
      Users::AddUserToEveryoneGroupJob.perform_later(user_id: id)
    end

    # add whitelisted domains users to teams.
    def add_user_to_whitelisted_teams
      AddUserToWhitelistedTeamsJob.perform_later(user_id: self.id)
    end

    def cleanup_invitation
      _invitations = Invitation.where(recipient_email: self.email, invitable: self.organization)
      _invitations.each do |i|
        i.update_columns(accepted_at: Time.now, recipient_id: self.id)
        i.attach_parameters_to_invitee
      end
    end

    def set_exclude_from_leaderboard
      self.exclude_from_leaderboard ||= false
      true
    end

    def set_user_status
      self.status ||= ACTIVE_STATUS
    end

    def update_everyone_group_members
      # first time confirmation
      if self.previous_changes.has_key?('confirmed_at') && previous_changes['confirmed_at'][0].nil? && !previous_changes['confirmed_at'][1].blank?
        add_user_to_everyone_team
      end
    end

    def prepare_user_profile
      create_external_profile
      Users::AfterCommitOnCreateJob.perform_later(user_id: id)
    end
end

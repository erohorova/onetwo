class CacheFactory

  def self.fetch_credential_and_client(api_key:nil)
    key = "credential_and_client-api_key:#{api_key}"

    Rails.cache.fetch(key, expires_in: 1.hour, unless_nil: true) do
      credential = Credential.includes(:client).where(api_key: api_key).first
      # send nil so no caching
      credential.nil? ? nil : [credential, credential.client, credential.client.try(:organization)]
    end
  end

  def self.fetch_group(organization_id: nil, client_resource_id: nil)
    key = "group-organization_id:#{organization_id}-client_resource_id:#{client_resource_id}"

    Rails.cache.fetch(key, expires_in: 1.hour, unless_nil: true) do
      Group.where(organization_id: organization_id,
                  client_resource_id: client_resource_id).first
    end
  end

  def self.fetch_latest_native_app_release_version(platform: 'ios', form_factor: 'phone')
    Rails.cache.fetch("app-version-platform:#{platform}-form_factor:#{form_factor}", expires_in: 1.hour) do
      conditions = {platform: platform, form_factor: form_factor, state: 'active'}
      Release.where(conditions).last #sorted by primary key. this is perfect since we always increment the version/build when we create a new record
    end
  end

  def self.org_with_savannah_app_id(savannah_app_id: nil)
    key = "savannah-app-id-#{savannah_app_id}"
    Rails.cache.fetch(key, expires_in: 1.hour, unless_nil: true) do
      Organization.where(savannah_app_id: savannah_app_id).first
    end
  end

  def self.org_with_api_key(api_key: nil)
    key = "savannah-client-api-key-#{api_key}"
    Rails.cache.fetch(key, expires_in: 1.hour, unless_nil: true) do
      credential = Credential.includes(:client).where(api_key: api_key).first
      credential.client.try(:organization) if credential
    end
  end

  def self.clear
    Rails.cache.clear
  end
end

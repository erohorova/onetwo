# == Schema Information
#
# Table name: groups_users
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  group_id   :integer
#  as_type    :string(191)
#  created_at :datetime
#  updated_at :datetime
#  banned     :boolean          default(FALSE)
#  role_label :string(255)
#

class GroupsUser < ActiveRecord::Base
  include StreamWorthy

  belongs_to :group, counter_cache: :users_count
  belongs_to :user

  validates :group, :presence => true
  validates :user, :presence => true
  validates_inclusion_of :as_type, in: %w(member admin)

  scope :admin, -> { where(:as_type => 'admin') }
  scope :member, -> { where(:as_type => 'member') }
  scope :student, -> { where(:role_label => 'student') }
  scope :not_banned, -> { where(:banned => false) }

  add_to_stream group_id: ->(record){
                                       record.group.parent_id #resource group id
                                    },
                item: ->(record){record.group},
                action: 'joined',
                if: ->(record){record.is_a?(PrivateGroup) && record.is_open?}

  remove_from_stream(action: 'joined', item: ->(record){record.group})

  class << self
    def get_user_role_in_groups(user_id: nil, group_ids: [])
      ret = {}
      gus = GroupsUser.where(user_id: user_id, group_id: group_ids)
      gus.each do |gu|
        if gu.as_type == 'instructor' || gu.as_type == 'admin'
          ret[gu.group_id] = 'instructor'
        else
          ret[gu.group_id] = 'student'
        end
      end
      ret
    end

    def is_active_user_in_group?(user_id: nil, group_id: nil)
      where(banned: false, group_id: group_id, user_id: user_id).present?
    end

    def is_active_admin_in_group?(user_id: nil, group_id: nil)
      where(banned: false, user_id: user_id, as_type: 'admin', group_id: group_id).present?
    end

    def is_active_member_in_group?(user_id: nil, group_id: nil)
      where(banned: false, user_id: user_id, as_type: 'member', group_id: group_id).present?
    end
  end 

  def organization
    group.try(:organization)
  end 

end

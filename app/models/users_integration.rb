# == Schema Information
#
# Table name: users_integrations
#
#  id             :integer          not null, primary key
#  user_id        :integer
#  integration_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  connected      :boolean          default(TRUE)
#  field_values   :text(65535)
#

class UsersIntegration < ActiveRecord::Base
  belongs_to :user
  belongs_to :integration

  delegate :name, to: :integration
  validates :user_id, :integration_id, presence: true
  validates :user_id, uniqueness: {scope: :integration_id}

  scope :connected, -> { where(connected: true)}
end

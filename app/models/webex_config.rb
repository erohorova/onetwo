# == Schema Information
#
# Table name: webex_configs
#
#  id              :integer          not null, primary key
#  name            :string(255)
#  site_name       :string(255)
#  site_id         :string(255)
#  partner_id      :string(255)
#  client_id       :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  organization_id :integer          not null
#

class WebexConfig < ActiveRecord::Base
  belongs_to :client
  belongs_to :organization
  validates :name, :site_name, :site_id, :partner_id, :organization_id, presence: true  
end

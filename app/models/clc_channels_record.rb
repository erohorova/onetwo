# == Schema Information
#
# Table name: clc_channels_records
#
#  created_at :datetime
#  user_id    :integer
#  score      :integer
#  card_id    :integer
#  channel_id :integer
#

# We are using this table to save raw data for score per user per card for channel
# score is in minute
class ClcChannelsRecord < ActiveRecord::Base
  belongs_to :user
  belongs_to :card
  belongs_to :channel

  validates :user_id, :card_id, :channel_id, presence: true
  validates :user_id, uniqueness: {scope: [:card_id, :channel_id]}
end

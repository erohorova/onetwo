class Price < ActiveRecord::Base
  include SyncCloneable

  belongs_to :card

  validates :amount, :currency, presence: true
  validates :amount, numericality: {greater_than: 0, less_than: 999999.99}
  validates_presence_of :card
  validates_uniqueness_of :card, scope: :currency

  before_save :round_off_amount
  after_destroy :sync_clone_card_association

  def round_off_amount
    self.amount = self.amount&.round
  end

  def skillcoin?
    currency == Settings.payment_currencies['edu_currency']
  end

end

# frozen_string_literal: true

class EmailTemplate < ActiveRecord::Base

  include EventTracker

  configure_metric_recorders(
    default_actor: -> (record) { },
    trackers: {
      on_create: {
        event: 'org_email_template_created'
      },
      on_edit: {
        event: 'org_email_template_edited',
      },
      on_delete: {
        event: 'org_email_template_deleted',
      }
    }
  )

  STANDARD_STATES = %w[draft published].freeze

  DEFAULT_TITLES = {
    notify: %w[
      start_scheduledstream
      new_smartbite_comment
      new_livestream_from_followed_user
      new_follower
      new_content_in_channel
      new_content_by_followed_user
      new_activity_commented_smartbite
      mention_in_comment
      completed_assignment
      assigned_content
      added_curator
      added_collaborator
      content_shared
    ],
    mailer: %w[
      edcast-bulk-import-admin
      edcast-confirmation-instruction
      edcast-reset-password-request
      admin-bulk-import-welcome
      follow-batch-email
      user-magic-link
      admin-group-announcements
      edcast-team-invite
    ]
  }.with_indifferent_access

  TEMPLATE_PARAMS = {
    notify: {
      start_scheduledstream: %w[
        user_name title join_url
      ],
      new_smartbite_comment: %w[
        content commenter.first_name commenter.last_name
        card.title card.title_simplified card.assignment_url card.image_url
      ],
      new_livestream_from_followed_user: %w[
        user_name receiver_name title title_simplified join_url
      ],
      new_follower: %w[
        follower_name image_url follower_link
      ],
      new_content_in_channel: %w[
        channel.name channel.channel_url card.url card.image_url
        card.author_image card.author_name card.title card.title_simplified team_name
      ],
      new_content_by_followed_user: %w[
        card.title card.title_simplified card.url card.image_url card.author_name
        card.author_image card.content_type
      ],
      new_activity_commented_smartbite: %w[
        activity.type activity.user_name activity.content
        activity.url activity.title activity.title_simplified
      ],
      mention_in_comment: %w[
        commenter_name url message
      ],
      completed_assignment: %w[
        user_name assignment.title assignment.title_simplified assignment.assignment_url
        assignment.image_url assignee team_name
      ],
      assigned_content: %w[
        user_name assignment.title assignment.assignment_url assignment.title_simplified
        assignment.assignment_message assignment.image_url assignor team_name
      ],
      added_curator: %w[
        first_name invite_url channel_name host_name
      ],
      added_collaborator: %w[
        first_name invite_url channel_name host_name
      ],
      content_shared: %w[
        card.title card.url card.image_url card.shared_by_name
        card.shared_by_image card.content_type
      ],
      global: %w[
        user.first_name user.last_name user.email user.org_name
        org.logo org.home_page org.name org.promotions org.appstore_link
        org.playstore_link org.support_email
        custom_css.header_background_color custom_css.text_color
        custom_css.highlight_text_color custom_css.section_text_color
        custom_css.card_text_color custom_css.button_background_color
      ]
    },
    mailer: {
      "edcast-bulk-import-admin": %w[
        total_users_count new_users_count old_users_count
        resent_emails_count invitees_count failed_users_count
        channels_names_list new_groups_count new_groups
        old_groups upload_time first_name show_app_download org_logo
      ],
      "edcast-confirmation-instruction": %w[
        link_url org_logo show_app_download first_name
      ],
      "edcast-reset-password-request": %w[
        link_url org_logo first_name
      ],
      "admin-bulk-import-welcome": %w[
        org_logo first_name subject team_name org_login_link
      ],
      "follow-batch-email": %w[
        first_name unsubscribe_url email followers name org_name
        org_logo followers_count picture url
      ],
      "user-magic-link": %w[
        first_name magic_link org_name org_logo
      ],
      "admin-group-announcements": %w[
        post_group_name org_logo post_message post_user first_name
        show_app_download unsubscribe_url
      ],
      "edcast-team-invite": %w[
        host invite_url org_logo first_name recipient_email inviter_name
        invitable_name show_app_download invitable_photo show_assignment_msg
        sender_role
      ],
      global: %w[
        org_logo email first_name org_display_name appstore_link playstore_link
        support_email show_edcast_name show_edcast_logo promotions
        home_page header_background_color text_color section_text_color
        highlight_text_color button_blue_color button_background_color
      ]
    }
  }.with_indifferent_access

  READABLE_TITLES = {
    'start_scheduledstream': 'When a user has to start a scheduled stream',
    'new_smartbite_comment': 'Comment on a SmartCard created by a user',
    'new_livestream_from_followed_user': 'User Livestream Starts',
    'new_follower': 'User New Follower',
    'new_content_in_channel': 'Content added to a channel',
    'new_content_by_followed_user': 'Content created by a user you are following',
    'new_activity_commented_smartbite': 'Comment added after your comment to a card',
    'mention_in_comment': 'Someone mentioned you in a comment',
    'completed_assignment': 'An assigned card is completed by a user',
    'assigned_content': 'Someone assigned a card to you',
    'added_curator': 'User added as a curator to a channel',
    'added_collaborator': 'User added as a collaborator to a channel',
    'edcast-bulk-import-admin': 'User import Summary - Admin',
    'edcast-reset-password-request': 'Request to Reset Password',
    'admin-bulk-import-welcome': 'Invite Email to join the platform',
    'follow-batch-email': 'New Follower to a User',
    'user-magic-link': 'Magic Link to sign in to an instance',
    'admin-group-announcements': 'Notification to a Group',
    'edcast-confirmation-instruction': 'Request to Confirm Account',
    'content_shared': 'When a card is shared with a group/user',
    'edcast-team-invite': 'When a user invited to a group'
  }.with_indifferent_access

  DEFAULT_SUBJECTS = {
    'edcast-bulk-import-admin': 'Bulk Import User Details',
    'edcast-confirmation-instruction': '[EdCast] Welcome - just one more step!',
    'edcast-reset-password-request': '[EdCast] Reset your password',
    'admin-bulk-import-welcome': 'Welcome to {{team_name}} on EdCast!',
    'follow-batch-email': '[EdCast] Congratulations! You have new followers',
    'user-magic-link': '[Edcast] Magic Link',
    'admin-group-announcements': 'Group Announcement from {{post_user}}',
    'start_scheduledstream': '{{user_name}} time to start your scheduled stream',
    'new_smartbite_comment': '{{commenter.first_name}} {{commenter.last_name}} commented on your SmartCard {{card.title_simplified}}',
    'new_livestream_from_followed_user': '{{user_name}} is starting a Livestream now.',
    'new_follower': '{{follower_name}} has followed you.',
    'new_content_in_channel': 'New card on {{channel.name}}',
    'new_content_by_followed_user': '{{card.author_name}} posted a new {{card.content_type}}: {{card.title_simplified}}',
    'new_activity_commented_smartbite': '{{activity.user_name}} commented on a SmartCard after you',
    'mention_in_comment': '{{commenter_name}} mentioned you in a comment',
    'completed_assignment': '{{assignee}} completed the assignment {{assignment.title}}',
    'assigned_content': '{{assignor}} has assigned you: {{assignment.title_simplified}}',
    'added_curator': 'You have been added as a curator to {{channel_name}} Channel',
    'added_collaborator': 'You have been added as a collaborator to {{channel_name}} Channel'
  }.with_indifferent_access

  belongs_to :organization
  belongs_to :creator, class_name: 'User', foreign_key: :creator_id
  belongs_to :default, class_name: 'EmailTemplate'

  has_many :custom_templates, class_name: 'EmailTemplate', foreign_key: :default_id

  scope :activated, -> { where.not(is_active: false, default_id: nil) }
  scope :customs, -> { where.not(default_id: nil) }

  validates :title, uniqueness: { scope: :organization_id, case_sensitive: false }
  validates :organization, :title, :language, :design, presence: true
  validates :state, inclusion: { in: STANDARD_STATES }
  validates :creator, :content, presence: true, if: :default_id

  def default?
    !default_id
  end

  def active?
    is_active
  end

  def activate_template
    # for each default_id in organization could be active only one template in scope of language
    customs = organization.email_templates.where.not(id: id).where(
      default_id: default_id, is_active: true, language: language
    )
    # deactivate these templates if they present
    if customs.any?
      customs.each do |template|
        template.update_attributes(is_active: false)
      end
    end
    { disabled: customs, is_activated: update_attributes(is_active: true) }
  end

  def disable_template
    update_attributes(is_active: false, state: 'draft')
  end

  def active_pair
    EmailTemplate.find_by(
      default_id: default_id,
      language: language,
      organization_id: organization_id,
      is_active: true
    )
  end

  # will be used to extract actual dynamic variables keys for each template
  class << self

    def get_dynamic_variables(template_id)
      template = find_by(id: template_id)
      return unless template
      title = get_proper_title(template)

      type = get_email_type(title)
      template_vars = get_template_params(title, type)

      global_vars = type.nil? ? [] : EmailTemplate::TEMPLATE_PARAMS[type][:global]

      {
        template: template_vars,
        global: global_vars
      }
    end

    # convert hash to dot notation:
    # e.g =>
    #   {user: {first_name: 'name'}, org_name: 'name'}
    # output:
    #   {'user.first_name': 'name', org_name: 'name'}
    def convert_to_dot(hash, path = '')
      hash.each_with_object({}) do |(key, val), obj|
        key_n = path + key.to_s
        obj.merge!(convert_to_dot(val, key_n + '.')) if val.is_a?(Hash)
        obj[key_n] = val unless val.is_a?(Hash)
      end
    end

    # get template specific params keys - based on existing NotificationEntry
    def get_template_params(template_title, type)
      return [] unless type
      if type == 'mailer'
        return EmailTemplate::TEMPLATE_PARAMS[:mailer][template_title]
      end
      instance = "Notify::#{template_title.camelize}Notifier".constantize
      ne = NotificationEntry.where.not(sourceable: nil).find_by(
        notification_name: template_title
      )
      convert_to_dot(
        instance.new.send(
          :notification_params, ne
        )
      ).keys
    rescue NameError, NoMethodError => e
      log.warn(
        "Extract params error. Title: #{template_title}. Error: #{e.inspect}"
      )
      EmailTemplate::TEMPLATE_PARAMS[:notify][template_title]
    end

    def get_email_type(title)
      return 'mailer' if title.in?(EmailTemplate::DEFAULT_TITLES[:mailer])
      'notify' if title.in?(EmailTemplate::DEFAULT_TITLES[:notify])
    end

    def get_proper_title(template)
      return template.title if template.default_id.nil?
      find_by(id: template.default_id).title
    end

    def get_readable_title(template)
      return template.title unless template.default_id.nil?
      EmailTemplate::READABLE_TITLES[template.title].presence || template.title
    end

    # Will return custom subject only if:
      # template is not the default template
      # custom subject is present for the template
    def get_email_subject(template)
      return template.subject if template.default_id.present? && template.subject.present?
      template_title = get_proper_title(template)
      EmailTemplate::DEFAULT_SUBJECTS[template_title].presence
    end

    def extract_variables(template_content:)
      parsed_content = Nokogiri::HTML::DocumentFragment.parse(template_content)
      extracted_variables = []
      # extract dynamic variables from text nodes
      # since variables will be only present in text nodes for custom templates
      parsed_content.traverse do |node|
        href_content = node.attribute('href')&.value
        next unless node.text? || href_content
        # extract text in double curly brackets from text node or href attrs
        match = node.content.scan(/{{(.*?)}}/)

        match = href_content.scan(/{{(.*?)}}/) if match.empty? && href_content

        extracted_variables.push(*match.flatten) unless match.empty?
      end
      extracted_variables
    end
  end
end

class FollowBatchEmailGenerator

  def self.perform
    now = Time.now.utc
    batch_job = BatchJob.find_by(name: self)

    # pick existing OR create new one
    if batch_job
      last_run_time = batch_job.last_run_time
      batch_job.update_attributes!(last_run_time: now)
    else
      BatchJob.create(last_run_time: now, name: self)
      last_run_time = now - 1.day
    end

    follows = Follow.where(followable_type: 'User', skip_notification: false).where("created_at >= (:last_run_time) AND created_at < (:now)", last_run_time: last_run_time, now: now).group_by(&:followable_id)

    follows.each do |followable_id, follows|
      FollowEmailNotificationGeneratorJob.perform_later(followable_id, follows.map(&:user_id))
    end
  end
end

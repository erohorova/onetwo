# == Schema Information
#
# Table name: tags
#
#  id         :integer          not null, primary key
#  name       :string(191)
#  created_at :datetime
#  updated_at :datetime
#  type       :string(191)
#  mapped     :boolean          default(FALSE)
#

require 'nokogiri'
class Tag < ActiveRecord::Base
  has_many :taggings, :dependent => :delete_all
  validates :name, :uniqueness => true, :presence => true

  scope :unmapped, -> { where(mapped: false)}
  # TODO: delayed till we come up with a better normalization strategy
  # before_create :downcase_name

  def downcase_name
    self.name.downcase!
  end
  class << self
    def extract_tags(text, mode=:hashtag)
      html = Nokogiri::HTML(text)
      if html.errors.empty?
        plain_text = html.inner_text
      else
        plain_text = text
      end

      scanner = case mode
                  when :hashtag
                    ->(s){ s.scan(/\B#(\w+)/).map{|match| match[0]} }
                  when :pipe_delimited
                    ->(s){ s.split('|').map(&:strip).reject(&:empty?) }
                  else
                    log.error "bad tag extraction mode #{mode}"
                    return []
                end

      tags = scanner.call plain_text
      tag_objects = find_tags(tags)
      tag_objects
    end

    def find_tags(tag_names)
      tag_objects = []
      if tag_names.present?
        tag_names.uniq.each do |tag|
          existing_tag = Tag.find_by_name(tag)
          if existing_tag.nil?
            tag_objects << Tag.create(:name => tag)
          else
            tag_objects << existing_tag
          end
        end
      end
      tag_objects
    end

    def extract_from_comma_separated_values(tags)
      find_tags(tags.split(/,\s*/))
    end

    def extract_from_array(tags)
      find_tags(tags)
    end
  end



end

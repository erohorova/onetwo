# == Schema Information
#
# Table name: dismissed_contents
#
#  id           :integer          not null, primary key
#  content_id   :integer
#  content_type :string(191)
#  user_id      :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class DismissedContent < ActiveRecord::Base
  include EventTracker

  belongs_to :content, polymorphic: true
  belongs_to :user
  validates_presence_of :content
  validates_uniqueness_of :content_id, scope: [:user_id, :content_type]

  scope :cards, -> {where(content_type: 'Card')}

  after_commit :remove_item_from_queue

  configure_metric_recorders(
    default_actor: :user,
    trackers: {
      on_create: {
        event: 'card_dismissed',
        job_args: [{card: :content}],
        if_block: lambda {|record| record.content.is_a?(Card)}
      }
    }
  )

  def remove_item_from_queue
    UserContentsQueue.remove_from_queue(self.user, self.content)
  end
end

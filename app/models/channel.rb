# == Schema Information
#
# Table name: channels
#
#  id                        :integer          not null, primary key
#  label                     :string(255)
#  description               :string(2000)
#  card_query_id             :integer
#  created_at                :datetime
#  updated_at                :datetime
#  image_file_name           :string(255)
#  image_content_type        :string(255)
#  image_file_size           :integer
#  image_updated_at          :datetime
#  slug                      :string(191)
#  visible                   :boolean          default(TRUE), not null
#  mobile_image_file_name    :string(255)
#  mobile_image_content_type :string(255)
#  mobile_image_file_size    :integer
#  mobile_image_updated_at   :datetime
#  is_private                :boolean          default(FALSE)
#  client_id                 :integer
#  keywords                  :string(1024)
#  autopull_content          :boolean          default(FALSE)
#  show_authors              :boolean          default(FALSE)
#  visible_during_onboarding :boolean          default(FALSE)
#  only_authors_can_post     :boolean          default(TRUE)
#  group_id                  :integer
#  organization_id           :integer
#  content_discovery_freq    :string(255)
#  last_discovered_at        :datetime
#  is_general                :boolean          default(FALSE)
#  is_promoted               :boolean          default(FALSE)
#  auto_follow               :boolean          default(FALSE)
#  user_id                   :integer
#  is_private_posting        :boolean          default(FALSE)
#  topics                    :text(65535)
#  ecl_enabled               :boolean          default(FALSE)
#  curate_only               :boolean          default(FALSE)
#  provider                  :string(255)
#  provider_image            :string(255)
#  is_open                   :boolean          default(FALSE)
#  curate_ugc                :boolean          default(FALSE)
#

class Channel < ActiveRecord::Base

  has_paper_trail ignore: [:created_at, :updated_at]

  FREQUENCIES = %w(hourly half_day daily)
  include EdcastSearchkickWrapper
  include PubnubChannels
  include EventTracker
  include CustomCarousel
  include ChannelModelLeaderboardConcern
  extend FriendlyId

  serialize :topics, Array
  serialize :language, Array
  serialize :content_type, Array

  belongs_to :organization
  belongs_to :client
  belongs_to :user
  has_many :clients_users, through: :client

  has_many :follows, as: :followable, dependent: :destroy, class_name: 'Follow'
  has_many :followers, through: :follows, source: :user
  has_many :channels_cards
  has_many :cards, through: :channels_cards
  has_many :curated_channels_cards, -> {where(channels_cards: {state: ChannelsCard::STATE_CURATED})}, class_name: 'ChannelsCard'

  has_many :channels_groups, dependent: :destroy
  has_many :groups, through: :channels_groups

  has_many :channels_users, dependent: :destroy, foreign_key: 'channel_id'
  has_many :users, through: :channels_users

  has_many :channels_authors, -> { author }, class_name: 'ChannelsUser', foreign_key: 'channel_id', dependent: :destroy
  has_many :authors, -> {not_suspended}, :class_name => 'User', through: :channels_authors, source: :user

  has_many :trusted_channels_authors, -> { trusted_author }, class_name: 'ChannelsUser', foreign_key: 'channel_id', dependent: :destroy
  has_many :trusted_authors, -> {not_suspended}, :class_name => 'User', through: :trusted_channels_authors, source: :user

  has_many :channels_curators, dependent: :destroy
  has_many :curators, -> {not_suspended}, class_name: 'User', through: :channels_curators, source: :user

  has_many :posts, as: :postable, :dependent => :destroy

  has_many :interests_channels, as: :taggable, class_name: 'Tagging', :dependent => :destroy
  has_many :interests, through: :interests_channels, class_name: 'Interest', foreign_key: 'tag_id', source: 'tag'

  # video streams are mapped to cards. No need to update this association
  has_many :channels_video_streams
  #TODO:CLEANUP: We already have a method called video_streams for channels. forced to use a different association name
  has_many :all_video_streams, through: :channels_video_streams, source: :video_stream

  has_one :pubnub_channel, dependent: :destroy, as: :item
  has_one :public_pubnub_channel, -> { public_channel }, class_name: 'PubnubChannel', as: :item
  has_one :config, as: :configable
  belongs_to :parent_channel, class_name: 'Channel', foreign_key: :parent_id

  belongs_to :creator, foreign_key: :user_id, class_name: 'User'

  has_many :ecl_sources, inverse_of: :channel, dependent: :destroy

  has_many :pins, as: :pinnable, dependent: :destroy
  has_many :clcs, as: :entity, dependent: :destroy
  has_many :teams_channels_follows, dependent: :destroy
  has_many :followed_teams, through: :teams_channels_follows, class_name: 'Team', source: :team

  has_many :widgets, as: :parent, dependent: :destroy

  validates :label, presence: true
  validates :organization, presence: true
  validate :eligible_for_open_channel
  validates_uniqueness_of :label, scope: :organization_id
  validates_uniqueness_of :parent_id, :allow_nil => true, scope: :organization_id
  friendly_id :slug_candidates, use: [:slugged, :history, :scoped], scope: :organization

  accepts_nested_attributes_for :ecl_sources, allow_destroy: true

  has_attached_file :image, default_url: 'default_banner_image_min.jpg', :styles => { small: '120x120',medium: '320x320' }
  validates_attachment_content_type :image, :content_type => /^image\/(png|jpeg|jpg|pjpeg|x-png|gif|tiff)$/,
                                    :message => 'Invalid file type'

  has_attached_file :mobile_image, default_url: 'default_banner_image_mobile.jpg', :styles => { small: '120x120',medium: '320x320' }
  validates_attachment_content_type :mobile_image, :content_type => /^image\/(png|jpeg|jpg|pjpeg|x-png|gif|tiff)$/,
                                    :message => 'Invalid file type'

  scope :not_private, ->{where.not(is_private: true)}
  scope :not_general, ->{where(is_general: false)}
  scope :as_open, -> {where(is_open: true)}
  scope :visible_during_onboarding, ->{where(visible_during_onboarding: true)}
  scope :followers_allowed_to_post, ->{where.not(only_authors_can_post: true)}
  scope :search_by, -> (query) { where("label like :q OR description like :q", q: "%#{query}%") }
  scope :shared, -> { where(shareable: true)}
  scope :cloned, -> { where("parent_id is not null") }
  scope :auto_pin_cards_enabled, -> { where(auto_pin_cards: true)}
  scope :auto_followed, -> { where(auto_follow: true) }

  after_commit :reindex_async_urgent

  after_commit :add_followers_to_channel, if: :auto_follow?
  after_commit :fetch_ecl_default_source_content, if: :ecl_enabled?
  after_commit :add_creator_as_curator, on: :create
  after_commit :clone_parent_channel_cards, on: :create, if: proc {|attrs| attrs[:parent_id].present? }
  after_commit :sync_channel_cards, on: :update
  around_update :record_channel_promotion_event
  before_destroy :unlink_cloned_channels, if: :should_unlink?
  before_destroy :remove_channel_data

  configure_metric_recorders(
    default_actor: :user,
    trackers: {
      on_create: {
        event: Analytics::ChannelMetricsRecorder::EVENT_CHANNEL_CREATED,
        exclude_job_args: [:org]
      },
      on_edit: {
        event: Analytics::ChannelMetricsRecorder::EVENT_CHANNEL_EDITED,
        exclude_job_args: [:org]
      },
      on_delete: {
        event: Analytics::ChannelMetricsRecorder::EVENT_CHANNEL_DELETED,
        exclude_job_args: [:org]
      }
    }
  )

  def self.get_channel_info_for_event(channel)
    channel = OpenStruct.new channel
    {
      channel_label: channel.label,
      is_private: channel.is_private,
      ecl_enabled: channel.ecl_enabled,
      curate_only: channel.curate_only,
      organization_id: channel.organization_id
    }
  end

  # Description:
  # This method will override association curators behaviour
  #
  # For open channel.curators -> #<ActiveRecord::Relation []>
  #
  # For open channel.curators << User.first <-- this will skip adding curator
  #
  def curators
    is_open? ? User.none : super
  end

  def cloned_channels
    Channel.where(parent_id: id)
  end

  def should_unlink?
    shareable && cloned_channels.present?
  end

  # we have 2 types of channel followers
  # 1. users explicitly following -> Follow table
  # 2. users members of teams(groups) following a particular channel.
  # #all_followers method gets followers from both these places depending upon params type sent to it.
  # if type is nil both (1) and (2) will be returned.
  # if type is 'individual', (1) will be returned.
  def all_followers(type: nil)
    case type
    when 'individual'
      followers
    else
      user_ids = follows.pluck(:user_id)
      followed_teams.each do |team|
        user_ids = user_ids | team.users.pluck(:id)
      end
      User.visible_members.where(id: user_ids, organization_id: self.organization_id)
    end
  end

  def add_followers_to_channel
    AddFollowersToChannelJob.perform_later(self) if previous_changes['auto_follow'] == [false, true]
  end

  def fetch_ecl_default_source_content
    #Enqueue only if channel_programming_v2 = false and ecl_enabled = true
    ChannelEclSourcesFetchJob.perform_later(self.id, { only_default_sources: true }) if (previous_changes['ecl_enabled'] == [false, true] and !organization.get_settings_for('channel_programming_v2'))
  end

  # channel creator should auto add as curator channel
  def add_creator_as_curator
    add_curators([creator]) if creator.present?
  end

  def clone_parent_channel_cards
    if self.parent_channel.shareable?
      CloneChannelCardsJob.perform_later(self.parent_id, self.id)
    end
  end

  def sync_channel_cards
    # When parent channel becomes unshareable delete child channels, and child channel cards
    if self.previous_changes["shareable"] && !self.shareable?
      UnsyncCloneChannelJob.perform_later(
        {
          parent_channel_id: self.id,
          action: 'remove'
        }
      )
    end
  end

  def eligible_for_open_channel
    errors.add(:base, "only public channel can be made open") if is_open? && is_private?
  end

  def set_carousels_config(carousels)
    carousels_config = Config.find_or_initialize_by(configable: self, name: 'channel_carousels', category: 'settings', data_type: 'json')
    carousels_config.update_attributes(value: carousels)
    carousels_config
  end

  def program(opts)
    ActiveRecord::Base.transaction do
      #Add ecl source
      ecl_sources = opts[:ecl_sources] || []
      ecl_sources.each do |source|
        ecl_source = EclSource.find_or_create_by(ecl_source_id: source[:source_id],
                                                 channel: self)

        ecl_source.private_content = source[:private_content]
        ecl_source.save
      end

      #Update language
      self.language = opts[:language]

      #Update content_type
      self.content_type = opts[:content_type]

      #Update topic
      if opts[:topics]
        topic = self.topics + opts[:topics]
        self.topics = topic.uniq
      end

      #Set content fetch limit
      if opts[:card_fetch_limit]
        self.card_fetch_limit = opts[:card_fetch_limit]
      end

      #Save Channel
      self.save

      #Sociative call for immediate fetch
      ChannelProgrammingJob.perform_later(self.id)
    end
  end


  def self.sortable_fields
    ['label']
  end

  def self.exact_match_fields
    ['topics.name']
  end

  def self.searchable_fields
    {
      'label' => {
        boost: 10,
      },
      'description' => {
        boost: 1
      },
      'topics.name' => {
        boost: 1,
      }
    }
  end

  edcast_searchkick

  def search_data

    addition_data = {}

    addition_data['curators'] = curators.map do |curator|
      { id: curator.id, name: curator.name }
    end

    addition_data['collaborators'] = authors.map do |collaborator|
      { id: collaborator.id, name: collaborator.name }
    end

    addition_data['followers'] = followers.not_suspended.map do |follower|
      { id: follower.id, name: follower.name }
    end

    addition_data['followed_team_ids'] = followed_teams.pluck(:id)

    as_json.merge(addition_data)
  end

  def reindex_async
    IndexJob.perform_later(self.class.name, self.id)
  end

  def reindex_async_urgent
    IndexJob.set(queue: :reindex_urgent).perform_later(self.class.name, id)
  end
  # ES ends

  def title
    label
  end

  def send_collaborator_invites tos: , from:
    if organization.notifications_and_triggers_enabled?
      tos.each {|a| EDCAST_NOTIFY.trigger_event(Notify::EVENT_ADD_COLLABORATOR, ChannelsUser.where(channel_id: id, user_id: a.id).first)}
    else
      authors.where(id: tos.map(&:id)).each {|a| UserMailer.invite_collaborator(from, a, self).deliver_later}
    end
  end

  def send_follower_invites tos: , from:
    if organization.notifications_and_triggers_enabled?
      tos.each {|a| EDCAST_NOTIFY.trigger_event(Notify::EVENT_ADD_CHANNEL_FOLLOWER, Follow.where(followable_id: id, user_id: a.id, followable_type: 'Channel').first)}
    else
      tos.each {|a| UserMailer.invite_follower(from, a, self).deliver_later }
    end
  end

  def send_curator_invites tos: , from:
    if organization.notifications_and_triggers_enabled?
      tos.each {|a| EDCAST_NOTIFY.trigger_event(Notify::EVENT_ADD_CURATOR, ChannelsCurator.where(channel_id: id, user_id: a.id).first)}
    else
      tos.each {|a| UserMailer.invite_curator(from, a, self).deliver_later}
    end
  end

  def generate_invite_notifications tos:, from:, type:
    NotificationGeneratorJob.perform_later(item_id: id, item_type: type, users_ids: tos.map(&:id), inviter: from.id)
  end

  def ugc_curation_enabled?
    curate_only && (curate_ugc? || organization.get_settings_for("channel/curation_for_followers_card"))
  end

  def in_org(user)
    user.organization_id == organization_id
  end

  def channel_url
    Rails.application.routes.url_helpers.learn_url(id: slug, host: organization.host)
  end

  def curated_smartbites
    cards.curated_cards_for_channel_id(id).smartbites.published.visible_cards
  end

  def curated_published_pathways
    cards.curated_cards_for_channel_id(id).published_pathways
  end

  def curated_published_journeys
    cards.curated_cards_for_channel_id(id).published_journeys
  end

  def curated_streams
    cards.curated_cards_for_channel_id(id).video_streams.published
  end

  def curated_courses
    cards.curated_cards_for_channel_id(id).courses.published
  end

  def add_authors(users, trusted = false)
    added = []
    users.each do |author|
      is_author = trusted ? is_trusted_author?(author) : is_author?(author)
      next if is_author || !in_org(author)
      begin
        author.can_post_to_channel = true
        trusted ? trusted_authors << author : authors << author
        added << author
      rescue ActiveRecord::RecordInvalid
        next
      end
    end
    add_followers users
    added
  end

  def add_followers(users, as_individual=false)
    added = []
    users.each do |follower|
      unless is_follower?(follower, as_individual) || !in_org(follower)
        begin
          followers << follower
          added << follower
        rescue ActiveRecord::RecordInvalid
          next
        end
      end
    end
    added
  end

  def add_curators(users)
    added = []
    users.each do |user|
      unless is_curator?(user) || !in_org(user)
        begin
          curators << user
          added << user
        rescue ActiveRecord::RecordInvalid
          next
        end
      end
    end
    add_authors users
    added
  end

  def add_collaborators(from:, users:, as_trusted: false)
    added = add_authors(users, as_trusted)
    # EP-16201 : Do not send notifications for trusted collaborators, before all requirements will be approved
    send_collaborator_invites tos: added, from: from unless as_trusted
    type_role = as_trusted ? Role::TYPE_TRUSTED_COLLABORATOR : Role::TYPE_COLLABORATOR
    users.each { |user| user.add_role(type_role) }
  end

  def version_logs
    PaperTrail::Version.select("DISTINCT(versions.id), versions.*")
                .joins("LEFT OUTER JOIN version_associations ON version_associations.version_id = versions.id")
                .where("(item_type=? and item_id=?) OR (foreign_key_id = ?)", 'Channel',self.id,self.id)
  end

  def remove_curators(users)
    users.each {|user| curators.destroy(user) }
  end

  def remove_authors users
    users.each {|user| authors.destroy(user)}
  end

  def remove_trusted_authors(users)
    users.each { |user| trusted_authors.destroy(user) }
  end

  def is_author?(user)
    channels_authors.exists?(user_id: user.id)
  end

  def is_trusted_author?(user)
    trusted_channels_authors.exists?(user_id: user.id)
  end

  def user_is_curator?(user)
    channels_curators.exists?(user_id: user.id)
  end

  # EP-7436:
  # Any org user should be curator for open channel
  # Definition of open channel:
  # Only public channel can be made open
  # In open channel all the followers who follow that channel are able to curate content
  # Open channel will skip check for permission:curate_content role
  # Open channel feature is added in order to expand curation scope from certain users to all followers.
  def is_curator?(user)
    is_curator = channels_curators.exists?(user_id: user.id)
    (is_open? && is_follower?(user)) || is_curator
  end

  # including following teams_users as followers
  def is_follower?(user, as_individual=false)
    is_following = follows.exists?(user_id: user.id)
    unless is_following || as_individual
      user_team_ids = user.teams_users.pluck(:team_id)
      common_teams = user_team_ids & followed_teams.pluck(:team_id)
      is_following = common_teams.present?
    end
    is_following
  end

  def photo(size = nil)
    if size
      url = image.url(size)
    else
      url = image.url
    end

    return if url.blank?

    uri = URI.parse(url)
    uri.scheme = Settings.serve_images_on_ssl ? 'https' : 'http'
    uri.to_s
  end

  def fetch_bannerimage_urls
    urls = {}
    image.styles.keys.each do |style|
      urls[style.to_s + "_url"] = photo(style)
    end
    urls
  end

  def fetch_profile_image_urls
    urls = {}
    mobile_image.styles.keys.each do |style|
      urls[style.to_s + "_url"] = mobile_photo(style)
    end
    urls
  end

  def mobile_photo(size = nil)
    url = size ? mobile_image.url(size) : mobile_image.url
    uri = URI.parse(url)
    uri.scheme = 'https' unless Rails.env.development?
    uri.to_s
  end

  # add tags and mentions to slug if necessary; id in desperation
  def slug_candidates
    if label.present?
      l = label
      cand = []

      # avoid validation error for reserved words
      # latest friendly_id will do this; need to update our fork
      cand << l unless (self.friendly_id_config.reserved_words.include?(l) rescue true)

      # add channel id for duplications & reserved words
      if self.persisted?
        cand << "#{l}-#{id}"
      else
        # guess the new channel id; sometimes wrong, but just need something unique for this rare case
        cand << l = "#{l}-#{Channel.maximum(:id).to_i.next}"
      end
      return cand
    end
  end

  def after_followed(follower, restore: nil)
    PubnubNotifyChangeJob.perform_later('subscribe',follower,self.public_pubnub_channel_name)
    IndexJob.perform_later(follower.class.name, follower.id)
    OnFollowEnqueueJob.perform_later(
      id: self.id,
      type: self.class.name,
      follower_id: follower.id
    )
  end

  def after_unfollowed(follower)
    PubnubNotifyChangeJob.perform_later('unsubscribe',follower,self.public_pubnub_channel_name)
    IndexJob.perform_later(follower.class.name, follower.id)
    OnUnfollowEnqueueJob.perform_later(
        id: self.id,
        type: self.class.name,
        follower_id: follower.id
    )
  end

  def follows_via_team?(user)
    user_team_ids = user.teams_users.pluck(:team_id)
    common_teams = user_team_ids & followed_teams.pluck(:team_id)
    common_teams.present?
  end

  def video_streams
    streams = Channel.video_stream_feed([self])
    live_stream = streams.select{|vs| vs.status == 'live'}.last
    past_streams = streams.select{|vs| vs.past_stream_available?}.reverse
    scheduled_streams = streams.select{|vs| vs.status == 'upcoming' && vs.start_time >= (Time.now.utc - 1.hour)}.sort_by(&:start_time).first(6)
    return {'live' => live_stream, 'past' => past_streams, 'scheduled' => scheduled_streams}
  end

  def videos_count
    Channel.video_stream_feed([self]).count
  end

  def self.video_stream_feed(channels, exclude_user=nil)
    feed = VideoStream.published.joins(card: :channels).where("channels.id in (:channel_ids)", channel_ids: channels.map(&:id))
    if exclude_user.present?
      return feed.where.not(creator: exclude_user)
    else
      return feed
    end
  end

  def followed_by_users
    self.all_followers
  end

  def should_generate_new_friendly_id?
    super || label_changed?
  end

## BUNCH OF AUTHORIZERS.TODO: Consolidate!

  # Can the user access the channel page (channels/architecture) and contents in it?
  # nil user input assumes no user, or in other words, is it world readable?
  def has_read_access? user
    org_access = organization.open? || (user.try(:organization) == organization)
    channel_access = !is_private || (user && (is_follower?(user) || is_author?(user) || user.is_admin_user? || user.is_admin?))
    channel_access && org_access
  end

  # Interface for postable
  def visible_without_auth?
    has_read_access?(nil)
  end

  # any follower or author of the channel can post to the forum.
  # Insight authoring unfortunately does not have the same policy
  def can_write_to_forum?(user)
    is_follower?(user) || is_author?(user)
  end

  # Same policy as has_read_access?
  def can_read_forum?(user)
    has_read_access?(user)
  end

  def is_active_super_user?(user)
    is_author?(user)
  end

  #provide default carousels fro channel if custom_carousels for channel is not resent in config
  def get_default_carousels
    return [{"custom_carousel":false,"default_label":"SmartCards","index":0,"visible":true},
      {"custom_carousel":false,"default_label":"Pathways","index":1,"visible":true},
      {"custom_carousel":false,"default_label":"Journeys","index":2,"visible":true},
      {"custom_carousel":false,"default_label":"Streams","index":3,"visible":true},
      {"custom_carousel":false,"default_label":"Courses","index":4,"visible":true}]
  end

  # cant use has_read_access? because you can follow
  # a private channel, but not read its contents
  def can_be_followed_by? _user
    organization.has_access?(_user)
  end

  # Channels that a user can post insights to
  # NOT applicable for forum
  def self.writable_channels_for user
    org = user.organization
    Channel.includes(:channels_users).where(organization: org).where("(channels_users.user_id = (:user_id) AND channels_users.as_type = 'author') OR (channels.only_authors_can_post=0 AND channels.id in (:followed_channel_ids))", user_id: user.id, followed_channel_ids: user.followed_channel_ids).references(:channels_users)
  end

  def self.readable_channels_for(user, _organization: , onboarding: true, offset: 0, limit: nil)
    if onboarding == true || (user.try(:organization_id) != _organization.id)
      # for onboarding non-default org, return all public channels
      Channel.not_private.where(organization: _organization).offset(offset).limit(limit)
    else
      cids = Follow.where(user: user, followable_type: 'Channel').pluck(:followable_id) | ChannelsUser.where(user: user, as_type: 'author').pluck(:channel_id) | user.channel_ids_followed_via_teams
      Channel.where("channels.organization_id = (:org_id) AND ((is_private = 1 AND channels.id in (:cids)) OR (is_private = 0))", org_id: _organization.id, cids: cids).offset(offset).limit(limit)
    end
  end

  def self.readable_channel_ids_for(user, _organization: , onboarding: true, offset: 0, limit: nil)
    if onboarding == true || (user.try(:organization_id) != _organization.id)
      # for onboarding non-default org, return all public channels
      Channel.not_private.where(organization: _organization).offset(offset).limit(limit).uniq.pluck(:id)
    else
      readable_channels(_organization.channels, user.id, _organization.id).offset(offset).limit(limit).uniq.pluck(:id)
    end
  end
## END - AUTHORIZERS

  def self.fetch_eligible_channels_for_user(user: , q: nil, filter_params: {})
    query = Channel.where(organization_id: user.organization_id)
    query = query.search_by(q) if !q.blank?

    #Note : If filter params are empty then it should return all the public channel of the orgs
    #and the channels he is a part of
    if filter_params.empty?
      query = readable_channels(query,user.id, user.organization_id)
    end

    if filter_params.has_key?(:is_following)
      query = following_channels(query, filter_params[:is_following], user.id)
    end


    if filter_params.has_key?(:is_author)
      query = authoring_channels(query, filter_params[:is_author], user.id, false)
    end

    if filter_params.has_key?(:is_trusted)
      joined = filter_params.has_key?(:is_author)
      query  = trusting_channels(query, filter_params[:is_trusted], user.id, joined)
    end

    if filter_params.has_key?(:is_curator)
      joined = filter_params.has_key?(:is_author) || filter_params.has_key?(:is_trusted)
      query  = curating_channels(query, filter_params[:is_curator], user.id, joined)
    end

    if filter_params.has_key?(:writables)
     query = writable_channels(query, user.id, filter_params: filter_params)
    end
    query.uniq
  end

  def self.eligible_channels_base_query(query, user_id)
    query
    .joins("LEFT JOIN (
                        teams_channels_follows
                        INNER JOIN teams_users
                        ON teams_channels_follows.team_id = teams_users.team_id AND teams_users.user_id = #{user_id}
                      )
            ON channels.id = teams_channels_follows.channel_id"
          )
    .joins("LEFT JOIN follows
            ON channels.id = follows.followable_id and follows.followable_type = 'Channel' AND follows.user_id = #{user_id} AND follows.deleted_at IS NULL")
  end

  def self.readable_channels(query,user_id, _organization_id)
    # Readable channels = Channels followed by user directly
    #                     + Channels followed by user via Team
    #                     + Public channels of organization
    eligible_channels_base_query(query, user_id)
    .where("channels.is_private = false OR follows.id IS NOT NULL OR teams_channels_follows.id IS NOT NULL")
  end

  def self.following_channels(query, is_following, user_id)
    # Following channels = Channels followed by user directly
    #                     + Channels followed by user via Team
    if is_following
      eligible_channels_base_query(query, user_id).where("follows.id IS NOT NULL OR teams_channels_follows.id IS NOT NULL")
    else
      eligible_channels_base_query(query, user_id).where("channels.is_private = false AND follows.id IS NULL AND teams_channels_follows.id IS NULL")
    end
  end

  def self.authoring_channels (query, is_author, user_id, joined)
    query = query.joins("LEFT JOIN channels_users ON channels.id = channels_users.channel_id and channels_users.user_id = #{user_id}") unless joined
      if is_author
          query = query.where("channels_users.as_type = 'author'and channels_users.user_id = #{user_id}")
      else
          query = query.where("channels_users.user_id is null")
      end
  end

  def self.curating_channels (query, is_curator, user_id, joined)
    query = query.joins("LEFT JOIN channels_curators ON channels_curators.channel_id = channels.id and channels_curators.user_id = #{user_id}") unless joined
      if is_curator
        query = query.where("channels_curators.user_id = #{user_id}  or channels.is_open = true")
      else
        query = query.where("channels_curators.user_id is null")
      end
  end

  def self.trusting_channels (query, is_trusted, user_id, joined)
    query = query.joins("LEFT JOIN channels_users ON channels.id = channels_users.channel_id and channels_users.user_id = #{user_id}") unless joined
      if is_trusted
        query = query.where("channels_users.as_type = 'trusted_author'and channels_users.user_id = #{user_id}")
      else
        query = query.where("channels_users.as_type = 'trusted_author' and channels_users.user_id != #{user_id}")
      end
  end

  def self.writable_channels (query, user_id, filter_params: {})
    if !filter_params.has_key?(:is_following)
      query = eligible_channels_base_query(query, user_id)
    end

    if !filter_params.has_key?(:is_curator)
        query = query.joins("LEFT JOIN channels_curators ON channels_curators.channel_id = channels.id and channels_curators.user_id = #{user_id}")
    end

    if !filter_params.has_key?(:is_author) && !filter_params.has_key?(:is_trusted)
      query = query.joins("LEFT JOIN channels_users ON channels.id = channels_users.channel_id")
    end

    if filter_params[:writables]
      query = query.where("
          ( ((follows.id IS NOT NULL) or teams_users.user_id = #{user_id}) and channels.only_authors_can_post = false)
          OR (channels_users.user_id = #{user_id} AND (channels_users.as_type = 'author' OR channels_users.as_type = 'trusted_author'))
          OR channels_curators.user_id = #{user_id}
          OR (channels.is_open = true)")
    else
      query = query.where("
          (    ((follows.id IS NOT NULL) or teams_users.user_id = #{user_id}) and channels.only_authors_can_post = true)
          AND (channels_users.user_id is null )
          AND channels_curators.user_id is null")
    end
  end

  def self.recommend_channels_for_user(user: , organization: , offset: 0, limit: 10)
    following_channel_ids = user.followed_channel_ids
    Channel.not_private.where(
      "organization_id = (:org_id) AND id NOT IN(:following_channel_ids)",
      org_id: organization.id,
      following_channel_ids: following_channel_ids
    ).offset(offset).limit(limit)
  end

  def public_pubnub_channel_name
    self.pubnub_channel_name(as_type: 'public', item_type: 'Channel')
  end

  def courses_for(user: )
    courses = []

    # via group (for channel-course association)
    groups.each do |group|
      if (
        ::ResourceGroup === group &&
        !group.close_access &&
        (!group.is_private || (user && user.courses.map(&:id).include?(group.id)))
      )
        courses << group
      end
    end

    courses
  end

  def snippet
   label
  end

  def get_app_deep_link_id_and_type_v2
    [id, 'channel']
  end

  def self.search_org_channels(org, keyword, limit, offset, sort)
    limit = limit > 100 ? 100 : limit #Limit check
    sort = ["label", "description"].include?(sort) ? sort : "label"
    org.channels.where("label like ? OR description like ?", "%#{keyword}%", "%#{keyword}%").limit(limit).offset(offset).order("#{sort} ASC")
  end

  def cms_json_data
    {
     id: id,
     label: label,
     description: description,
     image_url: mobile_photo,
     banner_url: photo
    }
  end

  def get_app_deep_link_id_and_type
    [id, 'channel']
  end

  def is_public?
    !is_private
  end

  def creator_name
    creator.try(:name) || ''
  end

  def promote
    update(is_promoted: true)
  end

  def demote
    update(is_promoted: false)
  end

  def remove_channel_data
    begin
      card_ids = []

      # card
      channels_cards = ChannelsCard.where(channel: self)
      card_ids = channels_cards.map(&:card_id)
      channels_cards.destroy_all

      Card.where(id: card_ids).map(&:reindex_async)
    rescue => e
      log.error "Error while removing data for channel with id: #{self.id}"
    end
  end

  # channel_edited will also trigger in this case, but
  # push an additional event if the is_promoted column gets changed
  def record_channel_promotion_event
    yield if block_given? # IMPORTANT - runs the update
                          # In this case we want to do this before sending metric
    if changes.keys.include? "is_promoted"
      event = if is_promoted
        Analytics::ChannelMetricsRecorder::EVENT_CHANNEL_PROMOTED
      else
        Analytics::ChannelMetricsRecorder::EVENT_CHANNEL_UNPROMOTED
      end
      record_channel_event event
    end
  end

  def record_channel_event(event)
    unless is_general?
      record_custom_event(
        event: event,
        actor: :user,
        exclude_job_args: [:org]
      )
    end
  end

  def push_view_event(viewer)
    record_custom_event(
      event: 'channel_visited',
      exclude_job_args: [:org],
      actor: lambda { |record| viewer }
    )
  end

  def is_related_to_channel?(user)
    channels_users.exists?(user_id: user.id) ||
    channels_curators.exists?(user_id: user.id) ||
    is_follower?(user)
  end

  def unlink_cloned_channels
    cloned_channels.each do |cloned_channel|
      # removing the reference to the parent channel before the parent channel's deleted.
      cloned_channel.update(parent_id: nil)
    end
  end

  def self.shared_with_orgs(channel_ids)
    Channel.joins(:organization)
      .where(parent_id: channel_ids)
      .select('parent_id, organizations.name')
      .group_by(&:parent_id)
  end

  def self.organization_name(channel_ids)
    Channel.joins(:organization)
      .where(id: channel_ids)
      .select('channels.id, organizations.name')
      .group_by(&:id)
  end

  def self.cloned_channel_id(channel_ids, current_organization_id)
    Channel.where(parent_id: channel_ids, organization_id: current_organization_id)
      .select('channels.id, channels.parent_id')
      .group_by(&:parent_id)
  end

  def is_creator?(user)
    user_id == user.id
  end

  def self.invalidate_followers_cache(channel_id)
    Rails.cache.delete fetch_followers_cache_key(channel_id)
  end

  private

  def self.fetch_followers_cache_key(channel_id)
    return unless channel_id
    "channel_#{channel_id}_followers_count".to_sym
  end
end

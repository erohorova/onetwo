# == Schema Information
#
# Table name: reports
#
#  id              :integer          not null, primary key
#  user_id         :integer          not null
#  reportable_id   :integer
#  reportable_type :string(191)
#  type            :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#  validity        :boolean          default(TRUE)
#

class CompromisedReport < Report
end

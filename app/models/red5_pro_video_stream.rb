# == Schema Information
#
# Table name: video_streams
#
#  id                         :integer          not null, primary key
#  creator_id                 :integer
#  group_id                   :integer
#  start_time                 :datetime
#  name                       :string(255)
#  uuid                       :string(191)      not null
#  status                     :string(191)
#  created_at                 :datetime
#  updated_at                 :datetime
#  publish_url                :string(255)
#  playback_url               :string(1024)
#  password                   :string(255)
#  x_id                       :string(255)
#  type                       :string(255)
#  comments_count             :integer          default(0)
#  votes_count                :integer          default(0)
#  slug                       :string(191)
#  last_stream_at             :datetime
#  thumbnail_file_resource_id :integer
#  anon_watchers_count        :integer          default(0)
#  organization_id            :integer
#  state                      :string(191)
#  is_official                :boolean          default(FALSE)
#  width                      :integer
#  height                     :integer
#  card_id                    :integer
#

class Red5ProVideoStream < VideoStream

  def publish_url
    "rtmp://#{get_origin_server}/live"
  end

  def rtmp_playback_url
    #let the client deal with chosing the origin. just return the default server
    "rtmp://#{get_origin_server}/live/#{uuid}"
  end

  def playback_url
    #let the client deal with chosing the origin. just return the default server
    "http://#{get_origin_server}/live/#{uuid}/playlist.m3u8"
  end

  def provider_name
    'red5pro'
  end

  def cluster_api_endpoint
    host_only_origin = get_origin_server.split(':').first
    "#{host_only_origin}:5080/cluster"
  end

  protected
  def get_origin_server
    red5_pro_servers = Settings.red5_pro.servers.split(',').map(&:strip).compact
    red5_pro_servers.first #we can upgrade this logic to do something more interesting in the future
  end
end

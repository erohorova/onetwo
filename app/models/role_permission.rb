# == Schema Information
#
# Table name: role_permissions
#
#  id            :integer          not null, primary key
#  role_id       :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  name          :string(30)
#  permission_id :integer
#  enabled       :boolean          default(TRUE)
#  description   :string(255)
#

class RolePermission < ActiveRecord::Base
  include EventTracker

  belongs_to :role

  validates_uniqueness_of :name, scope: [:role_id]

  scope :active, -> { where(enabled: true) }

  def enable!
    record_role_permission_change('org_role_permission_enabled')
    update(enabled: true)
  end

  def disable!
    record_role_permission_change('org_role_permission_disabled')
    update(enabled: false)
  end

  def record_role_permission_change(event)
    event_opts = -> (role_permission) {
      Analytics::MetricsRecorder.role_attributes(role).merge(
        role_permission: name
      )
    }

    record_custom_event(
      event:      event,
      job_args:   [],
      event_opts: event_opts
    )
  end
end

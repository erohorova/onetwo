require 'jwt'
class AppAuthentication
  #To find org if request headers contains ['X-SAVANNAH-APP-ID'] from savannah
  def self.get_org(request: {}, params: {})
    auth_token = request.headers['X-API-AUTH-TOKEN'] || params["auth_token"]
    savannah_app_id = request.headers['X-SAVANNAH-APP-ID'] || params["savannah_app_id"]
    current_org, jwt_payload, jwt_header = nil
    if savannah_app_id
      begin
        jwt_payload, jwt_header = JWT.decode(auth_token, Settings.savannah_token)
        jwt_payload = jwt_payload.with_indifferent_access
        jwt_header  = jwt_header.with_indifferent_access
        current_org = CacheFactory.org_with_savannah_app_id(savannah_app_id: savannah_app_id)
      rescue => e
        log.error "Authentication failed due to this exception: #{e}"
      end
    end
    [current_org, jwt_payload, jwt_header]
  end

  #request from forum, it will contain destiny_user_id as params, so we are looking for user by user_id 
  #instead of client_user and client_user creation is stopped now with updated api params
  def self.api_auth!(params: nil)
    unless params
      log.info("Invalid params") and return
    end

    current_org = Organization.where(savannah_app_id: params[:savannah_app_id]).first
    unless current_org
      log.info("No org found with savannah_app_id: #{params[:savannah_app_id]}")
      return
    end
    user_id = params[:destiny_user_id]
    if is_api_client_request_valid?(params)
      #setting the @current_user instance variable enables the current_user, user_signed_in? helper methods to work in the controller
      #Forum user is just default org user
      #This flow will update and stop creating user in default org once we will move forum out
      if user_id
        current_user = get_or_create_user(user_id: user_id, email: params[:user_email], org: current_org)
      else
        log.warn('Unable to look up user') and return
      end
    else
      log.warn('invalid API request') and return
    end
    auth_params = params.slice(:api_key, :token, :resource_id, :user_id, :destiny_user_id, :user_email, :user_role, :timestamp, :savannah_app_id)
    widget_request = true
    return [current_org, current_user, auth_params, widget_request]
  end

  #Now we will be creating token with savannah_app_id instead of shared_secret in savannah
  #And new token will be rename as auth_token, can't use token to keep backward compatible
  def self.is_api_client_request_valid?(pars)
    pars = pars.with_indifferent_access
    parts = [pars[:savannah_app_id],
             pars[:timestamp],
             pars[:user_id],
             pars[:user_email],
             pars[:user_role],
             pars[:resource_id],
             pars[:destiny_user_id]].compact
    token = pars[:auth_token]
    calculated_token = Digest::SHA256.hexdigest(parts.join('|'))
    if token == calculated_token
      return true
    else
      log.info("calculated token: #{calculated_token}, request token: #{token}")
      return false
    end
  end

  def self.get_or_create_user(user_id: nil, email: nil, org: nil)
    user = User.where(id: user_id, organization_id: org.id).first
    if user.nil?
      if email
        user = User.where(email: email, organization: org).first || User.new(email: email, password: User.random_password, is_complete: false, password_reset_required: true, organization: org, organization_role: 'member')
      else
        user = User.new(email: nil, password: User.random_password, is_complete: false, password_reset_required: true, organization: org, organization_role: 'member')
      end
      user.skip_confirmation!
      user.save!
    end
    user
  end
end

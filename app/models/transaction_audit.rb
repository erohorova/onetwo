class TransactionAudit < ActiveRecord::Base

  serialize :order_data, JSON
  serialize :orderable_prices_data, JSON
  serialize :orderable_data, JSON
  serialize :user_data, JSON
  serialize :transaction_data

  validates :transaction_id, presence: true

  belongs_to :user_transaction, foreign_key: "transaction_id", class_name: "Transaction"

  def self.create_audit!(transaction_id:, order_data:, orderable_prices_data:,
                         orderable_data:, user_data:, transaction_data:)
    TransactionAudit.create!(transaction_id: transaction_id,
                             order_data: order_data,
                             orderable_prices_data: orderable_prices_data,
                             orderable_data: orderable_data,
                             user_data: user_data,
                             transaction_data: transaction_data)
  end
end

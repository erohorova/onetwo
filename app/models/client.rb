# == Schema Information
#
# Table name: clients
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  user_id        :integer
#  created_at     :datetime
#  updated_at     :datetime
#  redirect_uri   :text(16777215)
#  scopes         :string(255)      default(""), not null
#  social_enabled :boolean          default(TRUE)
#

class Client < ActiveRecord::Base
  
  belongs_to :user
  has_many :credentials, :dependent => :destroy
  has_many :groups
  has_many :channels
  has_many :configs, as: :configable, :dependent => :delete_all
  has_many :clients_users
  has_many :users, through: :clients_users
  has_many :clients_admins
  has_many :admins, through: :clients_admins, class_name: "User"
  has_one :organization
  has_one :mailer_config
  has_one :webex_config, dependent: :destroy

  before_validation do
    self.redirect_uri = 'https://www.example.com/callback/path' unless redirect_uri.present?
  end

  validates :name, :presence => true
  validates :redirect_uri, redirect_uri: true

  before_create do
    self.credentials.build(label: 'sandbox')
  end

  after_create do
    self.admins << owner
  end

  def owner
    self.user
  end

  def has_dogwood_version?
    configs.where(name: 'app.config.dogwood_release', value: 'true').any?
  end

  def are_filters_disabled?
    configs.where(name: 'app.forum.left_panel.disable', value: 'true').any?
  end

  def valid_credential
    credentials.first
  end
end

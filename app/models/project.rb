class Project < ActiveRecord::Base
  belongs_to :card
  belongs_to :reviewer, foreign_key: "reviewer_id", class_name: "User"

  has_many :project_submissions

  validates_presence_of :card, :reviewer
  before_save :set_project_reviewer

  def set_project_reviewer
    self.reviewer = self.card.author
  end
end

class UserMetric #user for aggregrating user smartbites stats
  INITIAL_TIME = DateTime.parse("2015-01-05T00:00+00:00") #this is the starting time of all the time period offset
  METRICS = ['minutes_spent', 'smartbites_consumed', 'smartbites_created', 'smartbites_score', 'percentile']
  class Metrics
    include Virtus.model
    attribute :metric, String
    attribute :period, String
    attribute :offset, Integer, default: 0
    attribute :value, Integer, default: 0
  end

  include Elasticsearch::Persistence::Model

  self.index_name = "user_metric_#{Rails.env}_v1"

  attribute :id, Integer #user id
  attribute :organization_id, Integer
  attribute :photo_small, String
  attribute :name, String
  attribute :first_name, String
  attribute :last_name, String
  attribute :email, String
  attribute :handle, String
  attribute :last_activity_at, DateTime

  attribute :metrics, Array, mapping: {
      type: "nested",
      properties: {
      period: {
          type: "string"
      },
      metric: {
          type: "string"
      },
      offset: {
          type: "long"
      },
      value: {
          type: "long"
      }}}

  def to_weekly_format
    weekly_metrics_map = self.metrics.select{|metric| metric['period'] == 'week'}.group_by do |metric|
      "#{metric['metric']}-#{metric['offset']}"
    end
    current_week_offset = ((DateTime.now - INITIAL_TIME)/7).floor
    starting_week_offset = current_week_offset - 52 #go back 52 weeks
    results = {}
    METRICS.each do |metric|
      results[metric] = (starting_week_offset..current_week_offset).to_a.map do |offset|
        key = "#{metric}-#{offset}"
        if val = weekly_metrics_map[key]
          val.first['value']
        else
          0
        end
      end
    end
    results
  end

  def self.empty_weekly_metrics
    current_week_offset = ((DateTime.now - INITIAL_TIME)/7).floor
    starting_week_offset = current_week_offset - 52 #go back 52 weeks
    results = {}
    METRICS.each do |metric|
      results[metric] = (starting_week_offset..current_week_offset).to_a.map do |offset|
        0
      end
    end
    results
  end

  def self.current_week_smartbites_score(user_id:)

    js_script = <<-EOL
                  if (doc['action'].value == 'impression' && doc['object_type'].value == 'card' && doc['actor_id'].value == #{user_id}) {
                    4
                  }else if (doc['action'].value == 'impression' && doc['object_type'].value == 'video_stream' && doc['actor_id'].value == #{user_id}) {
                    10
                  }else if (doc['action'].value == 'like' && doc['actor_id'].value == #{user_id}) {
                    4
                  }else if (doc['action'].value == 'like' && doc['owner_id'].value == #{user_id}) {
                    10
                  }else if (doc['action'].value == 'created' && doc['actor_id'].value == #{user_id} && doc['object_type'].value == 'card') {
                    10
                  }else if (doc['action'].value == 'created' && doc['actor_id'].value == #{user_id} && doc['object_type'].value == 'video_stream') {
                    20
                  }else if (doc['action'].value == 'comment' && doc['actor_id'].value == #{user_id}) {
                    1
                  }else{
                    0
                  }
                EOL
    result = MetricRecord.gateway.client.search(index: MetricRecord.index_name, search_type: 'count',
                                                body: {
                                                    query: {
                                                        filtered: {
                                                            filter: {
                                                                and: [
                                                                    {or: [
                                                                        {and: [{term: {actor_id: user_id}}, {term: {actor_type: 'user'}}]},
                                                                        {and: [{term: {owner_id: user_id}}, {term: {owner_type: 'user'}}]
                                                                        }]},
                                                                    {range: {created_at: {from: Time.now.at_beginning_of_week.iso8601, to: "now"}}}
                                                                ]

                                                            }
                                                        }
                                                    },
                                                    aggs: {
                                                        smartbites_score: {sum: {script: js_script}}
                                                    }
                                                })
    result['aggregations']['smartbites_score']['value'].to_i
  end
end

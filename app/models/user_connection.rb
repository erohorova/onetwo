# == Schema Information
#
# Table name: user_connections
#
#  id            :integer          not null, primary key
#  user_id       :integer
#  connection_id :integer
#  network       :string(191)
#

class UserConnection < ActiveRecord::Base

  belongs_to :user
  belongs_to :connection, :class_name => "User"

  scope :facebook, -> { where(network: 'facebook') }
  scope :linkedin, -> { where(network: 'linkedin') }
end

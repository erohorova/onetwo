class CardSubscription < ActiveRecord::Base

  validates :start_date, :organization_id, :card_id, :user_id, :transaction_id, presence: true

  belongs_to :organization
  belongs_to :card
  belongs_to :user

  after_commit :send_email_notification, on: :create

  def self.create_card_subscription(start_date:, end_date: nil, card_id:, organization_id:, user_id:, transaction_id:)
    CardSubscription.find_or_create_by(start_date: (start_date || Time.now),
                                       end_date: (end_date || nil),
                                       card_id: card_id,
                                       organization_id: organization_id,
                                       user_id: user_id,
                                       transaction_id: transaction_id)
  end

  def self.get_end_date_by_cards(card_ids, user_id)
    return unless card_ids.is_a?(Array)
    where(user_id: user_id, card_id: card_ids)
      .select(:card_id, :end_date)
      .group_by(&:card_id)
      .transform_values(&:first)
  end

  def send_email_notification
    PaymentMailer.user_card_subscription_notification(id).deliver_later
  end
end

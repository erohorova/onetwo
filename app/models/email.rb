# == Schema Information
#
# Table name: emails
#
#  id                 :integer          not null, primary key
#  user_id            :integer
#  email              :string(191)
#  confirmation_token :string(255)
#  confirmed          :boolean
#  created_at         :datetime
#  updated_at         :datetime
#  contextable_id     :integer
#  contextable_type   :string(255)
#  bounced_at         :datetime
#

class Email < ActiveRecord::Base

  belongs_to :user
  belongs_to :contextable, polymorphic: true

  before_validation do |record|
    record.email = record.email.downcase.strip unless record.email.blank?
  end
  
  validates :email, presence: true, email_format: { message: 'is not a valid email address' }
  validates_presence_of :user_id

  after_commit :send_confirmation_email, on: :create

  def send_confirmation_email
    UserMailer.confirm_email_address(self.id).deliver_later unless self.confirmed
  end

  def bounced! bounce_time
    # this blacklists the email and removes it from the user.email if it
    # matches. This will make `user.mailable_email` ignore this email and
    # look for another confirmed, non-blacklisted email.
    self.bounced_at = bounce_time
    save!
  end
end

# == Schema Information
#
# Table name: posts
#
#  id             :integer          not null, primary key
#  title          :string(255)
#  message        :text(16777215)
#  type           :string(255)
#  user_id        :integer          not null
#  group_id       :integer
#  created_at     :datetime
#  updated_at     :datetime
#  votes_count    :integer          default(0)
#  resource_id    :integer
#  comments_count :integer          default(0)
#  hidden         :boolean          default(FALSE)
#  anonymous      :boolean          default(FALSE)
#  pinned         :boolean          default(FALSE)
#  context_id     :string(255)
#  context_label  :string(255)
#  is_mobile      :boolean          default(FALSE)
#  pinned_at      :datetime
#  postable_id    :integer
#  postable_type  :string(191)
#

class Question < Post

  has_many :answers, as: :commentable, :dependent => :destroy

  add_to_stream action: 'asked'
  remove_from_stream action: 'asked'

  # could be commentable obj for the Comment and
  # used to check visibility of related ActivityStream in team feed
  # for the viewer User
  # should be extended in future or restricted
  def visible_to_user(user)
    true
  end
end

class PaypalPaymentGateway < PaymentGateway

  def prepare_payment_parameters(price:, urls:)
    {
      intent: "authorize",
      payer: {
        payment_method: "paypal"
      },
      redirect_urls: {
        return_url: urls[:return_url],
        cancel_url: urls[:cancel_url]
      },
      transactions: [{
        amount: {
          total: price.amount.round,
          currency: price.currency
        },
        description: "ExpressBot Payment"
      }]
    }
  end

  def merge_gateway_params(params:)
    order = params.dig(:resp_params, :order)
    transaction = params.dig(:resp_params, :transaction)
    org = order&.organization
    base_url = params[:orderable].redirect_page_url(org: org) + "?app_token=" + params.dig(:resp_params, :token)
    urls = {
      return_url: base_url,
      cancel_url: base_url + '&cancelled=true'
    }
    payment_object = create_payment_object(
                       orderable: params[:orderable],
                       price: params[:price],
                       urls: urls,
                       user_id: order&.user_id
                     )
    transaction.update_gateway_information(gateway_id: payment_object.id, gateway_data: payment_object.to_hash)
    params[:resp_params].merge!({ redirect_url: payment_object.approval_url })
  end

  def create_payment_object(orderable:, price:, urls:, user_id:)
    payment_parameters = prepare_payment_parameters(price: price, urls: urls)
    payment = PayPal::SDK::REST::Payment.new(payment_parameters)
    if payment.create
      payment
    else
      log.warn "Paypal create api failed params: #{payment_parameters} for Card : #{orderable.id} and User: #{user_id}"
      raise "Paypal create api failed"
    end
  end

  def authorize_payment(transaction, args)
    payment = PayPal::SDK::REST::Payment.find(args[:payment_id])
    begin
     payment.execute!(payer_id: payment.payer.payer_info.payer_id)
    rescue => e
      log.warn "Paypal execute payment api failed params: TransactionId: #{transaction.id}
                  PayerId: #{args[:payer_id]} PaymentId: #{args[:payment_id]} Token: #{args[:token]} Error #{e}"
    end
    payment
  end

  def get_authorization_object(gateway:)
    auth_id = gateway.transactions[0].related_resources[0].authorization.id
    PayPal::SDK::REST::Authorization.find(auth_id)
  end

  def void_payment(gateway:)
    authorization = get_authorization_object(gateway: gateway)
    authorization.void()
  end

  def capture_payment(transaction:, gateway:)
    authorization = get_authorization_object(gateway: gateway)
    authorization.capture({
                    :amount => {
                      :currency => transaction.currency,
                      :total => transaction.amount.round
                    },
                    :is_final_capture => true
                  })
  end

  def typecast_object(gateway_object)
    {
      gateway_id: gateway_object.try(:id),
      gateway_data: gateway_object.to_hash
    }
  end
  
  def get_error(gateway_object)
    return gateway_object&.error.message if gateway_object&.error.present?
    authorization = get_authorization_object(gateway: gateway_object)
    return authorization&.error.message if authorization&.error.present?
  end
end

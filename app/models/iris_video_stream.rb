# == Schema Information
#
# Table name: video_streams
#
#  id                         :integer          not null, primary key
#  creator_id                 :integer
#  group_id                   :integer
#  start_time                 :datetime
#  name                       :string(255)
#  uuid                       :string(191)      not null
#  status                     :string(191)
#  created_at                 :datetime
#  updated_at                 :datetime
#  publish_url                :string(255)
#  playback_url               :string(1024)
#  password                   :string(255)
#  x_id                       :string(255)
#  type                       :string(255)
#  comments_count             :integer          default(0)
#  votes_count                :integer          default(0)
#  slug                       :string(191)
#  last_stream_at             :datetime
#  thumbnail_file_resource_id :integer
#  anon_watchers_count        :integer          default(0)
#  organization_id            :integer
#  state                      :string(191)
#  is_official                :boolean          default(FALSE)
#  width                      :integer
#  height                     :integer
#  card_id                    :integer
#

class IrisVideoStream < VideoStream
  before_update :populate_meta_data

  def publish_url
    ""
  end

  def rtmp_playback_url
    ""
  end

  def embed_playback_url
    "https://dist.bambuser.net/player/?#{{resourceUri: self.playback_url}.to_query}"
  end

  def embed_url
    "https://embed.bambuser.com/broadcast/#{x_id}"
  end
  def signed_playback_url
    sign_url(embed_url)
  end

  def provider_name
    'iris'
  end


  def populate_meta_data
    if x_id.nil? && status == 'live'
      if meta_data = get_meta_data
        self.x_id = meta_data['payload']['vid']
        self.width = meta_data['payload']['width']
        self.height = meta_data['payload']['height']
        self.playback_url = meta_data['payload']['resource_uri']
      end

    end
  end

  def get_meta_data
    begin
      signed_url = sign_url("https://api.bambuser.com/group/media.json?title_contains=#{uuid}")
      signed_uri = URI.parse(signed_url)

      #cannot use Faraday because it reorders the query params causing the signature validation to fail
      response = Net::HTTP.start(signed_uri.host, signed_uri.port, use_ssl: signed_uri.scheme == 'https') do |http|
        request = Net::HTTP::Get.new signed_uri
        http.request request
      end
    
      if response.is_a?(Net::HTTPSuccess)
        ActiveSupport::JSON.decode(response.body)['result']['results'].first
      else
        log.error("Fail to fetch meta data from IRIS for video_stream_id=#{id}")
        nil #returns nil if something went wrong
      end
    rescue Exception => e
      log.warn "Fail to fetch meta data from api.bambuser.com for video_stream_id=#{id} with exception #{e.message}"
    end
  end

  protected
    def sign_url(url, api_id = Settings.iris.api_id, api_secret_key = Settings.iris.api_secret_key)
      da_params = {
          da_id: api_id,
          da_timestamp: Time.now.to_i,
          da_nonce:  SecureRandom.uuid,
          da_signature_method: 'HMAC-SHA256'
      }
      _url = url + (url.include?('?') ? '&' : '?')
      _url += da_params.to_query
      signature = OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha256'), api_secret_key, "GET #{_url}")
      _url + '&da_signature=' + signature
    end
end

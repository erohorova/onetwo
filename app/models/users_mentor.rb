# == Schema Information
#
# Table name: users_mentors
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  email      :string(255)
#  mentor_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class UsersMentor < ActiveRecord::Base
  belongs_to :user
  belongs_to :mentor, class_name: 'User', foreign_key: 'mentor_id'
  validates_uniqueness_of :email
end

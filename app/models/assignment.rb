# == Schema Information
#
# Table name: assignments
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  title           :string(255)
#  assignable_type :string(191)
#  assignable_id   :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  state           :string(255)      default("assigned")
#  started_at      :datetime
#  completed_at    :datetime
#  due_at          :datetime
#

class Assignment < ActiveRecord::Base
  include Streamable #Not using acts_as_streamable because we only want to create a activity stream if assignment is completed
  include AASM
  include NotificationCleanup
  include EventTracker
  include Accessible

  belongs_to :assignee, foreign_key: 'user_id', class_name: 'User'
  belongs_to :assignable, polymorphic: true
  belongs_to :card, -> { where(assignments: {assignable_type: 'Card'}) }, foreign_key: 'assignable_id'

  has_many :team_assignments, dependent: :destroy
  has_many :teams, through: :team_assignments

  ASSIGNED  = 'assigned'
  STARTED   = 'started'
  COMPLETED = 'completed'
  DISMISSED = 'dismissed'

  EVENT_ACTIONS               = {assign: 'ASSIGNED', start: 'STARTED', complete: 'COMPLETED', dismiss: 'DISMISSED'}.with_indifferent_access.freeze
  N_DAYS_BEFORE_NOTIFICATION  = [7, 3, 1].freeze
  VALID_STATES                = %w[assigned started completed dismissed].freeze

  # This scope is not used anymore
  # scope :for_user_with_options, ->(options) { where(assignee: options[:user]).limit(options[:limit]).offset(options[:offset]).order("#{options[:order_attr]} #{options[:order]}") }
  scope :search, ->(term) {  where("title like ?", "%#{term}%")}
  scope :due_in_n_days, -> (n) {where(due_at: [Time.current..(Time.current + n.days).end_of_day])}
  scope :filter_by_state, ->(state) {where(state: state.upcase)}
  scope :cards, ->{ where(assignable_type: 'Card') }
  scope :not_dismissed, ->{ where.not(state: 'dismissed') }

  before_validation :set_title

  # All callbacks of assignments handled in BulkAssignService, please change corresponding
  # methods in BulkAssignService if you are adding or changing callbacks
  after_commit :set_assignment_completed, on: :create, if: :check_self_completed_cards?
  after_commit :schedule_due_notifications, on: :create
  after_commit :reschedule_due_notifications, on: :update
  after_commit :add_to_learning_queue, on: :create
  after_commit :set_user_content_completion_state, on: :update

  validates :assignee, presence: true, uniqueness: {scope: [:assignable_id, :assignable_type]}
  validates :assignable, presence: true
  validate :is_assignee_active
  validate :should_belong_to_same_org

  aasm column: 'state' do
    state :assigned, initial: true
    state :started
    state :completed
    state :dismissed

    event :start do
      before do
        self.started_at ||= Time.current
      end

      transitions from: :assigned, to: :started
    end

    event :complete do
      before do
        self.completed_at ||= Time.current
      end

      after do
        self.assignable.complete_learning_queue_item(user_id: self.user_id)
        EDCAST_NOTIFY.trigger_event(Notify::EVENT_COMPLETE_ASSIGNMENT, self)
      end

      transitions from: :started, to: :completed
    end

    event :uncomplete do
      before do
        self.completed_at = nil
        self.started_at = nil
      end

      after do
        self.assignable.uncomplete_learning_queue_item(user_id: self.user_id)
      end

      transitions from: :completed, to: :assigned
    end

    event :dismiss do
      before do
        self.completed_at = nil
        self.started_at = nil
      end

      transitions from: [:completed, :started, :assigned], to: :dismissed
    end

    event :re_assign do
      transitions from: :dismissed, to: :assigned
    end
  end

  configure_metric_recorders(
    default_actor: lambda { |record| record.team_assignments.last&.assignor || record.assignee},
    trackers: {
      on_create: {
        event: 'card_assigned',
        if_block: -> (record) {
          record.assignable.is_a?(Card)
        },
        job_args: [{ assignee: :assignee }, { card: :assignable }],
      },
      on_edit: {
        event: "card_assignment_dismissed",
        job_args: [{ assignee: :assignee }, { card: :assignable }],
        if_block: -> (record) {
          record.assignable.is_a?(Card) &&
          record.changes.key?("state") &&
          record.dismissed?
        },
        exclude_job_args: [:changed_column, :old_val, :new_val]
      }
    }
  )

  def check_self_completed_cards?
    UserContentCompletion.where(user_id: user_id, completable_id: assignable_id, state:"completed").exists?
  end

  def set_assignment_completed
    update_columns(state: COMPLETED, completed_at: self.completed_at.presence || Time.current) unless completed?
  end

  def self.sort_by_due_date(user, opts: {})
    # Need to order assignments in below order
    # 1st show assignments with due_date appraoching w.r.t today.
    # 2nd show past assignments.
    # 3rd show assignments with no due_dates ordered by created_at DESC.
    state = opts[:state]
    card_type = opts[:card_type]
    filter_by_language = opts[:filter_by_language]
    year_filter = opts[:year_filter].to_i 
    base_query = "SELECT assignments.* FROM assignments assignments INNER JOIN " +
      "cards ON (assignments.assignable_id = cards.id AND " +
      "assignments.assignable_type = 'Card' AND " +
      "cards.state = '#{Card::STATE_PUBLISHED}')"

    base_condition = " WHERE assignments.user_id = #{user.id} AND cards.deleted_at IS NULL "

    unless opts[:viewer].is_org_admin?
      base_query = base_query + get_accessibility_query(opts[:viewer])
      base_condition = base_condition + get_accessibility_condition(opts[:viewer])
    end

    base_query = base_query + base_condition
    
    if card_type.present?
      card_type = (card_type & Card::EXPOSED_TYPES_V2)
      base_query += " AND cards.card_type IN (#{card_type.to_sql_in}) "
    end

    if filter_by_language
      base_query += " AND (cards.language = '#{user.language}' OR cards.language IS NULL)"
    end

    if state.present?
      states = (state & Assignment::VALID_STATES)
      base_query += " AND assignments.state IN (#{states.to_sql_in})"
    end

    base_query += " AND "

    if opts[:mlp_period].present?
      case opts[:mlp_period]
      when *Quarter::VALID_QUARTERS
        if user.organization.custom_quarters_enabled?
          quarter = self.custom_quarter_dates(user, opts[:mlp_period], year_filter:year_filter)
        else
          quarter = self.quarter_dates(opts[:mlp_period], year_filter: year_filter)
        end
      when 'untimed'
        result_query = base_query + "assignments.due_at IS NULL ORDER BY assignments.created_at desc"
      when 'range'
        if (opts[:start_date].present? && opts[:end_date].present?)
          quarter = {begin_date: opts[:start_date].to_date, end_date: opts[:end_date].to_date }
        end
      when 'skill-wise'
        result_query = base_query + " cards.taxonomy_topics LIKE '%#{opts[:learning_topics_name]}%' "
      when 'other'
        other_query = []
        opts[:learning_topics_names].each do |learning_topics_name|
          other_query << " cards.taxonomy_topics NOT LIKE '%#{learning_topics_name}%' "
        end
        if opts[:learning_topics_names].blank?
          base_query = base_query.gsub(/(.*) AND /,'\1')
          result_query = base_query + " AND cards.taxonomy_topics IS NULL "
        else
          result_query = base_query + "(" + other_query.join(' AND ') + " OR cards.taxonomy_topics IS NULL " + ")"
        end
      end
      if quarter.present?
        result_query = base_query + "assignments.due_at between ('#{quarter[:begin_date]}') AND ('#{quarter[:end_date]}') ORDER BY assignments.due_at asc"
      end
      return "select * from (#{result_query}) as quarter_assignments"
    else
      upcoming_assignments = base_query + "assignments.due_at > DATE(NOW()) ORDER BY assignments.due_at asc"
      past_assignments = base_query + "assignments.due_at <= DATE(NOW()) ORDER BY assignments.due_at desc"
      assignments_with_no_due_date = base_query + "assignments.due_at IS NULL ORDER BY assignments.created_at desc"

      upcoming_assignments_query = "select * from (#{upcoming_assignments}) as upcoming_assignments"
      past_assignments_query = "select * from (#{past_assignments}) as past_assignments"
      assignments_with_no_due_date_query = "select * from (#{assignments_with_no_due_date}) as no_due_date_assignments"

      [upcoming_assignments_query, past_assignments_query, assignments_with_no_due_date_query].join(' UNION ALL ')
    end
  end

  def set_title
    title_for_assignment = assignable.try(:title).present? ? :title : :message
    self.title = assignable.try(title_for_assignment).try(:truncate, 255)
  end

  def create_notification(causal_user_id:, notification_type:, message: nil)
    notification = Notification.where(notifiable: self, user_id: assignee.id, notification_type: notification_type).first_or_initialize
    # dont create duplicate notifcations
    # assign: Same assignment assigned through different groups
    # if assignment is due in next few days, dont create notification everyday
    # update timestamps so that it would show up on notification bell
    notification.created_at = Time.current
    notification.updated_at = Time.current
    notification.custom_message = message unless message.blank?
    if notification.save
      NotificationsCausalUser.create!(notification: notification, user_id: causal_user_id, anonymous: false)
    else
      log.warn "Unable to create notification with error: #{notification.errors.full_messages}"
    end
    notification
  end

  def create_push_notification(notification_type:, message:)
    return unless notification_type || message
    deep_link_id, deep_link_type = get_app_deep_link_id_and_type_v2
    opts = { user_id: self.assignee.id, deep_link_id: deep_link_id, deep_link_type:  deep_link_type, item: self, notification_type: notification_type, content: message, host_name: self.assignee.organization.host }
    PubnubNotifier.notify(opts,false)
  end

  def get_app_deep_link_id_and_type
    assignable.try(:get_app_deep_link_id_and_type)
  end

  def get_app_deep_link_id_and_type_v2
    assignable.try(:get_app_deep_link_id_and_type_v2)
  end

  def email_snippet
    markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML, quote: true)
    markdown.render(assignable.try(:title) || assignable.try(:name) || assignable.try(:message)).gsub(/\n/,'')
  end

  def snippet
    assignable.try(:snippet) || ''
  end

  def get_due_days
    ((due_at - Time.current) / 1.day).to_i
  end

  def add_to_learning_queue
    return unless assignable.respond_to? :add_to_learning_queue
    assignable.add_to_learning_queue(
      user_id: user_id,
      source: "assignments"
    )
  end

  def is_stream_worthy?
    assignable.publicly_visible?
  end

  def deeplinked_assignment_url
    deep_link_id, deep_link_type = self.get_app_deep_link_id_and_type_v2
    assignment_url = BranchMetricUrlGenerator.generator_url(self.assignable.assignment_url, deep_link_id, "assignment-#{deep_link_type}")
  end

  def self.get_due_at_by_cards(card_ids, user_id)
    return unless card_ids.is_a?(Array)
    where(user_id: user_id, assignable_id: card_ids, assignable_type: 'Card')
      .select(:id, :due_at, :assignable_id)
      .group_by(&:assignable_id)
      .transform_values(&:first)
  end

  def set_changed_state
    self.changed_state = true
  end

  private

  def self.quarter_dates(quarter_name, year_filter: Date.current.year)
    case quarter_name
      when 'quarter-1'
        start_date = Date.parse("1 jan #{year_filter}")
      when 'quarter-2'
        start_date = Date.parse("1 apr #{year_filter}")
      when 'quarter-3'
        start_date = Date.parse("1 jul #{year_filter}")
      when 'quarter-4'
        start_date = Date.parse("1 oct #{year_filter}")
    end
    {
      :begin_date => start_date.beginning_of_quarter,
      :end_date => start_date.end_of_quarter
    }
  end

  def self.custom_quarter_dates(user, quarter_name, year_filter: Date.current.year)
    quarter = Quarter.where(name: quarter_name, organization_id: user.organization_id).first
    if (quarter && quarter.start_date.present? && quarter.end_date.present?)
      quarter_start_date = Quarter.where(name: "quarter-1", organization_id: user.organization_id).pluck(:start_date).first
      year_diff = (quarter_start_date.year - year_filter)
      {
        :begin_date => quarter.start_date - year_diff.year,
        :end_date => quarter.end_date - year_diff.year
      }
    else
      self.quarter_dates(quarter_name, year_filter: year_filter)
    end
  end

  def schedule_due_notifications
    schedule_all_due_notifications
  end

  def reschedule_due_notifications
    if previous_changes['due_at']  #http://api.rubyonrails.org/classes/ActiveModel/Dirty.html#method-i-previous_changes
      #schedule them
      schedule_all_due_notifications
    end
  end

  def schedule_all_due_notifications
    return if due_at.nil? || completed_at.present? || completed?
    N_DAYS_BEFORE_NOTIFICATION.each do |day_int|
      if ((due_at - Time.current) / 1.day) >= day_int
        AssignmentDueNotificationJob.set(wait_until: due_at - day_int.days).perform_later(self.id, self.due_at.to_i)
      end
    end
  end

  # fetches users of team assignments using assignor id, assignable data, state of assignment

  # arguments:
    # assignor_id
    # team_id
    # assignable_type
    # assignable_id
    # state of assignment (array)

  # returns users with following attributes:
    # first name
    # last name
    # email
    # assignment id of user

  def self.assigned_users(q:, assignor_id: nil, team_id: nil, content_id:, state:)
    query = includes(:assignee).joins(:team_assignments).where(assignable_type: 'Card', assignable_id: content_id)
    if assignor_id
      query = query.where("team_assignments.assignor_id = ?", assignor_id)
    end

    if team_id
      query = query.where("team_assignments.team_id = ?", team_id)
    end

    query = query.where(state: state) unless state.blank?

    if q.present?
      query = query.joins(:assignee).where("users.first_name like :q OR
                   users.last_name like :q OR
                   concat(users.first_name ,' ', users.last_name) like :q OR
                   users.email like :q OR
                   users.handle like :q",
        q: "%#{q}%")
    end

    query.distinct("assignments.user_id")
  end

  def self.assignments_per_assignor(user_id: , assignor_id:)
    joins(:team_assignments).where("assignments.user_id = ? and team_assignments.assignor_id = ?", user_id, assignor_id)
  end

  def is_assignee_active
    # Unfortunately we kinda messed up having 3 columns to decide status
    # of the user, this is going to be cleaned up by Crait team.
    if !assignee&.is_active? || assignee&.is_suspended? || assignee&.status != User::ACTIVE_STATUS
      errors.add(:user_id, 'User is not active')
    end
  end

  def should_belong_to_same_org
    if assignee&.organization_id != assignable&.organization_id
      errors.add(:base, 'User and Card are not in same organization')
    end
  end

  # sync UserContentCompletion records
  # For unit tests, pls check LrsAssignmentService tests.
  def set_user_content_completion_state
    SyncState.new(card: assignable, object: self).user_content_completion_update
    # ucc = UserContentCompletion.where(completable: assignable, user: assignee)
    #   .first_or_create
    #
    # begin
    #   ucc.send(self.event_for_state) if self.event_for_state
    # rescue AASM::InvalidTransition => e
    #   log.info "Invalid state transition: #{e.message} for user content completion with id: #{ucc.id}"
    # end
  end
end

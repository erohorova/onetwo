# == Schema Information
#
# Table name: identity_providers
#
#  id            :integer          not null, primary key
#  type          :string(191)
#  uid           :string(191)
#  user_id       :integer
#  token         :string(4096)
#  secret        :string(255)
#  expires_at    :datetime
#  created_at    :datetime
#  updated_at    :datetime
#  token_scope   :string(255)
#  auth          :boolean
#  auth_info     :text(65535)
#  refresh_token :string(255)
#

class FacebookProvider < IdentityProvider

  cattr_accessor :facebook_api

  def share(message: nil, object: nil)
    me = FbGraph::User.me(token)

    payload = {message: message}
    if object.kind_of? Post
      return if (message && object.ref_url).blank?
      payload.merge!({
          :link => object.ref_url,
          :name => object.title,
          :description => object.message_text
      })

      payload[:caption] = object.group.name if object.respond_to?(:group)
    elsif object.kind_of? Card
      owner = User.find(object.author_id)
      host_name = owner.organization.host
      payload.merge!({
        :link => "https://#{host_name}/insights/#{object.slug}"
      })
    else
      log.error "Trying to share an unsupported object: #{object.class.name}" and return
    end
    begin
      post = me.feed!(payload)
      return "http://www.facebook.com/#{post.identifier}"
    rescue => e
      log.warn("Error validating access token, error: #{e.message}; token: #{token}; user-email: #{me.email}")
    end
    ""
  end

  def self.is_verified?(auth_hash)
    verified = auth_hash[:info][:verified]
    verified = true if auth_hash[:info][:verified].nil?
    auth_hash[:info][:email] && verified
  end

  def scoped?(scope)
    if token_scope.nil?
      token_str = resolve_token_scope
      return super if token_str.nil?
      update(token_scope: token_str)
    end

    scope_string = {
        network: 'user_friends',
        share: 'publish_actions',
        location: 'user_location',
    }[scope]

    return super if scope_string.nil?
    return token_scope.split(',').map(&:strip).include?(scope_string)
  end

  private

  # determine scope from existing token
  def resolve_token_scope
    token_data = retrieve_token_attributes.try(:[], 'data')
    return nil unless token_data.try(:[], 'is_valid')
    return token_data.try(:[], 'scopes').try(:join, ',')
  end

  def retrieve_token_attributes
    if @@facebook_api.nil?
      oauth = Koala::Facebook::OAuth.new(Settings.facebook.api_key,
                                         Settings.facebook.secret,
                                         Settings.platform_host)
      access_token = oauth.get_app_access_token
      @@facebook_api = Koala::Facebook::API.new(access_token)
    end
    return @@facebook_api.debug_token(token)
  end
end

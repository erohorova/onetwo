# == Schema Information
#
# Table name: badgings
#
#  id                   :integer          not null, primary key
#  title                :string(255)
#  badge_id             :integer
#  badgeable_id         :integer
#  type                 :string(20)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  target_steps         :integer
#  all_quizzes_answered :boolean          default(FALSE)
#

class Badging < ActiveRecord::Base
  include EventTracker
  has_many :user_badges
  belongs_to :badge

  validates :badge_id, :title, presence: true

  configure_metric_recorders(
    default_actor: lambda { |record| },
      trackers: {
        on_create: {
         event: 'org_badging_created',
         job_args: [ {badging: :itself} ]
        },
        on_delete: {
          event: 'org_badging_deleted',
          job_args: [ {badging: :itself} ]
        },
        on_edit: {
          event: 'org_badging_edited',
          job_args: [ {badging: :itself} ]
        }
      }
  )
end

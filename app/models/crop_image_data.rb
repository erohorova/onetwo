# == Schema Information
#
# Table name: crop_image_data
#
#  id             :integer          not null, primary key
#  croppable_id   :integer
#  croppable_type :string(255)
#  crop_column    :string(255)
#  file_signature :string(255)
#  x1             :integer
#  y1             :integer
#  x2             :integer
#  y2             :integer
#  created_at     :datetime
#  updated_at     :datetime
#

class CropImageData < ActiveRecord::Base
  belongs_to :croppable, polymorphic: true
end

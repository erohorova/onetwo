# See documentation at http://www.opencalais.com/wp-content/uploads/folder/ThomsonReutersOpenCalaisAPIUserGuide020316R93.pdf
# See test file for example API response
class OpencalaisExtractor
  OPENCALAIS_ENDPOINT = "https://api.thomsonreuters.com"

  def initialize(text: )
    @text = text
    @opencalais_response_data = nil
  end

  # extracts opencalais response for a given URL
  def extract!
    @opencalais_response_data ||= (conn = Faraday.new(url: OPENCALAIS_ENDPOINT)

    result = conn.post do |req|
      req.url '/permid/calais'
      req.headers['Content-Type'] = 'text/raw'
      req.headers['x-ag-access-token'] = Settings.opencalais.api_key
      req.headers['outputFormat'] = 'application/json'
      req.body = @text
    end

    JSON.parse(result.body))
  end

  def tags
    raise_if_not_extracted!
    @tags ||= (
    # importance goes from 1 - most important to 3 - less important
    social_tags = @opencalais_response_data.values.select{|v| v['_typeGroup'] == 'socialTag' && v['importance'].to_i == 1}.map{|v| v['name']}

    # score goes from 0 to 1
    topic_tags = @opencalais_response_data.values.select{|v| v['_typeGroup'] == 'topics' && v['score'].to_f > 0.5}.map{|v| v['name']}

    # relevance goes from 0 to 1
    industry_tags = @opencalais_response_data.values.select{|v| v['_typeGroup'] == 'industry' && v['relevance'].to_f > 0.5}.map{|v| v['name']}

    # High relevance -> 0.8 or above
    # Zero relevance -> 0
    # Everything in between -> everything in between
    entity_tags = @opencalais_response_data.values.select{|v| v['_typeGroup'] == 'entities' && v['relevance'].to_f >= 0.8}.map{|v| v['name']}

    (social_tags | topic_tags | industry_tags | entity_tags).compact)
  end

  def raise_if_not_extracted!
    raise "Call extract before accessing this methid" if @opencalais_response_data.nil?
  end
end

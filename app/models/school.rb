# == Schema Information
#
# Table name: schools
#
#  id                   :integer          not null, primary key
#  name                 :string(255)
#  specialization       :string(255)
#  user_id              :integer
#  image_file_name      :string(255)
#  image_content_type   :string(255)
#  image_file_size      :integer
#  image_updated_at     :datetime
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  from_year            :string(255)
#  to_year              :string(255)
#  institution_type     :string(255)
#  degree_name          :string(255)
#  institution_location :string(255)
#

class School < ActiveRecord::Base
  belongs_to :user

  has_attached_file :image, :styles => { small: '120x120',medium: '320x320' }

  validates_attachment_content_type :image,  :content_type => /^image\/(png|jpeg|jpg|pjpeg|x-png|gif|tiff)$/,
                                    :message => 'Invalid file type'
  validates :name, :specialization, :from_year, :to_year, :user_id, presence: true
  validates :from_year, :to_year,
    format: {
      with: /\A(19|20)\d{2}\Z/i,
      message: "should be in YYYY format"
    }
  validate :validate_year_range

  def validate_year_range
    if self.from_year && self.to_year && self.to_year < self.from_year
      errors.add(:to_year, 'should be same or after the From Year')
      return
    end
  end
end

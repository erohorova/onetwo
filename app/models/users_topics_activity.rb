class UsersTopicsActivity < ActiveRecord::Base
  belongs_to :organization
  belongs_to :user

  %i{
    organization_id
    user_id
    topic_counts
    start_time
    end_time
  }.each { |col| validates_presence_of col }

  serialize :topic_counts, JSON

  has_many :teams_users, {
    source:      "TeamsUser",
    foreign_key: :user_id,
    primary_key: :user_id
  }

end

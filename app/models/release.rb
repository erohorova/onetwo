# == Schema Information
#
# Table name: releases
#
#  id          :integer          not null, primary key
#  state       :string(255)      default("inactive")
#  platform    :string(255)
#  form_factor :string(255)      default("phone")
#  version     :string(255)
#  notes       :string(10240)
#  created_at  :datetime
#  updated_at  :datetime
#  build       :integer
#

class Release < ActiveRecord::Base

  validates_presence_of :version

  validates_presence_of :state
  validates_inclusion_of :state, in: %w{active inactive}

  validates_presence_of :platform
  validates_inclusion_of :platform, in: %w{ios android}

  validates_presence_of :form_factor
  validates_inclusion_of :form_factor, in: %w{phone tablet}

  def download_url
    Settings.send("#{self.platform}_app_location".to_s)
  end
  
end

# == Schema Information
#
# Table name: user_roles
#
#  id      :integer          not null, primary key
#  user_id :integer
#  role_id :integer
#

class UserRole <  ActiveRecord::Base
  include EventTracker

  belongs_to :user, required: true
  belongs_to :role, required: true
  validates_uniqueness_of :role_id, scope: :user_id
  after_commit :reindex_user

  def reindex_user
    user.reindex_async
  end

  configure_metric_recorders(
    default_actor: :user,
    trackers: {
      on_create: {
        job_args: [{added_role: :role_name}, {user: :user}],
        event: 'user_role_added'
      },
      on_delete: {
        job_args: [{removed_role: :role_name}, {user: :user}],
        event: 'user_role_removed',
      }
    }
  )

  def role_name
    role.name
  end
end

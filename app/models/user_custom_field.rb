# == Schema Information
#
# Table name: user_custom_fields
#
#  id              :integer          not null, primary key
#  custom_field_id :integer          not null
#  user_id         :integer          not null
#  value           :text(65535)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class UserCustomField < ActiveRecord::Base
  include WorkflowPatcher
  include WorkflowObserver
  include EventTracker

  belongs_to :custom_field
  belongs_to :user

  validates :user_id, :custom_field_id, presence: true

  after_commit :reindex_user
  after_commit :detect_upsert, on: [:create, :update, :destroy]

  attr_accessor :upload_mode # Used for Workflow

  scope :enabled_in_people_search,
        -> { joins(:custom_field).where(custom_fields: {enable_people_search: true}) }

  delegate :organization, to: :custom_field

  metric_recorder_job_args = {
    member: :user, custom_field: :custom_field, user_custom_field: :itself
  }
  configure_metric_recorders(
    default_actor: lambda { |record| },
    trackers: {
      on_create: {
        event: 'user_custom_field_added',
        job_args: [ metric_recorder_job_args ]
      },
      on_edit: {
        event: 'user_custom_field_edited',
        job_args: [ metric_recorder_job_args ]
      },
      on_delete: {
        event: 'user_custom_field_removed',
        job_args: [ metric_recorder_job_args ]
      }
    }
  )

  class << self
    def create_or_update(args = {})
      args[:custom_fields].each do |custom_field|
        user_custom_field = UserCustomField.find_or_create_by(user_id: args[:user_id], custom_field_id: custom_field[:custom_field_id])
        user_custom_field.update_attribute(:value, custom_field[:value])
      end
    end

    def filter_by_configs(config_name)
      joins(custom_field: :configs).where(configs: { name: config_name, value: 'true' })
    end
  end

  def reindex_user
    user.reindex_async_urgent
  end
end

# == Schema Information
#
# Table name: contexts
#
#  id          :integer          not null, primary key
#  context_set :text(16777215)   not null
#  styles      :text(16777215)   not null
#

class Context < ActiveRecord::Base

  # context history saved as a set of contexts (hash), and an array of style symbols derived
  serialize :context_set, Hash
  validates :context_set, presence: true

  serialize :styles, Array
  validates :styles, presence: true

  # array of context provisioners. provisioners respond to .contexts=, then .styles, and .properties(styles)
  cattr_accessor :provisioners do [] end

  # hash to contain all property key/value pairs pertaining to current context
  cattr_accessor :properties do {} end

  class << self

    # initialize contexts, e.g. init(user: user1, time: Time.Now)
    # TODO allow for adding contexts incrementally rather than a single init
    def init(contexts = {})

      styles = []
      @@provisioners.each do |provisioner|
        # populate @@properties
        # TODO cache properties
        provisioner.contexts = contexts
        provisioner_styles = provisioner.styles
        styles |= provisioner_styles
        @@properties.merge! provisioner.properties(provisioner_styles)
      end

      create!(context_set: contexts, styles: styles)
    end

    # separate property existence check, to allow for nil property value
    def property?(name)
      @@properties.key? name
    end

    def property(name)
      @@properties[name]
    end

  end

end

require 'active_support/concern'

module Assessable
  extend ActiveSupport::Concern

  included do
    # Associated question (i.e card_type = `poll`) cards in a pathway.
    def associated_poll_cards
      children_cards = self.card_pack_relations.pluck(:from_id)

      Card.where(id: children_cards).polls
    end

    # AIM: Returns uniq users who attempted atleast one question from pathways
    # FLOW:
    #   1. Pick cover card from card_pack_relations (matching :cover_id)
    #   2. Pick quiz_question_attempts for question-cards added to a pack
    #   3. Users from quiz_question_attempts satisfying #2
    def attempted_by
      self
        .organization
        .users
        .joins(:quiz_question_attempts)
        .joins("INNER JOIN card_pack_relations on card_pack_relations.from_id = quiz_question_attempts.quiz_question_id and quiz_question_type = 'Card'")
        .where("card_pack_relations.cover_id = #{self.id}")
        .uniq
    end

    # AIM: Get stats of attempts i.e correct, wrong and total attempts. Expects question-card ids.
    # FLOW:
    #   1. Fetch all quiz options when question ids provided for a pathway.
    #   2. Pick correct option from cards obtained from #1.
    #   3. Pick stats for each question card
    #   4. Extract count of users who have attempted it as right, wrong and atleast attempted once.
    def correct_attempts_count quiz_question_ids
      total_attempt =  correct_answers =  wrong_answers = 0
      # correct option
      quiz_questions = Card.where(id: quiz_question_ids, organization_id: self.organization_id)

      quiz_questions.each do |quiz_question|
        next unless quiz_question.is_a_poll_question?

        correct_option = quiz_question.quiz_question_options.correct_option.first
        question_stats = quiz_question.quiz_question_stats
        total_attempt  = total_attempt + question_stats.try(:attempt_count)
        
        # attempted answers
        if question_stats.present?
          correct_answer_count = question_stats.try(:options_stats).try(:[], correct_option['id'].to_s) || 0
          correct_answers      = correct_answers + correct_answer_count
          wrong_answers        =  wrong_answers + (question_stats.try(:attempt_count) - correct_answer_count)
        end
      end

      {
        'correct_answers'     => correct_answers,
        'wrong_answers'       => wrong_answers,
        'total_answers_count' => total_attempt
      }
    end

    # Checks if the card is a poll quiz-question with one option set as correct.
    # USE-CASE 1: When used with cover card
    #   1. pick all children cards from pathways
    #   2. Pick all attempts for cards retrieved from #1
    # USE-CASE 2: When used with poll card
    #   1. Pick all attempts for cards retrieved directly
    def is_a_poll_question?
      if poll_card?
        self.quiz_question_options.correct_option.present?
      elsif pack_card?
        CardPackRelation
          .joins("INNER JOIN quiz_question_options on card_pack_relations.from_id = quiz_question_options.quiz_question_id")
          .where("quiz_question_options.is_correct = true and card_pack_relations.cover_id = #{id}")
          .present?
      end
    end

    def self.is_a_poll_question?(card_id)
      card = Card.find_by(id: card_id)
      card.is_a_poll_question?
    end

    def pack_quiz_correct_options
      QuizQuestionOption
        .joins("INNER JOIN card_pack_relations on card_pack_relations.from_id = quiz_question_options.quiz_question_id and quiz_question_type = 'Card'")
        .where("card_pack_relations.cover_id = #{self.id} and quiz_question_options.is_correct = true")
        .uniq
    end

    def percentage_of_correctness
      total_percentage_of_correctness = 0

      attempted_by.each do |user|
        total_percentage_of_correctness += user.percentage_correct(id)
      end

      attempted_by_count = attempted_by.count
      if attempted_by_count > 0
        (total_percentage_of_correctness / attempted_by_count).round(2)
      else
        0.0
      end
    end

    # AIM: Pick all `quiz_question_attempts` for all cards (poll/pack)
    # Pathway: When used with cover card
    #   1. pick all children cards from pathways
    #   2. Pick all attempts for cards retrieved from #1
    # Question card: When used with poll card
    #   1. Pick all attempts for cards retrieved directly
    def poll_attempts
      if self.pack_card?
        card_ids = self.card_pack_relations.pluck(:from_id)
      else
        card_ids = [self.id]
      end

      QuizQuestionAttempt.where(quiz_question_id: card_ids)
    end
  end
end

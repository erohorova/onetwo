require 'active_support/concern'

module Queueable
  extend ActiveSupport::Concern

  included do
    has_many :learning_queue_items, as: :queueable

    def add_to_learning_queue(user_id: , source: , state: nil)
      lq = LearningQueueItem.find_by(user_id: user_id, queueable: self)
      if lq
        state.nil? ? lq.touch : lq.update_attributes(state: state)
        if source
          lq.update_attributes(source: source)
        end
      else
        LearningQueueItem.create!(user_id: user_id, queueable: self, source: source, state: state)
      end
    end

    def complete_learning_queue_item(user_id: )
      lq = LearningQueueItem.find_by(user_id: user_id, queueable: self)
      lq&.complete
    end

    def uncomplete_learning_queue_item(user_id: )
      lq = LearningQueueItem.find_by(user_id: user_id, queueable: self)
      lq&.uncomplete
    end

    def mark_learning_queue_items_as_deleted
      learning_queue_items.each {|lqi| lqi.mark_as_deleted}
    end

    def dismiss_learning_queue_item(user_id: )
      lq = LearningQueueItem.find_by(user_id: user_id, queueable: self)
      lq&.dismiss
    end

  end
end


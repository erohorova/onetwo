require 'active_support/concern'

module WebexConference
  extend ActiveSupport::Concern

  included do
    def create_webex_meeting
      options = {}
      options[:duration] = ((event_end - event_start)/60.0) if event_end && event_start
      options[:emails] = conference_email == self.user.email ? [self.user.email] : [self.user.email,conference_email]
      options[:scheduled_date] = event_start
      webex_config = org.webex_config
      
      if webex_config
        webex_client = get_webex_client

        begin
          meeting_key = webex_client.create_meeting(name,options)
          if meeting_key
            self.webex_conference_key = meeting_key
          else
            self.errors.add(:conference_type, "Something went wrong, Please try it again")  
          end  
        rescue => e
          self.errors.add(:conference_type, e.message)
          raise ActiveRecord::RecordInvalid.new(self)
        end 
      else  
        
        self.errors.add(:conference_type, "Webex Credential can not be blank")
        raise ActiveRecord::RecordInvalid.new(self)
      end   
    end

    def set_join_url
      webex_client = get_webex_client

      self.conference_url = webex_client.get_meeting_join_url(self.webex_conference_key)
      self.save
    end

    def set_host_url
      webex_client = get_webex_client
      self.host_url = webex_client.get_meeting_host_url(webex_conference_key)
      self.save
    end
    def delete_webex_meeting
      webex_client = get_webex_client
      begin
        webex_client.delete_meeting(webex_conference_key)
      rescue=>e

      end
    end

    def webex_conference?
      self.conference_type == "webex"
    end

    def add_attendee(user)
      begin
        webex_client = get_webex_client
        webex_client.add_attendee_to_meeting(webex_conference_key,user.email,{:name=>user.name,:email_invitation=>"TRUE"})
      rescue => e
      end  
    end

    def remove_attendee(user)
      begin
        webex_client = get_webex_client
        webex_client.delete_attendee_from_meeting(webex_conference_key,user.email)
      rescue => e
      end  
    end

    def get_webex_client
      webex_config = org.webex_config
      WebexApi::Client.new(webex_id, webex_password, webex_config.site_id, webex_config.site_name, webex_config.partner_id, conference_email)
    end
  end
end

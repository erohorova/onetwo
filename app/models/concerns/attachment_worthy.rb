require 'active_support/concern'

module AttachmentWorthy
  extend ActiveSupport::Concern

  included do
    has_many :file_resources, as: :attachable, dependent: :destroy
  end
end


require 'active_support/concern'

module CustomCarousel
  extend ActiveSupport::Concern
  ExcludedClasses = ['ChannelsCard']

  included do
    after_destroy :remove_from_custom_carousel
    #Removing from custom carousel is different for channel cards.
    #If a card is removed from channel, it should be removed from that channel's carousel only.
    #Hence exluded it from common process and wrote it in ChannleCards itself.
    #This can be re-used for may be ChannelUsers etc.

    def remove_from_custom_carousel
      if ExcludedClasses.include?(self.class.to_s)
        self.send("remove_#{self.class.table_name}_from_custom_carousel")
        return
      end
      content = StructuredItem.where(entity: self)
      StructuredItem.reorder_and_destroy(content) if content.present?
    end

  end
end

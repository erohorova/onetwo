# Include this module in any model whose changes we have to monitor
module WorkflowObserver
  extend ActiveSupport::Concern

  included do
    alias_method :workflow_previous_changes, :previous_changes
    alias_method :workflow_id, :id

    after_commit do
      emit_changes_to_workflow if should_emit_changes?
    end

    # Here, UserCustomField is an exception
    # As the value is in one table and the custom field name in another. Hence, this code to manipulate record data
    # Format: prepend word +workflow_+ to the existing methods
    def workflow_previous_changes
      if instance_of?(UserCustomField)
        changes = previous_changes
          .except('created_at', 'updated_at')
          .merge(custom_field.abbreviation => previous_changes['value']) if previous_changes['value']
        changes = {custom_field.abbreviation => [value_was, nil], 'id' => [id_was, nil]} if destroyed?
        return changes
      end
      if instance_of?(User)
        user_columns = []
        WorkflowService::Source::INPUT_SOURCE[self.class.name.downcase.to_sym][:columns]
          .each { |user| user_columns << user[:column_name] }
        return previous_changes.with_indifferent_access.slice(*user_columns)
      end
    end

    def workflow_id
      instance_of?(UserCustomField) ? user_id : id
    end
  end

  private

  def emit_changes_to_workflow
    WorkflowObserverJob.set(wait: 4.seconds).perform_later(
      organization: organization,
      changes: workflow_previous_changes,
      model_name: self.class.name.to_s,
      input_data_ids: [workflow_id]
    )
  end

  def should_emit_changes?
    organization.simplified_workflow_enabled? &&
      workflow_previous_changes.present? &&
      (
        (instance_of?(User) && upload_mode != 'bulk_upload') ||
        (instance_of?(UserCustomField) && upload_mode != 'bulk_upload')
      )
  end

  def detect_upsert(*args)
    # 'detect_upsert_overridden from WorkflowPatcher'
    super(*args) unless organization.simplified_workflow_enabled?
  end

  def detect_delete
    # 'detect_delete_overridden from WorkflowPatcher'
    super unless organization.simplified_workflow_enabled?
  end
end

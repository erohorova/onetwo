require 'active_support/concern'

# =========================================
# This module is included by the Channel model
# It loads top-cards and top-users lists,
# which are used by the /channels/:id/aggregated_metrics endpoint.
# =========================================

module ChannelModelLeaderboardConcern

  # Loads a list of cards that have been added to the channel.
  # They are scored by the total amount of activity they have had.
  # This activity count is precomputed through CardMetricsAggregation records.
  def get_top_cards_data(**opts)
    query, params = top_cards_data_query(**opts)
    # Note, it really doesn't matter what model we use here,
    # we're loading custom custom columns with SELECT anyway
    records = Card.find_by_sql([query, params])
    compact_attrs(records) do |record|
      record.attributes
        .merge("title" => record.snippet)
        .except("message")
    end
  end

  # Loads a list of users who have contributed the most content to the channel.
  # This is determined by looking up channels_cards records joined with follows
  # and users. No part of it is pre-computed.
  def get_top_contributors(**opts)
    # The times get passed to this method as integers.
    # convert them to Time objects since we are comparing SQL DateTime columns.
    opts[:start_date] = Time.at(opts[:start_date])
    opts[:end_date] = Time.at(opts[:end_date]) if opts[:end_date]
    query, params = top_contributors_query(**opts)
    compact_attrs(User.find_by_sql([query, params])) do |record|
      filter_user_attrs(record.attributes.merge("photo" => record.photo))
    end
  end

  # Returns the top users who are following this channel.
  # Users ranked according to the user_score system (see event_scores.yml).
  # The per-day, per-user scores are precomputed in UserMetricsAggregation records.
  def get_top_scoring_users(**opts)
    query, params = top_scoring_users_query(**opts)
    compact_attrs(User.find_by_sql([query, params])) do |record|
      filter_user_attrs(record.attributes.merge("photo" => record.photo))
    end
  end

  private

  def compact_attrs(records, &blk)
    blk ||= -> (record) { record.attributes }
    records
      .map { |record| blk.call(record) }
      .map(&:compact)
  end

  def filter_user_attrs(user_attrs)
    user_attrs.slice *%w{
      first_name last_name user_id bio total_user_score num_cards photo
    }
  end

  def top_scoring_users_query(limit:, offset:, start_date:, end_date:)
    params = {
      limit:           limit,
      offset:          offset,
      start_date:      start_date.to_i,
      end_date:        end_date.to_i,
      organization_id: organization_id,
      channel_id:      id
    }
    query = <<-TXT.strip_heredoc
      SELECT
        users.id AS user_id,
        users.first_name,
        users.last_name,
        users.bio,
        users.avatar_file_name,
        users.avatar_content_Type,
        users.avatar_file_size,
        users.avatar_updated_at,
        users.picture_url,
        SUM(user_metrics_aggregations.total_user_score) AS total_user_score
      FROM user_metrics_aggregations
      INNER JOIN users ON user_metrics_aggregations.user_id = users.id
      INNER JOIN follows ON follows.user_id = users.id
      WHERE follows.followable_id = :channel_id
      AND follows.followable_type = 'Channel'
      AND follows.deleted_at IS NULL
      AND start_time >= :start_date
      AND end_time <= :end_date
      GROUP BY users.id
      ORDER BY total_user_score DESC
      LIMIT :limit
      OFFSET :offset
    TXT
    [query, params]
  end

  def top_contributors_query(limit:, offset:, start_date:, end_date:)
    # This query is going to SQL, and the times we compare against are
    # stored as DateTime objects, so we query using Time objects as well.
    unless [start_date, end_date].compact.all? { |val| val.is_a?(Time) }
      raise ArgumentError, "start_date and end_date must be Time objects"
    end
    params = {
      limit:           limit,
      offset:          offset,
      start_date:      start_date,
      end_date:        end_date,
      organization_id: organization_id,
      channel_id:      id
    }
    query = <<-TXT.strip_heredoc
      SELECT
        users.first_name,
        users.last_name,
        users.bio,
        users.avatar_file_name,
        users.avatar_content_Type,
        users.avatar_file_size,
        users.avatar_updated_at,
        users.picture_url,
        users.id AS user_id,
        COUNT(channels_cards.id) AS num_cards
      FROM channels_cards
      INNER JOIN cards ON cards.id = channels_cards.card_id
      INNER JOIN users ON cards.author_id = users.id
      WHERE channel_id = :channel_id
      AND cards.deleted_at IS NULL
      AND channels_cards.created_at >= :start_date
      AND channels_cards.created_at <= :end_date
      GROUP BY users.id
      ORDER BY num_cards DESC
      LIMIT :limit
      OFFSET :offset
    TXT
    [query, params]
  end

  def top_cards_data_query(limit:, offset:, start_date:, end_date:)
    params = {
      limit:           limit,
      offset:          offset,
      start_date:      start_date.to_i,
      end_date:        end_date.to_i,
      organization_id: organization_id,
      channel_id:      id
    }
    query = <<-TXT.strip_heredoc
      SELECT
        cards.title,
        cards.message,
        card_metrics_aggregations.card_id,
        card_metrics_aggregations.organization_id,
        SUM(num_likes) as sum_num_likes,
        SUM(num_views) as sum_num_views,
        SUM(num_completions) as sum_num_completions,
        SUM(num_bookmarks) as sum_num_bookmarks,
        SUM(num_comments) as sum_num_comments,
        SUM(num_assignments) as sum_num_assignments,
        (
          SUM(num_likes) +
          SUM(num_views) +
          SUM(num_completions) +
          SUM(num_bookmarks) +
          SUM(num_comments) +
          SUM(num_assignments)
        ) AS sort_score
      FROM card_metrics_aggregations
      INNER JOIN cards on card_metrics_aggregations.card_id=cards.id
      INNER JOIN channels_cards on channels_cards.card_id=cards.id
      INNER JOIN channels on channels_cards.channel_id = channels.id
      WHERE card_metrics_aggregations.organization_id = :organization_id
      AND cards.deleted_at IS NULL
      AND channel_id = :channel_id
      AND start_time >= :start_date
      AND end_time <= :end_date
      GROUP BY card_metrics_aggregations.card_id
      ORDER BY sort_score DESC
      LIMIT :limit
      OFFSET :offset
    TXT
    [query, params]
  end

end

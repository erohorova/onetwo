require 'active_support/concern'
module UserRollable
  extend ActiveSupport::Concern

  def add_roles(collection)
    collection.each do |role|
      self.add_role(role)
    end
  end

  # Add user role
  # user.add_role("customer")
  # user.add_role(role_object)
  # It will check if role is exist for organization
  #  If not create organization role and
  # then add role to user if role is not exist for user
  def add_role(role_name)
    if organization_roles_exist?(role_name)
      if !has_any_user_roles?(role_name)
        if role_name.is_a?(String)
          role = user_organization_roles.where(:default_name=>role_name).first
        else
          role = role_name
        end

        self.user_roles.create(role: role)
      end
    else
      # We don't want dynamic role
      # self.organization.roles.create(name: role_name)
      # add_role(role_name)
    end
  end

  def remove_curator_role(role_name)
    if self.channels_curators.empty?
      role = user_organization_roles.where(:default_name=>role_name).first
      self.user_roles.where(role_id: role.id).destroy_all
    end
  end

  def remove_collaborator_role(role_name)
    if self.channels_authors.empty?
      role = user_organization_roles.where(:default_name=>role_name).first
      self.user_roles.where(role_id: role.id).destroy_all
    end
  end

  # Validate first role is exist for that user
  # user.has_any_user_roles?("customer")
  def has_any_user_roles?(role)
    if role.is_a?(String)
      roles.pluck(:default_name).include?(role)
    elsif role.is_a?(Role)
      roles.include?(role)
    else
      false
    end
  end

  #  Get all user organization roles
  def user_organization_roles
    organization.roles
  end

  # Check role is exist or not for that organization
  def organization_roles_exist?(role)
    if role.is_a?(String)
      user_organization_roles.pluck(:default_name).include?(role)
    elsif role.is_a?(Role)
      user_organization_roles.include?(role)
    else
      false
    end
  end

  def authorize?(permission_name)
    self.get_user_permissions.include?(permission_name)
  end

  def get_all_user_permissions
    permissions = []
    permissions = self.get_user_permissions
    if !self.organization.enable_role_based_authorization
      can_curate = !Channel.joins(:channels_curators).where(channels_curators: {user_id: self.id}).blank?
      if can_curate
        permissions = permissions + Permissions.get_permission_slugs_for_role(Role::TYPE_CURATOR)
      end
      permissions.uniq
    end
    return permissions.uniq
  end

  def get_user_roles
    roles.pluck(:name) | [organization_role]
  end

  def user_roles_default_names
    roles.pluck(:default_name) | [organization_role]
  end

  def get_user_permissions
    self.permissions.active.pluck(:name)
  end

  # checking that the user has permissions only group_sub_admin
  # and no permissions are more important (without GROUP)
  def group_sub_admin_authorize?(permission_name)
    return false if self.authorize?(Permissions::ADMIN_ONLY)
    return false unless self.is_group_sub_admin?
    check_permission = self.authorize?(permission_name)
    check_permission_priority(permission_name) && check_permission
  end
  # checking that the user has permissions only group_sub_admin
  # and no permissions are more important (without GROUP)
  def sub_admin_authorize?(permission_name)
    return false if self.authorize?(Permissions::ADMIN_ONLY)
    check_permission = self.authorize?(permission_name)
    self.is_sub_admin? && check_permission
  end

  # check that the user does not have a specific permission from the list group_sub_admin
  def exclude_group_permission?(permission_name)
    priority_permissions = check_permission_priority(permission_name)

    join_permissions = join_permission_in_role_group_name(['group_sub_admin'])
    return false unless priority_permissions

    join_permissions.present? ? join_permissions.exclude?(permission_name) : false
  end

  def check_permission_priority(permission_name)
    non_group = permission_name.remove('_GROUP')
    sub_admin_permissions = join_permission_in_role_group_name(['sub_admin'])
    sub_admin_permissions.exclude?(non_group)
  end

  def exists_permission_for_role?(role: nil, permission: nil)
    roles.find_by(default_name: role)&.role_permissions&.exists?(name: permission, enabled: true)
  end
end

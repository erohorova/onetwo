require 'active_support/concern'

module Votable
  extend ActiveSupport::Concern

  included do
    has_many :up_votes, -> { up_votes },
      class_name: 'Vote', as: :votable, :dependent => :destroy
    has_many :upvoted_by, :class_name => 'User',
      through: :up_votes, source: :voter, as: :votable

    def voted_by? user
      upvoted_by.where('users.id' => user.id).present?
    end

    def self.voted_by? question, user
      question.voters && (question.voters.include? user.id)
    end

    def get_vote_action_validity(user, user_votes = nil)
      if self.user == user
        return [false, 'You cannot upvote your own post']
      end

      if user_votes.nil?
        if voted_by? user
          return [false, 'You have already upvoted this post']
        end
      else
        if user_votes.include?(self.id)
          return [false, 'You have already upvoted this post']
        end
      end

      return [true, '']
    end

  end
end

require 'active_support/concern'

module Reportable
  extend ActiveSupport::Concern

  included do

    has_many :reports, as: :reportable, :dependent => :destroy
    has_many :spam_reports, as: :reportable
    has_many :inappropriate_reports, as: :reportable
    has_many :compromised_reports, as: :reportable

    has_many :valid_reports, -> {valid}, as: :reportable, class_name: 'Report'

    def reported_by? user
      reports.where(:reporter => user).present?
    end

    # Currently assumes that all report types have the same validity rules
    def self.get_report_action_validity(user, question)
      # alternate classmethod for hashiemash
      if question.faculty_post
        return [false, 'You cannot report a faculty post']
      end

      if question.reporters.include? user.id
        return [false, 'Reported']
      end

      return [true, '']
    end

    def get_report_action_validity(user, user_reports =  nil)
      if self.group.nil?
        return [true, '']
      end
      if (@created_by_staff.nil? ? self.group.is_active_super_user?(self.user) : @created_by_staff) #if nil then run the query, else use the preload value
        return [false, 'You cannot report a faculty post']
      end

      if user_reports.nil?
        if reported_by? user
          return [false, 'Reported']
        end
      else
        if user_reports.include?(self.id)
          return [false, 'Reported']
        end
      end

      return [true, '']
    end

  end
end


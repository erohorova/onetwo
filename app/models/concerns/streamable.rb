require 'active_support/concern'

module Streamable
  extend ActiveSupport::Concern

  def self.obtain_card_id(streamable)
    case streamable
    when Card
      streamable.id
    when Comment
      streamable.commentable.id if streamable.commentable.is_a?(Card)
    when Vote
      streamable.votable.id if streamable.votable.is_a?(Card)
    when UserContentCompletion
      if streamable.completable.is_a?(Card)
       streamable.completable.id
      else
       streamable.completable.try(:card_id)
      end
    end
  end

  included do
    has_many :activity_streams, as: :streamable, dependent: :destroy

    def is_stream_worthy?
      raise "Error, please define this method"
    end

    def activity_initiator
      raise "Error, please define this method"
    end

    def activity_stream_action
      raise "Error, please define this method"
    end

    def create_activity_stream
      streamable = self.try(:streamable) || self
      self.add_to_activity_stream(streamable, self.activity_initiator, self.activity_stream_action)
    end

    def add_to_activity_stream(streamable, activity_initiator, action)
      if is_stream_worthy?
        args = {
          streamable_id: streamable.id,
          streamable_type: streamable.class.name,
          user_id: activity_initiator.id,
          action: action,
          card_id: Streamable.obtain_card_id(streamable)
        }
        ActivityStreamCreateJob.perform_later(args)
      end
    end
  end


  module ClassMethods
    def acts_as_streamable(options)
      self.after_commit(:create_activity_stream, options)
    end
  end
end

require 'active_support/concern'

module CustomLockable
  extend ActiveSupport::Concern

  included do
    MAX_FAILURE_ATTEMPTS = 5

    def valid_for_authentication?
      return super unless persisted? && account_locking_enabled?

      if super && !account_locked?
        true
      else
        self.failed_attempts ||= 0
        self.failed_attempts += 1
        if attempts_exceeded?
          lock_account! unless account_locked?
        else
          save(validate: false)
        end
        false
      end
    end

    def account_locked?
      !!locked_at
    end

    def lock_account!
      self.locked_at = Time.now.utc
      save(validate: false)
    end

    def unlock_account!
      self.locked_at = nil
      self.failed_attempts = 0 if respond_to?(:failed_attempts=)
      save(validate: false)
    end

    def attempts_exceeded?
      self.failed_attempts >= MAX_FAILURE_ATTEMPTS
    end

    def account_locking_enabled?
      organization.account_lockout_enabled?
    end

    def unauthenticated_message
      if Devise.paranoid
        super
      elsif account_locked? && attempts_exceeded?
        :locked
      else
        super
      end
    end
  end
end
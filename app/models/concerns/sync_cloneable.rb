# frozen_string_literal: true

require 'active_support/concern'

# Used for checking parent association has clone association and if yes then whether it needs update
module SyncCloneable
  extend ActiveSupport::Concern

  included do
    after_commit :sync_clone_card_association, on: [:create, :update]

    # Check whether parent card has cloned cards and if it needs update
    def sync_clone_card_association
      SyncCloneAssociationsJob.perform_later(option_values) if should_perform_sync_job?
    end

    def option_values
      {
        parent_card_id: @card.id,
        sync_association_type: self.class.name
      }
    end

    def should_perform_sync_job?
      case self.class.name
      when 'CardPackRelation' || 'JourneyPackRelation'
        # For journey and pathway check valid for sync against cover card
        @card = cover
      when 'QuizQuestionOption'
        @card = quiz_question
      else
        @card = card
      end
      parent_card_valid_for_sync?
    end

    def parent_card_valid_for_sync?
      @card.cloned_cards.exists? && @card.is_public
    end
  end
end
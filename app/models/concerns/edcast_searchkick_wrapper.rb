require 'active_support/concern'

module EdcastSearchkickWrapper
  extend ActiveSupport::Concern

  included do

    # This method generates the mapping we need for an index,
    # based on searchable_fields, sortable_fields and exact_match_fields
    # The output is something like 
    # {
    #   handle: { index: not_analyzed, type: :string },
    #   tags: {
    #     type: "nested"
    #     name: {
    #       type: "multi_field",
    #       fields: {
    #         raw: { index: :not_analyzed, type: :string },
    #         searchable: { index: :standard, type: :string }
    #       }
    #     }
    #   }
    # }  
    def self.edcast_search_property_mapping
      properties = {}
      _sortable_fields = sortable_fields
      _exact_match_fields = exact_match_fields
      searchable_fields.each do |property, opts|
        search_mapping = opts[:prefix_search_enabled] ? EsAnalyzers::PREFIX_SEARCHABLE_FIELD_MAPPING : EsAnalyzers::SEARCHABLE_FIELD_MAPPING
        # If a field is searchable as well as sortable or exact matchable, then we 
        # need to index it as multiple fields. We use the suffix ".searchable" for a search field, ".sortable" for a field that is sortable, and ".raw" for exact matching
        if _sortable_fields.include?(property) || _exact_match_fields.include?(property)
          fields = {searchable: search_mapping}
          if _sortable_fields.delete(property)
            fields.merge!({sortable: EsAnalyzers::SORTABLE_FIELD_MAPPING})
          end
          if _exact_match_fields.delete(property)
            fields.merge!({raw: EsAnalyzers::EXACT_MATCH_FIELD_MAPPING})
          end

          properties.deep_merge!(EsAnalyzers.format_nested(property, {
            type: 'multi_field',
            fields: fields
          }))
        else
          properties.deep_merge!(EsAnalyzers.format_nested(property, search_mapping))
        end
      end

      # Whatever is left out is just indexed as a sortable field. No multi field indexing needed
      _sortable_fields.each do |property|
        properties.deep_merge!(EsAnalyzers.format_nested(property, EsAnalyzers::SORTABLE_FIELD_MAPPING))
      end

      # Whatever is left out is just indexed as a exact match field. No multi field indexing needed
      _exact_match_fields.each do |property|
        properties.deep_merge!(EsAnalyzers.format_nested(property, EsAnalyzers::EXACT_MATCH_FIELD_MAPPING))
      end

      {
        mappings: {
          _default_: {
            properties: properties
          }
        }
      }
    end

    # Override this method. See card.rb for an example
    def self.searchable_fields
      {}
    end

    # Override this method. See card.rb for an example
    def self.sortable_fields
      []
    end

    # Override this method. See card.rb for an example
    def self.exact_match_fields
      []
    end

    # See https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-multi-match-query.html
    # Given searchable fields: [{message: {boost: 1}, {title: {boost: 10}}],
    # generates ["message^1", "title^10"]
    def self.searchable_fields_query_format
      fields = []
      searchable_fields.each do |k, v|
        if (sortable_fields | exact_match_fields).include?(k)
          fields << "#{k}.searchable^#{v[:boost] || 1}"
        else
          fields << "#{k}^#{v[:boost] || 1}"
        end
      end
      fields
    end

    def self.sortable_field_mapping(field)
      searchable_fields.has_key?(field.to_s) ? "#{field}.sortable" : field
    end

    def self.exact_match_field_query_mapping(field)
      searchable_fields.has_key?(field.to_s) ? "#{field}.raw" : field
    end

    # Initializes searchkick with appropriate mapping
    # Accepts the same hashmap as searchkick in case you need to override
    # settings. See user.rb for an example of overriding default settings
    def self.edcast_searchkick opts={}
      searchkick(EsAnalyzers::DEFAULT_SEARCHKICK_OPTS.deep_merge(edcast_search_property_mapping).deep_merge(opts))
    end

  end
end


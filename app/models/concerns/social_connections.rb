require 'active_support/concern'

module SocialConnections
  extend ActiveSupport::Concern

  included do

    has_many :user_connections, :class_name => "UserConnection", :foreign_key => "connection_id", :dependent => :delete_all
    has_many :connections, :through => :user_connections, :source => :user

    has_many :facebook_connections_join, -> { facebook },
      :class_name => "UserConnection", :foreign_key => "connection_id"
    has_many :facebook_connections, :through => :facebook_connections_join, :source => :user

    has_many :linkedin_connections_join, -> { linkedin },
      :class_name => "UserConnection", :foreign_key => "connection_id"
    has_many :linkedin_connections, :through => :linkedin_connections_join, :source => :user



    # always use all_*_connections instead of the defined relations to read
    # this is needed because the relations only return users that are friends of given user
    # but these functions return users that are friends of given users as well as
    # all the users that the current user is a friend of.
    def all_facebook_connections
      all_connections('facebook')
    end

    def all_linkedin_connections
      all_connections('linkedin')
    end

    def all_connections_ids(network = nil)
      ucs= UserConnection.where('user_id = ? OR connection_id = ?', id, id)
      if network
        ucs.where!(network: network)
      end
      ucs.map{|uc| [uc.user_id, uc.connection_id]}.flatten.uniq - [id]
    end

    def all_connections(network = nil)
      User.where(id: all_connections_ids(network))
    end

    def all_connection_ids_in_org(network = nil)
      all_connections_in_org(network).where(users: {is_suspended: false}).pluck(:id)
    end

    def all_connections_in_org(network = nil)
      all_connections(network).where(organization: organization)
    end

    def all_connections_in_group(group)
      all_connections & User.where(id: GroupsUser.where(group: group).not_banned.pluck(:user_id))
      all_connections.select{|c| group.is_active_user?(c)}
    end

    def facebook_connection? user
      all_facebook_connections.pluck(:id).include? user.id
    end

    def linkedin_connection? user
      all_linkedin_connections.pluck(:id).include? user.id
    end

    def connection? user
      all_connections.pluck(:id).include? user
    end

    def facebook_connections_update connections
      self.connection_update connections, 'facebook'
    end

    def linkedin_connections_update connections
      self.connection_update connections, 'linkedin'
    end

    def connection_update connections, network
      network_connection_ids = self.send("#{network}_connections").pluck(:id)

      new_connections = connections - network_connection_ids
      dropped_connections = network_connection_ids - connections

      self.send("#{network}_connections") << User.find(new_connections)
      self.send("#{network}_connections").destroy(User.find(dropped_connections))
    end
  end
end

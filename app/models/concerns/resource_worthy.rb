require 'active_support/concern'

module ResourceWorthy
  extend ActiveSupport::Concern

  included do
    belongs_to :resource
    
    after_create :add_resource

    def add_resource
      self.resource = Resource.extract_resource(_resource_extraction_field)
      self.save
    end

    def update_resource
      add_resource
    end

  end
end

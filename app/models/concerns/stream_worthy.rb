require 'active_support/concern'

module StreamWorthy
  extend ActiveSupport::Concern
  included do
  end

  module ClassMethods

    def remove_from_stream(item: nil, action: nil)
      before_destroy do |record|
        _user_id = nil
        if record.respond_to?(:user_id)
          _user_id = record.user_id
        end

        _item = nil
        if item && item.respond_to?(:call)
          _item = item.call(record)
        else
          _item = record
        end

        Stream.where(action: action, user_id: _user_id, item: _item).destroy_all
      end
    end

    def add_to_stream(group_id: nil, item: nil, action: 'create', **options)
      after_commit on: :create do |record|
        unless record.class == Comment && record.commentable_type != 'Post' && record.commentable_type != 'Comment'
          unless record.respond_to?(:anonymous) && record.anonymous
            _group_id = nil
            if group_id && group_id.respond_to?(:call)
              _group_id = group_id.call(record)
            elsif record.respond_to?(:group_id)
              _group_id = record.group_id
            end
            _user_id = nil
            if record.respond_to?(:user_id)
              _user_id = record.user_id
            end
            _item = nil
            if item && item.respond_to?(:call)
              _item = item.call(record)
            else
              _item = record
            end

            skip = options[:if] && options[:if].respond_to?(:call) && !options[:if].call(_item)
            unless skip
              StreamCreateJob.perform_later(action: action, item_id: _item.id, item_type: _item.class.name, created_at: Time.now.utc.to_s(:db), user_id: _user_id, group_id: _group_id)
            end
          end
        end
      end
    end
  end
end

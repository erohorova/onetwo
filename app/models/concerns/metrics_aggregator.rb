require 'active_support/concern'

module MetricsAggregator
  extend ActiveSupport::Concern

  included do

    class << self

      def update_from_increments(increments: , timestamp: , opts: )
        PeriodOffsetCalculator.applicable_offsets(timestamp).each do |period, offset|
          inst = find_or_initialize_by(opts.merge({period: period, offset: offset}))
          inst.with_lock do
            increments.each do |attr, increment|
              inst.increment(attr, increment)
            end
            inst.save!
          end
        end
      end

      def create_increment_methods(metrics)
        singleton_class.instance_eval do
          metrics.each do |metric|
            define_method("increment_#{metric}") do |opts, increment, timestamp|
              PeriodOffsetCalculator.applicable_offsets(timestamp).each do |period, offset|
                inst = find_or_initialize_by(opts.merge({offset: offset, period: period}))
                inst.with_lock do
                  inst.increment(metric, increment)
                  inst.save!
                end
              end
            end
          end
        end
      end

      def create_getter_methods(metrics)
        singleton_class.instance_eval do
          metrics.each do |metric|
            define_method("get_#{metric}") do |opts, period, offset|
              where(opts.merge({period: period, offset: offset})).first.try(metric) || 0
            end
          end
        end
      end
    end
  end
end

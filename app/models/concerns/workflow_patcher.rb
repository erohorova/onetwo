# frozen_string_literal: true
require 'json-schema'
module WorkflowPatcher
  extend ActiveSupport::Concern

  SOURCES = %w[profile dynamic-selections-input-source]

  ACTION_TYPES = %w[upsert delete alter-schema]

  ENTRIES = %w[User Profile UserCustomField CustomField]

  WHITE_LISTED_USER_FIELDS = %w[first_name last_name status email is_suspended is_anonymized]

  # need to keep schema on edc-workflow up to date
  # TODO: create a storage system and detection for schema changes
  def detect_upsert(import: false)
    return if skip_processing
    action = tr_action_type
    changes = self.previous_changes
    ## start payload assigning
    payload = case self.class.name
    when 'CustomField'
      # alter-schema for CF
      if action == 'create'
        org = self.organization
        schema_payload(
          PatchDataMapper.took_columns_mapper(org),
          org,
          'profile',
          'alter-schema'
        )
      end
    when 'Profile', 'User', 'UserCustomField'
      return if self.is_a?(User) && action == 'create'
      usr = self.is_a?(User) ? self : self.try(:user)
      status_changed = changes['status'].try(:last) == User::SUSPENDED_STATUS
      status_changed = true if usr.suspended? || usr.is_anonymized
      # if user was suspended req as `delete` type
      if status_changed
        schema_payload(
          PatchDataMapper.row_mapper(usr),
          usr.organization,
          'profile',
          'delete'
        )
      else
        # skip if no updates
        return if action == 'update' && changes.blank? && !self.try(:workflow_import)
        # skip patch if updated not white listed user fields
        if self.is_a?(User) && action == 'update' && !self.try(:workflow_import)
          return unless (changes.keys & WHITE_LISTED_USER_FIELDS).any?
        end
        # skip UserCustomField creation without value
        return if self.is_a?(UserCustomField) && action == 'create' && changes['value'].nil?
        schema_payload(
          PatchDataMapper.row_mapper(usr),
          usr.organization,
          'profile',
          'upsert'
        )
      end
    end
    ## end payload assigning
    
    return unless payload
    org = Organization.find_by(host_name: payload[:orgName])

    unless org
      log.warn(<<-TXT)
        Organization not found. Host name: #{payload[:orgName]}
      TXT
      return
    end
    return payload if import
    WorkflowPatchSaverJob.perform_later(data: payload, org_id: org.id)
  end

  def detect_delete
    return if skip_processing
    return unless transaction_include_any_action?([:destroy])
    payload = case self.class.name
    when 'CustomField'
      # trigger CF deletion to pass alter-schema req
      org = self.organization
      schema_payload(
        PatchDataMapper.took_columns_mapper(org),
        org,
        'profile',
        'alter-schema'
      )
    when 'Profile', 'User'
      usr = self.is_a?(User) ? self : self&.user
      # trigger user/profile deletion
      schema_payload(
        PatchDataMapper.row_mapper(usr),
        usr.organization,
        'profile',
        'delete'
      )
    else
      nil
    end
    return unless payload
    org = Organization.find_by(host_name: payload[:orgName])

    unless org
      log.warn(<<-TXT)
        Organization not found. Host name: #{payload[:orgName]}
      TXT
      return
    end

    WorkflowPatchSaverJob.perform_later(data: payload, org_id: org.id)
  end

  def schema_payload(data, org, id, type)
    return unless id.in?(WorkflowPatcher::SOURCES)
    return unless type.in?(WorkflowPatcher::ACTION_TYPES)
    {
      edcastSourceId: id,
      orgName: org.host_name,
      patchType: type,
      patchTimestamp: Time.current.strftime('%s%3N').to_i,
      data: data
    }
  end

  def tr_action_type
    return 'create' if transaction_include_any_action?([:create])
    return 'update' if transaction_include_any_action?([:update])
    'destroy' if transaction_include_any_action?([:destroy])
  end

  def skip_processing
    return true unless self.try(:organization)&.workflow_service_enabled?
    usr = self.is_a?(User) ? self : self.try(:user)
    if usr
      return false if usr.try(:workflow_import)
      return true if (usr.try(:bulk_import) || self.try(:bulk_import))
    end
    false
  end

  class PatchDataMapper
    extend WorkflowConcern

    class << self

      def took_columns_mapper(org_id)
        columns_mapper('profile', org_id)
      end

      def row_mapper(usr)
        profile = usr.external_profile
        assigned_custom_fields = usr.assigned_custom_fields
          .includes(:custom_field)

        row_values = if profile
          {
            profile_id: profile.id.to_s,
            profile_external_id: profile.external_id.to_s,
            profile_uid: profile.uid,
            id: usr.id.to_s,
            user_email: usr.email,
            user_first_name: usr.first_name,
            user_last_name: usr.last_name,
            user_status: usr.status,
            user_created_at: usr.created_at.strftime('%s%3N').to_i
          }
        else
          {
            id: usr.id.to_s,
            user_email: usr.email,
            user_first_name: usr.first_name,
            user_last_name: usr.last_name,
            user_status: usr.status,
            user_created_at: usr.created_at.strftime('%s%3N').to_i
          }
        end

        assigned_custom_fields.each do |assigned_custom_field|
          custom_field = assigned_custom_field.custom_field
          custom_field_value = assigned_custom_field.value.presence
          next unless custom_field_value
          key = 'custom_' + custom_field.abbreviation
          row_values[key.to_sym] = custom_field_value
        end

        row_values
      end
    end
  end
end

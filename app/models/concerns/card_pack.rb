module CardPack
  extend ActiveSupport::Concern
  included do
    has_many :card_pack_relations, foreign_key: :cover_id
    attr_accessor :topics

    def is_pack?
      @is_pack ||= (card_type == "pack")
    end
  end
end

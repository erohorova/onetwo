require 'active_support/concern'

module Mentionable
  extend ActiveSupport::Concern

  included do

    has_many :mentions, as: :mentionable, dependent: :destroy
    has_many :mentioned_users, through: :mentions, class_name: 'User', foreign_key: :user_id, source: 'user'

    def extract_mentioned_users
      mus = []
      org = self.organization.try(:id)
      if self.respond_to?(:message)
        mus << Mention.extract_mentioned_users(self.message, org)
      end

      if self.respond_to?(:title)
        mus << Mention.extract_mentioned_users(self.title, org)
      end
      mus
    end

    def add_mentions
      self.mentioned_users << extract_mentioned_users
    end

    def update_mentions
      mentions.destroy_all
      self.mentioned_users << extract_mentioned_users
    end
  end
end

require 'active_support/concern'

module Assignable
  extend ActiveSupport::Concern
  included do

    def create_assignment_for_teams(team_ids: [], assignor: , opts: {})
      teams = if assignor.is_org_admin?
        Team.where(organization_id: assignor.organization_id, id: team_ids)
      else
        assignor.organization.teams.where(id: team_ids)
      end
      
      teams.each do |team|
        if team.is_admin?(assignor) || assignor.is_admin_user?
          TeamAssignmentJob.perform_later(
            team_id: team.id,
            assignor_id: assignor.id,
            assignee_ids: [],
            assignable_id: self.id,
            assignable_type: self.class.name,
            opts: opts
          )
        end
        # create UsersCardsManagement when assigning to the teams
        next unless author != assignor
        UsersCardsManagementJob.perform_later(assignor.id, id, UsersCardsManagement::ACTION_TEAM_ASSIGNMENT, team.id)
      end
    end

    def create_assignment_for_individuals(assignee_ids: [], assignor:, opts: {})
      IndividualAssignmentJob.perform_later(
        assignee_ids: assignee_ids,
        assignor_id: assignor.id,
        assignable_id: self.id,
        opts: opts
      )
    end

    def create_assignment(user:, assignor:, assignable: nil, opts: {})
      assignable ||= self
      assignment = Assignment.where(assignee: user, assignable: assignable).first_or_initialize
      assignment.due_at = Date.strptime(opts[:due_at], "%m/%d/%Y").end_of_day if opts[:due_at]
      assignment.start_date = Date.strptime(opts[:start_date], "%m/%d/%Y").beginning_of_day if opts[:start_date]
      assignment.started_at = Date.strptime(opts[:started_at], "%m/%d/%Y").end_of_day if opts[:started_at]
      assignment.completed_at = Date.strptime(opts[:completed_at], "%m/%d/%Y").beginning_of_day if opts[:completed_at]
      if assignment.dismissed?
        assignment.re_assign
      end

      if assignment.save
        team_assignment = TeamAssignment.where(assignor: assignor, assignment: assignment, message: opts[:message]).first_or_initialize
        team_assignment.skip_email_notifications = true if opts[:skip_mail]
        team_assignment.save

        if (assignment.user_id != assignor.id) && !opts[:skip_notifications]
          EDCAST_NOTIFY.trigger_event(Notify::EVENT_NEW_ASSIGNMENT, assignment) 
        end
        # create UsersCardsManagement for self or individual assignment
        if assignable.author != assignor
          action = UsersCardsManagement::ACTION_SELF_ASSIGNMENT
          action = UsersCardsManagement::ACTION_USER_ASSIGNMENT if user != assignor
          UsersCardsManagementJob.perform_later(assignor.id, assignable.id, action, user.id)
        end

        assignment
      end

    end

    alias_method :update_assignment, :create_assignment

  end
end

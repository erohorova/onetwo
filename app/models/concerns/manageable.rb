# frozen_string_literal: true

require 'active_support/concern'

# the main purpose: tracking cards which is not authored by the User,
#                   but these cards are used in the User content.
# Data is lying in UsersCardsManagement(UCM)
module Manageable
  extend ActiveSupport::Concern

  included do

    # check if UsersCardsManagement should be created
    def is_manageable?
      raise 'Error, please define this method'
    end

    # get appropriate parameters for the creation of UCM
    # e.g. [user_id, card_id, action, target_id]
    def get_ucm_values
      raise 'Error, please define this method'
    end

    def get_journey_ucm_values
      raise 'Error, please define this method'
    end

    def create_management
      return unless is_manageable?
      jpr = JourneyPackRelation.find_by(from_id: id)
      values = (jpr.present? ? get_journey_ucm_values : get_ucm_values)
      return unless values.all?
      UsersCardsManagementJob.perform_later(*values)
    end

    # for now it used only for deletion UCM which related to pathway/journey actions
    # after removing of the card from pathway or journey section it should be deleted from UCM content
    def destroy_management
      return unless is_manageable?
      jpr = JourneyPackRelation.find_by(from_id: id)
      options = (jpr.present? ? get_journey_ucm_values : get_ucm_values)
      user_id, card_id, action, target_id = options
      return unless UsersCardsManagement.exists?(user_id: user_id, card_id: card_id,
                                                 action: action, target_id: target_id)

      UsersCardsManagementJob.perform_later(*options, opts: 'destroy')
    end
  end

  class_methods do
    def acts_as_manageable(options)
      return unless options.is_a?(Hash) && options[:on]&.in?(%i[create])
      after_commit(:create_management, options)
    end
  end
end

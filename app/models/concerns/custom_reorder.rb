# frozen_string_literal: true

require 'active_support/concern'

# reordering of objects for relations similar to LinkedList structure
# CardPackRelation or JourneyPackRelation
module CustomReorder
  extend ActiveSupport::Concern

  included do

    RELATION_MODELS = { pack: CardPackRelation, journey: JourneyPackRelation }.freeze
    RELATION_METHODS = { pack: 'pack_relations', journey: 'journey_relations' }.freeze

    def relations_reorder(order:)
      # take relations for acceptable models (CardPackRelation, JourneyPackRelation)
      current_rel = relations(self)
      return false if current_rel.blank?
      new_rel_order = [current_rel[0]]
      order.each { |i| new_rel_order.push(current_rel.detect { |obj| obj.from_id == i }) }
      # we keep deleted objects in tail of new_rel_order
      deleted_obj = current_rel - new_rel_order
      new_rel_order.push(*deleted_obj) if deleted_obj.present? && deleted_obj.all?(&:deleted)

      # start reorder
      return false if new_rel_order.uniq.length != current_rel.length
      safe_transaction(preparing_obj(new_rel_order.uniq))
    end

    def relations(card)
      return [] unless card.card_type
      # prepare array of purified AR objs
      RELATION_MODELS[card.card_type.to_sym]
        .send(RELATION_METHODS[card.card_type.to_sym], cover_id: card.id)
    end

    def preparing_obj(new_rel_order)
      order = new_rel_order
      case order[0].class.name
      when 'JourneyPackRelation'
        start_date = order[0].start_date
        order.each_with_index do |obj, index|
          obj.to_id = new_rel_order[index + 1]&.id
          obj.start_date = start_date
          start_date = start_date.try { |i| i + 7.days } unless index.zero?
        end
      else
        order.each_with_index do |obj, index|
          obj.to_id = order[index + 1]&.id
        end
      end
    end

    def safe_transaction(rel)
      rel[0].class.transaction do
        rel.each { |r| r.save validate: false }
        status = rel.map(&:valid?).all?
        raise ActiveRecord::Rollback unless status
        status
      end
    end
  end

end
module EventTracker

  # NOTE: we have had a recurring issue in the QA environment where
  # sidekiq gets backed up and it becomes difficult to test analytics events.
  # This environment variable can be set to record events to Influx immediately
  # as they occur in the application.
  # However, this should only be used on the QA environment because it will
  # slightly degrade the performance of our APIs.
  SKIP_SIDEKIQ = ENV["SKIP_SIDEKIQ_FOR_INFLUX_ANALYTICS"]&.downcase == "true"

  extend ActiveSupport::Concern

  # Instance method: record_custom_event
  # Class method: :configure_metric_recorders
  # Class method: track_create_event
  # Class method: track_delete_event
  # Class method: track_edit_event

  # EXAMPLE USAGE:
  # configure_metric_recorders(
  #   default_actor: :author,
  #   trackers: {
  #     on_create: {
        # if_block: lambda {|record| record.published?},
        # job_args: [{org: :organization}],
        # exclude_job_args: [:org],
        # event_opts: lambda {|record| {journey_id: record.cover_id} },
  #       event: 'card_created'
  #     },
  #     on_edit: {
  #       event: 'card_edited',
  #     },
  #     on_delete: {
  #       event: 'card_deleted',
  #     }
  #   }
  # )

  included do
    attr_accessor :recorder, :event, :default_actor,
      :org, :metric_recorder

    def record_custom_event(**args)
      set_attrs!(args)
      push_to_background_job
    end

    private
    # Generic method that finds actor based on request metadata and
    # enqueue event job
    def push_to_background_job(options: {})
      return unless @if_block_given
      return unless @recorder

      metadata =  RequestStore.read(:request_metadata) || {}
      metadata.merge! @event_opts if @event_opts
      @actor = User.find_by(id: metadata[:user_id]) || @actor

      unless @actor
        log.warn "Actor not found for event #{event} with Id: #{metadata[:user_id]}"
        return
      end

      job_args = options
        .merge(additional_data: metadata)
        .merge(background_job_args)
        .except(*@exclude_job_args)

      store_data_in_cache(job_args)

      if EventTracker::SKIP_SIDEKIQ
        Analytics::MetricsRecorderJob.perform_now job_args
      else
        Analytics::MetricsRecorderJob.perform_later job_args
      end
    end

    # This instance method, finds all changes on model and
    # enqueue job with additional args
    def record_changes_on_model
      yield if block_given?
      return if changes.empty?
      return unless @recorder

      sanitized_changes = Analytics::MetricsRecorder.sanitize_times_in_changes(changes)
      sanitized_changes.each do |changed_column, (old_val, new_val)|
        # We don't want to push updates for every changed column.
        next if @recorder.is_ignored_event?(changed_column)

        # We don't want to push sensitive information about sso configuration to influx
        # Below line ignore changes doene to saml certificate.
        if @recorder.respond_to?(:sensitive_info_keys) &&
          @recorder.sensitive_info_keys.include?(changed_column)
          old_val, new_val = "", ""
        end

        push_to_background_job(options: {
          changed_column: changed_column.to_s,
          old_val: old_val&.to_s,
          new_val: new_val&.to_s,
        })
      end
    end

    def store_data_in_cache(data)
      Analytics::LastActivityRecorder.new(user_id: @actor.id)
        .add_data_to_cache(
          event: @event, 
          value: data.except(*[:event,:org,:actor,:additional_data,:recorder])
        )
    end

    # THIS TAKES A OBJECT AND LOAD ALL MODEL ATTRIBUTES NEEDED FOR RECORDERS
    # EX card oject, would invoke card_attributes method
    def load_klass_attributes_for_metric(_object_)
      begin
        _class_attributes_method = "#{_object_.class.name.underscore}_attributes".to_sym
        return false unless Analytics::MetricsRecorder.respond_to?(_class_attributes_method)
        Analytics::MetricsRecorder.send _class_attributes_method, _object_
      rescue => e
        log.error "No method found for object #{_object_.class.name}"
      end
    end

    # It prepares job args based on various options set in set_attrs!
    # some of the things come from model config. Ex job_args
    # Object in job args is a combination of key(which will be passed to job)
    # and value would be a method that must respond on model instance
    # ex job_args: [{key: :card, value: :author}]
    def background_job_args
      {
        recorder: @recorder.to_s,
        event: @event,
        actor: load_klass_attributes_for_metric(@actor),
        org: load_klass_attributes_for_metric(resolve_org),
        timestamp: Time.now.utc.to_i,
      }.tap do |args|
        @job_args.each do |_hash_|
          _hash_.each do |key, value|
            args[key] = get_job_arg_attributes(value)
          end
        end
      end
    end

    # symbol_or_object => :card, :author or any symbol referenced to method that
    # would return object attributes. Ex card_attributes in metrics recorder
    def get_job_arg_attributes(symbol_or_object)
      if symbol_or_object.is_a?(Symbol) && respond_to?(symbol_or_object)
        load_klass_attributes_for_metric(send(symbol_or_object)) || send(symbol_or_object)
      else
        load_klass_attributes_for_metric(symbol_or_object) || symbol_or_object
      end
    end

    # This can be done in better way, :)
    # This sets all instance variable and make it available to background job
    # recorder, event and job_args are mandatatory
    def set_attrs!(args)
      raise "Event is not provided" unless args[:event]

      @if_block_given = resolve_if_block(args[:if_block])
      @actor          = get_actor_for_event(args[:actor])
      @event          = args[:event]
      @event          = @event.call(self) if @event.is_a?(Proc)
      @recorder       = Analytics::MetricsRecorder.get_recorder_for_event(@event)
      @job_args       = args[:job_args] || [ { self.class.name.underscore.to_sym => :itself } ]
      @job_args       = @job_args.call(self) if @job_args.is_a?(Proc)
      @event_opts     = args[:event_opts]&.call(self) || {}
      @exclude_job_args = args[:exclude_job_args] || []
      self #it is used as part of `if:` block.
    end

    def resolve_org
      respond_to?(:organization) ? send(:organization) : @actor.organization
    end

    def resolve_if_block(if_block)
      return true unless if_block
      return send(if_block) if if_block.is_a? Symbol
      return if_block&.call(self) if if_block.is_a? Proc
    end

    def get_actor_for_event(_actor)
      # 1. if it's symbol, fetch by calling method on model
      # 2. if Proc, call proc within model scope
      # 3. if class level default actor defined, call default_actor
      # 4. return nil and event won't get recorded
      case _actor
      when Symbol
        send(_actor)
      when Proc
        _actor&.call self
      else
        self.class.default_actor&.call(self) || nil
      end
    end
  end

  module ClassMethods
    attr_accessor :default_actor

    # Generic class method that takes standard model
    # create/edit/destroy events
    # Ex usage, pls see models/cards.rb
    def configure_metric_recorders(**args)
      set_default_actor args[:default_actor]

      track_create_event(args[:trackers][:on_create]) if args[:trackers][:on_create]
      track_delete_event(args[:trackers][:on_delete]) if args[:trackers][:on_delete]
      track_edit_events(args[:trackers][:on_edit]) if args[:trackers][:on_edit]
    end

    # Track model record create event
    def track_create_event(**args)
      after_commit :push_to_background_job, on: :create, if: -> (record) { set_attrs!(args) }
    end

    # track model record delete event
    def track_delete_event(**args)
      before_destroy do
        set_attrs!(args)
        push_to_background_job
      end
    end

    # track model record edit event
    def track_edit_events(**args)
      before_update do
        set_attrs!(args)
      end
      around_update :record_changes_on_model
    end

    # sets the default actor for all 3 events above
    def set_default_actor(actor)
      self.default_actor = if actor.is_a?(Symbol)
        -> (record) { record.send actor }
      elsif actor.is_a?(Proc)
        -> (record) { actor.call(record) }
      else
        raise ArgumentError
      end
    end
  end
end
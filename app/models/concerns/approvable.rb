require 'active_support/concern'

module Approvable
  extend ActiveSupport::Concern

  included do
    has_one :approval, class_name: 'Approval', as: :approvable, dependent: :destroy

    def has_approval?
      Approval.where(:approvable => self).present?
    end

    def self.has_approval? question
      question.approver || false
    end
  end
end

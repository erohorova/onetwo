# frozen_string_literal: true

# =========================================
# Note: this module is extended to the Team model,
# and defines the class-level Team.fetch_v2_analytics method.
#
# =========================================

module TeamModelLeaderboardV2Concern

  # Makes #group_by_sequence and #bury available at the top level in this file
  using ObjectRefinements::GroupBySequence
  using ObjectRefinements::Bury

  # A custom error class which gets rescued by the controller
  class ParameterError < StandardError; end

  # The entry method which gets called by the controller
  def fetch_v2_analytics(params:, current_org:)
    TeamModelLeaderboardV2Concern::Service.fetch_v2_analytics(
      params: params,
      current_org: current_org
    )
  end

  # This is a static class which contains the private methods,
  # to avoid polluting the Team namespace directly.
  class Service

    # Provides the .extract_sort_order method here
    extend SortOrderExtractor

    # Provides the .cache_get_set method
    extend CacheUtil

    # Provides the .add_null_records_for_empty_days! method
    extend AggregationQueryUtil

    class << self

      # Checks the cache to see if there's data for this param set plus org.
      # If so, returns that data. If not, queries for the data and stores
      # it in the cache.
      def fetch_v2_analytics(params:, current_org:)
        cache_key = build_cache_key(params: params, current_org: current_org)
        cache_get_set(cache_key, expires_in: 30.minutes) do
          get_results(params: params, current_org: current_org)
        end
      end

      private

      # Method that is called if the cache does not have the results already.
      # Fires multiple SQL queries and returns an array of team metrics
      def get_results(params:, current_org:)
        query_params = build_query_params(params, current_org)
        query_results = build_and_send_queries(query_params)
        combined_results = combine_results(query_results)
        # The controller expects a record for each day in the period.
        # We manually fill the "daily_records" results with
        # 0-activity records for days in which there is no activity.
        combined_results.values.flat_map(&:values).each do |record|
          add_null_records_for_empty_days!(
            results: record["daily_records"],
            start_time: query_params[:from_date],
            end_time: query_params[:to_date],
          ) do |obj, start_time:, end_time:|
            obj[start_time] = default_record.merge(
              "start_time" => start_time,
              "end_time" => end_time,
              "engagement_index" => 0,
              "new_users" => 0
            ).except("daily_records")
          end
        end
        flattened_results = flatten_and_sort_results(combined_results, query_params)
        merge_total_period_time_bounds!(flattened_results, query_params)
        merge_average_session_for_total_period!(flattened_results, query_params)
        merge_engagement!(flattened_results)
        flattened_results
      end

      # Get a cryptographic hash of the params and current org
      def build_cache_key(params:, current_org:)
        meaningful_params = params.slice *%i{
          from_date to_date order order_attr team_ids limit offset
        }
        [
          "organizations",
          current_org.id,
          "team-leaderboard",
          meaningful_params.hash
        ].join("/")
      end

      # Given the results of the separate queries as a hash ("combined"),
      # flattens them into a array and applies sorting.
      # Note that the sorting is applied to SQL queries elsewhere, but that
      # is only to determine which records get included in the results.
      # By this point, the exact order may have been lost, but we can easily
      # add it back by calling #sort_by on the array.
      def flatten_and_sort_results(combined, query_params)
        results = combined.values.flat_map(&:values).map do |record|
          new_record = default_record.merge record.compact
          new_record["daily_records"] = new_record["daily_records"]
            .values
            .sort_by { |daily_record| daily_record["start_time"] }
          new_record
        end
        sorted = results.sort_by do |result|
          result[query_params[:order_attr]]
        end
        query_params[:order].downcase == "desc" ? sorted.reverse : sorted
      end

      # The default attributes for a record. For days that fall within the 
      # requested range that have no activity, a modified version of this object
      # is used.
      def default_record
        {
          "smartbites_comments_count"  => 0,
          "smartbites_created"         => 0,
          "smartbites_consumed"        => 0,
          "smartbites_liked"           => 0,
          "total_users"                => 0,
          "active_users"               => 0,
          "time_spent"                 => 0,
          "daily_records"              => {}
        }
      end

      # Combines multiple result hashes into a single hash.
      # The result is a nested hash indexed by org id, team id, and,
      # within the 'daily_records' key, start time.
      def combine_results(results)
        combined = {}
        results[:daily_period].each do |org_id, org_records|
          org_records.each do |team_id, team_records|
            team_records.each do |start_time, daily_records|
              # There should only one record in each daily_records list here,
              # but we treat it like an array anyway
              daily_records.each do |daily_record|
                bury!(
                  combined,
                  [org_id, team_id, "daily_records", start_time],
                  # We exclude these keys from the daily records.
                  # They're either irrelevant or available elsewhere in the data.
                  daily_record.except(*%w{
                    created_at updated_at id organization_id team_id
                  })
                )
              end
            end
          end
        end
        results[:total_period].each do |_name, total_period_section|
          total_period_section.each do |org_id, org_records|
            org_records.each do |team_id, team_records|
              # There should only one record in each team_records list here,
              # but we treat it like an array anyway
              team_records.each do |team_record|
                bury!(
                  combined,
                  [org_id, team_id],
                  team_record
                ) do |record, key, val|
                  record[key] ||= {}
                  record[key].merge!(val)
                end
              end
            end
          end
        end
        combined
      end

      # Builds and sends SQL queries using the given parameters.
      # We build 4 queries here:
      #
      # 1. All columns grouped by day (this was previous referred to as 'drilldown')
      #
      # 2. Aggregate over total period for additive and averaged columns
      #    These are smartbites_created, smartbites_consumed,
      #    num_likes, and num_comments, and time_spent
      #    Note we calculate average_session separately
      #
      # 3. Total unique users over total period
      #
      # 4. Total unique active users over total period 
      #
      # We then add engagement_index using Ruby separately.
      #
      # Note that the teams which are included in the result are determined by
      # the query at step 2. Order, limit, and offset is only applied to this
      # query.
      #
      # The other queries add additional data for the teams returned by the
      # step 2 query.
      #
      def build_and_send_queries(query_params)
        results = {}

        total_period_sums_query = build_total_period_column_sums_query(
          query_params
        )

        total_period_column_sums = group_by_sequence(
          Group
            .find_by_sql([
              total_period_sums_query,
              query_params
            ])
            .map(&:attributes),
            ["organization_id", "team_id"]
        )
        results[:total_period] = {}
        results[:total_period][:column_sums] = total_period_column_sums
        team_ids = results[:total_period][:column_sums].values.flat_map(&:keys)

        # We take the team_ids from the first query, and apply them to
        # the remaining queries.
        #
        # We also remove the order, order_attr, limit, and offset params
        # for the remaining queries.
        subsequent_query_params = query_params
          .merge(team_ids: team_ids)
          .except(:order, :order_attr, :limit, :offset)

        results[:total_period].merge!(
          total_users: group_by_sequence(
            Group
              .find_by_sql([
                build_total_period_unique_users_query(subsequent_query_params),
                subsequent_query_params
              ])
              .map(&:attributes),
            ["organization_id", "team_id"]
          ),
          active_users: group_by_sequence(
            Group
              .find_by_sql([
                build_total_period_unique_active_users_query(subsequent_query_params),
                subsequent_query_params
              ])
              .map(&:attributes),
            ["organization_id", "team_id"]
          )
        )

        results[:daily_period] = group_by_sequence(
          Group
            .find_by_sql([
              build_daily_period_query(subsequent_query_params),
              subsequent_query_params
            ])
            .map(&:attributes),
          ["organization_id", "team_id", "start_time"]
        )

        results
      end

      def merge_engagement!(results)
        results.each do |result|
          engagement = (
            result["active_users"].to_f / [result["total_users"], 1].max
          ).round(2)
          result["engagement_index"] = engagement
        end
      end

      # The definition for "average_session" is actually just the average
      # "time_spent_minutes" (not my decision to call it this).
      #
      # We cannot simply use the SQL "AVG" function because it will not take
      # into account days on which there is no activity (and therefore no
      # "0 time spent" record).
      #
      # Rather, we use SQL to select the time spent per day, and determine
      # the average manually based on the number of days in the period.
      #
      def merge_average_session_for_total_period!(results, query_params)
        num_days_in_period = (
          (
            Time.at(query_params[:to_date]) -
            Time.at(query_params[:from_date])
          ) / 60 / 60 / 24
        ).round
        results.each do |result|
          average_session = result["daily_records"].sum do |record|
            record["time_spent"].to_f
          end / num_days_in_period
          result["average_session"] = average_session.round(2)
        end
      end

      # Merge the :from_date and :to_date to the results.
      # These are top-level keys that show the dates containing all the
      # queried data.
      def merge_total_period_time_bounds!(results, query_params)
        results.each do |result|
          result["from_date"] = query_params[:from_date]
          result["to_date"] = query_params[:to_date]
        end
      end

      # Request per-day data for a specific list of teams.
      # This is the "drilldown" data.
      def build_daily_period_query(query_params)
        team_filter = if query_params[:team_ids]&.any?
          "AND team_id IN (:team_ids)"
        end
        <<-SQL.strip_heredoc
          SELECT
            smartbites_created,
            smartbites_consumed,
            num_comments AS smartbites_comments_count,
            num_likes AS smartbites_liked,
            time_spent_minutes AS time_spent,
            total_users,
            active_users,
            new_users,
            engagement_index,
            start_time,
            end_time,
            team_id,
            organization_id
          FROM
            group_metrics_aggregations
          WHERE
            organization_id = :org_id
          #{team_filter}
          AND
            start_time >= :from_date
          AND
            end_time <= :to_date
        SQL
      end

      # Requests the total-period data for all teams.
      # Applies pagination and ordering to only show a subset of the results.
      # The team ids here are used in a filter for the other queries.
      def build_total_period_column_sums_query(query_params)
        team_filter = if query_params[:team_ids]&.any?
          "AND team_id IN (:team_ids)"
        end
        # Limit and offset are only included if we're querying for multiple teams
        # (e.g. from the metrics_v2 endpoint).
        # If we're querying for a single team only (analytics_v2 endpoint)
        # we don't want them. 
        limit_clause = "LIMIT :limit" if query_params[:limit]
        offset_clause = "OFFSET :offset" if query_params[:offset]
        <<-SQL.strip_heredoc
          SELECT
            SUM(smartbites_created) AS  smartbites_created,
            SUM(smartbites_consumed) AS smartbites_consumed,
            SUM(num_comments) AS smartbites_comments_count,
            SUM(num_likes) AS smartbites_liked,
            SUM(time_spent_minutes) AS time_spent,
            team_id,
            organization_id
          FROM
            group_metrics_aggregations
          WHERE
            organization_id = :org_id
          AND
            start_time >= :from_date
          AND
            end_time <= :to_date
          #{team_filter}
          GROUP BY
            team_id
          ORDER BY
            #{query_params[:order_attr]} #{query_params[:order]}
          #{limit_clause}
          #{offset_clause}
        SQL
      end

      # For a specific list of team ids, selects the number of total unique
      # users for each over the specified time range.
      def build_total_period_unique_users_query(query_params)
        team_filter = if query_params[:team_ids]&.any?
          "AND team_id IN (:team_ids)"
        end
        <<-SQL.strip_heredoc
          SELECT
            COUNT(DISTINCT(user_id)) AS total_users,
            teams.id AS team_id,
            teams.organization_id
          FROM
            teams_users
          INNER JOIN
            teams
          ON
            teams_users.team_id = teams.id
          WHERE
            teams.organization_id = :org_id
          #{team_filter}
          AND
            UNIX_TIMESTAMP(teams_users.created_at) >= :from_date
          AND
            UNIX_TIMESTAMP(teams_users.created_at) <= :to_date
          GROUP BY
            teams.id
        SQL
      end

      # For a specific list of team ids, requests the number of unique active
      # users for each over the total requested period.
      # "Active" is defined as having time_spent_minutes > 0
      def build_total_period_unique_active_users_query(query_params)
        team_filter = if query_params[:team_ids]&.any?
          "AND team_id IN (:team_ids)"
        end
        <<-SQL.strip_heredoc
          SELECT
            teams_users.team_id,
            teams.organization_id,
            COUNT(DISTINCT(user_metrics_aggregations.user_id)) AS active_users
          FROM
            user_metrics_aggregations
          INNER JOIN
            teams_users
          ON
            teams_users.user_id = user_metrics_aggregations.user_id
          INNER JOIN
            teams
          ON
            teams.id = teams_users.team_id
          WHERE
            teams.organization_id = :org_id
          AND
            UNIX_TIMESTAMP(teams_users.created_at) >= :from_date
          AND
            UNIX_TIMESTAMP(teams_users.created_at) <= :to_date
          #{team_filter}
          AND
            user_metrics_aggregations.time_spent_minutes > 0
          GROUP BY 
            teams.id
        SQL
      end

      # Builds parameters that are used in the Influx query.
      def build_query_params(params, current_org)
        # Parse date parameters and convert to timestamps
        from_date, to_date = parse_date_params(params)
        # Determine the column and direction to order in.
        # This must be directly interpolated into the query, so it's sanitized.
        order_attr, order = extract_sort_order(
          valid_order_attrs: %w{
            smartbites_created smartbites_consumed
            smartbites_liked   smartbites_comments_count
            time_spent
          },
          default_order_attr: "smartbites_consumed",
          order:      params[:order],
          order_attr: params[:order_attr]
        )
        # Build the parameters which may be used by the query
        {
          from_date:  from_date,
          to_date:    to_date,
          org_id:     current_org.id,
          team_ids:   params[:team_ids],
          limit:      params[:limit],
          offset:     params[:offset],
          order_attr: order_attr,
          order:      order
        }
      end

      # Note: this error gets rescued by the controller,
      # where it renders unprocessible_entity
      def raise_parameter_error(msg)
        raise TeamModelLeaderboardV2Concern::ParameterError.new(msg)
      end

      # Returns [from_date, to_date] as integer timestamps
      def parse_date_params(params)
        from_date, to_date = params.values_at(:from_date, :to_date)
        unless [from_date, to_date].all?
          raise_parameter_error("`from_date` or `to_date` is missing")
        end
        from_date = Time.strptime(from_date, "%m/%d/%Y").utc.beginning_of_day
        to_date   = Time.strptime(to_date, "%m/%d/%Y").utc.end_of_day
        if from_date > to_date
          raise_parameter_error("Start date can not be greater than end date")
        end
        if ((to_date - from_date) / 1.year) > Settings.analytics_max_date_range
          raise_parameter_error("Exceeded maximum date range")
        end
        [from_date, to_date].map &:to_i
      end
    end
  end

end

# =========================================
# For testing:

=begin

  require 'byebug'

  Team.fetch_v2_analytics(
    params: {
      from_date: (Date.yesterday - 5.days).beginning_of_day.strftime("%m/%d/%Y"),
      to_date: Date.yesterday.end_of_day.strftime("%m/%d/%Y"),
      limit: 9000,
      offset: 0
    },
    current_org: Organization.new(id: 15)
  )

=end
# =========================================

require 'active_support/concern'

module Taggable
  extend ActiveSupport::Concern

  included do

    has_many :taggings,  -> { where( visible: true )}, as: :taggable, :dependent => :destroy
    has_many :tags, through: :taggings

    def add_tags
      _tags = []
      _taggable_fields.each do |tf|
        _tags |= Tag.extract_tags(tf)
      end
      self.tags << _tags
      try(:after_tagged)
    end

    def update_tags
      taggings.destroy_all
      _tags = []
      _taggable_fields.each do |tf|
        _tags |= Tag.extract_tags(tf)
      end
      self.tags << _tags
    end
  end

  def update_topics
    return if topics.nil?
    begin
      existing_tags = tags.pluck(:name)
      tags_passed = topics.is_a?(Array) ? topics : (topics&.split(/,\s*/) || [])
      tags_to_delete = existing_tags - tags_passed
      new_tags_to_add = tags_passed - existing_tags

      taggings.joins(:tag).where("tags.name IN (?)", tags_to_delete).destroy_all if tags_to_delete.any?

      if new_tags_to_add.any?
        _tags = Tag.extract_from_array(new_tags_to_add)
        self.tags << _tags.uniq
        self.topics = nil
      end
    rescue Exception => e
      log.error("Failed at update topics: #{e.message}")
    end
  end

end

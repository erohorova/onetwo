require 'active_support/concern'

module PubnubChannels
  extend ActiveSupport::Concern

  included do
    def pubnub_channel_name(as_type: , item_type: )
      tries = 0
      channel = nil
      ActiveRecord::Base.connection.uncached do
        begin
          channel = PubnubChannel.where(item: self, as_type: as_type).first
          if channel.nil?
            Octopus.using(:master) do
              query = ActiveRecord::Base.send(:sanitize_sql_array, ['INSERT INTO pubnub_channels (name, item_id, item_type, created_at, updated_at, as_type) values(?,?,?,?,?,?)', PubnubChannel.get_name_for(item_id: self.id, item_type: item_type, as_type: as_type), self.id, item_type, Time.now.utc, Time.now.utc, as_type])
              ActiveRecord::Base.connection.raw_connection.query(query)
              channel = PubnubChannel.where(item: self, as_type: as_type).first
            end
          end
        rescue Mysql2::Error => e
          if tries < 4
            tries += 1
            retry
          else
            log.warn "Couldn't get private pubnub channel in 5 attempts for item id: #{self.id} item type: #{item_type} with exception #{e.message}"
            return nil
          end
        end
      end
      channel.name
    end
  end
end

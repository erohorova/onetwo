require 'active_support/concern'

module Followable
  extend ActiveSupport::Concern

  included do

    has_many :follows, as: :followable, dependent: :destroy
    has_many :followers, through: :follows, source: :user

    after_create :follow_own

    def follow(user)
      followers << user unless followers.include? user
    end

    def follow_own
      follow user
    end

    def notify_followers(content, omit_user_id=nil, activity_id)
      followable_class_name = self.class.name
      notify = (['Question', 'Announcement'].exclude?(followable_class_name) || (self.group && self.group.forum_notifications_enabled))
      if notify
        ActivityNotificationJob.perform_later(followable_class_name: followable_class_name, followable_id: self.id, content: content,omit_user_id: omit_user_id, activity_id: activity_id)
      end
    end
  end
end

require 'active_support/concern'

module Accessible
  extend ActiveSupport::Concern

  included do
    class << self
      def get_accessibility_query(user)
        user_channel_ids(user)
        user_group_ids(user)

        query = "LEFT JOIN channels_cards cc ON cc.card_id = cards.id AND cc.channel_id in (#{@user_channel_ids}) " +
          "LEFT JOIN teams_cards tc ON tc.card_id = cards.id AND tc.team_id in (#{@user_group_ids}) " +
          "LEFT JOIN card_user_shares ON card_user_shares.card_id = cards.id AND card_user_shares.user_id = (#{user.id}) "+
          "LEFT JOIN `card_user_permissions` ON `card_user_permissions`.`card_id` = `cards`.`id` "+
          "LEFT JOIN `card_team_permissions` ON `card_team_permissions`.`card_id` = `cards`.`id` "
        
        query
      end

      def get_accessibility_condition(user)
        condition = "AND (cards.is_public = true " +
        "|| cards.author_id = #{user.id} " +
        "|| (cards.is_public = false " +
            "&& ((card_user_permissions.id IS NOT NULL && card_user_permissions.user_id = #{user.id}) "+
              "|| (card_team_permissions.id IS NOT NULL && card_team_permissions.team_id IN (#{@user_group_ids})) "+
              "|| ((cc.id IS NOT NULL || tc.id IS NOT NULL || card_user_shares.id IS NOT NULL) && " +
                  "(card_user_permissions.id IS NULL && card_team_permissions.id IS NULL)))))"
        
        condition
      end

      private
      def user_channel_ids(user)
        @user_channel_ids = user.followed_channel_ids.count > 0 ? user.followed_channel_ids.join(',') : 'NULL'
      end

      def user_group_ids(user)
        @user_group_ids = user.team_ids.any? ? user.team_ids.join(',') : 'NULL'
      end
    end
  end
end
# == Schema Information
#
# Table name: user_taxonomy_topic_level_metrics
#
#  id                  :integer          not null, primary key
#  user_id             :integer
#  organization_id     :integer
#  smartbites_score    :integer          default(0)
#  smartbites_created  :integer          default(0)
#  smartbites_consumed :integer          default(0)
#  time_spent          :integer          default(0)
#  period              :string(255)
#  offset              :integer
#  taxonomy_label      :string(255)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class UserTaxonomyTopicLevelMetric < ActiveRecord::Base
  include MetricsAggregator
  create_increment_methods([:smartbites_score, :smartbites_consumed, :smartbites_created, :time_spent])
  create_getter_methods([:smartbites_score, :smartbites_consumed, :smartbites_created, :time_spent])
  belongs_to :user
  belongs_to :organization

   validates :organization, presence: true
   validates :user, presence: true
   validates :taxonomy_label, presence: true

  before_validation on: :create do
    self.organization_id ||= user.try :organization_id
  end

  def label_name  
    self.taxonomy_label.split(".").last.humanize rescue nil
  end

end

# == Schema Information
#
# Table name: user_preferences
#
#  id                  :integer          not null, primary key
#  user_id             :integer
#  key                 :string(191)
#  value               :string(191)
#  created_at          :datetime
#  updated_at          :datetime
#  preferenceable_id   :integer
#  preferenceable_type :string(191)
#

class UserPreference < ActiveRecord::Base
  
  include IdentityCache
  
  WEEKLY_ACTIVITY_KEY = "notification.weekly_activity"
  FORUM_DAILY_DIGEST_KEY = "notification.daily_digest.opt_out"
  ASSIGNMENT_NOTIFICATION_KEY = "notification.assignment"
  TEAM_NOTIFICATION_KEY = "notification.admin.team.opt_out"

  belongs_to :user
  belongs_to :preferenceable, polymorphic: true
  
  cache_index :user_id, :key, :preferenceable_type, :preferenceable_id, unique: true

  def organization
  	preferenceable.try(:organization)
  end
end

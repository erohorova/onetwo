# == Schema Information
#
# Table name: user_sessions
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  start_time :datetime
#  end_time   :datetime
#  uuid       :string(191)
#

class UserSession < ActiveRecord::Base

  SESSION_CUTOFF_TIME = 10.minutes

  belongs_to :user

  validates :user, presence: true

  before_validation on: :create do
    self.uuid = SecureRandom.hex(64)
  end

  class << self

    # User session calculation
    # Session cut off time is 10 minutes
    # last_session.end_time >= 10 minutes, then 5 seconds time_spent (new session, new record)
    # last_session.end_time <  10 minutes, then update existing record end_time to timestamp argument

    def update_session(user_id, timestamp)
      last_session = where(user_id: user_id).where("start_time <= ?", timestamp).order(end_time: :desc).first
      time_delta = 0
      if last_session.nil? || last_session.end_time < timestamp - SESSION_CUTOFF_TIME
        # Create a new session
        if new(user_id: user_id, start_time: timestamp, end_time: timestamp + 5.seconds).save
          time_delta = 5 # seconds
        else
          log.error "Failed to create new user session for user id: #{user_id}. timestamp: #{timestamp}"
        end
      else
        # Update end time of existing session
        last_session_end_time = last_session.end_time
        unless last_session_end_time > timestamp
          # Only process is current event is ahead of already recorded session end time

          if last_session.update_attributes(end_time: timestamp)
            time_delta = (timestamp - last_session_end_time).to_i
          else
            log.error "Failed to update user session for user id: #{user_id}. timestamp: #{timestamp}"
          end
        end
      end
      time_delta
    end
  end
end

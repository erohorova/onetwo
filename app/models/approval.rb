# == Schema Information
#
# Table name: approvals
#
#  id              :integer          not null, primary key
#  user_id         :integer          not null
#  approvable_id   :integer          not null
#  approvable_type :string(191)      not null
#  created_at      :datetime
#  updated_at      :datetime
#

class Approval < ActiveRecord::Base
  include StreamWorthy
  
  belongs_to :approver, class_name: 'User', foreign_key: :user_id
  belongs_to :approvable, :polymorphic => true

  validates :approver, :presence => true
  validates :approvable, :presence => true

  validate :check_approval_rights
  validate :one_approval_per_approvable

  after_create :update_stars, :touch_approvable
  before_destroy :reset_stars
  after_commit :generate_notification, on: :create

  def generate_notification
    NotificationGeneratorJob.perform_later(item_id: id, item_type: 'approval')
  end

  add_to_stream action: 'approved',
                item: ->(record){record.approvable},
                group_id: ->(record){
                  approvable = record.approvable
                  if approvable.is_a?(Post)
                    approvable.group_id
                  elsif approvable.is_a?(Comment)
                    approvable.commentable.group_id
                  end
                }
  
  remove_from_stream(action: 'approved', item: ->(record){record.approvable})

  def self.get_approvals(approvable_ids: [], approvable_type: nil)
    if approvable_ids.blank?
      return []
    end

    Approval.where(:approvable_id => approvable_ids,
                   :approvable_type => approvable_type).pluck(:approvable_id)
  end

  def touch_approvable
    approvable.touch(:updated_at)
  end

  def update_stars
    if !approvable.anonymous
      if approvable.group && approvable.user
        score_entry = Score.where("group_id = ? AND user_id = ?", approvable.group, approvable.user).first
        if score_entry.blank?
          score_entry = Score.new({group: approvable.group, user: approvable.user,
                                   score: 0, stars: 1})
          score_entry.save
        else
          score_entry.increment_stars_and_save
        end
      end
    end
  end

  def reset_stars
    if !approvable.anonymous
      if approvable.group && approvable.user
        score_entry = Score.where("group_id = ? AND user_id = ?", approvable.group, approvable.user).first
        score_entry.decrement_stars_and_save
      end
    end
  end

  def check_approval_rights
    if approvable.group && !approvable.group.is_active_super_user?(approver)
      errors.add(:approver, 'Need to be instructor to approve')
    end
  end

  def one_approval_per_approvable
    if approvable.has_approval?
      errors.add(:approvable, 'Item already approved')
    end
  end

  def organization
    approvable.try(:organization)
  end

end

# == Schema Information
#
# Table name: badgings
#
#  id                   :integer          not null, primary key
#  title                :string(255)
#  badge_id             :integer
#  badgeable_id         :integer
#  type                 :string(20)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  target_steps         :integer
#  all_quizzes_answered :boolean          default(FALSE)
#

class ClcBadging < Badging
  belongs_to :clc_badge, foreign_key: :badge_id
  belongs_to :clc, foreign_key: :badgeable_id

  has_many :user_badges, foreign_key: :badging_id

  delegate :image, to: :clc_badge, prefix: true, allow_nil: true
  delegate :image_social, to: :clc_badge, prefix: true
end

# == Schema Information
#
# Table name: batch_jobs
#
#  id            :integer          not null, primary key
#  name          :string(255)
#  last_run_time :datetime
#

class BatchJob < ActiveRecord::Base

  validates :name, presence: true, uniqueness: true
  validates :last_run_time, presence: true
end

# == Schema Information
#
# Table name: badgings
#
#  id                   :integer          not null, primary key
#  title                :string(255)
#  badge_id             :integer
#  badgeable_id         :integer
#  type                 :string(20)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  target_steps         :integer
#  all_quizzes_answered :boolean          default(FALSE)
#

class CardBadging < Badging
  belongs_to :card_badge, foreign_key: :badge_id
  belongs_to :card, foreign_key: :badgeable_id

  has_many :user_badges, foreign_key: :badging_id
  delegate :image, to: :card_badge, prefix: true
  delegate :image_social, to: :card_badge, prefix: true
end

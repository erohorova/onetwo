# == Schema Information
#
# Table name: team_assignment_metrics
#
#  id              :integer          not null, primary key
#  team_id         :integer          not null
#  assignor_id     :integer          not null
#  card_id         :integer          not null
#  created_at      :datetime         not null
#  assigned_count  :integer          default(0)
#  started_count   :integer          default(0)
#  completed_count :integer          default(0)
#  updated_at      :datetime
#  assigned_at     :datetime
#

class TeamAssignmentMetric < ActiveRecord::Base
  belongs_to :team
  belongs_to :assignor, foreign_key: 'assignor_id', class_name: 'User'
  belongs_to :card

  validates :team, presence: true, uniqueness: {scope: [:assignor_id, :card_id]}
end

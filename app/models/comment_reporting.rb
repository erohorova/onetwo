class CommentReporting < ActiveRecord::Base
  acts_as_paranoid

  # Associations
  belongs_to :comment
  belongs_to :user

  # Validations
  validates :user, :reason, presence: true
  validates :comment, presence: true, uniqueness: { scope: :user_id }

  class << self
    def filter_content(state:, comment_id:, organization:)
      reportings = (state == 'trashed') ? trashed_comments(organization) : reported_comments(organization)
      if comment_id.present?
        reportings = reportings.with_deleted.where(comment_id: comment_id)
      else
        reportings = reportings.joins("
          INNER JOIN (
            select max(id) as max_id, comment_id FROM comment_reportings GROUP BY comment_id
          ) sub on sub.max_id = comment_reportings.id")
      end
      reportings
    end

    def trashed_comments(organization)
      only_deleted.joins("INNER JOIN comments ON comments.id = comment_reportings.comment_id AND comments.deleted_at IS NOT NULL")
                  .joins("INNER JOIN users ON users.id = comments.user_id")
                  .joins("INNER JOIN users as reported_by on reported_by.id = comment_reportings.user_id")
                  .joins("INNER JOIN cards on cards.id = comments.commentable_id AND comments.commentable_type = 'Card'")
                  .where(users: {organization_id: organization.id})
                  .select("#{attrs}")
    end

    def reported_comments(organization)
      joins(comment: [:user, :card])
      .joins("INNER JOIN users as reported_by on reported_by.id = comment_reportings.user_id")
      .where(users: {organization_id: organization.id})
      .select("#{attrs}")
    end

    def attrs
      " comments.id as comment_id,
        comments.created_at as comment_created_at,
        cards.title,
        cards.message,
        comments.message as comments_message,
        cards.ecl_metadata,
        cards.card_type as card_type,
        cards.id as card_id,
        comments.user_id as comment_created_by_user_id,
        users.first_name as comment_author_first_name,
        users.last_name as comment_author_last_name,
        reported_by.first_name as reported_by_first_name,
        reported_by.last_name as reported_by_last_name,
        comment_reportings.comment_id,
        comment_reportings.user_id,
        comment_reportings.reason,
        comment_reportings.created_at,
        comments.commentable_type,
        comments.commentable_id,
        (SELECT count(id) from comment_reportings where comment_reportings.comment_id = comments.id) as comment_reporting_count"
    end
  end
end

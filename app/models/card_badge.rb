# == Schema Information
#
# Table name: badges
#
#  id                        :integer          not null, primary key
#  organization_id           :integer
#  type                      :string(20)
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  image_file_name           :string(255)
#  image_content_type        :string(255)
#  image_file_size           :integer
#  image_updated_at          :datetime
#  image_social_file_name    :string(255)
#  image_social_content_type :string(255)
#  image_social_file_size    :integer
#  image_social_updated_at   :datetime
#

class CardBadge < Badge
  has_many :card_badgings, foreign_key: :badge_id
end

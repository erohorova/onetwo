class OrgSubscription < ActiveRecord::Base

  validates :name, :description, :start_date, :end_date, :organization_id, :sku, presence: true

  belongs_to :organization
  has_many :orders, as: :orderable
  has_many :prices, foreign_key: "org_subscription_id", class_name: "OrganizationPrice", dependent: :destroy

  def redirect_page_url(org:)
    org&.home_page
  end
end

class GroupMetricsAggregation < ActiveRecord::Base
  belongs_to :team
  belongs_to :organization

  validates_presence_of *%i{
    team_id            organization_id
    smartbites_created smartbites_consumed
    time_spent_minutes total_users
    active_users       new_users
    engagement_index   num_comments
    num_likes          start_time
    end_time
  }

end

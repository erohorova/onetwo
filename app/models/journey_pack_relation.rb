# == Schema Information
#
# Table name: journey_pack_relations
#
#  id         :integer          not null, primary key
#  cover_id   :integer
#  from_id    :integer
#  to_id      :integer
#  start_date :datetime
#  deleted    :boolean          default(FALSE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

# mostly same logic as CardPackRelation, will be extended in future in regards journey section ordering and etc.
class JourneyPackRelation < ActiveRecord::Base
  include EventTracker
  include CardMetadata
  include SyncCloneable

  belongs_to :cover, class_name: 'Card'
  belongs_to :card, class_name: 'Card', foreign_key: :from_id

  validates :cover, presence: true
  # from_id and from_type represent pack content object (like AR STI, but sometimes with ancestor class names)
  # to_id is id of next relation object
  # not using activerecord association here because object may have been deleted
  validates :from_id, presence: true
  validates :to_id, uniqueness: true, allow_nil: true

  after_commit :reindex_cover
  after_destroy :sync_clone_card_association
  after_commit  :reset_completed_percentage, on: [:create, :destroy]

  configure_metric_recorders(
    default_actor: lambda {|record| record.cover&.author},
    trackers: {
      on_create: {
        event: 'card_added_to_journey',
        job_args: [{card: :card}],
        event_opts: lambda { |record|
          {
            journey_id: record.cover_id,
            journey_name: record.cover&.title,
          }
        }
      },
      on_delete: {
        event: 'card_removed_from_journey',
        job_args: [{card: :card}],
        event_opts: lambda { |record|
          {
            journey_id: record.cover_id,
            journey_name: record.cover&.title,
          }
        }
      }
    }
  )

  def from=(obj)
    self.from_id = obj.id
  end

  def to=(obj)
    # for 'weekly' first relation start_date equals to cover
    date = cover_id.eql?(from_id) ? start_date : start_date.try { |d| d + 7.days }
    to_reln = JourneyPackRelation.find_or_create_by(cover: cover, from_id: obj.id, start_date: date)
    return unless to_reln && to_reln.errors.empty?
    self.to_id = to_reln.id
    JourneyPackRelation.reindex(obj) if to_reln
  end


  class << self
    def add(cover_id:, add_id:)
      error = proc do |msg|
        log.error "#{msg || 'Error'}: adding id #{add_id} to journey cover card #{cover_id}"
        return
      end
      added_item = Card.find_by(id: add_id)
      error.call('Incorrect pack subtype') if added_item.card_subtype != 'simple'
      add_id = add_id.to_i
      cover_id = cover_id.to_i
      ordered = journey_relations(cover_id: cover_id)
      error.call('Pack not found') if ordered.nil?
      return true if ordered.map { |o| [o.from_id] }.include? [add_id]

      linked = ordered.last
      error.call(linked.errors.full_messages) unless linked.update to: added_item
      true
    end

    def journey_relations(cover_id:)
      error = proc do |msg|
        log.error "#{msg} for journey cover card #{cover_id}"
        return
      end

      relations = where(cover_id: cover_id).to_a

      if relations.empty?
        card = Card.find_by(id: cover_id)
        if card&.card_type == 'journey'
          # create root relation if it not present and push to relations variable
          relations.push(
            create(cover_id: cover_id, from_id: cover_id)
          )
        end
      end

      first_relation = relations.detect { |r| r.from_id == cover_id }
      error.call('missing cover relation') if first_relation.nil?

      ordered = [relations.delete(first_relation)]
      until relations.empty? do
        last_size = relations.size
        relations.delete_if {|reln| ordered << reln if reln.id == ordered.last.to_id }
        error.call('missing relation') unless relations.size < last_size
      end

      ordered
    end

    def reindex(objs)
      objs = [objs] unless Array === objs
      exemplar = objs.first
      if exemplar.respond_to?(:reindex) && exemplar.class.respond_to?(:searchkick_index)
        objs.each do |obj|
          obj.inline_indexing = true if obj.respond_to?(:inline_indexing=)
          obj.reindex
        end
      end
    end

    def remove(cover_id:, inline_indexing: false, delete_hidden:)
      error = proc do |msg|
        log.error "#{msg} for journey cover card #{cover_id}"
        return
      end

      relations = where(cover_id: cover_id)
      error.call('no relations') unless relations.any?

      error.call('error destroying relations') unless relations.map(&:destroy).all?

      cover = Card.find_by(id: cover_id)
      cover_channels = cover.channels

      if cover
        cover.inline_indexing = inline_indexing
        cover.destroy
      else
        log.error "can't find journey cover card #{cover_id}"
        return
      end

      pack_card_ids = relations.map(&:from_id) - [cover_id]
      hidden_cards = Card.where(id: pack_card_ids)
      hidden_cards.each { |c| remove_section_card(card_id: c, delete_hidden: delete_hidden) }
      true
    end

    def remove_from_journey(cover_id:, remove_id:, destroy_hidden:)
      cover_id  = cover_id.to_i
      remove_id = remove_id.to_i
      remove_node = find_by(cover_id: cover_id, from_id: remove_id)
      return false unless remove_node

      before_node = find_by(cover_id: cover_id, to_id: remove_node.id)
      return false unless before_node

      ActiveRecord::Base.transaction do
        remove_node.destroy
        before_node.update(to_id: remove_node.to_id)
      end

      return unless remove_node.destroyed?
      remap_start_date(cover_id: cover_id) if remove_node.to_id && remove_node.start_date
      separate_section_card(card_id: remove_id, delete_hidden: destroy_hidden)
      true
    end

    def remap_start_date(cover_id:)
      pack = journey_relations(cover_id: cover_id)
      return if pack.blank?
      # take root section start_date
      start_date = pack.first&.start_date
      pack[1..-1].each do |jpr|
        jpr.update_attributes(start_date: start_date)
        start_date += 7.days unless start_date.nil?
      end
    end

    def separate_section_card(card_id:, delete_hidden:)
      pack = Card.find_by(id: card_id)
      return unless pack&.hidden && pack.state == Card::STATE_DRAFT
      pack.update_column(:hidden, false)
    end

    def remove_section_card(card_id:, delete_hidden:)
      pack = Card.find_by(id: card_id)
      return unless pack&.hidden && pack.state == Card::STATE_DRAFT
      # if hidden and state draft => section created in journey context
      #  CPR.remove with opt destroy_hidden
      # else
      #   pack could be owned by author(published/draft) or not owned(public)
      #   for now (since JPR already delete in this point) do nothing
      CardPackRelation.remove(
        cover_id: pack.id,
        destroy_hidden: delete_hidden,
        inline_indexing: false
      )
    end

    def reorder_journey_relations(cover_id:, from_pos:, to_pos:)
      relation = valid_relation(cover_id, from_pos, to_pos)
      return false unless relation
      from_pos = valid_position(relation, from_pos.to_i)
      to_pos = valid_position(relation, to_pos.to_i)
      return false unless from_pos || to_pos
      to_reorder = relations_to_update(relation, from_pos, to_pos)
      transaction_valid = false
      relation[from_pos].with_transaction_returning_status do
        to_reorder.each { |r| r.save validate: false }
        transaction_valid = to_reorder.map(&:valid?).all?
      end
      remap_start_date(cover_id: cover_id) if relation[0].start_date && transaction_valid
      transaction_valid
    end

    def valid_position(rel, pos)
      valid_relation = rel.clone.delete_if(&:deleted)
      rel.index(valid_relation[pos])
    end

    def valid_relation(cover_id, from_pos, to_pos)
      cover_id, from_pos, to_pos = [cover_id, from_pos, to_pos].map(&:to_i)
      return nil if [from_pos, to_pos].any?(&:zero?) || from_pos == to_pos
      relations = journey_relations(cover_id: cover_id)
      return nil if relations.blank? || [from_pos, to_pos].any? { |i| i > relations.count - 1 }
      relations
    end

    def relations_to_update(rel, from, to)
      from_relation = rel[from]
      to_relation = rel[to]
      from_previous = rel[from - 1]
      from_previous.to_id = from_relation.to_id
      [from_previous, from_relation].push(
        if from < to #move down
          from_relation.to_id = to_relation.to_id
          to_relation.to_id = from_relation.id
          to_relation
        else #move up
          to_previous = rel[to - 1]
          to_previous.to_id = from_relation.id
          from_relation.to_id = to_relation.id
          to_previous
        end
      )
    end
  end

  private

  def reindex_cover
    cover.reindex_card if cover.respond_to?(:reindex_card)
  end

  #Reset completed_percentage for journey when card is added/removed from journey
  def reset_completed_percentage
    PackJourneyCardReprocessJob.perform_later(cover_id)
  end
end

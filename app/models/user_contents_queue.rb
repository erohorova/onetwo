class UserContentsQueue

  include Elasticsearch::Persistence::Model
  extend ElasticsearchPersistenceBuffer

  self.index_name = "user_contents_queue_#{Rails.env}"

  attribute :user_id, Integer
  attribute :content_id, Integer
  attribute :content_type, String, mapping: {type: 'string', index: 'not_analyzed'}
  attribute :card_type, String, mapping: {type: 'string', index: 'not_analyzed'}
  attribute :group_id, Integer
  attribute :last_enqueue_event_time, DateTime
  attribute :is_public, Boolean
  attribute :priority, Integer
  attribute :channel_ids, Array
  attribute :tags, Array, mapping: {type: 'string', index: 'not_analyzed'}
  attribute :card_template, String, mapping: {type: 'string', index: 'not_analyzed'}

  # array of metadata hashes
  # The hash itself can be free form, but it is advisable to store some kind of
  # identifier to help in removing the trigger if a user reverses his/her action (upvote/unupvote)
  attribute :metadata, Array, mapping: {type: 'object'}

  def self.get_cards_queue_for_user(user_id:, limit: 10, offset: 0, filter_params: {}, method: :search)
    get_queue_for_user(user_id: user_id, content_type: 'Card', limit: limit, offset: offset, filter_params: filter_params, method: method)
  end

  def self.get_posts_queue_for_user(user_id:, limit: 10, offset: 0, filter_params: {}, method: :search)
    get_queue_for_user(user_id: user_id, content_type: 'Post', limit: limit, offset: offset, filter_params: filter_params, method: method)
  end

  def self.get_queue_for_user(user_id:, content_type: nil, limit: 10,
      offset: 0, filter_params: {},
      method: :search, sort: :enqueue_time)
    filters = [{term: {user_id: user_id}}]
    filters << {term: {content_type: content_type}} unless content_type.nil?
    filters << {terms: {group_id: filter_params[:include_group_ids]}} if filter_params[:include_group_ids].present?

    if filter_params[:include_card_types].present?
      filters << {or: [{not: {term: {content_type: 'Card'}}},
                       {terms: {card_type: filter_params[:include_card_types]}}
      ]}
    end

    if filter_params[:exclude_card_types].present?
      filters << {or: [{not: {term: {content_type: 'Card'}}},
                       {not: {terms: {card_type: filter_params[:exclude_card_types]}}}
      ]}
    end

    filters << {term: {is_public: true}} if filter_params[:is_public]
    filters << {not: {terms: {_id: filter_params[:exclude_queue_item_ids]}}} if filter_params[:exclude_queue_item_ids].present?
    filters << {not: {terms: {content_id: filter_params[:exclude_content_ids]}}} if filter_params[:exclude_content_ids].present?
    filters << {terms: {content_type: filter_params[:content_types]}} if filter_params[:content_types].present?
    filters << {terms: {tags: filter_params[:topics]}} if filter_params[:topics].present?
    filters << {range: {last_enqueue_event_time: {gte: filter_params[:from_time]}}} if filter_params[:from_time].present?

    case method
    when :search
      pars = {filter: {and: filters}}
      case sort
      when :enqueue_time
        pars.merge!(sort: [{'last_enqueue_event_time' => {'order' => 'desc'}}])
      when :random
        pars = {query: {function_score: pars.merge(random_score: {})}}
      when :priority
        # See config/initializers/elasticsearch_feedranking_custom_script.rb
        pars = {query: {function_score: pars.merge(script_score: {params: {now: Time.now.utc}, lang: 'groovy', script_id: "indexedFeedRankingScript"})}}
      end
      pars.merge!(from: offset, size: limit)
      return self.search(pars).results
    when :count
      return self.count filter: {and: filters}
    end
  end

  def self._get(user_id:, content_id:, content_type:)
    (self.search query: {filtered: {query: {match_all: {}}, filter: {and: [{term: {user_id: user_id}}, {term: {content_id: content_id}}, {term: {content_type: content_type}}]}}}).results.first
  end

  def self.get_users_for_content(user_ids:, content_id:, content_type:)
    search(
      query: {
        filtered: {
          query: {
            match_all: {}
          },
          filter: {
            and: [
              {terms: {user_id: user_ids}},
              {term: {content_id: content_id}},
              {term: {content_type: content_type}}
            ]
          }
        }
      }
    ).results
  end

  def self.get_contents_for_user(user_id:, content_ids:, content_type:)
    search(
      query: {
        filtered: {
          query: {
            match_all: {}
          },
          filter: {
            and: [
              {term: {user_id: user_id}},
              {terms: {content_id: content_ids}},
              {term: {content_type: content_type}}
            ]
          }
        }
      },
      size: content_ids.length
    ).results
  end

  # If content already in queue, merge or replace metadata, and update last_enqueue_event_time
  # metadata merging assumes exactly one metadata hash exists in array
  #   keys must be strings and values must be arrays (of strings rather than symbols)
  def self.enqueue_for_user(user_id:, content_id:, content_type:, channel_ids: [], metadata: {}, queue_timestamp: Time.now.utc,
      merge_metadata: false, overwrite_timestamp: true)
    user = User.find(user_id)

    content = content_type.constantize.find_by(id: content_id)
    if content.nil?
      log.warn "Trying to enqueue non existent content. content_id: #{content_id}, content_type: #{content_type}"
      return
    end

    return false if user.has_dismissed_content?(content_id, content_type)
    return false if user.has_bookmarked_content?(content_id, content_type)
    return false if user.completion_state_for(content_type, content_id) == UserContentCompletion::COMPLETED

    priority = get_queue_priority_for(content: content)
    existing = _get(user_id: user_id, content_id: content_id, content_type: content_type)

    if existing
      if merge_metadata
        existing.metadata = [existing.metadata.first.merge(metadata) {|_, oldv, newv| oldv | newv}]
      else
        existing.metadata = [metadata]
      end
      existing.priority = priority
      existing.last_enqueue_event_time = queue_timestamp if overwrite_timestamp
      existing.channel_ids = channel_ids
      if content_type == 'Card'
        existing.card_template = content.card_subtype
      end
      existing.save
    else

      pars = {content_id: content_id, content_type: content_type, priority: priority, last_enqueue_event_time: queue_timestamp, metadata: [metadata], user_id: user_id}
      case content_type
      when 'Card'
        pars.merge!({card_type: content.card_type, channel_ids: channel_ids, card_template: content.card_subtype, group_id: content.group_id, is_public: content.is_public, tags: content.tags.map(&:name)})
      when 'Post'
        pars.merge!({group_id: content.group_id})
      end
      create(pars)
    end

    gateway.refresh_index!
  end

  # takes array of item hashes of form {content_id:, metadata:}
  # skips all ActiveRecord stuff done by enqueue_for_user
  # same metadata behavior as enqueue_for_user
  def self.enqueue_bulk_for_user(user_id:, content_type:, items:, merge_metadata: false, flush: true)
    in_queue = get_contents_for_user(user_id: user_id, content_ids: items.map {|i| i[:content_id]}, content_type: content_type)

    content_ids = items.map { |item| item[:content_id] }
    item_ids_to_remove = ineligible_content_ids(user_id: user_id, content_ids: content_ids, content_type: content_type)
    items = items.index_by {|item| item[:content_id]}.except!(*item_ids_to_remove).values

    items.each do |item|
      priority = 1

      existing = in_queue.find {|i| i.content_id.to_i == item[:content_id].to_i}
      if existing
        if merge_metadata
          # assume exactly one metadata entry exists
          existing.metadata = [existing.metadata.first.merge(item[:metadata]) {|_, oldv, newv| oldv | newv}]
        else
          existing.metadata = [item[:metadata]]
        end
        existing.priority = priority
        existing.last_enqueue_event_time = item[:queue_timestamp]
        buffer_save existing
      else
        pars = {content_id: item[:content_id], content_type: content_type, channel_ids: item[:channel_ids], last_enqueue_event_time: item[:queue_timestamp], priority: priority, metadata: [item[:metadata]], user_id: user_id}
        buffer_create pars
      end
    end
    buffer_flush if flush
  end

  def self.bulk_delete(contents:)
    contents.each {|content| buffer_delete content}
    buffer_flush
  end
  
  # Enqueue a content for multiple users
  def self.enqueue_content_for_users(selected_users:, channel_ids: [], content_id:, content_type:, overwrite_timestamp: true)
    return unless selected_users.is_a? Hash
    user_ids = selected_users&.keys
    return unless user_ids.present?
    users_with_content = get_users_for_content(user_ids: user_ids, content_id: content_id, content_type: content_type)

    content = content_type.constantize.find_by(id: content_id)
    if content.nil?
      log.warn "enqueue_content_for_users: Trying to enqueue non existent content. content_id: #{content_id}, content_type: #{content_type}"
      return
    end

    priority = get_queue_priority_for(content: content)
    remove_user_ids = ineligible_user_ids(user_ids: user_ids, content_id: content_id, content_type: content_type)
    selected_users.except!(*remove_user_ids)

    selected_users.each do |user_id, value|
      existing = users_with_content.find {|user_content| user_content.user_id.to_i == user_id.to_i}
      metadata = {rationale: value[:rationale].presence || {}}
      if existing
        existing.metadata = [metadata]
        existing.priority = priority
        existing.last_enqueue_event_time = (value[:enqueued_at].presence || Time.now.utc) if overwrite_timestamp
        existing.channel_ids = channel_ids
        existing.card_template = content.card_subtype if content_type == 'Card'
        buffer_save existing
      else
        pars = {
          content_id: content_id, content_type: content_type, priority: priority,
          last_enqueue_event_time: value[:enqueued_at] || Time.now.utc, metadata: [metadata], user_id: user_id
        }
        case content_type
          when 'Card'
            pars.merge!({
              card_type: content.card_type, channel_ids: channel_ids, card_template: content.card_subtype,
              group_id: content.group_id, is_public: content.is_public, tags: content.tags.map(&:name)
            })
          when 'Post'
            pars.merge!({group_id: content.group_id})
        end
        buffer_create pars
      end
    end
    buffer_flush
  end

  # removes some content from a user's queue
  def self.dequeue_for_user(user_id:, content_id:, content_type:)
    existing = _get(user_id: user_id, content_id: content_id, content_type: content_type)
    if existing

      # Fix bugsnag:
      # https://app.bugsnag.com/edcast/1-lxp-production-tasks/errors/5a602162c1a34200197550f6
      # Do safe destroy because index record might get delete in other job at same time

      existing&.destroy
    else
      false
    end
  end

  def self.dequeue_content(content_id:, content_type:)
    self.delete_by_query({query: {filtered: {query: {match_all: {}}, filter: {and: [{term: {content_id: content_id}}, {term: {content_type: content_type}}]}}}})
  end

  def self.clear_docs_older_than(itime)
    self.delete_by_query({query: {filtered: {query: {range: {updated_at: {lte: Time.at(itime)}}}}}})
  end

  # gets card contents corresponding to queue in Search::SearchResponse form
  def self.get_cards_for_user(user_id:, limit: 10, offset: 0, filter_params: {})
    queue_card_results = get_cards_queue_for_user(
        user_id: user_id,
        limit: limit,
        offset: offset,
        filter_params: filter_params
    )
    if queue_card_results.present?
      queue_card_ids = queue_card_results.map(&:content_id)
      queue_cards_unsorted = Search::CardSearch.new.search_by_id(queue_card_ids) || []
      cards = queue_cards_unsorted.sort_by {|x| queue_card_ids.index(x['id'].to_i)}
      cards.each do |c|
        card = queue_card_results.find {|qcr| c['id'] == qcr['content_id']}
        c['queue_data'] = card['metadata'] if card
      end
    else
      cards = []
    end

    return Search::SearchResponse.new(
        offset: offset,
        limit: limit,
        total: get_cards_queue_for_user(user_id: user_id, filter_params: filter_params, method: :count),
        results: cards,
    )
  end

  def self.post_process categorized_items, type
    ret = []
    if type == 'Card'
      categorized_items.each do |ci|

        ids = ci['items'].map(&:content_id)
        its = Search::CardSearch.new.search_by_id(ids) || []
        unless its.blank?
          ret << ci.merge({'type' => type, 'queue_items' => ci['items'], 'items' => its.sort {|x, y| ids.index(x.id) <=> ids.index(y.id)}})
        end
      end
    elsif type == 'VideoStream'
      categorized_items.each do |ci|
        vids = ci['items'].map(&:content_id)
        its = VideoStream.where(id: vids)
        unless its.blank?
          ret << ci.merge({'type' => type, 'queue_items' => ci['items'], 'items' => its.sort {|x, y| vids.index(x.id) <=> vids.index(y.id)}, 'status' => its.first.status})
        end
      end
    elsif type == "User"
      categorized_items.each do |ci|
        ids = ci['items'].map(&:content_id)
        its = User.where(id: ids)
        unless its.length < 2
          ret << ci.merge({'type' => type, 'queue_items' => ci['items'], 'items' => its})
        end
      end
    elsif type == 'Channel'
      categorized_items.each do |ci|
        ids = ci['items'].map(&:content_id)
        channels = Channel.where(id: ids)
        unless channels.length < 2
          followers_count = Follow.where(followable_type: 'Channel', followable_id: ids).group(:followable_id).count
          insights_count = {}
          channels.each do |channel|
            insights_count[channel.id] = Search::CardSearch.new.count(
                channel,
                channel.organization,
                viewer: nil, # defaulting to public. not ideal
                filter_params: {card_type: Card::EXPOSED_TYPES, state: 'published'}
            )
          end
          ret << ci.merge({'type' => type, 'followers_count' => followers_count, 'insights_count' => insights_count, 'queue_items' => ci['items'], 'items' => channels})
        end
      end
    end
    ret
  end

  # DEPRECATED
  def self.categorize_feed items, options = {}
    ret = []
    permanent_exclusion_list = []
    item_types = items.map {|i| i.content_type}.uniq
    return ret, permanent_exclusion_list if items.blank?
    [['VideoStream', :categorize_stream_recommendations], ['User', :categorize_user_recommendations], ['Card', :categorize_card_recommendations], ['Channel', :categorize_channel_recommendations]].sort_by {|x| item_types.index(x[0]) || -1}.each do |type|
      items_of_type = items.select {|i| i.content_type == type[0]}
      unless items_of_type.blank?
        rec, exclusions = Recommend.send(type[1], items_of_type, options)
        categorized_items_of_type = rec || []
        processed = post_process(categorized_items_of_type, type[0])
        ret += processed
        permanent_exclusion_list += exclusions
      end
    end
    return ret, permanent_exclusion_list
  end

  # All content put on same priority updated on 07/25/2016
  def self.get_queue_priority_for(content:)
    return 1
  end

  def self.feed_content_for_v2(user: , limit: 15, offset: 0, filter_params: {})
    top_queue_items = get_queue_for_user(user_id: user.id, content_type: "Card",
                                         limit: limit, offset: offset)
    cards = Card.where(state: "published", id: top_queue_items.map(&:content_id))
    cards = cards.filtered_by_language(user.language) if filter_params[:filter_by_language]
    cards_hashed_by_id = cards.index_by(&:id)
    items_hashed_by_queue_item_id = Hash[top_queue_items.map{|queue_item| [queue_item.id, cards_hashed_by_id[queue_item.content_id]]}]
    top_queue_items.select do |item|
      items_hashed_by_queue_item_id[item.id].present?
    end.map do |item|
      {
          'rationale' => item.load_rationale,
          'item' => items_hashed_by_queue_item_id[item.id],
      }
    end
  end

  def self.feed_content_for(user:, feed_request_id: nil, limit: 15, filter_params: {}, options: {})

=begin
1. Get content you have shown to the user on this request from user_feed_cache
2. Retrieve top items in user's queue excluding the ids from step 1
3. Categorize top items from Step 2 into groups of 3, discard any that don't fit into a group of 3
4. Return the categories and the items in them to controller
5. Add the items from step 4 to user_feed_cache - either update existing entry or create a new entry if feed_request_id.nil?
=end

    # set default options for course promo, channel recommendations, influencers and scheduled streams
    # by default they all be part of categorized feed unless explicitly disabled
    options[:show_course_promo] = options.has_key?(:show_course_promo) ? options[:show_course_promo] : true
    options[:show_channel_recommendations] = options.has_key?(:show_channel_recommendations) ? options[:show_channel_recommendations] : true
    options[:show_user_influencers] = options.has_key?(:show_user_influencers) ? options[:show_user_influencers] : true
    options[:show_scheduled_streams] = options.has_key?(:show_scheduled_streams) ? options[:show_scheduled_streams] : true

    options[:author_must] = options.has_key?(:author_must) ? options[:author_must] : false
    options[:pathway_author_must] = options.has_key?(:pathway_author_must) ? options[:pathway_author_must] : false

    #merge topics filter
    filter_params.merge!(topics: options[:topics]) unless options[:topics].blank?

    # Step 1
    exclude_ids = []
    ufc = nil

    if feed_request_id
      ufc = UserFeedCache.find_by(user: user, feed_request_id: feed_request_id)
      begin
        exclude_ids |= JSON.parse(ufc.queue_item_ids)
      rescue
      end
    end
    filter_params[:exclude_queue_item_ids] = exclude_ids unless exclude_ids.blank?

    # Step 2
    filter_params[:content_types] = options[:show_user_influencers] ? ['Card', 'User', 'VideoStream'] : ['Card', 'VideoStream']
    top_queue_items = get_queue_for_user(user_id: user.id, limit: limit,
                                         offset: 0, filter_params: filter_params,
                                         method: :search, sort: :priority)

    # Step 3
    categorized_feed, permanent_exclusion_list = categorize_feed(top_queue_items, options)

    # degenerate case where we only have users in the feed
    # This case only exists because we queue users in bulk,
    # and retrieve them only in groups of 3. So multiple pages
    # of the feed is just users around the time the job
    # to recommend users is run.
    if categorized_feed.size == 1 && categorized_feed.first['type'] == 'User'
      top_non_user_queue_items = get_queue_for_user(user_id: user.id, limit: limit,
                                                    offset: 0, filter_params: filter_params.merge({content_types: ['Card', 'VideoStream']}),
                                                    method: :search, sort: :priority)

      more_feed, more_exclusions = categorize_feed(top_non_user_queue_items, options)
      categorized_feed += more_feed
      permanent_exclusion_list += more_exclusions
    end

    # We either haven't found anything or found only users
    if (categorized_feed.size == 1 && categorized_feed.first['type'] == 'User') || categorized_feed.size == 0
      # This is the case when all items on the top of the queue
      # have been filtered out. One such case is when all the top items are past video streams
      # with no recordings. Just fetch cards here.
      top_card_queue_items = get_queue_for_user(user_id: user.id, limit: limit,
                                                offset: 0, filter_params: filter_params.merge({content_types: ['Card']}),
                                                method: :search, sort: :priority)

      more_feed, more_exclusions = categorize_feed(top_card_queue_items)
      categorized_feed += more_feed
      permanent_exclusion_list += more_exclusions
    end

    # Get channel recommendations on page 2 of the feed, or page 1 if nothing in queue
    channel_recommendations_shown = ufc.try(:channel_recommendations_shown)
    if ((ufc && !ufc.channel_recommendations_shown) || (ufc.nil? && categorized_feed.size == 0)) && options[:show_channel_recommendations]
      top_channel_queue_items = get_queue_for_user(user_id: user.id, limit: limit, content_type: 'Channel', sort: :priority)
      more_feed, more_exclusions = categorize_feed(top_channel_queue_items)
      # Set to true even if the recommendations list is empty.
      # Empty list probably means there are no channels in the queue, unlikely there will be any by the time the user scrolls
      channel_recommendations_shown = true
      categorized_feed += more_feed
      permanent_exclusion_list += more_exclusions
    end

    # Add promotions to end of page 1
    if Settings.features.promotions.enabled && ufc.nil? && options[:show_course_promo]
      promos = Promotion.promotions_for(user: user).to_a
      unless promos.empty?
        categorized_feed << {
            'type' => 'Promotion',
            'title' => 'Recommended courses',
            'items' => promos,
            'queue_items' => [],
        }
      end
    end

    # Step 4
    queue_item_ids_for_cache = categorized_feed.map {|c| c['queue_items']}.flatten.map(&:id)
    queue_item_ids_for_cache += permanent_exclusion_list.map(&:id)


    if ufc
      exclude_ids += queue_item_ids_for_cache
      ufc.queue_item_ids = exclude_ids.to_json
      ufc.channel_recommendations_shown = channel_recommendations_shown
      ufc.save
    elsif !queue_item_ids_for_cache.empty?
      feed_request_id = Digest::SHA256.hexdigest([user.id, SecureRandom.hex, Time.now.utc.to_i].join('|'))
      ufc = UserFeedCache.create(user: user, feed_request_id: feed_request_id, channel_recommendations_shown: channel_recommendations_shown, queue_item_ids: queue_item_ids_for_cache.to_json)
    end

    # Step 5
    return categorized_feed, feed_request_id

  end

  def self.feed_topics_for_user(user_id:, limit: 15)
    filters = [
        {term: {user_id: user_id}},
        {terms: {content_type: ['Card', 'VideoStream']}},
        {range: {created_at: {gte: 1.month.ago.to_date.to_s, lte: "now"}}}
    ]
    scoped_query = {filtered: {filter: {and: filters}}}

    aggs_query = {
        aggs: {
            top_tags: {
                terms: {
                    field: 'tags',
                    size: limit
                },
                aggs: {
                    top_tag_hits: {
                        top_hits: {
                            sort: [
                                {
                                    created_at: {
                                        order: 'desc'
                                    }
                                }
                            ],
                            size: 1
                        }
                    }
                }
            }
        }
    }

    aggs_query[:query] = scoped_query

    results = UserContentsQueue.gateway.client.search(index: UserContentsQueue.index_name, body: aggs_query)
    results['aggregations']['top_tags']['buckets'].collect {|item| item['key'] if item['doc_count'] > 0}
  end

  def self.delete_by_query(query)
    log.info "Executing query -> #{query}"
    find_each(query: query) do |doc|
      doc.delete
    end
  end

  def self.remove_from_queue(user, content)
    content_type = "Card" if !content.class.to_s.index('Card').nil?
    content_type = "VideoStream" if !content.class.to_s.index('VideoStream').nil?
    log.error "content_type not found for user:#{user.id}, content_id: #{content.id}, content_type:#{content.class.to_s}" if content_type.nil?
    UserContentsQueue.dequeue_for_user(user_id: user.id, content_id: content.id, content_type: content_type)
  end

  def load_rationale
    begin
      rationale = metadata.first['rationale']
      case rationale['type']
      when 'following_channel'
        channel = Channel.find_by(id: rationale['channel_id'])
        "Because you are following #{channel.label}"
      when 'following_user'
        user = User.find_by(id: rationale['user_id'])
        "Because you are following #{user.name}"
      when 'user_interest'
        interest = Interest.find_by(id: rationale['interest_id'])
        "Because you are interested in #{interest.name}"
      else
        log.warn "Could not match rationale type for queue item id: #{id}, for user: #{user_id}, content_id: #{content_id}, content_type: #{content_type}. Using default rationale"
        'Content you might be interested in'
      end
    rescue => e
      log.warn "Error when getting rationale for queue item id: #{id}, for user: #{user_id}, content_id: #{content_id}, content_type: #{content_type}. Using default rationale. Error: #{e.message}"
      'Content you might be interested in'
    end
  end

  def self.ineligible_user_ids(user_ids:, content_id:, content_type:)
    dismissed_content_user_ids = DismissedContent.where(user_id: user_ids, content_id: content_id, content_type: content_type).pluck(:user_id)
    bookmark_user_ids = Bookmark.where(user_id: user_ids, bookmarkable_id: content_id, bookmarkable_type: content_type).pluck(:user_id)
    content_completion_user_ids = UserContentCompletion
                                      .where(user_id: user_ids, completable_id: content_id, completable_type: 'Card', state: UserContentCompletion::COMPLETED)
                                      .pluck(:user_id)
    (dismissed_content_user_ids + bookmark_user_ids + content_completion_user_ids).map(&:to_s).uniq
  end

  def self.ineligible_content_ids(user_id:, content_ids:, content_type:)
    dismissed_content_ids = DismissedContent.where(user_id: user_id, content_id: content_ids, content_type: content_type).pluck(:content_id)
    bookmarkable_ids = Bookmark.where(user_id: user_id, bookmarkable_id: content_ids, bookmarkable_type: content_type).pluck(:bookmarkable_id)
    completable_ids = UserContentCompletion
                          .where(user_id: user_id, completable_id: content_ids, completable_type: content_type, state: UserContentCompletion::COMPLETED)
                          .pluck(:completable_id)
    (dismissed_content_ids + bookmarkable_ids + completable_ids).uniq
  end
end

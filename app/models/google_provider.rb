# == Schema Information
#
# Table name: identity_providers
#
#  id            :integer          not null, primary key
#  type          :string(191)
#  uid           :string(191)
#  user_id       :integer
#  token         :string(4096)
#  secret        :string(255)
#  expires_at    :datetime
#  created_at    :datetime
#  updated_at    :datetime
#  token_scope   :string(255)
#  auth          :boolean
#  auth_info     :text(65535)
#  refresh_token :string(255)
#

class GoogleProvider < IdentityProvider

  def share(message: nil, object: nil)
    log.warn "Attempted Google share by user #{user.id}"
    return nil
  end

  def scoped?(_)
    return false
  end

  def self.is_verified?(auth_hash)
    auth_hash[:info][:email] && auth_hash[:extra][:raw_info][:email_verified]
  end
end

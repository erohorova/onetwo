# == Schema Information
#
# Table name: notification_entries
#
#  id                         :integer          not null, primary key
#  user_id                    :integer          not null
#  event_name                 :string(191)      not null
#  notification_name          :string(191)      not null
#  sourceable_type            :string(191)      not null
#  sourceable_id              :integer          not null
#  config_web_single          :boolean          not null
#  status_web_single          :integer          not null
#  web_text                   :string(512)
#  config_email_single        :boolean          not null
#  status_email_single        :integer          not null
#  config_email_digest_daily  :boolean          not null
#  status_email_digest_daily  :integer          not null
#  config_email_digest_weekly :boolean          not null
#  status_email_digest_weekly :integer          not null
#  config_push_single         :boolean          not null
#  status_push_single         :integer          not null
#  config_push_digest_daily   :boolean          not null
#  status_push_digest_daily   :integer          not null
#  config_push_digest_weekly  :boolean          not null
#  status_push_digest_weekly  :integer          not null
#  date_in_timezone           :date             not null
#  sending_time_in_timezone   :datetime         not null
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#

class NotificationEntry < ActiveRecord::Base
  belongs_to :user
  belongs_to :sourceable, polymorphic: true

  after_commit :send_single_notification_async, on: :create

  serialize :web_text

  serialize :additional_content

  def self.unsent_for_sql(medium, interval)
    "(#{Notify.config_field_name(medium, interval)} = 1 AND
      #{Notify.status_field_name(medium, interval)} = #{Notify::STATUS_UNSENT})"
  end

  def enabled?(medium, interval)
    send(Notify.config_field_name(medium, interval))
  end

  def set_enabled(medium, interval)
    send("#{Notify.config_field_name(medium, interval)}=", true)
  end

  def set_disabled(medium, interval)
    send("#{Notify.config_field_name(medium, interval)}=", false)
  end

  def set_status(medium, interval, status)
    send("#{Notify.status_field_name(medium, interval)}=", status)
  end

  def get_status(medium, interval)
    send(Notify.status_field_name(medium, interval))
  end

  def self.status_hash(medium, interval, status)
    params = {}
    params[Notify.status_field_name(medium, interval)] = status

    params
  end

  def send_single_notification_async
    if self.enabled?(:push, :single) || self.enabled?(:email, :single)
      SendSingleNotificationJob.perform_later(self.id)
    end
  end
end

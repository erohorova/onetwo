# == Schema Information
#
# Table name: user_badges
#
#  id         :integer          not null, primary key
#  badging_id :integer
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  identifier :string(10)
#

class UserBadge < ActiveRecord::Base
  include EventTracker
  belongs_to :card_badging, foreign_key: :badging_id
  belongs_to :clc_badging, foreign_key: :badging_id
  belongs_to :user
  belongs_to :badging, foreign_key: :badging_id

  before_create :generate_identifier
  after_create :send_new_clc_badge_notification
  after_commit :record_badge_completion_event, on: :create

  validates :badging_id, :user_id, presence: true
  validates :card_badging, uniqueness: { scope: :user }
  validates :identifier, uniqueness: true

  def generate_identifier
    self.identifier = loop do
      random_identifier = SecureRandom.urlsafe_base64(6, false)
      break random_identifier unless UserBadge.exists?(identifier: random_identifier)
    end
  end

  def send_new_clc_badge_notification
    if self.clc_badging.present?
      NotificationGeneratorJob.perform_later({item_id: self.id, item_type: 'clc', notification_type: 'won_badge', user_id: user.id})
    end
  end

  def record_badge_completion_event
    event_args = {
      event: Analytics::UserBadgeRecorder::EVENT_USER_BADGE_COMPLETED,
      actor: :user,
      job_args: [{user_badge: :itself}]
    }
    record_custom_event(event_args)
  end
end

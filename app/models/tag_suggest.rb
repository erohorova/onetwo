# See https://www.elastic.co/guide/en/elasticsearch/reference/current/suggester-context.html
class TagSuggest
  include Elasticsearch::Persistence::Model

  self.index_name = "tag_suggest_#{Rails.env}"

  attribute :name, String, mapping: { type: 'string', index: 'not_analyzed' }
  attribute :group_id, Integer
  attribute :tag_type, String, mapping: { type: 'string', index: 'not_analyzed' }
  attribute :organization_id, Integer

  attribute :suggest_name, String, default: {}, mapping: { type: 'completion', payloads: true, context: {group_id: {type: 'category', default: "0"}, organization_id: {type: 'category', default: "0"}, tag_type: {type: 'category', default: 'none'}}}

  def self.index_for_forum_autosuggest(_name, _group_id)
    _group_id ||= 0
    existing = self.search(filter: {and: [{term: {name: _name}}, {term: {group_id: _group_id}}]}).first

    if existing
      existing.suggest_name['weight'] += 1
      existing.save
    else
      tag_suggest_params = {suggest_name: {input: _name, context: {group_id: _group_id.to_s}, weight: 1, output: _name}, name: _name, group_id: _group_id}
      self.create(tag_suggest_params)
    end

    gateway.refresh_index!
  end

  def self.index_for_destiny_autosuggest(_name, _organization_id, _tag_type)
    _organization_id ||= Organization.default_org&.id
    _tag_type ||= 'none'

    # For interests, we index them under both the default 'none' category and
    # the specific 'Interest' category, so they are available in both sets
    # Seems like there should be an easier way
    context_tag_type = [_tag_type]
    if _tag_type != 'none'
      context_tag_type << 'none'
    end

    existing = self.search(filter: {and: [{term: {name: _name}}, {term: {organization_id: _organization_id}}]}).first

    if existing
      existing.tag_type = _tag_type # If tag type is upgraded to Interest, change it here
      existing.suggest_name['weight'] += 1
      existing.suggest_name['context']['tag_type'] = context_tag_type
      existing.save
    else
      tag_suggest_params = {suggest_name: {input: _name, context: {organization_id: _organization_id.to_s, tag_type: context_tag_type}, weight: 1, output: _name}, name: _name, organization_id: _organization_id, tag_type: _tag_type}
      self.create(tag_suggest_params)
    end

    gateway.refresh_index!
  end

  def self.suggestions_for_forum(term: , size: 10, group_id: 0)
    client = self.gateway.client
    res = client.suggest(index: index_name, body: {tags: {text: term, completion: {field: 'suggest_name', size: size, context: {group_id: group_id.to_s, organization_id: "0", tag_type: "none"}}}})
    options = res.try(:[], 'tags').try(:first).try(:[], 'options')
    options.nil? ? [] : options.map{|o| o['text']}
  end

  def self.get_blank_state(size: , organization_id: , tag_type: )
    self.search(filter: {and: [{term: {tag_type: tag_type}}, {term: {organization_id: organization_id}}]}, size: size).results
  end

  def self.suggestions_for_destiny(term: , size: 10, organization_id: , tag_type: 'none')
    client = self.gateway.client
    if term.blank?
      res = self.get_blank_state(size: size, organization_id: organization_id, tag_type: tag_type)
      return res.map{|r| r['name']}
    else
      res = client.suggest(index: index_name, body: {tags: {text: term, completion: {field: 'suggest_name', size: size, context: {group_id: "0", organization_id: organization_id, tag_type: tag_type}}}})
      options = res.try(:[], 'tags').try(:first).try(:[], 'options')
      return options.nil? ? [] : options.map{|o| o['text']}
    end
  end

  def self.reindex
    gateway.create_index! force: true
    Tagging.find_each do |tg|
      self.index_for_autosuggest(tg)
    end
  end

  def self.index_for_autosuggest _tagging
    taggable = _tagging.taggable
    if ((taggable.kind_of?(Post) && taggable.postable_type == 'Group') ||
            (taggable.kind_of?(Comment) && taggable.commentable_type == 'Post' && taggable.commentable.postable_type == 'Group'))
      self.index_for_forum_autosuggest(_tagging.tag.name, taggable.group_id)
    else
      self.index_for_destiny_autosuggest(_tagging.tag.name, taggable.try(:organization_id), _tagging.tag.type)
    end
  end
end

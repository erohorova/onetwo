class UserPurchase < ActiveRecord::Base

  validates :transaction_id, :order_id, :user_id, presence: true

  belongs_to :user_transaction, foreign_key: "transaction_id", class_name: "Transaction"
  belongs_to :order
end

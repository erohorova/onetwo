class Transaction < ActiveRecord::Base
  include AASM

  serialize :gateway_data
  enum payment_state: {initiated: 0,
                       canceled: 4,
                       processing: 6,
                       failed: 2,
                       pending: 5,
                       successful: 1,
                       voided: 7,
                       unknown: 3}
  # payments life cycle :
  #  initiated   => processing/canceled
  #  processing  => failed/pending
  #  pending     => successful/failed/voided

  aasm :column => :payment_state, :enum => true do
    state :initiated, :initial => true    
    state :canceled   
    state :processing   
    state :failed, :before_enter => :set_gateway_information   
    state :pending   
    state :successful, :before_enter => :set_gateway_information   
    state :voided, :before_enter => :set_gateway_information 

    error_on_all_events :log_error_and_update_order

    # Indicates that the payment is being processed and authorization will be done
    event :start_processing do
      transitions from: :initiated, to: :processing
    end

    # The transaction has been cancelled
    event :cancel do
      transitions from: :initiated, to: :canceled
    end

    # The payment has been processed but is not yet complete. Authorization has been done but not captured
    event :complete_processing do
      after do
        complete_order
      end
      transitions from: :processing, to: :pending
    end

    # The payment i.e capture is successful
    event :success do
      transitions from: :pending, to: :successful
    end

    # There was a failure in the payment
    event :failure do
      transitions from: [:pending, :processing, :initiated], to: :failed
    end

    # The payment was not captured as the course was not unlocked
    event :void do
      transitions from: :pending, to: :voided
    end
  end

  validates_inclusion_of :gateway, in: ['braintree', 'wallet', 'paypal']

  has_many :transaction_audits
  has_many :user_purchases

  validates :order_id, :amount, :currency, :payment_state, :user_id, presence: true
  validates :order_id, uniqueness: { scope: [:user_id, :gateway, :amount, :currency],
                                     message: "Duplicate transaction for the same Order." }

  belongs_to :order
  belongs_to :user

  after_save :update_order_status, on: [:update]
  after_save :create_transaction_audit
  after_save :create_user_purchase, :create_user_subscription, :create_wallet_transaction, :create_card_subscription, on: [:update], if: -> {
              (payment_state == PaymentGateway::STATUS_SUCCESSFUL)}

  attr_accessor :gateway_instance, :external_gateway_object

  # record transaction_audit for every create/update of
  # transaction (successful/failure/error transaction).
  def create_transaction_audit
    transaction_data = { gateway_data: (self.reload.gateway_data || ""), payment_state: payment_state }
    TransactionAudit.create_audit!(transaction_id: id,
                                   order_data: order.reload,
                                   orderable_prices_data: order.orderable.prices,
                                   orderable_data: order.orderable,
                                   user_data: user,
                                   transaction_data: transaction_data)
  end

  def create_user_subscription
    orderable_type = order.orderable_type
    orderable = order.orderable
    if (orderable_type == "OrgSubscription")
      user.user_subscriptions.find_or_create_by!(
        org_subscription_id: orderable.id,
        organization_id: order.organization_id,
        order_id: order.id,
        amount: amount,
        currency: currency,
        gateway_id: gateway_id) do |user_subscription|
        user_subscription.start_date = Date.today
        user_subscription.end_date = (Date.today + 1.year - 1.day)
      end
    end
  end

  # create card_subscription record for every card payment.
  def create_card_subscription
    orderable_type = order.orderable_type
    orderable = order.orderable
    if orderable_type == "Card"
      card_subscription = CardSubscription.create_card_subscription(
        start_date: Time.now,
        organization_id: orderable.organization_id,
        card_id: orderable.id,
        user_id: user.id,
        transaction_id: id)
    end
  end

  # create wallet_transaction credit record for every recharge
  def create_wallet_transaction
    orderable_type = order.orderable_type
    orderable = order.orderable
    if orderable_type == "Recharge"
      user.wallet_transactions.find_or_create_by!(order_id: order.id,
                                                  user_id: user_id,
                                                  payment_type: "credit",
                                                  gateway_data: "",
                                                  amount: orderable.skillcoin,
                                                  payment_state: "successful")
    end
  end

  # create user purchases after successful transaction
  def create_user_purchase
    user_purchases.find_or_create_by!(order_id: order_id,
                                      user_id: user_id)
  end

  def log_error_and_update_order
    log.error "Error while transitioning from #{aasm.from_state} to #{aasm.to_state} Event #{aasm.current_event}"
    self.errors.add(:base, :payment_error, message: "Error in payment during event: #{aasm.current_event}")
    order.update_column(:state, 2)
  end

  # update order status after success/failure/unknown transaction.
  def update_order_status
    state = case payment_state
            when "successful"
              1
            when "failed", "unknown", "voided", "canceled"
              2
            end
    order.update_attribute(:state, state) unless (order.processed? || state.nil?)
  end

  def check_duplicate_transaction
    Transaction.where(order_id: order_id).exists?(payment_state: 1)
  end

  # update gateway_id and gateway_data for audit purposes
  def set_gateway_information
    result = gateway_instance.typecast_object(external_gateway_object)
    update_gateway_information(gateway_id: result[:gateway_id], gateway_data: result[:gateway_data])
  end

  def update_gateway_information(gateway_id:, gateway_data:)
    update_columns(gateway_id: gateway_id, gateway_data: gateway_data)
  end

  # Fetch record if present with state initiated, else create a new record
  def self.initialize_payment!(order_id:, gateway:, amount:, currency:, user_id:, source_name:, order_type:)
    Transaction.find_or_create_by!(order_id: order_id,
                                   gateway: gateway,
                                   amount: amount,
                                   currency: currency,
                                   user_id: user_id,
                                   payment_state: 'initiated') do |transaction|
      transaction.source_name = source_name
      transaction.order_type = order_type
    end
  end

  # Call unlock api of content provider for Card. Will return true otherwise
  def confirm_order
    return true if (order&.orderable_type != 'Card')
    card = order.orderable
    begin
      card.authorize_course(card_transaction: self, user: order.user)
    rescue => e
      log.error "Error while authorizing course after transaction
                        Transaction Id : #{id} for Card Id : #{card.id} Error : #{e}"
      return false
    end
  end

  def mark_failure_and_handle_error(errored_step:)
    failure!
    error = gateway_instance.get_error(external_gateway_object)
    self.errors.add(:base, :payment, message: "#{errored_step} failed. #{error}")
  end

  # Call Authorize api of the gateway. If successful, call complete_processing! event.
  # Else add errors to transaction object and call failure! event
  # Validation and processor/gateway declined errors will be raised here
  def authorize_payment(args)
    self.external_gateway_object = gateway_instance.authorize_payment(self, args)
    external_gateway_object.success? ? complete_processing! : mark_failure_and_handle_error(errored_step: 'Authorization')
  end

  # Call confirm_order to unlock course for card. If successful, only then call capture_payment api
  # Else void the payment
  def complete_order
    confirm_order ? capture_payment : void_payment
  end

  # Actual payment will be captured here. If successfully captured, transaction is successful
  # Else add errors to transaction object and call failure! event
  # Api failing at this step is very rare.
  def capture_payment
    gateway_instance.capture_payment(transaction: self, gateway: external_gateway_object) ? success! : mark_failure_and_handle_error(errored_step: 'Payment Capture')
  end

  # Only authorized payments can be voided. Once voided, user will not be charged with the amount
  def void_payment
    gateway_instance.void_payment(gateway: external_gateway_object) ? void! : failure!
    self.errors.add(:base, :payment, message: "Error while purchasing the course. Please note that you will not be charged for it.")
  end

  def process_payment!(args)
    if (payment_state == 'successful') || check_duplicate_transaction
      raise PaymentAlreadyProcessedError
    end

    self.gateway_instance = PaymentGateway.gateway_cls(gateway).new
    start_processing!
    authorize_payment(args)

    self
  end

  def cancel_payment!
    if canceled?
      raise TransactionAlreadyCancelledError
    end
    cancel!
    self
  end

  def mark_unknown(gateway_data:)
    update(payment_state: 'unknown', gateway_data: gateway_data)
  end

  def via_wallet?
    (gateway == 'wallet')
  end
end

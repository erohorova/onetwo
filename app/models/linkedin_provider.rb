# == Schema Information
#
# Table name: identity_providers
#
#  id            :integer          not null, primary key
#  type          :string(191)
#  uid           :string(191)
#  user_id       :integer
#  token         :string(4096)
#  secret        :string(255)
#  expires_at    :datetime
#  created_at    :datetime
#  updated_at    :datetime
#  token_scope   :string(255)
#  auth          :boolean
#  auth_info     :text(65535)
#  refresh_token :string(255)
#

class LinkedinProvider < IdentityProvider

  def share(message: nil, object: nil)

    client = LinkedIn::Client.new(Settings.linkedin.api_key,
                                  Settings.linkedin.secret,
                                  self.token)


    result = client.add_share(comment: message,
                      content: {
                          title: object.title,
                          description: object.message_text,
                          submitted_url: object.ref_url
                      }
                    )

    ActiveSupport::JSON.decode(result.body)['updateUrl']
  end

  def scoped?(scope)
    return super if token_scope.nil?

    scope_string = {
        network: 'r_network',
        share: 'rw_nus',
    }[scope]

    return token_scope.split.include?(scope_string)
  end

  def self.is_verified?(auth_hash)
    true #linkedin accounts are always verified
  end

end

# == Schema Information
#
# Table name: access_logs
#
#  id                         :integer          not null, primary key
#  user_id                    :integer
#  platform                   :string(191)
#  access_uri                 :string(4096)
#  access_at                  :date
#  created_at                 :datetime
#  annotation                 :string(255)
#  is_signup_event            :boolean          default(FALSE)
#  insider_follow_processed   :boolean          default(FALSE)
#  insiders_already_following :text(65535)
#

# DEPRECATED
class AccessLog < ActiveRecord::Base
  belongs_to :user
  validates :platform, inclusion: ['web', 'ios', 'forum', 'android']
end

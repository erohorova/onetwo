# == Schema Information
#
# Table name: assignment_notice_statuses
#
#  id                  :integer          not null, primary key
#  user_id             :integer
#  notice_dismissed_at :datetime         default(NULL)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class AssignmentNoticeStatus < ActiveRecord::Base
  belongs_to :user
end

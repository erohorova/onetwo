class UserMetricsAggregation < ActiveRecord::Base
  belongs_to :user
  belongs_to :organization
  PERIODS = ['year', 'month', 'week']

  validates_presence_of *%i{
    organization_id            user_id
    smartbites_commented_count smartbites_completed_count
    smartbites_consumed_count  smartbites_created_count
    smartbites_liked_count     time_spent_minutes
    total_smartbite_score      total_user_score
    end_time                   start_time
  }


  # fetch avg time spent by the org users
  def self.avg_time_spent_by_users(org_id:, period: "year", limit: 10, offset: 0)
    if !PERIODS.include?(period)
      return "Invalid period: #{period}. Valid periods: #{PERIODS.join(",")}"
    end
    
    select_fields, group_by_fields = query_data(period)
    query = <<-SQL.strip_heredoc
      SELECT
        SUM(time_spent_minutes)/COUNT(distinct user_id)   as avg_time_spent,
        #{select_fields}
      FROM user_metrics_aggregations
      WHERE organization_id = :organization_id
      GROUP BY #{group_by_fields}
      ORDER BY created_at DESC
      LIMIT :limit
      OFFSET :offset
    SQL
    query_params = { organization_id: org_id, limit: limit, offset: offset}
    UserMetricsAggregation
      .using(:replica1)
      .find_by_sql([query, query_params])
      .map { |value| value.attributes.compact }
  end

  # fetch sum of  time spent by given user
  def self.time_spent_by_user(user_id:, period: "year", limit: 20, offset: 0)
    if !PERIODS.include?(period)
      return "Invalid period: #{period}. Valid periods: #{PERIODS.join(",")}"
    end
    select_fields, group_by_fields = query_data(period)
    query = <<-SQL.strip_heredoc
      SELECT
        SUM(time_spent_minutes) as time_spent,
        #{select_fields}
      FROM user_metrics_aggregations
      WHERE user_id = :user_id
      GROUP BY #{group_by_fields}
      ORDER BY created_at DESC
      LIMIT :limit
      OFFSET :offset
    SQL
    query_params = { user_id: user_id, limit: limit, offset: offset}
    UserMetricsAggregation
      .using(:replica1)
      .find_by_sql([query, query_params])
      .map { |value| value.attributes.compact }
  end

  def self.query_data (period)
    select_fields     = ""
    group_by_fields   = ""
    case period
    when "year"
      select_fields   = "YEAR(created_at) as year"
      group_by_fields = "year"
    when "month"
      select_fields   = "YEAR(created_at) as year, MONTH(created_at) as month"
      group_by_fields = "year, month"
    when "week"
      select_fields   = "YEAR(created_at) as year, MONTH(created_at) as month, WEEK(created_at) as week"
      group_by_fields = "year, month, week"
    end 
    [select_fields,group_by_fields]      
  end

end

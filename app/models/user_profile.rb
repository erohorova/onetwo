# == Schema Information
#
# Table name: user_profiles
#
#  id                 :integer          not null, primary key
#  user_id            :integer
#  expert_topics      :text(65535)
#  learning_topics    :text(65535)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  hr_job_role_id     :integer
#  hr_organization_id :integer
#  hr_location_id     :integer
#  hr_competency_id   :integer
#  hr_domain_id       :integer
#  time_zone          :string(64)
#  onboarding_options :text(65535)
#  tac_accepted       :boolean
#  language           :string(10)
#  dob                :date
#  job_title          :string
#

class UserProfile < ActiveRecord::Base

  include EventTracker

  VALID_LEVELS = [1,2,3].freeze
  SKILL_LEVELS = { 1 => 'beginner', 2 => 'intermediate', 3 => 'advanced' }
  VALID_LANGUAGES = {
    'English' => 'en', 'Russian' => 'ru', 'French' => 'fr', 'Swedish' => 'sv',
    'Dutch (Belgium)' => 'nl-BE', 'Italian' => 'it', 'Polish' => 'pl',
    'French (Canada)'=> 'fr-CA', 'Spanish' => 'es', 'German' => 'de',
    'Tamil (India)' => 'ta-IN', 'Hindi' => 'hi', 'Telugu' => 'te', 'Gujarati' => 'gu',
    'Marathi (India)' => 'mr-IN', 'Portuguese' => 'pt', 'Greek' => 'el', 'Czech' => 'cs-CZ',
    'Serbian (Serbia)' => 'sr-Latn-RS', 'Ukrainian' =>'uk-UA', 'Turkish' =>'tr-TR',
    'Malay (Malaysia)' => 'ms-Latn-MY', 'Lithuanian' => 'lt-LT', 'Hungarian' => 'hu-HU',
    'Japanese' => 'ja-JP', 'Chinese' => 'zh-CN', 'Chinese (Traditional)' => 'zh-Hans-CN',
    'Portuguese (Brazil)' => 'pt-BR', 'Romanian (Romania)' => 'ro-RO', 'Arabic' => 'ar',
    'Hebrew' => 'he', 'Slovak (Slovakia)' => 'sk-SK', 'Urdu' => 'ur',
    'Indonesian (Indonesia)' => 'id-ID'
  }

  VALID_LANGUAGES_LOOKUP = VALID_LANGUAGES.reduce({}) do |memo, (key, val)|
    memo.merge(key.downcase => val, val.downcase => val)
  end

  serialize :expert_topics, Array
  serialize :learning_topics, Array
  serialize :dashboard_info, Array
  serialize :onboarding_options, Hash

  belongs_to :user, inverse_of: :profile

  before_validation :set_language

  validates :user, presence: true
  validate :check_users_age, if: :dob_changed?
  validate :validate_time_zone, if: :time_zone_changed?
  validate :validate_learning_topic_level, if: :learning_topics_changed?
  validates :language, inclusion: { in: VALID_LANGUAGES.values }, length: { in: 2..10 }, allow_blank: true
  validates :job_title, length: { in: 2..50 }, allow_blank: true

  after_commit :reindex_user

  after_commit :auto_follow_associated_channels, if: :trigger_auto_follow_for_channels
  after_commit :auto_assign_pathways_for_learning

  after_update :record_user_profile_update_event

  before_save :set_tac_accepted_at, if: :tac_accepted_changed?

  def set_tac_accepted_at
    if tac_accepted
      self.tac_accepted_at = Time.now
    end
  end

  def auto_follow_associated_channels
    topic_ids = self.learning_topics.map{|topic| topic['topic_id']} | self.expert_topics.map{|topic| topic['topic_id']}
    channels_to_follow = []
    # the new sociative taxonomy node sends topic_id as a long number not alphanumeric
    # topic column in channel renders it as an explicit string by adding '' since it's a text column
    topic_ids.each do |topic_id|
       channels_to_follow += self.user.organization.channels
        .not_private
        .where("topics like ? or topics like ?", "%id: #{topic_id}%", "%id: '#{topic_id}'%")
    end
    channels_to_follow.uniq.each do |channel|
      channel.followers << self.user unless user.following?(channel)
    end
  end

  def auto_assign_pathways_for_learning
    if user.organization.auto_assign_feature_enabled? && previous_changes.include?(:learning_topics)
      AutoAssignPathwaysForLearningJob.perform_later(user_id)
    end
  end

  def self.set_profile(user:, attributes:)
    profile_attributes = attributes.select { |key, value| value.present? }
    return if profile_attributes.empty?

    user_profile = user.profile || user.build_profile
    user_profile.assign_attributes(profile_attributes)
    user_profile.save
  end

  private

  def trigger_auto_follow_for_channels
    self.user.organization.autofollow_based_on_topic? && (self.previous_changes.key?(:learning_topics) || self.previous_changes.key?(:expert_topics))
  end

  def reindex_user
    user.reindex_async
  end

  def check_users_age
    if (dob > 13.years.ago.to_date)
      errors.add(:dob, 'Users age has to be greater than or equal to 13')
    end
  end

  def validate_time_zone
    errors.add(:time_zone, 'Invalid time zone') if Time.find_zone(self.time_zone).nil?
  end

  def validate_learning_topic_level
    self.learning_topics.each{|topic|
      if topic[:level].present?
        unless (VALID_LEVELS.include? topic[:level].try(:to_i))
          errors.add(:learning_topics, 'Invalid value for learning topic level')
        else
          topic[:level] = topic[:level].try(:to_i)
        end
      end
    }
  end

  def record_user_profile_update_event
    metadata = RequestStore.read(:request_metadata) || {}
    actor = find_actor metadata
    if actor
      # Expert topics and learning topic changes are handled uniquely
      # We do a diff on the new vs old topics and record a different
      # event for each difference.
      record_changes_to_expert_and_learning_topics(actor, changes, metadata)
      # Most of the other profile attributes are tracked through
      # the user_edited event.
      record_changes_to_profile_attributes(actor, changes)
    end
  end

  def record_changes_to_expert_and_learning_topics(actor, changes, metadata)
    changed_profile_keys = changes.keys & %w{expert_topics learning_topics}
    changed_profile_keys.each do |key|
      # Note, this recorder functions differently to the others because the
      # events it handles are actually registered to UserMetricsRecorder.
      # So we use perform_later instead of record_custom_event here.
      Analytics::MetricsRecorderJob.perform_later(
        recorder: "Analytics::UserProfileRecorder",
        actor: Analytics::MetricsRecorder.user_attributes(actor),
        org: Analytics::MetricsRecorder.org_attributes(actor.organization),
        user_profile: Analytics::MetricsRecorder.user_profile_attributes(self),
        changed_profile_keys: changes.slice(key),
        timestamp: Time.now.to_i,
        metadata: metadata
      )
    end
  end

  def record_changes_to_profile_attributes(actor, changes)
    whitelisted = Analytics::UserMetricsRecorder.whitelisted_profile_change_attrs
    (changes.keys & whitelisted).each do |change_key|
      record_custom_event(
        event: "user_edited",
        actor: actor,
        job_args: [{
          user: user,
          changed_column: change_key,
          old_val: changes[change_key][0].to_s,
          new_val: changes[change_key][1].to_s
        }]
      )
    end
  end

  def find_actor(metadata)
    User.find_by(id: metadata[:user_id]) || user
  end

  def set_language
    if language.present?
      valid_language = VALID_LANGUAGES_LOOKUP[language.downcase]
      self.language = valid_language if valid_language
    end
  end
end

# == Schema Information
#
# Table name: clc_organizations_records
#
#  created_at      :datetime
#  user_id         :integer
#  score           :integer
#  card_id         :integer
#  organization_id :integer
#

# We are using this table to save raw data for score per user per card
# score is in minute
class ClcOrganizationsRecord < ActiveRecord::Base
  belongs_to :user
  belongs_to :card
  belongs_to :organization

  validates :user_id, :card_id, :organization_id, presence: true
  validates :user_id, uniqueness: {scope: :card_id}
end

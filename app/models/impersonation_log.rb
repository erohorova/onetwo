# == Schema Information
#
# Table name: impersonation_logs
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  target_id  :integer
#  created_at :datetime
#  updated_at :datetime
#

class ImpersonationLog < ActiveRecord::Base
  belongs_to :user
  belongs_to :target, class_name: 'User'
end

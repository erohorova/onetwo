class SearchQuery
  include Elasticsearch::Persistence::Model
  STOP_WORDS = YAML.load(File.read "config/stop_words.yml")

  self.index_name = "search_queries_#{Rails.env}_v1"

  attribute :user_id, Integer
  attribute :organization_id, Integer
  attribute :query, String, mapping: { index: 'not_analyzed' }
end

# == Schema Information
#
# Table name: developer_api_credentials
#
#  id              :integer          not null, primary key
#  api_key         :string(255)
#  shared_secret   :string(255)
#  organization_id :integer
#  deleted_at      :datetime
#  created_at      :datetime
#  updated_at      :datetime
#

class DeveloperApiCredential < ActiveRecord::Base
  include EventTracker
  acts_as_paranoid

  belongs_to :organization
  belongs_to :creator, class_name: 'User', foreign_key: 'user_id'

  before_validation on: :create do
    self.api_key = SecureRandom.hex if self.api_key.blank?
    self.shared_secret = SecureRandom.hex(32) if self.shared_secret.blank?
  end

  validates :organization, :creator, presence: true
  validates :api_key, :shared_secret, presence: true, uniqueness: true

  configure_metric_recorders(
    default_actor: :creator,
    trackers: {
      on_create: {
        job_args: [{ org: :organization }],
        event_opts: lambda {|record| { developer_api_key: record.api_key } },
        event: 'org_developer_api_credentials_created'
      },
      on_delete: {
        job_args: [{ org: :organization }],
        event_opts: lambda {|record| { developer_api_key: record.api_key } },
        event: 'org_developer_api_credentials_deleted'
      }
    }
  )

  def self.fetch_api_credential_and_org(api_key: nil)
    org_credential = where(api_key: api_key).first
    [org_credential, org_credential.try(:organization)]
  end
end

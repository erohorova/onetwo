# == Schema Information
#
# Table name: comments
#
#  id               :integer          not null, primary key
#  message          :text(16777215)   not null
#  type             :string(255)
#  commentable_id   :integer
#  commentable_type :string(191)
#  user_id          :integer          not null
#  created_at       :datetime
#  updated_at       :datetime
#  votes_count      :integer          default(0)
#  resource_id      :integer
#  hidden           :boolean          default(FALSE)
#  anonymous        :boolean          default(FALSE)
#  is_mobile        :boolean          default(FALSE)
#  client_item_id   :string(191)
#  comments_count   :integer          default(0)
#

class Comment < ActiveRecord::Base
  acts_as_paranoid

  include Votable
  include Approvable
  include Reportable
  include Taggable
  include Mentionable
  include ResourceWorthy
  include AttachmentWorthy
  include StreamWorthy
  include Streamable
  include NotificationCleanup
  include EventTracker

  has_paper_trail ignore: [:created_at, :updated_at]

  belongs_to :client
  belongs_to :user
  belongs_to :commentable, polymorphic: true, counter_cache: :comments_count
  belongs_to :card, -> { where("comments.commentable_type = 'Card'") }, foreign_key: :commentable_id

  has_one :activity_stream, as: :streamable
  has_many :comments, as: :commentable, :dependent => :destroy

  has_many :notifications, as: :notifiable, :dependent => :destroy
  has_many :comment_reportings, dependent: :destroy

  after_save :associate_files, if: "file_ids.present?"
  after_create :add_tags
  after_create :add_mentions, if: :add_mentions?
  after_create :update_follows

  after_commit :generate_notification, on: :create
  after_commit :reindex_commentable_async
  after_commit :touch_commentable, on: :create, unless: :non_triggerable_comment?
  after_commit :update_video_stream_score, on: [:create, :destroy]
  after_commit :record_card_comment_event, on: :create
  before_destroy :record_card_uncomment_event

  validates :message, :presence => true
  validates :user, :presence => true
  validates :commentable, :presence => true

  validate :user_authorized
  before_save :remove_script_tags

  attr_accessor :created_by_staff, :file_ids

  acts_as_streamable on: :create

  add_to_stream group_id: ->(record){ record.commentable.group_id }, action: 'commented',
    if: ->(comment){ comment.commentable_type == 'Post' || comment.commentable_type == 'Comment' }

  remove_from_stream action: 'commented'

  def remove_script_tags
    self.message.gsub!(/<script.*?>[\s\S]*<\/script>/i, "")
  end

  def associate_files
    file_resources.each do |fr|
      fr.destroy unless file_ids.include?(fr.id)
    end

    frs = FileResource.where(id: file_ids)
    frs.each do |fr|
      self.file_resources << fr unless file_resources.include?(fr)
    end
  end

  def _taggable_fields
    [message]
  end

  def _resource_extraction_field
    message
  end

  def touch_commentable
    commentable.touch(:updated_at)
  end

  def generate_notification
    if commentable_type == 'Post' || commentable_type == 'Card'
      NotificationGeneratorJob.perform_later(item_id: id, item_type: 'comment')
    end
  end

  def reindex_commentable_async
    if self.commentable_type == 'Post' || self.commentable_type == 'Card'
      IndexJob.perform_later(self.commentable_type, self.commentable_id)
    end
  end

  def add_mentions?
    %w[Card VideoStream Team Organization ActivityStream].include?(commentable_type)
  end

  def search_data
    ret = as_json
    ret['tags'] = tags.map{|tag| {'id'=>tag.id, 'name'=>"##{tag.name}"}}
    ret['mentioned_users'] = mentioned_users.map &:capsule_data
    ret['file_resources'] = file_resources.as_json
    if resource
      ret['resource'] = resource.as_json
    end
    ret['user'] = user.capsule_data
    ret
  end

  def user_authorized
    errors.add(:user, 'User is not authorized to create this comment.') unless (commentable && user && commentable.can_comment?(user))
  end

  def can_comment? _user
    commentable.can_comment?(_user)
  end

  def visible_without_auth?
    !!commentable.try(:visible_without_auth?)
  end

  def group
    commentable ? commentable.try(:group) : nil
  end

  def group_id
    commentable ? commentable.try(:group_id) : nil
  end

  def organization
    commentable.class.name.eql?('Organization') ? commentable : commentable.try(:organization)
  end

  def created_by_staff?
    if self.group.nil?
      false
    else
      !self.anonymous && (if @created_by_staff.nil?
        self.group.is_active_super_user?(self.user)
      else
        @created_by_staff
      end)
    end
  end

  def update_follows
    # we currently don't include followable concern on cards
    if commentable.respond_to?(:notify_followers)
      commentable.notify_followers message, user.id, self.id
      commentable.follow user
    end
  end

  def trigger_event
    EDCAST_NOTIFY.trigger_event(Notify::EVENT_NEW_COMMENT, self)
  end

  def is_mobile?
    is_mobile
  end

  def creator_label
    gu = GroupsUser.where(user: user, group: group).first
    unless gu
      return nil
    end
    if gu.role_label
      return gu.role_label
    else
      return gu.as_type
    end
  end

  def snippet
    SimpleMessageExtractor.simple_text(message).truncate(60, separator: ' ')
  end

  def as_json(options={})
    super.merge 'resource' => self.resource.as_json
  end

  def update_video_stream_score
    if commentable.kind_of?(VideoStream)
      VideoStreamUpdateScoreJob.perform_later(commentable)
    end
  end

  def get_app_deep_link_id_and_type
    resource_id, resource_type = nil, nil
    resource_id, resource_type = commentable.public_send(:get_app_deep_link_id_and_type) if commentable.respond_to?(:get_app_deep_link_id_and_type)
  end

  def get_app_deep_link_id_and_type_v2
    resource_id, resource_type = nil, nil
    resource_id, resource_type = commentable.public_send(:get_app_deep_link_id_and_type_v2) if commentable.respond_to?(:get_app_deep_link_id_and_type_v2)
  end


  def activity_initiator
    user
  end

  def activity_stream_action
    ActivityStream::ACTION_COMMENT
  end

  def is_stream_worthy?
    return true if commentable.kind_of?(Team) || commentable.kind_of?(Organization)
    !commentable.kind_of?(Post) && commentable.try(:publicly_visible?) && !commentable.try(:hidden)
  end

  def non_triggerable_comment?
    %w(ActivityStream Team Organization).include?(self.commentable_type)
  end

  def visible_to_user(user)
    # visible if user is author or admin
    return true if (user_id.present? && user_id == user.id) || user.is_admin_user?
    # check commentable for visibility
    return false unless commentable.linkable_item.respond_to?(:visible_to_user)

    commentable.linkable_item.visible_to_user(user)
  end

  def recent_votes(limit = 10)
    up_votes.includes(:voter).order(id: :desc).limit(limit)
  end

  def is_reported_by?(current_user)
    comment_reportings.where(user: current_user).exists?
  end

  private

  def record_card_comment_event
    return if self.commentable.class != Card
    record_comment_event('card_comment_created')
  end

  def record_card_uncomment_event
    return if self.commentable.class != Card
    record_comment_event('card_comment_deleted')
  end

  def record_comment_event(event)
    record_custom_event(
      event: event,
      actor: :user,
      job_args: [
        { card: :commentable},
        { comment: :itself}
      ]
    )
  end

end

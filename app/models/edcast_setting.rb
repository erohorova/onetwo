# == Schema Information
#
# Table name: edcast_settings
#
#  id         :integer          not null, primary key
#  get        :boolean          default(FALSE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class EdcastSetting < ActiveRecord::Base

  def get_enabled?
    self.get
  end

  def self.get_enabled?
    return first.get_enabled? unless first.nil?
    return false
  end
  
end

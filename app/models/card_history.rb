class CardHistory
  include Elasticsearch::Persistence::Model
  extend ActionView::Helpers::NumberHelper

  self.index_name = "card_history_#{Rails.env}"

  # using id instead of card_id will update the existing index whenever we save new version
  attribute :card_id, Integer
  # id of user who made the change
  attribute :user_id, Integer
  # array of attributes that were updated
  attribute :updated_fields, Array, mapping: {type: 'object'}

  ATTRIBUTES_TO_RECORD = %w(title message duration filestack readable_card_type
    topics prices_attributes options can_be_reanswered is_paid is_public deleted_at
    level author_id
  )

  ATTRIBUTE_TO_ASSOCIATION_MAPPING = {
    options: :quiz_question_options,
    prices_attributes: :prices,
    card_metadatum_attributes: :card_metadatum
  }

  class << self
    def association_changes(card)
      changes = {}
      # resource_id is sent when a card is created
      changes["resource"] = resource_changes(card) if card.previous_changes.has_key?("resource_id")
      ATTRIBUTE_TO_ASSOCIATION_MAPPING.each_pair do |attribute, association|
        changes[attribute] = send("#{association}_changes", card)
      end
      changes
    end

    def create_new_version(card_id, user_id, changes)
      create(
        card_id: card_id,
        user_id: user_id,
        updated_fields: [changes]
      )
    end

    def fetch_versions(id:)
      search(query: {match: {card_id: id}}, sort: {created_at: "desc"})
    end

    # track changes not possible via association or direct attributes
    def misc_changes(card_id, changes)
      changes = track_resource_changes(card_id, changes)
      track_readable_card_type_name(changes)
    end

    private

    def track_readable_card_type_name(changes)
      return changes unless changes.has_key?("readable_card_type")
      readable_card_type_changes = changes["readable_card_type"]
      before = Card.readable_card_type_name(readable_card_type_changes[0])&.humanize&.titleize || ""
      after = Card.readable_card_type_name(readable_card_type_changes[1])&.humanize&.titleize || ""
      changes["readable_card_type"] = [before, after]
      changes
    end

    def track_resource_changes(card_id, changes)
      return changes if changes.has_key?("resource")
      return changes unless (card = Card.find_by(id: card_id))
      return changes unless (resource_id = card&.resource_id)
      resource_record = CardResourceHistory.fetch_record(resource_id)
      if resource_record
        changes["resource"] = resource_record.updated_fields
        resource_record.destroy
      end
      changes
    end

    def resource_changes(card)
      CardResourceHistory.get_changes(card.previous_changes["resource_id"])
    end

    def quiz_question_options_changes(card)
      card.card_type == "poll" ? poll_option_changes(card) : quiz_option_changes(card)
    end

    def quiz_option_changes(card)
      delta = card.quiz_question_options.each_with_object([]) do |option, arr|
        prev_changes = option.previous_changes.slice("label", "is_correct")
        ans = option.is_correct ? ": answer" : ""

        # when new option is added or when there is no update in option
        if prev_changes.blank?
          arr << ["#{option.label}#{ans}", "#{option.label}#{ans}"]
          next
        end

        # when existing options are updated
        keys = prev_changes.keys
        # when label is changed
        before = keys.include?("label") ? prev_changes["label"][0].to_s : option.label
        after = keys.include?("label") ? prev_changes["label"][1].to_s : option.label
        # when correct answer is changed
        if keys.include?("is_correct")
          before += prev_changes["is_correct"][0] ? ": answer" : ""
          after += prev_changes["is_correct"][1] ? ": answer" : ""
        else
          before += ans
          after += ans
        end

        arr << [before, after]
      end

      check_and_return_changes(delta)
    end

    def poll_option_changes(card)
      delta = card.quiz_question_options.each_with_object([]) do |option, arr|
        prev_changes = option.previous_changes.slice("label")
        # when new option is added or when there is no update in option
        if prev_changes.blank?
          arr << [option.label, option.label]
        else
          arr << prev_changes["label"]
        end
      end

      check_and_return_changes(delta)
    end

    def prices_changes(card)
      changes = card.prices.each_with_object([]) do |price, arr|
        prev = price.previous_changes
        # when new pricing records are added
        # we get changes in currency and amount
        # there is no other case when we can get change in currency
        if prev.has_key?("currency")
          before = readable_price(prev["amount"][0], prev["currency"][0])
          after = readable_price(prev["amount"][1], prev["currency"][1])
        elsif prev.has_key?("amount") # when amount is updated in existing price record
          before = readable_price(prev["amount"][0], price.currency)
          after = readable_price(prev["amount"][1], price.currency)
        else
          # when there is no change in existing pricing
          before = after = readable_price(price.amount, price.currency)
        end

        arr << [before, after]
      end
      check_and_return_changes(changes)
    end

    def card_metadatum_changes(card)
      card_metadatum = card.card_metadatum
      card_metadatum&.previous_changes
    end

    def readable_price(amount, currency)
      if currency == Settings.payment_currencies['edu_currency']
        "#{amount} #{Settings.payment_currencies['edu_currency']}"
      else
        number_to_currency(amount, unit: CURRENCY_TO_SYMBOL[currency])
      end
    end

    def check_and_return_changes(delta)
      before = delta.map { |a| a[0] }.reject(&:blank?).join(", ")
      after = delta.map { |a| a[1] }.reject(&:blank?).join(", ")
      before == after ? [] : [before, after]
    end
  end
end

#To define dynamic quarters for any organization

# == Schema Information
# Table name: quarters
#
#  id                   :integer          not null, primary key
#  organization_id      :integer
#  start_date           :date
#  end_date             :date
#  name                 :string(255)

class Quarter < ActiveRecord::Base
  VALID_QUARTERS = %w[quarter-1 quarter-2 quarter-3 quarter-4].freeze

  belongs_to :organization
  validate :validate_quarter_name
  validates :name, :organization_id, :start_date, :end_date, presence: true
  validates_uniqueness_of :name, scope: :organization_id
  after_commit :invalidate_cache

  private

  def invalidate_cache
    Rails.cache.delete("organization_quarter_config_#{organization_id}")
  end

  def validate_quarter_name
    return if name.blank?
    unless VALID_QUARTERS.include? name
      errors.add(:quarter, "name is invalid")
    end
  end

end

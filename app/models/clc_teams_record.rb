# == Schema Information
#
# Table name: clc_teams_records
#
#  created_at :datetime
#  user_id    :integer
#  score      :integer
#  card_id    :integer
#  team_id    :integer
#

class ClcTeamsRecord < ActiveRecord::Base
  belongs_to :user
  belongs_to :card
  belongs_to :team

  validates :user_id, :card_id, :team_id, presence: true
  validates :user_id, uniqueness: {scope: [:card_id, :team_id]}
end

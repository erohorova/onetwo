# == Schema Information
#
# Table name: uploads
#
#  id                :integer          not null, primary key
#  sender_id         :integer
#  output_url        :text(16777215)
#  created_at        :datetime
#  updated_at        :datetime
#  file_file_name    :string(255)
#  file_content_type :string(255)
#  file_file_size    :integer
#  file_updated_at   :datetime
#  type              :string(255)
#  group_id          :integer
#

class Upload < ActiveRecord::Base

  belongs_to :sender, :class_name => 'User'

  validates :sender, presence: true
  has_attached_file :file, validate_media_type: false
  do_not_validate_attachment_file_type :file
  validates :file, presence: true
  
end

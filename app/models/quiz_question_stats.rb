# == Schema Information
#
# Table name: quiz_question_stats
#
#  id                 :integer          not null, primary key
#  quiz_question_id   :integer
#  quiz_question_type :string(191)
#  stats              :text(16777215)
#  attempt_count      :integer          default(0)
#  lock_version       :integer          default(0)
#

class QuizQuestionStats < ActiveRecord::Base

  store :stats, accessors: [:options_stats], coder: JSON
  
  belongs_to :quiz_question, polymorphic: true

  validates :quiz_question, presence: true
  validates :quiz_question_id, uniqueness: {scope: :quiz_question_type}

  after_commit :reindex_quiz_question, on: [:create, :update]
  
  def as_json
    {
      'attempt_count' => attempt_count,
      'options_stats' => options_stats
    }
  end

  def reindex_quiz_question
    if quiz_question.respond_to?(:reindex)
      quiz_question.reindex
    end
  end
end

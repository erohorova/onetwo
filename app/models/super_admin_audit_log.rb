# == Schema Information
#
# Table name: super_admin_audit_logs
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  organization_id :integer
#  ip              :string(255)
#  user_agent      :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#

class SuperAdminAuditLog < ActiveRecord::Base

end

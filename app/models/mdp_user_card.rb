class MdpUserCard < ActiveRecord::Base

  NECESSARY_KEYS = %w(source_of_mdp sub_competency_value learning_activity)

  serialize :form_details, JSON

  belongs_to :card
  belongs_to :user

  validates :form_details, presence: true
  validate :necessary_form_details

  after_commit :create_user_content_completion, on: :create

  def create_user_content_completion
    # Creates UserContentCompletion record with state = 'initialized'
    user_completion = UserContentCompletion.where(user_id: self.user_id,
      completable_id: self.card_id, completable_type: 'Card').first_or_initialize

    if user_completion.persisted? # if record exists then change updated_at value
      user_completion.touch
    else
      user_completion.save
    end
  end

  private

  def necessary_form_details
    unless self.form_details.blank?
      # Checks all necessary key are passed
      if (required_keys = NECESSARY_KEYS - self.form_details.keys).present?
        errors.add(:form_details, "#{required_keys.join(',')} not present") and return
      end

      # Checks values are passed for all necessary key
      self.form_details.each do |key, val|
        if NECESSARY_KEYS.include?(key.downcase) && val.blank?
          errors.add(:form_details, "#{key} can't be blank")
        end
      end
    end
  end
end

# == Schema Information
#
# Table name: upsell_requests
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  fields      :text(65535)
#  approved    :boolean          default(FALSE)
#  upsell_type :string(255)
#

class UpsellRequest < ActiveRecord::Base
  belongs_to :user
  validates :user, presence: true
  validates :upsell_type, presence: true
  validates_inclusion_of :upsell_type, in: %w(channel)
end

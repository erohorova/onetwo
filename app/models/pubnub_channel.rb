# == Schema Information
#
# Table name: pubnub_channels
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  item_id    :integer
#  item_type  :string(191)
#  as_type    :string(191)
#  created_at :datetime
#  updated_at :datetime
#

class PubnubChannel < ActiveRecord::Base
  belongs_to :item, polymorphic: true

  before_validation do
    if self.name.nil?
      self.name = PubnubChannel.get_name_for(item_id: self.item_id, item_type: self.item_type, as_type: self.as_type)
    end
  end

  validates :name, presence: true, uniqueness: true
  validates :item, presence: true
  validates_uniqueness_of :as_type, :scope => [:item_id, :item_type]

  scope :private_channel, ->{where(as_type: 'private')}
  scope :public_channel, ->{where(as_type: 'public')}

  def self.get_name_for(item_id: , item_type: , as_type: )
    "#{item_type}-#{item_id}-#{as_type}-#{SecureRandom.hex}"
  end
end

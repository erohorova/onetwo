# == Schema Information
#
# Table name: ssos
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#  description :string(255)
#

class Sso < ActiveRecord::Base
  DEFAULT_SSO_NAMES = %w[email facebook google linkedin].freeze
  # email is not treated like an sso at all
  # email is part of this list just so that we can control all
  # entry points using the same logic.
  # for any other sso based access rule, email is excluded.

  has_many :org_ssos, dependent: :destroy
  has_many :organizations, through: :org_ssos

  validates :name, uniqueness: true
  validates_presence_of :name
end

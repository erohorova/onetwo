# == Schema Information
#
# Table name: quiz_question_attempts
#
#  id                 :integer          not null, primary key
#  user_id            :integer
#  quiz_question_id   :integer
#  quiz_question_type :string(191)
#  response           :text(16777215)
#  created_at         :datetime
#  updated_at         :datetime
#

class QuizQuestionAttempt < ActiveRecord::Base

  belongs_to :quiz_question, polymorphic: true
  belongs_to :user

  validates :user, presence: true
  validates :quiz_question, presence: true

  store :response, accessors: [:selections], coder: JSON

  after_commit :record_quiz_question_stats, on: :create
  after_commit :generate_poll_answer_notification, on: :create, if: :poll_answer?
  after_commit :create_poll_or_quiz_response_influx_metric, on: :create

  def self.has_user_attempted_quiz_question_card?(_card_id, _user_id)
    QuizQuestionAttempt.where(quiz_question_id: _card_id, quiz_question_type: 'Card', user_id: _user_id).present?
  end

  def self.attempted_option(card_id:, user:)
    attempt = user.quiz_question_attempts.where(quiz_question_id: card_id, quiz_question_type: 'Card').last
    attempt.try(:selected_option)
  end

  def create_poll_or_quiz_response_influx_metric
    metadata = RequestStore.read(:request_metadata) || {}
    Analytics::MetricsRecorderJob.perform_later(
      recorder: "Analytics::PollResponseMetricsRecorder",
      timestamp: Time.now.to_i,
      org: Analytics::MetricsRecorder.org_attributes(quiz_question.organization),
      actor: Analytics::MetricsRecorder.user_attributes(user),
      card: Analytics::MetricsRecorder.card_attributes(quiz_question),
      quiz_question_options: [selected_option].compact.map(
        &Analytics::MetricsRecorder.method(:quiz_question_option_attributes)
      ),
      additional_data: metadata
    )
  end

  def record_quiz_question_stats
    QuizQuestionStatsRecorderJob.perform_later({quiz_question_attempt_id: self.id})
  end

  def generate_poll_answer_notification
    NotificationGeneratorJob.perform_later(item_id: self.id, item_type: 'poll_answer')
  end

  def poll_answer?
    quiz_question_type == "Card"
  end

  # TODO as of now we only allow one option to be selected for a poll,
  # if we change it for multiple options in the future, this query will have to be changed.
  def selected_option
    return unless response.present?

    QuizQuestionOption.find_by(id: response['selections'][0].to_i)
  end

  def current_selection
    response['selections'][0].to_i
  end
end

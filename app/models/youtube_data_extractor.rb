class YoutubeDataExtractor

  GOOGLE_API = Settings.google.youtube_api

  def initialize(url: )
    @url = url
    @response_data = nil
  end

  def extract!
    parsed_url = is_youtube_url?
    return unless parsed_url

    @response_data ||= (
    conn = Faraday.new(url: GOOGLE_API)

    result = conn.get do |req|
      req.url '/youtube/v3/videos'
      req.options.timeout = 5
      req.params['id'] = parsed_url.to_a[2]
      req.params['part'] = 'snippet'
      req.params['fields'] = 'items(id,snippet/tags)'
      req.params['key'] = Settings.google.api_key
    end

    JSON.parse(result.body))
  end

  def tags
    raise_if_api_error!
    @response_data ? @response_data['items'][0]['snippet']['tags'] : []
  end

  private
  def raise_if_api_error!
    raise "Tag extraction failed: #{@response_data}" if @response_data && @response_data['error']
  end

  def is_youtube_url?
    /https?:\/\/(?:[a-zA_Z]{2,3}.)?(?:youtube\.com\/watch\?)((?:[\w\d\-\_\=]+&amp;(?:amp;)?)*v(?:&lt;[A-Z]+&gt;)?=([0-9a-zA-Z\-\_]+))/.match(@url)
  end
end
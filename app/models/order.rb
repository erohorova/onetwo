class Order < ActiveRecord::Base

  validates :orderable_id, :orderable_type, :user_id,
            :organization_id, :state, presence: true
  before_create :validate_uniqueness_based_on_orderable

  belongs_to :orderable, polymorphic: true
  belongs_to :user
  belongs_to :organization

  has_many :user_purchases
  has_many :order_transactions, foreign_key: 'order_id', class_name: 'Transaction'
  has_one :wallet_transaction

  enum state: {unprocessed: 0, processed: 1, processing_error: 2}

  def all_user_orders_for_orderable
    @orders ||= Order.where(user_id: user_id, orderable_id: orderable_id, orderable_type: orderable_type)
  end

  def only_processed_orders_exist?
    all_user_orders_for_orderable.pluck(:state).all? {|state| state != 0}
  end

  def last_order_failed?
    all_user_orders_for_orderable.last&.processing_error?
  end

  def validate_uniqueness_based_on_orderable
    return true if (orderable_type == "Recharge")

    validation_error = Proc.new do
      raise DuplicateOrderError
    end

    case orderable_type
    when "OrgSubscription"
      return true if last_order_failed? # Allow a new entry if the latest order record is in errored state
      if all_user_orders_for_orderable.any?
         validation_error.call()
      end
    when "Card"
      return true if all_user_orders_for_orderable.empty? # If there is no entry in orders table

      # allow a new entry in orders table only for following conditions
      # - card_subscriptions contains only expired subscriptions and orders table has only processed entries and no unprocessed ones
      # - the latest order record has errored state, i.e the transaction for order had failed
      if (user.all_subscriptions_expired?(card_id: orderable_id) && only_processed_orders_exist?) || last_order_failed?
        return true
      end
      validation_error.call()
    end
  end

  def self.initialize_order!(args: ,orderable_type:)
    if orderable_type == 'Recharge'
      Order.create!(args)
    else
      Order.find_or_create_by!(args.merge!({state: 0}))
    end
  end

  def wallet_recharge?
    (orderable_type == 'Recharge')
  end
end

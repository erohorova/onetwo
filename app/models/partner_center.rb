# == Schema Information
#
# Table name: partner_centers
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  user_id         :integer
#  enabled         :boolean          default(FALSE)
#  description     :string(255)
#  embed_link      :string(255)
#  integration     :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#


class PartnerCenter < ActiveRecord::Base
  belongs_to :organization
  belongs_to :user

  validates :embed_link, :integration, :organization, :user, presence: true

  scope :enabled, -> { where(enabled: true) }

  # =========================================
  # EVENT RECORDING CONFIG
  include EventTracker
  configure_metric_recorders(
    default_actor: ->(record) { },
    trackers: {
      on_create: {
        event: 'org_partner_center_created'
      },
      on_edit: {
        event: 'org_partner_center_edited',
      },
      on_delete: {
        event: 'org_partner_center_deleted',
      }
    }
  )

  # =========================================
end

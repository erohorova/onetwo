# == Schema Information
#
# Table name: topic_daily_learnings
#
#  id                :integer          not null, primary key
#  topic_id          :string(191)      not null
#  organization_id   :integer          not null
#  ecl_ids           :text(65535)
#  date              :string(11)       not null
#  processing_state  :string(255)      not null
#  created_at        :datetime
#  updated_at        :datetime
#  topic_name        :string(255)
#  requested_by_user :boolean          default(TRUE)
#

class TopicDailyLearning < ActiveRecord::Base

  belongs_to :organization

  serialize :ecl_ids, Array

  validates :organization_id, presence: true
  validates :topic_id, presence: true, uniqueness: {scope: [:organization_id, :date]}
  validates :processing_state, presence: true, inclusion: {in: [DailyLearningProcessingStates::INITIALIZED, DailyLearningProcessingStates::STARTED, DailyLearningProcessingStates::ERROR, DailyLearningProcessingStates::COMPLETED]}

  before_validation :set_processing_state, on: :create

  scope :completed, -> { where(processing_state: DailyLearningProcessingStates::COMPLETED)}

  def self.process_for_topic!(topic_id:, topic_name:, date:, organization_id:, requested_by_user: false)
    topic_daily_learning = self.find_or_initialize_by(
      topic_id: topic_id,
      topic_name: topic_name,
      date: date,
      organization_id: organization_id
    )
    topic_daily_learning.requested_by_user = requested_by_user
    topic_daily_learning.save!

    case topic_daily_learning.processing_state
      when DailyLearningProcessingStates::ERROR, DailyLearningProcessingStates::COMPLETED
        # do nothing
      else
        begin
          topic_daily_learning.process!
        rescue => e
          # Try to move on from the loss of a topic. There are other topics. Life goes on
        end
    end
  end

  def set_processing_state
    if self.processing_state.nil?
      self.processing_state = DailyLearningProcessingStates::INITIALIZED
    end
  end

  def process!
    begin
      # Module to get cards. Code from EP-2788 goes here
      start_processing!
      ecl_ids = TopicDailyLearningService.new.get_ecl_ids(self.topic_name, self.organization_id, self.date)
      update_attributes!(ecl_ids: ecl_ids)
      complete_processing!
    rescue => e
      log.error e
      mark_as_errored!
      raise e # So that caller in turn is marked as errored
    end
  end

  private
  def start_processing!
    update!(processing_state: DailyLearningProcessingStates::STARTED)
  end

  def complete_processing!
    update!(processing_state: DailyLearningProcessingStates::COMPLETED)
  end

  def mark_as_errored!
    update!(processing_state: DailyLearningProcessingStates::ERROR)
  end
end

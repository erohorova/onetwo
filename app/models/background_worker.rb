# == Schema Information
#
# Table name: background_workers
#
#  id         :integer          not null, primary key
#  name       :string(191)
#  command    :string(255)
#  state      :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class BackgroundWorker < ActiveRecord::Base
  cattr_accessor :pids do
    {}
  end

  def pid
    self.class.pids[name]
  end

  def pid=(val)
    self.class.pids[name] = val
  end

  def run
   puts "[Background Worker] Spawning: #{name} using `#{command}`"
   self.pid = Process.spawn(command)
  end

  def stop
    puts "[Background Worker] Stopping: #{name}"
    Process.kill('TERM', pid)
    Process.detach(pid)
    self.pid = nil
  end

  def is_running?
    if pid
      begin
        Process.kill(0, pid)
        true
      rescue Errno::ESRCH # No such process
        false
      rescue Errno::EPERM # The process exists, but you dont have permission to send the signal to it.
        true
      end
    else
      false
    end
  end
end

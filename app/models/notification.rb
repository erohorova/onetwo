# == Schema Information
#
# Table name: notifications
#
#  id                    :integer          not null, primary key
#  notifiable_id         :integer
#  notifiable_type       :string(191)
#  group_id              :integer
#  user_id               :integer
#  notification_type     :string(255)
#  unseen                :boolean          default(TRUE)
#  accepted_at           :datetime
#  created_at            :datetime
#  updated_at            :datetime
#  widget_deep_link_id   :integer
#  card_id               :integer
#  widget_deep_link_type :string(255)
#  app_deep_link_id      :integer
#  app_deep_link_type    :string(255)
#  custom_message        :string(255)
#

class Notification < ActiveRecord::Base
  TYPE_MENTION_IN_CARD    = 'mention_in_card'.freeze
  TYPE_MENTION_IN_COMMENT = 'mention_in_comment'.freeze
  TYPE_ASSIGNED           = 'assigned'.freeze
  TYPE_ASSIGNMENT_NOTICE  = 'assignment_notice'.freeze
  TYPE_ACTION_IN_TEAM     = %w[join_group leave_group accept_group_invite
                               decline_group_invite team_card_share].freeze

  belongs_to :group
  belongs_to :notifiable, :polymorphic => true
  belongs_to :user
  has_many :notifications_causal_users, :dependent => :delete_all
  has_many :causal_users, through: :notifications_causal_users, class_name: 'User', source: :user

  validates :user, presence: true
  validate :is_recipient_active
  before_create :set_widget_deep_link, :set_card_id, :set_app_deep_link

  scope :unseen, -> {where(unseen: true)}

  def app_deep_link_id_and_type
    _id, _type = case notification_type
      when 'new_follow'
        # dummy id. There isn't really a deep link business object corresponding to a follow notification
        [0, 'follow']
        # NOTE: everything below is notifiable_type, only the top one is notification_type
      when 'following'
        [notifiable_id, 'channel']
      when 'group_invite', 'join_group'
        [notifiable_id, 'team']
      else
        # Ex. notifiable = Comment, Post, Card. returns [id, 'comment/post/card']
        return [nil, nil] if notifiable.blank?
        notifiable.public_send(:get_app_deep_link_id_and_type) if notifiable.respond_to?(:get_app_deep_link_id_and_type)
      end
    return _id, _type
  end

  def set_app_deep_link
    _id, _type = app_deep_link_id_and_type
    if _id && _type
      self.app_deep_link_id, self.app_deep_link_type = _id, _type
      # keep populating card_id for a while
      if _type == 'card'
        self.card_id = _id
      end
    end
  end

  def self.preload_app_deep_link_snippets notifications
    hsh = {}
    grouped = notifications.group_by{|n| n.notifiable_type}
    grouped.each do |type, notifs|
      next if type.nil?
      obj_hash = {}
      if type == 'Card'
        obj_hash = Card.where(id: notifs.map{|n| n.notifiable_id}).index_by(&:id)
      else
        obj_hash = type.constantize.where(id: notifs.map{|n| n.notifiable_id}).index_by(&:id)
      end
      hsh[type] = obj_hash
    end
    Hash[notifications.map{|n| [n.id, hsh.try(:[], n.notifiable_type).try(:[], n.notifiable_id).try(:snippet)]}]
  end

  def self.widget_deep_link_id_and_type notification_type, notifiable_type, notifiable_id
    _id, _type = nil, nil
    if notifiable_type == 'Post'
      _id = notifiable_id
      _type = 'post'
    elsif notifiable_type == 'Comment'
      c = Comment.find_by(id: notifiable_id)
      if c && c.commentable_type == 'Post'
        _id = c.commentable_id
        _type = 'post'
      end
    end
    return _id, _type
  end

  def set_widget_deep_link
    self.widget_deep_link_id, self.widget_deep_link_type = Notification.widget_deep_link_id_and_type notification_type, notifiable_type, notifiable_id
  end

  def set_card_id
    if notifiable_type == 'Comment'
      c = Comment.find_by(id: notifiable_id)
      if c
        if c.commentable_type == 'Card'
          self.card_id = c.commentable_id
        end
      end
    elsif notifiable_type == 'Card'
      self.card_id = notifiable_id
    end
  end

  def mark_as_seen
    update_attributes(unseen: false)
  end

  def seen?
    !unseen
  end

  def snippet
    notifiable.respond_to?(:snippet) ? notifiable.snippet : ''
  end

  def ref_id
    if notifiable_type == 'Post'
      return notifiable_id
    elsif notifiable_type == 'Comment'
      c = Comment.find_by(id: notifiable_id)
      if c && c.commentable_type == 'Post'
        return c.commentable_id
      end
    end
    return nil
  end

  def causal_user_name
    ncu = notifications_causal_users.first
    if ncu
      ncu.causal_user_name
    else
      nil
    end
  end

  def causal_user
    causal_users.try(:first)
  end

  def causal_user_id
    ncu = notifications_causal_users.first
    if ncu
      return ncu.user_id
    else
      return nil
    end
  end

  def causal_user_picture
    ncu = notifications_causal_users.first
    if ncu
      ncu.causal_user_picture
    else
      nil
    end
  end

  def causify_messages(_message)
    msg = case causal_users.size
          when 1
            "#{causal_user_name} #{_message}"
          when 2
            "#{causal_user_name} and 1 other person #{_message}"
          else
            "#{causal_user_name} and #{causal_users.size - 1} other people #{_message}"
          end
    msg
  end

  def resolve_mention_in_comment_message
    case app_deep_link_type
    when 'card' then 'the insight'
    when 'collection' then 'the insight'
    when 'video_stream' then 'the video stream'
    when 'post' then 'the post'
    else 'this post'
    end
  end

  def message(preloaded_snippets = nil, opts = {})
    return custom_message if custom_message.present? && notification_type != 'team_card_share'

    # new follower message doesn't fit into the format right now, unfortunately.
    # Special case
    if notification_type == 'new_follow'
      case causal_users.size
      when 1
        return "#{causal_user_name} is following you"
      when 2
        return "#{causal_users.first.name} and #{causal_users.last.name} are following you"
      else
        return "#{causal_user_name} and #{causal_users.size - 1} other people are following you"
      end
    end

    if notification_type == 'user_card_share'
      card_name = notifiable&.message.presence || "SmartCard"
      return "#{causal_user_name} shared #{card_name} with you."
    end

    if notification_type == 'team_card_share'
      card_name = notifiable&.message.presence || "SmartCard"
      return "#{causal_user_name} shared #{card_name} with your group(s)."
    end

    message = ''
    case notifiable_type
    when 'Post'
      item = Post.find_by(id: notifiable_id)
      message = case notification_type
                when 'new_post_upvote'
                  causify_messages('liked your post')
                when 'new_announcement'
                  "#{causal_user_name} made a new announcement"
                when 'new_comment'
                  causify_messages('commented on post')
                when 'new_approval'
                  "#{causal_user_name} approved your post"
                end
    when 'Comment'
      message = case notification_type
                when 'new_comment_upvote'
                  causify_messages('liked your comment')
                when 'new_approval'
                  "#{causal_user_name} approved your comment"
                when 'mention_in_comment'
                  msg_ = "mentioned you in a comment on #{resolve_mention_in_comment_message}"
                  causify_messages(msg_)
                end
    when 'Card'
      message = case notification_type
                when 'new_comment'
                  causify_messages('commented on post')
                when 'new_card_upvote'
                  causify_messages('liked your post')
                when 'mention_in_card'
                  causify_messages('mentioned you in the insight')
                when 'new_poll'
                  causify_messages('shared a poll with you')
                when 'poll_answer'
                  causify_messages('answered your poll')
                when 'stream_scheduled'
                  "#{causal_user_name} scheduled a live stream"
                when 'stream_started'
                  if notifiable.present? && notifiable.video_stream && notifiable.video_stream.status == 'live'
                    "#{causal_user_name} is now live"
                  else
                    "#{causal_user_name} was live"
                  end
                when 'scheduled_stream_reminder'
                  'Time to start your stream'
                when 'skipped_card'
                  "Channel rejected your card"
                end
    when 'Channel'
      message = case notification_type
                when 'following'
                  'You are following'
                when 'added_author'
                  causify_messages('added you as a collaborator of the channel')
                when 'added_follower'
                  causify_messages('added you as a follower of the channel')
                when 'curated_card'
                  "Your card has been successfully published to channel"
                when 'new_card_for_curation'
                  "There is a new card for curation in channel"
                end
    when 'Group'
      message = case notification_type
                when 'assigned'
                  causify_messages('assigned you a Course')
                end
    when 'Team'
      message = case notification_type
                when 'add_admin_to_team'
                  'You were added as an group leader of the team'
                when 'add_member_to_team'
                  'You were added as a member of the team'
                when 'add_sub_admin_to_team'
                  'You were added as a group admin of the team'
                end
    when 'Assignment'
      message = case notification_type
                when 'assigned'
                  causify_messages("assigned you the #{notification_card_type}")
                when 'assignment_due_soon'
                  get_assingment_due_message
                when 'assignment_notice'
                  #This is already coveredon first line of this method.
                  custom_message || ''
                end
    end
    "#{message}: '#{preloaded_snippets.try(:[], self.id) || snippet}'"
  end

  def causal_user_anonymous? user
    ncu = notifications_causal_users.where(user: user).first
    return ncu.anonymous if ncu
    log.error 'Asking if causal user anonymous for non causal user'
    false
  end

  def get_assingment_due_message
    message = ""

    return message unless notifiable_type == 'Assignment' && notifiable.present?
    due_days = notifiable.get_due_days
    message = if due_days == 0
      "Assignment due today"
    elsif due_days > 0
      "Assignment due in #{due_days} days"
    elsif due_days < 0
      "Assignment overdue by #{due_days.abs} days"
    end
    message
  end

  def organization
    notifiable.try(:organization)
  end

  def notification_card_id
    app_deep_link_type == "video_stream" ? VideoStream.find(app_deep_link_id).try(:card_id) : notifiable_id
  end

  def notification_card_type
    return if notifiable.blank? || notifiable.assignable.blank?
    case notifiable.assignable.card_type
    when 'pack'
      'Pathway'
    when 'journey'
      'Journey'
    else
      'SmartCard'
    end
  end

  def access_notification_type_for_team
    notifiable.is_a?(Team) || Notification::TYPE_ACTION_IN_TEAM.include?(notification_type)
  end

  private

  def is_recipient_active
    if user && (user.is_suspended? || !user.is_active?)
      errors.add(:user_id, "id: #{user.id} is not active")
    end
  end
end

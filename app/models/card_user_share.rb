# frozen_string_literal: true

class CardUserShare < ActiveRecord::Base
  include EventTracker

  belongs_to :card
  belongs_to :user
  belongs_to :shared_by

  validates_presence_of :card
  validates_presence_of :user
  validates_uniqueness_of :card_id, scope: :user_id

  validate :should_belong_to_same_org

  after_commit :generate_notification, on: :create, if: :card_visible?
  after_destroy :remove_card_share_notification

  configure_metric_recorders(
    default_actor: -> (record) { },
    trackers: {
      on_create: {
        event: 'card_shared',
        job_args: [ { card: :card, shared_to_user: :user } ]
      }
    }
  )

  def should_belong_to_same_org
    unless card.organization_id == user.organization_id
      errors.add(:base, "Card and User Organizations don't match")
    end
  end

  # to send bell for share if card is accessible to user
  def card_visible?
    return true if card.is_public?
    return card.visible_to_user(user)
  end

  def generate_notification
    NotificationGeneratorJob.perform_later(item_id: id, item_type: 'user_card_share' ,item_shared_by: shared_by_id)
  end

  def remove_card_share_notification
    notification = Notification.find_by(notification_type: 'user_card_share',
                                        user_id: self.user_id,
                                        app_deep_link_id: self.card_id)
    notification.destroy if notification
  end
end

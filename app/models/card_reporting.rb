class CardReporting < ActiveRecord::Base
  acts_as_paranoid

  # Associations
  belongs_to :card
  belongs_to :user

  # Validations
  validates_presence_of :user, :reason
  validates :card, presence: true, uniqueness: { scope: :user_id,
    message: 'is already reported by user' }
  validate :should_belong_to_same_org

  def should_belong_to_same_org
    errors.add(:base, "Card and User organizations don't match") unless card.organization_id == user.organization_id
  end

  def json_data
    {
      id: id,
      user_id: user_id,
      reason: reason,
      created_at: created_at,
      updated_at: updated_at,
      deleted_at: deleted_at
    }
  end

  class << self
    def fetch_ordered_records
      order('created_at DESC').to_sql
    end

    def fetch_content(organization: , state: "reported")
      trashed = state == "trashed"
      count_query = "(SELECT count(id) from card_reportings where card_reportings.card_id = cards.id)"

      reporting_records = fetch_records(trashed: trashed)

      scoped = organization.cards
      scoped = scoped.only_deleted if trashed
      scoped = scoped.includes(:author).joins("
        INNER JOIN (#{reporting_records}) cr on cr.card_id = cards.id
        INNER JOIN users as reported_by on reported_by.id = cr.user_id")

      scoped.select("cards.author_id, CONCAT(COALESCE(reported_by.first_name, ''), ' ', COALESCE(reported_by.last_name, '')) as reported_by_user,
        cards.id, cards.title, cards.message, cards.card_type,
        cards.ecl_metadata, cr.reason, cr.created_at, cards.created_at as card_created_at,
        #{count_query} as card_reporting_count").group(:card_id).order('cr.created_at DESC')
    end

    private

    def fetch_records(trashed: false)
      trashed ? only_deleted.fetch_ordered_records : fetch_ordered_records
    end
  end
end

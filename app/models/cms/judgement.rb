# == Schema Information
#
# Table name: cms_judgements
#
#  id                :integer          not null, primary key
#  user_id           :integer
#  content_item_id   :integer
#  content_item_type :string(191)
#  action            :string(191)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  additional_info   :text(65535)
#

class Cms::Judgement < ActiveRecord::Base

  belongs_to :content_item, polymorphic: true
  belongs_to :user

  validates :user, presence: true
  validates :content_item, presence: true

  def self.create_content_trail(item_id, action, user, item_type, additional_info = nil)
    begin
      create!(user_id: user.id, content_item_id: item_id, content_item_type: item_type, action: action, additional_info: additional_info.to_json)
    rescue => e
      log.error e.inspect
    end
  end
end

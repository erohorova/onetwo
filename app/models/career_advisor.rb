class CareerAdvisor < ActiveRecord::Base
  include EventTracker

  SKILLS = ['data_scientist', 'data_architect', 'dev_ops_engineer']

  belongs_to :organization
  has_many :career_advisor_cards, dependent: :destroy
  has_many :cards, through: :career_advisor_cards
  accepts_nested_attributes_for :career_advisor_cards,
    reject_if: proc { |attr| attr['card_id'].blank? },
    allow_destroy: true

  validates :skill, presence: true, inclusion: { in: SKILLS, message: "%{value} is not a valid skill" }

  # Create default CareerAdvisor skills for all Organizations in migration
  def self.create_default_skills
    Organization.pluck(:id).each do |org_id|
      SKILLS.each { |skill| CareerAdvisor.find_or_create_by(skill: skill, organization_id: org_id)  }
    end
  end

  configure_metric_recorders(
     default_actor: lambda { |record| },
     trackers: {
       on_create: {
         event: 'org_career_advisor_created',
       },
       on_delete: {
         event: 'org_career_advisor_deleted'
       },
       on_edit: {
         event: 'org_career_advisor_edited'
       }
     }
   )
end

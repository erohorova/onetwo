# == Schema Information
#
# Table name: video_streams
#
#  id                         :integer          not null, primary key
#  creator_id                 :integer
#  group_id                   :integer
#  start_time                 :datetime
#  name                       :string(255)
#  uuid                       :string(191)      not null
#  status                     :string(191)
#  created_at                 :datetime
#  updated_at                 :datetime
#  publish_url                :string(255)
#  playback_url               :string(1024)
#  password                   :string(255)
#  x_id                       :string(255)
#  type                       :string(255)
#  comments_count             :integer          default(0)
#  votes_count                :integer          default(0)
#  slug                       :string(191)
#  last_stream_at             :datetime
#  thumbnail_file_resource_id :integer
#  anon_watchers_count        :integer          default(0)
#  organization_id            :integer
#  state                      :string(191)
#  is_official                :boolean          default(FALSE)
#  width                      :integer
#  height                     :integer
#  card_id                    :integer
#

class WowzaVideoStream < VideoStream

  def publish_url
    self.class.publish_url(uuid)
  end

  def rtmp_playback_url
    "#{Settings.video_streaming_publish_endpoint}/#{uuid}"
  end


  def playback_url
    self.class.playback_url(uuid)
  end

  def provider_name
    'wowza'
  end

  def self.playback_url(uuid)
    #adaptive hsl live/ngrp:neil_all/playlist.m3u8
    if Settings.video_streaming_playback_adaptive_bitrate_enabled
      "#{Settings.video_streaming_playback_endpoint}/ngrp:#{uuid}_all/playlist.m3u8"
    else
      "#{Settings.video_streaming_playback_endpoint}/#{uuid}/playlist.m3u8"
    end
  end

  def self.publish_url(uuid)
    "#{Settings.video_streaming_publish_endpoint}"
  end
end

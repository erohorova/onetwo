# == Schema Information
#
# Table name: channels_cards
#
#  id         :integer          not null, primary key
#  channel_id :integer          not null
#  card_id    :integer          not null
#  state      :string(191)
#  curated_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ChannelsCard < ActiveRecord::Base
  has_paper_trail ignore: [:created_at, :updated_at]

  include AASM
  include EventTracker
  include CustomCarousel

  STATE_NEW = :new
  STATE_CURATED = :curated
  STATE_SKIPPED = :skipped
  STATE_ARCHIVED = :archived

  belongs_to :channel
  belongs_to :card

  validates :channel, presence: true
  validates :card, presence: true, uniqueness: { scope: [:channel_id] }
  validate :should_belong_to_same_org

  before_create :set_state
  after_create :trigger_notification
  after_commit :enqueue_for_followers, :enqueue_for_following_teams, on: :create
  after_commit :create_users_management_card, on: :create
  after_commit :sync_card_to_clone_channel, on: :destroy
  after_commit :reindex_card
  after_destroy :remove_pinned_cards
  after_destroy :set_rank

  before_update :set_rank, if: -> { (self.rank_changed?) }

  configure_metric_recorders(
    default_actor: :author,
    trackers: {
      on_create: {
        event: 'card_added_to_channel',
        actor: lambda { |record| record.channel.creator},
        job_args: [{channel: :channel}, {card: :card}],
        if_block: :curated?
      },
      on_delete: {
        event: 'card_removed_from_channel',
        actor: lambda { |record| record.channel.creator},
        job_args: [{channel: :channel}, {card: :card}],
        if_block: :curated?
      }
    }
  )

  aasm column: :state, whiny_transitions: false do
    state STATE_NEW, initial: true
    state STATE_CURATED
    state STATE_SKIPPED
    state STATE_ARCHIVED

    event :curate do
      before do
        check_channel_card_state unless may_curate?
       self.curated_at = Time.now.utc
      end
      transitions from: STATE_NEW, to: STATE_CURATED

      after do
        if curation_required?
          ChannelCardEnqueueJob.perform_later(self.channel.id, self.card.id)
          TeamsChannelsFollow.where(channel_id: self.channel_id).find_in_batches(batch_size: 100) do |group|
            TeamsChannelsAutofollowJob.perform_later(team_ids: group.collect(&:team_id), channel_id: self.channel_id)
          end
          EDCAST_NOTIFY.trigger_event(Notify::EVENT_CURATE_CARD, self)
        end
        record_add_to_channel_event if persisted?
        sync_card_to_clone_channel('add') if self.channel.shareable? && self.channel.cloned_channels.exists?
      end
    end
    event :skip do
      before do
        check_channel_card_state unless may_skip?
      end
      transitions from: STATE_NEW, to: STATE_SKIPPED
      after do
        if curation_required?
          EDCAST_NOTIFY.trigger_event(Notify::EVENT_SKIP_CARD, self)
        end
      end
    end
    event :repost do
      transitions from: STATE_SKIPPED, to: STATE_NEW
    end
    event :archive do
      before do
        check_channel_card_state unless may_archive?
      end
      transitions from: [STATE_NEW, STATE_CURATED, STATE_SKIPPED], to: STATE_ARCHIVED
    end
  end

  def create_users_management_card
    return if card.cloned_card?
    if card.current_user != card.author
      UsersCardsManagementJob.perform_later(card.current_user.id, card_id, UsersCardsManagement::ACTION_POST_TO_CHANNEL, channel_id)
    end
  end

  def check_channel_card_state
    errors.add(aasm.current_event, "can not be set for #{state}")
  end

  def should_belong_to_same_org
    unless self.card.organization_id == self.channel.organization_id
      errors.add(:base, "Card and Channel Organizations don't match")
    end
  end

  def set_state
    unless curation_required?
      self.curate
    end
  end

  def set_rank
    related_cards  = related_channel_cards
    return true if related_cards.nil? || !self.curated? || self.rank.blank?
    if self.destroyed?
      channel_cards = ChannelsCard.where(channel_id: channel_id,
      card_id: related_cards.pluck(:id)).where("rank > ? AND rank IS NOT NULL", rank)
      channel_cards.order("rank").update_all("rank=rank-1") if channel_cards.present?
    elsif rank_was.nil? || rank_was > rank
      highest_rank = ChannelsCard.where(channel_id: channel_id, card_id: related_cards.pluck(:id)).maximum(:rank)
      channel_cards = ChannelsCard.where(channel_id: channel_id,
      card_id: related_cards.pluck(:id)).where("rank BETWEEN ? and ? AND rank IS NOT NULL", rank, (rank_was || highest_rank || rank))
      channel_cards.order("rank").update_all("rank=rank+1") if channel_cards.present?
    elsif rank_was < rank
      channel_cards = ChannelsCard.where(channel_id: channel_id,
      card_id: related_cards.pluck(:id)).where("rank BETWEEN ? and ? AND rank IS NOT NULL", (rank_was || rank), rank)
      channel_cards.order("rank").update_all("rank=rank-1") if channel_cards.present?
    end
  end

  def related_channel_cards
    if card.video_stream?
      channel.curated_streams
    elsif card.course_card?
      channel.curated_courses
    elsif card.media_card? || card.poll_card?
      channel.curated_smartbites
    elsif card.pack_card?
      channel.curated_published_pathways
    elsif card.journey_card?
      channel.curated_published_journeys
    end
  end

  def sync_card_to_clone_channel(action='destroy')
    SyncCloneChannelCardsJob.set(wait_until: 10.seconds).perform_later(
      {
        parent_card_id: card_id,
        parent_channel_id: channel.id,
        action: action
      }
    )
  end

  def curation_required?
    return false unless channel.curate_only

    usr = card.current_user

    return true unless usr

    if channel.user_is_curator?(usr)
      # content posted by curator doesn't require curation
      user_can_skip_curation = true
    else
      user_can_skip_curation = if usr.organization.trusted_collaborators_enabled?
        # content posted by trusted collaborator doesn't require curation
        # collaborator can bypass curation with permissions BYPASS_CURATION
        channel.is_trusted_author?(usr) || (
          channel.is_author?(usr) && usr.exists_permission_for_role?(
                                       role: 'collaborator',
                                       permission: Permissions::BYPASS_CURATION
                                     )
        )
      else
        # collaborator can skip curation if trusted collaboration is not enabled
        channel.is_author?(usr)
      end
    end

    # non-ugc card posted by curator/author/follower authorized to bypass_curation permission
    # should not require curation
    return true if !card.ugc? && !user_can_skip_curation

    # to return false if author/curator/follower of channel
    # AND
    # authorized to skip curation
    return false if user_can_skip_curation

    # user is not author
    # user is just a follower
    # in open channels there are no curators
    # and user not authorized to bypass curation permission
    # if is_open? is true
    # curation_required will be true if ugc_curation is enabled
    # else will return false
    return channel.ugc_curation_enabled? if channel.is_open?

    # is_open is false
    # user is not author/curator
    # if ugc_curation is true
    # will return true if user is not curator(
    # ie, user is just a follower and not authorized to bypass_curation permission)
    channel.ugc_curation_enabled?
  end

  def trigger_notification
    if self.curation_required?
      EDCAST_NOTIFY.trigger_event(Notify::EVENT_NEW_CARD_FOR_CURATION, self.channel) #new notification
      # NotificationGeneratorJob.perform_later({item_id: self.channel_id, item_type: 'new_card_for_curation', notification_type: 'new_card_for_curation'}) #old notification
    end
  end

  def enqueue_for_followers
    if !curation_required?
      ChannelCardEnqueueJob.perform_later(self.channel.id, self.card.id)
    end
  end

  def enqueue_for_following_teams
    if !curation_required?
      TeamsChannelsFollow.where(channel_id: self.channel_id).find_in_batches(batch_size: 100) do |group|
        TeamsChannelsAutofollowJob.perform_later(team_ids: group.collect(&:team_id), channel_id: self.channel_id)
      end
    end
  end

  def remove_pinned_cards
    pinned_card = Pin.find_by(object: self.card, pinnable: self.channel)

    if pinned_card
      pinned_card.destroy
    end
  end

  private

  def remove_channels_cards_from_custom_carousel
    #this will remove cards from custom carousel which are removed from channel
    structure_id = Structure.where(parent_id: self.channel_id, parent_type: 'Channel').pluck(:id)
    content = StructuredItem.where(structure_id: structure_id, entity_id: self.card_id, entity_type: 'Card')
    StructuredItem.reorder_and_destroy(content) if content.present?
  end

  def record_add_to_channel_event
    return unless self.curated?
    event = 'card_added_to_channel'
    record_card_channel_event(event, self.created_at)
  end

  def record_remove_from_channel_event
    return unless self.curated?
    event = 'card_removed_from_channel'
    record_card_channel_event(event, Time.now)
  end

  def record_card_channel_event(event, timestamp)
    record_custom_event(
      event: event,
      actor: lambda { |record| record.channel.creator},
      job_args: [{channel: :channel}, {card: :card}],
      if_block: :curated?
    )
  end

  def reindex_card
      card.inline_indexing = true
      card.reindex_card
  end

end

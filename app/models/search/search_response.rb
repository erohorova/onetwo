class Search::SearchResponse

  FIELDS = %i[offset limit query total results status aggs]
  FIELDS.each { |attr| attr_reader attr }

  def initialize(pars={})
    set_values(pars, FIELDS)
  end

  private

  def set_values(values, fields)
    values.slice(*fields).each {|k,v| instance_variable_set("@#{k}", v)}
  end
end

class Search::CourseSearch < Search::QuerySearch

  def search_for_cms(free_text_query, organization_id, filter_params: {}, offset: 0, limit: 10, sort: {})

    ands = [{term: {organization_id: organization_id}}, {term: {_type: 'resource_group'}}]
    [:is_promoted].each do |par|
      unless filter_params[par].nil?
        ands << {term: {par => filter_params[par]}}
      end
    end

    # unless filter_params[:channel_ids].nil?
    #   ors = []
    #   ors << {terms: {channel_ids: filter_params[:channel_ids]}}
    #   if filter_params[:allow_empty_channel_ids]
    #     ors << {missing: {field: 'channel_ids'}}
    #   end
    #   ands << {or: ors}
    # end
    #
    # unless filter_params[:topics].nil?
    #   ands << {terms: {'tags.name' => filter_params[:topics]}}
    # end

    query_part = free_text_query.blank? ? {"match_all" => {}} : {multi_match: {fields: ['name', 'description', 'website_url'], query: free_text_query}}

    query_for_es = {query: {filtered: {filter: {and: ands}, query: query_part}}}

    order = { (sort[:key] || :created_at).to_sym => { order: sort[:order] } }

    return _search(query: query_for_es, offset: offset, limit: limit, sort: order)
  end

  def search_class
    ResourceGroup
  end

  def response_class
    Search::CourseSearchResponse
  end

  def response_fields
    super.merge(
        query: @q,
    )
  end

end

class Search::CardSearch < Search::QuerySearch
  # returns query hash for search or percolation
  # also populates instance variables for search response
  def generate_query(target, organization, viewer=nil, filter_params = {})
    musts = []
    musts << term_filter(:organization_id, organization.id)
    musts << negate_filter(term_filter('hidden', true))
    musts << term_filter(:state, filter_params[:state] || 'published')
    musts << term_filter(:card_type, filter_params[:card_type])
    musts << term_filter(:is_official, filter_params[:is_official])
    musts << term_filter(:card_template, filter_params[:card_template])

    musts << term_filter( :id, filter_params[:id]) unless filter_params[:id].blank?
    musts << { term: { 'resource.url' => filter_params[:link_url] } } if filter_params[:link_url]
    if filter_params[:language]
      musts << should_filter([
        term_filter(:language, filter_params[:language]),
        missing_filter(:language)
      ])
    end

    if filter_params[:relevent_cards].present?
      if filter_params[:channel_ids].present?
        channel_ids = filter_params[:channel_ids]
      else
        channel_ids = Channel.readable_channel_ids_for(viewer, _organization: organization, onboarding: false)
      end
      channel_filter = term_filter(:curated_channel_ids, channel_ids)
      if filter_params[:taxonomy_topics].present?
        taxonomy_filter = boolean_or_filter(
          term_filter(:taxonomy_topics, filter_params[:taxonomy_topics]),
          term_filter("user_taxonomy_topics.path",filter_params[:taxonomy_topics])
        )
        musts <<  boolean_and_filter(channel_filter, taxonomy_filter)
      else
        musts << channel_filter
      end
    else
      if filter_params[:channel_ids].present?
        channel_ids = filter_params[:channel_ids]
      else
        channel_ids = Channel.readable_channel_ids_for(viewer, _organization: organization, onboarding: false)
      end
      musts << boolean_or_filter(
        term_filter(:channel_ids, channel_ids),
        missing_filter(:channel_ids)
      )

      unless filter_params[:taxonomy_topics].nil?
        musts << boolean_or_filter(
          term_filter(:taxonomy_topics, filter_params[:taxonomy_topics]),
          term_filter("user_taxonomy_topics.path", filter_params[:taxonomy_topics])
        )
      end
    end

    unless filter_params[:taxonomy_topic_with_level].nil?
      musts << boolean_or_filter(
        term_filter(:taxonomy_topics, filter_params[:taxonomy_topic_with_level][:name]),
        term_filter("user_taxonomy_topics.path", filter_params[:taxonomy_topic_with_level][:name])
      )
      musts << term_filter(:author_skill_level, filter_params[:taxonomy_topic_with_level][:level_name])
    end

    # Fetch video-cards
    # 'card_type' = 'video_stream'
    # OR
    # 'card_type' = 'media' AND 'card_subtype': 'video'
    if filter_params[:video_cards].presence
      musts << boolean_or_filter(
        term_filter(:card_type, 'video_stream'), boolean_and_filter(term_filter(:card_type, 'media'),
        term_filter(:card_subtype, 'video'))
      )
    end

    musts << negate_filter(term_filter(:id, filter_params[:exclude_ids]))
    musts << term_filter(:author_id, filter_params[:author_id])
    musts << term_filter(:channel_ids, filter_params[:channel_ids])
    musts << exists_filter(:author_id) if filter_params[:author_must]
    musts << exists_filter(:small_image_url) if filter_params[:small_image_url]
    musts << { range: filter_params[:range] } if filter_params[:range]

    # topics filter
    musts << term_filter('tags.name', filter_params[:topics])

    # Origin filter
    musts << term_filter('ecl.origin', filter_params[:ecl_origin])

    musts << term_filter(:channel_ids, [target.id]) if target.is_a?(Channel)

    unless filter_params[:ranges].blank?
      filter_params[:ranges].each do |range|
        musts << {range: range}
      end
    end

    musts << term_filter(:author_id, target.id) if target.is_a?(User)
    musts << term_filter('ecl_metadata.source_id', filter_params[:ecl_source_id]) if filter_params[:ecl_source_id].present?
    if filter_params[:exclude_users_on_likes].present?
      musts << negate_filter(term_filter('liked_by.id', filter_params[:exclude_users_on_likes]))
    end
    if filter_params[:exclude_users_on_comments].present?
      musts << negate_filter(term_filter('comments.user_id', filter_params[:exclude_users_on_comments]))
    end

    query = nil
    if target.is_a?(String)
      query = target
      @query_string = query
    end

    return aggregate_filters_and_query(musts: musts, query: free_text_query(query))
  end

  def count(target, organization, viewer: , filter_params: {})
    query = generate_query(target, organization, viewer, filter_params)
    return _count(query)
  end

  def search_body(target, organization, viewer: , filter_params: {}, source: false, offset: 0, limit: 10, sort: nil, load: false)
    @sort = sort
    query = generate_query(target, organization, viewer, filter_params)

    order = case @sort
            when :published
              sort_field_query(:published_at)
            when :created
              sort_field_query(:created_at)
            when :updated
              sort_field_query(:updated_at)
            when :like_count
              sort_field_query(:like_count)
            when :recently_liked
              [sort_field_query(:like_count), sort_field_query(:created_at)]
            when :promoted_first
              [sort_field_query(:is_official), sort_field_query(:promoted_at), sort_field_query(:published_at)]
            end
    return {search: {query: query, offset: offset, limit: limit, sort: order, load: load, source: source}, instance: self}
  end

  def search(target, organization, viewer: , filter_params: {}, source: false, offset: 0, limit: 10, sort: nil, load: false)
    return _search(search_body(target, organization, viewer: viewer, filter_params: filter_params, source: source, offset: offset, limit: limit, sort: sort, load: load)[:search])
  end

  def search_for_cms(_free_text_query, organization_id, filter_params: {}, offset: 0, limit: 10, sort: {})
    musts = []
    musts << term_filter(:organization_id, organization_id)
    musts << term_filter(:is_official, filter_params[:is_official])

    unless filter_params[:ugc_only].nil?
      musts << exists_filter(:author_id)
    end

    musts << term_filter('author.handle', filter_params[:ugc_creator_handle])

    [:card_type, :state].each do |filter_key|
      musts << term_filter(filter_key, filter_params[filter_key])
    end

    unless filter_params[:channel_ids].nil?
      if filter_params[:allow_empty_channel_ids]
        musts << boolean_or_filter(missing_filter(:channel_ids), term_filter(:channel_ids, filter_params[:channel_ids]))
      else
        musts << term_filter(:channel_ids, filter_params[:channel_ids])
      end
    end

    musts << term_filter('tags.name', filter_params[:topics])

    unless filter_params[:ranges].blank?
      filter_params[:ranges].each do |range|
        musts << {range: range}
      end
    end

    query_for_es = aggregate_filters_and_query(musts: musts, query: free_text_query(_free_text_query))

    sort_fields = []
    if sort.empty?
      sort_fields << sort_field_query(:created_at)
    else
      sort_fields << sort_field_query(sort[:key], sort[:order])
    end

    return _search(query: query_for_es, offset: offset, limit: limit, sort: sort_fields)
  end

  def search_pathways_for_cms(free_text_query, organization_id, filter_params: {}, offset: 0, limit: 10, sort: {})
    filter_params[:card_type] = ['pack']
    search_for_cms(free_text_query, organization_id, filter_params: filter_params, offset: offset, limit: limit, sort: sort)
  end

  def search_by_id(ids, state: 'published')
    _retrieve_one_or_many_by_ids(musts: [term_filter(:state, state)], ids: ids)
  end

  def response_fields
    super.merge(query: @query_string)
  end

  def search_class
    Card
  end

  def response_class
    Search::CardSearchResponse
  end

  def search_in_channel(q:, organization_id:, channel_id:, viewer:, filter: {}, offset: 0, limit: 10, load: false)
    # STEP 1: Define all filters
    # 1. Mandatory Filters
    musts = [
      term_filter(:organization_id, organization_id),
      term_filter(:curated_channel_ids, channel_id),
      term_filter(:hidden, false),
      term_filter(:state, 'published')
    ]

    # 2. Exclude dismissed content if request is from web
    musts << negate_filter(term_filter(:id, viewer.dismissed_card_ids)) unless filter[:is_cms]

    unless viewer.is_org_admin?
      # 3. Access control filter
      musts << access_control_filter(viewer)
    end

    query = aggregate_filters_and_query(
              musts: musts,
              query: prepare_search_term_query(q)
            )

    # 3. Params filter
    params_filter = []

    if filter[:content_type].present?
      shoulds = filter[:content_type].map do |content_info|
                  if content_info['card_subtype'].present?
                    must_filter([
                      term_filter(:card_type, content_info['card_type']),
                      term_filter(:card_subtype, content_info['card_subtype'])
                    ])
                  else
                    term_filter(:card_type, content_info['card_type'])
                  end
                end
      params_filter << should_filter(shoulds)
    end
    if filter[:source_display_name].present?
      params_filter << should_filter(
        filter[:source_display_name].map { |source_name| term_filter('ecl_metadata.source_display_name', source_name) }
      )
    end
    if filter[:author_id].present?
      params_filter << should_filter(
        filter[:author_id].map { |author_id| term_filter(:author_id, author_id) }
      )
    end
    if filter[:date_range].present?
      terms = filter[:date_range].map do |range|
                {
                  range: {
                    created_at: {
                      gte: range['from_date'].to_date.beginning_of_day.strftime('%FT%T.000%:z'),
                      lte: range['till_date'].to_date.end_of_day.strftime('%FT%T.000%:z')
                    }
                  }
                }
              end
      params_filter << should_filter(terms)
    end

    query[:post_filter] = must_filter(params_filter) if params_filter.any?

    # STEP 2: Add aggregations
    query[:aggs] = build_aggregations_query unless filter[:skip_aggs]

    # STEP 3: Get the response
    search_response = _search(query: query, offset: offset, limit: limit, sort: sort_field_query(:created_at), load: load)

    # STEP 4: Format the response for aggregations & return
    if filter[:skip_aggs]
      {
        results: search_response.results,
        total: search_response.total
      }
    else
      {
        results: search_response.results,
        total: search_response.total,
        aggs: readable_aggregations(search_response.aggs)
      }
    end
  end

  private

  def prepare_search_term_query(q)
    if q.blank? || q == '*'
      {
        match_all: {}
      }
    else
      free_text_query(q, fields: %w[title.searchable^10 message^5])
    end
  end

  # ------------------------- ENTITLEMENT FILTERS ------------------------------
  def access_control_filter(viewer)
    should_filter([
      term_filter('is_public', true ),            # Include public cards
      term_filter('author_id', viewer.id ),       # Include author's cards
      accessible_private_cards_filter(viewer)     # Include accessible private cards
    ])
  end

  def accessible_private_cards_filter(viewer)
    musts = term_filter('is_public', false)
    shoulds = []
    # 1. User has direct access to private card
    shoulds << user_access_level_privacy_filter(viewer)

    # 2. User has access via team to private card
    if viewer_team_ids = viewer.team_ids
      shoulds << team_level_privacy_filter(viewer, viewer_team_ids)
    end

    # 3. User is a part of channel
    if viewer_channel_ids = viewer.followed_channel_ids
      shoulds << channel_level_privacy_filter(viewer, viewer_channel_ids)
    end

    access_shoulds = should_filter(shoulds)

    must_filter([musts, access_shoulds])
  end

  def user_access_level_privacy_filter(viewer)
    musts = term_filter('users_with_access_ids', viewer.id )
    shoulds = restriction_level_access_filter(viewer)

    must_filter([musts, shoulds])
  end

  def team_level_privacy_filter(viewer, viewer_team_ids)
    musts = term_filter('shared_with_teams_ids', viewer_team_ids)
    shoulds = restriction_level_access_filter(viewer)

    must_filter([musts, shoulds])
  end

  def channel_level_privacy_filter(viewer, viewer_channel_ids)
    musts = term_filter('channel_ids', viewer_channel_ids )
    shoulds = restriction_level_access_filter(viewer)

    must_filter([musts, shoulds])
  end

  def restriction_level_access_filter(viewer)
    should_filter(
      [
        term_filter('users_with_permission_ids', viewer.id ),
        term_filter('teams_with_permission_ids', viewer.team_ids),
        must_filter([ missing_filter('users_with_permission_ids'), missing_filter('teams_with_permission_ids')])
      ]
    )
  end
  # ------------------------- ENTITLEMENT FILTERS ------------------------------

  # ----------------------------- AGGREGATIONS ---------------------------------
  def build_aggregations_query
    aggs = {}
    aggs[:content_type] = add_content_type_aggregation
    aggs[:source_display_name] = add_source_display_name_aggregation
    aggs[:author_id] = add_author_id_aggregation
    aggs
  end

  def add_content_type_aggregation
    {
      terms: {
        field: 'card_type',
        size: 1000,
        order: { _count: :desc }
      },
      aggs: {
        card_subtype: {
          terms: {
            field: 'card_subtype',
            size: 1000,
            order: { _count: :desc }
          }
        }
      }
    }
  end

  def add_source_display_name_aggregation
    get_aggregation_body('ecl_metadata.source_display_name')
  end

  def add_author_id_aggregation
    get_aggregation_body(:author_id)
  end

  def get_aggregation_body(field)
    {
      terms: {
        field: field,
        size: 1000,
        order: { _count: :desc }
      }
    }
  end

  def readable_aggregations(aggregations)
    # Sample response is at end of file.
    readable_source_display_name_aggregations(aggregations['source_display_name']) +
    readable_content_type_aggregations(aggregations['content_type']) +
    readable_author_id_aggregations(aggregations['author_id'])
  end

  def readable_source_display_name_aggregations(source_display_name_aggs)
    source_display_name_aggregations = []
    buckets = source_display_name_aggs['buckets']
    buckets.present? && buckets.each do |bucket|
      source_display_name_aggregations << { type: 'source_display_name', display_name: bucket['key'], id: bucket['key'], count: bucket['doc_count'] }
    end
    source_display_name_aggregations
  end

  def readable_content_type_aggregations(content_type_aggs)
    # Since we have to keep aggregations in sync with our SEARCH API aggregations,
    # the if-else conditions do that
    content_type_aggregations = []
    buckets = content_type_aggs['buckets']
    buckets.present? && buckets.each do |card_type_bucket|
      if card_type_bucket['key'] == 'media'
        card_type_bucket['card_subtype']['buckets'].each do |card_subtype_bucket|
          content_type_aggregations << {
            type: 'content_type',
            display_name: card_subtype_bucket['key'] == 'link' ? 'article' : card_subtype_bucket['key'],
            id: card_subtype_bucket['key'],
            count: card_subtype_bucket['doc_count'],
            card_type: card_type_bucket['key'],
            card_subtype: card_subtype_bucket['key']
          }
        end
      else
        total_count = card_type_bucket['card_subtype']['buckets'].map { |card_subtype_bucket| card_subtype_bucket['doc_count'] }.sum
        content_type_aggregations << {
          type: 'content_type',
          display_name: card_type_bucket['key'] == 'pack' ? 'pathway' : card_type_bucket['key'],
          id: card_type_bucket['key'],
          count: total_count,
          card_type: card_type_bucket['key'],
          card_subtype: ''
        }
      end

    end
    content_type_aggregations
  end

  def readable_author_id_aggregations(author_id_aggs)
    author_id_aggregations = []
    buckets = author_id_aggs['buckets']
    buckets.present? && buckets.each do |bucket|
      begin
        author = User.find(bucket['key'])
        author_id_aggregations << { type: 'author_id', display_name: author.name, id: bucket['key'], count: bucket['doc_count'] }
      rescue => e
        log.error "Search::CardSearch : User with id #{bucket['key']} is not present in database."
      end
    end
    author_id_aggregations
  end
  # ----------------------------- AGGREGATIONS ---------------------------------
end

# WARNING!! No code will be executed beyond this line.
__END__
Aggregations response for `search_in_channel` method
{
  "content_type"=>{
    "doc_count_error_upper_bound"=>0,
    "sum_other_doc_count"=>0,
    "buckets"=>[
      {
        "key"=>"media",
        "doc_count"=>21,
        "card_subtype"=>{
          "doc_count_error_upper_bound"=>0,
          "sum_other_doc_count"=>0,
          "buckets"=>[
            {"key"=>"link", "doc_count"=>20},
            {"key"=>"text", "doc_count"=>1}
          ]
        }
      }
    ]
  },
  "source_display_name"=>{
    "doc_count_error_upper_bound"=>0,
    "sum_other_doc_count"=>0,
    "buckets"=>[
      {"key"=>"UGC", "doc_count"=>9}
    ]
  },
  "author_id"=>{
    "doc_count_error_upper_bound"=>0,
    "sum_other_doc_count"=>0,
    "buckets"=>[
      {"key"=>76828, "doc_count"=>9}
    ]
  }
}

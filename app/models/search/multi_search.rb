class Search::MultiSearch
  def initialize()
    @requests = []
  end

  def add(key, body)
    query = body[:search][:query]
    from = body[:search][:offset]
    size = body[:search][:limit]
    sort = body[:search][:sort]
    index = body[:instance].search_class.searchkick_index.name
    _body = {}
    _body[:index] = index
    _body[:search] = query
    _body[:search][:from] = from if from
    _body[:search][:size] = size if size
    _body[:search][:sort] = sort if sort
    @requests << {key: key, search: _body, instance: body[:instance]}
  end

  def execute
    body = @requests.map{|v| v[:search]}
    search_result = ES_SEARCH_CLIENT.msearch(body: body)
    #{"responses"=>[{"took"=>1, "timed_out"=>false, "_shards"=>{"total"=>1, "successful"=>1, "failed"=>0}, "hits"=>{"total"=>1, "max_score"=>0.46593314, "hits"=>[{"_index"=>"cards_test_20151019151443577", "_type"=>"card", "_id"=>"12", "_score"=>0.46593314, "_source"=>{"id"=>12, "cards_config_id"=>2, "group_id"=>nil, "super_card_id"=>nil, "author_id"=>2, "created_at"=>"2015-10-19T22:14:39.000Z", "updated_at"=>"2015-10-19T22:14:39.000Z", "is_public"=>true, "is_manual"=>false, "features"=>{}, "client_id"=>nil, "client_item_id"=>nil, "comments_count"=>0, "order_index"=>0, "slug"=>"card-59acdce5-0ad5-406b-a5dc-c2a2081bec82", "impressions_count"=>0, "outbounds_count"=>0, "shares_count"=>0, "answers_count"=>0, "expansions_count"=>0, "plays_count"=>0, "hidden"=>false, "resource_id"=>nil, "metadata"=>{}, "topic_ids"=>[], "card_type"=>"media", "actions"=>{"student"=>{}, "instructor"=>{}}, "author_name"=>"first last", "sub_cards_count"=>0, "comments"=>[], "shown_to_users"=>[], "location"=>nil, "subcards"=>[], "mentioned_users"=>[], "channel_ids"=>[], "packs"=>[], "in_private_channel"=>false, "content"=>{"message"=>"matchy me"}, "card_template"=>"video", "tags"=>[], "like_count"=>0, "handle"=>"@handle2", "author"=>{"id"=>2, "first_name"=>"fname2", "last_name"=>"lname2", "handle"=>"@handle2", "name"=>"fname2 lname2", "photo"=>"http://cdn-test-host.com/assets/anonymous-user.png", "picture"=>"http://cdn-test-host.com/assets/anonymous-user.png", "influencer"=>false}, "file_resources"=>[], "quiz_question_options"=>[], "quiz_question_stats"=>nil, "config"=>{"id"=>2, "card_type"=>"media", "action_layout_type"=>"single_action", "ui_layout_type"=>"video", "user_id"=>4, "shareable"=>true, "created_at"=>"2015-10-19T22:14:32.000Z", "updated_at"=>"2015-10-19T22:14:32.000Z", "roles"=>[]}}}]}}, {"took"=>4, "timed_out"=>false, "_shards"=>{"total"=>1, "successful"=>1, "failed"=>0}, "hits"=>{"total"=>1, "max_score"=>2.2035804, "hits"=>[{"_index"=>"edcast_users_test_20151019151447580", "_type"=>"user", "_id"=>"8", "_score"=>2.2035804, "_source"=>{"id"=>8, "email"=>"person8@example.com", "first_name"=>"trung", "last_name"=>"pham", "created_at"=>"2015-10-19T22:14:45.000Z", "updated_at"=>"2015-10-19T22:14:47.000Z", "picture_url"=>nil, "anonimage_file_name"=>nil, "anonimage_content_type"=>nil, "anonimage_file_size"=>nil, "anonimage_updated_at"=>nil, "avatar_file_name"=>nil, "avatar_content_type"=>nil, "avatar_file_size"=>nil, "avatar_updated_at"=>nil, "location_id"=>nil, "handle"=>"@trungpham", "is_brand"=>false, "bio"=>"my bio", "coverimage_file_name"=>nil, "coverimage_content_type"=>nil, "coverimage_file_size"=>nil, "coverimage_updated_at"=>nil, "password_reset_required"=>false, "is_complete"=>true, "name"=>"trung pham", "photo"=>"http://cdn-test-host.com/assets/anonymous-user.png", "normalized_handle"=>"@trungpham", "pubnub_channels_to_subscribe_to"=>["User-8-private-f2096286bda7511f49e31df8288663a8", "pbnb-glc"], "followed_users"=>[], "followed_channels"=>[]}}]}}]}

    results = {}
    search_result['responses'].each_with_index do |response, i|
      instance = @requests[i][:instance]
      offset = @requests[i][:search][:from]
      limit = @requests[i][:search][:size]
      response_class = instance.response_class
      search_class = instance.search_class
      result = Searchkick::Results.new(search_class, response)

      if search_class.respond_to?(:do_post_processing)
        result = search_class.do_post_processing(result)
      end

      results[@requests[i][:key]] = response_class.new({
                             offset: offset,
                             limit: limit,
                             total: result.total_count,
                             results: result.results,
                             status: 'success',
                         }.merge(instance.response_fields))
    end
    results
  end
end

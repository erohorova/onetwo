class Search::UserSearch < Search::QuerySearch
  def suggest(q: nil, organization_id: nil, offset: 0, limit: 10)
    @q = q
    search_query = nil
    musts = [filter_incomplete_user, filter_suspended_user]
    unless organization_id.nil?
      musts << filter_for_organization_id(organization_id)
    end
    search_query = q.nil? ? nil : aggregate_filters_and_query(musts: musts, query: {prefix: {"name.sortable" => q.downcase}})
    _search(query: search_query, offset: offset, limit: limit, sort: {_score: 'desc'})
  end

  # if allow_blank_q is true, then a blank q will return all results; otherwise none
  def search_body(q: nil, viewer: , offset: 0, limit: 10, sort: :score,
                  allow_blank_q: false, filter: {}, order: [{sort: 'desc'}, {id: 'asc'}],
                  load: false, use_custom_fields: false, custom_fields_names: [])
    @q = q
    musts = []

    musts << filter_suspended_user unless filter[:include_suspended]

    musts << filter_only_suspended_user if filter[:only_suspended]

    organization_id = viewer.try(:organization_id) || Organization.default_org&.id
    musts << filter_for_organization_id(organization_id)

    common_aggs_filters = [filter_suspended_user, filter_for_organization_id(organization_id)]

    skills_aggs_filters, custom_fields_aggs_filters, dynamic_selection_musts = Array.new(3) { [] }

    teams_and_channels_shoulds = []
    teams_and_channels_shoulds << filter_for_team_ids(filter[:team_ids]) unless filter[:team_ids].blank?
    teams_and_channels_shoulds << filter_for_channel_ids(filter[:channel_ids]) unless filter[:channel_ids].blank?
    musts << should_filter(teams_and_channels_shoulds) if teams_and_channels_shoulds.present?

    musts << negate_filter(term_filter(:id, filter[:exclude_ids])) if filter[:exclude_ids].present?

    #skills filter by expert_topics.topic_label
    if filter[:expertises_names]
      expertises_filter = filter_for_expertises(filter[:expertises_names])
      musts << expertises_filter
      skills_aggs_filters << expertises_filter
    end
    #skills filter expert_topics.topic_name
    if filter[:expert_topic_names]
      expertises_filter = filter_for_expertise_name(filter[:expert_topic_names])
      musts << expertises_filter
      skills_aggs_filters << expertises_filter
    end

    #names filter for dynamic selection
    if filter[:names_for_filtering]
      names_filter = filter_for_names(filter[:names_for_filtering])
      dynamic_selection_musts << names_filter
      common_aggs_filters << names_filter
    end

    #emails filter for dynamic selection
    if filter[:emails_for_filtering]
      emails_filter = filter_for_emails(filter[:emails_for_filtering])
      dynamic_selection_musts << emails_filter
      common_aggs_filters << emails_filter
    end

    #custom fields filter
    if filter[:custom_fields]&.is_a?(Array)
      custom_fields = filter[:custom_fields]
      custom_fields_terms = []
      custom_fields.each do |cf|
        name_term = term_filter('custom_fields.display_name', cf['name'])
        values_term = term_filter('custom_fields.value', cf['values'])
        custom_fields_terms << filter_for_custom_fields(name_term, values_term)
      end
      custom_fields_musts = must_filter(custom_fields_terms)
      if filter[:is_dynamic_selection]
        dynamic_selection_musts << custom_fields_musts
      else
        musts << custom_fields_musts
      end
      custom_fields_aggs_filters << custom_fields_musts
    end
    musts << must_filter(dynamic_selection_musts) if dynamic_selection_musts.present?

    #date filter
    if filter[:from_date].present? && filter[:to_date].present?
      joined_date_filter = filter_for_joined_date(filter[:from_date], filter[:to_date])
      musts << joined_date_filter
      common_aggs_filters << joined_date_filter
    end

    #categories filter
    follows_and_roles_shoulds = []
    follows_and_roles_shoulds << filter_for_following(viewer) if viewer && filter[:following]
    follows_and_roles_shoulds << filter_for_followers(viewer) if viewer && filter[:followers]
    follows_and_roles_shoulds << filter_for_roles(filter[:role]) if filter[:role]
    if follows_and_roles_shoulds.present?
      categories_shoulds = should_filter(follows_and_roles_shoulds)
      musts << categories_shoulds
      common_aggs_filters << categories_shoulds
    end

    search_query = nil

    search_query =  case filter[:scope]
                    when Team
                      team_query(
                        query: q,
                        team: filter[:scope],
                        role: filter[:role],
                        include_suspended: filter[:include_suspended],
                        uncomplete: filter[:uncomplete])
                    else
                      if filter[:search_expertise]
                        # org context was getting skipped from expert search for users
                        aggregate_filters_and_query(musts: musts, query: topics_query(q, viewer, "expert"))
                      elsif filter[:search_peer_learners]
                        aggregate_filters_and_query(musts: musts, query: topics_query(q, viewer, "peer"))
                      elsif q.present? && filter[:search_by].present?
                        query = free_text_query(q, prefix_length: nil, fields: filter[:search_by])
                        common_aggs_filters << query
                        aggregate_filters_and_query(
                          query: query,
                          musts: musts
                        )
                      elsif q.present? || allow_blank_q
                        search_query = dis_max_query(query: q)
                        query = if use_custom_fields
                                  should_filter([search_query, query_with_custom_fields(q)])
                                else
                                  search_query
                                end
                        common_aggs_filters << query if q.present?
                        aggregate_filters_and_query(
                          query: query,
                          musts: musts
                        )
                      end
                    end

    name_order = [:last_name, :first_name, :email].map { |field| sort_field_query(field, :asc) }
    order = [sort_field_query(:is_suspended, :asc)] + ( case sort.to_sym
                                                        when :name
                                                          name_order
                                                        when :role
                                                          get_filter_role_order(filter: filter) + name_order
                                                        when :score
                                                          [sort_field_query(:_score)]
                                                        else
                                                          [sort_field_query(sort, order)]
                                                        end)

    if use_custom_fields && !filter[:skip_aggs]
      # get dynamic aggs for each of custom fields enabled in people search
      fields_aggregations = custom_fields_names.map do |name|
                              field_filter = if custom_fields_aggs_filters.present?
                                              # exclude filter for current custom field to get all values in aggregations
                                              all_filters = custom_fields_aggs_filters.reduce({}, :merge)[:bool][:must]
                                              all_filters.reject do |i|
                                                i.dig(:nested, :filter, :bool, :must, :term, 'custom_fields.display_name') == name
                                              end
                                            else
                                              []
                                            end

                              global_field_aggs(name, common_aggs_filters,  skills_aggs_filters, field_filter)
                            end

      search_query[:aggs] = aggregation_query(
                              common_aggs_filters,
                              custom_fields_aggs_filters,
                              fields_aggregations.reduce({}, :merge)
                            )
    end
    { search: { query: search_query, offset: offset, limit: limit, sort: order, load: load }, instance: self }
  end

  def search(pars)
    _search(search_body(pars)[:search])
  end

  def search_class
    User
  end

  def response_class
    Search::UserSearchResponse
  end

  def response_fields
    super.merge(
        query: @q,
    )
  end

  def count_team(q: nil, team:)
    _count(team_query(query: q, team: team))
  end

  private

  def get_filter_role_order(filter: {})
    case filter[:scope]
    when Team
      [{'teams.role' => { mode: 'min', order: 'asc', nested_path: 'teams', nested_filter: team_filter(filter[:scope]) } }]
    else
      %w[organization_role]
    end
  end

  # ------------------------------FILTERS---------------------------------------
  def filter_incomplete_user
    term_filter(:is_complete, true)
  end

  def filter_suspended_user
    term_filter(:status, [User::ACTIVE_STATUS, User::INACTIVE_STATUS])
  end

  def filter_only_suspended_user
    term_filter(:status, User::SUSPENDED_STATUS)
  end

  def filter_for_organization_id(org_id)
    term_filter(:organization_id, org_id)
  end

  def filter_for_team_ids(team_ids)
    {
      nested: {
        path: :teams,
        filter: term_filter('teams.id', team_ids)
      }
    }
  end

  def filter_for_expertises(expertise_names)
    terms = []
    expertise_names.each do |name|
      terms << { term: {'expert_topics.topic_label' => name}}
    end
    should_filter(terms)
  end  

  def filter_for_expertise_name(expertise_names)
    terms = []
    expertise_names.each do |name|
      terms << { term: {'expert_topics.topic_name' => name}}
    end
    should_filter(terms)
  end

  def filter_for_joined_date(from_date, to_date)
    {
      range: {
        joined_at: {
          gte: from_date,
          lte: to_date,

        }
      }
    }
  end

  def filter_for_following(viewer)
    term_filter(:follower_ids, viewer.id)
  end

  def filter_for_followers(viewer)
    term_filter(:following_ids, viewer.id)
  end

  def filter_for_channel_ids(channel_ids)
    term_filter("followed_channels.id", channel_ids)
  end

  # used sortable, because this field uses edcast_sort_analyzer with keyword tokenizer,
  # that accepts whatever text it is given and outputs the exact same text as a single term.
  def filter_for_emails(emails)
    term_filter('email.sortable', emails)
  end

  def filter_for_names(names)
    term_filter('name.sortable', names.map { |n| n.mb_chars.downcase })
  end

  def filter_for_custom_fields(must, shoulds)
    {
      nested: {
        path: :custom_fields,
        filter: combined_filter(must, shoulds)
      }
    }
  end

  def filter_for_roles(role_name)
    term_filter(:roles, role_name)
  end

  def team_filter(team, member_role: nil)
    query = {
      and: [
        term_filter('teams.id', team.id)
      ]
    }

    query[:and] << term_filter('teams.role', member_role) unless member_role.nil?
    query
  end

  def topic_filter(viewer, type)
    terms = type == "expert" ?
      {'expert_topics.topic_name' => viewer.learning_topics_name} : {'learning_topics.topic_name' => viewer.learning_topics_name }
    { terms: terms }
  end

  # --------------------------------QUERIES-------------------------------------
  # EP-20015 : This method is added in order to improve the matching of user names.
  # Exact match for user names should be given more preference as compared to
  # fuzzy match.
  def dis_max_query(query: q)
    return { match_all: {} } if query.blank?
    {
      dis_max: {
        queries: [
          {
            multi_match: {
              query: query,
              fields: [
                "name.searchable^200",
                "first_name.searchable^150",
                "last_name.searchable^150",
                "email.searchable^50",
                "handle.searchable^50",
                "expert_topics.searchable_topic_label"
              ]
            }
          },
          free_text_query(query, prefix_length: nil)
        ]
      }
    }
  end

  def topics_query(query, viewer, topic_type)
    {
      query: {
        filtered: {
          filter: {
            bool: {
              filter: topic_filter(viewer, topic_type)
            }
          },
          query: query
        }
      }
    }
  end

  def query_with_custom_fields(query)
    {
      nested: {
        path: :custom_fields,
        query: {
          filtered: {
            query: {
              match: {
                'custom_fields.value' => query
              }
            },
            filter: term_filter('custom_fields.enabled_in_search', true)
          }
        }
      }
    }
  end

  def aggregation_query(common_aggs_filters, custom_fields_aggs_filters, custom_fields_aggs)
    common_skills_filter = combined_filter(common_aggs_filters, custom_fields_aggs_filters)
    skills_aggregations = skills_aggs(common_skills_filter)
    skills_aggregations.merge! custom_fields_aggs
  end

  # ElasticSearch provides a special aggregation—global—that is executed globally on all the documents
  # without being influenced by the query. We can use it with filters.
  def global_field_aggs(field_name, common_aggs_filters, skills_aggs_filters, field_filter)
    {
      "#{field_name}": {
        global: {},
        aggs: {
          filtered: {
            filter: {
              bool: {
                must: common_aggs_filters + [must_filter(field_filter)], # Must be taken all common filters and filters from adjacent sections of custom fields
                should: skills_aggs_filters # At least one of skills filters must match.
              }
            },
            aggs: field_agg_body(field_name)
          }
        }
      }
    }
  end

  def field_agg_body(field_name)
    {
      keywords: {
        nested: {
          path: :custom_fields
        },
        aggs: {
          values: {
            terms: {
              field: 'custom_fields.display_name',
              include: field_name,
              size: 0
            },
            aggs: {
              count: {
                terms: {
                  field: 'custom_fields.value',
                  size: 0
                }
              }
            }
          }
        }
      }
    }
  end

  def skills_aggs(skills_filter)
    {
      expert_topics: {
        global: {},
        aggs: {
          filtered: {
            filter: skills_filter,
            aggregations: {
              values: {
                terms: {
                  field: 'expert_topics.topic_label',
                  size: 0
                }
              }
            }
          }
        }
      }
    }
  end

  def team_query(query: nil, team: , role: nil, include_suspended: nil, uncomplete: false)
    search_query = free_text_query(query)
    musts = uncomplete ? [] : [filter_incomplete_user]

    musts << {
      nested: {
        path: :teams,
        filter: team_filter(team, member_role: role)
      }
    }

    musts << filter_suspended_user unless include_suspended
    aggregate_filters_and_query(musts: musts, query: search_query)
  end
end

class Search::QuerySearch

  def initialize(opts = {})
    @load = opts[:load]
  end

  def aggregate_filters_and_query(musts: , query: )
    {
      query: {
        filtered: {
          filter: {
            bool: {
              must: musts.compact
            }
          },
          query: query
        }
      }
    }
  end

  def free_text_query(query, prefix_length: 3, fields: nil)
    if query.blank?
      {
        match_all: {}
      }
    else
      fields ||= search_class.searchable_fields_query_format
      multi_match = {
        query: query,
        fields: fields,
        fuzziness: "AUTO"
      }
      multi_match[:prefix_length] = prefix_length if prefix_length.present?

      {
        multi_match: multi_match
      }
    end
  end

  def terms_aggregation(property)
    {
      property => {
        terms: {field: property}
      }
    }
  end

  def should_filter(shoulds)
    {
      bool: {
        should: shoulds
      }
    }
  end

  def must_filter(musts)
    {
      bool: {
        must: musts
      }
    }
  end

  def combined_filter(musts, shoulds)
    {
      bool: {
        must: musts,
        should: shoulds
      }
    }
  end

  def term_filter(property, value)
    return nil if value.nil?
    exact_match_property = search_class.exact_match_field_query_mapping(property)
    _filter = if value.is_a?(Array)
      { terms: { exact_match_property => value } }
    else
      { term: { exact_match_property => value } }
    end
  end

  def negate_filter(filter)
    filter.nil? ? nil : { not: filter }
  end

  def missing_filter(property)
    { missing: { field: property } }
  end

  def exists_filter(property)
    { exists: { field: property } }
  end

  def boolean_or_filter(*filters)
    { or: filters.compact }
  end

  def boolean_and_filter(*filters)
    { and: filters.compact }
  end

  def sort_field_query(field, order='desc')
    { search_class.sortable_field_mapping(field) => { order: order } }
  end

  def search_by_id(ids)
    _retrieve_one_or_many_by_ids(musts: [], ids: ids)
  end

  def get_org_filter(organization_id)
    term_filter(:organization_id, organization_id)
  end

  private

  def _count query
    begin
      index_name = search_class.try(:index_name) || search_class.searchkick_index.name
      results = Searchkick.client.count index: index_name, body: query
      return results["count"]
    rescue => e
      log.error "Elastic #{search_class.name} count failed for query: #{query} with exception #{e.inspect}"
      return 0
    end
  end

  def _retrieve_one_or_many_by_ids(musts: [], ids: )
    is_array = Array === ids
    _musts = musts + [term_filter(:id, ids)]
    response = _search(query: aggregate_filters_and_query(musts: _musts, query: nil),
                       limit: (is_array ? ids.size : 1),
                       offset: 0)

    if response.status == 'failure'
      log.error "Card features retrieval from Elastic Search failed for id(s): #{ids} \
      Status: #{response.status}, Response: #{response}"
      return nil
    end
    if is_array
      response.results
    else
      response.results.first
    end
  end

  def _search(query: nil, aggs: nil, offset: 0, limit: 10, sort: nil, load: false, source: false)
    @offset = offset
    @limit = limit

    # api/v2 require load true and api/v1 use load false.
    # methods that are common to both, go through chain of methods
    # Longer term, we should set it as instance variable and remove load:

    @load ||= load

    return failure_response(query) if query.nil?
    query.merge!(from: @offset, size: @limit)
    query.merge!(_source: source) if source
    query.merge!(sort: sort) unless sort.nil?
    query.merge!(aggs: aggs) unless aggs.nil?

    begin
      response = search_class.search body: query, load: @load
      if response.nil?
        log.error "Elastic #{search_class.name} search failed for query: #{query} with nil response"
        return failure_response(query)
      else
        return response_class.new({
                                      offset: @offset,
                                      limit: @limit,
                                      total: response.total_count,
                                      results: response.results,
                                      status: 'success',
                                      aggs: response.aggregations,
                                  }.merge(response_fields))
      end
    rescue => e
      log.error "Elastic #{search_class.name} search failed for query: #{query} with exception #{e.inspect}"
      return failure_response(query)
    end
  end

  def failure_response(query)
    response_class.new({
                           offset: @offset,
                           limit: @limit,
                           total: 0,
                           results: [],
                           status: 'failure',
                       }.merge(response_fields))
  end

  # additional fields for the response object to be supplied by the subclass
  def response_fields
    {}
  end

end

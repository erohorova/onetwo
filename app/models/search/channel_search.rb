class Search::ChannelSearch < Search::QuerySearch

  def build_query(q: nil, viewer:, filter: {})
    musts = []
    organization_id = viewer.try(:organization_id) || Organization.default_org&.id
    musts << get_org_filter(organization_id)
    common_aggs_filters = [get_org_filter(organization_id)] unless filter[:skip_aggs]
    curators_aggs_filters, followers_aggs_filters, collaborators_aggs_filters = Array.new(3) { [] } unless filter[:skip_aggs]
    private_term = filter_for_private_channels(true)
    open_term = filter_for_private_channels(false)

    # Dont invoke get_private_filter if include_private is true
    musts << get_private_filter unless filter[:include_private]&.to_bool

    musts << onboarding_channels_filter(filter[:onboarding]) if filter[:onboarding]

    musts << filter_for_users_by_id('followers', viewer.id) if filter[:only_my_channels]

    unless (topics = filter[:by_topics]).nil?
      musts << term_filter('topics.name', topics)
    end

    if filter[:is_promoted].present?
      musts << term_filter('is_promoted', filter[:is_promoted])
    end

    if filter[:exclude_ids].present?
      musts << negate_filter(term_filter(:id, filter[:exclude_ids]))
    end

    if filter[:is_private].present?
      is_private = filter[:is_private].to_bool
      musts << filter_for_private_channels(is_private)
      common_aggs_filters << filter_for_private_channels(is_private) unless filter[:skip_aggs]
    end

    # if user doesn't have admins' permissions - he should not get not his private channels
    unless viewer.is_admin_user? || viewer.is_admin?
      #checking: whether the user is the channel's curators or collaborator or follower.
      # if yes - he can receive this channel in a result of a search
      access_terms = filter_by_access(viewer)

      # getting private channels to which the user has access and all open channels
      private_terms = combined_filter(private_term, access_terms)
      shoulds = should_filter([private_terms, open_term])
      musts << shoulds
      common_aggs_filters << shoulds unless filter[:skip_aggs]
    end

    if filter[:curators_names]&.is_a?(Array)
      filter_users = filter_for_users_by_name('curators', filter[:curators_names])
      musts << filter_users
      curators_aggs_filters << filter_users unless filter[:skip_aggs]
    end

    if filter[:collaborators_names]&.is_a?(Array)
      filter_users = filter_for_users_by_name('collaborators', filter[:collaborators_names])
      musts << filter_users
      collaborators_aggs_filters << filter_users unless filter[:skip_aggs]
    end

    if filter[:followers_names]&.is_a?(Array)
      filter_users = filter_for_users_by_name('followers', filter[:followers_names])
      musts << filter_users
      followers_aggs_filters << filter_users unless filter[:skip_aggs]
    end

    text_query = free_text_query(q)
    common_aggs_filters << text_query if q.present? && !filter[:skip_aggs]

    query = aggregate_filters_and_query(musts: musts, query: text_query)

    aggs = aggregation_query(common_aggs_filters, curators_aggs_filters, collaborators_aggs_filters, followers_aggs_filters) unless filter[:skip_aggs]
    query[:aggs] = aggs unless filter[:skip_aggs]

    query
  end

  def search_body(q: nil, viewer: , limit: 10, offset: 0, sort: nil, filter: {}, load: false)
    sort = [sort].flatten.compact.select { |s| s.present? }

    query = build_query(q: q, viewer: viewer, filter: filter)

    order = []

    if sort.present?
      sort.each do |s|
        order << sort_field_query(s[:key], s[:order])
      end
    else
      order << sort_field_query(:created_at)
    end

    {search: {query: query, offset: offset, limit: limit, sort: order, load: load}, instance: self}
  end

  def search(q: nil, viewer: , limit: 10, offset: 0, sort: {}, filter: {}, load: false)
    _search(search_body(q: q, viewer: viewer, limit: limit, offset: offset, sort: sort, filter: filter, load: load)[:search])
  end


  def autosuggest_for_cms(q, organization_id, filter: {}, limit: 10, offset: 0)
    q ||= ''
    musts = []
    musts << get_org_filter(organization_id)
    musts << get_private_filter unless filter[:include_private] == true
    musts << get_prefix_query(q.downcase)

    _search(query: aggregate_filters_and_query(musts: musts, query: free_text_query(nil)), offset: offset, limit: limit, sort: sort_field_query(:created_at))
  end

  def search_class
    Channel
  end

  def response_class
    Search::ChannelSearchResponse
  end

  def response_fields
    super.merge(
        query: @q,
    )
  end

  def get_prefix_query(prefix)
    { prefix: { "label.sortable" => prefix } }
  end

  def get_private_filter(include_private = false)
    term_filter(:is_private, include_private)
  end

  def onboarding_channels_filter(onboarding)
    term_filter(:visible_during_onboarding, onboarding)
  end

  def filter_by_access(viewer)
    access_filter = %w[followers curators collaborators].map do |type|
                      filter_for_users_by_id(type, viewer.id)
                    end
    access_filter << term_filter('followed_team_ids', viewer.teams.pluck(:id))
    should_filter(access_filter)
  end

  def filter_for_users_by_name(user_type, user_names)
    user_names.map! do |user_name|
      term_filter("#{user_type}.name", user_name)
    end

    should_filter(user_names)
  end

  def filter_for_users_by_id(user_type, viewer_id)
    term_filter("#{user_type}.id", viewer_id)
  end

  def filter_for_private_channels(value)
    term_filter(:is_private, value)
  end


  # ElasticSearch provides a special aggregation—global—that is executed globally on all the documents
  # without being influenced by the query. We can use it with filters.

  def aggregation_query(common_aggs_filters, curators_filters, collaborators_filters, followers_filters)
    {
      curators: {
        global: {},
        aggs: {
          filtered: {
            filter: {
              bool: {
                must: common_aggs_filters,
                should: collaborators_filters + followers_filters
              }
            },
            aggs: {
              values: {
                terms: {
                  field: 'curators.name',
                  size: 0
                }
              }
            }
          }
        }
      },
      collaborators: {
        global: {},
        aggs: {
          filtered: {
            filter: {
              bool: {
                must: common_aggs_filters,
                should: curators_filters + followers_filters
              }
            },
            aggs: {
              values: {
                terms: {
                  field: 'collaborators.name',
                  size: 0
                }
              }
            }
          }
        }
      },
      followers: {
        global: {},
        aggs: {
          filtered: {
            filter: {
              bool: {
                must: common_aggs_filters,
                should: curators_filters + collaborators_filters
              }
            },
            aggs: {
              values: {
                terms: {
                  field: 'followers.name',
                  size: 0
                }
              }
            }
          }
        }
      }
    }
  end
end

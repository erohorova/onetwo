class Search::TeamSearch < Search::QuerySearch

  def build_query(q: nil, viewer:, filter: {})
    musts = []
    should = []
    organization_id = viewer.try(:organization_id)
    musts << get_org_filter(organization_id)
    private_term = filter_for_private_teams(true)
    open_term = filter_for_private_teams(false)

    common_aggs_filters = [get_org_filter(organization_id)] unless filter[:skip_aggs]
    leaders_aggs_filters, admins_aggs_filters, members_aggs_filters = Array.new(3) { [] } unless filter[:skip_aggs]

    musts << get_channel_filter(filter[:channel_ids]) if filter[:channel_ids].present?
    musts << filter_for_user_teams(viewer) if filter[:current_user_teams]
    musts << filter_for_pending_user_teams(viewer) if filter[:current_user_pending_teams]

    if filter[:is_private].present?
      is_private = filter[:is_private].to_bool
      musts << filter_for_private_teams(is_private)
      common_aggs_filters << filter_for_private_teams(is_private) unless filter[:skip_aggs]
    end

    unless viewer.authorize?(Permissions::ADMIN_ONLY)
      shoulds = [filter_for_user_teams(viewer), filter_for_pending_user_teams(viewer)]
      access_terms = should_filter(shoulds)
      private_terms = combined_filter(private_term, access_terms)
      teams_shoulds = should_filter([private_terms, open_term])
      musts << teams_shoulds
      common_aggs_filters << teams_shoulds unless filter[:skip_aggs]
    end

    if filter[:leaders_id].present?
      musts << { term: {'leaders.id' => filter[:leaders_id]}}
    end

    if filter[:leaders_names].present?
      filter_users = filter_for_users('leaders', filter[:leaders_names])
      musts << filter_users
      leaders_aggs_filters << filter_users unless filter[:skip_aggs]
    end

    if filter[:admins_names].present?
      filter_users = filter_for_users('admins', filter[:admins_names])
      musts << filter_users
      admins_aggs_filters << filter_users unless filter[:skip_aggs]
    end

    if filter[:members_names].present?
      filter_users = filter_for_users('members', filter[:members_names])
      musts << filter_users
      members_aggs_filters << filter_users unless filter[:skip_aggs]
    end

    text_query = free_text_query(q, prefix_length: nil)
    common_aggs_filters << text_query if q.present? && !filter[:skip_aggs]

    query = aggregate_filters_and_query(musts: musts, query: text_query)

    aggs = aggregation_query(common_aggs_filters, leaders_aggs_filters, admins_aggs_filters, members_aggs_filters) unless filter[:skip_aggs]
    query[:aggs] = aggs unless filter[:skip_aggs]

    query
  end

  def search_body(q: nil, viewer: , limit: 10, offset: 0, sort: nil, filter: {}, load: false)
    sort = [sort].flatten.compact.select { |s| s.present? }

    query = build_query(q: q, viewer: viewer, filter: filter)

    order = []

    if sort.present?
      sort.each do |s|
        order << sort_field_query(s[:key], s[:order])
      end
    end

    {search: {query: query, offset: offset, limit: limit, sort: order, load: load}, instance: self}
  end

  def search(q: nil, viewer: , limit: 10, offset: 0, sort: {}, filter: {}, load: false)
    _search(search_body(q: q, viewer: viewer, limit: limit, offset: offset, sort: sort, filter: filter, load: load)[:search])
  end

  def get_channel_filter(channel_ids)
    term_filter(:channel_ids, channel_ids)
  end

  def search_class
    Team
  end

  def response_class
    Search::TeamSearchResponse
  end

  def response_fields
    super.merge(
        query: @q,
    )
  end

  def filter_for_users(user_type, user_names)
    user_names.map! do |user_name|
      term_filter("#{user_type}.name", user_name)
    end

    should_filter(user_names)
  end

  def filter_for_user_teams(viewer)
    term_filter(:users_ids, viewer.id)
  end

  def filter_for_pending_user_teams(viewer)
    term_filter(:pending_users_ids, viewer.id)
  end

  def filter_for_private_teams(value)
    term_filter(:is_private, value)
  end

  # ElasticSearch provides a special aggregation—global—that is executed globally on all the documents
  # without being influenced by the query. We can use it with filters.
  #
  # use common_aggs_filters - for each of aggs section
  # use combines of leaders_aggs_filters, admins_aggs_filters and
  # members_aggs_filters to save visible all checkboxes in a current section
  # and response to filters from other sections.
  def aggregation_query(common_aggs_filters, leaders_aggs_filters, admins_aggs_filters, members_aggs_filters)
    {
      leaders: {
        global: {},
        aggs: {
          filtered: {
            filter: {
              bool: {
                must: common_aggs_filters,
                should: admins_aggs_filters + members_aggs_filters
              }
            },
            aggs: {
              values: {
                terms: {
                  field: 'leaders.name',
                  size: 0
                }
              }
            }
          }
        }
      },
      admins: {
        global: {},
        aggs: {
          filtered: {
            filter: {
              bool: {
                must: common_aggs_filters,
                should: leaders_aggs_filters + members_aggs_filters
              }
            },
            aggs: {
              values: {
                terms: {
                  field: 'admins.name',
                  size: 0
                }
              }
            }
          }
        }
      }
    }
  end

end

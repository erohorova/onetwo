# DEPRECATED: Video streams are not searchable. they're are part of cards
class Search::VideoStreamSearch < Search::QuerySearch

  def search_body(q: nil, viewer: , offset: 0, limit: 10, filter_params: {}, sort_params:{_score: 'desc'})
    @q = q
    org_id = viewer.try(:organization_id) || filter_params[:organization_id] || Organization.default_org&.id

    musts = [
      term_filter(:state, 'published'),
      term_filter(:organization_id, org_id),
      boolean_or_filter(term_filter(:status, ['live', 'upcoming']), term_filter(:past_stream_available?, true))
    ]

    # Excludes private channels from results
    organization = Organization.find(org_id)
    channel_ids = Channel.readable_channels_for(viewer, _organization: organization, onboarding: false).map(&:id)
    musts << boolean_or_filter(term_filter(:channel_ids, channel_ids), missing_filter(:channel_ids))

    musts << negate_filter(missing_filter(:creator_id)) if filter_params[:creator_must]

    musts << term_filter(:channel_ids, filter_params[:channel_ids])
    musts << negate_filter(term_filter(:id, filter_params[:exclude_ids]))

    musts << {range: filter_params[:range]}  if filter_params[:range]

    return {search: {query: aggregate_filters_and_query(musts: musts, query: free_text_query(q)), offset: offset, limit: limit, sort: sort_params}, instance: self}
  end

  def search(q: nil, viewer: , offset: 0, limit: 10, filter_params: {}, sort_params: {_score: 'desc'})
    _search(search_body(q: q, viewer: viewer, offset: offset, limit: limit, filter_params: filter_params, sort_params:sort_params)[:search])
  end

  def search_for_cms(_free_text_query, organization_id, filter_params: {}, offset: 0, limit: 10, sort: {})

    musts = [
      term_filter(:organization_id, organization_id),
      negate_filter(term_filter(:state, 'draft'))
    ]

    [:status, :state, :is_official].each do |par|
      musts << term_filter(par, filter_params[par])
    end

    unless filter_params[:channel_ids].nil?
      if filter_params[:allow_empty_channel_ids]
        musts << boolean_or_filter(missing_filter(:channel_ids), term_filter(:channel_ids, filter_params[:channel_ids]))
      else
        musts << term_filter(:channel_ids, filter_params[:channel_ids])
      end
    end

    musts << term_filter('tags.name', filter_params[:topics])
    musts << {range: filter_params[:range]} if filter_params[:range]
    musts << term_filter(:creator_id, filter_params[:creator_id]) if filter_params[:creator_id]

    return _search(query: aggregate_filters_and_query(musts: musts, query: free_text_query(_free_text_query)), offset: offset, limit: limit, sort: sort_field_query((sort[:key] || :created_at), sort[:order]))
  end

  def find_objects_with_invalid_score
    query = {
      query: {
        filtered: {
          filter: {
            or: [
              {range: { weekly_score_valid_till: { 'lte': Time.zone.now }}},
              {range: { monthly_score_valid_till: { 'lte': Time.zone.now }}},
              {range: { quarterly_score_valid_till: { 'lte': Time.zone.now }}}
            ]
          }
        }
      }
    }
    _search(query: query, sort: sort_field_query(:created_at), limit: VideoStream.count, offset: 0)
  end

  def filter_channel_status_musts(channel_ids, status)
    musts = []
    musts << boolean_or_filter(term_filter(:status, ['live', 'upcoming']), term_filter(:past_stream_available?, true))
    musts << term_filter(:channel_ids, channel_ids)
    musts << term_filter(:state, 'published')
    musts << term_filter(:status, status) if status != :all
    musts
  end

  def find_popular_videos(channel_ids, status , limit, offset)
    musts = filter_channel_status_musts(channel_ids, status)
    _search(query: aggregate_filters_and_query(musts: musts, query: nil), sort: sort_field_query(:quarterly_score), limit: limit, offset: offset)
  end

  def find_trending_videos(channel_ids, status, limit, offset)
    musts = filter_channel_status_musts(channel_ids, status)
    _search(query: aggregate_filters_and_query(musts: musts, query: nil), sort: sort_field_query(:weekly_score), limit: limit, offset: offset)
  end

  def newest_streams(channel_ids, status, limit, offset)
    musts = filter_channel_status_musts(channel_ids, status)
    _search(query: aggregate_filters_and_query(musts: musts, query: nil), sort: sort_field_query(:created_at), limit: limit, offset: offset)
  end

  def search_class
    VideoStream
  end

  def response_class
    Search::VideoStreamSearchResponse
  end

  def response_fields
    super.merge(
        query: @q,
    )
  end

end

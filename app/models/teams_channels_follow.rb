# == Schema Information
#
# Table name: teams_channels_follows
#
#  id         :integer          not null, primary key
#  team_id    :integer
#  channel_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class TeamsChannelsFollow < ActiveRecord::Base
  include EventTracker

  belongs_to :team
  belongs_to :channel

  after_commit :reindex_team
  after_commit :reindex_channel
  after_commit :invalidate_channel_followers_count

  validates :team_id, :channel_id, presence: true
  validates_uniqueness_of :team_id, scope: :channel_id

  configure_metric_recorders(
    default_actor: lambda {|record| record&.team&.admins&.first },
    trackers: {
      on_create: {
        job_args: [ { group: :team, channel: :channel, org: :organization } ],
        event: 'group_channel_followed'
      },
      on_delete: {
        job_args: [ { group: :team, channel: :channel, org: :organization } ],
        event: 'group_channel_unfollowed',
      }
    }
  )

  def organization
    team.try(:organization) || channel.try(:organization)
  end

  # Since we don't index team_id in channel's index, only indexing team for now
  def reindex_team
    team.reindex_async_urgent
  end

  def reindex_channel
    channel.reindex_async_urgent
  end

  def invalidate_channel_followers_count
    Channel.invalidate_followers_cache(channel_id)
  end
end

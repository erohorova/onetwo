# == Schema Information
#
# Table name: users_external_courses
#
#  id                 :integer          not null, primary key
#  user_id            :integer
#  external_course_id :integer
#  status             :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  raw_info           :text(65535)
#  due_date           :date
#  assigned_date      :date
#

class UsersExternalCourse < ActiveRecord::Base
  belongs_to :user
  belongs_to :external_course

  validates :user_id, :external_course_id, :status, presence: true
  validates :user_id, uniqueness: {scope: :external_course_id}

  serialize :raw_info, JSON
end

# == Schema Information
#
# Table name: bookmarks
#
#  id                :integer          not null, primary key
#  user_id           :integer
#  bookmarkable_id   :integer
#  bookmarkable_type :string(191)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Bookmark < ActiveRecord::Base
  include EventTracker

  belongs_to :user
  belongs_to :bookmarkable, polymorphic: true, counter_cache: :bookmarks_count
  has_one :pin, as: :pinnable, dependent: :destroy

  after_create :add_to_learning_queue
  before_destroy :remove_from_learning_queue
  after_commit :record_card_bookmark_event, on: :create
  before_destroy :record_card_unbookmark_event

  validates :user, presence: true
  validates :bookmarkable, presence: true

  validates_uniqueness_of :bookmarkable_id, scope: [:user_id, :bookmarkable_type]

  scope :cards, ->{ where(bookmarkable_type: 'Card') }

  def add_to_learning_queue
    self.bookmarkable.add_to_learning_queue(user_id: self.user_id, source: "bookmarks")
  end

  def remove_from_learning_queue
    if user.user_content_completions.where(completable: bookmarkable).blank?
      user.learning_queue_items.where(source: "bookmarks", state: "active", queueable: bookmarkable).destroy_all
    else
      # to tackle the situation when a card that is bookmarked -> completed -> unbookmarked
      # the expectation is that the unbookmarked maintains the state of a card which was just marked as complete
      completed_bookmarked = user.learning_queue_items
        .find_by(source: "bookmarks", state: "completed", queueable: bookmarkable)
      if completed_bookmarked.present?
        completed_bookmarked.update({source: "assignments"})
      end
    end
  end

  private

  def record_card_bookmark_event
    return unless bookmarkable.is_a?(Card)
    record_bookmark_event('card_bookmarked')
  end

  def record_card_unbookmark_event
    return unless bookmarkable.is_a?(Card)
    record_bookmark_event('card_unbookmarked')
  end

  def record_bookmark_event(event)
    record_custom_event(
      event: event,
      actor: :user,
      job_args: [
        { card: :bookmarkable},
        {bookmark_id: :id}
      ]
    )
  end
end

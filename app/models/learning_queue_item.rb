# == Schema Information
#
# Table name: learning_queue_items
#
#  id             :integer          not null, primary key
#  display_type   :string(255)
#  state          :string(191)
#  queueable_type :string(191)
#  queueable_id   :integer
#  user_id        :integer          not null
#  created_at     :datetime
#  updated_at     :datetime
#  source         :string(255)
#  deep_link_type :string(255)
#  deep_link_id   :integer
#

class LearningQueueItem < ActiveRecord::Base

  COMPLETION_STATE = "completed"
  ACTIVE_STATE = "active"
  DELETION_STATE = "deleted"
  DISMISSED_STATE = "dismissed"

  SOURCES = ["bookmarks", "assignments"].freeze
  CONTENT_TYPES = ['card', 'collection', 'video_stream'].freeze
  STATES = [COMPLETION_STATE, ACTIVE_STATE].freeze

  belongs_to :user
  belongs_to :queueable, polymorphic: true

  # All callbacks of learning queue item handled in BulkAssignService, please change corresponding
  # methods in BulkAssignService if you are adding or changing callbacks
  before_validation :set_display_type, on: :create
  before_validation :set_state, on: :create
  before_validation :set_deep_link_id_and_type, on: :create

  validate :in_same_org
  validates :state, inclusion: {in: [ACTIVE_STATE, COMPLETION_STATE, DELETION_STATE, DISMISSED_STATE]}
  validates :source, inclusion: {in: SOURCES}

  scope :active, ->{where(state: ACTIVE_STATE)}
  scope :visible_learning_items, -> {
    joins("INNER JOIN cards ON queueable_id = cards.id").where("cards.hidden = ? AND queueable_type = ?", false, 'Card')
  }

  def set_display_type
    self.display_type = (case queueable_type
      when 'Card'
        self.queueable.card_type == "media" ? self.queueable.card_subtype : self.queueable.card_type
      when 'VideoStream'
        'video_stream'
      when 'Group'
        'group'
      end) if self.display_type.nil?
  end

  def set_state
    self.state = ACTIVE_STATE if self.state.nil?
  end

  def in_same_org
    self.errors.add(:base, "queueable and user not in the same org") unless self.queueable.organization_id == self.user.organization_id
  end

  def complete
    update_attributes(state: COMPLETION_STATE)
  end

  def uncomplete
    update_attributes(state: ACTIVE_STATE)
  end

  def mark_as_deleted
    update_attributes(state: DELETION_STATE)
  end

  def dismiss
    update_attributes(state: DISMISSED_STATE)
  end

  def get_app_deep_link_id_and_type
    queueable.try(:get_app_deep_link_id_and_type) || [nil, nil]
  end

  def snippet
    queueable.try(:snippet)
  end

  def set_deep_link_id_and_type
    id, type = get_app_deep_link_id_and_type
    self.deep_link_id = id
    self.deep_link_type = type
  end

  class << self
    def load_queueables(items)
      cards = Search::CardSearch.new.search_by_id(items.select{|i| i.queueable_type == "Card"}.map(&:queueable_id)).index_by(&:id)
      video_streams = Search::VideoStreamSearch.new.search_by_id(items.select{|i| i.queueable_type == "Card" && i.deep_link_type == "video_stream"}.map(&:deep_link_id)).index_by(&:id)
      Hash[items.map{|item| [item.id, (item.deep_link_type == "video_stream" ? video_streams[item.deep_link_id] : cards[item.queueable_id])]}]
    end
  end
end

# == Schema Information
#
# Table name: cards_ratings
#
#  id         :integer          not null, primary key
#  card_id    :integer
#  user_id    :integer
#  rating     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  level      :string(255)
#

class CardsRating < ActiveRecord::Base
  include EventTracker

  belongs_to :card
  belongs_to :user
  validates :user, presence: true
  validates :card, presence: true, uniqueness: {scope: :user_id}
  ALLOWED_RATINGS = [1,2,3,4,5].freeze
  ALLOWED_LEVELS = ["beginner", "intermediate", "advanced"].freeze
  validates :rating, inclusion: { in: ALLOWED_RATINGS }, if: proc { |x| x.rating.present? }
  validates :level, inclusion: {in: ALLOWED_LEVELS }, if: proc { |x| x.level.present? }

  after_commit :update_card_metadata, if: :should_update?
  after_create :record_card_rated_event
  after_update :record_card_rated_event

  def record_card_rated_event
    return unless %w{level rating}.any? &changes.method(:has_key?)

    record_custom_event(
      event: 'card_relevance_rated',
      actor: :user,
      job_args: [{cards_rating: :itself}, {card: :card}]
    )
  end

  private

  def update_card_metadata
    card_metadata = card.card_metadatum || card.build_card_metadatum
    card_metadata.level = level if is_level_changed?
    card_metadata.average_rating = card.cards_ratings.average(:rating) if is_rating_changed?
    card_metadata.save
  end

  def should_update?
    is_rating_changed? || is_level_changed?
  end

  def is_rating_changed?
    previous_changes.keys.include?('rating')
  end

  def is_level_changed?
    (previous_changes.keys.include?('level')) && (user.is_admin_user? || user_id == card.author_id)
  end
end

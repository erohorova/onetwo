# == Schema Information
#
# Table name: recordings
#
#  id                           :integer          not null, primary key
#  bucket                       :string(255)
#  location                     :string(1024)
#  key                          :string(255)
#  checksum                     :string(255)
#  video_stream_id              :integer
#  sequence_number              :integer
#  created_at                   :datetime
#  updated_at                   :datetime
#  transcoding_status           :string(255)      default("pending")
#  hls_location                 :string(1024)
#  mp4_location                 :string(1024)
#  last_aws_transcoding_message :string(4096)
#  last_aws_transcoding_job_id  :string(255)
#  source                       :string(255)      default("client")
#  default                      :boolean          default(FALSE)
#

class Recording < ActiveRecord::Base
  belongs_to :video_stream
  has_many :thumbnails, dependent: :destroy

  validates :transcoding_status, inclusion: ['pending', 'submitted', 'transcoding', 'error', 'available']
  validates :sequence_number, uniqueness: { scope: [:video_stream_id, :source]}

  after_commit :submit_aws_transcoder_job, on: :create

  attr_accessor :skip_transcoding

  scope :default, ->{where(default: true)}

  def submit_aws_transcoder_job
    unless skip_transcoding || self.video_stream&.card&.super_card_id&.present?
      if Rails.env.development?
        self.update_columns(hls_location: 'dev-hls', mp4_location: 'dev-mp4', transcoding_status: 'available')
        self.video_stream.reindex
      else
        TranscoderJob.perform_later(recording_id: self.id)
      end
    end
  end

  def start_transcoding!
    # This is the ID of the Elastic Transcoder pipeline that was created when
    # setting up your AWS environment:
    # https://w.amazon.com/index.php/User:Ramsdenj/Samples/Environment_Setup/Create_Elastic_Transcoder_Pipeline#Create_the_Pipeline
    pipeline_id = Settings.transcoder.pipeline_id


    # This is the name of the input key that you would like to transcode.
    input_key = self.key

    # Region where you setup your AWS resources.
    region = Settings.transcoder_region
    if region.blank?
      raise 'AWS region is missing'
    end

    # Create the client for Elastic Transcoder.
    transcoder_client = AWS::ElasticTranscoder::Client.new(region: region)

    input = {key: input_key}

    hls_preset_ids = Settings.transcoder.hls_preset_id.split(',').map(&:strip)

    high_res_hls_output = {
        key: 'high_hls_output',
        thumbnail_pattern: 'thumbnails/{count}',
        segment_duration: '5',
        preset_id: hls_preset_ids[0]
    }

    medium_res_hls_output = hls_preset_ids[1].nil? ? nil : {
        key: 'medium_hls_output',
        segment_duration: '5',
        preset_id: hls_preset_ids[1]
    }

    low_res_hls_output = hls_preset_ids[2].nil? ? nil : {
        key: 'low_hls_output',
        segment_duration: '5',
        preset_id: hls_preset_ids[2]
    }

    mp4_output = {
        key: 'mp4_output.mp4',
        preset_id: Settings.transcoder.mp4_preset_id
    }

    # Create a job on the specified pipeline and return the job ID.
    job_params = {pipeline_id: pipeline_id,
                  input: input,
                  output_key_prefix: output_key_prefix,
                  outputs: [high_res_hls_output, mp4_output, medium_res_hls_output, low_res_hls_output].compact, #do not mess with the ordering for backward compatibility reason
                  user_metadata: {recording_id: self.id.to_s}}

    if hls_preset_ids.length > 1
      job_params[:playlists] = [{name: 'hls_playlist',
                                 format: 'HLSv3',
                                 output_keys: [high_res_hls_output[:key],
                                              medium_res_hls_output[:key],
                                              low_res_hls_output[:key]
                                 ]}]
    end

    response = transcoder_client.create_job(job_params)
    log.info("submitted to transcoder recording_id=#{self.id} response=#{response.data.to_json}")
    self.last_aws_transcoding_message = response.data.to_json
    self.last_aws_transcoding_job_id = response.data[:job][:id]

    self.transcoding_status = 'submitted'
    self.save!
  end

  def retranscode!
    log.info("Retranscoding recording_id=#{id} video_stream_id=#{video_stream_id}")
    begin

      #clean up the output directory
      _bucket = AWS::S3.new.buckets[self.bucket]
      _bucket.objects.with_prefix(output_key_prefix).delete_all

      sleep 10
      # give AWS S3 enough time to delete the old files before submitting the new transcoding request.
      # transcoding will fail if the old file is still laying around

      self.start_transcoding!

    rescue => e
      log.error e
      log.error "Problem with recording_id=#{id} video_stream_id=#{video_stream_id} error=#{e}"
    end

  end

  def mark_default!
    if (self.source == "client") || (self.source == "server" && self.video_stream.recordings.default.try(:first).try(:source) != 'client')
      Recording.where(video_stream_id:self.video_stream_id).where.not(id:self.id).update_all(default: false)
      self.default = true
      self.save!
    end
  end

  private

  def output_key_prefix
    "video-streams-output-v2/#{self.source}/#{self.video_stream_id}/#{self.video_stream.uuid}/#{self.id}/#{self.sequence_number}/"
  end
end

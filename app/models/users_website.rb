# == Schema Information
#
# Table name: users_websites
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  url        :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class UsersWebsite < ActiveRecord::Base
  belongs_to :user
  validates_uniqueness_of :url, scope: :user
end

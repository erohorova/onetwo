class CardTeamPermission < ActiveRecord::Base
  belongs_to :card
  belongs_to :team

  validates_presence_of :card, :team
  validates_uniqueness_of :card_id, scope: :team_id

  validate :should_belong_to_same_org

  def should_belong_to_same_org
    unless self.card.organization_id == self.team.organization_id
      errors.add(:base, "Card and Team Organizations don't match")
    end
  end
end

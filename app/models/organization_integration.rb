# == Schema Information
#
# Table name: organization_integrations
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  integration_id  :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class OrganizationIntegration < ActiveRecord::Base
  belongs_to :organization
  belongs_to :integration
end

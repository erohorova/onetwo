class Permissions
  PERMISSIONS = [
    {
      group_name: 'CMS',
      permissions: [
        {name: 'Show User Manager', slug: 'SHOW_USER_MANAGER'},
        {name: 'Show Content Manager', slug: 'SHOW_CONTENT_MANAGER'},
        {name: 'Show Course Manager', slug: 'SHOW_COURSE_MANAGER'},
        {name: 'Show Channel Manager', slug: 'SHOW_CHANNEL_MANAGER'},
        {name: 'CMS Access', slug: 'HAS_CMS_ACCESS'},
        {name: 'Create Role', slug: 'CREATE_ROLE'},
        {name: 'Assign Roles', slug: 'ASSIGN_ROLES'}
      ]
    },
    {
      group_name: 'admin',
      permissions: [
        { name: 'Admin only', slug: 'ADMIN_ONLY'},
        { name: 'Configure Custom Labels', slug: 'CUSTOM_LABELS'},
        { name: 'Report Content', slug: 'REPORT_CONTENT' },
        { name: 'Report Comment', slug: 'REPORT_COMMENT' }
      ]
    },
    {
        group_name: 'leaderboard',
        permissions: [
            { name: 'Get leaderboard information', slug: 'GET_LEADERBOARD_INFORMATION'},
            { name: 'User Opt out of Leaderboard', slug: 'USER_OPT_OUT_OF_LEADERBOARD'}
        ]
    },
    {
      group_name: 'manage_card',
      permissions: [
        { name: 'Manage Card', slug: 'MANAGE_CARD'},
        { name: 'View Card Analytics', slug: 'VIEW_CARD_ANALYTICS'},
        { name: 'Upload', slug: 'UPLOAD'},
        { name: 'Create Livestream', slug: 'CREATE_LIVESTREAM'},
        { name: 'Publish Links', slug: 'PUBLISH_LINKS'},
        { name: 'Change Author', slug: 'CHANGE_AUTHOR'},
        { name: 'Upload content cover images', slug: 'UPLOAD_CONTENT_COVER_IMAGES'},
        { name: 'Can set content as Private', slug: 'MARK_AS_PRIVATE'},
        { name: 'Create Text Card', slug: 'CREATE_TEXT_CARD'}
      ]
    },
    {
      group_name: 'scorm_content',
      permissions: [
        { name: 'Upload Scorm Content', slug: 'UPLOAD_SCORM_CONTENT'},
      ]
    },
    { group_name: 'card_actions',
      permissions: [
        {name: 'Create Comment', slug: 'CREATE_COMMENT'},
        {name: 'Like', slug: 'LIKE_CONTENT'},
        {name: 'Bookmark', slug: 'BOOKMARK_CONTENT'},
        {name: 'Mark as complete', slug: 'MARK_AS_COMPLETE'},
        {name: 'Create Leap', slug: 'CREATE_LEAP'},
        {name: 'Update Leap', slug: 'UPDATE_LEAP'},
        {name: 'Delete Leap', slug: 'DELETE_LEAP'}
      ]
    },
    { group_name: 'card_advanced',
      permissions: [
        {name: 'Assign', slug: 'ASSIGN_CONTENT'},
        {name: 'Dismiss', slug: 'DISMISS_CONTENT'},
        {name: 'Add to Pathway', slug: 'ADD_TO_PATHWAY'},
        {name: 'Share to Team', slug: 'SHARE'}
      ]
    },
    {
      group_name: 'curator',
      permissions: [
        { name: 'Curate Cards for Channel', slug: 'CURATE_CONTENT'},
        { name: 'Pin Cards for Channel', slug: 'PIN_CONTENT'}
      ]
    },
    {
      group_name: 'edgraph',
      permissions: [
        { name: 'Enable EdGraph', slug: 'ENABLE_EDGRAPH'}
      ]
    },
    {
      group_name: 'manage_channel',
        permissions: [
          { name: 'Create Channel', slug: 'CREATE_CHANNEL'}
        ]
    },
    {
      group_name: 'manage_group',
        permissions: [
          { name: 'Create Group', slug: 'CREATE_GROUP'}
        ]
    },
    {
      group_name: 'create_journey',
        permissions: [
          { name: 'Create Journey', slug: 'CREATE_JOURNEY'},
        ]
    },
    {
      group_name: 'sub_admin',
      permissions: [
        { name: 'manage_content', slug: 'MANAGE_CONTENT'},
        { name: 'manage_users', slug: 'MANAGE_USERS'},
        { name: 'manage_analytics', slug: 'MANAGE_ANALYTICS'}
      ]
    },
    {
      group_name: 'group_sub_admin',
      permissions: [
        { name: 'manage_group_content', slug: 'MANAGE_GROUP_CONTENT'},
        { name: 'manage_group_users', slug: 'MANAGE_GROUP_USERS'},
        { name: 'manage_group_analytics', slug: 'MANAGE_GROUP_ANALYTICS'},
      ]
    },
    {
      group_name: 'content_curation',
      permissions: [
        { name: 'bypass_curation', slug: 'BYPASS_CURATION'}
      ]
    },
    {
      group_name: 'external_user',
      permissions: [
        { name: 'developer', slug: 'DEVELOPER'}
      ]
    },
    {
      group_name: 'dynamic_selection',
      permissions: [
        { name: 'use_dynamic_selection', slug: 'USE_DYNAMIC_SELECTION'}
      ]
    },
    {
      group_name: 'workflow',
      permissions: [
        { name: 'Workflow', slug: 'WORKFLOW'}
      ]
    },
  ]

  ROLE_TO_PERMISSON_GROUPS_MAP = HashWithIndifferentAccess.new(
    {
      'admin': %w[admin manage_card card_actions card_advanced leaderboard
                  edgraph manage_channel create_journey sub_admin group_sub_admin
                  content_curation dynamic_selection scorm_content manage_group workflow],
      'curator': %w[curator create_journey content_curation],
      'collaborator': %w[manage_card create_journey content_curation],
      'member': %w[manage_card card_actions card_advanced leaderboard edgraph
                   dynamic_selection],
      'group_leader': %w[],
      'group_admin': %w[],
      'trusted_collaborator': %[content_curation]
    }
  ).freeze

  PERMISSIONS_INDEXED_BY_SLUG = {}

  # Populate permissions
  # this set constants like Permissions::MANAGE_CARD = 'MANAGE_CARD', so that this can be used later in authorize method ( see ApplicatinoController#authorize)
  PERMISSIONS.each do |permission_group|
    permission_group[:permissions].each do |permission|
      self.const_set(permission[:slug], permission[:slug])
      PERMISSIONS_INDEXED_BY_SLUG[permission[:slug]] = permission
    end
  end

  # Given a list of permission strings, only return the ones that are actually valid
  def self.filter_valid_permissions(permissions)
    permissions.select {|x| PERMISSIONS_INDEXED_BY_SLUG[x].present?}
  end

  def self.get_permissions_for_role(role)
    get_permissions_for_groups(ROLE_TO_PERMISSON_GROUPS_MAP[role]) unless ROLE_TO_PERMISSON_GROUPS_MAP[role].blank?
  end

  def self.get_permission_slugs_for_role(role)
    permissions = get_permissions_for_role(role)
    return [] if permissions.blank?
    return permissions.keys
  end

  def self.get_permissions_for_groups(group_names)
    permissions = {}
    PERMISSIONS.each do |permission_group|
      unless group_names.index(permission_group[:group_name]).nil?
        permission_group[:permissions].each do |permission|
          permissions[permission[:slug]] = permission
        end
      end
    end
    return permissions
  end

  def self.permission_name(slug)
    permission = PERMISSIONS_INDEXED_BY_SLUG[slug]

    if permission.present?
      permission[:name]
    else
      nil
    end
  end
end

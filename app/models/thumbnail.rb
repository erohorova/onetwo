# == Schema Information
#
# Table name: thumbnails
#
#  id              :integer          not null, primary key
#  recording_id    :integer
#  uri             :string(255)
#  sequence_number :integer
#  is_default      :boolean          default(FALSE)
#  created_at      :datetime
#  updated_at      :datetime
#

class Thumbnail < ActiveRecord::Base
  
  belongs_to :recording

  validates_presence_of :uri
  validates_presence_of :recording_id
  validates_presence_of :sequence_number
  
  after_commit :update_video_stream_if_default, on: :create

  scope :default_thumbnails, -> { where(is_default:true) }
  
  def make_default!
    transaction do
      Thumbnail.where(recording_id: self.recording_id).where.not(id: self.id).update_all(is_default: false) #does not trigger validation or callback
      self.update!(is_default: true)
      self.update_video_stream
    end
  end

  def update_video_stream_if_default
    self.update_video_stream if is_default
  end

  def update_video_stream
    if self.recording && self.recording.video_stream
      self.recording.video_stream.update_default(self)
    end
  end

end

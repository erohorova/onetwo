class Embargo < ActiveRecord::Base
  include EventTracker
  belongs_to :organization
  belongs_to :user

  validates :value, :category, :user_id, :organization_id, presence: true
  validates_uniqueness_of :value, scope: [:organization_id]

  SITES = %w[approved_sites unapproved_sites].freeze
  VALID_TYPES = %w[approved_sites unapproved_sites unapproved_words].freeze

  scope :approved_sites, -> { where(category: 'approved_sites') }
  scope :unapproved_sites, -> { where(category: 'unapproved_sites') }
  scope :unapproved_words, -> { where(category: 'unapproved_words') }

  configure_metric_recorders(
    default_actor: lambda { |record| },
    trackers: {
      on_create: {
        event: 'org_embargo_added',
        job_args: [ {embargo: :itself} ],
      },
      on_delete: {
        event: 'org_embargo_removed',
        job_args: [ {embargo: :itself} ],
      },
      on_edit: {
        event: 'org_embargo_edited',
        job_args: [ {embargo: :itself} ],
      }
    }
  )

  def self.get_org_embargos_values(organization)
    words = organization.embargos.unapproved_words.pluck(:value)
    bad_urls = organization.embargos.unapproved_sites.pluck(:value)
    good_urls = organization.embargos.approved_sites.pluck(:value)
    {unapproved_words: words, unapproved_sites: bad_urls, approved_sites: good_urls}
  end

end

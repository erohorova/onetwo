# == Schema Information
#
# Table name: user_onboardings
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  current_step :integer          default(1)
#  status       :string(191)
#  completed_at :datetime
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class UserOnboarding < ActiveRecord::Base
  include AASM
  include EventTracker

  STEPS = [1, 2, 3].freeze

  # Hash values matches to values from UserOnboarding::STEPS
  DEFAULT_ONBOARDING_STEPS  = { 'profile_setup' => 1 }.freeze
  OPTIONAL_ONBOARDING_STEPS = { 'add_interests' => 2, 'add_expertise' => 3 }.freeze

  belongs_to :user, touch: true

  validates :current_step, inclusion: { in: UserOnboarding::STEPS }

  validates_uniqueness_of :user_id

  delegate :organization, to: :user

  # WORKAROUND 1:
  #   Web development is 3 weeks ahead of mobile development.
  #   As per new implementation, `profile_setup` is a mandatory step irrespective of onboarding steps configured or not.
  #   This will break onboarding flow in mobiles.
  #   Hence, customised onboarding is flexible with query parameter (`custom_onboarding` = TRUE)
  attr_accessor :custom_onboarding

  aasm column: :status do
    state :new, initial: true
    state :started
    state :completed

    event :reset do
      before do
        self.current_step = user_onboarding_options.first
      end
      after do
        user.reset_onboarding!
      end
      transitions from: :started, to: :new
      transitions from: :completed, to: :new
    end

    event :start do
      before do
        self.current_step = user_onboarding_options.first
      end
      transitions from: :new, to: :started
    end

    event :complete do
      before do
        self.completed_at = Time.now.utc
        self.current_step = user_onboarding_options.last
      end

      after do
        self.user.onboard!
      end

      transitions from: :started, to: :completed
    end
  end

  # TEMPORARY:
  #   Pick onboarding steps as per parameters configured from controller.
  #   If custom_onboarding = true then pick user's custom onboarding steps.
  #   If custom_onboarding = false then pick STEPS.
  def user_onboarding_options
    if self.custom_onboarding
      options = self.user.onboarding_options.values
    else
      options = STEPS
    end

    options.sort
  end

  edited_events = -> (record) {
    case record.status
    when "started"   then "user_onboarding_started"
    when "completed"   then "user_onboarding_completed"
    end
  }

  configure_metric_recorders(
    default_actor: :user,
      trackers: {
        on_create: {
          event: 'user_onboarding_created',
          job_args: [ {user_onboarding: :itself} ],
        },
        on_delete: {
          event: 'user_onboarding_deleted',
          job_args: [ {user_onboarding: :itself} ],
        },
        on_edit: {
          event: edited_events,
          job_args: [ {user_onboarding: :itself} ],
        }
      }
  )
end

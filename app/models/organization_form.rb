# == Schema Information
#
# Table name: organization_forms
#
#  id                :integer          not null, primary key
#  first_name        :string(255)
#  last_name         :string(255)
#  email             :string(255)
#  organization_name :string(255)
#  host_name         :string(255)
#  organization_size :string(255)
#  interests         :string(1024)
#  phone_number      :string(255)
#  created_at        :datetime
#  updated_at        :datetime
#

# DEPRECATED
class OrganizationForm < ActiveRecord::Base
  # job_role_id: job role of org admin
  # job_roles: job roles supported by an org

  validates_presence_of :first_name
  validates_presence_of :last_name
  validates_presence_of :organization_name
  validates_presence_of :email
  validates_presence_of :host_name
  validates_presence_of :interests
  
end

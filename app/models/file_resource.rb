# == Schema Information
#
# Table name: file_resources
#
#  id                      :integer          not null, primary key
#  created_at              :datetime
#  updated_at              :datetime
#  attachment_file_name    :string(255)
#  attachment_content_type :string(255)
#  attachment_file_size    :integer
#  attachment_updated_at   :datetime
#  attachable_id           :integer
#  attachable_type         :string(255)
#  url                     :text(16777215)
#  user_id                 :integer
#  filestack               :text(65535)
#  filestack_handle        :string(255)
#

class FileResource < ActiveRecord::Base

  FILE_CONTENT_TYPE_MAPPING = {
    pdf: ["application/pdf"],
    xls: ["application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          "application/excel", "application/x-iwork-keynote-sffnumbers"],
    doc: ["application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
          "application/vnd.ms-office"],
    ppt: ["application/vnd.openxmlformats-officedocument.presentationml.presentation", "application/vnd.ms-powerpoint"],
    csv: ["application/csv", "text/comma-separated-values", "text/csv"],
    text: ["text/plain","application/vnd.oasis.opendocument.text"],
    json: ["application/json"],
    rtf: ["application/rtf"],
    zip: ["application/zip", "application/x-zip-compressed"],
    html: ["text/html"]
  }

  belongs_to :attachable, polymorphic: true

  # required for access control, as we now have a ping end point for file resources
  belongs_to :user

  has_attached_file :attachment, styles: lambda { |a|
                                                    if a.instance.image?
                                                      {small: '320',
                                                       medium: '750',
                                                       large: '1080'}
                                                    else
                                                      {}
                                                    end
                                                }

  process_in_background :attachment

  #We have bunch of different type of file types in there. We could also add explicit validation for all of them.
  #TODO : To be reviewed and discussed.
  do_not_validate_attachment_file_type :attachment

  validate :validate_file_mime_type
  after_create :set_url
  # before_destroy :remove_file_from_filestack, if: "filestack_handle.present?"
  before_save :set_file_resource_author

  serialize :filestack, JSON

  scope :images, -> {where(%q{attachment_content_type LIKE 'image/%'})}

  attr_accessor :filestack_filename, :file_ext

  def valid_mime_type?
    image? || video? || audio?  || zip? || attachment_of_type_file? || attachment_of_type_document?
  end

  def set_url
    if self.attachment.present?
      self.update_attributes(url: attachment.url)
    end
  end

  def remove_file_from_filestack
    unless CARD_STOCK_IMAGE.any? {|h| h[:handle] == filestack_handle }
      card_id = attachable_type == 'Card' ? attachable.id : nil
      RemoveFileFromFilestackJob.perform_later(handle: filestack_handle, card_id: card_id)
    end
  end

  def file_extension
    filestack_filename.present? ? File.extname(filestack_filename) : ''
  end

  def image?
    !(/^image\/(png|svg\+xml|webp|jpeg|jpg|pjpeg|x-png|gif|tiff)$/.match(attachment_content_type).nil?)
  end

  def video?
    (/^video\/(mp4|ogg|webm|quicktime|ogv)$/).match(attachment_content_type).present?
  end

  def audio?
    (/^audio\/(mp3|mp4|mpeg|oga|ogg|m4a|aac|x-m4a|hls.variant.audio)$/).match(attachment_content_type).present?
  end

  def zip?
    FILE_CONTENT_TYPE_MAPPING[:zip].include?(attachment_content_type) || file_extension == '.zip'
  end

  def pdf?
    FILE_CONTENT_TYPE_MAPPING[:pdf].include?(attachment_content_type)
  end

  def ppt?
    FILE_CONTENT_TYPE_MAPPING[:ppt].include?(attachment_content_type)
  end

  def doc?
    FILE_CONTENT_TYPE_MAPPING[:doc].include?(attachment_content_type)
  end

  def xls?
    file_ext = attachment_file_name&.split('.')&.last
    FILE_CONTENT_TYPE_MAPPING[:xls].include?(attachment_content_type) || file_ext == 'xls' || file_ext == 'xlsx'
  end

  def csv?
    file_ext = attachment_file_name&.split('.')&.last
    FILE_CONTENT_TYPE_MAPPING[:csv].include?(attachment_content_type) || file_ext == 'csv'
  end

  def text?
    FILE_CONTENT_TYPE_MAPPING[:text].include?(attachment_content_type)
  end

  def json?
    FILE_CONTENT_TYPE_MAPPING[:json].include?(attachment_content_type)
  end

  def html?
    FILE_CONTENT_TYPE_MAPPING[:html].include?(attachment_content_type)
  end

  def rtf?
    FILE_CONTENT_TYPE_MAPPING[:rtf].include?(attachment_content_type)
  end

  def as_json
    {
      'id' => id,
      'file_url' => file_url,
      'file_type' => file_type,
      'file_name' => file_name,
      'available'  => available?
    }
  end

  def file_url(size = nil)
    if size && available?
      attachment.url(size)
    elsif available?
      attachment.url
    else
      nil
    end
  end

  def file_name
    available? ? attachment_file_name : nil
  end

  def file_type
    if available?
      if image?
        'image'
      elsif video?
        'video'
      elsif audio?
        'audio'
      elsif zip?
        'zip'
      elsif attachment_of_type_file? || attachment_of_type_document?
        'file'
      end
    end
  end

  def available?
    attachment.present?
  end

  after_commit :reindex_post

  def reindex_post
    if attachable_type == 'Post'
      IndexJob.perform_later('Post', attachable_id)
    end
  end

  def fetch_attachment_urls
    urls = {}
    attachment.styles.keys.each do |style|
      urls[style.to_s + "_url"] = attachment(style)
    end
    urls
  end

  def file_absolute_url
     begin
      uri = URI.parse(self.url)
      self.url.match(/http/) || uri.scheme = Settings.platform_force_ssl ? 'https' : 'http'
      return uri.to_s
    rescue => e
      log.warn "og image extraction failed for url: #{self.url}. Exception: #{e.inspect}"
      return nil
    end
  end

  def organization
    attachable.try(:organization)
  end

  def attachment_of_type_file?
    json? || html? || rtf?
  end

  def attachment_of_type_document?
    ppt? || pdf? || xls? || doc? || csv? || text?
  end

  private
  def set_file_resource_author
    self.user = attachable.try(:user) || attachable.try(:author) unless self.user
  end

  def validate_file_mime_type
    errors.add(:attachment_content_type, 'MIME type is not supported') unless valid_mime_type?
  end
end

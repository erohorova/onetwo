class CareerAdvisorCard < ActiveRecord::Base
  include EventTracker
  belongs_to :card
  belongs_to :career_advisor

  validates :card_id, :career_advisor_id, :level, presence: true
  validates :card_id, uniqueness: { scope: :career_advisor_id }
  validate :validate_card_id

  def card_label
    card.title.blank? ? card.message : card.title
  end

  private

  def validate_card_id
    errors.add(:card_id, 'is not a course') unless Card.courses.exists?(self.card_id)
  end

  configure_metric_recorders(
     default_actor: lambda { |record| },
     trackers: {
       on_create: {
         event: 'card_added_to_career_advisor',
         job_args: [ {career_advisor_card: :itself}, {card: :card} ]
       },
       on_delete: {
         event: 'card_removed_from_career_advisor',
         job_args: [ {career_advisor_card: :itself}, {card: :card} ]
       },
       on_edit: {
         event: 'card_edited_from_career_advisor',
         job_args: [ {career_advisor_card: :itself}, {card: :card} ]
       }
     }
   )
end

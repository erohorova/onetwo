class Profile < ActiveRecord::Base
  include WorkflowPatcher
  include WorkflowObserver

  belongs_to :user, inverse_of: :external_profile
  belongs_to :organization

  after_commit :detect_upsert, on: [:create, :update]
  after_commit :detect_delete, on: :destroy

  before_validation on: :create do
    self.uid = SecureRandom.uuid if uid.blank?
    self.external_id = uid if external_id.blank?
  end

  attr_accessor :bulk_import # used in process of users bulk import

  validates :organization_id, :user_id, :uid, :external_id, presence: true
  validates_uniqueness_of [:uid, :user_id], scope: [:organization_id]
  validates_uniqueness_of [:external_id, :id], scope: [:organization_id]
  validate :validate_user_id

  delegate(
    :first_name, :last_name, :status, :email,
    :assigned_custom_fields, :language, to: :user, allow_nil: true
  )

  private

  def validate_user_id
    unless self.organization.users.exists?(id: self.user_id)
      errors.add(:user_id, 'does not belong to the organization profile')
    end
  end
end

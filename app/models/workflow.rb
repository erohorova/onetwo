class Workflow < ActiveRecord::Base

  WF_TYPES = %w[User].freeze

  NEW = 'new'.freeze
  PROCESSING = 'processing'.freeze
  COMPLETED = 'completed'.freeze

  belongs_to :organization
  belongs_to :author, foreign_key: 'user_id', class_name: 'User'

  validates :organization_id, :name, :user_id, presence: true
  validate :necessary_workflows_details

  scope :enabled, (-> { where(is_enabled: true) })

  # wf_input_source
  # E.g.
  # [
  # for db as a source
  #   {
  #     type: 'DB', 
  #     source: User
  #   }
  # for csv as a source
  #   {
  #     type: 'CSV',
  #     source: 'link of csv'
  #   }
  # ]
  serialize :wf_input_source, Array

  # wf_actionable_columns
  # E.g.
  # [
  #   for db and csv both
  #   { 
  #     column_name: nil, => Represents column name of DB
  #     csv_name: 'DynamicColumnName',
  #     renamed: nil,
  #     type: 'string', 
  #     is_dynamic: true,
  #     formula: 'case statement of MySql', 
  #     is_identifier: false
  #   }
  # ]
  serialize :wf_actionable_columns, Array

  # wf_rules
  # E.g.
  # {
  #   'and':[
  #     {
  #       'equals': {'country: 'US'}
  #     },
  #     {
  #       'not_equals': {'location': 'SF'}
  #     },
  #     {
  #       'must_exists': 'location'
  #     }
  #   ]
  # }
  serialize :wf_rules, JSON

  # wf_action
  # E.g.
  # {
  #   entity: "Group",
  #   type: "create_dynamic" => [create_dynamic, create_custom, existing],
  #   value: {
  #       column_name: 'title',
  #       wf_column_names: ["location"],
  #       prefix: '',
  #       suffix: ''
  #     },
  #   metadata: {
  #     is_private: true/false,
  #     description: 'A group by workflow'
  # }
  serialize :wf_action, JSON

  # wf_output_result
  # E.g
  # {
  #   entity: 'Team'
  #   ids: [1,2,3,4,6] ids of created entity by using current Workflow
  # }
  serialize :wf_output_result, JSON

  def wf_column_names
    (wf_action && wf_action['value']) ? wf_action['value']['wf_column_names'] : []
  end

  def output_dynamic?
    wf_action['type'] == WorkflowService::Output::CREATE_DYNAMIC
  end

  def input_source
    wf_input_source[0][:source]
  end

  private

  def necessary_workflows_details
    if self.wf_type.blank?
      errors.add(:base, "Workflow type can't be blank")
    end

    if self.wf_input_source.blank?
      errors.add(:base, "Workflow input source can't be blank")
    end

    if self.wf_actionable_columns.blank?
      errors.add(:base, "Workflow actionable columns can't be blank")
    end

    if self.wf_action.blank?
      errors.add(:base, "Workflow action's output columns can't be blank")
    end
  end
end
# == Schema Information
#
# Table name: teams
#
#  id                  :integer          not null, primary key
#  name                :string(191)      not null
#  description         :text(65535)
#  slug                :string(191)
#  organization_id     :integer
#  image_file_name     :string(255)
#  image_content_type  :string(255)
#  image_file_size     :integer
#  image_updated_at    :datetime
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  is_everyone_team    :boolean          default(FALSE)
#  is_public           :boolean          default(FALSE)
#  comments_count      :integer          default(0)
#  auto_assign_content :boolean          default(FALSE)
#

# We are using this table to save raw data for score per user per card for team
# score is in minute
class Team < ActiveRecord::Base
  extend FriendlyId
  include EventTracker
  include EdcastSearchkickWrapper
  extend TeamModelLeaderboardV2Concern

  friendly_id :slug_candidates, use: [:slugged, :history, :scoped], scope: :organization

  serialize :source_ids, Array

  belongs_to :organization
  has_many :teams_users, foreign_key: 'team_id'

  # https://github.com/rails/rails/issues/27567
  has_many :users, through: :teams_users
  has_many :teams_admins, -> { admin }, class_name: 'TeamsUser', foreign_key: 'team_id'
  has_many :admins, class_name: 'User', through: :teams_admins, source: :user
  has_many :teams_members, -> { member }, class_name: 'TeamsUser', foreign_key: 'team_id'
  has_many :members, class_name: 'User', through: :teams_members, source: :user
  has_many :teams_sub_admins, -> { sub_admin }, class_name: 'TeamsUser', foreign_key: 'team_id'
  has_many :sub_admins, class_name: 'User', through: :teams_sub_admins, source: :user
  has_many :team_assignments, dependent: :destroy
  has_many :assignments, through: :team_assignments
  has_many :clcs, as: :entity, dependent: :destroy
  has_many :teams_cards, dependent: :destroy
  has_many :shared_teams_cards, -> { shared_cards }, class_name: 'TeamsCard', foreign_key: 'team_id'
  has_many :shared_cards, through: :shared_teams_cards, source: :card

  has_many :teams_channels_follows, dependent: :destroy
  has_many :following_channels, through: :teams_channels_follows, class_name: 'Channel', source: :channel
  has_many :comments, as: :commentable, dependent: :destroy

  has_many :pronouncements, as: :scope
  has_many :invitations, as: :invitable

  has_many :pins, as: :pinnable, dependent: :destroy
  has_many :card_team_permissions
  has_many :cards_permitted, through: :card_team_permissions, source: :card
  has_one  :dynamic_group_revision, class_name: 'DynamicGroupRevision', foreign_key: 'group_id'
  has_one :config, as: :configable

  validate :check_mandatory
  validates :organization, :description, presence: true
  validates :name, presence: true, uniqueness: {scope: :organization_id, case_sensitive: false}
  before_create :set_mandatory

  after_destroy :cleanup_team_users

  after_commit :reindex_async_urgent

  scope :accessible_source_ids, ->{where.not(source_ids: nil).pluck(:source_ids)}
  scope :excluding_everyone_team, ->{where.not(is_everyone_team: true)}
  scope :everyone_teams, ->{where(is_everyone_team: true)}
  scope :search_by_name, -> (q) { where("name like :q", q: "%#{q}%") }
  scope :open, ->{ where(is_private: false) }
  scope :assigned_groups_for_card, -> (card_id) { joins(:assignments).where('assignments.assignable_id = ?', card_id).uniq }

  delegate :external_id, to: :dynamic_group_revision, allow_nil: true

  has_attached_file :image, default_url: 'default_banner_image_min.jpg', :styles => { small: '120x120', medium: '320x320' }
  validates_attachment_content_type :image, :content_type => /^image\/(png|jpeg|jpg|pjpeg|x-png|gif|tiff)$/,
                                    :message => 'Invalid file type'
  validates_attachment_size :image, :less_than => 5.megabytes, :message => 'Max file size is 5MB'

  METRICS = [:smartbites_consumed, :smartbites_created, :average_session, :total_users, :new_users, :active_users, :engagement_index]

  configure_metric_recorders(
    default_actor: lambda {|record| record.organization&.admins&.first},
    trackers: {
      on_create: {
        job_args: [{group: :itself}],
        event: 'group_created'
      },
      on_edit: {
        job_args: [{group: :itself}],
        event: 'group_edited'
      },
      on_delete: {
        job_args: [{group: :itself}],
        event: 'group_deleted'
      }
    }
  )

  def push_view_event(viewer)
    record_custom_event(
      event: 'group_visited',
      job_args: [{group: :itself}],
      actor: lambda { |record| viewer }
    )
  end
  attr_accessor :skip_indexing

  def cleanup_team_users
    team_poro = Analytics::MetricsRecorder.team_attributes self
    TeamUsersCleanupJob.perform_later team: team_poro
  end

  def slug_candidates
    candidates = [name]

    if description
      msg = ActionView::Base.full_sanitizer.sanitize description
      msg_words = msg[0..50].split.take(10)
      msg_words.length.times do |i|
        candidates << [name, *msg_words.take(i+1)]
      end
    end

    candidates << [name, SecureRandom.uuid]

    return candidates
  end

  def is_deletable?
    !is_everyone_team
  end

  def should_generate_new_friendly_id?
    super || name_changed?
  end

  def is_user?(user)
    teams_users.where(user: user).exists?
  end

  def is_role?(user, role:)
    teams_users.where(user: user, as_type: role).exists?
  end

  def is_admin?(user)
    is_role?(user, role: 'admin')
  end

  def is_member?(user)
    is_role?(user, role: 'member')
  end

  def is_sub_admin?(user)
    is_role?(user, role: 'sub_admin')
  end

  def is_pending?(user)
    invitations.pending.exists?(recipient_id: user.id)
  end

  def add_to_team(user_ids, as_type: 'member', actor_name: nil, skip_notify: false)
    users = organization
      .users
      .not_suspended
      .not_anonymized
      .where(id: user_ids)

    if users.present?
      users.each do |user|
        begin
          send("add_#{as_type}".to_sym, user, {actor_name: actor_name})
          notify_added_user(to_id: user.id, role: as_type) unless skip_notify
        rescue ActiveRecord::RecordInvalid
          next
        end
      end
    end
  end

  # Added 'skip_flush_cache' to flush the channels followers count cache this will be used in while bulk
  # import of users.
  # Added this to avoid this issue EP-21404.
  def add_user(user, as_type: 'member', skip_flush_cache: false, skip_team_indexing: false)
    return unless organization_id == user.organization_id

    tu = teams_users.where(user: user)

    return tu.new(as_type: as_type, skip_flush_cache: skip_flush_cache, skip_team_indexing: skip_team_indexing).save if tu.count.zero?

    tu = case as_type
      when 'member'
        tu.destroy_all unless tu.exists?(as_type: 'member')
        tu.new(as_type: as_type, skip_flush_cache: skip_flush_cache, skip_team_indexing: skip_team_indexing) unless tu.exists?(as_type: 'member')
      when 'admin'
        tu.first.update(as_type: as_type, skip_flush_cache: skip_flush_cache, skip_team_indexing: skip_team_indexing) if tu.exists?(as_type: 'member')
        tu.new(as_type: as_type, skip_flush_cache: skip_flush_cache, skip_team_indexing: skip_team_indexing) if tu.exists?(as_type: 'sub_admin')
      when 'sub_admin'
        tu.first.update(as_type: as_type, skip_flush_cache: skip_flush_cache, skip_team_indexing: skip_team_indexing) if tu.exists?(as_type: 'member')
        tu.new(as_type: as_type, skip_flush_cache: skip_flush_cache, skip_team_indexing: skip_team_indexing) if tu.exists?(as_type: 'admin')
      end

    tu.save if tu&.new_record?
  end

  def add_member(user, actor_name: nil, skip_flush_cache: false)
    add_user(user, as_type: 'member', skip_flush_cache: skip_flush_cache)
  end

  def add_admin(user, actor_name: nil)
    is_member_or_sub_admin = TeamsUser.where(team_id: self.id, as_type: ['member', 'sub_admin'], user_id: user.id).any?

    if is_member_or_sub_admin && !actor_name.nil?
      EDCAST_NOTIFY.trigger_event(Notify::EVENT_GROUP_USER_PROMOTED_TO_ADMIN, self, { user_id: user.id, actor_name: actor_name })
    end

    add_user(user, as_type: 'admin')
    if user.roles.pluck(:default_name).exclude?('group_leader')
      user.add_role('group_leader')
    end
  end

  def add_sub_admin(user, actor_name: nil)
    is_member = TeamsUser.where(team_id: self.id, as_type: 'member', user_id: user.id).any?

    if is_member && !actor_name.nil?
      EDCAST_NOTIFY.trigger_event(Notify::EVENT_GROUP_USER_PROMOTED_TO_SUB_ADMIN, self, { user_id: user.id, actor_name: actor_name })
    end

    add_user(user, as_type: 'sub_admin')
    if user.roles.pluck(:default_name).exclude?('group_admin')
      user.add_role('group_admin')
    end
  end

  def remove_users_type(user_ids, as_type)
    unless user_ids.blank? && as_type.blank?
      type_count = teams_users.where(user_id: user_ids).group(:user_id).count
      type_count = type_count.reject { |user_id, counter| counter.zero? }
      type_count.each do |user_id, counter|
        tu = teams_users.find_by(user_id: user_id, as_type: as_type)
        counter >= 2 ? tu.destroy : tu.update(as_type: 'member')
      end
    end
  end

  def assignment_cards
    # TODO
    # will use teams_cards once the assignment tech debt is tackled

    #Note:-
    #1. explicitly mentioned the index_team_assignments_on_team_id, bcz sql was taking index_team_assignments_on_assignment_id but our conditions are on team.
    #2. used left join as it was giving faster result than inner join, difference was 300 - 400 ms

    Card.joins("LEFT JOIN
                  (assignments INNER JOIN team_assignments use index (index_team_assignments_on_team_id) ON team_assignments.assignment_id = assignments.id and team_assignments.team_id = #{id})
              ON assignments.assignable_id = cards.id AND assignments.assignable_type = 'Card' and cards.organization_id = #{organization_id}  AND cards.state = 'published'")
      .where("cards.deleted_at IS NULL AND (team_assignments.team_id = #{id}) ")
      .group("assignments.assignable_id ")
      .order("team_assignments.id DESC").uniq
  end

  def notify_added_user to_id:, role:
    NotificationGeneratorJob.perform_later(item_id: id, item_type: 'added_to_team', user_id: to_id, notification_type: "add_#{role}_to_team")
  end

  def top_contributors(limit, offset)
    query = " select sum(total_count) as count, user_id from (select count(card_id) as total_count, user_id from teams_cards where team_id = #{id} and type = 'SharedCard'"+
      "group by user_id union all select count(distinct(a.assignable_id)) as total_count, ta.assignor_id from assignments a join team_assignments ta on a.id = ta.assignment_id " +
      "where team_id = #{id} group by assignor_id) as integrated_table group by user_id order by count DESC limit #{limit} offset #{offset}"

    results = []
    ActiveRecord::Base.connection.execute(query).each do |result|
      # example result [score, user_id]
      results << {user: User.select(:id, :email, :handle, :first_name, :last_name, :picture_url).find_by(id: result[1]),
      score: result[0]}
    end
    results
  end

  def get_new_users_count_over_span(begin_time: , end_time: )
    users.not_suspended.completed_profile
      .where("users.created_at >= (:begin_time) AND users.created_at < (:end_time)", begin_time: begin_time, end_time: end_time).count
  end

  def real_users_count_before(time)
    users.not_suspended.completed_profile
      .where("users.created_at <= (:time)", time: time).count
  end

  # timeranges: -> PeriodOffsetCalculator#drill_down_timeranges_for_timerange
  # [[Time.now, Time.now + 1], [Time.now + 1, Time.now + 2]]
  def get_new_users_drill_down_data_for_timeranges(timeranges:)
    all_new_users = users.not_suspended.completed_profile.where("users.created_at >= (:begin_time_period) AND users.created_at < (:end_time_period)", begin_time_period: timeranges.first.first, end_time_period: timeranges.last.last).select(:id, :created_at)
    timeranges.map{|tr| all_new_users.select{|user| user.created_at >= tr.first && user.created_at < tr.last}.length}
  end

  def metrics_over_span(organization: , team_id: , period: , offsets: , timeranges: )
    # returns metrics for a team with offset range
      # output format:
      # {smartbites_created: 0, smartbites_consumed: 0}
    data = TeamLevelMetric.get_team_metrics_over_span(
      team_id: id,
      period: period,
      begin_offset: offsets.first,
      end_offset: offsets.last)

    # returns number if new users created over a timerange
    new_users_count = get_new_users_count_over_span(
      begin_time: timeranges.first.first,
      end_time: timeranges.last.last)

    # returns number of total users before particular time
    total_users_count = real_users_count_before(timeranges.last.last)

    # to fetch active users, average session at the org level, since team_level_metric does not have this data
    users_data = UserLevelMetric.get_organization_metrics_over_span(
      organization_id: organization_id,
      team_id: id,
      period: period,
      begin_offset: offsets.first,
      end_offset: offsets.last)

    data.merge({
      total_users: total_users_count, new_users: new_users_count,
      average_session: users_data[:average_session], active_users: users_data[:active_users],
      engagement_index: OrganizationMetricsGatherer.calculate_engagement_index(users_data[:active_users], total_users_count)})
  end

  def drill_down_data_for_custom_period(organization: , team_id: , timerange: ,period: :day)
    data = TeamLevelMetric.get_team_drill_down_data_for_timerange(organization_id: organization.id, team_id: team_id, period: period, timerange: timerange)

    drill_down_timeranges = PeriodOffsetCalculator.drill_down_timeranges_for_timerange(period: period, timerange: timerange)

    drill_down_total_users_count = drill_down_timeranges.map{|tr| real_users_count_before(tr.last)}
    data[:total_users] = drill_down_total_users_count
    data[:engagement_index] = drill_down_timeranges.map
      .with_index{|tr, index| OrganizationMetricsGatherer.calculate_engagement_index(data[:active_users][index], drill_down_total_users_count[index])}

    data[:new_users] = get_new_users_drill_down_data_for_timeranges(timeranges: drill_down_timeranges)
    data
  end


  # ops: :remove, :switch_role
  # returns [success_boolean, 'error symbol; nil if no error or unknown error']
  def modify_user(user_or_id, op:)
    key = User === user_or_id ? :user : :user_id
    tu = teams_users.find_by(key => user_or_id)

    return false, :not_in_team if tu.nil?
    # there must be one other admin to remove an admin
    return false, :sole_admin if tu.as_type == 'admin' && admins.count == 1

    case op
      when :remove
        return tu.destroy.present?, nil
      when :switch_role
        return tu.update(as_type: (%w[admin member] - [tu.as_type]).first).present?, nil
    end
  end

  def photo
    if image.present?
      image.url
    else
      ActionController::Base.helpers.asset_path('default_banner_image_min.jpg')
    end
  end

  def distinct_cards
    shared_cards.pluck(:id) | assignment_cards.pluck(:id)
  end

  def top_content
    initial_offset = PeriodOffsetCalculator.day_offset(PeriodOffsetCalculator::BEGINNING_OF_TIME)
    final_offset = PeriodOffsetCalculator.day_offset(Time.now)

    Card.joins("inner join content_level_metrics on cards.id = content_level_metrics.content_id").
      select("sum(content_level_metrics.views_count) as totalScore, cards.title, cards.message, cards.id").
      where("content_level_metrics.content_id in (?) and period = 'day' and offset >= ? AND offset <= ?", distinct_cards, initial_offset, final_offset).
      group("content_level_metrics.content_id, content_level_metrics.content_type").
      order("totalScore desc")
  end

  # returns hash of form {admin: 3, member: 19}
  def membership_counts
    {
        'admin' => 0,
        'member' => 0,
    }.merge(teams_users.group(:as_type).count).with_indifferent_access
  end

  def create_team_assignments_for(assignee_ids: , assignor_id: , assignable_type: , assignable_id:, opts: {})
    BulkAssignService.new(team: self, assignor_id: assignor_id, assignable_type: assignable_type,
      assignable_id: assignable_id, opts: opts).run(assignee_ids: assignee_ids, batch_size: 200)
  end

  def mass_assignment(assignor: , assignable: , assignee_ids: [], opts: {self_assign: false})
    TeamAssignmentJob.perform_later(team_id: self.id, assignor_id: assignor.id, assignee_ids: assignee_ids, assignable_id: assignable.id, assignable_type: assignable.class.name, opts: opts)
  end

  def fetch_assignor_for(assignment)
    team_assignments.where(assignment: assignment).first.try(:assignor)
  end

  def fetch_unassigned_members_for(assignable:)
    users.where.not(id: assignments.select("assignments.user_id").where(assignable: assignable))
  end

  def active_users
    users.includes(:user_onboarding).where(user_onboardings: { status: 'completed'})
  end

  # below methods will be used for filtering activity_streams
  def can_comment?(user)
    true
  end

  def visible_to_user(user)
    is_user?(user)
  end

  def snippet
    name
  end

  def get_app_deep_link_id_and_type_v2
    [id, 'team']
  end

  def viewable_for_team_members(user)
    user_team_ids = user.team_ids.any? ? user.team_ids.join(",") : 'NULL'
    Card.joins("inner join teams_cards on cards.id = teams_cards.card_id and teams_cards.type = 'SharedCard' " +
      "left join `card_user_permissions` on card_user_permissions.card_id = teams_cards.card_id " +
      "left join card_team_permissions on card_team_permissions.card_id = teams_cards.card_id ")
    .where("(teams_cards.team_id = #{id}) && " +
      "(cards.is_public || " +
      "((card_user_permissions.id is null || card_user_permissions.user_id = #{user.id}) && " +
      "(card_team_permissions.id is null || card_team_permissions.team_id in (#{user_team_ids}))))").uniq
  end

  def viewable_shared_cards(user)
    if organization.is_admin?(user) || is_admin?(self)
      shared_cards
    else
      viewable_for_team_members(user)
    end
  end

  def search_data
    {
      name: name,
      description: description,
      organization_id: organization_id,
      auto_assign_content: auto_assign_content,
      source_ids: source_ids,
      is_private: is_private,
      users_ids: users.pluck(:id),
      pending_users_ids: pending_users_ids, #pending requests
      leaders: admins.map { |u| { id: u.id, name: u.name } },
      admins: sub_admins.map { |u| {id: u.id, name: u.name } },
      members: members.map { |u| {id: u.id, name: u.name } },
      channel_ids: following_channels.pluck(:id),
      domains: domains&.split(",")&.map {|domain| domain.downcase.strip }
    }
  end

  def self.searchable_fields
    {
      'name' => {},
      'description' => {}
    }
  end

  def self.sortable_fields
    ['name']
  end

  def pending_users_ids
    invitations.pending.pluck(:recipient_id)
  end

  edcast_searchkick merge_mappings: true, mappings: {
	                    "team": {
	                              "properties": {
	                                              "domains": {
	                                                           "type": "string",
	                                                          "index": "not_analyzed",
	                                                          "store": false
	                                                         }
	                                            }
	                            }
                    }

  def reindex_async
    return if skip_indexing
    IndexJob.perform_later(self.class.name, self.id)
  end

  def reindex_async_urgent
    return if skip_indexing
    IndexJob.set(queue: :reindex_urgent).perform_later(self.class.name, id)
  end

  def set_carousels_config(carousels)
    carousels_config = Config.find_or_initialize_by(configable: self, name: 'team_carousels', category: 'settings', data_type: 'json')
    carousels_config.update_attributes(value: carousels)
    carousels_config
  end

  #provide default carousels for team if custom_carousels for team is not present in config
  def get_default_carousels
    return [{'default_label':'Featured', 'index':0, 'visible':true},
            {'default_label':'Assigned', 'index':1, 'visible':true},
            {'default_label':'Shared', 'index':2, 'visible':true},
            {'default_label':'Channels', 'index':3, 'visible':true}]
  end

  private

  def set_mandatory
    self.is_mandatory = true if is_dynamic
  end

  def check_mandatory
    if is_mandatory && !is_private
      errors.add(:base, 'Only private and dynamic team can be mandatory') unless is_dynamic
    end
  end


end

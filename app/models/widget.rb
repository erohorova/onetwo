class Widget < ActiveRecord::Base
  CONTEXT_TYPES = ['channel']

  # Associations
  belongs_to :organization
  belongs_to :creator, class_name: 'User'
  belongs_to :parent, polymorphic: true

  # Validations
  validates :creator, :organization, :parent, :code, :context, presence: true
  validates_inclusion_of :context, in: CONTEXT_TYPES

  # Callbacks
  before_validation :downcase_context

  # Scopes

  def downcase_context
    self.context = self.context&.downcase
  end
end

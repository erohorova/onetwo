# == Schema Information
#
# Table name: groups
#
#  id                                 :integer          not null, primary key
#  name                               :string(255)      not null
#  description                        :text(16777215)
#  website_url                        :string(255)
#  created_at                         :datetime
#  updated_at                         :datetime
#  client_id                          :integer
#  client_resource_id                 :string(191)
#  type                               :string(255)
#  access                             :string(255)
#  users_count                        :integer          default(0)
#  course_data_url                    :text(16777215)
#  image_url                          :text(16777215)
#  details_locked                     :boolean          default(FALSE)
#  creator_id                         :integer
#  parent_id                          :integer
#  close_access                       :boolean          default(FALSE)
#  topic_id                           :integer
#  picture_file_name                  :string(255)
#  picture_content_type               :string(255)
#  picture_file_size                  :integer
#  picture_updated_at                 :datetime
#  is_private                         :boolean          default(FALSE)
#  start_date                         :datetime
#  course_term                        :string(255)
#  end_date                           :datetime
#  connect_to_social_mediums          :boolean          default(TRUE)
#  enable_mobile_app_download_buttons :boolean          default(TRUE)
#  share_to_social_mediums            :boolean          default(TRUE)
#  daily_digest_enabled               :boolean          default(FALSE)
#  forum_notifications_enabled        :boolean          default(TRUE)
#  language                           :string(255)      default("en")
#  is_promoted                        :boolean          default(FALSE)
#  is_hidden                          :boolean          default(FALSE)
#  course_code                        :string(255)
#  organization_id                    :integer
#

require 'set'
class Group < ActiveRecord::Base
  include Taggable
  include PubnubChannels
  include Queueable

  belongs_to :creator, :class_name => 'User'
  belongs_to :client
  belongs_to :organization
  has_many :events, :dependent => :destroy
  has_many :daily_group_stats, :dependent => :destroy
  has_many :cards, :dependent => :destroy
  has_many :video_streams
  has_many :posts, as: :postable, :dependent => :destroy
  has_many :questions, as: :postable
  has_many :groups_users, foreign_key: 'group_id', :dependent => :destroy
  has_many :users, through: :groups_users
  has_many :groups_members, -> { member }, class_name: 'GroupsUser', foreign_key: 'group_id'
  has_many :members, :class_name => 'User', through: :groups_members, source: :user
  has_many :groups_admins, -> { admin }, class_name: 'GroupsUser', foreign_key: 'group_id'
  has_many :admins, :class_name => 'User', through: :groups_admins, source: :user
  has_many :channels_groups
  has_many :channels, through: :channels_groups
  has_many :scores

  has_one :pubnub_channel, as: :item
  has_one :public_pubnub_channel, -> { public_channel }, class_name: 'PubnubChannel', as: :item

  validates :name, presence: true

  after_commit :unpromote_if_made_private, on: :update

  scope :not_closed, -> { where.not(:close_access => true) }
  scope :client_private_groups, -> (client, parent_id){ where(client: client, type: 'PrivateGroup', parent_id: parent_id) }
  scope :org_private_groups, -> (org, parent_id){ where(organization: org, type: 'PrivateGroup', parent_id: parent_id) }
  scope :open, -> { where(access: 'open') }
  scope :latest, -> { order('groups_users.created_at desc') }
  scope :with_digest_enabled, -> { where(daily_digest_enabled: true) }

  scope :recent, -> { order('created_at DESC') }

  searchkick callbacks: false, merge_mappings: true, mappings: {
    _default_: {
      dynamic_templates: [],
      properties: {
        term: {
          type: 'string',
          index: 'not_analyzed'
        },
      }
    }
  }

  def unpromote_if_made_private
    if self.previous_changes['is_private'] == [false, true]
      Promotion.unpromote(promotable: self)
    end
  end

  def search_data
    additional_data = {}
    if organization_id
      additional_data[:organization_id] = organization_id
    end

    if tags.present?
      additional_data['tags'] = tags.map do |tag|
        {'id' => tag.id, 'name' => tag.name}
      end
    end

    if channels.present?
      additional_data['channels'] = channels.map do |channel|
        {id: channel.id, label: channel.label}
      end
    end

    as_json.merge(additional_data)
  end

  after_commit :reindex_async
  def reindex_async
    IndexJob.perform_later(self.class.name, self.id)
  end

  def unanswered_questions_count
    questions.where(:comments_count => 0).count
  end

  def is_user? user
    groups_users.where(user_id: user.id).present?
  end

  def is_admin? user
    groups_users.where(user_id: user.id, as_type: ['admin','instructor']).present?
  end

  def is_member? user
    groups_users.where(user_id: user.id, as_type: ['member','student']).present?
  end

  def ban_user(user)

    self.groups_users.where(user: user).each do |gu|
      unless gu.update(banned: true)
        return false
      end
    end

    true
  end

  def unban_user(user)
    self.groups_users.where(user: user).each do |gu|
      unless gu.update(banned: false)
        return false
      end
    end

    true
  end

  #user who is not banned
  def is_active_user?(user)
    is_active_user_id?(user.id) || is_active_user_id?(user.forum_user.try(:id))
  end

  alias_method :can_read_forum?, :is_active_user?
  alias_method :can_write_to_forum?, :is_active_user?

  def is_active_user_id?(uid)
    GroupsUser.is_active_user_in_group?(user_id: uid, group_id: self.id)
  end

  def is_active_admin?(user)
    is_active_admin_id?(user.id) || is_active_admin_id?(user.forum_user.try(:id))
  end

  def is_active_admin_id?(uid)
    GroupsUser.is_active_admin_in_group?(user_id: uid, group_id: self.id)
  end

  def is_active_member?(user)
    is_active_member_id?(user.id) || is_active_member_id?(user.forum_user.try(:id))
  end

  def is_active_member_id?(uid)
    GroupsUser.is_active_member_in_group?(user_id: uid, group_id: self.id)
  end

  # deprecated
  def is_active_super_user?(user)
    is_active_admin?(user)
  end

  # deprecated
  def is_active_normal_user?(user)
    is_active_member?(user)
  end

  def user_role(user)
    gu = groups_users.where(user_id: user.id).first
    if (gu.nil?)
      'none'
    elsif gu.banned
      'banned'
    else
      gu.as_type
    end
  end

  def add_user(user: nil, role: nil, label: nil)
    if role == 'member' || role == 'admin'
      if !is_user?(user)
          GroupsUser.create!(group: self, user: user, as_type: role, role_label: label)
      else
        gu = GroupsUser.where(group: self, user: user).first
        gu.update_attributes(as_type: role, role_label: label)
      end
    end
  end

  def add_members users
    users.each {|u| add_member(u)}
  end

  def add_member(user)
    if !is_user?(user)
      members << user
    elsif is_admin?(user)
      admins.destroy(user)
      members << user
    end
  end

  def add_admin(user)
    if !is_user?(user)
      admins << user
    elsif is_member?(user)
      members.destroy(user)
      admins << user
    end
  end

  def get_tags(start, finish)
    # query for both post and comment tags and convert them to a hash
    post = posts.joins(:tags)
      .select("tags.id as tag_id, tags.name AS tag_name, count(*) AS tag_count")
      .where('posts.created_at >= (:start) AND posts.created_at < (:finish)', {:start => start, :finish => finish})
      .group("tags.id")
      .order('tag_count desc').map { |x| { 'tag_id' => x.tag_id, 'tag_name' => x.tag_name, 'tag_count' => x.tag_count} }

    comment = Comment.joins(:tags).
      select("tags.id as tag_id, tags.name as tag_name, count(*) as tag_count").
      where("taggings.taggable_id IN (
                       Select comments.id from comments
                       INNER join posts
                       ON posts.id = comments.commentable_id
                       AND comments.commentable_type = 'Post'
                       where posts.postable_type = 'Group' and posts.postable_id = #{id})").
      group("tags.id").
      order('tag_count desc').map { |x| { 'tag_id' => x.tag_id, 'tag_name' => x.tag_name, 'tag_count' => x.tag_count} }

    # remove duplicate tags from the hash and sort them based on tag count
    (post + comment).uniq { |t| t['tag_name'] }.sort { |x, y| y['tag_count'] <=> x['tag_count'] }
  end

  def get_top_tags(start, finish, count)
    top_tags = get_tags(start, finish).first(count)
  end

  def tag_stats(user, tag_label)
    if user.nil? || tag_label.nil?
      return nil
    end

    ret = {}
    tag_posts = posts.includes(:tags).where(tags: {name: tag_label})
    ret[:num_posts] = tag_posts.count
    tag_comments = Comment.includes(:tags).references(:tags).where("commentable_type = 'Post' AND (commentable_id in (:commentables) OR tags.name = (:tag_label))", {commentables: tag_posts.map{|p| p.id}, tag_label: tag_label})
    ret[:num_comments] = tag_comments.count

    ret[:num_user_posts] = tag_posts.where(user: user).count
    ret[:num_user_comments] = tag_comments.where(user: user).count
    ret

  end

  def contexts
    results = Post.search '*', aggs: {context: {buckets: {order: {'doc_count' => 'desc'}}, where: {postable_type: 'Group', postable_id: self.id}}}, limit:0, load: false,  smart_aggs: false
    results.aggs['context']['buckets'].map do |bucket|
      context = ActiveSupport::JSON.decode(bucket['key']) #{id => ..., label => ...}
      context['type'] = 'context'
      context['count'] = bucket['doc_count']
      context
    end
  end

  def context_stats(user, context_id)
    if user.nil? || context_id.nil?
      return nil
    end

    ret = {}
    context_posts = posts.where(context_id: context_id)
    ret[:num_posts] = context_posts.count
    context_comments = Comment.where(commentable_type: 'Post', commentable_id: context_posts.map{|p| p.id})
    ret[:num_comments] = context_comments.count

    ret[:num_user_posts] = context_posts.where(user: user).count
    ret[:num_user_comments] = context_comments.where(user: user).count
    ret
  end

  def right_rail_stats
    # get stats entries which have either
    # trending tags or trending context
    group_stats = DailyGroupStat.where("group_id = (:group_id) AND (trending_tags IS NOT NULL OR trending_contexts IS NOT NULL)", group_id: id).order(date: :desc)

    keyset = Set.new([])
    ret = []
    group_stats.each do |gs|
      contexts = gs.trending_contexts.blank? ? [] : JSON.parse(gs.trending_contexts)
      contexts.each do |context|
        unless keyset.include?(context['id'])
          keyset << context['id']
          ret << {'type' => 'context', 'id' => context['id'], 'label' => context['label'], 'count' => context['count']}
        end
      end

      tags = gs.trending_tags.blank? ? [] : gs.trending_tags.split(',')
      tags.each do |tag|
        t, c = tag.split('#')
        unless keyset.include?(t)
          keyset << t
          ret << {'type' => 'tag', 'label' => t, 'count' => c.to_i}
        end
      end

      if ret.count > 10
        break
      end
    end
    ret
  end

  def get_stats(user, date)
    # get stats for last 30 days
    group_stats = DailyGroupStat.get_daily_stats(id, date, 30)

    all_posts = Post.search('*', where: {group_id: id}, aggs: {comments_count: {ranges: [{to: 1}, {from: 1}]}},
                             load: false, limit: 0)
    no_posts = all_posts.nil? || all_posts.response.blank?
    posts_read = PostRead.reads_count(group_id: self.id, user_id: user.id)

    trending_tags = group_stats.count > 0 && !group_stats.last.trending_tags.nil? ? group_stats.last.trending_tags.split(',') : []
    trending_contexts = group_stats.count > 0 && !group_stats.last.trending_contexts.nil? ? JSON.parse(group_stats.last.trending_contexts) : []
    user_score = user.score(self)
    total_posts = no_posts ? 0 : all_posts.response['hits']['total']

    stats = {}
    stats[:weekly] = {}
    stats[:alltime] = {}

    stats[:stars] = user_score[:stars]
    stats[:score] = user_score[:score]

    stats[:weekly][:trending_tags] = trending_tags
    stats[:weekly][:trending_contexts] = trending_contexts
    stats[:weekly][:num_threads] = group_stats.last(7).collect(&:total_posts).sum
    stats[:weekly][:num_replies] = group_stats.last(7).collect(&:total_comments).sum

    stats[:alltime][:num_threads] = total_posts
    stats[:alltime][:num_replies] = no_posts ? 0 : all_posts.aggregations['comments_count']['comments_count']['buckets'].last['doc_count'].to_i
    stats[:alltime][:num_posts_unread] = [(total_posts - posts_read),0].max

    # this would need to change to is_active_super_user? if you
    # are going to do stats for private groups
    if is_active_super_user?(user)
      stats[:weekly][:num_threads_without_replies] = group_stats.last(7).collect(&:total_unanswered_posts).sum
      stats[:alltime][:num_threads_without_replies] = no_posts ? 0 : all_posts.aggregations['comments_count']['comments_count']['buckets'].first['doc_count'].to_i

      stats[:daily_stats] = []
      stats[:daily_user_stats] = []
      group_stats.each do |s|
        day = s.date.strftime('%m/%d')
        stats[:daily_stats] << {day: day, num_threads: s.total_posts, num_threads_with_replies: s.total_posts-s.total_unanswered_posts}
        stats[:daily_user_stats] << {day: day, active_users: s.total_active_forum_users + s.total_active_mobile_forum_users, posted_users: s.total_users_with_posts, answered_users: s.total_users_with_comments}
      end

    end

    stats
  end

  # Any resource group created from savannah should be tied to the edcast admin user
  # No point in showing courses where there's no place to take users to (hence the website_url test)
  def savannah?
    organization && organization.client && organization.client.try(:user).try(:email) == User.cm_admin.try(:email) && !website_url.nil?
  end

  def promotion_eligible?
    !is_private && savannah? && (end_date.nil? || end_date > Date.current)
  end

  def details_locked?
    details_locked
  end

  def close
    update_attributes(close_access: true)
    Promotion.close(promotable: self)
  end

  def closed?
    close_access
  end

  def image(size = :medium)
    image_url
  end

  def public_pubnub_channel_name
    self.pubnub_channel_name(as_type: 'public', item_type: 'Group')
  end

  def update_memory_store
    CacheFactory.clear
  end

  def mailer_config
    organization.mailer_config
  end

  #client related methods
  def valid_credential
    organization.client.valid_credential
  end

  def app_admins
    organization.client.admins
  end

  def app_user
    organization.client.user
  end

  def app_name
    organization.client.name
  end

  def social_enabled
    organization.client.social_enabled
  end

  def digest_attrs
    [self.name, course_code, website_url]
  end

  def absolute_image_url
    return if self.image_url.to_s.blank?

    uri = URI.parse(self.image_url)
    uri.scheme = Settings.platform_force_ssl ? 'https' : 'http'

    uri.to_s
  end

  #group with valid organization
  def self.by_client_resource_id_and_api_key(params)
    joins(organization: [client: :credentials])
      .find_by(credentials: { api_key: params[:api_key] }, client_resource_id: params[:resource_id])
  end

  def self.by_client_resource_id(params: {}, organization_id: nil)
    where(client_resource_id: params[:resource_id], organization_id: organization_id).first
  end
end

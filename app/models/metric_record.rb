class MetricRecord
  MR_CARD_TYPES = %w(collection card video_stream)
  MR_CARD_ACTIONABLE_EVENTS = %w(like bookmark comment mark_completed_self mark_uncompleted_self mark_completed_assigned mark_uncompleted_assigned visit playback poll_respond)

  class Properties
    include Virtus.model
    attribute :duration, Integer #milliseconds, how long did the action take, like viewing a video.
    attribute :view, String, mapping: { index: 'not_analyzed' } #summary or full card impression
    attribute :startTime, Integer #milliseconds from 1970
    attribute :service, String, mapping: { index: 'not_analyzed' } #addthis sharing service provider
    #add more attributes if needed.
  end

  include Elasticsearch::Persistence::Model

  self.index_name = "metric_record_#{Rails.env}_v1"

  attribute :actor_id, Integer
  attribute :actor_type, String, mapping: { index: 'not_analyzed' }
  attribute :device_id, String, mapping: { index: 'not_analyzed' } #supporting unauthenticated users
  attribute :object_id, Integer
  attribute :object_type, String, mapping: { index: 'not_analyzed' }
  attribute :owner_id, Integer
  attribute :owner_type, String, mapping: { index: 'not_analyzed' }
  attribute :action, String, mapping: { index: 'not_analyzed' }
  attribute :platform, String, mapping: { index: 'not_analyzed' }
  #created_at, and #updated_at are automatically added
  attribute :properties, Properties, mapping: {type: 'object'} #nested property object
  attribute :organization_id, Integer
  attribute :referer, String, mapping: { index: 'not_analyzed' }
  attribute :ecl_id, String, mapping: { index: 'not_analyzed' }

  after_save :update_video_stream_score
  after_save :perform_aggregations, if: :should_perform_aggregations?
  after_save :mark_actor_as_destiny_user, if: :is_destiny_event?
  after_save :consume_card_for_actionable_events

  def is_destiny_event?
    should_perform_aggregations? && platform != 'forum'
  end

  def mark_actor_as_destiny_user
    actor = User.where(id: actor_id).first
    if actor && !actor.is_complete
      actor.update_with_reindex_async(is_complete: true)
    end
  end

  def should_perform_aggregations?
    object_type.present? && object_id.present? && actor_id.present? &&
      (object_type.in?(['collection', 'card', 'channel', 'user', 'team']) || object_type.include?('video_stream'))
  end

  def perform_aggregations
    aggregate_time_spent_metric
    # Optimization to ensure jobs dont get enqueued when not required
    unless is_partial_impression?
      aggregate_user_level_metrics
      aggregate_user_content_level_metrics
      aggregate_team_level_metrics
      aggregate_org_level_metrics
      aggregate_content_level_metrics
      aggregate_user_topic_level_metrics
      aggregate_user_taxonomy_level_metrics
    end
  end

  def aggregate_team_level_metrics
    TeamLevelMetricsAggregatorJob.perform_later(attributes_for_activejob)
  end


  def aggregate_time_spent_metric
    UserTimeSpentAggregatorJob.perform_later(attributes_for_activejob)
  end

  def aggregate_user_level_metrics
    UserLevelMetricsAggregatorJob.perform_later(attributes_for_activejob)
  end

  def aggregate_user_content_level_metrics
    UserContentLevelMetricsAggregatorJob.perform_later(attributes_for_activejob)
  end

  def aggregate_org_level_metrics
    OrgLevelMetricsAggregatorJob.perform_later(attributes_for_activejob)
  end

  def aggregate_content_level_metrics
    ContentLevelMetricsAggregatorJob.perform_later(attributes_for_activejob)
  end

  def aggregate_user_topic_level_metrics
    UserTopicLevelMetricsAggregatorJob.perform_later(attributes_for_activejob)
  end

  def aggregate_user_taxonomy_level_metrics
    UserTaxonomyLevelMetricsAggregatorJob.perform_later(attributes_for_activejob)
  end


  # should consume card if not already for actionable events
  def consume_card_for_actionable_events
    if (attributes[:object_type].in?(MR_CARD_TYPES) &&
      attributes[:action].in?(MR_CARD_ACTIONABLE_EVENTS))

      attrs                    = attributes.dup
      attrs[:action]           = 'impression'
      attrs[:created_at_to_i]  = created_at.to_i
      attrs[:properties]       = Properties.new(view: 'full')
      attrs[:id]               = nil

      if MetricRecord.is_first_such_event?(attrs)
        MetricRecord.new(attrs).save
      end
    end
  end

  def is_partial_impression?
    action == "impression" && properties.try(:view) == "summary"
  end

  # Normalize polymorphic classes wowza_video_stream, iris_video_stream etc on to video_stream
  def normalized_object_type_and_id
    if object_type && object_type.include?("video_stream")
      {object_type: "video_stream", object_id: object_id}
    elsif object_type == "card" # Backward compatibility reason. Remove once one time task is run
      c = Card.find_by(id: object_id)
      if c.try(:card_type) == "pack"
        {object_type: "collection", object_id: object_id}
      else
        {object_type: object_type, object_id: object_id}
      end
    else
      {object_type: object_type, object_id: object_id}
    end
  end

  def attributes_for_activejob
    @activerecord_attributes ||= attributes.except(:created_at, :updated_at, :properties).merge({created_at_to_i: created_at.to_i, properties: properties.as_json}).merge(normalized_object_type_and_id)
  end

  class << self

    def get_taxonomy_topics_for_object(_id)
      Card.find_by(id: _id).try(:taxonomy_topics) || []
    end

    def get_topics_for_object(attributes)
      object = (case attributes[:object_type]
      when 'card', 'collection'
        Card.find_by(id: attributes[:object_id])
      when 'video_stream'
        VideoStream.find_by(id: attributes[:object_id])
      else
        nil
      end)

      object.nil? ? [] : object.tags.where(type: "Interest")
    end

    def is_partial_impression?(attributes)
      attributes[:action] == "impression" && attributes[:properties].try(:[], "view") == "summary"
    end

    def is_first_such_event?(attributes)
      ands = [{term: {actor_type: "user"}},
              {term: {actor_id: attributes[:actor_id]}},
              {term: {action: attributes[:action]}},
              {term: {object_id: attributes[:object_id]}},
              {term: {object_type: attributes[:object_type]}},
              {range: {created_at: {lt: Time.at(attributes[:created_at_to_i])}}}]
      if attributes[:properties].try(:[], "view")
        ands << {term: {"properties.view" => attributes[:properties]["view"]}}
      end

      MetricRecord.count(query: {
        filtered: {
          filter: {
            and: ands
          }
        }
      }) < 1
    end

    def object_type_supported?(object_type_param)
      object_type_param.in?(['collection', 'card', 'channel', 'user', 'team']) || object_type_param.include?('video_stream')
    end

    def should_log_org_level_metric? attributes
      !is_partial_impression?(attributes) &&
        object_type_supported?(attributes[:object_type]) &&
        (attributes[:action] == "comment" || is_first_such_event?(attributes)) &&
        !(attributes[:object_type] == 'card' && Card.find_by(id: attributes[:object_id]).try(:state) == "draft")
    end

    def should_increment_smartbite_score? attributes
      !is_partial_impression?(attributes) &&
        object_type_supported?(attributes[:object_type]) &&
        (attributes[:action] == "comment" || is_first_such_event?(attributes)) &&
        !((attributes[:object_type] == 'card' || attributes[:object_type] == 'collection') && Card.find_by(id: attributes[:object_id]).try(:state) == "draft")
    end

    def is_creation_event? attributes
      attributes[:action].in?(["create", "poll_create", "videostream_create", "publish"]) &&
        attributes[:object_type].in?(MR_CARD_TYPES) &&
          !(attributes[:object_type] == 'card' && Card.find_by(id: attributes[:object_id]).try(:state) == "draft") # "publish" action is for pathways, should consolidate all under "create"
    end

    def is_consumption_event? attributes
      attributes[:action] == "impression" && !is_partial_impression?(attributes) && is_first_such_event?(attributes)
    end

    def is_comment_event? attributes
      attributes[:action] == "comment"
    end

    def is_uncomment_event? attributes
      attributes[:action] == "uncomment"
    end

    def is_like_event? attributes
      attributes[:action] == "like" && is_first_such_event?(attributes)
    end

    def is_unlike_event? attributes
      attributes[:action] == "unlike" && is_first_such_event?(attributes)
    end

    def is_mark_completed_event? attributes
      attributes[:action].in?(["mark_completed_assigned", "mark_completed_self"]) && is_first_such_event?(attributes)
      end

    def is_mark_uncompleted_event? attributes
      attributes[:action].in?(["mark_uncompleted_assigned", "mark_uncompleted_self"]) && is_first_such_event?(attributes)
    end

    def is_bookmark_event? attributes
      attributes[:action] == 'bookmark' && is_first_such_event?(attributes)
    end

    def reaggregate_time_spent_metrics(user_id)
      find_each(query: {filtered: {filter: {and: [{term: {actor_id: user_id}}]}}}, sort: [{created_at: :asc}]) do |mr|
        if mr.should_perform_aggregations?
          mr.aggregate_time_spent_metric
        end
      end
    end

    def reaggregate_user_level_metrics(user_id)
      find_each(query: {filtered: {filter: {and: [{term: {actor_id: user_id}}]}}}, sort: [{created_at: :asc}]) do |mr|
        if mr.should_perform_aggregations?
          unless mr.is_partial_impression?
            mr.aggregate_user_level_metrics
            mr.aggregate_user_content_level_metrics
          end
        end
      end
    end

    def reaggregate_team_level_metrics(team)
      team.users.select(:id).find_each do |user|
        find_each(query: {filtered: {filter: {and: [{term: {actor_id: user.id}}]}}}) do |mr|
          if mr.should_perform_aggregations?
            unless mr.is_partial_impression?
              mr.aggregate_team_level_metrics
            end
          end
        end
      end
    end

    def reaggregate_content_level_metrics(content_id, content_type)
      find_each(query: {filtered: {filter: {and: [{term: {object_id: content_id}}, {term: {object_type: content_type}}]}}}) do |mr|
        if mr.should_perform_aggregations?
          unless mr.is_partial_impression?
            mr.aggregate_content_level_metrics
          end
        end
      end
    end

    def reaggregate_org_level_metrics(organization_id)
      User.where(organization_id: organization_id).find_each do |user|
        find_each(query: {filtered: {filter: {and: [{term: {actor_id: user.id}}]}}}) do |mr|
          if mr.should_perform_aggregations?
            unless mr.is_partial_impression?
              mr.aggregate_org_level_metrics
            end
          end
        end
      end
    end

    def reaggregate_user_topic_level_metrics(topic)
      Tagging.where(tag_id: topic.id, taggable_type: ["Card", "VideoStream"]).find_each do |tagging|
        _object_type = tagging.taggable.class.name.underscore
        find_each(query: {filtered: {filter: {and: [{term: {object_id: tagging.taggable_id}}, {term: {object_type: _object_type}}]}}}) do |mr|
          if mr.should_perform_aggregations?
            unless mr.is_partial_impression?
              mr.aggregate_user_topic_level_metrics
            end
          end
        end
      end
    end

  end

  def initialize(attributes={})
    keys_to_remove = []
    attributes.each do |k, v|
      if v.is_a?(ActiveRecord::Base)
        self.send("#{k}_id=".to_sym, v.id)
        self.send("#{k}_type=".to_sym, v.class.name.underscore)
        keys_to_remove << k
      end
    end
    keys_to_remove.each do |k|
      attributes.delete(k)
    end

    super

  end

  def update_video_stream_score
    if self.object_type && !self.object_type.index('video_stream').nil? and self.action == "playback"
      video_stream = VideoStream.find(self.object_id)
      video_stream.update_views_count
      VideoStreamUpdateScoreJob.perform_later(video_stream)
    end
  end

  def self.video_stream_view_count(video_stream, time)
    range_query = {}
    if(!time.nil?)
      range_query = { range: {created_at: { 'gte': time }}}
    end
    query = { query: { filtered:
      { filter:
        { and: [
          { term: {object_type: 'video_stream'}},
          { term: {object_id: video_stream.id }},
          { term: {action: 'playback' }},
          range_query ]
        }
      }
    }}
    return MetricRecord.gateway.client.count(index:MetricRecord.index_name, body: query)['count']
  end

  def self.oldest_view(video_stream, time)
    query = {
      query: { filtered: {
          filter: {
            and: [
              { term: {object_type: 'video_stream'}},
              { term: {object_id: video_stream.id }},
              { term: {action: 'playback' }},
              { range: { created_at: { 'gte': time }} }]
          },
        }
      },
      sort: {
        'created_at': 'asc'
      },
      size: 1
    }
    response = MetricRecord.gateway.client.search(index:MetricRecord.index_name, body: query)
    unless response['hits']['total'] == 0
      return Time.zone.parse(response['hits']['hits'].first['_source']['created_at'])
    else
      return nil
    end
  end

  def self.views_count(content_type:, content_id:)
    data = MetricRecord.gateway.client.search(
      index: MetricRecord.index_name,
      body: {
        size: 0,
        query: {
          filtered: {
            filter: {
              and: [
                { term: { object_type: content_type } },
                { term: { object_id: content_id } },
                { term: { action: 'impression' } },
                { not:  { term: {"properties.view" => "summary" } } }
              ]
            }
          }
        },
        aggs: {
          unique_actors: {
            cardinality: {
              field: "actor_id"
            }
          }
        }
      }
    )

    data.present? ? data["aggregations"]["unique_actors"]["value"] : 0
  end

  # Due to multiple view records get created for a user with card,
  # added aggregate query to get unique actor ids
  def self.get_users(action:, content_type:, content_id:, limit:, offset:)
    query = {
              and: [
                { term: { object_type: content_type } },
                { term: { object_id: content_id } },
                { term: { action: action } }
              ]
            }

    # skip partial impressions
    if action == "impression"
      query[:and] << { not: { term: {"properties.view" => "summary" } } }
    end

    result = MetricRecord.gateway.client.search(
      index: MetricRecord.index_name,
      body: {
        size: 0,
        query: {
          filtered: {
            filter: query,
          }
        },

        sort: {
          'created_at': 'asc'
        },

        aggs: {
          unique_actors: {
            terms: {
              field: "actor_id",
              size: 0
            }
          }
        }
      }
    )

    user_ids = result['aggregations']["unique_actors"]["buckets"].map{|user| user['key']}
    user_ids_with_pagination = user_ids[offset, limit]

    user_ids_with_pagination.present? ? User.where(id: user_ids_with_pagination) : []
  end

  def self.get_action_key(key)
    case key
      when "impression"
        'view'
      when *%w(mark_completed_assigned mark_completed_self)
        'complete'
      when *%w(mark_uncompleted_assigned mark_uncompleted_self)
        'uncomplete'
      else
        key
    end
  end
end

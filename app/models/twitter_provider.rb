# == Schema Information
#
# Table name: identity_providers
#
#  id            :integer          not null, primary key
#  type          :string(191)
#  uid           :string(191)
#  user_id       :integer
#  token         :string(4096)
#  secret        :string(255)
#  expires_at    :datetime
#  created_at    :datetime
#  updated_at    :datetime
#  token_scope   :string(255)
#  auth          :boolean
#  auth_info     :text(65535)
#  refresh_token :string(255)
#

class TwitterProvider < IdentityProvider
  def is_active?
    true
  end

  def self.is_verified?(auth_hash)
    false
  end

  def share(message: nil, object: nil)
    client = Twitter::REST::Client.new do |config|
      config.consumer_key        = Settings.twitter.api_key
      config.consumer_secret     = Settings.twitter.secret
      config.access_token        = self.token
      config.access_token_secret = self.secret
    end

    if object.is_a?(Card)
      owner = User.find(object.author_id)
      org = owner.organization
      host_name = org.host
    else
      host_name = Settings.platform_host
    end

    long_url = (object.kind_of? Post) ? object.ref_url : "https://#{host_name}/insights/#{object.slug}"
    short_url = "https://#{host_name}/~/#{Shortener::ShortenedUrl.generate(long_url).unique_key}"

    begin
      up_to = short_url.length
      status = message.truncate(140-up_to-1)

      result = client.update("#{status} #{short_url}")

      result.url.to_s
    rescue => e
      log.error("Twitter share failed due to #{e.inspect}")
      nil
    end
  end
end

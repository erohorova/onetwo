# == Schema Information
#
# Table name: resources
#
#  id          :integer          not null, primary key
#  url         :text(16777215)   not null
#  description :text(16777215)
#  title       :string(255)
#  type        :string(255)
#  site_name   :string(255)
#  image_url   :text(16777215)
#  video_url   :text(16777215)
#  created_at  :datetime
#  updated_at  :datetime
#  embed_html  :text(16777215)
#  url_hash    :string(64)
#  user_id     :integer
#

class Resource < ActiveRecord::Base
  require 'open-uri'
  require 'open_uri_redirections'

  include UrlMetadataExtractor
  include AttachmentWorthy
  include Actionable

  has_many :cards

  before_validation on: :create do
    unless image_url.blank?
      begin
        img_url = Addressable::URI.parse(image_url)
        unless img_url.scheme == 'http' || img_url.scheme == 'https' || image_url.start_with?('//')
          self.image_url = nil
        end
      rescue
        self.image_url = nil
      end
    end
  end

  validates :description, length: { maximum: 1000 }
  validates :url, :url => true
  validate :check_image_url, :unless => "image_url.blank?"
  validates :video_url, :url => true, :unless => "video_url.blank?"

  after_commit :download_and_save_assets, on: [:create, :update], if: :record_changed?
  after_create :autoplay_video
  before_save :generate_url_hash
  before_save :sanitize_title_description
  before_save :strip_emoji_title_description

  scope :by_url, -> (url) { where(url_hash: Resource.get_url_hash(url)) }

  def is_scorm_resource?
    url && url.include?('/api/scorm/launch?course_id')
  end

  def autoplay_video
    if video_url
      self.video_url = UrlBuilder.build(video_url, {autoplay: 1}).to_s
      self.save
    end
  end

  def record_changed?
    previous_changes.present?
  end

  def download_and_save_assets
    ResourceJob.perform_later(self)
  end

  # Resource Job finished
  def callback_asset_download_completed
    #Process object where resource is being used
    cards.map(&:reindex_card)
  end

  # Expired URL 'https://xyz.wordpress.com/2016/11/03/deleted-post/'
  # Redirected URL 'http://99tests.edcastcloud.com/s/fae0a'
  # URL 'https://xyz.wordpress.com/2015/04/08/preview-document-before-uploading-via-jquery/'
  def expired?
    connection = Faraday.new do |conn|
      conn.use FaradayMiddleware::FollowRedirects
      conn.adapter Faraday.default_adapter
    end

    response = connection.get(self.url)

    !response.success?
  end

  class << self
    # creates and returns a 'Resource' activerecord object from
    # text that contains a url
    # Returns nil if no url present in text
    # Does not create a new resource if concerned url is already present in the \
    # Resource table
    def extract_resource(text, user: nil, organization: nil, s3_bucket: false)
      # Don't do anything if we don't have text
      return nil if text.nil?

      # Check if we got a valid URL for the resource
      url = UrlMetadataExtractor.return_url text
      return nil if url.nil?

      # Parse the URL and get the hostname
      parsed_url = URI.parse(url)
      hostname = parsed_url.hostname rescue nil

      # If an EdCast URL is pasted, then we want to copy over the resource of
      # the pasted smartcard instead of actually going to scraping the URL.
      # Let SCORM urls pass by as usual
      unless parsed_url.path.starts_with?('/api')
        if Organization.find_by_request_host(hostname).present?
          if parsed_url.path.include?("/discover/")
            return resource_from_source(parsed_url, organization, user)
          else
            return resource_from_existing_card(parsed_url, organization, user)
          end
        end
      end

      # If text contains url, create and return resource, else nil
      if url && !(url.include?(Settings.s3_prefix) || url.include?(Settings.s3_prefix_https))
        url_string = CGI.unescapeHTML(url)
        opts = if s3_bucket.present?
          array_name_region = organization.s3_bucket_name_and_region
          { s3_bucket: array_name_region } if array_name_region.all?(&:present?)
        end
        # New resource
        resource_params = get_resource_params(url, opts || {})
        resource_params[:url] = url_string

        #Update user resources in future
        resource_params[:user_id] = user.id unless user.nil?

        Resource.create(resource_params)
      else
        nil
      end
    end

    def resource_from_source(parsed_url, organization, user)
      url_path = parsed_url.path

      response = EclApi::CardConnector.new(organization_id: organization.id)
        .fetch_source_by_slug(url_path.split('/')[2])

      if response['source']

        new_resource =  Resource.new(
          title: response['source']['display_name'],
          description: response['source']['description'],
          url: parsed_url.to_s,
          type: "Article",
          image_url: response['source']['banner_url'] || response['source']['logo_url'] ||
            response['source']['source_type']['image_url']
        )
        new_resource.user_id = user.id if user.present?

        if new_resource.save
          new_resource
        end
      end
    end

    def resource_from_existing_card(parsed_url, organization, user)
      # STEP 1: Get existing resource
      url_path = parsed_url.path

      existing_card = Card.from_url_path(url_path, organization)

      return nil if existing_card.nil?

      if existing_card.resource_id.present?
        # STEP 2: If the existing card has a resource, clone the
        # resource
        new_resource = clone_resource(existing_card.resource)
      else
        # STEP 3: If the existing card does not have a resource, create
        # a new resource with:
        # 1. Link to the smartcard within edcast
        # 2. Have the title as the title of the original smartcard
        # 3. Description as the original description
        new_resource = Resource.new(
          title: existing_card.title,
          description: existing_card.message,
          url: parsed_url.to_s,
          type: "Article"
        )
      end
      new_resource.user_id = user.id if user.present?

      # return the newly created resource
      new_resource.save
      if new_resource.persisted?
        new_resource
      else
        nil
      end
    end

    def clone_resource(existing_resource, user_id: nil)
      Resource.new(
        url: existing_resource.url,
        description: existing_resource.description,
        title: existing_resource.title,
        type: existing_resource.type,
        site_name: existing_resource.site_name,
        image_url: existing_resource.image_url,
        video_url: existing_resource.video_url,
        embed_html: existing_resource.embed_html,
        url_hash: existing_resource.url_hash,

        user_id: user_id,
      )
    end

    def get_resource_params(url, opts)
      resource_params = {}
      url_string = CGI.unescapeHTML(url)
      metadata = UrlMetadataExtractor.get_metadata url
      # get embed html
      resource_params[:embed_html] = metadata['embed_html']
      title = populate_resource metadata, ['title', 'og:title', 'twitter:title']
      if title.nil?
        resource_params[:title] = ''
      else
        resource_params[:title] = title
      end

      resource_params[:title] = resource_params[:title].truncate(250) # fit into string db column

      resource_params[:description] = populate_resource metadata, ['description','og:description', 'twitter:description']
      unless resource_params[:description].nil?
        resource_params[:description] = resource_params[:description].truncate(500)
      end
      resource_params[:image_url] = populate_resource metadata, ['og:image','twitter:image']
      unless valid_image_url(resource_params[:image_url])
        resource_params[:image_url] = if opts[:s3_bucket].present?
          # return random image_url from s3 if correct bucket_name and bucket_region
          # else return StockImageArray::CARD_STOCK_IMAGE.sample[:url]
          set_default_image_for_resource(*opts[:s3_bucket]) # { s3_bucket: [bucket_name, bucket_region] }
        else
          CARD_STOCK_IMAGE.sample[:url]
        end
      end

      resource_params[:video_url] = generate_youtube_video_url(url)
      if resource_params[:video_url].blank?
        resource_params[:video_url] = populate_resource(metadata, ['og:video:url','og:video','twitter:player'])
        # trying to get video link from oembed_html src if video_link still blank
        # check EP-10553 (lynda sources)
        if resource_params[:video_url].blank? && resource_params[:embed_html] && metadata['oembed_type'] == 'video'
          resource_params[:video_url] = UrlMetadataExtractor.get_src_from_embed(resource_params[:embed_html])
        end
        resource_params[:video_url] = UrlMetadataExtractor.valid_video_url(resource_params[:video_url]) if resource_params[:video_url].present?
      end
      resource_params[:site_name] = populate_resource metadata, ['og:site_name', 'twitter:site']
      if metadata.key?('oembed_type') && (metadata['oembed_type'] == 'video') && resource_params[:video_url].present?
        resource_params[:type] = 'Video'
      elsif metadata.key?('og:type') && metadata['og:type'] =~ /video/i && resource_params[:video_url].present?
        resource_params[:type] = 'Video' # refer EP-8493 for more details
      elsif metadata.key?('og:type') && metadata['og:type'] == 'article'
        resource_params[:type] = 'Article'
      elsif metadata['content_type'] =~ /^image\//i
        resource_params[:type] = 'Image'
        resource_params[:image_url] = url_string
      # more info in EP-8324
      elsif metadata['content_type'] =~ /^video\//i
        resource_params[:type] = 'Video'
        resource_params[:video_url] = url_string
      else
        resource_params[:type] = 'Article'
      end
      return resource_params
    end

    def valid_image_url(image_url)
      img_url = Addressable::URI.parse(image_url)
      return true if img_url.present? && (img_url.scheme == 'http' || img_url.scheme == 'https' || image_url.start_with?('//'))
    end

    # populates params to create resource object by searching the 'metadata' hash for keys given in 'search_fields'
    # params
    # params_key: The Resource key to be filled in (Example: title, description, image_url ...)
    # resource_params: The hash of params to construct a resource object
    # metadata: metadata hash
    # search_fields: Array of fields to search for in order
    #
    def populate_resource metadata, search_fields
      search_fields.each do |key|
        if metadata.key?(key)
          return metadata[key]
        end
      end
      nil
    end

    def generate_youtube_video_url url
      if /\A((http|https):)?(\/\/)?(www.)?(youtube.com|youtu.be).*(?:\/|v=)(?<video_id>[^&$#?]+)/ =~ url
        yt_url = "//www.youtube.com/v/#{video_id}"
        UrlBuilder.build(yt_url, { autohide: 1 }).to_s
      end
    end

    def read_tags_and_keywords_from url
      # Fetch tags from link.
      # This step is delayed to prevent code break for UGC
      # This step is scoped to CMS only
      # However, it does make an extra fetch call to the link to fetch metatags
      begin
        metadata = UrlMetadataExtractor.get_metadata url

        # Match keyword or tags Ex name="news_keywords", name="xyz:tags"
        # split by "," and sanitize them
        # remove duplicates
        return metadata.select {|mt| mt.match(/keyword/) || mt.match(/tag/)}.values.inject([]) {|topics, v| topics+= v.split(',') }.map(&:strip).uniq
      rescue => e
        log.error "Extracting tags from url: #{url} failed with error: #{e}"
        return []
      end
    end


    def get_url_hash url
      Digest::SHA256.hexdigest URI(url).normalize.to_s
    end

    def set_default_image_for_resource(bucket_name, bucket_region)
      obj = S3ObjectExtractorService.new(
        bucket: bucket_name, region: bucket_region
      )
      data = obj.extract_image_objects
      if obj.failures.has_key?(:aws_access) || data.blank?
        log.warn(obj.failures[:aws_access])
        return CARD_STOCK_IMAGE.sample[:url]
      end

      URI.escape(data.sample)
    end
  end

  def url_with_context(current_user_id)
    # Sanity checks
    return url if url.blank?
    return url if Settings.scorm.user_context_secret.blank?
    return url if current_user_id.blank?

    # Parse the URL
    parsed_url = Addressable::URI.parse(url)
    return url if parsed_url.hostname.nil?

    # Check if we have EdCast's hostname
    # Will NOT work with vanity URLs
    platform_domain = Settings.platform_domain
    if !(parsed_url.hostname == platform_domain ||
         parsed_url.hostname.end_with?(".#{platform_domain}"))
      return url
    end

    # Check if we have the right path
    return url unless parsed_url.path == '/api/scorm/launch'

    # Append user context
    user_context = CGI.escape(JWT.encode({user_id: current_user_id}, Settings.scorm.user_context_secret, 'HS256'))
    url + "&user_context=#{user_context}"
  end

  def image_url_secure
    return false if image_url.blank?
    return image_url if Settings.secure_images.prefix_url.blank?
    parsed_image_url = Addressable::URI.parse(image_url)

    if parsed_image_url.scheme == 'https' || image_url.start_with?('//')
      image_url
    else
      encoded_url = JWT.encode({url: image_url}, Settings.secure_images.secret, 'HS256')
      "#{Settings.secure_images.prefix_url}#{CGI.escape(encoded_url)}"
    end
  end

  def check_image_url
    begin
      Addressable::URI.parse(image_url)
    rescue => e
      self.errors.add(:image, "Invalid Image URL")
    end
  end

  def parsed_client_resource_id
    return if url.blank?
    normalized_path = Addressable::URI.parse(url).normalized_path
    url_path_components = normalized_path.split("/").reject { |c| c.empty? }
    case url_path_components.last
    when 'link'
      group_id = url_path_components[1]&.to_i
      group = Group.find_by(id: group_id)
      group&.client_resource_id
    when 'redirect'
      url_path_components[1]&.to_i
    end
  end

  private

  def generate_url_hash
    # Hash the URL, but don't hash null values
    if self[:url].present?
      self[:url_hash] = self.class.get_url_hash(self[:url])
    else
      self[:url_hash] = nil
    end
  end

  def sanitize_title_description
    self.title = self.title.try(:clean)
    self.description = self.description.try(:clean)
  end

  def strip_emoji_title_description
    self.title = strip_emoji(title) if title.present?
    self.description = strip_emoji(description) if description.present?
  end
end

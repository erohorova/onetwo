class EmbargoFile < ActiveRecord::Base
  belongs_to :user
  belongs_to :organization

  def extract_specific_rows(limit: 10, offset: 0)
    BulkImportEmbargo.new(file_id: self.id).paginate_csv_data(get_range(limit, offset))
  end

  private

  def get_range(limit, offset)
    offset..(limit + offset) - 1
  end

end

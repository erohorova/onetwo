class ProjectSubmissionReview < ActiveRecord::Base
  belongs_to :reviewer, foreign_key: "reviewer_id", class_name: "User"
  belongs_to :project_submission

  validates_presence_of :project_submission, :reviewer
  validate :validate_submission_reviewer

  enum status: {in_progress: 0, approved: 1, rejected: 2}

  scope :approved_review, ->(project_submission_ids) { where("status = ? and
        project_submission_id in (?)", 1, project_submission_ids) }

  def validate_submission_reviewer
    errors.add(:base, "You cannot Review this project submission.") if (
        reviewer_id != project_submission.project.reviewer_id)
  end
end

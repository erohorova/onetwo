# == Schema Information
#
# Table name: comments
#
#  id               :integer          not null, primary key
#  message          :text(16777215)   not null
#  type             :string(255)
#  commentable_id   :integer
#  commentable_type :string(191)
#  user_id          :integer          not null
#  created_at       :datetime
#  updated_at       :datetime
#  votes_count      :integer          default(0)
#  resource_id      :integer
#  hidden           :boolean          default(FALSE)
#  anonymous        :boolean          default(FALSE)
#  is_mobile        :boolean          default(FALSE)
#  client_item_id   :string(191)
#  comments_count   :integer          default(0)
#

class LiveComment < Comment

  # More functionality coming to replay these comments with the stream
end

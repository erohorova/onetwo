# == Schema Information
#
# Table name: posts
#
#  id             :integer          not null, primary key
#  title          :string(255)
#  message        :text(16777215)
#  type           :string(255)
#  user_id        :integer          not null
#  group_id       :integer
#  created_at     :datetime
#  updated_at     :datetime
#  votes_count    :integer          default(0)
#  resource_id    :integer
#  comments_count :integer          default(0)
#  hidden         :boolean          default(FALSE)
#  anonymous      :boolean          default(FALSE)
#  pinned         :boolean          default(FALSE)
#  context_id     :string(255)
#  context_label  :string(255)
#  is_mobile      :boolean          default(FALSE)
#  pinned_at      :datetime
#  postable_id    :integer
#  postable_type  :string(191)
#

class Announcement < Post

  validate :staff_created_announcement
  
  after_commit :notify_group, on: :create
  after_commit :generate_notification, on: :create

  add_to_stream action: 'posted'
  remove_from_stream action: 'posted'

  def staff_created_announcement
    unless created_by_staff?
      errors.add(:user, 'Announcements must be created by instructors.')
    end
  end

  def notify_group
    AnnouncementJob.perform_later({post_id: self.id}) if group.forum_notifications_enabled
  end

  def generate_notification
    NotificationGeneratorJob.perform_later(item_id: id, item_type: 'announcement')
  end

end

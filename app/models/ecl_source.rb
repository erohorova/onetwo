# frozen_string_literal: true

# == Schema Information
#
# Table name: ecl_sources
#
#  id              :integer          not null, primary key
#  channel_id      :integer
#  ecl_source_id   :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  rate_interval   :integer
#  rate_unit       :string(255)
#  number_of_items :integer
#  next_run_at     :datetime
#

class EclSource < ActiveRecord::Base
  include EventTracker

  attr_accessor :display_name,:restricted_users,:restricted_teams

  DEFAULT_NUMBER_OF_ITEMS = 10

  belongs_to :channel, inverse_of: :ecl_sources

  validates :ecl_source_id, presence: true
  validates :channel, presence: true

  validates_uniqueness_of :ecl_source_id, scope: [:channel_id]

  # TODO: Verify ECL source
  # validate :verify_ecl_source_id

  before_create :set_next_run_at
  #Enqueue setup_fetch only if channel_programming_v2 = false
  after_commit :setup_fetch, on: :create, unless: :channel_programming_v2_enabled?

  configure_metric_recorders(
    default_actor: lambda { |record| record.channel&.creator },
    trackers: {
      on_create: {
        event: 'channel_ecl_source_added',
        job_args: [{channel: :channel}, {ecl_source_id: :ecl_source_id}],
        exclude_job_args: [:org]
      },
      on_delete: {
        event: 'channel_ecl_source_removed',
        job_args: [{channel: :channel}, {ecl_source_id: :ecl_source_id}],
        exclude_job_args: [:org]
      }
    }
  )

  def setup_fetch
    EclSourceFetchJob.perform_later(self.id)
  end

  def search_ecl(filters = {}, offset = 0)
    no_of_items = number_of_items || DEFAULT_NUMBER_OF_ITEMS
    channel_topics_names = channel.topics.map {|topic| topic['name']}
    channel_topic_ids = channel.topics.map {|topic| topic['id']}
    filter_params = {
        source_id: [ecl_source_id],
        topic: channel_topics_names,
        topic_ids: channel_topic_ids,
        sort_by_recency: true,
        type: "channel_programming"
    }
    filter_params.merge!(filters) if filters.present?
    res = EclApi::EclSearch.new(channel.organization.id).search(
      query: '',
      limit: no_of_items,
      offset: offset,
      filter_params: filter_params
    )
    res.results
  end

  def channel_programming_v2_enabled?
    self.channel.organization.get_settings_for('channel_programming_v2')
  end

  def rate
    case self.rate_unit
    when "hours"
      rate = self.rate_interval.hours
    when "minutes"
      rate = self.rate_interval.minutes
    when "days"
      rate = self.rate_interval.days
    else
      rate = 1.hours
    end
    return rate
  end

  private

  def set_next_run_at
    if self.next_run_at.nil?
      self.next_run_at = Time.now.utc + rate
    end
  end
end

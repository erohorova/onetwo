class CardMetricsAggregation < ActiveRecord::Base
  belongs_to :organization
  belongs_to :card

  validates_presence_of *%i{
    organization_id card_id
    num_likes       num_views
    num_completions num_bookmarks
    num_comments    end_time
    start_time      num_assignments
  }
end

class RechargePrice < ActiveRecord::Base

  belongs_to :recharge

  validates :amount, :currency, presence: true
end

# == Schema Information
#
# Table name: clcs
#
#  id              :integer          not null, primary key
#  entity_id       :integer
#  entity_type     :string(20)
#  from_date       :date
#  to_date         :date
#  target_score    :integer
#  organization_id :integer
#  target_steps    :string(100)
#  deleted_at      :datetime
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  name            :string(255)
#

# All CLC settings for entities (Organization, Channel and Group) will be saved in this table
# target_score will be saved in hours here.
class Clc < ActiveRecord::Base
  include EventTracker
  acts_as_paranoid

  belongs_to :organization
  belongs_to :entity, polymorphic: true

  has_many :clc_progresses
  has_many :clc_badgings, foreign_key: :badgeable_id

  validates :organization_id, :from_date, :to_date, :entity_id, :entity_type, :target_score, :name, presence: true
  validates :name, length: { maximum: 25, too_long: "25 characters is the maximum allowed" }
  validate :from_date_and_to_date

  before_create :process_new_clc

  after_commit :check_if_clc_started, on: [:create]

  accepts_nested_attributes_for :clc_badgings, update_only: true

  def active?
    self.from_date <= Date.today && self.to_date >= Date.today
  end

  def self.active
    where('from_date <= :today AND to_date >= :today', today: Date.today)
  end

  def check_if_clc_started
    Clc.create_notification_for_started_clc
  end

  def self.create_notification_for_started_clc
    today_clc = Clc.find_by("Date(from_date)=?", Date.today)
    if today_clc.present?
      NotificationGeneratorJob.perform_later({item_id: today_clc.id, item_type: 'clc', notification_type: 'clc_started'})
    end
  end

  def process_new_clc
    existing = self.class.where(entity_id: entity_id, entity_type: entity_type).first
    existing.destroy if existing
  end

  def from_date_and_to_date
    errors.add(:from_date, 'Invalid from_date') if(from_date  && (from_date < Date.today))
    errors.add(:to_date, 'Invalid to_date') if(to_date && from_date && (to_date <= from_date))
  end

  def self.get_clc_for_entity(args)
    where(
      entity_id: args[:entity_id],
      entity_type: args[:entity_type]
    ).includes(clc_badgings: [:clc_badge]).order("id DESC")
  end

  configure_metric_recorders(
     default_actor: lambda { |record| },
     trackers: {
       on_create: {
         event: 'org_clc_created'
       },
       on_delete: {
         event: 'org_clc_deleted'
       },
       on_edit: {
         event: 'org_clc_edited'
       }
     }
   )
end

# == Schema Information
#
# Table name: xapi_activities
#
#  id                :integer          not null, primary key
#  actor_id          :integer
#  object_id         :integer
#  object_type       :string(255)
#  object_definition :string(255)
#  organization_id   :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  verb              :string(191)
#  verb_identifier   :string(191)
#

class XapiActivity < ActiveRecord::Base
  VERB_SHARED = 'shared'.freeze
  VERB_COMPLETED = 'completed'.freeze
  VERB_PREFERRED = 'preferred'.freeze
  VERB_RESPONDED = 'responded'.freeze
  VERB_AUTHORED = 'authored'.freeze
  VERBS_IRI_MAP = {
    XapiActivity::VERB_COMPLETED => 'http://adlnet.gov/expapi/verbs/completed',
    XapiActivity::VERB_SHARED => 'http://adlnet.gov/expapi/verbs/shared',
    XapiActivity::VERB_PREFERRED => 'http://adlnet.gov/expapi/verbs/preferred',
    XapiActivity::VERB_RESPONDED => 'http://adlnet.gov/expapi/verbs/responded',
    XapiActivity::VERB_AUTHORED =>'http://activitystrea.ms/schema/1.0/author'
  }.freeze
  ACTIVITY_STREAM_TO_VERBS_MAP = {
    ActivityStream::ACTION_CREATED_CARD => XapiActivity::VERB_SHARED,
    ActivityStream::ACTION_CREATED_PATHWAY => XapiActivity::VERB_SHARED,
    ActivityStream::ACTION_CREATED_LIVESTREAM => XapiActivity::VERB_SHARED,
    ActivityStream::ACTION_UPVOTE => XapiActivity::VERB_PREFERRED,
    ActivityStream::ACTION_COMMENT => XapiActivity::VERB_RESPONDED,
    ActivityStream::ACTION_SMARTBITE_COMPLETED => XapiActivity::VERB_COMPLETED,
    ActivityStream::ACTION_CHANGED_AUTHOR => XapiActivity::VERB_AUTHORED
  }.freeze

  belongs_to :object, polymorphic: true
  belongs_to :actor, class_name: 'User', foreign_key: :actor_id
  belongs_to :organization

  validates_presence_of :verb, :object, :actor, :object_definition, :organization

  before_validation :set_object_definition
  after_commit :push_to_lrs, if: :persisted?

  def set_object_definition
    if object.is_a?(Card) && object.card_type == "pack"
      object_definition = "http://adlnet.gov/expapi/activities/module"
    elsif object.is_a?(Card)
      object_definition = "http://adlnet.gov/expapi/activities/lesson"
    elsif object.kind_of?(VideoStream)
      object_definition = "http://adlnet.gov/expapi/activities/media"
    end
    self.object_definition = object_definition
  end

  def object_path
    host = organization.host
    if object.is_a?(Card) && object.card_type == "pack"
      url = ClientOnlyRoutes.collection_url(object)
      ecl_id = object.ecl_id
    elsif object.is_a?(Card)
      url = ClientOnlyRoutes.show_insight_url(object)
      ecl_id = object.ecl_id
    elsif object.kind_of?(VideoStream)
      url = Rails.application.routes.url_helpers.video_stream_url(object, host: host)
      ecl_id = object.card&.ecl_id
    end
    ecl_id ? add_ecl_id(url, ecl_id) : url
  end

  def add_ecl_id(url, ecl_id)
    "#{url}?ecl_id=#{ecl_id}"
  end

  def lrs_hash
    {
      actor: {
        name: actor.full_name,
        mbox: "mailto:#{actor.email}",
      },
      verb: {
        id: VERBS_IRI_MAP[verb],
        display: { 'en-US': verb }
      },
      object: {
        id: object_path,
        definition: object_detail
      },
      timestamp: created_at
    }
  end


  def object_detail
    {
      name: {"en-US" => object.message || object.title},
      description: {"en-US" => object.title},
      type: object_definition
    }
  end

  def self.create_from_activity_stream(activity_stream)
    verb = ACTIVITY_STREAM_TO_VERBS_MAP[activity_stream.action]
    xapi_activity = XapiActivity.create({
                      actor_id: activity_stream.user_id,
                      verb: verb,
                      verb_identifier: VERBS_IRI_MAP[verb],
                      object: activity_stream.linkable_item,
                      organization_id: activity_stream.organization_id})
  end

  def push_to_lrs
    xapi_credential = organization.xapi_credential
    if xapi_credential.nil?
      log.info "Xapi credentials are missing for organization id #{organization.id}"
    else
      begin
        conn = Faraday.new xapi_credential.end_point
        conn.basic_auth xapi_credential.lrs_login_key, xapi_credential.lrs_password_key
        res = conn.post do |req|
          req.body = self.lrs_hash.to_json
          req.headers['Content-Type'] = "application/json"
          req.headers['X-Experience-API-Version'] = xapi_credential.lrs_api_version
        end
        unless res.success?
          log.error "Failed xapi activity push #{self.id} with status: #{res.status}"
        end
      rescue => e
        log.error "Failed xapi activity push #{self.id} #{e.message}"
      end
    end
  end

end

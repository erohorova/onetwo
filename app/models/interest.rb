# == Schema Information
#
# Table name: tags
#
#  id         :integer          not null, primary key
#  name       :string(191)
#  created_at :datetime
#  updated_at :datetime
#  type       :string(191)
#  mapped     :boolean          default(FALSE)
#

class Interest < Tag
  class << self
    def suggested_channels_based_on_interests_for(user: , interests: )
      channels = Channel.joins(:interests).not_private.where(organization: user.organization).where(tags: {id: interests.map(&:id)}).distinct
      channels.reject {|ch| user.followed_channel_ids.include?(ch.id)}
    end

    def suggested_users_based_on_interests_for(user: , interests: )
      channels = Channel.joins(:interests).includes(:authors).not_private.where(organization: user.organization).where(tags: {id: interests.map(&:id)}).distinct
      channel_collaborators = channels.map(&:authors).flatten

      users_with_similar_interests = User.joins(:interests).where(organization: user.organization).where(tags: {id: interests.map(&:id)}).distinct
      (channel_collaborators | users_with_similar_interests).reject {|u| u.id == user.id || user.followed_user_ids.include?(u.id)}
    end
  end   
end

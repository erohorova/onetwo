class ActivityRecord

  include Elasticsearch::Persistence::Model

  self.index_name = "activity_record_#{Rails.env}_v4"

  attribute :user_id, Integer
  attribute :group_id, Integer
  attribute :type, String
  attribute :date, String

  def self.make_primary_key(group_id:nil, user_id:nil, date:nil, type:nil)
    "#{type}-group-id-#{group_id}-user-id-#{user_id}-date-#{date}"
  end

  def self.create_with_id(group_id:nil, user_id:nil, date:nil, type:nil)
    id = self.make_primary_key(group_id: group_id, user_id: user_id, date: date, type: type)
    ar = self.new(id: id, user_id: user_id, date: date, type: type, group_id: group_id)
    ar.save
  end

  def self.get_current_time_utc
    Time.now.utc
  end
end

# == Schema Information
#
# Table name: daily_group_stats
#
#  id                              :integer          not null, primary key
#  group_id                        :integer          not null
#  date                            :date
#  total_users_with_posts          :integer          default(0)
#  total_users_with_comments       :integer          default(0)
#  total_active_forum_users        :integer          default(0)
#  total_users_with_events         :integer          default(0)
#  total_users_with_event_response :integer          default(0)
#  total_active_events_users       :integer          default(0)
#  total_posts                     :integer          default(0)
#  total_unanswered_posts          :integer          default(0)
#  total_answered_posts            :integer          default(0)
#  total_new_events                :integer          default(0)
#  created_at                      :datetime
#  updated_at                      :datetime
#  trending_tags                   :text(16777215)
#  total_comments                  :integer          default(0)
#  total_active_mobile_forum_users :integer          default(0)
#  trending_contexts               :text(16777215)
#

class DailyGroupStat < ActiveRecord::Base

  belongs_to :group
  validates :date, :uniqueness => {:scope => :group_id}

  def active_day?
    stats_attributes = %w(id group_id created_at updated_at date)
    DailyGroupStat.column_defaults.except(*stats_attributes) != attributes.except(*stats_attributes)
  end

  def self.get_daily_stats(group_id, date, num_days)
    from = (date-num_days.days).strftime('%Y-%m-%d')
    to = date.strftime('%Y-%m-%d')
    active_stats = where('group_id = :group_id and date > :from and date <= :to',
                         group_id: group_id, from: from, to: to).order(date: :asc).
        to_a.each_with_object({}){ |c,h| h[c.date.to_s] = c }
    (0..num_days-1).each do |i|
      d = (date-i.days).strftime('%Y-%m-%d')
      if active_stats[d].nil?
        active_stats[d] = DailyGroupStat.new(group_id: group_id, date: (date-i.days))
      end
    end
    active_stats.values.sort{|a,b| a.date <=> b.date}
  end

end

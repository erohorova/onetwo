# == Schema Information
#
# Table name: identity_providers
#
#  id            :integer          not null, primary key
#  type          :string(191)
#  uid           :string(191)
#  user_id       :integer
#  token         :string(4096)
#  secret        :string(255)
#  expires_at    :datetime
#  created_at    :datetime
#  updated_at    :datetime
#  token_scope   :string(255)
#  auth          :boolean
#  auth_info     :text(65535)
#  refresh_token :string(255)
#

class SalesforceuserProvider < IdentityProvider
  def self.is_verified?(auth_hash)
    true #salesforce developer accounts are always verified
  end
end

# frozen_string_literal: true

# == Schema Information
#
# Table name: teams_cards
#
#  id         :integer          not null, primary key
#  team_id    :integer
#  card_id    :integer
#  type       :string(10)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#

class SharedCard < TeamsCard
  include EventTracker

  belongs_to :team
  belongs_to :card

  before_save :set_user, on: :create

  configure_metric_recorders(
    default_actor: :user,
    trackers: {
      on_create: {
        event: 'card_shared',
        job_args: [ { card: :card }, { team_id: :team_id } ]
      }
    }
  )

  def set_user
    self.user_id = card.current_user.id if card.current_user.present?
  end
end

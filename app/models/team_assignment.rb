# == Schema Information
#
# Table name: team_assignments
#
#  id            :integer          not null, primary key
#  assignment_id :integer
#  team_id       :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  assignor_id   :integer
#  message       :string(255)
#

# Misnomer, This is really an assignment-assignor join table. Multiple people can assign the same piece of content
# to a user, hence this join table. team_id is optional. Only populated when the assignment comes in the context of a team
class TeamAssignment < ActiveRecord::Base
  belongs_to :team
  belongs_to :assignment
  belongs_to :assignor, foreign_key: 'assignor_id', class_name: 'User'

  # All callbacks of team assignments handled in BulkAssignService, please change corresponding
  # methods in BulkAssignService if you are adding or changing callbacks
  after_commit :generate_email_notification, on: :create
  after_commit :generate_client_notification, on: :create
  

  attr_accessor :skip_email_notifications

  private

  def generate_email_notification
    return if (self.assignment.user_id == self.assignor_id || self.skip_email_notifications)
    UserMailer.assignment_notification(self.team, self.assignment, self.assignor, self.message).deliver_later if self.assignment.assignee.pref_notification_assignment
  end

  # Create web/mobile in-app notifications when assignment assigned to user
  def generate_client_notification
    return if self.assignment.user_id == self.assignor_id
    UserTeamAssignmentNotificationJob.perform_later({team_assignment_id: id, notification_type: Notification::TYPE_ASSIGNED})
  end

  def self.assigned_teams(q:, assignor_id: nil, content_id:)
    query = Team.joins(team_assignments: :assignment)
      .where(assignments: {assignable_id: content_id, assignable_type: 'Card'})
    
    if assignor_id
      query = query.where("team_assignments.assignor_id = ?", assignor_id)
    end

    if q.present?
      query = query.where("teams.name like :q", q: "%#{q}%")
    end
    
    query.select("DISTINCT(teams.id), teams.name")
  end
end

# == Schema Information
#
# Table name: roles
#
#  id              :integer          not null, primary key
#  name            :string(191)
#  organization_id :integer
#  master_role     :boolean          default(FALSE)
#  created_at      :datetime
#  updated_at      :datetime
#  image_url       :string(255)
#  threshold       :integer
#

class Role < ActiveRecord::Base
  MASTER_ROLES = %w[sme admin member].freeze
  TYPE_SME = 'sme'.freeze
  TYPE_CURATOR = 'curator'.freeze
  TYPE_ADMIN = 'admin'.freeze
  TYPE_MEMBER = 'member'.freeze
  TYPE_COLLABORATOR = 'collaborator'.freeze
  TYPE_TRUSTED_COLLABORATOR = 'trusted_collaborator'.freeze
  TYPE_GROUP_LEADER = 'group_leader'.freeze
  TYPE_GROUP_ADMIN = 'group_admin'.freeze
  STANDARD_ROLES = %w[admin member curator collaborator group_leader
                      group_admin trusted_collaborator]

  # =========================================
  # EVENT RECORDER CONFIG

  include EventTracker

  recorder_event_opts = -> (role) {
    Analytics::MetricsRecorder.role_attributes(role)
  }

  configure_metric_recorders(
    default_actor: -> (record) { },
    trackers: {
      on_create: {
        event: 'org_role_created',
        exclude_job_args: [:role],
        event_opts: recorder_event_opts
      },
      on_delete: {
        event: 'org_role_deleted',
        exclude_job_args: [:role],
        event_opts: recorder_event_opts
      }
    }
  )
  # =========================================


  before_destroy :can_be_destroyed?
  before_validation :set_default_name
  after_commit :reindex_users, on: :update, if: :should_reindex?

  belongs_to :organization

  has_many :user_roles, dependent: :destroy
  has_many :users, through: :user_roles

  has_many :role_permissions, dependent: :destroy
  has_many :enabled_role_permissions, -> { where(enabled: true) }, class_name: 'RolePermission', foreign_key: :role_id

  validates :name, :organization, presence: true
  validates :default_name, uniqueness: { scope: :organization_id, case_sensitive: false }
  validates :name, uniqueness: { scope: [:default_name, :organization_id], case_sensitive: false }

  scope :admins, -> { where(default_name: TYPE_ADMIN) }
  scope :members, -> { where(default_name: TYPE_MEMBER) }
  scope :sme, -> { where(default_name: TYPE_SME) }
  scope :curators, -> { where(default_name: TYPE_CURATOR) }
  scope :collaborators, -> { where(default_name: TYPE_COLLABORATOR) }
  scope :trusted_collaborators, -> { where(default_name: TYPE_TRUSTED_COLLABORATOR) }
  scope :group_leaders, -> { where(default_name: TYPE_GROUP_LEADER) }
  scope :group_admins, -> { where(default_name: TYPE_GROUP_ADMIN) }

  def get_permissions
    enabled_role_permissions.map(&:name)
  end

  def update_permissions(permissions)
    valid_permissions = Permissions.filter_valid_permissions(permissions)

    clear_permissions!
    valid_permissions.each do |p|
      role_permissions.create(name: p)
    end
  end

  def clear_permissions!
    role_permissions.delete_all
  end

  def should_reindex?
    previous_changes.keys.include?('name')
  end

  def reindex_users
    BulkPartialReindexUserRolesJob.perform_later(org_id: organization_id, id: id)
  end

  def add_user(user)
    user_roles.create(user: user)
  end

  def remove_user(user)
    user_roles.where(user: user).destroy_all
  end

  def self.create_master_roles(organization)
    MASTER_ROLES.each do |role_name|
      role = Role.where(
        organization: organization, default_name: role_name,
        name: role_name, master_role: true
      ).first_or_create
      role.create_role_permissions
    end
  end

  def self.create_standard_roles(organization)
    Role::STANDARD_ROLES.each do |role_name|
      role = Role.where(
        organization: organization, default_name: role_name,
        name: role_name, master_role: true
      ).first_or_create
      role.create_role_permissions
    end
  end

  def create_role_permissions
    permissions = Permissions.get_permissions_for_role(default_name) || {}
    permissions.each do |key, value|
      role_permission = RolePermission.where(role: self, name: value[:slug]).first_or_initialize
      if role_permission.new_record?
        role_permission.description = value[:name]
        role_permission.save
      end
    end
  end

  private

  def can_be_destroyed?
    def_name = default_name.presence || name.downcase
    return unless def_name.in?(STANDARD_ROLES | MASTER_ROLES)
    errors.add(:base, 'You can not delete standard role')
    false
  end

  def set_default_name
    standard_roles = ['influencer'] + STANDARD_ROLES | MASTER_ROLES
    return if default_name && default_name.in?(standard_roles)
    self.default_name = name.downcase
  end
end

class WorkflowPatchRevision < ActiveRecord::Base

  AVAILABLE_VERSIONS = [0, 2].freeze
  BATCH_LIMIT = 5000.freeze
  VALID_PROCESSING_INFO_ACTIONS = {
    'update': %w[info errors]
  }

  serialize :meta, JSON
  serialize :params, JSON
  serialize :data, JSON
  serialize :processing_info, JSON

  belongs_to :organization

  after_commit :push_patch_job, on: :create, if: ->{ !failed && wuid.present? }

  scope :failed_patches, ->(since_date = '2019-02-10') { where(<<-SQL.strip_heredoc, since_date) }
    failed = true AND patch_type IN ('groups', 'users_groups') AND created_at >= ?
  SQL

  scope :outgoing_patches, ->{ where(<<-SQL.strip_heredoc).order(id: :asc) }
    patch_type IN ('upsert', 'alter-schema', 'delete') AND 
    wuid IS NULL AND 
    completed = false AND 
    in_progress = false
  SQL

  VALID_PROCESSING_INFO_ACTIONS.each do |key, value|
    value.each do |val|
      define_method("#{key}_#{val}") do |payload|
        raise 'Argument should be a Hash' unless payload.is_a?(Hash)
        if processing_info&.key?(val) && processing_info[val].is_a?(Array)
          processing_info[val].push(payload)
        else
          processing_info&.merge({val => [payload]})
        end

        self.processing_info = {val => [payload]} unless processing_info.is_a?(Hash)
        self.failed = true if val == 'errors'
        self.save
      end
    end
  end

  def set_processing(job_id)
    self.update_attributes(
      in_progress: true,
      job_id: job_id,
      processing_info: {info: [{started_at: Time.current}], errors: []},
      completed: false,
      failed: false
    )
  end

  private

  def push_patch_job
    version = self.version.to_s
    case version
    when '0'
      WorkflowsJob.perform_later(patch_id: self.id)
    when '2'
      WorkflowsV2Job.perform_later(patch_ids: [self.id])
    else
      nil
    end
  end
end

# == Schema Information
#
# Table name: organization_level_metrics
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  user_id         :integer
#  action          :string(191)
#  content_type    :string(191)
#  events_count    :integer          default(0)
#  period          :string(191)
#  offset          :integer
#

class OrganizationLevelMetric < ActiveRecord::Base
  include MetricsAggregator

  belongs_to :user
  belongs_to :organization

  create_increment_methods([:events_count])
  create_getter_methods([:events_count])

end

# == Schema Information
#
# Table name: pwc_records
#
#  id         :integer          not null, primary key
#  skill      :text(65535)
#  level      :integer
#  user_id    :integer
#  abbr       :string(5)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class PwcRecord < ActiveRecord::Base
  serialize :skill

  belongs_to :user
  validates :user_id, :skill, :level, :abbr, presence: true
end

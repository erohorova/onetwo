# == Schema Information
#
# Table name: video_streams
#
#  id                         :integer          not null, primary key
#  creator_id                 :integer
#  group_id                   :integer
#  start_time                 :datetime
#  name                       :string(255)
#  uuid                       :string(191)      not null
#  status                     :string(191)
#  created_at                 :datetime
#  updated_at                 :datetime
#  publish_url                :string(255)
#  playback_url               :string(1024)
#  password                   :string(255)
#  x_id                       :string(255)
#  type                       :string(255)
#  comments_count             :integer          default(0)
#  votes_count                :integer          default(0)
#  slug                       :string(191)
#  last_stream_at             :datetime
#  thumbnail_file_resource_id :integer
#  anon_watchers_count        :integer          default(0)
#  organization_id            :integer
#  state                      :string(191)
#  is_official                :boolean          default(FALSE)
#  width                      :integer
#  height                     :integer
#  card_id                    :integer
#

class VideoStream < ActiveRecord::Base
  extend FriendlyId

  include EdcastSearchkickWrapper
  include AASM

  acts_as_paranoid

  # live = currently broadcasting
  # upcoming = future broadcast
  # past = completed broadcast
  # pending =  pending broadcast while creating the broadcast,
  #           will convert to live when the broadcast provider is ready to start broadcasting
  VIDEO_STREAM_STATUS = ['live', 'upcoming','past', 'pending']

  EXCLUDED_ATTRIBUTES_FROM_CLONNING = [:uuid, :creator_id, :comments_count, :votes_count, :last_stream_at,
    :anon_watchers_count, :organization_id]

  ACTION_VIEW = 1
  ACTION_LIKE = 5
  ACTION_COMMENT = 5

  attr_accessor :partial_update, :update_hash, :topics, :provider, :recording

  ## BEGIN Org association
  # Validations ensure that
  # (a) Org is present
  # (b) Creator belongs to the org
  #
  # Before validation hooks to
  # (a) associate stream to org through the user if org not explicitly associated.
  # This is to avoid touching all parts of the sytem where streams are currently
  # created
  belongs_to :organization
  belongs_to :card, inverse_of: :video_stream, dependent: :destroy
  belongs_to :creator, class_name: 'User', foreign_key: :creator_id
  has_many :channels_cards, through: :card
  has_many :teams_cards, through: :card
  has_many :notifications, as: :notifiable, :dependent => :destroy
  has_many :video_streams_users, foreign_key: :video_stream_id, dependent: :destroy
  has_many :watchers, through: :video_streams_users, class_name: 'User', foreign_key: :user_id, source: :user
  has_many :recordings, ->{ where(transcoding_status: 'available').order('sequence_number ASC') }

  belongs_to :thumbnail_file_resource, class_name: "FileResource", foreign_key: 'thumbnail_file_resource_id'

  before_validation :assign_uuid, on: :create
  before_validation :create_card, on: :create
  before_validation :update_card, on: :update
  before_validation on: :create do
    if self.organization_id.nil?
      self.organization_id = creator.try :organization_id
    end
  end
  before_validation on: :create do
    self.state = 'published' if self.state.nil?
  end
  ## BEGIN - CMS State code. CMS users can delete published video streams
  validates :state, inclusion: ['draft', 'published', 'deleted']
  validates :organization, :card, presence: true
  validate :user_in_org
  validates :uuid, presence: true, uniqueness: true
  validates :start_time, presence: true
  validates :name, presence: true
  validates :status, inclusion: VIDEO_STREAM_STATUS

  before_create :set_type

  after_create :create_recording

  after_commit :schedule_reminder_push_notification, on: :create
  after_commit :create_index, on: :create
  after_commit :update_index, on: :update
  after_commit :delete_index, on: :destroy
  after_commit :remove_assignments, on: :destroy
  after_commit :remove_user_content_completion, on: :destroy
  after_commit :update_card_state, on: :update

  after_commit(on: :destroy) do
    Notification.where(notifiable: self).destroy_all
    UserContentsQueue.dequeue_content(
        content_id: self.id,
        content_type: 'VideoStream'
    )
  end

  delegate :add_to_learning_queue, :complete_learning_queue_item, :create_assignment_for_individuals,
    :create_assignment_for_teams, :mark_learning_queue_items_as_deleted, to: :card

  scope :live, ->{where(status: 'live')}
  scope :upcoming, ->{where(status: 'upcoming')}
  scope :past, ->{where(status: 'past')}
  scope :pending, ->{where(status: 'pending')} #while we creating the video stream
  scope :live_or_upcoming, ->{where(status: ['live', 'upcoming'])}
  scope :published, -> {where state: 'published'}
  scope :drafts, -> {where state: 'draft'}
  scope :promoted, -> {where is_official: true}

  friendly_id :slug_candidates, use: [:slugged, :history, :scoped], scope: :organization_id

  aasm column: :status do
    state :live
    state :upcoming
    state :past
    state :pending, initial: true

    event :start do
      before do
        self.last_stream_at = Time.current
      end

      after do
        CreateLiveStreamNotificationJob.perform_later(video_stream_id: self.id)
      end
      transitions from: [:pending, :upcoming], to: :live
    end

    event :stop do
      transitions from: :live, to: :past
    end
  end

  def self.exact_match_fields
    ['tags.name', 'state', 'status']
  end

  def self.sortable_fields
    ['name']
  end

  def self.searchable_fields
    {
      'tags.name' => {
        boost: 15,
      },
      'creator.name' => {
        boost: 5
      },
      'name' => {
        boost: 10
      },
    }
  end

  edcast_searchkick

  #setting subclass of VideoStream
  def set_type
    self.type = get_video_stream_class.name if type.nil?
  end

  def create_card(org_id: nil)
    unless card
      card = Card.new(
        title: name,
        message: name,
        author_id: creator_id,
        organization_id: org_id || organization_id,
        state: state,
        card_type: 'video_stream',
        card_subtype: 'simple',
        is_official: is_official,
        topics: topics,
        is_public: true
      )

      self.card = card
    end
  end

  def update_card
    if !card
      create_card
    elsif name_changed? || is_official_changed? || topics
      card.title = name
      card.topics = topics
      card.is_official = is_official
      return true if card.save
      # add appropriate error
      errors.add(:card, card.errors.full_messages.join(', '))
    end
  end

  def create_card_tags
    card.tags = tags
  end

  def create_file_resource_for_card(fr)
    if fr
      fr.update(attachable: card)
      card.reload
    end
  end

  def update_file_resource(fr)
    card.file_resources.destroy_all if card.file_resources
    create_file_resource_for_card(fr)
  end

  def get_video_stream_class
    if provider == 'red5pro'
      provider_klass = Red5ProVideoStream
    elsif provider == 'wowza'
      provider_klass = WowzaVideoStream
    else
      provider_klass = IrisVideoStream
    end
    provider_klass
  end

  def user_in_org
    if creator && self.organization_id
      errors.add(:organization_id, "Creator does not belong to organization") unless creator.organization_id == self.organization_id
    end
  end

  def publish!
    update_attributes(state: 'published')
    # handled separately in update_card_state method
    # card.publish
    card.add_to_activity_stream(card, self.activity_initiator, ActivityStream::ACTION_CREATED_LIVESTREAM )
  end

  def published?
    state == 'published'
  end

  def draft?
    state == 'draft'
  end

  def delete_from_cms!
    ActiveRecord::Base.transaction do
      update_attributes(state: 'deleted')
        #soft delete
      card.destroy  if card.present? && card.persisted?
    end
  end

  def deleted?
    state == 'deleted'
  end
  ## END - CMS State code

  def assign_uuid
    if self.uuid.nil?
      self.uuid = SecureRandom.uuid
    end
  end

  def publish_token
    time_now = Time.now.to_i
    token = Digest::SHA256.hexdigest([Edcast::Application.config.secret_key_base, self.uuid, time_now].join('|'))
    "#{token}-#{time_now.to_i}"
  end

  # override default searchkick async reindex, which needs a newer ActiveJob version
  def reindex_async
    IndexJob.perform_later(self.class.name, self.id)
  end

  def empty_update_data
    empty_data = {
      views_count: 0,
      weekly_score:0,
      weekly_score_valid_till: Time.now + 1000.years,
      monthly_score:0,
      monthly_score_valid_till: Time.now + 1000.years,
      quarterly_score:0,
      quarterly_score_valid_till: Time.now + 1000.years,
      all_time_score:0
    }
  end

  def creator_hash
    {
        'id' => creator_id,
        'first_name' => creator.first_name,
        'last_name' => creator.last_name,
        'name' => creator.name,
        'photo' => creator.photo,
        'normalized_handle' => creator.handle.nil? ? nil : creator.handle.downcase,
        'handle' => creator.handle.nil? ? nil : creator.handle.downcase,
        'roles' => creator.roles.collect(&:name),
        'organization_role' => creator.organization_role
    }
  end

  def search_data
    addition_data = {}

    if creator
      addition_data['creator'] = creator_hash
    end

    if card
      addition_data['card_slug'] = card.slug
    end

    if image
      addition_data['image'] = image
    end

    if tags.present?
      addition_data['tags'] = tags.map do |tag|
        {'id' => tag.id, 'name' => tag.name}
      end
    end
    if channels
      addition_data['channel_ids'] = channel_ids
      addition_data['channels'] = channels.as_json(only: [:id, :label])
    end

    # Recording data
    addition_data['past_stream_available?'] = past_stream_available?
    addition_data['default_recording'] = default_recording.as_json(only: [:id, :url, :transcoding_status, :mp4_location, :hls_location])
    addition_data['recordings'] = recordings.as_json(only: [:id, :url, :transcoding_status, :mp4_location, :hls_location])
    as_json.merge(addition_data)
  end

  def channel_ids
    channels.map(&:id).uniq
  end

  def default_recording
    recordings.default.first
  end

  def slug_candidates
    uuid = "videostream-#{SecureRandom.uuid}"
    unless name.nil?
      cand = name.gsub(/\s+/, '-')
      return [cand, "#{cand}-#{SecureRandom.uuid}", uuid]
    end
    return [uuid]
  end

  # prevent db column overrun
  def normalize_friendly_id(text)
    super[0..250]
  end

  def _taggable_fields
    [name]
  end

  def schedule_reminder_push_notification
    if status == 'upcoming' && start_time > Time.now.utc + 10.minutes
      if card.organization.notifications_and_triggers_enabled?
        ScheduleStreamNotificationJob.set(wait_until: start_time - 10.minutes).perform_later({card_id: card.id})
      else
        NotificationGeneratorJob.set(wait_until: start_time - 10.minutes).perform_later({item_id: id, item_type: 'video_stream', notification_type: 'scheduled_stream_reminder'})
      end
    end
  end

  after_commit :generate_going_live_notification, on: :update

  def generate_going_live_notification
    if !self.draft? && (self.previous_changes['status'] == ['upcoming', 'live'] || self.previous_changes['status'] == ['pending', 'live'])
      EnqueueStreamsJob.perform_later({id: id})
      NotificationGeneratorJob.perform_later(item_id: id, item_type: 'video_stream', notification_type: 'stream_started')
    end
  end

  after_commit :enqueue_stream_on_publishing_draft, on: :update

  def enqueue_stream_on_publishing_draft
    if self.previous_changes['state'] == ['draft', 'published']
      EnqueueStreamsJob.perform_later({id: id})
    end
  end

  after_commit :process_past_stream, on: :update

  def process_past_stream
    if self.previous_changes['status'] == ['live', 'past']
      PubnubGetHistoryJob.perform_later(self.id)
      if self.is_a?(WowzaVideoStream)
        WowzaVideoStreamServerUploadJob.perform_later({video_stream_id: self.id})
      elsif self.is_a?(IrisVideoStream)
        IrisVideoStreamServerUploadJob.set(wait: 15.seconds).perform_later({video_stream_id: self.id})
      else
        Red5ProVideoStreamServerUploadJob.perform_later({video_stream_id: self.id})
      end
    end
  end

  def image(size=nil)
    if file_resource
      file_resource.try(:file_url, size)
    elsif past_stream_available?
      thumbnail_file_resource.try(:file_url, size)
    end
  end

  def card_slug
    card&.slug
  end

  def image_attachment
    file_resource.try(:attachment) || thumbnail_file_resource.try(:attachment)
  end

  def fetch_image_attachment_url
    image_attachment.try(:url)
  end

  def mobile_photo
    image(:small)
  end

  def update_default(thumbnail)
    thumbnail_file_resource = self.thumbnail_file_resource || FileResource.new
    thumbnail_file_resource.attachment = thumbnail.uri
    thumbnail_file_resource.save
    self.thumbnail_file_resource = thumbnail_file_resource
    self.save
  end

  def group
    #does nothing. need this for the comments controller
  end

  def can_comment?(user)
    true #anyone can comment
  end

  def snippet
    markdown = Redcarpet::Markdown.new(Redcarpet::Render::StripDown)
    markdown.render(name).gsub(/\n/,'').truncate(60, separator: ' ')
  end

  def relevant_channels
    channels
  end

  def publicly_visible?
    channels.blank? || channels.reject{|ch| ch.is_private}.any?
  end

  def past_stream_available?
    status == 'past' && recordings.default.first && (!recordings.default.first.hls_location.nil? || !recordings.default.first.mp4_location.nil?)
  end

  # DEPRECATED
  def has_access? _user
    (organization.open? || _user.try(:organization_id) == organization_id) && (channels.empty? || (channels & Channel.readable_channels_for(_user, _organization: organization, onboarding: false)).any?)
  end

  def can_be_accessed_by? _user
    channel_ids.empty? || (channel_ids & Channel.fetch_eligible_channels_for_user(user: _user).pluck(:id)).any?
  end

  def provider_name
    'base'
  end

  def initialize_score
  end

  def self.index_name
    self.searchkick_index.name
  end

  def elasticsearch_client
    return ES_SEARCH_CLIENT
  end

  def create_index
    data = search_data.merge(empty_update_data)
    data = data.merge(update_hash) if partial_update
    elasticsearch_client.create index: VideoStream.index_name,  type: self.type.underscore, id: self.id, body: data
  end

  def reindex
    update_index
  end

  def update_index
    begin
      if partial_update
        elasticsearch_client.update index: VideoStream.index_name,  type: self.type.underscore, id: self.id, body: {doc: update_hash}
      else
        elasticsearch_client.update index: VideoStream.index_name,  type: self.type.underscore, id: self.id, body: {doc: search_data}
      end
    rescue Elasticsearch::Transport::Transport::Errors::NotFound => e
      log.error "VideoStream update index failure handled #{e.message}"
      create_index
    end
  end

  def delete_index
    elasticsearch_client.delete index: VideoStream.index_name,  type: self.type.underscore, id: self.id
  end

  def update_score
    action_score = get_action_score(type)
    elasticseach_object = Search::VideoStreamSearch.new.search_by_id(self.id)
    update_hash =  {}
    {weekly_score: 7, monthly_score: 30, quarterly_score: 90}.each do |score_type, valid_till_days_value|
      score_update = get_score_update_in_time(valid_till_days_value.days.ago)
      update_hash[score_type] = score_update[:score]
      update_hash[score_type.to_s + "_valid_till"] =  score_update[:oldest_event_time] + valid_till_days_value.days
    end
    update_hash[:all_time_score] = MetricRecord.video_stream_view_count(self, nil) * ACTION_VIEW + votes_count * ACTION_LIKE + comments_count * ACTION_COMMENT
    update_data_in_elastic_search(update_hash)
  end

  def get_score_update_in_time(time)
    like_count = Vote.where(votable:self).where(['created_at >= ?', time]).count
    comment_count = Comment.where(commentable:self).where(['created_at >= ?', time]).count
    view_count = MetricRecord.video_stream_view_count(self, time)
    score = view_count  * ACTION_VIEW + like_count * ACTION_LIKE + comment_count * ACTION_COMMENT
    oldest_view_time = view_count == 0 ? Time.zone.now : MetricRecord.oldest_view(self, time)
    oldest_vote_time = like_count == 0 ? Time.zone.now : Vote.where(votable:self).where(['created_at >= ?', time]).order('created_at').first.created_at
    oldest_comment_time = comment_count == 0 ? Time.zone.now : Comment.where(commentable:self).where(['created_at >= ?', time]).order('created_at').first.created_at
    oldest_event_time = [oldest_view_time, oldest_vote_time, oldest_comment_time].min
    { score: score, oldest_event_time: oldest_event_time }
  end

  def update_views_count
    video_stream = Search::VideoStreamSearch.new.search_by_id(id)
    views_count = video_stream[:views_count].to_i + 1
    update_data_in_elastic_search({views_count: views_count})
  end

  def update_data_in_elastic_search(update_hash)
    self.partial_update = true
    self.update_hash = update_hash
    self.update_index
    self.partial_update = false
  end

  def get_action_score(type)
    return type
  end

  def enqueue(user, rationale, timestamp, flush=true)
    UserContentsQueue.enqueue_bulk_for_user(
        user_id: user.id,
        content_type: 'VideoStream',
        items: [{
          content_id: self.id,
          tags: self.tags.map(&:name),
          channel_ids: self.channels.map(&:id),
          metadata: { 'rationale' => rationale }
        }],
        queue_timestamp: timestamp,
        merge_metadata: false,
        flush: flush
    )
  end

  def is_stream_worthy?
    (status != 'pending') && state == 'published' && publicly_visible? && !creator.nil?
  end

  def activity_initiator
    self.creator
  end

  def activity_stream_action
    ActivityStream::ACTION_CREATED_LIVESTREAM
  end

  def invalidate_score
    update_score
  end

  def vote_count_for_month(month_identifier)
    MonthlyTopVideoStream.find_for_video_stream_and_month(self, month_identifier).try(:votes_count).to_i
  end

  def end_time
    start_time.is_a?(String) ? Time.parse(start_time) : start_time + 15.minutes if status == "upcoming"
  end

  def get_app_deep_link_id_and_type
    [id, 'video_stream']
  end

  def get_recording_sequence_number
    Recording.where(video_stream_id: self.id).count
  end

  def get_presigned_post_object
    bucket_name = Settings.send(self.provider_name.to_sym).s3_bucket
    video_stream_s3_bucket = AWS::S3.new.buckets[bucket_name]

    recording = Recording.new(sequence_number: get_recording_sequence_number, video_stream_id: self.id, source: 'client')

    s3_key = "#{Settings.send(self.provider_name.to_sym).s3_upload_prefix}/#{self.id}/server/#{self.uuid}-#{Time.now.utc.to_i}.mp4"
    recording.bucket = bucket_name
    recording.key = s3_key
    recording.location = "https://#{bucket_name}.s3.amazonaws.com/#{s3_key}"
    recording.skip_transcoding = true
    recording.save!

    recording_upload = video_stream_s3_bucket.presigned_post(key: recording.key, success_action_status: 201, acl: :public_read).where(:content_type).starts_with('')
    [recording, recording_upload]
  end

  def title
    name
  end

  def remove_assignments
    Assignment.where(assignable: self).destroy_all
  end

  def remove_user_content_completion
    UserContentCompletion.where(completable: self).destroy_all
  end

  def assignment_url
    Rails.application.routes.url_helpers.video_stream_url(self.slug, host: self.organization.host)
  end

  def bookmarked_by? user
    Bookmark.where(bookmarkable: self.card, user: user).present?
  end

  def create_recording
    if self.recording
      vs_recording = Recording.new(video_stream_id: self.id, source: 'client')
      vs_recording.bucket    = self.recording[:bucket]
      vs_recording.key       = self.recording[:key]
      vs_recording.location  = self.recording[:location]
      vs_recording.checksum  = self.recording[:checksum]
      vs_recording.sequence_number  = self.recording[:sequence_number] || 0
      vs_recording.default = self.recording[:default] || true
      vs_recording.save
    end
  end

  #Delegate all downstream methods to Card
  def channels
    card.try(:channels)
  end

  #expecting array
  def channels=(vs_channels)
    card.channels = vs_channels
  end

  def tags
    card&.tags
  end

  def tags=(vs_tags)
    card.tags = vs_tags
  end

  def file_resource=(fr)
    card.file_resources << fr
  end

  def file_resource
    card.file_resources.first if card.try(:file_resources)
  end

  def comments
    card.try(:comments)
  end

  def up_votes
    card.try(:upvotes)
  end

  def xapi_activities
    card.try(:xapi_activities)
  end

  def activity_streams
    card.try(:activity_streams)
  end

  def learning_queue_items
    card.try(:learning_queue_items)
  end

  def voted_by?(user)
    #TODO To move when we move to Card
    Vote.where(votable: self, voter: user).exists? || try(:card).try(:voted_by?, user)
  end

  def comments_count
    card&.comments_count || 0
  end

  #watch out on cyclic dependency. Need to refactor this to use
  # state machine.
  # see update_video_stream_state method in models/card.
  # or should use state column on card rather than on VS

  def update_card_state
    if card && card.state != self.state
      card.update(state: self.state)
    end
  end

  def add_viewer(user)
    video_streams_users.create(user: user)
  end

end

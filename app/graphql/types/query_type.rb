UserFilterInput = GraphQL::InputObjectType.define do
  name('UserFilter')

  argument :followers, types[types.Int]
  argument :following, types[types.Int]
  argument :user_ids, types[types.Int]
end


Types::QueryType = GraphQL::ObjectType.define do
  name 'Query'

  field :card do
    type Types::CardType

    argument :q, !types.String

    argument :offset, types.Int
    argument :limit, types.Int
    argument :cards_limit, types.Int
    argument :cards_offset, types.Int
    argument :channels_limit, types.Int
    argument :channels_offset, types.Int
    argument :users_limit, types.Int
    argument :users_offset, types.Int

    argument :content_type, types[types.String]
    argument :source_id, types[types.String]
    argument :source_type_name, types[types.String]
    argument :exclude_cards, types[types.String]
    argument :query_type, types.String

    argument :posted_by, UserFilterInput
    argument :liked_by, UserFilterInput
    argument :commented_by, UserFilterInput
    argument :bookmarked_by, UserFilterInput

    argument :from_date, types.String
    argument :till_date, types.String

    argument :sociative, types.String

    resolve ->(obj, args, ctx) { { present: true, args: args.to_h } }
  end
end

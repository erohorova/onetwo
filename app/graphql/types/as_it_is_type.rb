Types::AsItIsType = GraphQL::ScalarType.define do
  name 'Hash'

  coerce_input -> (x) { x }
  coerce_result -> (x) { x }
end

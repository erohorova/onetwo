Types::CardType = GraphQL::ObjectType.define do
  name 'Card'

  field :id, types.Boolean, hash_key: :present
  field :card_type, types.Boolean, hash_key: :present
  field :card_subtype, types.Boolean, hash_key: :present
  field :slug, types.Boolean, hash_key: :present
  field :state, types.Boolean, hash_key: :present
  field :is_official, types.Boolean, hash_key: :present

  field :created_at, types.Boolean, hash_key: :present
  field :updated_at, types.Boolean, hash_key: :present
  field :published_at, types.Boolean, hash_key: :present

  field :title, types.Boolean, hash_key: :present
  field :message, types.Boolean, hash_key: :present
  field :views_count, types.Boolean, hash_key: :present
  field :share_url, types.Boolean, hash_key: :present

  field :ecl_metadata, types.Boolean, hash_key: :present

  field :comments_count, types.Boolean, hash_key: :present
  field :votes_count, types.Boolean, hash_key: :present
  field :voters, types.Boolean, hash_key: :present
  field :channel_ids, types.Boolean, hash_key: :present

  field :average_rating, types.Boolean, hash_key: :present
  field :user_rating, types.Boolean, hash_key: :present
  field :all_ratings, types.Boolean, hash_key: :present

  field :is_upvoted, types.Boolean, hash_key: :present
  field :is_bookmarked, types.Boolean, hash_key: :present
  field :is_assigned, types.Boolean, hash_key: :present
  field :completion_state, types.Boolean, hash_key: :present
  field :filestack, types.Boolean, hash_key: :present

  field :resource, types.Boolean, hash_key: :present
  field :mentions, types.Boolean, hash_key: :present
  field :author, types.Boolean, hash_key: :present
  field :tags, types.Boolean, hash_key: :present
  field :file_resources, types.Boolean, hash_key: :present
  field :channels, types.Boolean, hash_key: :present

  field :pack_cards, types.Boolean, hash_key: :present
  field :poll, types.Boolean, hash_key: :present
  field :video_stream, types.Boolean, hash_key: :present
  field :resource_group, types.Boolean, hash_key: :present
  field :assignment, types.Boolean, hash_key: :present

  field :lxp, Types::AsItIsType, hash_key: :args
end

class UserPagesController < ApplicationController
  include ContentTypeFilter
  include CardSearchHelpers
  include ApplicationHelper
  include EventTracker

  skip_before_filter :authenticate_user!, only: [:public, :handle]
  before_action :set_pagination, only: [:public]
  before_action :read_org_permissions, only: [:public, :channels]
  skip_before_filter :check_onboarding_and_redirect, only: [:handle]
  #skip_before_filter :verify_authenticity_token, only: [:edit_settings]

  respond_to :html

  def notifications
    @user = current_user
    @right_rail_disabled = true
    limit = params.has_key?(:limit) ? params[:limit].to_i : 25
    offset = params.has_key?(:offset) ? params[:offset].to_i : 0

    filters = {}
    query = "widget_deep_link_id IS NULL AND app_deep_link_id IS NOT NULL"

    nquery = query

    uns = UserNotificationRead.find_or_initialize_by(user: @user)
    uns.update_attributes(seen_at: Time.now.utc)
    @unseen_count = 0

    db_notifications = @user.notifications.includes(:notifications_causal_users,:causal_users).where(nquery, filters).order(id: :desc).limit(limit+1).offset(offset).all
    @notifications = db_notifications[0..(limit-1)]

    @preloaded_snippets = Notification.preload_app_deep_link_snippets @notifications
    @has_more = db_notifications.length > limit
    @limit = limit
    respond_to do |format|
      format.inline {
        headers['X-Next-Url'] = url_for(params.merge(offset: offset + limit)) if @has_more
      }
    end

  end

  def channels
    @right_rail_disabled = false
    @page_hopper_enabled = false

    # Referenced from discovery page. Showing all channels
    @view_all_channels = Channel.readable_channels_for(current_user, _organization: current_org, onboarding: false).order(is_promoted: :desc, label: :asc)
    @insights_count = get_channel_insight_count(@view_all_channels)
  end

  def public
    handle = params[:handle]
    handle = '@'+handle unless handle.starts_with?('@')
    redirect_to "#{current_org.home_page}/#{handle}", status: :moved_permanently and return
  end

  def settings
    @avatar_upload = AWS_S3_TEMP_UPLOAD_BUCKET.presigned_post(key: "organization_#{current_user.organization.id}/temp_uploads/#{SecureRandom.uuid}/${filename}", success_action_status: 201, acl: :public_read).where(:content_type).starts_with('')
    @cover_upload = AWS_S3_TEMP_UPLOAD_BUCKET.presigned_post(key: "organization_#{current_user.organization.id}/temp_uploads/#{SecureRandom.uuid}/${filename}", success_action_status: 201, acl: :public_read).where(:content_type).starts_with('')

    template = "#{params[:t] || 'show'}_user"
    d = Dir[File.join(Rails.root.to_s, 'app', 'views', 'user_pages', '*')]
    partials = d.map{|x| File.basename(x).gsub(/(\..*)/,'')}.select{|x| x.ends_with?('_user')}
    @template_to_render = partials.include?(template) ? template : 'show_user'

    @notification_prefs = [
        # {
        #     pref: :pref_notification_content,
        #     title: 'Notifications on your content',
        #     desc: 'Get an email notification for feedback that your network leaves on your content',
        # },
        {
            pref: :pref_notification_follow,
            title: 'Follow notifications',
            desc: 'Get a daily email notification with a list of your new followers',
        },
        # {
        #     pref: :pref_notification_mention,
        #     title: 'Mention notifications',
        #     desc: 'Get an email notification when you\'re mentioned on an insight or comment',
        # },
        {
            pref: :pref_newsletter_opt_in,
            title: 'EdCast Network Announcements',
            desc: 'Get (very occasional) email updates from EdCast on the best and latest from the Open Learning Network',
        },
        {
            pref: :pref_notification_weekly_activity,
            title: 'Weekly Notifications',
            desc: 'Get email updates on top 3 Insights, Video Streams & Pathways of the week',
        },
        #{
        #    pref: :pref_notification_activity_on_content,
        #    title: 'Likes & Comments Notifications',
        #    desc: 'Get email notifications when your Insights are liked, commented on, or added to a pathway.',
        #},

    ]
  end

  def edit_settings
    pp = user_params
    if current_user.update_with_reindex_async(pp)

      old_impersonator_id = session['devise.impersonator']
      bypass_sign_in current_user
      session['devise.impersonatee'] = current_user.name
      session['devise.impersonator'] = old_impersonator_id

      respond_to do |format|
        format.html {
          flash[:success] = 'Successfully updated settings.'
          redirect_to settings_path
        }
        format.json {
          render json: {}, status: :ok
        }
        format.js {
          render js: "window.location.reload()", status: :ok
        }
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = current_user.errors.full_messages.to_sentence
          redirect_to settings_path
        }
        format.json {
          render json: {errors: current_user.errors.full_messages.to_sentence}, status: :unprocessable_entity
        }
        format.js {
          render js: "", status: :unproccessable_entity
        }
      end
    end
  end

  def handle
    if params[:handle].present?
      if current_user && current_user.handle_no_at == params[:handle]
        head :no_content
      elsif (User.where(handle: "@#{params[:handle]}", organization: current_org).any? ||
             User.reserved_handle?("@#{params[:handle]}"))
        head :unprocessable_entity
      else
        head :no_content
      end
    else
      head :unprocessable_entity
    end
  end

  def check_handle
    render js: '', status: :bad_request and return unless params[:handle].present?

    user = User.find_by(handle: "#{params[:handle]}", organization: current_org)
    if user
      record_user_profile_visited_event(user)
      respond_to do |format|
        format.html
        format.json { render :json => {:id => user.id,
                                       :first_name => user.first_name,
                                       :last_name => user.last_name,
                                       :name => user.name,
                                       :handle => user.handle }, status: :ok }
      end
    else
      head :not_found
    end

  end

  private
  def set_pagination
    @limit = params[:limit] || 15
    @offset = params[:offset].to_i # yields 0 if not supplied
  end

  def user_params
    params
      .require(:user)
      .permit(
        :avatar,
        :bio,
        :coverimage,
        :first_name,
        :handle,
        :last_name,
        :password,
        :pref_notification_content,
        :pref_notification_follow,
        :pref_notification_mention,
        :pref_newsletter_opt_in,
        :pref_notification_weekly_activity,
        :pref_notification_activity_on_content,
        :time_in_role
      ).reject {|k, v| (v.blank? && ["handle", "first_name"].include?(k))}
  end

  def unauthenticated_actions
    user_signed_in? ? [] : [:public]
  end

  def get_channel_insight_count(channels)
    insights_count = {}
    channels.each do |channel|
      insights_count[channel.id] = Search::CardSearch.new.count(
          channel,
          current_org,
          viewer: current_user,
          filter_params: {card_type: Card::EXPOSED_TYPES, state: 'published'}
      )
    end
    return insights_count
  end

  def record_user_profile_visited_event(user)
    record_custom_event(
      event: 'user_profile_visited',
      actor: :current_user,
      job_args: [ {visited_profile_user: user} ],
      exclude_job_args: [:event]
    )
  end
end

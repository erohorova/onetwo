class OrganizationsController < ApplicationController
  include EmptyAttachmentPreProcessor
  include SavannahAuthentication
  include OrganizationConcern

  before_action :set_upload_object, only: [:new, :create ]

  before_action :ensure_default_org, only: [:new, :create, :add_user]
  before_action :ensure_not_default_org, only: [:show]

  skip_before_action :authenticate_user!, only: [:add_user, :host_name_check, :new, :create]
  skip_before_action :set_organization_context, only: [:add_user]
  skip_before_action :verify_authenticity_token, only: [:add_user]
  before_action :authenticate_from_savannah!  , only: [:add_user]
  before_action :read_org_permissions, only: [:show]

  # API called by savannah to auto create a user
  # in the default org and the associated org
  def add_user
    user_id = @jwt_payload[:user_id]
    user_email = @jwt_payload[:user_email]
    user_role = @jwt_payload[:user_role]

    default_org = Organization.default_org

    normalized_user_role = (user_role == 'admin' ? 'admin' : 'member')

    user = PartnerAuthentication.get_or_create_user(@jwt_payload, @current_org)

    if @current_org.present? && @current_org.id != default_org.id
      org_user = User.where(email: user_email, organization: @current_org).first
      if org_user.nil?
        org_user = user.clone_for_org(@current_org, normalized_user_role)
      end
      org_user.update_with_reindex_async(organization_role: normalized_user_role, parent_user_id: user.id)
    end
    head :ok and return
  end

  def show
    @organization = @current_org
    @right_rail_disabled = true
    @is_admin = @organization.has_admin_access?(current_user)
=begin
    @collaborating_channels = Channel.includes(:channels_users).where(organization_id: @organization.id, channels_users: {user_id: current_user.id, as_type: 'author'})
    channels = @collaborating_channels
    @followed_channels = current_user.followed_channels.where.not(id: channels.map(&:id)).where(organization_id: @organization.id)
    channels += @followed_channels
    @other_channels = Channel.not_private.where(organization_id: @organization.id).where.not(id: channels.map(&:id))
    channels += @other_channels

    @followers_count = Follow.where(followable_type: 'Channel', followable_id: channels.map(&:id)).group(:followable_id).count
    @insights_count = {}
    channels.each do |channel|
      @insights_count[channel.id] = Search::CardSearch.new.count(
          channel,
          viewer: current_user,
          filter_params: {card_type: Card::EXPOSED_TYPES}
      )
    end
=end
    @courses = @organization.featured_courses
  end

  def new
    @organization = Organization.new
    @right_rail_disabled = true
  end

  def create
    org_attrs = prepare_organization_params(organization_params)
    @organization = Organization.new(org_attrs.except(:is_closed))
    @organization.is_open = !org_attrs[:is_closed]
    if @organization.save
      #add admin user
      new_user = current_user.clone_for_org(@organization, 'admin')

      # These are temp cookies to sign the user in. TODO: replace with something more robust. See organization_context filter
      cookies.signed[:new_org_uid] = {value: new_user.id, expires: 1.hour.from_now, domain: Settings.platform_domain}
      cookies.signed[:new_org_oid] = {value: @organization.id, expires: 1.hour.from_now, domain: Settings.platform_domain}

      redirect_to safe_redirect(@organization.home_page) and return
    else
      @right_rail_disabled = true
      flash[:error] = 'Team is not created! Please enter a valid data.'
      render :new
    end
  end

  def host_name_check
    host_name = params.require(:host_name)
    if Settings.disallowed_hosts.include?(host_name) || (another_org_exists = Organization.where(host_name: host_name).any?)|| !Organization.valid_hostname?(host_name)
      if(another_org_exists)
        head :unprocessable_entity and return
      else
        head :not_acceptable and return
      end
    else
      head :no_content and return
    end
  end

  private

  def unauthenticated_actions
    user_signed_in? ? [] : [:show]
  end

  def organization_params
    params.require(:organization).permit(:is_closed, :name, :description, :image, :mobile_image, :host_name, :co_branding_logo, :is_registrable)
  end

  def set_upload_object
    @image_upload = AWS_S3_TEMP_UPLOAD_BUCKET
                        .presigned_post(
                            key:                   "organization_#{current_user.organization.id}/temp_uploads/#{SecureRandom.uuid}/${filename}",
                            success_action_status: 201,
                            acl:                   :public_read )
                        .where(:content_type)
                        .starts_with('')
  end
end

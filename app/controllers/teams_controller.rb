class TeamsController < ApplicationController
  include ContentTypeFilter
  include LimitOffsetConcern
  include TeamsConcern
  include CardSearchHelpers
  include TrackActions

  has_limit_offset_constraints only: [:show], limit_default: 15

  before_action :set_team, except: %i[new create]
  before_action :authenticate_admin!, only: %i[settings update destroy]
  before_action :redirect_unfriendly, only: %i[show settings]
  before_action :set_upload_object, only: %i[new create settings]

  def show
    # team directory tab
    @is_admin = @team.is_admin?(current_user)
    @right_rail_disabled = true
    @show_follow_all = current_user.show_follow_all_for_team(@team)
  end

  def new
    @team = Team.new
    @right_rail_disabled = true
  end

  def create
    team = Team.new(team_params.merge(organization: current_org))
    if team.save
      team.add_admin current_user
      track_action(team, 'create')
      redirect_to team_path(team)
    else
      flash[:error] = "Error creating team: #{team.errors.full_messages}"
      redirect_to new_team_path
    end
  end

  def settings
    @right_rail_disabled = true
  end

  def update
    if @team.update(team_params)
      redirect_to team_path(@team)
    else
      flash[:error] = "Error updating team: #{@team.errors.full_messages}"
      redirect_to team_settings_path(@team)
    end
  end

  def destroy
    @team.destroy
    redirect_to public_user_page_path(handle: current_user.handle_no_at, tab: :teams)
  end

  private

  def team_params
    params.require(:team).permit(
        :name,
        :description,
        :image,
    )
  end

  def redirect_unfriendly
    if params[:id].to_i == @team.id
      redirect_to url_for(id: @team.slug), status: :moved_permanently
    end
  end

  def set_upload_object
    @image_upload = AWS_S3_TEMP_UPLOAD_BUCKET
                        .presigned_post(
                            key:                   "organization_#{current_user.organization.id}/temp_uploads/#{SecureRandom.uuid}/${filename}",
                            success_action_status: 201,
                            acl:                   :public_read )
                        .where(:content_type)
                        .starts_with('')
  end
end

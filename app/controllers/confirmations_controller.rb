class ConfirmationsController < Devise::ConfirmationsController
  def show
    if params[:app_redirect] == 'true'
      @current_org = Organization.find_by_request_host(request.host)
      redirect_to "#{@current_org.mobile_uri_scheme}://auth/confirmations?confirmation_token=#{params[:confirmation_token]}&host=#{request.host}" and return
    end

    self.resource = resource_class.find_by_confirmation_token(params[:confirmation_token])

    failure and return unless self.resource
    if self.resource && self.resource.confirmed?
      log_in = true
      # sign_in resource
      flash[:notice] = "You've already confirmed your account"
    else
      self.resource = resource_class.confirm_by_token(params[:confirmation_token])
      # don't do anything for a bad token
      failure and return if resource.errors.include?(:confirmation_token)
      log_in =  resource.errors.empty?
      set_flash_message(:notice, :confirmed) if is_flashing_format?
    end

    if log_in
      if resource.is_complete #make the user a completed user
        @access_uri = 'login.email'
      else
        @access_uri = 'signup.email'
        # Current post sign up email is relevant only for www.edcast.com users.
        if resource.organization == Organization.default_org
          DeviseMandrillMailer.send_after_signup_email(resource.id, get_platform, 'email').deliver_later
        end
      end
      sign_in resource
    end

    response.set_cookie("first_sign_in", {value: true, path: "/"}) if(resource.sign_in_count == 1)
    if resource.errors.empty?
      respond_to do |format|
        format.html {
          respond_with_navigational(resource){ redirect_to after_confirmation_path_for(resource_name, resource) } and return
        }
        format.json {
          @user = resource
          render 'users/info' and return
        }
      end
    else
      failure and return
    end
  end

  private

  def failure
    respond_to do |format|
      format.html {
        if resource.present?
          respond_with_navigational(resource.errors, status: :unprocessable_entity){ render :new }
        else
          flash[:error] = 'Invalid Request.'
        end
      }
      format.json {
        head :bad_request
      }
    end
  end
end

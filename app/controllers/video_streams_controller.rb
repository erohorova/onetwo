class VideoStreamsController < ApplicationController

  skip_filter :authenticate_user!, only: [:signed_playback_url]
  before_action :read_org_permissions, only: [:signed_playback_url]

  def signed_playback_url
    video_stream = VideoStream.find_by(params[:id])
    redirect_to safe_redirect(video_stream.signed_playback_url)
  end

  private

  def unauthenticated_actions
    user_signed_in? ? [] : [:show]
  end
end

class ApiController < ActionController::Base
  before_filter :verify_jwt
  include IdentityCacheFilter
  include RequestHandler
  include Entry
  include NativeAppDetection
  include DetectPlatform
  include AccessLogFilter
  include DeviceCookieFilter
  include CurrentAppBuildConcern
  include ApplicationConcern

  resource_description do
    api_version 'Deprecated'
  end
  # Prevent CSRF attacks
  protect_from_forgery with: :null_session

  skip_before_filter :verify_authenticity_token, if: :is_native_app?
  skip_before_filter :verify_authenticity_token, if: :is_using_jwt_token?

  before_action :set_request_store
  before_filter :cm_auth_user, except: :unknown_route, if: -> { !is_using_jwt_token? }
  after_action :add_app_version_for_native_app
  before_filter :set_cache_headers

  def verify_jwt
    if is_using_jwt_token?
      token = request.headers['X-API-TOKEN'] || params["token"]
      @jwt_payload, @jwt_header = JWT.decode(token, Settings.features.integrations.secret)
      @current_user = User.find(@jwt_payload['user_id'])
      @current_org = @current_user.organization
      organization = Organization.find_by_request_host(request.host)
      if((organization != @current_org) || !@current_user.active_for_authentication? )
        respond_to do |format|
          format.html { render "errors/not_found", layout: 'errors', status: :not_found and return }
          format.json { head :not_found and return }
        end
      end
    end
  end

  def client_supports_required_build?(ios_build_number, android_build_number)
    if is_native_app?
      @app_build_number = request.headers['X-Build-Number'].to_i || 0
      platform = native_app_platform
      if (platform == 'android' && @app_build_number > android_build_number) || (platform == 'ios' && @app_build_number > ios_build_number)
        return true
      end
    end
    false
  end

  def add_app_version_for_native_app
    if is_native_app?
      release = CacheFactory.fetch_latest_native_app_release_version(platform: native_app_platform, form_factor: native_app_form_factor)
      if release
        headers['X-App-Version'] = release.version
        headers['X-Latest-Version'] = release.version
        headers['X-Latest-Build'] = release.build
      end
    end
  end

  def authenticate_admin!
    authenticate_user!
    unless current_user.is_admin?
      respond_to do |f|
        f.html { render 'errors/unauthorized', status: 401 and return }
        f.js   { render json: :no_content, status: :unauthorized and return }
        f.json { render json: :no_content, status: :unauthorized and return }
      end
    end
  end

  #authenticate cm admin for org creation from savannah just checking token
  def authenticate_cm_admin!
    if request.format == 'application/json'
      if params[:token].present? && params[:token] == Settings.savannah_token
        @savannah_request = true
        admin_user = User.cm_admin
        @current_user = admin_user
      else
        @savannah_request = false
        authenticate_user!
      end
    else
      authenticate_user!
    end
  end

  # adapted from https://github.com/whitequark/rack-utf8_sanitizer/blob/master/lib/rack/utf8_sanitizer.rb
  def sanitize_invalid_UTF8(str)
    return str unless String === str
    return str if str.dup.force_encoding(Encoding::UTF_8).valid_encoding?
    return str.
        force_encoding(Encoding::ASCII_8BIT).
        encode!(
            Encoding::UTF_8,
            invalid: :replace,
            undef:   :replace,
            # replace: '',
        )
  end

  private

  def admin_only!
    if current_user.nil?
      render json: :no_content, status: :unauthorized and return
    end
    unless current_user.is_admin_user?
      render json: :no_content, status: :unauthorized and return
    end
  end

  def is_using_jwt_token?
    !request.headers['X-Edcast-JWT'].nil?
  end
  # Override this in subclasses if you need custom logic
  # to skip authentication. See lib/entry#cm_auth_user
  # Also see implementation in PostsController and CommentsController
  # for examples
  def visible_without_auth?
    false
  end

  def super_admin_only!
    if current_user.nil? || !current_user.is_admin?
      render json: { error: 'You don\'t have access to do this action.'}, status: :unauthorized and return
    end
  end

  def set_request_store
    payload = {
      user_id:        current_user&.id,
      platform:       get_platform,
      user_agent:     request.headers.env['HTTP_USER_AGENT'],
      platform_version_number: request.headers.env['HTTP_X_VERSION_NUMBER'],
      is_admin_request: ApplicationController.is_admin_request?(request)
    }

    RequestStore.store[:request_metadata] = payload
  end

  def replica_database(&block)
    Octopus.using(:replica1, &block)
  end
end

# deprecated
class Users::ProfileController < ApplicationController

  skip_before_filter :authenticate_user!, only: [:continue]
  skip_before_filter :set_organization_context, only: [:continue]
  before_filter :set_current_org_without_redirects, only: [:continue]

  #user profile page for current user
  def show
    #redirect to the edit page for now
    redirect_to action: :edit
  end

  #show the edit user form
  def edit
    @user = current_user
  end

  #update the user profile
  def update
    @user = current_user
    if @user.update(user_params)
      bypass_sign_in @user
      flash[:notice] = 'Successfully updated your profile'
      redirect_to action: :edit
    else
      flash[:error] = 'Unable to update your profile'
      render action: :edit
    end
  end

  #Post sign-up page
  def continue
    @right_rail_disabled = true
  end

  protected
  def user_params
    params.require(:user).permit(:first_name, :last_name, :password, :avatar).delete_if do |k, v|
      if v.is_a? String
        v.strip.blank?
      else
        false
      end
    end

  end

  def unauthenticated_actions
    user_signed_in? ? [] : [:continue]
  end
end

class Users::SessionsController < Devise::SessionsController
  include RequestIp, OktaHelpers
  include Devise::Controllers::Rememberable
  include DetectPlatform, SafeRedirect
  include SessionConcern

  skip_before_action :verify_authenticity_token
  before_action :ensure_known_request
  after_action :set_roles_enabled_for_admin, only: [:create], if: -> { @current_user.is_admin_user? }
  after_action :switch_user_status, only: [:create], if: -> { @current_user.status == User::INACTIVE_STATUS }

  def new
    @current_org.reload # reload from the cache so we can get the real is_registrable flag
    if params[:deeplink] == 'true' && @current_org.can_deep_link_to_social?
      redirect_to view_context.social_connect_url(@current_org.available_ssos.keys.first)
      return
    end
    if params[:error_code] == 'invite_only'
      flash[:error] = "Cannot join this team without an invitation"
      redirect_to new_user_session_path
      return
    end
    if @current_org == Organization.default_org
      render_with_display :new_default
    else
      render_with_display
    end
  end

  def create
    org_sso = @current_org.org_ssos.includes(:sso).where(ssos: {name: 'email'}).first

    unless org_sso && org_sso.is_enabled
      log.warn("Attempted to login with a disabled sso option: organization=#{@current_org.id} provider=email")
      render inline: 'Not a valid sign in option', status: :forbidden
      return
    end

    super do |resource|
      resource.allow_sign_in!
      if !resource.organization.expire_session_on_closing_browser
        remember_me(resource)
      end
        
      @access_uri = 'login.email'
      session[:access_uri] = @access_uri

      resource.set_time_zone(ip_address: parse_user_ip) if resource.time_zone.blank?
      if params[:user][:terms_accepted]
        resource.set_terms_accepted unless resource.profile&.tac_accepted?
      end

      if session[:invitation_id].present?
        invitation = Invitation.find_by(id: session[:invitation_id])
        invitation.update(recipient_id: current_user.id) if invitation.present?
        session.delete(:invitation_id)
      end

      if @current_org.okta_enabled?
        params[:consumer_redirect_uri] = federated_connection(params['user'])
      end

      response.set_cookie("first_sign_in", {value: true, path: "/"}) if(resource.sign_in_count == 1)
      @is_web = (get_platform == 'web')

      @annual_org_subscription =
        UserSubscription.paid_annual_org_subscription?(org_id: current_org.id,
                                                       user_id: current_user.id)

      record_logged_in_event(org: current_org, user: current_user)

      respond_to do |format|
        format.json {
            @user = resource
            user_data = JSON.parse(render_to_string(template: 'users/info.json.jbuilder'))
            user_data[:consumer_redirect_uri] = params[:consumer_redirect_uri]
            render json: user_data and return
        }
        format.js {
          # for js requests, return 200 instead of redirect
          @authenticity_token = form_authenticity_token
          @target_url = after_sign_in_path_for(resource)
          render action: :create and return
        }
        format.html {
          respond_with resource, location: after_sign_in_path_for(resource) and return
        }
      end
    end
  end

  def destroy
    current_user.update(sign_out_at: DateTime.now)
    OktaSignoutJob.perform_later(user_id: current_user.id, organization_id: current_org.id) if current_org.okta_enabled? && current_user.federated_identifier.present?
    super do
      all_orgs = cookies.signed[:current_orgs]
      if all_orgs
        all_org_ids = all_orgs.split(',').map(&:to_i)
        all_org_ids.delete(@current_org.id)
        cookies.signed[:current_orgs] = {value: all_org_ids.join(','), domain: Settings.platform_domain}
      end

    end
  end

  def choose_organization
    render_with_display
  end

  def find_organization
    if request.get?
      render_with_display
    end
  end

  def switch_organization
    org = Organization.find_by_host_name(params[:organization][:host_name])
    if org
      redirect_to new_user_session_url(host: org.host)
    else
      @error = true
      render_with_display :choose_organization
    end
  end

  protected

  def after_sign_in_path_for(resource)
    #clear all flash messages
    flash.clear
    if request.referrer == new_user_session_url && params[:next].blank?
      super
    else
      params[:next].present? && params[:next] || stored_location_for(resource) || referrer_path || root_path
    end
  end

  def referrer_path
    request.referrer if request.referrer && request.referrer.include?(request.host)
  end

  def render_with_display(template=nil)
    _template = template || action_name
    if params[:display] == 'popup' || params[:next] && params[:next].start_with?('/oauth/authorize')
      render "users/sessions/display/popup/#{_template}"
    else
      render "users/sessions/display/page/#{_template}"
    end
  end

  def after_sign_out_path_for(resource)
    flash.clear
    if @current_org && (@current_org.host != Organization.default_org&.host)
      return new_user_session_path
    else
      return root_path
    end
  end

  def set_roles_enabled_for_admin
    @current_user.roles.find_by_name('admin')&.create_role_permissions
  end

  def switch_user_status
    @current_user.switch_to_active_status
  end
end

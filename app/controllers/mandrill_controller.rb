class MandrillController < ApplicationController

  skip_before_filter :authenticate_user!
  skip_before_filter :verify_authenticity_token
  before_filter :verify_request_signature, only: [:blacklist]
  skip_before_filter :set_organization_context

  def get_blacklist
    render text: ""
  end

  def blacklist
    # mandrill can send batched events
    events = JSON.parse(params['mandrill_events'])

    events.each do |event|
      blacklist_email = event['msg']['email']
      ts = event['ts'] # the timestamp when the bounce occurred

      email = Email.find_by_email(blacklist_email)

      if email.present?
        email.bounced! Time.at(ts).utc.to_datetime
      else
        user = User.find_by_email(blacklist_email)
        if user.present?
          # creates a new `Email` for that user with bounced_at set for bookkeeping.
          # this way if multiple emails are sent out to an address and the first one
          # `nil`s the user email, future postbacks wont complain of email not existing.
          if !Email.new(user: user,
                        email: blacklist_email,
                        confirmed: true,
                        bounced_at: Time.at(ts).utc.to_datetime).save
            log.error "Couldnt save bounced_at email for user: #{user.id} with email: #{blacklist_email}"
          end
        else
          # mandill should never be sending emails to addresses that dont exist in the system
          log.info "Mandrill Email not found in the system: #{blacklist_email}"
        end
      end
    end

    render text: ""
  end

  private
  def verify_request_signature
    expected_signature = request.headers['HTTP_X_MANDRILL_SIGNATURE']

    if expected_signature.nil?
      head :forbidden and return
    end

    signed_data = request.url
    signed_data += request.request_parameters.sort.join

    result = false

    sender = JSON.parse(params["mandrill_events"]).first["msg"]["sender"]

    mandrill_webhook_keys = MailerConfig.find_by_from_address(sender).try(:webhook_keys)
    mandrill_webhook_keys = mandrill_webhook_keys.split(',').map(&:strip) if mandrill_webhook_keys.present?
    mandrill_webhook_keys ||= Settings.mandrill.webhook_keys

    if mandrill_webhook_keys.empty?
      head :forbidden and return
    end

    mandrill_webhook_keys.each do |key|
      signature = Base64.strict_encode64(OpenSSL::HMAC.digest('sha1', key, signed_data))
      result = (signature == expected_signature)
      break if result
    end

    if !result
      head(:forbidden, :text => 'Mandrill Signature mismatch') and return
    end
  end
end

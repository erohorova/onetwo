require 'uri'
require 'rack/utils'

class UiController < ActionController::Base
  include RequestHandler
  include Entry
  include NativeAppDetection
  include DetectPlatform
  include AccessLogFilter

  layout 'ui'
  # Prevent CSRF attacks
  protect_from_forgery with: :null_session
  after_action :allow_iframe

  def qa
    prepare_data(params)
    if not performed?
      if @ref_id
        @ref_post = Post.find_by(id: @ref_id.to_i)
      end
      @app_name = 'ui'
    end
  end

  def qa_v1
    prepare_data(params)
    if not performed?
      if @ref_id
        @ref_post = Post.find_by(id: @ref_id.to_i)
      end
      @app_name = 'ui'
    end
  end

  def events
    prepare_data(params)
    if not performed?
      @app_name = 'events'
    end
  end

  protected

  def allow_iframe
    response.headers.except! 'X-Frame-Options'
  end
end

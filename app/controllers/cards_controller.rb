require 'uri'
class CardsController < ApiController
  skip_before_filter :set_organization_context, only: [:dashboard_stream] # forum API, does not make org scoped API calls

  def dashboard_stream
    @user = current_user

    group_ids = @user.not_banned_from_groups.not_closed.where(organization: @current_org).pluck(:id)

    #Temp fix, will remove it after migration
    www_user = @user.parent_user
    if(www_user && @user != www_user)
      group_ids << www_user.not_banned_from_groups.not_closed.where(organization: @current_org).pluck(:id)
      group_ids = group_ids.flatten.uniq
    end

    @posts = []
    unless group_ids.blank?
      limit = params.has_key?(:limit) ? params[:limit].to_i : 10
      offset = params.has_key?(:offset) ? params[:offset].to_i : 0
      @posts = retrieve_dashboard_posts(group_ids: group_ids, limit: limit, offset: offset)

      @votes_by_user = []

      if @posts.present?
        post_ids = @posts.map{|p| p['id']}
        add_stream_data(@posts)
        @votes_by_user = Vote.get_votes_by_user(@user,
                                                votable_type: 'Post',
                                                votable_ids: post_ids)
      end
    end
    render 'dashboard_stream'
  end

  private

  def retrieve_dashboard_posts(group_ids: , limit: , offset: )
    group_ids ||= []
    limit ||= 15
    offset ||= 0
    Post.search '*', where: {group_id: group_ids}, order: {created_at: :desc}, limit: limit, offset: offset, load: false
  end

  def add_stream_data(posts)

    group_ids = []
    posts.each do |post|
      group_ids << post['group_id']
    end

    all_groups = Group.where(id: group_ids).index_by(&:id)

    posts.each do |post|
      group = all_groups[post['group_id'].to_i]
      post['deeplink_id'] = post['id']
      post['group_name'] = group.name
      post['website_url'] = group.website_url
      post['deeplink_url'] = UrlBuilder::build(group.website_url, {'edcast_ref' => post['id']})
    end
  end
end

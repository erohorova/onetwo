class UsersController < ApiController
  include CardSearchHelpers
  include OrganizationContext
  include LimitOffsetConcern
  include ContentTypeFilter
  include DetectPlatform

  has_limit_offset_constraints only: [:cards, :followers, :suggested_users, :following_users], limit_default: 15, offset_default: 0

  skip_before_filter :cm_auth_user, only: [:user_profile, :confirmed, :cards, :streams, :verify_badge]
  skip_before_filter :set_organization_context, except: [:info, :cards, :suggested_users,:permissions]
  layout :false, only: [:verify_badge]

  api :GET, "groups/:id/users", "Get a list of users in this group (filter by search term)"
  param :id, String, :desc => 'Id of concerned group', :required => true
  param :private_group_id, Integer, :desc => 'Pass in the private group id to see the user role within the private group', :required => false
  param :search, String, :desc => 'fuzzy search term', :required => false
  param :offset, Integer, :desc => 'Where to start from the result set?', :required => false
  param :limit, Integer, :desc => 'How many users to return back', :required => false
  formats ['json']
  def index
    @group = Group.find_by_id(params[:id])
    head :bad_request and return if @group.nil?
    head :forbidden and return unless @group.is_user? current_user

    @users = @group.users.limit(params[:limit] || 20).offset(params[:offset]||0).order(id: :asc)
    @users = @users.where("first_name like :term or last_name like :term or email like :term", term: "%#{params[:search]}%") unless (params[:search].blank?)

    emails_to_users = {}
    default_org_id = Organization.default_org&.id
    @users.each do |user|
      email = user.email
      existing = emails_to_users[email]
      if existing.present?
        if user.organization_id != default_org_id
          emails_to_users[email] = user
        end
      else
        emails_to_users[email] = user
      end
    end
    @users = emails_to_users.values
    @user_role_in_group = {}

    user_ids = @users.map(&:id)

    if params.has_key?(:private_group_id)
      GroupsUser.where(group_id: params[:private_group_id], user_id: user_ids).group_by(&:user_id).map { |k, v|
        @user_role_in_group[k] = v.first.as_type
      }
    end
  end

  def verify_badge
    if params[:identifier].present?
      @user_badge = UserBadge.find_by(identifier: params[:identifier])
      head :not_found if @user_badge.nil?
    else
      head :bad_request
    end
  end

  api :GET, "users/:id/addable_private_groups", "Get groups user can be added to"
  param :id, String, :desc => "user id", :required => true
  formats ['json']
  def addable_private_groups
    user = User.find(params[:id])
    current_user_groups = current_user.groups.where(organization: @current_org, type: 'PrivateGroup')
    @open_groups = []
    current_user_groups.each do |g|
      unless g.is_user?(user)
        @open_groups << g
      end
    end
    render 'private_groups/index', formats: [:json] and return
  end

  api :GET, "users/groups/:group_id/score", "Get a user's score for a course"
  param :group_id, String,
    :desc => "get the user's score in this group",
    :required => true
  formats ['json']
  def score
    group = Group.find_by_id(params['group_id'])
    if group.nil?
      head :bad_request and return
    end
    render partial: 'score', locals: {user: current_user, group_id: group.id}
  end

  def current

  end


  api :PATCH, "users/:id", "Update user"
  param :id, String,
        :desc => "user id",
        :avatar => "[File data]",
        :required => true
  formats ['json']
  def update
    @user = current_user
    if @user != User.find(params[:id])
      head :forbidden and return
    end
    _params = user_params
    email = _params.delete(:email) # do not let the user update the email, need to confirm it first
    contextable = params[:contextable] || {}
    if email
      email_record = @user.emails.where(email: email).first
      unless email_record
        @email_record = @user.emails.create!(email: email, contextable_type: contextable[:type],
            contextable_id: contextable[:id],
           confirmed: false, confirmation_token: SecureRandom.hex(32))
      end
    end

    if @user.update_with_reindex_async(_params)

      # update newsletter preference
      if params.has_key?(:newsletter_opt_in) && params[:newsletter_opt_in]
        current_user.pref_newsletter_opt_in=(true)
      else
        current_user.pref_newsletter_opt_in=(false)
      end

      bypass_sign_in @user
      cookies.delete :collect_user_data
      respond_to do |format|
        format.js do

        end
        format.json do
        end
        format.all do
          head :success
        end
      end
    else
      respond_to do |format|
        format.js do

        end

        format.json do
          render json: @user.errors, status: :unprocessable_entity
        end

        format.all do
          head :unproccessable_entity
        end
      end
    end
  end

  def user_profile
    head :unauthorized and return unless params['SAVANNAH_TOKEN'] == ENV['SAVANNAH_TOKEN']
    @user = User.find(params.require(:user_id))
    # render json: @user, :only => [:email]
    render json: {:email => @user.email}
  end

  api :GET, "api/users/groups", "Get a user's savannah groups for flip mode"
  formats ['json']
  error 404, "Not found"
  def groups
    @user = current_user
    @groups = @user.courses
  end

  api :GET, "api/users/info", "Get a user's profile info"
  param :full_response, String, "Get a full response of user's profile info", required: false
  formats ['json']
  error 404, "Not found"
  def info
    @user = current_user
    @orgs = currently_signed_in_orgs
    @impersonation = session['devise.impersonator'] ? session['devise.impersonatee'] : nil
    @is_web = get_platform == 'web'
    @annual_org_subscription =
      UserSubscription.paid_annual_org_subscription?(org_id: current_org.id,
                                                     user_id: current_user.id)
  end

  api :GET, "api/users/:id/basic_info", "Get basic info for a user, used to identify users in chat"
  param :id, Integer, 'Id of the user'
  formats ['json']
  def basic_info
    @user = User.find(params[:id])
  end

  api :GET, 'api/users/info/following', 'Get a list of users being followed by the current user'
  param :offset, Integer,
        :desc => "Staring index of results to fetch",
        :required => false
  param :limit, Integer,
        :desc => "Max number of items to return for this request",
        :required => false
  formats ['json']
  def following_users
    @offset = offset
    @users = current_user.followed_users.offset(offset).limit(limit)
  end

  api :GET, "api/users/permissions", "Get a user permissions"
  formats ['json']
  error 404, "Not found"
  def permissions
    @user = current_user
  end

  api :GET, "groups/:group_id/users/:id", "Get the user's stats from this forum"
  param :group_id, String,
    :desc => "the group of interest",
    :required => true
  param :id, String,
    :desc => "the user id belonging to this group",
    :required => true
  formats ['json']
  def stats
    results = Searchkick.client.msearch(index: Post.searchkick_index.name,search_type: 'count', body: [{
        search: {query: {filtered: { filter: { bool: { must: [{term: { group_id: params[:group_id]}}, {term: {user_id: params[:id]}}]
                }         }         }       }
        }}, {
        search: {query: {filtered: { filter: { bool: { must: [{term: { 'comments.user_id' => params[:id] }},
                                                    {term: { group_id: params[:group_id]}}]

                }         }         }
              }}}])


    render json: {
                  postedCount: results['responses'][0]['hits']['total'],
                  answeredCount: results['responses'][1]['hits']['total']
                }
  end

  def confirmed
    if params.has_key?(:email) && params[:email].present?
      user = User.find_by_email(params[:email])
      if user.present? && user.confirmed?
        @authenticity_token = form_authenticity_token
        render action: :confirmed and return
      end
    end
    render js: "", status: :unprocessable_entity
  end

  api :GET, 'api/users/:id/cards', 'cards created by a user'
  param :id, Integer, desc: 'id of the user for whose card to retrieve', required: true
  param :offset, Integer, :desc => 'Where to start from the result set?', :required => false
  param :card_types, Array, desc: 'Array of card types. possible values: poll, media, pack, course Ex. card_types=[poll, pack]'
  param :limit, Integer, :desc => 'How many cards to return back', :required => false
  formats ['json']
  def cards
    @user = User.find(params[:id])
    card_states = ['published']
    card_types = []

    card_types = params[:card_types].present? ? (params[:card_types] & Card::EXPOSED_TYPES) : Card::EXPOSED_TYPES
    if current_user.id == @user.id
      card_states << 'draft'
    end

    filter_params = { card_type: card_types }
    filter_params.merge! state: card_states

    @response = Search::CardSearch.new.search(
        @user,
        current_org,
        filter_params: filter_params,
        viewer: current_user,
        offset: offset,
        limit: limit,
        sort: :created,
    )

    @cards = @response.results
    @offset = offset
    @limit = limit

    set_user_actions(@cards)
    set_card_comments(@cards)
    set_pack_counts(@cards)
    #set_channel_data(@cards)
  end

  api :GET, 'api/users/:id/streams', 'cards created by a user'
  formats ['json']
  def streams
    @user = User.find(params[:id])
    @streams = @user.my_video_streams_visible_to(viewer: current_user)
  end

  api :POST, '/api/users/set_config', 'update user config'
  formats ['json']
  def set_config
    @user = current_user
    @user.set_config(params[:key], params[:value])
    @user.save!
    head :ok
  end

  # DEPRECATED
  api :GET, '/api/suggested_users', 'return suggested users for current user to follow'
  param :offset, Integer, desc: 'current offset', required: false, default: 0
  param :limit, Integer, desc: 'number of results', required: false, default: 15
  def suggested_users
    @user = current_user
    @suggestions = User.suggested_users_for(user: @user).limit(limit).offset(offset)
  end

  api :GET, '/api/user/:id/followers', 'return all user followers'
  param :id, Integer, desc: 'id of the user whose follower to look up', required: true
  param :offset, Integer, desc: 'current offset in the response', required: false
  param :limit, Integer, :desc => 'number of followers per page', :required => false
  formats ['json']
  def followers
    @user = User.find(params[:id])
    @followers = @user.followed_by_users(offset: offset, limit: limit)
  end

  def user_params
    #do not allow user to change email address without user confirming it
    #contact_email can change at anytime without validation
    user_attributes = [:email, :first_name, :last_name,
                       :bio, :handle, :password, :avatar]
    params[:user][:password] = params[:password] if params.has_key?(:user) && params.has_key?(:password)
    if params.has_key?(:user)
      params.require(:user).permit(user_attributes)
    else
      params.permit(user_attributes)
    end
  end
end

class PostsController < ApiController
  include PostSearchHelpers

  before_action :set_postable
  before_action :allow_iframing
  after_action :set_timestamp, only: [:index]

  api :GET, "groups/:group_id/posts", "Get posts for a group"
  api :GET, "channels/:channel_id/posts", "Get posts for a channel"
  api :GET, "course_offerings/:client_resource_id/posts", "Get posts for a course_offering group"

  param :group_id, Integer, :desc => "The concerned group", :required => false
  param :channel_id, Integer, :desc => "The concerned channel", :required => false
  param :client_resource_id, Integer, :desc => "The concerned course offering id", :required => false
  param :sort, String, :desc => 'Sort criterion. One of: \'date\', \'responses\', \'votes\', \'updates\'. Default is date.', :required => false
  param :order, String, :desc => 'Sort order. One of: \'desc\', \'asc\'. Default is date.',:required => false
  param :search, String, :desc => 'Search query', :required => false
  param :tag, String, :desc => 'Filter by this tag', :required => false
  param :author_id, String, :desc => 'Filter by author of the post', :required => false
  param :commenter_id, String, :desc => 'Filter posted with answers from this user', :required => false
  param :reported, String,
    :desc => 'Get items hidden because of reports only',
    :required => false
  param :limit, Integer, :desc => 'Limit pagination', :required => false
  param :type, String, :desc => 'Filter by post type', :required => false
  param :pinned, String, :desc => "set to 'true' if you want pinned posts only, default - false", :required => false
  param :last_fetch_time, String, :desc => 'The timestamp when the last request was made', :required => false
  param :following, String, desc: 'Posts followed by logged-in user', required: false
  formats ['json']
  def index
    pars = params.permit(:sort, :order, :type, :commenter_id, :author_id,
                         :search, :tag, :limit, :reported, :page, :context_id,
                         :last_fetch_time, :pinned, :following)

    unless @postable.can_read_forum?(current_user)
      head :unauthorized and return
    end

    @posts = search_posts_for_postable(@postable, pars)
    set_private_group_org_user(@postable) if (@postable.respond_to?(:type) && @postable.type == "PrivateGroup")

    @reads = {}
    @reports_by_user = @votes_by_user = @approved_posts = []
    post_ids = []
    if current_user && @posts.present?
      post_ids = @posts.map{|post| post.id.to_i }
      @reads = PostRead.get_reads(post_ids, current_user)

      @reports_by_user = Report.get_reports_by_user(current_user,
                                                    reportable_type: 'Post',
                                                    reportable_ids: post_ids)
      @votes_by_user = Vote.get_votes_by_user(current_user,
                                              votable_type: 'Post',
                                              votable_ids: post_ids)

      @approved_posts = Approval.get_approvals(approvable_ids: post_ids,
                                                   approvable_type: 'Post')
    end
  end

  api :GET, "groups/:group_id/posts/filters", "Get categorized posts count for a group"
  param :group_id, Integer, desc: "The concerned group", required: true
  formats ['json']
  def filters
    pars = params.permit(:group_id, :context_id)

    group = Group.find(pars['group_id'])
    unless group.is_active_user?(current_user)
      head :unauthorized and return
    end

    @results = {}
    filter_params = { group_id: group.id }
    filters = { all: true, pinned: true, reported: true, 'type' => 'Announcement', author_id: current_user.id, 'following' => true }
    filters.each do |i, v|
      hash = filter_params.merge(i => v)
      i != :reported && hash.merge!(reported: false)
      @results[i.to_sym] = search_posts_for_postable(group, hash).total_count
    end
  end

  api :GET, '/posts/:id', "Get the requested post"
  param :id, :number, :desc => "The concerned post", :required => true
  formats ['json']
  def show
    @post = Post.retrieve_by_ids([params[:id]]).first
    if @post.nil?
      head :not_found and return
    end
  end

  api :POST, "groups/:group_id/posts", "Create a post for a group"
  param :group_id, :number, :desc => "The concerned group", :required => true
  param :title, String, :desc => "title/summary of the post", :required=> true
  param :message, String, :desc => "The post text", :required => true
  param :anonymous, String, :desc => "Post the post anonymously", :required => false
  param :pinned, String, :desc => 'Pin the post to top', :required => false
  param :networks, Array, :desc => "Which social network to share the post to", :required => false
  param :type, String, :desc => "Post type", :required => true
  param :file_ids, Array, :desc => "File ids to be attached", :required => false
  def create
    pars = params[:post] || params

    # pushing file_ids array in pars
    if (params.has_key?(:file_ids))
        pars[:file_ids] = params[:file_ids]
    end

    if !(pars.key?('message') && pars.key?('title'))
      head :bad_request and return
    end

    # support only 'Question' and 'Announcement' types
    if pars['type'] == 'Question'
      @post = Question.new({:title => pars['title'],
                            :message => filter_message(pars['message'])})
    elsif pars['type'] == 'Announcement'
      @post = Announcement.new({:title => pars['title'],
                                :message => filter_message(pars['message'])})
    else
      head :unprocessable_entity and return
    end

    @post.context_id = pars[:context_id] unless pars[:context_id].blank?
    @post.context_label = pars[:context_label] unless pars[:context_label].blank?

    @post.anonymous = pars['anonymous']

    @post.postable = @postable

    if @postable.is_active_super_user?(current_user) && pars[:pinned]
      @post.pinned = true
      @post.pinned_at = DateTime.now
    end

    @post.user = current_user

    unless @widget_request
      @post.is_mobile = true
    end

    if @post.save

      # associate files to this post
      if (pars.has_key?(:file_ids) && pars[:file_ids].is_a?(Array))
        frs = FileResource.where(id: pars[:file_ids])
        @post.file_resources << frs
      end

      if (pars.has_key?(:networks) && pars[:networks].is_a?(Array))
        #share to the social network
        ShareJob.perform_later(sharable_class: @post.class.name, sharable_id: @post.id, user_id: current_user.id, networks: pars[:networks])
      end
      respond_to do |f|
        f.json { render 'show', status: :created }
        f.html
        f.js
      end
    else
      respond_to do |f|
        f.json { render json: @post.errors, status: :unprocessable_entity }
        f.html
        f.js
      end
    end
  end

  api :PATCH, "groups/:group_id/posts/:id", "Update the post for the group"
  param :group_id, :number, :desc => "The concerned group", :required => true
  param :title, String, :desc => "title/summary of the post", :required => true
  param :message, String, :desc => "The post text", :required => true
  param :file_ids, Array, :desc => "File ids to be attached", :required => false
  def update
    @post = Post.find(params[:id])
    unless current_user.is_equivalent_user(@post.user) #@post.user != current_user
      head :forbidden and return
    end

    update_params = params.permit(:message, :title)
    @post.update(update_params)
    if @post.save
      @post.update_resource
      @post.update_tags

      # associate files to this post, overwrite existing ones
      params[:file_ids] ||= [] if params.has_key?(:file_ids)

      if (params.has_key?(:file_ids) && params[:file_ids].is_a?(Array))
        @post.file_resources.each do |fr|
          unless params[:file_ids].include?(fr.id)
            fr.destroy
          end
        end

        @post.reload

        frs = FileResource.where(id: params[:file_ids])
        frs.each do |fr|
          @post.file_resources << fr unless @post.file_resources.include?(fr)
        end

      end

      render 'show', status: :ok
    else
      render json: @post.errors, status: :unprocessable_entity
    end
  end

  api :DELETE, "groups/:group_id/posts/:id", "Delete a post"
  param :group_id, :number, :desc => "The concerned group", :required => true
  param :id, :number, :desc => "The post to delete", :required => true
  formats ['json']
  def destroy
    @post = Post.find(params[:id])
    if !current_user.is_equivalent_user(@post.user) && !@post.postable.is_active_super_user?(current_user)
      head :forbidden and return
    end

    @post.destroy
    head :ok and return
  end

  api :POST, "groups/:group_id/posts/:id/restore",
    "Restore a reported post"
  param :group_id, :number, :desc => "The concerned group", :required => true
  param :id, :number, :desc => "The post to restore", :required => true
  formats ['json']
  def restore
    @post = Post.find(params[:id])
    unless @post.postable.is_active_super_user?(current_user)
      head :forbidden and return
    end

    @post.reports.each do |report|
      # need to take care of corner case where faculty reports and restores item
      if report.reporter == current_user
        report.destroy
      else
        report.validity = false
        report.save
      end
    end
    @post.hidden = false
    @post.save
    head :ok and return
  end

  api :POST, "posts/:id/pin",
    "Pin a post"
  param :id, :number, :desc => "The post to restore", :required => true
  param :state, String, :desc => "Pin state, true or false", :required => true
  def pin
    @post = Post.find(params[:id])
    unless @post.postable.is_active_super_user?(current_user)
      head :forbidden and return
    end

    if params.key?(:state)
      @post.pinned = params[:state]
      @post.pinned_at = DateTime.now
      @post.save
    end

    head :ok and return
  end

  api :POST, "post/:id/convert_to_card", "Convert Post to Card"
  param :id, :number, :desc => "The post to convert into a card", :required => true
  param :tags, String, :desc => "The tags for the card", :required => false
  def convert_to_card
    @post = Post.find(params[:id])

    unless @post.postable.is_active_super_user?(current_user)
      head :unauthorized and return
    end

    if !@post.resource.nil?
      title = @post.title
      message = "#{@post.message}"
      topics = ""
      unless params[:tags].blank?
        tags = Tag.extract_tags(params[:tags])
        topics = tags.collect(&:name).join(', ')
        logger.warn "Holy Crap, We sent tags like this to posts controller"
      end
      ui_layout_type = nil
      case @post.resource.type
      when 'Video'
        ui_layout_type = 'video'
      else
        ui_layout_type = 'link'
      end

      author_info = {
        author_id: @post.user.id
      }

      card_create_params = {
          card_type: 'media',
          card_subtype: ui_layout_type,
          is_public: true,
          title: title,
          message: message,
          resource_id: @post.resource_id,
          state: 'published',
          topics: topics
      }.merge author_info

      card = Card.new(card_create_params)
      if card.save
        render json: {status: 'created', card: card}, status: :created
      else
        render json: {status: 'unprocessable entity', errors: card.full_errors}, status: :unprocessable_entity
      end

    elsif @post.resource.nil?
      response = {status: 'error', error: 'Cannot create a card since the post does not have a media resource.'}
      render json: response, status: :unprocessable_entity
    elsif @post.card_exists?
      response = {status: 'error', error: 'Card already exists.'}
      render json: response, status: :unprocessable_entity
    else
      response = {status: 'error', error: 'Card could not be created.'}
      render json: response, status: :unprocessable_entity
    end
  end

  def allow_iframing
    response.headers["X-FRAME-OPTIONS"] = "ALLOWALL"
  end

  private

  def visible_without_auth?
    # For viewing comments/replies, see if publicly visible, everything else requires auth
    if action_name == 'index'
      set_postable
      return !!@postable.try(:visible_without_auth?)
    elsif action_name == 'show' # Direct routes also available, so cant depend on set_postable
      @postable = Post.find(params[:id]).postable
      return !!@postable.try(:visible_without_auth?)
    end
    false
  end

  def set_postable
    return unless @postable.nil?
    if params[:group_id]
      @postable = Group.find(params[:group_id])
    elsif params[:channel_id]
      @postable = Channel.find(params[:channel_id])
    elsif params[:client_resource_id]
      @postable = Group.find_by!(client_resource_id: params[:client_resource_id])
    end
  end

  def filter_message(content)
    content.gsub(/(\n<p>&nbsp;<\/p>)*$/, '')
  end

  def set_timestamp
    response.headers['X-Last-Fetch-Timestamp'] = Time.now.utc.strftime('%Y-%m-%dT%H:%M:%S.%LZ')
  end

  def set_private_group_org_user(private_group)
    GroupsUser.find_or_create_by(group_id: private_group.id, user_id: current_user.id) do |gu|
      gu.as_type = private_group.user_role(current_user.try(:forum_user))
    end
  end
end

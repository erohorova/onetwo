class EmailsController < ApplicationController

  skip_before_filter :authenticate_user!
  def confirm
    email = Email.find_by_confirmation_token!(params[:token])

    email.confirmed = true

    if email.save
      user = email.user
      if user.email.blank?
        user.skip_confirmation!
        user.skip_reconfirmation!
        user.email = email.email
        unless user.save
          log.error(%Q{Unable to update the user's email address errors=#{user.errors.to_json}})
        end
      end
    else
      log.error(%Q{Unable to confirm the user's email address errors=#{email.errors.to_json}})
    end

  end
end

class EventsController < ApiController

  before_action :set_event, only: [:show, :edit, :update, :destroy]
  before_filter :authorize_user_for_group, :except => [:watch, :hangout]

  skip_before_filter :cm_auth_user, :only => [:hangout, :watch]
  before_filter :perform_api_auth!, :only => [:watch]
  protect_from_forgery :except => :hangout

  api :GET, "groups/:group_id/events", "Get events for a group"
  param :group_id, Integer, :desc => "The concerned group", :required => true
  param :limit, Integer, :desc => "Limit pagination", :required => false
  param :myevents, [true, false], :desc => "toggle between events list and my events",
    :required => false
  formats ['json']
  def index
    limit = params[:limit].nil? ? 25 : params[:limit]
    if params[:myevents]
      @events = Event.includes(:events_users).
        where(:group_id => params[:group_id]).joins(:events_users).
        where(:events_users => {:rsvp => ['yes', 'maybe'], :user => current_user})
    else
      @events = Event.upcoming.where(:group_id => params[:group_id]).order(event_start: :asc).limit(limit)
    end
  end

  api :GET, "groups/:group_id/events/:id", "get the requested event"
  param :group_id, Integer, "the group containing the event", :required => true
  param :id, Integer, "the id of the concerned event", :required => true
  formats ['json']
  def show
  end

  api :GET, "groups/:group_id/webex_enabled", "check webex is enabled or not"
  param :group_id, Integer, "the group containing the event", :required => true
  formats ['json']
  def webex_enabled
    enabled =  @group.organization.webex_config.present?
    render json: {enabled: enabled}, status: 200
  end

  api :POST, "groups/:group_id/events", "create a new event in the given group"
  formats ['json']
  def create
    @event = Event.new(event_params)
    @event.webex_id = params[:webex_id]
    @event.webex_password = params[:webex_password]
    @event.group_id = params[:group_id]
    @event.user_id = current_user.id
    if @event.save
      render action: 'show', status: :created
    else
      render json: @event.full_errors, status: :unprocessable_entity
    end
  end

  api :PATCH, "groups/:group_id/events/:id", "update the given event"
  param :group_id, Integer, "the concerned group", :required => true
  param :name, String, "the name of the event", :required => false
  param :address, String, "the location of the event", :required => false
  param :event_start, String, "when the event is being held", :required => false
  param :event_end, String, "when the event ends", :required => false
  param :latitude, Integer, "the latitude of the event venue", :required => false
  param :longitude, Integer, "the longitude of the event venue", :required => false
  param :state, String, "whether the event is open or closed", :required => false
  param :privacy, String, "whether a private or public course", :required => false
  formats ['json']
  def update

    if @event.user != current_user
      head :forbidden and return
    end

    if @event.update(event_params)
      @event.generate_ical
      head :ok
    else
      render json: @event.full_errors, status: :unprocessable_entity
    end
  end

  api :DELETE, "groups/:group_id/events/:id", "delete the given event"
  param :group_id, Integer, "the concerned group", :required => true
  param :id, Integer, "the id of the concerned event", :required => true
  formats ['json']
  def destroy

    if @event.user != current_user
      head :forbidden and return
    end

    if @event.destroy
      head :ok
    else
      render json: @event.errors, status: :unprocessable_entity
    end
  end

  api :GET, "groups/:group_id/plot/events", "list co-ords for all events in group"
  param :limit, Integer, :desc => "Limit pagination", :required => false
  param :group_id, Integer, "the concerned group", :required => true
  formats ['json']
  def plot
    limit = params[:limit].nil? ? 25 : params[:limit]
    @coords = Event.upcoming.where(group_id: params[:group_id]).limit(limit).pluck(:latitude, :longitude)
  end

  def hangout
    begin
      @event_id = ActiveSupport::MessageEncryptor.new(Edcast::Application.config.secret_key_base).decrypt_and_verify(params[:data])
    rescue ActiveSupport::MessageVerifier::InvalidSignature
      head :unauthorized and return
    end

    @event = Event.find(@event_id)
    if (params[:url])
      @event.conference_url = params[:url]
    end
    if (params[:youtube_url])
      @event.youtube_url = params[:youtube_url]
    end
    if @event.save
      head :ok and return
    end
  end

  api :GET, "groups/:group_id/events/:id/watch", "get the requested event"
  param :group_id, Integer, "the group containing the event", :required => true
  param :id, Integer, "the id of the concerned event", :required => true
  formats ['json']
  def watch
    event = Event.where(group_id: params[:group_id], id: params[:id]).first
    if event.nil? || event.youtube_url.nil?
      head :bad_request and return
    end

    unless event.going?(current_user)
      head :unauthorized and return
    end

    @discussion_enabled = event.hangout_discussion_enabled
    @youtube_url = event.youtube_url
    @api_key = params[:api_key]
    @user_id = params[:user_id]
    @user_email = params[:user_email]
    @timestamp = params[:timestamp]
    @token = params[:token]
    @resource_id = params[:resource_id]
    @user_role = params[:user_role]

    if event.context_enabled
      @context_id = event.id
      @context_label = event.name
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_event
    @event = Event.find(params[:id])
  end


  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:name, :address, :description, :event_start, :event_end, :state, :privacy, :latitude, :longitude, :conference_type, :hangout_discussion_enabled, :context_enabled, :youtube_url, :data, :conference_email)
    end

    def authorize_user_for_group
      # in the absence of authority gem, we use this to authorize the users
      # access inside the group
      @group = Group.find(params[:group_id])
      unless @group.is_active_user?(current_user)
        head :unauthorized and return
      end
    end
end

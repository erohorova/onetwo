class ApprovalsController < ApiController

  before_action :set_approvable, :only => [:create, :destroy]

  # approval for a approvable
  api :POST, "posts/:post_id/approvals", "Approve for a Post"
  api :POST, "comments/:comment_id/approvals", "Approve for a Comment"
  formats ['json']
  error 422, "Unprocessable"
  description "Sends 201 Created on Success"
  def create
    @approval = Approval.new
    @approval.approvable = @approvable
    @approval.approver = current_user

    if @approval.save
      head :created
    else
      render json: @approval.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @approval = Approval.where(approvable:  @approvable, approver: current_user).first
    if @approval
      @approval.destroy
      head :ok
    else
      head :bad_request
    end
  end
  private
  def set_approvable
    
    if params.key? 'post_id'
      @approvable = Post.find(params['post_id'])
    elsif params.key? 'comment_id'
      @approvable = Comment.find(params['comment_id'])
    end
  end
end


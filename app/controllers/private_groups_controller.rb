class PrivateGroupsController < ApiController

  before_filter :set_pagination, only: [:enrolled_groups, :open_groups, :search]
  before_filter :set_user_groups, only: [:index, :enrolled_groups, :open_groups, :search]
  before_filter :get_group, only: [:users, :show, :destroy, :update, :add_user, :remove_user, :ban_user, :unban_user]
  before_filter :is_admin_user, only: [:enrolled_groups, :open_groups, :destroy]

  api :GET, 'groups/:id/private_groups', "Show current user's private groups"
  param :id, Integer, desc: 'parent resource group id' , required: true
  formats ['json']
  def index
    ids = @groups.collect(&:id).presence || 0
    @open_groups = PrivateGroup.where('id NOT IN (?)', ids).org_private_groups(@current_org, params[:id]).open
  end

  api :GET, 'groups/:id/private_groups/enrolled_groups', "Show current user's enrolled groups"
  param :id, Integer, desc: 'parent resource group id' , required: true
  param :offset, Integer, desc: 'set offset for the results' , required: false
  param :limit, Integer, desc: 'limit to the results' , required: false
  formats ['json']
  def enrolled_groups
    @user_groups_count = @groups.length
    @groups = @groups.limit(@limit).offset(@offset)# if params[:type].blank? || params[:type] == 'my_groups'
  end

  api :GET, 'groups/:id/private_groups/open_groups', "Show current user's oepn groups"
  param :id, Integer, desc: 'parent resource group id' , required: true
  param :offset, Integer, desc: 'set offset for the results' , required: false
  param :limit, Integer, desc: 'limit to the results' , required: false
  def open_groups
    ids = @groups.pluck(:id).presence || 0
    @open_groups = PrivateGroup.where('id NOT IN (?)', ids).org_private_groups(@current_org, params[:id]).open
    @open_groups_count = @open_groups.count
    @open_groups = @open_groups.limit(@limit).offset(@offset)
  end

  api :GET, 'groups/:id/private_groups/search', 'Search for groups across user\'s and recommended groups'
  param :q, String, desc: 'Search query', required: false
  formats ['json']
  def search
    user_groups_ids = @groups.pluck(:id)

    @groups = PrivateGroup.search params[:q],
      where: {
        organization_id: @current_org.id,
        id:        user_groups_ids },
      limit:       @limit,
      offset:      @offset,
      order:       { name: :asc }

    @open_groups = PrivateGroup.search params[:q],
      where: {
        id:        { not: user_groups_ids },
        parent_id: params[:id],
        organization_id: @current_org.id,
        access:    'open' },
      limit:       @limit,
      offset:      @offset,
      order:       { name: :asc }

    @user_groups_count = @groups.total_count
    @open_groups_count = @open_groups.total_count
  end

  api :GET, 'private_groups/:id', 'Show detail of the single private group'
  formats ['json']
  param :id, Integer, desc: 'Id of the private group'
  def show
    head :forbidden and return unless @group.is_active_user? current_user
  end

  api :GET, 'private_groups/:id/users', 'Show all members of this group'
  param :id, Integer, desc: 'group id', required: true
  formats ['json']
  def users
    head :forbidden and return unless @group.is_active_user? current_user
    @users = @group.users.includes(:groups_users)
    emails_to_users = {}
    default_org_id = Organization.default_org&.id
    @users.each do |user|
      email = user.email
      existing = emails_to_users[email]
      if existing.present?
        if user.organization_id != default_org_id
          emails_to_users[email] = user
        end
      else
        emails_to_users[email] = user
      end
    end
    @users = emails_to_users.values
  end

  api :POST, 'groups/:id/private_groups', 'Create a group'
  param :id, Integer, desc: 'parent resource group id' , required: true
  param :name, String, desc: 'name of the group', required: true
  param :access, String, desc: 'access type, open or invite'
  formats ['json']
  def create

    parent_group = ResourceGroup.find(params[:id])
    head :unprocessable_entity and return if parent_group.nil?

    head :forbidden and return unless parent_group.is_user? current_user

    access = params[:access].nil? ? 'open' : params[:access]
    @group = PrivateGroup.new(name: params[:name].truncate(255, separator: ' '),
                             access: access,
                             client: @client,
                             organization: @current_org,
                             creator: current_user,
                             resource_group: parent_group)
    if @group.save
      render 'show', status: :created
    else
      render json:{error: @group.errors.full_messages}, status: :unprocessable_entity
    end
  end

  api :DELETE, 'private_groups/:id', 'Delete a group'
  param :id, Integer, desc: 'private group id' , required: true
  formats ['json']
  def destroy
    head :forbidden and return if !(@group.is_admin?(current_user) || @is_admin)
    if @group.destroy
      head :ok
    else
      render :json, @group.errors, :unprocessable_entity
    end
  end

  api :PATCH, 'private_groups', 'Update the group details'
  param :name, String, desc: 'name of the group'
  param :access, String, desc: 'access type, open or invite'
  param :id, Integer, desc: 'group id', required: true
  formats ['json']
  def update
    head :forbidden and return unless @group.is_active_admin? current_user

    p = params.permit([:name, :access])
    p[:name] = p[:name].truncate(255, separator: ' ') unless p[:name].blank?
    if @group.update(p)
      render 'show', status: :ok
    else
      render :json, @group.errors, :unprocessable_entity
    end
  end

  api :POST, 'private_groups/:id/users/:user_id', 'Add a user to the group. Call this api again with a new role to promote user to the new role'
  param :member_id, Integer, desc: 'user id to be added to this group. Have to user member_id because user_id is taken by the security layer'
  param :role, String, desc: 'role of the user to be added. ex: member, or admin'
  formats ['json']
  def add_user
    head :forbidden and return if @group.creator_id == params[:member_id].to_i

    user = User.find(params[:member_id])
    if @group.is_user?(user)
      # role change for existing users requires admin priv.
      head :forbidden and return unless @group.is_active_admin?(current_user)
    else
      if params[:role] == 'admin'
        head :forbidden and return unless @group.is_active_admin?(current_user)
      elsif params[:role] == 'member'
        head :forbidden and return if @group.access == 'invite' && !@group.is_active_user?(current_user)
        head :forbidden and return if @group.access == 'open' && !@group.is_active_user?(current_user) && current_user.id != user.id
      end
    end

    WelcomeGroupMemberMailer.welcome_group_member_email(group_id: @group.id,to_user_id: user.id,from_user_id: current_user.id).deliver_later unless @group.is_user?(user)

    @group.add_user(user: user, role: params[:role])
    head :ok
  end

  api :DELETE, 'private_groups/:id/users/:member_id', 'Remove user from the group. Logged in user can leave this group by calling this API with his own user id'
  param :member_id, Integer, desc: 'user id to be removed from this group'
  formats ['json']
  def remove_user
    head :forbidden and return if @group.creator_id == params[:member_id].to_i

    #verify that user is admin of the group in order to add another admin user
    head :forbidden and return unless @group.is_active_admin?(current_user) || params[:member_id].to_i == current_user.id

    user = User.find(params[:member_id])
    gu = GroupsUser.find_by(user: user, group: @group)

    if gu.nil? || @group.groups_users.destroy(gu)
      head :ok
    else
      head :unprocessable_entity
    end
  end

  api :POST, 'private_groups/:id/users/:user_id/ban', 'Ban user from the group'
  param :member_id, Integer, desc: 'user id to be banned from this group'
  formats ['json']
  def ban_user
    head :forbidden and return if @group.creator_id == params[:member_id].to_i

    #verify that user is admin of the group in order to add another admin user
    head :forbidden and return unless @group.is_active_admin?(current_user)


    user = User.find(params[:member_id])
    if @group.ban_user(user)
      head :ok
    else
      head :unprocessable_entity
    end
  end

  api :DELETE, 'private_groups/:id/users/:user_id/unban', 'Unban user from the group'
  param :member_id, Integer, desc: 'user id to be unbanned from this group'
  formats ['json']
  def unban_user

    #verify that user is admin of the group in order to add another admin user
    head :forbidden and return unless @group.is_active_admin?(current_user)

    user = User.find(params[:member_id])
    if @group.unban_user(user)
      head :ok
    else
      head :unprocessable_entity
    end
  end

  private
  def get_group
    @group = PrivateGroup.where(id: params[:id], organization: @current_org).first!
  end

  def is_admin_user
    resource_group = ResourceGroup.find_by(id:  params[:id])
    if resource_group.blank?
      private_group = PrivateGroup.find_by(id: params[:id])
      resource_group = ResourceGroup.find_by(id: private_group.parent_id) if private_group.present?
    end
    return @is_admin = false if resource_group.blank?
    @is_admin = resource_group.user_role(current_user) == "admin"
  end

  def set_pagination
    @limit = [params[:limit] || 10].detect(&:present?).to_i
    @offset = params[:offset].to_i || 0
  end

  # TODO: This breaks for users in custom orgs. The user enrolled in the group
  # is in the default org, so current_user.groups will return [] for people in custom orgs
  def set_user_groups
    www_user = current_user.parent_user
    user_ids = [current_user.id, www_user.try(:id)].compact

    # @groups = current_user.groups.org_private_groups(@current_org, params[:id]).latest
    @groups = Group.joins(:groups_users).org_private_groups(@current_org, params[:id]).where("groups_users.user_id IN (?)", user_ids).uniq.latest
  end
end

class Cms::BaseController < ApplicationController
  include CompoundIdReader
  include AuthenticationHelper

  resource_description do
    api_version 'Deprecated'
  end

  before_filter :authenticate_org_or_team_admin!
  before_filter :set_request_store

  layout 'cms'
  def authenticate_org_or_team_admin!
    # Let all @edcast.com users access cms.
    # Let org admins access cms

    unless is_org_or_team_admin
      respond_to do |format|
        format.html {
          render "errors/not_found", layout: 'errors', status: :not_found and return
        }
        format.all {
          head :not_found and return
        }
      end
    end
  end

  def is_org_or_team_admin
    team_admin = Team.find_by(id: params[:team_id]).try(:is_admin?, current_user)
    current_org.has_admin_access?(current_user) || team_admin
  end

  def home
    render 'cms/home'
  end

  private

  def build_sort_params(pars)
    sort_hash = {}
    sort_hash[:key] = pars[:sort].blank? ? 'created_at' : pars[:sort]
    sort_hash[:order] = pars[:order].blank? ? 'desc' : pars[:order]
    sort_hash
  end

  # card helpers begin
  def perform_state_change card_ids, event_method
    cards = Card.where(id: params[:card_ids], organization_id: current_org.id)
    updated_cards = []
    cards.each do |c|
      begin
        updated_cards << c.id if c.send(event_method)
      rescue
      end
    end
    updated_cards
  end

  def augment_card_data
    return unless @cards
    channel_ids = @cards.map(&:channel_ids).flatten
    channels = Channel.where(id: channel_ids).index_by(&:id)
    # For each card, adds an attribute called "channels" that is an array of hashes with 'id' and 'label' keys
    # Elasticsearch index for cards already contains the channel_ids array, we are just augmenting with channel label here
    @cards.each {|card| card.channels = (card.channel_ids.map do |ch_id|
      ch = channels[ch_id]
      if ch
        {id: ch.id, label: ch.label}
      else
        nil
      end
    end).compact}
  end

  def set_request_store
    payload = {
      user_id:        current_user&.id,
      platform:       get_platform,
      user_agent:     request.headers.env['HTTP_USER_AGENT'],
      platform_version_number: request.headers.env['HTTP_X_VERSION_NUMBER'],
      is_admin_request: ApplicationController.is_admin_request?(request)
    }

    RequestStore.store[:request_metadata] = payload
  end
  # card helpers end
end

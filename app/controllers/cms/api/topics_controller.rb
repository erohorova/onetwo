# DEPRECATED
class Cms::Api::TopicsController < Cms::BaseController
  include LimitOffsetConcern
  has_limit_offset_constraints only: [:suggest]

  api :GET, "cms/api/topics/suggest", "Get topics for autosuggest"
  param :q, String, "Search term", required: false
  param :limit, Integer, "Limit for pagination, default 10", required: false
  formats ['json']
  def suggest
    tag_suggestions = TagSuggest.suggestions_for_destiny(term: params[:q], size: limit, organization_id: current_org.id)
    render json: {"query" => params[:q], "results" => tag_suggestions}, status: :ok and return
  end
end

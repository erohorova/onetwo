class Cms::Api::VideoStreamsController < Cms::BaseController
  include LimitOffsetConcern
  has_limit_offset_constraints only: [:index]
  before_action -> {current_user.is_org_admin? || check_exclude_group_permission(Permissions::MANAGE_GROUP_CONTENT) },
    only: [:index]

  api :GET, "cms/api/video_streams", "Get video streams in an organization for the cms, sorted by newest first"
  param :q, String, "Search term", required: false
  param :limit, Integer, "Limit for pagination, default 10", required: false
  param :offset, Integer, "Offset for pagination, default 0", required: false
  param :status, String, "One of 'live', 'upcoming', 'past'. Defaults to all"
  param :state, String, "One of 'published', 'deleted'. Defaults to all"
  param :topics, Array, "Array of topic strings to filter for"
  param :group_id, Array, "Array of group ids to filter for"
  param :channel_ids, Array, "Array of channel ids to filter for"
  param :sort, String, "Attribute name to sort by, ex title, created_at"
  param :order, String, "Sort order, ASC or DESC"
  formats ['json']
  def index
    video_stream_params = params.permit(
      :q, :sort, :order, :limit, :offset, :status, :state, topics: [], channel_ids: [], creator_id: []
    )
    query = video_stream_params[:q] || ''
    group_sub_admin = current_user.group_sub_admin_authorize?(Permissions::MANAGE_GROUP_CONTENT)

    if group_sub_admin && video_stream_params[:channel_ids].present?
      video_stream_params[:channel_ids] &= current_user.followed_channel_ids
    elsif group_sub_admin
      video_stream_params[:channel_ids] = current_user.followed_channel_ids
    end

    if group_sub_admin && params[:group_id].present?
      video_stream_params[:creator_id] = current_user.user_ids_for_teams('sub_admin', group_ids: params[:group_id])
    end

    filter_params = {}
    [:topics, :channel_ids, :status, :state, :creator_id].each do |k|
      filter_params[k] = video_stream_params[k] if video_stream_params.has_key?(k)
    end

    es_response = Search::VideoStreamSearch.new.search_for_cms(
        query,
        current_org.id,
        filter_params: filter_params,
        offset: offset,
        limit: limit,
        sort: build_sort_params(video_stream_params)
    )
    @video_streams = es_response.results
    @total_count = es_response.total
  end

  api :POST, "cms/api/video_streams/publish", "Publish an array of video streams from the cms"
  param :video_stream_ids, Array, "Array of video stream ids to publish", required: false
  formats ['json']
  def publish
    perform_state_change params[:video_stream_ids], :publish!
    params[:video_stream_ids].map(&:to_i).each do |vid|
      Cms::Judgement.create_content_trail vid, params[:action], current_user, "VideoStream"
    end
    head :ok and return
  end

  api :POST, "cms/api/video_streams/delete", "Delete an array of video streams from the cms"
  param :video_stream_ids, Array, "Array of video stream ids to delete", required: false
  formats ['json']
  def delete
    perform_state_change params[:video_stream_ids], :delete_from_cms!
    params[:video_stream_ids].map(&:to_i).each do |vid|
      Cms::Judgement.create_content_trail vid, params[:action], current_user, "VideoStream"
    end
    head :ok and return
  end

  api :PATCH, 'cms/api/video_streams/:id', 'Update a Video stream'
  param :id, Integer, 'Video stream id to be updated', required: true
  param :name, String, 'Name for the stream', required: false
  param :is_official, String, 'Toggle is official', required: false
  param :channel_ids, Array, 'Array of channel ids for the card', required: false
  param :topics, Array, 'Array of topics for the card', required: false
  param :image_url, String, 'New image for the stream'
  # TODO: move to api/v2
  def update
    @video_stream = VideoStream.find(params[:id])
    ActiveRecord::Base.transaction do
      video_stream_update_params = params.permit(:is_official, :name)
      @video_stream.update_attributes!(video_stream_update_params)

      # Channel Updates
      if params.has_key?(:channel_ids)
        channels = current_org.channels.where(id: params[:channel_ids])
        @video_stream.channels = channels
      end

      # Topic Updates
      if params.has_key?(:topics)
        tag_objects = params[:topics].present? ? (params[:topics].map { |t| Tag.find_or_create_by(name: t) }) : []
        @video_stream.tags = tag_objects.uniq
      end

      if params[:image_url].present?
        fs_url = Filestack::Security.read_filestack_url(params[:image_url])
        image_url = Filestack::Security.new.signed_url(fs_url)

        @video_stream&.file_resource&.destroy!

        fr = FileResource.new(attachable: @video_stream.card, attachment: image_url)
        fr.save!
      end
    end

    if @video_stream.errors.present?
      return render json: {message: @video_stream.errors.full_messages.join(', ')}, status: :unprocessable_entity
    end

    @video_stream.reload
    render 'cms/api/video_streams/show', status: :ok and return
  end

  private

  def perform_state_change video_stream_ids, event_method
    vs = VideoStream.where(id: video_stream_ids)
    vs.each do |v|
      begin
        v.send(event_method)
      rescue
      end
    end
  end
end

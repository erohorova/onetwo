class Cms::Api::CoursesController < Cms::BaseController
  include LimitOffsetConcern
  has_limit_offset_constraints only: [:index]

  api :GET, "cms/api/courses", "Get courses in an organization for the cms, sorted by newest first"
  param :q, String, "Search term", required: false
  param :limit, Integer, "Limit for pagination, default 10", required: false
  param :offset, Integer, "Offset for pagination, default 0", required: false
  param :promoted, String, "If true then only show promoted courses", required: false
  param :sort, String, "Attribute name to sort by, ex: created_at, default: created_at"
  param :order, String, "Sort order, asc or desc, default: desc"
  formats ['json']
  def index
    pp = params.permit(:q, :limit, :offset, :promoted, :sort, :order)
    query = pp[:q] || ''

    filter_params = {}
    if pp.has_key?(:promoted)
      filter_params[:is_promoted] = pp[:promoted]
    end

    es_response = Search::CourseSearch.new.search_for_cms(
        query,
        current_org.id,
        filter_params: filter_params,
        offset: offset,
        limit: limit,
        sort: build_sort_params(pp)
    )
    @courses = es_response.results
    @total_count = es_response.total
  end

  api :POST, "cms/api/courses/promote", "Promote a list of courses"
  param :course_ids, Array, "Array of course ids to promote", required: false
  formats ['json']
  def promote
    perform_state_change params[:course_ids], :promote!
    head :ok and return
  end

  api :POST, "cms/api/courses/unpromote", "Unpromote a list of courses"
  param :course_ids, Array, "Array of course ids to unpromote", required: false
  formats ['json']
  def unpromote
    perform_state_change params[:course_ids], :unpromote!
    head :ok and return
  end

  api :POST, "cms/api/courses/hide", "Hide a list of courses"
  param :course_ids, Array, "Array of course ids to hide", required: false
  formats ['json']
  def hide
    perform_state_change params[:course_ids], :hide!
    head :ok and return
  end


  api :POST, "cms/api/courses/unhide", "Unhide a list of courses"
  param :course_ids, Array, "Array of course ids to unhide", required: false
  formats ['json']
  def unhide
    perform_state_change params[:course_ids], :unhide!
    head :ok and return
  end

  api :PATCH, "cms/api/courses/:id", "Update the course details"
  param :is_promoted, String, "promote or unpromote the course", required: false
  param :is_hidden, String, "hide or unhide the course", required: false
  def update
    @course = ResourceGroup.find_by(client_resource_id: params[:id])
    ActiveRecord::Base.transaction do
      course_update_params = params.permit(:is_promoted, :is_hidden, :website_url, :image_url, :name, :description)
      course_update_params[:details_locked] = true
      @course.update_attributes!(course_update_params)

      # Channel Updates
      if params.has_key?(:channel_ids)
        channels = current_org.channels.where(id: params[:channel_ids])
        @course.channels = channels
      end

      # Topic Updates
      if params.has_key?(:topics)
        tag_objects = params[:topics].present? ? (params[:topics].map{|t| Tag.find_or_create_by(name: t)}) : []
        @course.tags = tag_objects.uniq
      end

    end
    @course.reload
    render "cms/api/courses/show", status: :ok and return
  end

  api :GET, "cms/api/courses/:id", "Get the course details"
  formats ['json']
  def show
    @course = ResourceGroup.find_by(client_resource_id: params[:id])
  end

  private
  def perform_state_change(courses_ids, event_method)
    courses = ResourceGroup.where(id: courses_ids)
    courses.each do |course|
      begin
        course.send(event_method)
      rescue => e
        log.error e
        log.error "error updating state #{course.full_errors}"
      end
    end
  end
end

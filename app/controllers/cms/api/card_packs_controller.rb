class Cms::Api::CardPacksController < Cms::BaseController
  include LimitOffsetConcern
  has_limit_offset_constraints only: [:index]

  api :GET, "cms/api/collection", "Get pathways in an organization for the cms, sorted by newest first"
  param :q, String, "Search term", required: false
  param :sort, String, :desc => "Attribute name to sort by, ex title, created_at"
  param :order, String, :desc => "Sort order, ASC or DESC"
  param :limit, Integer, "Limit for pagination, default 10", required: false
  param :offset, Integer, "Offset for pagination, default 0", required: false
  param :is_official, Integer, desc: 'filter to get official pathways, use true or false', required: false
  param :state, String, "One of 'new', 'published', 'archived', 'deleted'. Defaults to all"
  param :topics, Array, "Array of topic strings to filter for"
  param :channel_ids, Array, "Array of channel ids to filter for"
  formats ['json']
  def index
    pp = params.permit(:q, :sort, :order, :limit, :offset, :is_official, :state, topics: [], channel_ids: [])
    query = pp[:q] || ''
    filter_params = {}

    simple_filters = [:state, :topics, :channel_ids]

    simple_filters.each do |filter_key|
      filter_params[filter_key] = pp[filter_key] if pp.has_key?(filter_key)
    end

    if !pp[:is_official].blank?
      filter_params[:is_official] = pp[:is_official] == "true"
    end

    es_response = Search::CardSearch.new.search_pathways_for_cms(
        query,
        current_org.id,
        filter_params: filter_params,
        offset: offset,
        limit: limit,
        sort: build_sort_params(pp)
    )
    @cards = es_response.results
    @total_count = es_response.total
    augment_card_data
  end

  api :PUT, "cms/api/collection/:id", "Update pathway with channels and topics"
  param :channel_ids, Array, "Array of channel ids for the pack", required: false
  param :topics, Array, "Array of topics for the pack", required: false
  def update
    # refer: http://stackoverflow.com/questions/20164354/rails-strong-parameters-with-empty-arrays
    params[:channel_ids] ||= [] if params.has_key?(:channel_ids)
    params[:topics] ||= [] if params.has_key?(:topics)

    pp = params.permit(:is_official,:user_taxonomy_topics, channel_ids: [], topics: [], user_taxonomy_topics: [:path, :label])
    @card = Card.where(id: params[:id], organization: current_org).first!
    ActiveRecord::Base.transaction do
      # Channel Updates
      @card.channels = current_org.channels.where(id: pp[:channel_ids]) if pp.has_key?(:channel_ids)

      # Topics Update
      @card.tags = pp[:topics].map{|t| Tag.find_or_create_by(name: t)}.uniq if pp.has_key?(:topics)
      if pp.has_key?(:user_taxonomy_topics)
        @card.update_attributes(user_taxonomy_topics: pp[:user_taxonomy_topics])
      end

      if pp.has_key?(:is_official)
        @card.update_attributes(is_official: pp[:is_official])
      else
        @card.touch # Need for invoking reindex
      end
    end
    log.info "Calling ContentsBulkDequeue Job via cards_pack_controller for id #{@card.id}"
    @card.remove_from_queue_and_reenqueue #When Channels are updated, all cards are reenqueued to its followers
    render "cms/api/cards/show", status: :ok and return
  end

  api :POST, "cms/api/collection/publish", "Publish an array of card_packs from the cms"
  param :card_ids, Array, "Array of card ids to publish", required: false
  formats ['json']
  def publish
    published_ids = perform_state_change params[:card_ids], :publish!
    published_ids.each do |published_id|
      Cms::Judgement.create_content_trail published_id, params[:action], current_user,"Card"
    end
    render json: {published_ids: published_ids} and return
  end

  api :POST, "cms/api/collection/delete", "Delete an array of card_packs from the cms"
  param :card_ids, Array, "Array of card ids to delete", required: false
  formats ['json']
  def delete
    current_org.cards.where(id: params[:card_ids]).destroy_all
    deleted_ids = params[:card_ids].map(&:to_i) - current_org.cards.where(id: params[:card_ids]).pluck(:id)
    deleted_ids.each do |deleted_id|
      Cms::Judgement.create_content_trail deleted_id, params[:action], current_user, "Card"
    end
    render json: {deleted_ids: deleted_ids} and return
  end
end

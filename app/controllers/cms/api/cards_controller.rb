class Cms::Api::CardsController < Cms::BaseController
  include LimitOffsetConcern
  include TrackActions

  has_limit_offset_constraints only: [:index]

  api :GET, "cms/api/cards", "Get insights in an organization for the cms, sorted by newest first"
  param :q, String, "Search term", required: false
  param :sort, String, :desc => "Attribute name to sort by, ex title, created_at"
  param :order, String, :desc => "Sort order, ASC or DESC"
  param :limit, Integer, "Limit for pagination, default 10", required: false
  param :offset, Integer, "Offset for pagination, default 0", required: false
  param :state, String, "One of 'new', 'published', 'archived', 'deleted'. Defaults to all"
  param :type, String, "One of 'article', 'video', 'photo', 'question'. Defaults to all"
  param :topics, Array, "Array of topic strings to filter for"
  param :channel_ids, Array, "Array of channel ids to filter for"
  param :ugc_creator_handle, String, "@handle of creator to filter"
  param :cms_creator_handle, String, "Source Name to filter"
  param :ugc_only, String, "Search only UGC content"
  param :cms_only, String, "Search only CMS content"
  formats ['json']
  def index
    pp = params.permit(:q, :sort, :order, :limit, :offset, :state, :ugc_creator_handle, :cms_creator_handle, :ugc_only, :cms_only, :type, topics: [], channel_ids: [])
    query = pp[:q] || ''

    es_response = Search::CardSearch.new.search_for_cms(
        query,
        current_org.id,
        filter_params: build_filter_params(pp),
        offset: offset,
        limit: limit,
        sort: build_sort_params(pp)
    )
    @cards = es_response.results
    @total_count = es_response.total
    augment_card_data
  end

  api :GET, "cms/api/cards/:id/history", "Get history of specific card"
  param :id, Integer, "Id of the card we want history of", required: true
  formats ['json']
  def history
    history = []
    card = Card.find(params[:id])
    judgements = Cms::Judgement.where(content_item_id: card.id, content_item_type: "Card")
    judgements.each do |j|
      user = User.find(j.user_id)
      history << {"user":user.handle, "date":j.created_at, "action":j.action}
    end
    if history.length == 0
      history << {"user": card.author ? "Creator: #{card.author.handle}" : "CMS: Autopublished", "date":card.created_at, "action":card.state}
    end
    render json: history and return
  end

  api :POST, "cms/api/cards/publish/(all)", "Publish an array of cards from the cms"
  param :card_ids, Array, "Array of card ids to publish or all", required: false
  param :query, String, "Query params: q, state, type, topics"
  formats ['json']
  def publish
    if params[:card_ids].kind_of?(String) && params[:card_ids].downcase == "all"
      @cards = query_all_specific_cards(params)
      @cards.each do |c|
        card = Card.find(c.id)
        card.update(state: "published")
        track_action(card, 'content_publish')
      end
      Cms::Judgement.create_content_trail current_org.cards.first.id, params[:action], current_user, "Card"
      render json: {"success": true} and return
    else
      published_ids = perform_state_change params[:card_ids], :publish!
      published_ids.each do |published_id|
        Cms::Judgement.create_content_trail published_id, params[:action], current_user, "Card"
        card = Card.find(published_id)
        track_action(card, 'content_publish')
      end
      render json: {ids: published_ids} and return
    end
  end

  api :POST, "cms/api/cards/archive/(all)", "Archive an array of cards from the cms"
  param :card_ids, Array, "Array of card ids to archive or all", required: false
  param :query, String, "Query params: q, state, type, topics"
  formats ['json']
  def archive
    if params[:card_ids].kind_of?(String) && params[:card_ids].downcase == "all"
      @cards = query_all_specific_cards(params)
      @cards.each do |c|
        Card.find(c.id).update(state: "archived")
      end
      Cms::Judgement.create_content_trail current_org.cards.first.id, params[:action], current_user, "Card"
      render json: {"success":true} and return
    else
      archived_ids = perform_state_change params[:card_ids], :archive!
      archived_ids.each do |archived_id|
        Cms::Judgement.create_content_trail archived_id, params[:action], current_user, "Card"
      end
      render json: {ids: archived_ids} and return
    end
  end

  api :POST, "cms/api/cards/delete/(all)", "Delete an array of cards from the cms"
  param :card_ids, Array, "Array of card ids to delete or all", required: false
  param :query, String, "Query params: q, state, type, topics"
  formats ['json']
  def delete
    if params[:card_ids].kind_of?(String) && params[:card_ids].downcase == "all"
      @cards = query_all_specific_cards(params)
      current_org.cards.where(id: @cards.map(&:id)).find_each do |card|
        card.current_user = current_user
        card.destroy
      end

      Cms::Judgement.create_content_trail current_org.cards.first.id, params[:action], current_user, "Card"
      render json: {"success": true} and return
    else
      current_org.cards.where(id: params[:card_ids]).find_each do |card|
        card.current_user = current_user
        card.destroy
      end
      deleted_ids = params[:card_ids].map(&:to_i) - current_org.cards.where(id: params[:card_ids]).pluck(:id)

      deleted_ids.each do |deleted_id|
        Cms::Judgement.create_content_trail deleted_id, params[:action], current_user, "Card"
      end
      render json: {ids: deleted_ids} and return
    end
  end

  api :PATCH, "cms/api/cards/:id", "Update a card"
  param :id, Integer, "Card id to be updated", required: true
  param :title, String, "Title for the card", required: false
  param :message, String, "Message for the card", required: false
  param :channel_ids, Array, "Array of channel ids for the card", required: false
  param :topics, Array, "Array of topics for the card", required: false
  param :is_official, String, "Set to true if this content should show up in the featured tab", required: false
  param :resource, Hash, "Hash of resource attributes", required: false do
    param :title, String, "New link title"
    param :description, String, "New link description"
    param :image_url, String, "New image url"
  end
  param :duration, String, "Duration for the card", required: false
  param :is_paid, String, "Price required for card."
  param :prices_attributes, Array, "Array of price attributes", required: false do
    param :id, Integer, "Id of price record"
    param :currency, String, "Currency of amount Default: USD"
    param :amount, String, "Amount of card"
  end
  def update
    @card = load_card_from_compound_id(params[:id])

    update_params = card_update_params
    update_params[:prices_attributes] = add_price_details(update_params)

    if topic_changed?(params) || update_params[:prices_attributes].present?
      @card.ecl_update_required = true
    end

    ActiveRecord::Base.transaction do
      @card.update_attributes(update_params)

      # Channel Updates
      if params.has_key?(:channel_ids)
        channels = current_org.channels.where(id: params[:channel_ids])
        @card.channels = channels
      end

      # Topic Updates
      # TODO: refactor this once topics PR merged
      if params.has_key?(:topics)
        tag_objects = params[:topics].present? ? (params[:topics].map{|t| Tag.find_or_create_by(name: t)}) : []
        @card.tags = tag_objects.uniq
      end

      # Resource image update
      # If the initial card is from a file upload (UGC image cards), then that is stored in card.file_resources.first
      # All other types (link, video) are in card.resource
      if params.has_key?(:resource)
        resource_update_params = params[:resource].permit(:title, :description, :image_url)
        if !@card.file_resources.blank?
          @card.file_resources.destroy_all
          unless resource_update_params[:image_url].nil?
            @card.file_resources << FileResource.new(attachment: resource_update_params[:image_url])
          end
        elsif !@card.resource.nil?
          @card.resource.update_attributes(resource_update_params)
        end
      end
    end
    log.info "Calling ContentsBulkDequeue Job via cards_controller for id #{@card.id}"
    @card.remove_from_queue_and_reenqueue #When Channels are updated, all cards are reenqueued to its followers
    @card.reload
    render "cms/api/cards/show", status: :ok and return
  end

  def topic_changed?(card_attrs)
    card_attrs[:topics] && (@card.topics_label.sort != card_attrs[:topics].sort)
  end

  private

  def build_filter_params(pars)
    filter_params = {card_type: ['media', 'course', 'poll'], state: ['new','archived','published']}
    simple_filters = [:topics, :channel_ids, :ugc_creator_handle, :cms_creator_handle, :ugc_only, :cms_only]

    simple_filters.each do |filter_key|
      filter_params[filter_key] = pars[filter_key] if pars.has_key?(filter_key)
    end

    if pars[:state]
      filter_params[:state] = ["#{pars[:state]}"]
    end

    # convert params[:type] in to card_type and card_template filters
    filter_params.merge!(
      case pars[:type]
      when 'article' then {card_template: ['link']}
      when 'video' then {card_template: ['video']}
      when 'question' then {card_template: ['text']}
      when 'photo' then {card_template: ['image']}
      when nil then {}
      end
    )
    filter_params
  end

  def query_all_specific_cards(pars)
    pp = pars.permit(:q, :state, :type, topics: [])
    query = pp[:q] || ''

    pp[:state] = pp[:state] || ["new","published","archived"]

    es_response = Search::CardSearch.new.search_for_cms(
      query,
      current_org.id,
      filter_params: build_filter_params(pp)
    )
    es_response.results
  end

  def card_update_params
    params.permit(:title, :message, :is_official, :duration, :price_required, :is_paid,
                  prices_attributes: [:amount, :currency, :id, :_destroy])
  end

  #to set org_id and ecl_id of card
  def add_price_details(update_params)
    if update_params[:is_paid]&.to_bool && update_params[:prices_attributes].present?
      update_params[:prices_attributes].map do |price|
        price.merge!({ecl_id: @card.ecl_id})
      end
    else
      []
    end
  end

end
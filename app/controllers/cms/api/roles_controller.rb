class Cms::Api::RolesController < Cms::BaseController

  before_action :set_role, :only=>[:update, :show, :assign_permissions, :destroy, :add_users, :remove_users]

  api :GET, "cms/api/roles", "Get roles in an organization for the cms"
  formats ['json']
  def index
    @roles = current_org.roles.includes(:role_permissions, :users)
  end

  api :POST, "cms/api/roles", "Create Role"
  param :name, String, :desc => "role name", :required => true
  def create
    @role = current_org.roles.new(role_params)
    if @role.save
      render 'show'
    else
      render json: {errors: @role.errors.full_messages}, status: :unprocessable_entity
    end
  end

  api :PUT, "cms/api/roles/:id", "Update Role"
  param :name ,String, :desc => "role name", :required => false
  param :id, Integer, :desc => "role id", :required => true
  def update
    if @role.update(role_params)
      render 'show'
    else
      render json: {errors: @role.errors.full_messages}, status: :unprocessable_entity
    end
  end

  api :GET, "cms/api/roles/:id", "Show Role"
  def show
  end

  api :DELETE, "cms/api/roles/:id", "Delete a Role"
  param :id, Integer, :desc => "role id", :required => true
  def destroy
    if @role.destroy
      head :ok and return
    else
      head :unprocessable_entity and return
    end
  end

  api :POST, "cms/api/roles/:id/assign_permissions", "Assign Permissions"
  param :permission_ids,Array,:desc=>"Pemission Ids",:required=>true
  def assign_permissions
    # For uncheckd all permission  Blank Permission
    permission_params = role_permission_params.blank? ? {:permissions=>[]} : role_permission_params
    if @role.update_permissions(permission_params[:permissions])
      render 'show'
    else
      render json: {errors: @role.errors.full_messages}, status: :unprocessable_entity
    end
  end

  api :POST, "cms/api/roles/:id/add_users", "Add users to a role"
  param :id, Integer, :desc => "role id", :required => true
  param :user_ids, Array, :desc=>"Ids of the users being added", :required=>true
  def add_users
    current_org.users.where(id: params[:user_ids]).each do |user|
      @role.add_user(user)
    end

    head :ok and return
  end

  api :POST, "cms/api/roles/:id/remove_users", "Remove users from a role"
  param :id, Integer, :desc => "role id", :required => true
  param :user_ids, Array, :desc=>"Ids of the users being removed", :required=>true
  def remove_users
    current_org.users.where(id: params[:user_ids]).each do |user|
      @role.remove_user(user)
    end

    head :ok and return
  end

  protected

  def role_params
    params.permit(:name)
  end

  def role_permission_params
    params.permit(:permissions=>[])
  end

  def set_role
    @role = current_org.roles.find(params[:id])
  end

end

require 'open-uri'
require 'csv'

class Cms::Api::UsersController < Cms::BaseController

  resource_description do
    formats ['json']
    error code: 404, desc: "Missing :id"
  end

  api :PUT, "cms/api/users/:id", "PUT updates for users"
  param :id, Integer, "Id of the record to be updated", required: true
  example "Update specific user:\nPUT /cms/api/users/12 \n{showcase:true} \n\nResponse (200):\n{result:'success'} "
  example "Valid Params:\n{showcase: true|false}"
  def update
    user_params = params.permit(
      :showcase
      )

    # Find a specific user
    @user = User.where(id: params[:id]).first!

    # Update the params
    ActiveRecord::Base.transaction do
      @user.update_attributes(user_params)
    end

    render json: {"result" => "success"}, status: :ok and return
  end

  api :PUT, "cms/api/users/:id/assign_roles", "PUT assign roles"
  param :id, Integer, "User id", required: true
  param :role_ids, Array, "role ids to be assigned", required: true
  example "Assign role to specific user:\nPUT /cms/api/users/12 \n{role_ids:[1,2]} \n\nResponse (200):\n{result:'success'} "

  def assign_roles
    @user = User.find(params[:id])
    role_ids = []

    # Validate Assigning  Org Roles
    # TODO We can move to concern (already we have assign roles methods)
    # Based on UI we need to update this method
    # currently assuming all role ids will pass from frontend
    if user_role_params.present?
      role_ids = current_org.roles.where(:id=>user_role_params[:role_ids]).pluck(:id)
    end

    # NOTE: update(role_ids: role_ids) doesn't trigger destroy callbacks on
    # user_role model. Manually triggering destroy to capture event

    # added roles
    added_role_ids = role_ids - @user.roles.pluck(:id)
    # removed roles
    removed_role_ids = @user.roles.pluck(:id) - role_ids

    @user.user_roles.where(role_id: removed_role_ids).map(&:destroy)
    user_role_ids = @user.roles.pluck(:id)

    if @user.update(:role_ids=>(added_role_ids | user_role_ids), skip_indexing: true)
      if @user.roles.collect(&:default_name).include?(Role::TYPE_ADMIN)
        @user.set_org_role(Role::TYPE_ADMIN)
      elsif @user.roles.collect(&:default_name).include?(Role::TYPE_MEMBER)
        @user.set_org_role(Role::TYPE_MEMBER)
      end
      render json: {message: "Successfully assign roles"}
    else
      render json: {errors: @user.errors.full_messages}, status: :unprocessable_entity
    end
  end

  api :POST, "cms/api/users/preview_bulk_import", "Send bulk import preview details"
  param :csv_file_url, String, desc: "Url of CSV file uploaded to S3"
  param :emails, Array, desc: "Array of emails"
  param :team_id, Integer, desc: 'Team ID through which users will be invited'
  param :user_details, Hash, desc: "details of users like first_name, last_name and email" do
    param :first_name, String, desc: "first_name of user"
    param :last_name, String, desc: "last_name of user"
    param :email, String, desc: "email of user"
  end
  param :groups, String, desc: "Comma separated group names", required: false
  def preview_bulk_import
    status, data = params['csv_file_url'].present? ? csv_data : generate_csv_data_from_params
    unless status
      render json: { status: false, message: data } and return
    end

    invitation_file = InvitationFile.new(sender_id: current_user.id,
                                         invitable: current_org,
                                         data: data,
                                         roles: "member",
                                         skip_processing_invitations: true)
    if invitation_file.save
      status, message = true, "CSV is successfully uploaded"
      if params[:bulk_suspend]
        users = BulkSuspendUsers.new(file_id: invitation_file.id).preview_csv
      else
        users = BulkImportUsers.new(file_id: invitation_file.id).preview_csv
      end
    end
    render json: { status: status, message: message, file_id: invitation_file.try(:id), users: users }
  end

  api :PUT, "cms/api/users/bulk_suspend_upload", "Update users status from CSV file"
  param :file_id, Integer, "Id of invitation file obtained using the preview api 'cms/api/users/preview_bulk_import'"
  def bulk_suspend_upload
    BulkSuspendUsersJob.perform_later(
        file_id: params[:file_id],
        admin_id: current_user.id
      )
    render json: {}
  end


  api :PUT, "cms/api/users/bulk_upload", "Upload users from CSV file"
  param :file_id, Integer, "Id of invitation file obtained using the preview api 'cms/api/users/preview_bulk_import'"
  param :send_welcome_email, String, desc: "Boolean, send welcome email to new users, default: false"
  param :channel_ids, Array, desc: "Array of channel ids."
  param :team_id, Integer, desc: "Team id to which user will be imported into an org from"
  param :team_ids, Array, desc: "Array of team ids."
  param :revised_user_import, [true, false], desc: 'Decides version of bulk import to be used'
  param :onboarding_options, Hash, desc: "Hash of onboarding_options i.e { profile_setup: 1, ... }"
  def bulk_upload
    opts = {
      channel_ids: params[:channel_ids],
      invitable:   params[:invitable],
      onboarding_options: params[:onboarding_options],
      send_welcome_email: params[:send_welcome_email],
      team_ids:    params[:team_ids]
    }

    # In revised version of bulk import users, we use 'send_invite_email' to send welcome email to user.
    # But when we preview_bulk_import than value of 'send_invite_email' value is always true irrespective of
    # value you selected while importing user.
    # To update the value of 'send_invite_email' making below chnages.
    @file = InvitationFile.find_by(id: params[:file_id])

    @file.update(send_invite_email: params[:send_welcome_email])

    # Case 1: Importing users in a group. Team invitation from edc-web.
    # Case 2: Inviting SSO users. Invitations from CMS.
    if params[:invitable] || params[:team_id].present?
      BulkImportUsersJob.perform_later(
        file_id: params[:file_id],
        options: opts,
        admin_id: current_user.id)

      render json: {} and return
    end

    case params[:user_import_version]
    when 'v1'
      BulkImportUsersJob.perform_later(
        file_id: params[:file_id],
        options: opts,
        admin_id: current_user.id)
    when 'v2'
      BulkImportUsersJobV2.perform_later(
        file_id: params[:file_id],
        options: opts,
        admin_id: current_user.id
      )
    when 'v3'
      BulkImportUsersV3Job.perform_later(
        file_id: params[:file_id],
        options: opts,
        organization_id: current_org.id
      )
    end

    render json: {}
  end

  protected
    def csv_data
      csv_file_url = params['csv_file_url']
      if csv_file_url =~ /.csv/
        begin
          csv_data = Faraday.get(csv_file_url).body

          csv_data.force_encoding("UTF-8")
          if params[:bulk_suspend]
            header_error = BulkSuspendUsers.csv_headers_validation(csv_data) 
          else
            header_error = BulkImportUsers.csv_headers_validation(csv_data)
          end
          if header_error
            status, message = false, header_error
          else
            status, message = true, csv_data
          end
        rescue => e
          status, message = false, e.message
        end
      else
        status, message = false, "Invalid file url extension."
      end
    end

    def generate_csv_data_from_params
      if params["user_details"].present?
        return [false, 'User details first_name, last_name and email all are mandatory.'] if params["user_details"]["first_name"].blank? || params["user_details"]["last_name"].blank? || params["user_details"]["email"].blank?
        csv_data = BulkImportUsers.csv_formatted_user_details(params['user_details'], params['groups'])
        status, message = true, csv_data
      elsif params['emails'].present?
        csv_data = BulkImportUsers.csv_formatted_user_emails(params['emails'], params['groups'])
        status, message = true, csv_data
      else
        status, message = false, 'Emails must be provided'
      end
    end

    def user_role_params
      params.permit(role_ids: [])
    end
end

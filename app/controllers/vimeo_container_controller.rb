class VimeoContainerController < ApiController

  def show
    @video_id = params[:id].to_i
    @embed_html = nil
    begin
      vimeo_video_metadata_url = "http://vimeo.com/api/oembed.json?" + { url: "http://vimeo.com/#{@video_id}" }.to_query
      resp = Faraday.get(vimeo_video_metadata_url)
      vimeo_hash = JSON.parse(resp.body)

      vimeo_embed_code = vimeo_hash['html']
      if vimeo_embed_code
        @embed_html = vimeo_embed_code.insert(vimeo_embed_code.index("src"), "style=\"position:absolute;top:0;left:0;height:100%;width:100%\" ")
      end
    rescue => e
      log.error "Error getting vimeo embed url for video id: #{@video_id}"
    end
  end
end

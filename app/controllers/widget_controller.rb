class WidgetController < ActionController::Base
  include NativeAppDetection
  include DetectPlatform

  before_action :custom_authenticate_user!, only: [:share]
  before_action :authenticate_user!, only: [:preview]
  # Prevent CSRF attacks
  protect_from_forgery with: :null_session

  # The learn button widget
  def learn_button
    @url = "#{root_url}#{params[:username]}"
    @text = params[:text] || 'Learn'
  end

  def share
    @redirect_uri = params[:redirect_uri]
    @link = params[:link]
    @res = Resource.extract_resource @link
    render 'share', layout: 'popup'
  end

  def preview
    @res = Resource.extract_resource params[:link]
    if @res.nil?
      head :bad_request and return
    end
    render partial: 'resources/details', locals: {resource: @res}
  end

  def share_demo
  end

  def share_button
    @url = share_dialog_url(link: params[:link] || request.referrer, redirect_uri: params['redirect-uri'] || params['redirect_uri'], display: params[:display] || 'popup')
    @text = params[:text] || 'Share'
  end

  private

  def custom_authenticate_user!
    unless user_signed_in?
      redirect_to new_user_session_path(display: params[:display], next: request.original_url)
    end
  end
end

class GroupsController < ApiController
  include Entry
  include SavannahAuthentication, CourseRetriever

  skip_before_filter :cm_auth_user, only: [:index, :update, :clear_posts, :preferences,
                                           :data_dump, :upload_s3_urls, :add_user, :sync_course_data, :channels, :forum_data]
  before_filter :authenticate_user!, only: [:index, :update, :clear_posts, :data_dump, :search]
  before_filter :authenticate_admin!, only: [:upload_s3_urls, :sync_course_data]
  before_filter :authenticate_from_savannah!, only: [:close,:add_user,:remove_user, :channels, :forum_data]
  before_filter :find_group, only: [:tags, :top_tags, :stats, :right_rail_stats, :contexts, :topic_stats]

  api :GET, "groups/search", "Search for groups"
  param :q, String, :desc => "Search query", :required => true
  formats ['json']
  def search
    render json: [] and return if params[:q].nil?
    limit = params[:limit].nil? ? 10 : params[:limit].to_i
    offset = params[:offset].nil? ? 0 : params[:offset].to_i
    @groups = ResourceGroup.search params[:q], limit: limit, offset: offset
  end

  # DEPRECATED
  # Use api/suggest instead. Currently still being used by Forum - Jan 4 2016
  api :GET, "groups/:id/tags", "Get tags for a group"
  param :id, String, :desc => "Id of concerned group", :required => true
  formats ['json']
  def tags
    @tags = @group.get_tags(@group.created_at, Time.now)
  end

  api :GET, "groups/:id/top_tags", "Get top 3 tags for a group"
  param :id, String, :desc => "Id of concerned group", :required => true
  param :count, Integer, :desc => "No of tags returned default is 3", :required => false
  formats ['json']
  def top_tags
    count = params[:count] && params[:count] > 0 ? params[:count].to_i : 3
    @top_tags = @group.get_top_tags(@group.created_at, Time.now, count)
  end

  api :GET, "groups/:id/stats", "Get stats for a group"
  param :id, String, :desc => 'Id of concerned group', :required => true
  formats ['json']
  def stats
    @user = current_user
    @stats = @group.get_stats(@user, Time.now.utc.to_date)
  end

  api :GET, "groups/:id/right_rail_stats", "Get right rail stats for a group"
  param :id, String, :desc => 'Id of concerned group', :required => true
  formats ['json']
  def right_rail_stats
    @user = current_user
    @trending_labels = @group.right_rail_stats
  end

  api :GET, "groups/:id/contexts", "Get contexts for a group"
  param :id, String, :desc => 'Id of concerned group', :required => true
  formats ['json']
  def contexts
    @user = current_user
    @trending_labels = @group.contexts
    render 'groups/right_rail_stats'
  end

  api :GET, "groups/:id/topic_stats", "Get right rail stats for a group"
  param :id, String, :desc => 'Id of concerned group', :required => true
  param :topic_type, String, :desc => 'topic type -> one of [tag, context]', :required => true
  param :topic_id, String, :desc => 'topic id -> topic id', :required => true
  formats ['json']
  def topic_stats
    @user = current_user
    if params[:topic_type] == 'tag'
      @stats = @group.tag_stats(@user, params[:topic_id])
    elsif params[:topic_type] == 'context'
      @stats = @group.context_stats(@user, params[:topic_id])
    else
      head :bad_request and return
    end
  end

  api :GET, "addon_stats", "Get stats for an externally provisioned resource"
  param :api_key, String, desc: 'API key for client that owns the resource', :required => true
  param :resource_id, String, desc: 'Id of concerned resource/group', :required => true
  param :user_id, String, desc: 'Client user id', :required => true
  param :days, Integer, desc: 'Number of days to include', :required => true
  param :to_date, String, desc: 'End date for date range; default today'
  param :token, String, desc: 'SHA256 token of "shared_secret|user_id|resource_id"', :required => true
  formats ['json']
  def addon_stats
    head :bad_request and return if params[:days].blank?
    days = params[:days].to_i
    head :unprocessable_entity and return if days < 1
    @group = Group.by_client_resource_id(params: params, organization_id: @current_org.id)
    head :unprocessable_entity and return if @group.nil?
    if params.key?(:to_date)
      begin
        date = DateTime.parse(params[:to_date]).to_date
      rescue ArgumentError
        head :unprocessable_entity and return
      end
    else
      date = DateTime.now.utc.to_date
    end
    @stats = DailyGroupStat.get_daily_stats(@group.id, date, days)
  end

  api :GET, "forum_data", "Get Forum Report for an externally provisioned resource"
  param :api_key, String, desc: 'API key for client that owns the resource', :required => true
  param :resource_id, String, desc: 'Id of concerned resource/group', :required => true
  param :from_date, Integer, desc: 'Start date for date range; default course_created date'
  param :to_date, String, desc: 'End date for date range; default today'
  param :token, String, desc: 'SHA256 token of "shared_secret|user_id|user_email|user_role|resource_id"', :required => true
  formats ['json']
  def forum_data
    @group = Group.find_by(client_resource_id: params[:resource_id])
    head :not_found and return if @group.nil?
    if params.key?(:from_date) && params.key?(:to_date)
      begin
        from_date = DateTime.parse(params[:from_date]).to_date
        to_date = DateTime.parse(params[:to_date]).to_date
      rescue ArgumentError
        head :unprocessable_entity and return
      end
    else
      from_date = @group.created_at.to_date
      to_date = DateTime.now.utc.to_date
    end
    @forum_data = ForumReportService.new.get_forum_data(@group.id, from_date, to_date)
  end

  api :GET, "channels", "Get all promoted channels for this group"
  param :api_key, String, desc: 'API key for client that owns the resource', :required => true
  param :resource_id, String, desc: 'Id of concerned resource/group', :required => true
  param :user_id, String, desc: 'Client user id', :required => true
  param :user_email, String, desc: 'email id of user', :required => false
  param :token, String, desc: 'SHA256 token of "shared_secret|user_id|user_email|user_role|resource_id"', :required => true
  formats ['json']
  def channels
    pars = @jwt_payload || params
    @group = @current_org.groups.where(client_resource_id: pars["resource_id"]).first
    head :unprocessable_entity and return if @group.nil?
    @channels = @group.channels.not_private
    render 'channels'
  end

  alias_method :forum_channels, :channels

  api :GET, "remove_user", "Unenroll a user from a particular group"
  param :api_key, String, desc: 'API key for client that owns the resource', :required => true
  param :resource_id, String, desc: 'Id of concerned resource/group', :required => true
  param :user_id, String, desc: 'Client user id', :required => true
  param :user_email, String, desc: 'email id of user', :required => false
  param :token, String, desc: 'SHA256 token of "shared_secret|user_id|user_email|user_role|resource_id"', :required => true
  formats ['json']
  def remove_user
    @group = Group.by_client_resource_id(params: @jwt_payload, organization_id: @current_org.id)
    head :unprocessable_entity and return if @group.nil?

    gu = GroupsUser.where(group: @group, user: current_user).first
    if gu
      gu.destroy
      Follow.remove_followers(@group.posts, gu.user)
      head :ok and return
    else
      head :unprocessable_entity and return
    end
  end

  api :POST, "add_user", "Enroll a user in a particular group"
  param :api_key, String, desc: 'API key for client that owns the resource', :required => true
  param :resource_id, String, desc: 'Id of concerned resource/group', :required => true
  param :user_id, String, desc: 'Client user id', :required => true
  param :user_email, String, desc: 'email id of user', :required => false
  param :user_role, String, desc: 'user role, default is member', :required => false
  param :token, String, desc: 'SHA256 token of "shared_secret|user_id|user_email|user_role|resource_id"', :required => true
  formats ['json']
  def add_user
    prepare_data(@jwt_payload)
    if not performed?
      head :ok and return
    end
  end

  api :POST, "sync_course_data", "Create or update group"
  param :api_key, String, desc: 'API key for client that owns the resource', :required => true
  param :resource_id, String, desc: 'Id of concerned resource/group', :required => true
  param :token, String, desc: 'token used by savannah', :required => true
  param :resource_name, String, desc: "Name of new resource / group", :required => false
  param :resource_description, String, desc: "Description of new resource / group", :required => false
  param :resource_is_private, String, desc: "Whether the course is public or not", :required => false
  param :resource_course_term, String, desc: "course term. such as Fall 2015", :required => false
  param :resource_image_url, String, desc: "course logo", :required => false
  param :resource_website_url, String, desc: "course website url", :required => false
  param :resource_start_date, String, desc: "Course start date", required: false
  param :resource_end_date, String, desc: "Course end date", required: false
  param :promotion_data, Hash, desc: "Promotional data", required: false
  param :resource_course_code, String, desc: "Course code", required: false
  formats ['json']
  def sync_course_data
    unless (@jwt_payload[:api_key].present? || @jwt_payload[:savannah_app_id].present?)
      head :bad_request and return
    end

    if @current_org.nil?
      log.error("Org is not present")
      head :unauthorized and return
    end

    start_date = nil
    begin
      start_date = Date.parse(@jwt_payload[:resource_start_date]) unless @jwt_payload[:resource_start_date].nil?
    rescue => e
      log.warn "Unable to parse start date: #{@jwt_payload[:resource_start_date]}"
    end

    end_date = nil
    begin
      end_date = Date.parse(@jwt_payload[:resource_end_date]) unless @jwt_payload[:resource_end_date].nil?
    rescue => e
      log.warn "Unable to parse end date: #{@jwt_payload[:resource_end_date]}"
    end
    #TODO remove client once savannah API call will update
    group = ResourceGroup.find_or_initialize_by(organization: @current_org, client_resource_id: @jwt_payload[:resource_id])
    if group.update_attributes(creator: User.cm_admin,
                            name: @jwt_payload[:resource_name] || @jwt_payload[:resource_id], #need this to pass validation
                            description: @jwt_payload[:resource_description] || @jwt_payload[:resource_id], #need this to pass validation
                            website_url: @jwt_payload[:resource_website_url],
                            is_private: !!@jwt_payload[:resource_is_private],
                            course_term: @jwt_payload[:resource_course_term],
                            course_code: @jwt_payload[:resource_course_code],
                            image_url: @jwt_payload[:resource_image_url],
                            start_date: start_date,
                            end_date: end_date,
                            language: @jwt_payload[:language],
                            organization:  @current_org,
                            client: @current_org.client
                           )

      if @jwt_payload[:promotion_data]
        Promotion.promote(group, @jwt_payload[:promotion_data].symbolize_keys)
      end

      render json: {'id' => group.id}, status: :ok and return
    else
      render json: {'errors' => group.errors}, status: :unprocessable_entity and return
    end
  end

  api :GET, "users/:id/org_details", "Get Org detail"
  param :id, String, :desc => 'UserId', :required => true
  formats ['json']
  def user_org_details
    @user = current_user
    @organization = Organization.find_by(id: @user.organization_id)
  end

  def index
    #TODO update it once savannah API updated
    client = Client.find(params[:client_id])
    unless client.admins.include?(current_user)
      head :forbidden and return
    end
    @groups = Group.where(organization: client.organization.id).where.not(type: 'PrivateGroup')
  end

  def update
    group = Group.find(params[:id])
    unless group.app_admins.include?(current_user)
      head :forbidden and return
    end

    group_params = params.require(:group).permit(:close_access, :website_url, :details_locked, :name)

    group.update_attributes(group_params)
    head :ok and return
  end

  def clear_posts
    group = Group.find(params[:id])
    if group.app_user != current_user
      head :forbidden and return
    end

    Post.where(postable: group).find_each do |post|
      post.destroy
    end
    head :ok and return
  end

  def data_dump
    group = Group.find(params.require(:id))
    unless group.app_admins.include?(current_user)
      head :forbidden and return
    end
    format = params[:format]
    head :unprocessable_entity and return unless (format == 'JSON' || format == 'XML' || format == 'CSV')
    DataDumpJob.perform_later(format, current_user.id, group.id)
    head :ok and return
  end

  api :GET, "api/groups/:id/course_data", "Get course data for a group"
  param :id, String, :desc => "Id of concerned group", :required => true
  formats ['json']
  error 422, "Unprocessable Entity"
  def course_data
    pull_course_data_from_s3
  end

  # TODO: This method
  # look up groups by client resource id. This is
  # DANGEROUS, we should always look up groups by
  # (client id, client resource id) pair
  def upload_s3_urls
    s3_urls_array = params.require(:urls)
    s3_urls_array.each do |pairing|
      group = Group.find_by_client_resource_id(pairing[:group_id])
      next unless group
      group.course_data_url = pairing[:json_url]
      group.save
    end
    render json: {}, status: :ok
  end

  def close
    group = Group.where(organization_id: @current_org.id, client_resource_id: @jwt_payload[:resource_id]).first
    if group.nil?
      head :bad_request and return
    end
    unless group.is_active_super_user?(current_user)
      head :unauthorized and return
    end
    group.close
    head :ok and return
  end

  def preferences
    @group = Group.find_by_id(params[:id])
  end

  def change_preference
    pars = params.permit(
      :id,
      preferences: [
        :connect_to_social_mediums,
        :daily_digest_enabled,
        :enable_mobile_app_download_buttons,
        :share_to_social_mediums,
        :forum_notifications_enabled ])

    group = Group.find_by_id(pars[:id])
    group.assign_attributes(pars[:preferences])
    group.save
    head :ok
  end

  def authenticate_admin!
    if request.format == 'application/json'
      if(request.headers['X-API-TOKEN'].present? || request.headers['X-API-AUTH-TOKEN'].present?)
        authenticate_from_savannah!
      elsif params[:token] == ENV['SAVANNAH_TOKEN']
        @jwt_payload = params
        @current_org = PartnerAuthentication.fetch_org_from_cache(params)
        admin_user = User.cm_admin
        current_user = admin_user
      else
        head :unauthorized and return
      end
    else
      respond_to do |f|
        f.html { render 'errors/unauthorized', status: 401 and return }
        f.js   { render json: :no_content, status: :unauthorized and return }
      end
    end
  end

  def find_group
    @group = Group.find_by_id(params[:id])
    head :bad_request and return if @group.nil?
  end

  private
  def regenerate_brightcove_url bc_videos
    bc_videos.each {|bc_v| bc_v['video_id'] = "#{brightcove_videos_url(host: @group.organization.host)}?video_url=#{bc_v['video_id']}" }
  end
end

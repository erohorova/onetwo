class NotificationsController < ApiController

  after_action :set_timestamp, only: [:index]

  api :GET, "groups/:group_id/notifications", "Get notifications for a user for a group"
  api :GET, "api/notifications", "Get all notifications for a user for mobile"
  param :group_id, Integer, :desc => "The concerned group, required if called from widget", :required => false
  param :limit, Integer, :desc => "page limit", :required => false
  param :offset, Integer, :desc => "page offset", :required => false
  param :last_fetch_time, String, :desc => "get only new notifications after this time", :required => false
  formats ['json']
  def index
    @user = current_user

    @native_app = is_native_app?

    limit = params.has_key?(:limit) ? params[:limit].to_i : 20
    offset = params.has_key?(:offset) ? params[:offset].to_i : 0

    filters = {}
    if @widget_request
      query = "widget_deep_link_id IS NOT NULL AND group_id = (:group_id)"
      filters[:group_id] = params[:group_id]
    elsif @native_app
      query = "app_deep_link_id IS NOT NULL AND notification_type NOT IN ('new_approval') "
    else
      query = "widget_deep_link_id IS NULL AND app_deep_link_id IS NOT NULL"
    end

    nquery = query
    # last fetch time only applies to the notifications list
    if params[:last_fetch_time]
      ct = DateTime.parse(params[:last_fetch_time])
      nquery += " AND created_at >= (:created_at)"
      filters[:created_at] = ct
    end

    seen_upto = UserNotificationRead.where(user: @user).first

    if seen_upto
      @unseen_count = @user.unseen_notifications.where(query, filters).where("created_at >= (:seen_upto)", seen_upto: seen_upto.seen_at).count
    else
      @unseen_count = @user.unseen_notifications.where(query, filters).count
    end
    @notifications = @user.notifications.includes(:notifications_causal_users, :causal_users).where(nquery, filters).order(id: :desc).limit(limit).offset(offset).select{|n| n.notifications_causal_users.size > 0}
    if @native_app
      card_ids = @notifications.select{|n| n.app_deep_link_type == 'card'}.map{|n| n.app_deep_link_id}.uniq
      @cards_array = Search::CardSearch.new.search_by_id(card_ids) || []
      @cards_map = Hash[@cards_array.map{|c| [c['id'].to_i, c]}]

      if native_app_platform == 'android'
        # android 73 and above supports polls and collections and everything
        if !client_supports_required_build?(-1, 72)
          if !client_supports_required_build?(-1, 70)
            # Send media cards only
            @notifications = @notifications.to_a.reject{|n| n.app_deep_link_type == 'collection' || (n.app_deep_link_type == 'card' && @cards_map[n.app_deep_link_id].try(:card_type) != 'media')}
          else
            # Collections are okay, but not other cards
            @notifications = @notifications.to_a.reject{|n| n.app_deep_link_type == 'card' && @cards_map[n.app_deep_link_id].try(:card_type) != 'media'}
          end
        end
      end

      group_ids = @notifications.map{|n| n.group_id}.uniq
      @user_actions = CardsUser.get_user_actions(
        card_ids: card_ids,
        user_id: @user.id)

      post_ids = @notifications.select{|n| n.app_deep_link_type == 'post'}.map{|n| n.app_deep_link_id}.uniq
      posts_array = post_ids.empty? ? [] : Post.retrieve_by_ids(post_ids)
      @posts_map = Hash[posts_array.map{|p| [p.id.to_i, p]}]
      @votes_by_user = Vote.get_votes_by_user(@user,
                                              votable_type: 'Post',
                                              votable_ids: post_ids)

      video_stream_ids = @notifications.select{|n| n.app_deep_link_type == 'video_stream'}.map{|n| n.app_deep_link_id}.uniq
      video_streams_array = VideoStream.includes(:creator, card: [:file_resources, :tags]).where(id: video_stream_ids)
      @video_streams_map = Hash[video_streams_array.map{|v| [v.id.to_i, v]}]
    end
    @preloaded_snippets = Notification.preload_app_deep_link_snippets @notifications
  end

  api :POST, "notifications/:id/read", "Read a particular notification"
  formats ['json']
  def read
    @user = current_user
    @notification = Notification.find(params[:id])
    unless @notification.user_id == @user.id
      head :unauthorized and return
    end
    @notification.mark_as_seen
    head :ok and return
  end

  api :POST, "notifications/see_upto", "See all notifications upto now"
  param :seen_at, DateTime, :desc => 'The timestamp of last fetch before the user clicked on the notification bubbble', :required => true
  formats ['json']
  def see_upto
    @user = current_user
    uns = UserNotificationRead.find_or_initialize_by(user: @user)
    uns.seen_at = params[:seen_at]
    if uns.save
      head :ok and return
    else
      head :bad_request and return
    end
  end

  private
  def set_timestamp
    response.headers['X-Last-Fetch-Timestamp'] = Time.now.utc.strftime('%Y-%m-%dT%H:%M:%S.%LZ')
  end

end

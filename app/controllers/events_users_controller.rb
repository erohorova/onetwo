class EventsUsersController < ApiController

  before_action :authorize_user_for_group

  api :POST, '/events/:events_id/events_users', 'rsvp user to an event'
  param :rsvp, ['yes', 'no', 'maybe'], "rsvp status"
  formats ['json']
  def create
    events_user = EventsUser.new(events_user_params)
    events_user.user_id = current_user.id
    events_user.event_id = params[:event_id]

    message, status = events_user.save ? 
      [events_user.event.rsvp_yes_count, :ok] :
        [events_user.event.rsvp_yes_count, :unprocessable_entity]

    render json: message, status: status
  end

  api :PATCH, '/events/:event_id/events_users/:id', 'rsvp user to an event'
  param :rsvp, ['yes', 'no', 'maybe'], "rsvp status for the event"
  formats ['json']
  def update
    events_user = EventsUser.find(params[:id])

    if events_user.user != current_user
      head :unauthorized and return
    end

    events_user.rsvp =  params[:events_user][:rsvp]

    message, status = events_user.save ? 
      [events_user.event.rsvp_yes_count, :ok] :
        [events_user.event.rsvp_yes_count, :unprocessable_entity]

    render json: message, status: status
  end

  private
    def authorize_user_for_group
      event = Event.find(params[:event_id])

      unless event.group.is_active_user?(current_user)
        head :unauthorized and return
      end
    end

    def events_user_params
      params.require(:events_user).permit(:rsvp)
    end
end

class UserPreferencesController < ApiController

  skip_before_filter :cm_auth_user, only: [:set_from_token]


  def index
    preferenceable_type = params.require(:preferenceable_type)
    preferenceable_id = params.require(:preferenceable_id)
    @user_preferences = UserPreference.where(user: current_user,
                                    preferenceable_type: preferenceable_type,
                                    preferenceable_id: preferenceable_id)
  end

  def update
    preferenceable_type = params.require(:preferenceable_type)
    preferenceable_id = params.require(:preferenceable_id)
    key = params.require(:key)
    @user_preference = UserPreference.find_or_initialize_by(user: current_user,
                                    key: key,
                                    preferenceable_type: preferenceable_type,
                                    preferenceable_id: preferenceable_id)
    @user_preference.value = params.require(:value)
    if @user_preference.save
      head :ok
    else
      head :unprocessable_entity
    end
  end

  def set_from_token
    if params[:token]
      preference_token = params[:token]
      begin
        preference_params = ActiveSupport::JSON.decode(ActiveSupport::MessageEncryptor.
                                                           new(Edcast::Application.config.secret_key_base).
                                                           decrypt_and_verify(CGI::unescape(preference_token).gsub('_', '=')))
      rescue => e
        log.info("Problem with decoding the preference token, error=#{e.message}")
        head :not_found
        return
      end


      user_preference = UserPreference.find_or_initialize_by(user_id: preference_params['user_id'], key: preference_params['key'],
                                                             preferenceable_type: preference_params['preferenceable_type'],
                                                             preferenceable_id: preference_params['preferenceable_id'])

      if user_preference.value == preference_params['value']
        @status = 'already_set'
      else
        if user_preference.update(value: preference_params['value'])
          @status = 'success'
        else
          @status = 'error'
        end
      end

      render layout: 'errors', template: 'user_preferences/destiny_unsubscribe'
    else
      log.info('Calling user_preferences#set_from_token without the token parameter')
      head :not_found
    end
  end
end

class WelcomeController < ApplicationController

  skip_filter :authenticate_user!
  layout (Settings.features.layout_v4 ? 'application_v4' : 'application_v2')

  def action_missing(m, *args, &block)
    unknown_route
  end

  def index
    if current_org.id == Organization.default_org&.id
      response_body_from_corp = Rails.cache.fetch('home-page-from-corp', expires_in: 1.minutes) do
        conn = Faraday.new(:url => Settings.edcast_wordpress_url )
        response = conn.get '/corp/'
        response.body
      end
      render inline: response_body_from_corp, layout: false and return
    else
      redirect_to new_user_session_path({from_root: true}), status: :moved_permanently and return
    end
  end

  def about

  end

  def jobs

  end

  def contact

  end

  def tos

  end

  def privacy

  end

  def ferpa

  end
  def partners

  end
  def eula

  end

end

class VotesController < ApiController

  before_action :set_votable, :only => [:create, :destroy]

  # vote for a votable
  api :POST, "posts/:post_id/votes", "Vote for a Post"
  api :POST, "comments/:comment_id/votes", "Vote for a Comment"
  api :POST, "video_streams/:video_stream_id/votes", "Vote for a Video Stream"
  formats ['json']
  error 422, "Unprocessable"
  description "Sends 201 Created on Success"
  def create
    @vote = Vote.new
    @vote.votable = @votable
    @vote.voter = current_user
    @vote.weight = 1

    if @vote.save
      track_vote

      head :created
    else
      render json: @vote.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @vote = Vote.where(votable: @votable, voter: current_user).first
    if @vote
      @vote.destroy
      track_vote(destroyed: true)
      head :ok
    else
      head :bad_request
    end
  end

  private
  def set_votable
    # There has to be a better way
    
    if params.key? 'post_id'
      @votable = Post.find(params['post_id'])
    elsif params.key? 'comment_id'
      @votable = Comment.find(params['comment_id'])
    elsif params.key? 'video_stream_id'
      @votable = VideoStream.find(params['video_stream_id']).try(:card)
    end
  end

  def track_vote(destroyed: false)
    @votable ||= @vote.votable
    track_opts = {
        user_id: current_user.id,
        object: @votable.class.name.underscore,
        object_id: @votable.id,
        action: "#{'un' if destroyed}like",
        platform: get_platform,
        device_id: cookies[:_d],
        organization_id: current_user.organization_id,
        referer: params[:referer].presence || request.referer
    }
    Tracking::track_act track_opts
  end
end

class CommentsController < ApiController

  after_action :set_timestamp, only: [:index]
  before_action :set_commentable

  api :GET, "posts/:post_id/comments", "Get comments for a post"
  api :GET, "comments/:comment_id/comments", "Get replies for a comment"
  param :post_id, String, :desc => "Get comments for this post", :required => false
  param :comment_id, String, :desc => "Get replies for this comment", :required => false
  param :sort, String, :desc => 'Sort criterion. One of: \'date\', \'votes\'. Default is date.'
  param :order, String, :desc => 'Sort order. One of: \'desc\', \'asc\'. Default is desc.'
  param :limit, Integer, desc: 'How many items to return back'
  param :offset, Integer, desc: 'Where to start within the result set'
  param :last_fetch_time, String, desc: 'Fetch only comments returned after this time'
  param :first_item_id, Integer, desc: 'Id of the first comment in client list, for reverse pagination'
  formats ['json']
  def index
    pars = params.permit(:sort, :order, :limit, :offset, :last_fetch_time, :first_item_id)
    @comments = []

    if @commentable.kind_of?(Post) && @group && !@group.is_active_user?(current_user)
      head :unauthorized and return
    end

    unless pars['order'].nil? || ['desc','asc'].include?(pars['order'])
      head :unprocessable_entity and return
    end

    case pars['sort']
      when 'votes'
        order = pars['order'].blank? ? 'desc' : pars['order']
        sort_params = {votes_count: order.to_sym, created_at: :desc}
      when nil, 'date'
        order = pars['order'].blank? ? 'asc' : pars['order']
        sort_params = {created_at: order.to_sym}
      else
        head :unprocessable_entity and return
    end

    if pars[:last_fetch_time]
      @comments = @commentable.comments.includes(:user,:resource,:tags).where("created_at > (:created_at)", created_at: pars[:last_fetch_time]).order(sort_params).limit(pars['limit'] || 20).offset(pars['offset'] || 0)
    elsif pars[:first_item_id]
      # get n number of items before first_seen_id, in the order in which the comments were made
      @comments = @commentable.comments.includes(:user, :resource, :tags).where("id < (:last_id)", last_id: pars[:first_item_id].to_i).order(id: :desc).limit(pars['limit'] || 20).sort{|x,y| x.id <=> y.id}
    else
      @comments = @commentable.comments.includes(:user,:resource,:tags).order(sort_params).limit(pars['limit'] || 20).offset(pars['offset'] || 0)
    end
    @reports_by_user = @votes_by_user = @approved_comments = []
    @staff_users_cache = @score_users_cache = {}
    if current_user && @comments.present?
      comment_ids = @comments.map(&:id)
      user_ids = @comments.map(&:user_id)
      if @commentable.try(:group)
        @staff_users_cache = @commentable.group.groups_users.select(:user_id).where(banned: false, user_id: user_ids, as_type: 'admin').group_by(&:user_id)
        @score_users_cache = Score.where(group_id: @commentable.group_id, user_id: user_ids).group_by(&:user_id)
      end
      @comments.each do |comment|
        comment.created_by_staff = !@staff_users_cache[comment.user_id].blank?
        score = !@score_users_cache[comment.user_id].blank? && @score_users_cache[comment.user_id].first
        comment.user.score_entry = score ? {score: score.score, stars: score.stars} : {score: 0, stars: 0}
      end

      @reports_by_user = Report.get_reports_by_user(current_user,
                                                    reportable_type: 'Comment',
                                                    reportable_ids: comment_ids)
      @votes_by_user = Vote.get_votes_by_user(current_user,
                                              votable_type: 'Comment',
                                              votable_ids: comment_ids)

      @approved_comments = Approval.get_approvals(approvable_ids: comment_ids,
                                                   approvable_type: 'Comment')
    end
    respond_to do |f|
      f.js
      f.html
      f.json
    end
  end

  api :GET, 'posts/:post_id/comments/:id', "Get a particular comment on a post"
  api :GET, 'comments/:comment_id/comments/:id', "Get a particular reply to a comment"
  param :post_id, :number, :desc => "The post on which the comment was made", :required => false
  param :comment_id, :number, :desc => "The comment on which the reply was made", :required => false
  param :id, :number, :desc => "The particular comment or reply", :required => true
  formats ['json']
  def show
    @comment = Comment.includes(:user,:resource,:tags).where("commentable_id = ? AND id = ?", @commentable.id, params[:id]).first
    if @comment.nil?
      raise ActiveRecord::RecordNotFound
    end
  end

  api :POST, "posts/:post_id/comments", "Post comment for a post"
  api :POST, "comments/:comment_id/comments", "Post reply to a comment"
  param :post_id, String, :desc => "The concerned post, either this or comment_id is required", :required => false
  param :comment_id, String, :desc => "The concerned comment, either this or post_id is required", :required => false
  param :message, String, :desc => "The comment text", :required => true
  param :type, String, :desc => "The comment type, such as Answer", :required => false
  param :file_ids, Array, :desc => "File ids to be attached", :required => false
  formats ['json']
  def create
    pars = params[:comment] || params

    # pushing file_ids array in pars
    if (params.has_key?(:file_ids))
        pars[:file_ids] = params[:file_ids]
    end

    if !pars.key?('message')
      head :bad_request and return
    end

    @comment = Comment.new({:message => pars[:message]})
    @comment.user = current_user
    @comment.commentable = @commentable
    @comment.anonymous = pars[:anonymous]

    if ['Announcement', 'Question'].include?(@commentable.class.name)
      @comment.type = 'Answer'
    end

    @comment.file_ids = pars[:file_ids] if (pars.has_key?(:file_ids) && pars[:file_ids].is_a?(Array))
    if @comment.save
      track_comment

      respond_to do |f|
        f.json { render 'show', status: :created }
        f.html
        f.js
      end

    else
      respond_to do |f|
        f.json { render json: @comment.errors, status: :unprocessable_entity }
        f.html
        f.js
      end
    end
  end

  api :PATCH, "posts/:post_id/comments/:id", "Update a comment on a post"
  api :PATCH, "comments/:comment_id/comments/:id", "Update a reply on a comment"
  api :PATCH, "/api/cards/:card_id/comments/:id", "Update a comment on a card"
  param :post_id, :number, :desc => "The post on which the comment was made, either this or comment_id is required", :required => false
  param :comment_id, :number, :desc => "The comment on which the reply was made, either this or post_id is required", :required => false
  param :card_id, :number, :desc => "The card on which the comment was made, either this or comment_id is required", :required =>false
  param :id, :number, :desc => "The comment to edit", :required => true
  param :message, String, :desc => "The new message", :required => true
  param :file_ids, Array, :desc => "File ids to be attached", :required => false
  formats ['json']
  def update
    if !params.key?(:message)
      head :bad_request and return
    end

    @comment = Comment.find(params[:id])

    unless current_user.is_equivalent_user(@comment.user)
      head :forbidden and return
    end

    @comment.update(:message => params[:message])

    params[:file_ids] ||= [] if params.has_key?(:file_ids)
    @comment.file_ids = params[:file_ids]

    if @comment.save
      @comment.update_resource
      @comment.update_tags

      render 'show', status: :ok
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end

  api :DELETE, "posts/:post_id/comments/:id", "Delete a comment on a post"
  api :DELETE, "comments/:id", "Delete a comment"
  api :DELETE, "comments/:comment_id/comments/:id", "Delete a reply to a comment"
  param :post_id, :number, :desc => "The post on which the comment was made", :required => false
  param :comment_id, :number, :desc => "The comment on which the reply was made", :required => false
  param :id, :number, :desc => "The comment to delete", :required => true
  formats ['json']
  def destroy
    @comment = Comment.find(params[:id])

    unless ((@group && @group.is_active_super_user?(current_user)) || current_user.is_equivalent_user(@comment.user))
      head :forbidden and return
    end

    @comment.destroy

    track_comment(destroyed: true)

    head :no_content and return
  end

  api :POST, "posts/:post_id/comments/:id/restore", "Restore a comment on a post"
  api :POST, "comments/:comment_id/comments/:id/restore", "Restore a reply on a comment"
  api :POST, "comments/:id/restore", "Restore a comment"
  param :post_id, :number, :desc => "The post on which the comment was made", :required => false
  param :comment_id, :number, :desc => "The comment on which the reply was made", :required => false
  param :id, :number, :desc => "The comment to restore", :required => true
  formats ['json']
  def restore
    @comment = Comment.find(params[:id])
    unless @group.is_active_super_user?(current_user)
      head :forbidden and return
    end

    @comment.reports.each do |report|
      if report.reporter == current_user
        report.destroy
      else
        report.validity = false
        report.save
      end
    end
    @comment.hidden = false
    @comment.save
    head :ok and return
  end

  private

  def visible_without_auth?
    # For viewing comments/replies, see if publicly visible, everything else requires auth
    if ['index', 'show'].include?(action_name)
      set_commentable
      return !!@commentable.try(:visible_without_auth?)
    end
    false
  end

  def set_commentable
    return unless @commentable.nil? # return if already set
    if params[:post_id].present?
      @commentable = Post.find(params[:post_id])
    elsif params[:event_id].present?
      @commentable = Event.find(params[:event_id])
    elsif params[:card_id].present?
      @commentable = Card.friendly.find(params[:card_id])
    elsif params[:video_stream_id].present?
      @commentable = VideoStream.find(params[:video_stream_id]).try(:card)
    elsif params[:comment_id].present?
      @commentable = Comment.find(params[:comment_id])
    end
    @group = @commentable.try(:group)
  end

  def set_timestamp
    response.headers['X-Last-Fetch-Timestamp'] = Time.now.utc.strftime('%Y-%m-%dT%H:%M:%S.%LZ')
  end

  def set_files_ids
    @comment.file_ids = params[:file_ids] if (params.has_key?(:file_ids) && params[:file_ids].is_a?(Array))
  end

  def track_comment(destroyed: false)
    @commentable ||= @comment.commentable
    track_opts = {
        user_id: current_user.id,
        object: @commentable.class.name.underscore,
        object_id: @commentable.id,
        action: "#{'un' if destroyed}comment",
        platform: get_platform,
        device_id: cookies[:_d],
        organization_id: current_user.organization_id,
        referer: params[:referer].presence || request.referer
    }
    case @commentable
      when Card
        track_opts.merge! card: show_insight_url(@commentable)
    end
    Tracking::track_act track_opts
  end

end

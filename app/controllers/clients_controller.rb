class ClientsController < ApplicationController
  include SavannahAuthentication
  skip_before_filter :authenticate_user!
  before_filter :authenticate_admin!
  protect_from_forgery with: :null_session, only: :manage

  def index
    @clients = current_user.administered_clients
    @clients = @clients.where('name like :term', term: "%#{params[:search]}%") unless params[:search].blank?
  end

  def manage
    render :layout => 'developers'
  end

  def new
    @client = Client.new
  end

  #Creation will accept savannah_app_id and redirect_uri and save in Organization table
  #Removing dependecies of client
  def create
    new_client_params = client_params
    existing_client_with_name = Client.where(user: current_user,
                                             name: new_client_params[:name]).first

    # don't create two clients with the same name for
    # savannah requests
    if existing_client_with_name && @savannah_request
      @client = existing_client_with_name
      @client.redirect_uri = client_params[:redirect_uri] if client_params[:redirect_uri].present?
      @client.save!
      @credential = Credential.new
      @client.credentials << @credential
    else
      @client = Client.new(new_client_params)
      @client.user = current_user
    end
    if @client.save
      unless existing_client_with_name && @savannah_request
        @credential = @client.valid_credential unless existing_client_with_name
      end
      org_host_name = params[:organization].try(:[], :host_name)

      unless org_host_name.nil?
        org = Organization.find_by_host_name(org_host_name)
        if org
          pars = {client_id: @client.id}
          if(params[:organization])
            pars[:savannah_app_id] = params[:organization][:savannah_app_id]
            pars[:redirect_uri]    = params[:organization][:redirect_uri]
          end
          org.update_attributes(pars)
        else
          org = Organization.new org_params
          org.client_id = @client.id
          if org.save
            new_user = current_user.clone_for_org(org, 'admin')
          else
            # this error would require someone to go in and create an org manually
            log.error "Failed to create org user: #{current_user.id} org: #{params[:organization]} client: #{@client.id} errors: #{org.full_errors}"
          end
        end
      end
      render 'show'
    else
      respond_to do |format|
        format.html {render 'new', status: :bad_request}
        format.json {render json: {:error => @client.errors.full_messages},
        status: :bad_request}
      end
    end
  end

  def update
    @client = Client.find(params[:id])
    if @client.update(client_update_params)
      @client.organization.update(social_enabled: params[:client][:social_enabled]) if params[:client][:social_enabled]
      head :ok
    else
      head :unprocessable_entity
    end
  end

  def destroy
    @client = Client.find(params[:id])
    @client.destroy
    head :ok and return
  end

  def show
    @client = Client.find(params[:id])
    if @client.owner != current_user
      head :forbidden and return
    end
    @credential = @client.valid_credential
  end

  private
  def client_update_params
    params.require(:client).permit(:social_enabled)
  end

  def client_params
    params.require(:client).permit(:name, :redirect_uri)
  end

  def org_params
    params.require(:organization).permit(:name, :description, :image, :mobile_image, :host_name, :savannah_app_id, :redirect_uri)
  end

  def authenticate_admin!
    if request.format == 'application/json'
      if params[:token].present? && params[:token] == Settings.savannah_token
        @savannah_request = true
        admin_user = User.cm_admin
        @current_user = admin_user
      else
        @savannah_request = false
        authenticate_user!
      end
    else
      authenticate_user!
    end
  end
end
class ReferralsController < ApplicationController
  include Devise::Controllers::Rememberable
  include TrackActions

  skip_before_filter :authenticate_user!
  skip_before_filter :set_organization_context
  before_filter :set_current_org_without_redirects
  before_filter :authorize_join!, only: [:join]

  def join
    #render an html page to resubmit via POST method. prevent crawlers from hitting this URL
    if (request.get? && request.format.symbol == :html) && !params[:internal_redirect].try(:to_bool)
      render layout: false
      return
    end

    token = params[:token]

    @invitation = Invitation.find_by_token(token)

    if @invitation.nil? || @invitation.accepted_at.present?
      respond_to do |format|
        format.html {redirect_to root_path}
        format.json {head :unprocessable_entity}
      end
      return
    end

    # If user is signed in, assign @user to current_user since we prompt enterprise user to sign in.
    # If user is not signed in (which happens if `authenticable_recipient` = FALSE), assign @user from recipient.
    if current_user
      @user = current_user
    else
      @user = User.where(email: @invitation.recipient_email, organization: current_org).first
    end

    invitable = @invitation.invitable
    redirect_path = root_path

    case invitable
      when Organization
        @org = invitable
        if @user.nil?
          invite_role = @invitation.roles.present? && @invitation.roles.include?('admin') ? 'admin' : 'member'

          # If user doesnt exist in default org and then create new one in current org
          if @user.nil?
            @user = User.new(email: @invitation.recipient_email,
                             first_name: @invitation.first_name,
                             last_name: @invitation.last_name,
                             is_complete: true,
                             organization: current_org,
                             organization_role: invite_role,
                             password_reset_required: true, #force new account user to set their password
                             password: User.random_password)
            @user.skip_confirmation!
          end
          @user.save!
        else
          @user.confirm unless @user.confirmed?
        end

        # skip/show onboarding for user on admin invite join
        if @org.onboarding_hidden? && !@user.onboarding_completed?
          @user.skip_onboarding
        end
      when Team
        redirect_path = team_path(invitable)
        @org = invitable.organization
        clone_or_create_user("member") if @user.nil?

        if @user
          if @invitation.roles.include?('admin')
            invitable.add_admin @user
          else
            invitable.add_member @user
          end

          track_action(invitable, 'join', @user)
          @user.confirm unless @user.confirmed?
        end
    end

    if @user
      accepted_at = @invitation.accepted_at.nil? ? Time.now : @invitation.accepted_at
      @invitation.update(recipient_id: @user.id, accepted_at: accepted_at)

      bypass_sign_in @invitation.recipient
      if !@invitation.recipient.organization.expire_session_on_closing_browser
        remember_me @invitation.recipient
      end  
      cookies[:collect_user_data] = '1'

      # set first_sign_in cookie (for invited user login)
      # sign_in_count == 0 because sign_in(bypass: true)
      response.set_cookie("first_sign_in", {value: true, path: "/"}) if(@user.sign_in_count == 0)
    end

    respond_to do |format|
      format.html {redirect_to redirect_path}
      format.json {head :ok}
    end
  end

  private

    # CASE 1: Prompt enterprise user to sign in.
    # CASE 2: Non-enterprise user must accept invitation and logs him/her
    # Once user is logged in, invitation is linked to either current_user or recipient
    def authorize_join!
      @invitation = Invitation.find_by(token: params[:token])

      if @invitation && @invitation.authenticate_recipient?
        # Make sure that logged in user`s email matches with invitation`s recipient_email.
        #   Signs out user if they do not match. Assuming that logged in user will be the intended user will be risky.
        #   Verifying `:invitation_id` is mandatory otherwise user will be logged out even he is authentic.
        if session[:invitation_id].nil? && (current_user && current_user.email != @invitation.recipient_email)
          sign_out current_user
        end

        # Once authentic SSO user logs in, let them in and accept the invitation and link it with current_user
        if session[:invitation_id].present? && current_user
          session.delete(:invitation_id)
        else
          # When user is yet to log in, set the `:invitation_id` to this join URL and ask them to log in.
          session[:invitation_id] = @invitation.id
          redirect_to root_path and return
        end
      else
        session.delete(:invitation_id)
      end
    end

    def clone_or_create_user(invite_role)
      if @org.present? && @org != Organization.default_org
        default_org_user = User.where(email: @invitation.recipient_email, organization: Organization.default_org).first
        if default_org_user
          @user = default_org_user.clone_for_org(@org, invite_role)
        end
      end

      # If used doesnt exist in default org and then create new one in current org
      if @user.nil?
        @user = User.new(email: @invitation.recipient_email,
                         first_name: @invitation.first_name,
                         last_name: @invitation.last_name,
                         is_complete: false,
                         organization: current_org,
                         organization_role: invite_role,
                         password_reset_required: true, #force new account user to set their password
                         password: User.random_password)
        @user.skip_confirmation!
      end
      @user.save!
    end
end

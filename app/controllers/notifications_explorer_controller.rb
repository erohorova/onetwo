class NotificationsExplorerController < ApplicationController
  skip_before_filter :authenticate_user!

  layout false

  def note_entry
    entry = NotificationEntry.find(params[:entry_id])
    @content = EDCAST_NOTIFY.get_single_notification_content(entry)
  end
end

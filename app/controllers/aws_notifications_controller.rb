class AwsNotificationsController < ActionController::Base
  include NativeAppDetection
  include DetectPlatform

  # Prevent CSRF attacks
  protect_from_forgery with: :null_session

  def create
    sns_message = AWS::SNS::Message.new(request.raw_post)
    if sns_message.authentic?
      case sns_message.type
        when :SubscriptionConfirmation
          log.error "AWS SNS SubscribeURL: #{sns_message.subscribe_url}" #open this URL in logentries to comfirm the subscription in production environment
        when :Notification
          message_payload = ActiveSupport::JSON.decode(sns_message.message)
          recording = Recording.find(message_payload['userMetadata']['recording_id'])
          recording.last_aws_transcoding_message = sns_message.message
          case message_payload['state']
            when 'COMPLETED'
              TranscoderCompleteJob.perform_later(recording, sns_message.message)
            when 'ERROR'
              recording.transcoding_status = 'error'
              recording.save!
            when 'PROGRESSING'
              recording.transcoding_status = 'transcoding'
              recording.save!
          end

      end
      log.info "AWS Transcoder message: #{sns_message.message}"
      render nothing: true, status: :ok
    else
      render nothing: true, status: :unauthorized
    end

  end
end
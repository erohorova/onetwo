
class LtiController < ActionController::Base
  include RequestHandler
  include Entry
  include NativeAppDetection
  include DetectPlatform

  require 'ims/lti'
  require 'oauth/request_proxy/rack_request'

  before_action :authorize!, only: [:events, :forum_v1]
  # Prevent CSRF attacks
  protect_from_forgery with: :null_session
  after_action :allow_iframe, only: [:events, :forum_v1]

  def events
    prepare_data(ui_params)
    if not performed?
      @app_name = 'events'
      render 'events', layout: 'ui' and return
    end
  end

  def forum_v1
    prepare_data(ui_params)
    if not performed?
      if @ref_id
        @ref_post = Post.find_by(id: @ref_id.to_i)
      end
      @app_name = 'ui'
      render 'forum_v1', layout: 'ui' and return
    end
  end

  private
  def authorize!
    allow_iframe
    if !params.key?('oauth_consumer_key')
      @error_message = 'Oauth consumer key absent'
      log.error @error_message
      render 'error' and return
    end

    @credential, client, organization = CacheFactory.fetch_credential_and_client(api_key: params['oauth_consumer_key'])
    if client.nil? || @credential.nil?
      @error_message = "Client look up failed, probably bad api key (#{params['oauth_consumer_key']})"
      log.error @error_message
      render 'error' and return
    end

    @tool_provider = IMS::LTI::ToolProvider.new(@credential.api_key,
                                     @credential.shared_secret,
                                     params)

    unless @tool_provider.valid_request?(request)
      @error_message = 'oauth signature not valid'
      log.error @error_message
      render 'error' and return
    end

    # check the timestamp but continue if it doesn't match. Log a warning for monitoring.
    if Time.now.utc.to_i - @tool_provider.request_oauth_timestamp.to_i > 60*60
      @error_message = "timestamp too old [now: #{Time.now.utc.to_s}, ts: #{Time.at(@tool_provider.request_oauth_timestamp.to_i).utc.to_s}, api_key: #{params[:oauth_consumer_key]}]"
      log.warn @error_message
      # render 'error' and return
    end
  end


  def allow_iframe
    response.headers.except! 'X-Frame-Options'
  end

  def ui_params
    resource_id = params['resource_link_id']
    resource_name = params['resource_link_title']
    resource_description = params['resource_link_description']
    timestamp = params['oauth_timestamp']

    launch_return_url = params['launch_presentation_return_url']
    launch_return_url = request.referrer if launch_return_url.blank?

    # lti specs let the tool consumer pass in a
    # comma separated list of roles. We will be
    # using the first one only. The specs don't say
    # anything about how to handle conflicting roles
    launch_roles = params['roles']

    first_role = nil

    begin
      cs_list_roles = launch_roles.split(',')
      first_role = cs_list_roles[0].downcase
    rescue => e
      @error_message = 'Bad roles. Please check integration'
      render 'error' and return
    end

=begin
    if @tool_provider.student?
      role = 'student'
    elsif @tool_provider.instructor? || @tool_provider.ta?
      role = 'instructor'
    else
      role = 'student'
    end
=end

    user_id = params['user_id']
    email = params['lis_person_contact_email_primary']
    user_image = params['user_image']
    user_name = params['lis_person_name_full']
    user_first_name = params['lis_person_name_given']
    user_last_name = params['lis_person_name_family']

    # we make email mandatory for our LTI plugin
    if resource_id.nil?
      @error_message = 'resource_link_id is mandatory parameter'
      render 'error' and return
    end


    parts = [@credential.shared_secret,
             timestamp,
             user_id,
             email,
             first_role,
             resource_id].compact

    token = Digest::SHA256.hexdigest(parts.join('|'))
    return {:token => token,
            :user_role => first_role,
            :user_id => user_id,
            :user_email => email,
            :user_image => user_image,
            :user_name => user_name,
            :user_first_name => user_first_name,
            :user_last_name => user_last_name,
            :api_key => @credential.api_key,
            :timestamp => timestamp,
            :resource_id => resource_id,
            :resource_name => resource_name,
            :resource_description => resource_description,
            :parent_url => launch_return_url,
            :event_request => true
    }
  end

end

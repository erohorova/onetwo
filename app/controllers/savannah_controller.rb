class SavannahController < ActionController::Base
  include SavannahAuthentication
  before_action :authenticate_from_savannah!
  # Prevent CSRF attacks
  protect_from_forgery with: :null_session

  def list_ssos
    @org_ssos = @current_org.org_ssos.active.visible.order(:position)
    available_ssos = Sso.all
    @unenabled_ssos = available_ssos.select do |sso|
      !@org_ssos.any? {|org_sso| org_sso.sso_id == sso.id}
    end
    @results = @org_ssos + @unenabled_ssos
  end

  def enable_sso
    sso = Sso.find_by_name!(@jwt_payload[:name])
    @org_sso = OrgSso.find_or_initialize_by(sso_id: sso.id, organization_id: @current_org.id)
    @org_sso.position = @current_org.org_ssos.count if @org_sso.new_record?

    if @org_sso.update(is_enabled: true)
      render action: :show
    else
      render json: @org_sso.errors, status: :unprocessable_entity
    end
  end

  def disable_sso
    sso = Sso.find_by_name!(@jwt_payload[:name])
    @org_sso = OrgSso.where(sso_id: sso.id, organization_id: @current_org.id).first!

    if @org_sso.update(is_enabled: false)
      render action: :show
    else
      render json: @org_sso.errors, status: :unprocessable_entity
    end
  end

  def sso_login
    user = @current_org.users.where(:email=>@jwt_payload["email"]).first
    if user && current_user.blank?
      bypass_sign_in user
    end

    @redirect_uri =  @jwt_payload["redirect_url"]
    render :layout=>false
  end


end

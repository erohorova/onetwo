class MetricRecorderController < ApplicationController
  protect_from_forgery with: :null_session
  skip_before_filter :verify_authenticity_token, if: :is_native_app?
  skip_before_action :authenticate_user!
  skip_before_action :drop_device_cookie
  skip_before_action :set_organization_context

  def create
    user_id = user_signed_in? ?  current_user.id : nil
    Tracking::track(user_id, 'act', {
                                 device_id: cookies[:_d], object: params[:object_type], object_id: params[:object_id],
                                 type: params[:act], #cannnot use action because it's a rails reserved parameter
                                 platform: get_platform, properties: params[:properties],
                                 organization_id: current_user.try(:organization_id),
                                 referer: params[:referer].presence || request.referer
                                })
    render nothing: true
  end

end
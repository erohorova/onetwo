class BrightcoveVideosController < ApplicationController

  def show
    video_url = params[:video_url]
    config = @current_org.configs.where(category: 'brightcove', name: 'account_id').first
    redirect_url = if !config.blank? && video_url
      video_id = fetch_video_id_from_url video_url
      account_id = config.value

      "http://players.brightcove.net/#{account_id}/default_default/index.html?videoId=#{video_id}&autoplay=yes"
    else
      log.error "Missing brightcove account, redirecting to: #{params[:video_url]}"
      params[:video_url]
    end
    redirect_to redirect_url
  end


  private
  def fetch_video_id_from_url(url)
    conn = Faraday.new url: url
    dest_url = conn.get['location']
    parse_url_query(dest_url)['bctid'] if dest_url
  end

  def parse_url_query(url)
    Rack::Utils.parse_query URI(url).query
  end
end

require 'csv'
class InvitationsController < ApplicationController
  before_filter :set_invitable, only: [:index, :create, :resend, :destroy, :upload]
  before_filter(only: [:index, :create, :resend, :destroy, :upload]) { check_role!(:user) } #user is both member and admin

  def index
    @invitations = Invitation.where(invitable: @invitable)
    if params[:search_term].present?
      @invitations = @invitations.where('recipient_email like ?', "%#{params[:search_term]}%")
    end
    if params[:pending].present?
      @invitations.where!(accepted_at: nil)
    end
    if params[:roles].present?
      @invitations.where!(roles: params[:roles])
    end
    if params[:offset].present?
      @invitations.offset!(params[:offset].to_i)
    end
    if params[:limit].present?
      @invitations.limit!(params[:limit].to_i || 50)
    end

    @invitations.order!(id: :desc)
    @total_count = @invitations.only(:where).count
  end

  def create
    invitable = @invitable
    if invitable
      payload = {invitable: invitable, roles: params.require(:roles)}

      if params[:email].blank?
        log.info('Missing handle or email')
        render json: {'error': 'Something went wrong. Please try again'}, status: :unprocessable_entity and return
      end

      if params[:email].is_a?(Array)
        payload[:recipient_email] = params[:email]
        payload[:recipient_email].reject! {|email| email.match(Devise::email_regexp).nil? }
      elsif params[:email].is_a?(String) && !params[:email].match(Devise::email_regexp).nil?
        payload[:recipient_email] = [] << params[:email]
      else
        render json: {error: "Error occurred while inviting to #{params[:email]}"}
      end

      payload[:sender_id] = current_user.id
      process_multi_invites(payload) and return
    end
  end

  def destroy
    invitation = @invitation
    if invitation.destroy
      head :ok
    else
      head :unprocessable_entity
    end
  end

  def resend
    @invitation.resend_mail
    head :ok
  end

  def upload
    file_data = params[:file].read
    begin
      CSV.parse(file_data)
      invitation_file = InvitationFile.new(:sender_id => current_user.id,
                        :invitable => @invitable,
                        :data => file_data,
                        :roles => params[:role] || "member")
      if invitation_file.save
        status, message = true, "CSV is successfully uploaded"
      end
    rescue => e
      status, message = false, "CSV is not properly formatted. #{e}"
    end
    respond_to do |format|
      if status
        format.html { redirect_to :back, success: message }
        format.json { render json: {message: message, status: :created} }
      else
        format.html { redirect_to :back, success: message }
        format.json { render json: {message: message}, status: :unprocessable_entity }
      end
    end

  end


  private

  def set_invitable
    if params.has_key?(:id)
      @invitation = Invitation.find(params.require(:id))
      @invitable = @invitation.invitable
    else params.has_key?(:invitable_type) && params.has_key?(:invitable_id)
      if %w(Team Group Organization).exclude?(params[:invitable_type].classify)
        head :unprocessable_entity
        return
      end

      @invitable = params.require(:invitable_type).classify.constantize.find(params.require(:invitable_id))
    end
  end

  def find_invitable_user(email:)
    user = nil
    #This would respond to invitables where users association exist on the model
    # Invitable models with no direct association with users may need to handle it
    # separately
    if @invitable.respond_to?(:users)
      user = @invitable.users.where(email: email).first
    end
    user
  end

  def check_role!(role = :member)
    case @invitable
      when Organization
        unless @invitable.send("is_#{role}?".to_sym, current_user)
          head :forbidden
        end
    end
  end

  def process_multi_invites(payload)
    valid_emails   = []
    invalid_emails = []

    payload[:recipient_email].each do |email|
      next if email.blank?

      existing_invitation = Invitation.where(
        invitable: payload[:invitable],
        roles: payload[:roles],
        recipient_email: email
      ).first

      existing_user = find_invitable_user(email: email)

      if existing_invitation || existing_user
        invalid_emails << email
      else
        valid_emails << email
      end
    end

    if valid_emails.present?
      MultiInvitationsJob.perform_later(
        emails: valid_emails,
        sender_id: current_user.id,
        invitable_type: payload[:invitable].class.name,
        invitable_id: payload[:invitable].id,
        roles: payload[:roles]
      )
    end

    if invalid_emails.present?
      render(
        json: {"error": "Invites or users already exists for these emails: #{invalid_emails.join(', ')}" },
        status: :ok
      )
    else
      head :ok
    end
  end
end

class InsightsController < ApplicationController

# -----------------------------------------------------------------------------
# NOTE
#
# This file is deprecated, but is still used by edc-web so it cannot be removed.
# The tests have already been removed.
# See the following commits:
# - 879fb9ae2d2ceb2b29de5fd96bfb64ec8aabebf8
# - f49b35c1f23eaeaeba5ce86ea25afb8f71caec61
# -----------------------------------------------------------------------------

  include CompoundIdReader # need to support ECL VCs.
  include AuthenticationHelper


  layout 'card_destination'

  skip_filter :authenticate_user!, only: [:visit]
  before_action :check_card_paid_by_user, only: [:visit]

  def check_card_paid_by_user
    @card = load_card_from_compound_id(params[:id])
    paywall_enabled = @card.lxp_payment_enabled?
    if paywall_enabled && (@card.is_paid_card && !@card.paid_by_user?(current_user))
      render_with_status_and_message('Payment is not initiated or confirmed.', status: :not_paid) and return
    end
  end

  def visit
    url = fetch_url params[:id]
    record_card_click_through_metric(@card, current_user)
    redirect_to url
  end

  def visit_url
    @url = fetch_url params[:id]
  end

  def fetch_url id
    @card ||= load_card_from_compound_id(params[:id])
    @card.skip_indexing = true # not worth reindexing the card over this.

    if partner_center_link = @card.integrated_partner_link
      return partner_center_link
    end

    url = @card.resource.try(:url_with_context, current_user.try(:id)) || @card.get_content('link_url')

    unless (browser.bot? || browser.search_engine?)
      @card.increment!(:outbounds_count)
      user_id = user_signed_in? ? current_user.id : nil
      Tracking::track(user_id, 'act', {type: 'visit', object_id: @card.id, object: 'card',
                             platform: get_platform,
                             organization_id: current_user.try(:organization_id),
                             referer: params[:referer].presence || request.referer
                             })
    end
    url
  end

  def record_card_click_through_metric(card, user)
    return unless card && user
    Analytics::ViewEventMetricRecorder.record_click_through_event(
      entity: card,
      viewer: user
    )
  end

end
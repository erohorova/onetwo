require 'active_support/concern'

# =========================================
# This module is included by Api::V2::TeamsController
# It provides endpoints related to the teams leaderboard.
# =========================================

module TeamControllerLeaderboardConcern

  extend ActiveSupport::Concern

  included do

# =========================================
# analytics_v2 endpoint
# Is used to get leaderboard for a SINGLE TEAM
# To view leaderboard of multiple teams, use metrics_v2 endpoint.
# =========================================

    api :GET, 'api/v2/teams/:id/analytics_v2',
        'Analytics for a single team'

    param :id, Integer, required: true,
          desc: "ID of team for which analytics is to be fetched"
          
    param :from_date, String, required: true,
          desc: "Starting date. Format: mm/dd/yyyy. Metrics are inclusive of this date"
          
    param :to_date, String, required: true,
          desc: "End date. Format: mm/dd/yyyy. Metrics are inclusive of this date"
          
    # param :team_ids, Array, desc: "array of team ids to filter on", required: false
    def analytics_v2
      query_params = params
        .slice(:from_date, :to_date)
        .merge(team_ids: [@team.id])

      results = Team.fetch_v2_analytics(
        params: query_params,
        current_org: current_org,
      )
      render json: results, status: :ok
    rescue TeamModelLeaderboardV2Concern::ParameterError => e
      render_unprocessable_entity(e.message)
    end

# =========================================
# metrics_v2 endpoint
# Is used to get leaderboard for multiple teams (e.g. on admin page)
# =========================================

    api :GET, 'api/v2/teams/metrics_v2',
        'Analytics for teams in an org'
        
    param :team_ids, Array, required: false,
          desc: "if given, only show analytics for these teams"
          
    param :from_date, String, required: true,
          desc: "Starting date (inclusive). Format: mm/dd/yyyy."
          
    param :to_date, String, required: true,
          desc: "End date (inclusive). Format: mm/dd/yyyy."
          
    param :limit, Integer, required: false,
          desc: "Number of results to return. Defaults to 10"
          
    param :offset, Integer, required: false,
          desc: "Offset of results for pagination. Defaults to 0."
          
    def metrics_v2
      query_params = params
        .slice(:from_date, :to_date, :team_ids)
        .merge(limit: limit, offset: offset)

      results = Team.fetch_v2_analytics(
        params:       query_params,
        current_org:  current_org,
      )
      render json: results, status: :ok
    rescue TeamModelLeaderboardV2Concern::ParameterError => e
      render_unprocessable_entity(e.message)
    end


  end

end


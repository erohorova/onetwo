module SessionConcern
  extend ActiveSupport::Concern
  include EventTracker

  def record_logged_in_event(org: , user:)
    payload = {
      controller:     self.class.name,
      action:         self.action_name,
      method:         request.method,
      path:           (request.fullpath rescue "unknown"),
      user_id:        user.id,
      platform:       defined?(get_platform) ? get_platform : '',
      request_ip:     request.ip,
      request_host:   request.headers.env['HTTP_HOST'],
      build_number:   request.headers.env['HTTP_X_BUILD_NUMBER'],
      platform_version_number: request.headers.env['HTTP_X_VERSION_NUMBER'],
      user_agent:     request.headers.env['HTTP_USER_AGENT']
    }

    Analytics::MetricsRecorderJob.perform_later(
      recorder: "Analytics::UserLoginMetricsRecorder",
      metadata: payload,
      timestamp: Time.now.to_i,
      actor: Analytics::MetricsRecorder.user_attributes(user),
      org: Analytics::MetricsRecorder.org_attributes(org)
    )
  end
end
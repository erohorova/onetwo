module RequestIp
  extend ActiveSupport::Concern

  # Amazon Cloudfront changes client ip addresses for custom domains
  # To get actual user ip we have to use X-Forwarded-For header
  # Reference: http://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/RequestAndResponseBehaviorCustomOrigin.html#RequestCustomIPAddresses
  def parse_user_ip
    ips = request.env["HTTP_X_FORWARDED_FOR"]
    return ips.split(',').first if ips

    # Fall back
    request.remote_ip.partition(':').first
  end
end

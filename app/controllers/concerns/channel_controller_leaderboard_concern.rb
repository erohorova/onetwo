require 'active_support/concern'

# =========================================
# This module is included by Api::V2::ChannelsController
# It provides endpoints related to the channel leaderboard.
# =========================================

module ChannelControllerLeaderboardConcern

  extend ActiveSupport::Concern

  included do

    # Provides the add_null_records_for_empty_days! method
    include AggregationQueryUtil

    api :GET, 'api/v2/channels/:id/aggregated_metrics', 'Get aggregation metrics for a channel'
    param :id, Integer, desc: 'Channel id', required: true
    param :start_date, String, desc: "Start or from date for events to pull from.
      Format: epoch timestamp", required: true
    param :end_date, String, desc: "end or till date for events to pull. default: none", required: false
    formats ['json']
    api_version 'v2'

    def aggregated_metrics
      unless start_date = params[:start_date].try(:to_i)
        render_unprocessable_entity("No start date given"); return
      end
      # End date defaults to the current time
      end_date = params[:end_date].try(:to_i) || Time.now.to_i
      # First we fetch channel_aggregation_daily records from Influx
      # for the given date range and channel id.
      results = get_channel_aggregations(start_date, end_date)
      results[0] ||= {}
      results[0]["name"] ||= Analytics::ChannelMetricsAggregationRecorder::MEASUREMENT
      results[0]["tags"] ||= {}
      results[0]["values"] ||= []
      add_fill_records_for_empty_days!(results, start_date, end_date)
      # We apply some special formatting to the results.
      format_channel_aggregations!(results)
      # Next, fetch the card and user leaderboards for the channel (from SQL)
      # we're going to merge the results into the "tags" hash
      tags = results[0]["tags"]
      leaderboard_opts = {
        limit: 10,
        offset: 0,
        start_date: start_date,
        end_date: end_date
      }
      tags["top_cards"] = @channel.get_top_cards_data(leaderboard_opts)
      tags["top_contributors"] = @channel.get_top_contributors(leaderboard_opts)
      tags["top_scoring_users"] = @channel.get_top_scoring_users(leaderboard_opts)
      # Finally, return the results.
      render json: results || []
    rescue InfluxQuery::InfluxQueryError => e
      render_unprocessable_entity(e.message)
    end

    api :GET, 'api/v2/channels/:id/top_cards', 'Get top cards for a channel'
    param :id, Integer, desc: 'Channel id', required: true
    param :start_date, String, desc: "Start or from date for events to pull from.
      Format: epoch timestamp", required: true
    param :end_date, String, desc: "end or till date for events to pull. default: none", required: false
    param :limit, Integer, desc: 'Limit, default: 10', required: false
    param :offset, Integer, desc: 'Offset, default: 0', required: false
    formats ['json']
    api_version 'v2'
    def top_cards
      unless start_date = params[:start_date].try(:to_i)
        render_unprocessable_entity("No start date given"); return
      end
      # End date defaults to the current time
      end_date = params[:end_date].try(:to_i) || Time.now.to_i
      results = {}
      results["top_cards"] = @channel.get_top_cards_data(
        limit: limit || 10,
        offset: offset || 0,
        start_date: start_date,
        end_date: end_date
      )
      render json: results, status: :ok
    end

    api :GET, 'api/v2/channels/:id/top_users', 'Get top users for a channel'
    param :id, Integer, desc: 'Channel id', required: true
    param :start_date, String, desc: "Start or from date for events to pull from.
      Format: epoch timestamp", required: true
    param :end_date, String, desc: "end or till date for events to pull. default: none", required: false
    param :limit, Integer, desc: 'Limit, default: 10', required: false
    param :offset, Integer, desc: 'Offset, default: 0', required: false
    formats ['json']
    api_version 'v2'
    def top_users
      unless start_date = params[:start_date].try(:to_i)
        render_unprocessable_entity("No start date given"); return
      end
      # End date defaults to the current time
      end_date = params[:end_date].try(:to_i) || Time.now.to_i
      results = {}
      opts = {
        limit: limit || 10,
        offset: offset || 0,
        start_date: start_date,
        end_date: end_date
      }
      results["top_contributors"] = @channel.get_top_contributors(opts)
      results["top_scoring_users"] = @channel.get_top_scoring_users(opts)
      render json: results, status: :ok
    end

  end

  private

  def get_channel_aggregations(start_date, end_date)
    measurement = Analytics::ChannelMetricsAggregationRecorder::MEASUREMENT
    query, fetch_params = InfluxQuery.get_query_for_entity(
      entity: measurement,
      organization_id: current_org.id,
      filters: {
        start_date: start_date,
        end_date: end_date,
        channel_id: @channel.id.to_s,
      }
    )
    InfluxQuery.fetch(query, fetch_params)
  end

  def add_fill_records_for_empty_days!(results, start_date, end_date)
    results&.each do |result|
      result_obj = result["values"].index_by do |record|
        Time.parse(record["time"]).utc.to_i
      end
      add_null_records_for_empty_days!(
        results: result_obj,
        start_time: start_date,
        end_time: end_date
      ) do |obj, start_time:, end_time:|
        result["values"].push default_record.merge(
          "time" => "#{Time.at(start_time).utc.strftime("%Y-%m-%d")}T00:00:00Z"
        )
      end
      # We apply manual sorting, to ensure they are returned to the client
      # in the correct order.
      result["values"].sort_by! { |record| Time.parse(record["time"]) }
    end
  end

  def format_channel_aggregations!(results)
    results&.each do |result|
      result["values"].each do |val|
        val["num_monthly_active_users"] = {
          "total" => val.delete("num_monthly_active_users_total") || 0
        }
        val.keys.each do |key|
          val[key] ||= 0
          platform = key
            .scan(/num_monthly_active_users_in_(.+)_platform/)
            .flatten
            .first
          next unless platform
          val["num_monthly_active_users"][platform] = val.delete(key)
        end
      end
    end
  end

  def default_record
    {
      "analytics_version" => Settings.influxdb.event_tracking_version,
      "channel_id" => @channel.id.to_s,
      "num_cards_added_to_channel" => 0,
      "num_cards_commented_in_channel" => 0,
      "num_cards_completed_in_channel" => 0,
      "num_cards_liked_in_channel" => 0,
      "num_cards_removed_from_channel" => 0,
      "num_channel_follows" => 0,
      "num_channel_visits" => 0,
      "num_monthly_active_users_in_android_platform" => 0,
      "num_monthly_active_users_in_extension_platform" => 0,
      "num_monthly_active_users_in_ios_platform" => 0,
      "num_monthly_active_users_in_web_platform" => 0,
      "num_monthly_active_users_total" => 0,
      "org_id" => current_org.id.to_s
    }
  end

end


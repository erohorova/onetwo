module TrackActions
  extend ActiveSupport::Concern

  def track_action(record, action_name, actor= nil)
    user = actor || current_user

    Tracking::track_act(
      user_id: user.id,
      object: record.class.name.underscore,
      object_id: record.id,
      action: action_name,
      platform: get_platform,
      device_id: cookies[:_d],
      organization_id: user.organization_id,
      referer: params[:referer].presence || request.referer
    )
  end

  def track_mark_as_completion(record)
    self_or_assigned = current_user.has_an_assignment?(record) ? 'assigned' : 'self'
    track_action(record, "mark_completed_#{self_or_assigned}")
  end

  def track_mark_as_uncompletion(record)
    self_or_assigned = current_user.has_an_assignment?(record) ? 'assigned' : 'self'
    track_action(record, "mark_uncompleted_#{self_or_assigned}")
  end

end
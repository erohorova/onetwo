#All common methods for invalid response and validating request
module RequestHelper
  extend ActiveSupport::Concern

  # DEPRECATED
  def get_subdomain
    request.host.split('.').first.downcase
  end

  def require_organization_admin
    return render_unauthorized unless current_org.is_admin?(current_user)
  end

  # Error code renders
  def render_unauthorized(message = nil)
    render_with_status_and_message(message, status: :unauthorized)
  end

  def render_unprocessable_entity(message = nil)
    render_with_status_and_message(message, status: :unprocessable_entity)
  end

  def render_bad_request(message = nil)
    render_with_status_and_message(message, status: :bad_request)
  end

  def render_not_found(message = nil)
    render_with_status_and_message(message, status: :not_found)
  end

  def render_with_status_and_message(message = nil , status:)
    if message
      render json: {message: message}, status: status
    else
      render json: :no_content, status: status
    end
  end
end
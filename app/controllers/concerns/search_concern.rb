module SearchConcern
  extend ActiveSupport::Concern

  def do_search(_params, query_type:'and')
    @query = _params[:q]
    set_limit_offset(_params)

    @aggs = []
    if params[:source_id].blank? && !interactive_search? && !date_filter? && !rating_or_level? && !price_filter? && !is_paid_filter? && !plan_filter?
      if _params[:content_type].blank? || _params[:content_type].include?('channel')
        @channels, total = do_channels_search(viewer: current_user, query: _params[:q])
        if total > 0
          load_is_following_for(@channels.map(&:id), 'Channel', current_user)
          @aggs << { 'display_name' => "Channels", 'type' => "content_type", 'id' => "channel", 'count' => total }
        end
      end

      if (_params[:content_type].blank? || _params[:content_type].include?('user')) && !current_org.disable_user_search?
        @users, total = do_users_search(
          viewer: current_user,
          query: _params[:q],
          search_by: _params[:search_by]
        )
        if total > 0
          load_is_following_for(@users.map(&:id), 'User', current_user)
          @aggs << { 'display_name' => "Users",  'type' => "content_type", 'id' => "user", 'count' => total }
        end
      end

      if _params[:content_type]&.include?('team')
        @teams, total = do_teams_search(viewer: current_user, query: _params[:q])
        if total > 0
          @aggs << { 'display_name' => "Teams", 'type' => "content_type", 'id' => "team", 'count' => total}
        end
      end
    end

    if _params[:exclude_cards].present?
      exclude_ecl_card_ids = []
      if _params[:exclude_cards].include?('completed')
        exclude_ecl_card_ids += Card.joins(:user_content_completions).where(
          "user_content_completions.state = '#{UserContentCompletion::COMPLETED}' AND
            user_content_completions.user_id = #{current_user.id}"
        ).where.not(ecl_id: nil).pluck(:ecl_id)
      end
      _params[:exclude_ecl_card_id] = exclude_ecl_card_ids
      _params.delete :exclude_cards
    end

    # Add access control parameters
    _params[:users_with_access] = [current_user.id]
    _params[:groups_with_access] = TeamsUser.where(user: current_user).pluck(:team_id)
    _params[:channels_with_access] = current_user.followed_channels.pluck(:id)
    _params[:is_admin] = current_user.is_org_admin?

    @cards, @aggs, @pathways, @journeys = ecl_content_search(_params, query_type, @aggs)
  end

  def do_users_search(viewer:, query:, search_by: nil)
    filter = {}
    if search_by.in? %w[name skills]
      filter[:search_by] = if search_by == 'skills'
        ['expert_topics.searchable_topic_label']
      else
        %w[name.searchable first_name.searchable last_name.searchable]
      end
    end
    res = Search::UserSearch.new.search(
      q: query,
      viewer: viewer,
      filter: filter,
      offset: @users_offset,
      limit: @users_limit,
      load: true
    )
    [res.results, res.total]
  end

  def do_teams_search(viewer:, query:)
    res = Search::TeamSearch.new.search(
      q: query,
      viewer: viewer,
      filter: { skip_aggs: true },
      offset: @teams_offset,
      limit: @teams_limit,
      load: true
    )
    [res.results, res.total]
  end

  def do_channels_search(viewer:, query:)
    res = Search::ChannelSearch.new.search(
      q: query,
      viewer: viewer,
      limit: @channels_limit,
      offset: @channels_offset,
      filter: { include_private: true, skip_aggs: true },
      load: true,
      sort: [{key: '_score', order: :desc}]
    )
    [res.results, res.total]
  end

  def date_filter?
    params[:from_date].present? || params[:till_date].present?
  end

  def ecl_content_search(_params, query_type, aggs)
    cards = []
    pathways = []
    journeys = []
    _params[:ecl_card_ids] = get_interactive_card_ecl_ids(params).compact if interactive_search?
    unless not_content_search?(_params)
      _params["content_type"] -= ['user', 'channel'] if _params["content_type"]
      rank = true
      if !_params[:rank].nil? && _params[:rank].to_s == 'false'
        rank = false
      end
      load_tags = true
      if !_params[:load_tags].nil? && _params[:load_tags].to_s == 'false'
        load_tags = false
      end

      filter_params = {
                    content_type: _params["content_type"],
                    type: _params["type"] || 'search',
                    source_id: _params[:source_id],
                    provider_name: _params[:provider_name],
                    languages: _params[:languages],
                    subjects: _params[:subjects],
                    source_type_name: _params[:source_type_name],
                    rank: rank, rank_params: { user_ids: current_org.sme_ids },
                    exclude_ecl_card_id: _params[:exclude_ecl_card_id],
                    topic: _params[:topic],
                    users_with_access: _params[:users_with_access],
                    groups_with_access: _params[:groups_with_access],
                    channels_with_access: _params[:channels_with_access],
                    only_private: _params[:only_private],
                    group_id: _params[:group_id],
                    channel_id: _params[:channel_id],
                    is_admin: _params[:is_admin]
                  }

      filter_params.merge!(ecl_card_ids: _params[:ecl_card_ids]) if _params[:ecl_card_ids].present?
      filter_params.merge!(from_date: _params[:from_date]) if _params[:from_date].present?
      filter_params.merge!(till_date: _params[:till_date]) if _params[:till_date].present?
      filter_params.merge!(sociative: current_org.search_with_sociative?) if current_org.search_with_sociative?
      filter_params.merge!(level: _params[:level]) if _params[:level].present?
      filter_params.merge!(rating: _params[:rating]) if _params[:rating].present?
      filter_params.merge!(currency: _params[:currency]) if _params[:currency].present?
      filter_params.merge!(amount_range: _params[:amount_range]) if _params[:amount_range].present?
      filter_params.merge!(is_paid: _params[:is_paid]) if _params[:is_paid].present?
      filter_params.merge!(plan: _params[:plan]) if _params[:plan].present?
      filter_params.merge!(sort_attr: _params[:sort_attr],
        sort_order: _params[:sort_order]) if _params[:sort_attr].present? && _params[:sort_order].present?
      filter_params.merge!(load_hidden: _params[:load_hidden] == 'true')

      if _params["segregate_pathways"] == 'true'
        cards, content_aggs, pathways, journeys = search_with_pathway_and_journey(_params, filter_params, query_type, cards,  load_tags)
      else
        cards, content_aggs = search(_params, filter_params, query_type, cards, load_tags)
      end
    end

    unless _params[:load_hidden] == 'true'
      cards = cards.reject(&:hidden)
    end

    aggs += content_aggs if content_aggs
    [cards, aggs, pathways, journeys]
  end

  def search(_params, filter_params, query_type, cards, load_tags)
    res = EclApi::EclSearch.new(current_org.id, current_user.try(:id)).search(
      query: _params[:q],
      load_tags: load_tags,
      limit: @cards_limit,
      offset: @cards_offset,
      query_type: query_type,
      filter_params: filter_params
    )

    cards = res.results
    aggs = res.aggregations

    [cards, aggs]
  end

  def search_with_pathway_and_journey(_params, filter_params, query_type, cards, load_tags)
    pathways, pathways_aggs, journeys, journeys_aggs, cards, cards_aggs = Array.new(6) { [] }
    cards, cards_aggs = smartbite_search(_params, filter_params, query_type, cards, load_tags) if do_smartbite_search?(_params)
    pathways, pathways_aggs = pathway_search(_params, filter_params, query_type, cards, load_tags) if do_pathway_search?(_params)
    journeys, journeys_aggs = journey_search(_params, filter_params, query_type, cards, load_tags) if do_journey_search?(_params)

    aggs = merge_aggs(cards_aggs, pathways_aggs, journeys_aggs)

    [cards, aggs, pathways, journeys]
  end

  def smartbite_search(_params, filter_params, query_type, cards, load_tags)
    filter_params[:content_type] = EclApi::EclSearch::CARD_CONTENT_TYPE
    if _params["content_type"].present?
      filter_params[:content_type] = filter_params[:content_type] & _params["content_type"]
    end

    search(_params, filter_params, query_type, cards, load_tags)
  end

  def pathway_search(_params, filter_params, query_type, cards, load_tags)
    filter_params[:content_type] = [EclApi::EclSearch::PATHWAY_CONTENT_TYPE]

    search(_params, filter_params, query_type, cards, load_tags)
  end

  def journey_search(_params, filter_params, query_type, cards, load_tags)
    filter_params[:content_type] = [EclApi::EclSearch::JOURNEY_CONTENT_TYPE]

    search(_params, filter_params, query_type, cards, load_tags)
  end

  def merge_aggs(aggs1, aggs2, aggs3)
    final_aggs = []

    #Getting uniq aggregation keys from all sets
    agg_ids = aggs1.map{|a| a["id"]} | aggs2.map{|a| a["id"]} | aggs3.map{|a| a["id"]}

    agg_ids.each do |id|
      #Find out count for a key from all sets
      agg1_count = aggs1.find{|a| a["id"] == id}.try(:[], "count")
      agg2_count = aggs2.find{|a| a["id"] == id}.try(:[], "count")
      agg3_count = aggs3.find{|a| a["id"] == id}.try(:[], "count")
      #Find out Aggregation hash from any set
      aggregation = aggs1.find{|a| a["id"] == id} || aggs2.find{|a| a["id"] == id} || aggs3.find{|a| a["id"] == id}

      #SUM All 3 aggs for a key.
      #nil.to_i will be 0 so it will work if key is not present in particular aggs
      aggregation["count"] = agg1_count.to_i + agg2_count.to_i + agg3_count.to_i

      final_aggs << aggregation
    end

    final_aggs
  end

  def do_pathway_search?(_params)
    _params["content_type"].blank? || _params["content_type"].include?(EclApi::EclSearch::PATHWAY_CONTENT_TYPE)
  end

  def do_journey_search?(_params)
    _params["content_type"].blank? || _params["content_type"].include?(EclApi::EclSearch::JOURNEY_CONTENT_TYPE)
  end

  def do_smartbite_search?(_params)
    _params["content_type"].blank? || (_params["content_type"] & EclApi::EclSearch::CARD_CONTENT_TYPE).present?
  end

  #params[:content_type] can be one of the following
  # ['channel'], ['user'], ['channel', 'user'], nil, []
  def not_content_search? params
    content_type_filter = params[:content_type]
    (content_type_filter && ((!(content_type_filter & Card::CONTENT_TYPES).any? && (content_type_filter & ['channel', 'user']).any?)) || (interactive_search? && !params[:ecl_card_ids].present?))
  end

  def interactive_search?
    [params[:liked_by], params[:commented_by], params[:posted_by], params[:bookmarked_by]].compact.present?
  end

  def rating_or_level?
    [params[:level], params[:rating]].compact.present?
  end

  def price_filter?
    [params[:currency], params[:amount_range]].compact.present?
  end

  def is_paid_filter?
    params[:is_paid].present?
  end

  def plan_filter?
    params[:plan].present?
  end

  def get_votable_ids(user_ids)
    Vote.where(user_id: user_ids, votable_type: "Card").pluck(:votable_id)
  end

  def get_commentable_ids(user_ids)
    Comment.where(user_id: user_ids, commentable_type: "Card").pluck(:commentable_id)
  end

  def get_postable_ids(user_ids)
    Card.where(author_id: user_ids).pluck(:id)
  end

  def get_bookmarkable_ids(user_ids)
    Bookmark.where(user_id: user_ids, bookmarkable_type: "Card").pluck(:bookmarkable_id)
  end

  #get cards liked_by, commented by, posted by and bookmarked by
  #Result should be intersection of all categories
  #[[1, 2, 3], [2, 3, 4], [2, 5, 6]] result would be [2]
  #[[1, 2, 3], [4, 5, 6], [7, 8, 9]] result would be []
  def get_interactive_card_ecl_ids(params)
    card_ids = []

    card_ids << get_votable_ids(liked_user_ids(params)) if params[:liked_by].present?
    card_ids << get_commentable_ids(commented_user_ids(params)) if params[:commented_by].present?
    card_ids << get_postable_ids(posted_user_ids(params)) if params[:posted_by].present?
    card_ids << get_bookmarkable_ids(bookmarked_user_ids(params)) if params[:bookmarked_by].present?
    card_ids = card_ids.inject(&:&)

    Card.where(id: card_ids).pluck(:ecl_id)
  end

  def get_user_ids(option)
    user_ids = []
    user_ids |= current_user.follower_ids if option[:followers].present?
    user_ids |= current_user.following_ids if option[:following].present?
    user_ids |= option[:user_ids].split(",").flatten if option[:user_ids].present?
    user_ids
  end

  #Result should be OR based within same section
  def liked_user_ids(params)
    get_user_ids(params[:liked_by])
  end

  def commented_user_ids(params)
    get_user_ids(params[:commented_by])
  end

  def posted_user_ids(params)
    get_user_ids(params[:posted_by])
  end

  def bookmarked_user_ids(params)
    get_user_ids(params[:bookmarked_by])
  end

  def set_limit_offset(_params)
    #required in response
    @limit = limit
    @offset = offset
    @cards_limit = _params[:cards_limit] || limit
    @cards_offset = _params[:cards_offset] || offset
    @channels_limit = _params[:channels_limit] || limit
    @channels_offset = _params[:channels_offset] || offset
    @teams_limit = _params[:teams_limit] || limit
    @teams_offset = _params[:teams_offset] || offset
    @users_limit = _params[:users_limit] || limit
    @users_offset = _params[:users_offset] || offset
  end
end

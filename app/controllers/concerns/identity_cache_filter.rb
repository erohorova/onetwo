require 'active_support/concern'
#see https://github.com/Shopify/identity_cache#memoized-cache-proxy
module IdentityCacheFilter
  extend ActiveSupport::Concern

  included do
    around_filter :identity_cache_memoization
  end

  def identity_cache_memoization
    IdentityCache.cache.with_memoization{ yield }
  end
end

require 'active_support/concern'


#EdCast/1.0 CFNetwork/711.1.12 Darwin/14.0.0    GET request
#EdCast/1.0 (iPhone; iOS 8.1.1; Scale/2.00)     non-GET request
#EdCast/1.0 (iPhone Simulator; iOS 8.1; Scale/2.00) non-GET request
#Edcast-Android #all
module DetectPlatform
  extend ActiveSupport::Concern
  include NativeAppDetection

  def get_platform
    if is_native_app?
      platform = native_app_platform
    else
      if (controller_name == 'ui' && action_name.starts_with?('qa')) || (self.respond_to?(:is_api_client_request?) && is_api_client_request?(params)) #if not native app, but using apikey is most likely forum request
        platform = 'forum'
      elsif request.env['HTTP_ORIGIN'].try(:starts_with?, 'chrome-extension')
        platform = 'extension'
      else
        platform = 'web'
      end
    end
    platform
  end

  def is_mobile_browser?
    request.user_agent =~ /\b(Android|iPhone|iPad)\b/i
  end

  def is_branch_metrics_request?
    /Branch Metrics/i.match(request.user_agent).length > 0
  end
end

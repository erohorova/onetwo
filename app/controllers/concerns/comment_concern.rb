module CommentConcern
  extend ActiveSupport::Concern

  def api_type
    if request.url.include? 'v2/'
      request.url.split('v2/')[1].split('/')[0]
    else
      'cards'
    end
  end

  def find_commentable
    case api_type
    when 'activity_streams'
      current_org.activity_streams.find(params[:activity_stream_id])
    when 'organizations'
      current_org
    when 'teams'
      current_org.teams.find(params[:team_id])
    else
      if request.method == 'POST'
        load_card_from_compound_id(params[:card_id])
      else
        current_org.cards.friendly.find(params[:card_id])
      end
    end
  end

  def create_comment
    commentable = find_commentable
    @comment = commentable.comments.build(comment_params.merge(user_id: current_user.id))
    @comment.save
  end

  def delete_comment
    commentable = find_commentable
    eligible_comments = commentable.comments

    # Team admin or card author can delete any comment on the card, otherwise a user can delete their own comments only
    unless current_user.is_admin_user? || (commentable.class == Card && commentable.author_id == current_user.id)
      eligible_comments = eligible_comments.where(user_id: current_user.id)
    end

    @comment = eligible_comments.find(params[:id])

    CommentReporting.unscoped.where(comment_id: @comment.id).delete_all

    # We do not want to change the hard delete feature by addition of paranoia
    # in comment. Hence, used destroy_without_paranoia
    @comment.destroy_without_paranoia
  end

  def show_comment
    commentable = find_commentable
    @comment = commentable.comments.find(params[:id])
  end

  def trash_comment
    @comment = Comment.where(id: params[:id]).first
    service = FlagContent::CommentTrashingService.new(comment: @comment)
    service.call
    service.status
  end

  protected
    def track_comment(action: "comment")
      commentable = @comment.commentable
      track_opts = {
          user_id: current_user.id,
          object: commentable.class.name.underscore,
          object_id: commentable.id,
          action: action,
          platform: get_platform,
          device_id: cookies[:_d],
          organization_id: current_user.organization_id,
          referer: params[:referer].presence || request.referer
      }
      track_opts.merge!(
        card: ClientOnlyRoutes.show_insight_url(commentable)
      ) unless @comment.non_triggerable_comment?
      Tracking::track_act track_opts
    end
end

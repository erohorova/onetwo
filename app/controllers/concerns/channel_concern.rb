require 'active_support/concern'
module ChannelConcern
  extend ActiveSupport::Concern

  def prepare_channel_params(attrs)
    attrs["mobile_image"] = Filestack::Security.new.signed_url(attrs["mobile_image"]) if is_filestack?(attrs["mobile_image"])
    attrs["provider_image"] = Filestack::Security.read_filestack_url(attrs["provider_image"]) if attrs["provider_image"].present?
    #want to set language only if language parameter is present and its empty
    attrs["language"] = nil if params[:language] and params[:language].empty?
    attrs["content_type"] = nil if params[:language] and params[:language].empty?
    attrs
  end

  def is_filestack?(image_url)
    return false if image_url.blank?
    host = URI.parse(image_url).host
    image_url && host == "cdn.filestackcontent.com"
  end

  def channel_list
    order = {is_promoted: :desc}
    order.merge!(label: params[:order_label].presence || :asc)

    @channels = Channel.fetch_eligible_channels_for_user(user: current_user, q: params[:q]).limit(limit).offset(offset).order(order)
    following_channel_ids = current_user.follows.where(followable_type: 'Channel', followable_id: @channels.map(&:id)).pluck(:followable_id)

    @following = Hash[@channels.map {|channel| [channel, following_channel_ids.include?(channel.id)]}]
  end

  def current_channel
    @channel = current_org.channels.friendly.find(params[:id])
  end

  def verify_batch_params
    unless params[:channel_ids].is_a?(Array) && params[:channel_ids].present?
      render_unprocessable_entity('Missing or invalid required params')
    end
  end

  def channels_actions(action)
    channels = current_org.channels.where(id: params[:channel_ids])

    invalid_channel_ids = channels.reject(&action.to_sym).map(&:id)
    message = "There were errors when calling #{action} on the following channel ids: #{invalid_channel_ids.join(', ')}"
    return render_with_status_and_message(message, status: :ok) if invalid_channel_ids.any?

    head :ok
  end

  def require_channel_read_access
    return render_unauthorized('You are not authorized to access this channel') unless @channel.has_read_access?(current_user)
  end

  def require_org_admin_or_channel_collaborator
    unless (@channel.is_author?(current_user) || current_user.is_admin_user?)
      return render_unauthorized('You are not authorized to add or remove collaborators to this channel')
    end
  end

  def require_org_admin_or_channel_curator
    is_curator = @channel&.is_curator?(current_user)
    is_admin = current_org.has_admin_access?(current_user)
    unless is_curator || is_admin
      render_unauthorized("must be curator of channel or organization admin")
    end
  end

  def channel_search(query, offset, limit)
    filters = prepare_channel_search_params(params)
    Search::ChannelSearch.new.search(
      q: query,
      viewer: current_user,
      filter: filters,
      offset: offset,
      limit: limit,
      sort: [ { key: '_score', order: :desc } ],
      load: true
    )
  end

  def card_search_in_channel(query, offset, limit)
    filters = prepare_card_search_in_channel_params(params)
    Search::CardSearch.new.search_in_channel(
      q: query,
      channel_id: @channel.id,
      viewer: current_user,
      organization_id: current_org.id,
      filter: filters,
      limit: limit,
      offset: offset,
      load: true
    )
  end

  def prepare_channel_search_params(attrs)
    filters = {}

    # We are skipping aggregation by default as this not requried on channels/all and channels/my page.
    # We need aggregation only for this `channels/search?q=` i.e when q is present.
    # For above we are sending skip_aggs=false.
    filters[:skip_aggs] = params[:skip_aggs].nil? ? true : params[:skip_aggs]&.to_bool

    filters[:include_private] = true

    filters[:is_private] = attrs[:is_private] if attrs[:is_private].present?
    filters[:curators_names] = attrs[:curators_names] if attrs[:curators_names]&.is_a?(Array)
    filters[:collaborators_names] = attrs[:collaborators_names] if attrs[:collaborators_names]&.is_a?(Array)
    filters[:followers_names] = attrs[:followers_names] if attrs[:followers_names]&.is_a?(Array)
    filters[:only_my_channels] = attrs[:only_my_channels] if attrs[:only_my_channels]&.to_bool
    filters
  end

  def prepare_card_search_in_channel_params(attrs)
    filters = {}
    filters[:content_type] = attrs[:content_type] if attrs[:content_type]&.is_a?(Array)
    filters[:source_display_name] = attrs[:source_display_name] if attrs[:source_display_name]&.is_a?(Array)
    filters[:author_id] = attrs[:author_id] if attrs[:author_id]&.is_a?(Array)
    filters[:date_range] = attrs[:date_range] if attrs[:date_range].present? && attrs[:date_range].is_a?(Array)
    # Do not skip aggregations by default
    filters[:skip_aggs] = params[:skip_aggs].present? ? params[:skip_aggs]&.to_bool : false
    filters[:is_cms] = params[:is_cms].present? ? params[:is_cms]&.to_bool : false
    filters
  end
end

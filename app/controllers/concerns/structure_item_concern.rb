require 'active_support/concern'
module StructureItemConcern
  extend ActiveSupport::Concern

  def generate_response
    response = {}
    channels = @channels.present? ? get_channel_response(@channels) : []
    cards = @cards.present? ? get_card_response(@cards) : []
    users = @users.present? ? get_user_response(@users) : []

    @all_structure_items.group_by{|si| si.structure_id}.each do |key, structure_items|
      response[key] ||= {}
      case structure_items.first.entity_type
      when 'Channel'
          response[key] = generate_entity_hash(structure_items, channels)
      when 'Card'
          response[key] = generate_entity_hash(structure_items, cards)
      when 'User'
          response[key] = generate_entity_hash(structure_items, users)
      end
    end
    response
  end

  def generate_entity_hash(structure_items, entities)
    data = {}
    structure_items.sort_by(&:position).each do |str_item|
      entity = entities.find{|en| str_item.entity_id == en[:id].to_i }
      data[str_item.position] = { entity: entity } if entity.present?
    end
    data
  end

  def get_channel_response(channels)
    channel_fields = params[:channel_fields] || 'id,label,description,isPrivate,allowFollow,bannerImageUrls,profileImageUrl,profileImageUrls,isFollowing,updatedAt,slug'
    ChannelBridge.new(channels, user: current_user, fields: params[:channel_fields]).attributes
  end

  def get_card_response(cards)
    CardBridge.new(cards, user: current_user, options: { is_native_app: is_native_app? }).attributes
  end

  def get_user_response(users)
    user_fields = params[:user_fields] || 'id,handle,avatarimages,roles,name,is_following,following_count,followers_count,expert_skills,status,company'
    UserBridge.new(users, user: current_user, fields: user_fields).attributes
  end

  def fetch_viewable_structure_items(filter_by_language: false)
    # Cache organization structures, invalidated in 2 hours or on org structure update
    org_structures = Rails.cache.fetch("org-#{current_org.id}-structures", expires_in: 2.hours) do
      current_org.structures.active || []
    end
    @structures = org_structures.select{|structure| params[:structure_ids].map(&:to_i).include?(structure.id)}

    cards, channel_ids, @users, @all_structure_items = get_entity_data
    cards = cards.filtered_by_language(current_user.language) if cards.present? && filter_by_language
    @cards = cards.present? ? (current_user.is_org_admin? ? cards : cards.visible_to_userV2(current_user)) : []
    @channels = channel_ids.present? ? current_user.viewable_channels(channel_ids) : []
  end

  def get_entity_data
    # Cache entities, invalidated in 2 hours or on org structure_item update
    all_structure_items = Rails.cache.fetch("org-#{current_org.id}-structure-entities", expires_in: 2.hours) do
      StructuredItem.where(structure_id: current_org.structures.active.pluck(:id)) || []
    end
    channel_ids, card_ids, user_ids = [], [], []
    @structures.each do |structure|
      structure_items = all_structure_items.select{|str_item| str_item.structure_id == structure.id }
      entity_ids = structure_items.map{|str_item| str_item.entity_id }
      case structure.slug
      when 'discover-channels'
        channel_ids.push(entity_ids)
      when 'discover-cards', 'channel-cards'
        card_ids.push(entity_ids)
      when 'discover-users', 'channel-users'
        user_ids.push(entity_ids)
      end
    end
    _structure_items = all_structure_items.select{|str_item| params[:structure_ids].map(&:to_i).include?(str_item.structure_id)} || []
    cards = card_ids.present? ? Card.where(id: card_ids.flatten.uniq) : []
    users = user_ids.present? ? User.not_suspended.where(id: user_ids.flatten.uniq) : []
    [cards, channel_ids.flatten.uniq, users, _structure_items]
  end
end

require 'active_support/concern'

module CardMetadata
  extend ActiveSupport::Concern
  included do

    after_commit :recalculate_cover_card_metadata, on: [:create, :destroy]

    def recalculate_cover_card_metadata
      card = Card.find_by(id: cover_id)
      unless card.nil?
        card.recalculate_card_metadata(cover_card: true)
      else
        log.error "Card with #{cover_id} does not exist"
      end
    end
  end
end
require 'active_support/concern'


## USAGE:
## include AnalyticsPeriodOffsetConcern
##
## extract_analytics_period_offsets only: [:index]
## Let's you support api params which behave as follows:
## param :days, Integer, :desc => 'get data for this many days in the past, default: 0, just today', :required => false
## param :period, Integer, :desc => 'one of \'day\', \'week\', \'month\', \'year\' or \'alltime\'. The \'days\' param is ignored if this is passed', :required => false
module AnalyticsPeriodOffsetConcern
  extend ActiveSupport::Concern

  protected

  def set_analytics_period_and_offset
    applicable_offsets = PeriodOffsetCalculator.applicable_offsets(Time.now.utc).with_indifferent_access
    if !params[:period].nil? && !applicable_offsets.has_key?(params[:period])
        render json: {error: "Bad period parameter. Use one of 'day', 'week', 'month', 'year' or 'alltime'"}, 
          status: :bad_request and return
    end

    @_analytics_period = :day
    @_analytics_offset = 0
    if params[:period]
      @_analytics_period = params[:period]
      @_analytics_offset = applicable_offsets[params[:period]]
    else
      number_of_days = params[:days].to_i
      @_analytics_offset = ((applicable_offsets[:day]-number_of_days)..applicable_offsets[:day]).to_a
    end
  end

  def analytics_period
    @_analytics_period
  end

  def analytics_offset
    @_analytics_offset
  end

  class_methods do

    def extract_analytics_period_offsets(options)
      self.before_action(:set_analytics_period_and_offset, options)
    end

  end

end

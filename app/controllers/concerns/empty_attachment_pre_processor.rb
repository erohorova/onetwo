require 'active_support/concern'
#TODO:: Generic version to handle params, nested attributes.
module EmptyAttachmentPreProcessor
  extend ActiveSupport::Concern

  included do
    before_filter :pre_process_images, only: [:create, :update]
  end

  protected
    def pre_process_images
      keys = [:image, :mobile_image]
      keys.each do |image_key|
        if(params[:organization].has_key?(image_key))
          params[:organization][image_key] = params[:organization][image_key].blank? ? nil : params[:organization][image_key]
        end
      end
    end

end

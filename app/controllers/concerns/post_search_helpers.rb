require 'active_support/concern'
module PostSearchHelpers
  extend ActiveSupport::Concern

  included do
    # Postable is either a channel or a group
    # pars can include the following. Also see docs for PostsController#index and Post#retrieve.
    # 0. following => ['true', 'false'] => Filter for posts I'm following
    # 1. pinned => ['true', 'false']
    # 2. author_id => Filter for this user
    # 3. commenter_id => Filter for posts that have comments by this user
    # 4. type => ['Announcement', 'Question'], get posts of this type only
    # 5. tag => Filter for posts with this tag
    # 6. reported => ['true', 'false']. Get flagged posts only
    # 7. context_id => Get posts made in this context (edx subsection)
    # 8. limit => Number of posts to get.
    # 9. offset => Get posts from this offset, for pagination
    # 10. page => Page number. Prefer to use offset
    # 11. search => Search term. Free text
    # 12. sort => ['votes', 'updates', 'responses', 'date'], 'date' by default. Sort criteria
    # 13. order => ['asc', desc'], 'desc' by default. Sort Order
    # 14. last_fetch_time => For autorefresh. Retrieve only posts newer than this time. Valid only with sort criteria: 'date'
    def search_posts_for_postable(postable, pars)
      postable_klass_name = postable.class.to_s
      if postable.kind_of?(Channel)
        filter_params = {:postable_id => postable.id, :postable_type => 'Channel'}
      elsif postable.kind_of?(Group)
        filter_params = {:postable_id => postable.id, :postable_type => 'Group'}
      else
        return []
      end

      filter_params[:user_id] = pars[:author_id]
      filter_params['comments.user_id'] = pars[:commenter_id]

      order = pars['order'] == 'asc' ? 'asc' : 'desc'
      case pars['sort']
      when 'votes'
        sort_params = {votes_count: order.to_sym, created_at: :desc}
      when 'updates'
        sort_params = {last_commented: order.to_sym}
      when 'responses'
        sort_params = {comments_count: order.to_sym, created_at: :desc}
      when 'followed'
        sort_params = {followers_count: order.to_sym, created_at: :desc}
      else
        # make sure pinned posts are sent first in default view
        sort_params = {pinned: :desc, created_at: order.to_sym}
        # if latest seen id provided, send only newer than that
        if pars[:last_fetch_time]
          filter_params[:created_at] = {gt: pars[:last_fetch_time]}
        end
      end

      if pars[:pinned]
        filter_params[:pinned] = true
      end

      if pars[:tag]
        filter_params[:tag_names] = pars[:tag]
      end

      if pars[:reported]
        filter_params[:reported] = true
      end

      if pars['type']
        filter_params[:type] = pars['type']
      end

      if pars['following']
        filter_params[:follower_ids] = current_user.id
      end

      filter_params[:context_id] = pars[:context_id]

      # hidden is a derived parameter that ensures that
      # hidden posts aren't sent out in the json response
      unless filter_params[:reported]
        filter_params[:hidden] = false
      end

      search_query = pars['search'] || "*"

      limit = pars[:limit].nil? ? Post.get_postable_default_page_limit(postable_klass_name) : pars[:limit].to_i

      offset = pars[:offset].nil? ? (pars[:page].nil? ? 0 : (pars[:page].to_i * limit)) : pars[:offset].to_i

      _hashtags = search_query.scan(/#(\S+)/).flatten
      if _hashtags.any?
        filter_params.merge!(tag_names: _hashtags.join(' '))
        _misspelling = 0
      else
        _misspelling = nil
      end

      Post.retrieve(filter_params: filter_params,
                    sort_params: sort_params,
                    misspelling: _misspelling,
                    search_query: search_query,
                    limit: limit,
                    offset: offset)
    end
  end
end

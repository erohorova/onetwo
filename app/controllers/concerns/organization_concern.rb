module OrganizationConcern
  extend ActiveSupport::Concern

  def prepare_organization_params(attrs)
    update_paperclip_attributes attrs
    attrs
  end

  private

  def update_paperclip_attributes(attrs)
    paperclip_attributes = Organization.paperclip_definitions.keys
    paperclip_attributes.each do |attr|
      if is_filestack_url?(attrs[attr])
        fs_url = Filestack::Security.read_filestack_url(attrs[attr])
        attrs[attr] = Filestack::Security.new.signed_url(fs_url)
      end
    end
    attrs
  end

  def is_filestack_url?(image_url)
    return false if image_url.blank?
    image_url.include?("cdn.filestackcontent.com") || image_url.include?("#{current_org.host}/uploads/")
  end
end

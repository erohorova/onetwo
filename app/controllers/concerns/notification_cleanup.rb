require 'active_support/concern'

module NotificationCleanup
  extend ActiveSupport::Concern

  included do
  	after_destroy :remove_notifications
  	
	def remove_notifications
	  #job for removing corresponding notifications
	  NotificationCleanUpJob.perform_later(id: id, type: self.class.name)
	end
  end
end

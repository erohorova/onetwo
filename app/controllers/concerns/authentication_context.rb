module AuthenticationContext
  extend ActiveSupport::Concern

  included do
    append_before_action :set_authentication_method #make sure this the last filter that runs
  end

  private

  def set_authentication_method
    if user_signed_in? #force load the @current_user from the session
      @current_user.authentication_method = user_session[:authentication_method] || :default
    end
  end

  def authentication_method
    return user_session.nil? ? nil : user_session[:authentication_method]
  end
end

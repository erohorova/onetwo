require 'active_support/concern'
module CardSearchHelpers
  extend ActiveSupport::Concern

  included do
    # set user actions on cards
    # bookmark, upvote, like
    def set_user_actions(cards)
      unless cards.empty?
        @user_actions = CardsUser.get_user_actions(
            card_ids: cards.map{|c| c['id'].to_i},
            user_id: current_user.id)
      end
    end

    #set card comments
    def set_card_comments(cards)
      unless cards.empty?
        cards_with_less_than_10_comments = cards.select{|c| c['comments_count'] <= 10}
        cards_with_more_than_10_comments = cards.select{|c| c['comments_count'] > 10}

        @comments_hash = Comment.includes(:file_resources, :user, :resource).where(commentable_type: 'Card', commentable_id: cards_with_less_than_10_comments.map{|c| c['id'].to_i}).group_by(&:commentable_id)

        cards_with_more_than_10_comments.each do |card|
          @comments_hash[card['id'].to_i] = Comment.includes(:file_resources, :user, :resource).where(commentable_type: 'Card', commentable_id: cards_with_more_than_10_comments.map{|c| c['id'].to_i}).limit(10).order(id: :desc).reverse
        end

        comment_ids = @comments_hash.values.flatten.map(&:id)
        @votes_by_user = Vote.get_votes_by_user(current_user,
                                                votable_type: 'Comment',
                                                votable_ids: comment_ids)
      end
    end

    # set pack counts for any pack covers, NOT including cover
    def set_pack_counts(cards)
      pack_covers = cards.select{|c| c['card_type'] == 'pack'}
      pack_counts = CardPackRelation.pack_counts(cover_ids: pack_covers.map{|c| c['id']})
      pack_covers.each {|cover| cover['pack_count'] = (pack_counts[cover['id']] || 1) - 1 }
    end

    def multi_search(params, filter_params, users_limit: 9)
      @offset = params[:offset].to_i # yields 0 if not supplied
      limit = params[:limit].to_i
      @limit = limit.zero? ? 15 : limit

      multi_search = Search::MultiSearch.new
      multi_search.add('cards', Search::CardSearch.new.search_body(
                                  params[:q],
                                  current_org,
                                  filter_params: filter_params.except(:creator_must),
                                  viewer: current_user,
                                  offset: @offset,
                                  limit: @limit,
                                ))

      @users = []
      @total_users = 0

      @query = params[:q].strip

      if (@offset == 0) # only search for users on first page query
        if user_signed_in?
          SearchQuery.create(user_id: current_user.id, query: @query, organization_id: current_org.id)
        end
        multi_search.add('users', Search::UserSearch.new.search_body(q: params[:q], viewer: current_user, offset: @offset, limit: users_limit))
        multi_search.add('video_streams', Search::VideoStreamSearch.new.search_body(q: params[:q], viewer: current_user, offset: @offset, limit: users_limit, filter_params: filter_params.slice(:creator_must)))
        multi_search.add('channels', Search::ChannelSearch.new.search_body(q: params[:q], viewer: current_user, offset: @offset, limit: users_limit))
      end

      search_results = multi_search.execute
      card_response = search_results['cards']
      @cards = card_response.results
      @total_cards = card_response.total
      @status = card_response.status
      @next_offset = (@offset + @limit) < @total_cards ? (@offset + @limit) : nil

      users_response = search_results['users']
      if users_response
        @users = users_response.results
        @total_users = users_response.total
        @has_more_users = @total_users > users_limit
      end

      video_streams_response = search_results['video_streams']
      if video_streams_response
        @video_streams = video_streams_response.results
        @total_video_streams = video_streams_response.total
        @has_more_video_streams = @total_video_streams > users_limit
      end

      channels_response = search_results['channels']
      if channels_response
        @channels = channels_response.results

        # Need to fetch image urls and other information. Loading activerecord objects
        @channels = @current_org.channels.where(id: @channels.map(&:id))
        following_channel_ids = current_user.follows.where(followable_type: 'Channel', followable_id: @channels.map(&:id)).pluck(:followable_id)
        @following  = Hash[@channels.map {|c| [c.id, following_channel_ids.include?(c.id)]}]

        @total_channels = channels_response.total
        @has_more_channels = @total_channels > users_limit
      end
    end

    # Insert first 3 cards
    def insert_pack_cards(cards)
      return if cards.empty?

      cards.each do |card|
        card['pack_data'] = card['pack_cards'].first(3) if card['pack_cards'] && !card['pack_cards'].blank?
        card.delete 'pack_cards' #clean up
      end
    end

    def set_channel_data(cards)
      return if cards.blank?
      collect_channel_ids = cards.collect {|c| c['channel_ids']}.flatten.compact.uniq
      channels_hash = Channel.where(id: collect_channel_ids).select(:id, :label).index_by(&:id)
      cards.each do |card|
        card['channels'] ||= []
        card['channel_ids'].each {|ch_id| card['channels'] << channels_hash[ch_id]}
      end
    end

    def fetch_cards_in_current_org(query, viewer: nil, filter_params: {card_type: Card::EXPOSED_TYPES}, limit: 15, offset: 0)
      @response = Search::CardSearch.new.search(
          query,
          current_org,
          viewer: viewer,
          filter_params: filter_params,
          offset: offset,
          limit: limit,
          sort: :created,
      )

      @cards = @response.results
    end

  end
end

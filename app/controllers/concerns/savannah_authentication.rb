module SavannahAuthentication
  def authenticate_from_savannah!
    @current_org, @jwt_payload, @jwt_header = PartnerAuthentication.get_org(request: request, params: params)
    return head :unauthorized unless @current_org
  end

  # Set authentication_method so that Savannah recognises the origin of user
  # this must include authentication origin (`auth_provider`) as well which is picked from `user_session`
  # Two areas that lands onto Savannah course:
  # 1. Promoted courses (PromotionsController)
  # 2. Deeplink URLs (CoursesController)
  def set_authentication_method
    @query_values = {}

    if user_session.present? && user_session[:authentication_method].present?
      @query_values['auth_provider'] = user_session[:authentication_method]
    end

    @query_values
  end
end

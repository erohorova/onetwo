require 'active_support/concern'
module UserLeaderboardV2Concern
  extend ActiveSupport::Concern

  included do

    api :GET, "api/v2/organizations/leaderboard_v2",
              "Get User Leaderboard for an organization"

    param :limit,
          Integer,
          desc: 'limit for pagination; default 10',
          required: false

    param :offset,
          Integer,
          desc: 'offset for pagination; default 0',
          required: false

    param :from_date,
          String,
          desc: "Starting (earliest) date, mm/dd/yyyy format.",
          required: false

    param :to_date,
          String,
          desc: "Ending (latest) date. mm/dd/yyyy format.",
          required: false

    param :active,
          String,
          desc: "Boolean, if given will filter active or inactive users",
          required: false

    param :group_ids,
          String,
          desc: "Array of group ids. Show only users who are members of selected groups",
          required: false

    param :roles,
          String,
          desc: "Array of role names. Show only   users with selected roles",
          required: false

    param :expertise_names,
          String,
          desc: "Array of expertise names. Show only users with selected expertises",
          required: false

    param :show_my_result,
          String,
          desc: "Boolean. If true, include key for current user results.",
          required: false

    param :order,
          String,
          desc: "Either \"asc\" or \"desc\", default: \"desc\"",
          required: false

    param :q,
          String,
          desc: "Show only users that match the search term",
          required: false

    param :order_attr,
          String,
          desc: "Attribute to sort on. " +
                "One of smartbites_consumed, smartbites_created, " +
                "smartbites_comments_count, smartbites_liked, " +
                "time_spent, smartbites_completed, and smartbites_score. " +
                "Default is smartbites score."

    # =========================================
    # The entry point from the controller.
    # =========================================
    def show_leaderboard
      leaderboard_results = UserLeaderboardV2Service.get_leaderboard_results(
        current_org:  current_org,
        current_user: current_user,
        params:       params,
        offset:       offset,
        limit:        limit
      )
      render status: 200, json: leaderboard_results
    rescue UserLeaderboardV2Service::ParameterError => e
      render_unprocessable_entity(e.message)
    end

  end
end

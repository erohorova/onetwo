module CoursesConcern
  extend ActiveSupport::Concern

  def scrape_courses
    #expecting all fields in params[:fields] && return false if any field is blank
    if params[:fields] && params[:fields].all? {|k,v| v.present?} && params[:provider]
      external_uid =  case params[:provider]
      when "Coursera"
        params[:fields][:coursera_public_page_id]
      when "Udemy"
        params[:fields][:udemy_public_page_id]
      when "Future Learn"
        params[:fields][:future_learn_profile_id]
      when "Skill Share"
        params[:fields][:skill_share_user_name]
      when "Plural Sight"
        params[:fields][:plural_sight_user_name]
      when "Tree House"
        params[:fields][:tree_house_user_name]
      when "Code School"
        params[:fields][:code_school_user_name]
      when "Alison"
        params[:fields][:alison_user_id]
      when "Codecademy"
        params[:fields][:codecademy_username]
      end

      CoursesScraperJob.perform_later(current_user.id, external_uid, params[:provider])
    end
  end

end

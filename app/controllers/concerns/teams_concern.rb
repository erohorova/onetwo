module TeamsConcern
  extend ActiveSupport::Concern

  def include_assigned_users
    return if @cards.empty?
    card_ids = @cards.map {|c| c['id']}
    assignees = @team.assignments.select('id, assignable_id, user_id').where(assignable_id: card_ids, assignable_type: 'Card').group_by(&:assignable_id)
    @cards.each do |card|
      card["assignee_ids"] = assignees[card['id']] ? assignees[card['id']].map(&:user_id) : []
    end
  end

  def fetch_assigned_groups
    @assigned_team_ids = @teams.joins(:assignments).where('assignments.assignable_id = ? AND assignable_type = ?', @assignable.id, @assignable.class.name).pluck(:id)
  end

  def fetch_unassigned_members_count(teams: )
    @members_count = {}
    teams.each {|team| @members_count[team.id] = team.fetch_unassigned_members_for(assignable: @assignable).count}
  end

  def set_team
    @team = Team.where(organization: current_org).friendly.find(params[:id])
    user_access = @team.is_user?(current_user) || current_org.has_admin_access?(current_user)
    accessibility = !@team.is_private || user_access
    redirect_to not_found_path unless accessibility
  end

  def set_team_user
    @user = @team.users.find(params[:user_id])
    redirect_to not_found_path unless @user
  end

  def authenticate_admin!
    super unless @team.is_admin?(current_user)
  end

  def load_assignable(pars)
    @assignable = nil
    return if !pars[:assign_id].present? || !pars[:assign_type].present?

    assignable_type = pars[:assign_type]
    assignable_id = pars[:assign_id]

    @assignable = if params[:assign_type].downcase == 'card'
      load_card_from_compound_id(params[:assign_id])
    else
      assignable_type.classify.constantize.find assignable_id
    end

    @assignable = @assignable.card if pars[:assign_type] == "VideoStream"
  end
end

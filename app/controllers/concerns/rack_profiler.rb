require 'active_support/concern'

module RackProfiler
  ALLOWED_EMAILS = ["rishav.rastogi@gmail.com", "rishav@edcast.com", "bharath@edcast.com", "kapil@edcast.com", "student3@edcast.com", "ramin@edcast.com"]
  extend ActiveSupport::Concern
  #TODO Make changes once the RBAC stuff is built
  included do
    before_action do
      if current_user && !RackProfiler::ALLOWED_EMAILS.index(current_user.email).nil?
        Rack::MiniProfiler.authorize_request
      end
    end
  end

end

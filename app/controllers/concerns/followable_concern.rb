module FollowableConcern
  include ActiveSupport::Concern

  # Load whether given user follows channel/user in channels/users list
  # Stores hash of followable_type: {#{followable_type}_id and boolean value} in an instance variable called following
  # Ex. {'User' => {1 => true}, "Channel" => {2 => true}}
  def load_is_following_for(followable_ids, followable_type, user)
    return {} if user.nil? || followable_ids.blank?
    followable_ids_for_user = user.follows.where(followable_type: followable_type, followable_id: followable_ids).pluck(:followable_id)

    #getting channel ids for channels followed via teams
    if followable_type == 'Channel'
      channel_ids_followed_via_team = user.channel_ids_followed_via_teams
      followable_ids_for_user << channel_ids_followed_via_team unless channel_ids_followed_via_team.empty?
    end

    @following ||= {}
    @following[followable_type] = {} unless @following.has_key?(followable_type)
    @following[followable_type].merge!(
      Hash[
        followable_ids.map {|followable_id|
          [followable_id, followable_ids_for_user.include?(followable_id)]
        }
      ]
    )
  end
end

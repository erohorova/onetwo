# frozen_string_literal: true
require 'json-schema'
module WorkflowConcern
  extend ActiveSupport::Concern

  WORKFLOW_TYPES = %w[groups users_groups].freeze

  # TODO: fix duplicates `faraday` reqs
  def response_mapper(entries)
    return [[], []] if entries.empty?
    # standard response keys from #profile developer/v5
    columns_keys = profile_static_fields
    # get custom_fields keys example based on first profile
    @current_org.custom_fields.each do |cf|
      # col_names = columns_keys.map { |i| i[:id] }
      abbr = 'custom_' + cf.abbreviation.to_s
      # abbr += '_' if cf.abbreviation.in?(col_names)
      columns_keys.push(
        { id: abbr, type: 'string', label: cf.display_name }
      )
    end
    rows = []
    # populate rows values, keys eq column names and/or custom_field attrs
    entries.each do |en|
      user = en.user
      next unless user
      c_f = user.assigned_custom_fields.includes(:custom_field)
      row_values = {
        id: user.id.to_s,
        profile_id: en.id,
        profile_external_id: en.external_id,
        profile_uid: en.uid,
        user_email: user.email,
        user_first_name: user.first_name,
        user_last_name: user.last_name,
        user_status: user.status,
        user_created_at: user.created_at.to_datetime.strftime('%Q').to_i
      }
      c_f.each do |cf|
        custom_field = cf.custom_field
        value = cf.value.presence
        next unless value
        key = 'custom_' + custom_field.abbreviation
        row_values[key] = value
      end
      rows.push(row_values)
    end
    [columns_keys, rows]
  end

  def columns_mapper(name, org = @current_org)
    const = profile_static_fields
    return [] unless const
    case name
    when 'profile'
      org.custom_fields.each do |cf|
        abbr = 'custom_' + cf.abbreviation.to_s
        const.push(
          { id: abbr, type: 'string', label: cf.display_name }
        )
      end
      const
    else
      []
    end
  end

  def profile_static_fields
    [
      {id: 'id', type: 'string', label: 'User ID'},
      {id: 'profile_id', type: 'number', label: 'User Profile ID'},
      {id: 'profile_external_id', type: 'string', label: 'Profile External ID'},
      {id: 'profile_uid', type: 'string', label: 'Profile UID'},
      {id: 'user_email', type: 'string', label: 'User Email'},
      {id: 'user_first_name', type: 'string', label: 'User First Name'},
      {id: 'user_last_name', type: 'string', label: 'User Last Name'},
      {id: 'user_status', type: 'string', label: 'User Status'},
      {id: 'user_created_at', type: 'date', label: 'User Created At'}
    ]
  end

  def validate_schema(params, type)
    schema = case type
    when 'handle_patches'
      File.read(
        Rails.root.join('lib', 'json_schemas', 'workflows', "to_edcast_patch.json")
      )
    when 'dynamic_selections'
      File.read(
        Rails.root.join('lib', 'json_schemas', 'workflows', "#{type}_payload.json")
      )
    else
      nil
    end
    JSON::Validator.fully_validate(schema, params)
  end

  def dynamic_data_formatter(params)
    return unless params.key?(:action)
    data = format_filter_titles(params)
    return unless data
    case data[:action]
    when 'create'
      validation = validate_schema(data, 'dynamic_selections')
      return { errors: validation } unless validation.empty?
      orig_title = data[:payload][:groupTitle]
      {
        payload: data[:payload],
        additions: data.except(:payload).merge({original_title: orig_title})
      }
    when 'delete'
      fields = data[:payload]
      ext_group = DynamicGroupRevision.find_by(
        group_id: fields[:groupId],
        organization_id: current_org.id
      )
      return { errors: 'Group does not exists' } unless ext_group
      {
        payload: {
          external_id: ext_group.external_id
        },
        additions: data.except(:payload).merge({dynamic_group_revision_id: ext_group.id})
      }
    else
      nil
    end
  end

  def dynamic_group_revision(res:, addition:, status:, payload:)
    # TODO: accurate status processing, finalize the requirements
    unless status.to_i.between?(200, 299)
      # destroy DynamicGroup if not exists on Workflow side
      if res['message'] == 'not found' && status.to_i == 404
        current_org.dynamic_group_revisions.find_by(
          external_id: payload[:external_id]
        )&.destroy
        return { message: 'DynamicSelectionGroup removed', status: 'ok' }
      end
      return { message: 'Service failed', status: 'unprocessable_entity' }
    end
    case addition[:action]
    when 'create'
      info = addition[:additionInfo]
      fields = {
          title: addition[:original_title],
          uid: res['dynamicId'],
          external_id: res['id'],
          user_id: current_user.id,
          organization_id: current_org.id,
          item_id: info[:objectId],
          item_type: info[:objectType],
          action: info[:action],
          message: info[:message]
      }
      revision = DynamicGroupRevision.new(fields)
      if revision.valid?
        revision.save
        { message: 'DynamicGroup created', status: 'ok' }
      else
        message = revision.full_errors
        {
          message: message,
          status: 'unprocessable_entity'
        }
      end
    when 'delete'
      {
        message: 'Removing of the DynamicGroup in progress',
        status: 'ok'
      }
    end
  end

  def send_dynamic_selection(payload: nil, type:)
    url = 'api/dynamic-selections'
    url = "api/dynamic-selections/#{payload[:external_id]}/delete" if type == 'delete'
    info = []
    conn = Faraday.new(:url => Settings.workflows.host_url) do |faraday|
      faraday.adapter Faraday.default_adapter
      faraday.options[:open_timeout] = 2
      faraday.options[:timeout] = 15
      faraday.headers['x-api-token'] = Settings.workflows.edcast_master_token || ''
      faraday.headers['org-name'] = current_org.host_name
      faraday.headers['Content-Type'] = 'application/json'
    end

    begin
      res = conn.post do |req|
        req.url url
        req.body = payload.to_json if type == 'create'
      end
      info.push(JSON.parse(res.body), res.status.to_s)
    rescue Faraday::ConnectionFailed, Faraday::TimeoutError => e
      log.warn "EdcWorkflow error: #{e.message}. Payload: #{payload}"
      info = [{}, 'TimeoutError']
    end

    info
  end

  #TODO: retry service implementation if server not available
  def server_available?
    return false unless Settings.workflows.host_url
    begin
      conn = Faraday.new(:url => Settings.workflows.host_url)
      res = conn.get do |req|
        req.url 'status'
      end
      return true if res.status == 200
    rescue Faraday::ConnectionFailed, Faraday::TimeoutError => e
      log.warn "EdcWorkflow status check fail: #{e.message}"
      return false
    end
  end

  def format_filter_titles(params)
    return params if params[:action] == 'delete'
    keys_map = %i[payload profileFilterOptions filterRules]
    filters_exist = extract_specific_data(
      keys_map,
      params
    ).present?
    return unless filters_exist
    default = User::DEFAULT_FIELDS
    filter_rules = format_name_field(params.dig(*keys_map))
    filter_rules.map! do |rule|
      rule[:colId] = if rule[:colId].in?(default)
        "user_#{rule[:colId].underscorize}"
      else
        "custom_#{rule[:colId].underscorize}"
      end
      rule
    end
    params
  end

  def format_name_field(filter_rules)
    name_obj = filter_rules.detect { |el| el['colId'] == 'name' }
    return filter_rules unless name_obj
    full_name = name_obj['targetValue'].split(' ')
    valid_filter_criteria = []
    full_name.each_with_index do |val, idx|
      valid_filter_criteria.push({
        targetValue: val,
        condition: 'equals',
        colId: idx.zero? ? 'first_name' : 'last_name'
      }.with_indifferent_access)
    end
    filter_rules.delete(name_obj)
    filter_rules.push(*valid_filter_criteria)
  end

  def extract_specific_data(keys_map, params)
    return unless keys_map.present? && params.is_a?(Hash)
    keys_map.reduce(params) do |memo, key|
      memo.try(:fetch, key, nil)
    end
  end

   module_function :validate_schema
end

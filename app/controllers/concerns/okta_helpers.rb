require 'active_support/concern'
module OktaHelpers
  extend ActiveSupport::Concern

  protected
  def federated_connection(user_params)
    user_data    = { 'password' => user_params['password'] }
    redirect_uri = is_mobile_browser? ?
      native_scheme_redirect_url(native_scheme: current_org.okta_native_redirect) :
        root_url
    resource.provision_and_sign_in(args: user_data, redirect_url: redirect_uri)
  end
end

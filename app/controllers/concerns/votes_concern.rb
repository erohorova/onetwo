module VotesConcern
  extend ActiveSupport::Concern
  include NativeAppDetection
  include DetectPlatform

  def create_vote
    @vote = Vote.new(voter: current_user, votable: @votable)
    @vote.save
  end

  def delete_vote
    @vote = Vote.find_by(votable: @votable, voter: current_user)
    return unless @vote
    # We do not want to change the hard delete feature by addition of paranoia
    # in vote. Hence, used destroy_without_paranoia
    @vote.destroy_without_paranoia
  end

  protected
  def votable_params
    params.require(:vote).permit(:content_type, :content_id)
  end

  def track_vote(action: "like")
    votable ||= @vote.votable
    track_opts = {
        user_id: current_user.id,
        object: votable.class.name.underscore,
        object_id: votable.id,
        action: action,
        platform: get_platform,
        device_id: cookies[:_d],
        organization_id: current_user.organization_id,
        referer: params[:referer].presence || request.referer
    }
    Tracking::track_act track_opts
  end

  def find_votable
    @votable = if votable_params[:content_type] == 'Card'
      load_card_from_compound_id(votable_params[:content_id])
    else
      votable_params[:content_type].constantize.find(votable_params[:content_id])
    end
  end
end

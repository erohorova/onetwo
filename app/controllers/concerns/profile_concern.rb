module ProfileConcern
  extend ActiveSupport::Concern

  EXCEPTION_RETURN_CLASSES = %w[User Profile UserCustomField]
  REQUIRED_PARAMS = %w[external_id email first_name last_name]

  def initialize_profile(user, external_profile_params)
    external_profile = user.external_profile || user.create_external_profile
    save_or_skip_attributes(external_profile, attributes: external_profile_params)
  end

  def initialize_user(user_params, opts: {})
    users = current_org.users
    user = users.find_by(email: user_params['email']) || users.new

    email_regexp = %r{
      ^([a-zA-Z0-9]+[\+|\-|\.|\_]?)+[a-zA-Z0-9]
      @(([a-zA-Z0-9]+[\+|\-|\.|\_]?[a-zA-Z0-9]+)+
      \.[a-zA-Z0-9]{2,})$
    }x

    if user_params['email'].match(email_regexp).nil?
      @messages.push('Email address is invalid')
      return [user, true]
    end

    save_or_skip_attributes(user, attributes: user_params, opts: opts)
  end

  def initialize_user_profile(user, user_profile_params)
    user_profile = user.profile || user.build_profile
    save_or_skip_attributes(user_profile, attributes: user_profile_params)
  end

  def initialize_roles(user, roles: [])
    valid_roles = get_valid_roles(roles: roles)
    return if valid_roles.blank?
    add_or_remove_role(roles: valid_roles, action: 'add_user', user: user)
  end

  def set_params(params)
    user_key = [:first_name, :last_name, :email, :status, :avatar]
    user_profile_key = [:expert_topics, :learning_topics, :language, :time_zone, :job_title]
    skip_key = [:external_id, :uid, :roles, :send_email_notification]

    user_params = params.permit(*user_key).presence
    user_profile_params = params.permit(*user_profile_key).presence
    except_params = user_key.push(*skip_key).push(*user_profile_key)
    custom_fields = params.permit!.except(*except_params).presence

    [user_params, custom_fields, user_profile_params]
  end

  def update_value_custom_fields(user, custom_fields_params)
    return if custom_fields_params.blank? || user.blank?
    custom_fields_params.each do |key, value|
      custom_field = current_org.custom_fields.find_by(abbreviation: key)
      if custom_field.blank?
       @messages.push(key + " key is missing in custom fields")
       next
      end
      user_custom_fields = user.assigned_custom_fields
      user_custom_field = user_custom_fields.find_or_initialize_by(custom_field_id: custom_field.id)
      user_custom_field, skip = save_or_skip_attributes(user_custom_field, attributes: { value: value })
      next if skip
    end
  end

  # checking role names on the organization
  def get_valid_roles(roles: [])
    org_role = current_org.roles.pluck(:name)
    roles = [roles] unless roles.is_a?(Array)
    invalid_roles = roles - org_role
    @messages.push("Invalid role names: #{invalid_roles}") if invalid_roles.present?
    if action_name == 'create'
      member_role = current_org.roles.find_by_default_name('member').name
      roles.push(member_role) if member_role
    end
    org_role & roles
  end

  def add_or_remove_role(roles:, action:, user:)
    roles = current_org.roles.where(name: roles)
    roles.each { |role| role.send(action, user) } if roles.present?
  end

  def batch_initialize(profiles: [])
    profiles.each_with_index do |profile, index|
      @all_messages[index] = @messages = []

      missing_params = ProfileConcern::REQUIRED_PARAMS.any? { |key| profile[key].blank? }
      if missing_params
        @messages.push('Missing required params')
        next
      end

      user_params, custom_fields_params, user_profile_params = set_params(profile)
      external_profile_params = profile.permit(:external_id, :uid).presence
      opts = {}

      notification = profile[:send_email_notification]&.to_bool
      opts[:skip_confirmation] = true if notification.is_a?(FalseClass)

      user, skip = initialize_user(user_params, opts: opts)
      next if skip
      external_profile, skip = initialize_profile(user, external_profile_params)
      next if skip
      initialize_roles(user, roles: profile['roles'])
      initialize_user_profile(user, user_profile_params) if user_profile_params.present?
      update_value_custom_fields(user, custom_fields_params) if custom_fields_params.present?
      @created_profiles.push(external_profile.reload)
    end
  end

  def save_or_skip_attributes(object, attributes: {}, skip: false, opts: {})
    if object.is_a?(User) && object.new_record?
      attributes = attributes.merge(password: User.random_password)
    end
    object.attributes = attributes
    object.skip_confirmation! if object.is_a?(User) && opts[:skip_confirmation]
    unless object.save
      @messages.push(object.full_errors)
      skip = true if ProfileConcern::EXCEPTION_RETURN_CLASSES.include? object.model_name.name
    end
    [object, skip]
  end
end



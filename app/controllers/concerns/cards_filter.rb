require 'active_support/concern'

module CardsFilter
  extend ActiveSupport::Concern

  def set_filter_by_language
    @filter_by_language = current_user.language.present? &&
      !!params[:filter_by_language]&.to_bool
  end

  def set_last_access_opts
    @last_access_at = params[:last_access_at]&.to_i
    @options = @last_access_at.present? ? { last_access_at: @last_access_at } : {}
  end
end

# frozen_string_literal: true

# will be changed in future accordingly to provided requirements.
# implemented for (cards, comments and resources) -> create/update actions

module EmbargoChecker
  def embargo_content_check
    current_org_embargo # take org embargos
    data = params
    # return if data blank or there is no any embargo entries in organization
    return if data.blank? || @embargo.values.all?(&:empty?)
    forbid_key = values_to_check(data)
    return unless forbid_key
    message = "Forbidden content in the #{forbid_key} field"
    render json: {message: message}, status: :unprocessable_entity
  end

  private

  def parse_to_regex(embargos, word)
    # handle wildcard regexp to initialize case of matching
    # return obj with keys: [match, full]
    # match: indicates if match occur
    # full: indicates if matching was a full
    #       handle diff *.com vs http://exmaple.com,
    #       where the full match will have more weight
    default = {match: false, full: nil}
    return default if [embargos, word].any?(&:blank?)
    embargos.each do |embargo|
      # rid off URI prefixes or www and `/` ending
      embargo_n, word_n = [embargo, word].map! do |i|
        i.gsub(%r{(^(http|https):\/{2}|w{3}.)|(\/)}, '')
      end
      reg = Regexp.new "^#{Regexp.escape(embargo_n).gsub('\*', '.*?')}$", Regexp::IGNORECASE
      return {match: true, full: !embargo_n.include?('*')} if !!(word_n =~ reg)
    end
    default
  end

  def is_embargo_content?(value)
    return unless value.present? && value.is_a?(String)
    forbid = false
    # split value to words -> check firstly on unapproved words,
    # then determining if the word could contain a link ->
    # check on embargo urls if a link
    value.scan(/\S+/) do |word|
      word_match = parse_to_regex(@embargo[:unapproved_words], word.gsub(/[[:punct:]]/,''))
      # words could be only fully matched
      # most weighted case
      if word_match[:match]
        forbid = true
        break
      end

      url = get_url_from_markdown(word) || UrlMetadataExtractor.return_url(word)
      if url
        host_url = sanitize_url(url)
        #parse_to_regex returns {key: boolean, key: boolean}
        forbid_url = parse_to_regex(@embargo[:unapproved_sites], host_url)
        # values.all? check to full match
        # {match: true, full: true}
        # true points to most weighted case
        if forbid_url.values.all?
          # forbid: true if full match detected in unapproved list
          forbid = true
          break
        end
        approved_url = parse_to_regex(@embargo[:approved_sites], host_url)
        # if for unapproved we have not full match
        # checking approved category
        # go next if approved got full match
        # case when *.com - forbidden
        #   but http://exmple.com is approved
        if forbid_url[:match] && !forbid_url[:full]
          next if approved_url.values.all?
          forbid = true
          break
        end

        # if approved list not empty and we have no matches
        # forbid: true
        if @embargo[:approved_sites].present?
          unless approved_url[:match]
            forbid = true
            break
          end
        end
      end
    end
    forbid
  end

  def values_to_check(hash)
    # returns the key for which value matched embargo content
    # take first key from params
    key = hash.first[0]
    case key.to_s
    when 'card'
      keys = %i[message title options description topics]
      forbid_content_in_key(keys, hash[key])
    when 'comment'
      keys = %i[message]
      forbid_content_in_key(keys, hash[key])
    when 'resource'
      keys = %i[link]
      forbid_content_in_key(keys, hash[key])
    end
  end

  def forbid_content_in_key(keys, params)
    keys.find do |key|
      val = params[key]
      next unless val
      value_to_check = collect_nested_values(val, key)
      is_embargo_content?(value_to_check)
    end
  end

  def collect_nested_values(hash, key)
    case key
    when :options
      # collecting options params[:label] for quiz cards
      hash.map { |el| el[:label] }.join(' ')
    when :topics
      hash.join(' ')
    else
      hash
    end
  end

  def current_org_embargo
    @embargo = Embargo.get_org_embargos_values(current_org)
  end

  def get_url_from_markdown(markdown_url)
    markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true)
    html_value = Nokogiri::HTML(markdown.render(markdown_url))
    html_value.at_css("a").present? ? html_value.at_css("a")[:href] : nil
  end

  # http://www.youtube.com/adfadsf -> www.youtube.com
  def sanitize_url(url)
    URI.parse(url).host
  end
end

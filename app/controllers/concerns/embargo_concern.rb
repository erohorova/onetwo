module EmbargoConcern
  extend ActiveSupport::Concern
  include SortOrderExtractor

  VALID_ORDER_ATTRS = %w[created_at value].freeze

  def get_embargo_for_org
    @embargoes = if params[:category] && Embargo::VALID_TYPES.include?(params[:category])
      params[:category].in?(Embargo::SITES) ? sites : words
    else
      org_embargo
    end

    if params[:q]
      @embargoes = @embargoes.where("embargos.value like ?", "%#{params[:q]}%")
    end

    order_attr, order = extract_sort_order(valid_order_attrs: VALID_ORDER_ATTRS, default_order_attr: "created_at")

    @embargoes = @embargoes.order("#{order_attr} #{order}")
    @total = @embargoes.count
    @embargoes = @embargoes.limit(limit).offset(offset).includes(:user)
  end

  def org_embargo
    current_org.embargos
  end

  def sites
    if params[:category] == 'approved_sites'
      org_embargo.approved_sites
    else
      org_embargo.unapproved_sites
    end
  end

  def words
    org_embargo.unapproved_words
  end

  protected

  def update_content
    @embargo = find_embargo
    @embargo.update_attributes(embargo_params)
  end

  def find_embargo
    org_embargo.find_by(id: params[:id])
  end

end

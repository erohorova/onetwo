module ManageSenderConcern
  extend ActiveSupport::Concern

  def mailer_config_params
    params.require(:mailer_config).permit(
      :from_name, :is_active, :from_address
    )
  end

  def set_email_sender
    org = current_user.organization
    @email_sender = org.mailer_configs.where(id: params[:id], from_admin: true).first
    return render_not_found('Email sender does not exist') unless @email_sender
  end

  def check_user_permissions
    is_admin = current_user.authorize?(Permissions::ADMIN_ONLY)
    render_unauthorized('Permission denied') unless is_admin
  end

  def get_sender_mailer_config(token)
    payload, jwt_header = JWT.decode(token, Settings.features.integrations.secret)
    mailer_config = MailerConfig.where(id: payload['id'], from_address: payload['from_address']).first
  end
end
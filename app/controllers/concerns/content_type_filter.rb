require 'active_support/concern'
module ContentTypeFilter
  extend ActiveSupport::Concern

  included do
    def get_content_type_filters ct
      default = {card_type: Card::EXPOSED_TYPES}
      case ct
      when 'collections'
        return {card_type: ['pack']}
      when 'videos'
        return default.merge({card_template: 'video'})
      when 'polls'
        return {card_type: ['poll']}
      else
        return default
      end
    end

    def get_content_type_filters_for_channel ct
      default = {card_type: Card::EXPOSED_TYPES}
      case ct
      when 'collections'
        return {card_type: 'pack'}
      when 'videos'
        return default.merge({card_template: 'video'})
      when 'polls'
        return {card_type: ['poll']}
      when 'text'
        {card_type: ['media']}.merge({card_template: ct})
      when 'article'
        {card_type: ['media']}.merge({card_template: 'link'})
      else
        return default
      end
    end

  end
end

require 'active_support/concern'


## USAGE:
## include SortOrderExtractor
module SortOrderExtractor
  extend ActiveSupport::Concern

  protected

  # Filters params to return a valid order attribute if passed in.
  # Default order is "desc", unless params[:order] == "asc"
  # Adds support for "order" and "order_attr" in params
  def extract_sort_order(
    valid_order_attrs:,
    default_order_attr:,
    order: params[:order],
    order_attr: params[:order_attr]
  )
    order = order&.downcase == "asc" ? "asc" : "desc"
    order_attr = valid_order_attrs.include?(order_attr) ? order_attr : default_order_attr
    [order_attr, order]
  end
end

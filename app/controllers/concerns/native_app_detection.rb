require 'active_support/concern'


#EdCast/1.0 CFNetwork/711.1.12 Darwin/14.0.0    GET request
#EdCast/1.0 (iPhone; iOS 8.1.1; Scale/2.00)     non-GET request
#EdCast/1.0 (iPhone Simulator; iOS 8.1; Scale/2.00) non-GET request
#Edcast-Android #all
module NativeAppDetection
  extend ActiveSupport::Concern

  included do

    helper_method :is_native_app?

    def is_native_app?
      request.user_agent.blank? ? false : request.user_agent.downcase.starts_with?('edcast')
    end

    def native_app_form_factor
      #TODO: add logic for detecting tablet
      'phone'
    end

    def native_app_platform
      if request.user_agent.downcase.include?('android') #Android user agent string is Edcast-Android
       'android'
      else
        'ios' #default to ios as always
      end
    end
  end
end

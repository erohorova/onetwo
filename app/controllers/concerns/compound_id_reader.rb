require 'active_support/concern'

## USAGE:
## include CompoundIdReader
##
## reads_compound_id only: [:create], id_parameter_name: "card_id"
## Let's you support look up of virtual cards from ECL, whose ids are in the form: "ECL.xxx"
## See 'test/controllers/concerns/compound_id_reader_test.rb' for example usage
module CompoundIdReader
  extend ActiveSupport::Concern

  protected
  def load_cards_from_compound_ids(compound_ids)
    ids = compound_ids.collect{ |id|
      if Card.is_ecl_id?(id.to_s)
        load_card_from_ecl_id(id)&.id
      else
        id
      end
    }
    ids.compact!
    current_org.cards.where(id: ids).order("field(id, #{ids.join(',')})")
  end

  def load_card_from_compound_id(compound_id)
    compound_id = compound_id.to_s
    if Card.is_ecl_id?(compound_id)
      load_card_from_ecl_id(compound_id)
    else
      current_org.cards.friendly.find(compound_id)
    end
  end

  def load_card_from_ecl_id(compound_id)
    Octopus.using(:master) do
      ecl_id = Card.get_ecl_id(compound_id)
      EclApi::CardConnector.new(ecl_id: ecl_id, organization_id: current_org.id).card_from_ecl_id
    end
  end
end

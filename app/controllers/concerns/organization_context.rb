require 'active_support/concern'

# NOT READY TO BE INCLUDED IN ApiController.
# Forum APIs are likely to break if included.
module OrganizationContext
  extend ActiveSupport::Concern

  included do
    before_filter :set_organization_context
  end

  def current_org
    @current_org
  end

  def currently_signed_in_orgs
    @currently_signed_in_orgs
  end

  protected

  def ensure_default_org
    organization = Organization.find_by_request_host(request.host)
    unless organization == Organization.default_org
      respond_to do |format|
        format.html { render "errors/not_found", layout: 'errors', status: :not_found and return }
        format.json { head :not_found and return }
      end
    end
  end

  def ensure_not_default_org
    if @current_org == Organization.default_org
      respond_to do |format|
        format.json { head :not_found and return }
        format.html { render "errors/not_found", layout: 'errors', status: :not_found and return }
      end
    end
  end

  def set_currently_signed_in_orgs
    @currently_signed_in_orgs = []
    all_orgs = cookies.signed[:current_orgs]
    all_org_ids = []
    if all_orgs
      all_org_ids = all_orgs.split(',').map(&:to_i)
      if current_user && @current_org && !all_org_ids.include?(@current_org.id) && @current_org.has_access?(current_user)
        all_org_ids << @current_org.id
        cookies.signed[:current_orgs] = {value: all_org_ids.join(','), domain: Settings.platform_domain}
      end
      @currently_signed_in_orgs = Organization.fetch_multi(all_org_ids)
    else
      if current_user && @current_org && @current_org.has_access?(current_user)
        all_org_ids << @current_org.id
        cookies.signed[:current_orgs] = {value: all_org_ids.join(','), domain: Settings.platform_domain}
        @currently_signed_in_orgs = Organization.fetch_multi(all_org_ids)
      end
    end
  end

  def set_current_org_without_redirects
    @current_org = Organization.find_by_request_host(request.host)
  end

  def set_organization_context
    Octopus.using(:replica1) do
      # redirect to platform_host if no request domain (e.g., numeric ip)
      redirect_to root_url(host: Settings.platform_host) and return unless request.domain
      @current_org = Organization.find_by_request_host(request.host)

      if @current_org.nil?
        if (!params[:org].nil? && params[:org].is_a?(String))
          @current_org = Organization.find_by_host_name(params[:org])
        elsif params[:organization_id].present?
          @current_org = Organization.friendly.find(params[:organization_id])
        end
      end
    end

    if @current_org.nil?
      render "errors/not_found", layout: 'errors', status: :not_found and return
    end

    # BEGIN - This is to load all orgs the user is currently signed into.
    # For the side bar and for the /organizations page
    set_currently_signed_in_orgs
    # END

    # This is to ensure there is no redirect loop
    return if devise_controller?

    # BEGIN - This is to auto sign in the user on org create. See organizations_controller#create
    oid = cookies.signed[:new_org_oid]
    uid = cookies.signed[:new_org_uid]
    if oid.to_i == @current_org.id
      u = User.find_by(id: uid)
      if u
        warden.set_user(u)
        @current_user = u
        sign_in u
      end
      cookies.delete(:new_org_oid, domain: Settings.platform_domain)
      cookies.delete(:new_org_uid, domain: Settings.platform_domain)
      redirect_to @current_org.home_page and return
    end
    # END

    # Return if welcome controller. This is for the home page when the user is not signed in
    return if params[:controller] == "welcome"

    # Return if insights controller.
    # User should be redirected to content provider platform on both mobile and web.
    return if params[:controller] == 'insights'

    # No access check if open org
    return if @current_org.open?

    # redirect to custom org sign in page in case signed in user has no access
    redirect_to new_user_session_url(host: @current_org.host) unless current_user
  end

  def read_org_permissions
    @can_create_channel     = @current_org.get_channel_permissions_for(current_user)
    @can_share_on_social_nw = @current_org.get_sharing_permissions
  end

  def ensure_open_registration
    if !current_org.reload.email_registrable?(sign_up_params[:email])
      log.warn "User attempted to sign up for a closed organization, organization_id=#{current_org.id}"
      render inline: "This org is not open for registration", status: :forbidden
    end
  end
end

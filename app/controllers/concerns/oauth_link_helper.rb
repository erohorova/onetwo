require 'active_support/concern'

module OauthLinkHelper
  extend ActiveSupport::Concern

  private

  def current_resource_owner
    instance_eval(&Doorkeeper.configuration.authenticate_resource_owner)
  end

  # This method is used by doorkeeper gem
  # https://github.com/doorkeeper-gem/doorkeeper/blob/master/lib/doorkeeper/request/code.rb#L6
  def pre_auth
    @oauth_pre_auth
  end

  # == Args
  #
  # +reuse_query+::
  #   When true. Share the input path that matched the registered callback url otherwise append redirect_uri as a query string
  def generate_oauth_link(org:, destination_url:, reuse_query: false, app_data: nil)
    #client is deprecated, will update the logic later
    client = org.try(:client)
    destination_uri = URI.parse(destination_url)
    _redirect_uri = nil
    if client
      _redirect_uri = client.redirect_uri.split.find do |registered_redirect_uri|
        URI.parse(registered_redirect_uri).host == destination_uri.host
      end
    end

    if _redirect_uri
      @oauth_server ||= Doorkeeper::Server.new(self)
      @oauth_pre_auth ||= Doorkeeper::OAuth::PreAuthorization.new(Doorkeeper.configuration,
                                                                  client,
                                                                  {response_type: 'code',
                                                                   redirect_uri: _redirect_uri
                                                                  })

      @oauth_request_code ||= Doorkeeper::Request::Code.new @oauth_server
      @authorize_object ||= @oauth_request_code.request.authorize
      # IF: used when regular sign up from sessions controller to handover request to Savannah's sessions controller
      # ELSE: used when accessing Promotions/Deep Link URLs
      # This segregation is specifically used to pass the redirect_uri to Savannah and rest is handled in Savannah
      final_url = @authorize_object.redirect_uri + '&'
      final_url += reuse_query ? destination_uri.query : { redirect_uri: destination_url }.to_param
      final_url += final_url + '&' + app_data.to_query('app_data') if app_data.present?
      #https://something.com/auth/edcast/callback?code=579d91b6009642501c05bd95ae6b3d6a119f4430ef16ad080643b1ea2f0a14ed&redirect_uri=https%3A%2F%2Fsomething.com%2Fpath%2Fto%2Fpromo%3Fkey%3Dvalue
      final_url
    else
      log.info("Cannot find matching registered redirect_uri for url=#{destination_url}")
      destination_url
    end

  end
end

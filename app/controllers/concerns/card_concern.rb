module CardConcern
  extend ActiveSupport::Concern


  def prepare_card_params(attrs, opts = {})
    attrs[:state] = :published if attrs[:state].blank?
    attrs[:inline_indexing] = opts[:inline_indexing] || false
    attrs[:resource_id] = extract_url(attrs) if attrs[:resource_url]

    # Has to be explicitly marked as false
    attrs[:is_public] = if attrs[:is_public].present?
                          attrs[:is_public].to_bool
                        else
                          true
                        end

    set_card_type attrs
    set_file_resources attrs
    set_channels attrs
    set_groups attrs
    set_user_access attrs
    set_restriction attrs
    update_filestack attrs
    update_provider_image attrs
    sanitize_video_stream_attrs(attrs, opts[:action]) if attrs[:card_type] == 'video_stream'
    sanitize_poll_attrs (attrs) if attrs[:card_type] == 'poll'

    if attrs[:state] == :published &&  opts[:action] == :create
      attrs[:published_at] = Time.current
    end
    if attrs[:readable_card_type_name]
      attrs[:readable_card_type_name] = attrs[:readable_card_type_name].try(:underscorize)
    end
    attrs
  end

  # DESC:
  #   Pick filestack's raw URL before creating/updating.
  def update_filestack(attrs)
    return attrs if attrs[:filestack].blank?
    attrs["filestack"].map do |filestack|
      %w(url thumbnail).each do |type|
        next if filestack[type].blank?
        filestack[type] = Filestack::Security.read_filestack_url(filestack[type])
      end
      filestack
    end
    attrs
  end

  # DESC:
  #   Pick filestack's raw URL before creating/updating.
  def update_provider_image(attrs)
    return attrs if attrs[:provider_image].blank?
    attrs[:provider_image] = Filestack::Security.read_filestack_url(attrs[:provider_image])
    attrs
  end

  def set_card_type(attrs)
    # video stream
    if attrs[:video_stream].present?
      attrs[:card_type] = 'video_stream'
      return
    elsif attrs[:project_attributes].present?
      attrs[:card_type] = 'project'
      return
    # scrom course card
    elsif is_scorm_course_card?(attrs)
      attrs[:card_type] = 'course'
      return
    end

    # If options block present, update card_type to poll
    if !attrs[:options].blank?
      attrs[:card_type] = 'poll'
      return
    end
  end

  def is_scorm_course_card?(attrs)
    if attrs[:filestack].present?
      attrs[:filestack].first[:scorm_course]
    elsif attrs[:resource_id].present?
      scorm_resource = Resource.where(id: attrs[:resource_id]).first
      scorm_resource.url.include?('/api/scorm/launch?course_id=')
    end
  end

  def extract_url(attrs)
    resource = Resource.extract_resource attrs[:resource_url]
    attrs.delete :resource_url
    resource.try(:id)
  end

  def set_file_resources(attrs)
    if !attrs[:file_resource_ids].blank?
      ids = FileResource.where(id: attrs[:file_resource_ids]).uniq.pluck :id
      attrs.delete :file_resource_ids

      unless ids.blank?
        attrs[:file_resource_ids] = ids
      end
    end
  end

  def set_channels(attrs)
    if !attrs[:channel_ids].blank?
      # This is done to handle the situation when a card created inside a pathway(hidden: true)
      # is also posted to a channel. Such a card will not be hidden
      if attrs[:hidden] || @card.try(:hidden)
        attrs[:hidden] = false
      end
      attrs[:channel_ids] = attrs[:channel_ids].collect(&:to_i)
      # This is to ensure that the channels association with card is not removed when card is edited by admin user
      attrs[:channel_ids] = if current_user && !current_user.is_admin_user?
        writable_channel_ids = Channel.fetch_eligible_channels_for_user(user: current_user, q: nil, filter_params: {writables: true}).pluck(:id)
        # For EP-12551
        # the current user does not have access to post cards in these channels but the card
        # in consideration is already posted here.
        # P.S Also, ensuring that the api does not let user post to a non writable channel the card did not exist in already

        non_writable_channel_ids = if @card && @card.channel_ids
          (attrs[:channel_ids] - writable_channel_ids) & @card.channel_ids
        else
          []
        end
        (attrs[:channel_ids] & writable_channel_ids) + non_writable_channel_ids
      else
        current_org.channels.where(id: attrs[:channel_ids]).pluck(:id)
      end
    end
  end

  def set_groups(attrs)
    if !attrs[:team_ids].blank?
      # This is done to handle the situation when a card created inside a pathway(hidden: true)
      # is also posted to a group. Such a card will not be hidden
      attrs[:hidden] = false

      team_ids = attrs.delete(:team_ids).collect(&:to_i)

      attrs[:shared_with_team_ids] = if current_user
                                       writable_team_ids = current_user.teams.pluck(:id)
                                       team_ids & writable_team_ids
                                     else
                                       current_org.teams.where(id: team_ids).pluck(:id)
                                     end
    end
  end

  def set_user_access(attrs)
    if !attrs[:users_with_access_ids].blank?
      # This is done to handle the situation when a card created inside a pathway(hidden: true)
      # is is access controlled. Such a card will not be hidden
      attrs[:hidden] = false

      # Only get user_ids from the current org users
      attrs[:users_with_access_ids] = current_org.users.where(id: attrs[:users_with_access_ids]).pluck(:id)
    end
    attrs[:users_with_access_ids] ||= []
  end

  def set_restriction(attrs)
    attrs[:users_with_permission_ids] ||= []
    attrs[:teams_with_permission_ids] ||= []
  end

  def sanitize_video_stream_attrs(attrs={}, action)
    if action == :create
      attrs[:video_stream][:name] = attrs[:title] || attrs[:message]
      attrs[:video_stream][:state] = attrs[:state]
      attrs[:video_stream][:creator_id] = current_user.id
    end
    attrs[:video_stream_attributes] = attrs[:video_stream]
    attrs.delete :video_stream
  end

  def sanitize_poll_attrs(attrs={})
    attrs[:quiz_question_options_attributes] = attrs[:options]
    attrs.delete :options
  end

  #object_type: card or collection
  def record_card_action_metric(card, object_type, action)
    Tracking::track_act(
        user_id: current_user.id,
        object: object_type,
        object_id: card.id,
        card: ClientOnlyRoutes.show_insight_url(card),
        action: action,
        platform: get_platform,
        device_id: cookies[:_d],
        organization_id: current_user.organization_id,
        referer: params[:referer].presence || request.referer
    )
  end

  def record_card_create_metric(card)
    platform = defined?(get_platform) && get_platform
    device_id = defined?(cookies) && cookies[:_d]
    Tracking::track(
        card.author_id,
        'create',
        object: 'card',
        object_id: card.id,
        type: card.card_type,
        layout: nil,
        card: card.slug,
        platform: platform,
        device_id: device_id,
        organization_id: current_user.organization_id,
        referer: params[:referer].presence || request.referer
    )
  end

  def get_cards
    filter_states = []
    if params[:state].present?
      filter_states = params[:state] & Card::ADMIN_ALLOWED_STATES if current_user.is_admin_user?
      filter_states = params[:state] & Card::AUTHOR_ALLOWED_STATES if current_user.id == params[:author_id].to_i
    end

    filter_states = Card::MEMBER_ALLOWED_STATES if filter_states.blank? #default to last accessible cards

    card_types = params[:card_type].present? ? (params[:card_type] & Card::EXPOSED_TYPES_V2) : Card::EXPOSED_TYPES_V2

    @cards = if params[:only_deleted]&.to_bool
      current_org.cards.with_deleted.visible_cards
    else
      current_org.cards.visible_cards
    end

    @cards = if params[:readable_card_type].present?
      readable_card_types = params[:readable_card_type].map do |readable_card_type_name|
        Card.readable_card_type(readable_card_type_name)
      end.compact
       @cards.where(state: filter_states, readable_card_type: readable_card_types)
    else
       @cards.where(state: filter_states, card_type: card_types)
    end

    exclude_ids = []
    if params[:skip_content].present?
      exclude_ids |= current_user.bookmarked_card_ids if params[:skip_content][:bookmarked].present? && params[:skip_content][:bookmarked].to_bool
      exclude_ids |= current_user.completed_card_ids if params[:skip_content][:completed].present? && params[:skip_content][:completed].to_bool
      exclude_ids |= current_user.dismissed_card_ids if params[:skip_content][:dismissed].present? && params[:skip_content][:dismissed].to_bool
    else
      exclude_ids = current_user.dismissed_card_ids
    end

    if params[:card_subtype].present?
      @cards = @cards.where(card_subtype: params[:card_subtype])
    end

    if params[:is_official].present?
      @cards = @cards.where(is_official: params[:is_official].to_bool)
    end

    if params[:is_ugc].present?
      @cards = params[:is_ugc].to_bool ? @cards.user_created : @cards.system_generated
    end

    if exclude_ids.present?
      @cards = @cards.where.not(id: exclude_ids)
    end

    if params[:author_id].present?
      @cards = @cards.where(author_id: params[:author_id])
    end

    if params[:only_deleted]&.to_bool
      @cards = @cards.where.not(deleted_at: nil)
    end

    if params[:topics].present?
      @cards = @cards.distinct.joins(:tags).where(tags:{ name: params[:topics] })
    end

    if params[:load_topics].try(:to_bool)
      @cards = @cards.includes(:tags)
    end

    if params[:channel_id].present?
      @cards = @cards.curated_cards_for_channel_id(params[:channel_id])
    end

    if params[:q]
      @cards = @cards.where("cards.title like ? or cards.message like ? or cards.ecl_source_name like ?", "%#{params[:q]}%", "%#{params[:q]}%", "%#{params[:q]}%")
    end

    if params[:sort] && params[:sort].downcase == 'rating'
      @cards = @cards
      .joins("LEFT JOIN(SELECT AVG(IFNULL(cards_ratings.rating, 0)) as average_card_rating, card_id from cards_ratings
        GROUP BY cards_ratings.card_id) cards_ratings ON cards_ratings.card_id = cards.id")
    end

    if params[:sort] && params[:sort].downcase == 'views_count'
      @cards = @cards
      .joins("LEFT JOIN(SELECT SUM(num_views) as views, card_id
        FROM card_metrics_aggregations
        WHERE organization_id = #{current_org.id}
        GROUP BY card_metrics_aggregations.card_id)
        as card_views ON card_views.card_id = cards.id")
    end

    if params[:sort] && params[:sort].downcase == 'added_to_channel'
      @cards = @cards.joins(:channels_cards)
    end

    if params[:filter_by_language]&.to_bool && current_user.language
      @cards = @cards.filtered_by_language(current_user.language)
    end

    # To call on the visibility filter only if user is not an org admin
    # To not call the visibility filter if there are no cards to process
    @cards = @cards.visible_to_userV2(current_user) unless (current_user.is_org_admin? || @cards.empty?)
    @cards = @cards.order(card_order_query(params[:sort]))
    @total = @cards.count
    @cards = @cards.limit(limit).offset(offset)
  end

  def get_cards_for_cms
    card_types = (params[:card_type].present? ? (params[:card_type] & Card::EXPOSED_TYPES_V2) : Card::EXPOSED_TYPES_V2)

    filter_states = (params[:state].present? ?  (params[:state] & Card::ADMIN_ALLOWED_STATES) : Card::ADMIN_ALLOWED_STATES)

    @cards = current_org.cards

    if current_user.sub_admin_authorize?(Permissions::MANAGE_CONTENT)
      # sub_admin visible all open cards
      # visible private cards where he sub_admin in the team
      @cards = filter_cards_for_sub_admin(sub_admin: true)
    elsif current_user.group_sub_admin_authorize?(Permissions::MANAGE_GROUP_CONTENT)
      # group_sub_admin visible open/private cards where he sub_admin in the team
      # in admin_console (is_cms: true)
      @cards = filter_cards_for_sub_admin(sub_admin: false)
    end

    @cards = @cards.where(state: filter_states, card_type: card_types)

    # Default display UGC cards for cms if is_ugc not passed
    if params[:is_ugc].nil? || params[:is_ugc].try(:to_bool)
      @cards = @cards.user_created
      if params[:q].present?
        @cards = @cards.joins(:author).where("cards.message like :q or
          cards.ecl_source_name like :q or
          users.first_name like :q or
          users.last_name like :q or
          concat(users.first_name, ' ', users.last_name) like :q",
          q: "%#{params[:q]}%")
      end
    else
      @cards = @cards.system_generated
      if params[:q].present?
        @cards = @cards.where("cards.message like :q or cards.ecl_source_name like :q", q: "%#{params[:q]}%")
      end
    end

    if params[:channel_id].present?
      @cards = @cards.cards_for_channel_id(params[:channel_id])
    end

    if ['created', 'published', 'updated', 'ecl_source_name', 'card_type', 'state'].include?(params[:sort])
      @cards = @cards.order(card_order_query(params[:sort]))
    end

    if params[:sort] == 'author_name'
      @cards = @cards.includes(:author).order(card_order_query(params[:sort]))
    end

    @total = @cards.count
    @cards = @cards.limit(limit).offset(offset)
  end

  def card_order_query(sort_param)
    sort_param = sort_param.present? ? sort_param.to_sym : :published
    case sort_param
    when :published
      sort_field_query(:published_at)
    when :created
      sort_field_query(:created_at)
    when :updated
      sort_field_query(:updated_at)
    when :like_count
      sort_field_query(:votes_count)
    when :promoted_first
      "#{sort_field_query(:is_official)}, #{sort_field_query(:promoted_at)}, #{sort_field_query(:published_at)}"
    when :rating
      sort_field_query("cards_ratings.average_card_rating")
    when :views_count
      sort_field_query("card_views.views")
    when :added_to_channel
      sort_field_query("channels_cards.created_at")
    when :ecl_source_name
      sort_field_query(:ecl_source_name)
    when :card_type
      sort_field_query(:card_type)
    when :state
      sort_field_query(:state)
    when :title
      sort_field_query(:title)
    when :message
      sort_field_query(:message)
    when :author_name
      sort_field_query("users.first_name")
    when :rank
      "CASE WHEN channels_cards.rank IS NULL THEN 1 ELSE 0 END, channels_cards.rank asc, channels_cards.created_at desc"
    end
  end

  def sort_field_query(column)
    order = (params[:order].present?) ? params[:order] : 'DESC'
    "#{column} #{order}"
  end

  def filter_cards_for_sub_admin(sub_admin: false)
    team_ids = params[:group_id] || []
    cards = current_user.cards_for_sub_admin(team_ids: team_ids)
    if sub_admin && team_ids.blank?
      all_public = current_org.cards.where(is_public: true) # all public cards
      # includes the request of all open cards in cards for sub_admin
      Card.from("(#{cards.to_sql} UNION #{all_public.to_sql}) AS cards")
    else
      cards
    end
  end

  protected

  def update_card(params)
    if Card.is_ecl_id?(params[:id])
      @card = load_card_from_compound_id(params[:id])
      raise ActiveRecord::RecordNotFound if @card.nil?
    elsif params[:card][:channel_ids].present?
      find_org_card
    else
      @card = current_user.is_admin_user? ? find_org_card : find_author_card
    end

    options = {inline_indexing: true, action: :update}

    # will return an array of channels where card is in new or curated state
    old_card_channel_ids = @card.non_curated_channel_ids | @card.curated_channel_ids

    @card.current_user = current_user
    @card.old_author_id = @card.author_id
    card_attrs = prepare_card_params(card_params, options)
    card_attrs = update_filestack(card_attrs)
    card_attrs = update_provider_image(card_attrs)

    # Unconditional update to ensure that access parameters are copied
    @card.ecl_update_required = true

    new_team_ids = card_params[:team_ids] - @card.shared_with_team_ids if card_params[:team_ids].present?

    if params[:card].key?(:team_ids)
      card_attrs[:shared_with_team_ids] = params[:card][:team_ids] || []
    end

    new_user_access_ids = card_attrs[:users_with_access_ids] - @card.users_with_access_ids if card_attrs[:users_with_access_ids].present?

    new_channel_ids = card_attrs[:channel_ids] - old_card_channel_ids if card_attrs[:channel_ids].present?

    @card.repost_to_channel(new_channel_ids) if @card.rejected_from_channels(new_channel_ids).present?
    tags_list = @card.tags.pluck(:name) # previous tags
    if (updated = @card.update_attributes(card_attrs)) && old_card_channel_ids != card_attrs[:channel_ids]
      @card.update_channel_pins(old_card_channel_ids, card_attrs[:channel_ids])
      log.info "Calling ContentsBulkDequeue Job via card_concern for id #{@card.id}"
      @card.remove_from_queue_and_reenqueue #When Channels are updated, all cards are reenqueued to its followers

      # handle creation of UMC for post_to_channel action
      if current_user != @card.author && card_attrs[:channel_ids].is_a?(Array)
        new_channels = card_attrs[:channel_ids] - old_card_channel_ids
        action = UsersCardsManagement::ACTION_POST_TO_CHANNEL
        new_channels.each do |channel|
          UsersCardsManagementJob.perform_later(current_user.id, @card.id, action, channel)
        end
      end
    end

    # @card.update_curation(card_attrs[:channel_ids], current_user) if updated

    # here we are updating CardsRating level to nil if CardsRating is present for current_user and
    # card_metadatum_attributes level is set to nil
    if card_params[:card_metadatum_attributes].try(:level) == nil
      card_rating = CardsRating.find_by(user_id: current_user.id, card_id: @card.id)
      if card_rating
        card_rating.level = nil
        card_rating.save
      end
    end

    if new_user_access_ids.present? || new_team_ids.present?

      @card.send_card_create_or_share_notification(event: Card::EVENT_CARD_SHARE,
        user_ids: new_user_access_ids,
        team_ids: new_team_ids, share_by_id: current_user.id,
        permitted_user_ids: card_params[:users_with_permission_ids],
        permitted_team_ids: card_params[:teams_with_permission_ids])
    end
    if @card.journey_card? && @card.card_subtype == 'weekly' && params[:card][:journey_start_date].present? && updated
      start_date = Time.zone.at(params[:card][:journey_start_date].to_i)
      JourneyPackRelation.find_by(cover: @card).update_attributes!(start_date: start_date)
    end
    if @card.journey_card? && updated
      rel = JourneyPackRelation.journey_relations(cover_id: @card.id)
      # if rel and start_date present - invoke remap of start_date
      if rel&.first&.start_date
        JourneyPackRelation.remap_start_date(cover_id: @card.id)
      end
    end
    if updated
      # Removing card reporting index
      CardReportingIndexingJob.perform_later(card_id: @card.id)

      # topics changes
      topics_before = tags_list&.join(",")
      topics_after = card_attrs[:topics]&.join(",")
      topics_changes = topics_before == topics_after ? [] : [topics_before, topics_after]

      # Log changes
      LogHistoryJob.perform_later(
        "entity_type" => 'card',
        "entity_id" => @card.id,
        "changes" => @card.previous_changes.merge(topics: topics_changes).as_json,
        "association_changes" => CardHistory.association_changes(@card).as_json,
        "user_id" => current_user.id
      )
      # resize image
      ProcessFilestackImagesJob.perform_later(@card.id) if @card.previous_changes.keys.include?("filestack")
    end
    updated
  end

  def delete_card(params)
    is_ecl_id = Card.is_ecl_id?(params[:id])
    is_ecl_id ? find_org_card_by_ecl_id : find_org_card

    if @card
      @card.current_user = current_user
      if (current_user.is_admin_user? || @card.author == current_user)
        delete_dependent = params[:delete_dependent].present? ? params[:delete_dependent].to_bool : false
        @card.delete_card(delete_dependent: delete_dependent)
      end
    else
      if current_user.is_admin_user? && is_ecl_id
        EclCardArchiveJob.perform_later(params[:id].gsub('ECL-', ''), current_org.id)
      else
        raise ActiveRecord::RecordNotFound
      end
    end
  end

  def find_author_card
    current_org.cards.where(author_id: current_user.id).friendly.find(params[:id])
  end

  def find_published_card
    @card = current_org.cards.published.friendly.find(params[:id])
  end

  def find_org_card
    @card = current_org.cards.friendly.find(params[:id])
  end

  def find_org_card_by_ecl_id
    @card = current_org.cards.find_by(ecl_id: params[:id].gsub('ECL-', ''))
  end

  def require_card_author_or_permission
    @card = find_org_card
    if @card.author != current_user
      unless current_user.authorize?(Permissions::VIEW_CARD_ANALYTICS)
        render_unauthorized('Permission denied') and return
      end
    end
  end

  def check_compound_ids
    if params[:content_ids].delete_if(&:blank?) && params[:content_ids].blank?
      return render_unprocessable_entity('Missing required params')
    end
  end

  def check_poll_card_for_allow_re_answer
    card = get_poll_card(params[:id])
    if QuizQuestionAttempt.has_user_attempted_quiz_question_card?(card.id, current_user.id) && !card.can_be_reanswered
      return render_unprocessable_entity('Can not re-answer this card')
    end
  end

  def get_array_ids(array_ids)
    if array_ids.is_a?(Array)
      array_ids.delete_if(&:blank?)
      return render_unprocessable_entity('Missing required params') if array_ids.blank?
      array_ids
    else
      return render_unprocessable_entity('Invalid data type')
    end
  end
end

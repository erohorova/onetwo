require 'active_support/concern'

module DeviceCookieFilter
  extend ActiveSupport::Concern

  included do
    before_filter :drop_device_cookie
  end

  protected
  def drop_device_cookie
    unless cookies[:_d]
      cookies[:_d] = { value: SecureRandom.urlsafe_base64, expires: 1.year.from_now, secure: Settings.secure_cookies }
    end
  end
end

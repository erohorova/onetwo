require 'active_support/concern'
module WidgetConcern
  extend ActiveSupport::Concern

  def authorize_viewer
    return if current_user.is_org_admin?
    parent = @parent || @widget&.parent

    # As and when the scope of widgets will be expanded,
    # more `when` clauses will be added to this switch-case
    case parent.class.to_s
    when 'Channel'
      verify_curator_or_creator(parent)
    else
      render_unprocessable_entity('Invalid parent')
    end
  end

  def verify_curator_or_creator(channel)
    unless channel.is_curator?(current_user) || channel.is_creator?(current_user)
      render_unauthorized('User should be curator or creator of channel or organization admin')
    end
  end
end

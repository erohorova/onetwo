require 'active_support/concern'

module SafeRedirect
  extend ActiveSupport::Concern

  def safe_redirect(url)
    URI.parse(url).to_s
  end
end
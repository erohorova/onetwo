require 'active_support/concern'


## USAGE:
## include LimitOffsetConcern
##
## has_limit_offset_constraints only: [:channels], limit_default: 10, offset_default: 0, max_limit: 50
module LimitOffsetConcern
  extend ActiveSupport::Concern

  protected
  def set_limit_and_offset_default
    @_limit = [(params[:limit] || self.class.limit_offset_options[:limit_default]).to_i, self.class.limit_offset_options[:max_limit]].compact.min
    @_offset = params[:offset] ? params[:offset].to_i : self.class.limit_offset_options[:offset_default]
  end

  def limit
    @_limit
  end

  def offset
    @_offset
  end

  def is_limit_exceeds(max_limit: )
    max_limit < limit 
  end

  class_methods do

    def has_limit_offset_constraints(options = {})
      self.class_attribute :limit_offset_options
      self.limit_offset_options = default_options.merge(options)
      self.before_action(:set_limit_and_offset_default, options)
    end

    def default_options
      {
        limit_default: 10,
        offset_default: 0
      }
    end
  end

end

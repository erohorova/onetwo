require 'active_support/concern'
module CardCreationHelpers
  extend ActiveSupport::Concern

  included do
    def validate_client_input_params
      client_params = { item_id: params[:item_id] }
      #TODO deprecated
      if @client
        client_params.merge!({client_id: @client.id})
      end
      return client_params, nil
    end

    def share_to_social card
      return if card.blank?
      networks = params[:networks]
      unless networks.blank?
        message = card.message
        if card.card_type == 'poll'
          message = card.title
        end
        ShareJob.perform_later(sharable_class: 'Card', sharable_id: card.id, user_id: current_user.id, networks: networks, message: message)
      end
    end

    def ui_layout_from_resource res
      case res.type
      when 'Video'
        'video'
      when 'Image'
        'image'
      else
        'link'
      end
    end

  end
end

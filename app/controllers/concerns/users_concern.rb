module UsersConcern
  extend ActiveSupport::Concern

  def prepare_filter(attrs)
    filter_params = {}

    filter_params[:skip_aggs] = attrs[:skip_aggs].nil? ? true : attrs[:skip_aggs]&.to_bool

    if attrs[:role].present?
      filter_params[:role] = current_org.roles.where(default_name: attrs[:role]).pluck(:name)
    end

    if attrs[:followers]&.to_bool
      filter_params[:followers] = true
    end

    if attrs[:following]&.to_bool
      filter_params[:following] = true
    end

    if attrs[:include_suspended]&.to_bool
      filter_params[:include_suspended] = true
    end

    if attrs[:only_suspended]&.to_bool
      filter_params[:only_suspended] = true
    end

    if attrs[:expertises_names].present? && attrs[:expertises_names].is_a?(Array)
      filter_params[:expertises_names] = attrs[:expertises_names]
    end

    if attrs[:from_date].present? && attrs[:to_date].present?
      filter_params[:from_date] = attrs[:from_date]
      filter_params[:to_date] = attrs[:to_date]
    end

    if attrs[:custom_fields].present? && is_json?(attrs[:custom_fields])
      filter_params[:custom_fields] = ActiveSupport::JSON.decode(attrs[:custom_fields])
    end

    if attrs[:search_by].in? %w[name skills]
      filter_params[:search_by] = attrs[:search_by] == 'skills' ? ['expert_topics.searchable_topic_label'] : ['name.searchable', 'first_name.searchable', 'last_name.searchable']
    end

    if attrs[:names_for_filtering].present? && attrs[:names_for_filtering].is_a?(Array)
      filter_params[:names_for_filtering] = attrs[:names_for_filtering]
    end

    if attrs[:emails_for_filtering].present? && attrs[:emails_for_filtering].is_a?(Array)
      filter_params[:emails_for_filtering] = attrs[:emails_for_filtering]
    end

    if attrs[:is_dynamic_selection]&.to_bool
      filter_params[:is_dynamic_selection] = true
    end

    filter_params
  end

  def users_search(query, offset, limit)
    filter_params = prepare_filter(params)
    custom_fields_names = current_org.custom_fields
      .where(enable_people_search: true).pluck(:display_name)
    opts = {
      q: query,
      viewer: current_user,
      allow_blank_q: true,
      filter: filter_params,
      offset: offset,
      limit: limit,
      load: true,
      use_custom_fields: true,
      custom_fields_names: custom_fields_names
    }
    opts[:sort] = params[:sort] if params[:sort].present?
    opts[:order] = params[:order] if params[:order].present?
    Search::UserSearch.new.search(opts)
  end

  def load_all_users_by_filter(query, offset, limit:)
    res = users_search(query, offset, limit)
    @users = res.results
    total = res.total

    if total > limit
      diff = total - @users.count
      while !diff.zero? do
        offset = @users.count
        res = users_search(query, offset, limit)
        @users.push(*res.results)
        diff = total - @users.count
      end
    end
    @users
  end

  def get_last_viewed
    Analytics::LastActivityRecorder.new(
      user_id: current_user.id
    ).get_data_from_cache
  end

  def get_peer_learning_data
    {
      orgUsersData:   UserMetricsAggregation
                      .avg_time_spent_by_users(
                        org_id:   current_org.id, 
                        period:   params["period"], 
                        limit:    limit, 
                        offset:   offset
                      ),
      userData:       UserMetricsAggregation
                      .time_spent_by_user(
                        user_id:  params[:id], 
                        period:   params["period"], 
                        limit:    limit, 
                        offset:   offset
                      )
    }
  end

  
end

require 'active_support/concern'

module CurrentAppBuildConcern
  extend ActiveSupport::Concern
  included do
    helper_method :current_app_build
    def current_app_build
      @current_app_build ||= ENV['DOCKER_IMAGE']
    end
  end
end

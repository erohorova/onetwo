require 'active_support/concern'

#EdCast/1.0 CFNetwork/711.1.12 Darwin/14.0.0    GET request
#EdCast/1.0 (iPhone; iOS 8.1.1; Scale/2.00)     non-GET request
#EdCast/1.0 (iPhone Simulator; iOS 8.1; Scale/2.00) non-GET request
#Edcast-Android #all
module AccessLogFilter
  extend ActiveSupport::Concern

  included do
    after_filter :log_user_request
  end

  protected
  def log_user_request(user: nil)
    _user = user || current_user
    return unless _user

    datetime = DateTime.current #utc timezone
    access_at_date = datetime.beginning_of_day.to_date.to_s(:db)

    opts = {
        user_id: _user.id,
        platform: get_platform,
        access_at_date: access_at_date,
        access_uri: @access_uri || "#{request.method}:#{request.fullpath}",
        created_at: datetime.to_s(:db),
        organization_id: _user.organization_id,
        referer: request.referer
    }

    if request.cookies['access'].present? && @access_uri.try(:starts_with?, 'signup.')
      log.warn("Repeated signup from the same browser: user_id=#{_user.id} user_name=#{_user.name}")
      opts[:annotation] = 'repeat_signup'
    end

    # Log only once a day for the forum. For destiny requests, this is taken care of by the "access" cookie block below. Forum cookies aren't stored by the browser because it is loaded within savannah (third party cookies)
    if opts[:platform] == "forum" && AccessLog.exists?(platform: "forum", user_id: opts[:user_id], access_at: opts[:access_at_date])
      return
    end

    # See if 'access' cookie is present, and if yes, is it older than a day?
    if (request.cookies['access'].present? ? (access_at_date > request.cookies['access']) : true) ||
        (@access_uri.try(:starts_with?, 'signup.')
      )
      AccessLogJob.perform_later(opts)
      cookies[:access] = { value: access_at_date, expires: 1.year.from_now }
    end

    if %w[signup. login.].any? {|s| @access_uri.try(:starts_with?, s) }
      cookie_data = Tracking::track_entry(opts)
      cookies[:entry] = cookie_data if cookie_data
    end
  end
end

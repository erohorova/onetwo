module ApplicationConcern
  extend ActiveSupport::Concern

  def set_cache_headers
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"

    # Sets max-age to 1 YEAR only for https protocol
    response.headers["Strict-Transport-Security"] = 'max-age=31536000; includeSubDomains' if request.ssl?
  end
end

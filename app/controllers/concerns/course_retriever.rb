## Pulls course data from S3
module CourseRetriever
  def pull_course_data_from_s3
    @group = Group.find(params[:id])
    unless @group.is_active_user?(current_user)
      head :unauthorized and return
    end
    s3_url = @group.course_data_url
    unless s3_url
      # Send an acceptable empty state
      @lectures = []
      @savannah_data = {}
      render 'groups/course_data', status: :ok and return
    end
    begin
      data = open(s3_url).read
    rescue => e
      log.error "Error in getting data from S3 for course: #{@group.name}"
      head :unprocessable_entity and return
    end

    v1_ios_build_number = 44
    v1_android_build_number = 37
    if client_supports_required_build?(v1_ios_build_number, v1_android_build_number)
      @api_version = 1
    else
      @api_version = 0
    end

    begin
      @downloaded_json = JSON.parse(data)
    rescue
      log.error "Error in parsing S3 data for course: #{@group.name}"
      head :unprocessable_entity and return
    end

    @savannah_data = @downloaded_json['meta_savannah']
    @start_date = Date.parse(@savannah_data['start_date'])
    if @start_date > Date.today
      has_started = false
    else
      has_started = true
    end

    @lectures = []
    if has_started
      chapters = @downloaded_json['chapters']

      chapters.each do |chapter|
        lecture_videos = []
        lecture_statics = []
        title = chapter['chapter_title']
        sequentials = chapter['sequentials']
        sequentials.each do |sequential|
          verticals = sequential['verticals']
          verticals.each do |vertical|
            if @api_version == 0
              videos = vertical['videos']
            elsif @api_version == 1 # include vimeo
              videos = vertical['videos'] | (vertical['vimeo_videos'].nil? ? [] : vertical['vimeo_videos'])
            end

            # support brightcove video
            ios_build_number = 379
            android_build_number = 59
            if is_native_app? && client_supports_required_build?(ios_build_number, android_build_number)
              if vertical['brightcove_videos'] && !vertical['brightcove_videos'].empty?
                regenerate_brightcove_url vertical['brightcove_videos']
                videos += vertical['brightcove_videos']
              end
            end

            videos.each do |video|
              if @api_version == 0
                lecture_videos << {'youtube_id' => video['youtube'],
                                   'youtube_title' => video['youtube_title']}
              elsif @api_version == 1
                lecture_videos << video
              end
            end

            if vertical['static'].present?
              vertical['static'].each do |static|
                lecture_statics << {
                                    'static_title' => static['static_title'],
                                    'static_url' => static['static_url']
                                   }
              end
            end
          end
        end

        if lecture_videos.length > 0 || lecture_statics.length > 0
          @lectures << {
                        'chapter_title' => title,
                        'videos' => lecture_videos,
                        'statics' => lecture_statics
                       }
        end
      end
    end
    FlipmodeActivityRecorderJob.perform_later(current_user.email, @group.client_resource_id)
    render 'groups/course_data', status: :ok and return
  end

  private
    def regenerate_brightcove_url bc_videos
      bc_videos.each {|bc_v| bc_v['video_id'] = "#{brightcove_videos_url(host: @group.organization.host)}?video_url=#{bc_v['video_id']}" }
    end
end

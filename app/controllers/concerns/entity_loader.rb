# frozen_string_literal: true

require 'active_support/concern'

module EntityLoader
  extend ActiveSupport::Concern
  ENTITY_LIST = %w[Card Channel Organization User Bookmark Team]

  def load_entity_from_type_and_id(entity_type, entity_id)
    return unless ENTITY_LIST.include?(entity_type)

    case entity_type
    when 'Card'
      load_card_from_compound_id(entity_id)
    when 'Organization', 'Bookmark'
      entity_type.constantize.find_by(id: entity_id)
    when 'User'
      User.find_by(id: entity_id, organization_id: current_org.id)
    else
      entity_type.constantize.find_by(organization_id: current_org.id, id: entity_id)
    end
  end

  def load_entities_from_type_and_ids(entity_type, entity_ids)
    return unless ENTITY_LIST.include?(entity_type)

    case entity_type
    when 'Card'
      load_cards_from_compound_ids(entity_ids)
    when 'User'
      current_org.users.where(id: entity_ids).order("field(id, #{entity_ids.join(',')})")
    when 'Channel'
      current_org.channels.where(id: entity_ids).order("field(id, #{entity_ids.join(',')})")
    end
  end
end

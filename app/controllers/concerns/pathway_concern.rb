module PathwayConcern
  extend ActiveSupport::Concern
  include CompoundIdReader

  def process_section(cover:, section:)
    section[:content_ids].delete_if(&:blank?)
    return if section[:content_ids].blank? || cover&.card_type != 'pack'

    any_ecl_card = section[:content_ids].any? { |content_id| Card.is_ecl_id?(content_id.to_s) }
    contents = any_ecl_card ? load_cards_from_compound_ids(section[:content_ids]) : current_org.cards.where(id: section[:content_ids])

    contents.compact.each do |content|
      add_card(cover: cover, content: content)
    end

    NotifyUserJob.perform_later({action: 'added_to', card_ids: section[:content_ids].map(&:to_i), cover_ids: [section[:section_id].to_i]})
  end

  # add a card(content) to pathway(cover)
  def add_card(cover:, content:)
    ordered = CardPackRelation.where(cover_id: cover.id).order(to_id: :desc)
    if ordered.empty?
      log.error "Pack not found: adding id #{content.id} type Card to pack cover card #{cover.id}"
      return
    end

    return if ordered.map(&:from_id).include?(content.id.to_i)

    linked = ordered.last
    unless linked.update(to: content)
      log.error "#{linked.errors.full_messages}: adding id #{content.id} type Card to pack cover card #{cover.id}"
      @errors.push(content.id)
    end
  end
end
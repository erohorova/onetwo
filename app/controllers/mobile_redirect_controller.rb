class MobileRedirectController < ApplicationController

  skip_before_filter :authenticate_user!

  layout false

  # Use this end point when you want to launch tne app
  # for mobile/tablet browsers
  # DEPRECATED
  def redirect
    @redirect_url = params['url']
  end
end

class GraphqlController < Api::V2::BaseController
  include FollowableConcern
  include SearchConcern

  def execute
    # Decide fields for cards
    variables = ensure_hash(params[:variables])
    query = params[:query]
    operation_name = params[:operationName]
    context = {
      # Query context goes here, for example:
      # current_user: current_user,
    }
    result = EdcastSchema.execute(query, variables: variables, context: context, operation_name: operation_name)
    @fields = result['data']['card']

    # Normal query for cards
    query_type = params[:query_type].presence || 'and'
    do_search(search_params(@fields['lxp']), query_type: query_type)
  end

  private

  # Handle form data, JSON body, or a blank value
  def ensure_hash(ambiguous_param)
    case ambiguous_param
    when String
      if ambiguous_param.present?
        ensure_hash(JSON.parse(ambiguous_param))
      else
        {}
      end
    when Hash, ActionController::Parameters
      ambiguous_param
    when nil
      {}
    else
      raise ArgumentError, "Unexpected parameter: #{ambiguous_param}"
    end
  end

  def search_params(p)
    ActionController::Parameters.new(p).permit(:q, :from_date, :till_date, :cards_offset, :cards_limit, :channels_offset, :channels_limit, :users_offset, :users_limit, :sociative,
                  source_id: [], content_type: [], source_type_name: [], exclude_cards: [],
                  posted_by: [:followers, :following, :user_ids],
                  liked_by: [:followers, :following, :user_ids],
                  commented_by: [:followers, :following, :user_ids],
                  bookmarked_by: [:followers, :following, :user_ids]
    )
  end
end

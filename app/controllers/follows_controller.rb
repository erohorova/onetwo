class FollowsController < ApiController
  include Tracking::Visit

  skip_before_filter :cm_auth_user, only: [:unsubscribe]
  before_action :set_followable, only: [:create, :destroy]

  api :GET, 'unsubscribe', "unfollow the user from a given subscribable"
  param :unsubscribe_token, String, "the encrypted token for unfollow"
  formats ['html']
  def unsubscribe
    tok = params.has_key?(:unsubscribe_token) ? params[:unsubscribe_token] : params[:token]
    unless tok
      @error = 'badtoken'
      render 'unsubscribe' and return
    end

    follow_id = ActiveSupport::MessageEncryptor.
      new(Edcast::Application.config.secret_key_base).
      decrypt_and_verify(CGI::unescape(tok))

    follow = Follow.where(:id => follow_id).first

    if follow
      @user = follow.user
      @followable = follow.followable
      if !follow.destroy
        @error = 'deleteerror'
      end
    else
      @error = 'deleted'
    end
  end

  api :POST, 'follows', "follow a channel, query, user or post"
  param :channel_id, Integer, desc: 'id of channel to follow', required: false
  param :user_id, Integer, desc: 'id of user to follow', required: false
  param :query, String, desc: 'query string to follow', required: false
  param :mode, String, desc: 'optional mode for follow, e.g. "onboarding"', required: false
  param :post_id, Integer, desc: 'id of post to follow', required: false
  formats ['json']
  def create

    if @followable.kind_of?(Channel) && !@followable.has_read_access?(current_user)
      render json: {status: 'forbidden', errors: ''}, status: :forbidden and return
    end

    follow = Follow.only_deleted.where(user: current_user, followable: @followable).first
    if follow.nil?
      follow = Follow.new(user: current_user, followable: @followable)
      result = follow.save
    else
      result = follow.restore
    end

    if result
      EDCAST_NOTIFY.trigger_event(Notify::EVENT_NEW_FOLLOW, follow) if @followable.kind_of?(User)
      track_follow
      remove_cached_discover_items_counts
      render json: {status: 'success'}
    else
      # re-follow can occur if logging in during follow attempt; just warn
      level = follow.errors.to_h == {user_id: 'has already been taken'} ? :warn : :error
      log.send level, "Unable to follow, errors=#{follow.errors.to_json}"
      render json: {status: 'error', errors: follow.full_errors}, status: :unprocessable_entity
    end
  end

  api :DELETE, 'follows', "unfollow a channel, query, user or post"
  param :channel_id, Integer, desc: 'id of channel to unfollow', required: false
  param :user_id, Integer, desc: 'id of user to unfollow', required: false
  param :query, String, desc: 'query string to unfollow', required: false
  param :post_id, Integer, desc: 'id of post to unfollow', required: false
  formats ['json']
  def destroy
    follow = Follow.find_by(user: current_user, followable: @followable)

    if follow.nil?
      error = "Trying to unfollow not-followed object: user #{current_user.id}, params #{params}"
      log.error(error)
      render json: {status: 'error', errors: error}, status: :unprocessable_entity
      return
    end

    if follow.destroy
      track_follow destroyed: true
      remove_cached_discover_items_counts
      render json: {status: 'success'}
    else
      log.error("Unable to unfollow, errors=#{follow.errors.to_json}")
      render json: {status: 'error', errors: follow.full_errors}, status: :unprocessable_entity
    end
  end

  # additional routes for documentation
  api :POST, 'api/users/:user_id/follow', 'follow a user; alternative to /follows'
  api :POST, 'api/channels/:channel_id/follow', 'follow a channel; alternative to /follows'
  api :POST, 'api/posts/:post_id/follow', 'follow a post; alternative to /follows'
  api :POST, 'api/users/:user_id/unfollow', 'unfollow a user; alternative to /follows'
  api :POST, 'api/channels/:user_id/unfollow', 'unfollow a channel; alternative to /follows'
  api :POST, 'api/posts/:post_id/unfollow', 'unfollow a post; alternative to /follows'

  private

  # discover items count should update on follow/unfollow action
  # for users, channels
  def remove_cached_discover_items_counts
    if @followable.kind_of?(Channel) || @followable.kind_of?(User)
      Rails.cache.delete("discover-items-counts-for-#{current_user.id}")
    end
  end

  def set_followable
    if params[:channel_id]
      @followable = Channel.find(params[:channel_id])
    elsif params[:post_id]
      @followable = Post.find(params[:post_id])
    elsif params[:user_id]
      @followable = User.find(params[:user_id])
    end
  end

  def track_follow(destroyed: false)
    track_opts = {
        user_id: current_user.id,
        action: "#{'un' if destroyed}follow",
        platform: get_platform,
        object_id: @followable.id,
        device_id: cookies[:_d],
        organization_id: current_user.organization_id,
        referer: params[:referer].presence || request.referer
    }
    track_opts.merge! tracking_params(@followable)
    track_opts[:object] ||= @followable.class.name.underscore
    track_opts.merge!(mode: params[:mode]) if params[:mode]
    Tracking::track_act track_opts
  end
end

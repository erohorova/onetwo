class StreamsController < ApiController

  api :GET, "groups/:group_id/streams", "Get stream of activities from a given group. Returns the newest activity to oldest activity"
  param :group_id, Integer, desc: "The concerned group", required: true
  param :limit, Integer, desc: "page limit", required: false
  param :page, Integer, desc: "page number", required: false
  param :last_fetch_time, Integer, desc: "last fetch time", required: false
  formats ['json']

  def index
    group_id = params.require(:group_id)
    limit    = params[:limit].blank? ? 20 : [params[:limit].to_i, 20].min
    offset   = params[:page].blank? ? 0 : (params[:page].to_i * limit.to_i)

    # make sure it works with blank string
    last_fetch_time = params[:last_fetch_time].blank? ? nil : params[:last_fetch_time]
    streams = Stream.includes(:user, :item).where(group_id: group_id).forum_activities

    if params[:context_id].present?
      post_ids = Post.where(context_id: params[:context_id]).pluck(:id)
      comment_ids = Comment.where(commentable_type: "Post", commentable_id: post_ids).pluck(:id)
      streams = streams.where("(item_type = 'Post' AND item_id IN(?)) OR (item_type = 'Comment' AND item_id IN(?))", post_ids, comment_ids)
    end

    @streams =  last_fetch_time ?
      streams.where('updated_at > ?', DateTime.parse(last_fetch_time)).order!(updated_at: :desc) :
      streams.order!(updated_at: :desc).limit(limit).offset(offset)

    response.headers['X-Last-Fetch-Timestamp'] =
       @streams.first ? @streams.first.updated_at.utc.strftime('%Y-%m-%dT%H:%M:%S.%LZ') : (last_fetch_time || nil)


    # only including the streams
    # whose item(post, comment) do not have hidden = 1
    @streams = @streams.select do |s|
      if !s.item
        false
      # if there is a hidden function in the item i.e it is a commentor a post,
      elsif s.item.respond_to?(:hidden)
        # this returns 1 if it is 0 & vice versa
        not s.item.hidden
      else
        # if the item is not a post or a comment it inserts the stream into the resultset
        true
      end
    end

  end
end

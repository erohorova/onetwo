class PromotionsController < ApplicationController
  include OauthLinkHelper, SavannahAuthentication

  before_action :set_authentication_method, only: [:show]

  def show
    promo           = Promotion.find(params[:id])
    destination_url = promo.url
    oauth_url       = nil

    if group = promo.promotable.is_a?(ResourceGroup) && promo.promotable
      if group && group.organization
        destination_url = group.website_url
        oauth_url = generate_oauth_link(org: promo.promotable.organization, destination_url: destination_url)
      end
    end

    redirect_to oauth_url ? safe_redirect(oauth_url) : safe_redirect(destination_url)
  end
end

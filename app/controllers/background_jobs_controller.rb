class BackgroundJobsController < ApplicationController
  include OrganizationContext
  include LimitOffsetConcern
  before_filter :ensure_default_org
  layout :false

  has_limit_offset_constraints only: [:index]

  skip_before_filter :cm_auth_user
  skip_before_filter :set_organization_context

  before_filter do
    authenticate_user!
    if current_user.id != User.cm_admin.id
      render "errors/not_found", layout: 'errors', status: :not_found and return
    end
  end

  def reenqueue
    job = BackgroundJob.find_by_job_id(params[:id])
    job.re_enqueue
    render json: {status: :success} and return
  end

  def index
    ands = []
    @filters = params.slice(:job_id, :job_class, :job_queue, :status).merge({limit: limit, offset: offset}).with_indifferent_access

    ar_filters = {}
    [:job_id, :job_class, :job_queue, :status].each do |k|
      ar_filters[k] = @filters[k] unless params[k].blank?
    end
    @jobs = BackgroundJob.where(ar_filters).limit(limit).offset(offset).order(enqueued_at: :desc)
  end
end

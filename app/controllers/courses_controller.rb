class CoursesController < ApplicationController
  include OauthLinkHelper, SavannahAuthentication

  before_action :set_authentication_method, only: [:link, :redirect]

  skip_before_action :authenticate_user!
  skip_before_action :check_onboarding_and_redirect

  # This is used to link Destiny's ResourceGroup to Savannah's CourseOffering
  # Also, known as deep-link url
  # Shown on `/home/courses`
  def link
    @group = Group.find(params[:id])

    redirect_to_course
  end

  def redirect
    @group = Group.find_by(client_resource_id: params[:course_id])

    redirect_to_course
  end

  private
    def redirect_to_course
      course_website_url = safe_redirect(@group.website_url)
      auto_enroll = params[:auto_enroll] == 'true'
      if auto_enroll
        if course_website_url.include?('?')
          course_website_url += '&auto_enroll=true'
        else
          course_website_url += '?auto_enroll=true'
        end
      end

      if current_user && @group.organization
        oauth_url = generate_oauth_link(org: @group.organization, destination_url: course_website_url, app_data: @query_values)

        redirect_to oauth_url and return
      end

      redirect_to course_website_url
    end
end

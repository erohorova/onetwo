class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  layout :layout_by_resource
  include IdentityCacheFilter
  include RequestHandler
  include NativeAppDetection
  include DetectPlatform
  include ApplicationConcern
  include AccessLogFilter
  include DeviceCookieFilter
  include OrganizationContext
  include CurrentAppBuildConcern
  include SafeRedirect
  # Prevent CSRF attacks by raising an exception.
  protect_from_forgery with: :exception
  skip_before_filter :verify_authenticity_token, if: :is_native_app?

  before_filter :authenticate_user!, except: :unknown_route
  before_filter :set_cache_headers
  before_action :set_request_store
  before_action :set_paper_trail_whodunnit
  before_filter :check_onboarding_and_redirect

  skip_before_filter :check_onboarding_and_redirect, if: :devise_controller?

  def check_onboarding_and_redirect
    if Settings.features.onboarding_redirect_enabled
      if current_user &&
        current_user.organization_id != Organization.default_org&.id &&
        (!current_user.user_onboarding.nil? && !current_user.user_onboarding.completed?)
        redirect_to '/onboarding' and return
      end
    end
  end

  def self.authorize(action_name, permission)
    before_action -> { authorize_permission(permission) }, only: [action_name]
  end

  def self.is_admin_request?(request)
    (
      request.headers["X-ADMIN"] == "true" ||
      URI.parse(request.referer || '').path.start_with?("/admin/")
    ) ? 1 : 0
  end

  def is_admin_request?
    self.class.is_admin_request? request
  end

  def authorize_permission(permission)
    user_not_authorized unless current_user.authorize?(permission)
  end

  include AuthenticationContext #include this only after devise's authenticate_user!
  protected

  helper_method :layout_by_resource

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :email, :password])
  end

  # persist the ?next=/oauth/authorize&display=popup in all generated URLs
  def default_url_options
    opts = {}
    if devise_controller?
      if params.has_key?(:next) && params[:next].is_a?(String) && params[:next].present?
        opts[:next] = params[:next]
      end
      if params.has_key?(:display) && params[:display].is_a?(String) && params[:display].present?
        opts[:display] = params[:display]
      end
    end
    opts
  end

  def layout_by_resource
    if devise_controller?
      if params[:display] == 'popup' ||
          params[:next] && params[:next].start_with?('/oauth/authorize') #use popup layout for oauth login
        'popup'
      else
        'application_v2'
      end
    else
      if Settings.features.dashboard_layout.enabled && !Settings.features.layout_v4
        if defined?(unauthenticated_actions)
          unauthenticated_actions.include?(action_name.to_sym) ? 'application_v2' : 'application_v3'
        else
          'application_v2'
        end
      elsif !Settings.features.dashboard_layout.enabled && Settings.features.layout_v4
        'application_v4'
      else
        'application_v2'
      end
    end
  end

  def authenticate_admin!
    authenticate_user!
    unless current_user.is_admin? || current_org.is_admin?(current_user)
      respond_to do |f|
        f.html { render 'errors/unauthorized', status: 401 and return }
        f.js   { render json: :no_content, status: :unauthorized and return }
        f.json { render json: :no_content, status: :unauthorized and return }
      end
    end
  end

  def authenticate_admin_or_collaborator!
    authenticate_user!
    @channel = current_org.channels.friendly.find(params[:channel_id] || params[:id])
    unless current_user.is_admin? || current_user.is_admin_user? || @channel.is_author?(current_user)
      respond_to do |f|
        f.html { render 'errors/unauthorized', status: 401 and return }
        f.js   { render js: '', status: :unauthorized and return }
        f.json { render json: {}, status: :unauthorized and return }
      end
    end
  end


  def user_not_authorized

    respond_to do |f|
        f.html { render 'errors/unauthorized', status: 401 and return }
        f.js   { render json: :no_content, status: :unauthorized and return }
        f.json { render json: :no_content, status: :unauthorized and return }
    end
  end

  protected
    def ensure_known_request
      return if request.format.to_sym.present?
      render :nothing => true, :status => 404
    end

    def set_request_store
      payload = {
        user_id:        current_user&.id,
        platform:       get_platform,
        user_agent:     request.headers.env['HTTP_USER_AGENT'],
        platform_version_number: request.headers.env['HTTP_X_VERSION_NUMBER'],
        is_admin_request: ApplicationController.is_admin_request?(request)
      }

      RequestStore.store[:request_metadata] = payload
    end
end

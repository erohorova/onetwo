class ChannelsController < ApplicationController
  include ContentTypeFilter
  include CardSearchHelpers
  include TrackActions

  before_action :authenticate_admin!, only: [:index]
  before_action :read_org_permissions, only: [:create, :index]

  def index
    @channels = Channel.all
    @right_rail_disabled = true
  end
  
  private

    def channel_params
      params.require(:channel).permit(
        :label,
        :description,
        :image,
        :is_private,
        :authors,
        :only_authors_can_post,
        :visible_during_onboarding,
        :mobile_image,
        :auto_follow,
        :content_discovery_freq,
        :keywords)
    end
end

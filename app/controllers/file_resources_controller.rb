class FileResourcesController < ApiController

  # Deprecated
  def create
    @file = FileResource.new(attachment: params[:file], user: current_user)
    if @file.save
      render 'show', status: :created, format: :json
    else
      render json: @file.errors, status: :unprocessable_entity
    end
  end

  api :POST, "file_resources/v1", "Upload a file"
  def create_v1
    @file = FileResource.new(user: current_user)
    data = params[:file]
    if data.nil?
      render json: {'error' => 'file parameter absent'}, status: :unprocessable_entity and return
    end
    @file.attachment = data
    if @file.save
      render 'show', status: :created, format: :json
    else
      render json: @file.errors, status: :unprocessable_entity
    end
  end

  api :GET, "file_resources/:id/"
  param :id, Integer, :desc => 'id of the file object', :required => true
  def show
    @file = FileResource.find(params[:id])
    if @file.user != current_user
      head :unauthorized and return
    end
    render 'show', status: :ok
  end
end
class OrgSsosController < ApplicationController

  before_action :get_org_sso
  before_action :verify_permission

  resource_description do
    api_version 'Deprecated'
    formats ['json']
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, 'org_ssos'
  description <<-EOS
    List of all SSO options for authentication.
    SSO options enabled for current organization followed by other non-enabled SSO options.
  EOS
  def index
    @org_ssos = current_org.org_ssos.order(:position).all
    available_ssos = Sso.all
    @unenabled_ssos = available_ssos.select do |sso|
      !@org_ssos.any? {|org_sso| org_sso.sso_id == sso.id}
    end
    @results = @org_ssos + @unenabled_ssos
  end

  api :POST, 'org_ssos/:id/enable'
  description <<-EOS
    Enable SSO option for current organization. (works with both id and name input)
  EOS
  param :name, String, "Name of SSO option"
  param :id, Integer, "Id of SSO option"
  def enable
    unless @org_sso #request was submit with primary id
      sso = Sso.find_by_name!(params.require(:name))
      @org_sso = OrgSso.find_or_initialize_by(sso_id: sso.id, organization_id: current_org.id)
      @org_sso.position = current_org.org_ssos.count if @org_sso.new_record?
    end

    if @org_sso.update(is_enabled: true)
      render action: :show
    else
      render json: @org_sso.errors, status: :unprocessable_entity
    end
  end

  api :POST, 'org_ssos/:id/disable'
  description <<-EOS
    Disable SSO option for current organization. (works with both id and name input)
  EOS
  param :name, String, "Name of SSO option"
  param :id, Integer, "Id of SSO option"
  def disable
    if @org_sso.update(is_enabled: false)
      render action: :show
    else
      render json: @org_sso.errors, status: :unprocessable_entity
    end
  end

  def update
    if @org_sso.update(org_sso_params)
      render action: :show
    else
      render json: @org_sso.errors, status: :unprocessable_entity
    end
  end

  private
    def get_org_sso
      if params.has_key? :id
        @org_sso = OrgSso.find(params.require(:id))
      end
    end

    def verify_permission
      if params.has_key?(:id) && @org_sso
        head :forbidden unless current_org.is_admin?(current_user) && @org_sso.organization == current_org
      else
        head :forbidden unless current_org.is_admin?(current_user)
      end
    end

    def org_sso_params
      params
        .permit(
          :is_enabled,
          :label_name,
          :oauth_authorize_url,
          :oauth_client_id,
          :oauth_client_secret,
          :oauth_login_url,
          :oauth_scopes,
          :oauth_token_url,
          :oauth_user_info_url,
          :saml_assertion_consumer_service_url,
          :saml_idp_cert,
          :saml_idp_sso_target_url,
          :saml_issuer )
    end
end
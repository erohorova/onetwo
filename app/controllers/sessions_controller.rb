class SessionsController <  ActionController::Base
  include RequestHandler
  include NativeAppDetection
  include AccessLogFilter
  include DetectPlatform
  include OauthLinkHelper
  include RequestIp
  include Devise::Controllers::Rememberable
  include SafeRedirect
  include SessionConcern

  skip_before_filter :check_onboarding_and_redirect
  before_action :read_from_omniauth

  def setup
    if params[:publish_required]
      @oauth_reader.strategy.options[:scope] = "#{Settings.facebook.scope},publish_actions"
    else
      @oauth_reader.strategy.options[:scope] = Settings.facebook.scope
    end
    render text: 'Setup complete', status: 404
  end

  def native_scheme_redirect
    native_scheme = params.require(:native_scheme)
    redirect_to safe_redirect(native_scheme) + Base64.urlsafe_encode64(request.headers['HTTP_COOKIE'])
  end

  #the saml post back also goes here because it uses the HTTP POST method instead of HTTP GET
  def create_mobile_session
    unless @oauth_reader.auth && @oauth_reader.auth.info
      head :unprocessable_entity and return
    end

    # Mobile does not have encrypted current_host in the omniauth params, mobile clients directly call ge.edcast.com/auth/facebook_access_token/callback
    @host = request.host
    status = create #defer to the web version of the create action
    unless false == status # do not do anything if the create action returned false
      if ['facebook_access_token', 'linkedin_access_token'].include? params[:token_provider] #this is really a hack: mobile expects json even though they do not set application type to json.
        render 'users/info', formats: [:json] and return
      else
        render action: :create and return
      end
    end
  end

  def create
    omniauth_data = @oauth_reader.auth
    omniauth_data && omniauth_data.merge!(current_platform: get_platform)

    if session['devise.impersonatee'].present? || session['devise.impersonator'].present?
      render inline: 'Social connection not allowed while impersonating someone else!', status: :forbidden
      return false
    end

    if @oauth_reader.params && @oauth_reader.params.current_host
      current_host = CustomCipher.new.decrypt(context: @oauth_reader.params.current_host)
      @host = current_host
    end

    redirect_uri = nil

    # `redirect_uri` is set when user is authenticated via Savannah.
    # Use-case: Savannah relies on destiny to authenticate Users.
    # Once user is authenticated, redirect back to Savannah once iteration is completed.
    if @oauth_reader.params.present?
      redirect_uri = @oauth_reader.params.redirect_uri
      provider_name = @oauth_reader.params.provider
    end

    # `current_host` is passed in from Savannah
    # Use-case: This helps to modify user for an app.
    # Due to this, we add different users with same email across different orgs
    # If no host available, fall back to default org (i.e. `www`)
    if @host
      @current_org = Organization.find_by_request_host(@host) || Organization.default_org
    else
      @current_org = Organization.default_org
    end

    email = @oauth_reader.auth.info.email
    uid   = omniauth_data[:uid]
    provider = omniauth_data[:provider].gsub('_access_token','')

    # Check if user already exists for following cases:
    # 1. user with email exists?
    # 2. user with invitation record exists?
    # 3. user with blank email but with identity provider record exists?
    user_exists = User.already_exists?(organization: @current_org, email: email, uid: uid, provider: provider)
    omniauth_params = request.env['omniauth.params']

    unless (user_exists || @current_org.email_registrable?(email))
      log.warn "Social user attempted to sign up for a closed organization, email=#{email} organization_id=#{@current_org.id}"
      respond_to do |format|
        format.json {
          head :forbidden
        }
        format.js {
          head :forbidden
        }
        format.html {
          @target_url =  if omniauth_params && omniauth_params['origin'].present? && omniauth_params['widget']&.to_bool
            redirect_widget_uri = omniauth_params['origin']
            error_message = 'The user is not registered'
            @widget = true
            redirect_widget_uri += '?' + { error: error_message }.to_param
            redirect_widget_uri
          else
            @should_close_tab = !(omniauth_params && omniauth_params['close_tab'] == 'false')
            @error_code = 'invite_only'
            new_user_session_url(host: @current_org.host, error_code: 'invite_only')
          end
        }
      end

      return false
    end

    # Make sure this has enabled this sso
    org_sso = get_org_sso(provider: provider)

    # Allow linking use case to go through
    unless (org_sso && org_sso.is_enabled) || (@oauth_reader.params.user_id.present?)
      log.warn("Attempted to login with a disabled sso option: organization=#{@current_org.id} provider=#{provider}")
      render inline: 'Not a valid sign in option', status: :forbidden
      return false
    end

    # Forum use case
    user_aggregator = UserInfo::Aggregator.new(
      organization: @current_org,
      user_id:      @oauth_reader.params.user_id,
      user_info:    omniauth_data.merge!(provider_name: provider_name) )

    user_aggregator.aggregate

    if user_aggregator.error.present?
      render inline: user_aggregator.error, status: :forbidden
      return false
    end
    @current_user = user_aggregator.user  #need this for access log to work
    authenticable = user_aggregator.authenticable

    @current_user.set_time_zone(ip_address: parse_user_ip) if @current_user.time_zone.blank?
    if @oauth_reader.params.terms_accepted&.to_bool
      @current_user.set_terms_accepted unless @current_user.profile&.tac_accepted?
    end

    if authenticable #sign the user in
      if user_aggregator.complete_registration
        @access_uri = "login.#{provider}"
      else
        @access_uri = "signup.#{provider}"
      end
    end



    session[:access_uri] = @access_uri

    redirect_uri = @oauth_reader.params.redirect_uri

    # if the origin data is present in the omniauth params then redirect it to origin
    if omniauth_params && omniauth_params['origin']
      redirect_uri = omniauth_params['origin']
      if omniauth_params['widget'] == 'true'
        payload = {
          jwtToken: @current_user.jwt_token({ is_web: (get_platform == 'web') }),
          firstname: @current_user.first_name,
          lastname: @current_user.last_name
        }
        redirect_uri += '?' + payload.to_query
      end
    end

    log.info "Printing SAML response here =================================================="
    log.info "Params: #{params}"
    log.info "omniauth.auth: #{omniauth_data}"
    log.info "Omniauth.params: #{request.env['omniauth.params']}"
    log.info "Provider: #{provider}"
    log.info "App Data: #{collate_app_data}"
    log.info "redirect_uri: #{redirect_uri}"
    log.info "=============================================================================="
    if @oauth_reader.params.current_host && current_host != Settings.platform_host && current_host != request.host_with_port
      redirect_to auth_finish_url(
        host: current_host,
        user_id: CustomCipher.new.encrypt(context: @current_user.id.to_s),
        authenticable: authenticable, provider: provider,
        cb: @oauth_reader.params.cb,
        close_tab: omniauth_params && omniauth_params['close_tab'],
        native_scheme: @oauth_reader.params.native_scheme,
        redirect_uri: redirect_uri, app_data: collate_app_data,
        provider_type: provider_name
      ) and return
    else
      should_close_tab = ((provider == Settings.okta.internal_sso) ? false : !(omniauth_params && omniauth_params['close_tab'] == 'false'))
      do_after_auth(
        user: @current_user, authenticable: authenticable, provider: provider,
        cb: @oauth_reader.params.cb,
        should_close_tab: should_close_tab,
        redirect_uri: redirect_uri, app_data: collate_app_data,
        provider_type: provider_name)
    end

    # Use Case: For SSO login through web view initiated by the mobile app
    if native_scheme = (@oauth_reader.params.native_scheme)
      redirect_to native_scheme_redirect_path(native_scheme: native_scheme) and return false
    end

    record_logged_in_event(org: @current_org, user: @current_user)
    # Use Case : Bookmarked URL (example: /auth/saml?next=/courses/:id/link)
    # This is to redirect immediately once user is authenticated
    # For this, URL must have `next` query param
    omniauth_raw_info = collate_app_data
    if redirect_destination = (@oauth_reader.params.next)
      callback_url = UrlBuilder.build(redirect_destination, collate_app_data)
      redirect_to callback_url
      return false
    end
  end

  #put the user on the same domain as the org then sign him in
  def finish
    user_id = CustomCipher.new.decrypt(context: params.require(:user_id))
    user = User.find(user_id)
    do_after_auth(
      user: user,
      authenticable: true,
      provider: params[:provider],
      cb: params[:cb],
      redirect_uri: params[:redirect_uri],
      app_data: params[:app_data],
      should_close_tab: !(params[:close_tab] == 'false'),
      provider_type: params[:provider_type])

    if params[:native_scheme].present?
      # Use Case: For SSO login through web view initiated by the mobile app
      redirect_to native_scheme_redirect_path(native_scheme: params[:native_scheme]) and return
    else
      render action: :create and return
    end
  end

  def failure
    @error_code = env['omniauth.error'].try(:code) || env['omniauth.error'].try(:error)
    @error_description = env['omniauth.error'].try(:description) || env['omniauth.error'].try(:error_reason)
    log.error "Omniauth failure: code=#{@error_code} description=#{@error_description}"
    render status: :unprocessable_entity
  end

  protected

    def app_data
      if request.env['omniauth.params'] && !request.env['omniauth.params']['app_data'].blank?
        return Hash[URI.decode_www_form(request.env["omniauth.params"]["app_data"])] || {}
      elsif params["app_data"]
        return Hash[URI.decode_www_form(params["app_data"])] || {}
      end
      {}
    end

    ############ Saml response within raw_info ################
    # {
    #   'first_name' => ['John'],
    #   'last_name'  => ['Doe'],
    #   'IdentityType' => ['P,C', 'E'],
    #   'country' => ['IN'],
    #   'Industry Group' => ['Ge Service'],
    #   'Preferred Name' => ['Service']
    # }
    def collate_app_data
      # forcefully converts keys
      auth       = @oauth_reader.auth.deep_symbolize_keys
      raw_info   = app_data
      raw_info['external_uid'] = auth[:uid]

      raw_info
    end

    def do_after_auth(user:, authenticable:, provider:, cb:, redirect_uri:, app_data: {}, should_close_tab:, provider_type: nil)
      warden.set_user(user)
      @current_user = user
      @user = user
      @provider = provider
      @cb = cb
      @should_close_tab = should_close_tab
      if redirect_uri.present?
        #handle savannah redirect
        @target_url = generate_oauth_link(org: user.organization, destination_url: redirect_uri, reuse_query: true, app_data: app_data)
      else #edcast use case
        @target_url = session[:user_return_to].blank? ? root_url : session.delete(:user_return_to) #remove this from the session as well
      end

      if provider_type && Settings.okta.provision.split(',').include?(provider_type)
        args = { sso_id: app_data['external_uid'] }
        @target_url = @current_user.provision_and_sign_in(args: args, redirect_url: @target_url, is_sso_user: true)
      end

      if authenticable
        bypass_sign_in user

        @current_user.update(sign_out_at: nil)
        # set first_sign_in cookie
        response.set_cookie("first_sign_in", {value: true, path: "/"}) if(user.sign_in_count == 1)
        if !@current_user.organization.expire_session_on_closing_browser
          remember_me(user) # remember user session default (2 weeks)
        end
        # reference: https://github.com/plataformatec/devise/wiki/Omniauthable,-sign-out-action-and-rememberable

        user_session[:authentication_method] = provider.to_sym
      end

      # accept invitation incase if session[:invitation_id] exist
      if session[:invitation_id].present?
        @user.accept_invitation(invitation_id: session[:invitation_id])
        session.delete(:invitation_id)
      end

      # clear all the flash messages
      flash.clear
      @authenticity_token = form_authenticity_token
    end

    def get_org_sso(provider:)
      # if - provider is lxp_oauth there can be multiple saml and social logins.
      # else - legacy ssos.
      if provider == 'lxp_oauth'
        org_sso_id = JwtAuth.decode(@oauth_reader.params.connector)
        org_sso = @current_org.org_ssos.find(org_sso_id)
      else
        org_sso = @current_org.org_ssos.by_name(name: provider).first
      end
      org_sso
    end

    def read_from_omniauth
      @oauth_reader = OmniauthReader.new(environment: request.env)
      @oauth_reader.read
    end
end

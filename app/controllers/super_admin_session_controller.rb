class SuperAdminSessionController < ApplicationController
  skip_before_action :verify_authenticity_token, only: :create
  before_action :ensure_super_admin_user, only: [:new, :submit]
  skip_before_action :authenticate_user!, only: [:create]
  skip_before_action :set_organization_context, only: [:create]


  def new

  end

  def submit
    host_name = params[:host_name]
    if host_name && @organization = Organization.find_by_host_name(host_name)
      exp = Time.now.to_i + 10 #must be redeemed within 10 seconds
      exp_payload = { :data => ActiveSupport::JSON.encode({'organization_id' => @organization.id, 'user_id' => current_user.id}),
                      :exp => exp }
      @admin_jwt_token = JWT.encode(exp_payload, Edcast::Application.config.secret_key_base)
      render layout: false
    else
      @error = "Cannot find team with '#{host_name}' host name"
      render action: :new
    end
  end

  def create
    begin
      decoded_token, header = JWT.decode(params.require(:admin_jwt_token), Edcast::Application.config.secret_key_base)
      decoded_data = ActiveSupport::JSON.decode(decoded_token['data'])
      organization = Organization.find(decoded_data['organization_id'])
      admin_user = User.where(organization: organization, is_edcast_admin: true, organization_role: 'admin').first!
      admin_user.confirm unless admin_user.confirmed?
      bypass_sign_in admin_user
      admin_user.allow_sign_in!
      log.info("SuperAdmin: user_id=#{decoded_data['user_id']} became admin user for organization_id=#{organization.id}")
      SuperAdminAuditLog.create!(user_id: decoded_data['user_id'], organization_id: organization.id, ip: request.ip, user_agent: request.user_agent)
      redirect_to root_path
    rescue JWT::ExpiredSignature => e
      log.warn("Invalid super admin request. error=#{e} message=#{e.message}")
      redirect_to not_found_path
    end
  end

  private

  def ensure_super_admin_user
    unless current_user.is_admin?
      redirect_to not_found_path
    end
  end

end

class SavannahApi::AppConfigsController < SavannahApi::BaseController
  def save
    config = @current_org.configs.new @jwt_payload
    if config.save
      head :ok and return
    else
      log.error "Unable to save instructor mailer config with params: #{@jwt_payload} error: #{config.errors.full_messages}"
      head :error and return
    end
  end

  def destroy
    config = @current_org.configs.find_by(name: @jwt_payload[:name], configable_id: @current_org.id)
    if config.destroy
      head :ok and return
    else
      log.error "Unable to destroy instructor mailer config with client ID: #{@current_org.client.id} error: #{config.errors.full_messages}"
      head :error and return
    end
  end
end

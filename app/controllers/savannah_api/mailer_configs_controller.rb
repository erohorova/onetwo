class SavannahApi::MailerConfigsController < SavannahApi::BaseController
  def destroy
    mailer_config = MailerConfig.find_by(organization: @current_org)
    if mailer_config && mailer_config.destroy
      status = :ok
    else
      log.error "Unable to delete mailer config with id: #{mailer_config.id} error: #{mailer_config.full_errors}"
      status = :unprocessable_entity
    end

    head status and return
  end

  # This handles both Create and Update activities from Savannah
  def save
    mailer_config = MailerConfig.where(organization: @current_org).first_or_initialize
    mailer_config.assign_attributes(mailer_config_params)

    if mailer_config.save
      status = :ok
    else
      log.error "Unable to save mailer config id #{mailer_config.id} with params: #{mailer_config_params} error: #{mailer_config.full_errors}"
      status = :unprocessable_entity
    end

    head status and return
  end

  private
  def mailer_config_params
    @jwt_payload.except(:app_id)
  end
end

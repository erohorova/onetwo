class SavannahApi::BaseController < ActionController::Base
  include NativeAppDetection
  include DetectPlatform
  include SavannahAuthentication

  before_filter :authenticate_from_savannah!
  # Prevent CSRF attacks
  protect_from_forgery with: :null_session
end

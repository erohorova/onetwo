class ActivityRecordsController < ApiController

  api :POST, "groups/:group_id/activity_records", "Create an activity entry"
  formats ['json']
  error 422, "Unprocessable"
  description "Sends 201 Created on Success"
  def create
    date = ActivityRecord.get_current_time_utc.strftime("%Y-%m-%d")
    if ActivityRecord.create_with_id(user_id: current_user.id, group_id: params.require(:group_id),
                                     date: date, type: params.require(:type))
      head :created
    else
      head :unprocessable_entity
    end
  end

end

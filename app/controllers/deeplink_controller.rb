class DeeplinkController < ActionController::Base
  # Prevent CSRF attacks
  protect_from_forgery with: :null_session

  def index
    # Get the user
    user_context = params[:user_context]
    decoded_user_context, headers = JWT.decode(user_context, Edcast::Application.config.secret_key_base)
    decoded_user_context = decoded_user_context['data']

    # Log him in
    organization = Organization.find_by!(host_name: decoded_user_context['hostname'])
    user = User.find_by!(email: decoded_user_context['email'], organization: organization)
    bypass_sign_in user

    # Redirect to root, whether subdomain or vanity
    redirect_to '/'
  end
end

class RegistrationsController < Devise::RegistrationsController
  include OrganizationContext
  skip_before_filter :verify_authenticity_token
  before_filter :perform_auth
  before_filter :ensure_open_registration, only: :create, if: :non_savannah_request?

  def create
    user_params = sign_up_params.merge!({ password: User.random_password, password_reset_required: true })

    klass = UserInfo::Alliance.new(organization: current_org, user_params: user_params, opts: { platform: get_platform, skip_confirmation_email: !@confirm })
    resource = klass.connect

    if klass.errors.empty?
      yield resource if block_given?

      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_flashing_format?
        sign_up(resource_name, resource)
        respond_to do |format|
          format.json { render json: resource.capsule_data, status: :ok and return }
          format.js {
            @authenticity_token = form_authenticity_token
            render action: :create and return
          }
          format.html {
            respond_with resource, location: after_sign_up_path_for(resource)
          }
        end
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_flashing_format?
        expire_data_after_sign_in!
        respond_to do |format|
          format.json { render json: resource.capsule_data, status: :ok and return }
          format.js {
            @authenticity_token = form_authenticity_token
            render action: :create and return
          }
          format.html {
            respond_with resource, location: after_inactive_sign_up_path_for(resource)
          }
        end
      end

    else
      clean_up_passwords resource
      user = User.where(email: user_params[:email], organization_id: current_org.id).first
      if user.present? && !user.is_suspended?
        respond_to do |format|
          format.json { render json: { message: klass.message } , status: :unprocessable_entity and return }
          format.js
          format.html {
            respond_with user and return
          }
        end
      else
        @user_suspended = user && user.is_suspended?
        respond_to do |format|
          format.js {
            @authenticity_token = form_authenticity_token
            render action: :create, status: :unprocessable_entity and return
          }
          format.json {
            head :bad_request and return
          }
          format.html {
            render action: :create, status: :unprocessable_entity and return
          }
        end
      end
    end
  end

  private
  def perform_auth
    @confirm = true
    if params[:api_key].present?
      @confirm = false
      validate_api_with_api_key
    elsif params[:savannah_app_id].present?
      @confirm = false
      validate_api_with_savannah_app_id
    elsif params[:from_edc_web].present?
      ensure_edc_web?
    elsif !is_native_app?
      verify_authenticity_token
    end
  end

  #TODO deprecated
  def validate_api_with_api_key
    api_key = params.require(:api_key)
    user_email = params[:email]
    token = params.require(:token)
    cred, client, organization = CacheFactory.fetch_credential_and_client(api_key: params[:api_key])

    if cred.nil?
      head :unauthorized and return
    end

    secret = cred.shared_secret
    parts = [secret, user_email, 'user'].compact
    calc_token = Digest::SHA256.hexdigest(parts.join('|'))

    unless calc_token == token
      head :unauthorized and return
    end
  end

  def validate_api_with_savannah_app_id
    user_email = params[:email]
    savannah_app_id = params[:savannah_app_id]
    auth_token = params.require(:auth_token)

    parts = [savannah_app_id, user_email, 'user'].compact
    calc_token = Digest::SHA256.hexdigest(parts.join('|'))

    unless calc_token == auth_token
      head :unauthorized and return
    end
  end

  def non_savannah_request?
    !params.has_key?(:savannah_app_id)
  end

  def ensure_edc_web?
    unless params[:from_edc_web] == 'true'
      head :unauthorized and return
    end
  end
end

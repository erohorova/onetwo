class ReportsController < ApiController

  before_action :set_reportable, :only => [:create, :stats]

  api :POST, "groups/:group_id/posts/:post_id/reports", "Report for a post"
  api :POST, "posts/:post_id/comments/:comment_id/reports", "Report for a comment"
  api :POST, "posts/:post_id/reports", "Report for a Post"
  api :POST, "comments/:comment_id/reports", "Report for a Comment"
  param :type, String, :desc => 'type of report, can be one of \'spam\', \'inappropriate\' or \'compromised\''
  formats ['json']
  error 422, "Unprocessable"
  description "Sends 201 Created on Success"
  def create
    @report = Report.new
    @report.reportable = @reportable
    @report.reporter = current_user

    case params[:type]
    when 'spam'
      @report.type = 'SpamReport'
    when 'inappropriate'
      @report.type = 'InappropriateReport'
    when 'compromised'
      @report.type = 'CompromisedReport'
    else
      render json: {'error' => 'bad type parameter'}, status: :unprocessable_entity
      return
    end
    
    if @report.save
      head :created
    else
      render json: @report.errors, status: :unprocessable_entity
    end
  end

  api :GET, "groups/:group_id/questions/:question_id/reports/stats", "Report stats for a question"
  api :GET, "questions/:question_id/answers/:answer_id/reports/stats", "Report stats for a question"
  api :GET, "questions/:question_id/reports/stats", "Report stats for a question"
  api :GET, "posts/:post_id/reports/stats", "Report stats for a post"
  api :GET, "answers/:answer_id/reports/stats", "Report stats for an answer"
  api :GET, "comments/:comment_id/reports/stats", "Report stats for a comment"
  formats ['json']
  def stats
    @reports = Report.where(:reportable => @reportable, :validity => true)
  end

  private
  def set_reportable
    if params.key? 'comment_id'
      @reportable = Comment.find(params['comment_id'])
    elsif params.key? 'post_id'
      @reportable = Post.find(params['post_id'])
    end
  end
end


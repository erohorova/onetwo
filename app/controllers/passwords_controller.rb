class PasswordsController < Devise::PasswordsController
  include OktaHelpers
  before_filter :user_suspended?, only: [:create]
  skip_before_action :verify_authenticity_token, only: [:create, :update], if: :is_native_app?
  before_action :ensure_known_request

  def new
    self.resource = resource_class.new
  end

  # POST /resource/password
  def create
    self.resource = resource_class.send_reset_password_instructions(resource_params)
    self.resource.update password_reset_required: true
    if successfully_sent?(resource)
      respond_to do |format|
        format.json {
          head :ok and return
        }
        format.html {
          # show flash alert in edc-web,using querystring sent_reset_password_mail=true
          flash.clear
          respond_with({}, location: after_sending_reset_password_instructions_path_for(resource_name))
        }
      end
    else
      respond_to do |format|
        format.json {
          head :ok and return
        }
        format.html {
          respond_with(resource)
        }
      end
    end
  end

  # GET /resource/password/edit?reset_password_token=abcdef
  # TODO:If user doesn't have mobile app, user won't be able to update the password
  # disable `edit` for now

  # def edit
    # if is_mobile_browser?
    #   redirect_to "edcastapp://auth/password_reset?reset_password_token=#{params[:reset_password_token]}" and return
    # else
    #   super
    # end
  # end

  # PUT /resource/password
  # set first_sign_in cookie (for bulk import user login)
  def update
    # OKTA: Try to update password to okta
    User.update_password_to_okta(resource_params)
    self.resource = resource_class.reset_password_by_token(resource_params)
    yield resource if block_given?

    if resource.errors.empty?
      resource.unlock_access! if unlockable?(resource)
      if Devise.sign_in_after_reset_password
        respond_to do |format|
          format.html {
            flash_message = resource.active_for_authentication? ? :updated : :updated_not_active
            set_flash_message(:notice, flash_message)
            sign_in(resource_name, resource)
            response.set_cookie("first_sign_in", {value: true, path: "/"}) if(resource.sign_in_count == 1)
            respond_with resource, location: after_resetting_password_path_for(resource)
          }

          format.json {
            if resource.organization.okta_enabled?
              @user = resource
              user_data = JSON.parse(render_to_string(template: 'users/info.json.jbuilder'))
              redirect_url = is_mobile_browser? ?
                              native_scheme_redirect_url(native_scheme: current_org.okta_native_redirect) :
                              root_url
              user_data[:consumer_redirect_uri] = User.url_to_federated_service(details: {
                                                                                            "password" => Base64.encode64(resource_params[:password]),
                                                                                            'federated_identifier' => resource.federated_identifier,
                                                                                            "redirect_url" => redirect_url
                                                                                          },
                                                                                organization: resource.organization)
              render json: user_data and return
            else
              render json: {jwtToken: resource.jwt_token, email: resource.email} and return
            end
          }
        end
      else
        respond_to do |format|
          format.html {
            set_flash_message(:notice, :updated_not_active)
            respond_with resource, location: after_resetting_password_path_for(resource)
          }

          format.json {
            render json: {message: 'Please sign in with your email and updated password'}
          }
        end
      end
    else
      respond_to do |format|
        format.html {
          set_minimum_password_length
          respond_with resource
        }

        format.json {
          render json: {message: resource.errors.full_messages.join(", ")} and return
        }
      end
    end
  end

private

  def user_suspended?
    user = User.where(email: params[:user][:email], organization_id: params[:user][:organization_id]).first
    if user && user.is_suspended?
      respond_to do |format|
        format.json {
          head :bad_request and return
        }
        format.html {
          flash[:error] = 'Invalid Request. Your account has been suspended. Please contact your team or edcast administrator for further details.'
          redirect_to :back
          return
        }
      end
    end
  end

  def after_sending_reset_password_instructions_path_for(resource_name)
    new_session_path(resource_name, sent_reset_password_mail: true) if is_navigational_format?
  end
end

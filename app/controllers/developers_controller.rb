class DevelopersController < ApplicationController
  layout 'application_v2'
  skip_before_filter :authenticate_user!, only: [:index, :features, :docs, :join, :admin_create]

  def dev_redir
    redirect_to dev_home_path
  end


  def invite
    @clients = current_user.administered_clients
  end

  def invitation
    email = params["email"]

    @client = Client.find(params['client_id'])
    unless @client.admins.include?(current_user)
      redirect_to dev_manage_path and return
    end

    invite = DeveloperAdminInvitation.new(recipient_email: email,
                                          sender: current_user,
                                          first_name: params["first_name"],
                                          last_name: params["last_name"],
                                          client_id: params["client_id"])
    if invite.save
      flash.now[:alert] = "You have invited #{email}"
      redirect_to dev_manage_path
    else
      flash.now[:alert] = "You have FAILED to invite #{email}"
      render action: "invite"
    end

  end

  def join
    @token = params[:token]
    @invitation = DeveloperAdminInvitation.find_by_token(@token)
    if @invitation.nil?
      log.error "Invalid landing token: #{@token}"
      head :unauthorized and return
    end

    recipient = @invitation.recipient
    if recipient
      log.debug "developer invitation has recipient"

      assign_client_admin_relationship(recipient, @invitation.client)
      bypass_sign_in recipient
      redirect_to dev_manage_path
    end
  end

  def admin_create
    user = User.new(user_params)
    if user.save
      client = Client.find params["client_id"]
      user.administered_clients << client
      redirect_to dev_manage_path and return
    else
      log.error "Developer admin user not created."
      flash[:error] = "There was a problem."
    end
  end

  def user_params
     params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation)
  end

  private

  def assign_client_admin_relationship(user, client)
    if user.administered_clients.include?(client)
      flash[:notice] = "You are already an adminstrator of client: #{client.name}"
    else
      log.debug "developer: existing user: add admin relationship"
      begin
        user.administered_clients << client
      rescue ActiveRecord::RecordInvalid => invalid
        # ignore duplicate join
        log.debug "** user is already an admin"
      end
      flash[:notice] = "You are now an administrator of client: #{client.name}"
    end
  end

end

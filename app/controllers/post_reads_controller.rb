class PostReadsController < ApiController

  before_action :set_readable, only: [:create]

  api :POST, "posts/:post_id/read", "Read a Post"
  formats ['json']
  error 422, "Unprocessable"
  description "Sends 201 Created on Success"
  def create
    id = PostRead.make_primary_key(post:@readable, user: current_user)

    #this query avoid logging 404 error
    @read = PostRead.search(query: {ids: {values: [id]}}).first

    if @read
      #update it
      @read.touch #just update the last read time
    else
      @read = PostRead.create(id: id, #prevent duplicate read by manually constructing the primary key
                              group_id: @readable.group_id,
                              user_id: current_user.id,
                              post_id: @readable.id)
    end

    if (@read)
      head :created
    else
      head :unprocessable_entity
    end
  end

  private
  def set_readable
    # either post or question
    if params.key?(:post_id)
      @readable = Post.find(params[:post_id])
    end
  end
end


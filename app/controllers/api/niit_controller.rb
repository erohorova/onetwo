class Api::NiitController < ApiController
  skip_before_filter :cm_auth_user

  api :GET, 'api/niit/launch', 'redirect to NIIT launch url'
  param :course_id, String, desc: "NIIT course id", required: true
  param :course_type, Integer, desc: "NIIT course type", required: true
  param :domain, String, desc: "Domain", required: false
  param :language, String, desc: 'Language of the course', required: false

  def launch
    niit_service = NiitService.new(
      user:            current_user,
      domain:          niit_course_params[:domain] || current_org.host_name,
      course_id:       niit_course_params[:course_id],
      course_type:     niit_course_params[:course_type],
      language:        niit_course_params[:language]
    )

    deeplink_url = niit_service.fetch_deeplink_url

    if deeplink_url.present?
      redirect_to deeplink_url
    else
      @error = "Niit course deeplink generation failed for course_id: #{niit_course_params[:course_id]}, course_type: #{niit_course_params[:course_type]}, please try again after sometime."
      render "api/error", status: 404
    end
  end

  protected

  def niit_course_params
    params.permit(:course_type, :course_id, :domain, :language)
  end
end

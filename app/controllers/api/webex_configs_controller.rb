class Api::WebexConfigsController < ApiController
  include SavannahAuthentication
  before_action :authenticate_from_savannah!
  skip_filter :cm_auth_user
  def create
    webex_config = WebexConfig.find_or_initialize_by(organization_id: @current_org.id)
    if webex_config.update(webex_config_params)
      head :ok and return
    else
      log.error "Unable to save mailer config with params: #{webex_config_params} error: #{webex_config.errors.full_messages}"
      head :error and return
    end
  end

  def get_webex_credentials
    @webex_config = @current_org.webex_config
  end
  
  private
  def webex_config_params
    @jwt_payload.except(:app_id)
  end
end

# DEPRECATED
class Api::EmailTestController < ApiController
  include OrganizationContext

  def weekly_activity
    score_stats = UserLevelMetric.fetch_stats_for_weekly_activity_email(organization_id: current_org.id, user_id: current_user.id)
    UserMailer.weekly_activity(current_org, [ current_user ], current_org.weekly_activity_mail_attributes , score_stats ).deliver_later
    head :ok
  end

end

class Api::OrganizationsController < ApiController
  include OrganizationContext
  include EmptyAttachmentPreProcessor
  include LimitOffsetConcern
  include OrganizationConcern

  skip_before_filter :set_organization_context, only: [:show, :create_from_savannah]
  skip_before_filter :cm_auth_user, only: [:show, :create_org_with_admin, :create_from_savannah]
  skip_before_action :authenticate_user!, only: [:host_name_check, :create_org_with_admin, :create_from_savannah]
  before_action :authenticate_cm_admin!, only: :create_from_savannah
  before_action :org_admin_check, only: [:settings, :update]

  has_limit_offset_constraints only: [:index]

  api :GET, 'api/organizations', 'Get all orgs in edcast'
  param :limit, Integer, desc: 'Limit, default 15', required: false
  param :offset, Integer, desc: 'Offset, default 0', required: false
  formats ['json']
  def index
    head :unprocessable_entity and return unless current_user.is_admin?
    @orgs = Organization.exclude_default_org.limit(limit).offset(offset)
    render json: { organizations: @orgs }
  end

  api :GET, 'api/organizations/settings', 'Get org details for edit form'
  def settings; end

  api :PUT, 'api/organizations/settings', 'Update org changes'
  param :organization, Hash, desc: 'org parameters', required: true do
    param :name, String, desc: 'Name of new org', required: true
    param :description, String, desc: 'Description for new org', required: true
    param :is_registrable, String, desc: 'Allow open registration or invite only', required: false
    param :image, String, desc: 'Banner image', required: false
    param :mobile_image, String, desc: 'Banner image for mobile display', required: false
    param :splash_image, String, desc: 'Splash image', required: false
    param :co_branding_logo, String, desc: 'Co-Branding logo for an Org', required: false
    param :show_sub_brand_on_login, :bool, desc: 'Show or hide the Sub-Brand image on Login page', required: false
    param :configs_attributes, Hash, desc: 'config settings for organization', required: false
  end
  formats ['json']
  def update
    org_attrs = prepare_organization_params(organization_params)
    @current_org.update(org_attrs.except(:host_name))
  end

  api :post, 'api/organizations/setup'
  param :organization, Hash, desc: 'org parameters', required: true do
    param :name, String, desc: 'Name of new org', required: true
    param :host_name, String, desc: 'Host name for new org', required: true
    param :description, String, desc: 'Description for new org', required: false
  end
  param :user, Hash, desc: 'org parameters', required: true do
    param :first_name, String, desc: 'First name of the user', required: true
    param :last_name, String, desc: 'Last name of the user', required: true
    param :email, String, desc: 'A valid email for the user', required: true
    param :password, String, desc: 'Minimum 8 character length password', required: true
    param :handle, String, desc: 'User handle', required: true
  end
  formats ['json']
  def create_org_with_admin
    # Request for team(aka org) will go through below steps
    # 1. create org with BD as one of the admins
    # 2. Send an email to staff

    org_params = params.require(:organization).permit(:name, :description, :host_name)
    user_params = params.require(:user).permit(:email, :password, :first_name, :last_name, :handle)

    org = nil
    org_admin = nil
    begin
      org = create_organization org_params
      org_admin = create_admin_user user_params, org
    rescue ActiveRecord::RecordInvalid
      head :unprocessable_entity and return
    end

    SelfServeMailer.notify_staff(org, org_admin, params[:user][:phone_number]).deliver_later
    head :ok
  end

  api :post, 'api/organizations/create_from_savannah'
  param :organization, Hash, desc: 'org parameters', required: true do
    param :name, String, desc: 'Name of new org', required: true
    param :host_name, String, desc: 'Host name for new org', required: true
    param :description, String, desc: 'Description for new org', required: false
    param :savannah_app_id, Integer, desc: 'Savannah app is', required: false
    param :redirect_uri, String, desc: 'Savannah app redirect URI', required: false
  end
  def create_from_savannah
    org_host_name = params[:organization].try(:[], :host_name)
    if org_host_name
      @organization = Organization.find_by_host_name(org_host_name)
      if @organization && params[:organization]
        pars = {savannah_app_id: params[:organization][:savannah_app_id], redirect_uri: params[:organization][:redirect_uri]}
        @organization.update_attributes(pars)
      else
        org_attrs = prepare_organization_params(organization_params)
        @organization = Organization.new org_attrs
        if @organization.save
          current_user.clone_for_org(@organization, 'admin')
        else
          head :unprocessable_entity and return
        end
      end
      render 'show'
    else
      head :bad_request and return
    end
  end

  api :GET, 'api/org_details', 'Get org details by host name'
  param :host_name, String, desc: 'host name to check. If calling www.edcast.com, use this param. If making a request to a custom domain (ge.edcast.com), then the host_name parameter is ignored', required: true
  formats ['json']
  def show
    set_current_org_without_redirects

    @organization = current_org

    if @current_org.id == Organization.default_org&.id
      @organization = Organization.find_by(host_name: params[:host_name])
    end

    head :not_found and return if @organization.nil?
  end

  api :PUT, 'api/org_settings', 'Update org settings'
  param :setting, Hash, desc: 'update org settings', required: true do
    param :id, String, desc: 'id of the config. if id is not provided, then will create a new one'
    param :name, String, desc: 'name of the config. if name is provided, then will create a new config with the given name'
    param :value, String, desc: 'setting value, boolean, string or matching setting value data type'
    param :category, String, desc: 'setting category. default to "settings" if not provided'
  end
  formats ['json']
  def set_org_settings
    head :forbidden and return unless @current_org.is_admin?(current_user)
    pars = params.require(:setting).permit(:id, :value, :name, :category)
    @config = save_org_settings pars
  end

  api :GET, 'api/org_settings', 'Get org settings'
  param :category, String, desc: 'setting category. default to "settings" if not provided'
  formats ['json']
  def read_org_settings
    head :forbidden and return unless @current_org.is_admin?(current_user)
    @configs = @current_org.configs.where(category: params.fetch('category', 'settings'))
  end

  api :GET, 'api/organizations/host_name_check', 'Host name availability check'
  param :host_name, String, desc: 'host name to check', required: true
  formats ['json']
  def host_name_check
    host_name = params.require(:host_name)
    another_org_exists = Organization.where(host_name: host_name).any?
    if Settings.disallowed_hosts.include?(host_name) || another_org_exists || !Organization.valid_hostname?(host_name)
      if another_org_exists
        head :unprocessable_entity and return
      else
        head :not_acceptable and return
      end
    else
      head :no_content and return
    end
  end

  api :GET, 'api/organizations/activate', 'Activate an org, only Edcast self serve admin can activate it'
  formats ['json']
  def activate_org
    head :unprocessable_entity and return if (!current_user.is_edcast_admin || current_org.status != 'pending')
    current_org.update status: 'active'
    admin_user = current_org.real_admins.where.not(id: current_user.id).first

    message = if admin_user.update(is_active: true) && current_org.status == 'active'
      # updaet users whose status is not active
      current_org.members.where(is_active: false).each {|user| user.update_with_reindex_async(is_active: true)}
      SelfServeMailer.notify_org_admin(current_org, admin_user).deliver_later
      "#{current_org.name} is now active"
    else
      "#{current_org.name} can not be activated at this time"
    end
    render json: {redirect_url: current_org.home_page, message: message}
  end

  private

  def save_org_settings(setting)
    begin
      category = setting.fetch('category', 'settings')
      config_id = setting['id']
      if config_id.present?
        config = Config.where(configable: @current_org, id: setting['id']).first!
        config.update_attributes(value: setting['value'])
        @current_org.invalidate_config_cache(config)
        config
      else
        Config.create!(configable: @current_org, category: category, name: setting['name'], value: setting['value'], data_type: setting[:data_type])
      end
    rescue Exception => e
      log.error "Error while updating org settings for #{@current_org.id} : new settings #{setting} - #{e.message}"
    end
  end

  def organization_params
    params.require(:organization).permit(:is_open, :is_registrable, :name, :description, :show_onboarding,
      :image, :mobile_image, :splash_image, :host_name, :savannah_app_id, :redirect_uri, :co_branding_logo,
      :favicon, :show_sub_brand_on_login, configs_attributes: [:id, :name, :value])

  end

  def create_organization(org_params)
    organization = Organization.create!(host_name: org_params[:host_name],
                                        name: org_params[:name],
                                        status: 'pending',
                                        description: org_params[:description] || org_params[:name] #generate some sensible default description since we did not capture on the sign up form
    )
    organization
  end

  def create_admin_user(user_params, org)
    default_org_user = User.where(email: user_params[:email], organization: Organization.default_org).first
    if org.present? && org != Organization.default_org && default_org_user
      user = default_org_user.clone_for_org(org, 'admin') if default_org_user
      user.update is_active: false
    else
      user = User.new(email: user_params[:email],
                       first_name: user_params[:first_name],
                       last_name: user_params[:last_name],
                       handle: user_params[:handle],
                       is_complete: true,
                       organization: org,
                       organization_role: 'admin',
                       password_reset_required: false,
                       password: user_params[:password],
                       password_confirmation: user_params[:password],
                       is_active: false
                      )
      user.save!
    end
    user
  end

  def org_admin_check
    message = 'You are unauthorized to view this page. Please contact your organization administrator'
    unless current_user.is_admin_user?
      return render json: { message: message }, status: :forbidden
    end
    @org = @current_org
  end
end

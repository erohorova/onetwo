class Api::ConfigsController < ApiController
  include OrganizationContext

  respond_to :json

  api :GET, 'api/configs', 'Get channels with user following info'
  formats ['json']
  def index
    @configs = @current_org.configs.where(category: (params[:category] || 'settings'))
  end
end

# DEPRECATED
class Api::FeedPromotionsController < ApiController
  include OrganizationContext

  api :GET, "api/feed_promos/courses", "Get courses to be promoted on the feed"
  param :limit, Integer, desc: 'Number of promoted courses', required: false
  def courses
    limit = params[:limit] ? params[:limit] : 3
    @promotions = Promotion.promotions_for(user: current_user, limit: limit)
  end

  api :GET, "api/feed_promos/channels", "Get channels to be promoted on the feed"
  def channels
    channel_ids = UserContentsQueue.get_queue_for_user(user_id: current_user.id, content_type: 'Channel').map(&:content_id)
    @channels = (channel_ids.blank? ? [] : Channel.where(id: channel_ids).order("FIELD(id, #{channel_ids.join(',')})").includes(:followers).sample(3))
  end
end

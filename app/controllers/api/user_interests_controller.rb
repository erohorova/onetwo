# DEPRECATED
class Api::UserInterestsController < ApiController
  include AuthenticationHelper, DetectPlatform

  before_filter :set_token, only: :pwc_callback
  before_filter :authenticate_user_from_token!, only: :pwc_callback
  skip_filter :cm_auth_user, only: :pwc_callback

  api :GET, '/api/user_interests', "Get current user's interests"
  formats ['json']
  def index
    interests = current_user.interests
    render json: interests.map(&:name), status: :ok and return
  end

  #TODO - Refactor this. User user#update_interests
  api :POST, '/api/user_interests', 'Set interests for a user'
  param :interests, Array, desc: "Array of interest strings", required: true
  formats ['json']
  def create
    interest_names = params.require(:interests)
    current_interests_by_name = current_user.interests.map(&:name)
    new_interests_by_name = interest_names - current_interests_by_name
    to_be_deleted_names = current_interests_by_name - interest_names
    to_be_deleted = Interest.where(name: to_be_deleted_names)
    to_be_deleted.each {|del| current_user.interests.destroy(del)}
    new_interests_by_name.each do |ni|
      tg = Tag.find_or_create_by(name: ni)
      tg.update_attributes(type: 'Interest')
      current_user.interests_users.create(tag: tg)
    end

    new_interests = Interest.where(name: new_interests_by_name)
    @suggested_channels = Interest.suggested_channels_based_on_interests_for(user: current_user, interests: new_interests)
    @suggested_users = Interest.suggested_users_based_on_interests_for(user: current_user, interests: new_interests)

    render 'suggestions', status: :ok and return
  end

  api :DELETE, 'api/user_interests', 'Delete interest for a user'
  param :interest, String, desc: "Interest string to delete", required: true
  formats ['json']
  def destroy
    interest = Interest.find_by_name!(params.require(:interest))
    current_user.interests.destroy(interest)
    head :ok and return
  end

  api :GET, 'api/pwc_callback', 'process pwc and redirect to onboarding page'
  def pwc_callback
    process_pwc
    if is_mobile_browser?
      redirect_to "#{current_user.organization.mobile_uri_scheme}://wef/pwc_assessment/" and return
    else
      redirect_to "/onboard/v2"
    end
  end

  api :GET, 'api/profile_pwc_callback', 'process pwc and redirect to profile url'
  def profile_pwc_callback
    process_pwc
    if is_mobile_browser?
      redirect_to "#{current_user.organization.mobile_uri_scheme}://wef/pwc_assessment/" and return
    else
      redirect_to "/"
    end
  end

  protected
    def set_token
      params[:token] = params[:id]
    end

    def process_pwc
      default_topics = current_user.organization.default_topics
      if default_topics.present?
        UserInterests::Pwc.new(params, current_user, default_topics).process
      end
    end
end

class Api::CardSearchController < ApiController
  include CardSearchHelpers
  include OrganizationContext
  include ContentTypeFilter

  api :GET, 'api/search', 'Get search results for a query'
  param :q, String, :desc => 'search query string', :required => true
  param :limit, Integer, :desc => 'limit number of cards returned; default 15', :required => false
  param :offset, Integer, :desc => 'offset for pagination; default 0', :required => false
  formats ['json']
  def search
    unless params[:q].present?
      @cards = []
      @total_users = 0
      return
    end

    card_types = Card::EXPOSED_TYPES

    filter_params = { card_type: card_types }

    filter_params.merge!({state: 'published'})

    # Send only video streams with creators for native apps
    if is_native_app?
      unless client_supports_required_build?(560, 72)
        filter_params.merge!({pathway_author_must: true})
        filter_params.merge!({creator_must: true})
      end
      # ios 461 and above support cms cards. android 67 and above
      ios_build_number = 460
      android_build_number = 66
      unless client_supports_required_build?(ios_build_number, android_build_number)
        filter_params.merge!({author_must: true})
      end
    end
    @user = current_user
    multi_search(params, filter_params)
    if @cards.present?
      set_user_actions(@cards)
      set_card_comments(@cards)
    end
  end
end

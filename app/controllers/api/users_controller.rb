class Api::UsersController < ApiController
  include OrganizationContext
  include LimitOffsetConcern
  include ApplicationHelper

  PROFILE_ACTIONS = [:fetch_external_courses, :my_courses, :toggle_integration, :integrations, :toggle_profile_pref, :interests]
  PUBLIC_PROFILE_ACTIONS = [:my_courses, :interests]

  has_limit_offset_constraints only: [:index, :pending_users], limit_default: 15, offset_default: 0

  skip_before_action :cm_auth_user, only: PUBLIC_PROFILE_ACTIONS << :current_orgs
  before_filter :get_user, only: PROFILE_ACTIONS

  skip_before_action :verify_jwt, only: [:token]
  # Note: for pending users use 'inivitations'
  api :GET, 'api/users', 'Search current organization members'
  param :q, String, desc: 'Query string; default matches all', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :include_suspended, String, desc: "Boolean, whether to include suspended users or not, default: false", required: false
  param :only_suspended, String, desc: "Boolean, whether to show only suspended users or not, default: false", required: false
  param :followers, String, desc: "Boolean, show only followers, default: false", required: false
  param :following, String, desc: "Boolean, show only following, default: false", required: false
  param :influencers, String, desc: "Boolean, show only influencers, default: false", required: false
  param :role, String, desc: "Array, show only users with roles, ex. role=['sme'] or role='sme'", required: false
  param :team_ids, Array, desc: "Search users within particular team ids only", required: false
  param :sort, String, "Attribute name to sort by, ex name, created_at, current_sign_in_at"
  param :order, String, "Sort order, asc or desc"
  formats ['json']
  def index
    #Large number of followers, need to revisit
    if params[:followers].present? && params[:followers].to_bool
      user_ids = current_user.followers.where(is_suspended: false, is_complete: true).
        where("email like (:search_term) OR first_name like (:search_term) OR last_name like (:search_term) OR handle like (:search_term)", search_term: "%#{params[:q]}%").
        limit(limit).offset(offset).pluck(:id)

      @users = Search::UserSearch.new.search_by_id(user_ids)
    elsif params[:following].present? && params[:following].to_bool
      user_ids = current_user.followed_users.where(is_suspended: false, is_complete: true).
        where("email like (:search_term) OR first_name like (:search_term) OR last_name like (:search_term) OR handle like (:search_term)", search_term: "%#{params[:q]}%").
        limit(limit).offset(offset).pluck(:id)

      @users = Search::UserSearch.new.search_by_id(user_ids)
    else
      include_suspended = params[:include_suspended] == 'true'
      filter_params = {include_suspended: include_suspended}
      if params[:only_suspended] == 'true'
        only_suspended = include_suspended = true
        filter_params[:include_suspended] = true
        filter_params.merge!({only_suspended: only_suspended})
      end
      if params.has_key?(:team_ids)
        filter_params.merge!({team_ids: params[:team_ids]})
      end

      [:influencers].each do |filter_type|
        filter_params[filter_type] = true if params[filter_type].present? && params[filter_type].to_bool
      end

      if params[:role].present? && !params[:role].blank?
        filter_params[:role] = current_org.roles.where(name: params[:role]).map(&:name)
      end

      @users = Search::UserSearch.new.search(q: params[:q], viewer: current_user, offset: offset, limit: limit, allow_blank_q: true, filter: filter_params, sort: (params[:sort] || :created_at), order: (params[:order] || :desc)).results
      @is_admin_user = current_user.is_admin_user?
    end
  end

  api :PUT, 'api/users/:user_id/change_role', 'Toggle role between member and admin'
  formats ['json']
  def toggle_role
    @user = current_org.users.where(id: params[:user_id]).first!
    if current_user.is_admin_user? && (!@user.is_admin_user? || current_org.real_admins.count > 1)
      role = (%w[admin member] - [@user.organization_role]).first
      @user.set_org_role(role)
      if current_org.enable_role_based_authorization
        if role == 'admin'
          @user.add_role('admin')
        end
      else
          @user.add_role('member')
      end
      head :ok
    else
      render json: {errors: "User not found or not authorized"}, status: :unprocessable_entity
    end
  end

  # Top Nav api
  api :GET, "api/users/nav", "Get the information required to render the Top Nav"
  formats ['json']
  def top_nav
    # /collections/new route has been removed.
    add_content_dropdown = user_signed_in? ? {createSmartBiteLink: {label: 'SmartBites Insight', url: new_insights_manage_path}, createPathwayLink: {label: 'Pathway', url: '/collections/new'}} : {}
    auth_links = {signUp: {label: 'Sign Up', url: '/sign_up'}, signIn: {label: 'Sign In', url: '/log_in'}, signOut: {label: 'Sign Out', url: '/sign_out'}}
    profile_dropdown = user_signed_in? ? { 'user' => {'avatar' => current_user.photo(:tiny), 'name' => current_user.name, 'email' => current_user.email, 'handle' => current_user.handle}, 'currentOrgName' => current_user.organization.name, 'links' => {'myProfileLink' => {label: 'Profile', url: current_user.handle}, 'settingsLink' => {label: 'Settings', url: '/settings'}, 'signOutLink' => {label: 'Sign Out', url: '/sign_out'}, 'impersonationLink' => impersonation}} : {}
    if user_signed_in? && current_user.is_admin?
      profile_dropdown['links']['newOrganizationLink'] = {label: 'Create a Team', url: new_organization_url(host: Organization.default_org.host)}
    end
    co_branding = {enabled: ((current_user.organization.host != Organization.default_org.host) and (current_user.organization.co_branding_logo.present? || current_user.organization.photo.present? || current_user.organization.mobile_photo.present? )), orgLogo: current_user.organization.co_branding_logo.url, orgTeamLogo: current_user.organization.mobile_photo || current_user.organization.photo, orgSubBrandImage: get_co_branding_image }
    nav = {
      "coBranding" => co_branding,
      "userSignedIn" => user_signed_in?,
      "profileDropdown" => profile_dropdown,
      "authLinks" => auth_links,
      "position" => 0,
      "searchPageMode" => is_search_page?,
      "addContentDropdown" => add_content_dropdown
    }
    render json: nav and return
  end

  api :POST, 'api/users/:id/suspend', 'Suspend a user'
  param :id, Integer, desc: 'Id of the user to suspend', required: true
  formats ['json']
  def suspend
    head :unauthorized and return unless current_org.is_admin?(current_user)
    user = User.where(organization: current_org, id: params[:id]).first!
    user.suspend!
    head :ok and return
  end

  api :POST, 'api/users/:id/unsuspend', 'Unsuspend a user'
  param :id, Integer, desc: 'Id of the user to unsuspend', required: true
  formats ['json']
  def unsuspend
    head :unauthorized and return unless current_org.is_admin?(current_user)
    user = User.where(organization: current_org, id: params[:id]).first!
    user.unsuspend!
    head :ok and return
  end

  api :get, 'api/users/:user_id/integrations', 'get integrations of a user'
  formats ['json']
  def integrations
    if @user
      @integrations = Integration.enabled
    else
      render json: {errors: "User not found or not authorized"}, status: :unprocessable_entity
    end
  end

  api :put, 'api/users/:user_id/integrations', 'get integrations of a user'
  param :connect, String, desc: 'true/false', required: true
  param :provider, String, desc: "External provider name", required: true
  param :fields, Hash, desc: "Required if connected = true", required: false
  param :integration_id, Integer, desc: "Id of integration", required: false
  def toggle_integration
    if @user
      @users_integration = @user.users_integrations.where(integration_id: params[:integration_id]).first
      @users_integration.update_attribute(:connected, params[:connect]) if @users_integration
      if !params[:connect] || (params[:fields] && params[:fields].all? {|k,v| v.present?}) && courses
        head :ok
      else
        render json: {errors: "Unable to fetch courses."}, status: :unprocessable_entity
      end
    else
      render json: {errors: "User not found or not authorized"}, status: :unprocessable_entity
    end
  end

  api :post, 'api/users/:user_id/fetch_external_courses', 'get external_courses of a user from integration'
  param :fields, Hash, desc: 'input fields', required: true
  param :provider, String, desc: 'External provider name', required: true
  def fetch_external_courses
    if @user && (@user.id == current_user.id) && courses
      head :ok
    else
      render json: {errors: "Failed to fetch courses from: #{params[:provider]}"}, status: :unprocessable_entity
    end
  end

  api :get, 'api/users/:user_id/my_external_courses', 'get courses for a user'
  def my_courses
    if @user
      @external_courses = @user.load_external_courses
      @internal_courses = @user.courses
    else
      render json: {errors: "Failed to get external courses"}, status: :unprocessable_entity
    end
  end

  api :put, 'api/users/:user_id/toggle_profile_pref', "update user's profile preferences"
  param :prefs, Hash, desc: 'preferences hash', required: true
  def toggle_profile_pref
    if (@user && current_user && @user.id == current_user.id) && params[:profile]
      @user.pref_set_bool(params[:profile].keys[0], params[:profile].values[0])
    else
      render json: {errors: "Failed to update #{params[:profile].keys[0]} prefernces"}, status: :unprocessable_entity
    end
  end

  api :GET, "api/current_orgs", "Get current orgs for a user"
  def current_orgs
    render json: currently_signed_in_orgs.as_json(only: [:id, :name, :host_name]), status: :ok and return
  end

  api :GET, "api/users/token", "Get the JWT token for a user"
  def token
    render json: { token: current_user.jwt_token }
  end

  api :GET, 'api/users/:user_id/interests', 'get list of interests or competencies for a user'
  formats ['json']
  def interests
    if @user
      interests = @user.interests
      render json: interests.map(&:name), status: :ok and return
    else
      render json: {errors: 'Failed to get list of interests or competencies'}, status: :unprocessable_entity
    end
  end

  private
    def courses
      #expecting all fields in params[:fields] && return false if any field is blank
      if params[:fields] && params[:fields].all? {|k,v| v.present?}
        scraper =  case params[:provider]
        when "Coursera"
          Integrations::CourseraService.new(external_uid: params[:fields][:coursera_public_page_id], user_id: params[:user_id])
        when "Udemy"
          Integrations::UdemyService.new(external_uid: params[:fields][:udemy_public_page_id], user_id: params[:user_id])
        when "Future Learn"
          Integrations::FutureLearnService.new(external_uid: params[:fields][:future_learn_profile_id], user_id: params[:user_id])
        when "Skill Share"
          Integrations::SkillShareService.new(external_uid: params[:fields][:skill_share_user_name], user_id: params[:user_id])
        when "Plural Sight"
          Integrations::PluralSightService.new(external_uid: params[:fields][:plural_sight_user_name], user_id: params[:user_id])
        when "Tree House"
          Integrations::TreeHouseService.new(external_uid: params[:fields][:tree_house_user_name], user_id: params[:user_id])
        when "Code School"
          Integrations::CodeSchoolService.new(external_uid: params[:fields][:code_school_user_name], user_id: params[:user_id])
        when "Alison"
          Integrations::AlisonService.new(external_uid: params[:fields][:alison_user_id], user_id: params[:user_id])
        when "Codecademy"
          Integrations::CodecademyService.new(external_uid: params[:fields][:codecademy_username], user_id: params[:user_id])
        end
        scraper.scrape
      end
    end

    def get_user
      @user = current_org.users.where(id: params[:user_id]).first
    end

    def profile_params
      params.require(:profile).permit(:show_continuous_learning, :show_education, :show_competencies)
    end

    def impersonation
      if session['devise.impersonator']
        impersonationLink_enabled = true
        impersonator = User.find(session['devise.impersonator'])
        impersonationLink_url = impersonations_path
        impersonationLink_label = "Back to #{impersonator.handle}"
        impersonationLink_method = 'delete'
      else
        impersonationLink_enabled = (user_signed_in? && current_user.is_admin? && @user && current_user != @user)
        impersonationLink_url = impersonationLink_enabled ? impersonations_path(user_id: @user.id) : '#'
        impersonationLink_label = 'Impersonate'
        impersonationLink_method = 'post'
      end

      impersonationLink = {enabled: impersonationLink_enabled, url: impersonationLink_url, label: impersonationLink_label, method: impersonationLink_method}
    end
end

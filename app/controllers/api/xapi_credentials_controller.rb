class Api::XapiCredentialsController < ApiController

  include OrganizationContext

  before_filter :admin_only!

  api :GET, 'api/xapi_credentials', 'Get org xapi credentials'
  formats ['json']
  def index
    @xapi_credential = current_org.xapi_credential
  end

  api :POST, 'api/xapi_credentials', 'Create a xapi Credential for an org'
  param :xapi_credential, Hash, desc: "Xapi Credential Wrapper object", required: true do
    param :end_point, String,  desc: 'url endpoint for lrs xapi', required: true
    param :lrs_login_key, String, desc: 'login for lrs xapi', required: true
    param :lrs_password_key, String, desc: 'password for lrs xapi', required: true
    param :lrs_api_version, String, desc: 'api version for lrs', required: true
  end
  def create
    xapi_credential = current_org.build_xapi_credential(xapi_credential_params)
    if xapi_credential.save
      render json: xapi_credential
    else
      render status: :error, json: { errors: xapi_credential.full_errors }
    end
  end

  api :PUT, 'api/xapi_credentials/:id', 'Create a xapi Credential for an org'
  param :xapi_credential, Hash, desc: "Xapi Credential Wrapper object", required: true do
    param :end_point, String,  desc: 'url endpoint for lrs xapi', required: true
    param :lrs_login_key, String, desc: 'login for lrs xapi', required: true
    param :lrs_password_key, String, desc: 'password for lrs xapi', required: true
    param :lrs_api_version, String, desc: 'api version for lrs', required: true
  end
  def update
    xapi_credential = XapiCredential.find(params[:id])
    if xapi_credential.organization_id != current_org.id
      render status: :error, json: { error: 'Organization ids dont match' } and return
    end
    if xapi_credential.update(xapi_credential_params)
      render json: xapi_credential and return
    else
      render status: :error, json: { errors: xapi_credential.full_errors } and return
    end
  end

  api :PUT, 'api/xapi_credential/activate', 'activate xAPI for an organization'
  formats ['json']
  def activate
    if !current_org.xapi_credential.nil? && current_org.update(xapi_enabled: true)
      render json: { xapi_enabled: current_org.xapi_enabled }
    else
      current_org.update(xapi_enabled: false)
      render json: { error: 'No xAPI credentials found. Please update before activating' }, status: :unprocessable_entity
    end
  end

  api :PUT, 'api/xapi_credential/deactivate', 'activate xAPI for an organization'
  formats ['json']
  def deactivate
    if current_org.update(xapi_enabled: false)
      head :ok and return
    else
      head :unprocessable_entity and return
    end
  end

  def xapi_credential_params
    xapi_credential_params = params.require(:xapi_credential).permit(:end_point, :lrs_login_key, :lrs_password_key, :lrs_api_version)
  end

end

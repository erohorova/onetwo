# deprecated
class Api::UserOnboardingsController < ApiController
  include OrganizationContext

  before_action :decrypt_password_payload, only: [:update], unless: :is_native_app?
  
  def index
    # WORKAROUND:
    #   To handle revised custom onboarding.
    #   current_step may exceeds the last step configured by admin for onboarding.
    #   In this case, mark user onboarding as complete.
    #   This is a temporary change.
    user_onboarding = current_user.try(:user_onboarding)
    current_step    = user_onboarding.try(:current_step)
    onboarding_options = current_user.onboarding_options.values
    if params[:custom_onboarding].to_s.to_bool && !current_user.onboarding_completed?
      last_step = onboarding_options.last
      # Use case 1: If last step is disabled by org-admin, complete the user's onboarding status
      current_step > last_step && user_onboarding.complete!
      # Use case 2: If intermediate is disabled by org-admin, shift the current step to next step of onboarding options
      !onboarding_options.include?(current_step) && user_onboarding.update(current_step: onboarding_options.last)
    end

    render json: { csrf_token: form_authenticity_token, step: user_onboarding.try(:current_step), user: current_user.onboarding_data, onboarding_completed: current_user.onboarding_completed? }
  end

  def update
    if params[:step].blank?
      head :unprocessable_entity and return
    end
    attempted_step  = params[:step].to_i
    user_onboarding = current_user.user_onboarding

    # WORKAROUND:
    #   Web development is 3 weeks ahead of mobile development.
    #   As per new implementation, `profile_setup` is a mandatory step irrespective of onboarding steps configured or not.
    #   Keeping backward-compatibility in mind for initial stages.
    #   Hence, customised onboarding is flexible with query parameter (`custom_onboarding` = TRUE)
    if params[:custom_onboarding].to_s.to_bool
      user_onboarding.custom_onboarding = true
      onboarding_steps = current_user.onboarding_options.values
      current_step = onboarding_steps.at(onboarding_steps.find_index(attempted_step) + 1)
    else
      onboarding_steps = UserOnboarding::STEPS
      current_step = attempted_step + 1
    end

    if current_user.onboarded?
      render json: { status: :ok } and return
    end

    if !current_user.user_onboarding.started?
      user_onboarding.start!
    end

    if params[:step].to_i == onboarding_steps.last
      user_onboarding.complete!
    end

    if params[:step] == "1" && params[:user][:password].blank?
      render json: { status: :error, errors: [ "Password is null" ]  } and return
    end

    # To save if user accepted the terms & condition
    # Handles in step 1, profile setup
    if params[:step] == '1' && params[:user][:tac_accepted]
      if current_user.update(user_params)
        render json: { status: :ok }
      else
        render json: { status: :error, errors: [ "Please accept terms & conditions to proceed" ]  } and return
      end
    end

    # Handles step 3 and step 4 where there is no params[:user]
    if params[:user].blank? && !params[:step].blank?
      current_user.user_onboarding.update({ current_step: current_step })
    end

    if !params[:user].blank? && user_params.present? && current_user.update(user_params)
      bypass_sign_in current_user
      current_user.user_onboarding.update({ current_step: current_step })
      render json: { status: :ok }
    else
      render json: { status: :error, errors:  current_user.full_errors }
    end
  end

  def user_params
    if (current_user.first_name.present? && current_user.identity_providers.present?)
      params[:user].delete(:first_name)
    end
    if (current_user.last_name.present? && current_user.identity_providers.present?)
      params[:user].delete(:last_name)
    end
    params
      .require(:user)
      .permit(
        :first_name,
        :last_name,
        :password,
        :bio,
        :avatar,
        websites: [],
        user_interests: [],
        mentor_emails: [],
        profile_attributes: [
          :tac_accepted,
          { expert_topics: [:topic_name, :topic_id, :topic_label, :domain_name, :domain_id, :domain_label] },
          { learning_topics: [:topic_name, :topic_id, :topic_label, :domain_name, :domain_id, :domain_label] }
        ]
      )
  end

  private

    def decrypt_password_payload
      if current_user.organization.encryption_payload? && user_params[:password].present? && ENV['JS_PRIVATE_KEY'].present?
        begin
          private_key = OpenSSL::PKey::RSA.new(ENV['JS_PRIVATE_KEY'].gsub("\\n", "\n"))
          params[:user].merge!(password: private_key.private_decrypt(Base64.decode64(params[:user][:password])))
        rescue OpenSSL::PKey::RSAError
          params
        end
      end
    end

end

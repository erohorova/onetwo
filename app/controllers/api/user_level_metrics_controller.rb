# deprecated
class Api::UserLevelMetricsController < ApiController
  include OrganizationContext
  include LimitOffsetConcern

  api :GET, "api/user_level_metrics", "Get User Level Metrics for signed in user"
  param :period, Integer, :desc => 'one of \'day\', \'week\', \'month\', \'year\' or \'alltime\'. Default to \'all_time\'.', :required => false
  formats ['json']
  def index
    applicable_offsets = PeriodOffsetCalculator.applicable_offsets(Time.now.utc).with_indifferent_access

    time_period = params[:period] || :all_time
    time_period_offset = applicable_offsets[time_period]

    @metric = UserLevelMetric.where(period: time_period, offset: time_period_offset, user_id: current_user.id).first

    # Dont calculate for inactive users, just send 0.
    @percentile = @metric.try(:percentile).to_i #|| UserLevelMetric.get_percentile_for_inactive_users(organization_id: current_org.id, period: time_period, offset: time_period_offset)

    @topic_level_metrics = UserTopicLevelMetric.where(period: time_period, offset: time_period_offset, user_id: current_user.id).
                                      group("user_id, tag_id").
                                      select("user_id, tag_id, sum(smartbites_score) as smartbites_score").
                                      order("smartbites_score DESC").
                                      limit(3)
  end

  api :GET, "api/user_level_metrics/trend", "Get trend of metrics for signed in user"
  param :period, String, :desc => 'one of \'day\', \'week\', \'month\' or  \'year\'. Default to \'day\'.', :required => false
  param :lookback, Integer, :desc => "Works with the period param, to say how many days, weeks, months, or year we should get data for. For example, if period=\'day\' and lookback=10, we will return data for 10 days in the past. Default: 10. Includes current period"
  def trend
    applicable_offsets = PeriodOffsetCalculator.applicable_offsets(Time.now.utc).with_indifferent_access
    if !params[:period].nil? && !applicable_offsets.has_key?(params[:period])
        render json: {error: "Bad period parameter. Use one of 'day', 'week', 'month', 'year' or 'alltime'"},
          status: :bad_request and return
    end

    period = params[:period] || :day
    lookback_count = params[:lookback].to_i || 10

    @offsets = ((applicable_offsets[period] - lookback_count + 1)..applicable_offsets[period]).to_a

    @metrics_by_offset = UserLevelMetric.where(period: period,
                                               offset: @offsets,
                                               user_id: current_user.id).
                                         index_by(&:offset)

    @inactive_percentiles_by_offset = Hash[
      @offsets.map do |offset|
        [offset, UserLevelMetric.get_percentile_for_inactive_users(organization_id: current_org.id,
                                                                   period: period,
                                                                   offset: offset)
        ]
      end
    ]
  end


  api :GET, "api/organization_metrics", "Get Metrics data for org analytics dashboard"
  param :period, Integer, :desc => 'one of \'week\', \'month\' or \'year\'. Default to \'week\'.', :required => false
  formats ['json']
  def organization_metrics
    # returns offsets using current date
    # format: { "all_time" => 0, "day" => 25, "week" => 10, "month" => 2, "year" => 1 }
    applicable_offsets = PeriodOffsetCalculator.applicable_offsets(Time.now.utc).with_indifferent_access
    time_period = params[:period] || :week
    time_period_offset = applicable_offsets[time_period]

    args = {organization: current_org, period: time_period, offset: time_period_offset}

    current_period_data = OrganizationMetricsGatherer.current_period_data(args)
    comparison_data = OrganizationMetricsGatherer.comparison_data(args)

    current_period_drill_down_data = OrganizationMetricsGatherer.drill_down_data(args)
    previous_period_drill_down_data = OrganizationMetricsGatherer.drill_down_data(args.merge({offset: time_period_offset-1}))

    render json: Hash[OrganizationMetricsGatherer::METRICS.map{|metric|
      [metric, {
        value: current_period_data[metric],
        change: comparison_data[metric],
        drilldown: {
          current: current_period_drill_down_data[metric],
          previous: previous_period_drill_down_data[metric]
        }
      }]
    }], status: :ok and return
  end

end

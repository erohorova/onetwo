class Api::SchoolsController < ApplicationController
  skip_before_filter :authenticate_user!, only: [:index]
  before_action :set_school, only: [:show, :update, :destroy]

  resource_description do
    api_version 'Deprecated'
  end

  api :GET, 'api/users/:user_id/schools', 'Get schools of a user'
  param :user_id, String, :desc => "User id of a school", :required => true
  formats ['json']
  def index
    @user = set_user
    unless @user
      head :not_found and return 
    else
      @schools = @user.schools
    end
  end

  api :POST, 'api/users/:user_id/schools/', "Create a school"
  param :name, String, :desc => "Name of the school", :required=> true
  param :specialization, String, :desc => "Specialization of school", :required => true
  param :from_year, String, :desc => "From year of a school", :required => true
  param :to_year, String, :desc => "From year of a school", :required => false
  param :user_id, String, :desc => "User id of a school", :required => true
  param :image, File, :desc => "Logo of a school", :required => false
  formats ['json']
  def create
    @user = set_user
    unless @user
      head :not_found and return
    else
      @school = @user.schools.new school_params
      if @school.save
        render json: {'id' => @school.id}, status: :ok and return
      else
        render json: @school.errors, status: :unprocessable_entity
      end
    end
  end

  api :PUT, 'api/users/:user_id/schools/:id', 'Update scholl of a user'
  param :user_id, String, :desc => "User id of a school", :required => true
  formats ['json']
  def update
    head :not_found and return unless @user
    if @school.update_attributes(school_params)
      head :ok
    else
      render json: @school.errors, status: :unprocessable_entity
    end
  end

  api :DELETE, 'api/users/:user_id/schools/:id', 'Delete a school'
  param :user_id, String, :desc => "User id of a school", :required => true
  formats ['json']
  def destroy
    @school.destroy
    head :ok
  end

  private
    def school_params
      params.require(:school).permit(:name, :specialization, :from_year, :to_year, :user_id, :image)
    end

    def set_school
      begin
        @user = set_user
        @school = @user.schools.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        head :not_found
      end
    end

    def set_user
      @user = User.find_by(id: params[:user_id])
    end
end

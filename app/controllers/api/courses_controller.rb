class Api::CoursesController < ApiController
  include OrganizationContext
  include LimitOffsetConcern, CourseRetriever

  has_limit_offset_constraints only: [:index]

  respond_to :json

  ## --------------------------------------------------------------------------------------------------------------
  ## NOTE: This is temporary fix for now until we have all apps migrated to dogwood (from Savannah perspective)
  ## --------------------------------------------------------------------------------------------------------------
  ## This endpoint handles two scenarios:
  ## Scenario 1: When client has dogwood release, then returns EdX data structure
  ## Scenario 2: When client has older release (for say, Cypress), then returns data structure that reads from S3
  ## Check api/courses_controller_test for actual API response from Savannah. Its deadly!!!
  ## --------------------------------------------------------------------------------------------------------------
  api :GET, 'api/courses/:id/course_structure', 'Retrieves course from Savannah OR S3'
  param :id, Integer, desc: 'Current group Id', required: true
  formats ['json']
  def course_structure
    group = ResourceGroup.where(id: params[:id]).first
    unless group
      render json: { status: 'error' }, status: :not_found and return
    end

    unless group.is_active_user?(current_user)
      head :unauthorized and return
    end

    struct_params = {}
    struct_params[:user] = current_user if params[:simplify].present?
    data = group.get_cache_course_structure(struct_params) || { status: 'error', msg: 'Something went wrong' }

    # Forcing dogwood_release in the response as TRUE since mobile is heavily dependent on this key.
    if data[:status] == 'success'
      render json: data.merge!(dogwood_release: true)
    else
      render json: { status: data[:status], msg: data[:msg] }, status: :unprocessable_entity
    end
  end

  api :GET, 'api/courses/:id/last_accessed', 'Retrieves the last accessed unit by the user'
  param :id, Integer, :desc => 'Resource group id', :required => true
  formats ['json']
  def get_last_accessed
    group = ResourceGroup.find(params[:id])

    status, data = SavannahService::LastAccessed.new(group).get_last_accessed(current_user)

    render json: data, status: status
  end

  api :PUT, 'api/courses/:id/last_accessed', 'Sets the last accessed unit by the user'
  param :id, Integer, :desc => 'Resource group id', :required => true
  formats ['json']
  def set_last_accessed
    group = ResourceGroup.find(params[:id])

    status, data = SavannahService::LastAccessed.new(group).set_last_accessed(current_user, params[:last_module_id])

    render json: data, status: status
  end

  api :GET, 'api/courses/:id/course_progress', 'Retrieves the course progress for a user'
  param :id, Integer, :desc => 'Resource group id', :required => true
  formats ['json']
  def course_progress
    group = ResourceGroup.find(params[:id])

    status, data = SavannahService::CourseProgress.new(group).get_course_progress(current_user)

    render json: data, status: status
  end

  ## NOTE: Need to fix this as group is located by client_resource_id
  api :POST, 'api/courses/:id/enroll', 'Enroll user to a Savannah course'
  param :id, Integer, :desc => 'Savannah course id', :required => true
  formats ['json']
  def enroll
    group = ResourceGroup.find_by_client_resource_id(params[:id])
    if (!group.nil?)
      post_uri = URI("#{Settings.savannah_base_location}/course_enroll")
      payload = {'item_id' => params[:id],
                 'user' => {
                     'email' => current_user.email,
                     'first_name' => current_user.first_name,
                     'last_name' => current_user.last_name
                 }}

      conn = Faraday.new(url: "#{post_uri.scheme}://#{post_uri.host}:#{post_uri.port}")

      result = conn.post do |req|
        req.url post_uri.path
        req.headers['Content-Type'] = 'application/json'
        req.headers['Accept'] = 'application/json'
        req.headers['X-Api-Key'] = group.valid_credential.api_key
        req.headers['X-Shared-Secret'] = group.valid_credential.shared_secret
        req.headers['X-Savannah-App-Id'] = @current_org.savannah_app_id.to_s
        req.body = payload.to_json
      end

      if (result.success?)
        group.add_member(current_user)
        render json: {status: 'success', savannah_id: params[:id], edcast_id: group.id}
      else
        render json: {status: 'error', msg: "savannah returned #{result.status}"}, status: :unprocessable_entity
      end

    else
      render json: {status: 'error'}, status: :unprocessable_entity
    end
  end

  api :GET, 'api/courses', 'Get Courses for the current organization'
  param :limit, Integer, :desc => "Limit for courses returned for pagination purposes ", required: false
  param :offset, Integer, :desc => "Offset for courses returned for pagination purposes ", required: false
  param :include_enrolled, String, :desc => "Wont return courses enrolled if set to false", required: false, default: "true"
  param :query, String, :desc => "query to look for specific courses", required: false
  formats ['json']
  def index
    @courses = current_org.featured_courses.limit(limit).offset(offset)

    #when include_enrolled is passed as false will not return enrolled courses.
    include_enrolled = params[:include_enrolled].present? ? params[:include_enrolled].to_bool : true
    unless include_enrolled
      current_user_courses = current_user.courses.map(&:id)
      @courses = @courses.includes(:enrolled_students).where.not(id: current_user_courses)
    end

    # to search in list of courses for course name that matches the search query term
    if params['query'].present?
      @courses = @courses.where("name like ? ", "%#{params[:query]}%")
    end
    @courses_count = current_org.featured_courses.count
  end

end

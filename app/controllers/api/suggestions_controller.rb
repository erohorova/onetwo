# DEPREACATED
class Api::SuggestionsController < ApiController
  include OrganizationContext
  skip_before_filter :cm_auth_user, only: [:suggest, :initial_state]

  api :GET, "api/suggest_interests", "Get Interest Suggestions"
  param :q, String, :desc => "get suggestions for this string, defaults to empty string", :required => true
  param :limit, Integer, :desc => "limit number of results, default 10", :required => false
  formats ['json']
  def suggest_interests
    q = params[:q] || ''
    limit = params[:limit] || 10
    interests = current_user.interests.collect(&:name)
    limit = limit + current_user.interests.length
    results = TagSuggest.suggestions_for_destiny(term: q, size: limit, organization_id: current_org.id, tag_type: 'Interest')
    interests.each {|interest| results.delete(interest)}
    render json: results, status: :ok and return
  end

  api :GET, "api/suggest", "Get user and tag completion suggestions"
  param :q, String, :desc => "get suggestions for this string, defaults to empty string", :required => true
  param :limit, Integer, :desc => "limit number of results, default 10", :required => false
  param :exclude_users, String, :desc => "dont suggest users", :required => false
  param :exclude_tags, String, :desc => "dont suggest tags", :required => false
  param :tag_filters, Hash, :desc => "Additional filters on tags" do
    param :group_id, Integer, :desc => "Search in this group only", :required => false
  end
  formats ['json']
  def suggest
    @user_suggestions = @tag_suggestions = nil

    q = params[:q]
    limit = params[:limit] || 10

    @exclude_users = params[:exclude_users]
    unless @exclude_users
      uq = q

      organization_id = current_user.try(:organization_id) || Organization.default_org&.id
      @user_suggestions = Search::UserSearch.new.suggest(q: uq, organization_id: organization_id, limit: limit)
    end

    @exclude_tags = params[:exclude_tags]
    unless @exclude_tags
      tq = q.gsub('#', '')
      _group_id = params[:tag_filters].try(:[], 'group_id')
      if _group_id.to_i != 0
        @tag_suggestions = TagSuggest.suggestions_for_forum(term: tq, size: limit, group_id: _group_id)
      else
        @tag_suggestions = TagSuggest.suggestions_for_destiny(term: tq, size: limit, organization_id: current_org.id)
      end
    end

    @results = {user_suggestions: @user_suggestions, tags: @tag_suggestions}
  end

  api :GET, "api/suggest/suggest_tags"
  param :term, String, :desc => "get suggestions for this string, defaults to empty string"
  formats ['json']
  def suggest_tags
    q = params[:term] || ''
    @tag_suggestions = TagSuggest.suggestions_for_destiny(term: q, size: 10, organization_id: current_org.id)
  end

  api :GET, "api/suggest/initial_state", "Get initial state data like recent and popular searches. Only include recent searches if the user is logged in"
  formats ['json']
  def initial_state
    if user_signed_in?
      recent_response = SearchQuery.search(filter: {and: [{term: {user_id: current_user.id}}]}, sort: {created_at: 'desc'}, size: 100)
      @recent = recent_response.collect {|item| item["query"]}.uniq[0..9]
    end

    organization_id = current_user.try(:organization_id) || Organization.default_org&.id
    popular_response = SearchQuery.gateway.client.search(
      index: SearchQuery.index_name,
      search_type: 'count',
      body: {
        aggs: {
          popular: { terms: { field: 'query', exclude: SearchQuery::STOP_WORDS } }
        },
        query: {
          filtered: {
            filter: {
              and: [{term: {organization_id: organization_id}}]
            }
          }
        }
      })
    #popular_response['aggregations'] = {"popular"=>{"doc_count_error_upper_bound"=>0, "sum_other_doc_count"=>2, "buckets"=>[{"key"=>"user1 query1", "doc_count"=>1}, {"key"=>"user1 query2", "doc_count"=>1}, {"key"=>"user1 query3", "doc_count"=>1}, {"key"=>"user1 query4", "doc_count"=>1}, {"key"=>"user1 query5", "doc_count"=>1}, {"key"=>"user1 query6", "doc_count"=>1}, {"key"=>"user2 query1", "doc_count"=>1}, {"key"=>"user2 query2", "doc_count"=>1}, {"key"=>"user2 query3", "doc_count"=>1}, {"key"=>"user2 query4", "doc_count"=>1}]}}
    @popular = popular_response['aggregations']['popular']['buckets'].map {|bucket| bucket['key']}
  end
end
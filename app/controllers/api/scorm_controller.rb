require 'scorm_cloud'
require 'uri'
class Api::ScormController < ApiController
  skip_before_filter :cm_auth_user

  api :GET, 'api/scorm/launch', 'redirect to launch url'
  param :course_id, String, :desc => "Scorm course id", :required => true
  def launch
    if (url = launch_url)
      redirect_to url
    else
      @error = "Scorm URL is invalid, please try again after sometime."
      render "api/error", status: 404
    end
  end

  api :GET, 'api/scorm/close', 'redirect to close url'
  def close
  end

  def completion
    log.info("params received from scorm cloud: #{params}")
    if params[:data].present?
      scorm_completion_service = ScormCompletionService.new
      scorm_completion_service.complete_with_result(params[:data])
    else
      log.warn("No data from scorm cloud params: #{params}")
    end
    head :ok
  end

  private

  def launch_url
    begin
      # Get current user
      # Find will raise an exception if the user is not found, and will have the route
      # throw an error automatically
      user_context_encoded = params[:user_context]
      return false if user_context_encoded.blank?

      user_context, _ = JWT.decode(user_context_encoded, Settings.scorm.user_context_secret)
      return false if (current_user = User.find_by_id(user_context['user_id'])).blank?

      # Rest of the stuff
      sc = ScormCloud::ScormCloud.new(Settings.scorm.app_id, Settings.scorm.secret)
      # course_id = 'SequencingSimpleRemediation_SCORM20043rdEdition18cd465d-7c3c-4647-a3aa-50e5833e5161'
      course_id = params[:course_id]
      reg_id = "#{current_user.id}-#{course_id}"
      postback_url = "#{current_user.organization.secure_home_page}/api/scorm/completion"
      unless (registration_exist(sc, reg_id))
        unless register_on_scorm(sc, current_user, course_id, reg_id, postback_url)
          log.error("Registration failed on scorm for course_id #{course_id}, reg_id #{reg_id} for user #{current_user.id}")
          return false
        end
      end
      sc.registration.launch(reg_id, "#{current_user.organization.home_page}/api/scorm/close")
    rescue Exception => e
      log.error("Scorm launch url failed for course_id #{course_id} and user_id #{current_user.id} with exception #{e.message}")
      return false
    end
  end

  def register_on_scorm(sc, current_user, course_id, reg_id, postback_url)
    options={postbackurl: URI::encode(postback_url)}
    sc.registration.create_registration(course_id, reg_id, current_user.first_name, current_user.last_name, current_user.id, options)
  end

  def registration_exist(sc, reg_id)
    reg_detail = Nokogiri::XML(sc.registration.get_registration_result(reg_id))
    reg_detail.at_xpath("//rsp").attributes["stat"].value == "ok"
  end
end
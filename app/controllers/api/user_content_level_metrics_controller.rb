class Api::UserContentLevelMetricsController < ApiController
  include OrganizationContext
  include LimitOffsetConcern
  include AnalyticsPeriodOffsetConcern

  has_limit_offset_constraints only: [:index]
  extract_analytics_period_offsets only: [:index]

  api :GET, "api/content_level_metrics", "Get Content Level Metrics for a User"
  param :limit, Integer, :desc => 'limit for pagination; default 10', :required => false
  param :offset, Integer, :desc => 'offset for pagination; default 0', :required => false
  param :days, Integer, :desc => 'get data for this many days in the past, default: 0, just today', :required => false
  param :period, Integer, :desc => 'one of \'day\', \'week\', \'month\', \'year\' or \'alltime\'. The \'days\' param is ignored if this is passed', :required => false
  formats ['json']
  def index
    @metrics = UserContentLevelMetric.where(period: analytics_period, offset: analytics_offset, user_id: current_user.id).
                                      group("user_id, content_id, content_type").
                                      select("user_id, content_id, content_type, sum(smartbites_score) as smartbites_score").
                                      order("smartbites_score DESC").
                                      limit(limit).
                                      offset(offset)
  end
end

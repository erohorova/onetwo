# DEPRECATED
class Api::ContentLevelMetricsController < ApiController
  include OrganizationContext
  include LimitOffsetConcern
  include AnalyticsPeriodOffsetConcern
  include SortOrderExtractor

  has_limit_offset_constraints only: [:index]
  extract_analytics_period_offsets only: [:index]

  api :GET, "api/content_level_metrics", "Get Metrics for each content item in an organization"
  param :limit, Integer, :desc => 'limit for pagination; default 10', :required => false
  param :offset, Integer, :desc => 'offset for pagination; default 0', :required => false
  param :days, Integer, :desc => 'get data for this many days in the past, default: 0, just today', :required => false
  param :period, Integer, :desc => 'one of \'day\', \'week\', \'month\', \'year\' or \'alltime\'. The \'days\' param is ignored if this is passed', :required => false
  param :order_attr, String, :desc => "Attribute to sort on. One of \"views_count\", \"comments_count\", \"likes_count\""
  param :order, String, :desc => "Either \"asc\" or \"desc\", default: \"desc\"", required: false
  param :search_term, String, :desc => "Show only content names that match the search term", required: false
  formats ['json']
  def index
    order_attr, order = extract_sort_order(valid_order_attrs: ["views_count", "comments_count", "likes_count", "completes_count"], default_order_attr: "views_count")

    filtered_query = ContentLevelMetric.where(period: analytics_period, offset: analytics_offset, organization_id: current_org.id)
    search_term = params[:search_term]

    if search_term.present?
      term = "%#{search_term}%"
      filtered_query = filtered_query
        .joins("left join cards on cards.id = content_level_metrics.content_id and content_level_metrics.content_type = 'card'
                left join video_streams on video_streams.id = content_level_metrics.content_id and content_level_metrics.content_type = 'video_stream'")
        .where("cards.title like :search_term OR cards.message like :search_term OR video_streams.name like :search_term", search_term: term)
    end

    @metrics = filtered_query.
      group("content_id, content_type").
      select("content_id, content_type, sum(views_count) as views_count, sum(content_level_metrics.comments_count) as comments_count, sum(likes_count) as likes_count, sum(completes_count) as completes_count, sum(content_level_metrics.bookmarks_count) as bookmarks_count").
      order("#{order_attr} #{order}").
      limit(limit).
      offset(offset)
  end
end

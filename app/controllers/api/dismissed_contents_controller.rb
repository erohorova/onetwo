# DEPRECATED

class Api::DismissedContentsController < ApiController

  api :POST, 'api/dismissed_contents', 'Dismiss a piece of content'
  param :content_id, Integer, desc: 'Content Id', required: true
  param :content_type, String, desc: 'Content Type', required: true
  formats ['json']
  def create
    @dismissed_content = DismissedContent.new(content_id: params[:content_id], content_type:params[:content_type], user: current_user)
    if @dismissed_content.save
      head :ok and return
    else
      head :unprocessable_entity and return
    end
  end
end

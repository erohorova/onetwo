class Api::UserTopicLevelMetricsController < ApiController
  include OrganizationContext
  include LimitOffsetConcern
  include AnalyticsPeriodOffsetConcern

  before_filter :load_target, only: [:knowledge_map]

  api :GET, "api/knowledge_map", "Get Knowledge map for an organization"
  api :GET, "api/users/:user_id/knowledge_map", "Get Knowledge map for a user"
  api :GET, "api/topics/:topic_id/knowledge_map", "Get knowledge map for a topic"
  param :limit, Integer, :desc => 'limit for how many top items to include; default 10', :required => false
  param :user_id, Integer, :desc => "Get knowledge map for this user, that is, returns top topics that this user contributes to", :required => false
  param :topic_id, Integer, :desc => "Get knowledge map for this topic, that is, returns top users who contribute to this topic. This parameter is ignored if user_id is passed in", :required => false
  formats ['json']
  def knowledge_map
    limit = params[:limit] ? params[:limit].to_i : 10
    @knowledge_map = UserTopicLevelMetric.knowledge_map(current_org, @target, limit)
  end

  private
  def load_target
    if params.has_key? 'user_id'
      @target = current_org.users.find(params[:user_id])
    elsif params.has_key? 'topic_id'
      @target = Tag.find(params[:topic_id])
    else
      @target = current_org
    end
  end

end

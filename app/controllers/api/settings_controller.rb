class Api::SettingsController < ApiController
  skip_before_filter :cm_auth_user
  skip_before_filter :authenticate_user!

  api :GET, 'api/settings/version', 'Get version of API and Build'
  formats ['json']
  def version
    render json: { build: current_app_build}
  end

end

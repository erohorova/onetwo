# DEPRECATED
class Api::DiscoverItemsController < ApiController
  include OrganizationContext
  include ContentTypeFilter

  respond_to :json

  api :GET, 'api/discover_items/counts', 'Get total counts of discover tab items'
  formats ['json']
  def counts
    @data = Rails.cache.fetch("discover-items-counts-for-#{current_user.id}", expires_in: 10.minutes) do
      @user = current_user
      exclude_user_ids = @user.followed_user_ids + [@user.id]

      collections_filter_params = get_content_type_filters("collections")
      collections_filter_params.merge! state: 'published'
      collections = Search::CardSearch.new.search(nil, current_org, viewer: @user,
        filter_params: collections_filter_params, sort: :promoted_first)

      video_streams =  Search::VideoStreamSearch.new.search(q: nil, viewer: @user,
        filter_params: {}, sort_params: [{is_official: {order: 'desc'}}, {_score: "desc"}])

      {
        total_users_count: current_org.users.size,
        pathways_count: collections.total,
        video_streams_count: video_streams.total,
        courses_count: current_org.featured_courses.count,
        channels_count: Channel.fetch_eligible_channels_for_user(user: @user).count,
        suggested_users_count: current_org.users.sme_and_influencers.active(exclude_ids: exclude_user_ids).count,
        total_suggested_users_count: current_org.users.sme_and_influencers.active(exclude_ids:[]).count
      }
    end

    render json: @data, status: :ok
  end
end

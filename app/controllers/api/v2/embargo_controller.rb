class Api::V2::EmbargoController < Api::V2::BaseController
  include EmbargoConcern

  has_limit_offset_constraints only: [:index, :preview_csv]

  before_action :require_organization_admin
  before_action :check_values_and_category, only: [:batch_create]
  before_action :check_params_to_delete, only: [:batch_remove]
  before_action :load_file, only: [:bulk_upload]
  before_action :check_file_url_extension, only: [:preview_csv]

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, 'api/v2/embargo', 'Get embargo content'
  param :limit, Integer, desc: 'Number of content to retrieve', required: false
  param :offset, Integer, desc: 'Number of records to skip before the next record', required: false
  param :order, String, desc: 'Ordering based on created_at, desc or asc. Default is desc.', required: false
  param :order_attr, String, desc: 'Sort results. Default: created_at date. Values: created_at, value'
  param :category, String, desc: 'Filter by content type. Must be one of `approved_sites`, `unapproved_sites`, `unapproved_words`', required: false
  param :q, String, 'Search term', required: false
  def index
    get_embargo_for_org
  end

  api :GET, 'api/v2/embargo/:id', 'Get detailed info about content'
  param :id, Integer, desc: 'Id of the embargo content', required: true
  def show
    @embargo = find_embargo
  end

  api :POST, 'api/v2/embargo/batch_create', 'Create embargo content'
  param :category, String, desc: 'Content type. Must be one of `approved_sites`, `unapproved_sites`, `unapproved_words`', required: true
  param :values, Array, desc: 'Array of embargo content values', required: true
  def batch_create
    errors = []
    @values.each do |value|
      embargo = org_embargo.build(value: value, category: @category, user: current_user)
      unless embargo.save
        errors.push(value)
      end
    end
    if errors.any?
      message = "There were errors when adding the following content: #{errors.join(", ")}"
      render_with_status_and_message(message, status: :ok)
    else
      head :ok
    end
  end

  api :POST, 'api/v2/embargo/batch_remove', 'Delete an array of embargo content'
  param :embargo_ids, Array, 'Array of embargo ids to delete(ids must be integer)', required: true
  def batch_remove
    content_to_remove = org_embargo.where(id: params[:embargo_ids])
    errors = []

    not_found_ids = params[:embargo_ids] - content_to_remove.pluck(:id)
    errors.concat(not_found_ids)

    content_to_remove.each do |c|
      errors.push(c) unless c.destroy
    end

    if errors.any?
      message = "There were errors when removing content with the following ids: #{errors.join(", ")}"
      render_with_status_and_message(message, status: :ok)
    else
      head :ok
    end
  end

  api :PUT, 'api/v2/embargo/:id', 'Update embargo content'
  param :id, Integer, desc: 'Id of the embargo content', required: true
  param :category, String, desc: 'Content type. Must be one of `approved_sites`, `unapproved_sites`, `unapproved_words`', required: true
  param :value, String, desc: 'Value of content', required: true
  def update
    if update_content
      render 'show'
    else
      render_unprocessable_entity(@embargo.errors.full_messages.join(", "))
    end
  end

  api :GET, "api/v2/embargo/preview_csv", 'Send bulk upload preview details'
  param :csv_file_url, String, desc: 'Url of CSV file uploaded to S3', required: true
  param :display_row_count, String, desc: 'Count of rows that should be displayed in preview. Default: 10', required: false
  param :file_id, Integer, desc: 'ID of a CSV data file', required: false
  param :limit, Integer, desc: 'Number of content to retrieve', required: false
  param :offset, Integer, desc: 'Number of records to skip before the next record', required: false
  def preview_csv
    load_full_csv_data and return if params[:file_id].present?

    status, data = csv_data
    return render json: {status: false, message: data} unless status
    embargo_file = current_org.embargo_files.build(user_id: current_user.id, data: data)
    if embargo_file.save
      display_row_count = params[:display_row_count]&.to_i || 10
      embargoes, rows_total_count = BulkImportEmbargo.new(file_id: embargo_file.id).preview_csv(display_row_count)

      render json: {
        status: true, message: 'CSV is successfully uploaded',
        file_id: embargo_file.try(:id), embargoes: embargoes, rows_total_count: rows_total_count
      }, status: :ok
    else
      render_unprocessable_entity(embargo_file.errors.full_messages.join(", "))
    end
  end

  api :POST, "api/v2/embargo/bulk_upload", "Upload embargo content from CSV file"
  param :file_id, Integer, "Id of embargo file obtained using the preview api 'api/v2/embargo/preview_csv'"
  def bulk_upload
    return render_not_found("Can not find file with the following id: #{params[:file_id]}") unless @file

    BulkImportEmbargoesJob.perform_later(
      file_id: @file.id
    )
    render json: {}
  end

  private

  def csv_data
    csv_file_url = params['csv_file_url']
    begin
      csv_data = Faraday.get(csv_file_url).body
      header_error = BulkImportEmbargo.csv_headers_validation(csv_data)
      if header_error
        [false, header_error]
      else
        [true, csv_data]
      end
    rescue => e
      [false, e.message]
    end
  end

  def load_file
    @file = current_org.embargo_files.find_by(id: params[:file_id])
  end

  def embargo_params
    params.require(:embargo).permit(:category, :value)
  end

  def check_values_and_category
    if params[:values].is_a?(Array) && Embargo::VALID_TYPES.include?(params[:category])
      @values = params[:values].reject(&:blank?)
      @category = params[:category]
      render_unprocessable_entity('Missing required params') if @values.empty?
    else
      render_unprocessable_entity('Invalid data type')
    end
  end

  def check_params_to_delete
    unless params[:embargo_ids].is_a?(Array) && params[:embargo_ids].any?
      render_unprocessable_entity('Missing required params')
    end
  end

  def check_file_url_extension
    return if params[:file_id].present?
    unless params['csv_file_url'].present? && params['csv_file_url'] =~ /.csv/
      render_unprocessable_entity('Invalid file url extension.')
    end
  end

  def load_full_csv_data
    load_file
    return render_not_found('File does not exists') unless @file
    rows, total = @file.extract_specific_rows(limit: limit, offset: offset)

    render json: {
      embargoes: rows,
      rows_total_count: total
    }, status: :ok
  end
end

class Api::V2::TranslationLanguagesController < Api::V2::BaseController
  skip_before_filter :authenticate_user_from_token!, only: :index

  resource_description do
    api_version 'v2'
    formats ['json']
  end

  api :GET, 'api/v2/translation_languages', 'Get list of translation languages'
  formats ['json']
  def index
    render json: {body: LANGUAGES}
  end
end

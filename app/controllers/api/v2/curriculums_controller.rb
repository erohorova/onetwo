# frozen_string_literal: true

class Api::V2::CurriculumsController < Api::V2::BaseController
  include CardConcern

  def_param_group :curriculum do
    param :curriculum, Hash, required: true do
      param :journey_ids, Array, 'Ids of journey to be linked with new Pathway', required: true
      param :title, String, 'Title of pathway', required: true
      param :message, String, 'Message of pathway'
    end
  end
    
  api :POST, 'api/v2/curriculums', 'Create curriculum'
  description <<-EOS
    Create a curriculum pathway.
    This API is only for linking existing Journey(s) with a new Pathway.
    
    For Backend ONLY
  EOS
  param_group :curriculum
  authorize :create, Permissions::MANAGE_CARD
  def create
    options = { inline_indexing: true, action: :create }
    card_attrs = prepare_card_params(pathway_params.merge(create_attrs), options)
    @card = current_org.cards.build(card_attrs)
    if @card.save
      record_card_create_metric(@card)
      add_journey_to_pathway(@card)
      head :ok
    else
      render_unprocessable_entity(@card.full_errors)
    end
  end

  private

  def add_journey_to_pathway(pathway)
    journey_ids.each do |journey_id|
      pathway.add_card_to_pathway(journey_id, 'Card')
    end
  end

  def pathway_params
    params.require(:curriculum).permit(:title, :message)
  end

  def create_attrs
    {
      card_type: 'pack',
      card_subtype: 'curriculum',
      author_id: current_user.id,
      state: 'draft'
    }
  end

  def journey_ids
    params.dig(:curriculum, :journey_ids) || []
  end
end
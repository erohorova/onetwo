class Api::V2::Channels::TeamsController < Api::V2::BaseController

  has_limit_offset_constraints only: [:index]

  api :GET, 'api/v2/channels/:channel_id/teams', 'Get teams followers of a channel'
  param :q, String, desc: 'Search teams'
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  def index
    @channel = current_org.channels.friendly.find(params[:channel_id])
    @teams = @channel.followed_teams
    @teams = @teams.search_by_name(params[:q]) if params[:q].present?
    @total = @teams.count
    @teams = @teams.limit(limit).offset(offset)
  end

  api :DELETE, 'api/v2/channels/:channel_id/teams/:id', 'teams unfollows a channel'
  param :ids, Array, desc: 'Array of ids to be unfollowed from channel'
  def destroy
    teams_channels_follow = TeamsChannelsFollow.where(channel_id: params[:channel_id], team_id: params[:ids]) if params[:ids].present?
    teams_channels_follow.destroy_all if teams_channels_follow.present?

    head :ok
  end
end

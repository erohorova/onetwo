class Api::V2::WalletTransactionsController < Api::V2::BaseController
  include CompoundIdReader

  before_action :find_orderable_and_price, only: :initiate_transaction
  before_action :validate_amount, only: :initiate_transaction

  rescue_from EdcastError, with: :render_specific_error_response

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized to access channel'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :POST, 'api/v2/wallet_purchase/new', 'Create wallet transaction, initiate for payment.'
  description <<-EOS
  This is the first step for a wallet transaction,
  this creates the transaction with initial state.
  EOS
  param :orderable_id, Integer, desc: 'Id of orderable, for which payment is done',
        required: true
  param :orderable_type, String, desc: 'Type of orderable for which order will be
        created. `org_subscription/card` ', required: true
  param :price_id, Integer, desc: 'Id of the price for the orderable, for which
        payment is done', required: true
  def initiate_transaction
    @response = PaymentGatewayService.new(
      orderable_type: params["orderable_type"],
      orderable: @orderable,
      price: @price,
      user_id: current_user.id,
      organization_id: current_org.id,
      gateway: 'wallet').prepare_payment!
  end

  api :POST, 'api/v2/wallet_purchase/complete', 'Process wallet payment for a transaction.'
  description <<-EOS
  This is the final step for a wallet payment transaction,
  this completes the transaction.
  EOS
  param :token, Integer, desc: 'token which is shared by wallet_purchase/new api',
        required: true
  def process_payment
    if params.values_at("token").any?(&:blank?)
      return render_unprocessable_entity('payment_nonce or token can not be
                                                        blank.')
    end
    @transaction = PaymentGatewayService.process_payment!(
      token: params["token"])

    if @transaction.successful?
      order = @transaction.order
      if (order&.orderable_type == 'Card')
        card = order.orderable
        @auth_status = true
        @redirect_url = card&.resource&.url
      end
      @message = 'Payment is Successful'
    else
      message = @transaction.errors.full_messages.to_sentence
      message ||= "Something went wrong, please try again in sometime"
      render_unprocessable_entity(message) and return
    end
  end

  private
  def find_orderable_and_price
    @orderable = load_card_from_compound_id(params["orderable_id"])
    render_not_found('Order can not be found.') and return unless @orderable
    @price = @orderable.prices.find_by(id: params["price_id"])
    render_not_found('Price can not be found.') and return unless @price
  end

  def validate_amount
    WalletPaymentGateway.validate_amount(amount:@price.amount, user_id: current_user)
  end

  def render_specific_error_response(exception)
    render json: { message: exception.message, code: exception.code }, status: exception.http_status
  end
end
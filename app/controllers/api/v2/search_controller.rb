class Api::V2::SearchController < Api::V2::BaseController
  include FollowableConcern
  include SearchConcern
  include EventTracker
  include CardsFilter

  has_limit_offset_constraints only: [:index, :private_content]
  before_action :set_last_access_opts, only: [:index]

  api :GET, 'api/v2/search/initial_state', 'Get initial state data like recent and popular searches.'
  formats ['json']
  api_version 'v2'
  def initial_state
    recent_response = SearchQuery.search(filter: {and: [{term: {user_id: current_user.id}}]}, sort: {created_at: 'desc'}, size: 100)
    @recent = recent_response.collect {|item| item["query"]}.uniq[0..9]

    popular_response = SearchQuery.gateway.client.search(
      index: SearchQuery.index_name,
      search_type: 'count',
      body: {
        aggs: {
          popular: { terms: { field: 'query', exclude: SearchQuery::STOP_WORDS } }
        },
        query: {
          filtered: {
            filter: {
              and: [{term: {organization_id: current_org.id}}]
            }
          }
        }
      })
    @popular = popular_response['aggregations']['popular']['buckets'].map {|bucket| bucket['key']}
  end

  api :GET, 'api/v2/search', 'Search for users, channels and content'
  formats ['json']
  api_version 'v2'
  description <<-EOS
    This will return users, channels and cards from the org that matches the search query.
    Clients can filter by content_type and source_id.
    Returns search results as well as aggregation counts for different content and source ids
    === Example:
    ?q=google search across users, channels and cards for the term 'google'\n
    ?q=google&content_type=['pathway', 'video'] searches only pathways and videos that match the search term 'google'. \n
    ?q=google&source_id=['abc-def'] searches only content from source id 'abc-def' that matches the search term 'google' \n
  EOS
  param :q, String, desc: 'Query string', required: true
  param :topic, Array, desc: 'Result offset; default 0', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :cards_limit, Integer, desc: 'Result limit; default 10', required: false
  param :cards_offset, Integer, desc: 'Result offset; default 0', required: false
  param :channels_limit, Integer, desc: 'Result limit; default 10', required: false
  param :channels_offset, Integer, desc: 'Result offset; default 0', required: false
  param :users_limit, Integer, desc: 'Result limit; default 10', required: false
  param :users_offset, Integer, desc: 'Result offset; default 0', required: false
  param :content_type, Array, desc: <<-TXT, required: false
    Filters search results to include these content types only.
    Some examples: channel, user, pathway, poll, article, video,
    insight, video_stream, course, journey.
    This list will expand in the future.
  TXT
  param :source_id, Array, desc: <<-TXT, required: false
    Filters search results to include content from these source ids only.
    Source Ids are obtained from the aggregation response from this api.
  TXT
  param :provider_name, Array, desc: <<-TXT, required: false
    Filters search results to include content from these provider names only.
    Provider Names are obtained from the aggregation response from this api.
  TXT
  param :languages, Array, desc: <<-TXT, required: false
    Filters search results to include content from these languages only.
    Languages are obtained from the aggregation response from this api.
  TXT
  param :subjects, Array, desc: <<-TXT, required: false
    Filters search results to include content from these subjects only.
    Subjects are obtained from the aggregation response from this api.
  TXT
  param :source_type_name, Array, <<-TXT, required: false
    Filters search results to include content from these source types only.
    Source_type names are obtained from the aggregation response from this api
  TXT
  param :query_type, String, <<-TXT, required: false
    Search query type; default 'and', 'or' - union of all matched results.
    'and' - fetch results which satisfies all applied conditions.
  TXT
  param :group_id, Array, desc: 'Array of groups to search'
  param :channel_id, Array, desc: 'Array of channels to search'
  param :posted_by, Hash, desc: "Search query based on other user posting", :required => false do
    param :followers, String, desc: "Cards posted by current user's followers. Boolean value - true / false"
    param :following, String, desc: "Cards posted by current user's follwing. Boolean value - true / false"
    param :user_ids, String, desc: "Cards liked by users with user_ids, comma seperated string"
  end
  param :liked_by, Hash, desc: "Search query based on other user likes", :required => false do
    param :followers, String, desc: "Cards liked by current user's followers. Boolean value - true / false"
    param :following, String, desc: "Cards liked by current user's follwing. Boolean value - true / false"
    param :user_ids, String, desc: "Cards liked by users with user_ids, comma seperated string"
  end
  param :commented_by, Hash, desc: "Search query based on other user comments", :required => false do
    param :followers, String, desc: "Cards commented by current user's followers. Boolean value - true / false"
    param :following, String, desc: "Cards commented by current user's follwing. Boolean value - true / false"
    param :user_ids, String, desc: "Cards commented by users with user_ids, comma seperated string"
  end
  param :bookmarked_by, Hash, desc: "Search query based on other user bookmarks", :required => false do
    param :followers, String, desc: "Cards bookmarked by current user's followers. Boolean value - true / false"
    param :following, String, desc: "Cards bookmarked by current user's follwing. Boolean value - true / false"
    param :user_ids, String, desc: "Cards bookmarked by users with user_ids, comma seperated string"
  end

  param :from_date, String, desc: "Filter to get cards created after a date, format: dd/mm/yyyy", required: false
  param :till_date, String, desc: "Filter to get cards create before date, format: dd/mm/yyyy", required: false
  param :level, Array, desc: "Search query based on BIA level", required: false
  param :rating, Array, desc: "Search query based on rating", required: false
  param :currency, String, desc: "Price currency for search", required: false
  param :amount_range, Array, desc: "amount range for search", required: false
  param :sort_attr, String, desc: "Sort attribute", required: false
  param :sort_order, String, desc: "Sort order", required: false
  param :is_paid, String, desc: "Filter based on whether the card is free or paid Boolean value - true / false", required: false
  param :plan, String, desc: "Filter based on whether the card is free/paid/premium/subscription", required: false
  param :load_hidden, String, desc: "Should we include hidden cards", required: false
  param :last_access_at, String, desc: 'Time of last access, format: 1505832150', required: false


  formats ['json']
  param :segregate_pathways, String, desc: 'Get pathways sepereately from content. Boolean value - true / false', required: false
  def index
    render_unprocessable_entity("Query parameter q cannot be empty") and return if params[:q].nil?

    # Merge to detect if the requests originated from mobile or web.
    @options.merge!(is_native_app: is_native_app?)

    query_type = params[:query_type].presence || 'and'
    do_search(search_params, query_type: query_type)
    record_search_metrics
  end

  api :GET, 'api/v2/search/private_content', 'Get private content'
  formats ['json']
  api_version 'v2'
  param :q, String, desc: 'Query string', required: true
  param :topic, Array, desc: 'Result offset; default 0', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  def private_content
    render_unprocessable_entity("Query parameter q cannot be empty") and return if params[:q].nil?

    # Merge to detect if the requests originated from mobile or web.
    (@options ||= {}).merge!(is_native_app: is_native_app?)

    @cards = EclApi::CardConnector.new.get_private_content_from_ecl(current_user.id, limit, offset)
  end

  private

  def search_params
    params.permit(:q, :from_date, :till_date, :load_tags, :cards_offset, :cards_limit, :channels_offset,
      :channels_limit, :sort_attr, :sort_order, :users_offset, :users_limit, :segregate_pathways, :is_paid, :load_hidden,
      :only_private, :limit, :offset, :currency, level: [], rating: [], group_id: [], channel_id: [],
      source_id: [], topic: [], content_type: [], source_type_name: [], exclude_cards: [],
      posted_by: [:followers, :following, :user_ids],
      liked_by: [:followers, :following, :user_ids],
      commented_by: [:followers, :following, :user_ids],
      bookmarked_by: [:followers, :following, :user_ids],
      amount_range: [], plan: [], provider_name: [], languages: [], subjects: []
    )
  end

  def record_search_metrics
    # We only want to record the initial search for a given query.
    # If any of the 'offset' params are given, we don't send metrics.
    offsets = %i{offset cards_offset channels_offset users_offset}
    return if offsets.any? { |offset| params[offset].to_i > 0 }
    results_count = @aggs
      .select { |agg| agg["type"] == "content_type" }
      .map { |result| result["count"] }
      .sum

    record_custom_event(
      event: 'homepage_searched',
      actor: :current_user,
      job_args: [{search_query: params[:q]}, {results_count: results_count}]
    )
  end
end

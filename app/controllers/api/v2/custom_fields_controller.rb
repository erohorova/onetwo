# frozen_string_literal: true

class Api::V2::CustomFieldsController < Api::V2::BaseController

  before_action :set_custom_field, only: [:update, :destroy, :custom_field_config]

  resource_description do
    api_version 'v2'
    formats ['json']
  end

  api :POST, 'api/v2/custom_fields', 'Create custom field in an organization'
  param :custom_field, Hash, desc: 'Custom Field' do
    param :display_name, String, desc: 'Name of a custom field', required: true
    param :enable_people_search, String, desc: 'Boolean. Enable people search.', required: false
  end
  def create
    @custom_field = current_org.custom_fields.new(custom_field_params)
    unless @custom_field.save
      render_unprocessable_entity(@custom_field.full_errors)
    end
  end

  api :GET, 'api/v2/custom_fields', 'Get all custom fields in current Organization'
  def index
    @custom_fields = current_org.custom_fields
  end

  api :PUT, 'api/v2/custom_fields/:id', 'Update enable_people_search for particular custom field'
  param :id, Integer, desc: 'ID of the custom field', required: true
  param :enable_people_search, String, desc: 'Boolean. Enable people search.', required: false
  authorize :update, Permissions::ADMIN_ONLY
  def update
    return render_unprocessable_entity('Missed required params') if custom_field_params['enable_people_search'].nil?
    if @custom_field.update_attributes(enable_people_search: custom_field_params['enable_people_search'])
      head :ok
    else
      render_unprocessable_entity(@custom_field.errors.full_messages)
    end
  end

  api :DELETE, 'api/v2/custom_fields/:id', 'Delete particular custom field'
  param :id, Integer, desc: 'ID of the custom field', required: true
  authorize :destroy, Permissions::ADMIN_ONLY
  def destroy
    if @custom_field.destroy
      head :ok
    else
      render_unprocessable_entity(@custom_field.errors.full_messages)
    end
  end

  api :POST, 'api/v2/custom_fields/:id/config', 'Update config enable in filters or user profile or editable by users for custom field'
  param :id, Integer,  desc: 'Id of the custom field', required: true
  param :config_name, String, 'name config, enable_in_filters, editable_by_users or show_in_profile', required: true
  param :value, [true, false], 'value for config', required: true
  authorize :filters, Permissions::ADMIN_ONLY
  def custom_field_config
    unless params['config_name'].in?(%w[enable_in_filters show_in_profile editable_by_users])
      return render_unprocessable_entity('Config_name is not valid')
    end

    if params['value'].to_s.blank?
      return render_unprocessable_entity('Value can`t be empty')
    end
    @custom_field.set_value_for_config(params['config_name'], params['value'])
  end

  private

  def custom_field_params
    params.require(:custom_field).permit(:display_name, :enable_people_search)
  end

  def set_custom_field
    @custom_field = current_org.custom_fields.find_by(id: params[:id])
    render_not_found('Custom field does not exists') unless @custom_field
  end
end

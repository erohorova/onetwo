class Api::V2::RecommendationsController < Api::V2::BaseController

  api :get, 'api/v2/recommendations/', 'Get recommended cards, users'
  description <<-EOS
    Recommend more content similar to content (content_id) passed in
    this request.
  EOS
  param :content_id, Integer, desc: 'Card Id', required: true
  param :user_id, Integer, desc: 'User Id', required: false
  param :tag, String, desc: 'one of CC, CUC, CT, CU, defaults to: CC', required: false
  def index
    pp = params.permit(:content_id, :tag, :user_id)

    user = pp[:user_id] && current_org.users.find_by(id: pp[:user_id]) || current_user
    render_not_found and return unless pp[:content_id]

    status, @results = SociativeRecommenderService.new(
      user_id: user.id,
      content_id: pp[:content_id],
      tag: pp[:tag] || 'CC'
    ).recommendations

    if status != 200
      render_unprocessable_entity(
        "Error occurred while getting content: #{@results[:error]}"
      ) and return
    end
  end
end

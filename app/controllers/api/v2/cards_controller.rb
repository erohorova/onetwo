# frozen_string_literal: true

class Api::V2::CardsController < Api::V2::BaseController
  include CardConcern
  include TrackActions
  include FollowableConcern
  include EventTracker
  include EmbargoChecker
  include CardsFilter

  has_limit_offset_constraints only: [
    :index,            :addable_pathways,
    :metric_users,     :cards_user_metric,
    :addable_journeys, :purchased,
    :card_reportings,  :duplicate_cards,
    :assignable,       :deleted_cards
  ], limit_default: 15, offset_default: 0

  around_filter :replica_database, only: [:index]

  before_action :load_cover, only: [
    :add,               :batch_add,
    :batch_remove,      :journey_add,
    :journey_batch_add, :journey_remove,
    :remove,            :publish,
    :reorder,           :journey_reorder,
    :custom_order,      :journey_batch_remove
  ]
  before_action :validate_pack_card_type, only: [
    :add,               :batch_add,
    :batch_remove,      :journey_add,
    :journey_batch_add, :journey_remove,
    :remove,            :publish,
    :reorder,           :journey_batch_remove
  ]
  before_action :set_last_access_opts, only: [:index]
  before_action :set_channels_card, only: [:curate, :skip, :archive]
  before_action :verify_channel_curator, only: [:curate, :skip, :archive]
  before_action :require_card_author_or_permission, only: [
    :metric, :metric_users, :time_range_analytics
  ]
  before_action :load_card, only: [
    :rate,                  :addable_pathways,
    :time_range_analytics,  :add_to_pathways,
    :share,                 :dismiss,
    :set_lock,              :start,
    :addable_journeys,      :promote,
    :unpromote,             :post_to_channel,
    :trash,                 :edit,
    :assignable,            :transfer_ownership,
    :remove_assignments,    :show,
    :add_to_mdp,            :mdp_card_detail,
    :secured_urls
  ]
  before_action :authorize_transfer_ownership, only: :transfer_ownership
  before_action :load_reported_or_trashed_card, only: [:unreport, :card_reportings]
  before_action :require_card_access, only: [:show, :pathway]
  before_action :validate_journey_type, only: [:create]
  before_action :check_pack, only: [:set_lock]
  before_action :check_compound_ids, only: [:batch_add, :journey_batch_add]
  before_action :check_poll_card_for_allow_re_answer, only: :responses

  skip_before_action :authenticate_user_from_token!, only: :course_status
  before_action :authenticate_from_provider, only: :course_status
  after_action :notify_user, only: [:batch_add, :batch_remove, :add_to_pathways]
  before_action :embargo_content_check, only: [:create, :update]
  before_action :require_organization_admin, only: [:unreport, :card_reportings, :trash]

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized to access channel'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  def_param_group :card do
    param :card, Hash, required: true do
      param :message, String, desc: 'Card message - can include links and mentions', :required => true
      param :language, String, desc: 'Card language', :required => false
      param :card_type, String, desc: 'Type of card. One of `media` `poll` `pack` `course` `video_stream` `project`. Default: Based on message, server would set the card type'
      param :card_subtype, String, desc: 'Sub Type of card. Currently we support this for only `video_stream` type cards, values can be `simple` or `role_play`'
      param :state, ["new", "draft", "published"], desc: "card state"
      param :auto_complete, String, desc: 'Boolean, value of autocomplete for cards with type `pack` or `journey`', :required => false
      param :provider, String, desc: "Card provider", :required => false
      param :provider_image, String, desc: "Card provider's image url", :required => false
      param :file_resource_ids, Array, desc: "Attachments to this card", :required => false
      param :channel_ids, Integer, desc: "Array of channels to post the card to", :required => false
      param :group_ids, Integer, desc: "Array of groups to post the card to", :required => false
      param :resource_id, Integer, "Resource id", required: false
      param :topics, Array, desc: 'Array of related topics to the card. Topics are strings', required: false
      param :filestack, Array, desc: 'Array of ojects received from filestack', required: false
      param :last_access_at, String, desc: 'Time of last access, format: 1505832150', required: true
      param :options, Array, desc: "Response options for poll if card type is poll", :required => true do
        param :label, String, desc: "option label"
        param :file_resource_id, Integer, desc: "Id of file upload"
      end
      param :video_stream, Hash, desc: "Video Stream attributes" do
        param :status, ["pending", "live", "upcoming"], desc: "Video Stream status", required: false
        param :start_time, String, desc: "Start time of the stream, format: 2016-10-10T10:00:00Z", required: false
        param :uuid, String, desc: "Video Stream UUID, Unique indentifier received from uuid api", required: false
        param :recording, Hash, desc: "Recording attributes. Use this attribute to upload a video file to be associated with the card. Clients are expected to upload the file to S3 first, and provide the following information to the API" do
          param :bucket, String, desc: "s3 bucket name. Example: 'edcast-production-paperclip-store", :required=> true
          param :key, String, desc: "s3 file key. Example: 'video_streams/6393/server/b1c7d0ad-78c4-41d1-aaca-4a31150a8a13.flv'", :required => true
          param :location, String, desc: "Full s3 file location. Example: 'https://edcast-production-paperclip-store.s3.amazonaws.com/video_streams/6393/server/b1c7d0ad-78c4-41d1-aaca-4a31150a8a13.flv'", :required => true
          param :checksum, String, desc: "s3 file checksum. This will be a string returned by the S3 SDK", :required => true
        end
      end
      param :card_badging_attributes, Hash, desc: "card badging attributes " do
        param :title, String, desc: "title for the badge in a pathway", required: false
        param :badge_id, Integer, desc: "id of badge", required: false
        param :all_quizzes_answered, [true, false], desc: 'Set true/false to issue a badge when all quizzes are answered', required: false
      end
      param :is_public, [true, false], desc: 'Is the card public? Defaults to true', required: false
      param :is_paid, String, "Price required for card."
      param :card_metadata, Hash, desc: 'Metadata for card' do
        param :plan, CardMetadatum::PLAN_TYPES, desc: 'plan type of a card', required: false
      end
      param :prices_attributes, Array, "Array of price attributes", required: false do
        param :id, Integer, "Id of price record"
        param :currency, String, "Currency of amount Default: USD"
        param :amount, String, "Amount of card"
      end
    end
  end

  api :GET, 'api/v2/cards', 'Returns list of cards from org'
  description <<-EOS
    This will return cards from the org that current user belongs to.
    Supports keyword search, current user cards, channel cards, pathways, official cards and state filters
    and sort results by several keys
    === Example:
    PS: All possible options are scoped to the org that current user belongs to
    ?q=google search in all cards \n
    ?q=google&author_id={current user id} search in cards created by current user. \n
    ?q=google&author_id={current user id}&card_type=['pack'] search in cards created by current user and return only pathways \n
  EOS
  param :q, String, desc: 'Query string; default matches all', required: false
  param :filter_by_language, [true, false], desc: "Filter cards based on the user's language", required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :state, Array, desc: 'Array of card states to filter by. Default: ["published"]. Allowed values: new, published, archived, deleted', required: false
  param :is_cms, String, desc: 'Flag set for admin/content/items to show all the cards from the organization', required: false
  param :author_id, Array, desc: 'Fetch cards created by users with these ids. Most common use case is to get cards created by a single user, in which case set params["author_id"] = [user_id]', required: false
  param :channel_id, Array, desc: 'Fetch cards posted to at least one of the channels identified with these ids. Most common use case is to get cards for a channel, in which case set params["channel_id"] = [channel_id]', required: false
  param :group_id, Array, desc: 'Fetch cards posted to at least one of the groups identified with these ids. Most common use case is to get cards for a group, in which case set params["group_id"] = [group_id]', required: false
  param :card_type, Array, desc: 'Array of card types. possible values: poll, media, pack Ex. card_type=[poll, pack]'
  param :card_subtype, Array, desc: 'Array of card subtypes.', required: false
  param :readable_card_type, Array, desc: 'Fetch cards based on the labels course,podcast,webinar Ex. readable_card_type=[course, podcast, webinar]'
  param :is_official, Integer, desc: 'filter to get official pathways, use true/t/yes/y/1 or false/f/no/n/0', required: false
  param :is_ugc, Integer, desc: 'filter to get ugc and non_ugc cards, use true/t/yes/y/1 or false/f/no/n/0', required: false
  param :topics, Array, desc: 'Fetch cards that match at least one of these topics. Topics are strings', required: false
  param :sort, String, desc: <<-EOS
    Sort results. Default: published_at date.
    Values: promoted_first, votes_count, created, updated, added_to_channel,
    ecl_source_name,title, message, state, author_name, views_count
  EOS
  param :only_deleted, String, desc: 'filter to get only deleted cards, use true/t/yes/y/1 or false/f/no/n/0', required: false
  param :full_response, String, desc: 'Gives full response for edc-cms', required: false
  param :skip_content, Hash, desc: 'Filter content specific type', required: true do
    param :bookmarked, String, desc: 'filter to skip bookmarked cards, use true/t/yes/y/1 or false/f/no/n/0', required: false
    param :completed, String, desc: 'filter to skip completed cards, use true/t/yes/y/1 or false/f/no/n/0', required: false
    param :dismissed, String, desc: 'filter to skip dismissed cards, use true/t/yes/y/1 or false/f/no/n/0', required: false
  end

  def index
    # Merge to detect if the requests originated from mobile or web.
    @options.merge!(is_native_app: is_native_app?)

    # ignore user specific flags for is_cms
    if params[:is_cms].try(:to_bool) && current_org.has_admin_access?(current_user)
      # skip the group admin permission check for admin user
      check_exclude_group_permission(Permissions::MANAGE_GROUP_CONTENT) unless current_user.is_org_admin?
      get_cards_for_cms
    else
      get_cards

      #TODO - Move to this to CardConcern#get_cards, once confirmed with sunil about developer API
      @assignments = current_user.assignments.where(
        assignable_type: 'Card',
        assignable_id: @cards.map(&:id)
      ).index_by(&:assignable_id)

      channel_card_rank(params[:channel_id]) if params[:sort] == 'rank' && params[:channel_id].present?

      load_is_following_for(@cards.map(&:author_id), 'User', current_user)
    end
    if params[:full_response] && (params[:card_type] == ['journey'] || params[:card_type] == ['pack'])
      card_ids = @cards.map(&:id)
      @chosen_assignments = Assignment.where(user_id: current_user.id, assignable_id: card_ids, assignable_type: 'Card')
        .select(:id, :due_at, :assignable_id)
        .group_by(&:assignable_id)
        .transform_values(&:first)

      @chosen_subscriptions = CardSubscription.where(user_id: current_user.id, card_id: card_ids)
        .select(:card_id, :end_date)
        .group_by(&:card_id)
        .transform_values(&:first)
    end
  end

  api :GET, 'api/v2/cards/purchased', 'Returns list of cards from org'
  description <<-EOS
    This will return all purchased cards from the org that belongs to current user.
  EOS
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 15', required: false
  def purchased
    subscription_ids = current_user.active_card_subscriptions.pluck(:card_id)
    @cards = Card.where("id in (?)", subscription_ids)
               .order("id desc").limit(limit).offset(offset)
    @total = Card.where("id in (?)", subscription_ids).count
  end

  api :POST, 'api/v2/cards', 'create smartbite/insight/card'
  description <<-EOS
    Create a smartbite from message and other resources
    It supports file resources. If images attached, this would be image card
    Links are accepted as resource_id parameter. Clients first need to obtain a resource_id while posting a link
    Except message everything else is optional
    This api also supports poll card creation.
  EOS
  param_group :card
  authorize :create, Permissions::MANAGE_CARD
  def create
    options = {inline_indexing: true, action: :create}
    card_attrs = prepare_card_params(card_params, options)
    @card = current_org.cards.build(card_attrs)
    @card.card_user_shares.map {|cus| cus.shared_by_id = current_user.id } if !@card.card_user_shares.empty?
    @card.author_id = current_user.id unless card_params[:is_carousel].try(:to_bool)
    @card.current_user = current_user

    @card.hidden = true if card_params[:card_subtype] == 'journey'
    if @card.save
      @card.upload_to_scorm if @card.filestack.present? && @card.filestack.first[:scorm_course]

      @card.send_card_create_or_share_notification(event: Card::EVENT_NEW_CARD_SHARE,
        user_ids: card_attrs[:users_with_access_ids],
        team_ids: card_params[:team_ids], share_by_id: current_user.id,
        permitted_user_ids: card_params[:users_with_permission_ids],
        permitted_team_ids: card_params[:teams_with_permission_ids])

      if @card.journey_card? && @card.card_subtype == 'weekly'
        start_date = params[:card][:journey_start_date].present? ? Time.zone.at(params[:card][:journey_start_date].to_i) : nil
        JourneyPackRelation.find_by(cover: @card).update_attributes!(start_date: start_date)
      end
      #  treat poll card creation differently due to scoring
      #  score for normal card creation: 40
      #  score for poll card creation : 20
      # score for videostream card is 80
      if @card.poll_card?
        record_card_action_metric(@card, 'card', 'poll_create')
      elsif @card.video_stream?
        record_card_action_metric(@card, 'card', 'videostream_create')
      else
        record_card_create_metric(@card)
      end
      LogHistoryJob.perform_later(
        "entity_type" => 'card',
        "entity_id" => @card.id,
        "changes" => @card.previous_changes.merge(topics: ["", card_attrs[:topics]&.join(",")]).as_json,
        "association_changes" => CardHistory.association_changes(@card).as_json,
        "user_id" => current_user.id
      )
      # resize image
      ProcessFilestackImagesJob.perform_later(@card.id) if @card.previous_changes.keys.include?("filestack")
    else
      return render_unprocessable_entity(@card.full_errors)
    end
    render 'show'
  end

  api :GET, 'api/v2/cards/:id/edit', 'Get details of a card required during edit'
  description <<-EOS
    This end-point will give all the data required to support edit functionality of a smartcard.
  EOS
  def edit
    head :not_found and return if @card.pack_card? || @card.journey_card?
    # Send filestack as url gets expired and empty image is displayed when modal is opened
    # same with resource object.
    fields = %w[
      language        auto_complete   duration
      teams_permitted users_permitted completed_percentage
      file_resources  channel_ids     non_curated_channel_ids
      paid_by_user    show_restrict   users_with_access
      teams(id,label) tags(id,name)   channels(id,label,is_private)
      channels        tags            teams
      filestack       provider        provider_image
    ]
    fields << 'leaps' if @card.poll_card?
    fields << 'resource' if @card.resource_id.present?
    fields = fields.join(",")

    card = CardBridge.new(
      [@card],
      user: current_user,
      fields: fields,
      options: { is_native_app: is_native_app? }
    ).attributes[0]

    card.merge!(readableCardType: @card.card_type_display_name)
    render json: card
  end

  def secured_urls
    card = CardBridge.new(
      [@card],
      user: current_user,
      fields: "file_resources,filestack,resource",
      options: { is_native_app: is_native_app? }
    ).attributes[0]
    render json: card
  end

  api :PUT, 'api/v2/cards/:id', 'update smartbite/insight/card'
  description <<-EOS
    This endpoint is to update card.
  EOS
  param_group :card
  param :id, Integer, desc: 'Id of the card', required: true
  param :fields, String, desc: 'Gives On-demand response', required: false
  authorize :update, Permissions::MANAGE_CARD
  def update
    if update_card(params)
      if !is_native_app? && (@card.card_type == 'pack' || @card.card_type == 'journey')
        fields = params[:fields].present? ? params[:fields] : 'id,published_at,state,message,title,is_public,teams,channels,users_with_access'
        card = CardBridge.new(
          [@card],
          user: current_user,
          fields: fields,
          options: { is_native_app: is_native_app? }
        ).attributes[0]
        render json: card
      else
        render 'show'
      end
    else
      return render_unprocessable_entity(@card.full_errors)
    end
  end

  api :PUT, 'api/v2/cards/:id/promote', 'Promote a card'
  param :id, Integer, 'Id of card to be promoted', required: true
  authorize :promote, Permissions::MANAGE_CARD
  def promote
    return render_unprocessable_entity('Card is already promoted') if @card.is_official
    if @card.promote
      head :ok
    else
      render_unprocessable_entity(@card.errors.full_messages.join(', '))
    end
  end

  api :PUT, 'api/v2/cards/:id/unpromote', 'Unpromote a card'
  param :id, Integer, 'Id of card to be unpromoted', required: true
  authorize :unpromote, Permissions::MANAGE_CARD
  def unpromote
    return render_unprocessable_entity('Card is not promoted') unless @card.is_official
    if @card.unpromote
      head :ok
    else
      render_unprocessable_entity(@card.errors.full_messages.join(', '))
    end
  end

  api :GET, 'api/v2/cards/:id/report', 'Get details of each card i.e assessment'
  param :id, Integer, desc: "Card id", required: true
  def report
    AssessmentReportGenerationJob.perform_later(card_id: params[:id], user_id: current_user.id)

    head :ok
  end


  api :PUT, 'api/v2/cards/:id/rate', 'update/insert smartbite/insight/card rating'
  description <<-EOS
    This endpoint is to insert/update the card rating.
  EOS
  param :id, Integer, desc: "Card id", required: true
  param :rating, Integer, desc: "Card Rating, A number between 1 and 5, inclusive", :required => false
  param :level, String, desc: "Card Level, beginner, intermediate, advanced", :required => false
  def rate
    card_rating = CardsRating.where(
      card: @card,
      user_id: current_user.id
    ).first_or_initialize
    card_rating.rating = params[:rating].to_i if params[:rating].present?
    card_rating.level = params[:level] if params[:level].present?
    if !card_rating.save
      return render_unprocessable_entity(card_rating.errors.full_messages.join(", "))
    end
    track_rating()
    LogHistoryJob.perform_later(
      "entity_type" => 'card',
      "entity_id" => @card.id,
      "changes" => card_rating.previous_changes.select {|k, v| k == 'level'}.as_json,
      "user_id" => current_user.id
    ) if params[:level].present?
    render 'show'
  end

  api :GET, 'api/v2/cards/:id', 'Get detailed info about card'
  description <<-EOS
    Render card details. This will return all the card information available
  EOS
  param :id, Integer, "Card id", required: true
  def show
    #Admin and Author should be able to fetch draft card content
    head :not_found and return unless @card.published? || current_user.authorize?(Permissions::ADMIN_ONLY) || current_user.id == @card.author.id
    load_is_following_for([@card.author_id], 'User', current_user)
    @assignment = current_user.assignments.where(assignable_type: "Card", assignable_id: @card.id).first
  end

  api :DELETE, 'api/v2/cards/:id', "delete a card"
  param :delete_dependent, String, desc: 'In case of a pathway, set to true if all cards in the pathway are to be deleted as well. Cards that have an independent existence outside of the pathway will still NOT be deleted. Default false', required: :false
  description <<-EOS
    Delete user card. Only card authors or admins can delete card
    Card indexed immediately.
  EOS
  param :id, Integer, desc: "Card id", required: true
  authorize :destroy, Permissions::MANAGE_CARD
  def destroy
    if delete_card(params)
      head :ok and return
    else
      return render_unprocessable_entity(@card.errors.full_messages.join(", "))
    end
  end

  api :POST, 'api/v2/cards/delete', 'Delete an array of cards from the cms'
  param :card_ids, Array, 'Array of card ids to delete', required: true
  authorize :destroy_cards, Permissions::ADMIN_ONLY
  def destroy_cards
    card_ids = get_array_ids(params[:card_ids])
    card_ids.map!(&:to_i)
    cards = current_org.cards.where(id: card_ids)
    not_found_ids = card_ids - cards.pluck(:id)
    cards.find_each do |card|
      card.current_user = current_user
      card.destroy
    end
    invalid_ids = card_ids & cards.pluck(:id)
    invalid_ids = not_found_ids | invalid_ids
    if invalid_ids.blank?
      return head :ok
    else
      render json: { errors: invalid_ids }
    end
  end

  api :POST, 'api/v2/cards/:id/add', 'Add card to existing pathway'
  param :id, Integer, desc: 'Id of the pathway card', required: true
  param :content_id, String, desc: 'Id of the content', required: true
  authorize :add, Permissions::ADD_TO_PATHWAY
  def add
    unless params[:content_id].present?
      return render_unprocessable_entity("Missing required params")
    end
    params[:content_ids] = [params[:content_id]]
    add_to_pathway
  end

  api :POST, 'api/v2/cards/:id/journey_add', 'Add pack with journey subtype to existing journey'
  param :id, Integer, desc: 'Id of the journey card', required: true
  param :content_id, String, desc: 'Id of the content', required: true
  authorize :journey_add, Permissions::CREATE_JOURNEY
  def journey_add
    unless params[:content_id].present?
      return render_unprocessable_entity("Missing required params")
    end
    params[:content_ids] = [params[:content_id]]
    add_to_journey
  end

  api :POST, 'api/v2/cards/:id/share', 'Share a card with a group'
  param :team_ids, Array, desc: "team ids to share", required: false
  param :user_ids, Array, desc: "user ids to share", required: false
  param :remove_team_ids, Array, desc: "team ids to unshare", required: false
  param :remove_user_ids, Array, desc: "user ids to unshare", required: false
  authorize :share, Permissions::SHARE
  def share
    unless @card.shareable?(current_user)
      return render_unauthorized('Permission denied')
    end

    @card.share_card(team_ids: params[:team_ids]||= [], user_ids: params[:user_ids]||= [], user: current_user) if params[:team_ids] || params[:user_ids]
    @card.unshare_card(team_ids:  params[:remove_team_ids]||= [], user_ids: params[:remove_user_ids]||= []) if params[:remove_team_ids] || params[:remove_user_ids]
  end

  api :POST, 'api/v2/cards/:id/post_to_channel', 'Post a card in a channel'
  param :channel_ids, Array, desc: "array of channel ids", required: false
  param :remove_channel_ids, Array, desc: "unpost card from channel_ids", required: false
  authorize :post_to_channel, Permissions::MANAGE_CARD
  def post_to_channel
    @card.current_user = current_user

    if params[:card][:channel_ids].present?
      channel_ids = get_channel_ids_to_process
      @required, @not_required = @card.split_by_curation(channel_ids)
      @card.post_to_channel(channel_ids) if channel_ids.present?
    end
    @card.remove_from_channel(params[:card][:remove_channel_ids]) if params[:card][:remove_channel_ids].present?
  end

  api :POST, 'api/v2/cards/:id/batch_add', 'Add array of cards to existing pathway'
  param :id, Integer, desc: 'Id of the pathway card', required: true
  param :content_ids, Array, desc: 'Array Ids of the content', required: true
  authorize :batch_add, Permissions::ADD_TO_PATHWAY
  def batch_add
    add_to_pathway
  end

  api :POST, 'api/v2/pathways/:id/cards/batch_remove', 'Remove array of cards from the pathway'
  param :id, Integer, desc: 'Id of the pathway card', required: true
  param :card_ids, Array, desc: 'Array Ids of cards which should be removed from the pathway', required: true
  authorize :remove, Permissions::MANAGE_CARD
  def batch_remove
    unless params[:card_ids].is_a?(Array)
      render_unprocessable_entity("Missing required params") and return
    end

    delete_dependent = params[:delete_dependent]&.to_bool || false
    cards_to_remove_ids = @cover.pack_cards.where(id: params[:card_ids]).pluck(:id)
    errors = []

    if (params[:card_ids] - cards_to_remove_ids).any?
      errors.concat(params[:card_ids] - cards_to_remove_ids)
    end

    cards_to_remove_ids.each do |card_id|
      unless @cover.remove_card_from_pathway(card_id, 'Card', {delete_dependent: delete_dependent})
        errors.push(card_id)
      end
    end

    if errors.any?
      message = "There were errors when removing cards with the following ids: #{errors}"
      render_with_status_and_message(message, status: :ok)
    else
      head :ok
    end
  end

  api :POST, 'api/v2/journey/:id/cards/journey_batch_remove', 'Remove array of pathways from the journey'
  param :id, Integer, desc: 'Id of the journey card', required: true
  param :card_ids, Array, desc: 'Array Ids of pathways which should be removed from the journey', required: true
  param :delete_dependent, String, desc: 'true/false for deletion hidden cards in section', required: false
  authorize :remove, Permissions::MANAGE_CARD
  def journey_batch_remove
    cards_to_remove_ids = @cover.journey_pathways.where(id: params[:card_ids]).pluck(:id)
    errors = []

    if (params[:card_ids] - cards_to_remove_ids).any?
      errors.concat(params[:card_ids] - cards_to_remove_ids)
    end

    cards_to_remove_ids.each do |card_id|
      unless @cover.remove_card_from_journey(card_id, params[:delete_dependent]&.to_bool)
        errors.push(card_id)
      end
    end

    if errors.any?
      message = "There were errors when removing cards with the following ids: #{errors}"
      render_with_status_and_message(message, status: :ok)
    else
      head :ok
    end
  end

  api :POST, 'api/v2/cards/:id/journey_batch_add', 'Add array of packs with journey subtype to existing journey'
  param :id, Integer, desc: 'Id of the journey card', required: true
  param :content_ids, Array, desc: 'Array Ids packs with journey subtype', required: true
  authorize :journey_batch_add, Permissions::CREATE_JOURNEY
  def journey_batch_add
    add_to_journey
  end

  api :POST, 'api/v2/cards/:id/add_to_journeys', 'Add a packs to array of journeys to existing journey'
  param :id, Integer, desc: 'Id of the Pathway card', required: true
  param :journey_ids, Array, desc: 'Array Ids of journey ids', required: true
  authorize :add_to_journeys, Permissions::CREATE_JOURNEY
  def add_to_journeys
    if params[:journey_ids].blank? || params[:id].blank?
      return render_unprocessable_entity('Missing required params')
    end
    add_pathway_to_journeys
  end

  api :POST, 'api/v2/cards/:id/add_to_pathways', 'Add card to array of existing pathways'
  param :id, Integer, desc: 'Id of the card', required: true
  param :pathway_ids, Array, desc: 'Array of pathway ids', required: true
  authorize :add_to_pathways, Permissions::ADD_TO_PATHWAY
  def add_to_pathways
    unless params[:pathway_ids].present?
      render_unprocessable_entity("Missing required params") and return
    end

    covers = current_user.cards.pathways.
        where(state: [Card::STATE_DRAFT, Card::STATE_PUBLISHED], id: params[:pathway_ids])

    @errors = []

    covers.each do |cover|
      unless cover.add_card_to_pathway(@card.id, 'Card')
        @errors << cover
      end
    end

    @successes = covers - @errors
  end

  api :POST, 'api/v2/cards/:id/remove', 'Remove card from pathway'
  param :id, Integer, desc: 'Id of the pathway card', required: true
  param :content_id, Integer, desc: 'Id of the content ', required: true
  param :content_type, String, desc: 'Content Type: For now only Card is supported', required: true
  authorize :remove, Permissions::MANAGE_CARD
  def remove
    unless (params[:content_id].present? && params[:content_type].present?)
      return render_unprocessable_entity("Missing required params")
    end

    if @cover.id == params[:content_id].to_i && params[:content_type] == 'Card'
      return render_unprocessable_entity('Cannot remove the cover card from a collection.')
    end

    if @cover.remove_card_from_pathway(params[:content_id], params[:content_type], {delete_dependent: true})
      head :ok
    else
      return render_unprocessable_entity("Error while removing card from pathway")
    end
  end

  api :POST, 'api/v2/cards/:id/journey_remove', 'Remove card from journey'
  param :id, Integer, desc: 'Id of the journey card', required: true
  param :content_id, Integer, desc: 'Id of the content', required: true
  param :delete_dependent, String, desc: 'true/false for deletion hidden cards in section', required: false
  authorize :remove, Permissions::MANAGE_CARD
  def journey_remove
    return render_unprocessable_entity('Missing required params') if params[:content_id].blank?
    return render_unprocessable_entity('Cannot remove the cover card from a collection.') if @cover.id == params[:content_id].to_i

    if @cover.remove_card_from_journey(params[:content_id], params[:delete_dependent]&.to_bool)
      head :ok
    else
      render_unprocessable_entity('Error while removing card from journey')
    end
  end

  api :POST, 'api/v2/cards/:id/journey_reorder', 'Reorder sections in journey'
  param :id, Integer, desc: 'ID of the journey card', required: true
  param :from, Integer, desc: 'Position of the section in journey', required: true
  param :to, Integer, desc: 'Position of the section in journey', required: true
  authorize :journey_reorder, Permissions::MANAGE_CARD
  def journey_reorder
    return render_unprocessable_entity('Missing parameters') unless params[:from] && params[:to]
    if @cover.reorder_journey(params[:from], params[:to])
      head :ok
    else
      render_unprocessable_entity('Error while moving section in journey')
    end
  end

  api :POST, 'api/v2/cards/:id/custom_order', 'Reorder cards in pathway or journey sections'
  param :id, Integer, desc: 'ID of the card', required: true
  param :content_ids, Array, desc: 'Array of card ids in provided order'
  authorize :custom_order, Permissions::MANAGE_CARD
  def custom_order
    unless params[:content_ids].is_a?(Array) || %w[pack journey].include?(@cover.card_type)
      return render_unprocessable_entity('Wrong parameters')
    end
    if @cover.relations_reorder(order: params[:content_ids])
      head :ok
    else
      render_unprocessable_entity('Error while reordering')
    end
  end

  api :POST, 'api/v2/cards/:id/publish', 'Publish a pathway'
  authorize :publish, Permissions::MANAGE_CARD
  def publish
    if @cover.publish && @cover.save

      @cover.send_card_create_or_share_notification(event: Card::EVENT_NEW_CARD_SHARE,
        user_ids: @cover.users_with_access_ids,
        team_ids: @cover.shared_with_team_ids, share_by_id: current_user.id,
        permitted_user_ids: params.dig(:card, :users_with_permission_ids),
        permitted_team_ids: params.dig(:card, :teams_with_permission_ids))

      record_card_action_metric(@cover, 'collection', 'publish')
      render json: {}
    else
      return render_unprocessable_entity(@cover.errors.full_messages.join(", "))
    end
  end

  api :POST, 'api/v2/cards/publish', 'Publish an array of cards from the cms'
  param :card_ids, Array, 'Array of card ids to publish', required: true
  authorize :publish_cards, Permissions::ADMIN_ONLY
  def publish_cards
    card_ids = get_array_ids(params[:card_ids])
    cards = current_org.cards.where(id: card_ids)
    invalid_ids = card_ids - cards.pluck(:id)
    cards.each do |card|
      if card.publish!
        track_action(card, 'content_publish')
      else
        invalid_ids.push(card.id)
      end
    end
    published_ids = card_ids - invalid_ids
    render json: {successes: published_ids, errors: invalid_ids}
  end

  api :PUT, 'api/v2/cards/:id/transfer_ownership', 'Transfer Ownership of cards'
  param :user_id, Integer, 'transfer ownership to this user', required: true
  authorize :transfer_ownership, Permissions::CHANGE_AUTHOR
  def transfer_ownership
    return render_unprocessable_entity('Missing parameters') unless params[:user_id]

    @card.old_author_id = @card.author_id
    @card.current_user = current_user
    if @card.update(author_id: params[:user_id])
      LogHistoryJob.perform_later(
        "entity_type" => 'card',
        "entity_id" => @card.id,
        "changes" => @card.previous_changes.as_json,
        "user_id" => current_user.id
      )
      head :ok
    else
      render_unprocessable_entity(@card.errors.full_messages.join(', '))
    end
  end

  api :POST, 'api/v2/cards/:id/reorder', 'Re-order cards in a pathway'
  param :id, Integer, desc: 'Id of the pathway card', required: true
  param :from, Integer, desc: 'Position of the card in pathway ', required: true
  param :to, Integer, desc: 'Id of the content ', required: true
  authorize :reorder, Permissions::MANAGE_CARD
  def reorder
    if params[:from] && params[:to]
      if @cover.reorder_pathway(params[:from], params[:to])
        head :ok
      else
        return render_unprocessable_entity("Error while removing card from pathway")
      end
    else
      return render_unprocessable_entity("Missing parameters")
    end
  end

  api :POST, 'api/v2/cards/:id/responses', "Record response on a poll card"
  param :id, Integer, desc: 'Id of the poll card', required: true
  param :selections, Hash, desc: "Array of option ids selected by the user", required: true
  def responses
    @card = get_poll_card(params[:id])
    qqa = get_quiz_question_attempt(
      @card,
      current_user,
      params[:selections]
    )
    if qqa.save
      if qqa.poll_answer?
        record_card_action_metric(@card, 'card', 'poll_respond')
      end
      head :ok and return
    else
      return render_unprocessable_entity("Could not save response")
    end
  end

  api :PUT, 'api/v2/cards/:id/start', 'start a piece of content'
  description <<-EOS
    This will mark a card as 'started' for a user. This will also in turn mark an assignment as started if the card is assigned to the user
  EOS
  param :id, Integer, 'Card id', required: true
  def start
    if @card.mark_feature_disabled?
      render_unprocessable_entity('feature is disabled for this card') and return
    end

    user_content_completion = current_user.user_content_completions.where(completable: @card).first_or_create
    return render_unprocessable_entity('Can not start already started or completed card') if %w[completed started].include?(user_content_completion.state)
    user_content_completion.start
    if user_content_completion.save
      render partial: 'api/v2/user_content_completions/details', locals: {ucc: user_content_completion}
    else
      render_unprocessable_entity(user_content_completion.errors.full_messages)
    end
  end

  api :POST, 'api/v2/cards/:id/complete', "complete a piece of content. It also supports ECL cards"
  param :id, String, desc: 'Card Id or ECL id', required: true
  param :topics, Array, desc: 'An array of topics for the content', required: false
  authorize :complete, Permissions::MARK_AS_COMPLETE
  def complete
    card = load_card_from_compound_id(params[:id])

    render_not_found('Content not found') and return unless card

    if card.mark_feature_disabled?
      render_unprocessable_entity('feature is disabled for this card') and return
    end

    user_content_completion = current_user.user_content_completions.where(completable: card).first_or_create
    user_content_completion.topics = params[:topics] unless params[:topics].blank?

    if !user_content_completion.started? && !user_content_completion.completed?
      user_content_completion.start
    end

    if !user_content_completion.completed? && user_content_completion.started?
      user_content_completion.complete
    end

    if !user_content_completion.save
      render_unprocessable_entity(user_content_completion.errors.full_messages) and return
    end

    if user_content_completion.completed?
      UpdateClcScoreForUserJob.perform_later(card.id, current_user.id, current_user.organization_id, "Organization")
      @user_badge = UserBadgeService.new("Card", card, current_user.id).assign_badge
      track_mark_as_completion(card)
    end

    render partial: 'api/v2/user_content_completions/details', locals: {ucc: user_content_completion} and return
  end

  api :POST, 'api/v2/cards/:id/uncomplete', "uncomplete a piece of content"
  param :id, String, desc: 'Card Id or ECL id', required: true
  authorize :uncomplete, Permissions::MARK_AS_COMPLETE
  def uncomplete
    card = load_card_from_compound_id(params[:id])
    render_not_found('Content not found') and return unless card

    if card.mark_feature_disabled?
      render_unprocessable_entity('feature is disabled for this card') and return
    end

    user_content_completion = current_user.user_content_completions.find_by(completable: card)

    render_unprocessable_entity('Card must be completed') and return unless user_content_completion&.completed?

    if user_content_completion.completed?
      user_content_completion.uncomplete
    end

    unless user_content_completion.save
      return render_unprocessable_entity(user_content_completion.errors.full_messages)
    end

    if user_content_completion.initialized?
      track_mark_as_uncompletion(card)
    end

    render partial: 'api/v2/user_content_completions/details', locals: {ucc: user_content_completion} and return
  end

  api :POST, 'api/v2/cards/:id/dismiss', "Dismiss card. Dismissed card will not show up in users feed"
  param :id, Integer, desc: "Card Id", required: true
  authorize :dismiss, Permissions::DISMISS_CONTENT
  def dismiss
    dismissed_content = current_user.dismissed_contents.new(content: @card)
    if dismissed_content.save
      head :ok
    else
      return render_unprocessable_entity(dismissed_content.errors.full_messages)
    end
  end

  api :PUT, 'api/v2/channels/:channel_id/cards/:id/curate', "Curate a card in a channel"
  param :channel_id, Integer, desc: "Channel Id", required: true
  param :id, Integer, desc: "Card Id", required: true
  authorize :curate, Permissions::CURATE_CONTENT
  def curate
    if @channels_card.curate && @channels_card.save
      record_card_approved_event
      head :ok and return
    else
      return render_unprocessable_entity(@channels_card.errors.full_messages.join(", "))
    end
  end

  api :PUT, 'api/v2/channels/:channel_id/cards/:id/skip', "Skip a card in a channel. This card wont be shown on any user pages"
  param :channel_id, Integer, desc: "Channel Id", required: true
  param :id, Integer, desc: "Card Id", required: true
  authorize :skip, Permissions::CURATE_CONTENT
  def skip
    if @channels_card.skip && @channels_card.save
      record_card_rejected_event
      head :ok and return
    else
      return render_unprocessable_entity(@channels_card.errors.full_messages.join(", "))
    end
  end

  api :PUT, 'api/v2/channels/:channel_id/cards/:id/archive', "archive a card in a channel"
  param :channel_id, Integer, desc: "Channel Id", required: true
  param :id, Integer, desc: "Card Id", required: true
  authorize :archive, Permissions::CURATE_CONTENT
  def archive
    if @channels_card.archive && @channels_card.save
      head :ok and return
    else
      return render_unprocessable_entity(@channels_card.errors.full_messages.join(", "))
    end
  end

  api :POST, 'api/v2/cards/archive', 'Archive an array of cards from the cms'
  param :card_ids, Array, 'Array of card ids to archive', required: true
  authorize :archive_cards, Permissions::ADMIN_ONLY
  def archive_cards
    card_ids = get_array_ids(params[:card_ids])
    cards = current_org.cards.where(id: card_ids)
    invalid_ids = card_ids - cards.pluck(:id)
    cards.each do |card|
      unless card.archive!
        invalid_ids.push(card.id)
      end
    end
    published_ids = card_ids - invalid_ids
    render json: {successes: published_ids, errors: invalid_ids}
  end

  api :GET, 'api/v2/cards/:id/addable_pathways', "Returns list of pathways that this card can be added to"
  param :id, Integer, desc: 'Id of the card', required: true
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 15', required: false
  param :sort, String, "Attribute name to sort by param"
  param :order, String, "Sort order, asc or desc"
  authorize :addable_pathways, Permissions::ADD_TO_PATHWAY
  def addable_pathways
    return render_not_found('Card not found or was lost connection with ECL') if @card.blank?

    pack_ids = @card.pack_ids
    query = current_org.cards.visible_cards.
      where(state: [Card::STATE_DRAFT, Card::STATE_PUBLISHED], card_type: 'pack', author_id: current_user.id).
      where.not(id: pack_ids)

    if params[:sort].present?
      order = %w[asc desc].include?(params[:order]&.downcase) ? params[:order] : 'desc'
      query = query.order(params[:sort] => order)
    end

    @total = query.count
    @cards = query.limit(limit).offset(offset)
  end

  # Invoked when a pathway is accessed in modal and standalone views.
  api :GET, 'api/v2/cards/:id/pathway', 'Get detailed info about card'
  description <<-EOS
    Render pathway details. This will return info required by pathway and the cards in it.
  EOS

  api :GET, 'api/v2/cards/:id/addable_journeys', 'Returns list of journeys that this card can be added to'
  param :id, Integer, desc: 'Id of the card', required: true
  param :type, ['card', 'pack'], desc: 'addable card is pathway or smartcard', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 15', required: false
  authorize :addable_journeys, Permissions::CREATE_JOURNEY
  def addable_journeys
    if @card.journey_card?
      return render_unprocessable_entity("journey card can't be added to journeys")
    end
    is_pack = (params[:type] == 'pack')
    valid_ids = @card.journeys_sections(pack: is_pack)
    query = Card.where(id: valid_ids[:card_ids],
                        author_id: current_user.id,
                        state: [Card::STATE_DRAFT, Card::STATE_PUBLISHED],
                        card_type: 'journey')
    if !is_pack
      query = query.includes(journey_pack_relations: :card)
                    .where(journey_pack_relations: { from_id: valid_ids[:jpr_ids] })
    end
    @total = query.count
    @cards = query.limit(limit).offset(offset)
  end

  api :GET, 'api/v2/cards/:id/metric', "Returns card metrics"
  param :id, Integer, desc: 'Id of the card', required: true
  authorize :metric, Permissions::VIEW_CARD_ANALYTICS
  def metric
    # results = results.group_by {|s| s['tags']['event']}.with_indifferent_access
    #=================================================================================
    # NOTE: We don't have good data prior to Jan 17th release. Commented out for now.
    #=================================================================================

    # data[:bookmarks_count] = event_count.call(results[:card_bookmarked]) - event_count.call(results[:card_unbookmarked])
    # data[:likes_count] = event_count.call(results[:card_liked]) - event_count.call(results[:card_unliked])
    # data[:comments_count] = event_count.call(results[:card_comment_created]) - event_count.call(results[:card_comment_deleted])
    # data[:completes_count] = event_count.call(results[:card_marked_as_complete]) - event_count.call(results[:card_marked_as_uncomplete])

    data = {
      bookmarks_count: current_org.users.joins(:bookmarks).where("bookmarkable_id = ? and bookmarkable_type =?", @card.id, 'Card').count(:id),
      likes_count: current_org.users.joins(:votes).where("votable_id = ? and votable_type =?", @card.id, 'Card').count(:id),
      comments_count: current_org.users.joins(:comments).where("commentable_id = ? and commentable_type =?", @card.id, 'Card').count(:id),
      completes_count: current_org.users.joins(:user_content_completions).where("completable_id = ? and completable_type =? and state = ?", @card.id, 'Card', 'completed').count(:id),
      views_count: @card.all_time_views_count
    }

    if @card.is_a_poll_question?
      # poll_question can exist just by itself as well
      quiz_attempts = @card.poll_attempts.pluck(:quiz_question_id)
      data.merge!(@card.correct_attempts_count(quiz_attempts))
    end
    render json: data, status: :ok
  end

  api :GET, 'api/v2/cards/:id/metric_users', "Returns metric users for a particular action on card"
  param :id, Integer, desc: 'Id of the card', required: true
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :action_type, String, desc: "Action on card, eg: like, bookmark, comment, view, mark_as_complete", required: true
  def metric_users
    limit = params[:limit] ? params[:limit].to_i : 10
    active_users = current_org.users.not_suspended
    @users =
      case params[:action_type]
      when 'like'
        active_users.joins(:votes).where("votable_type = ? and votable_id = ?", 'Card', @card.id).limit(limit).offset(offset)
      when 'bookmark'
        active_users.joins(:bookmarks).where("bookmarkable_type = ? and bookmarkable_id = ?", 'Card', @card.id).limit(limit).offset(offset)
      when 'comment'
        active_users.joins(:comments).where("commentable_type =? and commentable_id = ?", 'Card', @card.id).limit(limit).offset(offset)
      when 'mark_as_complete'
        active_users.joins(:user_content_completions).where("completable_type =? and completable_id = ? and state = '#{UserContentCompletion::COMPLETED}'", 'Card', @card.id).limit(limit).offset(offset)
      else
        []
      end
  end

  api :GET, 'api/v2/cards/user_metric', 'Get user metric for cards'
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  def cards_user_metric
    @metrics = UserContentLevelMetric.where(period: :all_time, offset: 0, user_id: current_user.id).
                                      group("user_id, content_id, content_type").
                                      select("user_id, content_id, content_type, sum(smartbites_score) as smartbites_score").
                                      order("smartbites_score DESC").
                                      limit(limit).
                                      offset(offset)
  end

  api :GET, 'api/v2/cards/:id/time_range_analytics', 'Get temporal analytic report for card'
  param :id, Integer, desc: 'Id of card', required: true
  param :from_date, String, desc: 'Date with mm/dd/yyyy format', required: true
  param :to_date, String, desc: 'Date with mm/dd/yyyy format', required: true
  param :action_types, Array, desc: 'Action name. Available case: like, unlike, playback, comment, follow,
    unfollow, assign, bookmark, mark_completed_self, mark_completed_assigned, add_to_channel, add_to_pathway', required: true
  def time_range_analytics
    @metrics = AnalyticsReport::ContentActivityReport.new(card_id: @card.id, from_date: params[:from_date],
      to_date: params[:to_date], action_types: params[:action_types]).generate_report
  end

  api :GET, 'api/v2/cards/new_content_count', 'Get number of new cards in featured, required and curated content'
  param :last_access_at, String, desc: 'Time of last access, format: 1505832150', required: true
  param :content_types, Array, desc: 'Array of content types. Available case: featured, required, curated.', required: true
  def new_content_count
    unless params[:last_access_at].present? && params[:content_types].present?
      render_unprocessable_entity("Missing required params") and return
    end

    unless current_user.authorize?(Permissions::ADMIN_ONLY) || current_user.authorize?(Permissions::CURATE_CONTENT)
      params[:content_types].delete 'curated'
    end

    new_content_data = AnalyticsReport::NewContentReport
        .new(last_access: params[:last_access_at], content_types: params[:content_types], viewer: current_user)
        .generate_report

    render json: new_content_data, status: :ok
  end

  api :POST, 'api/v2/cards/:id/set_lock', 'Lock card in pathway'
  param :pathway_id, Integer, desc: 'Pathway ID', required: true
  param :locked, String, desc: 'Status of lock true/false', required: true
  param :id, Integer, desc: 'Card ID', required: true
  authorize :lock, Permissions::MANAGE_CARD
  def set_lock
    if @cpr.update_attributes(locked: @status)
      head :ok
    else
      render_unprocessable_entity('Error while setting lock on card')
    end
  end

  api :PUT, 'api/v2/cards/course_status', 'Update course subscription'
  param :status, String, desc: 'Status of course'
  param :url, String, desc: 'Url of the course'
  param :name, String, desc: 'Name of the course'
  def course_status
    status = params[:status]&.downcase
    if status == "start"
      params[:status] = "started"
    elsif status == "complete"
      params[:status] = "completed"
    end

    if !["assigned", "started", "completed", "expired"].include?(params[:status]&.downcase)
      return render_unprocessable_entity("Status is invalid")
    end

    if params[:url] || params[:name]
      card = find_card
      if card.present?
        if ['expired', 'completed'].include?(params[:status]&.downcase)
          current_user.expire_card_subscription(card: card)
        end
        LrsAssignmentService.new( user_id: current_user.id,
                                  card: card,
                                  status: params[:status] || "completed").run
        head :ok
      else
        render_not_found('card not found with given url')
      end
    else
      render_unprocessable_entity
    end
  end

  def find_card
    if params[:url]
      Card.find_by_resource_url(params[:url], @current_org.id)
    elsif params[:name]
      Card.find_in_source_with_message(params[:name], @source_id)
    end
  end

  api :POST, 'api/v2/cards/:id/trash', 'Trashing a reported content'
  param :id, Integer, desc: 'Card ID', required: true
  authorize :trash, Permissions::REPORT_CONTENT
  def trash
    if @card
      Card.transaction do
        # Soft delete card reportings
        @card.card_reportings.update_all(deleted_at: DateTime.now)
        @card.trash_from_ecl

        # Update the pathways in which the card is present
        CardPackRelation.where(from_id: @card.id).update_all(deleted: true)

        @card.organization.invalidate_trashed_ecl_ids
        @card.update_column(:deleted_at, DateTime.now)
        CardReportingIndexingJob.perform_later(card_id: @card.id)
        head :ok
      end
    else
      render_not_found('Card not found')
    end
  end

  api :PUT, 'api/v2/cards/:id/unreport', 'Unreport a reported / trashed content'
  param :id, Integer, desc: 'Card ID', required: true
  authorize :unreport, Permissions::REPORT_CONTENT
  def unreport
    if @card
      Card.transaction do
        if @card.deleted?
          Card.restore(@card.id)
          @card.recover_content_from_ecl
          CardPackRelation.where(from_id: @card.id).update_all(deleted: false)
        else
          @card.card_reportings.update_all(deleted_at: Time.now)
        end
        @card.organization.invalidate_trashed_ecl_ids
        CardReportingTrashingJob.perform_later(card_id: @card.id)

        head :ok
      end
    else
      render_not_found('Card not found')
    end
  end

  api :GET, 'api/v2/cards/:id/card_reportings', 'List all card reportings for a particular card'
  param :id, Integer, desc: 'Card ID', required: true
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  authorize :card_reportings, Permissions::ADMIN_ONLY
  def card_reportings
    @card_reportings = @card.fetch_card_reportings.order('created_at DESC').limit(limit).offset(offset)
  end

  api :GET, 'api/v2/cards/:id/assignable', 'List all users or teams'
  param :id, Integer, desc: 'Card ID', required: true
  param :q, String, desc: 'Filter teams by this search term', required: false
  param :type, String, desc: 'Specify type of assignable category i.e teams or individuals', required: true
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 10', required: false
  param :sort, String, 'Attribute name to sort by param', required: false
  param :order, String, 'Sort order, asc or desc, default: asc', required: false
  def assignable
    if params[:id].blank? || params[:type].blank?
      return render_unprocessable_entity('Missing required params')
    end
    if %w[individuals teams].exclude?(params[:type])
      return render_unprocessable_entity('Invalid type')
    end

    if params[:type] == 'teams'
      filter_params = {}
      filter_params[:leaders_id] = current_user.id unless current_user.is_admin_user?
      res = Search::TeamSearch.new.search(viewer: current_user,
                                           filter: filter_params,
                                           offset: offset, limit: limit,
                                           sort: { key: 'name', order: 'asc' },
                                           load: true)
      @teams = res.results
      @assigned_teams = Team.assigned_groups_for_card(@card.id).where(organization: current_org).pluck(:id)
    else
      res = Search::UserSearch.new.search(q: params[:q], viewer: current_user,
                                        allow_blank_q: true,
                                        offset: offset, limit: limit,
                                        sort: :first_name,
                                        order: 'asc',
                                        load: true)
      @users = res.results
      @assigned_users = User.assigned_users_for_card(@card.id).where(organization: current_org).pluck(:id)
    end
  end

  api :GET, 'api/v2/cards/card_by_ids', 'Returns list of requested cards'
  param :card_ids, Array, desc: "Array of card ids", required: true
  def card_by_ids
    if params[:card_ids].nil? || params[:card_ids].empty?
      return render_unprocessable_entity('Missing required params')
    end
    @cards = Card.where(id: params[:card_ids], organization: current_user.organization)
    @cards = @cards.visible_cards_to_user(current_user)
    render 'index'
  end

  api :DELETE, 'api/v2/cards/:id/remove_assignments', 'Destroy assignments from card'
  param :id, Integer, desc: 'Card ID', required: true
  param :assignment_ids, Array, 'Ids of the assignment', required: false
  param :team_ids, Array, desc: 'Ids of teams', required: false
  description <<-EOS
    This method will destroy the assignments of card only if either of
    assignment_ids or team_ids is passed.
  EOS
  def remove_assignments
    return render_unprocessable_entity('Missing required params') unless params[:assignment_ids].present? || params[:team_ids].present?

    authorized = current_org.is_admin?(current_user) ||
      current_user.has_access_assign_content?

    return render_unauthorized('Permission denied') unless authorized

    card_assignments = @card.assignments
    if params[:assignment_ids]
      assignments = card_assignments.where(id: params[:assignment_ids])

      return render_not_found('No assignments found') unless assignments.present?
      
      team_assignments_for_user = TeamAssignment.where(assignment_id: assignments.pluck(:id))
      
      assignors = team_assignments_for_user
        .group("team_assignments.team_id").pluck(:team_id, :assignor_id).to_h
      
      if assignments.destroy_all
        @card.reprocess_team_assignment_metric(assignors.keys, assignors)
      end
    end
    
    team_ids = params[:team_ids]
    
    if team_ids
      team_assignments_for_card = TeamAssignment.joins(:assignment).where("assignments.assignable_id = #{@card.id}")

      all_teams_for_card = team_assignments_for_card.pluck(:team_id).uniq
      
      # {1=>123, 2=>221} here 1, 2 are teams and 123 and 221 are assignor_ids
      teams_assignors = team_assignments_for_card.group("team_assignments.team_id").pluck(:team_id, :assignor_id).to_h
      
      if all_teams_for_card.to_set == team_ids.to_set
        team_assignments = card_assignments.joins(:team_assignments)
          .where('team_assignments.team_id in (?)', team_ids)
      else
        team_assignments = team_assignments_for_card.where(team_id: team_ids)
      end
      return render_not_found('No assignments found') unless team_assignments.present?
      
      if team_assignments.destroy_all
        @card.reprocess_team_assignment_metric(team_ids, teams_assignors)
      end
    end

    head :ok
  end

  api :GET, 'api/v2/cards/deleted_cards', 'Return list of deleted cards related to user'
  param :only_own, [true, false], desc: 'Fetch deleted cards only where user is author'
  param :fields, String, desc: 'Necessary card fields delimited by comma'
  def deleted_cards
    cards = if params[:only_own]&.to_bool
      current_user.cards.with_deleted.where.not(deleted_at: nil)
    else
      current_user.user_related_deleted_cards
    end

    @total = cards.count
    @cards = cards.order(deleted_at: :desc).limit(limit).offset(offset)

    cards_resp = CardBridge.new(
      @cards, user: current_user, fields: params[:fields]
    ).attributes

    render json: {
      total: @total,
      cards: cards_resp
    }, status: :ok
  end

  api :GET, 'api/v2/duplicate_cards', 'get list duplicate cards'
  param :resource_url, String, desc: 'Url of resource', required: true
  def duplicate_cards
    url = params[:resource_url]
    unless url.is_a? String
      return render_unprocessable_entity('resource_url param has an invalid data type')
    end

    valid_url = url.start_with?(/(http|https):\/{2}/)

    return render_unprocessable_entity('Invalid resource url') unless valid_url
    visible_card_ids = current_org.cards.visible_to_userV2(current_user).pluck(:id)
    cards = Search::CardSearch.new.search(
        nil,
        current_user.organization,
        viewer: current_user,
        filter_params: { id: visible_card_ids, link_url: url },
        offset: offset,
        limit: limit,
        load: true
    )

    @cards = cards.results
    @total = cards.total
    fields = 'id,share_url'
    cards_info = CardBridge.new(@cards, user: current_user, fields: fields, options: { is_native_app: is_native_app? }).attributes
    render json: { cards: cards_info, total: @total }, status: :ok
  end

  api :POST, 'api/v2/cards/:id/add_to_mdp', 'Adding card to mdp_record'
  param :id, Integer, desc: 'Card ID', required: true
  param :form_details, Hash, desc: "Attributes for form_details", required: true do
    param :source_of_mdp, String, desc: "Source of MDP", required: true
    param :learning_activity, String, desc: "Learning Activity", required: true
    param :sub_competency_value, String, desc: "Sub - Competency selection", required: true
    param :additional_comments, String, desc: "Additional comments", required: false
  end
  def add_to_mdp
    @mdp_record = current_user.mdp_user_cards.build(card_id: @card.id,
      form_details: params[:form_details])

    unless @mdp_record.save
      render_unprocessable_entity("Error while creating MDP Record #{@mdp_record.errors.full_messages.join(",")}") and return
    end

    render 'mdp_card_detail'
  end

  api :GET, 'api/v2/cards/:id/mdp_card_detail', 'get mdp_record of user'
  param :id, Integer, desc: 'Card ID', required: true
  def mdp_card_detail
    @mdp_record = MdpUserCard.where(card_id: @card.id, user_id: current_user.id).first

    unless @mdp_record.present?
      render_unprocessable_entity("MDP User Record not found for card") and return
    end
  end

  private

  def notify_user
    case params[:action]
    when 'batch_add'
      NotifyUserJob.perform_later({action: 'added_to', card_ids: params[:content_ids].map(&:to_i), cover_ids: [params[:id].to_i], actor_id: current_user.id})
    when 'batch_remove'
      NotifyUserJob.perform_later({action: 'removed_from', card_ids: params[:card_ids].map(&:to_i), cover_ids: [params[:id].to_i], actor_id: current_user.id})
    when 'add_to_pathways'
      NotifyUserJob.perform_later({action: 'added_to', card_ids: [params[:id].to_i], cover_ids: params[:pathway_ids].map(&:to_i), actor_id: current_user.id})
    end
  end

  def set_channels_card
    @channel = current_org.channels.find(params[:channel_id])
    card = current_org.cards.find(params[:id])
    @channels_card = ChannelsCard.where(channel_id: @channel.id, card_id: card.id).first
  end

  def mdp_params
    params.require(:mdp_user_card).permit(
      form_details: [:source_of_mdp, :learning_activity, :sub_competency_value, :additional_comments]
    )
  end

  def card_params
    # set empty arrays if child associtions are being removed

    child_assoc_array = [:file_resource_ids, :filestack, :channel_ids, :topics]

    child_assoc_array.each do |assoc|
      if params[:card].key?(assoc) && params[:card][assoc].blank?
        params[:card][assoc] = []
      end
    end

  params.require(:card).permit(:message, :language, :resource_id, :card_type, :card_subtype, :state, :is_carousel, :title,
                               :hidden, :readable_card_type_name, :auto_complete, :provider, :provider_image,
                               :duration, :is_public, :is_paid, :can_be_reanswered, :author_id,
        user_taxonomy_topics: [:path, :label],
        filestack: [:name, :filename, :mimetype, :size, :source, :url, :handle, :status, :scorm_course, :thumbnail],
        topics:[],
        file_resource_ids: [],
        channel_ids: [],
        team_ids: [],
        users_with_access_ids: [],
        users_with_permission_ids: [],
        teams_with_permission_ids: [],
        options: [:id, :_destroy, :label, :file_resource_id, :is_correct],
        video_stream: [:status, :start_time, :uuid, recording: [:bucket, :key, :location, :checksum]],
        card_badging_attributes: [:title, :badge_id, :all_quizzes_answered],
        prices_attributes: [:amount, :currency, :id, :_destroy],
        card_metadatum_attributes: [:id, :plan, :level],
        resource_attributes: [:id, :description])
  end

  def load_cover
    if current_user.is_admin_user?
      @cover = Card.where(organization_id: current_org.id).friendly.find(params[:id])
    else
      @cover = current_user.cards.where(organization_id: current_org.id).friendly.find(params[:id])
    end
  end
  def validate_pack_card_type
    if @cover.blank? || !%w(pack journey).include?(@cover.card_type)
      return render_unprocessable_entity("Card is not a pathway")
    end
  end

  def load_card
    @card = load_card_from_compound_id(params[:id])
  end

  def load_reported_or_trashed_card
    @card = current_org.cards.with_deleted.friendly.find(params[:id])
    return render_not_found('Card not found') unless @card&.card_reportings&.with_deleted&.exists?
  end

  def track_rating(destroyed: false)
    track_opts = {
      user_id: current_user.id,
      object: "card",
      object_id: @card.id,
      action: "rate",
      platform: get_platform,
      device_id: cookies[:_d],
      organization_id: current_user.organization_id,
      referer: params[:referer].presence || request.referer
    }
    Tracking::track_act track_opts
  end

  def add_to_pathway
    cards = load_cards_from_compound_ids(params[:content_ids])

    errors = []
    cards.each do |card|
      unless @cover.add_card_to_pathway(card.id, 'Card')
        errors.push(card.id)
      end
    end

    # recalculate user content completions related to cover
    UpdatePathwayCompletionsJob.perform_later(
      cover_id: @cover.id, org_id: current_org.id
    )

    if errors.any?
      message = "There were errors when adding cards with the following ids: #{errors}"
      render_with_status_and_message(message, status: :ok)
    else
      head :ok
    end
  end

  def add_to_journey
    cards = load_cards_from_compound_ids(params[:content_ids])
    errors = []
    return render_unprocessable_entity('Too many sections. Allowed only up to 12') if cards.length > 12
    cards.each do |card|
      unless @cover.add_card_to_journey(card.id)
        errors.push(card.id)
      end
    end
    if errors.any?
      message = "There were errors when adding cards with the following ids: #{errors}"
      render_with_status_and_message(message, status: :ok)
    else
      head :ok
    end
  end

  def add_pathway_to_journeys
    if params[:journey_ids].reject!(&:blank?) && params[:journey_ids].blank?
      return render_unprocessable_entity('Missing required params')
    end
    journeys = load_cards_from_compound_ids(params[:journey_ids])
    errors = []
    pathway = load_card_from_compound_id(params[:id])
    if %w[pack journey].exclude?(pathway.card_type)
      return render_unprocessable_entity("Card is not a pathway")
    end
    journeys.each do |journey|
      unless journey.add_card_to_journey(pathway.id)
        errors.push(journey.id)
      end
    end
    if errors.any?
      message = "There were errors when adding cards with the following ids: #{errors}"
      render_with_status_and_message(message, status: :ok)
    else
      head :ok
    end
  end

  def validate_journey_type
    card = params[:card]
    return unless Card::SUB_TYPES['journey'].include?(card[:card_subtype])
    date_invalid = Time.zone.at(card[:journey_start_date].to_i) < Time.current.midnight
    if card[:card_subtype] == 'weekly' && (card[:journey_start_date].blank? || date_invalid)
      render_unprocessable_entity('journey_start_date should be present and valid')
    end
  end

  # Do not check curate permission for open channels
  def authorize_permission(permission)
    super unless (permission == Permissions::CURATE_CONTENT && @channel&.is_open?)
  end

  def get_poll_card(id)
    current_org.cards.where(card_type: 'poll').friendly.find(id)
  end

  def get_quiz_question_attempt(card, current_user, selections)
    QuizQuestionAttempt.new(
      quiz_question: card,
      user: current_user,
      selections: selections
    )
  end

  def check_pack
    return render_unprocessable_entity('Missing required params') if params[:pathway_id].nil? || params[:locked].nil?
    pack = current_org.cards.find_by(id: params[:pathway_id])
    return render_unprocessable_entity('Pathway card not found') if pack.nil?
    return render_unprocessable_entity('Card should be in provided pathway') unless pack.pack_cards.exists?(id: @card.id)
    @cpr = CardPackRelation.find_by(cover_id: params[:pathway_id], from_id: @card.id)
    return render_unprocessable_entity('Card relation not found') if @cpr.nil?
    @status = params[:locked] == 'true'
  end

  def record_card_approved_event
    record_card_moderation_event('card_approved_for_channel')
  end

  def record_card_rejected_event
    record_card_moderation_event('card_rejected_for_channel')
  end

  def record_card_moderation_event(event)
    card = @channels_card.card
    channel = @channels_card.channel

    record_custom_event(
      event: event,
      actor: :current_user,
      job_args: [{card: card}, {channel: channel}]
    )
  end

  def channel_card_rank(channel_id)
    @cards = @cards.joins(:channels_cards)
      .where(channels_cards: {channel_id: channel_id, state: ChannelsCard::STATE_CURATED})
      .select("cards.*, channels_cards.rank")
      .each{|d| d.channel_card_rank = d.rank}
  end

  def authorize_transfer_ownership
    unless (@current_user.is_admin_user? || @card.author_id == @current_user.id)
      return render_unauthorized('Permission denied')
    end

    unless @card.ugc?
      return render_unauthorized('Permission denied - This is NON-UGC content')
    end

    if @card.is_paid
      return render_unauthorized('Permission denied - This is paid content')
    end
  end

  def get_channel_ids_to_process
    new_channel_ids = set_channels(params[:card])
    @card.repost_to_channel(new_channel_ids) if @card.rejected_from_channels(new_channel_ids).present?
    new_channel_ids -= @card.channel_ids
  end
end

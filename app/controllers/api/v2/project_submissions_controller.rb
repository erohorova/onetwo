class Api::V2::ProjectSubmissionsController < Api::V2::BaseController

  has_limit_offset_constraints only: [:index], limit_default: 15, offset_default: 0
  before_action :find_project, only: :create
  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized to access channel'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :POST, '/api/v2/project_submissions', 'create project submission for a project card'
  description <<-EOS
    Create a project submission for a project card.
    It support attachments like Video.
  EOS
  param :project_id, Integer, desc: 'Id of the project', required: true
  param :description, String, desc: 'description of the submission', required: true
  param :filestack, String, desc: 'filestack object after uploading.', required: true
  def create
    @submission = @project.project_submissions.build(project_submission_params)
    @submission.submitter = current_user
    unless @submission.save
      return render_unprocessable_entity(@submission.full_errors)
    end
  end

  api :GET, '/api/v2/project_submissions', 'list of all the project submission of a project card or list of all the submissions made by a user.'
  description <<-EOS
    List of all the submissions for a project card or all submissions by the user
  EOS
  param :project_id, Integer, desc: 'Id of the project', required: false
  param :filter, String, desc: 'submitter/reviewer/all are three filters to fetch
  the list of submissions', required: false
  param :offset, Integer, desc: 'Result offset; default 0', required: false
  param :limit, Integer, desc: 'Result limit; default 15', required: false
  def index
    if params[:filter].present?
      case params[:filter]
      when "submitter"
        submissions = current_user.project_submissions.awaiting_reviews
      when "reviewer"
        find_project
        submissions = ProjectSubmission.reviews_required([@project.id])
      when "all"
        #This returns all submissions of a project
        find_project
        submissions = current_user.project_submissions.where("project_id in (?)", [@project.id])
      end
    else
      find_project
      submissions = current_user.project_submissions.reviews_required([@project.id])
    end
    @submissions_count = submissions.count
    @submissions =
      submissions.order("project_submissions.id desc").limit(limit).offset(offset)
    head :no_content if @submissions.blank?
  end

  private
  def find_project
    project_id = (params[:project_id] || params[:project_submission] &&
                  params[:project_submission][:project_id] || nil)
    @project = Project.find_by(id: project_id) if project_id.present?
    render_not_found('Content not found') and return if @project.blank?
  end

  def project_submission_params
    params.require(:project_submission).permit(
      :description, :project_id,
      filestack: [:filename, :handle, :mimetype, :original_path, :size, :source,
                  :url, :upload_id, :original_file, :status, :key, :container])
  end
end

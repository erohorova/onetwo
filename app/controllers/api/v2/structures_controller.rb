class Api::V2::StructuresController < Api::V2::BaseController
  before_action :set_parent, only: [:create, :index]
  before_action :set_structure, only: [:destroy, :edit, :update, :reorder, :show]

  api :POST, 'api/v2/structures', 'Create a new structure in an organization'
  param :structure, Hash, required: true do
    param :display_name, String, desc: 'card message - can include links and mentions', required: true
    param :parent_type, String, desc: 'Organization/Channel/Card', required: true
    param :parent_id, Integer, desc: 'Organization/Channel/Card', required: true
    param :context, String, desc: 'discover/channel', required: true
    param :slug, String, desc: 'cards/channels/videos', required: true
  end
  formats ['json']
  api_version 'v2'
  def create
    @structure = current_org.structures.build(creator: current_user)
    @structure.parent = @parent
    @structure.display_name = structure_params[:display_name]
    @structure.current_context = structure_params[:context].downcase
    @structure.current_slug = structure_params[:slug].downcase

    unless @structure.save
      return render_unprocessable_entity(@structure.full_errors)
    end
  end

  api :GET, 'api/v2/structures', 'Get all structures for an organization'
  param :context, String, desc: 'Specify the area where structures will be listed', required: false
  param :only_enabled, String, desc: 'A flag to decide if all items must be retrieved or only enabled ones', required: false
  param :structure, Hash, required: true do
    param :parent_id, Integer, desc: 'id of the parent where the structures will be shown', required: true
    param :parent_type, String, desc: 'Type of the parent', required: true
  end
  param :slug, String, desc: 'Type to receive structure type'
  formats ['json']
  api_version 'v2'
  def index
    only_enabled = params[:only_enabled].try(:to_bool)

    @structures = current_org.structures.where(parent: @parent).includes(:structured_items)

    if only_enabled
      @structures = @structures.active
    end

    if params[:context].present?
      @structures = @structures.where(context: params[:context])
    end

    if params[:slug].present?
      @structures = @structures.where(slug: params[:slug])
    end

  end

  api :GET, 'api/v2/structures/:id', 'show a structure in an organization'
  param :id, Integer, desc: 'A structure ID', required: true
  formats ['json']
  api_version 'v2'
  def show
  end

  api :PUT, 'api/v2/structures/:id', 'Update a structure in an organization'
  param :id, Integer, desc: 'A structure ID', required: true
  param :structure, Hash, required: true do
    param :display_name, String, desc: 'card message - can include links and mentions', required: true
    param :parent_type, String, desc: 'Organization/Channel/Card', required: true
    param :parent_id, Integer, desc: 'Organization/Channel/Card', required: true
    param :context, String, desc: 'discover/channel', required: true
    param :slug, String, desc: 'cards/channels/videos', required: true
  end
  formats ['json']
  api_version 'v2'
  def update
    @structure.update(structure_params)
  end

  api :PUT, 'api/v2/structures/:id/reorder', 'Reorder a structure in an organization'
  param :id, Integer, desc: 'A structure ID', required: true
  param :entity_id, Integer, desc: 'ID of object that is moved', required: true
  param :entity_type, String, desc: 'Type of object that is moved', required: true
  param :position, Integer, desc: 'Specify the position where the object will be moved', required: true
  formats ['json']
  api_version 'v2'
  def reorder
    item_to_move = StructuredItem.fetch_by_entity(params[:entity_id], params[:entity_type], @structure)
    item_to_move.insert_at(params[:position])

    @structured_items = @structure.structured_items
  end

  api :DELETE, 'api/v2/structures/:id', 'Deletes a structure in an organization'
  param :id, Integer, desc: 'A structure ID', required: true
  formats ['json']
  api_version 'v2'
  def destroy
    unless @structure.destroy
      return render_unprocessable_entity(@structure.full_errors)
    end

    head :ok and return
  end

  private
    def structure_params
      params.require(:structure).permit(:display_name, :context, :parent_id, :parent_type, :enabled, :slug)
    end

    def set_parent
      if structure_params[:parent_type].blank? || structure_params[:parent_id].blank?
        return render_unprocessable_entity('Blank parent_type or parent_id')
      end

      @parent = load_entity_from_type_and_id(structure_params[:parent_type], structure_params[:parent_id])

      if @parent.nil?
        return render_unprocessable_entity('Invalid parent')
      end
    end

    def set_structure
      @structure = current_org.structures.find_by(id: params[:id])

      unless @structure
        render_not_found('No structure defined') and return
      end
    end
end

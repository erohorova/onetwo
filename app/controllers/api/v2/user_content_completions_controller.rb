class Api::V2::UserContentCompletionsController < Api::V2::BaseController

  has_limit_offset_constraints only: [:index]

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized to access channel'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, 'api/v2/user_content_completions', 'Get user content completion records of a specific user'
  param :state, String, desc: "state of the content, can be: started/completed, default: started", required: false
  param :card_type, String, desc: 'possible values: poll, media, pack, journey, course', required: true
  def index
    ensure_state_and_type_params

    cards = Card.user_completed_cards_with_state(
      params[:state],
      params[:card_type],
      current_user.id
    )
    @cards = cards.limit(limit).offset(offset)
    @total = cards.count

    @assignments = current_user.assignments.includes(:assignable).where(assignable_id: @cards.map(&:id)).index_by(&:assignable_id)
  end

  def ensure_state_and_type_params
    params[:state] ||= UserContentCompletion::STARTED
    params[:card_type] ||= 'media'

    if Card::EXPOSED_TYPES_V2.exclude?(params[:card_type])
      return render_unprocessable_entity('Invalid value for card type')
    end

    if [UserContentCompletion::STARTED, UserContentCompletion::COMPLETED].exclude?(params[:state])
      return render_unprocessable_entity('Invalid value for state')
    end
  end
end
# frozen_string_literal: true

class Api::V2::AdminEventsController < Api::V2::BaseController

  include EventTracker
  before_action :return_not_found#, :require_organization_admin

  #TODO: ACTIONS THAT ARE USING `record_integration_event` WILL BE SCHEDULED FOR DEPRECATION

  api :POST, 'api/v2/admin_events/integration_ecl_content_access_enabled',
             'record event when ecl content access is enabled for an org'
  def integration_ecl_content_access_enabled
    record_integration_event('integration_ecl_content_access_enabled')
  end

  api :POST, 'api/v2/admin_events/integration_ecl_content_access_disabled',
             'record event when ecl content access is disabled for an org'
  def integration_ecl_content_access_disabled
    record_integration_event('integration_ecl_content_access_disabled')
  end

  api :POST, 'api/v2/admin_events/integration_added',
             'record event when an integration is added'
  param :integration_name, String, desc: 'Integration name', required: true
  param :integration_attributes, Hash, desc: 'Integration attributes', required: true
  def integration_added
    record_integration_event('integration_added',
      integration_name: String, integration_attributes: Hash
    )
  end

  api :POST, 'api/v2/admin_events/integration_removed',
             'record event when an integration is removed'
  param :integration_name, String, desc: 'Integration name', required: true
  def integration_removed
    record_integration_event('integration_removed', integration_name: String)
  end

  api :POST, 'api/v2/admin_events/integration_activated',
             'record event when an integration is activated'
  param :integration_name, String, desc: 'Integration name', required: true
  def integration_activated
    record_integration_event('integration_activated', integration_name: String)
  end

  api :POST, 'api/v2/admin_events/integration_deactivated',
             'record event when an integration is deactivated'
  param :integration_name, String, desc: 'Integration name', required: true
  def integration_deactivated
    record_integration_event('integration_deactivated', integration_name: String)
  end

  api :POST, 'api/v2/admin_events/integration_featured',
             'record event when an integration is featured'
  param :integration_name, String, desc: 'Integration name', required: true
  def integration_featured
    record_integration_event('integration_featured', integration_name: String)
  end

  api :POST, 'api/v2/admin_events/integration_unfeatured',
             'record event when an integration is unfeatured'
  param :integration_name, String, desc: 'Integration name', required: true
  def integration_unfeatured
    record_integration_event('integration_unfeatured', integration_name: String)
  end

  api :POST, 'api/v2/admin_events/integration_edited',
             'record event when an integration is edited'
  param :integration_name, String, desc: 'Integration name', required: true
  param :integration_attributes, Hash, desc: 'Integration attributes', required: true
  def integration_edited
    record_integration_event('integration_edited',
      integration_name: String, integration_attributes: Hash
    )
  end

  api :POST, 'api/v2/admin_events/integration_content_fetched',
             'record event when an integration has its content fetched'
  param :integration_name, String, desc: 'Integration name', required: true
  def integration_content_fetched
    record_integration_event('integration_content_fetched', integration_name: String)
  end

  api :POST, 'api/v2/admin_events/lrs_integration_added',
             'record event when an LRS integration is added'
  param :integration_name, String, desc: 'Integration name', required: true
  param :integration_attributes, Hash, desc: 'Integration attributes', required: true
  def lrs_integration_added
    record_integration_event('lrs_integration_added',
      integration_name: String, integration_attributes: Hash
    )
  end

  api :POST, 'api/v2/admin_events/lrs_integration_activated',
             'record event when an lrs integration is activated'
  param :integration_name, String, desc: 'Integration name', required: true
  def lrs_integration_activated
    record_integration_event('lrs_integration_activated', integration_name: String)
  end

  api :POST, 'api/v2/admin_events/lrs_integration_deactivated',
             'record event when an lrs integration is deactivated'
  param :integration_name, String, desc: 'Integration name', required: true
  def lrs_integration_deactivated
    record_integration_event('lrs_integration_deactivated', integration_name: String)
  end

  api :POST, 'api/v2/admin_events/lrs_integration_edited',
             'record event when an LRS integration is edited'
  param :integration_name, String, desc: 'Integration name', required: true
  param :integration_attributes, Hash, desc: 'Integration attributes', required: true
  def lrs_integration_edited
    record_integration_event('lrs_integration_edited',
      integration_name: String, integration_attributes: Hash
    )
  end

  api :POST, 'api/v2/admin_events/lrs_integration_removed',
             'record event when an lrs integration is removed'
  param :integration_name, String, desc: 'Integration name', required: true
  def lrs_integration_removed
    record_integration_event('lrs_integration_removed', integration_name: String)
  end

  private

  def require_params(required_params)
    required_params.all? do |key, type|
      unless params[key].is_a?(type)
        render_unprocessable_entity(
          "Param #{key} is required. " +
          "It was either not given or is not of type #{type}."
        )
        return false
      end
      true
    end
  end

  def record_integration_event(event_name, required_params={})
    return unless require_params(required_params)
    job_args = required_params.keys.map do |key|
      { key.to_sym => params[key] }
    end
    record_custom_event(
      {
        event: event_name,
        actor: :current_user
      }.merge(job_args: job_args)
    )
    head :ok
  end

  def return_not_found
    render_with_status_and_message(
      'API has been removed', status: :gone
    ) and return
  end
end

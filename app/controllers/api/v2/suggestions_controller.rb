class Api::V2::SuggestionsController < Api::V2::BaseController
  include FollowableConcern
  include SearchConcern
  has_limit_offset_constraints only: [:index]

  api :GET, "api/v2/suggestions", "Get content, people, channel, team suggestions"
  param :q, String, :desc => "get suggestions for this string, defaults to empty string", :required => true
  param :limit, Integer, :desc => "limit number of results, default 10", :required => false
  param :content_type, String, :desc => "Get suggestions specific for example ['users', 'cards', 'channels', 'teams']", :required => false
  param :search_by, String, desc: "One of ['name', 'skills']. Suggest users for search only by skills or by user's name", required: false
  formats ['json']
  def index
    q = params[:q]
    render_unprocessable_entity("Query parameter q cannot be empty") and return if q.nil?

    do_search(suggest_params, query_type: 'or')
    @tags = TagSuggest.suggestions_for_destiny(term: q, size: limit, organization_id: current_org.id)
  end

  private

  def suggest_params
    params.permit(:q, content_type: [], exclude_cards: [])
  end
end

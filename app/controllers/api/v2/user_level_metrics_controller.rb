class Api::V2::UserLevelMetricsController < Api::V2::BaseController

  api :GET, "api/v2/user_level_metrics", "Get User Level Metrics for signed in user"
  param :period, String, desc: 'one of \'day\', \'week\', \'month\', \'year\' or \'alltime\'. Default to \'all_time\'.', required: false
  param :user_id, Integer, desc: 'Presence of User id will return respective user metrics info'
  formats ['json']
  api_version 'v2'
  def index
    if params[:user_id].present?
      @user = User.find_by(id: params[:user_id], organization: current_org)
      render_not_found if !@user.present?
    else
      @user = current_user
    end

    applicable_offsets = PeriodOffsetCalculator.applicable_offsets(Time.now.utc).with_indifferent_access

    time_period = params[:period] || :all_time
    time_period_offset = applicable_offsets[time_period]

    @metric = UserLevelMetric.where(period: time_period, offset: time_period_offset, user_id: @user.id).first

    # Dont calculate for inactive users, just send 0.
    @percentile = @metric.try(:percentile).to_i

    @topic_level_metrics = UserTopicLevelMetric.where(period: time_period, offset: time_period_offset, user_id: @user.id).
                                      group("user_id, tag_id").
                                      select("user_id, tag_id, sum(smartbites_score) as smartbites_score").
                                      order("smartbites_score DESC").
                                      limit(3)
    if @user.learning_topics_name.present?       
      @taxonomy_level_metrics = UserTaxonomyTopicLevelMetric.where(period: time_period, offset: time_period_offset, user_id: @user.id).
                                group("user_id, taxonomy_label").
                                where("taxonomy_label in (?)",@user.learning_topics_name).
                                select("user_id, taxonomy_label, sum(smartbites_score) as smartbites_score").
                                order("smartbites_score DESC")              
    end                                      
  end


  api :GET, "api/v2/user_level_metrics/trend", "Get trend of metrics for signed in user"
  param :period, String, :desc => 'one of \'day\', \'week\', \'month\' or  \'year\'. Default to \'day\'.', :required => false
  param :lookback, Integer, :desc => "Works with the period param, to say how many days, weeks, months, or year we should get data for. For example, if period=\'day\' and lookback=10, we will return data for 10 days in the past. Default: 10. Includes current period"
  formats ['json']
  api_version 'v2'
  def trend
    applicable_offsets = PeriodOffsetCalculator.applicable_offsets(Time.now.utc).with_indifferent_access
    if !params[:period].nil? && !applicable_offsets.has_key?(params[:period])
      render_unprocessable_entity("Bad period parameter. Use one of 'day', 'week', 'month', 'year' or 'alltime'") and return
    end

    period = params[:period] || :day
    lookback_count = params[:lookback].to_i || 10

    @offsets = ((applicable_offsets[period] - lookback_count + 1)..applicable_offsets[period]).to_a

    @metrics_by_offset = UserLevelMetric.where(period: period,
                                               offset: @offsets,
                                               user_id: current_user.id).
                                         index_by(&:offset)

    @inactive_percentiles_by_offset = Hash[
      @offsets.map do |offset|
        [offset, UserLevelMetric.get_percentile_for_inactive_users(organization_id: current_org.id,
                                                                   period: period,
                                                                   offset: offset)
        ]
      end
    ]
  end
end

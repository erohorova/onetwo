class Api::V2::TransactionsController < Api::V2::BaseController
  include CompoundIdReader

  before_action :find_orderable_and_price, only: :initiate_transaction
  before_action :validate_payment_params, only: :process_payment

  rescue_from EdcastError, with: :render_specific_error_response

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized to access channel'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :POST, 'api/v2/purchase/new', 'Create transaction, initiate for payment.'
  description <<-EOS
  This is the first step for a transaction,
  this creates the transaction with initial state.
  EOS
  param :orderable_id, Integer, desc: 'Id of a card, for which payment is done',
        required: true
  param :orderable_type, String, desc: 'Type of order for which order will be
        created. `org_subscription/card` ', required: true
  param :price_id, Integer, desc: 'Id of the price for the orderable, for which
        payment is done', required: true
  param :gateway, Integer, desc: 'gateway used for payment braintree/paypal', required: true
  def initiate_transaction
    @response = PaymentGatewayService.new(
      orderable_type: params["orderable_type"],
      orderable: @orderable,
      price: @price,
      user_id: current_user.id,
      organization_id: current_org.id,
      gateway: @gateway).prepare_payment!
  end

  api :POST, 'api/v2/purchase/complete', 'Process payment for a transaction.'
  description <<-EOS
  This is the final step for a payment transaction,
  this completes the transaction.
  EOS
  param :payment_method_nonce, String, desc: 'payment method nonce which is
        shared by braintree', required: true
  param :token, String, desc: 'transaction token which is shared by purchase new api',
        required: true
  param :gateway, String, desc: 'gateway used for transaction braintree/paypal',
        required: true
  def_param_group :paypal do
    param :paypal_data, Hash, required: true do
      param :payment_id, String, desc: 'payment id which is shared by paypal',
            required: true
      param :payer_id, String, desc: 'payer id which is shared by paypal',
            required: true
      param :token, String, desc: 'token which is shared by paypal',
            required: true
    end
  end
  def process_payment
    @transaction = PaymentGatewayService.process_payment!(
      token: params["token"],
      gateway: params["gateway"],
      nonce: params["payment_method_nonce"],
      payment_id: params.dig(:paypal_data, :payment_id),
      payer_id: params.dig(:paypal_data, :payer_id),
      paypal_token: params.dig(:paypal_data, :token))

    if @transaction.successful?
      order = @transaction.order
      if (order&.orderable_type == 'Card')
        card = order.orderable
        @auth_status = true
        @redirect_url = card&.resource&.url
      end
      @message = 'Payment is Successful'
    else
      message = @transaction.errors.full_messages.to_sentence
      message ||= "Something went wrong, please try again in sometime"
      render_unprocessable_entity(message) and return
    end
  end

  param :token, String, desc: 'Transaction token which is shared by purchase new api', required: true
  def cancel
    @transaction = PaymentGatewayService.cancel_payment!(token: params["token"])
    if @transaction.canceled?
      head :ok
    else
      message = @transaction.errors.full_messages.to_sentence
      render_unprocessable_entity(message)
    end
  end

  private
  def find_orderable_and_price
    orderable_id = params["orderable_id"]
    @orderable = case params["orderable_type"]&.downcase
                 when "org_subscription"
                   current_org.org_subscriptions.find_by(id: orderable_id )
                 when "card"
                   load_card_from_compound_id(orderable_id)
                 when "recharge"
                   Recharge.active.find_by(id: orderable_id)
                 end
    render_not_found('Order can not be found.') and return unless @orderable
    @price = @orderable.prices.find_by(id: params["price_id"])
    render_not_found('Price can not be found.') and return unless @price
    @gateway = params['gateway']
  end

  def validate_payment_params

    case params["gateway"]
    when 'braintree'
      if params.values_at("payment_method_nonce", "token").any?(&:blank?)
        message = 'payment_nonce or token can not be blank.'
      end
    when 'paypal'
      if [params[:token], params.dig(:paypal_data, :payment_id), params.dig(:paypal_data, :payer_id),params.dig(:paypal_data, :token)].any?(&:blank?)
        message = 'paypal data or token can not be blank.'
      end
    end
    render_unprocessable_entity(message) if message.present?
  end

  def render_specific_error_response(exception)
    render json: { message: exception.message, code: exception.code }, status: exception.http_status
  end

end

class Api::V2::SourcesCredentialsController < Api::V2::BaseController

  before_filter :require_superadmin, only: [:create, :destroy, :update]
  before_action :require_organization_admin, only: [:index, :show]

  resource_description do
    api_version 'v2'
    formats ['json']
    error 401, 'Unauthorized to access channel'
    error 422, 'Unprocessable entity'
    error 500, 'Internal server error'
  end

  api :GET, 'api/v2/sources_credentials', "Returns source credentials information"
  description <<-EOS
    This will return source credentials
  EOS
  def index
    @sources_credentials = current_org.sources_credentials
  end

  api :GET, 'api/v2/sources_credentials/:id', "Return source credentials information"
  description <<-EOS
    This will return info of a source credential
  EOS
  param :id, Integer, desc: 'Id of the source credential', required: true
  def show
    @source_credential = current_org.sources_credentials.find(params[:id])
  end

  api :POST, 'api/v2/sources_credentials', "Create new source credential"
  description <<-EOS
    This will create new source credential and return details
  EOS
  param :source_id, Integer, desc: 'Source Id for which configuration needs to be done', required: true
  param :auth_api_info, Hash, desc: "Attributes for auth_api_info" do
    param :url, String, desc: "Relative url of course unlock api", required: false
    param :method, String, desc: "Http Method for the course unlock api", required: false
    param :host, String, desc: "Host name for the course unlock api", required: false
    param :headers, Hash, desc: "Header information for the course unlock api", required: false
    param :auth_required, String, desc: "Boolean flag to indicate if authorization is required or not", required: false
  end
  param :source_name, Integer, desc: 'Source name of the course', required: true

  formats ['json']
  def create
    @source_credential = current_org
      .sources_credentials
      .build(source_id: params[:source_id],
         auth_api_info: params[:auth_api_info],
         source_name: params[:source_name])

    unless @source_credential.save
      render_unprocessable_entity("Error while generating source credentials #{@source_credential.errors.full_messages.join(",")}") and return
    end
    render 'show'
  end

  api :PUT, 'api/v2/sources_credentials/:id', "Update source credential information"
  description <<-EOS
    This will update source credential and return details
  EOS
  param :id, Integer, desc: 'Id of the source credential', required: true
  param :auth_api_info, Hash, desc: "Attributes for auth_api_info" do
    param :url, String, desc: "Relative url of course unlock api", required: false
    param :method, String, desc: "Http Method for the course unlock api", required: false
    param :host, String, desc: "Host name for the course unlock api", required: false
    param :headers, Hash, desc: "Header information for the course unlock api", required: false
    param :auth_required, String, desc: "Boolean flag to indicate if authorization is required or not", required: false
  end
  param :source_name, String, desc: 'Source name of the course', required: true
  formats ['json']
  def update
    @source_credential = current_org
      .sources_credentials
      .find(params[:id])

    unless @source_credential.update(auth_api_info: params[:auth_api_info], source_name: params[:source_name])
      render_unprocessable_entity("Error while updating source credentials #{@source_credential.errors.full_messages.join(",")}") and return
    end
    render 'show'
  end

  api :DELETE, 'api/v2/sources_credentials/:id', "Delete source credential"
  param :id, Integer, desc: 'Id of the credential', required: true
  description <<-EOS
    This will delete source credential
  EOS
  def destroy
    source_credential = current_org.sources_credentials.find(params[:id])

    if source_credential.destroy
      head :ok and return
    else
      render_unprocessable_entity("Error while deleting source credential #{source_credential.errors.full_messages.join(",")}") and return
    end

  end
end
